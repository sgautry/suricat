<?xml version="1.0" encoding="UTF-8"?>
<beans
    xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:aop="http://www.springframework.org/schema/aop"
    xmlns:p="http://www.springframework.org/schema/p"
    xmlns:util="http://www.springframework.org/schema/util"
    xmlns:security="http://www.springframework.org/schema/security"
    xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
	http://www.springframework.org/schema/security http://www.springframework.org/schema/security/spring-security.xsd
	http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop.xsd
    http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util.xsd">

    <!-- ========================= This Spring configuration file contains definitions for the optional core beans. ========================= -->
    <!-- The beans here are shared between all projects. -->
    <!-- A bean belongs here if all of the following conditions are met: -->
    <!-- 1. WebCentral will start without the bean. -->
    <!-- 2. The bean is a core Java class, or is in "ext" package. -->
    <!-- 3. The bean is either: -->
    <!--  - Referenced in a core Java class, and the reference is expected to fail (there is if(containsBean) or try/catch block around getBean call); -->
    <!--  Or -->
    <!--  - Referenced in an application Java class. -->

    <!-- Cascade and SQL injection handlers require archibus-common-resources.jar -->

    <!-- Cascade update/delete handler, used by the DataSource implementation. -->
    <bean
        id="cascadeHandler"
        class="com.archibus.datasource.CascadeHandlerImpl">
        <!-- 
            Used by SLA.
            
            Disables the allow_null property of the fields for cascade delete.
            For a cascade delete the field in the list is not going to be set to NULL but the entire record will be deleted.
            The value of the field should be in the format: {table name}.{field name}
        -->
        <property name="cascadeDeleteDoNotAllowNullFieldsNames">
            <util:list value-type="java.lang.String">
                <value>helpdesk_sla_request.bl_id</value>
                <value>helpdesk_sla_request.dp_id</value>
                <value>helpdesk_sla_request.dv_id</value>
                <value>helpdesk_sla_request.em_std</value>
                <value>helpdesk_sla_request.eq_id</value>
                <value>helpdesk_sla_request.eq_std</value>
                <value>helpdesk_sla_request.fl_id</value>
                <value>helpdesk_sla_request.pmp_id</value>
                <value>helpdesk_sla_request.prob_type</value>
                <value>helpdesk_sla_request.requestor</value>
                <value>helpdesk_sla_request.rm_id</value>
                <value>helpdesk_sla_request.site_id</value>
            </util:list>
        </property>
        <!-- 
            Used by cascade delete. Supported values (yes,no)
            
            Example: In case a room is deleted then the record in the child table is nulled as described: 

            - (default) if the property value ="yes" then set to null all multi part keys: activity_log.bl_id=null,activity_log.fl_id=null,activity_log.rm_id=null
            - if the property value ="no" - then set to null only the field from child table that references the parent key: activity_log.rm_id=null
        -->
        <property name="cascadeDeleteMultipartKeyNulling" value="yes"/>
        
    </bean>

    <!-- SQL injection handler. Checks whether SQL queries and restrictions passed from the client contain specified keywords, and stops the operation if they do. -->
    <bean
        id="sqlInjectionHandler"
        class="com.archibus.datasource.SqlInjectionHandlerImpl">
        <property name="reservedWords">
            <list>
                <value>SELECT</value>
                <value>UPDATE</value>
                <value>UNION</value>
                <value>DROP</value>
                <value>DELETE</value>
                <value>INSERT</value>
                <value>TABLE</value>
                <value>--</value>
                <value>/*</value>
                <value>AFM_GROUPS</value>
                <value>AFM_GROUPSFORROLES</value>
                <value>AFM_ROLES</value>
                <value>AFM_USERS</value>
                <value>@@</value>
            </list>
        </property>
        <property name="allowSubqueries" value="true"/>
    </bean>

    <!-- Additional, more strict SQL injection handler for the replaceColumn web service. Specifies keywords checked in addition to those in the sqlInjectionHandler. -->
    <bean
        id="sqlInjectionHandlerForReplaceColumn"
        class="com.archibus.datasource.SqlInjectionHandlerImpl">
        <property name="reservedWords">
            <list>
                <value>SELECT</value>
            </list>
        </property>
    </bean>

    <!-- Script injection handler. Checks whether field values passed from the client contain specified keywords, and stops the operation if they do. -->
    <bean
        id="scriptInjectionHandler"
        class="com.archibus.datasource.SqlInjectionHandlerImpl">
        <property name="reservedWords">
            <list>
                <value>&lt;SCRIPT</value>
                <value>&lt;IMG</value>
                <value>&lt;SVG</value>
                <value>&lt;EMBED</value>
                <value>&lt;OBJECT</value>
                <value>&lt;LINK</value>
                <value>&lt;AUDIO</value>
                <value>&lt;VIDEO</value>
                <value>&lt;STYLE</value>
                <value>URI(</value>
            </list>
        </property>
        <property name="checkForWordsOnly" value="false"/>
        <property name="checkForMatchingQuotes" value="false"/>
    </bean>

    <!-- Database Importer -->
    <!-- This bean is the backbone of all database importer API that is used by Project Update Wizard, Data Transfer related Jobs, Localization and other WFRs -->
    <bean
        id="databaseImporter"
        class="com.archibus.ext.importexport.importer.DatabaseImporterImpl"
        scope="prototype">
    </bean>
    <!-- Database Exporter -->
    <!-- This bean is the backbone of all database exporter API that is used by Project Update Wizard, Data Transfer related Jobs, Localization and other WFRs -->
    <bean
        id="databaseExporter"
        class="com.archibus.ext.importexport.exporter.DatabaseExporterImpl"
        scope="prototype">
    </bean>

    <!-- Data Transfer Job bean -->
    <!-- This bean supports the data transfer in/out job from the view file -->
    <bean
        id="dataTransfer"
        class="com.archibus.ext.datatransfer.DataTransferJob"
        scope="prototype"
        p:databaseImporter-ref="databaseImporter"
        p:databaseExporter-ref="databaseExporter">
    </bean>

</beans>
