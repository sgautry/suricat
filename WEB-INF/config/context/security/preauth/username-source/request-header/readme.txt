This folder contains configuration files for the following security configuration:

Preauth

Use SSO server for authentication (an example of such SSO server is SiteMinder).
Get userId from HTTP request header.

To use this configuration: 

1. In WEB-INF/config/security.properties modify security.configurationFile value:
	
	security.configurationFile=context/security/security-preauth-request-header.xml

2. Enter usernameKey parameter in \username-source\request-header\username-source.properties file.
SmartClient uses key "username".

3. Enter projectIdKey parameter in \projectid-source\request-header\projectid-source.properties file.
Web Central gets project name from HTTP request header specified by projectIdKey.
SmartClient uses key "projectId".

4. (Optional) Store the project name in a .properties file instead of passing it as a request header attribute:
By Default, Web Central looks for the project name to connect to in an additional header attribute (step 3 above). 
Alternatively, you can hardcode the project id into a properties file instead of passing it as a request header attribute. 
To do so, follow these steps:

	A. Open the following file in a text editor: context\security\security-preauth-request-header.xml 

	B. Locate the import:
	resource="preauth\projectid-source\request-header\projectid-source.xml"
	and change it to: 
	resource="preauth\projectid-source\property\projectid-source.xml"

	C. Open the following file in a text editor:
	context\security\preauth\projectid-source\property\projectid-source.properties

	D. Modify the projectId attribute to match the project name value (not the project title) for the project to be used. 
	This must match an active project name as defined in afm-projects.xml. 
	It is also important only to make changes to the projectId value (and not to the projectIdKey value). 
	So if you want to connect to the project name=”HQ-SQL Server” then your file should look like this:

	projectidSource.projectIdKey=projectId 
	projectidSource.projectId=HQ-SQL Server


