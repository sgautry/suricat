JARs in this folder are used for:
- Compiling and running JUnit tests (Gradle configurations testCompile, testRuntime);
- Running Gradle build (tasks related to JS, CSS) in Add-In Manager mode.
- Servlet API JARs - required for compilation in Add-In Manager mode.
