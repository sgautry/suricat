package com.archibus.app.sysadmin.updatewizard.project.transfer.mergeschema;

import static com.archibus.app.sysadmin.updatewizard.project.util.ProjectUpdateWizardConstants.*;

import java.util.List;

import com.archibus.app.sysadmin.updatewizard.project.util.*;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;
import com.archibus.utility.ExceptionBase;

/**
 *
 * Provides methods that returns specific DataSource object.
 * <p>
 *
 * Used by MergeSchema.
 *
 * @author Catalin Purice
 * @since 21.3
 *
 */
public final class DataSourceBuilder {

	/**
	 * Constant.
	 */
	private static final String REC_ACTION_FIELDS = "autonumbered_id,attributes,allow_null,afm_module,afm_type,afm_size,"
			+ "data_type,decimals,dep_cols,dflt_val,edit_group,edit_mask,enum_list,field_grouping,field_name,is_atxt,is_tc_traceable,"
			+ "max_val,min_val,ml_heading,num_format,primary_key,ref_table,review_group,sl_heading,string_format,table_name,validate_data,"
			+ "change_type,data_dict_diffs,sql_table_diffs,chosen_action,comments,transfer_status,"
			+ "(CASE WHEN chosen_action <> 'NO ACTION' THEN chosen_action ELSE rec_action END) rec_action";

	/**
	 * Constant.
	 */
	private static final String IN_CLAUSE_VALUES_FOR_APPLY = "APPLY CHANGE,DELETE FIELD";

	/**
	 * Private default constructor: utility class is non-instantiable.
	 */
	private DataSourceBuilder() {
		super();
	}

	/**
	 * @return afm_flds_trans dataSource.
	 */
	public static DataSource afmFldsTrans() {
		return ProjectUpdateWizardUtilities.createDataSourceForTable(AFM_FLDS_TRANS)
				.addRestriction(Restrictions.isNotNull(AFM_FLDS_TRANS, "data_dict_diffs"));
	}

	/**
	 * @param actionType
	 *            action type
	 * @return DataRecords records.
	 *         <p>
	 *         Suppress PMD warning "AvoidUsingSql" in this method.
	 *         <p>
	 *         Justification: Case #1: Statements with SELECT WHERE EXISTS ...
	 *         pattern.
	 */
	public static List<DataRecord> loadChanges(final ActionType actionType) {

		final String query = "SELECT %s FROM afm_flds_trans a1 WHERE autonumbered_id NOT IN "
				+ "(SELECT autonumbered_id FROM afm_flds_trans a1 WHERE change_type='NEW' "
				+ "AND EXISTS(SELECT 1 FROM afm_flds_trans a2 "
				+ "WHERE a2.table_name = a1.table_name AND a2.change_type='TBL_IS_NEW')) ";

		final DataSource dataS = DataSourceBuilder.afmFldsTrans();

		switch (actionType) {
		case REC_ACTION:
			dataS.addQuery(String.format(query, REC_ACTION_FIELDS));
			break;
		case CHOSEN_ACTION:
			dataS.addQuery(String.format(query, "*"));
			dataS.addRestriction(Restrictions.in(AFM_FLDS_TRANS, "chosen_action", IN_CLAUSE_VALUES_FOR_APPLY));
			break;
		default:
			throw new ExceptionBase("Unsupported action type: " + actionType);
		}

		dataS.addSort("(CASE WHEN afm_flds_trans",
				"change_type='TBL_IS_NEW' THEN 1 WHEN afm_flds_trans.change_type='NEW' THEN 2 WHEN afm_flds_trans.change_type='REF_TABLE' THEN 3 ELSE 4 END)");

		return dataS.getRecords();
	}

	/**
	 * @return afm_flds dataSource.
	 */
	public static DataSource afmFlds() {
		return ProjectUpdateWizardUtilities.createDataSourceForTable(AFM_FLDS);
	}

	/**
	 * @return afm_tbls dataSource.
	 */
	public static DataSource afmTbls() {
		return ProjectUpdateWizardUtilities.createDataSourceForTable(AFM_TBLS);
	}

	/**
	 * @return afm_flds_lang dataSource.
	 */
	public static DataSource afmFldsLang() {

		return DataSourceFactory.createDataSource().addTable(AFM_FLDS_LANG)
				.addField(ProjectUpdateWizardUtilities.TABLE_NAME).addField(ProjectUpdateWizardUtilities.FIELD_NAME)
				.addField("enum_list" + LangUtilities.getFieldSuffix())
				.addField("ml_heading" + LangUtilities.getFieldSuffix());

	}

}
