package com.archibus.app.sysadmin.command;

import java.io.FileInputStream;

import org.springframework.util.Assert;

import com.archibus.app.sysadmin.updatewizard.script.service.BatchFileJob;
import com.archibus.context.ContextStore;
import com.archibus.datasource.DataSourceTestBase;

/**
 * Command-line utility, to be invoked by the build script.
 * <p>
 * Reads ARCHIBUS database schema and applies it to SQL schema.
 *
 * Integration test for BatchFileJob. Invoked by BatchFileUtility; provides context for BatchFileJob
 * and runs BatchFileJob Job.
 * <p>
 * Integration test is used here as framework that runs WebCentral without servlet container.
 *
 * @author Catalin Purice
 * @since 23.2
 *
 */
public class BatchFileUtility extends DataSourceTestBase {

    /**
     * Property: full path of the .duw file to be imported.
     */
    private static String filePath;

    /**
     * Runs this unit test as command-line utility. Requires argument: file path.
     *
     * @param args Requires argument: file path name.
     *
     *            <p>
     *            Suppress PMD warning "DoNotCallSystemExit".
     *            <p>
     *            Justification: This is a command-line utility. TestRunner thread will not stop, it
     *            needs to be stopped by System.exit().
     */
    @SuppressWarnings("PMD.DoNotCallSystemExit")
    public static void main(final String[] args) {

        Assert.isTrue(args.length == 1, "Must be one argument: DUW file path");
        filePath = args[0];

        junit.textui.TestRunner.run(BatchFileUtility.class);
        System.exit(0);
    }

    /**
     * Tests run method of BatchFileJob.
     *
     * @throws Exception if DUW file not found, or DUW script fails.
     *
     */
    public void testRun() throws Exception {
        try {
            new BatchFileJob(new FileInputStream(filePath)).run();
            ContextStore.get().getDbConnection().commit();
        } catch (final Exception e) {
            // Report error to console
            e.printStackTrace();

            System.exit(1);
        }
    }

    @Override
    protected String[] getConfigLocations() {
        return new String[] { "/context/core/core-infrastructure.xml",
                "/context/core/core-optional.xml", "appContext-test.xml" };
    }
}
