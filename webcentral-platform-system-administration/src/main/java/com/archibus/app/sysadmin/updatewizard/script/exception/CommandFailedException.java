package com.archibus.app.sysadmin.updatewizard.script.exception;

import com.archibus.utility.ExceptionBase;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Exception to be used wen the command execution fails. This extends
 * AdaptorException because we want to stream.
 * <p>
 *
 * @author Catalin
 * @since 23.2
 *
 */
public class CommandFailedException extends ExceptionBase {

	/**
	 * Property: serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Default TODO constructor specifying TODO. Private default constructor:
	 * utility class is non-instantiable.
	 *
	 * @param localizedMessage
	 *            message
	 * @param cause
	 *            exception
	 */
	public CommandFailedException(final String localizedMessage, final Throwable cause) {
		super(localizedMessage, cause);
		// TODO Auto-generated constructor stub
	}

}
