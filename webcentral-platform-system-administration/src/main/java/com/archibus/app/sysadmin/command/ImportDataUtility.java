package com.archibus.app.sysadmin.command;

import java.io.*;

import org.springframework.util.Assert;

import com.archibus.context.ContextStore;
import com.archibus.datasource.DataSourceTestBase;
import com.archibus.ext.importexport.importer.*;
import com.archibus.ext.report.xls.XlsBuilder;
import com.archibus.utility.FileUtil;

/**
 * Command-line utility, to be invoked by the build script.
 * <p>
 * Imports data into ARCHIBUS database. The data must be in ARCHIBUS .csv Data
 * Transfer format. Requires arguments: dataPath, fileName.
 *
 * Integration test for TransferInJob. Invoked by ImportDataUtility; provides
 * context for TransferInJob and runs TransferInJob.
 * <p>
 * Integration test is used here as framework that runs WebCentral without
 * servlet container.
 *
 *
 * @author Valery Tydykov
 * @since 23.2
 *
 */
public class ImportDataUtility extends DataSourceTestBase {
	/**
	 * Property: absolute path of the file to import data from; does not include
	 * file name. Typical value :
	 * "C:/Projects/WebCentral/21.3_Production/WebCentral".
	 */
	private static String dataPath;

	/**
	 * Property: name of the file to import data from. Typical value :
	 * "afm_flds.csv".
	 */
	private static String fileName;

	/**
	 * Runs this unit test as command-line utility.
	 *
	 * Supports the import of the CSV/XLS/XLSX files. Requires 2 parameters:
	 * [0](mandatory) - path to csv/xls file [1](mandatory) - the name of the
	 * file including extension
	 *
	 * @param args
	 *            Requires arguments: dataPath, fileName.
	 *
	 *            <p>
	 *            Suppress PMD warning "DoNotCallSystemExit".
	 *            <p>
	 *            Justification: This is a command-line utility. TestRunner
	 *            thread will not stop, it needs to be stopped by System.exit().
	 */
	@SuppressWarnings("PMD.DoNotCallSystemExit")
	public static void main(final String[] args) {
		Assert.isTrue(args.length == 2, "Must be two arguments: dataPath fileName");

		dataPath = args[0];
		fileName = args[1];

		junit.textui.TestRunner.run(ImportDataUtility.class);
		System.exit(0);
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "/context/core/core-infrastructure.xml", "/context/core/core-optional.xml",
				"appContext-test.xml" };
	}

	/**
	 * Tests run method of TransferInJob.
	 *
	 * @throws FileNotFoundException
	 *             if specified file to import data from not found.
	 */
	public void testRun() throws FileNotFoundException {
		final DatabaseImporter dtInManager = (DatabaseImporter) ContextStore.get()
				.getBean(DatabaseImporterImpl.DATABASEIMPORTOR_BEAN);

		XlsBuilder.FileFormatType fileType = null;
		final String extension = FileUtil.getExtension(fileName);

		if (XlsBuilder.FileFormatType.CSV.name().equalsIgnoreCase(extension)) {
			fileType = XlsBuilder.FileFormatType.CSV;
		} else if (XlsBuilder.FileFormatType.XLS.name().equalsIgnoreCase(extension)) {
			fileType = XlsBuilder.FileFormatType.XLS;
		} else if (XlsBuilder.FileFormatType.XLSX.name().equalsIgnoreCase(extension)) {
			fileType = XlsBuilder.FileFormatType.XLSX;
		}

		if (fileType == null) {
			fail("Unsupported file type.");
		} else {
			final InputStream inputStream = new FileInputStream(new File(dataPath + File.separator + fileName));
			dtInManager.importData(inputStream, fileType, true, dataPath, true, "", true);

			// Commit the changes
			ContextStore.get().getDbConnection().commit();
		}

	}
}
