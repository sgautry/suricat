package com.archibus.app.sysadmin.command;

import java.util.ArrayList;

import org.springframework.util.Assert;

import com.archibus.app.sysadmin.updatewizard.project.job.AddTableNamesToTransferSetJob;
import com.archibus.app.sysadmin.updatewizard.schema.job.UpdateSchemaJob;
import com.archibus.context.ContextStore;
import com.archibus.datasource.DataSourceTestBase;

/**
 * Command-line utility, to be invoked by the build script.
 * <p>
 * Reads ARCHIBUS database schema and applies it to SQL schema.
 *
 * Integration test for UpdateSchemaJob. Invoked by UpgradeSchemaUtility;
 * provides context for UpdateSchemaJob and runs UpdateSchemaJob.
 * <p>
 * Integration test is used here as framework that runs WebCentral without
 * servlet container.
 *
 *
 * @author Valery Tydykov
 * @since 23.2
 *
 */
public class UpdateSchemaUtility extends DataSourceTestBase {

	/**
	 * Property values.
	 */
	private static final String[] PARAMS = { "fk", "a", "r" };

	/**
	 * Property: name of the table for which schema should be updated. Typical
	 * value : "ac" or "bl%".
	 */
	private static String tableNameLikeExpression;

	/**
	 * Property: re-creates the foreign keys for the specified tables.
	 */
	private static boolean recreateForeignKeys;

	/**
	 * Property: re-creates the foreign keys for the specified tables.
	 */
	private static boolean isRecreateTable;

	/**
	 * Runs this unit test as command-line utility. Requires argument: table
	 * name.
	 *
	 * @param args
	 *            Requires argument: table name.
	 *
	 *
	 *
	 *            Requires 1, 2 or 3 parameters: [0](mandatory) - name of the
	 *            tables to be updated. Supports like expressions as well (bl%)
	 *            [1](optional) - specifying 'fk' will recreate the FK for the
	 *            tables as well [2](optional) - can be 'a' or 'r' and indicates
	 *            whether the tables should be altered or recreated.
	 *
	 *
	 *            <p>
	 *            Suppress PMD warning "DoNotCallSystemExit".
	 *            <p>
	 *            Justification: This is a command-line utility. TestRunner
	 *            thread will not stop, it needs to be stopped by System.exit().
	 */
	@SuppressWarnings("PMD.DoNotCallSystemExit")
	public static void main(final String[] args) {

		Assert.isTrue(args.length >= 1,
				"Must be at least one argument, max three: table name/like expression(required), fk, r/a, ");
		tableNameLikeExpression = args[0];

		if (args.length >= 2) {
			Assert.isTrue(args[1].equalsIgnoreCase(PARAMS[0]),
					"Second parameter can be \"fk\" (which means if the foreign keys would be recreated)");
			recreateForeignKeys = true;
		}

		if (args.length == 3) {
			Assert.isTrue(args[2].equalsIgnoreCase(PARAMS[2]) || args[2].equalsIgnoreCase(PARAMS[1]),
					"Thrid parameter can be \"a(alter)\" or \"r(recreate)\"");
			isRecreateTable = args[2].equalsIgnoreCase(PARAMS[2]);
		}

		junit.textui.TestRunner.run(UpdateSchemaUtility.class);
		System.exit(0);
	}

	/**
	 * Tests run method of UpdateSchemaJob.
	 */
	public void testRun() {
		new AddTableNamesToTransferSetJob(new ArrayList<String>(), false, tableNameLikeExpression, true).run();
		new UpdateSchemaJob(true, false, isRecreateTable, recreateForeignKeys, false, "").run();
		// Commit the changes
		ContextStore.get().getDbConnection().commit();
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "/context/core/core-infrastructure.xml", "/context/core/core-optional.xml",
				"appContext-test.xml" };
	}
}
