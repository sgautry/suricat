package com.archibus.app.common.extensionsarcgis;

import org.json.JSONObject;

import com.archibus.datasource.DataSourceTestBase;

/**
 *
 * Test JsonReaderWriter#readFile.
 *
 */
public class TestReadJson extends DataSourceTestBase {

	/**
	 * Read a sample JSON file.
	 */
	public void testReadJson() {

		/*
		 * Create the file name.
		 */
		final String filename = ArchibusProjectUtilities.getGeoJsonPath() + "json-test.json";
		JSONObject jsonTest = null;

		/*
		 * Read the file.
		 */
		try {
			jsonTest = JsonReadWriteUtilities.readFile(filename);
		} finally {
			assertNotNull(jsonTest);
		}

	}

}
