package com.archibus.app.common.extensionsarcgis;

import org.junit.experimental.categories.Category;

import com.archibus.context.ContextStore;
import com.archibus.core.test.fixture.category.DatabaseTest;
import com.archibus.datasource.DataSourceTestBase;
import com.archibus.jobmanager.EventHandlerContext;

@Category({ DatabaseTest.class })
public class TestUpdateArcgisBuildingFeatureData extends DataSourceTestBase {

    public void testUpdateArcgisBuildingFeatureData() {
        final EventHandlerContext context = ContextStore.get().getEventHandlerContext();

        final String whereClause = "bl_id='marriottharborbeach-floorplan-level1'";
        // final String whereClause = "1=1";

        context.addInputParameter("whereClause", whereClause);

        // ArcgisExtensionsService.updateArcgisFeatureDataWithRestriction("bl",
        // whereClause);
    }

}
