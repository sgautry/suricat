package com.archibus.app.common.extensionsarcgis;

import java.util.*;

import org.json.*;
import org.springframework.util.Assert;

import com.archibus.datasource.DataSourceTestBase;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Test the GeoTools 'project' method.
 *
 *
 * @author knight
 * @since 23.2
 *
 */
public class TestProjectFeatures extends DataSourceTestBase {

    public void testProjectFeatures() {

        // TODO: this test is currently broken.
        // projection is working however.
        // this was verified by publishing a drawing with the SCE.

        // create features
        final JSONArray features = new JSONArray();

        final JSONObject feature = new JSONObject();
        feature.put(ArcgisExtensionsConstantsJsonNames.JSON_TYPE,
            ArcgisExtensionsConstantsCommon.FEATURE);
        feature.put(ArcgisExtensionsConstantsJsonNames.JSON_ID, "MC02;1;1001");

        final JSONObject featureProperties = new JSONObject();
        featureProperties.put(ArcgisExtensionsConstantsTableFieldNames.FIELD_BL_ID, "MC_02");
        featureProperties.put(ArcgisExtensionsConstantsTableFieldNames.FIELD_FL_ID, "1");
        featureProperties.put(ArcgisExtensionsConstantsTableFieldNames.FIELD_RM_ID, "1001");
        featureProperties.put(ArcgisExtensionsConstantsTableFieldNames.FIELD_GEO_LEVEL, "1");
        featureProperties.put(ArcgisExtensionsConstantsTableFieldNames.FIELD_ASSET_TYPE, "rm");
        featureProperties.put(ArcgisExtensionsConstantsJsonNames.JSON_DWGNAME, "BED-MC02_1");
        feature.put(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES, featureProperties);

        final JSONObject featureGeometry = new JSONObject();
        featureGeometry.put(ArcgisExtensionsConstantsJsonNames.JSON_TYPE,
            ArcgisExtensionsConstantsCommon.POLYGON);
        final JSONArray coordinates = new JSONArray();
        final List<GeoJsonPoint> outerRing = new ArrayList<GeoJsonPoint>();
        outerRing.add(new GeoJsonPoint(716819.46, 3016326.21));
        outerRing.add(new GeoJsonPoint(716869.58, 3016379.82));
        outerRing.add(new GeoJsonPoint(716845.44, 3016402.38));
        outerRing.add(new GeoJsonPoint(716823.08, 3016423.29));
        outerRing.add(new GeoJsonPoint(716819.46, 3016326.21));
        coordinates.put(0, outerRing);
        featureGeometry.put(ArcgisExtensionsConstantsJsonNames.JSON_COORDINATES, coordinates);
        feature.put(ArcgisExtensionsConstantsJsonNames.JSON_GEOMETRY, featureGeometry);

        features.put(0, feature);

        // create feature collection
        final JSONObject crs = new JSONObject();
        final JSONObject crsProperties = new JSONObject();
        // "crs":{ "type":"name", "properties":{ "name":"EPSG:2249"}}
        crsProperties.put(ArcgisExtensionsConstantsJsonNames.JSON_NAME, "EPSG:2249");
        crs.put(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES, crsProperties);
        crs.put(ArcgisExtensionsConstantsJsonNames.JSON_TYPE,
            ArcgisExtensionsConstantsJsonNames.JSON_NAME);

        // "properties":{"dwgInfo":{"dwgname":"bed-mc02_1"}}
        final JSONObject collectionProperties = new JSONObject();
        final JSONObject dwgInfo = new JSONObject();
        dwgInfo.put(ArcgisExtensionsConstantsJsonNames.JSON_DWGNAME, "BED-MC02_1");
        collectionProperties.put(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES,
            collectionProperties);

        // Feature Collection Object
        //
        // {
        // "type":"FeatureCollection",
        // "crs":{
        // "type":"name",
        // "properties":{
        // "name":"EPSG:2249"
        // }
        // },
        // "properties":{
        // "dwgInfo":{
        // "dwgname":"bed-mc02_1"
        // }
        // },
        // "features":[]
        // }
        final JSONObject featureCollection = new JSONObject();
        featureCollection.put(ArcgisExtensionsConstantsJsonNames.JSON_TYPE,
            ArcgisExtensionsConstantsCommon.FEATURECOLLECTION);
        featureCollection.put(ArcgisExtensionsConstantsJsonNames.JSON_CRS, crs);
        featureCollection.put(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES,
            collectionProperties);
        featureCollection.put(ArcgisExtensionsConstantsJsonNames.JSON_FEATURES, features);

        // create outputSrs
        final String outputSrs = ArcgisExtensionsConstantsCommon.EPSG_WEB_MERC;

        // get the publishing configuration
        final ArcgisPublishingConfiguration arcgisPublishingConfiguration =
                new ArcgisPublishingConfiguration();

        // project the features
        final GeoJsonFeatureProjector geoJsonFeatureProjector = new GeoJsonFeatureProjector();
        final JSONObject geoJsonProj = geoJsonFeatureProjector.project(featureCollection, outputSrs,
            arcgisPublishingConfiguration);

        // test assertions
        Assert.notNull(geoJsonProj);

    }

    @Override
    protected String[] getConfigLocations() {
        return new String[] { "context\\core\\core-infrastructure.xml", "appContext-test.xml",
                "appContext-extensionsarcgis.xml" };
    }

}