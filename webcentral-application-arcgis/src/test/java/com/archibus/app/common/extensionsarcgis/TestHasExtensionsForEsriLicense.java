package com.archibus.app.common.extensionsarcgis;

import org.junit.experimental.categories.Category;

import com.archibus.core.test.fixture.category.DatabaseTest;
import com.archibus.datasource.DataSourceTestBase;
import com.archibus.utility.ExceptionBase;

/**
 *
 * Test the ArcGIS Extensions Service hasExtensionsForEsriLicense() method.
 *
 */
@Category({ DatabaseTest.class })
public class TestHasExtensionsForEsriLicense extends DataSourceTestBase {

	/**
	 * Test to see if a valid Extensions for Esri license is found.
	 */
	public void testHasExtensionsForEsriLicense() {

		try {
			ArcgisExtensionsService.checkExtensionsForEsriLicense();
			// fail("ExceptionBase expected");
		} catch (final ExceptionBase e) {
			assertEquals("A valid Extensions for ESRI license was not found.", e.getLocalizedMessage());
		}
	}
}
