package com.archibus.app.common.extensionsarcgis;

import org.junit.experimental.categories.Category;

import com.archibus.context.*;
import com.archibus.core.test.fixture.category.DatabaseTest;
import com.archibus.datasource.DataSourceTestBase;

/**
 *
 * Test the ArcgisExtensionsService requestArcgisServerAccessToken() method.
 *
 */
@Category({ DatabaseTest.class })
public class TestRequestArcgisExtensionsToken extends DataSourceTestBase {

    /**
     * Request an ArcGIS Server access token.
     */
    public void testRequestArcgisServerAccessToken() {

        final Context context = ContextStore.get();
        context.setRequestURL("http://localhost:8080/archibus");

        String accessToken = null;
        accessToken = ArcgisExtensionsService.requestArcgisServerAccessToken();

        assertNotNull("Failed requesting access token from ArcGIS Server.", accessToken);

    }

    @Override
    protected String[] getConfigLocations() {
        return new String[] { "context\\core\\core-infrastructure.xml", "appContext-test.xml",
                "appContext-extensionsarcgis.xml" };
    }

}
