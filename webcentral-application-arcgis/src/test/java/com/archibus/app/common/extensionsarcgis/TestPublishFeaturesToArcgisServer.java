package com.archibus.app.common.extensionsarcgis;

import com.archibus.context.ContextStore;
import com.archibus.datasource.DataSourceTestBase;
import com.archibus.jobmanager.EventHandlerContext;

public class TestPublishFeaturesToArcgisServer extends DataSourceTestBase {

    public void testPublishFeaturesToArcgisServer() {

        // Create the context/
        final EventHandlerContext context = ContextStore.get().getEventHandlerContext();

        // Add the drawing name the context.
        context.addInputParameter("dwgname", "bed-mc02_1");

        // Add method name to the context.
        context.addInputParameter("methodName", "publishFeaturesToArcgisServer");

        // Call the service.
        final ArcgisExtensionsService arcgisExtensionsService = new ArcgisExtensionsService();
        arcgisExtensionsService.callWorkflowRuleFromSmartClientExtension();

    }

    @Override
    protected String[] getConfigLocations() {
        return new String[] { "context\\core\\core-infrastructure.xml", "appContext-test.xml",
                "appContext-extensionsarcgis.xml" };
    }

}
