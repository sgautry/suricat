package com.archibus.app.common.extensionsarcgis;

import org.springframework.util.Assert;

import com.archibus.context.*;
import com.archibus.jobmanager.EventHandlerContext;
import com.archibus.model.licensing.*;
import com.archibus.utility.ExceptionBase;

/**
 *
 * The application interface class for the Extensions for Esri (ArcGIS).
 *
 * @author knight
 *
 */
public class ArcgisExtensionsService {

    /**
     * Constant: assert asset type has length message.
     */
    private static final String ASSET_TYPE_HAS_LENGTH_MESSAGE = "assetType must be specified!";

    /**
     * Constant: method name not specified error.
     */
    private static final String METHOD_NAME_NOT_SPECIFIED_ERROR =
            "Cannot call workflow rule from Smart Client Extension. Method name not specified.";

    /**
     * Constant: assert object id has length message.
     */
    private static final String OBJECTID_HAS_LENGTH_MESSAGE = "objectId must be specified!";

    /**
     * Provides access to multiple workflow rules for the Smart Client Extension through a single
     * method. The context should include a methodName parameter thats contains the name of the
     * method to call.
     *
     * @throws ExceptionBase if method name not specified, or method name not found, or Extensions
     *             for ESRI license was not found.
     */
    public void callWorkflowRuleFromSmartClientExtension() throws ExceptionBase {
        checkExtensionsForEsriLicense();

        final String methodName = getMethodNameFromContextParameter();
        final ArcgisExtensionsPublisher arcgisExtensionsPublisher = new ArcgisExtensionsPublisher();

        // call the method
        if ("saveDrawingGeoreferenceParameters".equalsIgnoreCase(methodName)) {
            GeoreferenceManager
                .saveDrawingGeoreferenceParametersToDatabaseFromSmartClientExtension();
        } else if ("publishFeaturesToArcgisServer".equalsIgnoreCase(methodName)) {
            // new method name
            arcgisExtensionsPublisher.publishFeatures();
        } else if ("publishRoomFeaturesToArcgisServer".equalsIgnoreCase(methodName)) {
            // legacy method name
            arcgisExtensionsPublisher.publishFeatures();
        } else {
            // @non-translatable
            final String errorMessage = String.format(
                "Cannot call workflow rule from Smart Client Extension. Method name not found=[%s]",
                methodName);
            throw new ExceptionBase(errorMessage);
        }
    }

    /**
     * Returns methodName specified as parameter in current Context.
     *
     * @return methodName specified as parameter in current Context.
     * @throws ExceptionBase if method name not specified.
     */
    private String getMethodNameFromContextParameter() throws ExceptionBase {
        String methodName = null;

        // get method name from context
        final EventHandlerContext context = ContextStore.get().getEventHandlerContext();
        if (context.parameterExists(ArcgisExtensionsConstantsCommon.PARAMETER_METHODNAME)) {
            methodName = (String) context
                .getParameter(ArcgisExtensionsConstantsCommon.PARAMETER_METHODNAME);
        } else {
            throw new ExceptionBase(METHOD_NAME_NOT_SPECIFIED_ERROR);
        }

        return methodName;
    }

    /**
     * Checks to see if a valid Extensions for ESRI license exists.
     *
     * Called from the ArcGIS Extensions Service and the Enhanced Map Control for Esri.
     *
     * @throws ExceptionBase if Extensions for ESRI license was not found.
     */
    static void checkExtensionsForEsriLicense() throws ExceptionBase {
        if (!hasExtensionsForEsriLicense()) {
            // @non-translatable
            final String errorMessage =
                    String.format("A valid Extensions for ESRI license was not found.");
            throw new ExceptionBase(errorMessage);
        }
    }

    /**
     * Returns true if a valid Extensions for ESRI license exists.
     *
     * Called from the ArcGIS Extensions Service and the Enhanced Map Control for Esri.
     *
     * @return true if if a valid Extensions for ESRI license exists.
     *
     */
    public static boolean hasExtensionsForEsriLicense() {
        final LicenseManager licenseManager = ContextStore.get().getLicenseManager();
        final ActivityLicense activityLicense =
                licenseManager.getLicenseValues().findActivityLicense("AbRPLMEsriExtensions");
        return activityLicense.isEnabled();

    }

    /**
     *
     * Updates fields in the ArcGIS feature layer with values from fields in the ARCHIBUS asset
     * table. Updates features by OBJECTID.
     *
     * Called from the Enhanced Map Control for Esri (JS).
     *
     * @param assetType The ARCHIBUS asset type.
     * @param objectId The ArcGIS feature OBJECTID to update.
     * @throws ExceptionBase if Extensions for ESRI license was not found.
     */
    public void updateArcgisFeatureDataByObjectId(final String assetType, final String objectId)
            throws ExceptionBase {
        Assert.hasLength(assetType, ASSET_TYPE_HAS_LENGTH_MESSAGE);
        Assert.hasLength(objectId, OBJECTID_HAS_LENGTH_MESSAGE);

        checkExtensionsForEsriLicense();

        final ArcgisExtensionsPublisher arcgisExtensionsPublisher = new ArcgisExtensionsPublisher();
        arcgisExtensionsPublisher.updateFeatureDataByObjectId(assetType, objectId);
    }

    /**
     *
     * Request an access token from ArcGIS Server. A token is required when accessing secure ArcGIS
     * services.
     *
     * @return accessToken The ArcGIS Server access token.
     */
    public static String requestArcgisServerAccessToken() {

        final Context context = ContextStore.get();
        final ArcgisExtensionsAuthenticationClient arcgisExtensionsAuthenticationClient =
                (ArcgisExtensionsAuthenticationClient) context
                    .getBean("arcgisExtensionsAuthenticationClient");

        return arcgisExtensionsAuthenticationClient.requestAccessToken();
    }
}
