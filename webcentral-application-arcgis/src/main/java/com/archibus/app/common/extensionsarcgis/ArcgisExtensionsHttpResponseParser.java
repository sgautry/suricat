package com.archibus.app.common.extensionsarcgis;

import java.io.IOException;
import java.text.ParseException;

import org.apache.http.*;
import org.apache.http.util.EntityUtils;
import org.json.*;
import org.springframework.util.Assert;

import com.archibus.utility.ExceptionBase;

/**
 *
 * Parses the HTTP response from ArcGIS Server.
 *
 * Used by the Extensions for Esri.
 *
 * @author knight
 *
 */
public final class ArcgisExtensionsHttpResponseParser {

    /**
     * Private default constructor: utility class is non-instantiable.
     *
     * @throws InstantiationException always, since this constructor should never be called.
     */
    private ArcgisExtensionsHttpResponseParser() throws InstantiationException {
        throw new InstantiationException(
            "Never instantiate " + this.getClass().getName() + "; use static methods!");
    }

    /**
     *
     * Gets the result from the HTTP Response.
     *
     * @param httpResponse The HTTP response.
     * @return The result.
     */
    public static JSONObject getResult(final HttpResponse httpResponse) {

        /*
         * Assert httpResponse.
         */
        Assert.notNull(httpResponse, "httpResponse must be specified!");

        return parseResponse(httpResponse);
    }

    /**
     *
     * Parse the result of the HTTP Response.
     *
     * @param httpResponse The HTTP response.
     * @return The result.
     */
    private static JSONObject parseResponse(final HttpResponse httpResponse) {

        JSONObject result = null;
        String response = null;

        final HttpEntity httpEntity = httpResponse.getEntity();
        if (httpEntity != null) {
            try {
                response = EntityUtils.toString(httpEntity);
                final JSONTokener tokener = new JSONTokener(response);
                result = new JSONObject(tokener);
                EntityUtils.consume(httpEntity);
            } catch (final org.apache.http.ParseException exception) {
                // @non-translatable
                throw new ExceptionBase(ArcgisExtensionsConstantsErrors.ERROR_IO_EXCEPTION,
                    exception);
            } catch (final IOException exception) {
                // @non-translatable
                throw new ExceptionBase(ArcgisExtensionsConstantsErrors.ERROR_IO_EXCEPTION,
                    exception);
            } catch (final ParseException exception) {
                // @non-translatable
                throw new ExceptionBase(ArcgisExtensionsConstantsErrors.ERROR_IO_EXCEPTION,
                    exception);
            }
        }

        return result;
    }

}
