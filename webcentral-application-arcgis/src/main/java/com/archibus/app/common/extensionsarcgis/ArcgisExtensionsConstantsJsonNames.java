package com.archibus.app.common.extensionsarcgis;

/**
 *
 * JSON name constants for the ArcgisExtensions.
 *
 * @author knight
 *
 */
public final class ArcgisExtensionsConstantsJsonNames {

    /**
     * attribute JSON code.
     */
    public static final String JSON_ATTRIBUTES = "attributes";

    /**
     * background JSON code.
     */
    public static final String JSON_BACKGROUND = "background";

    /**
     * coordinates JSON code.
     */
    public static final String JSON_COORDINATES = "coordinates";

    /**
     * code JSON code.
     */
    public static final String JSON_CODE = "code";

    /**
     * crs JSON code.
     */
    public static final String JSON_CRS = "crs";

    /**
     * detail JSON code
     */
    public static final String JSON_DETAIL = "detail";

    /**
     * dwgInfo JSON code.
     */
    public static final String JSON_DWGINFO = "dwgInfo";

    /**
     * dwgname JSON code.
     */
    public static final String JSON_DWGNAME = "dwgname";

    /**
     * description JSON code.
     */

    public static final String JSON_DESCRIPTION = "description";

    /**
     * eq JSON code.
     */
    public static final String JSON_EQ = "eq";

    /**
     * error JSON code.
     */
    public static final String JSON_ERROR = "error";

    /**
     * error_description JSON code.
     */
    public static final String JSON_ERROR_DESCRIPTION = "error_description";

    /**
     * features JSON code.
     */
    public static final String JSON_FEATURES = "features";

    /**
     * geoX JSON code.
     */
    public static final String JSON_GEOX = "geoX";

    /**
     * geoY JSON code.
     */
    public static final String JSON_GEOY = "geoY";

    /**
     * geoRotate JSON code.
     */
    public static final String JSON_GEOROTATE = "geoRotate";

    /**
     * geoScale JSON code.
     */
    public static final String JSON_GEOSCALE = "geoScale";

    /**
     * geoSRS JSON code.
     */
    public static final String JSON_GEOSRS = "geoSRS";

    /**
     * geoLevel JSON code.
     */
    public static final String JSON_GEOLEVEL = "geoLevel";

    /**
     * geometry JSON code.
     */
    public static final String JSON_GEOMETRY = "geometry";

    /**
     * gros JSON code.
     */
    public static final String JSON_GROS = "gros";

    /**
     * id JSON code.
     */
    public static final String JSON_ID = "id";

    /**
     * isGeoreferenced JSON code.
     */
    public static final String JSON_ISGEOREFERENCED = "isGeoreferenced";

    /**
     * layer JSON code.
     */
    public static final String JSON_LAYER = "layer";

    /**
     * name JSON code.
     */
    public static final String JSON_NAME = "name";

    /**
     * paths JSON code.
     */
    public static final String JSON_PATHS = "paths";

    /**
     * properties JSON code.
     */
    public static final String JSON_PROPERTIES = "properties";

    /**
     * rings JSON code.
     */
    public static final String JSON_RINGS = "rings";

    /**
     * rm JSON code.
     */
    public static final String JSON_RM = "rm";

    /**
     * spatialReference JSON code.
     */
    public static final String JSON_SPATIALREFERENCE = "spatialReference";

    /**
     * type JSON code.
     */
    public static final String JSON_TYPE = "type";

    /**
     * wkid JSON code.
     */
    public static final String JSON_WKID = "wkid";

    /**
     * x JSON code.
     */
    public static final String JSON_X = "x";

    /**
     * y JSON code.
     */
    public static final String JSON_Y = "y";

    /**
     *
     * Private default constructor: utility class is non-instantiable.
     *
     * @throws InstantiationException always, since this constructor should never be called.
     */
    private ArcgisExtensionsConstantsJsonNames() throws InstantiationException {
        throw new InstantiationException(
            "Never instantiate " + this.getClass().getName() + "; use static methods!");
    }

}
