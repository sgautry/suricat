package com.archibus.app.common.extensionsarcgis;

import org.json.*;
import org.springframework.util.Assert;

import com.archibus.utility.StringUtil;

/**
 *
 * Utilities for ArcgisExtensions to work with ARCHIBUS JSON features.
 *
 * @author knight
 *
 */
public final class ArchibusJsonUtilities {

	/**
	 * Constant: assert archibusJson not null message.
	 */
	private static final String ARCHIBUS_JSON_NOT_NULL_MESSAGE = "archibusJson must be specified!";

	/**
	 * Constant: assert asset not null message.
	 */
	private static final String ARCHIBUS_ASSET_NOT_NULL_MESSAGE = "asset must be specified!";

	/**
	 * Constant: assert archibus background object not null message.
	 */
	private static final String ARCHIBUS_BACKGROUND_OBJECT_NOT_NULL_MESSAGE = "archibusBackgroundObj must be specified!";

	/**
	 * Constant: assert asset key fields has length message.
	 */
	private static final String ASSET_KEY_FIELDS_HAS_LENGTH_MESSAGE = "assetKeyFields must be specified!";

	/**
	 * Constant: assert asset key has length message.
	 */
	private static final String ASSET_KEY_HAS_LENGTH_MESSAGE = "assetKey must be specified!";

	/**
	 * Constant: assert asset type has length message.
	 */
	private static final String ASSET_TYPE_HAS_LENGTH_MESSAGE = "assetType must be specified!";

	/**
	 * Constant: assert dwgname has length message.
	 */
	private static final String DWGNAME_HAS_LENGTH_MESSAGE = "dwgname must be specified!";

	/**
	 * Constant: assert dwg info not null message.
	 */
	private static final String DWG_INFO_NOT_NULL_MESSAGE = "properties must be specified!";

	/**
	 * Constant: assert extent min not null message.
	 */
	private static final String EXTENT_MIN_NOT_NULL_MESSAGE = "extentMin must be specified!";

	/**
	 * Constant: assert extent max not null message.
	 */
	private static final String EXTENT_MAX_NOT_NULL_MESSAGE = "extentMax must be specified!";

	/**
	 * Constant: assert features not null message.
	 */
	private static final String FEATURES_NOT_NULL_MESSAGE = "features must be specified!";

	/**
	 * Constant: assert geo level not null message.
	 */
	private static final String GEOLEVEL_NOT_NULL_MESSAGE = "geoLevel must be specified!";

	/**
	 * Private default constructor: utility class is non-instantiable.
	 *
	 * @throws InstantiationException
	 *             always, since this constructor should never be called.
	 */
	private ArchibusJsonUtilities() throws InstantiationException {
		throw new InstantiationException("Never instantiate " + this.getClass().getName() + "; use static methods!");
	}

	/**
	 *
	 * Get the drawing name from the ARCHIBUS JSON.
	 *
	 * @param archibusJson
	 *            Object with the ARCHIBUS JSON.
	 * @return String with the ARCHIBUS drawing name.
	 */
	public static String getDwgnameFromArchibusJson(final JSONObject archibusJson) {
		Assert.notNull(archibusJson, ARCHIBUS_JSON_NOT_NULL_MESSAGE);

		final String id = archibusJson.getString(ArcgisExtensionsConstantsCommon.ID_STRING).toString();
		final String[] idStrings = id.split("[\\\\]");
		final String dwgFilename = idStrings[idStrings.length - 1];

		return dwgFilename.substring(0, dwgFilename.length() - ArcgisExtensionsConstantsCommon.INT_4);
	}

	/**
	 *
	 * Verify that an ARCHIBUS asset is valid. Checks to see that the asset has
	 * a key, asset type, and shape.
	 *
	 * @param archibusAsset
	 *            The ARCHIBUS asset.
	 * @return true if the asset is valid, false if the asset is not valid.
	 */
	public static Boolean assetIsValid(final JSONObject archibusAsset) {
		Assert.notNull(archibusAsset, ARCHIBUS_ASSET_NOT_NULL_MESSAGE);
		Boolean valid = false;

		if (archibusAsset.has(ArcgisExtensionsConstantsCommon.KEY)
				&& archibusAsset.has(ArcgisExtensionsConstantsCommon.ASSETTYPE)
				&& archibusAsset.has(ArcgisExtensionsConstantsCommon.SHAPE)) {

			final JSONObject shape = archibusAsset.getJSONObject(ArcgisExtensionsConstantsCommon.SHAPE);
			if (shape.has(ArcgisExtensionsConstantsCommon.EXTENTSMIN)
					&& shape.has(ArcgisExtensionsConstantsCommon.EXTENTSMAX)) {
				valid = true;
			}
		}

		return valid;
	}

	/**
	 *
	 * Create the feature properties for an ARCHIBUS asset.
	 *
	 * @param geoLevel
	 *            The drawing geo level.
	 * @param assetKeyFields
	 *            The asset key fields.
	 * @param assetKey
	 *            The asset key.
	 * @param assetType
	 *            The ARCHIBUS asset type.
	 * @param dwgname
	 *            The ARCHIBUS drawing name.
	 * @return Object with the feature properties.
	 */
	public static JSONObject createFeatureProperties(final int geoLevel, final String[] assetKeyFields,
			final String assetKey, final String assetType, final String dwgname) {

		Assert.notNull(geoLevel, GEOLEVEL_NOT_NULL_MESSAGE);
		Assert.notEmpty(assetKeyFields, ASSET_KEY_FIELDS_HAS_LENGTH_MESSAGE);
		Assert.hasLength(assetKey, ASSET_KEY_HAS_LENGTH_MESSAGE);
		Assert.hasLength(assetType, ASSET_TYPE_HAS_LENGTH_MESSAGE);
		Assert.hasLength(dwgname, DWGNAME_HAS_LENGTH_MESSAGE);

		final JSONObject featureProperties = new JSONObject();

		if (StringUtil.notNullOrEmpty(geoLevel)) {
			featureProperties.put(ArcgisExtensionsConstantsTableFieldNames.FIELD_GEO_LEVEL, geoLevel);
		} else {
			featureProperties.put(ArcgisExtensionsConstantsTableFieldNames.FIELD_GEO_LEVEL, null);
		}

		// TODO multi-part keys?
		featureProperties.put(assetKeyFields[0], assetKey);

		featureProperties.put(ArcgisExtensionsConstantsTableFieldNames.FIELD_ASSET_TYPE, assetType);
		featureProperties.put(ArcgisExtensionsConstantsCommon.DWGNAME, dwgname);

		return featureProperties;
	}

	/**
	 *
	 * Creates an ARCHIBUS feature collection. (This is similar to a GeoJSON
	 * feature collection, however, it is not truely a GeoJSON feature
	 * collection until it has been projected).
	 *
	 *
	 * @param dwgInfo
	 *            The dwgInfo object.
	 * @param features
	 *            The features of the feature collection.
	 * @return The GeoJSON feature collection;
	 */
	public static JSONObject createFeatureCollection(final JSONObject dwgInfo, final JSONArray features) {

		Assert.notNull(dwgInfo, DWG_INFO_NOT_NULL_MESSAGE);
		Assert.notNull(features, FEATURES_NOT_NULL_MESSAGE);

		final JSONObject properties = new JSONObject();
		properties.put(ArcgisExtensionsConstantsJsonNames.JSON_DWGINFO, dwgInfo);
		final JSONObject featureCollection = new JSONObject();
		featureCollection.put(ArcgisExtensionsConstantsJsonNames.JSON_FEATURES, features);
		featureCollection.put(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES, properties);
		featureCollection.put(ArcgisExtensionsConstantsJsonNames.JSON_TYPE,
				ArcgisExtensionsConstantsCommon.FEATURECOLLECTION);

		return featureCollection;
	}

	/**
	 *
	 * Calculate the center point of an extent.
	 *
	 * @param extentMin
	 *            The lower left corner of the extent.
	 * @param extentMax
	 *            The upper right corner of the extent.
	 * @return GeoJsonPoint with center of the extent.
	 */
	public static GeoJsonPoint calculateCenterFromExtent(final JSONObject extentMin, final JSONObject extentMax) {

		Assert.notNull(extentMin, EXTENT_MIN_NOT_NULL_MESSAGE);
		Assert.notNull(extentMax, EXTENT_MAX_NOT_NULL_MESSAGE);

		final double xMin = extentMin.getDouble(ArcgisExtensionsConstantsCommon.UPPERCASE_X);
		final double yMin = extentMin.getDouble(ArcgisExtensionsConstantsCommon.UPPERCASE_Y);
		final double xMax = extentMax.getDouble(ArcgisExtensionsConstantsCommon.UPPERCASE_X);
		final double yMax = extentMax.getDouble(ArcgisExtensionsConstantsCommon.UPPERCASE_Y);

		final double xCoord = (xMax + xMin) / 2;
		final double yCoord = (yMax + yMin) / 2;

		final GeoJsonPoint centerPoint = new GeoJsonPoint(xCoord, yCoord);

		return centerPoint;
	}

	/**
	 *
	 * Get the geometry for an ARCHIBUS background object.
	 *
	 * @param archibusBackgroundObj
	 *            ARCHIBUS background object with geometry.
	 * @return Array with background coordinates.
	 */
	public static JSONArray getGeometryForBackgroundObject(final JSONObject archibusBackgroundObj) {
		final JSONArray backgroundGeometry = new JSONArray();

		Assert.notNull(archibusBackgroundObj, ARCHIBUS_BACKGROUND_OBJECT_NOT_NULL_MESSAGE);

		/*
		 * Check to see if we have a line.
		 */
		if (archibusBackgroundObj.has(ArcgisExtensionsConstantsCommon.UPPERCASE_P1)
				&& (archibusBackgroundObj.has(ArcgisExtensionsConstantsCommon.UPPERCASE_P2))) {
			backgroundGeometry.put(archibusBackgroundObj);
		}

		/*
		 * Check to see if we have a segment.
		 */
		if (archibusBackgroundObj.has(ArcgisExtensionsConstantsCommon.SEGMENTS)) {
			backgroundGeometry.put(archibusBackgroundObj);
		}

		/*
		 * Check to see if we have a block insert.
		 */
		if (archibusBackgroundObj.has(ArcgisExtensionsConstantsCommon.SHAPE)) {

			String backgroundLayer = "";
			if (archibusBackgroundObj.has(ArcgisExtensionsConstantsCommon.LAYER)) {
				backgroundLayer = archibusBackgroundObj.getString(ArcgisExtensionsConstantsCommon.LAYER);
			}

			final JSONObject backgroundShape = archibusBackgroundObj
					.getJSONObject(ArcgisExtensionsConstantsCommon.SHAPE);
			if (backgroundShape.has(ArcgisExtensionsConstantsCommon.PARTS)) {
				final JSONArray backgroundParts = backgroundShape.getJSONArray(ArcgisExtensionsConstantsCommon.PARTS);

				final JSONObject backgroundSegments = new JSONObject();
				backgroundSegments.put(ArcgisExtensionsConstantsCommon.LAYER, backgroundLayer);
				backgroundSegments.put(ArcgisExtensionsConstantsCommon.SEGMENTS, backgroundParts);

				backgroundGeometry.put(backgroundSegments);
			}
		}

		return backgroundGeometry;
	}

}
