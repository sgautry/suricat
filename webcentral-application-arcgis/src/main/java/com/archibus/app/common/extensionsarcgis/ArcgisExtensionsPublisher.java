package com.archibus.app.common.extensionsarcgis;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.util.Assert;

import com.archibus.context.ContextStore;
import com.archibus.jobmanager.EventHandlerContext;
import com.archibus.utility.ExceptionBase;

/**
 *
 * Provides methods to publish and update ARCHIBUS assets to GeoJSON and ArcGIS feature services for
 * the ArcgisExtensions.
 *
 * Used by the Extensions for Esri.
 *
 * @author knight
 *
 */
public class ArcgisExtensionsPublisher {

    /**
     * Constant: assert asset type has length message.
     */
    private static final String ASSET_TYPE_HAS_LENGTH_MESSAGE = "assetType must be specified!";

    /**
     * Constant: assert dwgname has length message.
     */
    private static final String DWGNAME_HAS_LENGTH_MESSAGE = "dwgname must be specified!";

    /**
     * Constant: assert esri feature collection not null message.
     */
    private static final String ESRI_FEATURE_COLLECTION_NOT_NULL_MESSAGE =
            "esriFeatureCollection must be specified!";

    /**
     * Constant: assert feature layer not null message.
     */
    private static final String FEATURE_LAYER_NOT_NULL_MESSAGE = "featureLayer must be specified!";

    /**
     * Constant: assert object id has length message.
     */
    private static final String OBJECTID_HAS_LENGTH_MESSAGE = "objectId must be specified!";

    /**
     * Constant: assert where clause has length message.
     */
    private static final String WHERE_CLAUSE_HAS_LENGTH_MESSAGE = "whereClause must be specified!";

    /**
     * Logger to log messages to archibus log.
     */
    protected final Logger logger = Logger.getLogger(this.getClass());

    /**
     * Publish drawing features to GeoJSON or ArcGIS. The context must include the drawing name.
     *
     * @throws ExceptionBase if the context does not contain the dwgname.
     */
    public void publishFeatures() throws ExceptionBase {

        // Do we need to Assert the DI objects?

        if (this.logger.isInfoEnabled()) {
            this.logger.info("publishFeatures() -> Starting publish features.");
        }

        /*
         * Get the dwgname from the context.
         */
        String dwgname = null;
        final EventHandlerContext context = ContextStore.get().getEventHandlerContext();
        if (context.parameterExists(ArcgisExtensionsConstantsCommon.DWGNAME)) {
            dwgname = (String) context.getParameter(ArcgisExtensionsConstantsCommon.DWGNAME);
            publishFeaturesForDrawing(dwgname);
        } else {
            // @ non-translatable
            throw new ExceptionBase("Failed to get drawing name from context.");
        }

        if (this.logger.isInfoEnabled()) {
            this.logger.info("publishFeatures() -> Completed publish features.");
        }
    }

    /**
     *
     * Publish features for the drawing.
     *
     * @param dwgname the ARCHIBUS drawing name.
     */
    private void publishFeaturesForDrawing(final String dwgname) {
        Assert.hasLength(dwgname, DWGNAME_HAS_LENGTH_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(String.format(
                "publishFeaturesForDrawing() --> Starting publish features for drawing: dwgname=[%s].",
                dwgname));
        }

        /*
         * Publish to GeoJSON.
         */
        final JSONObject assets = publishToGeoJson(dwgname);

        /*
         * Get the publishing configuration
         */
        final ArcgisPublishingConfiguration arcgisPublishingConfiguration =
                new ArcgisPublishingConfiguration();

        /*
         * Publish to ArcGIS Server.
         */
        if (ArcgisExtensionsConstantsCommon.UPPERCASE_ARCGIS
            .equalsIgnoreCase(arcgisPublishingConfiguration.getPublishFormat())) {
            this.publishToArcgisServer(assets);
        }

        if (this.logger.isInfoEnabled()) {
            this.logger.info(String.format(
                "publishFeaturesForDrawing() --> Completed publish features for drawing: dwgname=[%s].",
                dwgname));
        }
    }

    /**
     *
     * Publish the drawing to GeoJSON format.
     *
     * @param dwgname The ARCHIBUS drawing name.
     * @return The published GeoJSON.
     */
    private JSONObject publishToGeoJson(final String dwgname) {

        Assert.hasLength(dwgname, DWGNAME_HAS_LENGTH_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(String.format(
                "publishToGeoJson() --> Starting publishing to GeoJSON for drawing: dwgname=[%s].",
                dwgname));
        }

        /*
         * Create the asset file names.
         */
        final String assetsFilename = ArchibusProjectUtilities.getAssetsFilename(dwgname);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(String.format("publishToGeoJson() -> ARCHIBUS assetsFilename=[%s]",
                assetsFilename));
        }

        /*
         * Read the assets.
         */
        JSONObject assets = JsonReadWriteUtilities.readFile(assetsFilename.toLowerCase());

        /*
         * Create the GeoJSON path.
         */
        final String geoJsonPath = ArchibusProjectUtilities.getGeoJsonPath();
        if (this.logger.isInfoEnabled()) {
            this.logger.info(String.format("publishToGeoJson() -> geoJsonPath=[%s]", geoJsonPath));
        }

        /*
         * Get the publishing configuration
         */
        final ArcgisPublishingConfiguration arcgisPublishingConfiguration =
                new ArcgisPublishingConfiguration();

        /*
         * Convert ArchibusJSON to GeoJSON.
         */
        final ArchibusJsonConverter archibusJsonConverter = new ArchibusJsonConverter();
        assets = archibusJsonConverter.convertToGeoJson(assets, arcgisPublishingConfiguration);
        String outFilename = geoJsonPath + dwgname + "_CAD.json";
        JsonReadWriteUtilities.writeFile(assets, outFilename);

        /*
         * Transform CAD coordinates to local geographic coordinates.
         */
        final GeoJsonFeatureTransformer geoJsonFeatureTransformer = new GeoJsonFeatureTransformer();
        assets = geoJsonFeatureTransformer.transform(assets, arcgisPublishingConfiguration);
        outFilename = geoJsonPath + dwgname + "_LOCAL.json";
        JsonReadWriteUtilities.writeFile(assets, outFilename);

        /*
         * Transform local geographic coordinates to Lat-Lon.
         */
        final GeoJsonFeatureProjector geoJsonFeatureProjector = new GeoJsonFeatureProjector();
        final JSONObject assetsLL = geoJsonFeatureProjector.project(assets,
            ArcgisExtensionsConstantsCommon.EPSG_LAT_LON, arcgisPublishingConfiguration);
        outFilename = geoJsonPath + dwgname + "_LL.json";
        JsonReadWriteUtilities.writeFile(assetsLL, outFilename);

        /*
         * Project local geographic coordinates to Web Mercator.
         */
        assets = geoJsonFeatureProjector.project(assets,
            ArcgisExtensionsConstantsCommon.EPSG_WEB_MERC, arcgisPublishingConfiguration);
        outFilename = geoJsonPath + dwgname + "_WM.json";
        JsonReadWriteUtilities.writeFile(assets, outFilename);

        /*
         * Convert GeoJSON to EsriJSON.
         */
        final GeoJsonFormatConverter geoJsonFormatConverter = new GeoJsonFormatConverter();
        assets = geoJsonFormatConverter.convertToEsriJson(assets, arcgisPublishingConfiguration);
        outFilename = geoJsonPath + dwgname + "_ESRI.json";
        JsonReadWriteUtilities.writeFile(assets, outFilename);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(String.format(
                "publishToGeoJson() --> Completed publishing to GeoJSON for drawing: dwgname=[%s].",
                dwgname));
        }

        return assets;
    }

    /**
     *
     * Publish the drawing to ArcGIS.
     *
     * @param esriFeatureCollection The GeoJSON assets to publish.
     */
    private void publishToArcgisServer(final JSONObject esriFeatureCollection) {

        Assert.notNull(esriFeatureCollection, ESRI_FEATURE_COLLECTION_NOT_NULL_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info("publishToArcgisServer() --> Start publishing to ArcGIS.");
        }

        /*
         * Get the publishing configuration
         */
        final ArcgisPublishingConfiguration arcgisPublishingConfiguration =
                new ArcgisPublishingConfiguration();

        /*
         * Get the assets to publish from the publishing configuration.
         */
        final String[] publishAssetList = arcgisPublishingConfiguration.getPublishAssetTypes();

        /*
         * Publish each asset in the publish asset list.
         */
        for (final String assetType : publishAssetList) {

            /*
             * Add the features to ArcGIS Server.
             */
            final ArcgisFeatureServiceManager arcgisFeatureServiceManager =
                    new ArcgisFeatureServiceManager();
            arcgisFeatureServiceManager.addFeatures(esriFeatureCollection, assetType);

            /*
             * Get the feature layer.
             */
            final ArcgisFeatureLayerManager arcgisFeatureLayerManager =
                    new ArcgisFeatureLayerManager();
            final ArcgisFeatureLayer featureLayer = arcgisFeatureLayerManager.createFeatureLayer(
                ArcgisExtensionsConstantsCommon.ASSET_TYPE_BACKGROUND,
                ArcgisExtensionsConstantsCommon.SQL_ONE_EQUALS_ONE);

            /*
             * Call the connectors to update the feature data.
             */
            // TODO set the where clause for the feature layer?
            this.updateFeatureDataForDrawing(assetType, featureLayer);

        }

        /*
         * Publish the background graphics.
         */

        if (arcgisPublishingConfiguration.getPublishBackground()) {

            /*
             * Add the features to ArcGIS Server.
             */
            final ArcgisFeatureServiceManager arcgisFeatureServiceManager =
                    new ArcgisFeatureServiceManager();
            arcgisFeatureServiceManager.addFeatures(esriFeatureCollection,
                ArcgisExtensionsConstantsCommon.ASSET_TYPE_BACKGROUND);

            /*
             * Done. Don't call any connectors for background graphics.
             */

        }

        if (this.logger.isInfoEnabled()) {
            this.logger.info("publishToArcgisServer() --> Completed publishing to ArcGIS.");
        }

    }

    /**
     *
     * Update the feature data for a drawing. OBJECTID (required) and other configurable fields from
     * ArcGIS to ARCHIBUS. ARCHIBUS asset data (configurable) to ArcGIS.
     *
     * The context must include the drawing name.
     *
     * This is called as the last step in the Smart Client Extensions ArcGIS publishing process.
     *
     * @param assetType The ARCHIBUS asset type to update.
     * @param featureLayer The ArcGIS feature layer for the asset.
     *
     * @throws ExceptionBase if dwgname is not available in Context.
     */
    public void updateFeatureDataForDrawing(final String assetType,
            final ArcgisFeatureLayer featureLayer) throws ExceptionBase {

        Assert.hasLength(assetType, ASSET_TYPE_HAS_LENGTH_MESSAGE);
        Assert.notNull(featureLayer, FEATURE_LAYER_NOT_NULL_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(
                "updateFeatureDataForDrawing() --> Starting update feature data for drawing.");
        }

        /*
         * Get the dwgname from the context.
         */
        String dwgname = null;
        final EventHandlerContext context = ContextStore.get().getEventHandlerContext();
        if (context.parameterExists(ArcgisExtensionsConstantsCommon.DWGNAME)) {
            dwgname = (String) context.getParameter(ArcgisExtensionsConstantsCommon.DWGNAME);
        } else {
            // @non-translatable
            throw new ExceptionBase(
                "Failed to get 'dwgname' from Context. Cannot update feature data for drawing.");
        }

        if (this.logger.isInfoEnabled()) {
            this.logger.info(String.format("dwgname=[%s]", dwgname));
        }

        /*
         * Create the where clause.
         */
        final String whereClause =
                ArcgisExtensionsConstantsCommon.DWGNAME + ArcgisExtensionsConstantsCommon.EQUALS
                        + ArcgisExtensionsConstantsCommon.SINGLE_QUOTE + dwgname
                        + ArcgisExtensionsConstantsCommon.SINGLE_QUOTE;

        /*
         * Update the feature data.
         */
        updateFeatureData(assetType, featureLayer, whereClause);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(
                "updateFeatureDataForDrawing() --> Completed update feature data for drawing.");
        }

    }

    /**
     *
     * Update feature data by the ArcGIS OBJECTID.
     *
     * @param assetType The ARCHIBUS asset type.
     * @param objectId The OBJECTID of the ArcGIS feature to update.
     */
    public void updateFeatureDataByObjectId(final String assetType, final String objectId) {

        Assert.hasLength(assetType, ASSET_TYPE_HAS_LENGTH_MESSAGE);
        Assert.hasLength(objectId, OBJECTID_HAS_LENGTH_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(String.format(
                "updateFeatureDataByObjectId() --> Starting update feature data by object id: assetType=[%s], objectId=[%s].",
                assetType, objectId));
        }

        /*
         * Create the where clause.
         */
        final String whereClause = ArcgisExtensionsConstantsTableFieldNames.FIELD_GEO_OBJECTID
                + ArcgisExtensionsConstantsCommon.EQUALS + objectId;

        /*
         * Create the feature layer.
         */
        final ArcgisFeatureLayerManager arcgisFeatureLayerManager = new ArcgisFeatureLayerManager();
        final ArcgisFeatureLayer featureLayer =
                arcgisFeatureLayerManager.createFeatureLayer(assetType, whereClause);

        /*
         * Update the feature data.
         */
        updateFeatureData(assetType, featureLayer, whereClause);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(String.format(
                "updateFeatureDataByObjectId() --> Completed update feature data by object id: assetType=[%s], objectId=[%s].",
                assetType, objectId));
        }

    }

    /**
     *
     * Update feature data with a restriction.
     *
     * @param assetType The ARCHIBUS asset type.
     * @param whereClause The SQL where clause to apply to the ArcGIS features.
     */

    public void updateFeatureDataByRestriction(final String assetType, final String whereClause) {

        Assert.hasLength(assetType, ASSET_TYPE_HAS_LENGTH_MESSAGE);
        Assert.hasLength(whereClause, WHERE_CLAUSE_HAS_LENGTH_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(String.format(
                "updateFeatureDataByRestriction() --> Starting update feature data by restriction: assetType=[%s], whereClause=[%s]",
                assetType, whereClause));
        }

        /*
         * Create the where clause.
         */
        final String featureWhereClause = whereClause + ArcgisExtensionsConstantsCommon.SQL_AND
                + ArcgisExtensionsConstantsTableFieldNames.FIELD_GEO_OBJECTID
                + ArcgisExtensionsConstantsCommon.SQL_IS_NOT_NULL;

        /*
         * Create the feature layer.
         */
        final ArcgisFeatureLayerManager arcgisFeatureLayerManager = new ArcgisFeatureLayerManager();
        final ArcgisFeatureLayer featureLayer =
                arcgisFeatureLayerManager.createFeatureLayer(assetType, featureWhereClause);

        /*
         * Update the feature data.
         */
        this.updateFeatureData(assetType, featureLayer, whereClause);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(String.format(
                "updateFeatureDataByRestriction() --> Completed update feature data by restriction: assetType=[%s], whereClause=[%s].",
                assetType, whereClause));
        }

    }

    /**
     *
     * Update the feature data.
     *
     * This is called as the last step in the Smart Client Extensions ArcGIS publishing process.
     * This is also called as the last step from many of the asset connectors and/or via one of the
     * ArcgisExtensionsService#updateFeatureData methods.
     *
     * @param assetType The ARCHIBUS asset type to update.
     * @param featureLayer The ArcGIS feature layer for the asset.
     * @param whereClause The whereClause for the ARCGIS feature layer.
     */
    private void updateFeatureData(final String assetType, final ArcgisFeatureLayer featureLayer,
            final String whereClause) {

        Assert.hasLength(assetType, ASSET_TYPE_HAS_LENGTH_MESSAGE);
        Assert.notNull(featureLayer, FEATURE_LAYER_NOT_NULL_MESSAGE);
        Assert.hasLength(whereClause, WHERE_CLAUSE_HAS_LENGTH_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(String.format(
                "updateFeatureData --> Starting update feature data: assetType=[%s], whereClause=[%s].",
                assetType, whereClause));
        }
        /*
         * Create the import/export filenames.
         */
        final String geoJsonPath = ArchibusProjectUtilities.getGeoJsonPath();
        final String importFilename = geoJsonPath + assetType
                + ArcgisExtensionsConstantsCommon.FILENAME_IMPORT_ARCGIS_JSON;
        final String exportFilename = geoJsonPath + assetType
                + ArcgisExtensionsConstantsCommon.FILENAME_EXPORT_ARCGIS_JSON;

        Assert.hasText(importFilename, "importFilename must be specified!");
        Assert.hasText(exportFilename, "exportFilename must be specified!");

        if (this.logger.isInfoEnabled()) {
            this.logger
                .info(String.format("updateFeatureData() -> importFilename=[%s]", importFilename));
            this.logger
                .info(String.format("updateFeatureData() -> exportFilename=[%s]", exportFilename));
        }

        /*
         * Query the feature layer for the object ids and asset ids.
         */
        final ArcgisFeatureServiceManager arcgisFeatureServiceManager =
                new ArcgisFeatureServiceManager();
        final JSONObject importData = arcgisFeatureServiceManager
            .queryFeatureServiceForObjectIds(featureLayer, whereClause);

        /*
         * Save the asset data for the import connector.
         */
        JsonReadWriteUtilities.writeFile(importData, importFilename);

        /*
         * Call the import connector to import the asset data.
         */
        final String importConnectorId = featureLayer.getImportConnectorId();
        ArcgisConnectorManager.runJsonImportConnector(importConnectorId, importFilename);

        /*
         * Call the export connector to export the asset data.
         */
        final String exportConnectorId = featureLayer.getExportConnectorId();
        final String exportWhereClause = whereClause + ArcgisExtensionsConstantsCommon.SQL_AND
                + ArcgisExtensionsConstantsTableFieldNames.FIELD_GEO_OBJECTID
                + ArcgisExtensionsConstantsCommon.SQL_IS_NOT_NULL;
        ArcgisConnectorManager.runJsonExportConnector(exportConnectorId, exportFilename,
            exportWhereClause);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(String.format(
                "updateFeatureData --> Completed update feature data: assetType=[%s], whereClause=[%s].",
                assetType, whereClause));
        }

        // The export connector post process will call
        // ArcgisExtensionsService#updateArcgisFeatureDataFromConnector method
        // to post the changes to the ArcGIS feature service.
    }
}
