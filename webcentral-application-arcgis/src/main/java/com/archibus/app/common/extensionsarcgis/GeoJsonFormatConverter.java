package com.archibus.app.common.extensionsarcgis;

import java.util.List;

import org.apache.log4j.Logger;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.json.*;
import org.springframework.util.Assert;

import com.vividsolutions.jts.geom.*;

/**
 *
 * Provides methods to convert GeoJSON to EsriJSON format.
 *
 * Used by the Extensions for Esri.
 *
 * @author knight
 * @since 23.1
 *
 */
public class GeoJsonFormatConverter {

    /**
     * Constant: assert asset type has length message.
     */
    private static final String ASSET_TYPE_HAS_LENGTH_MESSAGE = "assetType must be specified!";

    /**
     * Constant: assert attributes not null message.
     */
    private static final String ATTRIBUTES_NOT_NULL_MESSAGE = "attributes must be specified!";

    /**
     * Constant: assert crsObj not null message.
     */
    private static final String CRSOBJ_NOT_NULL_MESSAGE = "crsObj must be specified!";

    /**
     * Constant: assert esri features not null message.
     */
    private static final String ESRI_FEATURES_NOT_NULL_MESSAGE = "esriFeatures must be specified!";

    /**
     * Constant: assert geoJSON not null message.
     */
    private static final String GEOJSON_NOT_NULL_MESSAGE = "geoJson must be specified!";

    /**
     * Constant: assert geoJSON features not null message.
     */
    private static final String GEOJSON_FEATURES_NOT_NULL_MESSAGE =
            "geoJsonFeatures must be specified!";

    /**
     * Constant: assert geoJSON points not empty message.
     */
    private static final String GEOJSON_POINTS_NOT_EMPTY_MESSAGE =
            "geoJsonPoints must be specified!";

    /**
     * Constant: assert geometry not null message.
     */
    private static final String GEOMETRY_NOT_NULL_MESSAGE = "geometry must be spoecified!";

    /**
     * Constant: assert pubConfig not null message.
     */
    private static final String PUBCONFIG_NOT_NULL_MESSAGE = "pubConfig must be specified!";

    /**
     * Logger to log messages to archibus log.
     */
    protected final Logger logger = Logger.getLogger(this.getClass());

    /**
     *
     * Convert GeoJSON to EsriJSON format. Used by the Extensions for Esri.
     *
     * @param geoJson The GeoJSON Feature Collection.
     * @param arcgisPublishingConfiguration The publishing configuration.
     * @return The Esri JSON.
     */
    public JSONObject convertToEsriJson(final JSONObject geoJson,
            final ArcgisPublishingConfiguration arcgisPublishingConfiguration) {

        Assert.notNull(geoJson, GEOJSON_NOT_NULL_MESSAGE);
        Assert.notNull(arcgisPublishingConfiguration, PUBCONFIG_NOT_NULL_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info("convertToEsriJson() -> Starting GeoJSON to esriJSON conversion.");
        }

        /*
         * Get features from collection.
         */
        final JSONArray geoJsonFeatures =
                geoJson.getJSONArray(ArcgisExtensionsConstantsTableFieldNames.FIELD_FEATURES);

        /*
         * Create the Esri feature list.
         */
        final JSONArray esriFeatures = new JSONArray();

        /*
         * Get the JSON and crs objects from the geoJson.
         */
        final JSONObject jsonProperties =
                geoJson.getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES);
        final JSONObject crsObj =
                geoJson.getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_CRS);

        /*
         * Get the assets to publish from the publishing configuration.
         */
        final String[] publishAssetList = arcgisPublishingConfiguration.getPublishAssetTypes();

        /*
         * Convert each asset in the publish asset list.
         */
        for (final String assetType : publishAssetList) {
            /*
             * Convert the features.
             */
            convertFeatures(esriFeatures, geoJsonFeatures, crsObj, assetType);
        }

        /*
         * Convert the background graphics.
         */

        if (arcgisPublishingConfiguration.getPublishBackground()) {
            /*
             * Convert the background features.
             */
            convertFeatures(esriFeatures, geoJsonFeatures, crsObj,
                ArcgisExtensionsConstantsJsonNames.JSON_BACKGROUND);
        }

        /*
         * Create the Esri JSON feature collection.
         */
        final JSONObject esriJson = new JSONObject();
        esriJson.put(ArcgisExtensionsConstantsJsonNames.JSON_TYPE,
            ArcgisExtensionsConstantsCommon.FEATURECOLLECTION);
        esriJson.put(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES, jsonProperties);
        esriJson.put(ArcgisExtensionsConstantsTableFieldNames.FIELD_FEATURES, esriFeatures);

        if (this.logger.isInfoEnabled()) {
            this.logger.info("convertToEsriJson() -> Completed geoJSON to esriJSON conversion.");
        }

        return esriJson;
    }

    /**
     *
     * Convert GeoJSON to EsriJSON format.
     *
     * @param esriFeatures The converted EsriJSON features.
     * @param geoJsonFeatures The GeoJson features to convert.
     * @param crsObj The coordinate reference system object.
     * @param assetType The ARCHIBUS asset type to convert.
     *
     */
    private void convertFeatures(final JSONArray esriFeatures, final JSONArray geoJsonFeatures,
            final JSONObject crsObj, final String assetType) {

        Assert.notNull(esriFeatures, ESRI_FEATURES_NOT_NULL_MESSAGE);
        Assert.notNull(geoJsonFeatures, GEOJSON_FEATURES_NOT_NULL_MESSAGE);
        Assert.notNull(crsObj, CRSOBJ_NOT_NULL_MESSAGE);
        Assert.hasLength(assetType, ASSET_TYPE_HAS_LENGTH_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(String.format(
                "convertFeatures() -> Starting geoJson to esriJson conversion for assetType=[%s]",
                assetType));
        }

        /*
         * Get the GeoJson features.
         */
        final JSONArray filteredFeatures =
                GeoJsonFeatureUtilities.filterFeatures(geoJsonFeatures, assetType);

        if (filteredFeatures.length() > 0) {
            geoJsonToEsriJson(esriFeatures, filteredFeatures, crsObj, assetType);
            if (this.logger.isInfoEnabled()) {
                this.logger.info(String.format(
                    "convertFeatures() -> Completed GeoJson to esriJson conversion for assetType=[%s]",
                    assetType));
            }
        } else {
            if (this.logger.isInfoEnabled()) {
                this.logger.info(String.format(
                    "convertFeatures() -> No GeoJSON assets were found to convert to esriJSON for assetType=[%s]",
                    assetType));
            }
        }
    }

    /**
     *
     * Convert GeoJSON to EsriJson.
     *
     * @param esriFeatures The converted EsriJSON features.
     * @param geoJsonFeatures The GeoJSON features to convert.
     * @param crsObj The coordinate reference system object.
     * @param assetType The ARCHIBUS asset type.
     */
    private void geoJsonToEsriJson(final JSONArray esriFeatures, final JSONArray geoJsonFeatures,
            final JSONObject crsObj, final String assetType) {

        Assert.notNull(esriFeatures, ESRI_FEATURES_NOT_NULL_MESSAGE);
        Assert.notNull(geoJsonFeatures, GEOJSON_FEATURES_NOT_NULL_MESSAGE);
        Assert.notNull(crsObj, CRSOBJ_NOT_NULL_MESSAGE);
        Assert.hasLength(assetType, ASSET_TYPE_HAS_LENGTH_MESSAGE);

        // TODO this should be based on geometry type, and not on asset type
        if (assetType.equalsIgnoreCase(ArcgisExtensionsConstantsCommon.ASSET_TYPE_RM)) {
            this.geoJsonPolygonFeaturesToEsriJson(esriFeatures, geoJsonFeatures, crsObj);
        } else if (assetType.equalsIgnoreCase(ArcgisExtensionsConstantsCommon.ASSET_TYPE_GROSS)) {
            this.geoJsonPolygonFeaturesToEsriJson(esriFeatures, geoJsonFeatures, crsObj);
        } else if (assetType.equalsIgnoreCase(ArcgisExtensionsConstantsCommon.ASSET_TYPE_EQ)) {
            this.geoJsonPointFeaturesToEsriJson(esriFeatures, geoJsonFeatures, crsObj);
        } else if (assetType
            .equalsIgnoreCase(ArcgisExtensionsConstantsCommon.ASSET_TYPE_BACKGROUND)) {
            this.geoJsonLineFeaturesToEsriJson(esriFeatures, geoJsonFeatures, crsObj);
        } else {
            this.logger.error(String.format(
                "geoJsonToEsriJson() -> Failed converting GeoJSON to esriJSON. Asset type not found. assetType=[%s]",
                assetType));
        }
    }

    /**
     *
     * Convert GeoJSON polygon features to EsriJSON format.
     *
     * @param esriFeatures The converted EsriJSON features.
     * @param geoJsonFeatures The GeoJSON features to convert.
     * @param crsObj The coordinate reference system object.
     */
    private void geoJsonPolygonFeaturesToEsriJson(final JSONArray esriFeatures,
            final JSONArray geoJsonFeatures, final JSONObject crsObj) {

        Assert.notNull(esriFeatures, ESRI_FEATURES_NOT_NULL_MESSAGE);
        Assert.notNull(geoJsonFeatures, GEOJSON_FEATURES_NOT_NULL_MESSAGE);
        Assert.notNull(crsObj, CRSOBJ_NOT_NULL_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(
                "geoJsonPolygonFeaturesToEsriJson() -> Starting polygon conversion from geoJSON to esriJSON.");
        }

        /*
         * Construct wkidObj object.
         */
        final JSONObject wkidObj = createWkidObjFromCrsObj(crsObj);

        /*
         * Loop over feature list
         */
        for (int i = 0; i < geoJsonFeatures.length(); i++) {

            /*
             * Get the feature.
             */
            final JSONObject feature = geoJsonFeatures.getJSONObject(i);

            /*
             * Prepare the geometry object.
             */
            final JSONArray rings =
                    feature.getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_GEOMETRY)
                        .getJSONArray(ArcgisExtensionsConstantsJsonNames.JSON_COORDINATES);
            final JSONObject geometry = new JSONObject();
            geometry.put(ArcgisExtensionsConstantsJsonNames.JSON_RINGS, rings);
            geometry.put(ArcgisExtensionsConstantsJsonNames.JSON_SPATIALREFERENCE, wkidObj);

            /*
             * Prepare the attribute object.
             */
            // TODO add common fields, asset specific fields, and configurable
            // fields
            final JSONObject featureProperties =
                    feature.getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES);
            final JSONObject attributes = featureProperties;

            /*
             * Create the Esri feature.
             */
            final JSONObject esriFeature = createEsriFeature(geometry, attributes);

            /*
             * Add the feature to the Esri feature array.
             */
            esriFeatures.put(esriFeature);
        }

        if (this.logger.isInfoEnabled()) {
            this.logger.info(
                "geoJsonPolygonFeaturesToEsriJson() -> Completed polygon conversion from geoJSON to esriJSON.");
        }
    }

    /**
     *
     * Convert GeoJSON point features to EsriJSON format.
     *
     * @param esriFeatures The converted EsriJSON features.
     * @param geoJsonFeatures The GeoJSON features to convert.
     * @param crsObj The coordinate reference system object.
     */
    private void geoJsonPointFeaturesToEsriJson(final JSONArray esriFeatures,
            final JSONArray geoJsonFeatures, final JSONObject crsObj) {

        Assert.notNull(esriFeatures, ESRI_FEATURES_NOT_NULL_MESSAGE);
        Assert.notNull(geoJsonFeatures, GEOJSON_FEATURES_NOT_NULL_MESSAGE);
        Assert.notNull(crsObj, CRSOBJ_NOT_NULL_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(
                "geoJsonPointFeaturesToEsriJson() -> Starting conversion of points from geoJSON to esriJSON.");
        }
        /*
         * Construct wkidObj object.
         */
        final JSONObject wkidObj = createWkidObjFromCrsObj(crsObj);

        /*
         * Loop over feature list.
         */
        for (int i = 0; i < geoJsonFeatures.length(); i++) {

            /*
             * Get the feature.
             */
            final JSONObject feature = geoJsonFeatures.getJSONObject(i);

            /*
             * Prepare geometry object.
             */
            final JSONObject featureGeometry =
                    feature.getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_GEOMETRY);
            final GeoJsonPoint coordinate = (GeoJsonPoint) featureGeometry
                .get(ArcgisExtensionsConstantsJsonNames.JSON_COORDINATES);

            final double xCoord = coordinate.x;
            final double yCoord = coordinate.y;
            final JSONObject geometry = new JSONObject();
            geometry.put(ArcgisExtensionsConstantsJsonNames.JSON_X, xCoord);
            geometry.put(ArcgisExtensionsConstantsJsonNames.JSON_Y, yCoord);
            geometry.put(ArcgisExtensionsConstantsJsonNames.JSON_SPATIALREFERENCE, wkidObj);

            /*
             * Prepare attribute object.
             */
            // TODO add common fields, asset specific fields, and configurable
            // fields
            final JSONObject featureProperties =
                    feature.getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES);
            final JSONObject attributes = featureProperties;

            /*
             * Create the Esri feature.
             */
            final JSONObject esriFeature = createEsriFeature(geometry, attributes);

            /*
             * Add to feature list.
             */
            esriFeatures.put(esriFeature);
        }

        if (this.logger.isInfoEnabled()) {
            this.logger.info(
                "geoJsonPointFeaturesToEsriJson() -> Completed conversion of points from geoJSON to esriJSON.");
        }
    }

    /**
     *
     * Convert GeoJSON line features to EsriJSON format.
     *
     * @param esriFeatures The converted EsriJSON features.
     * @param geoJsonFeatures The GeoJSON features to convert.
     * @param crsObj The coordinate reference system object.
     */
    private void geoJsonLineFeaturesToEsriJson(final JSONArray esriFeatures,
            final JSONArray geoJsonFeatures, final JSONObject crsObj) {

        Assert.notNull(esriFeatures, ESRI_FEATURES_NOT_NULL_MESSAGE);
        Assert.notNull(geoJsonFeatures, GEOJSON_FEATURES_NOT_NULL_MESSAGE);
        Assert.notNull(crsObj, CRSOBJ_NOT_NULL_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(
                "geoJsonLineFeaturesToEsriJson() -> Starting conversion of lines from geoJSON to esriJSON.");
        }

        /*
         * Construct wkidObj object.
         */
        final JSONObject wkidObj = createWkidObjFromCrsObj(crsObj);

        /*
         * Loop over feature list
         */
        for (int i = 0; i < geoJsonFeatures.length(); i++) {

            /*
             * Get the feature.
             */
            final JSONObject feature = geoJsonFeatures.getJSONObject(i);

            /*
             * Prepare the geometry object.
             */
            final List<GeoJsonPoint> coordinates = (List<GeoJsonPoint>) feature
                .getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_GEOMETRY)
                .get(ArcgisExtensionsConstantsJsonNames.JSON_COORDINATES);

            // If the coordinates are valid, create the esri feature.
            if (checkLineCoordinates(coordinates)) {

                final JSONArray paths = new JSONArray();
                paths.put(coordinates);
                final JSONObject geometry = new JSONObject();
                geometry.put(ArcgisExtensionsConstantsJsonNames.JSON_PATHS, paths);
                geometry.put(ArcgisExtensionsConstantsJsonNames.JSON_SPATIALREFERENCE, wkidObj);

                /*
                 * Prepare the attribute object.
                 */
                final JSONObject featureProperties =
                        feature.getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES);
                final JSONObject attributes = featureProperties;

                /*
                 * Create the Esri feature.
                 */
                final JSONObject esriFeature = createEsriFeature(geometry, attributes);

                /*
                 * Add the Esri feature to Esri feature list.
                 */
                esriFeatures.put(esriFeature);
            }
        }

        if (this.logger.isInfoEnabled()) {
            this.logger.info(
                "geoJsonLineFeaturesToEsriJson() -> Completed conversion of lines from geoJSON to esriJSON.");
        }

    }

    /**
     *
     * Create a WKID object from a CRS object.
     *
     * @param crsObj The CRS object.
     * @return The WKID object.
     */
    private static JSONObject createWkidObjFromCrsObj(final JSONObject crsObj) {

        Assert.notNull(crsObj, CRSOBJ_NOT_NULL_MESSAGE);

        /*
         * Get the epsg string.
         */
        final String[] epsg =
                crsObj.getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES)
                    .getString(ArcgisExtensionsConstantsJsonNames.JSON_NAME)
                    .split(ArcgisExtensionsConstantsCommon.COLON);
        final int wkid = Integer.parseInt(epsg[1]);
        /*
         * Create the WKID object.
         */
        final JSONObject wkidObj = new JSONObject();
        wkidObj.put(ArcgisExtensionsConstantsJsonNames.JSON_WKID, wkid);

        return wkidObj;
    }

    /**
     *
     * Create an Esri feature from geometry and attributes.
     *
     * @param geometry The Esri feature geometry.
     * @param attributes The Esri feature attributes.
     * @return The Esri feature.
     */
    private static JSONObject createEsriFeature(final JSONObject geometry,
            final JSONObject attributes) {

        Assert.notNull(geometry, GEOMETRY_NOT_NULL_MESSAGE);
        Assert.notNull(attributes, ATTRIBUTES_NOT_NULL_MESSAGE);

        final JSONObject esriFeature = new JSONObject();
        esriFeature.put(ArcgisExtensionsConstantsJsonNames.JSON_GEOMETRY, geometry);
        esriFeature.put(ArcgisExtensionsConstantsJsonNames.JSON_ATTRIBUTES, attributes);
        return esriFeature;
    }

    /**
     *
     * Check line coordinates for valid geometry (length > 0.01).
     *
     * @param geoJsonPoints The line coordinates to check.
     * @return True if the coordinates are valid, false if the coordinates are invalid.
     */
    private static Boolean checkLineCoordinates(final List<GeoJsonPoint> geoJsonPoints) {

        Assert.notEmpty(geoJsonPoints, GEOJSON_POINTS_NOT_EMPTY_MESSAGE);

        Boolean coordinatesValid = false;

        final GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory(null);
        final GeoJsonPoint geoPoint1 = geoJsonPoints.get(0);
        final GeoJsonPoint geoPoint2 = geoJsonPoints.get(1);
        final Point point1 = geometryFactory.createPoint(new Coordinate(geoPoint1.x, geoPoint2.y));
        final Point point2 = geometryFactory.createPoint(new Coordinate(geoPoint2.x, geoPoint2.y));

        final Coordinate[] coordinates =
                new Coordinate[] { point1.getCoordinate(), point2.getCoordinate() };

        final LineString lineString = geometryFactory.createLineString(coordinates);
        final Double segmentLength = lineString.getLength();

        if (segmentLength > ArcgisExtensionsConstantsCommon.WEB_MERCATOR_LINE_SEGMENT_MIN_LENGTH) {
            coordinatesValid = true;
        }

        return coordinatesValid;
    }

}