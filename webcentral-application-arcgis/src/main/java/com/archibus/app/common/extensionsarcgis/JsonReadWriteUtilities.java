package com.archibus.app.common.extensionsarcgis;

import java.io.*;
import java.text.ParseException;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.springframework.util.Assert;

import com.archibus.utility.ExceptionBase;

/**
 *
 * Utilities to read and write JSON files for the ArcgisExtensions.
 *
 * @author knight
 *
 */
public final class JsonReadWriteUtilities {

	/**
	 * Constant: assert filename has length message.
	 */
	private static final String FILENAME_HAS_LENGTH_ERROR = "filename must be specified.";

	/**
	 * Constant: assert jsonContent not null message.
	 */
	private static final String JSONCONTENT_NOT_NULL_ERROR = "jsonContent must be specified.";

	/**
	 * Private default constructor: utility class is non-instantiable.
	 *
	 * @throws InstantiationException
	 *             always, since this constructor should never be called.
	 */
	private JsonReadWriteUtilities() throws InstantiationException {
		throw new InstantiationException("Never instantiate " + this.getClass().getName() + "; use static methods!");
	}

	/**
	 *
	 * Reads JSON object from file.
	 *
	 * @param filePath
	 *            path of the JSON file to read.
	 * @return Object with the contents of the JSON file.
	 * @throws ExceptionBase
	 *             If fails to read or parse the file.
	 */
	public static JSONObject readFile(final String filePath) throws ExceptionBase {
		Assert.hasLength(filePath, FILENAME_HAS_LENGTH_ERROR);

		String fileContents = null;

		try {
			final File jsonFile = new File(filePath);
			fileContents = FileUtils.readFileToString(jsonFile);
		} catch (final IOException exception) {
			// @non-translatable
			throw new ExceptionBase(String.format("Failed to read JSON file: [%s]", filePath), exception);
		}

		JSONObject result = null;
		try {
			result = new JSONObject(fileContents);
		} catch (final ParseException exception) {
			// @non-translatable
			throw new ExceptionBase(String.format("Failed to parse JSON file: [%s]", filePath), exception);
		}

		return result;
	}

	/**
	 *
	 * Writes JSON object to file.
	 *
	 * @param jsonObject
	 *            Object to be written.
	 * @param filePath
	 *            Path of the JSON file to write.
	 */
	public static void writeFile(final JSONObject jsonObject, final String filePath) {
		Assert.hasLength(filePath, FILENAME_HAS_LENGTH_ERROR);
		Assert.notNull(jsonObject, JSONCONTENT_NOT_NULL_ERROR);

		try {
			final File file = new File(filePath);
			FileUtils.writeStringToFile(file, jsonObject.toString());
		} catch (final IOException exception) {
			// @non-translatable
			throw new ExceptionBase(String.format("Failed to write JSON file: [%s]", filePath), exception);
		}
	}
}
