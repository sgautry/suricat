package com.archibus.app.common.extensionsarcgis;

import java.awt.geom.Point2D.Double;
import java.text.*;
import java.util.Locale;

import org.springframework.util.Assert;

/**
 *
 * Defines a point specified in double precision.
 *
 * @author knight
 *
 */
public class GeoJsonPoint extends Double {

	/**
	 * Constant : assert coordinate not null message.
	 */
	private static final String COORDINATE_NOT_NULL_ERROR = "X and Y coordinate values are required to construct a point!";

	/**
	 * Constant: The serial version ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constant : The maximum number of fractional digits (to suppress
	 * scientific notation).
	 */
	private static final int INT_THREE_HUNDRED_FORTY = 340;

	/**
	 *
	 * Construct an empty GeoJsonPoint.
	 *
	 */
	public GeoJsonPoint() {
		super();
	}

	/**
	 *
	 * Constructs and initializes a GeoJsonPoint with specified coordinates.
	 *
	 * @param arg0
	 *            The X coordinate of the point.
	 * @param arg1
	 *            The Y coordinate of the point.
	 */
	public GeoJsonPoint(final double arg0, final double arg1) {
		super(arg0, arg1);

		Assert.notNull(arg0, COORDINATE_NOT_NULL_ERROR);
		Assert.notNull(arg1, COORDINATE_NOT_NULL_ERROR);
	}

	/**
	 * Convert this GeoJsonPoint to a String.
	 *
	 * @return A String representation of this GeoJsonPoint.
	 */
	@Override
	public String toString() {
		final DecimalFormat decimalFormatter = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
		decimalFormatter.setMaximumFractionDigits(INT_THREE_HUNDRED_FORTY);
		return "[" + decimalFormatter.format(this.x) + ", " + decimalFormatter.format(this.y) + "]";
	}
}
