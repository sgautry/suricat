package com.archibus.app.common.extensionsarcgis;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.*;
import org.springframework.util.Assert;

import com.archibus.app.common.connectors.domain.ConnectorConfig;
import com.archibus.context.ContextStore;
import com.archibus.utility.ExceptionBase;

/**
 *
 * Provides methods for querying and updating with ArcGIS feature services.
 *
 * @author knight
 *
 */
public class ArcgisFeatureServiceManager {

    /**
     * Constant: assert arcigs feature layer not null message.
     */
    private static final String ARCGIS_FEATURE_LAYER_NOT_NULL_MESSAGE =
            "arcgisFeatureLayer must be specifed!";

    /**
     * Constant: assert asset type has length message.
     */
    private static final String ASSET_TYPE_HAS_LENGTH_MESSAGE = "assetType must be specified!";

    /**
     * Constant: assert connector config not null message.
     */
    private static final String CONNECTOR_CONFIG_HAS_LENGTH_MESSAGE =
            "connectorConfig must be specified!";

    /**
     * Constant: assert delete result not null message.
     */
    private static final String DELETE_RESULT_NOT_NULL_MESSAGE = "deleteResult must be specified!";

    /**
     * Constant: assert feature collection not empty message.
     */
    private static final String FEATURE_COLLECTION_NOT_NULL_MESSAGE =
            "featureCollection must be specified!";

    /**
     * Constant: assert feature url has length message.
     */
    private static final String FEATURE_URL_HAS_LENGTH_MESSAGE = "featureUrl must be specified!";

    /**
     * Constant: assert features not null message.
     */
    private static final String FEATURES_NOT_NULL_MESSAGE = "features must be specified!";

    /**
     * Constant: assert url parameters has length message.
     */
    private static final String URL_PARAMETERS_HAS_LENGTH_MESSAGE =
            "urlParameters must be specified!";

    /**
     * Constant: asset where clause has length message.
     */
    private static final String WHERE_CLAUSE_HAS_LENGTH_MESSAGE = "whereClause must be specified!";

    /**
     * Logger to log messages to archibus log.
     */
    protected final Logger logger = Logger.getLogger(this.getClass());

    /**
     *
     * Add features to an ArcGIS feature layer.
     *
     * @param featureCollection An Esri feature collection containing the features to add.
     * @param assetType The ARCHIBUS asset type.
     */
    public void addFeatures(final JSONObject featureCollection, final String assetType) {

        Assert.notNull(featureCollection, FEATURE_COLLECTION_NOT_NULL_MESSAGE);
        Assert.hasLength(assetType, ASSET_TYPE_HAS_LENGTH_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(
                String.format("addFeatures() -> Started add features: assetType=[%s]", assetType));
        }

        /*
         * Get the ARCHIBUS dwgname.
         */
        final String dwgname =
                featureCollection.getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES)
                    .getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_DWGINFO)
                    .getString(ArcgisExtensionsConstantsCommon.DWGNAME);

        /*
         * Get the Esri features from the JSON Object.
         */
        final JSONArray features = featureCollection
            .getJSONArray(ArcgisExtensionsConstantsTableFieldNames.FIELD_FEATURES);

        /*
         * Filter the Esri features for asset type.
         */
        final JSONArray esriFeatures = ArcgisJsonUtilities.filterEsriFeatures(features, assetType);

        final ArcgisFeatureLayerManager arcgisFeatureLayerManager = new ArcgisFeatureLayerManager();
        final ArcgisFeatureLayer featureLayer = arcgisFeatureLayerManager
            .createFeatureLayer(assetType, ArcgisExtensionsConstantsCommon.SQL_ONE_EQUALS_ONE);
        final String featureLayerUrl = featureLayer.getUrl();
        final String assetTypeField = featureLayer.getAssetTypeField();

        /*
         * First, delete the existing features in the feature layer for the drawing.
         */
        final String deleteParams = ArcgisQueryStringUtilities
            .constructDeleteParamsByDrawing(dwgname, assetType, assetTypeField);
        final String deleteUrl = ArcgisQueryStringUtilities.constructDeleteUrl(featureLayerUrl);
        final JSONObject deleteResult = postToArcgisServer(deleteUrl, deleteParams);

        /*
         * Next, add new features to the feature layer for the drawing.
         */
        final String addParams = ArcgisQueryStringUtilities.constructAddParams(esriFeatures);
        final String addUrl = ArcgisQueryStringUtilities.constructAddUrl(featureLayerUrl);

        if (deleteResultSuccess(deleteResult)) {
            postToArcgisServer(addUrl, addParams);
        } else {
            // @ non-translatable
            throw new ExceptionBase(
                String.format("Delete features failed. New features not added. result=[%s]",
                    deleteResult.toString()));
        }

        if (this.logger.isInfoEnabled()) {
            this.logger.info(String.format("addFeatures() -> featureUrl=[%s]", featureLayerUrl));
            this.logger.info(String
                .format("addFeatures() -> Completed add features: assetType=[%s]", assetType));
        }
    }

    /**
     *
     * Query an ArcGIS feature service for OBJECTIDs.
     *
     * @param arcgisFeatureLayer the ArcGIS feature layer.
     * @param whereClause the where clause to use in the feature layer query.
     * @return the OBJECTIDs for the feature layer.
     */
    public JSONObject queryFeatureServiceForObjectIds(final ArcgisFeatureLayer arcgisFeatureLayer,
            final String whereClause) {

        Assert.notNull(arcgisFeatureLayer, ARCGIS_FEATURE_LAYER_NOT_NULL_MESSAGE);
        Assert.hasLength(whereClause, WHERE_CLAUSE_HAS_LENGTH_MESSAGE);

        final String idField = arcgisFeatureLayer.getIdField();
        final String objectIdField = arcgisFeatureLayer.getObjectIdField();
        final String featureLayerUrl = arcgisFeatureLayer.getUrl();

        final String queryUrl = ArcgisQueryStringUtilities.constructQueryUrl(featureLayerUrl);
        final String queryParams = ArcgisQueryStringUtilities
            .constructQueryParamsForObjectIds(whereClause, idField, objectIdField);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(
                String.format("queryFeatureLayerForObjectIds() -> queryUrl=[%s], queryParams=[%s].",
                    queryUrl, queryParams));
        }

        final JSONObject queryResult = postToArcgisServer(queryUrl, queryParams);

        return queryResult;
    }

    /**
     *
     * Update fields in the ArcGIS feature layer with values from fields in the ARCHIBUS asset
     * table. The ArcGIS Export connector runs this method as a post process.
     *
     * @param connectorConfig The connector configuration.
     *
     */
    public void updateArcgisFeatureDataFromConnector(final ConnectorConfig connectorConfig) {

        Assert.notNull(connectorConfig, CONNECTOR_CONFIG_HAS_LENGTH_MESSAGE);

        /*
         * Get the json file.
         */
        final String filename = connectorConfig.getConnStringDb();

        /*
         * Read the feature data from file.
         */
        final JSONObject features = JsonReadWriteUtilities.readFile(filename);

        /*
         * Get the asset type.
         */
        final String assetType = connectorConfig.getDestinationTbl();

        /*
         * Get the feature layer URL.
         */
        final ArcgisFeatureLayerManager arcgisFeatureLayerManager = new ArcgisFeatureLayerManager();
        final ArcgisFeatureLayer featureLayer = arcgisFeatureLayerManager
            .createFeatureLayer(assetType, ArcgisExtensionsConstantsCommon.SQL_ONE_EQUALS_ONE);
        final String featureUrl = featureLayer.getUrl();

        ArcgisExtensionsService.checkExtensionsForEsriLicense();

        this.updateFeatureData(featureUrl, features);
    }

    /**
     *
     * Update the ARCHIBUS geo_objectid field for the asset with OBJECTIDs from the ArcGIS feature
     * layer.
     *
     * @param connectorConfig The connector configuration.
     */
    public void updateGeoObjectIdsFromConnector(final ConnectorConfig connectorConfig) {

        Assert.notNull(connectorConfig, CONNECTOR_CONFIG_HAS_LENGTH_MESSAGE);

        /*
         * Check for Extensions for Esri license.
         */
        ArcgisExtensionsService.checkExtensionsForEsriLicense();

        /*
         * Get the asset type.
         */
        final String assetType = connectorConfig.getDestinationTbl();

        /*
         * Get the restriction.
         */
        final String whereClause = connectorConfig.getClause();

        /*
         * Get the feature layer URL.
         */
        final ArcgisFeatureLayerManager arcgisFeatureLayerManager = new ArcgisFeatureLayerManager();
        final ArcgisFeatureLayer featureLayer =
                arcgisFeatureLayerManager.createFeatureLayer(assetType, whereClause);

        /*
         * Query the feature layer for object ids.
         */
        final JSONObject featureData =
                this.queryFeatureServiceForObjectIds(featureLayer, whereClause);

        /*
         * Create the import filename.
         */
        final String geoJsonPath = ArchibusProjectUtilities.getGeoJsonPath();
        final String importFilename = geoJsonPath + assetType
                + ArcgisExtensionsConstantsCommon.FILENAME_IMPORT_ARCGIS_JSON;

        /*
         * Save the asset data for the import connector.
         */
        JsonReadWriteUtilities.writeFile(featureData, importFilename);

        /*
         * Call the import connector to import the asset data.
         */
        final String importConnectorId = featureLayer.getImportConnectorId();
        ArcgisConnectorManager.runJsonImportConnector(importConnectorId, importFilename);

    }

    /**
     *
     * Update ArcGIS feature data with ARCHIBUS data generated from Connector.
     *
     * @param featureUrl the ArcGIS feature service URL.
     * @param features the ArcGIS features to update.
     */
    private void updateFeatureData(final String featureUrl, final JSONObject features) {

        Assert.hasLength(featureUrl, FEATURE_URL_HAS_LENGTH_MESSAGE);
        Assert.notNull(features, FEATURES_NOT_NULL_MESSAGE);

        /*
         * Get the features.
         */
        final JSONArray featureData =
                features.getJSONArray(ArcgisExtensionsConstantsTableFieldNames.FIELD_FEATURES);
        /*
         * Create the update parameter URL.
         */
        final String updateParams = ArcgisExtensionsConstantsTableFieldNames.FIELD_FEATURES
                + ArcgisExtensionsConstantsCommon.EQUALS + featureData.toString()
                + ArcgisExtensionsConstantsCommon.ARCGIS_URL_ADD_UPDATE_FEATURES_PARAM;
        final String updateUrl =
                featureUrl + ArcgisExtensionsConstantsCommon.ARCGIS_URL_UPDATE_FEATURES;

        if (this.logger.isInfoEnabled()) {
            this.logger
                .info(String.format("updateFeatureData() -> updateUrl=[%s], updateParams=[%s]",
                    updateUrl, updateParams));
        }
        /*
         * Post the update to ArcGIS.
         */
        final JSONObject result = this.postToArcgisServer(updateUrl, updateParams);
        // If success, the result objects is an array of feature ids.
        // If fail, the result object contains error information.
        if (result != null && this.logger.isInfoEnabled()) {
            this.logger
                .info(String.format("updateFeatureData() -> result=[%s]", result.toString()));
        }

    }

    /**
     * Post to ArcGIS Server.
     *
     * @param featureUrl The url for the ArcGIS Server service.
     * @param urlParameters The url parameters for the ArcGIS Server service.
     * @return The result from the ArcGIS Server service.
     */
    private JSONObject postToArcgisServer(final String featureUrl, final String urlParameters) {

        /*
         * Assert url and parameters.
         */
        Assert.hasLength(featureUrl, FEATURE_URL_HAS_LENGTH_MESSAGE);
        Assert.hasLength(urlParameters, URL_PARAMETERS_HAS_LENGTH_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger
                .info(String.format("postToArcgisServer() -> featureUrl=[%s], urlParameters=[%s].",
                    featureUrl, urlParameters));
        }

        /*
         * Check to see if we need a token.
         */
        String accessToken = null;
        String arcgisParameters = null;

        final String authenticationMethod =
                ContextStore.get().getProject().getActivityParameterManager()
                    .getParameterValue(ArcgisExtensionsConstantsCommon.ABCOMMONRESOURCES
                            + ArcgisExtensionsConstantsCommon.DASH
                            + ArcgisExtensionsConstantsCommon.ARCGIS_AUTHENTICATION_METHOD);
        if (authenticationMethod.equalsIgnoreCase(ArcgisExtensionsConstantsCommon.OAUTH2)
                || authenticationMethod
                    .equalsIgnoreCase(ArcgisExtensionsConstantsCommon.TOKEN_BASED)) {
            final ArcgisExtensionsAuthenticationClient arcgisExtensionsAuthenticationClient =
                    new ArcgisExtensionsAuthenticationClient();
            accessToken = arcgisExtensionsAuthenticationClient.requestAccessToken();
        }

        if (StringUtils.isNotEmpty(accessToken)) {
            arcgisParameters = urlParameters + ArcgisExtensionsConstantsCommon.AMPERSAND
                    + ArcgisExtensionsConstantsCommon.TOKEN + ArcgisExtensionsConstantsCommon.EQUALS
                    + accessToken;
        } else {
            arcgisParameters = urlParameters;
        }

        /*
         * Execute the http post.
         */
        final ArcgisExtensionsHttpClient arcgisExtensionsHttpClient =
                new ArcgisExtensionsHttpClient();
        final JSONObject result =
                arcgisExtensionsHttpClient.executePostString(featureUrl, arcgisParameters);

        return result;
    }

    /**
     *
     * Check the delete results for an error.
     *
     * @param deleteResult The delete result from ArcGIS Server.
     * @return true if no error, false if error.
     */
    private static boolean deleteResultSuccess(final JSONObject deleteResult) {
        Assert.notNull(deleteResult, DELETE_RESULT_NOT_NULL_MESSAGE);

        boolean result = true;

        if (deleteResult.has(ArcgisExtensionsConstantsJsonNames.JSON_ERROR)) {
            result = false;
        }

        return result;
    }

}
