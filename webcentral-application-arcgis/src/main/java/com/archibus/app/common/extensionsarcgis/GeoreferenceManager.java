package com.archibus.app.common.extensionsarcgis;

import java.text.ParseException;

import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;
import org.springframework.util.Assert;

import com.archibus.context.ContextStore;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;
import com.archibus.jobmanager.EventHandlerContext;
import com.archibus.utility.*;

/**
 *
 * Provides methods to import or export drawing georeference parameters to or
 * from database.
 *
 * Used by the Extensions for Esri.
 *
 * @author knight
 *
 */
public final class GeoreferenceManager {

	/**
	 * Constant: assert dwgname has length message.
	 */
	private static final String DWGNAME_HAS_LENGTH_MESSAGE = "dwgname must be specified!";

	/**
	 * Constant: assert geoInfo not null message.
	 */
	private static final String GEO_INFO_NOT_NULL_MESSAGE = "geoInfo must be specified!";

	/**
	 * Record not found in afm_dwgs error message.
	 */
	private static final String ERROR_RECORD_NOT_FOUND_IN_AFM_DWGS = "Record not found in afm_dwgs.";

	/**
	 * Parsing geoInfo error message.
	 */
	private static final String ERROR_PARSING_GEO_INFO = "Error parsing geoInfo: ";

	/**
	 * Cannot save georeference parameters error message.
	 */
	private static final String ERROR_CANNOT_SAVE_GEOREFERENCE_PARAMETERS = "Cannot save georeference parameters. ";

	/**
	 * Private default constructor: utility class is non-instantiable.
	 *
	 * @throws InstantiationException
	 *             always, since this constructor should never be called.
	 */
	private GeoreferenceManager() throws InstantiationException {
		throw new InstantiationException("Never instantiate " + this.getClass().getName() + "; use static methods!");
	}

	/**
	 * Save the drawing georeference parameters to the database from the Smart
	 * Client Extension. The drawing name will be obtained from the event
	 * handler context.
	 *
	 * @throws ExceptionBase
	 *             if record cannot be saved to database.
	 */
	public static void saveDrawingGeoreferenceParametersToDatabaseFromSmartClientExtension() throws ExceptionBase {

		/*
		 * Get the dwgname and geoInfo parameters from the context.
		 */
		final EventHandlerContext context = ContextStore.get().getEventHandlerContext();

		String dwgname = null;
		JSONObject geoInfo = null;

		if (context.parameterExists(ArcgisExtensionsConstantsCommon.DWGNAME)) {
			dwgname = (String) context.getParameter(ArcgisExtensionsConstantsCommon.DWGNAME);
		}
		if (context.parameterExists(ArcgisExtensionsConstantsCommon.PARAMETER_GEOINFO)) {
			final String geoInfoStr = (String) context.getParameter(ArcgisExtensionsConstantsCommon.PARAMETER_GEOINFO);
			try {
				geoInfo = new JSONObject(geoInfoStr);
			} catch (final ParseException exception) {

				final String exceptionMessage = ERROR_CANNOT_SAVE_GEOREFERENCE_PARAMETERS
						+ ArcgisExtensionsConstantsCommon.COMMA + ERROR_PARSING_GEO_INFO;
				throw new ExceptionBase(exceptionMessage, exception);
			}
		}

		if (StringUtil.notNullOrEmpty(dwgname) && StringUtil.notNullOrEmpty(geoInfo)) {

			/*
			 * Create the datasource for afm_dwgs.
			 */
			final DataSource afmDwgsDs = createDrawingDataSource(dwgname);

			/*
			 * Get the record.
			 */
			final DataRecord dwgRecord = afmDwgsDs.getRecord();

			/*
			 * Update the record.
			 */
			if (StringUtil.notNullOrEmpty(dwgRecord)) {
				/*
				 * Add the values.
				 */
				dwgRecord.setValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_X,
						geoInfo.get(ArcgisExtensionsConstantsJsonNames.JSON_GEOX));
				dwgRecord.setValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_Y,
						geoInfo.get(ArcgisExtensionsConstantsJsonNames.JSON_GEOY));
				dwgRecord.setValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_SCALE,
						geoInfo.get(ArcgisExtensionsConstantsJsonNames.JSON_GEOSCALE));
				dwgRecord.setValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_ROTATE,
						geoInfo.get(ArcgisExtensionsConstantsJsonNames.JSON_GEOROTATE));
				dwgRecord.setValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_SRS,
						geoInfo.get(ArcgisExtensionsConstantsJsonNames.JSON_GEOSRS));
				dwgRecord.setValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_LEVEL,
						geoInfo.get(ArcgisExtensionsConstantsJsonNames.JSON_GEOLEVEL));

				/*
				 * Update the record.
				 */
				afmDwgsDs.updateRecord(dwgRecord);
			} else {
				final String errorMessage = ERROR_CANNOT_SAVE_GEOREFERENCE_PARAMETERS
						+ ArcgisExtensionsConstantsCommon.COMMA + ERROR_RECORD_NOT_FOUND_IN_AFM_DWGS;
				throw new ExceptionBase(errorMessage);
			}

		}

	}

	/**
	 * Save the drawing georeference parameters to the database.
	 *
	 * @param dwgname
	 *            The drawing name.
	 * @param geoInfo
	 *            The georeference parameters.
	 *
	 * @throws ExceptionBase
	 *             if record cannot be saved to database.
	 */
	public static void saveDrawingGeoreferenceParametersToDatabase(final String dwgname, final JSONObject geoInfo)
			throws ExceptionBase {

		Assert.hasLength(dwgname, DWGNAME_HAS_LENGTH_MESSAGE);
		Assert.notNull(geoInfo, GEO_INFO_NOT_NULL_MESSAGE);

		if (StringUtil.notNullOrEmpty(dwgname) && StringUtil.notNullOrEmpty(geoInfo)) {

			/*
			 * Create the datasource for afm_dwgs.
			 */
			final DataSource dwgsDs = createDrawingDataSource(dwgname);

			/*
			 * Get the record.
			 */
			final DataRecord dwgRecord = dwgsDs.getRecord();

			/*
			 * Update the record.
			 */
			if (StringUtil.notNullOrEmpty(dwgRecord)) {
				/*
				 * Add the values.
				 */
				setDrawingRecordValue(geoInfo, ArcgisExtensionsConstantsJsonNames.JSON_GEOX, dwgRecord,
						ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_X);
				setDrawingRecordValue(geoInfo, ArcgisExtensionsConstantsJsonNames.JSON_GEOY, dwgRecord,
						ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_Y);
				setDrawingRecordValue(geoInfo, ArcgisExtensionsConstantsJsonNames.JSON_GEOSCALE, dwgRecord,
						ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_SCALE);
				setDrawingRecordValue(geoInfo, ArcgisExtensionsConstantsJsonNames.JSON_GEOROTATE, dwgRecord,
						ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_ROTATE);
				setDrawingRecordValue(geoInfo, ArcgisExtensionsConstantsJsonNames.JSON_GEOSRS, dwgRecord,
						ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_SRS);
				setDrawingRecordValue(geoInfo, ArcgisExtensionsConstantsJsonNames.JSON_GEOLEVEL, dwgRecord,
						ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_LEVEL);

				/*
				 * Update the record.
				 */
				dwgsDs.updateRecord(dwgRecord);

			} else {

				final String errorMessage = ERROR_CANNOT_SAVE_GEOREFERENCE_PARAMETERS
						+ ArcgisExtensionsConstantsCommon.COMMA + ERROR_RECORD_NOT_FOUND_IN_AFM_DWGS;
				throw new ExceptionBase(errorMessage);
			}

		}

	}

	/**
	 * Get the drawing georeference parameters from the database.
	 *
	 * @param dwgname
	 *            The drawing name.
	 * @return The drawing georeference parameters.
	 */
	public static JSONObject getDrawingGeoreferenceParametersFromDatabase(final String dwgname) {

		Assert.hasLength(dwgname, DWGNAME_HAS_LENGTH_MESSAGE);

		JSONObject geoInfo = new JSONObject();

		if (StringUtil.notNullOrEmpty(dwgname)) {
			/*
			 * Get the record.
			 */
			final DataRecord dwgRecord = getDrawingRecordFromDatabase(dwgname);

			/*
			 * Create geoInfo from record.
			 */
			geoInfo = createGeoInfoFromRecord(dwgRecord);
		}

		return geoInfo;
	}

	/**
	 *
	 * Get the space hierarchy values for the drawing from the afm_dwgs table..
	 *
	 * @param dwgname
	 *            The drawing name.
	 * @return The space hierarchy values.
	 */
	public static String getSpaceHierFieldValuesFromDatabase(final String dwgname) {

		Assert.hasLength(dwgname, DWGNAME_HAS_LENGTH_MESSAGE);

		String spaceHierFieldValues = null;

		final DataRecord dwgRecord = getDrawingRecordFromDatabase(dwgname);

		if (StringUtil.notNullOrEmpty(dwgRecord
				.getOldValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_SPACE_HIER_FIELD_VALUES))) {
			spaceHierFieldValues = (String) dwgRecord
					.getOldValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_SPACE_HIER_FIELD_VALUES);
		}

		return spaceHierFieldValues;
	}

	/**
	 *
	 * Get the record for the drawing from the afm_dwgs table.
	 *
	 * @param dwgname
	 *            The drawing name.
	 * @return The record for the drawing.
	 */
	private static DataRecord getDrawingRecordFromDatabase(final String dwgname) {

		Assert.hasLength(dwgname, DWGNAME_HAS_LENGTH_MESSAGE);

		DataRecord dwgRecord = null;

		if (StringUtil.notNullOrEmpty(dwgname)) {
			/*
			 * Create the datasource for afm_dwgs.
			 */
			final DataSource dwgsDs = createDrawingDataSource(dwgname);

			/*
			 * Get the record.
			 */
			dwgRecord = dwgsDs.getRecord();
		}

		return dwgRecord;

	}

	/**
	 *
	 * Set the drawing record value.
	 *
	 * @param jsonObject
	 *            the JSON object.
	 * @param jsonKey
	 *            the data key.
	 * @param dataRecord
	 *            the data record.
	 * @param recordKey
	 *            the data record key.
	 */
	private static void setDrawingRecordValue(final JSONObject jsonObject, final String jsonKey,
			final DataRecord dataRecord, final String recordKey) {

		Assert.notNull(jsonObject, "jsonObject must be specified!");
		Assert.hasLength(jsonKey, "jsonKey must be specified!");
		Assert.notNull(dataRecord, "dataRecord must be specified!");
		Assert.hasLength(recordKey, "recordKey must be specified!");

		if (jsonObject.has(jsonKey)) {
			dataRecord.setValue(recordKey, jsonObject.get(jsonKey));
		}

	}

	/**
	 *
	 * Create the georeferencing information object from a data record.
	 *
	 * @param dwgRecord
	 *            The data record containing the georeferencing information.
	 * @return The georeferencing information.
	 */
	private static JSONObject createGeoInfoFromRecord(final DataRecord dwgRecord) {
		// Assert that the record is not null and has all required fields.
		//
		// The spatial reference system (geo_srs) is required.
		// The geo level (geo_level) is required.

		Assert.notNull(dwgRecord, "dwgRecord must be specified!");
		Assert.notNull(dwgRecord.getOldValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_LEVEL),
				"afm_dwgs.geo_level value is required!");
		Assert.notNull(dwgRecord.getOldValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_SRS),
				"afm_dwgs.geo_srs value is required!");

		// The insertion point (geoX, geoY), scale (geoScale), and rotation
		// (geoRotate) are required if the file is a DWG.
		Assert.notNull(dwgRecord.getOldValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_DWG_FILE),
				"afm_dwgs.dwg_file value is required!");
		final String fileExtension = FilenameUtils.getExtension(
				(String) dwgRecord.getOldValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_DWG_FILE));
		if (fileExtension.equalsIgnoreCase(ArcgisExtensionsConstantsCommon.DWG)) {
			Assert.notNull(dwgRecord.getOldValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_X),
					"afm_dwgs.geo_x value is required!");
			Assert.notNull(dwgRecord.getOldValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_Y),
					"afm_dwgs.geo_y value is required!");
			Assert.notNull(
					dwgRecord.getOldValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_SCALE),
					"afm_dwgs.geo_scale value is required!");
			Assert.notNull(
					dwgRecord.getOldValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_ROTATE),
					"afm_dwgs.geo_rotate value is required!");
		}

		final JSONObject geoInfo = new JSONObject();

		/*
		 * Add required values from record to geoInfo.
		 */
		geoInfo.put(ArcgisExtensionsConstantsJsonNames.JSON_GEOSRS,
				dwgRecord.getOldValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_SRS));
		geoInfo.put(ArcgisExtensionsConstantsJsonNames.JSON_GEOLEVEL,
				dwgRecord.getOldValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_LEVEL));
		/*
		 * Add optional values from record to geoInfo.
		 */
		if (fileExtension.equalsIgnoreCase(ArcgisExtensionsConstantsCommon.DWG)) {
			geoInfo.put(ArcgisExtensionsConstantsJsonNames.JSON_GEOX,
					dwgRecord.getOldValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_X));
			geoInfo.put(ArcgisExtensionsConstantsJsonNames.JSON_GEOY,
					dwgRecord.getOldValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_Y));
			geoInfo.put(ArcgisExtensionsConstantsJsonNames.JSON_GEOSCALE,
					dwgRecord.getOldValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_SCALE));
			geoInfo.put(ArcgisExtensionsConstantsJsonNames.JSON_GEOROTATE,
					dwgRecord.getOldValue(ArcgisExtensionsConstantsTableFieldNames.TABLE_FIELD_AFM_DWGS_GEO_ROTATE));
		}

		return geoInfo;
	}

	/**
	 *
	 * Create afm_dwgs datasource to access the geoInfo.
	 *
	 * @param dwgname
	 *            The ARCHIBUS drawing name.
	 * @return The afm_dwgs datasource.
	 */
	private static DataSource createDrawingDataSource(final String dwgname) {

		Assert.hasLength(dwgname, DWGNAME_HAS_LENGTH_MESSAGE);

		final DataSource dwgsDs = DataSourceFactory.createDataSource()
				.addTable(ArcgisExtensionsConstantsTableFieldNames.TABLE_AFM_DWGS, DataSource.ROLE_MAIN)
				.addField(ArcgisExtensionsConstantsTableFieldNames.FIELD_DWG_NAME)
				.addField(ArcgisExtensionsConstantsTableFieldNames.FIELD_SPACE_HIER_FIELD_VALUES)
				.addField(ArcgisExtensionsConstantsTableFieldNames.FIELD_GEO_X)
				.addField(ArcgisExtensionsConstantsTableFieldNames.FIELD_GEO_Y)
				.addField(ArcgisExtensionsConstantsTableFieldNames.FIELD_GEO_SCALE)
				.addField(ArcgisExtensionsConstantsTableFieldNames.FIELD_GEO_ROTATE)
				.addField(ArcgisExtensionsConstantsTableFieldNames.FIELD_GEO_SRS)
				.addField(ArcgisExtensionsConstantsTableFieldNames.FIELD_GEO_LEVEL)
				.addField(ArcgisExtensionsConstantsTableFieldNames.FIELD_DWG_FILE)
				.addRestriction(Restrictions.eq(ArcgisExtensionsConstantsTableFieldNames.TABLE_AFM_DWGS,
						ArcgisExtensionsConstantsTableFieldNames.FIELD_DWG_NAME, dwgname));
		return dwgsDs;
	}

}
