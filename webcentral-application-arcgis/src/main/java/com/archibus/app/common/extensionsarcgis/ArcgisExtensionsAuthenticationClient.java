package com.archibus.app.common.extensionsarcgis;

import java.util.*;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.jasypt.encryption.pbe.*;
import org.json.JSONObject;
import org.springframework.util.Assert;

import com.archibus.config.Project;
import com.archibus.servletx.WebCentralConfigListener;
import com.archibus.utility.ExceptionBase;

/**
 * This class contains methods for the Extensions for Esri to authenticate with ArcGIS Server or
 * ArcGIS Online.
 *
 * @author knight
 * @since 23.2
 *
 */

public class ArcgisExtensionsAuthenticationClient {

    /**
     * Constant: authentication method activity parameter not configured message.
     */
    private static final String AUTHENTICATION_METHOD_HAS_TEXT_MESSAGE =
            "AbCommonResources-ArcgisAuthenticationMethod activity parameter must be specified.";

    /**
     * Constant: authentication url activity parameter not configured message.
     */
    private static final String AUTHENTICATION_URL_HAS_TEXT_MESSAGE =
            "AbCommonResources-ArcgisAuthenticationUrl activity parameter must be specified.";

    /**
     * Logger to log messages to archibus log.
     */
    protected final Logger logger = Logger.getLogger(this.getClass());

    /**
     * The appId used to authenticate with ArcGIS Server (OAUTH2).
     */
    private String appId;

    /**
     * The appSecret used to authenticate with ArcGIS Server (OAUTH2).
     */
    private String appSecret;

    /**
     * The username used to authenticate with ArcGIS Server (TOKEN-BASED).
     */
    private String username;

    /**
     * The password used to authenticate with ArcGIS Server (TOKEN-BASED).
     */
    private String password;

    /**
     * An instance of the ArcgisExtenionsHttpClient, used to post to ArcGIS Server.
     */
    private ArcgisExtensionsHttpClient arcgisExtensionsHttpClient;

    /**
     * A reference to the current ARCHIBUS project, required to get activity parameter value.
     */
    private Project.Immutable project;

    /**
     *
     * Request an access token to access secure ArcGIS services.
     *
     * @return The access token.
     */
    public String requestAccessToken() {
        return this.requestTokenFromArcgisServer();
    }

    /**
     *
     * Request an access token from the ArcGIS Server using the configured authentication method,
     * url, and credentials.
     *
     * @return The accessToken.
     * @throws ExceptionBase if the ArcGIS token request fails.
     */
    private String requestTokenFromArcgisServer() {

        if (this.logger.isInfoEnabled()) {
            this.logger.info("requestTokenFromArcgisServer() -> Starting request.");
        }

        /*
         * The ArcGIS access token.
         */
        String accessToken = null;

        /*
         * The ArcGIS authentication URL.
         */
        final String authenticationUrl = this.project.getActivityParameterManager()
            .getParameterValue(ArcgisExtensionsConstantsCommon.ABCOMMONRESOURCES
                    + ArcgisExtensionsConstantsCommon.DASH
                    + ArcgisExtensionsConstantsCommon.ARCGIS_AUTHENTICATION_URL);

        /*
         * The ArcGIS authentication method.
         */
        final String authenticationMethod = this.project.getActivityParameterManager()
            .getParameterValue(ArcgisExtensionsConstantsCommon.ABCOMMONRESOURCES
                    + ArcgisExtensionsConstantsCommon.DASH
                    + ArcgisExtensionsConstantsCommon.ARCGIS_AUTHENTICATION_METHOD);

        /*
         * Assert that we have both a method and a URL.
         */
        Assert.hasText(authenticationMethod, AUTHENTICATION_METHOD_HAS_TEXT_MESSAGE);
        Assert.hasText(authenticationUrl, AUTHENTICATION_URL_HAS_TEXT_MESSAGE);

        /*
         * Create the form parameters for the post.
         */
        final List<NameValuePair> formParams = this.createFormParameters(authenticationMethod);

        /*
         * Execute the post.
         */
        final JSONObject result =
                this.arcgisExtensionsHttpClient.executePostForm(authenticationUrl, formParams);

        this.logger.info("requestTokenFromArcgisServer() -> Completed request.");

        /*
         * Assert result.
         */
        Assert.notNull(result, "Result from ArcGIS token request was null.");

        /*
         * Get the access token from the result.
         */
        if (result.has(ArcgisExtensionsConstantsCommon.TOKEN)) {
            accessToken = result.getString(ArcgisExtensionsConstantsCommon.TOKEN);
        } else if (result.has(ArcgisExtensionsConstantsCommon.ACCESS_TOKEN)) {
            accessToken = result.getString(ArcgisExtensionsConstantsCommon.ACCESS_TOKEN);
        } else {
            // @ non-translatable
            // log error until JIRA WC-2020 is resolved.
            this.logger.error(result.toString());
            throw new ExceptionBase(String
                .format("Failed to get token from ArcGIS Server. Result=[%s]", result.toString()));
        }

        this.logger.info("ArcGIS Server access token : " + accessToken);

        return accessToken;
    }

    /**
     *
     * Create the form parameters (name value pairs) for the ArcGIS Server http post.
     *
     * The form parameters include: username= password= client="HTTP Referer"
     * referer="localhost:8080/archibus" expiration= encrypted="true" f="json"
     *
     * @param authenticationMethod the authentication method to use for the post.
     * @return the form parameters.
     */
    private List<NameValuePair> createFormParameters(final String authenticationMethod) {

        final List<NameValuePair> formParams = new ArrayList<NameValuePair>();

        final String refererUrl = ArchibusProjectUtilities.getBaseURL();

        if (this.logger.isInfoEnabled()) {
            this.logger.info(String.format("refererUrl=[%s]", refererUrl));
        }

        if (authenticationMethod.equalsIgnoreCase(ArcgisExtensionsConstantsCommon.OAUTH2)) {
            // ArcGIS OAuth 2.0
            final String clientId = decodeParameter(this.getAppId());
            final String clientSecret = decodeParameter(this.getAppSecret());
            formParams.add(new BasicNameValuePair("client_id", clientId));
            formParams.add(new BasicNameValuePair("client_secret", clientSecret));
            formParams.add(new BasicNameValuePair("grant_type", "client_credentials"));
        } else if (authenticationMethod
            .equalsIgnoreCase(ArcgisExtensionsConstantsCommon.TOKEN_BASED)) {
            // ArcGIS generateToken REST API
            final String arcgisUsername = decodeParameter(this.getUsername());
            final String arcgisPassword = decodeParameter(this.getPassword());
            formParams.add(
                new BasicNameValuePair(ArcgisExtensionsConstantsCommon.USERNAME, arcgisUsername));
            formParams.add(
                new BasicNameValuePair(ArcgisExtensionsConstantsCommon.PASSWORD, arcgisPassword));
            formParams.add(new BasicNameValuePair("client", "HTTP Referer"));
            // For reference:
            // http://server.arcgis.com/en/server/latest/administer/linux/acquiring-arcgis-tokens.htm
            formParams.add(new BasicNameValuePair("referer", refererUrl));
            formParams.add(new BasicNameValuePair("expiration", ""));
            formParams.add(new BasicNameValuePair("encrypted", "true"));
            formParams.add(new BasicNameValuePair("f", "json"));
        } else {
            if (this.logger.isInfoEnabled()) {
                this.logger.info("ArcGIS Server authentication is not configured.");
            }
        }

        return formParams;
    }

    /**
     * Decrypts an encoded string. It is used to decrypt the encrypted username and password.
     *
     * @param parameter The encrypted string.
     * @return parameterDecrypted The decrypted string.
     */
    private static String decodeParameter(final String parameter) {
        final PBEStringEncryptor stringEncryptor = new StandardPBEStringEncryptor();
        stringEncryptor.setPassword(WebCentralConfigListener.PASSWORD);
        String parameterDecrypted = parameter;

        if (parameter.startsWith("ENC(")) {
            parameterDecrypted = stringEncryptor.decrypt(
                parameter.substring(ArcgisExtensionsConstantsCommon.INT_4, parameter.length() - 1));
        }
        return parameterDecrypted;
    }

    /**
     * Getter for the appId property.
     *
     * @return the appId property.
     */
    public String getAppId() {
        return this.appId;
    }

    /**
     * Setter for the appId property.
     *
     * @param appId the appId to set.
     */
    public void setAppId(final String appId) {
        this.appId = appId;
    }

    /**
     * Getter for the appSecret property.
     *
     * @return the appSecret property.
     */
    public String getAppSecret() {
        return this.appSecret;
    }

    /**
     * Setter for the appSecret property.
     *
     * @param appSecret the appSecret to set.
     */
    public void setAppSecret(final String appSecret) {
        this.appSecret = appSecret;
    }

    /**
     * Getter for the username property.
     *
     * @see username
     * @return the username property.
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Setter for the username property.
     *
     * @see username
     * @param username the username to set
     */

    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * Getter for the password property.
     *
     * @see password
     * @return the password property.
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Setter for the password property.
     *
     * @see password
     * @param password the password to set
     */

    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * Getter for the arcgisExtensionsHttpClient property.
     *
     * @return the arcgisExtensionsHttpClient property.
     */
    public ArcgisExtensionsHttpClient getArcgisExtensionsHttpClient() {
        return this.arcgisExtensionsHttpClient;
    }

    /**
     * Setter for the arcgisExtensionsHttpClient property.
     *
     * @param arcgisExtensionsHttpClient the arcgisExtensionsHttpClient to set.
     */
    public void setArcgisExtensionsHttpClient(
            final ArcgisExtensionsHttpClient arcgisExtensionsHttpClient) {
        this.arcgisExtensionsHttpClient = arcgisExtensionsHttpClient;
    }

    /**
     * Getter for the project property.
     *
     * @return the project property.
     */
    public Project.Immutable getProject() {
        return this.project;
    }

    /**
     * Setter for the project property.
     *
     * @param project the project to set.
     */
    public void setProject(final Project.Immutable project) {
        this.project = project;
    }

}
