package com.archibus.app.common.extensionsarcgis;

/**
 *
 * Common constants for the ArcgisExtensions.
 *
 * @author knight
 *
 */
public final class ArcgisExtensionsConstantsCommon {

    /**
     * The AbCommonResources string.
     */
    public static final String ABCOMMONRESOURCES = "AbCommonResources";

    /**
     * access_token string.
     */
    public static final String ACCESS_TOKEN = "access_token";

    /**
     * The ampersand character.
     */
    public static final String AMPERSAND = "&";

    /**
     * Arcgis string.
     */
    public static final String ARCGIS = "Arcgis";

    /**
     * The ArcgisAuthenticationUrl application parameter string.
     */
    public static final String ARCGIS_AUTHENTICATION_URL = "ArcgisAuthenticationUrl";

    /**
     * The ArcgisAuthenticationMethod application parameter string.
     */
    public static final String ARCGIS_AUTHENTICATION_METHOD = "ArcgisAuthenticationMethod";

    /**
     * The ArcGIS query feature URL parameter (1).
     */
    public static final String ARCGIS_URL_QUERY_FEATURES_PARAM_1 =
            "&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=";

    /**
     * The ArcGIS query feature URL parameter (2).
     */
    public static final String ARCGIS_URL_QUERY_FEATURES_PARAM_2 =
            "&returnGeometry=false&maxAllowableOffset=&geometryPrecision=&outSR=&gdbVersion=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&f=json";

    /**
     * The ArcGIS query features URL.
     */
    public static final String ARCGIS_URL_QUERY_FEATURES = "/query?";

    /**
     * The ArcGIS add or update features URL parameter.
     */
    public static final String ARCGIS_URL_ADD_UPDATE_FEATURES_PARAM =
            "&gdbVersion=&rollbackOnFailure=true&f=json";

    /**
     * The ArcGIS add features URL parameter.
     */
    public static final String ARCGIS_URL_ADD_FEATURES = "/addFeatures?f=json";

    /**
     * The ArcGIS update features URL.
     */
    public static final String ARCGIS_URL_UPDATE_FEATURES = "/updateFeatures?f=json";

    /**
     * The ArcGIS delete features URL parameter.
     */
    public static final String ARCGIS_URL_DELETE_FEATURES_PARAM =
            "&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&gdbVersion=&rollbackOnFailure=true&f=json";

    /**
     * The ArcGIS delete features URL.
     */
    public static final String ARCGIS_URL_DELETE_FEATURES = "/deleteFeatures?f=json";

    /**
     * The single quote HTML code.
     */
    public static final String ARCGIS_URL_SINGLE_QUOTE = "%27";

    /**
     * Asset string.
     */
    public static final String ASSETS = "Assets";

    /**
     * AssetType string.
     */
    public static final String ASSETTYPE = "AssetType";

    /**
     * Asset type background string.
     */
    public static final String ASSET_TYPE_BACKGROUND = "background";

    /**
     * Asset type building string.
     */
    public static final String ASSET_TYPE_BL = "bl";

    /**
     * Asset type gros string.
     */
    public static final String ASSET_TYPE_GROSS = "gros";

    /**
     * Asset type equipment string.
     */
    public static final String ASSET_TYPE_EQ = "eq";

    /**
     * Asset type property string.
     */
    public static final String ASSET_TYPE_PROPERTY = "property";

    /**
     * Asset type room string.
     */
    public static final String ASSET_TYPE_RM = "rm";

    /**
     * Background string.
     */
    public static final String BACKGROUND = "Background";

    /**
     * Building string.
     */
    public static final String BUILDING = "Building";

    /**
     * BuildingId string.
     */
    public static final String BUILDINGID = "BuildingId";

    /**
     * Comma string.
     */
    public static final String COMMA = ",";

    /**
     * Colon string.
     */
    public static final String COLON = ":";

    /**
     * Dash string.
     */
    public static final String DASH = "-";

    /**
     * DrawingName string.
     */
    public static final String DRAWINGNAME = "DrawingName";

    /**
     * Double forward slash string.
     */
    public static final String DOUBLE_FORWARD_SLASH = "//";

    /**
     * dwg string.
     */
    public static final String DWG = "dwg";

    /**
     * dwgname string.
     */
    public static final String DWGNAME = "dwgname";

    /**
     * Forward slash string.
     */
    public static final String FORWARD_SLASH = "/";

    /**
     * Equals.
     */
    public static final String EQUALS = "=";

    /**
     * Equipment string.
     */
    public static final String EQUIPMENT = "Equipment";

    /**
     * EquipmentId string.
     */
    public static final String EQUIPMENTID = "EquipmentId";

    /**
     * ExportConnectorId string.
     */
    public static final String EXPORTCONNECTORID = "ExportConnectorId";

    /**
     * ExtentsMax string.
     */
    public static final String EXTENTSMAX = "ExtentsMax";

    /**
     * ExtentsMin string.
     */
    public static final String EXTENTSMIN = "ExtentsMin";

    /**
     * Web Mercator EPSG code.
     */
    public static final String EPSG_WEB_MERC = "EPSG:3857";

    /**
     * Latitude-Longitude EPSG code.
     */
    public static final String EPSG_LAT_LON = "EPSG:4326";

    /**
     * Feature layer query parameter message.
     */
    public static final String FEATURE_LAYER_QUERY_PARAMETERS = "Feature layer query parameters: ";

    /**
     * Export to ArgGIS JSON filename.
     */
    public static final String FILENAME_EXPORT_ARCGIS_JSON = "-export-arcgis.json";

    /**
     * Import from ArgGIS JSON filename.
     */
    public static final String FILENAME_IMPORT_ARCGIS_JSON = "-import-arcgis.json";

    /**
     * Feature string.
     */
    public static final String FEATURE = "Feature";

    /**
     * FeatureCollection string.
     */
    public static final String FEATURECOLLECTION = "FeatureCollection";

    /**
     * FloorId string.
     */
    public static final String FLOORID = "FloorId";

    /**
     * GeoLevel string.
     */
    public static final String GEOLEVEL = "GeoLevel";

    /**
     * GeometryType string.
     */
    public static final String GEOMETRYTYPE = "GeometryType";

    /**
     * -geo.json file suffix/extension.
     */
    public static final String GEO_JSON_FILE_SUFFIX_EXTENSION = "-geo.json";

    /**
     * ID string.
     */
    public static final String ID_STRING = "ID";

    /**
     * ImportConnectorId string.
     */
    public static final String IMPORTCONNECTORID = "ImportConnectorId";

    /**
     * IsGeoreferenced string.
     */
    public static final String ISGEOREFERENCED = "IsGeoreferenced";

    /**
     * The JSON string.
     */
    public static final String JSON = "JSON";

    /**
     * Key string.
     */
    public static final String KEY = "Key";

    /**
     * Layer string.
     */
    public static final String LAYER = "Layer";

    /**
     * LayerAssetTypeField string.
     */
    public static final String LAYERASSETYPEFIELD = "LayerAssetTypeField";

    /**
     * LayerUrl string.
     */
    public static final String LAYERURL = "LayerUrl";

    /**
     * LayerIdField string.
     */
    public static final String LAYERIDFIELD = "LayerIdField";

    /**
     * LayerObjectIdField string.
     */
    public static final String LAYEROBJECTIDFIELD = "LayerObjectIdField";

    /**
     * Line string.
     */
    public static final String LINE = "Line";

    /**
     * LineString string.
     */
    public static final String LINESTRING = "LineString";

    /**
     * N/A string.
     */
    public static final String N_A = "N/A";

    /**
     * The integer '1'.
     */
    public static final int INT_1 = 1;

    /**
     * The integer '4'.
     */
    public static final int INT_4 = 4;

    /**
     * The integer '5'.
     */
    public static final int INT_5 = 5;

    /**
     * The number '100.0'.
     */
    public static final double DOUBLE_ONE_HUNDRED_POINT_ZERO = 100.0;

    /**
     * The number '1000000.0'.
     */
    public static final double DOUBLE_ONE_MILLION_POINT_ZERO = 100000000.0;

    /**
     * OAUTH2 string.
     */
    public static final String OAUTH2 = "OAUTH2";

    /**
     * geoInfo parameter.
     */
    public static final String PARAMETER_GEOINFO = "geoInfo";

    /**
     * methodName parameter.
     */
    public static final String PARAMETER_METHODNAME = "methodName";

    /**
     * objectIds parameter.
     */
    public static final String PARAMETER_OBJECTIDS = "objectIds";

    /**
     * where parameter.
     */
    public static final String PARAMETER_WHERE = "where";

    /**
     * whereClause parameter.
     */
    public static final String PARAMETER_WHERECLAUSE = "whereClause";

    /**
     * Parts string.
     */
    public static final String PARTS = "Parts";

    /**
     * password string.
     */
    public static final String PASSWORD = "password";

    /**
     * pipe character.
     */
    public static final String PIPE = "|";

    /**
     * Point string.
     */
    public static final String POINT = "Point";

    /**
     * Polygon string.
     */
    public static final String POLYGON = "Polygon";

    /**
     * Property string.
     */
    public static final String PROPERTY = "Property";

    /**
     * Room string.
     */
    public static final String ROOM = "Room";

    /**
     * RoomId string.
     */
    public static final String ROOMID = "RoomId";

    /**
     * Drive letter REGEX expression (e.g. 'C:\, D:\, Z:\, etc.')
     */
    public static final String REGEX_DRIVE_LETTER = "^[a-zA-Z]:\\\\.*";

    /**
     * Segments string.
     */
    public static final String SEGMENTS = "Segments";

    /**
     * Semicolon.
     */
    public static final String SEMICOLON = ";";

    /**
     * Shape string.
     */
    public static final String SHAPE = "Shape";

    /**
     * Single quote.
     */
    public static final String SINGLE_QUOTE = "'";

    /**
     * ' AND ' SQL expression.
     */
    public static final String SQL_AND = " AND ";

    /**
     * ' IS NOT NULL ' SQL expression.
     */
    public static final String SQL_IS_NOT_NULL = " IS NOT NULL ";

    /**
     * '1=1' SQL expression.
     */
    public static final String SQL_ONE_EQUALS_ONE = "1=1";

    /**
     * success string.
     */
    public static final String SUCCESS = "success";

    /**
     * token string.
     */
    public static final String TOKEN = "token";

    /**
     * TOKEN-BASED string.
     */
    public static final String TOKEN_BASED = "TOKEN-BASED";

    /**
     * Underscore character delimiter.
     */
    public static final String UNDERSCORE = "_";

    /**
     * username string.
     */
    public static final String USERNAME = "username";

    /**
     * Verticies string (Yes, it is incorrectly spelled in the ARCHIBUS published JSON).
     */
    public static final String VERTICES = "Verticies";

    /**
     * Web Mercator geometry precision.
     */
    public static final Double WEB_MERCATOR_LINE_SEGMENT_MIN_LENGTH = 0.0;

    /**
     * Upper case ARCGIS string.
     */
    public static final String UPPERCASE_ARCGIS = "ARCGIS";

    /**
     * P1 string.
     */
    public static final String UPPERCASE_P1 = "P1";

    /**
     * P2 string.
     */
    public static final String UPPERCASE_P2 = "P2";

    /**
     * X string.
     */
    public static final String UPPERCASE_X = "X";

    /**
     * Y string.
     */
    public static final String UPPERCASE_Y = "Y";

    /**
     * UTF-8 string.
     */
    public static final String UTF_8 = "UTF-8";

    /**
     *
     * Private default constructor: utility class is non-instantiable.
     *
     * @throws InstantiationException always, since this constructor should never be called.
     */
    private ArcgisExtensionsConstantsCommon() throws InstantiationException {
        throw new InstantiationException(
            "Never instantiate " + this.getClass().getName() + "; use static methods!");
    }

}
