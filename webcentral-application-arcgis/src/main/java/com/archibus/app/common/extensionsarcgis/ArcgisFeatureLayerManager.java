package com.archibus.app.common.extensionsarcgis;

import org.springframework.util.Assert;

import com.archibus.context.ContextStore;

import freemarker.template.utility.StringUtil;

/**
 *
 * Provides methods to create an ARCHIBUS ArcigsFeatureLayer for an ARCHIBUS Asset Type.
 *
 * @author knight
 *
 */
public class ArcgisFeatureLayerManager {

    /**
     * Constant: assert asset type has length message.
     */
    private static final String ASSET_TYPE_HAS_LENGTH_ERROR = "assetType must be specified!";

    /**
     * Constant: assert feature layer not null message.
     */
    private static final String FEATURE_LAYER_NOT_NULL_MESSAGE = "featureLayer must be specified!";

    /**
     * Constant: assert parameter value has length message.
     */
    private static final String PARAMETER_VALUE_HAS_LENGTH_MESSAGE =
            "parameterValue must be specified!";

    /**
     * Constant: assert where clause has length message.
     */
    private static final String WHERE_CLAUSE_HAS_LENGTH_ERRROR = "whereClause must be specified!";

    /**
     *
     * Create an ARCHIBUS ArcgisFeatureLayer. The method gets the necessary activity parameters to
     * create the feature layer.
     *
     * @param assetType The ARCHIBUS asset type.
     * @param whereClause The where clause for the feature layer.
     * @return the ARCHIBUS ArcgisFeatureLayer.
     */
    public ArcgisFeatureLayer createFeatureLayer(final String assetType, final String whereClause) {

        Assert.hasLength(assetType, ASSET_TYPE_HAS_LENGTH_ERROR);
        Assert.hasLength(whereClause, WHERE_CLAUSE_HAS_LENGTH_ERRROR);

        final ArcgisFeatureLayer arcgisFeatureLayer = new ArcgisFeatureLayer();

        /*
         * Set the layer URL.
         */
        final String urlParameter = ArcgisExtensionsConstantsCommon.ABCOMMONRESOURCES
                + ArcgisExtensionsConstantsCommon.DASH + ArcgisExtensionsConstantsCommon.ARCGIS
                + StringUtil.capitalize(assetType) + ArcgisExtensionsConstantsCommon.LAYERURL;
        final String url = getActivityParameterValue(urlParameter);
        arcgisFeatureLayer.setUrl(url);

        /*
         * Set the where clause.
         */
        arcgisFeatureLayer.setWhereClause(whereClause);

        /*
         * Add additional properties for ARCHIBUS assets. Do not add the asset type for background
         * graphics.
         */
        if (!(assetType.equalsIgnoreCase(ArcgisExtensionsConstantsCommon.ASSET_TYPE_BACKGROUND))) {
            addAssetParameters(arcgisFeatureLayer, assetType);
        }

        return arcgisFeatureLayer;
    }

    /**
     *
     * Add additional parameters to the feature layer specific to ARCHIBUS asset. The method gets
     * the necessary activity parameters to add the additional parameters.
     *
     * @param arcgisFeatureLayer The ArcGIS feature layer.
     * @param assetType The ARCHIBUS asset type.
     */
    private void addAssetParameters(final ArcgisFeatureLayer arcgisFeatureLayer,
            final String assetType) {

        Assert.notNull(arcgisFeatureLayer, FEATURE_LAYER_NOT_NULL_MESSAGE);
        Assert.hasLength(assetType, ASSET_TYPE_HAS_LENGTH_ERROR);

        /*
         * Set the ArcGIS asset type field.
         */
        final String assetTypeFieldParameter = ArcgisExtensionsConstantsCommon.ABCOMMONRESOURCES
                + ArcgisExtensionsConstantsCommon.DASH + ArcgisExtensionsConstantsCommon.ARCGIS
                + StringUtil.capitalize(assetType)
                + ArcgisExtensionsConstantsCommon.LAYERASSETYPEFIELD;
        final String assetTypeField = getActivityParameterValue(assetTypeFieldParameter);
        arcgisFeatureLayer.setAssetTypeField(assetTypeField);

        /*
         * Set the ARCHIBUS primary keys
         */
        final String[] assetKeyFields = ArchibusProjectUtilities.getAssetKeyFieldNames(assetType);
        arcgisFeatureLayer.setAssetKeyFields(assetKeyFields);

        /*
         * Set the ARCHIBUS layer id field.
         */
        final String idFieldParameter = ArcgisExtensionsConstantsCommon.ABCOMMONRESOURCES
                + ArcgisExtensionsConstantsCommon.DASH + ArcgisExtensionsConstantsCommon.ARCGIS
                + StringUtil.capitalize(assetType) + ArcgisExtensionsConstantsCommon.LAYERIDFIELD;
        final String idField = getActivityParameterValue(idFieldParameter);
        arcgisFeatureLayer.setIdField(idField);

        /*
         * Set the ArcGIS layer object id field.
         */
        final String objectIdFieldParameter = ArcgisExtensionsConstantsCommon.ABCOMMONRESOURCES
                + ArcgisExtensionsConstantsCommon.DASH + ArcgisExtensionsConstantsCommon.ARCGIS
                + StringUtil.capitalize(assetType)
                + ArcgisExtensionsConstantsCommon.LAYEROBJECTIDFIELD;
        final String objectIdField = getActivityParameterValue(objectIdFieldParameter);
        arcgisFeatureLayer.setObjectIdField(objectIdField);

        /*
         * Set the import connector id.
         */
        final String importConnectorIdParameter = ArcgisExtensionsConstantsCommon.ABCOMMONRESOURCES
                + ArcgisExtensionsConstantsCommon.DASH + ArcgisExtensionsConstantsCommon.ARCGIS
                + StringUtil.capitalize(assetType)
                + ArcgisExtensionsConstantsCommon.IMPORTCONNECTORID;
        final String importConnectorId = getActivityParameterValue(importConnectorIdParameter);
        arcgisFeatureLayer.setImportConnectorId(importConnectorId);

        /*
         * Set the export connector id.
         */
        final String exportConnectorIdParameter = ArcgisExtensionsConstantsCommon.ABCOMMONRESOURCES
                + ArcgisExtensionsConstantsCommon.DASH + ArcgisExtensionsConstantsCommon.ARCGIS
                + StringUtil.capitalize(assetType)
                + ArcgisExtensionsConstantsCommon.EXPORTCONNECTORID;
        final String exportConnectorId = getActivityParameterValue(exportConnectorIdParameter);
        arcgisFeatureLayer.setExportConnectorId(exportConnectorId);

        /*
         * Set the feature geometry type.
         */
        final String geometryTypeParameter = ArcgisExtensionsConstantsCommon.ABCOMMONRESOURCES
                + ArcgisExtensionsConstantsCommon.DASH + ArcgisExtensionsConstantsCommon.ARCGIS
                + StringUtil.capitalize(assetType) + ArcgisExtensionsConstantsCommon.GEOMETRYTYPE;
        final String geometryType = getActivityParameterValue(geometryTypeParameter);
        arcgisFeatureLayer.setGeometryType(geometryType);

    }

    /**
     *
     * Get an activity parameter value from the database.
     *
     * @param parameterName The activity parameter name.
     * @return The activity parameter value.
     */
    private static String getActivityParameterValue(final String parameterName) {

        Assert.hasLength(parameterName, PARAMETER_VALUE_HAS_LENGTH_MESSAGE);

        String parameterValue = null;
        parameterValue = ContextStore.get().getProject().getActivityParameterManager()
            .getParameterValue(parameterName);
        return parameterValue;
    }

}
