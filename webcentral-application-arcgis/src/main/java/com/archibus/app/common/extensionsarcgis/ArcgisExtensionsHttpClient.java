package com.archibus.app.common.extensionsarcgis;

import java.io.IOException;
import java.util.List;

import org.apache.http.*;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.*;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.util.Assert;

import com.archibus.utility.ExceptionBase;

/**
 *
 * Executes an HTTP post to ArcGIS Server.
 *
 * Used by the Extensions for Esri.
 *
 * @author knight
 * @since 23.2
 *
 */
public class ArcgisExtensionsHttpClient {

    /**
     * application/x-www-form-urlencoded string.
     */
    private static final String APPLICATION_X_WWW_FORM_URLENCODED =
            "application/x-www-form-urlencoded";

    /**
     * Content-Type string.
     */
    private static final String CONTENT_TYPE = "Content-Type";

    /**
     * Constant: assert url has text message.
     */
    private static final String URL_HAS_TEXT_MESSAGE = "url must be specified!";

    /**
     * Logger to log messages to archibus log.
     */
    protected final Logger logger = Logger.getLogger(this.getClass());

    /**
     *
     * Execute the HTTP post to the ArcGIS Server.
     *
     * @param url The URL for the ArcGIS Server service.
     * @param formParams The form parameters for the HTTP post.
     * @return the HTTP response from the ArcGIS Server.
     * @throws ExceptionBase if post fails with io exception.
     */
    public JSONObject executePostForm(final String url, final List<NameValuePair> formParams)
            throws ExceptionBase {

        Assert.hasText(url, URL_HAS_TEXT_MESSAGE);
        Assert.notNull(formParams, "formParams must be specified!");

        final CloseableHttpClient httpClient = HttpClients.createDefault();
        JSONObject result = null;

        try {
            HttpResponse httpResponse = null;
            final HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader(CONTENT_TYPE, APPLICATION_X_WWW_FORM_URLENCODED);

            final UrlEncodedFormEntity formEntity =
                    new UrlEncodedFormEntity(formParams, ArcgisExtensionsConstantsCommon.UTF_8);
            httpPost.setEntity(formEntity);

            httpResponse = httpClient.execute(httpPost);

            result = ArcgisExtensionsHttpResponseParser.getResult(httpResponse);

        } catch (final IOException exception) {
            // @non-translatable
            throw new ExceptionBase(ArcgisExtensionsConstantsErrors.ERROR_IO_EXCEPTION, exception);
        } finally {
            HttpClientUtils.closeQuietly(httpClient);
        }

        return result;
    }

    /**
     *
     * Execute the HTTP post to the ArcGIS Server.
     *
     * @param url The URL for the ArcGIS Server service.
     * @param urlParameters The url parameters for the HTTP post.
     * @return the HTTP response from the ArcGIS Server.
     * @throws ExceptionBase if post fails with io exception.
     */
    public JSONObject executePostString(final String url, final String urlParameters) {

        Assert.hasText(url, URL_HAS_TEXT_MESSAGE);
        Assert.hasText(urlParameters, "urlParameters must be specified!");

        final CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        JSONObject result = null;

        try {
            HttpResponse httpResponse = null;

            final StringEntity stringEntity = new StringEntity(urlParameters);

            final HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader(CONTENT_TYPE, APPLICATION_X_WWW_FORM_URLENCODED);
            httpPost.setEntity(stringEntity);

            httpResponse = httpClient.execute(httpPost);

            result = ArcgisExtensionsHttpResponseParser.getResult(httpResponse);

        } catch (final IOException exception) {
            // @non-translatable
            throw new ExceptionBase(ArcgisExtensionsConstantsErrors.ERROR_IO_EXCEPTION, exception);
        } finally {
            HttpClientUtils.closeQuietly(httpClient);
        }
        return result;
    }

}
