package com.archibus.app.common.extensionsarcgis;

import java.util.*;

import org.apache.log4j.Logger;
import org.geotools.factory.FactoryRegistryException;
import org.geotools.geometry.jts.*;
import org.geotools.referencing.CRS;
import org.json.*;
import org.opengis.geometry.MismatchedDimensionException;
import org.opengis.referencing.*;
import org.opengis.referencing.crs.*;
import org.opengis.referencing.operation.*;
import org.springframework.util.Assert;

import com.archibus.utility.ExceptionBase;
import com.vividsolutions.jts.geom.*;

/**
 * Provides methods for projecting GeoJSON features between two geographic coordinate systems.
 *
 * Used by the Extensions for Esri.
 *
 * @author knight
 */
public class GeoJsonFeatureProjector {

    /**
     * Assert: assert assetType has length message.
     */
    private static final String ASSET_TYPE_HAS_LENGTH_MESSAGE = "assetType must be specified!";

    /**
     * Assert: assert epsg code has length message.
     */
    private static final String EPSG_CODE_HAS_LENGTH_MESSAGE = "epsgCode must be specified!";

    /**
     * Constant: assert geoJson not null message.
     */
    private static final String GEOJSON_NOT_NULL_MESSAGE = "geoJson must be specified!";

    /**
     * Constant: assert geoJson features not null message.
     */
    private static final String GEOJSON_FEATURES_NOT_NULL_MESSSAGE =
            "geoJsonFeatures must be specified!";

    /**
     * Constant: assert geoJson projection features not null message.
     */
    private static final String GEOJSON_FEATURES_PROJ_NOT_NULL_MESSSAGE =
            "geoJsonFeaturesProj must be specified!";

    /**
     * Constant: assert geoJson point not null message.
     */
    private static final String GEOJSON_POINT_NOT_NULL_MESSAGE = "geoJsonPoint must be specified!";

    /**
     * Constant: assert inoutSrs has length message.
     */
    private static final String INPUT_SRS_HAS_LENGTH_MESSAGE = "inputSrs must be specified!";

    /**
     * Constant: assert outputSrs has length message.
     */
    private static final String OUTPUT_SRS_HAS_LENGTH_MESSAGE = "outputSrs must be specified!";

    /**
     * Constant: assert pub config not null message.
     */
    private static final String PUB_CONFIG_NOT_NULL_MESSAGE = "pubConfig must be specified!";

    /**
     * Allow for some error due to different datums.
     */
    private static final boolean LENIENT = false;

    /**
     * Logger to log messages to archibus log.
     */
    protected final Logger logger = Logger.getLogger(this.getClass());

    /**
     *
     * Project GeoJSON features between two geographic coordinate systems.
     *
     * @param geoJson The feature collection to project.
     * @param outputSrs The output spatial reference system.
     * @param arcgisPublishingConfiguration The publishing configuration.
     * @return The projected feature collection.
     */
    public JSONObject project(final JSONObject geoJson, final String outputSrs,
            final ArcgisPublishingConfiguration arcgisPublishingConfiguration) {

        Assert.notNull(geoJson, GEOJSON_NOT_NULL_MESSAGE);
        Assert.hasLength(outputSrs, OUTPUT_SRS_HAS_LENGTH_MESSAGE);
        Assert.notNull(arcgisPublishingConfiguration, PUB_CONFIG_NOT_NULL_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(String.format(
                "project() -> Starting projection from local geographic to world geographic: outputSrs=[%s]",
                outputSrs));
        }

        /*
         * Get the projection parameters.
         */
        final JSONObject crsObj =
                geoJson.getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_CRS);
        final JSONObject crsProp =
                crsObj.getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES);
        final String epsgSrc = crsProp.getString(ArcgisExtensionsConstantsJsonNames.JSON_NAME);
        String epsgDest = ArcgisExtensionsConstantsCommon.EPSG_WEB_MERC;
        if (outputSrs.equals(ArcgisExtensionsConstantsCommon.EPSG_LAT_LON)) {
            epsgDest = ArcgisExtensionsConstantsCommon.EPSG_LAT_LON;
        }

        /*
         * Get the features from the collection.
         */
        final JSONArray geoJsonFeatures =
                geoJson.getJSONArray(ArcgisExtensionsConstantsTableFieldNames.FIELD_FEATURES);
        geoJson.getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES);

        /*
         * Create an array for the projected features.
         */
        final JSONArray geoJsonFeaturesProj = new JSONArray();

        /*
         * Get the assets to publish from the publishing configuration.
         */
        final String[] publishAssetList = arcgisPublishingConfiguration.getPublishAssetTypes();

        /*
         * Project each asset in the publish asset list.
         */
        for (final String assetType : publishAssetList) {
            /*
             * project the features.
             */
            this.projectFeatures(geoJsonFeaturesProj, geoJsonFeatures, assetType, epsgSrc,
                epsgDest);
        }

        /*
         * Project the background graphics.
         */
        if (arcgisPublishingConfiguration.getPublishBackground()) {
            /*
             * Project the background features.
             */
            this.projectFeatures(geoJsonFeaturesProj, geoJsonFeatures,
                ArcgisExtensionsConstantsJsonNames.JSON_BACKGROUND, epsgSrc, epsgDest);
        }

        /*
         * Get the feature collection properties.
         */
        final JSONObject properties =
                geoJson.getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES);

        /*
         * Create the projected feature collection.
         */
        final JSONObject geoJsonProj = GeoJsonFeatureCreator
            .createGeoJsonFeatureCollection(epsgDest, properties, geoJsonFeaturesProj);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(String.format(
                "project() -> Completed projection from local geographic to world geographic: outputSrs=[%s]",
                outputSrs));
        }

        return geoJsonProj;
    }

    /**
     *
     * Project features from between two geographic coordinate systems.
     *
     * @param geoJsonFeaturesProj The projected features.
     * @param geoJsonFeatures The features to project.
     * @param assetType The ARCHIBUS asset type.
     * @param inputSrs The source spatial reference system.
     * @param outputSrs The destination spatial reference system.
     */
    private void projectFeatures(final JSONArray geoJsonFeaturesProj,
            final JSONArray geoJsonFeatures, final String assetType, final String inputSrs,
            final String outputSrs) {

        Assert.notNull(geoJsonFeatures, GEOJSON_FEATURES_NOT_NULL_MESSSAGE);
        Assert.hasLength(assetType, ASSET_TYPE_HAS_LENGTH_MESSAGE);
        Assert.hasLength(inputSrs, INPUT_SRS_HAS_LENGTH_MESSAGE);
        Assert.hasLength(outputSrs, OUTPUT_SRS_HAS_LENGTH_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(String
                .format("projectFeatures() -> Starting projection for assetType=[%s]", assetType));
        }

        /*
         * Get the GeoJSON features.
         */

        final JSONArray filteredFeatures =
                GeoJsonFeatureUtilities.filterFeatures(geoJsonFeatures, assetType);

        if (filteredFeatures.length() > 0) {
            if (assetType.equalsIgnoreCase(ArcgisExtensionsConstantsCommon.ASSET_TYPE_RM)) {
                this.projectPolygonFeatures(geoJsonFeaturesProj, filteredFeatures, inputSrs,
                    outputSrs);
            } else if (assetType
                .equalsIgnoreCase(ArcgisExtensionsConstantsCommon.ASSET_TYPE_GROSS)) {
                this.projectPolygonFeatures(geoJsonFeaturesProj, filteredFeatures, inputSrs,
                    outputSrs);
            } else if (assetType.equalsIgnoreCase(ArcgisExtensionsConstantsCommon.ASSET_TYPE_EQ)) {
                projectPointFeatures(geoJsonFeaturesProj, filteredFeatures, inputSrs, outputSrs);
            } else if (assetType
                .equalsIgnoreCase(ArcgisExtensionsConstantsCommon.ASSET_TYPE_BACKGROUND)) {
                this.projectLineFeatures(geoJsonFeaturesProj, filteredFeatures, inputSrs,
                    outputSrs);
            }

        } else {
            this.logger.error(String.format(
                "projectFeatures() -> Geometry type not defined for assetType=[%s]", assetType));
        }

        if (this.logger.isInfoEnabled()) {
            this.logger.info(String
                .format("projectFeatures() -> Completed projection for assetType=[%s]", assetType));
        }
    }

    /**
     *
     * Project polygon features between two geographic coordinate systems.
     *
     * @param geoJsonFeaturesProj The projected polygon features.
     * @param geoJsonFeatures The polygon features to transform.
     * @param inputSrs The source spatial reference system.
     * @param outputSrs The destination spatial reference system.
     * @throws ExceptionBase if projection fails with no such element exception.
     */
    private void projectPolygonFeatures(final JSONArray geoJsonFeaturesProj,
            final JSONArray geoJsonFeatures, final String inputSrs, final String outputSrs)
            throws ExceptionBase {

        Assert.notNull(geoJsonFeaturesProj, GEOJSON_FEATURES_PROJ_NOT_NULL_MESSSAGE);
        Assert.notNull(geoJsonFeatures, GEOJSON_FEATURES_NOT_NULL_MESSSAGE);
        Assert.hasLength(inputSrs, INPUT_SRS_HAS_LENGTH_MESSAGE);
        Assert.hasLength(outputSrs, OUTPUT_SRS_HAS_LENGTH_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info("projectPolygonFeatures() -> Starting polygon projection.");
        }

        try {

            /*
             * Loop over the features.
             */
            for (int i = 0; i < geoJsonFeatures.length(); i++) {

                final JSONObject geoJsonFeature = geoJsonFeatures.getJSONObject(i);

                final String featureId =
                        geoJsonFeature.getString(ArcgisExtensionsConstantsJsonNames.JSON_ID);
                final JSONObject featureProperties = geoJsonFeature
                    .getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES);
                final JSONObject featureGeometry = geoJsonFeature
                    .getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_GEOMETRY);
                featureGeometry.getString(ArcgisExtensionsConstantsJsonNames.JSON_TYPE);
                final JSONArray coordinates = featureGeometry
                    .getJSONArray(ArcgisExtensionsConstantsJsonNames.JSON_COORDINATES);
                final List<GeoJsonPoint> outerRing = (List<GeoJsonPoint>) coordinates.get(0);

                /*
                 * Create array list for projected coordinates.
                 */
                final List<GeoJsonPoint> coordinatesProj = new ArrayList<GeoJsonPoint>();

                /*
                 * Loop over the coordinates associated with the the individual asset.
                 */
                for (int j = 0; j < outerRing.size(); j++) {

                    /*
                     * Get source point from the polygon rings.
                     */
                    final GeoJsonPoint srcPoint = outerRing.get(j);

                    /*
                     * Project the point.
                     */
                    final GeoJsonPoint projPoint = this.projectPoint(srcPoint, inputSrs, outputSrs);

                    /*
                     * Add the projected point to the projected coordinate array.
                     */
                    coordinatesProj.add(projPoint);
                }
                final JSONArray polygonRings = new JSONArray();
                polygonRings.put(0, coordinatesProj);

                final JSONObject geoJsonFeatureProj = GeoJsonFeatureCreator
                    .createGeoJsonPolygonFeature(featureId, featureProperties, polygonRings);

                /*
                 * Add feature to feature list.
                 */
                geoJsonFeaturesProj.put(geoJsonFeatureProj);
            }
        } catch (final NoSuchElementException exception) {
            final String errorMessage = ArcgisExtensionsConstantsErrors.ERROR_PROJECTION_FAILED
                    + ArcgisExtensionsConstantsCommon.COMMA
                    + ArcgisExtensionsConstantsErrors.ERROR_NO_SUCH_ELEMENT_EXCEPTION;
            throw new ExceptionBase(errorMessage, exception);
        }

        if (this.logger.isInfoEnabled()) {
            this.logger.info("projectPolygonFeatures() -> Completed polygon projection.");
        }

    }

    /**
     *
     * Project point features between two geographic coordinate systems.
     *
     * @param geoJsonFeaturesProj The projected point features.
     * @param geoJsonFeatures The point features to transform.
     * @param inputSrs The source spatial reference system.
     * @param outputSrs The destination spatial reference system.
     * @throws ExceptionBase if transform fails with no such element exception.
     */
    private void projectPointFeatures(final JSONArray geoJsonFeaturesProj,
            final JSONArray geoJsonFeatures, final String inputSrs, final String outputSrs)
            throws ExceptionBase {

        Assert.notNull(geoJsonFeaturesProj, GEOJSON_FEATURES_PROJ_NOT_NULL_MESSSAGE);
        Assert.notNull(geoJsonFeatures, GEOJSON_FEATURES_NOT_NULL_MESSSAGE);
        Assert.hasLength(inputSrs, INPUT_SRS_HAS_LENGTH_MESSAGE);
        Assert.hasLength(outputSrs, OUTPUT_SRS_HAS_LENGTH_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info("projectPointFeatures() -> Starting point projection.");
        }

        try {

            /*
             * Loop over the list of assets.
             */
            for (int i = 0; i < geoJsonFeatures.length(); i++) {

                final JSONObject geoJsonFeature = geoJsonFeatures.getJSONObject(i);
                final String featureId =
                        geoJsonFeature.getString(ArcgisExtensionsConstantsJsonNames.JSON_ID);
                final JSONObject featureProperties = geoJsonFeature
                    .getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES);
                final JSONObject featureGeometry = geoJsonFeature
                    .getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_GEOMETRY);
                featureGeometry.getString(ArcgisExtensionsConstantsJsonNames.JSON_TYPE);

                /*
                 * Get the source point.
                 */
                final GeoJsonPoint srcPoint = (GeoJsonPoint) featureGeometry
                    .get(ArcgisExtensionsConstantsJsonNames.JSON_COORDINATES);

                /*
                 * Project the point.
                 */
                final GeoJsonPoint projPoint = this.projectPoint(srcPoint, inputSrs, outputSrs);

                /*
                 * Create the JSON for the projected feature.
                 */
                final JSONObject geoJsonFeatureProj = GeoJsonFeatureCreator
                    .createGeoJsonPointFeature(featureId, featureProperties, projPoint);

                /*
                 * Add the feature to the feature list.
                 */
                geoJsonFeaturesProj.put(geoJsonFeatureProj);
            }

        } catch (final NoSuchElementException exception) {
            final String errorMessage = ArcgisExtensionsConstantsErrors.ERROR_PROJECTION_FAILED
                    + ArcgisExtensionsConstantsCommon.COMMA
                    + ArcgisExtensionsConstantsErrors.ERROR_NO_SUCH_ELEMENT_EXCEPTION;
            throw new ExceptionBase(errorMessage, exception);
        }

        if (this.logger.isInfoEnabled()) {
            this.logger.info("projectPointFeatures() -> Completed point projection.");
        }

    }

    /**
     *
     * Project line features between two geographic coordinate systems.
     *
     * @param geoJsonFeaturesProj The projected line features.
     * @param geoJsonFeatures The line features to transform.
     * @param inputSrs The source spatial reference system.
     * @param outputSrs The destination spatial reference system.
     * @throws ExceptionBase if transform fails with no such element exception.
     */

    private void projectLineFeatures(final JSONArray geoJsonFeaturesProj,
            final JSONArray geoJsonFeatures, final String inputSrs, final String outputSrs)
            throws ExceptionBase {

        Assert.notNull(geoJsonFeaturesProj, GEOJSON_FEATURES_PROJ_NOT_NULL_MESSSAGE);
        Assert.notNull(geoJsonFeatures, GEOJSON_FEATURES_NOT_NULL_MESSSAGE);
        Assert.hasLength(inputSrs, INPUT_SRS_HAS_LENGTH_MESSAGE);
        Assert.hasLength(outputSrs, OUTPUT_SRS_HAS_LENGTH_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info("projectLineFeatures() -> Starting line projection.");
        }

        try {

            /*
             * Loop over the list of assets.
             */
            for (int i = 0; i < geoJsonFeatures.length(); i++) {
                /*
                 * Get the feature.
                 */
                final JSONObject geoJsonFeature = geoJsonFeatures.getJSONObject(i);

                /*
                 * Get the feature properties.
                 */
                // final String featureId = "NONE";
                final JSONObject featureProperties = geoJsonFeature
                    .getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES);
                final JSONObject featureGeometry = geoJsonFeature
                    .getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_GEOMETRY);
                featureGeometry.getString(ArcgisExtensionsConstantsJsonNames.JSON_TYPE);

                /*
                 * Get the array of coordinates for the line.
                 */
                final List<GeoJsonPoint> coordinates = (List<GeoJsonPoint>) featureGeometry
                    .get(ArcgisExtensionsConstantsJsonNames.JSON_COORDINATES);

                /*
                 * Create a new array list for the projected coordinates.
                 */
                final List<GeoJsonPoint> coordinatesProj = new ArrayList<GeoJsonPoint>();

                /*
                 * Loop over the coordinates of the line.
                 */
                for (int j = 0; j < coordinates.size(); j++) {

                    /*
                     * Get the point from the line.
                     */
                    final GeoJsonPoint srcPoint = coordinates.get(j);

                    /*
                     * Project the point.
                     */
                    final GeoJsonPoint projPoint = this.projectPoint(srcPoint, inputSrs, outputSrs);

                    /*
                     * Add the projected point to the coordinate array.
                     */
                    coordinatesProj.add(projPoint);

                }

                /*
                 * Create the GeoJSON for the projected feature.
                 */
                // Background graphics don't have feature ids, so create one.
                final String featureId = String.valueOf(i);
                final JSONObject geoJsonFeatureProj = GeoJsonFeatureCreator
                    .createGeoJsonLineStringFeature(featureId, featureProperties, coordinatesProj);

                /*
                 * Add feature to feature list.
                 */
                geoJsonFeaturesProj.put(geoJsonFeatureProj);
            }
        } catch (final NoSuchElementException exception) {
            final String errorMessage = ArcgisExtensionsConstantsErrors.ERROR_PROJECTION_FAILED
                    + ArcgisExtensionsConstantsCommon.COMMA
                    + ArcgisExtensionsConstantsErrors.ERROR_NO_SUCH_ELEMENT_EXCEPTION;
            throw new ExceptionBase(errorMessage, exception);
        }

        if (this.logger.isInfoEnabled()) {
            this.logger.info("projectLineFeatures() -> Completed line projection.");
        }

    }

    /**
     *
     * Project a point.
     *
     * @param geoJsonPoint The source point.
     * @param inputSrs The source ESPG code.
     * @param outputSrs The destination EPSG code.
     * @return The projected point.
     * @throws ExceptionBase if the projection fails.
     */
    private GeoJsonPoint projectPoint(final GeoJsonPoint geoJsonPoint, final String inputSrs,
            final String outputSrs) throws ExceptionBase {

        Assert.notNull(geoJsonPoint, GEOJSON_POINT_NOT_NULL_MESSAGE);
        Assert.hasLength(inputSrs, INPUT_SRS_HAS_LENGTH_MESSAGE);
        Assert.hasLength(outputSrs, OUTPUT_SRS_HAS_LENGTH_MESSAGE);

        GeoJsonPoint finalPoint = null;

        try {
            /*
             * Create the source and destination coordinate systems.
             */
            final CoordinateReferenceSystem crsSrc = createCoordinateReferenceSystem(inputSrs);
            final CoordinateReferenceSystem crsDst = createCoordinateReferenceSystem(outputSrs);

            /*
             * Create the transform.
             */
            final MathTransform transform = CRS.findMathTransform(crsSrc, crsDst, LENIENT);

            /*
             * Create the geometry factory.
             */
            final GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory(null);

            /*
             * Create the source coordinate.
             */
            final Point srcGeo = geometryFactory
                .createPoint(new Coordinate(geoJsonPoint.getX(), geoJsonPoint.getY()));
            /*
             * Do the projection.
             */
            Geometry dstGeo = null;
            try {
                dstGeo = JTS.transform(srcGeo, transform);
            } catch (final MismatchedDimensionException error) {
                // log non-fatal error.
                this.logger.error(
                    ArcgisExtensionsConstantsErrors.ERROR_PROJECTION_FAILED + error.getMessage());
            } catch (final TransformException error) {
                // log non-fatal error.
                this.logger.error(
                    ArcgisExtensionsConstantsErrors.ERROR_PROJECTION_FAILED + error.getMessage());
            }
            /*
             * Create the projected point.
             */
            final GeoJsonPoint projPoint =
                    new GeoJsonPoint(dstGeo.getCoordinate().x, dstGeo.getCoordinate().y);

            /*
             * Round the point coordinates.
             */
            finalPoint = GeoJsonFeatureUtilities.roundPointCoordinates(projPoint, outputSrs);

        } catch (final FactoryException exception) {

            final String errorMessage = ArcgisExtensionsConstantsErrors.ERROR_PROJECTION_FAILED
                    + ArcgisExtensionsConstantsCommon.COMMA
                    + ArcgisExtensionsConstantsErrors.ERROR_FACTORY_EXCEPTION;
            this.logger.error(errorMessage);
            throw new ExceptionBase(errorMessage, exception);

        } catch (final FactoryRegistryException exception) {

            final String errorMessage = ArcgisExtensionsConstantsErrors.ERROR_PROJECTION_FAILED
                    + ArcgisExtensionsConstantsCommon.COMMA
                    + ArcgisExtensionsConstantsErrors.ERROR_FACTORY_REGISTRY_EXCEPTION;
            this.logger.error(errorMessage);
            throw new ExceptionBase(errorMessage, exception);

        }

        return finalPoint;

    }

    /**
     *
     * Create a Coordinate Reference System object for the Projection.
     *
     * @param epsgCode The EPSG coordinate system code.
     * @return The Coordinate Reference System object.
     * @throws ExceptionBase if the coordinate reference system cannot be created.
     */
    private static CoordinateReferenceSystem createCoordinateReferenceSystem(final String epsgCode)
            throws ExceptionBase {

        Assert.hasLength(epsgCode, EPSG_CODE_HAS_LENGTH_MESSAGE);

        final CRSAuthorityFactory crsFactory = CRS.getAuthorityFactory(true);
        CoordinateReferenceSystem coordinateReferenceSystem = null;

        try {
            coordinateReferenceSystem = crsFactory.createCoordinateReferenceSystem(epsgCode);
        } catch (final NoSuchAuthorityCodeException exception) {

            final String errorMessage = ArcgisExtensionsConstantsErrors.ERROR_PROJECTION_FAILED
                    + ArcgisExtensionsConstantsCommon.COMMA
                    + ArcgisExtensionsConstantsErrors.ERROR_NO_SUCH_AUTHORITY_CODE_EXCEPTION;
            // @non-translatable
            throw new ExceptionBase(errorMessage, exception);

        } catch (final FactoryException exception) {

            final String errorMessage = ArcgisExtensionsConstantsErrors.ERROR_PROJECTION_FAILED
                    + ArcgisExtensionsConstantsCommon.COMMA
                    + ArcgisExtensionsConstantsErrors.ERROR_FACTORY_EXCEPTION;

            // @non-translatable
            throw new ExceptionBase(errorMessage, exception);

        }

        return coordinateReferenceSystem;

    }
}
