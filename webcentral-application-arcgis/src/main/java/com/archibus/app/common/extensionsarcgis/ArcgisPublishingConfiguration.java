package com.archibus.app.common.extensionsarcgis;

import org.springframework.util.Assert;

import com.archibus.context.ContextStore;
import com.enterprisedt.util.debug.Logger;

/**
 *
 * Stores publishing properties used by the Extensions for Esri (ArcGIS) classes.
 *
 * @author knight
 * @since 22.1
 *
 */

public final class ArcgisPublishingConfiguration {

    /**
     * Constant: assert arcgis publish asset types has length message.
     */
    private static final String ARCGIS_PUBLISH_ASSET_TYPES_HAS_LENGTH_MESSAGE =
            "ArcgisPublishAssetTypes activity parameter is required!";

    /**
     * Constant: assert arcgis publish format has length message.
     */
    private static final String ARCGIS_PUBLISH_FORMAT_HAS_LENGTH_MESSAGE =
            "ArcgisPublishFormat activity parameter is required!";

    /**
     * Include the background graphics when publishing.
     *
     */
    private Boolean publishBackground;

    /**
     * The format which the Extensions will publish to.
     *
     */
    private String publishFormat;

    /**
     * The list of asset types to publish.
     */
    private String[] publishAssetTypes;

    /**
     *
     * Create an instance of the publishing configuration.
     */
    public ArcgisPublishingConfiguration() {
        // create the publish configuration.
        getPublishConfigurationPropertiesFromDatabase();
    }

    /**
     * Get the publish configuration properties from the database.
     */
    private void getPublishConfigurationPropertiesFromDatabase() {

        // AbCommonResources.ArcgisPublishFormat
        this.getPublishFormatFromDatabase();

        // AbCommonResources.ArcgisPublishBackground
        this.getPublishBackgroundFromDatabase();

        // AbCommonResources.ArcgisPublishAssetTypes
        this.getPublishAssetTypesFromDatabase();

    }

    /**
     * Get the publish format from the database.
     */
    private void getPublishFormatFromDatabase() {

        final String arcgisPublishFormat =
                ContextStore.get().getProject().getActivityParameterManager()
                    .getParameterValue("AbCommonResources-ArcgisPublishFormat");
        Assert.hasLength(arcgisPublishFormat, ARCGIS_PUBLISH_FORMAT_HAS_LENGTH_MESSAGE);

        this.setPublishFormat(arcgisPublishFormat);
    }

    /**
     * Get publish background flag from database.
     */
    private void getPublishBackgroundFromDatabase() {

        final String background = ContextStore.get().getProject().getActivityParameterManager()
            .getParameterValue("AbCommonResources-ArcgisPublishBackground");
        // We make no assertion here since we can proceed without background
        // publishing.

        if ("TRUE".equalsIgnoreCase(background)) {
            this.setPublishBackground(true);
        } else if ("FALSE".equalsIgnoreCase(background)) {
            this.setPublishBackground(false);
            Logger.getLogger(ArcgisPublishingConfiguration.class).info(
                "getPublishBackgroundFromDatabase() -> ArcGIS background publishing is disabled.");
        } else {
            this.setPublishBackground(false);
            Logger.getLogger(ArcgisPublishingConfiguration.class).info(
                "getPublishBackgroundFromDatabase() -> ArcGIS background publishing is not configured.");
        }
    }

    /**
     * Get the list of asset types to publish from the database.
     */
    private void getPublishAssetTypesFromDatabase() {

        final String arcgisPublishAssetTypes =
                ContextStore.get().getProject().getActivityParameterManager()
                    .getParameterValue("AbCommonResources-ArcgisPublishAssetTypes");
        Assert.hasLength(arcgisPublishAssetTypes, ARCGIS_PUBLISH_ASSET_TYPES_HAS_LENGTH_MESSAGE);

        final String[] assetTypes = arcgisPublishAssetTypes.split(",");
        this.setPublishAssetTypes(assetTypes);

    }

    /**
     * Getter for the publishBackground property.
     *
     * @see publishBackground
     * @return the publishBackground property.
     */
    public Boolean getPublishBackground() {
        return this.publishBackground;
    }

    /**
     * Setter for the publishBackground property.
     *
     * @see publishBackground
     * @param publishBackground the publishBackground to set
     */

    public void setPublishBackground(final Boolean publishBackground) {
        this.publishBackground = publishBackground;
    }

    /**
     * Getter for the publishFormat property.
     *
     * @see publishFormat
     * @return the publishFormat property.
     */

    public String getPublishFormat() {
        return this.publishFormat;
    }

    /**
     * Setter for the publishFormat property.
     *
     * @see publishFormat
     * @param publishFormat the publishFormat to set.
     */

    public void setPublishFormat(final String publishFormat) {
        this.publishFormat = publishFormat;
    }

    /**
     *
     * Getter for the publishAssetTypes property.
     *
     * @see publishAssetTypes
     * @return the publishAssetTypes property.
     */
    public String[] getPublishAssetTypes() {
        return this.publishAssetTypes.clone();
    }

    /**
     *
     * Setter for the publishAssetTypes property.
     *
     * @see publishAssetTypes
     * @param publishAssetTypes the assetTypes to set.
     */
    public void setPublishAssetTypes(final String[] publishAssetTypes) {
        this.publishAssetTypes = publishAssetTypes.clone();
    }

}