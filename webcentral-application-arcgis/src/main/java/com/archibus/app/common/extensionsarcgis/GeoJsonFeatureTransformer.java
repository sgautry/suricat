package com.archibus.app.common.extensionsarcgis;

import java.awt.geom.AffineTransform;
import java.util.*;

import org.apache.log4j.Logger;
import org.json.*;
import org.springframework.util.Assert;

import com.archibus.utility.ExceptionBase;

/**
 * Provides methods for transforming GeoJSON features from CAD to local geographic coordinates.
 *
 * Used by the Extensions for Esri.
 *
 * @author knight
 */
public class GeoJsonFeatureTransformer {

    /**
     * Assert: assert assetType has length message.
     */
    private static final String ASSET_TYPE_HAS_LENGTH_MESSAGE = "assetType must be specified!";

    /**
     * Constant: assert geoInfo not null message.
     */
    private static final String GEO_INFO_NOT_NULL_MESSAGE = "geoInfo must be specified!";

    /**
     * Constant: assert geo x not null message.
     */
    private static final String GEO_X_NOT_NULL_MESSAGE = "geoX must be specified!";

    /**
     * Constant: assert geo y not null message.
     */
    private static final String GEO_Y_NOT_NULL_MESSAGE = "geoY must be specified!";

    /**
     * Constant: assert geo scale not null message.
     */
    private static final String GEO_SCALE_NOT_NULL_MESSAGE = "geoScale must be specified!";

    /**
     * Constant: assert geo rotate not null message.
     */
    private static final String GEO_ROTATE_NOT_NULL_MESSAGE = "geoRotate must be specified!";

    /**
     * Constant: assert geoJson not null message.
     */
    private static final String GEOJSON_NOT_NULL_MESSAGE = "geoJson must be specified!";

    /**
     * Constant: assert geoJson features not null message.
     */
    private static final String GEOJSON_FEATURES_NOT_NULL_MESSSAGE =
            "geoJsonFeatures must be specified!";

    /**
     * Constant: assert geoJson transform features not null message.
     */
    private static final String GEOJSON_FEATURES_XFRM_NOT_NULL_MESSAGE =
            "geoJsonFeaturesXfrm must be specified!";

    /**
     * Constant: assert geoJson point not null message.
     */
    private static final String GEOJSON_POINT_NOT_NULL_MESSAGE = "geoJsonPoint must be specified!";

    /**
     * Constant: assert hasGeoReferenceParams not null message.
     */
    private static final String HAS_GEOREFERENCE_PARAMS_NOT_NULL_MESSAGE =
            "hasGeoreferenceParams must be specified!";

    /**
     * Constant: assert isGeoreferenced not null message.
     */
    private static final String IS_GEOREFERENCED_NOT_NULL_MESSAGE =
            "isGeoreferenced must be specified!";

    /**
     * Constant: assert pub config not null message.
     */
    private static final String PUB_CONFIG_NOT_NULL_MESSAGE = "pubConfig must be specified!";

    /**
     * Logger to log messages to archibus log.
     */
    protected final Logger logger = Logger.getLogger(this.getClass());

    /**
     *
     * Transform features from Cartesian coordinates to local geographic coordinates.
     *
     * @param geoJson The feature collection to transform.
     * @param arcgisPublishingConfiguration The publishing configuration.
     * @return The transformed feature collection.
     */
    public JSONObject transform(final JSONObject geoJson,
            final ArcgisPublishingConfiguration arcgisPublishingConfiguration) {

        Assert.notNull(geoJson, GEOJSON_NOT_NULL_MESSAGE);
        Assert.notNull(arcgisPublishingConfiguration, PUB_CONFIG_NOT_NULL_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(
                "transform() -> Starting transformation from cartesian to local geographic coordinates.");
        }

        /*
         * Get the drawing properties.
         */

        final JSONObject drawingInfo =
                GeoJsonFeatureUtilities.getDrawingInfoFromCollection(geoJson);
        final Boolean isGeoreferenced = Boolean.valueOf(
            drawingInfo.getString(ArcgisExtensionsConstantsJsonNames.JSON_ISGEOREFERENCED));
        final String dwgname = drawingInfo.getString(ArcgisExtensionsConstantsCommon.DWGNAME);

        /*
         * Get the georeference parameters.
         */
        final JSONObject geoInfo =
                GeoreferenceManager.getDrawingGeoreferenceParametersFromDatabase(dwgname);

        /*
         * Check to see if we have a complete set of georeference parameters.
         */
        final Boolean hasGeoreferenceParams =
                GeoJsonFeatureUtilities.checkGeoreferenceParams(geoInfo);

        /*
         * Transform the features.
         */
        final JSONArray geoJsonFeaturesXfrm = this.doTransformFeatures(geoJson,
            arcgisPublishingConfiguration, geoInfo, hasGeoreferenceParams, isGeoreferenced);

        /*
         * Remove the georeference string from drawingInfo.
         */
        drawingInfo.remove(ArcgisExtensionsConstantsJsonNames.JSON_ISGEOREFERENCED);

        /*
         * Get the spatial reference system.
         */
        String geoSRS = "UNKNOWN";
        final Boolean hasSpatialReferenceSystem =
                GeoJsonFeatureUtilities.checkSpatialReferenceSystem(geoInfo);
        if (hasSpatialReferenceSystem) {
            geoSRS = geoInfo.getString(ArcgisExtensionsConstantsJsonNames.JSON_GEOSRS);
        }

        /*
         * Create the transformed feature collection.
         */
        final JSONObject properties = new JSONObject();
        properties.put(ArcgisExtensionsConstantsJsonNames.JSON_DWGINFO, drawingInfo);
        final JSONObject geoJsonXfrm = GeoJsonFeatureCreator.createGeoJsonFeatureCollection(geoSRS,
            properties, geoJsonFeaturesXfrm);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(
                "transform() -> Completed transformation from cartesian to local geographic coordinates.");
        }

        return geoJsonXfrm;
    }

    /**
     *
     * Transform features from Cartesian coordinates to local geographic coordinates.
     *
     * @param geoJson The GeoJSON feature collection.
     * @param arcgisPublishingConfiguration The publishing configuration.
     * @param geoInfo The georeference information.
     * @param hasGeoreferenceParams Has georeference parameters?
     * @param isGeoreferenced Is georeferenced?
     * @return The transformed GeoJSON features.
     */
    private JSONArray doTransformFeatures(final JSONObject geoJson,
            final ArcgisPublishingConfiguration arcgisPublishingConfiguration,
            final JSONObject geoInfo, final Boolean hasGeoreferenceParams,
            final Boolean isGeoreferenced) {

        Assert.notNull(geoJson, GEOJSON_NOT_NULL_MESSAGE);
        Assert.notNull(arcgisPublishingConfiguration, PUB_CONFIG_NOT_NULL_MESSAGE);
        Assert.notNull(geoInfo, GEO_INFO_NOT_NULL_MESSAGE);
        Assert.notNull(hasGeoreferenceParams, HAS_GEOREFERENCE_PARAMS_NOT_NULL_MESSAGE);
        Assert.notNull(isGeoreferenced, IS_GEOREFERENCED_NOT_NULL_MESSAGE);

        JSONArray geoJsonFeaturesXfrm = new JSONArray();

        if (hasGeoreferenceParams) {
            geoInfo.getString(ArcgisExtensionsConstantsJsonNames.JSON_GEOSRS);
        }

        /*
         * Get the features from the collection.
         */
        final JSONArray geoJsonFeatures =
                GeoJsonFeatureUtilities.getFeaturesFromCollection(geoJson);

        /*
         * If the assets are already georeferenced, don't apply the transformation.
         */
        if (isGeoreferenced) {
            geoJsonFeaturesXfrm = geoJsonFeatures;
        }

        /*
         * If the assets are not georeferenced and we have a georeference parameters, apply the
         * transformation.
         */
        if (!isGeoreferenced && hasGeoreferenceParams) {
            /*
             * Get the georeference parameters
             */
            final double geoX = geoInfo.getDouble(ArcgisExtensionsConstantsJsonNames.JSON_GEOX);
            final double geoY = geoInfo.getDouble(ArcgisExtensionsConstantsJsonNames.JSON_GEOY);
            final double geoRotate =
                    geoInfo.getDouble(ArcgisExtensionsConstantsJsonNames.JSON_GEOROTATE);
            final double geoScale =
                    geoInfo.getDouble(ArcgisExtensionsConstantsJsonNames.JSON_GEOSCALE);
            /*
             * Get the assets to publish from the publishing configuration.
             */
            final String[] publishAssetList = arcgisPublishingConfiguration.getPublishAssetTypes();

            /*
             * Transform each asset in the publish asset list.
             */
            for (final String assetType : publishAssetList) {
                /*
                 * transform the features.
                 */
                this.transformFeatures(geoJsonFeaturesXfrm, geoJsonFeatures, assetType, geoX, geoY,
                    geoScale, geoRotate);
            }

            /*
             * Transform the background graphics.
             */
            if (arcgisPublishingConfiguration.getPublishBackground()) {
                /*
                 * Transform the background features.
                 */
                this.transformFeatures(geoJsonFeaturesXfrm, geoJsonFeatures,
                    ArcgisExtensionsConstantsJsonNames.JSON_BACKGROUND, geoX, geoY, geoScale,
                    geoRotate);
            }
        }

        return geoJsonFeaturesXfrm;
    }

    /**
     *
     * Transform features from Cartesian coordinates to local geographic coordinates.
     *
     * @param geoJsonFeaturesXfrm The transformed features.
     * @param geoJsonFeatures The features to transform.
     * @param assetType The ARCHIBUS asset type.
     * @param geoX The X coordinate of the transform.
     * @param geoY The Y coordinate of the transform.
     * @param geoScale The scale of the transform.
     * @param geoRotate The rotation of the transform.
     */
    private void transformFeatures(final JSONArray geoJsonFeaturesXfrm,
            final JSONArray geoJsonFeatures, final String assetType, final double geoX,
            final double geoY, final double geoScale, final double geoRotate) {

        Assert.notNull(geoJsonFeaturesXfrm, GEOJSON_FEATURES_XFRM_NOT_NULL_MESSAGE);
        Assert.notNull(geoJsonFeatures, GEOJSON_FEATURES_NOT_NULL_MESSSAGE);
        Assert.hasLength(assetType, ASSET_TYPE_HAS_LENGTH_MESSAGE);
        Assert.notNull(geoX, GEO_X_NOT_NULL_MESSAGE);
        Assert.notNull(geoY, GEO_Y_NOT_NULL_MESSAGE);
        Assert.notNull(geoScale, GEO_SCALE_NOT_NULL_MESSAGE);
        Assert.notNull(geoRotate, GEO_ROTATE_NOT_NULL_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info(String.format(
                "transformFeatures() -> Starting transformation for assetType=[%s]", assetType));
        }

        /*
         * Get the GeoJSON features.
         */

        final JSONArray filteredFeatures =
                GeoJsonFeatureUtilities.filterFeatures(geoJsonFeatures, assetType);

        /*
         * Get geometry type from feature layer.
         */
        final ArcgisFeatureLayerManager arcgisFeatureLayerManager = new ArcgisFeatureLayerManager();
        String geometryType = null;
        if (assetType.equalsIgnoreCase(ArcgisExtensionsConstantsCommon.BACKGROUND)) {
            geometryType = ArcgisExtensionsConstantsCommon.LINE;
        } else {
            final ArcgisFeatureLayer featureLayer = arcgisFeatureLayerManager
                .createFeatureLayer(assetType, ArcgisExtensionsConstantsCommon.SQL_ONE_EQUALS_ONE);
            geometryType = featureLayer.getGeometryType();
        }

        /*
         * Transform the feature geometry.
         */
        if (geometryType.equalsIgnoreCase(ArcgisExtensionsConstantsCommon.POLYGON)) {
            this.transformPolygonFeatures(geoJsonFeaturesXfrm, filteredFeatures, geoX, geoY,
                geoScale, geoRotate);
        } else if (geometryType.equalsIgnoreCase(ArcgisExtensionsConstantsCommon.POINT)) {
            this.transformPointFeatures(geoJsonFeaturesXfrm, filteredFeatures, geoX, geoY, geoScale,
                geoRotate);
        } else if (geometryType.equalsIgnoreCase(ArcgisExtensionsConstantsCommon.LINE)) {
            this.transformLineFeatures(geoJsonFeaturesXfrm, filteredFeatures, geoX, geoY, geoScale,
                geoRotate);
        } else {
            this.logger.error(String.format(
                "transformFeatures() -> Geometry type not defined for assetType=[%s]", assetType));
        }

        if (this.logger.isInfoEnabled()) {
            this.logger.info(String.format(
                "transformFeatures() -> Completed transformation for assetType=[%s]", assetType));
        }

    }

    /**
     *
     * Transform polygon features.
     *
     * @param geoJsonFeaturesXfrm The transformed polygon features.
     * @param geoJsonFeatures The polygon features to be transformed.
     * @param geoX The X coordinate of the transform.
     * @param geoY The Y coordinate of the transform.
     * @param geoScale The scale of the transform.
     * @param geoRotate The rotation of the transform.
     * @throws ExceptionBase if transform fails with no such element exception.
     */
    private void transformPolygonFeatures(final JSONArray geoJsonFeaturesXfrm,
            final JSONArray geoJsonFeatures, final double geoX, final double geoY,
            final double geoScale, final double geoRotate) throws ExceptionBase {

        Assert.notNull(geoJsonFeaturesXfrm, GEOJSON_FEATURES_XFRM_NOT_NULL_MESSAGE);
        Assert.notNull(geoJsonFeatures, GEOJSON_FEATURES_NOT_NULL_MESSSAGE);
        Assert.notNull(geoX, GEO_X_NOT_NULL_MESSAGE);
        Assert.notNull(geoY, GEO_Y_NOT_NULL_MESSAGE);
        Assert.notNull(geoScale, GEO_SCALE_NOT_NULL_MESSAGE);
        Assert.notNull(geoRotate, GEO_ROTATE_NOT_NULL_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info("transformPolygonFeatures() -> Starting polygon transformation.");
        }

        try {

            /*
             * Loop over the features.
             */
            for (int i = 0; i < geoJsonFeatures.length(); i++) {

                final JSONObject geoJsonFeature = geoJsonFeatures.getJSONObject(i);

                final String featureId =
                        geoJsonFeature.getString(ArcgisExtensionsConstantsJsonNames.JSON_ID);
                final JSONObject featureProperties = geoJsonFeature
                    .getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES);
                final JSONObject featureGeometry = geoJsonFeature
                    .getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_GEOMETRY);
                featureGeometry.getString(ArcgisExtensionsConstantsJsonNames.JSON_TYPE);
                final JSONArray coordinates = featureGeometry
                    .getJSONArray(ArcgisExtensionsConstantsJsonNames.JSON_COORDINATES);
                final List<GeoJsonPoint> outerRing = (List<GeoJsonPoint>) coordinates.get(0);

                /*
                 * Create array list for transformed coordinates.
                 */
                final List<GeoJsonPoint> coordinatesXfrm = new ArrayList<GeoJsonPoint>();

                /*
                 * Loop over the coordinates associated with the individual asset.
                 */
                for (int j = 0; j < outerRing.size(); j++) {

                    /*
                     * Get the point.
                     */
                    final GeoJsonPoint point = outerRing.get(j);

                    /*
                     * Apply scale, rotation, translation.
                     */
                    final GeoJsonPoint xfrmPoint =
                            transformPoint(point, geoScale, geoRotate, geoX, geoY);

                    /*
                     * Add coordinate to transformed coordinate array.
                     */
                    coordinatesXfrm.add(xfrmPoint);
                }
                final JSONArray polygonRings = new JSONArray();
                polygonRings.put(0, coordinatesXfrm);

                /*
                 * Create GeoJSON for the transformed feature.
                 */
                final JSONObject geoJsonFeatureXfrm = GeoJsonFeatureCreator
                    .createGeoJsonPolygonFeature(featureId, featureProperties, polygonRings);

                /*
                 * Add the feature to the feature list.
                 */
                geoJsonFeaturesXfrm.put(geoJsonFeatureXfrm);
            }

        } catch (final NoSuchElementException exception) {
            final String errorMessage = ArcgisExtensionsConstantsErrors.ERROR_TRANSFORM_FAILED
                    + ArcgisExtensionsConstantsCommon.COMMA
                    + ArcgisExtensionsConstantsErrors.ERROR_NO_SUCH_ELEMENT_EXCEPTION;
            throw new ExceptionBase(errorMessage, exception);
        }

        if (this.logger.isInfoEnabled()) {
            this.logger.info("transformPolygonFeatures() -> Completed polygon transformation.");
        }
    }

    /**
     *
     * Transform point features.
     *
     * @param geoJsonFeaturesXfrm The transformed point features.
     * @param geoJsonFeatures The point features to be transformed.
     * @param geoX The X coordinate of the transform.
     * @param geoY The Y coordinate of the transform.
     * @param geoScale The scale of the transform.
     * @param geoRotate The rotation of the transform.
     *
     * @throws ExceptionBase if transform fails with no such element exception.
     */
    private void transformPointFeatures(final JSONArray geoJsonFeaturesXfrm,
            final JSONArray geoJsonFeatures, final double geoX, final double geoY,
            final double geoScale, final double geoRotate) throws ExceptionBase {

        Assert.notNull(geoJsonFeaturesXfrm, GEOJSON_FEATURES_XFRM_NOT_NULL_MESSAGE);
        Assert.notNull(geoJsonFeatures, GEOJSON_FEATURES_NOT_NULL_MESSSAGE);
        Assert.notNull(geoX, GEO_X_NOT_NULL_MESSAGE);
        Assert.notNull(geoY, GEO_Y_NOT_NULL_MESSAGE);
        Assert.notNull(geoScale, GEO_SCALE_NOT_NULL_MESSAGE);
        Assert.notNull(geoRotate, GEO_ROTATE_NOT_NULL_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info("transformPointFeatures() -> Starting point transformation.");
        }

        try {
            /*
             * Loop over the list of assets.
             */
            for (int i = 0; i < geoJsonFeatures.length(); i++) {

                final JSONObject geoJsonFeature = geoJsonFeatures.getJSONObject(i);
                final String featureId =
                        geoJsonFeature.getString(ArcgisExtensionsConstantsJsonNames.JSON_ID);
                final JSONObject featureProperties = geoJsonFeature
                    .getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES);
                final JSONObject featureGeometry = geoJsonFeature
                    .getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_GEOMETRY);
                featureGeometry.getString(ArcgisExtensionsConstantsJsonNames.JSON_TYPE);
                final GeoJsonPoint coordinate = (GeoJsonPoint) featureGeometry
                    .get(ArcgisExtensionsConstantsJsonNames.JSON_COORDINATES);

                /*
                 * Apply scale, rotation, translation.
                 */
                final GeoJsonPoint xfrmPoint =
                        transformPoint(coordinate, geoScale, geoRotate, geoX, geoY);

                /*
                 * Create GeoJSON for the transformed feature.
                 */

                final JSONObject geoJsonFeatureXfrm = GeoJsonFeatureCreator
                    .createGeoJsonPointFeature(featureId, featureProperties, xfrmPoint);

                /*
                 * Add the feature to the feature list.
                 */
                geoJsonFeaturesXfrm.put(geoJsonFeatureXfrm);
            }

        } catch (final NoSuchElementException exception) {

            final String errorMessage = ArcgisExtensionsConstantsErrors.ERROR_TRANSFORM_FAILED
                    + ArcgisExtensionsConstantsCommon.COMMA
                    + ArcgisExtensionsConstantsErrors.ERROR_NO_SUCH_ELEMENT_EXCEPTION;
            this.logger.error(errorMessage);
            throw new ExceptionBase(errorMessage, exception);
        }

        if (this.logger.isInfoEnabled()) {
            this.logger.info("transformPointFeatures() -> Completed point transformation.");
        }

    }

    /**
     *
     * Transform line features.
     *
     * @param geoJsonFeaturesXfrm The transformed line features.
     * @param geoJsonFeatures The line features to be transformed.
     * @param geoX The X coordinate of the transform.
     * @param geoY The Y coordinate of the transform.
     * @param geoScale The scale of the transform.
     * @param geoRotate The rotation of the transform.
     *
     * @throws ExceptionBase if transform fails with no such element exception.
     */
    private void transformLineFeatures(final JSONArray geoJsonFeaturesXfrm,
            final JSONArray geoJsonFeatures, final double geoX, final double geoY,
            final double geoScale, final double geoRotate) throws ExceptionBase {

        Assert.notNull(geoJsonFeaturesXfrm, GEOJSON_FEATURES_XFRM_NOT_NULL_MESSAGE);
        Assert.notNull(geoJsonFeatures, GEOJSON_FEATURES_NOT_NULL_MESSSAGE);
        Assert.notNull(geoX, GEO_X_NOT_NULL_MESSAGE);
        Assert.notNull(geoY, GEO_Y_NOT_NULL_MESSAGE);
        Assert.notNull(geoScale, GEO_SCALE_NOT_NULL_MESSAGE);
        Assert.notNull(geoRotate, GEO_ROTATE_NOT_NULL_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info("transformLineFeatures() -> Starting line transformation.");
        }

        try {

            /*
             * Loop over the list of assets.
             */
            for (int i = 0; i < geoJsonFeatures.length(); i++) {
                final JSONObject geoJsonFeature = geoJsonFeatures.getJSONObject(i);
                // TODO (we dont want this for backgrounds, but we need it for
                // linear assets)
                // featureObj.getString(ArcgisConstants.JSON_ID);
                // final String featureId = "";

                final JSONObject featureProperties = geoJsonFeature
                    .getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES);
                final JSONObject featureGeometry = geoJsonFeature
                    .getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_GEOMETRY);
                featureGeometry.getString(ArcgisExtensionsConstantsJsonNames.JSON_TYPE);
                final List<GeoJsonPoint> coordinates = (List<GeoJsonPoint>) featureGeometry
                    .get(ArcgisExtensionsConstantsJsonNames.JSON_COORDINATES);

                /*
                 * Create array list for transformed coordinates.
                 */
                final List<GeoJsonPoint> coordinatesXfrm = new ArrayList<GeoJsonPoint>();

                /*
                 * Loop over the coordinates associated with the individual asset.
                 */
                for (int j = 0; j < coordinates.size(); j++) {

                    /*
                     * Get the point.
                     */
                    final GeoJsonPoint point = coordinates.get(j);

                    /*
                     * Apply scale, rotation, translation.
                     */
                    final GeoJsonPoint xfrmPoint =
                            transformPoint(point, geoScale, geoRotate, geoX, geoY);

                    /*
                     * Add transformed coordinate to transformed coordinate array.
                     */
                    coordinatesXfrm.add(xfrmPoint);

                }

                /*
                 * Create GeoJSON for the transformed feature.
                 */
                // Background graphics don't have feature ids, so create one.
                final String featureId = String.valueOf(i);
                final JSONObject geoJsonFeatureXfrm = GeoJsonFeatureCreator
                    .createGeoJsonLineStringFeature(featureId, featureProperties, coordinatesXfrm);

                /*
                 * Add the feature to the feature list.
                 */
                geoJsonFeaturesXfrm.put(geoJsonFeatureXfrm);
            }

        } catch (final NoSuchElementException exception) {
            final String errorMessage = ArcgisExtensionsConstantsErrors.ERROR_TRANSFORM_FAILED
                    + ArcgisExtensionsConstantsCommon.COMMA
                    + ArcgisExtensionsConstantsErrors.ERROR_NO_SUCH_ELEMENT_EXCEPTION;
            throw new ExceptionBase(errorMessage, exception);
        }

        if (this.logger.isInfoEnabled()) {
            this.logger.info("transformLineFeatures() -> Completed line transformation.");
        }

    }

    /**
     *
     * Apply scale, rotation, and translation parameters to a point.
     *
     * @param geoJsonPoint The point to transform.
     * @param geoScale The scale factor.
     * @param geoRotate The rotation.
     * @param geoX The x translation.
     * @param geoY The y translation.
     * @return The transformed point.
     */
    @SuppressWarnings({ "PMD.UnusedFormalParameter" })
    /*
     * Justification: The geoScale parameter is a necessary georeferencing parameter. This parameter
     * will be re-enabled in a future release.
     *
     * See KB# 3046730.
     */
    private static GeoJsonPoint transformPoint(final GeoJsonPoint geoJsonPoint,
            final double geoScale, final double geoRotate, final double geoX, final double geoY) {

        Assert.notNull(geoJsonPoint, GEOJSON_POINT_NOT_NULL_MESSAGE);
        Assert.notNull(geoX, GEO_X_NOT_NULL_MESSAGE);
        Assert.notNull(geoY, GEO_Y_NOT_NULL_MESSAGE);
        Assert.notNull(geoScale, GEO_SCALE_NOT_NULL_MESSAGE);
        Assert.notNull(geoRotate, GEO_ROTATE_NOT_NULL_MESSAGE);

        /*
         * Apply Scale. Omit for now. See justification above.
         */
        // final GeoJsonPoint scalePoint = scalePoint(geoJsonPoint, geoScale);
        // Scale by 1 for now, until KB is resolved.
        final GeoJsonPoint scalePoint = scalePoint(geoJsonPoint, 1);

        /*
         * Apply Rotation.
         */
        final GeoJsonPoint rotatePoint = rotatePoint(scalePoint, geoRotate);

        /*
         * Apply Translation.
         */
        final GeoJsonPoint transPoint = translatePoint(rotatePoint, geoX, geoY);

        /*
         * Round coordinates.
         */
        final GeoJsonPoint newPoint = GeoJsonFeatureUtilities.roundPointCoordinates(transPoint,
            ArcgisExtensionsConstantsCommon.EPSG_WEB_MERC);

        return newPoint;
    }

    /**
     *
     * Apply scale to a point.
     *
     * @param geoJsonPoint The point to scale.
     * @param geoScale The scale factor.
     * @return The scaled point.
     */
    private static GeoJsonPoint scalePoint(final GeoJsonPoint geoJsonPoint, final double geoScale) {

        Assert.notNull(geoJsonPoint, GEOJSON_POINT_NOT_NULL_MESSAGE);
        Assert.notNull(geoScale, GEO_SCALE_NOT_NULL_MESSAGE);

        final AffineTransform xfrmScale = AffineTransform.getScaleInstance(geoScale, geoScale);

        // TODO test!!!
        // final GeoJsonPoint scalePoint = null;
        final GeoJsonPoint scalePoint = new GeoJsonPoint();
        xfrmScale.transform(geoJsonPoint, scalePoint);

        return scalePoint;

    }

    /**
     *
     * Apply rotation to a point.
     *
     * @param geoJsonPoint The point to rotate.
     * @param geoRotate The rotation.
     * @return The rotated point.
     */
    private static GeoJsonPoint rotatePoint(final GeoJsonPoint geoJsonPoint,
            final double geoRotate) {

        Assert.notNull(geoJsonPoint, GEOJSON_POINT_NOT_NULL_MESSAGE);
        Assert.notNull(geoRotate, GEO_ROTATE_NOT_NULL_MESSAGE);

        final AffineTransform xfrmRotate =
                AffineTransform.getRotateInstance(Math.toRadians(geoRotate), 0.0, 0.0);

        final GeoJsonPoint srcPoint = new GeoJsonPoint(geoJsonPoint.getX(), geoJsonPoint.getY());
        // TODO test!!!
        // final GeoJsonPoint rotatePoint = null;
        final GeoJsonPoint rotatePoint = new GeoJsonPoint();
        xfrmRotate.transform(srcPoint, rotatePoint);

        return rotatePoint;
    }

    /**
     *
     * Apply translation to a point.
     *
     * @param geoJsonPoint The point to translate.
     * @param geoX The x-translation.
     * @param geoY The y-translation.
     * @return The translated point.
     */
    private static GeoJsonPoint translatePoint(final GeoJsonPoint geoJsonPoint, final double geoX,
            final double geoY) {

        Assert.notNull(geoJsonPoint, GEOJSON_POINT_NOT_NULL_MESSAGE);
        Assert.notNull(geoX, GEO_X_NOT_NULL_MESSAGE);
        Assert.notNull(geoY, GEO_Y_NOT_NULL_MESSAGE);

        final AffineTransform xfrmTranslate = AffineTransform.getTranslateInstance(geoX, geoY);

        final GeoJsonPoint srcPoint = new GeoJsonPoint(geoJsonPoint.getX(), geoJsonPoint.getY());
        // TODO test!!!
        // final GeoJsonPoint transPoint = null;
        final GeoJsonPoint transPoint = new GeoJsonPoint();
        xfrmTranslate.transform(srcPoint, transPoint);

        return transPoint;
    }

}
