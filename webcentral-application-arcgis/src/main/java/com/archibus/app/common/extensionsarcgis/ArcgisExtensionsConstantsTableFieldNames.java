package com.archibus.app.common.extensionsarcgis;

/**
 *
 * Table and field name constants for the ArcgisExtensions.
 *
 * @author knight
 *
 */
public final class ArcgisExtensionsConstantsTableFieldNames {

	/**
	 * asset_type field name.
	 */
	public static final String FIELD_ASSET_TYPE = "asset_type";

	/**
	 * bl_id field name.
	 */
	public static final String FIELD_BL_ID = "bl_id";

	/**
	 * dwg_file field name.
	 */
	public static final String FIELD_DWG_FILE = "dwg_file";

	/**
	 * dwg_name field name.
	 */
	public static final String FIELD_DWG_NAME = "dwg_name";

	/**
	 * features field name.
	 */
	public static final String FIELD_FEATURES = "features";

	/**
	 * eq_id field name.
	 */
	public static final String FIELD_EQ_ID = "eq_id";

	/**
	 * fl_id field name.
	 */
	public static final String FIELD_FL_ID = "fl_id";

	/**
	 * geo_objectid field name.
	 */
	public static final String FIELD_GEO_OBJECTID = "geo_objectid";

	/**
	 * geo_x field name.
	 */
	public static final String FIELD_GEO_X = "geo_x";

	/**
	 * geo_y field name.
	 */
	public static final String FIELD_GEO_Y = "geo_y";

	/**
	 * geo_rotate field name.
	 */
	public static final String FIELD_GEO_ROTATE = "geo_rotate";

	/**
	 * geo_scale field name.
	 */
	public static final String FIELD_GEO_SCALE = "geo_scale";

	/**
	 * geo_srs field name.
	 */
	public static final String FIELD_GEO_SRS = "geo_srs";

	/**
	 * geo_level field name.
	 */
	public static final String FIELD_GEO_LEVEL = "geo_level";

	/**
	 * rm_id field name.
	 */
	public static final String FIELD_RM_ID = "rm_id";

	/**
	 * space_hier_field_values field name.
	 */
	public static final String FIELD_SPACE_HIER_FIELD_VALUES = "space_hier_field_values";

	/**
	 * afm_dwgs table name.
	 */
	public static final String TABLE_AFM_DWGS = "afm_dwgs";

	/**
	 * afm_dwgs.dwg_file table-field name.
	 */
	public static final String TABLE_FIELD_AFM_DWGS_DWG_FILE = "afm_dwgs.dwg_file";

	/**
	 * afm_dwgs.space_hier_field_values table-field name.
	 */
	public static final String TABLE_FIELD_AFM_DWGS_SPACE_HIER_FIELD_VALUES = "afm_dwgs.space_hier_field_values";

	/**
	 * afm_dwgs.geo_x table-field name.
	 */
	public static final String TABLE_FIELD_AFM_DWGS_GEO_X = "afm_dwgs.geo_x";

	/**
	 * afm_dwgs.geo_y table-field name.
	 */
	public static final String TABLE_FIELD_AFM_DWGS_GEO_Y = "afm_dwgs.geo_y";

	/**
	 * afm_dwgs.geo_rotate table-field name.
	 */
	public static final String TABLE_FIELD_AFM_DWGS_GEO_ROTATE = "afm_dwgs.geo_rotate";

	/**
	 * afm_dwgs.geo_scale table-field name.
	 */
	public static final String TABLE_FIELD_AFM_DWGS_GEO_SCALE = "afm_dwgs.geo_scale";

	/**
	 * afm_dwgs.geo_srs table-field name.
	 */
	public static final String TABLE_FIELD_AFM_DWGS_GEO_SRS = "afm_dwgs.geo_srs";

	/**
	 * afm_dwgs.geo_level table-field name.
	 */
	public static final String TABLE_FIELD_AFM_DWGS_GEO_LEVEL = "afm_dwgs.geo_level";

	/**
	 *
	 * Private default constructor: utility class is non-instantiable.
	 *
	 * @throws InstantiationException
	 *             always, since this constructor should never be called.
	 */
	private ArcgisExtensionsConstantsTableFieldNames() throws InstantiationException {
		throw new InstantiationException("Never instantiate " + this.getClass().getName() + "; use static methods!");
	}

}
