package com.archibus.app.common.extensionsarcgis;

/**
 *
 * Error message constants for the ArcgisExtensions.
 *
 * @author knight
 *
 */
public final class ArcgisExtensionsConstantsErrors {

    /**
     * Factory exception message.
     */
    public static final String ERROR_FACTORY_EXCEPTION = "Factory Exception. ";

    /**
     * Factory registry exception message.
     */
    public static final String ERROR_FACTORY_REGISTRY_EXCEPTION = "Factory Registry Exception. ";

    /**
     * IO exception message.
     */
    public static final String ERROR_IO_EXCEPTION = "IO Exception. ";

    /**
     * No Such Element Exception message.
     */
    public static final String ERROR_NO_SUCH_ELEMENT_EXCEPTION = "No Such Element Exception. ";

    /**
     * No Such Authority Code Exception message.
     */
    public static final String ERROR_NO_SUCH_AUTHORITY_CODE_EXCEPTION =
            "No Such Authority Code Exception.";

    /**
     * Projection failed message.
     */
    public static final String ERROR_PROJECTION_FAILED = "Projection failed. ";

    /**
     * Transform failed message.
     */
    public static final String ERROR_TRANSFORM_FAILED = "Transform failed. ";

    /**
     *
     * Private default constructor: utility class is non-instantiable.
     *
     * @throws InstantiationException always, since this constructor should never be called.
     */
    private ArcgisExtensionsConstantsErrors() throws InstantiationException {
        throw new InstantiationException(
            "Never instantiate " + this.getClass().getName() + "; use static methods!");
    }

}
