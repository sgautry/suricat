package com.archibus.app.common.extensionsarcgis;

import org.json.JSONArray;
import org.springframework.util.Assert;

/**
 *
 * Provides methods to create query strings for ArcGIS feature services.
 *
 *
 * @author knight
 *
 */
public final class ArcgisQueryStringUtilities {

    /**
     * Constant: assert asset type has length message.
     */
    private static final String ASSET_TYPE_HAS_LENGTH_MESSAGE = "assetType must be specified!";

    /**
     * Constant: assert dwgname has length message.
     */
    private static final String DWGNAME_HAS_LENGTH_MESSAGE = "dwgname must be specified!";

    /**
     * Constant: assert esriFeatures not null message.
     */
    private static final String ESRI_FEATURES_NOT_NULL_MESSAGE = "esriFeatures must be specified!";

    /**
     * Constant: assert featureLayerUrl has length message.
     */
    private static final String FEATURE_LAYER_URL_HAS_LENGTH_MESSAGE =
            "featureLayerUrl must be specified!";

    /**
     * Constant: assert id field has length message.
     */
    private static final String ID_FIELD_HAS_LENGTH_MESSAGE = "idField must be specified!";

    /**
     * Constant: assert objectId field has length message.
     */
    private static final String OBJECTID_FIELD_HAS_LENGTH_MESSAGE =
            "objectIdField must be specified!";

    /**
     * Constant: assert room key not empty message.
     */
    private static final String ROOMKEY_NOT_EMPTY_MESSAGE = "roomKey must be specified!";

    /**
     * Constant: assert where clause has length message.
     */
    private static final String WHERE_CLAUSE_HAS_LENGTH_MESSAGE = "whereClause muse be specified";

    /**
     *
     * Private default constructor: utility class is non-instantiable.
     *
     * * @throws InstantiationException always, since this constructor should never be called.
     */
    private ArcgisQueryStringUtilities() throws InstantiationException {
        throw new InstantiationException(
            "Never instantiate " + this.getClass().getName() + "; use static methods!");

    }

    /**
     *
     * Creates the add parameters string necessary to add features to an ArcGIS feature layer.
     *
     * @param esriFeatures The Esri features to add.
     * @return The Esri features with the add parameters.
     */
    public static String constructAddParams(final JSONArray esriFeatures) {
        Assert.notNull(esriFeatures, ESRI_FEATURES_NOT_NULL_MESSAGE);

        final String addParams = ArcgisExtensionsConstantsTableFieldNames.FIELD_FEATURES
                + ArcgisExtensionsConstantsCommon.EQUALS + esriFeatures.toString()
                + ArcgisExtensionsConstantsCommon.ARCGIS_URL_ADD_UPDATE_FEATURES_PARAM;

        return addParams;
    }

    /**
     *
     * Creates the add features url for an ArcGIS feature layer.
     *
     * @param featureLayerUrl The feature layer url.
     * @return The add string.
     */
    public static String constructAddUrl(final String featureLayerUrl) {
        Assert.hasLength(featureLayerUrl, FEATURE_LAYER_URL_HAS_LENGTH_MESSAGE);

        return featureLayerUrl + ArcgisExtensionsConstantsCommon.ARCGIS_URL_ADD_FEATURES;
    }

    /**
     *
     * Constructs the delete parameters to delete features from an ArcGIS feature layer.
     *
     * @param dwgname The ARCHIBUS drawing name.
     * @param assetType The ARCHIBUS asset type.
     * @param assetTypeField The ArcGIS field name containing the ARCHIBUS asset type.
     * @return The delete parameters.
     */
    public static String constructDeleteParamsByDrawing(final String dwgname,
            final String assetType, final String assetTypeField) {

        Assert.hasLength(dwgname, DWGNAME_HAS_LENGTH_MESSAGE);
        Assert.hasLength(assetType, ASSET_TYPE_HAS_LENGTH_MESSAGE);

        String deleteParams = ArcgisExtensionsConstantsCommon.PARAMETER_OBJECTIDS
                + ArcgisExtensionsConstantsCommon.EQUALS + ArcgisExtensionsConstantsCommon.AMPERSAND
                + ArcgisExtensionsConstantsCommon.PARAMETER_WHERE
                + ArcgisExtensionsConstantsCommon.EQUALS + ArcgisExtensionsConstantsCommon.DWGNAME
                + ArcgisExtensionsConstantsCommon.EQUALS
                + ArcgisExtensionsConstantsCommon.ARCGIS_URL_SINGLE_QUOTE + dwgname
                + ArcgisExtensionsConstantsCommon.ARCGIS_URL_SINGLE_QUOTE;

        if (assetTypeField != null) {
            deleteParams += ArcgisExtensionsConstantsCommon.SQL_AND;
            deleteParams += assetTypeField + ArcgisExtensionsConstantsCommon.EQUALS;
            deleteParams += ArcgisExtensionsConstantsCommon.ARCGIS_URL_SINGLE_QUOTE + assetType;
            deleteParams += ArcgisExtensionsConstantsCommon.ARCGIS_URL_SINGLE_QUOTE;
        }

        deleteParams += ArcgisExtensionsConstantsCommon.ARCGIS_URL_DELETE_FEATURES_PARAM;

        return deleteParams;
    }

    /**
     *
     * Constructs the delete features url for an ArcGIS feature layer.
     *
     * @param featureLayerUrl The feature layer url.
     * @return The delete string.
     */
    public static String constructDeleteUrl(final String featureLayerUrl) {

        Assert.hasLength(featureLayerUrl, FEATURE_LAYER_URL_HAS_LENGTH_MESSAGE);

        return featureLayerUrl + ArcgisExtensionsConstantsCommon.ARCGIS_URL_DELETE_FEATURES;
    }

    /**
     *
     * Constructs the query parameters to query ObjectIDs from an ArcGIS feature layer.
     *
     * @param whereClause The query where clause to be applied to the feature layer.
     * @param idField The ArcGIS feature layer id field name.
     * @param objectIdField The ArcGIS feature layer object id field name.
     * @return The query parameters.
     */
    public static String constructQueryParamsForObjectIds(final String whereClause,
            final String idField, final String objectIdField) {

        Assert.hasLength(whereClause, WHERE_CLAUSE_HAS_LENGTH_MESSAGE);
        Assert.hasLength(idField, ID_FIELD_HAS_LENGTH_MESSAGE);
        Assert.hasLength(objectIdField, OBJECTID_FIELD_HAS_LENGTH_MESSAGE);

        final String queryParams = ArcgisExtensionsConstantsCommon.PARAMETER_WHERE
                + ArcgisExtensionsConstantsCommon.EQUALS + whereClause
                + ArcgisExtensionsConstantsCommon.ARCGIS_URL_QUERY_FEATURES_PARAM_1 + idField
                + ArcgisExtensionsConstantsCommon.COMMA + objectIdField
                + ArcgisExtensionsConstantsCommon.ARCGIS_URL_QUERY_FEATURES_PARAM_2;
        return queryParams;
    }

    /**
     *
     * Constructs the query parameters to query ObjectIds by drawing name from an ArcGIS feature
     * layer.
     *
     * @param dwgname The ARCHIBUS drawing name.
     * @param idField The ArcGIS feature layer id field name.
     * @param objectIdField The ArcGIS feature layer object id field name.
     * @return The query parameters.
     */
    public static String constructQueryParamsForObjectIdsByDrawing(final String dwgname,
            final String idField, final String objectIdField) {

        Assert.hasLength(dwgname, DWGNAME_HAS_LENGTH_MESSAGE);
        Assert.hasLength(idField, ID_FIELD_HAS_LENGTH_MESSAGE);
        Assert.hasLength(objectIdField, OBJECTID_FIELD_HAS_LENGTH_MESSAGE);

        final String queryParams = ArcgisExtensionsConstantsCommon.PARAMETER_WHERE
                + ArcgisExtensionsConstantsCommon.EQUALS + ArcgisExtensionsConstantsCommon.DWGNAME
                + ArcgisExtensionsConstantsCommon.EQUALS
                + ArcgisExtensionsConstantsCommon.ARCGIS_URL_SINGLE_QUOTE + dwgname
                + ArcgisExtensionsConstantsCommon.ARCGIS_URL_SINGLE_QUOTE
                + ArcgisExtensionsConstantsCommon.ARCGIS_URL_QUERY_FEATURES_PARAM_1 + idField
                + ArcgisExtensionsConstantsCommon.COMMA + objectIdField
                + ArcgisExtensionsConstantsCommon.ARCGIS_URL_QUERY_FEATURES_PARAM_2;
        return queryParams;
    }

    /**
     *
     * Constructs the query parameters to query Room ObjectIds from an ArcGIS feature layer.
     *
     * @param dwgname The ARCHIBUS drawing name.
     * @param roomKey The ARCHIBUS room key.
     * @param idField The ArcGIS feature layer id field name.
     * @param objectIdField The ArcGIS feature layer object id field name.
     * @return The query parameters.
     */
    public static String constructQueryParamsForRoomObjectIds(final String dwgname,
            final String[] roomKey, final String idField, final String objectIdField) {

        Assert.hasLength(dwgname, DWGNAME_HAS_LENGTH_MESSAGE);
        Assert.notEmpty(roomKey, ROOMKEY_NOT_EMPTY_MESSAGE);
        Assert.hasLength(idField, ID_FIELD_HAS_LENGTH_MESSAGE);
        Assert.hasLength(objectIdField, OBJECTID_FIELD_HAS_LENGTH_MESSAGE);

        final String queryParams = ArcgisExtensionsConstantsCommon.PARAMETER_WHERE
                + ArcgisExtensionsConstantsCommon.EQUALS + ArcgisExtensionsConstantsCommon.DWGNAME
                + ArcgisExtensionsConstantsCommon.EQUALS
                + ArcgisExtensionsConstantsCommon.ARCGIS_URL_SINGLE_QUOTE + dwgname
                + ArcgisExtensionsConstantsCommon.ARCGIS_URL_SINGLE_QUOTE
                + ArcgisExtensionsConstantsCommon.SQL_AND + roomKey[2] + " NOT IN ("
                + ArcgisExtensionsConstantsCommon.ARCGIS_URL_SINGLE_QUOTE + "EXT"
                + ArcgisExtensionsConstantsCommon.ARCGIS_URL_SINGLE_QUOTE
                + ArcgisExtensionsConstantsCommon.COMMA
                + ArcgisExtensionsConstantsCommon.ARCGIS_URL_SINGLE_QUOTE + "INT"
                + ArcgisExtensionsConstantsCommon.ARCGIS_URL_SINGLE_QUOTE + ") "
                + ArcgisExtensionsConstantsCommon.ARCGIS_URL_QUERY_FEATURES_PARAM_1 + idField
                + ArcgisExtensionsConstantsCommon.COMMA + objectIdField
                + ArcgisExtensionsConstantsCommon.ARCGIS_URL_QUERY_FEATURES_PARAM_2;
        return queryParams;
    }

    /**
     *
     * Constructs the query features url for an ArcGIS feature layer.
     *
     * @param featureLayerUrl The feature layer url.
     * @return The query string.
     */
    public static String constructQueryUrl(final String featureLayerUrl) {
        Assert.hasLength(featureLayerUrl, FEATURE_LAYER_URL_HAS_LENGTH_MESSAGE);

        return featureLayerUrl + ArcgisExtensionsConstantsCommon.ARCGIS_URL_QUERY_FEATURES;

    }
}
