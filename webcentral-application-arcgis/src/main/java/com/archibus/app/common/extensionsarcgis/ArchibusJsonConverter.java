package com.archibus.app.common.extensionsarcgis;

import java.util.*;

import org.apache.log4j.Logger;
import org.json.*;
import org.springframework.util.Assert;

import com.archibus.utility.StringUtil;

/**
 *
 * Provides methods to convert ARCHIBUS JSON to the standard GeoJSON format.
 *
 * Used by the Extensions for Esri.
 *
 * @author knight
 * @since 23.1
 *
 */
public class ArchibusJsonConverter {

    /**
     * Constant: assert Archibus background not null message.
     */
    private static final String ARCHIBUS_BACKGROUND_NOT_NULL_MESSAGE =
            "archibusBackground must be specified!";

    /**
     * Constant: assert Archibus JSON not null message.
     */
    private static final String ARCHIBUS_JSON_NOT_NULL_MESSAGE = "archibusJson must be specified!";

    /**
     * Constant: assert Archibus JSON assets not null message.
     */
    private static final String ARCHIBUS_JSON_ASSETS_NOT_NULL_MESSAGE =
            "archibusJsonAssert must be specified!";

    /**
     * Constant: assert asset type has length message.
     */
    private static final String ASSET_TYPE_HAS_LENGTH_MESSAGE = "assetType must be specified!";

    /**
     * Constant: assert dwgname has length message.
     */
    private static final String DWGNAME_HAS_LENGTH_MESSAGE = "dwgname must be specified!";

    /**
     * Constant: assert GeoJson features not null message.
     */
    private static final String GEOJSON_FEATURES_NOT_NULL_MESSAGE =
            "geoJsonFeatures must be specified!";

    /**
     * Constant: assert geolevel not null message.
     */
    private static final String GEOLEVEL_NOT_NULL_MESSAGE = "geoLevel must be specified!";

    /**
     * Constant: assert geometry object not null message.
     */
    private static final String GEOMETRY_OBJ_NOT_NULL_MESSAGE = "geometryObj must be specified!";

    /**
     * Constant: assert pub config not null message.
     */
    private static final String PUB_CONFIG_NOT_NULL_MESSAGE = "pubConfig must be specified!";

    /**
     * Constant: assert shape object not null message.
     */
    private static final String SHAPE_OBJ_NOT_NULL_MESSAGE = "shapeObj must be specified!";

    /**
     * Logger to log messages to archibus log.
     */
    protected final Logger logger = Logger.getLogger(this.getClass());

    /**
     *
     * Convert ArchibusJSON to GeoJSON format.
     *
     * @param archibusJson The ARCHIBUS JSON.
     * @param arcgisPublishingConfiguration The publishing configuration.
     *
     * @return The GeoJSON.
     */
    public JSONObject convertToGeoJson(final JSONObject archibusJson,
            final ArcgisPublishingConfiguration arcgisPublishingConfiguration) {

        Assert.notNull(archibusJson, ARCHIBUS_JSON_NOT_NULL_MESSAGE);
        Assert.notNull(arcgisPublishingConfiguration, PUB_CONFIG_NOT_NULL_MESSAGE);

        if (this.logger.isInfoEnabled()) {
            this.logger.info("convertToGeoJson() -> Started converting achibusJSON to GeoJSON.");
        }

        /*
         * Get the assets to publish from the publishing configuration.
         */
        final String[] publishAssetList = arcgisPublishingConfiguration.getPublishAssetTypes();

        /*
         * Get the drawing name.
         */
        final String dwgname = ArchibusJsonUtilities.getDwgnameFromArchibusJson(archibusJson);
        /*
         * Get the georeferenced condition (of the JSON).
         */
        final String isGeoreferenced =
                archibusJson.getString(ArcgisExtensionsConstantsCommon.ISGEOREFERENCED).toString();
        /*
         * Create the dwgInfo object.
         */
        final JSONObject dwgInfo = new JSONObject();
        dwgInfo.put(ArcgisExtensionsConstantsCommon.DWGNAME, dwgname);
        dwgInfo.put(ArcgisExtensionsConstantsJsonNames.JSON_ISGEOREFERENCED, isGeoreferenced);

        /*
         * Get the geoLevel from the database.
         */
        Integer geoLevel = null;
        final JSONObject geoInfo =
                GeoreferenceManager.getDrawingGeoreferenceParametersFromDatabase(dwgname);
        if (geoInfo.has(ArcgisExtensionsConstantsJsonNames.JSON_GEOLEVEL)) {
            geoLevel = geoInfo.getInt(ArcgisExtensionsConstantsJsonNames.JSON_GEOLEVEL);
        }

        /*
         * Get the ARCHIBUS assets from the ARCHIBUS JSON.
         */
        final JSONObject archibusAssets =
                archibusJson.getJSONObject(ArcgisExtensionsConstantsCommon.ASSETS);

        /*
         * Create the GeoJSON features.
         */
        final JSONArray geoJsonFeatures = new JSONArray();

        /*
         * Convert each feature in the publish asset list.
         */
        for (final String assetType : publishAssetList) {
            /*
             * Convert the features.
             */
            this.convertFeatures(geoJsonFeatures, archibusAssets, dwgname, geoLevel, assetType);
        }

        /*
         * Convert the background graphics.
         */
        if (arcgisPublishingConfiguration.getPublishBackground()) {
            /*
             * Get the background from the ARCHIBUS JSON.
             */
            JSONArray archibusBackground = new JSONArray();
            if (archibusJson.has(ArcgisExtensionsConstantsCommon.BACKGROUND)) {
                archibusBackground =
                        archibusJson.getJSONArray(ArcgisExtensionsConstantsCommon.BACKGROUND);
                /*
                 * Convert the background features.
                 */
                this.convertBackground(geoJsonFeatures, archibusBackground, dwgname, geoLevel);
            } else {
                this.logger.error(String.format(
                    "convertToGeoJson() -> Cannot publish background. Background JSON not available for drawing=[%s]",
                    dwgname));
            }
        }

        /*
         * Create the GeoJSON feature collection.
         */
        final JSONObject geoJsonFeatureCollection =
                ArchibusJsonUtilities.createFeatureCollection(dwgInfo, geoJsonFeatures);

        if (this.logger.isInfoEnabled()) {
            this.logger.info("convertToGeoJson() -> Completed converting achibusJSON to GeoJSON.");
        }

        return geoJsonFeatureCollection;
    }

    /**
     *
     * Convert ARCHIBUS assets to GeoJSON format.
     *
     * @param geoJsonFeatures The converted GeoJSON features.
     * @param archibusAssets The ARCHIBUS assets.
     * @param dwgname The ARCHIBUS drawing name.
     * @param geoLevel The geo level for the ARCHIBUS draiwng.
     * @param assetType The ARCHIBUS asset type to convert.
     *
     */
    private void convertFeatures(final JSONArray geoJsonFeatures, final JSONObject archibusAssets,
            final String dwgname, final Integer geoLevel, final String assetType) {

        Assert.notNull(geoJsonFeatures, GEOJSON_FEATURES_NOT_NULL_MESSAGE);
        Assert.hasLength(dwgname, DWGNAME_HAS_LENGTH_MESSAGE);
        Assert.notNull(geoLevel, GEOLEVEL_NOT_NULL_MESSAGE);
        Assert.hasLength(assetType, ASSET_TYPE_HAS_LENGTH_MESSAGE);

        /*
         * Get the ARCHIBUS assets from the ARCHIBUS JSON.
         */
        if (archibusAssets.has(assetType)) {
            final JSONArray archibusJsonAssets = archibusAssets.getJSONArray(assetType);
            if (StringUtil.notNullOrEmpty(archibusAssets)) {
                this.archibusJsonToGeoJson(geoJsonFeatures, archibusJsonAssets, dwgname, geoLevel,
                    assetType);
            }
        }
    }

    /**
     *
     * Convert the ARCHBUS background graphics to GeoJSON format.
     *
     * @param geoJsonFeatures The converted GeoJSON features.
     * @param archibusBackground The ARCHIBUS background graphics.
     * @param dwgname The ARCHIBUS drawing name.
     * @param geoLevel The geo level of the ARCHIBUS drawing.
     */
    private void convertBackground(final JSONArray geoJsonFeatures,
            final JSONArray archibusBackground, final String dwgname, final Integer geoLevel) {

        Assert.notNull(geoJsonFeatures, GEOJSON_FEATURES_NOT_NULL_MESSAGE);
        Assert.notNull(archibusBackground, ARCHIBUS_BACKGROUND_NOT_NULL_MESSAGE);
        Assert.hasLength(dwgname, DWGNAME_HAS_LENGTH_MESSAGE);
        Assert.notNull(geoLevel, GEOLEVEL_NOT_NULL_MESSAGE);

        this.archibusBackgroundAssetsToGeoJson(geoJsonFeatures, archibusBackground, dwgname,
            geoLevel);
    }

    /**
     *
     * Convert ARCHIBUS GeoJSON to standard GeoJSON format.
     *
     * @param geoJsonFeatures The converted GeoJSON features.
     * @param archibusJsonAssets The ARCHIBUS assets.
     * @param dwgname The ARCHIBUS drawing name.
     * @param geoLevel The geo level of the ARCHIBUS drawing.
     * @param assetType The ARCHIBUS asset type to convert.
     */
    private void archibusJsonToGeoJson(final JSONArray geoJsonFeatures,
            final JSONArray archibusJsonAssets, final String dwgname, final Integer geoLevel,
            final String assetType) {

        Assert.notNull(geoJsonFeatures, GEOJSON_FEATURES_NOT_NULL_MESSAGE);
        Assert.notNull(archibusJsonAssets, ARCHIBUS_JSON_ASSETS_NOT_NULL_MESSAGE);
        Assert.hasLength(dwgname, DWGNAME_HAS_LENGTH_MESSAGE);
        Assert.notNull(geoLevel, GEOLEVEL_NOT_NULL_MESSAGE);
        Assert.hasLength(assetType, ASSET_TYPE_HAS_LENGTH_MESSAGE);

        /*
         * Get geometry type from feature layer.
         */
        final ArcgisFeatureLayerManager arcgisFeatureLayerManager = new ArcgisFeatureLayerManager();
        final ArcgisFeatureLayer featureLayer = arcgisFeatureLayerManager
            .createFeatureLayer(assetType, ArcgisExtensionsConstantsCommon.SQL_ONE_EQUALS_ONE);
        final String geometryType = featureLayer.getGeometryType();

        if (geometryType.equalsIgnoreCase(ArcgisExtensionsConstantsCommon.POLYGON)) {
            this.archibusPolygonAssetsToGeoJson(geoJsonFeatures, archibusJsonAssets, dwgname,
                geoLevel, assetType);
        } else if (geometryType.equalsIgnoreCase(ArcgisExtensionsConstantsCommon.POINT)) {
            this.archibusPointAssetsToGeoJson(geoJsonFeatures, archibusJsonAssets, dwgname,
                geoLevel, assetType);
        } else if (assetType
            .equalsIgnoreCase(ArcgisExtensionsConstantsCommon.ASSET_TYPE_BACKGROUND)) {
            this.archibusBackgroundAssetsToGeoJson(geoJsonFeatures, archibusJsonAssets, dwgname,
                geoLevel);
        } else {
            this.logger.error(String.format(
                "archibusJsonToGeoJson() -> Geometry not found for assetType=[%s]", assetType));
        }

    }

    /**
     *
     * Convert ARCHIBUS polygon geometry to GeoJSON format.
     *
     * @param geoJsonFeatures The converted GeoJSON features.
     * @param archibusJsonAssets The ARCHIBUS assets.
     * @param dwgname The ARCHIBUS drawing name.
     * @param geoLevel The geo level of the ARCHIBUS drawing.
     * @param assetType The ARCHIBUS asset type to convert.
     * @return The GeoJSON features.
     */
    private JSONArray archibusPolygonAssetsToGeoJson(final JSONArray geoJsonFeatures,
            final JSONArray archibusJsonAssets, final String dwgname, final Integer geoLevel,
            final String assetType) {

        Assert.notNull(geoJsonFeatures, GEOJSON_FEATURES_NOT_NULL_MESSAGE);
        Assert.notNull(archibusJsonAssets, ARCHIBUS_JSON_ASSETS_NOT_NULL_MESSAGE);
        Assert.hasLength(dwgname, DWGNAME_HAS_LENGTH_MESSAGE);
        Assert.notNull(geoLevel, GEOLEVEL_NOT_NULL_MESSAGE);
        Assert.hasLength(assetType, ASSET_TYPE_HAS_LENGTH_MESSAGE);

        /*
         * Loop over the list of assets.
         */
        for (int i = 0; i < archibusJsonAssets.length(); i++) {
            /*
             * Get the asset.
             */
            final JSONObject asset = archibusJsonAssets.getJSONObject(i);

            /*
             * If the asset has a shape, proceed.
             */
            if (asset.has(ArcgisExtensionsConstantsCommon.SHAPE)) {
                /*
                 * Get the asset key.
                 */
                final String assetKey = asset.getString(ArcgisExtensionsConstantsCommon.KEY);
                /*
                 * Get the asset key values.
                 */
                final String[] assetKeys =
                        assetKey.split(ArcgisExtensionsConstantsCommon.SEMICOLON);
                /*
                 * Get the asset key fields.
                 */
                final String[] assetKeyFields =
                        ArchibusProjectUtilities.getAssetKeyFieldNames(assetType);
                /*
                 * Get the asset shape/geometry.
                 */
                final JSONObject shapeObj =
                        asset.getJSONObject(ArcgisExtensionsConstantsCommon.SHAPE);
                /*
                 * Convert the geometry to GeoJson
                 */
                final JSONArray polygonRings =
                        this.convertArchibusShapeObjToGeoJsonPolygon(shapeObj);

                /*
                 * Create the GeoJson feature.
                 */
                final JSONObject geoJsonFeature = new JSONObject();
                geoJsonFeature.put(ArcgisExtensionsConstantsJsonNames.JSON_TYPE,
                    ArcgisExtensionsConstantsCommon.FEATURE);

                geoJsonFeature.put(ArcgisExtensionsConstantsJsonNames.JSON_ID, assetKey);

                final JSONObject featureProperties = new JSONObject();

                featureProperties.put(assetKeyFields[0], assetKeys[0]);
                featureProperties.put(assetKeyFields[1], assetKeys[1]);
                featureProperties.put(assetKeyFields[2], assetKeys[2]);

                if (StringUtil.notNullOrEmpty(geoLevel)) {
                    featureProperties.put(ArcgisExtensionsConstantsTableFieldNames.FIELD_GEO_LEVEL,
                        geoLevel);
                } else {
                    // TODO
                    featureProperties.put(ArcgisExtensionsConstantsTableFieldNames.FIELD_GEO_LEVEL,
                        ArcgisExtensionsConstantsCommon.INT_1);
                }

                featureProperties.put(ArcgisExtensionsConstantsTableFieldNames.FIELD_ASSET_TYPE,
                    assetType);
                featureProperties.put(ArcgisExtensionsConstantsCommon.DWGNAME, dwgname);
                geoJsonFeature.put(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES,
                    featureProperties);

                final JSONObject featureGeometry = new JSONObject();
                featureGeometry.put(ArcgisExtensionsConstantsJsonNames.JSON_TYPE,
                    ArcgisExtensionsConstantsCommon.POLYGON);

                featureGeometry.put(ArcgisExtensionsConstantsJsonNames.JSON_COORDINATES,
                    polygonRings);
                geoJsonFeature.put(ArcgisExtensionsConstantsJsonNames.JSON_GEOMETRY,
                    featureGeometry);

                /*
                 * If we have geometry, add the feature to the features array.
                 */
                final List<GeoJsonPoint> polygonRing = (List<GeoJsonPoint>) polygonRings.get(0);
                if (!polygonRing.isEmpty()) {
                    geoJsonFeatures.put(geoJsonFeature);
                }
            }
        }

        return geoJsonFeatures;
    }

    /**
     *
     * Convert ARCHIBUS point geometry to GeoJSON format.
     *
     * @param geoJsonFeatures The converted GeoJSON features.
     * @param archibusJsonAssets The ARCHIBUS assets.
     * @param dwgname The ARCHIBUS drawing name.
     * @param geoLevel The geo level of the ARCHIBUS drawing. param assetType The ARCHIBUS asset
     *            type.
     * @param assetType The ARCHIBUS asset type.
     * @return The GeoJSON point features.
     */
    private JSONArray archibusPointAssetsToGeoJson(final JSONArray geoJsonFeatures,
            final JSONArray archibusJsonAssets, final String dwgname, final Integer geoLevel,
            final String assetType) {

        Assert.notNull(geoJsonFeatures, GEOJSON_FEATURES_NOT_NULL_MESSAGE);
        Assert.notNull(archibusJsonAssets, ARCHIBUS_JSON_ASSETS_NOT_NULL_MESSAGE);
        Assert.hasLength(dwgname, DWGNAME_HAS_LENGTH_MESSAGE);
        Assert.notNull(geoLevel, GEOLEVEL_NOT_NULL_MESSAGE);
        Assert.hasLength(assetType, ASSET_TYPE_HAS_LENGTH_MESSAGE);

        /*
         * Loop over the list of assets.
         */
        for (int i = 0; i < archibusJsonAssets.length(); i++) {

            /*
             * Get the asset.
             */
            final JSONObject asset = archibusJsonAssets.getJSONObject(i);

            /*
             * Verify that we have a valid asset.
             */
            if (ArchibusJsonUtilities.assetIsValid(asset)) {
                /*
                 * Get the asset key value.
                 */
                final String assetKey = asset.getString(ArcgisExtensionsConstantsCommon.KEY);
                ArchibusProjectUtilities.getAssetKeyFieldNames(assetType);

                /*
                 * Get the asset key fields.
                 */
                final String[] assetKeyFields =
                        ArchibusProjectUtilities.getAssetKeyFieldNames(assetType);

                /*
                 * Get the asset type.
                 */
                final String assetTypeVal =
                        asset.getString(ArcgisExtensionsConstantsCommon.ASSETTYPE);

                /*
                 * Get the asset shape/geometry.
                 */
                final JSONObject shapeObj =
                        asset.getJSONObject(ArcgisExtensionsConstantsCommon.SHAPE);
                /*
                 * Get the insertion point from the shape/geometry.
                 */
                final JSONObject extentMin =
                        shapeObj.getJSONObject(ArcgisExtensionsConstantsCommon.EXTENTSMIN);
                final JSONObject extentMax =
                        shapeObj.getJSONObject(ArcgisExtensionsConstantsCommon.EXTENTSMAX);

                /*
                 * Construct the point.
                 */
                final GeoJsonPoint coordinate =
                        ArchibusJsonUtilities.calculateCenterFromExtent(extentMin, extentMax);

                /*
                 * Create feature properties JSON.
                 */
                final JSONObject featureProperties = ArchibusJsonUtilities.createFeatureProperties(
                    geoLevel, assetKeyFields, assetKey, assetTypeVal, dwgname);

                /*
                 * Create the GeoJson feature.
                 */
                final JSONObject geoJsonFeature = GeoJsonFeatureCreator
                    .createGeoJsonPointFeature(assetKey, featureProperties, coordinate);

                /*
                 * Add geoJSON feature to asset list.
                 */
                geoJsonFeatures.put(geoJsonFeature);
            } else {
                this.logger.error(String.format(
                    "archibusPointAssetsToGeoJson -> Invalid asset. Skipping asset conversion for: asset=[%s]",
                    asset.toString()));
            }

        }

        return geoJsonFeatures;
    }

    /**
     *
     * Convert ARCHIBUS background graphics to GeoJSON format.
     *
     * @param geoJsonFeatures The converted GeoJSON features.
     * @param archibusBackground The ARCHIBUS background graphics.
     * @param dwgname The ARCHIBUS drawing name.
     * @param geoLevel The geo level of the ARCHIBUS drawing.
     */
    private void archibusBackgroundAssetsToGeoJson(final JSONArray geoJsonFeatures,
            final JSONArray archibusBackground, final String dwgname, final Integer geoLevel) {

        Assert.notNull(geoJsonFeatures, GEOJSON_FEATURES_NOT_NULL_MESSAGE);
        Assert.notNull(archibusBackground, ARCHIBUS_BACKGROUND_NOT_NULL_MESSAGE);
        Assert.hasLength(dwgname, DWGNAME_HAS_LENGTH_MESSAGE);
        Assert.notNull(geoLevel, GEOLEVEL_NOT_NULL_MESSAGE);

        for (int i = 0; i < archibusBackground.length(); i++) {
            // Get the asset object.
            final JSONObject backgroundObj = archibusBackground.getJSONObject(i);
            // Get the geometry for the object
            final JSONArray backgroundGeometry =
                    ArchibusJsonUtilities.getGeometryForBackgroundObject(backgroundObj);

            for (int j = 0; j < backgroundGeometry.length(); j++) {

                if (backgroundGeometry.length() == 0) {
                    this.logger.error(
                        "archibusBackgroundAssetsToGeoJson() -> Invalid object. Object contains no geometry.");
                }

                // Get the geometry object.
                final JSONObject geometryObj = backgroundGeometry.getJSONObject(j);

                // Create the LineString coordinates.
                List<GeoJsonPoint> coordinates = new ArrayList<GeoJsonPoint>();

                // Check to see if this is a segment.
                if (geometryObj.has(ArcgisExtensionsConstantsCommon.SEGMENTS)) {
                    coordinates = createLineStringCoordinatesFromSegments(geometryObj);
                }
                // Check to see if this is a line.
                if (geometryObj.has(ArcgisExtensionsConstantsCommon.UPPERCASE_P1)
                        && (geometryObj.has(ArcgisExtensionsConstantsCommon.UPPERCASE_P2))) {
                    coordinates = createLineStringCoordinatesFromLine(geometryObj);
                }

                // Create the feature JSON.
                final JSONObject geoJsonFeature = new JSONObject();
                geoJsonFeature.put(ArcgisExtensionsConstantsJsonNames.JSON_TYPE,
                    ArcgisExtensionsConstantsCommon.FEATURE);

                // Get bl_id and fl_id for the feature properties.
                final String spaceHierFieldValues =
                        GeoreferenceManager.getSpaceHierFieldValuesFromDatabase(dwgname);
                final String[] buildingFloorKeys =
                        spaceHierFieldValues.split(ArcgisExtensionsConstantsCommon.SEMICOLON);
                final String buildingId = buildingFloorKeys[0];
                final String floorId = buildingFloorKeys[1];

                // Get the CAD layer
                String layerName = "";
                if (geometryObj.has(ArcgisExtensionsConstantsCommon.LAYER)) {
                    layerName = geometryObj.getString(ArcgisExtensionsConstantsCommon.LAYER);
                }

                // The feature properties.
                final JSONObject featureProperties = new JSONObject();
                // TODO add additional/configurable attributes
                featureProperties.put(ArcgisExtensionsConstantsTableFieldNames.FIELD_BL_ID,
                    buildingId);
                featureProperties.put(ArcgisExtensionsConstantsTableFieldNames.FIELD_FL_ID,
                    floorId);

                if (StringUtil.notNullOrEmpty(geoLevel)) {
                    featureProperties.put(ArcgisExtensionsConstantsTableFieldNames.FIELD_GEO_LEVEL,
                        geoLevel);
                } else {
                    featureProperties.put(ArcgisExtensionsConstantsTableFieldNames.FIELD_GEO_LEVEL,
                        ArcgisExtensionsConstantsCommon.INT_1);
                }

                featureProperties.put(ArcgisExtensionsConstantsTableFieldNames.FIELD_ASSET_TYPE,
                    ArcgisExtensionsConstantsJsonNames.JSON_BACKGROUND);
                featureProperties.put(ArcgisExtensionsConstantsJsonNames.JSON_DWGNAME, dwgname);
                featureProperties.put(ArcgisExtensionsConstantsJsonNames.JSON_LAYER, layerName);

                geoJsonFeature.put(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES,
                    featureProperties);

                // The feature geometry.
                final JSONObject featureGeometry = new JSONObject();
                featureGeometry.put(ArcgisExtensionsConstantsJsonNames.JSON_TYPE,
                    ArcgisExtensionsConstantsCommon.LINESTRING);
                featureGeometry.put(ArcgisExtensionsConstantsJsonNames.JSON_COORDINATES,
                    coordinates);
                geoJsonFeature.put(ArcgisExtensionsConstantsJsonNames.JSON_GEOMETRY,
                    featureGeometry);

                // Add the feature to the feature list.
                geoJsonFeatures.put(geoJsonFeature);
            }
        }
    }

    /**
     *
     * Convert ARCHIBUS shape geometry to GeoJSON polygon.
     *
     * @param shapeObj The ARCHIBUS shape object to convert.
     * @return The GeoJSON polygon object.
     */
    private JSONArray convertArchibusShapeObjToGeoJsonPolygon(final JSONObject shapeObj) {

        Assert.notNull(shapeObj, SHAPE_OBJ_NOT_NULL_MESSAGE);

        /*
         * The GeoJson polygon.
         */
        final JSONArray polygonRings = new JSONArray();
        /*
         * Construct outer ring of polygon.
         */
        final List<GeoJsonPoint> outerRing = new ArrayList<GeoJsonPoint>();

        /*
         * If we have a valid shape object.
         */
        if (shapeObj.has(ArcgisExtensionsConstantsCommon.VERTICES)) {
            /*
             * Get the list of vertices.
             */
            final JSONArray verticesList =
                    shapeObj.getJSONArray(ArcgisExtensionsConstantsCommon.VERTICES);

            /*
             * Loop over list of vertices.
             */
            for (int j = 0; j < verticesList.length(); j++) {
                final JSONObject verticeObj = verticesList.getJSONObject(j);
                final double xCoord =
                        verticeObj.getDouble(ArcgisExtensionsConstantsCommon.UPPERCASE_X);
                final double yCoord =
                        verticeObj.getDouble(ArcgisExtensionsConstantsCommon.UPPERCASE_Y);
                /*
                 * Create a new 2D point.
                 */
                final GeoJsonPoint coord = new GeoJsonPoint(xCoord, yCoord);
                /*
                 * Add 2D Point to outerRing.
                 */
                outerRing.add(coord);
            }
            /*
             * The first and last point in polygon array must be the same (coincident).
             */
            outerRing.add(outerRing.get(0));

        }
        /*
         * Add the outer ring.
         */
        polygonRings.put(0, outerRing);

        return polygonRings;
    }

    /**
     *
     * Create GeoJSON LineString coordinates from ARCHIBUS Segments.
     *
     * @param geometryObj The ARCHIBUS geometry object (segments).
     * @return The GeoJSON LineString coordinates.
     */
    private static List<GeoJsonPoint> createLineStringCoordinatesFromSegments(
            final JSONObject geometryObj) {

        Assert.notNull(geometryObj, GEOMETRY_OBJ_NOT_NULL_MESSAGE);

        final List<GeoJsonPoint> coordinates = new ArrayList<GeoJsonPoint>();

        final JSONArray geometryArray =
                geometryObj.getJSONArray(ArcgisExtensionsConstantsCommon.SEGMENTS);

        for (int k = 0; k < geometryArray.length(); k++) {

            final JSONObject geometrySegment = geometryArray.getJSONObject(k);
            /*
             * For the first segment, get X and Y coordinates of P1 and P2 and add to LineString
             * coordinates.
             */
            if (geometrySegment.has(ArcgisExtensionsConstantsCommon.UPPERCASE_P1)
                    && (geometrySegment.has(ArcgisExtensionsConstantsCommon.UPPERCASE_P2))) {

                if (k == 0) {
                    final JSONObject point1 = geometrySegment
                        .getJSONObject(ArcgisExtensionsConstantsCommon.UPPERCASE_P1);
                    final double xCoord1 =
                            point1.getDouble(ArcgisExtensionsConstantsCommon.UPPERCASE_X);
                    final double yCoord1 =
                            point1.getDouble(ArcgisExtensionsConstantsCommon.UPPERCASE_Y);
                    final GeoJsonPoint coord1 = new GeoJsonPoint(xCoord1, yCoord1);

                    final JSONObject point2 = geometrySegment
                        .getJSONObject(ArcgisExtensionsConstantsCommon.UPPERCASE_P2);
                    final double xCoord2 =
                            point2.getDouble(ArcgisExtensionsConstantsCommon.UPPERCASE_X);
                    final double yCoord2 =
                            point2.getDouble(ArcgisExtensionsConstantsCommon.UPPERCASE_Y);
                    final GeoJsonPoint coord2 = new GeoJsonPoint(xCoord2, yCoord2);

                    coordinates.add(coord1);
                    coordinates.add(coord2);
                }
                /*
                 * For subsequent segments, get coordinates of P2 and add to linestring coordinates.
                 */
                if (k > 0) {
                    final JSONObject point2 = geometrySegment
                        .getJSONObject(ArcgisExtensionsConstantsCommon.UPPERCASE_P2);
                    final double xCoord2 =
                            point2.getDouble(ArcgisExtensionsConstantsCommon.UPPERCASE_X);
                    final double yCoord2 =
                            point2.getDouble(ArcgisExtensionsConstantsCommon.UPPERCASE_Y);
                    final GeoJsonPoint coord2 = new GeoJsonPoint(xCoord2, yCoord2);

                    coordinates.add(coord2);
                }
            }

        }
        return coordinates;
    }

    /**
     *
     * Create GeoJSON LineString coordinates from ARCHIBUS Line.
     *
     * @param geometryObj The ARCHIBUS geometry object (line).
     * @return The GeoJSON LineString coordinates.
     */
    private static List<GeoJsonPoint> createLineStringCoordinatesFromLine(
            final JSONObject geometryObj) {

        Assert.notNull(geometryObj, GEOMETRY_OBJ_NOT_NULL_MESSAGE);

        final List<GeoJsonPoint> coordinates = new ArrayList<GeoJsonPoint>();

        /*
         * Get the X and Y coordinates for P1 and P2
         */
        final JSONObject point1 =
                geometryObj.getJSONObject(ArcgisExtensionsConstantsCommon.UPPERCASE_P1);
        final double xCoord1 = point1.getDouble(ArcgisExtensionsConstantsCommon.UPPERCASE_X);
        final double yCoord1 = point1.getDouble(ArcgisExtensionsConstantsCommon.UPPERCASE_Y);
        final GeoJsonPoint coord1 = new GeoJsonPoint(xCoord1, yCoord1);

        final JSONObject point2 =
                geometryObj.getJSONObject(ArcgisExtensionsConstantsCommon.UPPERCASE_P2);
        final double xCoord2 = point2.getDouble(ArcgisExtensionsConstantsCommon.UPPERCASE_X);
        final double yCoord2 = point2.getDouble(ArcgisExtensionsConstantsCommon.UPPERCASE_Y);
        final GeoJsonPoint coord2 = new GeoJsonPoint(xCoord2, yCoord2);

        coordinates.add(coord1);
        coordinates.add(coord2);

        return coordinates;
    }

}
