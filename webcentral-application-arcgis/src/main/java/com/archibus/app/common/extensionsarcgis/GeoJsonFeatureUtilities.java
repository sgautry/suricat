package com.archibus.app.common.extensionsarcgis;

import org.json.*;
import org.springframework.util.Assert;

/**
 *
 * Utilities for ArcgisExtensions to work with GeoJSON features.
 *
 * @author knight
 *
 */
public final class GeoJsonFeatureUtilities {

	/**
	 * Constant: assert assetType not null message.
	 */
	private static final String ASSETTYPE_NOT_NULL_ERROR = "assetType must be specified!";

	/**
	 * Constant: assert dwgInfo not present message.
	 */
	private static final String DWGINFO_PROPERTY_MISSING_ERROR = "geoJson must have dwgInfo property!";

	/**
	 * Constant: assert features not present message.
	 */
	private static final String FEATURES_PROPERTY_MISSING_ERROR = "geoJson must have features property!";

	/**
	 * Constant: assert features not empty message.
	 */
	private static final String FEATURES_NOT_EMPTY_ERROR = "geoJson features is empty or null!";

	/**
	 * Constant: assert geoInfo not null message.
	 */
	private static final String GEOINFO_NOT_NULL_ERROR = "geoInfo must be specified!";

	/**
	 * Constant: assert geoJson not null message.
	 */
	private static final String GEOJSON_NOT_NULL_ERROR = "geoJson must be specified!";

	/**
	 * Constant: assert geoJsonPoint not null message.
	 */
	private static final String GEOJSONPOINT_NOT_NULL_ERROR = "geoJsonPoint must be specified!";

	/**
	 * Constant: assert geoJsonFeatures not null message.
	 */
	private static final String GEOJSONFEATURES_NOT_NULL_ERROR = "geoJsonFeatures must be specified!";

	/**
	 * Constant: assert geoSRS not null message.
	 */
	private static final String GEOSRS_NOT_NULL_ERROR = "geoSRS must be specified!";

	/**
	 *
	 * Private default constructor: utility class is non-instantiable.
	 *
	 * * @throws InstantiationException always, since this constructor should
	 * never be called.
	 */
	private GeoJsonFeatureUtilities() throws InstantiationException {
		throw new InstantiationException("Never instantiate " + this.getClass().getName() + "; use static methods!");
	}

	/**
	 *
	 * Check to see that all the georeference parameters exist.
	 *
	 * @param geoInfo
	 *            The georeference parameters to check.
	 * @return True or false to indicate whether all of the georeference
	 *         parameters exist.
	 */
	public static Boolean checkGeoreferenceParams(final JSONObject geoInfo) {
		// assert that geoInfo is not null
		Assert.notNull(geoInfo, GEOINFO_NOT_NULL_ERROR);

		Boolean hasGeoreferenceParams = false;

		// check each required parameter
		int count = 0;
		if (geoInfo.has(ArcgisExtensionsConstantsJsonNames.JSON_GEOX)) {
			count = count + 1;
		}
		if (geoInfo.has(ArcgisExtensionsConstantsJsonNames.JSON_GEOY)) {
			count = count + 1;
		}
		if (geoInfo.has(ArcgisExtensionsConstantsJsonNames.JSON_GEOSCALE)) {
			count = count + 1;
		}
		if (geoInfo.has(ArcgisExtensionsConstantsJsonNames.JSON_GEOROTATE)) {
			count = count + 1;
		}
		if (geoInfo.has(ArcgisExtensionsConstantsJsonNames.JSON_GEOSRS)) {
			count = count + 1;
		}

		if (count == ArcgisExtensionsConstantsCommon.INT_5) {
			hasGeoreferenceParams = true;
		}

		return hasGeoreferenceParams;
	}

	/**
	 *
	 * Check to see that the spatial reference system exists.
	 *
	 * @param geoInfo
	 *            The georeference parameters to check.
	 * @return True or false to indicate whether the spatial reference system
	 *         exists.
	 */
	public static Boolean checkSpatialReferenceSystem(final JSONObject geoInfo) {
		Assert.notNull(geoInfo, GEOINFO_NOT_NULL_ERROR);

		Boolean hasSpatialReference = false;
		if (geoInfo.has(ArcgisExtensionsConstantsJsonNames.JSON_GEOSRS)) {
			hasSpatialReference = true;
		}
		return hasSpatialReference;
	}

	/**
	 *
	 * Create a GeoJSON coordinate reference system object.
	 *
	 * @param geoSRS
	 *            The spatial reference system.
	 * @return The GeoJSON coordinate reference system object.
	 */
	public static JSONObject createCoordinateReferenceSystem(final String geoSRS) {
		// assert that geoSRS has text content
		Assert.hasText(geoSRS, GEOSRS_NOT_NULL_ERROR);

		final JSONObject crsObj = new JSONObject();
		final JSONObject crsName = new JSONObject();
		crsName.put(ArcgisExtensionsConstantsJsonNames.JSON_NAME, geoSRS);
		crsObj.put(ArcgisExtensionsConstantsJsonNames.JSON_TYPE, ArcgisExtensionsConstantsJsonNames.JSON_NAME);
		crsObj.put(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES, crsName);

		return crsObj;
	}

	/**
	 *
	 * Get the drawing info from the GeoJSON.
	 *
	 * @param geoJson
	 *            The GeoJSON.
	 * @return The drawing info.
	 */
	public static JSONObject getDrawingInfoFromCollection(final JSONObject geoJson) {
		// assert that geoJson is not null
		Assert.notNull(geoJson, GEOJSON_NOT_NULL_ERROR);
		// assert that geoJson has dwgInfo property
		Assert.notNull(geoJson.getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES)
				.getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_DWGINFO), DWGINFO_PROPERTY_MISSING_ERROR);

		final JSONObject dwgInfo = geoJson.getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES)
				.getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_DWGINFO);
		return dwgInfo;
	}

	/**
	 *
	 * Get the features from a GeoJSON feature collection.
	 *
	 * @param geoJson
	 *            The GeoJSON collection.
	 * @return The GeoJSON features.
	 */
	public static JSONArray getFeaturesFromCollection(final JSONObject geoJson) {
		// assert that geoJson is not null
		Assert.notNull(geoJson, GEOJSON_NOT_NULL_ERROR);
		// assert that geoJson has features property
		Assert.isTrue(geoJson.has(ArcgisExtensionsConstantsJsonNames.JSON_FEATURES), FEATURES_PROPERTY_MISSING_ERROR);

		final JSONArray geoJsonFeatures = geoJson.getJSONArray(ArcgisExtensionsConstantsJsonNames.JSON_FEATURES);
		// assert that we have at least one feature
		Assert.isTrue(geoJsonFeatures.length() > 0, FEATURES_NOT_EMPTY_ERROR);

		return geoJsonFeatures;
	}

	/**
	 *
	 * Filter a GeoJSON feature list by asset type.
	 *
	 * @param geoJsonFeatures
	 *            The GeoJSON features.
	 * @param assetType
	 *            The ARCHIBUS asset type.
	 * @return Array The filtered GeoJSON assets.
	 */
	public static JSONArray filterFeatures(final JSONArray geoJsonFeatures, final String assetType) {
		Assert.notNull(geoJsonFeatures, GEOJSONFEATURES_NOT_NULL_ERROR);
		Assert.hasLength(assetType, ASSETTYPE_NOT_NULL_ERROR);

		final JSONArray filteredAssetList = new JSONArray();

		for (int i = 0; i < geoJsonFeatures.length(); i++) {
			final JSONObject asset = geoJsonFeatures.getJSONObject(i);
			final JSONObject assetProperties = asset.getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES);
			if (assetProperties.getString(ArcgisExtensionsConstantsTableFieldNames.FIELD_ASSET_TYPE)
					.equals(assetType)) {
				filteredAssetList.put(asset);
			}
		}

		return filteredAssetList;
	}

	/**
	 *
	 * Round the coordinates of a point for Web Mercator (2 decimals) or Lat-Lon
	 * (6 decimals).
	 *
	 * @param geoJsonPoint
	 *            The point with coordinates to round.
	 * @param geoSRS
	 *            The spatial reference system.
	 * @return The point with rounded coordinates.
	 */
	public static GeoJsonPoint roundPointCoordinates(final GeoJsonPoint geoJsonPoint, final String geoSRS) {
		// assert that geoJsonPoint is not null
		Assert.notNull(geoJsonPoint, GEOJSONPOINT_NOT_NULL_ERROR);
		// assert that geoSRS is not null
		Assert.hasText(geoSRS, GEOSRS_NOT_NULL_ERROR);

		double xCoord = 0;
		double yCoord = 0;
		GeoJsonPoint newPoint = null;

		// round coordinates to 2 decimals for web mercator or 6 decimals for
		// lat-lon
		if (geoSRS.equalsIgnoreCase(ArcgisExtensionsConstantsCommon.EPSG_WEB_MERC)) {
			xCoord = Math.round(geoJsonPoint.x * ArcgisExtensionsConstantsCommon.DOUBLE_ONE_HUNDRED_POINT_ZERO)
					/ ArcgisExtensionsConstantsCommon.DOUBLE_ONE_HUNDRED_POINT_ZERO;
			yCoord = Math.round(geoJsonPoint.y * ArcgisExtensionsConstantsCommon.DOUBLE_ONE_HUNDRED_POINT_ZERO)
					/ ArcgisExtensionsConstantsCommon.DOUBLE_ONE_HUNDRED_POINT_ZERO;
			newPoint = new GeoJsonPoint(xCoord, yCoord);
		} else if (geoSRS.equalsIgnoreCase(ArcgisExtensionsConstantsCommon.EPSG_LAT_LON)) {
			xCoord = Math.round(geoJsonPoint.x * ArcgisExtensionsConstantsCommon.DOUBLE_ONE_MILLION_POINT_ZERO)
					/ ArcgisExtensionsConstantsCommon.DOUBLE_ONE_MILLION_POINT_ZERO;
			yCoord = Math.round(geoJsonPoint.y * ArcgisExtensionsConstantsCommon.DOUBLE_ONE_MILLION_POINT_ZERO)
					/ ArcgisExtensionsConstantsCommon.DOUBLE_ONE_MILLION_POINT_ZERO;
			newPoint = new GeoJsonPoint(xCoord, yCoord);
		} else {
			newPoint = geoJsonPoint;
		}

		return newPoint;
	}
}
