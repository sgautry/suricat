package com.archibus.app.common.extensionsarcgis;

import java.util.List;

import org.json.*;
import org.springframework.util.Assert;

/**
 *
 * Provides methods for creating GeoJSON features for ARCHIBUS assets.
 *
 * @author knight
 *
 */
public class GeoJsonFeatureCreator {

    /**
     * Constant: assert coordinates not null message.
     */
    private static final String COORDINATES_NOT_NULL_MESSAGE = "coordinates must be specified!";

    /**
     * Constant: assert coordinates not empty message.
     */
    private static final String COORDINATES_NOT_EMPTY_MESSAGE = "coordinates must not be empty!";

    /**
     * Constant: assert epsg code has length message.
     */
    private static final String EPSG_CODE_NOT_EMPTY_MESSAGE = "epsgCode must be specified!";

    /**
     * Constant: assert features not null message.
     */
    private static final String FEATURES_NOT_NULL_MESSAGE = "features must be specified!";

    /**
     * Constant: assert feature id has length message.
     */
    private static final String FEATURE_ID_HAS_LENGTH_MESSAGE = "featureId must be specified!";

    /**
     * Constant: assert feature properties not null message.
     */
    private static final String FEATURE_PROPERTIES_NOT_NULL_MESSAGE =
            "featureProperties must be specified";

    /**
     * Constant: assert feature type has length message.
     */
    private static final String FEATURE_TYPE_HAS_LENGTH_MESSAGE = "featureType must be specified!";

    /**
     * Constant: assert properties not null message.
     */
    private static final String PROPERTIES_NOT_NULL_MESSAGE = "properties must be specified!";

    /**
     *
     * Create a GeoJSON Point feature.
     *
     * @param featureId The feature id.
     * @param featureProperties The feature properties.
     * @param coordinates The feature coordinate.
     * @return The GeoJSON feature.
     */
    public static final JSONObject createGeoJsonPointFeature(final String featureId,
            final JSONObject featureProperties, final GeoJsonPoint coordinates) {

        Assert.hasLength(featureId, FEATURE_ID_HAS_LENGTH_MESSAGE);
        Assert.notNull(featureProperties, FEATURE_PROPERTIES_NOT_NULL_MESSAGE);
        Assert.notNull(coordinates, COORDINATES_NOT_NULL_MESSAGE);

        final JSONObject geoJsonFeature = new JSONObject();

        geoJsonFeature.put(ArcgisExtensionsConstantsJsonNames.JSON_ID, featureId);
        geoJsonFeature.put(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES, featureProperties);
        geoJsonFeature.put(ArcgisExtensionsConstantsJsonNames.JSON_TYPE,
            ArcgisExtensionsConstantsCommon.FEATURE);

        final JSONObject featureGeometry = createGeoJsonPointFeatureGeometry(
            ArcgisExtensionsConstantsCommon.POINT, coordinates);

        geoJsonFeature.put(ArcgisExtensionsConstantsJsonNames.JSON_GEOMETRY, featureGeometry);

        return geoJsonFeature;
    }

    /**
     *
     * Create a GeoJSON LineString feature.
     *
     * @param featureId The feature id.
     * @param featureProperties The feature properties.
     * @param coordinates The feature coordinates.
     * @return The GeoJSON feature.
     */
    public static final JSONObject createGeoJsonLineStringFeature(final String featureId,
            final JSONObject featureProperties, final List<GeoJsonPoint> coordinates) {

        Assert.hasLength(featureId, FEATURE_ID_HAS_LENGTH_MESSAGE);
        Assert.notNull(featureProperties, FEATURE_PROPERTIES_NOT_NULL_MESSAGE);
        Assert.notEmpty(coordinates, COORDINATES_NOT_EMPTY_MESSAGE);

        final JSONObject geoJsonFeature = new JSONObject();

        geoJsonFeature.put(ArcgisExtensionsConstantsJsonNames.JSON_ID, featureId);
        geoJsonFeature.put(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES, featureProperties);
        geoJsonFeature.put(ArcgisExtensionsConstantsJsonNames.JSON_TYPE,
            ArcgisExtensionsConstantsCommon.FEATURE);

        final JSONObject featureGeometry = createGeoJsonLineStringFeatureGeometry(
            ArcgisExtensionsConstantsCommon.LINESTRING, coordinates);

        geoJsonFeature.put(ArcgisExtensionsConstantsJsonNames.JSON_GEOMETRY, featureGeometry);

        return geoJsonFeature;
    }

    /**
     *
     * Create a GeoJSON Polygon feature.
     *
     * @param featureId The feature id.
     * @param featureProperties The feature properties.
     * @param coordinates The feature coordinates.
     * @return The GeoJSON feature.
     */
    public static final JSONObject createGeoJsonPolygonFeature(final String featureId,
            final JSONObject featureProperties, final JSONArray coordinates) {

        Assert.hasLength(featureId, FEATURE_ID_HAS_LENGTH_MESSAGE);
        Assert.notNull(featureProperties, FEATURE_PROPERTIES_NOT_NULL_MESSAGE);
        Assert.notNull(coordinates, COORDINATES_NOT_NULL_MESSAGE);

        final JSONObject geoJsonFeature = new JSONObject();

        geoJsonFeature.put(ArcgisExtensionsConstantsJsonNames.JSON_ID, featureId);
        geoJsonFeature.put(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES, featureProperties);
        geoJsonFeature.put(ArcgisExtensionsConstantsJsonNames.JSON_TYPE,
            ArcgisExtensionsConstantsCommon.FEATURE);

        final JSONObject featureGeometry =
                createGeoJsonFeatureGeometry(ArcgisExtensionsConstantsCommon.POLYGON, coordinates);

        geoJsonFeature.put(ArcgisExtensionsConstantsJsonNames.JSON_GEOMETRY, featureGeometry);

        return geoJsonFeature;
    }

    /**
     *
     * Create GeoJSON Point feature geometry.
     *
     * @param featureType The GeoJSON feature type.
     * @param coordinates The feature coordinates.
     * @return The GeoJSON feature geometry.
     */
    private static JSONObject createGeoJsonPointFeatureGeometry(final String featureType,
            final GeoJsonPoint coordinates) {

        Assert.hasLength(featureType, FEATURE_TYPE_HAS_LENGTH_MESSAGE);
        Assert.notNull(COORDINATES_NOT_NULL_MESSAGE, COORDINATES_NOT_NULL_MESSAGE);

        final JSONObject featureGeometry = new JSONObject();

        featureGeometry.put(ArcgisExtensionsConstantsJsonNames.JSON_TYPE, featureType);
        featureGeometry.put(ArcgisExtensionsConstantsJsonNames.JSON_COORDINATES, coordinates);

        return featureGeometry;

    }

    /**
     *
     * Create GeoJSON LineString feature geometry.
     *
     * @param featureType The GeoJSON feature type.
     * @param coordinates The feature coordinates.
     * @return The GeoJSON feature geometry.
     */
    private static JSONObject createGeoJsonLineStringFeatureGeometry(final String featureType,
            final List<GeoJsonPoint> coordinates) {

        Assert.hasLength(featureType, FEATURE_TYPE_HAS_LENGTH_MESSAGE);
        Assert.notNull(COORDINATES_NOT_NULL_MESSAGE, COORDINATES_NOT_NULL_MESSAGE);

        final JSONObject featureGeometry = new JSONObject();

        featureGeometry.put(ArcgisExtensionsConstantsJsonNames.JSON_TYPE, featureType);
        featureGeometry.put(ArcgisExtensionsConstantsJsonNames.JSON_COORDINATES, coordinates);

        return featureGeometry;

    }

    /**
     *
     * Create GeoJSON Polygon feature geometry.
     *
     * @param featureType The GeoJSON feature type.
     * @param coordinates The feature coordinates.
     * @return The GeoJSON feature geometry.
     */
    private static JSONObject createGeoJsonFeatureGeometry(final String featureType,
            final JSONArray coordinates) {

        Assert.hasLength(FEATURE_TYPE_HAS_LENGTH_MESSAGE, FEATURE_TYPE_HAS_LENGTH_MESSAGE);
        Assert.notNull(COORDINATES_NOT_NULL_MESSAGE, COORDINATES_NOT_NULL_MESSAGE);

        final JSONObject featureGeometry = new JSONObject();

        featureGeometry.put(ArcgisExtensionsConstantsJsonNames.JSON_TYPE, featureType);
        featureGeometry.put(ArcgisExtensionsConstantsJsonNames.JSON_COORDINATES, coordinates);

        return featureGeometry;

    }

    /**
     *
     * Creates a GeoJSON feature collection.
     *
     * @param epsgCode The coordinate reference system object.
     * @param properties The properties of the feature collection.
     * @param features The features of the feature collection.
     * @return The GeoJSON feature collection;
     */
    public static JSONObject createGeoJsonFeatureCollection(final String epsgCode,
            final JSONObject properties, final JSONArray features) {

        Assert.hasLength(epsgCode, EPSG_CODE_NOT_EMPTY_MESSAGE);
        Assert.notNull(properties, PROPERTIES_NOT_NULL_MESSAGE);
        Assert.notNull(features, FEATURES_NOT_NULL_MESSAGE);

        final JSONObject geoJsonFeatureCollection = new JSONObject();
        final JSONObject crsObj = GeoJsonFeatureUtilities.createCoordinateReferenceSystem(epsgCode);
        geoJsonFeatureCollection.put(ArcgisExtensionsConstantsJsonNames.JSON_CRS, crsObj);
        geoJsonFeatureCollection.put(ArcgisExtensionsConstantsJsonNames.JSON_FEATURES, features);
        geoJsonFeatureCollection.put(ArcgisExtensionsConstantsJsonNames.JSON_PROPERTIES,
            properties);
        geoJsonFeatureCollection.put(ArcgisExtensionsConstantsJsonNames.JSON_TYPE,
            ArcgisExtensionsConstantsCommon.FEATURECOLLECTION);

        return geoJsonFeatureCollection;
    }

}
