package com.archibus.app.common.extensionsarcgis;

import org.json.*;
import org.springframework.util.Assert;

/**
 *
 * Utilities for ArcgisExtensions to work with ArcGIS GeoJSON features.
 *
 * @author knight
 *
 */

public final class ArcgisJsonUtilities {

	/**
	 * Constant: assert assetType not null message.
	 */
	private static final String ASSET_TYPE_NOT_NULL_ERROR = "assetType must be specified!";

	/**
	 * Constant: assert esriFeatures not null message.
	 */
	private static final String ESRI_FEATURES_NOT_NULL_ERROR = "esriFeatures must be specified!";

	/**
	 * Private default constructor: utility class is non-instantiable.
	 *
	 * @throws InstantiationException
	 *             always, since this constructor should never be called.
	 */
	private ArcgisJsonUtilities() throws InstantiationException {
		// Throw an exception if this *is* called.
		throw new InstantiationException("Never instantiate " + this.getClass().getName() + "; use static methods!");
	}

	/**
	 *
	 * Filter an Esri feature list by asset type.
	 *
	 * @param esriFeatures
	 *            The Esri features.
	 * @param assetType
	 *            The ARCHIBUS asset type.
	 * @return Array with filtered Esri features.
	 */
	public static JSONArray filterEsriFeatures(final JSONArray esriFeatures, final String assetType) {
		Assert.notNull(esriFeatures, ESRI_FEATURES_NOT_NULL_ERROR);
		Assert.hasLength(assetType, ASSET_TYPE_NOT_NULL_ERROR);

		final JSONArray filteredFeatures = new JSONArray();

		for (int i = 0; i < esriFeatures.length(); i++) {
			final JSONObject feature = esriFeatures.getJSONObject(i);
			final JSONObject featureAttributes = feature
					.getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_ATTRIBUTES);
			if (featureAttributes.getString(ArcgisExtensionsConstantsTableFieldNames.FIELD_ASSET_TYPE)
					.equals(assetType)) {
				filteredFeatures.put(feature);
			}
		}

		return filteredFeatures;
	}

	/**
	 *
	 * Filter an Esri feature list for Rm and Gros features. Provided as a
	 * convenience to filterEsriFeatuers.
	 *
	 * @param esriFeatures
	 *            Array of Esri features.
	 * @return Array with filtered Esri Rm and Gros features.
	 */
	public static JSONArray filterEsriFeaturesForRmAndGros(final JSONArray esriFeatures) {
		Assert.notNull(esriFeatures, ESRI_FEATURES_NOT_NULL_ERROR);

		final JSONArray spaceFeatures = new JSONArray();

		for (int i = 0; i < esriFeatures.length(); i++) {
			final JSONObject feature = esriFeatures.getJSONObject(i);
			final JSONObject featureAttributes = feature
					.getJSONObject(ArcgisExtensionsConstantsJsonNames.JSON_ATTRIBUTES);
			if (featureAttributes.getString(ArcgisExtensionsConstantsTableFieldNames.FIELD_ASSET_TYPE)
					.equals(ArcgisExtensionsConstantsJsonNames.JSON_RM)
					|| featureAttributes.getString(ArcgisExtensionsConstantsTableFieldNames.FIELD_ASSET_TYPE)
							.equals(ArcgisExtensionsConstantsJsonNames.JSON_GROS)) {
				spaceFeatures.put(feature);
			}
		}

		return spaceFeatures;
	}

}
