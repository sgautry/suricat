package com.aremis.eventhandler;

import java.sql.ResultSet;
import java.util.List;

import org.apache.log4j.Logger;

import sun.security.action.GetLongAction;

import com.archibus.context.ContextStore;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.db.DbConnection;

/**
 * Provides TODO. - if it has behavior (service), or 
 * Represents TODO. - if it has state (entity, domain object, model object). 
 * Utility class. Provides methods TODO.
 * Interface to be implemented by classes that TODO.
 *<p>
 *
 * Used by TODO to TODO.
 * Managed by Spring, has prototype TODO singleton scope. Configured in TODO file.
 *
 * @author n.ledent
 * @since 20.1
 *
 */
public class AremisContractService {
    static Logger log = Logger.getLogger(AremisContractService.class);
    
    public static void updateInformationOfContract() {
        
       final String updateInternalContractant ="BEGIN "+
                                               "For rmloop in (  "+
                                               "                 SELECT COUNT(DISTINCT ar_client.name) AS nbcontractant,MIN(ar_client.name) AS contractant, "+
                                               "                         COUNT(DISTINCT dp.name) AS nbpreneur,MIN(dp.name) AS preneur, "+
                                               "                         MIN(LS.date_start)cont_start,MAX(LS.date_resiliation)cont_end,MIN(ar_contract.cont_id) AS id "+
                                               "                 FROM bl "+
                                               "                 LEFT JOIN ls ON bl.bl_id = ls.bl_id "+
                                               "                 LEFT JOIN ar_contract ON ar_contract.cont_id = ls.cont_id "+
                                               "                 LEFT JOIN dp ON dp.dp_id = ls.tn_name  "+
                                               "                 LEFT JOIN dv ON dv.dv_id = dp.dv_id  "+
                                               "                 LEFT JOIN ar_client ON ar_client.cl_id = dv.cl_id "+
                                               "                 WHERE ls.tn_name IS NOT NULL "+
                                               "                 GROUP BY ls.cont_id "+
                                               "                 UNION ALL "+
                                               "                 SELECT COUNT(DISTINCT ar_tiers_act.name) AS nbcontractant,MIN(ar_tiers_act.name) AS contractant, "+
                                               "                       COUNT(DISTINCT ar_tiers_ens.name) AS nbpreneur,MIN(ar_tiers_ens.name) AS preneur, "+
                                               "                       MIN(LS.date_start)cont_start,MAX(LS.date_resiliation)cont_end,MIN(ar_contract.cont_id) AS id "+
                                               "                FROM bl "+
                                               "                 LEFT JOIN ls ON bl.bl_id = ls.bl_id "+
                                               "                 LEFT JOIN ar_contract ON ar_contract.cont_id = ls.cont_id  "+
                                               "                 LEFT JOIN ar_tiers_ens ON ar_tiers_ens.ens_id = ls.tn_name_ext "+
                                               "                 LEFT JOIN ar_tiers_ssact ON ar_tiers_ssact.ssact_id = ar_tiers_ens.ssact_id "+
                                               "                 LEFT JOIN ar_tiers_act ON ar_tiers_act.act_id = ar_tiers_ssact.act_id "+
                                               "                 LEFT JOIN ar_tiers_rs ON ar_tiers_rs.rs_id = ar_tiers_act.rs_id  "+
                                               "                 WHERE ls.tn_name_ext IS NOT NULL "+
                                               "                 GROUP BY ls.cont_id "+
                                               "                 )  "+
                                               " LOOP "+
                                               "     CASE WHEN  rmloop.nbcontractant = 1 AND  rmloop.nbpreneur = 1 THEN "+
                                               "                UPDATE ar_contract set contractant = rmloop.contractant ,preneur = rmloop.preneur,cont_start=rmloop.cont_start,cont_end=rmloop.cont_end  "+ 
                                               "                WHERE cont_id = rmloop.id; "+
                                               "         WHEN  rmloop.nbcontractant = 1 THEN "+
                                               "                UPDATE ar_contract set contractant = rmloop.contractant ,preneur = '-',cont_start=rmloop.cont_start,cont_end=rmloop.cont_end  "+ 
                                               "                WHERE cont_id = rmloop.id; "+
                                               "         WHEN  rmloop.nbpreneur = 1 THEN "+
                                               "                UPDATE ar_contract set contractant = '-',preneur = rmloop.preneur,cont_start=rmloop.cont_start,cont_end=rmloop.cont_end  "+ 
                                               "                WHERE cont_id = rmloop.id; "+
                                               "     ELSE "+
                                               "         UPDATE ar_contract set cont_start=rmloop.cont_start,cont_end=rmloop.cont_end WHERE cont_id = rmloop.id; "+
                                               "     END CASE; "+
                                               " END LOOP; "+
                                               "   COMMIT; "+
                                               " END;";
        DbConnection.ThreadSafe dbCnx = ContextStore.get().getDbConnection();
        dbCnx.executeUpdate(updateInternalContractant);
        // update contract area based on lease 
        dbCnx.executeUpdate("update ar_contract set cont_area = (select sum(ls.area_negotiated) from ls where ls.cont_id = ar_contract.cont_id)");
    }
    protected static void logDebug(String message){
        if(log.isDebugEnabled()){
            log.debug(message);
        }
    }
    
    public static void generateTable(String valeurdefault){

        DbConnection.ThreadSafe dbCnx = ContextStore.get().getDbConnection();
        if(valeurdefault.equals("I")){
            dbCnx.executeUpdate("UPDATE ar_contract set cont_even = NULL");
        }
        final String transcode =
                "BEGIN "+
                " FOR rmloop IN "+ 
                " (   "+
                " SELECT cont_id,cont_desc,cont_area,cont_start,cont_end,contractant,preneur,cont_actif FROM ar_contract_tmp  "+
                " ) "+
                " LOOP "+
                "         INSERT INTO ar_contract(cont_id,cont_desc,cont_area,cont_start,cont_end,contractant,preneur,cont_actif,cont_even)  "+
                "         SELECT rmloop.cont_id,rmloop.cont_desc,rmloop.cont_area,rmloop.cont_start,rmloop.cont_end,rmloop.contractant,rmloop.preneur,rmloop.cont_actif,'"+valeurdefault+"'"+
                "         FROM DUAL  "+
                "         WHERE NOT EXISTS( SELECT  0 FROM    ar_contract  WHERE cont_id = rmloop.cont_id);  "+
                "         UPDATE ar_contract set cont_id=rmloop.cont_id,cont_desc=rmloop.cont_desc,cont_area = rmloop.cont_area,cont_start=rmloop.cont_start,cont_end=rmloop.cont_end ,"+
                "         contractant = rmloop.contractant,preneur = rmloop.preneur,cont_actif = rmloop.cont_actif, "+
                "         cont_even =  (case when cont_even IS NULL OR cont_even ='"+valeurdefault+"' then '"+valeurdefault+"' else 'R' end)"+
                "         WHERE cont_id = rmloop.cont_id; "+
                "         DELETE FROM ar_contract_tmp WHERE ar_contract_tmp.cont_id = rmloop.cont_id; "+
                " END LOOP; "+
                " COMMIT; "+
                " END;";
         dbCnx.executeUpdate(transcode);     }
    }
    

