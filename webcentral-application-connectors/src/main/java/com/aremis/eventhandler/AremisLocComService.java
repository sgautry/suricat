package com.aremis.eventhandler;
import java.sql.ResultSet;

import com.archibus.context.ContextStore;
import com.archibus.db.DbConnection;

/**
 * Provides TODO. - if it has behavior (service), or 
 * Represents TODO. - if it has state (entity, domain object, model object). 
 * Utility class. Provides methods TODO.
 * Interface to be implemented by classes that TODO.
 *<p>
 *
 * Used by TODO to TODO.
 * Managed by Spring, has prototype TODO singleton scope. Configured in TODO file.
 *
 * @author NicolasLEDENT
 * @since 20.1
 *
 */
public class AremisLocComService {
    public static void transcodification() {
        /*BEGIN 
                FOR rmloop IN 
                (   select AR_LOC_COM_TMP.cont_id,AR_LOC_COM_TMP.uic_id,AR_LOC_COM_TMP.loc_com_id,AR_LOC_COM_TMP.is_pv,AR_LOC_COM_TMP.pv_area,AR_LOC_COM_TMP.pv_ca , ar_site_uic.site_id,ens_id_immo
                    from AR_LOC_COM_TMP
                    INNER JOIN ar_site_uic ON ar_site_uic.uic_id = ar_loc_com_tmp.uic_id
                    INNER JOIN ar_transco_ens   ON ar_transco_ens.ens_id_even = AR_LOC_COM_TMP.ens_id)
                LOOP
                         INSERT INTO ar_loc_com(cont_id,site_id,uic_id,loc_com_id,is_pv,ens_id,pv_area,pv_ca) 
                         SELECT rmloop.cont_id,rmloop.site_id,rmloop.uic_id,rmloop.loc_com_id,rmloop.is_pv,rmloop.ens_id_immo,rmloop.pv_area,rmloop.pv_ca
                         FROM DUAL 
                         WHERE NOT EXISTS( SELECT  0 FROM    ar_loc_com WHERE   loc_com_id = rmloop.loc_com_id AND cont_id = rmloop.cont_id); 
                 END LOOP;
                 COMMIT;
                 END;

*/
        final String transcode =
                " BEGIN "+
                " FOR rmloop IN "+
                "(   select AR_LOC_COM_TMP.cont_id,AR_LOC_COM_TMP.uic_id,AR_LOC_COM_TMP.loc_com_id,AR_LOC_COM_TMP.is_pv,AR_LOC_COM_TMP.pv_area,AR_LOC_COM_TMP.pv_ca , ar_site_uic.site_id,ens_id_immo"+
                "    from AR_LOC_COM_TMP"+
                "    INNER JOIN ar_site_uic ON ar_site_uic.uic_id = ar_loc_com_tmp.uic_id"+
                "    INNER JOIN ar_transco_ens   ON ar_transco_ens.ens_id_even = AR_LOC_COM_TMP.ens_id)"+
                " LOOP"+
                "         INSERT INTO ar_loc_com(cont_id,site_id,uic_id,loc_com_id,is_pv,ens_id,pv_area,pv_ca) "+
                "         SELECT rmloop.cont_id,rmloop.site_id,rmloop.uic_id,rmloop.loc_com_id,rmloop.is_pv,rmloop.ens_id_immo,rmloop.pv_area,rmloop.pv_ca"+
                "         FROM DUAL "+
                "         WHERE NOT EXISTS( SELECT  0 FROM    ar_loc_com WHERE   loc_com_id = rmloop.loc_com_id AND cont_id = rmloop.cont_id); "+
                " END LOOP;"+
                " COMMIT;"+
                " END;";
         DbConnection.ThreadSafe dbCnx = ContextStore.get().getDbConnection();
         dbCnx.executeUpdate(transcode);
     }
}
