package com.aremis.eventhandler;

import java.io.File;
import java.sql.*;
import java.text.*;
import java.util.*;

import com.archibus.context.ContextStore;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;
import com.archibus.db.DbConnection;

public class CustomConnectorProcessing{

    public void archiveFile(String sourceFilePath, String connectorType, String connectorId){
        String archiveDirectoryPath = null;
        List<DataRecord> paramValue = null;
        DataRecord dataRecord = null;
        DataSource actvityParamDS = null;
        String selectSql = null;
        Iterator<DataRecord> iterator = null;
        final String[] tableFields = new String[] {"param_value"};
        actvityParamDS = DataSourceFactory
            .createDataSourceForFields("afm_activity_params", tableFields);
        if(connectorType.equals("CHENE")){
            actvityParamDS.addRestriction(Restrictions.eq("afm_activity_params",
            "param_id", "chene-archived-path"));
        }else if(connectorType.equals("DECISIS")){
            actvityParamDS.addRestriction(Restrictions.eq("afm_activity_params",
                "param_id", "decisis-archived-path"));
        }else{
            actvityParamDS.addRestriction(Restrictions.eq("afm_activity_params",
                "param_id", "otari-archived-path"));
        }
        actvityParamDS.addRestriction(Restrictions.eq("afm_activity_params",
            "activity_id", "AbRPLMPortfolioAdministration"));
        selectSql = actvityParamDS.formatSqlQuery(null, true);
        paramValue = SqlUtils.executeQuery(actvityParamDS.getMainTableName(), tableFields, selectSql);
        if (paramValue.size() > 0) {
            iterator = paramValue.iterator();
            dataRecord = (DataRecord) iterator.next();
            archiveDirectoryPath = dataRecord.getValue("afm_activity_params.param_value").toString();
        }
        if(archiveDirectoryPath.contains("\\")){
            if(archiveDirectoryPath.charAt(archiveDirectoryPath.length()-1)!='\\'){
                archiveDirectoryPath += "\\";
            }
        }
        if(archiveDirectoryPath.contains("/")){
            if(archiveDirectoryPath.charAt(archiveDirectoryPath.length()-1)!='/'){
                archiveDirectoryPath += "/";
            }
        }
        if (! new File(archiveDirectoryPath).exists()){
            new File(archiveDirectoryPath).mkdir();
        }
        File sourceFile = new File(getConnectorFileName(sourceFilePath, connectorId));
        sourceFile.renameTo(new File(archiveDirectoryPath + sourceFile.getName()));
        sourceFile.delete();
    }
    
    public void deactivateAccMgmtRefData(boolean isDeactivateBu){
        String updateSql = null;
        if(isDeactivateBu){
            updateSql = "update bu set active='No' where bu_id not in(select distinct bu_id from ar_temp_chene where bu_id is not null)";
            SqlUtils.executeUpdate("bu", updateSql);
        }
        else{
            updateSql = "update dv set active='No' where dv_id not in(select distinct dv_id from ar_temp_chene where dv_id is not null)";
            SqlUtils.executeUpdate("dv", updateSql);
            updateSql = "update dp set active='No' where dv_id||dp_id not in(select distinct dv_id||dp_id from ar_temp_chene where dp_id is not null and dv_id is not null)";
            SqlUtils.executeUpdate("dp", updateSql);
            updateSql = "delete from ar_temp_chene";
            SqlUtils.executeUpdate("ar_temp_chene", updateSql);
            Connection conn = ContextStore.get().getDbConnection().getConnection();
            try {
                conn.createStatement().execute("alter sequence ar_temp_chene_s restart start with 0");
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        SqlUtils.commit();
    }
    public boolean isConnectorExecutionSucceed(String connectorId){
        List<DataRecord> paramValue = null;
        DataSource connectorParamDS = null;
        String selectSql = null;
        final String[] tableFields = new String[] {"msg"};
        connectorParamDS = DataSourceFactory
            .createDataSourceForFields("afm_conn_log", tableFields);
        connectorParamDS.addRestriction(Restrictions.eq("afm_conn_log",
            "connector_id", connectorId));
        connectorParamDS.addRestriction(Restrictions.sql("to_char(to_date(date_log), 'dd-mm-yy')=to_char(to_date(current_date), 'dd-mm-yy')"));
        connectorParamDS.addRestriction(Restrictions.sql("upper(msg) like '%ERROR%'"));
        selectSql = connectorParamDS.formatSqlQuery(null, true);
        paramValue = SqlUtils.executeQuery(connectorParamDS.getMainTableName(), tableFields, selectSql);
        if (paramValue.size() > 0) {
            return false;
        }
        return true;
    }
    
    public boolean isConnectorExecuted(String connectorId){
        List<DataRecord> paramValue = null;
        DataSource connectorParamDS = null;
        String selectSql = null;
        final String[] tableFields = new String[] {"msg"};
        connectorParamDS = DataSourceFactory
            .createDataSourceForFields("afm_conn_log", tableFields);
        connectorParamDS.addRestriction(Restrictions.eq("afm_conn_log",
            "connector_id", connectorId));
        connectorParamDS.addRestriction(Restrictions.sql("to_char(to_date(date_log), 'dd-mm-yy')=to_char(to_date(current_date), 'dd-mm-yy')"));
        selectSql = connectorParamDS.formatSqlQuery(null, true);
        paramValue = SqlUtils.executeQuery(connectorParamDS.getMainTableName(), tableFields, selectSql);
        if (paramValue.size() > 0) {
            return true;
        }
        return false;
    }
    
    public String getConnectorFileName(String sourceFilePath, String connectorId){
        String fileName = null;
        String sourceFileName = null;
        String sourceDirectoryPath = null;
        String selectedDate = null;
        String selectedFile = null;
        try{
            String connectorType = getConnectorType(connectorId);
            if(connectorType.equals("CHENE")){
                selectedDate = "20150101144000";
            }else{
                selectedDate = "01012015144000";
            }
            File file = new File(sourceFilePath);
            if(sourceFilePath.contains("/")){
                sourceDirectoryPath = sourceFilePath.substring(0,sourceFilePath.lastIndexOf("/"));
                    
            }else{
                sourceDirectoryPath = sourceFilePath.substring(0,sourceFilePath.lastIndexOf("\\"));
            }
            sourceFileName = file.getName();
            File dir = new File(sourceDirectoryPath);
            File[] dir_contents = dir.listFiles();
            for (int i = 0; i < dir_contents.length; i++) {
                fileName = dir_contents[i].getName();
                if(fileName.contains(sourceFileName)){
                    String tempFileName = fileName.substring(0,fileName.lastIndexOf("."));
                    String tempDate = (tempFileName.substring(tempFileName.lastIndexOf("_",tempFileName.lastIndexOf("_")-1))).replaceAll("_","");
                    if(sourceFilePath.contains("/")){
                        fileName=sourceDirectoryPath+"/"+fileName;
                    }else{
                        fileName=sourceDirectoryPath+"\\"+fileName;
                    }
                    if(!compareDateWithoutTimeZones(tempDate,selectedDate,connectorType)){
                        selectedDate = tempDate;
                        selectedFile = fileName;
                    }
                }
            }
        } catch(Exception e){
        }
        if(selectedFile!=null){
            return selectedFile;
        }
        return sourceFilePath;
    }
    
    public String getConnectorType(String connectorId){
        String connectorType = null;
        String connectorsList = null;
        List<DataRecord> paramValue = null;
        DataRecord dataRecord = null;
        DataSource actvityParamDS = null;
        String selectSql = null;
        Iterator<DataRecord> iterator = null;
        final String[] tableFields = new String[] {"param_value"};
        actvityParamDS = DataSourceFactory
            .createDataSourceForFields("afm_activity_params", tableFields);
        actvityParamDS.addRestriction(Restrictions.eq("afm_activity_params",
            "param_id", "suricat-connectors-chene"));
        actvityParamDS.addRestriction(Restrictions.eq("afm_activity_params",
            "activity_id", "AbRPLMPortfolioAdministration"));
        selectSql = actvityParamDS.formatSqlQuery(null, true);
        paramValue = SqlUtils.executeQuery(actvityParamDS.getMainTableName(), tableFields, selectSql);
        if (paramValue.size() > 0) {
            iterator = paramValue.iterator();
            dataRecord = (DataRecord) iterator.next();
            connectorsList = dataRecord.getValue("afm_activity_params.param_value").toString();
        }
        if(connectorsList.contains(connectorId)){
            connectorType = "CHENE";
            return connectorType;
        }
        actvityParamDS = DataSourceFactory
                .createDataSourceForFields("afm_activity_params", tableFields);
            actvityParamDS.addRestriction(Restrictions.eq("afm_activity_params",
                "param_id", "suricat-connectors-decisis"));
            actvityParamDS.addRestriction(Restrictions.eq("afm_activity_params",
                "activity_id", "AbRPLMPortfolioAdministration"));
        selectSql = actvityParamDS.formatSqlQuery(null, true);
        paramValue = SqlUtils.executeQuery(actvityParamDS.getMainTableName(), tableFields, selectSql);
        if (paramValue.size() > 0) {
            iterator = paramValue.iterator();
            dataRecord = (DataRecord) iterator.next();
            connectorsList = dataRecord.getValue("afm_activity_params.param_value").toString();
        }
        if(connectorsList.contains(connectorId)){
            connectorType = "DECISIS";
            return connectorType;
        }
        
        actvityParamDS = DataSourceFactory
                .createDataSourceForFields("afm_activity_params", tableFields);
            actvityParamDS.addRestriction(Restrictions.eq("afm_activity_params",
                "param_id", "suricat-connectors-otari"));
            actvityParamDS.addRestriction(Restrictions.eq("afm_activity_params",
                "activity_id", "AbRPLMPortfolioAdministration"));
        selectSql = actvityParamDS.formatSqlQuery(null, true);
        paramValue = SqlUtils.executeQuery(actvityParamDS.getMainTableName(), tableFields, selectSql);
        if (paramValue.size() > 0) {
            iterator = paramValue.iterator();
            dataRecord = (DataRecord) iterator.next();
            connectorsList = dataRecord.getValue("afm_activity_params.param_value").toString();
        }
        if(connectorsList.contains(connectorId)){
            connectorType = "OTARI";
            return connectorType;
        }
        return connectorType;
    }
    
    public String getDivisionCode(String dpId){
        String dvId = null;
        List<DataRecord> paramValue = null;
        DataRecord dataRecord = null;
        DataSource departmentDS = null;
        String selectSql = null;
        Iterator<DataRecord> iterator = null;
        final String[] tableFields = new String[] {"dv_id"};
        departmentDS = DataSourceFactory
            .createDataSourceForFields("dp", tableFields);
        departmentDS.addRestriction(Restrictions.eq("dp",
            "dp_id", dpId));
        selectSql = departmentDS.formatSqlQuery(null, true);
        paramValue = SqlUtils.executeQuery(departmentDS.getMainTableName(), tableFields, selectSql);
        if (paramValue.size() > 0) {
            iterator = paramValue.iterator();
            dataRecord = (DataRecord) iterator.next();
            dvId = dataRecord.getValue("dp.dv_id").toString();
        }
        return dvId;
    }
    
    public String getAgencyCode(String blAgencyName){
        String agencyId = null;
        List<DataRecord> paramValue = null;
        DataRecord dataRecord = null;
        DataSource departmentDS = null;
        String selectSql = null;
        Iterator<DataRecord> iterator = null;
        final String[] tableFields = new String[] {"ag_id"};
        departmentDS = DataSourceFactory
            .createDataSourceForFields("ar_ag_transco", tableFields);
        departmentDS.addRestriction(Restrictions.eq("ar_ag_transco",
            "bl_ag_name", blAgencyName));
        selectSql = departmentDS.formatSqlQuery(null, true);
        paramValue = SqlUtils.executeQuery(departmentDS.getMainTableName(), tableFields, selectSql);
        if (paramValue.size() > 0) {
            iterator = paramValue.iterator();
            dataRecord = (DataRecord) iterator.next();
            agencyId = dataRecord.getValue("ar_ag_transco.ag_id").toString();
        }
        return agencyId;
    }
    
    public static boolean compareDateWithoutTimeZones(String tempDate,String selectedDate, String connectorType){
        int selectedYear = 0, selectedMonth = 0, selectedDay = 0, selectedHours = 0, selectedMinitues = 0, selectedSeconds = 0;
        int tempYear = 0, tempMonth = 0, tempDay = 0, tempHours = 0,tempMinitues = 0, tempSeconds = 0;
        if(connectorType.equals("CHENE")){
            selectedYear = Integer.parseInt(selectedDate.substring(0,4));
            selectedMonth = Integer.parseInt(selectedDate.substring(4,6));
            selectedDay = Integer.parseInt(selectedDate.substring(6,8));
            selectedHours = Integer.parseInt(selectedDate.substring(8,10));
            selectedMinitues = Integer.parseInt(selectedDate.substring(10,12));
            selectedSeconds = Integer.parseInt(selectedDate.substring(12,14));
            tempYear = Integer.parseInt(tempDate.substring(0,4));
            tempMonth = Integer.parseInt(tempDate.substring(4,6));
            tempDay = Integer.parseInt(tempDate.substring(6,8));
            tempHours = Integer.parseInt(tempDate.substring(8,10));
            tempMinitues = Integer.parseInt(tempDate.substring(10,12));
            tempSeconds = Integer.parseInt(tempDate.substring(12,14));
        }else{
            selectedYear = Integer.parseInt(selectedDate.substring(4,8));
            selectedMonth = Integer.parseInt(selectedDate.substring(2,4));
            selectedDay = Integer.parseInt(selectedDate.substring(0,2));
            selectedHours = Integer.parseInt(selectedDate.substring(8,10));
            selectedMinitues = Integer.parseInt(selectedDate.substring(10,12));
            selectedSeconds = Integer.parseInt(selectedDate.substring(12,14));
            tempYear = Integer.parseInt(tempDate.substring(4,8));
            tempMonth = Integer.parseInt(tempDate.substring(2,4));
            tempDay = Integer.parseInt(tempDate.substring(0,2));
            tempHours = Integer.parseInt(tempDate.substring(8,10));
            tempMinitues = Integer.parseInt(tempDate.substring(10,12));
            tempSeconds = Integer.parseInt(tempDate.substring(12,14));
        }
        
        if(selectedYear > tempYear){
            return true;
        }
        if(selectedYear < tempYear){
            return false;
        }
        if(selectedYear == tempYear){
            if(selectedMonth > tempMonth){
                return true;
            }
            if(selectedMonth < tempMonth){
                return false;
            }
            if(selectedMonth == tempMonth){
                if(selectedDay > tempDay){
                    return true;
                }
                if(selectedDay < tempDay){
                    return false;
                }
                if(selectedDay == tempDay){
                    if(selectedHours > tempHours){
                        return true;
                    }
                    if(selectedHours < tempHours){
                        return false;
                    }
                    if(selectedHours == tempHours){
                        if(selectedMinitues > tempMinitues){
                            return true;
                        }
                        if(selectedMinitues < tempMinitues){
                            return false;
                        }
                        if(selectedMinitues == tempMinitues){
                            if(selectedSeconds > tempSeconds){
                                return true;
                            }
                            if(selectedSeconds < tempSeconds){
                                return false;
                            }                       
                        }                   
                    }
                }
            }       
        }       
        return false;
    }
    
    public String getStateCode(String cityId){
        String stateId = null;
        List<DataRecord> paramValue = null;
        DataRecord dataRecord = null;
        DataSource cityDS = null;
        String selectSql = null;
        Iterator<DataRecord> iterator = null;
        final String[] tableFields = new String[] {"state_id"};
        cityDS = DataSourceFactory
            .createDataSourceForFields("city", tableFields);
        cityDS.addRestriction(Restrictions.eq("city",
            "city_id", cityId));
        selectSql = cityDS.formatSqlQuery(null, true);
        paramValue = SqlUtils.executeQuery(cityDS.getMainTableName(), tableFields, selectSql);
        if (paramValue.size() > 0) {
            iterator = paramValue.iterator();
            dataRecord = (DataRecord) iterator.next();
            stateId = dataRecord.getValue("city.state_id").toString();
        }
        return stateId;
    }
    public void updateInformationOfContract(){
        
    }
    public void deleteNonDecisisRooms(){
        DataSource roomDs = DataSourceFactory.createDataSource();
        String childTable = null,childRec = null, sqlQuery = null;
        String data[] = null;
        int allowNull = 0;
        sqlQuery = "update rm set is_decisis_room = 1 where rtrim(bl_id)||rtrim(fl_id)||rtrim(rm_id) in (select rtrim(bl_id)||rtrim(fl_id)||rtrim(rm_id) from ar_temp_rooms)";
        SqlUtils.executeUpdate("rm", sqlQuery);
        SqlUtils.commit();
        List<DataRecord> roomDataRecord = null;
        try{
            //Compter le nombre de de room a 1. 
            //Si ce nombre est inferieur au nombre autorisé alors ne pas supprimer
            String sql = 
                   " BEGIN "+
                   " FOR rmloop IN(SELECT MAX(locaux_supprime) locaux_supprimer,MAX(nb_max_locaux_supprimer) nb_max_locaux_supprimer FROM( "+
                   "                         SELECT 0 locaux_supprime,param_value nb_max_locaux_supprimer FROM afm_activity_params WHERE param_id='Nombre de locaux supprimable' "+
                   "                         UNION ALL "+
                   "                         SELECT COUNT(*)locaux_supprime,'0' nb_max_locaux_supprimer FROM RM WHERE is_decisis_room !=1) "+
                   "             ) "+
                   "     LOOP "+
                   "         CASE WHEN(rmloop.locaux_supprimer > rmloop.nb_max_locaux_supprimer) THEN  "+
                   "             INSERT INTO AFM_CONN_LOG(CONNECTOR_ID,DATE_LOG,MSG,TIME_LOG,RUN_ID) VALUES('Import DECISIS Room',SYSDATE,' Le nombre de locaux a supprimer est trop important :'||rmloop.locaux_supprimer,SYSDATE,7); "+
                   "         ELSE "+
                   "             DELETE FROM rm WHERE is_decisis_room =0; "+
                   "             INSERT INTO AFM_CONN_LOG(CONNECTOR_ID,DATE_LOG,MSG,TIME_LOG,RUN_ID) VALUES('Import DECISIS Room',SYSDATE,'Nombre de locaux a supprimer :'||rmloop.locaux_supprimer,SYSDATE,7);  "+
                   "         END CASE; "+
                   "     END LOOP; "+
                   "     UPDATE rm set is_decisis_room=0; "+
                   "     DELETE FROM ar_temp_rooms; "+
                   "     COMMIT; "+
                   " END; ";
            DbConnection.ThreadSafe dbCnx = ContextStore.get().getDbConnection();
            dbCnx.executeUpdate(sql);
            Connection conn = ContextStore.get().getDbConnection().getConnection();
            conn.createStatement().execute("alter sequence ar_temp_rooms_s restart start with 0");
            SqlUtils.commit();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void syncDrawingInfo(String connectorId) {
        String sqlQuery="";
        try{
             //log the list of rooms for which an update has been performed in RM 
            sqlQuery="INSERT INTO afm_conn_log (connector_id,run_id,msg) " 
                    + " SELECT '"+connectorId+"',CASE WHEN (SELECT max(run_id) FROM afm_conn_log WHERE connector_id='"+connectorId+"') is null THEN 1 ELSE " 
                    + " (SELECT max(run_id)+1 FROM afm_conn_log WHERE connector_id='"+connectorId+"')END as run_id," 
                    + " 'Room [ ' || rm.bl_id||' | '||rm.fl_id||' | '||rm.rm_id || ' ] is updated with ' || CASE WHEN (rm.ehandle<>rm_temp.ehandle OR rm.ehandle is null) THEN ' [ehandle=' || rm_temp.ehandle || ']' ELSE '' END "
                    + " || CASE WHEN (rm.dwgname<>rm_temp.dwgname OR rm.dwgname is null) THEN ' [ dwgname= ' || rm_temp.dwgname || ']' ELSE '' END as test "
                    + " FROM rm INNER JOIN rm_temp ON rm_temp.bl_id=rm.bl_id AND rm_temp.fl_id=rm.fl_id AND rm_temp.rm_id=rm.rm_id  "
                    + " WHERE ((rm.ehandle<>rm_temp.ehandle OR rm.ehandle is null) OR (rm.dwgname<>rm_temp.dwgname OR rm.dwgname is null)) AND (rm_temp.ehandle is not null OR rm_temp.dwgname is not null)";
            SqlUtils.executeUpdate("afm_conn_log", sqlQuery);
            
            //update ehandle in rm table only,when ehandle is different and not null in rm_temp 
            sqlQuery="UPDATE (SELECT rm.ehandle as OLD, rm_temp.ehandle as NEW " 
                     + " FROM rm INNER JOIN rm_temp ON rm_temp.bl_id=rm.bl_id AND rm_temp.fl_id=rm.fl_id AND rm_temp.rm_id=rm.rm_id "
                     + " WHERE (rm.ehandle<>rm_temp.ehandle OR rm.ehandle is null) AND (rm_temp.ehandle is not null))r "
                     + " SET r.OLD = r.NEW";
            SqlUtils.executeUpdate("rm", sqlQuery);
    
            //update dwgname in rm table only,when dwgname is different and not null in rm_temp 
            sqlQuery="UPDATE (SELECT rm.dwgname as OLD, rm_temp.dwgname as NEW " 
                    + " FROM rm INNER JOIN rm_temp ON rm_temp.bl_id=rm.bl_id AND rm_temp.fl_id=rm.fl_id AND rm_temp.rm_id=rm.rm_id "
                    + " WHERE (rm.dwgname<>rm_temp.dwgname  OR rm.dwgname is null) AND (rm_temp.dwgname is not null))r "
                    + " SET r.OLD = r.NEW";
            SqlUtils.executeUpdate("rm", sqlQuery);
            SqlUtils.commit();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}