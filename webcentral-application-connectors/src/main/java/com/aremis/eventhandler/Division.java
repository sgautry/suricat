package com.aremis.eventhandler;

public class Division {
	private String dvId;
	private String name;
	
	public Division(String dvId, String name) {
		super();
		this.dvId = dvId;
		this.name = name;
	}

    public String getDvId() {
        return dvId;
    }

    public void setDvId(String dvId) {
        this.dvId = dvId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}