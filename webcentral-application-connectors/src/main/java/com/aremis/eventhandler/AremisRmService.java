package com.aremis.eventhandler;

import java.sql.ResultSet;

import com.archibus.context.ContextStore;
import com.archibus.db.DbConnection;

/**
 * Provides TODO. - if it has behavior (service), or 
 * Represents TODO. - if it has state (entity, domain object, model object). 
 * Utility class. Provides methods TODO.
 * Interface to be implemented by classes that TODO.
 *<p>
 *
 * Used by TODO to TODO.
 * Managed by Spring, has prototype TODO singleton scope. Configured in TODO file.
 *
 * @author NicolasLEDENT
 * @since 20.1
 *
 */
public class AremisRmService {

    
    public static void transfertRm(){
        String sql ="BEGIN "+
                        "FOR rmloop IN(select count(*) nb from ar_rm_temp where rm_id||bl_id||fl_id NOT IN(select rm_id||bl_id||fl_id from rm)) "+
                        "LOOP "+
                        "   INSERT INTO AFM_CONN_LOG(CONNECTOR_ID,DATE_LOG,MSG,TIME_LOG,RUN_ID) VALUES('Import DECISIS Room',SYSDATE,' Nombre de locaux a ajouter :'||rmloop.nb,SYSDATE,7); "+
                        "END LOOP; "+
                        "FOR rmloop IN(select count(*) nb from ar_rm_temp where rm_id||bl_id||fl_id IN(select rm_id||bl_id||fl_id from rm)) "+
                        "LOOP "+
                        "    INSERT INTO AFM_CONN_LOG(CONNECTOR_ID,DATE_LOG,MSG,TIME_LOG,RUN_ID) VALUES('Import DECISIS Room',SYSDATE,' Nombre de locaux a mettre a jours :'||rmloop.nb,SYSDATE,7); "+
                        "END LOOP; "+
                        "FOR rmloop IN(select count(*) nb from rm where rm_id||bl_id||fl_id NOT IN(select rm_id||bl_id||fl_id from ar_rm_temp)) "+
                        "LOOP "+
                        "   INSERT INTO AFM_CONN_LOG(CONNECTOR_ID,DATE_LOG,MSG,TIME_LOG,RUN_ID) VALUES('Import DECISIS Room',SYSDATE,' Nombre de locaux a supprimer :'||rmloop.nb,SYSDATE,7); "+
                        "END LOOP; "+
                        "COMMIT;"+ 
                    "END; ";

        DbConnection.ThreadSafe dbCnx = ContextStore.get().getDbConnection();
        dbCnx.executeUpdate(sql);
        sql =    "BEGIN "+
             "   FOR rmloop IN("+
             "   select CASE WHEN (F.rm_id IS NULL)THEN 0 ELSE 1 END exist,A.bl_id,A.fl_id,A.rm_id,B.ls_id,C.rm_std,D.rm_cat,E.rm_type,A.name "+
             "          from ar_rm_temp A "+
             "          LEFT JOIN ls B on A.ls_id = B.ls_id"+ 
             "          LEFT JOIN rmstd C on A.rm_std = C.rm_std "+
             "          LEFT JOIN rmcat D on A.rm_cat = D.rm_cat "+
             "          LEFT JOIN rmtype E on A.rm_type = E.rm_type"+
             "          LEFT JOIN rm F on A.rm_id = F.rm_id AND A.fl_id = F.fl_id AND A.bl_id = F.bl_id"+
             "     "+     
             "          )"+
             "  LOOP"+
             "   CASE WHEN rmloop.exist = 1 THEN"+
             "       update rm set fl_id=rmloop.fl_id,bl_id=rmloop.bl_id,rm_id=rmloop.rm_id,name=rmloop.name,rm_type=rmloop.rm_type,rm_cat=rmloop.rm_cat,rm_std=rmloop.rm_std,ls_id=rmloop.ls_id,is_decisis_room = 1"+
             "       WHERE rm_id =rmloop.rm_id AND fl_id = rmloop.fl_id AND bl_id = rmloop.bl_id;"+
             "   ELSE "+
             "       INSERT INTO rm (fl_id,bl_id,rm_id,name,rm_type,rm_cat,rm_std,ls_id,is_decisis_room ) "+
             "       VALUES(rmloop.fl_id,rmloop.bl_id,rmloop.rm_id,rmloop.name,rmloop.rm_type,rmloop.rm_cat,rmloop.rm_std,rmloop.ls_id,1 );"+
             "       END CASE;"+
                "END LOOP;"+
                "COMMIT;"+
                "END;";

        dbCnx = ContextStore.get().getDbConnection();
        dbCnx.executeUpdate(sql);
         sql = "BEGIN "+
              "  FOR rmloop IN( "+
              "  SELECT MAX(nb_ls_id)-MAX(nb_lsid_native) nb_ls, MAX(nb_rmstd)-MAX(nb_rmstd_native) nb_rmstd,"+
              "  MAX(nb_rm_cat)-MAX(nb_rm_cat_native) nb_rmcat, MAX(nb_rm_type)-MAX(nb_rm_type_native) nb_rmtype"+
              "  "+
              "  FROM("+
              "  SELECT COUNT(*) nb_lsid_native,0 nb_ls_id,0 nb_rmstd_native,0 nb_rmstd,0 nb_rm_cat_native,0 nb_rm_cat,0 nb_rm_type_native,0 nb_rm_type"+
              "  from ar_rm_temp A "+
              "  LEFT JOIN ls B on A.ls_id = B.ls_id "+
              "  LEFT JOIN rmstd C on A.rm_std = C.rm_std "+
              "  LEFT JOIN rmcat D on A.rm_cat = D.rm_cat "+
              "  LEFT JOIN rmtype E on A.rm_type = E.rm_type"+
              "  WHERE A.ls_id is null"+
              "  UNION ALL"+
              "  SELECT 0 nb_lsid_native,COUNT(*) nb_ls_id,0 nb_rmstd_native,0 nb_rmstd,0 nb_rm_cat_native,0 nb_rm_cat,0 nb_rm_type_native,0 nb_rm_type "+
              "  from ar_rm_temp A "+
              "  LEFT JOIN ls B on A.ls_id = B.ls_id "+
              "  LEFT JOIN rmstd C on A.rm_std = C.rm_std "+
              "  LEFT JOIN rmcat D on A.rm_cat = D.rm_cat "+
              "  LEFT JOIN rmtype E on A.rm_type = E.rm_type"+
              "  WHERE B.ls_id is null"+
              "  UNION ALL "+
              "  SELECT 0 nb_lsid_native,0 nb_ls_id,COUNT(*)  nb_rmstd_native,0 nb_rmstd,0 nb_rm_cat_native,0 nb_rm_cat,0 nb_rm_type_native,0 nb_rm_type "+
              "  from ar_rm_temp A "+
              "  LEFT JOIN ls B on A.ls_id = B.ls_id "+
              "  LEFT JOIN rmstd C on A.rm_std = C.rm_std "+
              "  LEFT JOIN rmcat D on A.rm_cat = D.rm_cat  "+
              "  LEFT JOIN rmtype E on A.rm_type = E.rm_type "+
              "  WHERE A.rm_std is null "+
              "  UNION ALL "+
              "  SELECT 0 nb_lsid_native,0 nb_ls_id,0 nb_rmstd_native,COUNT(*)  nb_rmstd,0 nb_rm_cat_native,0 nb_rm_cat,0 nb_rm_type_native,0 nb_rm_type "+
              "  from ar_rm_temp A "+
              "  LEFT JOIN ls B on A.ls_id = B.ls_id "+
              "  LEFT JOIN rmstd C on A.rm_std = C.rm_std "+
              "  LEFT JOIN rmcat D on A.rm_cat = D.rm_cat "+
              "  LEFT JOIN rmtype E on A.rm_type = E.rm_type "+
              "  WHERE C.rm_std is null "+
              "  UNION ALL "+
              "  SELECT 0 nb_lsid_native,0 nb_ls_id,0 nb_rmstd_native,0 nb_rmstd,COUNT(*)  nb_rm_cat_native,0 nb_rm_cat,0 nb_rm_type_native,0 nb_rm_type "+
              "  from ar_rm_temp A "+
              "  LEFT JOIN ls B on A.ls_id = B.ls_id "+
              "  LEFT JOIN rmstd C on A.rm_std = C.rm_std "+
              "  LEFT JOIN rmcat D on A.rm_cat = D.rm_cat "+
              "  LEFT JOIN rmtype E on A.rm_type = E.rm_type "+
              "  WHERE A.rm_cat is null "+
              "  UNION ALL "+
              "  SELECT 0 nb_lsid_native,0 nb_ls_id,0 nb_rmstd_native,0 nb_rmstd,0 nb_rm_cat_native,COUNT(*)  nb_rm_cat,0 nb_rm_type_native,0 nb_rm_type "+
              "  from ar_rm_temp A "+
              "  LEFT JOIN ls B on A.ls_id = B.ls_id "+
              "  LEFT JOIN rmstd C on A.rm_std = C.rm_std "+
              "  LEFT JOIN rmcat D on A.rm_cat = D.rm_cat "+
              "  LEFT JOIN rmtype E on A.rm_type = E.rm_type "+
              "  WHERE D.rm_cat is null "+
              "  UNION ALL"+
              "  SELECT 0 nb_lsid_native,0 nb_ls_id,0 nb_rmstd_native,0 nb_rmstd,0 nb_rm_cat_native,0 nb_rm_cat,COUNT(*) nb_rm_type_native,0 nb_rm_type "+
              "  from ar_rm_temp A "+
              "  LEFT JOIN ls B on A.ls_id = B.ls_id "+
              "  LEFT JOIN rmstd C on A.rm_std = C.rm_std "+
              "  LEFT JOIN rmcat D on A.rm_cat = D.rm_cat "+
              "  LEFT JOIN rmtype E on A.rm_type = E.rm_type"+
              "  WHERE A.rm_type is null"+
              "  UNION ALL"+
              "  SELECT 0 nb_lsid_native,0 nb_ls_id,0 nb_rmstd_native,0 nb_rmstd,0 nb_rm_cat_native,0  nb_rm_cat,0 nb_rm_type_native,COUNT(*) nb_rm_type "+
              "  from ar_rm_temp A "+
              "  LEFT JOIN ls B on A.ls_id = B.ls_id "+
              "  LEFT JOIN rmstd C on A.rm_std = C.rm_std "+
              "  LEFT JOIN rmcat D on A.rm_cat = D.rm_cat "+
              "  LEFT JOIN rmtype E on A.rm_type = E.rm_type"+
              "  WHERE E.rm_type is null))"+
              "  LOOP"+
              "      INSERT INTO AFM_CONN_LOG(CONNECTOR_ID,DATE_LOG,MSG,TIME_LOG,RUN_ID) VALUES('Import DECISIS Room',SYSDATE,' Nombre de locaux avec un code bail incorrect :'||rmloop.nb_ls,SYSDATE,7);"+
              "      INSERT INTO AFM_CONN_LOG(CONNECTOR_ID,DATE_LOG,MSG,TIME_LOG,RUN_ID) VALUES('Import DECISIS Room',SYSDATE,' Nombre de locaux avec un rmstd incorrect :'||rmloop.nb_rmstd,SYSDATE,7);"+
              "      INSERT INTO AFM_CONN_LOG(CONNECTOR_ID,DATE_LOG,MSG,TIME_LOG,RUN_ID) VALUES('Import DECISIS Room',SYSDATE,' Nombre de locaux avec une categorie incorrecte :'||rmloop.nb_rmcat,SYSDATE,7);"+
              "      INSERT INTO AFM_CONN_LOG(CONNECTOR_ID,DATE_LOG,MSG,TIME_LOG,RUN_ID) VALUES('Import DECISIS Room',SYSDATE,' Nombre de locaux avec un type incorrect :'||rmloop.nb_rmtype,SYSDATE,7);"+
              "  END LOOP;"+
              "  COMMIT;"+
              "  END;";

        dbCnx = ContextStore.get().getDbConnection();
        dbCnx.executeUpdate(sql);
    }
}

