package com.aremis.eventhandler;
import java.text.*;
import java.util.*;

import com.archibus.app.common.connectors.domain.ConnectorFieldConfig;
import com.archibus.app.common.connectors.exception.ConfigurationException;
import com.archibus.app.common.connectors.impl.archibus.translation.record.IRecordTranslator;
import com.archibus.app.common.connectors.translation.exception.TranslationException;

public class CustomConnectorRules implements IRecordTranslator{
    
    private String connectorId;
    public boolean requiresExistingValue(){
        return true;
    }
    public void applyRule(Map<String, Object> record, Map<String, Object> originalRecord)
        throws TranslationException{
        
        if(connectorId.equals("Import DECISIS Room")){
            if(record.get("SURFACE_DEVELOPPEE_LOCAL_BATIMENT")!= null){
                record.put("SURFACE_DEVELOPPEE_LOCAL_BATIMENT", (record.get("SURFACE_DEVELOPPEE_LOCAL_BATIMENT").toString()).trim());
            }
        }
        
        if(connectorId.equals("Import CHENE DV") || connectorId.equals("Import CHENE RG")){
            if(record.get("STATUT")!=null){
                if(record.get("STATUT").toString().equalsIgnoreCase("I")){
                   record.put("STATUT", "No"); 
                }
                if(record.get("STATUT").toString().equalsIgnoreCase("A")){
                    record.put("STATUT", "Yes"); 
                }
            }
        }
        
        if(connectorId.equals("Import DECISIS Lease")){
            if(record.get("CODE_DONNEUR")!= null){
                String dvId = new CustomConnectorProcessing().getDivisionCode(record.get("CODE_DONNEUR").toString());
                if(dvId!=null && dvId!=""){
                    record.put("CODE_DIVISION", new CustomConnectorProcessing().getDivisionCode(record.get("CODE_DONNEUR").toString()));
                }else{
                    record.put("CODE_DONNEUR", null);
                }
            }
            if(record.get("CODE_PRENEUR_INTERNE")!= null){
                String dvId = new CustomConnectorProcessing().getDivisionCode(record.get("CODE_PRENEUR_INTERNE").toString());
                if(dvId!=null && dvId!=""){
                    record.put("CODE_DIVISION_TN", new CustomConnectorProcessing().getDivisionCode(record.get("CODE_PRENEUR_INTERNE").toString()));
                }else{
                    record.put("CODE_PRENEUR_INTERNE", null);
                }
            }
            if(record.get("SURFACE_DEVELOPPEE_BAIL")!= null){
                record.put("SURFACE_DEVELOPPEE_BAIL", (record.get("SURFACE_DEVELOPPEE_BAIL").toString()).trim());
            }
            if(record.get("AVENANT")!= null){
                record.put("AVENANT", (record.get("AVENANT").toString().replace('.', ' ')).trim());
            }
            String tempDate = null;
            if(record.get("DATE_DEBUT_BAIL")!= null && !record.get("DATE_DEBUT_BAIL").toString().contains("-")
                    && !record.get("DATE_DEBUT_BAIL").toString().isEmpty()){
                tempDate = record.get("DATE_DEBUT_BAIL").toString().trim();
                record.put("DATE_DEBUT_BAIL", getConvertedDate(tempDate));
            }
            if(record.get("DATE_FIN_BAIL")!= null && !record.get("DATE_FIN_BAIL").toString().contains("-")
                    && !record.get("DATE_FIN_BAIL").toString().isEmpty()){
                tempDate = record.get("DATE_FIN_BAIL").toString().trim();
                record.put("DATE_FIN_BAIL", getConvertedDate(tempDate));
            }
            if(record.get("DATE_RESILIATION_BAIL")!= null && !record.get("DATE_RESILIATION_BAIL").toString().contains("-")
                    && !record.get("DATE_RESILIATION_BAIL").toString().isEmpty()){
                tempDate = record.get("DATE_RESILIATION_BAIL").toString().trim();
                record.put("DATE_RESILIATION_BAIL", getConvertedDate(tempDate));
            }
            if(record.get("CATEGORIE_BAIL")!= null){
                String lsCat = null;
                lsCat = record.get("CATEGORIE_BAIL").toString();
                if(lsCat.contains("Non") && lsCat.contains("pour GC")){
                    record.put("CATEGORIE_BAIL", "No regulates");
                }else{
                    if(lsCat.contains("pour GC")){
                        record.put("CATEGORIE_BAIL", "Regulates");
                    }
                }
            } 
        }
        
        if(connectorId.equals("Import CHENE RG")){
            if(record.get("CODE_RG")!= null){
                if(new CustomConnectorProcessing().getDivisionCode(record.get("CODE_RG").toString())== null){
                    record.put("CODE_RG", null);
                }else{
                    record.put("CODE_DIVISION", new CustomConnectorProcessing().getDivisionCode(record.get("CODE_RG").toString()));
                }
            }
        }
         
        if(connectorId.equals("Import DECISIS Building")){
            if(record.get("SURFACE_DEVELOPPEE_BIEN")!= null){
                record.put("SURFACE_DEVELOPPEE_BIEN", (record.get("SURFACE_DEVELOPPEE_BIEN").toString()).trim());
            }
            if(record.get("ENTITE_REGIONALE")!= null){
                record.put("BL_IS_GC", (record.get("ENTITE_REGIONALE").toString()).startsWith("AG") ? "Yes" : "No");
                record.put("ENTITE_REGIONALE", new CustomConnectorProcessing().getAgencyCode(record.get("ENTITE_REGIONALE").toString()));
            }else {
                record.remove("BL_IS_GC");
            }
            String tempDate = null;
            if(record.get("DATE_FIN_EXISTENCE_BIEN")!= null && !record.get("DATE_FIN_EXISTENCE_BIEN").toString().contains("-")
                    && !record.get("DATE_FIN_EXISTENCE_BIEN").toString().isEmpty()){
                tempDate = record.get("DATE_FIN_EXISTENCE_BIEN").toString().trim();
                record.put("DATE_FIN_EXISTENCE_BIEN", getConvertedDate(tempDate));
            }
            if(record.get("DATE_DEBUT_EXISTENCE_BIEN")!= null && !record.get("DATE_DEBUT_EXISTENCE_BIEN").toString().contains("-")
                    && !record.get("DATE_DEBUT_EXISTENCE_BIEN").toString().isEmpty()){
                tempDate = record.get("DATE_DEBUT_EXISTENCE_BIEN").toString().trim();
                record.put("DATE_DEBUT_EXISTENCE_BIEN", getConvertedDate(tempDate));
            }
        }
        if(connectorId.equals("Import DECISIS Site")){
            if(record.get("CODE_REGION_ADM_UT")==null || record.get("CODE_REGION_ADM_UT").toString().isEmpty()){
                record.put("CODE_REGION_ADM_UT", null);
            }
            if(record.get("CODE_INSEE_UT")!=null){
                record.put("CODE_DEPARTEMENT_UT", new CustomConnectorProcessing().getStateCode(record.get("CODE_INSEE_UT").toString()));
            }
        }
       
    }
   
    @Override
    public void init(ConnectorFieldConfig connectorField) throws ConfigurationException {
        this.connectorId = connectorField.getConnectorId();
    }

    public String getConvertedDate(String inputDate){
        if(inputDate.contains("_")){
            return inputDate;
        }
        String convertedDate = null;
        if(!inputDate.contains("-")){
            convertedDate = inputDate.substring(4,8)+"-"+inputDate.substring(2,4)+"-"+inputDate.substring(0,2);
        }
        return convertedDate;
    }
}