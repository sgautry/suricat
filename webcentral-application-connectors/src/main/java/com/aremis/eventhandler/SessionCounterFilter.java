package com.aremis.eventhandler;
import java.io.IOException;
import java.sql.*;
import java.sql.Date;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import org.springframework.security.core.context.SecurityContextImpl;

/**
 * This Class catch all user logins and logouts and write this info to DB (excepting session timeout)
 * 
 * @author Ghita Marina
 *
 */
public class SessionCounterFilter implements Filter {

    private Map<String,String> activeUserSessions;
    private boolean sessionsAreClosed;
    private static final String sessionTable = "afm_concurent_users";

    public void init(FilterConfig config) {
        this.activeUserSessions = (Map<String,String>)config.getServletContext().getAttribute("userSessions");
        sessionsAreClosed = false;
    }

    public void destroy() {
        this.activeUserSessions = null;
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpSession session = httpRequest.getSession();
        String sessionId = session.getId();
        boolean userIsAuthenticable = false;
        SecurityContextImpl springContext = (SecurityContextImpl)session.getAttribute("SPRING_SECURITY_CONTEXT");
        if(springContext != null && springContext.getAuthentication() != null && !activeUserSessions.containsKey(sessionId)){
            userIsAuthenticable = true;
        }

        chain.doFilter(request, response);
            
        if (springContext != null && springContext.getAuthentication() != null && userIsAuthenticable) {
            
            if(!sessionsAreClosed){
                // se inchid sesiunile fara logout date (logout date/time = login date/time + session timeout)(aparute din restarturi fortate de exemplu...)
                int sessionTimeOut = session.getMaxInactiveInterval();
                String updSql = "update " + sessionTable +" set logout_time = TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SS')  where logout_time is null";              
                ExternalDAO.executeUpdate(updSql);
                sessionsAreClosed = true;
            }
            //login
            String user = springContext.getAuthentication().getName().toUpperCase();
            activeUserSessions.put(sessionId, user);
            Map<String,Object> record = new HashMap<String,Object>();
                record.put("user_name", user);
                record.put("session_id", sessionId);
                record.put("concurent_users", activeUserSessions.size());
                record.put("remote_ip", httpRequest.getRemoteAddr());
            ExternalDAO.saveRecord(sessionTable, record);
            if(ExternalDAO.getLog().isDebugEnabled()){
                ExternalDAO.getLog().debug("USER "+user+" login is saved to "+sessionTable+" table");
                ExternalDAO.getLog().debug("USERS logged in: "+activeUserSessions);
            }
        }else{
            if((springContext == null || springContext.getAuthentication() == null) && activeUserSessions.containsKey(sessionId)){//logout
                String reqSql = "select login_id from " + sessionTable + " where session_id = '" + sessionId + "' and logout_time is null";      
                
                Map<String,Object> record = ExternalDAO.getFirstRecord(reqSql);
                    if(record != null){ //save logout into database
                        String updSql = "update " + sessionTable +" set logout_time = TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SS')  where login_id ="+Integer.parseInt((String)(record.get("login_id")));              
                        ExternalDAO.executeUpdate(updSql);
   
                    }else{
                        // logout already saved, so do nothing
                    }
                if(ExternalDAO.getLog().isDebugEnabled()){
                    ExternalDAO.getLog().debug("USER "+activeUserSessions.get(sessionId)+" logout is saved to "+sessionTable+" table");
                    activeUserSessions.remove(sessionId);
                    ExternalDAO.getLog().debug("USERS logged in: "+activeUserSessions);
                }else{
                    activeUserSessions.remove(sessionId);
                }                
            }
        }
    }
}