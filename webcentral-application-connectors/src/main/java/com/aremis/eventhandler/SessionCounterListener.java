package com.aremis.eventhandler;

import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;
/**
 * This Class handle session timeout and record logout for users. Also initialize some variables.
 * 
 * @author Ghita Marina
 *
 */
public class SessionCounterListener implements HttpSessionAttributeListener, ServletContextListener {

    private Map<String,String> activeUserSessions;
    private static final String sessionTable = "afm_concurent_users";
    
    public void contextDestroyed(ServletContextEvent arg0) {}

    public void contextInitialized(ServletContextEvent contextInitEvt) {
        activeUserSessions = new HashMap<String,String>();
        contextInitEvt.getServletContext().setAttribute("userSessions", activeUserSessions);
    }
    
    public void attributeAdded(HttpSessionBindingEvent addEvent) {
        
    }
    
    public void attributeReplaced(HttpSessionBindingEvent replaceEvent) {
        
    }

    public void attributeRemoved(HttpSessionBindingEvent removeEvent) {
        if(removeEvent.getName() == "SPRING_SECURITY_CONTEXT"){
            String sessionId = removeEvent.getSession().getId();
            if(activeUserSessions.containsKey(sessionId)){//logout
                Calendar cal = Calendar.getInstance();
                String strDate = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DATE);
                String strTime = cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);
                String updSql = "update " + sessionTable + " set logout_time = TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SS') where session_id = '" + sessionId + "' and logout_time is null";              
                ExternalDAO.executeUpdate(updSql);
                if(ExternalDAO.getLog().isDebugEnabled()){
                    ExternalDAO.getLog().debug("USER "+activeUserSessions.get(sessionId)+" session timeout logout is saved to "+sessionTable+" table");
                    activeUserSessions.remove(sessionId);
                    ExternalDAO.getLog().debug("USERS logged in: "+activeUserSessions);
                }else{
                    activeUserSessions.remove(sessionId);
                }
            }
        }
    }
}
