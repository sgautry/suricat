package com.aremis.eventhandler;

import com.archibus.app.common.connectors.domain.ConnectorConfig;
import com.archibus.app.common.connectors.exception.ConfigurationException;
import com.archibus.app.common.connectors.service.ConnectorJob;
 
public final class CustomConnectorUtil {

    /**
     * Private default constructor: utility class is non-instantiable.
     */
    static boolean isDeactivateBu = false;
    private CustomConnectorUtil() {}
    
    public static void postprocessEPICBU(final ConnectorConfig connector) throws ConfigurationException {
        CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
        customConnectorProcessing.archiveFile(connector.getConnStringDb(), customConnectorProcessing.getConnectorType(connector.getConnectorId()), connector.getConnectorId());
        isDeactivateBu = true;
        customConnectorProcessing.deactivateAccMgmtRefData(isDeactivateBu);
        new ConnectorJob().executeConnector("Import CHENE_ArbreMDG DV");
    }

    public static void postprocessTreeDV(final ConnectorConfig connector) throws ConfigurationException {
       new ConnectorJob().executeConnector("Import CHENE_ArbreMDG RG");
    }
    
    public static void postprocessTreeRG(final ConnectorConfig connector) throws ConfigurationException {
        CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
        customConnectorProcessing.archiveFile(connector.getConnStringDb(), customConnectorProcessing.getConnectorType(connector.getConnectorId()), connector.getConnectorId());
        isDeactivateBu = false;
        customConnectorProcessing.deactivateAccMgmtRefData(isDeactivateBu);
        new ConnectorJob().executeConnector("Import CHENE DV");
    }
    
    public static void postprocessDV(final ConnectorConfig connector) throws ConfigurationException {
        CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
        customConnectorProcessing.archiveFile(connector.getConnStringDb(), customConnectorProcessing.getConnectorType(connector.getConnectorId()), connector.getConnectorId());
        try {
            new ExceptionalDivisions().generateXLSX();
        } catch (Exception e) {
            e.printStackTrace();
        }
        new ConnectorJob().executeConnector("Import CHENE RG");
    }
    
    public static void postprocessRG(final ConnectorConfig connector) throws ConfigurationException {
        CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
        customConnectorProcessing.archiveFile(connector.getConnStringDb(), customConnectorProcessing.getConnectorType(connector.getConnectorId()), connector.getConnectorId());
        
        //Update date connector        
        AremisJsonDataService.generateConnectorUpdateDate("chene");

    }
    
    public static void postprocessRoles(final ConnectorConfig connector) throws ConfigurationException {
        CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
        customConnectorProcessing.archiveFile(connector.getConnStringDb(), customConnectorProcessing.getConnectorType(connector.getConnectorId()), connector.getConnectorId());
        new ConnectorJob().executeConnector("Import DECISIS Activity");
    }
    
    public static void postprocessActivity(final ConnectorConfig connector) throws ConfigurationException {
        CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
        customConnectorProcessing.archiveFile(connector.getConnStringDb(), customConnectorProcessing.getConnectorType(connector.getConnectorId()), connector.getConnectorId());
        new ConnectorJob().executeConnector("Import DECISIS Sub Activity");
    }
    
    public static void postprocessSubActivity(final ConnectorConfig connector) throws ConfigurationException {
        CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
        customConnectorProcessing.archiveFile(connector.getConnStringDb(), customConnectorProcessing.getConnectorType(connector.getConnectorId()), connector.getConnectorId());
        new ConnectorJob().executeConnector("Import DECISIS ENS");
    }
    
    public static void postprocessENS(final ConnectorConfig connector) throws ConfigurationException {
        CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
        customConnectorProcessing.archiveFile(connector.getConnStringDb(), customConnectorProcessing.getConnectorType(connector.getConnectorId()), connector.getConnectorId());

        //Update date connector        
        AremisJsonDataService.generateConnectorUpdateDate("tiers");
    }
    
    public static void postprocessContract(final ConnectorConfig connector) throws ConfigurationException {
        CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
        customConnectorProcessing.archiveFile(connector.getConnStringDb(), customConnectorProcessing.getConnectorType(connector.getConnectorId()), connector.getConnectorId());
        AremisContractService.generateTable("I");
        new ConnectorJob().executeConnector("Import DECISIS Lease");
        AremisJsonDataService.generateConnectorUpdateDate("contrat");
    }
    
    public static void postprocessLease(final ConnectorConfig connector) throws ConfigurationException {
        CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
        customConnectorProcessing.archiveFile(connector.getConnStringDb(), customConnectorProcessing.getConnectorType(connector.getConnectorId()), connector.getConnectorId());
        AremisLeaseService.updateBuildingId();
        AremisContractService.updateInformationOfContract();
        new ConnectorJob().executeConnector("Import DECISIS Operation");
        AremisJsonDataService.generateConnectorUpdateDate("bail");
    }
    public static void postprocessOperation(final ConnectorConfig connector) throws ConfigurationException {
        CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
        customConnectorProcessing.archiveFile(connector.getConnStringDb(), customConnectorProcessing.getConnectorType(connector.getConnectorId()), connector.getConnectorId());

        //Update date connector        
    }
    public static void postprocessSite(final ConnectorConfig connector) throws ConfigurationException {
        CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
        customConnectorProcessing.archiveFile(connector.getConnStringDb(), customConnectorProcessing.getConnectorType(connector.getConnectorId()), connector.getConnectorId());
        new ConnectorJob().executeConnector("Import DECISIS Building");
    }
    
    public static void postprocessBuilding(final ConnectorConfig connector) throws ConfigurationException {
        CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
        customConnectorProcessing.archiveFile(connector.getConnStringDb(), customConnectorProcessing.getConnectorType(connector.getConnectorId()), connector.getConnectorId());
        new ConnectorJob().executeConnector("Import DECISIS Floor");
    }
    
    public static void postprocessFloor(final ConnectorConfig connector) throws ConfigurationException {
        CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
        customConnectorProcessing.archiveFile(connector.getConnStringDb(), customConnectorProcessing.getConnectorType(connector.getConnectorId()), connector.getConnectorId());
        new ConnectorJob().executeConnector("Import DECISIS Room");
    }
    
    public static void postprocessRoom(final ConnectorConfig connector) throws ConfigurationException {
        CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
        customConnectorProcessing.archiveFile(connector.getConnStringDb(), customConnectorProcessing.getConnectorType(connector.getConnectorId()), connector.getConnectorId());
        AremisRmService.transfertRm();
       customConnectorProcessing.deleteNonDecisisRooms();
        AremisLeaseService.arLsUicGen();
        //Update date connector        
        AremisJsonDataService.generateConnectorUpdateDate("patrimoine");
        customConnectorProcessing.syncDrawingInfo(connector.getConnectorId());
    }
    
    /* OTARI connector ---- */

    public static void postprocessEvenContract(final ConnectorConfig connector) throws ConfigurationException {
        CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
        customConnectorProcessing.archiveFile(connector.getConnStringDb(), customConnectorProcessing.getConnectorType(connector.getConnectorId()), connector.getConnectorId());
        AremisContractService.generateTable("E");
        new ConnectorJob().executeConnector("Import OTARI Local Commercial");
    }

    public static void postprocessLocCom(final ConnectorConfig connector) throws ConfigurationException {
        CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
        customConnectorProcessing.archiveFile(connector.getConnStringDb(), customConnectorProcessing.getConnectorType(connector.getConnectorId()), connector.getConnectorId());
        AremisLocComService.transcodification();
        new ConnectorJob().executeConnector("Import OTARI Operation");
    }

    public static void postprocessEvenOperation(final ConnectorConfig connector) throws ConfigurationException {
        CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
        customConnectorProcessing.archiveFile(connector.getConnStringDb(), customConnectorProcessing.getConnectorType(connector.getConnectorId()), connector.getConnectorId());

    }

    public static void postprocessFactuInt(final ConnectorConfig connector) throws ConfigurationException {
        CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
        customConnectorProcessing.archiveFile(connector.getConnStringDb(), customConnectorProcessing.getConnectorType(connector.getConnectorId()), connector.getConnectorId());
        AremisJsonDataService.generateConnectorUpdateDate("external_rental");
    }

    public static void postprocessFactuExt(final ConnectorConfig connector) throws ConfigurationException {
        CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
        customConnectorProcessing.archiveFile(connector.getConnStringDb(), customConnectorProcessing.getConnectorType(connector.getConnectorId()), connector.getConnectorId());
        new ConnectorJob().executeConnector("Import OTARI Facturation interne");
    }
    
    public static void postprocessChiffreAffaire(final ConnectorConfig connector) throws ConfigurationException{
        CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
        customConnectorProcessing.archiveFile(connector.getConnStringDb(), customConnectorProcessing.getConnectorType(connector.getConnectorId()), connector.getConnectorId());
        new ConnectorJob().executeConnector("Import OTARI Facturation externe");
    }
}