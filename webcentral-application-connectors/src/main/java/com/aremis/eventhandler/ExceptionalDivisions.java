package com.aremis.eventhandler;

import java.io.*;
import java.text.*;
import java.util.*;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;

import com.archibus.context.ContextStore;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.*;
import com.archibus.eventhandler.EventHandlerBase;
import com.archibus.eventhandler.ehs.EhsNotificationMessage;
import com.archibus.jobmanager.EventHandlerContext;
import com.archibus.utility.StringUtil;

public class ExceptionalDivisions extends EventHandlerBase{
    protected static Logger Classlog = Logger.getLogger(ExceptionalDivisions.class);
    private static final String ACTIVITY_ID = "AbSystemAdministration";
    
    public void writeExcel(List<Division> requestList, String excelFilePath) throws IOException {

        @SuppressWarnings("resource")
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet();
    
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        cellStyle.setWrapText(true);
        cellStyle.setVerticalAlignment(CellStyle.VERTICAL_TOP);
        cellStyle.setAlignment(CellStyle.ALIGN_LEFT);
        createHeaderRow(sheet);

        sheet.setColumnWidth(0, 7000);
        sheet.setColumnWidth(1, 15000);
        
        int rowCount = 0;
        for (Division request : requestList) {
            Row row = sheet.createRow(++rowCount);
            writeBook(request, row, cellStyle);
        }

        try {
            FileOutputStream outputStream = new FileOutputStream(excelFilePath);
            workbook.write(outputStream);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void createHeaderRow(Sheet sheet) {

        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        Font font = sheet.getWorkbook().createFont();
        font.setBold(true);
        font.setFontHeightInPoints((short) 12);
        cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        cellStyle.setFont(font);
        cellStyle.setWrapText(true);
        cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
        cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        cellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

        Row row = sheet.createRow(0);
        Cell cell = null;
        cell = row.createCell(0);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Code Division");
        cell = row.createCell(1);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Nom");
    }

    private void writeBook(Division request, Row row, CellStyle cellStyle) {
        Cell cell = null;
        cell = row.createCell(0);
        cell.setCellStyle(cellStyle);
        cell.setCellValue(request.getDvId());
        cell = row.createCell(1);
        cell.setCellStyle(cellStyle);
        cell.setCellValue(request.getName());
    }

    private List<Division> getRequestList() {
        List<DataRecord> dvRecords = null;
        List<Division> dvData = new ArrayList<Division>();
        DataSource connectorParamDS = null;
        Division book = null;
        final String[] dvFields = new String[] {"dv_id", "name"};
        connectorParamDS = DataSourceFactory.createDataSource();
        connectorParamDS.addTable("dv",DataSource.ROLE_MAIN);
        connectorParamDS.addField("dv", dvFields);
        connectorParamDS.addSort("dv","dv_id", DataSource.SORT_ASC);
        connectorParamDS.addRestriction(Restrictions.isNull("dv", "cl_id"));
        dvRecords = connectorParamDS.getAllRecords();
        if (dvRecords.size() > 0) {
            for (DataRecord record : dvRecords) {
                book = new Division(record.getNeutralValue("dv.dv_id"), 
                    record.getNeutralValue("dv.name"));
                dvData.add(book);
            }
        }
        return dvData;
    }

    public void generateXLSX()throws Exception{
        ExceptionalDivisions excelWriter = new ExceptionalDivisions();
        List<Division> listBook = excelWriter.getRequestList();
        final List<String> attachments = new ArrayList<String>();
        final String propertyFile = getWebCentralRoot().trim() + "/WEB-INF/config/mail.properties";
        String to = getToMailProperties(propertyFile).trim().equals("") || getToMailProperties(propertyFile).trim()==null ?"" : getToMailProperties(propertyFile).trim();
        String subject = "Rapport des Divisions sans Client";
        String body = "Veuillez trouver en PJ la liste des Divisions qui ne sont pas liées à un Client."
                + "\nVous pouvez les assigner à un Client via le rapport "+"Divisions sans Client"+" dans SURICAT.";
        String excelFilePath = ContextStore.get().getWebAppPath().toString() + File.separator + "Exceptional_Division_Report.xls";
        try {
            excelWriter.writeExcel(listBook, excelFilePath);
            File file = new File(excelFilePath);
            
            final EventHandlerContext context = ContextStore.get().getEventHandlerContext();
            if (StringUtil.notNullOrEmpty(excelFilePath)) {
                attachments.add(excelFilePath);
            }
            sendExceptionalDivisionEmail(body, subject, to, ACTIVITY_ID, attachments,context);
            file.delete();
          } catch (IOException e) {
            e.printStackTrace();
        }
    
    }
    
    public void sendExceptionalDivisionEmail(final String emailBody, final String emailSubject,
            final String recipient, final String activityCode,
            final List<String> attachmentFileNames,EventHandlerContext context) {
        
        final String from = getEmailFrom(context);
        final String host = getEmailHost(context);
        final String port = getEmailPort(context);
        final String userId = getEmailUserId(context);
        final String password = getEmailPassword(context);
        if(recipient != null && !recipient.isEmpty()){
            sendEmail(emailBody, from, host, port, emailSubject, recipient, null, null, userId,
                password, (ArrayList<String>) attachmentFileNames, CONTENT_TYPE_TEXT_UFT8, activityCode);
        }else{
           this.log.warn("E-mail address is empty..., can not send mail."
                    + " Please append 'mail.addresses.to=example@mycompany.local' into '/archibus/WEB-INF/config/mail.properties'");
        }
    }
    
    public String getWebCentralRoot() {
        Properties properties = System.getProperties();
        Enumeration<Object> enumeration = properties.keys();
        for (int i = 0; i < properties.size(); i++) {
            Object obj = enumeration.nextElement();
            if (obj.toString().equals("WebCentral.root")) {
                return System.getProperty(obj.toString()).replace("\\", "/");
            }
        }
        return "";
    }
    
    public String getToMailProperties(String propertyFile) throws Exception{
        File file = new File(propertyFile);
        FileInputStream fileInput = new FileInputStream(file);
        Properties properties = new Properties();
        properties.load(fileInput);
        fileInput.close();
        Enumeration<?> enuKeys = properties.keys();
        while (enuKeys.hasMoreElements()) {
            String key = (String) enuKeys.nextElement();
             if(key.equals("mail.addresses.to")){
                return properties.getProperty(key);
            }
        }
    return "";
    }
}