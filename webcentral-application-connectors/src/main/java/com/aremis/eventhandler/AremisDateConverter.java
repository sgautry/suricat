package com.aremis.eventhandler;

import com.archibus.app.common.connectors.domain.ConnectorConfig;
import com.archibus.app.common.connectors.domain.ConnectorFieldConfig;
import com.archibus.app.common.connectors.impl.archibus.DataSourceUtil;
import com.archibus.app.common.connectors.impl.archibus.translation.field.IFieldTranslator;
import com.archibus.app.common.connectors.translation.exception.TranslationException;
import com.archibus.config.UserSession.Immutable;
import com.archibus.context.Context;
import com.archibus.context.ContextStore;
import com.archibus.db.ViewField;
import com.archibus.schema.DataType;
import com.archibus.utility.StringUtil;
import java.text.SimpleDateFormat;
import org.apache.log4j.Logger;

public class AremisDateConverter
  implements IFieldTranslator
{
  static Logger log = Logger.getLogger(AremisDateService.class);
  private SimpleDateFormat foreignDateFormat;
  private boolean isExport;
  private boolean isTime;
  private boolean isDate;
  
  public void init(ConnectorFieldConfig connectorField)
  {
    if (StringUtil.isNullOrEmpty(connectorField.getParameter())) {
      this.foreignDateFormat = new SimpleDateFormat();
    } else {
      this.foreignDateFormat = new SimpleDateFormat(connectorField.getParameter(), ContextStore.get().getUserSession().getLocale());
    }
    this.isExport = connectorField.getConnector().getExport().booleanValue();
    ViewField.Immutable fieldDef = DataSourceUtil.getFieldDef(connectorField);
    DataType sqlType = fieldDef == null ? null : DataType.get(fieldDef.getSqlType());
    this.isTime = (sqlType == DataType.TIME);
    this.isDate = (sqlType == DataType.DATE);
  }
  
  public boolean requiresExistingValue()
  {
    return true;
  }
  
  public Object applyRule(Object value)
    throws TranslationException
  {
    try
    {
      String inputDate = value.toString();
      String yearmonth = value.toString();
      if (yearmonth.length() != 6) {
        return null;
      }
      inputDate = "01-" + yearmonth.substring(4, 6) + "-" + yearmonth.substring(0, 4);
      SimpleDateFormat df = new SimpleDateFormat("dd-mm-YYYY");
      df.setLenient(false);
      df.parse(inputDate);
      return yearmonth.substring(0, 4) + "-" + yearmonth.substring(4, 6) + "-01";
    }
    catch (Exception ex) {}
    return null;
  }
  
  protected static void logDebug(String message)
  {
    if (log.isDebugEnabled()) {
      log.debug(message);
    }
  }
  
  public static String convertYearMonthToFullDate(String yearmonth)
  {
    try
    {
      String inputDate = yearmonth;
      log.debug("yearmonth : " + yearmonth);
      if (yearmonth.length() != 6) {
        return null;
      }
      inputDate = "01-" + yearmonth.substring(4, 6) + "-" + yearmonth.substring(0, 4);
      log.debug("INPUT DATE" + inputDate);
      SimpleDateFormat df = new SimpleDateFormat("dd-mm-YYYY");
      df.setLenient(false);
      df.parse(inputDate);
      return yearmonth.substring(0, 4) + "-" + yearmonth.substring(4, 6) + "-01";
    }
    catch (Exception ex) {}
    return null;
  }
}
