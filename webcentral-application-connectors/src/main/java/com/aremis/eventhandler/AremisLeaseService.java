package com.aremis.eventhandler;

import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.archibus.context.ContextStore;
import com.archibus.datasource.*;
import com.archibus.db.DbConnection;

/**
 * Provides TODO. - if it has behavior (service), or 
 * Represents TODO. - if it has state (entity, domain object, model object). 
 * Utility class. Provides methods TODO.
 * Interface to be implemented by classes that TODO.
 *<p>
 *
 * Used by TODO to TODO.
 * Managed by Spring, has prototype TODO singleton scope. Configured in TODO file.
 *
 * @author n.ledent
 * @since 20.1
 *
 */
public class AremisLeaseService {
    static Logger log = Logger.getLogger(AremisLeaseService.class);
    public static void updateBuildingId(){
        String sql;
        final String update   = "BEGIN "+
                                 "   For rmloop in (  "+
                                 "                   SELECT  MIN(ls_id) ls_id, MIN(bl_id) bl_id FROM RM WHERE ls_id IS NOT NULL GROUP BY ls_id "+
                                 "                  ) "+
                                 "   LOOP "+
                                 "           update ls set BL_ID = rmloop.bl_id WHERE ls_id = rmloop.ls_id; "+
                                 "   END LOOP; "+
                                 "   COMMIT; "+
                                 "   END;";
        DbConnection.ThreadSafe dbCnx = ContextStore.get().getDbConnection();
        dbCnx.executeUpdate(update);
    }
    public static void arLsUicGen(){
        final String erase   = "DELETE FROM ar_ls_uic";
        final String insert ="BEGIN "+
                             "   DELETE FROM ar_ls_uic;"+
                             "      For rmloop in (  "+
                             "                       select fl.uic_id,ls_id FROM fl "+
                             "                       LEFT JOIN ls ON ls.bl_id = fl.bl_id "+
                             "                       WHERE ls_id IS NOT NUll AND fl.uic_id IS NOT NUll "+
                             "                       GROUP BY fl.uic_id,ls_id"+
                             "                       )  "+
                             "       LOOP "+
                             "               INSERT INTO ar_ls_uic(ls_id,uic_id) "+
                             "               SELECT rmloop.ls_id,rmloop.uic_id  "+
                             "               FROM DUAL "+
                             "               WHERE NOT EXISTS( SELECT  0 FROM    ar_ls_uic WHERE   ls_id = rmloop.ls_id AND uic_id = rmloop.uic_id); "+
                             "               UPDATE ls SET uic_id = rmloop.uic_id WHERE ls_id = rmloop.ls_id;"+
                             "       END LOOP;"+ 
                             "       COMMIT;"+ 
                             "       END;";
        DbConnection.ThreadSafe dbCnx = ContextStore.get().getDbConnection();
        dbCnx.executeUpdate(erase);
        dbCnx.executeUpdate(insert);
    }
    protected static void logDebug(String message){
        if(log.isDebugEnabled()){
            log.debug(message);
        }
    }
}
