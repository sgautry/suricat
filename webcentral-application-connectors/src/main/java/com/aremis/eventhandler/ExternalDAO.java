package com.aremis.eventhandler;

import java.sql.*;
import java.util.*;

import org.apache.log4j.Logger;

import com.archibus.config.Database;
import com.archibus.context.ContextStore;
import com.archibus.db.*;
import com.archibus.eventhandler.EventHandlerBase;
import com.archibus.jobmanager.*;

/**
 * Helper Class that execute selects, inserts and updates into DB.
 * 
 * @author Ghita Marina
 * 
 */
public class ExternalDAO extends EventHandlerBase {

    private static EventHandlerContext context;

    private static com.archibus.context.Context localContext;

    private static PooledDbDriver.Immutable connectionPool;

    private static final Logger log = Logger.getLogger(com.archibus.jobmanager.EventHandler.class);

    static {
        // ServiceHelper.prepareContext();
        localContext = ContextStore.get();
        Database.Immutable localImmutable = localContext.getProject().findDatabase("data");
        DbConnection.ThreadSafe localThreadSafe = localImmutable.getPool().getConnection();
        connectionPool = localImmutable.getPool();
        localContext.setDbConnection(localThreadSafe);
        EventHandlerContextImpl localEventHandlerContextImpl = new EventHandlerContextImpl(null,
            null);
        localEventHandlerContextImpl.addInputParameter("context", localContext.getUserSession());
        localEventHandlerContextImpl.addInputParameter("project", localContext.getProject());
        localEventHandlerContextImpl.addInputParameter("database", localImmutable);
        localEventHandlerContextImpl
            .addInputParameter("connection", localContext.getDbConnection());
        localContext.setEventHandlerContext(localEventHandlerContextImpl);
        context = ContextStore.get().getEventHandlerContext();
    }

    private ExternalDAO() {
    }

    public static List<Map<String, Object>> getRecords(String sql) {
        try {
            return retrieveDbRecords(context, sql);
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
    }

    public static Map<String, Object> getFirstRecord(String sql) {
        try {
            List<Map<String, Object>> records = retrieveDbRecords(context, sql);
            if (records.size() > 0) {
                return records.get(0);
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
    }

    public static void executeUpdate(String sql) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = connectionPool.getConnection().getConnection();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            connection.commit();
        } catch (SQLException e) {
            log.error(e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                log.error(ex.getMessage());
            }
        }
    }

    public static void saveRecord(String tableName, Map<String, Object> record) {
        try {
            executeDbSave(context, tableName, record);
            executeDbCommit(context);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    public static Logger getLog() {
        return log;
    }
}
