package com.archibus.app.common.connectors.smsbuilder;

import com.archibus.app.common.connectors.transfer.exception.AdaptorException;
import com.archibus.context.ContextStore;
import com.archibus.eventhandler.EventHandlerBase;
import com.archibus.jobmanager.*;
import com.archibus.utility.ExceptionBase;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 *
 * Special Connector Job that executes the dependent connectors, defined as
 * parameter, in order.
 * <p>
 *
 * @author CatalinP
 * @since 23.1
 *
 */
public final class SmsBuilderConnectorJob extends JobBase {

	/**
	 * SMS Builder license id.
	 */
	private static final String SMS_BUILDER_LICENSE_ID = "AbEAMSMSBuilderExtension";

	/**
	 * Units per step.
	 */
	private static final int UNITS_PER_STEP = 100;

	/**
	 * Master connector.
	 */
	private SmsBuilderMasterConnector masterConnector;

	/**
	 * Executes the base connector.
	 *
	 * @param masterConnectorId
	 *            base connector id.
	 */
	public void executeConnector(final String masterConnectorId) {
		if (isSMSBuilderLicensed()) {
			this.masterConnector = new SmsBuilderMasterConnector(masterConnectorId);
			if (this.masterConnector.getConnectors().isEmpty()) {
				this.masterConnector.getConnectorLogger().writeMessage("No connector defined for master connector.");
			} else {
				this.masterConnector.getConnectorLogger().writeMessage("Executing master connector...");
				run();
			}
			this.masterConnector.getConnectorLogger().writeMessage("Complete executing master connector.");

		} else {
			this.status.setCode(JobStatus.JOB_FAILED);
			throw new ExceptionBase(
					"The license for the Extension for SMS Builder is not present. As a result, the master SMS Builder Connector was not executed.");
		}
	}

	/**
	 * Returns true if the SMS Builder is licensed.
	 *
	 * @return boolean
	 */
	private boolean isSMSBuilderLicensed() {
		return EventHandlerBase.isActivityLicenseEnabled(ContextStore.get().getEventHandlerContext(),
				SMS_BUILDER_LICENSE_ID);
	}

	/**
	 * Executes all connectors in order.
	 */
	@Override
	public void run() {
		this.status.setCode(JobStatus.JOB_STARTED);
		this.status.setTotalNumber(this.masterConnector.getConnectors().size() * UNITS_PER_STEP);
		int index = 0;
		for (final String connectorId : this.masterConnector.getConnectorsOrder()) {
			index++;
			this.masterConnector.getConnectorLogger()
					.writeMessage("Executing connector #" + index + ": " + connectorId);

			try {
				this.masterConnector.getConnectors().get(connectorId).execute();
			} catch (final AdaptorException e) {
				throw new ExceptionBase(
						String.format("Error while executing connector [%s]: %s", connectorId, e.getMessage()));

			}
			this.status.setCurrentNumber(UNITS_PER_STEP);
		}

		this.status.setCode(JobStatus.JOB_COMPLETE);
	}

}
