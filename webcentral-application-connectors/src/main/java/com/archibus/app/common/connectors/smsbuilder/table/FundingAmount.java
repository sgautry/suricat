package com.archibus.app.common.connectors.smsbuilder.table;

import com.archibus.datasource.*;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides TODO. - if it has behavior (service), or Represents TODO. - if it has state (entity,
 * domain object, model object). Utility class. Provides methods TODO. Interface to be implemented
 * by classes that TODO.
 * <p>
 *
 * Used by TODO to TODO. Managed by Spring, has prototype TODO singleton scope. Configured in TODO
 * file.
 *
 * @author Catalin
 * @since 23.1
 *
 */
public final class FundingAmount {

    /**
     * Table name.
     */
    public static final String TABLE_NAME = "funding_amt";

    /**
     * Field name.
     */
    public static final Field FUND_ID = new Field(TABLE_NAME, "fund_id");

    /**
     * Field name.
     */
    public static final Field FISCAL_YEAR = new Field(TABLE_NAME, "fiscal_year");

    /**
     * Field name.
     */
    public static final Field AMOUNT = new Field(TABLE_NAME, "amount");

    /**
     * Default TODO constructor specifying TODO. Private default constructor: utility class is
     * non-instantiable.
     */
    private FundingAmount() {
        super();
    }

    /**
     * Create datasource for all fields.
     *
     * @return DataSource
     */
    public static DataSource createDataSource() {
        return DataSourceFactory.createDataSourceForFields(TABLE_NAME,
            new String[] { FUND_ID.getName(), FISCAL_YEAR.getName(), AMOUNT.getName() });
    }

}
