package com.archibus.app.common.connectors.smsbuilder.connector.rule;

import java.util.Map;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Holds the Java method parameters.
 *
 * @author Catalin
 * @since 23.1
 *
 */
public class ConnectorJavaMethodParameter {

    /**
     * class name including the package.
     */
    private final String className;

    /**
     * method name.
     */
    private final String methodName;

    /**
     * method parameters.
     */
    private final Map<String, JavaType> parameters;

    /**
     * Constructor.
     *
     * @param className class name
     * @param methodName method name
     * @param parameters parameters
     */
    public ConnectorJavaMethodParameter(final String className, final String methodName,
            final Map<String, JavaType> parameters) {
        super();
        this.className = className;
        this.methodName = methodName;
        this.parameters = parameters;
    }

    /**
     * Getter for the className property.
     *
     * @return the className property.
     */
    public String getClassName() {
        return this.className;
    }

    /**
     * Getter for the methodName property.
     *
     * @return the methodName property.
     */
    public String getMethodName() {
        return this.methodName;
    }

    /**
     * Getter for the parameters property.
     *
     * @return the parameters property.
     */
    public Map<String, JavaType> getParameters() {
        return this.parameters;
    }

}
