package com.archibus.app.common.connectors.impl.db.inbound;

import java.util.*;

import javax.sql.RowSet;

import org.json.JSONObject;

import com.archibus.app.common.connectors.domain.*;
import com.archibus.app.common.connectors.exception.ConfigurationException;
import com.archibus.app.common.connectors.impl.archibus.inbound.InboundRequests;
import com.archibus.app.common.connectors.impl.db.DbUtil;
import com.archibus.app.common.connectors.logging.IUserLog;
import com.archibus.app.common.connectors.transfer.common.ConnectorObfuscationUtil;
import com.archibus.app.common.connectors.transfer.db.*;
import com.archibus.app.common.connectors.translation.common.outbound.*;
import com.archibus.app.common.connectors.translation.common.outbound.impl.RequestDef;
import com.archibus.app.common.connectors.translation.db.inbound.*;
import com.archibus.utility.StringUtil;

/**
 *
 * Provides requests to a database connection to produce records for ARCHIBUS database.
 *
 * @author Catalin Purice
 * @since 21.3
 *
 */
public class InboundDbRequests
        extends InboundRequests<DbReadRequest, RowSet, Map<String, Object>, DbResponseRecordDef> {

    /**
     * afm_connectors.parameter json key indicating characters to be used for reserved words.
     */
    public static final String IDENTIFIER_TERMINALS = "identifierTerminals";

    /**
     * Generate a series of requests to a file system to produce records from delimited text.
     *
     * @param stepName a descriptive name for this step.
     * @param connector the afm_connector record to use as configuration
     * @param log a place to write user friendly status messages
     * @throws ConfigurationException if an associated connector rule can't be instantiated.
     */
    public InboundDbRequests(final String stepName, final ConnectorConfig connector,
            final IUserLog log) throws ConfigurationException {
        // CHECKSTYLE:OFF checkstyle doesn't like method generics requiring a space after the <>
        super(stepName, Collections.singletonList(Collections.<String, Object>emptyMap()),
            new RequestDef<IRequestFieldDefinition>(
                Collections.<IRequestFieldDefinition>emptyList()),
            // CHECKSTYLE:ON
            createRequestTemplate(connector),
            new InboundDatabaseAdaptor(new JdbcConnectionConfig(connector.getConnString(),
                DbUtil.getDriver(connector), connector.getConnUser(),
                ConnectorObfuscationUtil.decodeParameter(connector.getConnPassword()))),
            Collections.singletonList(new DbResponseRecordDef(connector)), log);
    }

    /*
     * JUSTIFICATION: Not an ARCHIBUS database, or even one with a known structure.
     */
    /**
     * @param connector the configuration for the connector that requires these requests to be
     *            issued.
     * @return a template (SQL query) to retrieve records defined by connector fields from the
     *         foreign database.
     * @throws ConfigurationException if there is an error getting an instance of a connector rule
     *             defining whether the field should be queried.
     */
    @SuppressWarnings("PMD.AvoidUsingSql")
    private static IRequestTemplate<DbReadRequest> createRequestTemplate(
            final ConnectorConfig connector) throws ConfigurationException {

        final String[] identifierTerminal = getIdentifierTerminal(connector.getConnParams());

        final StringBuffer query = new StringBuffer("SELECT ");
        for (final ConnectorFieldConfig connectorField : connector.getConnectorFields()) {
            if (connectorField.getRule().getInstance().requiresExistingValue()) {
                query.append(identifierTerminal[0]).append(connectorField.getForeignFieldPath())
                    .append(identifierTerminal[1]).append(',');
            }
        }
        query.delete(query.length() - 1, query.length());
        query.append(" FROM ").append(identifierTerminal[0]).append(connector.getForeignTxPath())
            .append(identifierTerminal[1]);
        if (!StringUtil.isNullOrEmpty(connector.getClause())) {
            query.append(" WHERE ").append(connector.getClause());
        }
        // CHECKSTYLE:OFF checkstyle doesn't like method generics requiring a space after the <>
        return new DbReadRequestTemplate(query.toString(), Collections.<String>emptyList());
        // CHECKSTYLE:ON
    }

    /**
     * Gets terminal identifiers.
     *
     * @param jsonObject connector's config
     * @return terminals
     */
    private static String[] getIdentifierTerminal(final JSONObject jsonObject) {
        final String[] identifiers = { "", "" };
        if (jsonObject.has(IDENTIFIER_TERMINALS)) {
            final String value = jsonObject.getString(IDENTIFIER_TERMINALS);
            if (value != null && value.toString().length() == 2) {
                identifiers[0] = String.valueOf(value.charAt(0));
                identifiers[1] = String.valueOf(value.charAt(1));
            }
        }
        return identifiers;
    }
}
