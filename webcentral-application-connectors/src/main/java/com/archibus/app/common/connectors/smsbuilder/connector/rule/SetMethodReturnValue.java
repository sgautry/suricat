package com.archibus.app.common.connectors.smsbuilder.connector.rule;

import java.text.ParseException;
import java.util.*;

import org.json.*;

import com.archibus.app.common.connectors.domain.ConnectorFieldConfig;
import com.archibus.app.common.connectors.exception.ConfigurationException;
import com.archibus.app.common.connectors.impl.archibus.translation.record.IRecordTranslator;
import com.archibus.utility.StringUtil;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Defines a field connector rule which is capable of calling a Java method and use the returning
 * value in translated record.
 * <p>
 *
 *
 * @author Catalin Purice
 * @since 23.3
 *
 */
public class SetMethodReturnValue implements IRecordTranslator {

    /**
     * The delimiter between a class and a method in connector configuration.
     */
    private static final String CLASS_METHOD_DELIMITER = "\\|";

    /**
     * Constant.
     */
    private static final String PARAMETERS = "parameters";

    /**
     * Constant.
     */
    private static final String CLASS = "class";

    /**
     * The field to be truncated.
     */
    private String fieldKey;

    /**
     * Size to truncate to.
     */
    private MethodCall method;

    /**
     * Instantiate this rule, completely resetting it's state.
     *
     * @param connectorField ignored.
     * @throws ConfigurationException if parameter is not specified
     */
    @Override
    public void init(final ConnectorFieldConfig connectorField) throws ConfigurationException {
        this.fieldKey = connectorField.getFieldId();
        final String connectorFieldId =
                connectorField.getConnectorId() + "." + connectorField.getFieldId();
        try {
            final JSONObject fieldParam =
                    new JSONObject(StringUtil.isNullOrEmpty(connectorField.getParameter()) ? "{}"
                            : connectorField.getParameter());
            final String className = fieldParam.has(CLASS) ? fieldParam.getString(CLASS) : "";
            if (StringUtil.isNullOrEmpty(className)) {
                throw new ConfigurationException("Please define a valid class|method.", null);
            }
            createMethodStep("call Java method", className, fieldParam);
        } catch (final ConfigurationException e) {
            throw new ConfigurationException(
                "Error while creating java method for field " + connectorFieldId, e);
        } catch (final ParseException e) {
            throw new ConfigurationException(
                "Please define a valid class|method string for " + connectorFieldId, e);
        }
    }

    /**
     * Call the method defined the value to the specified size.
     *
     * @param record the record to be modified (may already be modified by other rules).
     * @param originalRecord the record prior to translation by other record level rules.
     */
    @Override
    public void applyRule(final Map<String, Object> record,
            final Map<String, Object> originalRecord) {
        record.put(this.fieldKey, this.method.execute(buildArguments(record)));
    }

    /**
     *
     * Builds the arguments for the method.
     *
     * @param record record
     * @return array of argument values
     */
    private Object[] buildArguments(final Map<String, Object> record) {
        final Object[] values = new Object[this.method.getParameter().getParameters().size()];
        final Iterator<String> iter =
                this.method.getParameter().getParameters().keySet().iterator();
        int index = 0;
        while (iter.hasNext()) {
            values[index++] = record.get(iter.next());
        }
        return values;
    }

    /**
     * @param name Name of the step.
     * @param methodDescriptor class|method
     * @param parameters parameters
     * @throws ConfigurationException if for some reason the method cannot be found or accessed
     */
    private void createMethodStep(final String name, final String methodDescriptor,
            final JSONObject parameters) throws ConfigurationException {
        final String[] methodQualifiers = methodDescriptor.split(CLASS_METHOD_DELIMITER);
        Map<String, JavaType> resultMap = new LinkedHashMap<String, JavaType>();
        if (parameters.has(PARAMETERS)) {
            final JSONArray params = parameters.getJSONArray(PARAMETERS);
            resultMap = MethodCall.toType(params);
        }
        try {
            this.method = new MethodCall(new ConnectorJavaMethodParameter(methodQualifiers[0],
                methodQualifiers[1], resultMap));
        } catch (final SecurityException e) {
            throw new ConfigurationException("", e);
        }
    }

    /**
     * @return true as there should be a value to be truncated.
     */
    @Override
    public boolean requiresExistingValue() {
        return false;
    }
}