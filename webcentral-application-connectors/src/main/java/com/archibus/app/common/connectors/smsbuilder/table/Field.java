
package com.archibus.app.common.connectors.smsbuilder.table;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides TODO. - if it has behavior (service), or Represents TODO. - if it has state (entity,
 * domain object, model object). Utility class. Provides methods TODO. Interface to be implemented
 * by classes that TODO.
 * <p>
 *
 * Used by TODO to TODO. Managed by Spring, has prototype TODO singleton scope. Configured in TODO
 * file.
 *
 * @author Catalin
 * @since 23.1
 *
 */
public class Field {

    private final String tableName;

    private final String name;

    /**
     * Default TODO constructor specifying TODO. Private default constructor: utility class is
     * non-instantiable.
     *
     * @param tableName
     * @param name
     */
    public Field(final String tableName, final String name) {
        super();
        this.tableName = tableName;
        this.name = name;
    }

    public String getFullName() {
        return this.tableName + "." + this.name;
    }

    /**
     * Getter for the tableName property.
     *
     * @return the tableName property.
     */
    public String getTableName() {
        return this.tableName;
    }

    /**
     * Getter for the name property.
     *
     * @return the name property.
     */
    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return getName();
    }

}
