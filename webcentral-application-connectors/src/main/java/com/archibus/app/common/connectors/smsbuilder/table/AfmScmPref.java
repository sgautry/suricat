package com.archibus.app.common.connectors.smsbuilder.table;

import com.archibus.datasource.*;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides TODO. - if it has behavior (service), or Represents TODO. - if it has state (entity,
 * domain object, model object). Utility class. Provides methods TODO. Interface to be implemented
 * by classes that TODO.
 * <p>
 *
 * Used by TODO to TODO. Managed by Spring, has prototype TODO singleton scope. Configured in TODO
 * file.
 *
 * @author Catalin
 * @since 23.1
 *
 */
public final class AfmScmPref {

    /**
     * Table name.
     */
    public static final String TABLE_NAME = "afm_scmpref";

    /**
     * Field name.
     */
    public static final Field FISCAL_YEAR_START_DAY = new Field(TABLE_NAME, "fiscalyear_startday");

    /**
     * Field name.
     */
    public static final Field FISCAL_YEAR_START_MONTH =
            new Field(TABLE_NAME, "fiscalyear_startmonth");

    /**
     * Constructor.
     */
    private AfmScmPref() {
        super();
    }

    /**
     * Create datasource for all fields.
     *
     * @return DataSource
     */
    public static DataSource createDataSource() {
        return DataSourceFactory.createDataSourceForFields(TABLE_NAME,
            new String[] { FISCAL_YEAR_START_DAY.getName(), FISCAL_YEAR_START_MONTH.getName() });
    }

}
