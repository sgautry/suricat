package com.archibus.app.common.connectors.service;

import org.apache.log4j.Logger;

import com.archibus.app.common.connectors.dao.IConnectorDao;
import com.archibus.app.common.connectors.dao.datasource.ConnectorDataSource;
import com.archibus.app.common.connectors.domain.ConnectorConfig;
import com.archibus.app.common.connectors.domain.ConnectorTypes.ExecFlag;
import com.archibus.app.common.connectors.exception.*;
import com.archibus.app.common.connectors.logging.common.ConnectorLogTableLogger;
import com.archibus.app.common.connectors.service.exception.ConnectorException;
import com.archibus.jobmanager.*;
import com.archibus.utility.ExceptionBase;
import com.aremis.eventhandler.*;

/**
 * Workflow rules/jobs for managing connectors.
 *
 * @author cole
 */
public class ConnectorJob extends JobBase {

    /**
     * Version information for connectors.
     */
    public static final String VERSION_INFO = "Starting Connector Job using built-in Connectors.";

    /**
     * Prefix for logged error messages.
     */
    public static final String ERROR_PREFIX = "Error: ";

    /**
     * The package prefix for connectors.
     */
    public static final String PACKAGE_PREFIX = "com.archibus.app.common.connectors";

    /**
     * Number of units of progress to log for the job per step of the connector
     * executed.
     */
    private static final int UNITS_PER_STEP = 100;

    /**
     * A textual log of events during connector execution.
     */
    private final Logger eventLog = Logger.getLogger(ConnectorJob.class);

    /**
     * The configuration for the connector to be executed by this job.
     */
    private ConnectorConfig connectorConfig;

    /**
     * A means for updating connector status using the connectorBean.
     */
    private IConnectorDao connectorDao;

    /**
     * To be used when constructed as a job.
     */
    public ConnectorJob() {
        /*
         * DAO and Bean to be set by executeConnector.
         */
        super();
    }

    /**
     * Create a connector with the connector configuration provided.
     *
     * @param connectorBean
     *            the connector configuration.
     * @param connectorDao
     *            a means for updating connector status using the connectorBean.
     */
    public ConnectorJob(final ConnectorConfig connectorBean, final IConnectorDao connectorDao) {
        super();
        this.connectorDao = connectorDao;
        this.connectorConfig = connectorBean;
    }

    /**
     * Create a connector with the connector configuration in the afm_connectors
     * table.
     *
     * @param connectorId
     *            the identifier for the record in the afm_connectors table.
     */
    public ConnectorJob(final String connectorId) {
        super();
        this.connectorDao = new ConnectorDataSource();
        this.connectorConfig = this.connectorDao.get(connectorId);
    }

    /**
     * Execute a connector specified by the id and looked up from the
     * afm_connector table.
     *
     * @param connectorId
     *            the id of the connector on the afm_connectors table.
     */
    public void executeConnector(final String connectorId) {
        this.connectorDao = new ConnectorDataSource();
        this.connectorConfig = this.connectorDao.get(connectorId);
            CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
            if(customConnectorProcessing.getConnectorType(this.connectorConfig.getConnectorId())!=null){
                this.connectorConfig.setConnStringDb(customConnectorProcessing.getConnectorFileName(this.connectorConfig.getConnStringDb(), 
                    this.connectorConfig.getConnectorId()));
            }
        run();
    }

    /**
     * Execute a connector assigned to the connectorBean attribute of this
     * class.
     */
    @Override
    public void run() {
        this.status.setCode(JobStatus.JOB_STARTED);
        final ConnectorLogTableLogger log = new ConnectorLogTableLogger(this.connectorConfig.getConnectorId());

        /*
         * Log beginning.
         */
        this.eventLog.info(VERSION_INFO);
        log.clear();
        log.writeMessage(VERSION_INFO);

        /*
         * Set status to running.
         */
        final ConnectorConfig unmodifiedConnectorBean = this.connectorDao.get(this.connectorConfig.getConnectorId());
        unmodifiedConnectorBean.setExecFlag(ExecFlag.RUNNING);
        this.connectorDao.update(unmodifiedConnectorBean);

        Throwable exception = null;
        String exceptionMessage = null;
            String connectorId = null;
        try {

            /*
             * Create the connector and execute it.
             */
            new DatabaseConfiguredConnector(this.connectorConfig.getConnectorId(), log, this.status,
                    this.connectorConfig, UNITS_PER_STEP).execute();

            /*
             * Consider the job a success.
             */
            this.status.setCode(JobStatus.JOB_COMPLETE);
            this.status.setMessage("Success");
            ConnectorEmailHelper.sendConnectorEmailComplete(this.connectorConfig, log.getLogSummary());

        } catch (final StepException t) {
            exception = t;
            exceptionMessage = new StringBuilder().append(ERROR_PREFIX).append(ExceptionUtil.getExceptionBaseMessage(t))
                    .toString();
        } catch (final ExceptionBase t) {
            exception = t;
            exceptionMessage = new StringBuilder().append(ERROR_PREFIX).append(ExceptionUtil.getExceptionBaseMessage(t))
                    .append(ExceptionUtil.getFilteredStackTrace(t, PACKAGE_PREFIX)).toString();
        } catch (final RuntimeException t) {
            /*
             * TODO the intention is to use standard job UI components to handle
             * runtime exceptions in the future, instead of catching them.
             */
            exception = t;
            final StringBuilder messageBuilder = new StringBuilder();
            messageBuilder.append(ERROR_PREFIX);
            messageBuilder.append(ExceptionUtil.getRuntimeExceptionMessage(t));
            messageBuilder.append(ExceptionUtil.getFilteredStackTrace(t, PACKAGE_PREFIX));
            exceptionMessage = messageBuilder.toString();
        } finally {
            /*
             * Reset execution status.
             */
            unmodifiedConnectorBean.setExecFlag(ExecFlag.READY);
            this.connectorDao.update(unmodifiedConnectorBean);
        }
        if (exception != null) {
            /*
             * Log exceptions and consider the job a failure.
             */
            log.writeMessage(exceptionMessage);
            this.status.setCode(JobStatus.JOB_FAILED);
            this.status.setMessage(exceptionMessage);
            log.writeMessage("Connector Failed");
            ConnectorEmailHelper.sendConnectorEmailError(this.connectorConfig, exceptionMessage);
            if(exceptionMessage.contains("Missing file")){
                connectorId = this.connectorConfig.getConnectorId();
                CustomConnectorProcessing customConnectorProcessing = new CustomConnectorProcessing();
                if(connectorId.equals("Import CHENE EPIC_BU")){
                    if(!customConnectorProcessing.isConnectorExecuted("Import CHENE_ArbreMDG DV")){
                        new ConnectorJob().executeConnector("Import CHENE_ArbreMDG DV");
                    }
                }
                if(connectorId.equals("Import CHENE_ArbreMDG DV")){
                    if(!customConnectorProcessing.isConnectorExecuted("Import CHENE_ArbreMDG RG")){
                        new ConnectorJob().executeConnector("Import CHENE_ArbreMDG RG");
                    }
                }
                if(connectorId.equals("Import CHENE_ArbreMDG RG")){
                    if(!customConnectorProcessing.isConnectorExecuted("Import CHENE DV")){
                        new ConnectorJob().executeConnector("Import CHENE DV");
                    }
                }
                if(connectorId.equals("Import CHENE DV")){
                    if(!customConnectorProcessing.isConnectorExecuted("Import CHENE RG")){
                        new ConnectorJob().executeConnector("Import CHENE RG");
                    }
                }
                if(connectorId.equals("Import DECISIS Roles")){
                    if(!customConnectorProcessing.isConnectorExecuted("Import DECISIS Activity")){
                        new ConnectorJob().executeConnector("Import DECISIS Activity");
                    }
                }
                if(connectorId.equals("Import DECISIS Activity")){
                    if(!customConnectorProcessing.isConnectorExecuted("Import DECISIS Sub Activity")){
                        new ConnectorJob().executeConnector("Import DECISIS Sub Activity");
                    }
                }
                if(connectorId.equals("Import DECISIS Sub Activity")){
                    if(!customConnectorProcessing.isConnectorExecuted("Import DECISIS ENS")){
                        new ConnectorJob().executeConnector("Import DECISIS ENS");
                    }
                }
                if(connectorId.equals("Import DECISIS Contract")){
                    if(!customConnectorProcessing.isConnectorExecuted("Import DECISIS Lease")){
                        new ConnectorJob().executeConnector("Import DECISIS Lease");
                    }
                }
                if(connectorId.equals("Import DECISIS Lease")){
                    if(!customConnectorProcessing.isConnectorExecuted("Import DECISIS Site")){
                        new ConnectorJob().executeConnector("Import DECISIS Site");
                    }
                }
                if(connectorId.equals("Import DECISIS Site")){
                    if(!customConnectorProcessing.isConnectorExecuted("Import DECISIS Building")){
                        new ConnectorJob().executeConnector("Import DECISIS Building");
                    }
                }
                if(connectorId.equals("Import DECISIS Building")){
                    if(!customConnectorProcessing.isConnectorExecuted("Import DECISIS Floor")){
                        new ConnectorJob().executeConnector("Import DECISIS Floor");
                    }
                }
                if(connectorId.equals("Import DECISIS Floor")){
                    if(!customConnectorProcessing.isConnectorExecuted("Import DECISIS Room")){
                        new ConnectorJob().executeConnector("Import DECISIS Room");
                    }
                }
            }
            throw new ConnectorException(exceptionMessage, exception);
        }
    }
}
