package com.archibus.app.common.connectors.smsbuilder;

import java.util.*;

import org.apache.commons.lang.StringUtils;
import org.json.*;

import com.archibus.app.common.connectors.dao.IConnectorDao;
import com.archibus.app.common.connectors.dao.datasource.ConnectorDataSource;
import com.archibus.app.common.connectors.domain.ConnectorConfig;
import com.archibus.app.common.connectors.logging.common.ConnectorLogTableLogger;
import com.archibus.app.common.connectors.service.*;
import com.archibus.context.ContextStore;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * SMS builder master connector definition.
 *
 * @author Catalin
 * @since 23.1
 *
 */
public class SmsBuilderMasterConnector {

    /**
     * Connector table.
     */
    private static final String AFM_CONNECTOR = "afm_connector";

    /**
     * Number of units of progress to log for the job per step of the connector executed.
     */
    private static final int UNITS_PER_STEP = 100;

    /**
     * Json element name.
     */
    private static final String CONNECTOR_ID_ELEMENT = "connectorIdList";

    /**
     * Json element name.
     */
    private static final String UPDATE_CREDENTIALS_ELEMENT = "updateCredentials";

    /**
     * Json element name.
     */
    private static final String UPDATE_MACRO_VALUES_ELEMENT = "updateMacroValues";

    /**
     * Macro for activity parameter.
     */
    private static final String[] ACTIVITY_PARAM_MACRO = { "${activity.parameter(", ")}" };

    /**
     * Base connector id.
     */
    private final ConnectorConfig masterConnector;

    /**
     * Dependent connector list.
     */
    private final LinkedHashMap<String, DatabaseConfiguredConnector> connectors;

    /**
     * Connector logger.
     */
    private final ConnectorLogTableLogger connectorLogger;

    /**
     * Connector ids to be executed. Supports same connector running multimple times.
     */
    private final List<String> connectorsOrder;

    /**
     * Constructor.
     *
     * @param baseConnectorId master connector id.
     */
    public SmsBuilderMasterConnector(final String baseConnectorId) {
        this.connectorsOrder = new ArrayList<String>();
        final IConnectorDao connectorDao = new ConnectorDataSource();
        this.masterConnector = connectorDao.get(baseConnectorId);
        this.connectors = new LinkedHashMap<String, DatabaseConfiguredConnector>();
        this.connectorLogger = new ConnectorLogTableLogger(baseConnectorId);
        loadConnectors();
    }

    /**
     * Adds connector to the list.
     *
     * @param connectorId connector id.
     */
    private void addConnector(final String connectorId) {
        this.connectorsOrder.add(connectorId);
        this.connectors.put(connectorId,
            new DatabaseConfiguredConnector(connectorId, new ConnectorLogTableLogger(connectorId),
                new ConnectorJob(connectorId).getStatus(),
                new ConnectorDataSource().get(connectorId), UNITS_PER_STEP));

    }

    /**
     * Load dependent connectors ids.
     *
     */
    private void loadConnectors() {
        updateNestedConnector();
        final JSONObject connectorObj = this.masterConnector.getConnParams();
        if (connectorObj.has(CONNECTOR_ID_ELEMENT)) {
            final JSONArray connectorList = connectorObj.getJSONArray(CONNECTOR_ID_ELEMENT);
            for (int index = 0; index < connectorList.length(); index++) {
                final String connectorId = connectorList.get(index).toString();
                if (!connectorId.startsWith("--")) {
                    addConnector(connectorId);
                }
            }
        }
    }

    /**
     * Updates the nested connector's credentials.
     *
     */
    private void updateNestedConnector() {
        String connectorList = "";
        final JSONObject connectorObj = this.masterConnector.getConnParams();
        if (connectorObj.has(UPDATE_CREDENTIALS_ELEMENT)
                && connectorObj.getBoolean(UPDATE_CREDENTIALS_ELEMENT)
                && connectorObj.has(CONNECTOR_ID_ELEMENT)) {
            connectorList = connectorObj.getJSONArray(CONNECTOR_ID_ELEMENT).toString()
                .replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "");
            updateCredential(connectorList);
        }
        if (connectorObj.has(UPDATE_MACRO_VALUES_ELEMENT)
                && connectorObj.getBoolean(UPDATE_MACRO_VALUES_ELEMENT)) {
            updateSqlRestriction(connectorList);
        }

    }

    private String getPreparedRestriction(final String sqlRestriction) {
        String restriction = sqlRestriction;
        final String[] matches = StringUtils.substringsBetween(sqlRestriction,
            ACTIVITY_PARAM_MACRO[0], ACTIVITY_PARAM_MACRO[1]);
        if (matches != null) {
            for (final String match : matches) {
                restriction = restriction.replace(
                    ACTIVITY_PARAM_MACRO[0] + match + ACTIVITY_PARAM_MACRO[1],
                    getActivityParameterValue(match));
            }
        }
        return restriction;
    }

    private void updateSqlRestriction(final String connectorIds) {
        final DataSource dataSource = DataSourceFactory.createDataSourceForTable(AFM_CONNECTOR);
        dataSource.addRestriction(Restrictions.in(AFM_CONNECTOR, "connector_id", connectorIds));
        final List<DataRecord> records = dataSource.getRecords();
        for (final DataRecord record : records) {
            record.setValue("afm_connector.clause",
                getPreparedRestriction(record.getString("afm_connector.clause")));
            dataSource.saveRecord(record);
        }
    }

    private String getActivityParameterValue(final String param) {
        return ContextStore.get().getProject().getActivityParameterManager()
            .getParameterValue(param);
    }

    /**
     * update credentials for fonnector list.
     *
     * @param connectorIds connector ids
     */
    private void updateCredential(final String connectorIds) {
        final DataSource dataSource = DataSourceFactory.createDataSourceForTable(AFM_CONNECTOR);
        dataSource.addRestriction(Restrictions.in(AFM_CONNECTOR, "connector_id", connectorIds));
        final List<DataRecord> records = dataSource.getRecords();
        for (final DataRecord record : records) {
            record.setValue("afm_connector.type", this.masterConnector.getTypeDb());
            record.setValue("afm_connector.conn_string", this.masterConnector.getConnStringDb());
            record.setValue("afm_connector.conn_user", this.masterConnector.getConnUser());
            record.setValue("afm_connector.conn_password", this.masterConnector.getConnPassword());
            dataSource.saveRecord(record);
        }
    }

    /**
     * Getter for the baseConnector property.
     *
     * @return the baseConnector property.
     */
    public ConnectorConfig getBaseConnector() {
        return this.masterConnector;
    }

    /**
     * Getter for the connectors property.
     *
     * @return the connectors property.
     */
    public LinkedHashMap<String, DatabaseConfiguredConnector> getConnectors() {
        return this.connectors;
    }

    /**
     * Getter for the connectorLogger property.
     *
     * @return the connectorLogger property.
     */
    public ConnectorLogTableLogger getConnectorLogger() {
        return this.connectorLogger;
    }

    /**
     * Getter for the connectorsOrder property.
     *
     * @return the connectorsOrder property.
     */
    public List<String> getConnectorsOrder() {
        return this.connectorsOrder;
    }

}
