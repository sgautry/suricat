package com.archibus.app.common.connectors.smsbuilder.connector.rule;

import java.lang.reflect.*;
import java.util.*;

import org.json.*;

import com.archibus.app.common.connectors.exception.ConfigurationException;
import com.archibus.app.common.connectors.impl.method.exception.MethodExecutionException;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Class that executes a java method based on connector parameter.
 * <p>
 *
 * @author Catalin
 * @since 23.1
 *
 */
public class MethodCall {

    /**
     * Parameters.
     */
    private final ConnectorJavaMethodParameter parameter;

    /**
     * Method to be called.
     */
    private Method method;

    /**
     * Constructor.
     *
     * @param parameter parameter
     */
    public MethodCall(final ConnectorJavaMethodParameter parameter) {
        super();
        this.parameter = parameter;
        try {
            final Class<?> clazz = Class.forName(parameter.getClassName());
            this.method = clazz.getMethod(parameter.getMethodName(),
                toJavaType(parameter.getParameters()));
        } catch (final ClassNotFoundException e) {
            final String message = "Class for java method step in connector not found : "
                    + parameter.getClassName();
            throw new ConfigurationException(message, e);
        } catch (final NoSuchMethodException e) {
            final String message = "Method for java method step in connector not found : "
                    + parameter.getClassName() + '#' + parameter.getMethodName();
            throw new ConfigurationException(message, e);
        } catch (final SecurityException e) {
            final String message = "Method for java method step in connector inaccessible : "
                    + parameter.getClassName() + '#' + parameter.getMethodName();
            throw new ConfigurationException(message, e);
        }
    }

    /**
     * Converts type names to Java types.
     *
     * @param map type names map
     * @return Java classes array
     */
    private Class<?>[] toJavaType(final Map<String, JavaType> map) {
        final Class<?>[] clazz = new Class<?>[map.size()];
        final Iterator<String> iter = map.keySet().iterator();
        int index = 0;
        while (iter.hasNext()) {
            final String id = iter.next();
            clazz[index++] = map.get(id).getType();
        }
        return clazz;
    }

    /**
     *
     * Execute method.
     *
     * @param args arguments
     * @return returned object
     */
    public Object execute(final Object[] args) {
        try {
            return this.method.invoke(null, args);
        } catch (final IllegalAccessException e) {
            final String message = "Unable to execute method: "
                    + this.method.getDeclaringClass().getName() + '#' + this.method.getName();
            throw new ConfigurationException(message, e);
        } catch (final IllegalArgumentException e) {
            final String message = "Method should not require parameters, but does: "
                    + this.method.getDeclaringClass().getName() + '#' + this.method.getName();
            throw new ConfigurationException(message, e);
        } catch (final InvocationTargetException e) {
            final String message = "Error executing method: "
                    + this.method.getDeclaringClass().getName() + '#' + this.method.getName();
            throw new MethodExecutionException(message, e.getCause());
        }
    }

    /**
     * Getter for the method property.
     *
     * @return the method property.
     */
    public Method getMethod() {
        return this.method;
    }

    /**
     * JSON object to map of types.
     *
     * @param params parameter
     * @return map of types
     */
    public static Map<String, JavaType> toType(final JSONArray params) {
        final Map<String, JavaType> resultMap = new LinkedHashMap<String, JavaType>();
        for (int i = 0; i < params.length(); i++) {
            final JSONObject param = (JSONObject) params.get(i);
            final String key = param.keys().next().toString();
            resultMap.put(key, JavaType.valueOf(param.getString(key).toUpperCase()));
        }
        return resultMap;
    }

    /**
     * Getter for the parameter property.
     *
     * @return the parameter property.
     */
    ConnectorJavaMethodParameter getParameter() {
        return this.parameter;
    }

    /**
     * Setter for the method property.
     *
     * @param method the method to set.
     */
    void setMethod(final Method method) {
        this.method = method;
    }

}
