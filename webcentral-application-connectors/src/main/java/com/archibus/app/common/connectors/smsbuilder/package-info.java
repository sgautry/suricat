/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides Sms Builder Master connector Job implementation.
 *
 * @author Catalin
 * @since 23.2
 *
 */
package com.archibus.app.common.connectors.smsbuilder;