/**
 * Contains Archibus table/field definition.
 */
package com.archibus.app.common.connectors.smsbuilder.table;