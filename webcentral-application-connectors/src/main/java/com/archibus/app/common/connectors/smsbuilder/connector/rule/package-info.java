/**
 * Contains connector rules specific to Builder SMS.
 */
package com.archibus.app.common.connectors.smsbuilder.connector.rule;