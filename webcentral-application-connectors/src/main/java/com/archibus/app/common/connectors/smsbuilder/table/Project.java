package com.archibus.app.common.connectors.smsbuilder.table;

import com.archibus.datasource.*;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides TODO. - if it has behavior (service), or Represents TODO. - if it has state (entity,
 * domain object, model object). Utility class. Provides methods TODO. Interface to be implemented
 * by classes that TODO.
 * <p>
 *
 * Used by TODO to TODO. Managed by Spring, has prototype TODO singleton scope. Configured in TODO
 * file.
 *
 * @author Catalin
 * @since 23.1
 *
 */
public final class Project {
    /**
     * Table name.
     */
    public static final String TABLE_NAME = "project";

    /**
     * Field name.
     */
    public static final Field PROJECT_ID = new Field(TABLE_NAME, "project_id");

    /**
     * Field name.
     */
    public static final Field BL_ID = new Field(TABLE_NAME, "bl_id");

    /**
     * Field name.
     */
    public static final Field STATUS = new Field(TABLE_NAME, "status");

    /**
     * Field name.
     */
    public static final Field PROJECT_TYPE = new Field(TABLE_NAME, "project_type");

    /**
     * Field name.
     */
    public static final Field SITE_ID = new Field(TABLE_NAME, "site_id");

    /**
     * Field name.
     */
    public static final Field CONTACT_ID = new Field(TABLE_NAME, "contact_id");

    /**
     * Field name.
     */
    public static final Field DESCRIPTION = new Field(TABLE_NAME, "description");

    /**
     * Field name.
     */
    public static final Field SOURCE_SYSTEM_ID = new Field(TABLE_NAME, "source_system_id");

    /**
     * Field name.
     */
    public static final Field SOURCE_TABLE = new Field(TABLE_NAME, "source_table");

    /**
     * Field name.
     */
    public static final Field SOURCE_TIME_UPDATE = new Field(TABLE_NAME, "source_time_update");

    /**
     * Field name.
     */
    public static final Field SOURCE_DATE_UPDATE = new Field(TABLE_NAME, "source_date_update");

    /**
     * Field name.
     */
    public static final Field SOURCE_RECORD_ID = new Field(TABLE_NAME, "source_record_id");

    /**
     * Constructor.
     */
    private Project() {
        super();
    }

    /**
     * Create datasource for all fields.
     *
     * @return DataSource
     */
    public static DataSource createDataSource() {
        return DataSourceFactory.createDataSourceForFields(TABLE_NAME,
            new String[] { PROJECT_ID.getName(), PROJECT_TYPE.getName(), BL_ID.getName(),
                    SITE_ID.getName(), STATUS.getName(), DESCRIPTION.getName(),
                    CONTACT_ID.getName(), SOURCE_SYSTEM_ID.getName(), SOURCE_TABLE.getName(),
                    SOURCE_TIME_UPDATE.getName(), SOURCE_DATE_UPDATE.getName(), SOURCE_RECORD_ID.getName() });
    }

}
