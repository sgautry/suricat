package com.archibus.app.common.connectors.smsbuilder;

import java.text.DecimalFormat;
import java.util.*;

import org.apache.log4j.Logger;

import com.archibus.app.common.connectors.domain.*;
import com.archibus.app.common.connectors.exception.ConfigurationException;
import com.archibus.context.ContextStore;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;
import com.archibus.eventhandler.EventHandlerBase;
import com.archibus.model.config.*;
import com.archibus.schema.NumericFormat;
import com.archibus.schema.TableDef.ThreadSafe;
import com.archibus.utility.*;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Post/pre processing rules for Builder SMS connectors.
 *
 * @author Catalin
 * @since 23.1
 *
 */
public final class DataProcess {

    /**
     * Logger.
     */
    private static final Logger LOGGER = Logger.getLogger(DataProcess.class);

    /**
     * Staging table.
     */
    private static final String STAGING = "staging";

    /**
     * Staging table.
     */
    private static final String COMPLEX = "complex";

    /**
     * Field name.
     */
    private static final String COMPLEX_ID = "complex_id";

    /**
     * Org table.
     */
    private static final String ORG = "org";

    /**
     * Field name.
     */
    private static final String STAGING_PARENT = "staging_parent";

    /**
     * Field name.
     */
    private static final String STAGING_CHILD = "staging_child";

    /**
     * Field name.
     */
    private static final String SOURCE_RECORD_ID = "source_record_id";

    /**
     * Field name.
     */
    private static final String STAGING_KEY = "staging_key";

    /**
     * Field name.
     */
    private static final String ORG_ID = "org_id";

    /**
     * Decimal format.
     */
    private static final String DECIMAL_FORMAT = "#.##";

    /**
     * Private default constructor: utility class is non-instantiable.
     */
    private DataProcess() {
        super();
    }

    /**
     *
     * Update foreign keys values.
     *
     * @param connectorConfig connector config
     * @throws ConfigurationException exception
     */
    public static void postProcessUpdateCsiDescription(final ConnectorConfig connectorConfig)
            throws ConfigurationException {
        SqlUtils.executeUpdate("csi",
            "UPDATE csi SET csi.description = (SELECT staging.staging_value FROM staging WHERE staging.staging_key=csi.description) WHERE EXISTS (SELECT 1 FROM staging WHERE staging.staging_key=csi.description)");
        SqlUtils.commit();
    }

    /**
     * Generates missing GUID for source_record_id field if NULL.
     *
     * @param connectorConfig connector config
     */
    public static void generateGUID(final ConnectorConfig connectorConfig) {
        final DataSource dataS =
                DataSourceFactory.createDataSourceForTable(connectorConfig.getArchibusTable());
        if (StringUtil.notNullOrEmpty(connectorConfig.getClause())) {
            dataS.addRestriction(Restrictions.sql(connectorConfig.getClause()));
        }
        dataS.addRestriction(Restrictions.sql("source_record_id  IS NULL"));
        final List<DataRecord> records = dataS.getRecords();
        for (final DataRecord record : records) {
            record.setValue(toFullName(connectorConfig.getArchibusTable(), SOURCE_RECORD_ID),
                UUID.randomUUID().toString().toUpperCase());
            dataS.saveRecord(record);
        }
        SqlUtils.commit();
    }

    /**
     * Generates missing GUID.
     *
     * @param connectorConfig connector config
     */
    public static void generateAllGUID(final ConnectorConfig connectorConfig) {
        final DataSource dataS =
                DataSourceFactory.createDataSourceForTable(connectorConfig.getArchibusTable());
        dataS.addRestriction(Restrictions.sql("source_record_id IS NULL"));
        final List<DataRecord> records = dataS.getRecords();
        for (final DataRecord record : records) {
            record.setValue(toFullName(connectorConfig.getArchibusTable(), SOURCE_RECORD_ID),
                UUID.randomUUID().toString().toUpperCase());
            dataS.saveRecord(record);
        }
        SqlUtils.commit();
    }

    /**
     *
     * Convert fields values to project units.
     *
     * @param connectorConfig connector's config
     * @throws ExceptionBase exception
     *
     */
    public static void convertToUnit(final ConnectorConfig connectorConfig) throws ExceptionBase {
        final Units sourceUnits = Units.Metric;
        final Units targetUnits = ContextStore.get().getProject().getUnits();
        if (sourceUnits.equals(targetUnits)) {
            return;
        }

        final ThreadSafe tableDef =
                ContextStore.get().getProject().loadTableDef(connectorConfig.getArchibusTable());

        for (final ConnectorFieldConfig confField : connectorConfig.getConnectorFields()) {
            final NumericFormat numFormat =
                    tableDef.getFieldDef(confField.getArchibusField()).getNumericFormat();
            Measure measure = null;
            if (numFormat == NumericFormat.LENGTH) {
                measure = Measure.LENGTH;
            } else if (numFormat == NumericFormat.AREA) {
                measure = Measure.AREA;
            }

            if (measure != null) {

                final UnitsProperties unitsProperties = new UnitsProperties(sourceUnits, measure);

                SqlUtils.executeUpdate(connectorConfig.getArchibusTable(),
                    String.format("UPDATE %s SET %s = %s*%f WHERE source_system_id='SMS-Builder'",
                        connectorConfig.getArchibusTable(), confField.getArchibusField(),
                        confField.getArchibusField(), unitsProperties.getConversionFactor()));
            }
        }
        SqlUtils.commit();
    }

    /**
     *
     * Convert units into staging table.
     *
     * @param connectorConfig connector config
     * @throws ExceptionBase exception
     */
    public static void convertToUnitInStaging(final ConnectorConfig connectorConfig)
            throws ExceptionBase {

        final Units targetUnits = Units.Metric;
        final Units sourceUnits = ContextStore.get().getProject().getUnits();
        if (sourceUnits.equals(targetUnits)) {
            return;
        }

        truncateStaging();

        final double lengthConversionFactor =
                new UnitsProperties(sourceUnits, Measure.LENGTH).getConversionFactor();
        final double areaConversionFactor =
                new UnitsProperties(sourceUnits, Measure.AREA).getConversionFactor();

        final DataSource blDs = DataSourceFactory.createDataSourceForFields("bl",
            new String[] { "bl_id", SOURCE_RECORD_ID, "perimeter", "area_gross_int" });
        final List<DataRecord> blRecords = blDs.getRecords();

        final DataSource stagingDs = DataSourceFactory.createDataSourceForFields(STAGING,
            new String[] { STAGING_KEY, "staging_value_area", "staging_value_length" });

        final DataRecord stagingRecord = stagingDs.createNewRecord();

        for (final DataRecord blRecord : blRecords) {
            stagingRecord.setValue("staging.staging_key",
                blRecord.getString("bl.source_record_id"));
            stagingRecord.setValue("staging.staging_value_length",
                Double.valueOf(new DecimalFormat(DECIMAL_FORMAT)
                    .format(blRecord.getDouble("bl.perimeter") * lengthConversionFactor)));
            stagingRecord.setValue("staging.staging_value_area",
                Double.valueOf(new DecimalFormat(DECIMAL_FORMAT)
                    .format(blRecord.getDouble("bl.area_gross_int") * areaConversionFactor)));
            stagingDs.saveRecord(stagingRecord);
        }

        SqlUtils.commit();
    }

    /**
     *
     * @param connectorConfig connector config
     */
    public static void archibusToSmsHierarchy(final ConnectorConfig connectorConfig) {

        truncateStaging();

        final DataSource orgDs = DataSourceFactory.createDataSourceForFields(ORG,
            new String[] { ORG_ID, SOURCE_RECORD_ID, "hierarchy_ids" });
        final List<DataRecord> records = orgDs.getRecords();

        DataSourceFactory.createDataSourceForFields(STAGING,
            new String[] { STAGING_KEY, STAGING_PARENT, STAGING_CHILD });
        for (final DataRecord record : records) {
            final String hierarchy = record.getString("org.hierarchy_ids");
            if (hierarchy.contains("|")) {
                final List<String> nodes = Arrays.asList(hierarchy.split("\\|"));

                String parent = "";
                String child = "";
                if (nodes.isEmpty() || nodes.size() == 1) {
                    child = record.getString("org.source_record_id");
                    saveStaging(child, parent, child);
                } else {
                    for (final String node : nodes) {
                        child = getSourceRecordId(ORG, ORG_ID, node);
                        saveStaging(child, parent, child);
                        parent = child;
                    }
                }
            }
        }

        SqlUtils.commit();
    }

    /**
     *
     * SaveStaging into staging table.
     *
     * @param key key
     * @param parent parent
     * @param child child
     */
    private static void saveStaging(final String key, final String parent, final String child) {
        final DataSource stagingDs = DataSourceFactory.createDataSourceForFields(STAGING,
            new String[] { STAGING_KEY, STAGING_PARENT, STAGING_CHILD });
        final DataRecord stagingRecord = stagingDs.createNewRecord();
        stagingRecord.setValue(toFullName(STAGING, STAGING_KEY), key);
        stagingRecord.setValue(toFullName(STAGING, STAGING_PARENT), parent);
        stagingRecord.setValue(toFullName(STAGING, STAGING_CHILD), child);
        stagingDs.updateRecord(stagingRecord);
    }

    /**
     *
     * Full name.
     *
     * @param tableName table name
     * @param fieldName field name
     * @return full name
     */
    private static String toFullName(final String tableName, final String fieldName) {
        return tableName + "." + fieldName;
    }

    /**
     * Return the source record ID of the primary key.
     *
     * @param tableName table name
     * @param pkFieldName pk field name
     * @param pkValue pk value
     * @return source record id
     */
    private static String getSourceRecordId(final String tableName, final String pkFieldName,
            final String pkValue) {
        final DataSource orgDs = DataSourceFactory.createDataSourceForFields(tableName,
            new String[] { pkFieldName, SOURCE_RECORD_ID });
        orgDs.addRestriction(Restrictions.eq(tableName, pkFieldName, pkValue));
        return orgDs.getRecord() == null ? ""
                : orgDs.getRecord().getString(toFullName(tableName, SOURCE_RECORD_ID));
    }

    /**
     * Truncate staging the table.
     */
    public static void truncateStaging() {
        EventHandlerBase.executeDbSql(ContextStore.get().getEventHandlerContext(),
            "TRUNCATE TABLE staging", false);
    }

    /**
     *
     * Sets the Organization Root.
     *
     * @param connectorConfig connector config
     */
    public static void setOrganizationRoot(final ConnectorConfig connectorConfig) {
        final DataSource orgRootDs = DataSourceFactory.createDataSourceForFields(STAGING,
            new String[] { STAGING_KEY, "staging_value" });
        orgRootDs.addRestriction(Restrictions.eq(STAGING, STAGING_KEY, "SMS_ORG_ROOT"));
        final DataRecord rec = orgRootDs.getRecord();
        final String orgRootGUID = rec == null ? null : rec.getString("staging.staging_value");
        if (StringUtil.notNullOrEmpty(orgRootGUID)) {
            final DataSource stagingDs = DataSourceFactory.createDataSourceForFields(STAGING,
                new String[] { STAGING_KEY, STAGING_PARENT });
            stagingDs.addRestriction(Restrictions.sql("staging_parent IS NULL"));
            final List<DataRecord> records = stagingDs.getRecords();
            for (final DataRecord record : records) {
                record.setValue("staging.staging_parent", orgRootGUID);
                stagingDs.saveRecord(record);
            }
        }
        SqlUtils.commit();

    }

    /**
     * Create unassigned complexes for Buildings.
     *
     * @param connectorConfig config
     */
    public static void createUnassignedComplex(final ConnectorConfig connectorConfig) {
        final DataSource complexDs = DataSourceFactory.createDataSourceForFields(COMPLEX,
            new String[] { COMPLEX_ID, SOURCE_RECORD_ID, "name" });
        final DataSource blDs = DataSourceFactory.createDataSourceForFields("bl",
            new String[] { "bl_id", COMPLEX_ID, SOURCE_RECORD_ID });
        blDs.addRestriction(Restrictions.eq("bl", COMPLEX_ID, ""));

        final List<DataRecord> records = blDs.getRecords();
        for (final DataRecord blRecord : records) {
            final String unassignedComplexGuid = UUID.randomUUID().toString();
            final DataRecord unassignedComplexRecord = complexDs.createNewRecord();
            unassignedComplexRecord.setValue("complex.complex_id", unassignedComplexGuid);
            unassignedComplexRecord.setValue("complex.source_record_id", unassignedComplexGuid);
            unassignedComplexRecord.setValue("complex.name", "Unassigned");
            complexDs.saveRecord(unassignedComplexRecord);
            blRecord.setValue("bl.complex_id", unassignedComplexGuid);
            blDs.saveRecord(blRecord);
        }

        SqlUtils.commit();

    }
}
