package com.archibus.app.common.connectors.smsbuilder.projmgmt;

import java.util.*;

import org.apache.log4j.Logger;

import com.archibus.app.common.connectors.service.exception.ConnectorException;
import com.archibus.app.common.connectors.smsbuilder.table.*;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;
import com.archibus.utility.StringUtil;
import com.archibus.utility.Utility;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Builder SMS project management specific methods.
 *
 * @author Catalin
 * @since 23.1
 *
 */
public final class ProjectMgmt {
    /**
     * Constant.
     */
    private static final String DASH = "-";

    /**
     * Logger.
     */
    private static final Logger LOGGER = Logger.getLogger(ProjectMgmt.class);

    /**
     * Create or get a project for inspection.
     *
     * @param projectId project ID
     * @param fiscalYear fiscal year
     * @param siteId Site ID
     * @return project ID
     */
    public static String getProjectId(final String projectId, final String fiscalYear,
            final String siteId) {

        return StringUtil.notNullOrEmpty(projectId) ? projectId : nextProjectId(fiscalYear, siteId);
    }

    public static void deleteFromActivityLog() {
        SqlUtils.executeUpdate("activity_log",
            "DELETE FROM activity_log WHERE activity_log.source_system_id = 'SMS-Builder' "
                    + "AND activity_log.source_table = 'WorkItem' "
                    + "AND activity_log.project_id like '%BUILDER%' "
                    + "AND activity_log.source_record_id IS NOT NULL "
                    + "AND NOT EXISTS(SELECT 1 FROM project WHERE project.source_record_id = activity_log.source_record_id)");
    }

    /**
     *
     * Calculate Funding Amount.
     *
     * @param fundId funding ID
     * @return funding amount
     */
    public static Double calculateFundingAmount(final String fundId) {
        final DataSource dsStaging = Staging.createDataSource();
        dsStaging.addRestriction(
            Restrictions.eq(Staging.TABLE_NAME, Staging.STAGING_VALUE_2.getName(), fundId));
        dsStaging.addRestriction(Restrictions.eq(Staging.TABLE_NAME,
            Staging.STAGING_VALUE.getName(), "$FUNDING_AMOUNT$"));
        final List<DataRecord> records = dsStaging.getRecords();
        Double total = 0.00;
        for (final DataRecord record : records) {
            String value = record.getString(Staging.STAGING_VALUE_1.getFullName());
            value = StringUtil.isNullOrEmpty(value) ? "0.00" : value;
            total += Double.valueOf(value);
        }
        return total;
    }

    /**
     * Calculate next project id.
     *
     * @param fiscalYear fiscal year
     * @param siteId site id
     * @return project ID
     */
    private static String nextProjectId(final String fiscalYear, final String siteId) {
        final String projectId = fiscalYear + DASH + siteId + DASH + "BUILDER";
        int count = DataStatistics.getInt(Project.TABLE_NAME, Project.PROJECT_ID.getName(), "COUNT",
            Restrictions.like(Project.TABLE_NAME, Project.PROJECT_ID.getName(), projectId + "%"));
        count++;
        return count > 0 && count < 10 ? projectId + "00" + count
                : count >= 10 && count < 100 ? projectId + "0" + count : projectId + count;
    }

    /**
     * get project start date.
     *
     * @param fiscalYear fiscal year
     * @return date
     */
    public static Date getProjectStartDate(final String fiscalYear) {
        final DataRecord record = AfmScmPref.createDataSource().getRecord();
        final int day = record.getInt(AfmScmPref.FISCAL_YEAR_START_DAY.getFullName());
        final int month = record.getInt(AfmScmPref.FISCAL_YEAR_START_MONTH.getFullName());
        if (day <= 0 || month <= 0) {
            throw new ConnectorException(
                "Error while calculating the project start date. Please set the fiscal year start month and day in afm_scmpref table.",
                null);
        }
        return new GregorianCalendar(Integer.valueOf(fiscalYear), month, day).getTime();
    }

    public static Date getProjectEndDate(final String fiscalYear) {
        final DataRecord record = AfmScmPref.createDataSource().getRecord();
        final int day = record.getInt(AfmScmPref.FISCAL_YEAR_START_DAY.getFullName());
        final int month = record.getInt(AfmScmPref.FISCAL_YEAR_START_MONTH.getFullName());
        if (day <= 0 || month <= 0) {
            throw new ConnectorException(
                "Error while calculating the project end date. Please set the fiscal year start month and day in afm_scmpref table.",
                null);
        }
        final Calendar calendar = new GregorianCalendar(Integer.valueOf(fiscalYear), month, day);
        calendar.add(GregorianCalendar.DATE, 364);
        return calendar.getTime();

    }

    public static Date getProjectTargetEndDate(final String completionYear) {
        final DataRecord record = AfmScmPref.createDataSource().getRecord();
        final int day = record.getInt(AfmScmPref.FISCAL_YEAR_START_DAY.getFullName());
        final int month = record.getInt(AfmScmPref.FISCAL_YEAR_START_MONTH.getFullName());
        if (day <= 0 || month <= 0) {
            throw new ConnectorException(
                "Error while calculating the target end date. Please set the fiscal year start month and day in afm_scmpref table.",
                null);
        }
        final Calendar calendar =
                new GregorianCalendar(Integer.valueOf(completionYear), month, day);
        calendar.add(GregorianCalendar.DATE, -1);
        return calendar.getTime();

    }

    /**
     * Create or get a project for inspection.
     *
     * @param blId building ID
     * @param siteId Site ID
     * @return project ID
     */
    public static String getOrCreateProjectForBuilding(final String blId, final String siteId) {

        if (StringUtil.isNullOrEmpty(blId) || StringUtil.isNullOrEmpty(siteId)) {
            return "";
        }
        final String projectId =
                Calendar.getInstance().get(Calendar.YEAR) + DASH + blId + DASH + "BUILDER";
        final DataSource projectDs = Project.createDataSource();

        // buildRestrictionForProject(projectDs, blId, projectId, statusNotIn);

        final List<DataRecord> records = projectDs.getRecords();

        DataRecord projectRecord = null;
        if (records.isEmpty()) {
            projectRecord = createProjectRecord(projectDs, blId, projectId, siteId);
            LOGGER.info("New project record(" + projectId + ") created.");
        } else {
            projectRecord = records.get(0);
            LOGGER.info("Project " + projectId + " found.");
        }

        return projectRecord.getString(Project.PROJECT_ID.getFullName());

    }

    /**
     *
     * Create new project record.
     *
     * @param projectDs ds
     * @param blId bl
     * @param projectId project
     * @param siteId site
     * @return data record
     */
    private static DataRecord createProjectRecord(final DataSource projectDs, final String blId,
            final String projectId, final String siteId) {
        projectDs.clearRestrictions();
        final DataRecord record = projectDs.createNewRecord();
        record.setValue(Project.PROJECT_ID.getFullName(), projectId);
        record.setValue(Project.BL_ID.getFullName(), blId);
        record.setValue(Project.STATUS.getFullName(), "Created");
        record.setValue(Project.PROJECT_TYPE.getFullName(), "Repair");
        record.setValue(Project.SITE_ID.getFullName(), siteId);
        record.setValue(Project.CONTACT_ID.getFullName(), "TBD");
        record.setValue(Project.SOURCE_SYSTEM_ID.getFullName(), "SMS-Builder");
        record.setValue(Project.SOURCE_TABLE.getFullName(), "WorkItem");
        record.setValue(Project.SOURCE_TIME_UPDATE.getFullName(), Utility.currentTime());
        record.setValue(Project.SOURCE_DATE_UPDATE.getFullName(), Utility.currentDate());
        record.setValue(Project.SOURCE_RECORD_ID.getFullName(),
            UUID.randomUUID().toString().toUpperCase());
        record.setValue(Project.DESCRIPTION.getFullName(),
            "Assessment project contains inspections imported from BUILDER SMS");
        return projectDs.saveRecord(record);
    }

    /**
     *
     * Executes given SQL.
     *
     * @param sql sql
     * @param workRequest work request from SMS Builder
     * @return value
     */
    public static String getWrId(final String sql, final String workRequest) {

        final String wrId;
        if (StringUtil.isNullOrEmpty(workRequest) || !isNumber(workRequest)) {
            wrId = "";
        } else {
            final DataSource dataSource = DataSourceFactory
                    .createDataSourceForFields("wr", new String[] { "wr_id" })
                    .addQuery(String.format(sql, workRequest));
                wrId = dataSource.getRecord() == null ? null : dataSource.getRecord().getString("wr.wr_id");
        }
        return wrId;
    }

    /**
     *
     * Checks if the value is a number.
     * 
     * @param value value
     * @return boolean
     */
    public static boolean isNumber(final String value) {
        try {
            Long.parseLong(value);
        } catch (final NumberFormatException e) {
            return false;
        }
        return true;
    }

}
