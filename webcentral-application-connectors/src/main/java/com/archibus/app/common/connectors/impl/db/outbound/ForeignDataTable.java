package com.archibus.app.common.connectors.impl.db.outbound;

import java.util.*;

import org.json.*;

import com.archibus.app.common.connectors.domain.ConnectorConfig;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * An data table constructed from a connector configuration.
 *
 * @author Catalin
 * @since 23.3
 *
 */
public final class ForeignDataTable {

    /**
     * A connector parameter that when set to false will not permit records to be updated (insert
     * only). Default: true.
     */
    private static final String SUPPORT_UPDATES_PARAM = "supportUpdates";

    /**
     * A connector parameter that when set to false will not permit records to be inserted (update
     * only). Default: true.
     */
    private static final String SUPPORT_INSERTS_PARAM = "supportInserts";
    
    /**
     * afm_connectors.parameter json key indicating characters to be used for reserved words.
     */
    private static final String IDENTIFIER_TERMINALS_PARAM = "identifierTerminals";

    /**
     * afm_connectors.parameter json key indicating sql errors that can be ignored.
     */
    private static final String IGNORE_SQL_ERROR_CODES_PARAM = "ignoreSqlErrorCodes";

    /**
     * Remote table name.
     */
    private final String foreignTableName;

    /**
     * Whether to support inserts.
     */
    private final boolean supportInsert;

    /**
     * Whether to support updates.
     */
    private final boolean supportUpdate;
    
    /**
     * Identifiers for reserved words.
     */
    private final String[] identifierTerminal;
    
    /**
     * Continue when this error code is thrown by DB engine.
     */
    private final Set<Integer> ignoreErrorCodes;


    private final String catalog;

    private final String schema;

    private final String table;

    private final List<String> fields;

    /**
     * @param connector the connector configuration used to configure the data table.
     */
    public ForeignDataTable(final ConnectorConfig connector, final String catalog,
            final String schema, final String table, final List<String> fields) {
        super();
        this.foreignTableName = connector.getSourceTbl();
        this.supportInsert = connector.getConnParams().optBoolean(SUPPORT_INSERTS_PARAM, true);
        this.supportUpdate = connector.getConnParams().optBoolean(SUPPORT_UPDATES_PARAM, true);
        this.catalog = catalog;
        this.schema = schema;
        this.table = table;
        this.fields = fields;
        this.identifierTerminal = getIdentifierTerminal(connector.getConnParams());
        this.ignoreErrorCodes = getIgnoreErrorCodes(connector.getConnParams());
    }

    /**
     * Getter for the foreignTableName property.
     *
     * @return the foreignTableName property.
     */
    public String getForeignTableName() {
        return this.foreignTableName;
    }

    /**
     * Getter for the supportInsert property.
     *
     * @return the supportInsert property.
     */
    public boolean isSupportInsert() {
        return this.supportInsert;
    }

    /**
     * Getter for the supportUpdate property.
     *
     * @return the supportUpdate property.
     */
    public boolean isSupportUpdate() {
        return this.supportUpdate;
    }

    /**
     * Getter for the catalog property.
     *
     * @return the catalog property.
     */
    public String getCatalog() {
        return this.catalog;
    }

    /**
     * Getter for the schema property.
     *
     * @return the schema property.
     */
    public String getSchema() {
        return this.schema;
    }

    /**
     * Getter for the table property.
     *
     * @return the table property.
     */
    public String getTable() {
        return this.table;
    }

    /**
     * Getter for the fields property.
     *
     * @return the fields property.
     */
    public List<String> getFields() {
        return this.fields;
    }
    
    
    /**
     * Getter for the identifierTerminal property.
     * 
     * @return the identifierTerminal property.
     */
    public String[] getIdentifierTerminal() {
        return this.identifierTerminal;
    }
    
    /**
     * Getter for the ignoreErrorCode property.
     * 
     * @return the ignoreErrorCode property.
     */
    public Set<Integer> getIgnoreErrorCodes() {
        return this.ignoreErrorCodes;
    }

    /**
     * Gets terminal identifiers.
     *
     * @param jsonObject connector's config
     * @return terminals
     */
    private static String[] getIdentifierTerminal(final JSONObject jsonObject) {
        final String[] identifiers = { "", "" };
        if (jsonObject.has(IDENTIFIER_TERMINALS_PARAM)) {
            final String value = jsonObject.getString(IDENTIFIER_TERMINALS_PARAM);
            if (value != null && value.toString().length() == 2) {
                identifiers[0] = String.valueOf(value.charAt(0));
                identifiers[1] = String.valueOf(value.charAt(1));
            }
        }
        return identifiers;
    }

    /**
     * Gets errCodes.
     *
     * @param jsonObject connector's config
     * @return errCodes
     */
    private static Set<Integer> getIgnoreErrorCodes(final JSONObject jsonObject) {
        Set<Integer> sqlCodes = new HashSet<Integer>();
        if (jsonObject.has(IGNORE_SQL_ERROR_CODES_PARAM)) {
            final JSONArray values = jsonObject.getJSONArray(IGNORE_SQL_ERROR_CODES_PARAM);
            for (int i = 0; i < values.length(); i++) {
                sqlCodes.add(values.optInt(i));
            }
        }
        return sqlCodes;
    }
    
}
