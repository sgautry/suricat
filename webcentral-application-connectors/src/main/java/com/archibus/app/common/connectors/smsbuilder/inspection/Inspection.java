package com.archibus.app.common.connectors.smsbuilder.inspection;

import java.util.*;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.archibus.app.common.connectors.smsbuilder.table.*;
import com.archibus.context.ContextStore;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;
import com.archibus.utility.*;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Custom rules for Inspections.
 *
 * @author Catalin
 * @since 23.1
 *
 */
public final class Inspection {
    /**
     * Logger.
     */
    private static final Logger LOGGER = Logger.getLogger(Inspection.class);

    /**
     * Project type.
     */
    private static final String ASSESSMENT = "ASSESSMENT";

    /**
     * Constant.
     */
    private static final String DASH = "-";

    /**
     * Private default constructor: utility class is non-instantiable.
     */
    private Inspection() {
        super();
    }

    /**
     * Create or get a project for inspection.
     *
     * @param blId building ID
     * @param siteId Site ID
     * @return project ID
     */
    public static String getOrCreateProjectForBuilding(final String blId, final String siteId) {

        if (StringUtil.isNullOrEmpty(blId) || StringUtil.isNullOrEmpty(siteId)) {
            return "";
        }
        final String projectId =
                Calendar.getInstance().get(Calendar.YEAR) + DASH + blId + DASH + "BUILDER";
        final String statusNotIn =
                "('Requested-Rejected','Approved-Cancelled','Issued-Stopped','Completed-Pending','Completed-Not Ver','Completed-Verified','Closed')";

        final DataSource projectDs = Project.createDataSource();

        buildRestrictionForProject(projectDs, blId, projectId, statusNotIn);

        final List<DataRecord> records = projectDs.getRecords();

        DataRecord projectRecord = null;
        if (records.isEmpty()) {
            final String nextProjectId = nextProjectId(projectId);
            projectRecord = createProjectRecord(projectDs, blId, nextProjectId, siteId);
            LOGGER.info("New project record(" + nextProjectId + ") created.");
        } else {
            projectRecord = records.get(0);
            LOGGER.info("Project " + projectRecord.getString(Project.PROJECT_ID.getFullName()) + " found.");
        }

        return projectRecord.getString(Project.PROJECT_ID.getFullName());

    }
    
    /**
     * Calculate next project id.
     *
     * @return project ID
     */
    private static String nextProjectId(final String projectId) {
        int count = DataStatistics.getInt(Project.TABLE_NAME, Project.PROJECT_ID.getName(), "COUNT",
            Restrictions.like(Project.TABLE_NAME, Project.PROJECT_ID.getName(), projectId + "%"));
        count++;
        return count > 0 && count < 10 ? projectId + "00" + count
                : count >= 10 && count < 100 ? projectId + "0" + count : projectId + count;
    }


    public static Date getAssessedDate() {
        return new Date();
    }

    /**
     *
     * Return user name to be used in BUILDER SMS.
     *
     * @param inspectionLink inspection link
     * @return user name
     */
    public static String getInspector(final String email) {
        return getSmsUserByEmail(email).getString(Staging.STAGING_KEY.getFullName());
    }

    /**
     *
     * Return user name to be used in Archibus.
     *
     * @param inspectionLink inspection link
     * @return user name
     */
    public static String getAssessedBy(final String inspectionLink) {
        final String smsUserEmail =
                getSmsUserByKey(inspectionLink).getString(Staging.STAGING_VALUE_1.getFullName());
        if (StringUtil.notNullOrEmpty(smsUserEmail)) {
            final String user = getUserNameByEmail(smsUserEmail);
            if (StringUtil.notNullOrEmpty(user)) {
                return user;
            }
        }
        return ContextStore.get().getUser().getName();
    }

    /**
     *
     * @param email email
     * @return user name
     */
    private static String getUserNameByEmail(final String email) {
        final Map<String, String> archibusUsers = getArchibusUsers();
        final Iterator<Entry<String, String>> iter = archibusUsers.entrySet().iterator();
        while (iter.hasNext()) {
            final Entry<String, String> element = iter.next();
            if (email.equalsIgnoreCase(element.getValue())) {
                return element.getKey();
            }
        }
        return null;
    }

    /**
     * Add restriction to datasource.
     *
     * @param projectDs datasource
     * @param blId bl
     * @param projectId project id
     * @param statusNotIn status
     */
    private static void buildRestrictionForProject(final DataSource projectDs, final String blId,
            final String projectId, final String statusNotIn) {
        projectDs.addRestriction(
            Restrictions.eq(Project.TABLE_NAME, Project.PROJECT_TYPE.getName(), ASSESSMENT));
        projectDs
            .addRestriction(Restrictions.eq(Project.TABLE_NAME, Project.BL_ID.getName(), blId));
        projectDs.addRestriction(
            Restrictions.like(Project.TABLE_NAME, Project.PROJECT_ID.getName(), projectId));
        projectDs.addRestriction(
            Restrictions.notIn(Project.TABLE_NAME, Project.STATUS.getName(), statusNotIn));
    }

    /**
     *
     * Create new project record.
     *
     * @param projectDs ds
     * @param blId bl
     * @param projectId project
     * @param siteId site
     * @return data record
     */
    private static DataRecord createProjectRecord(final DataSource projectDs, final String blId,
            final String projectId, final String siteId) {
        projectDs.clearRestrictions();
        final DataRecord record = projectDs.createNewRecord();
        record.setValue(Project.PROJECT_ID.getFullName(), projectId);
        record.setValue(Project.BL_ID.getFullName(), blId);
        record.setValue(Project.STATUS.getFullName(), "Created");
        record.setValue(Project.PROJECT_TYPE.getFullName(), ASSESSMENT);
        record.setValue(Project.SITE_ID.getFullName(), siteId);
        record.setValue(Project.CONTACT_ID.getFullName(), "TBD");
        record.setValue(Project.DESCRIPTION.getFullName(),
            "Assessment project contains inspections imported from BUILDER SMS");
        record.setValue(Project.SOURCE_SYSTEM_ID.getFullName(), "SMS-Builder");
        record.setValue(Project.SOURCE_TABLE.getFullName(), "Inspection_Data");
        record.setValue(Project.SOURCE_TIME_UPDATE.getFullName(), Utility.currentTime());
        record.setValue(Project.SOURCE_DATE_UPDATE.getFullName(), Utility.currentDate());
        record.setValue(Project.SOURCE_RECORD_ID.getFullName(),
            UUID.randomUUID().toString().toUpperCase());
        return projectDs.saveRecord(record);
    }

    /**
     * Return the user name to be used in inspections.
     *
     * @param inspectorLink inspector ID
     * @return inspector
     */
    private static DataRecord getSmsUserByKey(final String inspectorLink) {
        final DataSource stagingDs = Staging.createDataSource();
        stagingDs.addRestriction(
            Restrictions.eq(Staging.TABLE_NAME, Staging.STAGING_KEY.getName(), inspectorLink));
        stagingDs.addRestriction(
            Restrictions.eq(Staging.TABLE_NAME, Staging.STAGING_VALUE.getName(), "$USER$"));
        stagingDs.addSort(Staging.TABLE_NAME, Staging.STAGING_VALUE_2.getName());
        return stagingDs.getRecord();
    }

    /**
     * Return the user name to be used in inspections.
     *
     * @param inspectorEmail inspector email
     * @return inspector
     */
    private static DataRecord getSmsUserByEmail(final String inspectorEmail) {
        final DataSource stagingDs = Staging.createDataSource();
        stagingDs.addRestriction(
            Restrictions.eq(Staging.TABLE_NAME, Staging.STAGING_VALUE_1.getName(), inspectorEmail));
        stagingDs.addRestriction(
            Restrictions.eq(Staging.TABLE_NAME, Staging.STAGING_VALUE.getName(), "$USER$"));
        stagingDs.addSort(Staging.TABLE_NAME, Staging.STAGING_VALUE_2.getName());
        return stagingDs.getRecord();
    }

    /**
     * Return the user name to be used in inspections.
     *
     * @param inspectorLink inspector ID
     * @return inspector
     */
    private static String[] getSmsUserByName(final String inspectorLink) {
        final DataSource stagingDs = DataSourceFactory.createDataSourceForFields(Staging.TABLE_NAME,
            new String[] { Staging.STAGING_KEY.getName(), Staging.STAGING_VALUE.getName() });
        stagingDs.addRestriction(
            Restrictions.eq(Staging.TABLE_NAME, Staging.STAGING_KEY.getName(), inspectorLink));
        stagingDs.addSort(Staging.TABLE_NAME, Staging.STAGING_VALUE.getName());
        final DataRecord record = stagingDs.getRecord();
        return record == null ? null
                : record.getString(Staging.STAGING_VALUE.getFullName()).split(";");
    }

    /**
     * @return archibus users
     */
    private static Map<String, String> getArchibusUsers() {
        final DataSource archibusUserDs = DataSourceFactory.createDataSourceForFields("afm_users",
            new String[] { "user_name", "email" });
        final List<DataRecord> records = archibusUserDs.getRecords();
        final Map<String, String> archibusUsers = new HashMap<>();

        for (final DataRecord record : records) {
            archibusUsers.put(record.getString("afm_users.user_name"),
                record.getString("afm_users.email"));
        }
        return archibusUsers;
    }

}
