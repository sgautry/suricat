package com.archibus.app.common.connectors.smsbuilder.table;

import com.archibus.app.common.connectors.domain.ConnectorConfig;
import com.archibus.datasource.*;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Temporary table for BUILDER SMS usage.
 *
 * @author Catalin
 * @since 23.1
 *
 */
public final class Staging {

    /**
     * Table name.
     */
    public static final String TABLE_NAME = "staging";

    /**
     * Field name.
     */
    public static final Field STAGING_KEY = new Field(TABLE_NAME, "staging_key");

    /**
     * Field name.
     */
    public static final Field STAGING_VALUE = new Field(TABLE_NAME, "staging_value");

    /**
     * Field name.
     */
    public static final Field STAGING_VALUE_1 = new Field(TABLE_NAME, "staging_value1");

    /**
     * Field name.
     */
    public static final Field STAGING_VALUE_2 = new Field(TABLE_NAME, "staging_value2");

    /**
     * Field name.
     */
    public static final Field STAGING_VALUE_3 = new Field(TABLE_NAME, "staging_value3");

    /**
     * Field name.
     */
    public static final Field STAGING_VALUE_4 = new Field(TABLE_NAME, "staging_value4");

    /**
     * Constructor.
     */
    private Staging() {
        super();
    }

    /**
     * Truncate the table.
     *
     * @param connectorConfig config
     */
    public static void truncate(final ConnectorConfig connectorConfig) {
        SqlUtils.executeUpdate(TABLE_NAME, "TRUNCATE TABLE staging");
    }

    /**
     * Create datasource for all fields.
     *
     * @return DataSource
     */
    public static DataSource createDataSource() {
        return DataSourceFactory.createDataSourceForFields(TABLE_NAME,
            new String[] { STAGING_KEY.getName(), STAGING_VALUE.getName(),
                    STAGING_VALUE_1.getName(), STAGING_VALUE_2.getName(), STAGING_VALUE_3.getName(),
                    STAGING_VALUE_4.getName() });
    }

}
