package com.archibus.app.common.connectors.smsbuilder.connector.rule;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Converts data type names to Java types.
 *
 * @author Catalin
 * @since 23.1
 *
 */
public enum JavaType {

    /**
     * String type.
     */
    STRING(String.class),

    /**
     * Integer type.
     */
    INTEGER(Integer.class);

    /**
     * Class type.
     */
    private final Class<?> type;

    /**
     *
     * Constructor.
     *
     * @param type class type
     */
    JavaType(final Class<?> type) {
        this.type = type;
    }

    /**
     * Getter for the type property.
     *
     * @return the type property.
     */
    public Class<?> getType() {
        return this.type;
    }

}
