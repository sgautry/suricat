package com.archibus.app.common.connectors.impl.archibus.translation.record;

import java.util.Map;

import com.archibus.app.common.connectors.domain.ConnectorFieldConfig;
import com.archibus.app.common.connectors.exception.ConfigurationException;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Truncates a field to the specified size.
 *
 * @author CatalinP
 * @since 23.2
 *
 */
public class Truncate implements IRecordTranslator {
	/**
	 * The field to be truncated.
	 */
	private String fieldKey;

	/**
	 * Size to truncate to.
	 */
	private Integer size;

	/**
	 * Instantiate this rule, completely resetting it's state.
	 *
	 * @param connectorField
	 *            ignored.
	 * @throws ConfigurationException
	 *             if parameter is not specified
	 */
	@Override
	public void init(final ConnectorFieldConfig connectorField) throws ConfigurationException {
		this.fieldKey = connectorField.getFieldId();
		try {
			this.size = Integer.parseInt(connectorField.getParameter());
		} catch (final NumberFormatException e) {
			throw new ConfigurationException(
					connectorField.getFieldId() + "'s parameter must specify an integer size to truncate to", null);
		}
	}

	/**
	 * Truncates the value to the specified size.
	 *
	 * @param record
	 *            the record to be modified (may already be modified by other
	 *            rules).
	 * @param originalRecord
	 *            the record prior to translation by other record level rules.
	 */
	@Override
	public void applyRule(final Map<String, Object> record, final Map<String, Object> originalRecord) {
		final Object value = record.get(this.fieldKey);
		record.put(this.fieldKey,
				value == null ? null
						: (value.toString().length() > this.size) ? value.toString().substring(0, this.size - 1)
								: value.toString());
	}

	/**
	 * @return true as there should be a value to be truncated.
	 */
	@Override
	public boolean requiresExistingValue() {
		return true;
	}
}