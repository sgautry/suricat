/**
 * This package contains API of WorkplacePortalMobileService for Workplace Services Portal mobile
 * application, the Hoteling part.
 */
package com.aremis.immosis;