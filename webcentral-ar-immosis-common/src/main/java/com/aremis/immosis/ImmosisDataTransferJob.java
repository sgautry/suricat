package com.aremis.immosis;

import java.io.*;
import java.nio.file.*;
import java.util.zip.*;

import com.archibus.context.*;
import com.archibus.ext.datatransfer.DataTransferJob;
import com.archibus.utility.ExceptionBase;

//Created 2013-12-13 by AREMIS FK for integration in the SNCF project.
@SuppressWarnings("deprecation")
public class ImmosisDataTransferJob extends DataTransferJob {

	public void uploadFile(int sessionId, String fileName, final InputStream inputStream) {
		try {
			final Context context = ContextStore.get();
			if (sessionId == 0) {
				sessionId = (new ImmosisJob()).createSession();
				this.status.addProperty("newSessionStarted", "true");
			}
			else {
				this.status.addProperty("newSessionStarted", "false");
			}
			this.status.addProperty("sessionId", Integer.toString(sessionId));
			// Correct fileName if needed
			if (fileName.lastIndexOf(File.separator) > -1) {
				fileName = fileName.substring(fileName.lastIndexOf(File.separator) + 1);
			}
			final Path path = Paths.get(context.getWebAppPath() + File.separator + "projects" + File.separator + "immosis" + File.separator + "sessions" + File.separator + "in" + File.separator + Integer.toString(sessionId) + File.separator);
			if (!Files.exists(path)) {
				Files.createDirectories(path);
			}
			this.saveInputStreamToDisk(path.toString() + File.separator + fileName, inputStream);
			if (fileName.endsWith(".zip")) {
				this.unZip(path.toString() + File.separator + fileName, path.toString());
				Files.delete(Paths.get(path.toString() + File.separator + fileName));
			}
		}
		catch (final Exception e) {
			throw new ExceptionBase(String.format("Fail to perform the uploadFile - " + e.getMessage()), e);
		}
	}

	private void saveInputStreamToDisk(final String fileName, final InputStream inputStream) throws IOException {
		try {
			final byte[] buffer = new byte[8 * 1024];
			final OutputStream outputStream = new FileOutputStream(fileName);
			try {
				int bytesRead;
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, bytesRead);
				}
			}
			finally {
				outputStream.close();
			}
		}
		finally {
			inputStream.close();
		}
	}

	protected void unZip(final String zipFile, final String outputFolder) throws Exception {
		final byte[] buffer = new byte[1024];
		ZipInputStream zipInputStream = null;
		ZipEntry zipEntry = null;
		try {
			final File folder = new File(outputFolder);
			if (!folder.exists()) {
				folder.mkdir();
			}
			zipInputStream = new ZipInputStream(new FileInputStream(zipFile));
			zipEntry = zipInputStream.getNextEntry();
			while (zipEntry != null) {
				String fileName = zipEntry.getName();
				if (fileName.length() > 4 && fileName.endsWith(".txt")) {
					if (fileName.indexOf("/") >= 0) {
						fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
					}
					final File newFile = new File(outputFolder + File.separator + fileName);
					final FileOutputStream fileOutputStream = new FileOutputStream(newFile);
					int len;
					while ((len = zipInputStream.read(buffer)) > 0) {
						fileOutputStream.write(buffer, 0, len);
					}
					fileOutputStream.close();
				}
				zipEntry = zipInputStream.getNextEntry();
			}
			zipInputStream.closeEntry();
			zipInputStream.close();
		}
		catch (final IOException e) {
			zipInputStream.closeEntry();
			zipInputStream.close();
			throw (new Exception("Unable to unZip file " + zipFile + ".", e));
		}
	}
}
