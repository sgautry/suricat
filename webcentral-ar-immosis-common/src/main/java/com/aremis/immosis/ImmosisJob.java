package com.aremis.immosis;

import java.io.*;
import java.nio.file.*;
import java.sql.*;
import java.text.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.Date;
import java.util.zip.*;

import org.apache.log4j.Logger;

import com.archibus.context.*;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.db.DbConnection;
import com.archibus.eventhandler.EventHandlerBase;
import com.archibus.jobmanager.*;
import com.archibus.model.view.datasource.*;
import com.archibus.schema.FieldJavaDateImpl;
import com.archibus.utility.*;

//Created 2013-12-13 by AREMIS FK for integration in the SNCF project.
public class ImmosisJob extends JobBase {

    public Context	context	= null;

	public class tableSpecification {

		public String			tableName						= "";
		public String[]		fieldNames					= null;
		public String[]		fieldNamesPrefixed	= null;
		public DataSource	dataSource					= null;
		public boolean		importFile					= false;
		public String			expectedFileName		= "";

		public tableSpecification(final String tableName) {
			this(tableName, false);
		}

		public tableSpecification(final String tableName, final boolean importFile) {
			this(tableName, importFile, null);
		}

		public tableSpecification(final String tableName, final boolean importFile, final String[] fieldNames) {
			this.tableName = tableName;
			this.fieldNames = fieldNames;
			if (this.fieldNames == null) {
				// Retrieve the field names from the afm schema
				final DataSource ds = DataSourceFactory.createDataSourceForFields("afm_flds", new String[] { "afm_flds.table_name", "afm_flds.field_name" }).setApplyVpaRestrictions(false).addSort("field_name");
				final ParsedRestrictionDef rest = new ParsedRestrictionDef();
				rest.addClause("afm_flds", "table_name", tableName, ClauseDef.Operation.EQUALS);
				final List<DataRecord> records = ds.getRecords(rest);
				final List<String> fieldNamesList = new ArrayList<String>();
				for (final DataRecord rec : records) {
					fieldNamesList.add(rec.getValue("afm_flds.field_name").toString());
				}
				this.fieldNames = fieldNamesList.toArray(new String[0]);
			}
			// Calculate the fieldNamesPrefixed Map values
			final List<String> fieldNamesPrefixedList = new ArrayList<String>();
			for (final String fieldName : this.fieldNames) {
				fieldNamesPrefixedList.add(this.tableName + "." + fieldName);
			}
			this.fieldNamesPrefixed = fieldNamesPrefixedList.toArray(new String[0]);
			// Create the dataSource
			this.dataSource = DataSourceFactory.createDataSourceForFields(this.tableName, this.fieldNamesPrefixed).setApplyVpaRestrictions(false);
			// Import related settings
			if (this.tableName.startsWith("immosis_in_")) {
				this.importFile = importFile;
				if (this.importFile) {
					this.expectedFileName = this.tableName.substring(11).toUpperCase() + ".txt";
				}
			}
		}
	}
	public Logger													log									= null;
	public Map<String, tableSpecification>	tableSpecifications	= null;
	public int															nrTextFiles					= 0;
	public int															nrFilesImported			= 0;
	public boolean													initialized					= false;

	public void initialize() {
		try {
			this.context = ContextStore.get();
			if (!this.initialized) {
				this.log = Logger.getLogger(this.getClass());
				this.tableSpecifications = new HashMap<String, tableSpecification>();
				// Some utility tables
				this.tableSpecifications.put("immosis_session", new tableSpecification("immosis_session", false, new String[] { "session_id", "user_name", "bl_id", "fl_id", "date_start", "date_end", "time_start", "time_end", "status" }));
				this.tableSpecifications.put("immosis_in_file", new tableSpecification("immosis_in_file", false, new String[] { "session_id", "file_name", "file_size" }));
				this.tableSpecifications.put("immosis_in_data", new tableSpecification("immosis_in_data", false, new String[] { "session_id", "file_name", "line_nr", "line_data" }));
				this.tableSpecifications.put("rm_temp", new tableSpecification("rm_temp"));
				this.tableSpecifications.put("immosis_backup_rm", new tableSpecification("immosis_backup_rm"));
				// The tables that will be imported from the text files
				this.tableSpecifications.put("immosis_in_pdb_zonegare", new tableSpecification("immosis_in_pdb_zonegare", true, new String[] { "session_id", "line_nr", "zonegare_code", "zonegare_lib" }));
				this.tableSpecifications.put("immosis_in_tcp_batiment_l", new tableSpecification("immosis_in_tcp_batiment_l", true, new String[] { "session_id", "line_nr", "bal_cle", "udj_cle", "bap_code", "uto_code" }));
				this.tableSpecifications.put("immosis_in_tcp_batiment_p", new tableSpecification("immosis_in_tcp_batiment_p", true, new String[] { "session_id", "line_nr", "bap_plan_integre", "bap_desc_terminee", "uto_code", "bap_code", "lig_code", "cop_pk", "cop_date_mes", "cop_lib", "tba_code", "emp_cle", "bap_surf_projetee", }));
				this.tableSpecifications.put("immosis_in_tcp_commune", new tableSpecification("immosis_in_tcp_commune", true, new String[] { "session_id", "line_nr", "com_code_insee", "com_lib_insee" }));
				this.tableSpecifications.put("immosis_in_tcp_emploi", new tableSpecification("immosis_in_tcp_emploi", true, new String[] { "session_id", "line_nr", "emp_cle", "emp_lib" }));
				this.tableSpecifications.put("immosis_in_tcp_ligne", new tableSpecification("immosis_in_tcp_ligne", true, new String[] { "session_id", "line_nr", "lig_code", "lig_lib" }));
				this.tableSpecifications.put("immosis_in_tcp_locaux", new tableSpecification("immosis_in_tcp_locaux", true, new String[] { "session_id", "line_nr", "loc_code", "loc_num_ordre", "loc_surface_base", "loc_surface_fiscale", "usa_code", "sod_code", "uco_code_occup", "uco_code_proprio", "loc_effectif", "udj_cle", "tlo_code", "loc_date_mes", "loc_lib", "emp_cle", "zone_gare", "commentaires" }));
				this.tableSpecifications.put("immosis_in_tcp_plaque", new tableSpecification("immosis_in_tcp_plaque", true, new String[] { "session_id", "line_nr", "pla_code", "pla_lib" }));
				this.tableSpecifications.put("immosis_in_tcp_region_sncf", new tableSpecification("immosis_in_tcp_region_sncf", true, new String[] { "session_id", "line_nr", "reg_code", "reg_lib", "reg_adresse", "pla_code" }));
				this.tableSpecifications.put("immosis_in_tcp_sous_domaine", new tableSpecification("immosis_in_tcp_sous_domaine", true, new String[] { "session_id", "line_nr", "sod_code", "sod_lib", "dom_code" }));
				this.tableSpecifications.put("immosis_in_tcp_type_batiment", new tableSpecification("immosis_in_tcp_type_batiment", true, new String[] { "session_id", "line_nr", "tba_code", "tba_lib" }));
				this.tableSpecifications.put("immosis_in_tcp_type_etage", new tableSpecification("immosis_in_tcp_type_etage", true, new String[] { "session_id", "line_nr", "teg_code", "teg_lib" }));
				/* this.tableSpecifications.put("immosis_in_tcp_type_local", new tableSpecification("immosis_in_tcp_type_local", true, new String[] { "session_id", "line_nr", "tlo_code", "tlo_lib", "usa_code" })); */
				this.tableSpecifications.put("immosis_in_tcp_udj", new tableSpecification("immosis_in_tcp_udj", true, new String[] { "session_id", "line_nr", "udj_code", "udj_lib", "com_code_insee", "udj_cle", "udj_principale" }));
				/* this.tableSpecifications.put("immosis_in_tcp_usage", new tableSpecification("immosis_in_tcp_usage", true, new String[] { "session_id", "line_nr", "usa_code", "usa_lib", "dst_code" })); */
				this.tableSpecifications.put("immosis_in_tcp_ut", new tableSpecification("immosis_in_tcp_ut", true, new String[] { "session_id", "line_nr", "uto_code", "reg_code", "uto_lib" }));
			}
			this.initialized = true;
		}
		catch (final Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void importAllFiles(final int sessionId) throws Exception {
		try {
			this.initialize();
			final String folder = Paths.get(this.context.getWebAppPath() + File.separator + "projects" + File.separator + "immosis" + File.separator + "sessions" + File.separator + "in" + File.separator + Integer.toString(sessionId)).toString();
			this.importAllFilesInFolder(sessionId, folder);
			deleteFolder(new File(folder));
		}
		catch (final Exception e) {
			this.handleException("Unable to import all files", e);
		}
	}

	public void startSession(final int sessionId, final boolean hasConfirmed) throws Exception {
		try {
			this.initialize();
			final String blId = this.getBlIdForSession(sessionId);
			final String flId = this.getFlIdForSession(sessionId);
			if (this.existsAnActiveSessionForSameFloor(blId, flId) && !hasConfirmed) { throw (new Exception("Another session is active for the same building and floor.")); }
			this.processAllData(sessionId, blId, flId);
			this.createSessionRollbackPoint(sessionId, blId, flId);
			// Update the status of the session
			final DataSource ds = this.tableSpecifications.get("immosis_session").dataSource;
			final DataRecord rec = ds.getRecord("session_id = " + Integer.toString(sessionId));
			rec.setValue("immosis_session.status", "2_STARTED");
			ds.saveRecord(rec);
		}
		catch (final Exception e) {
			this.handleException("Unable to start the session.", e);
		}
	}

	public void endSession(final int sessionId) throws Exception {
		try {
			this.initialize();
			this.fillExportTables(sessionId);
			this.createExportFiles(sessionId);
			// Update the status of the session
			final DataSource ds = this.tableSpecifications.get("immosis_session").dataSource;
			final DataRecord rec = ds.getRecord("session_id = " + Integer.toString(sessionId));
			rec.getString("immosis_session.status");
			final Date d = new Date();
			rec.setValue("immosis_session.status", "3_ENDED");
			rec.setValue("immosis_session.date_end", d);
			rec.setValue("immosis_session.time_end", new Time(d.getTime()));
			ds.saveRecord(rec);
		}
		catch (final Exception e) {
			this.handleException("Unable to end the session.", e);
		}
	}

	public void cancelSession(final int sessionId) throws Exception {
		try {
			this.initialize();
			final DataSource ds = this.tableSpecifications.get("immosis_session").dataSource;
			final DataRecord rec = ds.getRecord("session_id = " + Integer.toString(sessionId));
			final String status = rec.getString("immosis_session.status");
			if (status.equalsIgnoreCase("2_STARTED")) {
				this.rollBackSession(sessionId);
			}
			// Update the status of the session
			final Date d = new Date();
			rec.setValue("immosis_session.status", "4_CANCELED");
			rec.setValue("immosis_session.date_end", d);
			rec.setValue("immosis_session.time_end", new Time(d.getTime()));
			ds.saveRecord(rec);
		}
		catch (final Exception e) {
			this.handleException("Unable to cancel the session.", e);
		}
	}

	public void importAllFilesInFolder(final int sessionId, final String folder) throws Exception {
		final File dir = new File(folder);
		for (final File file : dir.listFiles()) {
			if (file.isFile() && file.canRead() && file.getName().matches(".+\\.txt")) {
				this.nrTextFiles += 1;
			}
		}
		this.status.setTotalNumber(this.nrTextFiles * 2);
		for (final File file : dir.listFiles()) {
			if (this.stopRequested) {
				break;
			}
			if (file.isFile() && file.canRead() && file.getName().matches(".+\\.txt")) {
				importFile(sessionId, file);
			}
		}		String blId = null;
		String flId = null;
		List<DataRecord> records = null;
		DataRecord rec = null;
		records = this.tableSpecifications.get("immosis_in_tcp_batiment_p").dataSource.getRecords("session_id = " + Integer.toString(sessionId));
		if (records.size() == 0) { throw (new Exception("No building specified in txt files.")); }
		if (records.size() > 1) { throw (new Exception("More than one building specified in txt files.")); }
		rec = records.get(0);
		blId = rec.getString("immosis_in_tcp_batiment_p.uto_code") + rec.getString("immosis_in_tcp_batiment_p.bap_code");
		records = this.tableSpecifications.get("immosis_in_tcp_type_etage").dataSource.getRecords("session_id = " + Integer.toString(sessionId));
		if (records.size() == 0) { throw (new Exception("No floor specified in txt files.")); }
		if (records.size() > 1) { throw (new Exception("More than one floor specified in txt files.")); }
		rec = records.get(0);
		flId = rec.getString("immosis_in_tcp_type_etage.teg_code");
		final DataSource ds = this.tableSpecifications.get("immosis_session").dataSource;
		rec = ds.getRecord("session_id = " + Integer.toString(sessionId));
		rec.setValue("immosis_session.bl_id", blId);
		rec.setValue("immosis_session.fl_id", flId);
		rec.setValue("immosis_session.status", "1_UPLOADING");
		rec = ds.saveRecord(rec);
		this.status.setMessage("");
	}

	public int createSession() {
		this.initialize();
		final String userName = this.context.getUser().getName();
		final DataSource ds = this.tableSpecifications.get("immosis_session").dataSource;
		DataRecord rec = ds.createNewRecord();
		rec.setValue("immosis_session.user_name", userName);
		rec = ds.saveRecord(rec);
		return rec.getInt("immosis_session.session_id");
	}

	public void importFile(final int sessionId, final File file) throws Exception {
		final String fileName = file.getName();
		try {
			final String tableName = "immosis_in_" + fileName.substring(0, fileName.length() - 4).toLowerCase();
			if (this.tableSpecifications.containsKey(tableName) && this.tableSpecifications.get(tableName).importFile) {
				this.status.setCurrentNumber(this.nrFilesImported * 2 + 1);
				// this.status.setMessage("Uploading " + fileName);
				this.status.setMessage("Transfert de fichier " + fileName + " en cours");
				final DataSource ds = this.tableSpecifications.get("immosis_in_file").dataSource;
				DataRecord rec = null;
				rec = ds.getRecord("session_id = " + Integer.toString(sessionId) + " and file_name = '" + fileName + "'");
				if (rec != null) {
					// This file has previously been imported. Remove all existing traces of it
					final EventHandlerContext eventHandlerContext = this.context.getEventHandlerContext();
					String sqlDelete = null;
					// Delete data from the specific table
					if (this.tableSpecifications.containsKey(tableName) && this.tableSpecifications.get(tableName).importFile) {
						sqlDelete = "delete from " + tableName + " where session_id = " + Integer.toString(sessionId);
						EventHandlerBase.executeDbSql(eventHandlerContext, tableName, sqlDelete, false);
					}
					// Delete data from the generic table
					sqlDelete = "delete from immosis_in_data where session_id = " + Integer.toString(sessionId) + " and file_name = '" + fileName + "'";
					EventHandlerBase.executeDbSql(eventHandlerContext, "immosis_in_data", sqlDelete, false);
					// Delete data from the file table
					sqlDelete = "delete from immosis_in_file where session_id = " + Integer.toString(sessionId) + " and file_name = '" + fileName + "'";
					EventHandlerBase.executeDbSql(eventHandlerContext, "immosis_in_file", sqlDelete, false);
				}
				rec = ds.createNewRecord();
				rec.setValue("immosis_in_file.session_id", sessionId);
				rec.setValue("immosis_in_file.file_name", fileName);
				rec.setValue("immosis_in_file.file_size", (int) file.length());
				ds.saveRecord(rec);
				BufferedReader br = null;
				try {
					br = new BufferedReader(new FileReader(file));
				}
				catch (final FileNotFoundException e) {
					this.log.error("The file could not be found", e);
				}
				int lineNr = 0;
				String lineData;
				try {
					while ((lineData = br.readLine()) != null && !this.stopRequested) {
						lineNr += 1;
						importLineGenericTable(sessionId, fileName, lineNr, lineData);
						importLineSpecificTable(sessionId, fileName, lineNr, lineData);
					}
					br.close();
				}
				catch (final IOException e) {
					this.log.error("The file could not be read", e);
				}
				if (!this.stopRequested) {
					this.nrFilesImported += 1;
					this.status.setCurrentNumber(this.nrFilesImported * 2);
					// this.status.setMessage("Uploaded " + fileName);
					this.status.setMessage("Chargé fichier " + fileName);
				}
			}
		}
		catch (final Exception e) {
			this.handleException("Unable to import file " + fileName, e);
		}
	}
	public void importLineGenericTable(final int sessionId, final String fileName, final int lineNr, final String lineData) {
		final String tableName = "immosis_in_" + fileName.substring(0, fileName.length() - 4).toLowerCase();
		if (this.tableSpecifications.containsKey(tableName) && this.tableSpecifications.get(tableName).importFile) {
			final DataSource ds = this.tableSpecifications.get("immosis_in_data").dataSource;
			final DataRecord rec = ds.createNewRecord();
			rec.setValue("immosis_in_data.session_id", sessionId);
			rec.setValue("immosis_in_data.file_name", fileName);
			rec.setValue("immosis_in_data.line_nr", lineNr);
			rec.setValue("immosis_in_data.line_data", lineData);
			ds.saveRecord(rec);
		}
	}
	public void importLineSpecificTable(final int sessionId, final String fileName, final int lineNr, final String lineData) {
		boolean process = false;
		final String tableName = "immosis_in_" + fileName.substring(0, fileName.length() - 4).toLowerCase();
		if (this.tableSpecifications.containsKey(tableName) && this.tableSpecifications.get(tableName).importFile) {
			process = true;
		}
		if (process) {
			final DataSource ds = this.tableSpecifications.get(tableName).dataSource;
			final String[] columnNames = this.tableSpecifications.get(tableName).fieldNamesPrefixed;
			final String[] values = lineData.split("#", -1);
			final DataRecord rec = ds.createNewRecord();
			rec.setValue(tableName + ".session_id", sessionId);
			rec.setValue(tableName + ".line_nr", lineNr);
			for (int i = 2; i < columnNames.length; i++) {
				final String columnName = columnNames[i];
				if (ds.getAllFields().get(i).getFieldDef().getJavaType().getClass().equals(FieldJavaDateImpl.class)) {
					rec.setValue(columnName, this.getDateFromImportString(values[i - 2]));
				}
				else {
					rec.setUiValue(columnName, values[i - 2]);
				}
			}
			ds.saveRecord(rec);
		}
	}
	public String getYYYYMMDD(final String value) {
		if (value == null) { return null; }
		if (value.length() == 0) { return null; }
		return value.substring(6, 10) + "-" + value.substring(3, 5) + "-" + value.substring(0, 2);
	}
	public Date getDateFromImportString(final String value) {
		if (value == null) { return null; }
		if (value.length() == 0) { return null; }
		final int year = Integer.parseInt(value.substring(6, 10), 10);
		final int month = Integer.parseInt(value.substring(3, 5), 10);
		final int date = Integer.parseInt(value.substring(0, 2), 10);
		final Calendar c = Calendar.getInstance();
		c.set(year, month - 1, date, 0, 0, 0);
		return (c.getTime());
	}
	public String getBlIdForSession(final int sessionId) throws Exception {
		this.initialize();
		String blId = null;
		List<DataRecord> records = null;
		DataRecord rec = null;
		records = this.tableSpecifications.get("immosis_in_tcp_batiment_p").dataSource.getRecords("session_id = " + Integer.toString(sessionId));
		if (records.size() == 0) { throw (new Exception("No building specified in txt files.")); }
		if (records.size() > 1) { throw (new Exception("More than one building specified in txt files.")); }
		rec = records.get(0);
		blId = rec.getString("immosis_in_tcp_batiment_p.uto_code") + rec.getString("immosis_in_tcp_batiment_p.bap_code");
		return blId;
	}
	public String getFlIdForSession(final int sessionId) throws Exception {
		this.initialize();
		String flId = null;
		List<DataRecord> records = null;
		DataRecord rec = null;
		records = this.tableSpecifications.get("immosis_in_tcp_type_etage").dataSource.getRecords("session_id = " + Integer.toString(sessionId));
		if (records.size() == 0) { throw (new Exception("No floor specified in txt files.")); }
		if (records.size() > 1) { throw (new Exception("More than one floor specified in txt files.")); }
		rec = records.get(0);
		flId = rec.getString("immosis_in_tcp_type_etage.teg_code");
		return flId;
	}
	public boolean existsAnActiveSessionForSameFloor(final String blId, final String flId) throws Exception {
		this.initialize();
		boolean recordsFound = false;
		List<DataRecord> records = null;
		final ParsedRestrictionDef rest = new ParsedRestrictionDef();
		rest.addClause("immosis_session", "bl_id", blId, ClauseDef.Operation.EQUALS);
		rest.addClause("immosis_session", "fl_id", flId, ClauseDef.Operation.EQUALS);
		rest.addClause("immosis_session", "status", "2_STARTED", ClauseDef.Operation.EQUALS);
		records = this.tableSpecifications.get("immosis_session").dataSource.getRecords(rest);
		if (records.size() > 0) {
			recordsFound = true;
		}
		return recordsFound;
	}
	public void processAllData(final int sessionId, final String blId, final String flId) {
		final EventHandlerContext eventHandlerContext = this.context.getEventHandlerContext();
		// Do all inserts in the right order
		
		
		//Driss BANA   modification de la table pdb_zonegare par   et le champ zonegare_code par rm_std
		
		this.executeInsert("rmstd", "immosis_in_pdb_zonegare", "rm_std, description", "TO_CHAR(s.zonegare_code), s.zonegare_lib", "TO_CHAR(s.zonegare_code) = d.rm_std", "s.session_id = " + Integer.toString(sessionId) + " and d.rm_std is null");
		//this.executeInsert("pdb_zonegare", "immosis_in_pdb_zonegare", "zonegare_code, zonegare_lib", "s.zonegare_code, s.zonegare_lib", "s.zonegare_code = d.zonegare_code", "s.session_id = " + Integer.toString(sessionId) + " and d.zonegare_code is null");
		if (EventHandlerBase.isOracle(eventHandlerContext)) {
			this.executeInsert("state", "(select '00' state_id, '-' name from dual)", "state_id, name", "s.state_id, s.name", "s.state_id = d.state_id", "d.state_id is null");
		}
		else {
			this.executeInsert("state", "(select '00' state_id, '-' name)", "state_id, name", "s.state_id, s.name", "s.state_id = d.state_id", "d.state_id is null");
		}
		this.executeInsert("city", "immosis_in_tcp_commune", "city_id, name, state_id", "s.com_code_insee, s.com_lib_insee, '00'", "s.com_code_insee = d.city_id", "s.session_id = " + Integer.toString(sessionId) + " and d.city_id is null");
		this.executeInsert("tcp_emploi", "immosis_in_tcp_emploi", "emp_cle, emp_lib", "s.emp_cle, s.emp_lib", "s.emp_cle = d.emp_cle", "s.session_id = " + Integer.toString(sessionId) + " and d.emp_cle is null");
		this.executeInsert("tcp_ligne", "immosis_in_tcp_ligne", "lig_code, lig_lib", "s.lig_code, s.lig_lib", "s.lig_code = d.lig_code", "s.session_id = " + Integer.toString(sessionId) + " and d.lig_code is null");
		this.executeInsert("tcp_sous_domaine", "immosis_in_tcp_sous_domaine", "sod_code, sod_lib, dom_code", "s.sod_code, s.sod_lib, s.dom_code", "s.sod_code = d.sod_code", "s.session_id = " + Integer.toString(sessionId) + " and d.sod_code is null");
		this.executeInsert("tcp_type_batiment", "immosis_in_tcp_type_batiment", "tba_code, tba_lib", "s.tba_code, s.tba_lib", "s.tba_code = d.tba_code", "s.session_id = " + Integer.toString(sessionId) + " and d.tba_code is null");
		/* this.executeInsert("tcp_usage", "immosis_in_tcp_usage", "usa_code, usa_lib, dst_code", "s.usa_code, s.usa_lib, s.dst_code", "s.usa_code = d.usa_code", "s.session_id = " + Integer.toString(sessionId) + " and d.usa_code is null"); */
		/* this.executeInsert("tcp_   type_local", "immosis_in_tcp_type_local", "tlo_code, tlo_lib, usa_code", "s.tlo_code, s.tlo_lib, s.usa_code", "s.tlo_code = d.tlo_code", "s.session_id = " + Integer.toString(sessionId) + " and d.tlo_code is null"); */
		this.executeInsert("tcp_plaque", "immosis_in_tcp_plaque", "pla_code, pla_lib", "s.pla_code, s.pla_lib", "s.pla_code = d.pla_code", "s.session_id = " + Integer.toString(sessionId) + " and d.pla_code is null");
		this.executeInsert("tcp_udj", "immosis_in_tcp_udj", "udj_code, udj_lib, state_id, city_id, udj_cle, udj_principale", "s.udj_code, s.udj_lib, '00', s.com_code_insee, s.udj_cle, case when s.udj_principale is null then -1 else s.udj_principale end", "s.udj_cle = d.udj_cle", "s.session_id = " + Integer.toString(sessionId) + " and d.udj_cle is null");
		this.executeInsert("tcp_region_sncf", "immosis_in_tcp_region_sncf", "reg_code, reg_lib, reg_adresse, pla_code", "s.reg_code, s.reg_lib, s.reg_adresse, s.pla_code", "s.reg_code = d.reg_code", "s.session_id = " + Integer.toString(sessionId) + " and d.reg_code is null");
		this.executeInsert("site", "immosis_in_tcp_ut", "site_id, reg_code, name", "s.uto_code, s.reg_code, s.uto_lib", "s.uto_code = d.site_id", "s.session_id = " + Integer.toString(sessionId) + " and d.site_id is null");
		if (EventHandlerBase.isOracle(eventHandlerContext)) {
			this.executeInsert("bl", "immosis_in_tcp_batiment_p", "site_id, bl_id, bap_code, lig_code, cop_pk, cop_date_mes, name, tba_code, emp_cle, bap_plan_integre, bap_desc_terminee", "s.uto_code, s.uto_code || s.bap_code, s.bap_code, s.lig_code, s.cop_pk, s.cop_date_mes, s.cop_lib, s.tba_code, s.emp_cle, nvl(s.bap_plan_integre, -1), nvl(s.bap_desc_terminee, -1)", "s.uto_code || s.bap_code = d.bl_id", "s.session_id = " + Integer.toString(sessionId) + " and d.bl_id is null");
			this.executeInsert("tcp_batiment_l", "immosis_in_tcp_batiment_l", "bal_cle, udj_cle, site_id, bl_id, bap_code", "s.bal_cle, s.udj_cle, s.uto_code, s.uto_code || s.bap_code, s.bap_code", "s.bal_cle = d.bal_cle", "s.session_id = " + Integer.toString(sessionId) + " and d.bal_cle is null");
		}
		else {
			this.executeInsert("bl", "immosis_in_tcp_batiment_p", "site_id, bl_id, bap_code, lig_code, cop_pk, cop_date_mes, name, tba_code, emp_cle, bap_plan_integre, bap_desc_terminee", "s.uto_code, s.uto_code + s.bap_code, s.bap_code, s.lig_code, s.cop_pk, s.cop_date_mes, s.cop_lib, s.tba_code, s.emp_cle, isnull(s.bap_plan_integre, -1), isnull(s.bap_desc_terminee, -1)", "s.uto_code + s.bap_code = d.bl_id", "s.session_id = " + Integer.toString(sessionId) + " and d.bl_id is null");
			this.executeInsert("tcp_batiment_l", "immosis_in_tcp_batiment_l", "bal_cle, udj_cle, site_id, bl_id, bap_code", "s.bal_cle, s.udj_cle, s.uto_code, s.uto_code + s.bap_code, s.bap_code", "s.bal_cle = d.bal_cle", "s.session_id = " + Integer.toString(sessionId) + " and d.bal_cle is null");
		}
		this.executeInsert("fl", "immosis_in_tcp_type_etage", "bl_id, fl_id, name", "'" + blId + "', s.teg_code, s.teg_lib", "'" + blId + "' = d.bl_id and s.teg_code = d.fl_id", "s.session_id = " + Integer.toString(sessionId) + " and d.bl_id is null");
		if (EventHandlerBase.isOracle(eventHandlerContext)) {
		    
		    
		    //  Driss BANA import de la table immosis_in_tcp_locaux vers la table rm avec les nouveaux champs.
		    this.executeInsert("rm_temp", "immosis_in_tcp_locaux", "bl_id, fl_id, rm_id, loc_code, loc_num_ordre, loc_surface_base, area, rm_cat, sod_code, count_em, udj_cle, rm_type, loc_date_mes, name, emp_cle, rm_std, comments", "'" + blId + "', '" + flId + "', s.loc_code || '-' || s.loc_num_ordre, s.loc_code, s.loc_num_ordre, s.loc_surface_base, s.loc_surface_fiscale, s.usa_code, s.sod_code, nvl(s.loc_effectif, 0), s.udj_cle, s.tlo_code, s.loc_date_mes, s.loc_lib, s.emp_cle, s.zone_gare, s.commentaires", "'" + blId + "' = d.bl_id and '" + flId + "' = d.fl_id and s.loc_code || '-' || s.loc_num_ordre = d.rm_id", "s.session_id = " + Integer.toString(sessionId) + " and d.bl_id is null");
		 //   this.executeInsert("rm_temp", "immosis_in_tcp_locaux", "bl_id, fl_id, rm_id, loc_code, loc_num_ordre, loc_surface_base, area, usa_code, sod_code, count_em, udj_cle, tlo_code, loc_date_mes, name, emp_cle, zonegare_code, comments", "'" + blId + "', '" + flId + "', s.loc_code || '-' || s.loc_num_ordre, s.loc_code, s.loc_num_ordre, s.loc_surface_base, s.loc_surface_fiscale, s.usa_code, s.sod_code, nvl(s.loc_effectif, 0), s.udj_cle, s.tlo_code, s.loc_date_mes, s.loc_lib, s.emp_cle, s.zone_gare, s.commentaires", "'" + blId + "' = d.bl_id and '" + flId + "' = d.fl_id and s.loc_code || '-' || s.loc_num_ordre = d.rm_id", "s.session_id = " + Integer.toString(sessionId) + " and d.bl_id is null");
		}
		else {
		    
		    
		    
		   //  Driss BANA import de la table immosis_in_tcp_locaux vers la table rm avec les nouveaux champs.
		    
		    this.executeInsert("rm_temp", "immosis_in_tcp_locaux", "bl_id, fl_id, rm_id, loc_code, loc_num_ordre, loc_surface_base, area, rm_cat, sod_code, count_em, udj_cle, rm_type, loc_date_mes, name, emp_cle, rm_std, comments", "'" + blId + "', '" + flId + "', s.loc_code || '-' || s.loc_num_ordre, s.loc_code, s.loc_num_ordre, s.loc_surface_base, s.loc_surface_fiscale, s.usa_code, s.sod_code, nvl(s.loc_effectif, 0), s.udj_cle, s.tlo_code, s.loc_date_mes, s.loc_lib, s.emp_cle, s.zone_gare, s.commentaires", "'" + blId + "' = d.bl_id and '" + flId + "' = d.fl_id and s.loc_code || '-' || s.loc_num_ordre = d.rm_id", "s.session_id = " + Integer.toString(sessionId) + " and d.bl_id is null");
			//this.executeInsert("rm_temp", "immosis_in_tcp_locaux", "bl_id, fl_id, rm_id, loc_code, loc_num_ordre, loc_surface_base, area, usa_code, sod_code, count_em, udj_cle, tlo_code, loc_date_mes, name, emp_cle, zonegare_code, comments", "'" + blId + "', '" + flId + "', s.loc_code + '-' + s.loc_num_ordre, s.loc_code, s.loc_num_ordre, s.loc_surface_base, s.loc_surface_fiscale, s.usa_code, s.sod_code, isnull(s.loc_effectif, 0), s.udj_cle, s.tlo_code, s.loc_date_mes, s.loc_lib, s.emp_cle, s.zone_gare, s.commentaires", "'" + blId + "' = d.bl_id and '" + flId + "' = d.fl_id and s.loc_code + '-' + s.loc_num_ordre = d.rm_id", "s.session_id = " + Integer.toString(sessionId) + " and d.bl_id is null");
		}
		
		//Driss BANA   modification de la table pdb_zonegare par rmstd  
		this.executeUpdate("rmstd", "immosis_in_pdb_zonegare", "TO_CHAR(s.zonegare_code) as zonegare_code, s.zonegare_lib", "TO_CHAR(s.zonegare_code) = d.rm_std", "s.session_id = " + Integer.toString(sessionId),"zonegare_lib", "description");
		// Do all updates in the right order
		//this.executeUpdate("pdb_zonegare", "immosis_in_pdb_zonegare", "s.zonegare_code, s.zonegare_lib", "s.zonegare_code = d.zonegare_code", "s.session_id = " + Integer.toString(sessionId), "zonegare_lib");
		this.executeUpdate("city", "immosis_in_tcp_commune", "s.com_code_insee, s.com_lib_insee name", "s.com_code_insee = d.city_id", "s.session_id = " + Integer.toString(sessionId), "name");
		this.executeUpdate("tcp_emploi", "immosis_in_tcp_emploi", "s.emp_cle, s.emp_lib", "s.emp_cle = d.emp_cle", "s.session_id = " + Integer.toString(sessionId), "emp_lib");
		this.executeUpdate("tcp_ligne", "immosis_in_tcp_ligne", "s.lig_code, s.lig_lib", "s.lig_code = d.lig_code", "s.session_id = " + Integer.toString(sessionId), "lig_lib");
		this.executeUpdate("tcp_sous_domaine", "immosis_in_tcp_sous_domaine", "s.sod_code, s.sod_lib, s.dom_code", "s.sod_code = d.sod_code", "s.session_id = " + Integer.toString(sessionId), "sod_lib, dom_code");
		this.executeUpdate("tcp_type_batiment", "immosis_in_tcp_type_batiment", "s.tba_code, s.tba_lib", "s.tba_code = d.tba_code", "s.session_id = " + Integer.toString(sessionId), "tba_lib");
		/* this.executeUpdate("tcp_usage", "immosis_in_tcp_usage", "s.usa_code, s.usa_lib, s.dst_code", "s.usa_code = d.usa_code", "s.session_id = " + Integer.toString(sessionId), "usa_lib, dst_code"); */
		/* this.executeUpdate("tcp_type_local", "immosis_in_tcp_type_local", "s.tlo_code, s.tlo_lib, s.usa_code", "s.tlo_code = d.tlo_code", "s.session_id = " + Integer.toString(sessionId), "tlo_lib, usa_code"); */
		this.executeUpdate("tcp_plaque", "immosis_in_tcp_plaque", "s.pla_code, s.pla_lib", "s.pla_code = d.pla_code", "s.session_id = " + Integer.toString(sessionId), "pla_lib");
		this.executeUpdate("tcp_udj", "immosis_in_tcp_udj", "s.udj_cle, s.udj_code, s.udj_lib, s.com_code_insee city_id, case when s.udj_principale is null then -1 else s.udj_principale end udj_principale", "s.udj_cle = d.udj_cle", "s.session_id = " + Integer.toString(sessionId), "udj_code, udj_lib, city_id, udj_principale");
		this.executeUpdate("tcp_region_sncf", "immosis_in_tcp_region_sncf", "s.reg_code, s.reg_lib, s.reg_adresse, s.pla_code", "s.reg_code = d.reg_code", "s.session_id = " + Integer.toString(sessionId), "reg_lib, reg_adresse, pla_code");
		this.executeUpdate("site", "immosis_in_tcp_ut", "s.uto_code, s.reg_code, s.uto_lib name", "s.uto_code = d.site_id", "s.session_id = " + Integer.toString(sessionId), "reg_code, name");
		if (EventHandlerBase.isOracle(eventHandlerContext)) {
			this.executeUpdate("bl", "immosis_in_tcp_batiment_p", "s.uto_code, s.bap_code, s.uto_code site_id, s.lig_code, s.cop_pk, s.cop_date_mes, s.cop_lib name, s.tba_code, s.emp_cle, nvl(s.bap_plan_integre, -1) bap_plan_integre, nvl(s.bap_desc_terminee, -1) bap_desc_terminee", "s.uto_code || s.bap_code = d.bl_id", "s.session_id = " + Integer.toString(sessionId), "site_id, bap_code, lig_code, cop_pk, cop_date_mes, name, tba_code, emp_cle, bap_plan_integre, bap_desc_terminee");
			this.executeUpdate("tcp_batiment_l", "immosis_in_tcp_batiment_l", "s.bal_cle, s.udj_cle, s.uto_code site_id, s.uto_code || s.bap_code bl_id, s.bap_code", "s.bal_cle = d.bal_cle", "s.session_id = " + Integer.toString(sessionId), "udj_cle, site_id, bl_id, bap_code");
		}
		else {
			this.executeUpdate("bl", "immosis_in_tcp_batiment_p", "s.uto_code, s.bap_code, s.uto_code site_id, s.lig_code, s.cop_pk, s.cop_date_mes, s.cop_lib name, s.tba_code, s.emp_cle, isnull(s.bap_plan_integre, -1) bap_plan_integre, isnull(s.bap_desc_terminee, -1) bap_desc_terminee", "s.uto_code + s.bap_code = d.bl_id", "s.session_id = " + Integer.toString(sessionId), "site_id, bap_code, lig_code, cop_pk, cop_date_mes, name, tba_code, emp_cle, bap_plan_integre, bap_desc_terminee");
			this.executeUpdate("tcp_batiment_l", "immosis_in_tcp_batiment_l", "s.bal_cle, s.udj_cle, s.uto_code site_id, s.uto_code + s.bap_code bl_id, s.bap_code", "s.bal_cle = d.bal_cle", "s.session_id = " + Integer.toString(sessionId), "udj_cle, site_id, bl_id, bap_code");
		}
		this.executeUpdate("fl", "immosis_in_tcp_type_etage", "d.bl_id, s.teg_code, s.teg_lib name", "'" + blId + "' = d.bl_id and s.teg_code = d.fl_id", "s.session_id = " + Integer.toString(sessionId), "name");
		if (EventHandlerBase.isOracle(eventHandlerContext)) {
		    
		   
		    //  Driss BANA mettre à jour  la table rm avec les nouveaux champs.
		   this.executeUpdate("rm_temp", "immosis_in_tcp_locaux", "s.loc_code, s.loc_num_ordre, s.loc_surface_base, s.loc_surface_fiscale area, s.usa_code, s.sod_code, nvl(s.loc_effectif, 0) count_em, s.udj_cle, s.tlo_code, s.loc_date_mes, s.loc_lib name, s.emp_cle, s.zone_gare zonegare_code, s.commentaires comments", "'" + blId + "' = d.bl_id and '" + flId + "' = d.fl_id and s.loc_code || '-' || s.loc_num_ordre = d.rm_id", "s.session_id = " + Integer.toString(sessionId), "loc_code, loc_num_ordre, loc_surface_base, usa_code, sod_code, count_em, udj_cle, tlo_code, loc_date_mes, name, emp_cle, zonegare_code, comments", "loc_code, loc_num_ordre, loc_surface_base, rm_cat, sod_code, count_em, udj_cle, rm_type, loc_date_mes, name, emp_cle, rm_std, comments");
		   //this.executeUpdate("rm_temp", "immosis_in_tcp_locaux", "s.loc_code, s.loc_num_ordre, s.loc_surface_base, s.loc_surface_fiscale area, s.usa_code, s.sod_code, nvl(s.loc_effectif, 0) count_em, s.udj_cle, s.tlo_code, s.loc_date_mes, s.loc_lib name, s.emp_cle, s.zone_gare zonegare_code, s.commentaires comments", "'" + blId + "' = d.bl_id and '" + flId + "' = d.fl_id and s.loc_code || '-' || s.loc_num_ordre = d.rm_id", "s.session_id = " + Integer.toString(sessionId), "loc_code, loc_num_ordre, loc_surface_base, usa_code, sod_code, count_em, udj_cle, tlo_code, loc_date_mes, name, emp_cle, zonegare_code, comments");
		}
		else {
			
	  	     //  Driss BANA mettre à jour  la table rm avec les nouveaux champs.		    
		    this.executeUpdate("rm_temp", "immosis_in_tcp_locaux", "s.loc_code, s.loc_num_ordre, s.loc_surface_base, s.loc_surface_fiscale area, s.usa_code, s.sod_code, isnull(s.loc_effectif, 0) count_em, s.udj_cle, s.tlo_code, s.loc_date_mes, s.loc_lib name, s.emp_cle, s.zone_gare zonegare_code, s.commentaires comments", "'" + blId + "' = d.bl_id and '" + flId + "' = d.fl_id and s.loc_code + '-' + s.loc_num_ordre = d.rm_id", "s.session_id = " + Integer.toString(sessionId), "loc_code, loc_num_ordre, loc_surface_base, usa_code, sod_code, count_em, udj_cle, tlo_code, loc_date_mes, name, emp_cle, zonegare_code, comments", "loc_code, loc_num_ordre, loc_surface_base, rm_cat, sod_code, count_em, udj_cle, rm_type, loc_date_mes, name, emp_cle, rm_std, comments");
		    //this.executeUpdate("rm_temp", "immosis_in_tcp_locaux", "s.loc_code, s.loc_num_ordre, s.loc_surface_base, s.loc_surface_fiscale area, s.usa_code, s.sod_code, isnull(s.loc_effectif, 0) count_em, s.udj_cle, s.tlo_code, s.loc_date_mes, s.loc_lib name, s.emp_cle, s.zone_gare zonegare_code, s.commentaires comments", "'" + blId + "' = d.bl_id and '" + flId + "' = d.fl_id and s.loc_code + '-' + s.loc_num_ordre = d.rm_id", "s.session_id = " + Integer.toString(sessionId), "loc_code, loc_num_ordre, loc_surface_base, usa_code, sod_code, count_em, udj_cle, tlo_code, loc_date_mes, name, emp_cle, zonegare_code, comments");
		}
		// Delete rooms for the imported floor if they were not in the import data
		if (EventHandlerBase.isOracle(eventHandlerContext)) {
			this.executeDelete("rm_temp", "immosis_in_tcp_locaux", "rm_id", "s.loc_code || '-' || s.loc_num_ordre rm_id", "'" + blId + "' = d.bl_id and '" + flId + "' = d.fl_id and s.loc_code || '-' || s.loc_num_ordre = d.rm_id", "s.session_id = " + Integer.toString(sessionId), blId, flId);
		}
		else {
			this.executeDelete("rm_temp", "immosis_in_tcp_locaux", "rm_id", "s.loc_code + '-' + s.loc_num_ordre rm_id", "'" + blId + "' = d.bl_id and '" + flId + "' = d.fl_id and s.loc_code + '-' + s.loc_num_ordre = d.rm_id", "s.session_id = " + Integer.toString(sessionId), blId, flId);
		}
		// delete/mark inactive for tables that contain "all data" instead of only "plan specific"
		/* this.executeDelete("tcp_type_local", "immosis_in_tcp_type_local", "tlo_code", "s.tlo_code", "s.tlo_code = d.tlo_code", "s.session_id = " + Integer.toString(sessionId), "", ""); */
		/* this.executeDelete("tcp_usage", "immosis_in_tcp_usage", "usa_code", "s.usa_code", "s.usa_code = d.usa_code", "s.session_id = " + Integer.toString(sessionId), "", ""); */
		this.executeDelete("tcp_emploi", "immosis_in_tcp_emploi", "emp_cle", "s.emp_cle", "s.emp_cle = d.emp_cle", "s.session_id = " + Integer.toString(sessionId), "", "");
		
		
		//Driss BANA   modification de la table pdb_zonegare par rmstd  
		this.executeDelete("rmstd", "immosis_in_pdb_zonegare", "rm_std", "TO_CHAR(s.zonegare_code)", "TO_CHAR(s.zonegare_code) = d.rm_std", "s.session_id = " + Integer.toString(sessionId), "", "");
		//this.executeDelete("pdb_zonegare", "immosis_in_pdb_zonegare", "zonegare_code", "s.zonegare_code", "s.zonegare_code = d.zonegare_code", "s.session_id = " + Integer.toString(sessionId), "", "");
	}
	public void executeInsert(final String destinationTable, final String sourceTable, final String destinationFields, final String sourceFields, final String joinCriterium, final String restriction) {
		this.executeInsert(destinationTable, sourceTable, destinationFields, sourceFields, joinCriterium, restriction, false);
	}
	public void executeInsert(final String destinationTable, final String sourceTable, final String destinationFields, final String sourceFields, final String joinCriterium, final String restriction, final Boolean identityInsert) {
		final EventHandlerContext eventHandlerContext = ContextStore.get().getEventHandlerContext();
		final String sqlBaseInsert = "insert into {0} ({1}) select {2} from {3} s left join {4} d on {5} where {6}";
		final Object[] parameters = new String[] { destinationTable, destinationFields, sourceFields, sourceTable, destinationTable, joinCriterium, restriction };
		String sqlInsert = MessageFormat.format(sqlBaseInsert, parameters);
		if (identityInsert) {
			sqlInsert = "set identity_insert rm_temp on;" + sqlInsert + "set identity_insert rm_temp off;";
		}
		EventHandlerBase.executeDbSql(eventHandlerContext, destinationTable, sqlInsert, false);
	}
	public void executeUpdate(final String destinationTable, final String sourceTable, final String sourceFields, final String joinCriterium, final String restriction, final String updateFields) {
        final EventHandlerContext eventHandlerContext = ContextStore.get().getEventHandlerContext();
        final String sqlBaseMerge = "merge into {0} d using (select {1} from {2} s inner join {3} d on {4} where {5}) s on ({6}) when matched then update set {7}";
        final String[] uFields = updateFields.split(", ", -1);
        String updateCombos = "";
        for (final String uField : uFields) {
            if (updateCombos.length() > 0) {
                updateCombos += ", ";
            }
            updateCombos += uField + " = s." + uField;
        }
        final Object[] parameters = new String[] { destinationTable, sourceFields, sourceTable, destinationTable, joinCriterium, restriction, joinCriterium, updateCombos };
        final String sqlMerge = MessageFormat.format(sqlBaseMerge, parameters);
        EventHandlerBase.executeDbSql(eventHandlerContext, destinationTable, sqlMerge, false);
    }
	
	//driss BANA nouvellle methode d update dans les cas champs sources diferent de champ destination
	
	public void executeUpdate(final String destinationTable, final String sourceTable, final String sourceFields, final String joinCriterium, final String restriction, final String sourceUpdateFields ,final String destUpdateFields) {
		final EventHandlerContext eventHandlerContext = ContextStore.get().getEventHandlerContext();
		final String sqlBaseMerge = "merge into {0} d using (select {1} from {2} s inner join {3} d on {4} where {5}) s on ({6}) when matched then update set {7}";
		final String[] sourceUFields = sourceUpdateFields.split(", ", -1);
		final String[] destUFields = destUpdateFields.split(", ", -1);
		String updateCombos = "";		
		
		int cpt=0;
		
		for (final String uField : sourceUFields) {
		    
			if (updateCombos.length() > 0) {
				updateCombos += ", ";
			}
			updateCombos += destUFields[cpt] + " = s." + uField;
			cpt++;
		}
		final Object[] parameters = new String[] { destinationTable, sourceFields, sourceTable, destinationTable, joinCriterium, restriction, joinCriterium, updateCombos };
		final String sqlMerge = MessageFormat.format(sqlBaseMerge, parameters);
		EventHandlerBase.executeDbSql(eventHandlerContext, destinationTable, sqlMerge, false);
	}
	public void executeDelete(final String destinationTable, final String sourceTable, final String destinationField, final String sourceField, final String joinCriterium, final String restriction, final String blId, final String flId) {
		final EventHandlerContext eventHandlerContext = ContextStore.get().getEventHandlerContext();
		// final String sqlBaseMerge = "merge into {0} d using (select {1} from {2} s inner join {3} d on {4} where {5}) s on ({6}) when not matched by s{7} then delete;";
		final String sqlBaseDelete = "delete from {0} where {1} not in (select {2} from {3} s inner join {4} d on {5} where {6}){7}";
		String locationSpecificCriterium = "";
		if (blId != null && blId.length() > 0) {
			locationSpecificCriterium += " and bl_id = '" + blId + "'";
		}
		if (flId != null && flId.length() > 0) {
			locationSpecificCriterium += " and fl_id = '" + flId + "'";
		}
		final Object[] parameters = new String[] { destinationTable, destinationField, sourceField, sourceTable, destinationTable, joinCriterium, restriction, locationSpecificCriterium };
		final String sqlDelete = MessageFormat.format(sqlBaseDelete, parameters);
		EventHandlerBase.executeDbSql(eventHandlerContext, destinationTable, sqlDelete, false);
	}
	public void createSessionRollbackPoint(final int sessionId, final String blId, final String flId) {
		doBackup(sessionId, "rm", "bl_id = '" + blId + "' and fl_id = '" + flId + "'");
	}
	// !!!! attention, la logique de la fonction est a revoir dans le cas de la table immosis_backup_rm; c'est un back up de rm_temp mais l'argument d'appel est rm
	public void doBackup(final int sessionId, final String tableName, final String restriction) {
		final EventHandlerContext eventHandlerContext = this.context.getEventHandlerContext();
		EventHandlerBase.executeDbSql(eventHandlerContext, "immosis_backup_" + tableName, "delete from immosis_backup_" + tableName + " where session_id = " + Integer.toString(sessionId), false);
		String insertFields = "";
		String selectFields = "";
		for (final String fieldName : this.tableSpecifications.get("immosis_backup_" + tableName).fieldNames) {
			if (!insertFields.equalsIgnoreCase("")) {
				insertFields += ", ";
			}
			insertFields += fieldName;
			if (!selectFields.equalsIgnoreCase("")) {
				selectFields += ", ";
			}
			if (fieldName.equalsIgnoreCase("session_id")) {
				selectFields += Integer.toString(sessionId);
			}
			else {
				selectFields += fieldName;
			}
		}
		EventHandlerBase.executeDbSql(eventHandlerContext, "immosis_backup_" + tableName, "insert into immosis_backup_" + tableName + " (" + insertFields + ") select " + selectFields + " from " + tableName + "_temp"+ " where " + restriction, false);
	}
	public void rollBackSession(final int sessionId) throws Exception {
		this.context.getEventHandlerContext();
		final String blId = this.getBlIdForSession(sessionId);
		final String flId = this.getFlIdForSession(sessionId);
		String destinationFields = "";
		String sourceFields = "";
		String updateFields = "";
		for (final String fieldName : this.tableSpecifications.get("immosis_backup_rm").fieldNames) {
			if (fieldName.equalsIgnoreCase("session_id")) {
				continue;
			}
			if (!destinationFields.equalsIgnoreCase("")) {
				destinationFields += ", ";
			}
			destinationFields += fieldName;
			if (!sourceFields.equalsIgnoreCase("")) {
				sourceFields += ", ";
			}
			sourceFields += "s." + fieldName;
			if (!(fieldName.equalsIgnoreCase("bl_id") || fieldName.equalsIgnoreCase("fl_id") || fieldName.equalsIgnoreCase("rm_id") || fieldName.equalsIgnoreCase("loc_id"))) {
				if (!updateFields.equalsIgnoreCase("")) {
					updateFields += ", ";
				}
				updateFields += fieldName;
			}
		}
		final String joinCriterium = "s.bl_id = d.bl_id and s.fl_id = d.fl_id and s.rm_id = d.rm_id";
		String restriction = "";
		final Boolean useIdentityInsert = false;
		// Insert missing rooms from backup
		restriction = "s.session_id = " + Integer.toString(sessionId) + " and d.bl_id is null";
		this.executeInsert("rm_temp", "immosis_backup_rm", destinationFields, sourceFields, joinCriterium, restriction, useIdentityInsert);
		// Update existing rooms from backup
		restriction = "s.session_id = " + Integer.toString(sessionId);
		this.executeUpdate("rm_temp", "immosis_backup_rm", sourceFields, joinCriterium, restriction, updateFields);
		// Delete rooms that are not in backup
		this.executeDelete("rm_temp", "immosis_backup_rm", "rm_id", "s.rm_id", joinCriterium, restriction, blId, flId);
	}
public void fillExportTables(final int sessionId) throws Exception {
		final EventHandlerContext eventHandlerContext = this.context.getEventHandlerContext();
		final String blId = this.getBlIdForSession(sessionId);
		final String flId = this.getFlIdForSession(sessionId);
		String insertTable = "";
		String insertFields = "";
		String selectFields = "";
		String selectTables = "";
		String whereClause = "";
		String orderClause = "";
		// Delete all data
		EventHandlerBase.executeDbSql(eventHandlerContext, "immosis_out_pdb_etiquettes", "delete from immosis_out_pdb_etiquettes where session_id = " + Integer.toString(sessionId), false);
		EventHandlerBase.executeDbSql(eventHandlerContext, "immosis_out_pdb_maj", "delete from immosis_out_pdb_maj where session_id = " + Integer.toString(sessionId), false);
		EventHandlerBase.executeDbSql(eventHandlerContext, "immosis_out_tcp_locaux", "delete from immosis_out_tcp_locaux where session_id = " + Integer.toString(sessionId), false);
		// Insert immosis_out_pdb_etiquettes
		insertTable = "immosis_out_pdb_etiquettes";
		insertFields = "session_id, uto_code, bap_code, teg_code, loc_code, loc_num_ordre, surf_dev, uco_code_occup, sod_code, usa_code, loc_effectif";
		
		//Driss BANA  modification du champs rm_temp.USA_CODE par rm_temp.rm_cat		
		
		selectFields = Integer.toString(sessionId) + ", bl.site_id, bl.bap_code, rm_temp.fl_id, rm_temp.loc_code, rm_temp.loc_num_ordre, rm_temp.area, '00000', rm_temp.sod_code, rm_temp.rm_cat, cast(round(rm_temp.count_em, 0) as int)";
		//selectFields = Integer.toString(sessionId) + ", bl.site_id, bl.bap_code, rm_temp.fl_id, rm_temp.loc_code, rm_temp.loc_num_ordre, rm_temp.area, '00000', rm_temp.sod_code, rm_temp.usa_code, cast(round(rm_temp.count_em, 0) as int)";
		selectTables = "rm_temp inner join bl on bl.bl_id = rm_temp.bl_id";
		whereClause = "rm_temp.bl_id = '" + blId + "' and rm_temp.fl_id = '" + flId + "' and rm_temp.loc_code is not null and rm_temp.loc_num_ordre is not null";
		orderClause = "rm_temp.loc_code, rm_temp.loc_num_ordre";
		EventHandlerBase.executeDbSql(eventHandlerContext, insertTable, "insert into " + insertTable + " (" + insertFields + ") select " + selectFields + " from " + selectTables + " where " + whereClause + " order by " + orderClause, false);
		// Insert immosis_out_pdb_maj
		insertTable = "immosis_out_pdb_maj";
		insertFields = "session_id, uto_code, bap_code, teg_code, loc_id, type_action, etat_ordre";
		selectFields = Integer.toString(sessionId) + ", nvl(bl.site_id, babl.site_id) site_id, nvl(bl.bap_code, babl.bap_code) bap_code, nvl(rm_temp.fl_id, barm.fl_id) fl_id, nvl(rm_temp.loc_id, barm.loc_id) loc_id, ";
		selectFields += "case ";
		selectFields += "when barm.bl_id is null then 'A' ";
		selectFields += "when rm_temp.bl_id is null then 'S' ";
		selectFields += "when ";
		selectFields += "nvl(bl.site_id    , '?') <> nvl(babl.site_id , '?') ";
		selectFields += "or nvl(bl.bap_code, '?') <> nvl(babl.bap_code, '?') ";
		selectFields += "or nvl(rm_temp.fl_id   , '?') <> nvl(barm.fl_id   , '?') ";
		selectFields += "or nvl(cast(rm_temp.loc_id as varchar2(30)), '?') <> nvl(cast(barm.loc_id as varchar2(30)), '?') ";
		selectFields += "then 'M' else null end type_action, row_number() over(order by nvl(rm_temp.loc_id, barm.loc_id)) etat_ordre";
		selectTables = "rm_temp full join immosis_backup_rm barm on barm.session_id = " + Integer.toString(sessionId) + " and barm.bl_id = rm_temp.bl_id and barm.fl_id = rm_temp.fl_id and barm.rm_id = rm_temp.rm_id left join bl on bl.bl_id = rm_temp.bl_id left join bl babl on babl.bl_id = barm.bl_id";
		whereClause = "nvl(barm.session_id, " + Integer.toString(sessionId) + ") = " + Integer.toString(sessionId) + " and nvl(rm_temp.bl_id, barm.bl_id) = '" + blId + "' and nvl(rm_temp.fl_id, barm.fl_id) = '" + flId + "' and nvl(rm_temp.loc_code, barm.loc_code) is not null and nvl(rm_temp.loc_num_ordre, barm.loc_num_ordre) is not null";
		orderClause = "loc_id";
		EventHandlerBase.executeDbSql(eventHandlerContext, insertTable, "insert into " + insertTable + " (" + insertFields + ") select " + selectFields + " from " + selectTables + " where " + whereClause + " order by " + orderClause, false);
		// Insert immosis_out_tcp_locaux
		insertTable = "immosis_out_tcp_locaux";
		insertFields = "session_id, uto_code, bap_code, teg_code, loc_code, loc_num_ordre, surf_dev, desbien_code, sod_code, loc_effectif, udj_cle, usa_code, loc_date_mes, loc_lib, emp_cle, zone_gare, commentaires, type_action, loc_id";
		
		// Driss BANA   rm_temp.USA_CODE par rm_temp.rm_cat	et  rm_temp.TLO_CODE par rm_temp.rm_type et rm_temp.ZONEGARE_CODE par rm_temp.rm_std
		selectFields = Integer.toString(sessionId) + ", nvl(bl.site_id, babl.site_id) uto_code, nvl(bl.bap_code, babl.bap_code) bap_code, nvl(rm_temp.fl_id, barm.fl_id) teg_code, nvl(rm_temp.loc_code, barm.loc_code) loc_code, nvl(rm_temp.loc_num_ordre, barm.loc_num_ordre) loc_num_ordre, nvl(rm_temp.area, barm.area) surf_dev, nvl(rm_temp.rm_cat, barm.rm_cat) desbien_code, nvl(rm_temp.sod_code, barm.sod_code) sod_code, cast(round(nvl(rm_temp.count_em, barm.count_em), 0) as int) loc_effectif, nvl(rm_temp.udj_cle, barm.udj_cle) udj_cle, nvl(rm_temp.rm_type, barm.rm_type) usa_code, nvl(rm_temp.loc_date_mes, barm.loc_date_mes) loc_date_mes, nvl(rm_temp.name, barm.name) loc_lib, nvl(rm_temp.emp_cle, barm.emp_cle) emp_cle, nvl(rm_temp.rm_std, barm.rm_std) zone_gare, nvl(rm_temp.comments, barm.comments) commentaires, ";
		//selectFields = Integer.toString(sessionId) + ", nvl(bl.site_id, babl.site_id) uto_code, nvl(bl.bap_code, babl.bap_code) bap_code, nvl(rm_temp.fl_id, barm.fl_id) teg_code, nvl(rm_temp.loc_code, barm.loc_code) loc_code, nvl(rm_temp.loc_num_ordre, barm.loc_num_ordre) loc_num_ordre, nvl(rm_temp.area, barm.area) surf_dev, nvl(rm_temp.usa_code, barm.usa_code) desbien_code, nvl(rm_temp.sod_code, barm.sod_code) sod_code, cast(round(nvl(rm_temp.count_em, barm.count_em), 0) as int) loc_effectif, nvl(rm_temp.udj_cle, barm.udj_cle) udj_cle, nvl(rm_temp.tlo_code, barm.tlo_code) usa_code, nvl(rm_temp.loc_date_mes, barm.loc_date_mes) loc_date_mes, nvl(rm_temp.name, barm.name) loc_lib, nvl(rm_temp.emp_cle, barm.emp_cle) emp_cle, nvl(rm_temp.zonegare_code, barm.zonegare_code) zone_gare, nvl(rm_temp.comments, barm.comments) commentaires, ";
		selectFields += "case ";
		selectFields += "when barm.bl_id is null then 'A' ";
		selectFields += "when rm_temp.bl_id is null then 'S' ";
		selectFields += "when ";
		selectFields += "nvl(bl.site_id         , '?')  <> nvl(babl.site_id      , '?') ";
		selectFields += "or nvl(bl.bap_code     , '?')  <> nvl(babl.bap_code     , '?') ";
		selectFields += "or nvl(rm_temp.fl_id        , '?')  <> nvl(barm.fl_id        , '?') ";
		selectFields += "or nvl(rm_temp.loc_code     , '?')  <> nvl(barm.loc_code     , '?') ";
		selectFields += "or nvl(rm_temp.loc_num_ordre, '?')  <> nvl(barm.loc_num_ordre, '?') ";
		selectFields += "or nvl(rm_temp.area         , -1.0) <> nvl(barm.area         , -1.0) ";
		
		
		//Driss BANA  modification du champs rm_temp.USA_CODE par rm_temp.rm_cat        
		
		selectFields += "or nvl(rm_temp.rm_cat     , '?')  <> nvl(barm.rm_cat     , '?') ";
		//selectFields += "or nvl(rm_temp.usa_code     , '?')  <> nvl(barm.usa_code     , '?') ";
		selectFields += "or nvl(rm_temp.sod_code     , '?')  <> nvl(barm.sod_code     , '?') ";
		selectFields += "or cast(round(nvl(rm_temp.count_em, -1), 0) as int) <> cast(round(nvl(barm.count_em, -1), 0) as int) ";
		selectFields += "or nvl(cast(rm_temp.udj_cle       as varchar2(30)), '?') <> nvl(cast(barm.udj_cle       as varchar2(30)), '?') ";
		
		// Driss BANA  modification du champs rm_temp.TLO_CODE par rm_temp.rm_type
		selectFields += "or nvl(rm_temp.rm_type     , '?')  <> nvl(barm.rm_type     , '?') ";
		//selectFields += "or nvl(rm_temp.tlo_code     , '?')  <> nvl(barm.tlo_code     , '?') ";
		selectFields += "or nvl(rm_temp.loc_date_mes , SYSDATE)  <> nvl(barm.loc_date_mes , SYSDATE) ";
		selectFields += "or nvl(rm_temp.name         , '?')  <> nvl(barm.name         , '?') ";
		selectFields += "or nvl(cast(rm_temp.emp_cle       as varchar2(30)), '?') <> nvl(cast(barm.emp_cle       as varchar2(30)), '?') ";
		
		// Driss BANA  modification du champs rm_temp.ZONEGARE_CODE par rm_temp.rm_std
		selectFields += "or nvl(cast(rm_temp.rm_std as varchar2(30)), '?') <> nvl(cast(barm.rm_std as varchar2(30)), '?') ";
		//selectFields += "or nvl(cast(rm_temp.zonegare_code as varchar2(30)), '?') <> nvl(cast(barm.zonegare_code as varchar2(30)), '?') ";
		selectFields += "or nvl(rm_temp.comments     , '?')  <> nvl(barm.comments     , '?') ";
		selectFields += "then 'M' else null end type_action, nvl(rm_temp.loc_id, barm.loc_id) loc_id";
		selectTables = "rm_temp full join immosis_backup_rm barm on barm.session_id = " + Integer.toString(sessionId) + " and barm.bl_id = rm_temp.bl_id and barm.fl_id = rm_temp.fl_id and barm.rm_id = rm_temp.rm_id left join bl on bl.bl_id = rm_temp.bl_id left join bl babl on babl.bl_id = barm.bl_id";
		whereClause = "nvl(barm.session_id, " + Integer.toString(sessionId) + ") = " + Integer.toString(sessionId) + " and nvl(rm_temp.bl_id, barm.bl_id) = '" + blId + "' and nvl(rm_temp.fl_id, barm.fl_id) = '" + flId + "' and nvl(rm_temp.loc_code, barm.loc_code) is not null and nvl(rm_temp.loc_num_ordre, barm.loc_num_ordre) is not null";
		orderClause = "loc_id";
		EventHandlerBase.executeDbSql(eventHandlerContext, insertTable, "insert into " + insertTable + " (" + insertFields + ") select " + selectFields + " from " + selectTables + " where " + whereClause + " order by " + orderClause, false);
	}

	public void createExportFiles(final int sessionId) throws IOException, SQLException {
		final EventHandlerContext eventHandlerContext = this.context.getEventHandlerContext();
		final DbConnection.ThreadSafe dbConnection = EventHandlerBase.getDbConnection(eventHandlerContext);
		final String[] sql = new String[3];
		ResultSet rs = null;
		if (EventHandlerBase.isOracle(eventHandlerContext)) {
			sql[0] = "select uto_code, bap_code, teg_code, loc_code, loc_num_ordre, surf_dev, uco_code_occup, sod_code, usa_code, loc_effectif from immosis_out_pdb_etiquettes where session_id = " + Integer.toString(sessionId) + " order by uto_code, bap_code, teg_code, loc_code, loc_num_ordre";
			sql[1] = "select uto_code, bap_code, teg_code, loc_id, type_action, etat_ordre from immosis_out_pdb_maj where session_id = " + Integer.toString(sessionId) + " order by etat_ordre";
			sql[2] = "select uto_code, bap_code, teg_code, loc_code, loc_num_ordre, surf_dev, desbien_code, sod_code, loc_effectif, udj_cle, usa_code, loc_date_mes, loc_lib, emp_cle, zone_gare, commentaires, type_action, loc_id from immosis_out_tcp_locaux where session_id = " + Integer.toString(sessionId) + " order by uto_code, bap_code, teg_code, loc_code, loc_num_ordre";
		}
		else {
			sql[0] = "select uto_code, bap_code, teg_code, loc_code, loc_num_ordre, cast(round(surf_dev, 2) as varchar(16)), uco_code_occup, sod_code, usa_code, cast(loc_effectif as varchar(10)) from immosis_out_pdb_etiquettes where session_id = " + Integer.toString(sessionId) + " order by uto_code, bap_code, teg_code, loc_code, loc_num_ordre";
			sql[1] = "select uto_code, bap_code, teg_code, cast(loc_id as varchar(10)), type_action, cast(etat_ordre as varchar(10)) from immosis_out_pdb_maj where session_id = " + Integer.toString(sessionId) + " order by etat_ordre";
			sql[2] = "select uto_code, bap_code, teg_code, loc_code, loc_num_ordre, cast(round(surf_dev, 2) as varchar(16)), desbien_code, sod_code, cast(loc_effectif as varchar(10)), cast(udj_cle as varchar(10)), usa_code, convert(varchar(10), loc_date_mes, 103), loc_lib, cast(emp_cle as varchar(10)), cast(zone_gare as varchar(10)), commentaires, type_action, cast(loc_id as varchar(10)) from immosis_out_tcp_locaux where session_id = " + Integer.toString(sessionId) + " order by uto_code, bap_code, teg_code, loc_code, loc_num_ordre";
		}
		rs = dbConnection.execute(sql[0], 0);
		this.createExportFile(sessionId, "PDB_ETIQUETTES.txt", rs);
		rs = dbConnection.execute(sql[1], 0);
		this.createExportFile(sessionId, "PDB_MAJ.txt", rs);
		rs = dbConnection.execute(sql[2], 0);
		this.createExportFile(sessionId, "TCP_LOCAUX.txt", rs);
		this.zipExportFiles(sessionId);
		File f;
		f = new File(this.context.getWebAppPath() + File.separator + "projects" + File.separator + "immosis" + File.separator + "sessions" + File.separator + Integer.toString(sessionId) + File.separator + "out" + File.separator + "PDB_ETIQUETTES.txt");
		f.delete();
		f = new File(this.context.getWebAppPath() + File.separator + "projects" + File.separator + "immosis" + File.separator + "sessions" + File.separator + Integer.toString(sessionId) + File.separator + "out" + File.separator + "PDB_MAJ.txt");
		f.delete();
		f = new File(this.context.getWebAppPath() + File.separator + "projects" + File.separator + "immosis" + File.separator + "sessions" + File.separator + Integer.toString(sessionId) + File.separator + "out" + File.separator + "TCP_LOCAUX.txt");
		f.delete();
	}

	public void createExportFile(final int sessionId, final String fileName, final ResultSet rs) throws IOException, SQLException {
		this.context.getEventHandlerContext();
		final Path path = Paths.get(this.context.getWebAppPath() + File.separator + "projects" + File.separator + "immosis" + File.separator + "sessions" + File.separator + Integer.toString(sessionId) + File.separator + "out" + File.separator);
		final String encoding = "Cp1252";
		final String separator = "#";
		final String endOfLine = "\r\n";
		if (!Files.exists(path)) {
			Files.createDirectories(path);
		}
		final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(path.toString() + File.separator + fileName), encoding);
		final PrintWriter printWriter = new PrintWriter(outputStreamWriter);
		final ResultSetMetaData metadata = rs.getMetaData();
		final int columnCount = metadata.getColumnCount();
		String columnTypeName = "";
		int scale = 0;
		final Locale locale = new Locale("fr", "FR");
		final NumberFormat numberFormat = NumberFormat.getInstance(locale);
		if (numberFormat instanceof DecimalFormat) {
			((DecimalFormat) numberFormat).setGroupingUsed(false);
		}
		String columnValue = "";
		while (rs.next()) {
			String lineContent = "";
			for (int i = 0; i < columnCount; i++) {
				if (i > 0) {
					lineContent += separator;
				}
				columnTypeName = metadata.getColumnTypeName(i + 1);
				scale = metadata.getScale(i + 1);
				if (columnTypeName.equalsIgnoreCase("DATE")) {
					columnValue = new SimpleDateFormat("dd/MM/yyyy").format(rs.getDate(i + 1));
				}
				else if (columnTypeName.equalsIgnoreCase("NUMBER") && scale > 0) {
					if (numberFormat instanceof DecimalFormat) {
						((DecimalFormat) numberFormat).setMinimumFractionDigits(scale);
						((DecimalFormat) numberFormat).setMaximumFractionDigits(scale);
					}
					columnValue = numberFormat.format(rs.getDouble(i + 1));
				}
				else {
					columnValue = StringUtil.notNull(rs.getString(i + 1));
				}
				lineContent += columnValue;
			}
			lineContent += endOfLine;
			printWriter.write(lineContent);
		}
		printWriter.flush();
		printWriter.close();
		outputStreamWriter.close();
	}

	public String getZipFileNameForSession(final int sessionId) {
		this.initialize();
		String zipFileName = "upload_to_immosis_";
		List<DataRecord> records = null;
		DataRecord rec = null;
		records = this.tableSpecifications.get("immosis_in_tcp_batiment_p").dataSource.getRecords("session_id = " + Integer.toString(sessionId));
		if (records.size() > 0) {
			rec = records.get(0);
			zipFileName += rec.getString("immosis_in_tcp_batiment_p.uto_code") + "_" + rec.getString("immosis_in_tcp_batiment_p.bap_code") + "_";
		}
		else {
			zipFileName += "_XXXXXXX_XXX_";
		}
		records = this.tableSpecifications.get("immosis_in_tcp_type_etage").dataSource.getRecords("session_id = " + Integer.toString(sessionId));
		if (records.size() > 0) {
			rec = records.get(0);
			zipFileName += rec.getString("immosis_in_tcp_type_etage.teg_code") + ".zip";
		}
		else {
			zipFileName += "XX.zip";
		}
		return zipFileName;
	}

	public void zipExportFiles(final int sessionId) throws IOException {
		final byte[] buffer = new byte[1024];
		final String path = this.context.getWebAppPath() + File.separator + "projects" + File.separator + "immosis" + File.separator + "sessions" + File.separator + Integer.toString(sessionId) + File.separator + "out" + File.separator;
		final File dir = new File(path);
		final File f = new File(path + this.getZipFileNameForSession(sessionId));
		final ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(f));
		for (final File file : dir.listFiles()) {
			if (file.isFile() && file.canRead() && file.getName().matches(".+\\.txt")) {
				final ZipEntry e = new ZipEntry(file.getName());
				zipOut.putNextEntry(e);
				final FileInputStream fis = new FileInputStream(file);
				int len;
				while ((len = fis.read(buffer)) > 0) {
					zipOut.write(buffer, 0, len);
				}
				zipOut.closeEntry();
			}
		}
		zipOut.close();
	}

	public void handleException(final String message, final Exception e) throws Exception {
		/*
		 * this.status.setMessage(message); this.status.setDetails("Details: " + e.getLocalizedMessage()); this.log.error(message, e);
		 */
		this.status.setCode(JobStatus.JOB_FAILED);
		throw new ExceptionBase(message + "  ", e);
	}

	public static void deleteFolder(final File folder) {
		final File[] files = folder.listFiles();
		if (files != null) {
			for (final File f : files) {
				if (f.isDirectory()) {
					deleteFolder(f);
				}
				else {
					f.delete();
				}
			}
		}
		folder.delete();
	}
}
