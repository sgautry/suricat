package com.aremis.immosis;
import java.io.*;
import java.sql.*;
import java.text.MessageFormat;
import java.util.*;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

import oracle.jdbc.OracleTypes;

import org.aopalliance.intercept.MethodInvocation;
import org.apache.log4j.Logger;
import org.apache.commons.net.ftp.FTPReply;



import com.archibus.context.*;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.db.DbConnection;
import com.archibus.eventhandler.EventHandlerBase;
import com.archibus.jobmanager.EventHandlerContext;
import com.archibus.service.interceptor.AbstractInterceptor;
import com.archibus.utility.ExceptionBase;

import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
/**
 * Provides TODO. - if it has behavior (service), or 
 * Represents TODO. - if it has state (entity, domain object, model object). 
 * Utility class. Provides methods TODO.
 * Interface to be implemented by classes that TODO.
 *<p>
 *
 * Used by TODO to TODO.
 * Managed by Spring, has prototype TODO singleton scope. Configured in TODO file.
 *
 * @author NicolasLEDENT
 * @since 20.1
 *
 */

public class AremisCheckInterceptor extends AbstractInterceptor {
    
    static Logger log = Logger.getLogger(AremisCheckInterceptor.class);
    private static final String CHECKIN = "checkin";
    private static final String CHECKOUT = "checkout";
    private static final String SESSION_DEFAULT_STATUS = "2_STARTED";
    private static final String SESSION_TABLE ="immosis_session";
    private  String ftpRemoteFolder =null;
    private  String ftpLocalFolder =null;
    private  String ftpServer =null;
    private  String ftpUsername =null;
    private  String ftpPassword =null;
    private  String ftpPort =null;
    private String dbDriver =null;
    private String dbConnection =null;
    private String dbUser =null;
    private String dbPassword =null;
    private final String STEP_10 = "STEP_10";
    private final String STEP_21 = "STEP_21";
    private final String STEP_22 = "STEP_22";
    private int userId = 0;
    private String pdb =  null;
    private String blId = null;
    private String flId = null;
    private String tegCode = null;
    protected Context   context = null;
    protected String userName = null;   
    protected int sessionId = 0;
    protected boolean initialized = false;
    /** {@inheritDoc} */
    
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        ReturnValues returnValues = workAroundSpring4BugIfToStringCalled(invocation);
        if (returnValues.isProcessed()) {
          return returnValues.getReturnValue();
        }
        Object retVal = invocation.proceed();
        String methodName = invocation.getMethod().getName();
        
        if (CHECKIN.equals(methodName)) {
          if (retrieveContextAndUser(invocation)) {
            checkin(invocation);
          } else {
              logDebug("ERROR","Une erreur s'est produite lors de la récupération de l'utilisateur.");
            throw new ExceptionBase("Erreur lors du retrieve de session.");
          }
        }

        if (CHECKOUT.equals(methodName))
        {
          if (retrieveContextAndUser(invocation)) {
              logDebug("INFO","Utilisateur connecte :"+this.userName);
            checkout(invocation);
          } else {
              logDebug("ERROR","Une erreur s'est produite lors de la récupération de l'utilisateur.");
            throw new ExceptionBase("Erreur lors du retrieve de session.", new Exception("Erreur lors du retrieve de session."));
          }
        }
        return retVal;
        
    }
    
    private void CheckInTest(MethodInvocation invocation){
        logDebug("INFO","CheckInPostProcessTest");
        this.initData(invocation);
        this.getDBConnection();
        this.exportDWG();
    }
    private void initData(MethodInvocation invocation){
        Object[] args = invocation.getArguments();
        this.pdb = ((String) args[0]).toUpperCase();
        this.blId = this.pdb.substring(0, 11).replace("_","");
        this.flId = this.pdb.substring(12, 14);
        this.tegCode = "0";
        
    }
    private void retrieveUserId(){
        Connection dbConnection = null;
        Statement stmt = null;
        ResultSet rs;
        
        int retour = 0;
        try {
            dbConnection = this.getDBConnection();
            stmt = dbConnection.createStatement();
            rs = stmt.executeQuery("SELECT USR_PK FROM ABSIS.USERS WHERE USR_LOGIN ='"+this.userName+"'");
            while (rs.next()) {
             this.userId = rs.getInt(1); 
            }
        } catch (Exception e) {
            logDebug("ERROR",e.getMessage());
        } finally {
            if (dbConnection != null) {
                try {
                    dbConnection.close();
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }
    private boolean retrieveContextAndUser(MethodInvocation invocation){

        try{
            this.initData(invocation);
            this.context = ContextStore.get();
            this.userName =  this.context.getUser().getName();
            logDebug("INFO","Recuperation de l'utilisateur :"+this.userName);
            this.retrieveUserId();
            // For debug 
            return (this.userId !=0 && this.userName!= null);
        }catch(Exception e){
            logDebug("ERROR",e.getMessage());
            
            return false;
        }
    }
    //Appeller depuis le checkout.
    private void createSession(){
        try{
            String[] fields = {"session_id","user_name","status","bl_id","fl_id"};
            final DataSource ds = DataSourceFactory.createDataSourceForFields(SESSION_TABLE, fields);
            DataRecord rec = ds.createNewRecord();
            rec.setValue(SESSION_TABLE+".user_name", this.userName);
            rec.setValue(SESSION_TABLE+".status", SESSION_DEFAULT_STATUS );
            rec.setValue(SESSION_TABLE+".bl_id", this.blId );
            rec.setValue(SESSION_TABLE+".fl_id",  this.flId);
            rec = ds.saveRecord(rec);
            this.sessionId = rec.getInt(SESSION_TABLE+".session_id");
            logDebug("INFO","Une nouvelle session["+this.sessionId+"] a été créé");
            
            logDebug("INFO","Le backup a été réalisé.");
        }catch(Exception e){
            e.printStackTrace();
            logDebug("ERROR",e.getMessage());
        }
    }

    private boolean retrieveSession(){
        boolean retour = false;
        DbConnection.ThreadSafe dbCnx = ContextStore.get().getDbConnection();
        ResultSet internalSet = dbCnx.execute("select session_id FROM immosis_session where user_name ='"+this.userName+"' AND STATUS LIKE '2_STARTED'" , 0);
        try { 
            while(internalSet.next()){
                this.sessionId = internalSet.getInt(1);
               retour = true;
            }
            return retour;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return retour;
        }
    }
    private void cancelSession(){   
        
        String[] fields = {"session_id","user_name","status","bl_id","fl_id","date_end","time_end"};
        final DataSource ds = DataSourceFactory.createDataSourceForFields(SESSION_TABLE, fields);
        final DataRecord rec = ds.getRecord("session_id = " + Integer.toString(this.sessionId));
        rec.getString(SESSION_TABLE+".status");
        final Date d = new Date();
        rec.setValue(SESSION_TABLE+".status", "4_CANCEL");
        rec.setValue(SESSION_TABLE+".date_end", d);
        rec.setValue(SESSION_TABLE+".time_end", new Time(d.getTime()));
        ds.saveRecord(rec);
    }
    private void endSession(){   
        String[] fields = {"session_id","user_name","status","bl_id","fl_id","date_end","time_end"};
        final DataSource ds = DataSourceFactory.createDataSourceForFields(SESSION_TABLE, fields);
        final DataRecord rec = ds.getRecord("session_id = " + Integer.toString(this.sessionId));
        rec.getString(SESSION_TABLE+".status");
        final Date d = new Date();
        rec.setValue(SESSION_TABLE+".status", "3_ENDED");
        rec.setValue(SESSION_TABLE+".date_end", d);
        rec.setValue(SESSION_TABLE+".time_end", new Time(d.getTime()));
        ds.saveRecord(rec);
  }

    /**
     * TODO checkout. 
     * @param invocation
     * args contient deux paramètres :  [0] string contenant le nom du dwg
     *                                  [1] boolea
     * @throws Throwable 
     */
    private void checkout(MethodInvocation invocation) throws ExceptionBase {
        // TODO Auto-generated method stub
        
        //R
        Object[] args = invocation.getArguments();
        try {
            //this.TestUnit();
            if(!AmIOnPlanReferencing()){
            String status = this.pim_s_workflow_step(this.pdb);
            if(status == null){
                logDebug("ERROR","Le statu est null.");
                throw new ExceptionBase("Le statu est null.");
            }else{
                logDebug("INFO","LE statu du pdb :"+status);
               if(status.equals(this.STEP_10)){
                   this.createSession();
                   if(this.pim_i_evenement(2,2,0)){
                   if(pim_u_workflow_step(this.pdb,this.STEP_10,this.STEP_21,this.userId) == 1){
                           if(this.pim_u_plan(""+this.userId,null)){ 
                               logDebug("INFO","PIM U PLAN OK'");
                               
                               //insert dans immosis in 
                            if(this.export_data()){
                                    this.processAllData(this.sessionId, this.blId, this.flId);
                                    this.doBackup();
                                    logDebug("INFO","CHECK OUT FINI ");
                            }else{
                                logDebug("ERROR","Une erreur est survenue lors de l'export des data.'");
                                throw new ExceptionBase("Une erreur est survenue lors de l'export des data.");
                            }
                           }else{
                               logDebug("ERROR","PIM U PLAN ERROR");
                              throw new ExceptionBase("Le plan n'est pas référencé et n'a pas été référencé");
                           }  
                       }else{
                           logDebug("ERROR","Une erreur est survenue lors de la mise à jour du statu.");
                           throw new ExceptionBase("Impossible de créer l'evenement");
                        }     
                    }else{
                        logDebug("ERROR","Impossible de créer l'evenement");
                        throw new ExceptionBase("Une erreur est survenue lors de la mise à jour du statu.");
                    }
                }else{
                    logDebug("ERROR","Le plan n est pas au bon statu.");
                    throw new ExceptionBase("Le plan n est pas au bon statu.");
                }
            }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            logDebug("ERROR","Une erreur est survenue lors du post process checkout.'");
            e.printStackTrace();
            throw new ExceptionBase("Une erreur est survenue lors du post process checkout.");
        }
        
    }
    public boolean AmIonCheckinAfterReferencing(){
        boolean retour = false;
        try{
            String is_referenced= "SELECT pdb_log_type  from ar_kitpdb_logs  WHERE user_pk ='"+this.userId+"' AND pdb ='"+this.pdb+"' AND pdb_log_type IN ('CHECKIN','REFERENCEMENT') ORDER BY LOG_DATE DESC";
            logDebug("INFO","Am i on plan referencing check :"+is_referenced);
            DbConnection.ThreadSafe dbCnx = ContextStore.get().getDbConnection();
            ResultSet internalSet = dbCnx.execute(is_referenced, 0);
            int count = 0;
            while(internalSet.next() && count==0){
                logDebug("DEBUG","LOG TYPE :"+internalSet.getString(1));
                retour = (internalSet.getString(1).equals("REFERENCEMENT"));
                count++;
            } 
            logDebug("INFO","Am i on plan referencing ? "+retour);
            return retour;
        }catch(Exception e){
            e.printStackTrace();
            logDebug("ERROR","Am i on plan referencing ? "+e.getMessage());
            return false;
            
        }
    }
    public boolean AmIOnPlanReferencing(){
        boolean retour = false;
        try{
            String is_referenced= "SELECT CASE WHEN (nb = 1 AND (SYSDATE - to_date(log_date, 'YYYY-MM-DD HH24:MI:SS')) < 0.0006) THEN 1 ELSE 0 END is_reference_case FROM ( "+
                    "select count(*) nb,MIN(log_date) log_date "+
                    "from ar_kitpdb_logs "+
                    "WHERE user_pk ='"+this.userId+"' AND pdb ='"+this.pdb+"' AND PDB_LOG_TYPE ='REFERENCEMENT')";
            logDebug("INFO","Am i on plan referencing check :"+is_referenced);
            DbConnection.ThreadSafe dbCnx = ContextStore.get().getDbConnection();
            ResultSet internalSet = dbCnx.execute(is_referenced, 0);
            while(internalSet.next()){
                retour = (internalSet.getInt(1) >0);
            }
            logDebug("INFO","Am i on plan referencing ? "+retour);
            return retour;
        }catch(Exception e){
            e.printStackTrace();
            logDebug("ERROR","Am i on plan referencing ? "+e.getMessage());
            return false;
            
        }
    }

    /**
     * TODO checkin. 
     * @param invocation
     - * Check and unlock 01 true  02 true
     - * Check in and keep locked 01 false 02 true
     - * Discard changes and unlock  01 true 02 false
     * 
     */
    private void checkin(MethodInvocation invocation) {
        // TODO Auto-generated method stub
        Object[] args = invocation.getArguments();
        boolean args1 = (Boolean) args[1];
        boolean args2 = (Boolean) args[2];
        
        //Discard ...
        if(!this.AmIonCheckinAfterReferencing()){
                if(args1 && !args2){
                    this.cancelSession();
                    try{
                    if(this.pim_u_workflow_step(this.pdb, this.STEP_21, this.STEP_10, this.userId) != 1){
                        logDebug("ERROR","pim_u_workflow_step error lors de l'annulation'");
                       throw new ExceptionBase("pim_u_workflow_step error");
                    }else{
                        logDebug("INFO","Succès de l'annulation");
                        this.endSession();
                   }
                    }catch(Exception e){
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        logDebug("ERROR","Une erreur est survenue");
                       throw new ExceptionBase("Une erreur est survenue");
                    }
                }else{
                    boolean isreferenced =false;
                    boolean processSucess = false;
                    String status = null;
            
                    try {
                        isreferenced = this.planIsReferenced(this.pdb);
                        if(isreferenced){
                            status = this.pim_s_workflow_step(this.pdb);
                                if(status.equals(this.STEP_21)){
                                    // ON DOIT LE FAIRE ICI ---
                                
                                this.fillExportTables(sessionId);
                                if(this.immosis_out_pdb_etiquettes(this.pdb)){
                                    if(this.immosis_out_pdb_maj(this.pdb)){
                                        if(this.immosis_out_tcp_locaux(this.pdb)){
                                            if(this.pim_i_evenement(2,3,0)){
                                                if(pim_u_workflow_step(this.pdb,this.STEP_21,this.STEP_22,this.userId) == 1){
                                                        if(this.pim_u_plan(null,""+this.userId)){ 
                                                            logDebug("INFO","Succès du CHECKIN'");
                                                            this.endSession();
                                                        }else{
                                                            logDebug("ERROR","PIM U PLAN ERROR");
                                                           throw new ExceptionBase("Le plan n'est pas référencé et n'a pas été référencé");
                                                        }  
                                                    }else{
                                                        logDebug("ERROR","pim_u_workflow_step.");
                                                        throw new ExceptionBase("pim_u_workflow_step");
                                                     }     
                                                 }else{
                                                     logDebug("ERROR","Impossible de créer l'evenement");
                                                     throw new ExceptionBase("Une erreur est survenue lors de la mise à jour du statu.");
                                                 }
                                        }else{
                                            logDebug("ERROR","immosis_out_tcp_locaux'");
                                            throw new ExceptionBase("immosis_out_tcp_locaux");
                                        }
                                    }else{
                                        logDebug("ERROR","immosis_out_pdb_maj error msg'");
                                        throw new ExceptionBase("immosis_out_pdb_maj error msg");
                                    }
                                }
                            }else{
                                logDebug("ERROR","Le plan est référence mais le statu n'est pas bon ["+status+"]'");
                               throw new ExceptionBase("Le plan est référence mais le statu n'est pas bon ["+status+"]");
                            }
                        }else{
                            //case 3 et 4
                            logDebug("INFO","Le plan n'est pas référencé");
                            
                            if(this.pim_i_evenement(3,1,0)){
                            if(this.pim_u_plan(null,null)){
                                logDebug("REFERENCEMENT","Succès de la déclaration de plan");
                            }else{
                                logDebug("ERROR","Le plan n'est pas référencé et n'a pas été référencé(pim u plan)");
                               throw new ExceptionBase("Le plan n'est pas référencé et n'a pas été référencé");
                            }
                            }else{
                                logDebug("ERROR","Le plan n'est pas référencé et n'a pas été référencé (pim i evenement)");
                               throw new ExceptionBase("Le plan n'est pas référencé et n'a pas été référencé");
                            }
                        }
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        logDebug("ERROR","Une erreur est survenue :");
                       throw new ExceptionBase("Une erreur est survenue");
                    }
                }
        }
        
    }
    /* Methods use by checkin and chekout */
    /*
     * Show a message on archibus.log
     */
    protected void logDebug(String code,String message)
    {
      this.KitPdbLog(code,message);
      if (log.isDebugEnabled()) {
        log.debug(message);
      }
    }
    private void KitPdbLog(String code,String message){
        try{
            String sqlInsert= " BEGIN INSERT INTO ar_kitpdb_logs (user_pk,pdb,pdb_log_type,pdb_log_msg) "
                    + "VALUES('"+this.userId+"','"+this.pdb+"','"+code+"','"+message.replace("'", "''")+"');COMMIT;END;";

            DbConnection.ThreadSafe dbCnx = ContextStore.get().getDbConnection();
            dbCnx.executeUpdate(sqlInsert);
            log.debug("KitPdbLog success");
        }catch(Exception e){
            e.printStackTrace();
            
        }
    }
    /*
     * Call the proc with the same name in immosis db.
     */
   private String pim_s_workflow_step (String pdb) throws Exception {
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        String retour = null;
        try {
            dbConnection = this.getDBConnection();
            callableStatement = dbConnection.prepareCall("{call AIM_PTBPDB_COMMUN.PIM_S_WORKFLOW_STEP(?,?)}");
            callableStatement.setString(1, pdb);
            callableStatement.registerOutParameter(2, java.sql.Types.VARCHAR);
            callableStatement.executeUpdate();
            retour = callableStatement.getString(2);
            return retour;
        } catch (Exception e) {
            e.printStackTrace();
            logDebug("ERROR",e.getMessage());
        } finally {

            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
        return retour;

    } 
    /*
     * Call the proc with the same name in immosis db.
     */
   private int pim_u_workflow_step(String pdb,String pi_from_wfs_code,String pi_toward_wfs_code,int pi_usr_pk) throws Exception {

        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        int retour = 0;
        try {
            dbConnection = this.getDBConnection();
            callableStatement = dbConnection.prepareCall("{call AIM_PTBPDB_COMMUN.PIM_U_WORKFLOW_STEP(?,?,?,?,?)}");
            callableStatement.setString(1, this.pdb);
            callableStatement.setString(2, pi_from_wfs_code);
            callableStatement.setString(3, pi_toward_wfs_code);
            callableStatement.setInt(4, pi_usr_pk);
            callableStatement.registerOutParameter(5, java.sql.Types.INTEGER);
            callableStatement.executeUpdate();
            retour = callableStatement.getInt(5);
            return retour;
        } catch (Exception e) {
            e.printStackTrace();
            logDebug("ERROR",e.getMessage());
        } finally {

            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
        return retour;

    } 
    /*
     * Call the proc with the same name in immosis db.
     */
    @SuppressWarnings("finally")
    private boolean pim_u_plan(String userPrisFk,String userRendreFk) throws Exception {
        boolean retour =false;
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        try {
            dbConnection = this.getDBConnection();
            callableStatement = dbConnection.prepareCall("{call AIM_PTBPDB_COMMUN.PIM_U_PLAN(?,?,?,?,?,?,?,?,?)}");
            callableStatement.setString(1, this.pdb.substring(0,7));
            callableStatement.setString(2, this.pdb.substring(8,11));
            callableStatement.setString(3, this.flId);
            callableStatement.setInt(4, 1);
            callableStatement.setString(5, this.ftpLocalFolder); // ligne modifiée le 05/02/2019 
            callableStatement.setString(6, userPrisFk);
            callableStatement.setString(7, userRendreFk);
            callableStatement.setString(8, null);
            callableStatement.setString(9, null);
            callableStatement.executeUpdate();
            retour = true;
        } catch (Exception e) {
            logDebug("ERROR",e.getMessage());
            retour=false;
        }finally {

            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
            return retour;
        }
    }

    /*
     * Call the proc with the same name in immosis db.
     * TEG_CODE = FL_ID 
     */
    @SuppressWarnings("finally")
    private boolean pim_i_evenement(int pi_n_etat_evt,int pi_n_type_evt,int pi_n_msg) throws Exception {
        logDebug("INFO","userId :"+this.userId);
        logDebug("INFO","pi_v_uto_codepi_v_uto_code :"+this.pdb.substring(0,7));
        logDebug("INFO","pi_v_bap_code :"+this.pdb.substring(8,11));
        logDebug("INFO","pi_v_teg_code :"+this.flId);
        logDebug("INFO","pi_n_etat_evt :"+pi_n_etat_evt);
        logDebug("INFO","pi_n_type_evt :"+pi_n_type_evt);
        logDebug("INFO","pi_n_msg :"+pi_n_msg);
        boolean retour =false;
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        try {
            dbConnection = this.getDBConnection();
            callableStatement = dbConnection.prepareCall("{call AIM_PTBPDB_EVENEMENT.PIM_I_EVENEMENT(?,?,?,?,?,?,?)}");
            callableStatement.setInt(1, this.userId);
            callableStatement.setString(2, this.pdb.substring(0,7));
            callableStatement.setString(3, this.pdb.substring(8,11));
            callableStatement.setString(4, this.flId);
            callableStatement.setInt(5, pi_n_etat_evt);
            callableStatement.setInt(6, pi_n_type_evt);
            callableStatement.setInt(7, pi_n_msg);
            callableStatement.executeUpdate();
            retour = true;
        } catch (Exception e) {
            logDebug("ERROR",e.getMessage());
            retour=false;
        }finally {

            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
            return retour;
        }
    }
    /*
     * Acquire a connection object from immosis database
     */
    private Connection getDBConnection() {
        Connection dbConnection = null;
        try {

            DbConnection.ThreadSafe dbCnx = ContextStore.get().getDbConnection();
            ResultSet internalSet = dbCnx.execute("SELECT * from ar_kitpdb_conf" , 0);
            while(internalSet.next()){
                this.dbConnection = internalSet.getString(1);
                this.dbUser = internalSet.getString(2);
                this.dbPassword = internalSet.getString(3);
                this.dbDriver = internalSet.getString(4);
                this.ftpServer = internalSet.getString(5);
                this.ftpUsername = internalSet.getString(6);
                this.ftpPassword= internalSet.getString(7);
                this.ftpPort = internalSet.getString(8);
                this.ftpRemoteFolder = internalSet.getString(9);
                this.ftpLocalFolder = internalSet.getString(10); // ce dossier est à utiliser dans pim_u_plan
            }
            Class.forName(this.dbDriver);
            dbConnection = DriverManager.getConnection(
            this.dbConnection, this.dbUser,this.dbPassword);
            return dbConnection;

        } catch (Exception e) {
            logDebug("ERROR",e.getMessage());
            }
        return dbConnection;

    }
    
    /*
     * Perfom a transfer file from local to immosis sftp
     */
    
    private boolean exportDWG(){
       
        FTPClient ftp = new FTPClient();
        String remotedir = this.ftpRemoteFolder+"/";
        String ftpDWG = this.pdb.toLowerCase();
        int reply;
        try {
            ftp = new FTPClient();
            ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
            
            ftp.connect(this.ftpServer,Integer.parseInt(this.ftpPort));
            logDebug("INFO","FTP URL is:"+ftp.getDefaultPort());
            reply = ftp.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                ftp.disconnect();
                logDebug("ERROR","Impossible de se connecter le server ftp");
                return false;
            }
            ftp.login(this.ftpUsername, this.ftpPassword);
            ftp.setFileType(FTP.BINARY_FILE_TYPE);
            ftp.enterLocalPassiveMode();
            ftp.makeDirectory(remotedir);
            ftp.changeWorkingDirectory(remotedir);
            ftp.enterLocalPassiveMode();
            logDebug("INFO","Connecté au serveur ftp");
            InputStream input = new FileInputStream(new File(this.ftpLocalFolder+ftpDWG));
            logDebug("INFO","le stream a bien ete cree ["+this.ftpLocalFolder+ftpDWG+"]"); 
            ftp.storeFile(ftpDWG, input);
            logDebug("INFO","Le fichier a bien été transfere"); 
            if (ftp.isConnected()) {
                try {
                    ftp.logout();
                    ftp.disconnect();
                } catch (IOException f) {
                    // do nothing as file is already saved to server
                }
            }
                return true;
         }catch(Exception e){
            logDebug("ERROR",e.getMessage());
            return false;
        }
        
    }
    /* Checkout methods -------------------------------------------------------- */
    
    private boolean export_data(){
        boolean retour =true;
        try {
            if(this.pim_s_export_commune())
                if(this.pim_s_export_type_batiment())
                    if(this.pim_s_export_emploi())
                        if(this.pim_s_export_sous_domaine())
                            if(this.pim_s_export_ut())
                                if(this.pim_s_export_region_sncf())
                                    if(this.pim_s_export_ligne())
                                        if(this.pim_s_export_type_etage())
                                            if(this.pim_s_export_plaque())
                                                if(this.pim_s_export_zonegare())
                                                    if(this.pim_s_export_bap())
                                                        if(this.pim_s_export_locaux())
                                                            if(this.pim_s_export_udj())
                                                                if(this.pim_s_export_bal())
                                                                   return true;
                                                                else return false;
                                                            else return false;
                                                       else return false;
                                                    else return false;
                                                 else return false;
                                            else return false;
                                         else return false;
                                    else return false;
                                else return false;
                            else return false;
                        else return false;
                    else return false;
                else return false;
            else return false;
         
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return retour;
    }
    @SuppressWarnings("finally")
    private boolean pim_s_export_commune() throws Exception{
        boolean retour =false;
        logDebug("INFO","Execution de pim_s_export_commune");
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        ResultSet rs;
        String insertSQL ="BEGIN ";
        DbConnection.ThreadSafe dbCnx;
        ResultSet internalSet;
        try {
            dbConnection = this.getDBConnection();
            callableStatement = dbConnection.prepareCall("{CALL AIM_PDB_PRENDRE.PIM_S_EXPORT_COMMUNE(?,?)}");
            callableStatement.setString(1,this.pdb);
            callableStatement.registerOutParameter(2,OracleTypes.CURSOR);
            callableStatement.executeUpdate();
            // get cursor and cast it to ResultSet
            rs = (ResultSet) callableStatement.getObject(2);
            // loop it like normal
            int line =0;
            while (rs.next()) {
                String[] args = rs.getString(1).split("#");
                insertSQL += "INSERT INTO immosis_in_tcp_commune(SESSION_ID,LINE_NR,COM_CODE_INSEE,COM_LIB_INSEE) VALUES("+this.sessionId+","+line+",'"+args[0].replace("'", "''")+"','"+args[1].replace("'", "''")+"');";
                line++;
            }
            insertSQL+=" COMMIT; END;";
            logDebug("INFO","Execution de la requète pim_s_export_commune :"+insertSQL);
            dbCnx = ContextStore.get().getDbConnection();
            dbCnx.executeUpdate(insertSQL);
            retour = true;
        } catch (Exception e) {
            e.printStackTrace();
            logDebug("ERROR",e.getMessage());
            return false;
        }finally{
            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
            return retour;
        }
    }
    @SuppressWarnings("finally")
    private boolean pim_s_export_type_batiment() throws Exception{

        boolean retour =false;
        logDebug("INFO","Execution de pim_s_export_type_batiment");
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        ResultSet rs;
        String insertSQL ="BEGIN ";
        DbConnection.ThreadSafe dbCnx;
        ResultSet internalSet;
        try {
            dbConnection = this.getDBConnection();
            callableStatement = dbConnection.prepareCall("{call AIM_PDB_PRENDRE.PIM_S_EXPORT_TYPE_BATIMENT(?,?)}");
            callableStatement.setString(1,this.pdb);
            callableStatement.registerOutParameter(2,OracleTypes.CURSOR);
            callableStatement.executeUpdate();
            // get cursor and cast it to ResultSet
            rs = (ResultSet) callableStatement.getObject(2);
            // loop it like normal
            dbCnx = ContextStore.get().getDbConnection();
            int line = 0;
            while (rs.next()) {
                String[] args = rs.getString(1).split("#");
                insertSQL += "INSERT INTO immosis_in_tcp_type_batiment(SESSION_ID,LINE_NR,TBA_CODE,TBA_LIB) VALUES("+this.sessionId+","+line+",'"+args[0].replace("'", "''")+"','"+args[1].replace("'", "''")+"');";
                line++;
            }
            insertSQL+=" COMMIT; END;";
            logDebug("INFO","Execution de la requète pim_s_export_type_batiment :"+insertSQL);

            dbCnx = ContextStore.get().getDbConnection();
            dbCnx.executeUpdate(insertSQL);
            retour = true;
        } catch (Exception e) {
            logDebug("ERROR","Erreur lors du pim_s_export_type_batiment:"+e.getMessage());
            e.printStackTrace();
            retour = false;
        } finally {

            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
            return retour;
        }
    }
    @SuppressWarnings("finally")
    private boolean pim_s_export_emploi() throws Exception{
        logDebug("INFO","Execution de pim_s_export_emploi");
        boolean retour =false;
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        ResultSet rs;
        String insertSQL ="BEGIN ";
        DbConnection.ThreadSafe dbCnx;
        ResultSet internalSet;
        try {
            dbConnection = this.getDBConnection();
            callableStatement = dbConnection.prepareCall("{call AIM_PDB_PRENDRE.PIM_S_EXPORT_EMPLOI(?)}");
            callableStatement.registerOutParameter(1,OracleTypes.CURSOR);
            callableStatement.executeUpdate();
            // get cursor and cast it to ResultSet
            rs = (ResultSet) callableStatement.getObject(1);
            // loop it like normal
            dbCnx = ContextStore.get().getDbConnection();
            int line =0;
            String values="";
            while (rs.next()) {             
                String[] args = rs.getString(1).split("#");
                insertSQL += "INSERT INTO immosis_in_tcp_emploi(SESSION_ID,LINE_NR";
                values= this.sessionId+","+line;
                if(args[0].length()>0){
                    insertSQL+=",EMP_CLE";
                    values+=","+Integer.parseInt(args[0]);
                }
                insertSQL+=",EMP_LIB) VALUES("+values+",'"+args[1].replace("'", "''")+"');";
                line++;
            }
            insertSQL+=" COMMIT; END;";
            logDebug("INFO","Execution de la requète pim_s_export_emploi :"+insertSQL);

            dbCnx = ContextStore.get().getDbConnection();
            dbCnx.executeUpdate(insertSQL);
            retour = true;
        } catch (Exception e) {
            logDebug("ERROR","Erreur lors du pim_s_export_emploi:"+e.getMessage());
            retour = false;
        } finally {

            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
            return retour;
        }
    }
    @SuppressWarnings("finally")
    private boolean pim_s_export_sous_domaine() throws Exception{
        logDebug("INFO","Execution de pim_s_export_sous_domaine");
        boolean retour =false;
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        ResultSet rs;
        String insertSQL ="BEGIN ";
        DbConnection.ThreadSafe dbCnx;
        ResultSet internalSet;
        try {
            dbConnection = this.getDBConnection();
            callableStatement = dbConnection.prepareCall("{call AIM_PDB_PRENDRE.PIM_S_EXPORT_SOUS_DOMAINE(?)}");
            callableStatement.registerOutParameter(1,OracleTypes.CURSOR);
            callableStatement.executeUpdate();
            // get cursor and cast it to ResultSet
            rs = (ResultSet) callableStatement.getObject(1);
            // loop it like normal
            dbCnx = ContextStore.get().getDbConnection();
            int line =0;
            while (rs.next()) {
                String[] args = rs.getString(1).split("#");
                insertSQL+= "INSERT INTO immosis_in_tcp_sous_domaine(SESSION_ID,LINE_NR,SOD_CODE,SOD_LIB,DOM_CODE) VALUES(";
                insertSQL+= this.sessionId+","+line+",'"+args[0].replace("'", "''")+"','"+args[1].replace("'", "''")+"','"+args[2].replace("'", "''")+"');";
                line++;
            }
            insertSQL+=" COMMIT; END;";
            logDebug("INFO","Execution de la requète pim_s_export_sous_domaine :"+insertSQL);

            dbCnx = ContextStore.get().getDbConnection();
            dbCnx.executeUpdate(insertSQL);
            retour = true;
        } catch (Exception e) {
            logDebug("ERROR","Erreur lors du pim_s_export_sous_domaine:"+e.getMessage());
            retour = false;
        } finally {

            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
            return retour;
        }
    }
    @SuppressWarnings("finally")
    private boolean pim_s_export_ut() throws Exception{
        logDebug("INFO","Execution de pim_s_export_ut");
        boolean retour =false;
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        ResultSet rs;
        String insertSQL ="BEGIN ";
        DbConnection.ThreadSafe dbCnx;
        ResultSet internalSet;
        try {
            dbConnection = this.getDBConnection();
            callableStatement = dbConnection.prepareCall("{call AIM_PDB_PRENDRE.PIM_S_EXPORT_UT(?,?)}");
            callableStatement.setString(1,this.pdb);
            callableStatement.registerOutParameter(2,OracleTypes.CURSOR);
            callableStatement.executeUpdate();
            // get cursor and cast it to ResultSet
            rs = (ResultSet) callableStatement.getObject(2);
            // loop it like normal
            dbCnx = ContextStore.get().getDbConnection();
            int line = 0;
            String values ="";
            while (rs.next()) {
                String[] args = rs.getString(1).split("#");
                insertSQL+= "INSERT INTO immosis_in_tcp_ut(SESSION_ID,LINE_NR,UTO_CODE";
                values= this.sessionId+","+line+",'"+args[0].replace("'", "''")+"'";
                if(args[1].length()>0){
                    insertSQL+=",REG_CODE";
                    values+=","+Integer.parseInt(args[1]);
                }
                insertSQL+= ",UTO_LIB) VALUES("+values+",'"+args[2].replace("'", "''")+"');";
                line++;
            }
            insertSQL+=" COMMIT; END;";
            logDebug("INFO","Execution de la requète pim_s_export_ut :"+insertSQL);
            dbCnx = ContextStore.get().getDbConnection();
            dbCnx.executeUpdate(insertSQL);
            retour = true;
        } catch (Exception e) {
            e.printStackTrace();
            logDebug("ERROR","Erreur lors du pim_s_export_ut:"+e.getMessage());
            retour = false;
        } finally {

            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
            return retour;
        }
    }
    @SuppressWarnings("finally")
    private boolean pim_s_export_region_sncf() throws Exception{
        logDebug("INFO","Execution de pim_s_export_region_sncf");
        boolean retour =false;
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        ResultSet rs;
        String insertSQL ="BEGIN ";
        DbConnection.ThreadSafe dbCnx;
        ResultSet internalSet;
        try {
            dbConnection = this.getDBConnection();
            callableStatement = dbConnection.prepareCall("{call AIM_PDB_PRENDRE.PIM_S_EXPORT_REGION_SNCF(?,?)}");
            callableStatement.setString(1,this.pdb);
            callableStatement.registerOutParameter(2,OracleTypes.CURSOR);
            callableStatement.executeUpdate();
            // get cursor and cast it to ResultSet
            rs = (ResultSet) callableStatement.getObject(2);
            // loop it like normal
            dbCnx = ContextStore.get().getDbConnection();
            int line =0;
            while (rs.next()) {
                String[] args = rs.getString(1).split("#");
                insertSQL+= "INSERT INTO immosis_in_tcp_region_sncf(SESSION_ID,LINE_NR,REG_CODE,REG_LIB,REG_ADRESSE,PLA_CODE) VALUES(";
                insertSQL+= this.sessionId+","+line+",'"+args[0].replace("'", "''")+"','"+args[1].replace("'", "''")+"','"+args[2].replace("'", "''")+"','"+args[3].replace("'", "''")+"');";
                line++;
            }
            insertSQL+=" COMMIT; END;";
            logDebug("INFO","Execution de la requète pim_s_export_region_sncf :"+insertSQL);
            dbCnx = ContextStore.get().getDbConnection();
            dbCnx.executeUpdate(insertSQL);
            retour = true;
        } catch (Exception e) {
            e.printStackTrace();
            logDebug("ERROR","Erreur lors du pim_s_export_region_sncf:"+e.getMessage());
            retour = false;
        } finally {

            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
            return retour;
        }
    }
    private boolean pim_s_export_ligne() throws Exception{
        logDebug("INFO","Execution de pim_s_export_ligne");
        boolean retour =false;
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        ResultSet rs;
        String insertSQL ="BEGIN ";
        DbConnection.ThreadSafe dbCnx;
        ResultSet internalSet;
        try {
            dbConnection = this.getDBConnection();
            callableStatement = dbConnection.prepareCall("{call AIM_PDB_PRENDRE.PIM_S_EXPORT_LIGNE(?,?)}");
            callableStatement.setString(1,this.pdb);
            callableStatement.registerOutParameter(2,OracleTypes.CURSOR);
            callableStatement.executeUpdate();
            // get cursor and cast it to ResultSet
            rs = (ResultSet) callableStatement.getObject(2);
            // loop it like normal
            dbCnx = ContextStore.get().getDbConnection();
            int line = 0;
            while (rs.next()) {
                String[] args = rs.getString(1).split("#");
                insertSQL+= "INSERT INTO immosis_in_tcp_ligne(SESSION_ID,LINE_NR,LIG_CODE,LIG_LIB) VALUES(";
                insertSQL+= this.sessionId+","+line+",'"+args[0].replace("'", "''")+"','"+args[1].replace("'", "''")+"');";
                line++;
            }
            insertSQL+=" COMMIT; END;";
            logDebug("INFO","Execution de la requète pim_s_export_ligne :"+insertSQL);
            dbCnx = ContextStore.get().getDbConnection();
            dbCnx.executeUpdate(insertSQL);
            retour = true;
        } catch (Exception e) {
            e.printStackTrace();
            logDebug("ERROR","Erreur lors du pim_s_export_ligne:"+e.getMessage());
            retour = false;
        } finally {

            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
            return retour;
        }
    }
    private boolean pim_s_export_type_etage() throws Exception{
        logDebug("INFO","Execution de pim_s_export_type_etage");
        boolean retour =false;
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        ResultSet rs;
        String insertSQL ="BEGIN ";
        DbConnection.ThreadSafe dbCnx;
        ResultSet internalSet;
        try {
            dbConnection = this.getDBConnection();
            callableStatement = dbConnection.prepareCall("{call AIM_PDB_PRENDRE.PIM_S_EXPORT_TYPE_ETAGE(?,?)}");
            callableStatement.setString(1,this.pdb);
            callableStatement.registerOutParameter(2,OracleTypes.CURSOR);
            callableStatement.executeUpdate();
            // get cursor and cast it to ResultSet
            rs = (ResultSet) callableStatement.getObject(2);
            // loop it like normal
            dbCnx = ContextStore.get().getDbConnection();
            int line =0;
            while (rs.next()) {
                String[] args = rs.getString(1).split("#");
                insertSQL+= "INSERT INTO immosis_in_tcp_type_etage(SESSION_ID,LINE_NR,TEG_CODE,TEG_LIB) VALUES(";
                insertSQL+= this.sessionId+","+line+",'"+args[0].replace("'", "''")+"','"+args[1].replace("'", "''")+"');";
                line++;
            }
            insertSQL+=" COMMIT; END;";
            logDebug("INFO","Execution de la requète pim_s_export_type_etage :"+insertSQL);
            dbCnx = ContextStore.get().getDbConnection();
            dbCnx.executeUpdate(insertSQL);
            retour = true;
        } catch (Exception e) {
            e.printStackTrace();
            logDebug("ERROR","Erreur lors du pim_s_export_type_etage:"+e.getMessage());
            retour = false;
        } finally {

            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
            return retour;
        }
    }
    private boolean pim_s_export_plaque() throws Exception{
        logDebug("INFO","Execution de pim_s_export_plaque");
        boolean retour =false;
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        ResultSet rs;
        String insertSQL ="BEGIN ";
        DbConnection.ThreadSafe dbCnx;
        ResultSet internalSet;
        try {
            dbConnection = this.getDBConnection();
            callableStatement = dbConnection.prepareCall("{call AIM_PDB_PRENDRE.PIM_S_EXPORT_PLAQUE(?,?)}");
            callableStatement.setString(1,this.pdb);
            callableStatement.registerOutParameter(2,OracleTypes.CURSOR);
            callableStatement.executeUpdate();
            // get cursor and cast it to ResultSet
            rs = (ResultSet) callableStatement.getObject(2);
            // loop it like normal
            dbCnx = ContextStore.get().getDbConnection();
            int line =0;
            while (rs.next()) {
                String[] args = rs.getString(1).split("#");
                insertSQL+= "INSERT INTO immosis_in_tcp_plaque(SESSION_ID,LINE_NR,PLA_CODE,PLA_LIB) VALUES(";
                insertSQL+= this.sessionId+","+line+",'"+args[0].replace("'", "''")+"','"+args[1].replace("'", "''")+"');";
                line++;
            }
            insertSQL+=" COMMIT; END;";
            logDebug("INFO","Execution de la requète pim_s_export_plaque :"+insertSQL);
            dbCnx = ContextStore.get().getDbConnection();
            dbCnx.executeUpdate(insertSQL);
            retour = true;
        } catch (Exception e) {
            e.printStackTrace();
            logDebug("ERROR","Erreur lors du pim_s_export_plaque:"+e.getMessage());
            retour = false;
        } finally {

            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
            return retour;
        }
    }
    private boolean pim_s_export_zonegare() throws Exception{
        logDebug("INFO","Execution de pim_s_export_zonegare");
        boolean retour =false;
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        ResultSet rs;
        String insertSQL ="BEGIN ";
        DbConnection.ThreadSafe dbCnx;
        ResultSet internalSet;
        try {
            dbConnection = this.getDBConnection();
            callableStatement = dbConnection.prepareCall("{call AIM_PDB_PRENDRE.PIM_S_EXPORT_ZONEGARE(?)}");
            callableStatement.registerOutParameter(1,OracleTypes.CURSOR);
            callableStatement.executeUpdate();
            // get cursor and cast it to ResultSet
            rs = (ResultSet) callableStatement.getObject(1);
            // loop it like normal
            dbCnx = ContextStore.get().getDbConnection();
            int line =0;
            String values="";
            while (rs.next()) {
                String[] args = rs.getString(1).split("#");
                insertSQL+= "INSERT INTO immosis_in_pdb_zonegare(SESSION_ID,LINE_NR";
                values= this.sessionId+","+line;
                if(args[0].length()>0){
                    insertSQL+=",ZONEGARE_CODE";
                    values+=","+Integer.parseInt(args[0]);
                }
                insertSQL+= ",ZONEGARE_LIB) VALUES("+values+",'"+args[1].replace("'", "''")+"');";
                line++;
            }
            insertSQL+=" COMMIT; END;";
            logDebug("INFO","Execution de la requète pim_s_export_zonegare :"+insertSQL);
            dbCnx = ContextStore.get().getDbConnection();
            dbCnx.executeUpdate(insertSQL);
            retour = true;
        } catch (Exception e) {
            e.printStackTrace();
            logDebug("ERROR","Erreur lors du pim_s_export_zonegare:"+e.getMessage());
            retour = false;
        } finally {

            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
            return retour;
        }
    }
    @SuppressWarnings("finally")
    private boolean pim_s_export_bap() throws Exception{
        logDebug("INFO","Execution de pim_s_export_bap");
        boolean retour =false;
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        ResultSet rs;
        String insertSQL ="BEGIN ";
        DbConnection.ThreadSafe dbCnx;
        ResultSet internalSet;
        try {
            dbConnection = this.getDBConnection();
            callableStatement = dbConnection.prepareCall("{call AIM_PDB_PRENDRE.PIM_S_EXPORT_BAP(?,?)}");
            callableStatement.setString(1,this.pdb);
            callableStatement.registerOutParameter(2,OracleTypes.CURSOR);
            callableStatement.executeUpdate();
            // get cursor and cast it to ResultSet
            rs = (ResultSet) callableStatement.getObject(2);
            // loop it like normal
            dbCnx = ContextStore.get().getDbConnection();
            int line =0;
            String values ="";
            while (rs.next()) {
                String[] args = rs.getString(1).split("#");
                insertSQL+= "INSERT INTO immosis_in_tcp_batiment_p(SESSION_ID,LINE_NR";
                values= this.sessionId+","+line;
                if(args[0].length()>0){
                    insertSQL+=",BAP_PLAN_INTEGRE";
                    values+=","+Integer.parseInt(args[0]);
                }
                if(args[1].length()>0){
                    insertSQL+=",BAP_DESC_TERMINEE";
                    values+=","+Integer.parseInt(args[1]);
                }
                insertSQL+=",UTO_CODE,BAP_CODE,LIG_CODE,COP_PK,COP_DATE_MES,COP_LIB,TBA_CODE";
                values+= ",'"+args[2].replace("'", "''").replace("'", "''")+"','"+args[3].replace("'", "''")+"','"+args[4].replace("'", "''")+"','";
                values+= args[5]+"',to_date('"+args[6].replace(".0","")+"','DD/MM/YYYY'),'"+args[7].replace("'", "''")+"','"+args[8].replace("'", "''")+"'";
                if(args[9].length()>0){
                    insertSQL+=",EMP_CLE";
                    values+=","+Integer.parseInt(args[9]);
                }
                if(args[10].length()>0){
                    insertSQL+=",BAP_SURF_PROJETEE";
                    values+=","+Integer.parseInt(args[10]);
                }
                
                insertSQL+=") VALUES("+values+");";

                line++;
            }
            insertSQL+=" COMMIT; END;";
            logDebug("INFO","Execution de la requète pim_s_export_bap :"+insertSQL);
            dbCnx = ContextStore.get().getDbConnection();
            dbCnx.executeUpdate(insertSQL);
            retour = true;
        } catch (Exception e) {
            e.printStackTrace();
            logDebug("ERROR","Erreur lors du pim_s_export_bap:"+e.getMessage());
            retour = false;
        } finally {

            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
            return retour;
        }
    }
    @SuppressWarnings("finally")
    private boolean pim_s_export_locaux() throws Exception{
        logDebug("INFO","Execution de pim_s_export_locaux");
        boolean retour =false;
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        ResultSet rs;
        String insertSQL ="BEGIN ";
        DbConnection.ThreadSafe dbCnx;
        ResultSet internalSet;
        try {
            dbConnection = this.getDBConnection();
            callableStatement = dbConnection.prepareCall("{call AIM_PDB_PRENDRE.PIM_S_EXPORT_LOCAUX(?,?)}");
            callableStatement.setString(1,this.pdb);
            callableStatement.registerOutParameter(2,OracleTypes.CURSOR);
            callableStatement.executeUpdate();
            // get cursor and cast it to ResultSet
            rs = (ResultSet) callableStatement.getObject(2);
            // loop it like normal
            dbCnx = ContextStore.get().getDbConnection();
            int line =0;
            String values="";
            while (rs.next()) {
                String[] args = rs.getString(1).split("#"); 
                insertSQL+= "INSERT INTO immosis_in_tcp_locaux(SESSION_ID,LINE_NR,LOC_CODE,LOC_NUM_ORDRE";
                values= this.sessionId+","+line+",'"+args[0].replace("'", "''")+"','"+args[1].replace("'", "''")+"'";
                if(args[2].length()>0){
                    insertSQL+=",LOC_SURFACE_BASE";
                    values+=","+Integer.parseInt(args[2]);
                }
                if(args[3].length()>0){
                    insertSQL+=",LOC_SURFACE_FISCALE";
                    values+=","+Integer.parseInt(args[3]);
                }
                insertSQL+=",USA_CODE,SOD_CODE,UCO_CODE_OCCUP,UCO_CODE_PROPRIO";
                values+= ",'"+args[4].replace("'", "''")+"','"+args[5].replace("'", "''")+"','"+args[6].replace("'", "''")+"','"+args[7].replace("'", "''")+"'";
                if(args[8].length()>0){
                    insertSQL+=",LOC_EFFECTIF";
                    values+=","+Integer.parseInt(args[8]);
                }
                if(args[9].length()>0){
                    insertSQL+=",UDJ_CLE";
                    values+=","+Integer.parseInt(args[9]);
                }
                insertSQL+=",TLO_CODE";
                values+=",'"+args[10].replace("'", "''")+"'";

                if(args[11].length()>0){
                    insertSQL+=",LOC_DATE_MES";
                    values+=",to_date('"+args[11].replace(".0","")+"','DD/MM/YYYY')";
                }
                insertSQL+=",LOC_LIB";        
                values+=",'"+args[12].replace("'", "''")+"'";
                if(args[13].length()>0){
                    insertSQL+=",EMP_CLE";
                    values+=","+Integer.parseInt(args[13]);
                }
                if(args[14].length()>0){
                    insertSQL+=",ZONE_GARE";
                    values+=","+Integer.parseInt(args[14]);
                }
                if(args.length > 15){
                    insertSQL+= ",COMMENTAIRES) VALUES("+values+",'"+args[15].replace("'", "''")+"');";
                }else{
                    insertSQL+=") VALUES("+values+");";
                }
                line++;
            }
            insertSQL+=" COMMIT; END;";
            dbCnx = ContextStore.get().getDbConnection();
            dbCnx.executeUpdate(insertSQL);
            logDebug("INFO","Execution de la requète pim_s_export_locaux reussite");
            retour = true;
        } catch (Exception e) {
            e.printStackTrace();
            logDebug("ERROR","Erreur lors du pim_s_export_locaux:"+e.getMessage());
            retour = false;
        } finally {

            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
            return retour;
        }
    }
    @SuppressWarnings("finally")
    private boolean pim_s_export_udj() throws Exception{
        logDebug("INFO","Execution de pim_s_export_udj");
        boolean retour =false;
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        ResultSet rs;
        String insertSQL ="BEGIN ";
        DbConnection.ThreadSafe dbCnx;
        ResultSet internalSet;
        try {
            dbConnection = this.getDBConnection();
            callableStatement = dbConnection.prepareCall("{call AIM_PDB_PRENDRE.PIM_S_EXPORT_UDJ(?,?)}");
            callableStatement.setString(1,this.pdb);
            callableStatement.registerOutParameter(2,OracleTypes.CURSOR);
            callableStatement.executeUpdate();
            // get cursor and cast it to ResultSet
            rs = (ResultSet) callableStatement.getObject(2);
            // loop it like normal
            dbCnx = ContextStore.get().getDbConnection();
            int line =0;
            while (rs.next()) {

                String[] args = rs.getString(1).split("#");
                insertSQL+= "INSERT INTO immosis_in_tcp_udj(SESSION_ID,LINE_NR,UDJ_CODE,UDJ_LIB,COM_CODE_INSEE,UDJ_CLE,UDJ_PRINCIPALE) VALUES(";
                insertSQL+= this.sessionId+","+line+",'"+args[0]+"','"+args[1]+"','"+args[2]+"',"+((args[3].length() >0)? Integer.parseInt(args[3]):"0")+","+((args[4].length() >0)? Integer.parseInt(args[4]):"0")+");";

                line++;
            }
            insertSQL+=" COMMIT; END;";
            dbCnx = ContextStore.get().getDbConnection();
            dbCnx.executeUpdate(insertSQL);
            logDebug("INFO","Execution de la requète pim_s_export_udj reusite");
            retour = true;
        } catch (Exception e) {
            e.printStackTrace();
            logDebug("ERROR","Erreur lors du pim_s_export_udj:"+e.getMessage());
            retour = false;
        } finally {

            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
            return retour;
        }
    } @SuppressWarnings("finally")
    private boolean pim_s_export_bal() throws Exception{
        logDebug("INFO","Execution de pim_s_export_bal");
        boolean retour =false;
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        ResultSet rs;
        String insertSQL ="BEGIN ";
        DbConnection.ThreadSafe dbCnx;
        ResultSet internalSet;
        try {
            dbConnection = this.getDBConnection();
            callableStatement = dbConnection.prepareCall("{call AIM_PDB_PRENDRE.PIM_S_EXPORT_BAL(?,?)}");
            callableStatement.setString(1,this.pdb);
            callableStatement.registerOutParameter(2,OracleTypes.CURSOR);
            callableStatement.executeUpdate();
            // get cursor and cast it to ResultSet
            rs = (ResultSet) callableStatement.getObject(2);
            // loop it like normal
            dbCnx = ContextStore.get().getDbConnection();
            int line =0;
            while (rs.next()) {
                    String[] args = rs.getString(1).split("#");
                    insertSQL+= "INSERT INTO immosis_in_tcp_batiment_l(SESSION_ID,LINE_NR,BAL_CLE,UDJ_CLE,BAP_CODE,UTO_CODE) VALUES(";
                    insertSQL+= this.sessionId+","+line+","+((args[0].length() >0)? Integer.parseInt(args[0]):"0")+","+((args[1].length() >0)? Integer.parseInt(args[1]):"0")+",'"+args[2]+"','"+args[3]+"');";
                    line++;
            }
            insertSQL+=" COMMIT; END;";
            logDebug("INFO","Execution de la requète pim_s_export_bal :"+insertSQL);
            dbCnx = ContextStore.get().getDbConnection();
            dbCnx.executeUpdate(insertSQL);
            retour = true;
        } catch (Exception e) {
            e.printStackTrace();
            logDebug("ERROR","Erreur lors du pim_s_export_bal:"+e.getMessage());
            retour = false;
        } finally {

            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
            return retour;
        }
    }
    
 
    /* Checkin methods --------------------------------------------------------- */
    
    /*
     * Check if plan is referenced
     * @param plan : name of plan ex: 000484m_0001_00.dwg
     * @return boolean true if exist false if not exist
     */
    private boolean planIsReferenced(String plan) throws Exception{
            boolean retour =false;
            Connection dbConnection = null;
            CallableStatement callableStatement = null;
            try {
                dbConnection = this.getDBConnection();
                Statement statement = dbConnection.createStatement();
                ResultSet internalSet = statement.executeQuery("SELECT COUNT(*) FROM edm_document WHERE EDMDOC_FILE_NAME = '"+plan.toLowerCase()+"'");
                while(internalSet.next()){
                    retour = (internalSet.getInt(1) > 0);
                }
                logDebug("INFO","Le plan "+this.pdb+" est reference ? :"+retour);
                return retour;
            } catch (Exception e) {
                logDebug("ERROR",e.getMessage());
            } finally {
                if (callableStatement != null) {
                    callableStatement.close();
                }
                if (dbConnection != null) {
                    dbConnection.close();
                }
            }
            return retour;
        }
    

    /**
     * Select elem from table immosis_out_pdb_etiquettes (from archibus) and transfert the data to tim_pdb_etiq_br (on immosis database) 
     * @param pdb contain de name of plan 
     * @throws Exception 
     * 
     */
    private boolean immosis_out_pdb_etiquettes(String pdb) throws Exception {
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        String req =    "SELECT UTO_CODE,BAP_CODE,TEG_CODE,LOC_CODE,LOC_NUM_ORDRE,SURF_DEV,UCO_CODE_OCCUP,SOD_CODE,USA_CODE,LOC_EFFECTIF FROM immosis_out_pdb_etiquettes  WHERE uto_code = SUBSTR('"+pdb+"', 1, 7) "+
                        "AND bap_code = SUBSTR('"+pdb+"', 9, 3) "+
                        "AND teg_code = SUBSTR('"+pdb+"', 13, 2) ";
        
        String insert="BEGIN ";
        try {

            DbConnection.ThreadSafe dbCnx = ContextStore.get().getDbConnection();
            ResultSet internalSet = dbCnx.execute(req , 0);
            int nb = ThreadLocalRandom.current().nextInt(0, 111111);
            dbConnection = this.getDBConnection();
            String sql="";
            String values ="";
            String field="";
            while(internalSet.next()){
                nb = nb + 1;
                sql=" INSERT INTO tim_pdb_etiq_br (ETIQ_PK,";
                values= nb+",";
                if(internalSet.getString(1) != null){
                    sql+="UTO_CODE,";
                    values+= "'"+internalSet.getString(1).replace("'","''")+"',";
                }
                if(internalSet.getString(2) != null){
                    sql+="BAP_CODE,";
                    values+= "'"+internalSet.getString(2).replace("'","''")+"',";
                }
                if(internalSet.getString(3) != null){
                    sql+="TEG_CODE,";
                    values+= "'"+internalSet.getString(3).replace("'","''")+"',";
                }
                if(internalSet.getString(4) != null){
                    sql+="LOC_CODE,";
                    values+= "'"+internalSet.getString(4).replace("'","''")+"',";
                }
                if(internalSet.getString(5) != null){
                    sql+="NUM_ORDRE,";
                    values+= "'"+internalSet.getString(5).replace("'","''")+"',";
                }
                sql+="SURF_DEV,";
                values+= internalSet.getInt(6)+",";

                if(internalSet.getString(7) != null){
                    sql+="RG_OCC,";
                    values+= "'"+internalSet.getString(7).replace("'","''")+"',";
                }
                if(internalSet.getString(8)!= null){
                    sql+="SOD_CODE,";
                    values+= "'"+internalSet.getString(8).replace("'","''")+"',";
                }
                if(internalSet.getString(9) != null){
                    sql+="DESBIEN_CODE,";
                    values+= "'"+internalSet.getString(9).replace("'","''")+"',";
                }
                sql+="EFFECTIF)";
                values+= internalSet.getInt(10);

                insert+=sql+" VALUES("+values+");";
            }
            insert+=" COMMIT; END;";
            logDebug("INFO","insert :"+insert);
            Statement statement = dbConnection.createStatement();
            internalSet = statement.executeQuery(insert);
            logDebug("INFO","Execution de la requète tim_pdb_etiq_br reussite");
            return true;
        } catch (Exception e) {
            logDebug("ERROR",e.getMessage());
            return false;
        } finally {
            
            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
    }
    /**
     * Select elem from table immosis_out_pdb_etiquettes (from archibus) and transfert the data to tim_pdb_etiq_br (on immosis database) 
     * @param pdb contain de name of plan 
     * @throws Exception 
     * 
     */
    private boolean immosis_out_pdb_maj(String pdb) throws Exception {
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        String req =    "SELECT UTO_CODE,BAP_CODE,TEG_CODE,LOC_ID,ETAT_ORDRE FROM immosis_out_pdb_maj  WHERE uto_code = SUBSTR('"+pdb+"', 1, 7) "+
                        "AND bap_code = SUBSTR('"+pdb+"', 9, 3) "+
                        "AND teg_code = SUBSTR('"+pdb+"', 13, 2) ";
        
        String insert="BEGIN ";
        try {

            DbConnection.ThreadSafe dbCnx = ContextStore.get().getDbConnection();
            ResultSet internalSet = dbCnx.execute(req , 0);

            int nb = ThreadLocalRandom.current().nextInt(0, 111111);
            dbConnection = this.getDBConnection();
            String sql="";
            String values ="";
            String field="";
            while(internalSet.next()){
                nb = nb +1;
                sql="INSERT INTO TIM_PDB_ETATS_BR (ETATS_PK,";
                values= nb+",";

                if(internalSet.getString(1) != null){
                    sql+="UTO_CODE,";
                    values+= "'"+internalSet.getString(1).replace("'","''")+"',";
                }
                if(internalSet.getString(2)!= null){
                    sql+="BAP_CODE,";
                    values+= "'"+internalSet.getString(2).replace("'","''")+"',";
                }
                if(internalSet.getString(3) != null){
                    sql+="TEG_CODE,";
                    values+= "'"+internalSet.getString(3).replace("'","''")+"',";
                }
                sql+="LOC_ID,";
                values+= internalSet.getInt(4)+",";
                sql+="ETAT_ORDRE)";
                values+= internalSet.getInt(5);
                insert+=sql+" VALUES("+values+");";
            }
            insert+=" COMMIT; END;";
            logDebug("INFO","insert :"+insert);
            Statement statement = dbConnection.createStatement();
            internalSet = statement.executeQuery(insert);
            logDebug("INFO","Execution de la requète TIM_PDB_ETATS_BR reussite");
            return true;
        } catch (Exception e) {
            logDebug("ERROR",e.getMessage());
            return false;
        } finally {
            
            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
    }

    private boolean immosis_out_tcp_locaux(String pdb) throws Exception {
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        String req =    "SELECT UTO_CODE,BAP_CODE,TEG_CODE,LOC_CODE,LOC_NUM_ORDRE,SURF_DEV,DESBIEN_CODE,SOD_CODE,LOC_EFFECTIF,UDJ_CLE,USA_CODE,LOC_DATE_MES,LOC_LIB,EMP_CLE,ZONE_GARE,COMMENTAIRES,TYPE_ACTION,LOC_ID "+
                        "FROM immosis_out_tcp_locaux  WHERE uto_code = SUBSTR('"+pdb+"', 1, 7) "+
                        "AND bap_code = SUBSTR('"+pdb+"', 9, 3) "+
                        "AND teg_code = SUBSTR('"+pdb+"', 13, 2) AND session_id ="+this.sessionId; // ajout de la condition session id le 05/02/2019
        String recupererSql = " SELECT cell.cell_pk FROM cellules cell "+
                                "INNER JOIN cellules cellbat ON cellbat.cell_pk = cell.cell_maitre "+
                                "INNER JOIN cellules cellut ON cellut.cell_pk = cellbat.cell_maitre "+
                                "INNER JOIN tim_type_etage pet ON pet.peg_code_etage_immosis = cell.cell_code "+
                                "WHERE cellut.cell_code =  '"+this.blId.substring(0,7)+"' "+
                                "AND cellbat.cell_code = 'B ' || '"+this.pdb.substring(8,11)+ "' "+
                                "AND pet.peg_code_etage_copi = '"+this.flId+"'";
        int teg_pk=0;

        int nb = ThreadLocalRandom.current().nextInt(0, 111111);
        String insert="BEGIN ";
        try {
            // Récupération du teg_pk
            dbConnection = this.getDBConnection();
            Statement stmt = dbConnection.createStatement();
            ResultSet rs = stmt.executeQuery(recupererSql);
            while(rs.next()){
                teg_pk = rs.getInt(1);
            }
            
            //Reciperation des données rm-temp;
            DbConnection.ThreadSafe dbCnx = ContextStore.get().getDbConnection();
            ResultSet internalSet = dbCnx.execute(req , 0);   
            
            //Création de la req d'insertion dans tim_pdb-locaux
            Statement statement = dbConnection.createStatement();
            String sql="";
            String values ="";
            while(internalSet.next()){
                nb = nb + 1;
                sql="INSERT INTO TIM_PDB_LOCAUX_BR (LOCAUX_PK,TEG_PK,";
                
                values= nb+",";
                values+= teg_pk+",";
                if(internalSet.getString(1) != null){
                    sql+="UTO_CODE,";
                    values+= "'"+internalSet.getString(1).replace("'","''")+"',";
                }
                if(internalSet.getString(2)!= null){
                    sql+="BAP_CODE,";
                    values+= "'"+internalSet.getString(2).replace("'","''")+"',";
                }
                if(internalSet.getString(3) != null){
                    sql+="TEG_CODE,";
                    values+= "'"+internalSet.getString(3).replace("'","''")+"',";
                }
                if(internalSet.getString(4) != null){
                    sql+="LOC_CODE,";
                    values+= "'"+internalSet.getString(4).replace("'","''")+"',";
                }
                if(internalSet.getString(5)!= null){
                    sql+="NUM_ORDRE,";
                    values+= "'"+internalSet.getString(5).replace("'","''")+"',";
                }
                sql+="SURF_DEV,";
                values+= internalSet.getInt(6)+",";

                if(internalSet.getString(7) != null){
                    sql+="DESBIEN_CODE,";
                    values+= "'"+internalSet.getString(7)+"',";
                }
                if(internalSet.getString(8)!= null){
                    sql+="SOD_CODE,";
                    values+= "'"+internalSet.getString(8)+"',";
                }
                sql+="EFFECTIF,UDJ_CLE,";
                values+= internalSet.getInt(9)+",";
                values+= internalSet.getInt(10)+",";
                if(internalSet.getString(11) != null){
                    sql+="USA_CODE,";
                    values+= "'"+internalSet.getString(11).replace("'","''")+"',";
                }  

                if(internalSet.getString(12)!= null){
                    sql+="DATE_MEX,";
                    values+= "to_date('"+internalSet.getString(12).replace(".0","")+"','YYYY-MM-DD HH24:MI:SS'),"; 
                }

                if(internalSet.getString(13)!= null){
                    sql+="LOC_LIB,";
                    values+= "'"+internalSet.getString(13).replace("'","''")+"',";
                }

                if(internalSet.getString(14)!= null){
                    sql+="EMP_CLE,";
                    values+= "'"+internalSet.getString(14).replace("'","''")+"',";
                }
                if(internalSet.getInt(15) > 0){
                    sql+="ZONE_GARE,";
                    values+= internalSet.getInt(15)+",";
                }
                if(internalSet.getString(16) != null){
                    sql+="COMMENTAIRE,";
                    values+= "'"+internalSet.getString(16).replace("'","''")+"',";
                }                
                if(internalSet.getString(17) != null){
                    sql+="LOC_ACTION,";
                    values+= "'"+internalSet.getString(17).replace("'","''")+"',";
                }
                sql+="LOC_ID)";
                values+=internalSet.getInt(18)+"";
                insert+=sql+" VALUES("+values+");";
            }
            insert+=" COMMIT; END;";
            logDebug("INFO","insert :"+insert);
            internalSet = statement.executeQuery(insert);
            logDebug("INFO","Execution de la requète TIM_PDB_LOCAUX_BR reussite");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            logDebug("ERROR",e.getMessage());
            return false;
        } finally {
            
            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
    }

    
    /* End checkin methods ------------------------------------------------------ */
    protected void processAllData(final int sessionId, final String blId, final String flId) {
        final EventHandlerContext eventHandlerContext = this.context.getEventHandlerContext();
        // Do all inserts in the right order
        
        
        //Driss BANA   modification de la table pdb_zonegare par   et le champ zonegare_code par rm_std
        
        this.executeInsert("rmstd", "immosis_in_pdb_zonegare", "rm_std, description", "TO_CHAR(s.zonegare_code), s.zonegare_lib", "TO_CHAR(s.zonegare_code) = d.rm_std", "s.session_id = " + Integer.toString(sessionId) + " and d.rm_std is null");
        //this.executeInsert("pdb_zonegare", "immosis_in_pdb_zonegare", "zonegare_code, zonegare_lib", "s.zonegare_code, s.zonegare_lib", "s.zonegare_code = d.zonegare_code", "s.session_id = " + Integer.toString(sessionId) + " and d.zonegare_code is null");
        if (EventHandlerBase.isOracle(eventHandlerContext)) {
            this.executeInsert("state", "(select '00' state_id, '-' name from dual)", "state_id, name", "s.state_id, s.name", "s.state_id = d.state_id", "d.state_id is null");
        }
        else {
            this.executeInsert("state", "(select '00' state_id, '-' name)", "state_id, name", "s.state_id, s.name", "s.state_id = d.state_id", "d.state_id is null");
        }
        this.executeInsert("city", "immosis_in_tcp_commune", "city_id, name, state_id", "s.com_code_insee, s.com_lib_insee, '00'", "s.com_code_insee = d.city_id", "s.session_id = " + Integer.toString(sessionId) + " and d.city_id is null");
        this.executeInsert("tcp_emploi", "immosis_in_tcp_emploi", "emp_cle, emp_lib", "s.emp_cle, s.emp_lib", "s.emp_cle = d.emp_cle", "s.session_id = " + Integer.toString(sessionId) + " and d.emp_cle is null");
        this.executeInsert("tcp_ligne", "immosis_in_tcp_ligne", "lig_code, lig_lib", "s.lig_code, s.lig_lib", "s.lig_code = d.lig_code", "s.session_id = " + Integer.toString(sessionId) + " and d.lig_code is null");
        this.executeInsert("tcp_sous_domaine", "immosis_in_tcp_sous_domaine", "sod_code, sod_lib, dom_code", "s.sod_code, s.sod_lib, s.dom_code", "s.sod_code = d.sod_code", "s.session_id = " + Integer.toString(sessionId) + " and d.sod_code is null");
        this.executeInsert("tcp_type_batiment", "immosis_in_tcp_type_batiment", "tba_code, tba_lib", "s.tba_code, s.tba_lib", "s.tba_code = d.tba_code", "s.session_id = " + Integer.toString(sessionId) + " and d.tba_code is null");
        /* this.executeInsert("tcp_usage", "immosis_in_tcp_usage", "usa_code, usa_lib, dst_code", "s.usa_code, s.usa_lib, s.dst_code", "s.usa_code = d.usa_code", "s.session_id = " + Integer.toString(sessionId) + " and d.usa_code is null"); */
        /* this.executeInsert("tcp_   type_local", "immosis_in_tcp_type_local", "tlo_code, tlo_lib, usa_code", "s.tlo_code, s.tlo_lib, s.usa_code", "s.tlo_code = d.tlo_code", "s.session_id = " + Integer.toString(sessionId) + " and d.tlo_code is null"); */
        this.executeInsert("tcp_plaque", "immosis_in_tcp_plaque", "pla_code, pla_lib", "s.pla_code, s.pla_lib", "s.pla_code = d.pla_code", "s.session_id = " + Integer.toString(sessionId) + " and d.pla_code is null");
        this.executeInsert("tcp_udj", "immosis_in_tcp_udj", "udj_code, udj_lib, state_id, city_id, udj_cle, udj_principale", "s.udj_code, s.udj_lib, '00', s.com_code_insee, s.udj_cle, case when s.udj_principale is null then -1 else s.udj_principale end", "s.udj_cle = d.udj_cle", "s.session_id = " + Integer.toString(sessionId) + " and d.udj_cle is null");
        this.executeInsert("tcp_region_sncf", "immosis_in_tcp_region_sncf", "reg_code, reg_lib, reg_adresse, pla_code", "s.reg_code, s.reg_lib, s.reg_adresse, s.pla_code", "s.reg_code = d.reg_code", "s.session_id = " + Integer.toString(sessionId) + " and d.reg_code is null");
        this.executeInsert("site", "immosis_in_tcp_ut", "site_id, reg_code, name", "s.uto_code, s.reg_code, s.uto_lib", "s.uto_code = d.site_id", "s.session_id = " + Integer.toString(sessionId) + " and d.site_id is null");
        if (EventHandlerBase.isOracle(eventHandlerContext)) {
            this.executeInsert("bl", "immosis_in_tcp_batiment_p", "site_id, bl_id, bap_code, lig_code, cop_pk, cop_date_mes, name, tba_code, emp_cle, bap_plan_integre, bap_desc_terminee", "s.uto_code, s.uto_code || s.bap_code, s.bap_code, s.lig_code, s.cop_pk, s.cop_date_mes, s.cop_lib, s.tba_code, s.emp_cle, nvl(s.bap_plan_integre, -1), nvl(s.bap_desc_terminee, -1)", "s.uto_code || s.bap_code = d.bl_id", "s.session_id = " + Integer.toString(sessionId) + " and d.bl_id is null");
            this.executeInsert("tcp_batiment_l", "immosis_in_tcp_batiment_l", "bal_cle, udj_cle, site_id, bl_id, bap_code", "s.bal_cle, s.udj_cle, s.uto_code, s.uto_code || s.bap_code, s.bap_code", "s.bal_cle = d.bal_cle", "s.session_id = " + Integer.toString(sessionId) + " and d.bal_cle is null");
        }
        else {
            this.executeInsert("bl", "immosis_in_tcp_batiment_p", "site_id, bl_id, bap_code, lig_code, cop_pk, cop_date_mes, name, tba_code, emp_cle, bap_plan_integre, bap_desc_terminee", "s.uto_code, s.uto_code + s.bap_code, s.bap_code, s.lig_code, s.cop_pk, s.cop_date_mes, s.cop_lib, s.tba_code, s.emp_cle, isnull(s.bap_plan_integre, -1), isnull(s.bap_desc_terminee, -1)", "s.uto_code + s.bap_code = d.bl_id", "s.session_id = " + Integer.toString(sessionId) + " and d.bl_id is null");
            this.executeInsert("tcp_batiment_l", "immosis_in_tcp_batiment_l", "bal_cle, udj_cle, site_id, bl_id, bap_code", "s.bal_cle, s.udj_cle, s.uto_code, s.uto_code + s.bap_code, s.bap_code", "s.bal_cle = d.bal_cle", "s.session_id = " + Integer.toString(sessionId) + " and d.bal_cle is null");
        }
        this.executeInsert("fl", "immosis_in_tcp_type_etage", "bl_id, fl_id, name", "'" + blId + "', s.teg_code, s.teg_lib", "'" + blId + "' = d.bl_id and s.teg_code = d.fl_id", "s.session_id = " + Integer.toString(sessionId) + " and d.bl_id is null");
        /*
        // Code ajouté le 06/02
        String update ="BEGIN "+
                            "FOR rmloop IN(select loc_code || '-' || loc_num_ordre as rm_id ,loc_code, loc_num_ordre, loc_surface_base, loc_surface_fiscale, usa_code, "+
                            "                     sod_code, nvl(loc_effectif, 0) as count_em, udj_cle, tlo_code, loc_date_mes, loc_lib, emp_cle, zone_gare, "+
                            "                     commentaires "+
                            "                     from immosis_in_tcp_locaux where session_id="+this.sessionId+"') "+
                            "LOOP "+
                            "  update rm_temp set loc_code = rmloop.loc_code,loc_num_ordre=rmloop.loc_num_ordre, loc_surface_base=rmloop.loc_surface_base, area=rmloop.loc_surface_fiscale, rm_cat=rmloop.usa_code, "+
                            "        sod_code=rmloop.sod_code, count_em=rmloop.count_em, udj_cle=rmloop.udj_cle, rm_type=rmloop.tlo_code, loc_date_mes=rmloop.loc_date_mes, name=rmloop.loc_lib, emp_cle=rmloop.emp_cle, rm_std=rmloop.zone_gare, comments=rmloop.commentaires "+
                            "        where bl_id='"+this.blId+"' AND fl_id='"+this.flId+"' and rm_id = rmloop.rm_id;"+
                            "END LOOP;"+
                            "COMMIT;"+
                        "END; ";
        try{
            DbConnection.ThreadSafe dbCnx = ContextStore.get().getDbConnection();
            ResultSet internalSet = dbCnx.execute(update , 0);
        }catch(Exception e){
            logDebug("ERROR","Une erreure est survenue lors de la mise à jours des rm_temp :"+e.getMessage());
            e.printStackTrace();
        }
   */
        if (EventHandlerBase.isOracle(eventHandlerContext)) {
            //  Driss BANA import de la table immosis_in_tcp_locaux vers la table rm avec les nouveaux champs.
            this.executeInsert("rm_temp", "immosis_in_tcp_locaux", "bl_id, fl_id, rm_id, loc_code, loc_num_ordre, loc_surface_base, area, rm_cat, sod_code, count_em, udj_cle, rm_type, loc_date_mes, name, emp_cle, rm_std, comments", "'" + blId + "', '" + flId + "', s.loc_code || '-' || s.loc_num_ordre, s.loc_code, s.loc_num_ordre, s.loc_surface_base, s.loc_surface_fiscale, s.usa_code, s.sod_code, nvl(s.loc_effectif, 0), s.udj_cle, s.tlo_code, s.loc_date_mes, s.loc_lib, s.emp_cle, s.zone_gare, s.commentaires", "'" + blId + "' = d.bl_id and '" + flId + "' = d.fl_id and s.loc_code || '-' || s.loc_num_ordre = d.rm_id", "s.session_id = " + Integer.toString(sessionId) + " and d.bl_id is null");
            //   this.executeInsert("rm_temp", "immosis_in_tcp_locaux", "bl_id, fl_id, rm_id, loc_code, loc_num_ordre, loc_surface_base, area, usa_code, sod_code, count_em, udj_cle, tlo_code, loc_date_mes, name, emp_cle, zonegare_code, comments", "'" + blId + "', '" + flId + "', s.loc_code || '-' || s.loc_num_ordre, s.loc_code, s.loc_num_ordre, s.loc_surface_base, s.loc_surface_fiscale, s.usa_code, s.sod_code, nvl(s.loc_effectif, 0), s.udj_cle, s.tlo_code, s.loc_date_mes, s.loc_lib, s.emp_cle, s.zone_gare, s.commentaires", "'" + blId + "' = d.bl_id and '" + flId + "' = d.fl_id and s.loc_code || '-' || s.loc_num_ordre = d.rm_id", "s.session_id = " + Integer.toString(sessionId) + " and d.bl_id is null");
        }
        else {
           //  Driss BANA import de la table immosis_in_tcp_locaux vers la table rm avec les nouveaux champs.            
            this.executeInsert("rm_temp", "immosis_in_tcp_locaux", "bl_id, fl_id, rm_id, loc_code, loc_num_ordre, loc_surface_base, area, rm_cat, sod_code, count_em, udj_cle, rm_type, loc_date_mes, name, emp_cle, rm_std, comments", "'" + blId + "', '" + flId + "', s.loc_code || '-' || s.loc_num_ordre, s.loc_code, s.loc_num_ordre, s.loc_surface_base, s.loc_surface_fiscale, s.usa_code, s.sod_code, nvl(s.loc_effectif, 0), s.udj_cle, s.tlo_code, s.loc_date_mes, s.loc_lib, s.emp_cle, s.zone_gare, s.commentaires", "'" + blId + "' = d.bl_id and '" + flId + "' = d.fl_id and s.loc_code || '-' || s.loc_num_ordre = d.rm_id", "s.session_id = " + Integer.toString(sessionId) + " and d.bl_id is null");
            //this.executeInsert("rm_temp", "immosis_in_tcp_locaux", "bl_id, fl_id, rm_id, loc_code, loc_num_ordre, loc_surface_base, area, usa_code, sod_code, count_em, udj_cle, tlo_code, loc_date_mes, name, emp_cle, zonegare_code, comments", "'" + blId + "', '" + flId + "', s.loc_code + '-' + s.loc_num_ordre, s.loc_code, s.loc_num_ordre, s.loc_surface_base, s.loc_surface_fiscale, s.usa_code, s.sod_code, isnull(s.loc_effectif, 0), s.udj_cle, s.tlo_code, s.loc_date_mes, s.loc_lib, s.emp_cle, s.zone_gare, s.commentaires", "'" + blId + "' = d.bl_id and '" + flId + "' = d.fl_id and s.loc_code + '-' + s.loc_num_ordre = d.rm_id", "s.session_id = " + Integer.toString(sessionId) + " and d.bl_id is null");
        }
        
        //Driss BANA   modification de la table pdb_zonegare par rmstd  
        this.executeUpdate("rmstd", "immosis_in_pdb_zonegare", "TO_CHAR(s.zonegare_code) as zonegare_code, s.zonegare_lib", "TO_CHAR(s.zonegare_code) = d.rm_std", "s.session_id = " + Integer.toString(sessionId),"zonegare_lib", "description");
        // Do all updates in the right order
        //this.executeUpdate("pdb_zonegare", "immosis_in_pdb_zonegare", "s.zonegare_code, s.zonegare_lib", "s.zonegare_code = d.zonegare_code", "s.session_id = " + Integer.toString(sessionId), "zonegare_lib");
        this.executeUpdate("city", "immosis_in_tcp_commune", "s.com_code_insee, s.com_lib_insee name", "s.com_code_insee = d.city_id", "s.session_id = " + Integer.toString(sessionId), "name");
        this.executeUpdate("tcp_emploi", "immosis_in_tcp_emploi", "s.emp_cle, s.emp_lib", "s.emp_cle = d.emp_cle", "s.session_id = " + Integer.toString(sessionId), "emp_lib");
        this.executeUpdate("tcp_ligne", "immosis_in_tcp_ligne", "s.lig_code, s.lig_lib", "s.lig_code = d.lig_code", "s.session_id = " + Integer.toString(sessionId), "lig_lib");
        this.executeUpdate("tcp_sous_domaine", "immosis_in_tcp_sous_domaine", "s.sod_code, s.sod_lib, s.dom_code", "s.sod_code = d.sod_code", "s.session_id = " + Integer.toString(sessionId), "sod_lib, dom_code");
        this.executeUpdate("tcp_type_batiment", "immosis_in_tcp_type_batiment", "s.tba_code, s.tba_lib", "s.tba_code = d.tba_code", "s.session_id = " + Integer.toString(sessionId), "tba_lib");
        /* this.executeUpdate("tcp_usage", "immosis_in_tcp_usage", "s.usa_code, s.usa_lib, s.dst_code", "s.usa_code = d.usa_code", "s.session_id = " + Integer.toString(sessionId), "usa_lib, dst_code"); */
        /* this.executeUpdate("tcp_type_local", "immosis_in_tcp_type_local", "s.tlo_code, s.tlo_lib, s.usa_code", "s.tlo_code = d.tlo_code", "s.session_id = " + Integer.toString(sessionId), "tlo_lib, usa_code"); */
        this.executeUpdate("tcp_plaque", "immosis_in_tcp_plaque", "s.pla_code, s.pla_lib", "s.pla_code = d.pla_code", "s.session_id = " + Integer.toString(sessionId), "pla_lib");
        this.executeUpdate("tcp_udj", "immosis_in_tcp_udj", "s.udj_cle, s.udj_code, s.udj_lib, s.com_code_insee city_id, case when s.udj_principale is null then -1 else s.udj_principale end udj_principale", "s.udj_cle = d.udj_cle", "s.session_id = " + Integer.toString(sessionId), "udj_code, udj_lib, city_id, udj_principale");
        this.executeUpdate("tcp_region_sncf", "immosis_in_tcp_region_sncf", "s.reg_code, s.reg_lib, s.reg_adresse, s.pla_code", "s.reg_code = d.reg_code", "s.session_id = " + Integer.toString(sessionId), "reg_lib, reg_adresse, pla_code");
        this.executeUpdate("site", "immosis_in_tcp_ut", "s.uto_code, s.reg_code, s.uto_lib name", "s.uto_code = d.site_id", "s.session_id = " + Integer.toString(sessionId), "reg_code, name");
        if (EventHandlerBase.isOracle(eventHandlerContext)) {
            this.executeUpdate("bl", "immosis_in_tcp_batiment_p", "s.uto_code, s.bap_code, s.uto_code site_id, s.lig_code, s.cop_pk, s.cop_date_mes, s.cop_lib name, s.tba_code, s.emp_cle, nvl(s.bap_plan_integre, -1) bap_plan_integre, nvl(s.bap_desc_terminee, -1) bap_desc_terminee", "s.uto_code || s.bap_code = d.bl_id", "s.session_id = " + Integer.toString(sessionId), "site_id, bap_code, lig_code, cop_pk, cop_date_mes, name, tba_code, emp_cle, bap_plan_integre, bap_desc_terminee");
            this.executeUpdate("tcp_batiment_l", "immosis_in_tcp_batiment_l", "s.bal_cle, s.udj_cle, s.uto_code site_id, s.uto_code || s.bap_code bl_id, s.bap_code", "s.bal_cle = d.bal_cle", "s.session_id = " + Integer.toString(sessionId), "udj_cle, site_id, bl_id, bap_code");
        }
        else {
            this.executeUpdate("bl", "immosis_in_tcp_batiment_p", "s.uto_code, s.bap_code, s.uto_code site_id, s.lig_code, s.cop_pk, s.cop_date_mes, s.cop_lib name, s.tba_code, s.emp_cle, isnull(s.bap_plan_integre, -1) bap_plan_integre, isnull(s.bap_desc_terminee, -1) bap_desc_terminee", "s.uto_code + s.bap_code = d.bl_id", "s.session_id = " + Integer.toString(sessionId), "site_id, bap_code, lig_code, cop_pk, cop_date_mes, name, tba_code, emp_cle, bap_plan_integre, bap_desc_terminee");
            this.executeUpdate("tcp_batiment_l", "immosis_in_tcp_batiment_l", "s.bal_cle, s.udj_cle, s.uto_code site_id, s.uto_code + s.bap_code bl_id, s.bap_code", "s.bal_cle = d.bal_cle", "s.session_id = " + Integer.toString(sessionId), "udj_cle, site_id, bl_id, bap_code");
        }
        this.executeUpdate("fl", "immosis_in_tcp_type_etage", "d.bl_id, s.teg_code, s.teg_lib name", "'" + blId + "' = d.bl_id and s.teg_code = d.fl_id", "s.session_id = " + Integer.toString(sessionId), "name");
        if (EventHandlerBase.isOracle(eventHandlerContext)) {
            
           
            //  Driss BANA mettre à jour  la table rm avec les nouveaux champs.
           this.executeUpdate("rm_temp", "immosis_in_tcp_locaux", "s.loc_code, s.loc_num_ordre, s.loc_surface_base, s.loc_surface_fiscale area, s.usa_code, s.sod_code, nvl(s.loc_effectif, 0) count_em, s.udj_cle, s.tlo_code, s.loc_date_mes, s.loc_lib name, s.emp_cle, s.zone_gare zonegare_code, s.commentaires comments", "'" + blId + "' = d.bl_id and '" + flId + "' = d.fl_id and s.loc_code || '-' || s.loc_num_ordre = d.rm_id", "s.session_id = " + Integer.toString(sessionId), "loc_code, loc_num_ordre, loc_surface_base, usa_code, sod_code, count_em, udj_cle, tlo_code, loc_date_mes, name, emp_cle, zonegare_code, comments", "loc_code, loc_num_ordre, loc_surface_base, rm_cat, sod_code, count_em, udj_cle, rm_type, loc_date_mes, name, emp_cle, rm_std, comments");
           //this.executeUpdate("rm_temp", "immosis_in_tcp_locaux", "s.loc_code, s.loc_num_ordre, s.loc_surface_base, s.loc_surface_fiscale area, s.usa_code, s.sod_code, nvl(s.loc_effectif, 0) count_em, s.udj_cle, s.tlo_code, s.loc_date_mes, s.loc_lib name, s.emp_cle, s.zone_gare zonegare_code, s.commentaires comments", "'" + blId + "' = d.bl_id and '" + flId + "' = d.fl_id and s.loc_code || '-' || s.loc_num_ordre = d.rm_id", "s.session_id = " + Integer.toString(sessionId), "loc_code, loc_num_ordre, loc_surface_base, usa_code, sod_code, count_em, udj_cle, tlo_code, loc_date_mes, name, emp_cle, zonegare_code, comments");
        }
        else {
            
             //  Driss BANA mettre à jour  la table rm avec les nouveaux champs.            
            this.executeUpdate("rm_temp", "immosis_in_tcp_locaux", "s.loc_code, s.loc_num_ordre, s.loc_surface_base, s.loc_surface_fiscale area, s.usa_code, s.sod_code, isnull(s.loc_effectif, 0) count_em, s.udj_cle, s.tlo_code, s.loc_date_mes, s.loc_lib name, s.emp_cle, s.zone_gare zonegare_code, s.commentaires comments", "'" + blId + "' = d.bl_id and '" + flId + "' = d.fl_id and s.loc_code + '-' + s.loc_num_ordre = d.rm_id", "s.session_id = " + Integer.toString(sessionId), "loc_code, loc_num_ordre, loc_surface_base, usa_code, sod_code, count_em, udj_cle, tlo_code, loc_date_mes, name, emp_cle, zonegare_code, comments", "loc_code, loc_num_ordre, loc_surface_base, rm_cat, sod_code, count_em, udj_cle, rm_type, loc_date_mes, name, emp_cle, rm_std, comments");
            //this.executeUpdate("rm_temp", "immosis_in_tcp_locaux", "s.loc_code, s.loc_num_ordre, s.loc_surface_base, s.loc_surface_fiscale area, s.usa_code, s.sod_code, isnull(s.loc_effectif, 0) count_em, s.udj_cle, s.tlo_code, s.loc_date_mes, s.loc_lib name, s.emp_cle, s.zone_gare zonegare_code, s.commentaires comments", "'" + blId + "' = d.bl_id and '" + flId + "' = d.fl_id and s.loc_code + '-' + s.loc_num_ordre = d.rm_id", "s.session_id = " + Integer.toString(sessionId), "loc_code, loc_num_ordre, loc_surface_base, usa_code, sod_code, count_em, udj_cle, tlo_code, loc_date_mes, name, emp_cle, zonegare_code, comments");
        }
        // Delete rooms for the imported floor if they were not in the import data
        if (EventHandlerBase.isOracle(eventHandlerContext)) {
            this.executeDelete("rm_temp", "immosis_in_tcp_locaux", "rm_id", "s.loc_code || '-' || s.loc_num_ordre rm_id", "'" + blId + "' = d.bl_id and '" + flId + "' = d.fl_id and s.loc_code || '-' || s.loc_num_ordre = d.rm_id", "s.session_id = " + Integer.toString(sessionId), blId, flId);
        }
        else {
            this.executeDelete("rm_temp", "immosis_in_tcp_locaux", "rm_id", "s.loc_code + '-' + s.loc_num_ordre rm_id", "'" + blId + "' = d.bl_id and '" + flId + "' = d.fl_id and s.loc_code + '-' + s.loc_num_ordre = d.rm_id", "s.session_id = " + Integer.toString(sessionId), blId, flId);
        }
        // delete/mark inactive for tables that contain "all data" instead of only "plan specific"
        /* this.executeDelete("tcp_type_local", "immosis_in_tcp_type_local", "tlo_code", "s.tlo_code", "s.tlo_code = d.tlo_code", "s.session_id = " + Integer.toString(sessionId), "", ""); */
        /* this.executeDelete("tcp_usage", "immosis_in_tcp_usage", "usa_code", "s.usa_code", "s.usa_code = d.usa_code", "s.session_id = " + Integer.toString(sessionId), "", ""); */
        this.executeDelete("tcp_emploi", "immosis_in_tcp_emploi", "emp_cle", "s.emp_cle", "s.emp_cle = d.emp_cle", "s.session_id = " + Integer.toString(sessionId), "", "");
        
        
        //Driss BANA   modification de la table pdb_zonegare par rmstd  
        this.executeDelete("rmstd", "immosis_in_pdb_zonegare", "rm_std", "TO_CHAR(s.zonegare_code)", "TO_CHAR(s.zonegare_code) = d.rm_std", "s.session_id = " + Integer.toString(sessionId), "", "");
        //this.executeDelete("pdb_zonegare", "immosis_in_pdb_zonegare", "zonegare_code", "s.zonegare_code", "s.zonegare_code = d.zonegare_code", "s.session_id = " + Integer.toString(sessionId), "", "");
   
    
    }
    protected void executeInsert(final String destinationTable, final String sourceTable, final String destinationFields, final String sourceFields, final String joinCriterium, final String restriction) {
        this.executeInsert(destinationTable, sourceTable, destinationFields, sourceFields, joinCriterium, restriction, false);
    }
    protected void executeInsert(final String destinationTable, final String sourceTable, final String destinationFields, final String sourceFields, final String joinCriterium, final String restriction, final Boolean identityInsert) {
        final EventHandlerContext eventHandlerContext = ContextStore.get().getEventHandlerContext();
        final String sqlBaseInsert = "insert into {0} ({1}) select {2} from {3} s left join {4} d on {5} where {6}";
        final Object[] parameters = new String[] { destinationTable, destinationFields, sourceFields, sourceTable, destinationTable, joinCriterium, restriction };
        String sqlInsert = MessageFormat.format(sqlBaseInsert, parameters);
        if (identityInsert) {
            sqlInsert = "set identity_insert rm on;" + sqlInsert + "set identity_insert rm off;";
        }
        EventHandlerBase.executeDbSql(eventHandlerContext, destinationTable, sqlInsert, false);
    }
    protected void executeUpdate(final String destinationTable, final String sourceTable, final String sourceFields, final String joinCriterium, final String restriction, final String updateFields) {
        final EventHandlerContext eventHandlerContext = ContextStore.get().getEventHandlerContext();
        final String sqlBaseMerge = "merge into {0} d using (select {1} from {2} s inner join {3} d on {4} where {5}) s on ({6}) when matched then update set {7}";
        final String[] uFields = updateFields.split(", ", -1);
        String updateCombos = "";
        for (final String uField : uFields) {
            if (updateCombos.length() > 0) {
                updateCombos += ", ";
            }
            updateCombos += uField + " = s." + uField;
        }
        final Object[] parameters = new String[] { destinationTable, sourceFields, sourceTable, destinationTable, joinCriterium, restriction, joinCriterium, updateCombos };
        final String sqlMerge = MessageFormat.format(sqlBaseMerge, parameters);
        EventHandlerBase.executeDbSql(eventHandlerContext, destinationTable, sqlMerge, false);
    }
    protected void executeUpdate(final String destinationTable, final String sourceTable, final String sourceFields, final String joinCriterium, final String restriction, final String sourceUpdateFields ,final String destUpdateFields) {
        final EventHandlerContext eventHandlerContext = ContextStore.get().getEventHandlerContext();
        final String sqlBaseMerge = "merge into {0} d using (select {1} from {2} s inner join {3} d on {4} where {5}) s on ({6}) when matched then update set {7}";
        final String[] sourceUFields = sourceUpdateFields.split(", ", -1);
        final String[] destUFields = destUpdateFields.split(", ", -1);
        String updateCombos = "";       
        
        int cpt=0;
        
        for (final String uField : sourceUFields) {
            
            if (updateCombos.length() > 0) {
                updateCombos += ", ";
            }
            updateCombos += destUFields[cpt] + " = s." + uField;
            cpt++;
        }
        final Object[] parameters = new String[] { destinationTable, sourceFields, sourceTable, destinationTable, joinCriterium, restriction, joinCriterium, updateCombos };
        final String sqlMerge = MessageFormat.format(sqlBaseMerge, parameters);
        EventHandlerBase.executeDbSql(eventHandlerContext, destinationTable, sqlMerge, false);
    }
    protected void executeDelete(final String destinationTable, final String sourceTable, final String destinationField, final String sourceField, final String joinCriterium, final String restriction, final String blId, final String flId) {
        final EventHandlerContext eventHandlerContext = ContextStore.get().getEventHandlerContext();
        // final String sqlBaseMerge = "merge into {0} d using (select {1} from {2} s inner join {3} d on {4} where {5}) s on ({6}) when not matched by s{7} then delete;";
        final String sqlBaseDelete = "delete from {0} where {1} not in (select {2} from {3} s inner join {4} d on {5} where {6}){7}";
        String locationSpecificCriterium = "";
        if (blId != null && blId.length() > 0) {
            locationSpecificCriterium += " and bl_id = '" + blId + "'";
        }
        if (flId != null && flId.length() > 0) {
            locationSpecificCriterium += " and fl_id = '" + flId + "'";
        }
        final Object[] parameters = new String[] { destinationTable, destinationField, sourceField, sourceTable, destinationTable, joinCriterium, restriction, locationSpecificCriterium };
        final String sqlDelete = MessageFormat.format(sqlBaseDelete, parameters);
        EventHandlerBase.executeDbSql(eventHandlerContext, destinationTable, sqlDelete, false);
    }
    protected void doBackup() {
        String copy = 
                "BEGIN delete from immosis_backup_rm where session_id ='" + Integer.toString(this.sessionId)+"'; "+
                "            insert into immosis_backup_rm ( session_id,area,area_alloc,area_chargable,area_comn,area_comn_nocup,area_comn_ocup,area_comn_rm,area_comn_serv,area_manual,area_unalloc,bl_id,cap_em,comments, "+
                "                        cost,cost_sqft,count_em,dp_id,dv_id,dwgname,ehandle,emp_cle,extension,fl_id,hotelable,layer_name,length,loc_code,loc_date_mes,loc_id,loc_num_ordre,loc_surface_base, "+
                "                        ls_id,name,option1,option2,org_id,phone,prorate,recovery_status,reservable,rm_cat,rm_id,rm_std,rm_type,rm_use,sod_code,tc_level,tlo_code,transfer_status,udj_cle,usa_code,zonegare_code "+
                "                        ) "+
                "    select '" + Integer.toString(this.sessionId)+"',area,area_alloc,area_chargable,area_comn,area_comn_nocup,area_comn_ocup,area_comn_rm,area_comn_serv,area_manual,area_unalloc,bl_id,cap_em,comments, "+
                "            cost,cost_sqft,count_em,dp_id,dv_id,dwgname,ehandle,emp_cle,extension,fl_id,hotelable,layer_name,length,loc_code,loc_date_mes,loc_id,loc_num_ordre,loc_surface_base, "+
                "            ls_id,name,option1,option2,org_id,phone,prorate,recovery_status,reservable,rm_cat,rm_id,rm_std,rm_type,rm_use,sod_code,tc_level,tlo_code,transfer_status,udj_cle,usa_code,zonegare_code "+
                "    from rm_temp  where bl_id = '" + blId + "' and fl_id = '" + flId + "';"+
                " COMMIT;END;";
        try{
            DbConnection.ThreadSafe dbCnx = ContextStore.get().getDbConnection();
            dbCnx.executeUpdate(copy);
        }catch(Exception e){
            logDebug("ERROR","Une erreure est survenue lors du backup :"+e.getMessage());
            e.printStackTrace();
        }
    }
    protected void fillExportTables(final int sessionId) throws Exception {
        final EventHandlerContext eventHandlerContext = this.context.getEventHandlerContext();

        String insertTable = "";
        String insertFields = "";
        String selectFields = "";
        String selectTables = "";
        String whereClause = "";
        String orderClause = "";
        // Delete all data
        EventHandlerBase.executeDbSql(eventHandlerContext, "immosis_out_pdb_etiquettes", "delete from immosis_out_pdb_etiquettes where session_id = " + Integer.toString(sessionId), false);
        EventHandlerBase.executeDbSql(eventHandlerContext, "immosis_out_pdb_maj", "delete from immosis_out_pdb_maj where session_id = " + Integer.toString(sessionId), false);
        EventHandlerBase.executeDbSql(eventHandlerContext, "immosis_out_tcp_locaux", "delete from immosis_out_tcp_locaux where session_id = " + Integer.toString(sessionId), false);
        // Insert immosis_out_pdb_etiquettes
        insertTable = "immosis_out_pdb_etiquettes";
        insertFields = "session_id, uto_code, bap_code, teg_code, loc_code, loc_num_ordre, surf_dev, uco_code_occup, sod_code, usa_code, loc_effectif";
        
        //Driss BANA  modification du champs rm_temp.USA_CODE par rm_temp.rm_cat        
        
        selectFields = Integer.toString(sessionId) + ", bl.site_id, bl.bap_code, rm_temp.fl_id, rm_temp.loc_code, rm_temp.loc_num_ordre, rm_temp.area, '00000', rm_temp.sod_code, rm_temp.rm_cat, cast(round(rm_temp.count_em, 0) as int)";
        //selectFields = Integer.toString(sessionId) + ", bl.site_id, bl.bap_code, rm_temp.fl_id, rm_temp.loc_code, rm_temp.loc_num_ordre, rm_temp.area, '00000', rm_temp.sod_code, rm_temp.usa_code, cast(round(rm_temp.count_em, 0) as int)";
        selectTables = "rm_temp inner join bl on bl.bl_id = rm_temp.bl_id";
        whereClause = "rm_temp.bl_id = '" + blId + "' and rm_temp.fl_id = '" + flId + "' and rm_temp.loc_code is not null and rm_temp.loc_num_ordre is not null";
        orderClause = "rm_temp.loc_code, rm_temp.loc_num_ordre";
        EventHandlerBase.executeDbSql(eventHandlerContext, insertTable, "insert into " + insertTable + " (" + insertFields + ") select " + selectFields + " from " + selectTables + " where " + whereClause + " order by " + orderClause, false);
        // Insert immosis_out_pdb_maj
        insertTable = "immosis_out_pdb_maj";
        insertFields = "session_id, uto_code, bap_code, teg_code, loc_id, type_action, etat_ordre";
        selectFields = Integer.toString(sessionId) + ", nvl(bl.site_id, babl.site_id) site_id, nvl(bl.bap_code, babl.bap_code) bap_code, nvl(rm_temp.fl_id, barm.fl_id) fl_id, nvl(rm_temp.loc_code, barm.loc_code) loc_id, ";
        selectFields += "case ";
        selectFields += "when barm.bl_id is null then 'A' ";
        selectFields += "when rm_temp.bl_id is null then 'S' ";
        selectFields += "when ";
        selectFields += "nvl(bl.site_id    , '?') <> nvl(babl.site_id , '?') ";
        selectFields += "or nvl(bl.bap_code, '?') <> nvl(babl.bap_code, '?') ";
        selectFields += "or nvl(rm_temp.fl_id   , '?') <> nvl(barm.fl_id   , '?') ";
        selectFields += "or nvl(cast(rm_temp.loc_id as varchar2(30)), '?') <> nvl(cast(barm.loc_id as varchar2(30)), '?') ";
        selectFields += "then 'M' else null end type_action, row_number() over(order by nvl(rm_temp.loc_code, barm.loc_code)) etat_ordre";
        selectTables = "rm_temp full join immosis_backup_rm barm on barm.session_id = " + Integer.toString(sessionId) + " and barm.bl_id = rm_temp.bl_id and barm.fl_id = rm_temp.fl_id and barm.rm_id = rm_temp.rm_id left join bl on bl.bl_id = rm_temp.bl_id left join bl babl on babl.bl_id = barm.bl_id";
        whereClause = "nvl(barm.session_id, " + Integer.toString(sessionId) + ") = " + Integer.toString(sessionId) + " and nvl(rm_temp.bl_id, barm.bl_id) = '" + blId + "' and nvl(rm_temp.fl_id, barm.fl_id) = '" + flId + "' and nvl(rm_temp.loc_code, barm.loc_code) is not null and nvl(rm_temp.loc_num_ordre, barm.loc_num_ordre) is not null";
        orderClause = "loc_id";
        EventHandlerBase.executeDbSql(eventHandlerContext, insertTable, "insert into " + insertTable + " (" + insertFields + ") select " + selectFields + " from " + selectTables + " where " + whereClause + " order by " + orderClause, false);
        // Insert immosis_out_tcp_locaux
        insertTable = "immosis_out_tcp_locaux";
        insertFields = "session_id, uto_code, bap_code, teg_code, loc_code, loc_num_ordre, surf_dev, desbien_code, sod_code, loc_effectif, udj_cle, usa_code, loc_date_mes, loc_lib, emp_cle, zone_gare, commentaires, type_action, loc_id";
        
        // Driss BANA   rm_temp.USA_CODE par rm_temp.rm_cat et  rm_temp.TLO_CODE par rm_temp.rm_type et rm_temp.ZONEGARE_CODE par rm_temp.rm_std
        selectFields = Integer.toString(sessionId) + ", nvl(bl.site_id, babl.site_id) uto_code, nvl(bl.bap_code, babl.bap_code) bap_code, nvl(rm_temp.fl_id, barm.fl_id) teg_code, nvl(rm_temp.loc_code, barm.loc_code) loc_code, nvl(rm_temp.loc_num_ordre, barm.loc_num_ordre) loc_num_ordre, nvl(rm_temp.area, barm.area) surf_dev, nvl(rm_temp.rm_cat, barm.rm_cat) desbien_code, nvl(rm_temp.sod_code, barm.sod_code) sod_code, cast(round(nvl(rm_temp.count_em, barm.count_em), 0) as int) loc_effectif, nvl(rm_temp.udj_cle, barm.udj_cle) udj_cle, nvl(rm_temp.rm_type, barm.rm_type) usa_code, nvl(rm_temp.loc_date_mes, barm.loc_date_mes) loc_date_mes, nvl(rm_temp.name, barm.name) loc_lib, nvl(rm_temp.emp_cle, barm.emp_cle) emp_cle, nvl(rm_temp.rm_std, barm.rm_std) zone_gare, nvl(rm_temp.comments, barm.comments) commentaires, ";
        //selectFields = Integer.toString(sessionId) + ", nvl(bl.site_id, babl.site_id) uto_code, nvl(bl.bap_code, babl.bap_code) bap_code, nvl(rm_temp.fl_id, barm.fl_id) teg_code, nvl(rm_temp.loc_code, barm.loc_code) loc_code, nvl(rm_temp.loc_num_ordre, barm.loc_num_ordre) loc_num_ordre, nvl(rm_temp.area, barm.area) surf_dev, nvl(rm_temp.usa_code, barm.usa_code) desbien_code, nvl(rm_temp.sod_code, barm.sod_code) sod_code, cast(round(nvl(rm_temp.count_em, barm.count_em), 0) as int) loc_effectif, nvl(rm_temp.udj_cle, barm.udj_cle) udj_cle, nvl(rm_temp.tlo_code, barm.tlo_code) usa_code, nvl(rm_temp.loc_date_mes, barm.loc_date_mes) loc_date_mes, nvl(rm_temp.name, barm.name) loc_lib, nvl(rm_temp.emp_cle, barm.emp_cle) emp_cle, nvl(rm_temp.zonegare_code, barm.zonegare_code) zone_gare, nvl(rm_temp.comments, barm.comments) commentaires, ";
        selectFields += "case ";
        selectFields += "when barm.bl_id is null then 'A' ";
        selectFields += "when rm_temp.bl_id is null then 'S' ";
        selectFields += "when ";
        selectFields += "nvl(bl.site_id         , '?')  <> nvl(babl.site_id      , '?') ";
        selectFields += "or nvl(bl.bap_code     , '?')  <> nvl(babl.bap_code     , '?') ";
        selectFields += "or nvl(rm_temp.fl_id        , '?')  <> nvl(barm.fl_id        , '?') ";
        selectFields += "or nvl(rm_temp.loc_code     , '?')  <> nvl(barm.loc_code     , '?') ";
        selectFields += "or nvl(rm_temp.loc_num_ordre, '?')  <> nvl(barm.loc_num_ordre, '?') ";
        selectFields += "or nvl(rm_temp.area         , -1.0) <> nvl(barm.area         , -1.0) ";
        
        
        //Driss BANA  modification du champs rm_temp.USA_CODE par rm_temp.rm_cat        
        
        selectFields += "or nvl(rm_temp.rm_cat     , '?')  <> nvl(barm.rm_cat     , '?') ";
        //selectFields += "or nvl(rm_temp.usa_code     , '?')  <> nvl(barm.usa_code     , '?') ";
        selectFields += "or nvl(rm_temp.sod_code     , '?')  <> nvl(barm.sod_code     , '?') ";
        selectFields += "or cast(round(nvl(rm_temp.count_em, -1), 0) as int) <> cast(round(nvl(barm.count_em, -1), 0) as int) ";
        selectFields += "or nvl(cast(rm_temp.udj_cle       as varchar2(30)), '?') <> nvl(cast(barm.udj_cle       as varchar2(30)), '?') ";
        
        // Driss BANA  modification du champs rm_temp.TLO_CODE par rm_temp.rm_type
        selectFields += "or nvl(rm_temp.rm_type     , '?')  <> nvl(barm.rm_type     , '?') ";
        //selectFields += "or nvl(rm_temp.tlo_code     , '?')  <> nvl(barm.tlo_code     , '?') ";
        selectFields += "or nvl(rm_temp.loc_date_mes , SYSDATE)  <> nvl(barm.loc_date_mes , SYSDATE) ";
        selectFields += "or nvl(rm_temp.name         , '?')  <> nvl(barm.name         , '?') ";
        selectFields += "or nvl(cast(rm_temp.emp_cle       as varchar2(30)), '?') <> nvl(cast(barm.emp_cle       as varchar2(30)), '?') ";
        
        // Driss BANA  modification du champs rm_temp.ZONEGARE_CODE par rm_temp.rm_std
        selectFields += "or nvl(cast(rm_temp.rm_std as varchar2(30)), '?') <> nvl(cast(barm.rm_std as varchar2(30)), '?') ";
        //selectFields += "or nvl(cast(rm_temp.zonegare_code as varchar2(30)), '?') <> nvl(cast(barm.zonegare_code as varchar2(30)), '?') ";
        selectFields += "or nvl(rm_temp.comments     , '?')  <> nvl(barm.comments     , '?') ";
        selectFields += "then 'M' else null end type_action, nvl(rm_temp.loc_id, barm.loc_id) loc_id";
        selectTables = "rm_temp full join immosis_backup_rm barm on barm.session_id = " + Integer.toString(sessionId) + " and barm.bl_id = rm_temp.bl_id and barm.fl_id = rm_temp.fl_id and barm.rm_id = rm_temp.rm_id left join bl on bl.bl_id = rm_temp.bl_id left join bl babl on babl.bl_id = barm.bl_id";
        whereClause = "nvl(barm.session_id, " + Integer.toString(sessionId) + ") = " + Integer.toString(sessionId) + " and nvl(rm_temp.bl_id, barm.bl_id) = '" + blId + "' and nvl(rm_temp.fl_id, barm.fl_id) = '" + flId + "' and nvl(rm_temp.loc_code, barm.loc_code) is not null and nvl(rm_temp.loc_num_ordre, barm.loc_num_ordre) is not null";
        orderClause = "loc_id";
        EventHandlerBase.executeDbSql(eventHandlerContext, insertTable, "insert into " + insertTable + " (" + insertFields + ") select " + selectFields + " from " + selectTables + " where " + whereClause + " order by " + orderClause, false);
    }
}


/* Class tablespecification */
