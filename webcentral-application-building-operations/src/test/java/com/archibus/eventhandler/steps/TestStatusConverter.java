package com.archibus.eventhandler.steps;

import org.junit.experimental.categories.Category;

import com.archibus.core.test.fixture.category.UnitTest;

import junit.framework.TestCase;

@Category({ UnitTest.class })
public class TestStatusConverter extends TestCase {

	public void testStatus() {

		assertEquals("Com", StatusConverter.getWorkRequestStatus("COMPLETED"));
		assertEquals("COMPLETED", StatusConverter.getActionStatus("Com"));
		assertEquals("N/A", StatusConverter.getActionStatus(""));
		assertEquals("N/A", StatusConverter.getActionStatus(null));

	}

}
