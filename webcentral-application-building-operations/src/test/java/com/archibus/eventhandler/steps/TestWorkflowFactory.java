package com.archibus.eventhandler.steps;

import java.util.HashMap;

import com.archibus.eventhandler.helpdesk.TestAll;
import com.archibus.fixture.EventHandlerFixture;
import com.archibus.fixture.EventHandlerFixture.EventHandlerContextImplTest;
import com.archibus.jobmanager.EventHandlerContext;

import junit.framework.TestCase;

public class TestWorkflowFactory extends TestCase {

    static final String ACTIVITY_ID = "AbBldgOpsHelpDesk";

    private static EventHandlerFixture fixture = null;

    private static Object transactionContext = null;

    /*
     * TODO Fix test. public static Test suite() { TestSuite testSuite = new
     * TestSuite(TestWorkflowFactory.class);
     *
     * TestSetup wrapper = new TestSetup(testSuite) {
     *
     * public void setUp() throws Exception { fixture = new EventHandlerFixture(this,
     * "ab-ex-echo.axvw"); fixture.setUp(); transactionContext = fixture.beginTransaction(); }
     *
     * public void tearDown() throws Exception { fixture.tearDown(); }
     *
     * };
     *
     * return wrapper; }
     */
    @Override
    protected void setUp() throws Exception {
        if (TestAll.fixtureAll != null) {
            fixture = TestAll.fixtureAll;
        }
        // always start transaction
        transactionContext = fixture.beginTransaction();
    }

    @Override
    protected void tearDown() throws Exception {
        // always rollback at the end of a test method
        fixture.rollbackTransaction(transactionContext);
    }

    public void testGetStatusManager() {

        final HashMap inputs = new HashMap();
        final EventHandlerContext context = new EventHandlerContextImplTest(inputs,
            fixture.getUserSession(), transactionContext, null);

        final StatusManager statusManager =
                WorkflowFactory.getStatusManager(context, ACTIVITY_ID, 320);
        assertNotNull(statusManager);

        final StepManager stepManager = WorkflowFactory.getStepManager(context, ACTIVITY_ID, 320);
        assertNotNull(stepManager);

    }

}
