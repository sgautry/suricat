package com.archibus.eventhandler.helpdesk;

import org.junit.experimental.categories.Category;

import com.archibus.core.test.fixture.category.DatabaseTest;
import com.archibus.datasource.DataSourceTestBase;

/**
 * Test HelpdeskBase
 *
 */
@Category({ DatabaseTest.class })
public class TestHelpdeskBase extends DataSourceTestBase {

	/**
	 * test method getCfIdForCurrentUser().
	 */
	public void testGetCfIdForCurrentUser() {
		final String cfId = HelpdeskEventHandlerBase.getCfIdForCurrentUser();
		assertNull(cfId);
	}

	/**
	 * test method notNull().
	 */
	public void testNotNull() {
		final String result = HelpdeskEventHandlerBase.notNull(null);
		assertEquals(result, "");
	}
}
