package com.archibus.eventhandler.helpdesk;

import org.junit.experimental.categories.Category;

import com.archibus.context.ContextStore;
import com.archibus.core.test.fixture.category.DatabaseTest;
import com.archibus.datasource.DataSourceTestBase;
import com.archibus.jobmanager.EventHandlerContext;

/**
 * Test EscalationHandler
 */
@Category({ DatabaseTest.class })
public class TestEscalationHandler extends DataSourceTestBase {

	/**
	 * test method runSLAEscalations().
	 */
	public void testRunSLAEscalations() {
		final EventHandlerContext context = ContextStore.get().getEventHandlerContext();
		final EscalationHandler handler = new EscalationHandler();
		handler.runSLAEscalations(context);
	}
}
