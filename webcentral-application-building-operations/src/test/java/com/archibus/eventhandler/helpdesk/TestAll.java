package com.archibus.eventhandler.helpdesk;

import com.archibus.fixture.EventHandlerFixture;

import junit.framework.TestCase;

public class TestAll extends TestCase {
	/**
	 * Helper object providing test-related resource and methods.
	 */
	// make it package available
	public static EventHandlerFixture fixtureAll = null;

	/*
	 * TODO Fix test. public static Test suite() { TestSuite testSuite = new
	 * TestSuite();
	 *
	 * //helpdesk test suites testSuite.addTestSuite(TestActivityHandler.class);
	 * testSuite.addTestSuite(TestCommon.class);
	 * testSuite.addTestSuite(TestCommonHandler.class);
	 * testSuite.addTestSuite(TestEscalationHandler.class);
	 * testSuite.addTestSuite(TestQuestionnaireHandler.class);
	 * testSuite.addTestSuite(TestReportHandler.class);
	 * testSuite.addTestSuite(TestRequestHandler.class);
	 * testSuite.addTestSuite(Tester.class);
	 *
	 * //on demand test suites
	 * testSuite.addTestSuite(TestWorkRequestHandler.class);
	 * testSuite.addTestSuite(TestScheduleHandler.class);
	 *
	 * //SLA test suites testSuite.addTestSuite(TestCalendarManager.class);
	 * testSuite.addTestSuite(TestServiceLevelAgreementHandler.class);
	 * testSuite.addTestSuite(TestServiceWindow.class);
	 * testSuite.addTestSuite(TestServiceLevelAgreement.class);
	 * testSuite.addTestSuite(TestSLAAutomation.class);
	 *
	 * //Steps test suites testSuite.addTestSuite(TestStatusConverter.class);
	 * testSuite.addTestSuite(TestStepHandler.class);
	 * testSuite.addTestSuite(TestSteps.class);
	 * testSuite.addTestSuite(TestApproval.class);
	 * testSuite.addTestSuite(TestReview.class);
	 * testSuite.addTestSuite(TestStepManagerImpl.class);
	 * testSuite.addTestSuite(TestWorkflowFactory.class);
	 *
	 * //Roles test suites
	 * testSuite.addTestSuite(TestBuildingManagerLookup.class);
	 *
	 * TestSetup wrapper = new TestSetup(testSuite) {
	 *
	 * public void setUp() throws Exception { fixtureAll = new
	 * EventHandlerFixture(this, "ab-ex-echo.axvw"); fixtureAll.setUp(); }
	 *
	 * public void tearDown() throws Exception { fixtureAll.tearDown(); }
	 *
	 * };
	 *
	 * return wrapper; }
	 */

	@Override
	protected void setUp() throws Exception {
		// do nothing
	}

	@Override
	protected void tearDown() throws Exception {
		// do nothing
	}

}
