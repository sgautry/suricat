package com.archibus.eventhandler.prevmaint;

import java.util.Calendar;

import org.junit.experimental.categories.Category;

import com.archibus.context.ContextStore;
import com.archibus.core.test.fixture.category.DatabaseTest;
import com.archibus.datasource.DataSourceTestBase;
import com.archibus.eventhandler.EventHandlerBase;
import com.archibus.jobmanager.EventHandlerContext;
import com.ibm.icu.text.SimpleDateFormat;

@Category({ DatabaseTest.class })
public class ForecastDatesGeneratorTest extends DataSourceTestBase {

	private final String isoDateStart;

	private final String isoDateEnd;

	public ForecastDatesGeneratorTest() {
		super();
		final SimpleDateFormat dateFormat = new SimpleDateFormat();
		dateFormat.applyPattern("yyyy-MM-dd");
		final Calendar calendar = Calendar.getInstance();
		this.isoDateStart = dateFormat.format(calendar.getTime());
		calendar.add(Calendar.DATE, 10);
		this.isoDateEnd = dateFormat.format(calendar.getTime());

	}

	public void testForecastPMResources() {

		final EventHandlerContext eventHandlerContext = ContextStore.get().getEventHandlerContext();

		final String dateFrom = EventHandlerBase.formatSqlIsoToNativeDate(eventHandlerContext, this.isoDateStart);
		final String dateTo = EventHandlerBase.formatSqlIsoToNativeDate(eventHandlerContext, this.isoDateEnd);

		// Call ForecastDatesGenerator to create forecast records in database.
		final ForecastDatesGenerator forecastGenerator = new ForecastDatesGenerator("ALL", " 1=1 ", dateFrom, dateTo,
				null, null, null);

		forecastGenerator.run();
	}

	public void testForecastPM52W() {

		final EventHandlerContext eventHandlerContext = ContextStore.get().getEventHandlerContext();

		final String dateFrom = EventHandlerBase.formatSqlIsoToNativeDate(eventHandlerContext, this.isoDateStart);
		final String dateTo = EventHandlerBase.formatSqlIsoToNativeDate(eventHandlerContext, this.isoDateEnd);

		// Call ForecastDatesGenerator to create forecast records in database.
		final ForecastDatesGenerator forecastGenerator = new ForecastDatesGenerator("52W", " 1=1 ", dateFrom, dateTo,
				null, null, null);

		forecastGenerator.run();
	}

	public void testForecastPM12M() {

		final EventHandlerContext eventHandlerContext = ContextStore.get().getEventHandlerContext();

		final String dateFrom = EventHandlerBase.formatSqlIsoToNativeDate(eventHandlerContext, this.isoDateStart);
		final String dateTo = EventHandlerBase.formatSqlIsoToNativeDate(eventHandlerContext, this.isoDateEnd);

		// Call ForecastDatesGenerator to create forecast records in database.
		final ForecastDatesGenerator forecastGenerator = new ForecastDatesGenerator("12M", " 1=1 ", dateFrom, dateTo,
				null, null, null);

		forecastGenerator.run();
	}

}
