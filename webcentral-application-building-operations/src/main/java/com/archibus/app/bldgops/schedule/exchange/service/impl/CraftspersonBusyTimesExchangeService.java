package com.archibus.app.bldgops.schedule.exchange.service.impl;

import static com.archibus.app.bldgops.schedule.exchange.util.CraftspersonScheduleConstants.*;

import java.util.*;

import com.archibus.app.bldgops.schedule.dao.*;
import com.archibus.app.bldgops.schedule.domain.*;
import com.archibus.app.bldgops.schedule.exchange.service.*;
import com.archibus.app.bldgops.schedule.exchange.util.TimeZoneConverter;
import com.archibus.utility.StringUtil;

import microsoft.exchange.webservices.data.*;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Workflow rule service to get busy times from exchange server.
 *
 * @author Jia
 * @since 23.2
 *
 */
public class CraftspersonBusyTimesExchangeService implements ICraftspersonBusyTimesExchangeService {
	/**
	 * exchange helper, used to read properties from application exchange
	 * property file.
	 */
	private CraftspersonExchangeServiceHelper exchangeServiceHelper;
	/**
	 * provides methods to handle craftsperson object.
	 */
	private ICraftspersonDao<Craftsperson> craftspersonDao;
	/**
	 * provides methods to handle scheduled variances object.
	 */
	private ICraftspersonScheduleVariancesDao<CraftspersonScheduleVariances> scheduledVarianceDao;

	/**
	 * map list to store craftsperson email address and time zone id.
	 */
	Map<String, String> craftspersonTimeZoneMapper = new HashMap<String, String>();

	/**
	 * Default exchange server time zone name.
	 */
	private final String exchangeSeverTimeZoneId = EXCHANGE_SERVER_TIME_ZONE_ID;

	@Override
	public void retrieveCFBusyTimes() throws Exception {
		// use enabled property in configuration file to determine retrieve busy
		// times or not.
		if (!this.exchangeServiceHelper.isEnabled()) {
			return;
		}
		// Get exchange service.
		final ExchangeService service = this.exchangeServiceHelper.getService();

		// Get all attendees from craftsperson table..
		final List<AttendeeInfo> attendees = getAllAttendees();
		// get time zone for all attendees.
		this.craftspersonTimeZoneMapper = getTimeZoneForAttendees(attendees);
		GetUserAvailabilityResults results;

		final Date start = new Date();
		final Calendar endDateCalendar = Calendar.getInstance();
		endDateCalendar.add(Calendar.DATE, 62);
		final Date end = endDateCalendar.getTime();

		try {
			// get attendee availability.
			results = service.getUserAvailability(attendees, new TimeWindow(start, end), AvailabilityData.FreeBusy);
			// CHECKSTYLE:OFF : Suppress IllegalCatch warning. Justification:
			// third-party API method
			// throws a checked Exception, which needs to be wrapped in
			// ExceptionBase.
		} catch (final Exception exception) {
			// CHECKSTYLE:ON
			// @translatable
			throw new com.archibus.app.bldgops.schedule.exchange.domain.CalendarException(
					"Error retrieving attendee availability", exception, CraftspersonBusyTimesExchangeService.class);
		}

		// get availability information.
		int attendeeIndex = 0;

		// delete all existing busy times in ARCHIBUS database in the time
		// period.
		this.scheduledVarianceDao.deleteBusyVariancesInTimePeriod(start, end);

		// retrieve all available busy times from attendees availability.
		for (final AttendeeAvailability attendeeAvailability : results.getAttendeesAvailability()) {
			// get craftsperson object by craftsperson email.
			final Craftsperson craftsperson = this.craftspersonDao
					.getCraftspersonByEmail(attendees.get(attendeeIndex).getSmtpAddress());

			if (attendeeAvailability.getErrorCode() == ServiceError.NoError) {
				for (final CalendarEvent calendarEvent : attendeeAvailability.getCalendarEvents()) {
					// To determine for which emails you can retrieve the
					// free-busy info by using the linkedDomain property.
					if (this.exchangeServiceHelper.isLinkedDomain(craftsperson.getEmail())) {
						// Get craftsperson time zone id.
						// 1. get time zone from building of employee that
						// associate
						// with craftsperson through email.
						// If the craftsperson is associated with a Building,
						// then
						// we can usually get the time zone from afm_timezones.
						// If
						// we cannot get the timezone because the craftsperson
						// has
						// no building (or some other reason), then we'll just
						// have
						// to create the Busy time with the GMT time.
						final String targetTimeZoneId = this.craftspersonTimeZoneMapper.get(craftsperson.getEmail());
						// format time between two different time zones.
						final Date formattedStartTime = TimeZoneConverter.calculateDateTime(
								calendarEvent.getStartTime(), this.exchangeSeverTimeZoneId, targetTimeZoneId);
						final Date formattedEndTime = TimeZoneConverter.calculateDateTime(calendarEvent.getEndTime(),
								this.exchangeSeverTimeZoneId, targetTimeZoneId);

						// Save busy times from exchange to craftsperson
						// scheduled
						// variances table.
						final CraftspersonScheduleVariances newVariances = new CraftspersonScheduleVariances();
						newVariances.setCfId(craftsperson.getCfId());
						newVariances.setDateStart(formattedStartTime);
						newVariances.setTimeStart(new java.sql.Time(formattedStartTime.getTime()));
						newVariances.setDateEnd(formattedEndTime);
						newVariances.setTimeEnd(new java.sql.Time(formattedEndTime.getTime()));
						newVariances.setSchedVarsType(VARIANCE_TYPE_BUSY);
						this.scheduledVarianceDao.save(newVariances);
					}

				}
			}
			attendeeIndex++;
		}
	}

	/**
	 * Get all attendees.
	 *
	 * @return Attendee object list.
	 */
	public List<AttendeeInfo> getAllAttendees() {
		final List<AttendeeInfo> attendees = new ArrayList<AttendeeInfo>();
		final List<Craftsperson> craftspersons = this.craftspersonDao.getAllCraftspersons();
		for (int i = 0; i < craftspersons.size(); i++) {
			final Craftsperson craftsperson = craftspersons.get(i);
			if (StringUtil.notNullOrEmpty(craftsperson.getEmail())) {
				attendees.add(new AttendeeInfo(craftsperson.getEmail()));
			}
		}

		return attendees;
	}

	/**
	 * Get time zone id for attendees.
	 *
	 * @param attendees
	 *            -AttendeeInfo List.
	 * @return timeZoneMapper
	 */
	public Map<String, String> getTimeZoneForAttendees(final List<AttendeeInfo> attendees) {
		final Map<String, String> timeZoneMapper = new HashMap<String, String>();
		for (final AttendeeInfo attendee : attendees) {
			// get attendee email address.
			final String emailAddress = attendee.getSmtpAddress();
			final String buildingId = this.craftspersonDao.getReferencedEmployeeBuildingCode(emailAddress);
			String timeZoneId = this.exchangeSeverTimeZoneId;
			if (!StringUtil.isNullOrEmpty(buildingId)) {
				timeZoneId = TimeZoneConverter.getTimeZoneIdForBuilding(buildingId);
				if (StringUtil.isNullOrEmpty(timeZoneId)) {
					timeZoneId = this.exchangeSeverTimeZoneId;
				}
			}

			timeZoneMapper.put(emailAddress, timeZoneId);
		}

		return timeZoneMapper;
	}

	/**
	 * Getter for the exchangeServiceHelper property.
	 *
	 * @return the exchangeServiceHelper property.
	 */
	public CraftspersonExchangeServiceHelper getExchangeServiceHelper() {
		return this.exchangeServiceHelper;
	}

	/**
	 * Setter for the exchangeServiceHelper property.
	 *
	 * @param exchangeServiceHelper
	 *            the exchangeServiceHelper to set.
	 */
	public void setExchangeServiceHelper(final CraftspersonExchangeServiceHelper exchangeServiceHelper) {
		this.exchangeServiceHelper = exchangeServiceHelper;
	}

	/**
	 * Getter for the craftspersonDao property.
	 *
	 * @return the craftspersonDao property.
	 */
	public final ICraftspersonDao<Craftsperson> getCraftspersonDao() {
		return this.craftspersonDao;
	}

	/**
	 * Setter for the craftspersonDao property.
	 *
	 * @param craftspersonDao
	 *            the craftspersonDao to set.
	 */
	public final void setCraftspersonDao(final ICraftspersonDao<Craftsperson> craftspersonDao) {
		this.craftspersonDao = craftspersonDao;
	}

	/**
	 * Getter for the scheduledVarianceDao property.
	 *
	 * @return the scheduledVarianceDao property.
	 */
	public final ICraftspersonScheduleVariancesDao<CraftspersonScheduleVariances> getScheduledVarianceDao() {
		return this.scheduledVarianceDao;
	}

	/**
	 * Setter for the scheduledVarianceDao property.
	 *
	 * @param scheduledVarianceDao
	 *            the scheduledVarianceDao to set.
	 */
	public final void setScheduledVarianceDao(
			final ICraftspersonScheduleVariancesDao<CraftspersonScheduleVariances> scheduledVarianceDao) {
		this.scheduledVarianceDao = scheduledVarianceDao;
	}

}
