package com.archibus.app.bldgops.partinv.dao;

import java.util.List;

import com.archibus.core.dao.IDao;

/**
 *
 * Provides methods to load Work Request Part.
 * <p>
 *
 * @author Guo Jiangtao
 * @since 23.2
 *
 * @param <WorkRequestPart> type of object
 */
public interface IWorkRequestPartDao<WorkRequestPart> extends IDao<WorkRequestPart> {

    /**
     * Find estimated parts by part code and wrId and date assigned and time assigned.
     *
     * @param partId part code
     * @param wrId work request code
     * @param dateAssigned date assigned
     * @param timeAssigned time assigned
     * @return WorkRequestPart object
     */
    WorkRequestPart findEstimatedPart(final String partId, final int wrId,
            final String dateAssigned, final String timeAssigned);

    /**
     * Find estimated parts by part code ,part location code and wrId and date assigned and time
     * assigned.
     *
     * @param partId part code
     * @param partLocId part location code
     * @param wrId work request code
     * @param dateAssigned date assigned
     * @param timeAssigned time assigned
     * @return WorkRequestPart object
     */
    WorkRequestPart findEstimatedPart(final String partId, final String partLocId, final int wrId,
            final String dateAssigned, final String timeAssigned);

    /**
     * Find estimated parts by part code and status.
     *
     * @param partId part code
     * @param statuses wrpt statuses
     * @return list of WorkRequestPart object
     */
    List<WorkRequestPart> findEstimatedParts(final String partId, final String[] statuses);

    /**
     * Find estimated parts by part code and location code and status.
     *
     * @param partId part code
     * @param locId location code
     * @param statuses wrpt statuses
     * @return list of WorkRequestPart object
     */
    List<WorkRequestPart> findEstimatedParts(final String partId, final String locId,
            final String[] statuses);

    /**
     * Find estimated parts by part code.
     *
     * @param partId part code
     * @return list of WorkRequestPart object
     */
    List<WorkRequestPart> findEstimatedParts(final String partId);

    /**
     * Find estimated parts by part code.
     *
     * @param partId part code
     * @param locId location code
     * @return list of WorkRequestPart object
     */
    List<WorkRequestPart> findEstimatedParts(final String partId, final String locId);

}
