package com.archibus.app.bldgops.schedule.domain;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for Craftsperon.
 *
 * @author Jia
 * @since 23.2
 *
 */
public class Craftsperson {
	/**
	 * Craftsperson Code.
	 */
	private String cfId;

	/**
	 * Craftsperson Email.
	 */
	private String email;

	/**
	 * Getter for the cfId property.
	 *
	 * @return the cfId property.
	 */
	public final String getCfId() {
		return this.cfId;
	}

	/**
	 * Setter for the cfId property.
	 *
	 * @param cfId
	 *            the cfId to set.
	 */
	public final void setCfId(final String cfId) {
		this.cfId = cfId;
	}

	/**
	 * Getter for the email property.
	 *
	 * @return the email property.
	 */
	public final String getEmail() {
		return this.email;
	}

	/**
	 * Setter for the email property.
	 *
	 * @param email
	 *            the email to set.
	 */
	public final void setEmail(final String email) {
		this.email = email;
	}

}
