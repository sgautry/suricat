package com.archibus.app.bldgops.partinv.domain;

import java.util.Date;

/**
 * Domain object for po_line.
 * <p>
 *
 * @author Guo Jiangtao,Jia
 * @since 23.2
 *
 */
public class PurchaseOrderLineItem {

    /** The purchase order id. */
    private int poId;

    /** The purchase order line id. */
    private int poLineId;

    /** The purchase order line date received. */
    private Date dateReceived;

    /** The purchase order line status. */
    private String status;

    /** The purchase order line employee id. */
    private String emId;

    /** The purchase order line unit cost. */
    private double unitCost;

    /** The purchase order line qunatity. */
    private int qunatity;

    /** The purchase order line catelog number. */
    private String catNo;

    /** The purchase order line description. */
    private String description;

    /** The purchase order line comments. */
    private String comments;

    /**
     * Getter for the poId property.
     *
     * @see poId
     * @return the poId property.
     */
    public int getPoId() {
        return this.poId;
    }

    /**
     * Setter for the poId property.
     *
     * @see poId
     * @param poId the poId to set
     */

    public void setPoId(final int poId) {
        this.poId = poId;
    }

    /**
     * Getter for the poLineId property.
     *
     * @see poLineId
     * @return the poLineId property.
     */
    public int getPoLineId() {
        return this.poLineId;
    }

    /**
     * Setter for the poLineId property.
     *
     * @see poLineId
     * @param poLineId the poLineId to set
     */

    public void setPoLineId(final int poLineId) {
        this.poLineId = poLineId;
    }

    /**
     * Getter for the dateReceived property.
     *
     * @see dateReceived
     * @return the dateReceived property.
     */
    public Date getDateReceived() {
        return this.dateReceived;
    }

    /**
     * Setter for the dateReceived property.
     *
     * @see dateReceived
     * @param dateReceived the dateReceived to set
     */

    public void setDateReceived(final Date dateReceived) {
        this.dateReceived = dateReceived;
    }

    /**
     * Getter for the status property.
     *
     * @see status
     * @return the status property.
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * Setter for the status property.
     *
     * @see status
     * @param status the status to set
     */

    public void setStatus(final String status) {
        this.status = status;
    }

    /**
     * Getter for the emId property.
     *
     * @see emId
     * @return the emId property.
     */
    public String getEmId() {
        return this.emId;
    }

    /**
     * Setter for the emId property.
     *
     * @see emId
     * @param emId the emId to set
     */

    public void setEmId(final String emId) {
        this.emId = emId;
    }

    /**
     * Getter for the unitCost property.
     *
     * @see unitCost
     * @return the unitCost property.
     */
    public double getUnitCost() {
        return this.unitCost;
    }

    /**
     * Setter for the unitCost property.
     *
     * @see unitCost
     * @param unitCost the unitCost to set
     */

    public void setUnitCost(final double unitCost) {
        this.unitCost = unitCost;
    }

    /**
     * Getter for the qunatity property.
     *
     * @see qunatity
     * @return the qunatity property.
     */
    public int getQunatity() {
        return this.qunatity;
    }

    /**
     * Setter for the qunatity property.
     *
     * @see qunatity
     * @param qunatity the qunatity to set
     */

    public void setQunatity(final int qunatity) {
        this.qunatity = qunatity;
    }

    /**
     * Getter for the catNo property.
     *
     * @see catNo
     * @return the catNo property.
     */
    public String getCatNo() {
        return this.catNo;
    }

    /**
     * Setter for the catNo property.
     *
     * @see catNo
     * @param catNo the catNo to set
     */

    public void setCatNo(final String catNo) {
        this.catNo = catNo;
    }

    /**
     * Getter for the description property.
     *
     * @see description
     * @return the description property.
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Setter for the description property.
     *
     * @see description
     * @param description the description to set
     */

    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Getter for the comments property.
     *
     * @see comments
     * @return the comments property.
     */
    public String getComments() {
        return this.comments;
    }

    /**
     * Setter for the comments property.
     *
     * @see comments
     * @param comments the comments to set
     */

    public void setComments(final String comments) {
        this.comments = comments;
    }

}
