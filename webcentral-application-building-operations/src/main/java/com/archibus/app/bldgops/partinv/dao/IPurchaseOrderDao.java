package com.archibus.app.bldgops.partinv.dao;

import org.json.JSONArray;

import com.archibus.core.dao.IDao;

/**
 *
 * Provides methods to load Purchase Order.
 * <p>
 *
 * @author Guo Jiangtao,Jia
 * @since 23.2
 *
 * @param <PurchaseOrder> type of object
 */
public interface IPurchaseOrderDao<PurchaseOrder> extends IDao<PurchaseOrder> {
    /**
     * Update purchase order info by purchase order status.
     *
     * @param poId Purchase order ID.
     * @param beforeStatus Purchase order status before change.
     * @param afterStatus Purchase order status after change.
     */
    void updatePurchaseOrderInfoByPoStatus(final int poId, final String beforeStatus,
            final String afterStatus);

    /**
     * Create new purchase order records.
     *
     * @param poRecord Purchase Order parameter record
     * @param poLineRecords Purchase Order Line Item Records
     *
     */
    void createNewPurchaseOrder(final JSONArray poRecord, final JSONArray poLineRecords);

    /**
     * Get Purchase Order by id.
     *
     * @param poId Purchase Order code
     * @return PurchaseOrder object
     */
    PurchaseOrder findPurchaseOrder(final int poId);

    /**
     *
     * Change Purchase Orders to be Received or Error.
     *
     * @param poId Purchase Order id
     * @param poLineId Purchase Order Item Id
     * @param partCode Part code
     * @param receivedLocation Receiving Location
     * @param transQty Transaction Quantity
     * @param costUnit Cost unit price
     * @param status Status 'Received' or 'Error'
     */
    void setPurchaseOrderTobeReceivedOrError(final String poId, final String poLineId,
            final String partCode, final String receivedLocation, final Double transQty,
            final Double costUnit, final String status);

    /**
     * Add record to an existing purchase order.
     *
     * @param poId Purchase Order code
     * @param vnId Vendor code.
     * @param poLineRecords Purchase Order Line Item Records
     */
    void addToExistingPurchaseOrder(final int poId, final String vnId,
            final JSONArray poLineRecords);

    /**
     * Remove item from purchase order line item.
     *
     * @param poId Purchase order code
     * @param poLineId Purchase order line code.
     */
    void removeItemFromPoLine(final int poId, final int poLineId);

    /**
     * Update Purchase Order.
     *
     * @param purchaseOrderRecords Purchase order field record
     */
    void updatePurchaseOrderInfor(final JSONArray purchaseOrderRecords);

    /**
     * Save or update purchase order line item.
     *
     * @param poLineItemRecords Purchase order line item JSONArray.
     * @param isNew Flag to save record or update record.
     */
    void savePurchaseOrderLineItem(JSONArray poLineItemRecords, boolean isNew);
}
