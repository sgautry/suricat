package com.archibus.app.bldgops.schedule.dao;

import java.util.Date;

import com.archibus.app.bldgops.schedule.domain.CraftspersonWorkRequest;
import com.archibus.core.dao.IDao;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Class to provide method to handler with Craftsperson Work Requests.
 *
 * @author Jia
 * @since 23.2
 *
 */
public interface ICraftspersonWorkRequestDao extends IDao<CraftspersonWorkRequest> {
	/**
	 *
	 * Get craftsperson work request.
	 *
	 * @param cfId
	 *            -Craftsperson Code.
	 * @param wrId
	 *            -Work Request Code.
	 * @param dateAssigned
	 *            - Date Assigned.
	 * @param timeAssigned
	 *            -Time Assigned.
	 * @return Craftsperson Object.
	 */
	CraftspersonWorkRequest getCraftspersonWorkRequest(String cfId, int wrId, Date dateAssigned, Date timeAssigned);

	/**
	 * Unschedule past scheduled work requests.
	 */
	void unschedulePastScheduledWorkRequests();

}
