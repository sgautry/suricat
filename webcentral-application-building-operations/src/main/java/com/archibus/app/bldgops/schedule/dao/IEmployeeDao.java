package com.archibus.app.bldgops.schedule.dao;

import java.util.List;

import com.archibus.core.dao.IDao;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides methods to handle with the Employee.
 *
 * @param <Employee>
 *            Employee.
 * @author Jia
 * @since 23.2
 *
 */
public interface IEmployeeDao<Employee> extends IDao<Employee> {
	/**
	 * Get employee by employee primary key.
	 *
	 * @param emId
	 *            -Employee Code.
	 * @return Employee.
	 */
	Employee getEmployee(String emId);

	/**
	 * Get employee by email address.
	 *
	 * @param email
	 *            Email.
	 *
	 * @return -Craftsperson object.
	 */
	List<Employee> getEmployeesByEmail(String email);
}
