package com.archibus.app.bldgops.partinv.domain;

import java.util.Date;

/**
 * Domain object for supply_req.
 * <p>
 *
 * @author Guo Jiangtao,Jia
 * @since 23.2
 *
 */
public class SupplyRequisition {
    /** The supply requisition id. */
    private int supplyReqId;

    /** The supply requisition status. */
    private String status;

    /** The supply requisition last updated person. */
    private String lastUpdateBy;

    /** The supply requisition date created. */
    private Date dateCreated;

    /** The supply requisition time created. */
    private Date timeCreated;

    /** The supply requisition comments. */
    private String comments;

    /**
     * Getter for the supplyReqId property.
     *
     * @see supplyReqId
     * @return the supplyReqId property.
     */
    public int getSupplyReqId() {
        return this.supplyReqId;
    }

    /**
     * Setter for the supplyReqId property.
     *
     * @see supplyReqId
     * @param supplyReqId the supplyReqId to set
     */

    public void setSupplyReqId(final int supplyReqId) {
        this.supplyReqId = supplyReqId;
    }

    /**
     * Getter for the status property.
     *
     * @see status
     * @return the status property.
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * Setter for the status property.
     *
     * @see status
     * @param status the status to set
     */

    public void setStatus(final String status) {
        this.status = status;
    }

    /**
     * Getter for the lastUpdateBy property.
     *
     * @see lastUpdateBy
     * @return the lastUpdateBy property.
     */
    public String getLastUpdateBy() {
        return this.lastUpdateBy;
    }

    /**
     * Setter for the lastUpdateBy property.
     *
     * @see lastUpdateBy
     * @param lastUpdateBy the lastUpdateBy to set
     */

    public void setLastUpdateBy(final String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
    }

    /**
     * Getter for the dateCreated property.
     *
     * @see dateCreated
     * @return the dateCreated property.
     */
    public Date getDateCreated() {
        return this.dateCreated;
    }

    /**
     * Setter for the dateCreated property.
     *
     * @see dateCreated
     * @param dateCreated the dateCreated to set
     */

    public void setDateCreated(final Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * Getter for the timeCreated property.
     *
     * @see timeCreated
     * @return the timeCreated property.
     */
    public Date getTimeCreated() {
        return this.timeCreated;
    }

    /**
     * Setter for the timeCreated property.
     *
     * @see timeCreated
     * @param timeCreated the timeCreated to set
     */

    public void setTimeCreated(final Date timeCreated) {
        this.timeCreated = timeCreated;
    }

    /**
     * Getter for the comments property.
     *
     * @see comments
     * @return the comments property.
     */
    public String getComments() {
        return this.comments;
    }

    /**
     * Setter for the comments property.
     *
     * @see comments
     * @param comments the comments to set
     */

    public void setComments(final String comments) {
        this.comments = comments;
    }

}
