package com.archibus.app.bldgops.schedule.domain;

import java.util.Date;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for craftsperson work request.
 *
 * @author Jia
 * @since 23.2
 *
 */
public class CraftspersonWorkRequest {
	/**
	 * Field for WorkRequest Code.
	 */
	private int wrId;
	/**
	 * Field for Craftsperson Code.
	 */
	private String cfId;
	/**
	 * Field for Date Assigned.
	 */
	private Date dateAssigned;
	/**
	 * Field for Time Assigned.
	 */
	private Date timeAssigned;
	/**
	 * Field for Task Order.
	 */
	private int taskOrder;

	/**
	 * Field for Hour Estimate.
	 */
	private double hoursEst;

	/**
	 * Field for Is Scheduled.
	 */
	private int isScheduled;

	/**
	 * Field for Status.
	 */
	private String status;

	/**
	 * Getter for the wrId property.
	 *
	 * @return the wrId property.
	 */
	public int getWrId() {
		return this.wrId;
	}

	/**
	 * Setter for the wrId property.
	 *
	 * @param wrId
	 *            the wrId to set.
	 */
	public void setWrId(final int wrId) {
		this.wrId = wrId;
	}

	/**
	 * Getter for the cfId property.
	 *
	 * @return the cfId property.
	 */
	public String getCfId() {
		return this.cfId;
	}

	/**
	 * Setter for the cfId property.
	 *
	 * @param cfId
	 *            the cfId to set.
	 */
	public void setCfId(final String cfId) {
		this.cfId = cfId;
	}

	/**
	 * Getter for the dateAssigned property.
	 *
	 * @return the dateAssigned property.
	 */
	public Date getDateAssigned() {
		return this.dateAssigned;
	}

	/**
	 * Setter for the dateAssigned property.
	 *
	 * @param dateAssigned
	 *            the dateAssigned to set.
	 */
	public void setDateAssigned(final Date dateAssigned) {
		this.dateAssigned = dateAssigned;
	}

	/**
	 * Getter for the timeAssigned property.
	 *
	 * @return the timeAssigned property.
	 */
	public Date getTimeAssigned() {
		return this.timeAssigned;
	}

	/**
	 * Setter for the timeAssigned property.
	 *
	 * @param timeAssigned
	 *            the timeAssigned to set.
	 */
	public void setTimeAssigned(final Date timeAssigned) {
		this.timeAssigned = timeAssigned;
	}

	/**
	 * Getter for the taskOrder property.
	 *
	 * @return the taskOrder property.
	 */
	public int getTaskOrder() {
		return this.taskOrder;
	}

	/**
	 * Setter for the taskOrder property.
	 *
	 * @param taskOrder
	 *            the taskOrder to set.
	 */
	public void setTaskOrder(final int taskOrder) {
		this.taskOrder = taskOrder;
	}

	/**
	 * Getter for the hoursEst property.
	 *
	 * @return the hoursEst property.
	 */
	public double getHoursEst() {
		return this.hoursEst;
	}

	/**
	 * Setter for the hoursEst property.
	 *
	 * @param hoursEst
	 *            the hoursEst to set.
	 */
	public void setHoursEst(final double hoursEst) {
		this.hoursEst = hoursEst;
	}

	/**
	 * Getter for the isScheduled property.
	 *
	 * @return the isScheduled property.
	 */
	public int getIsScheduled() {
		return this.isScheduled;
	}

	/**
	 * Setter for the isScheduled property.
	 *
	 * @param isScheduled
	 *            the isScheduled to set.
	 */
	public void setIsScheduled(final int isScheduled) {
		this.isScheduled = isScheduled;
	}

	/**
	 * Getter for the status property.
	 *
	 * @return the status property.
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 * Setter for the status property.
	 *
	 * @param status
	 *            the status to set.
	 */
	public void setStatus(final String status) {
		this.status = status;
	}

}
