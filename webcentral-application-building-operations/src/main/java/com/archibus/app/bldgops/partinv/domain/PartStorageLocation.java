package com.archibus.app.bldgops.partinv.domain;

import java.util.Date;

/**
 * Domain object for pt_store_loc_pt.
 * <p>
 *
 * @author Guo Jiangtao,Jia
 * @since 23.2
 *
 */
public class PartStorageLocation extends Part {

    /** The part storage location id. */
    private String locId;

    /** The part storage location building code. */
    private String blId;

    /** The part storage location floor code. */
    private String flId;

    /** The part storage location room code. */
    private String rmId;

    /** The part storage location date of last count field. */
    private Date dateOfLastCnt;

    /**
     * Getter for the locId property.
     *
     * @see locId
     * @return the locId property.
     */
    public String getLocId() {
        return this.locId;
    }

    /**
     * Setter for the locId property.
     *
     * @see locId
     * @param locId the locId to set
     */

    public void setLocId(final String locId) {
        this.locId = locId;
    }

    /**
     * Getter for the blId property.
     *
     * @see blId
     * @return the blId property.
     */
    public String getBlId() {
        return this.blId;
    }

    /**
     * Setter for the blId property.
     *
     * @see blId
     * @param blId the blId to set
     */

    public void setBlId(final String blId) {
        this.blId = blId;
    }

    /**
     * Getter for the flId property.
     *
     * @see flId
     * @return the flId property.
     */
    public String getFlId() {
        return this.flId;
    }

    /**
     * Setter for the flId property.
     *
     * @see flId
     * @param flId the flId to set
     */

    public void setFlId(final String flId) {
        this.flId = flId;
    }

    /**
     * Getter for the rmId property.
     *
     * @see rmId
     * @return the rmId property.
     */
    public String getRmId() {
        return this.rmId;
    }

    /**
     * Setter for the rmId property.
     *
     * @see rmId
     * @param rmId the rmId to set
     */

    public void setRmId(final String rmId) {
        this.rmId = rmId;
    }

    /**
     * Getter for the dateOfLastCnt property.
     *
     * @see dateOfLastCnt
     * @return the dateOfLastCnt property.
     */
    public Date getDateOfLastCnt() {
        return this.dateOfLastCnt;
    }

    /**
     * Setter for the dateOfLastCnt property.
     *
     * @see dateOfLastCnt
     * @param dateOfLastCnt the dateOfLastCnt to set
     */

    public void setDateOfLastCnt(final Date dateOfLastCnt) {
        this.dateOfLastCnt = dateOfLastCnt;
    }

}
