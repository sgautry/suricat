package com.archibus.app.bldgops.partinv.domain;

import java.util.Date;

/**
 * Domain object for po.
 * <p>
 *
 * @author Guo Jiangtao,Jia
 * @since 23.2
 *
 *        <p>
 *        Suppress PMD.TooManyFields warning.
 *        <p>
 *        Justification: This entity contains many fields.
 *
 */
@SuppressWarnings({ "PMD.TooManyFields", "PMD.ExcessivePublicCount" })
public class PurchaseOrder {

    /** The purchase order id. */
    private int poId;

    /** The purchase order status. */
    private String status;

    /** The purchase order approved by. */
    private String approvedBy;

    /** The purchase order date received. */
    private Date dateReceived;

    /** The purchase order vendor id. */
    private String vnCode;

    /** The purchase order receiving location. */
    private String receivingLocation;

    /** The purchase order date approved. */
    private Date dateApproved;

    /** The purchase order date request. */
    private Date dateRequest;

    /** The purchase order date send. */
    private Date dateSent;

    /** The purchase order employee id. */
    private String emCode;

    /** The purchase order account id. */
    private String acCode;

    /** The purchase order number. */
    private String poNum;

    /** The purchase order source. */
    private String source;

    /** The purchase order comments. */
    private String comments;

    /** The purchase order amount_approved. */
    private double amountApproved;

    /** The purchase order date paid. */
    private Date datePaid;

    /** The purchase order federal tax. */
    private double federalTax;

    /** The purchase order state tax. */
    private double stateTax;

    /** The purchase order shipping. */
    private double shipping;

    /** The purchase order shipping emplyee code. */
    private String shippingEmId;

    /** The purchase order billing emplyee code. */
    private String billingEmId;

    /** The purchase order shipping address1. */
    private String shipAddress1;

    /** The purchase order billing address1. */
    private String billAddress1;

    /** The purchase order shipping address2. */
    private String shipAddress2;

    /** The purchase order billing address2. */
    private String billAddress2;

    /** The purchase order ship city code. */
    private String shipCityId;

    /** The purchase order bill city code. */
    private String billCityId;

    /** The purchase order ship state code. */
    private String shipStateId;

    /** The purchase order bill state code. */
    private String billStateId;

    /** The purchase order ship zip. */
    private String shipZip;

    /** The purchase order bill zip. */
    private String billZip;

    /**
     * Getter for the poId property.
     *
     * @see poId
     * @return the poId property.
     */
    public int getPoId() {
        return this.poId;
    }

    /**
     * Setter for the poId property.
     *
     * @see poId
     * @param poId the poId to set
     */

    public void setPoId(final int poId) {
        this.poId = poId;
    }

    /**
     * Getter for the status property.
     *
     * @see status
     * @return the status property.
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * Setter for the status property.
     *
     * @see status
     * @param status the status to set
     */

    public void setStatus(final String status) {
        this.status = status;
    }

    /**
     * Getter for the approvedBy property.
     *
     * @see approvedBy
     * @return the approvedBy property.
     */
    public String getApprovedBy() {
        return this.approvedBy;
    }

    /**
     * Setter for the approvedBy property.
     *
     * @see approvedBy
     * @param approvedBy the approvedBy to set
     */

    public void setApprovedBy(final String approvedBy) {
        this.approvedBy = approvedBy;
    }

    /**
     * Getter for the dateReceived property.
     *
     * @see dateReceived
     * @return the dateReceived property.
     */
    public Date getDateReceived() {
        return this.dateReceived;
    }

    /**
     * Setter for the dateReceived property.
     *
     * @see dateReceived
     * @param dateReceived the dateReceived to set
     */

    public void setDateReceived(final Date dateReceived) {
        this.dateReceived = dateReceived;
    }

    /**
     * Getter for the vnCode property.
     *
     * @see vnCode
     * @return the vnCode property.
     */
    public String getVnCode() {
        return this.vnCode;
    }

    /**
     * Setter for the vnCode property.
     *
     * @see vnCode
     * @param vnCode the vnCode to set
     */

    public void setVnCode(final String vnCode) {
        this.vnCode = vnCode;
    }

    /**
     * Getter for the receivingLocation property.
     *
     * @see receivingLocation
     * @return the receivingLocation property.
     */
    public String getReceivingLocation() {
        return this.receivingLocation;
    }

    /**
     * Setter for the receivingLocation property.
     *
     * @see receivingLocation
     * @param receivingLocation the receivingLocation to set
     */

    public void setReceivingLocation(final String receivingLocation) {
        this.receivingLocation = receivingLocation;
    }

    /**
     * Getter for the dateApproved property.
     *
     * @see dateApproved
     * @return the dateApproved property.
     */
    public Date getDateApproved() {
        return this.dateApproved;
    }

    /**
     * Setter for the dateApproved property.
     *
     * @see dateApproved
     * @param dateApproved the dateApproved to set
     */

    public void setDateApproved(final Date dateApproved) {
        this.dateApproved = dateApproved;
    }

    /**
     * Getter for the dateRequest property.
     *
     * @see dateRequest
     * @return the dateRequest property.
     */
    public Date getDateRequest() {
        return this.dateRequest;
    }

    /**
     * Setter for the dateRequest property.
     *
     * @see dateRequest
     * @param dateRequest the dateRequest to set
     */

    public void setDateRequest(final Date dateRequest) {
        this.dateRequest = dateRequest;
    }

    /**
     * Getter for the dateSent property.
     *
     * @see dateSent
     * @return the dateSent property.
     */
    public Date getDateSent() {
        return this.dateSent;
    }

    /**
     * Setter for the dateSent property.
     *
     * @see dateSent
     * @param dateSent the dateSent to set
     */

    public void setDateSent(final Date dateSent) {
        this.dateSent = dateSent;
    }

    /**
     * Getter for the emCode property.
     *
     * @see emCode
     * @return the emCode property.
     */
    public String getEmCode() {
        return this.emCode;
    }

    /**
     * Setter for the emCode property.
     *
     * @see emCode
     * @param emCode the emCode to set
     */

    public void setEmCode(final String emCode) {
        this.emCode = emCode;
    }

    /**
     * Getter for the acCode property.
     *
     * @see acCode
     * @return the acCode property.
     */
    public String getAcCode() {
        return this.acCode;
    }

    /**
     * Setter for the acCode property.
     *
     * @see acCode
     * @param acCode the acCode to set
     */

    public void setAcCode(final String acCode) {
        this.acCode = acCode;
    }

    /**
     * Getter for the poNum property.
     *
     * @see poNum
     * @return the poNum property.
     */
    public String getPoNum() {
        return this.poNum;
    }

    /**
     * Setter for the poNum property.
     *
     * @see poNum
     * @param poNum the poNum to set
     */

    public void setPoNum(final String poNum) {
        this.poNum = poNum;
    }

    /**
     * Getter for the source property.
     *
     * @see source
     * @return the source property.
     */
    public String getSource() {
        return this.source;
    }

    /**
     * Setter for the source property.
     *
     * @see source
     * @param source the source to set
     */

    public void setSource(final String source) {
        this.source = source;
    }

    /**
     * Getter for the comments property.
     *
     * @see comments
     * @return the comments property.
     */
    public String getComments() {
        return this.comments;
    }

    /**
     * Setter for the comments property.
     *
     * @see comments
     * @param comments the comments to set
     */

    public void setComments(final String comments) {
        this.comments = comments;
    }

    /**
     * Getter for the amountApproved property.
     *
     * @see amountApproved
     * @return the amountApproved property.
     */
    public double getAmountApproved() {
        return this.amountApproved;
    }

    /**
     * Setter for the amountApproved property.
     *
     * @see amountApproved
     * @param amountApproved the amountApproved to set
     */

    public void setAmountApproved(final double amountApproved) {
        this.amountApproved = amountApproved;
    }

    /**
     * Getter for the datePaid property.
     *
     * @see datePaid
     * @return the datePaid property.
     */
    public Date getDatePaid() {
        return this.datePaid;
    }

    /**
     * Setter for the datePaid property.
     *
     * @see datePaid
     * @param datePaid the datePaid to set
     */

    public void setDatePaid(final Date datePaid) {
        this.datePaid = datePaid;
    }

    /**
     * Getter for the federalTax property.
     *
     * @see federalTax
     * @return the federalTax property.
     */
    public double getFederalTax() {
        return this.federalTax;
    }

    /**
     * Setter for the federalTax property.
     *
     * @see federalTax
     * @param federalTax the federalTax to set
     */

    public void setFederalTax(final double federalTax) {
        this.federalTax = federalTax;
    }

    /**
     * Getter for the stateTax property.
     *
     * @see stateTax
     * @return the stateTax property.
     */
    public double getStateTax() {
        return this.stateTax;
    }

    /**
     * Setter for the stateTax property.
     *
     * @see stateTax
     * @param stateTax the stateTax to set
     */

    public void setStateTax(final double stateTax) {
        this.stateTax = stateTax;
    }

    /**
     * Getter for the shipping property.
     *
     * @see shipping
     * @return the shipping property.
     */
    public double getShipping() {
        return this.shipping;
    }

    /**
     * Setter for the shipping property.
     *
     * @see shipping
     * @param shipping the shipping to set
     */

    public void setShipping(final double shipping) {
        this.shipping = shipping;
    }

    /**
     * Getter for the shippingEmId property.
     *
     * @see shippingEmId
     * @return the shippingEmId property.
     */
    public String getShippingEmId() {
        return this.shippingEmId;
    }

    /**
     * Setter for the shippingEmId property.
     *
     * @see shippingEmId
     * @param shippingEmId the shippingEmId to set
     */

    public void setShippingEmId(final String shippingEmId) {
        this.shippingEmId = shippingEmId;
    }

    /**
     * Getter for the billingEmId property.
     *
     * @see billingEmId
     * @return the billingEmId property.
     */
    public String getBillingEmId() {
        return this.billingEmId;
    }

    /**
     * Setter for the billingEmId property.
     *
     * @see billingEmId
     * @param billingEmId the billingEmId to set
     */

    public void setBillingEmId(final String billingEmId) {
        this.billingEmId = billingEmId;
    }

    /**
     * Getter for the shipAddress1 property.
     *
     * @see shipAddress1
     * @return the shipAddress1 property.
     */
    public String getShipAddress1() {
        return this.shipAddress1;
    }

    /**
     * Setter for the shipAddress1 property.
     *
     * @see shipAddress1
     * @param shipAddress1 the shipAddress1 to set
     */

    public void setShipAddress1(final String shipAddress1) {
        this.shipAddress1 = shipAddress1;
    }

    /**
     * Getter for the shipAddress2 property.
     *
     * @see shipAddress2
     * @return the shipAddress2 property.
     */
    public String getShipAddress2() {
        return this.shipAddress2;
    }

    /**
     * Setter for the shipAddress2 property.
     *
     * @see shipAddress2
     * @param shipAddress2 the shipAddress2 to set
     */

    public void setShipAddress2(final String shipAddress2) {
        this.shipAddress2 = shipAddress2;
    }

    /**
     * Getter for the billAddress2 property.
     *
     * @see billAddress2
     * @return the billAddress2 property.
     */
    public String getBillAddress2() {
        return this.billAddress2;
    }

    /**
     * Setter for the billAddress2 property.
     *
     * @see billAddress2
     * @param billAddress2 the billAddress2 to set
     */

    public void setBillAddress2(final String billAddress2) {
        this.billAddress2 = billAddress2;
    }

    /**
     * Getter for the shipCityId property.
     *
     * @see shipCityId
     * @return the shipCityId property.
     */
    public String getShipCityId() {
        return this.shipCityId;
    }

    /**
     * Setter for the shipCityId property.
     *
     * @see shipCityId
     * @param shipCityId the shipCityId to set
     */

    public void setShipCityId(final String shipCityId) {
        this.shipCityId = shipCityId;
    }

    /**
     * Getter for the billCityId property.
     *
     * @see billCityId
     * @return the billCityId property.
     */
    public String getBillCityId() {
        return this.billCityId;
    }

    /**
     * Setter for the billCityId property.
     *
     * @see billCityId
     * @param billCityId the billCityId to set
     */

    public void setBillCityId(final String billCityId) {
        this.billCityId = billCityId;
    }

    /**
     * Getter for the shipStateId property.
     *
     * @see shipStateId
     * @return the shipStateId property.
     */
    public String getShipStateId() {
        return this.shipStateId;
    }

    /**
     * Setter for the shipStateId property.
     *
     * @see shipStateId
     * @param shipStateId the shipStateId to set
     */

    public void setShipStateId(final String shipStateId) {
        this.shipStateId = shipStateId;
    }

    /**
     * Getter for the billStateId property.
     *
     * @see billStateId
     * @return the billStateId property.
     */
    public String getBillStateId() {
        return this.billStateId;
    }

    /**
     * Setter for the billStateId property.
     *
     * @see billStateId
     * @param billStateId the billStateId to set
     */

    public void setBillStateId(final String billStateId) {
        this.billStateId = billStateId;
    }

    /**
     * Getter for the shipZip property.
     *
     * @see shipZip
     * @return the shipZip property.
     */
    public String getShipZip() {
        return this.shipZip;
    }

    /**
     * Setter for the shipZip property.
     *
     * @see shipZip
     * @param shipZip the shipZip to set
     */

    public void setShipZip(final String shipZip) {
        this.shipZip = shipZip;
    }

    /**
     * Getter for the billZip property.
     *
     * @see billZip
     * @return the billZip property.
     */
    public String getBillZip() {
        return this.billZip;
    }

    /**
     * Setter for the billZip property.
     *
     * @see billZip
     * @param billZip the billZip to set
     */

    public void setBillZip(final String billZip) {
        this.billZip = billZip;
    }

    /**
     * Getter for the billAddress1 property.
     *
     * @see billAddress1
     * @return the billAddress1 property.
     */
    public String getBillAddress1() {
        return this.billAddress1;
    }

    /**
     * Setter for the billAddress1 property.
     *
     * @see billAddress1
     * @param billAddress1 the billAddress1 to set
     */

    public void setBillAddress1(final String billAddress1) {
        this.billAddress1 = billAddress1;
    }

}
