package com.archibus.app.bldgops.schedule.service.impl;

import com.archibus.app.bldgops.schedule.dao.ICraftspersonWorkRequestDao;
import com.archibus.app.bldgops.schedule.service.IUnschedulePastScheduleService;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Service to unschedule past scheduled work requests.
 *
 * Managed by Spring, bean object configured in
 * /webcentral-application-building-operations/src/main/resources/com/archibus/
 * app/bldgops/schedule/craftsperson-schedule-context.xml
 *
 * Called by scheduled
 * WFR:AbBldgOpsOnDemandWork-unschedulePastScheduledWorkRequests
 *
 * @author Jia
 * @since 23.2
 *
 */
public class UnschedulePastScheduleService implements IUnschedulePastScheduleService {
	private ICraftspersonWorkRequestDao craftspersonWorkRequestDao;

	@Override
	public void unschedulePastScheduleWorkRequests() {
		// unschedule past scheduled work request.
		this.craftspersonWorkRequestDao.unschedulePastScheduledWorkRequests();
	}

	/**
	 * Getter for the craftspersonWorkRequestDao property.
	 *
	 * @return the craftspersonWorkRequestDao property.
	 */
	public ICraftspersonWorkRequestDao getCraftspersonWorkRequestDao() {
		return this.craftspersonWorkRequestDao;
	}

	/**
	 * Setter for the craftspersonWorkRequestDao property.
	 *
	 * @param craftspersonWorkRequestDao
	 *            the craftspersonWorkRequestDao to set.
	 */
	public void setCraftspersonWorkRequestDao(final ICraftspersonWorkRequestDao craftspersonWorkRequestDao) {
		this.craftspersonWorkRequestDao = craftspersonWorkRequestDao;
	}

}
