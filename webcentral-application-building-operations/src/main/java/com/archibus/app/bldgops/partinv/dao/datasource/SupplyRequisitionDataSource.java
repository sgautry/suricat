package com.archibus.app.bldgops.partinv.dao.datasource;

import static com.archibus.app.bldgops.partinv.util.PartInventoryConstant.*;

import java.util.List;

import org.json.*;

import com.archibus.app.bldgops.partinv.dao.*;
import com.archibus.app.bldgops.partinv.domain.*;
import com.archibus.context.ContextStore;
import com.archibus.datasource.*;
import com.archibus.eventhandler.ondemandwork.WorkRequestHandler;
import com.archibus.eventhandler.resourcecalcs.ResourceCalculations;
import com.archibus.jobmanager.EventHandlerContext;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.ParsedRestrictionDef;
import com.archibus.utility.Utility;

/**
 *
 * Supply Requisition datasource.
 * <p>
 *
 *
 * @author Guo Jiangtao , Jia
 * @since 23.2
 *
 */
public class SupplyRequisitionDataSource extends ObjectDataSourceImpl<SupplyRequisition>
        implements ISupplyRequisitionDao<SupplyRequisition> {

    /**
     * Field names to property names mapping. All fields will be added to the DataSource.
     */
    private static final String[][] FIELDS_TO_PROPERTIES =
            { { SUPPLY_REQ_ID, "supplyReqId" }, { STATUS, "status" },
                    { LAST_UPDATE_BY, "lastUpdateBy" }, { DATE_CREATED, "dateCreated" },
                    { TIME_CREATED, "timeCreated" }, { COMMENTS, "comments" } };

    /**
     * Part storage location dataSource.
     */
    private ITransactionDao<Transaction> transactionDataSource;

    /**
     * Part storage location dataSource.
     */
    private IPartStorageLocationDao<PartStorageLocation> partStorageLocDataSource;

    /**
     * Constructs SupplyRequisitionDataSource, mapped to <code>supply_req</code> table, using
     * <code>supplyReq</code> bean.
     *
     */
    public SupplyRequisitionDataSource() {
        super("supplyReq", "supply_req");
    }

    @Override
    protected String[][] getFieldsToProperties() {
        return FIELDS_TO_PROPERTIES.clone();
    }

    /** {@inheritDoc} */

    @Override
    public SupplyRequisition findSupplyRequisition(final int supplyReqId) {

        final ParsedRestrictionDef resDef = new ParsedRestrictionDef();
        resDef.addClause(SUPPLY_REQ_TABLE, SUPPLY_REQ_ID, supplyReqId, Operation.EQUALS);

        SupplyRequisition supplyReq = null;
        final List<SupplyRequisition> supplyReqList = this.find(resDef);
        if (!supplyReqList.isEmpty()) {
            supplyReq = supplyReqList.get(0);
        }
        return supplyReq;
    }

    /** {@inheritDoc} */

    @Override
    public void createSupplyReq(final String fromStoreLoc, final String toStoreLoc,
            final String supplyReqComments, final JSONArray itRecords) {

        SupplyRequisition supplyReq = new SupplyRequisition();

        if (itRecords.length() > 0) {
            // Save new Record to supply requisition table
            supplyReq.setStatus(SUPPLY_REQ_STATUS_NEW);
            supplyReq.setLastUpdateBy(ContextStore.get().getUser().getName());
            supplyReq.setDateCreated(Utility.currentDate());
            supplyReq.setTimeCreated(Utility.currentTime());
            supplyReq.setComments(supplyReqComments);
            supplyReq = save(supplyReq);

            // save supply requisition items to it table
            this.transactionDataSource.addNewSupplyReqItems(supplyReq.getSupplyReqId(), itRecords,
                fromStoreLoc, toStoreLoc);

        }

    }

    /** {@inheritDoc} */

    @Override
    public void addPartsToExistingSupplyReq(final String supplyReqId, final JSONArray itRecords) {

        // From storage location
        String fromStorageLoctionId = "";
        // To storage location
        String toStorageLocationId = "";

        // get transaction based on supply requistion code
        final Transaction itObject = this.transactionDataSource
            .findTransactionBySupplyReq(Integer.parseInt(supplyReqId));
        if (itObject != null) {
            fromStorageLoctionId = itObject.getFromLocation();
            toStorageLocationId = itObject.getToLocation();
        }

        // Create new Record and save it to Inventory transactions table
        this.transactionDataSource.addNewSupplyReqItems(Integer.parseInt(supplyReqId), itRecords,
            fromStorageLoctionId, toStorageLocationId);

    }

    /** {@inheritDoc} */

    @Override
    public void updateSupplyReqItemWhenPartStoreLocationChange(
            final JSONArray supplyReqChangeRecords) {
        if (supplyReqChangeRecords.length() > 0) {

            for (int i = 0; i < supplyReqChangeRecords.length(); i++) {
                this.transactionDataSource.updateSupplyReqItemWhenPartStoreLocationChange(
                    supplyReqChangeRecords.getJSONObject(i));

            }

        }
    }

    /** {@inheritDoc} */

    @Override
    public void transferPartsBetweenStorageLocationBySupplyReq(final String supplyReqId,
            final String transId, final String fromStorageLocation, final String toStorageLocation,
            final String partCode, final Double transQty, final String status) {
        // If part not exists in To Storage Location , then save a new record to part storage
        // location.
        if (STATUS_RECEIVED.equals(status)) {
            this.partStorageLocDataSource.addNewPartToStoreLocIfNotExists(toStorageLocation,
                partCode);
        }

        // Update transaction status and update date and time
        this.transactionDataSource.updateTransactionStatus(Integer.parseInt(transId), status);

        // check to see if there are any remaining supply requisition items that are not
        // Received or Error

        if (this.transactionDataSource.findNotReceivedAndErrorItems(Integer.parseInt(supplyReqId))
            .isEmpty()) {
            final String supplyReqStatus = determineSupplyReqStatus(Integer.parseInt(supplyReqId));

            // update po status
            final SupplyRequisition supplyReqObject =
                    this.findSupplyRequisition(Integer.parseInt(supplyReqId));
            supplyReqObject.setStatus(supplyReqStatus);
            supplyReqObject.setLastUpdateBy(ContextStore.get().getUser().getName());
            update(supplyReqObject);
        }

        if (STATUS_RECEIVED.equals(status)) {
            // KB#3051919
            // update cost_unit_avg and cost total value of it table.
            this.transactionDataSource.updateCostValueAfterSupplyReqReceived(
                Integer.parseInt(transId), partCode, fromStorageLocation, transQty);

            // re-calculate average cost
            ResourceCalculations.updateAverageCostForTransfer(partCode, transQty,
                fromStorageLocation, toStorageLocation);

            // call WFR to transfer parts and calculate part count.
            final Double changeQty = transQty;
            if (changeQty > 0) {
                final ResourceCalculations resourceCalculate = new ResourceCalculations();
                resourceCalculate.updatePartsAndITForMPSL(partCode, changeQty, 0,
                    IT_TRANS_TYPE_VALUE_REQ_TRANSFER, fromStorageLocation, toStorageLocation, null);
            }

        }

    }

    /**
     * Determine supply requisition status.
     *
     * @param supplyReqId supply requisition code
     * @return status
     */
    private String determineSupplyReqStatus(final int supplyReqId) {
        String status = STATUS_RECEIVED;

        // Set po status to be Received or Partly Received.
        if (this.transactionDataSource.findErrorItems(supplyReqId).isEmpty()) {
            status = STATUS_RECEIVED;
        } else {

            status = this.transactionDataSource.findReceivedItems(supplyReqId).isEmpty()
                    ? STATUS_ERROR : STATUS_PARTIALLY_RECEIVED;

        }
        return status;
    }

    /** {@inheritDoc} */

    @Override
    public void transferPartsBetweenStorageLocationByAdjust(final String fromStorageLocation,
            final String toStorageLocation, final String partCode, final Double transQty,
            final Double qtyReserved, final JSONArray wrptRecords) {
        // If part not exists in To Storage Location , then save a new record to part storage
        // location.
        this.partStorageLocDataSource.addNewPartToStoreLocIfNotExists(toStorageLocation, partCode);

        // re-calculate average cost
        ResourceCalculations.updateAverageCostForTransfer(partCode, transQty, fromStorageLocation,
            toStorageLocation);

        // Change Quantity reserved if transfer quantity reserved count greater than 0
        if (qtyReserved > 0) {

            // Decrease from location quantity and increase to location quantity
            this.partStorageLocDataSource.transferReservedPartsBetweenLocation(partCode,
                fromStorageLocation, toStorageLocation, transQty);

            // For each selected Part Reservation record, update the value of wrpt.pt_store_loc_id
            // to match the To Storage Location field
            if (wrptRecords.length() > 0) {
                for (int i = 0; i < wrptRecords.length(); i++) {
                    final JSONObject wrptRecord = wrptRecords.getJSONObject(i);

                    final int wrId = wrptRecord.getInt(WRPT_TABLE + DOT + WR_ID);
                    String dateAssigned = null;
                    String timeAssigned = null;

                    dateAssigned = SqlUtils
                        .formatValueForSql(wrptRecord.get(WRPT_TABLE + DOT + DATE_ASSIGNED));
                    timeAssigned = SqlUtils
                        .formatValueForSql(wrptRecord.get(WRPT_TABLE + DOT + TIME_ASSIGNED));

                    final String whereSql = "part_id = '" + partCode + "' AND wr_id = '" + wrId
                            + "' AND date_assigned =${sql.date(" + dateAssigned
                            + ")} AND time_assigned  = ${sql.time(" + timeAssigned + ")} ";

                    final String updateWrptSql = "update wrpt set pt_store_loc_id='"
                            + toStorageLocation + "' where " + whereSql;

                    SqlUtils.executeUpdate(WRPT_TABLE, updateWrptSql);

                    // re-calculate work request cost based the to storage location
                    final String updateWrptCostSql =
                            " update wrpt SET cost_estimated = qty_estimated * (select pt_store_loc_pt.cost_unit_avg from pt_store_loc_pt where pt_store_loc_pt.part_id = wrpt.part_id and pt_store_loc_pt.pt_store_loc_id = wrpt.pt_store_loc_id) ,"
                                    + " cost_actual = qty_actual * (select pt_store_loc_pt.cost_unit_avg FROM pt_store_loc_pt where pt_store_loc_pt.part_id = wrpt.part_id and pt_store_loc_pt.pt_store_loc_id = wrpt.pt_store_loc_id)"
                                    + " where part_id = '" + partCode
                                    + "' and wrpt.pt_store_loc_id = '" + toStorageLocation
                                    + "' AND wr_id = " + wrId + " AND date_assigned = ${sql.date("
                                    + dateAssigned + ")} AND time_assigned = " + "${sql.time("
                                    + timeAssigned + ")}";

                    SqlUtils.executeUpdate(WRPT_TABLE, updateWrptCostSql);
                    final WorkRequestHandler wrHandler = new WorkRequestHandler();
                    final EventHandlerContext context = ContextStore.get().getEventHandlerContext();
                    wrHandler.recalculateCosts(context, wrId);
                    wrHandler.recalculateEstCosts(context, wrId);

                }
            }

        }
        // If the transfer quantity is greater than the sum of the selected reserved parts,
        // then transfer the remainder of the quantity of parts by using the same calculation as in
        // section 11.1.2
        final Double changeQty = transQty - qtyReserved;
        if (changeQty > 0) {
            final ResourceCalculations resourceCalculate = new ResourceCalculations();
            resourceCalculate.updatePartsAndITForMPSL(partCode, changeQty, 0,
                IT_TRANS_TYPE_VALUE_TRANSFER, fromStorageLocation, toStorageLocation, null);
        }

    }

    /** {@inheritDoc} */
    @Override
    public void updateSupplyReqInfor(final JSONArray supplyReqRecods) {
        for (int i = 0; i < supplyReqRecods.length(); i++) {
            final JSONObject supplyReqRecord = supplyReqRecods.getJSONObject(i);
            final int supplyReqId = supplyReqRecord.getInt(SUPPLY_REQ_TABLE + DOT + SUPPLY_REQ_ID);
            final SupplyRequisition supplyReq = this.findSupplyRequisition(supplyReqId);
            final String beforeStatus = supplyReq.getStatus();

            // set value to Supply reqsuition object from JSONObject
            supplyReq.setStatus(supplyReqRecord.getString(SUPPLY_REQ_TABLE + DOT + STATUS));
            supplyReq.setComments(supplyReqRecord.getString(SUPPLY_REQ_TABLE + DOT + COMMENTS));

            this.update(supplyReq);

            // update requisition item after storage location is changed.
            final String afterStatus = supplyReqRecord.getString(SUPPLY_REQ_TABLE + DOT + STATUS);
            final String beforeFromStoreLocId = supplyReqRecord.getString("beforeFromStoreLocId");
            final String beforeToStoreLocId = supplyReqRecord.getString("beforeToStoreLocId");
            final String afterFromStoreLocId =
                    supplyReqRecord.getString("supply_req.vfFromStorageLocation");
            final String afterToStoreLocId =
                    supplyReqRecord.getString("supply_req.vfToStorageLocation");

            final JSONArray reqChangeRecords = new JSONArray();
            final JSONObject record = new JSONObject();
            record.put("supply_req_id", supplyReqId);
            record.put("before_from_store_loc", beforeFromStoreLocId);
            record.put("new_from_store_loc", afterFromStoreLocId);
            record.put("before_to_store_loc", beforeToStoreLocId);
            record.put("new_to_store_loc", afterToStoreLocId);
            record.put("before_status", beforeStatus);
            record.put("new_status", afterStatus);

            reqChangeRecords.put(record);
            // update supply requisition item after storage location changed.
            updateSupplyReqItemWhenPartStoreLocationChange(reqChangeRecords);

        }
    }

    /** {@inheritDoc} */
    @Override
    public void saveSupplyReqItem(final JSONArray supplyReqItemRecords, final boolean isNew) {
        for (int i = 0; i < supplyReqItemRecords.length(); i++) {
            final JSONObject supplyReqItemRecord = supplyReqItemRecords.getJSONObject(i);
            Transaction supplyReqItemObject =
                    this.getTransactionObjectByJSONObject(supplyReqItemRecord, isNew);
            if (isNew) {
                supplyReqItemObject = this.transactionDataSource.save(supplyReqItemObject);
            } else {
                this.transactionDataSource.update(supplyReqItemObject);
            }

            // update field value if new status is 'Received' or 'Error'
            final String reqItemStatus =
                    supplyReqItemRecord.getString(IT_TABLE + DOT + REQ_ITEM_STATUS);
            if (STATUS_RECEIVED.equals(reqItemStatus) || STATUS_ERROR.equals(reqItemStatus)) {
                final int supplyReqId = supplyReqItemObject.getSupplyReqId();
                final int transId = supplyReqItemObject.getTransId();
                final String fromStoreLocId = supplyReqItemObject.getFromLocation();
                final String toStoreLocId = supplyReqItemObject.getToLocation();
                final String partCode = supplyReqItemObject.getPartId();
                final Double transQty = supplyReqItemObject.getTransQuantity();

                this.transferPartsBetweenStorageLocationBySupplyReq(String.valueOf(supplyReqId),
                    String.valueOf(transId), fromStoreLocId, toStoreLocId, partCode, transQty,
                    reqItemStatus);

            }
        }
    }

    /**
     * Get Transaction object based on JSONObject.
     *
     * @param supplyReqItemRecord Supply requisition JSON object
     * @param isNew Flag to save or update record.
     * @return Transaction Object
     */
    public Transaction getTransactionObjectByJSONObject(final JSONObject supplyReqItemRecord,
            final boolean isNew) {
        Transaction transactionObject;
        final int supplyReqId = supplyReqItemRecord.getInt(IT_TABLE + DOT + SUPPLY_REQ_ID);
        if (isNew) {
            transactionObject = new Transaction();
            transactionObject.setSupplyReqId(supplyReqId);
            transactionObject.setTransDate(Utility.currentDate());
            transactionObject.setTransTime(Utility.currentTime());
            transactionObject.setPerformedBy(ContextStore.get().getUser().getName());
        } else {
            final int transId = supplyReqItemRecord.getInt(IT_TABLE + DOT + TRANS_ID);
            transactionObject = this.transactionDataSource.findTransaction(transId);
        }

        transactionObject
            .setReqItemStatus(supplyReqItemRecord.getString(IT_TABLE + DOT + REQ_ITEM_STATUS));
        transactionObject.setPartId(supplyReqItemRecord.getString(IT_TABLE + DOT + PART_ID));
        transactionObject
            .setTransQuantity(supplyReqItemRecord.getDouble(IT_TABLE + DOT + TRANS_QUANTITY));
        transactionObject
            .setFromLocation(supplyReqItemRecord.getString(IT_TABLE + DOT + PT_STORE_LOC_FROM));
        transactionObject
            .setToLocation(supplyReqItemRecord.getString(IT_TABLE + DOT + PT_STORE_LOC_TO));
        transactionObject.setComments(supplyReqItemRecord.getString(IT_TABLE + DOT + COMMENTS));

        return transactionObject;
    }

    /**
     * Setter for the transactionDataSource property.
     *
     * @see transactionDataSource
     * @param transactionDataSource the transactionDataSource to set
     */

    public void setTransactionDataSource(final ITransactionDao<Transaction> transactionDataSource) {
        this.transactionDataSource = transactionDataSource;
    }

    /**
     * Setter for the partStorageLocDataSource property.
     *
     * @see partStorageLocDataSource
     * @param partStorageLocDataSource the partStorageLocationDataSource to set
     */

    public void setPartStorageLocDataSource(
            final IPartStorageLocationDao<PartStorageLocation> partStorageLocDataSource) {
        this.partStorageLocDataSource = partStorageLocDataSource;
    }

}
