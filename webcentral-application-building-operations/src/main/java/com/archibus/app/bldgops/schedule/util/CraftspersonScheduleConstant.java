package com.archibus.app.bldgops.schedule.util;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides constant fields for craftsperson schedules.
 *
 * @author Jia
 * @since 23.2
 *
 */
public final class CraftspersonScheduleConstant {

	private CraftspersonScheduleConstant() {
	}

	/**
	 * Employee table name.
	 */
	public static final String EM_TABLE = "em";
	/**
	 * Craftsperson table name.
	 */
	public static final String CF_TABLE = "cf";
	/**
	 * Table name of Craftsperson Schedule.
	 */
	public static final String CF_SCHEDULES_TABLE = "cf_schedule";

	/**
	 * TABLE name of Craftsperson Schedule Variances.
	 */
	public static final String CF_SCHEDULES_VARIANCES_TABLE = "cf_schedules_variances";

	/**
	 * Table name of WRCF.
	 */
	public static final String WRCF_TABLE = "wrcf";
	/**
	 * Field name for Work Request Code.
	 */
	public static final String WR_ID = "wr_id";

	/**
	 * Field name of Employee Code.
	 */
	public static final String EM_ID = "em_id";

	/**
	 * Field name of Email.
	 */
	public static final String EMAIL = "email";

	/**
	 * Field name of Craftperson Code.
	 */
	public static final String CF_ID = "cf_id";

	/**
	 * Field name of Craftsperson Scheduled Variance Id.
	 */
	public static final String CF_SCHED_VARS_ID = "cf_sched_vars_id";

	/**
	 * Field name of Date Start.
	 */
	public static final String DATE_START = "date_start";

	/**
	 * Field name of Date End.
	 */
	public static final String DATE_END = "date_end";

	/**
	 * Field name of Description.
	 */
	public static final String DESCRIPTION = "description";

	/**
	 * Field name of Scheduled Variance Type.
	 */
	public static final String SCHED_VARS_TYPE = "sched_vars_type";

	/**
	 * Field name of Time Start.
	 */
	public static final String TIME_START = "time_start";

	/**
	 * Field name of Time End.
	 */
	public static final String TIME_END = "time_end";

	/**
	 * Field name of Site Code.
	 */
	public static final String SITE_ID = "site_id";

	/**
	 * Field name of Building Code.
	 */
	public static final String BL_ID = "bl_id";
}
