/**
 * This package contains datasource objects for Craftsperson Schedule.
 **/
package com.archibus.app.bldgops.schedule.dao;
