package com.archibus.app.bldgops.partinv.dao.datasource;

import static com.archibus.app.bldgops.partinv.util.PartInventoryConstant.*;

import java.util.*;

import org.json.*;

import com.archibus.app.bldgops.partinv.dao.ITransactionDao;
import com.archibus.app.bldgops.partinv.domain.Transaction;
import com.archibus.context.ContextStore;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecordField;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.ParsedRestrictionDef;
import com.archibus.utility.Utility;

/**
 *
 * Transaction datasource.
 * <p>
 *
 *
 * @author Guo Jiangtao
 * @since 23.2
 *
 */
public class TransactionDataSource extends ObjectDataSourceImpl<Transaction>
        implements ITransactionDao<Transaction> {

    /**
     * Field names to property names mapping. All fields will be added to the DataSource.
     */
    private static final String[][] FIELDS_TO_PROPERTIES =
            { { TRANS_ID, "transId" }, { PART_ID, "partId" }, { REQ_ITEM_STATUS, "reqItemStatus" },
                    { SUPPLY_REQ_ID, "supplyReqId" }, { PT_STORE_LOC_FROM, "fromLocation" },
                    { PT_STORE_LOC_TO, "toLocation" }, { TRANS_DATE, "transDate" },
                    { TRANS_TIME, "transTime" }, { TRANS_TYPE, "transType" },
                    { PERFORMED_BY, "performedBy" }, { TRANS_QUANTITY, "transQuantity" },
                    { COMMENTS, "comments" }, { "cost_when_used", "costWhenUsed" },
                    { "cost_total", "costTotal" } };

    /**
     * Constructs TransactionDataSource, mapped to <code>it</code> table, using
     * <code>transaction</code> bean.
     *
     */
    public TransactionDataSource() {
        super("transaction", "it");
    }

    /** {@inheritDoc} */

    @Override
    public Transaction findTransaction(final int transId) {
        final PrimaryKeysValues primaryKeysValues = new PrimaryKeysValues();
        {
            final DataRecordField pkField = new DataRecordField();
            pkField.setName(IT_TABLE + DOT + TRANS_ID);
            pkField.setValue(transId);
            primaryKeysValues.getFieldsValues().add(pkField);
        }

        return this.get(primaryKeysValues);
    }

    /** {@inheritDoc} */

    @Override
    public Transaction findTransactionBySupplyReq(final int supplyReqId) {
        final ParsedRestrictionDef resDef = new ParsedRestrictionDef();
        resDef.addClause(IT_TABLE, SUPPLY_REQ_ID, supplyReqId, Operation.EQUALS);
        final List<Transaction> its = this.find(resDef);

        Transaction itObject = null;
        if (!its.isEmpty()) {
            itObject = its.get(0);
        }

        return itObject;
    }

    /** {@inheritDoc} */

    @Override
    public List<Transaction> findNotReceivedAndErrorItems(final int supplyReqId) {
        final ParsedRestrictionDef resDef = new ParsedRestrictionDef();
        resDef.addClause(IT_TABLE, SUPPLY_REQ_ID, supplyReqId, Operation.EQUALS);
        resDef.addClause(IT_TABLE, REQ_ITEM_STATUS,
            Arrays.asList(new String[] { STATUS_RECEIVED, STATUS_ERROR }), Operation.NOT_IN);

        return this.find(resDef);
    }

    /** {@inheritDoc} */

    @Override
    public List<Transaction> findReceivedItems(final int supplyReqId) {

        final ParsedRestrictionDef resDef = new ParsedRestrictionDef();
        resDef.addClause(IT_TABLE, SUPPLY_REQ_ID, supplyReqId, Operation.EQUALS);
        resDef.addClause(IT_TABLE, REQ_ITEM_STATUS, STATUS_RECEIVED, Operation.EQUALS);

        return this.find(resDef);
    }

    /** {@inheritDoc} */

    @Override
    public List<Transaction> findErrorItems(final int supplyReqId) {
        final ParsedRestrictionDef resDef = new ParsedRestrictionDef();
        resDef.addClause(IT_TABLE, SUPPLY_REQ_ID, supplyReqId, Operation.EQUALS);
        resDef.addClause(IT_TABLE, REQ_ITEM_STATUS, STATUS_ERROR, Operation.EQUALS);

        return this.find(resDef);
    }

    /** {@inheritDoc} */

    @Override
    public void addNewSupplyReqItems(final int supplyReqId, final JSONArray itRecords,
            final String fromStoreLoc, final String toStoreLoc) {
        // Save new inventory transaction record by supply requisition code
        for (int i = 0; i < itRecords.length(); i++) {

            final Transaction itObject = new Transaction();
            final JSONObject itRecord = itRecords.getJSONObject(i);

            itObject.setSupplyReqId(supplyReqId);
            itObject.setPartId(itRecord.getString(IT_TABLE + DOT + PART_ID));
            itObject.setTransQuantity(
                Double.parseDouble(itRecord.getString(IT_TABLE + DOT + TRANS_QUANTITY)));
            itObject.setFromLocation(fromStoreLoc);
            itObject.setToLocation(toStoreLoc);
            itObject.setTransType(IT_TRANS_TYPE_VALUE_TRANSFER);
            itObject.setTransDate(Utility.currentDate());
            itObject.setTransTime(Utility.currentTime());
            itObject.setPerformedBy(ContextStore.get().getUser().getName());
            itObject.setComments(itRecord.getString(IT_TABLE + DOT + COMMENTS));
            save(itObject);

        }

    }

    /** {@inheritDoc} */

    @Override
    public void updateCostValueAfterPoReceived(final int transId, final String poId,
            final String poLineId, final Double transQty, final Double costUnit,
            final String toStorageLocation, final String partCode) {

        // (1)Cost per Unit when used = Unit Cost from PO Line Item + (Shipping and Taxes from PO
        // Line Item / Transaction Quantity)
        // (2)The Total Cost is still calculated by multiplying (Cost per Unit when used) by
        // (Transaction Quantity),
        final Transaction item = this.findTransaction(transId);
        item.setCostWhenUsed(costUnit);
        item.setCostTotal(costUnit * transQty);
        update(item);

    }

    /** {@inheritDoc} */

    @Override
    public void updateCostValueAfterSupplyReqReceived(final int transId, final String partCode,
            final String fromLocation, final Double transQty) {
        String updateCostSql = "";
        if (SqlUtils.isOracle()) {
            updateCostSql =
                    "update it set cost_when_used=(select  NVL(sum(cost_unit_avg),0) from pt_store_loc_pt where pt_store_loc_id='"
                            + fromLocation + "' and      part_id='" + partCode
                            + "'),cost_total=(select NVL(sum(cost_unit_avg),0)*" + transQty
                            + " from  pt_store_loc_pt where pt_store_loc_id='" + fromLocation
                            + "' and   part_id='" + partCode + "') where  trans_id=" + transId;
        } else {
            updateCostSql =
                    "update it set cost_when_used=(select isnull(sum(cost_unit_avg),0) from pt_store_loc_pt where pt_store_loc_id='"
                            + fromLocation + "' and     part_id='" + partCode
                            + "'),cost_total=(select isnull(sum(cost_unit_avg),0)*" + transQty
                            + " from pt_store_loc_pt where pt_store_loc_id='" + fromLocation
                            + "' and  part_id='" + partCode + "') where trans_id=" + transId;
        }

        SqlUtils.executeUpdate(IT_TABLE, updateCostSql);
    }

    /** {@inheritDoc} */

    @Override
    public void updateSupplyReqItemWhenPartStoreLocationChange(
            final JSONObject supplyReqChangeRecord) {
        final int supplyRequisitionId = supplyReqChangeRecord.getInt(SUPPLY_REQ_ID);
        supplyReqChangeRecord.getString("before_from_store_loc");
        final String fromStoreLocNew = supplyReqChangeRecord.getString("new_from_store_loc");
        supplyReqChangeRecord.getString("before_to_store_loc");
        final String toStoreLocNew = supplyReqChangeRecord.getString("new_to_store_loc");

        supplyReqChangeRecord.getString("before_status");
        final String newStatus = supplyReqChangeRecord.getString("new_status");
        String updateSql = "update it set ";

        updateSql += " pt_store_loc_from='" + fromStoreLocNew + "',";

        updateSql += " pt_store_loc_to='" + toStoreLocNew + "',   ";

        updateSql += " req_item_status='" + newStatus + "'                        ";

        updateSql += " where it.supply_req_id='" + supplyRequisitionId
                + "' and it.req_item_status!='Received' and  it.req_item_status!='Error'";

        SqlUtils.executeUpdate(IT_TABLE, updateSql);

    }

    /** {@inheritDoc} */

    @Override
    public void updateTransactionStatus(final int transId, final String status) {
        final Transaction item = this.findTransaction(transId);
        item.setReqItemStatus(status);
        item.setTransDate(Utility.currentDate());
        item.setTransTime(Utility.currentTime());
        update(item);
    }

    @Override
    protected String[][] getFieldsToProperties() {
        return FIELDS_TO_PROPERTIES.clone();
    }

}
