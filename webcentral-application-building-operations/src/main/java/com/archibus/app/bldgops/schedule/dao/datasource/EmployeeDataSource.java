package com.archibus.app.bldgops.schedule.dao.datasource;

import static com.archibus.app.bldgops.schedule.util.CraftspersonScheduleConstant.*;

import java.util.List;

import com.archibus.app.bldgops.schedule.dao.IEmployeeDao;
import com.archibus.app.bldgops.schedule.domain.Employee;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecordField;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.ParsedRestrictionDef;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides methods for craftsperson schedule to handle with employee.
 *
 * @author Jia
 * @since 23.2
 *
 */
public class EmployeeDataSource extends ObjectDataSourceImpl<Employee> implements IEmployeeDao<Employee> {
	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { EM_ID, "emId" }, { EMAIL, "email" }, { BL_ID, "blId" } };

	@Override
	protected String[][] getFieldsToProperties() {
		// TODO Auto-generated method stub
		return this.FIELDS_TO_PROPERTIES.clone();
	}

	/**
	 * Constructor.
	 */
	public EmployeeDataSource() {
		super("scheduleEmpoyee", EM_TABLE);
	}

	/** {@inheritDoc} */
	@Override
	public Employee getEmployee(final String emId) {
		final PrimaryKeysValues primaryKeysValues = new PrimaryKeysValues();
		{
			final DataRecordField pkField = new DataRecordField();
			pkField.setName(EM_TABLE + DOT + EM_ID);
			pkField.setValue(emId);
			primaryKeysValues.getFieldsValues().add(pkField);
		}

		return this.get(primaryKeysValues);
	}

	/** {@inheritDoc} */
	@Override
	public List<Employee> getEmployeesByEmail(final String email) {
		final ParsedRestrictionDef resDef = new ParsedRestrictionDef();
		resDef.addClause(EM_TABLE, EMAIL, email, Operation.EQUALS);
		return this.find(resDef);
	}

}
