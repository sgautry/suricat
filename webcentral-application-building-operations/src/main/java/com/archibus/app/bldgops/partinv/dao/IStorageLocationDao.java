package com.archibus.app.bldgops.partinv.dao;

import com.archibus.core.dao.IDao;

/**
 *
 * Provides methods to load Storage Location.
 * <p>
 *
 * @author Guo Jiangtao
 * @since 23.2
 *
 * @param <StorageLocation> type of object
 */
public interface IStorageLocationDao<StorageLocation> extends IDao<StorageLocation> {
    /**
     * Get StorageLocation by location code.
     *
     * @param locId location code
     * @return StorageLocation object
     */
    StorageLocation findLocation(final String locId);

}
