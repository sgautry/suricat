package com.archibus.app.bldgops.partinv.domain;

import java.util.Date;

/**
 * Domain object for wrpt.
 * <p>
 *
 * @author Guo Jiangtao
 * @since 23.2
 *
 */
public class WorkRequestPart {

    /** The work request code. */
    private int wrId;

    /** The part code. */
    private String partId;

    /** The date assigned. */
    private Date dateAssigned;

    /** The time assigned. */
    private Date timeAssigned;

    /** The wrpt status. */
    private String status;

    /** The quantity estimated. */
    private double qtyEstimated;

    /**
     * Getter for the partId property.
     *
     * @see partId
     * @return the partId property.
     */
    public String getPartId() {
        return this.partId;
    }

    /**
     * Setter for the partId property.
     *
     * @see partId
     * @param partId the partId to set
     */

    public void setPartId(final String partId) {
        this.partId = partId;
    }

    /**
     * Getter for the wrId property.
     *
     * @see wrId
     * @return the wrId property.
     */
    public int getWrId() {
        return this.wrId;
    }

    /**
     * Setter for the wrId property.
     *
     * @see wrId
     * @param wrId the wrId to set
     */

    public void setWrId(final int wrId) {
        this.wrId = wrId;
    }

    /**
     * Getter for the dateAssigned property.
     *
     * @see dateAssigned
     * @return the dateAssigned property.
     */
    public Date getDateAssigned() {
        return this.dateAssigned;
    }

    /**
     * Setter for the dateAssigned property.
     *
     * @see dateAssigned
     * @param dateAssigned the dateAssigned to set
     */

    public void setDateAssigned(final Date dateAssigned) {
        this.dateAssigned = dateAssigned;
    }

    /**
     * Getter for the timeAssigned property.
     *
     * @see timeAssigned
     * @return the timeAssigned property.
     */
    public Date getTimeAssigned() {
        return this.timeAssigned;
    }

    /**
     * Setter for the timeAssigned property.
     *
     * @see timeAssigned
     * @param timeAssigned the timeAssigned to set
     */

    public void setTimeAssigned(final Date timeAssigned) {
        this.timeAssigned = timeAssigned;
    }

    /**
     * Getter for the status property.
     *
     * @see status
     * @return the status property.
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * Setter for the status property.
     *
     * @see status
     * @param status the status to set
     */

    public void setStatus(final String status) {
        this.status = status;
    }

    /**
     * Getter for the qtyEstimated property.
     *
     * @see qtyEstimated
     * @return the qtyEstimated property.
     */
    public double getQtyEstimated() {
        return this.qtyEstimated;
    }

    /**
     * Setter for the qtyEstimated property.
     *
     * @see qtyEstimated
     * @param qtyEstimated the qtyEstimated to set
     */

    public void setQtyEstimated(final double qtyEstimated) {
        this.qtyEstimated = qtyEstimated;
    }

}
