package com.archibus.app.bldgops.partinv.dao;

import java.util.List;

import org.json.JSONArray;

import com.archibus.core.dao.IDao;

/**
 *
 * Provides methods to load po_line records.
 * <p>
 *
 * @author Guo Jiangtao,Jia
 * @since 23.2
 *
 * @param <PurchaseOrderLineItem> type of object
 */
public interface IPurchaseOrderLineItemDao<PurchaseOrderLineItem>
        extends IDao<PurchaseOrderLineItem> {

    /**
     * Get Purchase Order Line Item by ids.
     *
     * @param poId purchase order code
     * @param poLineId purchase order line code
     * @return PurchaseOrderLineItem object
     */
    PurchaseOrderLineItem findPurchaseOrderLineItem(final int poId, final int poLineId);

    /**
     * Get Purchase Order Line item that not received and not error.
     *
     * @param poId purchase order code
     * @return PurchaseOrderLineItem list
     */
    List<PurchaseOrderLineItem> findNotReceivedAndErrorItems(final int poId);

    /**
     * Get Purchase Order Line item that received .
     *
     * @param poId purchase order code
     * @return PurchaseOrderLineItem list
     */
    List<PurchaseOrderLineItem> findReceivedItems(final int poId);

    /**
     * Get Purchase Order Line item that error .
     *
     * @param poId purchase order code
     * @return PurchaseOrderLineItem list
     */
    List<PurchaseOrderLineItem> findErrorItems(final int poId);

    /**
     * Check whether purchase order has line item.
     *
     * @param poId Purchase Order code
     * @return {Boolean} If it has line item,then true,else false.
     */
    List<PurchaseOrderLineItem> findAllItems(final int poId);

    /**
     * Update po line item status.
     *
     * @param poId purchase order code
     * @param poLineId purchase order line code
     * @param status purchase order line status
     */
    void updatePoLineStatus(final int poId, final int poLineId, final String status);

    /**
     * Update po line items status.
     *
     * @param poId purchase order code
     * @param status purchase order line status
     */
    void updatePoLineStatus(final int poId, final String status);

    /**
     * Add new po line item .
     *
     * @param poLines po line item json array
     * @param poId purchase order code
     * @param vnId vendor code
     * @param poEmId employee code
     */
    void addNewPoLineItem(final JSONArray poLines, final int poId, final String vnId,
            final String poEmId);
}
