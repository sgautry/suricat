/**
 * This package contains domain objects for Part Inventory Management.
 **/
package com.archibus.app.bldgops.partinv.domain;
