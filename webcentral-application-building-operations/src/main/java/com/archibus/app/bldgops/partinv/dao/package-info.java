/**
 * This package contains DAO objects for Part Inventory Management.
 **/
package com.archibus.app.bldgops.partinv.dao;
