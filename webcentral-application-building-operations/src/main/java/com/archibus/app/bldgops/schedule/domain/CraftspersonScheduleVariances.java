package com.archibus.app.bldgops.schedule.domain;

import java.util.Date;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for Craftsperson Scheduled Variances.
 *
 * @author Jia
 * @since 23.2
 *
 */
public class CraftspersonScheduleVariances {

	/**
	 * Craftsperson Schedules Variances ID.
	 */
	private int cfSchedVarsId;

	/**
	 * Start Date.
	 */
	private Date dateStart;
	/**
	 * End Date.
	 */
	private Date dateEnd;
	/**
	 * Description.
	 */
	private String description;
	/**
	 * Schedules Variance Type.
	 */
	private String schedVarsType;
	/**
	 * Site Code.
	 */
	private String siteId;
	/**
	 * Building Code.
	 */
	private String blId;
	/**
	 * Craftsperson Code.
	 */
	private String cfId;
	/**
	 * End Time.
	 */
	private Date timeEnd;
	/**
	 * Start Time.
	 */
	private Date timeStart;

	/**
	 * Getter for the blId property.
	 *
	 * @return the blId property.
	 */
	public String getBlId() {
		return this.blId;
	}

	/**
	 * Setter for the blId property.
	 *
	 * @param blId
	 *            the blId to set.
	 */
	public void setBlId(final String blId) {
		this.blId = blId;
	}

	/**
	 * Getter for the cfId property.
	 *
	 * @return the cfId property.
	 */
	public String getCfId() {
		return this.cfId;
	}

	/**
	 * Setter for the cfId property.
	 *
	 * @param cfId
	 *            the cfId to set.
	 */
	public void setCfId(final String cfId) {
		this.cfId = cfId;
	}

	/**
	 * Getter for the cfSchedVarsId property.
	 *
	 * @return the cfSchedVarsId property.
	 */
	public int getCfSchedVarsId() {
		return this.cfSchedVarsId;
	}

	/**
	 * Setter for the cfSchedVarsId property.
	 *
	 * @param cfSchedVarsId
	 *            the cfSchedVarsId to set.
	 */
	public void setCfSchedVarsId(final int cfSchedVarsId) {
		this.cfSchedVarsId = cfSchedVarsId;
	}

	/**
	 * Getter for the dateEnd property.
	 *
	 * @return the dateEnd property.
	 */
	public Date getDateEnd() {
		return this.dateEnd;
	}

	/**
	 * Setter for the dateEnd property.
	 *
	 * @param dateEnd
	 *            the dateEnd to set.
	 */
	public void setDateEnd(final Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	/**
	 * Getter for the dateStart property.
	 *
	 * @return the dateStart property.
	 */
	public Date getDateStart() {
		return this.dateStart;
	}

	/**
	 * Setter for the dateStart property.
	 *
	 * @param dateStart
	 *            the dateStart to set.
	 */
	public void setDateStart(final Date dateStart) {
		this.dateStart = dateStart;
	}

	/**
	 * Getter for the description property.
	 *
	 * @return the description property.
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Setter for the description property.
	 *
	 * @param description
	 *            the description to set.
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Getter for the schedVarsType property.
	 *
	 * @return the schedVarsType property.
	 */
	public String getSchedVarsType() {
		return this.schedVarsType;
	}

	/**
	 * Setter for the schedVarsType property.
	 *
	 * @param schedVarsType
	 *            the schedVarsType to set.
	 */
	public void setSchedVarsType(final String schedVarsType) {
		this.schedVarsType = schedVarsType;
	}

	/**
	 * Getter for the siteId property.
	 *
	 * @return the siteId property.
	 */
	public String getSiteId() {
		return this.siteId;
	}

	/**
	 * Setter for the siteId property.
	 *
	 * @param siteId
	 *            the siteId to set.
	 */
	public void setSiteId(final String siteId) {
		this.siteId = siteId;
	}

	/**
	 * Getter for the timeEnd property.
	 *
	 * @return the timeEnd property.
	 */
	public Date getTimeEnd() {
		return this.timeEnd;
	}

	/**
	 * Setter for the timeEnd property.
	 *
	 * @param timeEnd
	 *            the timeEnd to set.
	 */
	public void setTimeEnd(final Date timeEnd) {
		this.timeEnd = timeEnd;
	}

	/**
	 * Getter for the timeStart property.
	 *
	 * @return the timeStart property.
	 */
	public Date getTimeStart() {
		return this.timeStart;
	}

	/**
	 * Setter for the timeStart property.
	 *
	 * @param timeStart
	 *            the timeStart to set.
	 */
	public void setTimeStart(final Date timeStart) {
		this.timeStart = timeStart;
	}

}
