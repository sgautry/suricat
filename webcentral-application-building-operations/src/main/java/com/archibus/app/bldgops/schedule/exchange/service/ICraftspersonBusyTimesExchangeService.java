package com.archibus.app.bldgops.schedule.exchange.service;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides methods for craftsperson to retrieve busy times from exchange
 * server.
 * <p>
 *
 * Managed by Spring,configured in
 * /webcentral-application-building-operations/src/main/resources/com/archibus/
 * app/bldgops/schedule/exchange/exchange-integration-context.xml
 *
 * Called by scheduled workflow rule:
 * AbBldgOpsOnDemandWork-getCFBusyTimesExchange-retrieveCFBusyTimes
 *
 * @author Jia
 * @since 23.2
 *
 */
public interface ICraftspersonBusyTimesExchangeService {
	/**
	 * Retrieve busy times from exchange server.
	 *
	 * @throws Exception
	 *             -Exceptions when get user availability.
	 */
	void retrieveCFBusyTimes() throws Exception;
}
