package com.archibus.app.bldgops.partinv.domain;

/**
 * Domain object for pt_store_loc.
 * <p>
 *
 * @author Guo Jiangtao
 * @since 23.2
 *
 */
public class StorageLocation {

    /** The storage location id. */
    private String locId;

    /** The storage location site code. */
    private String siteId;

    /** The storage location building code. */
    private String blId;

    /** The storage location floor code. */
    private String flId;

    /** The storage location room code. */
    private String rmId;

    /**
     * Getter for the locId property.
     *
     * @see locId
     * @return the locId property.
     */
    public String getLocId() {
        return this.locId;
    }

    /**
     * Setter for the locId property.
     *
     * @see locId
     * @param locId the locId to set
     */

    public void setLocId(final String locId) {
        this.locId = locId;
    }

    /**
     * Getter for the siteId property.
     *
     * @see siteId
     * @return the siteId property.
     */
    public String getSiteId() {
        return this.siteId;
    }

    /**
     * Setter for the siteId property.
     *
     * @see siteId
     * @param siteId the siteId to set
     */

    public void setSiteId(final String siteId) {
        this.siteId = siteId;
    }

    /**
     * Getter for the blId property.
     *
     * @see blId
     * @return the blId property.
     */
    public String getBlId() {
        return this.blId;
    }

    /**
     * Setter for the blId property.
     *
     * @see blId
     * @param blId the blId to set
     */

    public void setBlId(final String blId) {
        this.blId = blId;
    }

    /**
     * Getter for the flId property.
     *
     * @see flId
     * @return the flId property.
     */
    public String getFlId() {
        return this.flId;
    }

    /**
     * Setter for the flId property.
     *
     * @see flId
     * @param flId the flId to set
     */

    public void setFlId(final String flId) {
        this.flId = flId;
    }

    /**
     * Getter for the rmId property.
     *
     * @see rmId
     * @return the rmId property.
     */
    public String getRmId() {
        return this.rmId;
    }

    /**
     * Setter for the rmId property.
     *
     * @see rmId
     * @param rmId the rmId to set
     */

    public void setRmId(final String rmId) {
        this.rmId = rmId;
    }

}
