package com.archibus.app.bldgops.partinv.dao.datasource;

import static com.archibus.app.bldgops.partinv.util.PartInventoryConstant.*;

import java.util.List;

import org.json.*;

import com.archibus.app.bldgops.partinv.dao.*;
import com.archibus.app.bldgops.partinv.domain.*;
import com.archibus.app.bldgops.partinv.util.PartInventoryUtility;
import com.archibus.context.ContextStore;
import com.archibus.datasource.*;
import com.archibus.datasource.data.*;
import com.archibus.eventhandler.resourcecalcs.ResourceCalculations;
import com.archibus.utility.Utility;

/**
 *
 * Part Storage Location datasource.
 * <p>
 *
 *
 * @author Guo Jiangtao
 * @since 23.2
 *
 */
public class PartStorageLocationDataSource extends ObjectDataSourceImpl<PartStorageLocation>
        implements IPartStorageLocationDao<PartStorageLocation> {

    /**
     * Field names to property names mapping. All fields will be added to the DataSource.
     */
    private static final String[][] FIELDS_TO_PROPERTIES = { { PART_ID, "partId" },
            { PT_STORE_LOC_ID, "locId" }, { "qty_on_hand", "qtyOnHand" },
            { "qty_on_reserve", "qtyOnReserve" }, { "qty_physical_count", "qtyPhysicalCount" },
            { "qty_to_order", "qtyToOrder" }, { "qty_min_hand", "qtyMinHand" }, { "bl_id", "blId" },
            { "fl_id", "flId" }, { "rm_id", "rmId" }, { "date_of_last_cnt", "dateOfLastCnt" } };

    /**
     * work reuqest part dataSource.
     */
    private IWorkRequestPartDao<WorkRequestPart> workRequestPartDataSource;

    /**
     * part dataSource.
     */
    private IPartDao<Part> partDataSource;

    /**
     * part dataSource.
     */
    private IStorageLocationDao<StorageLocation> storageLocationDataSource;

    /**
     * part transaction dataSource.
     */
    private ITransactionDao<Transaction> transactionDataSource;

    /**
     * Constructs PartStorageLocationDataSource, mapped to <code>pt_store_loc_pt</code> table, using
     * <code>partStorageLoc</code> bean.
     *
     */
    public PartStorageLocationDataSource() {
        super("partStorageLoc", PT_STORE_LOC_PT_TABLE);
    }

    @Override
    protected String[][] getFieldsToProperties() {
        return FIELDS_TO_PROPERTIES.clone();
    }

    /** {@inheritDoc} */

    @Override
    public PartStorageLocation getPartLoc(final String partId, final String locId) {

        final PrimaryKeysValues primaryKeysValues = new PrimaryKeysValues();
        {
            final DataRecordField pkField = new DataRecordField();
            pkField.setName(PT_STORE_LOC_PT_TABLE + DOT + PART_ID);
            pkField.setValue(partId);
            primaryKeysValues.getFieldsValues().add(pkField);
        }

        {
            final DataRecordField pkField = new DataRecordField();
            pkField.setName(PT_STORE_LOC_PT_TABLE + DOT + PT_STORE_LOC_ID);
            pkField.setValue(locId);
            primaryKeysValues.getFieldsValues().add(pkField);
        }

        return this.get(primaryKeysValues);
    }

    /** {@inheritDoc} */

    @Override
    public void updateWrptStatus(final String partId, final String locId, final double quantity) {
        if (quantity > 0) {

            this.setWrptStatusForQuantityIncrease(partId, locId, quantity);

        } else if (quantity < 0) {

            this.setWrptStatusForQuantityDecrease(partId, locId, quantity);
        }

        this.calculateReservedQuantityOfPart(partId, locId);

    }

    /** {@inheritDoc} */

    @Override
    public void removePartLocations(final String ptLocId) {
        SqlUtils.executeUpdate("pt_store_loc_pt",
            "update pt_store_loc_pt "
                    + "set bl_id = null, fl_id = null, rm_id = null, aisle_id = null,"
                    + " cabinet_id = null, shelf_id = null, bin_id = null"
                    + " where pt_store_loc_id='" + SqlUtils.makeLiteralOrBlank(ptLocId) + "'");

    }

    /**
     *
     * Update estimated parts status when available quantity increases.
     *
     * @param partId Part code.
     * @param locId location code.
     * @param quantity double adjusted part quantity.
     */
    public void setWrptStatusForQuantityIncrease(final String partId, final String locId,
            final double quantity) {

        // Query 'Not In Stock' estimated parts
        final List<WorkRequestPart> estimatedParts = this.workRequestPartDataSource
            .findEstimatedParts(partId, locId, new String[] { NOT_IN_STOCK });

        // Query to get current Part object
        final Part part = this.partDataSource.getPart(partId);

        // Query to get current PartStorageLocation object
        final PartStorageLocation partLoc = getPartLoc(partId, locId);

        // calculate new available quantity
        final double newAvailableQuantity = partLoc.getQtyOnHand() + quantity;

        // Loop all estimated parts to check if the quantity estimated is less than or equal to the
        // part's new Quantity Available,then change the status of the estimate to "In Stock, not
        // Reserved"
        for (final WorkRequestPart estimatePart : estimatedParts) {

            final double estimatedQuantity = estimatePart.getQtyEstimated();
            if (newAvailableQuantity >= estimatedQuantity) {

                estimatePart.setStatus(NOT_RESERVED);
                this.workRequestPartDataSource.update(estimatePart);
            }
        }

        part.setQtyOnHand(part.getQtyOnHand() + quantity);
        this.partDataSource.update(part);

        partLoc.setQtyOnHand(newAvailableQuantity);
        update(partLoc);
    }

    /**
     *
     * Update estimated parts status when available quantity decreases.
     *
     * @param partId Part code.
     * @param locId location code.
     * @param quantity double adjusted part quantity.
     */
    public void setWrptStatusForQuantityDecrease(final String partId, final String locId,
            final double quantity) {

        // Query 'In Stock, Not Reserved' estimated parts
        final List<WorkRequestPart> estimatedParts = this.workRequestPartDataSource
            .findEstimatedParts(partId, locId, new String[] { NOT_RESERVED });

        // Query to get current Part object
        final Part part = this.partDataSource.getPart(partId);

        // Query to get current PartStorageLocation object
        final PartStorageLocation partLoc = getPartLoc(partId, locId);

        // calculate new available quantity
        final double newAvailableQuantity = partLoc.getQtyOnHand() + quantity;

        // Loop all estimated parts to check if the quantity estimated is greater than the
        // part's new Quantity Available,then change the status of the estimate to "Not In Stock"
        for (final WorkRequestPart estimatePart : estimatedParts) {

            final double estimatedQuantity = estimatePart.getQtyEstimated();
            if (newAvailableQuantity < estimatedQuantity) {

                estimatePart.setStatus(NOT_IN_STOCK);
                this.workRequestPartDataSource.update(estimatePart);
            }
        }

        part.setQtyOnHand(part.getQtyOnHand() + quantity);
        this.partDataSource.update(part);

        partLoc.setQtyOnHand(newAvailableQuantity);
        update(partLoc);
    }

    /**
     *
     * Calculate part's reserved quantity after all part estimations update.
     *
     * @param partId Part code.
     * @param locId location code.
     */
    private void calculateReservedQuantityOfPart(final String partId, final String locId) {

        // Query to get all estimation of given part
        final List<WorkRequestPart> allEstimatedParts =
                this.workRequestPartDataSource.findEstimatedParts(partId);

        // Query to get current Part object
        final Part part = this.partDataSource.getPart(partId);

        // Query to get current PartStorageLocation object
        final PartStorageLocation partLoc = getPartLoc(partId, locId);

        // calculate all reserved part quantity
        double reservedQuantity = 0;
        for (final WorkRequestPart estimatePart : allEstimatedParts) {

            if (RESERVED.equalsIgnoreCase(estimatePart.getStatus())) {

                reservedQuantity += estimatePart.getQtyEstimated();
            }
        }

        // save on reserved quantity of part
        part.setQtyOnReserve(reservedQuantity);
        this.partDataSource.update(part);

        // Query to get all estimation of given part and location
        final List<WorkRequestPart> allEstimatedPartsOfCurrentLoc =
                this.workRequestPartDataSource.findEstimatedParts(partId, locId);

        // calculate all reserved part quantity
        double reservedQuantityOfCurrentLoc = 0;
        for (final WorkRequestPart estimatePart : allEstimatedPartsOfCurrentLoc) {

            if (RESERVED.equalsIgnoreCase(estimatePart.getStatus())) {

                reservedQuantityOfCurrentLoc += estimatePart.getQtyEstimated();
            }
        }
        // save on reserved quantity of part location
        partLoc.setQtyOnReserve(reservedQuantityOfCurrentLoc);
        update(partLoc);
    }

    /** {@inheritDoc} */

    @Override
    public void addNewPartToStoreLocIfNotExists(final String ptStoreLocId, final String partId) {

        PartStorageLocation partLoc = getPartLoc(partId, ptStoreLocId);
        if (partLoc == null) {
            // Query to get current Part object
            final Part part = this.partDataSource.getPart(partId);
            final StorageLocation loc = this.storageLocationDataSource.findLocation(ptStoreLocId);
            partLoc = new PartStorageLocation();
            partLoc.setPartId(partId);
            partLoc.setLocId(ptStoreLocId);
            partLoc.setQtyMinHand(part.getQtyMinHand());
            partLoc.setCostUnitStd(part.getCostUnitStd());
            partLoc.setBlId(loc.getBlId());
            partLoc.setFlId(loc.getFlId());
            partLoc.setRmId(loc.getRmId());
            save(partLoc);
        }

    }

    /** {@inheritDoc} */

    @Override
    public void transferReservedPartsBetweenLocation(final String partId, final String fromLocation,
            final String toLocation, final double transQty) {
        // Decrease the Quantity Reserved of the From Storage Location by the quantity of
        // reserved parts that are being transferred.
        final PartStorageLocation fromLoc = getPartLoc(partId, fromLocation);
        fromLoc.setQtyOnReserve(fromLoc.getQtyOnReserve() - transQty);
        update(fromLoc);

        // Increase the Quantity Reserved of the Storage Location To by the quantity of reserved
        // parts that are being transferred.
        final PartStorageLocation toLoc = getPartLoc(partId, toLocation);
        toLoc.setQtyOnReserve(toLoc.getQtyOnReserve() + transQty);
        update(toLoc);

    }

    /** {@inheritDoc} */

    @Override
    public void addNewPartToDefaultPartStoreLoc(final String partId) {

        addNewPartToStoreLocIfNotExists(PartInventoryUtility.getMainStorageLocation(), partId);

    }

    /** {@inheritDoc} */
    @Override
    public void updatePhysicalCount(final String storeLocId, final JSONArray partCodeArray,
            final Double qtyPhysicalCount, final boolean isAdjustQtyOnHand) {
        // JSONArray to use for calculate part quantity.
        final JSONArray ptRecords = new JSONArray();
        for (int i = 0; i < partCodeArray.length(); i++) {
            final JSONObject partCodeRecord = partCodeArray.getJSONObject(i);
            final String partId = partCodeRecord.getString("pt.part_id");
            // save physical count and current date to part storage location table.
            final PartStorageLocation ptStoreLoc = this.getPartLoc(partId, storeLocId);
            ptStoreLoc.setQtyPhysicalCount(qtyPhysicalCount);
            ptStoreLoc.setDateOfLastCnt(Utility.currentDate());
            update(ptStoreLoc);

            // Adjust Quantity Available from physical count if user checked the checkbox.
            if (isAdjustQtyOnHand) {
                final Part partObject = this.partDataSource.getPart(partId);
                // Build part record as parameter to call WFR of calculation.
                final JSONObject record = new JSONObject();
                record.put("pt_store_loc_pt.qty_on_reserve", ptStoreLoc.getQtyOnReserve());
                record.put("pt_store_loc_pt.pt_store_loc_id", storeLocId);
                record.put("pt_store_loc_pt.qty_physical_count", qtyPhysicalCount);
                record.put("pt_store_loc_pt.part_id", partId);
                record.put("pt_store_loc_pt.qty_on_hand", ptStoreLoc.getQtyOnHand());
                record.put("pt_store_loc_pt.cost_unit_last", ptStoreLoc.getCostUnitLast());
                record.put("pt.acc_prop_type",
                    partObject.getAccPropType() == null ? "" : partObject.getAccPropType());
                ptRecords.put(record);
            } else {
                // Insert record to IT table.
                final Transaction itObject = new Transaction();
                itObject.setPartId(partId);
                itObject.setFromLocation(storeLocId);
                itObject.setPerformedBy(ContextStore.get().getUser().getName());
                itObject.setTransType("Rectify");
                itObject.setTransDate(Utility.currentDate());
                itObject.setTransTime(Utility.currentTime());
                itObject.setTransQuantity(qtyPhysicalCount);
                this.transactionDataSource.save(itObject);
            }
        }

        if (ptRecords.length() > 0 && isAdjustQtyOnHand) {
            new ResourceCalculations().updateQuantityAvailableFromPhysicalCountForMPSL(ptRecords);
        }

    }

    /**
     * When a part estimation is changed, update the part's available quantity and its other
     * associated wrpt's status.
     *
     * @param partId part code
     * @param partLoc part location code
     * @param wrId work request id.
     * @param date assigned date.
     * @param time assigned time.
     * @param difference difference of part estimated quantity
     */
    public void adjustPartEstimationQuantity(final String partId, final String partLoc,
            final int wrId, final String date, final String time, final double difference) {
        final WorkRequestPart estimatePart =
                this.workRequestPartDataSource.findEstimatedPart(partId, partLoc, wrId, date, time);

        final double newEstimateQty = estimatePart.getQtyEstimated();
        final double oldEstimateQty = newEstimateQty - difference;
        final String status = estimatePart.getStatus();

        final PartStorageLocation partLocObject = getPartLoc(partId, partLoc);
        final double availableQuantity = partLocObject.getQtyOnHand();

        // if no estimated quantity change then return
        if (difference != 0) {
            final double quantity = PartInventoryUtility.updateStatusOfWrpt(estimatePart, status,
                difference, newEstimateQty, oldEstimateQty, availableQuantity);

            this.workRequestPartDataSource.update(estimatePart);

            if (quantity != 0) {

                this.updateWrptStatus(partId, partLoc, quantity);
            }
        }

    }

    /**
     *
     * Re-set status of wrpt that just changed the estimated quantity and is not 'reserved'.
     *
     * @param partId part code
     * @param partLoc part location code
     * @param estimatePart DataRecord wrpt record.
     * @param status String wrpt status.
     */
    public void setStatusForRequestClose(final String partId, final String partLoc,
            final DataRecord estimatePart, final String status) {
        final Part partObject = this.partDataSource.getPart(partId);
        final PartStorageLocation partLocObject = getPartLoc(partId, partLoc);
        final double estimateQty = estimatePart.getDouble(WRPT_QTY_ESTIMATED);
        final double actualQty = estimatePart.getDouble(QTY_ACTUAL);
        final double availableQty = partLocObject.getQtyOnHand();
        final double reservedQty = partLocObject.getQtyOnReserve();
        final double difference = actualQty - estimateQty;

        // kb3043739: when closing a Not reserved part estimation, just take its actual quantity
        // used as the difference between estimation and actualy used.
        if (NOT_IN_STOCK.equalsIgnoreCase(status) || NOT_RESERVED.equalsIgnoreCase(status)) {

            if (actualQty > 0) {
                this.setWrptStatusForQuantityDecrease(partId, partLoc, -actualQty);
            }

        } else {
            if (difference > 0) {

                if (difference <= availableQty) {

                    partObject.setQtyOnReserve(partObject.getQtyOnReserve() - estimateQty);
                    this.partDataSource.update(partObject);
                    partLocObject.setQtyOnReserve(reservedQty - estimateQty);
                    update(partLocObject);

                    this.setWrptStatusForQuantityDecrease(partId, partLoc, -difference);

                } else if (difference > availableQty) {

                    this.unReserveAllPartEstimations(partId, partLoc);
                    this.setWrptStatusForQuantityDecrease(partId, partLoc, -difference);
                }

            } else if (difference < 0) {

                partObject.setQtyOnReserve(partObject.getQtyOnReserve() - estimateQty);
                this.partDataSource.update(partObject);
                partLocObject.setQtyOnReserve(reservedQty - estimateQty);
                update(partLocObject);

                this.setWrptStatusForQuantityIncrease(partId, partLoc, -difference);

            } else {
                // kb#3044675: also update part's available quantity when actual used is equal to
                // estimated
                partObject.setQtyOnReserve(partObject.getQtyOnReserve() - estimateQty);
                this.partDataSource.update(partObject);
                partLocObject.setQtyOnReserve(reservedQty - estimateQty);
                update(partLocObject);

            }
        }

    }

    /**
     *
     * Update reserved estimated parts to be 'In Stock Not Reserved', recalculate reserved quantity
     * and available quantity.
     *
     * @param partId part code
     * @param partLoc part location code
     */
    public void unReserveAllPartEstimations(final String partId, final String partLoc) {

        // Query 'Reserved' estimated parts
        final List<WorkRequestPart> estimatedParts = this.workRequestPartDataSource
            .findEstimatedParts(partId, partLoc, new String[] { RESERVED });

        // Loop all reserved estimated parts to get the sum reserved quantity
        double sumReserved = 0;
        for (final WorkRequestPart estimatePart : estimatedParts) {

            final double estimatedQuantity = estimatePart.getQtyEstimated();
            sumReserved += estimatedQuantity;
            estimatePart.setStatus(NOT_RESERVED);
            this.workRequestPartDataSource.update(estimatePart);
        }

        final Part partObject = this.partDataSource.getPart(partId);
        final PartStorageLocation partLocObject = getPartLoc(partId, partLoc);

        partObject.setQtyOnReserve(partObject.getQtyOnReserve() - sumReserved);
        partObject.setQtyOnHand(partObject.getQtyOnHand() + sumReserved);
        this.partDataSource.update(partObject);

        partLocObject.setQtyOnReserve(0);
        partLocObject.setQtyOnHand(partLocObject.getQtyOnHand() + sumReserved);
        update(partLocObject);
    }

    /**
     *
     * Re-set status of part estimations as well as part's quantity values since directly setting of
     * part's phsical count.
     *
     * @param partId part code
     * @param partLoc part location code
     */
    public void setWrptStatusByPhsicalCount(final String partId, final String partLoc) {
        final PartStorageLocation partLocObject = getPartLoc(partId, partLoc);
        final double reservedQty = partLocObject.getQtyOnReserve();
        final double phsicalQty = partLocObject.getQtyPhysicalCount();
        final double availableQty = partLocObject.getQtyOnHand();

        if (phsicalQty < reservedQty) {

            this.unReserveAllPartEstimations(partId, partLoc);

            this.setWrptStatusForQuantityDecrease(partId, partLoc,
                phsicalQty - reservedQty - availableQty);
            // kb#3043789: also need to process 'Not In Stock' part estimations even after quantity
            // decrease.
            this.setWrptStatusForQuantityIncrease(partId, partLoc, 0);

        } else if (phsicalQty < reservedQty + availableQty) {

            this.setWrptStatusForQuantityDecrease(partId, partLoc,
                phsicalQty - reservedQty - availableQty);

        } else if (phsicalQty > reservedQty + availableQty) {

            this.setWrptStatusForQuantityIncrease(partId, partLoc,
                phsicalQty - reservedQty - availableQty);
        }
    }

    /**
     * Setter for the workRequestPartDataSource property.
     *
     * @see workRequestPartDataSource
     * @param workRequestPartDataSource the workRequestPartDataSource to set
     */

    public void setWorkRequestPartDataSource(
            final IWorkRequestPartDao<WorkRequestPart> workRequestPartDataSource) {
        this.workRequestPartDataSource = workRequestPartDataSource;
    }

    /**
     * Setter for the partDataSource property.
     *
     * @see partDataSource
     * @param partDataSource the partDataSource to set
     */

    public void setPartDataSource(final IPartDao<Part> partDataSource) {
        this.partDataSource = partDataSource;
    }

    /**
     * Setter for the storageLocationDataSource property.
     *
     * @see storageLocationDataSource
     * @param storageLocationDataSource the storageLocationDataSource to set
     */

    public void setStorageLocationDataSource(
            final IStorageLocationDao<StorageLocation> storageLocationDataSource) {
        this.storageLocationDataSource = storageLocationDataSource;
    }

    /**
     * Getter for the transactionDataSource property.
     *
     * @see transactionDataSource
     * @return the transactionDataSource property.
     */
    public ITransactionDao<Transaction> getTransactionDataSource() {
        return this.transactionDataSource;
    }

    /**
     * Setter for the transactionDataSource property.
     *
     * @see transactionDataSource
     * @param transactionDataSource the transactionDataSource to set
     */

    public void setTransactionDataSource(final ITransactionDao<Transaction> transactionDataSource) {
        this.transactionDataSource = transactionDataSource;
    }

    /** {@inheritDoc} */

    @Override
    public void update(final PartStorageLocation partLoc) {
        super.update(partLoc);
        // KB3052725-Automatically update Quantity of Understocked Parts after part quantity changes
        new ResourceCalculations().calculateUnderstockedPartsForMPSL();
    }

}
