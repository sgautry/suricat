package com.archibus.app.bldgops.schedule.exchange.util;

import java.sql.Time;
import java.util.*;

import org.json.JSONObject;

import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;
import com.archibus.utility.*;

/**
 * Utility class. Provides methods to convert busy time to a given time zone.
 * <p>
 *
 * Used by getting busy times from Exchange server.
 *
 * @author Jia
 * @since 23.2
 *
 */
public final class TimeZoneConverter {

	/** Time zones table name. */
	private static final String AFM_TIMEZONES = "afm_timezones";

	/** Time zone ID field name. */
	private static final String TIMEZONE_ID = "timezone_id";

	/**
	 * Private default constructor: utility class is non-instantiable.
	 */
	private TimeZoneConverter() {
	}

	/**
	 * Gets the time zone. If the building id is null or the building doesn't
	 * have a time zone, returns the default time zone of the server.
	 *
	 * @param buildingId
	 *            the building id
	 * @return the time zone if of the building, or the default time zone id
	 */
	public static String getTimeZoneIdForBuilding(final String buildingId) {
		String timeZoneId = null;
		timeZoneId = LocalDateTimeUtil.getLocationTimeZone(null, null, null, buildingId);

		return timeZoneId;
	}

	/**
	 * Get the time zone record for a given building.
	 *
	 * @param buildingId
	 *            the building id
	 * @return the time zone data record
	 */
	public static DataRecord getTimeZoneForBuilding(final String buildingId) {
		final DataSource dataSource = DataSourceFactory.createDataSourceForFields(AFM_TIMEZONES,
				new String[] { TIMEZONE_ID });
		dataSource.addRestriction(
				Restrictions.eq(AFM_TIMEZONES, TIMEZONE_ID, TimeZoneConverter.getTimeZoneIdForBuilding(buildingId)));
		return dataSource.getRecord();
	}

	/**
	 * Get the current local date/time for the given buildings.
	 *
	 * @param buildingIds
	 *            list of building IDs
	 * @return JSON mapping of each building id to the current date/time in that
	 *         building
	 */
	public static JSONObject getCurrentLocalDateTime(final List<String> buildingIds) {
		final Set<String> uniqueBuildingIds = new HashSet<String>(buildingIds);
		final JSONObject localDateTimes = new JSONObject();

		for (final String buildingId : uniqueBuildingIds) {
			final JSONObject buildingDateTime = new JSONObject();
			buildingDateTime.put("date", LocalDateTimeUtil.currentLocalDate(null, null, null, buildingId).toString());
			buildingDateTime.put("time", LocalDateTimeUtil.currentLocalTime(null, null, null, buildingId).toString());

			localDateTimes.put(buildingId, buildingDateTime);
		}

		return localDateTimes;
	}

	/**
	 * Calculate date time between two time zones.
	 *
	 * For web interface reservations will always be in the time zone of the
	 * building/site. Therefore the site is always required when making a
	 * search. The requestor time zone will be the site time zone.
	 *
	 * For reservations coming from Outlook, the requestor time zone will be
	 * GMT/UTC.
	 *
	 * @param startDateTime
	 *            the start date time
	 * @param sourceTimeZoneId
	 *            the source time zone id, current time zone of the date/time
	 * @param targetTimeZoneId
	 *            the target time zone id to convert to
	 *
	 * @return the date/time in the target time zone
	 */
	public static Date calculateDateTime(final Date startDateTime, final String sourceTimeZoneId,
			final String targetTimeZoneId) {
		TimeZone targetTimeZone = null;

		if (StringUtil.notNullOrEmpty(targetTimeZoneId)) {
			targetTimeZone = TimeZone.getTimeZone(targetTimeZoneId);
		} else {
			// if the time zone is not defined for the building, we assume the
			// time zone of the
			// server.
			targetTimeZone = TimeZone.getDefault();
		}

		// offset in milliseconds
		final int targetOffset = targetTimeZone.getOffset(startDateTime.getTime());

		TimeZone sourceTimeZone = null;

		if (StringUtil.notNullOrEmpty(sourceTimeZoneId)) {
			sourceTimeZone = TimeZone.getTimeZone(sourceTimeZoneId);
		} else {
			sourceTimeZone = TimeZone.getDefault();
		}

		// offset in milliseconds
		final int sourceOffset = sourceTimeZone.getOffset(startDateTime.getTime());

		final Calendar cal = Calendar.getInstance();
		cal.setTime(startDateTime);
		cal.add(Calendar.MILLISECOND, targetOffset);
		cal.add(Calendar.MILLISECOND, -sourceOffset);
		return cal.getTime();
	}

	/**
	 * Calculate the total offset in milliseconds between two time zones.
	 *
	 * @param date
	 *            the date
	 * @param time
	 *            the time
	 * @param sourceTimeZoneId
	 *            the source time zone id
	 * @param targetTimeZoneId
	 *            the target time zone id
	 *
	 * @return the combined offset
	 */
	public static int getCombinedOffset(final Date date, final Time time, final String sourceTimeZoneId,
			final String targetTimeZoneId) {
		final TimeZone sourceTimeZone = TimeZone.getTimeZone(sourceTimeZoneId);
		final TimeZone targetTimeZone = TimeZone.getTimeZone(targetTimeZoneId);
		final Date dateTime = Utility.toDatetime(date, time);

		return targetTimeZone.getOffset(dateTime.getTime()) - sourceTimeZone.getOffset(dateTime.getTime());
	}

}
