package com.archibus.app.bldgops.partinv.dao;

import org.json.JSONArray;

import com.archibus.core.dao.IDao;

/**
 *
 * Provides methods to load Part Storage Location Part.
 * <p>
 *
 * @author Guo Jiangtao
 * @since 23.2
 *
 * @param <PartStorageLocation> type of object
 */
public interface IPartStorageLocationDao<PartStorageLocation> extends IDao<PartStorageLocation> {

    /**
     * find part location.
     *
     * @param partId part code
     * @param locId location code
     * @return Part object
     */
    PartStorageLocation getPartLoc(final String partId, final String locId);

    /**
     * Update wrpt status.
     *
     * @param partId part code
     * @param locId location code
     * @param quantity quantity
     */
    void updateWrptStatus(final String partId, final String locId, final double quantity);

    /**
     * Remove aisle code shelf code Cabinet code and bin code.
     *
     * @param ptLocId part storage location
     */
    void removePartLocations(final String ptLocId);

    /**
     * Add New part to Storage Location if it doesn't exists in storage location.
     *
     * @param ptStoreLocId Part storage location code
     * @param partId Part code
     */
    void addNewPartToStoreLocIfNotExists(final String ptStoreLocId, final String partId);

    /**
     *
     * Add New part to default part storage location.
     *
     * @param partId Part code
     */
    void addNewPartToDefaultPartStoreLoc(final String partId);

    /**
     * transfer reserved part between locations.
     *
     * @param partId Part code
     * @param fromLocation from location
     * @param toLocation to location
     * @param transQty transfer quantity
     */
    void transferReservedPartsBetweenLocation(final String partId, final String fromLocation,
            final String toLocation, final double transQty);

    /**
     * Update Physical Count.
     *
     * @param storeLocId Storage location code
     * @param partCodeArray Part code array
     * @param qtyPhysicalCount Physical count quantity
     * @param isAdjustQtyOnHand Whether update Quantity Available when update physical count.
     */
    void updatePhysicalCount(String storeLocId, JSONArray partCodeArray, Double qtyPhysicalCount,
            boolean isAdjustQtyOnHand);
}
