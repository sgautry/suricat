package com.archibus.app.bldgops.schedule.dao.datasource;

import static com.archibus.app.bldgops.schedule.util.CraftspersonScheduleConstant.*;

import java.util.List;

import com.archibus.app.bldgops.schedule.dao.*;
import com.archibus.app.bldgops.schedule.domain.*;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecordField;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.ParsedRestrictionDef;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides methods to find/update/delete/insert craftsperson data.
 * <p>
 *
 * Managed by Spring.
 *
 * @author Jia
 * @since 23.2
 *
 */
public class CraftspersonDataSource extends ObjectDataSourceImpl<Craftsperson>
		implements ICraftspersonDao<Craftsperson> {

	/**
	 * Employee Dao.
	 */
	private IEmployeeDao scheduleEmployeeDao;
	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { CF_ID, "cfId" }, { EMAIL, "email" } };

	@Override
	protected final String[][] getFieldsToProperties() {
		// TODO Auto-generated method stub
		return this.FIELDS_TO_PROPERTIES.clone();
	}

	/**
	 * Constructor.
	 */
	public CraftspersonDataSource() {
		super("craftsperson", CF_TABLE);
	}

	/** {@inheritDoc} */
	@Override
	public final Craftsperson getCraftsperson(final String cfId) {
		final PrimaryKeysValues primaryKeysValues = new PrimaryKeysValues();
		final DataRecordField pkField = new DataRecordField();
		pkField.setName(CF_TABLE + DOT + CF_ID);
		pkField.setValue(cfId);
		primaryKeysValues.getFieldsValues().add(pkField);
		return this.get(primaryKeysValues);
	}

	/** {@inheritDoc} */
	@Override
	public final List<Craftsperson> getAllCraftspersons() {
		this.applyVpaRestrictions = false;
		final ParsedRestrictionDef resDef = new ParsedRestrictionDef();
		return this.find(resDef);
	}

	/** {@inheritDoc} */
	@Override
	public final Craftsperson getCraftspersonByEmail(final String email) {
		this.applyVpaRestrictions = false;
		final ParsedRestrictionDef resDef = new ParsedRestrictionDef();
		resDef.addClause(CF_TABLE, EMAIL, email, Operation.EQUALS);

		return this.find(resDef).get(0);
	}

	/** {@inheritDoc} */
	@Override
	public final String getReferencedEmployeeBuildingCode(final String craftspersonEmail) {
		String buildingId = "";
		this.applyVpaRestrictions = false;
		final List<Employee> employees = this.scheduleEmployeeDao.getEmployeesByEmail(craftspersonEmail);
		if (employees.size() > 0) {
			buildingId = employees.get(0).getBlId();
		}

		return buildingId;
	}

	/**
	 * Getter for the scheduleEmployeeDao property.
	 *
	 * @return the scheduleEmployeeDao property.
	 */
	public final IEmployeeDao getScheduleEmployeeDao() {
		return this.scheduleEmployeeDao;
	}

	/**
	 * Setter for the scheduleEmployeeDao property.
	 *
	 * @param scheduleEmployeeDao
	 *            the scheduleEmployeeDao to set.
	 */
	public final void setScheduleEmployeeDao(final IEmployeeDao scheduleEmployeeDao) {
		this.scheduleEmployeeDao = scheduleEmployeeDao;
	}

}
