package com.archibus.app.bldgops.schedule.dao;

import java.util.*;

import com.archibus.core.dao.IDao;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides interface for craftsperson scheduled variances.
 *
 * @author Jia
 * @since 23.2
 *
 */
public interface ICraftspersonScheduleVariancesDao<CraftspersonScheduleVariances>
		extends IDao<CraftspersonScheduleVariances> {
	/**
	 * Get Schedule Variance by scheduled variance primary key.
	 *
	 * @param scheduledVarianceId
	 *            -Schedule Variance Code.
	 * @return Craftsperson Schedule Variances object.
	 */
	CraftspersonScheduleVariances getScheduleVariance(int scheduledVarianceId);

	/**
	 * Get scheduled variances by craftsperson code.
	 *
	 * @param craftspersonId
	 *            -Craftsperson Code.
	 * @return CraftspersonScheduleVariances List.
	 */
	List<CraftspersonScheduleVariances> getScheduledVariancesByCraftsperson(String craftspersonId);

	/**
	 * Add new scheduled variance record. TODO addNewScheduleVariance.
	 *
	 * @param variance
	 *            -Scheduled Variance Object.
	 * @return
	 */
	void addNewScheduleVariance(CraftspersonScheduleVariances variance);

	/**
	 * Delete specify craftsperson scheduled variances.
	 *
	 * @param scheduledVarianceId
	 *            -Scheduled Variance Code.
	 *
	 * @param scheduledVarianceId-
	 *            Scheduled Variance Code.
	 */
	void deleteScheduledVariance(int scheduledVarianceId);

	/**
	 * Find and delete all overlapping scheduled variances during the start time
	 * and end time for the specified craftsperson.
	 *
	 * @param cfId
	 *            -Craftsperson Code.
	 * @param timeStart
	 *            - Start time.
	 * @param timeEnd
	 *            - End time.
	 */
	void deleteOverlappingTimeBlocks(String cfId, Date timeStart, Date timeEnd);

	/**
	 * Delete busy variances during time period.
	 *
	 * @param dateStart
	 *            - Date start.
	 * @param dateEnd
	 *            -Date end.
	 */
	void deleteBusyVariancesInTimePeriod(Date dateStart, Date dateEnd);
}
