package com.archibus.app.bldgops.partinv.domain;

import java.util.Date;

/**
 * Domain object for it.
 * <p>
 *
 * @author Guo Jiangtao
 * @since 23.2
 *
 */
public class Transaction {
    /** The transaction id. */
    private int transId;

    /** The part code. */
    private String partId;

    /** The supply requisition item status. */
    private String reqItemStatus;

    /** The supply requisition id. */
    private Integer supplyReqId;

    /** form location. */
    private String fromLocation;

    /** to location. */
    private String toLocation;

    /** transaction date. */
    private Date transDate;

    /** transaction time. */
    private Date transTime;

    /** transaction type. */
    private String transType;

    /** performed by. */
    private String performedBy;

    /** transaction quantity. */
    private double transQuantity;

    /** The transaction comments. */
    private String comments;

    /** transaction cost_when_used. */
    private double costWhenUsed;

    /** transaction cost_total. */
    private double costTotal;

    /**
     * Getter for the transId property.
     *
     * @see transId
     * @return the transId property.
     */
    public int getTransId() {
        return this.transId;
    }

    /**
     * Setter for the transId property.
     *
     * @see transId
     * @param transId the transId to set
     */

    public void setTransId(final int transId) {
        this.transId = transId;
    }

    /**
     * Getter for the partId property.
     *
     * @see partId
     * @return the partId property.
     */
    public String getPartId() {
        return this.partId;
    }

    /**
     * Setter for the partId property.
     *
     * @see partId
     * @param partId the partId to set
     */

    public void setPartId(final String partId) {
        this.partId = partId;
    }

    /**
     * Getter for the reqItemStatus property.
     *
     * @see reqItemStatus
     * @return the reqItemStatus property.
     */
    public String getReqItemStatus() {
        return this.reqItemStatus;
    }

    /**
     * Setter for the reqItemStatus property.
     *
     * @see reqItemStatus
     * @param reqItemStatus the reqItemStatus to set
     */

    public void setReqItemStatus(final String reqItemStatus) {
        this.reqItemStatus = reqItemStatus;
    }

    /**
     * Getter for the supplyReqId property.
     *
     * @see supplyReqId
     * @return the supplyReqId property.
     */
    public Integer getSupplyReqId() {
        return this.supplyReqId;
    }

    /**
     * Setter for the supplyReqId property.
     *
     * @see supplyReqId
     * @param supplyReqId the supplyReqId to set
     */

    public void setSupplyReqId(final Integer supplyReqId) {
        this.supplyReqId = supplyReqId;
    }

    /**
     * Getter for the fromLocation property.
     *
     * @see fromLocation
     * @return the fromLocation property.
     */
    public String getFromLocation() {
        return this.fromLocation;
    }

    /**
     * Setter for the fromLocation property.
     *
     * @see fromLocation
     * @param fromLocation the fromLocation to set
     */

    public void setFromLocation(final String fromLocation) {
        this.fromLocation = fromLocation;
    }

    /**
     * Getter for the toLocation property.
     *
     * @see toLocation
     * @return the toLocation property.
     */
    public String getToLocation() {
        return this.toLocation;
    }

    /**
     * Setter for the toLocation property.
     *
     * @see toLocation
     * @param toLocation the toLocation to set
     */

    public void setToLocation(final String toLocation) {
        this.toLocation = toLocation;
    }

    /**
     * Getter for the transDate property.
     *
     * @see transDate
     * @return the transDate property.
     */
    public Date getTransDate() {
        return this.transDate;
    }

    /**
     * Setter for the transDate property.
     *
     * @see transDate
     * @param transDate the transDate to set
     */

    public void setTransDate(final Date transDate) {
        this.transDate = transDate;
    }

    /**
     * Getter for the transTime property.
     *
     * @see transTime
     * @return the transTime property.
     */
    public Date getTransTime() {
        return this.transTime;
    }

    /**
     * Setter for the transTime property.
     *
     * @see transTime
     * @param transTime the transTime to set
     */

    public void setTransTime(final Date transTime) {
        this.transTime = transTime;
    }

    /**
     * Getter for the transType property.
     *
     * @see transType
     * @return the transType property.
     */
    public String getTransType() {
        return this.transType;
    }

    /**
     * Setter for the transType property.
     *
     * @see transType
     * @param transType the transType to set
     */

    public void setTransType(final String transType) {
        this.transType = transType;
    }

    /**
     * Getter for the performedBy property.
     *
     * @see performedBy
     * @return the performedBy property.
     */
    public String getPerformedBy() {
        return this.performedBy;
    }

    /**
     * Setter for the performedBy property.
     *
     * @see performedBy
     * @param performedBy the performedBy to set
     */

    public void setPerformedBy(final String performedBy) {
        this.performedBy = performedBy;
    }

    /**
     * Getter for the transQuantity property.
     *
     * @see transQuantity
     * @return the transQuantity property.
     */
    public double getTransQuantity() {
        return this.transQuantity;
    }

    /**
     * Setter for the transQuantity property.
     *
     * @see transQuantity
     * @param transQuantity the transQuantity to set
     */

    public void setTransQuantity(final double transQuantity) {
        this.transQuantity = transQuantity;
    }

    /**
     * Getter for the comments property.
     *
     * @see comments
     * @return the comments property.
     */
    public String getComments() {
        return this.comments;
    }

    /**
     * Setter for the comments property.
     *
     * @see comments
     * @param comments the comments to set
     */

    public void setComments(final String comments) {
        this.comments = comments;
    }

    /**
     * Getter for the costWhenUsed property.
     *
     * @see costWhenUsed
     * @return the costWhenUsed property.
     */
    public double getCostWhenUsed() {
        return this.costWhenUsed;
    }

    /**
     * Setter for the costWhenUsed property.
     *
     * @see costWhenUsed
     * @param costWhenUsed the costWhenUsed to set
     */

    public void setCostWhenUsed(final double costWhenUsed) {
        this.costWhenUsed = costWhenUsed;
    }

    /**
     * Getter for the costTotal property.
     *
     * @see costTotal
     * @return the costTotal property.
     */
    public double getCostTotal() {
        return this.costTotal;
    }

    /**
     * Setter for the costTotal property.
     *
     * @see costTotal
     * @param costTotal the costTotal to set
     */

    public void setCostTotal(final double costTotal) {
        this.costTotal = costTotal;
    }

}
