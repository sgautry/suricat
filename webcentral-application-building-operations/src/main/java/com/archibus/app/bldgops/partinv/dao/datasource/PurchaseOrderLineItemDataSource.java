package com.archibus.app.bldgops.partinv.dao.datasource;

import static com.archibus.app.bldgops.partinv.util.PartInventoryConstant.*;

import java.util.*;

import org.json.*;

import com.archibus.app.bldgops.partinv.dao.IPurchaseOrderLineItemDao;
import com.archibus.app.bldgops.partinv.domain.PurchaseOrderLineItem;
import com.archibus.datasource.*;
import com.archibus.datasource.data.*;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.ParsedRestrictionDef;
import com.archibus.utility.*;

/**
 *
 * Purchase Order line item datasource.
 * <p>
 *
 *
 * @author Guo Jiangtao , Jia
 * @since 23.2
 *
 */
public class PurchaseOrderLineItemDataSource extends ObjectDataSourceImpl<PurchaseOrderLineItem>
        implements IPurchaseOrderLineItemDao<PurchaseOrderLineItem> {

    /**
     * Field names to property names mapping. All fields will be added to the DataSource.
     */
    private static final String[][] FIELDS_TO_PROPERTIES =
            { { "po_id", "poId" }, { STATUS, "status" }, { PO_LINE_ID, "poLineId" },
                    { "date_received", "dateReceived" }, { "vn_id", "vnId" }, { "em_id", "emId" },
                    { UNIT_COST, "unitCost" }, { QUANTITY, "qunatity" }, { CATNO, "catNo" },
                    { COMMENTS, "comments" }, { DESCRIPTION, "description" } };

    /**
     * Constructs PurchaseOrderLineItemDataSource, mapped to <code>po_line</code> table, using
     * <code>purchaseOrderLineItem</code> bean.
     *
     */
    public PurchaseOrderLineItemDataSource() {
        super("purchaseOrderLineItem", PO_LINE_TABLE);
    }

    /** {@inheritDoc} */

    @Override
    public PurchaseOrderLineItem findPurchaseOrderLineItem(final int poId, final int poLineId) {
        final PrimaryKeysValues primaryKeysValues = new PrimaryKeysValues();
        {
            final DataRecordField pkField = new DataRecordField();
            pkField.setName(PO_LINE_TABLE + DOT + PO_ID);
            pkField.setValue(poId);
            primaryKeysValues.getFieldsValues().add(pkField);
        }

        {
            final DataRecordField pkField = new DataRecordField();
            pkField.setName(PO_LINE_TABLE + DOT + PO_LINE_ID);
            pkField.setValue(poLineId);
            primaryKeysValues.getFieldsValues().add(pkField);
        }

        return this.get(primaryKeysValues);
    }

    /** {@inheritDoc} */

    @Override
    public List<PurchaseOrderLineItem> findNotReceivedAndErrorItems(final int poId) {
        final ParsedRestrictionDef resDef = new ParsedRestrictionDef();
        resDef.addClause(PO_LINE_TABLE, PO_ID, poId, Operation.EQUALS);
        resDef.addClause(PO_LINE_TABLE, STATUS,
            Arrays.asList(new String[] { STATUS_RECEIVED, STATUS_ERROR }), Operation.NOT_IN);

        return this.find(resDef);
    }

    /** {@inheritDoc} */

    @Override
    public List<PurchaseOrderLineItem> findReceivedItems(final int poId) {
        final ParsedRestrictionDef resDef = new ParsedRestrictionDef();
        resDef.addClause(PO_LINE_TABLE, PO_ID, poId, Operation.EQUALS);
        resDef.addClause(PO_LINE_TABLE, STATUS, STATUS_RECEIVED, Operation.EQUALS);

        return this.find(resDef);
    }

    /** {@inheritDoc} */

    @Override
    public List<PurchaseOrderLineItem> findAllItems(final int poId) {
        final ParsedRestrictionDef resDef = new ParsedRestrictionDef();
        resDef.addClause(PO_LINE_TABLE, PO_ID, poId, Operation.EQUALS);

        return this.find(resDef);
    }

    /** {@inheritDoc} */

    @Override
    public List<PurchaseOrderLineItem> findErrorItems(final int poId) {
        final ParsedRestrictionDef resDef = new ParsedRestrictionDef();
        resDef.addClause(PO_LINE_TABLE, PO_ID, poId, Operation.EQUALS);
        resDef.addClause(PO_LINE_TABLE, STATUS, STATUS_ERROR, Operation.EQUALS);

        return this.find(resDef);
    }

    /** {@inheritDoc} */

    @Override
    public void updatePoLineStatus(final int poId, final int poLineId, final String status) {
        final PurchaseOrderLineItem poLineItem = findPurchaseOrderLineItem(poId, poLineId);
        poLineItem.setStatus(status);
        if (STATUS_RECEIVED.equals(status)) {
            poLineItem.setDateReceived(Utility.currentDate());
        }
        update(poLineItem);

    }

    /** {@inheritDoc} */

    @Override
    public void updatePoLineStatus(final int poId, final String status) {
        final List<PurchaseOrderLineItem> poLineItems = findNotReceivedAndErrorItems(poId);
        for (final PurchaseOrderLineItem poLineItem : poLineItems) {
            poLineItem.setStatus(status);
            update(poLineItem);
        }

    }

    /** {@inheritDoc} */

    @Override
    public void addNewPoLineItem(final JSONArray poLines, final int poId, final String vnId,
            final String poEmId) {
        for (int i = 0; i < poLines.length(); i++) {
            final PurchaseOrderLineItem poLineItem = new PurchaseOrderLineItem();
            final JSONObject poLineRecord = poLines.getJSONObject(i);

            poLineItem.setPoId(poId);
            poLineItem.setPoLineId(
                DataStatistics.getInt(PO_LINE_TABLE, PO_LINE_ID, "MAX", "po_id=" + poId) + 1);
            poLineItem.setUnitCost(poLineRecord.getDouble(PO_LINE_TABLE + DOT + UNIT_COST));
            poLineItem.setQunatity(poLineRecord.getInt(PO_LINE_TABLE + DOT + QUANTITY));
            poLineItem.setEmId(poEmId);

            final String catLogNo = poLineRecord.getString(PO_LINE_TABLE + DOT + CATNO);
            final String partId = poLineRecord.getString(PO_LINE_TABLE + DOT + PART_ID);
            addToPvTableIfVnPartNotExists(vnId, partId, catLogNo);

            poLineItem.setCatNo(catLogNo);
            poLineItem.setDescription(poLineRecord.getString(PO_LINE_TABLE + DOT + DESCRIPTION));
            save(poLineItem);
        }
    }

    /**
     * Insert a new record to pv table if record not exists by vnId and partId.
     *
     * @param vnId Vendor Id
     * @param partId Part Code
     * @param catLogNo Catlog Number
     */
    private void addToPvTableIfVnPartNotExists(final String vnId, final String partId,
            final String catLogNo) {
        final String pvCount = "pvCount";
        final String vnPtNum = "vnPtNum";
        final DataSource pvDs = DataSourceFactory.createDataSource().addTable(PV_TABLE)
            .addQuery("select count(1) as pvCount from pv where vn_id='" + vnId
                    + "' and part_id=                 '" + partId + "'          ")
            .addVirtualField(PV_TABLE, pvCount, DataSource.DATA_TYPE_TEXT);

        final DataRecord record = pvDs.getRecord();

        final int count = Integer.parseInt(record.getString(PV_TABLE + DOT + pvCount));

        if (count < 1) {
            final String insertSql = "insert into pv(vn_id,part_id,vn_pt_num) values ('" + vnId
                    + "',  '" + partId + "','" + catLogNo + "')";

            SqlUtils.executeUpdate(PV_TABLE, insertSql);
        } else {
            final DataSource vnPtNumDs = DataSourceFactory.createDataSource().addTable(PV_TABLE)
                .addQuery("select vn_pt_num as vnPtNum from pv where vn_id='" + vnId
                        + "' and part_id=       '" + partId + "'     ")
                .addVirtualField(PV_TABLE, vnPtNum, DataSource.DATA_TYPE_TEXT);
            final DataRecord vnPtNumRecord = vnPtNumDs.getRecord();

            final Object vnPtNumValue = vnPtNumRecord.getValue(PV_TABLE + DOT + vnPtNum);

            if (StringUtil.isNullOrEmpty(vnPtNumValue)) {
                final String vnPtNumSql = "update pv set vn_pt_num=part_id where vn_id='" + vnId
                        + "' and part_id='" + partId + "'";
                SqlUtils.executeUpdate(PV_TABLE, vnPtNumSql);
            }

        }
    }

    @Override
    protected String[][] getFieldsToProperties() {
        return FIELDS_TO_PROPERTIES.clone();
    }

}
