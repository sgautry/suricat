package com.archibus.app.bldgops.schedule.dao.datasource;

import static com.archibus.app.bldgops.schedule.util.CraftspersonScheduleConstant.*;

import java.util.*;

import com.archibus.app.bldgops.schedule.dao.ICraftspersonWorkRequestDao;
import com.archibus.app.bldgops.schedule.domain.CraftspersonWorkRequest;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecordField;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.ParsedRestrictionDef;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DataSource for craftsperson work request.
 *
 * @author Jia
 * @since 23.2
 *
 */
public class CraftspersonWorkRequestDataSource extends ObjectDataSourceImpl<CraftspersonWorkRequest>
		implements ICraftspersonWorkRequestDao {
	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { WR_ID, "wrId" }, { CF_ID, "cfId" },
			{ "date_assigned", "dateAssigned" }, { "time_assigned", "timeAssigned" }, { "task_order", "taskOrder" },
			{ "hours_est", "hoursEst" }, { "is_scheduled", "isScheduled" }, { "status", "status" } };

	/**
	 * Constructor.
	 */
	public CraftspersonWorkRequestDataSource() {
		super("craftspersonWorkRequest", WRCF_TABLE);
	}

	@Override
	protected String[][] getFieldsToProperties() {
		return this.FIELDS_TO_PROPERTIES.clone();
	}

	@Override
	public CraftspersonWorkRequest getCraftspersonWorkRequest(final String cfId, final int wrId,
			final Date dateAssigned, final Date timeAssigned) {
		final PrimaryKeysValues pkValues = new PrimaryKeysValues();
		{
			final DataRecordField pkField1 = new DataRecordField();
			pkField1.setName(WRCF_TABLE + DOT + CF_ID);
			pkField1.setValue(cfId);
			final DataRecordField pkField2 = new DataRecordField();
			pkField2.setName(WRCF_TABLE + DOT + WR_ID);
			pkField2.setValue(wrId);
			final DataRecordField pkField3 = new DataRecordField();
			pkField3.setName(WRCF_TABLE + DOT + "date_assigned");
			pkField3.setValue(dateAssigned);
			final DataRecordField pkField4 = new DataRecordField();
			pkField4.setName(WRCF_TABLE + DOT + "time_assigned");
			pkField4.setValue(timeAssigned);
			pkValues.getFieldsValues().add(pkField1);
			pkValues.getFieldsValues().add(pkField2);
			pkValues.getFieldsValues().add(pkField3);
			pkValues.getFieldsValues().add(pkField4);
		}

		return this.get(pkValues);
	}

	@Override
	public void unschedulePastScheduledWorkRequests() {
		final ParsedRestrictionDef resDef = new ParsedRestrictionDef();
		resDef.addClause(WRCF_TABLE, "is_scheduled", 1, Operation.EQUALS);
		resDef.addClause(WRCF_TABLE, "status", "Active", Operation.EQUALS);

		final List<CraftspersonWorkRequest> wrcfRecords = this.find(resDef);

		for (final CraftspersonWorkRequest cfWorkRequest : wrcfRecords) {
			final double estimateHours = cfWorkRequest.getHoursEst();
			// get date assigned.
			final Calendar dateAssignedCalendar = Calendar.getInstance();
			dateAssignedCalendar.setTime(cfWorkRequest.getDateAssigned());
			// get time assigned.
			final Calendar timeAssignedCalendar = Calendar.getInstance();
			timeAssignedCalendar.setTime(cfWorkRequest.getTimeAssigned());
			// Start Assigned Date and Time.
			dateAssignedCalendar.set(Calendar.HOUR_OF_DAY, timeAssignedCalendar.get(Calendar.HOUR_OF_DAY));
			dateAssignedCalendar.set(Calendar.MINUTE, timeAssignedCalendar.get(Calendar.MINUTE));
			dateAssignedCalendar.set(Calendar.SECOND, timeAssignedCalendar.get(Calendar.SECOND));

			// End Assigned Date and Time.
			final Calendar endAssignedDateTimeCalendar = Calendar.getInstance();
			endAssignedDateTimeCalendar.setTime(dateAssignedCalendar.getTime());
			endAssignedDateTimeCalendar.add(Calendar.SECOND, (int) (estimateHours * 60 * 60));

			// get current date
			final Calendar currentCalendar = Calendar.getInstance();

			if (dateAssignedCalendar.before(currentCalendar) && endAssignedDateTimeCalendar.before(currentCalendar)) {
				cfWorkRequest.setIsScheduled(0);
				cfWorkRequest.setHoursEst(1);
				this.update(cfWorkRequest);
			}

		}

	}

}
