package com.archibus.app.bldgops.schedule.exchange.domain;

import java.text.MessageFormat;

import com.archibus.context.ContextStore;
import com.archibus.eventhandler.EventHandlerBase;
import com.archibus.utility.ExceptionBase;

/**
 * A specific subclass of ReservationException that indicates an error in the
 * calendar service.
 *
 * @author Yorik Gerlo
 * @since 21.2
 */
public class CalendarException extends ExceptionBase {

	/** Generated serial version ID. */
	private static final long serialVersionUID = -4344500297872381595L;

	/**
	 * Create a reservation exception with localization based on the provided
	 * class.
	 *
	 * @param message
	 *            the message (to translate)
	 * @param clazz
	 *            the class where the message was defined
	 */
	public CalendarException(final String message, final Class<?> clazz) {
		super(EventHandlerBase.localizeString(ContextStore.get().getEventHandlerContext(), message, clazz.getName()));
		this.setForUser(true);
	}

	/**
	 * Create a reservation exception with localization based on the provided
	 * class. The additional arguments are used for formatting the translated
	 * string.
	 *
	 * @param message
	 *            the message (to translate)
	 * @param clazz
	 *            the class where the message was defined
	 * @param args
	 *            additional arguments used for formatting the localized message
	 */
	public CalendarException(final String message, final Class<?> clazz, final Object... args) {
		super(MessageFormat.format(
				EventHandlerBase.localizeString(ContextStore.get().getEventHandlerContext(), message, clazz.getName()),
				args));
		this.setForUser(true);
	}

	/**
	 * Create a reservation exception with localization based on the provided
	 * class.
	 *
	 * @param message
	 *            the message (to translate)
	 * @param cause
	 *            the causing exception
	 * @param clazz
	 *            the class where the message was defined
	 */
	public CalendarException(final String message, final Exception cause, final Class<?> clazz) {
		this(message, clazz);
		initCause(cause);
	}

	/**
	 * Create a reservation exception with localization based on the provided
	 * class. The additional arguments are used for formatting the translated
	 * string.
	 *
	 * @param message
	 *            the message (to translate)
	 * @param cause
	 *            the causing exception
	 * @param clazz
	 *            the class where the message was defined
	 * @param args
	 *            additional arguments used for formatting the localized message
	 */
	public CalendarException(final String message, final Exception cause, final Class<?> clazz, final Object... args) {
		this(message, clazz, args);
		initCause(cause);
	}

}
