package com.archibus.app.bldgops.schedule.service;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Service to unschedule past scheduled work requests.
 *
 * Managed by Spring, bean object configured in
 * /webcentral-application-building-operations/src/main/resources/com/archibus/
 * app/bldgops/schedule/craftsperson-schedule-context.xml
 *
 * Called by scheduled
 * WFR:AbBldgOpsOnDemandWork-unschedulePastScheduledWorkRequests
 *
 * @author Jia
 * @since 23.2
 *
 */
public interface IUnschedulePastScheduleService {
	/**
	 *
	 * Unschedule past scheduled work requests.
	 */
	void unschedulePastScheduleWorkRequests();
}
