/**
 * This package contains datasource objects for Part Inventory Management.
 **/
package com.archibus.app.bldgops.partinv.dao.datasource;
