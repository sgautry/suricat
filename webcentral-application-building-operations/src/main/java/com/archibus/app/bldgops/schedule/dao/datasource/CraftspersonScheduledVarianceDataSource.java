package com.archibus.app.bldgops.schedule.dao.datasource;

import static com.archibus.app.bldgops.schedule.util.CraftspersonScheduleConstant.*;

import java.util.*;

import com.archibus.app.bldgops.schedule.dao.ICraftspersonScheduleVariancesDao;
import com.archibus.app.bldgops.schedule.domain.CraftspersonScheduleVariances;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecordField;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.ParsedRestrictionDef;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides data source implementation to find/update/delete/insert scheduled
 * variances.
 * <p>
 *
 * Managed by Spring.
 *
 * @author Jia
 * @since 23.2
 *
 */
public class CraftspersonScheduledVarianceDataSource extends ObjectDataSourceImpl<CraftspersonScheduleVariances>
		implements ICraftspersonScheduleVariancesDao<CraftspersonScheduleVariances> {

	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { CF_SCHED_VARS_ID, "cfSchedVarsId" },
			{ DATE_START, "dateStart" }, { DATE_END, "dateEnd" }, { DESCRIPTION, "description" },
			{ SCHED_VARS_TYPE, "schedVarsType" }, { SITE_ID, "siteId" }, { BL_ID, "blId" }, { CF_ID, "cfId" },
			{ TIME_END, "timeEnd" }, { TIME_START, "timeStart" } };

	@Override
	protected String[][] getFieldsToProperties() {
		// TODO Auto-generated method stub
		return this.FIELDS_TO_PROPERTIES.clone();
	}

	public CraftspersonScheduledVarianceDataSource() {
		super("cfScheduledVariances", CF_SCHEDULES_VARIANCES_TABLE);
	}

	/** {@inheritDoc} */
	@Override
	public CraftspersonScheduleVariances getScheduleVariance(final int scheduledVarianceId) {
		final PrimaryKeysValues primaryKeysValues = new PrimaryKeysValues();
		{
			final DataRecordField pkField = new DataRecordField();
			pkField.setName(CF_SCHEDULES_VARIANCES_TABLE + DOT + CF_SCHED_VARS_ID);
			pkField.setValue(scheduledVarianceId);
			primaryKeysValues.getFieldsValues().add(pkField);
		}

		return this.get(primaryKeysValues);
	}

	/** {@inheritDoc} */
	@Override
	public List<CraftspersonScheduleVariances> getScheduledVariancesByCraftsperson(final String craftspersonId) {
		final ParsedRestrictionDef resDef = new ParsedRestrictionDef();
		resDef.addClause(CF_SCHEDULES_VARIANCES_TABLE, CF_ID, craftspersonId, Operation.EQUALS);
		return this.find(resDef);
	}

	/** {@inheritDoc} */
	@Override
	public void deleteOverlappingTimeBlocks(final String cfId, final Date timeStart, final Date timeEnd) {
		final List<CraftspersonScheduleVariances> scheduledVariances = this.getScheduledVariancesByCraftsperson(cfId);
		for (int i = 0; i < scheduledVariances.size(); i++) {
			final CraftspersonScheduleVariances scheduleVarianceRecord = scheduledVariances.get(i);
			if (scheduleVarianceRecord.getSchedVarsType() != "ON SITE") {
				// find out time blocks overlapping with the specify start time
				// and end time.

				// format variance time block end date
				final Date varianceStartDate = unionVarianceDateAndTime(scheduleVarianceRecord.getDateStart(),
						scheduleVarianceRecord.getTimeStart());
				final Date varianceEndDate = unionVarianceDateAndTime(scheduleVarianceRecord.getDateEnd(),
						scheduleVarianceRecord.getTimeEnd());
				// check whether variance overlap with the specify time start
				// and time end.
				if (!(varianceEndDate.before(timeStart) || varianceStartDate.after(timeEnd))) {
					this.delete(scheduleVarianceRecord);
				}
			}
		}

	}

	/** {@inheritDoc} */
	@Override
	public void deleteBusyVariancesInTimePeriod(final Date dateStart, final Date dateEnd) {
		final ParsedRestrictionDef resDef = new ParsedRestrictionDef();
		resDef.addClause("cf_schedules_variances", "sched_vars_type", "BUSY", Operation.EQUALS);
		resDef.addClause("cf_schedules_variances", "date_start", dateStart, Operation.GTE);
		resDef.addClause("cf_schedules_variances", "date_end", dateEnd, Operation.LTE);
		final List<CraftspersonScheduleVariances> craftsperonVariancesList = this.find(resDef);
		for (final CraftspersonScheduleVariances craftspersonVariance : craftsperonVariancesList) {
			this.delete(craftspersonVariance);
		}

	}

	/** {@inheritDoc} */
	@Override
	public void addNewScheduleVariance(final CraftspersonScheduleVariances variance) {
		// TODO Auto-generated method stub

	}

	/** {@inheritDoc} */
	@Override
	public void deleteScheduledVariance(final int scheduledVarianceId) {
		// TODO Auto-generated method stub

	}

	/**
	 * Format variance date object to union specify variance date and variance
	 * time object.
	 *
	 * @param date
	 *            - Variance date.
	 * @param time
	 *            - Variance time.
	 * @return Variance Date and Time.
	 */
	public Date unionVarianceDateAndTime(final Date date, final Date time) {
		// format variance time block end date
		final Calendar varianceCalendar = Calendar.getInstance();
		varianceCalendar.setTime(date);
		final Calendar varianceTimeCalendar = Calendar.getInstance();
		varianceTimeCalendar.setTime(time);
		varianceCalendar.set(Calendar.HOUR_OF_DAY, varianceTimeCalendar.get(Calendar.HOUR_OF_DAY));
		varianceCalendar.set(Calendar.MINUTE, varianceTimeCalendar.get(Calendar.MINUTE));
		varianceCalendar.set(Calendar.SECOND, varianceTimeCalendar.get(Calendar.SECOND));

		return varianceCalendar.getTime();
	}

}
