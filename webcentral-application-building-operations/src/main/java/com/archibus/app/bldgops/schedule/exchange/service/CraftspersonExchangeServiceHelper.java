package com.archibus.app.bldgops.schedule.exchange.service;

import java.net.*;

import org.apache.log4j.Logger;

import com.archibus.app.bldgops.schedule.exchange.domain.CalendarException;
import com.archibus.utility.StringUtil;

import microsoft.exchange.webservices.data.*;

/**
 * Utility Class for Exchange Service configuration.
 *
 * Managed in Spring.
 *
 * @author Bart Vanderschoot
 * @since 21.2
 */
public class CraftspersonExchangeServiceHelper {

	/** Error message indicating the connect failed. */
	// @translatable
	protected static final String CONNECT_FAILED = "Could not connect to Exchange. Please refer to archibus.log for details";

	/**
	 * Error message indicating connecting to Exchange is not enabled for the
	 * given email.
	 */
	// @translatable
	protected static final String NO_EXCHANGE_EMAIL = "The application is not configured to connect to Exchange for email [{0}]";

	/**
	 * Error message indicating the connect failed because the URL is invalid.
	 */
	// @translatable
	private static final String INVALID_EXCHANGE_URL = "Invalid Exchange URL [{0}]";

	/** The logger. */
	protected final Logger logger = Logger.getLogger(this.getClass());

	/** Exchange service status(enabled or disabled). */

	private boolean enabled;

	/** The url. */
	private String url;

	/** The user name. */
	private String userName;

	/** The password. */
	private String password;

	/** the network domain. */
	private String domain;

	/** The proxy server. */
	private String proxyServer;

	/** The proxy port. */
	private Integer proxyPort;

	/** The version of Exchange. */
	private String version;

	/** The domains known on the connected Exchange Server(s). */
	private String[] linkedDomains;

	/**
	 * Create instance of the Exchange Service without impersonation and without
	 * setting the URL.
	 *
	 * @return service EWS Exchange Service
	 */
	public ExchangeService getService() {
		ExchangeVersion exchangeVersion;
		if (this.version == null) {
			// default value
			exchangeVersion = ExchangeVersion.Exchange2010_SP1;
		} else {
			exchangeVersion = ExchangeVersion.valueOf(this.version);
		}

		final ExchangeService exchangeService = new ExchangeService(exchangeVersion);
		// Only try to set the URL if it was specified.
		if (StringUtil.notNullOrEmpty(this.getUrl())) {
			try {
				exchangeService.setUrl(new URI(this.getUrl()));
			} catch (final URISyntaxException exception) {
				throw new CalendarException(INVALID_EXCHANGE_URL, exception, CraftspersonExchangeServiceHelper.class,
						this.getUrl());
			}
		}
		// TODO optional: set user agent
		// exchangeService.setUserAgent("ARCHIBUS");

		// TODO: use encryption for password
		// final Decoder1 decoder = new Decoder1();
		// final String passwordDecrypted = decoder.decode(this.password);

		if (StringUtil.notNullOrEmpty(this.domain)) {
			exchangeService.setCredentials(new WebCredentials(this.userName, this.password, this.domain));
		} else {
			exchangeService.setCredentials(new WebCredentials(this.userName, this.password));
		}

		if (StringUtil.notNullOrEmpty(this.proxyServer) && this.proxyPort != 0) {
			final WebProxy proxy = new WebProxy(this.proxyServer, this.proxyPort);
			proxy.setCredentials(this.userName, this.password, this.domain);
			exchangeService.setWebProxy(proxy);
		}
		return exchangeService;
	}

	/**
	 * Getter for the enabled property.
	 *
	 * @return the enabled property.
	 */
	public boolean isEnabled() {
		return this.enabled;
	}

	/**
	 * Setter for the enabled property.
	 *
	 * @param enabled
	 *            the enabled to set.
	 */
	public void setEnabled(final boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Getter for the linkedDomains property.
	 *
	 * @return the linkedDomains property.
	 */
	public String[] getLinkedDomains() {
		return this.linkedDomains;
	}

	/**
	 * Getter for the Exchange URL property.
	 *
	 * @return the Exchange URL property.
	 */
	public String getUrl() {
		return this.url;
	}

	/**
	 * Setter for the Exchange URL property.
	 *
	 * @param url
	 *            the URL to set
	 */
	public void setUrl(final String url) {
		this.url = url;
	}

	/**
	 * Get the Exchange user name property.
	 *
	 * @return the userName property.
	 */
	public String getUserName() {
		return this.userName;
	}

	/**
	 * Setter for the userName property.
	 *
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(final String userName) {
		this.userName = userName;
	}

	/**
	 * Get the Exchange password property.
	 *
	 * @return the password property.
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * Setter for the password property.
	 *
	 * @param password
	 *            the password to set
	 */
	public void setPassword(final String password) {
		this.password = password;
	}

	/**
	 * Getter for the proxyServer property.
	 *
	 * @return the proxyServer property.
	 */
	public String getProxyServer() {
		return this.proxyServer;
	}

	/**
	 * Setter for the proxyServer property.
	 *
	 * @param proxyServer
	 *            the proxyServer to set
	 */
	public void setProxyServer(final String proxyServer) {
		this.proxyServer = proxyServer;
	}

	/**
	 * Getter for the proxyPort property.
	 *
	 * @return the proxyPort property.
	 */
	public Integer getProxyPort() {
		return this.proxyPort;
	}

	/**
	 * Setter for the proxyPort property.
	 *
	 * @param proxyPort
	 *            the proxyPort to set
	 */
	public void setProxyPort(final Integer proxyPort) {
		this.proxyPort = proxyPort;
	}

	/**
	 * Getter for the version property.
	 *
	 * @return the version property.
	 */
	public String getVersion() {
		return this.version;
	}

	/**
	 * Setter for the version property.
	 *
	 * @param version
	 *            the version to set
	 */
	public void setVersion(final String version) {
		this.version = version;
	}

	/**
	 * Set the linked domains.
	 *
	 * @param linkedDomains
	 *            the linked domains to set
	 */
	public void setLinkedDomains(final String[] linkedDomains) {
		this.linkedDomains = linkedDomains.clone();
	}
	
	

	/**
	 * Getter for the domain property.
	 * 
	 * @return the domain property.
	 */
	public String getDomain() {
		return domain;
	}

	/**
	 * Setter for the domain property.
	 * 
	 * @param domain the domain to set.
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}

	/**
	 * Check whether the given email matches one of the linked domains.
	 *
	 * @param email
	 *            the email address
	 * @return true if a match is found or no linked domains are specified,
	 *         false otherwise
	 */
	public boolean isLinkedDomain(final String email) {
		boolean isLinkedDomain = false;
		if (this.linkedDomains == null || this.linkedDomains.length == 0) {
			isLinkedDomain = true;
		} else if (StringUtil.notNullOrEmpty(email)) {
			for (final String linkedDomain : this.linkedDomains) {
				if (email.toLowerCase().endsWith(linkedDomain.toLowerCase())) {
					isLinkedDomain = true;
					break;
				}
			}
		}
		return isLinkedDomain;
	}

}