package com.archibus.app.bldgops.schedule.domain;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for Employee.
 *
 * @author Jia
 * @since 23.2
 *
 */
public class Employee {
	/**
	 * Employee code.
	 */
	private String emId;
	/**
	 * Employee email.
	 */
	private String email;
	/**
	 * Employee building code.
	 */
	private String blId;

	/**
	 * Getter for the emId property.
	 *
	 * @return the emId property.
	 */
	public String getEmId() {
		return this.emId;
	}

	/**
	 * Setter for the emId property.
	 *
	 * @param emId
	 *            the emId to set.
	 */
	public void setEmId(final String emId) {
		this.emId = emId;
	}

	/**
	 * Getter for the email property.
	 *
	 * @return the email property.
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * Setter for the email property.
	 *
	 * @param email
	 *            the email to set.
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * Getter for the blId property.
	 *
	 * @return the blId property.
	 */
	public String getBlId() {
		return this.blId;
	}

	/**
	 * Setter for the blId property.
	 *
	 * @param blId
	 *            the blId to set.
	 */
	public void setBlId(final String blId) {
		this.blId = blId;
	}

}
