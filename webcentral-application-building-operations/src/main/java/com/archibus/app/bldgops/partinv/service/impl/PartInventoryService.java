package com.archibus.app.bldgops.partinv.service.impl;

import java.util.List;

import org.json.JSONArray;

import com.archibus.app.bldgops.partinv.dao.*;
import com.archibus.app.bldgops.partinv.domain.*;
import com.archibus.app.bldgops.partinv.service.IPartInventoryService;
import com.archibus.app.bldgops.partinv.util.PartInventoryUtility;
import com.archibus.context.ContextStore;
import com.archibus.datasource.data.DataRecord;

/**
 * Bldgops Part Inventory Service holds Workflow Rule methods.
 *
 * <p>
 * History:
 * <li>21.2: Add for Bali3 Bldgops Part Inventory Feature.
 *
 * @author Zhang Yi
 *
 *         <p>
 *         Suppress PMD.TooManyMethods warning.
 *         <p>
 *         Justification: This class contains public methods called from axvw.
 */
@SuppressWarnings({ "PMD.TooManyMethods" })
public class PartInventoryService implements IPartInventoryService {

    /** The part data source. */
    private IPartDao<Part> partDataSource;

    /** The part storage location data source. */
    private IPartStorageLocationDao<PartStorageLocation> partStorageLocationDataSource;

    /** The supply requisition data source. */
    private ISupplyRequisitionDao<SupplyRequisition> supplyRequisitionDataSource;

    /** The part storage location data source. */
    private IPurchaseOrderDao<PurchaseOrder> purchaseOrderDataSource;

    /**
     *
     * Adjusts the status of part estimations as part quantities increase or decrease. The system
     * will call this WFR any time there is a change to any part's Quantity Available
     * (pt.qty_on_hand) or part estimates become "unreserved".
     *
     * @param partId String part code.
     * @param estimatedParts List estimated part records.
     * @param quantity int adjusted part quantity.
     */
    @Override
    public void updateWrptStatus(final String partId, final List<DataRecord> estimatedParts,
            final double quantity) {

        this.partDataSource.updateWrptStatus(partId, quantity);

    }

    /**
     * Adjusts the status of part estimations as part quantities increase or decrease. The system
     * will call this WFR any time there is a change to any part's Quantity Available
     * (pt.qty_on_hand) or part estimates become "unreserved".
     *
     * @param partId String part code.
     * @param partLocId String part storage location code.
     * @param quantity double adjusted part quantity.
     */
    @Override
    public void updateWrptStatusForMpsl(final String partId, final String partLocId,
            final double quantity) {

        this.partStorageLocationDataSource.updateWrptStatus(partId, partLocId, quantity);

    }

    /**
     * check schema changed.
     *
     * @return if necessary schema changes existed for Part Inventory Improvement.
     *
     */
    @Override
    public boolean isSchemaChanged() {

        return PartInventoryUtility.isSchemaChanged();
    }

    /**
     * Create Supply Requisition. by Jia Guoqiang
     *
     * @param fromStoreLoc From storage location code
     * @param toStoreLoc To storage location code
     * @param supplyReqComments Supply Requisition Comments
     * @param itRecords List of part records
     */
    @Override
    public void createSupplyReq(final String fromStoreLoc, final String toStoreLoc,
            final String supplyReqComments, final JSONArray itRecords) {
        ContextStore.get().getBean("BldgopsPartInventoryService");
        this.supplyRequisitionDataSource.createSupplyReq(fromStoreLoc, toStoreLoc,
            supplyReqComments, itRecords);
    }

    /**
     * Add parts to an existing supply requisition. by Jia Guoqiang
     *
     * @param supplyReqId Supply requisition code
     * @param itRecords JSON array of inventory transactions record
     */
    @Override
    public void addPartsToExistingSupplyReq(final String supplyReqId, final JSONArray itRecords) {
        this.supplyRequisitionDataSource.addPartsToExistingSupplyReq(supplyReqId, itRecords);
    }

    /**
     *
     * Add New part to default part storage location.
     *
     * @param partId Part code
     */
    @Override
    public void addNewPartToDefaultPartStoreLoc(final String partId) {
        this.partStorageLocationDataSource.addNewPartToDefaultPartStoreLoc(partId);
    }

    /**
     * Update All Supply Requisition items when part storage location is change.
     *
     * @param supplyReqChangeRecords Supply Requisition Changed Record
     */
    @Override
    public void updateSupplyReqItemWhenPartStoreLocationChange(
            final JSONArray supplyReqChangeRecords) {
        this.supplyRequisitionDataSource
            .updateSupplyReqItemWhenPartStoreLocationChange(supplyReqChangeRecords);

    }

    /**
     *
     * Transfer parts between storage location By Received action of supply requisitions.
     *
     * @param supplyReqId Supply requisition id
     * @param transId Transaction Id
     * @param fromStorageLocation From Storage Location
     * @param toStorageLocation To Storage Location
     * @param partCode Part code
     * @param transQty Transaction Quantity
     * @param status Status
     */
    @Override
    public void transferPartsBetweenStorageLocationBySupplyReq(final String supplyReqId,
            final String transId, final String fromStorageLocation, final String toStorageLocation,
            final String partCode, final Double transQty, final String status) {

        this.supplyRequisitionDataSource.transferPartsBetweenStorageLocationBySupplyReq(supplyReqId,
            transId, fromStorageLocation, toStorageLocation, partCode, transQty, status);
    }

    /**
     *
     * Transfer parts between storage location.
     *
     * @param fromStorageLocation From Storage Location
     * @param toStorageLocation To Storage Location
     * @param partCode Part code
     * @param transQty Transaction Quantity
     * @param qtyReserved Quantity Reserved
     * @param wrptRecords WRPT records
     */
    @Override
    public void transferPartsBetweenStorageLocationByAdjust(final String fromStorageLocation,
            final String toStorageLocation, final String partCode, final Double transQty,
            final Double qtyReserved, final JSONArray wrptRecords) {

        this.supplyRequisitionDataSource.transferPartsBetweenStorageLocationByAdjust(
            fromStorageLocation, toStorageLocation, partCode, transQty, qtyReserved, wrptRecords);
    }

    /**
     *
     * Change Purchase Orders to be Received or Error.
     *
     * @param poId Purchase Order id
     * @param poLineId Purchase Order Item Id
     * @param partCode Part code
     * @param receivedLocation Receiving Location
     * @param transQty Transaction Quantity
     * @param costUnit Cost unit price
     * @param status Status 'Received' or 'Error'
     */
    @Override
    public void setPurchaseOrderTobeReceivedOrError(final String poId, final String poLineId,
            final String partCode, final String receivedLocation, final Double transQty,
            final Double costUnit, final String status) {

        this.purchaseOrderDataSource.setPurchaseOrderTobeReceivedOrError(poId, poLineId, partCode,
            receivedLocation, transQty, costUnit, status);
    }

    /**
     * Update purchase order info by purchase order status.
     *
     * @param poId Purchase order ID.
     * @param beforeStatus Purchase order status before change.
     * @param afterStatus Purchase order status after change.
     */
    @Override
    public void updatePurchaseOrderInfoByPoStatus(final int poId, final String beforeStatus,
            final String afterStatus) {

        this.purchaseOrderDataSource.updatePurchaseOrderInfoByPoStatus(poId, beforeStatus,
            afterStatus);
    }

    /**
     * Save or update purchase order line item.
     *
     * @param poLineItemRecords Purchase order line item JSONArray.
     * @param isNew Flag to save record or update record.
     */
    @Override
    public void savePurchaseOrderLineItem(final JSONArray poLineItemRecords, final boolean isNew) {
        this.purchaseOrderDataSource.savePurchaseOrderLineItem(poLineItemRecords, isNew);
    }

    /**
     * Create new purchase order records.
     *
     * @param poRecord Purchase Order parameter record
     * @param poLineRecords Purchase Order Line Item Records
     *
     */
    @Override
    public void createNewPurchaseOrder(final JSONArray poRecord, final JSONArray poLineRecords) {

        this.purchaseOrderDataSource.createNewPurchaseOrder(poRecord, poLineRecords);
    }

    /**
     * Add record to an existing purchase order.
     *
     * @param poId Purchase Order code
     * @param vnId Vendor code.
     * @param poLineRecords Purchase Order Line Item Records
     */
    @Override
    public void addToExistingPurchaseOrder(final int poId, final String vnId,
            final JSONArray poLineRecords) {

        this.purchaseOrderDataSource.addToExistingPurchaseOrder(poId, vnId, poLineRecords);
    }

    /**
     * Remove item from purchase order line item.
     *
     * @param poId Purchase order code
     * @param poLineId Purchase order line code.
     */
    @Override
    public void removeItemFromPoLine(final int poId, final int poLineId) {
        this.purchaseOrderDataSource.removeItemFromPoLine(poId, poLineId);
    }

    /**
     * Remove aisle code shelf code Cabinet code and bin code.
     *
     * @param ptLocId part storage location
     */
    @Override
    public void removePartLocations(final String ptLocId) {

        this.partStorageLocationDataSource.removePartLocations(ptLocId);
    }

    /**
     * Check is storage location can be delete.
     *
     * @param storeLocId Storage location code.
     * @return if can be deleted ,return true,else, return false.
     */
    @Override
    public boolean checkIsStoreLocCanBeDeleted(final String storeLocId) {
        return PartInventoryUtility.checkIsStoreLocCanBeDeleted(storeLocId);
    }

    /**
     * Update Physical Count.
     *
     * @param storeLocId Storage location code
     * @param partCodeArray Part code array
     * @param qtyPhysicalCount Physical count quantity
     * @param isAdjustQtyOnHand Whether update Quantity Available when update physical count.
     */
    @Override
    public void updatePhysicalCount(final String storeLocId, final JSONArray partCodeArray,
            final Double qtyPhysicalCount, final boolean isAdjustQtyOnHand) {
        this.partStorageLocationDataSource.updatePhysicalCount(storeLocId, partCodeArray,
            qtyPhysicalCount, isAdjustQtyOnHand);
    }

    /**
     * Update Purchase Order.
     *
     * @param purchaseOrderRecords Purchase order field record
     */
    @Override
    public void updatePurchaseOrderInfor(final JSONArray purchaseOrderRecords) {
        this.purchaseOrderDataSource.updatePurchaseOrderInfor(purchaseOrderRecords);
    }

    /**
     * Update supply requisition.
     *
     * @param supplyReqRecods Supply Requisition records.
     */
    @Override
    public void updateSupplyReqInfor(final JSONArray supplyReqRecods) {
        this.supplyRequisitionDataSource.updateSupplyReqInfor(supplyReqRecods);
    }

    /**
     * Save or update the supply requisition item.
     *
     * @param supplyReqItemRecords Supply requisition item records
     * @param isNew Flag to save or update
     */
    @Override
    public void saveSupplyReqItem(final JSONArray supplyReqItemRecords, final boolean isNew) {
        this.supplyRequisitionDataSource.saveSupplyReqItem(supplyReqItemRecords, isNew);
    }

    /**
     * Setter for the partDataSource property.
     *
     * @see partDataSource
     * @param partDataSource the partDataSource to set
     */

    public void setPartDataSource(final IPartDao<Part> partDataSource) {
        this.partDataSource = partDataSource;
    }

    /**
     * Setter for the partStorageLocationDataSource property.
     *
     * @see partStorageLocationDataSource
     * @param partStorageLocationDataSource the partStorageLocationDataSource to set
     */

    public void setPartStorageLocationDataSource(
            final IPartStorageLocationDao<PartStorageLocation> partStorageLocationDataSource) {
        this.partStorageLocationDataSource = partStorageLocationDataSource;
    }

    /**
     * Setter for the supplyRequisitionDataSource property.
     *
     * @see supplyRequisitionDataSource
     * @param supplyRequisitionDataSource the supplyRequisitionDataSource to set
     */

    public void setSupplyRequisitionDataSource(
            final ISupplyRequisitionDao<SupplyRequisition> supplyRequisitionDataSource) {
        this.supplyRequisitionDataSource = supplyRequisitionDataSource;
    }

    /**
     * Setter for the purchaseOrderDataSource property.
     *
     * @see purchaseOrderDataSource
     * @param purchaseOrderDataSource the purchaseOrderDataSource to set
     */

    public void setPurchaseOrderDataSource(
            final IPurchaseOrderDao<PurchaseOrder> purchaseOrderDataSource) {
        this.purchaseOrderDataSource = purchaseOrderDataSource;
    }
}
