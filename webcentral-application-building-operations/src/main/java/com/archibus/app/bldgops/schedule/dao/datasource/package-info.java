/**
 * This package contains datasource objects for Craftsperson Schedule
 * Management.
 **/
package com.archibus.app.bldgops.schedule.dao.datasource;
