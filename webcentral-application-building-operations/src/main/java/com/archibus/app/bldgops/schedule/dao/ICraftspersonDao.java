package com.archibus.app.bldgops.schedule.dao;

import java.util.List;

import com.archibus.app.bldgops.schedule.domain.Craftsperson;
import com.archibus.core.dao.IDao;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides method to operate craftsperson object.
 * <p>
 *
 * Managed by Spring.
 *
 * @param <Craftsperon>
 *            Craftsperson.
 * @author Jia
 * @since 23.2
 *
 */
public interface ICraftspersonDao<Craftsperon> extends IDao<Craftsperson> {
	/**
	 * get craftsperson record by crafsperson code.
	 *
	 * @param cfId
	 *            -Craftsperson Code.
	 * @return Craftsperon Object.
	 */
	Craftsperson getCraftsperson(String cfId);

	/**
	 * Get all craftspersons in craftsperson table.
	 *
	 * @return Craftsperson List.
	 */
	List<Craftsperson> getAllCraftspersons();

	/**
	 * Get craftsperson by email address.
	 *
	 * @param email
	 *            -Email Address.
	 * @return Craftperson.
	 */
	Craftsperson getCraftspersonByEmail(String email);

	/**
	 * Get building code of craftsperson referenced employee's.
	 *
	 * @param craftspersonEmail
	 *            -Craftsperson Email.
	 * @return Building Code.
	 */
	String getReferencedEmployeeBuildingCode(String craftspersonEmail);
}
