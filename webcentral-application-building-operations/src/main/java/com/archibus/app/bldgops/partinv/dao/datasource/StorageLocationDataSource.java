package com.archibus.app.bldgops.partinv.dao.datasource;

import static com.archibus.app.bldgops.partinv.util.PartInventoryConstant.*;

import com.archibus.app.bldgops.partinv.dao.IStorageLocationDao;
import com.archibus.app.bldgops.partinv.domain.StorageLocation;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecordField;

/**
 *
 * Storage location datasource.
 * <p>
 *
 *
 * @author Guo Jiangtao
 * @since 23.2
 *
 */
public class StorageLocationDataSource extends ObjectDataSourceImpl<StorageLocation>
        implements IStorageLocationDao<StorageLocation> {

    /**
     * Field names to property names mapping. All fields will be added to the DataSource.
     */
    private static final String[][] FIELDS_TO_PROPERTIES =
            { { PT_STORE_LOC_ID, "locId" }, { "site_id", "siteId" }, { "bl_id", "blId" },
                    { "fl_id", "flId" }, { "rm_id", "rmId" } };

    /**
     * Constructs StorageLocationDataSource, mapped to <code>pt_store_loc</code> table, using
     * <code>storageLocation</code> bean.
     *
     */
    public StorageLocationDataSource() {
        super("storageLocation", "pt_store_loc");
    }

    /** {@inheritDoc} */

    @Override
    public StorageLocation findLocation(final String locId) {
        final PrimaryKeysValues primaryKeysValues = new PrimaryKeysValues();
        {
            final DataRecordField pkField = new DataRecordField();
            pkField.setName(PT_STORE_LOC_TABLE + DOT + PT_STORE_LOC_ID);
            pkField.setValue(locId);
            primaryKeysValues.getFieldsValues().add(pkField);
        }

        return this.get(primaryKeysValues);
    }

    @Override
    protected String[][] getFieldsToProperties() {
        return FIELDS_TO_PROPERTIES.clone();
    }

}
