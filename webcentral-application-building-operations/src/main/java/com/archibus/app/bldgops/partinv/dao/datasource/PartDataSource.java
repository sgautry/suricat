package com.archibus.app.bldgops.partinv.dao.datasource;

import static com.archibus.app.bldgops.partinv.util.PartInventoryConstant.*;

import java.util.List;

import com.archibus.app.bldgops.partinv.dao.*;
import com.archibus.app.bldgops.partinv.domain.*;
import com.archibus.app.bldgops.partinv.util.PartInventoryUtility;
import com.archibus.datasource.*;
import com.archibus.datasource.data.*;
import com.archibus.eventhandler.resourcecalcs.ResourceCalculations;

/**
 *
 * Part datasource.
 * <p>
 *
 *
 * @author Guo Jiangtao
 * @since 23.2
 *
 */
public class PartDataSource extends ObjectDataSourceImpl<Part> implements IPartDao<Part> {

    /**
     * Field names to property names mapping. All fields will be added to the DataSource.
     */
    private static final String[][] FIELDS_TO_PROPERTIES = { { "part_id", "partId" },
            { "qty_on_hand", "qtyOnHand" }, { "qty_on_reserve", "qtyOnReserve" },
            { "qty_physical_count", "qtyPhysicalCount" }, { "qty_to_order", "qtyToOrder" },
            { "qty_min_hand", "qtyMinHand" }, { "cost_unit_std", "costUnitStd" },
            { "cost_unit_last", "costUnitLast" }, { "acc_prop_type", "accPropType" } };

    /**
     * work reuqest part dataSource.
     */
    private IWorkRequestPartDao<WorkRequestPart> workRequestPartDataSource;

    /**
     * Constructs PartDataSource, mapped to <code>pt</code> table, using <code>part</code> bean.
     *
     */
    public PartDataSource() {
        super("part", "pt");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Part getPart(final String partId) {
        final PrimaryKeysValues primaryKeysValues = new PrimaryKeysValues();
        {
            final DataRecordField pkField = new DataRecordField();
            pkField.setName(PT_TABLE + DOT + PART_ID);
            pkField.setValue(partId);
            primaryKeysValues.getFieldsValues().add(pkField);
        }

        return this.get(primaryKeysValues);
    }

    @Override
    protected String[][] getFieldsToProperties() {
        return FIELDS_TO_PROPERTIES.clone();
    }

    /** {@inheritDoc} */

    @Override
    public void updateWrptStatus(final String partId, final double quantity) {

        if (quantity > 0) {

            this.updateStatusForQuantityIncrease(partId, quantity);

        } else if (quantity < 0) {

            this.updateStatusForQuantityDecrease(partId, quantity);
        }

        this.calculateReservedQuantityOfPart(partId);

    }

    /** {@inheritDoc} */

    @Override
    public void updateWrptStatusForWrReject(final String partCode) {
        this.updateStatusForQuantityIncrease(partCode, 0);

    }

    /**
     *
     * Update estimated parts status when available quantity increases.
     *
     * @param partId string part code.
     * @param quantity double adjusted part quantity.
     */
    public void updateStatusForQuantityIncrease(final String partId, final double quantity) {

        // Query 'Not In Stock' estimated parts
        final List<WorkRequestPart> estimatedParts = this.workRequestPartDataSource
            .findEstimatedParts(partId, new String[] { NOT_IN_STOCK });

        // Query to get current Part object
        final Part part = getPart(partId);

        // calculate new available quantity
        final double newAvailableQuantity = part.getQtyOnHand() + quantity;

        // Loop all estimated parts to check if the quantity estimated is less than or equal to the
        // part's new Quantity Available,then change the status of the estimate to "In Stock, not
        // Reserved"
        for (final WorkRequestPart estimatePart : estimatedParts) {

            final double estimatedQuantity = estimatePart.getQtyEstimated();
            if (newAvailableQuantity >= estimatedQuantity) {
                estimatePart.setStatus(NOT_RESERVED);
                this.workRequestPartDataSource.update(estimatePart);
            }
        }

        part.setQtyOnHand(newAvailableQuantity);
        update(part);
    }

    /**
     *
     * Update estimated parts status when available quantity decreases.
     *
     * @param partId string part code.
     * @param quantity double adjusted part quantity.
     */
    public void updateStatusForQuantityDecrease(final String partId, final double quantity) {

        // Query 'In Stock, Not Reserved' estimated parts
        final List<WorkRequestPart> estimatedParts = this.workRequestPartDataSource
            .findEstimatedParts(partId, new String[] { NOT_RESERVED });

        // Query to get current Part object
        final Part part = getPart(partId);

        // calculate new available quantity
        final double newAvailableQuantity = part.getQtyOnHand() + quantity;

        // Loop all estimated parts to check if the quantity estimated is greater than the
        // part's new Quantity Available,then change the status of the estimate to "Not In Stock"
        for (final WorkRequestPart estimatePart : estimatedParts) {

            final double estimatedQuantity = estimatePart.getQtyEstimated();
            if (newAvailableQuantity < estimatedQuantity) {

                estimatePart.setStatus(NOT_IN_STOCK);
                this.workRequestPartDataSource.update(estimatePart);
            }
        }

        part.setQtyOnHand(newAvailableQuantity);
        update(part);
    }

    /**
     *
     * Calculate part's reserved quantity after all part estimations update.
     *
     * @param partId string part code.
     */
    private void calculateReservedQuantityOfPart(final String partId) {

        final List<WorkRequestPart> allEstimatedParts =
                this.workRequestPartDataSource.findEstimatedParts(partId);

        double reservedQuantity = 0;

        for (final WorkRequestPart estimatePart : allEstimatedParts) {

            if (RESERVED.equalsIgnoreCase(estimatePart.getStatus())) {

                reservedQuantity += estimatePart.getQtyEstimated();
            }
        }

        // Query to get current Part object
        final Part part = getPart(partId);
        part.setQtyOnReserve(reservedQuantity);
        update(part);

    }

    /**
     *
     * When a part estimation is changed, update the part's available quantity and its other
     * associated wrpt's status.
     *
     * @param partId int work request part id.
     * @param wrId int work request id.
     * @param date String assigned date.
     * @param time String assigned time.
     * @param difference double difference of part estimated quantity.
     */
    public void adjustPartEstimationQuantity(final String partId, final int wrId, final String date,
            final String time, final double difference) {

        final WorkRequestPart estimatePart =
                this.workRequestPartDataSource.findEstimatedPart(partId, wrId, date, time);

        final double newEstimateQty = estimatePart.getQtyEstimated();
        final double oldEstimateQty = newEstimateQty - difference;
        final String status = estimatePart.getStatus();

        // Query to get current Part object
        final Part part = getPart(partId);
        final double availableQuantity = part.getQtyOnHand();

        // if no estimated quantity change then return
        if (difference == 0) {
            return;
        }

        final double quantity = PartInventoryUtility.updateStatusOfWrpt(estimatePart, status,
            difference, newEstimateQty, oldEstimateQty, availableQuantity);

        this.workRequestPartDataSource.update(estimatePart);

        if (quantity != 0) {

            this.updateWrptStatus(partId, quantity);
        }
    }

    /**
     *
     * Re-set status of wrpt that just changed the estimated quantity and is not 'reserved'.
     *
     * @param partId int work request part id.
     * @param estimatePart DataRecord wrpt record.
     * @param status String wrpt status.
     */
    public void updateStatusForRequestClose(final String partId, final DataRecord estimatePart,
            final String status) {

        // Query to get current Part object
        final Part part = getPart(partId);

        final double estimateQty = estimatePart.getDouble(WRPT_QTY_ESTIMATED);
        final double actualQty = estimatePart.getDouble("wrpt.qty_actual");
        final double availableQty = part.getQtyOnHand();
        final double reservedQty = part.getQtyOnReserve();
        final double difference = actualQty - estimateQty;

        // kb3043739: when closing a Not reserved part estimation, just take its actual quantity
        // used as the difference between estimation and actualy used.
        if (NOT_IN_STOCK.equalsIgnoreCase(status) || NOT_RESERVED.equalsIgnoreCase(status)) {

            if (actualQty > 0) {
                this.updateStatusForQuantityDecrease(partId, -actualQty);
            }

        } else {
            if (difference > 0) {

                if (difference <= availableQty) {

                    part.setQtyOnReserve(reservedQty - estimateQty);
                    update(part);

                    this.updateStatusForQuantityDecrease(partId, -difference);

                } else if (difference > availableQty) {

                    this.unReserveAllPartEstimations(partId);
                    this.updateStatusForQuantityDecrease(partId, -difference);
                }

            } else if (difference < 0) {

                part.setQtyOnReserve(reservedQty - estimateQty);
                update(part);

                this.updateStatusForQuantityIncrease(partId, -difference);

            } else {
                // kb#3044675: also update part's available quantity when actual used is equal to
                // estimated
                part.setQtyOnReserve(reservedQty - estimateQty);
                update(part);
            }
        }
    }

    /**
     *
     * Update reserved estimated parts to be 'In Stock Not Reserved', recalculate reserved quantity
     * and available quantity.
     *
     * @param partId int work request part id.
     */
    private void unReserveAllPartEstimations(final String partId) {

        // Query 'Reserved' estimated parts
        final List<WorkRequestPart> estimatedParts = this.workRequestPartDataSource
            .findEstimatedParts(partId, new String[] { RESERVED });

        // Loop all reserved estimated parts to get the sum reserved quantity
        double sumReserved = 0;
        for (final WorkRequestPart estimatePart : estimatedParts) {

            final double estimatedQuantity = estimatePart.getQtyEstimated();
            sumReserved += estimatedQuantity;
            estimatePart.setStatus(NOT_RESERVED);

            this.workRequestPartDataSource.update(estimatePart);
        }

        // Query to get current Part object
        final Part part = getPart(partId);
        part.setQtyOnReserve(0);
        part.setQtyOnHand(sumReserved + part.getQtyOnHand());
        update(part);
    }

    /**
     *
     * Re-set status of part estimations as well as part's quantity values since directly setting of
     * part's phsical count.
     *
     * @param partId int work request part id.
     *
     */
    public void updateWrptStatusByPhsicalCount(final String partId) {
        // Query to get current Part object
        final Part part = getPart(partId);
        final double reservedQty = part.getQtyOnReserve();
        final double phsicalQty = part.getQtyPhysicalCount();
        final double availableQty = part.getQtyOnHand();

        if (phsicalQty < reservedQty) {

            this.unReserveAllPartEstimations(partId);

            this.updateStatusForQuantityDecrease(partId, phsicalQty - reservedQty - availableQty);
            // kb#3043789: also need to process 'Not In Stock' part estimations even after quantity
            // decrease.
            this.updateStatusForQuantityIncrease(partId, 0);

        } else if (phsicalQty < reservedQty + availableQty) {

            this.updateStatusForQuantityDecrease(partId, phsicalQty - reservedQty - availableQty);

        } else if (phsicalQty > reservedQty + availableQty) {

            this.updateStatusForQuantityIncrease(partId, phsicalQty - reservedQty - availableQty);
        }
    }

    /**
     * Setter for the workRequestPartDataSource property.
     *
     * @see workRequestPartDataSource
     * @param workRequestPartDataSource the workRequestPartDataSource to set
     */

    public void setWorkRequestPartDataSource(
            final IWorkRequestPartDao<WorkRequestPart> workRequestPartDataSource) {
        this.workRequestPartDataSource = workRequestPartDataSource;
    }

    /** {@inheritDoc} */

    @Override
    public void update(final Part part) {
        super.update(part);
        // KB3052725-Automatically update Quantity of Understocked Parts after part quantity changes
        new ResourceCalculations().calculateUnderstockedParts();
    }

}
