package com.archibus.app.bldgops.partinv.dao;

import com.archibus.core.dao.IDao;

/**
 *
 * Provides methods to load Part.
 * <p>
 *
 * @author Guo Jiangtao
 * @since 23.2
 *
 * @param <Part> type of object
 */
public interface IPartDao<Part> extends IDao<Part> {
    /**
     * Get Part by name.
     *
     * @param partId part code
     * @return Part object
     */
    Part getPart(final String partId);

    /**
     * Update wrpt status.
     *
     * @param partCode part code
     * @param quantity quantity
     */
    void updateWrptStatus(final String partCode, final double quantity);

    /**
     * Update wrpt status after reject work request.
     *
     * @param partCode part code
     */
    void updateWrptStatusForWrReject(final String partCode);
}
