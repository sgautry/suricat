package com.archibus.app.bldgops.partinv.dao;

import org.json.JSONArray;

import com.archibus.core.dao.IDao;

/**
 *
 * Provides methods to load Supply Requisition.
 * <p>
 *
 * @author Guo Jiangtao,Jia
 * @since 23.2
 *
 * @param <SupplyRequisition> type of object
 */
public interface ISupplyRequisitionDao<SupplyRequisition> extends IDao<SupplyRequisition> {

    /**
     * Get SupplyRequisition by id.
     *
     * @param supplyReqId Supply Requisition code
     * @return SupplyRequisition object
     */
    SupplyRequisition findSupplyRequisition(final int supplyReqId);

    /**
     * Create Supply Requisition.
     *
     * @param fromStoreLoc From storage location code
     * @param toStoreLoc To storage location code
     * @param supplyReqComments Supply Requisition Comments
     * @param itRecords List of part records
     */
    void createSupplyReq(final String fromStoreLoc, final String toStoreLoc,
            final String supplyReqComments, final JSONArray itRecords);

    /**
     * Add parts to an existing supply requisition. by Jia Guoqiang
     *
     * @param supplyReqId Supply requisition code
     * @param itRecords JSON array of inventory transactions record
     */
    void addPartsToExistingSupplyReq(final String supplyReqId, final JSONArray itRecords);

    /**
     * Update All Supply Requisition items when part storage location is change.
     *
     * @param supplyReqChangeRecords Supply Requisition Changed Record
     */
    void updateSupplyReqItemWhenPartStoreLocationChange(final JSONArray supplyReqChangeRecords);

    /**
     *
     * Transfer parts between storage location By Received action of supply requisitions.
     *
     * @param supplyReqId Supply requisition id
     * @param transId Transaction Id
     * @param fromStorageLocation From Storage Location
     * @param toStorageLocation To Storage Location
     * @param partCode Part code
     * @param transQty Transaction Quantity
     * @param status Status
     */
    void transferPartsBetweenStorageLocationBySupplyReq(final String supplyReqId,
            final String transId, final String fromStorageLocation, final String toStorageLocation,
            final String partCode, final Double transQty, final String status);

    /**
     *
     * Transfer parts between storage location.
     *
     * @param fromStorageLocation From Storage Location
     * @param toStorageLocation To Storage Location
     * @param partCode Part code
     * @param transQty Transaction Quantity
     * @param qtyReserved Quantity Reserved
     * @param wrptRecords WRPT records
     */
    void transferPartsBetweenStorageLocationByAdjust(final String fromStorageLocation,
            final String toStorageLocation, final String partCode, final Double transQty,
            final Double qtyReserved, final JSONArray wrptRecords);

    /**
     * Update supply requisition.
     *
     * @param supplyReqRecods Supply Requisition records.
     */
    void updateSupplyReqInfor(JSONArray supplyReqRecods);

    /**
     * Save or update the supply requisition item.
     *
     * @param supplyReqItemRecords Supply requisition item records
     * @param isNew Flag to save or update
     */
    void saveSupplyReqItem(final JSONArray supplyReqItemRecords, final boolean isNew);
}
