package com.archibus.app.bldgops.partinv.dao;

import java.util.List;

import org.json.*;

import com.archibus.core.dao.IDao;

/**
 *
 * Provides methods to load transaction.
 * <p>
 *
 * @author Guo Jiangtao,Jia
 * @since 23.2
 *
 * @param <Transaction> type of object
 */
public interface ITransactionDao<Transaction> extends IDao<Transaction> {

    /**
     * Get transaction object by transaction id.
     *
     * @param transId transaction code
     * @return Transaction object
     */
    Transaction findTransaction(final int transId);

    /**
     * Get transaction object by supply requisition id..
     *
     * @param supplyReqId supply requisition code
     * @return Transaction object
     */
    Transaction findTransactionBySupplyReq(final int supplyReqId);

    /**
     * Get transaction that not received and not error.
     *
     * @param supplyReqId supply requisition code
     * @return Transaction list
     */
    List<Transaction> findNotReceivedAndErrorItems(final int supplyReqId);

    /**
     * Get transaction that received .
     *
     * @param supplyReqId supply requisition code
     * @return transaction list
     */
    List<Transaction> findReceivedItems(final int supplyReqId);

    /**
     * Get transaction item that error .
     *
     * @param supplyReqId supply requisition code
     * @return transaction list
     */
    List<Transaction> findErrorItems(final int supplyReqId);

    /**
     * Add new supply requisition items .
     *
     * @param supplyReqId supply requisition code
     * @param itRecords it json array
     * @param fromStoreLoc from location id
     * @param toStoreLoc to location id
     */
    void addNewSupplyReqItems(final int supplyReqId, final JSONArray itRecords,
            final String fromStoreLoc, final String toStoreLoc);

    /**
     *
     * Update cost field value after purchase order received.
     *
     * @param transId Transaction Id
     * @param poId Purchase Order Code
     * @param poLineId Purchase Order Item Code
     * @param transQty Quantity to Transfer
     * @param costUnit Unit cost + per quantity shipping and tax
     * @param toStorageLocation To Storage Location
     * @param partCode Part code
     */
    void updateCostValueAfterPoReceived(final int transId, final String poId, final String poLineId,
            final Double transQty, final Double costUnit, final String toStorageLocation,
            final String partCode);

    /**
     *
     * Update cost field value after supply requisition received.
     *
     * @param transId Transaction Id
     * @param partCode Part code
     * @param fromLocation from part location
     * @param transQty Quantity to Transfer
     */
    void updateCostValueAfterSupplyReqReceived(final int transId, final String partCode,
            final String fromLocation, final Double transQty);

    /**
     *
     * Update supply requisition item when location change.
     *
     * @param supplyReqChangeRecord supplyReqChangeRecord
     */
    void updateSupplyReqItemWhenPartStoreLocationChange(final JSONObject supplyReqChangeRecord);

    /**
     *
     * Update transaction status.
     *
     * @param transId transaction code
     * @param status transaction status
     */
    void updateTransactionStatus(final int transId, final String status);

}
