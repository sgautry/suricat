package com.archibus.app.bldgops.partinv.dao.datasource;

import static com.archibus.app.bldgops.partinv.util.PartInventoryConstant.*;

import java.util.Date;

import org.json.*;

import com.archibus.app.bldgops.partinv.dao.*;
import com.archibus.app.bldgops.partinv.domain.*;
import com.archibus.app.bldgops.partinv.util.PartInventoryUtility;
import com.archibus.context.ContextStore;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecordField;
import com.archibus.eventhandler.resourcecalcs.ResourceCalculations;
import com.archibus.utility.*;

/**
 *
 * Purchase Order datasource.
 * <p>
 *
 *
 * @author Guo Jiangtao , Jia
 * @since 23.2
 *
 */
public class PurchaseOrderDataSource extends ObjectDataSourceImpl<PurchaseOrder>
		implements IPurchaseOrderDao<PurchaseOrder> {

	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { "po_id", "poId" }, { STATUS, "status" },
			{ "approved_by", "approvedBy" }, { "date_received", "dateReceived" }, { "vn_id", "vnCode" },
			{ "receiving_location", "receivingLocation" }, { "date_approved", "dateApproved" },
			{ "date_request", "dateRequest" }, { "em_id", "emCode" }, { "ac_id", "acCode" }, { "po_number", "poNum" },
			{ SOURCE, SOURCE }, { COMMENTS, "comments" }, { "date_sent", "dateSent" },
			{ "amount_approved", "amountApproved" }, { "date_paid", "datePaid" }, { "federal_tax", "federalTax" },
			{ "state_tax", "stateTax" }, { SHIPPING, SHIPPING }, { "shipping_em_id", "shippingEmId" },
			{ "billing_em_id", "billingEmId" }, { "ship_address1", "shipAddress1" },
			{ "bill_address1", "billAddress1" }, { "ship_address2", "shipAddress2" },
			{ "bill_address2", "billAddress2" }, { "ship_city_id", "shipCityId" }, { "bill_city_id", "billCityId" },
			{ "ship_state_id", "shipStateId" }, { "bill_state_id", "billStateId" }, { "ship_zip", "shipZip" },
			{ "bill_zip", "billZip" } };

	/**
	 * Purchase order line item dataSource.
	 */
	private IPurchaseOrderLineItemDao<PurchaseOrderLineItem> purchaseOrderLineItemDataSource;

	/**
	 * Part storage location dataSource.
	 */
	private IPartStorageLocationDao<PartStorageLocation> partStorageLocDataSource;

	/**
	 * Part storage location dataSource.
	 */
	private ITransactionDao<Transaction> transactionDataSource;

	/**
	 * Constructs PurchaseOrderDataSource, mapped to <code>po</code> table,
	 * using <code>purchaseOrder</code> bean.
	 *
	 */
	public PurchaseOrderDataSource() {
		super("purchaseOrder", "po");
		this.setApplyVpaRestrictions(false);
	}

	/** {@inheritDoc} */

	@Override
	public PurchaseOrder findPurchaseOrder(final int poId) {
		final PrimaryKeysValues primaryKeysValues = new PrimaryKeysValues();
		{
			final DataRecordField pkField = new DataRecordField();
			pkField.setName(PO_TABLE + DOT + PO_ID);
			pkField.setValue(poId);
			primaryKeysValues.getFieldsValues().add(pkField);
		}

		return this.get(primaryKeysValues);
	}

	/** {@inheritDoc} */

	@Override
	public void setPurchaseOrderTobeReceivedOrError(final String poId, final String poLineId, final String partCode,
			final String receivedLocation, final Double transQty, final Double costUnit, final String status) {

		String location = "";
		if (StringUtil.isNullOrEmpty(receivedLocation)) {
			// Get main storage location
			location = PartInventoryUtility.getMainStorageLocation();
		} else {
			location = receivedLocation;
		}

		// If part not exists in To Storage Location , then save a new record to
		// part storage
		// location.
		this.partStorageLocDataSource.addNewPartToStoreLocIfNotExists(location, partCode);

		// update po line item status
		this.purchaseOrderLineItemDataSource.updatePoLineStatus(Integer.parseInt(poId), Integer.parseInt(poLineId),
				status);

		// check to see if there are any remaining po line items that are not
		// Received or Error
		if (this.purchaseOrderLineItemDataSource.findNotReceivedAndErrorItems(Integer.parseInt(poId)).isEmpty()) {
			final String poStatus = determinePoStatus(Integer.parseInt(poId));

			// update po status
			final PurchaseOrder poObject = this.findPurchaseOrder(Integer.parseInt(poId));
			poObject.setStatus(poStatus);
			poObject.setDateReceived(Utility.currentDate());
			update(poObject);

		}

		if (STATUS_RECEIVED.equals(status)) {

			// call WFR to transfer parts and calculate part count.
			final Double changeQty = transQty;
			final Double costUnitPrice = costUnit;
			if (changeQty > 0) {
				final ResourceCalculations resourceCalculate = new ResourceCalculations();
				final int transId = resourceCalculate.updatePartsAndITForMPSL(partCode, changeQty, costUnitPrice,
						IT_TRANS_TYPE_VALUE_ADD_NEW, null, location, null);
				// Update cost field value of IT table.
				this.transactionDataSource.updateCostValueAfterPoReceived(transId, poId, poLineId, transQty, costUnit,
						location, partCode);
			}
		}

	}

	/** {@inheritDoc} */

	@Override
	public void updatePurchaseOrderInfoByPoStatus(final int poId, final String beforeStatus, final String afterStatus) {
		final String currentUserName = ContextStore.get().getUser().getName();
		final PurchaseOrder poObject = this.findPurchaseOrder(poId);

		if (STATUS_ISSUED.equals(afterStatus)) {
			poObject.setDateSent(Utility.currentDate());
			poObject.setStatus(afterStatus);
			this.purchaseOrderLineItemDataSource.updatePoLineStatus(poId, "Issued");
		} else {
			if (STATUS_ISSUED.equals(beforeStatus)) {
				this.purchaseOrderLineItemDataSource.updatePoLineStatus(poId, "Not Yet Issued");
			}
		}

		final boolean isApproved = "Approved".equals(afterStatus);
		if (isApproved || "Rejected".equals(afterStatus)) {
			poObject.setDateApproved(Utility.currentDate());
			poObject.setApprovedBy(currentUserName);
			poObject.setStatus(afterStatus);

			if (isApproved) {
				updateAmountApproved(poId);
			}
		}
		if ("Requested".equals(afterStatus)) {
			poObject.setDateApproved(null);
			poObject.setDateSent(null);
			poObject.setApprovedBy(null);
			poObject.setAmountApproved(0);
			poObject.setStatus(afterStatus);

		}

		update(poObject);

	}

	/** {@inheritDoc} */

	@Override
	public void createNewPurchaseOrder(final JSONArray poRecord, final JSONArray poLineRecords) {
		// check if the rule specifies the security group, and the user is a
		// member of this group
		boolean isMemberOfPoApprover = false;
		if (ContextStore.get().getUser().isMemberOfGroup(SECURITY_GROUP_PO_APPROVER)) {
			isMemberOfPoApprover = true;
		}

		if (poLineRecords.length() > 0) {

			final JSONObject poJSONRecord = poRecord.getJSONObject(0);
			final String receivingLoc = poJSONRecord.getString("receivingLoc");
			final String vnId = poJSONRecord.getString("vnId");
			final String emId = poJSONRecord.getString("emId");

			PurchaseOrder poObject = new PurchaseOrder();
			poObject.setVnCode(vnId);
			poObject.setReceivingLocation(receivingLoc);
			poObject.setEmCode(emId);
			poObject.setDateRequest(Utility.currentDate());
			String status = "";
			if (isMemberOfPoApprover) {
				status = STATUS_APPROVED;
			} else {
				status = STATUS_REQUESTED;
			}

			poObject.setStatus(status);
			poObject.setAcCode(poJSONRecord.getString("acId"));
			poObject.setPoNum(poJSONRecord.getString("poNumber"));
			poObject.setSource(poJSONRecord.getString(SOURCE));
			poObject.setComments(poJSONRecord.getString(COMMENTS));

			poObject = save(poObject);
			final int poId = poObject.getPoId();

			// Update Building information by storage location code
			updatePoBuildingInforByStorageLocationCode(poId, receivingLoc);

			// Save purchase order item record by poLineRecords parameter.
			this.purchaseOrderLineItemDataSource.addNewPoLineItem(poLineRecords, poId, vnId, emId);

			if (STATUS_APPROVED.equals(status)) {
				// Update purchase order information when purchase order's
				// status is 'Approved'
				updatePurchaseOrderInfoByPoStatus(poId, "", STATUS_APPROVED);
			}
		}

	}

	/** {@inheritDoc} */

	@Override
	public void addToExistingPurchaseOrder(final int poId, final String vnId, final JSONArray poLineRecords) {

		final PurchaseOrder poObject = this.findPurchaseOrder(poId);
		final String approvedBy = poObject.getApprovedBy();
		final String poStatus = poObject.getStatus();
		if (poLineRecords.length() > 0) {
			// Save purchase order item record by poLineRecords parameter.
			this.purchaseOrderLineItemDataSource.addNewPoLineItem(poLineRecords, poId, vnId, poObject.getEmCode());
		}

		String beforeStatus;
		String afterStatus;

		if (poStatus.equals(STATUS_APPROVED)) {
			if (ContextStore.get().getUser().isMemberOfGroup(SECURITY_GROUP_PO_APPROVER)) {
				final String userName = ContextStore.get().getUser().getName();

				beforeStatus = STATUS_APPROVED;
				afterStatus = getPoAfterStatus(userName, approvedBy);

			} else {
				beforeStatus = STATUS_APPROVED;
				afterStatus = STATUS_REQUESTED;
			}

			updatePurchaseOrderInfoByPoStatus(poId, beforeStatus, afterStatus);
		}

	}

	/** {@inheritDoc} */
	@Override
	public void removeItemFromPoLine(final int poId, final int poLineId) {
		// delete the purchase order line item
		final PurchaseOrderLineItem poLineItem = new PurchaseOrderLineItem();
		poLineItem.setPoId(poId);
		poLineItem.setPoLineId(poLineId);
		this.purchaseOrderLineItemDataSource.delete(poLineItem);

		// delete purchase order if it does not has purchase order line
		// item,else , update purchase
		// order status.
		if (this.purchaseOrderLineItemDataSource.findAllItems(poId).size() > 0) {
			// update purchase order status based on the remaining purchase
			// order line item.
			if (this.purchaseOrderLineItemDataSource.findNotReceivedAndErrorItems(poId).isEmpty()) {
				final PurchaseOrder purchaseOrderObject = this.findPurchaseOrder(poId);
				purchaseOrderObject.setPoId(poId);
				purchaseOrderObject.setStatus(determinePoStatus(poId));
				update(purchaseOrderObject);
			}

		} else {
			// delete purchase order if it not has items.
			final PurchaseOrder purchaseOrder = new PurchaseOrder();
			purchaseOrder.setPoId(poId);
			delete(purchaseOrder);
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 */
	@Override
	public void updatePurchaseOrderInfor(final JSONArray purchaseOrderRecords) {
		if (purchaseOrderRecords.length() > 0) {
			for (int i = 0; i < purchaseOrderRecords.length(); i++) {
				final JSONObject poRecord = purchaseOrderRecords.getJSONObject(i);
				final int poId = poRecord.getInt("po.po_id");
				final PurchaseOrder poObject = this.findPurchaseOrder(poId);

				final String beforeStatus = poObject.getStatus();
				final String afterStatus = poRecord.getString(PO_TABLE + DOT + STATUS);

				// set value from JSONObject.
				setValueFromJSONObject(poObject, poRecord);

				// update field value.
				update(poObject);

				// update purchase order status after record saved
				if (!beforeStatus.equals(afterStatus)) {
					updatePurchaseOrderInfoByPoStatus(poId, beforeStatus, afterStatus);
				}
			}
		}

	}

	/** {@inheritDoc} */
	@Override
	public void savePurchaseOrderLineItem(final JSONArray poLineItemRecords, final boolean isNew) {
		if (poLineItemRecords.length() > 0) {
			for (int i = 0; i < poLineItemRecords.length(); i++) {
				final JSONObject poLineRecord = poLineItemRecords.getJSONObject(i);
				// get purchase order line item domain object from JSONArray.
				final PurchaseOrderLineItem poLineObject = this.setPoLineFieldsValueFromJSONArray(poLineRecord, isNew);

				// Save purchase order line item record
				if (isNew) {
					this.purchaseOrderLineItemDataSource.save(poLineObject);
				} else {
					this.purchaseOrderLineItemDataSource.update(poLineObject);
				}
				// get field value
				final int poId = poLineRecord.getInt(PO_LINE_TABLE + DOT + PO_ID);
				final int poLineId = poLineRecord.getInt(PO_LINE_TABLE + DOT + PO_LINE_ID);
				// re-update unit cost to add shipping and tax.
				setAllPoLineUnitCostByShippingAndTax(poId, poLineId);
				final PurchaseOrderLineItem poLineObjectNew = this.purchaseOrderLineItemDataSource
						.findPurchaseOrderLineItem(poId, poLineId);

				final PurchaseOrder poObject = this.findPurchaseOrder(poId);
				final String receivingLoction = poObject.getReceivingLocation();
				final String partCode = poLineRecord.getString("po_line.vfPartId");
				final int quantity = poLineObjectNew.getQunatity();

				final Double unitCost = poLineObjectNew.getUnitCost();

				final String status = poLineObjectNew.getStatus();
				// call method to set purchase order to be received if
				// status='Received'
				setPurchaseOrderTobeReceivedOrError(String.valueOf(poId), String.valueOf(poLineId), partCode,
						receivingLoction, (double) quantity, unitCost, status);
				// If purchase order status is Approved, then change status
				// based on current user.
				final String poStatus = poObject.getStatus();
				if (STATUS_APPROVED.equals(poStatus) && isNew) {
					final String approveBy = poObject.getApprovedBy();
					final String currentUser = ContextStore.get().getUser().getName();
					final String afterStatus = ContextStore.get().getUser().isMemberOfGroup(SECURITY_GROUP_PO_APPROVER)
							&& currentUser.equals(approveBy) ? STATUS_APPROVED : STATUS_REQUESTED;

					updatePurchaseOrderInfoByPoStatus(poId, STATUS_APPROVED, afterStatus);
				}

			}
		}
	}

	/**
	 * Set purchase order line item value from JSONArray.
	 *
	 * @param poLineItemRecord
	 *            Purchase order line JSONObject.
	 * @param isNew
	 *            Flag to save or update.
	 * @return Purchase Order domin object.
	 */
	public PurchaseOrderLineItem setPoLineFieldsValueFromJSONArray(final JSONObject poLineItemRecord,
			final boolean isNew) {
		PurchaseOrderLineItem poLineItemObject;
		final int poId = poLineItemRecord.getInt(PO_LINE_TABLE + DOT + PO_ID);
		int poLineId;
		final String status = poLineItemRecord.getString(PO_LINE_TABLE + DOT + STATUS);
		final String catNo = poLineItemRecord.getString(PO_LINE_TABLE + DOT + CATNO);
		final String description = poLineItemRecord.getString(PO_LINE_TABLE + DOT + DESCRIPTION);
		final int quantity = poLineItemRecord.getInt(PO_LINE_TABLE + DOT + QUANTITY);
		final Double unitCost = poLineItemRecord.getDouble(PO_LINE_TABLE + DOT + UNIT_COST);
		final String emId = poLineItemRecord.getString(PO_LINE_TABLE + DOT + EM_ID);

		if (isNew) {
			poLineItemObject = new PurchaseOrderLineItem();
			poLineItemObject.setPoId(poId);
			poLineId = DataStatistics.getInt(PO_LINE_TABLE, PO_LINE_ID, "MAX", "po_id=" + poId) + 1;
			poLineItemObject.setPoLineId(poLineId);
		} else {
			poLineId = poLineItemRecord.getInt(PO_LINE_TABLE + DOT + PO_LINE_ID);
			poLineItemObject = this.purchaseOrderLineItemDataSource.findPurchaseOrderLineItem(poId, poLineId);
		}
		poLineItemObject.setStatus(status);
		poLineItemObject.setCatNo(catNo);
		poLineItemObject.setDescription(description);
		poLineItemObject.setQunatity(quantity);
		poLineItemObject.setUnitCost(unitCost);
		poLineItemObject.setEmId(emId);

		return poLineItemObject;
	}

	/**
	 * Prepare purchase order object based on purchase order record.
	 *
	 * @param poObject
	 *            Purchase order Object
	 * @param poRecord
	 *            Purchase order JSONObject.
	 */
	private void setValueFromJSONObject(final PurchaseOrder poObject, final JSONObject poRecord) {
		// get field value from json record.
		poObject.setVnCode(poRecord.getString("po.vn_id"));
		poObject.setReceivingLocation(poRecord.getString("po.receiving_location"));
		poObject.setStatus(poRecord.getString("po.status"));
		// poRecord.getString("po.date_paid");
		// new SimpleDateFormat("yyyy-MM-dd");
		Date datePaid = null;
		final String datePaidValue = poRecord.getString("po.date_paid");
		if (StringUtil.notNullOrEmpty(datePaidValue)) {
			datePaid = DateTime.stringToDate(datePaidValue, "yyyy-MM-dd");
		}
		poObject.setDatePaid(datePaid);
		poObject.setComments(poRecord.getString("po.comments"));
		poObject.setAcCode(poRecord.getString("po.ac_id"));
		poObject.setPoNum(poRecord.getString("po.po_number"));
		poObject.setSource(poRecord.getString("po.source"));
		poObject.setFederalTax(poRecord.getDouble("po.federal_tax"));
		poObject.setEmCode(poRecord.getString("po.em_id"));
		poObject.setStateTax(poRecord.getDouble("po.state_tax"));
		poObject.setShipping(poRecord.getDouble("po.shipping"));
		poObject.setShippingEmId(poRecord.getString("po.shipping_em_id"));
		poObject.setBillingEmId(poRecord.getString("po.billing_em_id"));
		poObject.setShipAddress1(poRecord.getString("po.ship_address1"));
		poObject.setBillAddress1(poRecord.getString("po.bill_address1"));
		poObject.setShipAddress2(poRecord.getString("po.ship_address2"));
		poObject.setBillAddress2(poRecord.getString("po.bill_address2"));
		poObject.setShipCityId(poRecord.getString("po.ship_city_id"));
		poObject.setBillCityId(poRecord.getString("po.bill_city_id"));
		poObject.setShipStateId(poRecord.getString("po.ship_state_id"));
		poObject.setBillStateId(poRecord.getString("po.bill_state_id"));
		poObject.setShipZip(poRecord.getString("po.ship_zip"));
		poObject.setBillZip(poRecord.getString("po.bill_zip"));
	}

	/**
	 * Update Puchase order line item Unit Cost by shipping and Tax.
	 *
	 * @param poId
	 *            Purchase order code.
	 * @param poLineId
	 *            Purchase order line item code.
	 *
	 */
	private void setAllPoLineUnitCostByShippingAndTax(final int poId, final int poLineId) {
		final String sql = "update po_line set unit_cost=(po_line.unit_cost+(((po_line.quantity * po_line.unit_cost)/(select ${sql.isNull('sum(quantity*unit_cost)',0)} from po_line PL where PL.po_id=po_line.po_id))*(select ${sql.isNull('(po.federal_tax+po.state_tax+po.shipping)',0)} from po where po.po_id=po_line.po_id))) where po_line.po_id="
				+ poId + " and po_line.po_line_id=" + poLineId;

		SqlUtils.executeUpdate(PO_LINE_TABLE, sql);
	}

	/**
	 * Determine po status.
	 *
	 * @param poId
	 *            purchase order code
	 * @return status
	 */
	private String determinePoStatus(final int poId) {
		String poStatus = STATUS_RECEIVED;

		// Set po status to be Received or Partly Received.
		if (this.purchaseOrderLineItemDataSource.findErrorItems(poId).isEmpty()) {
			poStatus = STATUS_RECEIVED;
		} else {

			poStatus = this.purchaseOrderLineItemDataSource.findReceivedItems(poId).isEmpty() ? STATUS_ERROR
					: STATUS_PARTIALLY_RECEIVED;

		}
		return poStatus;
	}

	/**
	 * Update amount of approved.
	 *
	 * @param poId
	 *            purchase order code
	 */
	private void updateAmountApproved(final int poId) {
		final String updatePoSql = "update po set amount_approved=(select (select sum(po_line.quantity * po_line.unit_cost) from po_line where po_id='"
				+ poId + "')+po.federal_tax+po.state_tax+po.shipping from po where po_id='" + poId + "') where po_id='"
				+ poId + "'  ";

		SqlUtils.executeUpdate(PO_LINE_TABLE, updatePoSql);
	}

	/**
	 * Update purchase orders information from building table by storage
	 * location.
	 *
	 * @param poId
	 *            Purchase Order Code.
	 * @param receivingLoc
	 *            Receiving location.
	 */
	private void updatePoBuildingInforByStorageLocationCode(final int poId, final String receivingLoc) {

		final String updateSql = "update po set "
				+ " ship_address1=(select address1 from bl where bl.bl_id=(select bl_id from pt_store_loc where pt_store_loc_id='"
				+ receivingLoc + "')) ,"
				+ " bill_address1=(select address1 from bl where bl.bl_id=(select bl_id from pt_store_loc where pt_store_loc_id='"
				+ receivingLoc + "') ),"
				+ " ship_address2=(select address2 from bl where bl.bl_id=(select bl_id from pt_store_loc where pt_store_loc_id='"
				+ receivingLoc + "' )),"
				+ " bill_address2=(select address2 from bl where bl.bl_id=(select bl_id from pt_store_loc where pt_store_loc_id='"
				+ receivingLoc + "'  )),"
				+ " ship_city_id=(select city_id from bl where bl.bl_id=(select bl_id from pt_store_loc where pt_store_loc_id='"
				+ receivingLoc + "')  ),"
				+ " bill_city_id=(select city_id from bl where bl.bl_id=(select bl_id from pt_store_loc where pt_store_loc_id='"
				+ receivingLoc + "'))  ,"
				+ " ship_state_id=(select state_id from bl where bl.bl_id=(select bl_id from pt_store_loc where pt_store_loc_id='"
				+ receivingLoc + "')), "
				+ " bill_state_id=(select state_id from bl where bl.bl_id=(select bl_id from pt_store_loc where pt_store_loc_id='"
				+ receivingLoc + "')),  "
				+ " ship_zip=(select zip from bl where bl.bl_id=(select bl_id from pt_store_loc where pt_store_loc_id='"
				+ receivingLoc + "') ), "
				+ " bill_zip=(select zip from bl where bl.bl_id=(select bl_id from pt_store_loc where pt_store_loc_id='"
				+ receivingLoc + "' ) ) " + " where po_id='" + poId + "'       ";

		SqlUtils.executeUpdate(PO_TABLE, updateSql);
	}

	/**
	 * Get Purchase Order after status by userName and ApprovedBy field. If user
	 * is previous approver, then status is changed to be Approved.Else , status
	 * is changed to be Request.
	 *
	 * @param userName
	 *            Current Login User Name
	 * @param approvedBy
	 *            Purchase Order Approver
	 * @return afterStatus Approved or Requested
	 */
	private String getPoAfterStatus(final String userName, final String approvedBy) {
		String afterStatus;
		if (userName.equals(approvedBy)) {
			afterStatus = STATUS_APPROVED;
		} else {
			afterStatus = STATUS_REQUESTED;
		}

		return afterStatus;

	}

	@Override
	protected String[][] getFieldsToProperties() {
		return FIELDS_TO_PROPERTIES.clone();
	}

	/**
	 * Setter for the purchaseOrderLineItemDataSource property.
	 *
	 * @see purchaseOrderLineItemDataSource
	 * @param purchaseOrderLineItemDataSource
	 *            the purchaseOrderLineItemDataSource to set
	 */

	public void setPurchaseOrderLineItemDataSource(
			final IPurchaseOrderLineItemDao<PurchaseOrderLineItem> purchaseOrderLineItemDataSource) {
		this.purchaseOrderLineItemDataSource = purchaseOrderLineItemDataSource;
	}

	/**
	 * Setter for the partStorageLocationDataSource property.
	 *
	 * @see partStorageLocationDataSource
	 * @param partStorageLocDataSource
	 *            the partStorageLocationDataSource to set
	 */

	public void setPartStorageLocDataSource(
			final IPartStorageLocationDao<PartStorageLocation> partStorageLocDataSource) {
		this.partStorageLocDataSource = partStorageLocDataSource;
	}

	/**
	 * Setter for the transactionDataSource property.
	 *
	 * @see transactionDataSource
	 * @param transactionDataSource
	 *            the transactionDataSource to set
	 */

	public void setTransactionDataSource(final ITransactionDao<Transaction> transactionDataSource) {
		this.transactionDataSource = transactionDataSource;
	}

}
