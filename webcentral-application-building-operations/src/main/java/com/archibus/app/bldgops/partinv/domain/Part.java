package com.archibus.app.bldgops.partinv.domain;

/**
 * Domain object for pt.
 * <p>
 *
 * @author Guo Jiangtao
 * @since 23.2
 *
 */
public class Part {

    /** The part code. */
    private String partId;

    /** The quantity on hand. */
    private double qtyOnHand;

    /** The quantity on reserve. */
    private double qtyOnReserve;

    /** The quantity of physical. */
    private double qtyPhysicalCount;

    /** The quantity to order. */
    private double qtyToOrder;

    /** The quantity of min on hand. */
    private double qtyMinHand;

    /** The Cost unit std. */
    private double costUnitStd;

    /** The Cost unit last. */
    private double costUnitLast;

    /** The Accounting Property Type. */
    private String accPropType;

    /**
     * Getter for the partId property.
     *
     * @see partId
     * @return the partId property.
     */
    public String getPartId() {
        return this.partId;
    }

    /**
     * Setter for the partId property.
     *
     * @see partId
     * @param partId the partId to set
     */

    public void setPartId(final String partId) {
        this.partId = partId;
    }

    /**
     * Getter for the qtyOnHand property.
     *
     * @see qtyOnHand
     * @return the qtyOnHand property.
     */
    public double getQtyOnHand() {
        return this.qtyOnHand;
    }

    /**
     * Setter for the qtyOnHand property.
     *
     * @see qtyOnHand
     * @param qtyOnHand the qtyOnHand to set
     */

    public void setQtyOnHand(final double qtyOnHand) {
        this.qtyOnHand = qtyOnHand;
    }

    /**
     * Getter for the qtyOnReserve property.
     *
     * @see qtyOnReserve
     * @return the qtyOnReserve property.
     */
    public double getQtyOnReserve() {
        return this.qtyOnReserve;
    }

    /**
     * Setter for the qtyOnReserve property.
     *
     * @see qtyOnReserve
     * @param qtyOnReserve the qtyOnReserve to set
     */

    public void setQtyOnReserve(final double qtyOnReserve) {
        this.qtyOnReserve = qtyOnReserve;
    }

    /**
     * Getter for the qtyPhysicalCount property.
     *
     * @see qtyPhysicalCount
     * @return the qtyPhysicalCount property.
     */
    public double getQtyPhysicalCount() {
        return this.qtyPhysicalCount;
    }

    /**
     * Setter for the qtyPhysicalCount property.
     *
     * @see qtyPhysicalCount
     * @param qtyPhysicalCount the qtyPhysicalCount to set
     */

    public void setQtyPhysicalCount(final double qtyPhysicalCount) {
        this.qtyPhysicalCount = qtyPhysicalCount;
    }

    /**
     * Getter for the qtyToOrder property.
     *
     * @see qtyToOrder
     * @return the qtyToOrder property.
     */
    public double getQtyToOrder() {
        return this.qtyToOrder;
    }

    /**
     * Setter for the qtyToOrder property.
     *
     * @see qtyToOrder
     * @param qtyToOrder the qtyToOrder to set
     */

    public void setQtyToOrder(final double qtyToOrder) {
        this.qtyToOrder = qtyToOrder;
    }

    /**
     * Getter for the qtyMinHand property.
     *
     * @see qtyMinHand
     * @return the qtyMinHand property.
     */
    public double getQtyMinHand() {
        return this.qtyMinHand;
    }

    /**
     * Setter for the qtyMinHand property.
     *
     * @see qtyMinHand
     * @param qtyMinHand the qtyMinHand to set
     */

    public void setQtyMinHand(final double qtyMinHand) {
        this.qtyMinHand = qtyMinHand;
    }

    /**
     * Getter for the costUnitStd property.
     *
     * @see costUnitStd
     * @return the costUnitStd property.
     */
    public double getCostUnitStd() {
        return this.costUnitStd;
    }

    /**
     * Setter for the costUnitStd property.
     *
     * @see costUnitStd
     * @param costUnitStd the costUnitStd to set
     */

    public void setCostUnitStd(final double costUnitStd) {
        this.costUnitStd = costUnitStd;
    }

    /**
     * Getter for the costUnitLast property.
     *
     * @see costUnitLast
     * @return the costUnitLast property.
     */
    public double getCostUnitLast() {
        return this.costUnitLast;
    }

    /**
     * Setter for the costUnitLast property.
     *
     * @see costUnitLast
     * @param costUnitLast the costUnitLast to set
     */

    public void setCostUnitLast(final double costUnitLast) {
        this.costUnitLast = costUnitLast;
    }

    /**
     * Getter for the accPropType property.
     *
     * @see accPropType
     * @return the accPropType property.
     */
    public String getAccPropType() {
        return this.accPropType;
    }

    /**
     * Setter for the accPropType property.
     *
     * @see accPropType
     * @param accPropType the accPropType to set
     */

    public void setAccPropType(final String accPropType) {
        this.accPropType = accPropType;
    }

}
