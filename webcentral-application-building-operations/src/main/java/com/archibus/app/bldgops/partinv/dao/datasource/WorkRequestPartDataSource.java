package com.archibus.app.bldgops.partinv.dao.datasource;

import static com.archibus.app.bldgops.partinv.util.PartInventoryConstant.*;

import java.util.*;

import com.archibus.app.bldgops.partinv.dao.IWorkRequestPartDao;
import com.archibus.app.bldgops.partinv.domain.WorkRequestPart;
import com.archibus.datasource.ObjectDataSourceImpl;
import com.archibus.datasource.data.DataRecord;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.ParsedRestrictionDef;
import com.archibus.utility.Utility;

/**
 *
 * Work Request Part datasource.
 * <p>
 *
 *
 * @author Guo Jiangtao
 * @since 23.2
 *
 */
public class WorkRequestPartDataSource extends ObjectDataSourceImpl<WorkRequestPart>
        implements IWorkRequestPartDao<WorkRequestPart> {

    /**
     * Field names to property names mapping. All fields will be added to the DataSource.
     */
    private static final String[][] FIELDS_TO_PROPERTIES =
            { { "part_id", "partId" }, { "wr_id", "wrId" }, { "date_assigned", "dateAssigned" },
                    { "time_assigned", "timeAssigned" }, { STATUS, "status" },
                    { "qty_estimated", "qtyEstimated" } };

    /**
     * Constructs WorkRequestPartDataSource, mapped to <code>wrpt</code> table, using
     * <code>workRequestPart</code> bean.
     *
     */
    public WorkRequestPartDataSource() {
        super("workRequestPart", "wrpt");
    }

    @Override
    protected String[][] getFieldsToProperties() {
        return FIELDS_TO_PROPERTIES.clone();
    }

    /** {@inheritDoc} */

    @Override
    public List<WorkRequestPart> findEstimatedParts(final String partId) {

        final ParsedRestrictionDef resDef = new ParsedRestrictionDef();

        resDef.addClause(WRPT_TABLE, PART_ID, partId, Operation.EQUALS);

        return find(resDef);
    }

    /** {@inheritDoc} */

    @Override
    public WorkRequestPart findEstimatedPart(final String partId, final int wrId,
            final String dateAssigned, final String timeAssigned) {
        final StringBuffer restriction = new StringBuffer();
        restriction.append(" part_id = " + Utility.sqlrMakeLiteral(partId) + " AND  wr_id = " + wrId
                + " AND date_assigned =  " + dateAssigned + " AND time_assigned  = "
                + timeAssigned);

        WorkRequestPart wrptObject = null;
        final DataRecord estimatePart = this.getRecord(restriction.toString());
        if (estimatePart != null) {
            wrptObject = this.convertRecordToObject(estimatePart);
        }

        return wrptObject;

    }

    /** {@inheritDoc} */

    @Override
    public WorkRequestPart findEstimatedPart(final String partId, final String partLocId,
            final int wrId, final String dateAssigned, final String timeAssigned) {
        final StringBuffer restriction = new StringBuffer();
        restriction.append(" pt_store_loc_id = " + Utility.sqlrMakeLiteral(partLocId)
                + " and part_id = " + Utility.sqlrMakeLiteral(partId) + " AND wr_id = " + wrId
                + " AND date_assigned = " + dateAssigned + " AND time_assigned = " + timeAssigned);

        WorkRequestPart wrptObject = null;
        final DataRecord estimatePart = this.getRecord(restriction.toString());
        if (estimatePart != null) {
            wrptObject = this.convertRecordToObject(estimatePart);
        }

        return wrptObject;

    }

    /** {@inheritDoc} */

    @Override
    public List<WorkRequestPart> findEstimatedParts(final String partId, final String[] statuses) {

        final ParsedRestrictionDef resDef = new ParsedRestrictionDef();

        resDef.addClause(WRPT_TABLE, PART_ID, partId, Operation.EQUALS);
        resDef.addClause(WRPT_TABLE, STATUS, Arrays.asList(statuses), Operation.IN);

        return find(resDef);
    }

    /** {@inheritDoc} */

    @Override
    public List<WorkRequestPart> findEstimatedParts(final String partId, final String locId,
            final String[] statuses) {
        final ParsedRestrictionDef resDef = new ParsedRestrictionDef();

        resDef.addClause(WRPT_TABLE, PART_ID, partId, Operation.EQUALS);
        resDef.addClause(WRPT_TABLE, PT_STORE_LOC_ID, locId, Operation.EQUALS);
        resDef.addClause(WRPT_TABLE, STATUS, Arrays.asList(statuses), Operation.IN);

        return find(resDef);
    }

    /** {@inheritDoc} */

    @Override
    public List<WorkRequestPart> findEstimatedParts(final String partId, final String locId) {
        final ParsedRestrictionDef resDef = new ParsedRestrictionDef();

        resDef.addClause(WRPT_TABLE, PART_ID, partId, Operation.EQUALS);
        resDef.addClause(WRPT_TABLE, PT_STORE_LOC_ID, locId, Operation.EQUALS);

        return find(resDef);
    }

}
