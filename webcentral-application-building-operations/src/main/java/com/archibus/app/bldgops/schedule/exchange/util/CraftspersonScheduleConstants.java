package com.archibus.app.bldgops.schedule.exchange.util;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides TODO. - if it has behavior (service), or Represents TODO. - if it
 * has state (entity, domain object, model object). Utility class. Provides
 * methods TODO. Interface to be implemented by classes that TODO.
 * <p>
 *
 * Used by TODO to TODO. Managed by Spring, has prototype TODO singleton scope.
 * Configured in TODO file.
 *
 * @author Administrator
 * @since 23.1
 *
 */
public final class CraftspersonScheduleConstants {
	/**
	 * Private default constructor: utility class is non-instantiable.
	 */
	private CraftspersonScheduleConstants() {

	}

	/**
	 * Exchange server use GMT time as time zone.
	 */
	public static final String EXCHANGE_SERVER_TIME_ZONE_ID = "GMT";

	/**
	 * Schedule variance type: BUSY.
	 */
	public static final String VARIANCE_TYPE_BUSY = "BUSY";
}
