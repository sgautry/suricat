package com.archibus.app.helpdesk.mobile.maintenance.service;

import org.json.JSONObject;

/**
 * API of the WorkflowRule Service for mobile Maintenance application.
 * <p>
 * Only authenticated users are allowed to invoke methods in this service.
 *
 * @author Constantine Kriezis
 * @since 21.1
 *
 */
public interface IMaintenanceMobileService {

	/**
	 * Synchronizes all the data for the mobile maintenance application.
	 *
	 * @param cfUser
	 *            User Name of Crafts Person
	 * @param cfId
	 *            Crafts Person Code
	 * @return returnMessage - Return any issues with executing workflow
	 *         actions.
	 */
	// TODO: (VT): can cfId be deduced from cfUser here?
	JSONObject syncWorkData(final String cfUser, final String cfId);

	/**
	 * Synchronizes equipment maintenance history data of specified equipment
	 * code.
	 *
	 * @param cfUser
	 *            User Name of Crafts Person
	 * @param cfId
	 *            Crafts Person Code
	 * @param eqId
	 *            Equipment Code.
	 * @return returnMessage - Return any issues with executing workflow
	 *         actions.
	 */
	long syncEqMaintHistory(final String cfUser, final String cfId, final String eqId);

	/**
	 * Synchronizes related work request and its resource.
	 *
	 * @param cfUser
	 *            User Name of Crafts Person
	 * @param cfId
	 *            Crafts Person Code
	 * @param wrId
	 *            Work request code
	 * @return returnMessage - Return any issues with executing workflow
	 *         actions.
	 */
	long syncRelatedWorkRequest(final String cfUser, final String cfId, final String wrId);
}