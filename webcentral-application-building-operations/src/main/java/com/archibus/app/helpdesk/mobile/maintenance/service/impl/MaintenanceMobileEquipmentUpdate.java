package com.archibus.app.helpdesk.mobile.maintenance.service.impl;

import static com.archibus.app.common.mobile.util.FieldNameConstantsCommon.*;
import static com.archibus.app.common.mobile.util.FieldNameConstantsMaintenance.*;
import static com.archibus.app.common.mobile.util.ServiceConstants.SQL_DOT;
import static com.archibus.app.common.mobile.util.TableNameConstants.*;

import java.util.List;

import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;
import com.archibus.utility.StringUtil;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 *
 * This class is used to update and sync the Equipment table for mobile
 * maintenance app.
 *
 * Justification: Case #2: Statements with UPDATE ... WHERE pattern.
 *
 * @author Jia
 * @since 23.2
 */

@SuppressWarnings("PMD.AvoidUsingSql")
public class MaintenanceMobileEquipmentUpdate {
	/**
	 * Equipment Condition Field Name.
	 */
	public static final String EQ_CONDITION = "eq_condition";

	/**
	 * Equipment Status Field Name.
	 */
	public static final String EQ_STATUS = "eq_status";
	/**
	 * Mobile Equipment Changed Fields Name.
	 */
	public static final String MOB_EQ_FIELDS_CHANGED = "mob_eq_fields_changed";

	/**
	 * Array of fields for wr_sync table.
	 */
	public static final String[] WR_SYNC_MAINTENENANCE_EQ_FIELDS = { AUTO_NUMBER, WR_ID, MOB_WR_ID, MOB_IS_CHANGED,
			MOB_LOCKED_BY, EQ_ID, MOB_EQ_FIELDS_CHANGED, EQ_CONDITION, EQ_STATUS };

	/**
	 * Update equipment records according to the modified equipment sync record.
	 *
	 * @param userName
	 *            User Name
	 */
	static void updateEquipmentRecords(final String userName) {
		final DataSource wrSyncDs = DataSourceFactory.createDataSourceForFields(WR_SYNC_TABLE,
				WR_SYNC_MAINTENENANCE_EQ_FIELDS);
		wrSyncDs.setContext();
		wrSyncDs.setMaxRecords(0);
		// add restriction to data source to filter changed records by specified
		// user.
		wrSyncDs.addRestriction(Restrictions.eq(WR_SYNC_TABLE, MOB_IS_CHANGED, "1"));
		wrSyncDs.addRestriction(Restrictions.eq(WR_SYNC_TABLE, MOB_LOCKED_BY, userName));
		wrSyncDs.addRestriction(Restrictions.isNotNull(WR_SYNC_TABLE, MOB_EQ_FIELDS_CHANGED));
		final List<DataRecord> records = wrSyncDs.getRecords();

		for (final DataRecord wrSyncRecord : records) {
			// create Equipment table datasource.
			final DataSource eqDs = DataSourceFactory.createDataSourceForFields(EQ_TABLE, MAINTENANCE_EQ_FIELDS);
			final String eqId = wrSyncRecord.getString(WR_SYNC_TABLE + SQL_DOT + EQ_ID);
			final String mobEqFieldsChanged = wrSyncRecord.getString(WR_SYNC_TABLE + SQL_DOT + MOB_EQ_FIELDS_CHANGED);
			eqDs.addRestriction(Restrictions.eq(EQ_TABLE, EQ_ID, eqId));

			// Get any tool record that meets the restriction
			final DataRecord eqRecord = eqDs.getRecord();

			if (eqRecord != null) {
				// update eq record by modified eq sync record.
				final DataRecord updatedEqRecord = updateEquipmentRecord(eqRecord, wrSyncRecord, mobEqFieldsChanged);
				eqDs.saveRecord(updatedEqRecord);
			}
		}
	}

	/**
	 * Update single equipment record according to the equipment sync table
	 * record.
	 *
	 * @param eqRecord
	 *            -Equipment table record.
	 * @param wrSyncRecord
	 *            -WorkRequest Sync table record.
	 * @return Data Record
	 *
	 */
	static DataRecord updateEquipmentRecord(final DataRecord eqRecord, final DataRecord wrSyncRecord,
			final String mobChangedEqFields) {
		final String[] eqFieldsArray = mobChangedEqFields.split(",");
		for (final String element : eqFieldsArray) {
			if (EQ_STATUS.equals(element)) {
				eqRecord.setValue(EQ_TABLE + SQL_DOT + STATUS,
						wrSyncRecord.getString(WR_SYNC_TABLE + SQL_DOT + EQ_STATUS));
			}
			if (EQ_CONDITION.equals(element)) {
				eqRecord.setValue(EQ_TABLE + SQL_DOT + CONDITION,
						wrSyncRecord.getString(WR_SYNC_TABLE + SQL_DOT + EQ_CONDITION));
			}
		}

		return eqRecord;
	}

	/**
	 * Update Equipment Condition and Equipment Status fields value in Work
	 * request sync table by user name.
	 *
	 * @param userName-User
	 *            Name
	 */
	static void updateEqFieldValues(final String userName) {
		final String sql = "UPDATE wr_sync SET eq_condition=(select condition from eq where eq.eq_id=wr_sync.eq_id),eq_status=(select status from eq where eq.eq_id=wr_sync.eq_id),mob_eq_fields_changed='' WHERE mob_locked_by='"
				+ userName + "'";
		SqlUtils.executeUpdate(WR_SYNC_TABLE, sql);
	}

	/**
	 * Update Equipment Condition and Equipment Status fields value in Work
	 * request sync table by user name.
	 *
	 * @param res
	 *            Customized restriction
	 */
	static void updateEqFieldValuesByConditions(final String userName, final String res) {
		String sql = "UPDATE wr_sync SET eq_condition=(select condition from eq where eq.eq_id=wr_sync.eq_id),eq_status=(select status from eq where eq.eq_id=wr_sync.eq_id),mob_eq_fields_changed='' WHERE mob_locked_by='"
				+ userName + "'";
		if (StringUtil.notNullOrEmpty(res)) {
			sql = sql + " AND " + res;
		}
		SqlUtils.executeUpdate(WR_SYNC_TABLE, sql);
	}
}
