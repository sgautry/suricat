package com.archibus.eventhandler.prevmaint;

import java.text.ParseException;
import java.util.*;
import java.util.Map.Entry;

import org.json.*;

import com.archibus.app.common.recurring.RecurringScheduleService;
import com.archibus.context.ContextStore;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.db.ViewField;
import com.archibus.ext.report.xls.*;
import com.archibus.jobmanager.*;
import com.archibus.jobmanager.JobStatus.JobResult;
import com.archibus.model.view.form.processor.AnalysisViewDefGenerator;
import com.archibus.utility.*;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Generate 52 week PM plan for PM Planner.
 *
 * @author Guo Jiangtao
 * @since 23.2
 *
 */
public class PmPlanner52WeekPlanGenerator extends JobBase {
    /**
     * recurrence fields number.
     */
    private static final int RECURRENCE_FIELD_NUMBER = 4;

    /**
     * field name pms.vf_interval.
     */
    private static final String PMS_VF_INTERVAL = "pms.vf_interval";

    /**
     * field name pms.interval_.
     */
    private static final String PMS_INTERVAL = "pms.interval_";

    /**
     * field name pms.totalLaborText.
     */
    private static final String PMS_TOTAL_LABOR_TEXT = "pms.totalLaborText";

    /**
     * field name pms.week_.
     */
    private static final String PMS_WEEK = "pms.week_";

    /**
     * field name pms.groupValue.
     */
    private static final String FIELD_PMS_GROUP_VALUE = "pms.groupValue";

    /**
     * 52 week.
     */
    private static final int WEEK_52 = 52;

    /**
     * field id.
     */
    private static final String FIELD_ID = "id";

    /**
     * Report Title.
     */
    // @translatable
    private static final String TITLE = "52-Week Preventive Maintenance Plan";

    /**
     * Total Labor Hours Title.
     */
    // @translatable
    private static final String TOTAL_LABOR_HOURS_TEXT = "Total Labor Hours";

    /**
     * Multiple text.
     */
    // @translatable
    private static final String MULTIPLE = "Multiple";

    /**
     * None text.
     */
    // @translatable
    private static final String NONE = "None";

    /**
     * Manual text.
     */
    // @translatable
    private static final String MANUAL = "Manual";

    /**
     * Week text.
     */
    // @translatable
    private static final String WEEK = "Week";

    /**
     * export xls view name.
     */
    private final String viewName;

    /**
     * pms datasource name.
     */
    private final String pmsDataSourceID;

    /**
     * pmsd datasource name.
     */
    private final String pmsdDataSourceID;

    /**
     * group field.
     */
    private final Map<String, Object> groupField;

    /**
     * visible fields.
     */
    private final List<Map<String, Object>> visibleFields;

    /**
     * dataSource parameters.
     */
    private final Map<String, Object> parameters;

    /**
     * report title.
     */
    private final String reportTitle;

    /**
     * total labor hours text.
     */
    private final String totalLaborHoursText;

    /**
     * date range from.
     */
    private Date fromDate;

    /**
     * date range to.
     */
    private Date toDate;

    /**
     * Calendar from date.
     */
    private final String calendarFromDate;

    /**
     * pms group map.
     */
    private Map<String, List<DataRecord>> pmsGroupMap;

    /**
     * Constructor.
     *
     * @param groupField group field
     * @param visibleFields visible fields
     * @param parameters dataSource parameters
     */
    public PmPlanner52WeekPlanGenerator(final Map<String, Object> groupField,
            final List<Map<String, Object>> visibleFields, final Map<String, Object> parameters) {
        super();
        this.calendarFromDate = parameters.get("calendarFromDate").toString();
        this.viewName = parameters.get("viewName").toString();
        this.pmsDataSourceID = parameters.get("pmsDataSourceID").toString();
        this.pmsdDataSourceID = parameters.get("pmsdDataSourceID").toString();
        this.groupField = groupField;
        this.visibleFields = visibleFields;
        this.parameters = parameters;
        this.reportTitle = com.archibus.eventhandler.EventHandlerBase.localizeString(
            ContextStore.get().getCurrentContext(), TITLE, this.getClass().getName());
        this.totalLaborHoursText = com.archibus.eventhandler.EventHandlerBase.localizeString(
            ContextStore.get().getCurrentContext(), TOTAL_LABOR_HOURS_TEXT,
            this.getClass().getName());
    }

    @Override
    public void run() {
        // apply data source parameters
        applyDataSourceParameters();

        // load pms data
        final List<DataRecord> pmsRecords = loadPmsData();

        // set interval field values
        setIntervalField(pmsRecords);

        // add 52 week columns to the visible fields
        add52WeekColumns();

        // calculate labor hours
        calculateLaborHours(pmsRecords);

        // build xls file
        buildXLS(pmsRecords);

        this.status.setCode(JobStatus.JOB_COMPLETE);
    }

    /**
     * Calculate labor hours.
     *
     * @param pmsRecords pms records
     */
    private void calculateLaborHours(final List<DataRecord> pmsRecords) {
        // load pmsd records
        final List<DataRecord> pmsdRecords = loadPmsdData();

        // roll up labor hours to pms and week
        final Map<Object, Map<Object, Object>> pmsWeekHoursMap =
                new HashMap<Object, Map<Object, Object>>();
        this.pmsGroupMap = new HashMap<String, List<DataRecord>>();
        for (final DataRecord pmsdRecord : pmsdRecords) {
            final int pmsId = pmsdRecord.getInt("pmsd.pms_id");
            final int weekNum = getWeekNumber(pmsdRecord.getDate("pmsd.date_todo"));
            final double laborHours = pmsdRecord.getDouble("pmsd.vf_est_labor_hours");
            if (!pmsWeekHoursMap.containsKey(pmsId)) {
                pmsWeekHoursMap.put(pmsId, new HashMap<Object, Object>());
            }

            final Map<Object, Object> currentPmsWeekHoursMap = pmsWeekHoursMap.get(pmsId);
            if (!currentPmsWeekHoursMap.containsKey(weekNum)) {
                currentPmsWeekHoursMap.put(weekNum, 0.00);
            }

            currentPmsWeekHoursMap.put(weekNum,
                (Double) currentPmsWeekHoursMap.get(weekNum) + laborHours);
        }

        for (final DataRecord pmsRecord : pmsRecords) {
            final int pmsId = pmsRecord.getInt("pms.pms_id");
            final Map<Object, Object> currentPmsWeekHoursMap = pmsWeekHoursMap.get(pmsId);
            if (currentPmsWeekHoursMap != null) {
                for (int i = 1; i <= WEEK_52; i++) {
                    if (currentPmsWeekHoursMap.get(i) != null) {
                        pmsRecord.setValue(PMS_WEEK + i, currentPmsWeekHoursMap.get(i));
                    }
                }
            }

            final String groupValue = pmsRecord.getString(FIELD_PMS_GROUP_VALUE);
            if (!this.pmsGroupMap.containsKey(groupValue)) {
                this.pmsGroupMap.put(groupValue, new ArrayList<DataRecord>());
            }

            this.pmsGroupMap.get(groupValue).add(pmsRecord);
        }
    }

    /**
     * get Week Number.
     *
     * @param date date
     * @return week number
     */
    private int getWeekNumber(final Date date) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            calendar.add(Calendar.DATE, -1);
        }
        return calendar.get(Calendar.WEEK_OF_YEAR);
    }

    /**
     * Build XLS.
     *
     * @param records pms records
     */
    private void buildXLS(final List<DataRecord> records) {

        GridBuilder builder = null;
        final PrintRestrictionHandler printRestrictionHandler = getPrintRestrictionHandler();
        if ("''".equals(this.groupField.get(FIELD_ID))) {
            builder = buildXLSWithoutGroup(records, printRestrictionHandler);
        } else {
            builder = buildXLSByGroup(records, printRestrictionHandler);
        }

        final String fileName = builder.getFileName();
        final String url = builder.getURL();
        final JobResult result = new JobResult(this.reportTitle, fileName, url);
        this.status.setResult(result);
    }

    /**
     * Get print restriction handler.
     *
     * @return printRestrictionHandler
     */
    private PrintRestrictionHandler getPrintRestrictionHandler() {
        PrintRestrictionHandler printRestrictionHandler = null;
        List<Map<String, Object>> printableRestriction;
        try {
            printableRestriction = com.archibus.eventhandler.EventHandlerBase
                .fromJSONArray(StringUtil.notNull(this.parameters.get("printableRestriction")));

            printRestrictionHandler = new PrintRestrictionHandler();
            printRestrictionHandler.setPrintableRstrictions(printableRestriction);
        } catch (final ParseException e) {
            // @non-translatable
            throw new ExceptionBase("Cannot parse parameters", e);
        }

        return printRestrictionHandler;
    }

    /**
     * Add 52 Week Columns.
     */
    private void add52WeekColumns() {

        final Date currentDate = new Date();
        final Date calFromDate = DateTime.stringToDate(this.calendarFromDate, "yyy-MM-dd");

        final Calendar calendar = Calendar.getInstance();
        if (calFromDate.before(currentDate)) {
            calendar.setTime(currentDate);
        } else {
            calendar.setTime(calFromDate);
        }

        // if current date is Sunday, then (date-1) to make sure we use the
        // correct week, we start week at Monday, it is little different with
        // the Calendar
        if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            calendar.add(Calendar.DATE, -1);
        }

        // start from Monday
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        this.fromDate = calendar.getTime();

        final JSONArray fieldArray = new JSONArray();
        List<Map<String, Object>> newFields = null;
        final String weekText = com.archibus.eventhandler.EventHandlerBase.localizeString(
            ContextStore.get().getCurrentContext(), WEEK, this.getClass().getName());
        try {
            fieldArray.put(new JSONObject("{id:'pms.totalLaborText',title:'', isNumeric:false}"));
            for (int i = 1; i <= WEEK_52; i++) {
                fieldArray
                    .put(new JSONObject(
                        "{id:'pms.week_" + calendar.get(Calendar.WEEK_OF_YEAR) + "',title: '"
                                + weekText + " #" + calendar.get(Calendar.WEEK_OF_YEAR) + " \\r\\n"
                                + DateTime.dateToString(
                                    new java.sql.Date(calendar.getTime().getTime()), "MM/dd/yyyy")
                                + "', isNumeric:true, decimals:2}"));
                calendar.add(Calendar.WEEK_OF_YEAR, 1);
            }

            newFields = com.archibus.eventhandler.EventHandlerBase.fromJSONArray(fieldArray);
            for (final Map<String, Object> field : newFields) {
                this.visibleFields.add(field);
            }
        } catch (final ParseException e) {
            throw new ExceptionBase("", e);
        }

        calendar.add(Calendar.DATE, -1);
        this.toDate = calendar.getTime();
    }

    /**
     * Load Pms Data.
     *
     * @return pms records
     */
    private List<DataRecord> loadPmsData() {
        final DataSource dataSource =
                DataSourceFactory.loadDataSourceFromFile(this.viewName, this.pmsDataSourceID);
        return dataSource.getAllRecords();
    }

    /**
     * Load Pmsd Data.
     *
     * @return pmsd records
     */
    private List<DataRecord> loadPmsdData() {
        final EventHandlerContext eventHandlerContext = ContextStore.get().getEventHandlerContext();
        eventHandlerContext.addInputParameter("excludeCancelDate", true);
        eventHandlerContext.addInputParameter("useDateRange", true);
        eventHandlerContext.addInputParameter("dateStart", formatDate(this.fromDate));
        eventHandlerContext.addInputParameter("dateEnd", formatDate(this.toDate));
        final DataSource dataSource =
                DataSourceFactory.loadDataSourceFromFile(this.viewName, this.pmsdDataSourceID);
        return dataSource.getAllRecords();
    }

    /**
     * format date.
     */
    /**
     * format date.
     *
     * @param date date
     * @return format date
     */
    private String formatDate(final Date date) {
        return DateTime.dateToString(new java.sql.Date(date.getTime()), "yyyy-MM-dd");
    }

    /**
     * Apply DataSource Parameters.
     */
    private void applyDataSourceParameters() {
        final EventHandlerContext eventHandlerContext = ContextStore.get().getEventHandlerContext();
        eventHandlerContext.addInputParameter("groupField", this.groupField.get(FIELD_ID));
        for (final Entry<String, Object> entrySet : this.parameters.entrySet()) {
            eventHandlerContext.addInputParameter(entrySet.getKey(),
                StringUtil.notNull(entrySet.getValue()));
        }
    }

    /**
     * Build XLS Without Group.
     *
     * @param records pms records
     * @param printRestrictionHandler printRestrictionHandler
     * @return builder
     */
    private GridBuilder buildXLSWithoutGroup(final List<DataRecord> records,
            final PrintRestrictionHandler printRestrictionHandler) {
        final GridBuilder builder = new GridBuilder();
        builder.setFileName(builder.createFileName(this.viewName));
        builder.setPrintRestrictionHandler(printRestrictionHandler);
        addTotalRow(records);
        builder.build(records, this.reportTitle, this.visibleFields);
        return builder;
    }

    /**
     * Build XLS Without Group.
     *
     * @param records pms records
     * @param printRestrictionHandler printRestrictionHandler
     * @return builder
     */
    private GridBuilder buildXLSByGroup(final List<DataRecord> records,
            final PrintRestrictionHandler printRestrictionHandler) {
        final PmPlanner52WeekPlanXlsBuilder builder = new PmPlanner52WeekPlanXlsBuilder();
        builder.setCategoryCollapse(true);
        builder.setCategoryFont(null);
        builder.setCategoryOrder(null);
        builder.setCategoryColors(null);

        final DataSource dataSource =
                DataSourceFactory.loadDataSourceFromFile(this.viewName, this.pmsDataSourceID);
        final List<DataRecord> groupRecords = AnalysisViewDefGenerator
            .generateGroupsDataSourceAndGetGroups(this.viewName, this.pmsDataSourceID,
                dataSource.formatSqlQuery(null, false), FIELD_PMS_GROUP_VALUE);

        builder.setCategoryRecords(groupRecords);
        addGroupTotalRow(records, dataSource, groupRecords);

        final List<Map<String, Object>> fields = removeGroupFields();
        builder.setCategoryFields(getCategoryFields());
        builder.setFileName(builder.createFileName(this.viewName));
        builder.setPrintRestrictionHandler(printRestrictionHandler);
        builder.build(records, this.reportTitle, fields);
        return builder;
    }

    /**
     * Add Group Total Row.
     *
     * @param records records
     * @param dataSource dataSource
     * @param groupRecords groupRecords
     */
    private void addGroupTotalRow(final List<DataRecord> records, final DataSource dataSource,
            final List<DataRecord> groupRecords) {

        for (final DataRecord groupReord : groupRecords) {
            final String groupValue = groupReord.getString(FIELD_PMS_GROUP_VALUE);
            final DataRecord totalLaborRow = dataSource.createRecord();
            for (final ViewField.Immutable virtualFieldDef : dataSource.getVirtualFields()) {
                totalLaborRow.addField(virtualFieldDef);
            }
            totalLaborRow.setValue(FIELD_PMS_GROUP_VALUE, groupValue);
            totalLaborRow.setValue(PMS_TOTAL_LABOR_TEXT, this.totalLaborHoursText);
            String weekFieldName = "";
            for (int i = 1; i <= WEEK_52; i++) {
                weekFieldName = PMS_WEEK + i;
                totalLaborRow.setValue(weekFieldName,
                    getGroupTotalHours(groupValue, weekFieldName));
            }
            records.add(totalLaborRow);
        }
    }

    /**
     * Add Total Row.
     *
     * @param records records
     */
    private void addTotalRow(final List<DataRecord> records) {

        if (!records.isEmpty()) {
            final DataSource dataSource =
                    DataSourceFactory.loadDataSourceFromFile(this.viewName, this.pmsDataSourceID);
            final DataRecord totalLaborRow = dataSource.createRecord();
            for (final ViewField.Immutable virtualFieldDef : dataSource.getVirtualFields()) {
                totalLaborRow.addField(virtualFieldDef);
            }

            totalLaborRow.setValue(PMS_TOTAL_LABOR_TEXT, this.totalLaborHoursText);
            String weekFieldName = "";
            for (int i = 1; i <= WEEK_52; i++) {
                weekFieldName = PMS_WEEK + i;
                totalLaborRow.setValue(weekFieldName, getTotalHours(records, weekFieldName));
            }
            records.add(totalLaborRow);
        }

    }

    /**
     * Set Interval field.
     *
     * @param pmsRecords pm schedule records
     */
    private void setIntervalField(final List<DataRecord> pmsRecords) {
        for (final DataRecord pmsRecord : pmsRecords) {
            final String intervalType = pmsRecord.getString("pms.interval_type");
            final int index = pmsRecord.getInt("pms.interval_freq");
            if ("r".equals(intervalType)) {
                final String recurringInterval = getRecurringInterval(pmsRecord);
                pmsRecord.setValue(PMS_VF_INTERVAL, recurringInterval);
            } else if ("a".equals(intervalType)) {
                pmsRecord.setValue(PMS_VF_INTERVAL,
                    com.archibus.eventhandler.EventHandlerBase.localizeString(
                        ContextStore.get().getCurrentContext(), MANUAL, this.getClass().getName()));
            } else if ("e".equals(intervalType)) {
                pmsRecord.setValue(PMS_VF_INTERVAL,
                    "Meter:" + pmsRecord.getInt(PMS_INTERVAL + index));
            } else {
                pmsRecord.setValue(PMS_VF_INTERVAL,
                    "Every " + pmsRecord.getInt(PMS_INTERVAL + index) + " "
                            + getIntervalString(intervalType));
            }
        }

    }

    /**
     * get recurring interval String.
     *
     * @param pmsRecord pm schedule record
     * @return interval string
     */
    private String getRecurringInterval(final DataRecord pmsRecord) {
        final RecurringScheduleService recurringScheduleService = new RecurringScheduleService();

        String intervalString = "";
        boolean isMultiple = false;
        for (int i = 1; i <= RECURRENCE_FIELD_NUMBER; i++) {
            final String recurrence = pmsRecord.getString("pms.int" + i + "_recurrence");
            if (StringUtil.notNullOrEmpty(recurrence)) {
                if (StringUtil.isNullOrEmpty(intervalString)) {
                    intervalString =
                            recurringScheduleService.getRecurringPatternDescription(recurrence);
                } else {
                    isMultiple = true;
                    break;
                }
            }
        }

        if (isMultiple) {
            intervalString = com.archibus.eventhandler.EventHandlerBase.localizeString(
                ContextStore.get().getCurrentContext(), MULTIPLE, this.getClass().getName());
        } else {
            if (StringUtil.isNullOrEmpty(intervalString)) {
                intervalString = com.archibus.eventhandler.EventHandlerBase.localizeString(
                    ContextStore.get().getCurrentContext(), NONE, this.getClass().getName());
            }
        }

        return intervalString;

    }

    /**
     * get Interval String.
     *
     * @param intervalType interval type
     * @return interval string
     */
    private String getIntervalString(final String intervalType) {
        String intervalString = "";
        if ("d".equals(intervalType)) {
            intervalString = "Days";
        } else if ("ww".equals(intervalType)) {
            intervalString = "Weeks";
        } else if ("m".equals(intervalType)) {
            intervalString = "Months";
        } else if ("q".equals(intervalType)) {
            intervalString = "Quarters";
        } else if ("yyyy".equals(intervalType)) {
            intervalString = "Years";
        } else if ("i".equals(intervalType)) {
            intervalString = "Miles";
        } else if ("h".equals(intervalType)) {
            intervalString = "Hours";
        }

        return intervalString;

    }

    /**
     * Get group total hours.
     *
     * @param groupValue group value
     * @param weekFieldName week field name
     * @return group total hours
     */
    private Double getGroupTotalHours(final String groupValue, final String weekFieldName) {
        Double totalLaborHours = 0.0;
        final List<DataRecord> pmsRecords = this.pmsGroupMap.get(groupValue);
        if (pmsRecords != null) {
            for (final DataRecord pmsRecord : pmsRecords) {
                totalLaborHours += pmsRecord.getDouble(weekFieldName);
            }
        }

        return totalLaborHours;
    }

    /**
     * Get total hours.
     *
     * @param pmsRecords pms records
     * @param weekFieldName week field name
     * @return total hours
     */
    private Double getTotalHours(final List<DataRecord> pmsRecords, final String weekFieldName) {
        Double totalLaborHours = 0.0;
        for (final DataRecord pmsRecord : pmsRecords) {
            totalLaborHours += pmsRecord.getDouble(weekFieldName);
        }

        return totalLaborHours;
    }

    /**
     * Get Category Fields.
     *
     * @return Category Fields
     */
    private List<Map<String, Object>> getCategoryFields() {
        final List<Map<String, Object>> categroryFields = new ArrayList<Map<String, Object>>();
        this.groupField.put(FIELD_ID, FIELD_PMS_GROUP_VALUE);
        categroryFields.add(this.groupField);
        return categroryFields;
    }

    /**
     * Remove Group Fields.
     *
     * @return field list
     */
    private List<Map<String, Object>> removeGroupFields() {
        final List<Map<String, Object>> fields = new ArrayList<Map<String, Object>>();

        for (final Map<String, Object> field : this.visibleFields) {
            final boolean removeEqStd = "pms.eq_std".equals(field.get(FIELD_ID))
                    && "eq.eq_std".equals(this.groupField.get(FIELD_ID));
            if (!field.get(FIELD_ID).equals(this.groupField.get(FIELD_ID)) && !removeEqStd) {
                fields.add(field);
            }
        }

        return fields;
    }
}
