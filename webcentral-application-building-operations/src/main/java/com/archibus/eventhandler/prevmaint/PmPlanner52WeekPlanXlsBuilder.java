package com.archibus.eventhandler.prevmaint;

import com.archibus.ext.report.xls.CategoryGridBuilder;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */

/**
 * PM Planner 52 week plan XLS file builder.
 * <p>
 *
 *
 * @author Guo Jiangtao
 * @since 23.2
 *
 */
public class PmPlanner52WeekPlanXlsBuilder extends CategoryGridBuilder {

	@Override
	protected void buildCategoryRecord(final int rIndex, final String categoryValue, final int subRecordsNumber) {
		super.buildCategoryRecord(rIndex, categoryValue, subRecordsNumber - 1);
	}

}
