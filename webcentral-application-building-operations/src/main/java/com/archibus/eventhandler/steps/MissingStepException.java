package com.archibus.eventhandler.steps;

import com.archibus.utility.ExceptionBase;

public class MissingStepException extends ExceptionBase {
	/**
	 * Constant: serialVersionUID, generated automatically by Eclipse.
	 */
	private static final long serialVersionUID = -4078417333297749550L;

	public MissingStepException() {
	}

	public MissingStepException(final String pattern) {
		super(pattern);
	}

	public MissingStepException(final String operation, final String argument1) {
		super(operation, argument1);
	}

	public MissingStepException(final String operation, final Throwable nested) {
		super(operation, nested);
	}

	public MissingStepException(final String pattern, final boolean translatable) {
		super(pattern, translatable);
	}

	public MissingStepException(final String operation, final String pattern, final Object[] args) {
		super(operation, pattern, args);
	}

	public MissingStepException(final String operation, final String argument1, final String argument2) {
		super(operation, argument1, argument2);
	}

	public MissingStepException(final String operation, final String pattern, final Throwable nested) {
		super(operation, pattern, nested);
	}

	public MissingStepException(final String pattern, final Object[] args, final boolean translatable) {
		super(pattern, args, translatable);
	}

	public MissingStepException(final String operation, final String pattern, final Throwable nested,
			final Object[] args) {
		super(operation, pattern, nested, args);
	}

	public MissingStepException(final String operation, final String pattern, final Throwable nested,
			final int newErrorNumber) {
		super(operation, pattern, nested, newErrorNumber);
	}
}
