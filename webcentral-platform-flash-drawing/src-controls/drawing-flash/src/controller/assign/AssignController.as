/**
 * Holds the drawing controls's public event handler functions for the assign and unassign actions.
 */


/**
 * Clears locally cached data
 */
public function clearAssignCache():void {
	if (_assignUtilities != null)
		_assignUtilities.clear();
}


/**
 * Sets the selection color.
 *
 * @param color color to be set.
 */
public function setSelectColor(color:uint):void {
	_assignUtilities.setSelectColor(color);
}

/**
 * Sets the asset assign properties such as field name, value and color.
 *
 * @param fullField current assigned field, i.e. rm.rm_id
 * @param val current assigned value, i.e. 101
 * @param color highlight color after select.
 */
public function setToAssign(fullField:String, val:String, color:uint):void {
	_assignUtilities.setToAssign(fullField, val, color);
}

/**
 * Unassign the asset.
 *
 * @param fullField current assigned field, i.e. rm.rm_id
 * @param val current assigned value, i.e. 101
 */
public function unassign(fullField:String, val:String):void {
	_assignUtilities.unassign(fullField, val);
}
