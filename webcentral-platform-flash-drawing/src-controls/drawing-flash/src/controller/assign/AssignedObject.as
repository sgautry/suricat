package src.controller.assign {
	import src.model.util.CommonUtilities;

	/**
	 * Holds the properties for the recent assigned object including field name, value and the color.
	 * This object is usually contructed and set following a JavaScript click action.
	 */
	public class AssignedObject {
		/**
		 * assigned object's full field name, such as 'em.em_id'
		 */
		private var _field:String='';

		/**
		 * assigned object's value for the field, such as 'AFM'
		 */
		private var _value:String='';

		/**
		 * ids array of the asset that this object to be assigned to.
		 */
		private var _ids:Array=null;

		/**
		 * color value
		 */
		private var _color:uint=0;


		/**
		 * Constructor.
		 *
		 * @param field the full field name
		 * @param value field value
		 * @param color color to be set
		 */
		public function AssignedObject(field:String, value:String, color:uint) {
			super();

			this._field=field;
			this._value=value;

			if (color > 0)
				this._color=color;
		}


		/**
		 * Setter.
		 *
		 * @param value color to be set
		 */
		public function set color(value:uint):void {
			_color=value;
		}

		/**
		 * Resets the field and value.
		 */
		public function reset():void {
			this._field='';
			this._value='';
		}

		public function get field():String {
			return _field;
		}

		public function get value():String {
			return _value;
		}

		/**
		 * Checks if the assgined asset's field and value are not empty.
		 *
		 * @return true if field and value are not empty, false otherwise.
		 */
		public function checkValid():Boolean {
			if (!this._field.length || !this._value.length)
				return false;
			else
				return true;
		}

		public function get color():uint {
			return _color;
		}

		/**
		 * Calls the JavaScript API to restrive the color value from the assigned asset's field and its value.
		 *
		 * @return
		 */
		public function getColor():String {
			return CommonUtilities.getColorFromValue(this._field, this._value, this._value);
		}

		public function get ids():Array {
			return _ids;
		}

		public function set ids(value:Array):void {
			_ids=value;
		}

	}
}
