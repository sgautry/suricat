import flash.events.Event;
import flash.geom.Point;
import flash.geom.Rectangle;

import mx.events.ItemClickEvent;

import src.controller.toolbar.ToolbarMode;
import src.model.config.DrawingConfig;
import src.model.data.Location;
import src.model.redline.DrawMode;
import src.view.drawing.ArchibusDrawing;
private var redmarkOpts:DrawingConfig=new DrawingConfig();
private var doCompleteRedmarks:Boolean=false;

[Bindable]
public var redmarksEnabled:Boolean=false;

/**
 * Change the drawing mode for redlining based on the event index.
 * @param index
 */
public function changeDrawMode(drawMode:String):void {
	this.currentState=ToolbarMode.DRAW;

	var ads:Array=this.dwgContainer.getChildren();
	var ad:ArchibusDrawing=null;

	for (var i:Number=0; i < ads.length; i++) {
		ad=ads[i] as ArchibusDrawing;
		ad.drawingMode=drawMode;
	}
}

private function resetDrawMode():void {
	var ads:Array=this.dwgContainer.getChildren();
	var ad:ArchibusDrawing=null;

	for (var i:Number=0; i < ads.length; i++) {
		ad=ads[i] as ArchibusDrawing;
		ad.drawingMode=DrawMode.drawModeOff;
	}
}



//eventlistener for itemclick in ToggleButtonBar with redlines options
public function drawModeChanged(event:ItemClickEvent):void {

}

//PDF report
public function getDrawingZoomInfo():Object{
	var ads:Array=this.dwgContainer.getChildren();
	var ad:ArchibusDrawing=null;
	
	ad=ads[0] as ArchibusDrawing;
	var ZoomInfo:Object=null;
	if( ad.x !=0 && ad.y !=0){
		ZoomInfo = new Object();
		var lux:Number=-1 * ad.x / ad.drawingLayer.scaleX;
		var luy:Number=-1 * ad.y / ad.drawingLayer.scaleY;
		var rlx:Number=lux + this.width / ad.drawingLayer.scaleX;
		var rly:Number=luy + this.height / ad.drawingLayer.scaleY;
		ZoomInfo.x = lux;
		ZoomInfo.y = luy;
		ZoomInfo.width = Math.abs(lux - rlx);
		ZoomInfo.height = Math.abs(luy - rly);
	}
	return ZoomInfo;
}
//Gets redmarks from the archibusdrawing
public function getRedmarks():Object {
	var ads:Array=this.dwgContainer.getChildren();
	var ad:ArchibusDrawing=null;

	ad=ads[0] as ArchibusDrawing;

	var drawingRedmarks:Object=new Object();
	drawingRedmarks.drawingName=ad.getName();
	drawingRedmarks.redmarks=ad.saveRedmarks();

	var lux:Number=-1 * ad.x / ad.drawingLayer.scaleX;
	var luy:Number=-1 * ad.y / ad.drawingLayer.scaleY;
	var rlx:Number=lux + this.width / ad.drawingLayer.scaleX;
	var rly:Number=luy + this.height / ad.drawingLayer.scaleY;


	var position:Object=new Object();
	position.lu={x: lux, y: luy};
	position.rl={x: rlx, y: rly}
	drawingRedmarks.position=position;

	return drawingRedmarks;
}

//Draws redmarks on the archibusdrawing
public function drawRedmarks(opts:Object):void {
	this.redmarkOpts.set(opts);
	var locData:Location=new Location();
	locData.setDrawingName(redmarkOpts.rawDwgName);
	var ad:ArchibusDrawing=this.getDrawingByLocation(locData);

	if (ad == null) {
		doCompleteRedmarks=true;
		this.addDrawing(locData, opts);
	} else {
		if (ad.fullyLoaded) {
			this.completeRedmarks();
		} else {
			ad.addEventListener("fullyLoaded", completeRedmarksWhenFullyLoaded);
		}
	}
}

//Draws redmarks when archibusdrawing is fully loaded
private function completeRedmarksWhenFullyLoaded(e:Event):void {
	var ad:ArchibusDrawing=e.target as ArchibusDrawing;
	if (ad.fullyLoaded) {
		ad.removeEventListener("fullyLoaded", completeRedmarksWhenFullyLoaded);
		this.completeRedmarks();
	}
}

/**
 * Completes the drawRedmarks functionality.  This method will be called
 * when the 'assetsLoaded' event is fired if needed, or called directly.
 */
public function completeRedmarks():void {
	doCompleteRedmarks=false;
	var ads:Array=this.dwgContainer.getChildren();
	for (var j:int=0; j < ads.length; j++) {
		var ad:ArchibusDrawing=ads[j] as ArchibusDrawing;
		if (ad.location.getDrawingName() == this.redmarkOpts.rawDwgName) {
			//clear previous redmarks before drawing new
			ad.clearRedmarks();

			ad.drawRedmarks(this.redmarkOpts.redmarks as Array);
			if (this.redmarkOpts.position != null) {

				//check zoom
				var redlineWidth:Number=Math.abs(this.redmarkOpts.position.lux - this.redmarkOpts.position.rlx);
				var redlineHeight:Number=Math.abs(this.redmarkOpts.position.luy - this.redmarkOpts.position.rly);
				var scale:Number=Math.min(this.width / redlineWidth, this.height / redlineHeight);

				var x:Number=-1 * this.redmarkOpts.position.lux * scale;
				var y:Number=-1 * this.redmarkOpts.position.luy * scale;

				var width:Number=ad.dwgDrawing.content.width * scale;
				var height:Number=ad.dwgDrawing.content.height * scale;
				ad.resize(width, height);
				ad.x=x;
				ad.y=y;

			}
		}
	}
}

//Clears redmarks from archibusdrawing
public function clearRedmarks():void {
	var ads:Array=this.dwgContainer.getChildren();
	var ad:ArchibusDrawing=null;

	for (var i:Number=0; i < ads.length; i++) {
		ad=ads[i] as ArchibusDrawing;
		ad.clearRedmarks();
	}
}

//Enables redlining (show button to open redlines toolbar)
public function enableRedmarks():void {
	this.currentState=ToolbarMode.SELECT;

	this.redmarksEnabled=true;

	this.navCanvas.showRedlineButtons();
}

//Disables redlining (hide button to open redlines toolbar)
public function disableRedmarks():void {
	this.currentState=ToolbarMode.SELECT;

	this.redmarksEnabled=false;

	this.navCanvas.hideRedlineButtons();

}

//checks if all redmarks are visible in the current zoomlevel
public function checkAllRedmarksVisible():Boolean {
	var saveResult:Object=getRedmarks();
	var width:Number=(saveResult.position.rl.x as Number) - (saveResult.position.lu.x as Number);
	var height:Number=(saveResult.position.rl.y as Number) - (saveResult.position.lu.y as Number);
	var rec:Rectangle=new Rectangle(saveResult.position.lu.x, saveResult.position.lu.y, width, height);

	for (var i:int=0; i < saveResult.redmarks.length; i++) {
		var rmType:String=saveResult.redmarks[i].type;
		if (rmType == DrawMode.drawModeLine || rmType == DrawMode.drawModeArrow || rmType == DrawMode.drawModeRectangle || rmType == DrawMode.drawModeText) {
			var point:Point=new Point(saveResult.redmarks[i].x1, saveResult.redmarks[i].y1);
			var otherPoint:Point=new Point(saveResult.redmarks[i].x2, saveResult.redmarks[i].y2);
			if (!(rec.containsPoint(point) && rec.containsPoint(otherPoint))) {
				return false;
			}
		} else if (saveResult.redmarks[i].type == DrawMode.drawModePositionMarker) {
			if (!rec.contains(saveResult.redmarks[i].x, saveResult.redmarks[i].y)) {
				return false;
			}
		}
	}
	return true;
}

//Remove last redline (actually clear all and redraw all but the last one)
public function undoLastRedline():void {
	var ad:ArchibusDrawing=dwgContainer.getChildAt(0) as ArchibusDrawing;
	ad.undoLastRedline();
}


