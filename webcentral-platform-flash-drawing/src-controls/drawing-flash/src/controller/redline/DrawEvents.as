import flash.display.LineScaleMode;
import flash.geom.Point;

import mx.controls.Text;
import mx.controls.TextArea;

import src.controller.toolbar.ToolbarMode;
import src.model.redline.DrawMode;
import src.model.util.GraphicsUtilities;
import src.view.redline.EditableTextArea;

private var _drawingMode:String=DrawMode.drawModeOff;
private var redlineThickness:Number=3;

//coordinates
private var x1:Number;
private var y1:Number;
private var x2:Number;
private var y2:Number;

//previous coordinates
private var prevx1:Number;
private var prevy1:Number;
private var prevx2:Number;
private var prevy2:Number;

//currently drawing?
private var isDrawing:Boolean=false;

//current drawing color
public var drawColor:uint=0xff0000;

//redmarks drawng
private var _redMarks:Array;

//list of last redline types
private var lastRedlineTypes:Array;
//number of text fields
private var nrTextFields:Number=0;
//list of last text fields
private var lastTextFields:Array;

public function set drawingMode(value:String):void {
	this._drawingMode=value;
}

public function get drawingMode():String {
	return this._drawingMode;
}

public function get redMarks():Array {
	return this._redMarks;
}

//mouse down listener for redlining. saves the current mouse coordinates
public function handleRedliningMouseDown(mouseX:Number, mouseY:Number):void {
	var parentApp:abDrawing=(this.parentApplication as abDrawing)
	if (parentApp.currentState != ToolbarMode.DRAW)
		return;

	if (drawingMode != DrawMode.drawModeOff) {
		x1=mouseX;
		y1=mouseY;
		isDrawing=true;
	}
}

//mouse over listener for redlining. shows redline preview
public function handleRedliningMouseOver(mouseX:Number, mouseY:Number, buttonDown:Boolean):void {
	var parentApp:abDrawing=(this.parentApplication as abDrawing)
	if (parentApp.currentState != ToolbarMode.DRAW)
		return;

	if (drawingMode != DrawMode.drawModeOff) {
		if (!buttonDown) {
			isDrawing=false;
		}

		x2=mouseX;
		y2=mouseY;
		if (isDrawing) {
			if (drawingMode == DrawMode.drawModeLine) {
				preview.graphics.clear();
				preview.graphics.lineStyle(redlineThickness, drawColor);
				preview.graphics.moveTo(x1, y1);
				preview.graphics.lineTo(x2, y2);
			} else if (drawingMode == DrawMode.drawModeRectangle) {
				preview.graphics.clear();
				preview.graphics.lineStyle(redlineThickness, drawColor);

				preview.graphics.moveTo(x1, y1);
				preview.graphics.lineTo(x1, y2);
				preview.graphics.lineTo(x2, y2);
				preview.graphics.lineTo(x2, y1);
				preview.graphics.lineTo(x1, y1);

			} else if (drawingMode == DrawMode.drawModeText) {
				//do nothing?
				txtInput.visible=true;
				txtInput.x=x1;
				txtInput.y=y1;
				txtInput.width=Math.max(x1, x2) - Math.min(x1, x2);
				txtInput.height=Math.max(y1, y2) - Math.min(y1, y2);

			} else if (drawingMode == DrawMode.drawModeArrow) {
				preview.graphics.clear();
				preview.graphics.lineStyle(redlineThickness, drawColor);

				var start:Point=new Point(x1, y1);
				var end:Point=new Point(x2, y2);
				GraphicsUtilities.drawArrow(preview.graphics, start, end, null);
			}
		}
	}
}

//mouse up listener for redlining. draws the redline on the drawingLayer and saves the coordinates (+type) in the redMarks array
public function handleRedliningMouseUp(mouseX:Number, mouseY:Number):void {
	var parentApp:abDrawing=(this.parentApplication as abDrawing)
	if (parentApp.currentState != ToolbarMode.DRAW)
		return;

	if (drawingMode != DrawMode.drawModeOff) {
		preview.graphics.clear();
		drawingLayer.graphics.lineStyle(redlineThickness, drawColor, 1.0, false, LineScaleMode.NONE);

		x1=x1 / drawingLayer.scaleX;
		y1=y1 / drawingLayer.scaleY;
		x2=x2 / drawingLayer.scaleX;
		y2=y2 / drawingLayer.scaleY;
		drawingLayer.graphics.moveTo(x1, y1);

		if (prevx1 == prevx2 && prevy1 == prevy2 && x1 == prevx1 && y1 == prevy1) {
			return; //avoid drawing to infinity
		}
		prevx1=x1;
		prevx2=x2;
		prevy1=y1;
		prevy2=y2;
		if (x1 == x2 && y1 == y2 && drawingMode != DrawMode.drawModePositionMarker) {
			return;
		}
		if (drawingMode == DrawMode.drawModeLine) {
			drawingLayer.graphics.lineTo(x2, y2);
			this.saveLine(x1, y1, x2, y2);
		} else if (drawingMode == DrawMode.drawModeRectangle) {
			drawingLayer.graphics.lineTo(x1, y2);
			drawingLayer.graphics.lineTo(x2, y2);
			drawingLayer.graphics.lineTo(x2, y1);
			drawingLayer.graphics.lineTo(x1, y1);

			saveRect(x1, y1, x2, y2);
		} else if (drawingMode == DrawMode.drawModeText) {
			txtInput.visible=false;
			var txtField:EditableTextArea=new EditableTextArea("txtField" + nrTextFields, 
				Math.max(labelLayer.fsTextSize, labelLayer.idealLabelTextSize), 
				drawColor,
				Math.max(x1, x2) - Math.min(x1, x2),
				Math.max(y1, y2) - Math.min(y1, y2),
				x1,
				y1);
			nrTextFields++;
			drawingLayer.addChild(txtField);
			if (this.lastRedlineTypes == null) {
				this.lastRedlineTypes=new Array();
			}
			this.lastRedlineTypes.push(DrawMode.drawModeText);
			if (this.lastTextFields == null) {
				this.lastTextFields=new Array();
			}
			this.lastTextFields.push(txtField);
		} else if (drawingMode == DrawMode.drawModeArrow) {
			var start:Point=new Point(x1, y1);
			var end:Point=new Point(x2, y2);
			GraphicsUtilities.drawArrow(drawingLayer.graphics, start, end, {shaftThickness: redlineThickness, headWidth: 100, headLength: 150, shaftPosition: .20, edgeControlPosition: .5});
			saveArrow(x1, y1, x2, y2);
		} else if (drawingMode == DrawMode.drawModePositionMarker) {
			drawingLayer.graphics.lineStyle(2, drawColor, 1.0, false, LineScaleMode.NONE);
			drawingLayer.graphics.moveTo(x2, y2 - 20);
			drawingLayer.graphics.lineTo(x2, y2 + 20);
			drawingLayer.graphics.moveTo(x2 - 20, y2);
			drawingLayer.graphics.lineTo(x2 + 20, y2);
			drawingLayer.graphics.drawCircle(x2, y2, 40);
			savePositionMarker(x2, y2);
		}

		isDrawing=false;
	}
}

//save all redmarks in an array
public function saveRedmarks():Array {
	var redmarkArray:Array=new Array();
	if (this.redMarks != null) {
		redmarkArray=this.redMarks.concat();
	} else {
		return redmarkArray;
	}

	var children:Array=drawingLayer.getChildren();
	for (var i:int=0; i < children.length; i++) {
		var child:Object=children[i];
		if (child is TextArea) {
			var txtInput:TextArea=child as TextArea;
			var redline:Object=new Object();
			redline.type=DrawMode.drawModeText;
			redline.x1=txtInput.x;
			redline.y1=txtInput.y;
			redline.x2=txtInput.x + txtInput.width;
			redline.y2=txtInput.y + txtInput.height;

			redline.text=child.text;

			redmarkArray.push(redline);
		}
	}
	return redmarkArray;
}

//saves line coordinates to redMarks array
private function saveLine(x1:Number, y1:Number, x2:Number, y2:Number):void {
	var redline:Object=new Object();
	redline.type=DrawMode.drawModeLine;

	redline.x1=x1;
	redline.y1=y1;
	redline.x2=x2;
	redline.y2=y2;

	if (this._redMarks == null) {
		this._redMarks=new Array();
	}
	this._redMarks.push(redline);
	if (this.lastRedlineTypes == null) {
		this.lastRedlineTypes=new Array();
	}
	this.lastRedlineTypes.push(DrawMode.drawModeLine);
	this.parentApplication.navCanvas.drawLineButton.source = this.parentApplication.navCanvas._drawLineDefaultImage;
}

//saves rectangle coordinates to redMarks array
private function saveRect(x1:Number, y1:Number, x2:Number, y2:Number):void {
	var redline:Object=new Object();
	redline.type=DrawMode.drawModeRectangle;
	redline.x1=x1;
	redline.y1=y1;
	redline.x2=x2;
	redline.y2=y2;

	if (this._redMarks == null) {
		this._redMarks=new Array();
	}
	this._redMarks.push(redline);
	if (this.lastRedlineTypes == null) {
		this.lastRedlineTypes=new Array();
	}
	this.lastRedlineTypes.push(DrawMode.drawModeRectangle);
	this.parentApplication.navCanvas.drawRectButton.source = this.parentApplication.navCanvas._drawRectDefaultImage;
}

//saves arrow coordinates to redMarks array
private function saveArrow(x1:Number, y1:Number, x2:Number, y2:Number):void {
	var redline:Object=new Object();
	redline.type=DrawMode.drawModeArrow;
	redline.x1=x1;
	redline.y1=y1;
	redline.x2=x2;
	redline.y2=y2;

	if (this._redMarks == null) {
		this._redMarks=new Array();
	}
	this._redMarks.push(redline);
	if (this.lastRedlineTypes == null) {
		this.lastRedlineTypes=new Array();
	}
	this.lastRedlineTypes.push(DrawMode.drawModeArrow);
	this.parentApplication.navCanvas.drawArrowButton.source = this.parentApplication.navCanvas._drawArrowDefaultImage;
}

//saves position marker coordinates to redMarks array
private function savePositionMarker(x:Number, y:Number):void {
	var redline:Object=new Object();
	redline.type=DrawMode.drawModePositionMarker;
	redline.x=x;
	redline.y=y;

	if (this._redMarks == null) {
		this._redMarks=new Array();
	}
	this._redMarks.push(redline);
	if (this.lastRedlineTypes == null) {
		this.lastRedlineTypes=new Array();
	}
	this.lastRedlineTypes.push(DrawMode.drawModePositionMarker);
	this.parentApplication.navCanvas.drawPositionMarkerButton.source = this.parentApplication.navCanvas._drawMarkerDefaultImage;
}

//draws given redmarks on drawing layer
public function drawRedmarks(redlines:Array):void {
	if (redlines != null) {
		for (var i:int=0; i < redlines.length; i++) {

			var redline:Object=redlines[i];
			drawingLayer.graphics.lineStyle(redlineThickness, drawColor, 1.0, false, LineScaleMode.NONE);
			drawingLayer.graphics.moveTo(redline.x1, redline.y1);

			if (redline.type == DrawMode.drawModeLine) {
				drawingLayer.graphics.lineTo(redline.x2, redline.y2);
			} else if (redline.type == DrawMode.drawModeRectangle) {
				drawingLayer.graphics.lineTo(redline.x2, redline.y1);
				drawingLayer.graphics.lineTo(redline.x2, redline.y2);
				drawingLayer.graphics.lineTo(redline.x1, redline.y2);
				drawingLayer.graphics.lineTo(redline.x1, redline.y1);
			} else if (redline.type == DrawMode.drawModeArrow) {
				var start:Point=new Point(redline.x1, redline.y1);
				var end:Point=new Point(redline.x2, redline.y2);
				GraphicsUtilities.drawArrow(drawingLayer.graphics, start, end, {shaftThickness: redlineThickness, headWidth: 100, headLength: 150, shaftPosition: .20, edgeControlPosition: .5});
			} else if (redline.type == DrawMode.drawModePositionMarker) {
				drawingLayer.graphics.lineStyle(2, drawColor, 1.0, false, LineScaleMode.NONE);
				drawingLayer.graphics.moveTo(redline.x, redline.y - 20);
				drawingLayer.graphics.lineTo(redline.x, redline.y + 20);
				drawingLayer.graphics.moveTo(redline.x - 20, redline.y);
				drawingLayer.graphics.lineTo(redline.x + 20, redline.y);
				drawingLayer.graphics.drawCircle(redline.x, redline.y, 40);
			} else if (redline.type == DrawMode.drawModeText) {
				//var txtField:RedlineLabel = new RedlineLabel();
				var txtField:Text=new Text();
				txtField.width=redline.x2 - redline.x1;
				txtField.height=redline.y2 - redline.y1;
				txtField.text=redline.text;

				txtField.x=redline.x1;
				txtField.y=redline.y1;
				txtField.scaleX=txtField.scaleY=1 / labelLayer.scaleY;
				txtField.setStyle("fontSize", txtField.setStyle("fontSize", Math.max(labelLayer.fsTextSize, labelLayer.idealLabelTextSize)));
				txtField.setStyle("color", drawColor);
				txtField.setStyle("fontWeight", "bold");
				drawingLayer.addChild(txtField);
			}
		}
	}
}

//Removes all redmarks from the drawing layer
public function clearRedmarks():void {
	drawingLayer.graphics.clear();
	//remove text
	drawingLayer.removeAllChildren();
	//empty saved redmarks
	this._redMarks=new Array();
	this.lastRedlineTypes=new Array();
	this.lastTextFields=new Array();
}

public function undoLastRedline():void {
	if (this.lastRedlineTypes && this.lastRedlineTypes.length > 0) {
		var type:String=this.lastRedlineTypes.pop();
		if (type == DrawMode.drawModeText) {
			if (lastTextFields.length > 0) {
				drawingLayer.removeChild(lastTextFields.pop());
			}
		} else {
			drawingLayer.graphics.clear();
			this._redMarks.pop()
			drawRedmarks(this._redMarks);
		}
	}
}


