package src.controller.highlight {
	import flash.external.ExternalInterface;

	import src.model.util.CommonUtilities;
	import src.model.util.JsFunctionConstant;

	/**
	 * Define the highlight type retrieved from JavaScript's settings. The javascript settings usually is obtained
	 * from the constructor's configuration file or JavaScript setteer function.
	 */
	public class HighlightType {
		public static const THEMATIC:String="thematic";
		public static const RESTRICTION:String="restriction";
		public static const PREDEFINED:String="predefined";
		public static const AUTOASSIGNED:String="automatically assigned";

		// Property: the highlight type to apply to drawing, the value is either 'thematic' or 'restriction', defaults to 'thematic'.
		// If is set to 'thematic', the 'thematicHighlightStyle' will dictate colors used
		// If is set to 'restriction', this will apply the 'assigned' fill style
		private var _type:String=HighlightType.THEMATIC;

		//Property: true if highlight style is thermatic, false otherwise.
		private var _bThematic:Boolean=true;

		// Property: the highlight type for thermatic highlight, the value is either 'predefined' or 'automatically assigned' colors.
		// This property is only used when the 'hightlightType' is set to 'thematic'
		private var _thematicHighlightStyle:String=HighlightType.PREDEFINED;

		//Property: true if the thermatic highlight style is "predefine", false otherwise.
		// This property is only used when the 'hightlightType' is set to 'thematic'
		private var _bPredefined:Boolean=true;


		/**
		 * Default constructor.
		 */
		public function HighlightType() {
			super();

			var settings:Object=ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_GETHIGHLIGHTSETTINGS)) as Object;
			if (settings != null) {
				if (CommonUtilities.isValid(settings, 'highlightType') && settings.highlightType == HighlightType.RESTRICTION) {
					_type == HighlightType.RESTRICTION;
					_bThematic=false;
				}

				if (CommonUtilities.isValid(settings, 'thematicHighlightStyle') && settings.thematicHighlightStyle == HighlightType.AUTOASSIGNED) {
					_type == HighlightType.AUTOASSIGNED;
					_bPredefined=false;
				}
			}

		}

		public function isHighlightThematic():Boolean {
			return this._bThematic;
		}

		public function isThematicPredefined():Boolean {
			return this._bPredefined;
		}



	}
}
