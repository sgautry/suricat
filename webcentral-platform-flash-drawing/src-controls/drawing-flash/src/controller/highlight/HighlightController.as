// ActionScript file
import src.model.config.*;
import src.model.data.Location;
import src.view.asset.*;
import src.view.drawing.ArchibusDrawing;
import src.view.highlight.*;



/**
 * Highlights the provided set of rooms in given color.
 * This method initializes the highlightAssets functionality, which may
 * require a load of a drawing to support.  In that case, the actual
 * highlighting can only occur after the default asset map information
 * is fully loaded.  The 'completeHighlights' method completes the action.
 *
 * @param	opts	A JavaScript object used for the DwgOptions class
 */
public function highlightAssets(opts:Object):void {
	// highlight assets
	this.perDrawingConfig.set(opts);

	// zoomIn is not supported with highlightAssets, ensure it is set to false
	this.perDrawingConfig.zoomToId=false;

	var location:Location=new Location();
	location.setFromArray(this.perDrawingConfig.dwgName, this.perDrawingConfig.rawDwgName);

	// Ensure that the drawing for the highlighted rooms is loaded
	var ad:ArchibusDrawing=getDrawingByLocation(location);
	if (ad == null) {
		ad=addDrawing(location, opts);
		if (ad)
			ad.doCompleteHighlights=true;
	} else {
		if (ad.fullyLoaded) {
			ad.completeHighlights();
		} else {
			ad.addEventListener("fullyLoaded", completeHighlightsWhenFullyLoaded);
		}
	}
}


