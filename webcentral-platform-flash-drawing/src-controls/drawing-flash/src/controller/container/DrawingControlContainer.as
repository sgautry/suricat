import com.adobe.images.PNGEncoder;

import flash.display.*;
import flash.events.*;
import flash.external.ExternalInterface;
import flash.geom.*;
import flash.system.*;
import flash.utils.ByteArray;
import flash.utils.Dictionary;

import mx.collections.ArrayCollection;
import mx.controls.*;
import mx.controls.Alert;
import mx.controls.List;
import mx.core.UIComponent;
import mx.events.DragEvent;
import mx.events.FlexEvent;
import mx.managers.DragManager;
import mx.managers.PopUpManager;
import mx.printing.FlexPrintJob;
import mx.utils.StringUtil;

import src.controller.highlight.HighlightType;
import src.controller.toolbar.ToolbarMode;
import src.model.asset.*;
import src.model.asset.AssetLoader;
import src.model.assign.*;
import src.model.config.*;
import src.model.data.*;
import src.model.data.Location;
import src.model.layout.LayoutUtilities;
import src.model.text.AbTextFormat;
import src.model.util.*;
import src.view.asset.AssetLayer;
import src.view.asset.AssetObject;
import src.view.container.AbProgressBar;
import src.view.container.ImageCreator;
import src.view.drawing.ArchibusDrawing;
import src.view.highlight.*;
import src.view.menu.RightClickMenu;
import src.view.panel.AssetsPanel;
import src.view.text.*;
import src.view.toolbar.*;
import src.view.util.URLMultiLoader;


/**
 * Property: the drawing control properties defined in controls-drawing.xml file.
 */
private var _controlConfig:ControlConfig=new ControlConfig();

/**
 * Property: Per drawing settings to control display characteristics, usually passed in from javascript
 */
private var _perDrawingConfig:DrawingConfig=new DrawingConfig();

/**
 * Property: the full path of the drawing file, /archibus/projects/hq/enterprise-graphics/hq17.swf ect.
 */
private var _backgroundFile:String='';

/**
 * Property: the origin point of the current drawing
 */
private var _drawingStartPoint:Point;

/**
 * Proeprty: a map for all localized strings
 */
private var _localizedUtilities:LocalizeUtilities=new LocalizeUtilities();

/**
 * Property: the progress bar displayed when loading the drawing(s)
 */
private var _progressBar:AbProgressBar=null;

/**
 * Property: drawing tooltip.
 */
private var _tooltip:AbToolTip=new AbToolTip();

/**
 * Property: true when tooltip is enabled, false otherwise. The tooltip is temporarily disabled while in non-Selectd mode.
 */
public var _tooltipEnabled:Boolean=true;

/**
 * Property: layout manager
 */
private var _layoutUtilities:LayoutUtilities=new LayoutUtilities();

// 
/**
 * Property: image handler
 */
private var _imageCreator:ImageCreator=new ImageCreator();

/**
 * Property:  Highlight Type.
 */
private var _highlightType:HighlightType=new HighlightType();

/**
 * Property:  toolbar manager.
 */
private var _toolbarUtilities:ToolbarUtilities=new ToolbarUtilities();

/**
 * Property: the drawing manager to manage assign logic
 */
public var _assignUtilities:AssignUtilities=null;

/**
 * Records the asset's id for the last mouse click event.
 */
private var _lastMouseEventIds:Array=null;

/**
 * Context to define security and application domains
 */
private var _loaderContext:LoaderContext;

/**
 * Asset Panels - referred by asset type like "em".
 */
private var _assetPanels:Dictionary=new Dictionary();

/**
 * An arrray of the selected assets.
 * Each element of the selected asset array has this data structure:
 * 		.pks: an array of primary keys of the select asset
 * 		.selected: boolean. true of the asset is selected, false if unselected.
 * 		.color:  hex color.
 */
private var _multipleSelectedAssets:Array=new Array();

/**
 *Multiple Assets Processing Progress Bar 
 */
private var multipleAssetsProcessingProgressBar:ProgressBar;


/**
 * Adds the named drawing
 *
 * @param	locIn	A Location object
 * @param	opts  	A DwgOptions object. Optional
 */
public function addDrawing(location:Object, opts:Object=null):ArchibusDrawing {
	var loc:Location=new Location(location);
	var drawingName:String=loc.getDrawingName();

	if (drawingName == null || drawingName == "" || drawingName == "0")
		return null;

	//set the per drawing options.
	_perDrawingConfig.set(opts);
	
	_perDrawingConfig.rawDwgName = drawingName;

	// If multiple drawings are not supported, ensure that all others are removed
	if (!this._perDrawingConfig.multiple)
		removeAllDrawings(drawingName);

	// set the bdrawing file to load
	_backgroundFile=_perDrawingConfig.folder + "/" + drawingName + _perDrawingConfig.backgroundSuffix + ".swf";

	// set the current mode to 'select', except when redlining is enabled
	this.currentState=ToolbarMode.SELECT;

	init();

	try {
		var ads:Array=dwgContainer.getChildren();

		//no need to load if it is already loaded.
		var ad:ArchibusDrawing=isDrawingLoaded(ads, drawingName);
		if (ad){
			ad.setDraggableAssets(ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_GETDRAGGABLEASSETS)));
			return ad;
		}

		this._progressBar=new AbProgressBar(this.width, this.height, drawingName + _perDrawingConfig.backgroundSuffix);
		this.addChild(this._progressBar);
		// Create the drawing container
		ad=new ArchibusDrawing(_perDrawingConfig, loc);
		//set flag as drawing has not completed loading.
		ad.loadComplete=false;

		//set flag as room has not been found.
		ad.dwgLoadedForFind=false;

		// Load the architectural background
		ad.backgroundFile=_backgroundFile;

		// Add to drawing container
		dwgContainer.addChild(ad);

		//if the first drawing is added, then add the navigation toolbar
		if (dwgContainer.getChildren().length == 1){
			this.navCanvas.visible=true;
			// only enable multiple selection button when the assign mode is one-to-many
			this.navCanvas.multiSelectButton.visible = ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_MULTISELECTHANDLER_DEFINED));
		}

		// Define callback for loaded assets (assets are loaded after drawing is fully loaded)
		ad.addEventListener("assetsLoaded", assetsLoaded);

		// Define callback for architectural background load complete
		ad.addEventListener("updateSwf", drawingLoaded);
		ad.addEventListener("loadComplete", processLoadComplete);
		ad.addEventListener("loadFailed", processLoadFailed);
	} catch (err:Error) {
		ad.doCompleteHighlights=false;
		ad.doCompleteFindAsset=false;
		Alert.show(LocalizeUtilities.wordError + ": " + err.message);
		return null;
	} finally {
		clearProgressBar();
	}

	return ad;
}

/**
 * Called by JS to highlight and label multiple assets
 */
public function applyAssets(passedAssets:Array):void{
	if(multipleAssetsProcessingProgressBar == null){
		multipleAssetsProcessingProgressBar =  new ProgressBar ();
		multipleAssetsProcessingProgressBar.mode = ProgressBarMode.MANUAL;
		
		multipleAssetsProcessingProgressBar.width = 150;
		multipleAssetsProcessingProgressBar.height = 50;
		multipleAssetsProcessingProgressBar.setStyle("barColor", 0xFF0000);
		multipleAssetsProcessingProgressBar.indeterminate = true;
		multipleAssetsProcessingProgressBar.labelPlacement = ProgressBarLabelPlacement.CENTER;
		multipleAssetsProcessingProgressBar.move(this.width / 2, this.height / 2);
		var localizeUtilities:LocalizeUtilities=new LocalizeUtilities();
		if(localizeUtilities.map["MESSAGE_PLEASE_WAIT"] && localizeUtilities.map["MESSAGE_PLEASE_WAIT"]!=""){
			multipleAssetsProcessingProgressBar.label = localizeUtilities.map["MESSAGE_PLEASE_WAIT"];
		}else{
			multipleAssetsProcessingProgressBar.label="Please wait...";
		}
		
		multipleAssetsProcessingProgressBar.setStyle("fontSize", 12);
	}
	if(!this.contains(multipleAssetsProcessingProgressBar)){
		this.addChild(multipleAssetsProcessingProgressBar);
		
	}
	
	var drawing:ArchibusDrawing;
	var ads:Array=dwgContainer.getChildren();
	var assets:Array = reOrderAssetsArray(passedAssets);
	
	for (var i:Number=0; i < ads.length; i++) {
		drawing=ads[i] as ArchibusDrawing;
		
		drawing.assetLayer.clearAssets();
		drawing.labelLayer.clearLabels();
		drawing.highlight._usedValueColorList = {};
		drawing.assetLayer.removeAssets();
		if(assets != null && assets.length > 0){
			var urlMultiLoader:src.view.util.URLMultiLoader = new src.view.util.URLMultiLoader(drawing);
			var counter:Number = 0;
			for(var j:int=0;j<assets.length;j++){
				urlMultiLoader.addURLRequest(assets[j].assetType, _perDrawingConfig.folder, assets[j].planSuffix, _perDrawingConfig.assetSuffix);
				var highlightFormat:HighlightFormat = new HighlightFormat(HighlightFormat.REGULAR);
				if(assets[j].highlightType !=null && assets[j].highlightType == 'border'){
					highlightFormat = new HighlightFormat(HighlightFormat.BORDER);
					if(assets[j].border != null){
						highlightFormat.setBorder(Number(assets[j].border));
					}
				}
				urlMultiLoader.addCallBack(assets[j].assetType, applyAsset, [drawing, assets[j].assetType, assets[j].highlightDSName, assets[j].labelDSName,  highlightFormat , assets[j].labelingConfig, counter++]);
			}
			
			urlMultiLoader.load();
		}else{
			if(multipleAssetsProcessingProgressBar != null && this.contains(multipleAssetsProcessingProgressBar)){
				this.removeChild(multipleAssetsProcessingProgressBar);
			}
		}
	}

}


//always to make su or rm at the top od assets array
private function reOrderAssetsArray(assets:Array):Array{
	var result:Array = [];
	for(var j:int=0;j<assets.length;j++){
		var assetType:String = assets[j].assetType;
		if(assetType == "su"){
			result.push(assets[j]);
			assets.splice(j, 1);
		}
		if(assetType == "rm"){
			result.push(assets[j]);
			assets.splice(j, 1);
		}
	}
	return result.concat(assets)
}

private function applyAsset(ad:ArchibusDrawing, assetType:String, highlightDSName:String, labelDSName:String,  highlightFormat:HighlightFormat, labelingConfig:Object, counter:Number):void{
		if(highlightDSName != null){
			
			//do highlighting
			var datasource:DataSource =new DataSource(SourceType.HILITE, highlightDSName);
			if(highlightFormat !=null && highlightFormat.getHighlightFormat() == HighlightFormat.BORDER){
				if(highlightFormat == null){
					highlightFormat = new HighlightFormat(HighlightFormat.BORDER);
				}
				highlightFormat.setAssetType(assetType);
				ad.highlight.doBordersHighlight(datasource, highlightDSName, false, highlightFormat);
			}else{
				if(highlightFormat == null){
					highlightFormat = new HighlightFormat(HighlightFormat.REGULAR);
				}
				highlightFormat.setAssetType(assetType);
				ad.highlight.doRegularHighLight(datasource, highlightDSName, false, highlightFormat, counter > 0);
			}
		}
		if(labelDSName != null){
			//do labeling
			datasource =new DataSource(SourceType.LABEL, labelDSName);
			ad.labelLayer.populateLabels(ad.location.getPrimarykeysArray(), ad, datasource, labelingConfig);
		}
		if(multipleAssetsProcessingProgressBar != null && this.contains(multipleAssetsProcessingProgressBar)){
			this.removeChild(multipleAssetsProcessingProgressBar);
		}
}


/**
 * Updates the currently loaded drawings per the specified data source
 *
 * @param type:	either 'highlights' or 'labels'
 */
public function applyDS(type:String, highlightDSName:String=null, bordersHighlightDSName:String=null):void {
	var ad:ArchibusDrawing;
	var ads:Array=dwgContainer.getChildren();
	var draggableAssets:Array = null;
	if (type == 'labels'){
		draggableAssets = ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_GETDRAGGABLEASSETS));
	}
	for (var i:Number=0; i < ads.length; i++) {
		ad=ads[i] as ArchibusDrawing;
		if(type == 'labels'){
			ad.setDraggableAssets(draggableAssets);
		}
		ad.applyDS(type, highlightDSName, bordersHighlightDSName);
	}
}

/**
 * Called by Javascript to un-select specified selected assets.
  * @param idsList:	Array of array like [['HQ','18','105'], ['HQ','18','106']].
 */
public function unselectAssets(idsList:Array):void {
	doSelectAssets(idsList, false);
}

/**
 * Called by Javascript to select specified selectable assets.
 * @param idsList:	Array of array like [['HQ','18','105'], ['HQ','18','106']].
 */
public function selectAssets(idsList:Array):void {
	doSelectAssets(idsList, true);
}

/**
 * Called by unselectAssets and selectAssets.
 */
private function doSelectAssets(idsList:Array, isSelect:Boolean):void {
	if (idsList == null || idsList.length == 0) {
		return;
	}
	for (var i:int=0; i < dwgContainer.getChildren().length; i++) {
		var ad:ArchibusDrawing=dwgContainer.getChildren()[i] as ArchibusDrawing;
		for (var j:int=0; j < idsList.length; j++) {
			var ids:Array=idsList[j];
			var asset:AssetObject=ad.assetLayer.getAssetOb(ids);
			if (asset) {
				if (isSelect && asset.properties && asset.properties.isSelectable) {
					var assginedColor:String = _assignUtilities.getAssignedColor(ids);
					if(assginedColor){
						asset.select(null, assginedColor);
					}else{
						asset.select();
					}

				} else if (asset.properties && asset.properties.isSelected) {
					asset.unselect();
				}
			}
		}
	}
}

/**
 * Add data row actions to the asset panel (a popup dialog) specified by asset type
 *
 */
public function addAssetPanelRowAction(actionConfig:Object):void {
	var assetPanel:AssetsPanel=getAssetPanel(actionConfig.type);
	assetPanel.addRowAction(actionConfig);
}

/**
 *
 * Refresh the asset panel (a popup dialog) specified by asset type
 */
public function refreshAssetPanel(type:String, restriction:Object=null):void {
	var assetPanel:AssetsPanel=getAssetPanel(type);
	if (assetPanel != null) {
		assetPanel.refresh(restriction);
	}
}

/**
 *  Close (or hide) the asset panel (a popup dialog) specified by asset type
 *
 */
public function closeAssetPanel(type:String):void {
	var assetPanel:AssetsPanel=getAssetPanel(type);
	if (assetPanel != null) {
		assetPanel.close();
	}
}


/**
 * Enable asset panel (a popup dialog) specified by asset type
 *
 */
public function enableAssetPanel(type:String=null, title:String=null, dataSourceName:String=null, configOptions:Object=null):void {

	var assetPanel:AssetsPanel=getAssetPanel(type);
	if (assetPanel == null) {
		assetPanel=new AssetsPanel(configOptions);
		_assetPanels[type]=assetPanel;
	}
	if (title != null) {
		assetPanel.setTitle(title);
	}

	assetPanel.setDataSourceName(dataSourceName);

	PopUpManager.addPopUp(assetPanel, this);


	assetPanel.move(0, (this.height - assetPanel.height) >= 0 ? (this.height - assetPanel.height) : 0);
}

/**
 * Show asset panel (a popup dialog) specified by asset type
 *
 */
public function showAssetPanel(type:String):void {
	var assetPanel:AssetsPanel=getAssetPanel(type);
	if (assetPanel != null) {
		assetPanel.show(this);
	}
}

/**
 *
 * Get enabled asset panel (a popup dialog) specified by asset type
 */
public function getAssetPanel(type:String):AssetsPanel {
	for (var key:String in _assetPanels) {
		if (key == type) {
			return _assetPanels[key];
		}
	}

	return null;
}

/**
 * Removes specified labels.
 *
 * @param labels - Array. It looks like
 * 						[{field:'em.em_id',
							record:{rm.bl_id:'HQ',rm.fl_id:'18', rm.rm_id:'105', em.em_id:'XXX'}
						}]
 */
public function removeLabels(labels:Array):void {
	if (labels == null || labels.length == 0) {
		return;
	}

	var children:Array=dwgContainer.getChildren();
	for (var i:int=0; i < children.length; i++) {
		var ad:ArchibusDrawing=children[i] as ArchibusDrawing;
		var labLayer:LabelLayer=ad.labelLayer;
		for (var j:int=0; j < labels.length; j++) {
			var label:Object=labels[j];
			var layerObj:LabelObject=labLayer.getLabel(getLocation(label.record));

			if (layerObj == null) {
				continue;
			}
			layerObj.removeTextField(label.field, label.record[label.field]);
		}
		labLayer.refresh();
	}
}

/**
 *
 * Adds specified labels.
 *
* @param labels - Array. It looks like  [{field: 'em.em_id',
*				record:{
*                 	em.bl_id: 'HQ',
*                 	em.fl_id: '18',
*                	em.rm_id: '105',
*                	em.em_id: 'AFM',
*                	em.dp_id: 'XXX'
*          		}}]
*
*/
public function addLabels(labels:Array):void {
	if (labels == null || labels.length == 0) {
		return;
	}

	var defaultTextFormat:AbTextFormat=this._controlConfig.labelConfig.getTextFormat(LabelConfig.FORMAT_DEFAULT);
	for (var i:int=0; i < dwgContainer.getChildren().length; i++) {
		var ad:ArchibusDrawing=dwgContainer.getChildren()[i] as ArchibusDrawing;
		var labLayer:LabelLayer=ad.labelLayer;

		for (var j:int=0; j < labels.length; j++) {
			var label:Object=labels[j];
			var location:Array=getLocation(label.record);

			var layerObj:LabelObject=labLayer.getLabel(location);
			if (layerObj == null) {
				continue;
			}

			//XXX: avoid duplicated label
			layerObj.removeTextField(label.field, label.record[label.field]);
			layerObj.addField(label.field, label.record[label.field], defaultTextFormat, label.record);
		}

		labLayer.refresh();
	}
}

/**
 *
 * Adds specified highlgiht.
 *
 * @param values - Array. It looks like  [{color:1234, record:{'bl_id':'HQ','fl_id':'18','rm_id':'168'}}]
 *
 */
public function addHighlights(values:Array):void {
	if (values == null || values.length == 0) {
		return;
	}
	for (var i:int=0; i < dwgContainer.getChildren().length; i++) {
		var ad:ArchibusDrawing=dwgContainer.getChildren()[i] as ArchibusDrawing;
		if (ad.assetLayer == null) {
			continue;
		}
		for (var j:int=0; j < values.length; j++) {
			var data:Object=values[j];
			var location:Array=getLocation(data.record);
			var asset:AssetObject=ad.assetLayer.getAssetOb(location);

			if (asset == null) {
				continue;
			}
			
			//var color:String  = CommonUtilities.getColorFromValue(data.field, data.record[data.field], data.record[data.field])
			asset.select(null, data.color + "");
		}
	}
}

private function getLocation(record:Object):Array {
	var bl_id:String='';
	var fl_id:String='';
	var rm_id:String='';
	for (var name:String in record) {
		var value:String=record[name];
		if (name.indexOf('.location') > 0) {
			var ar:Array=value.split('-');
			if (ar != null && ar.length > 0) {
				bl_id=ar[0];
				fl_id=ar[1];
				rm_id=ar[2];
			}
		}

		if (name.indexOf('bl_id') >= 0) {
			bl_id=value;
		} else if (name.indexOf('fl_id') >= 0) {
			fl_id=value;
		} else if (name.indexOf('rm_id') >= 0) {
			rm_id=value;
		}
	}
	return [bl_id, fl_id, rm_id];
}

/**
 * Gets the label data and apply to the related drawing's assets.
 */
public function applyLabels():void {
	var labelData:Array=ExternalInterface.call(CommonUtilities.getFullFuncCall("getLabelData")) as Array;

	//labelData contain 3 elements: location, option, mode
	if (labelData.length < 3)
		return;

	//check if the label data contain the drawing information and the drawing is valid.
	var location:Object=labelData[0];
	var loc:Location=new Location(location);
	var ad:ArchibusDrawing=getDrawingByLocation(loc);
	if (ad == null)
		return;

	var dwgOpts:Object=labelData[1];
	var mode:int=labelData[2];

	//set the labels if the drawing is fully loaded
	if (ad.fullyLoaded) {
		ad.setLabels(dwgOpts, mode);
	} else {
		//otherwise, add event handler and call it when the drawing is fully loaded.
		ad.addEventListener("fullyLoaded", function(e:Event):void {
			if (ad.fullyLoaded) {
				ad.removeEventListener("fullyLoaded", arguments.callee);
				ad.setLabels(dwgOpts, mode);
			}
		});
	}
}


/**
 * Removes all persist fills for each drawing.
 */
public function clearPersistFills():void {
	var ads:Array=dwgContainer.getChildren();
	for (var j:int=0; j < ads.length; j++) {
		var ad:ArchibusDrawing=ads[j] as ArchibusDrawing;
		ad.clearPersistFills();
	}
	if(this.perDrawingConfig){
		this.perDrawingConfig.highlightId = '';
		this.perDrawingConfig.setRecs(null);
	}
}
/**
 * Removes the assets from each drawing
 */
public function clearRooms():void {
	var archibusDrawings:Array=dwgContainer.getChildren();
	for (var j:Number=0; j < archibusDrawings.length; j++) {
		var archibusDrawing:ArchibusDrawing=archibusDrawings[j] as ArchibusDrawing;
		archibusDrawing.clearAssets();
	}
}

/**
 * Print method to be used within Windows Prototype
 * (Obsolete, use Upload functionality below)
 *
 */
public function print():Array {
	var printlist:ArrayCollection=new ArrayCollection();

	var bd:BitmapData=new BitmapData(dwgContainer.width * 3, dwgContainer.height * 3);
	var m:Matrix=new Matrix();
	m.scale(2, 2);
	bd.draw(dwgContainer, m);

	var tmp:ByteArray=PNGEncoder.encode(bd);
	//var tmp:ByteArray = bd.getPixels(bd.rect);
	var arr:Array=[];
	for (var i:Number=0; i < tmp.length; i++) {
		arr[i]=tmp[i];
	}
	return arr;
}

/**
 * Print method to be used within Windows Prototype
 * (Obsolete, use Upload functionality below)
 *
 */
public function print2():void {
	var printJob:FlexPrintJob=new FlexPrintJob();
	printJob.start();
	printJob.addObject(dwgContainer);
	printJob.send();
}

/**
 * Creates an array in memory of a png or jpg image. This Image related functionality is an advanced implementation of the
 * print and print2 methods above.
 * @param	filetype:	Either png or png, defaults to png
 * @param	quality:	A value between 0 and 100, higher values = higher quality &
 * larger file size.  Only valid with jpg images
 * @param	doall:		A boolean indicating only print everything or only the first doc
 */
public function createImage(filetype:String="", quality:int=-1, doAll:Boolean=true):Array {
	return _imageCreator.createImage(filetype, quality, doAll);
}


/**
 * Zooms in into the room with the specified id and located in the given drawing
 *
 * @param	opts A JavaScript object used for the DwgOptions class
 */
public function findAsset(locIn:Object, opts:Object):void {
	if (locIn == null || opts == null)
		return;

	var location:Location=new Location(locIn);

	this._perDrawingConfig.set(opts);


	// Since finAsset is an exclusive operation, ensure that only
	// the specified drawing is loaded
	var currentDrawingName:String = location.getDrawingName();
	if(currentDrawingName == null || currentDrawingName == ''){
		currentDrawingName = createDwgName(this._perDrawingConfig);
	}
	var ad:ArchibusDrawing=removeAllDrawings(currentDrawingName);
	if (ad != null){
		ad.dwgLoadedForFind=false;
	}

	// Ensure that the drawing for the highlighted rooms is loaded
	if (this._perDrawingConfig.mode != FillTypes.UNSELECTED && getDrawingByLocation(location) == null) {
		ad=addDrawing(location, !this._perDrawingConfig.initialized ? opts : null);
		ad.doCompleteFindAsset=true;
	} else
		completeFindAsset(currentDrawingName);
}

/**
 * Returns an array that is a distinct value-color map as last applied in all loaded drawings.
 */
public function getAppliedValueColorList(dataSourceName:String=null):Array {
	var allVals:Array=[];
	var ad:ArchibusDrawing;
	var ads:Array=dwgContainer.getChildren();
	var usedValues:Array=[];
	for (var i:Number=0; i < ads.length; i++) {
		ad=ads[i] as ArchibusDrawing;
		var items:Array=ad.highlight.getUsedValueColorList(dataSourceName);
		if (items != null && items.length > 0) {
			for (var j:Number=0; j < items.length; j++) {
				var item:Object=items[j];
				if (usedValues.indexOf(item.value) < 0) {
					allVals.push(item);
					usedValues.push(item.value);
				}
			}
		}
	}
	return allVals;
}


/**
 * Retrieves the ArchibusDrawing object by its location.
 *
 * @param loc the specifying location
 * @return an ArchibusDrawing object.
 */
public function getDrawingByLocation(loc:Location):ArchibusDrawing {
	return getDrawingByName(loc.getDrawingName());
}

/**
 * Retrieves the ArchibusDrawing object by name.
 *
 * @param name the specifying name.
 *
 * @return an ArchibusDrawing object.
 */
public function getDrawingByName(name:String):ArchibusDrawing {
	var found:Boolean=false;
	var ads:Array=dwgContainer.getChildren();
	var ad:ArchibusDrawing=null;

	for (var i:Number=0; i < ads.length && !found; i++) {
		ad=ads[i] as ArchibusDrawing;
		if (ad.getName() == name)
			found=true;
	}

	if (!found)
		ad=null;

	return ad;
}

/**
 * Returns an array of bytes to be handled by the calling functionality
 * The createImage method must have been called prior to this method.
 * It is the javascript's responsibility to continually call this method
 * until it returns null.  This overly complex logic for supporting passing
 * the image array back to javascript is because in IE, trying to pass arrays
 * larger than 64k will cause this method to fail
 */
public function getImageBytes(start:int):Array {
	return _imageCreator.getImageBytes(start);
}

/**
 * Returns a list of selected ids to javascript
 */
public function getSelectedAssetIds():Array {
	var idList:Array=[];
	var ad:ArchibusDrawing;
	var ads:Array=dwgContainer.getChildren();
	for (var i:Number=0; i < ads.length; i++) {
		ad=ads[i] as ArchibusDrawing;
		idList=idList.concat(ad.getSelectedAssetIds());
	}

	return idList;
}

public function getMultipleSelectedAssets():Array {
	return this._multipleSelectedAssets;
}

public function getAssignOneToManyAssets():Array {
	return this._assignUtilities.assignedMapOneToMany;
}

public function getAssignedValue():String {
	if(this._assignUtilities && this._assignUtilities.assignedAsset && this._assignUtilities.assignedAsset.value != '')
		return this._assignUtilities.assignedAsset.value;
	else
		return '';
}


/**
 * Gets the selected asset's ids with the optional fields' information
 *
 * @param ad the Archibus Drawing of the selected asset
 * @param aob the asset object to be selected
 * @return an array of ids
 */
public function getSelectedIdsWithOptionalInfo(ad:ArchibusDrawing, aob:AssetObject):Array {
	var ids:Array=aob.properties.jsonData.ids;
	var idsWithOptionalInfo:Array=ids;
	if (this._controlConfig.runtimeConfig.optionalPKFields != null) {
		// append other data: kb
		var lo:LabelObject=ad.labelLayer.getLabel(ids);

		if (lo != null) {
			for (var i:int=0; i < this._controlConfig.runtimeConfig.optionalPKFields.length; i++) {
				var field:String=this._controlConfig.runtimeConfig.optionalPKFields[i];
				var tf:AbTextField=lo.getTextField(field);
				if (tf != null) {
					idsWithOptionalInfo[idsWithOptionalInfo.length]=tf.getText();
				} else {
					if (aob.properties != null && aob.properties.rec != null) {
						idsWithOptionalInfo[idsWithOptionalInfo.length]=aob.properties.rec[field];
					} else
						idsWithOptionalInfo[idsWithOptionalInfo.length]='';
				}
			}
		}
	}

	return idsWithOptionalInfo;
}



/**
 * Makes the asset selectable or unselectable.
 *
 * @param opts drawing options
 * @param selectable true if to make the asset selectable, false otherwise.
 */
public function makeAssetsSelectable(opts:Object, selectable:Boolean):void {
	var dwgOpts:DrawingConfig=new DrawingConfig();
	dwgOpts.set(opts);

	var location:Location=new Location();
	location.setFromArray(dwgOpts.dwgName);

	// Ensure that the drawing for the highlighted rooms is loaded
	var ad:ArchibusDrawing=getDrawingByLocation(location);
	if (ad != null)
		ad.makeAssetsSelectable(dwgOpts, selectable);
}

public function get perDrawingConfig():DrawingConfig {
	return _perDrawingConfig;
}



/**
 * Removes all drawings
 *
 * @param	name:	Drawing name to not remove (optional)
 */
public function removeAllDrawings(name:String):ArchibusDrawing {
	var ad:ArchibusDrawing;
	var currentDrawing:ArchibusDrawing=null;
	var ads:Array=dwgContainer.getChildren();
	for (var i:Number=0; i < ads.length; i++) {
		ad=ads[i] as ArchibusDrawing;
		if (ad.getName() != name)
			removeDrawing(ad.location);
		else
			currentDrawing=ad;
	}

	return currentDrawing;
}

/**
 * Removes the names drawing
 *
 * @param	locIn	A Location object
 */
public function removeDrawing(locIn:Object):void {
	for (var key:String in _assetPanels) {
		var assetPanel:AssetsPanel=_assetPanels[key] as AssetsPanel;
		assetPanel.close();
	}
	var loc:Location=new Location(locIn);
	var ad:ArchibusDrawing=getDrawingByLocation(loc);

	if (ad == null)
		return;
	// Remove references
	ad.removeEventListener(FlexEvent.CREATION_COMPLETE, drawingLoaded);
	dwgContainer.removeChild(ad);

	// Layout remaining drawings
	if (dwgContainer.getChildren().length > 0) {
		autoScale(_isometric);
	} else
		//if the last drawing is removed, then remove the navigation toolbar
		this.navCanvas.visible=false;
}

/**
 * Sets the ideal label text size for each drawing.
 *
 * @param textSize the text size to be set.
 */
public function setIdealLabelTextSize(textSize:uint):void {
	for each (var ad:Object in dwgContainer.getChildren()) {
		(ad as ArchibusDrawing).idealLabelTextSize=textSize;
	}
}


/**
 * Set the labels for the archibus drawing with the specifying location.
 *
 * @param locIn the location object.
 * @param opts per drawing configuration options
 * @param mode label's append mode.
 */
public function setLabels(locIn:Object, opts:Object, mode:int):void {
	if (locIn == null || opts == null)
		return;

	var loc:Location=new Location(locIn);
	var ad:ArchibusDrawing=getDrawingByLocation(loc);
	if (ad == null)
		return;

	ad.setLabels(opts, mode);
}

/**
 * Sets the minimum label text size for each drawing.
 *
 * @param textSize the text size to be set.
 */
public function setMinimumLabelTextSize(textSize:uint):void {
	for each (var ad:Object in dwgContainer.getChildren()) {
		(ad as ArchibusDrawing).minimumLabelTextSize=textSize;
	}
}

/**
 * Create right click menu and initialize it with the specifying array of items.
 *
 * @param contents an array of items.
 */
public function setRightClickMenu(contents:Array):void {
	if (contents != null) {
		var rightClickMenu:RightClickMenu=new RightClickMenu(this);
		rightClickMenu.initialize(contents);
	}
}

public function addContextMenuAction(title:String, actionID:String):void {
	var rightClickMenu:RightClickMenu=new RightClickMenu(this);
	rightClickMenu.addContextMenuAction(title, actionID);
}


/**
 * Sets the shrink label text to fit for each drawing.
 *
 * @param value true if set it to fit, false otherwise..
 */
public function setShrinkLabelTextToFit(value:Boolean):void {
	for each (var ad:Object in dwgContainer.getChildren()) {
		(ad as ArchibusDrawing).shrinkLabelTextToFit=value;
	}
}

/**
 * Sets the label's title text.
 *
 * @param location the location to which the archibus drawing belongs to.
 * @param textToSet the text to set.
 * @param fontConfig an object that represents a font in javascript(optional)
 */
public function setTitleText(location:Object, textToSet:String, fontConfig:Object=null):void {

	var loc:Location=new Location(location);
	var ad:ArchibusDrawing=getDrawingByLocation(loc);
	var tf:AbTextFormat=new AbTextFormat();
	if (fontConfig != null)
		tf.setFromFontConfig(null, fontConfig);
	else
		tf=null;

	if (ad != null)
		ad.setTitleText(textToSet, tf);
}

/**
 * Getter for the localization manager.
 */
public function get localizeManager():LocalizeUtilities {
	return this._localizedUtilities;
}

/**
 * Getter for the highlight type.
 */
public function get highlightType():HighlightType {
	return this._highlightType;
}


/**
 * Shows/hides or Enables/disables toolbar buttons.
 *
 * @param operation 'show' or 'enable'.
 * @param on  true or false
 * @param buttonName the toolbar button's name.
 */
public function toolbar(operation:String, on:Boolean, buttonName:String):void {
	if (operation == "show") {
		_toolbarUtilities.show(on, buttonName);
		if(this.redmarksEnabled)
			this.navCanvas.showRedlineButtons();
		else
			this.navCanvas.hideRedlineButtons();
	} else if (operation == "enable")
		_toolbarUtilities.enable(on, buttonName);
}

/**
 * Allows external functionality to execute toolbar commands
 */
public function toolbarCmd(buttonName:String):void {
	_toolbarUtilities.cmd(buttonName);
}


/**
 * Notify the javascript handlers that the app is now fully loaded
 */
private function appComplete():void {
	ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_APPLOADED));
}

/**
 * Completes the events such as highlight, find room, or redlining after the assets are loaded.
 * Calls the JavaScript flashAssetsLoaded event.
 */
private function assetsLoaded(event:Event):void {
	var ad:ArchibusDrawing=event.currentTarget as ArchibusDrawing;
	// if the doCompleteHighlights flag is set, complete that process at this time
	if (ad && ad.doCompleteHighlights)
		ad.completeHighlights();

	if (ad && ad.doCompleteFindAsset) {
		ad.dwgLoadedForFind=true;
		this.completeFindAsset();
	}

	//NEW for redlining
	if (doCompleteRedmarks) {
		this.completeRedmarks();
	}

	ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_ASSETSLOADED));
}

/**
 * Remove the loadComplete and loadFailed event handlers.
 *
 * @param ob the ArchibusDrawing object
 */
private function clearLoadHandlers(ad:ArchibusDrawing):void {
	clearProgressBar();
	ad.removeEventListener("loadComplete", processLoadComplete);
	ad.removeEventListener("loadFailed", processLoadFailed);
}

/**
 * Remove the progress bar from the application.
 */
private function clearProgressBar():void {
	if (_progressBar != null) {
		this.removeChild(_progressBar);
		_progressBar=null;
	}
}

/**
 * After the room is found, sets the room highlight. If the drawing is loaded, zoom into the highlighted room.
 */
private function completeFindAsset(drawingName:String = null):void {
	var ad:ArchibusDrawing =null;
	if(drawingName == null){
		 drawingName = createDwgName(this.perDrawingConfig);
	}
	
	ad = getDrawingByName(drawingName);
	
	if (ad == null)
		return;

	var mode:String=this.perDrawingConfig.mode;
	var af:FillFormat=_controlConfig.highlightConfig.getFillFormat((mode == null) ? FillTypes.SELECTED : mode);

	if (af == null)
		return;

	if (!mode.length)
		af.fillColor=this.perDrawingConfig.fill.fillColor;
	
	var roomId:String=this.perDrawingConfig.highlightId;
	ad.assetLayer.setAssetColor(roomId.split(_controlConfig.runtimeConfig.keyDelimiter), af, false);
	if (!ad.dwgLoadedForFind) {
		ad.dwgLoadedForFind=false;
		autoScale();
	}
	zoomOnFullLoad(ad );
}

/**
 * Completes the highlights and removes the fullyLoaded event handler.
 *
 * @param e
 */
private function completeHighlightsWhenFullyLoaded(e:Event):void {
	var ad:ArchibusDrawing=e.target as ArchibusDrawing;
	if (ad.fullyLoaded) {
		ad.removeEventListener("fullyLoaded", completeHighlightsWhenFullyLoaded);
		ad.completeHighlights();
	}
}

/**
 * Generate the drawing name used for loading the document.
 *
 * @param pks an array that has the following format [fieldName1, fieldValue1, fieldName2, fieldValue2, ....]
 * @param displayError true if show the error, false otherwise.
 *
 * @return the drawing name.
 */
private function createDwgName(drawingConfig:DrawingConfig, displayError:Boolean=true):String {
	if(drawingConfig && drawingConfig.rawDwgName && drawingConfig.rawDwgName != ''){
		return drawingConfig.rawDwgName;
	}
	var pks:Array = drawingConfig.dwgName;
	var name:String=new String();
	if (pks != null && pks.length) {
		for (var i:int=1; i < pks.length; i+=2) {
			name+=pks[i];
		}
		name=name.toLowerCase();
	} else if (displayError)
		Alert.show(LocalizeUtilities.wordNoDoc);

	return name;
}


/**
 * Callback method executed when the architectural background of a drawing gets loaded
 */
private function drawingLoaded(event:FlexEvent):void {
	var ad:ArchibusDrawing=event.target as ArchibusDrawing;
	if (ad.width != 0 && ad.height != 0) {
		if (!ad.loadComplete)
			return;

		ad.removeEventListener("updateSwf", drawingLoaded);

		ad.resize(ad.dwgDrawing.width * _scaleFactor, ad.dwgDrawing.height * _scaleFactor);

		handleIsometricChange(_isometric);

		zoomOnFullLoad(ad);

		if (_assignUtilities != null)
			_assignUtilities.restoreAssignedAssets(dwgContainer.getChildren());

		ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_DOCLOADED));

		// load assets
		this.loadMultipleAssets(ad);
	}
}

/**
 * Loads the asset json zlib files from the assetTypes parameter.
 *
 * @param ad - ArchibusDrawing
 */
private function loadMultipleAssets(ad:ArchibusDrawing):void {
	if(perDrawingConfig.assetTypes == "null"){
		return;
	}
	var assetTypes:Array = [];
	//trim each element of asset types then parsing them to an array
	if(perDrawingConfig.assetTypes != ""){
		 assetTypes=StringUtil.trimArrayElements(perDrawingConfig.assetTypes, ',').split(',');
	}else if(_perDrawingConfig.assetTypes != ""){
		assetTypes=StringUtil.trimArrayElements(_perDrawingConfig.assetTypes, ',').split(',');
	}
	
	//keep the asset types that alredy loaded to avoid the duplication
	var unqiueAssetTypes:Array=new Array();

	// large scale JSON should be load first to avoid the small scale asset hidden under them.
	if (assetTypes.indexOf("su") >= 0) {
		new AssetLoader(ad).load(_perDrawingConfig, 'su');
		unqiueAssetTypes.push('su');
	}
	if (assetTypes.indexOf("rm") >= 0) {
		new AssetLoader(ad).load(_perDrawingConfig, 'rm');
		unqiueAssetTypes.push('rm');
	}

	var assetType:String='';
	for (var i:int=0; i < assetTypes.length; i++) {
		if (assetTypes[i] != null && assetTypes[i].length > 0) {
			assetType=assetTypes[i];
		}
		if (assetType.length > 0 && unqiueAssetTypes.indexOf(assetType) < 0) {
			new AssetLoader(ad).load(_perDrawingConfig, assetType);
			unqiueAssetTypes.push(assetType);
		}
	}

	//if no assetType is specified, then use the "rm" as default asset type.
	if (unqiueAssetTypes.length == 0) {
		new AssetLoader(ad).load(_perDrawingConfig, 'rm');
	}
}

/**
 * Initializes common data as needed
 */
private function init():void {
	_controlConfig.init();
	_tooltip.init(tooltipCanvas, _controlConfig.getTooltipCfg());
	if (_assignUtilities == null)
		_assignUtilities=new AssignUtilities(this);

	// load up the logical highlight settings, only load if not already set
	if (!_highlightType)
		_highlightType=new HighlightType();

	//this.addEventListener(DragEvent.DRAG_ENTER,dragEnterHandler);
	//this.addEventListener(DragEvent.DRAG_DROP, dragDropHandler);	
}

private function dragEnterHandler(dragEvent:DragEvent):void {
	dragEvent.preventDefault();
	if (dragEvent.dragInitiator is src.view.text.AbTextField) {
		var dropTarget:UIComponent=dragEvent.currentTarget as UIComponent;
		DragManager.showFeedback(DragManager.MOVE);
		DragManager.acceptDragDrop(dropTarget);
	}
}

private function dragDropHandler(dragEvent:DragEvent):void {
	dragEvent.preventDefault();
	if (dragEvent.dragInitiator is AbTextField) {
		var abTextField:AbTextField=dragEvent.dragInitiator as AbTextField;
		abTextField.handleDropEvent(dragEvent.dragSource.dataForFormat("items"), [], dragEvent.dragSource.dataForFormat("table").toString(), null, false);

	}
}

/**
 * Initializes this application
 */
private function initApp():void {
	// Initialize the toolbar handler
	this._toolbarUtilities.setOwner(this);

	this.addChild(_imageCreator);

	// Registers callback methods for external usage
	addCallBackFunctions();

	this._toolbarUtilities.init();

	this._toolbarUtilities.localize(this._localizedUtilities.map);

	this.setLoaderContext();
}

private function setLoaderContext():void {
	this._loaderContext=new LoaderContext()

	/* Specify the current application's security domain. */
	this._loaderContext.securityDomain=SecurityDomain.currentDomain;

	/* Specify a new ApplicationDomain, which loads the sub-app into a peer ApplicationDomain. */
	this._loaderContext.applicationDomain=new ApplicationDomain();
}

public function get loaderContext():LoaderContext {
	return _loaderContext;
}

/**
 * Adds the callback functions.
 */
private function addCallBackFunctions():void {
	ExternalInterface.addCallback("addDrawing", addDrawing);

	ExternalInterface.addCallback("applyAssets", applyAssets);
	
	ExternalInterface.addCallback("removeDrawing", removeDrawing);
	ExternalInterface.addCallback("removeAllDrawings", removeAllDrawings);

	ExternalInterface.addCallback("setIsomtric", setIsometric);

	ExternalInterface.addCallback("highlightAssets", highlightAssets);


	ExternalInterface.addCallback("findAsset", findAsset);

	ExternalInterface.addCallback("applyDS", applyDS);

	ExternalInterface.addCallback("enableAssetPanel", enableAssetPanel);
	ExternalInterface.addCallback("showAssetPanel", showAssetPanel);
	ExternalInterface.addCallback("refreshAssetPanel", refreshAssetPanel);
	ExternalInterface.addCallback("closeAssetPanel", closeAssetPanel);

	ExternalInterface.addCallback("selectAssets", selectAssets);
	ExternalInterface.addCallback("unselectAssets", unselectAssets);


	ExternalInterface.addCallback("addAssetPanelRowAction", addAssetPanelRowAction);

	ExternalInterface.addCallback("getAppliedValueColorList", getAppliedValueColorList);
	ExternalInterface.addCallback("print", print);
	ExternalInterface.addCallback("print2", print2);

	ExternalInterface.addCallback("setSelectColor", setSelectColor);
	ExternalInterface.addCallback("getSelectedAssetIds", getSelectedAssetIds);

	ExternalInterface.addCallback("clearAssignCache", clearAssignCache);
	ExternalInterface.addCallback("setToAssign", setToAssign);
	ExternalInterface.addCallback("unassign", unassign);

	ExternalInterface.addCallback("setLabels", setLabels);
	ExternalInterface.addCallback("applyLabels", applyLabels);

	ExternalInterface.addCallback("addLabels", addLabels);
	ExternalInterface.addCallback("removeLabels", removeLabels);

	ExternalInterface.addCallback("getAssignedValue", getAssignedValue);
	ExternalInterface.addCallback("getMultipleSelectedAssets", getMultipleSelectedAssets);
	ExternalInterface.addCallback("getAssignOneToManyAssets", getAssignOneToManyAssets);

	//ExternalInterface.addCallback("addHighlights", addHighlights);


	ExternalInterface.addCallback("makeAssetsSelectable", makeAssetsSelectable);

	ExternalInterface.addCallback("toolbar", toolbar);
	ExternalInterface.addCallback("toolbarCmd", toolbarCmd);

	ExternalInterface.addCallback("setTitleText", setTitleText);
	ExternalInterface.addCallback("clearPersistFills", clearPersistFills);

	ExternalInterface.addCallback("setMinimumLabelTextSize", setMinimumLabelTextSize);
	ExternalInterface.addCallback("setIdealLabelTextSize", setIdealLabelTextSize);
	ExternalInterface.addCallback("setShrinkLabelTextToFit", setShrinkLabelTextToFit);

	ExternalInterface.addCallback("setRightClickMenu", setRightClickMenu);

	ExternalInterface.addCallback("addContextMenuAction", addContextMenuAction);

	//NEW for redlining
	ExternalInterface.addCallback("getRedmarks", getRedmarks);
	ExternalInterface.addCallback("drawRedmarks", drawRedmarks);
	ExternalInterface.addCallback("enableRedmarks", enableRedmarks);
	ExternalInterface.addCallback("disableRedmarks", disableRedmarks);

	ExternalInterface.addCallback("handleWheel", handleWheel);
	
	
	
	////PDF report
	ExternalInterface.addCallback("getDrawingPosition", getDrawingZoomInfo);
}

/**
 * Checks if the drawing is already loaded into the application and return the drawing if the drawing already loaded and return null otherwise.
 */
private function isDrawingLoaded(ads:Array, drawingName:String):ArchibusDrawing {
	for (var i:Number=0; i < ads.length; i++) {
		var ad:ArchibusDrawing=ads[i] as ArchibusDrawing;
		if (ad.getName() == drawingName)
			return ad;
	}

	return null;
}

/**
 * Clears the load event handlers and set the 'loadComplete' flag to true.
 * @param event the Flex Event
 */
private function processLoadComplete(event:FlexEvent):void {
	var ad:ArchibusDrawing=event.target as ArchibusDrawing;
	ad.loadComplete=true;

	clearLoadHandlers(ad);
}

/**
 * Clears the load event handlers and set the 'loadComplete' flag to false.
 * Remove the drawing from the container and prompt the error messge to users.
 * Calls the related JavaScript functions if any.
 *
 * @param event the IO error event.
 */
private function processLoadFailed(event:IOErrorEvent):void {
	var ad:ArchibusDrawing=event.target as ArchibusDrawing;
	ad.loadComplete=false;
	clearLoadHandlers(ad);

	removeDrawing(ad.location);

	Alert.show(LocalizeUtilities.wordUnableToLoad + ": " + _backgroundFile);

	var ob:Object={};
	ob.pks=ad.location.getPrimarykeysArray();
	ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_LOADFAILED), ob);
}

/**
 * set the last mouse event ids and Shows the tooltip. Calls the JavaScript function to show the ids information on the panel display.
 *
 * @param event the mouse event.
 */
private function tooltipEventHandler(event:MouseEvent):void {
	//retrieve the ids so that last mouse event ids can be set.
	var ids:Array=CommonUtilities.getIdsFromEvent(event);
	this._lastMouseEventIds=ids;

	if (_tooltipEnabled != true)
		return;

	if (ids != null) {
		this.systemManager.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler, true);
		_tooltip.show(event, this.mouseX, this.mouseY);
	}
	ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_SETLITEDISPLAY), ids);
}

/**
 * Getter for the drawing control configuration object.
 *
 * @return the ControlConfig object.
 */
public function get controlConfig():ControlConfig {
	return this._controlConfig;
}

/**
 * Getter for the localized manager.
 */
public function get localizedManager():LocalizeUtilities {
	return this._localizedUtilities;
}

/**
 * Getter for the last mouse click's event ids.
 *
 * @return last mouse click's event ids.
 */
public function get lastMouseEventIds():Array {
	return this._lastMouseEventIds;
}

/**
 * Setter for the last mouse click's event ids.
 *
 * @param lastIds the ids to be set.
 */
public function set lastMouseEventIds(lastIds:Array):void {
	this._lastMouseEventIds=lastIds;
}

public function handleWheel(event:Object):void {
	//trace("handlewheel");
	var mEvent:MouseEvent=new MouseEvent(MouseEvent.MOUSE_WHEEL, true, false, event.x, event.y, this, event.ctrlKey, event.altKey, event.shiftKey, false, Number(event.delta));
	this.dispatchEvent(mEvent);
}


