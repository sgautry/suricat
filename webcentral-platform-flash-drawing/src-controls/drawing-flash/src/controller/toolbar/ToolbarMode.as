package src.controller.toolbar {

	/**
	 * Enumeration for the toolbar buttons.
	 */
	public class ToolbarMode {
		public static const SELECT:String="Select";
		public static const PAN:String="Pan";
		public static const ZOOM_WINDOW:String="Zoom Window";
		public static const DRAW:String="Draw";
		public static const MULTI_SELECT:String="Multiple Rooms Selection";
	}
}


