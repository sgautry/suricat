/**
 * Holds the drawing controls's public event handler functions for toolbar buttons, such as ISO(Isometric), Zoom In, Zoom Out etc.
 *
 */
import flash.events.*;
import flash.external.ExternalInterface;
import flash.geom.*;

import src.controller.toolbar.ToolbarMode;
import src.model.util.*;
import src.view.drawing.ArchibusDrawing;


/**
 * Property: true if isometric transformation is applied, false otherwise.
 */
public var _isometric:Boolean=false;

/**
 * Property: the zoom factor to go back to the previous zoom scale. Used with the letter "p" key event.
 */
public var rollbackFactors:Array=new Array();

/**
 * Handles ISO change when user clicks on the ISO toolbar button.
 */
public function toggleIso():void {
	_isometric=!_isometric;
	handleIsometricChange(_isometric);
}

/**
 * Sets the isometict toggle from javascription and optionally
 * Applies/removes the isometric transformation to the drawings.
 *
 * @param	isometricTransform	True if isometric transform should be applied,
 * false otherwise
 * @param   apply				Apply the change
 */
public function setIsometric(isometricTransform:Boolean, apply:Boolean=false):void {
	_isometric=isometricTransform;
	if (apply)
		handleIsometricChange(!isometricTransform);
}

/**
 * Resets the assets when user click on the 'Reset Assets' toolbar button.
 */
public function resetAssets():void {
	var archibusDrawings:Array=this.dwgContainer.getChildren();
	for (var j:Number=0; j < archibusDrawings.length; j++) {
		var archibusDrawing:ArchibusDrawing=archibusDrawings[j] as ArchibusDrawing;
		archibusDrawing.resetAssets();
	}

	ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_ONRESERASSETS));
}

/**
 * Applies/removes the isometric transformation to the drawings.
 *
 * @param	isometricTransform	True if isometric transform should be applied,
 * false otherwise
 */
public function handleIsometricChange(isometricTransform:Boolean):void {
	_isometric=isometricTransform;
	var archibusDrawing:ArchibusDrawing;
	var archibusDrawings:Array=dwgContainer.getChildren();
	if (isometricTransform) {
		var isometricMatrix:Matrix=new Matrix(Math.cos(0.46365), Math.sin(0.46365), -Math.cos(0.46365), Math.sin(0.46365), 0, 0);
		for (var i:Number=0; i < archibusDrawings.length; i++) {
			archibusDrawing=archibusDrawings[i] as ArchibusDrawing;
			archibusDrawing.transform.matrix=isometricMatrix;
		}
	} else {
		var ortho:Matrix=new Matrix();
		for (i=0; i < archibusDrawings.length; i++) {
			archibusDrawing=archibusDrawings[i] as ArchibusDrawing;
			archibusDrawing.transform.matrix=ortho;
		}
	}
	autoScale(isometricTransform);
}

/**
 * Zooms in to the given zoom center
 */
public function zoomIn(zoomCenter:Point, factor:Number=1.25, isRollback:Boolean=false):void {
	if (zoomCenter == null)
		zoomCenter=new Point(this.dwgContainer.width / 2, this.dwgContainer.height / 2);

	var archibusDrawings:Array=this.dwgContainer.getChildren();
	if (archibusDrawings.length < 1)
		return;

	// zoom in - factor needs to be greater than 1
	if (factor < 1)
		factor=1;

	// make sure the zoom does not go beyond allowed extends
	if (this.zoomFactor * factor > CommonUtilities.ZOOMFACTOR_MAX) {
		factor=CommonUtilities.ZOOMFACTOR_MAX/this.zoomFactor;
	} 
	this.navCanvas.zoomSlider.value=this.zoomFactor=this.zoomFactor * factor;
	_scaleFactor=_scaleFactor * factor;

	trace("zoomIn _scaleFactor=" + _scaleFactor + " factor=" + factor + " zoomFactor=" + this.zoomFactor);

	//add the rollback factor if user clicked "p" to go back to previous zoom
	if (!isRollback && factor != 1)
		rollbackFactors.push(1 / factor);

	var ad:ArchibusDrawing;
	var ad0:ArchibusDrawing=null;
	var positions:Object=_layoutUtilities.calculatePositions(archibusDrawings);
	for (var i:Number=0; i < archibusDrawings.length; i++) {
		ad=archibusDrawings[i] as ArchibusDrawing;
		ad.resize(ad.width * factor, ad.height * factor);

		var ar:Array=positions[ad.location.getDrawingName()];
		if (ar[0] == 0 && ar[1] == 0)
			ad0=ad;
	}

	var scaledCenter:Point=new Point((zoomCenter.x - ad0.x) * factor + ad0.x, (zoomCenter.y - ad0.y) * factor + ad0.y);
	var xOffset:Number=ad0.x - scaledCenter.x + this.dwgContainer.width / 2;
	var yOffset:Number=ad0.y - scaledCenter.y + this.dwgContainer.height / 2;
	autoPosition(xOffset, yOffset);

	this.currentState=ToolbarMode.SELECT;
}

/**
 * Zoom out from the given zoom center.
 */
public function zoomOut(zoomCenter:Point, factor:Number=0.8, isRollback:Boolean=false):void {
	if (zoomCenter == null)
		zoomCenter=new Point(dwgContainer.width / 2, dwgContainer.height / 2);

	var archibusDrawings:Array=dwgContainer.getChildren();
	if (archibusDrawings.length < 1)
		return;

	//zoom out - factor needs to be within (0, 1) range
	if (factor > 1 || factor < 0)
		factor=1;

	// make sure the zoom does not go beyond allowed extends
	if (this.zoomFactor * factor < CommonUtilities.ZOOMFACTOR_MIN) {
		factor=CommonUtilities.ZOOMFACTOR_MIN / this.zoomFactor;
	}
	_scaleFactor=_scaleFactor * factor;

	trace("zoomOut _scaleFactor=" + _scaleFactor + " factor=" + factor + " zoomFactor=" + this.zoomFactor);

	// set zoom slider value
	this.navCanvas.zoomSlider.value=this.zoomFactor=this.zoomFactor * factor;

	//push the rollback factor if user clicked "p" to go back to previous zoom
	if (!isRollback && factor != 1)
		rollbackFactors.push(1 / factor);

	var ad:ArchibusDrawing;
	var ad0:ArchibusDrawing=null;
	var positions:Object=_layoutUtilities.calculatePositions(archibusDrawings);
	for (var i:Number=0; i < archibusDrawings.length; i++) {
		ad=archibusDrawings[i] as ArchibusDrawing;
		ad.resize(ad.width * factor, ad.height * factor);

		var ar:Array=positions[ad.location.getDrawingName()];
		if (ar[0] == 0 && ar[1] == 0)
			ad0=ad;
	}

	var scaledCenter:Point=new Point((zoomCenter.x - ad0.x) * factor + ad0.x, (zoomCenter.y - ad0.y) * factor + ad0.y);
	var xOffset:Number=ad0.x - scaledCenter.x + dwgContainer.width / 2;
	var yOffset:Number=ad0.y - scaledCenter.y + dwgContainer.height / 2;
	autoPosition(xOffset, yOffset);

	this.currentState=ToolbarMode.SELECT;
}

