/**
 * Holds the drawing controls's private event handler functions for the mouse click events.
 */
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.external.ExternalInterface;
import flash.geom.*;
import flash.utils.Timer;

import mx.utils.ObjectUtil;

import src.controller.toolbar.ToolbarMode;
import src.model.assign.*;
import src.model.config.DrawingConfig;
import src.model.redline.DrawMode;
import src.model.util.*;
import src.view.asset.AssetObject;
import src.view.drawing.ArchibusDrawing;
import src.view.highlight.*;
import src.view.text.AbTextField;
import src.view.util.CollisionDetection;

//time is used to recogize if the mouse click is single or double
private var m_timer:Timer;

//mouse click event
private var m_event:MouseEvent;

private var _mouseDownClicked:Boolean=true;

//if the mouse click a text field - used to set the flag for drag-drop event.
private var _textFieldClicked:Boolean= false;

private function onMouseClick(e:MouseEvent):void {
	m_event=e;

	trace("onMouseClick");

	m_timer=new Timer(250, 1);
	m_timer.addEventListener(TimerEvent.TIMER_COMPLETE, OnTimerComplete);
	m_timer.start();
}

private function onMouseDoubleClick(e:MouseEvent):void {
	if (m_timer != null) {
		trace("onMouseDoubleClick");
		m_timer.stop();
		m_timer.removeEventListener(TimerEvent.TIMER_COMPLETE, OnTimerComplete);
		m_timer=null;
		// dispatch a double click event for this button or call the double click handler function
		if (e.shiftKey)
			zoomOut(null);
		else
			zoomIn(null);
	}
}

private function OnTimerComplete(e:TimerEvent):void {
	// dispatch a click event for this button or call the click handler function
	this.mouseClickHandler(m_event);
}

/**
 * Event handler for mouse click event based on the assign mode defined in the current drawing's configuration.
 *
 * @param event the mouse click event
 */
public function mouseClickHandler(event:MouseEvent):void {
	trace("mouseClickHandler");
	//XXX: only allow users to click on AssetObject or AbTextField
	//TODO: those event lsisteners should be AssetObject and AbTextField
	if (!(event.target is AssetObject || event.target is AbTextField)) {
		return;
	}
	// retrieves the primary keys array from the mouse event and record it.
	var ids:Array=CommonUtilities.getIdsFromEvent(event);
	this.lastMouseEventIds=ids;

	// get the current drawing object and retrieve the asset object from the primary keys array.
	var ad:ArchibusDrawing=CommonUtilities.getDwgFromEvent(event);
	if (ad == null || ids == null || (this.currentState != ToolbarMode.SELECT && this.currentState != ToolbarMode.MULTI_SELECT)  || _panned){
		_panned = false;		
		return;
	}

	var assetOb:AssetObject=ad.getAssetOb(ids);

	// is the drawing in 'selection' mode and user indeed clicked on either the drawing area or an asset obejct?
	if (!_assignUtilities.isValidClick(assetOb))
		return;

	this.handleSelectOrAssignment(ad, ids, assetOb, true);

	// Pass this info back to any javascript handler for the onclick event
	var idsToUse:Array=this.getSelectedIdsWithOptionalInfo(ad, assetOb);
	ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_ONCLICK), idsToUse, assetOb.properties.isSelected, this.controlConfig.highlightConfig.assignSelectedColorOverride.toString(16), assetOb.getAssetType());
}

private function handleSelectOrAssignment(ad:ArchibusDrawing, ids:Array, assetOb:AssetObject, isclick:Boolean):Boolean{
	switch (ad.config.assignMode) {
		case AssignMode.ONE_TO_ONE:
			// handle one-on-one assign
			if (this._assignUtilities.assignedAsset && _assignUtilities.assignedAsset.checkValid())
				// can this asset be assigned?
				if(!_assignUtilities.alreadyAssigned(assetOb))
					return assignHandlerOneOnOne(ad, ids, isclick);
				else 
					this.doSingleOrMultipleSelect(ad, ids, assetOb, isclick);

			break;

		case AssignMode.ONE_TO_MANY:
			// handle one-to-many assign
			if(_assignUtilities.assignedAsset && _assignUtilities.assignedAsset.checkValid())
				return assignHandlerOneToMany(ad, ids, isclick);
			else
				this.doSingleOrMultipleSelect(ad, ids, assetOb, isclick);
			break;

		case AssignMode.MANY_TO_ONE:
			// handle one-to-many assign
			if (this._assignUtilities.assignedAsset && _assignUtilities.assignedAsset.checkValid())
				return assignHandlerManyToOne(ad, ids, isclick);
			else 
				this.doSingleOrMultipleSelect(ad, ids, assetOb, isclick);
			break;

		case AssignMode.MANY_TO_MANY:
			// Not yet implemented
			break;

		case AssignMode.NONE:
			this.doSingleOrMultipleSelect(ad, ids, assetOb, isclick);
			break;

		default:
			break;
	}

	//for select, alsways return true.
	return true;

}

private function doSingleOrMultipleSelect(ad:ArchibusDrawing, ids:Array, assetOb:AssetObject, isclick:Boolean):void{
	if(ad.config.selectionMode == SelectionMode.ALL){
		if(this.currentState == ToolbarMode.MULTI_SELECT && this.perDrawingConfig.multipleSelectionEnabled){
			this.multipleSelectHandler(ad, ids, isclick);
		} else if((this.currentState == ToolbarMode.SELECT || this.currentState == ToolbarMode.MULTI_SELECT) && isclick){
			//toggle the asset object's selection
			this.doDefaultSelectionOrAssignment(ad, ids, assetOb, ad.config.selectionMode);
		}
	}
}

private function doDefaultSelectionOrAssignment(ad:ArchibusDrawing, ids:Array, assetOb:AssetObject, mode:int):void {
	if ((mode != SelectionMode.NONE) && assetOb.properties != null && assetOb.properties.isSelectable) {
		//XXX: exclusive or single selection
		if(ad.config.exclusive ||  !ad.config.multipleSelectionEnabled){
			ad.assetLayer.unselectPrevious(ids);
		}

		ad.toggleAssetSelection(ids, mode, _assignUtilities.assignedAsset);
	}
}

/**
 * Handles one to one assign.
 *
 * @param ad - archibus drawing for the assign.
 * @param ids - the object's ids to be assigned.
 *
 * @return Boolean - return true if assign is successful, false otherwise
 */
private function assignHandlerOneOnOne(ad:ArchibusDrawing, ids:Array, isClick:Boolean):Boolean {
	var assetOb:AssetObject=ad.getAssetOb(ids);
	var preAssignedAsset:AssetObject=_assignUtilities.assignedMapOneToOne[_assignUtilities.assignedAsset.value];
	if (preAssignedAsset != null && preAssignedAsset.properties.jsonData.ids != assetOb.properties.jsonData.ids)
		preAssignedAsset.unselect();

	var assignSucessful:Boolean = false;

	//set assign color
	var af:FillFormat=setAssignColor(ad, assetOb, isClick);

	if(af!=null){
		var selectedAsset:Object = {};
		selectedAsset.pks = ids;
		selectedAsset.selected = assetOb.properties.isSelected;
		selectedAsset.color = af.fillColor;

		// update the assigned array
		if(this.perDrawingConfig.assignMode == AssignMode.ONE_TO_ONE){
			// update the assigned on one one map
			_assignUtilities.updateAssignOneOnOneMaps(assetOb);

			// set the retained assigned array
			_assignUtilities.setRetainedArrayOneOnOne(assetOb);

			assignSucessful = true;
		} 

		//select only
		this._multipleSelectedAssets[this._multipleSelectedAssets.length]=selectedAsset;
	}

	return assignSucessful;
}

public function multipleSelectHandler(ad:ArchibusDrawing, ids:Array, isClick:Boolean):void{
	var assetOb:AssetObject=ad.getAssetOb(ids);

	// set select color
	var af:FillFormat=this.controlConfig.highlightConfig.getFillFormat(FillTypes.SELECTED);
	setSelectColor(af.fillColor);

	//reset the array.
	if(isClick)
		this._multipleSelectedAssets.length = 0;

	assetOb.clear();

	//make sure multiple select will always select
	if(!isClick)
		assetOb.properties.isSelected=false;			

	this.doDefaultSelectionOrAssignment(ad, ids, assetOb, ad.config.selectionMode);

	//select only
	var selectedAsset:Object = {};
	selectedAsset.pks = ids;
	selectedAsset.selected = assetOb.properties.isSelected;
	selectedAsset.color = af.fillColor;
	this._multipleSelectedAssets[this._multipleSelectedAssets.length]=selectedAsset;
}

/**
 * assign one object to many assets on the drawing.
 *
 * @param ad the drawing control.
 * @param ids the ids of the asset object.
 *
 * @return Boolean - return true if assign is successful, false otherwise
 */
public function assignHandlerOneToMany(ad:ArchibusDrawing, ids:Array, isClick:Boolean):Boolean {
	var assetOb:AssetObject=ad.getAssetOb(ids);

	//set assign color
	var af:FillFormat=setAssignColor(ad, assetOb, isClick);

	var assignSucessful:Boolean = false;

	//store the asset object if the assign is successful
	if(af!=null){
		var selectedAsset:Object = {};
		selectedAsset.pks = ids;
		selectedAsset.selected = assetOb.properties.isSelected;
		selectedAsset.color = af.fillColor;

		// update the assigned array
		if(this.perDrawingConfig.assignMode == AssignMode.ONE_TO_MANY){
			this._assignUtilities.assignedMapOneToMany[this._assignUtilities.assignedMapOneToMany.length] = selectedAsset;
			this._assignUtilities.retainedAssignOneToMany[assetOb.properties.jsonData.ids]=assetOb;
			assignSucessful = true;
		} 

		//select only
		this._multipleSelectedAssets[this._multipleSelectedAssets.length]=selectedAsset;
	}

	return assignSucessful;
}

private function setAssignColor(ad:ArchibusDrawing, assetOb:AssetObject, isClick:Boolean):FillFormat{

	// set select color
	var af:FillFormat=this.controlConfig.highlightConfig.getFillFormat(FillTypes.SELECTED);

	//if the asset is already selected and user tries to click it when selecting, we use 'unselect' fillformat.
	if(assetOb.properties.isSelectable && assetOb.properties.isSelected && isClick && (ad.config.assignMode == AssignMode.NONE || _assignUtilities.assignedAsset == null))
		af=this.controlConfig.highlightConfig.getFillFormat(FillTypes.UNSELECTED);


	if(_assignUtilities.assignedAsset && _assignUtilities.assignedAsset.checkValid())
		setSelectColor(parseInt(_assignUtilities.assignedAsset.getColor(), 10));
	else
		setSelectColor(af.fillColor);

	var color:uint=this.controlConfig.highlightConfig.assignSelectedColorOverride;
	af.fillColor=color;

	var assignedColor:uint=assetOb.previousFillColor;
	var currentColor:uint=assetOb.fillFormat.fillColor;
	if (currentColor == color) {
		if (currentColor != assignedColor) {
			//only toggle the color when it is a click event. for multiple selection, we will keep the color as it is.
			if(isClick)
				ad.toggleAssetSelection(assetOb.properties.jsonData.ids, ad.config.selectionMode, this._assignUtilities.assignedAsset);
		} else {
			/*
			*	At this point both the color associated with the requested value
			*	change is the same as the already set color for the asset.
			*	In thise case, attempt to compare the value associated with the
			*	record with the currentAssignValue.  If they are different, then
			*	the requested change is valid and handle it as such.
			*/
			var recVal:String=ad.highlight.getHighlightValueByIds(assetOb.ids);
			if (_assignUtilities.assignedAsset && _assignUtilities.assignedAsset.checkValid() && _assignUtilities.assignedAsset.value != recVal){
				if(!isClick)
					assetOb.properties.isSelected = false;
				ad.toggleAssetSelection(assetOb.properties.jsonData.ids, ad.config.selectionMode, this._assignUtilities.assignedAsset);				
			}
			else {
				// the asset has already been assigned to the same value.
				af=null;
			}
		}
	} else if (assignedColor != color){
		//only toggle the color when it is a click event. for multiple selection, we will keep the color as it is.
		if(!isClick)
			assetOb.properties.isSelected = false;
		ad.toggleAssetSelection(assetOb.properties.jsonData.ids, ad.config.selectionMode, this._assignUtilities.assignedAsset);				
	}

	return af;
}

/**
 * assign many object to one asset.
 *
 * @param ad the archibus drawing.
 * @param ids the asset's id array.
 *
 * @return Boolean - return true if assign is successful, false otherwise
 */
public function assignHandlerManyToOne(ad:ArchibusDrawing, ids:Array, isClick:Boolean):Boolean {
	// The room name, instead of the selected item determines the color
	var assetId:String=_assignUtilities.idsToString(ids);

	// ?? YQ - what the dc.full_id for?
	setSelectColor(parseInt(CommonUtilities.getColorFromValue("dc.full_id", assetId, assetId), 10));

	// Handle possibility that some other room needs to be set to unassigned
	this.unassign(_assignUtilities.assignedAsset.field, _assignUtilities.assignedAsset.value);

	var assignSucessful:Boolean = false;

	// Now handle the currently selected room
	var assetOb:AssetObject=ad.getAssetOb(ids);
	var af:FillFormat=setAssignColor(ad, assetOb, isClick);

	//store the asset object if the assign is successful
	if(af!=null){
		var selectedAsset:Object = {};
		selectedAsset.pks = ids;
		selectedAsset.selected = assetOb.properties.isSelected;
		selectedAsset.color = af.fillColor;

		// update the assigned array
		if(this.perDrawingConfig.assignMode == AssignMode.MANY_TO_ONE){
			_assignUtilities.assignedMapManyToOne[_assignUtilities.assignedAsset.value]=assetOb;
			_assignUtilities.retainedAssginedManyToOne[_assignUtilities.retainedAssginedManyToOne.length] = selectedAsset;
			var tot:int=_assignUtilities.retainedAssignManyToOne[assetId] as int;
			_assignUtilities.retainedAssignManyToOne[assetId]=tot + 1;
			assignSucessful = true;
		} 

		//select only
		this._multipleSelectedAssets[this._multipleSelectedAssets.length]=selectedAsset;
	}

	return assignSucessful;
}

/**
 * Event handler for mouse down
 */
private function mouseDownHandler(event:MouseEvent):void {

	trace("mouseDownHandler");

	_tooltip.hide();

	var ad0:ArchibusDrawing=this.getFirstDrawing();
	if (!ad0)
		return;

	//reset variables
	_mouseDownClicked=true;
	_panned=false;
	_multipleSelectedAssets.length=0;

	if (event.target is AbTextField)
		_textFieldClicked=true;

	this.dwgContainer.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler, true);
	this.dwgContainer.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler, true);

	if (dwgContainer.getChildren().length > 0) {
		// if user clicks SHIFT+Mouse_Down, will be zoom window event.
		if (event.shiftKey)
			this.currentState=ToolbarMode.ZOOM_WINDOW;
		else if (event.ctrlKey && this.perDrawingConfig.multipleSelectionEnabled)
			this.currentState=ToolbarMode.MULTI_SELECT;

		if (this.currentState == ToolbarMode.SELECT) {
			// record mouse start point
			_panStartPoint=new Point(this.mouseX, this.mouseY);
			_tooltipEnabled=false;
		} else if (this.currentState == ToolbarMode.ZOOM_WINDOW || this.currentState == ToolbarMode.MULTI_SELECT) {
			this.systemManager.removeEventListener(MouseEvent.MOUSE_OUT, mouseOutHandler, true);
			this.systemManager.removeEventListener(MouseEvent.MOUSE_OVER, mouseOverHandler, true);

			// Start window zooming oe multiple selecting
			setStartSelecting();
			_tooltipEnabled=false;
		} else {
			_tooltipEnabled=true;
		}

		if(this.currentState == ToolbarMode.SELECT || this.currentState == ToolbarMode.MULTI_SELECT || (this.currentState == ToolbarMode.DRAW && ad0.drawingMode != DrawMode.drawModeText)){
			trace("set focus");
			ad0.stage.focus=ad0;
		}

	}
}

public function getFirstDrawing():ArchibusDrawing {

	var ads:Array=dwgContainer.getChildren();
	if (ads == null || ads.length == 0)
		return null;
	else
		return (ads[0] as ArchibusDrawing);

}

/**
 * Event handler for the mouse move
 */
private function mouseMoveHandler(event:MouseEvent):void {

	if (_textFieldClicked) {
		//drag/drop event - remove the move handler
		this.systemManager.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler, true);
		return;
	}

	//add the event back on since when selecting window or zooming, the mouse might move over the text field 
	//but it is not a drag/drop event
	this.systemManager.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler, true);
	this.systemManager.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler, true);

	//trace("mouseMoveHandler");
	//KB3038285
	//_tooltipEnabled=false;

	if ((this.currentState == ToolbarMode.ZOOM_WINDOW && _zooming)
		|| (this.currentState == ToolbarMode.MULTI_SELECT && _selecting)){
		// Update zoom window
		updateSelectWindowArea();
	} else if (this._panStartPoint != null) {
		ensureZoomWindowHidden();
		var panCurrentPoint:Point=new Point(this.mouseX, this.mouseY);
		var distanceX:Number=panCurrentPoint.x - _panStartPoint.x;
		var distanceY:Number=panCurrentPoint.y - _panStartPoint.y;
		if (this.currentState == ToolbarMode.SELECT) {
			if (_mouseDownClicked && (Math.abs(distanceX) >= 20 || Math.abs(distanceY) >= 20)) {
				this.currentState=ToolbarMode.PAN;
			} else {
				_tooltipEnabled=true;
				_panned=false;
			}
		}
		if (this.currentState == ToolbarMode.PAN) {
			// only pan for the move > 20 pixels to avoid the timing issue.
			trace("Pan to (" + panCurrentPoint.x + ", " + panCurrentPoint.y + ") from (" + _panStartPoint.x + ", " + _panStartPoint.y + ")");
			this.pan(distanceX, distanceY);
			_panStartPoint=panCurrentPoint;
			_panned=true;
		}
	}

	if (_tooltipEnabled) {
		_tooltip.setPos(this.mouseX, this.mouseY);
	}
}

public function pan(panXOffset:Number, panYOffset:Number):void {

	var ads:Array=dwgContainer.getChildren();

	for (var index:int=0; index < ads.length; index++) {
		var ad:ArchibusDrawing=ads[index] as ArchibusDrawing;
		ad.move(ad.x + panXOffset, ad.y + panYOffset);
	}
}


/**
 * Event handler for mouse out
 */
private function mouseOutHandler(event:MouseEvent):void {
	trace("mouseOutHandler");

	if (_tooltipEnabled == true) {
		_tooltip.hide();
		this.systemManager.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler, true);
		this.systemManager.removeEventListener(MouseEvent.MOUSE_OUT, mouseOutHandler, true);

		var vals:Array=[];
		vals[0]='';
		ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_SETLITEDISPLAY), vals);
	}
}

/**
 * Event handler for mouse over
 */
private function mouseOverHandler(event:MouseEvent):void {
	trace("mouseOverHandler");
	tooltipEventHandler(event);
}

public function reset():void{
	this._textFieldClicked=false;
	this.currentState=ToolbarMode.SELECT;
	this._panStartPoint = null;
}

/**
 * Event handler for the mouse up
 */
private function mouseUpHandler(event:MouseEvent):void {
	trace("mouseUpHandler");

	if (_textFieldClicked) {
		//drag/drop event is done
		reset();
		this.systemManager.removeEventListener(MouseEvent.MOUSE_UP, mouseUpHandler, true);
		return;
	}

	_tooltipEnabled=false;

	this.systemManager.removeEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler, true);
	this.systemManager.removeEventListener(MouseEvent.MOUSE_UP, mouseUpHandler, true);

	if (this.currentState == ToolbarMode.PAN) {
		this.currentState=ToolbarMode.SELECT;
		this._panStartPoint = null;
	} else if (this.currentState == ToolbarMode.ZOOM_WINDOW) {
		// Stop zooming
		_zooming=false;

		//fix for kb# 3031551 - In the drawing control, when the zoom window option is used and the user clicks thier mouse but does not drag,
		//a non-functional zoom rectangle will follow thier mouse."
		if (selectWindowArea.height > 5 && selectWindowArea.width > 5) {
			zoomToArea(selectWindowArea.getRect(dwgContainer));
		}
		this.currentState=ToolbarMode.SELECT;

		this.systemManager.addEventListener(MouseEvent.MOUSE_OUT, mouseOutHandler, true);
		this.systemManager.addEventListener(MouseEvent.MOUSE_OVER, mouseOverHandler, true);
	} else if (this.currentState == ToolbarMode.MULTI_SELECT) {
		// Stop selecting
		_selecting=false;

		// if the asset can be assigned.
		if (selectWindowArea.height > 5 || selectWindowArea.width > 5) {
			var continueLoop:Boolean=true;
			for (var i:int=0; i < this.dwgContainer.getChildren().length && continueLoop; i++) {
				var ad:ArchibusDrawing=this.dwgContainer.getChildAt(i) as ArchibusDrawing;
				for (var j:int=0; ad && j <ad.assetLayer.numChildren && continueLoop; j++) {
					var aob:AssetObject=ad.assetLayer.getChildAt(j) as AssetObject;
					if(CollisionDetection.checkForCollision(aob, selectWindowArea, this)){
						//if select or assign successful, the loop will continue only for one-to-many assignment
						if(this.handleSelectOrAssignment(ad, aob.ids, aob, false) && ad.config.assignMode != AssignMode.ONE_TO_MANY)
							continueLoop = false;
					}
				}
			}
		}

		ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_ONMULTIPLESELECT));

		this.currentState = ToolbarMode.SELECT;

		this.systemManager.addEventListener(MouseEvent.MOUSE_OUT, mouseOutHandler, true);
		this.systemManager.addEventListener(MouseEvent.MOUSE_OVER, mouseOverHandler, true);
	} else if (this.currentState == ToolbarMode.SELECT) {
		_tooltipEnabled=true;
	}

	//fix for kb# 3031551 - always hide the zoom window with "mouse up" action.
	ensureZoomWindowHidden();

	_mouseDownClicked=false;

	//tooltipEventHandler(event);
}

public function ensureZoomWindowHidden():void {
	selectWindowArea.width=0;
	selectWindowArea.height=0;
	selectWindowArea.visible=false;
}

private function zoomOnFullLoad(ad:ArchibusDrawing):void {
	if (ad == null)
		return;

	var perDrawingConfig:DrawingConfig = ad.config;
	if (perDrawingConfig == null || !perDrawingConfig.zoomToId)
		return;

	var roomId:String=perDrawingConfig.highlightId;
	var zoomArea:Rectangle=ad.findAsset(roomId.split(_controlConfig.runtimeConfig.keyDelimiter));

	if (zoomArea != null) {
		var zf:Number=_controlConfig.getZoomFactor();
		var multX:Number=0.0;
		var multY:Number=0.0;
		zoomArea.height=zoomArea.height * zf;
		zoomArea.width=zoomArea.width * zf;

		//position the zoom area to the center of the page.
		zoomArea.x+=(ad.x - dwgContainer.x);
		zoomArea.y+=(ad.y - dwgContainer.y);

		// TBD Not sure why the offset varies dependent on the zoomFactor
		// and is not linear.  At this time do not have a direct relationship
		// between them, so this lookup approximates the offset for most cases
		if (zf <= 1.2) {
			multX=0.0;
			multY=0.0;
		} else if (zf <= 2.0) {
			multX=0.25;
			multY=0.00;
		} else if (zf <= 3.0) {
			multX=0.28;
			multY=0.14;
		} else if (zf <= 4.0) {
			multX=0.33;
			multY=0.16;
		} else if (zf <= 6.0) {
			multX=0.36;
			multY=0.22;
		} else if (zf <= 10.0) {
			multX=0.41;
			multY=0.35;
		} else {
			multX=0.45;
			multY=0.42;
		}

		zoomArea.x-=(zoomArea.width * multX);
		zoomArea.y-=(zoomArea.height * multY);

		// If the control height > width, need to compensate.  Not sure why at this time,
		// but this does work better than if not adjusted here.  TBD, look into a better
		// way of handling this
		if (this.height > this.width)
			zoomArea.y+=(this.height * 0.20);

		zoomToArea(zoomArea);
	}
}

/**
 * Zoom in to given canvas
 */
private function zoomToArea(zoomArea:Rectangle):void {
	_zooming=false;
	_selectWindowStartPoint=null;

	var ad:ArchibusDrawing=null;
	var x_scale:Number=dwgContainer.width / zoomArea.width;
	var y_scale:Number=dwgContainer.height / zoomArea.height;

	var sfLocal:Number=Math.min(x_scale, y_scale);
	_scaleFactor=_scaleFactor * sfLocal;

	// set zoom slider value
	if (this.zoomFactor * sfLocal > CommonUtilities.ZOOMFACTOR_MAX) {
		this.navCanvas.zoomSlider.value=this.navCanvas.zoomSlider.maximum=this.zoomFactor=this.zoomFactor * sfLocal;
	} else {
		this.navCanvas.zoomSlider.value=this.zoomFactor=this.zoomFactor * sfLocal;
	}

	//add the rollback factor if user clicked "p" to go back to previous zoom
	if (sfLocal != 1)
		rollbackFactors.push(1 / sfLocal);

	var ads:Array=dwgContainer.getChildren();
	var positions:Object=_layoutUtilities.calculatePositions(ads);
	var ad0:ArchibusDrawing=null;
	for (var i:Number=0; i < ads.length; i++) {
		ad=ads[i] as ArchibusDrawing;
		ad.resize(ad.width * sfLocal, ad.height * sfLocal);

		var ar:Array=positions[ad.location.getDrawingName()];
		if (ar[0] == 0 && ar[1] == 0)
			ad0=ad;
	}

	if (ad0 == null)
		return;

	var zoomCenter:Point=new Point(zoomArea.x + zoomArea.width / 2, zoomArea.y + zoomArea.height / 2);
	var scaledCenter:Point=new Point((zoomCenter.x - ad0.x) * sfLocal + ad0.x, (zoomCenter.y - ad0.y) * sfLocal + ad0.y);
	var xOffset:Number=ad0.x - scaledCenter.x + dwgContainer.width / 2;
	var yOffset:Number=ad0.y - scaledCenter.y + dwgContainer.height / 2;
	autoPosition(xOffset, yOffset, false, positions);
}

/**
 * Handles mouse wheel event
 */
public function mouseWheelZoom(event:MouseEvent):void {
	(event.delta > 0) ? zoomIn(null) : zoomOut(null);
}


