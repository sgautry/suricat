/**
 * Holds the drawing controls's private event handler functions for the toolbar buttons,
 * such as zooming, panning, change state, change draw mode etc.
 */
import flash.events.MouseEvent;
import flash.geom.Point;

import mx.events.ItemClickEvent;
import mx.managers.CursorManager;

import src.controller.toolbar.ToolbarMode;
import src.model.redline.DrawMode;
import src.view.drawing.ArchibusDrawing;

// Property: ID of the current custom cursor ( can be PAN, ZOOM etc), or NO_CURSOR if the system cursor is showing.
//private var _cursorID:Number=0;

// Property: panning hand cursor. "pan" image will show.
//[Embed(source="assets/img/cursor_pan.gif")]
//private var _panCursor:Class;

// Property: zoom window hand cursor. "zoom window" image will show.
[Embed(source="assets/img/control/zoom_window_select.png")]
public var _zoomWindowSelectedImage:Class;

[Embed(source="assets/img/control/zoom_window.png")]
private var _zoomWindowDefaultImage:Class;

// Property: zoom window hand cursor. "zoom window" image will show.
[Embed(source="assets/img/control/multiple_selection_select.png")]
public var _multiSelectSelectedImage:Class;

[Embed(source="assets/img/control/multiple_selection.png")]
private var _multiSelectDefaultImage:Class;

// Property: draw hand cursor. "draw" image will show.
//[Embed(source="assets/img/icon_draw.gif")]
//private var _drawCursor:Class;

// Property: starting point of the panning action
private var _panStartPoint:Point;

// Property: true when right after a panning action, false otherwise
private var _panned:Boolean=false;

// Property: the starting point of the window zoom or multi-room select (usually the left upper corner or right botton corner)
private var _selectWindowStartPoint:Point;

// Property: true when zooming, false otherwise
private var _zooming:Boolean=false;

// Property: true when selecting multiple rooms, false otherwise
private var _selecting:Boolean=false;

/**
 * Resets zoom window's area before the zooming starts.
 */
private function setStartSelecting():void {
	_selectWindowStartPoint=new Point(this.mouseX, this.mouseY);
	selectWindowArea.x=_selectWindowStartPoint.x;
	selectWindowArea.y=_selectWindowStartPoint.y;
	selectWindowArea.width=0;
	selectWindowArea.height=0;
	selectWindowArea.visible=false;
	if(this.currentState == ToolbarMode.ZOOM_WINDOW)
		_zooming=true;
	else {
		this._assignUtilities.assignedMapOneToMany.length = 0;
		_selecting=true;
	}

}

public function clearSelectWindowArea():void {
	if(!_selectWindowStartPoint)
		return;

	selectWindowArea.x=_selectWindowStartPoint.x;
	selectWindowArea.y=_selectWindowStartPoint.y;
	selectWindowArea.width=0;
	selectWindowArea.height=0;
	selectWindowArea.visible=false;
	if(this.currentState == ToolbarMode.ZOOM_WINDOW)
		_zooming=false;
	else {
		this._assignUtilities.assignedMapOneToMany.length = 0;
		_selecting=false;
	}
}

/**
 * Updates window's zoom area based on the mouse position.
 */
private function updateSelectWindowArea():void {
	selectWindowArea.x=Math.min(this.mouseX, _selectWindowStartPoint.x);
	selectWindowArea.y=Math.min(this.mouseY, _selectWindowStartPoint.y);
	selectWindowArea.width=Math.abs(this.mouseX - _selectWindowStartPoint.x);
	selectWindowArea.height=Math.abs(this.mouseY - _selectWindowStartPoint.y);
	selectWindowArea.visible=true;

	trace("selectWindowArea x=" + selectWindowArea.x + " y=" + selectWindowArea.y + " width=" + selectWindowArea.width + " height=" + selectWindowArea.height);
}



