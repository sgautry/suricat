/**
 * Holds the drawing controls's functions for the layout such as auto center, auto scale and auto position.
 */
import flash.events.*;
import flash.utils.*;

import src.model.layout.*;
import src.view.drawing.ArchibusDrawing;

/**
 * Property: the interval to delay the auto scale action.
 * This variable is used to work around a Flash bug.
 */
private var _delayInterval:uint=0;

/**
 * Property: auto scale counter
 * This variable is used to work around a Flash bug.
 */
private var _numOfAutoScaleAttempts:int=0;

public var zoomFactor:Number=1;

/**
 * Property: currect applied scale factor for drawing
 */
private var _scaleFactor:Number=0.01;

/**
 * Auto center the drawing
 */
public function autoCenter(isoInit:Boolean=false):void {
	var w:Number=getMaxDwgWidth();
	var h:Number=getMaxDwgHeight();
	var layoutInfo:LayoutTable=_layoutUtilities.getDrawingsLayoutTable(this.dwgContainer.getChildren());
	if (layoutInfo == null)
		return;

	var xOffset:Number=(this.dwgContainer.width - (w * layoutInfo.numColumns)) / 2;
	var yOffset:Number=(this.dwgContainer.height - (h * layoutInfo.numRows)) / 2;

	autoPosition(xOffset, yOffset, isoInit);
}

/**
 * Auto position each drawing according to its canvas space.
 */
public function autoPosition(xOffset:Number=0, yOffset:Number=0, isoInit:Boolean=false, positions:Object=null):void {
	var ads:Array=this.dwgContainer.getChildren();
	if (ads == null || !ads.length)
		return;

	if (positions == null)
		positions=_layoutUtilities.calculatePositions(ads);

	// Determine the max width and height
	var w:Number=getMaxDwgWidth();
	var h:Number=getMaxDwgHeight();

	for (var drawingIndex:Number=0; drawingIndex < ads.length; drawingIndex++) {
		var ad:ArchibusDrawing=ads[drawingIndex];
		var ar:Array=positions[ad.location.getDrawingName()];
		ad.move(xOffset + (w * ar[0]) + (isoInit ? (w * 0.333) : 0), yOffset + (h * ar[1]));
	}
}

/**
 * Auto scale drawing to fit darwing container
 */
public function autoScale(isoInit:Boolean=false):void {
	var ads:Array=this.dwgContainer.getChildren();
	var layoutInfo:LayoutTable=_layoutUtilities.getDrawingsLayoutTable(ads);
	if (layoutInfo == null)
		return;

	var w:Number=this.getMaxDwgWidth();
	var h:Number=this.getMaxDwgHeight();
	var accHeight:Number=layoutInfo.numRows * h;
	var accWidth:Number=layoutInfo.numColumns * w;
	
	//XXX: avoid fatal error when accWidth or accHeight = 0
	if(accWidth == 0 || accHeight==0){
		return;
	}
	var x_scale:Number=this.dwgContainer.width / accWidth;
	var y_scale:Number=this.dwgContainer.height / accHeight;
	var scale:Number=Math.min(x_scale, y_scale);

	_scaleFactor=_scaleFactor * scale;

	for (var j:Number=0; j < ads.length; j++) {
		var ad:ArchibusDrawing=ads[j] as ArchibusDrawing;

		if (isNaN(ad.height) || ad.height < 1 || ad.width == 10000) {
			// This odd chunk of code satisfies the issue where sometimes
			// the dwgContainer.height is 0, not the actual height if
			// the addDrawing function was called during the initialization
			// of the Drawing Control.  This delay resolves that issue.
			// This appears to be a Flash bug that we are working around.
			_delayInterval=setInterval(delayFunction, 200);
		} else
			_numOfAutoScaleAttempts=0;

		ad.resize(ad.width * scale, ad.height * scale);
	}

	trace("autscale _scaleFactor: " + _scaleFactor);

	this.navCanvas.zoomSlider.value = this.zoomFactor = 1;

	autoCenter(isoInit);
}


/**
 * Gets the maximum drawing height from all drawings.
 *
 * @return the drawing height
 */
private function getMaxDwgHeight():Number {
	var ads:Array=this.dwgContainer.getChildren();
	if (ads == null || !ads.length)
		return 0;

	var h:Number=0;

	for (var i:int=0; i < ads.length; i++) {
		var ad:ArchibusDrawing=ads[i];
		if (ad.getDimensions().height > h)
			h=ad.getDimensions().height;
	}

	return h;
}


/**
 * Gets the maximum drawing width from all drawings.
 *
 * @return the drawing width
 */
private function getMaxDwgWidth():Number {
	var ads:Array=this.dwgContainer.getChildren();
	if (ads == null || !ads.length)
		return 0;

	var w:Number=0;

	for (var i:int=0; i < ads.length; i++) {
		var ad:ArchibusDrawing=ads[i];
		if (ad.getDimensions().width > w)
			w=ad.getDimensions().width;
	}

	return w;
}


/**
 * delay function for auto scale in order to workarounf a flash bug.
 */
private function delayFunction():void {
	clearInterval(_delayInterval);
	if (_numOfAutoScaleAttempts < 2) {
		_numOfAutoScaleAttempts++;
		autoScale();
	}
}


