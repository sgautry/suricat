package src.view.drawing {
	import flash.events.*;
	import flash.geom.*;
	import flash.external.ExternalInterface;
	import src.controller.assign.AssignedObject;
	import src.controller.toolbar.ToolbarMode;
	import src.model.config.*;
	import src.model.data.*;
	import src.model.redline.DrawMode;
	import src.model.text.AbTextFormat;
	import src.view.asset.AssetObject;
	import src.view.highlight.*;
	import src.model.util.CommonUtilities;
	import src.model.util.JsFunctionConstant;

	/**
	 * Defines the component representing a single drawing.
	 */
	public class ArchibusDrawing extends ArchibusDrawingCanvas {
		/**
		 *Is the drawing fully loaded? true or false.
		 */
		private var _fullyLoaded:Boolean=false;

		/**
		 * Properyt: true if the drawing loads completely.
		 * A valid load of the drawing is complete if and only if both the 'complete' and the 'updateComplete' events are fired from the ArchibusDrawing object.
		 * It is possible for a no-file found condition for the 'updateComplete' event to still fire.
		 * This is to ensure we can test for a full valid load of a file before processing further
		 */
		private var _loadComplete:Boolean=false;

		/**
		 * Property: true to allow highlighting to be completed after drawing is fully loaded, false otherwise.
		 */
		private var _doCompleteHighlights:Boolean=false;

		/**
		 * Property: true to allow findAsset to be completed after drawing is fully loaded, false otherwise.
		 */
		private var _doCompleteFindAsset:Boolean=false;

		/**
		 * Property: true if the room has been found and the drawing is reloaded, false otherwise.
		 * This value is set to false before finding room or add drawing.
		 */
		private var _dwgLoadedForFind:Boolean=false;

		/**
		 * Location information for the drawing such as bl_id, fl_id etc.
		 */
		private var _location:Location=null;

		/**
		 * the per drawing configuration.
		 */
		private var _config:DrawingConfig=null;

		/**
		 * publish scale for the drawing
		 */
		//private var _publishScale:Number=1.0;

		/**
		 * the drawing highlight
		 */
		private var _highlight:DrawingHighlight=null;
		
		private var _draggableAssets:Array=[];

		/**
		 * Constructor.
		 *
		 * @param config per-drawing config object
		 * @param location the location information for the drawing such as bl_id, fl_id etc.
		 */
		public function ArchibusDrawing(config:DrawingConfig, location:Location) {
			this._config=config;
			this._location=location;

			//set the asset id (i.e. rooom) to highlight
			this._highlight=new DrawingHighlight(this._config.highlightId, this);
			
			_draggableAssets=ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_GETDRAGGABLEASSETS));

			this.addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			this.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
		}

		public function getDraggableAssets():Array {
			return this._draggableAssets;
		}
		
		public function setDraggableAssets(draggableAssets:Array):void{
			this._draggableAssets = draggableAssets;
		}

		public function set loadComplete(value:Boolean):void {
			_loadComplete=value;
		}

		public function set doCompleteHighlights(value:Boolean):void {
			_doCompleteHighlights=value;
		}

		public function set doCompleteFindAsset(value:Boolean):void {
			_doCompleteFindAsset=value;
		}

		public function get dwgLoadedForFind():Boolean {
			return _dwgLoadedForFind;
		}

		public function set dwgLoadedForFind(value:Boolean):void {
			_dwgLoadedForFind=value;
		}

		public function set highlight(value:DrawingHighlight):void {
			_highlight=value;
		}

		public function set fullyLoaded(value:Boolean):void {
			_fullyLoaded=value;
		}

		public function get loadComplete():Boolean {
			return _loadComplete;
		}

		public function get doCompleteHighlights():Boolean {
			return _doCompleteHighlights;
		}

		public function get doCompleteFindAsset():Boolean {
			return _doCompleteFindAsset;
		}

		/**
		 * Highlights the provided set of assets in given color
		 *
		 * @param	dwgOpts	A DwgOptions object
		 * @param	af:		The default AssetFormat to apply
		 *
		 */
		public function highlightAssets(dwgConfig:DrawingConfig, af:FillFormat, resetPersist:Boolean=true):void {
			if (dwgConfig == null)
				return;

			assetLayer.drawAssetsFromOpts(dwgConfig, af);

			if (resetPersist) {
				if (!dwgConfig.persistRecFills){
					dwgConfig.setRecs(null);
				}
				this.config=dwgConfig;
			}
		}

		/**
		 * Getter for the assets property
		 *
		 * @return	The assets of this drawing.
		 */
		public function get assets():Array {
			if (assetLayer.assetMap == null)
				return null;
			else
				return assetLayer.assetMap.assets;
		}

		/**
		 * Setter for the assets property
		 *
		 * @param	value	The assets to be used
		 */
		public function set assets(value:Array):void {
			assetLayer.setAssets(value);
			assetLayer.visible=true;
			labelLayer.visible=true;

			// Apply default hilite, if any
			this._highlight.initialHilite(null, null, true);

			// Generate all of the labels for the assets
			labelLayer.populateLabels(_location.getPrimarykeysArray(true), this);

			//XXX:????
			// create a display label for this particular floor plan
			setTitleText(this.location.getDrawingName().toUpperCase());
			syncLayers();
			this.dispatchEvent(new Event("assetsLoaded"));

			this._fullyLoaded=true;
			this.dispatchEvent(new Event("fullyLoaded"));
		}

		/**
		 * Clears the highlighted assets.
		 */
		public function clearAssets():void {
			assetLayer.clearAssets();
			this.config.setRecs(null);
		}

		public function clearPersistFills():void {
			if (this.config == null)
				return;

			this.config.fill=null;

			if (this.config.recs == null || this.config.recs.length < 1) {
				return;
			}

			var af:FillFormat=new FillFormat(FillTypes.UNSELECTED);
			for (var i:int=0; i < this.config.recs.length; i++) {
				var rec:Object=this.config.recs[i];
				assetLayer.setAssetColor(rec.id.split(this.parentApplication.controlConfig.runtimeConfig.keyDelimiter), af, false);
			}
			
			//set config.recs to null
			this.config.setRecs(null);
		}

		/**
		 * Apply the specified DS
		 */
		public function applyDS(type:String, highlightDSName:String=null, bordersHighlightDSName:String=null):void {
			if (type == 'highlight') {
				this.highlight.initialHilite(highlightDSName, bordersHighlightDSName);
			} else if (type == 'labels') {
				this.labelLayer.clearLabels();
				this.labelLayer.populateLabels(this.location.getPrimarykeysArray(), this);
			}
		}

		/**
		 * Returns the bounding box of the asset having the specified id.
		 *
		 * @param	ids	The ids of the asset to be found
		 *
		 * @return	The bounding rectangle of the asset.
		 */
		public function findAsset(ids:Array, toGlobal:Boolean=false):Rectangle {
			if (assetLayer.assetMap == null)
				return null;

			var asset:Object=assetLayer.assetMap.getAsset(ids);
			
			if (asset != null && dwgDrawing.content != null) {
				//bring target asset to the front
				assetLayer.addChild(assetLayer.getAssetOb(ids));
				
				var minX:Number=new Number();
				var minY:Number=new Number();
				var maxX:Number=new Number();
				var maxY:Number=new Number();

				for (var j:Number=0; j < asset.swfGraphics.length; j++) {
					var segment:Object=asset.swfGraphics[j];
					if (minX == 0 || segment.startX < minX) {
						minX=segment.startX;
					}
					if (minY == 0 || segment.startY < minY) {
						minY=segment.startY;
					}
					if (segment.startX > maxX) {
						maxX=segment.startX;
					}
					if (segment.startY > maxY) {
						maxY=segment.startY;
					}
				}

				var area:Rectangle=new Rectangle();
				var scaleX:Number=dwgDrawing.width / dwgDrawing.content.width;
				var scaleY:Number=dwgDrawing.height / dwgDrawing.content.height;

				if (toGlobal) {
					var pt1:Point=this.localToGlobal(new Point(minX, minY));
					var pt2:Point=this.localToGlobal(new Point(maxX, maxY));
					area.x=pt1.x;
					area.y=pt1.y;
					area.width=pt2.x - pt1.x;
					area.height=pt2.y - pt1.y;
				} else {
					area.width=(maxX - minX) * scaleX * 1.2;
					area.height=(maxY - minY) * scaleY * 1.2;
					area.x=assetLayer.x + minX * scaleX;
					area.y=assetLayer.y + minY * scaleY;
				}

				return area;
			}
			return null;
		}

		public function get fullyLoaded():Boolean {
			return this._fullyLoaded;
		}

		public function getAssetOb(ids:Array):AssetObject {
			return assetLayer.getAssetOb(ids);
		}


		/**
		 * Completes the highlightAssets functionality.  This method will be called
		 * when the 'assetsLoaded' event is fired if needed, or called directly.
		 */
		public function completeHighlights():void {
			//flag the highlight is not completed yet.
			_doCompleteHighlights=false;

			if (this._config == null)
				return;

			//make sure highlight options
			var recs:Array=this._config.recs;
			if (recs == null || !recs.length)
				return;

			// We are only interested in updating drawings associated with
			// the specified records.  This will be used to filter our
			// list of drawings.
			var floorIds:Object={};

			//for non-room asset, the id does not contain the drawing name, use the rawDwgName to set the FloorIds value
			if (this._config.rawDwgName != null && this._config.rawDwgName.length > 0) {
				floorIds[this._config.rawDwgName.toLowerCase()]=true;
			} else {
				//for room asset, retrieve the bl_id and fl_id values from the id.
				for each (var rec:Object in recs) {
					var location:String=rec.id;
					var highlight:Array=location.split(this.parentApplication.controlConfig.runtimeConfig.keyDelimiter) as Array;
					var floorId:String=highlight[0] + highlight[1];
					floorIds[floorId.toLowerCase()]=true;
				}
			}

			var dwgName:String=this.location.getDrawingName();
			var doHighlights:Boolean=false;

			/*
			*	The logic below works for most common use cases.  Where it doesn't
			*  work is the case where there are multiple swf files loaded at the same
			*  time for a single floor plan.   e.g.  hq17-east, hq17-west, etc.
			*  The reason for this issue at all is that the rec objects that are
			*  processed above in most cases does not have knowledge of the actual
			*  drawing name, just the pkeys for that particular asset. e.g.  hq  17  101
			*  TBD: For a future release determine how to support this multiple views
			*  per floor plan case.
			*
			*/
			if (floorIds[dwgName] != true) {
				var swfId:String=this.location.getBuilding() + this.location.getFloor();
				doHighlights=(floorIds[swfId.toLowerCase()] == true);
			} else
				doHighlights=true;

			if (doHighlights)
				this.highlightAssets(this._config, null);

			this._doCompleteHighlights=true;
		}

		/**
		 * Utility method for obtaining the dimensions for this component,
		 * while taking into account the transformation applied to this
		 * specific component.
		 */
		public function getDimensions(debug:Boolean=false):Rectangle {
			var transform:Matrix=this.transform.matrix;
			var p1:Point=transform.transformPoint(new Point(0, 0));
			var p2:Point=transform.transformPoint(new Point(0, this.height));
			var p3:Point=transform.transformPoint(new Point(this.width, 0));
			var p4:Point=transform.transformPoint(new Point(this.width, this.height));
			return new Rectangle(Math.min(p1.x, p2.x, p3.x, p4.x), Math.min(p1.y, p2.y, p3.y, p4.y), Math.max(p1.x, p2.x, p3.x, p4.x) - Math.min(p1.x, p2.x, p3.x, p4.x), Math.max(p1.y, p2.y, p3.y, p4.y) - Math.min(p1.y, p2.y, p3.y, p4.y));
		}

		public function getJsonAssetOb(ids:Array):Object {
			if (assetLayer.assetMap == null)
				return null;
			else
				return assetLayer.assetMap.getAsset(ids);
		}

		/**
		 * Returns the Location name for this drawing
		 *
		 * @return	The Location name of this drawing.
		 */
		public function getName():String {
			return _location.getDrawingName();
		}

		public function getSelectedAssetIds():Array {
			return assetLayer.getSelectedAssetIds();
		}

		public function getUnscaledDimensions():Point {
			return new Point(this.width * this.scaleX, this.height * this.scaleY);
		}


		public function get idealLabelTextSize():uint {
			return this.labelLayer.idealLabelTextSize;
		}

		public function set idealLabelTextSize(textSize:uint):void {
			this.labelLayer.idealLabelTextSize=textSize;
		}


		/**
		 * Getter for the Location object
		 *
		 * @return	The Location of this drawing.
		 */
		public function get location():Location {
			return _location;
		}

		/**
		 * Setter for the location property
		 *
		 * @param	location		A Location object assocaited with this document
		 */
		public function set location(value:Location):void {
			_location=value;
		}

		public function makeAssetsSelectable(opts:DrawingConfig, selectability:Boolean):void {
			if (opts.recs == null || !opts.recs.length)
				return;

			for (var i:int=0; i < opts.recs.length; i++) {
				var rec:Object=opts.recs[i];
				var id:Array=rec.id.split(this.parentApplication.controlConfig.runtimeConfig.keyDelimiter) as Array;
				assetLayer.makeAssetSelectable(id, selectability);
			}
		}

		/**
		 * Resets the highlighted assets.
		 */
		public function resetAssets():void {
			assetLayer.resetToInitial();
		}

		/**
		 * Resizes this drawing component. This method should be used instead of explicitely
		 * using the width and height properties. This method will ensure all layers stay
		 * synchronized.
		 */
		public function resize(width:Number, height:Number):void {
			this.dwgDrawing.width=width;
			this.dwgDrawing.height=height;
			syncLayers();
		}


		/**
		 * Sets the labels given the drawing configuration and append mode.
		 * @param opts the drawing configuration object
		 * @param mode label append mode
		 */
		public function setLabels(opts:Object, mode:int):void {
			if (opts == null)
				return;

			var dwgOpts:DrawingConfig=new DrawingConfig();
			dwgOpts.set(opts);

			labelLayer.setLabels(dwgOpts, mode);
		}

		/**
		 * Sets the title's text given the text format. If the text format is NULL, the parent's text format will be used.
		 * @param textToSet the text content to set.
		 * @param tf thext format
		 */
		public function setTitleText(textToSet:String, tf:AbTextFormat=null):void {
			labelLayer.setTitle(textToSet, (tf == null) ? this.parentApplication.controlConfig.labelConfig.getTextFormat(LabelConfig.FORMAT_PARENT) : tf)
		}

		/**
		 * Toggles the asset selection on and off specifying asset ids and selection mode.
		 *
		 * @param ids the asset ids.
		 * @param mode selection mode.
		 */
		public function toggleAssetSelection(ids:Array, mode:int, assignedObject:AssignedObject=null):void {
			assetLayer.toggleAssetSelection(ids, mode, assignedObject);
		}

		override protected function commitProperties():void {
			super.commitProperties();
		}

		/**
		 * Synchronizes the architectural background layer with the asset layers.
		 */
		private function syncLayers():void {
			if (dwgDrawing.content != null) {
				drawingLayer.scaleX=labelLayer.scaleX=assetLayer.scaleX=dwgDrawing.width / dwgDrawing.content.width;
				drawingLayer.scaleY=labelLayer.scaleY=assetLayer.scaleY=dwgDrawing.height / dwgDrawing.content.height;
				drawingLayer.x=labelLayer.x=assetLayer.x=dwgDrawing.x;
				drawingLayer.y=labelLayer.y=assetLayer.y=dwgDrawing.y;
			}
		}

		public function get highlight():DrawingHighlight {
			return _highlight;
		}

		public function set config(value:DrawingConfig):void {
			_config=value;
		}


		public function get config():DrawingConfig {
			return _config;
		}


		public function get shrinkLabelTextToFit():Boolean {
			return this.labelLayer.shrinkLabelTextToFit;
		}

		public function set shrinkLabelTextToFit(value:Boolean):void {
			this.labelLayer.shrinkLabelTextToFit=value;
		}


		public function get minimumLabelTextSize():uint {
			return this.labelLayer.minimumLabelTextSize;
		}

		public function set minimumLabelTextSize(textSize:uint):void {
			this.labelLayer.minimumLabelTextSize=textSize;
		}

		public function onMouseOver(event:MouseEvent=null):void {
			if (this.focusPane == null) {
				var parentApp:abDrawing=(this.parentApplication as abDrawing);
				if(parentApp.currentState == ToolbarMode.SELECT || (parentApp.currentState == ToolbarMode.DRAW && this.drawingMode != DrawMode.drawModeText)){
					trace("set focus");
					stage.focus=this;
				}
			}
		}

		private function onKeyDown(event:KeyboardEvent):void {
			trace("keyDownHandler: " + event.keyCode);
			var parentApp:abDrawing=(this.parentApplication as abDrawing);
			switch (event.keyCode) {
				case 37:
					//LeftArrow = 37
					if(this.drawingMode != DrawMode.drawModeText)
						parentApp.pan(-20, 0);
					break;
				case 38:
					//UpArrow = 38
					if(this.drawingMode != DrawMode.drawModeText)
						parentApp.pan(0, -20);
					break;
				case 39:
					//RightArrow = 39
					if(this.drawingMode != DrawMode.drawModeText)
						parentApp.pan(20, 0);
					break;
				case 40:
					//DownArrow = 40
					if(this.drawingMode != DrawMode.drawModeText)
						parentApp.pan(0, 20);
					break;
				case 80:
					// letter p
					if (parentApp.rollbackFactors.length > 0) {
						var rollbackFactor:Number=parentApp.rollbackFactors.pop();
						if (rollbackFactor > 1)
							parentApp.zoomIn(null, rollbackFactor, true);
						else
							parentApp.zoomOut(null, rollbackFactor, true);
					}
					break;
				case 27:
					//escape key
					parentApp.currentState=ToolbarMode.SELECT;
					this.drawingMode=DrawMode.drawModeOff;
					parentApp.ensureZoomWindowHidden();
					parentApp._tooltipEnabled=true;
					stage.focus=this;
					break;
			}
		}
	}
}


