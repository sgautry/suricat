package src.view.drawing {
	import flash.external.ExternalInterface;
	
	import mx.collections.ArrayCollection;
	
	import src.model.config.ControlConfig;
	import src.model.data.DataSource;
	import src.model.data.SourceType;
	import src.model.util.CommonUtilities;
	import src.model.util.JsFunctionConstant;
	import src.view.highlight.*;
	import src.view.highlight.HighlightFormat;

	/**
	 * Supports the drawing highlight functionality, such as highlight an asset, find the color field and values etc.
	 */
	public class DrawingHighlight {

		/**
		 *	delimeter separated highilight asset's id, i.e. HQ,18,101
		 */
		private var _highlightId:String="";


		/**
		 * filed name for highlight, such as rm.rm_cat. The field is also used in legend.
		 */
		private var _colorFieldName:String="";

		/**
	  * Caches already decoded pattern map of patternString -> patternProperties.
																					* this map is used to significantly reduces the amount of overhead of having to query the server for each and every asset in a drawing for the
		* highligting data assocatiated with each asset.
		*/
		private var _existingPatterns:Object={};

		/**
		 * Highlight rule value field obtained from JavaScript.
		 */
		private var _highlightRuleValueField:String="";

		/**
		 * an array of all the highlight legend keys' field names
		 */
		private var _highlightValueFields:Array=null;

		/**
		 * A map of existing highlight value and color used, in format of:
		 *  [ { value: 'sales', color: 'ff00cc' }, {...}]
		 */
		public var _usedValueColorList:Object={};

		/**
		 * A map that provides quick look up based on the pks for all recs stored in the ds object.
		 */
		private var _recMap:Object=null;

		/**
		 * the archibus drawing related with the highlight object
		 * @default
		 */
		private var _drawing:ArchibusDrawing=null;

		/**
		 * Records the ids map for disabled assets.
		 * @default
		 */
		private var _disabledIds:Object={};


		/**
		 * Consructor.
		 *
		 * @param highlightId the asset id to highlight.
		 * @param abd the ArchibusDrawing object the highlight takes place.
		 */
		public function DrawingHighlight(highlightId:String, abd:ArchibusDrawing) {

			super();
			this._highlightId=highlightId;
			this._drawing=abd;
		}

		/**
		 * Gets the highlight value by asset ids.
		 *
		 * @param ids an array of asset's ids
		 * @return highlight values
		 */
		public function getHighlightValueByIds(ids:Array):String {
			var val:String="";
			var rec:Object=_recMap[ids];
			if (rec != null)
				val=getRecHighlightValue(rec);

			return val;
		}

		/**
		 * Retrieves the color field's value and decode it into an object.
		 *
		 * @param rec the record to retrive the color field's value from.
		 * @return an decoded pattern object
		 */
		private function getDecodedPattern(rec:Object):Object {
			var props:Object=null;

			if (this._drawing.parentApplication.highlightType.isHighlightThematic() && this._drawing.parentApplication.highlightType.isThematicPredefined()) {
				var pattern:String=rec[_colorFieldName];
				if (pattern && pattern.length) {
					// First lookup in our cached list of mapped values
					props=this._existingPatterns[pattern];
					if (props == null) {
						// If not already cached, then make a call back to the javascript to get this information
						props=ExternalInterface.call(CommonUtilities.getFullFuncCall("decodePattern"), pattern);
						this._existingPatterns[pattern]=props;
					}
				}
			}

			return props;
		}

		private function getFillProperties(rec:Object):Object {
			var props:Object=getDecodedPattern(rec);

			if (props == null) {
				props={};
				// First check per the value-color map to see if a color has already beeen
				// assigned to this value or not
				var val:String=getRecHighlightValue(rec); //rec[highlightValueField];					
				if (!val || !val.length) {
					props.color=this._drawing.parentApplication.controlConfig.highlightConfig.getFillColor(FillTypes.UNASSIGNED);
					props.opacity=this._drawing.parentApplication.controlConfig.highlightConfig.getFillOpacity(FillTypes.UNASSIGNED);
					props.boundaryColor=this._drawing.parentApplication.controlConfig.highlightConfig.getBoundaryColor(FillTypes.UNASSIGNED);
					props.boundaryOpacity=this._drawing.parentApplication.controlConfig.highlightConfig.getBoundaryOpacity(FillTypes.UNASSIGNED);
					props.boundaryThickness=this._drawing.parentApplication.controlConfig.highlightConfig.getBoundaryThickness(FillTypes.UNASSIGNED);
				} else {
					var ruleValue:String=rec[_highlightRuleValueField];
					var fields:String="";
					var tot:int=_highlightValueFields.length;
					for (var i:int=0; i < tot; i++)
						fields+=_highlightValueFields[i];
					props.color=CommonUtilities.getColorFromValue(fields, val, ruleValue);
					props.opacity=this._drawing.parentApplication.controlConfig.highlightConfig.getFillOpacity(FillTypes.ASSIGNED);
					props.boundaryColor=this._drawing.parentApplication.controlConfig.highlightConfig.getBoundaryColor(FillTypes.ASSIGNED);
					props.boundaryOpacity=this._drawing.parentApplication.controlConfig.highlightConfig.getBoundaryOpacity(FillTypes.ASSIGNED);
					props.boundaryThickness=this._drawing.parentApplication.controlConfig.highlightConfig.getBoundaryThickness(FillTypes.ASSIGNED);
				}
			}

			return props;
		}

		/**
		 * Gets the record's highlight values associated with highlight fields as string.
		 *
		 * @param rec the record object
		 * @param useSeparator add the separator between values
		 * @return the highlight values string
		 */
		private function getRecHighlightValue(rec:Object, useSeparator:Boolean=false):String {
			var val:String="";
			var tot:int=_highlightValueFields.length;

			// Just concatenate the values per the specified highlight value fields
			for (var i:int=0; i < tot; i++)
				val+=((useSeparator == true) ? "-" : "") + rec[_highlightValueFields[i]];

			if (useSeparator == true) {
				val=val.substr(1);
				// If there are are no value associated with the fields, ensure that
				// something like '::' is switched to just an empty string
				if (val.length == (_highlightValueFields.length - 1))
					val="";
			}

			return val;
		}


		/**
		 * Highlights the  highlighted assets as specified by the Highlight datasource
		 */
		public function initialHilite(highlightDSName:String=null, bordersHighlightDSName:String=null, useDefault:Boolean=false):void {
			var datasource:DataSource=null;

			_drawing.assetLayer.clearAssets();

			if (useDefault) {
				datasource=new DataSource(SourceType.HILITE);
				doRegularHighLight(datasource);
			} else {
				if (highlightDSName == null || highlightDSName == "") {
					highlightDSName="None";
				}

				if (isValidDataSourceName(highlightDSName)) {
					datasource=new DataSource(SourceType.HILITE, highlightDSName);
					doRegularHighLight(datasource, highlightDSName);
				}
			}

			if (isValidDataSourceName(bordersHighlightDSName)) {
				datasource=new DataSource(SourceType.HILITE, bordersHighlightDSName);
				doBordersHighlight(datasource, bordersHighlightDSName);
			}

		}

		/**
		 * Checks if client-side passes a valid datasource name.
		 */
		private function isValidDataSourceName(dataSourceName:String):Boolean {
			return (dataSourceName != null && dataSourceName.length > 0)
		}

		/**
		 * Does regular highlight.
		 */
		public function doRegularHighLight(datasource:DataSource, datasourceName:String=null,isClear:Boolean=true,  highlightFormat:HighlightFormat = null,keepPreviousFormat:Boolean=false):void {
			_usedValueColorList[datasourceName]=[];
			if(highlightFormat == null){
				highlightFormat = new HighlightFormat(HighlightFormat.REGULAR);
			}
			doHighLight(datasource, highlightFormat, datasourceName, isClear, keepPreviousFormat);
		}

		/**
		 * Does borders highlight.
		 */
		public function doBordersHighlight(datasource:DataSource, datasourceName:String=null,isClear:Boolean=true, highlightFormat:HighlightFormat = null):void {
			_usedValueColorList[datasourceName]=[];
			if(highlightFormat == null){
				highlightFormat = new HighlightFormat(HighlightFormat.BORDER);
			}
			doHighLight(datasource, highlightFormat, datasourceName, isClear);
		}

		/**
		 * Highlights regular and/or border.
		 */
		private function doHighLight(datasource:DataSource, highlightFormat:HighlightFormat, datasourceName:String=null, isClear:Boolean=true,keepPreviousFormat:Boolean=false):void {
			var rec:Object=null;
			var values:Array=[];
			var recs:ArrayCollection=new ArrayCollection();
			datasource.loadRecords(_drawing.location.getPrimarykeysArray());

			var cnt:int=datasource.numberOfRecords();
			_colorFieldName=datasource.getHPatternFieldName();
			_highlightValueFields=datasource.getHighlightLegendKeyFields();
			_highlightRuleValueField=ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_GETHIGHLIGHTRULEVALUEFIELD));

			var bCheckColor:Boolean=(_colorFieldName != null);
			var fillPropsPerKey:Object={};
			
			var af:FillFormat=new FillFormat(); 
			af = _drawing.assetLayer.calculateThicknessMultiplier(af);

			var afAssigned:FillFormat=null;
			var pac:ControlConfig=abDrawing(this._drawing.parentApplication).controlConfig;
			var nofillsEnabled:Boolean=pac.runtimeConfig.nofillsEnabled();
			var nofillsField:String=pac.runtimeConfig.nofillField;
			var usedValues:Array=[];
			_recMap={};

			var usedValueColorList:Array=[];

			if (!this._drawing.parentApplication.highlightType.isHighlightThematic())
				afAssigned=pac.highlightConfig.getFillFormat(FillTypes.ASSIGNED);

			for (var i:int=0; i < cnt; i++) {
				rec=datasource.getRecord(i);
				var key:Array=datasource.getPrimaryKeysValues(i);
				values[i]=key;
				recs.addItem(key);
				_recMap[key]=rec;


				if (nofillsEnabled && CommonUtilities.isValid(pac.runtimeConfig.nofillValues, rec[nofillsField])) {
					var afTmp:FillFormat=pac.runtimeConfig.getNofillAF(rec[nofillsField]);
					fillPropsPerKey[key]={};
					fillPropsPerKey[key].color=afTmp.disabled ? "-2" : afTmp.fillColor;
					fillPropsPerKey[key].opacity=afTmp.fillOpacity;
					fillPropsPerKey[key].boundaryColor=afTmp.boundaryColor;
					fillPropsPerKey[key].boundaryOpacity=afTmp.boundaryOpacity;
					fillPropsPerKey[key].boundaryThickness=afTmp.boundaryThickness;
					this._disabledIds[key]=true;
				} else if (afAssigned != null) {
					fillPropsPerKey[key]={};
					fillPropsPerKey[key].color=afAssigned.fillColor;
					fillPropsPerKey[key].opacity=afAssigned.fillOpacity;
					fillPropsPerKey[key].boundaryColor=afAssigned.boundaryColor;
					fillPropsPerKey[key].boundaryOpacity=afAssigned.boundaryOpacity;
					fillPropsPerKey[key].boundaryThickness=afAssigned.boundaryThickness;
				} else if (bCheckColor) {
					fillPropsPerKey[key]=getFillProperties(rec);
					fillPropsPerKey[key].opacity=pac.highlightConfig.getFillOpacity(FillTypes.ASSIGNED);
					fillPropsPerKey[key].boundaryColor=pac.highlightConfig.getBoundaryColor(FillTypes.ASSIGNED);
					fillPropsPerKey[key].boundaryOpacity=pac.highlightConfig.getBoundaryOpacity(FillTypes.ASSIGNED);
					fillPropsPerKey[key].boundaryThickness=pac.highlightConfig.getBoundaryThickness(FillTypes.ASSIGNED);

					// Build up a usedValueColroList, which is used by the legend functionality
					var val:String=getRecHighlightValue(rec, true); //rec[highlightValueField];	

					if (val != null && val.length && usedValues.indexOf(val) < 0) {
						usedValues.push(val);
						usedValueColorList.push({'value': val, 'color': fillPropsPerKey[key].color});
					}
				}



				if (highlightFormat.getHighlightFormat() == HighlightFormat.BORDER && datasourceName != "None") {
					//TODO: configurable boder's thickness???
					fillPropsPerKey[key].boundaryThickness = highlightFormat.getBorder();
					fillPropsPerKey[key].boundaryColor = fillPropsPerKey[key].color;
					fillPropsPerKey[key].boundaryOpacity = 1;
					af.highlightType = HighlightFormat.BORDER;
				}
			}
			var dsName:String=datasource.getDatasourceName();
			if (dsName == null) {
				dsName="default";
			}
			this._usedValueColorList[dsName]=usedValueColorList;

			drawing.assetLayer.drawAssets(recs, af, true, fillPropsPerKey, true, _recMap, highlightFormat,isClear, keepPreviousFormat);

			// this will only highlight a room if passed in to addDrawing
			if(this._drawing.config.highlightId!=null){
				var highlightIds:Array=String(this._drawing.config.highlightId).split(drawing.parentApplication.controlConfig.runtimeConfig.keyDelimiter) as Array;
				drawing.assetLayer.highlightRoom(highlightIds, highlightFormat.getHighlightFormat()); 
			}

			ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_ONHIGHLGHTSCHANGED));

			if (this._drawing.config) {
				this._drawing.highlightAssets(this._drawing.config, null, false);
			}

		}


		public function getUsedValueColorList(datasourceName:String=null):Array {
			if (datasourceName == null) {
				//first one as default
				for (var name:String in this._usedValueColorList) {
					return this._usedValueColorList[name];
				}
			}

			return this._usedValueColorList[datasourceName];
		}

		public function get highlightId():String {
			return _highlightId;
		}

		public function set highlightId(value:String):void {
			_highlightId=value;
		}


		private function clearDecodedPatterns():void {
			this._existingPatterns={};
		}

		public function set disabledIds(value:Object):void {
			_disabledIds=value;
		}

		public function isAssetDisabled(key:Array):Boolean {
			return this._disabledIds.hasOwnProperty(key);
		}


		public function get drawing():ArchibusDrawing {
			return _drawing;
		}


	}
}


