package src.view.util {

	import flash.system.Capabilities;

	public class Utilities {
		public function Utilities() {
		}

		/**
		 * Formats drag/drop records.
		 *
		 */
		public static function formatValues(draggedRecord:Object, roomIds:Array):Object {
			var record:Object={};
			for (var i:int=0; i < draggedRecord.length; i++) {
				var rec:Object=draggedRecord[i];
				var fieldFullName:String=rec.name;
				var fieldVaue:Object=rec.value;
				var fieldName:String=fieldFullName.split(".")[1];
				if (fieldName == "bl_id") {
					fieldVaue=roomIds[0];
				} else if (fieldName == "fl_id") {
					fieldVaue=roomIds[1];
				} else if (fieldName == "rm_id") {
					fieldVaue=roomIds[2];
				}
				record[fieldFullName]=fieldVaue;
			}
			return record;
		}

		/**
		 * Gets run-time flash player's major version number.
		 *
		 */
		public static function getFlashPlayerVersion():Number {
			var flashPlayerVersion:String=Capabilities.version;
			var osArray:Array=flashPlayerVersion.split(' ');
			var versionArray:Array=osArray[1].split(','); //The player versions. 9,0,115,0
			return parseInt(versionArray[0]);
		}
	}
}
