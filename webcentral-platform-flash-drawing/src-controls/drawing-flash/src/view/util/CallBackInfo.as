package src.view.util
{
	public class CallBackInfo{
		public var callBack:Function;
		public var methodArguments:Array;
		public function CallBackInfo(callBack:Function, methodArguments:Array){
			this.callBack = callBack;
			this.methodArguments = methodArguments;
		}
	}
}