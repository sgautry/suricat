package src.view.util
{
	import com.adobe.serialization.json.JSON;
	
	import flash.events.*;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.utils.*;
	import flash.utils.ByteArray;
	
	import src.model.config.DrawingConfig;
	import src.view.drawing.ArchibusDrawing;
	
	public class URLMultiLoader  extends EventDispatcher{
	
		private var urlLoaders:Dictionary; // key: URLLoader, value: URLRequest (same as above)
		public var data:Object = {};
		public var completedInfo:Dictionary;
		
		private var drawing:ArchibusDrawing;
		private var callbacks:Object;
		
		private var types:Array = [];
		
		public function URLMultiLoader(drawing:ArchibusDrawing){
			this.drawing = drawing;
			this.urlLoaders = new Dictionary();
			this.completedInfo = new Dictionary();
			this.callbacks = {};
			this.addEventListener(Event.COMPLETE, filesLoaded);
		}
		
		public function load():void {
			var tempURLRequest:URLRequest;
			for (var urlLoader:* in urlLoaders) {
				tempURLRequest = urlLoaders[urlLoader];
				urlLoader.load(tempURLRequest);
			}
		}
		
		public function addURLRequest(assetType:String, folder:String, planSuffix:String, assetSuffix:String):void {
			this.types.push(assetType);
		
			if(planSuffix != null && planSuffix.length > 0){
				planSuffix = "-" + planSuffix;
			}else{
				planSuffix = '';
			}
			var assetFileLocation:String = folder + "/" + drawing.location.getDrawingName();
			var assetFile:String = assetFileLocation + planSuffix + "-" + assetType + assetSuffix + ".json.zlib";
			var urlRequest:URLRequest = new URLRequest(assetFile);
			
			var tempURLLoader:URLLoader = new URLLoader();
			tempURLLoader.dataFormat = URLLoaderDataFormat.BINARY;
			tempURLLoader.addEventListener(Event.COMPLETE, onComplete);
			tempURLLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			this.urlLoaders[tempURLLoader] = urlRequest;
			this.completedInfo[urlRequest] = new URLInfo(assetType);
		}
		
		public function onIOError(event:Event):void {
			var urlLoader:* = event.target;
			try {
				urlLoader.close();
			} catch(event:Event) {}
			this.completedInfo[urlLoaders[urlLoader]].completed = true;
			this.loadingDone();
		}
		
		private function loadingDone():void{
			var done:Boolean = true;
			
			for (var urlLoader:* in urlLoaders) {
				var uRequest:URLRequest = urlLoaders[urlLoader];
				if(!this.completedInfo[uRequest].completed){
					done = false;
				}
			}
			
			if(done){
				dispatchEvent(new Event(flash.events.Event.COMPLETE));
			}
		}
		
		public function onComplete(event:Event):void {
			try{
				var compressedData:ByteArray=event.target.data as ByteArray;
				compressedData.uncompress();
				var rawData:String=compressedData.toString();
				
				var dataHeader:String=rawData.substr(0, 20);
				var newAssets:Array = new Array();
				if (dataHeader.indexOf("header") > 0) {
					var dataOb:Object=JSON.decode(rawData) as Object;
					newAssets = dataOb.assets;
				} else {
					newAssets = JSON.decode(rawData) as Array;
				}
				
				var tempURLRequest:URLRequest = urlLoaders[event.target];
				var info:URLInfo = this.completedInfo[tempURLRequest];
				info.completed = true;
				
				this.data[info.type] = newAssets;
				
				this.loadingDone();
			}catch (err:Error) {
				onIOError(event);
			}
		}
		
		public function addCallBack(type:String, callBack:Function, methodArguments:Array):void {
			this.callbacks[type] = new CallBackInfo(callBack, methodArguments);
		}
	
		private function filesLoaded(event:Event):void{
			if(isRequiredDelay()){
				delayedCalling();
			}else{
				normalCalling();
			}
		
			this.removeEventListener(Event.COMPLETE, filesLoaded);
		}
		
		private function delayedCalling():void{
			for(var type:String in this.data){
				//set background assets first like rm and su
				if(type == 'rm' || type == 'su'){
					var values:Array = this.data[type];
					this.drawing.assetLayer.setRoomAssets(values.length);
					
					if(this.drawing.assets){
						this.drawing.assets=this.drawing.assets.concat(values);
					} else {
						this.drawing.assets=values;
					}
				}
			}
			
			callLater(300);
		}
		
		private function normalCalling():void{
			for(var type:String in this.data){
				var values:Array = this.data[type];
				if(type == 'rm' || type == 'su'){
					this.drawing.assetLayer.setRoomAssets(values.length);
				}
				if(this.drawing.assets){
					this.drawing.assets=this.drawing.assets.concat(values);
				} else {
					this.drawing.assets=values;
				}
			}
			
			for(type in this.callbacks){
				var callBackInfo:CallBackInfo = this.callbacks[type];
				callBackInfo.callBack.apply(null, callBackInfo.methodArguments);
			}
			
		}
		
		private function isRequiredDelay():Boolean{
			return (this.types.length > 1 && this.types.indexOf("rm") >=0 ) || (this.types.length > 1 && this.types.indexOf("su") >= 0 );
		}
		
		private function callLater(milSeconds:int):void{
			var timer:Timer = new Timer(milSeconds); 
			timer.addEventListener(TimerEvent.TIMER, afterPeriodDelay);
			timer.start();
		}
		
		private function afterPeriodDelay(event:TimerEvent):void{
			var targetTimer:Timer = event.target as Timer;
			targetTimer.removeEventListener(TimerEvent.TIMER, afterPeriodDelay);
			targetTimer.stop();
			
			//set top assets like eq
			for(var type:String in this.data){
				if(type != 'rm' &&  type != 'su'){
					var values:Array = this.data[type];
					if(this.drawing.assets){
						this.drawing.assets=this.drawing.assets.concat(values);
					} else {
						this.drawing.assets=values;
					}
				}
			}
			
			for(type in this.callbacks){
				var callBackInfo:CallBackInfo = this.callbacks[type];
				callBackInfo.callBack.apply(null, callBackInfo.methodArguments);
			}
			
			targetTimer = null;
		}
	}
}