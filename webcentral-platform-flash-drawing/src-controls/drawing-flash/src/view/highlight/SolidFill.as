package src.view.highlight {
	import flash.display.Sprite;

	/**
	 * Solid fill implementation.
	 *
	 * Simply fills the asset with a color, using the graphics of the assetOb
	 */
	public class SolidFill extends Fill implements IFill {
		/**
		 * Constructor.
		 *
		 * @param component the component to draw the solid fill in.
		 * @param af fill properties.
		 */
		public function SolidFill(component:Sprite, af:FillFormat) {
			this._component=component;
			this._af=af;
		}

		public function beginFill(highlightFormat:HighlightFormat=null, doHighlighting:Boolean=true):void {
			initLineStyle();
			if (doHighlighting && (highlightFormat == null || (highlightFormat != null && highlightFormat.getHighlightFormat() == HighlightFormat.REGULAR))) {
				this._component.graphics.beginFill(this._af.fillColor, this._af.fillOpacity);
			}

		}

		public function endFill(highlightFormat:HighlightFormat=null, doHighlighting:Boolean=true):void {
			if (doHighlighting && (highlightFormat == null || (highlightFormat != null && highlightFormat.getHighlightFormat() == HighlightFormat.REGULAR))) {
				this._component.graphics.endFill();
			}
		}

		public function curveTo(controlX:Number, controlY:Number, anchorX:Number, anchorY:Number):void {
			this._component.graphics.curveTo(controlX, controlY, anchorX, anchorY);
		}

		public function lineTo(x:Number, y:Number):void {
			this._component.graphics.lineTo(x, y);
		}

		public function moveTo(x:Number, y:Number):void {
			this._component.graphics.moveTo(x, y);
		}

		public function clear():void {
			this._component.graphics.clear();
		}

		public function redraw():void {
			// does not need to be redrawn
			return;
		}
	}
}
