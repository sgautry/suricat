package src.view.highlight {

	/**
	 * Interface for the fill's basic functionality.
	 */
	public interface IFill {
		function beginFill(highlightFormat:HighlightFormat=null, doHighlighting:Boolean=true):void;

		function endFill(highlightFormat:HighlightFormat=null, doHighlighting:Boolean=true):void;

		function curveTo(controlX:Number, controlY:Number, anchorX:Number, anchorY:Number):void;

		function lineTo(x:Number, y:Number):void;

		function moveTo(x:Number, y:Number):void;

		function clear():void;

		function redraw():void;
	}
}
