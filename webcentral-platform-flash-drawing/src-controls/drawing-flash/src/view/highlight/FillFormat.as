package src.view.highlight {



	/**
	 * Defines supported visual attributes of higlight fill, either solid or hatched.
	 */
	public class FillFormat extends FillFormatBase {
		// flag to indicate the fill is not set from config object, but is determined internally.
		private var _disabled:Boolean=false;

		private var _patternName:String=null;

		private var _scale:Number=40;

		private var _angle:Number=0;

		private var _borderHatched:Boolean=false;
		
		private var _thicknessMultiplier:Number=5.0;

		/**
		 * Constructor
		 */

		public function FillFormat(id:String=null, cfgItem:Object=null) {
			super(id, cfgItem);

			if (cfgItem != null && cfgItem.fill != null && cfgItem.fill.color == -2)
				this._disabled=true;
		}


		public function get thicknessMultiplier():Number
		{
			return _thicknessMultiplier;
		}

		public function set thicknessMultiplier(value:Number):void
		{
			_thicknessMultiplier = value;
		}

		/**
		 * Copy Constructor.
		 *
		 * @param highlightFillFormat
		 */
		public function copy(highlightFillFormat:FillFormat):void {
			super.set(FillFormatBase(highlightFillFormat));

			this._disabled=highlightFillFormat._disabled;
			this._patternName=highlightFillFormat._patternName;
			this._angle=highlightFillFormat._angle;
			this._scale=highlightFillFormat._scale;
			this._thicknessMultiplier =highlightFillFormat._thicknessMultiplier;
		}

		/**
		 * Sets the fill format from the fill properties object.
		 *
		 * @param fillProps the specifying fill properties.
		 */
		public function setFromFillProps(fillProps:Object):void {
			if (fillProps == null)
				return;

			var sColor:String=fillProps.color;

			if (sColor == "-2") {
				this.disabled=true;
			} else if (sColor == FillTypes.NONE)
				this.fillOpacity=0.0;
			else if (uint(sColor) > 0)
				this.fillColor=uint(sColor);

			this.fillOpacity=fillProps.opacity;
			this.boundaryColor=fillProps.boundaryColor;
			this.boundaryOpacity=fillProps.boundaryOpacity;
			this.boundaryThickness=fillProps.boundaryThickness;

			if (fillProps.style == "HATCHED") {
				this.patternName=fillProps.patternName;
				this.angle=fillProps.angle;
				this.scale=fillProps.scale;
			}
		}

		public function toString():String {
			var s:String='{ id: ' + this._id.toString() + ', ' + 'pattern: ' + this._patternName + ', ' + 'angle: ' + this._angle + ', ' + 'scale: ' + this._scale + ', ' + 'fillColor: ' + this._fillColor.toString(16) + ', ' + 'fillOpacity: ' + this._fillOpacity.toString() + ', ' + 'boundaryColor: ' + this._boundaryColor.toString(16) + ', ' + 'boundaryOpacity: ' + this._boundaryOpacity.toString() + ', ' + 'boundaryThickness: ' + this._boundaryThickness + ', ' + 'disabled: ' + this._disabled.toString() + '}';

			return s;
		}

		public function get disabled():Boolean {
			return this._disabled;
		}

		public function get patternName():String {
			return this._patternName;
		}

		public function get angle():Number {
			return this._angle;
		}

		public function get scale():Number {
			return this._scale;
		}

		public function set disabled(b:Boolean):void {
			this._disabled=b;
		}

		public function set patternName(name:String):void {
			this._patternName=name;
		}

		public function set angle(a:Number):void {
			this._angle=a;
		}

		public function set scale(s:Number):void {
			this._scale=s;
		}

		public function getborderHatched():Boolean {
			return this._borderHatched;
		}

		public function setborderHatched(borderHatched:Boolean=false):void {
			this._borderHatched=borderHatched;
		}

	}
}
