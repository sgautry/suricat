package src.view.highlight {
	import com.adobe.serialization.json.JSON;

	import flash.external.ExternalInterface;

	import src.model.util.CommonUtilities;
	import src.model.util.JsFunctionConstant;

	/**
	 * the hatch patterns from the hpattern.properties file.
	 */
	public class HatchPatterns {
		//has the hatch pattern from hpatterns.properties files been restrieved once?
		private static var _initialized:Boolean=false;

		// store the hatch patterns in a map
		private static var patterns:Object={};

		/**
		 * Retrieve the pattern data from the list for the specifying name. use ANOPAT as default.
		 *
		 * @param name the pattern name
		 * @return pattern data array.
		 */
		public static function getPattern(name:String):HatchPattern {

			if (!_initialized) {
				var jsonPatterns:String=ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_GETHATCHPATTERNS));
				if (jsonPatterns != null) {
					patterns={};
					var patternsArray:Array=JSON.decode(jsonPatterns) as Array;

					//loop through to add each hatch pattern from hpattern.properties file.
					for (var index:int=0; index < patternsArray.length; index++) {
						var pattern:Object=JSON.decode(patternsArray[index]) as Object;
						var hatchPattern:HatchPattern=new HatchPattern(pattern.name, pattern.scale, pattern.angle);
						for (var indexLines:int=0; indexLines < pattern.lines.length; indexLines++) {
							hatchPattern.addLine(pattern.lines[indexLines].x, pattern.lines[indexLines].y, pattern.lines[indexLines].angle, pattern.lines[indexLines].offsetX, pattern.lines[indexLines].offsetY, pattern.lines[indexLines].dashPatterns);
						}
						patterns[pattern.name]=hatchPattern;
					}
					_initialized=true;
				}
			}

			return patterns[name] != null ? patterns[name] : patterns.ANOPAT;
		}
	}
}
