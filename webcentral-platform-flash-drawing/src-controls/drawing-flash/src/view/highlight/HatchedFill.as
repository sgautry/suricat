package src.view.highlight {
	import flash.display.BitmapData;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.setTimeout;

	import mx.events.MoveEvent;

	/**
	 * The current fill implementation for hatch patterns.
	 *
	 * It draws the hatch pattern on a component outside the display list, draws this component on an
	 * internal bitmap, and then fills the asset with this bitmap.
	 */
	public class HatchedFill extends Fill implements IFill {
		/**
		 * Scale is adapted by this factor to achieve similar scaling as in reports
		 * (determined by experimentation)
		 *
		 * Change this value if you want all patterns to be drawn a bit bigger or smaller.
		 */
		private static const SCALE_FACTOR:Number=0.23;

		/**
		 * contains all vertices and lines of the polygon.
		 */
		private var _drawData:Array;

		/**
		 * the bounds cached as field so we do not have it recomputed on redraws.
		 */
		private var _bounds:Rectangle;

		/**
		 * draw area.
		 */
		private var _drawArea:Shape=new Shape();

		/**
		 * The current viewscale the hatch pattern is drawn in
		 */
		private var _viewScale:Point=null;

		/**
		 * The bitmap the asset is filled with. Initialised with one white pixel, to have the asset
		 * drawn very quickly (this is needed for correct label positionining).
		 * with transparent 
		 */
		private var _bitmap:BitmapData=new BitmapData(1, 1, true, 0);

		public function HatchedFill(comp:Sprite, af:FillFormat) {
			this._component=comp;
			this._af=af;
		}

		/**
		 * clear the draw data and fill.
		 */
		public function beginFill(highlightFormat:HighlightFormat=null, doHighlighting:Boolean=true):void {
			this._drawData=[];

			// fill the drawarea with a dummy color (just used to calculate the bounds of the asset)
			if (highlightFormat == null || (highlightFormat != null && highlightFormat.getHighlightFormat() == HighlightFormat.REGULAR)) {
				this._drawArea.graphics.beginFill(0, 1);
			}
		}

		/**
		 *
		 * @param controlX
		 * @param controlY
		 * @param anchorX
		 * @param anchorY
		 */
		public function curveTo(controlX:Number, controlY:Number, anchorX:Number, anchorY:Number):void {
			this._drawData.push({type: 'c', cx: controlX, cy: controlY, ax: anchorX, ay: anchorY});

			this._drawArea.graphics.curveTo(controlX, controlY, anchorX, anchorY);
		}

		/**
		 *
		 * @param x
		 * @param y
		 */
		public function lineTo(x:Number, y:Number):void {
			this._drawData.push({type: 'l', x: x, y: y});

			this._drawArea.graphics.lineTo(x, y);
		}

		/**
		 *
		 * @param x
		 * @param y
		 */
		public function moveTo(x:Number, y:Number):void {
			this._drawData.push({type: 'm', x: x, y: y});

			this._drawArea.graphics.moveTo(x, y);
		}

		/**
		 *
		 */
		public function clear():void {
			this._drawData=[];
			this._component.graphics.clear();
			this._drawArea.graphics.clear();
			this._bitmap.dispose();
		}

		/**
		 *
		 */
		public function endFill(highlightFormat:HighlightFormat=null, doHighlighting:Boolean=true):void {
			this._drawArea.graphics.endFill();
			this._bounds=this._drawArea.getBounds(this._drawArea);

			this.draw();
		}

		/**
		 *
		 */
		private function draw():void {
			// update viewScale (i.e. the scale factor of the zoom)			
			if (this._viewScale == null) {
				this._viewScale=new Point(this._component.parent.scaleX, this._component.parent.scaleY);
			} else {
				this._viewScale.x=this._component.parent.scaleX;
				this._viewScale.y=this._component.parent.scaleY;
			}

			// quickly fill the asset with the current bitmap.
			//this.fillWithBitmap();

			// schedule call to drawHatches, if this is finished, the asset will be re-filled with the
			// correct bitmap.
			drawHatches();
		}

		/**
		 *
		 */
		private function drawHatches():void {
			// dimension of bitmap to create (size of bounds in screen coordinates)
			var dim:Point=new Point(this._bounds.width * this._viewScale.x, this._bounds.height * this._viewScale.y);
			if (dim.x < 1 || dim.y < 1) { // asset smaller than a pixel on the screen -> not drawing
				return;
			}

			// optimisation: dimension of bitmap reduced to screen size (or 2880, which is the biggest 
			// a bitmap can be in flex)
			// if the asset is bigger, the bitmap will be repeated
			dim.x=Math.min(dim.x, Math.min(this._component.stage.stageWidth, 2880));
			dim.y=Math.min(dim.y, Math.min(this._component.stage.stageHeight, 2880));


			// clear graphics before drawing new ones on it
			this._drawArea.graphics.clear();

			// transformations (flip y-axis, perform rotations & scaling and move to the center of the 
			// drawing box)
			// don't set this matrix as the drawArea's transform.matrix, this causes precision problems. Instead we're transformations the points manually before drawing
			var scale:Number=this._af.scale * SCALE_FACTOR;
			var transform:Matrix=new Matrix();
			transform.d=-transform.d;
			transform.rotate(-this._af.angle * Math.PI / 180);
			transform.scale(scale, scale);
			transform.translate(dim.x / 2.0, dim.y / 2.0);

			// radius of circle that needs to be covered by the pattern (this is divided by the scale, 
			// since scaling the pattern does not mean enlarging the drawing zone)
			var radius:Number=Point.distance(new Point(0, 0), dim) / 2 / scale;

			// draw hatch lines
			var pattern:HatchPattern=HatchPatterns.getPattern(this._af.patternName);
			this._drawArea.graphics.lineStyle(1, this._af.fillColor, this._af.fillOpacity);
			this._drawArea.graphics.beginFill(this._af.fillColor, 0);
			for each (var hatchLine:HatchLine in pattern.lines) {
				hatchLine.draw(this._drawArea.graphics, radius, transform);
			}
			this._drawArea.graphics.endFill();

			// convert drawn pattern into an internal bitmap with transparent 
			this._bitmap=new BitmapData(dim.x, dim.y, true, 0 );
			this._bitmap.draw(this._drawArea);

			// re-fill asset with correct bitmap
			this.fillWithBitmap();
		}

		/**
		 * set the scale, draw the assets and fill them with bitmap.
		 */
		private function fillWithBitmap():void {
			// calculate transformation for bitmap
			//	first undo all transformations, except translations, that will be applied by the component it is 
			//	drawn upon, by taking the inverse of the component transformation matrix and removing the 
			//	translations from this. In this case, this comes down to just the inverse scaling 
			//	of the zoom.
			//	Then add a translation to move the bitmap to the position of the bounds, so it is drawn there 
			//	where the asset is
			var moveBitmap:Matrix=new Matrix();
			moveBitmap.scale(1 / this._viewScale.x, 1 / this._viewScale.y);
			moveBitmap.translate(this._bounds.x, this._bounds.y);

			// draw the asset
			initLineStyle();

			//filling it with the bitmap.
			this._component.graphics.beginBitmapFill(this._bitmap, moveBitmap, true, true); // smoothing needed for isometric drawing
			for each (var d:Object in this._drawData) {
				switch (d.type) {
					case 'c':
						this._component.graphics.curveTo(d.cx, d.cy, d.ax, d.ay);
						break;
					case 'l':
						this._component.graphics.lineTo(d.x, d.y);
						break;
					case 'm':
						this._component.graphics.moveTo(d.x, d.y);
						break;
				}
			}
			this._component.graphics.endFill();
		}


		/**
		 * Redraw if the scale has been changed AND the drawing is loaded.
		 */
		public function redraw():void {
			// first check if the view scale (i.e. zoom factor) has been changed. If not, we do not need
			// to redraw, since it is currently drawn at this view scale.
			if (this._viewScale != null && this._viewScale.x == this._component.parent.scaleX && this._viewScale.y == this._component.parent.scaleY) {
				return;
			}

			// then check if the component is currently into view, if not, skip it.
			var tl:Point=this._component.localToGlobal(this._bounds.topLeft);
			var br:Point=this._component.localToGlobal(this._bounds.bottomRight);
			if (br.x < 0 || tl.x > this._component.stage.stageWidth || br.y < 0 || tl.y > this._component.stage.stageHeight) {
				return;
			}
			
			this._bitmap.dispose();
			draw();
		}
	}
}
