package src.view.highlight {

	/**
	 * fill format with the override boundary color flag and oberride boundary thickness flag.
	 */
	public class FillFormatOverride extends FillFormat {
		public var overrideBoundaryColor:Boolean=false;
		public var overrideBoundaryThickness:Boolean=false;

		/**
		 * Overrides the boundary thickness and turn the flag on
		 * @param value the value to set
		 */
		public function setBoundaryThickness(value:Number):void {
			this.boundaryThickness=value;
			this.overrideBoundaryThickness=true;
		}

		/**
		 * Overrides the boundary color and turn the flag on
		 * @param color the color to set
		 */
		public function setBoundaryColor(color:uint):void {
			this.boundaryColor=color;
			this.overrideBoundaryColor=true;
		}
	}
}
