package src.view.highlight {
	import flash.display.Graphics;
	import flash.geom.Matrix;
	import flash.geom.Point;


	/**
	 * Represents a single hatch lines.
	 */
	public class HatchLine {

		private var _angle:Number;
		private var _start:Point;
		private var _offset:Point;
		private var _dashPattern:Array;

		/**
		 * Constructor.
		 *
		 * @param x the start point's X position
	   * @param y the start point's Y position
			   * @param angle the angel
		 * @param offsetX the X offset
		 * @param offsetY the Y offset
		 * @param dashPattern the array to records the dashes.
		 */
		public function HatchLine(x:Number, y:Number, angle:Number, offsetX:Number, offsetY:Number, dashPattern:Array) {
			this._start=new Point(x, y);
			this._angle=angle;
			this._offset=new Point(offsetX, offsetY);
			this._dashPattern=dashPattern;
		}

		public function get angle():Number {
			return this._angle;
		}

		public function get x():Number {
			return this._start.x;
		}

		public function get y():Number {
			return this._start.y;
		}

		public function get offset():Point {
			return this._offset;
		}

		/**
		 * Draws this hatch line repeatedly covering the drawing zone, which is a circle around the
		 * origing with the given radius.
		 */
		public function draw(graphics:Graphics, radius:Number, parentTransform:Matrix):void {
			// transformation
			var transform:Matrix=new Matrix();
			transform.rotate(this._angle * Math.PI / 180);
			transform.translate(this._start.x, this._start.y);

			// This transformation will move the drawing zone (the circle around the center of the
			// bounds). To make sure the drawing zone still overlaps the full room shape, add
			// to the radius the distance from the draw center (the center of the bounds, i.e. 0,0) to the
			// transformed draw center.
			var pnt:Point=new Point(0, 0);
			radius+=Point.distance(pnt, transform.transformPoint(pnt));

			// add parent transformation to transformation
			transform.concat(parentTransform);

			// start drawing from 0,0
			while (pnt.y < radius) {
				drawDashPattern(pnt.x, pnt.y, graphics, radius, transform);
				if (pnt.y != 0) { // also draw negative side on y-axis
					drawDashPattern(-pnt.x, -pnt.y, graphics, radius, transform);
				}

				pnt.x+=this._offset.x;
				pnt.y+=this._offset.y;
			}
		}

		/**
		 * Draws the dash pattern of this hatch line repeatedly at the given y-coordinate.
		 */
		private function drawDashPattern(startX:Number, y:Number, graphics:Graphics, radius:Number, transform:Matrix):void {
			// if there is no dash pattern, draw a full line
			if (this._dashPattern.length == 0) {
				drawLine(startX - radius, y, startX + radius, y, transform, graphics);
				return;
			}

			// calculate length of dash pattern.
			var length:Number=0;
			for each (var dash:Number in this._dashPattern) {
				length+=Math.abs(dash);
			}


			// first draw from startX to the positive side until end of draw region (radius)

			// optimization: if the x-offset is very large, it can happen that the first dash 
			// pattern drawn, is drawn outside the drawing area. To avoid these extra lines, we're adding
			// the length to the x coordinate until this is no longer the case.
			var x:Number=startX;
			while (x + length < -radius) {
				x+=length;
			}
			var i:Number=0;
			while (x <= radius) {
				if (this._dashPattern[i] < 0) {
					x-=this._dashPattern[i];
				} else {
					drawLine(x, y, x+=this._dashPattern[i], y, transform, graphics);
				}
				if (++i == this._dashPattern.length)
					i=0;
			}

			// then draw from startX to the negative side until the end of the draw region.

			// similar optimization first
			x=startX;
			while (x - length > radius) {
				x-=length;
			}
			i=this._dashPattern.length - 1;
			while (x >= -radius) {
				if (this._dashPattern[i] < 0) {
					x+=this._dashPattern[i];
				} else {
					drawLine(x, y, x-=this._dashPattern[i], y, transform, graphics);
				}
				if (--i < 0)
					i=this._dashPattern.length - 1;
			}
		}


		/**
		 *	Draws a line on the given graphics from point (x1,y1) to (x2,y2), using the given transformation
		 */
		private function drawLine(x1:Number, y1:Number, x2:Number, y2:Number, transform:Matrix, graphics:Graphics):void {
			// transfrom points
			var start:Point=transform.transformPoint(new Point(x1, y1));
			var end:Point=transform.transformPoint(new Point(x2, y2));

			//draw line
			graphics.moveTo(start.x, start.y);
			graphics.lineTo(end.x, end.y);
		}
	}

}
