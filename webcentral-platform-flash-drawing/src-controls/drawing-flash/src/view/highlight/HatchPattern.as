package src.view.highlight {

	/**
	 * Defines the hatch pattern properties.
	 *
	 * @author Qin
	 */
	public class HatchPattern {
		private var _name:String='ANOPAT';

		private var _scale:Number=120;

		private var _angle:Number=0;

		private var _lines:Array=new Array();

		/**
		 * Constructor.
		 *
		 * @param name the hatch pattern name
		 * @param scale the scale
		 * @param angle the angle
		 */
		public function HatchPattern(name:String, scale:Number, angle:Number) {
			this._name=name;
			this._scale=scale;
			this._angle=angle;
			this._lines.length=0;
		}


		/**
		 * Retrieves the hatch lines.
		 *
		 * @return an array of hatch lines.
		 */
		public function get lines():Array {
			return _lines;
		}

		/**
		 * Adds a hatch line.
		 *
		 * @param x starting x to draw the line
		 * @param y starting y to draw the line
		 * @param angle the angle of the line
		 * @param offsetX the offset from the starting x
		 * @param offsetY the offset from the starting y
		 * @param dashPattern an array of dash patterns.
		 */
		public function addLine(x:Number, y:Number, angle:Number, offsetX:Number, offsetY:Number, dashPattern:Array):void {
			this._lines[this._lines.length]=new HatchLine(x, y, angle, offsetX, offsetY, dashPattern);
		}
	}
}
