package src.view.highlight {
	import flash.display.*;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.geom.*;

	import mx.core.UIComponent;

	import src.view.util.Utilities;

	/**
	 * Represents fills for assets. Can be implemented for solid fills, hatched fills, ...
	 * This has similar interface as flash.display.Graphics to draw the assets.
	 *
	 */
	public class Fill {
		public static var BORDERTHICKNESS:Number=10;

		public static var thicknessMultiplierInitialized:Boolean=false;

		public static var fillFormatOverride:FillFormatOverride=new FillFormatOverride();

		protected var _component:Sprite;

		protected var _af:FillFormat;

		[Embed(source='assets/img/drawing/hatch.png')]
		public static var CHatchLine:Class;



		/**
		 * Initializes the component's line style.
		 */
		public function initLineStyle():void {
			this._component.graphics.lineStyle(Fill.fillFormatOverride.overrideBoundaryThickness ? Fill.fillFormatOverride.boundaryThickness : this._af.boundaryThickness * this._af.thicknessMultiplier, Fill.fillFormatOverride.overrideBoundaryColor ? Fill.fillFormatOverride.boundaryColor : this._af.boundaryColor, this._af.boundaryOpacity);
			if (this._af.getborderHatched()) {
				if (src.view.util.Utilities.getFlashPlayerVersion() >= 10) {
					drawHatchedBorder();
				}
			}
		}

		/**
		 * Draws Hatched Border for room selection.
		 */
		private function drawHatchedBorder():void {
			var image:Bitmap=new CHatchLine();

			var bitmapData:BitmapData=image.bitmapData;
			//XXX: only transform image color for border highlight cases
			if (this._af.boundaryThickness == BORDERTHICKNESS) {
				for (var j:int=0; j < image.height; j++) {
					for (var i:int=0; i < image.width; i++) {
						//XXX: image's background color
						if (bitmapData.getPixel(i, j) == 0xFFFFFF)
							bitmapData.setPixel(i, j, this._af.boundaryColor);
					}
				}
			}

			this._component.graphics.lineBitmapStyle(bitmapData);
		}

	}
}


