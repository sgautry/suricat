package src.view.highlight {

	/**
	 * Defines different highlight fill types as defined in controls-drawing.xml file
	 *
	 */
	public class FillTypes {
		/**
		 * none fill type
		 */
		public static const NONE:String='none';

		/**
		 * assigned fill type
		 */
		public static const ASSIGNED:String='assigned';

		/**
		 * unassigned fill type
		 */
		public static const UNASSIGNED:String='unassigned';

		/**
		 * selected fill type
		 */
		public static const SELECTED:String='selected';

		/**
		 * unselected fill type
		 */
		public static const UNSELECTED:String='unselected';

		/**
		 * nofill fill type
		 */
		public static const NOFILL:String='nofill';

		/**
		 * default fill type - is not maintained by the config file
		 */
		public static const DEFAULT:String='default';

	}
}
