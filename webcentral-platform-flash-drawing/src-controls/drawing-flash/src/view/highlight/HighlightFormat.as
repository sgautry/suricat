package src.view.highlight {

	/**
	 * Enum for rooms or borders highlight.
	 */
	public class HighlightFormat extends Object {
		public static const REGULAR:String="regular";
		public static const BORDER:String="border";

		private var _format:String=REGULAR;
		
		private var _border:Number = 20;
		
		private var _assetType:String;
		
		public function setAssetType(type:String):void{
			_assetType = type;
		}
		
		public function getAssetType():String{
			return _assetType;
		}

		public function HighlightFormat(format:String) {
			super();
			_border = src.view.highlight.Fill.BORDERTHICKNESS;
			_format=format;
		}

		public function getHighlightFormat():String {
			return _format;
		}
		
		public function setBorder(newBorder:Number):void{
			_border = newBorder;
		}
		
		public function getBorder():Number{
			return _border;
		}
	}
}
