package src.view.highlight {
	import src.model.util.CommonUtilities;

	/**
	 * Defines supported visual attributes of a solid fill, i.e. fill color, opacity, boundary color. opacity and thickness.
	 */
	public class FillFormatBase {
		private static const ID:String='id';

		private static const FILLCOLOR:String='fc';

		private static const FILLOPACITY:String='fo';

		private static const BOUNDARYCOLOR:String='bc';

		private static const BOUNDARYOPACITY:String='bo';

		private static const BOUNDARYTHICKNESS:String='bt';

		protected var _id:String='';

		protected var _fillColor:uint=0xdddddd;

		protected var _fillOpacity:Number=0.7;

		protected var _boundaryColor:uint=0x000000;

		protected var _boundaryOpacity:Number=1.0;

		protected var _boundaryThickness:Number=3;

		private var _highlightType:String=HighlightFormat.REGULAR;

		/**
		 * Constructor
		 */
		public function FillFormatBase(id:String=null, cfgItem:Object=null) {
			if (id != null)
				this._id=id;
			if (cfgItem == null)
				return;

			if (cfgItem.fill != null) {
				this._fillColor=uint(CommonUtilities.toHex(cfgItem.fill.color));
				this._fillOpacity=cfgItem.fill.opacity;
			}

			if (cfgItem.border != null) {
				this._boundaryColor=cfgItem.border.color;
				this._boundaryOpacity=cfgItem.border.opacity;
				this._boundaryThickness=cfgItem.border.thickness;
			}
		}

		/**
		 * Sets the properties from the specifying fill format base.
		 *
		 * @param af an FillFormatBase object.
		 */
		public function set(af:FillFormatBase):void {
			this._id=af.id;
			this._fillColor=af._fillColor;
			this._fillOpacity=af._fillOpacity;
			this._boundaryColor=af._boundaryColor;
			this._boundaryOpacity=af._boundaryOpacity;
			this._boundaryThickness=af._boundaryThickness;
			this._highlightType=af._highlightType;
		}

		/**
		 * Sets the properties from the drawing control's fill options.
		 * @param fill the fill object
		 */
		public function setFromDwgOptions(fill:Object):void {
			if (!fill)
				return;

			if (CommonUtilities.isValid(fill, FILLCOLOR) && fill[FILLCOLOR] >= 0)
				this._fillColor=fill[FILLCOLOR];
			if (CommonUtilities.isValid(fill, FILLOPACITY))
				this._fillOpacity=fill[FILLOPACITY];
			if (CommonUtilities.isValid(fill, BOUNDARYCOLOR) && fill[BOUNDARYCOLOR] >= 0)
				this._boundaryColor=fill[BOUNDARYCOLOR];
			if (CommonUtilities.isValid(fill, BOUNDARYOPACITY))
				this._boundaryOpacity=fill[BOUNDARYOPACITY];
			if (CommonUtilities.isValid(fill, BOUNDARYTHICKNESS))
				this._boundaryThickness=fill[BOUNDARYTHICKNESS];
		}

		public function get id():String {
			return this._id;
		}

		public function get fillColor():uint {
			return this._fillColor;
		}

		public function get fillOpacity():Number {
			return this._fillOpacity;
		}

		public function get boundaryColor():uint {
			return this._boundaryColor;
		}

		public function get boundaryOpacity():Number {
			return this._boundaryOpacity;
		}

		public function get boundaryThickness():Number {
			return this._boundaryThickness;
		}

		public function set id(id:String):void {
			this._id=id;
		}

		public function set fillColor(c:uint):void {
			this._fillColor=c;
		}

		public function set fillOpacity(o:Number):void {
			this._fillOpacity=o;
		}

		public function set boundaryColor(c:uint):void {
			this._boundaryColor=c;
		}

		public function set boundaryOpacity(o:Number):void {
			this._boundaryOpacity=o;
		}

		public function set boundaryThickness(th:Number):void {
			this._boundaryThickness=th;
		}

		public function get highlightType():String {
			return _highlightType;
		}

		public function set highlightType(value:String):void {
			_highlightType = value;
		}


	}
}


