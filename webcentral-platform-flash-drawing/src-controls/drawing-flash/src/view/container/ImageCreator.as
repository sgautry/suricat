package src.view.container {
	import com.adobe.images.JPGEncoder;
	import com.adobe.images.PNGEncoder;

	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import flash.utils.ByteArray;

	import mx.core.UIComponent;

	import src.view.drawing.ArchibusDrawing;

	/**
	 * Component to create PNG or JPG images.
	 */
	public class ImageCreator extends UIComponent {

		/**
		 * max bytes allowed
		 */
		private var _max:int=62000;

		/**
		 * stores the bit array data with the max value for the bitmap data.
		 */
		private var _arr:Array=null;

		/**
		 * file type, can be PNG or JPG, defaults to PNG
		 */
		private var _filetype:String="png";

		/**
		 * The quality level between 1 and 100 that detrmines the level of compression used in the generated image
		 */
		private var _quality:int=50;

		/**
		 *
		 * @param filetype file type, can be 'png' or 'jpg', defaults to 'png'. Lower case.
		 * @param quality the quality level between 1 and 100
		 * @param doAll apply to all drawings
		 *
		 * @return an array that holds image information such as size and maximum bytes allowed.
		 */
		public function createImage(filetype:String="", quality:int=-1, doAll:Boolean=true):Array {
			if (filetype.length)
				this._filetype=filetype.toLowerCase();
			if (quality > 0)
				this._quality=quality;


			var w:int=0;
			var h:int=0;
			var mult:Number=3.0;
			var pa:abDrawing=this.parentApplication as abDrawing;
			var ad:ArchibusDrawing=null;

			if (doAll) {
				w=pa.width * mult;
				h=pa.height * mult
			} else {
				ad=pa.dwgContainer.getChildAt(0) as ArchibusDrawing;
				if (ad != null) {
					w=ad.width * mult;
					h=ad.height * mult;
				}
			}

			if (w > 2880 || h > 2880) {
				var tmp1:Number=Math.max(w, h);
				var tmp2:Number=2880 / tmp1;
				mult*=tmp2;
				w*=tmp2;
				h*=tmp2;
			}

			var bd:BitmapData=new BitmapData(w, h);
			var m:Matrix=new Matrix();
			m.scale(mult, mult);

			if (doAll) {
				// for some reason, settnig the borderStyle to 'none' does not
				// prevent the border from still being included in the image
				pa.dwgContainer.setStyle("borderStyle", "none");
				pa.navCanvas.visible = false;
				this.parentApplication.toolbar("show", false, null);
				bd.draw(pa, m, null, null);
				pa.dwgContainer.setStyle("borderStyle", "solid");
				this.parentApplication.toolbar("show", true, null);
				pa.navCanvas.visible = true;
			} else if (ad != null)
				bd.draw(ad, m, null, null);

			_arr=[];
			var ba:ByteArray=null;

			if (this._filetype == 'png') {
				ba=PNGEncoder.encode(bd);
			} else {
				var jpg:JPGEncoder=new JPGEncoder(this._quality);
				ba=jpg.encode(bd);
			}

			for (var i:Number=0; i < ba.length; i++) {
				_arr[i]=ba[i];
			}

			var imageInfo:Array=[];
			imageInfo[0]=(_arr != null) ? _arr.length : 0
			imageInfo[1]=_max;
			return imageInfo;
		}

		/**
		 * Retrieves the bytes array from the specifying position to the max bytes allowed for the image.
		 *
		 * @param start the starting position.
		 * @return image byte array.
		 */
		public function getImageBytes(start:int):Array {
			if (_arr == null || !_arr.length)
				return null;

			var i:int=start;
			var j:int=0;
			var size:int=_arr.length;
			var arr:Array=[];

			for (; i < size && j < _max; i++, j++)
				arr[j]=_arr[i];

			if (i == size)
				_arr=null;

			return arr;
		}

	}

}
