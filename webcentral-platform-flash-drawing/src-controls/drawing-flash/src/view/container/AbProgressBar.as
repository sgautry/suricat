package src.view.container {
	import mx.controls.ProgressBar;

	/**
	 * Defines the progress bar displayed while loading the drawing.
	 *
	 */
	public class AbProgressBar extends ProgressBar {
		/**
		 * Default constructor.
		 *
		 * @param width the width of the drawing.
		 * @param height the height of the drawing.
		 * @param name the drawing name.
		 */
		public function AbProgressBar(width:Number, height:Number, name:String) {
			super();

			this.id="pbar";
			this.indeterminate=true;
			this.x=this.width / 2 - 100;
			this.y=this.height / 2;
			this.label="Loading: " + name;
			this.visible=true;
		}
	}
}
