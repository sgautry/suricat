package src.view.text {
	import flash.events.*;
	import flash.external.ExternalInterface;
	import flash.geom.Point;
	import flash.text.*;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Image;
	import mx.core.DragSource;
	import mx.core.UIComponent;
	import mx.core.UITextField;
	import mx.events.DragEvent;
	import mx.managers.CursorManager;
	import mx.managers.DragManager;
	
	import src.model.config.ControlConfig;
	import src.model.config.LabelConfig;
	import src.model.text.AbTextFormat;
	import src.model.util.*;
	import src.view.asset.AssetLayer;
	import src.view.asset.AssetObject;
	import src.view.drawing.ArchibusDrawing;
	import src.view.highlight.HighlightFormat;
	import src.view.util.Utilities;

	/**
	 * Archibus Text Field.
	 */
	public class AbTextField extends UIComponent {
		public var field:String="";
		private var textField:TextField;
		private var textFormat:TextFormat;
		private var tableName:String;


		/**
		 * Fields with a lower priority may be hidden faster then fields with high priority.
		 */
		public var priority:uint=0;

		private var record:Object;

		//TODO: 
		[Embed(source='assets/img/drawing/pion.png')]
		static public var manImage:Class;

		[Embed(source='assets/img/drawing/asset.gif')]
		static public var man2Image:Class;



		/**
		 * Constructor.
		 *
		 * @param field the field name for the text.
		 * @param priority the priority which used to decide if the text will be shown on the asset layer.
		 * @param txt the text to set.
		 * @param textFormat the text format to set.
		 * @param isSelectable true if the text is selectable, false otherwise.
		 */
		public function AbTextField(field:String, priority:uint, txt:String, textFormat:TextFormat, isSelectable:Boolean, record:Object) {
			super();
			textField=new TextField();
			this.field=field;
			this.priority=priority;
			textField.text=txt;

			this.textFormat=textFormat;
			textField.setTextFormat(textFormat);

			textField.selectable=isSelectable;
			textField.autoSize=TextFieldAutoSize.CENTER;
			textField.antiAliasType=AntiAliasType.ADVANCED;
			textField.condenseWhite=true;

			this.record=record;

			//propagate asset drop events
			addEventListener(DragEvent.DRAG_ENTER, propagateDropStart);
			addEventListener(DragEvent.DRAG_DROP, propagateDropEnd);

			if (field != null) {
				tableName=field.substring(0, field.indexOf("."));
				//TODO: 
				if (tableName == "em") { // || tableName == "eq"  || tableName == "fn"){
					addEventListener(MouseEvent.MOUSE_DOWN, startDragging);
					addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
					addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
				} else {
					addEventListener(MouseEvent.MOUSE_OVER, propagationClick);
				}

			}

			addChild(textField);

		}
		private function propagateDropStart(e:DragEvent):void {
			var assetObj:AssetObject=getAssetObject();
			if (assetObj) {
				assetObj.dragEnterHandler(e);
			}
		}

		private function propagateDropEnd(e:DragEvent):void {
			var assetObj:AssetObject=getAssetObject();
			if (assetObj) {
				assetObj.dragDropHandler(e);
			}
		}

		private function getAssetObject():AssetObject {
			var lObject:LabelObject=this.parent as LabelObject;
			if (lObject) {
				var ad:ArchibusDrawing=lObject.parent.parent as ArchibusDrawing;
				var assetObj:AssetObject=ad.assetLayer.getAssetOb(lObject.ids);
				return assetObj;
			}

			return null;
		}

		private function getCurrentDrawing():ArchibusDrawing {
			var lObject:LabelObject=this.parent as LabelObject;
			if (lObject) {
				return lObject.parent.parent as ArchibusDrawing;
			}
			return null;
		}

		public function setText(text:String):void {
			textField.text=text;
		}

		public function getText():String {
			return textField.text;
		}

		public function getTextField():TextField {
			return textField;
		}

		/**
		 * no-draggable label but propagate its default click event
		 */
		private function propagationClick(e:MouseEvent):void {
			this.buttonMode=true;
			this.useHandCursor=false;
			this.mouseChildren=false;
			textField.setTextFormat(this.textFormat);
		}

		/**
		 * draggable label objects, change UI display
		 */
		private function onMouseOver(e:MouseEvent):void {
			this.buttonMode=true;
			this.useHandCursor=true;
			this.mouseChildren=false;
			this.textFormat.underline=true;
			this.textFormat.bold=true;
			this.textFormat.italic=true;
			this.textFormat.color=0xFF0000;

			textField.setTextFormat(this.textFormat);
		}

		/**
		 * restore label's display into previous status
		 */
		private function onMouseOut(e:MouseEvent):void {
			CursorManager.removeAllCursors();
			this.textFormat.underline=false;
			this.textFormat.bold=false;
			this.textFormat.color=0x000000;
			this.textFormat.italic=false;

			textField.setTextFormat(this.textFormat);
		}

		/**
		 *startDragging.
		 *
		 */
		private function startDragging(e:MouseEvent):void {
			//XXX: set panning position object to null
			var drawingControl:abDrawing = (this.parentApplication as abDrawing);
			if(drawingControl){
				drawingControl.reset();	
			}
			var archibusDrawing:ArchibusDrawing = this.getCurrentDrawing();
			var draggableAssets:Array=archibusDrawing.getDraggableAssets();
			var isDraggable:Boolean=(draggableAssets == null) ? false : new ArrayCollection(draggableAssets).contains(this.tableName);
			if (!isDraggable) {
				return;
			}

			var container:UIComponent=new UIComponent();

			var ds:DragSource=new DragSource();
			ds.addData(this.record, "items");
			ds.addData(this.tableName, "table");
			var imageProxy:Image=new Image();

			//TODO: 
			if (this.tableName == "em") {
				imageProxy.source=manImage;
			} else {
				imageProxy.source=man2Image;
			}

			imageProxy.width=15;
			imageProxy.height=15;
			container.addChild(imageProxy);
			container.owner=this;
			var label:UITextField=new UITextField();
			label.text=this.getText();
			label.x=18;
			container.addChild(label);

			var coord:Point=e.currentTarget.globalToLocal(new Point(e.stageX, e.stageY));
			container.x=coord.x;
			container.y=e.localY - 30;

			DragManager.doDrag(e.currentTarget as UIComponent, ds, e, container);
		}

		/**
		 *
		 * getFromRecord.
		 */
		public function getFromRecord(record:Object):Array {
			var fromLocation:Array=[];
			for (var name:String in record) {
				var rec:Object={name: name, value: record[name]};
				fromLocation.push(rec)
			}
			return fromLocation;
		}


		/**
		 *handleDropEvent
		 *
		 */
		public function handleDropEvent(draggedItem:Object, roomIds:Array, assetType:String, al:AssetLayer, addNew:Boolean):Boolean {
			var draggedRecord:Object=this.getFromRecord(draggedItem);
			if (draggedRecord != null) {

				var droppedRecord:Object={};
				droppedRecord.from=draggedRecord;
				droppedRecord.to=roomIds;

				var ids:Array=[];
				for (var i:int=0; i < roomIds.length; i++) {
					ids.push(roomIds[i].value);
				}
				var targetDrawing:ArchibusDrawing =  (al && al.parent)? (al.parent as ArchibusDrawing):this.getCurrentDrawing();
				
				var labLayer:LabelLayer=targetDrawing.labelLayer;
				var lo:LabelObject=labLayer.getLabel(ids);
				if (lo != null && lo.fieldExists(this.field, this.getText())) {
					return false;
				}
				var dwgname:String = (targetDrawing.config)? targetDrawing.config.rawDwgName : "";
			
				var droppable:Boolean=ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_ONASSETLOCATIONCHANGEHANLDER), droppedRecord, assetType, dwgname);
				if (droppable) {
					removeSelfFromParent();
					if (addNew) {
						var plo:LabelObject=this.parent as LabelObject;
						var pIds:Array=ids;
						if (plo) {
							pIds=plo.ids;
						}
						if (lo == null) {
							lo=labLayer.createLabelObject(pIds, draggedItem);
							var tf:AbTextFormat=this.parentApplication.controlConfig.labelConfig.getTextFormat(LabelConfig.FORMAT_DEFAULT);
							lo.addField("rm.rm_id", pIds[2], tf, roomIds);
						}

						this.record=Utilities.formatValues(draggedRecord, pIds);
						lo.addExistField(this);
						labLayer.refresh();
					}

					if (ids.length > 0 && al != null) {
						al.highlightRoom([pIds[0], pIds[1], pIds[2]],HighlightFormat.BORDER);
					}
					return true;
				} else {
					DragManager.showFeedback(DragManager.NONE);
				}
			} else {
				DragManager.showFeedback(DragManager.NONE);
			}
			return false;
		}

		//TODO
		private function removeSelfFromParent():void {
			var currentLO:LabelObject=this.parent as LabelObject;
			currentLO.removeExistTextField(this);
		}
	}
}


