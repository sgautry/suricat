package src.view.text {

	/**
	 * Defines the text's appending modes.
	 */
	public class AppendMode {
		/**
		 * Clear all the labels then append the labels.
		 */
		public static const REPLACE:int=0;

		/**
		 * replace the text field with the corresponding labels.
		 */
		public static const REPLACEPERFIELD:int=1;

		/**
		 * Appends the labels.
		 */
		public static const APPEND:int=2;

		/**
		 * Remove all the specifying labels.
		 */
		public static const REMOVE:int=3;

		/**
		 * Remove all the labels.
		 */
		public static const REMOVEALL:int=4;

		/**
		 * Remove all label objects and re-populate all the labels.
		 */
		public static const RESET:int=5;
	}
}
