package src.view.text {
	import flash.errors.IllegalOperationError;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	
	import mx.core.UIComponent;
	
	import src.model.config.ControlConfig;
	import src.model.config.DrawingConfig;
	import src.model.config.LabelConfig;
	import src.model.data.DataSource;
	import src.model.data.SourceType;
	import src.model.text.AbTextFormat;
	import src.model.util.CommonUtilities;
	import src.view.asset.AssetObject;
	import src.view.drawing.ArchibusDrawing;

	/**
	 * The components to holds all the labels.
	 *
	 */
	public class LabelLayer extends UIComponent {

		public static var minHeightMultiplier:Number=1.0;

		/**
		 * part of labels that do not need to fit (currently 0.2, which means 4/5 of the labels must fit)
		 */
		private static var LABEL_FIT_FAIL_PERCENTAGE:Number=0.2;

		/**
		 * Constructor.
		 */
		public function LabelLayer() {
			super();
		}

		/**
		 * ideal lebel text size
		 * @default 11
		 */
		private var _idealLabelTextSize:uint=11;

		/**
		 * minimum label text size.
		 * @default 8
		 */
		private var _minimumLabelTextSize:uint=8;

		/**
		 * will shrink the label if the text does not fit?
		 * @default false
		 */
		private var _shrinkLabelTextToFit:Boolean = false;

		/**
		 * current text size.
		 * @default 0
		 */
		private var _fsTextSize:Number=0;

		/**
		 * label objects on the layer.
		 */
		private var _labelObs:Object={};

		/**
		 * number of labels on the layer.
		 *
		 * @default 0
		 */
		private var _nrOfLabels:uint=0;

		/**
		 * the reference to the drawing where labels resides.
		 */
		private var _archibusDrawing:ArchibusDrawing;

		/**
		 * the label for the title.
		 */
		private var _titleLabel:TextField=null;

		/**
		 * the initial scale for the layer.
		 *
		 * @default 1.0
		 */
		private var _initialScale:Number=1.0;

		/**
		 * Sets the label text properties from the default settings in controls-drawing.xml
		 */
		public function init():Boolean {
			//get the default from controls-drawing.xml
			// user can change these value from javascript at runtime
			var dwgConfig:ControlConfig=parentApplication.controlConfig;

			if (!dwgConfig) {
				return false;
			}

			this._idealLabelTextSize=dwgConfig.getIdealLabelTextSize();
			this._minimumLabelTextSize=dwgConfig.getMinimumLabelTextSize();
			this._shrinkLabelTextToFit=dwgConfig.getShrinkLabelTextToFit();

			return true;
		}

		//TODO
		public function createLabelObject(ids:Array, record:Object):LabelObject {
			var assetOb:AssetObject=this._archibusDrawing.assetLayer.getAssetOb(ids);
			var lo:LabelObject=new LabelObject(assetOb, null, null, record);
			this._labelObs[assetOb.properties.jsonData.ids]=lo;
			++this._nrOfLabels;
			this.addChild(lo);
			return lo;
		}

		/**
		 * Displays all selectable room's id for drag/drop events.
		 */
		private function displayAssetIDS(ad:ArchibusDrawing, pac:ControlConfig, tf:AbTextFormat):void {
			var draggableAssets:Array = ad.getDraggableAssets();
			if(draggableAssets != null && draggableAssets.toString().indexOf("em") >= 0){
				for (var j:int=0; j < ad.assets.length; j++) {
					var asset:Object=ad.assets[j];
					var ids:Array=asset.ids;
					var assetObj:AssetObject=ad.assetLayer.getAssetOb(ids);
					if (assetObj.properties.isSelectable) {
						var lo:LabelObject=this.getLabel(ids);
						if (lo == null) {
							lo=createLabelObject(ids, null);
						}
						lo.addField("rm.rm_id", ids[2], tf, null);
					}
				}
			}
		}
		/**
		 * Populates all the labels for the specifying drawing and data.
		 * @param dwgPK an arry of primary keys restriction.
		 * @param ad the archibus drawing.
		 */
		public function populateLabels(dwgPK:Array, ad:ArchibusDrawing, ds:DataSource=null, labelingConfig:Object=null):void {
			this._archibusDrawing=ad;
			var pac:ControlConfig=parentApplication.controlConfig;

			var defaultTF:AbTextFormat=pac.labelConfig.getTextFormat(LabelConfig.FORMAT_DEFAULT);
			var tf:AbTextFormat = defaultTF.copy();
			/*if(labelingConfig){
				tf.setColor(labelingConfig);
				tf.setSize(labelingConfig);
			}*/
			var appliedAssetIds:Array=[]; // used for handling multiple records for a single asset
			if(ds == null){
				ds = new DataSource(SourceType.LABEL);
			}
			this.init();
			
			ds.loadRecords(dwgPK);
			var keys:Array=ds.getPrimaryKeyNames();
			var fieldNames:Array=ds.getFieldNames(false);

			if (ds.getFieldDefs()) {
				displayAssetIDS(ad, pac, tf);
			}
			var nofillsEnabled:Boolean=pac.runtimeConfig.nofillsEnabled() && !pac.isNofillLabelsEnabled();
			var iTot:int=ds.numberOfRecords();

			for (var i:int=0; i < iTot; i++) {
				var rec:Object=ds.getRecord(i);
				if (rec == null) {
					continue;
				}

				var assetIds:Array=ds.getPrimaryKeysValues(i);
				var assetOb:AssetObject=ad.assetLayer.getAssetOb(assetIds);
				if (assetOb == null) {
					continue;
				}

				// if this rec has a value that is part of the nofills settings and labels
				// are to be disabled with this condition, just continue at this point
				if (nofillsEnabled && ad.highlight.isAssetDisabled(assetIds)) {
					continue;
				}

				// If there already is an existing label
				//	(meaning multiple records were returned per asset)
				//	then, filter out the primary key values and just feed in
				//	the other values to the LabelOb constructor.  Which will
				//	in turn just append those values to the existing label
				var bExisting:Boolean=appliedAssetIds[assetIds];

				var lo:LabelObject;
				var fieldName:String;
				var txt:String;
				var j:int;
				var key:String;
				if (!bExisting) {
					var labelData:Array=[];
					for (j=0; j < fieldNames.length; j++) {
						var field:Object={};
						field.fieldName=fieldNames[j];
						//localized numbers
						field.value=rec[field.fieldName] +"";
						field.priority=0;

						// higher priority for area fields
						if (field.fieldName.match('area') != null) {
							field.priority=1;
						}

						// highest priority for primary keys
						for each (key in keys) {
							if (field.fieldName == key) {
								field.priority=2;
								break;
							}
						}

						labelData.push(field);
					}

					appliedAssetIds[assetIds]=true;
					lo=new LabelObject(assetOb, labelData, tf, rec);
					this._labelObs[assetOb.properties.jsonData.ids]=lo;
					this.addChild(lo);
					++this._nrOfLabels;
				} else {
					lo=this._labelObs[assetIds];
					for (j=0; j < fieldNames.length; j++) {
						fieldName=fieldNames[j];
						txt=rec[fieldName];

						var skip:Boolean=false;
						for each (key in keys) {
							if (fieldName == key) {
								skip=true;
								break;
							}
						}
						if (skip)
							continue;

						// also verify that there are no duplicate entries in the label
						if (lo.fieldExists(fieldName, txt))
							continue;

						lo.addField(fieldName, txt, tf, rec);
					}
				}
			}
			this.drawLabels();
		}

		/**
		 * Removes all the labels and reset all the related variables.
		 */
		public function clearLabels():void {
			for each (var label:LabelObject in this._labelObs) {
				this.removeChild(label);
			}
			this._labelObs={};
			this._nrOfLabels=0;
		}

		/**
		 * Adds all the labels for the specifying records.
		 *
		 * @param opts per drawing config which contains the recs information
		 * @param mode append mode.
		 */
		public function setLabels(opts:DrawingConfig, mode:int):void {
			var recs:Array=opts.recs;
			for (var i:uint=0; i < recs.length; i++) {
				var ids:Array=recs[i].id.split(parentApplication.controlConfig.runtimeConfig.keyDelimiter);
				var lo:LabelObject=this._labelObs[ids];
				if (lo == null)
					continue;

				if (mode != AppendMode.RESET) {
					lo.modify(mode, recs[i]);
				} else {
					this.removeChild(this._labelObs[ids]);
					this.populateLabels(new Array('rm.bl_id', ids[0], 'rm.fl_id', ids[1], 'rm.rm_id', ids[2]), this._archibusDrawing);
				}
			}

			this.drawLabels();
		}

		public function refresh():void {
			this.drawLabels();
		}

		/**
		 * Gets the label by the specifying ids.
		 *
		 * @param ids the label's ids.
		 * @return the label object.
		 */
		public function getLabel(ids:Array):LabelObject {
			return this._labelObs[ids];
		}

		/**
		 * Sets the title label to the specifying value and text format. If the title label does not exist, create one.
		 *
		 * @param value the title value
		 * @param tf the text format.
		 */
		public function setTitle(value:String, tf:AbTextFormat):void {
			if (this._titleLabel == null) {
				this._titleLabel=new TextField();
				this.addChild(this._titleLabel);
			}

			this._titleLabel.autoSize=TextFieldAutoSize.CENTER;
			this._titleLabel.text=value;
			this._titleLabel.setTextFormat(tf);

			this.positionTitleLabel();
		}

		public function get idealLabelTextSize():uint {
			return this._idealLabelTextSize;
		}

		public function set idealLabelTextSize(textSize:uint):void {
			if (textSize < this._minimumLabelTextSize) {
				throw new IllegalOperationError("Ideal text size cannot be smaller than minimal text size");
			}
			this._idealLabelTextSize=textSize;

			// triggers commitProperties to redraw labels
			invalidateProperties();
		}

		public function get minimumLabelTextSize():uint {
			return this._minimumLabelTextSize;
		}

		public function set minimumLabelTextSize(textSize:uint):void {
			if (textSize > this._idealLabelTextSize) {
				throw new IllegalOperationError("Minimal text size cannot be larger than ideal text size");
			}
			this._minimumLabelTextSize=textSize;

			// triggers commitProperties to redraw labels
			invalidateProperties();
		}

		public function get shrinkLabelTextToFit():Boolean {
			return this._shrinkLabelTextToFit;
		}

		public function set shrinkLabelTextToFit(value:Boolean):void {
			this._shrinkLabelTextToFit=value;

			// triggers commitProperties to redraw labels
			invalidateProperties();
		}

		public function get fsTextSize():Number {
			return this._fsTextSize;
		}

		override protected function commitProperties():void {
			super.commitProperties();

			// redraw labels when properties are committed
			this.drawLabels();
		}


		/**
		 * Calculates the maximal text size in full size coordinates.
		 *
		 * This text size is calculated for when only fields with the given shownPriority and higher are
		 * shown.
		 */
		private function calculateOptimalFSTextSize(shownPriority:Number=0):Number {
			var res:Number=Number.MAX_VALUE;
			var failed:Array=[];
			var toFail:uint=this._nrOfLabels * LABEL_FIT_FAIL_PERCENTAGE;
			for each (var label:LabelObject in this._labelObs) {
				label.showOnly(shownPriority);
				var maxTextSize:Number=label.getMaxTextSize();
				if (maxTextSize < res) {
					failed.push(maxTextSize);
					if (failed.length > toFail) {
						failed.sort(Array.NUMERIC);
						res=failed.pop();
					}
				}
			}

			return res;
		}

		/**
		 * Draws the full LabelLayer.
		 *
		 * For the labels, only the fields with priority equal or higher than the showPriority attribute
		 * are shown.
		 */
		private function drawLabels(shownPriority:Number=0):void {
			// calculate optimal textsize in screen coordinates
			var textSize:Number=this.calculateOptimalFSTextSize(shownPriority) * this.scaleY;
			textSize=Math.min(textSize, this._idealLabelTextSize);

			// if optimal text size is less then minimal text size, remove fields or use minimal text size
			if (textSize < this._minimumLabelTextSize) {
				// if possible and allowed go one priority higher, which means less fields
				if (this._shrinkLabelTextToFit && shownPriority < LabelObject.MAX_PRIORITY) {
					this.drawLabels(shownPriority + 1);
					return;
				} else {
					// otherwise use minimal text size, adjusted for asset density
					textSize=this.minimumLabelTextSize * getTextHeightAdjustment();
				}
			}

			// convert screen text size back into full size text size and set it on the labels
			this._fsTextSize=(textSize / this.scaleY) * LabelLayer.minHeightMultiplier;

			for each (var label:LabelObject in this._labelObs) {
				label.textSize=this._fsTextSize;
			}

			// position the title label
			this.positionTitleLabel();
		}
		
	  
		/**
		 * Positions the title label to the middle and center of archibus drawing.
		 */
		private function positionTitleLabel():void {
			if (this._titleLabel == null)
				return;
			this._titleLabel.scaleX=this._titleLabel.scaleY=1 / this.scaleY;
			this._titleLabel.x=(this._archibusDrawing.width / this.scaleX - this._titleLabel.width) / 2;
			this._titleLabel.y=this._archibusDrawing.height / this.scaleY - this._titleLabel.height;
		}

		/**
		 * Calculates the text height's adjustment factor.
		 *
		 * @return the adjustment factor.
		 */
		private function getTextHeightAdjustment():Number {
			var multFactor:Number=1.0;
			var cnt:int=0;

			if (this._initialScale == 1.0)
				this._initialScale=this.scaleY;

			if (this._archibusDrawing != null && this._archibusDrawing.assetLayer != null && this._archibusDrawing.assetLayer.assetMap != null && this._archibusDrawing.assetLayer.assetMap.assets != null)
				cnt=this._archibusDrawing.assetLayer.assetMap.assets.length;

			var num:Number=Math.sqrt(cnt / 65.0);
			if (num < 1.0)
				num=1.0;

			multFactor=1.0 / num;

			multFactor*=(this.scaleY / this._initialScale);

			return multFactor;
		}
	}
}
