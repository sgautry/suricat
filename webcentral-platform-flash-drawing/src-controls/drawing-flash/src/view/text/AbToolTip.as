package src.view.text {
	import flash.events.MouseEvent;
	import flash.filters.BevelFilter;
	import flash.text.TextFormat;
	import mx.containers.Canvas;
	import mx.core.UIComponent;
	import mx.utils.ObjectUtil;
	import src.model.util.CommonUtilities;
	import src.view.asset.AssetObject;
	import src.model.data.*;
	import src.model.text.AbTextFormat;
	import src.model.text.ToolTipConfig;
	import src.view.drawing.ArchibusDrawing;

	/**
	 * Component representing the tooltip.
	 */
	public class AbToolTip extends ToolTipConfig {

		private static const FIRSTLINE_SPACING:uint=25;
		private static const OTHERLINES_SPACING:uint=18;

		/**
		 * Constructor
		 */
		public function AbToolTip() {
			super();
		}
		/**
		 * Convas to hold the tooltip.
		 */
		private var _canvas:Canvas=null;

		/**
		 * the text lines inside of the canvas.
		 */
		private var _textlines:UIComponent=null;

		/**
		 * the text format for the first line.
		 */
		private var _firstlineFormat:AbTextFormat=new AbTextFormat();

		/**
		 * the text format for all the other lines.
		 */
		private var _subsequentlinesFormat:AbTextFormat=new AbTextFormat();

		/**
		 * Initialize the tooltip canvas.
		 *
		 * @param canvas the canvas to put the tooltip on.
		 * @param cfg
		 */
		public function init(canvas:Canvas, cfg:Object):void {
			// the canvas must have BevelFilter, which lets you add a bevel effect to display objects.
			if (!canvas.filters.length) {
				_canvas=canvas;
				_textlines=new UIComponent();
				_dsType=SourceType.NONE;

				this.setFromConfig(cfg);

				_canvas.setStyle('backgroundColor', _canvasColor);
				_canvas.alpha=_canvasOpacity;

				// validate that a specified source type is valid
				// if not, set to a known valid source type or SOURCE_NONE if needed
				this.validateSourceType();

				if (CommonUtilities.isValid(cfg, "firstline"))
					_firstlineFormat.setFromFontConfig("firstline", cfg.firstline);

				if (CommonUtilities.isValid(cfg, "subsequentlines"))
					_subsequentlinesFormat.setFromFontConfig("subsequentlines", cfg.subsequentlines);

				var bv:BevelFilter=getFilter();
				_canvas.addChild(_textlines);
				_canvas.filters=[bv];
			}
		}

		/**
		 * Show Tooltip at the specifying position.
		 *
		 * @param event the mouse event.
		 * @param xStart the start X position for the canvas
		 * @param yStart the start Y position for the canvas
		 */
		public function show(event:MouseEvent, xStart:int, yStart:int):void {
			if (!_enabled)
				return;


			var values:Array=getValues(event);

			if (!values.length)
				return;

			// initial y position
			var yPos:int=5;

			// append text
			for (var i:int=0; i < values.length; i++) {
				var txt:AbTextField=new AbTextField('', 0, values[i], this.getTextFormat(!i), false, null);
				txt.x=5;
				txt.y=yPos;
				yPos+=(!i ? FIRSTLINE_SPACING : OTHERLINES_SPACING) - 4;
				txt.width=txt.getTextField().textWidth + 10;

				_textlines.addChild(txt);
			}

			//reset the canvas width and height if the text is oversized.
			var width:int=txt.getTextField().textWidth + 20;
			var height:int=yPos + 5;

			_canvas.width=(width > _minWidth) ? width : _minWidth;
			_canvas.height=(height > _minHeight) ? height : _minHeight;

			setPos(xStart, yStart);

			_canvas.visible=true;
		}

		/**
		 * Gets the text values to display on the tooltip canvas.
		 *
		 * @param event the mouse event.
		 * @return an array of ids to display.
		 */
		private function getValues(event:MouseEvent):Array {
			var values:Array=[];

			var ids:Array=CommonUtilities.getIdsFromEvent(event);
			if (_dsType == SourceType.NONE) {
				var delim:String=_canvas.parentApplication.controlConfig.runtimeConfig.keyDelimiter;
				values[0]=ids.join(delim);
			} else if (_dsType == SourceType.LABEL) {
				var lo:LabelObject=null;
				var tf:AbTextField=null;
				var ob:Object=ObjectUtil.getClassInfo(event.target);

				if (ob.name == "src.view.asset::AssetObject") {
					var aob:AssetObject=event.target as AssetObject;
					var ad:ArchibusDrawing=aob.parent.parent as ArchibusDrawing;
					lo=ad.labelLayer.getLabel(ids);
				} else if (ob.name == "src.view.text::AbTextField") {
					tf=event.target as AbTextField;
					lo=tf.parent as LabelObject;
				}

				if (lo != null)
					values=lo.getToolTip();

			}
			return values;
		}


		/**
		 * Hides the canvas.
		 */
		public function hide():void {
			if (!_enabled)
				return;

			for (var i:int=_textlines.numChildren - 1; i >= 0; i--) {
				delete _textlines.getChildAt(i);
				_textlines.removeChildAt(i);
			}
			_canvas.visible=false;
		}

		/**
		 * Sets the origin of the tooltip.  This algorithm ensures that
		 * the tooltip will always reside in the visible area.
		 *
		 * @param	x	The raw mouseX position
		 * @param	y	The raw mouseY position
		 */
		public function setPos(xIn:int, yIn:int):void {
			if (!_canvas)
				return;

			// ideal setting
			var xPos:int=xIn - 20;

			// adjust if near min/max of the container width
			var dc:Canvas=_canvas.parentApplication.dwgContainer;
			var _maxWidthView:int=dc.width;
			var _maxHeightView:int=dc.height;
			if (xPos < 1)
				xPos=1;
			else if ((xPos + _canvas.width) >= _maxWidthView)
				xPos=_maxWidthView - _canvas.width - 1;

			// ideal setting
			var yPos:int=yIn + 20;

			// adjust if near min/max of the container height
			if ((yPos + _canvas.height) >= _maxHeightView)
				yPos=yIn - _canvas.height - 15;

			_canvas.x=xPos;
			_canvas.y=yPos;
		}

		public function set width(i:int):void {
			if (_canvas)
				_canvas.width=i;
		}

		public function set height(i:int):void {
			if (_canvas)
				_canvas.height=i;
		}

		/**
		 * Gets the text format for the firstline or the other lines.
		 *
		 * @param firstline true if first line, false otherwise.
		 * @return the TextFormat object.
		 */
		private function getTextFormat(firstline:Boolean):TextFormat {
			var fmt:TextFormat=new TextFormat();

			if (firstline) {
				fmt.font=_firstlineFormat.font;
				fmt.color=_firstlineFormat.color;
				fmt.bold=_firstlineFormat.bold;
				fmt.italic=_firstlineFormat.italic;
				fmt.underline=_firstlineFormat.underline;
				fmt.size=_firstlineFormat.size;
			} else {
				fmt.font=_subsequentlinesFormat.font;
				fmt.color=_subsequentlinesFormat.color;
				fmt.bold=_subsequentlinesFormat.bold;
				fmt.italic=_subsequentlinesFormat.italic;
				fmt.underline=_subsequentlinesFormat.underline;
				fmt.size=_subsequentlinesFormat.size;
			}

			return fmt;
		}


		/**
		 * Gets the default Bevel Filter object.
		 *
		 * @return the default Bevel Filter object.
		 */
		private function getFilter():BevelFilter {
			var bv:BevelFilter=new BevelFilter();
			bv.distance=4;
			bv.angle=45;
			bv.highlightColor=0xFFFFFF;
			bv.highlightAlpha=1;
			bv.shadowColor=0x00000;
			bv.shadowAlpha=1;
			bv.blurX=4;
			bv.blurY=4;
			bv.strength=1;
			bv.quality=5;
			bv.type=flash.filters.BitmapFilterType.OUTER;
			bv.knockout=false;
			return bv;
		}

	}
}
