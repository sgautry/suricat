package src.view.text {
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.*;
	import flash.text.AntiAliasType;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import mx.collections.ArrayCollection;
	import mx.core.UIComponent;
	import mx.events.DragEvent;
	import mx.managers.DragManager;
	
	import src.model.text.AbTextFormat;
	import src.view.asset.AssetObject;
	import src.view.drawing.ArchibusDrawing;

	/**
	 * Defines a label objects on the label layer.
	 */
	public class LabelObject extends UIComponent {

		private static const BASE_TEXT_SIZE:uint=100;

		public static const MAX_PRIORITY:uint=2;

		/**
		 * the asset object this label is a label for.
		 */
		private var _assetOb:AssetObject;


		/**
		 * Save the asset's bounds to avoid recomputation.
		 */
		private var _assetBounds:Rectangle;

		/**
		 * Array of text fields which made up the label.
		 */
		private var _textFields:Array=[];


		/**
		 * Space between vertical lines.
		 */
		private var _spacingFactor:Number=1.5;

		/**
		 * Label's text width.
		 */
		private var _textWidth:Number=0;

		/**
		 * Label's text height.
		 */
		private var _textHeight:Number;
		
		private var _json:Object;

		/**
		 * flag to indicate data has been changed (which means re-alignment and positioning is needed)
		 */
		private var _dataModified:Boolean=false;

		/**
		 * Positioning info
		 */
		private var _anchor:Object={pnt: null, // the point to position on 
				left: 0, // whether to place the label to the left or to the right of the point
				upper: 0 // whether to place the label to above or beneeth of the point
			};

		public function getTextFields():Array {
			return this._textFields;
		}

		/**
		 * Constructs a new LabelOb.
		 *
		 * data should be an array of objects, containing fieldName, value and priority.
		 */
		public function LabelObject(assetOb:AssetObject, data:Array, tf:AbTextFormat, record:Object) {
			// save asset and bounds
			this._assetOb=assetOb;
			this._assetBounds=assetOb.getBounds(assetOb);
			
			this._json  = this._assetOb.properties.jsonData;

			if (data != null) {
				// add the fields
				for each (var field:Object in data) {
					this.addField(field.fieldName, field.value, tf, record, field.priority);
				}

				// align the fields (this also computes textWidth & Height)
				this.alignFields();

			}

		}

		/**
		 * Gets the tooltip texts as an array.
		 */
		public function getToolTip():Array {
			var i:int=0;
			var toolTipText:Array=[];
			for each (var textField:AbTextField in this._textFields) {
				toolTipText[i]=textField.getText();
				i++;
			}
			return toolTipText;
		}

		/**
		 * Creates a text field with the specifying name and value.
		 *
		 * @param name the field name
		 * @param value the field value
		 * @param tfa the text format for the text
		 * @param priority the priority to show.
		 */
		public function addField(name:String, value:String, tfa:AbTextFormat, record:Object, priority:uint=0):void {
			//XXX: check???
			if (this.fieldExists(name, value)) {
				return;
			}
			var tf:TextFormat=tfa;
			//XXX: CANNOT dynamically set 
			tf.size=BASE_TEXT_SIZE;
			tf.align=TextFormatAlign.CENTER;

			var textField:AbTextField=new AbTextField(name, priority, (value != "" ? value : "-"), tf, false, record);
			this._textFields.push(textField);

			this._dataModified=true;
		}

		/**
		 * addExistField.
		 */
		public function addExistField(textField:AbTextField):void {
			this._textFields.push(textField);
			this.alignFields();
		}


		/**
		 *
		 * Remove text field by its field name and text value.
		 *
		 */
		public function removeTextField(fieldName:String, text:String):void {
			var targetTextField:AbTextField=null;
			var arr:ArrayCollection=new ArrayCollection(this._textFields);
			for (var i:int=0; i < arr.length; i++) {
				var f:AbTextField=arr[i] as AbTextField;
				if (f.field == fieldName && f.getText() == text) {
					targetTextField=f;
					arr.removeItemAt(i);
					break;
				}
			}

			this._textFields=arr.toArray();
			if (targetTextField != null) {
				this.removeChild(targetTextField);
			}
		}

		/**
		 * removeExistField.
		 */
		public function removeExistTextField(textField:AbTextField):void {
			removeTextField(textField.field, textField.getText());
		}


		/**
		 * Aligns the fields.
		 */
		public function alignFields():void {
		
			this._textWidth=0;
			for each (var textField:AbTextField in this._textFields) {
				
				// removing field to get real (unscaled) measurements
				if (textField.parent && this.contains(textField)) {
					this.removeChild(textField);
				}
				
				if (textField.visible)
					this._textWidth=Math.max(this._textWidth, textField.getTextField().textWidth);
			}
			var y:Number=0;
			for (var i:uint=0; i < this._textFields.length; ++i) {
				if (!this._textFields[i].visible)
					continue;
				var field:TextField = this._textFields[i].getTextField();
				field.x=(this._textWidth - field.width) / 2;
				this._textFields[i].y=y;
				this._textHeight=y + field.textHeight;
				y+=field.textHeight * this._spacingFactor;
				
				this.addChild(this._textFields[i]);
			}
			this._dataModified=false;
			
			
			if(!(this._json!=null && this._json.label)){
				this.determineAnchorByCorner();
			}
		
		}

		/**
		 * Gets the maximum text size.
		 *
		 * @return the max text size.
		 */
		public function getMaxTextSize():Number {
			return Math.min(this._assetBounds.width / this.textWidth, this._assetBounds.height / this.textHeight) * BASE_TEXT_SIZE;
		}

		/**
		 * Sets the text size to the specified value.
		 *
		 * @param textSize the text size to set.
		 */
		public function set textSize(textSize:Number):void {
			// text size is set on the scale. The real text size stays the same. This is necassary
			// because flash does not allow text sizes bigger then 127px, which are needed here because
			// of the scaling of the parent
			this.scaleX=this.scaleY=textSize / BASE_TEXT_SIZE;

			if(this._json!=null && this._json.label){
				this.x = this._json.label.X - this.textWidth * this.scaleX * 0.5;
				this.y = this._json.label.Y - this.textHeight * this.scaleY * 0.5;
			}else{
				// position label to anchor
				this.x=this._anchor.pnt.x - this.textWidth * this.scaleX * this._anchor.left;
				this.y=this._anchor.pnt.y - this.textHeight * this.scaleY * this._anchor.upper;
			}
		}

		/**
		 * Removes all the text fields and reset the variables.
		 */
		public function clear():void {
			for (var i:int=(this.numChildren - 1); i >= 0; i--) {
				this.removeChildAt(i);
			}
			this._textFields.length=0;
			this._textHeight=0;
			this._textWidth=0;
			this._dataModified=true;
		}

		/**
		 * Modify the label object to the specified values.
		 *
		 * @param mode append mode.
		 * @param labels the values to be append.
		 */
		public function modify(mode:int, record:Object):void {
			var label:Object;
			var textField:AbTextField;
			switch (mode) {
				case AppendMode.REPLACE:  {
					this.clear();
						// no break, continuing...
				}
				case AppendMode.APPEND:  {
					var tf:AbTextFormat=new AbTextFormat();
					for each (label in record.l) {
						tf.setFromFontConfig(label.field, label);
						this.addField(label.field, label.value, tf, record);
					}
					break;
				}
				case AppendMode.REPLACEPERFIELD:  {
					for each (label in record.l) {
						for each (textField in this._textFields) {
							if (textField.field == label.field) {
								textField.setText(label.value);
								break;
							}
						}
					}
					break;
				}
				case AppendMode.REMOVE:  {
					this.removeLabels(record.l);
					break;
				}
				case AppendMode.REMOVEALL:  {
					this.clear();
				}
			}
		}

		/**
		 * Removes the all the labels of the text fields.
		 *
		 * @param labels
		 */
		private function removeLabels(labels:Array):void {
			this._textFields=this._textFields.filter(function filt(textField:AbTextField, index:uint, arr:Array):Boolean {
				for each (var label:Object in labels) {
					if (textField.getText() == label.value && textField.field == label.field) {
						this.removeChild(textField);
						return false;
					}
				}
				return true;
			}, this);
			this._dataModified=true;
		}

		/**
		 * Checks if the text field with the specifying name and value exists.
		 *
		 * @param fieldName the field name to check.
		 * @param fieldValue the field value to check.
		 *
		 * @return true of the field is found, false otherwise.
		 */
		public function fieldExists(fieldName:String, fieldValue:String):Boolean {
			for each (var textField:AbTextField in this._textFields) {
				if (textField.field == fieldName && textField.getText() == fieldValue)
					return true;
			}
			return false;
		}

		/**
		 * Removes text fields that have a priority less then the given one.
		 * always display first text.
		 */
		public function showOnly(priority:Number):void {
			var index:int = 0;
			for each (var textField:AbTextField in this._textFields) {
				if (index != 0 && textField.priority < priority) {
					textField.visible=false;
				} else {
					textField.visible=true;
				}
				index++;
			}
			this._dataModified=true;
		}


		/**
		 * Finds an anchor to position on.
		 *
		 * First tries to center the label. If not possible, try the left upper corner, then right upper,
		 * left lower and right lower. If all fail, center label.
		 */
		private function determineAnchorByCorner(positionX:Number=0):void {
			var point:Point =  new Point(this._assetBounds.x + this._assetBounds.width / 2, this._assetBounds.y + this._assetBounds.height / 2);
			
			var center:Point=this._assetOb.localToGlobal(point);
			//if (this._assetOb.hitTestPoint(center.x, center.y, true)) {
				this._anchor.pnt=this._assetOb.globalToLocal(center);
				this._anchor.left=this._anchor.upper=.5;
				return;
			//}
		}

		
		/**
		 * Gets the text field by the specifying field name.
		 *
		 * @param fieldName the field name.
		 * @return the text field object.
		 */
		public function getTextField(fieldName:String):AbTextField {
			for each (var field:AbTextField in this._textFields) {
				if (field.field == fieldName) {
					return field;
				}
			}
			return null;
		}

		/**
		 * Draws the line.
		 */
		private function drawGlobalLine(x1:Number, y1:Number, x2:Number, y2:Number):void {
			var comp:Sprite=this._assetOb.parent as Sprite;

			var a:Point=new Point(x1, y1);
			a=comp.globalToLocal(a);
			var b:Point=new Point(x2, y2);
			b=comp.globalToLocal(b);

			comp.graphics.lineStyle(1, 0X00FF00, 0);
			comp.graphics.beginFill(0X00FF00);
			comp.graphics.moveTo(a.x, a.y);
			comp.graphics.lineTo(b.x, b.y);
			comp.graphics.endFill();
		}

		private function get textWidth():Number {
			if (this._dataModified) {
				this.alignFields()
			}
			return this._textWidth;
		}

		private function get textHeight():Number {
			if (this._dataModified) {
				this.alignFields()
			}
			return this._textHeight;
		}

		public function get ids():Array {
			return this._assetOb.properties.jsonData.ids;
		}


	}
}
