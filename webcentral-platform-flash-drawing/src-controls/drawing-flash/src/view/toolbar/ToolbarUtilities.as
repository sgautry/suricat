package src.view.toolbar {
	import flash.external.ExternalInterface;

	import mx.containers.Canvas;
	import mx.controls.Button;
	import mx.controls.Image;
	import mx.controls.ToggleButtonBar;

	import src.model.util.JsFunctionConstant;
	import src.model.util.CommonUtilities;

	/**
	 * Component holding all the toolbar buttons.
	 */
	public class ToolbarUtilities extends Canvas {
		private var _appRef:abDrawing=null;

		static private const ACTION_SHOW:String="show";
		static private const ACTION_HIDE:String="hide";
		static private const ACTION_ENABLE:String="enable";
		static private const ACTION_DISABLE:String="disable";

		public function setOwner(abd:abDrawing):void {
			this._appRef=abd;
		}

		/**
		 * Initializes the toolbar buttons to the specific actions.
		 *
		 * Format of tbSettings
		 *			action=listOfBUTTONNAME_s
		 *
		 *		Examples:
		 *			show=resetAssets,clearAssets;
		 *			hide=iso,autoCenter;
		 *			enable=iso,zoomOut;
		 *			disable=zoomIn,state;
		 *			hide=all;
		 *			hide=zoomOut;disable=autoScale,iso,etc;
		 */
		public function init():void {
			var tbSettings:String=ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_GETTOOLBARSETTINGS));
			if (tbSettings == null || tbSettings == '')
				return;

			var actionGroups:Array=tbSettings.split(';');
			for (var i:int=0; i < actionGroups.length; i++) {
				var s1:String=actionGroups[i] as String;
				var actionGroup:Array=s1.split('=');
				if (actionGroup == null || actionGroup.length != 2)
					continue;
				var action:String=actionGroup[0];
				var names:String=actionGroup[1];
				if (action == ACTION_SHOW)
					show(true, names);
				else if (action == ACTION_HIDE)
					show(false, names);
				else if (action == ACTION_ENABLE)
					enable(true, names);
				else if (action == ACTION_DISABLE)
					enable(false, names);
			}
		}

		/**
		 * Show or Hide the button of the specifying name.
		 *
		 * @param name the button name.
		 * @param visible true if show the button, false if hide.
		 */
		public function showButton(name:String, visible:Boolean):void {
			var fullName:String=name + "Button";
			var btn:Button=_appRef.getChildByName(fullName) as Button;
			if(btn)
				btn.visible=visible;
		}

		/**
		 * Enable or disable the button of the specifying name.
		 *
		 * @param name the button name.
		 * @param enabled true if enable the button, false otherwise.
		 */
		public function enableButton(name:String, enabled:Boolean):void {
			var fullName:String=name + "Button";
			var btn:Button=_appRef.getChildByName(fullName) as Button;
			if(btn)
				btn.enabled=enabled;
		}

		/**
		 * Show or Hide buttons of the specifying names.
		 *
		 * @param visible true if show the buttons, false otherwise.
		 * @param names a comma separated string of button names to show or hide.
		 */
		public function show(visible:Boolean, name:String):void {
			if (name == null)
				name=ToolbarButton.WORD_ALL;

			var setStateButtons:Boolean=false;
			if (name == ToolbarButton.WORD_ALL) {
				name=getAllButtonNames();
				setStateButtons=true;
			}

			var ar:Array=name.split(',');
			for (var i:int=0; i < ar.length; i++) {
				if (ar[i] != ToolbarButton.STATE_BUTTONS)
					showButton(ar[i], visible);
				else
					setStateButtons=true;
			}

			if (setStateButtons == true) {
				var bb:ToggleButtonBar=_appRef.getChildByName(ToolbarButton.TOOLBAR_NAME) as ToggleButtonBar;
				if(bb)
					bb.visible=visible;
			}
		}

		/**
		 * Enable or Disable buttons of the specifying names.
		 *
		 * @param enable true if enables the buttons, false otherwise.
		 * @param names a comma separated string of button names to enable or disable.
		 */
		public function enable(enabled:Boolean, names:String):void {
			if (names == null)
				names=ToolbarButton.WORD_ALL;

			var setStateButtons:Boolean=false;

			if (names == ToolbarButton.WORD_ALL) {
				names=getAllButtonNames();
				setStateButtons=true;
			}

			var ar:Array=names.split(',');
			for (var i:int=0; i < ar.length; i++) {
				if (ar[i] != ToolbarButton.STATE_BUTTONS)
					enableButton(ar[i], enabled);
				else
					setStateButtons=true;
			}

			if (setStateButtons == true) {
				var bb:ToggleButtonBar=_appRef.getChildByName(ToolbarButton.TOOLBAR_NAME) as ToggleButtonBar;
				if(bb)
					bb.enabled=enabled;
			}
		}

		private function getAllButtonNames():String {
			var names:String=ToolbarButton.ZOOMIN + "," + ToolbarButton.ZOOMOUT + "," + ToolbarButton.ZOOMEXTENTS + "," + ToolbarButton.AUTOCENTER + "," + ToolbarButton.ISO;
			return names;
		}

		public function cmd(bn:String):void {
			if (bn == ToolbarButton.ZOOMIN)
				_appRef.zoomIn(null);
			else if (bn == ToolbarButton.ZOOMOUT)
				_appRef.zoomOut(null);
			else if (bn == ToolbarButton.ZOOMEXTENTS)
				_appRef.autoScale();
			else if (bn == ToolbarButton.AUTOCENTER)
				_appRef.autoCenter();
			else if (bn == ToolbarButton.ISO)
				_appRef.toggleIso();
		}

		/**
		 * Localize all the buttons with the specifying map and set the tooltips.
		 *
		 * @param map the loalization map.
		 */
		public function localize(map:Object):void {
			if (map == null)
				return;

			setButtonTooltipByName(ToolbarButton.ZOOMIN, map['TB_ZOOMIN']);
			setButtonTooltipByName(ToolbarButton.ZOOMOUT, map['TB_ZOOMOUT']);
			setButtonTooltipByName(ToolbarButton.AUTOCENTER, map['TB_AUTOCENTER']);
			setButtonTooltipByName(ToolbarButton.ZOOMEXTENTS, map['TB_ZOOMEXTENTS']);
			setButtonTooltipByName(ToolbarButton.ISO, map['TB_ISO']);
			setButtonTooltipByName(ToolbarButton.ZOOMWINDOW, map['TB_ZOOMWINDOW']);
			setButtonTooltipByName(ToolbarButton.MULTISELECT, map['TB_MULTISELECT']);
			setButtonTooltipByName(ToolbarButton.DRAWLINE, map['TB_DRAWLINE']);
			setButtonTooltipByName(ToolbarButton.DRAWRECT, map['TB_DRAWRECT']);
			setButtonTooltipByName(ToolbarButton.DRAWARROW, map['TB_DRAWARROW']);
			setButtonTooltipByName(ToolbarButton.DRAWPOSITIONMARKER, map['TB_DRAWPOSITIONMARKER']);
			setButtonTooltipByName(ToolbarButton.DRAWTEXT, map['TB_DRAWTEXT']);
			setButtonTooltipByName(ToolbarButton.UNDOREDLINE, map['TB_UNDOREDLINE']);
			setButtonTooltipByName(ToolbarButton.CLEARREDMARKS, map['TB_CLEARREDMARKS']);
		}

		/**
		 * Sets button's tooltips.
		 *
		 * @param btn the button to set.
		 * @param val the tooltip value.
		 */
		private function setButtonTooltip(btn:Button, val:String):void {
			if (btn == null || val == null)
				return;
			btn.toolTip=val;
		}

		/**
		 * Set the button's tooltip by specifying name
		 *
		 * @param name button name
		 * @param val the value to set.
		 */
		private function setButtonTooltipByName(name:String, val:String):void {
			var fullName:String=name + "Button";
			var btn:Image=_appRef.navCanvas.getChildByName(fullName) as Image;
			if (btn == null || val == null)
				btn=_appRef.getChildByName(fullName) as Image;

			if (btn == null || val == null)
				return;

			btn.toolTip=val;
		}
	}
}


