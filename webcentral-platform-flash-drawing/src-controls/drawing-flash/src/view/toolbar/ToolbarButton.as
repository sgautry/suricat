package src.view.toolbar {

	/**
	 * Define the toolbar buttons names and other constants.
	 */
	public class ToolbarButton {
		static public var TOOLBAR_NAME:String="toolBar";

		static public var STATE_BUTTONS:String="state";

		static public var WORD_ALL:String="all";

		static public var AUTOCENTER:String="autoCenter";

		static public var ZOOMIN:String="zoomIn";

		static public var ZOOMOUT:String="zoomOut";

		static public var ZOOMEXTENTS:String="zoomExtends";

		static public var ZOOMWINDOW:String="zoomWindow";

		static public var ISO:String="iso";
		
		static public var MULTISELECT:String="multiSelect";
		
		static public var DRAWLINE:String="drawLine";
		
		static public var DRAWRECT:String="drawRect";
		
		static public var DRAWARROW:String="drawArrow";
		
		static public var DRAWPOSITIONMARKER:String="drawPositionMarker";
		
		static public var DRAWTEXT:String="drawText";
		
		static public var UNDOREDLINE:String="undoRedline";
		
		static public var CLEARREDMARKS:String="clearRedmarks";
		
	}
}


