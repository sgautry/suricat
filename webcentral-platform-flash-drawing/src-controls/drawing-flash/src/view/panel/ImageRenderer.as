package src.view.panel {
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;

	import mx.containers.HBox;
	import mx.controls.Image;
	import mx.controls.Label;
	import mx.controls.dataGridClasses.*;

	import src.model.util.*;

	public class ImageRenderer extends HBox {
		private var img:Image=new Image();

		public var key:String=null;
		public var actionID:String=null;
		public var imageSource:String=null;
		private var value:Object;

		private var record:Array=[];

		override public function set data(value:Object):void {
			this.value=value;
			img.source=imageSource;
			addChild(img);
			addEventListener(MouseEvent.MOUSE_DOWN, clickHandler);
		}

		private function clickHandler(event:MouseEvent):void {
			var record:Array=[];
			for (var name:String in this.value) {
				var value:Object=this.value[name];
				name=name.replace("__", ".");
				if (name.indexOf(".") > 0) {
					record.push({name: name, value: value});
				}
			}
			ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_ONASSETPANELROWACTION), actionID, record, this.key);

		}
	}
}
