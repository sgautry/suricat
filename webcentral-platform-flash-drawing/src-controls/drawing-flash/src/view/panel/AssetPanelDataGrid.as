package src.view.panel {
	import flash.display.Sprite;
	import flash.external.ExternalInterface;

	import mx.controls.DataGrid;
	import mx.controls.Image;
	import mx.core.DragSource;
	import mx.core.UIComponent;
	import mx.core.UITextField;
	import mx.events.DragEvent;
	import mx.managers.DragManager;
	import mx.styles.*;

	import src.model.config.LabelConfig;
	import src.model.text.AbTextFormat;
	import src.model.util.*;
	import src.view.asset.AssetLayer;
	import src.view.highlight.HighlightFormat;
	import src.view.text.*;
	import src.view.util.Utilities;
	import src.view.drawing.ArchibusDrawing;



	/***
	 *
	 * AssetPanelDataGrid entends DataGrid. It's called by AssetsPanel.
	 * Implementation of a custom drag image.
	 *
	 */
	public class AssetPanelDataGrid extends DataGrid {
		//TODO
		[Embed(source='assets/img/drawing/pion.png')]
		static public var manImage:Class;

		public function AssetPanelDataGrid() {
			super();

			this.dragEnabled=true;
			this.dragMoveEnabled=true;
			this.percentWidth=100;
			this.percentHeight=100;
			this.sortableColumns=true;
			this.dropEnabled=true;
			//this.setStyle("paddingTop", -1);
			//this.setStyle("paddingBottom", 1);
			//this.allowMultipleSelection = true;
			this.addEventListener(DragEvent.DRAG_ENTER, dragEnterHandling);
			this.addEventListener(DragEvent.DRAG_START, dragStartHandling);
			this.addEventListener(DragEvent.DRAG_DROP, dropEventHandler);
			this.addEventListener(DragEvent.DRAG_OVER, dragOverEventHandler);
		}

		/**
		 * selectionColor
		 */
		override protected function drawRowBackground(s:Sprite, rowIndex:int, y:Number, height:Number, color:uint, dataIndex:int):void {
			//var item:Object;
			//if(dataIndex < dataProvider.length){
			//item = dataProvider[dataIndex];
			//}
			//if(item){
			super.drawRowBackground(s, rowIndex, y, height, 0xE6EEF6, dataIndex);
			//}else{
			//super.drawRowBackground(s, rowIndex, y, height, color, 
			//dataIndex); 
			//}
		}


		private function dragOverEventHandler(event:DragEvent):void {
			event.preventDefault();
		}

		private function dragEnterHandling(event:DragEvent):void {
			event.preventDefault();

			if (event.dragInitiator is AbTextField) {
				DragManager.showFeedback(DragManager.MOVE);
				DragManager.acceptDragDrop(event.currentTarget as UIComponent);
			} else {
				DragManager.showFeedback(DragManager.NONE);
			}
		}

		private function dropEventHandler(e:DragEvent):void {
			e.preventDefault();

			var abTextField:AbTextField=e.dragInitiator as AbTextField;
			var isRfresh:Boolean=abTextField.handleDropEvent(e.dragSource.dataForFormat("items"), [], e.dragSource.dataForFormat("table").toString(), null, false);
			if (isRfresh) {
				var assetPanel:AssetsPanel=this.parent as AssetsPanel;
				assetPanel.refresh();
			}		
		}

		//custom drag proxy
		private function dragStartHandling(e:DragEvent):void {
			e.preventDefault();

			var data:Object=e.currentTarget.selectedItem;
			var container:UIComponent=new UIComponent();
			var imageProxy:Image=new Image();
			imageProxy.source=manImage;
			imageProxy.width=15;
			imageProxy.height=15;
			container.addChild(imageProxy);
			container.owner=this;
			var label:UITextField=new UITextField();
			label.text=data.primaryKeysValue;
			label.x=18;

			container.addChild(label);
			container.x=e.localX;
			container.y=e.localY - 30;

			var ds:DragSource=new DragSource();
			ds.addData(data, "items");
			ds.addData(data.table, "table");

			DragManager.doDrag(e.currentTarget as UIComponent, ds, e, container);
		}

		/**
		 * getFromRecord.
		 */
		public function getFromRecord(passedRecord:Object):Array {
			var fromLocation:Array=null;
			if (passedRecord.hasOwnProperty("fieldNames")) {
				fromLocation=[];
				for (var j:int=0; j < passedRecord.fieldNames.length; j++) {
					var fieldName:String=passedRecord.fieldNames[j];
					for (var name:String in passedRecord) {
						var value:Object=passedRecord[name];
						name=name.replace("__", ".");
						if (fieldName == name) {
							var rec:Object={name: fieldName, value: value};
							fromLocation.push(rec);
							break;
						}
					}
				}
			}
			return fromLocation;
		}


		/**
		 * dropEventHandler.
		 */
		public function handleDropEvent(draggedItem:Object, roomIds:Array, assetType:String, al:AssetLayer):Boolean {
			var draggedRecord:Object=this.getFromRecord(draggedItem);
			if (draggedRecord != null) {
				var droppedRecord:Object={};
				droppedRecord.from=draggedRecord;
				droppedRecord.to=roomIds;
				var targetDrawing:ArchibusDrawing =  (al && al.parent)? (al.parent as ArchibusDrawing):this.parentApplication.dwgContainer.getChildAt(0);
				var dwgname:String = "";
				
				if(targetDrawing && targetDrawing.config){
					dwgname = targetDrawing.config.rawDwgName;
				}
				var droppable:Boolean=ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_ONASSETLOCATIONCHANGEHANLDER), droppedRecord, assetType, dwgname);
				if (droppable) {
					var tf:AbTextFormat=this.parentApplication.controlConfig.labelConfig.getTextFormat(LabelConfig.FORMAT_DEFAULT);
					var ids:Array=[];
					for (var i:int=0; i < roomIds.length; i++) {
						ids.push(roomIds[i].value);
					}
					
					var labLayer:LabelLayer=targetDrawing.labelLayer;
					var lo:LabelObject=labLayer.getLabel(ids);
					if (lo == null) {
						lo=labLayer.createLabelObject(ids, draggedItem);
						lo.addField("rm.rm_id", ids[2], tf, roomIds);
					}
					
					lo.addField(draggedRecord[0].name, draggedRecord[0].value, tf, Utilities.formatValues(draggedRecord, ids));
					labLayer.refresh();

					if (ids.length > 0 && al != null) {
						al.highlightRoom([ids[0], ids[1], ids[2]], HighlightFormat.BORDER);
					}
					return true;
				} else {
					DragManager.showFeedback(DragManager.NONE);
				}
			} else {
				DragManager.showFeedback(DragManager.NONE);
			}

			return false;

		}
	}
}


