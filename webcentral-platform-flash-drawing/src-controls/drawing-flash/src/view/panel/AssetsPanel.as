package src.view.panel {
	import flash.display.DisplayObject;
	import flash.events.*;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.text.TextFormat;

	import mx.collections.ArrayCollection;
	import mx.containers.*;
	import mx.containers.ControlBar;
	import mx.containers.HBox;
	import mx.containers.Panel;
	import mx.containers.TitleWindow;
	import mx.controls.*;
	import mx.controls.DataGrid;
	import mx.controls.LinkButton;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.controls.dataGridClasses.DataGridDragProxy;
	import mx.core.ClassFactory;
	import mx.core.DragSource;
	import mx.core.IUIComponent;
	import mx.core.UIComponent;
	import mx.core.UITextField;
	import mx.effects.Resize;
	import mx.events.CloseEvent;
	import mx.events.DragEvent;
	import mx.managers.DragManager;
	import mx.managers.PopUpManager;

	import src.model.data.DataSource;
	import src.model.data.SourceType;
	import src.model.util.*;
	import src.view.text.LabelObject;



	/**
	 * A new Asset Panel which will be popup so that users could drag a record to a room.
	 *
	 */
	public class AssetsPanel extends TitleWindow {
		private var actionsContiner:HBox=new HBox();
		private var dataGrid:DataGrid;
		private var dataSourceName:String;
		private var fieldDefs:Array;
		private var barActions:Array=[];
		private var filterContiner:HBox=new HBox();
		private var filterText:TextInput=new TextInput();
		private var filterOn:Boolean=false;

		private var restriction:String=null;


		private var minMaxBtn:Button=null;

		private var effResize:Resize=new Resize();

		private var previousHeight:int=30;

		private var collapsible:Boolean=true;
		
		private var isCreated:Boolean = false;

		[Embed(source='assets/img/drawing/toggle_expand.png')]
		public static var Down:Class;

		[Embed(source='assets/img/drawing/toggle.png')]
		public static var Up:Class;

		private var minimize:String = "Minimize";
		private var maximize:String = "Maximize";
		
		private var filterTip1:String = "Filters the first column.";
		private var filterTip2:String = "Filter";

		/**
		 * Called by JS to set up asset panel's datasource
		 */
		public function setDataSourceName(dataSourceName:String):void {
			this.dataSourceName=dataSourceName;

		}

		/***
		 * Turn on/off toolbar's filter.
		 */
		public function setTitleBarFilterOn(filterOn:Boolean):void {
			this.filterOn=filterOn;
		}

		/**
		 * Called by JS to set up title bar actions
		 *
		 */
		public function setBarActions(actions:Array):void {
			barActions=actions;
		}

		/**
		 * Set up asset panel's title
		 */
		public function setTitle(title:String):void {
			this.title=title;
		}

		public function AssetsPanel(configOptions:Object=null) {
			super();
			var localizeUtilities:LocalizeUtilities=new LocalizeUtilities();
			if(localizeUtilities.map["TOOLTIP_MINIMIZE"] && localizeUtilities.map["TOOLTIP_MINIMIZE"]!=""){
				minimize = localizeUtilities.map["TOOLTIP_MINIMIZE"];
			}
			if(localizeUtilities.map["TOOLTIP_MAXIMIZE"] && localizeUtilities.map["TOOLTIP_MAXIMIZE"]!=""){
				maximize = localizeUtilities.map["TOOLTIP_MAXIMIZE"];
			}
			if(localizeUtilities.map["TOOLTIP_FILTER_COLUMN"] && localizeUtilities.map["TOOLTIP_FILTER_COLUMN"]!=""){
				filterTip1 = localizeUtilities.map["TOOLTIP_FILTER_COLUMN"];
			}
			if(localizeUtilities.map["TOOLTIP_FILTER"] && localizeUtilities.map["TOOLTIP_FILTER"]!=""){
				filterTip2 = localizeUtilities.map["TOOLTIP_FILTER"];
			}
			
			
			this.showCloseButton=false;
			var width:int=500;
			var height:int=300;
			var fontFamily:String="Arial";
			var fontSize:String="11";
			var selectionColor:String=null;
			if (configOptions != null) {
				//closable
				if (configOptions.hasOwnProperty("closable")) {
					this.showCloseButton=configOptions.closable;
				}

				//collapsible
				if (configOptions.hasOwnProperty("collapsible")) {
					this.collapsible=configOptions.collapsible;
				}


				//size
				if (configOptions.hasOwnProperty("size")) {
					if (configOptions.size.hasOwnProperty("width")) {
						width=configOptions.size.width;
					}
					if (configOptions.size.hasOwnProperty("height")) {
						height=configOptions.size.height;
					}

				}
				//actions
				if (configOptions.hasOwnProperty("actions")) {
					setBarActions(configOptions.actions);
				}

				//filterOn
				if (configOptions.hasOwnProperty("filter")) {
					setTitleBarFilterOn(configOptions.filter);
				}

				if (configOptions.hasOwnProperty("font") && configOptions.font.hasOwnProperty("fontFamily")) {
					fontFamily=configOptions.font.fontFamily;
				}
				if (configOptions.hasOwnProperty("font") && configOptions.font.hasOwnProperty("fontSize")) {
					fontSize=configOptions.font.fontSize;
				}
				if (configOptions.hasOwnProperty("selectionColor")) {
					selectionColor=configOptions.selectionColor;
				}
				if (configOptions.hasOwnProperty("restriction")) {
					restriction=configOptions.restriction;
				}
			}

			if (this.collapsible) {
				minMaxBtn=new LinkButton();

				minMaxBtn.setStyle("icon", Up);
				minMaxBtn.toolTip=minimize
				minMaxBtn.addEventListener(MouseEvent.CLICK, minimisePanel);
				minMaxBtn.height=20;
				minMaxBtn.width=22;
			}


			this.width=width;
			this.height=height;

			this.setStyle("borderAlpha", 1);
			this.setStyle("fontFamily", fontFamily);
			this.setStyle("fontSize", fontSize);
			this.setStyle("borderThicknessBottom", 0);
			this.setStyle("borderThicknessRight", 0);
			this.setStyle("borderThicknessLeft", 0);
			//this.setStyle("headerColor", "#C0C0C0" );


			dataGrid=new AssetPanelDataGrid();

			if (selectionColor != null && selectionColor.length > 0) {
				dataGrid.setStyle("rollOverColor", selectionColor);
				dataGrid.setStyle("selectionColor", selectionColor);
					//dataGrid.setStyle("backgroundColor", selectionColor);
			}

			dataGrid.setStyle("backgroundColor", 0xE6EEF6);
			dataGrid.setStyle("alternatingItemColors", [0xF7F7F7, 0xF7F7F7]);
			dataGrid.setStyle("backgroundAlpha", 0.5);

		}

		/**
		 * Minimzes asset panel.
		 */
		private function minimisePanel(e:MouseEvent):void {
			effResize.stop();
			minMaxBtn.removeEventListener(MouseEvent.CLICK, minimisePanel);
			//var down:Image = new Image();
			//down.source = "@Embed(source='assets/img/toggle_expand.png')";
			minMaxBtn.setStyle("icon", Down);
			minMaxBtn.toolTip= maximize;

			minMaxBtn.addEventListener(MouseEvent.CLICK, maxmisePanel);
			effResize.heightFrom=height;
			effResize.heightTo=previousHeight;
			previousHeight=height;
			effResize.play([this]);
			this.move(0, this.parentApplication.dwgContainer.height - 30);
		}

		/**
		 *  Maximzes asset panel.
		 */
		private function maxmisePanel(e:MouseEvent):void {
			this.move(0, this.parentApplication.dwgContainer.height - previousHeight);
			effResize.stop();
			minMaxBtn.removeEventListener(MouseEvent.CLICK, maxmisePanel);
			minMaxBtn.setStyle("icon", Up);
			minMaxBtn.toolTip= minimize;
			minMaxBtn.addEventListener(MouseEvent.CLICK, minimisePanel);
			effResize.heightFrom=height;
			effResize.heightTo=previousHeight;
			previousHeight=height;
			effResize.play([this]);
		}

		/**
		 * Title and title bar actions must be set up during asset panel creation
		 *
		 */
		override protected function createChildren():void {
			if(!isCreated){
				super.createChildren();
				
				addTitleBarActions();
				
				var datasource:DataSource=new DataSource(SourceType.HILITE, this.dataSourceName);
				
				var fieldDefObjects:Object=datasource.getFieldDefs();
				var fieldDefs:Object=fieldDefObjects["map"];
				var fieldNames:Object=fieldDefObjects["keys"];
				setColumns(fieldDefs, fieldNames);
				setdataRows(datasource, fieldDefs, fieldNames, restriction);
				
				addChild(dataGrid);
				
				this.addEventListener(CloseEvent.CLOSE, close);
			}
		}

		/**
		 * Add title bar actions
		 */
		private function addTitleBarActions():void {
			if (this.filterOn) {
				addFilter();
			}

			actionsContiner.setStyle("horizontalAlign", "right");
			actionsContiner.setStyle("verticalAlign", "center");
			actionsContiner.minHeight=20;
			actionsContiner.minWidth=250;

			if (barActions != null) {
				for (var t:int=0; t < barActions.length; t++) {
					var action:Object=barActions[t];
					addTitleBarButton(action);
				}
			}

			if (this.minMaxBtn) {
				actionsContiner.addChild(this.minMaxBtn);
			}


			this.titleBar.addChild(actionsContiner);
		}

		private function addFilter():void {
			filterContiner.toolTip=filterTip1;
			var filterLabel:Label=new Label();
			filterLabel.text=filterTip2;
			filterLabel.setStyle("fontFamily", "Arial");
			filterLabel.setStyle("fontWeight", "bold");
			filterLabel.setStyle("fontSize", "11");
			filterContiner.addChild(filterLabel);
			filterText.width=80;
			filterText.height=20;
			filterContiner.addChild(filterText);

			filterText.addEventListener(KeyboardEvent.KEY_DOWN, filterFirstColumn);

			filterText.addEventListener(MouseEvent.MOUSE_DOWN, disablePanelDrag);
			filterText.addEventListener(MouseEvent.MOUSE_OUT, enablePanelDrag);

			actionsContiner.addChild(filterContiner);
		}

		private function disablePanelDrag(event:MouseEvent):void {
			this.isPopUp=false;
		}

		private function enablePanelDrag(event:MouseEvent):void {
			this.isPopUp=true;
		}

		private function filterFirstColumn(event:KeyboardEvent=null):void {
			var data:ArrayCollection=dataGrid.dataProvider as ArrayCollection;
			data.filterFunction=filterIt;
			data.refresh();
		}

		private function filterIt(item:Object):Boolean {
			var firstColName:String=item.fieldNames[0];
			firstColName=firstColName.replace(".", "__");
			var pattern:RegExp=new RegExp("[^]*" + filterText.text + "[^]*", "i");
			return pattern.test(item[firstColName]);
		}

		/**
		 * Called by JS to refresh asset panel
		 */
		public function refresh(restriction:Object=null):void {
			var datasource:DataSource=new DataSource(SourceType.HILITE, this.dataSourceName);
			var fieldDefObjects:Object=datasource.getFieldDefs();
			var fieldDefs:Object=fieldDefObjects["map"];
			var fieldNames:Object=fieldDefObjects["keys"];
			setdataRows(datasource, fieldDefs, fieldNames, restriction);
			if (this.filterOn && filterText.text.length > 0) {
				filterFirstColumn();
			}

		}

		/**
		 * Set up panel's columns
		 */
		private function setColumns(fieldDefs:Object, fieldNames:Object):void {
			var cols:Array=dataGrid.columns;

			for (var i:int=0; i < fieldNames.length; i++) {
				var fieldName:String=fieldNames[i];
				var fieldDef:Object=fieldDefs[fieldName];
				if (fieldDef.hidden == "false") {
					var name:String=fieldDef.id;
					name=name.replace(".", "__");
					var dgc:DataGridColumn=new DataGridColumn();
					dgc.headerText=fieldDef.title;
					dgc.dataField=name;
					dgc.resizable=true;
					cols.push(dgc);
				}
			}

			dataGrid.columns=cols;
		}

		/**
		 * Set up panel's data rows
		 */
		private function setdataRows(datasource:DataSource, fieldDefs:Object, fieldNames:Object, restriction:Object=null):void {
			datasource.loadRecords(null, restriction);
			var cnt:int=datasource.numberOfRecords();
			var data:Array=[];
			var tableName:String;
			for (var i:int=0; i < cnt; i++) {
				var record:Object=datasource.getRecord(i);
				var row:Object={};
				var primaryKeysValue:String="";

				for (var j:int=0; j < fieldNames.length; j++) {
					var fieldName:String=fieldNames[j];
					tableName=fieldName.substring(0, fieldName.indexOf("."));

					var fieldDef:Object=fieldDefs[fieldName];
					var value:Object=record[fieldDef.id];
					if (fieldDef.hidden == "false") {
						var name:String=fieldDef.id;
						//XXX: Flex column doesn't support column name with "."
						name=name.replace(".", "__");
						row[name]=value;
					}
					if (fieldDef.primaryKey == "true" || fieldDef.primaryKey) {
						if (primaryKeysValue != "") {
							primaryKeysValue+=";";
						}
						primaryKeysValue+=value;
					}
				}
				row["primaryKeysValue"]=primaryKeysValue;
				row["fieldNames"]=fieldNames;
				row["table"]=tableName;
				data.push(row);
			}

			dataGrid.dataProvider=data;
		}

		/**
		 *
		 * Called by JS to add row action
		 */
		public function addRowAction(actionConfig:Object):void {
			var cols:Array=dataGrid.columns;

			var dgc:DataGridColumn=new DataGridColumn("Button");
			dgc.draggable=false;

			dgc.headerText="";

			var buttonRenderer:ClassFactory;

			if (actionConfig.hasOwnProperty("icon")) {
				buttonRenderer=new ClassFactory(ImageRenderer);
				buttonRenderer.properties={label: actionConfig.title, toolTip: actionConfig.title, key: this.dataSourceName, actionID: actionConfig.actionID, imageSource: actionConfig.icon};
				dgc.width=60;
				dgc.setStyle("paddingLeft", 25)
			} else {
				buttonRenderer=new ClassFactory(ActionButtonItemRenderer);
				buttonRenderer.properties={label: actionConfig.title, toolTip: actionConfig.title, key: this.dataSourceName, actionID: actionConfig.actionID};
				dgc.width=120;
			}

			dgc.itemRenderer=buttonRenderer;

			cols.push(dgc);

			dataGrid.columns=cols;
		}

		/**
		 * Position title bar actions
		 *
		 */
		override protected function layoutChrome(w:Number, h:Number):void {
			super.layoutChrome(w, h);

			var width:Number=actionsContiner.getExplicitOrMeasuredWidth();
			var height:Number=actionsContiner.getExplicitOrMeasuredHeight();
			actionsContiner.setActualSize(width, height);

			if (this.showCloseButton) {
				var closeButton:Button=this.getCloseButton();
				var x:Number=closeButton.x - width;
				var y:Number=closeButton.y - Math.floor((height - closeButton.height) * 0.50);
				actionsContiner.move(x, y + 1.8);
			} else {
				actionsContiner.move(w - width - 10, 6);
			}

		}

		private function getCloseButton():Button {
			for (var i:int=0; i < this.titleBar.numChildren; ++i) {
				if (this.titleBar.getChildAt(i) is Button && this.titleBar.getChildAt(i) != actionsContiner) {
					return this.titleBar.getChildAt(i) as Button;
				}
			}
			return null;
		}

		private function addNew(event:MouseEvent, actionID:String):void {
			ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_ONASSETPANELTITLEBARACTION), actionID, this.dataSourceName);
		}

		private function addArguments(method:Function, methodArguments:Array):Function {
			return function(event:Event):void {
				method.apply(null, [event].concat(methodArguments));
			}
		}

		private function addArgumentsImage(method:Function, methodArguments:Array):Function {
			return function(event:Event):void {
				this.isPopUp=false;
				method.apply(null, [event].concat(methodArguments));
			}
		}

		/**
		 * Add one title bar action
		 */
		private function addTitleBarButton(action:Object):void {
			if (action.hasOwnProperty("icon")) {
				var img:Image=new Image();
				img.source=action.icon;
				img.toolTip=action.title;

				img.addEventListener(MouseEvent.MOUSE_DOWN, addArgumentsImage(addNew, [action.actionID]));
				img.addEventListener(MouseEvent.MOUSE_OVER, disablePanelDrag);
				img.addEventListener(MouseEvent.MOUSE_OUT, enablePanelDrag);
				actionsContiner.addChild(img);
			} else {
				var addNewButton:LinkButton=new LinkButton();
				addNewButton.label=action.title;
				addNewButton.addEventListener(MouseEvent.CLICK, addArguments(addNew, [action.actionID]));
				actionsContiner.addChild(addNewButton);
			}
		}


		/**
		 * Close popup asset panel
		 */
		public function close(evt:CloseEvent=null):void {
			PopUpManager.removePopUp(this);
		}

		/**
		 * Show up asset panel
		 */
		public function show(continer:DisplayObject):void {
			isCreated = true;
			close();
			PopUpManager.addPopUp(this, continer);
		}
	}
}


