package src.view.panel {
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import mx.controls.Image;
	import mx.controls.Button;
	import mx.controls.listClasses.IDropInListItemRenderer;
	import mx.controls.listClasses.IListItemRenderer;
	import mx.controls.listClasses.ListBaseContentHolder;
	import mx.core.IFlexDisplayObject;

	import src.model.util.*;

	public class ActionButtonItemRenderer extends Button implements IFlexDisplayObject, IListItemRenderer, IDropInListItemRenderer {
		public var key:String=null;
		public var actionID:String=null;

		public function ActionButtonItemRenderer() {

			this.addEventListener(MouseEvent.CLICK, clickHandler);
		}

		override protected function clickHandler(event:MouseEvent):void {
			super.clickHandler(event);

			var record:Array=[];
			for (var name:String in this.data) {
				var value:Object=this.data[name];
				name=name.replace("__", ".");
				if (name.indexOf(".") > 0) {
					record.push({name: name, value: value});
				}
			}
			ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_ONASSETPANELROWACTION), actionID, record, this.key);
		}
	}
}


