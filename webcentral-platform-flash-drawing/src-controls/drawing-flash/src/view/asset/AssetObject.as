package src.view.asset {
	import flash.display.*;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.geom.Point;
	import flash.text.TextFormat;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	
	import mx.controls.Alert;
	import mx.controls.Image;
	import mx.core.UIComponent;
	import mx.events.DragEvent;
	import mx.managers.DragManager;
	
	import src.controller.assign.AssignedObject;
	import src.model.asset.AssetProperties;
	import src.model.config.LabelConfig;
	import src.model.text.AbTextFormat;
	import src.model.util.CommonUtilities;
	import src.model.util.JsFunctionConstant;
	import src.view.drawing.ArchibusDrawing;
	import src.view.highlight.FillFormat;
	import src.view.highlight.FillTypes;
	import src.view.highlight.HighlightFormat;
	import src.view.panel.AssetPanelDataGrid;
	import src.view.text.*;
	import src.view.text.AbTextField;


	/**
	 * Defines component representing an Archibus Asset Layer.
	 * This layer should be used in combination with the architectural background.
	 * By displaying this layer on top of the architectural background and synchronizing
	 * both layers, one is able to find and highlight the assets on the architectural
	 * background.
	 */
	public class AssetObject extends AssetFill {
		/**
		 * When vertecies of the polygon are compared to see if one is higher or lower (or more to th
		 * left or right), two vertices can be visually at the same position, but differ jsut a bit in
		 * coordinates. This factor is used to eliminate this small precision difference.
		 * Currently it is set to .01, which means vertices that differ less then 1% in width or height
		 * are considered equal.
		 */
		private static const VERTEX_COMPARISON_PRECISON_FACTOR:Number=.01;

		private var _properties:AssetProperties=null;

		private var _vertices:Array=[];

		private var _ids:Array=null;

		public var selected:Boolean=false;

		public var assignedObject:AssignedObject=null;
		
		private var _assetType:String;
		
		public function setAssetType(type:String):void{
			_assetType = type;
		}
		
		public function getAssetType():String{
			return _assetType;
		}


		public function AssetObject(jsonData:Object):void {
			super();
			this.cacheAsBitmap=true;
			this.properties=new AssetProperties(jsonData);

			this.ids=jsonData.ids;
			this.addEventListener(DragEvent.DRAG_ENTER, dragEnterHandler);
			this.addEventListener(DragEvent.DRAG_DROP, dragDropHandler);
		}

		private function onMouseOver(e:MouseEvent):void {
			var ad:ArchibusDrawing=this.parent.parent as ArchibusDrawing;

			var lo:LabelObject=ad.labelLayer.getLabel(_ids);

			for each (var textField:AbTextField in lo.getTextFields()) {

				var format:TextFormat=new TextFormat();
				format.color=0xFF0000;
				format.bold=true;
				textField.getTextField().setTextFormat(format);
			}
		}

		private function onMouseOut(e:MouseEvent):void {
			var ad:ArchibusDrawing=this.parent.parent as ArchibusDrawing;

			var lo:LabelObject=ad.labelLayer.getLabel(_ids);

			for each (var textField:AbTextField in lo.getTextFields()) {

				var format:TextFormat=new TextFormat();
				format.color=0x000000;
				format.bold=false;
				textField.getTextField().setTextFormat(format);
			}
		}


		/////////////////////////////////////drop events//////////////////////////////////
		public function dragEnterHandler(dragEvent:DragEvent):void {
			dragEvent.preventDefault();
			//TODO: check if needing to check room's isSelectable???
			if (this.properties.isSelectable) {
				if (((dragEvent.dragInitiator is src.view.panel.AssetPanelDataGrid) || (dragEvent.dragInitiator is src.view.text.AbTextField)) && dragEvent.dragSource != null) {
					var dropTarget:UIComponent=dragEvent.currentTarget as UIComponent;
					DragManager.showFeedback(DragManager.MOVE);
					DragManager.acceptDragDrop(dropTarget);
				} else {
					DragManager.showFeedback(DragManager.NONE);
				}
			} else {
				DragManager.showFeedback(DragManager.NONE);
			}
		}

		public function dragDropHandler(dragEvent:DragEvent):void {
			dragEvent.preventDefault();
			if (dragEvent.dragInitiator is src.view.panel.AssetPanelDataGrid) {
				var assetPanelDataGrid:src.view.panel.AssetPanelDataGrid=dragEvent.dragInitiator as src.view.panel.AssetPanelDataGrid;
				assetPanelDataGrid.handleDropEvent(dragEvent.dragSource.dataForFormat("items"), this.getToLocation(), dragEvent.dragSource.dataForFormat("table").toString(), this.parent as AssetLayer);
			} else if (dragEvent.dragInitiator is AbTextField) {
				var abTextField:AbTextField=dragEvent.dragInitiator as AbTextField;
				abTextField.handleDropEvent(dragEvent.dragSource.dataForFormat("items"), this.getToLocation(), dragEvent.dragSource.dataForFormat("table").toString(), this.parent as AssetLayer, true);
			} else {
				DragManager.showFeedback(DragManager.NONE);
			}
		}

		/////////////////////////////////////end drop events//////////////////////////////////

		private function getToLocation():Array {
			var toLocation:Array=[];
			if (this.ids != null) {
				var val:Object={name: "rm.bl_id", value: this.ids[0]};
				toLocation.push(val);
				val={name: "rm.fl_id", value: this.ids[1]};
				toLocation.push(val);
				val={name: "rm.rm_id", value: this.ids[2]};
				toLocation.push(val);
			}

			return toLocation;
		}

		////////////////////////////////////////end of drop event//////////////////////////

		public function setAssignedObject(assignedObject:AssignedObject):void {
			this.assignedObject=assignedObject;
		}

		public function getAssignedObject():AssignedObject {
			return this.assignedObject;
		}

		public function clear():void {
			if(fill!=null){
				fill.clear();
			}
		}

		public function create(af:FillFormat, resetFill:Boolean=false, usePrevious:Boolean=false, al:AssetLayer=null, highlightFormat:HighlightFormat=null, doHilighting:Boolean=true):void {
			if (this.properties == null) {
				return;
			}

			//XXX:???
			if (highlightFormat != null && highlightFormat.getHighlightFormat() == HighlightFormat.BORDER) {
				resetFill=false;
				if (af && this._previousFillFormat) {
					this._previousFillFormat.boundaryColor=af.boundaryColor;
					this._previousFillFormat.boundaryThickness=af.boundaryThickness;
					this._previousFillFormat.boundaryOpacity=af.boundaryOpacity;
					this._previousFillFormat.highlightType=af.highlightType;
				}
			}

			if (resetFill || (af != null && af.id == 'default')) {
				this._previousFillFormat=null;
			}

			if (!usePrevious && af != null) {
				if (af.id == FillTypes.SELECTED) {
					this.properties.isSelected=true;
				} else if (af.id == FillTypes.UNSELECTED) {
					this.properties.isSelected=false;
				}
			}

			if (this._previousFillFormat && (af == null || (af.id == FillTypes.UNSELECTED))) {
				af=this._previousFillFormat;
			}

			if (af == null) {
				return;
			}

			//if ((selectionMode == 0) || (selectionMode == 1 && af.id == FillTypes.UNASSIGNED)) {
			if (af.id == FillTypes.UNASSIGNED) {
				if (al == null) {
					al=this.parent as AssetLayer;
				}

				if (al != null) {
					var ab:ArchibusDrawing=al.parent as ArchibusDrawing;
					if(ab != null && ab.config != null){
						var selectMode:int = ab.config.selectionMode;
						if (selectMode == 0 || selectMode == 1) {
							this.properties.isSelectable=false;
						}
					}
				}
			}

			if (this.fillFormat == null) {
				this.fillFormat=this.parentApplication.controlConfig.highlightConfig.getFillFormat(FillTypes.UNASSIGNED);
			}
			this.fillFormat.copy(af);
			if (this.properties.jsonData == null || this.properties.jsonData.swfGraphics == null || this.properties.jsonData.swfGraphics.length == 0) {
				return;
			}

			var currentPoint:Point=new Point(this.properties.jsonData.swfGraphics[0].startX, this.properties.jsonData.swfGraphics[0].startY);

			// create fill to draw asset on
			this.createFill(af);
			
			fill.beginFill(highlightFormat, doHilighting);
			
			fill.moveTo(currentPoint.x, currentPoint.y);
			for (var j:Number=0; j < this.properties.jsonData.swfGraphics.length; j++) {
				var segment:Object=this.properties.jsonData.swfGraphics[j];
				if (segment.type == "line") {
					fill.lineTo(segment.startX, segment.startY);
					fill.lineTo(segment.endX, segment.endY);
				} else {
					fill.lineTo(segment.startX, segment.startY);
					fill.curveTo(segment.controlX, segment.controlY, segment.endX, segment.endY);
				}
				this._vertices.push(new Point(segment.startX, segment.startY));
				this._vertices.push(new Point(segment.endX, segment.endY));
			}
			
			fill.endFill(highlightFormat, doHilighting);
		
			if (!this._previousFillFormat) {
				this._previousFillFormat=this.parentApplication.controlConfig.highlightConfig.getFillFormat(FillTypes.UNASSIGNED);
				this._previousFillFormat.copy(af);
			}
		}

		/**
		 * Returns a corner vertex. Which is, from all vertices most upper (if upper is true, else most
		 * down), the one most to the left (if left is true, else right).
		 *
		 * To eliminate small differences between positions of vertices, they are compared with a
		 * delta difference.
		 */
		public function getCornerVertex(upper:Boolean, left:Boolean):Point {
			var res:Point=this._vertices[0];
			var delta:Number=res.y * VERTEX_COMPARISON_PRECISON_FACTOR;
			for (var i:uint=1; i < this._vertices.length; ++i) {
				if (this._vertices[i].y < res.y - delta) { // vertex is higher
					if (upper) {
						res=this._vertices[i];
						delta=res.y * VERTEX_COMPARISON_PRECISON_FACTOR;
					}
				} else if (this._vertices[i].y > res.y + delta) { // vertex is lower
					if (!upper) {
						res=this._vertices[i];
						delta=res.y * VERTEX_COMPARISON_PRECISON_FACTOR;
					}
				} else { // vertix is (more or less) at same y-position
					delta=res.x * VERTEX_COMPARISON_PRECISON_FACTOR;
					if (this._vertices[i].x < res.x - delta) { // vertex is more to the left
						if (left) {
							res=this._vertices[i];
							delta=res.x * VERTEX_COMPARISON_PRECISON_FACTOR;
						}
					} else if (this._vertices[i].x > res.x + delta) { // vertex is more to the right
						if (!left) {
							res=this._vertices[i];
							delta=res.x * VERTEX_COMPARISON_PRECISON_FACTOR;
						}
					}
				}
			}
			return res;
		}

		/**
		 * Returns the vertex closest to the center of the bounds.
		 *
		 * If more vertices are (more or less) at the same distance, return the one most to the top.
		 */
		public function getMostCentralVertex():Point {
			// calculate center
			var lu:Point=new Point(this._vertices[0].x, this._vertices[0].y);
			var rl:Point=new Point(this._vertices[0].x, this._vertices[0].y);
			for (var i:uint=1; i < this._vertices.length; ++i) {
				lu.x=Math.min(lu.x, this._vertices[i].x);
				lu.y=Math.min(lu.y, this._vertices[i].y);
				rl.x=Math.max(rl.x, this._vertices[i].x);
				rl.y=Math.max(rl.y, this._vertices[i].y);
			}
			var center:Point=new Point((lu.x + rl.x) / 2, (lu.y + rl.y) / 2);

			// find vertex
			var minDist:Number=Number.POSITIVE_INFINITY;
			var res:Point=new Point(Number.POSITIVE_INFINITY, Number.POSITIVE_INFINITY);
			var delta:Number=0;
			for each (var p:Point in this._vertices) {
				var dist:Number=Point.distance(p, center);
				// if it is closer or it is just about as close and is more to the top
				if ((dist < minDist - delta) || ((dist < minDist + delta) && p.y < res.y)) {
					minDist=dist;
					delta=minDist * VERTEX_COMPARISON_PRECISON_FACTOR;
					res=p;
				}
			}

			return res;
		}

		public function redraw():void {
			this.fill.redraw();
		}

		public function resetToInitial():void {
			if (this._previousFillFormat == null)
				return;

			clear();
			this.properties=new AssetProperties(null);
			create(null, false, true);
		}

		public function setColor(af:FillFormat, resetFill:Boolean=false, highlightFormat:HighlightFormat=null, isClear:Boolean=true, doHilighting:Boolean=true):void {
			if(isClear && isClearUp(af, highlightFormat)){
				this.clear();
			}
			this.create(af, resetFill, false, null, highlightFormat, doHilighting);
		}

		private function isClearUp(af:FillFormat, highlightFormat:HighlightFormat=null):Boolean{
			//UNSELECTED or  UNASSIGNED
			if(af != null && (af.id == FillTypes.UNSELECTED || af.id == FillTypes.UNASSIGNED)){
				if(highlightFormat==null || highlightFormat.getHighlightFormat()==HighlightFormat.REGULAR) 
					return true;
			}

			//_previousFillFormat.fillColor is different from current af.fillColor 
			if(af != null && this._previousFillFormat!=null && af.fillColor != this._previousFillFormat.fillColor){
				if(highlightFormat==null || highlightFormat.getHighlightFormat()==HighlightFormat.REGULAR) 
					return true;
			}

			return false;
		}

		/**
		 * Selects Asset
		 */
		public function select(af:FillFormat=null, color:String=null):void {
			if (this.properties.jsonData) {
				if (!af) {
					var al:AssetLayer=this.parent as AssetLayer;
					af=al.parentApplication.controlConfig.highlightConfig.getFillFormat(FillTypes.SELECTED);
				}

				//XXX: ????
				var _displayDiagonalSelectionPattern:Boolean=ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_GETDISPLAYDIAGONALSELECTIONPATTERN));
				if (_displayDiagonalSelectionPattern) {
					af.copy(this._previousFillFormat);
					//af.fillColor = this._previousFillFormat.fillColor;
					//af.fillOpacity = this._previousFillFormat.fillOpacity;

					//af.boundaryColor = this._previousFillFormat.boundaryColor;
					if (this._previousFillFormat.boundaryThickness == src.view.highlight.Fill.BORDERTHICKNESS) {
						af.boundaryThickness=this._previousFillFormat.boundaryThickness;
					} else {
						af.boundaryThickness=src.view.highlight.Fill.BORDERTHICKNESS - 1;
					}

					af.setborderHatched(true);
				}

				if (color != null && color.length > 0) {
					af.fillColor=parseInt(color, 10);
				}

				//XXX: clear fill
				this.clear();
				this.setColor(af, false);

				this.properties.isSelected=true;
			}
		}

		/**
		 * Un-selects asset
		 */
		public function unselect():void {
			if (this.properties.jsonData) {
				//XXX: clear fill
				this.clear();
				var af:FillFormat=this._previousFillFormat;
				this.setColor(af, false);

				this.properties.isSelected=false;
			}
		}

		public function get properties():AssetProperties {
			return _properties;
		}

		public function set properties(value:AssetProperties):void {
			_properties=value;
		}

		public function set ids(newIds:Array):void {
			this._ids=newIds;
		}

		public function get ids():Array {
			return this._ids;
		}

	}

}


