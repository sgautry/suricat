package src.view.asset {
	import flash.geom.Point;

	import mx.core.UIComponent;

	import src.view.highlight.*;

	/**
	 * Defines visual atributes of the asset fill such as color, opacity, boundary etc.
	 * Also Records the old and new fill format.
	 *
	 */
	public class AssetFill extends UIComponent {
		/**
		 * current fill format
		 */
		protected var _fillFormat:FillFormat=null;

		/**
		 * previous fill format
		 */
		protected var _previousFillFormat:FillFormat=null;

		/**
		 * fill interface to call different fill actions.
		 */
		protected var _fill:IFill;

		/**
		 * Creates a fill for the specified component, based on the settings in this assetFormat
		 */
		public function createFill(af:FillFormat):void {
			if (af.patternName == null) {
				this._fill=new SolidFill(this, af);
			} else {
				this._fill=new HatchedFill(this, af);
			}
		}

		/**
		 * fills the asset object with specifying fill format.
		 *
		 * @param asset the asset to be filled
		 * @param ff the fill format
		 *
		 */
		protected function fillAsset(asset:Object, ff:FillFormat, vertices:Array):Array {
			var currentPoint:Point=new Point(asset.swfGraphics[0].startX, asset.swfGraphics[0].startY);

			// create fill to draw asset on
			this.createFill(ff);

			this._fill.beginFill();
			this._fill.moveTo(currentPoint.x, currentPoint.y);
			for (var j:Number=0; j < asset.swfGraphics.length; j++) {
				var segment:Object=asset.swfGraphics[j];
				drawSegment(segment);
				vertices.push(new Point(segment.startX, segment.startY));
				vertices.push(new Point(segment.endX, segment.endY));
			}
			this._fill.endFill();

			return vertices;
		}

		/**
		 * Draws the line segment.
		 *
		 * @param segment the segment to draw.
		 *
		 */
		private function drawSegment(segment:Object):void {
			if (segment.type == "line") {
				_fill.lineTo(segment.startX, segment.startY);
				_fill.lineTo(segment.endX, segment.endY);
			} else {
				_fill.lineTo(segment.startX, segment.startY);
				_fill.curveTo(segment.controlX, segment.controlY, segment.endX, segment.endY);
			}
		}

		/**
		 * get the previous fill color.
		 *
		 * @return the previous fill color.
		 *
		 */
		public function get previousFillColor():uint {
			var color:uint=0;

			if (_previousFillFormat != null)
				color=_previousFillFormat.fillColor;

			return color;
		}

		/**
		 * getter for fill.
		 *
		 * @return the fill object.
		 *
		 */
		public function get fill():IFill {
			return _fill;
		}

		/**
		 * getter for current fill format.
		 *
		 * @return the current fill format.
		 *
		 */
		public function get fillFormat():FillFormat {
			return _fillFormat;
		}

		/**
		 * setter for current fill format.
		 *
		 * @param value the fill format to set.
		 *
		 */
		public function set fillFormat(value:FillFormat):void {
			_fillFormat=value;
		}
	}
}
