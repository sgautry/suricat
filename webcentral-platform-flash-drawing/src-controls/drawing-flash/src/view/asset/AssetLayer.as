package src.view.asset {
	import mx.collections.ArrayCollection;
	import mx.controls.*;
	import mx.core.UIComponent;
	
	import src.controller.assign.AssignedObject;
	import src.model.asset.AssetMap;
	import src.model.asset.AssetProperties;
	import src.model.assign.*;
	import src.model.config.*;
	import src.model.util.CommonUtilities;
	import src.view.drawing.ArchibusDrawing;
	import src.view.highlight.Fill;
	import src.view.highlight.FillFormat;
	import src.view.highlight.FillTypes;
	import src.view.highlight.HighlightFormat;
	import src.controller.toolbar.ToolbarMode;

	/**
	 * Defines component representing an Archibus Asset Layer.
	 * This layer should be used in combination with the architectural background.
	 * By displaying this layer on top of the architectural background and synchronizing
	 * both layers, one is able to find and highlight the assets on the architectural
	 * background.
	 */
	public class AssetLayer extends UIComponent {

		/**
		 * Constructor
		 */
		public function AssetLayer() {
			super();
		}


		// an array of ids to AssetOb objects
		private var _assetObs:Array;

		// the last selected asset.  For efficient handling of the DwgOptions.exclusive flag
		private var _lastSelectedIds:Array=null;

		// flag to check if unselect asset is allowed.
		private var _unselectEnabled:Boolean=true;

		//hold the JSON map for assets
		private var _assetMap:AssetMap;
		
		private var _roomAssetsNumber:Number = 0;
		public function setRoomAssets(value:Number):void{
			_roomAssetsNumber = value;
		}
		
		public function get assetMap():AssetMap {
			return _assetMap;
		}
		
		/**
		 * Setter for the AFM assets.
		 *
		 * @param	value	The new assets to be used for drawing the Layer
		 */
		public function setAssets(value:Array):void {
			_assetObs=[];

			_assetMap=new AssetMap(value);

			invalidateDisplayList();
		}

		/**
		 * Retrieve an asset data with the specifying ids.
		 *
		 * @param	ids 	primary key ids.
		 *
		 * @return	An asset data in JSON
		 */
		public function getAsset(ids:Array):Object {
			if (_assetMap == null)
				return null;

			return _assetMap.getAsset(ids);
		}

		/**
		 * Clears the asset layer, erasing all drawn assets's fill and boundary.
		 */
		public function clearAssets():void {
			for (var i:int=0; i < this.numChildren; i++) {
				var aob:AssetObject=this.getChildAt(i) as AssetObject;
				aob.clear();
			}
		}
		
		public function removeAssets():void{
			_assetObs=[];
			
			_assetMap=new AssetMap([]);
			
			_roomAssetsNumber = 0;
			
			invalidateDisplayList();
		}

		/**
		 * Resets the asset layer, setting all drawn assets to their original settings
		 */
		public function resetToInitial():void {
			for (var i:int=0; i < this.numChildren; i++) {
				var aob:AssetObject=this.getChildAt(i) as AssetObject;
				aob.resetToInitial();
			}
		}

		/**
		 * Highlights the provided assets by filling its area with the provided color.
		 *
		 * @param	highlights			an array indicating the assets to be drawn and highlight
		 * @param	af					fill format for the assets
		 * @param	unhiliteAllOthers	clear the fill color for all other assets.
		 * @param   fillPropsPerKey     fill properties map used to overwrite af properties if necessary.
		 */
		public function drawAssets(highlightIds:ArrayCollection, af:FillFormat=null, unhiliteAllOthers:Boolean=false, fillPropsPerKey:Object=null, resetFills:Boolean=false, recMap:Object=null, highlightFormat:HighlightFormat=null, isClear:Boolean=true,  keepPreviousFormat:Boolean=false):void {
			var defaultFillColor:uint=this.parentApplication.controlConfig.highlightConfig.getFillColor(FillTypes.UNASSIGNED);

			if (af == null) {
				af=this.parentApplication.controlConfig.highlightConfig.getFillFormat(FillTypes.UNASSIGNED);
			}

			if (recMap == null) {
				recMap={};
			}

			var hiliteList:Object={};
			// loop throught the highlight list and highlight each asset.
			for each (var highlightId:Array in highlightIds) {
				// default asset format is 'unassigned'
				var curAf:FillFormat=this.parentApplication.controlConfig.highlightConfig.getFillFormat(FillTypes.UNASSIGNED);
				curAf.copy(af);
				if (fillPropsPerKey) {
					curAf.setFromFillProps(fillPropsPerKey[highlightId]);
				}
				setAssetColor(highlightId, curAf, resetFills, recMap[highlightId], highlightFormat, isClear);
				if (unhiliteAllOthers) {
					hiliteList[highlightId]=true;
				}
			}

			// set the highlight to unassigned for all other assets.
			if (unhiliteAllOthers && this._assetMap != null) {
				curAf=this.parentApplication.controlConfig.highlightConfig.getFillFormat(FillTypes.UNASSIGNED);
				curAf.thicknessMultiplier = af.thicknessMultiplier;
				for (var i:int=0; i < _assetMap.assets.length; i++) {
					var ids:Array=_assetMap.assets[i].ids;
					if (!hiliteList[ids]) {
						highlightFormat.setAssetType(null);
						setAssetColor(ids, curAf, resetFills, recMap[highlightId], highlightFormat, isClear, false, keepPreviousFormat);
					}
				}
			}
			
			
		}


		/**
		 * draw asset with the specifying per-drawing options (usually passed from view or javascript)
		 *
		 * @param dwgOpts 	per drawing options
		 * @param af		fill format
		 * @param dwgScale	drawing scale
		 *
		 */
		public function drawAssetsFromOpts(dwgOpts:DrawingConfig, af:FillFormat=null):void {
			this._unselectEnabled=false;

			if (af == null){
				af = createFillFormat(dwgOpts);
			}

			var simpleStyle:Boolean=(dwgOpts.mode != FillTypes.NONE && dwgOpts.mode != FillTypes.DEFAULT);
			var highlighIds:ArrayCollection=new ArrayCollection();
			for each (var item:Object in dwgOpts.recs) {
				var highlightId:Array=String(item.id).split(parentApplication.controlConfig.runtimeConfig.keyDelimiter) as Array;
				if (simpleStyle)
					highlighIds.addItem(highlightId);
				else {
					//no fill
					if (item.f == null)
						continue;

					var fillFormat:FillFormat=getFillFormat(highlightId, item.f, dwgOpts.mode);

					setAssetColor(highlightId, fillFormat, false);
				}
			}

			if (simpleStyle == true)
				drawAssets(highlighIds, af, false, null, false);


			//the highlight asset function set the highlight record to be 'selected', now unselect these highlighted assets.
			//this is to support multiple select clicking event. if user calls the highlightAssets, then trying to assign the highlighted asset, 
			//the click event will interpret it as "unassign" otherwise.
			var parentApp:abDrawing=(this.parentApplication as abDrawing);
			if(highlighIds.length > 0 && parentApp.currentState ==  ToolbarMode.MULTI_SELECT){
				for (var j:int=0; j < highlighIds.length; j++) {
					var asset:AssetObject=this.getAssetOb(highlighIds[j]);
					if (asset) {
						asset.properties.isSelected = false;
					}
				}
			}

			this._unselectEnabled=true;
		}
		
		public function createFillFormat(dwgOpts:DrawingConfig):FillFormat{
			var af:FillFormat=new FillFormat();
			if (dwgOpts.mode.length && dwgOpts.mode != FillTypes.DEFAULT) {
				af=this.parentApplication.controlConfig.highlightConfig.getFillFormat(dwgOpts.mode);
				// setting the id to the 'unselected' value will
				// flag the fill functionality to restore the previous
				// fill settings
				if (dwgOpts.mode == FillTypes.UNSELECTED) {
					if (!af)
						af=new FillFormat();
					af.id=FillTypes.UNSELECTED;
				}
			} else{
				af.fillColor=dwgOpts.fill.fillColor;
			}
			
			return af;
		}

		/**
		 * get fill format
		 *
		 * @param highlightId		used to retrieve the asset object and its fill properties
		 * @param fillFromDwgOpts	fill properties from per- drawing options
		 * @param mode				per drawing option's mode
		 *
		 * @return	fill format
		 *
		 */
		public function getFillFormat(highlightId:Array, fillFromDwgOpts:Object, mode:String):FillFormat {
			var af:FillFormat=this.parentApplication.controlConfig.highlightConfig.getFillFormat(FillTypes.UNASSIGNED);
			var aob:AssetObject=_assetObs[highlightId];
			if (aob && aob.fillFormat) {
				af.copy(aob.fillFormat);
				af.id="";
			}

			af.setFromDwgOptions(fillFromDwgOpts);

			if (mode == FillTypes.DEFAULT)
				af.id='default';

			return af;
		}


		/**
		 * unselect and select the asset.
		 *
		 * @param ids 		the ids for the asset to toggle
		 * @param mode		selection mode
		 * @param dwgScale	drawing scale
		 *
		 */
		public function toggleAssetSelection(ids:Array, mode:int, assignedObject:AssignedObject=null):void {
			var asset:AssetObject=getAssetOb(ids);

			if (asset != null) {
				var selection:Boolean=!asset.properties.isSelected;
				var color:String=null;
				var previousAssignedObject:AssignedObject=asset.getAssignedObject();
				//AssignUtilities.clear() calls _assignedAsset.reset(); ???
				if (assignedObject != null && assignedObject.field != '' && assignedObject.value != '') {
					if (previousAssignedObject != null && previousAssignedObject.field == assignedObject.field && previousAssignedObject.value == assignedObject.value) {
						//same as previous assignment -> unselect
						selection=false;
					} else if (assignedObject.field != null && assignedObject.field != '' && assignedObject.value != null && assignedObject.value != '') {
						//select
						color=assignedObject.getColor();
						selection=true;
					}
				}

				if (selection) {
					asset.setAssignedObject(assignedObject);
					asset.select(null, color);
					_lastSelectedIds=ids;
				} else {
					asset.setAssignedObject(null);
					asset.unselect();
				}

					// toggle the selection
					//	if (asset.properties.isSelected){
					//asset.unselect();
					//	}else{
					//XXX:??????
				/*
				if (parentApplication.getAssignMode() == AssignMode.NONE){
					unselectPrevious(ids);
				}
				*/

					//if(assignedObject){
					///	color = assignedObject.getColor();
					//	}
					//asset.select(null, color);
					//_lastSelectedIds=ids;
					//}
			}
		}

		/**
		 * make the asset of the specifying ids selectable or not.
		 *
		 * @param ids				the asset ids
		 * @param selectability		true or false
		 *
		 */
		public function makeAssetSelectable(ids:Array, selectability:Boolean):void {
			var asset:AssetObject=getAssetOb(ids);
			if (asset != null)
				asset.properties.isSelectable=selectability;
		}

		/**
		 *
		 * @param ids
		 * @param af
		 * @param resetFills
		 * @param dwgScale
		 * @param rec
		 *
		 */
		public function setAssetColor(ids:Array, af:FillFormat, resetFills:Boolean, rec:Object=null, highlightFormat:HighlightFormat=null, isClear:Boolean=true, doHilighting:Boolean=true, keepPreviousFormat:Boolean=false):void {
			var aobGraphic:AssetObject = createAssetObject(ids, af, resetFills, rec,highlightFormat);
			if(aobGraphic != null ){
				if (af.disabled) {
						aobGraphic.clear();
						return;
				}
				if(keepPreviousFormat && aobGraphic.fillFormat){
					af.copy(aobGraphic.fillFormat);
				}
				if(highlightFormat!=null && highlightFormat.getAssetType()!=null && aobGraphic.getAssetType() != highlightFormat.getAssetType()){
					aobGraphic.setAssetType(highlightFormat.getAssetType());
				}
				
				aobGraphic.setColor(af, resetFills, highlightFormat, isClear, doHilighting);
			}
		}
		
		public function createAssetObject(ids:Array, af:FillFormat, resetFills:Boolean, rec:Object=null,highlightFormat:HighlightFormat=null):AssetObject{
			var aobGraphic:AssetObject= null;
			if (_assetMap == null) {
				return aobGraphic;
			}
			
			// If in exclusive mode, unselect the previously selected asset
			if (af.id == FillTypes.SELECTED) {
				//unselectPrevious(ids);
				_lastSelectedIds=ids;
			}
		
		
			ids=CommonUtilities.getValidJsonIds(ids, 3);
			
			if (ids == null || ids.length == 0) {
				return aobGraphic;
			}
			
			var aob:Object=_assetMap.getAsset(ids);
			aobGraphic = _assetObs[ids];
			
			if (aob != null) {
				if (aobGraphic == null) {
					aobGraphic=new AssetObject(aob);
					addChild(aobGraphic);
					aobGraphic.visible=true;
					_assetObs[aobGraphic.properties.jsonData.ids]=aobGraphic;
				} else {
					aobGraphic.properties=new AssetProperties(aob);
				}
			}
			
			if (aobGraphic != null && rec != null) {
				aobGraphic.properties.rec=rec;
			}
		
			return aobGraphic;
		}

		/**
		 *
		 * Highlight one room.
		 */
		public function highlightRoom(roomIDs:Array, highlightFormat:String):void {
			var af:FillFormat=this.parentApplication.controlConfig.highlightConfig.getFillFormat(FillTypes.SELECTED);
			this.setAssetColor(roomIDs, af, false, null, new HighlightFormat(highlightFormat));
		}

		/**
		 *
		 * @param ids
		 * @return
		 *
		 */
		public function getAssetOb(ids:Array):AssetObject {
			return (_assetObs != null) ? _assetObs[ids] : null;
		}

		/**
		 * Returns an array of selected asset ids.  Each item in the array is an array itself
		 */
		public function getSelectedAssetIds():Array {
			var allIds:Array=[];
			if (_assetMap == null || _assetMap.assets == null)
				return allIds;

			for (var i:int=0; i < _assetMap.assets.length; i++) {
				var ids:Array=_assetMap.assets[i].ids;
				var aob:AssetObject=_assetObs[ids];
				if (aob != null && aob.properties != null && aob.properties.isSelected)
					allIds[allIds.length]=ids;
			}

			return allIds;
		}

		public function calculateThicknessMultiplier(af:FillFormat):FillFormat {
			if (this._roomAssetsNumber  != 0){
				
				if (this._roomAssetsNumber <= 50)
					af.thicknessMultiplier=5.0;
				else if (this._roomAssetsNumber <= 100)
					af.thicknessMultiplier=4.0;
				else if (this._roomAssetsNumber <= 250)
					af.thicknessMultiplier=3.6;
				else if (this._roomAssetsNumber <= 500)
					af.thicknessMultiplier=3.2;
				else if (this._roomAssetsNumber < 1000)
					af.thicknessMultiplier=2.3;
				else if (this._roomAssetsNumber < 2000)
					af.thicknessMultiplier=1.5;
				else if (this._roomAssetsNumber < 4000)
					af.thicknessMultiplier=1.0;
				else
					af.thicknessMultiplier=0.5
			}
			return af;
		}

		override protected function commitProperties():void {
			super.commitProperties();
			// when properties are comitted, redraw all assets, so they are scaled correctly
			for each (var aob:AssetObject in this._assetObs) {
				aob.redraw();
			}
		}

		/**
		 * unselect the last selected asset other than the asset of specifying ids.
		 *
		 * @param ids		the specifying ids to get the current asset
		 * @param dwgScale	drawing scale
		 *
		 */
		public function unselectPrevious(ids:Array):void {
			if (!this._unselectEnabled) {
				return;
			}

			if (_lastSelectedIds && ids ){//_lastSelectedIds != ids) {
				var isEqual:Boolean = true;
				for(var i:int=0; i<_lastSelectedIds.length; i++){
					if(_lastSelectedIds[i] != ids[i]){
						isEqual = false; break;
					}
				}
				if(!isEqual){
					var asset:AssetObject=_assetObs[_lastSelectedIds];
					if (asset) {
						asset.unselect();
					}
				}
			}
		}
	}

}

