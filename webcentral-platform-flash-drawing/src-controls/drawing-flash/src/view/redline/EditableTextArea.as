package src.view.redline
{
	import flash.events.MouseEvent;

	import mx.controls.TextArea;

	import src.model.redline.DrawMode;
	import src.model.util.CommonUtilities;
	import src.view.drawing.ArchibusDrawing;

	public class EditableTextArea extends TextArea
	{
		public function EditableTextArea(id:String, fontSize:Number, drawColor:uint, width:Number, height:Number, x:Number, y:Number )
		{
			super();

			this.id=id;
			this.setStyle("fontSize", fontSize);
			this.setStyle("color", drawColor);
			this.setStyle("fontWeight", "bold");
			this.width=width;
			this.height=height;
			this.x=x;
			this.y=y;

			this.addEventListener(MouseEvent.CLICK, onMouseClick);
			this.addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			this.addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
		}

		internal function onMouseOver(event:MouseEvent):void {
			var ad:ArchibusDrawing=CommonUtilities.getDwgFromEvent(event);
			if(ad){
				ad.stage.focus=this;
			}
		}

		internal function onMouseOut(event:MouseEvent):void {
			var ad:ArchibusDrawing=CommonUtilities.getDwgFromEvent(event);
			if(ad)
				ad.parentApplication.navCanvas.drawTextButton.source = ad.parentApplication.navCanvas._drawTextDefaultImage;

		}

		internal function onMouseClick(event:MouseEvent):void {
			var ad:ArchibusDrawing=CommonUtilities.getDwgFromEvent(event);
			if(ad){
				ad.drawingMode = DrawMode.drawModeText;
				ad.stage.focus=this;
				ad.parentApplication.navCanvas.drawTextButton.source = ad.parentApplication.navCanvas._drawTextSelectedImage;
			}
		}
	}
}

