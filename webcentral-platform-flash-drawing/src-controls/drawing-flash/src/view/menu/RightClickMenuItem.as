// ActionScript file
package src.view.menu {

	/**
	 * Defines a right click context menue item.
	 */
	public class RightClickMenuItem {
		public var title:String="";

		public var handlerName:String="";

		/**
		 * Constructor.
		 *
		 * @param contents item's content.
		 */
		public function RightClickMenuItem(contents:Object):void {
			if (contents == null || contents.toString().length < 1) {
				return;
			}

			// The contents array consists of the 2 following parts:
			//	title:	(string)	Title to display in the Right Click menu
			//	handler:	(string)	Javascript callback name
			if (contents != null) {
				this.title=contents.title;
				this.handlerName=contents.handler;
			}
		}

	}
}
