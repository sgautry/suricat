package src.view.menu {
	import flash.display.DisplayObject;
	import flash.events.*;
	import flash.external.ExternalInterface;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;

	import mx.core.Application;
	import mx.utils.ObjectUtil;

	import src.model.util.*;

	/**
	 * Defines right click context menu.
	 */
	public class RightClickMenu {

		private static var currentContextmenuItems:Array=[];
		/**
		 * a reference to the archibus drawing.
		 */
		private var _appRef:abDrawing=null;

		/**
		 * an array of menu items.
		 */
		private var _items:Array=[];

		/**
		 * Constructor.
		 * @param application the application reference to set.
		 */
		public function RightClickMenu(application:abDrawing):void {
			this._appRef=application;
		}

		//The content menu item is displayed only if the user right-clicked on a room
		public function addContextMenuAction(title:String, actionID:String):void {
			this._appRef.contextMenu.addEventListener(ContextMenuEvent.MENU_SELECT, addArguments(contextMenuTriggered, [title, actionID]));
		}


		public function contextMenuTriggered(event:ContextMenuEvent, title:String, actionID:String):void {
			var ids:Array=this._appRef.lastMouseEventIds;
			if (ids != null && ids.length == 3) {
				var contextMenuItem:ContextMenuItem=new ContextMenuItem(title, true, true, true);
				contextMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, addArguments(onContextMenuAction, [actionID]));
				currentContextmenuItems.push(contextMenuItem);
				this._appRef.contextMenu.customItems.unshift(contextMenuItem);

			}
		}

		private function addArguments(method:Function, methodArguments:Array):Function {
			return function(event:Event):void {
				method.apply(null, [event].concat(methodArguments));
			}
		}

		private function onContextMenuAction(event:ContextMenuEvent, actionID:String):void {
			var targetCItem:ContextMenuItem=event.currentTarget as ContextMenuItem;

			var ids:Array=this._appRef.lastMouseEventIds;
			var vals:Array=[];
			if (ids != null) {
				var val:Object={name: "rm.bl_id", value: ids[0]};
				vals.push(val);
				val={name: "rm.fl_id", value: ids[1]};
				vals.push(val);
				val={name: "rm.rm_id", value: ids[2]};
				vals.push(val);
			}

			//call javascript function and return primary key value as array in format of: [{value:value1}, {value:value2}, ...] 
			ExternalInterface.call(CommonUtilities.getFullFuncCall("onContextMenuAction"), actionID, vals);

			//XXX: remove added contextMenuAction???
			var cm:ContextMenu=new ContextMenu();
			cm.hideBuiltInItems();
			this._appRef.contextMenu=cm;


		}


		/**
		 * adds the right click menu items.
		 *
		 * @param items the items to add.
		 */
		public function initialize(items:Array):void {
			this._items=new Array(items.length);

			//KB# 3032571.  Move new right-mouse-click menu options to the top of the list.
			for (var i:int=items.length - 1; i >= 0; i--) {
				var rightClickMenuItem:RightClickMenuItem=new RightClickMenuItem(items[i]);
				this._items[i]=rightClickMenuItem;

				// Now add the new right click menu item to the right click menu
				var contextMenuItem:ContextMenuItem=new ContextMenuItem(rightClickMenuItem.title, (i == 0), true, true);
				this._appRef.contextMenu.customItems.unshift(contextMenuItem);
				contextMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, onContextMenu);
			}
		}

		/**
		 *  call the javascript event when the item is clicked
		 *  return the primary keys of the item if available.
		 */

		private function onContextMenu(event:ContextMenuEvent):void {

			var contextMenuItem:ContextMenuItem=event.target as ContextMenuItem;

			if (contextMenuItem != null && this._items != null) {
				// Identify the javascript function associated with the specified caption
				var funcName:String="";
				for (var i:int=0; i < this._items.length; i++) {
					var rightClickMenuItem:RightClickMenuItem=this._items[i];

					if (rightClickMenuItem.title == contextMenuItem.caption) {
						funcName=rightClickMenuItem.handlerName;
						break;
					}
				}

				if (funcName != "") {
					//retrieve the last mouse event (tootip. mouse over etc) ids.
					var ids:Array=this._appRef.lastMouseEventIds;
					var vals:Array=[];
					if (ids != null) {
						//loop through the ids then push into vals to return to cleint
						for (i=0; i < ids.length; i++) {
							var val:Object={};
							val.value=ids[i];
							vals.push(val);
						}
					}

					//call javascript function and return primary key value as array in format of: [{value:value1}, {value:value2}, ...] 
					ExternalInterface.call(funcName, vals);
				}
			}
		}
	}

}
