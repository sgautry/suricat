package src.model.util {
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;

	import mx.utils.ObjectUtil;

	import src.view.asset.AssetObject;
	import src.view.drawing.ArchibusDrawing;
	import src.view.text.AbTextField;
	import src.view.text.LabelObject;

	/**
	 * Common utilities
	 */
	public class CommonUtilities {

		public static const ZOOMFACTOR_MAX:Number=10;
		public static const ZOOMFACTOR_MIN:Number=1;

		/**
		 * Constructor
		 */
		public function CommonUtilities() {
			super();
		}


		/**
		 * Gets the full function name for the callback function.
		 *
		 * @param name the function name.
		 * @return the full function name with flex app prefix.
		 */
		public static function getFullFuncCall(name:String):String {
			return JsFunctionConstant.JS_FUNC_NAME_FLEXAPP + "." + name;
		}

		/**
		 * Gets the valid ids up to the max number.
		 *
		 * @param ids the original array of ids
		 * @param maxIds the maximum number to retrieve the ids from
		 * @return
		 */
		public static function getValidJsonIds(ids:Array, maxIds:int):Array {
			if (ids.length < maxIds)
				return ids;

			for (var i:int=0; i < maxIds; i++) {
				if (ids[i] == null)
					ids[i]=new String();
			}

			return ids;
		}

		/**
		 * Gets the asset or label field ids from the mouse event object.
		 *
		 * @param event an Mouse event object.
		 *
		 * @return the ids
		 */
		public static function getIdsFromEvent(event:MouseEvent):Array {
			var ob:Object=ObjectUtil.getClassInfo(event.target);
			var ids:Array=null;
			if (ob.name == "src.view.asset::AssetObject") {
				var assetOb:AssetObject=event.target as AssetObject;
				ids=assetOb.properties.jsonData.ids;
			} else if (ob.name == "src.view.text::AbTextField") {
				var ob2:AbTextField=event.target as AbTextField;
				var lo:LabelObject=ob2.parent as LabelObject;

				if (lo != null)
					ids=lo.ids;
			}

			return ids;
		}

		/**
		 * Retrieve the ArchibusDrawing object from the mouse event.
		 *
		 * @param event the mouse event.
		 * @return an ArchibusDrawing object.
		 */
		public static function getDwgFromEvent(event:MouseEvent):ArchibusDrawing {
			var dob:DisplayObject=event.target as DisplayObject;
			while (dob != null) {
				var ad:ArchibusDrawing=dob as ArchibusDrawing;
				if (ad != null) {
					return ad;
				}
				dob=dob.parent;
			}
			return null;
		}

		/**
		 * Gets the color for the highlight field's specifying value.
		 *
		 * @param highlightField the highlight field, such as rm.rm_cat
		 * @param val the highlight value, such as 'CONFERENCE'
		 * @param ruleValue the rule value, such as 'OFF-C'
		 * @return the color value.
		 */
		public static function getColorFromValue(highlightField:String, val:String, ruleValue:String):String {
			//??? YQ - Note this value is the field value, not the color value, but when calling from the javascript, this value is actually passed as color value
			// maybe this is why the color for the asset can not be found then the auto colors are used.
			return ExternalInterface.call(getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_GETCOLORFROMVALUE), highlightField, val, false, ruleValue);
		}

		/**
		 * Convert a color value to a Hex value
		 *
		 * @param cIn the color value.
		 *
		 * @return the color's hex value.
		 */
		public static function toHex(cIn:String):String {
			if (cIn == null)
				return '';

			var c2:String=parseInt(cIn, 10).toString(16);
			if (c2.length < 6) {
				var i:int=c2.length;
				c2=("000000" + c2).substring(i);
			}

			var c3:String=new String();
			c3=c2.charAt(4) + c2.charAt(5) + c2.charAt(2) + c2.charAt(3) + c2.charAt(0) + c2.charAt(1);

			return '0x' + c3;
		}


		/**
		 * Checks if the property exists in the config object and is not null.
		 *
		 * @param ob the config object
		 * @param varName the property name.
		 *
		 * @return true if found and not null, false otherwise.
		 */
		public static function isValid(ob:Object, varName:String):Boolean {
			return ob != null && ob.hasOwnProperty(varName) && ob[varName] != null && ob[varName] != undefined;
		}
	}
}
