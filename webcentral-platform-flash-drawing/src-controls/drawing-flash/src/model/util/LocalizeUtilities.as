package src.model.util {
	import flash.external.ExternalInterface;

	/**
	 * Defines the localization strings retrieved from JavaScript.
	 */
	public class LocalizeUtilities {
		public static var wordError:String="Error";
		public static var wordNoDoc:String="No document specified to load";
		public static var wordUnableToLoad:String="Unable to load";

		// Proeprty: a map for all localized strings
		private var _map:Object;

		/**
		 * Constructor.
		 */
		public function LocalizeUtilities() {
			_map=ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_LOCALIZE));

			// Localize some of the possible prompts
			wordError=setLocalizeWord('PROMPT_ERROR', wordError);
			wordNoDoc=setLocalizeWord('PROMPT_NODOC', wordNoDoc);
			wordUnableToLoad=setLocalizeWord('PROMPT_UNABLETOLOAD', wordUnableToLoad);
		}


		/**
		 * Sets the localized value for the specifying string.
		 *
		 * @param sId input string
		 * @param defVal default value if the string not found in localized map.
		 *
		 * @return the lcoalized value.
		 */
		public function setLocalizeWord(sId:String, defVal:String):String {
			if (CommonUtilities.isValid(_map, sId))
				return _map[sId];
			else
				return defVal;
		}


		public function get map():Object {
			return _map;
		}


	}
}
