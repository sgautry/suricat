package src.model.util {

	/**
	 * Defines commonly accessed JavaScript function namess.
	 */
	public class JsFunctionConstant {
		public static const JS_FUNC_NAME_LOCALIZE:String="localizeFlash";

		public static const JS_FUNC_NAME_ONCLICK:String="onClickHandler";

		public static const JS_FUNC_NAME_ONMULTIPLESELECT:String="onMultipleSelectHandler";

		public static const JS_FUNC_NAME_ONRESERASSETS:String="onResetAssetsHandler";

		public static const JS_FUNC_NAME_SETROOMS:String="setRooms";

		public static const JS_FUNC_NAME_APPLOADED:String="flashAppLoaded";

		public static const JS_FUNC_NAME_DOCLOADED:String="flashDocLoaded";

		public static const JS_FUNC_NAME_LOADFAILED:String="flashDocLoadFailed";

		public static const JS_FUNC_NAME_ASSETSLOADED:String="flashAssetsLoaded";

		public static const JS_FUNC_NAME_SETLITEDISPLAY:String="setLiteDisplay";

		public static const JS_FUNC_NAME_GETHIGHLIGHTSETTINGS:String="getHighlightSettings";

		public static const JS_FUNC_NAME_SAVEREDMARKS:String="saveRedmarks";

		public static const JS_FUNC_NAME_GETTOOLBARSETTINGS:String="getToolbarSettings";

		public static const JS_FUNC_NAME_ONHIGHLGHTSCHANGED:String="onHighlightsChanged";

		public static const JS_FUNC_NAME_COLUMNSGROUPEDBY:String="getColumnsGroupedBy";

		public static const JS_FUNC_NAME_GETCONFIG:String='getDwgConfig';

		public static const JS_FUNC_NAME_GETDELIM:String='getDelim';

		public static const JS_FUNC_NAME_GETDRAGGABLEASSETS:String='getDraggableAssets';

		public static const JS_FUNC_NAME_GETDISPLAYDIAGONALSELECTIONPATTERN:String='getDiagonalSelectionPattern';

		public static const JS_FUNC_NAME_GETNOFILLFIELD:String='getNofillField';

		public static const JS_FUNC_NAME_GETNOFILLVALS:String='getNofillValues';

		public static const JS_FUNC_NAME_GETOPTIONALPKFIELDS:String='getOptionalPkFields';

		// The name of the glocal javascript object which contains the interface for this flex application
		public static const JS_FUNC_NAME_FLEXAPP:String="jsAccessFromFlex";

		public static const JS_FUNC_NAME_FILEEXISTS:String="fileExists";

		public static const JS_FUNC_NAME_GETCOLORFROMPATTERN:String="getColorFromPattern";

		public static const JS_FUNC_NAME_GETCOLORFROMVALUE:String="getColorFromValue";

		public static const JS_FUNC_NAME_GETHIGHLIGHTRULEVALUEFIELD:String="getHighlightRuleValueField";

		public static const JS_FUNC_NAME_ONASSETPANELTITLEBARACTION:String="onAssetPaneltitleBarAction";

		public static const JS_FUNC_NAME_ONASSETLOCATIONCHANGEHANLDER:String="onAssetLocationChangeHanlder";

		public static const JS_FUNC_NAME_ONASSETPANELROWACTION:String="onAssetPanelRowAction";

		public static const JS_FUNC_NAME_GETHATCHPATTERNS:String="getHatchPatterns";

		public static const JS_FUNC_NAME_MULTISELECTHANDLER_DEFINED:String="isMultipleSelectHandlerDefined";

	}
}


