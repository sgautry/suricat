package src.model.asset {

	/**
	 * Defines the asset map contained by the asset layer and its related public functions.
	 * Each item records an asset ids value (key) to the asset json format object (value).
	 *
	 * @author Qin
	 */
	public class AssetMap {
		/**
		 * a collection of assets contained by aset layer each asset is a json format object passed from zlib file.
		 */
		private var _assets:Array=new Array();

		/**
		 *  A map of ids to assets for quicker lookup.
		 */
		private var _map:Object={};

		public function AssetMap(value:Array) {
			_assets=value;
			_map={};

			for each (var asset:Object in value) {
				_map[asset.ids]=asset;
			}
		}

		/**
		 * Retrieve an asset data with the specifying ids.
		 *
		 * @param	ids 	primary key ids.
		 *
		 * @return	An asset data in JSON
		 */
		public function getAsset(ids:Array):Object {
			return _map[ids];
		}

		/**
		 * Getter for the AFM assets.
		 *
		 * @return	The current AFM assets being used for displaying the Layer
		 */
		public function get assets():Array {
			return _assets;
		}

	}
}
