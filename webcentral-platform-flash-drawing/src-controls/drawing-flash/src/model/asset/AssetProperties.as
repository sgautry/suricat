package src.model.asset {

	/**
	 * Define an asset's properties used in asset selection.
	 */
	public class AssetProperties {
		private var _jsonData:Object;

		private var _rec:Object=null;

		private var _isSelectable:Boolean=true;

		private var _isSelected:Boolean=false;


		public function AssetProperties(jsonData:Object) {
			super();
			this._jsonData=jsonData;
		}

		public function canToggle(mode:int):Boolean {
			return (mode == 2 || (mode == 1 && this.isSelectable))
		}


		public function set isSelectable(value:Boolean):void {
			_isSelectable=value;
		}

		public function get rec():Object {
			return _rec;
		}

		public function set rec(value:Object):void {
			_rec=value;
		}

		public function get isSelectable():Boolean {
			return _isSelectable;
		}

		public function get isSelected():Boolean {
			return _isSelected;
		}

		public function set isSelected(selected:Boolean):void {
			this._isSelected=selected;
		}


		public function get jsonData():Object {
			return _jsonData;
		}

		public function set jsonData(value:Object):void {
			this._jsonData=value;
		}

	}
}
