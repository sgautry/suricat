package src.model.asset {
	import com.adobe.serialization.json.JSON;

	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;

	import src.model.config.DrawingConfig;
	import src.view.drawing.ArchibusDrawing;

	/**
	 * Loads the assets of an Archibus drawing.
	 */
	public class AssetLoader {

		private var _dwgRef:ArchibusDrawing;
		private var _loader:URLLoader;

		/**
		 * Constructor
		 *
		 * @param	drawing	The drawing for which the assets will get loaded.
		 */
		public function AssetLoader(drawing:ArchibusDrawing) {
			this._dwgRef=drawing;
			this._loader=new URLLoader();
			this._loader.dataFormat=URLLoaderDataFormat.BINARY;
			this._loader.addEventListener(Event.COMPLETE, assetsLoaded);
			this._loader.addEventListener(IOErrorEvent.IO_ERROR, loaderIOErrorHandler);
		}

		/**
		 * Loads the assets which are stored in the specified file.
		 * This file should contain a compressed JSON string.
		 *
		 * @param	assetFile	The file containing the assets data
		 */
		public function load(perDrawingConfig:DrawingConfig, assetType:String):void {
			var assetFile:String=perDrawingConfig.folder + "/" + this._dwgRef.location.getDrawingName();
			assetFile+="-" + assetType + perDrawingConfig.assetSuffix + ".json.zlib";
			this._loader.load(new URLRequest(assetFile));
		}


		private function loaderIOErrorHandler(e:Event):void {
			trace(e.target.toString());
		}

		/**
		 * Callback function responding to the load complete event.
		 */
		private function assetsLoaded(e:Event):void {
			var compressedData:ByteArray=this._loader.data as ByteArray;
			compressedData.uncompress();
			var rawData:String=compressedData.toString();

			//
			// Look for the header info, and handle accordingly if found
			// If not found, is the original non-header version of asset data
			//
			//	Asset document has the following structure
			//		{ "header" : { paramlist.... }, "assets" : [{ id.... }, { id... }]}
			//
			var dataHeader:String=rawData.substr(0, 20);
			var newAssets:Array = new Array();
			if (dataHeader.indexOf("header") > 0) {
				var dataOb:Object=JSON.decode(rawData) as Object;
				newAssets = dataOb.assets;
			} else {
				newAssets = JSON.decode(rawData) as Array;
			}
			this._dwgRef.assetLayer.setRoomAssets(newAssets.length);
			if(this._dwgRef.assets){
				this._dwgRef.assets=this._dwgRef.assets.concat(newAssets);
			} else {
				this._dwgRef.assets=newAssets;
			}
			this._loader.removeEventListener(Event.COMPLETE, assetsLoaded);
			this._loader.removeEventListener(IOErrorEvent.IO_ERROR, loaderIOErrorHandler);
		}

	}
}


