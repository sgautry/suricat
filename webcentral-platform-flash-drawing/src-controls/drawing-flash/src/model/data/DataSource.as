package src.model.data {
	import flash.external.ExternalInterface;
	
	import src.model.util.CommonUtilities;

	/**
	 * Represents an Archibus DataSource.
	 *
	 * The primary purpose of this class is to provide an interface between
	 * the javascript handlers which actually handle the datasoure.  This allows
	 * actionscript to get at the same datasource.
	 */
	public class DataSource extends Object {

		/**
		 * Property: Data Source Type, can be NONE, LABEL, HIGHLIGHT
		 */
		private var _sourceType:int=SourceType.NONE;

		private var _datasourceName:String=null;

		private var _fieldDefs:Object=null;

		/**
		 *	the record values retrived from the database
		 *  kb3019893: Each item in this array is now just an array of values.
		 */
		private var _recs:Array=null;

		/**
		 * highlight pattern field name
		 */
		private var _hPatternFieldName:String="";

		/**
		 * an array of all fields which corresponding to a highlight value and legend key
		 */
		private var _highlightLegendKeyFields:Array=[];

		private var _pks:Array=[];

		private static const FIELD_HPATTERN_NAME:String="hpattern_acad";

		public function DataSource(sourceType:int, datasourceName:String=null) {
			super();
			_sourceType=sourceType;
			_datasourceName=datasourceName;
		}

	
		public function getFieldDefs():Object {
			if (_fieldDefs == null) {
				var ds:Object=ExternalInterface.call(CommonUtilities.getFullFuncCall(DataSourceKey.JS_FUNC_NAME_GETDATASOURCE), _sourceType, _datasourceName);
				if (ds != null && ds["id"] != "None") {
					this._datasourceName=ds["id"];
					this._pks = ds["primaryKeyFields"];
				}
				if (CommonUtilities.isValid(ds, DataSourceKey.PARAM_FIELDDEFS)) {
					_fieldDefs=ds[DataSourceKey.PARAM_FIELDDEFS];
				}
			}

			return this._fieldDefs;
		}


		private function setDatasourceName():void {
			if (this._datasourceName == null) {
				var ds:Object=ExternalInterface.call(CommonUtilities.getFullFuncCall(DataSourceKey.JS_FUNC_NAME_GETDATASOURCE), _sourceType, this._datasourceName);
				if (ds != null && ds["id"] != "None") {
					this._datasourceName=ds["id"];
					this._pks = ds["primaryKeyFields"];
				}

			}
		}

		public function getDatasourceName():String {
			this.setDatasourceName();
			return this._datasourceName;
		}


		/**
		 * Retrieves the field defs from database.
		 *
		 * @return true if successful, false otherwise
		 */
		public function setFieldDefs():Boolean {
			if (_fieldDefs == null) {
				var ds:Object=ExternalInterface.call(CommonUtilities.getFullFuncCall(DataSourceKey.JS_FUNC_NAME_GETDATASOURCE), _sourceType, _datasourceName);
				if (ds != null && ds["id"] != "None") {
					this._datasourceName=ds["id"];
					this._pks = ds["primaryKeyFields"];
				}
					
				if (CommonUtilities.isValid(ds, DataSourceKey.PARAM_FIELDDEFS))
					_fieldDefs=ds[DataSourceKey.PARAM_FIELDDEFS];
			}

			return (_fieldDefs != null) ? true : false;
		}

		/**
		 * Gets the all or visible field names from field definition.
		 *
		 * @param all true if all fields will be retrieved, false if only visible fields will be retrieved.
		 * @return an array of all or visible field names.
		 */
		public function getFieldNames(all:Boolean=true):Array {
			var keys:Array=[];
			if (all) {
				if (setFieldDefs() && CommonUtilities.isValid(_fieldDefs, DataSourceKey.PARAM_KEYS))
					keys=_fieldDefs[DataSourceKey.PARAM_KEYS];
			} else
				keys=getFilteredFieldNames(DataSourceKey.PARAM_HIDDEN, false, "false");

			return keys;
		}

		/**
		 * Retrieves the primary key names from the data source. If there are more than 3 primary keys, only the first 3 will be returned.
		 *
		 * @return primary key names array
		 * 
		 * TODO: this is not reliable implementation at all!!!
		 */
		public function getPrimaryKeyNames():Array {
			
			if (!this._pks || this._pks.length < 1) {
				this._pks=[];
				
				if (!setFieldDefs() || !CommonUtilities.isValid(_fieldDefs, DataSourceKey.PARAM_KEYS) || !CommonUtilities.isValid(_fieldDefs, DataSourceKey.PARAM_MAP))
					return this._pks;
				
				var primaryKeys:Array=getFilteredFieldNames(DataSourceKey.PARAM_PRIMARYKEY, true);
				
				// only supporting a maximum primary key of 3 elements
				//???? YQ - Why? needs to be fixed
				if (primaryKeys != null && primaryKeys.length > 0) {
					for (var i:int=0; i < primaryKeys.length && i < 3; i++) {
						this._pks[i]=primaryKeys[i];
					}
				}
			}
			
			return this._pks;
		}


		/**
		 * Loads the records from the data source with the specifying primary keys (can be null). If the specifying primary keys is null, all the records will be set.
		 *
		 * @param pks primary keys used to restrict the records
		 *
		 * @return true if more than one record is returned, false otherwise.
		 */
		public function loadRecords(pks:Array=null, restriction:Object=null):Boolean {
			getRecords(pks, restriction);
			return (_recs != null) ? true : false;
		}


		/**
		 * Gets the record of the specifying index.
		 *
		 * @param index the index number of the record to get.
		 *
		 * @return the record value object.
		 */
		public function getRecord(index:int):Object {
			return _recs[index];
		}


		/**
		 * Gets the primary keys' values with the specifying record's index.
		 *
		 * @param index the record's index.
		 *
		 * @return an array containing the primary keys' values.
		 */
		public function getPrimaryKeysValues(index:int):Array {
			var rec:Object=this.getRecord(index);
			if (rec == null)
				return [];

			var pkValues:Array=[];
			var pkNames:Array=this.getPrimaryKeyNames();

			for (var i:int=0; i < pkNames.length; i++)
				pkValues[i]=rec[pkNames[i]];

			return pkValues;
		}

		/**
		 * Gets the number of the records retrieved.
		 *
		 * @return the number of records.
		 */
		public function numberOfRecords():int {
			return (_recs != null) ? _recs.length : 0;
		}

		/**
		 * Loop through the field names, try to find the hpattern_acad* field.
		 *
		 * @return the field name with prefix 'hpattern_acad'.
		 */
		public function getHPatternFieldName():String {
			if (_hPatternFieldName.length || !setFieldDefs())
				return _hPatternFieldName;

			var fieldName:String=null;
			var found:Boolean=false;

			var fieldNames:Array=getFieldNames();
			for (var i:int=0; i < fieldNames.length && !found; i++) {
				fieldName=fieldNames[i];
				if (fieldName.search(FIELD_HPATTERN_NAME) >= 0) {
					_hPatternFieldName=fieldName;
					found=true;
				}
			}

			return _hPatternFieldName;
		}


		/**
		 * Gets an array of all the highlight legend keys' field names
		 *
		 * @return array
		 */
		public function getHighlightLegendKeyFields():Array {
			if (_highlightLegendKeyFields.length)
				return _highlightLegendKeyFields;

			var names:Array=getFilteredFieldNames(DataSourceKey.PARAM_LEGENDKEY, true, "true");
			if (names != null && names.length > 0) {
				for (var i:int=0; i < names.length; i++)
					_highlightLegendKeyFields[i]=names[i];
			}

			return _highlightLegendKeyFields;
		}

		/**
		 * get the records values by calling javascript data source functions.
		 *
		 * @param pks primary keys arrray
		 */
		private function getRecords(pks:Array=null, restriction:Object=null):void {
			_recs=null;
			var cnt:int=ExternalInterface.call(CommonUtilities.getFullFuncCall(DataSourceKey.JS_FUNC_NAME_QUERY), _sourceType, pks, _datasourceName, restriction);

			if (cnt > 0) {
				_recs=new Array(cnt);
				for (var i:int=0; i < cnt; i++)
					_recs[i]=ExternalInterface.call(CommonUtilities.getFullFuncCall(DataSourceKey.JS_FUNC_NAME_GETRECORDVALUES), i);
			}
		}

		/**
		 * Retrieves the list of filtered fields that satisfies the filter requirements. For example,
		 * specified filter set to the test value (true or false)
		 * e.g.
		 * 	'primaryKey' value set to true or 'true'
		 * 		or
		 *  'hidden' value set to false or 'false'
		 *
		 * @param fieldParam filter key defined in DataSourceKeys.as file, i.e. primaryKey, hidden etc.
		 * @param booleanTester the boolean requirement to satisfy, true or false. only used when strTest value is empty.
		 * @param stringTester the string requirement to satisfy. If the value is empty, the 'test' value will be used.
		 * @return an array of filtered fields
		 */
		private function getFilteredFieldNames(fieldParam:String, booleanTester:Boolean, stringTester:String=""):Array {



			// get the list of fields contains all fields specified in the data source
			if (setFieldDefs() && CommonUtilities.isValid(_fieldDefs, DataSourceKey.PARAM_KEYS) && CommonUtilities.isValid(_fieldDefs, DataSourceKey.PARAM_MAP)) {
				var fieldNames:Array=_fieldDefs[DataSourceKey.PARAM_KEYS];
				var fieldMap:Object=_fieldDefs[DataSourceKey.PARAM_MAP];

				// Iterate through the list and pick out only those that have the
				var filteredFields:Array=[];
				var index:int=0;
				for (var i:int=0; i < _fieldDefs.length; i++) {
					var fieldName:String=fieldNames[i];
					var fieldParamValues:Object=fieldMap[fieldName];
					var isSatisfy:Boolean=false;

					if (stringTester.length == 0)
						isSatisfy=(fieldParamValues[fieldParam] == booleanTester); // Do a boolean test
					else
						isSatisfy=(fieldParamValues[fieldParam] == stringTester); // Do a string test

					if (isSatisfy)
						filteredFields[index++]=fieldName;
				}
			}

			return filteredFields;
		}

	}

}
