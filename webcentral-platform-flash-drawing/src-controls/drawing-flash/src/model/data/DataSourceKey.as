package src.model.data {

	/**
	 * 	Defines commonly used paramater names for accessing information from a datasource.
	 *  Defines available javascript methods for dataSource handling.
	 */
	public class DataSourceKey extends Object {
		public static const PARAM_FIELDDEFS:String="fieldDefs";

		public static const PARAM_KEYS:String="keys";

		public static const PARAM_MAP:String="map";

		public static const PARAM_PRIMARYKEY:String="primaryKey";

		public static const PARAM_HIDDEN:String="hidden";

		public static const PARAM_LEGENDKEY:String="legendKey";

		public static const JS_FUNC_NAME_GETDATASOURCE:String="getDataSource";

		public static const JS_FUNC_NAME_QUERY:String="query";

		public static const JS_FUNC_NAME_GETRECORDVALUES:String="getRecValues";
	}
}
