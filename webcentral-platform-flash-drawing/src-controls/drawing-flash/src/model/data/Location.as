
package src.model.data {


	/**
	 * Defines the cuurent drawing's location properties such as building id, floor id, room id and drawing name.
	 */
	public class Location {

		/**
		 * Constant: primary key array's index for building
		 */
		public static var buildingIndex:int=0;

		/**
		 * Constant: primary key array's index for floor
		 */
		public static var floorIndex:int=1;

		/**
		 * Constant: primary key array's index for room
		 * */
		public static var roomIndex:int=2;

		/**
		 * Property: drawing name
		 */
		private var dwgname:String='';

		/**
		 * Property: map to hold primary key name and value pair.
		 */
		private var map:Object={};

		/**
		 * Property: primary key array
		 */
		private var pks:Array=[];

		/**
		 * Constructor specifying the location properties such drawing name, primary key array and primary keys and values map.
		 *
		 * @param ob
		 */
		public function Location(ob:Object=null) {
			set(ob);
		}

		/**
		 * Retrieves the building code from the map object.
		 *
		 * @return the building code.
		 *
		 */
		public function getBuilding():String {
			return this.map[pks[buildingIndex]];
		}

		/**
		 * Gets the drawing name. If the dwgname is empty, use the building and floor value to compose the drawing name.
		 *
		 * @return the drawing name.
		 */
		public function getDrawingName():String {
			var drawingName:String="";
			if (this.dwgname != null && this.dwgname != '')
				drawingName=this.dwgname;

			if (drawingName == null || drawingName.length == 0)
				drawingName=getBuilding() + getFloor();

			return drawingName.toLowerCase();
		}

		/**
		 * retrieves the floor code from the map object.
		 *
		 * @return the floor code
		 *
		 */
		public function getFloor():String {
			return this.map[pks[floorIndex]];
		}

		/**
		 * Retrieves primary key array in format of [key1, value1, key2, value2, key3, value3..].
		 * excudeRoomKey - boolean. true if the array will only contain building and floor keys and values, false all key and values will
		 */
		public function getPrimarykeysArray(excudeRoomKey:Boolean=false):Array {
			var primaryKeysArray:Array=[];

			var elementCounter:int=0;

			var numOfKeys:int=this.pks.length;
			if (excudeRoomKey == true)
				numOfKeys=numOfKeys - 1;

			for (var keyCounter:int=0; keyCounter < numOfKeys; keyCounter++) {
				var field:String=pks[keyCounter];
				var val:String=map[pks[keyCounter]];
				if (val != null && val.length) {
					primaryKeysArray[elementCounter++]=field;
					primaryKeysArray[elementCounter++]=val;
				}
			}
			return primaryKeysArray;
		}

		/**
		 * Retrieves the room code from the map obejct
		 * @return the room code
		 *
		 */
		public function getRoom():String {
			return this.map[pks[roomIndex]];
		}

		/**
		 * Sets the location object specifying the existing map with primary keys, map and drawing name.
		 */
		public function set(ob:Object):void {
			if (ob == null)
				return;

			if (ob.pks != null)
				this.pks=ob.pks;

			if (ob.map != null)
				this.map=ob.map;

			if (ob.dwgname != null)
				this.dwgname=ob.dwgname;
		}

		/**
		 * Sets the drawing name
		 */
		public function setDrawingName(rawDwgName:String):void {
			this.dwgname=rawDwgName;
		}

		/**
		 * Sets the location object from the specifying array and drawing name.
		 * The array is in format of [key1, value1, key2, value 2, key3, value 3...]
		 */
		public function setFromArray(locationArray:Array, rawDwgName:String=''):void {
			if (locationArray == null || !locationArray.length)
				return;

			//loop through the array's element
			for (var i:int=0; i < locationArray.length; i++) {
				var key:String=locationArray[i++];
				var value:String=locationArray[i];
				this.pks[pks.length]=key;
				this.map[key]=value;
			}

			if (rawDwgName != null && rawDwgName != '')
				this.dwgname=rawDwgName;
		}
	}
}
