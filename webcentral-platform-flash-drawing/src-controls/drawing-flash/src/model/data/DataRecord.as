package src.model.data {
	import src.model.util.CommonUtilities;


	/**
	 * Represents an Archibus DataRecord. Provides helper methods for easier access to data.
	 */
	public class DataRecord extends Object {
		/**
		 * field names and values map.
		 */
		private var _keysValuesMap:Object=null;

		/**
		 * primary key names array
		 */
		private var _pkNames:Array=null;

		/**
		 * Constructor
		 */
		public function DataRecord() {
			super();
		}

		/**
		 * Retrieves the primary key's value as a String separated by the specifying delimiter.
		 *
		 * @param toLowerCase true if change the result to lower case, false if no change.
		 * @param delimiter - the specifying delimeter to separate the primary key values.
		 *
		 * @return primary key values string
		 */
		public function getPksValue(toLowerCase:Boolean=true, delimiter:String=""):String {
			var pksValue:String="";
			if (!_pkNames)
				return pksValue;

			for (var i:int=0; i < _pkNames.length; i++)
				pksValue=pksValue + delimiter + this._keysValuesMap[_pkNames[i]];

			if (pksValue.length && delimiter.length)
				pksValue=pksValue.substr(delimiter.length);

			if (toLowerCase)
				pksValue=pksValue.toLowerCase();

			return pksValue;
		}

		public function get keysValuesMap():Object {
			return this._keysValuesMap;
		}

		public function set keysValuesMap(valueOb:Object):void {
			this._keysValuesMap=valueOb;
		}

		public function get pkNames():Array {
			return this._pkNames;
		}


		public function set pkNames(pks:Array):void {
			this._pkNames=pks;
		}

		public function getValidJsonIds():Array {
			var ids:Array=[];
			for (var i:int=0; i < _pkNames.length; i++)
				ids[i]=_keysValuesMap[_pkNames[i]];

			return CommonUtilities.getValidJsonIds(ids, _pkNames.length);
		}
	}
}
