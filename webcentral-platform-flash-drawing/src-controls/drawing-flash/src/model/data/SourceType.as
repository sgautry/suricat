package src.model.data {

	/**
	 * Defines Data Source Types.
	 */
	public class SourceType extends Object {
		public static const NONE:int=0;

		public static const LABEL:int=1;

		public static const HILITE:int=2;

	}
}
