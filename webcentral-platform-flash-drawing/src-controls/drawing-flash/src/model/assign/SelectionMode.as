package src.model.assign {

	/**
	 * Defines selection mode enumeration values.
	 *
	 */
	public class SelectionMode {

		/**
		 * selection is disabled, don't toggle
		 */
		public static const NONE:int=0;

		/**
		 * selection enabled only for selectable assets
		 */
		public static const ASSIGNEDONLY:int=1;

		/**
		 * selection enabled for all assets
		 */
		public static const ALL:int=2;
	}
}
