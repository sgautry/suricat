package src.model.assign {
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	
	import src.controller.assign.AssignedObject;
	import src.controller.toolbar.ToolbarMode;
	import src.model.config.*;
	import src.model.data.Location;
	import src.view.asset.AssetObject;
	import src.view.drawing.ArchibusDrawing;
	import src.view.highlight.*;

	/**
	 * Handles the assign and unassign assets.
	 */
	public class AssignUtilities {
		/**
		 * references to drawing container application
		 */
		private var appRef:abDrawing=null;

		/**
		 * assigned object.
		 */
		private var _assignedAsset:AssignedObject=null;

		/**
		 * Map of assign values to a unique asset (a room)
		 */
		private var _assignedMapOneToOne:Object={};

		/**
		 * Map of already assigned assets (rooms) to a unique value (person, category, etc)
		 */
		private var assignedAssetsOneToOne:Object={};
		
		//TODO: refactor following veriables to make them be consistent regarding of type and properties!!!

		/**
		 * array to hold the assigned asset object's value, ids and color properties.
		 */
		private var _retainedAssignArrayOneToOne:ArrayCollection=new ArrayCollection();

		/**
		 *  Map of assigned values to a unique asset object (a room)
		 */
		private var _assignedMapManyToOne:Object={};

		/**
		 * Map of rooms to # of assigned values associated with those rooms
		 */
		private var _retainedAssignManyToOne:Object={};

		public var retainedAssginedManyToOne:Array=new Array();
	
		/**
		 *  Map of many asset objects (many rooms) to a unique assigned values
		 *  This variable records the assigned asset object for each of the assign action (single or multiple select).
		 */
		private var _assignedMapOneToMany:Array=new Array();

		/**
		 * Map of assigned values to many asset object (a set of rooms)
		 * This variable records the assigned asset for all the assign actions (single or multiple select).
		 */
		private var _retainedAssignOneToMany:Object={};

		/**
		 * Prompt string.
		 */
		private var wordSelectedRoom:String='Selected room';

		/**
		 * Prompt string.
		 */
		private var wordAlreadyAssignedTo:String='already assigned to';


		/**
		 * Constructor
		 */
		public function AssignUtilities(app:abDrawing) {
			super();
			this.appRef=app;

			// Localize some of the possible prompts
			wordSelectedRoom=this.appRef.localizedManager.setLocalizeWord('PROMPT_SELECTEDROOM', wordSelectedRoom);
			wordAlreadyAssignedTo=this.appRef.localizedManager.setLocalizeWord('PROMPT_ALREADYASSIGNEDTO', wordAlreadyAssignedTo);
		}



		/**
		 *  Map of many asset objects (many rooms) to a unique assigned values
		 */
		public function get assignedMapOneToMany():Array
		{
			return _assignedMapOneToMany;
		}

		/**
		 * @private
		 */
		public function set assignedMapOneToMany(value:Array):void
		{
			_assignedMapOneToMany = value;
		}

		public function get assignedMapManyToOne():Object {
			return _assignedMapManyToOne;
		}

		public function set assignedMapManyToOne(value:Object):void {
			_assignedMapManyToOne=value;
		}

		public function get assignedMapOneToOne():Object {
			return _assignedMapOneToOne;
		}

		public function set assignedMapOneToOne(value:Object):void {
			_assignedMapOneToOne=value;
		}

		public function set retainedAssignOneToMany(value:Object):void {
			_retainedAssignOneToMany=value;
		}

		public function get retainedAssignOneToMany():Object {
			return _retainedAssignOneToMany;
		}

		public function get assignedAsset():AssignedObject {
			return _assignedAsset;
		}

		public function set assignedAsset(value:AssignedObject):void {
			_assignedAsset=value;
		}

		public function get retainedAssignArrayOneToOne():ArrayCollection {
			return _retainedAssignArrayOneToOne;
		}

		public function set retainedAssignArrayOneToOne(value:ArrayCollection):void {
			_retainedAssignArrayOneToOne=value;
		}


		public function get retainedAssignManyToOne():Object {
			return _retainedAssignManyToOne;
		}

		public function set retainedAssignManyToOne(value:Object):void {
			_retainedAssignManyToOne=value;
		}


		/**
		 * Checks if the click event is landing on the right asset.
		 *
		 * @param ad the archibus drawing
		 * @param ids room asset's ids
		 * @param assetOb the aset object related witht the click event.
		 *
		 * @return true of the click is valid, false otherwise.
		 */
		public function isValidClick(assetOb:AssetObject):Boolean {
			//3038381: even views set selectionMode="0", but they have JS click event handler to get clicked asset's detail report, in tis case no toggle
			// select is not allowed
			//if (this.appRef.perDrawingConfig.selectionMode == SelectionMode.NONE)
			//	return false;

			// even when the assign object is not valid when in the assign mode, the select event can still be invoked.
			//if (this.appRef.perDrawingConfig.assignMode != AssignMode.NONE && this._assignedAsset != null)
			//	return false;

			// did not click on the asset OR the asset is not selectable duirng the assign action
			if (assetOb == null || assetOb.properties == null || (this.appRef.perDrawingConfig.selectionMode == SelectionMode.ASSIGNEDONLY && !assetOb.properties.isSelectable))
				return false;

			return true;
		}

		/**
		 * Checks if the asset has not been assigned and can be assigned.
		 *
		 * @param assetOb - the asset object.
		 * @return true if the asset has not been assigned and can be assigned. false otherwise.
		 */
		public function alreadyAssigned(assetOb:AssetObject):Boolean {
			var storedAssignValue:String=this.assignedAssetsOneToOne[assetOb.properties.jsonData.ids];
			if (storedAssignValue != null && storedAssignValue != this._assignedAsset.value) {
				Alert.show(wordSelectedRoom + ": " + assetOb.properties.jsonData.ids.toString() + " " + wordAlreadyAssignedTo + ": " + storedAssignValue);
				return true;
			} else
				return false;
		}

		public function setRetainedArrayOneOnOne(assetOb:AssetObject):void {
			var ob:Object=null;

			// First check to see if this item exists in our retained list already or not
			var containsItem:Boolean=false;
			for (var i:int=0; i < this.retainedAssignArrayOneToOne.length && !containsItem; i++) {
				ob=this.retainedAssignArrayOneToOne[i];
				if (ob.val == this._assignedAsset.value) {
					ob.ids=assetOb.properties.jsonData.ids;
					containsItem=true;
				}
			}

			if (!containsItem) {
				ob={};
				ob['val']=this._assignedAsset.value;
				ob['ids']=assetOb.properties.jsonData.ids;
				ob['color']=this.appRef.controlConfig.highlightConfig.assignSelectedColorOverride;
				this.retainedAssignArrayOneToOne.addItem(ob);
			}
		}


		/**
		 * Sets the new map values to the specifying asset object for one on one maps
		 *
		 * @param assetOb the asset object to update the map to.
		 */
		public function updateAssignOneOnOneMaps(assetOb:AssetObject):void {
			//remove the existing values for one on one maps
			var aob:AssetObject=this.assignedMapOneToOne[this._assignedAsset.value];
			if (aob != null) {
				this.assignedMapOneToOne[this._assignedAsset.value]=null;
				this.assignedAssetsOneToOne[aob.properties.jsonData.ids]=null;
			}

			//set the new map values for one on one maps
			if (assetOb != null && assetOb.properties.isSelected) {
				this.assignedMapOneToOne[this._assignedAsset.value]=assetOb;
				this.assignedAssetsOneToOne[assetOb.properties.jsonData.ids]=this._assignedAsset.value;
			}
		}



		/**
		 * Sets the 'select' color value to override the default value.
		 *
		 * @param color color value to be set.
		 */
		public function setSelectColor(color:uint):void {
			this.appRef.controlConfig.highlightConfig.assignSelectedColorOverride=color;
		}

		/**
		 * Creates the assigned asset object.
		 *
		 * @param fullField the full field of the object (such as employee) to be assigned.
		 * @param val the value to be assigned.
		 * @param color the color to be set.
		 */
		public function setToAssign(fullField:String, val:String, color:uint):void {
			this._assignedAsset=new AssignedObject(fullField, val, color);

			if(_assignedAsset != null && this.appRef.currentState == ToolbarMode.MULTI_SELECT){
				this.resetMaps();
				this.appRef.ensureZoomWindowHidden();
			}
		}

		/**
		 * Unassigns the specifying object based on the field name and value. Removes it from the assigned map.
		 *
		 * @param fullField the full field of the object (such as employee) to be unassigned.
		 * @param val the value to be unassigned.
		 */
		public function unassign(fullField:String, val:String):void {
			if (this.appRef.perDrawingConfig.assignMode == AssignMode.NONE || fullField == null || val == null)
				return;

			switch (this.appRef.perDrawingConfig.assignMode) {
				case AssignMode.ONE_TO_ONE:
					var aob:AssetObject=this.assignedMapOneToOne[val];
					this.assignedMapOneToOne[val]=null;
					if (aob != null) {
						assignedAssetsOneToOne[aob.properties.jsonData.ids]=null;
						aob.unselect();
						var b:Boolean=false;
						for (var i:int=0; i < this.retainedAssignArrayOneToOne.length && !b; i++) {
							if (this.retainedAssignArrayOneToOne[i].val == val) {
								this.retainedAssignArrayOneToOne.removeItemAt(i);
								b=true;
							}
						}
					}
					break;

				case AssignMode.ONE_TO_MANY:
					// The value being passed in is that of a location,
					// in the following form 'HQ-17-101'
					// kb#3038455 - the Undo button for a single room does not remove the color in the drawing for that room	
					var ids:Array=val.split(this.appRef.controlConfig.runtimeConfig.keyDelimiter);

					aob=this.retainedAssignOneToMany[ids];
					if (aob != null) {
						var ad:ArchibusDrawing=getArchibusDrawingFromIds(ids);
						if (ad != null)
							ad.toggleAssetSelection(aob.properties.jsonData.ids, this.appRef.perDrawingConfig.selectionMode);
					}
					break;

				case AssignMode.MANY_TO_ONE:
					aob=assignedMapManyToOne[val]; // Get the previously assigned room

					if (aob != null) {
						// if a room has been previously associated with the specifed value,
						// then we will do one of 2 things:
						//		1.	If there are no other values associated with that room, then unselect it.
						//		2.	If there are other values, do not modify its appearance.
						//

						var previousAssetId:String=idsToString(aob.properties.jsonData.ids);
						var tot:int=retainedAssignManyToOne[previousAssetId] as int;

						if (tot == 1) {
							ad=getArchibusDrawingFromIds(aob.properties.jsonData.ids);
							if (ad != null)
								ad.toggleAssetSelection(aob.properties.jsonData.ids, this.appRef.perDrawingConfig.selectionMode);
						}
						retainedAssignManyToOne[previousAssetId]=tot - 1;
						assignedMapManyToOne[val]=null;
					}
					break;

				case AssignMode.MANY_TO_MANY:
					// TBD
					break;

				default:
					break;
			}
		}

		/**
		 * Restore the asset's color to the stored assigned assets' colors.
		 *
		 * @param ads an array of archibus drawings in the drawing container.
		 */
		public function restoreAssignedAssets(ads:Array):void {
			if (ads == null || this.appRef.perDrawingConfig.assignMode == AssignMode.NONE)
				return;

			var af:FillFormat=this.appRef.controlConfig.highlightConfig.getFillFormat(FillTypes.SELECTED);
			// Leaving the id set to SELECTED will reset highlights
			af.id=FillTypes.NONE;

			//loop throught the drawing array
			for (var i:Number=0; i < ads.length; i++) {
				var ad:ArchibusDrawing=ads[i] as ArchibusDrawing;
				
				if(this.appRef.perDrawingConfig.assignMode == AssignMode.ONE_TO_ONE){
					for (var j:Number=0; j < this.retainedAssignArrayOneToOne.length; j++) {
						var item:Object=this.retainedAssignArrayOneToOne[j];
						af.fillColor=item.color;
						ad.assetLayer.setAssetColor(item.ids, af, false);
					}
				}
			}
		}
		
		/**
		 * Gets pre-assgined asset color by ids.
		 */
		public function getAssignedColor(ads:Array):String{
			if(this.appRef.perDrawingConfig.assignMode == AssignMode.ONE_TO_MANY){
				for (var j:Number=0; j < this.assignedMapOneToMany.length; j++) {
					var item:Object=this.assignedMapOneToMany[j];
					if(item.pks.toString() == ads.toString()){
						return item.color;
					}
				}
			}else if(this.appRef.perDrawingConfig.assignMode == AssignMode.ONE_TO_ONE){
				for (var j:Number=0; j < this.retainedAssignArrayOneToOne.length; j++) {
					var item:Object=this.retainedAssignArrayOneToOne[j];
					if(item.ids.toString() == ads.toString()){
						return item.color;
					}
				}
			}else if(this.appRef.perDrawingConfig.assignMode == AssignMode.MANY_TO_ONE && this.retainedAssginedManyToOne.length > 0){
				//xxx: last assgined color???
				for (var j:Number=this.retainedAssginedManyToOne.length - 1; j >0; j--) {
					var item:Object=this.retainedAssginedManyToOne[j];
					if(item.pks.toString() == ads.toString()){
						return item.color;
					}
				}
			}
			return null;
		}

		/**
		 * Rreset all the assigned assets
		 */
		public function clear():void {
			if (_assignedAsset != null)
				_assignedAsset.reset();

			resetMaps();

			//reset the assgined color override
			this.appRef.controlConfig.highlightConfig.assignSelectedColorOverride = 0;

		}

		public function resetMaps():void{
			assignedMapOneToOne={};
			assignedAssetsOneToOne={};
			retainedAssignArrayOneToOne=new ArrayCollection();

			assignedMapOneToMany=new Array();
			retainedAssignOneToMany={};
		}

		/**
		 * get the archibus drawings based on the room ids.
		 *
		 * @param ids the Room ids to retrieve the drawing from.
		 *
		 * @return ArchibusDrawing object.
		 */
		private function getArchibusDrawingFromIds(ids:Array):ArchibusDrawing {
			var loc:Location=new Location();
			var ar:Array=['rm.bl_id', ids[0], 'rm.fl_id', ids[1], 'rm.rm_id', ids[2]];
			loc.setFromArray(ar);
			return this.appRef.getDrawingByLocation(loc);
		}

		/**
		 * convert the ids into a string.
		 *
		 * @param ids - an asset ids' array
		 * @return asset ids as a string.
		 */
		public function idsToString(ids:Array):String {
			var s:String="";
			for (var i:int=0; i < ids.length; i++)
				s+=ids[i];
			return s;
		}

	}
}


