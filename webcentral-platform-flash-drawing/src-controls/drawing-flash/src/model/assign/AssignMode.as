package src.model.assign {

	/**
	 * defines Assign Modes.
	 *
	 * assignMode Usage:

	 * assignMode determines how the application reacts for different types of _assignMode
	 *
	 * The primary use for this is on the onclick event in the main application according to the supported modes:
	 */
	public class AssignMode {
		/**
		 *	Not enabled (the default)
		 */
		public static const NONE:int=0;

		/**
		 * Only one value can be assigned to a single asset, e.g.  Assigning a single person to a room,
		 * not allowing other persons to be assigned to that room
		 */
		public static const ONE_TO_ONE:int=1;

		/**
		 * A value can be assigned to 1 or more assets, e.g.  Allowing more than one person to be assigned
		 * to a room.
		 */
		public static const ONE_TO_MANY:int=2;

		/**
		 * An asset can take on only one value, but the assignment can be applied to multiple assets at the same time,
		 * e.g. Setting the category for a group of rooms.
		 */
		public static const MANY_TO_ONE:int=3;

		/**
		 * Multiple assets can take on multiple values, e.g.  Setting Departments for a group of rooms,
		 * allowing multipe organaizations to both exist in a room.
		 */
		public static const MANY_TO_MANY:int=4;
	}
}
