package src.model.layout {
	import flash.external.ExternalInterface;

	import src.model.data.Location;
	import src.model.util.CommonUtilities;
	import src.model.util.JsFunctionConstant;
	import src.view.drawing.ArchibusDrawing;

	/**
	 * Manages the display of multiple floor plans (drawings) in the single view.
	 * The arrangment is determined based on the Building and DwgName value
	 */
	public class LayoutUtilities {

		/**
		 * Whether the drawing is grouped by column, which value controls the display of drawings. Default to true.
		 */
		private var orderByColumn:Boolean=true;

		/**
		 * Constructor
		 */
		public function LayoutUtilities():void {
			init();
		}

		/**
		 * Retrieves an object mapping the drawing name to its logic position array
		 *
		 * @param ads the archibus drawings loaded.
		 *
		 * @return an object map
		 */
		public function calculatePositions(ads:Array):Object {
			// no drawing is loaded.
			if (ads == null || !ads.length)
				return null;

			var results:Array=getBuildingsAndDrawingNames(ads);
			if (results == null || results.length != 2)
				return null;

			var buildings:Array=results[0];
			var bldgDwgnames:Object=results[1];

			// Now for each building/floor, let's assign a logical position
			var positions:Object={};
			for (var i:int=0; i < buildings.length; i++) {
				var building:String=buildings[i] as String;
				var dwgnames:Array=bldgDwgnames[building] as Array;

				if (orderByColumn)
					dwgnames.sort(Array.DESCENDING);
				else
					dwgnames.sort();

				for (var j:int=0; j < dwgnames.length; j++) {
					var name:String=dwgnames[j];

					var pos:Array=[];
					pos[0]=orderByColumn ? i : j;
					pos[1]=orderByColumn ? j : i;
					positions[name]=pos;
				}
			}

			return positions;
		}

		/**
		 * Gets DrawingsLayoutTable object
		 *
		 * @param ads the archibus drawings loaded.
		 * @return  an DrawingsLayoutTable object
		 */
		public function getDrawingsLayoutTable(ads:Array):LayoutTable {
			var positions:Object=this.calculatePositions(ads);
			if (positions == null)
				return null;

			var buildingsAndDrawingNames:Array=getBuildingsAndDrawingNames(ads);
			if (buildingsAndDrawingNames == null || buildingsAndDrawingNames.length != 2)
				return null;


			return new LayoutTable(buildingsAndDrawingNames, this.orderByColumn);
		}

		/**
		 * Sets the initial value for orderByColumn
		 */
		private function init():void {
			// get the grouping columns that controls the display of drawings
			// this string has the following format:
			//		order=row    or order=column
			//		TBD, to be enhanced in the future to allow specification of other field names such as a property or site field name
			var columnsGroupedBy:String=ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_COLUMNSGROUPEDBY)) as String;
			if (columnsGroupedBy && columnsGroupedBy.length) {
				var ar:Array=columnsGroupedBy.split("=");
				if (ar != null && ar.length == 2) {
					var val:String=ar[1];
					if (val == 'row')
						orderByColumn=false;
				}
			}
		}

		/**
		 * Returns an array of 2 items
		 *	First item	[0] = an array of building names
		 *	Second item	[1] = an object mapping building names to an array of its contained floors' drawing names
		 *
		 * @param ads the archibus drawings loaded.
		 * @return the array
		 */
		private function getBuildingsAndDrawingNames(ads:Array):Array {
			if (ads == null || !ads.length)
				return null;

			// an array of building names
			var buildings:Array=[];

			// an object mapping building names to an array of contained floors
			var buildingFloorsArrayMap:Object={};

			// loop through each loaded drawing
			for (var i:int=0; i < ads.length; i++) {
				var ad:ArchibusDrawing=ads[i] as ArchibusDrawing;

				var loc:Location=ad.location;
				var building:String=loc.getBuilding();
				var dwgname:String=loc.getDrawingName();

				//an array of contained floors' drawing names
				var floors:Array=buildingFloorsArrayMap[building] as Array;
				if (floors == null)
					floors=[];

				floors[floors.length]=dwgname;
				buildingFloorsArrayMap[building]=floors;

				var found:Boolean=false;
				for (var j:int=0; j < buildings.length && !found; j++) {
					var blg:String=buildings[j] as String;
					if (blg == building)
						found=true;
				}

				if (!found)
					buildings[buildings.length]=building;
			}

			buildings.sort();

			var results:Array=[];
			results[0]=buildings;
			results[1]=buildingFloorsArrayMap;

			return results;

		}


		/**
		 * Gets the first drawing with position [0, 0]
		 *
		 * @param ads the archibus drawing loaded
		 *
		 * @return the ArchibusDrawing object.
		 */
		public function getFirstDrawingByLogicPosition(ads:Array):ArchibusDrawing {
			var positions:Object=this.calculatePositions(ads);
			var ad:ArchibusDrawing=null, ad0:ArchibusDrawing;
			for (var i:Number=0; i < ads.length; i++) {
				ad=ads[i] as ArchibusDrawing;

				var ar:Array=positions[ad.location.getDrawingName()];
				if (ar[0] == 0 && ar[1] == 0)
					ad0=ad;
			}
			return ad0;
		}
	}
}


