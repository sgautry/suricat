package src.model.layout {

	/**
	 * Defines the drawing layout table's information within the container, such as the number of rows and columns.
	 * Each cell corresponds to a drawing.
	 */
	public class DrawingsLayoutTable {
		private var _numRows:int=1;
		private var _numColumns:int=1;



		/**
		 * Constructor given the specifying building/drawing names array and orderByColumn.
		 *
		 * @param buildingsAndDrawingNames an array of 2 items (First item is an array of building names, second item is an object mapping building names
		 * 								   to an array of its contained floors' drawing names).
		 * @param orderByColumn Whether the drawing is grouped by column, which value controls the display of drawings. Default to true.
		 */
		public function DrawingsLayoutTable(buildingsAndDrawingNames:Array, orderByColumn:Boolean) {
			super();

			var buildings:Array=buildingsAndDrawingNames[0];
			var bldgDwgnames:Object=buildingsAndDrawingNames[1];

			var columnCnt:Number=orderByColumn ? buildings.length : 1;
			var rowCnt:Number=orderByColumn ? 1 : buildings.length;

			for (var i:int=0; i < buildings.length; i++) {
				var building:String=buildings[i];
				var floors:Array=bldgDwgnames[building];

				if (orderByColumn)
					rowCnt=(floors.length > rowCnt) ? floors.length : rowCnt;
				else
					columnCnt=(floors.length > columnCnt) ? floors.length : columnCnt;
			}

			this._numRows=rowCnt;
			this._numColumns=columnCnt;
		}

		public function get numRows():int {
			return _numRows;
		}

		public function set numRows(value:int):void {
			_numRows=value;
		}

		public function get numColumns():int {
			return _numColumns;
		}

		public function set numColumns(value:int):void {
			_numColumns=value;
		}

	}

}
