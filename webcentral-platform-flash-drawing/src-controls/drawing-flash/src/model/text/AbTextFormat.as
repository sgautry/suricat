package src.model.text {
	import flash.text.*;

	import src.model.util.CommonUtilities;

	/**
	 * Defines supported visual attributes of text
	 */
	public class AbTextFormat extends TextFormat {
		private var _id:String='';

		private static const ID:String='id';
		private static const FONT:String='font';
		private static const COLOR:String='color';
		private static const TEXTHEIGHT:String='textheight';
		private static const ALIGN:String='justification';
		private static const LEFT:String='left';
		private static const CENTER:String='center';
		private static const RIGHT:String='right';
		private static const UNDERLINE:String='underline';

		/**
		 * Constructor.
		 *
		 * @param id the TextFormat id.
		 * @param cfg the text configuration object that contains the font configuration.
		 */
		public function AbTextFormat(id:String=null, cfg:Object=null) {
			if (id != null)
				this._id=id;

			if (cfg) {
				this.setFromFontConfig(id, cfg.font);
			} else {
				init();
			}
		}


		/**
		 * Sets the text format from the font configuration object.
		 *
		 * @param id text format id.
		 * @param cfg font configuration object
		 */
		public function setFromFontConfig(id:String=null, cfg:Object=null):void {
			if (id != null)
				this._id=id;
			if (cfg == null)
				return;

			if (CommonUtilities.isValid(cfg, FONT))
				this.font=cfg.font;
			setColor(cfg);
			setSize(cfg);
			//if (CommonUtilities.isValid(cfg, COLOR))
				//this.color=uint(CommonUtilities.toHex(cfg[COLOR] as String));
			///if (CommonUtilities.isValid(cfg, TEXTHEIGHT))
				//this.size=cfg[TEXTHEIGHT] * 0.6;
			if (CommonUtilities.isValid(cfg, ALIGN))
				setFontAlign(cfg);
			if (CommonUtilities.isValid(cfg, FontStyle.BOLD))
				this.bold=cfg[FontStyle.BOLD];
			if (CommonUtilities.isValid(cfg, FontStyle.ITALIC))
				this.italic=cfg[FontStyle.ITALIC];
			if (CommonUtilities.isValid(cfg, UNDERLINE))
				this.underline=cfg[UNDERLINE];
		}
		public function setColor(cfg:Object):void{
			if (CommonUtilities.isValid(cfg, COLOR)){
				this.color=uint(cfg[COLOR] as String);
			}
		}
		
		public function setSize(cfg:Object):void{
			if (CommonUtilities.isValid(cfg, TEXTHEIGHT)){
				this.size=cfg[TEXTHEIGHT] * 0.6;
			}
		}
		
		public function get id():String {
			return this._id;
		}

		public function set id(id:String):void {
			this._id=id;
		}

		public function toString():String {
			var s:String="{" + " id: " + id + ", font: " + font + ", color: " + color.toString(16) + ", size: " + size.toString() + ", align: " + align.toString() + ", bold: " + bold.toString() + ", italic: " + italic.toString() + ", underline: " + underline.toString() + "}";

			return s;
		}

		/**
		 * Sets the default text format values.
		 */
		private function init():void {
			this.font='Arial';
			this.color=0x000000;
			this.align=TextFormatAlign.CENTER;
			this.size=9;
			this.bold=false;
			this.italic=false;
			this.underline=false;
		}


		/**
		 * Sets the text align value.
		 *
		 * @param cfgFont a configuration object that contains font's properties.
		 */
		private function setFontAlign(cfgFont:Object):void {
			var justification:String=cfgFont.justification;
			if (justification != null && justification.length && (justification == TextFormatAlign.LEFT || justification == TextFormatAlign.CENTER || justification == TextFormatAlign.RIGHT)) {
				this.align=justification;
			} else {
				this.align=TextFormatAlign.CENTER;
			}
		}
		
		public function copy():AbTextFormat{
			var tf:AbTextFormat = new AbTextFormat();
			tf.id= this.id;
			tf.font = this.font
			tf.align=this.align;
			tf.size=this.size;
			tf.bold=this.bold;
			tf.italic=this.italic;
			tf.underline=this.underline;
			
			return tf;
		}
	}
}
