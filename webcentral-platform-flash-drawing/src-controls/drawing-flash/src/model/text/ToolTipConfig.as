package src.model.text {
	import src.model.data.SourceType;
	import src.model.data.DataSource;

	/**
	 * Defines the tooltip configurations.
	 *
	 */
	public class ToolTipConfig extends Object {
		/**
		 * is the tooltip enabled?
		 */
		protected var _enabled:Boolean=false;

		/**
		 * the minimum width for the tooltip
		 */
		protected var _minWidth:int=100;

		/**
		 * the minimum height for the tooltip
		 */
		protected var _minHeight:int=25;

		/**
		 * the datasource type of the tooltip
		 */
		protected var _dsType:int=SourceType.NONE;

		/**
		 * canvas color for the tooltip
		 */
		protected var _canvasColor:uint=0xEEEEFF;

		/**
		 * canvas opacity for the tooltip
		 */
		protected var _canvasOpacity:Number=0.85;


		/**
		 * Constructor.
		 */
		public function ToolTipConfig() {
			super();
		}

		public function get minWidth():int {
			return _minWidth;
		}

		public function get minHeight():int {
			return _minHeight;
		}

		public function get dsType():int {
			return _dsType;
		}

		/**
		 * Validates that a specified source type is valid by retrieving related fieldDefs.
		 * if not, set to a known valid source type or SourceTypes.SOURCE_NONE if needed
		 */
		public function validateSourceType():void {
			var dsTmp:DataSource=null;
			if (_dsType == SourceType.LABEL) {
				dsTmp=new DataSource(SourceType.LABEL);
				if (!dsTmp.setFieldDefs())
					_dsType=SourceType.NONE;
			} else if (_dsType == SourceType.HILITE) {
				dsTmp=new DataSource(SourceType.HILITE);
				if (!dsTmp.setFieldDefs())
					_dsType=SourceType.NONE;
			}
		}

		public function get enabled():Boolean {
			return _enabled;
		}

		public function get canvasColor():uint {
			return _canvasColor;
		}

		public function get canvasOpacity():Number {
			return _canvasOpacity;
		}

		/**
		 * Sets the tooltip properties from the configuration object.
		 *
		 * @param cfg the configuration object.
		 */
		public function setFromConfig(cfg:Object):void {
			if (cfg) {
				_enabled=cfg.enabled;
				_canvasColor=cfg.color;
				_canvasOpacity=cfg.opacity;
				_minWidth=cfg.minWidth;
				_minHeight=cfg.minHeight;

				var s:String=cfg.datasource;
				if (s == "name")
					_dsType=SourceType.NONE;
				else if (s == "label")
					_dsType=SourceType.LABEL;
				else if (s == "highlight")
					_dsType=SourceType.HILITE;
			}
		}
	}
}
