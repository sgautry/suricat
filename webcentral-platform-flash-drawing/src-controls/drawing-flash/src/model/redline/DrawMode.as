package src.model.redline {

	/**
	 * Defines Redlining Draw Modes and its related index value constant.
	 */
	public class DrawMode {
		public static const drawModeOff:String="drawOff";
		public static const drawModeOffIndex:int=-1;

		public static const drawModeLine:String="line";
		public static const drawModeLineIndex:int=0;

		public static const drawModeRectangle:String="rectangle";
		public static const drawModeRectangleIndex:int=1;

		public static const drawModeArrow:String="arrow";
		public static const drawModeArrowIndex:int=2;

		public static const drawModePositionMarker:String="positionMarker";
		public static const drawModePositionMarkerIndex:int=3;

		public static const drawModeText:String="text";
		public static const drawModeTextIndex:int=4;

		public static function getDrawModeByIndex(index:int):String {
			var drawMode:String=DrawMode.drawModeLine; // default draw line
			switch (index) {
				case 1:

					//draw rectangle
					drawMode=DrawMode.drawModeRectangle;
					break;

				case 2:
					//type arrow
					drawMode=DrawMode.drawModeArrow;
					break;
				case 3:
					//position marker
					drawMode=DrawMode.drawModePositionMarker;
					break;
				case 4:
					//type text
					drawMode=DrawMode.drawModeText;
					break;
			}
			return drawMode;
		}
	}
}
