package src.model.config {
	import src.model.text.AbTextFormat;
	import src.model.util.CommonUtilities;

	/**
	 * Represents the drawing control's label configuration defined in controls-drawing.xml file.
	 */
	public class LabelConfig extends Object {
		private var textTypes:Object={};

		private static const DEFAULT_TEXT_COLOR:String='0';

		public static const FORMAT_DEFAULT:String='default';
		public static const FORMAT_PARENT:String='parent';

		public function LabelConfig() {
			super();
		}

		/**
		 *
		 * @param ob
		 */
		public function initialize(ob:Object):void {
			// Set the 'default' and 'parent' label styles
			appendLabelStyle(FORMAT_DEFAULT, ob);
			appendLabelStyle(FORMAT_PARENT, ob);
		}

		private function appendLabelStyle(name:String, ob:Object):void {
			if (ob != null && CommonUtilities.isValid(ob.labels, name))
				textTypes[name]=new AbTextFormat(name, ob.labels[name]);
		}

		public function getTextColor(type:String):String {
			var text:AbTextFormat=textTypes[type];
			if (text == null)
				return DEFAULT_TEXT_COLOR;

			var color:String=String(text.color);
			return (color == null || !color.length) ? DEFAULT_TEXT_COLOR : color;
		}

		public function getTextFormat(id:String):AbTextFormat {
			var tf:AbTextFormat=textTypes[id] as AbTextFormat;

			if (tf == null)
				textTypes[id]=new AbTextFormat(id);

			return tf;
		}
	}
}
