package src.model.config {
	import src.model.util.CommonUtilities;
	import src.view.highlight.FillFormat;
	import src.view.highlight.FillTypes;

	/**
	 * Represents the drawing control's highlight configuration defined in controls-drawing.xml file.
	 */
	public class HighlightConfig extends Object {
		/**
		 * fill types map
		 */
		private var _fillTypes:Object={};

		/**
		 * the selected color to override the default value.
		 */
		private var _assignSelectedColorOverride:uint=0;

		/**
		 * Constructor.
		 */
		public function HighlightConfig() {
			super();
		}


		/**
		 * Initializes the fill style with the specifying obeject's value.
		 *
		 * @param ob the object with the fill style information.
		 */
		public function initialize(ob:Object):void {
			// Set the 'assigned', 'unassigned', and 'selected' fill styles
			appendFillStyle(FillTypes.ASSIGNED, ob);
			appendFillStyle(FillTypes.UNASSIGNED, ob);
			appendFillStyle(FillTypes.SELECTED, ob);
			appendFillStyle(FillTypes.NOFILL, ob);

			// create a default fillstyle for 'unselected'
			fillTypes[FillTypes.UNSELECTED]=new FillFormat(FillTypes.UNSELECTED);
		}

		/**
		 * Getter for _fillTypes
		 * @return fill types map
		 */
		public function get fillTypes():Object {
			return _fillTypes;
		}

		/**
		 * Setter for _fillTypes
		 * @param value the value to set
		 */
		public function set fillTypes(value:Object):void {
			_fillTypes=value;
		}

		/**
		 * Adds the FillFormat object with the  specifying highlight properties to the fillTypes map.
		 *
		 * @param name fill type name
		 * @param ob the object to retrieve the highlight properties.
		 */
		private function appendFillStyle(name:String, ob:Object):void {
			if (ob != null && CommonUtilities.isValid(ob.highlights, name))
				fillTypes[name]=new FillFormat(name, ob.highlights[name]);
		}

		/**
		 * Gets fill format for the specifying fill type. If fill type is SELECTED, override the fill color with the _assignSelectedColorOverride value.
		 *
		 * @param type the fill type
		 * @return FillFormat object
		 */
		public function getFillFormat(type:String):FillFormat {
			var afRet:FillFormat=null;
			var af:FillFormat=fillTypes[type] as FillFormat;
			if (af != null) {
				afRet=new FillFormat();
				afRet.copy(af);
				if (type == FillTypes.SELECTED && this._assignSelectedColorOverride > 0)
					afRet.fillColor=this._assignSelectedColorOverride;
			}

			return afRet;
		}

		/**
		 * Gets fill color for the specifying fill type. If fill type does not exist, return the unassigned fill type's fill color value instead.
		 *
		 * @param type the fill type
		 * @return fill color value
		 */
		public function getFillColor(type:String):uint {
			var defaultFillType:FillFormat=fillTypes[FillTypes.UNASSIGNED];
			var fillType:FillFormat=fillTypes[type];
			if (fillType == null)
				return defaultFillType.fillColor;

			return fillType.fillColor;
		}

		/**
		 * Gets fill opacity thickness for the specifying fill type. If fill type does not exist, return the unassigned fill type's fill opacity instead.
		 *
		 * @param type the fill type
		 * @return fill opacity value
		 */
		public function getFillOpacity(type:String):Number {
			var defaultFillType:FillFormat=fillTypes[FillTypes.UNASSIGNED] as FillFormat;

			var fillType:FillFormat=fillTypes[type];
			if (fillType == null)
				return defaultFillType.fillOpacity;

			return fillType.fillOpacity;
		}

		/**
		 * Gets boundary thickness for the specifying fill type. If fill type does not exist, return the unassigned fill type's boundary thickness instead.
		 *
		 * @param type the fill type
		 * @return boundary thickness value
		 */
		public function getBoundaryThickness(type:String):uint {
			var fillType:FillFormat=fillTypes[type];
			if (fillType == null) {
				var defaultFillType:FillFormat=fillTypes[FillTypes.UNASSIGNED] as FillFormat;
				return defaultFillType.boundaryThickness;
			} else {
				return fillType.boundaryThickness;
			}
		}

		/**
		 * Gets boundary color for the specifying fill type. If fill type does not exist, return the unassigned fill type's boundary color instead.
		 *
		 * @param type the fill type
		 * @return boundary color value
		 */
		public function getBoundaryColor(type:String):uint {
			var defaultFillType:FillFormat=fillTypes[FillTypes.UNASSIGNED] as FillFormat;

			var fillType:FillFormat=fillTypes[type];
			if (fillType == null)
				return defaultFillType.boundaryColor;

			return fillType.boundaryColor;
		}

		/**
		 * Gets boundary opacity for the specifying fill type. If fill type does not exist, return the unassigned fill type's boundary opacity instead.
		 *
		 * @param type the fill type
		 * @return boundary opacity value
		 */
		public function getBoundaryOpacity(type:String):Number {

			var fillType:FillFormat=fillTypes[type];
			if (fillType == null) {
				var defaultFillType:FillFormat=fillTypes[FillTypes.UNASSIGNED] as FillFormat;
				return defaultFillType.boundaryOpacity;
			} else {
				return fillType.boundaryOpacity;
			}
		}

		/**
		 * Getter for _assignSelectedColorOverride
		 * @return integer
		 */
		public function get assignSelectedColorOverride():uint {
			return _assignSelectedColorOverride;
		}

		/**
		 * Setter for _assignSelectedColorOverride
		 * @param value the value to be set.
		 */
		public function set assignSelectedColorOverride(value:uint):void {
			_assignSelectedColorOverride=value;
		}
	}
}
