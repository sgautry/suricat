package src.model.config {
	import com.adobe.serialization.json.JSON;

	import flash.external.ExternalInterface;

	import src.model.util.CommonUtilities;
	import src.model.util.JsFunctionConstant;
	import src.view.asset.*;
	import src.view.highlight.*;

	/**
	 * Represents the drawing control configuration defined in controls-drawing.xml file.
	 */
	public class ControlConfig extends Object {
		/**
		 * Control's highlight configuration from controls-drawing.xml file
		 */
		private var _highlightConfig:HighlightConfig=new HighlightConfig();

		/**
		 * Control's label configuration from controls-drawing.xml file
		 */
		private var _labelConfig:LabelConfig=new LabelConfig();

		/**
		 * Control's runtime configuration from controls-drawing.xml file
		 */
		private var _runtimeConfig:RuntimeConfig=null;

		/**
		 * Control's tooltip configuration from controls-drawing.xml file
		 */
		private var _tooltipConfig:Object={};

		/**
		 * NOTES: Ideal Text size and Minimal Text Size are the boundaries for the text size. The actual text size is usually in between in order to make the labels fit in their room. However, when the
		 * actual text size is out of the boundary, i.e. smaller than the Minimal Text size, the Ideal Text Size will be used.
		 */

		/**
		 * Property: ideal label text size (in points) in printed report.
		 */
		private var _idealLabelTextSize:uint=11;
		/**
		 * Property: minimum text size (in points) in printed report.
		 */
		private var _minimumLabelTextSize:uint=8;

		/**
		 * Shrink Text to fit Boundary in Flash Drawings? Yes/No.  Default is Yes.
		 */
		private var _shrinkLabelTextToFit:Boolean=true;

		/**
		 * The amount to zoom in when the zoom is enabled when finding an asset.
		 */
		private var _zoomFactor:Number=5.0;


		/**
		 * Specify whether or not to display labels in conjunction with the 'noFill' values
		 */
		private var _nofillLabels:Boolean=false;

		/**
		 * Specify whether or not to display tooltips in conjunction with the 'noFill' values
		 */
		private var _nofillTooltip:Boolean=false;


		/**
		 * flag to ensure the controls-drawing.xml is only read once.
		 */
		private var _initialized:Boolean=false;

		/**
		 * Default Constructor.
		 */
		public function ControlConfig() {
			super();
		}

		/**
		 * Retrieve the control configuration through JavaScript calls.
		 *
		 * @return true if the control's configuration has been retrieved successfully.
		 */
		public function init():Boolean {
			if (!_initialized) {
				var jsonCfg:String=ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_GETCONFIG));

				if (jsonCfg != null) {
					var ob:Object=JSON.decode(jsonCfg) as Object;
					if (ob == null)
						return false;

					_highlightConfig.initialize(ob);

					_labelConfig.initialize(ob);

					if (CommonUtilities.isValid(ob, 'tooltip'))
						_tooltipConfig=ob.tooltip;

					if (CommonUtilities.isValid(ob, 'zoomFactor'))
						_zoomFactor=ob.zoomFactor;

					if (CommonUtilities.isValid(ob, 'nofillTooltip'))
						_nofillTooltip=ob.nofillTooltip;

					if (CommonUtilities.isValid(ob, 'nofillLabels'))
						_nofillLabels=ob.nofillLabels;

					if (CommonUtilities.isValid(ob, 'idealLabelTextSize'))
						_idealLabelTextSize=ob.idealLabelTextSize;

					if (CommonUtilities.isValid(ob, 'minimumLabelTextSize'))
						_minimumLabelTextSize=ob.minimumLabelTextSize;

					if (CommonUtilities.isValid(ob, 'shrinkLabelTextToFit'))
						_shrinkLabelTextToFit=ob.shrinkLabelTextToFit;

					_initialized=true;
				}

				this._runtimeConfig=new RuntimeConfig(this._highlightConfig.getFillFormat(FillTypes.NOFILL));
			}

			return _initialized;
		}

		/**
		 * Getter for _labelConfig.
		 * @return LabelConfig object
		 */
		public function get labelConfig():LabelConfig {
			return _labelConfig;
		}

		/**
		 * Getter for _highlightConfig.
		 * @return HighlightConfig object
		 */
		public function get highlightConfig():HighlightConfig {
			return _highlightConfig;
		}


		/**
		 * Getter for _idealLabelTextSize.
		 * @return idea text label size
		 */
		public function getIdealLabelTextSize():uint {
			return this._idealLabelTextSize;
		}

		/**
		 * Getter for _minimumLabelTextSize.
		 * @return minimum label text size
		 */
		public function getMinimumLabelTextSize():uint {
			return this._minimumLabelTextSize;
		}


		/**
		 * Getter for _shrinkLabelTextToFit
		 * @return ShrinkLabelTextToFit object
		 */
		public function getShrinkLabelTextToFit():Boolean {
			return this._shrinkLabelTextToFit;
		}


		/**
		 * Getter for _tooltipConfig.
		 * @return a map object
		 */
		public function getTooltipCfg():Object {
			return _tooltipConfig;
		}

		/**
		 * Getter for _zoomFactor.
		 * @return a number
		 */
		public function getZoomFactor():Number {
			return _zoomFactor;
		}



		/**
		 * Getter for _nofillLabels.
		 * @return true/false
		 */
		public function isNofillLabelsEnabled():Boolean {
			return _nofillLabels;
		}

		/**
		 * Getter for _nofillTooltip.
		 * @return true/false
		 */
		public function isNofillTooltipEnabled():Boolean {
			return _nofillTooltip;
		}

		/**
		 * Getter for _runtimeConfig.
		 * @return RuntimeConfig object
		 */
		public function get runtimeConfig():RuntimeConfig {
			return _runtimeConfig;
		}


	}

}
