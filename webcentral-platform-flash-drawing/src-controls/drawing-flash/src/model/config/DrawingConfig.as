package src.model.config {
	import com.adobe.serialization.json.JSON;

	import src.model.assign.AssignMode;
	import src.model.assign.SelectionMode;
	import src.model.util.CommonUtilities;
	import src.view.asset.*;
	import src.view.highlight.*;
	import src.view.text.LabelLayer;

	/**
	 *
	 * Defines the per-drawing configuration passed from javascript and view files.
	 *
	 */
	public class DrawingConfig extends Object {
		/**
		 *
		 * default constructor.
		 */
		public function DrawingConfig() {
			reset();
		}

		/**
		 * Property: version of this DrawingOpts object
		 */
		private var _ver:Number=1.0;

		/**
		 * Property: folder containing drawing to load
		 */
		private var _folder:String='';

		/**
		 * Property: name of a drawing containing records to highlight (usually composed from location keys and has no .swf suffix)
		 */
		private var _dwgName:Array=null;

		/**
		 * Property: actual name of a drawing to load, not always contain the location keys. have .swf suffix
		 */
		private var _rawDwgName:String='';

		/**
		 * Property: comma delimited list of non-standard asset files to load
		 */
		private var _assetTypes:String=null;

		/**
		 * Property: true if zoom in to the specified asset, false otherwise. default is true
		 */
		private var _zoomToId:Boolean=false;

		/**
		 * Property: true when highlighting, ensure only specified ids are selected, false otherwise.
		 */
		private var _exclusive:Boolean=false;

		/**
		 * Property: flag to control whether or not more than 1 drawing can be loaded at a time
		 */
		private var _multiple:Boolean=true;

		/**
		 * Property: an asset id to be highlighted
		 */
		private var _highlightId:String='';

		/**
		 * Property: true if force the load of a drawing if needed. default to false.
		 */
		private var _forceload:Boolean=false;

		/**
		 * Property: allows onclick events to modify the graphics:  0 = None, 1 = Assigned Only, 2 = All
		 */
		private var _selectionMode:int=SelectionMode.ALL;

		/**
		 * Property: determines if more than one asset can be selected at a time
		 */
		private var _multipleSelectionEnabled:Boolean=true;

		/**
		 * Property: assign mode for the Drawing Control.
		 */
		private var _assignMode:int=AssignMode.NONE;

		/**
		 * Property: fill settings that override the mode setting
		 */
		private var _fill:FillFormat=null;

		/**
		 * Property: assigned, unassigned, selected, unselected, none, or empty string
		 */
		private var _mode:String='';

		/**
		 * Property: array of DrawingRec objects
		 */
		private var _recs:Array=null;

		/**
		 * Property: if highlightAssets called with this set true, these will override datasource changes
		 */
		private var _persistRecFills:Boolean=false;

		/**
		 * Property: optional suffix to be applied to the drawing name for loading different backgrounds
		 */
		private var _backgroundSuffix:String='';

		/**
		 * Property: optional suffix to be applied to the asset file name to load
		 */
		private var _assetSuffix:String='';

		/**
		 * Property: redmarks properties
		 */
		private var _redmarks:Object=null;

		/**
		 * Property: position properties.
		 */
		private var _position:Object=null;

		/**
		 * Property:  Whether or not this DwgOptions object has been initialized or not
		 */
		private var _initialized:Boolean=false;


		/**
		 * set drawing options
		 */
		public function set(opts:Object):void {
			if (opts == null)
				return;

			reset();

			if (CommonUtilities.isValid(opts, 'ver'))
				this._ver=opts.ver;
			if (CommonUtilities.isValid(opts, 'folder'))
				this._folder=opts.folder;
			if (CommonUtilities.isValid(opts, 'dwgName'))
				this._dwgName=opts.dwgName;
			if (CommonUtilities.isValid(opts, 'rawDwgName'))
				this._rawDwgName=opts.rawDwgName;
			if (CommonUtilities.isValid(opts, 'assetTypes'))
				this._assetTypes=opts.assetTypes;
			if (CommonUtilities.isValid(opts, 'zoomToId'))
				this._zoomToId=opts.zoomToId;
			if (CommonUtilities.isValid(opts, 'exclusive'))
				this._exclusive=opts.exclusive;
			if (CommonUtilities.isValid(opts, 'multiple'))
				this._multiple=opts.multiple;
			if (CommonUtilities.isValid(opts, 'highlightId'))
				this._highlightId=opts.highlightId;
			if (CommonUtilities.isValid(opts, 'forceload'))
				this._forceload=opts.forceload;
			if (CommonUtilities.isValid(opts, 'selectionMode'))
				this._selectionMode=opts.selectionMode;
			if (CommonUtilities.isValid(opts, 'multipleSelectionEnabled'))
				this._multipleSelectionEnabled=opts.multipleSelectionEnabled;
			if (CommonUtilities.isValid(opts, 'assignMode'))
				this._assignMode=opts.assignMode;
			if (CommonUtilities.isValid(opts, 'mode'))
				this._mode=opts.mode;
			if (CommonUtilities.isValid(opts, 'fill'))
				this._fill.setFromDwgOptions(opts.fill);
			if (CommonUtilities.isValid(opts, 'recs'))
				this._recs=opts.recs;
			if (CommonUtilities.isValid(opts, 'persistRecFills'))
				this._persistRecFills=opts.persistRecFills;
			if (CommonUtilities.isValid(opts, 'backgroundSuffix'))
				this._backgroundSuffix=opts.backgroundSuffix;
			if (CommonUtilities.isValid(opts, 'assetSuffix'))
				this._assetSuffix=opts.assetSuffix;
			if (CommonUtilities.isValid(opts, 'redmarks'))
				this._redmarks=JSON.decode(opts.redmarks);
			if (CommonUtilities.isValid(opts, 'position'))
				this._position=JSON.decode(opts.position);

			// Special handler for minAssetTextFactor value
			if (CommonUtilities.isValid(opts, 'minAssetTextFactor'))
				LabelLayer.minHeightMultiplier=opts.minAssetTextFactor;

			_initialized=true;
		}

		/**
		 * Sets all option paramters to default.
		 */
		public function reset():void {
			this._ver=1.0;
			this._folder='';
			this._dwgName=[];
			this._rawDwgName='';
			this._assetTypes='';
			this._zoomToId=false;
			this._exclusive=false;
			this._highlightId=new String();
			this._forceload=false;
			this._selectionMode=SelectionMode.ALL;
			this._multipleSelectionEnabled=true;
			this._assignMode=AssignMode.NONE;
			this._fill=new FillFormat();
			this._recs=null;
			this._mode=FillTypes.SELECTED;
			this._persistRecFills=false;
			this._backgroundSuffix='';
			this._assetSuffix='';
			this._redmarks={};
			this._position={};
		}

		/**
		 * Getter.
		 * @return version
		 *
		 */
		public function get ver():Number {
			return this._ver;
		}

		/**
		 * Getter.
		 * @return drawing name as n array
		 *
		 */
		public function get dwgName():Array {
			return this._dwgName;
		}

		/**
		 * Getter.
		 * @return raw drawing name as string
		 *
		 */
		public function get rawDwgName():String {
			return this._rawDwgName;
		}
		
		public function set rawDwgName(dwgname:String):void {
			 this._rawDwgName = dwgname;
		}

		/**
		 * Getter.
		 * @return asset types as string
		 *
		 */
		public function get assetTypes():String {
			return this._assetTypes;
		}

		/**
		 * Getter.
		 * @return folder path
		 *
		 */
		public function get folder():String {
			return this._folder;
		}

		/**
		 * Getter.
		 * @return the id to zoom to.
		 *
		 */
		public function get zoomToId():Boolean {
			return this._zoomToId;
		}

		/**
		 * Getter.
		 * @return exclusive (true/false)
		 *
		 */
		public function get exclusive():Boolean {
			return this._exclusive;
		}

		/**
		 * Getter.
		 * @return multiple (true/false)
		 *
		 */
		public function get multiple():Boolean {
			return this._multiple;
		}

		/**
		 * Getter.
		 * @return highlightId as string
		 *
		 */
		public function get highlightId():String {
			return this._highlightId;
		}
		public function set highlightId(highlightId:String):void {
			 this._highlightId = highlightId;
		}

		/**
		 * Getter.
		 * @return forceload value as true/false
		 *
		 */
		public function get forceload():Boolean {
			return this._forceload;
		}

		/**
		 * Getter.
		 * @return mode as String
		 *
		 */
		public function get mode():String {
			return this._mode;
		}

		/**
		 * Getter.
		 * @return selectionMode as integer
		 *
		 */
		public function get selectionMode():int {
			return this._selectionMode;
		}

		/**
		 * Getter.
		 * @return multipleSelectionEnabled (true/false)
		 *
		 */
		public function get multipleSelectionEnabled():Boolean {
			return this._multipleSelectionEnabled;
		}

		/**
		 * Getter.
		 * @return assignMode as integer
		 *
		 */
		public function get assignMode():int {
			return this._assignMode;
		}

		/**
		 * Getter.
		 * @return fill format
		 *
		 */
		public function get fill():FillFormat {
			return this._fill;
		}

		public function set fill(fill:FillFormat):void {
			this._fill=fill;
		}

		/**
		 * Getter.
		 * @return recs as an array
		 *
		 */
		public function get recs():Array {
			return this._recs;
		}

		public function setRecs(recs:Array):void {
			this._recs=recs;
		}

		/**
		 * Getter.
		 * @return persistRecFills (true/false)
		 *
		 */
		public function get persistRecFills():Boolean {
			return this._persistRecFills;
		}

		/**
		 * Getter.
		 * @return backgroundSuffix as String
		 *
		 */
		public function get backgroundSuffix():String {
			return this._backgroundSuffix;
		}

		/**
		 * Getter.
		 * @return assetSuffix as String
		 *
		 */
		public function get assetSuffix():String {
			return this._assetSuffix;
		}

		/**
		 * Getter.
		 * @return initialized as true/false
		 *
		 */
		public function get initialized():Boolean {
			return this._initialized;
		}

		/**
		 * Getter.
		 * @return redmarks object
		 *
		 */
		public function get redmarks():Object {
			return this._redmarks;
		}

		/**
		 * Getter.
		 * @return position object
		 *
		 */
		public function get position():Object {
			return this._position;
		}

		/**
		 * Setter.
		 * @param id id to zoom to.
		 *
		 */
		public function set zoomToId(id:Boolean):void {
			this._zoomToId=id;
		}

		/**
		 * Setter.
		 * @param s mode to set to
		 *
		 */
		public function set mode(s:String):void {
			this._mode=s;
		}

		/**
		 * Converts the configuration to a string.
		 * @return String
		 *
		 */
		public function toString():String {
			var result:String="DrawingConfig :\n";
			result+="version: " + this.ver + "\n";
			result+="folder: " + this.folder + "\n";
			result+="dwgname: " + this.dwgName + "\n";
			result+="rawDwgName: " + this.rawDwgName + "\n";
			result+="assettypes: " + this.assetTypes + "\n";
			result+="zoomToId: " + this.zoomToId + "\n";
			result+="exclusive: " + this.exclusive + "\n";
			result+="highlightId: " + this.highlightId + "\n";
			result+="forceload: " + this.forceload + "\n";
			result+="selectionMode: " + this.selectionMode + "\n";
			result+="multipleSelectionEnabled: " + this.multipleSelectionEnabled + "\n";
			result+="assignMode: " + this.assignMode + "\n";
			result+="fill" + this.fill + "\n";
			result+="recs: " + this.recs + "\n";
			result+="mode: " + this.mode + "\n";
			result+="persistRecFills: " + this.persistRecFills + "\n";
			result+="backgroundSuffix: " + this.backgroundSuffix + "\n";
			result+="assetSuffix: " + this.assetSuffix + "\n";
			result+="redmarks: " + this.redmarks + "\n";
			result+="position: " + this.position;

			return result;
		}

	}

}


