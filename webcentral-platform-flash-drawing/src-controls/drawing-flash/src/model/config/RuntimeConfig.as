package src.model.config {
	import flash.external.ExternalInterface;

	import src.model.util.CommonUtilities;
	import src.model.util.JsFunctionConstant;
	import src.view.highlight.FillFormat;

	public class RuntimeConfig extends Object {
		private var _nofillField:String='';

		private var _nofillValues:Object=null;


		/**
		 * Property: an array of optional array of full field names, used to instruct the
		 * onclick handler to return that extra information.
		 * ??? should rename to _optionalFields!
		 */
		private var _optionalPKFields:Array=null;

		/**
		 * Property: delimiter for location keys.
		 */
		private var _keyDelimiter:String=';';


		/**
		 * Constructor given the specifying fill format.
		 *
		 * @param fillFormat the fill format
		 */
		public function RuntimeConfig(fillFormat:FillFormat) {
			super();

			// The remaining configuration info is not from the drawing-control.xml file, but
			// instead is specified at runtime per app
			_nofillField=ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_GETNOFILLFIELD));

			setNoFillValues(fillFormat);

			var delimiter:String=ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_GETDELIM));
			if (delimiter != null && delimiter.length > 0)
				this._keyDelimiter=delimiter;

			_optionalPKFields=ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_GETOPTIONALPKFIELDS));

		}

		public function get nofillField():String {
			return _nofillField;
		}

		public function get nofillValues():Object {
			return _nofillValues;
		}

		public function get optionalPKFields():Array {
			return _optionalPKFields;
		}

		public function getNofillAF(key:String):FillFormat {
			var af:FillFormat=null;
			if (CommonUtilities.isValid(_nofillValues, key))
				af=_nofillValues[key];

			return af;
		}



		/**
		 * Sets no fill values map.
		 *
		 * @param fillFormat
		 */
		private function setNoFillValues(fillFormat:FillFormat):void {
			// The Array returned from the JS_GETNOFILLVALS call has the following form:
			//		[ { value: 'Sales', fill: fillOb}, { value: etc...
			//
			var ar:Array=ExternalInterface.call(CommonUtilities.getFullFuncCall(JsFunctionConstant.JS_FUNC_NAME_GETNOFILLVALS)) as Array;
			if (ar != null) {
				_nofillValues={};

				var ob:Object=null;
				for (var i:int=0; i < ar.length; i++) {
					ob=ar[i];
					var fillOb:Object=ob.fill;
					var af:FillFormat=new FillFormat(ob.value);

					if (fillOb != null) {
						af.setFromDwgOptions(fillOb);
					} else {
						af.copy(fillFormat);
					}
					_nofillValues[ob.value]=af;
				}
			}
		}


		public function get keyDelimiter():String {
			return _keyDelimiter;
		}

		public function set keyDelimiter(value:String):void {
			_keyDelimiter=value;
		}

		/**
		 * Checks if no fill field or no field values exists. return true if either value exists, false otherwise.
		 * @return
		 */
		public function nofillsEnabled():Boolean {
			return (!_nofillField.length || _nofillValues == null) ? false : true;
		}

	}
}
