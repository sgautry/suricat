/*
	VersionControl

	Version control placed in contextmenu

	version 1 - (0.0.1)

	Original Creator by Maikel Sibbald
	info@flexcoders.nl
	http://labs.flexcoders.nl

	Free to use.... just give me some credit
*/

import flash.events.ContextMenuEvent;
import flash.net.URLRequest;
import flash.net.navigateToURL;
import flash.ui.ContextMenuItem;

import mx.resources.ResourceBundle;
import mx.resources.ResourceManager;


[ResourceBundle("version")]

/**
 * Defines the rresource bundle objects.
 */
private static var _versionRB:ResourceBundle;

/**
 * Adds the extra entries (such as company, app, etc.) to the ContextMenu list.
 *
 * @param unscaledWidth unscaled width
 * @param unscaledHeight unsclae height
 */
override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
	super.updateDisplayList(unscaledWidth, unscaledHeight);

	if (appItem == null) {
		var companyItem:ContextMenuItem=new ContextMenuItem(ResourceManager.getInstance().getString(_versionRB.bundleName, "company"), false, true);
		this.contextMenu.customItems.push(companyItem);
		companyItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, this.openWebsite);

		var appItem:ContextMenuItem=new ContextMenuItem(ResourceManager.getInstance().getString(_versionRB.bundleName, "app") + " (" + ResourceManager.getInstance().getString(_versionRB.bundleName, "vers") + ")", false, false);
		this.contextMenu.customItems.push(appItem);
	}
}

/**
 * Opens an URL link on an ContextMenu click.
 *
 * @param event a ContextMenu event
 */
private function openWebsite(event:ContextMenuEvent):void {
	navigateToURL(new URLRequest(ResourceManager.getInstance().getString(_versionRB.bundleName, "website")), "_blank");
}
