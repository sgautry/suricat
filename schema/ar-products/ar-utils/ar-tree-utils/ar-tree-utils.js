// ar-tree-utils.js v1.0 2014-01 ARE FK
//
// This utility class adds functionality to augment interaction with trees.
// It attaches itself to an arUtils object if it exists and creates it if needed.
//
// The corresponding css file is optional.  It replaces the - signs before expanded nodes.
//
// Version 1.0 adds:
// - expandWholeTree(treePanel)
// - expandNodeIterative(node)


var arUtils = arUtils || {};

arUtils.treeUtils = {
	expandWholeTree: function(treePanel) {
		for (var i = 0; i < treePanel['_nodes'].length; i++) {
			var node = treePanel['_nodes'][i];
			this.expandNodeIterative(node);
		}
	}, 
	expandNodeIterative: function(node, onComplete) {
		if (node.isLoading) {
			this.expandNodeIterative.defer(10, this, [node, onComplete]);
			return;
		}
		else if (!node.expanded) {
			node.expand();
			this.expandNodeIterative.defer(10, this, [node, onComplete]);
			return;
		}
		else if (node.children.length > 0) {
			for (var i = 0; i < node.children.length; i++) {
				this.expandNodeIterative(node.children[i]);
			}
		}
		else {
			//onComplete();
		}
	}, 
};
