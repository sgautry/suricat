var ctl = View.createController('ctl', {
	
	version             : '1.0',
	sessionId           : 0, 
	ruleIdUploadFile    : 'AbCommonResources-AremisImmosisDataTransferJob-uploadFile', 
	ruleIdImportAllFiles: 'AbCommonResources-AremisImmosisJob-importAllFiles', 
	ruleIdStartSession  : 'AbCommonResources-AremisImmosisJob-startSession', 
	ruleIdEndSession    : 'AbCommonResources-AremisImmosisJob-endSession', 
	ruleIdCancelSession : 'AbCommonResources-AremisImmosisJob-cancelSession', 
	
	afterViewLoad: function() {
		View.openProgressBar('');
		this.initializeDisplay();
	},

	afterInitialDataFetch : function(){
		this.pnlActiveSessions.refresh("immosis_session.status < '3_ENDED'");
		this.pnlInactiveSessions.refresh("immosis_session.status >= '3_ENDED'");
		if (this.pnlActiveSessions.rows.length == 0) {
			this.pnlTabs.selectTab('pnlTabNewSession');
		}
		View.closeProgressBar();
	}, 
	
	initializeDisplay: function() {
		// Conditional display of inputfile element
		if(navigator.appName.indexOf("Internet Explorer")>= 0) {
			$('btnUploadNewIe').style.display = 'block';
			$('btnUploadNewNotIe').style.display = 'none';
		}
		else {
			$('btnUploadNewIe').style.display = 'none';
			$('btnUploadNewNotIe').style.display = 'block';
			$('inLocalFileExtra').parentNode.style.display = 'none';
			$('inLocalFileExtra').parentNode.previousSibling.style.display = 'none';
		} 
		$('createNewSessionLabel1').innerHTML = getMessage("CREATE_NEW_SESSION_AND_UPLOAD");
		$('createNewSessionLabel2').innerHTML = getMessage("CREATE_NEW_SESSION_AND_UPLOAD");
	}, 
	
	selectFileToUpload: function() {
		this.sessionId = 0;
		$('inLocalFile').click();
	}, 
	
	fileToUploadChanged: function() {
		var fileName =  $('inLocalFile').value;
		if(fileName == "") {
			View.showMessage('error', getMessage("PROGRESS_ERROR_EMPTY_LOCALE_FILE"));
			return;
		}
		if (!/.*\.zip/.test(fileName)) {
			// Only zip files are allowed to be transferred in
			return false;
		}
		this.jobId = "";
		this.sessionId = 0;
		Workflow.startJobWithUpload(this.ruleIdUploadFile, $('inLocalFile'), this.afterDataTransferStarted, this, this.sessionId, fileName);
	},

	fileToUploadExtraChanged: function() {
		var fileName =  $('inLocalFileExtra').value;
		if(fileName == ""){
			View.showMessage('error', getMessage("PROGRESS_ERROR_EMPTY_LOCALE_FILE"));
			return;
		}
		this.jobId = "";
		this.sessionId = parseInt(this.pnlSession.getFieldValue('immosis_session.session_id'), 10);
		Workflow.startJobWithUpload(this.ruleIdUploadFile, $('inLocalFileExtra'), this.afterDataTransferStarted, this, this.sessionId, fileName);
	},

	afterDataTransferStarted: function(result) {
		this.jobId = result.message;
		if (this.jobId.indexOf('job_' == 0)) {
			View.openProgressBar('Uploading file')
			this.showProgress('Upload');
		}
	},

	importAllFiles: function(sessionId) {
		if (sessionId) {
			var restriction = 'session_id = ' + sessionId;
			this.pnlTabs.selectTab('pnlTabSession', restriction);
			this.pnlSession.refresh(restriction);
			this.pnlInFiles.refresh(restriction);
			this.pnlSession.show(true);
			this.pnlInFiles.show(true)
			this.pnlInData.show(false);
			try {
				this.jobId = Workflow.startJob(this.ruleIdImportAllFiles, parseInt(sessionId, 10));
				if (this.jobId.indexOf('job_' == 0)) {
					View.openProgressBar('Importing txt files')
					this.showProgress('ImportAllFiles');
				}
			}
			catch (e) {
				alert(e);
			}
		}		
	}, 

	pnlSession_afterRefresh: function() {
		var td = $('inLocalFileExtra').parentNode;
		if (this.isVisible('uploadExtraFileField')) {
			td.style.display = '';
			td.previousSibling.style.display = '';
		}
		else {
			td.style.display = 'none';
			td.previousSibling.style.display = 'none';
		}
	}, 

	pnlSession_onUploadExtraFile: function() {
		this.sessionId = parseInt(this.pnlSession.getFieldValue('immosis_session.session_id'), 10);
		$('inLocalFileExtra').click();
	}, 
	
	pnlSession_onStartSession: function() {
		this.sessionId = parseInt(this.pnlSession.getFieldValue('immosis_session.session_id'), 10);
		var hasConfirmed = false;
		if (arguments.length > 0 && typeof arguments[0] == 'boolean') {
			hasConfirmed = arguments[0];
		}
		var existsAnActiveSessionForSameFloor = false;
		var blId = this.pnlSession.getFieldValue('immosis_session.bl_id');
		var flId = this.pnlSession.getFieldValue('immosis_session.fl_id');
		var restriction = new Ab.view.Restriction();
		restriction.addClause('immosis_session.bl_id', blId, "=");
		restriction.addClause('immosis_session.fl_id', flId, "=");
		restriction.addClause('immosis_session.status', '2_STARTED', "=");
		var records = this.dsSessions.getRecords(restriction);
		if (records.length > 0 && !hasConfirmed) {
			View.confirm(getMessage("ACTIVE_SESSION_FOR_SAME_FLOOR"), function(button) {
    		if (button == 'yes') {
    			ctl.pnlSession_onStartSession(true);
    		}
			});
			return;
		}
		try {
			this.jobId = Workflow.startJob(this.ruleIdStartSession, this.sessionId, hasConfirmed);
			if (this.jobId.indexOf('job_' == 0)) {
				//View.openProgressBar('Starting the session');
				View.openProgressBar('PRENDRE');
				this.showProgress('StartSession');
			}
			else {
				View.alert('Something went wrong while starting the session');
			}
		}
		catch (e) {
			alert(e);
		}
	}, 
	
	pnlSession_onEndSession: function() {
		this.sessionId = parseInt(this.pnlSession.getFieldValue('immosis_session.session_id'), 10);
		try {
			this.jobId = Workflow.startJob(this.ruleIdEndSession, this.sessionId);
			if (this.jobId.indexOf('job_' == 0)) {
				//View.openProgressBar('Ending the session');
				View.openProgressBar('RENDRE');
				this.showProgress('EndSession');
			}
		}
		catch (e) {
			alert(e);
		}
	}, 
	
	pnlSession_onCancelSession: function() {
		this.sessionId = parseInt(this.pnlSession.getFieldValue('immosis_session.session_id'), 10);
		try {
			this.jobId = Workflow.startJob(this.ruleIdCancelSession, this.sessionId);
			if (this.jobId.indexOf('job_' == 0)) {
				//View.openProgressBar('Canceling session ' + this.sessionId.toString());
				View.openProgressBar('LIBERER');
				this.showProgress('CancelSession');
			}
		}
		catch (e) {
			alert(e);
		}
	}, 
		
	pnlSession_onDownloadZip: function() {
		this.sessionId = parseInt(this.pnlSession.getFieldValue('immosis_session.session_id'), 10);
		var fileName = 'upload_to_immosis_' + this.pnlSession.getFieldValue('bl.site_id') + '_' + this.pnlSession.getFieldValue('bl.bap_code') + '_' + this.pnlSession.getFieldValue('immosis_session.fl_id') + '.zip';
		var downloadUrl = '/archibus/projects/immosis/sessions/' + this.sessionId.toString() + '/out/' + fileName;
		window.open(downloadUrl, 'Downloading');
	}, 
	
	showProgress: function(progressJobName) {
		if (progressJobName) this.progressJobName = progressJobName;
		var result = Workflow.getJobStatus(this.jobId);
		var jobHasCompleted = (result.jobStatusCode >= 3);
		var jobHasFailed = (result.jobStatusCode == 8);
		if (!jobHasCompleted) {
			var message = result.jobMessage;
			if (message != "" && message != this.progressMessage) {
				this.progressMessage = message;
				View.progressWindow.setTitle(this.progressMessage);
				if (this.progressJobName == 'ImportAllFiles') {
					this.pnlSession.refresh();
					this.pnlInFiles.refresh();
				}
			}
			this.showProgress.defer(1000, this);
		}
		else {
			if (jobHasFailed) {
				var errorMessage = result.jobMessage;
				var removeRecurringPrefix = "";
				if (errorMessage.indexOf(": ") >= 0) removeRecurringPrefix = errorMessage.substring(0, errorMessage.indexOf(": ") + 2);
				if (removeRecurringPrefix != '') {
					var newErrorMessage = errorMessage;
					errorMessage = "";
					while (newErrorMessage != errorMessage) {
						errorMessage = newErrorMessage;
						newErrorMessage = newErrorMessage.replace(removeRecurringPrefix, '');
					}
				}
				this.pnlSession.refresh();
				this.pnlInFiles.refresh();
				View.closeProgressBar();
				alert(errorMessage);
			}
			else {
				if (this.progressJobName == 'ImportAllFiles') {
					this.pnlSession.refresh();
					this.pnlInFiles.refresh();
					this.pnlSession_onStartSession();
				}
				else if (/StartSession|EndSession|CancelSession/.test(this.progressJobName)) {
					this.pnlSession.refresh();
				}
				View.closeProgressBar();
				if (this.progressJobName == 'Upload') {
					this.sessionId = parseInt(result.jobProperties['sessionId'], 10);
					this.importAllFiles(this.sessionId);
				}
				else if (this.progressJobName == 'EndSession') {
					this.pnlSession_onDownloadZip();
				}
				/*
				else if (this.progressJobName == 'StartSession') {
					var blId = this.pnlSession.getFieldValue('immosis_session.bl_id');
					var flId = this.pnlSession.getFieldValue('immosis_session.fl_id');
					var restriction = {'rm.bl_id': blId, 'rm.fl_id': flId };
					View.openDialog('sncf-rm.axvw', restriction, false, {
					    width: 800,
					    height: 600,
					    closeButton: true,
					    maximize: true,
					    afterViewLoad: function(dialogView) {
					    },
					    callback: function(res) {
					    }
					});
				}
				*/
			}
		}
	},
	
	isVisible: function (buttonId) {
		var status = this.pnlSession.getFieldValue('immosis_session.status');
		if (buttonId == 'uploadExtraFile') {
			return (status == '1_UPLOADING' && navigator.appName.indexOf("Internet Explorer")< 0);
		}
		if (buttonId == 'uploadExtraFileField') {
			return (status == '1_UPLOADING' && navigator.appName.indexOf("Internet Explorer")>= 0);
		}
		if (buttonId == 'startSession') {
			return (status == '1_UPLOADING');
		}
		if (buttonId == 'endSession') {
			return (status == '2_STARTED');
		}
		if (buttonId == 'cancelSession') {
			return (status == '1_UPLOADING' || status == '2_STARTED');
		}
		if (buttonId == 'downloadZip') {
			return (status == '3_ENDED');
		}
		return true;
	}

});
