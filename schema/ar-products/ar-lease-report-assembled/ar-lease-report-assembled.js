
var arLeaseReportAssembled = View.createController('arLeaseReportAssembled', {
    afterViewLoad: function(){
        console.log("test de controller");
    },
    detail_contract_onClear:function(){
    	console.log('"clearin g"');
    	var detailContract = this.view.panels.get('detail_contract');
    	detailContract.clear();
    	detailContract.show(false);
    },
    UTChoice_onClear:function(){

    	var uTChoice = this.view.panels.get('UTChoice');
    	var instantT = this.view.panels.get('instant_t_grid');
    	var detailContract = this.view.panels.get('detail_contract');
    	uTChoice.clear();
    	instantT.clear();
    	instantT.show(false);
    	detailContract.clear();
    	detailContract.show(false);
		document.getElementById('UTChoice_ar_uic.uic_id_labelCell').innerHTML  ='Code UIC';
    },
    UTChoice_onShow:function(){ 
		var restrictions = getRestriction();
		if(restrictions.error){
			alert(restrictions.message);
		}else{
			this.instant_t_grid_ShowData();
		}
    },
    instant_t_grid_afterRefresh:function(){
    	console.log("j'ai refresh ");
    },
    instant_t_grid_beforeRefresh:function(){
    	console.log("j'ai  before refresh ");

    },
    instant_t_grid_ShowData:function(){
    	console.log("instant_t_grid_ShowData");
    	var detailContract = this.view.panels.get('detail_contract');
 	   	var 	instantT = this.view.panels.get('instant_t_grid');
 	   	// CUSTOM LOADER ----
    	showHtmlLoader();
    	setTimeout(function(){
		var restrictions = getRestriction();
   		instantT.addParameter('dataRestriction',restrictions.message);
   		instantT.addParameter('dataGroupBy',restrictions.groupby);
   		instantT.setCategoryConfiguration({
	    	    		fieldName: 'ar_lr_cat.cat',
	    	    		order: ['1 - INTERNE','2 - EF','3 - PRESTATAIRES','4 - AUTORITE DE SURETE','5 - PRESTATION DE BASE','6 - COMMERCES RC','7 - NON RAPPROCHES',
	    	    				'8 - SURFACES MUTUALISEES','9 - EQUIPEMENTS','10 - SURFACES NON-BAILLEES','11 - SURFACES PROJETS',
	    	    		        '12 - SURFACES TRANSMANCHES','13 - PARKINGS','14 - TERRAINS']
	    	    	});
   		instantT.refresh();
   		// On remove le loader ------
   		//removeHtmlLoader();
   		instantT.setTitle(getSiteUICName());
   		detailContract.clear();
   		detailContract.show(false);
   		setRowColor(null);
	    hideSomeData();
   		removeHtmlLoader();
        }, 100);
    },
	instant_t_grid_onClickItem: function(row){
		// CUSTOM LOADER ----
    	showHtmlLoader();
    	var me = this;
    	setTimeout(function(){
        // Retrieve value of the clicked row. ---------------------------------------------------------------
		var values = row.getRecord().values;
		var title = "";
		var where='';
		// Retrieve values of filter ------------------------------------------------------------------------
	    var uic_id 		=	me.view.panels.get('UTChoice').getFieldMultipleValues('ar_uic.uic_id')[0];
	    var site_id 	=	me.view.panels.get('UTChoice').getFieldMultipleValues('ar_site_uic.site_id')[0]; 
	    
	    // Generate title of section and clause -------------------------------------------------------------
		if(values['ar_tableau_instant_t.cont_id'] =='SANS CONTRAT'){
			where+=" ar_tableau_bail_pv.cont_id = 'SANS CONTRAT'  AND ";
			title ="Detail des SANS CONTRAT de la catégorie "+values['ar_lr_cat.cat'];
		}else if(values['ar_tableau_instant_t.cont_id'] !='0_CONSOLIDATION'){
			where+=" ar_tableau_bail_pv.cont_id LIKE '" + values['ar_tableau_instant_t.cont_id'] + "' AND ";
			title ="Detail pour le contrat "+values['ar_tableau_instant_t.cont_id'];	
			
		}else{
			title ="Detail de la catégorie "+values['ar_lr_cat.cat'];
		}
		
		// Finalize clause where -----------------------------------------------------------------------------
		where+="ar_tableau_bail_pv.cat LIKE '"+values['ar_lr_cat.cat']+"' AND ";

	    if(uic_id.length >0){
	    	where+=" uic_id ='"+uic_id+"'";
	    }else{
	    	where+=" site_id ='"+site_id+"'";
	    }
	    setRowColor(values);
		// Apply all change needed
	    me.detail_contract.addParameter('dataRestriction',where);
	    me.detail_contract.show();
	    me.detail_contract.refresh();
	    me.detail_contract.setTitle(title);

   		removeHtmlLoader();
        }, 100);
    }
});
function hideSomeData(){

	var user  = View.user;
	var membervalo = user.isMemberOfGroup('SNCF-GC-VALO-VISIBILITY');
	if(!membervalo){
		var elems =	View.panels.get('instant_t_grid').gridRows;
		elems.each(function(row){
			if(row.getRecord().values['ar_lr_cat.cat'] != '1 - INTERNE'){
				for(var i=9;i<=15;i++){
					row.cells.items[i].dom.innerText='-';
					row.cells.items[i].dom.innerHTML = '-';
				}
					
				
			}
		});
	}
	//debugger;
}
function setRowColor(values){
	var instant_t_grid = 	View.panels.get('instant_t_grid');
	if(values !=null){
		instant_t_grid.gridRows.each(function(row){
			if(	row.getRecord().values['ar_tableau_instant_t.cont_id'] == values['ar_tableau_instant_t.cont_id'] && 
				row.getRecord().values['ar_lr_cat.cat'] == values['ar_lr_cat.cat'])
			{
				row.cells.each(function(cell){
					Ext.get(cell.dom).setStyle('background-color','#FFEAC6');
				});
		    }else if(row.getRecord().values['ar_tableau_instant_t.cont_id'] == '0_CONSOLIDATION'){
				row.cells.each(function(cell){
					Ext.get(cell.dom).setStyle('background-color','#D9E3F1');
					Ext.get(cell.dom).setStyle('font-weight','bold');
				});
		    }else{ 
				row.cells.each(function(cell){
					Ext.get(cell.dom).setStyle('background-color','rgba(243,247,250,0)');
				});
		    }
		});
	}else{
		instant_t_grid.gridRows.each(function(row){
			 if(row.getRecord().values['ar_tableau_instant_t.cont_id'] == '0_CONSOLIDATION'){
				row.cells.each(function(cell){
					Ext.get(cell.dom).setStyle('background-color','#D9E3F1');
					Ext.get(cell.dom).setStyle('font-weight','bold');
				});
		    }
		});
	}
}
function code_ut_change(command){
	 var site_id 	=	View.panels.get('UTChoice').getFieldMultipleValues('ar_site_uic.site_id')[0]; 
	 if(site_id.length >0){
			var dsLease = new Ab.data.createDataSourceForFields({
				   id: 'fake_data',
				   tableNames: ['ar_site_uic'],
				   fieldNames: ['ar_site_uic.site_id', 'ar_site_uic.uic_id']
				});
				var record = dsLease.getRecords("site_id = '"+site_id+"'");

		if(record.length> 1){
			document.getElementById('UTChoice_ar_uic.uic_id_labelCell').innerHTML  ='Code UIC ('+record.length+')';
		}else{
			document.getElementById('UTChoice_ar_uic.uic_id_labelCell').innerHTML  ='Code UIC';
		}
	 }
}
//UTChoice_ar_site_uic.site_id
//UTChoice_ar_site_uic.site_name

function code_uic_change(command){
	console.log("CODE UIC CHANGE");
	 var uic_id 	=	View.panels.get('UTChoice').getFieldMultipleValues('ar_uic.uic_id')[0]; 
	 if(uic_id.length >0){
			var dsLease = new Ab.data.createDataSourceForFields({
				   id: 'fake_data',
				   tableNames: ['ar_site_uic'],
				   fieldNames: ['ar_site_uic.site_id','ar_site_uic.site_name', 'ar_site_uic.uic_id']
				});
				var record = dsLease.getRecord("uic_id = '"+uic_id+"'");
		if(record.getValues().hasOwnProperty('ar_site_uic.site_id')){ 
			var site_id=record.getValue('ar_site_uic.site_id');
			var site_name =record.getValue('ar_site_uic.site_name');
			if(site_name.length >0){
				document.getElementById('UTChoice_ar_site_uic.site_id').value = site_id;
				code_ut_change(null);
			}else{
				document.getElementById('UTChoice_ar_site_uic.site_id').value = '';
			}
		}else{
			document.getElementById('UTChoice_ar_site_uic.site_id').value = '';
		}
	 }
}
function beforeSelectUic(command) {
    var site_id 	=	View.panels.get('UTChoice').getFieldMultipleValues('ar_site_uic.site_id')[0]; 
    if(site_id.length >0){
    command.dialogRestriction = "ar_uic.uic_id IN (SELECT uic_id FROM ar_site_uic WHERE ar_site_uic.site_id ='"+site_id+"')";
    }else{
        command.dialogRestriction = "";
    }
}

function beforeSelectUT(command){
	 var uic_id 	=	View.panels.get('UTChoice').getFieldMultipleValues('ar_uic.uic_id')[0]; 
    if(uic_id.length >0){
    command.dialogRestriction = " ar_site_uic.uic_id ='"+uic_id+"'";
    }else{
        command.dialogRestriction = "";
    }
}

function getSiteUICName(){
	var filterPanel = 	View.panels.get('UTChoice');
    var uic_name	=	filterPanel.getFieldMultipleValues('ar_uic.uic_name')[0];
    var site_name	=	filterPanel.getFieldMultipleValues('ar_site_uic.site_name')[0];

    if(uic_name.length >0){
    	return site_name+" - "+uic_name;
    }else{
    	return site_name;
    }
}
function getRestriction(){
	
	var filterPanel = 	View.panels.get('UTChoice');
    var uic_id 		=	filterPanel.getFieldMultipleValues('ar_uic.uic_id')[0];
    var site_id 	=	filterPanel.getFieldMultipleValues('ar_site_uic.site_id')[0]; 
    var restriction = 	[];
    var groupby ='';
    var retour = {error:0,message:''};
    	if(uic_id.length >0 || site_id.length >0){
    		if(uic_id.length >0){
            	restriction.push(" uic_id ='"+uic_id+"'");
            	groupby =',site_id,uic_id';
            }else{

            	groupby =',site_id';
            }
            if(site_id.length >0){
            	restriction.push(" site_id ='"+site_id+"'");
            }
            retour = {error:0,message: restriction.join(' AND '),groupby:groupby};
    	}else{
    		retour = {error:1, message:'Vous devez choisir un code UT et/ou un code UIC'};
    	}
    return retour;
}

function showHtmlLoader() {
	var viewWindow = (window.frameElement)? window.parent : window;
		  var xhttp = new XMLHttpRequest();
		  xhttp.open("GET", "schema/ar-products/common/resources/loader/loader.html");
		  xhttp.onload = function() {
		    	var frag = document.createRange().createContextualFragment(xhttp.response);
		    	viewWindow.document.body.appendChild(frag);
		  };
		  xhttp.send();
}

function removeHtmlLoader(){
	var viewWindow = (window.frameElement)? window.parent : window;
	var elem = viewWindow.document.getElementById("customloader");
	elem.parentNode.removeChild(elem);
}