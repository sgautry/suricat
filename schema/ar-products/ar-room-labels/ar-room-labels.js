﻿var ctl = View.createController('ctl', {
	selectedNodes: [], 
	rooms: {'keys': [], 'map': {}}, 
	afterViewLoad: function() {
		var tree = this.pnlTree;
		tree.setMultipleSelectionEnabled(0);
		tree.setMultipleSelectionEnabled(1);
		tree.setMultipleSelectionEnabled(2);
		tree.setMultipleSelectionEnabled(3);
		tree.addEventListener('onChangeMultipleSelection', this.pnlTree_onChangeMultipleSelection.createDelegate(this));
	}, 
	afterInitialDataFetch: function() {
	}, 
	attachIframeHandler: function() {
		ctl.frameDoc = document.getElementById('ifrRoomLabels').contentDocument || document.getElementById('ifrRoomLabels').contentWindow.document;
		var frameBodyEl = Ext.get(ctl.frameDoc.body);
		frameBodyEl.on('click', ctl.onLabelClick, ctl);
	}, 
	pnlConsole_onBlIdSelectValueAction: function() {
		var sqlRestriction = '';
		var siteId = this.pnlConsole.getFieldValue('bl.site_id') || '';
		if (siteId) {
			sqlRestriction = "bl.site_id = '" + siteId + "'";
		}	
    View.selectValue('pnlConsole', '', ['rm.bl_id'], 'bl', ['bl.bl_id'], ['bl.site_id','bl.bl_id','bl.name'], sqlRestriction);		
	}, 
	pnlConsole_onFilter: function() {
		try {
			var p = {
				'siteId'       : this.pnlConsole.getFieldValue('bl.site_id') || '', 
				'siteName'     : this.pnlConsole.getFieldValue('site.name')  || '', 
				'blId'         : this.pnlConsole.getFieldValue('rm.bl_id')   || '', 
				'blName'       : this.pnlConsole.getFieldValue('bl.name')    || '', 
				'flId'         : this.pnlConsole.getFieldValue('rm.fl_id')   || '', 
				'flName'       : this.pnlConsole.getFieldValue('fl.name')    || '', 
				'rmId'         : this.pnlConsole.getFieldValue('rm.rm_id')   || '', 
				'rmName'       : this.pnlConsole.getFieldValue('rm.name')    || '', 
				'hasEmployees' : (($('chkHasEmployees').checked == true) ? 'Y' : 'N')
			}
			this.pnlTree.addParameter('pSiteId'      , p.siteId       );
			this.pnlTree.addParameter('pSiteName'    , p.siteName     );
			this.pnlTree.addParameter('pBlId'        , p.blId         );
			this.pnlTree.addParameter('pBlName'      , p.blName       );
			this.pnlTree.addParameter('pFlId'        , p.flId         );
			this.pnlTree.addParameter('pFlName'      , p.flName       );
			this.pnlTree.addParameter('pRmId'        , p.rmId         );
			this.pnlTree.addParameter('pRmName'      , p.rmName       );
			this.pnlTree.addParameter('pHasEmployees', p.hasEmployees);
			this.pnlTree.show();
			this.pnlTree.refresh();
			return true;
		}
		catch (exception) {
			View.alert(exception);
			return false;
		}
	}, 
	pnlTree_onChangeMultipleSelection: function(node) {
		if (node.isSelected()) {
			if (node.level.levelIndex < 3) {
				arUtils.treeUtils.expandNodeIterative(node);
					this.detectSelectedNodes.defer(100, this, []);
					return;
			}
		} 
		this.detectSelectedNodes();
	},
	detectSelectedNodes: function() {
		// Keep on detecting the selected nodes as long as it takes
		var nodes = this.pnlTree.getSelectedNodes(3);
		if (nodes.length != this.selectedNodes.length) {
				this.selectedNodes = nodes;
				this.detectSelectedNodes.defer(500, this, []);
				return;
		}
		// Sort the selected nodes and store them
		nodes.sort(this.nodeSortFunction)
		this.selectedNodes = nodes;
		// Show the labels for the selected nodes
		this.buildLabelsForSelectedRooms();
	}, 
	nodeSortFunction: function(node1, node2) {
		var n1 = node1.data;
		var n2 = node2.data;
		var comparison = 
			n1['bl.site_id'] < n2['bl.site_id'] ? -1 : (
			n1['bl.site_id'] > n2['bl.site_id'] ?  1 : (
			n1['rm.bl_id']   < n2['rm.bl_id']   ? -1 : (
			n1['rm.bl_id']   > n2['rm.bl_id']   ?  1 : (
			n1['rm.fl_id']   < n2['rm.fl_id']   ? -1 : (
			n1['rm.fl_id']   > n2['rm.fl_id']   ?  1 : (
			n1['rm.rm_id']   < n2['rm.rm_id']   ? -1 : (
			n1['rm.rm_id']   > n2['rm.rm_id']   ?  1 : 0)))))));
		return comparison;
	}, 
	buildLabelsForSelectedRooms: function() {
		var rooms = {'keys': [], 'map': {}};
		var roomsToRemove = [];
		// Build list of rooms from the selectedNodes
		for (var i = 0; i < this.selectedNodes.length; i++) {
			var node = this.selectedNodes[i];
			var key = node.data['bl.site_id'] + '|' + node.data['rm.bl_id'] + '|' + node.data['rm.fl_id'] + '|' + node.data['rm.rm_id'];
			rooms.keys.push(key);
			var room = {
				'key'    : key, 
				'siteId' : node.data['bl.site_id'], 
				'blId'   : node.data['rm.bl_id'], 
				'flId'   : node.data['rm.fl_id'], 
				'rmId'   : node.data['rm.rm_id'], 
				'dvId'   : node.data['rm.dv_id'], 
				'dpId'   : node.data['rm.dp_id'], 
				'dvName' : node.data['dv.name'], 
				'dpName' : node.data['dp.name'], 
				'data'   : {  
					'firstLine'  : '', 
					'secondLine' : '', 
					'employees'  : {}, 
					'domIds'     : [] 
				}
			};
			room.data.firstLine = room.rmId || ' ';
			room.data.secondLine = room.dvId || ' ';
			rooms.map[key] = room;
		}
		// Copy over employee info from previously loaded list of rooms and mark deselected rooms as 'to be removed'
		if (this.rooms.keys.length > 0) {
			function copyRoomInfo(oldRoom, newRoom) {
				newRoom.data = oldRoom.data;
			}
			for (var i = 0; i < this.rooms.keys.length; i++) {
				var key = this.rooms.keys[i];
				var oldRoom = this.rooms.map[key];
				var newRoom = rooms.map[key];
				if (newRoom) {
					copyRoomInfo(oldRoom, newRoom);
				}
				else {
					roomsToRemove.push(key);
				}
			}
		}
		// Remove all labels for rooms that are no longer selected
		for (var i = 0; i < roomsToRemove.length; i++) {
			var key = roomsToRemove[i];
			this.hideLabelForRoom(key);
		}
		this.rooms = rooms;
		// Show labels for rooms that are selected
		var insertAfterDomId = 'manualLabelPage';
		for (var i = 0; i < this.rooms.keys.length; i++) {
			var key = this.rooms.keys[i];
			insertAfterDomId = this.showLabelForRoom(key, insertAfterDomId);
		}
	},	
	hideLabelForRoom: function(key) {
		var room = this.rooms.map[key];
		if (room.data.domIds && room.data.domIds.length > 0) {
			for (var i = 0; i < room.data.domIds.length; i++) {
				var el = this.frameDoc.getElementById(room.data.domIds[i]);
				el.parentNode.removeChild(el);
			}
		}
	}, 
	showLabelForRoom: function(key, insertAfterDomId) {
		var room = this.rooms.map[key];
		if (!room.data.employees.loaded) this.loadEmployeesForRoom(room);
		insertAfterDomId = this.renderRoom(room, insertAfterDomId);
		return insertAfterDomId;
	}, 
	pnlSignage_onPrint: function() {
		window.frames["ifrRoomLabels"].focus();
		window.frames["ifrRoomLabels"].print();
	}, 
	loadEmployeesForRoom: function(room) {
		room.data.employees = {
			'loaded': true, 
			'list'  : []
		}
		var restriction = new Ab.view.Restriction();
		restriction.addClause('em.bl_id', room.blId, "=", 'AND');
		restriction.addClause('em.fl_id', room.flId, "=", 'AND');
		restriction.addClause('em.rm_id', room.rmId, "=", ')AND(');
		restriction.addClause('em.rm_id', room.rmId + '._%', "LIKE", 'OR');
		var records = this.dsEmployees.getRecords(restriction);
		for (var i = 0; i < records.length; i++) {
			var record = records[i];
			var employee = {
				'emId'      : record.values['em.em_id'], 
				'nameFirst' : record.values['em.name_first'], 
				'nameLast'  : record.values['em.name_last'], 
				'company'   : record.values['em.option1'], 
				'data'      : {
					'field1'    : '', 
					'field2'    : '', 
					'field3'    : ''
				}
			}
			employee.data.field1 = employee.nameFirst || ' ';
			employee.data.field2 = employee.nameLast;
			employee.data.field3 = (employee.company == '') ? '' : '(' + employee.company + ')';
			room.data.employees.list.push(employee)
		}
		for (var i = room.data.employees.list.length; i < 6; i++) {
			var employee = {
				'emId'      : '', 
				'nameFirst' : '', 
				'nameLast'  : '', 
				'company'   : '', 
				'data'      : {
					'field1'    : '', 
					'field2'    : '', 
					'field3'    : ''
				}
			}
			employee.data.field1 = employee.nameFirst || ' ';
			employee.data.field2 = employee.nameLast;
			employee.data.field3 = (employee.company == '') ? '' : '(' + employee.company + ')';
			room.data.employees.list.push(employee)
		}
	}, 
	renderRoom: function(room, insertAfterDomId) {
		var roomsList = Ext.get(this.frameDoc.body);
		if (room.data.domIds && room.data.domIds.length > 0) {
			// Retrieve the id of the preceding label
			if (!insertAfterDomId) {
				var el = Ext.get(this.frameDoc.getElementById(room.data.domIds[0]));
				if (el && el.prev()) {
					insertAfterDomId = el.prev().id; 
				}
			}
			// Remove all existing labels for this room
			for (var i = 0; i < room.data.domIds.length; i++) {
				var el = this.frameDoc.getElementById(room.data.domIds[i]);
				if (el && el.parentNode) el.parentNode.removeChild(el);
			}
		}
		room.data.domIds = [];
		var useNonBreakingSpace = true;
		var labelNr = 1;
		var key = room.key + '|label' + labelNr.toString();
		room.data.domIds.push(key);
		var roomHTML = '<div class="page" id="' + this.htmlEncode(key) + '">';
		roomHTML += '<div class="label">';
		roomHTML += '<div class="firstLine">' + this.htmlEncode(room.data.firstLine, useNonBreakingSpace) + '</div>';
		roomHTML += '<div class="secondLine">' + this.htmlEncode(room.data.secondLine, useNonBreakingSpace) + '</div>';
		for (var i = 0; i < room.data.employees.list.length; i++) {
			var employee = room.data.employees.list[i];
			roomHTML += '<div class="compositeLine" name="line' + (i + 1).toString() + '"><span class="field1">' + this.htmlEncode(employee.data.field1, useNonBreakingSpace) + '</span> <span class="field2">' + this.htmlEncode(employee.data.field2) + '</span> <span class="field3">' + this.htmlEncode(employee.data.field3) + '</span></div>';
		}
		roomHTML += '<img class="gradient" src="ar-room-labels-line.png" />';
		roomHTML += '<div class="verticalMark left top"></div>';
		roomHTML += '<div class="verticalMark right top"></div>';
		roomHTML += '<div class="horizontalMark left top"></div>';
		roomHTML += '<div class="horizontalMark right top"></div>';
		roomHTML += '<div class="horizontalMark left bottom"></div>';
		roomHTML += '<div class="horizontalMark right bottom"></div>';
		roomHTML += '<div class="verticalMark left bottom"></div>';
		roomHTML += '<div class="verticalMark right bottom"></div>';
		roomHTML += '</div>';
		roomHTML += '</div>';
		if (insertAfterDomId) {
			var pageEl = Ext.get(this.frameDoc.getElementById(insertAfterDomId)).insertHtml('afterEnd', roomHTML, true);
		}
		else {
			var pageEl = roomsList.insertHtml('beforeEnd', roomHTML, true);
		}
		// Check if all composite lines fit on the label
		var allLinesFitOnLabel = false;
		while (!allLinesFitOnLabel) {
			var imgEl = pageEl.select('img.gradient').first();
			var lineEls = pageEl.select('div.compositeLine');
			var imgTop = imgEl.getTop();
			var lastLineEl = lineEls.last();
			var lastBottom = lastLineEl.getBottom();
			var roomHTML = '';
			// Remove trailing blanco lines if needed
			while (lastBottom >= imgTop) {
				if (lastLineEl.dom.innerHTML == '<span class="field1">&nbsp;</span> <span class="field2"></span> <span class="field3"></span>') {
					lineEls.removeElement(lastLineEl, true);
					lastLineEl = lineEls.last();
					lastBottom = lastLineEl.getBottom();
				}
				else {
					break;
				}
			}
			// Create a new page and move all non-fitting lines to it
			if (lastBottom >= imgTop) {
				labelNr += 1;
				insertAfterDomId = key;
				key = room.key + '|label' + labelNr.toString();
				room.data.domIds.push(key);
				roomHTML = '<div class="page" id="' + this.htmlEncode(key) + '">';
				roomHTML += '<div class="label">';
				roomHTML += '<div class="firstLine">' + this.htmlEncode(room.data.firstLine, useNonBreakingSpace) + '</div>';
				roomHTML += '<div class="secondLine">' + this.htmlEncode(room.data.secondLine, useNonBreakingSpace) + '</div>';
				// Move all non-fitting lines to the new page
				lineEls.each(function(el, c, idx) {
					if (el.getBottom() >= imgTop) {
						roomHTML += el.dom.outerHTML;
						el.dom.parentNode.removeChild(el.dom);
					}
				});
				/*
				// Remove all non-fitting lines from the dom
				for (var i = lineEls.elements.length; i >= 0; i--) {
					var el = lineEls.elements[i];
					if (el.getBottom() >= imgTop) {
						lineEls.removeElement(el, true);
					}
					else {
						break;
					}
				}
				*/
				roomHTML += '<img class="gradient" src="ar-room-labels-line.png" />';
				roomHTML += '<div class="verticalMark left top"></div>';
				roomHTML += '<div class="verticalMark right top"></div>';
				roomHTML += '<div class="horizontalMark left top"></div>';
				roomHTML += '<div class="horizontalMark right top"></div>';
				roomHTML += '<div class="horizontalMark left bottom"></div>';
				roomHTML += '<div class="horizontalMark right bottom"></div>';
				roomHTML += '<div class="verticalMark left bottom"></div>';
				roomHTML += '<div class="verticalMark right bottom"></div>';
				roomHTML += '</div>';
				roomHTML += '</div>';
				pageEl = Ext.get(this.frameDoc.getElementById(insertAfterDomId)).insertHtml('afterEnd', roomHTML, true);
			}
			else {
				allLinesFitOnLabel = true;
			}
		}
		return key;
	}, 
	onLabelClick: function(evt, domElement, opt) {
		var el = Ext.get(domElement);
		var domId = ctl.detectDomId(el);
		if (domId) {
			// Get the room
			var keys = domId.split('|');
			var labelNr = keys.pop().substr(5);
			var key = keys.join('|');
			var room = this.rooms.map[key];
			// Detect what has been clicked
			var change = {
				changeType      : '', 
				compositeLineNr : 0, 
				compositeField  : ''
			}
			var nodeName = domElement.nodeName;
			if (nodeName == 'DIV') {
				if (el.hasClass('firstLine')) {
					change.changeType = 'firstLine';
				}
				else if (el.hasClass('secondLine')) {
					change.changeType = 'secondLine';
				}
				else if (el.hasClass('compositeLine')) {
					change.changeType = 'compositeLine';
					change.compositeLineNr = parseInt(el.dom.getAttribute('name').substr(4), 10);
				}
			}
			else if (nodeName == 'SPAN') {
				if (el.hasClass('field1')) {
					change.changeType = 'compositeLine';
					change.compositeField = 'field1';
				}
				else if (el.hasClass('field2')) {
					change.changeType = 'compositeLine';
					change.compositeField = 'field2';
				}
				else if (el.hasClass('field3')) {
					change.changeType = 'compositeLine';
					change.compositeField = 'field3';
				}
				if (change.changeType != '') {
					change.compositeLineNr = parseInt(el.up('div.compositeLine').dom.getAttribute('name').substr(4), 10);
				}
			}
			if (change.changeType == 'firstLine') {
				ctl.pnlFirstLine.setFieldValue('firstLine', room.data.firstLine.trim());
				ctl.pnlFirstLine.data = {
					room: room 
				}
				var w = 250;
				var h = 130;
				ctl.pnlFirstLine.showInWindow({
					x: (window.innerWidth - w) / 2, 
					y: (window.innerHeight - h) / 2,
					width: w,
					height: h,
					title: getMessage('changeFirstLine') 
				});
			}
			else if (change.changeType == 'secondLine') {
				ctl.pnlSecondLine.setFieldValue('secondLine', room.data.secondLine.trim());
				ctl.pnlSecondLine.data = {
					room: room 
				}
				var w = 250;
				var h = 130;
				ctl.pnlSecondLine.showInWindow({
					x: (window.innerWidth - w) / 2, 
					y: (window.innerHeight - h) / 2,
					width: w,
					height: h,
					title: getMessage('changeSecondLine') 
				});
			}
			else if (change.changeType == 'compositeLine') {
				var employee = room.data.employees.list[change.compositeLineNr - 1];
				ctl.pnlCompositeLine.setFieldValue('field1', employee.data.field1.trim());
				ctl.pnlCompositeLine.setFieldValue('field2', employee.data.field2.trim());
				ctl.pnlCompositeLine.setFieldValue('field3', employee.data.field3.trim());
				ctl.pnlCompositeLine.data = {
					room: room, 
					employee: employee
				}
				var w = 700;
				var h = 130;
				ctl.pnlCompositeLine.showInWindow({
					x: (window.innerWidth - w) / 2, 
					y: (window.innerHeight - h) / 2,
					width: w,
					height: h,
					title: getMessage('changeCompositeLine') + ' ' + change.compositeLineNr.toString()
				});
			}
		}
	}, 
	detectDomId: function(el) {
		var page = el.up('div.page');
		if (page) {
			return page.id;
		}
		else {
			return;
		}
	}, 
	pnlFirstLine_onPnlFirstLineConfirmChanges: function() {
		var room = this.pnlFirstLine.data.room;
		room.data.firstLine = this.pnlFirstLine.getFieldValue('firstLine') || ' ';
		this.renderRoom(room);
		this.pnlFirstLine.closeWindow();
	}, 
	pnlSecondLine_onPnlSecondLineConfirmChanges: function() {
		var room = this.pnlSecondLine.data.room;
		room.data.secondLine = this.pnlSecondLine.getFieldValue('secondLine') || ' ';
		this.renderRoom(room);
		this.pnlSecondLine.closeWindow();
	}, 
	pnlCompositeLine_onPnlCompositeLineConfirmChanges: function() {
		var employee = this.pnlCompositeLine.data.employee;
		employee.data.field1 = this.pnlCompositeLine.getFieldValue('field1') || ' ';
		employee.data.field2 = this.pnlCompositeLine.getFieldValue('field2');
		employee.data.field3 = this.pnlCompositeLine.getFieldValue('field3');
		this.renderRoom(this.pnlCompositeLine.data.room);
		this.pnlCompositeLine.closeWindow();
	}, 
	htmlEncode: function(aText, aNonBreakingSpace, aNonBreakingDash) {
		if (!this.HTMLEncoder) {
			this.HTMLEncoder = document.createElement("div");
		}
		this.HTMLEncoder.innerHTML = '';
		this.HTMLEncoder.appendChild(document.createTextNode(aText));
		var sResult = this.HTMLEncoder.innerHTML;
		sResult = sResult.replace(/"/g, "&quot;");
		if (aNonBreakingSpace) sResult = sResult.replace(/\s/g, "&nbsp;");
		if (aNonBreakingDash) sResult = sResult.replace(/\-/g, "&#8209;");
		return sResult;
	}
})

afterGeneratingTreeNode = function(treeNode) {
	if (treeNode.treeControl && treeNode.treeControl.id == 'pnlTree') {
		if (treeNode.data['site.site_id'] == '-') {
			var labelText = '<span class="ygtvlabel_pk">' + ctl.htmlEncode(getMessage('noSite')) + '</span>';
			treeNode.setUpLabel(labelText);
		}
	}
}

pnlConsoleBlIdSelectValue = function() {
	ctl.pnlConsole_onBlIdSelectValueAction();
}

document.getElementById('ifrRoomLabels').onload = ctl.attachIframeHandler;
