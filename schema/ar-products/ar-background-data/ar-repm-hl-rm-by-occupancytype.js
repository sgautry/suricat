/**
 * @author Guo
 */
var filterSiteId;
var filterBlId;
var filterLsId;

var controller = View.createController('hlRmByOccupancyTypeController', {

    //----------------event handle--------------------
    afterViewLoad: function(){
        this.arRepmHlRmByOccupancytype_DrawingPanel.appendInstruction("default", "", getMessage('drawingPanelTitle1'));
        this.arRepmHlRmByOccupancytype_DrawingPanel.addEventListener('onclick', onClickDrawingHandler);
        this.arRepmHlRmByOccupancytype_TypeSumGrid.buildPostFooterRows = addTotalRowForSummaryPanel;
        this.site_tree.createRestrictionForLevel = createRestrictionForLevel;
        this.arRepmHlRmByOccupancytype_TypeSumGrid.show(true);
        var treePanel = this.site_tree;
        treePanel._updateTreeNodeChildren = function(node) {
                return;
            }
    },
    
    arRepmHlRmByOccupancytype_TypeSumGrid_afterRefresh: function(){
        resetColorFieldValue('arRepmHlRmByOccupancytype_TypeSumGrid', 'arRepmHlRmByOccupancytype_DrawingPanel', 'rooms_occupancy.occupancy', 'rooms_occupancy.hpattern_acad', 'arRepmHlRmByOccupancytype_TypeSumGrid_legend');
    },
    
    arRepmHlRmByOccupancytype_filterConsole_onShowTree: function(){
        filterSiteId = this.arRepmHlRmByOccupancytype_filterConsole.getFieldValue('bl.site_id');
        filterBlId = this.arRepmHlRmByOccupancytype_filterConsole.getFieldValue('bl.bl_id');
        filterLsId = this.arRepmHlRmByOccupancytype_filterConsole.getFieldValue('rm.ls_id');
        
        if (filterSiteId) {
            this.site_tree.addParameter('siteId', " site_id ='" + filterSiteId + "'");
            this.site_tree.addParameter('siteOfNullBl', " 1=0 ");
        }
        else {
            this.site_tree.addParameter('siteId', " 1=1 ");
            this.site_tree.addParameter('siteOfNullBl', " 1=1 ");
        }
        
        if (filterBlId) {
            var ds = View.dataSources.get('ds_ar-repm-hl-rm-by-occupancytype_form_bl');
            var rec = ds.getRecord(new Ab.view.Restriction({'bl.bl_id':filterBlId}));
            var dsBlId = rec.getValue('bl.bl_id');
            if(dsBlId)
            {
                var site_id = rec.getValue('bl.site_id');
                if(filterSiteId)
                {
                    if(filterSiteId == site_id)
                    {
                        this.site_tree.addParameter('siteId', " site_id ='" + filterSiteId + "'");
                        this.site_tree.addParameter('siteOfNullBl', " 1=0 ");
                    }
                    else
                    {
                        this.site_tree.addParameter('siteId', " 1=0");
                        this.site_tree.addParameter('siteOfNullBl', " 1=0 ");
                    }
                }
                else
                {
                    if(site_id)
                    {
                        this.site_tree.addParameter('siteId', " site_id ='" + site_id + "'");
                        this.site_tree.addParameter('siteOfNullBl', " 1=0 ");
                    }
                    else
                    {
                       this.site_tree.addParameter('siteId', " 1=0 ");
                       this.site_tree.addParameter('siteOfNullBl', " 1=1 "); 
                    }
                    
                }
            }
            else
            {
                this.site_tree.addParameter('siteId', " 1=0 ");
                this.site_tree.addParameter('siteOfNullBl', " 1=0 "); 
            }
            this.site_tree.addParameter('blId', " bl_id = '" + filterBlId + "'");
        }
        else {
            this.site_tree.addParameter('blId', " bl_id IS NOT NULL");
        }
        

        if (filterLsId) {
            this.site_tree.addParameter('lsId', " ls_id ='" + filterLsId + "'");
        }
        else {
            this.site_tree.addParameter('lsId', " 1=1 ");
        }

        this.site_tree.refresh();
        this.arRepmHlRmByOccupancytype_DrawingPanel.clear();
        this.arRepmHlRmByOccupancytype_DrawingPanel.lastLoadedBldgFloor = null;
        this.arRepmHlRmByOccupancytype_TypeSumGrid.clear();
        this.arRepmHlRmByOccupancytype_TypeSumGrid.show(true);
        setPanelTitle('arRepmHlRmByOccupancytype_DrawingPanel', getMessage('drawingPanelTitle1'));
    }
});

/**
 * event handler when click the floor level of the tree
 * @param {Object} ob
 */
function onFlTreeClick(ob){
    var currentNode = View.panels.get('site_tree').lastNodeClicked;
    var blId = currentNode.parent.data['bl.bl_id'];
    var flId = currentNode.data['fl.fl_id'];
     
    var restriction = new Ab.view.Restriction();
    restriction.addClause("rm.bl_id", blId, "=");
    restriction.addClause("rm.fl_id", flId, "=");
    restriction.addClause("rm.dwgname", currentNode.data['fl.dwgname'], "=");
    var drawingPanel = View.panels.get('arRepmHlRmByOccupancytype_DrawingPanel');
    var title = String.format(getMessage('drawingPanelTitle2'), blId + "-" + flId);
    drawingPanel.currentHighlightDS = 'ds_ar-repm-hl-rm-by-occupancytype_drawing_rmHighlight';
    displayFloor(drawingPanel, currentNode, title);
    
    var typeSumGrid = View.panels.get('arRepmHlRmByOccupancytype_TypeSumGrid');
    typeSumGrid.show(true);
    typeSumGrid.refresh(restriction);
}

/**
 * event handler when click room in the drawing panel
 * @param {Object} pk
 * @param {boolean} selected
 */
function onClickDrawingHandler(pk, selected){
    if (selected) {
        var restriction = new Ab.view.Restriction();
        restriction.addClause("rm.bl_id", pk[0], "=", true);
        restriction.addClause("rm.fl_id", pk[1], "=", true);
        restriction.addClause("rm.rm_id", pk[2], "=", true);
        
        var rmDetailPanel = View.panels.get('arRepmHlRmByOccupancytype_RmDetailPanel');
        rmDetailPanel.refresh(restriction);
        rmDetailPanel.show(true);
        rmDetailPanel.showInWindow({
            width: 500,
            height: 250
        });
        
        var drawingPanel = View.panels.get('arRepmHlRmByOccupancytype_DrawingPanel');
        drawingPanel.setTitleMsg(drawingPanel.instructs["default"].msg);
    }
}

function afterGeneratingTreeNode(treeNode){

    if (treeNode.tree.id != 'site_tree') {
        return;
    }
    var labelText1 = "";
    if (treeNode.level.levelIndex == 0) {
        var siteCode = treeNode.data['site.site_id'];
        
        if (!siteCode) {
            labelText1 = "<span class='" + treeNode.level.cssPkClassName + "'>" + getMessage("noSite") + "</span>";
            treeNode.setUpLabel(labelText1);
        }
    }
}


function createRestrictionForLevel(parentNode, level){
    var restriction = null;
    if (parentNode.data) {
        var siteId = parentNode.data['site.site_id'];
        if (!siteId && level == 1) {
            restriction = new Ab.view.Restriction();
            restriction.addClause('bl.site_id', '', 'IS NULL', 'AND', true);
        }
    }
    return restriction;
}

/**
 * add total statistical row for summary panel in hightlight type report
 * @param {Object} parentElement
 */
function addTotalRowForSummaryPanel(parentElement){
    if (this.rows.length < 2) {
        return;
    }
    var tableName = 'rm';
    if (this.sumaryTableName) {
        tableName = this.sumaryTableName;
    }
    var totalCount = 0;
    var totalArea = 0;
    var countRowValue = ''
    var areaRowValue = '';
    for (var i = 0; i < this.rows.length; i++) {
        var row = this.rows[i];
        if (row[tableName + '.total_count.raw']) {
            countRowValue = row[tableName + '.total_count.raw'];
        }
        else {
            countRowValue = row[tableName + '.total_count'];
        }
        
        if (row[tableName + '.total_area.raw']) {
            areaRowValue = row[tableName + '.total_area.raw'];
        }
        else {
            areaRowValue = row[tableName + '.total_area'];
        }
        totalCount += parseInt(countRowValue);
        totalArea += parseFloat(areaRowValue);
    }
    totalCount = insertGroupingSeparator(totalCount.toFixed(0), true, true);
    totalArea = insertGroupingSeparator(totalArea.toFixed(2), true, true);
    
    // create new grid row and cells containing statistics
    var gridRow = document.createElement('tr');
    parentElement.appendChild(gridRow);
    // column 1,2: empty      
    gridRow.appendChild(document.createElement('th'));
    gridRow.appendChild(document.createElement('th'));
    
    // column 3: total room count
    var gridCell = document.createElement('th');
    gridCell.innerHTML = totalCount.toString();
    gridCell.style.color = 'red';
    gridCell.style.textAlign = 'right';
    gridRow.appendChild(gridCell);
    
    // column 4: total room area
    gridCell = document.createElement('th');
    gridCell.innerHTML = totalArea.toString();
    gridCell.style.textAlign = 'right';
    gridCell.style.color = 'red';
    gridRow.appendChild(gridCell);
    
    // column 5: 'Total' title
    var gridCell = document.createElement('th');
    gridCell.innerHTML = getMessage('total');
    gridCell.style.color = 'red';
    gridRow.appendChild(gridCell);
}

/**
 * display floor drawing for highlight report
 * @param {Object} drawingPanel
 * @param {Object} res
 * @param {String} title
 */
function displayFloor(drawingPanel, currentNode, title){
    var blId = getValueFromTreeNode(currentNode, 'bl.bl_id');
    var flId = getValueFromTreeNode(currentNode, 'fl.fl_id');
    var dwgName = getValueFromTreeNode(currentNode, 'fl.dwgname');
    //if the seleted floor is same as the current drawing panel, just reset the highlight
    if (drawingPanel.lastLoadedBldgFloor == dwgName) {
        drawingPanel.clearHighlights();
        drawingPanel.applyDS('highlight');
    }
    else {
        var dcl = new Ab.drawing.DwgCtrlLoc(blId, flId, null, dwgName);
        drawingPanel.addDrawing(dcl);
        drawingPanel.lastLoadedBldgFloor = dwgName;
    }
    
    drawingPanel.appendInstruction("default", "", title);
    drawingPanel.processInstruction("default", "");
}

/**
 * get value from tree node
 * @param {Object} treeNode
 * @param {String} fieldName
 */
function getValueFromTreeNode(treeNode, fieldName){
    var value = null;
    if (treeNode.data[fieldName]) {
        value = treeNode.data[fieldName];
        return value;
    }
    if (treeNode.parent.data[fieldName]) {
        value = treeNode.parent.data[fieldName];
        return value;
    }
    if (treeNode.parent.parent.data[fieldName]) {
        value = treeNode.parent.parent.data[fieldName];
        return value;
    }
    if (treeNode.parent.parent.parent.data[fieldName]) {
        value = treeNode.parent.parent.parent.data[fieldName];
        return value;
    }
    return value;
}

/**
 * reset color field of the sumary grid
 * @param {string} gridPanelId
 * @param {string} drawingPanelId
 * @param {string} filedName
 * @param {string} hpFieldName
 * @param {string} colorfieldName
 */
function resetColorFieldValue(gridPanelId, drawingPanelId, filedName, hpFieldName, colorfieldName){
    var panel = View.panels.get(gridPanelId);
    var rows = panel.rows;
    var opacity = View.panels.get(drawingPanelId).getFillOpacity();
    
    for (var i = 0; i < rows.length; i++) {
        var val = rows[i][filedName];
        var color = '#FFFFFF';
        var hpval = rows[i][hpFieldName];
        if (hpval.length) 
            color = gAcadColorMgr.getRGBFromPatternForGrid(hpval, true);
        
        var cellEl = Ext.get(rows[i].row.cells.get(colorfieldName).dom);
        
        if (!hpval.length)
            cellEl.setStyle('background-color', color);
        
        cellEl.setOpacity(opacity);
    }
}
