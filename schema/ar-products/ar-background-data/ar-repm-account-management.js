var viewBuDvDpController = View.createController('viewBuDvDp', {

    //Current Selected Node 
    curTreeNode: null,
    
    //The tree panel 
    treeview: null,
    
    panelId: "",
    fieldId: "",
    //----------------event handle--------------------
    afterViewLoad: function(){
        this.ar_epic_tree.addParameter('epicIdSql', "");
        this.ar_epic_tree.addParameter('dvId', "dv.dv_id IS NOT NULL");
        this.ar_epic_tree.addParameter('dpId', "dp.dp_id IS NOT NULL");
        this.ar_epic_tree.createRestrictionForLevel = createRestrictionForLevel;
        var treePanel = this.ar_epic_tree;
        treePanel._updateTreeNodeChildren = function(node) {
                return;
            }
    },
     
    arEpicDvDpFilterPanel_onShow: function(){
        this.refreshTreeview();
        this.ar_epic_detail.show(false);
        this.bu_detail.show(false);
        this.dv_detail.show(false);
        this.dp_detail.show(false);
    },
    
    refreshTreeview: function(){
        var consolePanel = this.arEpicDvDpFilterPanel;
        var filterEpicId = consolePanel.getFieldValue('bu.epic_id');
        if(filterEpicId)
        {
            this.ar_epic_tree.addParameter('epicId', " ar_epic.epic_id ='" + convert2SafeSqlString(filterEpicId) + "'");
            this.ar_epic_tree.addParameter('epicOfNullBu', " 1=0 ");
        }
        else{
            this.ar_epic_tree.addParameter('epicId', " 1=1 ");
            this.ar_epic_tree.addParameter('epicOfNullBu', " 1=1 ");
        }
        var filterBusinessId = consolePanel.getFieldValue('bu.bu_id');
        if (filterBusinessId) {
            var ds = View.dataSources.get('ds_ar-repm-acc-mgnt_form_bu');
            var rec = ds.getRecord(new Ab.view.Restriction({'bu.bu_id':filterBusinessId}));
            var dsBu_id = rec.getValue('bu.bu_id');
            var dsEpic_id = rec.getValue('bu.epic_id');
            if(dsBu_id)
            {
                if(filterEpicId)
                {
                    if(filterEpicId == dsEpic_id)
                    {
                        this.ar_epic_tree.addParameter('epicId', " ar_epic.epic_id ='" + filterEpicId + "'");
                        this.ar_epic_tree.addParameter('epicOfNullBu', " 1=0 ");
                    }
                    else
                    {
                        this.ar_epic_tree.addParameter('epicId', " 1=0 ");
                        this.ar_epic_tree.addParameter('epicOfNullBu', " 1=0 ");
                    }
                }
                else
                {
                    if(dsEpic_id)
                    {
                        this.ar_epic_tree.addParameter('epicId', " ar_epic.epic_id ='" + dsEpic_id + "'");
                        this.ar_epic_tree.addParameter('epicOfNullBu', " 1=0 ");
                    }
                    else
                    {
                        this.ar_epic_tree.addParameter('epicId', " 1=0 ");
                        this.ar_epic_tree.addParameter('epicOfNullBu', " 1=1 ");
                    }  
                }
            }
            else
            {
                this.ar_epic_tree.addParameter('epicId', " 1=0 ");
                this.ar_epic_tree.addParameter('epicOfNullBu', " 1=0 ");
            }
            
            this.ar_epic_tree.addParameter('buId', " bu.bu_id ='" + convert2SafeSqlString(filterBusinessId) + "'");
        }
        else {
            this.ar_epic_tree.addParameter('buId', " 1=1 ");
        }
        
        var filterDivisionId = consolePanel.getFieldValue('dv.dv_id');
        if (filterDivisionId) {
            var ds_dv = View.dataSources.get('ds_ar-repm-acc-mgnt_form_dv');
            var rec_dv = ds_dv.getRecord(new Ab.view.Restriction({'dv.dv_id':filterDivisionId}));
            var dsDv_id = rec_dv.getValue('dv.dv_id');
            if(dsDv_id)
            {
                var dsBu_id = rec_dv.getValue('dv.bu_id');
                var ds_bu = View.dataSources.get('ds_ar-repm-acc-mgnt_form_bu');
                var rec_bu = ds_bu.getRecord(new Ab.view.Restriction({'bu.bu_id':dsBu_id}));
                var dsEpic_id = rec_bu.getValue('bu.epic_id');
                if(filterEpicId)
                {
                    if(filterEpicId == dsEpic_id)
                    {
                        this.ar_epic_tree.addParameter('epicId', " ar_epic.epic_id ='" + filterEpicId + "'");
                        this.ar_epic_tree.addParameter('epicOfNullBu', " 1=0 ");
                    }
                    else
                    {
                        this.ar_epic_tree.addParameter('epicId', " 1=0 ");
                        this.ar_epic_tree.addParameter('epicOfNullBu', " 1=0 ");
                    }
                }
                else
                {
                    if(dsEpic_id)
                    {
                        this.ar_epic_tree.addParameter('epicId', " ar_epic.epic_id ='" + dsEpic_id + "'");
                        this.ar_epic_tree.addParameter('epicOfNullBu', " 1=0 ");
                    }
                    else
                    {
                        this.ar_epic_tree.addParameter('epicId', " 1=0 ");
                        this.ar_epic_tree.addParameter('epicOfNullBu', " 1=1 ");
                    }  
                }
                if(filterBusinessId)
                {
                    if(filterBusinessId == dsBu_id)
                    {
                        this.ar_epic_tree.addParameter('buId', " bu.bu_id ='" + convert2SafeSqlString(filterBusinessId) + "'");
                    }
                    else
                    {
                        this.ar_epic_tree.addParameter('buId', " 1=0 ");
                        this.ar_epic_tree.addParameter('epicId', " 1=0 ");
                        this.ar_epic_tree.addParameter('epicOfNullBu', " 1=0 ");
                    }
                }
                else
                {
                    if(dsBu_id)
                    {
                        this.ar_epic_tree.addParameter('buId', " bu.bu_id ='" + convert2SafeSqlString(dsBu_id) + "'");
                    }
                    else
                    {
                        this.ar_epic_tree.addParameter('buId', " 1=0 ");
                    }  
                }
            }
            else
            {
                this.ar_epic_tree.addParameter('epicId', " 1=0 ");
                this.ar_epic_tree.addParameter('epicOfNullBu', " 1=0 ");
            }
            
            this.ar_epic_tree.addParameter('dvId', " dv.dv_id = '" + convert2SafeSqlString(filterDivisionId) + "'");            
        }
        else {
            this.ar_epic_tree.addParameter('dvId', " dv.dv_id IS NOT NULL");
        }
        
        var filterDeptId = consolePanel.getFieldValue('dp.dp_id');
        if (filterDeptId) {
            var restriction = new Ab.view.Restriction();
            if(filterDivisionId)
            {
                restriction.addClause('dp.dv_id', filterDivisionId);
            }
            
            restriction.addClause('dp.dp_id', filterDeptId);
            var ds_dp = View.dataSources.get('ds_ar-repm-acc-mgnt_form_dp');
            var rec_dp = ds_dp.getRecords(restriction);
            if(rec_dp && rec_dp.length == 0)
            {
                this.ar_epic_tree.addParameter('epicId', " 1=0 ");
                this.ar_epic_tree.addParameter('epicOfNullBu', " 1=0 ");
            }
            this.ar_epic_tree.addParameter('dpId', " dp.dp_id = '" + convert2SafeSqlString(filterDeptId) + "'");                       
        }
        else {
            this.ar_epic_tree.addParameter('dpId', " dp.dp_id IS NOT NULL");
        }
        
        this.ar_epic_tree.refresh();
        this.curTreeNode = null;
    },

    openDialogBox: function(panelName,fieldName){
    panelId = panelName;
    fieldId = fieldName;
    View.hpatternPanel = View.panels.get(panelName);
    View.hpatternField = fieldName;
    View.patternString = View.hpatternPanel.getFieldValue(fieldName);
    var thisController = this;
    var dialog = View.openDialog('ab-hpattern-dialog.axvw', null, false, {
            closeButton: false,
            width: 700,
            height: 530,
        
            // this callback function will be called after the dialog view is loaded
            afterViewLoad: function(dialogView) {
                // access the dialog controller property
                var dialogController = dialogView.controllers.get('setHighlightPattern_Controller');
    
                // set the dialog controller onClose callback                
                dialogController.onClose = thisController.dialog_onClose.createDelegate(thisController);
            },
    });
    
    },
  
    dialog_onClose: function(dialogController){
      showColors(panelId,fieldId);
    }

})

/*
 * set the global variable 'curTreeNode' in controller 'viewBuDvDp'
 */
function onTreeviewClick(){
    View.controllers.get('viewBuDvDp').curTreeNode = View.panels.get("ar_epic_tree").lastNodeClicked;
    showColors('ar_epic_detail','ar_epic.hpattern_acad');
    showColors('bu_detail','bu.hpattern_acad');
    showColors('dv_detail','dv.hpattern_acad');
    showColors('dp_detail','dp.hpattern_acad');
}

function onArEpicClick(){
    var curTreeNode = View.panels.get("ar_epic_tree").lastNodeClicked;
    var epicId = curTreeNode.data['ar_epic.epic_id'];
    View.controllers.get('viewBuDvDp').curTreeNode = curTreeNode;
    if (!epicId) {
        View.panels.get("ar_epic_detail").show(false);
        View.panels.get("bu_detail").show(false);
        View.panels.get("dv_detail").show(false);
        View.panels.get("dp_detail").show(false);
    }
    else {
        var restriction = new Ab.view.Restriction();
        restriction.addClause("ar_epic.epic_id", epicId, '=');
        View.panels.get('dvDpDetailTabs').selectTab("arEpicTab", restriction, false, false, false);
    }
    showColors('ar_epic_detail','ar_epic.hpattern_acad');
    showColors('bu_detail','bu.hpattern_acad');
    showColors('dv_detail','dv.hpattern_acad');
    showColors('dp_detail','dp.hpattern_acad');
}

function afterGeneratingTreeNode(treeNode){
    if (treeNode.tree.id != 'ar_epic_tree') {
        return;
    }
    var labelText1 = "";
    if (treeNode.level.levelIndex == 0) {
        var epicName = treeNode.data['ar_epic.name'];
        var epicCode = treeNode.data['ar_epic.epic_id'];
        
        if (!epicCode) {
            labelText1 = "<span class='" + treeNode.level.cssPkClassName + "'>" + getMessage("noEpic") + "</span> ";
            treeNode.setUpLabel(labelText1);
        }
    }
    if (treeNode.level.levelIndex == 1) {
        var buName = treeNode.data['bu.name'];
        var buCode = treeNode.data['bu.bu_id'];
        
        labelText1 = "<span class='" + treeNode.level.cssPkClassName + "'>" + buCode + "</span> ";
        labelText1 = labelText1 + "<span class='" + treeNode.level.cssClassName + "'>" + buName + "</span> ";
        treeNode.setUpLabel(labelText1);
    }
    if (treeNode.level.levelIndex == 2) {
        var dvName = treeNode.data['dv.name'];
        var dvCode = treeNode.data['dv.dv_id'];
        
        labelText1 = "<span class='" + treeNode.level.cssPkClassName + "'>" + dvCode + "</span> ";
        labelText1 = labelText1 + "<span class='" + treeNode.level.cssClassName + "'>" + dvName + "</span> ";
        treeNode.setUpLabel(labelText1);
    }
    if (treeNode.level.levelIndex == 3) {
        var deptName = treeNode.data['dp.name'];
        var deptCode = treeNode.data['dp.dp_id'];
        
        labelText1 = "<span class='" + treeNode.level.cssPkClassName + "'>" + deptCode + "</span> ";
        labelText1 = labelText1 + "<span class='" + treeNode.level.cssClassName + "'>" + deptName + "</span> ";
        treeNode.setUpLabel(labelText1);
    }
}

function createRestrictionForLevel(parentNode, level){
    var restriction = null;
    if (parentNode.data) {
        var epicId = parentNode.data['ar_epic.epic_id'];
        if (!epicId && level == 1) {
            restriction = new Ab.view.Restriction();
            restriction.addClause('bu.epic_id', '', 'IS NULL', 'AND', true);
        }
    }
    return restriction;
}

function selectHpattern(panelId, field){
   var currentView =  View.controllers.get('viewBuDvDp');
   currentView.openDialogBox(panelId,field);
}