var defineLocFLController = View.createController('defineLocationFL', {

    //Current Selected Node 
    curTreeNode: null,
    
    //The tree panel 
    treeview: null,
    
	// geographical id's
	ctryId: "",
	cityId: "",
    
    //----------------event handle--------------------
    afterViewLoad: function(){
        this.site_tree.addParameter('sitetIdSql', "");
        this.site_tree.addParameter('blId', "bl_id IS NOT NULL");
        this.site_tree.addParameter('flId', "fl_id IS NOT NULL");
        this.site_tree.addParameter('rmId', "rm_id IS NOT NULL");
        this.site_tree.createRestrictionForLevel = createRestrictionForLevel;
        var treePanel = this.site_tree;
        treePanel._updateTreeNodeChildren = function(node) {
                return;
        }
    },
    
    afterInitialDataFetch: function(){
        this.treeview = View.panels.get('site_tree');
    },
    
    
    sbfFilterPanel_onShow: function(){
        this.refreshTreeview();
        this.site_detail.show(false);
        this.bl_detail.show(false);
        this.fl_detail.show(false);
        this.rm_detail.show(false);
    },
  
   
   
    refreshTreeview: function(){
        var consolePanel = this.sbfFilterPanel;

        var filterSiteId = consolePanel.getFieldValue('bl.site_id');
        if (filterSiteId) {
            this.site_tree.addParameter('siteId', " site_id ='" + filterSiteId
             + "'");
            this.site_tree.addParameter('siteOfNullBl', " 1=0 ");
        }
        else {
            this.site_tree.addParameter('siteId', " 1=1 ");
            this.site_tree.addParameter('siteOfNullBl', " 1=1 ");
        }
        
        var filterShortBlId = consolePanel.getFieldValue('bl.short_bl_id');
        
        if (filterShortBlId) {
            var ds = View.dataSources.get('ds_ab-sp-def-loc_form_site');
            var dsBl = View.dataSources.get('ds_ab-sp-def-loc_form_bl');
            var restriction = '';
            restriction += "bl.site_id is null and bl.short_bl_id='"+filterShortBlId+"'";
            var recSiteNull = dsBl.getRecord(restriction);
            var rec_bl = dsBl.getRecord(new Ab.view.Restriction({'bl.short_bl_id':filterShortBlId}));
            var rec_site = ds.getRecord(new Ab.view.Restriction({'site.site_id':filterSiteId}));
            var site_id = rec_site.getValue('site.site_id');
            var shortBlId = rec_bl.getValue('bl.short_bl_id');
            if(shortBlId)
            {
                if(filterSiteId)
                {
                    if(filterSiteId == site_id)
                    {
                        this.site_tree.addParameter('siteId', " site_id ='" + filterSiteId + "'");
                        this.site_tree.addParameter('siteOfNullBl', " 1=0 ");
                    }
                    else
                    {
                        this.site_tree.addParameter('siteId', " 1=0");
                        this.site_tree.addParameter('siteOfNullBl', " 1=0 ");
                    }
                }
                else
                {
                    this.site_tree.addParameter('siteId', " site_id in (select site_id from bl where short_bl_id='"+filterShortBlId+"')");
                    if(recSiteNull.getValue('bl.short_bl_id'))
                    {
                        this.site_tree.addParameter('siteOfNullBl', " 1=1 ");
                    }
                    else
                    {
                        this.site_tree.addParameter('siteOfNullBl', " 1=0 ");
                    }
                    
                }
                
                this.site_tree.addParameter('blId', " short_bl_id = '" + filterShortBlId + "'");
            }
            else
            {
                this.site_tree.addParameter('blId', "1=0");
                this.site_tree.addParameter('siteId', " 1=0");
                this.site_tree.addParameter('siteOfNullBl', " 1=0 ");
            }
        }  
        else 
        {
            this.site_tree.addParameter('blId', "1=1");
        }      

        var countryId = consolePanel.getFieldValue('bl.ctry_id');
        if(countryId){
            this.site_tree.addParameter('ctryId', "ctry_id = '" + countryId + "'");
            this.site_tree.addParameter('siteOfNullBl', " 1=0 ");
        }else{
            this.site_tree.addParameter('ctryId', "1=1");     
        }
        
        var cityId = consolePanel.getFieldValue('bl.city_id');
        if(cityId){
            this.site_tree.addParameter('cityId', "city_id = '" + cityId + "'");
            this.site_tree.addParameter('siteOfNullBl', " 1=0 ");
        }else{
            this.site_tree.addParameter('cityId', "1=1");
        }

        this.site_tree.refresh();
        this.curTreeNode = null;
    },
    
    
	
	getCurrentBlRestriction:function(){
		var record = this.bl_detail.record;
    	var restriction = new Ab.view.Restriction();
    	
    	if(record) {
 			restriction.addClause('bl.bl_id', record.getValue('bl.bl_id'), '=', 'OR');  
 		} else {
    		restriction.addClause('bl.bl_id', 'null', "=", "OR");
    	}
    	return restriction;
	},
	
	dialog_onClose:function(dialogController){
		refreshBlDetailPanel();
	}
})

function refreshBlDetailPanel(){
	defineLocFLController.bl_detail.refresh(defineLocFLController.bl_detail.restriction);
}


function onClickTreeNode(){
    View.controllers.get('defineLocationFL').curTreeNode = View.panels.get("site_tree").lastNodeClicked;
}

function onClickSiteNode(){
    var curTreeNode = View.panels.get("site_tree").lastNodeClicked;
    var siteId = curTreeNode.data['site.site_id'];
    View.controllers.get('defineLocationFL').curTreeNode = curTreeNode;
    if (!siteId) {
        View.panels.get("site_detail").show(false);
        View.panels.get("bl_detail").show(false);
        View.panels.get("fl_detail").show(false);
    }
    else {
        var restriction = new Ab.view.Restriction();
        restriction.addClause("site.site_id", siteId, '=');
        View.panels.get('sbfDetailTabs').selectTab("siteTab", restriction, false, false, false);
    }
}

function afterGeneratingTreeNode(treeNode){

    if (treeNode.tree.id != 'site_tree') {
        return;
    }
    var labelText1 = "";
    if (treeNode.level.levelIndex == 0) {
        var siteCode = treeNode.data['site.site_id'];
        
        if (!siteCode) {
            labelText1 = "<span class='" + treeNode.level.cssPkClassName + "'>" + getMessage("noSite") + "</span>";
            treeNode.setUpLabel(labelText1);
        }
    }
    if (treeNode.level.levelIndex == 1) {
        var buildingName = treeNode.data['bl.name'];
        var buildingId = treeNode.data['bl.bl_id'];
        
        labelText1 = "<span class='" + treeNode.level.cssPkClassName + "'>" + buildingId + "</span>";
        labelText1 = labelText1 + "<span class='" + treeNode.level.cssClassName + "'> " + buildingName + "</span>";
        treeNode.setUpLabel(labelText1);
    }
    if (treeNode.level.levelIndex == 2) {
        var floorId = treeNode.data['fl.fl_id'];
        var floorName = treeNode.data['fl.name'];
        
        labelText1 = "<span class='" + treeNode.level.cssPkClassName + "'>" + floorId + "</span>";
        labelText1 = labelText1 + "<span class='" + treeNode.level.cssClassName + "'> " + floorName + "</span>";
        treeNode.setUpLabel(labelText1);
    }
    if (treeNode.level.levelIndex == 3) {
        var roomId = treeNode.data['rm.rm_id'];
        var roomName = treeNode.data['rm.name'];
        
        labelText1 = "<span class='" + treeNode.level.cssPkClassName + "'>" + roomId + "</span> ";
        labelText1 = labelText1 + "<span class='" + treeNode.level.cssClassName + "'>" + roomName + "</span> ";
        treeNode.setUpLabel(labelText1);
    }
}

function createRestrictionForLevel(parentNode, level){
    var restriction = null;
    if (parentNode.data) {
        var siteId = parentNode.data['site.site_id'];
        if (!siteId && level == 1) {
            restriction = new Ab.view.Restriction();
            restriction.addClause('bl.site_id', '', 'IS NULL', 'AND', true);
        }
    }
    return restriction;
}

