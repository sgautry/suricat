var defineLocFLController = View.createController('defineExtComReferential', {

    //Current Selected Node 
    curTreeNode: null,
    
    //The tree panel 
    treeview: null,
    
    panelId: "",
    fieldId: "",
    //----------------event handle--------------------
    afterViewLoad: function(){
		
        this.ext_comp_ref_tree.addParameter('actId', "act_id IS NOT NULL");
        this.ext_comp_ref_tree.addParameter('ssactId', "ssact_id IS NOT NULL");
        this.ext_comp_ref_tree.addParameter('ensId', "ens_id IS NOT NULL");
        this.ext_comp_ref_tree.createRestrictionForLevel = createRestrictionForLevel;
        var treePanel = this.ext_comp_ref_tree;
        treePanel._updateTreeNodeChildren = function(node) {
                return;
            }
    },
    
    afterInitialDataFetch: function(){    
        this.treeview = View.panels.get('ext_comp_ref_tree');
    },
    
    sbfFilterPanel_onShow: function(){
        this.refreshTreeview();
        View.panels.get("role_detail").show(false);
        View.panels.get("activity_detail").show(false);
        View.panels.get("sub_activity_detail").show(false);
        View.panels.get("teaches_detail").show(false);
    },
    
    sbfFilterPanel_onClear: function(){
        this.refreshTreeview();
        View.panels.get("role_detail").show(false);
        View.panels.get("activity_detail").show(false);
        View.panels.get("sub_activity_detail").show(false);
        View.panels.get("teaches_detail").show(false);
    },
    
    refreshTreeview: function(){
        var consolePanel = this.sbfFilterPanel;
        var rsId = consolePanel.getFieldValue('ar_tiers_rs.rs_id');
        if (rsId) {
            this.ext_comp_ref_tree.addParameter('rsId', "rs_id ='" + rsId + "'");
            this.ext_comp_ref_tree.addParameter('roleOfNullActivity', " 1=0 ");
        }
        else {
            this.ext_comp_ref_tree.addParameter('rsId', " 1=1 ");
            this.ext_comp_ref_tree.addParameter('roleOfNullActivity', " 1=1 ");
        }
        var activtyId = consolePanel.getFieldValue('ar_tiers_act.act_id');
        if (activtyId) {
            var dsa = View.dataSources.get('activity_form_ds');
            var reca = dsa.getRecord(new Ab.view.Restriction({'ar_tiers_act.act_id':activtyId}));
            var act_id = reca.getValue('ar_tiers_act.act_id');
            var rs_id = reca.getValue('ar_tiers_act.rs_id');
            if(act_id)
            {
                if(rsId)
                {
                    if(rsId == rs_id)
                    {
                        this.ext_comp_ref_tree.addParameter('rsId', " rs_id ='" + rs_id + "'");
                        this.ext_comp_ref_tree.addParameter('roleOfNullActivity', " 1=0 ");
                    }
                    else
                    {
                        this.ext_comp_ref_tree.addParameter('rsId', " 1=0 ");
                        this.ext_comp_ref_tree.addParameter('roleOfNullActivity', " 1=0 ");
                    }
                }
                else
                {
                    if(rs_id)
                    {
                        this.ext_comp_ref_tree.addParameter('rsId', " ar_tiers_rs.rs_id ='" + rs_id + "'");
                        this.ext_comp_ref_tree.addParameter('epicOfNullBu', " 1=0 ");
                    }
                    else
                    {
                        this.ext_comp_ref_tree.addParameter('rsId', " 1=0 ");
                        this.ext_comp_ref_tree.addParameter('roleOfNullActivity', " 1=1 ");
                    }
                }
            }
            else
            {
                this.ext_comp_ref_tree.addParameter('rsId', " 1=0 ");
                this.ext_comp_ref_tree.addParameter('roleOfNullActivity', " 1=0 ");
            }
            this.ext_comp_ref_tree.addParameter('actId', " act_id = '" + activtyId + "'");
        }
        else {
            this.ext_comp_ref_tree.addParameter('actId', " 1=1 ");
        }
        var subActivityId = consolePanel.getFieldValue('ar_tiers_ssact.ssact_id');
        if (subActivityId) {
            var dssa = View.dataSources.get('sub_activity_form_ds');
            var recsa = dssa.getRecord(new Ab.view.Restriction({'ar_tiers_ssact.ssact_id':subActivityId}));
            var ssact_id = recsa.getValue('ar_tiers_ssact.ssact_id');            
            if(ssact_id)
            {
                var act_id = recsa.getValue('ar_tiers_ssact.act_id');
                var dsa = View.dataSources.get('activity_form_ds');
                var reca = dsa.getRecord(new Ab.view.Restriction({'ar_tiers_act.act_id':act_id}));
                var rs_id = reca.getValue('ar_tiers_act.rs_id');
                if(rsId)
                {
                    if(rsId == rs_id)
                    {
                        this.ext_comp_ref_tree.addParameter('rsId', " rs_id ='" + rs_id + "'");
                        this.ext_comp_ref_tree.addParameter('roleOfNullActivity', " 1=0 ");
                    }
                    else
                    {
                        this.ext_comp_ref_tree.addParameter('rsId', " 1=0 ");
                        this.ext_comp_ref_tree.addParameter('roleOfNullActivity', " 1=0 ");
                    }
                }
                else
                {
                    if(rs_id)
                    {
                        this.ext_comp_ref_tree.addParameter('rsId', " ar_tiers_rs.rs_id ='" + rs_id + "'");
                        this.ext_comp_ref_tree.addParameter('epicOfNullBu', " 1=0 ");
                    }
                    else
                    {
                        this.ext_comp_ref_tree.addParameter('rsId', " 1=0 ");
                        this.ext_comp_ref_tree.addParameter('roleOfNullActivity', " 1=1 ");
                    }
                }
                if(activtyId)
                {
                    if(activtyId == act_id)
                    {
                        this.ext_comp_ref_tree.addParameter('actId', " act_id = '" + act_id + "'");
                    }
                    else
                    {
                        this.ext_comp_ref_tree.addParameter('actId', " 1=0 ");
                        this.ext_comp_ref_tree.addParameter('rsId', " 1=0 ");
                        this.ext_comp_ref_tree.addParameter('roleOfNullActivity', " 1=0 ");
                    }
                }
                else
                {
                    if(act_id)
                    {
                        this.ext_comp_ref_tree.addParameter('actId', " act_id = '" + act_id + "'");
                    }
                    else
                    {
                        this.ext_comp_ref_tree.addParameter('actId', " 1=0 ");
                    }  
                }
            }
            else
            {
                this.ext_comp_ref_tree.addParameter('rsId', " 1=0 ");
                this.ext_comp_ref_tree.addParameter('roleOfNullActivity', " 1=0 ");
            }
            this.ext_comp_ref_tree.addParameter('ssactId', " ssact_id = '" + subActivityId + "'");
        }
        else {
            this.ext_comp_ref_tree.addParameter('ssactId', "ssact_id IS NOT NULL");
        }
        

        var ensId = consolePanel.getFieldValue('ar_tiers_ens.ens_id');
        if (ensId) {
            var dstc = View.dataSources.get('teaches_from_ds');
            var rectc = dstc.getRecord(new Ab.view.Restriction({'ar_tiers_ens.ens_id':ensId}));
            var ens_id = rectc.getValue('ar_tiers_ens.ens_id');            
            if(ens_id) {
                var ssact_id = rectc.getValue('ar_tiers_ens.ssact_id');            
                var dssa = View.dataSources.get('sub_activity_form_ds');
                var recsa = dssa.getRecord(new Ab.view.Restriction({'ar_tiers_ssact.ssact_id':ssact_id}));
                if(ssact_id)
                {
                    var act_id = recsa.getValue('ar_tiers_ssact.act_id');
                    var dsa = View.dataSources.get('activity_form_ds');
                    var reca = dsa.getRecord(new Ab.view.Restriction({'ar_tiers_act.act_id':act_id}));
                    var rs_id = reca.getValue('ar_tiers_act.rs_id');
                    if(rsId)
                    {
                        if(rsId == rs_id)
                        {
                            this.ext_comp_ref_tree.addParameter('rsId', " rs_id ='" + rs_id + "'");
                            this.ext_comp_ref_tree.addParameter('roleOfNullActivity', " 1=0 ");
                        }
                        else
                        {
                            this.ext_comp_ref_tree.addParameter('rsId', " 1=0 ");
                            this.ext_comp_ref_tree.addParameter('roleOfNullActivity', " 1=0 ");
                        }
                    }
                    else
                    {
                        if(rs_id)
                        {
                            this.ext_comp_ref_tree.addParameter('rsId', " ar_tiers_rs.rs_id ='" + rs_id + "'");
                            this.ext_comp_ref_tree.addParameter('epicOfNullBu', " 1=0 ");
                        }
                        else
                        {
                            this.ext_comp_ref_tree.addParameter('rsId', " 1=0 ");
                            this.ext_comp_ref_tree.addParameter('roleOfNullActivity', " 1=1 ");
                        }
                    }
                    if(activtyId)
                    {
                        if(activtyId == act_id)
                        {
                            this.ext_comp_ref_tree.addParameter('actId', " act_id = '" + act_id + "'");
                        }
                        else
                        {
                            this.ext_comp_ref_tree.addParameter('actId', " 1=0 ");
                            this.ext_comp_ref_tree.addParameter('rsId', " 1=0 ");
                            this.ext_comp_ref_tree.addParameter('roleOfNullActivity', " 1=0 ");
                        }
                    }
                    else
                    {
                        if(act_id)
                        {
                            this.ext_comp_ref_tree.addParameter('actId', " act_id = '" + act_id + "'");
                        }
                        else
                        {
                            this.ext_comp_ref_tree.addParameter('actId', " 1=0 ");
                        }  
                    }
                    if(subActivityId)
                    {
                        if(subActivityId == ssact_id)
                        {
                            this.ext_comp_ref_tree.addParameter('ssactId', " ssact_id = '" + ssact_id + "'");
                        }
                        else
                        {
                            this.ext_comp_ref_tree.addParameter('ssact_id', " 1=0 ");
                            this.ext_comp_ref_tree.addParameter('actId', " 1=0 ");
                            this.ext_comp_ref_tree.addParameter('rsId', " 1=0 ");
                            this.ext_comp_ref_tree.addParameter('roleOfNullActivity', " 1=0 ");
                        }
                    }
                    else
                    {
                        if(ssact_id)
                        {
                            this.ext_comp_ref_tree.addParameter('ssactId', " ssact_id = '" + ssact_id + "'");
                        }
                        else
                        {
                            this.ext_comp_ref_tree.addParameter('ssact_id', " 1=0 ");
                        }  
                    }
                }
                else
                {
                    this.ext_comp_ref_tree.addParameter('rsId', " 1=0 ");
                    this.ext_comp_ref_tree.addParameter('roleOfNullActivity', " 1=0 ");
                }
            }
            else{
                this.ext_comp_ref_tree.addParameter('actId', " 1=0 ");
                this.ext_comp_ref_tree.addParameter('rsId', " 1=0 ");
                this.ext_comp_ref_tree.addParameter('roleOfNullActivity', " 1=0 ");
            }
            this.ext_comp_ref_tree.addParameter('ensId', " ens_id = '" + ensId + "'");
        }
        else {
            this.ext_comp_ref_tree.addParameter('ensId', "ens_id IS NOT NULL");
        }
        
        this.ext_comp_ref_tree.refresh();
        this.curTreeNode = null;
    },

    openDialogBox: function(panelName,fieldName){
    panelId = panelName;
    fieldId = fieldName;
    View.hpatternPanel = View.panels.get(panelName);
    View.hpatternField = fieldName;
    View.patternString = View.hpatternPanel.getFieldValue(fieldName);
    var thisController = this;
    var dialog = View.openDialog('ab-hpattern-dialog.axvw', null, false, {
            closeButton: false,
            width: 700,
            height: 530,
        
            // this callback function will be called after the dialog view is loaded
            afterViewLoad: function(dialogView) {
                // access the dialog controller property
                var dialogController = dialogView.controllers.get('setHighlightPattern_Controller');
    
                // set the dialog controller onClose callback                
                dialogController.onClose = thisController.dialog_onClose.createDelegate(thisController);
            },
    });
    
    },
  
    dialog_onClose: function(dialogController){
      showColors(panelId,fieldId);
    }

   
})

function onClickComRefTree(){
    View.controllers.get('defineExtComReferential').curTreeNode = View.panels.get("ext_comp_ref_tree").lastNodeClicked;
    showColors('role_detail','ar_tiers_rs.hpattern_acad');
    showColors('activity_detail','ar_tiers_act.hpattern_acad');
    showColors('sub_activity_detail','ar_tiers_ssact.hpattern_acad');
    showColors('teaches_detail','ar_tiers_ens.hpattern_acad');
}

function onClickRoleTree(){
    var curTreeNode = View.panels.get("ext_comp_ref_tree").lastNodeClicked;
    var rsId = curTreeNode.data['ar_tiers_rs.rs_id'];
    View.controllers.get('defineExtComReferential').curTreeNode = curTreeNode;
    if (!rsId) {
        View.panels.get("role_detail").show(false);
        View.panels.get("activity_detail").show(false);
        View.panels.get("sub_activity_detail").show(false);
        View.panels.get("teaches_detail").show(false);
    }
    else {
        var restriction = new Ab.view.Restriction();
        restriction.addClause("ar_tiers_rs.rs_id", rsId, '=');
        View.panels.get('sbfDetailTabs').selectTab("roleTab", restriction, false, false, false);
    }
    showColors('role_detail','ar_tiers_rs.hpattern_acad');
    showColors('activity_detail','ar_tiers_act.hpattern_acad');
    showColors('sub_activity_detail','ar_tiers_ssact.hpattern_acad');
    showColors('teaches_detail','ar_tiers_ens.hpattern_acad');
}

function afterGeneratingTreeNode(treeNode){

    if (treeNode.tree.id != 'ext_comp_ref_tree') {
        return;
    }
    var labelText1 = "";
    if (treeNode.level.levelIndex == 0) {
        var rsCode = treeNode.data['ar_tiers_rs.rs_id'];
        
        if (!rsCode) {
            labelText1 = "<span class='" + treeNode.level.cssPkClassName + "'>" + getMessage("noRole") + "</span>";
            treeNode.setUpLabel(labelText1);
        }
    }
    if (treeNode.level.levelIndex == 1) {
        var actName = treeNode.data['ar_tiers_act.name'];
        var actId = treeNode.data['ar_tiers_act.act_id'];
        
        labelText1 = "<span class='" + treeNode.level.cssPkClassName + "'>" + actId + "</span>";
        labelText1 = labelText1 + "<span class='" + treeNode.level.cssClassName + "'> " + actName + "</span>";
        treeNode.setUpLabel(labelText1);
    }
    if (treeNode.level.levelIndex == 2) {
        var ssactId = treeNode.data['ar_tiers_ssact.ssact_id'];
        var ssactName = treeNode.data['ar_tiers_ssact.name'];
        
        labelText1 = "<span class='" + treeNode.level.cssPkClassName + "'>" + ssactId + "</span>";
        labelText1 = labelText1 + "<span class='" + treeNode.level.cssClassName + "'> " + ssactName + "</span>";
        treeNode.setUpLabel(labelText1);
    }
    if (treeNode.level.levelIndex == 3) {
        var ensId = treeNode.data['ar_tiers_ens.ens_id'];
        var ensName = treeNode.data['ar_tiers_ens.name'];
        
        labelText1 = "<span class='" + treeNode.level.cssPkClassName + "'>" + ensId + "</span>";
        labelText1 = labelText1 + "<span class='" + treeNode.level.cssClassName + "'> " + ensName + "</span>";
        treeNode.setUpLabel(labelText1);
    }
}

function createRestrictionForLevel(parentNode, level){
    var restriction = null;
    if (parentNode.data) {
        var rsId = parentNode.data['ar_tiers_rs.rs_id'];
        if (!rsId && level == 1) {
            restriction = new Ab.view.Restriction();
            restriction.addClause('ar_tiers_act.rs_id', '', 'IS NULL', 'AND', true);
        }
    }
    return restriction;
}

function selectHpattern(panelId, field){
    var currentView =  View.controllers.get('defineExtComReferential');
    currentView.openDialogBox(panelId,field);
}