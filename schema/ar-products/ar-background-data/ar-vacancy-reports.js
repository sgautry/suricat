var vacnacyReport = View.createController('vacnacyReport', {
	
	afterInitialDataFetch:function(){
		this.filterPanel.showField('agNameAndId',false);
	},

	site_panel_report_data_afterRefresh: function() {
		var cTable = document.getElementById('site_panel_report_data_table');
		// remove auto-generated total row from cross table
		cTable.deleteRow(2);
		var rowLength = cTable.rows.length;
		// Change background color of specific cells
		for (i = 0; i < rowLength; i++){
			var oCells = cTable.rows.item(i).cells;
			var cellLength = oCells.length;
			var cellVal = oCells.item(0).innerHTML;
			if((cellVal.indexOf("01") != -1) || (cellVal.indexOf("04") != -1) || (cellVal.indexOf("10") != -1)){
				cTable.rows[i].style.backgroundColor ="#cce0f9";
			}
			if(cellVal.indexOf("10")!= -1){
				oCells.item(1).innerHTML = "";
			}
		}
		this.changeCellsWhiteSpaceStyle("site");
	},

	ag_panel_report_data_afterRefresh: function() {
		var cTable = document.getElementById('ag_panel_report_data_table');
		// remove auto-generated total row from cross table
		cTable.deleteRow(2);
		var rowLength = cTable.rows.length;
		// Change background color of specific cells
		for (i = 0; i < rowLength; i++){
			var oCells = cTable.rows.item(i).cells;
			var cellLength = oCells.length;
			var cellVal = oCells.item(0).innerHTML;
			if((cellVal.indexOf("01") != -1) || (cellVal.indexOf("04") != -1) || (cellVal.indexOf("10") != -1)){
				cTable.rows[i].style.backgroundColor ="#cce0f9";
			}
			if(cellVal.indexOf("10")!= -1){
				oCells.item(1).innerHTML = "";
			}
		}
		this.changeCellsWhiteSpaceStyle("ag");
	},

	bl_panel_report_data_afterRefresh: function() {
		var cTable = document.getElementById('bl_panel_report_data_table');
		// remove auto-generated total row from cross table
		cTable.deleteRow(2);
		var rowLength = cTable.rows.length;
		// Change background color of specific cells
		for (i = 0; i < rowLength; i++){
			var oCells = cTable.rows.item(i).cells;
			var cellLength = oCells.length;
			var cellVal = oCells.item(0).innerHTML;
			if((cellVal.indexOf("01") != -1) || (cellVal.indexOf("04") != -1) || (cellVal.indexOf("10") != -1)){
				cTable.rows[i].style.backgroundColor ="#cce0f9";
			}
			if(cellVal.indexOf("10")!= -1){
				oCells.item(1).innerHTML = "";
			}
		}
		this.changeCellsWhiteSpaceStyle("bl");
	},

	fl_panel_report_data_afterRefresh: function() {
		var cTable = document.getElementById('fl_panel_report_data_table');
		// remove auto-generated total row from cross table
		cTable.deleteRow(2);
		var rowLength = cTable.rows.length;
		// Change background color of specific cells
		for (i = 0; i < rowLength; i++){
			var oCells = cTable.rows.item(i).cells;
			var cellLength = oCells.length;
			var cellVal = oCells.item(0).innerHTML;
			if((cellVal.indexOf("01") != -1) || (cellVal.indexOf("04") != -1) || (cellVal.indexOf("10") != -1)){
				cTable.rows[i].style.backgroundColor ="#cce0f9";
			}
			if(cellVal.indexOf("10")!= -1){
				oCells.item(1).innerHTML = "";
			}
		}
		this.changeCellsWhiteSpaceStyle("fl");
	},

	changeCellsWhiteSpaceStyle: function(panelName){
		for(i = 0; i < 10; i++){
			if(document.getElementById('vacancy_report.area;' + panelName + '_panel_report_data_row_r'+i)){
				document.getElementById('vacancy_report.area;' + panelName + '_panel_report_data_row_r'+i).style.whiteSpace="pre";
			}
		}
	}

});

function applyRestriction(){
	View.clearPanelRestrictions();
	var restriction = new Ab.view.Restriction();
	if(document.getElementById('siteButton').checked) {
		getRestriction(restriction, 'vacancy_report','site');
		View.panels.get('site_panel_report_data').refresh();
		View.panels.get('ag_panel_report_data').show(false);
		View.panels.get('bl_panel_report_data').show(false);
		View.panels.get('fl_panel_report_data').show(false);
	}
	if(document.getElementById('agButton').checked) {
		if(checkFilters()){
			getRestriction(restriction, 'vacancy_report','ag');
			View.panels.get('ag_panel_report_data').refresh();
			View.panels.get('site_panel_report_data').show(false);
			View.panels.get('bl_panel_report_data').show(false);
			View.panels.get('fl_panel_report_data').show(false);
		}else{
			View.showMessage(getMessage('filterMessage'));
		}
	}
	if(document.getElementById('blButton').checked) {
		if(checkFilters()){
			getRestriction(restriction, 'vacancy_report','bl');
			View.panels.get('bl_panel_report_data').refresh();
			View.panels.get('site_panel_report_data').show(false);
			View.panels.get('ag_panel_report_data').show(false);
			View.panels.get('fl_panel_report_data').show(false);
		}else{
			View.showMessage(getMessage('filterMessage'));
		}
	}
	if(document.getElementById('flButton').checked) {
		if(checkFilters()){
			getRestriction(restriction, 'vacancy_report','fl');
			View.panels.get('fl_panel_report_data').refresh();	
			View.panels.get('site_panel_report_data').show(false);
			View.panels.get('ag_panel_report_data').show(false);
			View.panels.get('bl_panel_report_data').show(false);
		}else{
			View.showMessage(getMessage('filterMessage'));
		}
	}
}

function clearRestriction(){
	var filterPanel = View.panels.get('filterPanel');
	filterPanel.clear();
	View.panels.get('site_panel_report_data').show(false);
	View.panels.get('ag_panel_report_data').show(false);
	View.panels.get('bl_panel_report_data').show(false);
	View.panels.get('fl_panel_report_data').show(false);
	document.getElementById('siteButton').checked = true;
}

function addRestrictionClause (restriction, panel, fieldName,restfield) {
	if (panel.hasFieldMultipleValues(fieldName)) {
		restriction.addClause(restfield, panel.getFieldMultipleValues(fieldName), 'IN');
	} else {
		var value = panel.getFieldValue(fieldName);
		if (value) {
		restriction.addClause(restfield, value, '=');
		}
	}
}

function getRestriction(restriction,table_name,reportGroupBy){
	var filterPanel = View.panels.get('filterPanel');
	var siteList = filterPanel.getFieldValue('bl.site_id');
	var agList = filterPanel.getFieldValue('bl.ag_id');
	var blList = filterPanel.getFieldValue('bl.bl_id');
	var flList = filterPanel.getFieldValue('fl.fl_id');
	var isConditionRequired = false;
	var restrictionString = "";
	if(siteList){
		var str = filterPanel.getFieldMultipleValues('bl.site_id');
		str = str.toString().replace(/,/g,"','");
		str = " site_id in ('" + str + "')";
		if(isConditionRequired){
			restrictionString = restrictionString + " and "
		}
		restrictionString = restrictionString + str;
		isConditionRequired = true;
	}
	if(agList){
		var ag_name = filterPanel.getFieldMultipleValues('agNameAndId');
		ag_name = ag_name.toString().replace(/,/g,"','");
		var str = " ag_id in ('" + ag_name + "')";
		if(isConditionRequired){
			restrictionString = restrictionString + " and "
		}
		restrictionString = restrictionString + str;
		isConditionRequired = true;
	}
	if(blList){
		var str = filterPanel.getFieldMultipleValues('bl.bl_id');
		str = str.toString().replace(/,/g,"','");
		if(isConditionRequired){
			restrictionString = restrictionString + " and "
		}
		str = " bl_id in ('" + str + "')";
		restrictionString = restrictionString + str;
		isConditionRequired = true;
	}
	if(flList){
		var str = filterPanel.getFieldMultipleValues('fl.fl_id');
		str = str.toString().replace(/,/g,"','");
		if(isConditionRequired){
			restrictionString = restrictionString + " and "
		}
		str = " fl_id in ('" + str + "')";
		restrictionString = restrictionString + str;
		isConditionRequired = true;
	}
	if(!valueExistsNotEmpty(restrictionString)){
		restrictionString="1=1";
	}
	if(reportGroupBy == 'site'){
		var crossPanel = View.panels.get('site_panel_report_data');
		crossPanel.addParameter('dataRestriction',restrictionString);
		crossPanel.addParameter('groupRestriction',restrictionString);
	}
	if(reportGroupBy == 'ag'){
		var crossPanel = View.panels.get('ag_panel_report_data');
		crossPanel.addParameter('dataRestriction',restrictionString);
		crossPanel.addParameter('groupRestriction',restrictionString);
	}
	if(reportGroupBy == 'bl'){
		var crossPanel = View.panels.get('bl_panel_report_data');
		crossPanel.addParameter('dataRestriction',restrictionString);
		crossPanel.addParameter('groupRestriction',restrictionString);
	}
	if(reportGroupBy == 'fl'){
		var crossPanel = View.panels.get('fl_panel_report_data');
		crossPanel.addParameter('dataRestriction',restrictionString);
		crossPanel.addParameter('groupRestriction',restrictionString);
	}
	var popPanel = View.panels.get('panel_abVacancyReports_popup');
	popPanel.addParameter('dataRestriction',restrictionString);
}

function checkFilters(){
	var filterPanel = View.panels.get('filterPanel');
	var siteList = filterPanel.getFieldValue('bl.site_id');
	var agList = filterPanel.getFieldValue('bl.ag_id');
	var blList = filterPanel.getFieldValue('bl.bl_id');
	var flList = filterPanel.getFieldValue('fl.fl_id');
	if(siteList || agList || blList || flList){
		return true;
	}else{
		return false;
	}
}

function afterSelect(fieldName, selectedValue, previousValue){
	if(fieldName=="bl.ag_id" ||fieldName=="ar_ag.ag_id"){
		setAgList(selectedValue.split(","));
	}
}

function setAgList(agIdList){
	var filterPanel = View.panels.get('filterPanel');
	var ds_ar_ag = View.dataSources.get('ds_ar_ag');
	var restriction = " ag_id in (";
	for(var i=0;i<agIdList.length;i++){
		if(i==agIdList.length-1){
			restriction+="'"+agIdList[i].trim()+"')";
		}
		else{
			restriction+="'"+agIdList[i].trim()+"',";
		}	
	}
	restriction = restriction.toString().replace(/\u200c/g,"");
	var record=ds_ar_ag.getRecords(restriction);
	var agName=[];
	for (var j = 0; j < record.length; j++) {
		agName.push(record[j].getValue("ar_ag.ag_name"));
	}
	filterPanel.setFieldMultipleValues('agNameAndId',agName);
}