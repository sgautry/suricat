var defAgencyController = View.createController('defUgUic', {

    //Current Selected Node 
    curTreeNode: null,
    
    //The tree panel 
    treeview: null,
    
    //Operation Type // "INSERT", "UPDATE", "DELETE"
    operType: "",
    
    //Operaton Data Type //'BUSINESSUNIT', 'DIVISION','DEPARTMENT'
    operDataType: "",
    
    agencyChanged: false,
    
    //----------------event handle--------------------
    afterViewLoad: function(){
        this.ag_tree.addParameter('agIdSql', "");
        this.ag_tree.addParameter('ugId', " 1=1 ");
        this.ag_tree.addParameter('uicId', " 1=1 ");
        this.ag_tree.createRestrictionForLevel = createRestrictionForLevel;
    },
    
    afterInitialDataFetch: function(){
        var titleObj = Ext.get('addNew');
        titleObj.on('click', this.showMenu, this, null);
        
        this.treeview = View.panels.get('ag_tree');
    },
    
    showMenu: function(e, item){
        var menuItems = [];
        var menutitle_newAgency = getMessage("agency");
        var menutitle_newUnitStation = getMessage("unitStation");
        var menutitle_newService = getMessage("services");
        
        menuItems.push({
            text: menutitle_newAgency,
            handler: this.onAddNewButtonPush.createDelegate(this, ['AGENCY'])
        });
        menuItems.push({
            text: menutitle_newUnitStation,
            handler: this.onAddNewButtonPush.createDelegate(this, ['UNITSTATION'])
        });
        menuItems.push({
            text: menutitle_newService,
            handler: this.onAddNewButtonPush.createDelegate(this, ['SERVICES'])
        });
        
        var menu = new Ext.menu.Menu({
            items: menuItems
        });
        menu.showAt(e.getXY());
        
    },
    
    onAddNewButtonPush: function(menuItemId){
        var agId = "";
        var ugId = "";
        var nodeLevelIndex = -1;
        if (this.curTreeNode) {
            nodeLevelIndex = this.curTreeNode.level.levelIndex;
            switch (nodeLevelIndex) {
                case 0:
                    agId = this.curTreeNode.data["ar_ag.ag_id"];
                    break;
                case 1:
                    agId = this.curTreeNode.data["ar_ug.ag_id"];
                    ugId = this.curTreeNode.data["ar_ug.ug_id"];
                    break;
                case 2:
                    agId = this.curTreeNode.data["ar_ug.ag_id"];
                    ugId = this.curTreeNode.data["ar_uic.ug_id"];
                    break;
            }
        }
        
        this.operDataType = menuItemId;
        var restriction = new Ab.view.Restriction();
        switch (menuItemId) {
            case "AGENCY":
                this.ug_uic_DetailTabs.selectTab("agTab", null, true, false, false);
                break;
            case "UNITSTATION":
                restriction.addClause("ar_ug.ag_id", agId, '=');
                this.ug_uic_DetailTabs.selectTab("ugTab", restriction, true, false, false);
                break;
            case "SERVICES":
                if (nodeLevelIndex == 0 || nodeLevelIndex == -1) {
                    View.showMessage(getMessage("selectTreeNode"));
                    return;
                }
                restriction.addClause("ar_uic.ug_id", ugId, '=');
                this.ug_uic_DetailTabs.selectTab("uicTab", restriction, true, false, false);
                break;
        }
    },
    
    ag_ug_uicFilterPanel_onShow: function(){
        this.refreshTreeview();
        this.ag_detail.show(false);
        this.ug_detail.show(false);
        this.uic_detail.show(false);
    },
    
    ag_detail_onDelete: function(){
        this.operDataType = "AGENCY";
        this.commonDelete("ds_service-ref_form_ar_ag", "ag_detail", "ar_ag.ag_id");
    },
    ug_detail_onDelete: function(){
        this.operDataType = "UNITSTATION";
        this.commonDelete("ds_service-ref_form_ar_ug", "ug_detail", "ar_ug.ug_id");
    },
    uic_detail_onDelete: function(){
        this.operDataType = "SERVICES";
        this.commonDelete("ds_service-ref_form_ar_uic", "uic_detail", "ar_uic.uic_id");
    },
    commonDelete: function(dataSourceID, formPanelID, primaryFieldFullName){
        this.operType = "DELETE";
        var dataSource = View.dataSources.get(dataSourceID);
        var formPanel = View.panels.get(formPanelID);
        var record = formPanel.getRecord();
        var primaryFieldValue = record.getValue(primaryFieldFullName);
        if (!primaryFieldValue) {
            return;
        }
        var controller = this;
        var confirmMessage = getMessage("messageConfirmDelete").replace('{0}', primaryFieldValue);
        View.confirm(confirmMessage, function(button){
            if (button == 'yes') {
                try {
                    dataSource.deleteRecord(record);
                } 
                catch (e) {
                    var errMessage = getMessage("errorDelete").replace('{0}', primaryFieldValue);
                    View.showMessage('error', errMessage, e.message, e.data);
                    return;
                }
                controller.refreshTreePanelAfterUpdate(formPanel);
                formPanel.show(false);
                
            }
        })
    },
    
    ag_detail_onSave: function(){
        this.operDataType = "AGENCY";
        this.commonSave("ds_service-ref_form_ar_ag", "ag_detail");
    },
    ug_detail_onSave: function(){
        this.operDataType = "UNITSTATION";
        this.commonSave("ds_service-ref_form_ar_ug", "ug_detail");
    },
    uic_detail_onSave: function(){
        this.operDataType = "SERVICES";
        this.commonSave("ds_service-ref_form_ar_uic", "uic_detail");
    },
    commonSave: function(dataSourceID, formPanelID){
        var formPanel = View.panels.get(formPanelID);
		this.agencyChanged = this.hasChanged(formPanel);
        if (!formPanel.newRecord) {
            this.operType = "UPDATE";
        }
        else {
            this.operType = "INSERT";
        }
        if (formPanel.save()) {
			//refresh tree panel and edit panel
			this.onRefreshPanelsAfterSave(formPanel);
			//get message from view file			 
			var message = getMessage('formSaved');
			//show text message in the form				
			formPanel.displayTemporaryMessage(message);
		}
    },
    
    /**
     * refresh tree panel and edit panel after save
     * @param {Object} curEditPanel
     */
    onRefreshPanelsAfterSave: function(curEditPanel){

        if (this.agencyChanged) {
            this.refreshTreeview();
        }
        else {
            //refresh the tree panel
            this.refreshTreePanelAfterUpdate(curEditPanel);
        }
    },
    
    /**
     * refersh tree panel after save or delete
     * @param {Object} curEditPanel
     */
    refreshTreePanelAfterUpdate: function(curEditPanel){
        var parentNode = this.getParentNode(curEditPanel);
        if (parentNode.isRoot()) {
            this.refreshTreeview();
        }
        else {
            this.treeview.refreshNode(parentNode);
            if (parentNode.parent) {
                parentNode.parent.expand();
            }
            parentNode.expand();
        }
        //reset the global variable :curTreeNode
        this.setCurTreeNodeAfterUpdate(curEditPanel, parentNode);
    },
    
    /**
     * prepare the parentNode parameter for calling refreshNode function
     */
    getParentNode: function(){
        var rootNode = this.treeview.treeView.getRoot();
        var levelIndex = -1;
        if (this.curTreeNode) {
            levelIndex = this.curTreeNode.level.levelIndex;
        }
        if ("AGENCY" == this.operDataType) {
            return rootNode;
        }
        else //UNIT STAION
             if ("UNITSTATION" == this.operDataType) {
                switch (levelIndex) {
                    case 0:
                        return this.curTreeNode;
                        break;
                    case 1:
                        return this.curTreeNode.parent;
                        break;
                    case 2:
                        return this.curTreeNode.parent.parent;
                        break;
                    default:
                        return rootNode;
                        break;
                }
            }
            else {
                //SERVICE
                switch (levelIndex) {
                    case 1:
                        return this.curTreeNode;
                        break;
                    case 2:
                        return this.curTreeNode.parent;
                        break;
                    default:
                        View.showMessage(getMessage("selectTreeNode"));
                        break;
                }
            }
        
    },
    refreshTreeview: function(){
        var consolePanel = this.ag_ug_uicFilterPanel;
        var filterAgencyId = consolePanel.getFieldValue('ar_ug.ag_id');
        if (filterAgencyId) {
            this.ag_tree.addParameter('agId', " ar_ag.ag_id ='" + convert2SafeSqlString(filterAgencyId) + "'");
            this.ag_tree.addParameter('agOfNullUg', " 1=0 ");
        }
        else {
            this.ag_tree.addParameter('agId', " 1=1 ");
            this.ag_tree.addParameter('agOfNullUg', " 1=1 ");
        }
        
        var filterUnitStationId = consolePanel.getFieldValue('ar_ug.ug_id');
        if (filterUnitStationId) {
            var ds = View.dataSources.get('ds_service-ref_form_ar_ug');
            var rec = ds.getRecord(new Ab.view.Restriction({'ar_ug.ug_id':filterUnitStationId}));
            var dsUg_id = rec.getValue('ar_ug.ug_id');
            var dsAgency_id = rec.getValue('ar_ug.ag_id');
            if(dsUg_id)
            {
                if(filterAgencyId)
                {
                    if(filterAgencyId == dsAgency_id)
                    {
                        this.ag_tree.addParameter('agId', " ar_ag.ag_id ='" + filterAgencyId + "'");
                        this.ag_tree.addParameter('agOfNullUg', " 1=0 ");
                    }
                    else
                    {
                        this.ag_tree.addParameter('agId', " 1=0 ");
                        this.ag_tree.addParameter('agOfNullUg', " 1=0 ");
                    }
                }
                else
                {
                    if(dsAgency_id)
                    {
                        this.ag_tree.addParameter('agId', " ar_ag.ag_id ='" + dsAgency_id + "'");
                        this.ag_tree.addParameter('agOfNullUg', " 1=0 ");
                    }
                    else
                    {
                        this.ag_tree.addParameter('agId', " 1=0 ");
                        this.ag_tree.addParameter('agOfNullUg', " 1=1 ");
                    }  
                }
            }
            else
            {
                this.ag_tree.addParameter('agId', " 1=0 ");
                this.ag_tree.addParameter('agOfNullUg', " 1=0 ");
            }
            
            this.ag_tree.addParameter('ugId', " ar_ug.ug_id ='" + convert2SafeSqlString(filterUnitStationId) + "'");
        }
        else {
            this.ag_tree.addParameter('ugId', " 1=1 ");
        }
        
        var filterServiceId = consolePanel.getFieldValue('ar_uic.uic_id');
        if (filterServiceId) {
            var ds_ar_uic = View.dataSources.get('ds_service-ref_form_ar_uic');
            var rec_ar_uic = ds_ar_uic.getRecord(new Ab.view.Restriction({'ar_uic.uic_id':filterServiceId}));
            var dsUic_id = rec_ar_uic.getValue('ar_uic.uic_id');
            if(dsUic_id)
            {
                var dsUg_id = rec_ar_uic.getValue('ar_uic.ug_id');
                var ds_ar_ug = View.dataSources.get('ds_service-ref_form_ar_ug');
                var rec_ar_ug = ds_ar_ug.getRecord(new Ab.view.Restriction({'ar_ug.ug_id':dsUg_id}));
                var dsAg_id = rec_ar_ug.getValue('ar_ug.ag_id');
                if(filterAgencyId)
                {
                    if(filterAgencyId == dsAg_id)
                    {
                        this.ag_tree.addParameter('agId', " ar_ag.ag_id ='" + filterAgencyId + "'");
                        this.ag_tree.addParameter('agOfNullUg', " 1=0 ");
                    }
                    else
                    {
                        this.ag_tree.addParameter('agId', " 1=0 ");
                        this.ag_tree.addParameter('agOfNullUg', " 1=0 ");
                    }
                }
                else
                {
                    if(dsAg_id)
                    {
                        this.ag_tree.addParameter('agId', " ar_ag.ag_id ='" + dsAg_id + "'");
                        this.ag_tree.addParameter('agOfNullUg', " 1=0 ");
                    }
                    else
                    {
                        this.ag_tree.addParameter('agId', " 1=0 ");
                        this.ag_tree.addParameter('agOfNullUg', " 1=1 ");
                    }  
                }
                if(filterUnitStationId)
                {
                    if(filterUnitStationId == dsUg_id)
                    {
                        this.ag_tree.addParameter('ugId', " ar_ug.ug_id ='" + convert2SafeSqlString(filterUnitStationId) + "'");
                    }
                    else
                    {
                        this.ag_tree.addParameter('ugId', " 1=0 ");
                        this.ag_tree.addParameter('agId', " 1=0 ");
                        this.ag_tree.addParameter('agOfNullUg', " 1=0 ");
                    }
                }
                else
                {
                    if(dsUg_id)
                    {
                        this.ag_tree.addParameter('ugId', " ar_ug.ug_id ='" + convert2SafeSqlString(dsUg_id) + "'");
                    }
                    else
                    {
                        this.ag_tree.addParameter('ugId', " 1=0 ");
                    }  
                }
            }
            else
            {
                this.ag_tree.addParameter('agId', " 1=0 ");
                this.ag_tree.addParameter('agOfNullUg', " 1=0 ");
            }
            
            this.ag_tree.addParameter('uicId', " ar_uic.uic_id = '" + convert2SafeSqlString(filterServiceId) + "'");            
        }
        else {
            this.ag_tree.addParameter('uicId', " 1=1 ");
        }
        
        this.ag_tree.refresh();
        this.curTreeNode = null;
    },
    
    /**
     * reset the curTreeNode variable after operation
     * @param {Object} curEditPanel : current edit form
     * @param {Object} parentNode
     */
    setCurTreeNodeAfterUpdate: function(curEditPanel, parentNode){
        if (this.operType == "DELETE") {
            this.curTreeNode = null;
        }
        else {
            switch (this.operDataType) {
                case "AGENCY":
                    var pkFieldName = "ar_ag.ag_id";
                    break;
                case "UNITSTATION":
                    var pkFieldName = "ar_ug.ug_id";
                    break;
                case "SERVICES":
                    var pkFieldName = "ar_uic.uic_id";
                    break;
            }
            this.curTreeNode = this.getTreeNodeByCurEditData(curEditPanel, pkFieldName, parentNode);
        }
    },
    
    /**
     * check the curEditFormPanel.getRecord
     *
     * @param {Object} curEditFormPanel
     * return -- true means the user has changed the agency code
     */
    hasChanged: function(curEditFormPanel){
        if (curEditFormPanel.id == "ug_detail") {
            var oldagCode = curEditFormPanel.record.oldValues["ar_ug.ag_id"];
            if (curEditFormPanel.getFieldValue("ar_ug.ag_id") == oldagCode) {
                return false;
            }
            else {
                return true;
            }
        }
    },
    /**
     * get the treeNode according to the current edit from,
     *
     * so need to make the two consistent ,by current edit form
     * @param {Object} curEditForm
     * @param {Object} parentNode
     */
    getTreeNodeByCurEditData: function(curEditForm, pkFieldName, parentNode){
        var pkFieldValue = curEditForm.getFieldValue(pkFieldName);
        for (var i = 0; i < parentNode.children.length; i++) {
            var node = parentNode.children[i];
            if (node.data[pkFieldName] == pkFieldValue) {
                return node;
            }
        }
        return null;
    }
})


/*
 * set the global variable 'curTreeNode' in controller 'defUgUic'
 */
function onTreeviewClick(){
    View.controllers.get('defUgUic').curTreeNode = View.panels.get("ag_tree").lastNodeClicked;
}

function onAgencyClick(){
    var curTreeNode = View.panels.get("ag_tree").lastNodeClicked;
    var agId = curTreeNode.data['ar_ag.ag_id'];
    View.controllers.get('defUgUic').curTreeNode = curTreeNode;
    if (!agId) {
        View.panels.get("ag_detail").show(false);
        View.panels.get("ug_detail").show(false);
        View.panels.get("uic_detail").show(false);
    }
    else {
        var restriction = new Ab.view.Restriction();
        restriction.addClause("ar_ag.ag_id", agId, '=');
        View.panels.get('ug_uic_DetailTabs').selectTab("agTab", restriction, false, false, false);
    }
}

function afterGeneratingTreeNode(treeNode){
    if (treeNode.tree.id != 'ag_tree') {
        return;
    }
    var labelText1 = "";
    if (treeNode.level.levelIndex == 0) {
        var agName = treeNode.data['ar_ag.ag_name'];
        var agCode = treeNode.data['ar_ag.ag_id'];
        
        if (!agCode) {
            labelText1 = "<span class='" + treeNode.level.cssPkClassName + "'>" + getMessage("noagency") + "</span> ";
            treeNode.setUpLabel(labelText1);
        }
    }
    if (treeNode.level.levelIndex == 1) {
        var ugName = treeNode.data['ar_ug.name'];
        var ugCode = treeNode.data['ar_ug.ug_id'];
        
        labelText1 = "<span class='" + treeNode.level.cssPkClassName + "'>" + ugCode + "</span> ";
        labelText1 = labelText1 + "<span class='" + treeNode.level.cssClassName + "'>" + ugName + "</span> ";
        treeNode.setUpLabel(labelText1);
    }
    if (treeNode.level.levelIndex == 2) {
        var uicName = treeNode.data['ar_uic.uic_name'];
        var uicCode = treeNode.data['ar_uic.uic_id'];
        
        labelText1 = "<span class='" + treeNode.level.cssPkClassName + "'>" + uicCode + "</span> ";
        labelText1 = labelText1 + "<span class='" + treeNode.level.cssClassName + "'>" + uicName + "</span> ";
        treeNode.setUpLabel(labelText1);
    }
}

function createRestrictionForLevel(parentNode, level){
    var restriction = null;
    if (parentNode.data) {
        var agId = parentNode.data['ar_ag.ag_id'];
        if (!agId && level == 1) {
            restriction = new Ab.view.Restriction();
            restriction.addClause('ar_ug.ag_id', '', 'IS NULL', 'AND', true);
        }
    }
    return restriction;
}