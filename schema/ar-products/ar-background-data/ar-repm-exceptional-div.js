function linkClient(){
	var divisionPanel = View.panels.get("gridPanel");
	var dvPanel = View.panels.get("dvDetails");
	var selectedRow = divisionPanel.rows[divisionPanel.selectedRowIndex];
	var dvId = selectedRow["dv.dv_id"];
	var restriction = new Ab.view.Restriction();
  	restriction.addClause("dv.dv_id",dvId,'=');
  	dvPanel.refresh(restriction);
	dvPanel.showInWindow({
	    newRecord: false,
	    title: "Associer un Client",
	    x: 200,
	    y: 200, 
	    width: 400,
	    height: 200,
	    closeButton: true
	});	
}

function refreshDivList(){
	var divisionPanel = View.panels.get("gridPanel");
	divisionPanel.refresh();
}