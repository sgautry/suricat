var clientController = View.createController('clientController', {
  
  detailsPanel_onOpenDialogBox: function(){
    View.hpatternPanel = View.panels.get('detailsPanel');
    View.hpatternField = 'ar_client.hpattern_acad';
    View.patternString = View.hpatternPanel.getFieldValue('ar_client.hpattern_acad');
    var thisController = this;
    var dialog = View.openDialog('ab-hpattern-dialog.axvw', null, false, {
            closeButton: false,
            width: 700,
            height: 530,
        
            // this callback function will be called after the dialog view is loaded
            afterViewLoad: function(dialogView) {
                // access the dialog controller property
                var dialogController = dialogView.controllers.get('setHighlightPattern_Controller');
    
                // set the dialog controller onClose callback                
                dialogController.onClose = thisController.dialog_onClose.createDelegate(thisController);
            },
    });
    
  },
  
  dialog_onClose: function(dialogController){
      showColors('detailsPanel','ar_client.hpattern_acad');
  }

});

function deleteRecord(){
var grid = View.panels.get('gridPanel');
var panel = View.panels.get('detailsPanel');
var value = panel.getFieldValue('ar_client.cl_id');
var ds = View.dataSources.get('dv_ds');
var restriction = new Ab.view.Restriction();
restriction.addClause("dv.cl_id", value, '=');
var records = ds.getRecords(restriction);
if((records.length) > 0){
	View.alert("Foreign key constraint violation: [afm].[dv]([cl_id]) referencing [afm].[ar_client]([cl_id])");
	return;
}else{
    if(panel.deleteRecord()){
      grid.refresh();
      panel.show(false);
      View.alert("Record deleted successfully");
    }
  }
}