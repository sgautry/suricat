var connectorController = View.createController('connectorController', {

	connectorJob: 'AbSystemAdministration-ConnectorJob-executeConnector',
	masterConnectorJob: 'AbSystemAdministration-SMSBuilderConnectorJob-executeConnector',
	job_id: 0,

	connector_list_run_onClick : function(row, action){
		var controller = this;
		var connector_id = row.record['afm_connector.connector_id'];

		var connectorWfr = this.connectorJob;

		if(this.isMasterConnector(row)){
			connectorWfr = this.masterConnectorJob;
		}

		try{
			this.jobId =  Workflow.startJob(connectorWfr, connector_id);
            this.refreshLog();
            this.jobCurrentNumber = 0;
            this.reportTask = {
                run: function(){
                    var jobStatus = Workflow.getJobStatus(controller.jobId);
                    if (jobStatus.jobCurrentNumber !== controller.jobCurrentNumber) {
                        controller.jobCurrentNumber = jobStatus.jobCurrentNumber;
                        console.log('jobCurrentNumber ' + controller.jobCurrentNumber);
                        controller.refreshLog();
                    }
                    if (jobStatus.jobFinished) {
                        controller.reportTaskRunner.stop(controller.reportTask);
                        controller.refreshLog();
                    }
                },
                interval: 500
            };
            this.reportTaskRunner = new Ext.util.TaskRunner();
            this.reportTaskRunner.start(this.reportTask);

		}catch(e){
   			Workflow.handleError(e);
		}

	},
    refreshLog: function () {
        this.connector_log.refresh(this.connector_log.restriction);
    },
	/**
	 * Returns true if the connector is master.
	 */
	isMasterConnector: function(connector) {
		return 'SMS-Builder' === connector.record['afm_connector.description'];
	}
});