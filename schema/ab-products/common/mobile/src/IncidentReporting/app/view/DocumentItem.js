Ext.define('IncidentReporting.view.DocumentItem', {
    extend: 'Common.view.DocumentItem',

    xtype: 'incidentDocumentItem',

    config: {
        file: {
            margin: '10px',
            flex: 1,
            hidden: true
        },

        photoName: {
            margin: '10px',
            flex: 1
        }
    },

    applyPhotoName: function(config) {
        return Ext.factory(config, Ext.Component, this.getPhotoName());
    },

    updatePhotoName: function (newPhotoName, oldPhotoName) {
        if (newPhotoName) {
            this.insert(2, newPhotoName);
        }
        if (oldPhotoName) {
            this.remove(oldPhotoName);
        }
    }
});