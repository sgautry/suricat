Ext.define('IncidentReporting.model.Document', {
    extend: 'Common.data.Model',
    config: {
        uniqueIdentifier: ['mob_doc_id'],
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'doc',
                type: 'string',
                isDocumentField: true,
                isSyncField: true
            },
            {
                name: 'name',
                type: 'string'
            },
            {
                name: 'description',
                type: 'string'
            },
            {
                name: 'date_doc',
                type: 'DateClass'
            },
            {
                name: 'doc_author',
                type: 'string'
            },
            {
                name: 'mob_doc_id',
                type: 'IntegerClass'
            },
            {
                name: 'mob_is_changed',
                type: 'IntegerClass'
            },
            {
                name: 'mob_locked_by',
                type: 'string'
            },
            {
                name: 'mob_incident_id',
                type: 'IntegerClass'
            }
        ],

        validations: [
            {
                type: 'presence',
                field: 'doc'
            },
            {
                type: 'presence',
                field: 'name'
            },
            {
                type: 'presence',
                field: 'description'
            }
        ]
    }
});