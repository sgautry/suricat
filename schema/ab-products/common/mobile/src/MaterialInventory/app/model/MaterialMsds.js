Ext.define('MaterialInventory.model.MaterialMsds', {
    extend: 'Common.data.Model',
    config: {
        uniqueIdentifier: ['msds_id'],
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'msds_id',
                type: 'IntegerClass'
            },
            {
                name: 'doc',
                type: 'string',
                isDocumentField: true,
                isSyncField: true
            }
        ]
    }
});  