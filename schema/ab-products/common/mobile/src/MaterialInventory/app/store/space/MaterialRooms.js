Ext.define('MaterialInventory.store.space.MaterialRooms', {
    extend: 'Common.store.sync.ValidatingTableStore',
    requires: ['MaterialInventory.model.space.MaterialRoom'],

    serverTableName: 'rm',

    serverFieldNames: ['bl_id', 'fl_id', 'rm_id', 'rm_std', 'name'],
    inventoryKeyNames: ['bl_id', 'fl_id', 'rm_id'],

    config: {
        model: 'MaterialInventory.model.space.MaterialRoom',
        remoteSort: true,
        remoteFilter: true,
        tableDisplayName: LocaleManager.getLocalizedString('Rooms', 'MaterialInventory.store.MaterialLocations'),
        sorters: [
            {
                property: 'bl_id',
                direction: 'ASC'
            },
            {
                property: 'fl_id',
                direction: 'ASC'
            },
            {
                property: 'rm_id',
                direction: 'ASC'
            }
        ],
        storeId: 'materialRooms',
        enableAutoLoad: true,
        autoSync: false,
        proxy: {
            type: 'Sqlite'
        },
        timestampDownload: false

    },

    /**
     * @private
     * @param restriction
     * @returns {*}
     */
    importRecords: function (restriction, deleteAllRecordsOnSync, timestamp) {
        var me = this,
            resolveFunc,
            rejectFunc,
            pageSize = me.getSyncRecordsPageSize(),
            storeRestriction = me.getRestriction(),
            numberOfImportedRecords = pageSize,
            proxy = me.getProxy(),
            table = proxy.getTable(),
            columns = proxy.getColumns(),
            pagingKeys = {};

        var p = new Promise(function (resolve, reject) {
            resolveFunc = resolve;
            rejectFunc = reject;
        });

        var doImportRecords = function () {
            if (numberOfImportedRecords === 0 || numberOfImportedRecords < pageSize) {
                resolveFunc();
            } else {
                me.retrieveRecords(storeRestriction, pageSize, pagingKeys)
                    .then(function (records) {
                        numberOfImportedRecords = records.length;
                        // Get the new paging keys from the last record.
                        pagingKeys = me.getPagingKeysFromLastRecord(records);
                        return me.convertRecordsFromServer(records);
                    }, function (error) {  // retrieveRecords error handler
                        numberOfImportedRecords = 0;  // End recursion
                        rejectFunc(error);
                    })
                    .then(function (convertedRecords) {
                        return me.insertRecords(convertedRecords, table, columns, me.getModel(), deleteAllRecordsOnSync);
                    })
                    .then(null, function (error) {
                        numberOfImportedRecords = 0;  // End recursion
                        rejectFunc(error);
                    })
                    .then(function () {
                        doImportRecords();
                    });
            }
        };

        doImportRecords();

        return p;
    },

    retrieveRecords: function (restriction, pageSize, keys) {
        var me = this,
            fieldDefFieldNames = me.getFieldNamesFromTableDef(),
            fieldsToSync = Ext.Array.intersect(fieldDefFieldNames, me.serverFieldNames);

        return Common.service.MobileSyncServiceAdapter.retrievePagedRecordsByKey(me.serverTableName, fieldsToSync, restriction, pageSize, keys);

    },

    getPagingKeysFromLastRecord: function (records) {
        var me = this,
            lastRecord,
            primaryKeyFields,
            primaryKeyValues;

        if (records.length === 0) {
            return [];
        }

        lastRecord = records[records.length - 1];
        primaryKeyFields = Common.util.TableDef.getPrimaryKeyFieldsFromTableDef(me.tableDef);

        primaryKeyValues = me.getPrimaryKeyValuesFromFieldObject(lastRecord, primaryKeyFields);
        return primaryKeyValues;
    }
});