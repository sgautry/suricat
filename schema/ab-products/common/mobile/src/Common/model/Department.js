Ext.define('Common.model.Department', {
    extend: 'Common.data.Model',

    config: {
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'dv_id',
                type: 'string'
            },
            {
                name: 'dp_id',
                type: 'string'
            },
            {
                name: 'name',
                type: 'string'
            }
        ],
        uniqueIdentifier: ['dv_id','dp_id']

    }
});