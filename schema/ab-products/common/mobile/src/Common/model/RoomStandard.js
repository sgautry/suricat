Ext.define('Common.model.RoomStandard', {
    extend: 'Common.data.Model',

    config: {
        uniqueIdentifier: ['rm_std'],
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'rm_std',
                type: 'string'
            },
            {
                name: 'description',
                type: 'string'
            },
            {
                name: 'doc_graphic',
                type: 'string',
                isDocumentField: true
            },
            {
                name: 'doc_block',
                type: 'string',
                isDocumentField: true
            }
        ]
    }

});