Ext.define('Common.control.TextInput', {
    extend: 'Ext.field.Input',
    xtype: 'textinput',


    // @private
    getTemplate: function() {
        var items = [
            {
                reference: 'input',
                tag: this.tag
            },
            {
                reference: 'mask',
                classList: [this.config.maskCls]
            },
            {
                reference: 'clearIcon',
                cls: 'x-clear-icon'
            },
            {
                reference: 'actionIcon',
                cls: 'x-arrow'
            }
        ];
        return items;
    },

    initElement: function() {
        var me = this;

        me.callParent();

        if (me.actionIcon) {
            me.actionIcon.on({
                tap: 'onActionIconTap',
                scope: me
            });
        }
    },

    // @private
    onActionIconTap: function(e) {
        this.fireEvent('actionicontap', this, e);
    }
});