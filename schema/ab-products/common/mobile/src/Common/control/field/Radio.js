/**
 * A radio field with circle icon and text after icon.
 * @since 23.2
 * @author Ana Albu
 */
Ext.define('Common.control.field.Radio', {
   extend: 'Ext.field.Radio',
    xtype: 'commonradiofield',

    config: {
        color: null,
        /**
         * @cfg {Booleam} useFieldDefLabel true to use multiline heading from TableDef as field label,
         * or false to use defined label.
         */
        useFieldDefLabel: true,
        labelAlign: 'right',
        labelCls: 'ab-field-common-radio-label',
        labelWidth: '100%',
        cls: 'ab-field-common-radio',
        /**
         * @cfg
         * @inheritdoc
         */
        component: {
            type: 'radio',
            cls: 'ab-input-common-radio'
        }
    }
});