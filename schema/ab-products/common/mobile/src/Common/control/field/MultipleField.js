/**
 * Panel with fields added by tapping on a button.
 *
 * Examples:
 *
 * {
 *      xtype: 'multiplefield',
 *      maxFields: 3,
 *      buttonText: 'Add another employee',
 *      maxFieldsText: 'You have added all employees allowed',
 *      fieldConfig: {
 *          xtype: 'employeePrompt',
 *          label: 'Lookup Field',
 *          labelAlign: 'top'
 *      }
 *  }
 *
 * {
 *      xtype: 'multiplefield',
 *      maxFields: 3,
 *      fieldConfig: {
 *          xtype: 'calendarfield',
 *          label: 'Date Field',
 *          labelAlign: 'top'
 *      }
 *  }
 *
 * @since 23.2
 * @author Cristina Moldovan
 */
Ext.define('Common.control.field.MultipleField', {
    extend: 'Ext.Panel',
    xtype: 'multiplefield',

    config: {
        layout: 'vbox',

        /**
         * @config {int} maxFields
         * Maximum number of fields.
         */
        maxFields: 1,

        /**
         * @cfg {String} buttonText
         * The Button text.
         */
        buttonText: LocaleManager.getLocalizedString('Add field', 'Common.control.field.MultipleField'),

        /**
         * @cfg {String} maxFieldsText
         * The text of the button when {@link maxFields} fields were added.
         */
        maxFieldsText: LocaleManager.getLocalizedString('You have added all allowed fields',
            'Common.control.field.MultipleField'),

        /**
         * @config {Object} fieldConfig
         * Field config.
         */
        fieldConfig: {},

        items: [
            {
                xtype: 'button',
                text: LocaleManager.getLocalizedString('Add field', 'Common.control.field.MultipleField'),
                ui: 'link',
                action: 'addFieldButton'
            }
        ]
    },

    initialize: function () {
        var me = this,
            addFieldButton = me.down('button[action=addFieldButton]');

        addFieldButton.setText(me.config.buttonText);
        addFieldButton.addListener({
            tap: me.addField,
            scope: me
        });

        me.callParent(arguments);

        // add a first field
        me.addField();
    },

    /**
     * Adds a field before the button.
     */
    addField: function () {
        var me = this,
            numberOfFields = me.items.length - 1,
            fieldConfig = me.config.fieldConfig,
            addFieldButton = me.down('button[action=addFieldButton]');

        if (me.getMaxFields() > numberOfFields) {
            if (!Ext.isEmpty(fieldConfig)) {
                me.insertBefore(Ext.factory(me.config.fieldConfig, 'Ext.Component'), addFieldButton);

                // don't repeat the label on the next fields
                fieldConfig.label = null;
            }

            // set the button text informing that the maximum number of fields was reached
            numberOfFields = me.items.length - 1;
            if (me.getMaxFields() === numberOfFields) {
                addFieldButton.setText(me.getMaxFieldsText());
            }
        }
    }
});