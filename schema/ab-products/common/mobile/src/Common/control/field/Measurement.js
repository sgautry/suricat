/**
 * Number field with label for unit of measurement.
 * @since 23.2
 * @author Ana Albu
 */
Ext.define('Common.control.field.Measurement', {
    extend: 'Common.control.field.Number',

    xtype: 'measurementfield',

    config: {
        unitLabel: ''
    },

    initialize: function () {
        var cssClass,
            divElement,
            fieldInputElement,
            text;

        if (!Ext.isEmpty(this.getUnitLabel())) {
            if (this.getLabelAlign() === 'top') {
                cssClass = 'ab-measurement-field-long-form';
            } else {
                cssClass = 'ab-measurement-field-short-form';
            }

            divElement = document.createElement('div');
            divElement.setAttribute('class', cssClass);
            text = document.createTextNode('(' + this.getUnitLabel() + ')');
            divElement.appendChild(text);

            fieldInputElement = this.getComponent().input.dom.parentElement;
            fieldInputElement.parentElement.insertBefore(divElement, fieldInputElement);

        }

        this.callParent(arguments);
    },

    reset: function () {
        this.doClearIconTap(this);
    }
});