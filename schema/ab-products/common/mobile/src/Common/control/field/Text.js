/**
 * TextField with common configuration options applied.
 * Disables the Android input field overlay.
 *
 * @author Jeff Martin
 * @since 21.2
 */
Ext.define('Common.control.field.Text', {
    extend: 'Common.control.Text',

    requires: 'Common.control.TextInput',

    xtype: 'commontextfield',

    config: {
        autoComplete: 'off',

        component: {
            xtype: 'textinput',
            type: 'text',
            fastFocus: true
        },

        /**
         * @cfg actionIconCls The icon class name. The x-arrow class is used by default.
         */
        actionIconCls: null,

        /**
         * @cfg showActionIcon Displays the Action icon when true.
         */
        showActionIcon: false
    },

    applyActionIconCls: function (cls) {
        if (cls && cls !== 'x-arrow') {
            this.getComponent().actionIcon.removeCls('x-arrow');
            this.getComponent().actionIcon.addCls(cls);
        }
        return cls;
    },

    applyShowActionIcon: function(showIcon) {
        var component = this.getComponent(),
            actionIcon = component.actionIcon,
            clearIcon = component.clearIcon,
            style;

        if(showIcon) {
            style = 'display:block';
            clearIcon.dom.style = 'right:1.8em';
        } else {
            style = 'display:none';
            clearIcon.dom.style = 'right:-0.5em';
        }

        if(actionIcon) {
            actionIcon.dom.style = style;
        }

        return showIcon;
    },

    initialize: function () {
        var me = this,
            inputComponent = me.getComponent(),
            actionIcon = inputComponent.actionIcon;

        me.callParent();

        inputComponent.on('keyup', me.onInputChange, me);

        // Hide the text input overlay
        if (Ext.os.is.Android) {
            inputComponent.addCls('android-hide-overlay');
        }

        if(actionIcon) {
            actionIcon.on({
                tap: 'onActionIconTap',
                scope: me
            });
        }
    },

    onInputChange: function () {
        this.fireEvent('inputchanged', this);
    },

    onActionIconTap: function () {
        this.fireEvent('actionicontap', this);
    }
});
