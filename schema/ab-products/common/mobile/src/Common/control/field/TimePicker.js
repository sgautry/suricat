/**
 * This is a field that displays a Common.controls.TimePicker when tapped. The TimePickerField displays hours and
 * minutes like a normal text field in a form.
 * The TimePickerField extends the {@link Ext.field.DatePicker} control and is used in the same way. The difference is
 * that the TimePickerField only allows display of the time part of the date.
 *
 *     Ext.create('Ext.form.Panel',
 *         { fullscreen: true,
 *           items: [
 *               {
 *                   xtype: 'fieldset',
 *                       items: [
 *                           { xtype: 'timepickerfield',
 *                             label: 'Time Started',
 *                             name: 'time_start',
 *                             value: new Date()
 *                            }
 *                           ]
 *                      }
 *                   ]
 *           });
 *
 */

Ext.define('Common.control.field.TimePicker', {
    extend: 'Ext.field.DatePicker',

    xtype: 'timepickerfield',

    requires: [
        'Common.control.picker.Time',
        'Common.control.TimePickerInput'
    ],

    config: {
        dateFormat: 'H:i', // Default format show time only
        picker: true,
        /**
         * @cfg {Booleam} useFieldDefLabel true to use multiline heading from TableDef as field label,
         * or false to use defined label.
         */
        useFieldDefLabel: true,

        component: {
            xtype: 'timepickerinput',
            type: 'text',
            fastFocus: true
        },

        icon: false,

        clearIcon: true
    },

    initialize: function() {
        var me = this;

        me.callParent();

        me.getComponent().on({
            scope: this,
            timepickerstartstop: 'onTimePickerStartStop'
        });
    },

    /**
     * @override
     * @param {Object} value Source copied, small modification
     */
    applyValue: function (value) {
        var date,
            year,
            month,
            day;

        if (!Ext.isDate(value) && !Ext.isObject(value)) {
            value = null;
        }

        // Begin modified section
        if (Ext.isObject(value)) {
            date = new Date();
            year = value.year || date.getFullYear();
            month = value.month || date.getMonth();
            day = value.day || date.getDate();

            value = new Date(year, month, day, value.hours, value.minutes); // Added hour and minutes
        }
        // End modfied section!
        return value;
    },

    updateValue: function(newValue, oldValue) {
        var me     = this,
            picker = me._picker,
            valueValid = newValue !== undefined && newValue !== null && newValue !== "";

        if (picker && picker.isPicker) {
            picker.setValue(newValue);
        }

        // Ext.Date.format expects a Date
        if (newValue !== null) {
            me.getComponent().setValue(Ext.Date.format(newValue, me.getDateFormat() || Ext.util.Format.defaultDateFormat));
        } else {
            me.getComponent().setValue('');
        }

        if (newValue !== oldValue) {
            me.fireEvent('change', me, newValue, oldValue);
        }

        this[valueValid && this.isDirty() ? 'showClearIcon' : 'hideClearIcon']();

        this.syncEmptyCls();
    },

    applyPicker: function (picker) {
        picker = Ext.factory(picker, 'Common.control.picker.Time');
        picker.setHidden(true); // Do not show picker on creation
        Ext.Viewport.add(picker);
        return picker;
    },

    updatePicker: function (picker) {
        picker.on({
            scope: this,
            change: 'onPickerChange',
            hide: 'onPickerHide'
        });
        picker.setValue(this.getValue());
        return picker;
    },

    applyIcon: function(icon) {
        this.setIconCls(icon);
        return icon;
    },

    setIconCls: function(action) {
        var icon = this.getComponent().icon;
        if(action === 'start') {
            icon.removeCls('x-stop-icon');
            icon.addCls('x-start-icon');
        }

        if(action === 'stop') {
            icon.removeCls('x-start-icon');
            icon.addCls('x-stop-icon');
        }

        if(action === false) {
            icon.removeCls('x-start-icon');
            icon.removeCls('x-stop-icon');
        }
    },

    onTimePickerStartStop: function(timePicker, action, event) {
        var me = this;
        if(action === 'start') {
            this.fireEvent('timepickerstart', me, event);
        }

        if(action === 'stop') {
            this.fireEvent('timepickerstop', me, event);
        }
    }
});
