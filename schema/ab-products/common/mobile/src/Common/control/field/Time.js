/**
 * Time picker with format label displayed on the left side of the input component.
 */
Ext.define('Common.control.field.Time', {
    extend: 'Common.control.field.TimePicker',

    xtype: 'timefield',

    config: {
        timeFormatLabel: LocaleManager.getLocalizedString('hh:mm', 'Common.control.field.Time')
    },

    initialize: function(){
        this.getComponent().mask.dom.innerHTML = this.getTimeFormatLabel();

        if(this.getLabelAlign() === 'top'){
            this.addCls('ab-time-field-long-form');
        }else{
            this.addCls('ab-time-field-short-form');
        }
        this.callParent(arguments);
    }
});
