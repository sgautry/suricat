/**
 * A panel for entering document type answers.
 * @since 23.2
 * @author Cristina Moldovan
 */
Ext.define('Common.control.question.Document', {
    extend: 'Ext.Panel',
    xtype: 'documentquestion',

    requires: ['Common.view.navigation.ViewSelector'],

    config: {
        /**
         * @config {Boolean} layout `hbox` to display the question text and options inline.
         */
        layout: 'vbox',

        cls: ['ab-question'],

        /**
         * @config Question text
         */
        text: '',

        /**
         * @cfg {Boolean} required `true` to make this field required.
         *
         * __Note:__ this only causes a visual indication.
         *
         * Doesn't prevent user from submitting the form.
         * @accessor
         */
        required: false,

        /**
         * @cfg {String} requiredCls The `className` to be applied to this Field when the {@link #required} configuration is set to `true`.
         * @accessor
         */
        requiredCls: 'ab-question-required',

        /**
         * @cfg {Common.view.navigation.ViewSelector} viewSelectorConfig.
         * Configuration of the Documents view selector.
         */
        viewSelectorConfig: {
            xtype: 'viewselector',
            navigationView: null,
            displayViews: true,
            cls: ['ab-question-text'],
            items: [
                {
                    text: LocaleManager.getLocalizedString('Documents', 'Common.control.question.Document'),
                    documentSelect: true,
                    view: null,
                    store: null
                }
            ]
        },

        items: [
            {
                xtype: 'label',
                html: '',
                itemId: 'questionText',
                tag: 'span',
                cls: ['ab-question-text']
            }
        ]
    },

    initialize: function () {
        var me = this,
            labelField = me.down('#questionText'),
            viewSelectorConfig = me.getViewSelectorConfig();

        // set question text
        if (labelField !== null) {
            labelField.setHtml('<span class="ab-question-text-span">' + me.getText() + '</span>');
            if (me.getLayout().getOrient() === 'vertical') {
                labelField.addCls('ab-question-text-long-form');
            } else {
                labelField.addCls('ab-question-text-short-form');
            }
        }

        me.callParent(arguments);

        me.add(Ext.factory(viewSelectorConfig, 'Ext.Component'));
    },

    /**
     * Applies the view selector config parameter to this view selector config.
     * @param config
     * @returns {*}
     */
    applyViewSelectorConfig: function(config){
        return Ext.merge(this.config.viewSelectorConfig, config);
    },

    /**
     * Updates the {@link #required} configuration.
     * @private
     */
    updateRequired: function (newRequired) {
        var labelField = this.down('#questionText');
        labelField[newRequired ? 'addCls' : 'removeCls'](this.getRequiredCls());
    },

    /**
     * Updates the {@link #required} configuration
     * @private
     */
    updateRequiredCls: function (newRequiredCls, oldRequiredCls) {
        var labelField = this.down('#questionText');
        if (this.getRequired()) {
            labelField.replaceCls(oldRequiredCls, newRequiredCls);
        }
    }
});