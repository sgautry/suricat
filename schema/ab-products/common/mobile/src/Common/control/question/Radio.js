/**
 * A multiple choice single answer question which uses Common.control.field.Radio fields.
 * @since 23.2
 * @author Ana Albu
 */
Ext.define('Common.control.question.Radio', {
    extend: 'Ext.Panel',
    xtype: 'radioquestion',

    requires: ['Common.control.field.Radio'],

    config: {
        /**
         * @config {Boolean} layout `hbox` to display the question text and options inline.
         */
        layout: 'vbox',

        cls: ['ab-question'],

        items: [
            {
                xtype: 'label',
                html: '',
                itemId: 'questionText',
                tag: 'span',
                cls: ['ab-question-text']
            }

        ],

        /**
         * @config Question text
         */
        text: '',

        /**
         * @config Radio field options: array of items containing value and label and optionally checked or other radio field config values.
         * The name is set dynamically to ensure group membership.
         * For example:
         * options: [
         {
             value: 'red',
             label: 'Red',
             checked: true
         },
         {
             value: 'green',
             label: 'Green'
         },
         {
             value: 'blue',
             label: 'Blue'
         }
         ]
         */
        options: [],

        /**
         * @cfg {Boolean} required `true` to make this field required.
         *
         * __Note:__ this only causes a visual indication.
         *
         * Doesn't prevent user from submitting the form.
         * @accessor
         */
        required: false,

        /**
         * @cfg {String} requiredCls The `className` to be applied to this Field when the {@link #required} configuration is set to `true`.
         * @accessor
         */
        requiredCls: 'ab-question-required'
    },

    initialize: function () {
        var me = this,
            labelField = me.down('#questionText'),
            options = me.getOptions(),
            groupName = me.getId() + '_radiofield',
            radioField;

        // set question text
        if (labelField !== null) {
            labelField.setHtml('<span class="ab-question-text-span">' + me.getText() + '</span>');
            if (me.getLayout().getOrient() === 'vertical') {
                labelField.addCls('ab-question-text-long-form');
            } else {
                labelField.addCls('ab-question-text-short-form');
            }
        }

        Ext.each(options, function (option) {
            option.name = groupName;
            if(Ext.isEmpty(option.label)){
                // force creation of the label element to keep left alignment of the radio button
                option.label = '&nbsp;';
            }
            if (me.getLayout().getOrient() !== 'vertical') {
                option.flex = 1;
            }
            radioField = Ext.create('Common.control.field.Radio', option);
            radioField.addListener({
                change: me.onChangeOption,
                scope: me
            });
            me.add(radioField);
        });

        me.callParent(arguments);
    },

    onChangeOption: function (optionField, newChecked, oldChecked) {
        this.fireEvent('change', optionField, newChecked, oldChecked);
    },

    /**
     * Updates the {@link #required} configuration.
     * @private
     */
    updateRequired: function (newRequired) {
        var labelField = this.down('#questionText');
        labelField[newRequired ? 'addCls' : 'removeCls'](this.getRequiredCls());
    },

    /**
     * Updates the {@link #required} configuration
     * @private
     */
    updateRequiredCls: function (newRequiredCls, oldRequiredCls) {
        var labelField = this.down('#questionText');
        if (this.getRequired()) {
            labelField.replaceCls(oldRequiredCls, newRequiredCls);
        }
    },

    getValue: function () {
        var me = this,
            checkButtons = me.query('commonradiofield'),
            selectedValue = null;

        for (var i = 0; i < checkButtons.length; i++) {
            if (checkButtons[i].getComponent().getChecked() === true) {
                selectedValue = checkButtons[i].getValue();
                break;
            }
        }

        return selectedValue;
    },

    setValue: function (optionValue) {
        var me = this,
            checkButtons = me.query('commonradiofield'),
            i;

        for (i = 0; i < checkButtons.length; i++) {
            if (checkButtons[i].getValue().toString() === optionValue.toString()) {
                checkButtons[i].setChecked(true);
                break;
            }
        }
    },

    /**
     * Uncheck all checkboxes.
     */
    reset: function () {
        var me = this,
            checkButtons = me.query('commonradiofield');

        Ext.each(checkButtons, function (checkboxButton) {
            checkboxButton.uncheck();
        });
    }

});