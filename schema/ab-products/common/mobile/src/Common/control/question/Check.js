/**
 * A multiple choice multi answers question which uses Common.control.field.Checkbox fields.
 * @since 23.2
 * @author Ana Albu
 */
Ext.define('Common.control.question.Check', {
    extend: 'Ext.Panel',
    xtype: 'checkquestion',

    requires: ['Common.control.field.Checkbox'],

    config: {
        /**
         * @config {Boolean} layout `hbox` to display the question text and options inline.
         */
        layout: 'vbox',

        cls: ['ab-question'],

        items: [
            {
                xtype: 'label',
                html: '',
                itemId: 'questionText',
                cls: ['ab-question-text']
            }

        ],

        /**
         * @config Question text
         */
        text: '',

        /**
         * @config Checkbox field options: array of items containing value and label and optionally checked or other checkbox field config values.
         * For example:
         * options: [
         {
             value: 'red',
             label: 'Red',
             checked: true
         },
         {
             value: 'green',
             label: 'Green'
         },
         {
             value: 'blue',
             label: 'Blue'
         }
         ]
         */
        options: [],

        /**
         * @cfg {Boolean} required `true` to make this field required.
         *
         * __Note:__ this only causes a visual indication.
         *
         * Doesn't prevent user from submitting the form.
         * @accessor
         */
        required: false,

        /**
         * @cfg {String} requiredCls The `className` to be applied to this Field when the {@link #required} configuration is set to `true`.
         * @accessor
         */
        requiredCls: 'ab-question-required'
    },

    // separator character for multiple values in one field
    MULTIPLE_VALUES_SEPARATOR: ', ',

    initialize: function () {
        var me = this,
            labelField = me.down('#questionText'),
            options = me.getOptions(),
            checkboxField;

        // set question text
        if (labelField !== null) {
            labelField.setHtml('<span class="ab-question-text-span">' + me.getText() + '</span>');
            if (me.getLayout().getOrient() === 'vertical') {
                labelField.addCls('ab-question-text-long-form');
            } else {
                labelField.addCls('ab-question-text-short-form');
            }

        }

        Ext.each(options, function (option) {
            option.labelAlign = 'right';
            option.cls = 'ab-checkbox-field';
            option.labelWidth = '100%';
            if (me.getLayout().getOrient() !== 'vertical') {
                option.flex = 1;
            }
            checkboxField = Ext.create('Common.control.field.Checkbox', option);
            checkboxField.addListener({
                change: me.onChangeOption,
                scope: me
            });
            me.add(checkboxField);
        });

        me.callParent(arguments);
    },

    onChangeOption: function (optionField, newChecked, oldChecked) {
        this.fireEvent('change', optionField, newChecked, oldChecked);
    },

    /**
     * Updates the {@link #required} configuration.
     * @private
     */
    updateRequired: function (newRequired) {
        var labelField = this.down('#questionText');
        labelField[newRequired ? 'addCls' : 'removeCls'](this.getRequiredCls());
    },

    /**
     * Updates the {@link #required} configuration
     * @private
     */
    updateRequiredCls: function (newRequiredCls, oldRequiredCls) {
        var labelField = this.down('#questionText');
        if (this.getRequired()) {
            labelField.replaceCls(oldRequiredCls, newRequiredCls);
        }
    },

    /**
     * Get selected values.
     * @returns {Array} values of checked options
     */
    getValues: function () {
        var me = this,
            checkButtons = me.query('commoncheckbox'),
            selectedValues = [];

        for (var i = 0; i < checkButtons.length; i++) {
            if (checkButtons[i].getComponent().getChecked() === true) {
                selectedValues.push(checkButtons[i].getValue());
            }
        }

        return selectedValues;
    },

    getValue: function () {
        return this.getValues().join(this.MULTIPLE_VALUES_SEPARATOR);
    },

    setValues: function (optionValues) {
        var me = this,
            checkButtons = me.query('commoncheckbox'),
            i, j;

        for (i = 0; i < checkButtons.length; i++) {
            for (j = 0; j < optionValues.length; j++) {
                if (checkButtons[i].getValue().toString() === optionValues[j].toString()) {
                    checkButtons[i].setChecked(true);
                }
            }
        }
    },

    setValue: function (optionValues) {
        this.setValues(optionValues);
    },


    /**
     * Uncheck all checkboxes.
     */
    reset: function () {
        var me = this,
            checkButtons = me.query('commoncheckbox');

        Ext.each(checkButtons, function (checkboxButton) {
            checkboxButton.uncheck();
        });
    }

});