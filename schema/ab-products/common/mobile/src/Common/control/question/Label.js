/**
 * A label type question.
 * It can be used as header for multiple choice questions
 * by setting the {@link Common.control.question.Label.config.options} property.
 *
 * @since 23.2
 * @author Cristina Moldovan
 */
Ext.define('Common.control.question.Label', {
    extend: 'Ext.Panel',
    xtype: 'labelquestion',

    config: {
        /**
         * @config {Boolean} layout `vbox` to display the question text and options vertically.
         */
        layout: 'hbox',

        cls: ['ab-label-type-question'],

        items: [
            {
                xtype: 'label',
                html: '',
                itemId: 'questionText'
            }
        ],

        /**
         * @config Question text
         */
        text: '',

        /**
         * @config Array of items containing the answer options text.
         *
         * For example:
         * options: [
         {
             html: 'Yes'
         },
         {
             html: 'No'
         },
         {
             html: 'N/A'
         }
         ]
         */
        options: []
    },

    initialize: function () {
        var me = this,
            questionTextField = me.down('#questionText'),
            options = me.getOptions(),
            optionField;

        // set question text
        if (questionTextField !== null) {
            questionTextField.setHtml(me.getText());
            if (me.getLayout().getOrient() === 'vertical'
            || options.length === 0) {
                questionTextField.addCls('ab-question-text-long-form');
            } else {
                questionTextField.addCls('ab-question-text-short-form');
            }
        }

        Ext.each(options, function (option) {
            if (me.getLayout().getOrient() !== 'vertical') {
                option.flex = 1;
            }
            optionField = Ext.create('Ext.Label', option);
            optionField.addCls('ab-label-type-question-option');
            me.add(optionField);
        });

        me.callParent(arguments);
    }
});