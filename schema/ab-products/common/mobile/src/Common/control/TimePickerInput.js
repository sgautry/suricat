Ext.define('Common.control.TimePickerInput', {
    extend: 'Ext.field.Input',
    xtype: 'timepickerinput',

    // @private
    getTemplate: function() {
        // Template items
        return [
            {
                reference: 'input',
                tag: this.tag
            },
            {
                reference: 'mask',
                classList: [this.config.maskCls]
            },
            {
                reference: 'clearIcon',
                cls: 'x-clear-icon'
            },
            {
                reference: 'icon',
                cls: 'x-start-icon'
            }
        ];
    },

    /**
     * @override
     */
    initElement: function() {
        var me = this;

        me.callParent();

        me.input.on({
            scope: me,

            keyup: 'onKeyUp',
            keydown: 'onKeyDown',
            focus: 'onFocus',
            blur: 'onBlur',
            input: 'onInput',
            paste: 'onPaste',
            tap: 'onInputTap'
        });

        me.mask.on({
            scope: me,
            tap: 'onMaskTap'
        });

        if (me.clearIcon) {
            me.clearIcon.on({
                tap: 'onClearIconTap',
                touchstart: 'onClearIconPress',
                touchend: 'onClearIconRelease',
                scope: me
            });

            me.clearIcon.dom.style.right = '2em';
        }


        me.icon.on({
            scope: me,
            tap: 'onIconTap'
        });


        // Hack for IE10. Seems like keyup event is not fired for 'enter' keyboard button, so we use keypress event instead to handle enter.
        if(Ext.browser.is.ie && Ext.browser.version.major >=10){
            me.input.on({
                scope: me,
                keypress: 'onKeyPress'
            });
        }
    },


    onIconTap: function(e) {
        var action = '';
        e.stopPropagation();
        e.preventDefault();

        if(this.icon.dom.className === 'x-start-icon') {
            action = 'start';
        }

        if(this.icon.dom.className === 'x-stop-icon') {
            action = 'stop';
        }

        if(action !== '') {
            this.fireEvent('timepickerstartstop', this, action,  e);
        }

    },

    applyReadOnly: function(readOnly) {
        if(readOnly) {
            this.icon.addCls('x-timepicker-hide-icon');
        } else {
            this.icon.removeCls('x-timepicker-hide-icon');
        }

        return readOnly;
    }
});