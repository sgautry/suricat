Ext.define('Common.device.ScreenCapture', {
    singleton: true,

    /**
     * Performs a screen capture.
     * @returns {Promise} A Promise resolved with the dataURI representation of the caputured screen contents as a JPG file.
     */
    getURI: function() {
        return new Promise(function(resolve, reject) {
            navigator.screenshot.URI(function(err, res) {
                if(err) {
                    reject(err);
                } else {
                    resolve(res.URI);
                }
            });
        });
    },

    getFile: function() {
        return new Promise(function(resolve, reject) {
            navigator.screenshot.save(function(err, res) {
                if(err) {
                    reject(err);
                } else {
                    resolve(res.filePath);
                }
            });
        });
    }

});