Ext.define('Common.device.Camera', {
    singleton: true,

    imageQuality: 15,

    getPicture: function() {
        var config = {
            quality: Common.device.Camera.imageQuality,
            destinationType: Camera.DestinationType.DATA_URL,
            applicationName: Common.Application.appName,
            correctOrientation: true,
            targetWidth: 500,
            targetHeight: 500
        };

        return new Promise(function(resolve, reject) {
            navigator.camera.getPicture(resolve, reject, config);
        });
    },

    getPictureFromPhotoLibrary: function() {
        return new Promise(function(resolve, reject) {
            var config = {
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM
            };

            navigator.camera.getPicture(resolve, reject, config);
        });
    }


});