/**
 * Helper functions used to display documents
 * @since 21.3
 */
Ext.define('Common.document.DocumentManager', {
    alternateClassName: ['DocumentManager'],
    singleton: true,

    mixins: ['Common.promise.util.DatabaseOperation'],

    requires: [
        'Common.env.Feature',
        'Common.service.document.Document',
        'Common.device.File'
    ],

    msgBoxTitle: LocaleManager.getLocalizedString('Document Display', 'Common.document.DocumentManager'),
    featureNotAvailableMsg: LocaleManager.getLocalizedString('Feature not available in the desktop environment.', 'Common.document.DocumentManager'),
    fileNotFoundMessage: LocaleManager.getLocalizedString('File does not exist', 'Common.document.DocumentManager'),

    /**
     * Displays the photos on the Document list
     * @param imageData
     * @param isDisplayOnly
     */
    displayImagePanel: function (imageData, isDisplayOnly, cameraControl) {
        if (!cameraControl) {
            cameraControl = Ext.create('Common.control.Camera');
        }
        cameraControl.setImageData(imageData);
        cameraControl.setDisplayOnly(isDisplayOnly);
        cameraControl.showPanel();
    },

    isPhotoDocument: function (fileName) {
        var photoRe = /\.png|\.gif|\.jpg|\.jpeg|\.tiff|\.tif/;
        return photoRe.test(fileName.toLowerCase());
    },

    isOfficeOrPdfDocument: function (fileName) {
        var docRe = /\.doc|\.docx|\.xls|\.xlsx|\.pdf|\.txt/;
        return docRe.test(fileName.toLowerCase());
    },

    isDocumentValidToDisplay: function (fileName) {
        var isValid = this.isPhotoDocument(fileName) || this.isOfficeOrPdfDocument(fileName),
            msg = LocaleManager.getLocalizedString('File {0} cannot be displayed on this device.', 'Common.document.DocumentManager');

        if (!isValid) {
            Ext.Msg.alert(this.msgBoxTitle, Ext.String.format(msg, fileName));
        }
        return isValid;
    },

    displayDocument: function (documentData, fileName) {
        var genericErrorMessage = LocaleManager.getLocalizedString('An error occured while displaying the document', 'Common.document.DocumentManager');

        //<debug>
        var msg = LocaleManager.getLocalizedString('File {0} cannot be displayed when in Desktop mode.', 'Common.document.DocumentManager'),
            index,
            fileExtension,
            url;

        if (!Common.env.Feature.isNative) {
            // Get the file extension to use as the Mime type.
            // This implementation is a bit fragile in that it depends on the filename containing a single '.' character
            index = fileName.indexOf('.');
            if (index > -1) {
                fileExtension = fileName.substring(index + 1, fileName.length);
                url = 'data:application/' + fileExtension + ';base64,' + documentData;
                window.open(url);
            } else {
                Ext.Msg.alert(this.msgBoxTitle, Ext.String.format(msg, fileName));
            }
            return;
        }
        //</debug>

        /**
         * Converts the base64 document data to a binary file and displays the file.
         */
        DocumentConverter.convertBase64ToBinary(documentData, fileName, function (filePath) {
            // Display the document in the inAppBrowser if we are on an iOS device
            // Android will launch an intent to display the file
            // Windows Phone will launch the required app after converting the data to binary.
            if (Ext.os.is.ios) {
                window.open('file:///' + filePath, '_blank', 'location=no,EnableViewPortScale=yes');
            }
        }, function (error) {
            if (Ext.isEmpty(error)) {
                error = genericErrorMessage;
            }
            Ext.Msg.alert('', error);
        });
    },

    displayDocumentOrImage: function (documentData, fileName, cameraPanel) {
        var me = this;
        // Check if the document can be displayed on the device?
        if (me.isDocumentValidToDisplay(fileName)) {
            if (me.isPhotoDocument(fileName) && !Ext.os.is.ios) {
                me.displayImagePanel(documentData, true, cameraPanel);
            } else {
                me.displayDocument(documentData, fileName);
            }
        }
    },

    /**
     * Checks out an ARCHIBUS document and saves the document on the device file system or databasse.
     * @param {String} documentTableName The name of the table containing the document
     * @param {String} documentFieldName The document field name
     * @param {String} documentFileName The document file name as saved in Web Central
     * @param {Object} primaryKey An object containing the primary key fields and values. Example {ls_id:102}
     * @param {Ext.data.Store} store The store containing the document fields.
     * @returns {Promise} A Promise object resolved to true when the operation is completed.
     */
    downloadOnDemandDocument: function (documentTableName, documentFieldName, documentFileName, primaryKey, store) {
        var downloadMessage = LocaleManager.getLocalizedString('Downloading Document', 'Common.document.DocumentManager'),
            clientTable = store.getModel().getTableName();

        var getPrimaryKeyValue = function () {
            var primaryKeyFields = store.getDocumentConfig().documentTableKeys,
                primaryKeyValues = [];

            primaryKeyFields.forEach(function (keyField) {
                primaryKeyValues.push(primaryKey[keyField]);
            });

            return primaryKeyValues.join('|');
        };

        var downloadedDocument = {};
        downloadedDocument.isDownloaded = false;

        return Network.checkNetworkConnectionAndLoadDwrScripts(false)
            .then(function (isConnected) {
                if (isConnected) {
                    Mask.displayLoadingMask(downloadMessage);
                    return store.getClientDocuments()
                        .then(function (clientDocuments) {
                            store.clientDocuments = clientDocuments;
                            return Common.service.Session.start();
                        })
                        .then(function () {
                            // Get the version of the document on the server
                            var primaryKeyValue = getPrimaryKeyValue();
                            return MobileSyncServiceAdapter.getDocumentVersion(documentTableName, documentFieldName, primaryKeyValue);
                        })
                        .then(function (serverDocumentInfo) {
                            var primaryKeyValue = getPrimaryKeyValue(),
                                serverDocumentVersion = 0,
                                clientDocumentVersion = 0,
                                clientDocument;

                            if (!Ext.isEmpty(serverDocumentInfo)) {
                                serverDocumentVersion = serverDocumentInfo.version;
                            }

                            // Get the version from the clientDocuments object
                            clientDocument = store.clientDocuments[primaryKeyValue];
                            if (!Ext.isEmpty(clientDocument)) {
                                clientDocumentVersion = clientDocument[documentFieldName + '_version'];
                            }
                            if (serverDocumentVersion > clientDocumentVersion) {
                                return Common.document.DocumentManager.retrieveDocumentAndDownload(primaryKey, documentTableName, documentFieldName, documentFileName, clientTable, serverDocumentVersion);
                            } else {
                                return Promise.resolve(downloadedDocument);
                            }
                        })
                        .then(function (documentObject) {
                            downloadedDocument = documentObject;
                            return Common.service.Session.end();
                        })
                        .then(function () {
                            Mask.hideLoadingMask();
                            return Promise.resolve(downloadedDocument);
                        }, function (error) {
                            Ext.Msg.alert('', error);
                            Common.service.Session.end()
                                .then(function () {
                                    Mask.hideLoadingMask();
                                    return Promise.reject();
                                });
                        });
                } else {
                    Network.displayConnectionMessage();
                    return Promise.reject();
                }
            });
    },

    retrieveDocumentAndDownload: function (primaryKey, documentTableName, documentFieldName, documentFileName, clientTable, documentVersion) {
        var documentFolder = GlobalParameters.getUserDocumentFolder() + '/' + clientTable,
            fileUri,
            targetFolder;

        return Common.service.document.Document.retrieveDocumentURL(primaryKey, documentTableName, documentFieldName, documentFileName)
            .then(function (url) {
                Log.log('Document URL ' + url, 'info', null, 'downloadOnDemandDocument');
                targetFolder = documentFolder;
                fileUri = url;
                return Common.device.File.createDirectory(targetFolder);
            })
            .then(function () {
                var urlObject = {};
                Log.log('Start file download for URI ' + fileUri, 'info', null, 'downloadOnDemandDocument');
                urlObject.url = fileUri;
                return Common.document.DocumentManager.downloadDocumentFile(urlObject);
            })
            .then(function (documentObject) {
                // Save the downloaded document
                documentObject.field_name = documentFieldName;
                documentObject.keys_object = primaryKey;
                documentObject.doc_file = documentFileName;
                documentObject.table_name = documentTableName;
                documentObject.version = documentVersion;
                documentObject.isDownloaded = true;
                return Common.document.DocumentManager.saveDocument(clientTable, documentObject);
            });
    },

    getOrigin: function () {
        return Ext.String.format('{0}//{1}', document.location.protocol, document.location.host);
    },

    /**
     * Downloads a file from a server.
     * @param {String} fileURI The fileURI of the file to download. The URI can be a path to the Web Central
     * server or to any other server.
     * @param {String} targetFileName The file name to use when the device is written to the device.
     * @param {String} targetFolder The device folder where the file will be saved to. Example 'AbOnDemandDocuments/tmpFiles'. The
     * actual file location depends on the platform.
     * @returns {Promise} A Promise object that is resolved when the operation is completed.
     */
    downloadFileWithURI: function (fileURI, targetFileName, targetFolder) {

        //<debug>
        if (Ext.os.is.Desktop) {
            Ext.Msg.alert('', this.featureNotAvailableMsg);
            return Promise.reject('Feature not available');
        }
        //</debug>

        return Network.checkNetworkConnectionAndLoadDwrScripts(false)
            .then(function (isConnected) {
                if (isConnected) {
                    return Common.device.File.downloadFile(fileURI, targetFileName, targetFolder);
                } else {
                    Network.displayConnectionMessage();
                    return Promise.reject();
                }
            });

    },

    displayFile: function (filePath) {
        var me = this,
            deviceFilePath;

        Common.device.File.fileExists(filePath)
            .then(function (fileEntry) {
                deviceFilePath = fileEntry.nativeURL;
                if (Ext.os.is.ios) {
                    window.open(deviceFilePath, '_blank', 'location=no,EnableViewPortScale=yes');
                } else if (Ext.os.is.android || Ext.os.is.WindowsPhone) {
                    // Remove the file:/// prefix from the file
                    deviceFilePath = Ext.os.is.Android ? deviceFilePath.replace('file:///', '') : filePath;
                    DocumentConverter.displayFileUsingIntent(deviceFilePath, Ext.emptyFn, function (error) {
                        var errorMessage = (error === 'FileNotFound') ? me.fileNotFoundMessage : error;
                        Ext.Msg.alert('', errorMessage);
                    });
                }
            }, function () {
                Ext.Msg.alert('', me.fileNotFoundMessage);
            });
    },

    deleteDocumentFile: function (store, record, documentFieldName) {
        var me = this,
            useFileStorage = GlobalParameters.useFileStorage(),
            documentTable = store.getDocumentConfig().documentTable,
            documentFileName = record.get(documentFieldName),
            primaryKeyFields = store.getDocumentConfig().documentTableKeys,
            primaryKey = me.generatePrimaryKeyObject(primaryKeyFields, record),
            folder = GlobalParameters.documentFolder + '/' + ConfigFileManager.username + '/' + store.getModelTableName(),
            fileName = DocumentManager.generateFileName(documentTable, documentFieldName, documentFileName, primaryKey);

        // Always resolve the Promise
        return new Promise(function (resolve) {
            if (useFileStorage) {
                Common.device.File.deleteFile(folder + '/' + fileName)
                    .then(resolve, resolve);
            } else {
                resolve();
            }

        });
    },

    /**
     * @private
     */
    generatePrimaryKeyObject: function (keyFields, record) {
        var primaryKey = {};

        Ext.each(keyFields, function (keyField) {
            primaryKey[keyField] = record.get(keyField);
        });

        return primaryKey;
    },


    /**
     * Marks the document for deletion if a network connection is available. If the network is not available
     * the _contents field is set to MARK_DELETED. The document will be deleted on the next sync.
     * @param store
     * @param record
     * @param documentField
     */
    markDocumentDeleted: function (store, record, documentField) {
        var me = this,
            keyValues,
            documentTableName,
            removeDocumentMessage = LocaleManager.getLocalizedString('Removing Document', 'Common.document.DocumentManager'),
            documentConfig = store.getDocumentConfig();


        keyValues = me.generatePrimaryKeyObject(documentConfig.documentTableKeys, record);
        documentTableName = documentConfig.documentTable;

        return Network.checkNetworkConnectionAndLoadDwrScripts(false)
            .then(function (isConnected) {
                if (isConnected) {
                    Mask.displayLoadingMask(removeDocumentMessage);
                    return Common.service.Session.start()
                        .then(function () {
                            return Common.service.document.Document.markDocumentDeleted(keyValues, documentTableName, documentField);
                        })
                        .then(function () {
                            return Common.service.Session.end();
                        })
                        .then(function () {
                            Mask.hideLoadingMask();
                            return me.setDocumentContents(store, record, documentField, false);
                        })
                        .then(null, function (error) {
                            Mask.hideLoadingMask();
                            return Common.service.Session.end()
                                .then(function () {
                                    return Promise.reject(error);
                                });  // Close the session in case of error, Common.service.Session.end() always resolves.
                        });

                } else {
                    Ext.Msg.alert('', 'The network is not available. The document will be removed on the next sync.');
                    return me.setDocumentContents(store, record, documentField, true);
                }
            });

    },

    setDocumentContents: function (store, record, documentField, markForSyncDeletion) {
        return new Promise(function (resolve, reject) {
            var autoSync = store.getAutoSync();

            if (autoSync) {
                store.setAutoSync(false);
            }
            record.set(documentField, markForSyncDeletion ? documentField : null);
            record.set(documentField + '_contents',
                markForSyncDeletion ? Common.config.GlobalParameters.MARK_DELETED_TEXT : null);
            record.set(documentField + '_isnew', markForSyncDeletion);
            record.set(documentField + '_file', '');
            record.set(documentField + '_version', 0);

            store.sync(function () {
                store.setAutoSync(autoSync);
                resolve(record);
            }, function () {
                store.setAutoSync(autoSync);
                reject('Error syncing store ' + store.getStoreId());
            });
        });
    },

    deleteDocument: function (store, record, documentField) {
        var me = this,
            keyValues,
            documentTableName,
            documentConfig = store.getDocumentConfig();

        keyValues = me.generatePrimaryKeyObject(documentConfig.documentTableKeys, record);
        documentTableName = documentConfig.documentTable;

        return Common.service.document.Document.markDocumentDeleted(keyValues, documentTableName, documentField);
    },

    downloadDocumentFile: function (record) {
        return new Promise(function (resolve) {
            var xhr = new XMLHttpRequest(),
                url = window.location.origin + record.url;

            Log.log(url, 'info', Common.document.DocumentManager, 'downloadDocumentFile');
            Log.log(url, 'info');
            xhr.open('GET', url, true);
            xhr.responseType = 'blob';

            xhr.onload = function () {
                var blob;
                if (this.status === 200) {
                    blob = this.response;
                    Common.device.File.convertBlobToBase64(blob)
                        .then(function (base64Data) {
                            if (base64Data.length === 0) {
                                record.base64Data = '';
                            } else {
                                record.base64Data = base64Data.split(',')[1];
                            }
                            resolve(record);
                        });
                } else {
                    // Don't abort sync on failure to download document.
                    record.base64Data = '';
                    resolve(record);
                }
            };

            xhr.send();
        });
    },

    /**
     * Generates a unique file name for a document file
     * @param {String} tableName The document table name
     * @param {String} documentField The document field name
     * @param {String} documentFileName The document file name in Web Central
     * @param {Object} key An object containing the primary key fields and values
     * @returns {String}
     */
    generateFileName: function (tableName, documentField, documentFileName, key) {
        var keyValues = '',
            value;

        var docFileName = Ext.isEmpty(documentFileName) ? '' : documentFileName.replace(/\\/g, '_').replace(/:/g, '_');

        for (value in key) {
            keyValues += key[value] + '-';
        }

        return Ext.String.format('{0}{1}-{2}-{3}', keyValues, tableName, documentField, docFileName);
    },


    generateDocumentFileName: function (docObject) {
        return DocumentManager.generateFileName(docObject.table_name, docObject.field_name, docObject.doc_file, docObject.keys_object);
    },

    saveDocument: function (clientTableName, docObject) {
        var docFileName = Common.document.DocumentManager.generateDocumentFileName(docObject),
            documentFolder = GlobalParameters.getUserDocumentFolder(),
            filePath = documentFolder + '/' + clientTableName + '/' + docFileName,
            useFileStorage = GlobalParameters.useFileStorage();

        return Promise.resolve()
            .then(function () {
                if (useFileStorage) {
                    return Common.device.File.writeFile(filePath, docObject.base64Data);
                } else {
                    return Promise.resolve();
                }
            })
            .then(function () {
                docObject.clientFile = useFileStorage ? docFileName : '';
                return Common.document.DocumentManager.updateDocumentFileAndVersion(docObject, clientTableName);
            })
            .then(function () {
                return Promise.resolve(docObject);
            }, function (error) {
                return Promise.reject(error);
            });
    },

    saveDocuments: function (fileDataArray, clientTableName) {
        var saveFiles = function (fileDataArray) {
            var p = Promise.resolve();
            fileDataArray.forEach(function (fileData) {
                p = p.then(function () {
                    return Common.document.DocumentManager.saveDocument(clientTableName, fileData);
                });
            });
            return p;
        };

        return saveFiles(fileDataArray);
    },

    saveDocumentsToStorage: function (fileDataArray, clientTableName) {
        var documentFolder = GlobalParameters.getUserDocumentFolder() + '/' + clientTableName;

        return Common.device.File.createDirectory(documentFolder)
            .then(function () {
                return Common.document.DocumentManager.saveDocuments(fileDataArray, clientTableName);
            });
    },

    updateDocumentFileAndVersion: function (docObject, tableName) {
        var versionField = docObject.field_name + '_version',
            clientFileField = docObject.field_name + '_file',
            contentsField = docObject.field_name + '_contents',
            isNewField = docObject.field_name + '_isnew',
            useFileStorage = GlobalParameters.useFileStorage(),
            documentData = '',
            sql,
            values,
            isAndroid = Ext.os.is.Android,
            keysObject = docObject.keys_object;

        var getWhereClause = function () {
            var p,
                whereClause = [],
                clientKeysObject = {},
                clientKeyValues;

            if(docObject.clientTableKeys && docObject.clientTableKeys.length > 0) {
                clientKeyValues = docObject.pkey_value.split('|');
                docObject.clientTableKeys.forEach(function(key, index) {
                    clientKeysObject[key] = clientKeyValues[index];
                });
                keysObject = clientKeysObject;
            }

            for (p in keysObject) {
                whereClause.push(p + "='" + keysObject[p] + "'");
            }

            return whereClause.join(' AND ');
        };

        if (!useFileStorage) {
            if (isAndroid && (docObject.base64Data.length > GlobalParameters.ANDROID_MAX_FIELD_SIZE)) {
                documentData = Common.util.ImageData.fileNotAvailableImage;
                Common.controller.EventBus.fireFieldSizeLimitExceeded(docObject.table_name, docObject.field_name, keysObject, docObject.base64Data.length);
            } else {
                documentData = docObject.base64Data;
            }
        }

        sql = 'UPDATE ' + tableName + ' SET ' + isNewField + '=?,' + versionField + '=?,' + clientFileField + '=?,' + contentsField + '=? WHERE ' + getWhereClause();
        values = [false, docObject.version, docObject.clientFile, documentData];

        return new Promise(function (resolve, reject) {
            var db = SqliteConnectionManager.getConnection();
            db.transaction(function (tx) {
                tx.executeSql(sql, values, resolve, function (tx, error) {
                    reject(error.message);
                });
            });
        });
    },

    /**
     * Resets the value of all client database document version fields to 0
     */
    resetDocumentVersionFields: function () {
        var me = this,
            generateColumnUpdate = function (columns) {
                var columnSql = columns.map(function (column) {
                    return column + '=?';
                });
                return columnSql.join(',');
            };

        return me.getTablesAndVersionFields()
            .then(function (tables) {
                var sqlStatements = [];

                tables.forEach(function (table) {

                    var sql = 'UPDATE ' + table.name + ' SET ' + generateColumnUpdate(table.columns),
                        values = table.columns.map(function () {
                            return 0;
                        });
                    sqlStatements.push({sql: sql, values: values});
                });

                return me.executeUpdate(sqlStatements);
            });
    },


    /**
     * @private
     * @param sqlStatements
     * @returns {*}
     */
    executeUpdate: function (sqlStatements) {
        return new Promise(function (resolve) {
            var db = SqliteConnectionManager.getConnection(),
                executed = 0,
                totalRecords = sqlStatements.length;

            db.transaction(function (tx) {
                sqlStatements.forEach(function (sqlStatement) {
                    tx.executeSql(sqlStatement.sql, sqlStatement.values, function () {
                        executed++;
                        if (executed === totalRecords) {
                            resolve();
                        }
                    }, function () {
                        executed++;
                        if (executed === totalRecords) {
                            resolve();
                        }
                    });
                });
            });
        });
    },

    createTempDocumentTable: function () {
        return new Promise(function (resolve, reject) {
            var db = SqliteConnectionManager.getConnection(),
                sql = 'CREATE TABLE IF NOT EXISTS TmpDocs(id integer PRIMARY KEY, doc_field TEXT, doc_contents TEXT, pkey TEXT)';

            db.transaction(function (tx) {
                tx.executeSql(sql, null, resolve, reject);
            });
        });
    }
});