/**
 * Encapsulates details of MobileSecurityService, SecurityService, SmartClientConfigService, AdminService calls.
 * Translates service exceptions using ExceptionTranslator.
 *
 * Includes the Promise API for the DWR security API
 *
 * @author Valery Tydykov
 * @author Jeff Martin
 * @since 21.1
 */
Ext.define('Common.service.MobileSecurityServiceAdapter', {
    singleton: true,
    alternateClassName: ['MobileSecurityServiceAdapter'],

    requires: [
        'Common.service.ExceptionTranslator',
        'Common.security.Security',
        'Common.controller.EventBus'
    ],

    mixins: ['Common.service.MobileServiceErrorHandler'],

    options: {
        async: true,
        headers: {"cache-control": "no-cache"},
        callback: Ext.emptyFn,
        errorHandler: Ext.emptyFn
    },

    /**
     * @property {Boolean} throwExceptionOnError controls if an exception is thrown when an error occurs
     * in the service. Setting the value to true allows the calling routine to handle the error.
     */
    throwExceptionOnError: true,

    resultObject: {
        success: false,
        exception: null,
        parameter: null
    },

    /**
     * *********************** Promise API Methods ***************************
     * The methods in this section return Promise objects and are used to
     * simply using asynchronous calls
     *
     */

    startSession: function (deviceId, localeName) {
        var me = this;
        return me.isSsoMode()
            .then(function (isSsoMode) {
                if (isSsoMode) {
                    return me.startSsoSession();
                } else {
                    return me.startSessionForDeviceId(deviceId, false);
                }
            })
            .then(function () {
                return me.setLocaleForSession(localeName);
            });
    },

    startSessionForDeviceId: function (deviceId, isRegistrationCheck) {
        var me = this,
            resolveFunc,
            rejectFunc,
            errorHandler = function (message, exception) {
                // Check for Device Not Registered error
                if (exception && exception.errorNumber === 23) {
                    Common.controller.EventBus.fireDeviceNotRegistered();
                }
                me.errorHandlerFunction(rejectFunc, message, exception);
            },
            registrationCheckErrorHandler = function (message, exception) {
                rejectFunc(exception);
            };

        var p = new Promise(function (resolve, reject) {
            resolveFunc = resolve;
            rejectFunc = reject;
        });

        var startSession = function () {
            var deviceIdEncrypted = Common.security.Security.encryptString(deviceId),
                options = Ext.clone(me.options);
            options.callback = resolveFunc;
            options.errorHandler = isRegistrationCheck ? registrationCheckErrorHandler : errorHandler;

            MobileSecurityService.startMobileUserSessionForDeviceId(deviceIdEncrypted, options);
        };

        startSession();

        return p;
    },

    startSsoSession: function () {
        var me = this;
        return new Promise(function (resolve, reject) {
            var options = Ext.clone(me.options);

            options.callback = resolve;
            options.errorHandler = me.errorHandlerFunction.bind(me, reject);

            MobileSecurityService.startMobileSsoUserSession(options);
        });
    },

    /**
     * Ends the Session.
     * Does not report an error. Calling SecurityService.endMobileUserSession on an already closed session
     * is a NO-OP.
     * @returns {Promise} A Promise that is always resolved.
     */
    endSession: function () {
        var me = this;
        return new Promise(function (resolve) {
            var options = Ext.clone(me.options);
            options.callback = resolve;
            options.errorHandler = function (message, exception) {
                var errorMessage = Common.service.ExceptionTranslator.extractMessage(exception);
                Log.log(errorMessage, 'warn', me, arguments);
                // Do not report errors when logging out.
                resolve();
            };

            SecurityService.endMobileUserSession(options);
        });
    },

    /**
     * Sets the Session locale
     * @param {String} localeName The Java locale name
     * @returns {Promise}
     */
    setLocaleForSession: function (localeName) {
        var me = this;
        return new Promise(function (resolve, reject) {
            var options = Ext.clone(me.options);
            options.callback = resolve;
            options.errorHandler = me.errorHandlerFunction.bind(me, reject);

            SecurityService.setLocaleFromJavaLocale(localeName, options);
        });
    },

    /**
     * Detects if the connection is using SSO mode
     * @returns {Promise} A Promise that resolves to true is the connection is using SSO. The Promise
     * resolves to false for non SSO connections.
     */
    isSsoMode: function () {
        var me = this;
        return new Promise(function (resolve, reject) {
            // Determine if WebCentral is in SSO mode:
            var options = Ext.clone(me.options);
            options.callback = function (ssoModeEncrypted) {
                var ssoModeDecrypted;
                if (ssoModeEncrypted.length > 0) {
                    ssoModeDecrypted = Common.security.Security.decryptString(ssoModeEncrypted);
                    if (ssoModeDecrypted.indexOf('preauth') !== -1) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                } else {
                    resolve(false);
                }
            };
            options.errorHandler = me.errorHandlerFunction.bind(me, reject);

            SmartClientConfigService.getSsoMode(options);
        });
    },


    /**
     * Checks if the device is registered by opening a Web Central session. If the session is started
     * successfully the device is registered. If there is an error starting the session the device
     * is not registered.
     * @param {String} deviceId of the device to check registration
     * @returns {Promise} a Promise object resolved to true if the device is registered, false otherwise.
     */
    isDeviceRegistered: function (deviceId) {
        var me = this;

        // The device cannot be registered if the device id is empty.
        if (Ext.isEmpty(deviceId)) {
            return Promise.resolve(false);
        } else {
            return me.isSsoMode()
                .then(function (isSsoMode) {
                    if (isSsoMode) {
                        return me.startSsoSession();
                    } else {
                        return me.startSessionForDeviceId(deviceId, true);
                    }
                })
                .then(function () {
                    return me.endSession();
                })
                .then(function () {
                    return Promise.resolve(true);
                }, function () {
                    return Promise.resolve(false);
                });
        }

    },


    /**
     * Checks if the user is a member of the supplied group.
     * @param {String} group The name of the user group to check membership in
     * @returns {Promise} A Promise that resolves to true if the user is a member of the group
     */
    isUserMemberOfGroup: function (group) {
        var me = this;
        return new Promise(function (resolve, reject) {
            var options = Ext.clone(me.options);
            options.callback = resolve;
            options.errorHandler = me.errorHandlerFunction.bind(me, reject);

            AdminService.isUserMemberOfGroup(group, options);
        });
    },

    /**
     * Registers device with specified deviceId for ARCHIBUS user with specified username and password.
     *
     * @param {String} deviceId ID of the device to be registered.
     * @param {String} userName ARCHIBUS username for whom the device will be registered.
     * @param {String} password password of the ARCHIBUS user for whom the device will be registered.
     * @param {String} localeName Java locale name, for example "en_US". Used to translate exceptions.
     * @returns {Promise} A Promise object resolved when the registration action is completed.
     * */
    registerDevice: function (deviceId, userName, password, localeName) {
        var me = this;

        return new Promise(function (resolve, reject) {
            var deviceIdEncrypted = Common.security.Security.encryptString(deviceId),
                usernameEncrypted = Common.security.Security.encryptString(userName),
                passwordEncrypted = Common.security.Security.encryptString(password),
                logMessage = 'Calling MobileSecurityService.registerDevice: deviceId=[{0}], username=[{1}], password=[{2}], localeName=[{3}]',
                options = {
                    async: true,
                    headers: {"cache-control": "no-cache"},
                    callback: resolve,
                    errorHandler: me.errorHandlerFunction.bind(me, reject)
                };
            Log.log(Ext.String.format(logMessage, deviceId, userName, password, localeName), 'info');
            MobileSecurityService.registerDevice(deviceIdEncrypted, usernameEncrypted, passwordEncrypted, localeName, options);
        });
    },

    /**
     * Unregisters the device in Web Central. Sets the afm_users.mob_device_id value to null
     * for the user.
     * Records the unregistration event in the afm_mob_dev_reg table.
     * @param {String} userName The name of the user for whom to unregister the device.
     * @returns {Promise} A resolved Promise if the operation is successful. A rejected Promise
     * containing the error information if the operation fails.
     */
    unRegisterDevice: function (userName) {
        var me = this;
        return new Promise(function (resolve, reject) {
            var usernameEncrypted = Common.security.Security.encryptString(userName),
                deviceIdEncrypted = Common.security.Security.encryptString(ConfigFileManager.deviceId),
                logMessage = 'Calling MobileSecurityService.unRegisterDevice: username=[{0}]',
                options = {
                    async: true,
                    headers: {"cache-control": "no-cache"},
                    callback: resolve,
                    errorHandler: me.errorHandlerFunction.bind(me, reject)
                };
            Log.log(Ext.String.format(logMessage, userName), 'info');
            MobileSecurityService.unRegisterDevice(usernameEncrypted, deviceIdEncrypted, options);
        });
    },

    /**
     * Records the registration event in the Web Central afm_mob_dev_reg table.
     * @param {String} userName of the user registering the device
     * @param {String} deviceId of the device being registered.
     * @param {String} deviceName the user defined name of the device. Not supported on all mobile platforms.
     * @returns {Promise} A resolved promise if the operation succeeds, otherwise a rejected promise containing
     * the error information.
     */
    recordDeviceRegistration: function (userName, deviceId, deviceName) {
        var me = this;
        return new Promise(function (resolve, reject) {
            var usernameEncrypted = Common.security.Security.encryptString(userName),
                deviceIdEncrypted = Common.security.Security.encryptString(deviceId),
                logMessage = 'Calling MobileSecurityService.recordDeviceRegistration',
                options = {
                    async: true,
                    headers: {"cache-control": "no-cache"},
                    callback: resolve,
                    errorHandler: me.errorHandlerFunction.bind(me, reject)
                };
            Log.log(logMessage, 'info');
            MobileSecurityService.recordDeviceRegistration(usernameEncrypted, deviceIdEncrypted, deviceName, options);
        });
    },

    /**
     * Retrieves the passphrase used to encrypt the Sqlite database.
     * @returns {Promise} A Promise object resolved with the passphrase
     */
    getSqlitePassphrase: function () {
        var me = this;
        return new Promise(function (resolve, reject) {
            var options = {
                async: true,
                headers: {"cache-control": "no-cache"},
                callback: resolve,
                errorHandler: me.errorHandlerFunction.bind(me, reject)
            };

            MobileSecurityService.getSqlitePassphrase(options);
        });
    },


    /**
     * *********************** Deprecated Methods ***************************
     * The methods in this section have been deprecated. These methods have been
     * replaced with Promisfied versions.
     *
     * It is recommended to use the Promise versions wherever possible
     */

    /**
     * Ends user session.
     * @deprecated 22.1 Please use {@link #endSession} instead
     */
    logout: function () {
        SecurityService.endMobileUserSession({
            async: false,
            headers: {"cache-control": "no-cache"},
            callback: Ext.emptyFn,
            errorHandler: Ext.emptyFn
        });
    },

    getLicensedActivies: function () {

        var getLicenses = function (licenses) {
            var enabledLicenses = {};
            licenses.forEach(function (license) {
                if (license.enabled && license.enabled === true) {
                    enabledLicenses[license.id] = 1;
                }
            });

            return enabledLicenses;
        };

        return new Promise(function (resolve, reject) {
            var options = {
                async: true,
                headers: {"cache-control": "no-cache"},
                callback: function (licenseInfo) {
                    var licenses = getLicenses(licenseInfo.licenses);
                    resolve(licenses);
                },
                errorHandler: function (message, exception) {
                    reject(message);
                }
            };

            AdminService.getProgramLicense(options);
        });
    }



});