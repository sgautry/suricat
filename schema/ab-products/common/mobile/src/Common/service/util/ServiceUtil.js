Ext.define('Common.service.util.ServiceUtil', {
    alternateClassName: ['ServiceUtil'],

    singleton: true,

    getUniqueIdString: function(uniqueId, dataObject) {
        var uniqueIdString = '';
        uniqueId.forEach(function(id) {
            uniqueIdString += dataObject[id];
        });
        return uniqueIdString;
    },

    /**
     * Returns a single object where each record is indexed by the unique record id.
     * @param {String[]} uniqueId Array of field names that comprise the unique identifier for the record.
     * @param {Object[]] dataRecords Array of data records where each record object contains the name/value pairs
     * of the record fields.
     * @returns {Object}
     */
    generateIndexedRecordsObject: function(uniqueId, dataRecords) {
        var me = this,
            dataObject = {};

        dataRecords.forEach(function(dataRecord) {
            var uniqueIdString = me.getUniqueIdString(uniqueId, dataRecord);
            dataObject[uniqueIdString] = dataRecord;
        });

        return dataObject;
    }

});
