/**
 * Drawing services to retrieve SVG data from the WebCentral server.
 * @since 21.3
 * @author Jeff Martin
 */
Ext.define('Common.service.drawing.Drawing', {
    requires: [
        'Common.service.ExceptionTranslator'
    ],

    singleton: true,

    /**
     * @property {Object} options Common options used when calling the DWR services.
     */
    options: {
        async: true,
        headers: {"cache-control": "no-cache"},
        timeout: 180000, // 180 seconds
        callback: Ext.emptyFn,
        errorHandler: Ext.emptyFn
    },

    retrieveSvgFromServer: function (primaryKeys, planType, highlights) {
        var me = this;
        return new Promise(function (resolve, reject) {
            var options = Ext.clone(me.options);
            options.callback = function (svgData) {
                resolve(svgData === null ? '' : svgData);
            };
            options.errorHandler = function (message, exception) {
                var errorMessage;
                // Error code 4: Cannot find specified path
                // Error code 14: Cannot find specified file
                if(exception.errorNumber === 4 || exception.errorNumber === 14) {
                    resolve(''); // File does not exists is not an error condition
                } else {
                    errorMessage = Common.service.ExceptionTranslator.extractMessage(exception);
                    reject(errorMessage);
                }

            };
            DrawingSvgService.highlightSvgDrawing(primaryKeys, planType, highlights, options);
        });
    },

    /**
     * Returns the floor codes for all floors that have published SVG drawings
     * @param siteCodes
     * @param buildingCodes
     * @returns {*}
     */
    getFloorCodesForPublishedDrawings: function (siteCodes, buildingCodes) {
        var me = this;
        // TODO: siteCodes and buildingCodes must be array
        return new Promise(function (resolve, reject) {
            var options = Ext.clone(me.options);
            options.callback = resolve;
            options.errorHandler = function(message, exception) {
                var errorMessage = Common.service.ExceptionTranslator.extractMessage(exception);
                Log.log(errorMessage, 'warn', me, arguments);
                reject(errorMessage);
            };

            DrawingSvgService.retrieveFloorCodes(siteCodes, buildingCodes, options);

        });
    }
});