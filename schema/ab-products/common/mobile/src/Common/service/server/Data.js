Ext.define('Common.service.server.Data', {
    alternateClassName: ['ServerData'],
    requires: 'Common.service.MobileSyncServiceAdapter',
    singleton: true,

    /**
     * Retrieves records from the server. Formats the records as an array of objects where each
     * object contains the name/value pairs of each field in the record.
     * @param {String} table The name of the server side table.
     * @param {String[]} fields Array containing the names of each of the fields to retrieve.
     * @param {Object} restriction The restriction to apply to the server side table.
     * @returns {Object[]}
     */
    retrieveRecords: function(table, fields, restriction) {
        var me = this;
        return MobileSyncServiceAdapter.retrieveRecords(table, fields, restriction)
            .then(function(serverRecords) {
                var dataRecords = me.processServerRecords(serverRecords);
                return Promise.resolve(dataRecords);
            });
    },

    /**
     * @private
     * Modifies the format of the serverRecords.
     * @param serverRecords
     * @returns An array of objects where each object contains the name/value pair of each of the server fields.
     */
    processServerRecords: function(serverRecords) {
        var dataRecords = [];
        serverRecords.forEach(function(serverRecord) {
            var data = {};
            serverRecord.fieldValues.forEach(function(field){
                data[field.fieldName] = field.fieldValue;
            });
            dataRecords.push(data);
        });
        return dataRecords;
    }
});