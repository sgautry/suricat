/**
 *
 * @author Jeff Martin
 * @since 22.1
 * @singleton
 */
Ext.define('Common.env.Feature', {
    singleton: true,

    chromeMessage: LocaleManager.getLocalizedString('Running in Chrome or in WebView. Filesystem access enabled',
        'Common.env.Feature'),

    browserMessage: LocaleManager.getLocalizedString('Running in Browser Mode. Filesystem access disabled',
        'Common.env.Feature'),

    /**
     * @property {Boolean} hasFileSystem Returns true when the device or browser supports the HTML5 FileSystem API
     */
    hasFileSystem: false,

    isNative: false,
    
    isHTCPhone: false,

    isCrosswalk: false,

    hasNativeSqlite: false,

    hasCamera: false,
    
    hasTouch: false,

    hasBarcode: false,

    isBrowserMode: false,

    constructor: function() {
        var me = this;
        me.setIsCrosswalk();
        me.setHasFileSystem();
        me.setIsNative();
        me.setIsHTCPhone();
        me.setHasNativeSqlite();
        me.setHasCamera();
        me.setHasTouch();
        me.setHasBarcode();
        me.setIsBrowserMode();
    },

    /**
     * @private
     */
    setHasFileSystem: function() {
        // The Android Chrome Mobile browser includes file system support. We don't want to use the
        // file system on this platform so we set hasFileSystem to false.
        //<debug>
        if(Ext.os.is.Android && Ext.browser.is.ChromeMobile) {
            this.hasFileSystem = false;
            return;
        }
        //</debug>
        this.hasFileSystem = ('requestFileSystem' in window || 'webkitRequestFileSystem' in window);
    },

    setIsNative: function() {
        this.isNative = (Ext.browser.is.PhoneGap || this.getIsCrosswalk()) ? true: false;
    },
    
    setIsHTCPhone: function() {
        var ua = navigator.userAgent;
        
        this.isHTCPhone = /HTC/.test(ua);
    },

    setIsCrosswalk: function() {
        var ua = navigator.userAgent,
            isCrosswalk = /Crosswalk/.test(ua);

        this.isCrosswalk = isCrosswalk;
    },

    setHasNativeSqlite: function() {
        this.hasNativeSqlite = window.sqliteplugin ? true : false;
    },

    setHasCamera: function() {
        this.hasCamera = navigator.camera ? true : false;
    },
    
    setHasTouch: function() {
        this.hasTouch = 'ontouchstart' in document.documentElement;
    },

    setHasBarcode: function() {
        this.hasBarcode = false;
        if ((typeof plugins !== 'undefined') && plugins.barcodeScanner) {
            this.hasBarcode = true;
        }
        if ((typeof cordova !== 'undefined') && (typeof cordova.plugins !== 'undefined') && cordova.plugins.barcodeScanner) {
            this.hasBarcode = true;
        }
    },

    setIsBrowserMode: function() {
        var developerMode;

        if (typeof Abm !== 'undefined') {
            developerMode = Abm.DEV_MODE;
        } else {
            developerMode = false;
        }
        if (Ext.browser.is.PhoneGap || (Ext.browser.is.Chrome && developerMode)) {
            this.isBrowserMode = false;
        } else {
            this.isBrowserMode = true;
        }
    },

    /**
     * @private
     */
    getIsCrosswalk: function() {
        var ua = navigator.userAgent,
        isCrosswalk = /Crosswalk/.test(ua);

        return isCrosswalk;
    }

});