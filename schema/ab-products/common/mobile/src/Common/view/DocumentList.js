/**
 * Common DocumentList class. Super class for list of documents views.
 * @since 21.4
 * @author Ana Paduraru
 * @class
 */
Ext.define('Common.view.DocumentList', {
    extend: 'Ext.Container',
    requires: 'Common.view.DocumentItem',
    xtype: 'documentList',

    config: {
        layout: 'vbox',
        items: [
            {
                xtype: 'titlepanel',
                title: LocaleManager.getLocalizedString('Documents', 'Common.view.DocumentList'),
                docked: 'top'
            },
            {
                xtype: 'container',
                html: '<div class="empty-text">' + LocaleManager.getLocalizedString('No documents available', 'Common.view.DocumentList') + '</div>',
                itemId: 'documentMessage'
            }
        ],

        /**
         * @cfg {Ext.data.Store} The store associated with the document list. A store is required when using the
         * On Demand Document Download feature.
         */
        store: null,

        /**
         * @cfg {Boolean} enableImageRedline Set to true to display the image redline button
         */
        enableImageRedline: false,

        /**
         * @cfg {Boolean} enableDelete Set to true to display the delete button
         */
        enableDelete: false
    },

    applyStore: function (store) {
        if (store) {
            store = Ext.data.StoreManager.lookup(store);
        }
        return store;
    },

    applyRecord: function (config) {
        if (config) {
            this.processDocumentFields(config);
        }
        return config;
    },

    processDocumentFields: function (record) {
        var me = this,
            store = me.getStore(),
            documents = record.getDocumentFieldsAndData(),
            enableImageRedline = me.getEnableImageRedline(),
            enableDelete = me.getEnableDelete(),
            clientTable = record.getTableNameFromInstance(),
            documentFolder = GlobalParameters.getUserDocumentFolder() + '/' + clientTable,
            downloadDocumentsOnDemand = false;


        me.removeExistingItems();

        if (store && Ext.isFunction(store.getIncludeDocumentDataInSync)) {
            downloadDocumentsOnDemand = !store.getIncludeDocumentDataInSync();
        }

        if(downloadDocumentsOnDemand) {
            documents.forEach(function (document) {
                var documentConfig = store.getDocumentConfig(),
                    primaryKey = me.generatePrimaryKeyObject(documentConfig.documentTableKeys, record),
                    fileName = DocumentManager.generateFileName(documentConfig.documentTable, document.fieldName, document.file, primaryKey);

                document.filePath = documentFolder + '/' + fileName;
            }, me);

            me.documentFileExists(documents)
                .then(function(modifiedDocuments) {
                    modifiedDocuments.forEach(function(document) {
                        me.addDocumentItem(document, record, enableImageRedline, enableDelete, downloadDocumentsOnDemand);
                    });
                });
        } else {
            documents.forEach(function(document) {
                me.addDocumentItem(document, record, enableImageRedline, enableDelete, downloadDocumentsOnDemand);
            });
        }
    },

    documentFileExists: function (documents) {
        var updateFileExists = function() {
            var p = Promise.resolve(),
                modifiedDocObjects = [];
            documents.forEach(function (documentObject) {
                p = p.then(function () {
                    // TODO: only check if the doc field is populated.
                    return Common.device.File.fileExists(documentObject.filePath)
                        .then(function () {
                            documentObject.fileExists = true;
                            modifiedDocObjects.push(documentObject);
                            return Promise.resolve(modifiedDocObjects);
                        }, function () {
                            documentObject.fileExists = false;
                            modifiedDocObjects.push(documentObject);
                            return Promise.resolve(modifiedDocObjects);
                        });
                });
            });
            return p;
        };

      return updateFileExists();
  },

    addDocumentItem: function (document, record, enableImageRedline, enableDelete, downloadDocumentsOnDemand) {
        var me = this,
            documentMessage = me.down('#documentMessage'),
            documentItem;


        if (!Ext.isEmpty(document.file)) {
            documentMessage.setHidden(true);
            documentItem = Ext.factory({
                file: {
                    html: document.file
                },
                documentData: document.data,
                recordId: record.getId(),
                documentFieldId: document.fieldId,
                enableImageRedline: enableImageRedline,
                enableDelete: enableDelete,
                fieldName: document.fieldName,
                isNew: document.isNew,
                downloadDocumentsOnDemand: downloadDocumentsOnDemand,
                documentIsDownloaded: document.fileExists
            }, 'Common.view.DocumentItem');

            me.add(documentItem);
        }
    },


    /**
     * @private
     */
    generatePrimaryKeyObject: function (keyFields, record) {
        var primaryKey = {};

        Ext.each(keyFields, function (keyField) {
            primaryKey[keyField] = record.get(keyField);
        });

        return primaryKey;
    },


    /**
     * Destroy existing documentItems.
     */
    removeExistingItems: function () {
        var me = this,
            documentItems = me.query('documentItem');
        if (documentItems && documentItems.length > 0) {
            Ext.each(documentItems, function (documentItem) {
                me.remove(documentItem, true);
            }, me);
        }
    }

});