Ext.define('Common.view.navigation.FilterForm', {
    extend: 'Common.form.FormPanel',

    config: {
        /**
         * @cfg {Boolean} enableFilterHistory Allows saving the latest filter values and use them.
         * @since 23.1
         */
        enableFilterHistory: true,

        toolBarButtons: [
            {
                xtype: 'toolbarbutton',
                text: LocalizedStrings.z_Clear,
                name: 'clearButton',
                align: 'right'
            },
            {
                xtype: 'toolbarbutton',
                text: LocalizedStrings.z_Filter,
                name: 'filterButton',
                ui: 'action',
                align: 'right'
            }
        ]
    },

    initialize: function () {
        var me = this,
            buttons = me.getToolBarButtons();

        Ext.each(buttons, function (button) {
            if (button.name === 'clearButton') {
                button.listeners = {tap: me.clearFilter, scope: me};
            } else if (button.name === 'filterButton') {
                button.listeners = {tap: me.applyFilter, scope: me};
            }
        });

        me.callParent(arguments);

        me.loadLatestFiltersFromStorage();
        me.setValues(me.latestFilters);
    },

    loadLatestFiltersFromStorage: function () {
        var me = this,
            localStorageKey = me.getLocalStorageName(),
            storageValue,
            decodedJson;

        me.latestFilters = [];

        if (Ext.isEmpty(localStorageKey)) {
            return;
        } else {
            storageValue = localStorage.getItem(localStorageKey);
        }


        if (!Ext.isEmpty(storageValue)) {
            decodedJson = Ext.JSON.decode(storageValue);
            me.latestFilters = me.decodeValues(decodedJson);
        }
    },

    getLocalStorageName: function () {
        var appName = Common.Application.appName,
            xtype = this.xtype;

        if (Ext.isEmpty(xtype)) {
            return '';
        } else {
            return Ext.String.format('Ab.{0}.{1}', appName, xtype);
        }
    },

    //@private
    addFiltersToHistory: function () {
        var me = this,
            localStorageKey = me.getLocalStorageName(),
            values = me.getValues(),
            jsonValues;

        me.latestFilters = values;

        jsonValues = me.encodeValues(values);

        localStorage.setItem(localStorageKey, Ext.JSON.encode(jsonValues));
    },

    clearFilter: function () {
        var me = this,
            promptFields = me.query('prompt');

        // Reset prompt fields using setValue to clear keyValue as well. Field reset sets value '' on the promptInput component, not on the field directly.
        Ext.each(promptFields, function (field) {
            field.setValue('');
        });

        me.reset();
        me.addFiltersToHistory();

        me.fireEvent('clearFilter', me);
    },

    applyFilter: function () {
        var me = this;

        me.addFiltersToHistory();
        me.fireEvent('applyFilter', me);
    },

    /**
     * Adds type information to the stored filter values. Allows the storage of complex types such as Dates in a JSON
     * string.
     * @param {Object} values The values to encode
     */
    encodeValues: function(values) {
        var p,
            value,
            encodedValues = {};

        for(p in values) {
            value = values[p];
            if(Ext.isDate(value)) {
                encodedValues[p] = {$type:'date'};
            } else if(Ext.isNumber(value)) {
                encodedValues[p]= {$type:'number'};
            } else {
                encodedValues[p]= {$type:'string'};
            }
            encodedValues[p].$value = values[p];
        }
        return encodedValues;
    },

    /**
     * Creates a new value object using the type information stored in the JSON string.
     * @param {Object} jsonObject
     */
    decodeValues: function(jsonObject) {
        var valueObject = {},
            p;

        for(p in jsonObject) {
            if(jsonObject[p].$type === 'date') {
                valueObject[p] = new Date(jsonObject[p].$value);
            } else {
                valueObject[p] =jsonObject[p].$value;
            }
        }

        return valueObject;
    }
});

