/* Disable JSHint Variable Not Used and Too Many Statement errors. */
/* jshint -W098 */
/* jshint -W071 */
var VersionCheck = (function () {

    var serviceEndpoints = [
        'version',
        'revision',
        'schemaversion',
        'cordovaversion'
    ];

    var versionInfo = {
        version: '',
        revision: '',
        schemaversion: '',
        cordovaversion: ''
    };

    var TIMEOUT = 2;

    var vc = {};

    function getUrlParameters() {
        var match,
            pl = /\+/g,  // Regex for replacing addition symbol with a space
            search = /([^&=]+)=?([^&]*)/g,
            decode = function (s) {
                return decodeURIComponent(s.replace(pl, " "));
            },
            query = window.location.search.substring(1);

        var urlParams = {};
        while (match = search.exec(query)) {
            urlParams[decode(match[1])] = decode(match[2]);
        }

        return urlParams;
    }

    function getVersionInfo(origin) {
        var me = this,
            endpoints = serviceEndpoints;

        if (Ext.isEmpty(origin)) {
            origin = window.location.origin + '/archibus';
        }

        // Remove any trailing slashes from the origin URL
        origin = origin.replace(/\/*$/g, '');

        return Promise.all(endpoints.map(function (endpoint) {
            var url = origin + '/cxf/configs/' + endpoint;
            return callCxfService(url, endpoint);
        }))
            .then(function (versions) {
                Ext.each(versions, function (version) {
                    versionInfo[version.endpoint] = version.value;
                });
                return Promise.resolve(versionInfo);
            });
    }

    function callCxfService(url, endpoint) {
        var me = this;

        return new Promise(function (resolve) {
            var xhr = new XMLHttpRequest(),
                requestTimer,
                result = {};

            result.endpoint = endpoint;
            result.value = '';

            xhr.open('GET', url, true);
            requestTimer = setTimeout(function () {
                xhr.abort();
            }, TIMEOUT * 1000);
            xhr.onreadystatechange = function () {
                var status,
                    content;
                if (xhr.readyState === 4) {
                    clearTimeout(requestTimer);
                    status = xhr.status;
                    content = xhr.responseText;
                    if ((status >= 200 && status < 300) || status === 304 || (status === 0 && content.length > 0)) {
                        result.value = content;
                        resolve(result);
                    } else if (status === 404 || status === 500) {
                        result.value = 'NOT_FOUND';
                        resolve(result);
                    }
                    else {
                        result.value = '';
                        resolve(result);
                    }
                }
            };
            xhr.send(null);
        });
    }

    vc.getWebCentralVersion = function () {
        var origin = window.location.origin + '/archibus',
            mobileClientVersion = 0;
        return getVersionInfo(origin)
            .then(function(version) {
                var urlParameters = getUrlParameters();
                //Add Mobile Client version property
                if(urlParameters.mcversion) {
                    mobileClientVersion = urlParameters.mcversion;
                }
                version.mcversion = mobileClientVersion;

                return Promise.resolve(version);
            });
    };




    return vc;
})();