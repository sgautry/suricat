/* global StartTest */
/* jshint newcap:false */
StartTest(function (t) {

    t.diag('Testing Work Request Sync: test if a date value survives roundtrip to server and back.');


    t.requireOk('Maintenance.model.WorkRequest', 'Common.log.Logger',
        'Maintenance.store.WorkRequests', 'Common.config.GlobalParameters',
        'Maintenance.model.Validation', 'Common.store.TableDefs',
        'Common.service.Session', 'Common.test.util.Database', 'Common.test.util.TestUser',
        'Common.util.Network', 'Ext.data.Types',
        function () {

            var workRequestStore,
            async;

            // Register the tableDefs store in the StoreManager
            Ext.create('Common.store.TableDefs');
            workRequestStore = Ext.create('Maintenance.store.WorkRequests');
            workRequestStore.setDeleteAllRecordsOnSync(false);
            workRequestStore.setTimestampDownload(false);

            workRequestStore.setRestriction({
                tableName: 'wr_sync',
                fieldName: 'wr_id',
                operation: 'EQUALS',
                value: '1150000338'
            });

            async = t.beginAsync();

            Common.test.util.TestUser.registerTestUser('AFM', 'afm')
                .then(function () {
                    // Clean up the local database before inserting records
                    //Common.test.util.Database.deleteWorkRequestSyncTestRecords(function () {
                    return Common.service.Session.start()
                        .then(function() {
                            return workRequestStore.syncStore();
                        })
                        .then(function() {
                            return Common.service.Session.end();
                        })
                        .then(function() {
                            t.done();
                            t.endAsync(async);
                        })
                        .catch(function(error) {
                            t.fail(error);
                            Common.service.Session.end()
                                .then(function() {
                                    t.done();
                                    t.endAsync();
                                });
                        });
                });
        });
});