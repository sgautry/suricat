/* jshint newcap:false */
/* global StartTest */
StartTest(function (t) {

    t.requireOk('Common.test.util.TestUser', 'Common.service.Session', 'Common.store.sync.ValidatingTableStore',
        'Common.util.Network', 'Common.util.TableDef', 'Common.store.TableDefs', 'Common.store.proxy.Sqlite',
        'Common.store.Rooms', 'Common.log.Logger', 'Common.config.GlobalParameters',
        'Ext.data.Types', 'Common.store.TableDownloads', 'Common.store.AppPreferences', 'Common.sync.Manager',
        function () {

            var employeeStore,
                roomsStore,
                async;

            var createRoomTableRestriction = function () {
                var blIds = ['AB', 'EC01',
                    'EC02', 'EC03', 'EC04', 'EC05',
                    'EC06', 'EC07', 'EC08', 'EC09',
                    'EC10', 'EC11', 'EC12', 'EC13',
                    'EC14', 'EC15', 'EC17', 'EC18',
                    'EC19', 'EC20', 'EC21', 'EC22',
                    'EC23', 'EC24', 'EC25', 'EC26',
                    'EC27', 'MC01', 'MC02', 'MC03',
                    'MC04', 'MC05', 'MC06', 'MC07',
                    'MC08', 'MC09', 'MC10', 'MC11',
                    'MC12', 'NB', 'NC01', 'NC02',
                    'NC04', 'NC05', 'NC06', 'NC07',
                    'SRL'];

                var restriction = [];

                blIds.forEach(function (id, idx) {
                    var relOp = idx === 0 ? 'AND_BRACKET' : 'OR';
                    restriction.push({
                        tableName: 'rm',
                        fieldName: 'bl_id',
                        operation: 'EQUALS',
                        value: id,
                        relativeOperation: relOp
                    });
                });

                return restriction;
            };

            var appPreferencesStore = Ext.create('Common.store.AppPreferences');

            // Create TableDefs store
            Ext.create('Common.store.TableDefs');
            Ext.create('Common.store.TableDownloads');

            roomsStore = Ext.create('Common.store.Rooms');

            // Adjust the roomsStore syncRecordsPageSize
            roomsStore.setSyncRecordsPageSize(1000);

            async = t.beginAsync();

            // Register the test user
            Common.test.util.TestUser.registerTestUser('TRAM', 'afm')
                .then(function () {
                    return Common.service.Session.start();
                })
                .then(function() {
                    return Common.test.util.Database.loadStore(appPreferencesStore);
                })
                .then(function() {
                    return appPreferencesStore.deleteAndImportRecords();
                })
                .then(function () {
                    var restriction = createRoomTableRestriction();
                    roomsStore.setRestriction(restriction);
                    roomsStore.setTimestampDownload(false);
                    return Common.sync.Manager.downloadValidatingTable(roomsStore);
                })
                .then(function () {
                    return Common.test.util.Database.loadStore(roomsStore);
                })
                .then(function () {
                    var totalRecordCount = roomsStore.getTotalCount();
                    t.is(totalRecordCount, 8308, 'The Room table has the correct number of records.');
                    t.endAsync(async);
                    t.done();
                    return Promise.resolve();
                })
                .then(function () {
                    return Common.service.Session.end();
                })
                .catch(function (error) {
                    t.fail(error);
                    Common.service.Session.end()
                        .then(function () {
                            t.endAsync(async);
                            t.done();
                        });
                });

        });

});

