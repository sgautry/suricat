StartTest(function (t) {

    t.requireOk('Common.test.util.TestUser', 'Common.service.Session', 'Common.log.Logger', 'Ext.data.Types',
        'Common.config.GlobalParameters', 'Common.service.MobileSyncServiceAdapter', 'Maintenance.model.Validation',
        'Common.store.proxy.SqliteConnectionManager', 'Maintenance.store.WorkRequests', 'Common.store.TableDefs',
        function () {

            var table = 'wr_sync';
            var uniqueId = ['wr_id'];
            var workRequestsStore = Ext.create('Maintenance.store.WorkRequests');

            var getUniqueIdIndex = function (uniqueId, fieldObject) {
                var idStr = '';

                uniqueId.forEach(function (id) {
                    idStr += fieldObject[id];
                });

                return idStr;
            };

            var processRecords = function (uniqueId, records) {
                var dataRecords = {};
                records.forEach(function (record) {
                    var data = {};
                    record.fieldValues.forEach(function (field) {
                        data[field.fieldName] = field.fieldValue;
                    });
                    dataRecords[getUniqueIdIndex(uniqueId, data)] = data;
                });

                return dataRecords;
            };


            var generateGuidSql = function (clientTable, index, uniqueId, data) {
                var sqlObject = {},
                    sql,
                    val,
                    idExpression;

                var getIdExpression = function (uniqueId) {
                    var idData = {},
                        ids = [],
                        idValues = [];
                    uniqueId.forEach(function (id) {
                        ids.push(id + '= ?');
                        idValues.push(data[index][id]);
                    });
                    idData.ids = ids.join(' AND ');
                    idData.values = idValues;
                    return idData;
                };

                idExpression = getIdExpression(uniqueId);
                sql = 'UPDATE ' + clientTable + ' SET guid = ? WHERE ' + idExpression.ids;
                sqlObject.sql = sql;
                val = idExpression.values;
                val.unshift(data[index].guid);
                sqlObject.values = val;

                return sqlObject;
            };

            var executeSql = function (sql, values) {
                var db = SqliteConnectionManager.getConnection();
                return new Promise(function (resolve, reject) {
                    db.transaction(function (tx) {
                        tx.executeSql(sql, values, function () {
                            resolve();
                        }, function (tx, error) {
                            reject(error.message);
                        });
                    });
                });
            };

            var loadStore = function (store) {
                return new Promise(function (resolve, reject) {
                    store.load(function (records) {
                        resolve(records);
                    });
                });
            };

            var downloadedGUID;


            // Register the tableDefs store in the StoreManager
            Ext.create('Common.store.TableDefs');

            var async = t.beginAsync();
            // Register the test user
            Common.test.util.TestUser.registerTestUser('AFM', 'afm')
                .then(function () {
                    return Common.service.Session.start();
                })
                .then(function () {
                    // Sync the workrequest store
                    return workRequestsStore.syncStore();
                })
                .then(function () {
                    // Create restriction
                    var restriction = {};
                    restriction.clauses = [
                        {
                            tableName: 'wr_sync',
                            fieldName: 'mob_locked_by',
                            operation: 'EQUALS',
                            value: 'AFM'
                        }
                    ];
                    return MobileSyncServiceAdapter.retrieveRecords(table, ['wr_id', 'guid'], restriction);
                })
                .then(function (records) {
                    var dataRecords = processRecords(uniqueId, records);
                    // Check generated SQL for wr_id = 1150000003
                    var sql = generateGuidSql('WorkRequest', '1150000003', uniqueId, dataRecords);
                    downloadedGUID = sql.values[0];
                    return executeSql(sql.sql, sql.values);

                })
                .then(function () {
                    return Common.service.Session.end();
                })
                .then(function () {
                    workRequestsStore.clearFilter();
                    workRequestsStore.setFilters({property: 'wr_id', value: 1150000003});
                    return loadStore(workRequestsStore);
                })
                .then(function (records) {
                    var savedGuid = records[0].data.guid;
                    t.is(savedGuid, downloadedGUID, 'GUIDs Match.');

                })
                .catch(function (error) {
                    t.fail(error);
                    Common.service.Session.end();
                })
                .done(function () {
                    t.endAsync(async);
                    t.done();
                });

        });
}); 