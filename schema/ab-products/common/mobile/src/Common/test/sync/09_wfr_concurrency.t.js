StartTest(function (t) {

        t.requireOk('Common.test.util.TestUser', 'Common.service.Session', 'Common.util.Network',
            'Common.util.TableDef', 'Common.store.TableDefs', 'Common.log.Logger',
            'Common.config.GlobalParameters', 'Common.service.workflow.Workflow', function () {

                var async = t.beginAsync(100000);

                var startTime;

                /*
                var users = [
                    {userName: 'WALESC', cfId: 'C.WALES'},
                    {userName: 'PECKG', cfId: 'GREG PECK'},
                    {userName: 'JAP7', cfId: 'JOSEPH PAYNE'},
                    {userName: 'GJ',    cfId: 'GARY JARDINE'},
                    {userName: "GJS",cfId:"GARY SITOSKI"},
                    {userName:"GO2",cfId:"GREGORY ORR"},
                    {userName:"GRAHAMS",cfId:"STEPHEN GRAHAM"},
                    {userName:"HARPERK",cfId:"KEVIN HARPER"},
                    {userName:"HARTLEYS",cfId:"STEPHEN HARTLEY"},
                    {userName:"HINCHK",cfId:"KARL HINCH"},
                    {userName:"JB79",cfId:"JODY BENN"},
                    {userName:"AFM",cfId:"AFM"},
                    {userName:"JL90",cfId:"JEFF LEROUX"},
                    {userName:"JWNM",cfId:"JOHN MCCALLUM"},
                    {userName:"KAW2",cfId:"KEITH WILDE"},
                    {userName:"KISHR",cfId:"RUSSELL KISH"},
                    {userName:"KJKS",cfId:"KIRK SCHONWANDT"},
                    {userName:"KNOWLESR",cfId:"RICHARD KNOWLES"},
                    {userName:"MB182",cfId:"MATTHEW BARRETT"},
                    {userName:"MCCULLJ",cfId:"JASON MCCULLOUGH"},
                    {userName:"MOORHEAD",cfId:"SCOTT MOORHEAD"},
                    {userName:"PECKG",cfId:"GREG PECK"},
                    {userName:"PH4",cfId:"PAUL HALLADAY"},
                    {userName:"RASHOTTJ",cfId:"JEFF RASHOTTE"},
                    {userName:"RB114",cfId:"RYAN BERRY"},
                    {userName:"REASONR",cfId:"ROBERT REASON"},
                    {userName:"REC",cfId:"ROLAND CLANCY"},
                    {userName:"REH1",cfId:"ROBERT HUGHES"},
                    {userName:"RJK",cfId:"RICHARD KISH"},
                    {userName:"RV2",cfId:"R.VAN HEDDEGEM"},
                    {userName:"RYDERD",cfId:"DEAN RYDER"},
                    {userName:"SEARSD",cfId:"DAVID SEARS"},
                    {userName:"SK157",cfId:"SCOTT KERR"},
                    {userName:"SS227",cfId:"STEVE SENECHAL"},
                    {userName:"STERLING",cfId:"RICHARD STERLING"},
                    {userName:"TL45",cfId:"THOMAS LINDSAY"},
                    {userName:"TPR",cfId:"TED RODDY"},
                    {userName:"TWORTR",cfId:"ROBERT TWORT"},
                    {userName:"VELEYR",cfId:"RICHARD VELEY"},
                    {userName:"WALESC",cfId:"C.WALES"},
                    {userName:"AZULAY",cfId:"KYLE AZULAY"},
                    {userName:"BD4",cfId:"BRIAN DEIR"},
                    {userName:"BGT",cfId:"BEN TEE"},
                    {userName:"BM16",cfId:"BOGUMIL MEKARSKI"},
                    {userName:"BMP",cfId:"BERNARD PALMER"},
                    {userName:"BROWNELL",cfId:"RICHARD BROWNELL"},
                    {userName:"CAIRDS",cfId:"STEVE CAIRD"},
                    {userName:"CM1",cfId:"CHRISTOPHER MAYO"},
                    {userName:"COMPEAUS",cfId:"STEVE COMPEAU"},
                    {userName:"CS22",cfId:"CHRIS SLEETH"},
                    {userName:"DANOT",cfId:"TROY DANO"},
                    {userName:"DL3",cfId:"DAVID LYNCH"},
                    {userName:"DM96",cfId:"D. MORTON"},
                    {userName:"EJP3",cfId:"EDWARD PERRY"},
                    {userName:"EM9",cfId:"E.MCCLENAGHAN"},
                    {userName:"SVA4",cfId:"S. VELOSO ARRUDA"},
                    {userName:"KALB",cfId:"K. BIBER"},
                    {userName:"ALNS",cfId:"A. SMART"},
                    {userName:"BLC3",cfId:"BUFFY COUGHLIN"},
                    {userName:"TZL",cfId:"J. LI"},
                    {userName:"MP124",cfId:"MATTHEW PARRISH"},
                    {userName:"MV32",cfId:"M.VANALSTYNE"},
                    {userName:"NH5",cfId:"NEAL HILL"},
                    {userName:"NICOLM",cfId:"MICHAEL NICOL"},
                    {userName:"PC16",cfId:"PATRICK CUMMINGS"},
                    {userName:"RA19",cfId:"R. ALLEN"},
                    {userName:"HJA",cfId:"H. ALLEN"},
                    {userName:"AMODEO",cfId:"A. AMODEO"},
                    {userName:"AMODEOV",cfId:"V. AMODEO"},
                    {userName:"ANDERSOP",cfId:"P. ANDERSON"},
                    {userName:"ASSELSTD",cfId:"D. ASSELSTINE"},
                    {userName:"NB76",cfId:"N. BATES"},
                    {userName:"BAUMLT",cfId:"T. BAUML"},
                    {userName:"BOGAERTS",cfId:"D. BOGAERTS"},
                    {userName:"SB12",cfId:"S. BRAZEAU"},
                    {userName:"JB230",cfId:"J. BRICELAND"},
                    {userName:"CB188",cfId:"C. BRIGHTMAN"},
                    {userName:"SB145",cfId:"S. BULLOCK"},
                    {userName:"SB100",cfId:"S. BURCHAT"},
                    {userName:"JB258",cfId:"J. BYROM"},
                    {userName:"RB1",cfId:"R. BYROM"},
                    {userName:"AMC8",cfId:"A. CABRAL"},
                    {userName:"MC15",cfId:"M. CAHILL"},
                    {userName:"DMC1",cfId:"D. COLE"},
                    {userName:"SLC1",cfId:"S. COOK"},
                    {userName:"LC110",cfId:"L. COOPER"},
                    {userName:"AC85",cfId:"A. CUDDON"},
                    {userName:"PC10",cfId:"P. CUMMINGS"},
                    {userName:"DESOUSAS",cfId:"S. DESOUSA"},
                    {userName:"DEVINEA",cfId:"A. DEVINE"},
                    {userName:"BD17",cfId:"B. DEVINE"},
                    {userName:"DEVINEK",cfId:"K. DEVINE"},
                    {userName:"DUCHARMC",cfId:"C. DUCHARME"},
                    {userName:"DUSHARMR",cfId:"R. DUSHARM"},
                    {userName:"FLETCHRK",cfId:"K. FLETCHER"},
                    {userName:"FRASERT",cfId:"T. FRASER"},
                    {userName:"MG123",cfId:"M. GANTON"},
                    {userName:"CG9",cfId:"C. GRANT"},
                    {userName:"MH51",cfId:"M. HARNDEN"},
                    {userName:"SH136",cfId:"S. HART"},
                    {userName:"HAWESB",cfId:"W. HAWES"}

                ];
                */


                var users = [
                    {userName: 'AFM', cfId: 'afm'},
                    {userName:"CF59",cfId:"CF59"},
                    {userName:"CF13",cfId:"CF13"},
                    {userName:"CF41",cfId:"CF41"},
                    {userName:"CF3",cfId:"CF3"},
                    {userName:"KOSTER",cfId:"MARK KOSTER"},
                    {userName:"SUPERVISOR",cfId:"SU"},
                    {userName:"CF31",cfId:"CF31"},
                    {userName:"CF69",cfId:"CF69"},
                    {userName:"CF9",cfId:"CF9"},
                    {userName:"CF12",cfId:"CF12"},
                    {userName:"CF20",cfId:"CF20"},
                    {userName:"CF26",cfId:"CF26"},
                    {userName:"CF52",cfId:"CF52"},
                    {userName:"CF18",cfId:"CF18"},
                    {userName:"CF48",cfId:"CF48"},
                    {userName:"CF8",cfId:"CF8"},
                    {userName:"CF34",cfId:"CF34"},
                    {userName:"CF36",cfId:"CF36"},
                    {userName:"CF2",cfId:"CF2"},
                    {userName:"CF66",cfId:"CF66"},
                    {userName:"CF64",cfId:"CF64"},
                    {userName:"CF53",cfId:"CF53"},
                    {userName:"CF25",cfId:"CF25"},
                    {userName:"CF15",cfId:"CF15"},
                    {userName:"CF49",cfId:"CF49"},
                    {userName:"CF47",cfId:"CF47"},
                    {userName:"CF70",cfId:"CF70"},
                    {userName:"CF5",cfId:"CF5"},
                    {userName:"CF37",cfId:"CF37"},
                    {userName:"CF39",cfId:"CF39"},
                    {userName:"CF29",cfId:"CF29"},
                    {userName:"CF63",cfId:"CF63"},
                    {userName:"CF19",cfId:"CF19"},
                    {userName:"CF22",cfId:"CF22"},
                    {userName:"CF32",cfId:"CF32"},
                    {userName:"CF46",cfId:"CF46"},
                    {userName:"CF4",cfId:"CF4"},
                    {userName:"CF54",cfId:"CF54"},
                    {userName:"CF60",cfId:"CF60"},
                    {userName:"CF30",cfId:"CF30"},
                    {userName:"CF28",cfId:"CF28"},
                    {userName:"CF68",cfId:"CF68"},
                    {userName:"CF27",cfId:"CF27"},
                    {userName:"CF35",cfId:"CF35"},
                    {userName:"CF33",cfId:"CF33"},
                    {userName:"CF7",cfId:"CF7"},
                    {userName:"CF65",cfId:"CF65"},
                    {userName:"CF56",cfId:"CF56"},
                    {userName:"CF24",cfId:"CF24"},
                    {userName:"TRAM",cfId:"WILL TRAM"},
                    {userName:"CF14",cfId:"CF14"},
                    {userName:"CF44",cfId:"CF44"},
                    {userName:"CF50",cfId:"CF50"},
                    {userName:"CF38",cfId:"CF38"},
                    {userName:"HARVEY",cfId:"VALERIE  HARVEY"},
                    {userName:"CF57",cfId:"CF57"},
                    {userName:"CF21",cfId:"CF21"},
                    {userName:"CF51",cfId:"CF51"},
                    {userName:"CF55",cfId:"CF55"},
                    {userName:"CF11",cfId:"CF11"},
                    {userName:"CF43",cfId:"CF43"},
                    {userName:"CF45",cfId:"CF45"},
                    {userName:"CRAFTSPERSON",cfId:"CRAFTSPERSON"},
                    {userName:"CF1",cfId:"CF1"},
                    {userName:"CF17",cfId:"CF17"},
                    {userName:"CF61",cfId:"CF61"},
                    {userName:"CF67",cfId:"CF67"},
                    {userName:"CF40",cfId:"CF40"},
                    {userName:"CF58",cfId:"CF58"},
                    {userName:"CF16",cfId:"CF16"},
                    {userName:"CF10",cfId:"CF10"},
                    {userName:"CF42",cfId:"CF42"},
                    {userName:"CF6",cfId:"CF6"},
                    {userName:"CF62",cfId:"CF62"}

                ];


                t.diag('Number of Users to Test: ' + users.length);

                var executeWfr = function (args) {
                    return Common.service.workflow.Workflow.execute('AbBldgOpsHelpDesk-MaintenanceMobileService-syncWorkData', [args.userName, args.cfId]);
                };

                // Register the test user
                Common.test.util.TestUser.registerTestUser('AFM', 'afm')
                    .then(function () {
                        return Common.service.Session.start();
                    })
                    .then(function () {
                        startTime = Date.now();
                        t.diag('start time ' + Date.now());
                        return Promise.all(users.map(executeWfr));
                    })
                    .then(function () {
                        var elapsedTime = Date.now() - startTime;
                        t.diag('Elpased Time ' + elapsedTime);
                        return Common.service.Session.end();
                    })
                    .then(function () {
                        t.endAsync(async);
                        t.done();
                    }, function (error) {
                        t.fail(error);
                        Common.service.Session.end()
                            .then(function () {
                                t.done();
                                t.endAsync(async);
                            });
                    })
                    .done();

            });


});