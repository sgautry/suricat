StartTest(function (t) {

    t.requireOk('Common.test.util.TestUser', 'Common.service.Session', 'Common.util.Network',
        'Common.util.TableDef', 'Common.store.TableDefs', 'Common.log.Logger',
        'Common.config.GlobalParameters', 'Common.service.workflow.Workflow', function () {

            var async = t.beginAsync(100000);

            var restriction = {};

            var getRecords = function () {
                return new Promise(function (resolve, reject) {
                    var options = {
                        async: true,
                        headers: {"cache-control": "no-cache"},
                        callback: resolve,
                        errorHandler: reject
                    };

                    var keys = {
                        bl_id: 'SRL',
                        fl_id: 'B1',
                        rm_id: 'B12'
                    };

                    MobileSyncService.retrievePagedRecordsByKey('rm', ['bl_id','fl_id','rm_id','rm_std'], restriction, 10, keys, options);
                });
            };

            restriction.clauses = [
                {
                    tableName: 'rm',
                    fieldName: 'bl_id',
                    operation: 'EQUALS',
                    value: 'HQ',
                    relativeOperation: 'OR_BRACKET'
                },
                {
                    tableName: 'rm',
                    fieldName: 'bl_id',
                    operation: 'EQUALS',
                    value: 'SLR',
                    relativeOperation: 'OR'
                },
                {
                    tableName: 'rm',
                    fieldName: 'bl_id',
                    operation: 'EQUALS',
                    value: 'XC',
                    relativeOperation: 'OR'
                }
            ];

            Common.test.util.TestUser.registerTestUser('AFM', 'afm')
                .then(function () {
                    // Clean up the local database before inserting recordss
                    //Common.test.util.Database.deleteWorkRequestSyncTestRecords(function () {
                    return Common.service.Session.start()
                        .then(function () {
                            return getRecords();
                        })
                        .then(function (records) {
                            return Common.service.Session.end();
                        })
                        .then(function () {
                            t.done();
                            t.endAsync(async);
                        })
                        .catch(function (error) {
                            t.fail(error);
                            Common.service.Session.end()
                                .then(function () {
                                    t.done();
                                    t.endAsync();
                                });
                        });

                });
        });
});