// Verify that a new configuration file is created when load is called the first time.

/* Disable JSHint warning of Missing 'new' prefix for the StartTest function. */
/* jshint newcap: false */
/* global StartTest */

StartTest(function (t) {
    t.requireOk('Common.util.ConfigFileManager', 'Common.lang.ComponentLocalizer', 'Ext.data.Validations',
        'Common.scripts.loader.Loader', 'Common.util.Format', 'Common.log.Logger', 'Common.device.File',
        function () {

            var loadConfigFileManager = function () {
                return new Promise(function (resolve) {
                    Common.util.ConfigFileManager.load(resolve);
                });
            };


            var async = t.beginAsync();
            Common.device.File.deleteFile('MobileClient.conf')
                .then(function () {
                    return loadConfigFileManager();
                })
                .then(function () {
                    return Common.device.File.fileExists('MobileClient.conf');
                })
                .then(function () {
                    t.pass('Config file created.');
                    t.endAsync(async);
                    t.done();
                })
                .catch(function (error) {
                    t.fail(error);
                    t.endAsync(async);
                    t.done();
                });
        });

});