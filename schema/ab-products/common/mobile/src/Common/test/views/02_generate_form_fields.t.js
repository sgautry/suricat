/* Disable JSHint warning of Missing 'new' prefix for the StartTest function. */
/*jshint newcap: false */
StartTest(function (t) {

    t.requireOk('Common.util.Ui', 'Common.store.AppPreferences', 'Common.store.TableDefs',
        'Common.log.Logger', 'Common.config.GlobalParameters', 'Ext.data.Types',
        'Common.test.util.TestUser', function () {

            var appPreferences = Ext.create('Common.store.AppPreferences');
            var tableDefStore = Ext.create('Common.store.TableDefs');
            var async = t.beginAsync();


            Common.test.util.TestUser.registerTestUser('AFM', 'afm')
                .then(function () {
                    return Common.service.Session.start();
                })
                .then(function () {
                    return Common.promise.util.TableDef.getTableDefFromServer('eq_audit');
                })
                .then(function (tableDef) {
                    return Common.promise.util.TableDef.saveTableDef(tableDef);
                })
                .then(function () {
                    return Common.service.Session.end();
                })
                .then(function () {
                    return Common.test.util.Database.loadStore(tableDefStore);
                })
                .then(function () {
                    return Common.test.util.Database.loadStore(appPreferences);
                })
                .then(function () {
                    Common.util.Ui.generateFormFields('eq_audit');
                    t.done();
                    t.endAsync(async);
                    return Promise.resolve();
                })
                .catch(function (error) {
                    t.fail(error);
                    Common.service.Session.end()
                        .then(function () {
                            t.done();
                            t.endAsync();
                        });
                });


        });
});