/**
 * Created by jmartin on 2/25/16.
 */

/* jshint newcap: false */
/* global StartTest */
StartTest(function (test) {

    test.requireOk('Common.scripts.ScriptManager', 'Common.service.Session',
        'Common.service.MobileSecurityServiceAdapter', 'Common.util.ConfigFileManager',
        'Common.test.util.TestUser', 'Common.log.Logger', function () {

            var async;
            var onFinish = function () {
                Common.service.Session.end();
                test.endAsync(async);
                test.done();
            };

            async = test.beginAsync();

            // Configure test user.
            Common.test.util.TestUser.registerTestUser('TRAM', 'afm')
                .then(function () {
                    return Common.service.Session.start();
                })
                .then(function() {
                    return MobileSecurityServiceAdapter.recordDeviceRegistration('TRAM', Common.test.util.TestUser.testUserDeviceId, 'TEST' );
                })
                .then(function() {
                    return MobileSecurityServiceAdapter.recordDeviceRegistration('AFM', '12345', 'TEST' );
                })
                .then(function() {
                    return Common.service.Session.end();
                })
                .then(null, function(error) {
                    test.fail(error.localizedMessage);
                })
                .done(onFinish, onFinish);

        });


});
