StartTest(function (test) {

    test.requireOk('Common.scripts.ScriptManager', 'Common.service.Session',
        'Common.service.MobileSecurityServiceAdapter', 'Common.util.ConfigFileManager',
        'Common.test.util.TestUser', 'Common.log.Logger', function () {

            var async;
            var onFinish = function () {
                Common.service.Session.end();
                test.endAsync(async);
                test.done();
            };

            async = test.beginAsync();

            // Configure test user.
            Common.test.util.TestUser.registerTestUser('AFM', 'afm')
                .then(function () {
                    return Common.service.Session.start();
                })
                .then(function() {
                    return MobileSecurityServiceAdapter.getLicensedActivies();
                })
                .then(function(isLicensed) {
                    return Common.service.Session.end();
                })
                .then(null, function(error) {
                    test.fail(error);
                })
                .done(onFinish, onFinish);

        });


});

