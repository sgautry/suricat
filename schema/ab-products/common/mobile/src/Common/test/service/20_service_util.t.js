/* jshint newcap:false */
/* global StartTest */
StartTest(function (t) {

    t.requireOk('Common.service.util.ServiceUtil', function () {

        var dataObject = {
                bl_id: 'HQ',
                fl_id: '01',
                rm_id: 'AB'
            },
            data,
            indexedObject,
            result;

        var uniqueIdentifier = ['bl_id', 'fl_id', 'rm_id'];
        var uniqueIdString = ServiceUtil.getUniqueIdString(uniqueIdentifier, dataObject);
        t.is(uniqueIdString, 'HQ01AB', 'Unique ID Strings match.');

        data = [
            {
                bl_id: 'H1',
                fl_id: '01',
                rm_id: 'AA'
            },
            {
                bl_id: 'HQ',
                fl_id: '02',
                rm_id: 'AB'
            },
            {
                bl_id: 'XC',
                fl_id: '03',
                rm_id: 'AC'
            }
        ];


        indexedObject = ServiceUtil.generateIndexedRecordsObject(uniqueIdentifier, data);

        result = indexedObject.HQ02AB;
        t.is(result.bl_id,'HQ', 'Building Code values match');
        t.is(result.fl_id,'02', 'Floor Code values match');
        t.is(result.rm_id,'AB', 'Room Code values match');
        t.done();
    });
});;