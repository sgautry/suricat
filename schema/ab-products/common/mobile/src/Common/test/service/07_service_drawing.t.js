/* Disable JSHint warning of Missing 'new' prefix for the StartTest function. */
/* global StartTest */
/* jshint newcap:false */

/**
 * Tests the Floorplan.util.FloorPlan.getDrawingsForFloor function.
 * Environment: Requires the HQ Canonical database
 */
StartTest(function (t) {

    t.requireOk('Ext.data.Types', 'Common.log.Logger', 'Common.scripts.ScriptManager', 'Common.util.ConfigFileManager',
        'Common.service.MobileSecurityServiceAdapter', 'Common.store.TableDownloads',
        'Common.test.util.TestUser', 'Common.service.Session', 'Floorplan.util.Floorplan', 'Common.service.drawing.Drawing',
        'Floorplan.model.PublishDate', 'Floorplan.store.PublishDates', 'Common.store.TableDefs', function () {

            var pkeyValues = [{
                bl_id: 'HQ',
                fl_id: '15'
            }];

            var planTypes = [
                '1 - ALLOCATION',
                '2 - CATEGORY',
                '3 - TYPE',
                '4 - OCCUPANCY',
                '5 - VACANCY',
                '6 - LEASE',
                '7 - EMERGENCY',
                '8 - HAZMAT',
                '9 - SURVEY'
            ];

            var async = t.beginAsync();
            var endTest = function () {
                Common.service.Session.end();
                t.endAsync(async);
                t.done();
            };

            Ext.create('Common.store.TableDefs');
            Ext.create('Floorplan.model.PublishDate');
            Ext.create('Floorplan.store.PublishDates');
            Ext.create('Common.store.TableDownloads');

            Common.test.util.TestUser.registerTestUser('TRAM', 'afm')
                .then(function () {
                    return Common.service.Session.start();
                })
                .then(function () {
                    return Floorplan.util.Floorplan.getDrawingsForFloors(pkeyValues, planTypes);
                })
                .then(function (drawingData) {
                    t.is(drawingData.floorPlansDownloaded, 1, "Number of downloaded floors is correct.");
                    // Check that each drawing data object contains SVG data
                    return Promise.resolve();
                })
                .then(null, function (error) {
                    t.fail(error);
                    return Promise.resolve();
                })
                .done(endTest, endTest);
        });
});