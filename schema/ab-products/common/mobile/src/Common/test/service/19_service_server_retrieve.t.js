/* jshint newcap:false */
/* global StartTest */
StartTest(function (t) {

    t.requireOk('Common.test.util.TestUser', 'Common.service.Session', 'Common.log.Logger',
        'Common.config.GlobalParameters', 'Common.service.server.Data', function() {


            var async = t.beginAsync();
            // Register the test user
            Common.test.util.TestUser.registerTestUser('AFM', 'afm')
                .then(function () {
                    return Common.service.Session.start();
                })
                .then(function() {
                    return Common.service.server.Data.retrieveRecords('bl',['bl_id','name'], null);
                })
                .then(function(records) {
                    t.isGreater(records.length, 0, 'Retrieved Records');
                })
                .then(function() {
                    return Common.service.Session.end();
                })
                .catch(function (error) {
                    t.fail(error);
                    Common.service.Session.end();
                })
                .done(function () {
                    t.endAsync(async);
                    t.done();
                });

    });
});