/* Disable JSHint warning of Missing 'new' prefix for the StartTest function. */
/* jshint newcap: false */
/* global StartTest */
StartTest(function (t) {

    t.diag('Test retrieving document version data from the client database.');

    t.requireOk('Common.store.sync.SyncStore', 'Common.log.Logger', 'Common.config.GlobalParameters',
        'Ext.data.Types', 'Common.data.Model', 'Common.model.Employee', 'Common.test.util.TestUser',
        'Common.store.Employees', 'Common.service.Session', 'Common.store.proxy.Sqlite',
        'Common.store.TableDefs', 'Common.store.TableDownloads', 'Maintenance.model.WorkRequest', function () {

            var syncStore = Ext.create('Common.store.sync.SyncStore', {
                model: 'Common.model.Employee',
                documentConfig: {
                    documentTable: 'em',
                    documentTableKeys: 'em_id'
                },
                proxy: {
                    type: 'Sqlite'
                }
            });

            var async = t.beginAsync();


            syncStore.getClientDocuments()
                .then(function (clientDocuments) {
                    return syncStore.restoreClientDocuments(clientDocuments);
                })
                .then(function () {
                    var clientDocs = syncStore.getClientDocuments();
                    return Promise.resolve(clientDocs);
                })
                .then(function (clientDocuments) {
                    syncStore.clientDocuments = clientDocuments;
                    var documentsForServer = syncStore.convertClientDocumentsForServer();
                })
                .then(function () {
                    t.endAsync(async);
                    t.done();
                });
        });

});            