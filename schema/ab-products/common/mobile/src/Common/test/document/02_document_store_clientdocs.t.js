/* Disable JSHint warning of Missing 'new' prefix for the StartTest function. */
/* jshint newcap: false */
/* global StartTest */
StartTest(function (t) {

    t.diag('Test retrieving document version data from the client database.');

    t.requireOk('Common.store.sync.SyncStore', 'Common.log.Logger', 'Common.config.GlobalParameters',
        'Ext.data.Types', 'Common.data.Model', 'Common.model.Employee', 'Common.test.util.TestUser',
        'Common.store.Employees','Common.service.Session', 'Common.store.proxy.Sqlite',
        'Common.store.TableDefs', 'Common.store.TableDownloads', function () {

            var syncStore = Ext.create('Common.store.sync.SyncStore', {
                model: 'Common.model.Employee',
                documentTable: 'em',
                documentTablePrimaryKeyFields: ['em_id'],
                proxy: {
                    type: 'Sqlite'
                }
            });

            var loadStore = function(store) {
                return new Promise(function(resolve, reject) {
                    store.load(resolve);
                });
            };

            var employeeStore = Ext.create('Common.store.Employees');

            t.isInstanceOf(syncStore, 'Common.store.sync.SyncStore', 'SyncStore is created.');

            // Register the tableDefs store in the StoreManager
            Ext.create('Common.store.TableDefs');
            Ext.create('Common.store.TableDownloads');

            var async = t.beginAsync();

            Common.test.util.TestUser.registerTestUser('TRAM', 'afm')
                .then(function () {
                    // Clean up the local database before inserting records
                    return Common.service.Session.start()
                        .then(function () {
                            return loadStore(employeeStore);
                        })
                        .then(function() {
                            return syncStore.getClientDocuments();
                        })
                        .then(function(clientDocuments) {
                            console.log(clientDocuments);
                        })
                        .then(function () {
                            return Common.service.Session.end();
                        })
                        .then(function () {
                            t.endAsync(async);
                            t.done();
                        })
                        .catch(function (error) {
                            t.fail(error);
                            Common.service.Session.end();
                        });
                });

            t.done();

        });

});