// Test the synchronization of WorkRequest records created on the mobile client
/* Disable JSHint warning of Missing 'new' prefix for the StartTest function. */
/* jshint newcap: false */
/* global StartTest */
StartTest(function (t) {

    t.diag('Test auto generation of document model fields');

    t.requireOk('Ext.data.Types', 'Common.type.TypeManager', 'Common.model.Employee', 'Ext.data.identifier.Uuid',
        'Common.model.Building',  'Maintenance.model.WorkRequest', 'Maintenance.model.Validation', function () {

            var model = Ext.create('Common.model.Employee');
            var modelFieldConfig = model.config.fields;
            var modelDocumentFields = model.getModelDocumentFields(modelFieldConfig);
            var documentConfig = model.generateDocumentFieldConfig(modelDocumentFields),
                documentName,
                mergedFields,
                emModelFields,
                wrFields;

            t.is(documentConfig.length, 5, 'DocumentConfig contains 5 elements');

            // Check the name of the generated fields.
            documentName = documentConfig[0].name;

            t.is(documentConfig[1].name, documentName + '_contents');
            t.is(documentConfig[2].name, documentName + '_isnew');
            t.is(documentConfig[3].name, documentName + '_file');
            t.is(documentConfig[4].name, documentName + '_version');

            // Test the merge of the document fields with the configured model fields
            mergedFields = model.mergeDocumentFields(modelFieldConfig, documentConfig);
            t.is(mergedFields.length,16, 'Number of merged fields is correct');

            // Check the generated fields for the model instance.
            emModelFields = model.getFields();

            t.is(emModelFields.all.length,16, 'Number of model fields matches.');
            t.is(emModelFields.containsKey('em_photo'),true, 'Model contains em_photo field');
            t.is(emModelFields.containsKey('em_photo_file'),true, 'Model contains em_photo_file field');
            t.is(emModelFields.containsKey('em_photo_contents'),true, 'Model contains em_photo_contents field');
            t.is(emModelFields.containsKey('em_photo_version'),true, 'Model contains em_photo_version field');
            t.is(emModelFields.containsKey('em_photo_isnew'),true, 'Model contains em_photo_isnew field');

            // Check fields of model that does not contain any document fields.
            model = Ext.create('Common.model.Building');
            t.isGreater(model.getFields().all.length, 0, 'Building model has fields');
            t.is(model.getFields().all.length, 7, 'Building model contains 7 fields');

            modelDocumentFields = model.getModelDocumentFields(model.config.fields);
            t.is(modelDocumentFields.length,0, 'Building model contains no document fields');

            documentConfig = model.generateDocumentFieldConfig(modelDocumentFields);
            t.is(documentConfig.length,0, 'Building model contains no document fields');
            mergedFields = model.mergeDocumentFields(model.config.fields, documentConfig);
            t.is(mergedFields.length, 7, 'Building model contains 7 document fields');

            // Check fields for a model that contains multiple document fields.
            model = Ext.create('Maintenance.model.WorkRequest');
            modelDocumentFields = model.getModelDocumentFields(model.config.fields);
            t.is(modelDocumentFields.length,4, 'WorkRequest model contains 4 document fields');

            documentConfig = model.generateDocumentFieldConfig(modelDocumentFields);
            
            // Verify that all Work Request document fields have been created.
            wrFields = model.getFields();
            modelDocumentFields.forEach(function(docField) {
                t.is(wrFields.containsKey(docField.name + '_file'),true, 'WorkRequest model contains ' + docField.name + '_file field');
                t.is(wrFields.containsKey(docField.name + '_contents'),true, 'WorkRequest model contains ' + docField.name + '_contents field');
                t.is(wrFields.containsKey(docField.name + '_version'),true, 'WorkRequest model contains ' + docField.name + '_version field');
                t.is(wrFields.containsKey(docField.name + '_isnew'),true, 'WorkRequest model contains ' + docField.name + '_isnew field');
            });

            t.done('finish');
        });
});   