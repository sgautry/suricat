// Test the synchronization of WorkRequest records created on the mobile client
/* Disable JSHint warning of Missing 'new' prefix for the StartTest function. */
/* jshint newcap: false */
/* global StartTest */
StartTest(function (t) {

    t.diag('Testing Work Request Document Sync');

    t.requireOk('Ext.data.Types', 'Common.type.TypeManager', 'Common.store.Employees',
        'Common.store.TableDefs', 'Common.service.Session', 'Common.store.TableDownloads',
        'Common.log.Logger', 'Common.config.GlobalParameters', 'Common.store.AppPreferences',
        'Common.test.util.TestUser', 'Common.test.util.DataLoader', function () {


            var employeeStore = Ext.create('Common.store.Employees');
            var appPreferencesStore = Ext.create('Common.store.AppPreferences');

            var async = t.beginAsync();

            // Register the tableDefs store in the StoreManager
            Ext.create('Common.store.TableDefs');
            var tableDownloadStore = Ext.create('Common.store.TableDownloads');

            // Configure test user.
            Common.test.util.TestUser.registerTestUser('AFM', 'afm')
                .then(function() {
                    return Common.test.util.Database.resetTableDownloadTimes();
                })
                .then(function() {
                    return Common.test.util.DataLoader.loadStore(tableDownloadStore);
                })
                .then(function () {
                    return Common.service.Session.start();
                })
                .then(function() {
                    return Common.test.util.Database.loadStore(appPreferencesStore);
                })
                .then(function() {
                    return appPreferencesStore.deleteAndImportRecords();
                })
                .then(function () {
                    return employeeStore.deleteAndImportRecords();
                })
                .then(function () {
                    return Common.service.Session.end();
                })
                .then(function() {
                    return Common.test.util.DataLoader.loadStore(employeeStore);
                })
                .then(function(records) {
                    return Promise.resolve();
                })
                .then(function () {
                    t.endAsync(async);
                    t.done();
                })
                .catch(function (error) {
                        Common.service.Session.end();
                        t.fail(error);
                        t.endAsync(async);
                        t.done();
                    }
                );
        });
});

