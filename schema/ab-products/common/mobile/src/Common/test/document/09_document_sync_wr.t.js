// Test the synchronization of WorkRequest records created on the mobile client
/* Disable JSHint warning of Missing 'new' prefix for the StartTest function. */
/* jshint newcap: false */
/* global StartTest */
StartTest(function (t) {

    t.diag('Testing Work Request Document Sync');

    t.requireOk('Ext.data.Types', 'Common.type.TypeManager', 'Maintenance.model.WorkRequest', 'Maintenance.store.WorkRequests',
        'Common.store.TableDefs', 'Common.service.Session',
        'Common.log.Logger', 'Common.config.GlobalParameters', 'Maintenance.model.Validation',
        'Common.test.util.TestUser', 'Common.test.util.DataLoader', 'Common.service.workflow.Workflow', function () {


            var workRequestStore = Ext.create('Maintenance.store.WorkRequests');
            var async = t.beginAsync();

            // Register the tableDefs store in the StoreManager
            Ext.create('Common.store.TableDefs');

            // Configure test user.
            Common.test.util.TestUser.registerTestUser('AFM', 'afm')
                .then(function () {
                    return Common.service.Session.start();
                })
                .then(function() {
                    return Workflow.execute('AbBldgOpsHelpDesk-MaintenanceMobileService-syncWorkData', ['AFM', 'AFM']);
                })
                .then(function () {
                    return workRequestStore.syncStore();
                })
                .then(function () {
                    return Common.service.Session.end();
                })
                .then(function() {
                    t.diag('Number of documents downloaded: ' + workRequestStore.numberOfDocumentsDownloaded);
                    workRequestStore.setFilters({property: 'wr_id', value: 1150000898});
                    return Common.test.util.DataLoader.loadStore(workRequestStore);
                })
                .then(function(records) {

                })
                .then(function () {
                    t.endAsync(async);
                    t.done();
                })
                .catch(function (error) {
                        Common.service.Session.end();
                        t.fail(error);
                        t.endAsync(async);
                        t.done();
                    }
                );
        });
});

