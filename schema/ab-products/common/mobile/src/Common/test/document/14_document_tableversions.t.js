/* Disable JSHint warning of Missing 'new' prefix for the StartTest function. */
/* jshint newcap: false */
/* global StartTest */
StartTest(function (t) {

    t.diag('Test retrieving all version fields from the client database.');

    t.requireOk('Common.promise.util.DatabaseOperation', 'Common.document.DocumentManager', 'Common.log.Logger',
        function () {

            Ext.define('TestClass', {
                mixins: ['Common.promise.util.DatabaseOperation'],

                sayHello: function() {
                    alert('hello');
                }
            });

            var testClass = Ext.create('TestClass');
            var async = t.beginAsync();

            testClass.getTablesAndVersionFields()
                .then(function(tables) {
                    return Common.document.DocumentManager.resetDocumentVersionFields(tables);
                })
                .then(function() {
                    t.done();
                    t.endAsync(async);
                });
        });
});