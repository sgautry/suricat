/* Disable JSHint warning of Missing 'new' prefix for the StartTest function. */
/* jshint newcap: false */
/* global StartTest */
StartTest(function (t) {
    t.requireOk('Common.test.util.TestUser','Common.store.proxy.SqliteView', 'Common.store.sync.SqliteStore',
        'Common.store.Buildings', 'Common.store.TableDefs', 'Common.store.TableDownloads',
        'Common.log.Logger', 'Ext.data.Types', function () {

            var buildingStore,
                numberOfFields,
                store,
                async;

            Ext.define('TestView', {
                extend: 'Common.store.sync.SqliteStore',
                requires: 'Common.store.proxy.SqliteView',

                config: {
                    storeId: 'testViewStore',
                    fields: [
                        {name: 'bl_id', type: 'string'},
                        {name: 'site_id', type: 'string'},
                        {name: 'name', type: 'string'}
                    ],
                    remoteSort: true,
                    autoLoad: false,
                    enableAutoLoad: true,
                    remoteFilter: true,
                    proxy: {
                        type: 'SqliteView',

                        viewDefinition: 'SELECT bl.bl_id,bl.site_id,bl.name FROM Building bl ORDER BY bl.name',

                        viewName: 'TestView',

                        baseTables: ['Building']
                    },
                    sorters: [
                        {property: 'bl_id', direction: 'DESC'}
                    ]
                }
            });

            // Stores required for the sync
            Ext.create('Common.store.TableDefs');
            Ext.create('Common.store.TableDownloads');

            // Create and load the Buildings store so that the Building table is created in the client database.
            store = Ext.create('TestView');
            buildingStore = Ext.create('Common.store.Buildings');

            async = t.beginAsync();

            // Sync the bl table so that we have records in the client database.

            Common.test.util.TestUser.registerTestUser('AFM','afm')
                .then(function() {
                    return Common.service.Session.start();
                })
                .then(function() {
                    return buildingStore.deleteAndImportRecords();
                })
                .then(function() {
                    return Common.service.Session.end();
                })
                .then(function() {
                    return Common.test.util.Database.loadStore(buildingStore);
                })
                .then(function () {
                    return Common.test.util.Database.loadStore(store);
                })
                .then(function (records) {
                    var recordsExist = (records.length > 0);

                    t.is(recordsExist, true, 'Retrieved records from view.');
                    if (recordsExist) {
                        numberOfFields = records[0].getFields().items.length;
                        // Check number of fields plus the id field
                        t.is(numberOfFields, 4, 'Number of view fields is OK');
                        store.getProxy().setViewDefinition("SELECT bl_id, site_id,name FROM Building WHERE bl_id = 'HQ' ");
                        return Common.test.util.Database.loadStore(store);
                    }
                    else {
                        return Promise.resolve([]);
                    }
                })
                .then(function (records) {
                    if (records.length > 0) {
                        numberOfFields = records[0].getFields().items.length;
                        t.is(numberOfFields, 4, 'Number of modified view fields is OK');
                    } else {
                        t.fail('No records returned');
                    }
                    return Promise.resolve();
                })
                .then(function () {
                    t.endAsync(async);
                    t.done();
                })
                .catch(function(error) {
                    t.fail(error);
                    return Common.service.Session.end()
                        .then(function() {
                            t.endAsync(async);
                            t.done();
                        });
                });
        });
});
