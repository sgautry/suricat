Ext.define('Common.test.util.DataLoader', {
    singleton: true,

    loadJsonData: function (dataFileName) {

        return new Promise(function (resolve, reject) {

                var xhr = new XMLHttpRequest();

                xhr.onreadystatechange = function () {
                    if (this.readyState === 4) {
                        if (this.status === 200) {
                            resolve(xhr.responseText);
                        } else {
                            reject(xhr.statusText);
                        }
                    }
                };

                xhr.error = function (error) {
                    reject(error);
                };

                xhr.open('GET', dataFileName + '?now=' + new Date(), true);
                xhr.send(null);

            }
        );

    },

    loadStore: function (store) {
        return new Promise(function (resolve) {
            store.load(function (records) {
                resolve(records);
            });
        });
    }
});