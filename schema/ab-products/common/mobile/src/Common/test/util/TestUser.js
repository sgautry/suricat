Ext.define('Common.test.util.TestUser', {

    requires: [
        'Common.util.Device',
        'Common.util.ConfigFileManager',
        'Common.service.MobileSecurityServiceAdapter'
    ],

    singleton: true,

    testUserDeviceId: null,

    registerTestUser: function(userName, password) {

        return Common.test.util.TestUser.loadConfig()
            .then(function() {
                if(ConfigFileManager.username === userName && ConfigFileManager.isRegistered && !Ext.isEmpty(ConfigFileManager.deviceId)) {
                    Common.test.util.TestUser.testUserDeviceId = ConfigFileManager.deviceId;
                    return Promise.resolve();
                } else {
                    ConfigFileManager.deviceId = Device.generateDeviceId();
                    ConfigFileManager.username = userName;
                    ConfigFileManager.isRegistered = false;
                    ConfigFileManager.isDeviceRegistered = true;  // Set to true to satisfy the Session#isClientRegistered method.

                    this.testUserDeviceId = ConfigFileManager.deviceId;
                    return Common.service.MobileSecurityServiceAdapter.registerDevice(ConfigFileManager.deviceId, userName, password, 'en_US')
                        .then(function() {
                            ConfigFileManager.isRegistered = true;
                            ConfigFileManager.isDeviceRegistered = true;
                            return Common.test.util.TestUser.saveConfig();
                        });
                }
            });
    },

    saveConfig: function() {
        return new Promise(function(resolve, reject) {
            ConfigFileManager.sync(resolve, reject);
        });
    },

    loadConfig: function() {
        return new Promise(function(resolve, reject) {
            ConfigFileManager.load(resolve, reject);
        });
    }
});