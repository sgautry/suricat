// Platform: ios
// a3732cb71d9b1dd590338e8cf44196f366d46da3
/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */
;(function () {
    var PLATFORM_VERSION_BUILD_LABEL = '4.3.1';
// file: src/scripts/require.js

    /*jshint -W079 */
    /*jshint -W020 */

    var require,
        define;

    (function () {
        var modules = {},
            // Stack of moduleIds currently being built.
            requireStack = [],
            // Map of module ID -> index into requireStack of modules currently being built.
            inProgressModules = {},
            SEPARATOR = ".";


        function build(module) {
            var factory = module.factory,
                localRequire = function (id) {
                    var resultantId = id;
                    //Its a relative path, so lop off the last portion and add the id (minus "./")
                    if (id.charAt(0) === ".") {
                        resultantId = module.id.slice(0, module.id.lastIndexOf(SEPARATOR)) + SEPARATOR + id.slice(2);
                    }
                    return require(resultantId);
                };
            module.exports = {};
            delete module.factory;
            factory(localRequire, module.exports, module);
            return module.exports;
        }

        require = function (id) {
            if (!modules[id]) {
                throw "module " + id + " not found";
            } else if (id in inProgressModules) {
                var cycle = requireStack.slice(inProgressModules[id]).join('->') + '->' + id;
                throw "Cycle in require graph: " + cycle;
            }
            if (modules[id].factory) {
                try {
                    inProgressModules[id] = requireStack.length;
                    requireStack.push(id);
                    return build(modules[id]);
                } finally {
                    delete inProgressModules[id];
                    requireStack.pop();
                }
            }
            return modules[id].exports;
        };

        define = function (id, factory) {
            if (modules[id]) {
                throw "module " + id + " already defined";
            }

            modules[id] = {
                id: id,
                factory: factory
            };
        };

        define.remove = function (id) {
            delete modules[id];
        };

        define.moduleMap = modules;
    })();

//Export for use in node
    if (typeof module === "object" && typeof require === "function") {
        module.exports.require = require;
        module.exports.define = define;
    }

// file: src/cordova.js
    define("cordova", function (require, exports, module) {

// Workaround for Windows 10 in hosted environment case
// http://www.w3.org/html/wg/drafts/html/master/browsers.html#named-access-on-the-window-object
        if (window.cordova && !(window.cordova instanceof HTMLElement)) {
            throw new Error("cordova already defined");
        }


        var channel = require('cordova/channel');
        var platform = require('cordova/platform');


        /**
         * Intercept calls to addEventListener + removeEventListener and handle deviceready,
         * resume, and pause events.
         */
        var m_document_addEventListener = document.addEventListener;
        var m_document_removeEventListener = document.removeEventListener;
        var m_window_addEventListener = window.addEventListener;
        var m_window_removeEventListener = window.removeEventListener;

        /**
         * Houses custom event handlers to intercept on document + window event listeners.
         */
        var documentEventHandlers = {},
            windowEventHandlers = {};

        document.addEventListener = function (evt, handler, capture) {
            var e = evt.toLowerCase();
            if (typeof documentEventHandlers[e] != 'undefined') {
                documentEventHandlers[e].subscribe(handler);
            } else {
                m_document_addEventListener.call(document, evt, handler, capture);
            }
        };

        window.addEventListener = function (evt, handler, capture) {
            var e = evt.toLowerCase();
            if (typeof windowEventHandlers[e] != 'undefined') {
                windowEventHandlers[e].subscribe(handler);
            } else {
                m_window_addEventListener.call(window, evt, handler, capture);
            }
        };

        document.removeEventListener = function (evt, handler, capture) {
            var e = evt.toLowerCase();
            // If unsubscribing from an event that is handled by a plugin
            if (typeof documentEventHandlers[e] != "undefined") {
                documentEventHandlers[e].unsubscribe(handler);
            } else {
                m_document_removeEventListener.call(document, evt, handler, capture);
            }
        };

        window.removeEventListener = function (evt, handler, capture) {
            var e = evt.toLowerCase();
            // If unsubscribing from an event that is handled by a plugin
            if (typeof windowEventHandlers[e] != "undefined") {
                windowEventHandlers[e].unsubscribe(handler);
            } else {
                m_window_removeEventListener.call(window, evt, handler, capture);
            }
        };

        function createEvent(type, data) {
            var event = document.createEvent('Events');
            event.initEvent(type, false, false);
            if (data) {
                for (var i in data) {
                    if (data.hasOwnProperty(i)) {
                        event[i] = data[i];
                    }
                }
            }
            return event;
        }


        var cordova = {
            define: define,
            require: require,
            version: PLATFORM_VERSION_BUILD_LABEL,
            platformVersion: PLATFORM_VERSION_BUILD_LABEL,
            platformId: platform.id,
            /**
             * Methods to add/remove your own addEventListener hijacking on document + window.
             */
            addWindowEventHandler: function (event) {
                return (windowEventHandlers[event] = channel.create(event));
            },
            addStickyDocumentEventHandler: function (event) {
                return (documentEventHandlers[event] = channel.createSticky(event));
            },
            addDocumentEventHandler: function (event) {
                return (documentEventHandlers[event] = channel.create(event));
            },
            removeWindowEventHandler: function (event) {
                delete windowEventHandlers[event];
            },
            removeDocumentEventHandler: function (event) {
                delete documentEventHandlers[event];
            },
            /**
             * Retrieve original event handlers that were replaced by Cordova
             *
             * @return object
             */
            getOriginalHandlers: function () {
                return {
                    'document': {
                        'addEventListener': m_document_addEventListener,
                        'removeEventListener': m_document_removeEventListener
                    },
                    'window': {
                        'addEventListener': m_window_addEventListener,
                        'removeEventListener': m_window_removeEventListener
                    }
                };
            },
            /**
             * Method to fire event from native code
             * bNoDetach is required for events which cause an exception which needs to be caught in native code
             */
            fireDocumentEvent: function (type, data, bNoDetach) {
                var evt = createEvent(type, data);
                if (typeof documentEventHandlers[type] != 'undefined') {
                    if (bNoDetach) {
                        documentEventHandlers[type].fire(evt);
                    }
                    else {
                        setTimeout(function () {
                            // Fire deviceready on listeners that were registered before cordova.js was loaded.
                            if (type == 'deviceready') {
                                document.dispatchEvent(evt);
                            }
                            documentEventHandlers[type].fire(evt);
                        }, 0);
                    }
                } else {
                    document.dispatchEvent(evt);
                }
            },
            fireWindowEvent: function (type, data) {
                var evt = createEvent(type, data);
                if (typeof windowEventHandlers[type] != 'undefined') {
                    setTimeout(function () {
                        windowEventHandlers[type].fire(evt);
                    }, 0);
                } else {
                    window.dispatchEvent(evt);
                }
            },

            /**
             * Plugin callback mechanism.
             */
            // Randomize the starting callbackId to avoid collisions after refreshing or navigating.
            // This way, it's very unlikely that any new callback would get the same callbackId as an old callback.
            callbackId: Math.floor(Math.random() * 2000000000),
            callbacks: {},
            callbackStatus: {
                NO_RESULT: 0,
                OK: 1,
                CLASS_NOT_FOUND_EXCEPTION: 2,
                ILLEGAL_ACCESS_EXCEPTION: 3,
                INSTANTIATION_EXCEPTION: 4,
                MALFORMED_URL_EXCEPTION: 5,
                IO_EXCEPTION: 6,
                INVALID_ACTION: 7,
                JSON_EXCEPTION: 8,
                ERROR: 9
            },

            /**
             * Called by native code when returning successful result from an action.
             */
            callbackSuccess: function (callbackId, args) {
                cordova.callbackFromNative(callbackId, true, args.status, [args.message], args.keepCallback);
            },

            /**
             * Called by native code when returning error result from an action.
             */
            callbackError: function (callbackId, args) {
                // TODO: Deprecate callbackSuccess and callbackError in favour of callbackFromNative.
                // Derive success from status.
                cordova.callbackFromNative(callbackId, false, args.status, [args.message], args.keepCallback);
            },

            /**
             * Called by native code when returning the result from an action.
             */
            callbackFromNative: function (callbackId, isSuccess, status, args, keepCallback) {
                try {
                    var callback = cordova.callbacks[callbackId];
                    if (callback) {
                        if (isSuccess && status == cordova.callbackStatus.OK) {
                            callback.success && callback.success.apply(null, args);
                        } else if (!isSuccess) {
                            callback.fail && callback.fail.apply(null, args);
                        }
                        /*
                         else
                         Note, this case is intentionally not caught.
                         this can happen if isSuccess is true, but callbackStatus is NO_RESULT
                         which is used to remove a callback from the list without calling the callbacks
                         typically keepCallback is false in this case
                         */
                        // Clear callback if not expecting any more results
                        if (!keepCallback) {
                            delete cordova.callbacks[callbackId];
                        }
                    }
                }
                catch (err) {
                    var msg = "Error in " + (isSuccess ? "Success" : "Error") + " callbackId: " + callbackId + " : " + err;
                    console && console.log && console.log(msg);
                    cordova.fireWindowEvent("cordovacallbackerror", {'message': msg});
                    throw err;
                }
            },
            addConstructor: function (func) {
                channel.onCordovaReady.subscribe(function () {
                    try {
                        func();
                    } catch (e) {
                        console.log("Failed to run constructor: " + e);
                    }
                });
            }
        };


        module.exports = cordova;

    });

// file: src/common/argscheck.js
    define("cordova/argscheck", function (require, exports, module) {

        var utils = require('cordova/utils');

        var moduleExports = module.exports;

        var typeMap = {
            'A': 'Array',
            'D': 'Date',
            'N': 'Number',
            'S': 'String',
            'F': 'Function',
            'O': 'Object'
        };

        function extractParamName(callee, argIndex) {
            return (/.*?\((.*?)\)/).exec(callee)[1].split(', ')[argIndex];
        }

        function checkArgs(spec, functionName, args, opt_callee) {
            if (!moduleExports.enableChecks) {
                return;
            }
            var errMsg = null;
            var typeName;
            for (var i = 0; i < spec.length; ++i) {
                var c = spec.charAt(i),
                    cUpper = c.toUpperCase(),
                    arg = args[i];
                // Asterix means allow anything.
                if (c == '*') {
                    continue;
                }
                typeName = utils.typeName(arg);
                if ((arg === null || arg === undefined) && c == cUpper) {
                    continue;
                }
                if (typeName != typeMap[cUpper]) {
                    errMsg = 'Expected ' + typeMap[cUpper];
                    break;
                }
            }
            if (errMsg) {
                errMsg += ', but got ' + typeName + '.';
                errMsg = 'Wrong type for parameter "' + extractParamName(opt_callee || args.callee, i) + '" of ' + functionName + ': ' + errMsg;
                // Don't log when running unit tests.
                if (typeof jasmine == 'undefined') {
                    console.error(errMsg);
                }
                throw TypeError(errMsg);
            }
        }

        function getValue(value, defaultValue) {
            return value === undefined ? defaultValue : value;
        }

        moduleExports.checkArgs = checkArgs;
        moduleExports.getValue = getValue;
        moduleExports.enableChecks = true;


    });

// file: src/common/base64.js
    define("cordova/base64", function (require, exports, module) {

        var base64 = exports;

        base64.fromArrayBuffer = function (arrayBuffer) {
            var array = new Uint8Array(arrayBuffer);
            return uint8ToBase64(array);
        };

        base64.toArrayBuffer = function (str) {
            var decodedStr = typeof atob != 'undefined' ? atob(str) : new Buffer(str, 'base64').toString('binary');
            var arrayBuffer = new ArrayBuffer(decodedStr.length);
            var array = new Uint8Array(arrayBuffer);
            for (var i = 0, len = decodedStr.length; i < len; i++) {
                array[i] = decodedStr.charCodeAt(i);
            }
            return arrayBuffer;
        };

//------------------------------------------------------------------------------

        /* This code is based on the performance tests at http://jsperf.com/b64tests
         * This 12-bit-at-a-time algorithm was the best performing version on all
         * platforms tested.
         */

        var b64_6bit = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        var b64_12bit;

        var b64_12bitTable = function () {
            b64_12bit = [];
            for (var i = 0; i < 64; i++) {
                for (var j = 0; j < 64; j++) {
                    b64_12bit[i * 64 + j] = b64_6bit[i] + b64_6bit[j];
                }
            }
            b64_12bitTable = function () {
                return b64_12bit;
            };
            return b64_12bit;
        };

        function uint8ToBase64(rawData) {
            var numBytes = rawData.byteLength;
            var output = "";
            var segment;
            var table = b64_12bitTable();
            for (var i = 0; i < numBytes - 2; i += 3) {
                segment = (rawData[i] << 16) + (rawData[i + 1] << 8) + rawData[i + 2];
                output += table[segment >> 12];
                output += table[segment & 0xfff];
            }
            if (numBytes - i == 2) {
                segment = (rawData[i] << 16) + (rawData[i + 1] << 8);
                output += table[segment >> 12];
                output += b64_6bit[(segment & 0xfff) >> 6];
                output += '=';
            } else if (numBytes - i == 1) {
                segment = (rawData[i] << 16);
                output += table[segment >> 12];
                output += '==';
            }
            return output;
        }

    });

// file: src/common/builder.js
    define("cordova/builder", function (require, exports, module) {

        var utils = require('cordova/utils');

        function each(objects, func, context) {
            for (var prop in objects) {
                if (objects.hasOwnProperty(prop)) {
                    func.apply(context, [objects[prop], prop]);
                }
            }
        }

        function clobber(obj, key, value) {
            exports.replaceHookForTesting(obj, key);
            var needsProperty = false;
            try {
                obj[key] = value;
            } catch (e) {
                needsProperty = true;
            }
            // Getters can only be overridden by getters.
            if (needsProperty || obj[key] !== value) {
                utils.defineGetter(obj, key, function () {
                    return value;
                });
            }
        }

        function assignOrWrapInDeprecateGetter(obj, key, value, message) {
            if (message) {
                utils.defineGetter(obj, key, function () {
                    console.log(message);
                    delete obj[key];
                    clobber(obj, key, value);
                    return value;
                });
            } else {
                clobber(obj, key, value);
            }
        }

        function include(parent, objects, clobber, merge) {
            each(objects, function (obj, key) {
                try {
                    var result = obj.path ? require(obj.path) : {};

                    if (clobber) {
                        // Clobber if it doesn't exist.
                        if (typeof parent[key] === 'undefined') {
                            assignOrWrapInDeprecateGetter(parent, key, result, obj.deprecated);
                        } else if (typeof obj.path !== 'undefined') {
                            // If merging, merge properties onto parent, otherwise, clobber.
                            if (merge) {
                                recursiveMerge(parent[key], result);
                            } else {
                                assignOrWrapInDeprecateGetter(parent, key, result, obj.deprecated);
                            }
                        }
                        result = parent[key];
                    } else {
                        // Overwrite if not currently defined.
                        if (typeof parent[key] == 'undefined') {
                            assignOrWrapInDeprecateGetter(parent, key, result, obj.deprecated);
                        } else {
                            // Set result to what already exists, so we can build children into it if they exist.
                            result = parent[key];
                        }
                    }

                    if (obj.children) {
                        include(result, obj.children, clobber, merge);
                    }
                } catch (e) {
                    utils.alert('Exception building Cordova JS globals: ' + e + ' for key "' + key + '"');
                }
            });
        }

        /**
         * Merge properties from one object onto another recursively.  Properties from
         * the src object will overwrite existing target property.
         *
         * @param target Object to merge properties into.
         * @param src Object to merge properties from.
         */
        function recursiveMerge(target, src) {
            for (var prop in src) {
                if (src.hasOwnProperty(prop)) {
                    if (target.prototype && target.prototype.constructor === target) {
                        // If the target object is a constructor override off prototype.
                        clobber(target.prototype, prop, src[prop]);
                    } else {
                        if (typeof src[prop] === 'object' && typeof target[prop] === 'object') {
                            recursiveMerge(target[prop], src[prop]);
                        } else {
                            clobber(target, prop, src[prop]);
                        }
                    }
                }
            }
        }

        exports.buildIntoButDoNotClobber = function (objects, target) {
            include(target, objects, false, false);
        };
        exports.buildIntoAndClobber = function (objects, target) {
            include(target, objects, true, false);
        };
        exports.buildIntoAndMerge = function (objects, target) {
            include(target, objects, true, true);
        };
        exports.recursiveMerge = recursiveMerge;
        exports.assignOrWrapInDeprecateGetter = assignOrWrapInDeprecateGetter;
        exports.replaceHookForTesting = function () {
        };

    });

// file: src/common/channel.js
    define("cordova/channel", function (require, exports, module) {

        var utils = require('cordova/utils'),
            nextGuid = 1;

        /**
         * Custom pub-sub "channel" that can have functions subscribed to it
         * This object is used to define and control firing of events for
         * cordova initialization, as well as for custom events thereafter.
         *
         * The order of events during page load and Cordova startup is as follows:
         *
         * onDOMContentLoaded*         Internal event that is received when the web page is loaded and parsed.
         * onNativeReady*              Internal event that indicates the Cordova native side is ready.
         * onCordovaReady*             Internal event fired when all Cordova JavaScript objects have been created.
         * onDeviceReady*              User event fired to indicate that Cordova is ready
         * onResume                    User event fired to indicate a start/resume lifecycle event
         * onPause                     User event fired to indicate a pause lifecycle event
         *
         * The events marked with an * are sticky. Once they have fired, they will stay in the fired state.
         * All listeners that subscribe after the event is fired will be executed right away.
         *
         * The only Cordova events that user code should register for are:
         *      deviceready           Cordova native code is initialized and Cordova APIs can be called from JavaScript
         *      pause                 App has moved to background
         *      resume                App has returned to foreground
         *
         * Listeners can be registered as:
         *      document.addEventListener("deviceready", myDeviceReadyListener, false);
         *      document.addEventListener("resume", myResumeListener, false);
         *      document.addEventListener("pause", myPauseListener, false);
         *
         * The DOM lifecycle events should be used for saving and restoring state
         *      window.onload
         *      window.onunload
         *
         */

        /**
         * Channel
         * @constructor
         * @param type  String the channel name
         */
        var Channel = function (type, sticky) {
                this.type = type;
                // Map of guid -> function.
                this.handlers = {};
                // 0 = Non-sticky, 1 = Sticky non-fired, 2 = Sticky fired.
                this.state = sticky ? 1 : 0;
                // Used in sticky mode to remember args passed to fire().
                this.fireArgs = null;
                // Used by onHasSubscribersChange to know if there are any listeners.
                this.numHandlers = 0;
                // Function that is called when the first listener is subscribed, or when
                // the last listener is unsubscribed.
                this.onHasSubscribersChange = null;
            },
            channel = {
                /**
                 * Calls the provided function only after all of the channels specified
                 * have been fired. All channels must be sticky channels.
                 */
                join: function (h, c) {
                    var len = c.length,
                        i = len,
                        f = function () {
                            if (!(--i)) h();
                        };
                    for (var j = 0; j < len; j++) {
                        if (c[j].state === 0) {
                            throw Error('Can only use join with sticky channels.');
                        }
                        c[j].subscribe(f);
                    }
                    if (!len) h();
                },
                create: function (type) {
                    return channel[type] = new Channel(type, false);
                },
                createSticky: function (type) {
                    return channel[type] = new Channel(type, true);
                },

                /**
                 * cordova Channels that must fire before "deviceready" is fired.
                 */
                deviceReadyChannelsArray: [],
                deviceReadyChannelsMap: {},

                /**
                 * Indicate that a feature needs to be initialized before it is ready to be used.
                 * This holds up Cordova's "deviceready" event until the feature has been initialized
                 * and Cordova.initComplete(feature) is called.
                 *
                 * @param feature {String}     The unique feature name
                 */
                waitForInitialization: function (feature) {
                    if (feature) {
                        var c = channel[feature] || this.createSticky(feature);
                        this.deviceReadyChannelsMap[feature] = c;
                        this.deviceReadyChannelsArray.push(c);
                    }
                },

                /**
                 * Indicate that initialization code has completed and the feature is ready to be used.
                 *
                 * @param feature {String}     The unique feature name
                 */
                initializationComplete: function (feature) {
                    var c = this.deviceReadyChannelsMap[feature];
                    if (c) {
                        c.fire();
                    }
                }
            };

        function checkSubscriptionArgument(argument) {
            if (typeof argument !== "function" && typeof argument.handleEvent !== "function") {
                throw new Error(
                    "Must provide a function or an EventListener object " +
                    "implementing the handleEvent interface."
                );
            }
        }

        /**
         * Subscribes the given function to the channel. Any time that
         * Channel.fire is called so too will the function.
         * Optionally specify an execution context for the function
         * and a guid that can be used to stop subscribing to the channel.
         * Returns the guid.
         */
        Channel.prototype.subscribe = function (eventListenerOrFunction, eventListener) {
            checkSubscriptionArgument(eventListenerOrFunction);
            var handleEvent, guid;

            if (eventListenerOrFunction && typeof eventListenerOrFunction === "object") {
                // Received an EventListener object implementing the handleEvent interface
                handleEvent = eventListenerOrFunction.handleEvent;
                eventListener = eventListenerOrFunction;
            } else {
                // Received a function to handle event
                handleEvent = eventListenerOrFunction;
            }

            if (this.state == 2) {
                handleEvent.apply(eventListener || this, this.fireArgs);
                return;
            }

            guid = eventListenerOrFunction.observer_guid;
            if (typeof eventListener === "object") {
                handleEvent = utils.close(eventListener, handleEvent);
            }

            if (!guid) {
                // First time any channel has seen this subscriber
                guid = '' + nextGuid++;
            }
            handleEvent.observer_guid = guid;
            eventListenerOrFunction.observer_guid = guid;

            // Don't add the same handler more than once.
            if (!this.handlers[guid]) {
                this.handlers[guid] = handleEvent;
                this.numHandlers++;
                if (this.numHandlers == 1) {
                    this.onHasSubscribersChange && this.onHasSubscribersChange();
                }
            }
        };

        /**
         * Unsubscribes the function with the given guid from the channel.
         */
        Channel.prototype.unsubscribe = function (eventListenerOrFunction) {
            checkSubscriptionArgument(eventListenerOrFunction);
            var handleEvent, guid, handler;

            if (eventListenerOrFunction && typeof eventListenerOrFunction === "object") {
                // Received an EventListener object implementing the handleEvent interface
                handleEvent = eventListenerOrFunction.handleEvent;
            } else {
                // Received a function to handle event
                handleEvent = eventListenerOrFunction;
            }

            guid = handleEvent.observer_guid;
            handler = this.handlers[guid];
            if (handler) {
                delete this.handlers[guid];
                this.numHandlers--;
                if (this.numHandlers === 0) {
                    this.onHasSubscribersChange && this.onHasSubscribersChange();
                }
            }
        };

        /**
         * Calls all functions subscribed to this channel.
         */
        Channel.prototype.fire = function (e) {
            var fail = false,
                fireArgs = Array.prototype.slice.call(arguments);
            // Apply stickiness.
            if (this.state == 1) {
                this.state = 2;
                this.fireArgs = fireArgs;
            }
            if (this.numHandlers) {
                // Copy the values first so that it is safe to modify it from within
                // callbacks.
                var toCall = [];
                for (var item in this.handlers) {
                    toCall.push(this.handlers[item]);
                }
                for (var i = 0; i < toCall.length; ++i) {
                    toCall[i].apply(this, fireArgs);
                }
                if (this.state == 2 && this.numHandlers) {
                    this.numHandlers = 0;
                    this.handlers = {};
                    this.onHasSubscribersChange && this.onHasSubscribersChange();
                }
            }
        };


// defining them here so they are ready super fast!
// DOM event that is received when the web page is loaded and parsed.
        channel.createSticky('onDOMContentLoaded');

// Event to indicate the Cordova native side is ready.
        channel.createSticky('onNativeReady');

// Event to indicate that all Cordova JavaScript objects have been created
// and it's time to run plugin constructors.
        channel.createSticky('onCordovaReady');

// Event to indicate that all automatically loaded JS plugins are loaded and ready.
// FIXME remove this
        channel.createSticky('onPluginsReady');

// Event to indicate that Cordova is ready
        channel.createSticky('onDeviceReady');

// Event to indicate a resume lifecycle event
        channel.create('onResume');

// Event to indicate a pause lifecycle event
        channel.create('onPause');

// Channels that must fire before "deviceready" is fired.
        channel.waitForInitialization('onCordovaReady');
        channel.waitForInitialization('onDOMContentLoaded');

        module.exports = channel;

    });

// file: /Users/shazron/Documents/git/apache/cordova-ios/cordova-js-src/exec.js
    define("cordova/exec", function (require, exports, module) {

        /*global require, module, atob, document */

        /**
         * Creates a gap bridge iframe used to notify the native code about queued
         * commands.
         */
        var cordova = require('cordova'),
            utils = require('cordova/utils'),
            base64 = require('cordova/base64'),
            execIframe,
            commandQueue = [], // Contains pending JS->Native messages.
            isInContextOfEvalJs = 0,
            failSafeTimerId = 0;

        function massageArgsJsToNative(args) {
            if (!args || utils.typeName(args) != 'Array') {
                return args;
            }
            var ret = [];
            args.forEach(function (arg, i) {
                if (utils.typeName(arg) == 'ArrayBuffer') {
                    ret.push({
                        'CDVType': 'ArrayBuffer',
                        'data': base64.fromArrayBuffer(arg)
                    });
                } else {
                    ret.push(arg);
                }
            });
            return ret;
        }

        function massageMessageNativeToJs(message) {
            if (message.CDVType == 'ArrayBuffer') {
                var stringToArrayBuffer = function (str) {
                    var ret = new Uint8Array(str.length);
                    for (var i = 0; i < str.length; i++) {
                        ret[i] = str.charCodeAt(i);
                    }
                    return ret.buffer;
                };
                var base64ToArrayBuffer = function (b64) {
                    return stringToArrayBuffer(atob(b64));
                };
                message = base64ToArrayBuffer(message.data);
            }
            return message;
        }

        function convertMessageToArgsNativeToJs(message) {
            var args = [];
            if (!message || !message.hasOwnProperty('CDVType')) {
                args.push(message);
            } else if (message.CDVType == 'MultiPart') {
                message.messages.forEach(function (e) {
                    args.push(massageMessageNativeToJs(e));
                });
            } else {
                args.push(massageMessageNativeToJs(message));
            }
            return args;
        }

        function iOSExec() {

            var successCallback, failCallback, service, action, actionArgs;
            var callbackId = null;
            if (typeof arguments[0] !== 'string') {
                // FORMAT ONE
                successCallback = arguments[0];
                failCallback = arguments[1];
                service = arguments[2];
                action = arguments[3];
                actionArgs = arguments[4];

                // Since we need to maintain backwards compatibility, we have to pass
                // an invalid callbackId even if no callback was provided since plugins
                // will be expecting it. The Cordova.exec() implementation allocates
                // an invalid callbackId and passes it even if no callbacks were given.
                callbackId = 'INVALID';
            } else {
                throw new Error('The old format of this exec call has been removed (deprecated since 2.1). Change to: ' +
                    'cordova.exec(null, null, \'Service\', \'action\', [ arg1, arg2 ]);'
                );
            }

            // If actionArgs is not provided, default to an empty array
            actionArgs = actionArgs || [];

            // Register the callbacks and add the callbackId to the positional
            // arguments if given.
            if (successCallback || failCallback) {
                callbackId = service + cordova.callbackId++;
                cordova.callbacks[callbackId] =
                    {success: successCallback, fail: failCallback};
            }

            actionArgs = massageArgsJsToNative(actionArgs);

            var command = [callbackId, service, action, actionArgs];

            // Stringify and queue the command. We stringify to command now to
            // effectively clone the command arguments in case they are mutated before
            // the command is executed.
            commandQueue.push(JSON.stringify(command));

            // If we're in the context of a stringByEvaluatingJavaScriptFromString call,
            // then the queue will be flushed when it returns; no need for a poke.
            // Also, if there is already a command in the queue, then we've already
            // poked the native side, so there is no reason to do so again.
            if (!isInContextOfEvalJs && commandQueue.length == 1) {
                pokeNative();
            }
        }

// CB-10530
        function proxyChanged() {
            var cexec = cordovaExec();

            return (execProxy !== cexec && // proxy objects are different
                iOSExec !== cexec      // proxy object is not the current iOSExec
            );
        }

// CB-10106
        function handleBridgeChange() {
            if (proxyChanged()) {
                var commandString = commandQueue.shift();
                while (commandString) {
                    var command = JSON.parse(commandString);
                    var callbackId = command[0];
                    var service = command[1];
                    var action = command[2];
                    var actionArgs = command[3];
                    var callbacks = cordova.callbacks[callbackId] || {};

                    execProxy(callbacks.success, callbacks.fail, service, action, actionArgs);

                    commandString = commandQueue.shift();
                }
                ;
                return true;
            }

            return false;
        }

        function pokeNative() {
            // CB-5488 - Don't attempt to create iframe before document.body is available.
            if (!document.body) {
                setTimeout(pokeNative);
                return;
            }

            // Check if they've removed it from the DOM, and put it back if so.
            if (execIframe && execIframe.contentWindow) {
                execIframe.contentWindow.location = 'gap://ready';
            } else {
                execIframe = document.createElement('iframe');
                execIframe.style.display = 'none';
                execIframe.src = 'gap://ready';
                document.body.appendChild(execIframe);
            }
            // Use a timer to protect against iframe being unloaded during the poke (CB-7735).
            // This makes the bridge ~ 7% slower, but works around the poke getting lost
            // when the iframe is removed from the DOM.
            // An onunload listener could be used in the case where the iframe has just been
            // created, but since unload events fire only once, it doesn't work in the normal
            // case of iframe reuse (where unload will have already fired due to the attempted
            // navigation of the page).
            failSafeTimerId = setTimeout(function () {
                if (commandQueue.length) {
                    // CB-10106 - flush the queue on bridge change
                    if (!handleBridgeChange()) {
                        pokeNative();
                    }
                }
            }, 50); // Making this > 0 improves performance (marginally) in the normal case (where it doesn't fire).
        }

        iOSExec.nativeFetchMessages = function () {
            // Stop listing for window detatch once native side confirms poke.
            if (failSafeTimerId) {
                clearTimeout(failSafeTimerId);
                failSafeTimerId = 0;
            }
            // Each entry in commandQueue is a JSON string already.
            if (!commandQueue.length) {
                return '';
            }
            var json = '[' + commandQueue.join(',') + ']';
            commandQueue.length = 0;
            return json;
        };

        iOSExec.nativeCallback = function (callbackId, status, message, keepCallback, debug) {
            return iOSExec.nativeEvalAndFetch(function () {
                var success = status === 0 || status === 1;
                var args = convertMessageToArgsNativeToJs(message);

                function nc2() {
                    cordova.callbackFromNative(callbackId, success, status, args, keepCallback);
                }

                setTimeout(nc2, 0);
            });
        };

        iOSExec.nativeEvalAndFetch = function (func) {
            // This shouldn't be nested, but better to be safe.
            isInContextOfEvalJs++;
            try {
                func();
                return iOSExec.nativeFetchMessages();
            } finally {
                isInContextOfEvalJs--;
            }
        };

// Proxy the exec for bridge changes. See CB-10106

        function cordovaExec() {
            var cexec = require('cordova/exec');
            var cexec_valid = (typeof cexec.nativeFetchMessages === 'function') && (typeof cexec.nativeEvalAndFetch === 'function') && (typeof cexec.nativeCallback === 'function');
            return (cexec_valid && execProxy !== cexec) ? cexec : iOSExec;
        }

        function execProxy() {
            cordovaExec().apply(null, arguments);
        };

        execProxy.nativeFetchMessages = function () {
            return cordovaExec().nativeFetchMessages.apply(null, arguments);
        };

        execProxy.nativeEvalAndFetch = function () {
            return cordovaExec().nativeEvalAndFetch.apply(null, arguments);
        };

        execProxy.nativeCallback = function () {
            return cordovaExec().nativeCallback.apply(null, arguments);
        };

        module.exports = execProxy;

    });

// file: src/common/exec/proxy.js
    define("cordova/exec/proxy", function (require, exports, module) {


// internal map of proxy function
        var CommandProxyMap = {};

        module.exports = {

            // example: cordova.commandProxy.add("Accelerometer",{getCurrentAcceleration: function(successCallback, errorCallback, options) {...},...);
            add: function (id, proxyObj) {
                console.log("adding proxy for " + id);
                CommandProxyMap[id] = proxyObj;
                return proxyObj;
            },

            // cordova.commandProxy.remove("Accelerometer");
            remove: function (id) {
                var proxy = CommandProxyMap[id];
                delete CommandProxyMap[id];
                CommandProxyMap[id] = null;
                return proxy;
            },

            get: function (service, action) {
                return ( CommandProxyMap[service] ? CommandProxyMap[service][action] : null );
            }
        };
    });

// file: src/common/init.js
    define("cordova/init", function (require, exports, module) {

        var channel = require('cordova/channel');
        var cordova = require('cordova');
        var modulemapper = require('cordova/modulemapper');
        var platform = require('cordova/platform');
        var pluginloader = require('cordova/pluginloader');
        var utils = require('cordova/utils');

        var platformInitChannelsArray = [channel.onNativeReady, channel.onPluginsReady];

        function logUnfiredChannels(arr) {
            for (var i = 0; i < arr.length; ++i) {
                if (arr[i].state != 2) {
                    console.log('Channel not fired: ' + arr[i].type);
                }
            }
        }

        window.setTimeout(function () {
            if (channel.onDeviceReady.state != 2) {
                console.log('deviceready has not fired after 5 seconds.');
                logUnfiredChannels(platformInitChannelsArray);
                logUnfiredChannels(channel.deviceReadyChannelsArray);
            }
        }, 5000);

// Replace navigator before any modules are required(), to ensure it happens as soon as possible.
// We replace it so that properties that can't be clobbered can instead be overridden.
        function replaceNavigator(origNavigator) {
            var CordovaNavigator = function () {
            };
            CordovaNavigator.prototype = origNavigator;
            var newNavigator = new CordovaNavigator();
            // This work-around really only applies to new APIs that are newer than Function.bind.
            // Without it, APIs such as getGamepads() break.
            if (CordovaNavigator.bind) {
                for (var key in origNavigator) {
                    if (typeof origNavigator[key] == 'function') {
                        newNavigator[key] = origNavigator[key].bind(origNavigator);
                    }
                    else {
                        (function (k) {
                            utils.defineGetterSetter(newNavigator, key, function () {
                                return origNavigator[k];
                            });
                        })(key);
                    }
                }
            }
            return newNavigator;
        }

        if (window.navigator) {
            window.navigator = replaceNavigator(window.navigator);
        }

        if (!window.console) {
            window.console = {
                log: function () {
                }
            };
        }
        if (!window.console.warn) {
            window.console.warn = function (msg) {
                this.log("warn: " + msg);
            };
        }

// Register pause, resume and deviceready channels as events on document.
        channel.onPause = cordova.addDocumentEventHandler('pause');
        channel.onResume = cordova.addDocumentEventHandler('resume');
        channel.onActivated = cordova.addDocumentEventHandler('activated');
        channel.onDeviceReady = cordova.addStickyDocumentEventHandler('deviceready');

// Listen for DOMContentLoaded and notify our channel subscribers.
        if (document.readyState == 'complete' || document.readyState == 'interactive') {
            channel.onDOMContentLoaded.fire();
        } else {
            document.addEventListener('DOMContentLoaded', function () {
                channel.onDOMContentLoaded.fire();
            }, false);
        }

// _nativeReady is global variable that the native side can set
// to signify that the native code is ready. It is a global since
// it may be called before any cordova JS is ready.
        if (window._nativeReady) {
            channel.onNativeReady.fire();
        }

        modulemapper.clobbers('cordova', 'cordova');
        modulemapper.clobbers('cordova/exec', 'cordova.exec');
        modulemapper.clobbers('cordova/exec', 'Cordova.exec');

// Call the platform-specific initialization.
        platform.bootstrap && platform.bootstrap();

// Wrap in a setTimeout to support the use-case of having plugin JS appended to cordova.js.
// The delay allows the attached modules to be defined before the plugin loader looks for them.
        setTimeout(function () {
            pluginloader.load(function () {
                channel.onPluginsReady.fire();
            });
        }, 0);

        /**
         * Create all cordova objects once native side is ready.
         */
        channel.join(function () {
            modulemapper.mapModules(window);

            platform.initialize && platform.initialize();

            // Fire event to notify that all objects are created
            channel.onCordovaReady.fire();

            // Fire onDeviceReady event once page has fully loaded, all
            // constructors have run and cordova info has been received from native
            // side.
            channel.join(function () {
                require('cordova').fireDocumentEvent('deviceready');
            }, channel.deviceReadyChannelsArray);

        }, platformInitChannelsArray);


    });

// file: src/common/init_b.js
    define("cordova/init_b", function (require, exports, module) {

        var channel = require('cordova/channel');
        var cordova = require('cordova');
        var modulemapper = require('cordova/modulemapper');
        var platform = require('cordova/platform');
        var pluginloader = require('cordova/pluginloader');
        var utils = require('cordova/utils');

        var platformInitChannelsArray = [channel.onDOMContentLoaded, channel.onNativeReady, channel.onPluginsReady];

// setting exec
        cordova.exec = require('cordova/exec');

        function logUnfiredChannels(arr) {
            for (var i = 0; i < arr.length; ++i) {
                if (arr[i].state != 2) {
                    console.log('Channel not fired: ' + arr[i].type);
                }
            }
        }

        window.setTimeout(function () {
            if (channel.onDeviceReady.state != 2) {
                console.log('deviceready has not fired after 5 seconds.');
                logUnfiredChannels(platformInitChannelsArray);
                logUnfiredChannels(channel.deviceReadyChannelsArray);
            }
        }, 5000);

// Replace navigator before any modules are required(), to ensure it happens as soon as possible.
// We replace it so that properties that can't be clobbered can instead be overridden.
        function replaceNavigator(origNavigator) {
            var CordovaNavigator = function () {
            };
            CordovaNavigator.prototype = origNavigator;
            var newNavigator = new CordovaNavigator();
            // This work-around really only applies to new APIs that are newer than Function.bind.
            // Without it, APIs such as getGamepads() break.
            if (CordovaNavigator.bind) {
                for (var key in origNavigator) {
                    if (typeof origNavigator[key] == 'function') {
                        newNavigator[key] = origNavigator[key].bind(origNavigator);
                    }
                    else {
                        (function (k) {
                            utils.defineGetterSetter(newNavigator, key, function () {
                                return origNavigator[k];
                            });
                        })(key);
                    }
                }
            }
            return newNavigator;
        }

        if (window.navigator) {
            window.navigator = replaceNavigator(window.navigator);
        }

        if (!window.console) {
            window.console = {
                log: function () {
                }
            };
        }
        if (!window.console.warn) {
            window.console.warn = function (msg) {
                this.log("warn: " + msg);
            };
        }

// Register pause, resume and deviceready channels as events on document.
        channel.onPause = cordova.addDocumentEventHandler('pause');
        channel.onResume = cordova.addDocumentEventHandler('resume');
        channel.onActivated = cordova.addDocumentEventHandler('activated');
        channel.onDeviceReady = cordova.addStickyDocumentEventHandler('deviceready');

// Listen for DOMContentLoaded and notify our channel subscribers.
        if (document.readyState == 'complete' || document.readyState == 'interactive') {
            channel.onDOMContentLoaded.fire();
        } else {
            document.addEventListener('DOMContentLoaded', function () {
                channel.onDOMContentLoaded.fire();
            }, false);
        }

// _nativeReady is global variable that the native side can set
// to signify that the native code is ready. It is a global since
// it may be called before any cordova JS is ready.
        if (window._nativeReady) {
            channel.onNativeReady.fire();
        }

// Call the platform-specific initialization.
        platform.bootstrap && platform.bootstrap();

// Wrap in a setTimeout to support the use-case of having plugin JS appended to cordova.js.
// The delay allows the attached modules to be defined before the plugin loader looks for them.
        setTimeout(function () {
            pluginloader.load(function () {
                channel.onPluginsReady.fire();
            });
        }, 0);

        /**
         * Create all cordova objects once native side is ready.
         */
        channel.join(function () {
            modulemapper.mapModules(window);

            platform.initialize && platform.initialize();

            // Fire event to notify that all objects are created
            channel.onCordovaReady.fire();

            // Fire onDeviceReady event once page has fully loaded, all
            // constructors have run and cordova info has been received from native
            // side.
            channel.join(function () {
                require('cordova').fireDocumentEvent('deviceready');
            }, channel.deviceReadyChannelsArray);

        }, platformInitChannelsArray);

    });

// file: src/common/modulemapper.js
    define("cordova/modulemapper", function (require, exports, module) {

        var builder = require('cordova/builder'),
            moduleMap = define.moduleMap,
            symbolList,
            deprecationMap;

        exports.reset = function () {
            symbolList = [];
            deprecationMap = {};
        };

        function addEntry(strategy, moduleName, symbolPath, opt_deprecationMessage) {
            if (!(moduleName in moduleMap)) {
                throw new Error('Module ' + moduleName + ' does not exist.');
            }
            symbolList.push(strategy, moduleName, symbolPath);
            if (opt_deprecationMessage) {
                deprecationMap[symbolPath] = opt_deprecationMessage;
            }
        }

// Note: Android 2.3 does have Function.bind().
        exports.clobbers = function (moduleName, symbolPath, opt_deprecationMessage) {
            addEntry('c', moduleName, symbolPath, opt_deprecationMessage);
        };

        exports.merges = function (moduleName, symbolPath, opt_deprecationMessage) {
            addEntry('m', moduleName, symbolPath, opt_deprecationMessage);
        };

        exports.defaults = function (moduleName, symbolPath, opt_deprecationMessage) {
            addEntry('d', moduleName, symbolPath, opt_deprecationMessage);
        };

        exports.runs = function (moduleName) {
            addEntry('r', moduleName, null);
        };

        function prepareNamespace(symbolPath, context) {
            if (!symbolPath) {
                return context;
            }
            var parts = symbolPath.split('.');
            var cur = context;
            for (var i = 0, part; part = parts[i]; ++i) {
                cur = cur[part] = cur[part] || {};
            }
            return cur;
        }

        exports.mapModules = function (context) {
            var origSymbols = {};
            context.CDV_origSymbols = origSymbols;
            for (var i = 0, len = symbolList.length; i < len; i += 3) {
                var strategy = symbolList[i];
                var moduleName = symbolList[i + 1];
                var module = require(moduleName);
                // <runs/>
                if (strategy == 'r') {
                    continue;
                }
                var symbolPath = symbolList[i + 2];
                var lastDot = symbolPath.lastIndexOf('.');
                var namespace = symbolPath.substr(0, lastDot);
                var lastName = symbolPath.substr(lastDot + 1);

                var deprecationMsg = symbolPath in deprecationMap ? 'Access made to deprecated symbol: ' + symbolPath + '. ' + deprecationMsg : null;
                var parentObj = prepareNamespace(namespace, context);
                var target = parentObj[lastName];

                if (strategy == 'm' && target) {
                    builder.recursiveMerge(target, module);
                } else if ((strategy == 'd' && !target) || (strategy != 'd')) {
                    if (!(symbolPath in origSymbols)) {
                        origSymbols[symbolPath] = target;
                    }
                    builder.assignOrWrapInDeprecateGetter(parentObj, lastName, module, deprecationMsg);
                }
            }
        };

        exports.getOriginalSymbol = function (context, symbolPath) {
            var origSymbols = context.CDV_origSymbols;
            if (origSymbols && (symbolPath in origSymbols)) {
                return origSymbols[symbolPath];
            }
            var parts = symbolPath.split('.');
            var obj = context;
            for (var i = 0; i < parts.length; ++i) {
                obj = obj && obj[parts[i]];
            }
            return obj;
        };

        exports.reset();


    });

// file: src/common/modulemapper_b.js
    define("cordova/modulemapper_b", function (require, exports, module) {

        var builder = require('cordova/builder'),
            symbolList = [],
            deprecationMap;

        exports.reset = function () {
            symbolList = [];
            deprecationMap = {};
        };

        function addEntry(strategy, moduleName, symbolPath, opt_deprecationMessage) {
            symbolList.push(strategy, moduleName, symbolPath);
            if (opt_deprecationMessage) {
                deprecationMap[symbolPath] = opt_deprecationMessage;
            }
        }

// Note: Android 2.3 does have Function.bind().
        exports.clobbers = function (moduleName, symbolPath, opt_deprecationMessage) {
            addEntry('c', moduleName, symbolPath, opt_deprecationMessage);
        };

        exports.merges = function (moduleName, symbolPath, opt_deprecationMessage) {
            addEntry('m', moduleName, symbolPath, opt_deprecationMessage);
        };

        exports.defaults = function (moduleName, symbolPath, opt_deprecationMessage) {
            addEntry('d', moduleName, symbolPath, opt_deprecationMessage);
        };

        exports.runs = function (moduleName) {
            addEntry('r', moduleName, null);
        };

        function prepareNamespace(symbolPath, context) {
            if (!symbolPath) {
                return context;
            }
            var parts = symbolPath.split('.');
            var cur = context;
            for (var i = 0, part; part = parts[i]; ++i) {
                cur = cur[part] = cur[part] || {};
            }
            return cur;
        }

        exports.mapModules = function (context) {
            var origSymbols = {};
            context.CDV_origSymbols = origSymbols;
            for (var i = 0, len = symbolList.length; i < len; i += 3) {
                var strategy = symbolList[i];
                var moduleName = symbolList[i + 1];
                var module = require(moduleName);
                // <runs/>
                if (strategy == 'r') {
                    continue;
                }
                var symbolPath = symbolList[i + 2];
                var lastDot = symbolPath.lastIndexOf('.');
                var namespace = symbolPath.substr(0, lastDot);
                var lastName = symbolPath.substr(lastDot + 1);

                var deprecationMsg = symbolPath in deprecationMap ? 'Access made to deprecated symbol: ' + symbolPath + '. ' + deprecationMsg : null;
                var parentObj = prepareNamespace(namespace, context);
                var target = parentObj[lastName];

                if (strategy == 'm' && target) {
                    builder.recursiveMerge(target, module);
                } else if ((strategy == 'd' && !target) || (strategy != 'd')) {
                    if (!(symbolPath in origSymbols)) {
                        origSymbols[symbolPath] = target;
                    }
                    builder.assignOrWrapInDeprecateGetter(parentObj, lastName, module, deprecationMsg);
                }
            }
        };

        exports.getOriginalSymbol = function (context, symbolPath) {
            var origSymbols = context.CDV_origSymbols;
            if (origSymbols && (symbolPath in origSymbols)) {
                return origSymbols[symbolPath];
            }
            var parts = symbolPath.split('.');
            var obj = context;
            for (var i = 0; i < parts.length; ++i) {
                obj = obj && obj[parts[i]];
            }
            return obj;
        };

        exports.reset();


    });

// file: /Users/shazron/Documents/git/apache/cordova-ios/cordova-js-src/platform.js
    define("cordova/platform", function (require, exports, module) {

        module.exports = {
            id: 'ios',
            bootstrap: function () {
                require('cordova/channel').onNativeReady.fire();
            }
        };


    });

// file: src/common/pluginloader.js
    define("cordova/pluginloader", function (require, exports, module) {

        var modulemapper = require('cordova/modulemapper');
        var urlutil = require('cordova/urlutil');

// Helper function to inject a <script> tag.
// Exported for testing.
        exports.injectScript = function (url, onload, onerror) {
            onload();

            /*
             var script = document.createElement("script");
             // onload fires even when script fails loads with an error.
             script.onload = onload;
             // onerror fires for malformed URLs.
             script.onerror = onerror;
             script.src = url;
             document.head.appendChild(script);
             */
        };

        function injectIfNecessary(id, url, onload, onerror) {
            onerror = onerror || onload;
            if (id in define.moduleMap) {
                onload();
            } else {
                exports.injectScript(url, function () {
                    if (id in define.moduleMap) {
                        onload();
                    } else {
                        onerror();
                    }
                }, onerror);
            }
        }

        function onScriptLoadingComplete(moduleList, finishPluginLoading) {
            // Loop through all the plugins and then through their clobbers and merges.
            for (var i = 0, module; module = moduleList[i]; i++) {
                if (module.clobbers && module.clobbers.length) {
                    for (var j = 0; j < module.clobbers.length; j++) {
                        modulemapper.clobbers(module.id, module.clobbers[j]);
                    }
                }

                if (module.merges && module.merges.length) {
                    for (var k = 0; k < module.merges.length; k++) {
                        modulemapper.merges(module.id, module.merges[k]);
                    }
                }

                // Finally, if runs is truthy we want to simply require() the module.
                if (module.runs) {
                    modulemapper.runs(module.id);
                }
            }

            finishPluginLoading();
        }

// Handler for the cordova_plugins.js content.
// See plugman's plugin_loader.js for the details of this object.
// This function is only called if the really is a plugins array that isn't empty.
// Otherwise the onerror response handler will just call finishPluginLoading().
        function handlePluginsObject(path, moduleList, finishPluginLoading) {
            // Now inject the scripts.
            var scriptCounter = moduleList.length;

            if (!scriptCounter) {
                finishPluginLoading();
                return;
            }
            function scriptLoadedCallback() {
                if (!--scriptCounter) {
                    onScriptLoadingComplete(moduleList, finishPluginLoading);
                }
            }

            for (var i = 0; i < moduleList.length; i++) {
                injectIfNecessary(moduleList[i].id, path + moduleList[i].file, scriptLoadedCallback);
            }
        }

        function findCordovaPath() {
            var path = null;
            var scripts = document.getElementsByTagName('script');
            var term = '/cordova.js';
            for (var n = scripts.length - 1; n > -1; n--) {
                var src = scripts[n].src.replace(/\?.*$/, ''); // Strip any query param (CB-6007).
                if (src.indexOf(term) == (src.length - term.length)) {
                    path = src.substring(0, src.length - term.length) + '/';
                    break;
                }
            }
            return path;
        }

// Tries to load all plugins' js-modules.
// This is an async process, but onDeviceReady is blocked on onPluginsReady.
// onPluginsReady is fired when there are no plugins to load, or they are all done.
        exports.load = function (callback) {
            var pathPrefix = findCordovaPath();
            if (pathPrefix === null) {
                console.log('Could not find cordova.js script tag. Plugin loading may fail.');
                pathPrefix = '';
            }
            injectIfNecessary('cordova/plugin_list', pathPrefix + 'cordova_plugins.js', function () {
                var moduleList = require("cordova/plugin_list");
                handlePluginsObject(pathPrefix, moduleList, callback);
            }, callback);
        };


    });

// file: src/common/pluginloader_b.js
    define("cordova/pluginloader_b", function (require, exports, module) {

        var modulemapper = require('cordova/modulemapper');

// Handler for the cordova_plugins.js content.
// See plugman's plugin_loader.js for the details of this object.
        function handlePluginsObject(moduleList) {
            // if moduleList is not defined or empty, we've nothing to do
            if (!moduleList || !moduleList.length) {
                return;
            }

            // Loop through all the modules and then through their clobbers and merges.
            for (var i = 0, module; module = moduleList[i]; i++) {
                if (module.clobbers && module.clobbers.length) {
                    for (var j = 0; j < module.clobbers.length; j++) {
                        modulemapper.clobbers(module.id, module.clobbers[j]);
                    }
                }

                if (module.merges && module.merges.length) {
                    for (var k = 0; k < module.merges.length; k++) {
                        modulemapper.merges(module.id, module.merges[k]);
                    }
                }

                // Finally, if runs is truthy we want to simply require() the module.
                if (module.runs) {
                    modulemapper.runs(module.id);
                }
            }
        }

// Loads all plugins' js-modules. Plugin loading is syncronous in browserified bundle
// but the method accepts callback to be compatible with non-browserify flow.
// onDeviceReady is blocked on onPluginsReady. onPluginsReady is fired when there are
// no plugins to load, or they are all done.
        exports.load = function (callback) {
            var moduleList = require("cordova/plugin_list");
            handlePluginsObject(moduleList);

            callback();
        };


    });

// file: src/common/urlutil.js
    define("cordova/urlutil", function (require, exports, module) {


        /**
         * For already absolute URLs, returns what is passed in.
         * For relative URLs, converts them to absolute ones.
         */
        exports.makeAbsolute = function makeAbsolute(url) {
            var anchorEl = document.createElement('a');
            anchorEl.href = url;
            return anchorEl.href;
        };


    });

// file: src/common/utils.js
    define("cordova/utils", function (require, exports, module) {

        var utils = exports;

        /**
         * Defines a property getter / setter for obj[key].
         */
        utils.defineGetterSetter = function (obj, key, getFunc, opt_setFunc) {
            if (Object.defineProperty) {
                var desc = {
                    get: getFunc,
                    configurable: true
                };
                if (opt_setFunc) {
                    desc.set = opt_setFunc;
                }
                Object.defineProperty(obj, key, desc);
            } else {
                obj.__defineGetter__(key, getFunc);
                if (opt_setFunc) {
                    obj.__defineSetter__(key, opt_setFunc);
                }
            }
        };

        /**
         * Defines a property getter for obj[key].
         */
        utils.defineGetter = utils.defineGetterSetter;

        utils.arrayIndexOf = function (a, item) {
            if (a.indexOf) {
                return a.indexOf(item);
            }
            var len = a.length;
            for (var i = 0; i < len; ++i) {
                if (a[i] == item) {
                    return i;
                }
            }
            return -1;
        };

        /**
         * Returns whether the item was found in the array.
         */
        utils.arrayRemove = function (a, item) {
            var index = utils.arrayIndexOf(a, item);
            if (index != -1) {
                a.splice(index, 1);
            }
            return index != -1;
        };

        utils.typeName = function (val) {
            return Object.prototype.toString.call(val).slice(8, -1);
        };

        /**
         * Returns an indication of whether the argument is an array or not
         */
        utils.isArray = Array.isArray ||
            function (a) {
                return utils.typeName(a) == 'Array';
            };

        /**
         * Returns an indication of whether the argument is a Date or not
         */
        utils.isDate = function (d) {
            return (d instanceof Date);
        };

        /**
         * Does a deep clone of the object.
         */
        utils.clone = function (obj) {
            if (!obj || typeof obj == 'function' || utils.isDate(obj) || typeof obj != 'object') {
                return obj;
            }

            var retVal, i;

            if (utils.isArray(obj)) {
                retVal = [];
                for (i = 0; i < obj.length; ++i) {
                    retVal.push(utils.clone(obj[i]));
                }
                return retVal;
            }

            retVal = {};
            for (i in obj) {
                // https://issues.apache.org/jira/browse/CB-11522 'unknown' type may be returned in
                // custom protocol activation case on Windows Phone 8.1 causing "No such interface supported" exception
                // on cloning.
                if ((!(i in retVal) || retVal[i] != obj[i]) && typeof obj[i] != 'undefined' && typeof obj[i] != 'unknown') {
                    retVal[i] = utils.clone(obj[i]);
                }
            }
            return retVal;
        };

        /**
         * Returns a wrapped version of the function
         */
        utils.close = function (context, func, params) {
            return function () {
                var args = params || arguments;
                return func.apply(context, args);
            };
        };

//------------------------------------------------------------------------------
        function UUIDcreatePart(length) {
            var uuidpart = "";
            for (var i = 0; i < length; i++) {
                var uuidchar = parseInt((Math.random() * 256), 10).toString(16);
                if (uuidchar.length == 1) {
                    uuidchar = "0" + uuidchar;
                }
                uuidpart += uuidchar;
            }
            return uuidpart;
        }

        /**
         * Create a UUID
         */
        utils.createUUID = function () {
            return UUIDcreatePart(4) + '-' +
                UUIDcreatePart(2) + '-' +
                UUIDcreatePart(2) + '-' +
                UUIDcreatePart(2) + '-' +
                UUIDcreatePart(6);
        };


        /**
         * Extends a child object from a parent object using classical inheritance
         * pattern.
         */
        utils.extend = (function () {
            // proxy used to establish prototype chain
            var F = function () {
            };
            // extend Child from Parent
            return function (Child, Parent) {

                F.prototype = Parent.prototype;
                Child.prototype = new F();
                Child.__super__ = Parent.prototype;
                Child.prototype.constructor = Child;
            };
        }());

        /**
         * Alerts a message in any available way: alert or console.log.
         */
        utils.alert = function (msg) {
            if (window.alert) {
                window.alert(msg);
            } else if (console && console.log) {
                console.log(msg);
            }
        };


    });

    window.cordova = require('cordova');
// file: src/scripts/bootstrap.js

    cordova.define('cordova/plugin_list', function (require, exports, module) {
        module.exports = [
            {
                "id": "cordova-plugin-device.device",
                "file": "plugins/cordova-plugin-device/www/device.js",
                "pluginId": "cordova-plugin-device",
                "clobbers": [
                    "device"
                ]
            },
            {
                "id": "cordova-plugin-camera.Camera",
                "file": "plugins/cordova-plugin-camera/www/CameraConstants.js",
                "pluginId": "cordova-plugin-camera",
                "clobbers": [
                    "Camera"
                ]
            },
            {
                "id": "cordova-plugin-camera.CameraPopoverOptions",
                "file": "plugins/cordova-plugin-camera/www/CameraPopoverOptions.js",
                "pluginId": "cordova-plugin-camera",
                "clobbers": [
                    "CameraPopoverOptions"
                ]
            },
            {
                "id": "cordova-plugin-camera.camera",
                "file": "plugins/cordova-plugin-camera/www/Camera.js",
                "pluginId": "cordova-plugin-camera",
                "clobbers": [
                    "navigator.camera"
                ]
            },
            {
                "id": "cordova-plugin-camera.CameraPopoverHandle",
                "file": "plugins/cordova-plugin-camera/www/ios/CameraPopoverHandle.js",
                "pluginId": "cordova-plugin-camera",
                "clobbers": [
                    "CameraPopoverHandle"
                ]
            },
            {
                "id": "cordova-plugin-file.DirectoryEntry",
                "file": "plugins/cordova-plugin-file/www/DirectoryEntry.js",
                "pluginId": "cordova-plugin-file",
                "clobbers": [
                    "window.DirectoryEntry"
                ]
            },
            {
                "id": "cordova-plugin-file.DirectoryReader",
                "file": "plugins/cordova-plugin-file/www/DirectoryReader.js",
                "pluginId": "cordova-plugin-file",
                "clobbers": [
                    "window.DirectoryReader"
                ]
            },
            {
                "id": "cordova-plugin-file.Entry",
                "file": "plugins/cordova-plugin-file/www/Entry.js",
                "pluginId": "cordova-plugin-file",
                "clobbers": [
                    "window.Entry"
                ]
            },
            {
                "id": "cordova-plugin-file.File",
                "file": "plugins/cordova-plugin-file/www/File.js",
                "pluginId": "cordova-plugin-file",
                "clobbers": [
                    "window.File"
                ]
            },
            {
                "id": "cordova-plugin-file.FileEntry",
                "file": "plugins/cordova-plugin-file/www/FileEntry.js",
                "pluginId": "cordova-plugin-file",
                "clobbers": [
                    "window.FileEntry"
                ]
            },
            {
                "id": "cordova-plugin-file.FileError",
                "file": "plugins/cordova-plugin-file/www/FileError.js",
                "pluginId": "cordova-plugin-file",
                "clobbers": [
                    "window.FileError"
                ]
            },
            {
                "id": "cordova-plugin-file.FileReader",
                "file": "plugins/cordova-plugin-file/www/FileReader.js",
                "pluginId": "cordova-plugin-file",
                "clobbers": [
                    "window.FileReader"
                ]
            },
            {
                "id": "cordova-plugin-file.FileSystem",
                "file": "plugins/cordova-plugin-file/www/FileSystem.js",
                "pluginId": "cordova-plugin-file",
                "clobbers": [
                    "window.FileSystem"
                ]
            },
            {
                "id": "cordova-plugin-file.FileUploadOptions",
                "file": "plugins/cordova-plugin-file/www/FileUploadOptions.js",
                "pluginId": "cordova-plugin-file",
                "clobbers": [
                    "window.FileUploadOptions"
                ]
            },
            {
                "id": "cordova-plugin-file.FileUploadResult",
                "file": "plugins/cordova-plugin-file/www/FileUploadResult.js",
                "pluginId": "cordova-plugin-file",
                "clobbers": [
                    "window.FileUploadResult"
                ]
            },
            {
                "id": "cordova-plugin-file.FileWriter",
                "file": "plugins/cordova-plugin-file/www/FileWriter.js",
                "pluginId": "cordova-plugin-file",
                "clobbers": [
                    "window.FileWriter"
                ]
            },
            {
                "id": "cordova-plugin-file.Flags",
                "file": "plugins/cordova-plugin-file/www/Flags.js",
                "pluginId": "cordova-plugin-file",
                "clobbers": [
                    "window.Flags"
                ]
            },
            {
                "id": "cordova-plugin-file.LocalFileSystem",
                "file": "plugins/cordova-plugin-file/www/LocalFileSystem.js",
                "pluginId": "cordova-plugin-file",
                "clobbers": [
                    "window.LocalFileSystem"
                ],
                "merges": [
                    "window"
                ]
            },
            {
                "id": "cordova-plugin-file.Metadata",
                "file": "plugins/cordova-plugin-file/www/Metadata.js",
                "pluginId": "cordova-plugin-file",
                "clobbers": [
                    "window.Metadata"
                ]
            },
            {
                "id": "cordova-plugin-file.ProgressEvent",
                "file": "plugins/cordova-plugin-file/www/ProgressEvent.js",
                "pluginId": "cordova-plugin-file",
                "clobbers": [
                    "window.ProgressEvent"
                ]
            },
            {
                "id": "cordova-plugin-file.fileSystems",
                "file": "plugins/cordova-plugin-file/www/fileSystems.js",
                "pluginId": "cordova-plugin-file"
            },
            {
                "id": "cordova-plugin-file.requestFileSystem",
                "file": "plugins/cordova-plugin-file/www/requestFileSystem.js",
                "pluginId": "cordova-plugin-file",
                "clobbers": [
                    "window.requestFileSystem"
                ]
            },
            {
                "id": "cordova-plugin-file.resolveLocalFileSystemURI",
                "file": "plugins/cordova-plugin-file/www/resolveLocalFileSystemURI.js",
                "pluginId": "cordova-plugin-file",
                "merges": [
                    "window"
                ]
            },
            {
                "id": "cordova-plugin-file.isChrome",
                "file": "plugins/cordova-plugin-file/www/browser/isChrome.js",
                "pluginId": "cordova-plugin-file",
                "runs": true
            },
            {
                "id": "cordova-plugin-file.iosFileSystem",
                "file": "plugins/cordova-plugin-file/www/ios/FileSystem.js",
                "pluginId": "cordova-plugin-file",
                "merges": [
                    "FileSystem"
                ]
            },
            {
                "id": "cordova-plugin-file.fileSystems-roots",
                "file": "plugins/cordova-plugin-file/www/fileSystems-roots.js",
                "pluginId": "cordova-plugin-file",
                "runs": true
            },
            {
                "id": "cordova-plugin-file.fileSystemPaths",
                "file": "plugins/cordova-plugin-file/www/fileSystemPaths.js",
                "pluginId": "cordova-plugin-file",
                "merges": [
                    "cordova"
                ],
                "runs": true
            },
            {
                "id": "cordova-plugin-file-transfer.FileTransferError",
                "file": "plugins/cordova-plugin-file-transfer/www/FileTransferError.js",
                "pluginId": "cordova-plugin-file-transfer",
                "clobbers": [
                    "window.FileTransferError"
                ]
            },
            {
                "id": "cordova-plugin-file-transfer.FileTransfer",
                "file": "plugins/cordova-plugin-file-transfer/www/FileTransfer.js",
                "pluginId": "cordova-plugin-file-transfer",
                "clobbers": [
                    "window.FileTransfer"
                ]
            },
            {
                "id": "cordova-plugin-geolocation.Coordinates",
                "file": "plugins/cordova-plugin-geolocation/www/Coordinates.js",
                "pluginId": "cordova-plugin-geolocation",
                "clobbers": [
                    "Coordinates"
                ]
            },
            {
                "id": "cordova-plugin-geolocation.PositionError",
                "file": "plugins/cordova-plugin-geolocation/www/PositionError.js",
                "pluginId": "cordova-plugin-geolocation",
                "clobbers": [
                    "PositionError"
                ]
            },
            {
                "id": "cordova-plugin-geolocation.Position",
                "file": "plugins/cordova-plugin-geolocation/www/Position.js",
                "pluginId": "cordova-plugin-geolocation",
                "clobbers": [
                    "Position"
                ]
            },
            {
                "id": "cordova-plugin-geolocation.geolocation",
                "file": "plugins/cordova-plugin-geolocation/www/geolocation.js",
                "pluginId": "cordova-plugin-geolocation",
                "clobbers": [
                    "navigator.geolocation"
                ]
            },
            {
                "id": "phonegap-plugin-barcodescanner.BarcodeScanner",
                "file": "plugins/phonegap-plugin-barcodescanner/www/barcodescanner.js",
                "pluginId": "phonegap-plugin-barcodescanner",
                "clobbers": [
                    "cordova.plugins.barcodeScanner"
                ]
            },
            {
                "id": "cordova-sqlcipher-adapter.SQLitePlugin",
                "file": "plugins/cordova-sqlcipher-adapter/www/SQLitePlugin.js",
                "pluginId": "cordova-sqlcipher-adapter",
                "clobbers": [
                    "SQLitePlugin"
                ]
            },
            {
                "id": "cordova-plugin-contacts.contacts",
                "file": "plugins/cordova-plugin-contacts/www/contacts.js",
                "pluginId": "cordova-plugin-contacts",
                "clobbers": [
                    "navigator.contacts"
                ]
            },
            {
                "id": "cordova-plugin-contacts.Contact",
                "file": "plugins/cordova-plugin-contacts/www/Contact.js",
                "pluginId": "cordova-plugin-contacts",
                "clobbers": [
                    "Contact"
                ]
            },
            {
                "id": "cordova-plugin-contacts.convertUtils",
                "file": "plugins/cordova-plugin-contacts/www/convertUtils.js",
                "pluginId": "cordova-plugin-contacts"
            },
            {
                "id": "cordova-plugin-contacts.ContactAddress",
                "file": "plugins/cordova-plugin-contacts/www/ContactAddress.js",
                "pluginId": "cordova-plugin-contacts",
                "clobbers": [
                    "ContactAddress"
                ]
            },
            {
                "id": "cordova-plugin-contacts.ContactError",
                "file": "plugins/cordova-plugin-contacts/www/ContactError.js",
                "pluginId": "cordova-plugin-contacts",
                "clobbers": [
                    "ContactError"
                ]
            },
            {
                "id": "cordova-plugin-contacts.ContactField",
                "file": "plugins/cordova-plugin-contacts/www/ContactField.js",
                "pluginId": "cordova-plugin-contacts",
                "clobbers": [
                    "ContactField"
                ]
            },
            {
                "id": "cordova-plugin-contacts.ContactFindOptions",
                "file": "plugins/cordova-plugin-contacts/www/ContactFindOptions.js",
                "pluginId": "cordova-plugin-contacts",
                "clobbers": [
                    "ContactFindOptions"
                ]
            },
            {
                "id": "cordova-plugin-contacts.ContactName",
                "file": "plugins/cordova-plugin-contacts/www/ContactName.js",
                "pluginId": "cordova-plugin-contacts",
                "clobbers": [
                    "ContactName"
                ]
            },
            {
                "id": "cordova-plugin-contacts.ContactOrganization",
                "file": "plugins/cordova-plugin-contacts/www/ContactOrganization.js",
                "pluginId": "cordova-plugin-contacts",
                "clobbers": [
                    "ContactOrganization"
                ]
            },
            {
                "id": "cordova-plugin-contacts.ContactFieldType",
                "file": "plugins/cordova-plugin-contacts/www/ContactFieldType.js",
                "pluginId": "cordova-plugin-contacts",
                "merges": [
                    ""
                ]
            },
            {
                "id": "cordova-plugin-contacts.contacts-ios",
                "file": "plugins/cordova-plugin-contacts/www/ios/contacts.js",
                "pluginId": "cordova-plugin-contacts",
                "merges": [
                    "navigator.contacts"
                ]
            },
            {
                "id": "cordova-plugin-contacts.Contact-iOS",
                "file": "plugins/cordova-plugin-contacts/www/ios/Contact.js",
                "pluginId": "cordova-plugin-contacts",
                "merges": [
                    "Contact"
                ]
            },
            {
                "id": "cordova-plugin-network-information.network",
                "file": "plugins/cordova-plugin-network-information/www/network.js",
                "pluginId": "cordova-plugin-network-information",
                "clobbers": [
                    "navigator.connection",
                    "navigator.network.connection"
                ]
            },
            {
                "id": "cordova-plugin-network-information.Connection",
                "file": "plugins/cordova-plugin-network-information/www/Connection.js",
                "pluginId": "cordova-plugin-network-information",
                "clobbers": [
                    "Connection"
                ]
            },
            {
                "id": "cordova-plugin-device-motion.Acceleration",
                "file": "plugins/cordova-plugin-device-motion/www/Acceleration.js",
                "pluginId": "cordova-plugin-device-motion",
                "clobbers": [
                    "Acceleration"
                ]
            },
            {
                "id": "cordova-plugin-device-motion.accelerometer",
                "file": "plugins/cordova-plugin-device-motion/www/accelerometer.js",
                "pluginId": "cordova-plugin-device-motion",
                "clobbers": [
                    "navigator.accelerometer"
                ]
            },
            {
                "id": "cordova-plugin-device-orientation.CompassError",
                "file": "plugins/cordova-plugin-device-orientation/www/CompassError.js",
                "pluginId": "cordova-plugin-device-orientation",
                "clobbers": [
                    "CompassError"
                ]
            },
            {
                "id": "cordova-plugin-device-orientation.CompassHeading",
                "file": "plugins/cordova-plugin-device-orientation/www/CompassHeading.js",
                "pluginId": "cordova-plugin-device-orientation",
                "clobbers": [
                    "CompassHeading"
                ]
            },
            {
                "id": "cordova-plugin-device-orientation.compass",
                "file": "plugins/cordova-plugin-device-orientation/www/compass.js",
                "pluginId": "cordova-plugin-device-orientation",
                "clobbers": [
                    "navigator.compass"
                ]
            },
            {
                "id": "cordova-plugin-inappbrowser.inappbrowser",
                "file": "plugins/cordova-plugin-inappbrowser/www/inappbrowser.js",
                "pluginId": "cordova-plugin-inappbrowser",
                "clobbers": [
                    "cordova.InAppBrowser.open",
                    "window.open"
                ]
            },
            {
                "id": "cordova-plugin-media.MediaError",
                "file": "plugins/cordova-plugin-media/www/MediaError.js",
                "pluginId": "cordova-plugin-media",
                "clobbers": [
                    "window.MediaError"
                ]
            },
            {
                "id": "cordova-plugin-media.Media",
                "file": "plugins/cordova-plugin-media/www/Media.js",
                "pluginId": "cordova-plugin-media",
                "clobbers": [
                    "window.Media"
                ]
            },
            {
                "id": "cordova-plugin-media-capture.CaptureAudioOptions",
                "file": "plugins/cordova-plugin-media-capture/www/CaptureAudioOptions.js",
                "pluginId": "cordova-plugin-media-capture",
                "clobbers": [
                    "CaptureAudioOptions"
                ]
            },
            {
                "id": "cordova-plugin-media-capture.CaptureImageOptions",
                "file": "plugins/cordova-plugin-media-capture/www/CaptureImageOptions.js",
                "pluginId": "cordova-plugin-media-capture",
                "clobbers": [
                    "CaptureImageOptions"
                ]
            },
            {
                "id": "cordova-plugin-media-capture.CaptureVideoOptions",
                "file": "plugins/cordova-plugin-media-capture/www/CaptureVideoOptions.js",
                "pluginId": "cordova-plugin-media-capture",
                "clobbers": [
                    "CaptureVideoOptions"
                ]
            },
            {
                "id": "cordova-plugin-media-capture.CaptureError",
                "file": "plugins/cordova-plugin-media-capture/www/CaptureError.js",
                "pluginId": "cordova-plugin-media-capture",
                "clobbers": [
                    "CaptureError"
                ]
            },
            {
                "id": "cordova-plugin-media-capture.MediaFileData",
                "file": "plugins/cordova-plugin-media-capture/www/MediaFileData.js",
                "pluginId": "cordova-plugin-media-capture",
                "clobbers": [
                    "MediaFileData"
                ]
            },
            {
                "id": "cordova-plugin-media-capture.MediaFile",
                "file": "plugins/cordova-plugin-media-capture/www/MediaFile.js",
                "pluginId": "cordova-plugin-media-capture",
                "clobbers": [
                    "MediaFile"
                ]
            },
            {
                "id": "cordova-plugin-media-capture.helpers",
                "file": "plugins/cordova-plugin-media-capture/www/helpers.js",
                "pluginId": "cordova-plugin-media-capture",
                "runs": true
            },
            {
                "id": "cordova-plugin-media-capture.capture",
                "file": "plugins/cordova-plugin-media-capture/www/capture.js",
                "pluginId": "cordova-plugin-media-capture",
                "clobbers": [
                    "navigator.device.capture"
                ]
            },
            {
                "id": "cordova-plugin-vibration.notification",
                "file": "plugins/cordova-plugin-vibration/www/vibration.js",
                "pluginId": "cordova-plugin-vibration",
                "merges": [
                    "navigator.notification",
                    "navigator"
                ]
            },
            {
                "id": "cordova-plugin-splashscreen.SplashScreen",
                "file": "plugins/cordova-plugin-splashscreen/www/splashscreen.js",
                "pluginId": "cordova-plugin-splashscreen",
                "clobbers": [
                    "navigator.splashscreen"
                ]
            },
            {
                "id": "cordova-plugin-statusbar.statusbar",
                "file": "plugins/cordova-plugin-statusbar/www/statusbar.js",
                "pluginId": "cordova-plugin-statusbar",
                "clobbers": [
                    "window.StatusBar"
                ]
            },
            {
                "id": "com.archibus.plugins.configuration.Configuration",
                "file": "plugins/com.archibus.plugins.configuration/www/configuration.js",
                "pluginId": "com.archibus.plugins.configuration",
                "clobbers": [
                    "Configuration"
                ]
            },
            {
                "id": "com.archibus.plugins.appversion.AppVersion",
                "file": "plugins/com.archibus.plugins.appversion/www/appversion.js",
                "pluginId": "com.archibus.plugins.appversion",
                "clobbers": [
                    "AppVersion"
                ]
            },
            {
                 "file": "plugins/com.archibus.plugins.textarea/www/textarea.js",
                 "id": "com.archibus.plugins.textarea.TextArea",
                 "clobbers": [
                      "TextArea"
                 ]
            },
            {
                "id": "com.archibus.plugins.dbcredential.keychain",
                "file": "plugins/com.archibus.plugins.dbcredential/www/keychain.js",
                "pluginId": "com.archibus.plugins.dbcredential",
                "clobbers": [
                    "Keychain"
                ]
            },
            {
                "id": "com.archibus.plugins.docconverter.DocumentConveter",
                "file": "plugins/com.archibus.plugins.docconverter/www/docconverter.js",
                "pluginId": "com.archibus.plugins.docconverter",
                "clobbers": [
                    "DocumentConverter"
                ]
            },
            {
                "id": "com.unarin.cordova.beacon.underscorejs",
                "file": "plugins/com.unarin.cordova.beacon/www/lib/underscore-min-1.6.js",
                "pluginId": "com.unarin.cordova.beacon",
                "runs": true
            },
            {
                "id": "com.unarin.cordova.beacon.Q",
                "file": "plugins/com.unarin.cordova.beacon/www/lib/q.min.js",
                "pluginId": "com.unarin.cordova.beacon",
                "runs": true
            },
            {
                "id": "com.unarin.cordova.beacon.LocationManager",
                "file": "plugins/com.unarin.cordova.beacon/www/LocationManager.js",
                "pluginId": "com.unarin.cordova.beacon",
                "merges": [
                    "cordova.plugins"
                ]
            },
            {
                "id": "com.unarin.cordova.beacon.Delegate",
                "file": "plugins/com.unarin.cordova.beacon/www/Delegate.js",
                "pluginId": "com.unarin.cordova.beacon",
                "runs": true
            },
            {
                "id": "com.unarin.cordova.beacon.Region",
                "file": "plugins/com.unarin.cordova.beacon/www/model/Region.js",
                "pluginId": "com.unarin.cordova.beacon",
                "runs": true
            },
            {
                "id": "com.unarin.cordova.beacon.Regions",
                "file": "plugins/com.unarin.cordova.beacon/www/Regions.js",
                "pluginId": "com.unarin.cordova.beacon",
                "runs": true
            },
            {
                "id": "com.unarin.cordova.beacon.CircularRegion",
                "file": "plugins/com.unarin.cordova.beacon/www/model/CircularRegion.js",
                "pluginId": "com.unarin.cordova.beacon",
                "runs": true
            },
            {
                "id": "com.unarin.cordova.beacon.BeaconRegion",
                "file": "plugins/com.unarin.cordova.beacon/www/model/BeaconRegion.js",
                "pluginId": "com.unarin.cordova.beacon",
                "runs": true
            },
            {
                "id":"com.darktalker.cordova.screenshot.screenshot",
                "file": "plugins/com.darktalker.cordova.screenshot/www/Screenshot.js",
                "merges": [
                    "navigator.screenshot"
                ]
            },
            {
                "id":"com.archibus.plugins.connection.Connection",
                "file": "plugins/com.archibus.plugins.connection/www/connection.js",
                "clobbers": [
                    "Connection"
                ]
            }
        ];
        module.exports.metadata =
// TOP OF METADATA
            {
                "cordova-plugin-whitelist": "1.3.2",
                "cordova-plugin-device": "1.1.5",
                "cordova-plugin-compat": "1.1.0",
                "cordova-plugin-camera": "2.4.0",
                "cordova-plugin-file": "4.3.2",
                "cordova-plugin-file-transfer": "1.6.2",
                "cordova-plugin-geolocation": "2.4.2",
                "phonegap-plugin-barcodescanner": "6.0.6",
                "cordova-sqlcipher-adapter": "0.1.9-pre3",
                "cordova-plugin-contacts": "2.3.0",
                "cordova-plugin-network-information": "1.3.2",
                "cordova-plugin-device-motion": "1.2.4",
                "cordova-plugin-device-orientation": "1.0.6",
                "cordova-plugin-inappbrowser": "1.7.0",
                "cordova-plugin-media": "3.0.0",
                "cordova-plugin-media-capture": "1.4.2",
                "cordova-plugin-vibration": "2.1.4",
                "cordova-plugin-splashscreen": "4.0.2",
                "cordova-plugin-statusbar": "2.2.2",
                "com.archibus.plugins.configuration": "0.0.1",
                "com.archibus.plugins.appversion": "0.0.1",
                "com.archibus.plugins.dbcredential": "0.0.1",
                "com.archibus.plugins.docconverter": "0.0.1",
                "com.darktalker.cordova.screenshot": "0.1.5"

            };
// BOTTOM OF METADATA
    });
    cordova.define("cordova-plugin-device.device", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var argscheck = require('cordova/argscheck'),
            channel = require('cordova/channel'),
            utils = require('cordova/utils'),
            exec = require('cordova/exec'),
            cordova = require('cordova');

        channel.createSticky('onCordovaInfoReady');
// Tell cordova channel to wait on the CordovaInfoReady event
        channel.waitForInitialization('onCordovaInfoReady');

        /**
         * This represents the mobile device, and provides properties for inspecting the model, version, UUID of the
         * phone, etc.
         * @constructor
         */
        function Device() {
            this.available = false;
            this.platform = null;
            this.version = null;
            this.uuid = null;
            this.cordova = null;
            this.model = null;
            this.manufacturer = null;
            this.isVirtual = null;
            this.serial = null;

            var me = this;

            channel.onCordovaReady.subscribe(function () {
                me.getInfo(function (info) {
                    //ignoring info.cordova returning from native, we should use value from cordova.version defined in cordova.js
                    //TODO: CB-5105 native implementations should not return info.cordova
                    var buildLabel = cordova.version;
                    me.available = true;
                    me.platform = info.platform;
                    me.version = info.version;
                    me.uuid = info.uuid;
                    me.cordova = buildLabel;
                    me.model = info.model;
                    me.isVirtual = info.isVirtual;
                    me.manufacturer = info.manufacturer || 'unknown';
                    me.serial = info.serial || 'unknown';
                    channel.onCordovaInfoReady.fire();
                }, function (e) {
                    me.available = false;
                    utils.alert("[ERROR] Error initializing Cordova: " + e);
                });
            });
        }

        /**
         * Get device info
         *
         * @param {Function} successCallback The function to call when the heading data is available
         * @param {Function} errorCallback The function to call when there is an error getting the heading data. (OPTIONAL)
         */
        Device.prototype.getInfo = function (successCallback, errorCallback) {
            argscheck.checkArgs('fF', 'Device.getInfo', arguments);
            exec(successCallback, errorCallback, "Device", "getDeviceInfo", []);
        };

        module.exports = new Device();

    });

    cordova.define("cordova-plugin-camera.Camera", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * @module Camera
         */
        module.exports = {
            /**
             * @description
             * Defines the output format of `Camera.getPicture` call.
             * _Note:_ On iOS passing `DestinationType.NATIVE_URI` along with
             * `PictureSourceType.PHOTOLIBRARY` or `PictureSourceType.SAVEDPHOTOALBUM` will
             * disable any image modifications (resize, quality change, cropping, etc.) due
             * to implementation specific.
             *
             * @enum {number}
             */
            DestinationType: {
                /** Return base64 encoded string. DATA_URL can be very memory intensive and cause app crashes or out of memory errors. Use FILE_URI or NATIVE_URI if possible */
                DATA_URL: 0,
                /** Return file uri (content://media/external/images/media/2 for Android) */
                FILE_URI: 1,
                /** Return native uri (eg. asset-library://... for iOS) */
                NATIVE_URI: 2
            },
            /**
             * @enum {number}
             */
            EncodingType: {
                /** Return JPEG encoded image */
                JPEG: 0,
                /** Return PNG encoded image */
                PNG: 1
            },
            /**
             * @enum {number}
             */
            MediaType: {
                /** Allow selection of still pictures only. DEFAULT. Will return format specified via DestinationType */
                PICTURE: 0,
                /** Allow selection of video only, ONLY RETURNS URL */
                VIDEO: 1,
                /** Allow selection from all media types */
                ALLMEDIA: 2
            },
            /**
             * @description
             * Defines the output format of `Camera.getPicture` call.
             * _Note:_ On iOS passing `PictureSourceType.PHOTOLIBRARY` or `PictureSourceType.SAVEDPHOTOALBUM`
             * along with `DestinationType.NATIVE_URI` will disable any image modifications (resize, quality
             * change, cropping, etc.) due to implementation specific.
             *
             * @enum {number}
             */
            PictureSourceType: {
                /** Choose image from the device's photo library (same as SAVEDPHOTOALBUM for Android) */
                PHOTOLIBRARY: 0,
                /** Take picture from camera */
                CAMERA: 1,
                /** Choose image only from the device's Camera Roll album (same as PHOTOLIBRARY for Android) */
                SAVEDPHOTOALBUM: 2
            },
            /**
             * Matches iOS UIPopoverArrowDirection constants to specify arrow location on popover.
             * @enum {number}
             */
            PopoverArrowDirection: {
                ARROW_UP: 1,
                ARROW_DOWN: 2,
                ARROW_LEFT: 4,
                ARROW_RIGHT: 8,
                ARROW_ANY: 15
            },
            /**
             * @enum {number}
             */
            Direction: {
                /** Use the back-facing camera */
                BACK: 0,
                /** Use the front-facing camera */
                FRONT: 1
            }
        };

    });

    cordova.define("cordova-plugin-camera.CameraPopoverOptions", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var Camera = require('./Camera');

        /**
         * @namespace navigator
         */

        /**
         * iOS-only parameters that specify the anchor element location and arrow
         * direction of the popover when selecting images from an iPad's library
         * or album.
         * Note that the size of the popover may change to adjust to the
         * direction of the arrow and orientation of the screen.  Make sure to
         * account for orientation changes when specifying the anchor element
         * location.
         * @module CameraPopoverOptions
         * @param {Number} [x=0] - x pixel coordinate of screen element onto which to anchor the popover.
         * @param {Number} [y=32] - y pixel coordinate of screen element onto which to anchor the popover.
         * @param {Number} [width=320] - width, in pixels, of the screen element onto which to anchor the popover.
         * @param {Number} [height=480] - height, in pixels, of the screen element onto which to anchor the popover.
         * @param {module:Camera.PopoverArrowDirection} [arrowDir=ARROW_ANY] - Direction the arrow on the popover should point.
         */
        var CameraPopoverOptions = function (x, y, width, height, arrowDir) {
            // information of rectangle that popover should be anchored to
            this.x = x || 0;
            this.y = y || 32;
            this.width = width || 320;
            this.height = height || 480;
            this.arrowDir = arrowDir || Camera.PopoverArrowDirection.ARROW_ANY;
        };

        module.exports = CameraPopoverOptions;

    });

    cordova.define("cordova-plugin-camera.camera", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var argscheck = require('cordova/argscheck'),
            exec = require('cordova/exec'),
            Camera = require('./Camera');
        // XXX: commented out
        //CameraPopoverHandle = require('./CameraPopoverHandle');

        /**
         * @namespace navigator
         */

        /**
         * @exports camera
         */
        var cameraExport = {};

// Tack on the Camera Constants to the base camera plugin.
        for (var key in Camera) {
            cameraExport[key] = Camera[key];
        }

        /**
         * Callback function that provides an error message.
         * @callback module:camera.onError
         * @param {string} message - The message is provided by the device's native code.
         */

        /**
         * Callback function that provides the image data.
         * @callback module:camera.onSuccess
         * @param {string} imageData - Base64 encoding of the image data, _or_ the image file URI, depending on [`cameraOptions`]{@link module:camera.CameraOptions} in effect.
         * @example
         * // Show image
         * //
         * function cameraCallback(imageData) {
 *    var image = document.getElementById('myImage');
 *    image.src = "data:image/jpeg;base64," + imageData;
 * }
         */

        /**
         * Optional parameters to customize the camera settings.
         * * [Quirks](#CameraOptions-quirks)
         * @typedef module:camera.CameraOptions
         * @type {Object}
         * @property {number} [quality=50] - Quality of the saved image, expressed as a range of 0-100, where 100 is typically full resolution with no loss from file compression. (Note that information about the camera's resolution is unavailable.)
         * @property {module:Camera.DestinationType} [destinationType=FILE_URI] - Choose the format of the return value.
         * @property {module:Camera.PictureSourceType} [sourceType=CAMERA] - Set the source of the picture.
         * @property {Boolean} [allowEdit=true] - Allow simple editing of image before selection.
         * @property {module:Camera.EncodingType} [encodingType=JPEG] - Choose the  returned image file's encoding.
         * @property {number} [targetWidth] - Width in pixels to scale image. Must be used with `targetHeight`. Aspect ratio remains constant.
         * @property {number} [targetHeight] - Height in pixels to scale image. Must be used with `targetWidth`. Aspect ratio remains constant.
         * @property {module:Camera.MediaType} [mediaType=PICTURE] - Set the type of media to select from.  Only works when `PictureSourceType` is `PHOTOLIBRARY` or `SAVEDPHOTOALBUM`.
         * @property {Boolean} [correctOrientation] - Rotate the image to correct for the orientation of the device during capture.
         * @property {Boolean} [saveToPhotoAlbum] - Save the image to the photo album on the device after capture.
         * @property {module:CameraPopoverOptions} [popoverOptions] - iOS-only options that specify popover location in iPad.
         * @property {module:Camera.Direction} [cameraDirection=BACK] - Choose the camera to use (front- or back-facing).
         */

        /**
         * @description Takes a photo using the camera, or retrieves a photo from the device's
         * image gallery.  The image is passed to the success callback as a
         * Base64-encoded `String`, or as the URI for the image file.
         *
         * The `camera.getPicture` function opens the device's default camera
         * application that allows users to snap pictures by default - this behavior occurs,
         * when `Camera.sourceType` equals [`Camera.PictureSourceType.CAMERA`]{@link module:Camera.PictureSourceType}.
         * Once the user snaps the photo, the camera application closes and the application is restored.
         *
         * If `Camera.sourceType` is `Camera.PictureSourceType.PHOTOLIBRARY` or
         * `Camera.PictureSourceType.SAVEDPHOTOALBUM`, then a dialog displays
         * that allows users to select an existing image.
         *
         * The return value is sent to the [`cameraSuccess`]{@link module:camera.onSuccess} callback function, in
         * one of the following formats, depending on the specified
         * `cameraOptions`:
         *
         * - A `String` containing the Base64-encoded photo image.
         * - A `String` representing the image file location on local storage (default).
         *
         * You can do whatever you want with the encoded image or URI, for
         * example:
         *
         * - Render the image in an `<img>` tag, as in the example below
         * - Save the data locally (`LocalStorage`, [Lawnchair](http://brianleroux.github.com/lawnchair/), etc.)
         * - Post the data to a remote server
         *
         * __NOTE__: Photo resolution on newer devices is quite good. Photos
         * selected from the device's gallery are not downscaled to a lower
         * quality, even if a `quality` parameter is specified.  To avoid common
         * memory problems, set `Camera.destinationType` to `FILE_URI` rather
         * than `DATA_URL`.
         *
         * __Supported Platforms__
         *
         * - Android
         * - BlackBerry
         * - Browser
         * - Firefox
         * - FireOS
         * - iOS
         * - Windows
         * - WP8
         * - Ubuntu
         *
         * More examples [here](#camera-getPicture-examples). Quirks [here](#camera-getPicture-quirks).
         *
         * @example
         * navigator.camera.getPicture(cameraSuccess, cameraError, cameraOptions);
         * @param {module:camera.onSuccess} successCallback
         * @param {module:camera.onError} errorCallback
         * @param {module:camera.CameraOptions} options CameraOptions
         */
        cameraExport.getPicture = function (successCallback, errorCallback, options) {
            argscheck.checkArgs('fFO', 'Camera.getPicture', arguments);
            options = options || {};
            var getValue = argscheck.getValue;

            var quality = getValue(options.quality, 50);
            var destinationType = getValue(options.destinationType, Camera.DestinationType.FILE_URI);
            var sourceType = getValue(options.sourceType, Camera.PictureSourceType.CAMERA);
            var targetWidth = getValue(options.targetWidth, -1);
            var targetHeight = getValue(options.targetHeight, -1);
            var encodingType = getValue(options.encodingType, Camera.EncodingType.JPEG);
            var mediaType = getValue(options.mediaType, Camera.MediaType.PICTURE);
            var allowEdit = !!options.allowEdit;
            var correctOrientation = !!options.correctOrientation;
            var saveToPhotoAlbum = !!options.saveToPhotoAlbum;
            var popoverOptions = getValue(options.popoverOptions, null);
            var cameraDirection = getValue(options.cameraDirection, Camera.Direction.BACK);

            var args = [quality, destinationType, sourceType, targetWidth, targetHeight, encodingType,
                mediaType, allowEdit, correctOrientation, saveToPhotoAlbum, popoverOptions, cameraDirection];

            exec(successCallback, errorCallback, "Camera", "takePicture", args);
            // XXX: commented out
            //return new CameraPopoverHandle();
        };

        /**
         * Removes intermediate image files that are kept in temporary storage
         * after calling [`camera.getPicture`]{@link module:camera.getPicture}. Applies only when the value of
         * `Camera.sourceType` equals `Camera.PictureSourceType.CAMERA` and the
         * `Camera.destinationType` equals `Camera.DestinationType.FILE_URI`.
         *
         * __Supported Platforms__
         *
         * - iOS
         *
         * @example
         * navigator.camera.cleanup(onSuccess, onFail);
         *
         * function onSuccess() {
 *     console.log("Camera cleanup success.")
 * }
         *
         * function onFail(message) {
 *     alert('Failed because: ' + message);
 * }
         */
        cameraExport.cleanup = function (successCallback, errorCallback) {
            exec(successCallback, errorCallback, "Camera", "cleanup", []);
        };

        module.exports = cameraExport;

    });

    cordova.define("cordova-plugin-camera.CameraPopoverHandle", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var exec = require('cordova/exec');

        /**
         * @namespace navigator
         */

        /**
         * A handle to an image picker popover.
         *
         * __Supported Platforms__
         *
         * - iOS
         *
         * @example
         * navigator.camera.getPicture(onSuccess, onFail,
         * {
 *     destinationType: Camera.DestinationType.FILE_URI,
 *     sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
 *     popoverOptions: new CameraPopoverOptions(300, 300, 100, 100, Camera.PopoverArrowDirection.ARROW_ANY)
 * });
         *
         * // Reposition the popover if the orientation changes.
         * window.onorientationchange = function() {
 *     var cameraPopoverHandle = new CameraPopoverHandle();
 *     var cameraPopoverOptions = new CameraPopoverOptions(0, 0, 100, 100, Camera.PopoverArrowDirection.ARROW_ANY);
 *     cameraPopoverHandle.setPosition(cameraPopoverOptions);
 * }
         * @module CameraPopoverHandle
         */
        var CameraPopoverHandle = function () {
            /**
             * Can be used to reposition the image selection dialog,
             * for example, when the device orientation changes.
             * @memberof CameraPopoverHandle
             * @instance
             * @method setPosition
             * @param {module:CameraPopoverOptions} popoverOptions
             */
            this.setPosition = function (popoverOptions) {
                var args = [popoverOptions];
                exec(null, null, "Camera", "repositionPopover", args);
            };
        };

        module.exports = CameraPopoverHandle;

    });

    cordova.define("cordova-plugin-file.DirectoryEntry", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var argscheck = require('cordova/argscheck'),
            utils = require('cordova/utils'),
            exec = require('cordova/exec'),
            Entry = require('./Entry'),
            FileError = require('./FileError'),
            DirectoryReader = require('./DirectoryReader');

        /**
         * An interface representing a directory on the file system.
         *
         * {boolean} isFile always false (readonly)
         * {boolean} isDirectory always true (readonly)
         * {DOMString} name of the directory, excluding the path leading to it (readonly)
         * {DOMString} fullPath the absolute full path to the directory (readonly)
         * {FileSystem} filesystem on which the directory resides (readonly)
         */
        var DirectoryEntry = function (name, fullPath, fileSystem, nativeURL) {

            // add trailing slash if it is missing
            if ((fullPath) && !/\/$/.test(fullPath)) {
                fullPath += "/";
            }
            // add trailing slash if it is missing
            if (nativeURL && !/\/$/.test(nativeURL)) {
                nativeURL += "/";
            }
            DirectoryEntry.__super__.constructor.call(this, false, true, name, fullPath, fileSystem, nativeURL);
        };

        utils.extend(DirectoryEntry, Entry);

        /**
         * Creates a new DirectoryReader to read entries from this directory
         */
        DirectoryEntry.prototype.createReader = function () {
            return new DirectoryReader(this.toInternalURL());
        };

        /**
         * Creates or looks up a directory
         *
         * @param {DOMString} path either a relative or absolute path from this directory in which to look up or create a directory
         * @param {Flags} options to create or exclusively create the directory
         * @param {Function} successCallback is called with the new entry
         * @param {Function} errorCallback is called with a FileError
         */
        DirectoryEntry.prototype.getDirectory = function (path, options, successCallback, errorCallback) {
            argscheck.checkArgs('sOFF', 'DirectoryEntry.getDirectory', arguments);
            var fs = this.filesystem;
            var win = successCallback && function (result) {
                    var entry = new DirectoryEntry(result.name, result.fullPath, fs, result.nativeURL);
                    successCallback(entry);
                };
            var fail = errorCallback && function (code) {
                    errorCallback(new FileError(code));
                };
            exec(win, fail, "File", "getDirectory", [this.toInternalURL(), path, options]);
        };

        /**
         * Deletes a directory and all of it's contents
         *
         * @param {Function} successCallback is called with no parameters
         * @param {Function} errorCallback is called with a FileError
         */
        DirectoryEntry.prototype.removeRecursively = function (successCallback, errorCallback) {
            argscheck.checkArgs('FF', 'DirectoryEntry.removeRecursively', arguments);
            var fail = errorCallback && function (code) {
                    errorCallback(new FileError(code));
                };
            exec(successCallback, fail, "File", "removeRecursively", [this.toInternalURL()]);
        };

        /**
         * Creates or looks up a file
         *
         * @param {DOMString} path either a relative or absolute path from this directory in which to look up or create a file
         * @param {Flags} options to create or exclusively create the file
         * @param {Function} successCallback is called with the new entry
         * @param {Function} errorCallback is called with a FileError
         */
        DirectoryEntry.prototype.getFile = function (path, options, successCallback, errorCallback) {
            argscheck.checkArgs('sOFF', 'DirectoryEntry.getFile', arguments);
            var fs = this.filesystem;
            var win = successCallback && function (result) {
                    var FileEntry = require('./FileEntry');
                    var entry = new FileEntry(result.name, result.fullPath, fs, result.nativeURL);
                    successCallback(entry);
                };
            var fail = errorCallback && function (code) {
                    errorCallback(new FileError(code));
                };
            exec(win, fail, "File", "getFile", [this.toInternalURL(), path, options]);
        };

        module.exports = DirectoryEntry;

    });

    cordova.define("cordova-plugin-file.DirectoryReader", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var exec = require('cordova/exec'),
            FileError = require('./FileError');

        /**
         * An interface that lists the files and directories in a directory.
         */
        function DirectoryReader(localURL) {
            this.localURL = localURL || null;
            this.hasReadEntries = false;
        }

        /**
         * Returns a list of entries from a directory.
         *
         * @param {Function} successCallback is called with a list of entries
         * @param {Function} errorCallback is called with a FileError
         */
        DirectoryReader.prototype.readEntries = function (successCallback, errorCallback) {
            // If we've already read and passed on this directory's entries, return an empty list.
            if (this.hasReadEntries) {
                successCallback([]);
                return;
            }
            var reader = this;
            var win = typeof successCallback !== 'function' ? null : function (result) {
                var retVal = [];
                for (var i = 0; i < result.length; i++) {
                    var entry = null;
                    if (result[i].isDirectory) {
                        entry = new (require('./DirectoryEntry'))();
                    }
                    else if (result[i].isFile) {
                        entry = new (require('./FileEntry'))();
                    }
                    entry.isDirectory = result[i].isDirectory;
                    entry.isFile = result[i].isFile;
                    entry.name = result[i].name;
                    entry.fullPath = result[i].fullPath;
                    entry.filesystem = new (require('./FileSystem'))(result[i].filesystemName);
                    entry.nativeURL = result[i].nativeURL;
                    retVal.push(entry);
                }
                reader.hasReadEntries = true;
                successCallback(retVal);
            };
            var fail = typeof errorCallback !== 'function' ? null : function (code) {
                errorCallback(new FileError(code));
            };
            exec(win, fail, "File", "readEntries", [this.localURL]);
        };

        module.exports = DirectoryReader;

    });

    cordova.define("cordova-plugin-file.Entry", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var argscheck = require('cordova/argscheck'),
            exec = require('cordova/exec'),
            FileError = require('./FileError'),
            Metadata = require('./Metadata');

        /**
         * Represents a file or directory on the local file system.
         *
         * @param isFile
         *            {boolean} true if Entry is a file (readonly)
         * @param isDirectory
         *            {boolean} true if Entry is a directory (readonly)
         * @param name
         *            {DOMString} name of the file or directory, excluding the path
         *            leading to it (readonly)
         * @param fullPath
         *            {DOMString} the absolute full path to the file or directory
         *            (readonly)
         * @param fileSystem
         *            {FileSystem} the filesystem on which this entry resides
         *            (readonly)
         * @param nativeURL
         *            {DOMString} an alternate URL which can be used by native
         *            webview controls, for example media players.
         *            (optional, readonly)
         */
        function Entry(isFile, isDirectory, name, fullPath, fileSystem, nativeURL) {
            this.isFile = !!isFile;
            this.isDirectory = !!isDirectory;
            this.name = name || '';
            this.fullPath = fullPath || '';
            this.filesystem = fileSystem || null;
            this.nativeURL = nativeURL || null;
        }

        /**
         * Look up the metadata of the entry.
         *
         * @param successCallback
         *            {Function} is called with a Metadata object
         * @param errorCallback
         *            {Function} is called with a FileError
         */
        Entry.prototype.getMetadata = function (successCallback, errorCallback) {
            argscheck.checkArgs('FF', 'Entry.getMetadata', arguments);
            var success = successCallback && function (entryMetadata) {
                    var metadata = new Metadata({
                        size: entryMetadata.size,
                        modificationTime: entryMetadata.lastModifiedDate
                    });
                    successCallback(metadata);
                };
            var fail = errorCallback && function (code) {
                    errorCallback(new FileError(code));
                };
            exec(success, fail, "File", "getFileMetadata", [this.toInternalURL()]);
        };

        /**
         * Set the metadata of the entry.
         *
         * @param successCallback
         *            {Function} is called with a Metadata object
         * @param errorCallback
         *            {Function} is called with a FileError
         * @param metadataObject
         *            {Object} keys and values to set
         */
        Entry.prototype.setMetadata = function (successCallback, errorCallback, metadataObject) {
            argscheck.checkArgs('FFO', 'Entry.setMetadata', arguments);
            exec(successCallback, errorCallback, "File", "setMetadata", [this.toInternalURL(), metadataObject]);
        };

        /**
         * Move a file or directory to a new location.
         *
         * @param parent
         *            {DirectoryEntry} the directory to which to move this entry
         * @param newName
         *            {DOMString} new name of the entry, defaults to the current name
         * @param successCallback
         *            {Function} called with the new DirectoryEntry object
         * @param errorCallback
         *            {Function} called with a FileError
         */
        Entry.prototype.moveTo = function (parent, newName, successCallback, errorCallback) {
            argscheck.checkArgs('oSFF', 'Entry.moveTo', arguments);
            var fail = errorCallback && function (code) {
                    errorCallback(new FileError(code));
                };
            var srcURL = this.toInternalURL(),
                // entry name
                name = newName || this.name,
                success = function (entry) {
                    if (entry) {
                        if (successCallback) {
                            // create appropriate Entry object
                            var newFSName = entry.filesystemName || (entry.filesystem && entry.filesystem.name);
                            var fs = newFSName ? new FileSystem(newFSName, {
                                name: "",
                                fullPath: "/"
                            }) : new FileSystem(parent.filesystem.name, {name: "", fullPath: "/"});
                            var result = (entry.isDirectory) ? new (require('./DirectoryEntry'))(entry.name, entry.fullPath, fs, entry.nativeURL) : new (require('cordova-plugin-file.FileEntry'))(entry.name, entry.fullPath, fs, entry.nativeURL);
                            successCallback(result);
                        }
                    }
                    else {
                        // no Entry object returned
                        if (fail) {
                            fail(FileError.NOT_FOUND_ERR);
                        }
                    }
                };

            // copy
            exec(success, fail, "File", "moveTo", [srcURL, parent.toInternalURL(), name]);
        };

        /**
         * Copy a directory to a different location.
         *
         * @param parent
         *            {DirectoryEntry} the directory to which to copy the entry
         * @param newName
         *            {DOMString} new name of the entry, defaults to the current name
         * @param successCallback
         *            {Function} called with the new Entry object
         * @param errorCallback
         *            {Function} called with a FileError
         */
        Entry.prototype.copyTo = function (parent, newName, successCallback, errorCallback) {
            argscheck.checkArgs('oSFF', 'Entry.copyTo', arguments);
            var fail = errorCallback && function (code) {
                    errorCallback(new FileError(code));
                };
            var srcURL = this.toInternalURL(),
                // entry name
                name = newName || this.name,
                // success callback
                success = function (entry) {
                    if (entry) {
                        if (successCallback) {
                            // create appropriate Entry object
                            var newFSName = entry.filesystemName || (entry.filesystem && entry.filesystem.name);
                            var fs = newFSName ? new FileSystem(newFSName, {
                                name: "",
                                fullPath: "/"
                            }) : new FileSystem(parent.filesystem.name, {name: "", fullPath: "/"});
                            var result = (entry.isDirectory) ? new (require('./DirectoryEntry'))(entry.name, entry.fullPath, fs, entry.nativeURL) : new (require('cordova-plugin-file.FileEntry'))(entry.name, entry.fullPath, fs, entry.nativeURL);
                            successCallback(result);
                        }
                    }
                    else {
                        // no Entry object returned
                        if (fail) {
                            fail(FileError.NOT_FOUND_ERR);
                        }
                    }
                };

            // copy
            exec(success, fail, "File", "copyTo", [srcURL, parent.toInternalURL(), name]);
        };

        /**
         * Return a URL that can be passed across the bridge to identify this entry.
         */
        Entry.prototype.toInternalURL = function () {
            if (this.filesystem && this.filesystem.__format__) {
                return this.filesystem.__format__(this.fullPath, this.nativeURL);
            }
        };

        /**
         * Return a URL that can be used to identify this entry.
         * Use a URL that can be used to as the src attribute of a <video> or
         * <audio> tag. If that is not possible, construct a cdvfile:// URL.
         */
        Entry.prototype.toURL = function () {
            if (this.nativeURL) {
                return this.nativeURL;
            }
            // fullPath attribute may contain the full URL in the case that
            // toInternalURL fails.
            return this.toInternalURL() || "file://localhost" + this.fullPath;
        };

        /**
         * Backwards-compatibility: In v1.0.0 - 1.0.2, .toURL would only return a
         * cdvfile:// URL, and this method was necessary to obtain URLs usable by the
         * webview.
         * See CB-6051, CB-6106, CB-6117, CB-6152, CB-6199, CB-6201, CB-6243, CB-6249,
         * and CB-6300.
         */
        Entry.prototype.toNativeURL = function () {
            console.log("DEPRECATED: Update your code to use 'toURL'");
            return this.toURL();
        };

        /**
         * Returns a URI that can be used to identify this entry.
         *
         * @param {DOMString} mimeType for a FileEntry, the mime type to be used to interpret the file, when loaded through this URI.
         * @return uri
         */
        Entry.prototype.toURI = function (mimeType) {
            console.log("DEPRECATED: Update your code to use 'toURL'");
            return this.toURL();
        };

        /**
         * Remove a file or directory. It is an error to attempt to delete a
         * directory that is not empty. It is an error to attempt to delete a
         * root directory of a file system.
         *
         * @param successCallback {Function} called with no parameters
         * @param errorCallback {Function} called with a FileError
         */
        Entry.prototype.remove = function (successCallback, errorCallback) {
            argscheck.checkArgs('FF', 'Entry.remove', arguments);
            var fail = errorCallback && function (code) {
                    errorCallback(new FileError(code));
                };
            exec(successCallback, fail, "File", "remove", [this.toInternalURL()]);
        };

        /**
         * Look up the parent DirectoryEntry of this entry.
         *
         * @param successCallback {Function} called with the parent DirectoryEntry object
         * @param errorCallback {Function} called with a FileError
         */
        Entry.prototype.getParent = function (successCallback, errorCallback) {
            argscheck.checkArgs('FF', 'Entry.getParent', arguments);
            var fs = this.filesystem;
            var win = successCallback && function (result) {
                    var DirectoryEntry = require('./DirectoryEntry');
                    var entry = new DirectoryEntry(result.name, result.fullPath, fs, result.nativeURL);
                    successCallback(entry);
                };
            var fail = errorCallback && function (code) {
                    errorCallback(new FileError(code));
                };
            exec(win, fail, "File", "getParent", [this.toInternalURL()]);
        };

        module.exports = Entry;

    });

    cordova.define("cordova-plugin-file.File", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * Constructor.
         * name {DOMString} name of the file, without path information
         * fullPath {DOMString} the full path of the file, including the name
         * type {DOMString} mime type
         * lastModifiedDate {Date} last modified date
         * size {Number} size of the file in bytes
         */

        var File = function (name, localURL, type, lastModifiedDate, size) {
            this.name = name || '';
            this.localURL = localURL || null;
            this.type = type || null;
            this.lastModified = lastModifiedDate || null;
            // For backwards compatibility, store the timestamp in lastModifiedDate as well
            this.lastModifiedDate = lastModifiedDate || null;
            this.size = size || 0;

            // These store the absolute start and end for slicing the file.
            this.start = 0;
            this.end = this.size;
        };

        /**
         * Returns a "slice" of the file. Since Cordova Files don't contain the actual
         * content, this really returns a File with adjusted start and end.
         * Slices of slices are supported.
         * start {Number} The index at which to start the slice (inclusive).
         * end {Number} The index at which to end the slice (exclusive).
         */
        File.prototype.slice = function (start, end) {
            var size = this.end - this.start;
            var newStart = 0;
            var newEnd = size;
            if (arguments.length) {
                if (start < 0) {
                    newStart = Math.max(size + start, 0);
                } else {
                    newStart = Math.min(size, start);
                }
            }

            if (arguments.length >= 2) {
                if (end < 0) {
                    newEnd = Math.max(size + end, 0);
                } else {
                    newEnd = Math.min(end, size);
                }
            }

            var newFile = new File(this.name, this.localURL, this.type, this.lastModified, this.size);
            newFile.start = this.start + newStart;
            newFile.end = this.start + newEnd;
            return newFile;
        };


        module.exports = File;

    });

    cordova.define("cordova-plugin-file.FileEntry", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var utils = require('cordova/utils'),
            exec = require('cordova/exec'),
            Entry = require('./Entry'),
            FileWriter = require('./FileWriter'),
            File = require('./File'),
            FileError = require('./FileError');

        /**
         * An interface representing a file on the file system.
         *
         * {boolean} isFile always true (readonly)
         * {boolean} isDirectory always false (readonly)
         * {DOMString} name of the file, excluding the path leading to it (readonly)
         * {DOMString} fullPath the absolute full path to the file (readonly)
         * {FileSystem} filesystem on which the file resides (readonly)
         */
        var FileEntry = function (name, fullPath, fileSystem, nativeURL) {
            // remove trailing slash if it is present
            if (fullPath && /\/$/.test(fullPath)) {
                fullPath = fullPath.substring(0, fullPath.length - 1);
            }
            if (nativeURL && /\/$/.test(nativeURL)) {
                nativeURL = nativeURL.substring(0, nativeURL.length - 1);
            }

            FileEntry.__super__.constructor.apply(this, [true, false, name, fullPath, fileSystem, nativeURL]);
        };

        utils.extend(FileEntry, Entry);

        /**
         * Creates a new FileWriter associated with the file that this FileEntry represents.
         *
         * @param {Function} successCallback is called with the new FileWriter
         * @param {Function} errorCallback is called with a FileError
         */
        FileEntry.prototype.createWriter = function (successCallback, errorCallback) {
            this.file(function (filePointer) {
                var writer = new FileWriter(filePointer);

                if (writer.localURL === null || writer.localURL === "") {
                    if (errorCallback) {
                        errorCallback(new FileError(FileError.INVALID_STATE_ERR));
                    }
                } else {
                    if (successCallback) {
                        successCallback(writer);
                    }
                }
            }, errorCallback);
        };

        /**
         * Returns a File that represents the current state of the file that this FileEntry represents.
         *
         * @param {Function} successCallback is called with the new File object
         * @param {Function} errorCallback is called with a FileError
         */
        FileEntry.prototype.file = function (successCallback, errorCallback) {
            var localURL = this.toInternalURL();
            var win = successCallback && function (f) {
                    var file = new File(f.name, localURL, f.type, f.lastModifiedDate, f.size);
                    successCallback(file);
                };
            var fail = errorCallback && function (code) {
                    errorCallback(new FileError(code));
                };
            exec(win, fail, "File", "getFileMetadata", [localURL]);
        };


        module.exports = FileEntry;

    });

    cordova.define("cordova-plugin-file.FileError", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * FileError
         */
        function FileError(error) {
            this.code = error || null;
        }

// File error codes
// Found in DOMException
        FileError.NOT_FOUND_ERR = 1;
        FileError.SECURITY_ERR = 2;
        FileError.ABORT_ERR = 3;

// Added by File API specification
        FileError.NOT_READABLE_ERR = 4;
        FileError.ENCODING_ERR = 5;
        FileError.NO_MODIFICATION_ALLOWED_ERR = 6;
        FileError.INVALID_STATE_ERR = 7;
        FileError.SYNTAX_ERR = 8;
        FileError.INVALID_MODIFICATION_ERR = 9;
        FileError.QUOTA_EXCEEDED_ERR = 10;
        FileError.TYPE_MISMATCH_ERR = 11;
        FileError.PATH_EXISTS_ERR = 12;

        module.exports = FileError;

    });

    cordova.define("cordova-plugin-file.FileReader", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var exec = require('cordova/exec'),
            modulemapper = require('cordova/modulemapper'),
            utils = require('cordova/utils'),
            FileError = require('./FileError'),
            ProgressEvent = require('./ProgressEvent'),
            origFileReader = modulemapper.getOriginalSymbol(window, 'FileReader');

        /**
         * This class reads the mobile device file system.
         *
         * For Android:
         *      The root directory is the root of the file system.
         *      To read from the SD card, the file name is "sdcard/my_file.txt"
         * @constructor
         */
        var FileReader = function () {
            this._readyState = 0;
            this._error = null;
            this._result = null;
            this._progress = null;
            this._localURL = '';
            this._realReader = origFileReader ? new origFileReader() : {};
        };

        /**
         * Defines the maximum size to read at a time via the native API. The default value is a compromise between
         * minimizing the overhead of many exec() calls while still reporting progress frequently enough for large files.
         * (Note attempts to allocate more than a few MB of contiguous memory on the native side are likely to cause
         * OOM exceptions, while the JS engine seems to have fewer problems managing large strings or ArrayBuffers.)
         */
        FileReader.READ_CHUNK_SIZE = 256 * 1024;

// States
        FileReader.EMPTY = 0;
        FileReader.LOADING = 1;
        FileReader.DONE = 2;

        utils.defineGetter(FileReader.prototype, 'readyState', function () {
            return this._localURL ? this._readyState : this._realReader.readyState;
        });

        utils.defineGetter(FileReader.prototype, 'error', function () {
            return this._localURL ? this._error : this._realReader.error;
        });

        utils.defineGetter(FileReader.prototype, 'result', function () {
            return this._localURL ? this._result : this._realReader.result;
        });

        function defineEvent(eventName) {
            utils.defineGetterSetter(FileReader.prototype, eventName, function () {
                return this._realReader[eventName] || null;
            }, function (value) {
                this._realReader[eventName] = value;
            });
        }

        defineEvent('onloadstart');    // When the read starts.
        defineEvent('onprogress');     // While reading (and decoding) file or fileBlob data, and reporting partial file data (progress.loaded/progress.total)
        defineEvent('onload');         // When the read has successfully completed.
        defineEvent('onerror');        // When the read has failed (see errors).
        defineEvent('onloadend');      // When the request has completed (either in success or failure).
        defineEvent('onabort');        // When the read has been aborted. For instance, by invoking the abort() method.

        function initRead(reader, file) {
            // Already loading something
            if (reader.readyState == FileReader.LOADING) {
                throw new FileError(FileError.INVALID_STATE_ERR);
            }

            reader._result = null;
            reader._error = null;
            reader._progress = 0;
            reader._readyState = FileReader.LOADING;

            if (typeof file.localURL == 'string') {
                reader._localURL = file.localURL;
            } else {
                reader._localURL = '';
                return true;
            }

            if (reader.onloadstart) {
                reader.onloadstart(new ProgressEvent("loadstart", {target: reader}));
            }
        }

        /**
         * Callback used by the following read* functions to handle incremental or final success.
         * Must be bound to the FileReader's this along with all but the last parameter,
         * e.g. readSuccessCallback.bind(this, "readAsText", "UTF-8", offset, totalSize, accumulate)
         * @param readType The name of the read function to call.
         * @param encoding Text encoding, or null if this is not a text type read.
         * @param offset Starting offset of the read.
         * @param totalSize Total number of bytes or chars to read.
         * @param accumulate A function that takes the callback result and accumulates it in this._result.
         * @param r Callback result returned by the last read exec() call, or null to begin reading.
         */
        function readSuccessCallback(readType, encoding, offset, totalSize, accumulate, r) {
            if (this._readyState === FileReader.DONE) {
                return;
            }

            var CHUNK_SIZE = FileReader.READ_CHUNK_SIZE;
            if (readType === 'readAsDataURL') {
                // Windows proxy does not support reading file slices as Data URLs
                // so read the whole file at once.
                CHUNK_SIZE = cordova.platformId === 'windows' ? totalSize :
                    // Calculate new chunk size for data URLs to be multiply of 3
                    // Otherwise concatenated base64 chunks won't be valid base64 data
                FileReader.READ_CHUNK_SIZE - (FileReader.READ_CHUNK_SIZE % 3) + 3;
            }

            if (typeof r !== "undefined") {
                accumulate(r);
                this._progress = Math.min(this._progress + CHUNK_SIZE, totalSize);

                if (typeof this.onprogress === "function") {
                    this.onprogress(new ProgressEvent("progress", {loaded: this._progress, total: totalSize}));
                }
            }

            if (typeof r === "undefined" || this._progress < totalSize) {
                var execArgs = [
                    this._localURL,
                    offset + this._progress,
                    offset + this._progress + Math.min(totalSize - this._progress, CHUNK_SIZE)];
                if (encoding) {
                    execArgs.splice(1, 0, encoding);
                }
                exec(
                    readSuccessCallback.bind(this, readType, encoding, offset, totalSize, accumulate),
                    readFailureCallback.bind(this),
                    "File", readType, execArgs);
            } else {
                this._readyState = FileReader.DONE;

                if (typeof this.onload === "function") {
                    this.onload(new ProgressEvent("load", {target: this}));
                }

                if (typeof this.onloadend === "function") {
                    this.onloadend(new ProgressEvent("loadend", {target: this}));
                }
            }
        }

        /**
         * Callback used by the following read* functions to handle errors.
         * Must be bound to the FileReader's this, e.g. readFailureCallback.bind(this)
         */
        function readFailureCallback(e) {
            if (this._readyState === FileReader.DONE) {
                return;
            }

            this._readyState = FileReader.DONE;
            this._result = null;
            this._error = new FileError(e);

            if (typeof this.onerror === "function") {
                this.onerror(new ProgressEvent("error", {target: this}));
            }

            if (typeof this.onloadend === "function") {
                this.onloadend(new ProgressEvent("loadend", {target: this}));
            }
        }

        /**
         * Abort reading file.
         */
        FileReader.prototype.abort = function () {
            if (origFileReader && !this._localURL) {
                return this._realReader.abort();
            }
            this._result = null;

            if (this._readyState == FileReader.DONE || this._readyState == FileReader.EMPTY) {
                return;
            }

            this._readyState = FileReader.DONE;

            // If abort callback
            if (typeof this.onabort === 'function') {
                this.onabort(new ProgressEvent('abort', {target: this}));
            }
            // If load end callback
            if (typeof this.onloadend === 'function') {
                this.onloadend(new ProgressEvent('loadend', {target: this}));
            }
        };

        /**
         * Read text file.
         *
         * @param file          {File} File object containing file properties
         * @param encoding      [Optional] (see http://www.iana.org/assignments/character-sets)
         */
        FileReader.prototype.readAsText = function (file, encoding) {
            if (initRead(this, file)) {
                return this._realReader.readAsText(file, encoding);
            }

            // Default encoding is UTF-8
            var enc = encoding ? encoding : "UTF-8";

            var totalSize = file.end - file.start;
            readSuccessCallback.bind(this)("readAsText", enc, file.start, totalSize, function (r) {
                if (this._progress === 0) {
                    this._result = "";
                }
                this._result += r;
            }.bind(this));
        };


        /**
         * Read file and return data as a base64 encoded data url.
         * A data url is of the form:
         *      data:[<mediatype>][;base64],<data>
         *
         * @param file          {File} File object containing file properties
         */
        FileReader.prototype.readAsDataURL = function (file) {
            if (initRead(this, file)) {
                return this._realReader.readAsDataURL(file);
            }

            var totalSize = file.end - file.start;
            readSuccessCallback.bind(this)("readAsDataURL", null, file.start, totalSize, function (r) {
                var commaIndex = r.indexOf(',');
                if (this._progress === 0) {
                    this._result = r;
                } else {
                    this._result += r.substring(commaIndex + 1);
                }
            }.bind(this));
        };

        /**
         * Read file and return data as a binary data.
         *
         * @param file          {File} File object containing file properties
         */
        FileReader.prototype.readAsBinaryString = function (file) {
            if (initRead(this, file)) {
                return this._realReader.readAsBinaryString(file);
            }

            var totalSize = file.end - file.start;
            readSuccessCallback.bind(this)("readAsBinaryString", null, file.start, totalSize, function (r) {
                if (this._progress === 0) {
                    this._result = "";
                }
                this._result += r;
            }.bind(this));
        };

        /**
         * Read file and return data as a binary data.
         *
         * @param file          {File} File object containing file properties
         */
        FileReader.prototype.readAsArrayBuffer = function (file) {
            if (initRead(this, file)) {
                return this._realReader.readAsArrayBuffer(file);
            }

            var totalSize = file.end - file.start;
            readSuccessCallback.bind(this)("readAsArrayBuffer", null, file.start, totalSize, function (r) {
                var resultArray = (this._progress === 0 ? new Uint8Array(totalSize) : new Uint8Array(this._result));
                resultArray.set(new Uint8Array(r), this._progress);
                this._result = resultArray.buffer;
            }.bind(this));
        };

        module.exports = FileReader;

    });

    cordova.define("cordova-plugin-file.FileSystem", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var DirectoryEntry = require('./DirectoryEntry');

        /**
         * An interface representing a file system
         *
         * @constructor
         * {DOMString} name the unique name of the file system (readonly)
         * {DirectoryEntry} root directory of the file system (readonly)
         */
        var FileSystem = function (name, root) {
            this.name = name;
            if (root) {
                this.root = new DirectoryEntry(root.name, root.fullPath, this, root.nativeURL);
            } else {
                this.root = new DirectoryEntry(this.name, '/', this);
            }
        };

        FileSystem.prototype.__format__ = function (fullPath, nativeUrl) {
            return fullPath;
        };

        FileSystem.prototype.toJSON = function () {
            return "<FileSystem: " + this.name + ">";
        };

// Use instead of encodeURI() when encoding just the path part of a URI rather than an entire URI.
        FileSystem.encodeURIPath = function (path) {
            // Because # is a valid filename character, it must be encoded to prevent part of the
            // path from being parsed as a URI fragment.
            return encodeURI(path).replace(/#/g, '%23');
        };

        module.exports = FileSystem;

    });

    cordova.define("cordova-plugin-file.FileUploadOptions", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * Options to customize the HTTP request used to upload files.
         * @constructor
         * @param fileKey {String}   Name of file request parameter.
         * @param fileName {String}  Filename to be used by the server. Defaults to image.jpg.
         * @param mimeType {String}  Mimetype of the uploaded file. Defaults to image/jpeg.
         * @param params {Object}    Object with key: value params to send to the server.
         * @param headers {Object}   Keys are header names, values are header values. Multiple
         *                           headers of the same name are not supported.
         */
        var FileUploadOptions = function (fileKey, fileName, mimeType, params, headers, httpMethod) {
            this.fileKey = fileKey || null;
            this.fileName = fileName || null;
            this.mimeType = mimeType || null;
            this.params = params || null;
            this.headers = headers || null;
            this.httpMethod = httpMethod || null;
        };

        module.exports = FileUploadOptions;

    });

    cordova.define("cordova-plugin-file.FileUploadResult", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * FileUploadResult
         * @constructor
         */
        module.exports = function FileUploadResult(size, code, content) {
            this.bytesSent = size;
            this.responseCode = code;
            this.response = content;
        };
    });

    cordova.define("cordova-plugin-file.FileWriter", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var exec = require('cordova/exec'),
            FileError = require('./FileError'),
            ProgressEvent = require('./ProgressEvent');

        /**
         * This class writes to the mobile device file system.
         *
         * For Android:
         *      The root directory is the root of the file system.
         *      To write to the SD card, the file name is "sdcard/my_file.txt"
         *
         * @constructor
         * @param file {File} File object containing file properties
         * @param append if true write to the end of the file, otherwise overwrite the file
         */
        var FileWriter = function (file) {
            this.fileName = "";
            this.length = 0;
            if (file) {
                this.localURL = file.localURL || file;
                this.length = file.size || 0;
            }
            // default is to write at the beginning of the file
            this.position = 0;

            this.readyState = 0; // EMPTY

            this.result = null;

            // Error
            this.error = null;

            // Event handlers
            this.onwritestart = null;   // When writing starts
            this.onprogress = null;     // While writing the file, and reporting partial file data
            this.onwrite = null;        // When the write has successfully completed.
            this.onwriteend = null;     // When the request has completed (either in success or failure).
            this.onabort = null;        // When the write has been aborted. For instance, by invoking the abort() method.
            this.onerror = null;        // When the write has failed (see errors).
        };

// States
        FileWriter.INIT = 0;
        FileWriter.WRITING = 1;
        FileWriter.DONE = 2;

        /**
         * Abort writing file.
         */
        FileWriter.prototype.abort = function () {
            // check for invalid state
            if (this.readyState === FileWriter.DONE || this.readyState === FileWriter.INIT) {
                throw new FileError(FileError.INVALID_STATE_ERR);
            }

            // set error
            this.error = new FileError(FileError.ABORT_ERR);

            this.readyState = FileWriter.DONE;

            // If abort callback
            if (typeof this.onabort === "function") {
                this.onabort(new ProgressEvent("abort", {"target": this}));
            }

            // If write end callback
            if (typeof this.onwriteend === "function") {
                this.onwriteend(new ProgressEvent("writeend", {"target": this}));
            }
        };

        /**
         * Writes data to the file
         *
         * @param data text or blob to be written
         * @param isPendingBlobReadResult {Boolean} true if the data is the pending blob read operation result
         */
        FileWriter.prototype.write = function (data, isPendingBlobReadResult) {

            var that = this;
            var supportsBinary = (typeof window.Blob !== 'undefined' && typeof window.ArrayBuffer !== 'undefined');
            var isProxySupportBlobNatively = (cordova.platformId === "windows8" || cordova.platformId === "windows");
            var isBinary;

            // Check to see if the incoming data is a blob
            if (data instanceof File || (!isProxySupportBlobNatively && supportsBinary && data instanceof Blob)) {
                var fileReader = new FileReader();
                fileReader.onload = function () {
                    // Call this method again, with the arraybuffer as argument
                    FileWriter.prototype.write.call(that, this.result, true /* isPendingBlobReadResult */);
                };
                fileReader.onerror = function () {
                    // DONE state
                    that.readyState = FileWriter.DONE;

                    // Save error
                    that.error = this.error;

                    // If onerror callback
                    if (typeof that.onerror === "function") {
                        that.onerror(new ProgressEvent("error", {"target": that}));
                    }

                    // If onwriteend callback
                    if (typeof that.onwriteend === "function") {
                        that.onwriteend(new ProgressEvent("writeend", {"target": that}));
                    }
                };

                // WRITING state
                this.readyState = FileWriter.WRITING;

                if (supportsBinary) {
                    fileReader.readAsArrayBuffer(data);
                } else {
                    fileReader.readAsText(data);
                }
                return;
            }

            // Mark data type for safer transport over the binary bridge
            isBinary = supportsBinary && (data instanceof ArrayBuffer);
            if (isBinary && cordova.platformId === "windowsphone") {
                // create a plain array, using the keys from the Uint8Array view so that we can serialize it
                data = Array.apply(null, new Uint8Array(data));
            }

            // Throw an exception if we are already writing a file
            if (this.readyState === FileWriter.WRITING && !isPendingBlobReadResult) {
                throw new FileError(FileError.INVALID_STATE_ERR);
            }

            // WRITING state
            this.readyState = FileWriter.WRITING;

            var me = this;

            // If onwritestart callback
            if (typeof me.onwritestart === "function") {
                me.onwritestart(new ProgressEvent("writestart", {"target": me}));
            }

            // Write file
            exec(
                // Success callback
                function (r) {
                    // If DONE (cancelled), then don't do anything
                    if (me.readyState === FileWriter.DONE) {
                        return;
                    }

                    // position always increases by bytes written because file would be extended
                    me.position += r;
                    // The length of the file is now where we are done writing.

                    me.length = me.position;

                    // DONE state
                    me.readyState = FileWriter.DONE;

                    // If onwrite callback
                    if (typeof me.onwrite === "function") {
                        me.onwrite(new ProgressEvent("write", {"target": me}));
                    }

                    // If onwriteend callback
                    if (typeof me.onwriteend === "function") {
                        me.onwriteend(new ProgressEvent("writeend", {"target": me}));
                    }
                },
                // Error callback
                function (e) {
                    // If DONE (cancelled), then don't do anything
                    if (me.readyState === FileWriter.DONE) {
                        return;
                    }

                    // DONE state
                    me.readyState = FileWriter.DONE;

                    // Save error
                    me.error = new FileError(e);

                    // If onerror callback
                    if (typeof me.onerror === "function") {
                        me.onerror(new ProgressEvent("error", {"target": me}));
                    }

                    // If onwriteend callback
                    if (typeof me.onwriteend === "function") {
                        me.onwriteend(new ProgressEvent("writeend", {"target": me}));
                    }
                }, "File", "write", [this.localURL, data, this.position, isBinary]);
        };

        /**
         * Moves the file pointer to the location specified.
         *
         * If the offset is a negative number the position of the file
         * pointer is rewound.  If the offset is greater than the file
         * size the position is set to the end of the file.
         *
         * @param offset is the location to move the file pointer to.
         */
        FileWriter.prototype.seek = function (offset) {
            // Throw an exception if we are already writing a file
            if (this.readyState === FileWriter.WRITING) {
                throw new FileError(FileError.INVALID_STATE_ERR);
            }

            if (!offset && offset !== 0) {
                return;
            }

            // See back from end of file.
            if (offset < 0) {
                this.position = Math.max(offset + this.length, 0);
            }
            // Offset is bigger than file size so set position
            // to the end of the file.
            else if (offset > this.length) {
                this.position = this.length;
            }
            // Offset is between 0 and file size so set the position
            // to start writing.
            else {
                this.position = offset;
            }
        };

        /**
         * Truncates the file to the size specified.
         *
         * @param size to chop the file at.
         */
        FileWriter.prototype.truncate = function (size) {
            // Throw an exception if we are already writing a file
            if (this.readyState === FileWriter.WRITING) {
                throw new FileError(FileError.INVALID_STATE_ERR);
            }

            // WRITING state
            this.readyState = FileWriter.WRITING;

            var me = this;

            // If onwritestart callback
            if (typeof me.onwritestart === "function") {
                me.onwritestart(new ProgressEvent("writestart", {"target": this}));
            }

            // Write file
            exec(
                // Success callback
                function (r) {
                    // If DONE (cancelled), then don't do anything
                    if (me.readyState === FileWriter.DONE) {
                        return;
                    }

                    // DONE state
                    me.readyState = FileWriter.DONE;

                    // Update the length of the file
                    me.length = r;
                    me.position = Math.min(me.position, r);

                    // If onwrite callback
                    if (typeof me.onwrite === "function") {
                        me.onwrite(new ProgressEvent("write", {"target": me}));
                    }

                    // If onwriteend callback
                    if (typeof me.onwriteend === "function") {
                        me.onwriteend(new ProgressEvent("writeend", {"target": me}));
                    }
                },
                // Error callback
                function (e) {
                    // If DONE (cancelled), then don't do anything
                    if (me.readyState === FileWriter.DONE) {
                        return;
                    }

                    // DONE state
                    me.readyState = FileWriter.DONE;

                    // Save error
                    me.error = new FileError(e);

                    // If onerror callback
                    if (typeof me.onerror === "function") {
                        me.onerror(new ProgressEvent("error", {"target": me}));
                    }

                    // If onwriteend callback
                    if (typeof me.onwriteend === "function") {
                        me.onwriteend(new ProgressEvent("writeend", {"target": me}));
                    }
                }, "File", "truncate", [this.localURL, size]);
        };

        module.exports = FileWriter;

    });

    cordova.define("cordova-plugin-file.Flags", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * Supplies arguments to methods that lookup or create files and directories.
         *
         * @param create
         *            {boolean} file or directory if it doesn't exist
         * @param exclusive
         *            {boolean} used with create; if true the command will fail if
         *            target path exists
         */
        function Flags(create, exclusive) {
            this.create = create || false;
            this.exclusive = exclusive || false;
        }

        module.exports = Flags;

    });

    cordova.define("cordova-plugin-file.LocalFileSystem", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        exports.TEMPORARY = 0;
        exports.PERSISTENT = 1;

    });

    cordova.define("cordova-plugin-file.Metadata", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * Information about the state of the file or directory
         *
         * {Date} modificationTime (readonly)
         */
        var Metadata = function (metadata) {
            if (typeof metadata == "object") {
                this.modificationTime = new Date(metadata.modificationTime);
                this.size = metadata.size || 0;
            } else if (typeof metadata == "undefined") {
                this.modificationTime = null;
                this.size = 0;
            } else {
                /* Backwards compatiblity with platforms that only return a timestamp */
                this.modificationTime = new Date(metadata);
            }
        };

        module.exports = Metadata;

    });

    cordova.define("cordova-plugin-file.ProgressEvent", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

// If ProgressEvent exists in global context, use it already, otherwise use our own polyfill
// Feature test: See if we can instantiate a native ProgressEvent;
// if so, use that approach,
// otherwise fill-in with our own implementation.
//
// NOTE: right now we always fill in with our own. Down the road would be nice if we can use whatever is native in the webview.
        var ProgressEvent = (function () {
            /*
             var createEvent = function(data) {
             var event = document.createEvent('Events');
             event.initEvent('ProgressEvent', false, false);
             if (data) {
             for (var i in data) {
             if (data.hasOwnProperty(i)) {
             event[i] = data[i];
             }
             }
             if (data.target) {
             // TODO: cannot call <some_custom_object>.dispatchEvent
             // need to first figure out how to implement EventTarget
             }
             }
             return event;
             };
             try {
             var ev = createEvent({type:"abort",target:document});
             return function ProgressEvent(type, data) {
             data.type = type;
             return createEvent(data);
             };
             } catch(e){
             */
            return function ProgressEvent(type, dict) {
                this.type = type;
                this.bubbles = false;
                this.cancelBubble = false;
                this.cancelable = false;
                this.lengthComputable = false;
                this.loaded = dict && dict.loaded ? dict.loaded : 0;
                this.total = dict && dict.total ? dict.total : 0;
                this.target = dict && dict.target ? dict.target : null;
            };
            //}
        })();

        module.exports = ProgressEvent;

    });

    cordova.define("cordova-plugin-file.fileSystems", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

// Overridden by Android, BlackBerry 10 and iOS to populate fsMap.
        module.exports.getFs = function (name, callback) {
            callback(null);
        };

    });

    cordova.define("cordova-plugin-file.requestFileSystem", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        (function () {
            //For browser platform: not all browsers use this file.
            function checkBrowser() {
                if (cordova.platformId === "browser" && require('./isChrome')()) {
                    module.exports = window.requestFileSystem || window.webkitRequestFileSystem;
                    return true;
                }
                return false;
            }

            if (checkBrowser()) {
                return;
            }

            var argscheck = require('cordova/argscheck'),
                FileError = require('./FileError'),
                FileSystem = require('./FileSystem'),
                exec = require('cordova/exec');
            var fileSystems = require('./fileSystems');

            /**
             * Request a file system in which to store application data.
             * @param type  local file system type
             * @param size  indicates how much storage space, in bytes, the application expects to need
             * @param successCallback  invoked with a FileSystem object
             * @param errorCallback  invoked if error occurs retrieving file system
             */
            var requestFileSystem = function (type, size, successCallback, errorCallback) {
                argscheck.checkArgs('nnFF', 'requestFileSystem', arguments);
                var fail = function (code) {
                    if (errorCallback) {
                        errorCallback(new FileError(code));
                    }
                };

                if (type < 0) {
                    fail(FileError.SYNTAX_ERR);
                } else {
                    // if successful, return a FileSystem object
                    var success = function (file_system) {
                        if (file_system) {
                            if (successCallback) {
                                fileSystems.getFs(file_system.name, function (fs) {
                                    // This should happen only on platforms that haven't implemented requestAllFileSystems (windows)
                                    if (!fs) {
                                        fs = new FileSystem(file_system.name, file_system.root);
                                    }
                                    successCallback(fs);
                                });
                            }
                        }
                        else {
                            // no FileSystem object returned
                            fail(FileError.NOT_FOUND_ERR);
                        }
                    };
                    exec(success, fail, "File", "requestFileSystem", [type, size]);
                }
            };

            module.exports = requestFileSystem;
        })();

    });

    cordova.define("cordova-plugin-file.resolveLocalFileSystemURI", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */
        (function () {
            //For browser platform: not all browsers use overrided `resolveLocalFileSystemURL`.
            function checkBrowser() {
                if (cordova.platformId === "browser" && require('./isChrome')()) {
                    module.exports.resolveLocalFileSystemURL = window.resolveLocalFileSystemURL || window.webkitResolveLocalFileSystemURL;
                    return true;
                }
                return false;
            }

            if (checkBrowser()) {
                return;
            }

            var argscheck = require('cordova/argscheck'),
                DirectoryEntry = require('./DirectoryEntry'),
                FileEntry = require('./FileEntry'),
                FileError = require('./FileError'),
                exec = require('cordova/exec');
            var fileSystems = require('./fileSystems');

            /**
             * Look up file system Entry referred to by local URI.
             * @param {DOMString} uri  URI referring to a local file or directory
             * @param successCallback  invoked with Entry object corresponding to URI
             * @param errorCallback    invoked if error occurs retrieving file system entry
             */
            module.exports.resolveLocalFileSystemURL = module.exports.resolveLocalFileSystemURL || function (uri, successCallback, errorCallback) {
                    argscheck.checkArgs('sFF', 'resolveLocalFileSystemURI', arguments);
                    // error callback
                    var fail = function (error) {
                        if (errorCallback) {
                            errorCallback(new FileError(error));
                        }
                    };
                    // sanity check for 'not:valid:filename' or '/not:valid:filename'
                    // file.spec.12 window.resolveLocalFileSystemURI should error (ENCODING_ERR) when resolving invalid URI with leading /.
                    if (!uri || uri.split(":").length > 2) {
                        setTimeout(function () {
                            fail(FileError.ENCODING_ERR);
                        }, 0);
                        return;
                    }
                    // if successful, return either a file or directory entry
                    var success = function (entry) {
                        if (entry) {
                            if (successCallback) {
                                // create appropriate Entry object
                                var fsName = entry.filesystemName || (entry.filesystem && entry.filesystem.name) || (entry.filesystem == window.PERSISTENT ? 'persistent' : 'temporary');
                                fileSystems.getFs(fsName, function (fs) {
                                    // This should happen only on platforms that haven't implemented requestAllFileSystems (windows)
                                    if (!fs) {
                                        fs = new FileSystem(fsName, {name: "", fullPath: "/"});
                                    }
                                    var result = (entry.isDirectory) ? new DirectoryEntry(entry.name, entry.fullPath, fs, entry.nativeURL) : new FileEntry(entry.name, entry.fullPath, fs, entry.nativeURL);
                                    successCallback(result);
                                });
                            }
                        }
                        else {
                            // no Entry object returned
                            fail(FileError.NOT_FOUND_ERR);
                        }
                    };

                    exec(success, fail, "File", "resolveLocalFileSystemURI", [uri]);
                };

            module.exports.resolveLocalFileSystemURI = function () {
                console.log("resolveLocalFileSystemURI is deprecated. Please call resolveLocalFileSystemURL instead.");
                module.exports.resolveLocalFileSystemURL.apply(this, arguments);
            };
        })();

    });

    cordova.define("cordova-plugin-file.isChrome", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        module.exports = function () {
            // window.webkitRequestFileSystem and window.webkitResolveLocalFileSystemURL are available only in Chrome and
            // possibly a good flag to indicate that we're running in Chrome
            return window.webkitRequestFileSystem && window.webkitResolveLocalFileSystemURL;
        };

    });

    cordova.define("cordova-plugin-file.iosFileSystem", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        FILESYSTEM_PROTOCOL = "cdvfile";

        module.exports = {
            __format__: function (fullPath) {
                var path = ('/' + this.name + (fullPath[0] === '/' ? '' : '/') + FileSystem.encodeURIPath(fullPath)).replace('//', '/');
                return FILESYSTEM_PROTOCOL + '://localhost' + path;
            }
        };


    });

    cordova.define("cordova-plugin-file.fileSystems-roots", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

// Map of fsName -> FileSystem.
        var fsMap = null;
        var FileSystem = require('./FileSystem');
        var exec = require('cordova/exec');

// Overridden by Android, BlackBerry 10 and iOS to populate fsMap.
        require('./fileSystems').getFs = function (name, callback) {
            function success(response) {
                fsMap = {};
                for (var i = 0; i < response.length; ++i) {
                    var fsRoot = response[i];
                    var fs = new FileSystem(fsRoot.filesystemName, fsRoot);
                    fsMap[fs.name] = fs;
                }
                callback(fsMap[name]);
            }

            if (fsMap) {
                callback(fsMap[name]);
            } else {
                exec(success, null, "File", "requestAllFileSystems", []);
            }
        };


    });

    cordova.define("cordova-plugin-file.fileSystemPaths", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var exec = require('cordova/exec');
        var channel = require('cordova/channel');

        exports.file = {
            // Read-only directory where the application is installed.
            applicationDirectory: null,
            // Root of app's private writable storage
            applicationStorageDirectory: null,
            // Where to put app-specific data files.
            dataDirectory: null,
            // Cached files that should survive app restarts.
            // Apps should not rely on the OS to delete files in here.
            cacheDirectory: null,
            // Android: the application space on external storage.
            externalApplicationStorageDirectory: null,
            // Android: Where to put app-specific data files on external storage.
            externalDataDirectory: null,
            // Android: the application cache on external storage.
            externalCacheDirectory: null,
            // Android: the external storage (SD card) root.
            externalRootDirectory: null,
            // iOS: Temp directory that the OS can clear at will.
            tempDirectory: null,
            // iOS: Holds app-specific files that should be synced (e.g. to iCloud).
            syncedDataDirectory: null,
            // iOS: Files private to the app, but that are meaningful to other applications (e.g. Office files)
            documentsDirectory: null,
            // BlackBerry10: Files globally available to all apps
            sharedDirectory: null
        };

        channel.waitForInitialization('onFileSystemPathsReady');
        channel.onCordovaReady.subscribe(function () {
            function after(paths) {
                for (var k in paths) {
                    exports.file[k] = paths[k];
                }
                channel.initializationComplete('onFileSystemPathsReady');
            }

            exec(after, null, 'File', 'requestAllPaths', []);
        });


    });

    cordova.define("cordova-plugin-file-transfer.FileTransferError", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * FileTransferError
         * @constructor
         */
        var FileTransferError = function (code, source, target, status, body, exception) {
            this.code = code || null;
            this.source = source || null;
            this.target = target || null;
            this.http_status = status || null;
            this.body = body || null;
            this.exception = exception || null;
        };

        FileTransferError.FILE_NOT_FOUND_ERR = 1;
        FileTransferError.INVALID_URL_ERR = 2;
        FileTransferError.CONNECTION_ERR = 3;
        FileTransferError.ABORT_ERR = 4;
        FileTransferError.NOT_MODIFIED_ERR = 5;

        module.exports = FileTransferError;

    });

    cordova.define("cordova-plugin-file-transfer.FileTransfer", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /* global cordova, FileSystem */

        var argscheck = require('cordova/argscheck'),
            exec = require('cordova/exec'),
            FileTransferError = require('./FileTransferError'),
            ProgressEvent = require('cordova-plugin-file.ProgressEvent');

        function newProgressEvent(result) {
            var pe = new ProgressEvent();
            pe.lengthComputable = result.lengthComputable;
            pe.loaded = result.loaded;
            pe.total = result.total;
            return pe;
        }

        function getUrlCredentials(urlString) {
            var credentialsPattern = /^https?\:\/\/(?:(?:(([^:@\/]*)(?::([^@\/]*))?)?@)?([^:\/?#]*)(?::(\d*))?).*$/,
                credentials = credentialsPattern.exec(urlString);

            return credentials && credentials[1];
        }

        function getBasicAuthHeader(urlString) {
            var header = null;


            // This is changed due to MS Windows doesn't support credentials in http uris
            // so we detect them by regexp and strip off from result url
            // Proof: http://social.msdn.microsoft.com/Forums/windowsapps/en-US/a327cf3c-f033-4a54-8b7f-03c56ba3203f/windows-foundation-uri-security-problem

            if (window.btoa) {
                var credentials = getUrlCredentials(urlString);
                if (credentials) {
                    var authHeader = "Authorization";
                    var authHeaderValue = "Basic " + window.btoa(credentials);

                    header = {
                        name: authHeader,
                        value: authHeaderValue
                    };
                }
            }

            return header;
        }

        function convertHeadersToArray(headers) {
            var result = [];
            for (var header in headers) {
                if (headers.hasOwnProperty(header)) {
                    var headerValue = headers[header];
                    result.push({
                        name: header,
                        value: headerValue.toString()
                    });
                }
            }
            return result;
        }

        var idCounter = 0;

        /**
         * FileTransfer uploads a file to a remote server.
         * @constructor
         */
        var FileTransfer = function () {
            this._id = ++idCounter;
            this.onprogress = null; // optional callback
        };

        /**
         * Given an absolute file path, uploads a file on the device to a remote server
         * using a multipart HTTP request.
         * @param filePath {String}           Full path of the file on the device
         * @param server {String}             URL of the server to receive the file
         * @param successCallback (Function}  Callback to be invoked when upload has completed
         * @param errorCallback {Function}    Callback to be invoked upon error
         * @param options {FileUploadOptions} Optional parameters such as file name and mimetype
         * @param trustAllHosts {Boolean} Optional trust all hosts (e.g. for self-signed certs), defaults to false
         */
        FileTransfer.prototype.upload = function (filePath, server, successCallback, errorCallback, options, trustAllHosts) {
            argscheck.checkArgs('ssFFO*', 'FileTransfer.upload', arguments);
            // check for options
            var fileKey = null;
            var fileName = null;
            var mimeType = null;
            var params = null;
            var chunkedMode = true;
            var headers = null;
            var httpMethod = null;
            var basicAuthHeader = getBasicAuthHeader(server);
            if (basicAuthHeader) {
                server = server.replace(getUrlCredentials(server) + '@', '');

                options = options || {};
                options.headers = options.headers || {};
                options.headers[basicAuthHeader.name] = basicAuthHeader.value;
            }

            if (options) {
                fileKey = options.fileKey;
                fileName = options.fileName;
                mimeType = options.mimeType;
                headers = options.headers;
                httpMethod = options.httpMethod || "POST";
                if (httpMethod.toUpperCase() == "PUT") {
                    httpMethod = "PUT";
                } else {
                    httpMethod = "POST";
                }
                if (options.chunkedMode !== null || typeof options.chunkedMode != "undefined") {
                    chunkedMode = options.chunkedMode;
                }
                if (options.params) {
                    params = options.params;
                }
                else {
                    params = {};
                }
            }

            if (cordova.platformId === "windowsphone") {
                headers = headers && convertHeadersToArray(headers);
                params = params && convertHeadersToArray(params);
            }

            var fail = errorCallback && function (e) {
                    var error = new FileTransferError(e.code, e.source, e.target, e.http_status, e.body, e.exception);
                    errorCallback(error);
                };

            var self = this;
            var win = function (result) {
                if (typeof result.lengthComputable != "undefined") {
                    if (self.onprogress) {
                        self.onprogress(newProgressEvent(result));
                    }
                } else {
                    if (successCallback) {
                        successCallback(result);
                    }
                }
            };
            exec(win, fail, 'FileTransfer', 'upload', [filePath, server, fileKey, fileName, mimeType, params, trustAllHosts, chunkedMode, headers, this._id, httpMethod]);
        };

        /**
         * Downloads a file form a given URL and saves it to the specified directory.
         * @param source {String}          URL of the server to receive the file
         * @param target {String}         Full path of the file on the device
         * @param successCallback (Function}  Callback to be invoked when upload has completed
         * @param errorCallback {Function}    Callback to be invoked upon error
         * @param trustAllHosts {Boolean} Optional trust all hosts (e.g. for self-signed certs), defaults to false
         * @param options {FileDownloadOptions} Optional parameters such as headers
         */
        FileTransfer.prototype.download = function (source, target, successCallback, errorCallback, trustAllHosts, options) {
            argscheck.checkArgs('ssFF*', 'FileTransfer.download', arguments);
            var self = this;

            var basicAuthHeader = getBasicAuthHeader(source);
            if (basicAuthHeader) {
                source = source.replace(getUrlCredentials(source) + '@', '');

                options = options || {};
                options.headers = options.headers || {};
                options.headers[basicAuthHeader.name] = basicAuthHeader.value;
            }

            var headers = null;
            if (options) {
                headers = options.headers || null;
            }

            if (cordova.platformId === "windowsphone" && headers) {
                headers = convertHeadersToArray(headers);
            }

            var win = function (result) {
                if (typeof result.lengthComputable != "undefined") {
                    if (self.onprogress) {
                        return self.onprogress(newProgressEvent(result));
                    }
                } else if (successCallback) {
                    var entry = null;
                    if (result.isDirectory) {
                        entry = new (require('cordova-plugin-file.DirectoryEntry'))();
                    }
                    else if (result.isFile) {
                        entry = new (require('cordova-plugin-file.FileEntry'))();
                    }
                    entry.isDirectory = result.isDirectory;
                    entry.isFile = result.isFile;
                    entry.name = result.name;
                    entry.fullPath = result.fullPath;
                    entry.filesystem = new FileSystem(result.filesystemName || (result.filesystem == window.PERSISTENT ? 'persistent' : 'temporary'));
                    entry.nativeURL = result.nativeURL;
                    successCallback(entry);
                }
            };

            var fail = errorCallback && function (e) {
                    var error = new FileTransferError(e.code, e.source, e.target, e.http_status, e.body, e.exception);
                    errorCallback(error);
                };

            exec(win, fail, 'FileTransfer', 'download', [source, target, trustAllHosts, this._id, headers]);
        };

        /**
         * Aborts the ongoing file transfer on this object. The original error
         * callback for the file transfer will be called if necessary.
         */
        FileTransfer.prototype.abort = function () {
            exec(null, null, 'FileTransfer', 'abort', [this._id]);
        };

        module.exports = FileTransfer;

    });

    cordova.define("cordova-plugin-geolocation.Coordinates", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * This class contains position information.
         * @param {Object} lat
         * @param {Object} lng
         * @param {Object} alt
         * @param {Object} acc
         * @param {Object} head
         * @param {Object} vel
         * @param {Object} altacc
         * @constructor
         */
        var Coordinates = function (lat, lng, alt, acc, head, vel, altacc) {
            /**
             * The latitude of the position.
             */
            this.latitude = lat;
            /**
             * The longitude of the position,
             */
            this.longitude = lng;
            /**
             * The accuracy of the position.
             */
            this.accuracy = acc;
            /**
             * The altitude of the position.
             */
            this.altitude = (alt !== undefined ? alt : null);
            /**
             * The direction the device is moving at the position.
             */
            this.heading = (head !== undefined ? head : null);
            /**
             * The velocity with which the device is moving at the position.
             */
            this.speed = (vel !== undefined ? vel : null);

            if (this.speed === 0 || this.speed === null) {
                this.heading = NaN;
            }

            /**
             * The altitude accuracy of the position.
             */
            this.altitudeAccuracy = (altacc !== undefined) ? altacc : null;
        };

        module.exports = Coordinates;

    });

    cordova.define("cordova-plugin-geolocation.PositionError", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * Position error object
         *
         * @constructor
         * @param code
         * @param message
         */
        var PositionError = function (code, message) {
            this.code = code || null;
            this.message = message || '';
        };

        PositionError.prototype.PERMISSION_DENIED = PositionError.PERMISSION_DENIED = 1;
        PositionError.prototype.POSITION_UNAVAILABLE = PositionError.POSITION_UNAVAILABLE = 2;
        PositionError.prototype.TIMEOUT = PositionError.TIMEOUT = 3;

        module.exports = PositionError;

    });

    cordova.define("cordova-plugin-geolocation.Position", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var Coordinates = require('./Coordinates');

        var Position = function (coords, timestamp) {
            if (coords) {
                this.coords = new Coordinates(coords.latitude, coords.longitude, coords.altitude, coords.accuracy, coords.heading, coords.velocity, coords.altitudeAccuracy);
            } else {
                this.coords = new Coordinates();
            }
            this.timestamp = (timestamp !== undefined) ? timestamp : new Date().getTime();
        };

        module.exports = Position;

    });

    cordova.define("cordova-plugin-geolocation.geolocation", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var argscheck = require('cordova/argscheck'),
            utils = require('cordova/utils'),
            exec = require('cordova/exec'),
            PositionError = require('./PositionError'),
            Position = require('./Position');

        var timers = {};   // list of timers in use

// Returns default params, overrides if provided with values
        function parseParameters(options) {
            var opt = {
                maximumAge: 0,
                enableHighAccuracy: false,
                timeout: Infinity
            };

            if (options) {
                if (options.maximumAge !== undefined && !isNaN(options.maximumAge) && options.maximumAge > 0) {
                    opt.maximumAge = options.maximumAge;
                }
                if (options.enableHighAccuracy !== undefined) {
                    opt.enableHighAccuracy = options.enableHighAccuracy;
                }
                if (options.timeout !== undefined && !isNaN(options.timeout)) {
                    if (options.timeout < 0) {
                        opt.timeout = 0;
                    } else {
                        opt.timeout = options.timeout;
                    }
                }
            }

            return opt;
        }

// Returns a timeout failure, closed over a specified timeout value and error callback.
        function createTimeout(errorCallback, timeout) {
            var t = setTimeout(function () {
                clearTimeout(t);
                t = null;
                errorCallback({
                    code: PositionError.TIMEOUT,
                    message: "Position retrieval timed out."
                });
            }, timeout);
            return t;
        }

        var geolocation = {
            lastPosition: null, // reference to last known (cached) position returned
            /**
             * Asynchronously acquires the current position.
             *
             * @param {Function} successCallback    The function to call when the position data is available
             * @param {Function} errorCallback      The function to call when there is an error getting the heading position. (OPTIONAL)
             * @param {PositionOptions} options     The options for getting the position data. (OPTIONAL)
             */
            getCurrentPosition: function (successCallback, errorCallback, options) {
                argscheck.checkArgs('fFO', 'geolocation.getCurrentPosition', arguments);
                options = parseParameters(options);

                // Timer var that will fire an error callback if no position is retrieved from native
                // before the "timeout" param provided expires
                var timeoutTimer = {timer: null};

                var win = function (p) {
                    clearTimeout(timeoutTimer.timer);
                    if (!(timeoutTimer.timer)) {
                        // Timeout already happened, or native fired error callback for
                        // this geo request.
                        // Don't continue with success callback.
                        return;
                    }
                    var pos = new Position(
                        {
                            latitude: p.latitude,
                            longitude: p.longitude,
                            altitude: p.altitude,
                            accuracy: p.accuracy,
                            heading: p.heading,
                            velocity: p.velocity,
                            altitudeAccuracy: p.altitudeAccuracy
                        },
                        p.timestamp
                    );
                    geolocation.lastPosition = pos;
                    successCallback(pos);
                };
                var fail = function (e) {
                    clearTimeout(timeoutTimer.timer);
                    timeoutTimer.timer = null;
                    var err = new PositionError(e.code, e.message);
                    if (errorCallback) {
                        errorCallback(err);
                    }
                };

                // Check our cached position, if its timestamp difference with current time is less than the maximumAge, then just
                // fire the success callback with the cached position.
                if (geolocation.lastPosition && options.maximumAge && (((new Date()).getTime() - geolocation.lastPosition.timestamp) <= options.maximumAge)) {
                    successCallback(geolocation.lastPosition);
                    // If the cached position check failed and the timeout was set to 0, error out with a TIMEOUT error object.
                } else if (options.timeout === 0) {
                    fail({
                        code: PositionError.TIMEOUT,
                        message: "timeout value in PositionOptions set to 0 and no cached Position object available, or cached Position object's age exceeds provided PositionOptions' maximumAge parameter."
                    });
                    // Otherwise we have to call into native to retrieve a position.
                } else {
                    if (options.timeout !== Infinity) {
                        // If the timeout value was not set to Infinity (default), then
                        // set up a timeout function that will fire the error callback
                        // if no successful position was retrieved before timeout expired.
                        timeoutTimer.timer = createTimeout(fail, options.timeout);
                    } else {
                        // This is here so the check in the win function doesn't mess stuff up
                        // may seem weird but this guarantees timeoutTimer is
                        // always truthy before we call into native
                        timeoutTimer.timer = true;
                    }
                    exec(win, fail, "Geolocation", "getLocation", [options.enableHighAccuracy, options.maximumAge]);
                }
                return timeoutTimer;
            },
            /**
             * Asynchronously watches the geolocation for changes to geolocation.  When a change occurs,
             * the successCallback is called with the new location.
             *
             * @param {Function} successCallback    The function to call each time the location data is available
             * @param {Function} errorCallback      The function to call when there is an error getting the location data. (OPTIONAL)
             * @param {PositionOptions} options     The options for getting the location data such as frequency. (OPTIONAL)
             * @return String                       The watch id that must be passed to #clearWatch to stop watching.
             */
            watchPosition: function (successCallback, errorCallback, options) {
                argscheck.checkArgs('fFO', 'geolocation.getCurrentPosition', arguments);
                options = parseParameters(options);

                var id = utils.createUUID();

                // Tell device to get a position ASAP, and also retrieve a reference to the timeout timer generated in getCurrentPosition
                timers[id] = geolocation.getCurrentPosition(successCallback, errorCallback, options);

                var fail = function (e) {
                    clearTimeout(timers[id].timer);
                    var err = new PositionError(e.code, e.message);
                    if (errorCallback) {
                        errorCallback(err);
                    }
                };

                var win = function (p) {
                    clearTimeout(timers[id].timer);
                    if (options.timeout !== Infinity) {
                        timers[id].timer = createTimeout(fail, options.timeout);
                    }
                    var pos = new Position(
                        {
                            latitude: p.latitude,
                            longitude: p.longitude,
                            altitude: p.altitude,
                            accuracy: p.accuracy,
                            heading: p.heading,
                            velocity: p.velocity,
                            altitudeAccuracy: p.altitudeAccuracy
                        },
                        p.timestamp
                    );
                    geolocation.lastPosition = pos;
                    successCallback(pos);
                };

                exec(win, fail, "Geolocation", "addWatch", [id, options.enableHighAccuracy]);

                return id;
            },
            /**
             * Clears the specified heading watch.
             *
             * @param {String} id       The ID of the watch returned from #watchPosition
             */
            clearWatch: function (id) {
                if (id && timers[id] !== undefined) {
                    clearTimeout(timers[id].timer);
                    timers[id].timer = false;
                    exec(null, null, "Geolocation", "clearWatch", [id]);
                }
            }
        };

        module.exports = geolocation;

    });

    cordova.define("phonegap-plugin-barcodescanner.BarcodeScanner", function (require, exports, module) {
        /**
         * cordova is available under *either* the terms of the modified BSD license *or* the
         * MIT License (2008). See http://opensource.org/licenses/alphabetical for full text.
         *
         * Copyright (c) Matt Kane 2010
         * Copyright (c) 2011, IBM Corporation
         */


        var exec = cordova.require("cordova/exec");

        var scanInProgress = false;

        /**
         * Constructor.
         *
         * @returns {BarcodeScanner}
         */
        function BarcodeScanner() {

            /**
             * Encoding constants.
             *
             * @type Object
             */
            this.Encode = {
                TEXT_TYPE: "TEXT_TYPE",
                EMAIL_TYPE: "EMAIL_TYPE",
                PHONE_TYPE: "PHONE_TYPE",
                SMS_TYPE: "SMS_TYPE"
                //  CONTACT_TYPE: "CONTACT_TYPE",  // TODO:  not implemented, requires passing a Bundle class from Javascript to Java
                //  LOCATION_TYPE: "LOCATION_TYPE" // TODO:  not implemented, requires passing a Bundle class from Javascript to Java
            };

            /**
             * Barcode format constants, defined in ZXing library.
             *
             * @type Object
             */
            this.format = {
                "all_1D": 61918,
                "aztec": 1,
                "codabar": 2,
                "code_128": 16,
                "code_39": 4,
                "code_93": 8,
                "data_MATRIX": 32,
                "ean_13": 128,
                "ean_8": 64,
                "itf": 256,
                "maxicode": 512,
                "msi": 131072,
                "pdf_417": 1024,
                "plessey": 262144,
                "qr_CODE": 2048,
                "rss_14": 4096,
                "rss_EXPANDED": 8192,
                "upc_A": 16384,
                "upc_E": 32768,
                "upc_EAN_EXTENSION": 65536
            };
        }

        /**
         * Read code from scanner.
         *
         * @param {Function} successCallback This function will recieve a result object: {
         *        text : '12345-mock',    // The code that was scanned.
         *        format : 'FORMAT_NAME', // Code format.
         *        cancelled : true/false, // Was canceled.
         *    }
         * @param {Function} errorCallback
         * @param config
         */
        BarcodeScanner.prototype.scan = function (successCallback, errorCallback, config) {

            if (config instanceof Array) {
                // do nothing
            } else {
                if (typeof(config) === 'object') {
                    config = [config];
                } else {
                    config = [];
                }
            }

            if (errorCallback == null) {
                errorCallback = function () {
                };
            }

            if (typeof errorCallback != "function") {
                console.log("BarcodeScanner.scan failure: failure parameter not a function");
                return;
            }

            if (typeof successCallback != "function") {
                console.log("BarcodeScanner.scan failure: success callback parameter must be a function");
                return;
            }

            if (scanInProgress) {
                errorCallback('Scan is already in progress');
                return;
            }

            scanInProgress = true;

            exec(
                function (result) {
                    scanInProgress = false;
                    successCallback(result);
                },
                function (error) {
                    scanInProgress = false;
                    errorCallback(error);
                },
                'BarcodeScanner',
                'scan',
                config
            );
        };

        //-------------------------------------------------------------------
        BarcodeScanner.prototype.encode = function (type, data, successCallback, errorCallback, options) {
            if (errorCallback == null) {
                errorCallback = function () {
                };
            }

            if (typeof errorCallback != "function") {
                console.log("BarcodeScanner.encode failure: failure parameter not a function");
                return;
            }

            if (typeof successCallback != "function") {
                console.log("BarcodeScanner.encode failure: success callback parameter must be a function");
                return;
            }

            exec(successCallback, errorCallback, 'BarcodeScanner', 'encode', [
                {"type": type, "data": data, "options": options}
            ]);
        };

        var barcodeScanner = new BarcodeScanner();
        module.exports = barcodeScanner;

    });

    cordova.define("cordova-sqlcipher-adapter.SQLitePlugin", function (require, exports, module) {
        (function () {
            var DB_STATE_INIT, DB_STATE_OPEN, READ_ONLY_REGEX, SQLiteFactory, SQLitePlugin, SQLitePluginTransaction, SelfTest, argsArray, dblocations, iosLocationMap, newSQLError, nextTick, root, txLocks;

            root = this;

            READ_ONLY_REGEX = /^(\s|;)*(?:alter|create|delete|drop|insert|reindex|replace|update)/i;

            DB_STATE_INIT = "INIT";

            DB_STATE_OPEN = "OPEN";

            txLocks = {};

            newSQLError = function (error, code) {
                var sqlError;
                sqlError = error;
                if (!code) {
                    code = 0;
                }
                if (!sqlError) {
                    sqlError = new Error("a plugin had an error but provided no response");
                    sqlError.code = code;
                }
                if (typeof sqlError === "string") {
                    sqlError = new Error(error);
                    sqlError.code = code;
                }
                if (!sqlError.code && sqlError.message) {
                    sqlError.code = code;
                }
                if (!sqlError.code && !sqlError.message) {
                    sqlError = new Error("an unknown error was returned: " + JSON.stringify(sqlError));
                    sqlError.code = code;
                }
                return sqlError;
            };

            nextTick = window.setImmediate || function (fun) {
                    window.setTimeout(fun, 0);
                };


            /*
             Utility that avoids leaking the arguments object. See
             https://www.npmjs.org/package/argsarray
             */

            argsArray = function (fun) {
                return function () {
                    var args, i, len;
                    len = arguments.length;
                    if (len) {
                        args = [];
                        i = -1;
                        while (++i < len) {
                            args[i] = arguments[i];
                        }
                        return fun.call(this, args);
                    } else {
                        return fun.call(this, []);
                    }
                };
            };

            SQLitePlugin = function (openargs, openSuccess, openError) {
                var dbname;
                if (!(openargs && openargs['name'])) {
                    throw newSQLError("Cannot create a SQLitePlugin db instance without a db name");
                }
                dbname = openargs.name;
                if (typeof dbname !== 'string') {
                    throw newSQLError('sqlite plugin database name must be a string');
                }
                this.openargs = openargs;
                this.dbname = dbname;
                this.openSuccess = openSuccess;
                this.openError = openError;
                this.openSuccess || (this.openSuccess = function () {
                    console.log("DB opened: " + dbname);
                });
                this.openError || (this.openError = function (e) {
                    console.log(e.message);
                });
                this.open(this.openSuccess, this.openError);
            };

            SQLitePlugin.prototype.databaseFeatures = {
                isSQLitePluginDatabase: true
            };

            SQLitePlugin.prototype.openDBs = {};

            SQLitePlugin.prototype.addTransaction = function (t) {
                if (!txLocks[this.dbname]) {
                    txLocks[this.dbname] = {
                        queue: [],
                        inProgress: false
                    };
                }
                txLocks[this.dbname].queue.push(t);
                if (this.dbname in this.openDBs && this.openDBs[this.dbname] !== DB_STATE_INIT) {
                    this.startNextTransaction();
                } else {
                    if (this.dbname in this.openDBs) {
                        console.log('new transaction is waiting for open operation');
                    } else {
                        console.log('database is closed, new transaction is [stuck] waiting until db is opened again!');
                    }
                }
            };

            SQLitePlugin.prototype.transaction = function (fn, error, success) {
                if (!this.openDBs[this.dbname]) {
                    error(newSQLError('database not open'));
                    return;
                }
                this.addTransaction(new SQLitePluginTransaction(this, fn, error, success, true, false));
            };

            SQLitePlugin.prototype.readTransaction = function (fn, error, success) {
                if (!this.openDBs[this.dbname]) {
                    error(newSQLError('database not open'));
                    return;
                }
                this.addTransaction(new SQLitePluginTransaction(this, fn, error, success, false, true));
            };

            SQLitePlugin.prototype.startNextTransaction = function () {
                var self;
                self = this;
                nextTick((function (_this) {
                    return function () {
                        var txLock;
                        if (!(_this.dbname in _this.openDBs) || _this.openDBs[_this.dbname] !== DB_STATE_OPEN) {
                            console.log('cannot start next transaction: database not open');
                            return;
                        }
                        txLock = txLocks[self.dbname];
                        if (!txLock) {
                            console.log('cannot start next transaction: database connection is lost');
                            return;
                        } else if (txLock.queue.length > 0 && !txLock.inProgress) {
                            txLock.inProgress = true;
                            txLock.queue.shift().start();
                        }
                    };
                })(this));
            };

            SQLitePlugin.prototype.abortAllPendingTransactions = function () {
                var j, len1, ref, tx, txLock;
                txLock = txLocks[this.dbname];
                if (!!txLock && txLock.queue.length > 0) {
                    ref = txLock.queue;
                    for (j = 0, len1 = ref.length; j < len1; j++) {
                        tx = ref[j];
                        tx.abortFromQ(newSQLError('Invalid database handle'));
                    }
                    txLock.queue = [];
                    txLock.inProgress = false;
                }
            };

            SQLitePlugin.prototype.open = function (success, error) {
                var openerrorcb, opensuccesscb;
                if (this.dbname in this.openDBs) {
                    console.log('database already open: ' + this.dbname);
                    nextTick((function (_this) {
                        return function () {
                            success(_this);
                        };
                    })(this));
                } else {
                    console.log('OPEN database: ' + this.dbname);
                    opensuccesscb = (function (_this) {
                        return function () {
                            var txLock;
                            console.log('OPEN database: ' + _this.dbname + ' - OK');
                            if (!_this.openDBs[_this.dbname]) {
                                console.log('database was closed during open operation');
                            }
                            if (_this.dbname in _this.openDBs) {
                                _this.openDBs[_this.dbname] = DB_STATE_OPEN;
                            }
                            if (!!success) {
                                success(_this);
                            }
                            txLock = txLocks[_this.dbname];
                            if (!!txLock && txLock.queue.length > 0 && !txLock.inProgress) {
                                _this.startNextTransaction();
                            }
                        };
                    })(this);
                    openerrorcb = (function (_this) {
                        return function () {
                            console.log('OPEN database: ' + _this.dbname + ' FAILED, aborting any pending transactions');
                            if (!!error) {
                                error(newSQLError('Could not open database'));
                            }
                            delete _this.openDBs[_this.dbname];
                            _this.abortAllPendingTransactions();
                        };
                    })(this);
                    this.openDBs[this.dbname] = DB_STATE_INIT;
                    cordova.exec(opensuccesscb, openerrorcb, "SQLitePlugin", "open", [this.openargs]);
                }
            };

            SQLitePlugin.prototype.close = function (success, error) {
                if (this.dbname in this.openDBs) {
                    if (txLocks[this.dbname] && txLocks[this.dbname].inProgress) {
                        console.log('cannot close: transaction is in progress');
                        error(newSQLError('database cannot be closed while a transaction is in progress'));
                        return;
                    }
                    console.log('CLOSE database: ' + this.dbname);
                    delete this.openDBs[this.dbname];
                    if (txLocks[this.dbname]) {
                        console.log('closing db with transaction queue length: ' + txLocks[this.dbname].queue.length);
                    } else {
                        console.log('closing db with no transaction lock state');
                    }
                    cordova.exec(success, error, "SQLitePlugin", "close", [
                        {
                            path: this.dbname
                        }
                    ]);
                } else {
                    console.log('cannot close: database is not open');
                    if (error) {
                        nextTick(function () {
                            return error();
                        });
                    }
                }
            };

            SQLitePlugin.prototype.executeSql = function (statement, params, success, error) {
                var myerror, myfn, mysuccess;
                mysuccess = function (t, r) {
                    if (!!success) {
                        return success(r);
                    }
                };
                myerror = function (t, e) {
                    if (!!error) {
                        return error(e);
                    }
                };
                myfn = function (tx) {
                    tx.addStatement(statement, params, mysuccess, myerror);
                };
                this.addTransaction(new SQLitePluginTransaction(this, myfn, null, null, false, false));
            };

            SQLitePlugin.prototype.sqlBatch = function (sqlStatements, success, error) {
                var batchList, j, len1, myfn, st;
                if (!sqlStatements || sqlStatements.constructor !== Array) {
                    throw newSQLError('sqlBatch expects an array');
                }
                batchList = [];
                for (j = 0, len1 = sqlStatements.length; j < len1; j++) {
                    st = sqlStatements[j];
                    if (st.constructor === Array) {
                        if (st.length === 0) {
                            throw newSQLError('sqlBatch array element of zero (0) length');
                        }
                        batchList.push({
                            sql: st[0],
                            params: st.length === 0 ? [] : st[1]
                        });
                    } else {
                        batchList.push({
                            sql: st,
                            params: []
                        });
                    }
                }
                myfn = function (tx) {
                    var elem, k, len2, results;
                    results = [];
                    for (k = 0, len2 = batchList.length; k < len2; k++) {
                        elem = batchList[k];
                        results.push(tx.addStatement(elem.sql, elem.params, null, null));
                    }
                    return results;
                };
                this.addTransaction(new SQLitePluginTransaction(this, myfn, error, success, true, false));
            };

            SQLitePluginTransaction = function (db, fn, error, success, txlock, readOnly) {
                if (typeof fn !== "function") {

                    /*
                     This is consistent with the implementation in Chrome -- it
                     throws if you pass anything other than a function. This also
                     prevents us from stalling our txQueue if somebody passes a
                     false value for fn.
                     */
                    throw newSQLError("transaction expected a function");
                }
                this.db = db;
                this.fn = fn;
                this.error = error;
                this.success = success;
                this.txlock = txlock;
                this.readOnly = readOnly;
                this.executes = [];
                if (txlock) {
                    this.addStatement("BEGIN", [], null, function (tx, err) {
                        throw newSQLError("unable to begin transaction: " + err.message, err.code);
                    });
                } else {
                    this.addStatement("SELECT 1", [], null, null);
                }
            };

            SQLitePluginTransaction.prototype.start = function () {
                var err;
                try {
                    this.fn(this);
                    this.run();
                } catch (error1) {
                    err = error1;
                    txLocks[this.db.dbname].inProgress = false;
                    this.db.startNextTransaction();
                    if (this.error) {
                        this.error(newSQLError(err));
                    }
                }
            };

            SQLitePluginTransaction.prototype.executeSql = function (sql, values, success, error) {
                if (this.finalized) {
                    throw {
                        message: 'InvalidStateError: DOM Exception 11: This transaction is already finalized. Transactions are committed after its success or failure handlers are called. If you are using a Promise to handle callbacks, be aware that implementations following the A+ standard adhere to run-to-completion semantics and so Promise resolution occurs on a subsequent tick and therefore after the transaction commits.',
                        code: 11
                    };
                    return;
                }
                if (this.readOnly && READ_ONLY_REGEX.test(sql)) {
                    this.handleStatementFailure(error, {
                        message: 'invalid sql for a read-only transaction'
                    });
                    return;
                }
                this.addStatement(sql, values, success, error);
            };

            SQLitePluginTransaction.prototype.addStatement = function (sql, values, success, error) {
                var j, len1, params, sqlStatement, t, v;
                sqlStatement = typeof sql === 'string' ? sql : sql.toString();
                params = [];
                if (!!values && values.constructor === Array) {
                    for (j = 0, len1 = values.length; j < len1; j++) {
                        v = values[j];
                        t = typeof v;
                        params.push((v === null || v === void 0 ? null : t === 'number' || t === 'string' ? v : v.toString()));
                    }
                }
                this.executes.push({
                    success: success,
                    error: error,
                    sql: sqlStatement,
                    params: params
                });
            };

            SQLitePluginTransaction.prototype.handleStatementSuccess = function (handler, response) {
                var payload, rows;
                if (!handler) {
                    return;
                }
                rows = response.rows || [];
                payload = {
                    rows: {
                        item: function (i) {
                            return rows[i];
                        },
                        length: rows.length
                    },
                    rowsAffected: response.rowsAffected || 0,
                    insertId: response.insertId || void 0
                };
                handler(this, payload);
            };

            SQLitePluginTransaction.prototype.handleStatementFailure = function (handler, response) {
                if (!handler) {
                    throw newSQLError("a statement with no error handler failed: " + response.message, response.code);
                }
                if (handler(this, response) !== false) {
                    throw newSQLError("a statement error callback did not return false: " + response.message, response.code);
                }
            };

            SQLitePluginTransaction.prototype.run = function () {
                var batchExecutes, handlerFor, i, mycb, mycbmap, request, tropts, tx, txFailure, waiting;
                txFailure = null;
                tropts = [];
                batchExecutes = this.executes;
                waiting = batchExecutes.length;
                this.executes = [];
                tx = this;
                handlerFor = function (index, didSucceed) {
                    return function (response) {
                        var err;
                        if (!txFailure) {
                            try {
                                if (didSucceed) {
                                    tx.handleStatementSuccess(batchExecutes[index].success, response);
                                } else {
                                    tx.handleStatementFailure(batchExecutes[index].error, newSQLError(response));
                                }
                            } catch (error1) {
                                err = error1;
                                txFailure = newSQLError(err);
                            }
                        }
                        if (--waiting === 0) {
                            if (txFailure) {
                                tx.executes = [];
                                tx.abort(txFailure);
                            } else if (tx.executes.length > 0) {
                                tx.run();
                            } else {
                                tx.finish();
                            }
                        }
                    };
                };
                mycbmap = {};
                i = 0;
                while (i < batchExecutes.length) {
                    request = batchExecutes[i];
                    mycbmap[i] = {
                        success: handlerFor(i, true),
                        error: handlerFor(i, false)
                    };
                    tropts.push({
                        qid: null,
                        sql: request.sql,
                        params: request.params
                    });
                    i++;
                }
                mycb = function (result) {
                    var j, q, r, ref, res, resultIndex, type;
                    for (resultIndex = j = 0, ref = result.length - 1; 0 <= ref ? j <= ref : j >= ref; resultIndex = 0 <= ref ? ++j : --j) {
                        r = result[resultIndex];
                        type = r.type;
                        res = r.result;
                        q = mycbmap[resultIndex];
                        if (q) {
                            if (q[type]) {
                                q[type](res);
                            }
                        }
                    }
                };
                cordova.exec(mycb, null, "SQLitePlugin", "backgroundExecuteSqlBatch", [
                    {
                        dbargs: {
                            dbname: this.db.dbname
                        },
                        executes: tropts
                    }
                ]);
            };

            SQLitePluginTransaction.prototype.abort = function (txFailure) {
                var failed, succeeded, tx;
                if (this.finalized) {
                    return;
                }
                tx = this;
                succeeded = function (tx) {
                    txLocks[tx.db.dbname].inProgress = false;
                    tx.db.startNextTransaction();
                    if (tx.error && typeof tx.error === 'function') {
                        tx.error(txFailure);
                    }
                };
                failed = function (tx, err) {
                    txLocks[tx.db.dbname].inProgress = false;
                    tx.db.startNextTransaction();
                    if (tx.error && typeof tx.error === 'function') {
                        tx.error(newSQLError('error while trying to roll back: ' + err.message, err.code));
                    }
                };
                this.finalized = true;
                if (this.txlock) {
                    this.addStatement("ROLLBACK", [], succeeded, failed);
                    this.run();
                } else {
                    succeeded(tx);
                }
            };

            SQLitePluginTransaction.prototype.finish = function () {
                var failed, succeeded, tx;
                if (this.finalized) {
                    return;
                }
                tx = this;
                succeeded = function (tx) {
                    txLocks[tx.db.dbname].inProgress = false;
                    tx.db.startNextTransaction();
                    if (tx.success && typeof tx.success === 'function') {
                        tx.success();
                    }
                };
                failed = function (tx, err) {
                    txLocks[tx.db.dbname].inProgress = false;
                    tx.db.startNextTransaction();
                    if (tx.error && typeof tx.error === 'function') {
                        tx.error(newSQLError('error while trying to commit: ' + err.message, err.code));
                    }
                };
                this.finalized = true;
                if (this.txlock) {
                    this.addStatement("COMMIT", [], succeeded, failed);
                    this.run();
                } else {
                    succeeded(tx);
                }
            };

            SQLitePluginTransaction.prototype.abortFromQ = function (sqlerror) {
                if (this.error) {
                    this.error(sqlerror);
                }
            };

            dblocations = ["docs", "libs", "nosync"];

            iosLocationMap = {
                'default': 'nosync',
                'Documents': 'docs',
                'Library': 'libs'
            };

            SQLiteFactory = {

                /*
                 NOTE: this function should NOT be translated from Javascript
                 back to CoffeeScript by js2coffee.
                 If this function is edited in Javascript then someone will
                 have to translate it back to CoffeeScript by hand.
                 */
                openDatabase: argsArray(function (args) {
                    var dblocation, errorcb, okcb, openargs;
                    if (args.length < 1 || !args[0]) {
                        throw newSQLError('Sorry missing mandatory open arguments object in openDatabase call');
                    }
                    if (args[0].constructor === String) {
                        throw newSQLError('Sorry first openDatabase argument must be an object');
                    }
                    openargs = args[0];
                    if (!openargs.name) {
                        throw newSQLError('Database name value is missing in openDatabase call');
                    }
                    if (!openargs.iosDatabaseLocation && !openargs.location && openargs.location !== 0) {
                        throw newSQLError('Database location or iosDatabaseLocation setting is now mandatory in openDatabase call.');
                    }
                    if (!!openargs.location && !!openargs.iosDatabaseLocation) {
                        throw newSQLError('AMBIGUOUS: both location and iosDatabaseLocation settings are present in openDatabase call. Please use either setting, not both.');
                    }
                    dblocation = !!openargs.location && openargs.location === 'default' ? iosLocationMap['default'] : !!openargs.iosDatabaseLocation ? iosLocationMap[openargs.iosDatabaseLocation] : dblocations[openargs.location];
                    if (!dblocation) {
                        throw newSQLError('Valid iOS database location could not be determined in openDatabase call');
                    }
                    openargs.dblocation = dblocation;
                    if (!!openargs.createFromLocation && openargs.createFromLocation === 1) {
                        openargs.createFromResource = "1";
                    }
                    if (!!openargs.androidDatabaseImplementation && openargs.androidDatabaseImplementation === 2) {
                        openargs.androidOldDatabaseImplementation = 1;
                    }
                    if (!!openargs.androidLockWorkaround && openargs.androidLockWorkaround === 1) {
                        openargs.androidBugWorkaround = 1;
                    }
                    okcb = null;
                    errorcb = null;
                    if (args.length >= 2) {
                        okcb = args[1];
                        if (args.length > 2) {
                            errorcb = args[2];
                        }
                    }
                    return new SQLitePlugin(openargs, okcb, errorcb);
                }),
                deleteDatabase: function (first, success, error) {
                    var args, dblocation, dbname;
                    args = {};
                    if (first.constructor === String) {
                        throw newSQLError('Sorry first deleteDatabase argument must be an object');
                    } else {
                        if (!(first && first['name'])) {
                            throw new Error("Please specify db name");
                        }
                        dbname = first.name;
                        if (typeof dbname !== 'string') {
                            throw newSQLError('delete database name must be a string');
                        }
                        args.path = dbname;
                    }
                    if (!first.iosDatabaseLocation && !first.location && first.location !== 0) {
                        throw newSQLError('Database location or iosDatabaseLocation setting is now mandatory in deleteDatabase call.');
                    }
                    if (!!first.location && !!first.iosDatabaseLocation) {
                        throw newSQLError('AMBIGUOUS: both location and iosDatabaseLocation settings are present in deleteDatabase call. Please use either setting value, not both.');
                    }
                    dblocation = !!first.location && first.location === 'default' ? iosLocationMap['default'] : !!first.iosDatabaseLocation ? iosLocationMap[first.iosDatabaseLocation] : dblocations[first.location];
                    if (!dblocation) {
                        throw newSQLError('Valid iOS database location could not be determined in deleteDatabase call');
                    }
                    args.dblocation = dblocation;
                    delete SQLitePlugin.prototype.openDBs[args.path];
                    return cordova.exec(success, error, "SQLitePlugin", "delete", [args]);
                }
            };

            SelfTest = {
                DBNAME: '___$$$___litehelpers___$$$___test___$$$___.db',
                start: function (successcb, errorcb) {
                    SQLiteFactory.deleteDatabase({
                        name: SelfTest.DBNAME,
                        location: 'default'
                    }, (function () {
                        return SelfTest.start2(successcb, errorcb);
                    }), (function () {
                        return SelfTest.start2(successcb, errorcb);
                    }));
                },
                start2: function (successcb, errorcb) {
                    SQLiteFactory.openDatabase({
                        name: SelfTest.DBNAME,
                        location: 'default'
                    }, function (db) {
                        var check1;
                        check1 = false;
                        return db.transaction(function (tx) {
                            return tx.executeSql('SELECT UPPER("Test") AS upperText', [], function (ignored, resutSet) {
                                if (!resutSet.rows) {
                                    return SelfTest.finishWithError(errorcb, 'Missing resutSet.rows');
                                }
                                if (!resutSet.rows.length) {
                                    return SelfTest.finishWithError(errorcb, 'Missing resutSet.rows.length');
                                }
                                if (resutSet.rows.length !== 1) {
                                    return SelfTest.finishWithError(errorcb, "Incorrect resutSet.rows.length value: " + resutSet.rows.length + " (expected: 1)");
                                }
                                if (!resutSet.rows.item(0).upperText) {
                                    return SelfTest.finishWithError(errorcb, 'Missing resutSet.rows.item(0).upperText');
                                }
                                if (resutSet.rows.item(0).upperText !== 'TEST') {
                                    return SelfTest.finishWithError(errorcb, "Incorrect resutSet.rows.item(0).upperText value: " + (resutSet.rows.item(0).data) + " (expected: 'TEST')");
                                }
                                check1 = true;
                            }, function (sql_err) {
                                SelfTest.finishWithError(errorcb, "SQL error: " + sql_err);
                            });
                        }, function (tx_err) {
                            SelfTest.finishWithError(errorcb, "TRANSACTION error: " + tx_err);
                        }, function () {
                            if (!check1) {
                                return SelfTest.finishWithError(errorcb, 'Did not get expected upperText result data');
                            }
                            delete db.openDBs[SelfTest.DBNAME];
                            delete txLocks[SelfTest.DBNAME];
                            SelfTest.start3(successcb, errorcb);
                        });
                    }, function (open_err) {
                        return SelfTest.finishWithError(errorcb, "Open database error: " + open_err);
                    });
                },
                start3: function (successcb, errorcb) {
                    SQLiteFactory.openDatabase({
                        name: SelfTest.DBNAME,
                        location: 'default'
                    }, function (db) {
                        return db.sqlBatch(['CREATE TABLE TestTable(id integer primary key autoincrement unique, data);', ['INSERT INTO TestTable (data) VALUES (?);', ['test-value']]], function () {
                            var firstid;
                            firstid = -1;
                            return db.executeSql('SELECT id, data FROM TestTable', [], function (resutSet) {
                                if (!resutSet.rows) {
                                    SelfTest.finishWithError(errorcb, 'Missing resutSet.rows');
                                    return;
                                }
                                if (!resutSet.rows.length) {
                                    SelfTest.finishWithError(errorcb, 'Missing resutSet.rows.length');
                                    return;
                                }
                                if (resutSet.rows.length !== 1) {
                                    SelfTest.finishWithError(errorcb, "Incorrect resutSet.rows.length value: " + resutSet.rows.length + " (expected: 1)");
                                    return;
                                }
                                if (resutSet.rows.item(0).id === void 0) {
                                    SelfTest.finishWithError(errorcb, 'Missing resutSet.rows.item(0).id');
                                    return;
                                }
                                firstid = resutSet.rows.item(0).id;
                                if (!resutSet.rows.item(0).data) {
                                    SelfTest.finishWithError(errorcb, 'Missing resutSet.rows.item(0).data');
                                    return;
                                }
                                if (resutSet.rows.item(0).data !== 'test-value') {
                                    SelfTest.finishWithError(errorcb, "Incorrect resutSet.rows.item(0).data value: " + (resutSet.rows.item(0).data) + " (expected: 'test-value')");
                                    return;
                                }
                                return db.transaction(function (tx) {
                                    return tx.executeSql('UPDATE TestTable SET data = ?', ['new-value']);
                                }, function (tx_err) {
                                    return SelfTest.finishWithError(errorcb, "UPDATE transaction error: " + tx_err);
                                }, function () {
                                    var readTransactionFinished;
                                    readTransactionFinished = false;
                                    return db.readTransaction(function (tx2) {
                                        return tx2.executeSql('SELECT id, data FROM TestTable', [], function (ignored, resutSet2) {
                                            if (!resutSet2.rows) {
                                                throw newSQLError('Missing resutSet2.rows');
                                            }
                                            if (!resutSet2.rows.length) {
                                                throw newSQLError('Missing resutSet2.rows.length');
                                            }
                                            if (resutSet2.rows.length !== 1) {
                                                throw newSQLError("Incorrect resutSet2.rows.length value: " + resutSet2.rows.length + " (expected: 1)");
                                            }
                                            if (!resutSet2.rows.item(0).id) {
                                                throw newSQLError('Missing resutSet2.rows.item(0).id');
                                            }
                                            if (resutSet2.rows.item(0).id !== firstid) {
                                                throw newSQLError("resutSet2.rows.item(0).id value " + (resutSet2.rows.item(0).id) + " does not match previous primary key id value (" + firstid + ")");
                                            }
                                            if (!resutSet2.rows.item(0).data) {
                                                throw newSQLError('Missing resutSet2.rows.item(0).data');
                                            }
                                            if (resutSet2.rows.item(0).data !== 'new-value') {
                                                throw newSQLError("Incorrect resutSet2.rows.item(0).data value: " + (resutSet2.rows.item(0).data) + " (expected: 'test-value')");
                                            }
                                            return readTransactionFinished = true;
                                        });
                                    }, function (tx2_err) {
                                        return SelfTest.finishWithError(errorcb, "readTransaction error: " + tx2_err);
                                    }, function () {
                                        if (!readTransactionFinished) {
                                            SelfTest.finishWithError(errorcb, 'readTransaction did not finish');
                                            return;
                                        }
                                        return db.transaction(function (tx3) {
                                            tx3.executeSql('DELETE FROM TestTable');
                                            return tx3.executeSql('INSERT INTO TestTable (data) VALUES(?)', [123]);
                                        }, function (tx3_err) {
                                            return SelfTest.finishWithError(errorcb, "DELETE transaction error: " + tx3_err);
                                        }, function () {
                                            var secondReadTransactionFinished;
                                            secondReadTransactionFinished = false;
                                            return db.readTransaction(function (tx4) {
                                                return tx4.executeSql('SELECT id, data FROM TestTable', [], function (ignored, resutSet3) {
                                                    if (!resutSet3.rows) {
                                                        throw newSQLError('Missing resutSet3.rows');
                                                    }
                                                    if (!resutSet3.rows.length) {
                                                        throw newSQLError('Missing resutSet3.rows.length');
                                                    }
                                                    if (resutSet3.rows.length !== 1) {
                                                        throw newSQLError("Incorrect resutSet3.rows.length value: " + resutSet3.rows.length + " (expected: 1)");
                                                    }
                                                    if (!resutSet3.rows.item(0).id) {
                                                        throw newSQLError('Missing resutSet3.rows.item(0).id');
                                                    }
                                                    if (resutSet3.rows.item(0).id === firstid) {
                                                        throw newSQLError("resutSet3.rows.item(0).id value " + (resutSet3.rows.item(0).id) + " incorrectly matches previous unique key id value value (" + firstid + ")");
                                                    }
                                                    if (!resutSet3.rows.item(0).data) {
                                                        throw newSQLError('Missing resutSet3.rows.item(0).data');
                                                    }
                                                    if (resutSet3.rows.item(0).data !== 123) {
                                                        throw newSQLError("Incorrect resutSet3.rows.item(0).data value: " + (resutSet3.rows.item(0).data) + " (expected 123)");
                                                    }
                                                    return secondReadTransactionFinished = true;
                                                });
                                            }, function (tx4_err) {
                                                return SelfTest.finishWithError(errorcb, "second readTransaction error: " + tx4_err);
                                            }, function () {
                                                if (!secondReadTransactionFinished) {
                                                    SelfTest.finishWithError(errorcb, 'second readTransaction did not finish');
                                                    return;
                                                }
                                                return db.close(function () {
                                                    return SQLiteFactory.deleteDatabase({
                                                        name: SelfTest.DBNAME,
                                                        location: 'default'
                                                    }, successcb, function (cleanup_err) {
                                                        return SelfTest.finishWithError(errorcb, "Cleanup error: " + cleanup_err);
                                                    });
                                                }, function (close_err) {
                                                    return SelfTest.finishWithError(errorcb, "close error: " + close_err);
                                                });
                                            });
                                        });
                                    });
                                });
                            }, function (select_err) {
                                return SelfTest.finishWithError(errorcb, "SELECT error: " + select_err);
                            });
                        }, function (batch_err) {
                            return SelfTest.finishWithError(errorcb, "sql batch error: " + batch_err);
                        });
                    }, function (open_err) {
                        return SelfTest.finishWithError(errorcb, "Open database error: " + open_err);
                    });
                },
                finishWithError: function (errorcb, message) {
                    SQLiteFactory.deleteDatabase({
                        name: SelfTest.DBNAME,
                        location: 'default'
                    }, function () {
                        return errorcb(newSQLError(message));
                    }, function (err2) {
                        return errorcb(newSQLError("Cleanup error: " + err2 + " for error: " + message));
                    });
                }
            };

            root.sqlitePlugin = {
                sqliteFeatures: {
                    isSQLitePlugin: true,
                    version: '3.0'
                },
                echoTest: function (okcb, errorcb) {
                    var error, ok;
                    ok = function (s) {
                        if (s === 'test-string') {
                            return okcb();
                        } else {
                            return errorcb("Mismatch: got: '" + s + "' expected 'test-string'");
                        }
                    };
                    error = function (e) {
                        return errorcb(e);
                    };
                    return cordova.exec(ok, error, "SQLitePlugin", "echoStringValue", [
                        {
                            value: 'test-string'
                        }
                    ]);
                },
                selfTest: SelfTest.start,
                openDatabase: SQLiteFactory.openDatabase,
                deleteDatabase: SQLiteFactory.deleteDatabase
            };

        }).call(this);

    });

    cordova.define("cordova-plugin-contacts.contacts", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var argscheck = require('cordova/argscheck'),
            exec = require('cordova/exec'),
            ContactError = require('./ContactError'),
            Contact = require('./Contact'),
            fieldType = require('./ContactFieldType'),
            convertUtils = require('./convertUtils');

        /**
         * Represents a group of Contacts.
         * @constructor
         */
        var contacts = {
            fieldType: fieldType,
            /**
             * Returns an array of Contacts matching the search criteria.
             * @param fields that should be searched
             * @param successCB success callback
             * @param errorCB error callback
             * @param {ContactFindOptions} options that can be applied to contact searching
             * @return array of Contacts matching search criteria
             */
            find: function (fields, successCB, errorCB, options) {
                argscheck.checkArgs('afFO', 'contacts.find', arguments);
                if (!fields.length) {
                    if (errorCB) {
                        errorCB(new ContactError(ContactError.INVALID_ARGUMENT_ERROR));
                    }
                } else {
                    // missing 'options' param means return all contacts
                    options = options || {filter: '', multiple: true};
                    var win = function (result) {
                        var cs = [];
                        for (var i = 0, l = result.length; i < l; i++) {
                            cs.push(convertUtils.toCordovaFormat(contacts.create(result[i])));
                        }
                        successCB(cs);
                    };
                    exec(win, errorCB, "Contacts", "search", [fields, options]);
                }
            },

            /**
             * This function picks contact from phone using contact picker UI
             * @returns new Contact object
             */
            pickContact: function (successCB, errorCB) {

                argscheck.checkArgs('fF', 'contacts.pick', arguments);

                var win = function (result) {
                    // if Contacts.pickContact return instance of Contact object
                    // don't create new Contact object, use current
                    var contact = result instanceof Contact ? result : contacts.create(result);
                    successCB(convertUtils.toCordovaFormat(contact));
                };
                exec(win, errorCB, "Contacts", "pickContact", []);
            },

            /**
             * This function creates a new contact, but it does not persist the contact
             * to device storage. To persist the contact to device storage, invoke
             * contact.save().
             * @param properties an object whose properties will be examined to create a new Contact
             * @returns new Contact object
             */
            create: function (properties) {
                argscheck.checkArgs('O', 'contacts.create', arguments);
                var contact = new Contact();
                for (var i in properties) {
                    if (typeof contact[i] !== 'undefined' && properties.hasOwnProperty(i)) {
                        contact[i] = properties[i];
                    }
                }
                return contact;
            }
        };

        module.exports = contacts;

    });

    cordova.define("cordova-plugin-contacts.Contact", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var argscheck = require('cordova/argscheck'),
            exec = require('cordova/exec'),
            ContactError = require('./ContactError'),
            utils = require('cordova/utils'),
            convertUtils = require('./convertUtils');

        /**
         * Contains information about a single contact.
         * @constructor
         * @param {DOMString} id unique identifier
         * @param {DOMString} displayName
         * @param {ContactName} name
         * @param {DOMString} nickname
         * @param {Array.<ContactField>} phoneNumbers array of phone numbers
         * @param {Array.<ContactField>} emails array of email addresses
         * @param {Array.<ContactAddress>} addresses array of addresses
         * @param {Array.<ContactField>} ims instant messaging user ids
         * @param {Array.<ContactOrganization>} organizations
         * @param {DOMString} birthday contact's birthday
         * @param {DOMString} note user notes about contact
         * @param {Array.<ContactField>} photos
         * @param {Array.<ContactField>} categories
         * @param {Array.<ContactField>} urls contact's web sites
         */
        var Contact = function (id, displayName, name, nickname, phoneNumbers, emails, addresses,
                                ims, organizations, birthday, note, photos, categories, urls) {
            this.id = id || null;
            this.rawId = null;
            this.displayName = displayName || null;
            this.name = name || null; // ContactName
            this.nickname = nickname || null;
            this.phoneNumbers = phoneNumbers || null; // ContactField[]
            this.emails = emails || null; // ContactField[]
            this.addresses = addresses || null; // ContactAddress[]
            this.ims = ims || null; // ContactField[]
            this.organizations = organizations || null; // ContactOrganization[]
            this.birthday = birthday || null;
            this.note = note || null;
            this.photos = photos || null; // ContactField[]
            this.categories = categories || null; // ContactField[]
            this.urls = urls || null; // ContactField[]
        };

        /**
         * Removes contact from device storage.
         * @param successCB success callback
         * @param errorCB error callback
         */
        Contact.prototype.remove = function (successCB, errorCB) {
            argscheck.checkArgs('FF', 'Contact.remove', arguments);
            var fail = errorCB && function (code) {
                    errorCB(new ContactError(code));
                };
            if (this.id === null) {
                fail(ContactError.UNKNOWN_ERROR);
            }
            else {
                exec(successCB, fail, "Contacts", "remove", [this.id]);
            }
        };

        /**
         * Creates a deep copy of this Contact.
         * With the contact ID set to null.
         * @return copy of this Contact
         */
        Contact.prototype.clone = function () {
            var clonedContact = utils.clone(this);
            clonedContact.id = null;
            clonedContact.rawId = null;

            function nullIds(arr) {
                if (arr) {
                    for (var i = 0; i < arr.length; ++i) {
                        arr[i].id = null;
                    }
                }
            }

            // Loop through and clear out any id's in phones, emails, etc.
            nullIds(clonedContact.phoneNumbers);
            nullIds(clonedContact.emails);
            nullIds(clonedContact.addresses);
            nullIds(clonedContact.ims);
            nullIds(clonedContact.organizations);
            nullIds(clonedContact.categories);
            nullIds(clonedContact.photos);
            nullIds(clonedContact.urls);
            return clonedContact;
        };

        /**
         * Persists contact to device storage.
         * @param successCB success callback
         * @param errorCB error callback
         */
        Contact.prototype.save = function (successCB, errorCB) {
            argscheck.checkArgs('FFO', 'Contact.save', arguments);
            var fail = errorCB && function (code) {
                    errorCB(new ContactError(code));
                };
            var success = function (result) {
                if (result) {
                    if (successCB) {
                        var fullContact = require('./contacts').create(result);
                        successCB(convertUtils.toCordovaFormat(fullContact));
                    }
                }
                else {
                    // no Entry object returned
                    fail(ContactError.UNKNOWN_ERROR);
                }
            };
            var dupContact = convertUtils.toNativeFormat(utils.clone(this));
            exec(success, fail, "Contacts", "save", [dupContact]);
        };


        module.exports = Contact;

    });

    cordova.define("cordova-plugin-contacts.convertUtils", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var utils = require('cordova/utils');

        module.exports = {
            /**
             * Converts primitives into Complex Object
             * Currently only used for Date fields
             */
            toCordovaFormat: function (contact) {
                var value = contact.birthday;
                if (value !== null) {
                    try {
                        contact.birthday = new Date(parseFloat(value));

                        //we might get 'Invalid Date' which does not throw an error
                        //and is an instance of Date.
                        if (isNaN(contact.birthday.getTime())) {
                            contact.birthday = null;
                        }

                    } catch (exception) {
                        console.log("Cordova Contact toCordovaFormat error: exception creating date.");
                    }
                }
                return contact;
            },

            /**
             * Converts Complex objects into primitives
             * Only conversion at present is for Dates.
             **/
            toNativeFormat: function (contact) {
                var value = contact.birthday;
                if (value !== null) {
                    // try to make it a Date object if it is not already
                    if (!utils.isDate(value)) {
                        try {
                            value = new Date(value);
                        } catch (exception) {
                            value = null;
                        }
                    }
                    if (utils.isDate(value)) {
                        value = value.valueOf(); // convert to milliseconds
                    }
                    contact.birthday = value;
                }
                return contact;
            }
        };

    });

    cordova.define("cordova-plugin-contacts.ContactAddress", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * Contact address.
         * @constructor
         * @param {DOMString} id unique identifier, should only be set by native code
         * @param formatted // NOTE: not a W3C standard
         * @param streetAddress
         * @param locality
         * @param region
         * @param postalCode
         * @param country
         */

        var ContactAddress = function (pref, type, formatted, streetAddress, locality, region, postalCode, country) {
            this.id = null;
            this.pref = (typeof pref != 'undefined' ? pref : false);
            this.type = type || null;
            this.formatted = formatted || null;
            this.streetAddress = streetAddress || null;
            this.locality = locality || null;
            this.region = region || null;
            this.postalCode = postalCode || null;
            this.country = country || null;
        };

        module.exports = ContactAddress;

    });

    cordova.define("cordova-plugin-contacts.ContactError", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         *  ContactError.
         *  An error code assigned by an implementation when an error has occurred
         * @constructor
         */
        var ContactError = function (err) {
            this.code = (typeof err != 'undefined' ? err : null);
        };

        /**
         * Error codes
         */
        ContactError.UNKNOWN_ERROR = 0;
        ContactError.INVALID_ARGUMENT_ERROR = 1;
        ContactError.TIMEOUT_ERROR = 2;
        ContactError.PENDING_OPERATION_ERROR = 3;
        ContactError.IO_ERROR = 4;
        ContactError.NOT_SUPPORTED_ERROR = 5;
        ContactError.OPERATION_CANCELLED_ERROR = 6;
        ContactError.PERMISSION_DENIED_ERROR = 20;

        module.exports = ContactError;

    });

    cordova.define("cordova-plugin-contacts.ContactField", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * Generic contact field.
         * @constructor
         * @param {DOMString} id unique identifier, should only be set by native code // NOTE: not a W3C standard
         * @param type
         * @param value
         * @param pref
         */
        var ContactField = function (type, value, pref) {
            this.id = null;
            this.type = (type && type.toString()) || null;
            this.value = (value && value.toString()) || null;
            this.pref = (typeof pref != 'undefined' ? pref : false);
        };

        module.exports = ContactField;

    });

    cordova.define("cordova-plugin-contacts.ContactFindOptions", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * ContactFindOptions.
         * @constructor
         * @param filter used to match contacts against
         * @param multiple boolean used to determine if more than one contact should be returned
         * @param desiredFields
         * @param hasPhoneNumber boolean used to filter the search and only return contacts that have a phone number informed
         */

        var ContactFindOptions = function (filter, multiple, desiredFields, hasPhoneNumber) {
            this.filter = filter || '';
            this.multiple = (typeof multiple != 'undefined' ? multiple : false);
            this.desiredFields = typeof desiredFields != 'undefined' ? desiredFields : [];
            this.hasPhoneNumber = typeof hasPhoneNumber != 'undefined' ? hasPhoneNumber : false;
        };

        module.exports = ContactFindOptions;

    });

    cordova.define("cordova-plugin-contacts.ContactName", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * Contact name.
         * @constructor
         * @param formatted // NOTE: not part of W3C standard
         * @param familyName
         * @param givenName
         * @param middle
         * @param prefix
         * @param suffix
         */
        var ContactName = function (formatted, familyName, givenName, middle, prefix, suffix) {
            this.formatted = formatted || null;
            this.familyName = familyName || null;
            this.givenName = givenName || null;
            this.middleName = middle || null;
            this.honorificPrefix = prefix || null;
            this.honorificSuffix = suffix || null;
        };

        module.exports = ContactName;

    });

    cordova.define("cordova-plugin-contacts.ContactOrganization", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * Contact organization.
         * @constructor
         * @param pref
         * @param type
         * @param name
         * @param dept
         * @param title
         */

        var ContactOrganization = function (pref, type, name, dept, title) {
            this.id = null;
            this.pref = (typeof pref != 'undefined' ? pref : false);
            this.type = type || null;
            this.name = name || null;
            this.department = dept || null;
            this.title = title || null;
        };

        module.exports = ContactOrganization;

    });

    cordova.define("cordova-plugin-contacts.ContactFieldType", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        // Possible field names for various platforms.
        // Some field names are platform specific

        var fieldType = {
            addresses: "addresses",
            birthday: "birthday",
            categories: "categories",
            country: "country",
            department: "department",
            displayName: "displayName",
            emails: "emails",
            familyName: "familyName",
            formatted: "formatted",
            givenName: "givenName",
            honorificPrefix: "honorificPrefix",
            honorificSuffix: "honorificSuffix",
            id: "id",
            ims: "ims",
            locality: "locality",
            middleName: "middleName",
            name: "name",
            nickname: "nickname",
            note: "note",
            organizations: "organizations",
            phoneNumbers: "phoneNumbers",
            photos: "photos",
            postalCode: "postalCode",
            region: "region",
            streetAddress: "streetAddress",
            title: "title",
            urls: "urls"
        };

        module.exports = fieldType;

    });

    cordova.define("cordova-plugin-contacts.contacts-ios", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var exec = require('cordova/exec');

        /**
         * Provides iOS enhanced contacts API.
         */
        module.exports = {
            newContactUI: function (successCallback) {
                /*
                 *    Create a contact using the iOS Contact Picker UI
                 *    NOT part of W3C spec so no official documentation
                 *
                 * returns:  the id of the created contact as param to successCallback
                 */
                exec(successCallback, null, "Contacts", "newContact", []);
            },
            chooseContact: function (successCallback, options) {
                /*
                 *    Select a contact using the iOS Contact Picker UI
                 *    NOT part of W3C spec so no official documentation
                 *
                 *    @param errorCB error callback
                 *    @param options object
                 *    allowsEditing: boolean AS STRING
                 *        "true" to allow editing the contact
                 *        "false" (default) display contact
                 *      fields: array of fields to return in contact object (see ContactOptions.fields)
                 *
                 *    @returns
                 *        id of contact selected
                 *        ContactObject
                 *            if no fields provided contact contains just id information
                 *            if fields provided contact object contains information for the specified fields
                 *
                 */
                var win = function (result) {
                    var fullContact = require('./contacts').create(result);
                    successCallback(fullContact.id, fullContact);
                };
                exec(win, null, "Contacts", "chooseContact", [options]);
            }
        };

    });

    cordova.define("cordova-plugin-contacts.Contact-iOS", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var exec = require('cordova/exec'),
            ContactError = require('./ContactError');

        /**
         * Provides iOS Contact.display API.
         */
        module.exports = {
            display: function (errorCB, options) {
                /*
                 *    Display a contact using the iOS Contact Picker UI
                 *    NOT part of W3C spec so no official documentation
                 *
                 *    @param errorCB error callback
                 *    @param options object
                 *    allowsEditing: boolean AS STRING
                 *        "true" to allow editing the contact
                 *        "false" (default) display contact
                 */

                if (this.id === null) {
                    if (typeof errorCB === "function") {
                        var errorObj = new ContactError(ContactError.UNKNOWN_ERROR);
                        errorCB(errorObj);
                    }
                }
                else {
                    exec(null, errorCB, "Contacts", "displayContact", [this.id, options]);
                }
            }
        };

    });

    cordova.define("cordova-plugin-network-information.network", function (require, exports, module) {
        /*
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var exec = require('cordova/exec'),
            cordova = require('cordova'),
            channel = require('cordova/channel'),
            utils = require('cordova/utils');

// Link the onLine property with the Cordova-supplied network info.
// This works because we clobber the navigator object with our own
// object in bootstrap.js.
// Browser platform do not need to define this property, because
// it is already supported by modern browsers
        if (cordova.platformId !== 'browser' && typeof navigator != 'undefined') {
            utils.defineGetter(navigator, 'onLine', function () {
                return this.connection.type != 'none';
            });
        }

        function NetworkConnection() {
            this.type = 'unknown';
        }

        /**
         * Get connection info
         *
         * @param {Function} successCallback The function to call when the Connection data is available
         * @param {Function} errorCallback The function to call when there is an error getting the Connection data. (OPTIONAL)
         */
        NetworkConnection.prototype.getInfo = function (successCallback, errorCallback) {
            exec(successCallback, errorCallback, "NetworkStatus", "getConnectionInfo", []);
        };

        var me = new NetworkConnection();
        var timerId = null;
        var timeout = 500;

        channel.createSticky('onCordovaConnectionReady');
        channel.waitForInitialization('onCordovaConnectionReady');

        channel.onCordovaReady.subscribe(function () {
            me.getInfo(function (info) {
                    me.type = info;
                    if (info === "none") {
                        // set a timer if still offline at the end of timer send the offline event
                        timerId = setTimeout(function () {
                            cordova.fireDocumentEvent("offline");
                            timerId = null;
                        }, timeout);
                    } else {
                        // If there is a current offline event pending clear it
                        if (timerId !== null) {
                            clearTimeout(timerId);
                            timerId = null;
                        }
                        cordova.fireDocumentEvent("online");
                    }

                    // should only fire this once
                    if (channel.onCordovaConnectionReady.state !== 2) {
                        channel.onCordovaConnectionReady.fire();
                    }
                },
                function (e) {
                    // If we can't get the network info we should still tell Cordova
                    // to fire the deviceready event.
                    if (channel.onCordovaConnectionReady.state !== 2) {
                        channel.onCordovaConnectionReady.fire();
                    }
                    console.log("Error initializing Network Connection: " + e);
                });
        });

        module.exports = me;

    });

    cordova.define("cordova-plugin-network-information.Connection", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * Network status
         */
        module.exports = {
            UNKNOWN: "unknown",
            ETHERNET: "ethernet",
            WIFI: "wifi",
            CELL_2G: "2g",
            CELL_3G: "3g",
            CELL_4G: "4g",
            CELL: "cellular",
            NONE: "none"
        };

    });

    cordova.define("cordova-plugin-device-motion.Acceleration", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var Acceleration = function (x, y, z, timestamp) {
            this.x = x;
            this.y = y;
            this.z = z;
            this.timestamp = timestamp || (new Date()).getTime();
        };

        module.exports = Acceleration;

    });

    cordova.define("cordova-plugin-device-motion.accelerometer", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * This class provides access to device accelerometer data.
         * @constructor
         */
        var argscheck = require('cordova/argscheck'),
            utils = require("cordova/utils"),
            exec = require("cordova/exec"),
            Acceleration = require('./Acceleration');

// Is the accel sensor running?
        var running = false;

// Keeps reference to watchAcceleration calls.
        var timers = {};

// Array of listeners; used to keep track of when we should call start and stop.
        var listeners = [];

// Last returned acceleration object from native
        var accel = null;

// Timer used when faking up devicemotion events
        var eventTimerId = null;

// Tells native to start.
        function start() {
            exec(function (a) {
                var tempListeners = listeners.slice(0);
                accel = new Acceleration(a.x, a.y, a.z, a.timestamp);
                for (var i = 0, l = tempListeners.length; i < l; i++) {
                    tempListeners[i].win(accel);
                }
            }, function (e) {
                var tempListeners = listeners.slice(0);
                for (var i = 0, l = tempListeners.length; i < l; i++) {
                    tempListeners[i].fail(e);
                }
            }, "Accelerometer", "start", []);
            running = true;
        }

// Tells native to stop.
        function stop() {
            exec(null, null, "Accelerometer", "stop", []);
            accel = null;
            running = false;
        }

// Adds a callback pair to the listeners array
        function createCallbackPair(win, fail) {
            return {win: win, fail: fail};
        }

// Removes a win/fail listener pair from the listeners array
        function removeListeners(l) {
            var idx = listeners.indexOf(l);
            if (idx > -1) {
                listeners.splice(idx, 1);
                if (listeners.length === 0) {
                    stop();
                }
            }
        }

        var accelerometer = {
            /**
             * Asynchronously acquires the current acceleration.
             *
             * @param {Function} successCallback    The function to call when the acceleration data is available
             * @param {Function} errorCallback      The function to call when there is an error getting the acceleration data. (OPTIONAL)
             * @param {AccelerationOptions} options The options for getting the accelerometer data such as timeout. (OPTIONAL)
             */
            getCurrentAcceleration: function (successCallback, errorCallback, options) {
                argscheck.checkArgs('fFO', 'accelerometer.getCurrentAcceleration', arguments);

                if (cordova.platformId === "windowsphone") {
                    exec(function (a) {
                        accel = new Acceleration(a.x, a.y, a.z, a.timestamp);
                        successCallback(accel);
                    }, function (e) {
                        errorCallback(e);
                    }, "Accelerometer", "getCurrentAcceleration", []);

                    return;
                }

                var p;
                var win = function (a) {
                    removeListeners(p);
                    successCallback(a);
                };
                var fail = function (e) {
                    removeListeners(p);
                    if (errorCallback) {
                        errorCallback(e);
                    }
                };

                p = createCallbackPair(win, fail);
                listeners.push(p);

                if (!running) {
                    start();
                }
            },

            /**
             * Asynchronously acquires the acceleration repeatedly at a given interval.
             *
             * @param {Function} successCallback    The function to call each time the acceleration data is available
             * @param {Function} errorCallback      The function to call when there is an error getting the acceleration data. (OPTIONAL)
             * @param {AccelerationOptions} options The options for getting the accelerometer data such as timeout. (OPTIONAL)
             * @return String                       The watch id that must be passed to #clearWatch to stop watching.
             */
            watchAcceleration: function (successCallback, errorCallback, options) {
                argscheck.checkArgs('fFO', 'accelerometer.watchAcceleration', arguments);
                // Default interval (10 sec)
                var frequency = (options && options.frequency && typeof options.frequency == 'number') ? options.frequency : 10000;

                // Keep reference to watch id, and report accel readings as often as defined in frequency
                var id = utils.createUUID();

                var p = createCallbackPair(function () {
                }, function (e) {
                    removeListeners(p);
                    if (errorCallback) {
                        errorCallback(e);
                    }
                });
                listeners.push(p);

                timers[id] = {
                    timer: window.setInterval(function () {
                        if (accel) {
                            successCallback(accel);
                        }
                    }, frequency),
                    listeners: p
                };

                if (running) {
                    // If we're already running then immediately invoke the success callback
                    // but only if we have retrieved a value, sample code does not check for null ...
                    if (accel) {
                        successCallback(accel);
                    }
                } else {
                    start();
                }

                if (cordova.platformId === "browser" && !eventTimerId) {
                    // Start firing devicemotion events if we haven't already
                    var devicemotionEvent = new Event('devicemotion');
                    eventTimerId = window.setInterval(function () {
                        window.dispatchEvent(devicemotionEvent);
                    }, 200);
                }

                return id;
            },

            /**
             * Clears the specified accelerometer watch.
             *
             * @param {String} id       The id of the watch returned from #watchAcceleration.
             */
            clearWatch: function (id) {
                // Stop javascript timer & remove from timer list
                if (id && timers[id]) {
                    window.clearInterval(timers[id].timer);
                    removeListeners(timers[id].listeners);
                    delete timers[id];

                    if (eventTimerId && Object.keys(timers).length === 0) {
                        // No more watchers, so stop firing 'devicemotion' events
                        window.clearInterval(eventTimerId);
                        eventTimerId = null;
                    }
                }
            }
        };
        module.exports = accelerometer;

    });

    cordova.define("cordova-plugin-device-orientation.CompassError", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         *  CompassError.
         *  An error code assigned by an implementation when an error has occurred
         * @constructor
         */
        var CompassError = function (err) {
            this.code = (err !== undefined ? err : null);
        };

        CompassError.COMPASS_INTERNAL_ERR = 0;
        CompassError.COMPASS_NOT_SUPPORTED = 20;

        module.exports = CompassError;

    });

    cordova.define("cordova-plugin-device-orientation.CompassHeading", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var CompassHeading = function (magneticHeading, trueHeading, headingAccuracy, timestamp) {
            this.magneticHeading = magneticHeading;
            this.trueHeading = trueHeading;
            this.headingAccuracy = headingAccuracy;
            this.timestamp = timestamp || new Date().getTime();
        };

        module.exports = CompassHeading;

    });

    cordova.define("cordova-plugin-device-orientation.compass", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var argscheck = require('cordova/argscheck'),
            exec = require('cordova/exec'),
            utils = require('cordova/utils'),
            CompassHeading = require('./CompassHeading'),
            CompassError = require('./CompassError'),

            timers = {},
            eventTimerId = null,
            compass = {
                /**
                 * Asynchronously acquires the current heading.
                 * @param {Function} successCallback The function to call when the heading
                 * data is available
                 * @param {Function} errorCallback The function to call when there is an error
                 * getting the heading data.
                 * @param {CompassOptions} options The options for getting the heading data (not used).
                 */
                getCurrentHeading: function (successCallback, errorCallback, options) {
                    argscheck.checkArgs('fFO', 'compass.getCurrentHeading', arguments);

                    var win = function (result) {
                        var ch = new CompassHeading(result.magneticHeading, result.trueHeading, result.headingAccuracy, result.timestamp);
                        successCallback(ch);
                    };
                    var fail = errorCallback && function (code) {
                            var ce = new CompassError(code);
                            errorCallback(ce);
                        };

                    // Get heading
                    exec(win, fail, "Compass", "getHeading", [options]);
                },

                /**
                 * Asynchronously acquires the heading repeatedly at a given interval.
                 * @param {Function} successCallback The function to call each time the heading
                 * data is available
                 * @param {Function} errorCallback The function to call when there is an error
                 * getting the heading data.
                 * @param {HeadingOptions} options The options for getting the heading data
                 * such as timeout and the frequency of the watch. For iOS, filter parameter
                 * specifies to watch via a distance filter rather than time.
                 */
                watchHeading: function (successCallback, errorCallback, options) {
                    argscheck.checkArgs('fFO', 'compass.watchHeading', arguments);
                    // Default interval (100 msec)
                    var frequency = (options !== undefined && options.frequency !== undefined) ? options.frequency : 100;
                    var filter = (options !== undefined && options.filter !== undefined) ? options.filter : 0;

                    var id = utils.createUUID();
                    if (filter > 0) {
                        // is an iOS request for watch by filter, no timer needed
                        timers[id] = "iOS";
                        compass.getCurrentHeading(successCallback, errorCallback, options);
                    } else {
                        // Start watch timer to get headings
                        timers[id] = window.setInterval(function () {
                            compass.getCurrentHeading(successCallback, errorCallback);
                        }, frequency);
                    }

                    if (cordova.platformId === 'browser' && !eventTimerId) {
                        // Start firing deviceorientation events if haven't already
                        var deviceorientationEvent = new Event('deviceorientation');
                        eventTimerId = window.setInterval(function () {
                            window.dispatchEvent(deviceorientationEvent);
                        }, 200);
                    }

                    return id;
                },

                /**
                 * Clears the specified heading watch.
                 * @param {String} id The ID of the watch returned from #watchHeading.
                 */
                clearWatch: function (id) {
                    // Stop javascript timer & remove from timer list
                    if (id && timers[id]) {
                        if (timers[id] != "iOS") {
                            clearInterval(timers[id]);
                        } else {
                            // is iOS watch by filter so call into device to stop
                            exec(null, null, "Compass", "stopHeading", []);
                        }
                        delete timers[id];

                        if (eventTimerId && Object.keys(timers).length === 0) {
                            // No more watchers, so stop firing 'deviceorientation' events
                            window.clearInterval(eventTimerId);
                            eventTimerId = null;
                        }
                    }
                }
            };

        module.exports = compass;

    });

    cordova.define("cordova-plugin-inappbrowser.inappbrowser", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        (function () {
            // special patch to correctly work on Ripple emulator (CB-9760)
            if (window.parent && !!window.parent.ripple) { // https://gist.github.com/triceam/4658021
                module.exports = window.open.bind(window); // fallback to default window.open behaviour
                return;
            }

            var exec = require('cordova/exec');
            var channel = require('cordova/channel');
            var modulemapper = require('cordova/modulemapper');
            var urlutil = require('cordova/urlutil');

            function InAppBrowser() {
                this.channels = {
                    'loadstart': channel.create('loadstart'),
                    'loadstop': channel.create('loadstop'),
                    'loaderror': channel.create('loaderror'),
                    'exit': channel.create('exit')
                };
            }

            InAppBrowser.prototype = {
                _eventHandler: function (event) {
                    if (event && (event.type in this.channels)) {
                        this.channels[event.type].fire(event);
                    }
                },
                close: function (eventname) {
                    exec(null, null, "InAppBrowser", "close", []);
                },
                show: function (eventname) {
                    exec(null, null, "InAppBrowser", "show", []);
                },
                hide: function (eventname) {
                    exec(null, null, "InAppBrowser", "hide", []);
                },
                addEventListener: function (eventname, f) {
                    if (eventname in this.channels) {
                        this.channels[eventname].subscribe(f);
                    }
                },
                removeEventListener: function (eventname, f) {
                    if (eventname in this.channels) {
                        this.channels[eventname].unsubscribe(f);
                    }
                },

                executeScript: function (injectDetails, cb) {
                    if (injectDetails.code) {
                        exec(cb, null, "InAppBrowser", "injectScriptCode", [injectDetails.code, !!cb]);
                    } else if (injectDetails.file) {
                        exec(cb, null, "InAppBrowser", "injectScriptFile", [injectDetails.file, !!cb]);
                    } else {
                        throw new Error('executeScript requires exactly one of code or file to be specified');
                    }
                },

                insertCSS: function (injectDetails, cb) {
                    if (injectDetails.code) {
                        exec(cb, null, "InAppBrowser", "injectStyleCode", [injectDetails.code, !!cb]);
                    } else if (injectDetails.file) {
                        exec(cb, null, "InAppBrowser", "injectStyleFile", [injectDetails.file, !!cb]);
                    } else {
                        throw new Error('insertCSS requires exactly one of code or file to be specified');
                    }
                }
            };

            module.exports = function (strUrl, strWindowName, strWindowFeatures, callbacks) {
                // Don't catch calls that write to existing frames (e.g. named iframes).
                if (window.frames && window.frames[strWindowName]) {
                    var origOpenFunc = modulemapper.getOriginalSymbol(window, 'open');
                    return origOpenFunc.apply(window, arguments);
                }

                strUrl = urlutil.makeAbsolute(strUrl);
                var iab = new InAppBrowser();

                callbacks = callbacks || {};
                for (var callbackName in callbacks) {
                    iab.addEventListener(callbackName, callbacks[callbackName]);
                }

                var cb = function (eventname) {
                    iab._eventHandler(eventname);
                };

                strWindowFeatures = strWindowFeatures || "";

                exec(cb, cb, "InAppBrowser", "open", [strUrl, strWindowName, strWindowFeatures]);
                return iab;
            };
        })();

    });

    cordova.define("cordova-plugin-media.MediaError", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * This class contains information about any Media errors.
         */
        /*
         According to :: http://dev.w3.org/html5/spec-author-view/video.html#mediaerror
         We should never be creating these objects, we should just implement the interface
         which has 1 property for an instance, 'code'

         instead of doing :
         errorCallbackFunction( new MediaError(3,'msg') );
         we should simply use a literal :
         errorCallbackFunction( {'code':3} );
         */

        var _MediaError = window.MediaError;


        if (!_MediaError) {
            window.MediaError = _MediaError = function (code, msg) {
                this.code = (typeof code != 'undefined') ? code : null;
                this.message = msg || ""; // message is NON-standard! do not use!
            };
        }

        _MediaError.MEDIA_ERR_NONE_ACTIVE = _MediaError.MEDIA_ERR_NONE_ACTIVE || 0;
        _MediaError.MEDIA_ERR_ABORTED = _MediaError.MEDIA_ERR_ABORTED || 1;
        _MediaError.MEDIA_ERR_NETWORK = _MediaError.MEDIA_ERR_NETWORK || 2;
        _MediaError.MEDIA_ERR_DECODE = _MediaError.MEDIA_ERR_DECODE || 3;
        _MediaError.MEDIA_ERR_NONE_SUPPORTED = _MediaError.MEDIA_ERR_NONE_SUPPORTED || 4;
// TODO: MediaError.MEDIA_ERR_NONE_SUPPORTED is legacy, the W3 spec now defines it as below.
// as defined by http://dev.w3.org/html5/spec-author-view/video.html#error-codes
        _MediaError.MEDIA_ERR_SRC_NOT_SUPPORTED = _MediaError.MEDIA_ERR_SRC_NOT_SUPPORTED || 4;

        module.exports = _MediaError;

    });

    cordova.define("cordova-plugin-media.Media", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var argscheck = require('cordova/argscheck'),
            utils = require('cordova/utils'),
            exec = require('cordova/exec');

        var mediaObjects = {};

        /**
         * This class provides access to the device media, interfaces to both sound and video
         *
         * @constructor
         * @param src                   The file name or url to play
         * @param successCallback       The callback to be called when the file is done playing or recording.
         *                                  successCallback()
         * @param errorCallback         The callback to be called if there is an error.
         *                                  errorCallback(int errorCode) - OPTIONAL
         * @param statusCallback        The callback to be called when media status has changed.
         *                                  statusCallback(int statusCode) - OPTIONAL
         */
        var Media = function (src, successCallback, errorCallback, statusCallback) {
            argscheck.checkArgs('sFFF', 'Media', arguments);
            this.id = utils.createUUID();
            mediaObjects[this.id] = this;
            this.src = src;
            this.successCallback = successCallback;
            this.errorCallback = errorCallback;
            this.statusCallback = statusCallback;
            this._duration = -1;
            this._position = -1;
            exec(null, this.errorCallback, "Media", "create", [this.id, this.src]);
        };

// Media messages
        Media.MEDIA_STATE = 1;
        Media.MEDIA_DURATION = 2;
        Media.MEDIA_POSITION = 3;
        Media.MEDIA_ERROR = 9;

// Media states
        Media.MEDIA_NONE = 0;
        Media.MEDIA_STARTING = 1;
        Media.MEDIA_RUNNING = 2;
        Media.MEDIA_PAUSED = 3;
        Media.MEDIA_STOPPED = 4;
        Media.MEDIA_MSG = ["None", "Starting", "Running", "Paused", "Stopped"];

// "static" function to return existing objs.
        Media.get = function (id) {
            return mediaObjects[id];
        };

        /**
         * Start or resume playing audio file.
         */
        Media.prototype.play = function (options) {
            exec(null, null, "Media", "startPlayingAudio", [this.id, this.src, options]);
        };

        /**
         * Stop playing audio file.
         */
        Media.prototype.stop = function () {
            var me = this;
            exec(function () {
                me._position = 0;
            }, this.errorCallback, "Media", "stopPlayingAudio", [this.id]);
        };

        /**
         * Seek or jump to a new time in the track..
         */
        Media.prototype.seekTo = function (milliseconds) {
            var me = this;
            exec(function (p) {
                me._position = p;
            }, this.errorCallback, "Media", "seekToAudio", [this.id, milliseconds]);
        };

        /**
         * Pause playing audio file.
         */
        Media.prototype.pause = function () {
            exec(null, this.errorCallback, "Media", "pausePlayingAudio", [this.id]);
        };

        /**
         * Get duration of an audio file.
         * The duration is only set for audio that is playing, paused or stopped.
         *
         * @return      duration or -1 if not known.
         */
        Media.prototype.getDuration = function () {
            return this._duration;
        };

        /**
         * Get position of audio.
         */
        Media.prototype.getCurrentPosition = function (success, fail) {
            var me = this;
            exec(function (p) {
                me._position = p;
                success(p);
            }, fail, "Media", "getCurrentPositionAudio", [this.id]);
        };

        /**
         * Start recording audio file.
         */
        Media.prototype.startRecord = function () {
            exec(null, this.errorCallback, "Media", "startRecordingAudio", [this.id, this.src]);
        };

        /**
         * Stop recording audio file.
         */
        Media.prototype.stopRecord = function () {
            exec(null, this.errorCallback, "Media", "stopRecordingAudio", [this.id]);
        };

        /**
         * Pause recording audio file.
         */
        Media.prototype.pauseRecord = function () {
            exec(null, this.errorCallback, "Media", "pauseRecordingAudio", [this.id]);
        };

        /**
         * Resume recording audio file.
         */
        Media.prototype.resumeRecord = function () {
            exec(null, this.errorCallback, "Media", "resumeRecordingAudio", [this.id]);
        };

        /**
         * Release the resources.
         */
        Media.prototype.release = function () {
            exec(null, this.errorCallback, "Media", "release", [this.id]);
        };

        /**
         * Adjust the volume.
         */
        Media.prototype.setVolume = function (volume) {
            exec(null, null, "Media", "setVolume", [this.id, volume]);
        };

        /**
         * Adjust the playback rate.
         */
        Media.prototype.setRate = function (rate) {
            if (cordova.platformId === 'ios') {
                exec(null, null, "Media", "setRate", [this.id, rate]);
            } else {
                console.warn('media.setRate method is currently not supported for', cordova.platformId, 'platform.');
            }
        };

        /**
         * Get amplitude of audio.
         */
        Media.prototype.getCurrentAmplitude = function (success, fail) {
            exec(function (p) {
                success(p);
            }, fail, "Media", "getCurrentAmplitudeAudio", [this.id]);
        };

        /**
         * Audio has status update.
         * PRIVATE
         *
         * @param id            The media object id (string)
         * @param msgType       The 'type' of update this is
         * @param value         Use of value is determined by the msgType
         */
        Media.onStatus = function (id, msgType, value) {

            var media = mediaObjects[id];

            if (media) {
                switch (msgType) {
                    case Media.MEDIA_STATE :
                        if (media.statusCallback) {
                            media.statusCallback(value);
                        }
                        if (value == Media.MEDIA_STOPPED) {
                            if (media.successCallback) {
                                media.successCallback();
                            }
                        }
                        break;
                    case Media.MEDIA_DURATION :
                        media._duration = value;
                        break;
                    case Media.MEDIA_ERROR :
                        if (media.errorCallback) {
                            media.errorCallback(value);
                        }
                        break;
                    case Media.MEDIA_POSITION :
                        media._position = Number(value);
                        break;
                    default :
                        if (console.error) {
                            console.error("Unhandled Media.onStatus :: " + msgType);
                        }
                        break;
                }
            } else if (console.error) {
                console.error("Received Media.onStatus callback for unknown media :: " + id);
            }

        };

        module.exports = Media;

        function onMessageFromNative(msg) {
            if (msg.action == 'status') {
                Media.onStatus(msg.status.id, msg.status.msgType, msg.status.value);
            } else {
                throw new Error('Unknown media action' + msg.action);
            }
        }

        if (cordova.platformId === 'android' || cordova.platformId === 'amazon-fireos' || cordova.platformId === 'windowsphone') {

            var channel = require('cordova/channel');

            channel.createSticky('onMediaPluginReady');
            channel.waitForInitialization('onMediaPluginReady');

            channel.onCordovaReady.subscribe(function () {
                exec(onMessageFromNative, undefined, 'Media', 'messageChannel', []);
                channel.initializationComplete('onMediaPluginReady');
            });
        }

    });

    cordova.define("cordova-plugin-media-capture.CaptureAudioOptions", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * Encapsulates all audio capture operation configuration options.
         */
        var CaptureAudioOptions = function () {
            // Upper limit of sound clips user can record. Value must be equal or greater than 1.
            this.limit = 1;
            // Maximum duration of a single sound clip in seconds.
            this.duration = 0;
        };

        module.exports = CaptureAudioOptions;

    });

    cordova.define("cordova-plugin-media-capture.CaptureImageOptions", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * Encapsulates all image capture operation configuration options.
         */
        var CaptureImageOptions = function () {
            // Upper limit of images user can take. Value must be equal or greater than 1.
            this.limit = 1;
        };

        module.exports = CaptureImageOptions;

    });

    cordova.define("cordova-plugin-media-capture.CaptureVideoOptions", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * Encapsulates all video capture operation configuration options.
         */
        var CaptureVideoOptions = function () {
            // Upper limit of videos user can record. Value must be equal or greater than 1.
            this.limit = 1;
            // Maximum duration of a single video clip in seconds.
            this.duration = 0;
            // Video quality parameter, 0 means low quality, suitable for MMS messages, and value 1 means high quality.
            this.quality = 1;
        };

        module.exports = CaptureVideoOptions;

    });

    cordova.define("cordova-plugin-media-capture.CaptureError", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * The CaptureError interface encapsulates all errors in the Capture API.
         */
        var CaptureError = function (c) {
            this.code = c || null;
        };

// Camera or microphone failed to capture image or sound.
        CaptureError.CAPTURE_INTERNAL_ERR = 0;
// Camera application or audio capture application is currently serving other capture request.
        CaptureError.CAPTURE_APPLICATION_BUSY = 1;
// Invalid use of the API (e.g. limit parameter has value less than one).
        CaptureError.CAPTURE_INVALID_ARGUMENT = 2;
// User exited camera application or audio capture application before capturing anything.
        CaptureError.CAPTURE_NO_MEDIA_FILES = 3;
// User denied permissions required to perform the capture request.
        CaptureError.CAPTURE_PERMISSION_DENIED = 4;
// The requested capture operation is not supported.
        CaptureError.CAPTURE_NOT_SUPPORTED = 20;

        module.exports = CaptureError;

    });

    cordova.define("cordova-plugin-media-capture.MediaFileData", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /**
         * MediaFileData encapsulates format information of a media file.
         *
         * @param {DOMString} codecs
         * @param {long} bitrate
         * @param {long} height
         * @param {long} width
         * @param {float} duration
         */
        var MediaFileData = function (codecs, bitrate, height, width, duration) {
            this.codecs = codecs || null;
            this.bitrate = bitrate || 0;
            this.height = height || 0;
            this.width = width || 0;
            this.duration = duration || 0;
        };

        module.exports = MediaFileData;

    });

    cordova.define("cordova-plugin-media-capture.MediaFile", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var utils = require('cordova/utils'),
            exec = require('cordova/exec'),
            File = require('cordova-plugin-file.File'),
            CaptureError = require('./CaptureError');
        /**
         * Represents a single file.
         *
         * name {DOMString} name of the file, without path information
         * fullPath {DOMString} the full path of the file, including the name
         * type {DOMString} mime type
         * lastModifiedDate {Date} last modified date
         * size {Number} size of the file in bytes
         */
        var MediaFile = function (name, localURL, type, lastModifiedDate, size) {
            MediaFile.__super__.constructor.apply(this, arguments);
        };

        utils.extend(MediaFile, File);

        /**
         * Request capture format data for a specific file and type
         *
         * @param {Function} successCB
         * @param {Function} errorCB
         */
        MediaFile.prototype.getFormatData = function (successCallback, errorCallback) {
            if (typeof this.fullPath === "undefined" || this.fullPath === null) {
                errorCallback(new CaptureError(CaptureError.CAPTURE_INVALID_ARGUMENT));
            } else {
                exec(successCallback, errorCallback, "Capture", "getFormatData", [this.fullPath, this.type]);
            }
        };

        module.exports = MediaFile;

    });

    cordova.define("cordova-plugin-media-capture.helpers", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var MediaFile = require('./MediaFile');

        function wrapMediaFiles(pluginResult) {
            var mediaFiles = [];
            var i;
            for (i = 0; i < pluginResult.length; i++) {
                var mediaFile = new MediaFile();
                mediaFile.name = pluginResult[i].name;

                // Backwards compatibility
                mediaFile.localURL = pluginResult[i].localURL || pluginResult[i].fullPath;
                mediaFile.fullPath = pluginResult[i].fullPath;
                mediaFile.type = pluginResult[i].type;
                mediaFile.lastModifiedDate = pluginResult[i].lastModifiedDate;
                mediaFile.size = pluginResult[i].size;
                mediaFiles.push(mediaFile);
            }
            return mediaFiles;
        }

        module.exports = {
            wrapMediaFiles: wrapMediaFiles
        };
    });

    cordova.define("cordova-plugin-media-capture.capture", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var exec = require('cordova/exec'),
            helpers = require('./helpers');

        /**
         * Launches a capture of different types.
         *
         * @param (DOMString} type
         * @param {Function} successCB
         * @param {Function} errorCB
         * @param {CaptureVideoOptions} options
         */
        function _capture(type, successCallback, errorCallback, options) {
            var win = function (pluginResult) {
                successCallback(helpers.wrapMediaFiles(pluginResult));
            };
            exec(win, errorCallback, "Capture", type, [options]);
        }


        /**
         * The Capture interface exposes an interface to the camera and microphone of the hosting device.
         */
        function Capture() {
            this.supportedAudioModes = [];
            this.supportedImageModes = [];
            this.supportedVideoModes = [];
        }

        /**
         * Launch audio recorder application for recording audio clip(s).
         *
         * @param {Function} successCB
         * @param {Function} errorCB
         * @param {CaptureAudioOptions} options
         */
        Capture.prototype.captureAudio = function (successCallback, errorCallback, options) {
            _capture("captureAudio", successCallback, errorCallback, options);
        };

        /**
         * Launch camera application for taking image(s).
         *
         * @param {Function} successCB
         * @param {Function} errorCB
         * @param {CaptureImageOptions} options
         */
        Capture.prototype.captureImage = function (successCallback, errorCallback, options) {
            _capture("captureImage", successCallback, errorCallback, options);
        };

        /**
         * Launch device camera application for recording video(s).
         *
         * @param {Function} successCB
         * @param {Function} errorCB
         * @param {CaptureVideoOptions} options
         */
        Capture.prototype.captureVideo = function (successCallback, errorCallback, options) {
            _capture("captureVideo", successCallback, errorCallback, options);
        };


        module.exports = new Capture();

    });

    cordova.define("cordova-plugin-vibration.notification", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var exec = require('cordova/exec');

        /**
         * Provides access to the vibration mechanism on the device.
         */

        module.exports = {

            /**
             * Vibrates the device for a given amount of time or for a given pattern or immediately cancels any ongoing vibrations (depending on the parameter).
             *
             * @param {Integer} param       The number of milliseconds to vibrate (if 0, cancels vibration)
             *
             *
             * @param {Array of Integer} param    Pattern with which to vibrate the device.
             *                                      Pass in an array of integers that
             *                                      are the durations for which to
             *                                      turn on or off the vibrator in
             *                                      milliseconds. The FIRST value
             *                                      indicates the
             *                                      number of milliseconds for which
             *                                      to keep the vibrator ON before
             *                                      turning it off. The NEXT value indicates the
             *                                      number of milliseconds for which
             *                                      to keep the vibrator OFF before
             *                                      turning it on. Subsequent values
             *                                      alternate between durations in
             *                                      milliseconds to turn the vibrator
             *                                      off or to turn the vibrator on.
             *                                      (if empty, cancels vibration)
             */
            vibrate: function (param) {

                /* Aligning with w3c spec */

                //vibrate
                if ((typeof param == 'number') && param !== 0)
                    exec(null, null, "Vibration", "vibrate", [param]);

                //vibrate with array ( i.e. vibrate([3000]) )
                else if ((typeof param == 'object') && param.length == 1) {
                    //cancel if vibrate([0])
                    if (param[0] === 0)
                        exec(null, null, "Vibration", "cancelVibration", []);

                    //else vibrate
                    else
                        exec(null, null, "Vibration", "vibrate", [param[0]]);
                }

                //vibrate with a pattern
                else if ((typeof param == 'object') && param.length > 1) {
                    var repeat = -1; //no repeat
                    exec(null, null, "Vibration", "vibrateWithPattern", [param, repeat]);
                }

                //cancel vibration (param = 0 or [])
                else
                    exec(null, null, "Vibration", "cancelVibration", []);

                return true;
            },

            /**
             * Vibrates the device with a given pattern.
             *
             * @param {Array of Integer} pattern    Pattern with which to vibrate the device.
             *                                      Pass in an array of integers that
             *                                      are the durations for which to
             *                                      turn on or off the vibrator in
             *                                      milliseconds. The first value
             *                                      indicates the number of milliseconds
             *                                      to wait before turning the vibrator
             *                                      on. The next value indicates the
             *                                      number of milliseconds for which
             *                                      to keep the vibrator on before
             *                                      turning it off. Subsequent values
             *                                      alternate between durations in
             *                                      milliseconds to turn the vibrator
             *                                      off or to turn the vibrator on.
             *
             * @param {Integer} repeat              Optional index into the pattern array at which
             *                                      to start repeating (will repeat until canceled),
             *                                      or -1 for no repetition (default).
             */
            vibrateWithPattern: function (pattern, repeat) {
                repeat = (typeof repeat !== "undefined") ? repeat : -1;
                var pattern = pattern.unshift(0); //add a 0 at beginning for backwards compatibility from w3c spec
                exec(null, null, "Vibration", "vibrateWithPattern", [pattern, repeat]);
            },

            /**
             * Immediately cancels any currently running vibration.
             */
            cancelVibration: function () {
                exec(null, null, "Vibration", "cancelVibration", []);
            }
        };

    });

    cordova.define("cordova-plugin-splashscreen.SplashScreen", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        var exec = require('cordova/exec');

        var splashscreen = {
            show: function () {
                exec(null, null, "SplashScreen", "show", []);
            },
            hide: function () {
                exec(null, null, "SplashScreen", "hide", []);
            }
        };

        module.exports = splashscreen;

    });

    cordova.define("cordova-plugin-statusbar.statusbar", function (require, exports, module) {
        /*
         *
         * Licensed to the Apache Software Foundation (ASF) under one
         * or more contributor license agreements.  See the NOTICE file
         * distributed with this work for additional information
         * regarding copyright ownership.  The ASF licenses this file
         * to you under the Apache License, Version 2.0 (the
         * "License"); you may not use this file except in compliance
         * with the License.  You may obtain a copy of the License at
         *
         *   http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing,
         * software distributed under the License is distributed on an
         * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
         * KIND, either express or implied.  See the License for the
         * specific language governing permissions and limitations
         * under the License.
         *
         */

        /* global cordova */

        var exec = require('cordova/exec');

        var namedColors = {
            "black": "#000000",
            "darkGray": "#A9A9A9",
            "lightGray": "#D3D3D3",
            "white": "#FFFFFF",
            "gray": "#808080",
            "red": "#FF0000",
            "green": "#00FF00",
            "blue": "#0000FF",
            "cyan": "#00FFFF",
            "yellow": "#FFFF00",
            "magenta": "#FF00FF",
            "orange": "#FFA500",
            "purple": "#800080",
            "brown": "#A52A2A"
        };

        var StatusBar = {

            isVisible: true,

            overlaysWebView: function (doOverlay) {
                exec(null, null, "StatusBar", "overlaysWebView", [doOverlay]);
            },

            styleDefault: function () {
                // dark text ( to be used on a light background )
                exec(null, null, "StatusBar", "styleDefault", []);
            },

            styleLightContent: function () {
                // light text ( to be used on a dark background )
                exec(null, null, "StatusBar", "styleLightContent", []);
            },

            styleBlackTranslucent: function () {
                // #88000000 ? Apple says to use lightContent instead
                exec(null, null, "StatusBar", "styleBlackTranslucent", []);
            },

            styleBlackOpaque: function () {
                // #FF000000 ? Apple says to use lightContent instead
                exec(null, null, "StatusBar", "styleBlackOpaque", []);
            },

            backgroundColorByName: function (colorname) {
                return StatusBar.backgroundColorByHexString(namedColors[colorname]);
            },

            backgroundColorByHexString: function (hexString) {
                if (hexString.charAt(0) !== "#") {
                    hexString = "#" + hexString;
                }

                if (hexString.length === 4) {
                    var split = hexString.split("");
                    hexString = "#" + split[1] + split[1] + split[2] + split[2] + split[3] + split[3];
                }

                exec(null, null, "StatusBar", "backgroundColorByHexString", [hexString]);
            },

            hide: function () {
                exec(null, null, "StatusBar", "hide", []);
                StatusBar.isVisible = false;
            },

            show: function () {
                exec(null, null, "StatusBar", "show", []);
                StatusBar.isVisible = true;
            }

        };

// prime it. setTimeout so that proxy gets time to init
        window.setTimeout(function () {
            exec(function (res) {
                if (typeof res == 'object') {
                    if (res.type == 'tap') {
                        cordova.fireWindowEvent('statusTap');
                    }
                } else {
                    StatusBar.isVisible = res;
                }
            }, null, "StatusBar", "_ready", []);
        }, 0);

        module.exports = StatusBar;

    });

    cordova.define("com.archibus.plugins.configuration.Configuration", function (require, exports, module) {

        var exec = require('cordova/exec');


        var Configuration = {
            readConfigFile: function (success, error) {
                exec(success, error, 'Configuration', 'readConfigFile', []);
            },
            writeConfigFile: function (data, success, error) {
                exec(success, error, 'Configuration', 'writeConfigFile', [data]);
            },
            configFileExists: function (success, error) {
                exec(success, error, 'Configuration', 'configFileExists', []);
            },
            writeFile: function (fileName, data, success, error) {
                exec(success, error, 'Configuration', 'writeFile', [fileName, data]);
            },
            readFile: function (fileName, success, error) {
                exec(success, error, 'Configuration', 'readFile', [fileName]);
            }
        };

        module.exports = Configuration;

    });

    cordova.define("com.archibus.plugins.appversion.AppVersion", function (require, exports, module) {

        var exec = require('cordova/exec');


        var AppVersion = {
            getAppVersion: function (success, error) {
                exec(success, error, 'AppVersion', 'getAppVersion', []);
            },
            getMajorVersion: function() {
                return new Promise(function(resolve, reject) {
                    AppVersion.getAppVersion(function(version) {
                        var idx = version.indexOf('.');
                        resolve(version.substring(0,idx));
                    }, reject);
                });
            }
        };

        module.exports = AppVersion;


    });

    cordova.define('com.archibus.plugins.textarea.TextArea', function(require, exports, module) {
        var exec = require('cordova/exec');
        var TextArea = {
                openTextArea: function(text, maxCharacters, readOnly, success, failure) {
                   if(!maxCharacters) {
                       maxCharacters = 5000;
                   }
                   if(readOnly === 'undefined') {
                       readOnly = false;
                   }
                   cordova.exec(success, failure, "CDVTextArea", "showTextView", [text, maxCharacters, readOnly]);
                }
            };
            module.exports = TextArea;
    });

    cordova.define("com.archibus.plugins.dbcredential.keychain", function (require, exports, module) {

        var exec = require('cordova/exec');

        var Keychain = {
            setSqlitePassphrase: function (password, success, error) {
                exec(success, error, 'DbCredential', 'setSqlitePassphrase', [password]);
            }
        };

        module.exports = Keychain;


    });

    cordova.define("com.archibus.plugins.docconverter.DocumentConveter", function (require, exports, module) {

        var exec = require('cordova/exec');


        var DocumentConverter = {
            convertBase64ToBinary: function (base64String, fileName, success, error) {
                exec(success, error, 'DocumentConverter', 'convertBase64AndWriteToFile', [base64String, fileName]);
            }
        };

        module.exports = DocumentConverter;

    });

    cordova.define("com.unarin.cordova.beacon.underscorejs", function (require, exports, module) {
        //     Underscore.js 1.6.0
        //     http://underscorejs.org
        //     (c) 2009-2014 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
        //     Underscore may be freely distributed under the MIT license.
                   (function() {
                    
                    // Baseline setup
                    // --------------
                    
                    // Establish the root object, `window` in the browser, or `exports` on the server.
                    var root = this;
                    
                    // Save the previous value of the `_` variable.
                    var previousUnderscore = root._;
                    
                    // Save bytes in the minified (but not gzipped) version:
                    var ArrayProto = Array.prototype, ObjProto = Object.prototype, FuncProto = Function.prototype;
                    
                    // Create quick reference variables for speed access to core prototypes.
                    var
                    push             = ArrayProto.push,
                    slice            = ArrayProto.slice,
                    toString         = ObjProto.toString,
                    hasOwnProperty   = ObjProto.hasOwnProperty;
                    
                    // All **ECMAScript 5** native function implementations that we hope to use
                    // are declared here.
                    var
                    nativeIsArray      = Array.isArray,
                    nativeKeys         = Object.keys,
                    nativeBind         = FuncProto.bind,
                    nativeCreate       = Object.create;
                    
                    // Naked function reference for surrogate-prototype-swapping.
                    var Ctor = function(){};
                    
                    // Create a safe reference to the Underscore object for use below.
                    var _ = function(obj) {
                    if (obj instanceof _) return obj;
                    if (!(this instanceof _)) return new _(obj);
                    this._wrapped = obj;
                    };
                    
                    // Export the Underscore object for **Node.js**, with
                    // backwards-compatibility for the old `require()` API. If we're in
                    // the browser, add `_` as a global object.
                    if (typeof exports !== 'undefined') {
                    if (typeof module !== 'undefined' && module.exports) {
                    exports = module.exports = _;
                    }
                    exports._ = _;
                    } else {
                    root._ = _;
                    }
                    
                    // Current version.
                    _.VERSION = '1.8.3';
                    
                    // Internal function that returns an efficient (for current engines) version
                    // of the passed-in callback, to be repeatedly applied in other Underscore
                    // functions.
                    var optimizeCb = function(func, context, argCount) {
                    if (context === void 0) return func;
                    switch (argCount == null ? 3 : argCount) {
                    case 1: return function(value) {
                    return func.call(context, value);
                    };
                    case 2: return function(value, other) {
                    return func.call(context, value, other);
                    };
                    case 3: return function(value, index, collection) {
                    return func.call(context, value, index, collection);
                    };
                    case 4: return function(accumulator, value, index, collection) {
                    return func.call(context, accumulator, value, index, collection);
                    };
                    }
                    return function() {
                    return func.apply(context, arguments);
                    };
                    };
                    
                    // A mostly-internal function to generate callbacks that can be applied
                    // to each element in a collection, returning the desired result — either
                    // identity, an arbitrary callback, a property matcher, or a property accessor.
                    var cb = function(value, context, argCount) {
                    if (value == null) return _.identity;
                    if (_.isFunction(value)) return optimizeCb(value, context, argCount);
                    if (_.isObject(value)) return _.matcher(value);
                    return _.property(value);
                    };
                    _.iteratee = function(value, context) {
                    return cb(value, context, Infinity);
                    };
                    
                    // An internal function for creating assigner functions.
                    var createAssigner = function(keysFunc, undefinedOnly) {
                    return function(obj) {
                    var length = arguments.length;
                    if (length < 2 || obj == null) return obj;
                    for (var index = 1; index < length; index++) {
                    var source = arguments[index],
                    keys = keysFunc(source),
                    l = keys.length;
                    for (var i = 0; i < l; i++) {
                    var key = keys[i];
                    if (!undefinedOnly || obj[key] === void 0) obj[key] = source[key];
                    }
                    }
                    return obj;
                    };
                    };
                    
                    // An internal function for creating a new object that inherits from another.
                    var baseCreate = function(prototype) {
                    if (!_.isObject(prototype)) return {};
                    if (nativeCreate) return nativeCreate(prototype);
                    Ctor.prototype = prototype;
                    var result = new Ctor;
                    Ctor.prototype = null;
                    return result;
                    };
                    
                    var property = function(key) {
                    return function(obj) {
                    return obj == null ? void 0 : obj[key];
                    };
                    };
                    
                    // Helper for collection methods to determine whether a collection
                    // should be iterated as an array or as an object
                    // Related: http://people.mozilla.org/~jorendorff/es6-draft.html#sec-tolength
                    // Avoids a very nasty iOS 8 JIT bug on ARM-64. #2094
                    var MAX_ARRAY_INDEX = Math.pow(2, 53) - 1;
                    var getLength = property('length');
                    var isArrayLike = function(collection) {
                    var length = getLength(collection);
                    return typeof length == 'number' && length >= 0 && length <= MAX_ARRAY_INDEX;
                    };
                    
                    // Collection Functions
                    // --------------------
                    
                    // The cornerstone, an `each` implementation, aka `forEach`.
                    // Handles raw objects in addition to array-likes. Treats all
                    // sparse array-likes as if they were dense.
                    _.each = _.forEach = function(obj, iteratee, context) {
                    iteratee = optimizeCb(iteratee, context);
                    var i, length;
                    if (isArrayLike(obj)) {
                    for (i = 0, length = obj.length; i < length; i++) {
                    iteratee(obj[i], i, obj);
                    }
                    } else {
                    var keys = _.keys(obj);
                    for (i = 0, length = keys.length; i < length; i++) {
                    iteratee(obj[keys[i]], keys[i], obj);
                    }
                    }
                    return obj;
                    };
                    
                    // Return the results of applying the iteratee to each element.
                    _.map = _.collect = function(obj, iteratee, context) {
                    iteratee = cb(iteratee, context);
                    var keys = !isArrayLike(obj) && _.keys(obj),
                    length = (keys || obj).length,
                    results = Array(length);
                    for (var index = 0; index < length; index++) {
                    var currentKey = keys ? keys[index] : index;
                    results[index] = iteratee(obj[currentKey], currentKey, obj);
                    }
                    return results;
                    };
                    
                    // Create a reducing function iterating left or right.
                    function createReduce(dir) {
                    // Optimized iterator function as using arguments.length
                    // in the main function will deoptimize the, see #1991.
                    function iterator(obj, iteratee, memo, keys, index, length) {
                    for (; index >= 0 && index < length; index += dir) {
                    var currentKey = keys ? keys[index] : index;
                    memo = iteratee(memo, obj[currentKey], currentKey, obj);
                    }
                    return memo;
                    }
                    
                    return function(obj, iteratee, memo, context) {
                    iteratee = optimizeCb(iteratee, context, 4);
                    var keys = !isArrayLike(obj) && _.keys(obj),
                    length = (keys || obj).length,
                    index = dir > 0 ? 0 : length - 1;
                    // Determine the initial value if none is provided.
                    if (arguments.length < 3) {
                    memo = obj[keys ? keys[index] : index];
                    index += dir;
                    }
                    return iterator(obj, iteratee, memo, keys, index, length);
                    };
                    }
                    
                    // **Reduce** builds up a single result from a list of values, aka `inject`,
                    // or `foldl`.
                    _.reduce = _.foldl = _.inject = createReduce(1);
                    
                    // The right-associative version of reduce, also known as `foldr`.
                    _.reduceRight = _.foldr = createReduce(-1);
                    
                    // Return the first value which passes a truth test. Aliased as `detect`.
                    _.find = _.detect = function(obj, predicate, context) {
                    var key;
                    if (isArrayLike(obj)) {
                    key = _.findIndex(obj, predicate, context);
                    } else {
                    key = _.findKey(obj, predicate, context);
                    }
                    if (key !== void 0 && key !== -1) return obj[key];
                    };
                    
                    // Return all the elements that pass a truth test.
                    // Aliased as `select`.
                    _.filter = _.select = function(obj, predicate, context) {
                    var results = [];
                    predicate = cb(predicate, context);
                    _.each(obj, function(value, index, list) {
                           if (predicate(value, index, list)) results.push(value);
                           });
                    return results;
                    };
                    
                    // Return all the elements for which a truth test fails.
                    _.reject = function(obj, predicate, context) {
                    return _.filter(obj, _.negate(cb(predicate)), context);
                    };
                    
                    // Determine whether all of the elements match a truth test.
                    // Aliased as `all`.
                    _.every = _.all = function(obj, predicate, context) {
                    predicate = cb(predicate, context);
                    var keys = !isArrayLike(obj) && _.keys(obj),
                    length = (keys || obj).length;
                    for (var index = 0; index < length; index++) {
                    var currentKey = keys ? keys[index] : index;
                    if (!predicate(obj[currentKey], currentKey, obj)) return false;
                    }
                    return true;
                    };
                    
                    // Determine if at least one element in the object matches a truth test.
                    // Aliased as `any`.
                    _.some = _.any = function(obj, predicate, context) {
                    predicate = cb(predicate, context);
                    var keys = !isArrayLike(obj) && _.keys(obj),
                    length = (keys || obj).length;
                    for (var index = 0; index < length; index++) {
                    var currentKey = keys ? keys[index] : index;
                    if (predicate(obj[currentKey], currentKey, obj)) return true;
                    }
                    return false;
                    };
                    
                    // Determine if the array or object contains a given item (using `===`).
                    // Aliased as `includes` and `include`.
                    _.contains = _.includes = _.include = function(obj, item, fromIndex, guard) {
                    if (!isArrayLike(obj)) obj = _.values(obj);
                    if (typeof fromIndex != 'number' || guard) fromIndex = 0;
                    return _.indexOf(obj, item, fromIndex) >= 0;
                    };
                    
                    // Invoke a method (with arguments) on every item in a collection.
                    _.invoke = function(obj, method) {
                    var args = slice.call(arguments, 2);
                    var isFunc = _.isFunction(method);
                    return _.map(obj, function(value) {
                                 var func = isFunc ? method : value[method];
                                 return func == null ? func : func.apply(value, args);
                                 });
                    };
                    
                    // Convenience version of a common use case of `map`: fetching a property.
                    _.pluck = function(obj, key) {
                    return _.map(obj, _.property(key));
                    };
                    
                    // Convenience version of a common use case of `filter`: selecting only objects
                    // containing specific `key:value` pairs.
                    _.where = function(obj, attrs) {
                    return _.filter(obj, _.matcher(attrs));
                    };
                    
                    // Convenience version of a common use case of `find`: getting the first object
                    // containing specific `key:value` pairs.
                    _.findWhere = function(obj, attrs) {
                    return _.find(obj, _.matcher(attrs));
                    };
                    
                    // Return the maximum element (or element-based computation).
                    _.max = function(obj, iteratee, context) {
                    var result = -Infinity, lastComputed = -Infinity,
                    value, computed;
                    if (iteratee == null && obj != null) {
                    obj = isArrayLike(obj) ? obj : _.values(obj);
                    for (var i = 0, length = obj.length; i < length; i++) {
                    value = obj[i];
                    if (value > result) {
                    result = value;
                    }
                    }
                    } else {
                    iteratee = cb(iteratee, context);
                    _.each(obj, function(value, index, list) {
                           computed = iteratee(value, index, list);
                           if (computed > lastComputed || computed === -Infinity && result === -Infinity) {
                           result = value;
                           lastComputed = computed;
                           }
                           });
                    }
                    return result;
                    };
                    
                    // Return the minimum element (or element-based computation).
                    _.min = function(obj, iteratee, context) {
                    var result = Infinity, lastComputed = Infinity,
                    value, computed;
                    if (iteratee == null && obj != null) {
                    obj = isArrayLike(obj) ? obj : _.values(obj);
                    for (var i = 0, length = obj.length; i < length; i++) {
                    value = obj[i];
                    if (value < result) {
                    result = value;
                    }
                    }
                    } else {
                    iteratee = cb(iteratee, context);
                    _.each(obj, function(value, index, list) {
                           computed = iteratee(value, index, list);
                           if (computed < lastComputed || computed === Infinity && result === Infinity) {
                           result = value;
                           lastComputed = computed;
                           }
                           });
                    }
                    return result;
                    };
                    
                    // Shuffle a collection, using the modern version of the
                    // [Fisher-Yates shuffle](http://en.wikipedia.org/wiki/Fisher–Yates_shuffle).
                    _.shuffle = function(obj) {
                    var set = isArrayLike(obj) ? obj : _.values(obj);
                    var length = set.length;
                    var shuffled = Array(length);
                    for (var index = 0, rand; index < length; index++) {
                    rand = _.random(0, index);
                    if (rand !== index) shuffled[index] = shuffled[rand];
                    shuffled[rand] = set[index];
                    }
                    return shuffled;
                    };
                    
                    // Sample **n** random values from a collection.
                    // If **n** is not specified, returns a single random element.
                    // The internal `guard` argument allows it to work with `map`.
                    _.sample = function(obj, n, guard) {
                    if (n == null || guard) {
                    if (!isArrayLike(obj)) obj = _.values(obj);
                    return obj[_.random(obj.length - 1)];
                    }
                    return _.shuffle(obj).slice(0, Math.max(0, n));
                    };
                    
                    // Sort the object's values by a criterion produced by an iteratee.
                    _.sortBy = function(obj, iteratee, context) {
                    iteratee = cb(iteratee, context);
                    return _.pluck(_.map(obj, function(value, index, list) {
                                         return {
                                         value: value,
                                         index: index,
                                         criteria: iteratee(value, index, list)
                                         };
                                         }).sort(function(left, right) {
                                                 var a = left.criteria;
                                                 var b = right.criteria;
                                                 if (a !== b) {
                                                 if (a > b || a === void 0) return 1;
                                                 if (a < b || b === void 0) return -1;
                                                 }
                                                 return left.index - right.index;
                                                 }), 'value');
                    };
                    
                    // An internal function used for aggregate "group by" operations.
                    var group = function(behavior) {
                    return function(obj, iteratee, context) {
                    var result = {};
                    iteratee = cb(iteratee, context);
                    _.each(obj, function(value, index) {
                           var key = iteratee(value, index, obj);
                           behavior(result, value, key);
                           });
                    return result;
                    };
                    };
                    
                    // Groups the object's values by a criterion. Pass either a string attribute
                    // to group by, or a function that returns the criterion.
                    _.groupBy = group(function(result, value, key) {
                                      if (_.has(result, key)) result[key].push(value); else result[key] = [value];
                                      });
                    
                    // Indexes the object's values by a criterion, similar to `groupBy`, but for
                    // when you know that your index values will be unique.
                    _.indexBy = group(function(result, value, key) {
                                      result[key] = value;
                                      });
                    
                    // Counts instances of an object that group by a certain criterion. Pass
                    // either a string attribute to count by, or a function that returns the
                    // criterion.
                    _.countBy = group(function(result, value, key) {
                                      if (_.has(result, key)) result[key]++; else result[key] = 1;
                                      });
                    
                    // Safely create a real, live array from anything iterable.
                    _.toArray = function(obj) {
                    if (!obj) return [];
                    if (_.isArray(obj)) return slice.call(obj);
                    if (isArrayLike(obj)) return _.map(obj, _.identity);
                    return _.values(obj);
                    };
                    
                    // Return the number of elements in an object.
                    _.size = function(obj) {
                    if (obj == null) return 0;
                    return isArrayLike(obj) ? obj.length : _.keys(obj).length;
                    };
                    
                    // Split a collection into two arrays: one whose elements all satisfy the given
                    // predicate, and one whose elements all do not satisfy the predicate.
                    _.partition = function(obj, predicate, context) {
                    predicate = cb(predicate, context);
                    var pass = [], fail = [];
                    _.each(obj, function(value, key, obj) {
                           (predicate(value, key, obj) ? pass : fail).push(value);
                           });
                    return [pass, fail];
                    };
                    
                    // Array Functions
                    // ---------------
                    
                    // Get the first element of an array. Passing **n** will return the first N
                    // values in the array. Aliased as `head` and `take`. The **guard** check
                    // allows it to work with `_.map`.
                    _.first = _.head = _.take = function(array, n, guard) {
                    if (array == null) return void 0;
                    if (n == null || guard) return array[0];
                    return _.initial(array, array.length - n);
                    };
                    
                    // Returns everything but the last entry of the array. Especially useful on
                    // the arguments object. Passing **n** will return all the values in
                    // the array, excluding the last N.
                    _.initial = function(array, n, guard) {
                    return slice.call(array, 0, Math.max(0, array.length - (n == null || guard ? 1 : n)));
                    };
                    
                    // Get the last element of an array. Passing **n** will return the last N
                    // values in the array.
                    _.last = function(array, n, guard) {
                    if (array == null) return void 0;
                    if (n == null || guard) return array[array.length - 1];
                    return _.rest(array, Math.max(0, array.length - n));
                    };
                    
                    // Returns everything but the first entry of the array. Aliased as `tail` and `drop`.
                    // Especially useful on the arguments object. Passing an **n** will return
                    // the rest N values in the array.
                    _.rest = _.tail = _.drop = function(array, n, guard) {
                    return slice.call(array, n == null || guard ? 1 : n);
                    };
                    
                    // Trim out all falsy values from an array.
                    _.compact = function(array) {
                    return _.filter(array, _.identity);
                    };
                    
                    // Internal implementation of a recursive `flatten` function.
                    var flatten = function(input, shallow, strict, startIndex) {
                    var output = [], idx = 0;
                    for (var i = startIndex || 0, length = getLength(input); i < length; i++) {
                    var value = input[i];
                    if (isArrayLike(value) && (_.isArray(value) || _.isArguments(value))) {
                    //flatten current level of array or arguments object
                    if (!shallow) value = flatten(value, shallow, strict);
                    var j = 0, len = value.length;
                    output.length += len;
                    while (j < len) {
                    output[idx++] = value[j++];
                    }
                    } else if (!strict) {
                    output[idx++] = value;
                    }
                    }
                    return output;
                    };
                    
                    // Flatten out an array, either recursively (by default), or just one level.
                    _.flatten = function(array, shallow) {
                    return flatten(array, shallow, false);
                    };
                    
                    // Return a version of the array that does not contain the specified value(s).
                    _.without = function(array) {
                    return _.difference(array, slice.call(arguments, 1));
                    };
                    
                    // Produce a duplicate-free version of the array. If the array has already
                    // been sorted, you have the option of using a faster algorithm.
                    // Aliased as `unique`.
                    _.uniq = _.unique = function(array, isSorted, iteratee, context) {
                    if (!_.isBoolean(isSorted)) {
                    context = iteratee;
                    iteratee = isSorted;
                    isSorted = false;
                    }
                    if (iteratee != null) iteratee = cb(iteratee, context);
                    var result = [];
                    var seen = [];
                    for (var i = 0, length = getLength(array); i < length; i++) {
                    var value = array[i],
                    computed = iteratee ? iteratee(value, i, array) : value;
                    if (isSorted) {
                    if (!i || seen !== computed) result.push(value);
                    seen = computed;
                    } else if (iteratee) {
                    if (!_.contains(seen, computed)) {
                    seen.push(computed);
                    result.push(value);
                    }
                    } else if (!_.contains(result, value)) {
                    result.push(value);
                    }
                    }
                    return result;
                    };
                    
                    // Produce an array that contains the union: each distinct element from all of
                    // the passed-in arrays.
                    _.union = function() {
                    return _.uniq(flatten(arguments, true, true));
                    };
                    
                    // Produce an array that contains every item shared between all the
                    // passed-in arrays.
                    _.intersection = function(array) {
                    var result = [];
                    var argsLength = arguments.length;
                    for (var i = 0, length = getLength(array); i < length; i++) {
                    var item = array[i];
                    if (_.contains(result, item)) continue;
                    for (var j = 1; j < argsLength; j++) {
                    if (!_.contains(arguments[j], item)) break;
                    }
                    if (j === argsLength) result.push(item);
                    }
                    return result;
                    };
                    
                    // Take the difference between one array and a number of other arrays.
                    // Only the elements present in just the first array will remain.
                    _.difference = function(array) {
                    var rest = flatten(arguments, true, true, 1);
                    return _.filter(array, function(value){
                                    return !_.contains(rest, value);
                                    });
                    };
                    
                    // Zip together multiple lists into a single array -- elements that share
                    // an index go together.
                    _.zip = function() {
                    return _.unzip(arguments);
                    };
                    
                    // Complement of _.zip. Unzip accepts an array of arrays and groups
                    // each array's elements on shared indices
                    _.unzip = function(array) {
                    var length = array && _.max(array, getLength).length || 0;
                    var result = Array(length);
                    
                    for (var index = 0; index < length; index++) {
                    result[index] = _.pluck(array, index);
                    }
                    return result;
                    };
                    
                    // Converts lists into objects. Pass either a single array of `[key, value]`
                    // pairs, or two parallel arrays of the same length -- one of keys, and one of
                    // the corresponding values.
                    _.object = function(list, values) {
                    var result = {};
                    for (var i = 0, length = getLength(list); i < length; i++) {
                    if (values) {
                    result[list[i]] = values[i];
                    } else {
                    result[list[i][0]] = list[i][1];
                    }
                    }
                    return result;
                    };
                    
                    // Generator function to create the findIndex and findLastIndex functions
                    function createPredicateIndexFinder(dir) {
                    return function(array, predicate, context) {
                    predicate = cb(predicate, context);
                    var length = getLength(array);
                    var index = dir > 0 ? 0 : length - 1;
                    for (; index >= 0 && index < length; index += dir) {
                    if (predicate(array[index], index, array)) return index;
                    }
                    return -1;
                    };
                    }
                    
                    // Returns the first index on an array-like that passes a predicate test
                    _.findIndex = createPredicateIndexFinder(1);
                    _.findLastIndex = createPredicateIndexFinder(-1);
                    
                    // Use a comparator function to figure out the smallest index at which
                    // an object should be inserted so as to maintain order. Uses binary search.
                    _.sortedIndex = function(array, obj, iteratee, context) {
                    iteratee = cb(iteratee, context, 1);
                    var value = iteratee(obj);
                    var low = 0, high = getLength(array);
                    while (low < high) {
                    var mid = Math.floor((low + high) / 2);
                    if (iteratee(array[mid]) < value) low = mid + 1; else high = mid;
                    }
                    return low;
                    };
                    
                    // Generator function to create the indexOf and lastIndexOf functions
                    function createIndexFinder(dir, predicateFind, sortedIndex) {
                    return function(array, item, idx) {
                    var i = 0, length = getLength(array);
                    if (typeof idx == 'number') {
                    if (dir > 0) {
                    i = idx >= 0 ? idx : Math.max(idx + length, i);
                    } else {
                    length = idx >= 0 ? Math.min(idx + 1, length) : idx + length + 1;
                    }
                    } else if (sortedIndex && idx && length) {
                    idx = sortedIndex(array, item);
                    return array[idx] === item ? idx : -1;
                    }
                    if (item !== item) {
                    idx = predicateFind(slice.call(array, i, length), _.isNaN);
                    return idx >= 0 ? idx + i : -1;
                    }
                    for (idx = dir > 0 ? i : length - 1; idx >= 0 && idx < length; idx += dir) {
                    if (array[idx] === item) return idx;
                    }
                    return -1;
                    };
                    }
                    
                    // Return the position of the first occurrence of an item in an array,
                    // or -1 if the item is not included in the array.
                    // If the array is large and already in sort order, pass `true`
                    // for **isSorted** to use binary search.
                    _.indexOf = createIndexFinder(1, _.findIndex, _.sortedIndex);
                    _.lastIndexOf = createIndexFinder(-1, _.findLastIndex);
                    
                    // Generate an integer Array containing an arithmetic progression. A port of
                    // the native Python `range()` function. See
                    // [the Python documentation](http://docs.python.org/library/functions.html#range).
                    _.range = function(start, stop, step) {
                    if (stop == null) {
                    stop = start || 0;
                    start = 0;
                    }
                    step = step || 1;
                    
                    var length = Math.max(Math.ceil((stop - start) / step), 0);
                    var range = Array(length);
                    
                    for (var idx = 0; idx < length; idx++, start += step) {
                    range[idx] = start;
                    }
                    
                    return range;
                    };
                    
                    // Function (ahem) Functions
                    // ------------------
                    
                    // Determines whether to execute a function as a constructor
                    // or a normal function with the provided arguments
                    var executeBound = function(sourceFunc, boundFunc, context, callingContext, args) {
                    if (!(callingContext instanceof boundFunc)) return sourceFunc.apply(context, args);
                    var self = baseCreate(sourceFunc.prototype);
                    var result = sourceFunc.apply(self, args);
                    if (_.isObject(result)) return result;
                    return self;
                    };
                    
                    // Create a function bound to a given object (assigning `this`, and arguments,
                    // optionally). Delegates to **ECMAScript 5**'s native `Function.bind` if
                    // available.
                    _.bind = function(func, context) {
                    if (nativeBind && func.bind === nativeBind) return nativeBind.apply(func, slice.call(arguments, 1));
                    if (!_.isFunction(func)) throw new TypeError('Bind must be called on a function');
                    var args = slice.call(arguments, 2);
                    var bound = function() {
                    return executeBound(func, bound, context, this, args.concat(slice.call(arguments)));
                    };
                    return bound;
                    };
                    
                    // Partially apply a function by creating a version that has had some of its
                    // arguments pre-filled, without changing its dynamic `this` context. _ acts
                    // as a placeholder, allowing any combination of arguments to be pre-filled.
                    _.partial = function(func) {
                    var boundArgs = slice.call(arguments, 1);
                    var bound = function() {
                    var position = 0, length = boundArgs.length;
                    var args = Array(length);
                    for (var i = 0; i < length; i++) {
                    args[i] = boundArgs[i] === _ ? arguments[position++] : boundArgs[i];
                    }
                    while (position < arguments.length) args.push(arguments[position++]);
                    return executeBound(func, bound, this, this, args);
                    };
                    return bound;
                    };
                    
                    // Bind a number of an object's methods to that object. Remaining arguments
                    // are the method names to be bound. Useful for ensuring that all callbacks
                    // defined on an object belong to it.
                    _.bindAll = function(obj) {
                    var i, length = arguments.length, key;
                    if (length <= 1) throw new Error('bindAll must be passed function names');
                    for (i = 1; i < length; i++) {
                    key = arguments[i];
                    obj[key] = _.bind(obj[key], obj);
                    }
                    return obj;
                    };
                    
                    // Memoize an expensive function by storing its results.
                    _.memoize = function(func, hasher) {
                    var memoize = function(key) {
                    var cache = memoize.cache;
                    var address = '' + (hasher ? hasher.apply(this, arguments) : key);
                    if (!_.has(cache, address)) cache[address] = func.apply(this, arguments);
                    return cache[address];
                    };
                    memoize.cache = {};
                    return memoize;
                    };
                    
                    // Delays a function for the given number of milliseconds, and then calls
                    // it with the arguments supplied.
                    _.delay = function(func, wait) {
                    var args = slice.call(arguments, 2);
                    return setTimeout(function(){
                                      return func.apply(null, args);
                                      }, wait);
                    };
                    
                    // Defers a function, scheduling it to run after the current call stack has
                    // cleared.
                    _.defer = _.partial(_.delay, _, 1);
                    
                    // Returns a function, that, when invoked, will only be triggered at most once
                    // during a given window of time. Normally, the throttled function will run
                    // as much as it can, without ever going more than once per `wait` duration;
                    // but if you'd like to disable the execution on the leading edge, pass
                    // `{leading: false}`. To disable execution on the trailing edge, ditto.
                    _.throttle = function(func, wait, options) {
                    var context, args, result;
                    var timeout = null;
                    var previous = 0;
                    if (!options) options = {};
                    var later = function() {
                    previous = options.leading === false ? 0 : _.now();
                    timeout = null;
                    result = func.apply(context, args);
                    if (!timeout) context = args = null;
                    };
                    return function() {
                    var now = _.now();
                    if (!previous && options.leading === false) previous = now;
                    var remaining = wait - (now - previous);
                    context = this;
                    args = arguments;
                    if (remaining <= 0 || remaining > wait) {
                    if (timeout) {
                    clearTimeout(timeout);
                    timeout = null;
                    }
                    previous = now;
                    result = func.apply(context, args);
                    if (!timeout) context = args = null;
                    } else if (!timeout && options.trailing !== false) {
                    timeout = setTimeout(later, remaining);
                    }
                    return result;
                    };
                    };
                    
                    // Returns a function, that, as long as it continues to be invoked, will not
                    // be triggered. The function will be called after it stops being called for
                    // N milliseconds. If `immediate` is passed, trigger the function on the
                    // leading edge, instead of the trailing.
                    _.debounce = function(func, wait, immediate) {
                    var timeout, args, context, timestamp, result;
                    
                    var later = function() {
                    var last = _.now() - timestamp;
                    
                    if (last < wait && last >= 0) {
                    timeout = setTimeout(later, wait - last);
                    } else {
                    timeout = null;
                    if (!immediate) {
                    result = func.apply(context, args);
                    if (!timeout) context = args = null;
                    }
                    }
                    };
                    
                    return function() {
                    context = this;
                    args = arguments;
                    timestamp = _.now();
                    var callNow = immediate && !timeout;
                    if (!timeout) timeout = setTimeout(later, wait);
                    if (callNow) {
                    result = func.apply(context, args);
                    context = args = null;
                    }
                    
                    return result;
                    };
                    };
                    
                    // Returns the first function passed as an argument to the second,
                    // allowing you to adjust arguments, run code before and after, and
                    // conditionally execute the original function.
                    _.wrap = function(func, wrapper) {
                    return _.partial(wrapper, func);
                    };
                    
                    // Returns a negated version of the passed-in predicate.
                    _.negate = function(predicate) {
                    return function() {
                    return !predicate.apply(this, arguments);
                    };
                    };
                    
                    // Returns a function that is the composition of a list of functions, each
                    // consuming the return value of the function that follows.
                    _.compose = function() {
                    var args = arguments;
                    var start = args.length - 1;
                    return function() {
                    var i = start;
                    var result = args[start].apply(this, arguments);
                    while (i--) result = args[i].call(this, result);
                    return result;
                    };
                    };
                    
                    // Returns a function that will only be executed on and after the Nth call.
                    _.after = function(times, func) {
                    return function() {
                    if (--times < 1) {
                    return func.apply(this, arguments);
                    }
                    };
                    };
                    
                    // Returns a function that will only be executed up to (but not including) the Nth call.
                    _.before = function(times, func) {
                    var memo;
                    return function() {
                    if (--times > 0) {
                    memo = func.apply(this, arguments);
                    }
                    if (times <= 1) func = null;
                    return memo;
                    };
                    };
                    
                    // Returns a function that will be executed at most one time, no matter how
                    // often you call it. Useful for lazy initialization.
                    _.once = _.partial(_.before, 2);
                    
                    // Object Functions
                    // ----------------
                    
                    // Keys in IE < 9 that won't be iterated by `for key in ...` and thus missed.
                    var hasEnumBug = !{toString: null}.propertyIsEnumerable('toString');
                    var nonEnumerableProps = ['valueOf', 'isPrototypeOf', 'toString',
                                              'propertyIsEnumerable', 'hasOwnProperty', 'toLocaleString'];
                    
                    function collectNonEnumProps(obj, keys) {
                    var nonEnumIdx = nonEnumerableProps.length;
                    var constructor = obj.constructor;
                    var proto = (_.isFunction(constructor) && constructor.prototype) || ObjProto;
                    
                    // Constructor is a special case.
                    var prop = 'constructor';
                    if (_.has(obj, prop) && !_.contains(keys, prop)) keys.push(prop);
                    
                    while (nonEnumIdx--) {
                    prop = nonEnumerableProps[nonEnumIdx];
                    if (prop in obj && obj[prop] !== proto[prop] && !_.contains(keys, prop)) {
                    keys.push(prop);
                    }
                    }
                    }
                    
                    // Retrieve the names of an object's own properties.
                    // Delegates to **ECMAScript 5**'s native `Object.keys`
                    _.keys = function(obj) {
                    if (!_.isObject(obj)) return [];
                    if (nativeKeys) return nativeKeys(obj);
                    var keys = [];
                    for (var key in obj) if (_.has(obj, key)) keys.push(key);
                    // Ahem, IE < 9.
                    if (hasEnumBug) collectNonEnumProps(obj, keys);
                    return keys;
                    };
                    
                    // Retrieve all the property names of an object.
                    _.allKeys = function(obj) {
                    if (!_.isObject(obj)) return [];
                    var keys = [];
                    for (var key in obj) keys.push(key);
                    // Ahem, IE < 9.
                    if (hasEnumBug) collectNonEnumProps(obj, keys);
                    return keys;
                    };
                    
                    // Retrieve the values of an object's properties.
                    _.values = function(obj) {
                    var keys = _.keys(obj);
                    var length = keys.length;
                    var values = Array(length);
                    for (var i = 0; i < length; i++) {
                    values[i] = obj[keys[i]];
                    }
                    return values;
                    };
                    
                    // Returns the results of applying the iteratee to each element of the object
                    // In contrast to _.map it returns an object
                    _.mapObject = function(obj, iteratee, context) {
                    iteratee = cb(iteratee, context);
                    var keys =  _.keys(obj),
                    length = keys.length,
                    results = {},
                    currentKey;
                    for (var index = 0; index < length; index++) {
                    currentKey = keys[index];
                    results[currentKey] = iteratee(obj[currentKey], currentKey, obj);
                    }
                    return results;
                    };
                    
                    // Convert an object into a list of `[key, value]` pairs.
                    _.pairs = function(obj) {
                    var keys = _.keys(obj);
                    var length = keys.length;
                    var pairs = Array(length);
                    for (var i = 0; i < length; i++) {
                    pairs[i] = [keys[i], obj[keys[i]]];
                    }
                    return pairs;
                    };
                    
                    // Invert the keys and values of an object. The values must be serializable.
                    _.invert = function(obj) {
                    var result = {};
                    var keys = _.keys(obj);
                    for (var i = 0, length = keys.length; i < length; i++) {
                    result[obj[keys[i]]] = keys[i];
                    }
                    return result;
                    };
                    
                    // Return a sorted list of the function names available on the object.
                    // Aliased as `methods`
                    _.functions = _.methods = function(obj) {
                    var names = [];
                    for (var key in obj) {
                    if (_.isFunction(obj[key])) names.push(key);
                    }
                    return names.sort();
                    };
                    
                    // Extend a given object with all the properties in passed-in object(s).
                    _.extend = createAssigner(_.allKeys);
                    
                    // Assigns a given object with all the own properties in the passed-in object(s)
                    // (https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)
                    _.extendOwn = _.assign = createAssigner(_.keys);
                    
                    // Returns the first key on an object that passes a predicate test
                    _.findKey = function(obj, predicate, context) {
                    predicate = cb(predicate, context);
                    var keys = _.keys(obj), key;
                    for (var i = 0, length = keys.length; i < length; i++) {
                    key = keys[i];
                    if (predicate(obj[key], key, obj)) return key;
                    }
                    };
                    
                    // Return a copy of the object only containing the whitelisted properties.
                    _.pick = function(object, oiteratee, context) {
                    var result = {}, obj = object, iteratee, keys;
                    if (obj == null) return result;
                    if (_.isFunction(oiteratee)) {
                    keys = _.allKeys(obj);
                    iteratee = optimizeCb(oiteratee, context);
                    } else {
                    keys = flatten(arguments, false, false, 1);
                    iteratee = function(value, key, obj) { return key in obj; };
                    obj = Object(obj);
                    }
                    for (var i = 0, length = keys.length; i < length; i++) {
                    var key = keys[i];
                    var value = obj[key];
                    if (iteratee(value, key, obj)) result[key] = value;
                    }
                    return result;
                    };
                    
                    // Return a copy of the object without the blacklisted properties.
                    _.omit = function(obj, iteratee, context) {
                    if (_.isFunction(iteratee)) {
                    iteratee = _.negate(iteratee);
                    } else {
                    var keys = _.map(flatten(arguments, false, false, 1), String);
                    iteratee = function(value, key) {
                    return !_.contains(keys, key);
                    };
                    }
                    return _.pick(obj, iteratee, context);
                    };
                    
                    // Fill in a given object with default properties.
                    _.defaults = createAssigner(_.allKeys, true);
                    
                    // Creates an object that inherits from the given prototype object.
                    // If additional properties are provided then they will be added to the
                    // created object.
                    _.create = function(prototype, props) {
                    var result = baseCreate(prototype);
                    if (props) _.extendOwn(result, props);
                    return result;
                    };
                    
                    // Create a (shallow-cloned) duplicate of an object.
                    _.clone = function(obj) {
                    if (!_.isObject(obj)) return obj;
                    return _.isArray(obj) ? obj.slice() : _.extend({}, obj);
                    };
                    
                    // Invokes interceptor with the obj, and then returns obj.
                    // The primary purpose of this method is to "tap into" a method chain, in
                    // order to perform operations on intermediate results within the chain.
                    _.tap = function(obj, interceptor) {
                    interceptor(obj);
                    return obj;
                    };
                    
                    // Returns whether an object has a given set of `key:value` pairs.
                    _.isMatch = function(object, attrs) {
                    var keys = _.keys(attrs), length = keys.length;
                    if (object == null) return !length;
                    var obj = Object(object);
                    for (var i = 0; i < length; i++) {
                    var key = keys[i];
                    if (attrs[key] !== obj[key] || !(key in obj)) return false;
                    }
                    return true;
                    };
                    
                    
                    // Internal recursive comparison function for `isEqual`.
                    var eq = function(a, b, aStack, bStack) {
                    // Identical objects are equal. `0 === -0`, but they aren't identical.
                    // See the [Harmony `egal` proposal](http://wiki.ecmascript.org/doku.php?id=harmony:egal).
                    if (a === b) return a !== 0 || 1 / a === 1 / b;
                    // A strict comparison is necessary because `null == undefined`.
                    if (a == null || b == null) return a === b;
                    // Unwrap any wrapped objects.
                    if (a instanceof _) a = a._wrapped;
                    if (b instanceof _) b = b._wrapped;
                    // Compare `[[Class]]` names.
                    var className = toString.call(a);
                    if (className !== toString.call(b)) return false;
                    switch (className) {
                    // Strings, numbers, regular expressions, dates, and booleans are compared by value.
                    case '[object RegExp]':
                    // RegExps are coerced to strings for comparison (Note: '' + /a/i === '/a/i')
                    case '[object String]':
                    // Primitives and their corresponding object wrappers are equivalent; thus, `"5"` is
                    // equivalent to `new String("5")`.
                    return '' + a === '' + b;
                    case '[object Number]':
                    // `NaN`s are equivalent, but non-reflexive.
                    // Object(NaN) is equivalent to NaN
                    if (+a !== +a) return +b !== +b;
                    // An `egal` comparison is performed for other numeric values.
                    return +a === 0 ? 1 / +a === 1 / b : +a === +b;
                    case '[object Date]':
                    case '[object Boolean]':
                    // Coerce dates and booleans to numeric primitive values. Dates are compared by their
                    // millisecond representations. Note that invalid dates with millisecond representations
                    // of `NaN` are not equivalent.
                    return +a === +b;
                    }
                    
                    var areArrays = className === '[object Array]';
                    if (!areArrays) {
                    if (typeof a != 'object' || typeof b != 'object') return false;
                    
                    // Objects with different constructors are not equivalent, but `Object`s or `Array`s
                    // from different frames are.
                    var aCtor = a.constructor, bCtor = b.constructor;
                    if (aCtor !== bCtor && !(_.isFunction(aCtor) && aCtor instanceof aCtor &&
                                             _.isFunction(bCtor) && bCtor instanceof bCtor)
                        && ('constructor' in a && 'constructor' in b)) {
                    return false;
                    }
                    }
                    // Assume equality for cyclic structures. The algorithm for detecting cyclic
                    // structures is adapted from ES 5.1 section 15.12.3, abstract operation `JO`.
                    
                    // Initializing stack of traversed objects.
                    // It's done here since we only need them for objects and arrays comparison.
                    aStack = aStack || [];
                    bStack = bStack || [];
                    var length = aStack.length;
                    while (length--) {
                    // Linear search. Performance is inversely proportional to the number of
                    // unique nested structures.
                    if (aStack[length] === a) return bStack[length] === b;
                    }
                    
                    // Add the first object to the stack of traversed objects.
                    aStack.push(a);
                    bStack.push(b);
                    
                    // Recursively compare objects and arrays.
                    if (areArrays) {
                    // Compare array lengths to determine if a deep comparison is necessary.
                    length = a.length;
                    if (length !== b.length) return false;
                    // Deep compare the contents, ignoring non-numeric properties.
                    while (length--) {
                    if (!eq(a[length], b[length], aStack, bStack)) return false;
                    }
                    } else {
                    // Deep compare objects.
                    var keys = _.keys(a), key;
                    length = keys.length;
                    // Ensure that both objects contain the same number of properties before comparing deep equality.
                    if (_.keys(b).length !== length) return false;
                    while (length--) {
                    // Deep compare each member
                    key = keys[length];
                    if (!(_.has(b, key) && eq(a[key], b[key], aStack, bStack))) return false;
                    }
                    }
                    // Remove the first object from the stack of traversed objects.
                    aStack.pop();
                    bStack.pop();
                    return true;
                    };
                    
                    // Perform a deep comparison to check if two objects are equal.
                    _.isEqual = function(a, b) {
                    return eq(a, b);
                    };
                    
                    // Is a given array, string, or object empty?
                    // An "empty" object has no enumerable own-properties.
                    _.isEmpty = function(obj) {
                    if (obj == null) return true;
                    if (isArrayLike(obj) && (_.isArray(obj) || _.isString(obj) || _.isArguments(obj))) return obj.length === 0;
                    return _.keys(obj).length === 0;
                    };
                    
                    // Is a given value a DOM element?
                    _.isElement = function(obj) {
                    return !!(obj && obj.nodeType === 1);
                    };
                    
                    // Is a given value an array?
                    // Delegates to ECMA5's native Array.isArray
                    _.isArray = nativeIsArray || function(obj) {
                    return toString.call(obj) === '[object Array]';
                    };
                    
                    // Is a given variable an object?
                    _.isObject = function(obj) {
                    var type = typeof obj;
                    return type === 'function' || type === 'object' && !!obj;
                    };
                    
                    // Add some isType methods: isArguments, isFunction, isString, isNumber, isDate, isRegExp, isError.
                    _.each(['Arguments', 'Function', 'String', 'Number', 'Date', 'RegExp', 'Error'], function(name) {
                           _['is' + name] = function(obj) {
                           return toString.call(obj) === '[object ' + name + ']';
                           };
                           });
                    
                    // Define a fallback version of the method in browsers (ahem, IE < 9), where
                    // there isn't any inspectable "Arguments" type.
                    if (!_.isArguments(arguments)) {
                    _.isArguments = function(obj) {
                    return _.has(obj, 'callee');
                    };
                    }
                    
                    // Optimize `isFunction` if appropriate. Work around some typeof bugs in old v8,
                    // IE 11 (#1621), and in Safari 8 (#1929).
                    if (typeof /./ != 'function' && typeof Int8Array != 'object') {
                    _.isFunction = function(obj) {
                    return typeof obj == 'function' || false;
                    };
                    }
                    
                    // Is a given object a finite number?
                    _.isFinite = function(obj) {
                    return isFinite(obj) && !isNaN(parseFloat(obj));
                    };
                    
                    // Is the given value `NaN`? (NaN is the only number which does not equal itself).
                    _.isNaN = function(obj) {
                    return _.isNumber(obj) && obj !== +obj;
                    };
                    
                    // Is a given value a boolean?
                    _.isBoolean = function(obj) {
                    return obj === true || obj === false || toString.call(obj) === '[object Boolean]';
                    };
                    
                    // Is a given value equal to null?
                    _.isNull = function(obj) {
                    return obj === null;
                    };
                    
                    // Is a given variable undefined?
                    _.isUndefined = function(obj) {
                    return obj === void 0;
                    };
                    
                    // Shortcut function for checking if an object has a given property directly
                    // on itself (in other words, not on a prototype).
                    _.has = function(obj, key) {
                    return obj != null && hasOwnProperty.call(obj, key);
                    };
                    
                    // Utility Functions
                    // -----------------
                    
                    // Run Underscore.js in *noConflict* mode, returning the `_` variable to its
                    // previous owner. Returns a reference to the Underscore object.
                    _.noConflict = function() {
                    root._ = previousUnderscore;
                    return this;
                    };
                    
                    // Keep the identity function around for default iteratees.
                    _.identity = function(value) {
                    return value;
                    };
                    
                    // Predicate-generating functions. Often useful outside of Underscore.
                    _.constant = function(value) {
                    return function() {
                    return value;
                    };
                    };
                    
                    _.noop = function(){};
                    
                    _.property = property;
                    
                    // Generates a function for a given object that returns a given property.
                    _.propertyOf = function(obj) {
                    return obj == null ? function(){} : function(key) {
                    return obj[key];
                    };
                    };
                    
                    // Returns a predicate for checking whether an object has a given set of
                    // `key:value` pairs.
                    _.matcher = _.matches = function(attrs) {
                    attrs = _.extendOwn({}, attrs);
                    return function(obj) {
                    return _.isMatch(obj, attrs);
                    };
                    };
                    
                    // Run a function **n** times.
                    _.times = function(n, iteratee, context) {
                    var accum = Array(Math.max(0, n));
                    iteratee = optimizeCb(iteratee, context, 1);
                    for (var i = 0; i < n; i++) accum[i] = iteratee(i);
                    return accum;
                    };
                    
                    // Return a random integer between min and max (inclusive).
                    _.random = function(min, max) {
                    if (max == null) {
                    max = min;
                    min = 0;
                    }
                    return min + Math.floor(Math.random() * (max - min + 1));
                    };
                    
                    // A (possibly faster) way to get the current timestamp as an integer.
                    _.now = Date.now || function() {
                    return new Date().getTime();
                    };
                    
                    // List of HTML entities for escaping.
                    var escapeMap = {
                    '&': '&amp;',
                    '<': '&lt;',
                    '>': '&gt;',
                    '"': '&quot;',
                    "'": '&#x27;',
                    '`': '&#x60;'
                    };
                    var unescapeMap = _.invert(escapeMap);
                    
                    // Functions for escaping and unescaping strings to/from HTML interpolation.
                    var createEscaper = function(map) {
                    var escaper = function(match) {
                    return map[match];
                    };
                    // Regexes for identifying a key that needs to be escaped
                    var source = '(?:' + _.keys(map).join('|') + ')';
                    var testRegexp = RegExp(source);
                    var replaceRegexp = RegExp(source, 'g');
                    return function(string) {
                    string = string == null ? '' : '' + string;
                    return testRegexp.test(string) ? string.replace(replaceRegexp, escaper) : string;
                    };
                    };
                    _.escape = createEscaper(escapeMap);
                    _.unescape = createEscaper(unescapeMap);
                    
                    // If the value of the named `property` is a function then invoke it with the
                    // `object` as context; otherwise, return it.
                    _.result = function(object, property, fallback) {
                    var value = object == null ? void 0 : object[property];
                    if (value === void 0) {
                    value = fallback;
                    }
                    return _.isFunction(value) ? value.call(object) : value;
                    };
                    
                    // Generate a unique integer id (unique within the entire client session).
                    // Useful for temporary DOM ids.
                    var idCounter = 0;
                    _.uniqueId = function(prefix) {
                    var id = ++idCounter + '';
                    return prefix ? prefix + id : id;
                    };
                    
                    // By default, Underscore uses ERB-style template delimiters, change the
                    // following template settings to use alternative delimiters.
                    _.templateSettings = {
                    evaluate    : /<%([\s\S]+?)%>/g,
                    interpolate : /<%=([\s\S]+?)%>/g,
                    escape      : /<%-([\s\S]+?)%>/g
                    };
                    
                    // When customizing `templateSettings`, if you don't want to define an
                    // interpolation, evaluation or escaping regex, we need one that is
                    // guaranteed not to match.
                    var noMatch = /(.)^/;
                    
                    // Certain characters need to be escaped so that they can be put into a
                    // string literal.
                    var escapes = {
                    "'":      "'",
                    '\\':     '\\',
                    '\r':     'r',
                    '\n':     'n',
                    '\u2028': 'u2028',
                    '\u2029': 'u2029'
                    };
                    
                    var escaper = /\\|'|\r|\n|\u2028|\u2029/g;
                    
                    var escapeChar = function(match) {
                    return '\\' + escapes[match];
                    };
                    
                    // JavaScript micro-templating, similar to John Resig's implementation.
                    // Underscore templating handles arbitrary delimiters, preserves whitespace,
                    // and correctly escapes quotes within interpolated code.
                    // NB: `oldSettings` only exists for backwards compatibility.
                    _.template = function(text, settings, oldSettings) {
                    if (!settings && oldSettings) settings = oldSettings;
                    settings = _.defaults({}, settings, _.templateSettings);
                    
                    // Combine delimiters into one regular expression via alternation.
                    var matcher = RegExp([
                                          (settings.escape || noMatch).source,
                                          (settings.interpolate || noMatch).source,
                                          (settings.evaluate || noMatch).source
                                          ].join('|') + '|$', 'g');
                    
                    // Compile the template source, escaping string literals appropriately.
                    var index = 0;
                    var source = "__p+='";
                    text.replace(matcher, function(match, escape, interpolate, evaluate, offset) {
                                 source += text.slice(index, offset).replace(escaper, escapeChar);
                                 index = offset + match.length;
                                 
                                 if (escape) {
                                 source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";
                                 } else if (interpolate) {
                                 source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";
                                 } else if (evaluate) {
                                 source += "';\n" + evaluate + "\n__p+='";
                                 }
                                 
                                 // Adobe VMs need the match returned to produce the correct offest.
                                 return match;
                                 });
                    source += "';\n";
                    
                    // If a variable is not specified, place data values in local scope.
                    if (!settings.variable) source = 'with(obj||{}){\n' + source + '}\n';
                    
                    source = "var __t,__p='',__j=Array.prototype.join," +
                    "print=function(){__p+=__j.call(arguments,'');};\n" +
                    source + 'return __p;\n';
                    
                    try {
                    var render = new Function(settings.variable || 'obj', '_', source);
                    } catch (e) {
                    e.source = source;
                    throw e;
                    }
                    
                    var template = function(data) {
                    return render.call(this, data, _);
                    };
                    
                    // Provide the compiled source as a convenience for precompilation.
                    var argument = settings.variable || 'obj';
                    template.source = 'function(' + argument + '){\n' + source + '}';
                    
                    return template;
                    };
                    
                    // Add a "chain" function. Start chaining a wrapped Underscore object.
                    _.chain = function(obj) {
                    var instance = _(obj);
                    instance._chain = true;
                    return instance;
                    };
                    
                    // OOP
                    // ---------------
                    // If Underscore is called as a function, it returns a wrapped object that
                    // can be used OO-style. This wrapper holds altered versions of all the
                    // underscore functions. Wrapped objects may be chained.
                    
                    // Helper function to continue chaining intermediate results.
                    var result = function(instance, obj) {
                    return instance._chain ? _(obj).chain() : obj;
                    };
                    
                    // Add your own custom functions to the Underscore object.
                    _.mixin = function(obj) {
                    _.each(_.functions(obj), function(name) {
                           var func = _[name] = obj[name];
                           _.prototype[name] = function() {
                           var args = [this._wrapped];
                           push.apply(args, arguments);
                           return result(this, func.apply(_, args));
                           };
                           });
                    };
                    
                    // Add all of the Underscore functions to the wrapper object.
                    _.mixin(_);
                    
                    // Add all mutator Array functions to the wrapper.
                    _.each(['pop', 'push', 'reverse', 'shift', 'sort', 'splice', 'unshift'], function(name) {
                           var method = ArrayProto[name];
                           _.prototype[name] = function() {
                           var obj = this._wrapped;
                           method.apply(obj, arguments);
                           if ((name === 'shift' || name === 'splice') && obj.length === 0) delete obj[0];
                           return result(this, obj);
                           };
                           });
                    
                    // Add all accessor Array functions to the wrapper.
                    _.each(['concat', 'join', 'slice'], function(name) {
                           var method = ArrayProto[name];
                           _.prototype[name] = function() {
                           return result(this, method.apply(this._wrapped, arguments));
                           };
                           });
                    
                    // Extracts the result from a wrapped and chained object.
                    _.prototype.value = function() {
                    return this._wrapped;
                    };
                    
                    // Provide unwrapping proxy for some methods used in engine operations
                    // such as arithmetic and JSON stringification.
                    _.prototype.valueOf = _.prototype.toJSON = _.prototype.value;
                    
                    _.prototype.toString = function() {
                    return '' + this._wrapped;
                    };
                    
                    // AMD registration happens at the end for compatibility with AMD loaders
                    // that may not enforce next-turn semantics on modules. Even though general
                    // practice for AMD registration is to be anonymous, underscore registers
                    // as a named module because, like jQuery, it is a base library that is
                    // popular enough to be bundled in a third party lib, but not be part of
                    // an AMD load request. Those cases could generate an error when an
                    // anonymous define() is called outside of a loader request.
                    if (typeof define === 'function' && define.amd) {
                    define('underscore', [], function() {
                           return _;
                           });
                    }
                    }.call(this));
                    });

  cordova.define("com.unarin.cordova.beacon.Q", function(require, exports, module) {
                 !function(a){if("function"==typeof bootstrap)bootstrap("promise",a);else if("object"==typeof exports)module.exports=a();else if("undefined"!=typeof ses){if(!ses.ok())return;ses.makeQ=a}else Q=a()}(function(){"use strict";function a(a){return function(){return V.apply(a,arguments)}}function b(a){return a===Object(a)}function c(a){return"[object StopIteration]"===bb(a)||a instanceof R}function d(a,b){if(O&&b.stack&&"object"==typeof a&&null!==a&&a.stack&&-1===a.stack.indexOf(cb)){for(var c=[],d=b;d;d=d.source)d.stack&&c.unshift(d.stack);c.unshift(a.stack);var f=c.join("\n"+cb+"\n");a.stack=e(f)}}function e(a){for(var b=a.split("\n"),c=[],d=0;d<b.length;++d){var e=b[d];h(e)||f(e)||!e||c.push(e)}return c.join("\n")}function f(a){return-1!==a.indexOf("(module.js:")||-1!==a.indexOf("(node.js:")}function g(a){var b=/at .+ \((.+):(\d+):(?:\d+)\)$/.exec(a);if(b)return[b[1],Number(b[2])];var c=/at ([^ ]+):(\d+):(?:\d+)$/.exec(a);if(c)return[c[1],Number(c[2])];var d=/.*@(.+):(\d+)$/.exec(a);return d?[d[1],Number(d[2])]:void 0}function h(a){var b=g(a);if(!b)return!1;var c=b[0],d=b[1];return c===Q&&d>=S&&gb>=d}function i(){if(O)try{throw new Error}catch(a){var b=a.stack.split("\n"),c=b[0].indexOf("@")>0?b[1]:b[2],d=g(c);if(!d)return;return Q=d[0],d[1]}}function j(a,b,c){return function(){return"undefined"!=typeof console&&"function"==typeof console.warn&&console.warn(b+" is deprecated, use "+c+" instead.",new Error("").stack),a.apply(a,arguments)}}function k(a){return r(a)?a:s(a)?B(a):A(a)}function l(){function a(a){b=a,f.source=a,X(c,function(b,c){U(function(){a.promiseDispatch.apply(a,c)})},void 0),c=void 0,d=void 0}var b,c=[],d=[],e=$(l.prototype),f=$(o.prototype);if(f.promiseDispatch=function(a,e,f){var g=W(arguments);c?(c.push(g),"when"===e&&f[1]&&d.push(f[1])):U(function(){b.promiseDispatch.apply(b,g)})},f.valueOf=function(){if(c)return f;var a=q(b);return r(a)&&(b=a),a},f.inspect=function(){return b?b.inspect():{state:"pending"}},k.longStackSupport&&O)try{throw new Error}catch(g){f.stack=g.stack.substring(g.stack.indexOf("\n")+1)}return e.promise=f,e.resolve=function(c){b||a(k(c))},e.fulfill=function(c){b||a(A(c))},e.reject=function(c){b||a(z(c))},e.notify=function(a){b||X(d,function(b,c){U(function(){c(a)})},void 0)},e}function m(a){if("function"!=typeof a)throw new TypeError("resolver must be a function.");var b=l();try{a(b.resolve,b.reject,b.notify)}catch(c){b.reject(c)}return b.promise}function n(a){return m(function(b,c){for(var d=0,e=a.length;e>d;d++)k(a[d]).then(b,c)})}function o(a,b,c){void 0===b&&(b=function(a){return z(new Error("Promise does not support operation: "+a))}),void 0===c&&(c=function(){return{state:"unknown"}});var d=$(o.prototype);if(d.promiseDispatch=function(c,e,f){var g;try{g=a[e]?a[e].apply(d,f):b.call(d,e,f)}catch(h){g=z(h)}c&&c(g)},d.inspect=c,c){var e=c();"rejected"===e.state&&(d.exception=e.reason),d.valueOf=function(){var a=c();return"pending"===a.state||"rejected"===a.state?d:a.value}}return d}function p(a,b,c,d){return k(a).then(b,c,d)}function q(a){if(r(a)){var b=a.inspect();if("fulfilled"===b.state)return b.value}return a}function r(a){return b(a)&&"function"==typeof a.promiseDispatch&&"function"==typeof a.inspect}function s(a){return b(a)&&"function"==typeof a.then}function t(a){return r(a)&&"pending"===a.inspect().state}function u(a){return!r(a)||"fulfilled"===a.inspect().state}function v(a){return r(a)&&"rejected"===a.inspect().state}function w(){db.length=0,eb.length=0,fb||(fb=!0)}function x(a,b){fb&&(eb.push(a),db.push(b&&"undefined"!=typeof b.stack?b.stack:"(no stack) "+b))}function y(a){if(fb){var b=Y(eb,a);-1!==b&&(eb.splice(b,1),db.splice(b,1))}}function z(a){var b=o({when:function(b){return b&&y(this),b?b(a):this}},function(){return this},function(){return{state:"rejected",reason:a}});return x(b,a),b}function A(a){return o({when:function(){return a},get:function(b){return a[b]},set:function(b,c){a[b]=c},"delete":function(b){delete a[b]},post:function(b,c){return null===b||void 0===b?a.apply(void 0,c):a[b].apply(a,c)},apply:function(b,c){return a.apply(b,c)},keys:function(){return ab(a)}},void 0,function(){return{state:"fulfilled",value:a}})}function B(a){var b=l();return U(function(){try{a.then(b.resolve,b.reject,b.notify)}catch(c){b.reject(c)}}),b.promise}function C(a){return o({isDef:function(){}},function(b,c){return I(a,b,c)},function(){return k(a).inspect()})}function D(a,b,c){return k(a).spread(b,c)}function E(a){return function(){function b(a,b){var g;if("undefined"==typeof StopIteration){try{g=d[a](b)}catch(h){return z(h)}return g.done?k(g.value):p(g.value,e,f)}try{g=d[a](b)}catch(h){return c(h)?k(h.value):z(h)}return p(g,e,f)}var d=a.apply(this,arguments),e=b.bind(b,"next"),f=b.bind(b,"throw");return e()}}function F(a){k.done(k.async(a)())}function G(a){throw new R(a)}function H(a){return function(){return D([this,J(arguments)],function(b,c){return a.apply(b,c)})}}function I(a,b,c){return k(a).dispatch(b,c)}function J(a){return p(a,function(a){var b=0,c=l();return X(a,function(d,e,f){var g;r(e)&&"fulfilled"===(g=e.inspect()).state?a[f]=g.value:(++b,p(e,function(d){a[f]=d,0===--b&&c.resolve(a)},c.reject,function(a){c.notify({index:f,value:a})}))},void 0),0===b&&c.resolve(a),c.promise})}function K(a){return p(a,function(a){return a=Z(a,k),p(J(Z(a,function(a){return p(a,T,T)})),function(){return a})})}function L(a){return k(a).allSettled()}function M(a,b){return k(a).then(void 0,void 0,b)}function N(a,b){return k(a).nodeify(b)}var O=!1;try{throw new Error}catch(P){O=!!P.stack}var Q,R,S=i(),T=function(){},U=function(){function a(){for(;b.next;){b=b.next;var c=b.task;b.task=void 0;var e=b.domain;e&&(b.domain=void 0,e.enter());try{c()}catch(g){if(f)throw e&&e.exit(),setTimeout(a,0),e&&e.enter(),g;setTimeout(function(){throw g},0)}e&&e.exit()}d=!1}var b={task:void 0,next:null},c=b,d=!1,e=void 0,f=!1;if(U=function(a){c=c.next={task:a,domain:f&&process.domain,next:null},d||(d=!0,e())},"undefined"!=typeof process&&process.nextTick)f=!0,e=function(){process.nextTick(a)};else if("function"==typeof setImmediate)e="undefined"!=typeof window?setImmediate.bind(window,a):function(){setImmediate(a)};else if("undefined"!=typeof MessageChannel){var g=new MessageChannel;g.port1.onmessage=function(){e=h,g.port1.onmessage=a,a()};var h=function(){g.port2.postMessage(0)};e=function(){setTimeout(a,0),h()}}else e=function(){setTimeout(a,0)};return U}(),V=Function.call,W=a(Array.prototype.slice),X=a(Array.prototype.reduce||function(a,b){var c=0,d=this.length;if(1===arguments.length)for(;;){if(c in this){b=this[c++];break}if(++c>=d)throw new TypeError}for(;d>c;c++)c in this&&(b=a(b,this[c],c));return b}),Y=a(Array.prototype.indexOf||function(a){for(var b=0;b<this.length;b++)if(this[b]===a)return b;return-1}),Z=a(Array.prototype.map||function(a,b){var c=this,d=[];return X(c,function(e,f,g){d.push(a.call(b,f,g,c))},void 0),d}),$=Object.create||function(a){function b(){}return b.prototype=a,new b},_=a(Object.prototype.hasOwnProperty),ab=Object.keys||function(a){var b=[];for(var c in a)_(a,c)&&b.push(c);return b},bb=a(Object.prototype.toString);R="undefined"!=typeof ReturnValue?ReturnValue:function(a){this.value=a};var cb="From previous event:";k.resolve=k,k.nextTick=U,k.longStackSupport=!1,k.defer=l,l.prototype.makeNodeResolver=function(){var a=this;return function(b,c){b?a.reject(b):a.resolve(arguments.length>2?W(arguments,1):c)}},k.Promise=m,k.promise=m,m.race=n,m.all=J,m.reject=z,m.resolve=k,k.passByCopy=function(a){return a},o.prototype.passByCopy=function(){return this},k.join=function(a,b){return k(a).join(b)},o.prototype.join=function(a){return k([this,a]).spread(function(a,b){if(a===b)return a;throw new Error("Can't join: not the same: "+a+" "+b)})},k.race=n,o.prototype.race=function(){return this.then(k.race)},k.makePromise=o,o.prototype.toString=function(){return"[object Promise]"},o.prototype.then=function(a,b,c){function e(b){try{return"function"==typeof a?a(b):b}catch(c){return z(c)}}function f(a){if("function"==typeof b){d(a,h);try{return b(a)}catch(c){return z(c)}}return z(a)}function g(a){return"function"==typeof c?c(a):a}var h=this,i=l(),j=!1;return U(function(){h.promiseDispatch(function(a){j||(j=!0,i.resolve(e(a)))},"when",[function(a){j||(j=!0,i.resolve(f(a)))}])}),h.promiseDispatch(void 0,"when",[void 0,function(a){var b,c=!1;try{b=g(a)}catch(d){if(c=!0,!k.onerror)throw d;k.onerror(d)}c||i.notify(b)}]),i.promise},k.when=p,o.prototype.thenResolve=function(a){return this.then(function(){return a})},k.thenResolve=function(a,b){return k(a).thenResolve(b)},o.prototype.thenReject=function(a){return this.then(function(){throw a})},k.thenReject=function(a,b){return k(a).thenReject(b)},k.nearer=q,k.isPromise=r,k.isPromiseAlike=s,k.isPending=t,o.prototype.isPending=function(){return"pending"===this.inspect().state},k.isFulfilled=u,o.prototype.isFulfilled=function(){return"fulfilled"===this.inspect().state},k.isRejected=v,o.prototype.isRejected=function(){return"rejected"===this.inspect().state};var db=[],eb=[],fb=!0;k.resetUnhandledRejections=w,k.getUnhandledReasons=function(){return db.slice()},k.stopUnhandledRejectionTracking=function(){w(),fb=!1},w(),k.reject=z,k.fulfill=A,k.master=C,k.spread=D,o.prototype.spread=function(a,b){return this.all().then(function(b){return a.apply(void 0,b)},b)},k.async=E,k.spawn=F,k["return"]=G,k.promised=H,k.dispatch=I,o.prototype.dispatch=function(a,b){var c=this,d=l();return U(function(){c.promiseDispatch(d.resolve,a,b)}),d.promise},k.get=function(a,b){return k(a).dispatch("get",[b])},o.prototype.get=function(a){return this.dispatch("get",[a])},k.set=function(a,b,c){return k(a).dispatch("set",[b,c])},o.prototype.set=function(a,b){return this.dispatch("set",[a,b])},k.del=k["delete"]=function(a,b){return k(a).dispatch("delete",[b])},o.prototype.del=o.prototype["delete"]=function(a){return this.dispatch("delete",[a])},k.mapply=k.post=function(a,b,c){return k(a).dispatch("post",[b,c])},o.prototype.mapply=o.prototype.post=function(a,b){return this.dispatch("post",[a,b])},k.send=k.mcall=k.invoke=function(a,b){return k(a).dispatch("post",[b,W(arguments,2)])},o.prototype.send=o.prototype.mcall=o.prototype.invoke=function(a){return this.dispatch("post",[a,W(arguments,1)])},k.fapply=function(a,b){return k(a).dispatch("apply",[void 0,b])},o.prototype.fapply=function(a){return this.dispatch("apply",[void 0,a])},k["try"]=k.fcall=function(a){return k(a).dispatch("apply",[void 0,W(arguments,1)])},o.prototype.fcall=function(){return this.dispatch("apply",[void 0,W(arguments)])},k.fbind=function(a){var b=k(a),c=W(arguments,1);return function(){return b.dispatch("apply",[this,c.concat(W(arguments))])}},o.prototype.fbind=function(){var a=this,b=W(arguments);return function(){return a.dispatch("apply",[this,b.concat(W(arguments))])}},k.keys=function(a){return k(a).dispatch("keys",[])},o.prototype.keys=function(){return this.dispatch("keys",[])},k.all=J,o.prototype.all=function(){return J(this)},k.allResolved=j(K,"allResolved","allSettled"),o.prototype.allResolved=function(){return K(this)},k.allSettled=L,o.prototype.allSettled=function(){return this.then(function(a){return J(Z(a,function(a){function b(){return a.inspect()}return a=k(a),a.then(b,b)}))})},k.fail=k["catch"]=function(a,b){return k(a).then(void 0,b)},o.prototype.fail=o.prototype["catch"]=function(a){return this.then(void 0,a)},k.progress=M,o.prototype.progress=function(a){return this.then(void 0,void 0,a)},k.fin=k["finally"]=function(a,b){return k(a)["finally"](b)},o.prototype.fin=o.prototype["finally"]=function(a){return a=k(a),this.then(function(b){return a.fcall().then(function(){return b})},function(b){return a.fcall().then(function(){throw b})})},k.done=function(a,b,c,d){return k(a).done(b,c,d)},o.prototype.done=function(a,b,c){var e=function(a){U(function(){if(d(a,f),!k.onerror)throw a;k.onerror(a)})},f=a||b||c?this.then(a,b,c):this;"object"==typeof process&&process&&process.domain&&(e=process.domain.bind(e)),f.then(void 0,e)},k.timeout=function(a,b,c){return k(a).timeout(b,c)},o.prototype.timeout=function(a,b){var c=l(),d=setTimeout(function(){b&&"string"!=typeof b||(b=new Error(b||"Timed out after "+a+" ms"),b.code="ETIMEDOUT"),c.reject(b)},a);return this.then(function(a){clearTimeout(d),c.resolve(a)},function(a){clearTimeout(d),c.reject(a)},c.notify),c.promise},k.delay=function(a,b){return void 0===b&&(b=a,a=void 0),k(a).delay(b)},o.prototype.delay=function(a){return this.then(function(b){var c=l();return setTimeout(function(){c.resolve(b)},a),c.promise})},k.nfapply=function(a,b){return k(a).nfapply(b)},o.prototype.nfapply=function(a){var b=l(),c=W(a);return c.push(b.makeNodeResolver()),this.fapply(c).fail(b.reject),b.promise},k.nfcall=function(a){var b=W(arguments,1);return k(a).nfapply(b)},o.prototype.nfcall=function(){var a=W(arguments),b=l();return a.push(b.makeNodeResolver()),this.fapply(a).fail(b.reject),b.promise},k.nfbind=k.denodeify=function(a){var b=W(arguments,1);return function(){var c=b.concat(W(arguments)),d=l();return c.push(d.makeNodeResolver()),k(a).fapply(c).fail(d.reject),d.promise}},o.prototype.nfbind=o.prototype.denodeify=function(){var a=W(arguments);return a.unshift(this),k.denodeify.apply(void 0,a)},k.nbind=function(a,b){var c=W(arguments,2);return function(){function d(){return a.apply(b,arguments)}var e=c.concat(W(arguments)),f=l();return e.push(f.makeNodeResolver()),k(d).fapply(e).fail(f.reject),f.promise}},o.prototype.nbind=function(){var a=W(arguments,0);return a.unshift(this),k.nbind.apply(void 0,a)},k.nmapply=k.npost=function(a,b,c){return k(a).npost(b,c)},o.prototype.nmapply=o.prototype.npost=function(a,b){var c=W(b||[]),d=l();return c.push(d.makeNodeResolver()),this.dispatch("post",[a,c]).fail(d.reject),d.promise},k.nsend=k.nmcall=k.ninvoke=function(a,b){var c=W(arguments,2),d=l();return c.push(d.makeNodeResolver()),k(a).dispatch("post",[b,c]).fail(d.reject),d.promise},o.prototype.nsend=o.prototype.nmcall=o.prototype.ninvoke=function(a){var b=W(arguments,1),c=l();return b.push(c.makeNodeResolver()),this.dispatch("post",[a,b]).fail(c.reject),c.promise},k.nodeify=N,o.prototype.nodeify=function(a){return a?void this.then(function(b){U(function(){a(null,b)})},function(b){U(function(){a(b)})}):this};var gb=i();return k});
                 });

                        cordova.define("com.unarin.cordova.beacon.LocationManager", function (require, exports, module) {
                            /*
                             Licensed to the Apache Software Foundation (ASF) under one
                             or more contributor license agreements.  See the NOTICE file
                             distributed with this work for additional information
                             regarding copyright ownership.  The ASF licenses this file
                             to you under the Apache License, Version 2.0 (the
                             "License"); you may not use this file except in compliance
                             with the License.  You may obtain a copy of the License at

                             http://www.apache.org/licenses/LICENSE-2.0

                             Unless required by applicable law or agreed to in writing,
                             software distributed under the License is distributed on an
                             "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
                             KIND, either express or implied.  See the License for the
                             specific language governing permissions and limitations
                             under the License.
                             */

                            var exec = require('cordova/exec');
                            var _ = require('com.unarin.cordova.beacon.underscorejs');
                            var Q = require('com.unarin.cordova.beacon.Q');

                            var Regions = require('com.unarin.cordova.beacon.Regions');
                            var Delegate = require('com.unarin.cordova.beacon.Delegate');

                            var Region = require('com.unarin.cordova.beacon.Region');
                            var CircularRegion = require('com.unarin.cordova.beacon.CircularRegion');
                            var BeaconRegion = require('com.unarin.cordova.beacon.BeaconRegion');

                            /**
                             * Creates an instance of the plugin.
                             *
                             * Important note: Creating multiple instances is expected to break the delegate
                             * callback mechanism, as the native layer can only handle one  callback ID at a
                             * time.
                             *
                             * @constructor {LocationManager}
                             */

                            function LocationManager() {
                                this.delegate = new Delegate();
                                this._registerDelegateCallbackId();

                                this.bindMethodContexts();

                            }

                            /**
                             * Binds the contexts of instance methods to the actual {LocationManager}
                             * instance.
                             * The goal of this is to make the caller code clean of binding calls when
                             * the promise functions are chained for example.
                             *
                             * @returns {undefined}
                             */
                            LocationManager.prototype.bindMethodContexts = function () {
                                this.disableDebugLogs = _.bind(this.disableDebugLogs, this);
                                this.enableDebugLogs = _.bind(this.enableDebugLogs, this);
                            };


                            LocationManager.prototype.getDelegate = function () {
                                return this.delegate;
                            };

                            LocationManager.prototype.setDelegate = function (newDelegate) {
                                if (!(newDelegate instanceof Delegate)) {
                                    console.error('newDelegate parameter has to be an instance of Delegate.');
                                    return;
                                }
                                this.delegate = newDelegate;

                                this.onDomDelegateReady();

                                return this.delegate;
                            };

                            /**
                             * Calls the method 'registerDelegateCallbackId' in the native layer which
                             * saves the callback ID for later use.
                             *
                             * The saved callback ID will be used when the native layer wants to notify
                             * the DOM asynchronously about an event of it's own, for example entering
                             * into a region.
                             *
                             * The same callback will be used for success and fail handling since the
                             * handling is the same.
                             *
                             * @returns {Q.Promise}
                             */
                            LocationManager.prototype._registerDelegateCallbackId = function () {
                                this.appendToDeviceLog('registerDelegateCallbackId()');
                                var d = Q.defer();

                                exec(_.bind(this._onDelegateCallback, this, d), _.bind(this._onDelegateCallback, this, d), "LocationManager",
                                    "registerDelegateCallbackId", []);

                                return d.promise;
                            };

                            /**
                             * Handles asynchronous calls from the native layer. In this context async
                             * means that message is not a response to a request of the DOM.
                             *
                             * @param {type} deferred {promise, resolve, reject} object.
                             *
                             * @param {type} pluginResult The PluginResult object constructed by the
                             * native layer as the payload of the message it wishes to send to the DOM
                             * asynchronously.
                             *
                             * @returns {undefined}
                             */
                            LocationManager.prototype._onDelegateCallback = function (deferred, pluginResult) {

                                this.appendToDeviceLog('_onDelegateCallback() ' + JSON.stringify(pluginResult));

                                if (pluginResult && _.isString(pluginResult['eventType'])) { // The native layer calling the DOM with a delegate event.
                                    this._mapDelegateCallback(pluginResult);
                                } else if (Q.isPending(deferred.promise)) { // The callback ID registration finished, runs only once.
                                    deferred.resolve();
                                } else { // The native layer calls back the delegate without specifying an event, coding error.
                                    console.error('Delegate registration promise is already been resolved, all subsequent callbacks should provide an "eventType" field.');
                                }
                            };

                            /**
                             * Routes async messages arriving from the native layer to the appropriate
                             * delegate methods.
                             *
                             * @param {type} pluginResult The PluginResult object constructed by the
                             * native layer as the payload of the message it wishes to send to the DOM
                             *
                             * @returns {undefined}
                             */
                            LocationManager.prototype._mapDelegateCallback = function (pluginResult) {
                                var eventType = pluginResult['eventType']; // the Objective-C selector's name

                                this.appendToDeviceLog('_mapDelegateCallback() found eventType ' + eventType);

                                if (_.isFunction(this.delegate[eventType])) {
                                    this.delegate[eventType](pluginResult);
                                } else {
                                    console.error('Delegate unable to handle eventType: ' + eventType);
                                }
                            };

                            /**
                             * Goes through the provided pre-processors *in order* adn applies them to
                             * [pluginResult].
                             * When the pre-processing is done, [resolve] is called with the pre-
                             * processed results. The raw input is discarded.
                             *
                             * @param {Function} resolve A callback which will get called upon completion.
                             *
                             * @param {Array} pluginResult The PluginResult object constructed by the
                             * native layer as the payload of the message it wishes to send to the DOM.
                             * This function expects the [pluginResult] to be an array of elements.
                             *
                             * @param {Array} preProcessors An array of {Function}s which will be applied
                             * to [pluginResult], in order.
                             *
                             * @returns {undefined}
                             */
                            LocationManager.prototype._preProcessorExecutor = function (resolve, pluginResult, preProcessors) {
                                _.each(preProcessors, function (preProcessor) {
                                    pluginResult = preProcessor(pluginResult);
                                });
                                resolve(pluginResult);
                            };

                            /**
                             * Wraps a Cordova exec call into a promise, allowing the client code to
                             * operate with those promises instead of callbacks.
                             *
                             * @param {String} method The name of the method in the native layer to be
                             * called by Cordova.
                             *
                             * @param {Array} commandArgs An array of arguments to be passed for the
                             * native layer. Defaults to an empty array if omitted.
                             *
                             * @param {Array} preProcessors An array of callback functions all of which
                             * takes an iterable (array) as it's parameter and applies a certain
                             * operation to the elements of that iterable.
                             *
                             * @returns {Q.Promise}
                             */
                            LocationManager.prototype._promisedExec = function (method, commandArgs, preProcessors) {
                                var self = this;
                                commandArgs = _.isArray(commandArgs) ? commandArgs : [];
                                preProcessors = _.isArray(preProcessors) ? preProcessors : [];
                                preProcessors = _.filter(preProcessors, function (preProcessor) {
                                    return _.isFunction(preProcessor);
                                });

                                var d = Q.defer();


                                var resolveWrap = function (pluginResult) {
                                    self._preProcessorExecutor(d.resolve, pluginResult, preProcessors);
                                };

                                exec(resolveWrap, d.reject, "LocationManager", method, commandArgs);

                                return d.promise;
                            };

                            /**
                             * Signals the native layer that the client side is ready to consume messages.
                             * Readiness here means that it has a {Delegate} set by the consumer javascript
                             * code.
                             * <p>
                             * The {LocationManager.setDelegate()} will implicitly call this method as well,
                             * therefore the only case when you have to call this manually is if you don't
                             * wish to specify a {Delegate} of yours.
                             * <p>
                             * The purpose of this signaling mechanism is to make the events work when the
                             * app is being woken up by the Operating System to give it a chance to handle
                             * region monitoring events for example.
                             * <p>
                             * If you don't set a {Delegate} and don't call this method manually, an error
                             * message get emitted in the native runtime and the DOM as well after a certain
                             * period of time.
                             *
                             * @return {Q.Promise} Returns a promise which is resolved as soon as the
                             * native layer acknowledged the request and started to send events.
                             */
                            LocationManager.prototype.onDomDelegateReady = function () {
                                return this._promisedExec('onDomDelegateReady', [], []);
                            };

                            /**
                             * Determines if bluetooth is switched on, according to the native layer.
                             * @returns {Q.Promise} Returns a promise which is resolved with a {Boolean}
                             * indicating whether bluetooth is active.
                             */
                            LocationManager.prototype.isBluetoothEnabled = function () {
                                return this._promisedExec('isBluetoothEnabled', [], []);
                            };

                            /**
                             * Enables Bluetooth using the native Layer. (ANDROID ONLY)
                             *
                             * @returns {Q.Promise} Returns a promise which is resolved when Bluetooth
                             * could be enabled. If not, the promise will be rejected with an error.
                             */
                            LocationManager.prototype.enableBluetooth = function () {
                                return this._promisedExec('enableBluetooth', [], []);
                            };

                            /**
                             * Disables Bluetooth using the native Layer. (ANDROID ONLY)
                             *
                             * @returns {Q.Promise} Returns a promise which is resolved when Bluetooth
                             * could be enabled. If not, the promise will be rejected with an error.
                             */
                            LocationManager.prototype.disableBluetooth = function () {
                                return this._promisedExec('disableBluetooth', [], []);
                            };

                            /**
                             * Start monitoring the specified region.
                             *
                             * If a region of the same type with the same identifier is already being
                             * monitored for this application,
                             * it will be removed from monitoring. For circular regions, the region
                             * monitoring service will prioritize
                             * regions by their size, favoring smaller regions over larger regions.
                             *
                             * This is done asynchronously and may not be immediately reflected in monitoredRegions.
                             *
                             * @param {Region} region An instance of {Region} which will be monitored
                             * by the operating system.
                             *
                             * @return {Q.Promise} Returns a promise which is resolved as soon as the
                             * native layer acknowledged the dispatch of the monitoring request.
                             */
                            LocationManager.prototype.startMonitoringForRegion = function (region) {
                                Regions.checkRegionType(region);
                                return this._promisedExec('startMonitoringForRegion', [region], []);
                            };

                            /**
                             * Stop monitoring the specified region.  It is valid to call
                             * stopMonitoringForRegion: for a region that was registered for monitoring
                             * with a different location manager object, during this or previous
                             * launches of your application.
                             *
                             * This is done asynchronously and may not be immediately reflected in monitoredRegions.
                             *
                             * @param {Region} region An instance of {Region} which will be monitored
                             * by the operating system.
                             *
                             * @return {Q.Promise} Returns a promise which is resolved as soon as the
                             * native layer acknowledged the dispatch of the request to stop monitoring.
                             */
                            LocationManager.prototype.stopMonitoringForRegion = function (region) {
                                Regions.checkRegionType(region);
                                return this._promisedExec('stopMonitoringForRegion', [region], []);
                            };

                            /**
                             * Request state the for specified region. When result is ready
                             * didDetermineStateForRegion is triggered. This can be any region,
                             * also those which is not currently monitored.
                             *
                             * This is done asynchronously and may not be immediately reflected in monitoredRegions.
                             *
                             * @param {Region} region An instance of {Region} which will be monitored
                             * by the operating system.
                             *
                             * @return {Q.Promise} Returns a promise which is resolved as soon as the
                             * native layer acknowledged the dispatch of the request to stop monitoring.
                             */
                            LocationManager.prototype.requestStateForRegion = function (region) {
                                Regions.checkRegionType(region);
                                return this._promisedExec('requestStateForRegion', [region], []);
                            };

                            /**
                             * Start ranging the specified beacon region.
                             *
                             * If a region of the same type with the same identifier is already being
                             * monitored for this application, it will be removed from monitoring.
                             *
                             * This is done asynchronously and may not be immediately reflected in rangedRegions.
                             *
                             * @param {Region} region An instance of {BeaconRegion} which will be monitored
                             * by the operating system.
                             *
                             * @return {Q.Promise} Returns a promise which is resolved as soon as the
                             * native layer acknowledged the dispatch of the monitoring request.
                             */
                            LocationManager.prototype.startRangingBeaconsInRegion = function (region) {
                                if (!Regions.isBeaconRegion(region))
                                    throw new TypeError('The region parameter has to be an instance of BeaconRegion');

                                return this._promisedExec('startRangingBeaconsInRegion', [region], []);
                            };

                            /**
                             * Stop ranging the specified region.  It is valid to call
                             * stopMonitoringForRegion: for a region that was registered for ranging
                             * with a different location manager object, during this or previous
                             * launches of your application.
                             *
                             * This is done asynchronously and may not be immediately reflected in rangedRegions.
                             *
                             * @param {Region} region An instance of {BeaconRegion} which will be monitored
                             * by the operating system.
                             *
                             * @return {Q.Promise} Returns a promise which is resolved as soon as the
                             * native layer acknowledged the dispatch of the request to stop monitoring.
                             */
                            LocationManager.prototype.stopRangingBeaconsInRegion = function (region) {
                                if (!Regions.isBeaconRegion(region))
                                    throw new TypeError('The region parameter has to be an instance of BeaconRegion');

                                return this._promisedExec('stopRangingBeaconsInRegion', [region], []);
                            };

                            /**
                             * Queries the native layer to determine the current authorization in effect.
                             *
                             * @returns {Q.Promise} Returns a promise which is resolved with the
                             * requested authorization status.
                             */
                            LocationManager.prototype.getAuthorizationStatus = function () {
                                return this._promisedExec('getAuthorizationStatus', [], []);
                            };

                            /**
                             * For iOS 8 and above only. The permission model has changed by Apple in iOS 8, making it necessary for apps to
                             * explicitly request permissions via methods like these:
                             * <a href="https://developer.apple.com/library/prerelease/iOS/documentation/CoreLocation/Reference/CLLocationManager_Class/index.html#//apple_ref/occ/instm/CLLocationManager/requestWhenInUseAuthorization">requestWhenInUseAuthorization</a>
                             * <a href="https://developer.apple.com/library/prerelease/iOS/documentation/CoreLocation/Reference/CLLocationManager_Class/index.html#//apple_ref/occ/instm/CLLocationManager/requestAlwaysAuthorization">requestAlwaysAuthorization</a>
                             *
                             * If you are using this plugin on Android devices only, you will never have to use this, nor {@code requestAlwaysAuthorization}
                             * @returns {Q.Promise}
                             */
                            LocationManager.prototype.requestWhenInUseAuthorization = function () {
                                return this._promisedExec('requestWhenInUseAuthorization', [], []);
                            };

                            /**
                             * See the docuemntation of {@code requestWhenInUseAuthorization} for further details.
                             *
                             * @returns {Q.Promise}
                             */
                            LocationManager.prototype.requestAlwaysAuthorization = function () {
                                return this._promisedExec('requestAlwaysAuthorization', [], []);
                            };

                            /**
                             *
                             * @returns {Q.Promise} Returns a promise which is resolved with an {Array}
                             * of {Region} instances that are being monitored by the native layer.
                             */
                            LocationManager.prototype.getMonitoredRegions = function () {
                                var preProcessors = [Regions.fromJsonArray];
                                return this._promisedExec('getMonitoredRegions', [], preProcessors);
                            };

                            /**
                             *
                             * @returns {Q.Promise} Returns a promise which is resolved with an {Array}
                             * of {Region} instances that are being ranged by the native layer.
                             */
                            LocationManager.prototype.getRangedRegions = function () {
                                var preProcessors = [Regions.fromJsonArray];
                                return this._promisedExec('getRangedRegions', [], preProcessors);
                            };

                            /**
                             * Determines if ranging is available or not, according to the native layer.
                             * @returns {Q.Promise} Returns a promise which is resolved with a {Boolean}
                             * indicating whether ranging is available or not.
                             */
                            LocationManager.prototype.isRangingAvailable = function () {
                                return this._promisedExec('isRangingAvailable', [], []);
                            };

                            /**
                             * Determines if region type is supported or not, according to the native layer.
                             *
                             * @param {Region} region An instance of {Region} which will be checked
                             * by the operating system.
                             *
                             * @returns {Q.Promise} Returns a promise which is resolved with a {Boolean}
                             * indicating whether the region type is supported or not.
                             */
                            LocationManager.prototype.isMonitoringAvailableForClass = function (region) {
                                Regions.checkRegionType(region);
                                return this._promisedExec('isMonitoringAvailableForClass', [region], []);
                            };

                            /**
                             * Start advertising the specified region.
                             *
                             * If a region a different identifier is already being advertised for
                             * this application, it will be replaced with the new identifier.
                             *
                             * This call will accept a valid beacon even when no BlueTooth is available,
                             * and will start when BlueTooth is powered on. See {Delegate.}
                             *
                             * @param {Region} region An instance of {Region} which will be advertised
                             * by the operating system.
                             * @param {Integer} measuredPower: Optional parameter, if left empty, the device will
                             * use it's own default value.
                             *
                             * @return {Q.Promise} Returns a promise which is resolved as soon as the
                             * native layer acknowledged the dispatch of the advertising request.
                             */
                            LocationManager.prototype.startAdvertising = function (region, measuredPower) {
                                Regions.checkRegionType(region);
                                if (measuredPower)
                                    return this._promisedExec('startAdvertising', [region, measuredPower], []);
                                else
                                    return this._promisedExec('startAdvertising', [region], []);
                            };

                            /**
                             * Stop advertising as a beacon.
                             *
                             * This is done asynchronously and may not be immediately reflected in isAdvertising.
                             *
                             * @return {Q.Promise} Returns a promise which is resolved as soon as the
                             * native layer acknowledged the dispatch of the request to stop advertising.
                             */
                            LocationManager.prototype.stopAdvertising = function () {
                                return this._promisedExec('stopAdvertising', [], []);
                            };

                            /**
                             * Determines if advertising is available or not, according to the native layer.
                             * @returns {Q.Promise} Returns a promise which is resolved with a {Boolean}
                             * indicating whether advertising is available or not.
                             */
                            LocationManager.prototype.isAdvertisingAvailable = function () {
                                return this._promisedExec('isAdvertisingAvailable', [], []);
                            };

                            /**
                             * Determines if advertising is currently active, according to the native layer.
                             * @returns {Q.Promise} Returns a promise which is resolved with a {Boolean}
                             * indicating whether advertising is active.
                             */
                            LocationManager.prototype.isAdvertising = function () {
                                return this._promisedExec('isAdvertising', [], []);
                            };

                            /**
                             * Disables debug logging in the native layer. Use this method if you want
                             * to prevent this plugin from writing to the device logs.
                             *
                             * @returns {Q.Promise} Returns a promise which is resolved as soon as the
                             * native layer has set the logging level accordingly.
                             */
                            LocationManager.prototype.disableDebugLogs = function () {
                                return this._promisedExec('disableDebugLogs', [], []);
                            };

                            /**
                             * Enables the posting of debug notifications in the native layer. Use this method if you want
                             * to allow the plugin the posting local notifications.
                             * This can be very helpful when debugging how to apps behave when launched into the background.
                             *
                             * @returns {Q.Promise} Returns a promise which is resolved as soon as the
                             * native layer has set the flag to enabled.
                             */
                            LocationManager.prototype.enableDebugNotifications = function () {
                                return this._promisedExec('enableDebugNotifications', [], []);
                            };
                            /**
                             * Disables the posting of debug notifications in the native layer. Use this method if you want
                             * to prevent the plugin from posting local notifications.
                             *
                             * @returns {Q.Promise} Returns a promise which is resolved as soon as the
                             * native layer has set the flag to disabled.
                             */
                            LocationManager.prototype.disableDebugNotifications = function () {
                                return this._promisedExec('disableDebugNotifications', [], []);
                            };

                            /**
                             * Enables debug logging in the native layer. Use this method if you want
                             * a debug the inner workings of this plugin.
                             *
                             * @returns {Q.Promise} Returns a promise which is resolved as soon as the
                             * native layer has set the logging level accordingly.
                             */
                            LocationManager.prototype.enableDebugLogs = function () {
                                return this._promisedExec('enableDebugLogs', [], []);
                            };

                            /**
                             * Appends the provided [message] to the device logs.
                             * Note: If debug logging is turned off, this won't do anything.
                             *
                             * @param {String} message The message to append to the device logs.
                             *
                             * @returns {Q.Promise} Returns a promise which is resolved with the log
                             * message received by the native layer for appending. The returned message
                             * is expected to be equivalent to the one provided in the original call.
                             */
                            LocationManager.prototype.appendToDeviceLog = function (message) {
                                return this._promisedExec('appendToDeviceLog', [message], []);
                            };

                            var locationManager = new LocationManager();
                            locationManager.Regions = Regions;
                            locationManager.Region = Region;
                            locationManager.CircularRegion = CircularRegion;
                            locationManager.BeaconRegion = BeaconRegion;
                            locationManager.Delegate = Delegate;

                            module.exports.LocationManager = LocationManager;
                            module.exports.locationManager = locationManager;


                        });

                        cordova.define("com.unarin.cordova.beacon.Delegate", function (require, exports, module) {
                            /*
                             Licensed to the Apache Software Foundation (ASF) under one
                             or more contributor license agreements.  See the NOTICE file
                             distributed with this work for additional information
                             regarding copyright ownership.  The ASF licenses this file
                             to you under the Apache License, Version 2.0 (the
                             "License"); you may not use this file except in compliance
                             with the License.  You may obtain a copy of the License at

                             http://www.apache.org/licenses/LICENSE-2.0

                             Unless required by applicable law or agreed to in writing,
                             software distributed under the License is distributed on an
                             "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
                             KIND, either express or implied.  See the License for the
                             specific language governing permissions and limitations
                             under the License.
                             */

                            var _ = require('com.unarin.cordova.beacon.underscorejs');
                            var Regions = require('com.unarin.cordova.beacon.Regions');


                            /**
                             * Instances of this class are delegates between the {@link LocationManager} and
                             * the code that consumes the messages generated on in the native layer.
                             *
                             * @example
                             *
                             * var delegate = new cordova.plugins.LocationManager.Delegate();
                             *
                             * delegate.didDetermineStateForRegion = function(region) {
 *      console.log('didDetermineState:forRegion: ' + JSON.stringify(region));
 * };
                             *
                             * delegate.didStartMonitoringForRegion = function (region) {
 *      console.log('didStartMonitoringForRegion: ' + JSON.stringify(region));
 * }
                             *
                             * @returns {Delegate} An instance of the type {@type Delegate}.
                             */
                            function Delegate() {
                            };

                            /**
                             * A bunch of static pre-processor methods to parse and unmarshal the region objects.
                             */
                            Delegate.didDetermineStateForRegion = function (pluginResult) {
                                pluginResult.region = Regions.fromJson(pluginResult.region);
                            };

                            Delegate.monitoringDidFailForRegionWithError = function (pluginResult) {
                                pluginResult.region = Regions.fromJson(pluginResult.region);
                            };

                            Delegate.didStartMonitoringForRegion = function (pluginResult) {
                                pluginResult.region = Regions.fromJson(pluginResult.region);
                            };

                            Delegate.didExitRegion = function (pluginResult) {
                                pluginResult.region = Regions.fromJson(pluginResult.region);
                            };

                            Delegate.didEnterRegion = function (pluginResult) {
                                pluginResult.region = Regions.fromJson(pluginResult.region);
                            };

                            Delegate.didRangeBeaconsInRegion = function (pluginResult) {
                                pluginResult.region = Regions.fromJson(pluginResult.region);
                            };

                            Delegate.peripheralManagerDidStartAdvertising = function (pluginResult) {
                                pluginResult.region = Regions.fromJson(pluginResult.region);
                            };

                            Delegate.peripheralManagerDidUpdateState = function (pluginResult) {

                            };

                            Delegate.didChangeAuthorizationStatus = function (status) {

                            };

                            Delegate.safeTraceLogging = function (message) {
                                if (!_.isString(message)) {
                                    return;
                                }
                                try {
                                    cordova.plugins.locationManager.appendToDeviceLog(message);
                                } catch (e) {
                                    console.error('Fail in safeTraceLogging()' + e.message, e);
                                }
                            };


                            /**
                             * Default implementations of the Delegate methods which are noop.
                             */

                            Delegate.prototype.didDetermineStateForRegion = function () {
                                Delegate.safeTraceLogging('DEFAULT didDetermineStateForRegion()');
                            };

                            Delegate.prototype.monitoringDidFailForRegionWithError = function () {
                                Delegate.safeTraceLogging('DEFAULT monitoringDidFailForRegionWithError()');
                            };

                            Delegate.prototype.didStartMonitoringForRegion = function () {
                                Delegate.safeTraceLogging('DEFAULT didStartMonitoringForRegion()');
                            };

                            Delegate.prototype.didExitRegion = function () {
                                Delegate.safeTraceLogging('DEFAULT didExitRegion()');
                            };

                            Delegate.prototype.didEnterRegion = function () {
                                Delegate.safeTraceLogging('DEFAULT didEnterRegion()');
                            };

                            Delegate.prototype.didRangeBeaconsInRegion = function () {
                                Delegate.safeTraceLogging('DEFAULT didRangeBeaconsInRegion()');
                            };


                            Delegate.prototype.peripheralManagerDidStartAdvertising = function () {
                                Delegate.safeTraceLogging('DEFAULT peripheralManagerDidStartAdvertising()');
                            };

                            Delegate.prototype.peripheralManagerDidUpdateState = function () {
                                Delegate.safeTraceLogging('DEFAULT peripheralManagerDidUpdateState()');
                            };

                            Delegate.prototype.didChangeAuthorizationStatus = function () {
                                Delegate.safeTraceLogging('DEFAULT didChangeAuthorizationStatus()');
                            };


                            module.exports = Delegate;

                        });

                        cordova.define("com.unarin.cordova.beacon.Region", function (require, exports, module) {
                            /*
                             Licensed to the Apache Software Foundation (ASF) under one
                             or more contributor license agreements.  See the NOTICE file
                             distributed with this work for additional information
                             regarding copyright ownership.  The ASF licenses this file
                             to you under the Apache License, Version 2.0 (the
                             "License"); you may not use this file except in compliance
                             with the License.  You may obtain a copy of the License at

                             http://www.apache.org/licenses/LICENSE-2.0

                             Unless required by applicable law or agreed to in writing,
                             software distributed under the License is distributed on an
                             "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
                             KIND, either express or implied.  See the License for the
                             specific language governing permissions and limitations
                             under the License.
                             */

                            var _ = require('com.unarin.cordova.beacon.underscorejs');

                            /**
                             * Base class for different types of regions that the [LocationManager] can monitor.
                             * @constructor
                             *
                             * @param {String} identifier A unique identifier to associate with the region object.
                             *    You use this identifier to differentiate regions within your application.
                             *    This value must not be nil.
                             */
                            function Region(identifier) {
                                Region.checkIdentifier(identifier);
                                this.identifier = identifier;
                            };

                            Region.checkIdentifier = function (identifier) {
                                if (!_.isString(identifier)) {
                                    throw new TypeError(identifier + ' is not a String.');
                                }
                                if (_.isEmpty(identifier)) {
                                    throw new Error("'identifier' cannot be an empty string.");
                                }
                            };

                            module.exports = Region;


                        });

                        cordova.define("com.unarin.cordova.beacon.Regions", function (require, exports, module) {
                            /*
                             Licensed to the Apache Software Foundation (ASF) under one
                             or more contributor license agreements.  See the NOTICE file
                             distributed with this work for additional information
                             regarding copyright ownership.  The ASF licenses this file
                             to you under the Apache License, Version 2.0 (the
                             "License"); you may not use this file except in compliance
                             with the License.  You may obtain a copy of the License at

                             http://www.apache.org/licenses/LICENSE-2.0

                             Unless required by applicable law or agreed to in writing,
                             software distributed under the License is distributed on an
                             "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
                             KIND, either express or implied.  See the License for the
                             specific language governing permissions and limitations
                             under the License.
                             */

                            var _ = require('com.unarin.cordova.beacon.underscorejs');
                            var CircularRegion = require('com.unarin.cordova.beacon.CircularRegion');
                            var BeaconRegion = require('com.unarin.cordova.beacon.BeaconRegion');
                            var Region = require('com.unarin.cordova.beacon.Region');


                            /**
                             * Utility class for un-marshalling {Region} instances from JSON objects,
                             * checking their types.
                             *
                             * @type {Regions}
                             */
                            function Regions() {
                            };


                            /**
                             * Creates an instance of {@link Region} from the provided map of parameters.
                             *
                             * @param jsonMap The JSON object which is used to construct the return value.
                             *
                             * @returns {Region} Returns a subclass of {@link Region}.
                             */
                            Regions.fromJson = function (jsonMap) {

                                var typeName = jsonMap.typeName;
                                if (!_.isString(typeName) || _.isEmpty(typeName)) {
                                    throw new TypeError('jsonMap need to have a key "typeName"');
                                }

                                var identifier = jsonMap.identifier;

                                var region = null;
                                if (typeName === 'CircularRegion') {

                                    var latitude = jsonMap.latitude;
                                    var longitude = jsonMap.longitude;
                                    var radius = jsonMap.radius;
                                    region = new CircularRegion(identifier, latitude, longitude, radius);

                                } else if (typeName === 'BeaconRegion') {

                                    var uuid = jsonMap.uuid;
                                    var major = jsonMap.major;
                                    var minor = jsonMap.minor;
                                    region = new BeaconRegion(identifier, uuid, major, minor);
                                } else {
                                    console.error('Unrecognized Region typeName: ' + typeName);
                                }

                                return region;
                            };

                            Regions.fromJsonArray = function (jsonArray) {
                                if (!_.isArray(jsonArray)) {
                                    throw new TypeError('Expected an array.');
                                }

                                var result = [];
                                _.each(jsonArray, function (region) {
                                    result.push(Regions.fromJson(region));
                                });
                                return result;
                            };

                            /**
                             * Validates the input parameter [region] to be an instance of {Region}.
                             *
                             * @param {Region} region : The object that's type will be checked.
                             *
                             * @returns {undefined} If [region] is an instance of {Region}, throws otherwise.
                             */
                            Regions.checkRegionType = function (region) {
                                var regionHasInvalidType = !(region instanceof Region);
                                if (regionHasInvalidType) {
                                    throw new TypeError('The region parameter has to be an instance of Region');
                                }
                            };

                            Regions.isRegion = function (object) {
                                return object instanceof Region;
                            };

                            Regions.isCircularRegion = function (object) {
                                return object instanceof CircularRegion;
                            };

                            Regions.isBeaconRegion = function (object) {
                                return object instanceof BeaconRegion;
                            };


                            module.exports = Regions;


                        });

                        cordova.define("com.unarin.cordova.beacon.CircularRegion", function (require, exports, module) {
                            /*
                             Licensed to the Apache Software Foundation (ASF) under one
                             or more contributor license agreements.  See the NOTICE file
                             distributed with this work for additional information
                             regarding copyright ownership.  The ASF licenses this file
                             to you under the Apache License, Version 2.0 (the
                             "License"); you may not use this file except in compliance
                             with the License.  You may obtain a copy of the License at

                             http://www.apache.org/licenses/LICENSE-2.0

                             Unless required by applicable law or agreed to in writing,
                             software distributed under the License is distributed on an
                             "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
                             KIND, either express or implied.  See the License for the
                             specific language governing permissions and limitations
                             under the License.
                             */

                            var _ = require('com.unarin.cordova.beacon.underscorejs');
                            var Region = require('com.unarin.cordova.beacon.Region');


                            /**
                             * @constructor
                             *
                             * @param {String} identifier @see {CLRegion}
                             *
                             * @param {Number} latitude The latitude in degrees. Positive values indicate
                             * latitudes north of the equator. Negative values indicate latitudes south of
                             * the equator.
                             *
                             * @param {Number} longitude The longitude in degrees. Measurements are relative
                             * to the zero meridian, with positive values extending east of the meridian
                             * and negative values extending west of the meridian.
                             *
                             * @param {Number} radius A distance measurement (in meters) from an existing location.
                             *
                             * @throws {TypeError} if any of the parameters are passed with an incorrect type.
                             * @throws {Error} if any of the parameters are containing invalid values.
                             */

                            function CircularRegion(identifier, latitude, longitude, radius) {
                                // Call the parent constructor, making sure (using Function#call)
                                // that "this" is set correctly during the call
                                Region.call(this, identifier);

                                CircularRegion.checkLatitude(latitude);
                                CircularRegion.checkLongitude(longitude);
                                CircularRegion.checkRadius(radius);

                                this.latitude = latitude;
                                this.longitude = longitude;
                                this.radius = radius;

                                // {String} typeName A String holding the name of the Objective-C type that the value
                                //    this will get converted to once the data is in the Objective-C runtime.
                                this.typeName = 'CircularRegion';

                            };

                            // Create a CircularRegion.prototype object that inherits from Region.prototype.
                            // Note: A common error here is to use "new Region()" to create the
                            // CircularRegion.prototype. That's incorrect for several reasons, not least
                            // that we don't have anything to give Region for the "identifier"
                            // argument. The correct place to call Region is above, where we call
                            // it from CircularRegion.
                            CircularRegion.prototype = Object.create(Region.prototype);

                            // Set the "constructor" property to refer to CircularRegion
                            CircularRegion.prototype.constructor = CircularRegion;


                            CircularRegion.checkRadius = function (radius) {
                                if (_.isNaN(radius)) {
                                    throw new TypeError("'radius' is not a number.");
                                }
                                if (!_.isNumber(radius)) {
                                    throw new TypeError("'radius'" + radius + ' is not number.');
                                }
                                if (radius < 0) {
                                    throw new Error("'radius' has to be a finite, positive number.");
                                }
                            };

                            CircularRegion.checkLongitude = function (longitude) {
                                if (_.isNaN(longitude)) {
                                    throw new TypeError("'longitude' is not a number.");
                                }
                                if (!_.isNumber(longitude)) {
                                    throw new TypeError(longitude + ' is not a Number.');
                                }

                                if (longitude > 180 || longitude < -180) {
                                    throw new Error(longitude + ' has to be a value between -180 and +180');
                                }
                            };

                            CircularRegion.checkLatitude = function (latitude) {
                                if (_.isNaN(latitude)) {
                                    throw new TypeError("'latitude' is not a number.");
                                }
                                if (!_.isNumber(latitude)) {
                                    throw new TypeError(latitude + ' is not a Number.');
                                }

                                if (latitude > 90 || latitude < -90) {
                                    throw new Error(latitude + ' has to be a value between -90 and +90');
                                }
                            };

                            module.exports = CircularRegion;


                        });

                        cordova.define("com.unarin.cordova.beacon.BeaconRegion", function (require, exports, module) {
                            /*
                             Licensed to the Apache Software Foundation (ASF) under one
                             or more contributor license agreements.  See the NOTICE file
                             distributed with this work for additional information
                             regarding copyright ownership.  The ASF licenses this file
                             to you under the Apache License, Version 2.0 (the
                             "License"); you may not use this file except in compliance
                             with the License.  You may obtain a copy of the License at

                             http://www.apache.org/licenses/LICENSE-2.0

                             Unless required by applicable law or agreed to in writing,
                             software distributed under the License is distributed on an
                             "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
                             KIND, either express or implied.  See the License for the
                             specific language governing permissions and limitations
                             under the License.
                             */

                            var _ = require('com.unarin.cordova.beacon.underscorejs');
                            var Region = require('com.unarin.cordova.beacon.Region');

                            /**
                             * Constructor for {CLBeaconRegion}.
                             *
                             * @param {String} identifier @see {CLRegion}
                             *
                             * @param {String} uuid The proximity ID of the beacon being targeted.
                             * This value must not be blank nor invalid as a UUID.
                             *
                             * @param {Number} major The major value that you use to identify one or more beacons.
                             * @param {Number} minor The minor value that you use to identify a specific beacon.
                             *
                             * @param {BOOL} notifyEntryStateOnDisplay
                             *
                             * @returns {BeaconRegion} An instance of {BeaconRegion}.
                             */
                            function BeaconRegion(identifier, uuid, major, minor, notifyEntryStateOnDisplay) {
                                // Call the parent constructor, making sure (using Function#call)
                                // that "this" is set correctly during the call
                                Region.call(this, identifier);

                                BeaconRegion.checkUuid(uuid);
                                BeaconRegion.checkMajorOrMinor(major);
                                BeaconRegion.checkMajorOrMinor(minor);

                                this.uuid = uuid;
                                this.major = major;
                                this.minor = minor;
                                this.notifyEntryStateOnDisplay = notifyEntryStateOnDisplay;

                                this.typeName = 'BeaconRegion';
                            };

                            // Create a BeaconRegion.prototype object that inherits from Region.prototype.
                            // Note: A common error here is to use "new Region()" to create the
                            // BeaconRegion.prototype. That's incorrect for several reasons, not least
                            // that we don't have anything to give Region for the "identifier"
                            // argument. The correct place to call Region is above, where we call
                            // it from BeaconRegion.
                            BeaconRegion.prototype = Object.create(Region.prototype);

                            // Set the "constructor" property to refer to BeaconRegion
                            BeaconRegion.prototype.constructor = BeaconRegion;

                            BeaconRegion.isValidUuid = function (uuid) {
                                var uuidValidatorRegex = this.getUuidValidatorRegex();
                                return uuid.match(uuidValidatorRegex) != null;
                            };

                            BeaconRegion.getUuidValidatorRegex = function () {
                                return /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i;
                            };

                            BeaconRegion.checkUuid = function (uuid) {
                                if (!BeaconRegion.isValidUuid(uuid)) {
                                    throw new TypeError(uuid + ' is not a valid UUID');
                                }
                            };

                            BeaconRegion.checkMajorOrMinor = function (majorOrMinor) {
                                if (!_.isUndefined(majorOrMinor)) {
                                    if (!_.isFinite(majorOrMinor)) {
                                        throw new TypeError(majorOrMinor + ' is not a finite value');
                                    }

                                    if (majorOrMinor > BeaconRegion.U_INT_16_MAX_VALUE ||
                                        majorOrMinor < BeaconRegion.U_INT_16_MIN_VALUE) {
                                        throw new TypeError(majorOrMinor + ' is out of valid range of values.');
                                    }
                                }
                            };

                            BeaconRegion.U_INT_16_MAX_VALUE = (1 << 16) - 1;
                            BeaconRegion.U_INT_16_MIN_VALUE = 0;


                            module.exports = BeaconRegion;


                        });

    cordova.define("com.darktalker.cordova.screenshot.screenshot", function(require, exports, module){
        /*
         *  This code is adapted from the work of Michael Nachbaur
         *  by Simon Madine of The Angry Robot Zombie Factory
         *   - Converted to Cordova 1.6.1 by Josemando Sobral.
         *   - Converted to Cordova 2.0.0 by Simon MacDonald
         *  2012-07-03
         *  MIT licensed
         */
        var exec = require('cordova/exec'), formats = ['png','jpg'];
        module.exports = {
            save:function(callback,format,quality, filename) {
                format = (format || 'png').toLowerCase();
                filename = filename || 'screenshot_'+Math.round((+(new Date()) + Math.random()));
                if(formats.indexOf(format) === -1){
                    return callback && callback(new Error('invalid format '+format));
                }
                quality = typeof(quality) !== 'number'?100:quality;
                exec(function(res){
                    callback && callback(null,res);
                }, function(error){
                    callback && callback(error);
                }, "Screenshot", "saveScreenshot", [format, quality, filename]);
            },

            URI:function(callback, quality){
                quality = typeof(quality) !== 'number'?100:quality;
                exec(function(res){
                    callback && callback(null, res);
                }, function(error){
                    callback && callback(error);
                }, "Screenshot", "getScreenshotAsURI", [quality]);

            }
        };
    });

    cordova.define("com.archibus.plugins.connection.Connection", function(require, exports, module) {
        var exec = require('cordova/exec');


        var Connection = {
            isConnected: function(url, success, error) {
                exec(success, error, 'Connection','isConnected', [url]);
            }
        };

        module.exports = Connection;
    });




//insert packages here

                        require('cordova/init');

                    })();
