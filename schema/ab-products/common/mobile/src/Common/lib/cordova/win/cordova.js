(function () {
    var deviceReady = new Event('deviceready');
    document.dispatchEvent(deviceReady);

})();

(function (exports) {

    var device = {};

    //TODO: Generate UUID
    device.uuid = '12345';

    exports.device = device;


})(window);

(function (exports) {

    // The Mobile Client app runs inside of the Crosswalk wrapper. We don't have a good way to
    // retreive the version number of the actual Mobile Client app.
    // We set the version number here.
    var version = '2.0.0';
    var appVersion = {};

    appVersion.getAppVersion = function (success) {
        success(version);
    };

    exports.AppVersion = appVersion;

})(window);