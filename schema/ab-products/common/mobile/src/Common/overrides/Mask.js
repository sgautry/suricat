Ext.define('Common.overrides.Mask', {
    override: 'Ext.Mask',

    onEvent: function (e) {
        var controller = arguments[arguments.length - 1],
            floorplans;

        if (controller.info.eventName === 'painted') {
            floorplans = Ext.ComponentQuery.query('floorplanpanel');

            floorplans.forEach(function (floorplan) {
                var svg,
                    viewBox;

                if (Ext.os.is.ios && Ext.os.version.major > 10 && floorplan && Ext.isFunction(floorplan.getSvgComponent)) {
                    svg = floorplan.getSvgComponent().getSvg();

                    if (svg && !svg.empty()) {
                        viewBox = svg.attr('viewBox');
                        setTimeout(function () {
                            svg.attr('viewBox', viewBox);
                        }, 20);
                    }
                }
            });
        }

        if (controller.info.eventName === 'tap') {
            this.fireEvent('tap', this, e);
            return false;
        }

        if (e && e.stopEvent) {
            e.stopEvent();
        }

        return false;
    }
});