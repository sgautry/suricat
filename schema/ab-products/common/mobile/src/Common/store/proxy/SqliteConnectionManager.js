/**
 * Manages the only SQLite database connection: creates the database connection if it does not exist; stores the
 * database connection in a field.
 *
 * @author Valery Tydykov
 * @author Jeff Martin
 * @since 21.1
 *
 */
Ext.define('Common.store.proxy.SqliteConnectionManager', {
    alternateClassName: ['SqliteConnectionManager'],
    requires: [
        'Common.env.Feature',
        'Common.util.ConfigFileManager'
    ],

    singleton: true,

    /**
     * @cfg {String} name Name of database
     */
    name: 'ARCHIBUS',

    /**
     * @cfg {String} Version database version. If different than current, use updatedb event to update database.
     */
    version: '1.0',

    /**
     * @cfg {String} description Description of the database.
     */
    description: 'ARCHIBUS Mobile Applications Database',

    /**
     * @cfg {String} Size Max storage size in bytes.
     */
    size: 50 * 1024 * 1024,

    /**
     * {Database} The one and only Sqlite database connection.
     */
    connection: null,

    /**
     *
     * Creates the only database connection if it does not exist. Stores the database connection in a field.
     * The start up sequence requires that database connection is created after the Phonegap library is loaded.
     *
     * @return {Object} database connection.
     */
    getConnection: function () {
        var dbErrorMsg = LocaleManager.getLocalizedString('Error opening database: {0}', 'Common.store.proxy.SqliteConnectionManager'),
            userName = ConfigFileManager.username,
            dbName = 'ARCHIBUS',

            onSuccess = function () {
                Log.log('Database is open (callback) ' + dbName, 'debug');
            },

            onError = function () {
                Ext.Msg.alert('', Ext.String.format(dbErrorMsg));
            };


        if (Common.env.Feature.isNative) {
            if (Ext.isEmpty(userName)) {
                ConfigFileManager.setUserDatabase();
                console.log('Using unassigned database');
                dbName = ConfigFileManager.dbMap.unassigned;
            } else {
                dbName = ConfigFileManager.dbMap[userName];
            }
        }


        Common.store.proxy.SqliteConnectionManager.name = dbName;
        if (Common.store.proxy.SqliteConnectionManager.connection === null) {
            if (Common.env.Feature.isNative) {
                Log.log('Open Database [' + dbName + ']', 'verbose');
                Common.store.proxy.SqliteConnectionManager.connection = Common.store.proxy.SqliteConnectionManager.getNativeDatabaseConnection(onSuccess, onError);
            } else {
                Common.store.proxy.SqliteConnectionManager.connection = openDatabase(Common.store.proxy.SqliteConnectionManager.name,
                    Common.store.proxy.SqliteConnectionManager.version, Common.store.proxy.SqliteConnectionManager.description,
                    Common.store.proxy.SqliteConnectionManager.size);
            }
        }


        return Common.store.proxy.SqliteConnectionManager.connection;
    },

    invalidateConnection: function () {
        Common.store.proxy.SqliteConnectionManager.connection = null;
    },

    getNativeDatabaseConnection: function (onSuccess, onError) {
        var version = window.sqlitePlugin.sqliteFeatures.version;

        if (version && version === '3.0') {
            return window.sqlitePlugin.openDatabase({
                name: Common.store.proxy.SqliteConnectionManager.name,
                location: 'default'
            }, onSuccess, onError);
        } else {
            return window.sqlitePlugin.openDatabase(Common.store.proxy.SqliteConnectionManager.name,
                Common.store.proxy.SqliteConnectionManager.version, Common.store.proxy.SqliteConnectionManager.description, -1, onSuccess, onError);
        }
    }

});
