/**
 * Provides functionality to update schema of the store: updates model and table of the store according to the
 * server-side TableDef, if cached TableDef does not match.
 * Uses DWR service to get TableDef from the server.
 * Holds information required for mapping to the server-side table: serverTableName.
 *
 * @author Valery Tydykov
 * @author Jeff Martin
 * @since 21.1
 */
Ext.define('Common.store.sync.SchemaUpdaterStore', {
    extend: 'Common.store.sync.SqliteStore',
    requires: [
        'Common.service.MobileSyncServiceAdapter',
        'Common.data.ModelGenerator',
        'Common.controller.EventBus',
        'Common.util.ImageData',
        'Common.promise.util.TableDef',
        'Common.device.File',
        'Common.service.document.Document',
        'Common.document.DocumentManager'
    ],

    /**
     * @property {String} serverTableName The name of the server side table associated with the store
     */
    serverTableName: null,

    /**
     * @property {Object} clientDocuments Caches the document information for documents stored in the client database. The
     * clientDocuments cache is updated during the sync upload operation.
     */
    clientDocuments: {},

    config: {
        /**
         * @cfg {Number} syncRecordsPageSize The number of records to retrieve at one time from the Web Central server.
         */
        syncRecordsPageSize: 5000,

        /**
         * @cfg {Boolean} The document data is populated in the associated [doc]_contents fields when true. Set to false
         * to use the On Demand Document Download feature
         */
        includeDocumentDataInSync: true,


        documentConfig: {},

        /**
         * @cfg {Boolean} deleteAllRecordsOnSync When true all records for the table are deleted in the client database
         * during the sync. Set to false to enable the Partial Sync mode.
         */
        deleteAllRecordsOnSync: true,

        /**
         *  @cfg {Object/Object[]} restriction The restriction clause sent to the server during synchronization
         *  The restriction can be specified as a restriction object or an array of restriction objects
         *
         *  Example
         *
         *      restriction:  {
         *           fieldName: "status"
         *           operation: "EQUALS"
         *           tableName: "survey"
         *           value: "Issued"
         *      };
         *
         *  Multiple restrictions can be applied using an object array. The restrictions are combined using an AND
         *  operator. The relativeOps property can be used to use a different operator.
         *
         *  Applying multiple restrictions combined with an OR operator.
         *
         *     restriction: [
         *            {
         *                tableName: 'afm_activity_params',
         *                fieldName: 'applies_to',
         *                operation: 'EQUALS',
         *                value: 'Mobile',
         *                relativeOperation : 'OR'
         *            },
         *            {
         *                tableName: 'afm_activity_params',
         *                fieldName: 'param_id',
         *                operation: 'EQUALS',
         *                value: 'BudgetCurrency',
         *                relativeOperation : 'OR'
         *            }
         *      ]
         *
         *  This restriction generates the SQL statement of
         *      SELECT FROM afm_activity_params WHERE applies_to = 'Mobile' OR param_id = 'BudgetCurrency'
         *
         *  Restriction Properties:
         *
         *  The operation property can have the following values:
         *
         *     Operation         SQL Operator
         *
         *     EQUALS                 =
         *     NOT_EQUALS             <>
         *     LIKE                   LIKE
         *     NOT_LIKE               NOT_LIKE
         *     GT                     >
         *     LT                     <
         *     IS_NULL                IS NULL
         *     IS_NOT_NULL            IS NOT NULL
         *
         *  The relativeOperation property can have one of the following values
         *
         *     relativeOperation       SQL
         *
         *     AND                     WHERE field1 = 'A' AND field2 = 'B'
         *     OR                      WHERE field1 = 'A' OR field2 = 'B'
         *     AND_BRACKET             WHERE (field1 = 'A' AND field2 = 'B')
         *     OR_BRACKET              WHERE (filed1 = 'A' OR field2 = 'B')
         *
         */
        restriction: null,

        /**
         * @cfg {Boolean} timestampDownload true to enable the timestamp download feature for the store.
         * When false all records are downloaded and replaced on the client.
         */
        timestampDownload: false

    },

    applyDocumentConfig: function (config) {
        var me = this,
            documentConfig = config || {},
            defaultDocumentConfig = {
                documentTable: me.serverTableName,
                documentTableKeys: me.inventoryKeyNames,
                clientTableKeys: []
            };


        if (me.modelHasDocumentFields()) {
            documentConfig = Ext.merge(defaultDocumentConfig, documentConfig);
        }

        return documentConfig;

    },

    /**
     * Returns true if the model assigned to this store contains document fields.
     * Handles the case where the model does not extend Common.data.Model.
     * @returns {*|Object|Ext.data.Model}
     */
    modelHasDocumentFields: function () {
        var me = this,
            model = me.getModel();

        return (model && Ext.isFunction(model.hasDocumentFields) && model.hasDocumentFields());
    },

    /**
     * Converts the restriction configuration into an restriction.clauses array
     * @param {Object/Object[]} restriction
     * @returns {Object}
     */
    applyRestriction: function (restriction) {
        var clauses = [],
            restrictionObject = null;

        if (restriction) {
            // If the restriction is an Array add each restriction object to
            // the clauses array
            if (Ext.isArray(restriction)) {
                clauses = restriction;
            } else {
                clauses.push(restriction);
            }
            restrictionObject = {};
            restrictionObject.clauses = clauses;
        }
        return restrictionObject;
    },

    /**
     * Updates model and table of the store according to the server-side TableDef, if cached TableDef
     * does not match. Commits the cached tableDefs to the database.
     *
     * @param {Object} tableDef The TableDef object
     * @returns {Promise}
     */
    updateIfNotModelAndTable: function (tableDef) {
        var me = this,
            cachedTableDefObject = Common.promise.util.TableDef.getTableDefObject(me.serverTableName);

        if (cachedTableDefObject === null || !TableDef.compareTableDefObject(cachedTableDefObject, tableDef)) {
            // If this is a sync table we need to get a list of fields to include in the model from the AppPreferences store
            if (me.getDynamicModel()) {
                Common.data.ModelGenerator.generateModel(me.getModel(), me.serverTableName, tableDef);
            }
            return me.createOrModifyTable(me.getProxy().getTable(), me.getModel())
                .then(function () {
                    return Common.promise.util.TableDef.saveTableDef(tableDef);
                });

        } else {
            // Nothing to do, just resolve and return.
            return Promise.resolve();
        }
    },

    /**
     *
     * @deprecated 22.1
     * @param onCompleted
     * @param scope
     */
    getTableDefFromServer: function (onCompleted, scope) {
        MobileSyncServiceAdapter.getTableDefAsync(this.serverTableName, onCompleted, scope);
    },


    /* Paging Functions */

    getPrimaryKeyValuesFromFieldObject: function (fieldObject, primaryKeyFields) {
        var me = this,
            primaryKeyValues = {};

        Ext.each(fieldObject.fieldValues, function (field) {
            Ext.each(primaryKeyFields, function (primaryKeyField) {
                if (field.fieldName === primaryKeyField) {
                    primaryKeyValues[field.fieldName] = field.fieldValue;
                }
            }, me);
        }, me);

        return primaryKeyValues;
    },

    retrieveRecords: function (restriction, pageSize, timestamp) {
        var me = this,
            includeDocumentData = me.getIncludeDocumentDataInSync(),
            fieldDefFieldNames = me.getFieldNamesFromTableDef(),
            fieldsToSync = Ext.Array.intersect(fieldDefFieldNames, me.serverFieldNames);

        if (timestamp > 0) {
            return Common.service.MobileSyncServiceAdapter.retrieveModifiedRecords(me.serverTableName, fieldsToSync, restriction, pageSize, includeDocumentData, timestamp);
        } else {
            return Common.service.MobileSyncServiceAdapter.retrievePagedRecords(me.serverTableName, fieldsToSync, restriction, pageSize, includeDocumentData);
        }
    },

    getFieldNamesFromTableDef: function () {
        var me = this;

        var fieldDefFieldNames = me.tableDef.fieldDefs.map(function (fieldDef) {
            return fieldDef.name;
        });

        return fieldDefFieldNames;
    },

    createRestrictionUsingLastRecord: function (records, storeRestriction) {
        var me = this,
            lastRecord,
            primaryKeyFields,
            restriction;

        if (records.length === 0) {
            return null;
        }

        lastRecord = records[records.length - 1];
        primaryKeyFields = Common.util.TableDef.getPrimaryKeyFieldsFromTableDef(me.tableDef);
        restriction = me.generateRestrictionClause(lastRecord, primaryKeyFields, storeRestriction);

        return restriction;
    },

    generateRestrictionClause: function (lastRecord, primaryKeyFields, storeRestriction) {
        var me = this,
            tableName = me.serverTableName,
            restriction = {},
            clauses = [],
            pagingClauses = [],
            primaryKeyValues = me.getPrimaryKeyValuesFromFieldObject(lastRecord, primaryKeyFields);

        if (storeRestriction !== null) {
            clauses = Ext.Array.clone(storeRestriction.clauses);
        }

        // TODO: The restriction generator only handles tables with up to 3 primary key fields. Update function
        // to work with N primary key values.

        Ext.each(primaryKeyFields, function (keyField, index, allItems) {
            if (index === 0) {
                pagingClauses.push({
                    tableName: tableName,
                    fieldName: keyField,
                    operation: 'GT',
                    value: primaryKeyValues[keyField],
                    relativeOperation: 'OR'
                });
            }

            if (index === 1) {
                pagingClauses.push({
                    tableName: tableName,
                    fieldName: allItems[0],
                    operation: 'EQUALS',
                    value: primaryKeyValues[allItems[0]],
                    relativeOperation: 'OR_BRACKET'
                });
                pagingClauses.push({
                    tableName: tableName,
                    fieldName: keyField,
                    operation: 'GT',
                    value: primaryKeyValues[keyField],
                    relativeOperation: 'AND'
                });
            }

            if (index === 2) {
                pagingClauses.push({
                    tableName: tableName,
                    fieldName: allItems[0],
                    operation: 'EQUALS',
                    value: primaryKeyValues[allItems[0]],
                    relativeOperation: 'OR_BRACKET'
                });
                pagingClauses.push({
                    tableName: tableName,
                    fieldName: allItems[1],
                    operation: 'EQUALS',
                    value: primaryKeyValues[allItems[1]],
                    relativeOperation: 'AND'
                });
                pagingClauses.push({
                    tableName: tableName,
                    fieldName: allItems[2],
                    operation: 'GT',
                    value: primaryKeyValues[allItems[2]],
                    relativeOperation: 'AND'
                });
            }
        });

        restriction.clauses = pagingClauses.concat(clauses);

        return restriction;
    },

    convertRecordsFromServer: function (records) {
        var me = this,
            convertedRecords,

            convertRecord = function (record) {
                var data = {};

                Ext.each(record.fieldValues, function (field) {
                    data[field.fieldName] = field.fieldValue;
                }, me);

                // Reset the mob_is_changed field on checkout
                if (data.hasOwnProperty('mob_is_changed')) {
                    data.mob_is_changed = 0;
                }

                return data;
            };

        convertedRecords = Ext.Array.map(records, convertRecord);
        return Promise.resolve(convertedRecords);
    },


    // TODO: remove after removing dependency from DocumentManager
    getModelTableName: function () {
        var me = this,
            model = me.getModel(),
            modelName,
            table = 'UNKNOWN';

        if (model) {
            modelName = model.modelName;
            table = modelName.slice(modelName.lastIndexOf('.') + 1);
        }

        return table;
    },

    disableStoreEvents: function () {
        var me = this,
            model = me.getModel();
        // Disable store events during sync to prevent UI updates
        me.suspendEvents();
        model.prototype.disableEditHandling = true;
    },

    enableStoreEvents: function (error) {
        var me = this,
            model = me.getModel();

        me.resumeEvents(true);
        model.prototype.disableEditHandling = false;

        if (error) {
            return Promise.reject(error);
        } else {
            return Promise.resolve();
        }
    },

    /**
     * Retrieves the document version and file information from the client database.
     * @returns {Promise} A Promise resolved to an object containing the document version and file location.
     * The returned object is indexed using the client record identifier.
     * An empty object is resolved if the model contains no document fields.
     */
    getClientDocuments: function () {
        var me = this,
            model = me.getModel(),
            clientTableKeys = me.getDocumentConfig().clientTableKeys;

        var generateSql = function (documentFields) {
            var docSelectFields = [],
                whereClause = [],
                selectFields,
                uniqueId = clientTableKeys.length > 0 ? clientTableKeys : model.getUniqueIdentifier(),
                tableName = model.getTableName();

            documentFields.forEach(function (docField) {
                docSelectFields.push(docField + '_file');
                docSelectFields.push(docField + '_version');
                whereClause.push(docField + ' IS NOT NULL');
            });

            selectFields = uniqueId.concat(docSelectFields);

            return 'SELECT ' + selectFields.join(',') + ' FROM ' + tableName + ' WHERE ' + whereClause.join(' OR ');
        };

        var generateIndex = function (record) {
            var index = [],
                uniqueId = clientTableKeys.length > 0 ? clientTableKeys : model.getUniqueIdentifier();

            uniqueId.forEach(function (id) {
                index.push(record[id]);
            });

            return index.join('|');
        };

        if (Ext.isFunction(model.hasDocumentFields) && model.hasDocumentFields()) {
            return new Promise(function (resolve, reject) {
                var db = SqliteConnectionManager.getConnection(),
                    documentFields = model.getDocumentFieldNames(),
                    sql = generateSql(documentFields),
                    clientDocuments = {};

                db.transaction(function (tx) {
                    tx.executeSql(sql, null, function (tx, records) {
                            var result,
                                idx,
                                i;

                            for (i = 0; i < records.rows.length; i++) {
                                result = records.rows.item(i);
                                idx = generateIndex(result);
                                clientDocuments[idx] = result;
                                clientDocuments[idx].docFields = documentFields;
                            }
                            resolve(clientDocuments);
                        },
                        function (tx, error) {
                            reject(error.message);
                        });
                });
            });
        } else {
            return Promise.resolve({});
        }
    },

    restoreClientDocuments: function (clientDocuments) {
        var me = this,
            model = me.getModel(),
            clientTableKeys = me.getDocumentConfig().clientTableKeys,
            uniqueId = clientTableKeys.length > 0 ? clientTableKeys : model.getUniqueIdentifier(),
            statement = {};

        var generateSql = function (record) {
            var values = [],
                fields = [],
                whereClause = [],
                p;

            for (p in record) {
                if (!uniqueId.includes(p) && p !== 'docFields') {
                    fields.push(p + '=?');
                    values.push(record[p]);
                }
            }

            uniqueId.forEach(function (id) {
                whereClause.push(id + "='" + record[id] + "'");
            });

            statement.sql = 'UPDATE ' + model.getTableName() + ' SET ' + fields.join(',') + ' WHERE ' + whereClause.join(' AND ');
            statement.values = values;

            return statement;
        };


        var executeSql = function (tx, document) {
            var statement = generateSql(document);
            return new Promise(function (resolve, reject) {
                tx.executeSql(statement.sql, statement.values, resolve, function (tx, error) {
                    reject(error.message);
                });
            });
        };

        return Promise.resolve()
            .then(function () {
                var db = SqliteConnectionManager.getConnection();
                db.transaction(function (tx) {
                    var clientDocumentArray = Object.keys(clientDocuments).map(function (doc) {
                        return clientDocuments[doc];
                    });

                    return Promise.all(clientDocumentArray.map(function (document) {
                        executeSql(tx, document);
                    }));

                });
            });
    },

    restoreDocumentData: function () {
        var me = this,
            model = me.getModel(),
            clientTableKeys,
            uniqueId;

        var getDocumentData = function () {
            var db = SqliteConnectionManager.getConnection(),
                sql = "SELECT doc_field,doc_contents,pkey from TmpDocs where doc_contents is not null and doc_contents != ''";

            return new Promise(function (resolve, reject) {
                db.transaction(function (tx) {
                    tx.executeSql(sql, null, function (tx, result) {
                        var i,
                            resultArray = [];

                        for (i = 0; i < result.rows.length; i++) {
                            resultArray.push(result.rows.item(i));
                        }
                        resolve(resultArray);
                    }, function (tx, error) {
                        reject(error.message);
                    });
                });
            });
        };

        var generateSql = function (docData) {
            var whereClause = [],
                statement = {},
                pkeys = docData.pkey.split('|');

            uniqueId.forEach(function (id, idx) {
                whereClause.push(id + "='" + pkeys[idx] + "'");
            });

            statement.sql = 'UPDATE ' + model.getTableName() + ' SET ' + docData.doc_field + '_contents=?' + ' WHERE ' + whereClause.join(' AND ');
            statement.values = [docData.doc_contents];

            return statement;
        };

        var updateDocumentContents = function (docData) {
            var statement = generateSql(docData);
            var db = SqliteConnectionManager.getConnection();

            return new Promise(function (resolve, reject) {
                db.transaction(function (tx) {
                    tx.executeSql(statement.sql, statement.values, resolve, function (tx, error) {
                        reject(error.message);
                    });
                });
            });
        };

        var deleteTmpDocs = function () {
            var db = SqliteConnectionManager.getConnection();
            return new Promise(function (resolve, reject) {
                db.transaction(function (tx) {
                    tx.executeSql('DELETE FROM TmpDocs', null, resolve, function (tx, error) {
                        reject(error.message);
                    });
                });
            });
        };

        if (GlobalParameters.useFileStorage() || !me.modelHasDocumentFields()) {
            return Promise.resolve();
        } else {
            clientTableKeys = me.getDocumentConfig().clientTableKeys;
            uniqueId = clientTableKeys.length > 0 ? clientTableKeys : model.getUniqueIdentifier();
            return getDocumentData()
                .then(function (resultArray) {
                    return Promise.all(resultArray.map(updateDocumentContents));
                })
                .then(function () {
                    return deleteTmpDocs();
                });
        }
    },

    getDocumentUrls: function (documentsToDownload) {

        var getUrls = function (docs) {
            var p = Promise.resolve(),
                fields = [];

            docs.forEach(function (field) {
                p = p.then(function () {
                    return Common.service.document.Document.retrieveDocumentURL(field.keys_object, field.table_name, field.field_name, field.doc_file)
                        .then(function (url) {
                            field.url = url;
                            fields.push(field);
                            return Promise.resolve(fields);
                        }, function (error) {
                            return Promise.reject(error);
                        });
                });
            });
            return p;
        };

        return getUrls(documentsToDownload);
    },


    /**
     * Saves the state of the client document data. Retrieves the versions of the server side documents.
     * Stores the results in the clientDocuments and serverDocuments properties.
     * Does nothing if the store model does not contain document fields
     * @returns {Promise}
     */
    retrieveDocumentsForSync: function () {
        var me = this,
            includeDocumentDataInSync = me.getIncludeDocumentDataInSync();


        if (me.modelHasDocumentFields() && includeDocumentDataInSync) {
            return me.restoreClientDocuments(me.clientDocuments)
                .then(function () {
                    return me.getClientDocuments();
                })
                .then(function (clientDocuments) {
                    var convertedRecordsForServer;
                    me.clientDocuments = clientDocuments;
                    convertedRecordsForServer = me.convertClientDocumentsForServer();
                    return MobileSyncServiceAdapter.getMaxDocumentVersionsForTable(me.getDocumentConfig().documentTable, convertedRecordsForServer);
                })
                .then(function (serverDocuments) {
                    var clientTableKeys = me.getDocumentConfig().clientTableKeys;
                    
                    // Add the clientTableKeys to the document object
                    serverDocuments.forEach(function(docObject) {
                        docObject.clientTableKeys = clientTableKeys;
                    });
                    return me.downloadDocuments(serverDocuments);
                });
        } else {
            return Promise.resolve();
        }
    },

    convertClientDocumentsForServer: function () {
        var me = this,
            documentsForServer = [],
            tableName = me.getDocumentConfig().documentTable,
            documentFields = me.getModel().getDocumentFieldNames();

        // Convert clientDocuments to Array
        var clientDocumentsArray = Object.keys(me.clientDocuments).map(function (p) {
            me.clientDocuments[p].pkey_value = p;
            me.clientDocuments[p].table_name = tableName;
            return me.clientDocuments[p];
        });

        clientDocumentsArray.forEach(function (clientDocument) {
            // Create a fieldValue object for each document field
            documentFields.forEach(function (documentField) {
                var fieldValue = {};
                var fields = [
                    {
                        fieldName: 'field_name',
                        fieldValue: documentField
                    },
                    {
                        fieldName: 'table_name',
                        fieldValue: tableName
                    },
                    {
                        fieldName: 'pkey_value',
                        fieldValue: clientDocument.pkey_value
                    },
                    {
                        fieldName: 'version',
                        fieldValue: clientDocument[documentField + '_version']
                    }
                ];
                fieldValue.fieldValues = fields;
                documentsForServer.push(fieldValue);
            });


        });

        return documentsForServer;
    },

    downloadDocuments: function (documentFilesToDownload) {
        var me = this,
            clientTableName = me.getProxy().getTable();

        if (documentFilesToDownload.length === 0) {
            return Promise.resolve();
        } else {
            return me.getDocumentUrls(documentFilesToDownload)
                .then(function (documentRecords) {
                    return Promise.all(documentRecords.map(DocumentManager.downloadDocumentFile));
                })
                .then(function (docObjects) {
                    // Save to files
                    return DocumentManager.saveDocumentsToStorage(docObjects, clientTableName);
                });
        }
    },

    saveDocumentData: function () {
        var me = this,
            useFileStorage = GlobalParameters.useFileStorage();

        if (useFileStorage) {
            return Promise.resolve();
        } else {
            if (me.modelHasDocumentFields() && me.getIncludeDocumentDataInSync()) {
                return DocumentManager.createTempDocumentTable()
                    .then(function () {
                        var flattenedClientDocs = me.flattenClientDocuments();
                        return me.copyDocuments(flattenedClientDocs);
                    });
            } else {
                return Promise.resolve();
            }
        }
    },

    flattenClientDocuments: function () {
        var me = this,
            model = me.getModel(),
            clientDocs = me.clientDocuments,
            tableName = me.getProxy().getTable(),
            flattenedDocs = [],
            p,
            clientTableKeys = me.getDocumentConfig().clientTableKeys,
            uniqueId = clientTableKeys.length > 0 ? clientTableKeys : model.getUniqueIdentifier();

        for (p in clientDocs) {
            clientDocs[p].docFields.forEach(function (docField) {
                flattenedDocs.push({
                    tableName: tableName,
                    record: clientDocs[p],
                    docField: docField,
                    uniqueId: uniqueId,
                    primaryKeyValues: p
                });
            });
        }

        return flattenedDocs;
    },

    executeDocumentDataCopy: function (data) {
        var db = SqliteConnectionManager.getConnection();
        return new Promise(function (resolve, reject) {
            var whereClause = [],
                sql;

            data.uniqueId.forEach(function (id) {
                whereClause.push(id + "='" + data.record[id] + "'");
            });

            sql = 'INSERT INTO TmpDocs(doc_field,doc_contents,pkey)';
            sql += " SELECT '" + data.docField + "'," + data.docField + "_contents,'" + data.primaryKeyValues + "'";
            sql += ' FROM ' + data.tableName + ' WHERE ' + whereClause.join(' AND ') + ' AND ' + data.docField + '_contents IS NOT NULL';

            db.transaction(function (tx) {
                tx.executeSql(sql, null, resolve, reject);
            });
        });
    },

    copyDocuments: function (documentData) {
        var me = this;
        return Promise.all(documentData.map(me.executeDocumentDataCopy));
    }
});