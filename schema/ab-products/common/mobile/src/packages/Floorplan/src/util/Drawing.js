/**
 * Utility classes used in the Floorplan package
 * @since 22.1
 * @author Jeff Martin
 */
Ext.define('Floorplan.util.Drawing', {
    requires: [
        'Floorplan.util.Floorplan',
        'Common.util.Network',
        'Common.service.drawing.Drawing'
    ],

    singleton: true,

    /**
     * Reads the floor plan data from the database if it is available. If the data does not exist in the database
     * the floor plan is retrieved from the server.
     * @param {String} buildingCode The building code
     * @param {String} floorCode The floor code
     * @param {String} planType The Plan Type to apply to the SVG data
     * @param {Object[]} highLights The highlights to apply to the SVG data
     * @param {Boolean} useCurrentSession Creates a new Session when false. Uses the existing Session when true.
     */
    readDrawingFromStorageOrRetrieveIfNot: function (buildingCode, floorCode, planType, highLights, useCurrentSession) {
        var me = this,
            localPublishDate,
            serverPublishDate,
            pkeyValues = {
                bl_id: buildingCode,
                fl_id: floorCode
            };

        if (Ext.isEmpty(highLights)) {
            highLights = [];
        }

        if (!Ext.isDefined(useCurrentSession)) {
            useCurrentSession = false;
        }

        // Get the local floor plan publish date
        return Floorplan.util.Floorplan.createDrawingTableIfNotExists()
            .then(function () {
                return Floorplan.util.Floorplan.getDrawingPublishDate(buildingCode, floorCode, planType);
            })
            .then(function (publishDate) {
                localPublishDate = publishDate;
                serverPublishDate = Ext.getStore('publishDates').getServerPublishDate(buildingCode, floorCode);
                return Floorplan.util.Floorplan.readFloorPlan(buildingCode, floorCode, planType);
            })
            .then(function (svgData) {
                var tmpSvgData = svgData;
                if (Ext.isEmpty(svgData)) {
                    return me.retrieveSvgDataFromServer(pkeyValues, planType, highLights, useCurrentSession);
                } else {
                    // The floor plan is cached, check if there is a newer version on the server.
                    if (me.retrieveNewDrawing(localPublishDate, serverPublishDate)) {
                        return me.retrieveSvgDataFromServer(pkeyValues, planType, highLights, useCurrentSession)
                            .then(function (svgData) {
                                if (Ext.isEmpty(svgData)) {
                                    return Promise.resolve(tmpSvgData);
                                } else {
                                    return Promise.resolve(svgData);
                                }
                            });
                    } else {
                        return Promise.resolve(tmpSvgData);
                    }
                }

            });
    },

    /**
     * Returns true if there is a newer version of the drawing available on the server.
     * @param {Date} localPublishDate publish date stored on the client
     * @param {Date} serverPublishDate publish date stored on the server
     * @returns {boolean} true if there is a newer version of the drawing on the server.
     */
    retrieveNewDrawing: function (localPublishDate, serverPublishDate) {
        var loadNewDrawing = false;
        if (localPublishDate !== null && serverPublishDate !== null) {
            loadNewDrawing = (serverPublishDate > localPublishDate);
        }
        return loadNewDrawing;
    },

    /**
     * @private
     * @param primaryKeys
     * @param planType
     * @param highlights
     * @param useCurrentSession the existing Session is used when true. Otherwise a new Session is created.
     * @returns {Promise} A Promise object resolved with the SVG data.
     */
    retrieveSvgDataFromServer: function (primaryKeys, planType, highlights, useCurrentSession) {
        var promiseFn = function () {
            return Common.service.drawing.Drawing.retrieveSvgFromServer(primaryKeys, planType, highlights);
        };

        if (useCurrentSession) {
            return Common.service.drawing.Drawing.retrieveSvgFromServer(primaryKeys, planType, highlights);
        } else {
            return Common.service.Session.doInSession(promiseFn, true);
        }
    },

    /**
     * Retrieves the detail_dwg file names from the SpaceSite table
     * @returns {Promise} A Promise resolved with the detailDrawing object
     */
    getSiteDrawingRecords: function () {
        return new Promise(function (resolve, reject) {
            var db = SqliteConnectionManager.getConnection(),
                sql = 'SELECT site_id,detail_dwg FROM SpaceSite WHERE detail_dwg IS NOT NULL';

            db.transaction(function (tx) {
                tx.executeSql(sql, null, function (tx, result) {
                    var ln = result.rows.length,
                        detailDrawings = [],
                        i;

                    for (i = 0; i < ln; i++) {
                        detailDrawings.push({
                            site_id: result.rows.item(i).site_id,
                            detail_dwg: result.rows.item(i).detail_dwg
                        });
                    }
                    resolve(detailDrawings);
                }, function (tx, error) {
                    reject(error.message);
                });
            });
        });
    },

    /**
     * Returns an array of Active Plan Types
     * @returns {Ext.data.Model[]}
     */
    getActivePlanTypes: function () {
        var planTypesStore = Ext.getStore('planTypes'),
            activePlanTypes = [];

        // The Plan Types store is loaded and does not have a page size set
        planTypesStore.each(function (planType) {
            if (planType.get('active') === 1) {
                activePlanTypes.push(planType.get('plan_type'));
            }
        });
        return activePlanTypes;
    },

    /**
     * Returns an array of active Plan Types for specific app
     * @param {String} store id for active app plan types
     * @returns {Ext.data.Model[]}
     */
    getPlanTypesForApp: function (appPlanTypeStoreId) {
        var activePlanTypes = [],
            planTypesStore = Ext.getStore(appPlanTypeStoreId);

        // The Plan Type Groups store is loaded and does not have a page size set
        if (planTypesStore) {
            planTypesStore.each(function (planTypeRecord) {
                activePlanTypes.push(planTypeRecord.get('plan_type'));
            });
        }

        return activePlanTypes;
    },

    /**
     * Returns an array of Plan Types
     * @returns {Ext.data.Model[]}
     */
    getAllPlanTypes: function () {
        var planTypesStore = Ext.getStore('planTypes'),
            planTypes = [];

        // The Plan Types store is loaded and does not have a page size set
        planTypesStore.each(function (planType) {
            planTypes.push(planType.get('plan_type'));
        });

        return planTypes;
    },

    saveSiteDrawing: function (siteId, svgData) {
        var me = this,
            useFileStorage = GlobalParameters.useFileStorage(),
            p = Promise.resolve();

        return p.then(function () {
            if (useFileStorage) {
                return me.saveSiteDrawingToFile(siteId, svgData);
            } else {
                return Promise.resolve();
            }
        }).then(function () {
            var data = useFileStorage ? '' : svgData;
            return me.saveSiteDrawingToDatabase(siteId, data);
        });
    },

    saveSiteDrawingToDatabase: function (siteId, svgData) {
        return new Promise(function (resolve, reject) {
            var deleteSql = 'DELETE FROM SiteDrawing WHERE site_id=?',
                insertSql = 'INSERT INTO SiteDrawing(site_id, svg_data, svg_data_file) VALUES(?,?,?)',
                db = Common.store.proxy.SqliteConnectionManager.getConnection(),
                afterDelete = function (tx) {
                    tx.executeSql(insertSql, [siteId, svgData, siteId], resolve, function (tx, error) {
                        reject(error.message);
                    });
                };

            db.transaction(function (tx) {
                tx.executeSql(deleteSql, [siteId], afterDelete, function (tx, error) {
                    reject(error.message);
                });
            });

        });

    },

    saveSiteDrawingToFile: function (siteId, svgData) {
        var folder = GlobalParameters.floorplanFolder + '/' + ConfigFileManager.username;

        return Common.device.File.createDirectory(folder).then(function () {
            return Common.device.File.writeFile(folder + '/' + siteId, svgData);
        });
    },

    /**
     * Sets the drawing ViewBox attribute to force the drawing to be rendered.
     * Required as a work around for an issue in iOS 11 where the SVG element is hidden
     * when a mask is applied.
     *
     * @param view Floorplan or Redline view containing the drawing
     * @param delay The delay to execute the setting of the attribute in milliseconds.
     */
    refreshDrawing: function(view, delay) {
        var viewBox,
             svg;

        delay = (typeof delay !== 'undefined') ? delay : 20;

        return new Promise(function(resolve) {
            if(Ext.os.is.ios && Ext.os.version.major > 10 && view && Ext.isFunction(view.getSvgComponent)) {
                svg = view.getSvgComponent().getSvg();

                if(svg && !svg.empty()) {
                    viewBox = svg.attr('viewBox');
                    setTimeout(function() {
                        svg.attr('viewBox', viewBox);
                        resolve();
                    },delay);
                } else {
                    resolve();
                }
            } else {
                resolve();
            }
        });
    }


});