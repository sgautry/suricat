/**
 * A Redline controller class. This class should be extended in any app using the {@link Floorplan.view.Redline} class.
 * @since 22.1
 * @author Jeff Martin
 */
Ext.define('Floorplan.controller.Redline', {
    extend: 'Ext.app.Controller',

    requires: [
        'Floorplan.util.Drawing'
    ],

    config: {
        refs: {
            mainView: null, /* set in the app redline controller */
            redlineView: 'redline2panel',
            redlineLegend: 'redline2panel legend',
            redlineSvgComponent: 'redline2panel svgcomponent'
        },

        control: {
            redlineLegend: {
                legendcopytap: 'onLegendCopyTap',
                legendpastetap: 'onLegendPasteTap',
                legendreloadtap: 'onLegendReloadTap',
                legendsavetap: 'onSaveImage'
            },
            redlineSvgComponent: {
                svgpainted: 'onSvgPainted'
            },
            'button[action=openRedline]': {
                tap: 'displayRedlineView'
            },
            documentItem: {
                displayredline: 'displayRedlinePhoto'
            }
        },

        /**
         * @cfg {String} redlineStoreId The {@link Ext.data.Store} storeId of the store used to manage the model
         * containing the redline storage fields
         */
        redlineStoreId: null,

        /**
         * @cfg {String} documentField The name of the document field used to store the Redline image data. This
         * property should be set when the {@link Floorplan.view.Redline} view is instantiated.
         */
        documentField: null,

        /**
         * @cfg {String} redlinePlanType The Plan Type to be applied to the Redline SVG data
         */
        redlinePlanType: null,

        /**
         * @cfg {Object[]} redlineHightlightParameters The Highlight Parameters to be applied to the Redline SVG data.
         */
        redlineHighlightParameters: []
    },

    onLegendCopyTap: function () {
        this.getRedlineView().copyAssets();
    },

    onLegendPasteTap: function () {
        this.getRedlineView().pasteAssets();
    },

    onLegendReloadTap: function () {
        this.reloadView();
    },

    displayRedlineView: function () {
        var me = this,
            redlineView,
            currentView = me.getMainView().getNavigationBar().getCurrentView(),
            record = currentView.getRecord(),
            planType = me.getRedlinePlanType(),
            highlightParameters = me.getRedlineHighlightParameters(),
            documentField = me.getDocumentField();

        me.retrieveFloorPlanData(record, planType, highlightParameters)
            .then(function (svgData) {
                if (Ext.isEmpty(svgData)) {
                    Ext.Msg.alert('', LocaleManager.getLocalizedString('There is no Floor Plan available to Redline', 'Floorplan.controller.Redline'));
                } else {
                    redlineView = Ext.create('Floorplan.view.Redline');
                    redlineView.setSvgData(svgData);
                    redlineView.setRecord(record);
                    redlineView.setDocumentField(documentField);
                    me.getMainView().push(redlineView);
                }
            });
    },

    /**
     * Refreshes the displayed SVG or image data
     */
    reloadView: function () {
        var me = this,
            redlineView = me.getRedlineView(),
            record = redlineView.getRecord(),
            planType = me.getRedlinePlanType(),
            highlightParameters = me.getRedlineHighlightParameters(),
            documentField = redlineView.getDocumentField(),
            contentMode = redlineView.getContentMode(),
            imageData;

        if (contentMode === 'image') {
            imageData = record.get(documentField + '_contents');
            if (imageData && imageData.length > 0) {
                imageData = 'data:image/png;base64,' + imageData;
                redlineView.setImageData(imageData);
            }
        } else {
            me.retrieveFloorPlanData(record, planType, highlightParameters)
                .then(function (svgData) {
                    if (svgData !== '') {
                        redlineView.setSvgData(svgData);
                        me.onSvgPainted(me.getRedlineSvgComponent());
                    }
                });
        }
    },

    /**
     * Called when the SVG data has completed rendering.
     * @param {Floorplan.component.Svg} svgContainer The SVG component
     */
    onSvgPainted: function (svgContainer) {
        var me = this,
            currentView = me.getMainView().getNavigationBar().getCurrentView(),
            record = currentView.getRecord(),
            roomCode;

        if (record && Ext.isFunction(record.getRoomId)) {
            if (record.hasValidRoomCode()) {
                roomCode = record.getRoomId();
                setTimeout(function () {
                    svgContainer.findAssets([roomCode], {}); // Set {zoomFactor: 0} to not apply the zoom when finding assets
                }, 1);
            }
        }


        if(Ext.os.is.ios && Ext.os.version.major > 10 && currentView && Ext.isFunction(currentView.getSvgComponent)) {
            setTimeout(function () {
                var viewBox = currentView.getSvgComponent().getSvg().attr('viewBox');
                currentView.getSvgComponent().getSvg().attr('viewBox', viewBox);
            }, 300);
        }
    },

    /**
     * Saves the Redline image data
     */
    onSaveImage: function () {
        var me = this,
            currentView = me.getMainView().getNavigationBar().getCurrentView(),  // currentView is the redline view
            record = currentView.getRecord();

        me.saveRedlineData(record);
    },

    /**
     * @private
     * @param {Common.data.Record} record The record containing the Redline document field.
     */
    saveRedlineData: function (record) {
        var me = this,
            storeId = me.getRedlineStoreId(),
            store = Ext.getStore(storeId),
            isStoreAutoSync = store.getAutoSync(),
            documentFieldName = me.getRedlineView().getDocumentField(),
            isNative = Common.env.Feature.isNative,
            tmpImageData;


        return me.hideRedlineViewElements(me.getMainView(), me.getRedlineView(), true, isNative)
            .then(function() {
                return me.delay(100);
            })
            .then(function () {
                return me.getRedlineImage(isNative);
            })
            .then(function (imageData) {
                tmpImageData = imageData;
                if (imageData !== null) {
                    if (isStoreAutoSync) {
                        store.setAutoSync(false);
                    }
                    me.saveDataToModel(record, documentFieldName, imageData);
                    return me.syncStore(store);
                }
            })
            .then(function () {
                store.setAutoSync(isStoreAutoSync);
                me.refreshDocumentItem(documentFieldName, tmpImageData);

                me.hideRedlineViewElements(me.getMainView(), me.getRedlineView(), false, isNative);
            });
    },

    getCanvasImage: function (legendComponent) {
        return new Promise(function (resolve) {
            legendComponent.getImageBase64(resolve);

        });
    },

    delay: function (t) {
        return new Promise(function (resolve) {
            setTimeout(resolve, t);
        });
    },

    syncStore: function (store) {
        return new Promise(function(resolve) {
            store.sync(resolve);
        });
    },

    getRedlineImage: function (isNative) {
        var me = this,
            legendComponent = me.getRedlineView().getLegendComponent();
        if (isNative) {
            return Common.device.ScreenCapture.getURI()
                .then(function(imageData) {
                    var data = imageData.substring(23);
                    return Promise.resolve(data);
                });
        } else {
            return me.getCanvasImage(legendComponent);
        }
    },


    /**
     * Refreshes the document in the {@link Common.view.DocumentItem} class if the Redline image is displayed
     * in a {@linke Common.view.DocumentList}
     * @param {String} documentField The name of the document field.
     * @param {String} imageData The base64 image data
     */
    refreshDocumentItem: function (documentField, imageData) {
        var me = this,
            documentItems = Ext.ComponentQuery.query('documentItem');

        Ext.each(documentItems, function (item) {
            if (item.getFieldName() === documentField) {
                item.setDocumentData(imageData);
            }
        }, me);
    },

    /**
     * @private
     * @param record
     * @param planType
     * @param highlights
     */
    retrieveFloorPlanData: function (record, planType, highlights) {
        var locationFields = record.getLocationFields();

        return Floorplan.util.Drawing.readDrawingFromStorageOrRetrieveIfNot(locationFields.bl_id, locationFields.fl_id, planType, highlights, false);
    },

    displayRedlinePhoto: function (recordId, fileName, documentFieldId, fieldName) {
        var me = this,
            storeId = me.getRedlineStoreId(),
            store = Ext.getStore(storeId),
            imageData,
            redlineView;

        store.retrieveSingleRecord([{property: 'id', value: recordId}])
            .then(function (record) {
                if (record) {
                    imageData = record.get(fieldName + '_contents');
                    redlineView = Ext.create('Floorplan.view.Redline');
                    redlineView.setRecord(record);
                    redlineView.setDocumentField(fieldName);
                    redlineView.loadImage(imageData);
                    me.getMainView().push(redlineView);
                } else {
                    Log.log('Document record is not found', 'verbose');
                }
            });
    },

    /**
     * @private
     * @param record
     * @param fieldName
     * @param imageData
     */
    saveDataToModel: function (record, fieldName, imageData) {
        record.set(fieldName, fieldName + '.png');
        record.set(fieldName + '_contents', imageData);
        record.set(fieldName + '_isnew', true);
        record.set('mob_is_changed', 1);
    },

    hideRedlineViewElements: function (mainView, redlineView, hide, isNative) {
        var legend = redlineView.down('legend'),
            redlineTitleBar = redlineView.down('titlebar'),
            mainTitleBar,
            fn = hide ? 'hide' : 'show',
            viewBox,
            svg;

        if(!isNative) {
            if(hide) {
                Mask.displayLoadingMask();
            } else {
                Mask.hideLoadingMask();
            }
            return Promise.resolve();
        } else {
            return new Promise(function(resolve) {

                if (mainView) {
                    mainTitleBar = mainView.down('titlebar');
                    mainTitleBar[fn]();
                }
                legend[fn]();
                redlineTitleBar[fn]();

                StatusBar[fn]();

                if(Ext.os.is.ios && Ext.os.version.major > 10 && redlineView && Ext.isFunction(redlineView.getSvgComponent)) {
                    redlineView.getSvgComponent().home[fn]();
                    svg = redlineView.getSvgComponent().getSvg();

                    if (svg && !svg.empty()) {
                        viewBox = svg.attr('viewBox');
                        setTimeout(function () {
                            svg.attr('viewBox', viewBox);
                            resolve();
                        }, 20);
                    } else {
                        resolve();
                    }
                } else {
                    resolve();
                }
            });
        }
    }
});