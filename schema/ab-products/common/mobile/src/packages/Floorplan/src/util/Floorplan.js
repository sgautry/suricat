/**
 * Floorplan utility functions
 *
 * @author Jeff Martin
 * @since 22.1
 */
Ext.define('Floorplan.util.Floorplan', {
    singleton: true,

    requires: [
        'Floorplan.model.Drawing',
        'Common.sync.Manager'
    ],

    dateFormat: 'Y-m-d H:i:s.000',

    mixins: [
        'Common.promise.util.DatabaseOperation',
        'Ext.mixin.Observable'
    ],

    isDownloadCancelled: false,

    //kb3049434: status for network connection
    // TODO: remove
    isNetworkInterrupted: false,

    cancelDownload: function () {
        this.isDownloadCancelled = true;
    },

    saveFloorPlan: function (blId, flId, planType, data) {
        var me = this,
            useFileStorage = GlobalParameters.useFileStorage(),
            fileName = useFileStorage ? me.generateFileName(blId, flId, planType) : '',
            floorplanFolder = GlobalParameters.floorplanFolder + '/' + ConfigFileManager.username,
            publishedDates = Ext.getStore('publishDates').getPublishDates();

        if (Ext.isEmpty(data)) {
            return Promise.resolve();
        } else {
            return me.createDrawingTableIfNotExists()
                .then(function () {
                    if (useFileStorage) {
                        return me.writeFloorplanToFile(floorplanFolder, fileName, data);
                    } else {
                        return Promise.resolve();
                    }
                })
                .then(function () {
                    var svgData = useFileStorage ? '' : data,
                        publishDate = publishedDates[blId + ';' + flId];

                    if(!publishDate) {
                        publishDate = null;
                    }
                    return me.saveToDatabase(blId, flId, planType, fileName, svgData, publishDate);
                });
        }
    },

    /**
     * Reads the floorPlan drawing data from the database.
     * @param blId
     * @param flId
     * @param planType
     */
    readFloorPlan: function (blId, flId, planType) {
        var me = this,
            useFileStorage = GlobalParameters.useFileStorage(),
            fileName = me.generateFileName(blId, flId, planType),
            folder = GlobalParameters.floorplanFolder + '/' + ConfigFileManager.username;

        return me.createDrawingTableIfNotExists()
            .then(function () {
                if (useFileStorage) {
                    return me.readFloorplanFromFile(folder, fileName);
                } else {
                    return me.readFromDatabase(blId, flId, planType);
                }
            })
            .then(null, function (error) {
                Ext.Msg.alert('', error);
                return Promise.reject('');
            });
    },

    generateFileName: function (blId, flId, planType) {
        var fileName = blId + '_' + flId + '_' + planType;

        fileName = fileName.replace(/\s+/g, '');
        return fileName + '.svg';

    },

    writeFloorplanToFile: function (folder, file, data) {
        return Common.device.File.createDirectory(folder)
            .then(function () {
                return Common.device.File.writeFile(folder + '/' + file, data);
            });
    },

    readFloorplanFromFile: function (folder, file) {
        return Common.device.File.createDirectory(folder)
            .then(function () {
                return Common.device.File.readFile(folder + '/' + file);
            });
    },

    createDrawingTableIfNotExists: function () {
        var me = this,
            drawingModel = Ext.ModelManager.getModel('Floorplan.model.Drawing');
        return me.createOrModifyTable('Drawing', drawingModel);
    },

    saveToDatabase: function (blId, flId, planType, fileName, data, publishDate) {
        var me = this,
            deleteSql = 'DELETE FROM Drawing WHERE blId = ? AND flId = ? AND planType = ?',
            insertSql = 'INSERT INTO DRAWING(blId,flId,planType,data,fileName,publishDate) VALUES (?, ?, ?, ?, ?, ?);';

        return me.executeSqlCommand(deleteSql, [blId, flId, planType])
            .then(function () {
                var formattedDate = publishDate ? Ext.Date.format(publishDate, me.dateFormat): null;
                return me.executeSqlCommand(insertSql, [blId, flId, planType, data, fileName, formattedDate]);
            });
    },

    readFromDatabase: function (blId, flId, planType) {
        return new Promise(function (resolve, reject) {
            var sql = 'SELECT data FROM Drawing WHERE blId = ? AND flId = ? AND planType = ?',
                db = SqliteConnectionManager.getConnection();

            db.transaction(function (tx) {
                tx.executeSql(sql, [blId, flId, planType], function (tx, result) {
                    var data = null;
                    if (result.rows.length > 0) {
                        data = result.rows.item(0).data;
                    }
                    resolve(data);
                }, function (tx, error) {
                    reject(error.message);
                });
            });

        });
    },

    // TODO: Move
    executeSqlCommand: function (sql, parameters) {
        return new Promise(function (resolve, reject) {
            var db = SqliteConnectionManager.getConnection();
            db.transaction(function (tx) {
                tx.executeSql(sql, parameters, resolve,
                    function (tx, error) {
                        reject(error.message);
                    });
            });
        });
    },


    /**
     * Downloads the floor plans for each of the floors identified by the floorCodes array.
     * Writes each of the plan types for each floor to the database Drawing table
     * @param {Object[]} floorCodes An array of floor code primary keys
     * @param {String[]} planTypes The plan type codes to download
     */
    getDrawingsForFloors: function (floorCodes, planTypes) {
        var publishDateStore = Ext.getStore('publishDates');

        if ((floorCodes && floorCodes.length === 0) || (planTypes && planTypes.length === 0)) {
            return Promise.resolve();
        } else {
            // Update the pubish dates
            return publishDateStore.deleteAndImportRecords()
                .then(function () {
                    return SyncManager.loadStore(publishDateStore);
                })
                .then(function () {
                    var combinedFloorCodes = Floorplan.util.Floorplan.combineFloorCodesAndPlanTypes(floorCodes, planTypes);
                    var fn = Floorplan.util.Floorplan.downloadDrawings(combinedFloorCodes, planTypes);
                    return fn(combinedFloorCodes);
                });
        }
    },

    combineFloorCodesAndPlanTypes: function(floorCodes, planTypes) {
        var combinedFloorCodes = [];

        // Generate the array of floor codes and plan types
        floorCodes.forEach(function (floorCode) {
            planTypes.forEach(function (planType) {
                var combinedCode = {};
                combinedCode.floorCode = floorCode;
                combinedCode.planType = planType;
                combinedFloorCodes.push(combinedCode);
            });
        });

        return combinedFloorCodes;
    },

    downloadDrawings: function(combinedCodes, planTypes) {

        var numberOfPlanTypes = planTypes.length,
            numberOfCodes = combinedCodes.length,
            numberOfFloorPlansDownloaded = 0;

        var getDrawings = function(combinedCodes) {
            var codes = Ext.clone(combinedCodes),
                code = codes.shift(),
                isCanceled = Floorplan.util.Floorplan.isDownloadCancelled;

            if(isCanceled) {
                Log.log('Floorplan Download Canceled.', 'info');
                // Reset isDownloadCancelled flag
                Floorplan.util.Floorplan.isDownloadCancelled = false;
                return Promise.resolve({
                    floorPlansDownloaded: numberOfFloorPlansDownloaded,
                    canceled: isCanceled
                });
            } else {
                if(code) {
                    return Common.service.drawing.Drawing.retrieveSvgFromServer(code.floorCode, code.planType, null)
                        .then(function(svgData) {
                            Log.log('Floorplan Downloaded [bl_id:' + code.floorCode.bl_id + ', fl_id:' + code.floorCode.fl_id +', planType:' + code.planType + ']', 'info');
                            numberOfCodes--;
                            if(numberOfCodes % numberOfPlanTypes === 0) {
                                if(svgData !== '') {
                                    numberOfFloorPlansDownloaded +=1;
                                }
                                Floorplan.util.Floorplan.fireEvent('incrementprogress');
                            }
                            return Floorplan.util.Floorplan.saveFloorPlan(code.floorCode.bl_id, code.floorCode.fl_id, code.planType, svgData);
                        })
                        .then(function() {
                            return getDrawings(codes);
                        });
                } else {
                    return Promise.resolve({
                        floorPlansDownloaded: numberOfFloorPlansDownloaded,
                        canceled: isCanceled
                    });
                }
            }
        };

        return getDrawings;
    },

    /**
     * Retrieves the local publish date from the client Drawing table. The drawing publish time is
     * recorded when the floor plan is downloaded.
     * @param {String} blId building code
     * @param {String} flId floor code
     * @param {String} planType plan type.
     */
    getDrawingPublishDate: function(blId, flId, planType) {
        var me = this,
            sql = 'SELECT publishDate FROM Drawing WHERE blId = ? AND flId = ? AND planType = ?',
            db = SqliteConnectionManager.getConnection();

        return new Promise(function(resolve, reject) {
            db.transaction(function(tx) {
                tx.executeSql(sql, [blId, flId, planType], function(tx, result) {
                    var publishDate;
                    if(result.rows.length > 0) {
                        publishDate = result.rows.item(0).publishDate;
                        if(publishDate) {
                            publishDate = Ext.Date.parse(publishDate, me.dateFormat);
                        }
                        resolve(publishDate);
                    } else {
                        resolve(null);
                    }
                }, function(tx, error) {
                    reject(error.message);
                });
            });
        });
    }
});