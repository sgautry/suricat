/**
 * Utility class to manage download operations on Space objects
 *
 * @author Ana Paduraru
 * @since 21.2
 */
Ext.define('Space.SpaceDownload', {
    singleton: true,
    mixins: ['Ext.mixin.Observable'],
    requires: [
        'Common.service.drawing.Drawing',
        'Floorplan.util.Drawing',
        'Common.service.ExceptionTranslator',
        'Common.sync.Manager',
        'Floorplan.util.Floorplan'
    ],

    downloadFloorPlanMessage: LocaleManager.getLocalizedString('You are going to download plans for {0} floors and this action may take some time. Proceed?', 'Space.SpaceDownload'),
    noFloorPlansMessage: LocaleManager.getLocalizedString('Use the {0} icon to download your list of floors and buildings before downloading floor plans.', 'Space.SpaceDownload'),
    noFloorPlansTitle: LocaleManager.getLocalizedString('Floor Plans', 'Space.SpaceDownload'),
    downloadFloorPlansTitle: LocaleManager.getLocalizedString('Download Floor Plans', 'Space.SpaceDownload'),
    numberOfFloorPlansDownloadedMessage: LocaleManager.getLocalizedString('Downloaded plans for {0} floors.', 'Space.SpaceDownload'),
    cancelMessage: LocaleManager.getLocalizedString('Floor Plan download was cancelled.', 'Space.SpaceDownload'),
    noFloorPlansAvailable: LocaleManager.getLocalizedString('No Floor Plans available to download', 'Space.SpaceDownload'),

    noActivePlanTypesFound: LocaleManager.getLocalizedString('No active plan types found.', 'Space.SpaceDownload'),
    noPlanTypesFound: LocaleManager.getLocalizedString('No plan types found', 'Space.SpaceDownload'),
    downloadFloorPlansOnlyForFilteredSitesMessage: LocaleManager.getLocalizedString('Downloading floor plans for {0} sites, based on your search. <br />' +
        'To download plans for all sites, clear the search box and tap again on "Download Floor Plans".', 'Space.SpaceDownload'),

    numFilteredSites: 0,

    NUMBER_OF_PLANS_WARNING: 100,


    constructor: function () {
        Floorplan.util.Floorplan.on('incrementprogress', function () {
            Space.SpaceDownload.updateProgressBar();
        });
    },

    cancelDownload: function () {
        Floorplan.util.Floorplan.cancelDownload();
    },

    /**
     * Action Download Data.
     */
    onDownloadData: function (scope, onCompleted) {
        var me = this,
            siteList = scope.getSiteList(),
            emptyTextMessage = siteList.getEmptyText(),
            onFinish = function () {
                SyncManager.endSync();
                Common.service.Session.end()
                    .then(function () {
                        // KB3049392 - scroll to top of the list
                        if (siteList && siteList.getScrollable() && siteList.getScrollable().getScroller()) {
                            siteList.getScrollable().getScroller().scrollToTop();
                        }
                        siteList.setEmptyText(emptyTextMessage);
                        Ext.callback(onCompleted, scope || me);
                    });
            };

        if (SyncManager.syncIsActive) {
            return;
        }

        Mask.displayLoadingMask();
        Network.checkNetworkConnectionAndLoadDwrScripts(true)
            .then(function (isConnected) {
                Mask.hideLoadingMask();
                if (isConnected) {
                    Common.service.Session.start()
                        .then(function () {
                            SyncManager.startSync();
                            return SyncManager.downloadValidatingTables();
                        })
                        .then(function () {
                            // KB3049392 - reset sites store page to load first page on Download Data
                            Ext.getStore('spaceBookSites').currentPage = 1;
                            return SyncManager.loadStoresAfterSync();
                        })
                        .then(function () {
                            // Load the SiteDrawings store to ensure the table is created before saving drawings.
                            return SyncManager.loadStore('siteDrawings');
                        })
                        .then(function () {
                            Mask.setLoadingMessage(LocaleManager.getLocalizedString('Downloading Site Maps', 'Space.SpaceDownload'));
                            return me.downloadSiteDrawings();
                        })
                        .then(null, function (error) {
                            Ext.Msg.alert('', error);
                            return Promise.reject();
                        })
                        .done(onFinish, onFinish);
                }
            }, function () {
                // Check connection error
                Mask.hideLoadingMask();
                Ext.callback(onCompleted, scope || me);
            });
    },

    /**
     * Retrieve the site_id values for all of the records where the detail_dwg field is not null. Retrieve the site
     * drawing data from the server for each of the returned site records. Note: We are retrieving the site records from
     * the database to avoid complications that arise from using the SpaceSite store to display the main page list of
     * sites.
     * @return {Promise} A Promise that is resolved when the site drawings are retrieved.
     */

    downloadSiteDrawings: function () {
        var me = this;

        return Floorplan.util.Drawing.getSiteDrawingRecords()
            .then(function (siteRecords) {
                return me.getSiteDrawingFromServerWithoutSession(siteRecords);
            });
    },


    /**
     * Retrieves the site drawings from the server. Saves the site drawings to the client database.
     * @param {Object[]} siteRecords An array of objects containing the site_id.
     */
    getSiteDrawingFromServerWithoutSession: function (siteRecords) {
        var highlightParameters = [
            {
                view_file: 'ab-sp-space-book-bl.axvw',
                hs_ds: 'ds_ab-sp-space-book-bl_blHighlight',
                label_ds: 'ds_ab-sp-space-book-bl_blLabel',
                label_ht: '1'
            }
        ];

        var downloadSiteDrawings = function (siteRecords) {
            var p = Promise.resolve();
            siteRecords.forEach(function (siteRecord) {
                p = p.then(function () {
                    return Common.service.drawing.Drawing.retrieveSvgFromServer({site_id: siteRecord.site_id}, null, highlightParameters);
                }).then(function (svgData) {
                    return Floorplan.util.Drawing.saveSiteDrawing(siteRecord.site_id, svgData);
                });
            });
            return p;
        };

        return downloadSiteDrawings(siteRecords);
    },

    /**
     * Downloads all floor plans for the provided location level
     * @param {String} level The location level.
     * Valid levels are:
     *  - all: download all floor plans
     *  - site: download all floor plans for the site
     *  - building: download all floor plans for the building
     *  @param {Object} scope Caller scope
     *  @param {Boolean} useOnlyActivePlanTypes
     *  @param [applySecurityGroups] Applies user security groups when true
     *  @param {Ext.data.Store} appPlanTypeStoreId [since 22.1] store name for the store containing the active plan types for the current app.
     *  It uses the plantype_groups server table.
     */

    downloadFloorPlans: function (level, scope, useOnlyActivePlanTypes, applySecurityGroups, appPlanTypeStoreId) {
        var me = this,
            onFinish = function () {
                Common.service.Session.end();
            };

        return Network.checkNetworkConnectionAndLoadDwrScripts(true)
            .then(function (isConnected) {
                if (isConnected) {
                    return Common.service.Session.start()
                        .then(SyncManager.downloadValidatingTable.bind(SyncManager, 'planTypes'))
                        .then(SyncManager.loadStore.bind(SyncManager, 'planTypes'))
                        .then(function () {
                            if (applySecurityGroups) {
                                return SpaceBook.util.Ui.applyUserGroups();
                            } else {
                                return Promise.resolve();
                            }
                        })
                        .then(me.getFloorCodesToDownload.bind(me, level, scope))
                        .then(function (floorCodes) {
                            return me.doDownloadFloorPlans(floorCodes, useOnlyActivePlanTypes, appPlanTypeStoreId);
                        })
                        .then(function (result) {
                            var downloadMessage = Ext.String.format(me.numberOfFloorPlansDownloadedMessage, result.floorPlansDownloaded);

                            if (result.canceled) {
                                downloadMessage = me.cancelMessage + '<br>' + downloadMessage;
                            }
                            Ext.Msg.alert('', downloadMessage);
                        })
                        .then(null, function (error) {
                            Space.SpaceDownload.hideProgressView();
                            Ext.Msg.alert('', error);
                            return Promise.reject();
                        })
                        .done(onFinish, onFinish);
                }
            });

    },

    /**
     *
     * @param {String} level The location hierarchy level, site or building
     * @param {Object} scope The executing scope. Used to reference the current view
     * @returns {Promise} A Promise resolved with an object array containing building ids and floor ids of
     * floors that have published drawings
     */
    getFloorCodesToDownload: function (level, scope) {
        var me = this,
            siteIds = [],
            blIds = [],
            arrSite,
            i;

        if (level === 'building') {
            blIds.push(scope.getFloorsListView().getParentId());

            //KB# 3053656 - if no buildings are identified, no floor plans should be downloaded
            if (blIds[0] === null || blIds.length === 0) {
                return Promise.reject(me.noFloorPlansAvailable);
            }
        } else if (level === 'site') {
            siteIds.push(scope.getSiteView().getParentId());

            //KB# 3053656 - if there are buildings without a site or no site is identified, floor plans should not be downloaded
            if (siteIds[0] === null || siteIds.length === 0) {
                return Promise.reject(me.noFloorPlansAvailable);
            }
        } else if (level === 'filteredSites') {
            //kb# 3047360 floor plans will be downloaded only for the filtered sites
            arrSite = scope.getSiteList().getStore().data.items;
            for (i in arrSite) {
                siteIds.push(arrSite[i].data.site_id);
            }
            //KB# 3053656 - if no site is identified, floor plans should not be downloaded
            if (siteIds.length === 0) {
                return Promise.reject(me.noFloorPlansAvailable);
            }
        }
        //kb# 3047360 if there are filtered sites, the progress bar's message will change to downloadFloorPlansOnlyForFilteredSitesMessage
        me.numFilteredSites = siteIds.length;

        return Common.service.drawing.Drawing.getFloorCodesForPublishedDrawings(siteIds, blIds);
    },

    /**
     * Verify if there are active plan types for a specific app.
     * @param {String} store id for active app plan types
     * @returns {boolean}
     */
    verifyExistActivePlanTypesForApp: function (appPlanTypeStoreId) {
        var activePlanTypes = Floorplan.util.Drawing.getPlanTypesForApp(appPlanTypeStoreId);

        if (activePlanTypes.length === 0) {
            Ext.Msg.alert('', Space.SpaceDownload.noActivePlanTypesFound);
            return false;
        }

        return true;
    },

    updateProgressBar: function () {
        if(Space.SpaceDownload.progressView) {
            Space.SpaceDownload.progressView.increment();
        }
    },

    doDownloadFloorPlans: function (floorCodes, activePlanTypesOnly, appPlanTypeStoreId) {
        var planTypes,
            message,
            numberOfFloors = floorCodes.length,
            combinedFloorCodes;

        if (Ext.isEmpty(appPlanTypeStoreId)) {
            planTypes = activePlanTypesOnly ? Floorplan.util.Drawing.getActivePlanTypes() : Floorplan.util.Drawing.getAllPlanTypes();
        } else {
            planTypes = Floorplan.util.Drawing.getPlanTypesForApp(appPlanTypeStoreId);
        }

        if (planTypes.length === 0) {
            message = activePlanTypesOnly ? Space.SpaceDownload.noActivePlanTypesFound : Space.SpaceDownload.noPlanTypesFound;
            return Promise.reject(message);
        }

        if (numberOfFloors === 0) {
            return Promise.reject(Space.SpaceDownload.noFloorPlansAvailable);
        }

        combinedFloorCodes = Floorplan.util.Floorplan.combineFloorCodesAndPlanTypes(floorCodes, planTypes);

        return Space.SpaceDownload.checkFloorPlanDownloadLimitAndContinue(numberOfFloors)
            .then(function (continueDownload) {
                var fn;
                if (continueDownload) {
                    Space.SpaceDownload.displayProgressBar(numberOfFloors);
                    fn = Floorplan.util.Floorplan.downloadDrawings(combinedFloorCodes, planTypes);
                    return fn(combinedFloorCodes);
                } else {
                    return Promise.resolve();
                }
            });
    },

    /**
     * Retrieves floor codes from the BuildingFloors view.
     * @param filter {Object} Restricts the returned recordset. The applied filter
     * is determined by the selected location that the download action is initiated from.
     * @return {Promise} A Promise resolved to an array of Floor Codes
     */
    getFloorCodes: function (filter) {
        var me = this,
            buildingFloorStore = Ext.getStore('buildingFloorsStore'),
            floorCodes;

        return buildingFloorStore.retrieveAllRecords(filter)
            .then(function (records) {
                floorCodes = Ext.Array.map(records, function (record) {
                    return {
                        bl_id: record.data.bl_id,
                        fl_id: record.data.fl_id
                    };
                }, me);
                return Promise.resolve(floorCodes);
            });
    },

    /**
     *
     * @param {Number} numberOfPlans The number of floors to download floor plans for.
     * @returns {Promise} A Promise that resovles to true if the number of floor plans is less than the limit.
     * The Promise resolves to true if the number of floor plans exceeds the limit but the user decides to proceed.
     */
    checkFloorPlanDownloadLimitAndContinue: function (numberOfPlans) {
        var me = this;
        return new Promise(function (resolve) {
            var message;

            if (numberOfPlans < me.NUMBER_OF_PLANS_WARNING) {
                resolve(true);
            } else {
                message = me.downloadFloorPlanMessage.replace('{0}', numberOfPlans);
                Ext.Msg.confirm(me.downloadFloorPlansTitle, message, function (buttonId) {
                    resolve(buttonId === 'yes');
                });
            }
        });
    },

    displayProgressBar: function (maxValue) {
        var me = this,
            progressMessage;

        if (me.numFilteredSites > 0) {
            //kb# 3047360 floor plans will be downloaded only for the filtered sites and hint
            progressMessage = Ext.String.format(me.downloadFloorPlansOnlyForFilteredSitesMessage, me.numFilteredSites);
        } else {
            progressMessage = LocaleManager.getLocalizedString('Loading Plans for Floor {0} of {1}', 'Space.SpaceDownload');
        }

        if (!me.progressView) {
            me.progressView = Ext.create('Common.view.panel.ProgressBar', {
                value: 0,
                maxValue: maxValue,
                progressMessage: progressMessage
            });
            Ext.Viewport.add(me.progressView);
        } else {
            me.progressView.setProgressMessage(progressMessage);
            me.progressView.setValue(0);
            me.progressView.setMaxValue(maxValue);
            me.progressView.setCancelled(false);
        }
        me.progressView.show();
    },

    onCancelProgress: function () {
        Space.SpaceDownload.cancelDownload();
        Space.SpaceDownload.hideProgressView();
    },

    onCompleteProgress: function () {
        Space.SpaceDownload.hideProgressView();
    },

    hideProgressView: function () {
        if (Space.SpaceDownload.progressView) {
            Space.SpaceDownload.progressView.hide();
            Space.SpaceDownload.progressView.setValue(0);
        }
    }

});