Ext.define('Map.util.Map', {

    singleton: true,

    requires: 'Map.util.ColorBrewer',

    alternateClassName: 'MapUtil',

    xtype: 'abmaputil',

    leafletMarkerColors: ['black', 'blue', 'green', 'grey', 'orange', 'red', 'violet', 'yellow'],

    getUniqueFieldValues: function (storeId, fieldName) {
        var values = [],
            value,
            store = Ext.data.StoreManager.lookup(storeId),
            i;

        for (i = 0; i < store.getCount(); i++) {
            value = store.getAt(i).data[fieldName];
            Ext.Array.include(values, value);
        }

        return values;
    },

    getColorbrewerColors: function (colorBrewerClass, numberOfColors) {
        var colors = [];

        if (ColorBrewer.colors) {
            //TODO  what if we are asking for too many colors
            colors = ColorBrewer.colors[colorBrewerClass][numberOfColors];
        } else {
            Log.log('Colorbrewer library not loaded.', 'INFO');
        }
        return colors;
    },


    getLeafletIcon: function (color) {

        // TODO: Check if the color is valid.
        return new L.Icon({
            iconUrl: 'images/marker-icon-2x-' + color + '.png',
            shadowUrl: 'images/marker-shadow.png',
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
            shadowSize: [41, 41]
        });
    }

});


