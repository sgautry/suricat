The ARCHIBUS Mobile apps use the Siesta JavaScript testing library.

To install and run the tests using Siesta:

1. Download Siesta Lite from http://www.bryntum.com/products/siesta
2. Uzip the contents of the downloaded file
3. Copy the extracted Siesta files to the ../schema/ab-products/common/mobile/src/Siesta folder
4. Execute the tests by entering http://[web central]/schema/ab-products/common/mobile/src/Common/test.html in
   a Chrome browser where [web central] is the location of your Web Central application.