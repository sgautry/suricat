Ext.define('WorkplacePortal.model.ReservationEmployee', {
    extend: 'Common.data.Model',

    config: {
        uniqueIdentifier: ['em_id'],
        fields: [
            {
                name: 'em_id',
                type: 'string'
            },

            {
                name: 'email',
                type: 'string'
            },

            {
                name: 'phone',
                type: 'string'
            }
        ]
    }
});