Ext.define('WorkplacePortal.model.FacilityServicesMenu', {
    extend: 'Common.data.Model',

    config: {
        uniqueIdentifier: ['activity_type'],
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'activity_type',
                type: 'string'
            },
            {
                name: 'title',
                type: 'string'
            },
            {
                name: 'description',
                type: 'string'
            },
            {
                name: 'menu_icon',
                type: 'string',
                isDocumentField: true
            },
            {
                name: 'mobile_action',
                type: 'string'
            },
            {
                name: 'display_order',
                type: 'IntegerClass'
            },
            {
                name: 'is_licensed',
                type: 'IntegerClass',
                defaultValue: 1,
                isSyncField: false
            }
        ]
    }
});

