Ext.define('WorkplacePortal.model.MobileMenu', {
    extend: 'Common.data.Model',

    config: {
        uniqueIdentifier: ['menu_id'],
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'menu_id',
                type: 'string'
            },
            {
                name: 'title',
                type: 'string'
            },
            {
                name: 'description',
                type: 'string'
            },
            {
                name: 'menu_icon',
                type: 'string',
                isDocumentField: true
            },
            {
                name: 'activity_id',
                type: 'string'
            },
            {
                name: 'mobile_action',
                type: 'string'
            },
            {
                name: 'display_order',
                type: 'IntegerClass'
            },
            {
                name: 'group_name',
                type: 'string'
            },
            {
                name: 'is_licensed',
                type: 'IntegerClass',
                defaultValue: 1,
                isSyncField: false
            }
        ]
    }
});
    
