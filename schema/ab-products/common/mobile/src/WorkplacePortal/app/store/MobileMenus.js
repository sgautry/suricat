Ext.define('WorkplacePortal.store.MobileMenus', {
    extend: 'Common.store.sync.ValidatingTableStore',
    requires: 'WorkplacePortal.model.MobileMenu',

    serverTableName: 'afm_mobile_menu',
    serverFieldNames: [
        'menu_id',
        'title',
        'description',
        'menu_icon',
        'mobile_action',
        'display_order',
        'activity_id',
        'group_name'
    ],
    inventoryKeyNames: ['menu_id'],

    licenseMap: {
        'LOCATE_EMPLOYEE': 'AbBldgOpsHelpDesk',
        'LOCATE_ROOM': 'AbBldgOpsHelpDesk',
        'MY_DEPT_SPACE': 'AbBldgOpsHelpDesk',
        'MY_HOTELING_REQ': 'AbSpaceHotelling',
        'MY_RESERV_REQ': 'AbWorkplaceReservations',
        'MY_SERVICE_REQ':'AbBldgOpsHelpDesk'
    },

    config: {
        model: 'WorkplacePortal.model.MobileMenu',

        remoteSort: true,
        sorters: [
            {
                property: 'display_order',
                direction: 'ASC'
            }
        ],
        storeId: 'mobileMenusStore',
        enableAutoLoad: true,
        disablePaging: true,
        proxy: {
            type: 'Sqlite'
        },
        tableDisplayName: LocaleManager.getLocalizedString('Mobile Menus', 'WorkplacePortal.store.MobileMenus'),
        restriction: [
            {
                tableName: 'afm_mobile_menu',
                fieldName: 'group_name',
                operation: 'EQUALS',
                value: 'WORKSVC-MOB'
            },
            {
                tableName: 'afm_mobile_menu',
                fieldName: 'activity_id',
                operation: 'EQUALS',
                value: 'AbWorkplacePortal'
            }
        ],

        filters: [
            {property: 'is_licensed', value: 1}
        ]
    },

    retrieveRecords: function () {
        var me = this,
            retrievedRecords;

        return me.callParent(arguments)
            .then(function (records) {
                retrievedRecords = records;
                return MobileSecurityServiceAdapter.getLicensedActivies();
            })
            .then(function (licensedActivites) {
                retrievedRecords.forEach(function (record) {
                    var activityType = me.getFieldValueFromRecord('menu_id', record);
                    var isLicensed = me.isActivityLicensed(activityType, licensedActivites);

                    record.fieldValues.push({
                        fieldName: 'is_licensed',
                        fieldValue: isLicensed
                    });
                });

                return Promise.resolve(retrievedRecords);
            });
    },

    getFieldValueFromRecord: function (fieldName, record) {
        var i,
            value = null;

        for (i = 0; i < record.fieldValues.length; i++) {
            if (record.fieldValues[i].fieldName === fieldName) {
                value = record.fieldValues[i].fieldValue;
                break;
            }
        }

        return value;
    },

    isActivityLicensed: function (activityType, licensedActivities) {
        var me = this,
            isLicensed = 1,
            activityToCheck,
            license;


        // Get activity from the license map;
        activityToCheck = me.licenseMap[activityType];

        if (activityToCheck) {
            license = licensedActivities[activityToCheck];
            if (!license) {
                isLicensed = 0;
            }
        }

        return isLicensed;
    }
});
