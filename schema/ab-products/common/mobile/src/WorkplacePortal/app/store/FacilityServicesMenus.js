Ext.define('WorkplacePortal.store.FacilityServicesMenus', {
    extend: 'Common.store.sync.ValidatingTableStore',
    requires: 'WorkplacePortal.model.FacilityServicesMenu',

    serverTableName: 'activitytype',
    serverFieldNames: [
        'activity_type',
        'title',
        'description',
        'menu_icon',
        'mobile_action',
        'display_order'
    ],
    inventoryKeyNames: ['activity_type'],

    licenseMap: {
        'MOBILE_RESERVATIONS': 'AbWorkplaceReservations',
        'SERVICE DESK - COPY SERVICE': 'AbBldgOpsHelpDesk',
        'SERVICE DESK - DEPARTMENT SPACE': 'AbBldgOpsHelpDesk',
        'SERVICE DESK - FURNITURE': 'AbBldgOpsHelpDesk',
        'SERVICE DESK - HOTELING': 'AbSpaceHotelling',
        'SERVICE DESK - INDIVIDUAL MOVE': 'AbMoveManagement',
        'SERVICE DESK - MAINTENANCE': 'AbBldgOpsHelpDesk'
    },

    config: {
        model: 'WorkplacePortal.model.FacilityServicesMenu',

        remoteSort: true,
        sorters: [
            {
                property: 'display_order',
                direction: 'ASC'
            }
        ],
        tableDisplayName: LocaleManager.getLocalizedString('Facility Menu', 'WorkplacePortal.store.FacilityServicesMenus'),
        storeId: 'facilityServicesMenusStore',
        enableAutoLoad: true,
        disablePaging: true,
        proxy: {
            type: 'Sqlite'
        },

        restriction: [
            {
                tableName: 'activitytype',
                fieldName: 'group_name',
                operation: 'EQUALS',
                value: 'WORKSVC-MOB'
            }
        ],

        filters: [
            {property: 'is_licensed', value: 1}
        ]
    },

    retrieveRecords: function () {
        var me = this,
            retrievedRecords;

        return me.callParent(arguments)
            .then(function (records) {
                retrievedRecords = records;
                return MobileSecurityServiceAdapter.getLicensedActivies();
            })
            .then(function (licensedActivites) {
                retrievedRecords.forEach(function (record) {
                    var activityType = me.getFieldValueFromRecord('activity_type', record);
                    var isLicensed = me.isActivityLicensed(activityType, licensedActivites);

                    record.fieldValues.push({
                        fieldName: 'is_licensed',
                        fieldValue: isLicensed
                    });
                });

                return Promise.resolve(retrievedRecords);
            });
    },

    getFieldValueFromRecord: function (fieldName, record) {
        var i,
            value = null;

        for (i = 0; i < record.fieldValues.length; i++) {
            if (record.fieldValues[i].fieldName === fieldName) {
                value = record.fieldValues[i].fieldValue;
                break;
            }
        }

        return value;
    },

    isActivityLicensed: function (activityType, licensedActivities) {
        var me = this,
            isLicensed = 1,
            activityToCheck,
            license;


        // Get activity from the license map;
        activityToCheck = me.licenseMap[activityType];

        if (activityToCheck) {
            license = licensedActivities[activityToCheck];
            if (!license) {
                isLicensed = 0;
            }
        }

        return isLicensed;
    }
});