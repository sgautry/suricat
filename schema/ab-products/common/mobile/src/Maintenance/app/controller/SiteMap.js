Ext.define('Maintenance.controller.SiteMap', {
    extend: 'Ext.app.Controller',

    requires: 'Floorplan.util.Drawing',

    config: {
        refs: {
            mainView: 'mainview',
            siteMap: 'siteMapPanel'
        },

        control: {
            'button[itemId=sitePlanButton]': {
                tap: 'onShowSiteMap'
            }
        }
    },

    /**
     * Loads the site map
     *
     */
    loadSiteMap: function () {
        var siteMapPanel = this.getSiteMap(),
            record = siteMapPanel.getRecord();

        this.loadDrawing(record, siteMapPanel);
    },

    // TODO: Similar to Maintenance.controller.FloorPlan#loadDrawing
    loadDrawing: function (record, view) {
        var me = this,
            sitePanel = view,
            blId = record.get('bl_id'),
            siteId = record.get('site_id'),
            highlightParameters = [
                {
                    view_file: 'ab-sp-space-book-bl.axvw',
                    hs_ds: 'ds_ab-sp-space-book-bl_blHighlight',
                    label_ds: 'ds_ab-sp-space-book-bl_blLabel',
                    label_ht: '1'
                }
            ];

        me.getDrawingFromStoreOrRetrieveIfNot(siteId, highlightParameters)
            .then(function(svgData) {
                if (!Ext.isEmpty(svgData)) {
                    sitePanel.setSvgData(svgData);
                    sitePanel.setEventHandlers([
                        {
                            'assetType': 'bl',
                            'handler': view.onClickBuilding,
                            'scope': view
                        }
                    ]);
                    sitePanel.findAssets(blId, {});
                    Floorplan.util.Drawing.saveSiteDrawing(siteId, svgData)
                        .then(null, function(error) {
                            Log.log(error, 'error');
                        });
                }
            }, function(error) {
                Log.log(error, 'error');
            });
    },


    /**
     * Retreive the svg data from the {@link Floorplan.store.SiteDrawings} store. If the svg data does
     * not exist in the store for the given site id then try to retrieve the data from Web Central.
     * @param {String} siteId
     * @param {Object} highlightParameters
     */
    getDrawingFromStoreOrRetrieveIfNot: function (siteId, highlightParameters) {
        var siteStore = Ext.getStore('siteDrawings'),
            promiseChain = function() {
                return Floorplan.util.Drawing.retrieveSvgDataFromServer({site_id:siteId}, null, highlightParameters, true);
            };

        return siteStore.retrieveSingleRecord({property: 'site_id', value: siteId})
            .then(function(record) {
                if(record !== null) {
                    return Promise.resolve([record.get('svg_data')]);
                } else {
                    return Common.service.Session.doInSession(promiseChain, true);
                }
            });
    },

    onShowSiteMap: function () {
        var me = this,
            view = Ext.create('Maintenance.view.SiteMap'),
            record = me.getMainView().getNavigationBar().getCurrentView().getRecord();

        view.setRecord(record);
        me.loadSiteMap(record);
        me.getMainView().push(view);
    }
});