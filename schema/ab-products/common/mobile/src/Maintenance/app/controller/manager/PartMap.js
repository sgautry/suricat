/**
 * Controller for the Part Storage Locations Map view.
 * @author Jia
 * @since 23.1
 */
Ext.define('Maintenance.controller.manager.PartMap', {
    extend: 'Ext.app.Controller',
    requires:[

    ],
    config: {
        
        refs: {
            estimatePartsForm: 'estimateFormParts',
            workRequestPartEditPanel: 'workRequestPartEditPanel',
            requestDetailsPanel: 'requestDetailsPanel',
            addPartForm: 'estimateFormParts > #addPartForm',
            partMap: 'partmap esrimap',
            partMapView: 'partmap'
        },
        
        control: {

            //KB#3052029 Add Parts Storage Location map to the mobile application
            'estimateFormParts prompt[name=part_id]': {
                optiontap: 'onShowPartMap'
            },
            'workRequestPartEditPanel prompt[name=part_id]': {
                optiontap: 'onShowPartMap'
            },
            //KB#3052029 Add Parts Storage Location map to the mobile application
            'estimateFormParts prompt[name=pt_store_loc_id]': {
                optiontap: 'onShowStorageLocationMap'
            },
            'workRequestPartEditPanel prompt[name=pt_store_loc_id]': {
                optiontap: 'onShowStorageLocationMap'
            },
            partMap: {
                mapLoaded: 'onMapReady'
            }
            
        }
    },

    /**
     * KB#3052029 Add Parts Storage Location map to the mobile application
     */
    onShowPartMap: function (partPrompt) {
        var me = this,
            searchValue;

        //get text value of Part Code from search text field in prompt panel.
        searchValue = partPrompt.listPanel.down('search').getValue();
        if(!Ext.isEmpty(searchValue)){
            searchValue=searchValue.toUpperCase();
        }

        //Fixed KB#3053278 Use part code prompt search result to determine part availability.
        me.getPartCodeFromExactMatchFilter(partPrompt,searchValue).then(function(partCodeFromExactFilter){
            return me.getPartCodeFromAnyMatchFilter(partPrompt,searchValue,partCodeFromExactFilter);
        }).then(function(partCode){
            me.showMapViewByPartCode(partPrompt,partCode);
        });
    },

    /**
     * KB#3053278 Parts map should use the search results to determine part availability.
     * Get part code from exact match filter.
     * @param partPrompt Part Code prompt control object.
     * @param searchResult Search result from prompt search field.
     * @returns {*}
     */
    getPartCodeFromExactMatchFilter: function(partPrompt,searchResult){
        var me=this,
            exactMatchFilters=[],
            ptLocStore=Ext.getStore('partStorageLocStore'),
            returnPartCode="",
            storeLocIdFilter;

        //exact match filter.
        exactMatchFilters.push(Ext.create('Common.util.Filter', {
            property: 'part_id',
            value: searchResult,
            exactMatch:true,
            anyMatch:false
        }));

        //get storeage location field filter from part prompt parent form,push this filter into axact match filters.
        storeLocIdFilter=me.getPtStoreLocIdFilterFromPartPrompt(partPrompt);
        if(!Ext.isEmpty(storeLocIdFilter)){
            exactMatchFilters.push(storeLocIdFilter);
        }

        if(Ext.isEmpty(searchResult)){
            return Promise.resolve("");
        }else{
            return ptLocStore.retrieveAllRecords(exactMatchFilters).then(function(records){
                if(records.length>0){
                    returnPartCode=searchResult;
                }
                return Promise.resolve(returnPartCode);
            });
        }

    },

    /**
     * KB#3053278 Parts map should use the search results to determine part availability.
     * Get part code from any match filter.
     * @param partPrompt Part Code prompt control object.
     * @param searchResult Search result from prompt search field.
     * @param partCodeFromExactFilter Part code that got from exact match filter.
     * @returns {*}
     */
    getPartCodeFromAnyMatchFilter: function(partPrompt,searchResult,partCodeFromExactFilter){
        var me=this,
            anyMatchfilters=[],
            ptLocStore=Ext.getStore('partStorageLocStore'),
            returnPartCode="",//part code from any match filter.
            firstPartCodeInList,//the first part code in retrived records, used to compare with part code value of other items.
            isTheSameValueInItem=true,//flag that whether all items are the same part code value.
            storeLocIdFilter;

        //Any match filter.
        anyMatchfilters.push(Ext.create('Common.util.Filter', {
            property: 'part_id',
            value: searchResult,
            anyMatch:true
        }));
        //get storeage location field filter from part prompt parent form,push this filter into anyMatch filters.
        storeLocIdFilter=me.getPtStoreLocIdFilterFromPartPrompt(partPrompt);
        if(!Ext.isEmpty(storeLocIdFilter)){
            anyMatchfilters.push(storeLocIdFilter);
        }
        //If exact match filter doesn't get any part code, then get part code by using any match filter.
        if(Ext.isEmpty(partCodeFromExactFilter)){
            if(Ext.isEmpty(searchResult)){
                //if search result is empty, then set part code is empty.
                return Promise.resolve("");
            }else{
                //retrive all any match records.
                return ptLocStore.retrieveAllRecords(anyMatchfilters).then(function(records){
                    if(records.length>0){
                        firstPartCodeInList=records[0].get('part_id');
                        for(var i=1;i<records.length;i++){
                            if(records[i].get('part_id')!==firstPartCodeInList){
                                isTheSameValueInItem=false;
                                break;
                            }
                        }

                        if(isTheSameValueInItem){
                            returnPartCode=firstPartCodeInList;
                        }
                    }

                    return Promise.resolve(returnPartCode);
                });
            }

        }else{
            return Promise.resolve(partCodeFromExactFilter);
        }
    },
    /**
     * KB#3053278 Parts map should use the search results to determine part availability.
     * When user input value into Storage Location Code field. then Part Code field should inherit this filter.
     * @param partPrompt Part Code prompt control object.
     * @return {storeLocIdFilter} Filter of Storage Location Code field value.
     */
    getPtStoreLocIdFilterFromPartPrompt: function(partPrompt){
        var storeLocIdFilter,
            ptStoreLocId,
            ptStoreLocField;
        //get Storage Location Code field.
        ptStoreLocField=partPrompt.parent.query('prompt[itemId=pt_store_loc_id]')[0];
        if(!Ext.isEmpty(ptStoreLocField)){
            //Create filter by Storage Location Code.
            ptStoreLocId=ptStoreLocField.getValue();
            if(!Ext.isEmpty(ptStoreLocId)){
                storeLocIdFilter=Ext.create('Common.util.Filter', {
                    property: 'pt_store_loc_id',
                    value: ptStoreLocId.toUpperCase(),
                    conjunction: 'AND',
                    exactMatch: true,
                    anyMatch: false
                });
            }

        }
        return storeLocIdFilter;
    },
    /**
     * Show map view by Part Code which got from prompt search result.
     * @param partPrompt Part Code prompt control.
     * @param partCode Part Code which got from prompt search result.
     */
    showMapViewByPartCode: function(partPrompt,partCode){
        var me = this,
            wrIds=[],
            selectedMultipleWRs,
            wrId;

        selectedMultipleWRs=Ext.getStore('workRequestsStore').getSelectedWorkRequests();

        if(selectedMultipleWRs.length>0){
            for(var i=0;i<selectedMultipleWRs.length;i++){
                wrId=selectedMultipleWRs[i].data.wr_id.getValue();
                wrIds.push(wrId);
            }
        }else{
            //get work request code value from form.
            if(me.getRequestDetailsPanel()){
                wrId=me.getRequestDetailsPanel().query('hiddenfield[name=wr_id]')[0].getValue();
                wrIds.push(wrId);
            }
            //If add part form is from MyWork tab, get workRequest Code from form.
            if(me.getWorkRequestPartEditPanel()){
                wrId=me.getWorkRequestPartEditPanel().getRecord().data.wr_id.getValue();
                wrIds.push(wrId);
            }
        }

        // Moved the config items to the AddPartMap class
        me.mapView = Ext.create('Maintenance.view.manager.AddPartMap');
        Ext.Viewport.add(me.mapView);

        me.mapView.setPartCode(partCode);
        me.mapView.setWrIds(wrIds);
        me.mapView.setCallBack(function(storageLocationId,partId){
            me.onSelectStorageLocation(storageLocationId,partId,partPrompt);
        });
        me.mapView.show();
    },

    /**
     * Show storage location map view for storage location field.
     * @param storageLocationPrompt
     */
    onShowStorageLocationMap: function(storageLocationPrompt){
        var me = this,
            searchValue,
            selectedMultipleWRs,
            wrIds=[],
            wrId;

        selectedMultipleWRs=Ext.getStore('workRequestsStore').getSelectedWorkRequests();

        if(selectedMultipleWRs.length>0){
            for(var i=0;i<selectedMultipleWRs.length;i++){
                wrId=selectedMultipleWRs[i].data.wr_id.getValue();
                wrIds.push(wrId);
            }
        }else{
            //get work request code value from form.
            if(me.getRequestDetailsPanel()) {
                wrId = me.getRequestDetailsPanel().query('hiddenfield[name=wr_id]')[0].getValue();
                wrIds.push(wrId);
            }

            //If add part form is from MyWork tab, get workRequest Code from form.
            if(me.getWorkRequestPartEditPanel()){
                wrId=me.getWorkRequestPartEditPanel().getRecord().data.wr_id.getValue();
                wrIds.push(wrId);
            }
        }
        //get text value from search text field in prompt panel.
        searchValue = storageLocationPrompt.listPanel.down('search').getValue();

        // Moved the config items to the AddPartMap class
        me.mapView = Ext.create('Maintenance.view.manager.AddPartMap');
        Ext.Viewport.add(me.mapView);

        me.mapView.setStorageLocationCode(searchValue);
        me.mapView.setWrIds(wrIds);
        me.mapView.setCallBack(function(storageLocationId){
            me.onSelectStorageLocationByStoreLocation(storageLocationId,storageLocationPrompt);
        });
        me.mapView.show();
    },
    /**
     *  Callback: when select storage location code for part prompt view.
     * @param storageLocationId
     * @param partId
     */
    onSelectStorageLocation: function(storageLocationId,partId,partPrompt){
        var me=this,
            fieldSet;
        if(!Ext.isEmpty(partId)&&!Ext.isEmpty(storageLocationId)){
            //Close prompt view and set value to add Part form.
            partPrompt.hidePromptView();
            if(me.getAddPartForm()){
                fieldSet=me.getAddPartForm().getItems();
                fieldSet.get('part_id').setValue(partId);
                fieldSet.get('pt_store_loc_id').setValue(storageLocationId);
            }
            if(me.getWorkRequestPartEditPanel()){
                me.getWorkRequestPartEditPanel().query('prompt[name=part_id]')[0].setValue(partId);
                me.getWorkRequestPartEditPanel().query('prompt[name=pt_store_loc_id]')[0].setValue(storageLocationId);
            }
        }

        if(Ext.isEmpty(partId)&&!Ext.isEmpty(storageLocationId)){
            //Reset filter of part prompt view
            partPrompt.resetFilter();
            partPrompt.listPanel.down('search').setValue(storageLocationId);
            partPrompt.onApplyFilter(storageLocationId);
        }
    },
    /**
     * callback: when select storage location code for storage location prompt view.
     * @param storageLocationId Storage location code
     * @param storageLocationPrompt Storage location prompt view
     */
    onSelectStorageLocationByStoreLocation: function(storageLocationId,storageLocationPrompt){
        var me=this,
            fieldSet;
        if(!Ext.isEmpty(storageLocationId)){
            //Close prompt view.
            storageLocationPrompt.hidePromptView();
            //reset value to storage location code.
            if(me.getAddPartForm()){
                fieldSet=me.getAddPartForm().getItems();
                fieldSet.get('pt_store_loc_id').setValue(storageLocationId);
            }

            if(me.getWorkRequestPartEditPanel()){
                me.getWorkRequestPartEditPanel().query('prompt[name=pt_store_loc_id]')[0].setValue(storageLocationId);
            }
        }
    },

    /**
     * Called when the Esri map has completed initialization and loading.
     * @param {Map} map
     */
    onMapReady: function(map) {
        var partCode=Ext.getCmp('partMapCmp').getPartCode();
        this.showWorkRequestMarkers(map);
        if(!Ext.isEmpty(partCode)){
            this.showStorageLocationMarkersByPartCode(map);
        }else{
            this.showStorageLocationMarkers(map);
        }
    },

    /**
     * Show storage location markers by part code.
     */
    showStorageLocationMarkersByPartCode: function (map) {
        var filters = [];
        // show the markers
        map.showMarkers('storageLocationBuildingForParts', filters);
    },
    /**
     * Show storage location markers without part code
     */
    showStorageLocationMarkers: function (map) {
        var filters = [],
            filter,
            storageLocationCode;

        storageLocationCode=Ext.getCmp('partMapCmp').getStorageLocationCode();
        //If storage location is exists , add filter to show only this storage location in map view.
        if(!Ext.isEmpty(storageLocationCode)){
            //create filter by storage location code.
            filter = Ext.create('Common.util.Filter', {
                property: 'pt_store_loc_id',
                value: storageLocationCode,
                conjunction: 'AND',
                anyMatch: false
            });
            filters.push(filter);
        }

        // show the storage location markers
        map.showMarkers('storageLocationBuilding', filters);

    },

    /**
     * Show work request marker.
     * @param map
     */
    showWorkRequestMarkers: function(map){
        var filters=[],
            filter,
            wrIds=Ext.getCmp('partMapCmp').getWrIds();

        if(!Ext.isEmpty(wrIds)){
            for(var i=0;i<wrIds.length;i++){
                //create filter by storage location code.
                filter = Ext.create('Common.util.Filter', {
                    property: 'wr_id',
                    value: wrIds[i],
                    conjunction: 'OR',
                    anyMatch: true
                });
                filters.push(filter);
            }

        }

        //var isWorkRequestSameWithPartLocation=me.checkIsWorkRequestSameWithPartLocation(wrId,storageLocationCode);
        // show the storage location markers
        map.showMarkers('workRequestBuilding', filters);

    },

    /**
     * //TODO:If work request building is same as the storage location building, does not show work request marker.
     * Check If work request building the same with storage location building.
     * @param wrId Work request code
     * @param storageLocationCode Storage location code
     * @returns {boolean} If same with storage location, return true. else, return false.
     */

    checkIsWorkRequestSameWithPartLocation: function(wrId,storageLocationCode){
        var isSameWithPartBuilding=false,
            store=StorageLocationMapUtil.getWorkRequestByPartBuildingStore(wrId,storageLocationCode),
            count=store.getCount();
        if(count>0){
            isSameWithPartBuilding=true;
        }

        return isSameWithPartBuilding;
    }

    
    
});