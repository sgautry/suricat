Ext.define('Maintenance.controller.WorkRequestForms', {
    extend: 'Ext.app.Controller',

    requires: ['Maintenance.view.overlay.HoldAction'],

    config: {
        refs: {
            mainView: 'mainview',
            workRequestEditPanel: 'workRequestPanel',
            returnCfPanel: 'ReturnCfPanel',
            confirmReturnCfButton: 'button[itemId=confirmReturnCfButton]',
            confirmComplete: 'button[itemId=confirmCompleteButton]',
            completeRadioOptions: 'radiofield[checked=true]',
            cancelButton: 'button[action=workRequestCancel]',
            quickCompleteView: 'quickCompletePanel',
            //KB#3050226 Add verify functionality.
            verifyForm: 'verifyform',
            verifyConfirmButton: 'button[itemId=confirmVerfifyWorkRequest]',
            verfifyInCompleteButton:'button[itemId=returnIncompleteWorkRequest]'
        },
        control: {
            confirmReturnCfButton: {
                tap: 'onConfirmReturnCfButtonTapped'
            },
            confirmComplete: {
                tap: 'onConfirmCompleteButtonTapped'
            },
            cancelButton: {
                tap: 'onCancelButtonTapped'
            },
            'button[action=quickCompleteCancel]': {
                tap: 'onQuickCompleteCancel'
            },
            quickCompleteView: {
                'quickcompletesave': 'onQuickCompleteSave',
                'remove': 'onQuickCompleteItemRemoved'
            },
            verifyConfirmButton:{
                tap:'onConfirmVerifyButtonTapped'
            },
            verfifyInCompleteButton:{
                tap:'onVerifyIncompleteButtonTapped'
            }
        },

        returnCommentNotEmptyMessage: LocaleManager.getLocalizedString(
            'Please input the comments first',
            'Maintenance.controller.WorkRequestForms'),

        workRequestCancelActionTitle: LocaleManager.getLocalizedString(
            'Cancel',
            'Maintenance.controller.WorkRequestForms'),
        workRequestCancelActionMessage: LocaleManager.getLocalizedString(
            'This action cancels a request before any work has begun. Canceling a request ends the request workflow.<br/><br/>Cancel the work request?',
            'Maintenance.controller.WorkRequestForms')

    },

    onReturnCfButtonTapped: function () {
        // Get the work request record
        var me = this,
            returnCfForm,
            workRequestPanel = me.getMainView().getNavigationBar().getCurrentView(),
            workRequestRecord = workRequestPanel.getRecord();
        returnCfForm = Ext.create('Maintenance.view.manager.ReturnCfForm');
        returnCfForm.setRecord(workRequestRecord);
        me.getMainView().push(returnCfForm);
    },


    onConfirmReturnCfButtonTapped: function () {
        // Get the work request record
        var me = this,
            returnCfForm = me.getReturnCfPanel(),
            workRequestStore = Ext.getStore('workRequestsStore'),
            record = returnCfForm.getRecord(),
            comments = record.get('mob_step_comments'),
            errorMessages = [];

        returnCfForm.removeErrorPanelIfExists();
        if (!comments) {
            errorMessages.push({fieldName: 'mob_step_comments', errorMessage: me.getReturnCommentNotEmptyMessage()});
            if (errorMessages.length > 0) {
                returnCfForm.removeErrorPanelIfExists();

                returnCfForm.insert(0, {
                    xtype: 'errorpanel',
                    errorMessages: errorMessages
                });
            }

            return;
        }

        workRequestStore.setAutoSync(false);
        record.setMobileStepActionChanged('returnCf');
        workRequestStore.sync(function () {
            workRequestStore.setAutoSync(true);
            NavigationUtil.navigateBack(me.getMainView());
        });

    },

    onConfirmCompleteButtonTapped: function () {
        // Get the work request record
        var me = this,
            workRequestStore = Ext.getStore('workRequestsStore'),
            wrRecords = me.getMainView().getNavigationBar().getCurrentView().getWrRecords(),
            workRequestRecord = me.getMainView().getNavigationBar().getCurrentView().getRecord(),
            completeAction = me.getCompleteRadioOptions().getValue();

        workRequestStore.setAutoSync(false);
        if (Ext.isEmpty(wrRecords)) {
            workRequestRecord.setMobileStepActionChanged(completeAction);
        } else {
            for (var i = 0; i < wrRecords.length; i++) {
                wrRecords[i].setMobileStepActionChanged(completeAction);
            }
        }
        workRequestStore.sync(function () {
            workRequestStore.setAutoSync(true);
            me.getMainView().reset();
        });
    },

    onCancelButtonTapped: function (record) {
        var me = this;
        Ext.Msg.confirm(me.getWorkRequestCancelActionTitle(), me.getWorkRequestCancelActionMessage(),
            function (buttonId) {
                if (buttonId === 'yes') {
                    me.setStatusField('Can');
                    me.saveWorkRequestRecord(function () {
                        NavigationUtil.navigateBack(me.getMainView());
                    });
                }
            });
    },

    checkCraftspersonRecords: function () {
        var store = Ext.getStore('workRequestCraftspersonsStore'),
            record, totalHours;

        if (store.getCount() === 1) {
            // Check labor hours
            record = store.getAt(0);
            totalHours = record.getTotalHours();
            if (totalHours === 0) {
                return record;
            }
        }
        return null;
    },

    /**
     * Checks if all of the craftsperson records for this Work Request have labor hours populated.
     * @returns {boolean} true if all craftsperson records have labor data entered, false otherwise.
     */
    checkCraftspersonHours: function () {
        var craftsPersonsStore = Ext.getStore('workRequestCraftspersonsStore'),
            ln = craftsPersonsStore.getCount(),
            i;

        for (i = 0; i < ln; i++) {
            if (craftsPersonsStore.getAt(i).getTotalHours() === 0) {
                return false;
            }
        }
        return true;
    },

    onQuickCompleteSave: function (cfRecord) {
        // Check if the cf record is valid
        var me = this,
            workRequestPanel = me.getWorkRequestEditPanel(),
            workRequestRecord = workRequestPanel.getRecord(),
            workRequestStore = Ext.getStore('workRequestsStore'),
            completeAction = workRequestRecord.get('is_req_supervisor') === 1 ? 'supervisorComplete' : 'cfComplete',
            craftspersonRecord;

        if (cfRecord.isValid()) {
            //craftspersonStore = Ext.getStore('workRequestCraftspersonsStore');
            craftspersonRecord = me.quickCompletePanel.getRecord();
            craftspersonRecord.set('mob_is_changed', 1);
            workRequestStore.setAutoSync(false);
            workRequestRecord.setMobileStepActionChanged(completeAction);
            workRequestStore.sync(function () {
                workRequestStore.setAutoSync(true);
                NavigationUtil.navigateBack(me.getMainView());
            });
        } else {
            me.quickCompletePanel.displayErrors(cfRecord);
            if (!Ext.os.is.Phone) {
                me.quickCompletePanel.setHeight(420);
            }
            return;
        }
        me.quickCompletePanel.destroy();
    },

    onQuickCompleteCancel: function () {
        var cfRecord,
            craftspersonStore = Ext.getStore('workRequestCraftspersonsStore');

        if (this.quickCompletePanel) {
            // Set the hour values back to 0 if they were set
            craftspersonStore.setAutoSync(false);
            cfRecord = this.quickCompletePanel.getRecord();
            cfRecord.set('hours_straight', 0);
            cfRecord.set('hours_over', 0);
            cfRecord.set('hours_double', 0);
            craftspersonStore.sync(function () {
                craftspersonStore.setAutoSync(true);
            });
            this.quickCompletePanel.destroy();
        }
    },

    onQuickCompleteItemRemoved: function (panel) {
        if (!Ext.os.is.Phone) {
            panel.setHeight(300);
        }
    },

    setStatusField: function (status) {
        var view = this.getMainView().getNavigationBar().getCurrentView(),
            record = view.getRecord(),
            workRequestStore = Ext.getStore('workRequestsStore');

        workRequestStore.setAutoSync(false);
        record.set('status', status);
        record.setMobileStatusChanged(record.get('status_initial') !== record.get('status') ? 1 : 0);
        view.setValues({
            'status': status
        });
        workRequestStore.sync(function () {
            workRequestStore.setAutoSync(true);
        });
    },

    saveWorkRequestRecord: function (onCompleted, scope) {
        var workRequestRecord = this.getMainView().getNavigationBar().getCurrentView().getRecord(),
            status = workRequestRecord.get('status'),
            workRequestStore = Ext.getStore('workRequestsStore');

        workRequestStore.setAutoSync(false);
        workRequestRecord.setMobileStatusChanged(workRequestRecord.get('status_initial') !== workRequestRecord.get('status') ? 1 : 0);
        workRequestRecord.set('mob_pending_action', status);
        workRequestStore.sync(function () {
            workRequestStore.setAutoSync(true);
            Ext.callback(onCompleted, scope);
        });
    },

    onConfirmVerifyButtonTapped: function () {
        // Get the work request record
        var me = this,
            verifyForm = me.getVerifyForm(),
            workRequestStore = Ext.getStore('workRequestsStore'),
            record = verifyForm.getRecord(),
            comments = record.get('mob_step_comments'),
            errorMessages = [];

        verifyForm.removeErrorPanelIfExists();
        if (!comments) {
            errorMessages.push({fieldName: 'mob_step_comments', errorMessage: me.getReturnCommentNotEmptyMessage()});
            if (errorMessages.length > 0) {
                verifyForm.removeErrorPanelIfExists();

                verifyForm.insert(0, {
                    xtype: 'errorpanel',
                    errorMessages: errorMessages
                });
            }

            return;
        }

        workRequestStore.setAutoSync(false);
        record.setMobileStepActionChanged('verify');
        workRequestStore.sync(function () {
            workRequestStore.setAutoSync(true);
            NavigationUtil.navigateBack(me.getMainView());
        });

    },

    onVerifyIncompleteButtonTapped: function(){
        // Get the work request record
        var me = this,
            verifyForm = me.getVerifyForm(),
            workRequestStore = Ext.getStore('workRequestsStore'),
            record = verifyForm.getRecord();

        workRequestStore.setAutoSync(false);
        record.setMobileStepActionChanged('verifyIncomplete');
        workRequestStore.sync(function () {
            workRequestStore.setAutoSync(true);
            NavigationUtil.navigateBack(me.getMainView());
        });
    }
});