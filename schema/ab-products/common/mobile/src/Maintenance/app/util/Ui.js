/**
 * Holds common UI functions
 *
 * @author Cristina Moldovan
 * @since 21.3
 */
Ext.define('Maintenance.util.Ui', {
    alternateClassName: ['UiUtil'],

    singleton: true,

    formatHours: function (hours_straight, hours_over, hours_double) {
        var totalHours = hours_straight + hours_over + hours_double;
        return this.formatHour(totalHours);
    },

    formatHour: function (hour) {
        return Common.util.Format.formatNumber(hour, 2);
    },
    /**
     * Add black view title bar.
     * @param scope
     */
    addBlackTitleBar: function(scope){
        //add a black title bar
        var title='',
            titlePanel;
        if(Ext.isFunction(scope.getTitle)){
            title=scope.getTitle();
        }
        titlePanel = Ext.factory({docked: 'top', title: title}, Common.control.TitlePanel);
        scope.add(titlePanel);
    },
    /**
     * Get list title template for work request resource list.
     * @param displayTitles
     * @returns {*|String}
     */
    getResourceListTitleTemplate: function(displayTitles){
        var me=this,
            headerTemplate = '<div class="prompt-list-label">{0}</div>',
            headerItemTemplate = '<h3 style="width:xx%">{0}</h3>',
            templateText='',
            width=30;

        if(displayTitles.length>0){
            width=100/displayTitles.length;
        }

        headerItemTemplate=headerItemTemplate.replace('xx', width.toString());

        Ext.each(displayTitles, function (title) {
            templateText += Ext.String.format(headerItemTemplate, title);
        }, me);

        return Ext.String.format(headerTemplate, templateText);
    },

    /**
     * Reset work request resource list height of Craftperson,Tools,Parts.
     *
     * @param listControl List companent
     * @param listType WorkRequestFilter.listType
     * @param isMultipleSelection Is multiple selection.
     */
    resetWrResourcesListHeight: function(listControl,listType,isMultipleSelection){
        if(listType === Constants.MyWork||listType === Constants.Issued||listType === Constants.Completed){
            if(!isMultipleSelection){
                listControl.setHeight('28em');
            }
        }
    }
});