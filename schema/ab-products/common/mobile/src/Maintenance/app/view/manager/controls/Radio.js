/**
 * Created by Jia on 2017/6/14.
 */
/**
 * This control is extend from Common.control.field.Radio.
 * Common.control.field.Radio doesn't support readOnly property , so add readOnly property in this class to make Common.control.field.Radio support readOnly property.
 * @since 23.2
 * @author Jia
 */
Ext.define('Maintenance.view.manager.controls.Radio', {
    extend: 'Common.control.field.Radio',
    xtype: 'maintenancecommonradiofield',

    config: {
        readOnly: false
    },

    applyReadOnly: function (config) {
        var me = this;

        if (config) {
            me.element.addCls('x-compose-readonly');
        } else {
            me.element.removeCls('x-compose-readonly');

        }
        me.setDisabled(config);

        return config;
    }
});
