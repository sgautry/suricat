Ext.define('Maintenance.view.manager.EstimateFormParts', {
    extend: 'Ext.Panel',

    xtype: 'estimateFormParts',

    itemTemplate: '<div class="prompt-list-hbox"><h1 style="width:33.3%;text-align:left">{[this.getPartDisplayValue(values.wr_id, values.part_id)]}</h1>' +
    '<div style="width:33.3%;text-align:left">{[values.pt_store_loc_id]}</div>'+
    '<div style="width:33.3%;text-align:left">{[this.getQuantityDisplayValue(values.qty_estimated, values.qty_actual)]}</div></div>',

    config: {

        title: LocaleManager.getLocalizedString('Parts', 'Maintenance.view.manager.EstimateFormParts'),
        /**
         * True/false if view is opened for multiple selection (multiple work requests) or single selection
         */

        multipleSelection: false,

        displayMode: null,

        layout: 'vbox',

        items: [
            {
                xtype: 'titlebar',
                itemId: 'addPartToolBar',
                cls: 'res-form-header',
                items: [
                    //KB#3052028   Allow craftspersons to Add Purchased Parts on the mobile application
                    {
                        xtype:'button',
                        text: LocaleManager.getLocalizedString('Add Purchased Parts','Maintenance.view.manager.EstimateFormParts'),
                        action:'addPartsToInventory',
                        ui: 'action',
                        align:'right'
                    },
                    {
                        xtype: 'title',
                        itemId: 'part_titlebar_title',
                        title: LocaleManager.getLocalizedString('Add Part', 'Maintenance.view.manager.EstimateFormParts')
                    }
                ]
            },
            {
                xtype: 'fieldset',
                itemId: 'addPartForm',
                defaults: {
                    labelWidth: '40%',
                    labelWrap: true,
                    labelCls: Ext.os.is.Phone ? 'x-form-label-phone' : '',
                    stepValue: 0.1,
                    minValue: 0,
                    readOnly: true,
                    hidden: true
                },
                items: [
                    {
                        xtype: 'prompt',
                        name: 'part_id',
                        itemId: 'part_id',
                        title: LocaleManager.getLocalizedString('Part', 'Maintenance.view.manager.EstimateFormParts'),
                        label: LocaleManager.getLocalizedString('Part Code', 'Maintenance.view.manager.EstimateFormParts'),
                        required: true,
                        store: 'partStorageLocStore',
                        parentFields: ['pt_store_loc_id'],
                        displayFields: [
                            {
                                name: 'part_id',
                                title: LocaleManager.getLocalizedString('Part Code', 'Maintenance.view.manager.EstimateFormParts')
                            },
                            {
                                name: 'pt_store_loc_id',
                                title: LocaleManager.getLocalizedString('Storage Location Code', 'Maintenance.view.manager.EstimateFormParts')
                            },
                            //KB#3053142 Add Quantity on avaliable to Part code pop-up
                            {
                                name: 'qty_on_hand',
                                title: LocaleManager.getLocalizedString('Quantity Available', 'Maintenance.view.manager.EstimateFormParts')
                            }
                        ],
                        optionButton: {
                            iconCls: 'locate'
                        },
                        //KB#3053237 add refresh button to part code field to sync PT_STORE_LOC_PT table.
                        displayRefreshButton: true
                    },
                    
                    {
                        xtype: 'prompt',
                        name: 'pt_store_loc_id',
                        itemId: 'pt_store_loc_id',
                        title: LocaleManager.getLocalizedString('Storage Location Code', 'Maintenance.view.manager.EstimateFormParts'),
                        label: LocaleManager.getLocalizedString('Storage Location Code', 'Maintenance.view.manager.EstimateFormParts'),
                        required: true,
                        store: 'storageLocStore',
                        displayFields: [
                            {
                                name: 'pt_store_loc_id',
                                title: LocaleManager.getLocalizedString('Storage Location Code', 'Maintenance.view.manager.EstimateFormParts')
                            },
                            {
                                name: 'pt_store_loc_name',
                                title: LocaleManager.getLocalizedString('Storage Location Name', 'Maintenance.view.manager.EstimateFormParts')
                            },
                            {
                                name: 'bl_id',
                                title: LocaleManager.getLocalizedString('Building Code', 'Maintenance.view.manager.EstimateFormParts')
                            }
                        ],
                        optionButton: {
                            iconCls: 'locate'
                        }
                    },
                    
                    {
                        xtype: 'localizedspinnerfield',
                        label: LocaleManager.getLocalizedString('Quantity Estimated', 'Maintenance.view.manager.EstimateFormParts'),
                        name: 'qty_estimated',
                        itemId: 'qty_estimated'
                    },
                    {
                        xtype: 'localizedspinnerfield',
                        label: LocaleManager.getLocalizedString('Quantity Used', 'Maintenance.view.manager.EstimateFormParts'),
                        name: 'qty_actual',
                        itemId: 'qty_actual'
                    }
                ]
            },
            {
                xtype: 'toolbar',
                cls: 'res-form-header',
                items: [
                    {
                        xtype: 'title',
                        title: LocaleManager.getLocalizedString('Parts', 'Maintenance.view.manager.EstimateFormParts')
                    }
                ]
            },
            {
                xtype:'container',
                itemId:'listTitle',
                html:''
            },
            {
                xtype: 'list',
                store: 'workRequestPartsStore',
                // TODO: Use standard format notation - number:(v,2) instead of calling function
                itemTpl: '',
                itemId: 'partsList',
                height: Ext.os.is.Phone ? '8em' : '6em',
                // Set empty text
                emptyText: Ext.String.format(LocaleManager.getLocalizedString('{0} Tap {1} to add new parts{2}',
                    'Maintenance.view.manager.EstimateFormParts'), '<div class="empty-text">', '<span class="ab-add-icon"></span>', '</div>')
            }
        ]
    },

    initialize: function () {
        var me = this,
            list = me.down('list'),
            displayMode,
            listTitle=me.down('container[itemId=listTitle]'),
            template = me.itemTemplate,
            formattedTpl = template,
            wrIdTitle,
            partIdTitle,
            storeLocIdTitle,
            qtyEstimateTitle,
            qtyActualTitle,
            issuedOrCompletedList,
            xTpl = new Ext.XTemplate(formattedTpl, {
                getPartDisplayValue: function (wr_id, part_id) {
                    return '' + (me.getMultipleSelection() ? (wr_id + ' ') : '') + part_id;
                },
                getQuantityDisplayValue: function (qty_estimated, qty_actual) {
                    return Common.util.Format.formatNumber(issuedOrCompletedList ? qty_actual : qty_estimated, 2);
                }
            });

        if(me.getDisplayMode()){
            displayMode = me.getDisplayMode();
        }else{
            displayMode = WorkRequestFilter.listType;
        }

        issuedOrCompletedList= (displayMode === Constants.Issued|| displayMode === Constants.Completed ||displayMode===Constants.MyWork|| displayMode===Constants.EqMaintenanceHistory);

        //show list title
        if(listTitle && (!Ext.os.is.Phone)){
            partIdTitle=LocaleManager.getLocalizedString('Part Code', 'Maintenance.view.manager.EstimateFormParts');
            storeLocIdTitle=LocaleManager.getLocalizedString('Storage Location Code', 'Maintenance.view.manager.EstimateFormParts');
            qtyEstimateTitle=LocaleManager.getLocalizedString('Quantity Estimated', 'Maintenance.view.manager.EstimateFormParts');
            qtyActualTitle=LocaleManager.getLocalizedString('Quantity Used', 'Maintenance.view.manager.EstimateFormParts');

            listTitle.setHtml(UiUtil.getResourceListTitleTemplate([
                partIdTitle,
                storeLocIdTitle,
                issuedOrCompletedList?qtyActualTitle:qtyEstimateTitle
            ]));

        }

        if (list) {
            list.setItemTpl(xTpl);
        }

        //showList

        //add a black title bar
        UiUtil.addBlackTitleBar(me);

        me.callParent();

        me.displayFormFields();

        //set all fields readonly for history work request and other condition that have no rights to edit.
        FormUtil.setAllFormFieldsReadOnlyByConditions(me);

        //reset parts list height property.
        UiUtil.resetWrResourcesListHeight(list,WorkRequestFilter.listType,me.getMultipleSelection());
    },

    displayFormFields: function () {
        var me = this,
            displayMode,
            visibleFields,
            editableFields;

        if(me.getDisplayMode()){
            displayMode = me.getDisplayMode();
        }else{
            displayMode = WorkRequestFilter.listType;
        }

        if (displayMode === Constants.Issued || displayMode === Constants.Completed||displayMode === Constants.MyWork|| displayMode===Constants.EqMaintenanceHistory) {
            visibleFields = ['part_id', 'pt_store_loc_id','qty_estimated', 'qty_actual'];
            if (FormUtil.userCanEditResourcesAfterIssued()) {
                editableFields = ['part_id', 'pt_store_loc_id','qty_actual'];
            }
        } else {
            visibleFields = ['part_id', 'pt_store_loc_id','qty_estimated'];
            editableFields = ['part_id', 'pt_store_loc_id', 'qty_estimated'];
        }

        NavigationUtil.showFields(me, visibleFields, editableFields);
    }
});