Ext.define('Maintenance.view.manager.EstimateFormCosts', {
    extend: 'Ext.Panel',

    xtype: 'estimateFormCosts',

    itemTemplate: '<div class="prompt-list-hbox"><h1 style="width:25%;text-align:left">{[this.getCostDisplayValue(values.wr_id, values.other_rs_type)]}</h1>' +
        '<div style="width:25%;text-align:left">{[Common.util.Format.formatNumber(values.qty_used, 2)]}</div>' +
        '<div style="width:25%;text-align:left">{units_used}</div>' +
        '<div style="width:25%;text-align:left">' +
        '{[this.getQuantityDisplayValue(values.cost_total,values.cost_estimated)]}' +
        '</div></div>' +
        '<div class="prompt-list-vbox"><h3>{description}</h3></div>',

    config: {

        title: LocaleManager.getLocalizedString('Costs', 'Maintenance.view.manager.EstimateFormCosts'),
        /**
         * True/false if view is opened for multiple selection (multiple work requests) or single selection
         */
        multipleSelection: false,

        displayMode: null,

        layout:'vbox',

        items: [
            {
                xtype: 'titlebar',
                itemId: 'addCostToolBar',
                cls: 'res-form-header',
                items: [
                    {
                        xtype: 'title',
                        itemId: 'cost_titlebar_title',
                        title: LocaleManager.getLocalizedString('Add Other Cost', 'Maintenance.view.manager.EstimateFormCosts')
                    }
                ]
            },
            {
                xtype: 'fieldset',
                itemId: 'addOtherCostForm',
                defaults: {
                    labelWidth: '40%',
                    labelWrap: true,
                    labelCls: Ext.os.is.Phone ? 'x-form-label-phone' : '',
                    stepValue: 0.1,
                    minValue: 0,
                    readOnly: true,
                    hidden: true
                },
                items: [
                    {
                        xtype: 'selectlistfield',
                        label: LocaleManager.getLocalizedString('Other Resource Type', 'Maintenance.view.manager.EstimateFormCosts'),
                        name: 'other_rs_type',
                        itemId: 'other_rs_type',
                        required: true,
                        store: 'otherResourcesStore',
                        valueField: 'other_rs_type',
                        displayField: 'description'
                    },
                    {
                        xtype: 'commontextareafield',
                        label: LocaleManager.getLocalizedString('Other Resource Description', 'Maintenance.view.manager.EstimateFormCosts'),
                        name: 'description',
                        serverTableName: 'wr_other_sync',
                        itemId: 'description'
                    },
                    {
                        xtype: 'localizedspinnerfield',
                        minValue: 0,
                        stepValue: 1,
                        label: LocaleManager.getLocalizedString('Quantity Used', 'Maintenance.view.manager.EstimateFormCosts'),
                        name: 'qty_used',
                        itemId: 'qty_used',
                        value: 0
                    },
                    {
                        xtype: 'commontextfield',
                        label: LocaleManager.getLocalizedString('Units', 'Maintenance.view.manager.EstimateFormCosts'),
                        name: 'units_used',
                        itemId: 'units_used',
                        maxLength: 3,
                        style: 'border-bottom:1px solid #DDD',
                    },
                    {
                        xtype: 'localizedspinnerfield',
                        label: LocaleManager.getLocalizedString('Estimated Cost', 'Maintenance.view.manager.EstimateFormCosts'),
                        name: 'cost_estimated',
                        itemId: 'cost_estimated',
                        labelFormat: 'currency'
                    },
                    {
                        xtype: 'localizedspinnerfield',
                        label: LocaleManager.getLocalizedString('Actual Cost', 'Maintenance.view.manager.EstimateFormCosts'),
                        name: 'cost_total',
                        itemId: 'cost_total',
                        labelFormat: 'currency'
                    }
                ]
            },
            {
                xtype: 'toolbar',
                cls: 'res-form-header',
                items: [
                    {
                        xtype: 'title',
                        title: LocaleManager.getLocalizedString('Other Costs', 'Maintenance.view.manager.EstimateFormCosts')
                    }
                ]
            },
            {
                xtype:'container',
                itemId:'listTitle',
                html:''
            },
            {
                xtype: 'list',
                store: 'workRequestCostsStore',
                itemTpl: '',
                itemId: 'costsList',
                height: Ext.os.is.Phone ? '8em' : '6em',
                emptyText: Ext.String.format(LocaleManager.getLocalizedString('{0} Tap {1} to add new costs{2}',
                    'Maintenance.view.manager.EstimateFormParts'), '<div class="empty-text">', '<span class="ab-add-icon"></span>', '</div>')
            },
            {
                xtype: 'toolbar',
                cls: 'res-form-header',
                items: [
                    {
                        xtype: 'title',
                        itemId: 'resumeCostsFormTitle',
                        title: LocaleManager.getLocalizedString('Summary of Estimated Costs', 'Maintenance.view.manager.EstimateFormCosts')
                    }
                ]
            },
            {
                xtype: 'fieldset',
                itemId: 'resumeCostsForm',
                defaults: {
                    labelWidth: '40%',
                    labelWrap: true,
                    labelCls: Ext.os.is.Phone ? 'x-form-label-phone' : '',
                    labelFormat: 'currency',
                    numericKeyboard: Ext.os.is.Phone ? false : true,
                    readOnly: true,
                    hidden: true
                },
                items: [
                    {
                        xtype: 'formattednumberfield',
                        label: LocaleManager.getLocalizedString('Cost of Labor', 'Maintenance.view.manager.EstimateFormCosts'),
                        name: 'cost_est_labor'
                    },
                    {
                        xtype: 'formattednumberfield',
                        label: LocaleManager.getLocalizedString('Cost of Parts', 'Maintenance.view.manager.EstimateFormCosts'),
                        name: 'cost_est_parts'
                    },
                    {
                        xtype: 'formattednumberfield',
                        label: LocaleManager.getLocalizedString('Other Costs', 'Maintenance.view.manager.EstimateFormCosts'),
                        name: 'cost_est_other'
                    },
                    {
                        xtype: 'formattednumberfield',
                        label: LocaleManager.getLocalizedString('Total Cost', 'Maintenance.view.manager.EstimateFormCosts'),
                        name: 'cost_est_total'
                    },
                    {
                        xtype: 'formattednumberfield',
                        label: LocaleManager.getLocalizedString('Cost of Labor', 'Maintenance.view.manager.EstimateFormCosts'),
                        name: 'cost_labor'
                    },
                    {
                        xtype: 'formattednumberfield',
                        label: LocaleManager.getLocalizedString('Cost of Parts', 'Maintenance.view.manager.EstimateFormCosts'),
                        name: 'cost_parts'
                    },
                    {
                        xtype: 'formattednumberfield',
                        label: LocaleManager.getLocalizedString('Other Costs', 'Maintenance.view.manager.EstimateFormCosts'),
                        name: 'cost_other'
                    },
                    {
                        xtype: 'formattednumberfield',
                        label: LocaleManager.getLocalizedString('Total Cost', 'Maintenance.view.manager.EstimateFormCosts'),
                        name: 'cost_total'
                    }
                ]
            }
        ]
    },

    initialize: function () {
        var me = this,
            list = this.down('list'),
            displayMode,
            listTitle=me.down('container[itemId=listTitle]'),
            template = this.itemTemplate,
            formattedTpl = template,
            descriptionEnumList,
            issuedOrCompletedList,
            wrIdTitle,
            otherResTypeTitle,
            qtyActualTitle,
            unitsTitle,
            actualCostTitle,
            estimateCostTitle,
            xTpl = new Ext.XTemplate(formattedTpl, {
                getCostDisplayValue: function (wr_id, other_rs_type) {
                    return '' + (me.getMultipleSelection() ? (wr_id + ' ') : '') + other_rs_type;
                },
                getQuantityDisplayValue: function (cost_total, cost_estimated) {
                    return Common.util.Format.formatNumber(issuedOrCompletedList ? cost_total : cost_estimated, 2,Common.util.Currency.getCurrencySymbol());
                }
            });

        if(me.getDisplayMode()){
            displayMode = me.getDisplayMode();
        }else{
            displayMode = WorkRequestFilter.listType;
        }

        issuedOrCompletedList= (displayMode === Constants.Issued|| displayMode === Constants.Completed||displayMode === Constants.MyWork|| displayMode===Constants.EqMaintenanceHistory)

        //add list title
        if(listTitle && (!Ext.os.is.Phone)){
            wrIdTitle=LocaleManager.getLocalizedString('Work Request Code', 'Maintenance.view.manager.EstimateFormCosts');
            otherResTypeTitle=LocaleManager.getLocalizedString('Other Resource Type', 'Maintenance.view.manager.EstimateFormCosts');
            qtyActualTitle=LocaleManager.getLocalizedString('Quantity Used', 'Maintenance.view.manager.EstimateFormCosts');
            unitsTitle=LocaleManager.getLocalizedString('Units', 'Maintenance.view.manager.EstimateFormCosts');
            actualCostTitle=LocaleManager.getLocalizedString('Actual Cost', 'Maintenance.view.manager.EstimateFormCosts');
            estimateCostTitle= LocaleManager.getLocalizedString('Estimated Cost', 'Maintenance.view.manager.EstimateFormCosts');

            listTitle.setHtml(UiUtil.getResourceListTitleTemplate([
                me.getMultipleSelection()?wrIdTitle+'-'+otherResTypeTitle:otherResTypeTitle,
                qtyActualTitle,
                unitsTitle,
                issuedOrCompletedList?actualCostTitle:estimateCostTitle
            ]));
        }

        if (list) {
            list.setItemTpl(xTpl);
        }
        
        descriptionEnumList = TableDef.getEnumeratedList('other_rs', 'description');
        if (descriptionEnumList && descriptionEnumList.length > 0) {
            me.query('selectfield[name=other_rs_type]')[0].setOptions(descriptionEnumList);
        }

        //add a black title bar
        UiUtil.addBlackTitleBar(me);

        this.callParent();

        me.displayFormFields();
    },

    displayFormFields: function () {
        var me = this,
            displayMode,
            visibleFields,
            editableFields,
            summaryVisibleFields,
            resumeCostsForm = me.down('#resumeCostsForm');

        if(me.getDisplayMode()){
            displayMode = me.getDisplayMode();
        }else{
            displayMode = WorkRequestFilter.listType;
        }

        if (displayMode === Constants.Issued || displayMode === Constants.Completed || displayMode === Constants.MyWork|| displayMode===Constants.EqMaintenanceHistory) {
            visibleFields = ['other_rs_type', 'description', 'qty_used', 'units_used', 'cost_estimated', 'cost_total'];
            if(FormUtil.userCanEditResourcesAfterIssued()) {
                editableFields = ['other_rs_type', 'description', 'qty_used', 'units_used', 'cost_total'];
            }
            summaryVisibleFields = ['cost_labor', 'cost_parts', 'cost_other', 'cost_total'];
            me.down('[itemId=resumeCostsFormTitle]').setTitle(LocaleManager.getLocalizedString('Summary of Actual Costs',
                'Maintenance.view.manager.EstimateFormCosts'));
        } else {
            visibleFields = ['other_rs_type', 'description', 'qty_used', 'units_used', 'cost_estimated'];
            editableFields = ['other_rs_type', 'description', 'qty_used', 'units_used', 'cost_estimated'];
            summaryVisibleFields = ['cost_est_labor', 'cost_est_parts', 'cost_est_other', 'cost_est_total'];
        }

        NavigationUtil.showFields(me, visibleFields, editableFields);
        NavigationUtil.showFields(resumeCostsForm, summaryVisibleFields);
    },

    applyRecord: function (record) {
        if (record) {
            this.setResumeCostsFormFromWorkRequest(record);
        }
    },

    setResumeCostsFormFromWorkRequest: function (record) {
        var me = this,
            fieldSet = me.down('#resumeCostsForm').getItems();

        // set the costs to the fields
        fieldSet.each(function (field) {
            field.setValue(record.get(field.getName()));
        });
    },

    /**
     * Sums up the costs from the currently selected work requests and sets the values to the Resume Costs form fields.
     * To call for multiple selection case
     */
    setResumeCostsFormFromSelectedRequests: function () {
        var me = this,
            fieldSet = me.down('#resumeCostsForm').getItems(),
            fieldValues = [],
            selectedWorkRequests = Ext.getStore('workRequestsStore').getSelectedWorkRequests();

        // set to zero all costs
        fieldSet.each(function (field) {
            fieldValues[field.getName()] = 0.00;
        });

        // sum up the costs from WRs
        Ext.Array.each(selectedWorkRequests, function (wr) {
            fieldSet.each(function (field) {
                fieldValues[field.getName()] += wr.get(field.getName());
            });
        });

        // set the costs to the fields
        fieldSet.each(function (field) {
            if (fieldValues[field.getName()] > 0) {
                field.setValue(fieldValues[field.getName()]);
            }
        });
    }
});
