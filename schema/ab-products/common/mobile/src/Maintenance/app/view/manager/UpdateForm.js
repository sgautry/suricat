Ext.define('Maintenance.view.manager.UpdateForm', {
    extend: 'Maintenance.view.WorkRequestValidateBase',

    requires: ['Maintenance.view.ViewSelector'],

    xtype: 'updateFormPanel',

    config: {

        model: 'Maintenance.model.WorkRequest',
        storeId: 'workRequestsStore',
        
        title: LocaleManager.getLocalizedString('Update', 'Maintenance.view.manager.UpdateForm'),

        addTitle: LocaleManager.getLocalizedString('Create Work Request', 'Maintenance.view.manager.UpdateForm'),

        displayMode: '',
        isLinkNewForm: false,
        isRelatedRequest: false,
        isEqFieldsEditable: false,
        isEqConditionChanged:false,
        isEqStatusChanged:false,
        requestType: Constants.MyWork,

        viewIds: {
            workRequestId: null,
            mobileId: null
        },

        scrollable: {
            direction: 'vertical',
            directionLock: true
        },

        toolBarButtons: [
            {
                xtype: 'camera',
                align: 'left',
                iconCls: 'camera',
                displayOn: 'all'
            },
            {
                xtype: 'toolbarbutton',
                action: 'locateWorkRequest',
                align: 'left',
                iconCls: 'locate',
                displayOn: 'all'
            },
            {
                xtype: 'toolbarbutton',
                text: LocaleManager.getLocalizedString('Cancel', 'Maintenance.view.manager.UpdateForm'),
                align: 'right',
                action: 'workRequestCancel',
                displayOn: 'all'
            },
            {
                xtype: 'toolbarbutton',
                iconCls: 'redline',
                align: 'right',
                action: 'openRedline',
                displayOn: 'all'
            }
        ],

        items: [
            {
                xtype: 'requestDetailsPanel'
            },
            {
                xtype: 'toolbar',
                itemId: 'bottomToolbar',
                docked: 'bottom',
                items: [
                    {
                        xtype: 'workrequestviewselector',
                        centered: true,
                        allowToggle: false,
                        navigationView: 'mainview',
                        displayViews: true,
                        width: Ext.os.is.Phone ? '95%' : '42em',
                        algin: 'center',
                        defaults: {
                            width: Ext.os.is.Phone ? '16.6%' : '7em',
                            scrollable: {
                                direction: 'vertical',
                                directionLock: true
                            }
                        },
                        items: [
                            {
                                text: LocaleManager.getLocalizedString('Craftspersons', 'Maintenance.view.manager.UpdateForm'),
                                store: 'workRequestCraftspersonsStore',
                                view: 'scheduleFormCraftspersons',
                                record: null,
                                displayMode:null,
                            },
                            {
                                text: LocaleManager.getLocalizedString('Parts', 'Maintenance.view.manager.UpdateForm'),
                                store: 'workRequestPartsStore',
                                view: 'estimateFormParts'
                            },
                            {
                                text: LocaleManager.getLocalizedString('Tools', 'Maintenance.view.manager.UpdateForm'),
                                store: 'workRequestToolsStore',
                                view: 'scheduleFormTools',
                                record: null,
                            },
                            {
                                text: LocaleManager.getLocalizedString('Costs', 'Maintenance.view.manager.UpdateForm'),
                                store: 'workRequestCostsStore',
                                view: 'estimateFormCosts',
                                record: null,
                                hidden: true
                            },
                            {
                                text: LocaleManager.getLocalizedString('Documents', 'Maintenance.view.manager.UpdateForm'),
                                documentSelect: true,
                                view: 'workRequestDocumentList',
                                store: 'workRequestsStore'
                            },
                            //KB#3050980 Add Tab of Work Request References
                            {
                                text: LocaleManager.getLocalizedString('References', 'Maintenance.view.manager.UpdateForm'),
                                documentSelect: false,
                                showBadgeText: true,
                                view: 'workRequestReferencesList',
                                store: 'referenceStore'
                            }
                        ]
                    }
                ]
            }
        ]
    },

    initialize: function () {
        var me = this,
            isCreateView = me.getIsCreateView(),
            visibleFields, editableFields,
            bottomToolbar = me.query('[itemId=bottomToolbar]')[0],
            craftspersonForm = bottomToolbar.query('viewselector [view=scheduleFormCraftspersons]')[0],
            estimatePartsForm = bottomToolbar.query('viewselector [view=estimateFormParts]')[0],
            estimateCostsForm = bottomToolbar.query('viewselector [view=estimateFormCosts]')[0],
            toolsForm = bottomToolbar.query('viewselector [view=scheduleFormTools]')[0];

        if (me.getDisplayMode() === Constants.Approved) {
            visibleFields = ['requestor', 'bl_id', 'fl_id', 'rm_id', 'location', 'prob_type', 'eq_id', 'priority_label', 'description', 'cf_notes','eq_status','eq_condition'];
            bottomToolbar.setHidden(true);
        } else if (me.getDisplayMode() === Constants.Issued) {
            visibleFields = ['requestor', 'bl_id', 'fl_id', 'rm_id', 'location', 'prob_type', 'status', 'eq_id', 'priority_label', 'description', 'cf_notes','eq_status','eq_condition'];
            editableFields = ['cf_notes','eq_status','eq_condition'];
            estimateCostsForm.setHidden(false);
            me.setIsEqFieldsEditable(true);
        } else if (me.getDisplayMode() === Constants.Completed) {
            visibleFields = ['requestor', 'bl_id', 'fl_id', 'rm_id', 'location', 'prob_type', 'eq_id', 'priority_label', 'description', 'cf_notes','eq_status','eq_condition'];
            editableFields = ['cf_notes'];
            estimateCostsForm.setHidden(false);
        }else if (me.getDisplayMode() === Constants.MyWork) {
            visibleFields = ['requestor', 'bl_id', 'fl_id', 'rm_id', 'location', 'prob_type', 'status', 'cause_type', 'repair_type', 'eq_id', 'priority_label', 'description', 'cf_notes','eq_status','eq_condition'];
            editableFields = ['bl_id', 'fl_id', 'rm_id', 'location', 'prob_type','cause_type', 'repair_type','description','cf_notes','eq_status','eq_condition'];
            estimateCostsForm.setHidden(false);
            me.setIsEqFieldsEditable(true);
        }else if (me.getDisplayMode() === Constants.MyRequests) {
            visibleFields = ['requestor', 'bl_id', 'fl_id', 'rm_id', 'location', 'prob_type', 'status', 'cause_type', 'repair_type', 'eq_id', 'priority_label', 'description', 'cf_notes','eq_status','eq_condition'];
            editableFields = ['location', 'description'];
            estimateCostsForm.setHidden(false);
        }else if(me.getDisplayMode() === Constants.EqMaintenanceHistory ||me.getDisplayMode()===Constants.Requested){
            visibleFields = ['requestor', 'bl_id', 'fl_id', 'rm_id', 'location', 'prob_type', 'status', 'cause_type', 'repair_type', 'eq_id', 'priority_label', 'description', 'cf_notes','eq_status','eq_condition'];
            editableFields = [];
            estimateCostsForm.setHidden(false);
        }

        craftspersonForm.config.displayMode = me.getDisplayMode();
        estimatePartsForm.config.displayMode = me.getDisplayMode();
        estimateCostsForm.config.displayMode = me.getDisplayMode();
        toolsForm.config.displayMode = me.getDisplayMode();

        // Set the title
        var title = isCreateView ? me.getAddTitle() : me.getTitle();
        me.setTitle(title);

        //Set Equipment and Condition change event listener.
        me.query('[name=eq_status]')[0].on('change',function(field,newValue,oldValue){
            var record=me.getRecord();
            if(record){
                //set mobile change
                me.setIsEqStatusChanged(true);
                record.setMobileEqFieldsChanged('eq_status');
            }

        },me);

        me.query('[name=eq_condition]')[0].on('change',function(){
            var record=me.getRecord();
            if(me.getRecord()){
                me.setIsEqConditionChanged(true);
                record.setMobileEqFieldsChanged('eq_condition');
            }
        },me);

        if (me.getIsRelatedRequest()) {
            me.setTitle(LocaleManager.getLocalizedString('Work Request Details', 'Maintenance.view.manager.UpdateForm'));
        }
        me.callParent();
        //this.setFieldLabelAndLength('wr_sync');

        Maintenance.util.NavigationUtil.showFields(this, visibleFields, editableFields);
        
        if (isCreateView) {
            me.setCreateView();
        }
    },
    

    /**
     * show create related work reuqests
     */
    showCreateRelatedRequestTitle: function () {
        this.down('title').setTitle(LocaleManager.getLocalizedString('Create Related Work Request', 'Maintenance.view.manager.UpdateForm'));
    },
    
    setCreateView: function () {
        var me = this,
            userProfile,
            visibleFields = ['requestor', 'bl_id', 'fl_id', 'rm_id', 'location', 'prob_type', 'eq_id', 'description'],
            editableFields = ['requestor', 'bl_id', 'fl_id', 'rm_id', 'location', 'prob_type', 'eq_id', 'description'],
            record = me.getRecord(),
            segmentedButton = me.query('toolbar > segmentedbutton');

        // Get the user profile values for the current user
        userProfile = Common.util.UserProfile.getUserProfile();

        // Get the cached employee id from the ConfigFileManager
        userProfile.requestor = ConfigFileManager.employeeId;

        me.setValues(userProfile);

        // set the record data
        record.setData(userProfile);

        // Hide the segmented button if this is a create view
        if (segmentedButton) {
            segmentedButton[0].setHidden(true);
        }
        
        me.down('[name=related_reuqests]').setHidden(true);
        
        Maintenance.util.NavigationUtil.showFields(this, visibleFields, editableFields);
    },

    applyRecord: function (record) {
        var me = this,
            fields,
            viewSelector = me.down('viewselector'),
            estimateCostsForm,
            scheduleFormCraftspersons,
            eqId,
            equipmentPromptField = me.down('equipmentPrompt');

        if (record && viewSelector) {
            viewSelector.setRecord(record);

            if (me.getDisplayMode() === Constants.Issued || me.getDisplayMode() === Constants.Completed) {
                estimateCostsForm = me.down('viewselector [view=estimateFormCosts]');
                if (estimateCostsForm) {
                    estimateCostsForm.setRecord(record);
                }

                scheduleFormCraftspersons = me.down('viewselector [view=scheduleFormCraftspersons]');
                if (scheduleFormCraftspersons) {
                    scheduleFormCraftspersons.setRecord(record);
                }
            }
            
            if (record.get('status') === 'R' || record.get('status') === 'AA') {
                me.down('field[name=repair_type]').setHidden(true);
                me.down('field[name=cause_type]').setHidden(true);
                me.down('field[name=cf_notes]').setHidden(true);
                me.setIsEqFieldsEditable(false);
            }

            if ((me.getDisplayMode() === Constants.MyRequests) && (record.get('status') === 'I' || record.get('status') === 'Com' || record.get('status') === 'HP' || record.get('status') === 'HL' || record.get('status') === 'HA')) {
                me.down('[name=location]').setReadOnly(true);
                me.down('[name=description]').setReadOnly(true);
                me.down('field[name=repair_type]').setReadOnly(true);
                me.down('field[name=cause_type]').setReadOnly(true);
                me.down('field[name=cf_notes]').setReadOnly(true);
                me.setIsEqFieldsEditable(false);
            }

            //related work requests(request_type=='2') and the current user cannot access this request directly(request_type=='3':equipment maintenance history records), set all field read-only
            if (record.get('request_type') === 3||record.get('request_type') === 2 || record.get('prob_type') === 'PREVENTIVE MAINT' || record.get('is_wt_self_assign') === 1) {
                fields = me.query('field');
                Ext.each(fields, function (field) {
                    field.setReadOnly(true);
                });
                me.setIsEqFieldsEditable(false);
            }

            // Hide the segmented buttons if current user cannot access this request directly
            if (me.getDisplayMode() === Constants.MyRequests) {
                viewSelector.hide();
            }
            //Show the segmented buttons if current work request is history equipment maintenance history work request.
            if(record.get('request_type') === 3||record.get('request_type') === 2){
                viewSelector.show();
                me.hideAllToolbarButtons();
            }
            
            //KB#3054499 Add Equipment Status and Equipment Condition fields to the Update Work Request form.
            eqId=record.get('eq_id');
            me.showOrHideEqFields(eqId,me.getIsEqFieldsEditable());

            //get related work request
            if(!Ext.isEmpty(record.get('wr_id'))){
                WorkRequestAction.loadRelatedRequests(me,record.get('wr_id'));
            }

            //MOB-14:Display the history icon for equipment code field.
            if(eqId){
                equipmentPromptField.setShowActionIcon(true);
            }else{
                equipmentPromptField.setShowActionIcon(false);
            }
        }

        return record;
    },

    /**
     * Displays the photo images when the form is in create mode
     */
    applyPhotoData: function () {
        var me = this,
            record = me.getRecord(),
            documentFields = me.query('documentfield');

        if (record && record !== null) {
            Ext.each(documentFields, function (docField) {
                docField.setRecord(record);
                if (docField.getImageData().length > 0) {
                    docField.setHidden(false);
                }
            }, me);
        }
    },

    /**
     * Set Equipment Status and Equipment Condition field value.
     * @param eqId {Equipment Code}
     */
    showOrHideEqFields: function(eqId,isEqFieldsEditable){
        var me=this;
        if(!Ext.isEmpty(eqId)){
            //show Equipment Status and Equipment Condition field
            me.down('field[name=eq_status]').setHidden(false);
            me.down('field[name=eq_condition]').setHidden(false);


            if(isEqFieldsEditable){
                me.down('field[name=eq_status]').setReadOnly(false);
                me.down('field[name=eq_condition]').setReadOnly(false);
            }else{
                me.down('field[name=eq_status]').setReadOnly(true);
                me.down('field[name=eq_condition]').setReadOnly(true);
            }

        }else{
            me.down('field[name=eq_status]').setHidden(true);
            me.down('field[name=eq_condition]').setHidden(true);
        }
    },

    /**
     * Hide all toolbar buttons if user have no rights to edit data.
     */
    hideAllToolbarButtons: function(){
        var me=this,
            toolBarButtons=me.getToolBarButtons();

        Ext.each(toolBarButtons,function(button){
            button.hidden=true;
        });
    }
});