/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
Ext.define('Maintenance.view.manager.EqMaintenanceHistoryList', {
    extend: 'Maintenance.view.WorkRequestList',
    xtype: 'eqMaintenanceHistory',

    config: {

        title: LocaleManager.getLocalizedString('Equipment Maintenance History', 'Maintenance.view.manager.EqMaintenanceHistoryList'),

        displayMode: Constants.EqMaintenanceHistory,
        //selected equipment code.
        eqId:''
    },

    initialize: function () {
        var me=this,
            titleBar;
        me.down('buttonpicker').setHidden(true);
        //Remove the existing title bar because the orginal titlebar text always lign center and the titlebar titleAlign property only applies when the component is created.
        //So we can remove the existing titlebar then add a new component using titleAlign: 'left'.
        titleBar = me.down('titlebar');
        me.remove(titleBar);

        me.insert(1, {
            xtype: 'titlebar',
            titleAlign: 'left'
        });

        UiUtil.addBlackTitleBar(me);
        me.down('button[name=workRequestFilterButton]').setHidden(false);
    },

    applyEqId: function(eqId){
        var me=this,
            title;
        title=LocaleManager.getLocalizedString('Equipment Code : {0}','Maintenance.view.manager.EqMaintenanceHistoryList');
        me.down('titlebar').setTitle(Ext.String.format(title,eqId));

        return eqId;
    }
});