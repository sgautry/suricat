Ext.define('Maintenance.view.manager.ScheduleFormTools', {
    extend: 'Ext.Panel',

    xtype: 'scheduleFormTools',

    tabletItemTemplate: '<div class="prompt-list-hbox"><h1 style="width:25%;text-align:left">{[this.getToolDisplayValue(values.wr_id, values.tool_id)]}</h1>' +
    '<div style="width:25%;text-align:left">{[UiUtil.formatHour(values.hours_est)]} ' +
    LocaleManager.getLocalizedString("Hours", 'Maintenance.view.manager.ScheduleFormTools') +
    '</div><div class="prompt-list-date" style="width:25%;text-align:left">{date_assigned:date("{0}")}</div>' +
    '<div style="width:25%;text-align:left">{time_assigned:date("H:i")}</div></div>',

    phoneItemTemplate: '<div class="prompt-list-hbox"><h1 style="width:50%;text-align:left">{[this.getToolDisplayValue(values.wr_id, values.tool_id)]}</h1>' +
    '<div style="width:50%;text-align:right">{[UiUtil.formatHour(values.hours_est)]} ' +
    LocaleManager.getLocalizedString("Hours", 'Maintenance.view.manager.ScheduleFormTools') +
    '</div></div>' +
    '<div class="prompt-list-hbox"><div class="prompt-list-date" style="width:35%;text-align:center">{date_assigned:date("{0}")}</div>' +
    '<div style="width:30%;text-align:right">{time_assigned:date("H:i")}</div></div>',

    config: {
        //KB#3051445 Note 'Type form.getStoreId is not a function' pops up when I hit on Start button of field 'Time Started' in Assign Craftsperon form.
        storeId:'toolsStore',
        /**
         * True/false if view is opened for multiple selection (multiple work requests) or single selection
         */
        multipleSelection: false,

        displayMode: null,
        
        title: LocaleManager.getLocalizedString('Tools', 'Maintenance.view.manager.ScheduleFormTools'),

        layout : 'vbox',

        items: [
            {
                xtype: 'titlebar',
                itemId: 'assignToolToolBar',
                cls: 'res-form-header',
                items: [
                    {
                        xtype: 'title',
                        title: LocaleManager.getLocalizedString('Assign Tool', 'Maintenance.view.manager.ScheduleFormTools')
                    }
                ]
            },
            {
                xtype: 'fieldset',
                itemId: 'assignToolForm',
                defaults: {
                    labelWidth: '40%',
                    labelWrap: true,
                    labelCls: Ext.os.is.Phone ? 'x-form-label-phone' : '',
                    stepValue: 0.1,
                    minValue: 0,
                    readOnly: true,
                    hidden: true
                },
                items: [
                    {
                        xtype: 'prompt',
                        name: 'tool_id',
                        itemId: 'tool_id',
                        title: LocaleManager.getLocalizedString('Tool', 'Maintenance.view.manager.ScheduleFormTools'),
                        label: LocaleManager.getLocalizedString('Tool Code', 'Maintenance.view.manager.ScheduleFormTools'),
                        required: true,
                        store: 'toolsStore',
                        displayFields: [
                            {
                                name: 'tool_id',
                                title: LocaleManager.getLocalizedString('Tool Code', 'Maintenance.view.manager.ScheduleFormTools')
                            },
                            {
                                name: 'bl_id',
                                title: LocaleManager.getLocalizedString('Building Code', 'Maintenance.view.manager.ScheduleFormTools')
                            },
                            {
                                name: 'tool_type',
                                title: LocaleManager.getLocalizedString('Tool Type', 'Maintenance.view.manager.ScheduleFormTools')
                            }
                        ],
                    },
                    {
                        xtype: 'calendarfield',
                        label: LocaleManager.getLocalizedString('Date Scheduled', 'Maintenance.view.manager.ScheduleFormTools'),
                        name: 'date_assigned',
                        itemId: 'date_assigned',
                        dateFormat: LocaleManager.getLocalizedDateFormat(),
                        value: new Date(),
                        required: true
                        //width: Ext.os.is.Phone ? '100%' : '40%'
                    },
                    {
                        xtype: 'timepickerfield',
                        label: LocaleManager.getLocalizedString('Time Scheduled', 'Maintenance.view.manager.ScheduleFormTools'),
                        name: 'time_assigned',
                        itemId: 'time_assigned',
                        value: new Date(),
                        required: true,
                        style: 'border-bottom:1px solid #DDD',
                        picker: {xtype: 'timepickerconfig'},
                        icon: false
                    },
                    {
                        xtype: 'calendarfield',
                        label: LocaleManager.getLocalizedString('Date Started', 'Maintenance.view.manager.ScheduleFormTools'),
                        dateFormat: LocaleManager.getLocalizedDateFormat(),
                        name: 'date_start',
                        itemId: 'date_start'
                        //flex: Ext.os.is.Phone ? 1 : 4
                    },
                    {
                        xtype: 'timepickerfield',
                        label: LocaleManager.getLocalizedString('Time Started', 'Maintenance.view.manager.ScheduleFormTools'),
                        name: 'time_start',
                        itemId: 'time_start',
                        picker: {xtype: 'timepickerconfig'},
                        icon:'start'
                    },
                    {
                        xtype: 'calendarfield',
                        label: LocaleManager.getLocalizedString('Date Finished', 'Maintenance.view.manager.ScheduleFormTools'),
                        dateFormat: LocaleManager.getLocalizedDateFormat(),
                        name: 'date_end',
                        itemId: 'date_end'
                    },
                    {
                        xtype: 'timepickerfield',
                        label: LocaleManager.getLocalizedString('Time Finished', 'Maintenance.view.manager.ScheduleFormTools'),
                        name: 'time_end',
                        itemId: 'time_end',
                        picker: {xtype: 'timepickerconfig'},
                        icon:'stop'
                    },
                    {
                        xtype: 'localizedspinnerfield',
                        label: LocaleManager.getLocalizedString('Scheduled Hours', 'Maintenance.view.manager.ScheduleFormTools'),
                        name: 'hours_est',
                        itemId: 'hours_est',
                    },
                    {
                        xtype: 'localizedspinnerfield',
                        label: LocaleManager.getLocalizedString('Straight Time Hours Used', 'Maintenance.view.manager.ScheduleFormCraftspersons'),
                        name: 'hours_straight',
                        itemId: 'hours_straight'
                    }
                ]
            },
            {
                xtype: 'toolbar',
                cls: 'res-form-header',
                items: [
                    {
                        xtype: 'title',
                        title: LocaleManager.getLocalizedString('Tools', 'Maintenance.view.manager.ScheduleFormTools')
                    }
                ]
            },
            {
                xtype:'container',
                itemId:'listTitle',
                html:''
            },
            {
                xtype: 'list',
                store: 'workRequestToolsStore',
                itemTpl: '',
                height: Ext.os.is.Phone ? '8em' : '6em',
                emptyText: Ext.String.format(LocaleManager.getLocalizedString('{0} Tap {1} to assign new tools{2}',
                    'Maintenance.view.manager.EstimateFormParts'), '<div class="empty-text">', '<span class="ab-add-icon"></span>', '</div>')
            }
        ]
    },

    initialize: function () {
        var me = this,
            list = me.down('list'),
            displayMode,
            listTitle=me.down('container[itemId=listTitle]'),
            issuedOrCompletedList,
            template = Ext.os.is.Phone ? me.phoneItemTemplate : me.tabletItemTemplate,
            formattedTpl,
            xTpl,
            toolCodeTitle,
            schedualHourTitle,
            dateAssignTitle,
            timeAssignTitle;

        if(me.getDisplayMode()){
            displayMode = me.getDisplayMode();
        }else{
            displayMode = WorkRequestFilter.listType;
        }

        issuedOrCompletedList= (displayMode === Constants.Issued
        || displayMode === Constants.Completed|| displayMode === Constants.MyWork|| displayMode===Constants.EqMaintenanceHistory)


        if (issuedOrCompletedList) {
            template = template.replace("hours_est", "hours_straight");
            template = template.replace("date_assigned", "date_end");
            template = template.replace("time_assigned", "time_end");
        }

        formattedTpl = Ext.String.format(template, LocaleManager.getLocalizedDateFormat());
        xTpl = new Ext.XTemplate(formattedTpl, {
            getToolDisplayValue: function (wr_id, tool_id) {
                return '' + (me.getMultipleSelection() ? (wr_id + ' ') : '') + tool_id;
            }
        });

        //add list title
        if(listTitle&&(!Ext.os.is.Phone)){
            toolCodeTitle=LocaleManager.getLocalizedString('Tool Code', 'Maintenance.view.manager.ScheduleFormTools');

            if(issuedOrCompletedList){
                schedualHourTitle=LocaleManager.getLocalizedString('Straight Time Hours Used', 'Maintenance.view.manager.ScheduleFormTools');
                dateAssignTitle=LocaleManager.getLocalizedString('Date Finished', 'Maintenance.view.manager.ScheduleFormTools');
                timeAssignTitle=LocaleManager.getLocalizedString('Time Finished', 'Maintenance.view.manager.ScheduleFormTools');
            }else{
                schedualHourTitle=LocaleManager.getLocalizedString('Scheduled Hours', 'Maintenance.view.manager.ScheduleFormTools');
                dateAssignTitle=LocaleManager.getLocalizedString('Date Scheduled', 'Maintenance.view.manager.ScheduleFormTools');
                timeAssignTitle=LocaleManager.getLocalizedString('Time Scheduled', 'Maintenance.view.manager.ScheduleFormTools');
            }

            listTitle.setHtml(UiUtil.getResourceListTitleTemplate([
                toolCodeTitle,
                schedualHourTitle,
                dateAssignTitle,
                timeAssignTitle
            ]));

        }

        if (list) {
            list.setItemTpl(xTpl);
        }

        //add a black title bar
        UiUtil.addBlackTitleBar(me);

        me.callParent();

        me.displayFormFields();
        //set all fields readonly for history work request and other condition that have no rights to edit.
        FormUtil.setAllFormFieldsReadOnlyByConditions(me);

        FormUtil.registerDatesListeners(me);

        //reset tool list height.
        UiUtil.resetWrResourcesListHeight(list,WorkRequestFilter.listType,me.getMultipleSelection());
    },

    displayFormFields: function () {
        var me = this,
            displayMode,
            visibleFields,
            editableFields,
            visibleContainers,
            visibleButtons;

        if(me.getDisplayMode()){
            displayMode = me.getDisplayMode();
        }else{
            displayMode = WorkRequestFilter.listType;
        }

        if (displayMode === Constants.Issued || displayMode === Constants.Completed|| displayMode === Constants.MyWork|| displayMode===Constants.EqMaintenanceHistory) {
            visibleFields = ['tool_id', 'date_assigned', 'time_assigned', 'date_start', 'time_start', 'date_end', 'time_end', 'hours_est', 'hours_straight'];
            visibleContainers = ['dateTimeContainer'];
            if (FormUtil.userCanEditResourcesAfterIssued()) {
                editableFields = ['tool_id','date_start', 'time_start', 'date_end', 'time_end', 'hours_straight'];
            }
            visibleButtons = [];
        } else {
            visibleFields = ['tool_id', 'date_assigned', 'time_assigned', 'hours_est'];
            editableFields = ['tool_id', 'date_assigned', 'time_assigned', 'hours_est'];
            visibleContainers = ['dateTimeContainer'];
        }

        NavigationUtil.showItemsByItemId(me, visibleContainers);
        NavigationUtil.showFields(me, visibleFields, editableFields);
        NavigationUtil.showButtons(me, visibleButtons);
    }
});