Ext.define('Maintenance.view.SyncError', {
    extend: 'Ext.Container',

    xtype: 'syncerror',

    headerText: LocaleManager.getLocalizedString('The following records could not be applied to Web Central during the sync process.',
        'Maintenance.view.SyncError'),

    config: {
        scrollable: {
            direction: 'vertical',
            directionLock: true
        },
        content: '',
        zIndex: 101,
        items: [
            {
                xtype: 'titlebar',
                docked: 'top',
                title: LocaleManager.getLocalizedString('Sync Errors', 'Maintenance.view.SyncError'),
                items: [
                    {
                        xtype: 'button',
                        align: 'right',
                        action: 'closeSyncErrorView',
                        text: LocalizedStrings.z_Done
                    }
                ]
            },
            {
                xtype: 'container',
                padding: '6px',
                html: 'Sync Errors here.',
                itemId: 'viewContent',
                styleHtmlContent: true
            }
        ]
    },

    applyContent: function (content) {
        var me = this,
            container = me.down('#viewContent');

        container.setHtml(me.processContent(content));
    },

    processContent: function (content) {
        var me = this,

            htmlStr = '',
            p;

        for (p in content) {
            if(p !== 'hasMessage') {
                htmlStr += '<strong>' + me.getCategoryTitle(p) + '</strong><div>' + content[p] +'</div><br>';
            }
        }

        return '<strong>'+ me.headerText + '</strong><br>' + htmlStr;
    },

    getCategoryTitle: function (category) {
        var title = '';
        switch (category) {
            case 'LABOR':
                title = LocaleManager.getLocalizedString('Craftsperson Records:', 'Maintenance.view.SyncError');
                break;
            case 'PART':
                title = LocaleManager.getLocalizedString('Part Records:', 'Maintenance.view.SyncError');
                break;
            case 'COST':
                title = LocaleManager.getLocalizedString('Cost Records:', 'Maintenance.view.SyncError');
                break;
            case 'TRADE':
                title = LocaleManager.getLocalizedString('Trade Records:', 'Maintenance.view.SyncError');
                break;
            case 'TOOL':
                title = LocaleManager.getLocalizedString('Tool Records:', 'Maintenance.view.SyncError');
                break;
            default:
                title = LocaleManager.getLocalizedString('Request Records', 'Maintenance.view.SyncError');
                break;
        }
        return title;
    }

});