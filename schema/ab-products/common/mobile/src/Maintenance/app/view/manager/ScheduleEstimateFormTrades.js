Ext.define('Maintenance.view.manager.ScheduleEstimateFormTrades', {
    extend: 'Ext.Panel',

    xtype: 'scheduleEstimateFormTrades',

    itemTemplate: '<div class="prompt-list-hbox"><h1 style="width:50%;text-align:left">{[this.getTradeDisplayValue(values.wr_id, values.tr_id)]}</h1>' +
    '<div style="width:50%;text-align:left">{[UiUtil.formatHour(values.hours_est)]} ' +
    LocaleManager.getLocalizedString("Hours", 'Maintenance.view.manager.ScheduleEstimateFormTrades') +
    '</div></div>',

    config: {
        title: LocaleManager.getLocalizedString('Trades', 'Maintenance.view.manager.ScheduleEstimateFormTrades'),
        /**
         * True/false if view is opened for multiple selection (multiple work requests) or single selection
         */
        multipleSelection: false,

        layout: 'vbox',

        items: [
            {
                xtype: 'container',
                itemId: 'addTradeContainer',
                hidden: true,
                items: [
                    {
                        xtype: 'titlebar',
                        itemId: 'addTradeToolBar',
                        cls: 'res-form-header',
                        items: [
                            {
                                xtype: 'title',
                                itemId: 'trade_titlebar_title',
                                title: LocaleManager.getLocalizedString('Add Trade', 'Maintenance.view.manager.ScheduleEstimateFormTrades')
                            }
                        ]
                    },
                    {
                        xtype: 'fieldset',
                        itemId: 'addTradeForm',
                        defaults: {
                            labelWidth: '40%',
                            labelWrap: true,
                            labelCls: Ext.os.is.Phone ? 'x-form-label-phone' : '',
                            stepValue: 0.1,
                            minValue: 0
                        },
                        items: [
                            {
                                xtype: 'prompt',
                                name: 'tr_id',
                                itemId: 'tr_id',
                                title: LocaleManager.getLocalizedString('Trade', 'Maintenance.view.manager.ScheduleEstimateFormTrades'),
                                label: LocaleManager.getLocalizedString('Trade Code', 'Maintenance.view.manager.ScheduleEstimateFormTrades'),
                                required: true,
                                store: 'tradesStore',
                                displayFields: [
                                    {
                                        name: 'tr_id',
                                        title: LocaleManager.getLocalizedString('Trade Code', 'Maintenance.view.manager.ScheduleEstimateFormTrades')
                                    },
                                    {
                                        name: 'description',
                                        title: LocaleManager.getLocalizedString('Trade Description', 'Maintenance.view.manager.ScheduleEstimateFormTrades')
                                    }
                                ]
                            },
                            {
                                xtype: 'localizedspinnerfield',
                                label: LocaleManager.getLocalizedString('Estimated Hours', 'Maintenance.view.manager.ScheduleEstimateFormTrades'),
                                name: 'hours_est',
                                itemId: 'hours_est',
                                required: true
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'toolbar',
                cls: 'res-form-header',
                items: [
                    {
                        xtype: 'title',
                        title: LocaleManager.getLocalizedString('Trades', 'Maintenance.view.manager.ScheduleEstimateFormTrades')
                    }
                ]
            },
            {
                xtype:'container',
                itemId:'listTitle',
                html:''
            },
            {
                xtype: 'list',
                store: 'workRequestTradesStore',
                itemTpl: '',
                itemId: 'tradesList',
                height: Ext.os.is.Phone ? '8em' : '6em',
                emptyText: Ext.String.format(LocaleManager.getLocalizedString('{0} Tap {1} to add new trades{2}',
                    'Maintenance.view.manager.EstimateFormParts'), '<div class="empty-text">', '<span class="ab-add-icon"></span>', '</div>')
            }
        ]
    },

    initialize: function () {
        var me = this,
            list = me.down('list'),
            listTitle=me.down('container[itemId=listTitle]'),
            template = me.itemTemplate,
            formattedTpl = template,
            tradeCodeTitle,
            estimateHourTitle,
            xTpl = new Ext.XTemplate(formattedTpl, {
                getTradeDisplayValue: function (wr_id, tr_id) {
                    return '' + (me.getMultipleSelection() ? (wr_id + ' ') : '') + tr_id;
                }
            });

        //add list title bar
        if(listTitle && (!Ext.os.is.Phone)){
            tradeCodeTitle=LocaleManager.getLocalizedString('Trade Code', 'Maintenance.view.manager.ScheduleEstimateFormTrades');
            estimateHourTitle=LocaleManager.getLocalizedString('Estimated Hours', 'Maintenance.view.manager.ScheduleEstimateFormTrades');
            listTitle.setHtml(UiUtil.getResourceListTitleTemplate([
                tradeCodeTitle,
                estimateHourTitle
            ]));
        }

        if (list) {
            list.setItemTpl(xTpl);
        }

        //add a black title bar
        UiUtil.addBlackTitleBar(me);

        me.callParent();

        //set all fields readonly for history work request and other condition that have no rights to edit.
        FormUtil.setAllFormFieldsReadOnlyByConditions(me);

        //reset trades list height property.
        UiUtil.resetWrResourcesListHeight(list,WorkRequestFilter.listType,me.getMultipleSelection());
    }
});