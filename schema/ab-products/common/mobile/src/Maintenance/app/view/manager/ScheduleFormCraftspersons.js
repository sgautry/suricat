Ext.define('Maintenance.view.manager.ScheduleFormCraftspersons', {
    extend: 'Ext.Panel',

    requires: ['Maintenance.view.manager.controls.Radio'],

    xtype: 'scheduleFormCraftspersons',

    editableFields: [],

    config: {
        //KB#3051445 Note 'Type form.getStoreId is not a function' pops up when I hit on Start button of field 'Time Started' in Assign Craftsperon form.
        storeId: 'workRequestCraftspersonsStore',
        /**
         * True/false if view is opened for multiple selection (multiple work requests) or single selection
         */
        multipleSelection: false,

        wrRecord: null,

        viewIds: [],

        displayMode: null,

        title: LocaleManager.getLocalizedString('Craftpersons', 'Maintenance.view.manager.ScheduleFormCraftspersons'),

        layout: 'vbox',

        items: [
            {
                xtype: 'titlebar',
                itemId: 'assignCraftspersonToolBar',
                cls: 'res-form-header',
                items: [
                    {
                        xtype: 'button',
                        text: LocaleManager.getLocalizedString('Copy', 'Maintenance.view.manager.ScheduleFormCraftspersons'),
                        action: 'copyCraftsperson',
                        ui: 'action',
                        hidden: true,
                        align: 'right'
                    },
                    {
                        xtype: 'title',
                        title: LocaleManager.getLocalizedString('Assign Craftsperson', 'Maintenance.view.manager.ScheduleFormCraftspersons')
                    }
                ]
            },
            {
                xtype: 'fieldset',
                itemId: 'assignCraftspersonForm',
                defaults: {
                    labelWidth: '40%',
                    labelWrap: true,
                    labelCls: Ext.os.is.Phone ? 'x-form-label-phone' : '',
                    stepValue: 0.1,
                    minValue: 0,
                    readOnly: true,
                    hidden: true,
                    labelAlign: 'left'
                },
                items: [
                    {
                        xtype: 'prompt',
                        name: 'cf_id',
                        itemId: 'cf_id',
                        title: LocaleManager.getLocalizedString('Craftsperson', 'Maintenance.view.manager.ScheduleFormCraftspersons'),
                        label: LocaleManager.getLocalizedString('Craftsperson Code', 'Maintenance.view.manager.ScheduleFormCraftspersons'),
                        required: true,
                        store: 'craftspersonInPlannerWorkTeamStore',
                        displayFields: [
                            {
                                name: 'cf_id',
                                title: LocaleManager.getLocalizedString('Craftsperson Code', 'Maintenance.view.manager.ScheduleFormCraftspersons')
                            },
                            {
                                name: 'tr_id',
                                title: LocaleManager.getLocalizedString('Primary Trade', 'Maintenance.view.manager.ScheduleFormCraftspersons')
                            }
                        ]
                    },
                    {
                        xtype: 'maintenancecommonradiofield',
                        itemId:'is_scheduled_scheduled',
                        name : 'is_scheduled',
                        value: '1',
                        label: LocaleManager.getLocalizedString('Scheduled','Maintenance.view.manager.ScheduleFormCraftspersons'),
                        checked: true
                    },
                    {
                        xtype: 'maintenancecommonradiofield',
                        itemId:'is_scheduled_unscheduled',
                        name : 'is_scheduled',
                        value: '0',
                        label: LocaleManager.getLocalizedString('Unscheduled','Maintenance.view.manager.ScheduleFormCraftspersons')
                    },
                    {
                        xtype: 'calendarfield',
                        label: LocaleManager.getLocalizedString('Date Scheduled', 'Maintenance.view.manager.ScheduleFormCraftspersons'),
                        name: 'date_assigned',
                        itemId: 'date_assigned',
                        dateFormat: LocaleManager.getLocalizedDateFormat(),
                        required: true,
                        value: new Date()
                    },
                    {
                        xtype: 'timepickerfield',
                        label: LocaleManager.getLocalizedString('Time Scheduled', 'Maintenance.view.manager.ScheduleFormCraftspersons'),
                        name: 'time_assigned',
                        itemId: 'time_assigned',
                        required: true,
                        value: new Date(),
                        picker: {xtype: 'timepickerconfig'},
                        icon: false
                    },
                    {
                        xtype: 'localizedspinnerfield',
                        label: LocaleManager.getLocalizedString('Scheduled Hours', 'Maintenance.view.manager.ScheduleFormCraftspersons'),
                        name: 'hours_est',
                        itemId: 'hours_est'
                    },
                    {
                        xtype: 'localizedspinnerfield',
                        label: LocaleManager.getLocalizedString('Actual Hours', 'Maintenance.view.manager.ScheduleFormCraftspersons'),
                        name: 'hours_straight',
                        itemId: 'hours_straight'
                    },
                    {
                        xtype: 'localizedspinnerfield',
                        label: LocaleManager.getLocalizedString('Overtime Hours', 'Maintenance.view.manager.ScheduleFormCraftspersons'),
                        name: 'hours_over',
                        itemId: 'hours_over'
                    },
                    {
                        xtype: 'localizedspinnerfield',
                        label: LocaleManager.getLocalizedString('Doubletime Hours', 'Maintenance.view.manager.ScheduleFormCraftspersons'),
                        name: 'hours_double',
                        itemId: 'hours_double'
                    },
                    {
                        xtype: 'calendarfield',
                        label: LocaleManager.getLocalizedString('Date Started', 'Maintenance.view.manager.ScheduleFormCraftspersons'),
                        dateFormat: LocaleManager.getLocalizedDateFormat(),
                        name: 'date_start',
                        itemId: 'date_start',
                    },
                    {
                        xtype: 'timepickerfield',
                        label: LocaleManager.getLocalizedString('Time Started', 'Maintenance.view.manager.ScheduleFormCraftspersons'),
                        name: 'time_start',
                        itemId: 'time_start',
                        picker: {xtype: 'timepickerconfig'},
                        icon: 'start'
                    },
                    {
                        xtype: 'calendarfield',
                        label: LocaleManager.getLocalizedString('Date Finished', 'Maintenance.view.manager.ScheduleFormCraftspersons'),
                        dateFormat: LocaleManager.getLocalizedDateFormat(),
                        name: 'date_end',
                        itemId: 'date_end'
                    },
                    {
                        xtype: 'timepickerfield',
                        label: LocaleManager.getLocalizedString('Time Finished', 'Maintenance.view.manager.ScheduleFormCraftspersons'),
                        name: 'time_end',
                        itemId: 'time_end',
                        picker: {xtype: 'timepickerconfig'},
                        icon: 'stop'
                    },
                    {
                        xtype: 'selectlistfield',
                        label: LocaleManager.getLocalizedString('Work Type', 'Maintenance.view.manager.ScheduleFormCraftspersons'),
                        name: 'work_type',
                        itemId: 'work_type',
                        valueField: 'objectValue',
                        displayField: 'displayValue',
                        // The standard work types are entered here to handle the
                        // case when the TableDef is not available.
                        options: [
                            {
                                displayValue: 'UnSpecified',
                                objectValue: 'UnSp'
                            },
                            {
                                displayValue: 'Work',
                                objectValue: 'W'
                            },
                            {
                                displayValue: 'Material Pickup',
                                objectValue: 'P'
                            },
                            {
                                displayValue: 'Job Setup or Prep.',
                                objectValue: 'Prep'
                            },
                            {
                                displayValue: 'Travel Time',
                                objectValue: 'Tr'
                            },
                            {
                                displayValue: 'Wait for Security',
                                objectValue: 'WSec'
                            },
                            {
                                displayValue: 'Wait for Client',
                                objectValue: 'WCli'
                            }
                        ]
                    },
                    {
                        xtype: 'selectlistfield',
                        label: LocaleManager.getLocalizedString('Status', 'Maintenance.view.manager.ScheduleFormCraftspersons'),
                        name: 'status',
                        itemId: 'status',
                        valueField: 'objectValue',
                        displayField: 'displayValue',
                        // The standard work types are entered here to handle the
                        // case when the TableDef is not available.
                        options: [
                        ]
                    },
                    {
                        xtype: 'commontextareafield',
                        label: LocaleManager.getLocalizedString('Comments', 'Maintenance.view.manager.ScheduleFormCraftspersons'),
                        name: 'comments',
                        itemId: 'comments'
                    }
                ]
            },
            {
                xtype: 'toolbar',
                cls: 'res-form-header',
                items: [
                    {
                        xtype: 'title',
                        title: LocaleManager.getLocalizedString('Craftspersons', 'Maintenance.view.manager.ScheduleFormCraftspersons')
                    }
                ]
            },
            {
                xtype:'container',
                itemId:'listTitle',
                html:''
            },
            {
                xtype: 'wrCraftspersonList',
                multipleSelection: false,
                itemId: 'craftspersonsList',
                height: Ext.os.is.Phone ? '8em' : '6em',
                emptyText: Ext.String.format(LocaleManager.getLocalizedString('{0} Tap {1} to assign new craftspersons{2}',
                    'Maintenance.view.manager.EstimateFormParts'), '<div class="empty-text">', '<span class="ab-add-icon"></span>', '</div>')
            }
        ]
    },

    initialize: function () {
        var me = this,
            wrcfList=me.down('wrCraftspersonList'),
            displayMode,
            listTitle=me.down('container[itemId=listTitle]'),
            userProfile = Common.util.UserProfile.getUserProfile(),
            cfIdField = me.down('field[name=cf_id]'),
            wrRecord = me.getWrRecord(),
            statusEnumList,
            workTypeEnumList,
            cfIdTitle,
            workTypeTitle,
            totalHoursTitle,
            schedualHourTitle,
            dateEndTitle,
            dateAssignedTitle,
            timeEndTitle,
            timeAssignedTitle,
            statusTitle,
            issuedOrCompletedList;

        me.callParent();

        if(me.getDisplayMode()){
            displayMode = me.getDisplayMode();
        }else{
            displayMode = WorkRequestFilter.listType;
        }


        issuedOrCompletedList = (displayMode === Constants.Issued
        || displayMode === Constants.Completed || displayMode === Constants.MyWork || displayMode===Constants.EqMaintenanceHistory);

        //add check event listener to scheduled and unscheduled fields.
        me.query('maintenancecommonradiofield[name=is_scheduled]')[0].on('check', function (item,e) {
            me.showFieldsByIsScheudled('1');
        }, me);

        me.query('maintenancecommonradiofield[name=is_scheduled]')[1].on('check', function (item,e) {
            me.showFieldsByIsScheudled('0');
        }, me);

        if(me.getMultipleSelection()){
            wrRecord = Ext.getStore('workRequestsStore').getSelectedWorkRequests()[0];
        }

        workTypeEnumList = TableDef.getEnumeratedList('wrcf_sync', 'work_type');
        if (workTypeEnumList && workTypeEnumList.length > 0) {
            me.query('selectfield[name=work_type]')[0].setOptions(workTypeEnumList);
        }

        statusEnumList = TableDef.getEnumeratedList('wrcf_sync', 'status');
        if (statusEnumList && statusEnumList.length > 0) {
            me.query('selectfield[name=status]')[0].setOptions(statusEnumList);
        }

        // after the WR was issued, only Supervisor can add craftspersons
        if (!(!FormUtil.userCanEditResourcesAfterIssued()
            || (ApplicationParameters.getUserRoleName() !== 'supervisor'&& wrRecord && wrRecord.get('is_req_craftsperson') !== 1)
            || (wrRecord && wrRecord.get('is_req_supervisor') === 0 && wrRecord.get('is_req_craftsperson') === 1 && !ApplicationParameters.isCraftspersonPlanner))) {

            if (issuedOrCompletedList && ApplicationParameters.getUserRoleName() !== 'supervisor'
                && ((wrRecord && wrRecord.get('is_req_craftsperson') === 1) || me.getMultipleSelection())) {
                cfIdField.setValue(userProfile.cf_id);
            }
        }

        me.displayFormFields();

        if(wrRecord && wrRecord.get('is_req_supervisor') === 0 && wrRecord.get('is_req_craftsperson') === 1 && ApplicationParameters.isCraftspersonPlanner){
            Ext.each(me.query('field'), function (field) {
                field.setReadOnly(true);
            });
            me.down('timepickerfield[itemId=time_start]').setReadOnly(true);
            me.down('timepickerfield[itemId=time_end]').setReadOnly(true);

            FormUtil.setFieldsReadOnly(me, ['cf_id','work_type'], false);
        }

        //set all fields readonly for history work request and other condition that have no rights to edit.
        FormUtil.setAllFormFieldsReadOnlyByConditions(me);

        FormUtil.registerDatesListeners(me);

        //add list title
        if(listTitle && (!Ext.os.is.Phone)){
            cfIdTitle=LocaleManager.getLocalizedString('Craftsperson Code', 'Maintenance.view.manager.ScheduleFormCraftspersons');
            workTypeTitle=LocaleManager.getLocalizedString('Work Type', 'Maintenance.view.manager.ScheduleFormCraftspersons');
            totalHoursTitle=LocaleManager.getLocalizedString('Total Hours', 'Maintenance.view.manager.ScheduleFormCraftspersons');
            schedualHourTitle=LocaleManager.getLocalizedString('Scheduled Hours', 'Maintenance.view.manager.ScheduleFormCraftspersons');
            dateEndTitle=LocaleManager.getLocalizedString('Date Finished', 'Maintenance.view.manager.ScheduleFormCraftspersons');
            dateAssignedTitle=LocaleManager.getLocalizedString('Date Scheduled', 'Maintenance.view.manager.ScheduleFormCraftspersons');
            timeEndTitle=LocaleManager.getLocalizedString('Time Finished', 'Maintenance.view.manager.ScheduleFormCraftspersons');
            timeAssignedTitle=LocaleManager.getLocalizedString('Time Scheduled', 'Maintenance.view.manager.ScheduleFormCraftspersons');
            statusTitle=LocaleManager.getLocalizedString('Status', 'Maintenance.view.manager.ScheduleFormCraftspersons');

            listTitle.setHtml(UiUtil.getResourceListTitleTemplate([
                cfIdTitle,
                issuedOrCompletedList?totalHoursTitle:schedualHourTitle,
                issuedOrCompletedList?dateEndTitle:dateAssignedTitle,
                issuedOrCompletedList?timeEndTitle:timeAssignedTitle,
                workTypeTitle,
                statusTitle
            ]));
        }

        //add a black title bar
        UiUtil.addBlackTitleBar(me);

        //reset craftsperson list height.
        UiUtil.resetWrResourcesListHeight(wrcfList,WorkRequestFilter.listType,me.getMultipleSelection());
    },

    applyRecord: function (wrRecord) {
        var me = this;

        if (wrRecord) {
            me.setWrRecord(wrRecord);
        }

    },

    applyMultipleSelection: function (config) {
        // set the multiple selection flag to the craftsperson list
        this.down('[itemId=craftspersonsList]').setMultipleSelection(config);

        return config;
    },

    displayFormFields: function () {
        var me = this,
            displayMode,
            visibleFields,
            visibleButtons,
            visibleContainers,
            wrRecord = me.getWrRecord(),
            userProfile;

        if(me.getDisplayMode()){
            displayMode = me.getDisplayMode();
        }else{
            displayMode = WorkRequestFilter.listType;
        }

        Ext.each(me.query('field'), function (field) {
            field.setReadOnly(false);
        });

        if (displayMode === Constants.Issued ||displayMode === Constants.Completed ||displayMode===Constants.EqMaintenanceHistory){
            visibleFields = ['cf_id','is_scheduled', 'hours_est',
                'hours_straight', 'hours_over', 'hours_double',
                'date_start', 'time_start', 'date_end', 'time_end',
                'work_type', 'comments', 'status'];

            me.query('maintenancecommonradiofield[itemId=is_scheduled_unscheduled]')[0].setChecked(true);
            me.query('maintenancecommonradiofield[itemId=is_scheduled_scheduled]')[0].setReadOnly(true);
            me.query('maintenancecommonradiofield[itemId=is_scheduled_unscheduled]')[0].setReadOnly(true);

            if (FormUtil.userCanEditResourcesAfterIssued()) {
                me.editableFields = [//'cf_id',
                    'hours_straight', 'hours_over', 'hours_double',
                    'date_start', 'time_start', 'date_end', 'time_end',
                    'work_type', 'comments', 'status'];

                if (!(ApplicationParameters.getUserRoleName() !== 'supervisor'
                && ((wrRecord && wrRecord.get('is_req_craftsperson') === 1) || me.getMultipleSelection()))) {
                    me.editableFields = me.editableFields.concat('cf_id');
                }else{
                    //KB#3051620 Planner: Craftsperson Code field should be editable in Issued form/Assign Craftsperson form.
                    if( ApplicationParameters.isCraftspersonPlanner){
                        me.editableFields = me.editableFields.concat('cf_id');
                    }
                }
            }
        } else if(displayMode === Constants.MyWork){
            visibleFields = ['cf_id','is_scheduled','hours_straight', 'hours_over', 'hours_double',
                'date_start', 'time_start', 'date_end', 'time_end',
                'work_type', 'comments', 'status'];
            me.editableFields = ['hours_straight', 'hours_over', 'hours_double',
                'date_start', 'time_start', 'date_end', 'time_end',
                'work_type', 'comments', 'status'];

            userProfile = Common.util.UserProfile.getUserProfile();
            if(ApplicationParameters.isCraftspersonPlanner) {
                me.editableFields = me.editableFields.concat('cf_id');
            }

            me.query('maintenancecommonradiofield[itemId=is_scheduled_unscheduled]')[0].setChecked(true);
            me.query('maintenancecommonradiofield[itemId=is_scheduled_scheduled]')[0].setReadOnly(true);
            me.query('maintenancecommonradiofield[itemId=is_scheduled_unscheduled]')[0].setReadOnly(true);

        } else {
            visibleFields = ['cf_id', 'is_scheduled','date_assigned', 'time_assigned', 'hours_est', 'work_type', 'status'];
            me.editableFields = ['is_scheduled','date_assigned', 'time_assigned', 'hours_est', 'work_type', 'status'];
        }

        NavigationUtil.showFields(me, visibleFields, me.editableFields);
        NavigationUtil.showButtons(me, visibleButtons);
    },

    /**
     * Show or hidden Date Assigned and Time Assigned field by Is Scheduled field value.
     * @param isScheduled
     */
    showFieldsByIsScheudled: function(isScheduled){
        if(isScheduled ==='1'){
            this.query('calendarfield[name=date_assigned]')[0].setHidden(false);
            this.query('timepickerfield[name=time_assigned]')[0].setHidden(false);
        }else{
            this.query('calendarfield[name=date_assigned]')[0].setHidden(true);
            this.query('timepickerfield[name=time_assigned]')[0].setHidden(true);
        }
    }
});