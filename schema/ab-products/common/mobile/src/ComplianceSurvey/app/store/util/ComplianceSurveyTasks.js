Ext.define('ComplianceSurvey.store.util.ComplianceSurveyTasks', {
    extend: 'Common.store.sync.SqliteStore',
    requires: 'ComplianceSurvey.model.util.ComplianceSurveyTask',

    config: {
        storeId: 'complianceSurveyTasksStore',
        model: 'ComplianceSurvey.model.util.ComplianceSurveyTask',
        autoLoad: false,
        enableAutoLoad: false,
        remoteFilter: true,
        remoteSort: true,
        usesTransactionTable: true,
        proxy: {
            type: 'SqliteView',
            viewDefinition: 'SELECT s.activity_log_id, s.action_title, s.description, s.status, s.status_initial,' +
            ' s.date_scheduled, s.date_scheduled_end, s.date_required,' +
            ' s.location_id, s.questionnaire_id_ext, s.pct_complete, s.reg_program, s.project_id, s.regulation,' +
            ' s.priority, l.site_id, l.bl_id, l.fl_id, l.rm_id, l.eq_std, l.eq_id, l.em_id' +
            ' FROM ComplianceSurvey s LEFT JOIN ComplianceLocation l ON s.location_id = l.location_id',
            viewName: 'ComplianceSurveyTasks',

            baseTables: ['ComplianceSurvey', 'ComplianceLocation']
        }
    }
});
