Ext.define('ComplianceSurvey.store.util.UniqueEquipmentIds', {
    extend: 'Common.store.sync.SqliteStore',

    requires: [
        'ComplianceSurvey.model.util.UniqueEquipmentId'
    ],

    config: {
        storeId: 'uniqueEquipmentIds',
        model: 'ComplianceSurvey.model.util.UniqueEquipmentId',
        autoLoad: false,
        enableAutoLoad: false,
        remoteFilter: true,
        usesTransactionTable: true,
        proxy: {
            type: 'SqliteView',
            viewDefinition: 'SELECT DISTINCT eq_id FROM ComplianceLocation',
            viewName: 'UniqueEquipmentIds',

            baseTables: ['ComplianceLocation']
        }
    }
});