Ext.define('ComplianceSurvey.store.util.UniqueProjects', {
    extend: 'Common.store.sync.SqliteStore',

    requires: [
        'ComplianceSurvey.model.util.UniqueProject'
    ],

    config: {
        storeId: 'uniqueProjects',
        model: 'ComplianceSurvey.model.util.UniqueProject',
        autoLoad: false,
        enableAutoLoad: false,
        remoteFilter: true,
        usesTransactionTable: true,
        proxy: {
            type: 'SqliteView',
            viewDefinition: 'SELECT DISTINCT project_id FROM ComplianceSurvey',
            viewName: 'UniqueProjects',

            baseTables: ['ComplianceSurvey']
        }
    }
});