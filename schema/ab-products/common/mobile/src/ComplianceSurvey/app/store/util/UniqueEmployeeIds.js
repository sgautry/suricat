Ext.define('ComplianceSurvey.store.util.UniqueEmployeeIds', {
    extend: 'Common.store.sync.SqliteStore',

    requires: [
        'ComplianceSurvey.model.util.UniqueEmployeeId'
    ],

    config: {
        storeId: 'uniqueEmployeeIds',
        model: 'ComplianceSurvey.model.util.UniqueEmployeeId',
        autoLoad: false,
        enableAutoLoad: false,
        remoteFilter: true,
        usesTransactionTable: true,
        proxy: {
            type: 'SqliteView',
            viewDefinition: 'SELECT DISTINCT em_id FROM ComplianceLocation',
            viewName: 'UniqueEmployeeIds',

            baseTables: ['ComplianceLocation']
        }
    }
});