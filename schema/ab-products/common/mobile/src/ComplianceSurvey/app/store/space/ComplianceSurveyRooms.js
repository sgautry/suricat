Ext.define('ComplianceSurvey.store.space.ComplianceSurveyRooms', {
    extend: 'Common.store.sync.ValidatingTableStore',
    requires: ['ComplianceSurvey.model.space.ComplianceSurveyRoom'],

    serverTableName: 'rm',

    serverFieldNames: ['bl_id', 'fl_id', 'rm_id', 'rm_std', 'name', 'rm_type', 'rm_cat', 'area', 'dv_id', 'dp_id', 'phone'],
    inventoryKeyNames: ['bl_id', 'fl_id', 'rm_id'],

    config: {
        model: 'ComplianceSurvey.model.space.ComplianceSurveyRoom',
        remoteSort: true,
        remoteFilter: true,
        tableDisplayName: LocaleManager.getLocalizedString('Rooms', 'ComplianceSurvey.store.space.ComplianceSurveyRooms'),
        sorters: [
            {
                property: 'bl_id',
                direction: 'ASC'
            },
            {
                property: 'fl_id',
                direction: 'ASC'
            },
            {
                property: 'rm_id',
                direction: 'ASC'
            }
        ],
        storeId: 'complianceSurveyRooms',
        enableAutoLoad: true,
        autoSync: false,
        proxy: {
            type: 'Sqlite'
        },
        timestampDownload: false

    }
});