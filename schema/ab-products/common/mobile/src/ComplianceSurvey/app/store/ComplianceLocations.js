Ext.define('ComplianceSurvey.store.ComplianceLocations', {
    extend: 'Common.store.sync.ValidatingTableStore',
    requires: ['ComplianceSurvey.model.ComplianceLocation'],

    serverTableName: 'compliance_locations',

    serverFieldNames: ['location_id', 'site_id', 'bl_id', 'fl_id', 'rm_id', 'eq_std', 'eq_id', 'em_id'],
    inventoryKeyNames: ['location_id'],

    config: {
        model: 'ComplianceSurvey.model.ComplianceLocation',
        remoteFilter: true,
        tableDisplayName: LocaleManager.getLocalizedString('Locations', 'ComplianceSurvey.store.ComplianceLocations'),
        sorters: [
            {
                property: 'site_id',
                direction: 'ASC'
            },
            {
                property: 'bl_id',
                direction: 'ASC'
            },
            {
                property: 'fl_id',
                direction: 'ASC'
            },
            {
                property: 'rm_id',
                direction: 'ASC'
            }
        ],
        storeId: 'complianceLocationsStore',
        enableAutoLoad: true,
        autoSync: false,
        proxy: {
            type: 'Sqlite'
        }
    }
});