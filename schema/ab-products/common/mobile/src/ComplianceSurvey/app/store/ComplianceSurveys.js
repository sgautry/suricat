Ext.define('ComplianceSurvey.store.ComplianceSurveys', {
    extend: 'Common.store.sync.PagedSyncStore',
    requires: 'ComplianceSurvey.model.ComplianceSurvey',

    serverTableName: 'activity_log_sync',
    serverFieldNames: ['activity_log_id', 'action_title', 'activity_type', 'description', 'status', 'date_scheduled', 'date_scheduled_end', 'date_required',
        'location_id', 'questionnaire_id_ext', 'pct_complete', 'reg_program', 'project_id', 'regulation', 'priority', 'mob_locked_by', 'mob_is_changed'],
    inventoryKeyNames: ['activity_log_id'],

    config: {
        model: 'ComplianceSurvey.model.ComplianceSurvey',
        sorters: [
            {
                property: 'date_scheduled',
                direction: 'ASC'
            }
        ],
        storeId: 'complianceSurveysStore',
        enableAutoLoad: true,
        proxy: {
            type: 'Sqlite'
        },
        tableDisplayName: LocaleManager.getLocalizedString('Compliance Surveys', 'ComplianceSurvey.store.ComplianceSurveys'),
        timestampDownload: false
    },

    /**
     * Override to allow us to set the status_initial value with the status value
     *
     * @override
     * @param {Object[]}
     * @return {Promise}
     */
    convertRecordsFromServer: function (records) {
        var me = this;

        return me.callParent([records])
            .then(function (records) {
                me.setInitialValues(records);
                return Promise.resolve(records);
            });
    },

    setInitialValues: function (records) {
        var me = this;

        Ext.each(records, function (record) {
            record.status_initial = record.status;
        }, me);
    }
});
