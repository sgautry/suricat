Ext.define('ComplianceSurvey.store.space.ComplianceSurveyFloors', {
    extend: 'Common.store.sync.ValidatingTableStore',
    requires: 'ComplianceSurvey.model.space.ComplianceSurveyFloor',

    serverTableName: 'fl',
    serverFieldNames: [
        'bl_id',
        'fl_id',
        'name'
    ],

    inventoryKeyNames: [
        'bl_id',
        'fl_id'
    ],

    config: {
        model: 'ComplianceSurvey.model.space.ComplianceSurveyFloor',
        sorters: [
            {
                property: 'fl_id',
                direction: 'ASC'
            }
        ],
        storeId: 'complianceSurveyFloors',
        enableAutoLoad: true,
        autoSync: false,
        remoteFilter: true,
        proxy: {
            type: 'Sqlite'
        },
        tableDisplayName: LocaleManager.getLocalizedString('Floors', 'ComplianceSurvey.store.space.ComplianceSurveyFloors'),
        timestampDownload: false
    }
});