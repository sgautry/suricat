Ext.define('ComplianceSurvey.store.space.ComplianceSurveyBuildings', {
    extend: 'Common.store.sync.ValidatingTableStore',
    requires: ['ComplianceSurvey.model.space.ComplianceSurveyBuilding'],

    serverTableName: 'bl',
    serverFieldNames: [
        'bl_id',
        'name',
        'city_id',
        'state_id',
        'ctry_id',
        'address1',
        'address2',
        'lon',
        'lat',
        'site_id'
    ],

    inventoryKeyNames: ['bl_id'],

    config: {
        model: 'ComplianceSurvey.model.space.ComplianceSurveyBuilding',
        tableDisplayName: LocaleManager.getLocalizedString('Buildings', 'ComplianceSurvey.store.space.ComplianceSurveyBuildings'),
        sorters: [
            {
                property: 'bl_id',
                direction: 'ASC'
            }
        ],
        storeId: 'complianceSurveyBuildings',
        enableAutoLoad: true,
        autoSync: false,
        remoteFilter: true,
        proxy: {
            type: 'Sqlite'
        },
        timestampDownload: false
    }
});