Ext.define('ComplianceSurvey.store.Questionnaires', {
    extend: 'Common.store.sync.ValidatingTableStore',
    requires: ['ComplianceSurvey.model.Questionnaire'],

    serverTableName: 'questionnaire_ext',

    serverFieldNames: ['questionnaire_id', 'questionnaire_title', 'description', 'status'],
    inventoryKeyNames: ['questionnaire_id'],

    config: {
        model: 'ComplianceSurvey.model.Questionnaire',
        remoteSort: true,
        remoteFilter: true,
        tableDisplayName: LocaleManager.getLocalizedString('Questionnaires', 'ComplianceSurvey.store.Questionnaires'),
        sorters: [
            {
                property: 'questionnaire_title',
                direction: 'ASC'
            }
        ],
        storeId: 'questionnairesStore',
        enableAutoLoad: true,
        autoSync: false,
        proxy: {
            type: 'Sqlite'
        }
    }
});