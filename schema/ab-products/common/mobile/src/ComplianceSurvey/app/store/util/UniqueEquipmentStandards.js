Ext.define('ComplianceSurvey.store.util.UniqueEquipmentStandards', {
    extend: 'Common.store.sync.SqliteStore',

    requires: [
        'ComplianceSurvey.model.util.UniqueEquipmentStandard'
    ],

    config: {
        storeId: 'uniqueEquipmentStandards',
        model: 'ComplianceSurvey.model.util.UniqueEquipmentStandard',
        autoLoad: false,
        enableAutoLoad: false,
        remoteFilter: true,
        usesTransactionTable: true,
        proxy: {
            type: 'SqliteView',
            viewDefinition: 'SELECT DISTINCT eq_std FROM ComplianceLocation',
            viewName: 'UniqueEquipmentStandards',

            baseTables: ['ComplianceLocation']
        }
    }
});