/**
 * This view contains information from ComplianceSurveyBuilding, ComplianceSurveyFloor, ComplianceSurveyRoom and RoomStandard for the Room used by the Room Report.
 */
Ext.define('ComplianceSurvey.store.space.RoomsInfoReport', {
    extend: 'Common.store.sync.SqliteStore',

    requires: [
        'ComplianceSurvey.model.space.RoomInfoReport',
        'Common.model.RoomStandard'
    ],

    config: {
        storeId: 'surveysRoomInfoReportStore',
        model: 'ComplianceSurvey.model.space.RoomInfoReport',
        autoLoad: false,
        enableAutoLoad: false,  // The store is loaded when the Room Info report is requested.
        remoteFilter: true,
        proxy: {
            type: 'SqliteView',

            viewDefinition: 'SELECT bl.site_id, bl.ctry_id, bl.state_id, bl.city_id, bl.address1, bl.address2, ' +
            'bl.name as bl_name, fl.name as fl_name, ' +
            'rm.bl_id,rm.fl_id,rm.rm_id,rm.name,rm.rm_type,rm.rm_cat,rm.rm_std, rm.area, rm.dv_id,rm.dp_id, ' +
            'rmstd.doc_graphic, rmstd.doc_graphic_contents, rmstd.doc_block, rmstd.doc_block_contents, rm.phone ' +
            'FROM ComplianceSurveyRoom rm JOIN ComplianceSurveyBuilding bl ON rm.bl_id = bl.bl_id ' +
            'JOIN ComplianceSurveyFloor fl ON rm.bl_id = fl.bl_id AND rm.fl_id = fl.fl_id ' +
            'LEFT JOIN RoomStandard rmstd ON rm.rm_std = rmstd.rm_std',

            viewName: 'ComplianceSurveyRoomsInfoReport',

            baseTables: ['ComplianceSurveyBuilding', 'ComplianceSurveyFloor', 'ComplianceSurveyRoom', 'RoomStandard']
        }
    }
});