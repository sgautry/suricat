Ext.define('ComplianceSurvey.store.QuestQuestions', {
    extend: 'Common.store.sync.ValidatingTableStore',
    requires: ['ComplianceSurvey.model.QuestQuestion'],

    serverTableName: 'quest_question_ext',

    serverFieldNames: ['questionnaire_id', 'question_id', 'question_prefix', 'is_required', 'sort_order', 'display_format',
        'show_answer_text'
    ],
    inventoryKeyNames: ['questionnaire_id', 'question_id'],

    config: {
        model: 'ComplianceSurvey.model.QuestQuestion',
        remoteSort: true,
        remoteFilter: true,
        tableDisplayName: LocaleManager.getLocalizedString('Questionnaires', 'ComplianceSurvey.store.QuestQuestions'),
        storeId: 'questQuestionsStore',
        enableAutoLoad: true,
        autoSync: false,
        proxy: {
            type: 'Sqlite'
        }
    }
});