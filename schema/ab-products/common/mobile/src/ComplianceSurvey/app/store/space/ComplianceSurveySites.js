Ext.define('ComplianceSurvey.store.space.ComplianceSurveySites', {
    extend: 'Common.store.sync.ValidatingTableStore',
    requires: ['ComplianceSurvey.model.space.ComplianceSurveySite'],

    serverTableName: 'site',

    serverFieldNames: [
        'site_id',
        'name',
        'city_id',
        'state_id',
        'ctry_id',
        'detail_dwg'
    ],

    inventoryKeyNames: ['site_id'],

    config: {
        model: 'ComplianceSurvey.model.space.ComplianceSurveySite',
        storeId: 'complianceSurveySites',
        tableDisplayName: LocaleManager.getLocalizedString('Sites', 'ComplianceSurvey.store.space.ComplianceSurveySites'),
        remoteSort: true,
        remoteFilter: true,
        sorters: [
            {
                property: 'name',
                direction: 'ASC'
            }
        ],
        enableAutoLoad: true,
        proxy: {
            type: 'Sqlite'
        },
        timestampDownload: false
    }
});