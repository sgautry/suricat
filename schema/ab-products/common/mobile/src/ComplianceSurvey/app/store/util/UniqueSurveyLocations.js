Ext.define('ComplianceSurvey.store.util.UniqueSurveyLocations', {
    extend: 'Common.store.sync.SqliteStore',

    requires: [
        'ComplianceSurvey.model.util.UniqueSurveyLocation'
    ],

    config: {
        storeId: 'uniqueSurveyLocations',
        model: 'ComplianceSurvey.model.util.UniqueSurveyLocation',
        autoLoad: false,
        enableAutoLoad: false,
        remoteFilter: true,
        usesTransactionTable: true,
        proxy: {
            type: 'SqliteView',
            viewDefinition: 'SELECT DISTINCT location_id FROM ComplianceSurvey',
            viewName: 'UniqueSurveyLocations',

            baseTables: ['ComplianceSurvey']
        }
    }
});