Ext.define('ComplianceSurvey.store.util.QuestionItems', {
    extend: 'Common.store.sync.SqliteStore',
    requires: 'ComplianceSurvey.model.util.QuestionItem',

    config: {
        storeId: 'questionItemsStore',
        model: 'ComplianceSurvey.model.util.QuestionItem',
        autoLoad: false,
        enableAutoLoad: false,
        remoteFilter: true,
        remoteSort: true,
        usesTransactionTable: true,
        proxy: {
            type: 'SqliteView',
            viewDefinition: 'SELECT qq.questionnaire_id, q.question_id, qq.question_prefix, qq.is_required, ' +
            'qq.sort_order, q.num_responses_max, qq.display_format, ifnull(qq.show_answer_text,1) AS show_answer_text, ' +
            'q.question_type, q.question_text, q.hierarchy_ids, q.parent_answer_condition, q.is_required_child, q.sort_order_child, q.freeform_width, q.lookup_table, ' +
            'q.lookup_field, q.unit_type, q.unit_default ' +
            'FROM Question q LEFT JOIN QuestQuestion qq ON q.question_id = qq.question_id ORDER BY qq.sort_order ASC',
            viewName: 'QuestionItems',

            baseTables: ['Question', 'QuestQuestion']
        }
    }
});
