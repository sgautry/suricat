Ext.define('ComplianceSurvey.store.Answers', {
    extend: 'Common.store.sync.PagedSyncStore',
    requires: 'ComplianceSurvey.model.Answer',

    serverTableName: 'quest_answer_ext_sync',
    serverFieldNames: ['answer_id', 'question_id', 'questionnaire_id', 'survey_event_id', 'value_text', 'value_integer',
        'value_number', 'value_date', 'value_time', 'unit', 'unit_type', 'date_recorded', 'time_recorded',
        'doc',
        'mob_locked_by', 'mob_is_changed',
        'auto_number', 'guid',
        'deleted'
    ],
    inventoryKeyNames: ['survey_event_id', 'questionnaire_id', 'question_id', 'answer_id', 'guid'],

    config: {
        model: 'ComplianceSurvey.model.Answer',
        storeId: 'answersStore',
        autoSync: true,
        remoteFilter: true,
        remoteSort: true,
        enableAutoLoad: true,
        timestampDownload: false,
        proxy: {
            type: 'Sqlite'
        },
        tableDisplayName: LocaleManager.getLocalizedString('Answers', 'ComplianceSurvey.store.Answers'),
        documentConfig: {
            documentTable: 'quest_answer_ext_sync',
            documentTableKeys: ['auto_number'],
            clientTableKeys: ['auto_number']
        },
        guid: true
    },

    getNotDeletedDocumentsCount: function () {
        var me = this,
            docCount = 0;

        me.each(function (record) {
            if (record && !Ext.isEmpty(record.get('doc'))
                && record.get('doc_contents') !== Common.config.GlobalParameters.MARK_DELETED_TEXT) {
                docCount++;
            }
        }, me);

        return docCount;
    }
});
