Ext.define('ComplianceSurvey.store.AnswerOptions', {
    extend: 'Common.store.sync.ValidatingTableStore',
    requires: ['ComplianceSurvey.model.AnswerOption'],

    serverTableName: 'quest_answer_option_ext',

    serverFieldNames: ['question_id', 'answer_option_id', 'answer_option_text', 'sort_order'],
    inventoryKeyNames: ['question_id'],

    config: {
        model: 'ComplianceSurvey.model.AnswerOption',
        remoteSort: true,
        remoteFilter: true,
        tableDisplayName: LocaleManager.getLocalizedString('Answer Options', 'ComplianceSurvey.store.AnswerOptions'),
        storeId: 'answerOptionsStore',
        enableAutoLoad: true,
        autoSync: false,
        proxy: {
            type: 'Sqlite'
        },
        sorters: [
            {
                property: 'sort_order',
                direction: 'ASC'
            }
        ]
    }
});