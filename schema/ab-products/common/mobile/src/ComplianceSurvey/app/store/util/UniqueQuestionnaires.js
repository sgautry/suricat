Ext.define('ComplianceSurvey.store.util.UniqueQuestionnaires', {
    extend: 'Common.store.sync.SqliteStore',

    requires: [
        'ComplianceSurvey.model.util.UniqueQuestionnaire'
    ],

    config: {
        storeId: 'uniqueQuestionnaires',
        model: 'ComplianceSurvey.model.util.UniqueQuestionnaire',
        autoLoad: false,
        enableAutoLoad: false,
        remoteFilter: true,
        usesTransactionTable: true,
        proxy: {
            type: 'SqliteView',
            viewDefinition: 'SELECT DISTINCT questionnaire_id_ext FROM ComplianceSurvey',
            viewName: 'UniqueQuestionnaires',

            baseTables: ['ComplianceSurvey']
        }
    }
});