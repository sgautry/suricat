Ext.define('ComplianceSurvey.store.NavigationOptions', {
    extend: 'Ext.data.Store',
    requires: ['ComplianceSurvey.model.NavigationOption'],

    config: {
        model: 'ComplianceSurvey.model.NavigationOption',
        storeId: 'navigationOptionsStore',
        autoSync: true,
        disablePaging: true,
        data: [
            {
                action: 'info',
                text: LocaleManager.getLocalizedString('Survey Info', 'ComplianceSurvey.store.NavigationOption')
            },
            {
                action: 'locate',
                text: LocaleManager.getLocalizedString('Location', 'ComplianceSurvey.store.NavigationOption')
            }
        ]
    }
});