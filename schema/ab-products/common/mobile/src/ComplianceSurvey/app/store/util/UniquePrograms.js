Ext.define('ComplianceSurvey.store.util.UniquePrograms', {
    extend: 'Common.store.sync.SqliteStore',

    requires: [
        'ComplianceSurvey.model.util.UniqueProgram'
    ],

    config: {
        storeId: 'uniquePrograms',
        model: 'ComplianceSurvey.model.util.UniqueProgram',
        autoLoad: false,
        enableAutoLoad: false,
        remoteFilter: true,
        usesTransactionTable: true,
        proxy: {
            type: 'SqliteView',
            viewDefinition: 'SELECT DISTINCT reg_program FROM ComplianceSurvey',
            viewName: 'UniquePrograms',

            baseTables: ['ComplianceSurvey']
        }
    }
});