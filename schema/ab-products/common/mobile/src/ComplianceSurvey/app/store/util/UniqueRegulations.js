Ext.define('ComplianceSurvey.store.util.UniqueRegulations', {
    extend: 'Common.store.sync.SqliteStore',

    requires: [
        'ComplianceSurvey.model.util.UniqueRegulation'
    ],

    config: {
        storeId: 'uniqueRegulations',
        model: 'ComplianceSurvey.model.util.UniqueRegulation',
        autoLoad: false,
        enableAutoLoad: false,
        remoteFilter: true,
        usesTransactionTable: true,
        proxy: {
            type: 'SqliteView',
            viewDefinition: 'SELECT DISTINCT regulation FROM ComplianceSurvey',
            viewName: 'UniqueRegulations',

            baseTables: ['ComplianceSurvey']
        }
    }
});