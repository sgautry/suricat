Ext.define('ComplianceSurvey.store.Questions', {
    extend: 'Common.store.sync.ValidatingTableStore',
    requires: ['ComplianceSurvey.model.Question'],

    serverTableName: 'question_ext',

    serverFieldNames: ['question_id', 'question_type', 'question_text', 'is_active', 'hierarchy_ids', 'parent_answer_condition',
        'is_required_child', 'sort_order_child', 'freeform_width', 'lookup_table', 'lookup_field', 'unit_type', 'unit_default', 'num_responses_max'],
    inventoryKeyNames: ['question_id'],

    config: {
        model: 'ComplianceSurvey.model.Question',
        remoteSort: true,
        remoteFilter: true,
        tableDisplayName: LocaleManager.getLocalizedString('Questions', 'ComplianceSurvey.store.Questions'),
        storeId: 'questionsStore',
        enableAutoLoad: true,
        autoSync: false,
        proxy: {
            type: 'Sqlite'
        },
        restriction: [
            {
                tableName: 'question_ext',
                fieldName: 'is_active',
                operation: 'EQUALS',
                value: '1'
            }
        ]
    }
});