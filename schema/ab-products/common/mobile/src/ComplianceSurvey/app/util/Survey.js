Ext.define('ComplianceSurvey.util.Survey', {
    singleton: true,
    alternateClassName: ['SurveyUtil'],

    surveyCalculatedStatusMap: [{
        status: 'missed',
        colorClass: 'ab-status-missed',
        text: LocaleManager.getLocalizedString('Missed', 'ComplianceSurvey.util.Questionnaire'),
        filterQuery: "(status NOT IN ('CANCELLED', 'COMPLETED', 'COMPLETED-V', 'CLOSED')" +
        " AND date(date_scheduled_end) < date())"
    }, {
        status: 'overdue',
        colorClass: 'ab-status-overdue',
        text: LocaleManager.getLocalizedString('Overdue', 'ComplianceSurvey.util.Questionnaire'),
        filterQuery: "(status NOT IN ('CANCELLED', 'COMPLETED', 'COMPLETED-V', 'CLOSED', 'IN PROGRESS')" +
        " AND date(date_scheduled_end) >= date() AND date(date_scheduled) < date())"
    }, {
        status: 'in_progress',
        colorClass: 'ab-status-in-progress',
        text: LocaleManager.getLocalizedString('In Progress', 'ComplianceSurvey.util.Questionnaire'),
        filterQuery: "(status = 'IN PROGRESS'" +
        " AND date(date_scheduled_end) >= date())"
    }, {
        status: 'completed',
        colorClass: 'ab-status-completed',
        text: LocaleManager.getLocalizedString('Completed', 'ComplianceSurvey.util.Questionnaire'),
        filterQuery: "status IN ('CANCELLED', 'COMPLETED', 'COMPLETED-V', 'CLOSED')"
    }, {
        status: 'scheduled',
        colorClass: 'ab-status-in-progress',
        text: LocaleManager.getLocalizedString('Scheduled', 'ComplianceSurvey.util.Questionnaire'),
        filterQuery: "(status = 'SCHEDULED'" +
        " AND date(date_scheduled_end) >= date() AND date(date_scheduled) >= date())"
    }],

    // Calculated status: 1 - Missed, 2 - Overdue, 3 - In progress, 4 - Scheduled, 5 - Completed.
    surveySortByCalculatedStatus: "CASE"
    + " WHEN status = 'CANCELLED' || status = 'COMPLETED' || status = 'COMPLETED-V' || status = 'CLOSED' THEN 5"
    + " WHEN date(date_scheduled_end) < date() THEN 1"
    + " WHEN status = 'IN PROGRESS' THEN 3"
    + " WHEN date(date_scheduled) < date() THEN 2"
    + " ELSE 4"
    + " END",

    /**
     missed (Red): Missed events are those where activity_log.status NOT IN (CANCELLED, COMPLETED, COMPLETED-V, CLOSED) AND date_scheduled_end < today().
     overdue (Orange): Overdue events are those where activity_log.status NOT IN (CANCELLED, COMPLETED, COMPLETED-V, CLOSED, IN PROGRESS) AND date_ scheduled < today().
     in_progress (Blue): activity_log.status = 'IN PROGRESS'
     completed (Green): Event status IN (CANCELLED, COMPLETED, COMPLETED-V, CLOSED)
     */
    calculateSurveyStatus: function (record) {
        var status = record.get('status'),
            dateScheduled = record.get('date_scheduled'),
            dateScheduledEnd = record.get('date_scheduled_end'),
            today = (new Date()).setHours(0, 0, 0, 0),
            surveyStatus = '';

        if (status === 'CANCELLED' || status === 'COMPLETED' || status === 'COMPLETED-V' || status === 'CLOSED') {
            surveyStatus = 'completed';
        } else if (Ext.isDate(dateScheduledEnd) && dateScheduledEnd < today) {
            surveyStatus = 'missed';
        } else if (status === 'IN PROGRESS') {
            surveyStatus = 'in_progress';
        } else if (Ext.isDate(dateScheduled) && dateScheduled < today) {
            surveyStatus = 'overdue';
        } else if (status === 'SCHEDULED') {
            surveyStatus = 'scheduled';
        }

        return surveyStatus;
    },

    /**
     * Returns the calculated status item from {@link ComplianceSurvey.util.Survey.surveyCalculatedStatusMap}
     * for the calculated status passed as parameter.
     * @param calculatedStatus
     * @returns {*}
     */
    getCalculatedStatusItem: function (calculatedStatus) {
        var me = this,
            statusItem = null,
            filteredItems = Ext.Array.filter(me.surveyCalculatedStatusMap, function (item) {
                return item.status === calculatedStatus;
            });

        if (!Ext.isEmpty(filteredItems)) {
            statusItem = filteredItems[0];
        }

        return statusItem;
    },

    /**
     * Returns the css color class
     * for the calculated status passed as parameter.
     * @param calculatedStatus
     * @returns {string}
     */
    getCalculatedStatusColorClass: function (calculatedStatus) {
        var me = this,
            statusItem,
            colorClass = "";

        statusItem = me.getCalculatedStatusItem(calculatedStatus);

        if (!Ext.isEmpty(statusItem)) {
            colorClass = statusItem.colorClass;
        }

        return colorClass;
    },

    /**
     * Returns the status text to display
     * for the calculated status passed as parameter.
     * @param calculatedStatus
     * @returns {string}
     */
    getCalculatedStatusText: function (calculatedStatus) {
        var me = this,
            statusItem,
            text = "";

        statusItem = me.getCalculatedStatusItem(calculatedStatus);

        if (!Ext.isEmpty(statusItem)) {
            text = statusItem.text;
        }

        return text;
    },

    /**
     * Returns the sql query to filter the surveys store
     * for the calculated status passed as parameter.
     * @param calculatedStatus
     * @returns {string}
     */
    getCalculatedStatusFilterQuery: function (calculatedStatus) {
        var me = this,
            statusItem,
            filterQuery = "";

        statusItem = me.getCalculatedStatusItem(calculatedStatus);

        if (!Ext.isEmpty(statusItem)) {
            filterQuery = statusItem.filterQuery;
        }

        return filterQuery;
    }
});