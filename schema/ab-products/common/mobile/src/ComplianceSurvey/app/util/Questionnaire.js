Ext.define('ComplianceSurvey.util.Questionnaire', {
    singleton: true,
    alternateClassName: ['QuestionnaireUtil'],

    // separator character for multiple values in one field
    MULTIPLE_VALUES_SEPARATOR: ', ',

    questionRecordsToBeDeleted: [],

    nonSelectableQuestions: ['label', 'action'],

    generateFormFields: function (questionnaire, searchFilter, displayOnlyParentItems) {
        var me = this,
            items = [],
            formField,
            store = Ext.getStore(questionnaire.getStoreId()),
            filter = Ext.create('Common.util.Filter', {
                property: 'questionnaire_id',
                value: questionnaire.questionnaireId,
                exactMatch: true
            }),
            filterArray = [];

        questionnaire.numberOfParentQuestions = 0;

        //reset the map of child questions
        questionnaire.followUpMap = [];

        filterArray.push(filter);

        if (!Ext.isEmpty(searchFilter)) {
            filterArray.push(searchFilter);
        }

        return store.retrieveAllRecords(filterArray)
            .then(function (questionRecords) {
                var p = Promise.resolve();
                questionRecords.forEach(function (questionRecord) {
                    p = p.then(function () {
                        return me.setAnswerOptions(questionnaire, questionRecord)
                            .then(function (options) {
                                return new Promise(function (resolve) {
                                    formField = me.generateFormField(questionnaire, questionRecord, options, displayOnlyParentItems);
                                    if (!Ext.isEmpty(formField)) {
                                        items.push(formField);
                                        if (!Ext.Array.contains(me.nonSelectableQuestions, questionRecord.get('question_type'))) {
                                            questionnaire.numberOfParentQuestions++;
                                        }
                                    }
                                    resolve();
                                });
                            });
                    });
                });
                return p;
            })
            .then(function () {
                questionnaire.setItems(items);
                questionnaire.isViewLoading = true;
                return me.loadAnswers(items, questionnaire);
            })
            .then(function () {
                questionnaire.isViewLoading = false;
                return Promise.resolve();
            })
            .then(null, function () {
                questionnaire.isViewLoading = false;
                return Promise.reject();
            });
    },

    generateFormField: function (questionnaire, record, options, displayOnlyParentItems) {
        var me = this,
            type = record.get('question_type'),
            isSelectableQuestion = !Ext.Array.contains(me.nonSelectableQuestions, type),
            questionId = record.get('question_id'),
            questionnaireId = questionnaire.questionnaireId,
            numMaxResponses = record.get('num_responses_max'),
            supportedMultipleFieldTypes = ['lookup', 'date', 'time', 'count', 'measurement'],
            hierarchyIds = record.get('hierarchy_ids'),
            fieldConfig = {},
            fieldSet = {
                xtype: 'panel',

                items: [{
                    xtype: isSelectableQuestion ? 'checkboxfield' : 'spacer',
                    name: (isSelectableQuestion ? 'checkbox' : 'spacer') + '_question_' + questionnaireId + '_' + questionId,
                    cls: ['ab-list-checkbox', 'ab-form-checkbox'],
                    docked: 'left'
                }]
            },
            multiplefieldName;

        if (displayOnlyParentItems) {
            if (hierarchyIds !== (questionId + '|')) {
                return null;
            }
        }

        //multiple_choice_single;Multiple Choice - Single Answer;multiple_choice_multi;Multiple Choice - Multiselect;
        // freeform;Freeform;date;Date;time;Time;lookup;Lookup;count;Count;measurement;Measurement;label;Label;
        // document;Document/Photo;action;Action

        fieldConfig = me.getFieldConfig(questionnaire, record, options);

        if (Ext.isEmpty(fieldConfig)) {
            return null;
        }

        fieldConfig.name = me.getQuestionFieldName(questionnaire.questionnaireId, questionId);
        fieldConfig.labelWrap = Ext.os.is.Phone;
        fieldConfig.labelCls = Ext.os.is.Phone ? 'x-form-label-phone' : '';
        fieldConfig.labelWidth = '40%';
        fieldConfig.record = record;
        if (me.isParentQuestion(record)) {
            fieldConfig.required = record.get('is_required');
        } else {
            fieldConfig.required = record.get('is_required_child');
        }

        // multiple responses (records)
        if (numMaxResponses > 1 && Ext.Array.contains(supportedMultipleFieldTypes, type)) {
            multiplefieldName = fieldConfig.name + '_multirecord';
            fieldConfig = me.getMultipleFieldConfig(multiplefieldName, fieldConfig, numMaxResponses);
            fieldConfig.record = record;
        }

        fieldSet.items.push(fieldConfig);
        fieldSet.record = record;

        return fieldSet;
    },

    getQuestionFieldName: function (questionnaireId, questionId) {
        return 'question_' + questionnaireId + '_' + questionId;
    },

    getFieldConfig: function (questionnaire, record, options) {
        var me = this,
            type = record.get('question_type'),
            fieldConfig = null;

        switch (type) {
            case 'multiple_choice_single': {
                fieldConfig = me.getMultiChoiceSingleFieldConfig(questionnaire, record, options);
                break;
            }

            case 'multiple_choice_multi': {
                fieldConfig = me.getMultiChoiceMultiFieldConfig(questionnaire, record, options);
                break;
            }

            case 'freeform': {
                fieldConfig = me.getFreeFormFieldConfig(questionnaire, record);
                break;
            }

            case 'date': {
                fieldConfig = me.getDateFieldConfig(questionnaire, record);
                break;
            }

            case 'time': {
                fieldConfig = me.getTimeFieldConfig(questionnaire, record);
                break;
            }
            case 'lookup': {
                fieldConfig = me.getLookupFieldConfig(questionnaire, record);
                break;
            }
            case 'count': {
                fieldConfig = me.getCountFieldConfig(questionnaire, record);
                break;
            }
            case 'measurement': {
                fieldConfig = me.getMeasurementFieldConfig(questionnaire, record);
                break;
            }
            case 'label': {
                fieldConfig = me.getLabelFieldConfig(record, options);
                break;
            }
            case 'document': {
                fieldConfig = me.getDocumentFieldConfig(questionnaire, record);
                break;
            }
            case 'action': {
                fieldConfig = me.getActionFieldConfig(record);
                break;
            }
        }

        return fieldConfig;
    },

    getDocumentFieldConfig: function (questionnaire, record) {
        var me = this,
            questionnaireRecord = questionnaire.getRecord(),
            questionText = me.getQuestionText(record),
            buttonText = LocaleManager.getLocalizedString('Documents', 'ComplianceSurvey.util.Questionnaire');

        return {
            xtype: 'documentquestion',
            text: questionText,
            viewSelectorConfig: {
                xtype: 'complianceSurveyViewSelector',
                navigationView: 'main',
                questionRecord: record,
                questionnaireRecord: questionnaireRecord,
                items: [
                    {
                        text: buttonText,
                        store: 'answersStore',
                        view: 'complianceSurveyDocumentList',
                        documentSelect: true
                    }
                ]
            }
        };
    },

    getMultipleFieldConfig: function (name, fieldConfig, numMaxResponses) {
        return {
            xtype: 'multiplefield',
            name: name,
            maxFields: numMaxResponses,
            buttonText: LocaleManager.getLocalizedString('Add another response', 'ComplianceSurvey.util.Questionnaire'),
            maxFieldsText: LocaleManager.getLocalizedString('You have added all responses allowed for this question',
                'ComplianceSurvey.util.Questionnaire'),
            fieldConfig: fieldConfig
        };
    },

    getQuestionText: function (record) {
        var prefix = Ext.isEmpty(record.get('question_prefix')) ? '' : record.get('question_prefix'),
            text = Ext.isEmpty(record.get('question_text')) ? '' : record.get('question_text');

        return prefix + ' ' + text;
    },

    getMultiChoiceSingleFieldConfig: function (questionnaire, record, options) {
        var me = this,
            fieldConfig = {},
            questionText = this.getQuestionText(record),
            displayFormat = record.get('display_format'),
            showAnswerText = !!record.get('show_answer_text'),
            isFollowupQuestion = me.isFollowUpQuestion(record);

        fieldConfig.xtype = 'radioquestion';
        fieldConfig.text = questionText;
        fieldConfig.layout = (displayFormat === 'Long Form' || isFollowupQuestion) ? 'vbox' : 'hbox';
        if (showAnswerText) {
            fieldConfig.options = options;
        } else {
            fieldConfig.options = [];
            Ext.each(options, function (option) {
                fieldConfig.options.push({
                    value: option.value
                });
            });
        }
        fieldConfig.listeners = {
            change: function (optionField) {
                me.onFieldChange(questionnaire, optionField.getParent(), function () {
                    me.handleFollowUpItems(questionnaire, optionField);
                }, me);

            },
            scope: me
        };

        return fieldConfig;
    },

    getMultiChoiceMultiFieldConfig: function (questionnaire, record, options) {
        var me = this,
            fieldConfig = {},
            questionText = this.getQuestionText(record),
            displayFormat = record.get('display_format'),
            showAnswerText = !!record.get('show_answer_text'),
            isFollowupQuestion = me.isFollowUpQuestion(record);

        fieldConfig.xtype = 'checkquestion';
        fieldConfig.text = questionText;
        fieldConfig.layout = (displayFormat === 'Long Form' || isFollowupQuestion) ? 'vbox' : 'hbox';
        if (showAnswerText) {
            fieldConfig.options = options;
        } else {
            fieldConfig.options = [];
            Ext.each(options, function (option) {
                fieldConfig.options.push({
                    value: option.value
                });
            });
        }
        fieldConfig.listeners = {
            change: function (optionField) {
                me.onFieldChange(questionnaire, optionField.getParent(), function () {
                    me.handleFollowUpItems(questionnaire, optionField);
                }, me);

            },
            scope: me
        };

        return fieldConfig;
    },

    getFreeFormFieldConfig: function (questionnaire, record) {
        var me = this,
            fieldConfig = {},
            questionText = this.getQuestionText(record),
            freeFormWidth = record.get('freeform_width'),
            displayFormat = record.get('display_format');

        if (freeFormWidth <= 20) {
            fieldConfig.xtype = 'commontextfield';
        } else {
            fieldConfig.xtype = 'commontextareafield';
        }
        fieldConfig.maxLength = freeFormWidth;
        fieldConfig.label = questionText;
        fieldConfig.labelAlign = displayFormat === 'Long Form' ? 'top' : 'left';
        fieldConfig.listeners = {
            change: function (field) {
                me.onFieldChange(questionnaire, field);
            },
            clearicontap: function (field) {
                me.deleteAnswersForField(field, questionnaire);
            },
            scope: me
        };

        return fieldConfig;
    },

    getDateFieldConfig: function (questionnaire, record) {
        var me = this,
            fieldConfig = {},
            questionText = this.getQuestionText(record),
            displayFormat = record.get('display_format');

        fieldConfig.xtype = 'calendarfield';
        fieldConfig.label = questionText;
        fieldConfig.labelAlign = displayFormat === 'Long Form' ? 'top' : 'left';
        fieldConfig.listeners = {
            change: function (field) {
                me.onFieldChange(questionnaire, field);
            },
            clearicontap: function (field) {
                me.deleteAnswersForField(field, questionnaire);
            },
            scope: me
        };

        return fieldConfig;
    },

    getTimeFieldConfig: function (questionnaire, record) {
        var me = this,
            fieldConfig = {},
            questionText = this.getQuestionText(record),
            displayFormat = record.get('display_format');

        fieldConfig.xtype = 'timefield';
        fieldConfig.picker = {
            xtype: 'timepickerconfig',
            toolbar: {items: []} // no Clear button
        };
        fieldConfig.label = questionText;
        fieldConfig.labelAlign = displayFormat === 'Long Form' ? 'top' : 'left';
        fieldConfig.listeners = {
            change: function (field) {
                me.onFieldChange(questionnaire, field);
            },
            clearicontap: function (field) {
                me.deleteAnswersForField(field, questionnaire);
            },
            scope: me
        };

        return fieldConfig;
    },

    getLookupFieldConfig: function (questionnaire, record) {
        var me = this,
            fieldConfig = {},
            questionText = this.getQuestionText(record),
            displayFormat = record.get('display_format'),
            tableName,
            fieldName,
            tableDefObject,
            fieldDef,
            fieldTitle,
            store;

        tableName = record.get('lookup_table');
        fieldName = record.get('lookup_field');

        store = me.createLookupStore(tableName, fieldName);
        store.load();

        fieldConfig.xtype = 'prompt';
        fieldConfig.store = 'lookup_' + tableName + '_' + fieldName + '_store';
        fieldConfig.valueField = fieldName;
        fieldConfig.label = questionText;
        fieldConfig.labelAlign = displayFormat === 'Long Form' ? 'top' : 'left';
        fieldConfig.listeners = {
            change: function (field) {
                me.onFieldChange(questionnaire, field);
            },
            clearicontap: function (field) {
                me.deleteAnswersForField(field, questionnaire);
            },
            scope: me
        };

        tableDefObject = TableDef.getTableDefObject(tableName);
        fieldDef = TableDef.findFieldDef(tableDefObject, fieldName);
        if (fieldDef.archibusType === 'BARCODE') {
            fieldConfig.enableBarcodeScanning = true;
        }
        fieldTitle = fieldDef.multiLineHeadings.join(Ext.os.is.Phone ? '<br>' : ' ');
        fieldConfig.displayFields = [{name: fieldName, title: fieldTitle}];
        fieldConfig.title = tableDefObject.heading;

        return fieldConfig;
    },

    getCountFieldConfig: function (questionnaire, record) {
        var me = this,
            fieldConfig = {},
            questionText = this.getQuestionText(record),
            displayFormat = record.get('display_format');

        fieldConfig.xtype = 'localizedspinnerfield';
        fieldConfig.label = questionText;
        fieldConfig.labelAlign = displayFormat === 'Long Form' ? 'top' : 'left';
        fieldConfig.stepValue = 1;
        fieldConfig.decimals = 0;
        fieldConfig.listeners = {
            change: function (field) {
                me.onFieldChange(questionnaire, field);
            },
            clearicontap: function (field) {
                me.deleteAnswersForField(field, questionnaire);
            },
            scope: me
        };

        return fieldConfig;
    },

    getMeasurementFieldConfig: function (questionnaire, record) {
        var me = this,
            fieldConfig = {},
            questionText = this.getQuestionText(record),
            displayFormat = record.get('display_format'),
            unit = record.get('unit_default');

        fieldConfig.xtype = 'measurementfield';
        fieldConfig.label = questionText;
        fieldConfig.labelAlign = displayFormat === 'Long Form' ? 'top' : 'left';
        if (!Ext.isEmpty(unit)) {
            fieldConfig.unitLabel = unit;
        }
        fieldConfig.listeners = {
            change: function (field) {
                me.onFieldChange(questionnaire, field);
            },
            clearicontap: function (field) {
                me.deleteAnswersForField(field, questionnaire);
            },
            scope: me
        };

        return fieldConfig;
    },

    getLabelFieldConfig: function (record, options) {
        var fieldConfig = {},
            questionText = this.getQuestionText(record),
            labelOptions = [];

        fieldConfig.xtype = 'labelquestion';
        fieldConfig.text = questionText;
        if (!Ext.isEmpty(options)) {
            Ext.each(options, function (option) {
                labelOptions.push({
                    html: option.label
                });
            });
            fieldConfig.options = labelOptions;
        }

        return fieldConfig;
    },

    getActionFieldConfig: function (record) {
        var fieldConfig = {},
            questionText = this.getQuestionText(record);

        fieldConfig.xtype = 'label';
        fieldConfig.cls = ['ab-question-text', 'ab-question-text-long-form'];
        fieldConfig.html = questionText;

        return fieldConfig;
    },

    setAnswerOptions: function (questionnaire, questionRecord) {
        var options = [],
            store = Ext.getStore('answerOptionsStore'),
            filter = Ext.create('Common.util.Filter', {
                property: 'question_id',
                value: questionRecord.get('question_id'),
                exactMatch: true
            });


        return store.retrieveAllRecords([filter])
            .then(function (optionRecords) {
                var p = Promise.resolve();
                optionRecords.forEach(function (optionRecord) {
                    p = p.then(function () {
                        return new Promise(function (resolve) {
                            options.push({
                                value: optionRecord.get('answer_option_id'),
                                label: optionRecord.get('answer_option_text')
                            });
                            resolve();
                        });
                    });
                });
                return p;
            })
            .then(function () {
                return new Promise(function (resolve) {
                    resolve(options);
                });
            });
    },

    handleFollowUpItems: function (questionnaire, optionField) {
        var me = this,
            isChecked = optionField.getChecked(),
            questionCopmponent = optionField.getParent(),
            record;

        if (Ext.isEmpty(questionCopmponent)) {
            return;
        }
        record = questionCopmponent.getRecord();
        if (Ext.isEmpty(record)) {
            return;
        }

        if (isChecked) {
            me.addFollowUpQuestions(questionnaire, optionField);
        } else {
            me.removeFollowUpQuestions(questionnaire, optionField);
        }
    },

    addFollowUpQuestions: function (questionnaire, optionField) {
        var me = this,
            questionCopmponent = optionField.getParent(),
            record = questionCopmponent.getRecord(),
            followUpQuestionPanel,
            questionMapObject,
            filteredMap,
            incrementIndex = function (index, followUpRecord) {
                if (!Ext.isEmpty(questionnaire.getAt(index)) && !Ext.isEmpty(questionnaire.getAt(index).getRecord())) {
                    while (!me.isParentQuestion(questionnaire.getAt(index).getRecord())
                    && questionnaire.getAt(index).getRecord().get('sort_order_child')
                    < followUpRecord.get('sort_order_child')) {
                        index++;
                    }
                }
                return index;
            };

        me.findFollowUpItems(questionnaire, optionField, record, function (index, followUpRecords) {
            var p = Promise.resolve();
            followUpRecords.forEach(function (followUpRecord) {
                p = p.then(function () {
                    return me.setAnswerOptions(questionnaire, followUpRecord)
                        .then(function (options) {
                            return new Promise(function (resolve) {
                                questionMapObject = {
                                    parent_id: record.get('question_id'),
                                    parent_answer_condition: followUpRecord.get('parent_answer_condition'),
                                    child_id: followUpRecord.get('question_id'),
                                    is_required: followUpRecord.get('is_required_child')
                                };

                                filteredMap = questionnaire.followUpMap.filter(function (item) {
                                    if (item.parent_id === questionMapObject.parent_id &&
                                        item.parent_answer_condition === questionMapObject.parent_answer_condition &&
                                        item.child_id === questionMapObject.child_id) {
                                        return true;
                                    } else {
                                        return false;
                                    }
                                });
                                if (filteredMap.length > 0) {
                                    // the follow-up question is already displayed;
                                    // this might happen for multiple choice multiple answer parent questions with
                                    // follow up questions that have multiple id values for parent_answer_condition
                                    resolve();
                                } else {
                                    followUpQuestionPanel = me.generateFormField(questionnaire, followUpRecord, options, false);
                                    if (Ext.isEmpty(followUpQuestionPanel)) {
                                        resolve();
                                    } else {
                                        // indent follow-up questions
                                        followUpQuestionPanel.cls = 'ab-child-question-panel';

                                        // if there are follow-up questions for the same parent already displayed
                                        // increment the index according to follow-up question sort_order value
                                        incrementIndex(index, followUpRecord);
                                        questionnaire.insert(index, followUpQuestionPanel);

                                        // keep a map of follow-up questions which is used for removing them from the panel
                                        questionnaire.followUpMap.push(questionMapObject);
                                        return me.loadAnswers([followUpQuestionPanel], questionnaire)
                                            .then(function () {
                                                return me.updatePercentageOfCompletion(questionnaire);
                                            })
                                            .then(function () {
                                                if (questionnaire.isViewLoading) {
                                                    return Promise.resolve();
                                                } else {
                                                    return QuestionnaireUtil.resetSurveyStatusIfCompleted(questionnaire.getRecord());
                                                }
                                            })
                                            .then(resolve, null)
                                            .then(null, resolve);
                                    }
                                }
                            });
                        });
                });
            });
            return p;
        }, me);
    },

    findFollowUpItems: function (questionnaire, optionField, record, onCompleteFn, scope) {
        var me = this,
            value = optionField.getValue(),
            parentAnswerCond,
            parentAnswerCondIds = [],
            questionCopmponent = optionField.getParent(),
            questionId = record.get('question_id'),
            questionItemsStore = Ext.getStore(questionnaire.getStoreId()),
            filterArray = [],
            followUpRecords = [],
            parentQuestionItemIndex,
            items,
            i;

        filterArray.push(Ext.create('Common.util.Filter', {
            property: 'hierarchy_ids',
            value: questionId + '|%',
            exactMatch: true,
            conjunction: 'OR'
        }));
        filterArray.push(Ext.create('Common.util.Filter', {
            property: 'hierarchy_ids',
            value: '%|' + questionId + '|%',
            exactMatch: true,
            conjunction: 'OR'
        }));
        questionItemsStore.retrieveAllRecords(filterArray)
            .then(function (storeRecords) {
                // one record is the parent question itself
                if (storeRecords.length > 1) {
                    for (i = 0; i < storeRecords.length; i++) {
                        parentAnswerCond = storeRecords[i].get('parent_answer_condition');
                        if (!Ext.isEmpty(parentAnswerCond)) {
                            //remove non-ascii characters
                            parentAnswerCondIds = parentAnswerCond.replace(/[^\x00-\x7F]/g, "").split(me.MULTIPLE_VALUES_SEPARATOR);
                        }
                        // if it's not the parent question and the answer condition is met
                        if (storeRecords[i].get('hierarchy_ids') !== (questionId + '|') && Ext.Array.contains(parentAnswerCondIds, value.toString())) {
                            followUpRecords.push(storeRecords[i]);
                        }
                    }

                    items = questionnaire.getItems();
                    parentQuestionItemIndex = items.indexOf(questionCopmponent.getParent());
                    Ext.callback(onCompleteFn, scope || me, [parentQuestionItemIndex + 1, followUpRecords]);
                }
            });
    },

    removeFollowUpQuestions: function (questionnaire, optionField) {
        var me = this,
            questionCopmponent = optionField.getParent(),
            record = questionCopmponent.getRecord(),
            questionComponentValues,
            questionId = record.get('question_id'),
            followUpQuestionId,
            questionnaireId,
            followUpQuestion,
            tempFollowUpMap,
            parentAnswerCondIds = [],
            mapObject,
            keepChild = false,
            i;

        tempFollowUpMap = questionnaire.followUpMap.slice(); //copy array
        for (i = 0; i < questionnaire.followUpMap.length; i++) {
            mapObject = questionnaire.followUpMap[i];
            if (!Ext.isEmpty(mapObject.parent_answer_condition)) {
                parentAnswerCondIds = mapObject.parent_answer_condition.replace(/[^\x00-\x7F]/g, "").split(me.MULTIPLE_VALUES_SEPARATOR);
            }

            if ((record.get('question_type') === 'multiple_choice_multi')) {
                questionComponentValues = questionCopmponent.getValues();
                keepChild = me.checkOtherAnswers(parentAnswerCondIds, questionComponentValues);
            }
            if (questionId === mapObject.parent_id && Ext.Array.contains(parentAnswerCondIds, optionField.getValue().toString()) && !keepChild) {
                followUpQuestionId = mapObject.child_id;
                questionnaireId = questionnaire.questionnaireId;
                followUpQuestion = questionnaire.down('*[name=question_' + questionnaireId + '_' + followUpQuestionId + ']');

                // remove the parent which is the panel containing the follow up question and its selection checkbox
                me.questionRecordsToBeDeleted.push(followUpQuestion.getParent().getRecord());
                questionnaire.remove(followUpQuestion.getParent(), true);

                tempFollowUpMap.splice(tempFollowUpMap.indexOf(mapObject), 1);

                tempFollowUpMap = me.removeOrphanFollowUpQuestions(questionnaire, tempFollowUpMap, followUpQuestionId, questionnaireId);
            }
        }

        me.deleteAnswersForRecords(me.questionRecordsToBeDeleted, questionnaire.getRecord());

        questionnaire.followUpMap = tempFollowUpMap;
    },

    /**
     * Verify that other selected question options display the same follow-up question.
     * @param parentAnswerCondIds
     * @param parentSelectedAnswerIds
     * @returns {boolean}
     */
    checkOtherAnswers: function (parentAnswerCondIds, parentSelectedAnswerIds) {
        var i;

        for (i = 0; i < parentSelectedAnswerIds.length; i++) {
            if (parentAnswerCondIds.indexOf(parentSelectedAnswerIds[i].toString()) >= 0) {
                return true;
            }
        }

        return false;
    },

    removeOrphanFollowUpQuestions: function (questionnaire, tempFollowUpMap, followUpQuestionId, questionnaireId) {
        var me = this,
            newFollowUpMap,
            i,
            mapObject,
            orphanQuestionId,
            orphanQuestion;

        newFollowUpMap = tempFollowUpMap.slice(); //copy array
        for (i = 0; i < tempFollowUpMap.length; i++) {
            mapObject = tempFollowUpMap[i];
            if (followUpQuestionId === mapObject.parent_id) {
                orphanQuestionId = mapObject.child_id;
                orphanQuestion = questionnaire.down('*[name=question_' + questionnaireId + '_' + orphanQuestionId + ']');

                // remove the parent which is the panel containing the follow up question and its selection checkbox
                me.questionRecordsToBeDeleted.push(orphanQuestion.getParent().getRecord());
                questionnaire.remove(orphanQuestion.getParent(), true);

                newFollowUpMap.splice(newFollowUpMap.indexOf(mapObject), 1);

                me.removeOrphanFollowUpQuestions(questionnaire, newFollowUpMap, orphanQuestionId, questionnaireId);
            }
        }
        return newFollowUpMap;
    },

    isParentQuestion: function (record) {
        var questionId = record.get('question_id'),
            hierarchyIds = record.get('hierarchy_ids');

        return hierarchyIds === questionId + '|';

    },

    isFollowUpQuestion: function (record) {
        var questionId = record.get('question_id'),
            hierarchyIds = record.get('hierarchy_ids');

        return hierarchyIds.indexOf('|' + questionId + '|') > 0;
    },

    createAnswerRecord: function (questionnaireRecord, questionRecord, value) {
        var me = this,
            newRecord;

        newRecord = Ext.create('ComplianceSurvey.model.Answer');
        newRecord = me.setAnswerRecord(newRecord, questionnaireRecord, questionRecord, value);
        newRecord.setDirty(true);

        return newRecord;
    },

    onFieldChange: function (questionnaireView, field, callbackFn, scope) {
        var me = this,
            questionRecord = field.getParent().getRecord(),
            questionnaireRecord = questionnaireView.getRecord(),
            answerRecordId = field.answerRecordId,
            numMaxResponses = questionRecord.get('num_responses_max'),
            value,
            store = Ext.getStore('answersStore'),
            autoSync = store.getAutoSync(),
            filterArray,
            updateSurveyStatusAndExit = function () {
                me.updatePercentageOfCompletion(questionnaireView)
                    .then(function () {
                        if (questionnaireView.isViewLoading) {
                            return Promise.resolve();
                        } else {
                            return me.resetSurveyStatusIfCompleted(questionnaireRecord);
                        }
                    })
                    .then(function () {
                        Ext.callback(callbackFn, scope || me);
                    })
                    .then(null, function () {
                        Ext.callback(callbackFn, scope || me);
                    });
            },
            saveAnswer = function (record) {
                var newRecord = null,
                    hadNoAnswer = Ext.isEmpty(field.answerRecordId) || field.isAnswerDeleted === 1,
                    previousAnswerCount = me.countMultipleFieldAnswers(questionnaireView, field);

                if (Ext.isEmpty(record)) {
                    newRecord = me.createAnswerRecord(questionnaireRecord, questionRecord, value);
                    store.setAutoSync(false);
                    store.add(newRecord);
                } else {
                    me.setAnswerRecord(record, questionnaireRecord, questionRecord, value);
                }

                store.sync(function () {
                    store.setAutoSync(autoSync);

                    if (!Ext.isEmpty(newRecord)) {
                        field.answerId = newRecord.get('answer_id');
                        field.answerRecordId = newRecord.get('id');
                    }
                    field.isAnswerDeleted = newRecord ? newRecord.get('deleted') : record.get('deleted');

                    if (me.isParentQuestion(questionRecord)
                        && (hadNoAnswer && field.isAnswerDeleted === 0)
                        && (numMaxResponses <= 1 || previousAnswerCount === 0)) {
                        questionnaireView.numberOfAnsweredParentQuestions++;

                    }
                    updateSurveyStatusAndExit();
                });
            };

        if (Ext.isEmpty(questionRecord) || Ext.isEmpty(questionnaireRecord)) {
            Ext.callback(callbackFn, scope || me);
            return;
        }

        if (questionnaireView.isViewLoading || questionnaireView.disableFieldChangeEvents) {
            Ext.callback(callbackFn, scope || me);
            return;
        }

        if (questionRecord.get('question_type') === 'multiple_choice_multi') {
            value = field.getValues().join(me.MULTIPLE_VALUES_SEPARATOR);
        } else if (Ext.isDefined(field.getValue)) {
            // for 'action' questions there is no value
            value = field.getValue();
        }

        if (Ext.isEmpty(answerRecordId)) {
            saveAnswer();
        } else {
            filterArray = me.getAnswerFilters(questionnaireRecord, questionRecord, [answerRecordId]);
            store.clearFilter();
            store.setFilters(filterArray);
            store.load(function (records) {
                saveAnswer(!Ext.isEmpty(records) ? records[0] : null);
            });
        }
    },

    countMultipleFieldAnswers: function (questionnaireView, field) {
        var fieldName = typeof field.getName === 'function' ? field.getName() : field.name,
            answerFields,
            answerField,
            i,
            numberOfAnswers = 0;

        if (!Ext.isEmpty(fieldName)) {
            answerFields = questionnaireView.query('*[name=' + fieldName + ']');
            for (i = 0; i < answerFields.length; i++) {
                answerField = answerFields[i];
                if (!Ext.isEmpty(answerField.answerRecordId) && answerField.isAnswerDeleted === 0) {
                    numberOfAnswers++;
                }
            }
        }

        return numberOfAnswers;
    },

    setAnswerRecord: function (answerRecord, questionnaireRecord, questionRecord, value) {
        var type = questionRecord.get('question_type'),
            unitType = questionRecord.get('unit_type'),
            unit = questionRecord.get('unit_default');

        answerRecord.set('questionnaire_id', questionnaireRecord.get('questionnaire_id_ext'));
        answerRecord.set('survey_event_id', questionnaireRecord.get('activity_log_id'));
        answerRecord.set('question_id', questionRecord.get('question_id'));

        switch (type) {
            case 'multiple_choice_single': {
                answerRecord.set('value_text', value);
                break;
            }

            case 'multiple_choice_multi': {
                answerRecord.set('value_text', value);
                break;
            }

            case 'freeform': {
                answerRecord.set('value_text', value);
                break;
            }

            case 'date': {
                answerRecord.set('value_date', value);
                break;
            }

            case 'time': {
                answerRecord.set('value_time', value);
                break;
            }
            case 'lookup': {
                answerRecord.set('value_text', value);
                break;
            }
            case 'count': {
                answerRecord.set('value_integer', value);
                break;
            }
            case 'measurement': {
                answerRecord.set('value_number', value);
                answerRecord.set('unit', unit);
                answerRecord.set('unit_type', unitType);
                break;
            }
            case 'document': {
                break;
            }
            case 'action': {
                break;
            }
        }

        answerRecord.set('date_recorded', new Date());
        answerRecord.set('time_recorded', new Date());
        answerRecord.set('deleted', 0);
        answerRecord.setChangedOnMobile();

        return answerRecord;
    },

    getAnswerFiltersForDocumentQuestions: function (questionnaireRecord, questionRecord, answerRecordIds) {
        var filterArray = this.getAnswerFilters(questionnaireRecord, questionRecord, answerRecordIds),
            isDocFilter = Ext.create('Common.util.Filter', {
                property: 'doc',
                matchIsNullValue: true,
                isEqual: false,
                conjunction: 'AND'
            });

        filterArray.push(isDocFilter);

        return filterArray;
    },

    getAnswerFilters: function (questionnaireRecord, questionRecord, answerRecordIds) {
        var filterArray = [],
            subFilterArray = [],
            filter = Ext.create('Common.util.Filter', {
                property: 'dummyProperty',
                value: 'dummyValue'
            }),
            i;

        filterArray.push(Ext.create('Common.util.Filter', {
            property: 'questionnaire_id',
            value: questionnaireRecord.get('questionnaire_id_ext'),
            exactMatch: true,
            conjunction: 'AND'
        }));
        filterArray.push(Ext.create('Common.util.Filter', {
            property: 'survey_event_id',
            value: questionnaireRecord.get('activity_log_id'),
            exactMatch: true,
            conjunction: 'AND'
        }));

        if (!Ext.isEmpty(questionRecord)) {
            filterArray.push(Ext.create('Common.util.Filter', {
                property: 'question_id',
                value: questionRecord.get('question_id'),
                exactMatch: true,
                conjunction: 'AND'
            }));
        }

        if (!Ext.isEmpty(answerRecordIds)) {
            for (i = 0; i < answerRecordIds.length; i++) {
                subFilterArray.push({
                    property: 'id',
                    value: answerRecordIds[i],
                    conjunction: 'OR'
                });
            }
        }

        if (subFilterArray.length > 0) {
            filter.setSubFilter(subFilterArray);
            filterArray.push(filter);
        }

        return filterArray;
    },

    getAnswerFiltersForRecords: function (questionnaireRecord, questionRecordIds) {
        var filterArray = [],
            subFilterArray = [],
            filter = Ext.create('Common.util.Filter', {
                property: 'dummyProperty',
                value: 'dummyValue'
            }),
            i;

        filterArray.push(Ext.create('Common.util.Filter', {
            property: 'questionnaire_id',
            value: questionnaireRecord.get('questionnaire_id_ext'),
            exactMatch: true,
            conjunction: 'AND'
        }));

        filterArray.push(Ext.create('Common.util.Filter', {
            property: 'survey_event_id',
            value: questionnaireRecord.get('activity_log_id'),
            exactMatch: true,
            conjunction: 'AND'
        }));

        for (i = 0; i < questionRecordIds.length; i++) {
            subFilterArray.push({
                property: 'question_id',
                value: questionRecordIds[i],
                conjunction: 'OR'
            });
        }

        if (subFilterArray.length > 0) {
            filter.setSubFilter(subFilterArray);
            filterArray.push(filter);
        }

        return filterArray;
    },

    deleteAnswersForField: function (field, questionnaireView) {
        var me = this,
            store = Ext.getStore('answersStore'),
            //the question record is set on the panel containing the checkbox and the question field
            questionRecord = field.getParent().getRecord(),
            numMaxResponses = questionRecord.get('num_responses_max'),
            answerRecordId = field.answerRecordId,
            filterArray,
            previousAnswerCount = me.countMultipleFieldAnswers(questionnaireView, field);

        filterArray = me.getAnswerFilters(questionnaireView.getRecord(), questionRecord, [answerRecordId]);

        store.clearFilter();
        store.setFilters(filterArray);
        store.loadPage(1, function (answerRecords) {
            var newRecords = [];
            if (!Ext.isEmpty(answerRecords)) {
                answerRecords.forEach(function (answerRecord) {
                    var answerId = answerRecord.get('answer_id');
                    if (!Ext.isEmpty(answerId) && answerId > 0) {
                        answerRecord.set('deleted', '1');
                    } else {
                        newRecords.push(answerRecord);
                    }
                });

                store.remove(newRecords);
                store.sync(function () {
                    if (Ext.isEmpty(field.answerId) || field.answerId === 0) {
                        field.answerRecordId = null;
                    }
                    field.isAnswerDeleted = 1;

                    if (me.isParentQuestion(questionRecord) && (numMaxResponses <= 1 || previousAnswerCount === 1)) {
                        questionnaireView.numberOfAnsweredParentQuestions--;
                        me.updatePercentageOfCompletion(questionnaireView)
                            .then(function () {
                                if (!questionnaireView.isViewLoading) {
                                    me.resetSurveyStatusIfCompleted(questionnaireView.getRecord());
                                }
                            });
                    }
                });
            }
        });
    },

    deleteAnswersForRecords: function (questionRecords, questionnaireRecord) {
        var me = this,
            store = Ext.getStore('answersStore'),
            filterArray;

        return new Promise(function (resolve) {
            if (questionRecords.length === 0) {
                resolve();
            } else {
                filterArray = me.getAnswerFiltersForRecords(questionnaireRecord,
                    Ext.Array.pluck(Ext.Array.pluck(questionRecords, 'data'), 'question_id'));

                store.clearFilter();
                store.setFilters(filterArray);
                store.loadPage(1, function (answerRecords) {
                    var newRecords = [];
                    if (Ext.isEmpty(answerRecords)) {
                        resolve();
                    } else {
                        answerRecords.forEach(function (answerRecord) {
                            var answerId = answerRecord.get('answer_id');
                            if (!Ext.isEmpty(answerId) && answerId > 0) {
                                answerRecord.set('deleted', '1');
                            } else {
                                newRecords.push(answerRecord);
                            }
                        });

                        store.remove(newRecords);
                        store.sync(function () {
                            me.questionRecordsToBeDeleted = [];
                            resolve();
                        });
                    }
                });
            }
        });
    },

    loadAnswers: function (questionPanels, questionnaireView) {
        var me = this,
            questionRecord,
            questionField;

        var p = Promise.resolve();
        questionPanels.forEach(function (questionPanel) {
            p = p.then(function () {
                questionRecord = questionPanel.record;
                if (questionRecord.get('question_type') !== 'label') {
                    questionField = questionPanel.items[1];
                    if (!Ext.isEmpty(questionField)) {
                        return me.setAnswer(questionnaireView, questionField, questionRecord);
                    }
                }
                return Promise.resolve();
            });
        });
        return p;
    },

    setAnswer: function (questionnaireView, questionField, questionRecord) {
        var me = this,
            type = questionRecord.get('question_type'),
            numMaxResponses = questionRecord.get('num_responses_max'),
            setAnswerFn = me.setAnswerForField;

        return new Promise(function (resolve) {
            if (numMaxResponses > 1 && questionField.xtype === 'multiplefield') {
                setAnswerFn = me.setMultiRecordAnswerForField;
            }

            switch (type) {
                case 'multiple_choice_single': {
                    me.setAnswerForMultiChoiceField(questionField, questionnaireView, questionRecord, resolve);
                    break;
                }
                case 'multiple_choice_multi': {
                    me.setAnswerForMultiChoiceField(questionField, questionnaireView, questionRecord, resolve);
                    break;
                }
                case 'freeform': {
                    me.setAnswerForField(questionField, questionnaireView, questionRecord, 'value_text', resolve);
                    break;
                }
                case 'date': {
                    setAnswerFn.call(me, questionField, questionnaireView, questionRecord, 'value_date', resolve);
                    break;
                }
                case 'time': {
                    setAnswerFn.call(me, questionField, questionnaireView, questionRecord, 'value_time', resolve);
                    break;
                }
                case 'lookup': {
                    setAnswerFn.call(me, questionField, questionnaireView, questionRecord, 'value_text', resolve);
                    break;
                }
                case 'count': {
                    setAnswerFn.call(me, questionField, questionnaireView, questionRecord, 'value_integer', resolve);
                    break;
                }
                case 'measurement': {
                    setAnswerFn.call(me, questionField, questionnaireView, questionRecord, 'value_number', resolve);
                    break;
                }
                case 'document': {
                    me.setAnswerForDocumentField(questionField, questionnaireView, questionRecord, resolve);
                    break;
                }
                case 'action': {
                    me.setAnswerForActionField(questionField, questionnaireView, questionRecord, resolve);
                    break;
                }
                default:
                    resolve();
            }
        });

    },

    setAnswerForActionField: function (field, questionnaireView, questionRecord, callbackFn) {
        var me = this,
            store = Ext.getStore('answersStore'),
            questionnaireRecord = questionnaireView.getRecord(),
            filterArray,
            fieldObject;

        filterArray = me.getAnswerFilters(questionnaireRecord, questionRecord);
        store.clearFilter();
        store.setFilters(filterArray);
        store.loadPage(1, function (answerRecords) {
            var answerRecord = !Ext.isEmpty(answerRecords) ? answerRecords[0] : null;
            if (Ext.isEmpty(answerRecord)) {
                fieldObject = questionnaireView.down('*[name=' + field.name + ']');
                // add answer record
                me.onFieldChange(questionnaireView, fieldObject, function () {
                    me.setAnswerForField(field, questionnaireView, questionRecord, null, callbackFn);
                }, me);
            } else if(answerRecord.get('deleted') === 1) {
                // undelete the answer
                me.setAnswerRecord(answerRecord, questionnaireRecord, questionRecord, null);
                store.sync(function () {
                    me.setAnswerForField(field, questionnaireView, questionRecord, null, callbackFn);
                });
            } else {
                me.setAnswerForField(field, questionnaireView, questionRecord, null, callbackFn);
            }
        });
    },

    setAnswerForDocumentField: function (field, questionnaireView, questionRecord, callbackFn) {
        var me = this,
            store = Ext.getStore('answersStore'),
            questionnaireRecord = questionnaireView.getRecord(),
            filterArray,
            notDeletedDocCount;

        return new Promise(function (resolve) {
            filterArray = me.getAnswerFiltersForDocumentQuestions(questionnaireRecord, questionRecord);

            store.clearFilter();
            store.setFilters(filterArray);
            store.loadPage(1, function (answerRecords) {
                notDeletedDocCount = me.refreshQuestionnaireViewDocField(questionnaireView, questionRecord, answerRecords);
                resolve();
                Ext.callback(callbackFn, me);
            });
        });
    },

    setAnswerForMultiChoiceField: function (field, questionnaireView, questionRecord, callbackFn) {
        var me = this,
            store = Ext.getStore('answersStore'),
            questionnaireRecord = questionnaireView.getRecord(),
            filterArray,
            answerValue,
            optionValues = [],
            fieldObject;

        return new Promise(function (resolve) {
            filterArray = me.getAnswerFilters(questionnaireRecord, questionRecord);

            store.retrieveSingleRecord(filterArray)
                .then(function (answerRecord) {
                    if (!Ext.isEmpty(answerRecord)) {
                        fieldObject = questionnaireView.down('*[name=' + field.name + ']');
                        fieldObject.answerId = answerRecord.get('answer_id');
                        fieldObject.answerRecordId = answerRecord.get('id');
                        fieldObject.isAnswerDeleted = answerRecord.get('deleted');
                        if (fieldObject.isAnswerDeleted !== 1) {
                            answerValue = answerRecord.get('value_text');
                            optionValues = !Ext.isEmpty(answerValue) ? answerValue.split(me.MULTIPLE_VALUES_SEPARATOR) : [];
                            fieldObject.setValue(optionValues);
                        }
                        if (me.isParentQuestion(questionRecord) && fieldObject.isAnswerDeleted !== 1) {
                            questionnaireView.numberOfAnsweredParentQuestions++;
                        }
                    }
                    resolve();
                    Ext.callback(callbackFn, me);
                });
        });

    },

    setAnswerForField: function (field, questionnaireView, questionRecord, answerField, callbackFn) {
        var me = this,
            store = Ext.getStore('answersStore'),
            questionnaireRecord = questionnaireView.getRecord(),
            filterArray,
            fieldObject;

        return new Promise(function (resolve) {
            filterArray = me.getAnswerFilters(questionnaireRecord, questionRecord);

            store.retrieveSingleRecord(filterArray)
                .then(function (answerRecord) {
                    if (!Ext.isEmpty(answerRecord)) {
                        fieldObject = questionnaireView.down('*[name=' + field.name + ']');
                        fieldObject.answerId = answerRecord.get('answer_id');
                        fieldObject.answerRecordId = answerRecord.get('id');
                        fieldObject.isAnswerDeleted = answerRecord.get('deleted');
                        if (fieldObject.isAnswerDeleted !== 1 && !Ext.isEmpty(answerField)) {
                            fieldObject.setValue(answerRecord.get(answerField));
                        }
                        if (me.isParentQuestion(questionRecord) && fieldObject.isAnswerDeleted !== 1) {
                            questionnaireView.numberOfAnsweredParentQuestions++;
                        }

                    }
                    resolve();
                    Ext.callback(callbackFn, me);
                });
        });

    },

    setMultiRecordAnswerForField: function (multiplefield, questionnaireView, questionRecord, answerField, callbackFn) {
        var me = this,
            store = Ext.getStore('answersStore'),
            questionnaireRecord = questionnaireView.getRecord(),
            filterArray,
            i,
            fields,
            numberOfFieldsToAdd,
            numberOfAnswers,
            answerRecord,
            multipleFieldObject;

        return new Promise(function (resolve) {
            filterArray = me.getAnswerFilters(questionnaireRecord, questionRecord);

            store.retrieveAllRecords(filterArray)
                .then(function (answerRecords) {
                    if (!Ext.isEmpty(answerRecords)) {
                        // find the fields and add if necessary
                        fields = questionnaireView.query('*[name=' + multiplefield.fieldConfig.name + ']');
                        if (fields.length < answerRecords.length) {
                            multipleFieldObject = questionnaireView.down('*[name=' + multiplefield.name + ']');
                            numberOfFieldsToAdd = answerRecords.length - fields.length;
                            for (i = 0; i < numberOfFieldsToAdd; i++) {
                                multipleFieldObject.addField();
                            }
                        }

                        // set the answers
                        fields = questionnaireView.query('*[name=' + multiplefield.fieldConfig.name + ']');
                        numberOfAnswers = 0;
                        for (i = 0; i < Math.min(answerRecords.length, fields.length); i++) {
                            answerRecord = answerRecords[i];
                            fields[i].answerId = answerRecord.get('answer_id');
                            fields[i].answerRecordId = answerRecord.get('id');
                            fields[i].isAnswerDeleted = answerRecord.get('deleted');
                            if (fields[i].isAnswerDeleted !== 1) {
                                fields[i].setValue(answerRecord.get(answerField));
                                numberOfAnswers++;
                            }
                        }
                        if (me.isParentQuestion(questionRecord) && numberOfAnswers > 0) {
                            questionnaireView.numberOfAnsweredParentQuestions++;
                        }
                    }
                    resolve();
                    Ext.callback(callbackFn, me);
                });
        });
    },

    createLookupStore: function (tableName, fieldName) {
        var store,
            storeName = 'lookup_' + tableName + '_' + fieldName + '_store',
            modelName = 'lookup_' + tableName + '_' + fieldName;

        if (!Ext.isEmpty(tableName) && !Ext.isEmpty(fieldName)) {
            Ext.define(modelName, {
                extend: 'Common.data.Model',
                config: {
                    uniqueIdentifier: [fieldName],
                    fields: [
                        {name: 'id', type: 'int'},
                        {name: fieldName, type: 'string'}
                    ]
                }
            });

            Ext.define(storeName, {
                extend: 'Common.store.sync.ValidatingTableStore',
                config: {
                    model: modelName,
                    storeId: storeName,
                    remoteSort: true,
                    remoteFilter: true,
                    enableAutoLoad: true,
                    tableDisplayName: LocaleManager.getLocalizedString('Lookup Data',
                        'ComplianceSurvey.util.Questionnaire'),
                    proxy: {
                        type: 'Sqlite'
                    },
                    timestampDownload: false
                },
                serverTableName: tableName,
                serverFieldNames: [fieldName],
                inventoryKeyNames: [fieldName]

            });

            store = Ext.create(storeName);
        }
        return store;
    },

    // Verify that all required parent and child questions have answers
    isValidForm: function (questionnaireView) {
        var me = this,
            questionsStore = Ext.getStore('questQuestionsStore'), //parent questions store
            filter = Ext.create('Common.util.Filter', {
                property: 'questionnaire_id',
                value: questionnaireView.questionnaireId,
                exactMatch: true
            }),
            parentQuestionIds = [],
            tempParentQuestionIds = [],
            tempChildQuestionIds = [],
            requiredQuestionIds = [],
            questionId,
            i,
            needToSearchForChild = true;

        // get all parent questions and check for answers for required ones
        // search the followUpMap for direct children, check they are required and have answers
        // search the followUpMap for children of follow-up question if they are required and have answers

        return questionsStore.retrieveAllRecords([filter])
            .then(function (questionRecords) {
                //get the list of parent questions and required parent questions
                for (i = 0; i < questionRecords.length; i++) {
                    questionId = questionRecords[i].get('question_id');
                    parentQuestionIds.push(questionId);
                    if (questionRecords[i].get('is_required')) {
                        requiredQuestionIds.push(questionId);
                    }
                }

                //add the list of displayed required child questions to requiredQuestionIds
                tempParentQuestionIds = parentQuestionIds.slice();
                while (needToSearchForChild) {
                    needToSearchForChild = false;
                    tempChildQuestionIds = [];
                    for (i = 0; i < questionnaireView.followUpMap.length; i++) {
                        if (Ext.Array.contains(tempParentQuestionIds, questionnaireView.followUpMap[i].parent_id)) {
                            questionId = questionnaireView.followUpMap[i].child_id;
                            tempChildQuestionIds.push(questionId);
                            if (questionnaireView.followUpMap[i].is_required) {
                                requiredQuestionIds.push(questionId);
                            }
                            needToSearchForChild = true;
                        }
                    }
                    tempParentQuestionIds = tempChildQuestionIds;
                }

                // if there are no required questions then the form is valid
                if (requiredQuestionIds.length === 0) {
                    return new Promise(function (resolve) {
                        resolve(true);
                    });
                } else {
                    return me.getAnswersForRequiredQuestions(requiredQuestionIds, questionnaireView)
                        .then(function (answerRecords) {
                            return new Promise(function (resolve) {
                                if (!Ext.isEmpty(answerRecords) && answerRecords.length >= requiredQuestionIds.length) {
                                    resolve(true);
                                } else {
                                    resolve(false);
                                }
                            });
                        });


                }
            });
    },


    getAnswersForRequiredQuestions: function (requiredQuestionIds, questionnaireView) {
        var me = this,
            filterArray,
            answersStore = Ext.getStore('answersStore');

        filterArray = me.getAnswerFiltersForRecords(questionnaireView.getRecord(), requiredQuestionIds);

        // add restriction on not deleted answers
        filterArray.push(Ext.create('Common.util.Filter', {
            property: 'deleted',
            value: 0
        }));

        // add restriction on not deleted documents
        filterArray.push(Ext.create('Common.util.Filter', {
            property: 'doc_contents',
            value: Common.config.GlobalParameters.MARK_DELETED_TEXT,
            operator: '<>'
        }));

        return answersStore.retrieveAllRecords(filterArray);
    },

    updatePercentageOfCompletion: function (questionnaireView) {
        var me = this,
            responses = questionnaireView.numberOfAnsweredParentQuestions,
            total = questionnaireView.numberOfParentQuestions,
            questionnaireRecord = questionnaireView.getRecord(),
            percentage = total !== 0 ? Math.floor(responses * 100 / total) : 0,
            titlePanel = questionnaireView.down('titlepanel'),
            initialTitle = questionnaireView.initialTitle;

        titlePanel.setTitle(initialTitle + '<span name="percentage">&nbsp;(&nbsp;' + percentage + '%&nbsp;)</span>');

        return me.updateSurveyPercentage(questionnaireRecord, percentage);

    },

    updateSurveyPercentage: function (questionnaireRecord, percentage) {
        var store = Ext.getStore('complianceSurveysStore'),
            viewStore = Ext.getStore('complianceSurveyTasksStore'),
            filterArray = [];

        return new Promise(function (resolve) {
            filterArray.push(Ext.create('Common.util.Filter', {
                property: 'activity_log_id',
                value: questionnaireRecord.get('activity_log_id'),
                exactMatch: true
            }));

            store.clearFilter();
            store.setFilters(filterArray);
            store.load(function (surveyRecords) {
                if (!Ext.isEmpty(surveyRecords)) {
                    surveyRecords[0].set('pct_complete', percentage);
                    if (percentage > 0 && surveyRecords[0].get('status') === 'SCHEDULED') {
                        surveyRecords[0].set('status', 'IN PROGRESS');
                    } else if (percentage <= 0 && surveyRecords[0].get('status') === 'IN PROGRESS') {
                        surveyRecords[0].set('status', 'SCHEDULED');
                    }
                    store.sync(function () {
                        store.load(function () {
                            viewStore.loadPage(1, function () {
                                resolve();
                            });
                        });
                    });

                }
            });
        });
    },

    refreshQuestionnaireViewDocFields: function (questionnaireView, questionFields) {
        var me = this;

        Ext.Array.each(questionFields, function (field) {
            me.refreshQuestionnaireViewDocField(questionnaireView, field.getRecord());
        });
    },

    refreshQuestionnaireViewDocField: function (questionnaireView, questionRecord, answerRecords) {
        var me = this,
            questionField,
            answersStore = Ext.getStore('answersStore'),
            newCount = answersStore.getNotDeletedDocumentsCount(),
            documentButton,
            oldCount,
            answerData;

        questionField = questionnaireView.down('*[name=' +
            QuestionnaireUtil.getQuestionFieldName(questionnaireView.questionnaireId,
                questionRecord.get('question_id')) + ']');
        documentButton = questionField.down('viewselector');

        oldCount = documentButton.getRecord() ? documentButton.getRecord().getDocumentCount() : 0;

        questionRecord.setDocumentCount(newCount);
        documentButton.setRecord(questionRecord);

        questionField.answerIds = null;
        questionField.answerRecordIds = null;
        if (!Ext.isEmpty(answerRecords)) {
            answerData = Ext.Array.pluck(answerRecords, 'data');
            questionField.answerIds = Ext.Array.pluck(answerData, 'answer_id');
            questionField.answerRecordIds = Ext.Array.pluck(answerData, 'id');
        }

        if (oldCount !== newCount && me.isParentQuestion(questionRecord)) {
            if (oldCount > 0 && newCount === 0) {
                questionnaireView.numberOfAnsweredParentQuestions--;
            } else if (oldCount === 0 && newCount > 0) {
                questionnaireView.numberOfAnsweredParentQuestions++;
            }
        }
    },

    deleteAsnwersForDocumentQuestionFields: function (questionFields, questionnaireView) {
        var me = this;

        if (Ext.isEmpty(questionFields)) {
            return Promise.resolve();
        } else {
            return me.deleteAnswersForNewDocuments(questionFields, questionnaireView)
                .then(function () {
                    return me.deleteAnswersForDocuments(questionFields, questionnaireView);
                })
                .then(function () {
                    me.refreshQuestionnaireViewDocFields(questionnaireView, questionFields);
                    return Promise.resolve();
                });
        }
    },

    deleteAnswersForNewDocuments: function (questionFields, questionnaireView) {
        var store = Ext.getStore('answersStore'),
            answerRecordIds,
            newDocsFilter = Ext.create('Common.util.Filter', {
                property: 'doc_isnew',
                value: true,
                conjunction: 'AND'
            }),
            filterArray,
            newDocsFilterArray;

        if (Ext.isEmpty(questionFields)) {
            return Promise.resolve();
        } else {
            answerRecordIds = Ext.Array.flatten(Ext.Array.pluck(questionFields, 'answerRecordIds'));
            filterArray = QuestionnaireUtil.getAnswerFiltersForDocumentQuestions(questionnaireView.getRecord(), null,
                answerRecordIds);
            newDocsFilterArray = filterArray.concat([newDocsFilter]);

            return new Promise(function (resolve) {
                store.clearFilter();
                store.setFilters(newDocsFilterArray);
                store.loadPage(1, function (answerRecords) {
                    store.remove(answerRecords);
                    store.sync(resolve);
                });
            });
        }
    },

    deleteAnswersForDocuments: function (questionFields, questionnaireView) {
        var store = Ext.getStore('answersStore'),
            answerRecordIds,
            filterArray,
            docFieldName = 'doc';

        if (Ext.isEmpty(questionFields)) {
            return Promise.resolve();
        } else {
            answerRecordIds = Ext.Array.flatten(Ext.Array.pluck(questionFields, 'answerRecordIds'));
            filterArray = QuestionnaireUtil.getAnswerFiltersForDocumentQuestions(questionnaireView.getRecord(), null,
                answerRecordIds);

            store.clearFilter();
            store.setFilters(filterArray);
            store.loadPage(1, function (answerRecords) {
                var p = Promise.resolve();
                answerRecords.forEach(function (record) {
                    p = p.then(function () {
                        return DocumentManager.deleteDocumentFile(store, record, docFieldName)
                            .then(function () {
                                return DocumentManager.markDocumentDeleted(store, record, docFieldName);
                            })
                            .then(null, function (error) {
                                Log.log(error, 'error');
                                Ext.Msg.alert('', error);
                            });
                    });
                    return p;
                });
                return p;
            });
        }
    },


    /**
     * If the survey is Completed (status = 'COMPLETED'), resets it to its initial status.
     *
     * @param questionnaireRecord
     */
    resetSurveyStatusIfCompleted: function (questionnaireRecord) {
        var me = this,
            statusChangedMsg = LocaleManager.getLocalizedString('Changing the answers to a completed survey reverted the survey to its previous status. You can set the survey back to Completed after making the changes.',
                'ComplianceSurvey.util.Questionnaire');

        if (questionnaireRecord.get('status') === 'COMPLETED') {
            return new Promise(function (resolve, reject) {
                return me.changeSurveyStatus(questionnaireRecord, questionnaireRecord.get('status_initial'))
                    .then(function () {
                        Ext.Msg.alert('', statusChangedMsg, function () {
                            resolve();
                        });
                    })
                    .then(null, function () {
                        reject();
                    });
            });
        }

        return Promise.resolve();
    },

    changeSurveyStatus: function (surveyRecord, newStatus) {
        var complianceSurveyTasksStore = Ext.getStore('complianceSurveyTasksStore'),
            complianceSurveysStore = Ext.getStore('complianceSurveysStore'),
            filter = Ext.create('Common.util.Filter', {
                property: 'activity_log_id',
                value: surveyRecord.get('activity_log_id')
            }),
            errorTitle = LocaleManager.getLocalizedString('Error', 'ComplianceSurvey.util.Questionnaire'),
            recordNotFoundErrorMsg = LocaleManager.getLocalizedString('Survey record not found',
                'ComplianceSurvey.util.Questionnaire');

        complianceSurveysStore.clearFilter();
        complianceSurveysStore.setFilters([filter]);

        return new Promise(function (resolve, reject) {
            complianceSurveysStore.loadPage(1, function (records) {
                if (Ext.isArray(records) && records.length >= 1) {
                    records[0].set('status', newStatus);
                    surveyRecord.set('status', newStatus);
                    complianceSurveysStore.sync(function () {
                        complianceSurveyTasksStore.loadPage(1, function () {
                            resolve();
                        });
                    });
                } else {
                    Ext.Msg.alert(errorTitle, recordNotFoundErrorMsg, function () {
                        reject();
                    });
                }
            });
        });
    }
});