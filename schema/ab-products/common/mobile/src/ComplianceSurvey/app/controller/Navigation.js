Ext.define('ComplianceSurvey.controller.Navigation', {
    extend: 'Common.controller.NavigationController',

    config: {
        refs: {
            mainView: 'main',
            surveySearchField: 'surveysList search[name=searchSurvey]',
            surveySortField: 'surveysList selectfield[name=surveySortField]',
            filterButton: 'button[action=filterSurveysList]',
            filterPanel: 'filterPanel',
            surveyInfoBtn: 'button[itemId=infoBtn]',
            surveyLocateBtn: 'button[itemId=locateBtn]',
            barcodeScanBtn: 'button[action=barcodeFilter]',
            scanEqField: 'barcodefield[name=scan_eq]',
            scanEqView: 'panel[name=scanEqView]',
            mapBtn: 'toolbarbutton[action=mapSurveys]',
            closeSurveysMapBtn: 'button[action=closeSurveysMap]',
            surveyLocationSegmentedBtn: 'surveyLocation segmentedbutton',
            siteMapPanel: 'siteMapPanel',
            floorPlanPanel: 'floorPlanPanel',
            selectAllCheckbox: 'questionsList checkboxfield[name=selectAllQuestions]',
            questionsList: 'questionsList',
            resetQuestionsBtn: 'button[action=resetQuestions]',
            navigationOptionsBtn: 'buttonpicker[action=showNavOptions]'
        },
        control: {
            homeButton: {
                tap: 'onGoToHomePage'
            },
            mainView: {
                push: 'onViewPush',
                pop: 'onViewPopped'
            },
            surveySearchField: {
                searchkeyup: 'onSearchSurveyList',
                searchclearicontap: 'onClearSurveySearch'
            },
            surveySortField: {
                change: 'onApplySurveySort'
            },
            filterButton: {
                tap: 'displayFilterView'
            },
            filterPanel: {
                clearFilter: 'clearFilter',
                applyFilter: 'applyFilter'
            },
            surveyInfoBtn: {
                tap: 'showSurveyInfo'
            },
            surveyLocateBtn: {
                tap: 'locateSurvey'
            },
            barcodeScanBtn: {
                tap: 'scanEquipment'
            },
            scanEqField: {
                keyup: function (field) {
                    this.onFilterByEq(field.getValue());
                },
                clearicontap: 'onClearSurveySearch',
                scancomplete: function (scanResult) {
                    var me = this;
                    me.onFilterByEq(scanResult.code, function () {
                        if (!Ext.isEmpty(me.getScanEqView())) {
                            me.getScanEqView().hide();
                        }
                    }, me);
                }
            },
            mapBtn: {
                tap: 'showSurveysMap'
            },
            closeSurveysMapBtn: {
                tap: 'closeSurveysMap'
            },
            'surveysList': {
                itemsingletap: function (list, index, target, record) {
                    var questionsList;

                    if (this.getDisableListTapEvent()) {
                        this.setDisableListTapEvent(false);
                        return;
                    }

                    questionsList = Ext.create('ComplianceSurvey.view.QuestionsList', {record: record});
                    this.getMainView().push(questionsList);
                }
            },
            surveyLocationSegmentedBtn: {
                toggle: 'onSurveyLocationSegmentedButtonToggle'
            },
            selectAllCheckbox: {
                check: 'selectAllQuestions',
                uncheck: 'deselectAllQuestions'
            },
            resetQuestionsBtn: {
                tap: 'resetQuestions'
            },
            navigationOptionsBtn: {
                itemselected: 'onNavigationOptionSelected'
            },
            'button[action=completeSurvey]': {
                tap: 'onCompleteSurvey'
            }
        },

        disableListTapEvent: false,

        confirmDeleteMessage: LocaleManager.getLocalizedString('Please select at least one answer to delete',
            'ComplianceSurvey.controller.Navigation'),

        deleteMessage: LocaleManager.getLocalizedString('Do you want to delete the selected answers?',
            'ComplianceSurvey.controller.Navigation')
    },

    /**
     * Load surveys store on launch to ensure list display with Load More... text and latest sort filter is applied.
     */
    launch: function () {
        var surveySortField = this.getSurveySortField();
        this.onApplySurveySort(surveySortField, surveySortField.getValue());
    },

    /**
     * Handle tap on Apps button.
     */
    onGoToHomePage: function () {
        this.getMainView().reset();
    },

    onViewPopped: function () {
        var me = this,
            navigationBar = this.getMainView().getNavigationBar(),
            viewStack = navigationBar.getViewStack(),
            currentView;

        if (viewStack && viewStack.length > 0) {
            currentView = viewStack[viewStack.length - 1];
            if (currentView.xtype === 'surveysMap') {
                navigationBar.hide();
                me.refreshBuildingsStore()
                    .then(function () {
                        currentView.showMarkers();
                    }, me);

            } else {
                navigationBar.show();
            }
        }

        me.displayToolbarButtons(navigationBar.getCurrentView());
    },

    onViewPush: function (navView, view) {
        var me = this,
            navigationBar = me.getMainView().getNavigationBar();

        if (view.xtype === 'surveysMap') {
            navigationBar.hide();
            me.refreshBuildingsStore()
                .then(function () {
                        view.showMarkers();
                    }
                );
        } else {
            navigationBar.show();
        }

        me.displayToolbarButtons(view);
    },

    displayToolbarButtons: function (currentView) {
        var me = this,
            navigationBar = me.getMainView().getNavigationBar(),
            resetQuestionsBtn = navigationBar.down('button[action=resetQuestions]'),
            showNavOptionsBtn = navigationBar.down('button[action=showNavOptions]'),
            completeSurveyBtn = navigationBar.down('button[action=completeSurvey]'),
            syncSurveyBtn = navigationBar.down('button[action=syncSurvey]'),
            barcodeFilterBtn = navigationBar.down('button[action=barcodeFilter]');

        resetQuestionsBtn.setHidden(currentView.xtype !== 'questionsList');
        showNavOptionsBtn.setHidden(currentView.xtype !== 'questionsList');
        completeSurveyBtn.setHidden(currentView.xtype !== 'questionsList');
        syncSurveyBtn.setHidden(currentView.xtype !== 'questionsList' && currentView.xtype !== 'main');
        if (currentView.xtype === 'questionsList') {
            syncSurveyBtn.removeCls('ab-icon-action');
        } else {
            syncSurveyBtn.addCls('ab-icon-action');
        }
        barcodeFilterBtn.setHidden(currentView.xtype !== 'main');
    },

    /**
     * Filter the list of surveys by the value typed in the search field.
     * @param searchFieldValue
     */
    onSearchSurveyList: function (searchFieldValue) {
        // Filter the Survey table
        var surveysStore = Ext.getStore('complianceSurveyTasksStore'),
            searchFields = ['action_title', 'description', 'pct_complete', 'site_id', 'bl_id', 'fl_id', 'rm_id'],
            filters = this.buildSearchFilters(searchFields, searchFieldValue);

        surveysStore.clearFilter();
        surveysStore.setFilters(filters);
        surveysStore.loadPage(1);
    },

    /**
     * Remove all filters when the clear icon is tapped in the search field in surveys list view.
     */
    onClearSurveySearch: function () {
        var surveysStore = Ext.getStore('complianceSurveyTasksStore');

        surveysStore.clearFilter();
        surveysStore.loadPage(1);
    },

    buildSearchFilters: function (searchFields, searchValue) {
        var filterArray = [];

        Ext.each(searchFields, function (field) {
            var filter = Ext.create('Common.util.Filter', {
                property: field,
                value: searchValue,
                conjunction: 'OR',
                anyMatch: true
            });
            filterArray.push(filter);
        });

        return filterArray;
    },

    /**
     * Sort the surveys lists by the selected value in the sort field.
     * @param field sort field
     * @param value selected value for sort
     */
    onApplySurveySort: function (field, value) {
        var surveysStore = Ext.getStore('complianceSurveyTasksStore'),
            datePerformSort = [
                {
                    property: 'date_scheduled',
                    direction: 'ASC'
                }
            ],
            datePerformEndSort = [
                {
                    property: 'date_scheduled_end',
                    direction: 'ASC'
                }
            ],
            dateRequiredSort = [
                {
                    property: 'date_required',
                    direction: 'ASC'
                }
            ],
            surveyTitleSort = [
                {
                    property: 'action_title',
                    direction: 'ASC'
                }
            ],
            statusSort = [
                {
                    property: SurveyUtil.surveySortByCalculatedStatus,
                    direction: 'ASC'
                }
            ],
            locationSort = [
                {
                    property: 'site_id',
                    direction: 'ASC'
                },
                {
                    property: 'bl_id',
                    direction: 'ASC'
                },
                {
                    property: 'fl_id',
                    direction: 'ASC'
                },
                {
                    property: 'rm_id',
                    direction: 'ASC'
                }
            ]
            ;

        switch (value) {
            case 'date_perform': {
                surveysStore.setSorters(datePerformSort);
                break;
            }
            case 'date_perform_end': {
                surveysStore.setSorters(datePerformEndSort);
                break;
            }
            case 'date_required': {
                surveysStore.setSorters(dateRequiredSort);
                break;
            }
            case 'survey_title': {
                surveysStore.setSorters(surveyTitleSort);
                break;
            }
            case 'status': {
                surveysStore.setSorters(statusSort);
                break;
            }
            case 'location': {
                surveysStore.setSorters(locationSort);
                break;
            }
        }

        surveysStore.loadPage(1);
    },

    /**
     * Handle tap on the filter icon and display the Filter view.
     */
    displayFilterView: function () {
        var view = Ext.create('ComplianceSurvey.view.Filter');

        this.getMainView().push(view);
    },

    /**
     * Handle tap on the Clear button in the Filter view.
     */
    clearFilter: function () {
        var me = this,
            store = Ext.getStore('complianceSurveyTasksStore');

        store.clearFilter();
        store.loadPage(1, function () {
            me.loadPromptFieldStores();
        });
    },

    /**
     * Handle tap on the Filter button in the Filter view.
     */
    applyFilter: function () {
        var me = this,
            filterPanel = me.getFilterPanel(),
            filterValues = filterPanel.getValues(),
            fieldName,
            fieldValue,
            filterArray = [],
            store = Ext.getStore('complianceSurveyTasksStore');

        for (fieldName in filterValues) {
            if (filterValues.hasOwnProperty(fieldName)) {
                fieldValue = filterValues[fieldName];

                me.addFilterForField(filterArray, fieldName, fieldValue);
            }
        }

        store.clearFilter();
        store.setFilters(filterArray);
        store.loadPage(1, function () {
            me.loadPromptFieldStores(function () {
                me.getMainView().pop();
            }, me);
        }, me);
    },

    addFilterForField: function (filterArray, fieldName, fieldValue) {
        var me = this,
            stringFilterFields = ['site_id', 'bl_id', 'fl_id', 'rm_id', 'reg_program', 'project_id', 'regulation', 'eq_std', 'eq_id', 'em_id', 'priority'],
            dateFields = ['date_scheduled', 'date_scheduled_end', 'date_required'],
            statusField = ['status'];

        if (stringFilterFields.indexOf(fieldName) >= 0) {
            if (fieldValue === 'ALL') {
                return;
            }

            if (!Ext.isEmpty(fieldValue)) {
                filterArray.push(me.createFilter(fieldName, fieldValue, 'AND', true));
            }
        } else if (dateFields.indexOf(fieldName) >= 0) {
            switch (fieldName) {
                case 'date_scheduled': {
                    me.addDateFilter(filterArray, fieldName, fieldValue, '>=');
                    me.addDateFilter(filterArray, 'date_scheduled_end', fieldValue, '>=');
                    break;
                }
                case 'date_scheduled_end': {
                    me.addDateFilter(filterArray, fieldName, fieldValue, '<=');
                    me.addDateFilter(filterArray, 'date_scheduled', fieldValue, '<=');
                    break;
                }
                case 'date_required': {
                    me.addDateFilter(filterArray, fieldName, fieldValue, '<=');
                    break;
                }
                default: {
                    me.addDateFilter(filterArray, fieldName, fieldValue);
                }
            }
        } else if (statusField.indexOf(fieldName) >= 0) {
            me.addSurveyCalculatedStatusFilter(filterArray, fieldName, fieldValue);
        }
    },

    /**
     * Creates and returns a {@link Common.util.Filter} filter.
     *
     * @param {String} property The record property to set the filter on
     * @param {String} value The value to filter
     * @param {String} [conjunction='AND'] 'AND' by default
     * @param {Boolean} [exactMatch] true/false/undefined
     * If true or undefined, sets exactMatch property to true and anyMatch property to false;
     * If false, sets exactMatch property to false and anyMatch property to true.
     * @returns {Common.util.Filter}
     */
    createFilter: function (property, value, conjunction, exactMatch) {
        return Ext.create('Common.util.Filter', {
            property: property,
            value: value,
            conjunction: Ext.isDefined(conjunction) ? conjunction : 'AND',
            exactMatch: (!Ext.isDefined(exactMatch) || (Ext.isDefined(exactMatch) && exactMatch === true)),
            anyMatch: (Ext.isDefined(exactMatch) && exactMatch === false)
        });
    },

    /**
     * Adds date filter to the filter array.
     *
     * @param filterArray
     * @param fieldName
     * @param fieldValue
     * @param operator
     */
    addDateFilter: function (filterArray, fieldName, fieldValue, operator) {
        var filterFieldValue,
            filter;

        if (!Ext.isDate(fieldValue) || Ext.isEmpty(fieldValue)) {
            return;
        }

        filterFieldValue = Ext.Date.format(fieldValue, 'Y-m-d H:i:s.u');

        filter = Ext.create('Common.util.Filter', {
            property: fieldName,
            value: filterFieldValue,
            operator: Ext.isDefined(operator) ? operator : '=',
            conjunction: 'AND'
        });

        filterArray.push(filter);
    },

    /**
     * Adds filter on calculated value of the survey status
     *
     * @param filterArray
     * @param fieldName
     * @param fieldValue
     */
    addSurveyCalculatedStatusFilter: function (filterArray, fieldName, fieldValue) {
        if (fieldValue === 'ALL') {
            return;
        }

        filterArray.push(Ext.create('Common.util.SqlFilter', {
            sql: SurveyUtil.getCalculatedStatusFilterQuery(fieldValue)
        }));
    },

    loadPromptFieldStores: function (callbackFn, scope) {
        var me = this,
            storesToLoad = [
                'uniquePrograms',
                'uniqueProjects',
                'uniqueRegulations',
                'uniqueEquipmentStandards',
                'uniqueEquipmentIds',
                'uniqueEmployeeIds',
                'complianceSurveySites',
                'complianceSurveyBuildings',
                'complianceSurveyFloors',
                'complianceSurveyRooms'
            ];

        SyncManager.loadStores(storesToLoad)
            .then(function () {
                Ext.callback(callbackFn, scope || me);
            });
    },

    /**
     * Handles tap on info icon button on a survey list item.
     */
    showSurveyInfo: function (button) {
        this.showSurveyInfoForRecord(button.getRecord());
    },

    showSurveyInfoForRecord: function (record) {
        var me = this,
            store = Ext.getStore('complianceSurveyTasksStore'),
            reportConfig = me.getSurveyTaskInfoConfig(),
            surveyFilter,
            detailReport;

        if (Ext.isEmpty(record)) {
            return;
        }

        // Disable the list tap event to prevent navigation to questions list
        me.setDisableListTapEvent(true);

        detailReport = Ext.create('Common.view.report.Detail', reportConfig);
        detailReport.defaultViewTabletConfig.width = '800px';
        detailReport.init();

        surveyFilter = Ext.create('Common.util.Filter', {
            property: 'activity_log_id',
            value: record.get('activity_log_id'),
            exactMatch: true,
            operator: '='
        });

        store.retrieveRecord([surveyFilter], function (surveyRecord) {
            me.setSurveyCalculatedStatusText(surveyRecord);
            detailReport.setRecord(surveyRecord);
            detailReport.show();
        }, me);
    },

    setSurveyCalculatedStatusText: function (surveyRecord) {
        var surveyCalculatedStatus,
            statusText;

        if (Ext.isEmpty(surveyRecord)) {
            return;
        }

        surveyCalculatedStatus = SurveyUtil.calculateSurveyStatus(surveyRecord);
        statusText = SurveyUtil.getCalculatedStatusText(surveyCalculatedStatus);
        surveyRecord.set('status', statusText);
    },

    getSurveyTaskInfoConfig: function () {
        var config = {},
            tableDef1,
            tableDef2,
            tableDefFields,
            fieldsToDisplay;

        var reportFields = [
            'action_title',
            'status',
            'description',
            'reg_requirement',
            'priority',
            'reg_program',
            'project_id',
            'regulation',
            'eq_std',
            'eq_id',
            'em_id',
            'date_scheduled',
            'date_scheduled_end',
            'date_required'/*,
             This is not downloaded!?! 'date_completed'*/
        ];

        tableDef1 = TableDef.getTableDefWithFieldDefsAsObject('activity_log_sync');//.concat(TableDef.getTableDefWithFieldDefsAsObject('compliance_locations'));
        tableDefFields = Space.view.report.Configuration.getFieldsFromTableDefObject(tableDef1);

        tableDef2 = TableDef.getTableDefWithFieldDefsAsObject('compliance_locations');
        Ext.Array.insert(tableDefFields, 0, Space.view.report.Configuration.getFieldsFromTableDefObject(tableDef2));

        tableDef1.fieldDefs = this.addObjectProperties(tableDef1.fieldDefs, tableDef2.fieldDefs);
        config.tableDef = tableDef1;

        fieldsToDisplay = Ext.Array.intersect(reportFields, tableDefFields);

        config.fieldsToDisplay = fieldsToDisplay;

        config.title = LocaleManager.getLocalizedString('Survey Task Information Report', 'ComplianceSurvey.controller.Navigation');

        return config;
    },

    // Add properties from object2 to object1 and returns object1.
    addObjectProperties: function (object1, object2) {
        var property;

        for (property in object2) {
            object1[property] = object2[property];
        }
        return object1;
    },

    /**
     * Handles tap on locate icon button on a survey list item.
     */
    locateSurvey: function (button) {
        this.locateSurveyForRecord(button.getRecord());
    },

    locateSurveyForRecord: function (record) {
        var me = this,
            editViewClass = 'ComplianceSurvey.view.SurveyLocation',
            updateView,
            floorPlanView,
            siteMapView,
            errorMsg = LocaleManager.getLocalizedString('Unknown location for this survey.', 'ComplianceSurvey.controller.Navigation'),
            siteId = record.get('site_id'),
            blId = record.get('bl_id'),
            flId = record.get('fl_id');

        // Disable the list tap event to prevent navigation to questions list
        me.setDisableListTapEvent(true);

        if (!Ext.isEmpty(siteId) || (!Ext.isEmpty(blId) && !Ext.isEmpty(flId))) {
            updateView = Ext.create(editViewClass, {record: record});
            floorPlanView = updateView.down('floorPlanPanel');
            siteMapView = updateView.down('siteMapPanel');

            updateView.setRecord(record);
            me.getSiteMapData(siteMapView, record, function () {
                me.getFloorPlanData(floorPlanView, record, function () {
                    me.getMainView().push(updateView);
                }, me);
            }, me);
        } else {
            Ext.Msg.alert('', errorMsg);
        }
    },

    getSiteMapData: function (siteMapPanel, record, callbackFn, scope) {
        var me = scope || me,
            siteDrawingStore = Ext.getStore('siteDrawings'),
            svgComponent = siteMapPanel.down('svgcomponent'),
            siteId = record.get('site_id'),

            doProcessSvg = function (svgData) {
                if (svgData !== '') {
                    siteMapPanel.setSvgData(svgData);
                    siteMapPanel.setEventHandlers([
                        {
                            assetType: 'bl',
                            scope: me
                        }
                    ]);
                } else {
                    siteMapPanel.setEmptyDrawingText(LocaleManager.getLocalizedString('Site Map is not available', 'ComplianceSurvey.controller.Navigation'));
                }

                svgComponent.on('aftersvgload', me.locateBuildingAsset, me, record);
                Ext.callback(callbackFn, me);
            };

        siteDrawingStore.clearFilter();
        siteDrawingStore.filter('site_id', siteId);
        siteDrawingStore.load(function (records) {
            if (records && records.length > 0) {
                doProcessSvg(records[0].get('svg_data'));
            }
        });
    },

    locateBuildingAsset: function (svgComponent, record) {
        var opts = {cssClass: 'zoomed-asset-default', removeStyle: true};

        if (!Ext.isEmpty(record) && !Ext.isEmpty(record.get('bl_id'))) {
            svgComponent.findAssets([record.get('bl_id')], opts);
        }
    },

    getFloorPlanData: function (floorPlanPanel, record, callbackFn, scope) {
        var me = scope || this,
            svgComponent = floorPlanPanel.down('svgcomponent');

        // Retrieve the floor plan svg data
        Floorplan.util.Drawing.readDrawingFromStorageOrRetrieveIfNot(record.get('bl_id'), record.get('fl_id'), 'QuestionnaireFloorPlan', [], false)
            .then(function (svgData) {
                if (svgData !== '') {
                    floorPlanPanel.setSvgData(svgData);
                    floorPlanPanel.setEventHandlers([
                        {
                            assetType: 'rm',
                            handler: me.onClickRoom,
                            scope: me
                        }
                    ]);
                } else {
                    floorPlanPanel.setEmptyDrawingText(LocalizedStrings.z_Floorplan_Not_Available);
                }


                svgComponent.on('aftersvgload', me.locateRoomAsset, me, record);

                Ext.callback(callbackFn, me);
            });
    },

    onClickRoom: function (roomCode) {
        var me = this.scope,
            codes = roomCode.split(';'),
            blId = codes[0],
            flId = codes[1],
            rmId = codes[2],
            store = Ext.getStore('surveysRoomInfoReportStore'),
            filterArray = me.getRoomFilterArray(blId, flId, rmId),
            reportConfig,
            detailView;

        Mask.displayLoadingMask();
        store.retrieveSingleRecord(filterArray)
            .then(function (record) {
                if (Ext.isEmpty(record)) {
                    Ext.Msg.alert('', LocaleManager.getLocalizedString('Information available only for the highlighted room.', 'ComplianceSurvey.controller.Navigation'));
                    Mask.hideLoadingMask();
                    return;
                }
                reportConfig = Space.view.report.Configuration.getRoomInfoReportConfig();
                reportConfig.record = record;
                detailView = Ext.create('Common.view.report.Detail', reportConfig);
                Mask.hideLoadingMask();

                detailView.show();
            }, function () {
                // Error loading store.
                Mask.hideLoadingMask();
            });
    },

    getRoomFilterArray: function (blId, flId, rmId) {
        var filterArray = [];

        filterArray.push(Ext.create('Common.util.Filter', {
            property: 'bl_id',
            value: blId,
            exactMatch: true
        }));
        filterArray.push(Ext.create('Common.util.Filter', {
            property: 'fl_id',
            value: flId,
            exactMatch: true
        }));
        filterArray.push(Ext.create('Common.util.Filter', {
            property: 'rm_id',
            value: rmId,
            exactMatch: true
        }));

        return filterArray;
    },

    locateRoomAsset: function (svgComponent, record) {
        var me = this,
            fullRmId,
            opts = {cssClass: 'zoomed-asset-default', removeStyle: true};

        if (!Ext.isEmpty(record)) {
            fullRmId = me.getFullRoomId(record);
            if (fullRmId) {
                svgComponent.findAssets([fullRmId], opts);
            }
        }
    },

    getFullRoomId: function (record) {
        var fullRmId;

        if (record instanceof Common.data.Model) {
            fullRmId = record.getRoomId();
        } else if (!Ext.isEmpty(record.get('bl_id')) && !Ext.isEmpty(record.get('fl_id')) && !Ext.isEmpty(record.get('rm_id'))) {
            fullRmId = Ext.String.format('{0};{1};{2}', record.get('bl_id'), record.get('fl_id'), record.get('rm_id'));
        }

        return fullRmId;
    },

    scanEquipment: function () {
        var barcodePopup = this.getScanEqView(),
            isNew = false,
            title = LocaleManager.getLocalizedString('Scan or Enter Equipment ID', 'ComplianceSurvey.controller.Navigation');

        if (Ext.isEmpty(barcodePopup)) {
            barcodePopup = Ext.create('Ext.Panel', {
                name: 'scanEqView',
                modal: true,
                hideOnMaskTap: true,
                width: 300,
                items: [
                    {
                        xtype: 'toolbar',
                        docked: 'top',
                        title: title
                    },
                    {
                        xtype: 'barcodefield',
                        name: 'scan_eq',
                        style: 'padding: .1em .5em .1em .5em'
                    }
                ]

            });

            isNew = true;
        }

        if (isNew) {
            Ext.Viewport.add(barcodePopup);
        }
        barcodePopup.showBy(this.getBarcodeScanBtn());
    },

    onFilterByEq: function (searchFieldValue, callbackFn, scope) {
        // Filter the Survey table
        var surveysStore = Ext.getStore('complianceSurveyTasksStore'),
            filters = this.buildSearchFilters(['eq_id'], searchFieldValue);

        surveysStore.clearFilter();
        surveysStore.setFilters(filters);
        surveysStore.loadPage(1, callbackFn, scope);
    },

    /**
     * Handle tap on map icon button.
     */
    showSurveysMap: function () {
        var view = Ext.create('ComplianceSurvey.view.SurveysMap');

        this.getMainView().push(view);
    },

    closeSurveysMap: function () {
        var navigationToolbar = this.getMainView().down('navigationbar');

        navigationToolbar.show();

        this.getMainView().pop();
    },

    refreshBuildingsStore: function () {
        var buildingsStore = Ext.getStore('complianceSurveyBuildings'),
            surveysStore = Ext.getStore('complianceSurveyTasksStore'),
            surveysStoreFilters = surveysStore.getFilters(),
            buildings,
            filter,
            filterArray = [],
            constructFilter = function (buildings) {
                var p = Promise.resolve();
                buildings.forEach(function (building) {
                    p = p.then(function () {
                        filter = Ext.create('Common.util.Filter', {
                            property: 'bl_id',
                            value: building,
                            exactMatch: true,
                            operator: '='
                        });
                        return surveysStore.retrieveSingleRecord(surveysStoreFilters.concat([filter]))
                            .then(function (record) {
                                if (!Ext.isEmpty(record)) {
                                    filterArray.push(Ext.create('Common.util.Filter', {
                                        property: 'bl_id',
                                        value: record.get('bl_id'),
                                        exactMatch: true,
                                        operator: '=',
                                        conjunction: 'OR'
                                    }));
                                }
                            });
                    });
                });
                return p;
            };

        return buildingsStore.retrieveAllRecords([])
            .then(function (buildingRecords) {
                buildings = Ext.Array.pluck(Ext.Array.pluck(buildingRecords, 'data'), 'bl_id');
                return constructFilter(buildings);
            })
            .then(function () {
                buildingsStore.setFilters(filterArray);
                return SyncManager.loadStore('complianceSurveyBuildings');
            });
    },

    onSurveyLocationSegmentedButtonToggle: function (segmentedButton, button, isPressed) {
        var siteMap = this.getSiteMapPanel(),
            floorPlan = this.getFloorPlanPanel(),
            itemId = button.getItemId();

        if (isPressed) {
            if (itemId === 'siteMapBtn') {
                siteMap.setHidden(false);
                floorPlan.setHidden(true);
                // refresh building highlight
                this.locateBuildingAsset(siteMap.down('svgcomponent'), siteMap.getParent().getRecord());
            }
            if (itemId === 'floorPlanBtn') {
                siteMap.setHidden(true);
                floorPlan.setHidden(false);
                // refresh room highlight
                this.locateRoomAsset(floorPlan.down('svgcomponent'), floorPlan.getParent().getRecord());
            }
        }
    },

    /**
     * Handle check toolbar checkbox in questions list.
     */
    selectAllQuestions: function () {
        var checkboxFields = this.getSelectionCheckboxes();

        Ext.each(checkboxFields, function (checkbox) {
            checkbox.check();
        });
    },

    /**
     * Handle uncheck toolbar checkbox in questions list.
     */
    deselectAllQuestions: function () {
        var checkboxFields = this.getSelectionCheckboxes();

        Ext.each(checkboxFields, function (checkbox) {
            checkbox.uncheck();
        });
    },

    /**
     * Return checkboxes for questions selection.
     * @returns {Array} checkbox fields
     */
    getSelectionCheckboxes: function () {
        var questionsList = this.getQuestionsList(),
            checkboxFields = [],
            checkboxField,
            i;

        Ext.ComponentQuery.pseudos.validCheckbox = function (items) {
            for (i = 0; i < items.length; i++) {
                checkboxField = items[i];
                if (!Ext.isEmpty(checkboxField.getName())
                    && checkboxField.getName().indexOf('checkbox_question_') === 0) {
                    checkboxFields.push(checkboxField);
                }
            }
            return checkboxFields;
        };

        checkboxFields = questionsList.query('checkboxfield:validCheckbox');

        return checkboxFields;
    },

    /**
     * Handle tap on reset questions button.
     */
    resetQuestions: function () {
        var me = this,
            questionnaireView = me.getMainView().getNavigationBar().getCurrentView(),
            checkboxFields = [],
            checkboxField,
            checkboxName,
            i;

        Ext.ComponentQuery.pseudos.selectedCheckbox = function (items) {
            for (i = 0; i < items.length; i++) {
                checkboxField = items[i];
                checkboxName = checkboxField.getName();
                if (!Ext.isEmpty(checkboxName) && checkboxName.indexOf('checkbox_question_') === 0
                    && checkboxField.getChecked() === true) {

                    checkboxFields.push(checkboxField);
                }
            }
            return checkboxFields;
        };

        checkboxFields = questionnaireView.query('checkboxfield:selectedCheckbox');

        if (Ext.isEmpty(checkboxFields)) {
            Ext.Msg.alert('', me.getConfirmDeleteMessage());
        } else {
            Ext.Msg.confirm('', me.getDeleteMessage(), function (buttonId) {
                    if (buttonId === 'yes') {
                        me.doResetQuestions(checkboxFields);
                    }
                }
            );
        }
    },

    doResetQuestions: function (checkboxFields) {
        var questionnaireView = this.getMainView().getNavigationBar().getCurrentView(),
            checkboxField,
            questionCode,
            checkboxName,
            questionFields,
            questionRecords = [],
            selectAllCheckbox = this.getSelectAllCheckbox(),
            i,
            questionRecord,
            questionType,
            documentQuestionFields = [],
            retrieveQuestionRecordsToDelete = function () {
                for (i = 0; i < checkboxFields.length; i++) {
                    checkboxField = checkboxFields[i];
                    checkboxName = checkboxField.getName();
                    questionCode = checkboxName.split('checkbox_question_').pop();
                    questionFields = questionnaireView.query('*[name=question_' + questionCode + ']');
                    if (!Ext.isEmpty(questionFields)) {
                        questionRecord = questionFields[0].getParent().getRecord();
                        if (questionRecord.get('question_type') !== 'document'
                            && questionHadAnswers(questionFields)) {
                            questionRecords.push(questionRecord);
                        }
                    }
                }
                return questionRecords;
            },
            questionHadAnswers = function (questionFields) {
                var i,
                    questionField,
                    hadAnswers = false;

                for (i = 0; i < questionFields.length; i++) {
                    questionField = questionFields[i];
                    if (questionField.answerRecordId > 0 && questionField.isAnswerDeleted === 0) {
                        hadAnswers = true;
                    }
                }
                return hadAnswers;
            },
            retrieveDocumentQuestionFields = function () {
                for (i = 0; i < checkboxFields.length; i++) {
                    checkboxField = checkboxFields[i];
                    checkboxName = checkboxField.getName();
                    questionCode = checkboxName.split('checkbox_question_').pop();
                    questionFields = questionnaireView.query('*[name=question_' + questionCode + ']');
                    if (!Ext.isEmpty(questionFields)) {
                        questionRecord = questionFields[0].getParent().getRecord();
                        questionType = questionRecord.get('question_type');
                        if (questionType === 'document') {
                            documentQuestionFields = documentQuestionFields.concat(questionFields);
                        }
                    }
                }
                return documentQuestionFields;
            },
            resetQuestions = function () {
                for (i = 0; i < checkboxFields.length; i++) {
                    checkboxField = checkboxFields[i];
                    checkboxName = checkboxField.getName();
                    if (!questionnaireView.down('*[name=' + checkboxName + ']')) {
                        // followup questions are deleted on change event of the parent question
                        // (ComplianceSurvey.util.Questionnaire.handleFollowUpItems())
                        continue;
                    }
                    questionCode = checkboxName.split('checkbox_question_').pop();
                    questionFields = questionnaireView.query('*[name=question_' + questionCode + ']');
                    if (!Ext.isEmpty(questionFields)) {
                        questionRecord = questionFields[0].getParent().getRecord();
                        questionType = questionRecord.get('question_type');
                        if (questionType !== 'document') {
                            resetQuestion(questionFields, questionRecord, questionnaireView);
                        }
                    }
                    checkboxField.uncheck();
                }
            },
            resetQuestion = function (questionFields, questionRecord, questionnaireView) {
                var i,
                    questionField,
                    hadAnswers = questionHadAnswers(questionFields);

                for (i = 0; i < questionFields.length; i++) {
                    questionField = questionFields[i];
                    questionField.reset();
                    if (Ext.isEmpty(questionField.answerId) || questionField.answerId === 0) {
                        // newly added answer records, not yet synced, will be deleted
                        questionField.answerRecordId = null;
                    }
                    questionField.isAnswerDeleted = 1;
                }
                if (hadAnswers) {
                    if (QuestionnaireUtil.isParentQuestion(questionRecord)) {
                        questionnaireView.numberOfAnsweredParentQuestions--;
                    }
                }
            },
            onFinish = function () {
                questionnaireView.disableFieldChangeEvents = false;
            };


        questionnaireView.disableFieldChangeEvents = true;

        questionRecords = retrieveQuestionRecordsToDelete();
        documentQuestionFields = retrieveDocumentQuestionFields();

        QuestionnaireUtil.deleteAnswersForRecords(questionRecords, questionnaireView.getRecord())
            .then(function () {
                return QuestionnaireUtil.deleteAsnwersForDocumentQuestionFields(documentQuestionFields, questionnaireView);
            })
            .then(function () {
                resetQuestions();
                selectAllCheckbox.uncheck();
                return QuestionnaireUtil.updatePercentageOfCompletion(questionnaireView);
            })
            .then(function () {
                if (!questionnaireView.isViewLoading) {
                    QuestionnaireUtil.resetSurveyStatusIfCompleted(questionnaireView.getRecord());
                }
            })
            .done(onFinish, onFinish);
    },

    onNavigationOptionSelected: function (selectedValue) {
        var me = this,
            action = selectedValue.get('action'),
            questionsList = me.getMainView().getNavigationBar().getCurrentView(),
            surveyRecord = questionsList.getRecord();

        if (action === 'info') {
            me.showSurveyInfoForRecord(surveyRecord);
        } else if (action === 'locate') {
            me.locateSurveyForRecord(surveyRecord);
        }
    },

    onCompleteSurvey: function () {
        var me = this,
            questionsList = me.getMainView().getNavigationBar().getCurrentView(),
            surveyTaskRecord = questionsList.getRecord(),
            errorTitle = LocaleManager.getLocalizedString('Error', 'ComplianceSurvey.controller.Navigation'),
            errorMsg = LocaleManager.getLocalizedString('Not all required questions have answers. Can not complete the survey until all required questions have answers.', 'ComplianceSurvey.controller.Navigation');

        QuestionnaireUtil.isValidForm(questionsList)
            .then(function (isValid) {
                if (isValid) {
                    QuestionnaireUtil.changeSurveyStatus(surveyTaskRecord, 'COMPLETED')
                        .then(function () {
                            me.getMainView().pop();
                        });
                } else {
                    Ext.Msg.alert(errorTitle, errorMsg);
                }
            });
    }
});