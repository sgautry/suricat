Ext.define('ComplianceSurvey.controller.ComplianceSurveySync', {
    extend: 'Ext.app.Controller',

    requires: [
        'Common.sync.Manager'
    ],

    config: {
        refs: {
            mainView: 'main',
            questionnaireView: 'questionsList'
        },
        control: {
            'button[action=syncSurvey]': {
                tap: 'onSyncSurveys'
            }
        }
    },

    /**
     * Prevents multiple fast taps from triggering simultaneous sync actions.
     */
    onSyncSurveys: (function () {
        var isTapped = false;
        return function () {
            if (!isTapped) {
                isTapped = true;
                this.syncSurveyItems();
                setTimeout(function () {
                    isTapped = false;
                }, 500);
            }
        };
    })(),

    syncSurveyItems: function () {
        var me = this,
            currentView = me.getMainView().getNavigationBar().getCurrentView(),
            isQuestionnaireView = currentView.xtype === 'questionsList',
            completedSurvey = isQuestionnaireView && currentView.getRecord().get('status') === 'COMPLETED';

        var syncPromiseChain = function () {
            // SyncManager.syncTransactionTables(['complianceSurveysStore', 'answersStore'])
            return SyncManager.uploadModifiedRecords(['complianceSurveysStore', 'answersStore'])
                .then(function () {
                    return me.syncAnswers();
                })
                .then(function () {
                    //return SyncManager.syncTransactionTables(['complianceSurveysStore', 'answersStore']);
                    return SyncManager.downloadTransactionRecords(['complianceSurveysStore', 'answersStore']);
                })
                .then(function () {
                    // Load views on store complianceSurveysStore
                    var storesToLoad = [
                        'complianceSurveysStore',
                        'answersStore',
                        'uniquePrograms',
                        'uniqueProjects',
                        'uniqueRegulations'
                    ];

                    return SyncManager.loadStores(storesToLoad);
                })
                .then(function () {
                    return me.downloadFilteredBackgroundData();
                })
                .then(function () {
                    return me.downloadLookupTables();
                })
                .then(function () {
                    if(completedSurvey){
                        me.getMainView().pop();
                    }
                    return Promise.resolve();
                })
                .then(null, function (error) {
                    Log.log(error, 'error');
                    Ext.Msg.alert('', error);
                });
        };

        // Prevent concurrent sync actions from being fired
        if (SyncManager.syncIsActive) {
            return;
        }

        return SyncManager.doInSession(syncPromiseChain);

    },

    /**
     * Executes the server side workflow rules
     */
    syncAnswers: function () {
        return Common.service.workflow.Workflow.execute('AbRiskCompliance-ComplianceSurveyMobileService-syncAnswers',
            [ConfigFileManager.username], Network.SERVICE_TIMEOUT);
    },

    downloadFilteredBackgroundData: function () {
        var me = this;

        return me.downloadComplianceLocations()
            .then(function () {
                return me.downloadLocationsAndPlans();
            })
            .then(function () {
                return me.downloadQuestionnaires();
            })
            .then(function () {
                return me.downloadQuestions();
            });
    },

    downloadComplianceLocations: function () {
        var uniqueSurveyLocations = Ext.getStore('uniqueSurveyLocations'),
            complianceLocationsStore = Ext.getStore('complianceLocationsStore'),
            locations = [],
            restriction = [],
            i;

        return uniqueSurveyLocations.retrieveAllRecords()
            .then(function (surveyLocations) {
                locations = Ext.Array.pluck(Ext.Array.pluck(surveyLocations, 'data'), 'location_id');
                if (locations.length === 0) {
                    Log.log('No survey location available', 'info');//this message is used in console, no need to translate it
                } else {
                    for (i = 0; i < locations.length; i++) {
                        if (!Ext.isEmpty(locations[i])) {
                            restriction.push({
                                tableName: 'compliance_locations',
                                fieldName: 'location_id',
                                operation: 'EQUALS',
                                value: locations[i],
                                relativeOperation: 'OR'
                            });
                        }
                    }
                }

                // "download" zero locations to have the table fields definitions
                if (Ext.isEmpty(restriction)) {
                    restriction.push({
                        tableName: 'compliance_locations',
                        fieldName: 'location_id',
                        operation: 'IS_NULL'
                    });
                }
                complianceLocationsStore.setRestriction(restriction);

                return Promise.resolve(restriction);
            })
            .then(function () {
                return SyncManager.downloadValidatingTables(['complianceLocationsStore']);
            })
            .then(function () {
                // Load views on store complianceLocationsStore
                var storesToLoad = [
                    'complianceSurveyTasksStore',
                    'uniqueEquipmentStandards',
                    'uniqueEquipmentIds',
                    'uniqueEmployeeIds'
                ];

                return SyncManager.loadStores(storesToLoad);
            })
            .then(null, function (error) {
                Log.log(error, 'info');
            });
    },

    downloadLocationsAndPlans: function () {
        var me = this,
            complianceLocationsStore = Ext.getStore('complianceLocationsStore'),
            locationStores = ['complianceSurveySites', 'complianceSurveyBuildings', 'complianceSurveyFloors', 'complianceSurveyRooms'],
            floorCodes;

        return new Promise(function (resolve, reject) {
            complianceLocationsStore.retrieveAllStoreRecords(null, function (surveyLocations) {
                if (surveyLocations.length === 0) {
                    reject('No survey location available');//this message is used in console, no need to translate it
                }
                floorCodes = me.setLocationRestrictions(surveyLocations);
                resolve();
            }, this);
        })
            .then(function () {
                return SyncManager.downloadValidatingTables(locationStores);
            })
            .then(function () {
                return SyncManager.loadStores(locationStores);
            })
            .then(function () {
                return me.downloadSiteDrawings();
            })
            .then(function () {
                return me.downloadFloorPlans(floorCodes);
            })
            .then(null, function (error) {
                Log.log(error, 'info');
            });
    },

    /**
     * Loops through survey locations and set the restrictions for sites, buildings, floors and rooms stores and returns the floor codes necessary for downloading the floor plans.
     * @param surveyLocations all records from complianceLocationsStore
     * @returns {Array} floor codes necessary for downloading the floor plans
     */
    setLocationRestrictions: function (surveyLocations) {
        var sitesStore = Ext.getStore('complianceSurveySites'),
            buildingsStore = Ext.getStore('complianceSurveyBuildings'),
            floorsStore = Ext.getStore('complianceSurveyFloors'),
            roomsStore = Ext.getStore('complianceSurveyRooms'),
            sitesRestriction = [],
            buildingsRestriction = [],
            floorsRestriction = [],
            roomsRestriction = [],
            floorCodes = [],
            i;

        for (i = 0; i < surveyLocations.length; i++) {
            if (!Ext.isEmpty(surveyLocations[i].get('site_id'))) {
                sitesRestriction.push({
                    tableName: 'site',
                    fieldName: 'site_id',
                    operation: 'EQUALS',
                    value: surveyLocations[i].get('site_id'),
                    relativeOperation: 'OR'
                });
            }

            if (!Ext.isEmpty(surveyLocations[i].get('bl_id'))) {
                buildingsRestriction.push({
                    tableName: 'bl',
                    fieldName: 'bl_id',
                    operation: 'EQUALS',
                    value: surveyLocations[i].get('bl_id'),
                    relativeOperation: 'OR'
                });

                if (!Ext.isEmpty(surveyLocations[i].get('fl_id'))) {
                    floorsRestriction.push({
                        tableName: 'fl',
                        fieldName: 'bl_id',
                        operation: 'EQUALS',
                        value: surveyLocations[i].get('bl_id'),
                        relativeOperation: 'OR_BRACKET'
                    });
                    floorsRestriction.push({
                        tableName: 'fl',
                        fieldName: 'fl_id',
                        operation: 'EQUALS',
                        value: surveyLocations[i].get('fl_id'),
                        relativeOperation: 'AND'
                    });

                    floorCodes.push(
                        {
                            bl_id: surveyLocations[i].get('bl_id'),
                            fl_id: surveyLocations[i].get('fl_id')
                        }
                    );

                    if (!Ext.isEmpty(surveyLocations[i].get('rm_id'))) {
                        roomsRestriction.push({
                            tableName: 'rm',
                            fieldName: 'bl_id',
                            operation: 'EQUALS',
                            value: surveyLocations[i].get('bl_id'),
                            relativeOperation: 'OR_BRACKET'
                        });
                        roomsRestriction.push({
                            tableName: 'rm',
                            fieldName: 'fl_id',
                            operation: 'EQUALS',
                            value: surveyLocations[i].get('fl_id'),
                            relativeOperation: 'AND'
                        });
                        roomsRestriction.push({
                            tableName: 'rm',
                            fieldName: 'rm_id',
                            operation: 'EQUALS',
                            value: surveyLocations[i].get('rm_id'),
                            relativeOperation: 'AND'
                        });
                    }
                }
            }
        }

        // when no restriction exists don't download all locations
        if (Ext.isEmpty(sitesRestriction)) {
            sitesRestriction.push({
                tableName: 'site',
                fieldName: 'site_id',
                operation: 'IS_NULL'
            });
        }
        if (Ext.isEmpty(buildingsRestriction)) {
            buildingsRestriction.push({
                tableName: 'bl',
                fieldName: 'bl_id',
                operation: 'IS_NULL'
            });
        }
        if (Ext.isEmpty(floorsRestriction)) {
            floorsRestriction.push({
                tableName: 'fl',
                fieldName: 'fl_id',
                operation: 'IS_NULL'
            });
        }
        if (Ext.isEmpty(roomsRestriction)) {
            roomsRestriction.push({
                tableName: 'rm',
                fieldName: 'rm_id',
                operation: 'IS_NULL'
            });
        }

        sitesStore.setRestriction(sitesRestriction);
        buildingsStore.setRestriction(buildingsRestriction);
        floorsStore.setRestriction(floorsRestriction);
        roomsStore.setRestriction(roomsRestriction);

        return floorCodes;
    },

    downloadSiteDrawings: function () {
        var me = this;

        return me.getSiteRecords()
            .then(function (records) {
                if (records.length === 0) {
                    return Promise.resolve();
                } else {
                    return Space.SpaceDownload.getSiteDrawingFromServerWithoutSession(records);
                }
            });
    },

    getSiteRecords: function () {
        var sitesStore = Ext.getStore('complianceSurveySites'),
            siteRecords = [],
            i;
        return new Promise(function (resolve) {
            sitesStore.retrieveAllStoreRecords(null, function (records) {
                for (i = 0; i < records.length; i++) {
                    siteRecords.push({'site_id': records[i].get('site_id')});
                }
                resolve(siteRecords);
            });
        });
    },

    downloadFloorPlans: function (floorCodes) {
        //use the same view as app Asset and Equipment Survey
        var highlightParameters = [
            {
                view_file: 'ab-eq-survey-eqauditxrm.axvw',
                hs_ds: 'abEqSurveyEqauditxRmHighlight',
                label_ds: 'abEqSurveyEqauditxRmLabel',
                label_clr: 'gray',
                label_ht: '0.90'
            }
        ];

        var p = Promise.resolve();
        floorCodes.forEach(function (code) {
            p = p.then(function () {
                return Common.service.drawing.Drawing.retrieveSvgFromServer(code, null, highlightParameters);
            }).then(function (svgData) {
                return Floorplan.util.Floorplan.saveFloorPlan(code.bl_id, code.fl_id, 'QuestionnaireFloorPlan', svgData);
            });
        });
        return p;

    },

    downloadQuestionnaires: function () {
        var me = this,
            uniqueQuestionnaires = Ext.getStore('uniqueQuestionnaires'),
            questionnairesStore = Ext.getStore('questionnairesStore'),
            questQuestionsStore = Ext.getStore('questQuestionsStore'),
            questionnaireIds = [],
            questionnaireRestriction = [],
            questQuestionRestriction = [],
            i;

        return new Promise(function (resolve, reject) {
            uniqueQuestionnaires.load(function () {
                uniqueQuestionnaires.retrieveAllStoreRecords(null, function (surveyRecords) {
                    questionnaireIds = Ext.Array.pluck(Ext.Array.pluck(surveyRecords, 'data'), 'questionnaire_id_ext');

                    if (questionnaireIds.length === 0) {
                        reject('No questionnaire available');
                    }
                    for (i = 0; i < questionnaireIds.length; i++) {
                        if (!Ext.isEmpty(questionnaireIds[i])) {
                            questionnaireRestriction.push({
                                tableName: 'questionnaire_ext',
                                fieldName: 'questionnaire_id',
                                operation: 'EQUALS',
                                value: questionnaireIds[i],
                                relativeOperation: 'OR'
                            });
                            questQuestionRestriction.push({
                                tableName: 'quest_question_ext',
                                fieldName: 'questionnaire_id',
                                operation: 'EQUALS',
                                value: questionnaireIds[i],
                                relativeOperation: 'OR'
                            });
                        }
                    }
                    questionnairesStore.setRestriction(questionnaireRestriction);
                    questQuestionsStore.setRestriction(questQuestionRestriction);

                    resolve();
                }, me);
            }, me);
        })
            .then(function () {
                return SyncManager.downloadValidatingTables(['questionnairesStore', 'questQuestionsStore']);
            })
            .then(null, function (error) {
                Log.log(error, 'info');
            });

    },

    downloadQuestions: function () {
        var me = this,
            questQuestionsStore = Ext.getStore('questQuestionsStore'),
            questionsStore = Ext.getStore('questionsStore'),
            answerOptionsStore = Ext.getStore('answerOptionsStore'),
            questionIds,
            questionRestriction = [],
            answerOptionRestriction = [],
            i;

        return new Promise(function (resolve, reject) {
            questQuestionsStore.retrieveAllStoreRecords(null, function (questQuestionRecords) {
                questionIds = Ext.Array.pluck(Ext.Array.pluck(questQuestionRecords, 'data'), 'question_id');

                if (questionIds.length === 0) {
                    reject('No question available');
                }
                for (i = 0; i < questionIds.length; i++) {
                    if (!Ext.isEmpty(questionIds[i])) {
                        //Download also follow-up questions
                        questionRestriction.push({
                            tableName: 'question_ext',
                            fieldName: 'hierarchy_ids',
                            operation: 'LIKE',
                            value: questionIds[i] + '|%',
                            relativeOperation: 'OR'
                        });
                    }
                }
                questionsStore.setRestriction(questionRestriction);

                resolve();
            }, me);
        })
            .then(function () {
                return SyncManager.downloadValidatingTables(['questionsStore']);
            })
            .then(function () {
                // download answer options for all questions including follow-up ones
                return new Promise(function (resolve, reject) {
                    questionsStore.retrieveAllStoreRecords(null, function (questionRecords) {
                        questionIds = Ext.Array.pluck(Ext.Array.pluck(questionRecords, 'data'), 'question_id');

                        if (questionIds.length === 0) {
                            reject('No question available');
                        }
                        for (i = 0; i < questionIds.length; i++) {
                            if (!Ext.isEmpty(questionIds[i])) {
                                answerOptionRestriction.push({
                                    tableName: 'quest_answer_option_ext',
                                    fieldName: 'question_id',
                                    operation: 'EQUALS',
                                    value: questionIds[i],
                                    relativeOperation: 'OR'
                                });
                            }
                        }
                        answerOptionsStore.setRestriction(answerOptionRestriction);

                        resolve();
                    }, me);
                });
            })
            .then(function () {
                return SyncManager.downloadValidatingTables(['answerOptionsStore']);
            })
            .then(function () {
                return SyncManager.loadStores(['questionItemsStore']);
            })
            .then(null, function (error) {
                Log.log(error, 'info');
            });
    },

    downloadLookupTables: function () {
        var me = this,
            questionsStore = Ext.getStore('questionsStore'),
            filterArray = [],
            lookupStores = [],
            lookupStoreNames = [],
            store,
            tableName,
            fieldName,
            i;

        return new Promise(function (resolve) {
            filterArray.push(Ext.create('Common.util.Filter', {
                property: 'question_type',
                value: 'lookup',
                exactMatch: true
            }));
            questionsStore.retrieveAllStoreRecords(filterArray, function (questionRecords) {
                for (i = 0; i < questionRecords.length; i++) {
                    tableName = questionRecords[i].get('lookup_table');
                    fieldName = questionRecords[i].get('lookup_field');

                    store = QuestionnaireUtil.createLookupStore(tableName, fieldName);

                    lookupStores.push(store);
                    lookupStoreNames.push(store.getStoreId());
                }

                resolve();
            }, me);
        })
            .then(function () {
                return SyncManager.downloadValidatingTables(lookupStores);
            })
            .then(function () {
                return SyncManager.loadStores(lookupStoreNames);
            });
    }
});