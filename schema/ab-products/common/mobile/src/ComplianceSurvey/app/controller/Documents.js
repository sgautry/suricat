Ext.define('ComplianceSurvey.controller.Documents', {
    extend: 'Ext.app.Controller',

    requires: ['Common.document.DocumentManager'],

    config: {
        refs: {
            mainView: 'main',
            documentViewItem: 'documentItem',
            cameraPanel: 'camera',
            documentListView: 'complianceSurveyDocumentList',
            questionnaireView: 'questionsList'
        },
        control: {
            cameraPanel: {
                attach: 'onAttachPhoto'
            },
            documentViewItem: {
                displaydocument: 'onDisplayDocument',
                deleteDocument: 'onDeleteDocument'
            }
        },

        documentsUsedTitle: LocaleManager.getLocalizedString('Documents', 'ComplianceSurvey.controller.Documents'),
        documentsUsedText: LocaleManager.getLocalizedString('You have added all documents allowed for this question',
            'ComplianceSurvey.controller.Documents'),

        documentDeleteMessage: LocaleManager.getLocalizedString('This action will remove the document. Do you want to proceed?',
            'ComplianceSurvey.controller.Documents')
    },

    onDisplayDocument: function (answerId, fileName, documentFieldId, fieldName, isOnDemandDocument) {
        var answersStore = Ext.getStore('answersStore'),
            answerRecord = answersStore.findRecord('id', answerId),
            documentField = fieldName + '_contents',
            documentData;

        if (answerRecord) {
            documentData = answerRecord.get(documentField);
            DocumentManager.displayDocumentOrImage(documentData, fileName, this.cameraPanel);
        }
    },

    /**
     * Action when tapping delete document icon
     * @param recordId
     * @param documentFieldId
     * @param fieldName
     * @param documentData
     * @param documentListItem
     */
    onDeleteDocument: function (recordId, documentFieldId, fieldName, documentData, documentListItem) {
        var me = this,
            store = Ext.getStore('answersStore'),
            record = store.findRecord('id', recordId);

        if (record) {
            Ext.Msg.confirm(me.getDocumentsUsedTitle(), me.getDocumentDeleteMessage(), function (buttonId) {
                if (buttonId === 'yes') {
                    documentListItem.setDocumentData(Common.config.GlobalParameters.MARK_DELETED_TEXT);
                    me.doDeleteDocument(store, record, fieldName);
                }
            });
        }
    },

    /**
     * Deletes document
     * @param store
     * @param record
     * @param fieldName
     */
    doDeleteDocument: function (store, record, fieldName) {
        var me = this,
            documentListView = me.getDocumentListView(),
            questionnaireView = me.getQuestionnaireView(),
            questionRecord = documentListView.getQuestionRecord(),
            processDocumentFields = function () {
                return documentListView.processDocumentFields()
                    .then(function (answerRecords) {
                        QuestionnaireUtil.refreshQuestionnaireViewDocField(questionnaireView, questionRecord, answerRecords);
                        return QuestionnaireUtil.updatePercentageOfCompletion(questionnaireView)
                            .then(function () {
                                if (questionnaireView.isViewLoading) {
                                    return Promise.resolve();
                                } else {
                                    return QuestionnaireUtil.resetSurveyStatusIfCompleted(questionnaireView.getRecord());
                                }
                            });
                    });
            };

        // if record was not synchronized yet it can be removed immediately
        if (record.get(fieldName + '_isnew')) {
            store.remove(record);
            store.sync(function () {
                return processDocumentFields();
            }, me);
        } else {
            return DocumentManager.deleteDocumentFile(store, record, fieldName)
                .then(function () {
                    return DocumentManager.markDocumentDeleted(store, record, fieldName);
                })
                .then(function () {
                    return processDocumentFields();
                })
                .then(null, function (error) {
                    Log.log(error, 'error');
                    Ext.Msg.alert('', error);
                });
        }
    },

    /**
     * Saves the photo data in the Answer record.
     * @param cameraPanel
     */
    onAttachPhoto: function (cameraPanel) {
        var me = this,
            imageData = cameraPanel.getImageData(),
            currentView = me.getMainView().getNavigationBar().getCurrentView(),
            questionnaireRecord = currentView.getQuestionnaireRecord(),
            questionRecord = currentView.getQuestionRecord(),
            numMaxResponses = questionRecord.get('num_responses_max') || 0,
            answerRecord,
            answersStore = Ext.getStore('answersStore'),
            questionnaireView = me.getQuestionnaireView(),
            notDeletedDocumentsCount = answersStore.getNotDeletedDocumentsCount(),
            saveStore = function (store) {
                return new Promise(function (resolve) {
                    store.sync(resolve);
                });
            };

        if ((numMaxResponses >= 1 && notDeletedDocumentsCount >= numMaxResponses)
            || (numMaxResponses < 1 && notDeletedDocumentsCount >= 1)) {

            Ext.Msg.alert(me.getDocumentsUsedTitle(), me.getDocumentsUsedText());
            return;
        }

        document.activeElement.blur();
        answerRecord = QuestionnaireUtil.createAnswerRecord(questionnaireRecord, questionRecord);

        //Disable autoSync to prevent duplicate update statements from being generated.
        answersStore.setAutoSync(false);
        me.addPhotoToAnswerRecord(answerRecord, imageData);
        answersStore.add(answerRecord);
        saveStore(answersStore)
            .then(function () {
                answersStore.setAutoSync(true);

                currentView.processDocumentFields()
                    .then(function (answerRecords) {
                        QuestionnaireUtil.refreshQuestionnaireViewDocField(questionnaireView, questionRecord, answerRecords);
                        return QuestionnaireUtil.updatePercentageOfCompletion(questionnaireView)
                            .then(function () {
                                cameraPanel.onClosePanel();
                                if (questionnaireView.isViewLoading) {
                                    return Promise.resolve();
                                } else {
                                    return QuestionnaireUtil.resetSurveyStatusIfCompleted(questionnaireView.getRecord());
                                }
                            });
                    });

            });

    },

    addPhotoToAnswerRecord: function (answerRecord, imageData) {
        var documentField = 'doc';

        answerRecord.setDocumentFieldData(documentField, imageData);
    }
});