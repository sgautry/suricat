Ext.define('ComplianceSurvey.model.Question', {
    extend: 'Common.data.Model',

    config: {
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'question_id',
                type: 'int'
            },
            {
                name: 'question_type',
                type: 'string'
            },
            {
                name: 'question_text',
                type: 'string'
            },
            {
                name: 'is_active',
                type: 'int'
            },
            {
                name: 'hierarchy_ids',
                type: 'string'
            },
            {
                name: 'parent_answer_condition',
                type: 'string'
            },
            {
                name: 'is_required_child',
                type: 'int'
            },
            {
                name: 'sort_order_child',
                type: 'int'
            },
            {
                name: 'freeform_width',
                type: 'int'
            },
            {
                name: 'lookup_table',
                type: 'string'
            },
            {
                name: 'lookup_field',
                type: 'string'
            },
            {
                name: 'unit_type',
                type: 'string'
            },
            {
                name: 'unit_default',
                type: 'string'
            },
            {
                name: 'num_responses_max',
                type: 'int'
            }
        ]
    }

});
