Ext.define('ComplianceSurvey.model.util.UniqueProgram', {
    extend: 'Common.data.Model',
    config: {
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'reg_program',
                type: 'string'
            }
        ]
    }
});