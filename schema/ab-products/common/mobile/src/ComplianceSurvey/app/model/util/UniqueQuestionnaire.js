Ext.define('ComplianceSurvey.model.util.UniqueQuestionnaire', {
    extend: 'Common.data.Model',
    config: {
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'questionnaire_id_ext',
                type: 'int'
            }
        ]
    }
});