Ext.define('ComplianceSurvey.model.util.ComplianceSurveyTask', {
    extend: 'Common.data.Model',

    config: {
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'activity_log_id',
                type: 'IntegerClass'
            },
            {
                name: 'action_title',
                type: 'string'
            },
            {
                name: 'description',
                type: 'string'
            },
            {
                name: 'status',
                type: 'string'
            },
            {
                name: 'status_initial',
                type: 'string'
            },
            {
                name: 'date_scheduled',
                type: 'DateClass'
            },
            {
                name: 'date_scheduled_end',
                type: 'DateClass'
            },
            {
                name: 'date_required',
                type: 'DateClass'
            },
            {
                name: 'location_id',
                type: 'IntegerClass'
            },
            {
                name: 'questionnaire_id_ext',
                type: 'IntegerClass'
            },
            {
                name: 'pct_complete',
                type: 'IntegerClass'
            },
            {
                name: 'reg_program',
                type: 'string'
            },
            {
                name: 'project_id',
                type: 'string'
            },
            {
                name: 'regulation',
                type: 'string'
            },
            {
                name: 'priority',
                type: 'IntegerClass'
            },
            {
                name: 'site_id',
                type: 'string'
            },
            {
                name: 'bl_id',
                type: 'string'
            },
            {
                name: 'fl_id',
                type: 'string'
            },
            {
                name: 'rm_id',
                type: 'string'
            },
            {
                name: 'eq_std',
                type: 'string'
            },
            {
                name: 'eq_id',
                type: 'string'
            },
            {
                name: 'em_id',
                type: 'string'
            }
        ]


    }

});
