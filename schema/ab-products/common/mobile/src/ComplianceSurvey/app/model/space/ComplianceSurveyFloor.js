Ext.define('ComplianceSurvey.model.space.ComplianceSurveyFloor', {
    extend: 'Common.data.Model',

    config: {
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'bl_id',
                type: 'string'
            },
            {
                name: 'fl_id',
                type: 'string'
            },
            {
                name: 'name',
                type: 'string'
            }
        ]
    }
});