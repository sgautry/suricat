Ext.define('ComplianceSurvey.model.AnswerOption', {
    extend: 'Common.data.Model',

    config: {
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'question_id',
                type: 'int'
            },
            {
                name: 'answer_option_id',
                type: 'int'
            },
            {
                name: 'answer_option_text',
                type: 'string'
            },
            {
                name: 'sort_order',
                type: 'int'
            }
        ]
    }

});
