Ext.define('ComplianceSurvey.model.util.UniqueEmployeeId', {
    extend: 'Common.data.Model',
    config: {
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'em_id',
                type: 'string'
            }
        ]
    }
});