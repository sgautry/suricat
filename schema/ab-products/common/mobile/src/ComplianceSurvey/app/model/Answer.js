Ext.define('ComplianceSurvey.model.Answer', {
    extend: 'Common.data.Model',

    config: {
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'answer_id',
                type: 'IntegerClass'
            },
            {
                name: 'question_id',
                type: 'IntegerClass'
            },
            {
                name: 'questionnaire_id',
                type: 'IntegerClass'
            },
            {
                name: 'survey_event_id',
                type: 'IntegerClass'
            },
            {
                name: 'value_text',
                type: 'string'
            },
            {
                name: 'value_integer',
                type: 'IntegerClass'
            },
            {
                name: 'value_number',
                type: 'float'
            },
            {
                name: 'value_date',
                type: 'DateClass'
            },
            {
                name: 'value_time',
                type: 'TimeClass'
            },
            {
                name: 'unit',
                type: 'string'
            },
            {
                name: 'unit_type',
                type: 'string'
            },
            {
                name: 'date_recorded',
                type: 'DateClass'
            },
            {
                name: 'time_recorded',
                type: 'TimeClass'
            },
            {
                name: 'mob_is_changed',
                type: 'IntegerClass'
            },
            {
                name: 'mob_locked_by',
                type: 'string'
            },
            {
                name: 'doc',
                type: 'string',
                isDocumentField: true,
                isSyncField: true
            },
            {
                name: 'guid',
                type: 'guid'
            },
            {
                name: 'auto_number',
                type: 'int',
                isSyncField: false
            },
            {
                name: 'deleted',
                type: 'IntegerClass'
            }
        ],
        sqlIndexes: [
            {
                indexName: 'idxComplianceSurveyAnswerQuestionnaire',
                fields: ['questionnaire_id']
            },
            {
                indexName: 'idxComplianceSurveyAnswerQuestion',
                fields: ['questionnaire_id', 'question_id']
            }
        ],
        uniqueIdentifier: ['survey_event_id', 'questionnaire_id', 'question_id', 'answer_id', 'guid']
    },

    /**
     * Applies the image data to the document field
     * @param {String} documentField The name of the document field
     * @param {String} imageData The base64 encoded image data
     */
    setDocumentFieldData: function (documentField, imageData) {
        var me = this;

        me.set(documentField, documentField + '.jpg');
        me.set(documentField + '_contents', imageData);
        me.set(documentField + '_isnew', true);
        me.set('mob_is_changed', 1);
    }
});
