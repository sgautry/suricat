Ext.define('ComplianceSurvey.model.Questionnaire', {
    extend: 'Common.data.Model',

    config: {
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'questionnaire_id',
                type: 'int'
            },
            {
                name: 'questionnaire_title',
                type: 'string'
            },
            {
                name: 'description',
                type: 'string'
            },
            {
                name: 'status',
                type: 'string'
            }
        ]


    }

});
