Ext.define('ComplianceSurvey.model.QuestQuestion', {
    extend: 'Common.data.Model',

    config: {
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'questionnaire_id',
                type: 'int'
            },
            {
                name: 'question_id',
                type: 'int'
            },
            {
                name: 'question_prefix',
                type: 'string'
            },
            {
                name: 'is_required',
                type: 'int'
            },
            {
                name: 'sort_order',
                type: 'int'
            },
            {
                name: 'display_format',
                type: 'string'
            },
            {
                name: 'show_answer_text',
                type: 'int'
            }
        ]


    }

});
