Ext.define('ComplianceSurvey.model.util.UniqueRegulation', {
    extend: 'Common.data.Model',
    config: {
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'regulation',
                type: 'string'
            }
        ]
    }
});