Ext.define('ComplianceSurvey.model.ComplianceSurvey', {
    extend: 'Common.data.Model',

    config: {
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'activity_log_id',
                type: 'IntegerClass'
            },
            {
                name: 'action_title',
                type: 'string'
            },
            {
                name: 'activity_type',
                type: 'string'
            },
            {
                name: 'description',
                type: 'string'
            },
            {
                name: 'status',
                type: 'string'
            },
            {
                name: 'status_initial',
                type: 'string',
                isSyncField: false
            },
            {
                name: 'date_scheduled',
                type: 'DateClass'
            },
            {
                name: 'date_scheduled_end',
                type: 'DateClass'
            },
            {
                name: 'date_required',
                type: 'DateClass'
            },
            {
                name: 'location_id',
                type: 'IntegerClass'
            },
            {
                name: 'questionnaire_id_ext',
                type: 'IntegerClass'
            },
            {
                name: 'pct_complete',
                type: 'IntegerClass'
            },
            {
                name: 'reg_program',
                type: 'string'
            },
            {
                name: 'project_id',
                type: 'string'
            },
            {
                name: 'regulation',
                type: 'string'
            },
            {
                name: 'priority',
                type: 'IntegerClass'
            },
            {
                name: 'mob_locked_by',
                type: 'string'
            },
            {
                name: 'mob_is_changed',
                type: 'IntegerClass'
            }
        ]


    }

});
