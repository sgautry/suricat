Ext.define('ComplianceSurvey.model.space.ComplianceSurveySite', {
    extend: 'Common.data.Model',
    config: {
        uniqueIdentifier: ['site_id'],
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'site_id',
                type: 'string'
            },
            {
                name: 'name',
                type: 'string'
            },
            {
                name: 'city_id',
                type: 'string'
            },
            {
                name: 'state_id',
                type: 'string'
            },
            {
                name: 'ctry_id',
                type: 'string'
            },
            {
                name: 'detail_dwg',
                type: 'string',
                isDocumentField: true
            }
        ]
    }
});