Ext.define('ComplianceSurvey.model.util.QuestionItem', {
    extend: 'Common.data.Model',

    config: {
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'question_id',
                type: 'int'
            },
            {
                name: 'questionnaire_id',
                type: 'int'
            },
            {
                name: 'question_type',
                type: 'string'
            },
            {
                name: 'question_prefix',
                type: 'string'
            },
            {
                name: 'is_required',
                type: 'int'
            },
            {
                name: 'sort_order',
                type: 'int'
            },
            {
                name: 'num_responses_max',
                type: 'int'
            },
            {
                name: 'display_format',
                type: 'string'
            },
            {
                name: 'show_answer_text',
                type: 'int'
            },
            {
                name: 'question_text',
                type: 'string'
            },
            {
                name: 'hierarchy_ids',
                type: 'string'
            },
            {
                name: 'parent_answer_condition',
                type: 'string'
            },
            {
                name: 'is_required_child',
                type: 'int'
            },
            {
                name: 'sort_order_child',
                type: 'int'
            },
            {
                name: 'freeform_width',
                type: 'int'
            },
            {
                name: 'lookup_table',
                type: 'string'
            },
            {
                name: 'lookup_field',
                type: 'string'
            },
            {
                name: 'unit_type',
                type: 'string'
            },
            {
                name: 'unit_default',
                type: 'string'
            }
        ]
    },

    /**
     * Holds the document count for answers of type document.
     */
    documentAnswerCount: 0,

    setDocumentCount: function(docCount){
        this.documentAnswerCount = docCount;
    },

    getDocumentCount: function(){
        return this.documentAnswerCount;
    }
});
