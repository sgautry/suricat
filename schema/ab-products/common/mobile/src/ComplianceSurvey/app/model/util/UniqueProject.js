Ext.define('ComplianceSurvey.model.util.UniqueProject', {
    extend: 'Common.data.Model',
    config: {
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'project_id',
                type: 'string'
            }
        ]
    }
});