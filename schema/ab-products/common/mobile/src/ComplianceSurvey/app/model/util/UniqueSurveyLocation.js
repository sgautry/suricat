Ext.define('ComplianceSurvey.model.util.UniqueSurveyLocation', {
    extend: 'Common.data.Model',
    config: {
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'location_id',
                type: 'int'
            }
        ]
    }
});