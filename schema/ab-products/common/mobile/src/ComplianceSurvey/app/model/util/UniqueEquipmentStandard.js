Ext.define('ComplianceSurvey.model.util.UniqueEquipmentStandard', {
    extend: 'Common.data.Model',
    config: {
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'eq_std',
                type: 'string'
            }
        ]
    }
});