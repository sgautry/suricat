Ext.define('ComplianceSurvey.model.util.UniqueEquipmentId', {
    extend: 'Common.data.Model',
    config: {
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'eq_id',
                type: 'string'
            }
        ]
    }
});