Ext.define('ComplianceSurvey.model.space.ComplianceSurveyBuilding', {
    extend: 'Common.data.Model',

    config: {
        uniqueIdentifier: ['bl_id'],
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'bl_id',
                type: 'string'
            },
            {
                name: 'site_id',
                type: 'string'
            },
            {
                name: 'name',
                type: 'string'
            },
            {
                name: 'city_id',
                type: 'string'
            },
            {
                name: 'state_id',
                type: 'string'
            },
            {
                name: 'ctry_id',
                type: 'string'
            },
            {
                name: 'address1',
                type: 'string'
            },
            {
                name: 'address2',
                type: 'string'
            },
            {
                name: 'lat',
                type: 'float'
            },
            {
                name: 'lon',
                type: 'float'
            }
        ]
    }
});
