Ext.define('ComplianceSurvey.view.SurveysList', {
    extend: 'Ext.dataview.List',

    xtype: 'surveysList',

    requires: [
        'ComplianceSurvey.view.SurveysListItem'
    ],

    config: {
        scrollable: {
            direction: 'vertical',
            directionLock: true
        },

        scrollToTopOnRefresh: false,

        plugins: {
            xclass: 'Common.plugin.DataViewListPaging',
            autoPaging: false
        },

        loadingText: LocaleManager.getLocalizedString('Loading...', 'ComplianceSurvey.view.SurveysList'),

        useComponents: true,

        isNavigationList: true,

        defaultType: 'surveysListItem',

        store: 'complianceSurveyTasksStore',

        emptyText: '<div class="ab-empty-text">' +
        Ext.String.format(LocaleManager.getLocalizedString('There are no downloaded survey tasks at this time.<br>Tap the {0} icon to retrieve assigned surveys.',
            'ComplianceSurvey.view.SurveysList'), '<span class="ab-sync-icon"></span>') + '</div>',

        cls: 'component-list',

        editViewClass: 'ComplianceSurvey.view.QuestionsList',

        title: LocaleManager.getLocalizedString('My Survey Tasks', 'ComplianceSurvey.view.SurveysList'),

        items: [
            {
                xtype: 'titlepanel',
                docked: 'top'
            },
            {
                xtype: 'toolbar',
                cls: 'ab-toolbar',
                docked: 'top',
                items: [
                    {
                        xtype: 'search',
                        name: 'searchSurvey',
                        width: Ext.os.is.Phone ? '40%' : '15em'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'selectlistfield',
                        name: 'surveySortField',
                        isSortField: true,
                        valueField: 'objectValue',
                        displayField: 'displayValue',
                        width: Ext.os.is.Phone ? '40%' : '20em',
                        options: [
                            {
                                "displayValue": LocaleManager.getLocalizedString('Date Perform From',
                                    'ComplianceSurvey.view.SurveysList'),
                                "objectValue": "date_perform"
                            },
                            {
                                "displayValue": LocaleManager.getLocalizedString('Date Perform To End',
                                    'ComplianceSurvey.view.SurveysList'),
                                "objectValue": "date_perform_end"
                            },
                            {
                                "displayValue": LocaleManager.getLocalizedString('Date Required',
                                    'ComplianceSurvey.view.SurveysList'),
                                "objectValue": "date_required"
                            },
                            {
                                "displayValue": LocaleManager.getLocalizedString('Survey Title',
                                    'ComplianceSurvey.view.SurveysList'),
                                "objectValue": "survey_title"
                            },
                            {
                                "displayValue": LocaleManager.getLocalizedString('Status',
                                    'ComplianceSurvey.view.SurveysList'),
                                "objectValue": "status"
                            },
                            {
                                "displayValue": LocaleManager.getLocalizedString('Location',
                                    'ComplianceSurvey.view.SurveysList'),
                                "objectValue": "location"
                            }
                        ]
                    },
                    {
                        xtype: 'button',
                        iconCls: 'filter',
                        name: 'surveyFilterButton',
                        action: 'filterSurveysList',
                        width: '2em',
                        align: 'right',
                        cls: 'ab-icon-action'
                    }
                ]
            }
        ]
    },

    initialize: function () {
        var searchField = this.down('search'),
            sortField = this.down('selectlistfield');

        // let space for clear icon
        if (!Ext.isEmpty(searchField) && !Ext.isEmpty(searchField.down('searchfield')) && !Ext.isEmpty(searchField.down('searchfield').getComponent())) {
            searchField.down('searchfield').getComponent().setStyle('padding-right: 2em');
        }

        // let space for down arrow
        if (!Ext.isEmpty(sortField) && !Ext.isEmpty(sortField.getComponent())) {
            sortField.getComponent().setStyle('padding-right: 2.5em');
        }

        this.callParent(arguments);
    }
});