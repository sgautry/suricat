Ext.define('ComplianceSurvey.view.DocumentList', {
    extend: 'Common.view.DocumentList',

    xtype: 'complianceSurveyDocumentList',

    config: {
        toolBarButtons: [
            {
                xtype: 'camera',
                align: 'left',
                iconCls: 'camera',
                displayOn: 'all',
                appName: 'ComplianceSurvey'
            }
        ],
        enableImageRedline: false,
        enableDelete: true,
        store: 'answersStore',

        scrollable: {
            direction: 'vertical',
            directionLock: true
        },

        questionnaireRecord: null,

        questionRecord: null
    },

    processDocumentFields: function () {
        var me = this,
            store = me.getStore(),
            documents,
            enableImageRedline = me.getEnableImageRedline(),
            enableDelete = me.getEnableDelete(),
            clientTable,
            documentFolder,
            downloadDocumentsOnDemand = false,
            filterArray = QuestionnaireUtil.getAnswerFiltersForDocumentQuestions(me.getQuestionnaireRecord(), me.getQuestionRecord());

        return new Promise(function (resolve) {
            me.removeExistingItems();

            store.clearFilter();
            store.setFilters(filterArray);
            store.loadPage(1, function (answerRecords) {
                if (Ext.isEmpty(answerRecords)) {
                    resolve(answerRecords);
                } else {
                    if (Ext.isFunction(store.getIncludeDocumentDataInSync)) {
                        downloadDocumentsOnDemand = !store.getIncludeDocumentDataInSync();
                    }

                    answerRecords.forEach(function (answerRecord) {
                        documents = answerRecord.getDocumentFieldsAndData();

                        if (downloadDocumentsOnDemand) {
                            clientTable = answerRecord.getTableNameFromInstance();
                            documentFolder = GlobalParameters.getUserDocumentFolder() + '/' + clientTable;

                            documents.forEach(function (document) {
                                var documentConfig = store.getDocumentConfig(),
                                    primaryKey = me.generatePrimaryKeyObject(documentConfig.documentTableKeys, answerRecord),
                                    fileName = DocumentManager.generateFileName(documentConfig.documentTable, document.fieldName, document.file, primaryKey);

                                document.filePath = documentFolder + '/' + fileName;
                            }, me);

                            me.documentFileExists(documents)
                                .then(function (modifiedDocuments) {
                                    modifiedDocuments.forEach(function (document) {
                                        me.addDocumentItem(document, answerRecord, enableImageRedline, enableDelete, downloadDocumentsOnDemand);
                                    });
                                    resolve(answerRecords);
                                });
                        } else {
                            documents.forEach(function (document) {
                                me.addDocumentItem(document, answerRecord, enableImageRedline, enableDelete, downloadDocumentsOnDemand);
                            });
                            resolve(answerRecords);
                        }
                    }, me);
                }
            });
        });
    }
});