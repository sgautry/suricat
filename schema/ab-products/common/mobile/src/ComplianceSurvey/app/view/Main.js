Ext.define('ComplianceSurvey.view.Main', {
    extend: 'Common.view.navigation.NavigationView',

    xtype: 'main',

    config: {

        editViewClass: 'ComplianceSurvey.view.Task',

        useTitleForBackButtonText: false,

        navigationBar: {
            backButton: {
                cls: 'x-button-back'
            },

            hideSaveButtons: true
        },

        toolBarButtons: [
            {
                xtype: 'toolbarbutton',
                text: LocaleManager.getLocalizedString('Apps', 'ComplianceSurvey.view.Main'),
                cls: 'x-button-back',
                action: 'backToAppLauncher',
                displayOn: 'all'
            },
            {
                xtype: 'toolbarbutton',
                action: 'mapSurveys',
                align: 'left',
                iconCls: 'map',
                displayOn: 'all'
            }
        ],

        items: [
            {
                xtype: 'surveysList'
            }
        ]
    },

    initialize: function () {

        // Add additional toolbar buttons to the main view.
        var navBar = this.getNavigationBar();

        navBar.add({
            xtype: 'button',
            action: 'goToHomePage',
            align: 'left',
            iconCls: 'home',
            hidden: true
        });

        navBar.add(Ext.create('Ext.Button', {
            action: 'resetQuestions',
            align: 'right',
            iconCls: 'circle_cancel',
            style: 'padding-bottom: .3em',
            hidden: true
        }));

        navBar.add(Ext.create('Common.control.button.Picker', {
            action: 'showNavOptions',
            store: 'navigationOptionsStore',
            valueField: 'action',
            align: 'right',
            iconCls: 'navigatedown',
            cls: 'ab-icon-action',
            panelSize: {
                tablet: {width: '18em', height: '18em'},
                phone: {width: '14em', height: '18em'}
            },
            hidden: true
        }));

        navBar.add(Ext.create('Ext.Button', {
            action: 'barcodeFilter',
            align: 'right',
            iconCls: 'barcode',
            hidden: false
        }));

        navBar.add(Ext.create('Ext.Button', {
            action: 'syncSurvey',
            align: 'right',
            iconCls: 'refresh',
            cls: 'ab-icon-action',
            hidden: false
        }));

        navBar.add(Ext.create('Ext.Button', {
            action: 'completeSurvey',
            align: 'right',
            text: LocaleManager.getLocalizedString('Complete', 'ComplianceSurvey.view.Main'),
            ui: 'action',
            hidden: true
        }));

        this.callParent(arguments);
    }
});
