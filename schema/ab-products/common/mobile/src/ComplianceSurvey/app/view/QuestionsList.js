Ext.define('ComplianceSurvey.view.QuestionsList', {
    extend: 'Common.view.navigation.EditBase',

    xtype: 'questionsList',

    config: {
        scrollable: {
            direction: 'vertical',
            directionLock: true
        },

        storeId: 'questionItemsStore',
        model: 'ComplianceSurvey.model.util.QuestionItem',
        isNavigationEdit: false,

        title: '',

        initialTitle: '',

        questionnaireId: '',

        navigationBar: null,

        items: [],
        cls: 'ab-questions-panel'
    },

    //private
    followUpMap: [],

    numberOfParentQuestions: 0,
    numberOfAnsweredParentQuestions: 0,

    // used to prevent the change listeners to be executed
    isViewLoading: true,

    disableFieldChangeEvents: false,

    initialize: function () {
        var me = this,
            record = me.getRecord(),
            initialTitlePanel;

        if (Ext.isEmpty(record) || Ext.isEmpty(record.get('questionnaire_id_ext'))) {
            return;
        }

        me.questionnaireId = record.get('questionnaire_id_ext');

        me.setQuestionnaireTitle()
            .then(function () {
                return QuestionnaireUtil.generateFormFields(me, null, true);
            })
            .then(function () {
                QuestionnaireUtil.updatePercentageOfCompletion(me);
            });

        me.callParent(arguments);

        initialTitlePanel = me.down('titlepanel');
        me.remove(initialTitlePanel);

    },

    setQuestionnaireTitle: function () {
        var me = this,
            questionnairesStore = Ext.getStore('questionnairesStore'),
            filter = Ext.create('Common.util.Filter', {
                property: 'questionnaire_id',
                value: me.questionnaireId,
                exactMatch: true
            }),
            searchField;

        return questionnairesStore.retrieveSingleRecord([filter])
            .then(function (record) {
                if (!Ext.isEmpty(record)) {
                    me.add(Ext.factory({
                        docked: 'top',
                        title: record.get('questionnaire_title')
                    }, Common.control.TitlePanel));

                    me.initialTitle = record.get('questionnaire_title');
                }

                me.add(Ext.factory({
                    cls: 'ab-toolbar',
                    docked: 'top',
                    items: [
                        {
                            xtype: 'checkboxfield',
                            name: 'selectAllQuestions',
                            width: '2em'
                        },
                        {
                            xtype: 'search',
                            name: 'searchQuestion',
                            width: Ext.os.is.Phone ? '40%' : '15em'
                        }
                    ]
                }, 'Ext.Toolbar'));

                searchField = me.down('search');
                if (!Ext.isEmpty(searchField)) {
                    searchField.on('searchkeyup', me.searchQuestion, {scope: me});
                    searchField.on('searchclearicontap', me.searchQuestionClear, {scope: me});

                    if (!Ext.isEmpty(searchField.down('searchfield')) && !Ext.isEmpty(searchField.down('searchfield').getComponent())) {
                        searchField.down('searchfield').getComponent().addCls('ab-form-search-input');
                    }
                }

            });
    },

    searchQuestion: function (searchFieldValue) {
        var
            subFilterArray = [],
            filter = Ext.create('Common.util.Filter', {
                property: 'dummyProperty',
                value: 'dummyValue'
            });

        subFilterArray.push({
            property: 'question_text',
            value: '%' + searchFieldValue + '%',
            conjunction: 'OR'
        });

        subFilterArray.push({
            property: 'question_prefix',
            value: '%' + searchFieldValue + '%',
            conjunction: 'OR'
        });

        filter.setSubFilter(subFilterArray);

        QuestionnaireUtil.generateFormFields(this.scope, filter, false);
    },

    searchQuestionClear: function () {
        QuestionnaireUtil.generateFormFields(this.scope);
    }
});