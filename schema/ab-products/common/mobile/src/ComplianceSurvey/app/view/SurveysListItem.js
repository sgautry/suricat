Ext.define('ComplianceSurvey.view.SurveysListItem', {
    extend: 'Ext.dataview.component.ListItem',

    xtype: 'surveysListItem',

    config: {
        // reduce the space in the left of the list by overwriting the body component
        body: {
            xtype: 'component',
            style: 'padding-left: 0'
        },

        surveyInfo: {
            flex: Ext.os.is.Phone ? 7 : 5,
            cls: 'x-detail'
        },

        buttonContainer: {
            layout: 'vbox',
            flex: Ext.os.is.Phone ? 3 : 1,
            items: [
                {
                    xtype: 'button',
                    text: '',
                    itemId: 'statusBtn'
                },
                {
                    xtype: 'button',
                    iconCls: 'info',
                    itemId: 'infoBtn',
                    cls: ['ab-icon-button', 'x-button-icon-secondary'],
                    iconAlign: 'left'
                },
                {
                    xtype: 'button',
                    iconCls: 'locate',
                    itemId: 'locateBtn',
                    cls: ['ab-icon-button']
                }
            ]
        },
        cls: 'component-list-item',

        layout: {
            type: 'hbox',
            align: 'center'
        }
    },

    applySurveyInfo: function (config) {
        return Ext.factory(config, Ext.Component, this.getSurveyInfo());
    },

    updateSurveyInfo: function (newSurveyInfo, oldSurveyInfo) {
        if (newSurveyInfo) {
            this.add(newSurveyInfo);
        }
        if (oldSurveyInfo) {
            this.remove(oldSurveyInfo);
        }
    },

    applyButtonContainer: function (config) {
        return Ext.factory(config, Ext.Container, this.getButtonContainer());
    },

    updateButtonContainer: function (newContainer, oldContainer) {
        if (newContainer) {
            this.add(newContainer);
        }
        if (oldContainer) {
            this.remove(oldContainer);
        }
    },

    updateRecord: function (newRecord) {
        var me = this,
            surveyInfo = this.getSurveyInfo(),
            buttonContainer = this.getButtonContainer(),
            buttons;

        if (newRecord) {
            surveyInfo.setHtml(this.buildSurveyInfo(newRecord));

            buttons = buttonContainer.query('button');
            Ext.each(buttons, function (button) {
                button.setRecord(newRecord);
                if (button.getItemId() === 'statusBtn') {
                    me.updateStatusButton(button, newRecord);
                }
                if (button.getItemId() === 'locateBtn') {
                    me.enableLocationButton(button, newRecord);
                }
            }, this);
        }
        this.callParent(arguments);
    },

    buildSurveyInfo: function (record) {
        var html,
            actionTitle = Ext.isEmpty(record.get('action_title')) ? '' : record.get('action_title'),
            pctComplete = Ext.isEmpty(record.get('pct_complete')) ? '0' : record.get('pct_complete'),
            description = Ext.isEmpty(record.get('description')) ? '' : record.get('description'),
            dateFormat = LocaleManager.getLocalizedDateFormat(),
            formatDate = function (dateField, dateFormat) {
                return Ext.isEmpty(dateField) ? '' : Ext.DateExtras.format(dateField, dateFormat);
            },
            dateScheduled = formatDate(record.get('date_scheduled'), dateFormat),
            dateScheduledEnd = formatDate(record.get('date_scheduled_end'), dateFormat),
            dateRequired = formatDate(record.get('date_required'), dateFormat),
            locationTag;

        locationTag = this.getLocationTag(record);

        html = ['<div class="prompt-list-hbox">',
            '<h1>' + actionTitle + '&nbsp;|&nbsp;<span class="ab-percentage-indicator">' + pctComplete + '%</span></div>',
            '<div class="prompt-list-hbox">' + description + '</div>',
            '<div class="prompt-list-hbox"><span class="prompt-list-date">',
            LocaleManager.getLocalizedString('Scheduled for:', 'ComplianceSurvey.view.SurveysListItem'),
            dateScheduled + ' - ' + dateScheduledEnd + '</span></div>',
            '<div class="prompt-list-hbox"><span class="prompt-list-date">',
            LocaleManager.getLocalizedString('Required by:', 'ComplianceSurvey.view.SurveysListItem'),
            dateRequired + '</span></div>',
            locationTag]
            .join('');

        return html;
    },

    getLocationTag: function (record) {
        var siteId = Ext.isEmpty(record.get('site_id')) ? '' : record.get('site_id'),
            blId = Ext.isEmpty(record.get('bl_id')) ? '' : record.get('bl_id'),
            flId = Ext.isEmpty(record.get('fl_id')) ? '' : record.get('fl_id'),
            rmId = Ext.isEmpty(record.get('rm_id')) ? '' : record.get('rm_id'),
            locationTag = '';

        if (!Ext.isEmpty(siteId)) {
            locationTag = siteId;
        }
        if (!Ext.isEmpty(blId)) {
            if (Ext.isEmpty(locationTag)) {
                locationTag = blId;
            } else {
                locationTag += ' | ' + blId;
            }

            if (!Ext.isEmpty(flId)) {
                locationTag += ' | ' + flId;

                if (!Ext.isEmpty(rmId)) {
                    locationTag += ' | ' + rmId;
                }
            }
        }
        if (!Ext.isEmpty(locationTag)) {
            locationTag = '<div class="prompt-list-hbox">' + locationTag + '</div>';
        }

        return locationTag;
    },

    updateStatusButton: function (button, record) {
        var calculatedStatus = SurveyUtil.calculateSurveyStatus(record);

        button.setCls(SurveyUtil.getCalculatedStatusColorClass(calculatedStatus));
        button.setText(SurveyUtil.getCalculatedStatusText(calculatedStatus));
    },

    /**
     * Grey out locate button when site plan or floor plan can not be displayed.
     */
    enableLocationButton: function (button, record) {
        var enable = false,
            siteId = record.get('site_id'),
            blId = record.get('bl_id'),
            flId = record.get('fl_id');

        if (!Ext.isEmpty(siteId) || (!Ext.isEmpty(blId) && !Ext.isEmpty(flId))) {
            enable = true;
        }

        if (enable) {
            button.addCls('x-button-icon-secondary');
            button.removeCls('x-button-icon-grey');
        } else {
            button.addCls('x-button-icon-grey');
            button.removeCls('x-button-icon-secondary');
        }

    }
});