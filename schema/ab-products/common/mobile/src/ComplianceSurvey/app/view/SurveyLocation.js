Ext.define('ComplianceSurvey.view.SurveyLocation', {
    extend: 'Ext.Container',

    xtype: 'surveyLocation',

    config: {
        title: LocaleManager.getLocalizedString('Survey Task Location', 'ComplianceSurvey.view.SurveyLocation'),

        layout: 'vbox',

        items: [
            {
                xtype: 'siteMapPanel',
                hidden: true,
                flex: 1
            },
            {
                xtype: 'floorPlanPanel',
                flex: 1
            },
            {
                xtype: 'toolbar',
                docked: 'bottom',
                items: [
                    {
                        xtype: 'segmentedbutton',
                        centered: true,
                        width: Ext.os.is.Phone ? '90%' : '50%',
                        defaults: {
                            width: '50%',
                            labelWidth: '100%'
                        },
                        items: [
                            {
                                text: LocaleManager.getLocalizedString('Map', 'ComplianceSurvey.view.SurveyLocation'),
                                itemId: 'siteMapBtn'

                            },
                            {
                                text: LocaleManager.getLocalizedString('Floor Plan', 'ComplianceSurvey.view.SurveyLocation'),
                                itemId: 'floorPlanBtn',
                                pressed: true
                            }
                        ]
                    }
                ]
            }
        ]
    }
});