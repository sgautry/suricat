Ext.define('ComplianceSurvey.view.Filter', {
    extend: 'Common.view.navigation.FilterForm',

    xtype: 'filterPanel',

    config: {
        layout: 'vbox',

        items: [
            {
                xtype: 'titlepanel',
                title: LocaleManager.getLocalizedString('Set Filter', 'ComplianceSurvey.view.Filter')
            },
            {
                xtype: 'fieldset',
                defaults: {
                    labelWrap: true,
                    labelCls: Ext.os.is.Phone ? 'x-form-label-phone' : '',
                    labelWidth: '40%'
                },
                items: [
                    {
                        xtype: 'calendarfield',
                        name: 'date_scheduled',
                        label: LocaleManager.getLocalizedString('Date to Perform From', 'ComplianceSurvey.view.Filter')
                    },
                    {
                        xtype: 'calendarfield',
                        name: 'date_scheduled_end',
                        label: LocaleManager.getLocalizedString('Date to Perform To', 'ComplianceSurvey.view.Filter')
                    },
                    {
                        xtype: 'calendarfield',
                        name: 'date_required',
                        label: LocaleManager.getLocalizedString('Date Required', 'ComplianceSurvey.view.Filter')
                    },
                    {
                        xtype: 'selectlistfield',
                        name: 'status',
                        label: LocaleManager.getLocalizedString('Status', 'ComplianceSurvey.view.Filter'),
                        valueField: 'objectValue',
                        displayField: 'displayValue',
                        value: 'ALL'
                    },
                    {
                        xtype: 'prompt',
                        name: 'site_id',
                        label: LocaleManager.getLocalizedString('Site Code', 'ComplianceSurvey.view.Filter'),
                        title: LocaleManager.getLocalizedString('Sites', 'ComplianceSurvey.view.Filter'),
                        store: 'complianceSurveySites',
                        displayFields: [
                            {
                                name: 'site_id',
                                title: LocaleManager.getLocalizedString('Site Code', 'ComplianceSurvey.view.Filter')
                            },
                            {
                                name: 'name',
                                title: LocaleManager.getLocalizedString('Site Name', 'ComplianceSurvey.view.Filter')
                            }
                        ]
                    },
                    {
                        xtype: 'prompt',
                        name: 'bl_id',
                        label: LocaleManager.getLocalizedString('Building Code', 'ComplianceSurvey.view.Filter'),
                        title: LocaleManager.getLocalizedString('Buildings', 'ComplianceSurvey.view.Filter'),
                        store: 'complianceSurveyBuildings',
                        displayFields: [
                            {
                                name: 'site_id',
                                title: LocaleManager.getLocalizedString('Site Code', 'ComplianceSurvey.view.Filter')
                            },
                            {
                                name: 'bl_id',
                                title: LocaleManager.getLocalizedString('Building Code', 'ComplianceSurvey.view.Filter')
                            },
                            {
                                name: 'name',
                                title: LocaleManager.getLocalizedString('Building Name', 'ComplianceSurvey.view.Filter')
                            }
                        ],
                        parentFields: ['site_id']
                    },
                    {
                        xtype: 'prompt',
                        name: 'fl_id',
                        label: LocaleManager.getLocalizedString('Floor Code', 'ComplianceSurvey.view.Filter'),
                        title: LocaleManager.getLocalizedString('Floors', 'ComplianceSurvey.view.Filter'),
                        store: 'complianceSurveyFloors',
                        displayFields: [
                            {
                                name: 'bl_id',
                                title: LocaleManager.getLocalizedString('Building Code', 'ComplianceSurvey.view.Filter')
                            },
                            {
                                name: 'fl_id',
                                title: LocaleManager.getLocalizedString('Floor Code', 'ComplianceSurvey.view.Filter')
                            },
                            {
                                name: 'name',
                                title: LocaleManager.getLocalizedString('Building Name', 'ComplianceSurvey.view.Filter')
                            }
                        ],
                        parentFields: ['bl_id']
                    },
                    {
                        xtype: 'prompt',
                        name: 'rm_id',
                        label: LocaleManager.getLocalizedString('Room Code', 'ComplianceSurvey.view.Filter'),
                        title: LocaleManager.getLocalizedString('Rooms', 'ComplianceSurvey.view.Filter'),
                        store: 'complianceSurveyRooms',
                        displayFields: [
                            {
                                name: 'bl_id',
                                title: LocaleManager.getLocalizedString('Building Code', 'ComplianceSurvey.view.Filter')
                            },
                            {
                                name: 'fl_id',
                                title: LocaleManager.getLocalizedString('Floor Code', 'ComplianceSurvey.view.Filter')
                            },
                            {
                                name: 'rm_id',
                                title: LocaleManager.getLocalizedString('Room Code', 'ComplianceSurvey.view.Filter')
                            },
                            {
                                name: 'name',
                                title: LocaleManager.getLocalizedString('Building Name', 'ComplianceSurvey.view.Filter')
                            }
                        ],
                        parentFields: ['bl_id', 'fl_id']
                    },
                    {
                        xtype: 'prompt',
                        name: 'reg_program',
                        label: LocaleManager.getLocalizedString('Program', 'ComplianceSurvey.view.Filter'),
                        title: LocaleManager.getLocalizedString('Programs', 'ComplianceSurvey.view.Filter'),
                        store: 'uniquePrograms',
                        displayFields: [
                            {
                                name: 'reg_program',
                                title: LocaleManager.getLocalizedString('Program', 'ComplianceSurvey.view.Filter')
                            }
                        ]
                    },
                    {
                        xtype: 'prompt',
                        name: 'project_id',
                        label: LocaleManager.getLocalizedString('Project', 'ComplianceSurvey.view.Filter'),
                        title: LocaleManager.getLocalizedString('Projects', 'ComplianceSurvey.view.Filter'),
                        store: 'uniqueProjects',
                        displayFields: [
                            {
                                name: 'project_id',
                                title: LocaleManager.getLocalizedString('Project', 'ComplianceSurvey.view.Filter')
                            }
                        ]
                    },
                    {
                        xtype: 'prompt',
                        name: 'regulation',
                        label: LocaleManager.getLocalizedString('Regulation', 'ComplianceSurvey.view.Filter'),
                        title: LocaleManager.getLocalizedString('Regulations', 'ComplianceSurvey.view.Filter'),
                        store: 'uniqueRegulations',
                        displayFields: [
                            {
                                name: 'regulation',
                                title: LocaleManager.getLocalizedString('Regulation', 'ComplianceSurvey.view.Filter')
                            }
                        ]
                    },
                    {
                        xtype: 'prompt',
                        name: 'eq_std',
                        label: LocaleManager.getLocalizedString('Equipment Standard', 'ComplianceSurvey.view.Filter'),
                        title: LocaleManager.getLocalizedString('Equipment Standards', 'ComplianceSurvey.view.Filter'),
                        store: 'uniqueEquipmentStandards',
                        displayFields: [
                            {
                                name: 'eq_std',
                                title: LocaleManager.getLocalizedString('Equipment Standard', 'ComplianceSurvey.view.Filter')
                            }
                        ]
                    },
                    {
                        xtype: 'prompt',
                        name: 'eq_id',
                        label: LocaleManager.getLocalizedString('Equipment Code', 'ComplianceSurvey.view.Filter'),
                        title: LocaleManager.getLocalizedString('Equipment Codes', 'ComplianceSurvey.view.Filter'),
                        store: 'uniqueEquipmentIds',
                        enableBarcodeScanning: true,
                        displayFields: [
                            {
                                name: 'eq_id',
                                title: LocaleManager.getLocalizedString('Equipment Code', 'ComplianceSurvey.view.Filter')
                            }
                        ]
                    },
                    {
                        xtype: 'prompt',
                        name: 'em_id',
                        label: LocaleManager.getLocalizedString('Employee', 'ComplianceSurvey.view.Filter'),
                        title: LocaleManager.getLocalizedString('Employees', 'ComplianceSurvey.view.Filter'),
                        store: 'uniqueEmployeeIds',
                        displayFields: [
                            {
                                name: 'em_id',
                                title: LocaleManager.getLocalizedString('Employee', 'ComplianceSurvey.view.Filter')
                            }
                        ]
                    },
                    {
                        xtype: 'selectlistfield',
                        name: 'priority',
                        label: LocaleManager.getLocalizedString('Priority', 'ComplianceSurvey.view.Filter'),
                        valueField: 'objectValue',
                        displayField: 'displayValue',
                        value: 'ALL'
                    }
                ]
            }
        ],

        localizedAllValue: LocaleManager.getLocalizedString('All', 'ComplianceSurvey.view.Filter')
    },

    initialize: function () {
        this.setSurveyCalculatedStatusList();
        this.setEnumerationLists();
        this.callParent();
    },

    /*
     * Set option values for enum list fields.
     */
    setEnumerationLists: function () {
        var me = this,
            fieldNames = ['priority'];

        Ext.each(fieldNames, function (fieldName) {
            var options = [],
                fieldEnumList = TableDef.getEnumeratedList('activity_log_sync', fieldName);

            options.push({displayValue: me.getLocalizedAllValue(), objectValue: "ALL"});

            if (fieldEnumList && fieldEnumList.length > 0) {
                options = options.concat(fieldEnumList);
            }
            me.query('selectfield[name=' + fieldName + ']')[0].setOptions(options);
        });
    },

    /*
     * Set option values for status field. They are calculated values instead of the status field's enumlist.
     */
    setSurveyCalculatedStatusList: function () {
        var me = this,
            statusField = me.down('selectfield[name=status]'),
            options = [];

        options.push({displayValue: me.getLocalizedAllValue(), objectValue: "ALL"});
        Ext.Array.each(SurveyUtil.surveyCalculatedStatusMap, function (item) {
            options.push({
                displayValue: item.text,
                objectValue: item.status
            });
        });

        statusField.setOptions(options);
    },

    /**
     * @override Clear also the dates (instead of reseting them).
     */
    clearFilter: function () {
        var me = this,
            promptFields = me.query('prompt'),
            dateFields = me.query('calendarfield');

        // Reset prompt fields using setValue to clear keyValue as well. Field reset sets value '' on the promptInput component, not on the field directly.
        Ext.each(promptFields, function (field) {
            field.setValue('');
        });

        Ext.each(dateFields, function (field) {
            field.setValue('');
        });

        me.reset();
        me.addFiltersToHistory();

        me.fireEvent('clearFilter', me);
    }
});
