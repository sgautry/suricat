Ext.define('ComplianceSurvey.view.SiteMap', {
    extend: 'Floorplan.view.FloorPlan',

    xtype: 'siteMapPanel',

    config: {
        width: '100%',
        height: '100%',
        items: [
            {
                xtype: 'svgcomponent',
                name: 'siteMapComponent'
            }
        ]
    }
});