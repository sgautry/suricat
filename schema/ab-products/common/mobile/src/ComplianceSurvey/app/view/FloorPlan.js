Ext.define('ComplianceSurvey.view.FloorPlan', {
    extend: 'Floorplan.view.FloorPlan',

    xtype: 'floorPlanPanel',

    config: {
        width: '100%',
        height: '100%',

        items: [
            {
                xtype: 'svgcomponent',
                name: 'floorPlanComponent'
            }
        ]

    }

});