Ext.define('ComplianceSurvey.view.SurveysMap', {
    extend: 'Ext.Container',

    xtype: 'surveysMap',

    config: {
        items: [
            {
                xtype: 'esrimap',
                style: 'height:100%;width:100%'
            },
            {
                xtype: 'toolbar',
                cls: 'ab-toolbar',
                docked: 'top',
                title: LocaleManager.getLocalizedString('My Survey Tasks Buildings', 'ComplianceSurvey.view.SurveysMap'),
                items: [
                    {
                        xtype: 'button',
                        ui: 'back',
                        action: 'closeSurveysMap'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'button',
                        iconCls: 'filter',
                        action: 'filterSurveysList',
                        width: '2em',
                        align: 'right',
                        cls: 'ab-icon-action'
                    }
                ]
            }
        ]
    },

    initialize: function () {
        var me = this;

        me.configureMarkers();

        me.configureEventListeners();
    },

    configureEventListeners: function () {
        var me = this,
            abMap = me.down('esrimap');

        abMap.on('markerClick', me.onMarkerClick, me);
    },

    configureMarkers: function () {
        var me = this,
            abMap = me.down('esrimap');

        // configure the store
        var store = 'complianceSurveyBuildings',
            keyFields = ['bl_id'],
            geometryFields = ['lon', 'lat'],
            titleField = 'name',
            contentFields = ['address1', 'city_id', 'state_id', 'ctry_id'];

        // configure the marker options
        var markerOptions = {
            // optional
            renderer: 'simple',
            radius: 8,
            fillColor: '#e41a1c',
            fillOpacity: 0.90,
            stroke: true,
            strokeColor: '#fff',
            strokeWeight: 1.0
        };

        // cteate the markers
        abMap.createMarkers(
            store,
            keyFields,
            geometryFields,
            titleField,
            contentFields,
            markerOptions
        );
    },

    /**
     * Called from Navigation controller after setting buildings store filters.
     */
    showMarkers: function () {
        var me = this,
            abMap = me.down('esrimap'),
            buildingsStore = Ext.getStore('complianceSurveyBuildings'),
            filters = buildingsStore.getFilters();

        // show the markers only when surveys are displayed in the list
        if (!Ext.isEmpty(filters)) {
            abMap.showMarkers('complianceSurveyBuildings', filters);
        }
    }
});