Ext.define('ComplianceSurvey.view.ViewSelector', {
    extend: 'Common.view.navigation.ViewSelector',

    xtype: 'complianceSurveyViewSelector',

    config: {
        /**
         * @cfg {Object} questionRecord
         */
        questionRecord: null
    },

    /**
     * @override
     *
     * Displays the view assigned to the Common.control.button.ViewSelection view property.
     * Uses default logic to determine how the view is displayed. Override this method to provide
     * application specific logic.
     * @param {Common.control.button.ViewSelection} button
     */
    doDisplayView: function (button) {
        var me = this,
            panelRecord = null,
            store = button.getStore(),
            storeCount = 0,
            viewXtype,
            isCreateView,
            panel,
            isDocumentSelect = button.getDocumentSelect();

        // The selection timer prevents the view selection button from registering
        // multiple taps. This ensures only one instance of the view is displayed.
        me.startChildViewSelectionTimer();
        if (!me.enableChildViewSelection) {
            return;
        }
        me.enableChildViewSelection = false;

        if (store) {
            storeCount = store.getCount();
        }

        if (storeCount === 0) {
            viewXtype = me.getButtonViewToDisplay(button, 'edit');
            isCreateView = true;
        } else {
            viewXtype = me.getButtonViewToDisplay(button, 'list');
            isCreateView = false;
        }

        var navView = this.getNavigationView();

        // We need to get the record containing the document data to pass it to
        // the document view.
        if (isDocumentSelect) {
            panel = me.up('formpanel');
            if (panel) {
                panelRecord = panel.getRecord();
            }
        }

        if (isDocumentSelect) {
            navView.push({
                xtype: viewXtype,
                isCreateView: isCreateView,
                record: me.getQuestionRecord(),
                questionnaireRecord: panelRecord,
                questionRecord: me.getQuestionRecord(),
                callerButton: button
            });
        } else {
            navView.push({
                xtype: viewXtype,
                isCreateView: isCreateView
            });
        }
    }
});
