Ext.Loader.setPath({
    'Ext': '../touch/src',
    'Common': '../Common',
    'ComplianceSurvey': 'app',
    'Space': '../packages/Space/src',
    'Floorplan': '../packages/Floorplan/src',
    'Map': '../packages/Map/src'
});

Ext.require(['Common.scripts.ApplicationLoader', 'Common.Application', 'Common.lang.ComponentLocalizer', 'Common.lang.LocalizedStrings'], function () {

    Ext.application({
        name: 'ComplianceSurvey',

        requires: [
            'Common.control.field.Prompt',
            'Common.control.Search',
            'Common.control.Select',
            'Common.control.Spinner',
            'Common.control.TitlePanel',
            'Common.control.field.Calendar',
            'Common.control.field.Time',
            'Common.control.config.TimePicker',
            'Common.control.field.Measurement',
            'Common.control.field.TextArea',
            'Common.control.button.Picker',
            'Common.control.question.Label',
            'Common.control.question.Check',
            'Common.control.question.Radio',
            'Common.control.question.Document',
            'Common.control.field.MultipleField',
            'Common.plugin.DataViewListPaging',
            'Common.util.Filter',
            'Common.util.SqlFilter',
            'Common.util.RoomHighlightHelper',
            'Common.service.workflow.Workflow',
            'Common.store.proxy.SqliteView',
            'Space.view.report.Configuration',
            'Space.SpaceDownload',
            'Space.Space',
            'Map.component.SimpleMarker',
            'Map.component.EsriMap',
            'ComplianceSurvey.util.Questionnaire',
            'ComplianceSurvey.util.Survey'
        ],

        stores: [
            'Common.store.Apps',
            'ComplianceSurveys',
            'ComplianceLocations',
            'Questionnaires',
            'QuestQuestions',
            'Questions',
            'AnswerOptions',
            'Answers',
            'NavigationOptions',
            'ComplianceSurvey.store.util.UniqueSurveyLocations',
            'ComplianceSurvey.store.util.UniqueQuestionnaires',
            'ComplianceSurvey.store.util.UniquePrograms',
            'ComplianceSurvey.store.util.UniqueProjects',
            'ComplianceSurvey.store.util.UniqueRegulations',
            'ComplianceSurvey.store.util.UniqueEquipmentStandards',
            'ComplianceSurvey.store.util.UniqueEquipmentIds',
            'ComplianceSurvey.store.util.UniqueEmployeeIds',
            'ComplianceSurvey.store.util.ComplianceSurveyTasks',
            'ComplianceSurvey.store.util.QuestionItems',
            'ComplianceSurvey.store.space.ComplianceSurveySites',
            'ComplianceSurvey.store.space.ComplianceSurveyBuildings',
            'ComplianceSurvey.store.space.ComplianceSurveyFloors',
            'ComplianceSurvey.store.space.ComplianceSurveyRooms',
            'ComplianceSurvey.store.space.RoomsInfoReport',
            'Common.store.RoomStandards',
            'Floorplan.store.SiteDrawings',
            'Floorplan.store.PublishDates'
        ],

        views: [
            'Main',
            'SurveysList',
            'Filter',
            'SurveysMap',
            'SurveyLocation',
            'SiteMap',
            'FloorPlan',
            'QuestionsList',
            'DocumentList',
            'ComplianceSurvey.view.ViewSelector'
        ],

        controllers: [
            'Common.controller.AppHomeController',
            'Common.controller.Registration',
            'Navigation',
            'ComplianceSurveySync',
            'Documents'
        ],

        launch: function () {
            // Initialize the main view
            Ext.Viewport.add(Ext.create('ComplianceSurvey.view.Main'));
        }
    });
});
