Ext.define('AssetReceipt.control.SearchScanField', {
    extend: 'Ext.Container',

    xtype: 'searchScanField',

    config: {

        layout: {
            type: 'hbox',
            pack: 'start',
            align: 'center'
        },


        items: [
            {
                xtype: 'search',
                placeHolder: '',
                flex: 10
            },
            {
                xtype: 'iconbutton',
                action: 'search',
                iconCls: 'search',
                cls: 'ab-icon-action',
                align: 'left',
                style: 'max-width:2.5em',
                flex: 1
            }
        ]

    },

    initialize: function () {
        var me = this,
            barcodeIcon = me.down('iconbutton[itemId=barcodeIcon]'),
            historyIcon = me.down('iconbutton[itemId=historyIcon]'),
            textField = me.down('textfield'),
            searchField = me.down('search > searchfield');

        me.callParent(arguments);

        // when the search field is displayed in the toolbar it has padding-right,
        // and since it is not diplayed toolbar it needs padding-right to avoid overlapping the barcode icon
        textField.setStyle('padding-right:0.5em');

        // when input text has value (is clearable) it has padding-right and we need to overwrite that to keep field's size
        textField.getComponent().setStyle('padding-right: 0em');

        // display the barcode icon as an action button (orange)
        barcodeIcon.addCls('ab-icon-action');

        searchField.setFlex(8);
        barcodeIcon.setFlex(1);
        barcodeIcon.setStyle('min-width:2.8em;max-width:2.8em;margin:0 0.1em 0 0.1em');
        historyIcon.setFlex(1);
        historyIcon.setStyle('min-width:2.8em;max-width:2.8em;margin:0 0.1em 0 0.1em');
    },

    getValue: function () {
        return this.down('search').getValue();
    },

    setDisabled: function (isDisabled) {
        this.down('search').setReadOnly(isDisabled);
        this.down('iconbutton[action=search]').setDisabled(isDisabled);
    }
});