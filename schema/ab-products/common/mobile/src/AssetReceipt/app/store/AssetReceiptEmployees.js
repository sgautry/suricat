Ext.define('AssetReceipt.store.AssetReceiptEmployees', {
    extend: 'Common.store.sync.ValidatingTableStore',
    requires: 'AssetReceipt.model.AssetReceiptEmployee',

    serverTableName: 'em',

    serverFieldNames: ['em_id', 'bl_id', 'fl_id', 'rm_id'],
    inventoryKeyNames: ['em_id'],

    config: {
        model: 'AssetReceipt.model.AssetReceiptEmployee',
        storeId: 'assetReceiptEmployees',
        remoteSort: true,
        remoteFilter: true,
        sorters: [
            {
                property: 'em_id',
                direction: 'ASC'
            }
        ],
        tableDisplayName: LocaleManager.getLocalizedString('Employees', 'AssetReceipt.store.AssetReceiptEmployees'),
        enableAutoLoad: true,
        autoLoad: false,
        proxy: {
            type: 'Sqlite'
        },
        timestampDownload: false
    },

    /**
     * @private
     * @param restriction
     * @returns {*}
     */
    importRecords: function (restriction, deleteAllRecordsOnSync, timestamp) {
        var me = this,
            resolveFunc,
            rejectFunc,
            pageSize = me.getSyncRecordsPageSize(),
            storeRestriction = me.getRestriction(),
            numberOfImportedRecords = pageSize,
            proxy = me.getProxy(),
            table = proxy.getTable(),
            columns = proxy.getColumns(),
            pagingKeys = {};

        var p = new Promise(function (resolve, reject) {
            resolveFunc = resolve;
            rejectFunc = reject;
        });

        var doImportRecords = function () {
            if (numberOfImportedRecords === 0 || numberOfImportedRecords < pageSize) {
                resolveFunc();
            } else {
                me.retrieveRecords(storeRestriction, pageSize, pagingKeys)
                    .then(function (records) {
                        numberOfImportedRecords = records.length;
                        // Get the new paging keys from the last record.
                        pagingKeys = me.getPagingKeysFromLastRecord(records);
                        return me.convertRecordsFromServer(records);
                    }, function (error) {  // retrieveRecords error handler
                        numberOfImportedRecords = 0;  // End recursion
                        rejectFunc(error);
                    })
                    .then(function (convertedRecords) {
                        return me.insertRecords(convertedRecords, table, columns, me.getModel(), deleteAllRecordsOnSync);
                    })
                    .then(null, function (error) {
                        numberOfImportedRecords = 0;  // End recursion
                        rejectFunc(error);
                    })
                    .then(function () {
                        doImportRecords();
                    });
            }
        };

        doImportRecords();

        return p;
    },

    retrieveRecords: function (restriction, pageSize, keys) {
        var me = this,
            fieldDefFieldNames = me.getFieldNamesFromTableDef(),
            fieldsToSync = Ext.Array.intersect(fieldDefFieldNames, me.serverFieldNames);

        return Common.service.MobileSyncServiceAdapter.retrievePagedRecordsByKey(me.serverTableName, fieldsToSync, restriction, pageSize, keys);

    },

    getPagingKeysFromLastRecord: function (records) {
        var me = this,
            lastRecord,
            primaryKeyFields,
            primaryKeyValues;

        if (records.length === 0) {
            return [];
        }

        lastRecord = records[records.length - 1];
        primaryKeyFields = Common.util.TableDef.getPrimaryKeyFieldsFromTableDef(me.tableDef);

        primaryKeyValues = me.getPrimaryKeyValuesFromFieldObject(lastRecord, primaryKeyFields);
        return primaryKeyValues;
    }
});