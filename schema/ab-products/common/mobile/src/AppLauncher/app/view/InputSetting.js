Ext.define('AppLauncher.view.InputSetting', {
    extend: 'Common.form.FormPanel',

    xtype: 'inputsetting',

    config: {
        height: '100%',
        items: [
            {
                xtype: 'radiofield',
                name: 'input',
                label: LocaleManager.getLocalizedString('Mouse','AppLauncher.view.InputSetting'),
                value: 'mouse'
            },
            {
                xtype: 'radiofield',
                name: 'input',
                label: LocaleManager.getLocalizedString('Touch Only','AppLauncher.view.InputSetting'),
                value: 'touch'
            },
            {
                xtype: 'container',
                height: '80px',
                layout: {
                    type: 'hbox',
                    pack: 'center',
                    align: 'center'
                },
                items: [
                    {
                        xtype: 'button',
                        text: LocaleManager.getLocalizedString('Apply', 'AppLauncher.view.InputSetting'),
                        minWidth: Ext.os.is.Phone ? '300px' : '400px',
                        ui: 'action',
                        centered: true,
                        itemId: 'applyButton'
                    }
                ]
            }
        ]
    }
}); 