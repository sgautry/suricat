Ext.Loader.setPath({
    'Ext': '../touch/src',
    'Common': '../Common',
    'AppLauncher': 'app'
});


Ext.require(['Common.scripts.ApplicationLoader', 'Common.Application', 'Common.lang.ComponentLocalizer', 'Common.lang.LocalizedStrings'], function () {

    var inputType = localStorage.getItem('INPUT_TYPE');

    if(inputType && inputType === 'touch' && Common.env.Feature.hasTouch) {
        Ext.ClassManager.onCreated(function () {
            Ext.event.publisher.TouchGesture.override({
                handledEvents: ['touchstart', 'touchmove', 'touchend', 'touchcancel']
            });
        }, null, 'Ext.event.publisher.TouchGesture');
    }

    Ext.application({
        requires: [
            'Common.control.MessageBox',
            'AppLauncher.ui.IconData',
            'Ext.field.Toggle',
            'Common.control.field.Number',
            'Common.util.UserProfile'
        ],

        models: [
            'Common.model.App'
        ],

        stores: [
            'Common.store.Apps',
            'Common.store.Messages',
            'Preferences'
        ],

        views: [
            'Common.view.registration.RestartDevice',
            'Preferences',
            'AppItem',
            'AppList',
            'AppContainer',
            'AppLauncher.view.Log',
            'Setting',
            'WebCentral',
            'SyncSettings',
            'UserSettings',
            'LoggingSettings',
            'Version'
        ],

        controllers: [
            'Registration',
            'Version'
        ],

        profiles: ['Phone', 'Tablet'],


        name: 'AppLauncher'

    });
});
