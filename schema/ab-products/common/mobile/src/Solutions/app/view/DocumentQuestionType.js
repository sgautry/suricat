Ext.define('Solutions.view.DocumentQuestionType',{
    extend: 'Common.view.navigation.NavigationView',

    xtype: 'documentQuestionType',

    requires: 'Common.control.question.Document',

    config: {
        navigationBar: {
            hideSaveButtons: true
        },

        items: [
            {
                xtype: 'documentquestion',
                text: 'Please provide a photo',
                viewSelectorConfig: {
                    itemId: 'documentQuestionViewSelector',
                    navigationView: 'documentQuestionType',
                    items: [
                        {
                            text: 'Photo',

                            // The store that contains the records for the view to be displayed.
                            // The number of items in the store is used to update the button badge text.
                            store: 'selectorExampleStore',

                            // The xtype of view to be displayed when the button is tapped.
                            // An object can be used to specify different views for list and edit forms.
                            view: 'documentQuestionTypePhoto',

                            // True if this button selects a document view.
                            // Documents are typically stored in the parent view record.
                            documentSelect: true
                        }
                    ]
                }
            }
        ]
    }
});