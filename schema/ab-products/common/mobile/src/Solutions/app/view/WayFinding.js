Ext.define('Solutions.view.WayFinding', {
    extend: 'Ext.Container',

    requires: 'Map.util.Map',

    config: {
        items: [
            {
                xtype: 'esrimap',
                style: 'height:100%;width:100%'
            }
        ]
    },


    initialize: function () {
        var me = this,
            map = me.down('esrimap'),
            greenIcon = Map.util.Map.getLeafletIcon('green'),
            redIcon = Map.util.Map.getLeafletIcon('red');

        map.on('esrimappainted', function (esrimap) {
            var bosmed = {lat: 42.35842801, lon: -71.05976637, icon: greenIcon},
                bosoff = {lat: 42.34928300, lon: -71.07525600, icon: redIcon},
                bosmedMarker = L.marker([bosmed.lat, bosmed.lon], {icon: bosmed.icon}),
                bosoffMarker = L.marker([bosoff.lat, bosoff.lon], {icon: bosoff.icon}),

            featureGroup;

            bosmedMarker.bindPopup('Boston Medical Center');
            bosoffMarker.bindPopup('Boston Office');

            featureGroup = L.featureGroup([bosmedMarker, bosoffMarker]);

            featureGroup.addTo(esrimap.map);

            esrimap.map.fitBounds(featureGroup.getBounds());

            bosmedMarker.openPopup();
           
        });
    }
});