Ext.define('Solutions.view.QuestionTypes', {
    extend: 'Ext.form.Panel',

    requires: ['Common.control.field.Radio',
        'Common.control.question.Label',
        'Common.control.question.Check',
        'Common.control.field.Checkbox',
        'Common.control.field.Time',
        'Common.control.field.Measurement'
    ],

    config: {
        items: [
            {
                xtype: 'label',
                html: "<h2 class='ab-label-type-question'>Label Question: Section 1</h2>"
            },
            {
                xtype: 'commontextfield',
                label: 'Text Question:',
                labelAlign: 'top'
            },
            {
                xtype: 'label',
                html: "<h2 class='ab-label-type-question'>Label Question: Section 2</h2>"
            },
            {
                xtype: 'radioquestion',
                text: 'Radio Question 1',
                layout: 'hbox',
                options: [
                    {
                        value: 'yes',
                        label: 'Yes'
                    },
                    {
                        value: 'no',
                        label: 'No'
                    }
                ]
            },
            {
                xtype: 'radioquestion',
                text: 'Radio Question 2',
                options: [
                    {
                        value: 'red',
                        label: 'Red'
                    },
                    {
                        value: 'green',
                        label: 'Green'
                    },
                    {
                        value: 'blue',
                        label: 'Blue'
                    }
                ]
            },
            {
                xtype: 'checkquestion',
                text: 'Check Question 1',
                layout: 'hbox',
                options: [
                    {
                        value: 'value1',
                        label: 'Value 1'
                    },
                    {
                        value: 'value2',
                        label: 'Value 2'
                    },
                    {
                        value: 'value3',
                        label: 'Value 3'
                    }
                ]
            },
            {
                xtype: 'checkquestion',
                text: 'Check Question 2',
                options: [
                    {
                        value: 'red',
                        label: 'Red'
                    },
                    {
                        value: 'green',
                        label: 'Green'
                    },
                    {
                        value: 'blue',
                        label: 'Blue'
                    }
                ]
            },
            {
                xtype: 'labelquestion',
                text: "Label Question: Section 3",
                options: [
                    { html: 'Poor' },
                    { html: 'Fair' },
                    { html: 'Good' },
                    { html: 'Very Good' },
                    { html: 'Excellent' }
                ]
            },
            {
                xtype: 'radioquestion',
                text: 'Radio Question 1',
                layout: 'hbox',
                options: [
                    { value: 'Poor' },
                    { value: 'Fair' },
                    { value: 'Good' },
                    { value: 'Very Good' },
                    { value: 'Excellent' }
                ]
            },
            {
                xtype: 'radioquestion',
                text: 'Radio Question 2',
                layout: 'hbox',
                options: [
                    { value: 'Poor' },
                    { value: 'Fair' },
                    { value: 'Good' },
                    { value: 'Very Good' },
                    { value: 'Excellent' }
                ]
            },
            {
                xtype: 'radioquestion',
                text: 'Radio Question 3',
                layout: 'hbox',
                options: [
                    { value: 'Poor' },
                    { value: 'Fair' },
                    { value: 'Good' },
                    { value: 'Very Good' },
                    { value: 'Excellent' }
                ]
            },
            {
                xtype: 'labelquestion',
                text: "Label Question: Section 4",
                options: [
                    { html: 'M-F' },
                    { html: 'Sat' },
                    { html: 'Sun' },
                    { html: 'Holidays' }
                ]
            },
            {
                xtype: 'checkquestion',
                text: 'Check Question 1',
                layout: 'hbox',
                options: [
                    { value: 'M-F' },
                    { value: 'Sat' },
                    { value: 'Sun' },
                    { value: 'Holidays' }
                ]
            },
            {
                xtype: 'checkquestion',
                text: 'Check Question 2',
                layout: 'hbox',
                options: [
                    { value: 'M-F' },
                    { value: 'Sat' },
                    { value: 'Sun' },
                    { value: 'Holidays' }
                ]
            },
            {
                xtype: 'checkquestion',
                text: 'Check Question 3',
                layout: 'hbox',
                options: [
                    { value: 'M-F' },
                    { value: 'Sat' },
                    { value: 'Sun' },
                    { value: 'Holidays' }
                ]
            },
            {
                xtype: 'label',
                html: "<h2 class='ab-label-type-question'>Label Question: Section 5</h2>"
            },
            {
                xtype: 'timefield',
                label: 'Time Question 1',
                required: true
            },
            {
                xtype: 'timefield',
                label: 'Time Question 2',
                labelAlign: 'top',
                required: true
            },
            {
                xtype: 'label',
                html: "<h2 class='ab-label-type-question'>Label Question: Section 6</h2>"
            },
            {
                xtype: 'measurementfield',
                label: 'Measurement Question 1',
                required: true
            },
            {
                xtype: 'measurementfield',
                label: 'Measurement Question 2',
                unitLabel: 'lb',
                required: false
            },
            {
                xtype: 'measurementfield',
                label: 'Measurement Question 3',
                unitLabel: 'fpm',
                labelAlign: 'top',
                required: true
            }
        ]
    }
});