Ext.define('Solutions.view.SingleRecordSync', {
    extend: 'Ext.Container',


    config: {
        scrollable: true,
        items: [
            {
                xtype: 'container',
                html: ['<p>There may be times when it is useful for a mobile app to sync individual records instead of syncing an entire table. ',
                    'The ARCHIBUS Mobile Framework supports this scenario. Syncing individual records is done by setting the configuration options  ',
                    'on the Store object.</p>',

                    '<p>The following configurations are required to sync individual records</p>',
                    '<ul><li>Set the store <strong>deleteAllRecordsOnSync</strong> property to <strong>false</strong></li>',
                    '<li>Set the model <strong>uniqueIdentifier</strong> property</li><li>Set a restriction on the store to limit the sync to a single record</li></ul>',
                    '<p>This example demonstrates how to sync a single WorkRequest record to the wr_sync table.',
                    ' The example code performs the following:</p><ul><li>All Work Request records for the user are synced.</li>',
                    '<li>A restriction is added to the store to limit the sync to a single record.</li>',
                    '<li>The sync is executed again. This time only the single record set in the applied restriction is downloaded.</li></ul>',
                    '<p><strong>Records must exist in the wr_sync table for the logged in user for this example to sync the records.</strong></p>'
                ].join(''),
                styleHtmlContent: true
            },
            {
                xtype: 'button',
                text: 'Sync Individual Record',
                itemId: 'btnSync',
                margin: '10px 20px 10px 20px'
            }
        ]
    },

    initialize: function() {
        var syncButton = this.down('button');

        syncButton.on('tap', this.doSync);
    },

    doSync: function() {
        var workRequestStore = Ext.getStore('solutionsWorkRequests');
        Mask.displayLoadingMask();
        Common.service.Session.start()
            .then(function() {
                // sync all work request records for the user.
                return workRequestStore.syncStore();
            })
            .then(function() {
                // Set the deleteAllRecordsOnSync property to false so that the sync does not delete
                // all of the records in the client database table.
                workRequestStore.setDeleteAllRecordsOnSync(false);

                // Set the sync restriction
                workRequestStore.setRestriction({
                    tableName: 'wr_sync',
                    fieldName: 'wr_id',
                    operation: 'EQUALS',
                    value: '1150000338'
                });

                // Sync the record set in the restriction.
                return workRequestStore.syncStore();
            })
            .then(function() {
                Mask.hideLoadingMask();
                alert('Sync Completed.');
                return Common.service.Session.end();
            }, function(error) {
                alert('Error: ' + error);
                Mask.hideLoadingMask();
                Common.service.Session.end();
            });
    }

});