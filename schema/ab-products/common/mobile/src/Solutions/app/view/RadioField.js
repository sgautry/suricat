Ext.define('Solutions.view.RadioField', {
    extend: 'Ext.form.Panel',

    config: {
        items: [
            {
                xtype: 'commonradiofield',
                name : 'color',
                value: 'red',
                label: 'Red',
                checked: true
            },
            {
                xtype: 'commonradiofield',
                name : 'color',
                value: 'green',
                label: 'Green'
            },
            {
                xtype: 'commonradiofield',
                name : 'color',
                value: 'blue',
                label: 'Blue'
            }
        ]
    }
});