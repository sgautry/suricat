Ext.define('Solutions.view.MultipleField', {
    extend: 'Ext.form.Panel',

    requires: [
        'Common.control.field.MultipleField',
        'Common.control.prompt.Employee',
        'Common.control.field.Calendar',
        'Common.control.field.Time',
        'Common.control.Spinner',
        'Common.control.field.Measurement'
    ],

    config: {
        items: [
            {
                xtype: 'fieldset',
                title: 'Uses the control \'Common.control.field.MultipleField\'',
                items: [

                    {
                        xtype: 'multiplefield',
                        maxFields: 3,
                        fieldConfig: {
                            xtype: 'employeePrompt',
                            label: 'Lookup Field',
                            labelAlign: 'top'
                        }
                    },
                    {
                        xtype: 'multiplefield',
                        maxFields: 3,
                        fieldConfig: {
                            xtype: 'calendarfield',
                            label: 'Date Field',
                            labelAlign: 'top'
                        }
                    },
                    {
                        xtype: 'multiplefield',
                        maxFields: 3,
                        fieldConfig: {
                            xtype: 'timefield',
                            label: 'Time Field',
                            labelAlign: 'top'
                        }
                    },
                    {
                        xtype: 'multiplefield',
                        maxFields: 3,
                        fieldConfig: {
                            xtype: 'localizedspinnerfield',
                            label: 'Count Field',
                            labelAlign: 'top',
                            stepValue: 1,
                            decimals: 0
                        }
                    },
                    {
                        xtype: 'multiplefield',
                        maxFields: 3,
                        fieldConfig: {
                            xtype: 'measurementfield',
                            label: 'Measurement Field',
                            unitLabel: 'lb'
                        }
                    }
                ]
            }
        ]
    }
});