Ext.define('Solutions.view.CheckField', {
    extend: 'Ext.form.Panel',

    config: {
        items: [
            {
                xtype: 'commoncheckbox',
                name: 'check1',
                value: 'value1',
                label: 'Checkbox Question Option',
                labelAlign: 'right',
                labelWidth: '100%',
                cls: 'ab-checkbox-field',
                checked: true
            },
            {
                xtype: 'commoncheckbox',
                name: 'check2',
                value: 'value2',
                labelAlign: 'right',
                label: 'Standard Checkbox 1'
            },
            {
                xtype: 'commoncheckbox',
                name: 'check3',
                value: 'value3',
                labelAlign: 'right',
                label: 'Standard Checkbox 2'
            }
        ]
    }
});