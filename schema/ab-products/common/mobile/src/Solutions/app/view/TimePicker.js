Ext.define('Solutions.view.TimePicker', {
    extend: 'Ext.Container',

    requires: 'Common.control.field.TimePicker',

    config: {
        items: [
            {
                xtype: 'timepickerfield',
                icon: 'stop'
            }
        ]
    },

    initialize: function() {
        var me = this,
            timePicker = me.down('timepickerfield');

        timePicker.on('timepickerstop', me.onStop);
        timePicker.on('timepickerstart', me.onStart);
    },

    onStop: function(timepicker) {
        timepicker.setValue(new Date());
    },

    onStart: function(timepicker) {
        timepicker.setValue(new Date());
    }

});
