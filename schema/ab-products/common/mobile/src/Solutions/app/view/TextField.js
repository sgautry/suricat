Ext.define('Solutions.view.TextField', {
    extend: 'Ext.Container',


    isActionIconDisplayed: false,

    config: {
        items: [
            {
                xtype: 'commontextfield',
                style: 'padding:20px'
            },
            {
                xtype: 'button',
                text: 'Display Action Icon',
                style: 'margin:0 40px 0 40px'
            }
        ]
    },

    initialize: function () {
        var me = this,
            commontextfield = me.down('commontextfield'),
            button = me.down('button'),
            text = '{0} Action Icon';

        button.on('tap', function () {
            var iconDisplay = !me.isActionIconDisplayed;

            commontextfield.setShowActionIcon(iconDisplay);
            button.setText(Ext.String.format(text, iconDisplay? 'Hide':'Display'));

            me.isActionIconDisplayed = iconDisplay;
        });

        commontextfield.on('actionicontap', function () {
            Ext.Msg.alert('', 'The Action icon is tapped.');
        });
    }
});