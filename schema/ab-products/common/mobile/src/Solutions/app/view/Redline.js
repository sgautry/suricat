Ext.define('Solutions.view.Redline', {
    extend: 'Floorplan.view.Redline',

    requires: [
        'Floorplan.util.Drawing',
        'Solutions.view.DrawingCapture',
        'Common.device.ScreenCapture'
    ],

    initialize: function () {
        var me = this,
            redlineLegendContainer = me.down('legend');

        me.callParent();

        // Retrieve the SVG Data
        me.loadDrawing('1 - ALLOCATION', 'HQ', '17');

        // Register listeners for the Redline action buttons. These handlers would usually be implemented in
        // a Controller class. See {@link Floorplan.controller.Redline} for an example

        redlineLegendContainer.on({
            scope: me,
            legendcopytap: 'copyAssets',   // copyAssets and pasteAssets are methods on Floorplan.view.Redline
            legendpastetap: 'pasteAssets',
            legendreloadtap: 'onLegendReloadTap',
            legendsavetap: 'onLegendSaveTap'

        });
    },

    loadDrawing: function (planType, blId, flId) {
        var me = this;

        Floorplan.util.Drawing.readDrawingFromStorageOrRetrieveIfNot(blId, flId, planType, [], false)
            .then(function (svgData) {
                me.setSvgData(svgData);
                return Floorplan.util.Drawing.refreshDrawing(me, 300);
            });
    },

    onLegendReloadTap: function () {
        this.loadDrawing('1 - ALLOCATION', 'HQ', '17');
    },

    onLegendSaveTap: function () {
        this.saveRedlineData();
    },

    displaySavedRedlineImage: function (dataURI) {
        var me = this;
        if (!me.imageDisplay) {
            me.imageDisplay = Ext.create('Solutions.view.DrawingCapture');
            Ext.Viewport.add(me.imageDisplay);
        }

        me.imageDisplay.on('cancel', function () {
            Floorplan.util.Drawing.refreshDrawing(me);
        });

        me.imageDisplay.setImageUrl(dataURI);
        me.imageDisplay.show();
    },

    saveRedlineData: function () {
        var me = this,
            isNative = Common.env.Feature.isNative,
            tmpImageData,
            legendComponent,
            dataURI;

        return me.hideRedlineViewElements(me, me, true, isNative)
            .then(function () {
                return me.delay(100);
            })
            .then(function () {
                return me.getRedlineImage(isNative);
            })
            .then(function (imageData) {
                tmpImageData = imageData;
                return me.hideRedlineViewElements(me, me, false, isNative);
            })
            .then(function () {
                if (isNative) {
                    dataURI = 'data:image/png;base64,' + tmpImageData;
                    me.displaySavedRedlineImage(dataURI);
                } else {
                    legendComponent = me.down('legendcomponent');
                    legendComponent.getImageBase64(function (tmpImageData) {
                        var dataURI = 'data:image/png;base64,' + tmpImageData;
                        me.displaySavedRedlineImage(dataURI);
                    }, me);
                }
            });
    },

    hideRedlineViewElements: function (mainView, redlineView, hide, isNative) {
        var me = this,
            fn = hide ? 'hide' : 'show',
            parent;

        if (!isNative) {
            if (hide) {
                Mask.displayLoadingMask();
            } else {
                Mask.hideLoadingMask();
            }
            return Promise.resolve();
        } else {

            parent = me.getParent();

            parent.down('titlebar')[fn]();
            parent.down('demonestedlist')[fn]();

            me.down('legend')[fn]();
            me.down('titlebar')[fn]();
            StatusBar[fn]();

            return Floorplan.util.Drawing.refreshDrawing(redlineView, 300);
        }
    },

    delay: function (t) {
        return new Promise(function (resolve) {
            setTimeout(resolve, t);
        });
    },

    getRedlineImage: function (isNative) {
        var me = this,
            legendComponent = me.down('legendcomponent');

        if (isNative) {
            return Common.device.ScreenCapture.getURI()
                .then(function (imageData) {
                    var data = imageData.substring(23);
                    return Promise.resolve(data);
                });
        } else {
            return me.getCanvasImage(legendComponent);
        }
    },

    getCanvasImage: function (legendComponent) {
        return new Promise(function (resolve) {
            legendComponent.getImageBase64(resolve);
        });
    }
});