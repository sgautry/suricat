Ext.define('Solutions.model.WorkRequest', {
    extend: 'Common.data.Model',

    config: {

        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'mob_wr_id',
                type: 'IntegerClass'
            },
            {
                name: 'mob_is_changed',
                type: 'IntegerClass'
            },
            {
                name: 'mob_locked_by',
                type: 'string'
            },
            {
                name: 'mob_pending_action',
                type: 'string'
            },
            {
                name: 'wr_id',
                type: 'IntegerClass'
            },
            {
                name: 'parent_wr_id',
                type: 'IntegerClass'
            },
            {
                name: 'cf_notes',
                type: 'string'
            },
            {
                name: 'site_id',
                type: 'string'
            },
            {
                name: 'bl_id',
                type: 'string'
            },
            {
                name: 'fl_id',
                type: 'string'
            },
            {
                name: 'rm_id',
                type: 'string'
            },
            {
                name: 'location',
                type: 'string'
            },
            {
                name: 'prob_type',
                type: 'string'
            },
            {
                name: 'priority',
                type: 'IntegerClass',
                defaultValue: 1
            },
            {
                name: 'requestor',
                type: 'string'
            },
            {
                name: 'description',
                type: 'string'
            },
            {
                name: 'eq_id',
                type: 'string'
            },
            {
                name: 'status',
                type: 'string',
                defaultValue: 'R'
            },
            {
                name: 'date_requested',
                type: 'DateClass',
                defaultValue: new Date()
            },
            {
                name: 'tr_id',
                type: 'string'
            },
            {
                name: 'pmp_id',
                type: 'string'
            },
            {
                name: 'date_assigned',
                type: 'DateClass'
            },
            {
                name: 'date_est_completion',
                type: 'DateClass'
            },
            {
                name: 'date_escalation_completion',
                type: 'DateClass'
            },
            {
                name: 'request_type',
                type: 'IntegerClass',
                defaultValue: 0
            },
            {
                name: 'cause_type',
                type: 'string'
            },
            {
                name: 'repair_type',
                type: 'string'
            },
            {
                name: 'doc1',
                type: 'string',
                isDocumentField: true,
                isSyncField: true
            },
            {
                name: 'doc2',
                type: 'string',
                isDocumentField: true,
                isSyncField: true
            },
            {
                name: 'doc3',
                type: 'string',
                isDocumentField: true,
                isSyncField: true
            },
            {
                name: 'doc4',
                type: 'string',
                isDocumentField: true,
                isSyncField: true
            },
            // 21.3 fields
            {
                name: 'status_initial',
                type: 'string',
                isSyncField: false
            },
            {
                name: 'mob_stat_step_chg',
                type: 'IntegerClass'
            },
            {
                name: 'mob_step_action',
                type: 'string'
            },
            {
                name: 'mob_step_comments',
                type: 'string'
            },
            {
                name: 'escalated_response',
                type: 'IntegerClass'
            },
            {
                name: 'escalated_completion',
                type: 'IntegerClass'
            },
            {
                name: 'step',
                type: 'string'
            },
            {
                name: 'step_log_id',
                type: 'IntegerClass'
            },
            {
                name: 'step_type',
                type: 'string'
            },
            {
                name: 'step_status',
                type: 'string'
            },
            {
                name: 'step_user_name',
                type: 'string'
            },
            {
                name: 'step_em_id',
                type: 'string'
            },
            {
                name: 'step_role_name',
                type: 'string'
            },
            {
                name: 'is_req_supervisor',
                type: 'IntegerClass'
            },
            {
                name: 'is_req_craftsperson',
                type: 'IntegerClass'
            },
            {
                name: 'is_wt_self_assign',
                type: 'IntegerClass'
            },
            {
                name: 'cost_est_labor',
                type: 'float',
                defaultValue: 0
            },
            {
                name: 'cost_est_parts',
                type: 'float',
                defaultValue: 0
            },
            {
                name: 'cost_est_other',
                type: 'float',
                defaultValue: 0
            },
            {
                name: 'cost_est_total',
                type: 'float',
                defaultValue: 0
            },
            {
                name: 'cost_labor',
                type: 'float',
                defaultValue: 0
            },
            {
                name: 'cost_parts',
                type: 'float',
                defaultValue: 0
            },
            {
                name: 'cost_other',
                type: 'float',
                defaultValue: 0
            },
            {
                name: 'cost_total',
                type: 'float',
                defaultValue: 0
            },
            {
                name: 'time_requested',
                type: 'TimeClass'
            },
            {
                name: 'dv_id',
                type: 'string'
            },
            {
                name: 'dp_id',
                type: 'string'
            },
            {
                name: 'fwd_supervisor',
                type: 'string'
            },
            {
                name: 'fwd_work_team_id',
                type: 'string'
            },
            {
                name: 'supervisor',
                type: 'string'
            },
            {
                name: 'estimation_comp',
                type: 'IntegerClass'
            },
            {
                name: 'scheduling_comp',
                type: 'IntegerClass'
            },
            {
                name: 'work_team_id',
                type: 'string'
            },
            //23.2
            {
                name: 'eq_status',
                type: 'string'
            },
            {
                name: 'eq_condition',
                type: 'string'
            },
            {
                name: 'mob_eq_fields_changed',
                type: 'string'
            },
            {
                name: 'priority_label',
                type: 'string'
            },
            {
                name: 'guid',
                type: 'guid'
            }
        ],

        uniqueIdentifier: ['wr_id']
    }

});