Ext.define('Solutions.store.SolutionsWorkRequest', {
    extend: 'Common.store.sync.SyncStore',
    requires: [
        'Solutions.model.WorkRequest'
    ],

    serverTableName: 'wr_sync',
    serverFieldNames: ['wr_id', 'bl_id', 'fl_id', 'rm_id', 'site_id', 'cf_notes', 'date_requested', 'description',
        'eq_id', 'location', 'priority', 'prob_type', 'requestor', 'tr_id', 'status', 'mob_locked_by',
        'mob_pending_action', 'mob_is_changed', 'mob_wr_id', 'pmp_id', 'date_assigned', 'date_est_completion',
        'cause_type', 'repair_type', 'doc1', 'doc2', 'doc3', 'doc4', 'request_type', 'date_escalation_completion',
        // 21.3
        'mob_stat_step_chg', 'mob_step_action', 'mob_step_comments',
        'escalated_response', 'escalated_completion',
        'step', 'step_log_id', 'step_type', 'step_status', 'step_user_name', 'step_em_id', 'step_role_name',
        'is_req_supervisor', 'is_req_craftsperson', 'is_wt_self_assign',
        'cost_est_labor', 'cost_est_parts', 'cost_est_other', 'cost_est_total',
        'cost_labor', 'cost_parts', 'cost_other', 'cost_total',
        'time_requested',
        'dv_id', 'dp_id', 'supervisor', 'parent_wr_id',
        //21.4
        'estimation_comp', 'scheduling_comp', 'work_team_id',
        // 23.1
        'doc1_isnew', 'doc2_isnew', 'doc3_isnew', 'doc4_isnew',
        //23.2
        'eq_status', 'eq_condition', 'mob_eq_fields_changed', 'priority_label', 'guid'
    ],

    inventoryKeyNames: ['guid', 'mob_locked_by'],

    config: {
        model: 'Solutions.model.WorkRequest',
        storeId: 'solutionsWorkRequests',
        tableDisplayName: 'Work Request',


        documentConfig: {
            documentTable: 'wr',
            documentTableKeys: ['wr_id']
        },
        proxy: {
            type: 'Sqlite'
        }
    }
});