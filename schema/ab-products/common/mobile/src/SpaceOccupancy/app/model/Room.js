Ext.define('SpaceOccupancy.model.Room', {
    extend: 'Common.model.Room',
    config: {
        fields: [
            {
                name: 'survey_redline_rm',
                type: 'string',
                isDocumentField: true
            },
            {
                name: 'survey_redline_rm_contents',
                type: 'string',
                isSyncField: false
            }
        ]
    }
});