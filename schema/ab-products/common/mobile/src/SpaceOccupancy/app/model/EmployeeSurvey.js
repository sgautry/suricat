Ext.define('SpaceOccupancy.model.EmployeeSurvey', {
    extend: 'Common.data.Model',

    config: {
        uniqueIdentifier: ['em_id'],
        fields: [
            {
                name: 'id',
                type: 'int'
            },
            {
                name: 'survey_id',
                type: 'string'
            },
            {
                name: 'em_id',
                type: 'string'
            },
            {
                name: 'email',
                type: 'string'
            },
            {
                name: 'bl_id',
                type: 'string'
            },
            {
                name: 'fl_id',
                type: 'string'
            },
            {
                name: 'rm_id',
                type: 'string'
            },
            {
                name: 'phone',
                type: 'string'
            },
            {
                name: 'name_last',
                type: 'string'
            },
            {
                name: 'name_first',
                type: 'string'
            },
            {
                name: 'em_number',
                type: 'string'
            },
            {
                name: 'dv_id',
                type: 'string'
            },
            {
                name: 'dp_id',
                type: 'string'
            },
            {
                name: 'em_std',
                type: 'string'
            },
            {
                name: 'em_photo',
                type: 'string',
                isDocumentField: true,
                isSyncField: true
            },
            {
                name: 'mob_locked_by',
                type: 'string'
            },
            {
                name: 'mob_is_changed',
                type: 'IntegerClass'
            }
        ],

        validations: [
            {type: 'presence', field: 'em_id'}
        ]
    },

    /**
     * @override Override to set the _isnew file isSyncField property to false. MOB-251.
     * @param documentFields
     * @returns {Array}
     */
    generateDocumentFieldConfig: function (documentFields) {
        var documentFieldConfig = [],
            documentFieldTemplate = [
                {
                    name: '{0}_contents',
                    type: 'string',
                    isSyncField: true,
                    defaultValue: ''
                },
                {
                    name: '{0}_isnew',
                    type: 'boolean',
                    isSyncField: false,
                    defaultValue: false
                },
                {
                    name: '{0}_file',
                    type: 'string',
                    isSyncField: false,
                    defaultValue: ''
                },
                {
                    name: '{0}_version',
                    type: 'int',
                    isSyncField: false,
                    defaultValue: 0
                }
            ];


        documentFields.forEach(function (field) {
            var tmpTemplate = Ext.clone(documentFieldTemplate);
            var documentFields = tmpTemplate.map(function (templateField) {
                templateField.name = Ext.String.format(templateField.name, field.name);
                return templateField;
            });
            documentFieldConfig = documentFieldConfig.concat(field).concat(documentFields);
        });

        return documentFieldConfig;
    }

});