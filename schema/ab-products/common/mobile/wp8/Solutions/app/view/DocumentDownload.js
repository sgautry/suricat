Ext.define('Solutions.view.DocumentDownload', {
    extend: 'Ext.Container',

    requires: [
        'Common.document.DocumentManager',
        'Common.device.File'
    ],

    documentToDownload: {
        docTable: 'bl',
        docField: 'bldg_photo',
        docFile: 'bl_ec01.jpg',
        pkey: {
            bl_id: 'EC01'
        },
        storeId: 'solutionsBuildings'
    },

    config: {
        style: 'margin:10px',
        items: [
            {
                xtype : 'toolbar',
                docked: 'top',
                hidden: true,
                items: [
                    {
                        xtype: 'camera',
                        align: 'left',
                        iconCls: 'camera',
                        displayOn: 'all'
                    }
                ]
            },
            {
                xtype: 'fieldset',
                instructions: 'This example checks out an ARCHIBUS document and saves the document file on the device.'
            },
            {
                xtype: 'container',
                style: 'margin-top:20px',
                layout: {
                    type: 'hbox',
                    pack: 'center'
                },
                items: {
                    xtype: 'button',
                    text: 'Download',
                    itemId: 'downloadButton',
                    width: '70%'
                }
            },
            {
                xtype: 'container',
                style: 'margin-top:20px',
                layout: {
                    type: 'hbox',
                    pack: 'center'
                },
                items: {
                    xtype: 'button',
                    text: 'Display',
                    itemId: 'displayButton',
                    width: '70%'
                }
            }

        ]


    },

    initialize: function () {
        var me = this,
            downloadButton = me.down('#downloadButton'),
            displayButton = me.down('#displayButton'),
            docToDownload = me.documentToDownload,
            deviceDocumentFileName = DocumentManager.generateFileName(docToDownload.docTable, docToDownload.docField, docToDownload.docFile, docToDownload.pkey),
            documentFolder = GlobalParameters.onDemandDocumentFolder,
            deviceDocumentFolder = documentFolder + '/' + Common.Application.appName;

        downloadButton.on('tap', 'onDownloadDocument', me);
        displayButton.on('tap', 'onDisplayDocument', me);

        // Check if the document file has already been downloaded.
        Common.device.File.fileExists(deviceDocumentFolder + '/' + deviceDocumentFileName)
            .then(function () {
                // File Exists
                displayButton.setHidden(false);
                downloadButton.setText('Refresh');
            }, function () {
                // File does not exist
                displayButton.setHidden(true);
                downloadButton.setText('Download');
            });

    },

    /**
     * Download lease document ls-102-doc.doc
     */
    onDownloadDocument: function () {
        var me = this,
            docToDownload = me.documentToDownload,
            downloadButton = me.down('#downloadButton'),
            displayButton = me.down('#displayButton'),
            store = Ext.getStore(docToDownload.storeId);

        DocumentManager.downloadOnDemandDocument(docToDownload.docTable, docToDownload.docField, docToDownload.docFile, docToDownload.pkey, store)
            .then(function () {
                Ext.Msg.alert('', 'Downloaded document ' + docToDownload.docFile + ' from the ' + docToDownload.docTable + ' table.');
                displayButton.setHidden(false);
                downloadButton.setText('Refresh');
                me.onDisplayDocument();
            }, function (error) {
                Log.log('Error occured while downloading the document. ' + error, 'error');
            });
    },

    onDisplayDocument: function () {
        var me = this,
            docToDownload = me.documentToDownload,
            store = Ext.getStore(docToDownload.storeId),
            cameraPanel = me.down('camera'),
            documentData,
            filter = {};

        //DocumentManager.displayOnDemandDocument(docToDownload.key, docToDownload.docTable, docToDownload.docField, docToDownload.docFile, cameraPanel, store);

        // Create the filter
        var pkeyFieldName = Object.keys(docToDownload.pkey)[0];
        filter.property = pkeyFieldName;
        filter.value = docToDownload.pkey[pkeyFieldName];

        store.clearFilter();
        store.setFilters([filter]);

        store.load(function(records) {
            if(records && records.length > 0) {
                documentData = records[0].get(docToDownload.docField + '_contents');
                DocumentManager.displayDocumentOrImage(documentData, docToDownload.docFile, cameraPanel);
            }
        });
    }
});





