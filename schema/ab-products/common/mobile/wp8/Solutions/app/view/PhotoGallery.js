Ext.define('Solutions.view.PhotoGallery', {
    extend: 'Ext.Container',

    requires: [
        'Common.device.Camera',
        'Common.env.Feature'
    ],

    config: {
        items: [

            {
                xtype: 'fieldset',
                instructions: 'Feature is not available in the desktop environment.'
            },
            {
                xtype: 'button',
                text: 'Open Photo Gallery',
                style: 'margin:0 40px 10px 40px'
            },
            {
                xtype: 'container',
                margin: '20px 0 0 0',
                width: '90%',
                height: '95%',
                style: 'margin:0 5% 0 5%',
                scrollable: 'both',
                items: [
                    {
                        itemId: 'imageContainer',
                        xtype: 'image',
                        mode: 'src',
                        width: '100%',
                        height: '100%'
                    }
                ]
            }
        ]
    },

    initialize: function () {
        var me = this,
            photoGalleryButton = me.down('button');

        photoGalleryButton.on('tap', 'openPhotoGallery', me);
    },

    openPhotoGallery: function () {
        var me = this,
            imageContainer = me.down('#imageContainer');

        if(!Common.env.Feature.isNative) {
            Ext.Msg.alert('Feature Not Available', 'The Photo Gallery feature is only available when using the Mobile Client app.');
            return;
        }

        Common.device.Camera.getPictureFromPhotoLibrary()
            .then(function (imageData) {
                imageContainer.setSrc('data:image/jpg;base64,' + imageData);
            }, function (error) {
                alert(error);
            });
    }
});