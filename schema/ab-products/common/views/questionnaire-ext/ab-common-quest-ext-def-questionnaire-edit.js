var commonQuestExtDefQuestionnaireEditController = View.createController('commonQuestExtDefQuestionnaireEdit', {
	questionnaire_id: '',
	
	afterInitialDataFetch: function() {
		//this.questionnaireForm.enableField('questionnaire_ext.old_questionnaire_title', false);
		/*if (this.questionnaireForm.newRecord) {
			$('questionnaireForm_questionnaire_ext.old_questionnaire_title').disabled = true;
		} else this.questionnaireForm.showField('questionnaire_ext.old_questionnaire_title', false);*/
	},
	
	/**
	 * This event handler updates calculated form field values for current user name and date before saving a record.
	 * abCommonQuestExtQuestionnaireDefController_beforeSave
	 */
	questionnaireForm_beforeSave : function() {
		var form = this.questionnaireForm;		
		var ds = this.questionnairesDataSource;
		
		//Set Updated By User Name (user_name) value to current user's ID
		form.setFieldValue('questionnaire_ext.edited_by', this.view.user.name);
		
		//Set Date Last Updated to current date 		
		var currentDateObj = new Date();
		var currentDate = ds.formatValue('questionnaire_ext.date_last_edit', currentDateObj, false);
		form.setFieldValue('questionnaire_ext.date_last_edit', currentDate);
	},
	
	questionnaireForm_onSave: function() {
		var newRecord = this.questionnaireForm.newRecord;
		//var old_questionnaire_id = this.questionnaireForm.getFieldValue('questionnaire_ext.old_questionnaire_id');
		
		/*if (newRecord && old_questionnaire_id != '') {
			if (!this.questionnaireForm.canSave()) return;
			var record = this.questionnaireForm.getRecord();
			var ds = View.dataSources.get('questionnairesDataSource');	        		
			var currentDateObj = new Date();
			var questRecord = new Ab.data.Record({
				'questionnaire_ext.questionnaire_title': record.getValue('questionnaire_ext.questionnaire_title'),
				'questionnaire_ext.status': record.getValue('questionnaire_ext.status'),
				'questionnaire_ext.description': record.getValue('questionnaire_ext.description'),
				'questionnaire_ext.date_effective_start': record.getValue('questionnaire_ext.date_effective_start'),
				'questionnaire_ext.date_effective_end': record.getValue('questionnaire_ext.date_effective_end'),
				'questionnaire_ext.date_last_edit': ds.formatValue('question_ext.date_last_edit', currentDateObj, false),
				'questionnaire_ext.edited_by': View.user.name
			}, true);
			ds.saveRecord(questRecord);	
		
			var restriction = new Ab.view.Restriction();
			restriction.addClause('quest_question_ext.quesionnaire_id', old_questionnaire_id);
			this.selectQuestionsGrid.refresh(restriction);
			this.selectQuestionsGrid.showInWindow({});
		} else {*/
			if (!this.questionnaireForm.save()) return;
		
			var questionnaire_id = this.questionnaireForm.getFieldValue('questionnaire_ext.questionnaire_id');
			var questionnaire_title = this.questionnaireForm.getFieldValue('questionnaire_ext.questionnaire_title');
			if (View.parameters && View.parameters.callback) View.parameters.callback(questionnaire_id, questionnaire_title, newRecord);	
		//}
	}
});
