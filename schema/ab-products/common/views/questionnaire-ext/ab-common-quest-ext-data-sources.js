var multipleValueSeparator = Ab.form.Form.MULTIPLE_VALUES_SEPARATOR;
/**
 * Loads one questionnaire and questions and answer options.
 */
QuestionnaireLoader = Base.extend({}, {

    /**
     * Loads specified questionnaire and returns the loaded Questionnaire object.
     * @param questionnaireId The questionnaire_ext.questionnaire_id value.
     * @param disabled Boolean - true sets the questionnaire to read-only.
     * @param surveyEventId activity_log_id optional
     */
    load: function(questionnaireId, disabled, surveyEventId) {
        var questionnaire = null;
        if (!valueExistsNotEmpty(surveyEventId)) surveyEventId = null;

        var restriction = new Ab.view.Restriction();
        restriction.addClause('questionnaire_ext.questionnaire_id', questionnaireId, '=');

        var record = View.dataSources.get('questionnairesDataSource').getRecord(restriction);
        if (record) {
            questionnaire = {
                id: record.getValue('questionnaire_ext.questionnaire_id'),
                title: record.getValue('questionnaire_ext.questionnaire_title'),
                status: record.getValue('questionnaire_ext.status'),
                disabled: disabled,
                questions: [],
                sections: [],

                /**
                 * Returns all questions for this questionnaire, including:
                 * 1. All top-level questions.
                 * 2. All child questions currently expanded by the user.
                 * @returns {Array} of questions.
                 */
                getAllQuestions: function() {
                    var topQuestions = [];

                    topQuestions = _.union(topQuestions, this.questions);

                    _.each(this.sections, function(section) {
                        topQuestions = _.union(topQuestions, section.questions);
                    });

                    var allQuestions = [];

                    var allChildQuestions = null;
                    var addChildQuestions = function(question) {
                        _.each(question.childQuestions, function(childQuestion) {
                            allQuestions.push(childQuestion);
                            addChildQuestions(childQuestion);
                        });
                    }
                    _.each(topQuestions, function(topQuestion) {
                        allQuestions.push(topQuestion);
                        addChildQuestions(topQuestion);
                    });

                    return allQuestions;
                }
            };

            var questionRecords = View.dataSources.get('questionnaireQuestionsDataSource').getRecords(restriction);
            questionRecords = _.sortBy(questionRecords, function(record) {
                return Number(record.getValue('quest_question_ext.sort_order'));
            });

            var currentSection = null;

            _.each(questionRecords, function(questionRecord) {
                var question = QuestionnaireLoader.loadQuestion(questionRecord, surveyEventId);

                if (question.type === 'label') {
                    currentSection = question;
                    questionnaire.sections.push(question);
                } else if (currentSection) {
                    question.section = currentSection;
                    currentSection.questions.push(question);
                } else {
                    questionnaire.questions.push(question);
                }
            });
        }

        return questionnaire;
    },

    /**
     * Loads child questions that match selected answer options for specified parent question.
     * @param parentQuestion
     * @param surveyEventId activity_log optional
     */
    loadChildQuestions: function(parentQuestion, surveyEventId) {
        var childQuestions = [];
        if (!valueExistsNotEmpty(surveyEventId)) surveyEventId = null;

        var restriction = new Ab.view.Restriction();
        restriction.addClause('question_ext.question_id', parentQuestion.id, '!=');
        restriction.addClause('question_ext.hierarchy_ids', parentQuestion.hierarchyIds + '%', 'LIKE');
        var questionRecords = View.dataSources.get('questionsDataSource').getRecords(restriction);
        questionRecords = _.sortBy(questionRecords, function(record) {
            return Number(record.getValue('question_ext.sort_order_child'));
        });

        _.each(questionRecords, function(questionRecord) {
            var parentAnswerCondition = questionRecord.getValue('question_ext.parent_answer_condition');
            var parentAnswerConditions = parentAnswerCondition.split(multipleValueSeparator);

            var addChildQuestion = false;
            _.each(parentAnswerConditions, function(condition) {
                _.each(parentQuestion.selectedAnswerOptions, function(selectedAnswerOption) {
                    if (selectedAnswerOption.id === condition) {
                        addChildQuestion = true;
                    }
                });
            });

            if (addChildQuestion) {
            	var childQuestion = QuestionnaireLoader.loadQuestion(questionRecord, surveyEventId);
            	
                childQuestions.push(childQuestion);                
            }
        });

        return childQuestions;
    },

    /**
     * Loads a question and its answer options.
     * @param questionRecord
     * @param surveyEventId activity_log_id optional
     */
    loadQuestion: function(questionRecord, surveyEventId) {
    	var isRequired = questionRecord.getValue('quest_question_ext.is_required');
    	if (isChildQuestion(questionRecord)) isRequired = questionRecord.getValue('question_ext.is_required_child');
        var question = {
            id: questionRecord.getValue('question_ext.question_id'),
            text: questionRecord.getValue('question_ext.question_text'),
            prefix: questionRecord.getValue('quest_question_ext.question_prefix'),
            type: questionRecord.getValue('question_ext.question_type'),
            isLongForm: questionRecord.getValue('quest_question_ext.display_format') === 'Long Form',
            showAnswerText: questionRecord.getValue('quest_question_ext.show_answer_text') == '1',
            unitType: questionRecord.getValue('question_ext.unit_type'),
            unitDefault: questionRecord.getValue('question_ext.unit_default'),
            lookupTable: questionRecord.getValue('question_ext.lookup_table'),
            lookupField: questionRecord.getValue('question_ext.lookup_field'),
            hierarchyIds: questionRecord.getValue('question_ext.hierarchy_ids'),
            isRequired: isRequired,
            isChild: isChildQuestion(questionRecord),
            maxLength: questionRecord.getValue('question_ext.freeform_width'),
            maxResponses: questionRecord.getValue('question_ext.num_responses_max'),
            /**
             * Array of child questions.
             */
            questions: [],
            /**
             * Array of available answer options: { id: x, text: y }.
             */
            answerOptions: [],
            /**
             * Array of selected answer options: { id: x, text: y }. The application sets this property when it re-loads
             * a survey that has been previously saved.
             */
            selectedAnswerOptions: [],
            /**
             * Array of selected units, pne per value entered by the user. Used only for measurement questions.
             */
            units: [],
            /**
             * Array of DevExtreme controls for each response entered by the user. When the survey is initially displayed
             * to the user, this array contains one control, except document and multiple choice questions which do not use controls.
             */
            controls: [],
            /**
             * True if the question has an uploaded document. The application sets this property after the user
             * saves the survey, or after the user re-loads a survey that has been previously saved.
             */
            isUploaded: false,

            isValid: function() {
                var valid = true;
                if (this.type === 'date' || this.type === 'time' || this.type === 'measurement') {
                	_.each(this.controls, function(control) {
                        if (!control.option('isValid')) valid = false;
                    });
                }
                return valid;
            },
            isComplete: function() {
                var complete = false;
                if (this.selectedAnswerOptions.length > 0) {
                    complete = true;
                } else {
                    var values = this.getResponseValues();
                    if (values.length > 0 && valueExistsNotEmpty(values[0]) && this.isValid()) {
                        complete = true;
                    }
                }

                return complete;
            },
            getResponseValues: function() {
                return _.map(this.controls, function(control) {
                    return control.option('value');
                });
            },
            setResponseValues: function(values) {
                _.each(this.controls, function(control, index) {
                    control.option('value', values[index]);
                });
                if (this.documentControl && values.length >= 2) {
                    for (var i = 0; i < values.length; i += 2) {
                        this.documentControl.addSavedDocument(values[i], values[i+1]);
                    }
                }
            }
        };

        // load answer options for the question
        {
            var restriction = new Ab.view.Restriction();
            restriction.addClause('quest_answer_option_ext.question_id', question.id, '=');

            var answerOptionsRecords = View.dataSources.get('answerOptionsDataSource').getRecords(restriction);
            answerOptionsRecords = _.sortBy(answerOptionsRecords, function (record) {
                return record.getValue('quest_answer_option_ext.sort_order');
            });

            _.each(answerOptionsRecords, function (record) {
                question.answerOptions.push({
                    id: record.getValue('quest_answer_option_ext.answer_option_id'),
                    text: record.getValue('quest_answer_option_ext.answer_option_text')
                });
            });
        }

        // load bill units for the question
        if (question.type === 'measurement') {
            var restriction = new Ab.view.Restriction();
            restriction.addClause('bill_unit.bill_type_id', question.unitType, '=');

            var unitRecords = View.dataSources.get('billUnitsDataSource').getRecords(restriction);
            _.each(unitRecords, function (record) {
                question.units.push({
                    id: record.getValue('bill_unit.bill_unit_id'),
                    text: record.getValue('bill_unit.description')
                });
            });
        }
        
        // load selected answers, if any, for multiple-choice questions
        if (valueExistsNotEmpty(surveyEventId)) {
    	    if (question.type === 'multiple_choice_single' || question.type === 'multiple_choice_multi') {
    	    	var restriction = new Ab.view.Restriction();
        	    restriction.addClause('quest_answer_ext.survey_event_id', surveyEventId);
        	    restriction.addClause('quest_answer_ext.question_id', question.id);
        	    var eventAnswers = View.dataSources.get('answersDataSource').getRecords(restriction);
    	    	question.selectedAnswerOptions = getSelectedAnswerOptions(question, eventAnswers);
	        }
        }

        return question;
    }
});

function isChildQuestion(questionRecord) {
	var isChild = false;
	var hierarchyIds = questionRecord.getValue('question_ext.hierarchy_ids');
	if (hierarchyIds.indexOf('|') < (hierarchyIds.length-1)) isChild = true;
	return isChild;
}

function getSelectedAnswerOptions(question, answers) {
	var selectedAnswerOptions = [];
	_.each(answers, function(answer) {
		var answerOptions = answer.getValue('quest_answer_ext.value_text').split(multipleValueSeparator);
		for (var j=0;j<answerOptions.length;j++) {
			inner: for (var k=0;k<question.answerOptions.length;k++) {
				if (question.answerOptions[k].id == answerOptions[j]) {
					selectedAnswerOptions.push(question.answerOptions[k]);
					break inner;
				}
			}
		}
	});
	return selectedAnswerOptions;
}