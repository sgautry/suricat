
View.createController('CompleteQuestionnaire', {

    /**
     * The selected questionnaire.
     */
    questionnaire: null,

    /**
     * Register Handlebars helpers for functions invoked from the HTML template.
     */
    afterViewLoad: function() {
        var controller = this;

        Handlebars.registerHelper('renderQuestionResponse', function(question) {
            question.currentResponse = 0;
            var html = controller.renderQuestionResponse(question);

            if (html && html.length > 0) {
                return new Handlebars.SafeString('<div class="questionnaireQuestionResponse">' + html + '</div>');
            }
        });
        Handlebars.registerHelper('renderAnswerOption', function(question, answerOption) {
            var html = controller.renderAnswerOption(question, answerOption);

            return new Handlebars.SafeString(html);
        });
    },

    /**
     * Renders a response field for a question.
     * @param question
     */
    renderQuestionResponse: function(question) {
        var html = '';

        var id = question.id + '_' + question.currentResponse;

        if (question.type === 'freeform') {
            html = '<div class="dx-field"><div id="questionTextArea_' + id + '"></div></div>';

        } else if (question.type === 'lookup') {
            html = '<div class="dx-field"><div id="questionLookup_' + id + '"></div></div>';

        } else if (question.type === 'measurement') {
            html = '<div class="dx-field"><div id="questionMeasurement_' + id + '"></div></div>';
            html += '<div class="dx-field" style="display:inline-block"><div id="questionMeasurementUnits_' + id + '"></div></div>';

        } else if (question.type === 'count') {
            html = '<div class="dx-field"><div id="questionCount_' + id + '"></div></div>';

        } else if (question.type === 'date') {
            html = '<div class="dx-field"><div id="questionDate_' + id + '"></div></div>';

        } else if (question.type === 'time') {
            html = '<div class="dx-field"><div id="questionTime_' + id + '"></div></div>';

        } else if (question.type === 'document') {
            html = '<div class="dx-field" id="questionDocumentContainer_' + id + '"></div>';
        }

        if (question.type !== 'freeform' && question.type !== 'multiple_choice_single' && question.type !== 'multiple_choice_multi'
            && question.maxResponses && question.maxResponses > 1) {
            html += this.renderAddResponseLink(question);
        }

        return html;
    },

    /**
     * Renders a link that allows the user to add another response.
     * @param question
     */
    renderAddResponseLink: function(question) {
        var html = '';

        var id = 'addResponse_' + question.id;

        if (question.type !== 'document' && question.currentResponse === 0) {
            html = '<div class="addResponseLink"><a id="' + id + '">' + getMessage('addResponse') +'</a></div>';
        }

        return html;
    },

    /**
     * Renders an answer option (i.e. a check box or a radio button) for a question.
     * @param question
     */
    renderAnswerOption: function(question, answerOption) {
        var html = '';

        var text = answerOption.text;
        if (!question.isChild && !question.showAnswerText && question.section && question.section.answerOptions && question.section.answerOptions.length > 0) {
            text = '';
        }

        var checked = _.contains(question.selectedAnswerOptions, answerOption) ? 'checked' : '';
        var disabled = (this.questionnaire.disabled)?' disabled':'';

        if (question.type === 'multiple_choice_single') {
            html = '<input name="questionAnswerOption_' + question.id + '" value="' + answerOption.id + '" type="radio" ' + checked + disabled + '></input><span>' + text + '</span>';

        } else if (question.type === 'multiple_choice_multi') {
            html = '<input name="questionAnswerOption_' + question.id + '" value="' + answerOption.id + '" type="checkbox" ' + checked + disabled + '></input><span>' + text + '</span>';
        }

        return html;
    },

    /**
     * Attaches event listeners to questions and answer options.
     */
    attachQuestionEventListeners: function(question) {
        var controller = this;

        // create the response widget
        if (question.type === 'freeform') {
            this.createResponseWidget(question, jQuery('[id^=questionTextArea_' + question.id + ']'));

        } else if (question.type === 'lookup') {
            this.createResponseWidget(question, jQuery('[id^=questionLookup_' + question.id + ']'));

        } else if (question.type === 'measurement') {
            this.createResponseWidget(question, jQuery('[id^=questionMeasurement_' + question.id + ']'));

        } else if (question.type === 'count') {
            this.createResponseWidget(question, jQuery('[id^=questionCount_' + question.id + ']'));

        } else if (question.type === 'date') {
            this.createResponseWidget(question, jQuery('[id^=questionDate_' + question.id + ']'));

        } else if (question.type === 'time') {
            this.createResponseWidget(question, jQuery('[id^=questionTime_' + question.id + ']'));

        } else if (question.type === 'document') {
            var documentContainers = jQuery('[id^=questionDocumentContainer_' + question.id + ']');
            documentContainers.each(function(index, element) {
                question.documentControl = View.createControl({
                    control: 'DocumentUploadControl',
                    container: element.id,
                    question: question,
                    viewTooltip: getMessage('documentViewTooltip'),
                    uploadTooltip: getMessage('documentUploadTooltip'),
                    deleteTooltip: getMessage('documentDeleteTooltip'),
                    disabled: controller.questionnaire.disabled
                });
            });
        }

        // attach event listeners to answer options
        _.each(question.answerOptions, function(answerOption) {
            var answerOptionInput = jQuery('input[name="questionAnswerOption_' + question.id + '"][value="' + answerOption.id + '"]');
            if (answerOptionInput) {
                answerOptionInput.on('click', function() {
                    controller.onSelectAnswerOption(question, answerOption, answerOptionInput.is(":checked"))
                });
            }
        });

        // add event listener to call when the user clicks on the Add Another Response link
        if (question.type !== 'freeform' && !this.questionnaire.disabled) {
            jQuery('#question_' + question.id).on('click', '[id^=addResponse_]', function (event) {

                var maxResponses = parseInt(question.maxResponses);
                if (question.currentResponse + 1 < maxResponses) {
                    controller.addQuestionResponse(question, true);
                } else {
                    this.text = getMessage('cannotAddResponse');
                }
            });
        }
    },

    /**
     * Creates a response widget and attaches it to the response DOM element.
     * @param question
     * @param responseElement
     */
    createResponseWidget: function(question, responseElement) {
        var widget = null;
        var controller = this;

        if (question.type === 'freeform') {
            widget = responseElement.dxTextArea({
                height: 100,
                width: 400,
                maxLength: question.maxLength
            }).dxTextArea('instance');

        } else if (question.type === 'date') {
            widget = responseElement.dxDateBox({
                type: 'date',
                displayFormat: {
                    formatter: function(value) {
                        return controller.questionnaireQuestionsDataSource.formatValue('question_ext.date_last_edit', value, true);
                    },
                    parser: function(value) {
                        return controller.questionnaireQuestionsDataSource.parseValue('question_ext.date_last_edit', value, true);
                    }
                },
                onValueChanged: function() {
                    controller.onChangeResponse(question);
                }
            }).dxDateBox('instance');

        } else if (question.type === 'time') {
            widget = responseElement.dxDateBox({
                type: 'time',
                displayFormat: {
                    formatter: function(value) {
                        return controller.questionnaireQuestionsDataSource.formatValue('question_ext.time_last_edit', value, true);
                    },
                    parser: function(value) {
                        return controller.questionnaireQuestionsDataSource.parseValue('question_ext.time_last_edit', value, true);
                    }
                },
                onValueChanged: function() {
                    controller.onChangeResponse(question);
                }
            }).dxDateBox('instance');

        } else if (question.type === 'count') {
            widget = responseElement.dxNumberBox({
                mode: 'number',
                showSpinButtons: true,
                showClearButton: true,
                min: -999999999,
                max: 999999999,
                inputAttr: {'maxLength': '9'},
                onKeyPress: function(options) {
                    var keyCode = options.jQueryEvent.keyCode;
                    if (keyCode < 48 || keyCode > 57) {
                        options.jQueryEvent.preventDefault();
                    }
                },
                onValueChanged: function (options) {
                    if (!valueExistsNotEmpty(options.value)) {
                        options.component.option('value', 0);
                    }
                }
            }).dxNumberBox('instance');

        } else if (question.type === 'measurement') {
            widget = responseElement.dxNumberBox({
                mode: 'text',
                inputAttr: {'maxLength': '16'},
                showSpinButtons: false,
                value: ''
            }).dxNumberBox('instance');

        } else if (question.type === 'lookup') {
            widget = responseElement.dxLookup({
                showClearButton: true,
                showCancelButton: false,
                placeholder: getMessage('lookupPlaceholder'),
                searchPlaceholder: getMessage('lookupSearchPlaceholder'),
                clearButtonText:  getMessage('lookupClearButtonText'),
                dataSource: new DevExpress.data.DataSource({
                    byKey: function(key) {
                        return key;
                    },
                    load: function(loadOptions) {
                        var fullName = question.lookupTable + '.' + question.lookupField;

                        var dataSource = Ab.data.createDataSourceForFields({
                            id: 'questionLookupDataSource',
                            tableNames: [question.lookupTable],
                            fieldNames: [question.lookupField]
                        });

                        var restriction = new Ab.view.Restriction();
                        if (loadOptions.searchValue) {
                            // TODO: reuse getAutoCompleteRestriction from ab-form-autocomplete.js
                            restriction.addClause(fullName, loadOptions.searchValue.toUpperCase(), 'LIKE', ')AND(');
                            restriction.addClause(fullName, loadOptions.searchValue.toLowerCase(), 'LIKE', 'OR');
                        }

                        var records = dataSource.getRecords(restriction, {
                            isDistinct: true,
                            sortValues: [{
                                fieldName: fullName,
                                sortOrder: 1
                            }],
                            controlType: 'autoComplete'
                        });

                        var lookupValues = _.map(records, function(record) { return record.values[fullName]; });

                        var query = DevExpress.data.query(lookupValues);
                        query = query.slice(loadOptions.skip, loadOptions.take);
                        return query.toArray();
                    },
                    paginate: true,
                    pageSize: 20
                }),
                showPopupTitle: false,
                onSelectionChanged: function() {
                    controller.onChangeResponse(question);
                }
            }).dxLookup('instance');
        }

        if (widget) {
            widget.on('change', function() {
                controller.onChangeResponse(question);
            });
            widget.option('disabled', this.questionnaire.disabled);
            question.controls.push(widget);
        }

        if (question.type === 'measurement') {
        	var unitsElement = jQuery('[id^=questionMeasurementUnits_' + question.id + '_' + question.currentResponse + ']');
            var unitsWidget = unitsElement.dxSelectBox({
                dataSource: new DevExpress.data.ArrayStore({
                    data: question.units,
                    key: "id"
                }),
                displayExpr: "text",
                valueExpr: "id",
                value: question.unitDefault
            }).dxSelectBox('instance');
            unitsWidget.option('disabled', this.questionnaire.disabled); 
            
            question.controls.push(unitsWidget);
        }
    },

    /**
     * Loads and displays specified questionnaire.
     * @param questionnaireId The questionnaire ID.
     * @param disabled Boolean.  If true, all fields in Questionnaire are disabled in read-only mode.
     * @param surveyEventId activity_log_id
     * @param callback The callback function. If specified, this method calls it for each question,
     * and passes the question object as a parameter to the callback function.
     */
    loadQuestionnaire: function(questionnaireId, disabled, surveyEventId, callback) {
        this.questionnaire = QuestionnaireLoader.load(questionnaireId, disabled, surveyEventId);

        // render the questionnaire
        this.questionnaireForm.parentElement.innerHTML = '';
        var template = View.templates.get('questionnaireTemplate');
        template.render(this.questionnaire, this.questionnaireForm.parentElement);

        var controller = this;
        _.each(this.questionnaire.questions, function(question) {
            controller.attachQuestionEventListeners(question);

            if (callback) {
                callback(question);
            }
        });
        _.each(this.questionnaire.sections, function(section) {
            _.each(section.questions, function(question) {
                controller.attachQuestionEventListeners(question);

                if (callback) {
                    callback(question);
                }
            });
        });

        // update vertical scroll bar
        this.questionnaireForm.updateHeight();

        this.onChangeResponse();
    },

    /**
     * Called when the user changes the response for a question.
     * @param question
     */
    onChangeResponse: function(question) {
    	if (valueExistsNotEmpty(question)) clearValidationResult(question);
        var questions = this.questionnaire.getAllQuestions();
        var completedQuestions = _.filter(questions, function(question) {
            return question.isComplete();
        });
        var percentComplete = (100 * completedQuestions.length) / questions.length;

        var message = String.format(getMessage('progressMessage'), completedQuestions.length, questions.length, Math.round(percentComplete));
        jQuery('#questionnaireProgress').html(message);
    },

    /**
     * Called when the user selects an answer option.
     * @param questionId
     * @param answerOptionId
     */
    onSelectAnswerOption: function(question, answerOption, isChecked) {
        // update selected answer options
        if (question.type === 'multiple_choice_single') {
            question.selectedAnswerOptions = [answerOption];

        } else if (question.type === 'multiple_choice_multi') {
            if (isChecked) {
                question.selectedAnswerOptions.push(answerOption);
            } else {
                question.selectedAnswerOptions = _.without(question.selectedAnswerOptions, answerOption);
            }
        }

        this.loadChildQuestions(question, null);

        this.onChangeResponse(question);
    },

    /**
     * Loads and displays child questions for specified parent question.
     * @param question The parent question.
     * @param surveyEventId activity_log_id
     * @param callback The callback function. If specified, this method calls it for each question,
     * and passes the question object as a parameter to the callback function.
     */
    loadChildQuestions: function(question, surveyEventId, callback) {
        var childQuestions = QuestionnaireLoader.loadChildQuestions(question, surveyEventId);
        question.childQuestions = childQuestions;

        jQuery('#childQuestions_' + question.id).empty();

        if (question.childQuestions && question.childQuestions.length) {
            View.templates.get('childQuestionsTemplate').render(question, '#childQuestions_' + question.id);
        }

        var controller = this;
        _.each(childQuestions, function(childQuestion) {
            controller.attachQuestionEventListeners(childQuestion);

            childQuestion.currentResponse = 0;

            if (callback) {
                callback(childQuestion);
            }
        })

        // update vertical scroll bar
        this.questionnaireForm.updateHeight();
    },

    /**
     * Renders another response field for specified question.
     * @param question
     */
    addQuestionResponse: function(question, validateResponse) {
        clearValidationResult(question);
        if (valueExists(validateResponse) && validateResponse && !valueExistsNotEmpty(question.controls[0].option('value'))) {
            addInvalidHtmlField(question, getMessage('invalidValue'));
            return;
        }
        question.currentResponse++;
        // render another response
        var anotherResponseHtml = this.renderQuestionResponse(question);

        // insert the response just before the Add Another Response link
        var linkElement = jQuery('#addResponse_' + question.id).parent();
        linkElement.before(anotherResponseHtml);

        var responseElement = linkElement.prev();
        if (question.type === 'measurement') responseElement = responseElement.prev();
        this.createResponseWidget(question, responseElement);
    },

    /**
     * Checks in the document selected by the user as a response for specified question.
     * @param question
     */
    checkInQuestionDocument: function(question, answerId) {
        if (question.documentControl) {
            question.documentControl.checkInDocument(answerId);
        }
    }
});

/**
 * Test function: loads the questionnaire, expands some of the child questions, sets their response values.
 */
function testLoadQuestionnaire() {
    var controller = View.controllers.get('CompleteQuestionnaire');

    // load top-level questions
    controller.loadQuestionnaire(1, true, null, function(question) {
        console.log('Loaded question ' + question.id);

        // test: for the first question only
        if (question.id === '1') {
            // set the first answer option
            question.selectedAnswerOptions.push(question.answerOptions[0]);

            // load child questions for this answer option
            controller.loadChildQuestions(question, null, function(childQuestion) {
                console.log('Loaded child question ' + childQuestion.id);

                // test: for the first child question (lookup)
                if (childQuestion.id === '2') {
                    controller.addQuestionResponse(childQuestion);
                    childQuestion.setResponseValues(['ABBOT, PAUL', 'ADAMS, DEAN']);
                }

                // test: for the second child question (date)
                if (childQuestion.id === '3') {
                    controller.addQuestionResponse(childQuestion);
                    childQuestion.setResponseValues([new Date(), new Date()]);
                }
            });
        }
    });
}
