var multipleValueSeparator =  Ab.form.Form.MULTIPLE_VALUES_SEPARATOR;

function questionAnswersExist(question_id, questionnaire_id) {
	var answersExist = false;
	var restriction = new Ab.view.Restriction();
	if (question_id != null) restriction.addClause('quest_answer_ext.question_id', question_id);
	if (questionnaire_id != null) restriction.addClause('quest_answer_ext.questionnaire_id', questionnaire_id);
	var records = View.dataSources.get('questAnswersDataSource').getRecords(restriction);
	if (records.length > 0) answersExist = true;
	return answersExist;
}

function performRemoveQuestion(questionnaire_id, question_id) {
	var restriction = new Ab.view.Restriction();
	restriction.addClause('quest_question_ext.question_id', question_id);
	restriction.addClause('quest_question_ext.questionnaire_id', questionnaire_id);
	
	var ds = View.dataSources.get('questQuestionsDs');
	var record = ds.getRecord(restriction);
	ds.deleteRecord(record);		
}

// Delete parent question, all child questions and all answer options

function performDeleteQuestion(question_id) {
	var restriction = "'|' ${sql.concat} question_ext.hierarchy_ids LIKE '%|" + question_id + "|%'";
	
	var ds = View.dataSources.get('questionsDs');
	var records = ds.getRecords(restriction);
	for (var i=0;i<records.length;i++) {
		var record = records[i];
		deleteAnswers(record.getValue('question_ext.question_id'));
		ds.deleteRecord(record);
		View.updateProgressBar(0.5 + 0.5 * i / records.length);
	}
}

function deleteAnswers(question_id) {
	var answerDs = View.dataSources.get('answerOptionsDataSource');
	var answerRestriction = new Ab.view.Restriction();
	answerRestriction.addClause('quest_answer_option_ext.question_id', question_id);
	var answerRecords = answerDs.getRecords(answerRestriction);
	for (var i=0;i<answerRecords.length;i++) {
		answerDs.deleteRecord(answerRecords[i]);
	}
}

function setStatusFields(formId) {
	var form = View.panels.get(formId);
	form.setFieldValue('question_ext.edited_by', View.user.name);
		
	var currentDateObj = new Date();
	var ds = View.dataSources.get('questionsDataSource');
	var currentDate = ds.formatValue('question_ext.date_last_edit', currentDateObj, false);
	var currentTime = ds.formatValue('question_ext.time_last_edit', currentDateObj, false);
	form.setFieldValue('question_ext.date_last_edit', currentDate);
	form.setFieldValue('question_ext.time_last_edit', currentTime);
}

function getIsChildQuestion(hierarchy_ids) {
	var isChild = false;
	var lastIndex = hierarchy_ids.lastIndexOf('|');
	var secondLastIndex = hierarchy_ids.lastIndexOf('|', lastIndex - 1);
	if (secondLastIndex > 0) isChild = true;
	return isChild;
}

function getLastHierId(hierarchy_ids) {
	var lastIndex = hierarchy_ids.lastIndexOf('|');
	var index = hierarchy_ids.lastIndexOf('|', lastIndex - 1);
	var hier_id = hierarchy_ids.substring(index + 1, lastIndex);
	return hier_id;
}

function getSecondToLastHierId(hierarchy_ids) {	
	var lastIndex = hierarchy_ids.lastIndexOf('|');
	var secondLastIndex = hierarchy_ids.lastIndexOf('|', lastIndex - 1);
	var index = hierarchy_ids.lastIndexOf('|', secondLastIndex - 1);
	var hier_id = hierarchy_ids.substring(index + 1, secondLastIndex);
	return hier_id;
}

function replaceWithCopiedQuestionIds(hierarchy_ids, copiedQuestionIds) {
	hierarchy_ids = hierarchy_ids.replace(/\|/g, ';');
	hierarchy_ids = ";" + hierarchy_ids;
	for (var i=0;i<copiedQuestionIds.length;i++) {
		var old_id = ";" + copiedQuestionIds[i].old_question_id + ";";
		var new_id = ";" + copiedQuestionIds[i].new_question_id + ";";
		var regex = new RegExp(old_id, 'g');
		hierarchy_ids = hierarchy_ids.replace(regex, new_id);
	}
	hierarchy_ids = hierarchy_ids.replace(/;/g, '|');
	return hierarchy_ids.substring(1);
}

function getCopiedParentAnswerCondition(parent_answer_condition, copiedAnswerIds) {
	var reg = new RegExp(multipleValueSeparator, 'g');
	parent_answer_condition = parent_answer_condition.replace(reg, ';');
	parent_answer_condition = ";" + parent_answer_condition + ";";
	for (var i=0;i<copiedAnswerIds.length;i++) {
		var old_id = ";" + copiedAnswerIds[i].old_answer_option_id + ";";
		var new_id = ";" + copiedAnswerIds[i].new_answer_option_id + ";";
		var regex = new RegExp(old_id, 'g');
		parent_answer_condition = parent_answer_condition.replace(regex, new_id);
	}
	parent_answer_condition = parent_answer_condition.replace(/;/g, multipleValueSeparator);
	return parent_answer_condition.substring(3, parent_answer_condition.length-3);
}

function copyQuestion(record, newRecord) {
	var ds = View.dataSources.get('questionsDataSource');
	var currentDateObj = new Date();
	var copiedRecord = new Ab.data.Record({
		'question_ext.question_text': record.getValue('question_ext.question_text'),
		'question_ext.question_type': record.getValue('question_ext.question_type'),
		'question_ext.is_active': record.getValue('question_ext.is_active'),
		'question_ext.freeform_width': record.getValue('question_ext.freeform_width'),
		'question_ext.lookup_table': record.getValue('question_ext.lookup_table'),
		'question_ext.lookup_field': record.getValue('question_ext.lookup_field'),
		'question_ext.unit_type': record.getValue('question_ext.unit_type'),
		'question_ext.unit_default': record.getValue('question_ext.unit_default'),
		'question_ext.question_tag': record.getValue('question_ext.question_tag'),
		'question_ext.parent_answer_condition': record.getValue('question_ext.parent_answer_condition'),
		'question_ext.hierarchy_ids': record.getValue('question_ext.hierarchy_ids'),
		'question_ext.activity_type': record.getValue('question_ext.activity_type'),
		'question_ext.date_last_edit': ds.formatValue('question_ext.date_last_edit', currentDateObj, false),
		'question_ext.time_last_edit': ds.formatValue('question_ext.time_last_edit', currentDateObj, false),
		'question_ext.edited_by': View.user.name,
		'question_ext.question_id_old': record.getValue('question_ext.question_id'),
		'question_ext.sort_order_child': record.getValue('question_ext.sort_order_child'),
		'question_ext.is_required_child': record.getValue('question_ext.is_required_child'),
		'question_ext.num_responses_max': record.getValue('question_ext.num_responses_max')
	}, true);
	var copiedRecord = ds.saveRecord(copiedRecord);
	var new_question_id = copiedRecord.getValue('question_ext.question_id');
	if (newRecord) {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('question_ext.question_id', new_question_id);
		var questionRecord = ds.getRecord(restriction);
		var hierarchy_ids = record.getValue('question_ext.hierarchy_ids');
		hierarchy_ids += new_question_id + '|';
		questionRecord.setValue('question_ext.hierarchy_ids', hierarchy_ids);
		ds.saveRecord(questionRecord);
	}
	return new_question_id;
}

function copyAnswer(record, new_question_id) {
	var copiedRecord = new Ab.data.Record({
		'quest_answer_option_ext.question_id': new_question_id,
		'quest_answer_option_ext.answer_option_text': record.getValue('quest_answer_option_ext.answer_option_text'),
		'quest_answer_option_ext.sort_order': record.getValue('quest_answer_option_ext.sort_order')
	}, true);
	var ds = View.dataSources.get('answerOptionsDataSource');
	var copiedRecord = ds.saveRecord(copiedRecord);
	return copiedRecord.getValue('quest_answer_option_ext.answer_option_id');
}

function getAnswers(question_id) {
	var restriction = new Ab.view.Restriction();
	restriction.addClause('quest_answer_option_ext.question_id', question_id);
	var answers = View.dataSources.get('answerOptionsDataSource').getRecords(restriction);
	return answers;
}

/* Find the nearest preceding header */
function getHeaderId(questionnaire_id, question_id) {
	var header_id = '0';
	var sort_order = getSortOrder(questionnaire_id, question_id);
	var restriction = new Ab.view.Restriction();
	restriction.addClause('quest_question_ext.questionnaire_id', questionnaire_id);
	restriction.addClause('question_ext.question_type', 'label');
	restriction.addClause('quest_question_ext.sort_order', sort_order, '<=');
	var records = View.dataSources.get('questQuestionsDataSource').getRecords(restriction);
	if (records.length > 0) {
		header_id = records[records.length-1].getValue('quest_question_ext.question_id');
	}
	return header_id;
}

function getSortOrder(questionnaire_id, question_id) {
	var sort_order = 0;
	var restriction = new Ab.view.Restriction();
	restriction.addClause('quest_question_ext.questionnaire_id', questionnaire_id);
	restriction.addClause('quest_question_ext.question_id', question_id);
	var record = View.dataSources.get('questQuestionsDataSource').getRecord(restriction);
	if (record) sort_order = record.getValue('quest_question_ext.sort_order');
	return sort_order;
}

function getQuestionsInQuestionnaires(question_id, questionnaire_id) {
	var restriction = new Ab.view.Restriction();
	restriction.addClause('quest_question_ext.question_id', question_id);
	if (questionnaire_id) restriction.addClause('quest_question_ext.questionnaire_id', questionnaire_id);
	var records = View.dataSources.get('questQuestionsDataSource').getRecords(restriction);
	return records;
}

function validateQuestionFormFields(form, question_type) { //custom validation necessary for copying a child question
	if (question_type == 'lookup') {
		var fieldRestriction = new Ab.view.Restriction();
		fieldRestriction.addClause('afm_flds.table_name', form.getFieldValue('question_ext.lookup_table'));
		fieldRestriction.addClause('afm_flds.field_name', form.getFieldValue('question_ext.lookup_field'));
		var fieldRecords = View.dataSources.get('validateAfmFldsDs').getRecords(fieldRestriction);
		if (fieldRecords.length == 0) {
			View.alert(getMessage('enterLookupFields'));
			return false;
		}
	} else if (question_type == 'measurement') {
		var unitRestriction = new Ab.view.Restriction();
		unitRestriction.addClause('bill_unit.bill_type_id', form.getFieldValue('question_ext.unit_type'));
		unitRestriction.addClause('bill_unit.bill_unit_id', form.getFieldValue('question_ext.unit_default'));
		var unitRecords = View.dataSources.get('validateBillUnitsDs').getRecords(unitRestriction);
		if (unitRecords.length == 0) {
			View.alert(getMessage('enterUnitFields'));
			return false;
		}
	}
	return true;
}

function copyParentQuestion(controller, questionnaire_id, parent_question_id, parentQuestionRecord, replaceParentQuestion, sortOrder) {
	if (sortOrder == null) sortOrder = controller.highestSortOrder + 10;
	
	if (parentQuestionRecord == null) {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('question_ext.question_id', parent_question_id);
		parentQuestionRecord = View.dataSources.get('questionsDataSource').getRecord(restriction);
	}
	parentQuestionRecord.setValue('question_ext.hierarchy_ids', '');
	
	var copied_tag = parentQuestionRecord.getValue('question_ext.question_tag') + " (" + getMessage('copy') + ")";
    copied_tag = copied_tag.substring(0, 32);
	if (!replaceParentQuestion) parentQuestionRecord.setValue('question_ext.question_tag', copied_tag);
	
	var new_question_id = copyQuestion(parentQuestionRecord, true);
	
	var questRestriction = new Ab.view.Restriction();
	questRestriction.addClause('quest_question_ext.questionnaire_id', questionnaire_id);
	questRestriction.addClause('quest_question_ext.question_id', parent_question_id);
	var parentQuestQuestionRecord = View.dataSources.get('questQuestionsDataSource').getRecord(questRestriction);
	parentQuestQuestionRecord.setValue('quest_question_ext.question_id', new_question_id);
		
	if (replaceParentQuestion) {
		View.dataSources.get('questQuestionsDataSource').saveRecord(parentQuestQuestionRecord);
	} else {
		copyQuestQuestion(parentQuestQuestionRecord, sortOrder, null);
	}	

	return new_question_id;
}

function copyQuestQuestion(record, sortOrder, new_questionnaire_id) {
	var ds = View.dataSources.get('questQuestionsDataSource');
	var questionnaire_id = record.getValue('quest_question_ext.questionnaire_id');
	if (new_questionnaire_id) questionnaire_id = new_questionnaire_id;
	var copiedRecord = new Ab.data.Record({
		'quest_question_ext.sort_order': sortOrder,
		'quest_question_ext.questionnaire_id': questionnaire_id,
		'quest_question_ext.question_id': record.getValue('quest_question_ext.question_id'),
		'quest_question_ext.is_required': record.getValue('quest_question_ext.is_required'),
		'quest_question_ext.display_format': record.getValue('quest_question_ext.display_format'),
		'quest_question_ext.show_answer_text': record.getValue('quest_question_ext.show_answer_text'),
		'quest_question_ext.question_prefix': record.getValue('quest_question_ext.question_prefix')
	}, true);        
	ds.saveRecord(copiedRecord);
}

function onTreeExpandAll(treePanel, treeNode){
	if (!valueExists(treeNode)) {
		treeNode = treePanel.treeView.getRoot();
	}
	if (!treeNode.isRoot()) {
		if (!treeNode.expanded) {
			//treePanel.refreshNode(treeNode);
			treeNode.expand();
		}
	}
    if(treeNode.children.length > 0){ 
        for(var i=0; i<treeNode.children.length; i++){//hierTree does not return children beyond top-level
        	var node = null;
            node = treeNode.children[i];
            onTreeExpandAll(treePanel, node);
        }
    } else {
    	//treeNode.isLeafNode = true;
    }
}

/**
 *  ab-ac-edit.js
 *  refresh tree based on parent node, expand the child 
 *  in case the action is done on it.  used after saving question or deleting child from form.
 */
function refreshTreeAndExpandParent(panelId, isNewRecord, questionId, parentQuestionId){
	var treePanel = View.panels.get(panelId);
	var parentNode = getParentNode(treePanel, isNewRecord, questionId);
	if(parentNode == null || parentNode.isRoot()){
		refreshHierTree(panelId, parentQuestionId);
	}else{
		treePanel.expandNode(parentNode);
	}	
}

function refreshHierTree(panelId, parent_question_id) {
	var tree = View.panels.get(panelId);
	tree.addParameter('questionId', parent_question_id);
	tree.addParameter('multipleValueSeparator', multipleValueSeparator);
	tree.refresh();
	tree.show();
	onTreeExpandAll(tree, null);
}

/*
 * ab-ac-edit.js
 * for new records, returns lastNodeClicked or root if no click was done
 * for existing records, returns the parent of the record edited
 */
function getParentNode(treePanel, isNewRecord, questionId){//TODO: returning null for parentNode when edit first child and click save twice?
	var currentNode = treePanel.lastNodeClicked;
	if (isNewRecord) {
		if(currentNode!=null){
			return treePanel.lastNodeClicked;
		}else{
			return treePanel.treeView.getRoot();
		}
		
	} else {
		if(currentNode != null){
			for(var i=0;i<currentNode.children.length;i++){
				var tmpNode = currentNode.children[i];
				if(tmpNode.data["question_ext.question_id"] == questionId){
					currentNode = tmpNode;
					break;
				}
			}
			return currentNode.parent;
		}else{
			return treePanel.treeView.getRoot();
		}
	}	
}

function isMultipleChoice(question_id) {
	var isMultipleChoice = false;
	var form = View.panels.get('questionForm');
	var question_type = '';
	if (!valueExists(question_id)) {
		question_type = form.getFieldValue('question_ext.question_type');	
	} else {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('question_ext.question_id', question_id);
		var record = View.dataSources.get('questionsDataSource').getRecord(restriction);
		question_type = record.getValue('question_ext.question_type');
	}
	if (question_type && question_type.indexOf('multiple') >= 0) {
		isMultipleChoice = true;
	}
	return isMultipleChoice;
}

function isHeaderQuestion(obj) {
	var isHeader = false;
	var question_type = obj.getFieldValue('question_ext.question_type');
	if (question_type == 'label') isHeader = true;
	return isHeader;
}

function refreshAnswerOptionsGrid(grid, question_id) {
	if (question_id != '') {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('quest_answer_option_ext.question_id', question_id);
		grid.refresh(restriction);
	} else grid.refresh("1=2");
}

function setShowAnswerText(panelId, checked) {
	var form = View.panels.get(panelId);
	if (checked) {
		form.setFieldValue('quest_question_ext.show_answer_text', '0');
		form.enableField('quest_question_ext.display_format', false);
		form.setFieldValue('quest_question_ext.display_format', 'Short Form');
	} else {
		form.setFieldValue('quest_question_ext.show_answer_text', '1');
		form.enableField('quest_question_ext.display_format', true);
	}
}

function selectValueParentAnswer() {
	var parent_id = View.panels.get('questionForm').getFieldValue('question_ext.parent_id');
	var restriction = new Ab.view.Restriction();
	restriction.addClause('quest_answer_option_ext.question_id', parent_id);
	View.selectValue({
		formId: 'questionForm',
		title: getMessage('selectParentAnswer'),
		fieldNames: ['question_ext.parent_answer_condition'],
		selectTableName: 'quest_answer_option_ext',
		selectFieldNames: ['quest_answer_option_ext.answer_option_id'],
		visibleFieldNames: ['quest_answer_option_ext.answer_option_text', 'quest_answer_option_ext.sort_order', 'quest_answer_option_ext.answer_option_id', 'quest_answer_option_ext.question_id'],
		actionListener: 'afterSelectParentAnswer',
		selectValueType: 'multiple',
		sortFieldNames: ['quest_answer_option_ext.sort_order'],
		restriction: restriction,
		width: 1000,
		height: 500
		});
}

function afterSelectParentAnswer(fieldName, selectedValue, previousValue) {
	var form = View.panels.get('questionForm');
    if (fieldName == 'question_ext.parent_answer_condition') {
    	form.setFieldValue('question_ext.parent_answer_condition', selectedValue);
    	setParentAnswerText(form);
    }    
    return true;
}

function setParentAnswerText(form) {
	var parent_answer_condition = form.getFieldValue('question_ext.parent_answer_condition');
	var values = parent_answer_condition.split(multipleValueSeparator);
	var answer_option_text = '';
	if (parent_answer_condition != '') {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('quest_answer_option_ext.answer_option_id', values, 'IN');
		var records = View.dataSources.get('answerOptionsDs').getRecords(restriction);
		answer_option_text = getDisplayAnswerOptions(records);
	}
	form.setFieldValue('question_ext.parent_answer_select', answer_option_text);
}

function getDisplayAnswerOptions(records) {
	var text = '';
	for (var i=0;i<records.length;i++) {
		text += (text == '')?'':multipleValueSeparator;
		text += records[i].getValue('quest_answer_option_ext.answer_option_text');
	}
	return text;
}

function setQuestionTitle(form, questQuestionForm, question_id, isAddChild, isHeader, isChildQuestion, isChildActionQuestion) {
	var title = '';
	if (isAddChild) {
		title = String.format(getMessage('panelTitleChild'), question_id);
	} else {
		title = getMessage('panelTitleQuestion');
		if (isHeader) title = getMessage('panelTitleHeader');
		if (isChildQuestion) title = getMessage('panelTitleFollowUpQuestion');
		if (isChildActionQuestion) title = getMessage('panelTitleFollowUpAction');
	}		
	form.setTitle(title);
	
	if (!isChildQuestion && questQuestionForm) {
		var questQuestionTitle = getMessage('panelTitleQuestQuestion');
		if (isHeader) questQuestionTitle = getMessage('panelTitleQuestQuestionHeader');
		questQuestionForm.setTitle(questQuestionTitle);
	}
}

/* If the user is saving a multiple-choice question with Answer Position "Header" (i.e. quest_question_ext.show_answer_text = 0), 
 * and there are no Answer Options defined, the Answer Options, if any, 
 * will be copied from the nearest preceding Header. */
function copyHeaderAnswers(questQuestionForm, questionnaire_id, question_id) {
	var show_answer_text = questQuestionForm.getFieldValue('quest_question_ext.show_answer_text');
	if (show_answer_text == '0') {
		if (getAnswers(question_id).length == 0) {
			var header_id = getHeaderId(questionnaire_id, question_id);
			if (header_id != '0') {
				var headerAnswers = getAnswers(header_id);
				for (var i=0;i<headerAnswers.length;i++) {
					var record = headerAnswers[i];
					copyAnswer(record, question_id);
				}
				View.panels.get('answerOptionsGrid').refresh();
			}
		}
	}
}

function beforeSaveQuestionForm(controller, formId) {
	var form = View.panels.get(formId);
	if (!form.canSave()) return false;
	if (controller.isChildQuestion) {
		var parent_answer_condition = form.getFieldValue('question_ext.parent_answer_condition');
		if (parent_answer_condition == '') {
			var message = getMessage('noParentAnswerCondition');
			if (controller.isChildActionQuestion) message = getMessage('noParentAnswerConditionForAction');
			View.alert(message);
			return false;
		}
	}

	var question_type = form.getFieldValue('question_ext.question_type');
	if (question_type == '') {
		View.alert(getMessage('noQuestionType'));
		return false;
	}
	
	if (!validateQuestionFormFields(form, question_type)) return false;
	setStatusFields(formId);
	clearUnusedFields(formId);
	return true;
}

function clearUnusedFields(formId) {
	var form = View.panels.get(formId);
	var question_type = form.getFieldValue('question_ext.question_type');
	if (question_type != 'lookup') {
		form.setFieldValue('question_ext.lookup_table', '');
		form.setFieldValue('question_ext.lookup_field', '');
	}
	if (question_type != 'measurement') {
		form.setFieldValue('question_ext.unit_type', '');
		form.setFieldValue('question_ext.unit_default', '');
	}
	if (question_type != 'action') {
		form.setFieldValue('question_ext.activity_type', '');
	}
}

function addToHierarchyIds(form, question_id) {
	var hierarchy_ids = form.getFieldValue('question_ext.hierarchy_ids');
	hierarchy_ids += question_id + '|';
	return hierarchy_ids;
}

/* updateType: 'copyQuestion', 'updateParent', 'updateChild', 'addChild', 'deleteChild', 'deleteAnswer', 'updateAnswer', 'addAnswer' */
function getRefreshQuestionRestriction(updateType, form, parent_question_id, copiedQuestionIds, childQuestionRecord) {
	var question_id = parent_question_id;
	switch(updateType) {
		case 'copyQuestion': case 'updateParent': case 'deleteChild':
			question_id = parent_question_id;
			break;
		case 'updateChild':
			question_id = getNewCopiedId(copiedQuestionIds, 'question_id', form.getFieldValue('question_ext.question_id'));
			break;
		case 'addChild': 
			question_id = childQuestionRecord.getValue('question_ext.question_id');
			break;
		case 'deleteAnswer': case 'updateAnswer': case 'addAnswer':
			question_id = getNewCopiedId(copiedQuestionIds, 'question_id', form.getFieldValue('question_ext.question_id'));
			break;
	}
	var restriction = new Ab.view.Restriction();
	restriction.addClause('question_ext.question_id', question_id);
	return restriction;
}

function getNewCopiedId(copiedIds, fieldName, old_id) {
	var new_id = 0;
	for (var i=0;i<copiedIds.length;i++) {
		if (copiedIds[i]['old_' + fieldName] == old_id) {
			new_id = copiedIds[i]['new_' + fieldName];
			break;
		}
	}
	return new_id;
}

function deleteAnswerOption(answer_option_id) {
	var answerDs = View.dataSources.get('answerOptionsDataSource');
	var answerRestriction = new Ab.view.Restriction();
	answerRestriction.addClause('quest_answer_option_ext.answer_option_id', answer_option_id);
	var answerRecord = answerDs.getRecord(answerRestriction);
	answerDs.deleteRecord(answerRecord);
}

function questionnaireAssignedToRequirement(questionnaire_id) {
	var assignedToRequirement = false;
	var restriction = new Ab.view.Restriction();
	restriction.addClause('regrequirement.questionnaire_id', questionnaire_id);
	var records = View.dataSources.get('regRequirementDs').getRecords(restriction);
	if (records.length > 0) assignedToRequirement = true;
	return assignedToRequirement;
}

function copyQuestionnaire(questionnaire_id) {
	View.openProgressBar(getMessage('copying'));
	var ds = View.dataSources.get('questionnairesDataSource');
	var restriction = new Ab.view.Restriction();
	restriction.addClause('questionnaire_ext.questionnaire_id', questionnaire_id);
	var record = ds.getRecord(restriction);
	var questionnaire_title = record.getValue('questionnaire_ext.questionnaire_title');
	var copied_title = questionnaire_title + " (" + getMessage('copy') + ")";
    copied_title = copied_title.substring(0, 96);
	
	var currentDateObj = new Date();
	var copiedRecord = new Ab.data.Record({
		'questionnaire_ext.questionnaire_title': copied_title,
		'questionnaire_ext.status': record.getValue('questionnaire_ext.status'),
		'questionnaire_ext.description': record.getValue('questionnaire_ext.description'),
		'questionnaire_ext.date_effective_start': record.getValue('questionnaire_ext.date_effective_start'),
		'questionnaire_ext.date_effective_end': record.getValue('questionnaire_ext.date_effective_end'),
		'questionnaire_ext.date_last_edit': ds.formatValue('question_ext.date_last_edit', currentDateObj, false),
		'questionnaire_ext.edited_by': View.user.name
	}, true);
	var copiedRecord = ds.saveRecord(copiedRecord);
	View.closeProgressBar();
	return copiedRecord.getValue('questionnaire_ext.questionnaire_id');
}

function copyQuestQuestions(old_questionnaire_id, new_questionnaire_id) {
	var sortOrder = 10;
	var ds = View.dataSources.get('questQuestionsDs');
	var restriction = new Ab.view.Restriction();
	restriction.addClause('quest_question_ext.questionnaire_id', old_questionnaire_id);
	var records = ds.getRecords(restriction);
	for (var i=0;i<records.length;i++) {
		copyQuestQuestion(records[i], sortOrder, new_questionnaire_id);
		sortOrder += 10;
	}
}

function getMessageListQuestionnaires(msgId, questions) {
	var titles = '';
	for (var i=0;i<questions.length;i++) {
		if (titles != '') titles += ', ';
		titles += questions[i].getValue('questionnaire_ext.questionnaire_title');
	}
	var message = String.format(getMessage(msgId), titles);
	return message;
}

function getHierarchyIds(questionId) {
	var restriction = new Ab.view.Restriction();
	restriction.addClause('question_ext.question_id', questionId);
	var record = View.dataSources.get('questionsDataSource').getRecord(restriction);
	return record.getValue('question_ext.hierarchy_ids');
}

function isWorkflowRuleAction(activity_type) {
	var isWorkflow = false;
	if (activity_type != '') {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('activitytype.activity_type', activity_type);
		var record = View.dataSources.get('activityTypesDs').getRecord(restriction);
		if (record.getValue('activitytype.action_wfr') != '') isWorkflow = true;
	}
	return isWorkflow;
}
