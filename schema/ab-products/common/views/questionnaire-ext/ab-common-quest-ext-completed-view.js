var commonQuestExtCompletedViewController = View.createController('commonQuestExtCompletedView', {
	questionnaire_id: null,
	survey_event_id: 0,
	
	selectQuestionnaire: function(obj) {
		var questionnaire_id = obj.restriction['questionnaire_ext.questionnaire_id'];
		this.questionnaire_id = questionnaire_id;
		this.commonQuestExtCompletedView_surveyEventsGrid.addParameter('questionnaireId', questionnaire_id);
		this.commonQuestExtCompletedView_surveyEventsProfile.addParameter('questionnaireId', questionnaire_id);
		this.commonQuestExtCompletedView_surveyEventsGrid.refresh();
	},
	
	selectEvent: function(obj) {
		var survey_event_id = obj.restriction['activity_log.activity_log_id'];
		this.commonQuestExtCompletedView_answersGrid.refresh("quest_answer_ext.survey_event_id=" + survey_event_id);
		this.commonQuestExtCompletedView_answersGrid.appendTitle(getDescriptiveTitle(obj.restriction, this.questionnaire_id));
		//this.commonQuestExtCompletedView_surveyEventsProfile.refresh(obj.restriction);
		this.survey_event_id = obj.restriction['activity_log.activity_log_id'];
		
		if (this.questionnaire_id != null) {
			var questTab = View.panels.get('commonQuestExtCompletedView_tabs').findTab('commonQuestExtCompletedView_tab1');
			if (questTab.isContentLoaded) {
				loadQuestionnairePanel();
			} else {
				setTimeout(loadQuestionnairePanel, 5000);
			}
		}
	}	
});

function loadQuestionnairePanel() {
	var controller = View.controllers.get('commonQuestExtCompletedView');
	var questTab = View.panels.get('commonQuestExtCompletedView_tabs').findTab('commonQuestExtCompletedView_tab1');
	if (questTab.isContentLoaded) {
		questTab.getContentFrame().View.controllers.get('UpdateQuestionnaire').loadQuestionnaireWithResults(controller.questionnaire_id, true, controller.survey_event_id);
	}
}

function getDescriptiveTitle(restriction, questionnaire_id) {
	var record = View.dataSources.get('eventsDataSource').getRecord(restriction);
	var questRecord = View.dataSources.get('questionnairesDataSource').getRecord('questionnaire_ext.questionnaire_id = ' + questionnaire_id);
	var message = questRecord.getValue('questionnaire_ext.questionnaire_title');
	message += ' - ' + record.getLocalizedValue('activity_log.date_scheduled') + ' ' + record.getValue('activity_log.action_title');
	return message;
	
	
}