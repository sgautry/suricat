<!-- The Define Questionnaire reusable view.
     See https://confluence.archibus.zone/display/AP/Cross-App+-+Extended+Questionnaires+-+Bali6+Functional+Specifications -->
<view version="2.0">
    <!-- title>Define Questionnaire</title-->
    <js file="ab-common-quest-ext-def-questionnaire.js"/>
    
    <style type="text/css">    
        .x-panel-body-noheader,.x-panel-mc .x-panel-body{
            border-top:0px solid #99bbe8;
            border-right:0px solid #99bbe8;
            border-left:0px solid #99bbe8;
            border-bottom:0px solid #99bbe8;
        }        
        .panelToolbar.x-toolbar .ytb-text{
            font-size: 13px;
        }
        .columnReport{
            padding-top: 5px;
        }
        .columnReport td {
            border-bottom:0px solid #99bbe8;
            padding:0px 4px 5px;
        }
        .columnReportValue {
            color: #4D4B4B;
        }
        .columnReportLabel {
            font-weight: bold;
        }
    </style>
    
    <layout type="borderLayout" id="mainLayout">
        <!-- north id="questionnairesFilterRegion" initialSize="80px" split="true"/-->
        <west initialSize="22%" split="true"/>
        <center autoScroll="false"/>
        <east initialSize="28%" split="true" id="formsRegion"/>
    </layout>
    <layout id="nestedWest" containingLayout="mainLayout" region="west">
        <north id="questionnairesListRegion"  initialSize="40%" split="true"/>
        <center id="questionAddRegion" autoScroll="true"/>
    </layout>
    <layout id="nestedCenter" containingLayout="mainLayout" region="center">
        <center id="questionsListRegion"  autoScroll="false"/>
        <south id="questionTreeRegion" initialSize="25%" split="true"/>
    </layout>

    <panel type="view" id="commonView" file="ab-common-quest-ext-def-questionnaire-common.axvw"/>
    
    <dataSource id="questionSelectDataSource">        
        <table name="question_ext"/>               
        <field name="question_id"/>
        <field name="question_text"/>
        <field name="question_type"/>        
        <field name="is_active"/>
        <field name="freeform_width"/>
        <field name="freeform_width_display" table="question_ext" dataType="number" decimals="0">
            <title>Freeform Width</title>
            <sql dialect="generic">CASE WHEN question_type='freeform' THEN freeform_width ELSE NULL END</sql>
        </field>      
        <field name="lookup_table"/>
        <field name="lookup_field"/>
        <field name="unit_type"/>
        <field name="unit_default"/>
        
        <field name="question_tag"/>
        <field name="parent_answer_condition"/>
        <field name="hierarchy_ids"/>
        <field name="activity_type"/>
        <field name="date_last_edit"/>
        <field name="time_last_edit"/>
        <field name="edited_by"/>
        <field name="num_responses_max"/>
        <restriction type="sql" sql="hierarchy_ids NOT LIKE '%|%|%'"/>
    </dataSource>
    
    <dataSource id="questionElementDataSource">
        <sql dialect="generic">SELECT 0 ${sql.as} question_id, 'label' ${sql.as} question_type, ${parameters['label']} ${sql.as} question_text, ${parameters['desc_label']} ${sql.as} question_text_01 FROM afm_tbls
	        UNION (SELECT 1 ${sql.as} question_id, 'multiple_choice_single' ${sql.as} question_type, ${parameters['multiple_choice_single']} ${sql.as} question_text, ${parameters['desc_multiple_choice_single']} ${sql.as} question_text_01 FROM afm_tbls)
	        UNION (SELECT 2 ${sql.as} question_id, 'freeform' ${sql.as} question_type, ${parameters['freeform']} ${sql.as} question_text, ${parameters['desc_freeform']} ${sql.as} question_text_01 FROM afm_tbls)
	        UNION (SELECT 3 ${sql.as} question_id, 'date' ${sql.as} question_type,${parameters['date']} ${sql.as} question_text, ${parameters['desc_date']} ${sql.as} question_text_01 FROM afm_tbls)
	        UNION (SELECT 4 ${sql.as} question_id, 'time' ${sql.as} question_type,${parameters['time']} ${sql.as} question_text, ${parameters['desc_time']} ${sql.as} question_text_01 FROM afm_tbls)
	        UNION (SELECT 5 ${sql.as} question_id, 'lookup' ${sql.as} question_type,${parameters['lookup']} ${sql.as} question_text, ${parameters['desc_lookup']} ${sql.as} question_text_01 FROM afm_tbls)
	        UNION (SELECT 6 ${sql.as} question_id, 'count' ${sql.as} question_type,${parameters['count']} ${sql.as} question_text, ${parameters['desc_count']} ${sql.as} question_text_01 FROM afm_tbls)
	        UNION (SELECT 7 ${sql.as} question_id, 'measurement' ${sql.as} question_type,${parameters['measurement']} ${sql.as} question_text, ${parameters['desc_measurement']} ${sql.as} question_text_01 FROM afm_tbls)
	        UNION (SELECT 8 ${sql.as} question_id, 'document' ${sql.as} question_type,${parameters['document']} ${sql.as} question_text, ${parameters['desc_document']} ${sql.as} question_text_01 FROM afm_tbls)
	 	</sql>
        <table name="afm_tbls"/>
        <field name="question_type" hidden="true" dataType="text"/>
        <field name="question_text" dataType="text">
            <title>Question Type</title>
        </field>
        <field name="question_text_01" dataType="text"/>
        <field name="question_id" hidden="true" dataType="number" decimals="0"/>
        <sortField name="question_id"/>
        <parameter name="label" dataType="text" value="Header"/>
	    <parameter name="multiple_choice_single" dataType="text" value="Multiple Choice"/>
	    <parameter name="freeform" dataType="text" value="Freeform"/>
	    <parameter name="date" dataType="text" value="Date"/>
	    <parameter name="time" dataType="text" value="Time"/>
	    <parameter name="lookup" dataType="text" value="Lookup"/>
	    <parameter name="count" dataType="text" value="Count"/>
	    <parameter name="measurement" dataType="text" value="Measurement"/>
	    <parameter name="document" dataType="text" value="Document/Photo"/>
	    <parameter name="action" dataType="text" value="Action"/>
	    <parameter name="desc_label" dataType="text" value="Header bar"/>
	    <parameter name="desc_multiple_choice_single" dataType="text" value="Radio buttons/checkboxes"/>        
        <parameter name="desc_freeform" dataType="text" value="Text field"/>
        <parameter name="desc_date" dataType="text" value="Date field"/>
        <parameter name="desc_time" dataType="text" value="Time field"/>
        <parameter name="desc_lookup" dataType="text" value="Lookup a value in a table"/>
        <parameter name="desc_count" dataType="text" value="Integer field"/>
        <parameter name="desc_measurement" dataType="text" value="Measurement field"/>
        <parameter name="desc_document" dataType="text" value="Upload Document/Photo"/>
        <parameter name="desc_action" dataType="text" value="Create an Action Item"/>
   </dataSource>
    
    <!-- panel type="console" id="questionnaireFilter" dataSource="questionnairesDataSource" columns="3" layoutRegion="questionnairesFilterRegion">
        <title>Filter</title>
        <action id="filter">
            <title>Filter</title>
        </action>
        <action id="clear">
            <title>Clear</title>
            <command type="showPanel" panelId="questionnaireList" clearRestriction="true"/>
            <command type="clearPanel"/>
        </action>
        <field name="edited_by"/>       
        <field name="status"/>        
    </panel-->

    <panel type="columnReport" id="questionnaireReport" columns="1" bodyCssClass="panel-light" dataSource="questionnairesDataSource" showOnLoad="false" layoutRegion="questionnairesListRegion">
        <title translatable="true">Selected Questionnaire</title>
        <action id="select">
            <title>Select</title>
            <command type="callFunction" functionName="selectQuestionnaire"/>
        </action>
        <field name="questionnaire_title" table="questionnaire_ext" readOnly="true" />
        <field name="edited_by" readOnly="true" />
        <field name="date_last_edit" readOnly="true" />
        <field name="questionnaire_id" table="questionnaire_ext" hidden="true"/>
    </panel>

    <panel type="grid" id="questionnaireList" dataSource="questionnairesDataSource" showOnLoad="false" layoutRegion="questionnairesListRegion">
        <title>Select Questionnaire</title>        
        <action id="addNew">
            <title translatable="true">Add</title>
            <tooltip>Add New Questionnaire</tooltip>
        </action>
        <action type="menu" id="questionnaireList_toolsMenu" imageName="/schema/ab-core/graphics/icons/view/export.png">
            <tooltip>Export</tooltip>
            <action id="exportDOCX">
                <title>Export to DOCX</title>
                <command type="exportPanel" outputType="docx" orientation="landscape" panelId="questionnaireList"/>
            </action>
            <action id="exportXLS">
                <title>Export to XLS</title>
                <command type="exportPanel" outputType="xls" panelId="questionnaireList"/>
            </action>
            <action id="exportPDF">
                <title>Export to PDF</title>
                <command type="exportPanel" outputType="pdf" orientation="landscape" panelId="questionnaireList"/>
            </action>
        </action> 
        <event type="onClickItem">
            <command type="callFunction" functionName="abCommonQuestExtQuestionnaireDefController.selectQuestionnaire(this)"/>            
        </event>  
        <field name="questionnaire_title">
            <tooltip>Select Questionnaire</tooltip>
        </field>
        <field controlType="image" id="editQuestionnaire" imageName="/schema/ab-core/graphics/icons/view/edit.png">
            <tooltip>Edit Questionnaire Profile</tooltip>
        </field>
        <field controlType="image" id="copyQuestionnaire" imageName="/schema/ab-core/graphics/icons/add.png">
            <tooltip>Copy Questionnaire</tooltip>
        </field>
        <field controlType="image" id="deleteQuestionnaire" imageName="/schema/ab-core/graphics/icons/cross.png">
            <tooltip>Delete Questionnaire</tooltip>
        </field>
        
        <field name="status"/>
        <field name="edited_by"/>
        <field name="date_last_edit">
            <title>Date Edited</title>
        </field>
        <field name="description"/>
        <field name="questionnaire_id"/>
    </panel>
    
    <panel type="columnReport" id="questionnaireProfile" columns="3" bodyCssClass="panel-light" dataSource="questionnairesDataSource" showOnLoad="false" layoutRegion="questionsListRegion">
        <title translatable="true">Questions List</title>      
        <action id="assign" hidden="true" mainAction="false">
            <title>Assign</title>
            <command type="callFunction" functionName="assignQuestionnaire"/>
        </action>
        <action id="profile">
            <title>Profile</title>
            <tooltip>Edit Questionnaire Profile</tooltip>
        </action>
        <action id="preview" mainAction="true">
            <title>Preview</title>
            <tooltip>Preview Questionnaire</tooltip>
            <command type="callFunction" functionName="abCommonQuestExtQuestionnaireDefController.previewQuestionnaire()"/>
        </action>
        <action type="menu" id="questionsList_addMenu" imageName="/schema/ab-core/graphics/icons/view/add-icon.png">
            <tooltip>Add New Question</tooltip>
            <action id="addHeader">
                <title>Header</title>
                <command type="callFunction" functionName="questionsList_addQuestion('label')"/>
            </action>
            <action id="addMultipleChoice">
                <title>Multiple Choice</title>
                <command type="callFunction" functionName="questionsList_addQuestion('multiple_choice_single')"/>
            </action>
            <action id="addFreeform">
                <title>Freeform</title>
                <command type="callFunction" functionName="questionsList_addQuestion('freeform')"/>
            </action>
            <action id="addDate">
                <title>Date</title>
                <command type="callFunction" functionName="questionsList_addQuestion('date')"/>
            </action>
            <action id="addTime">
                <title>Time</title>
                <command type="callFunction" functionName="questionsList_addQuestion('time')"/>
            </action>
            <action id="addLookup">
                <title>Lookup</title>
                <command type="callFunction" functionName="questionsList_addQuestion('lookup')"/>
            </action>
            <action id="addCount">
                <title>Count</title>
                <command type="callFunction" functionName="questionsList_addQuestion('count')"/>
            </action>
            <action id="addMeasurement">
                <title>Measurement</title>
                <command type="callFunction" functionName="questionsList_addQuestion('measurement')"/>
            </action>
            <action id="addDoc">
                <title>Document/Photo</title>
                <command type="callFunction" functionName="questionsList_addQuestion('document')"/>
            </action>
        </action>
        <action type="menu" id="questionsList_toolsMenu" imageName="/schema/ab-core/graphics/icons/view/export.png">
            <tooltip>Export</tooltip>
            <action id="exportDOCX">
                <title>Export to DOCX</title>
                <command type="exportPanel" outputType="docx" orientation="landscape" panelId="questionsList"/>
            </action>
            <action id="exportXLS">
                <title>Export to XLS</title>
                <command type="exportPanel" outputType="xls" panelId="questionsList"/>
            </action>
            <action id="exportPDF">
                <title>Export to PDF</title>
                <command type="exportPanel" outputType="pdf" orientation="landscape" panelId="questionsList"/>
            </action>
        </action>
        <field name="description" colspan="3" readOnly="true" hidden="${record['questionnaire_ext.description'] == ''}"/>
        <field name="date_effective_start" readOnly="true" />
        <field name="date_effective_end" readOnly="true" hidden="false"/>
        <field name="status" readOnly="true"/>
        <field name="questionnaire_title" alias="additionalText" colspan="3">
            <title> </title>
        </field>
        <field name="questionnaire_id" table="questionnaire_ext" hidden="true"/>
    </panel>  

    <panel type="grid" id="questionsList" dataSource="questQuestionsDataSource" showOnLoad="false" layoutRegion="questionsListRegion">
        <sortField name="sort_order" table="quest_question_ext"/>
        <indexField name="question_prefix" table="quest_question_ext"/>
        <event type="onClickItem">
            <command type="callFunction" functionName="selectQuestion"/>
        </event>
        <field name="question_prefix" table="quest_question_ext"/> 
        <field name="question_text" table="question_ext" controlType="link"/> 
        <field name="question_tag" table="question_ext" controlType="link"/>      
        <field controlType="image" id="moveUp" imageName="/schema/ab-core/graphics/icons/view/arrow-up.png">
            <tooltip>Move Up</tooltip>
        </field>
        <field controlType="image" id="moveDown" imageName="/schema/ab-core/graphics/icons/view/arrow-down.png">
            <tooltip>Move Down</tooltip>
        </field>
        <field controlType="image" id="copyAsNew" imageName="/schema/ab-core/graphics/icons/add.png">
            <tooltip>Copy and add to Questionnaire</tooltip>
        </field>
        <field controlType="image" id="deleteQuestion" imageName="/schema/ab-core/graphics/icons/cross.png">
            <tooltip>Delete</tooltip>
        </field>
        <field name="is_required" controlType="link"/>                         
        <field name="question_type" table="question_ext" controlType="link"/>
        <field name="show_answer_text_multiple" table="quest_question_ext" dataType="text" controlType="link">
            <title>Show Answer Text</title>
        </field>              
        <field name="display_format" controlType="link"/>
        <field name="show_answer_text" hidden="true"/>
        <field name="num_responses"  table="question_ext" dataType="number" decimals="0" controlType="link">
            <title>Maximum Number of Responses</title>
        </field>
        <field name="freeform_width" table="question_ext" hidden="true"/> 
        <field name="freeform_width_display" table="question_ext" dataType="number" decimals="0" controlType="link">
            <title>Freeform Width</title>
        </field>   
        <field name="lookup_table" table="question_ext" controlType="link"/>
        <field name="lookup_field" table="question_ext" controlType="link"/>
        <field name="unit_type" table="question_ext" controlType="link"/>
        <field name="unit_default" table="question_ext" controlType="link"/>
                     
        <field name="is_active" table="question_ext" controlType="link"/>
        <field name="edited_by" table="question_ext" controlType="link"/>
        <field name="date_last_edit" table="question_ext" controlType="link"/>
        <field name="time_last_edit" table="question_ext" controlType="link"/>
         
        <field name="questionnaire_id" table="quest_question_ext" controlType="link"/>
        <field name="question_id" table="quest_question_ext" controlType="link"/>
        <field name="question_id_old" table="question_ext" controlType="link"/>
        <field name="sort_order" hidden="false" controlType="link"/>
    </panel>

    <tabs id="defQuestionnaireTabs" layoutRegion="questionAddRegion" tabRefreshPolicy="never">
        <tab name="defQuestionnaireTab1" selected="true">
	        <title>Add New</title>
		    <panel type="grid" id="questionCreatePanel" showIndexAndFilterOnLoad="false" dataSource="questionElementDataSource" showOnLoad="false">
		        <instructions>Drag and Drop a Question to the Questions List.</instructions>
		        <sortField name="question_id"/>
		        <field controlType="image" imageName="/schema/ab-core/graphics/icons/move-icon.png">
		            <tooltip>Drag and drop Question Type to Questions List</tooltip>
		        </field>
		        <field name="question_text">
		            <title>Question Type</title>
		            <!-- command type="callFunction" functionName="abCommonQuestExtQuestionnaireDefController.questionCreatePanel_onAddNew(this)"/-->
		        </field>
		        <field name="question_text_01">
		            <title>Description</title>
		        </field>
		        <field name="question_type" hidden="true"/>
		        <field name="question_id" hidden="true"/>
		    </panel>
	    </tab>
	    <tab name="defQuestionnaireTab2">
	        <title>Search Questions</title>
	        <panel type="console" id="questionSelectTitle" bodyCssClass="panel-light" dataSource="regRequirementDs" showOnLoad="false">
		        <fieldset layout="fluid">
		            <field name="questionnaire_title" table="questionnaire_ext" dataType="text" cssClass="shortField" showLabel="false" style="width:140px" >
		                <title>QUESTIONNAIRE TITLE</title>
		                <action type="selectValue" id="selValQuestTitle">
		                    <title>...</title>
		                    <command type="selectValue"
			                    fieldNames="questionnaire_ext.questionnaire_title"  
			                    selectFieldNames="questionnaire_ext.questionnaire_title"
			                    visibleFieldNames="questionnaire_ext.questionnaire_title, questionnaire_ext.description, questionnaire_ext.status, questionnaire_ext.edited_by, questionnaire_ext.date_last_edit, questionnaire_ext.questionnaire_id">
			                </command>
		                </action>
		            </field>
		            <field name="regulation" table="regrequirement" selectValueType="grid" cssClass="shortField" showLabel="false" style="width:140px">
		              <title>REGULATION</title>
		            </field>
		            <field name="reg_program" table="regrequirement" cssClass="shortField" showLabel="false" style="width:140px"/>
		            <field name="reg_requirement" table="regrequirement" cssClass="shortField" showLabel="false" style="width:140px"/>
		            </fieldset>
		            <fieldset layout="fluid">
		            <action id="filter"><title>Filter</title></action>
		            <action id="clear"><title>Clear</title></action>
		        </fieldset>
		    </panel>
		    <panel type="grid" id="questionSelectPanel" showIndexAndFilterOnLoad="true" dataSource="questionSelectDataSource" showOnLoad="false">
		        <sortField name="question_id"/>
		        <indexField name="question_tag" table="question_ext"/>
		        <field controlType="image" imageName="/schema/ab-core/graphics/icons/move-icon.png">
		            <tooltip>Drag and drop Question to Questions List</tooltip>
		        </field>
		        <event type="onClickItem">
		            <tooltip>View Question Details</tooltip>
		            <command type="callFunction" functionName="abCommonQuestExtQuestionnaireDefController.viewAvailQuestion(this)"/>
		        </event>
		        <field name="question_text">
		            <tooltip>View Details</tooltip>
		        </field>        
		        <field name="question_tag"/>     
		        <field name="hierarchy_ids" hidden="true"/>        
		        <field name="question_type"/> 
		        <field name="freeform_width_display" table="question_ext" dataType="number" decimals="0">
		            <title>Freeform Width</title>
		        </field>   
		        <field name="lookup_table"/>
		        <field name="lookup_field"/>
		        <field name="unit_type"/>
		        <field name="unit_default"/>      
		        <field name="is_active"/>
		        <field name="edited_by"/>
		        <field name="date_last_edit"/>
		        <field name="time_last_edit"/>
		        <field name="question_id"/>
		        <field name="question_id_old"/>
		    </panel>
        </tab>
    </tabs>
    
    <panel type="hierTree" id="questionHierTree" dataSource="questionsHierDataSource" showOnLoad="false" layoutRegion="questionTreeRegion">
        <title>Question Tree</title>
        <action id="addFollowUp" type="menu">
            <title>Add</title>
            <tooltip>Add a Follow-Up Question to the Selected Question</tooltip>
            <action>
                <title>Add Follow-Up Question</title>
                <command type="callFunction" functionName="abCommonQuestExtQuestionnaireDefController.questionHierTree_onAddChild(false)"/>
            </action>
            <action id="addAction">
                <title>Add Follow-Up Action</title>
                <command type="callFunction" functionName="abCommonQuestExtQuestionnaireDefController.questionHierTree_onAddChild(true)"/>
            </action>
        </action>
        <event type="onClickNode">
           <command type="callFunction" functionName="questionHierTree_onSelectQuestion"/>
        </event>        
    </panel>
    
    <panel type="form" id="questQuestionForm" dataSource="questQuestionsDataSource" showOnLoad="false" layoutRegion="formsRegion" >
            <title>Question Display Options</title>
            <action id="save">
                <title>Save</title>
            </action>           
            <field name="sort_order" table="quest_question_ext" style="width:50px" readOnly="true"/>
            <field name="question_prefix" table="quest_question_ext" style="width:120px">
                <title>Prefix</title>
            </field>  
            <field name="is_required" table="quest_question_ext" />
            <field id="checkboxShowAnswerText">
                <title></title>  
                <tooltip>Note: The Answer Options for this Question must match the Answer Options defined for the Header.  If Answer Options have already been defined for the Header, they will be copied upon Save.</tooltip>
                <html>
                    <input type="checkbox" id="showAnswerText_no" name="showAnswerText_checkbox" onclick="setShowAnswerText('questQuestionForm', this.checked)"><span translatable="true">Answer Labels Only in Header</span></input>
                </html>
            </field>
            <field name="display_format" table="quest_question_ext" hidden="false"/>
            <field name="show_answer_text" table="quest_question_ext" hidden="true"/>
            <field name="question_id" table="quest_question_ext" hidden="true"/>
            <field name="questionnaire_id" table="quest_question_ext" hidden="true"/>
    </panel>

    <!-- Data source for question form. -->
    <dataSource id="questionsFormDataSource">
        <table name="question_ext"/>

        <field name="question_id"/>
        <field name="question_text"/>
        <field name="question_type"/>
        <field name="is_active"/>
        <field name="freeform_width"/>
        <field name="lookup_table"/>
        <field name="lookup_field"/>
        <field name="unit_type"/>
        <field name="unit_default"/>
        <field name="question_tag"/>
        <field name="parent_answer_condition"/>
        <field name="hierarchy_ids"/>
        <field name="activity_type"/>
        <field name="date_last_edit"/>
        <field name="time_last_edit"/>
        <field name="edited_by"/>
        <field name="question_id_old"/>
        <field name="sort_order_child"/>
        <field name="is_required_child"/>
        <field name="num_responses_max"/>
        <field name="unit_type_display" dataType="text">
            <sql dialect="generic">
                ${parameters['unit_type_display']}
            </sql>
        </field>
        <parameter name="unit_type_display" dataType="verbatim" value="question_ext.unit_type"/>
    </dataSource>
    
    <panel type="form" id="questionForm" dataSource="questionsFormDataSource" columns="1" showOnLoad="false" layoutRegion="formsRegion" >
            <title>Question Details</title>
            <action id="save">
                <title>Save</title>
            </action>
            <action id="deleteChildQuestion" hidden="true">
	            <title>Delete</title>
	            <tooltip>Delete.  The follow-up question or action will be permanently deleted.</tooltip>
	            <command type="callFunction" functionName="abCommonQuestExtQuestionnaireDefController.deleteChildQuestion()"/>
	        </action>
            <field name="question_text" table="question_ext" width="240px" height="55px">
                <title>Text</title>
            </field>
            <field name="question_tag" table="question_ext" required="false">
                <title>Tag</title>
                <tooltip>A short identifier for grouping related questions.</tooltip>
            </field>
            <field name="question_type" table="question_ext" hidden="false" readOnly="true">
                <title>Type</title>
            </field>
            <field id="selectQuestionType" required="true">
                <title>Question Type</title>
                <html>
                    <select id="selectQuestionTypeSelector" class="inputField_box" onchange="onChangeQuestionTypeSelector()">
                        <option value="multiple" >
                            <span translatable="true">Multiple Choice</span>
                        </option>
                        <option value="freeform"  >
                            <span translatable="true">Freeform</span>
                        </option>
                        <option value="date"  >
                            <span translatable="true">Date</span>
                        </option>
                        <option value="time"  >
                            <span translatable="true">Time</span>
                        </option>
                        <option value="lookup"  >
                            <span translatable="true">Lookup</span>
                        </option>
                        <option value="count"  >
                            <span translatable="true">Count</span>
                        </option>
                        <option value="measurement"  >
                            <span translatable="true">Measurement</span>
                        </option>
                        <option value="document"  >
                            <span translatable="true">Document/Photo</span>
                        </option>
                    </select>
                </html>
            </field>
            <field name="freeform_width" table="question_ext"/>
            <field name="lookup_table" table="question_ext"/>
            <field name="lookup_field" table="question_ext"/>
            <field name="unit_type" table="question_ext" hidden="true"/>
            <field name="unit_type_display" table="question_ext" dataType="text" size="32">
                <title>Measurement Type</title>
                <action id="selVal_unit_type">
                    <command type="callFunction" functionName="View.controllers.get('abCommonQuestExtQuestionnaireDef').onSelectUnitType()"/>
                </action>
            </field>
            <field name="unit_default" table="question_ext">
                <action id="selVal_unit_default">
                    <command type="callFunction" functionName="View.controllers.get('abCommonQuestExtQuestionnaireDef').onSelectUnitsUnit()"/>
                </action>
            </field>
            <field/>
            <field name="num_responses_max" table="question_ext" style="width:40px">
                <title>Max # Responses</title>
                <tooltip>Allow the user to enter more than one response.</tooltip>
            </field>            
            <field id="selectActionType">
                <title>Response Action</title>
                <tooltip>Choose Service Request, Workflow Rule or Other.  A Service Request action will create a Service Request.  A Workfow Rule action calls a custom Workflow Rule.  Otherwise, you may generate an Action Item of the selected Response Action Type.</tooltip>
                <html>
                    <select id="selectActionTypeSelector" class="inputField_box" onchange="onChangeActionTypeSelector()">
                        <option value="serviceRequest" >
                            <span translatable="true">Service Request</span>
                        </option>
                        <option value="workflowRule"  >
                            <span translatable="true">Workflow Rule</span>
                        </option>
                        <option value="other"  >
                            <span translatable="true">Other</span>
                        </option>
                    </select>
                </html>
            </field>
            <field name="activity_type" table="question_ext">
                <action>
                    <title>...</title>
                    <command type="callFunction" functionName="onActivityTypeSelval"/>
                </action>
            </field>
            <field id="selectMultipleChoiceType">
                <title>Allow Selection of</title>   
                <html>
                    <input type="radio" id="selectMultipleChoiceType_radioSingle" name="selectMultipleChoiceType_radio" value="multiple_choice_single" onclick="setMultipleChoiceType(this.value)" checked="checked"><span translatable="true">One Answer Option</span></input><br/>
                    <input type="radio" id="selectMultipleChoiceType_radioMultiple" name="selectMultipleChoiceType_radio" value="multiple_choice_multi" onclick="setMultipleChoiceType(this.value)"><span translatable="true">More than One Answer Option</span></input>
                </html>
            </field>
            <field>
                <html>
                    <div style="height:1em"><span id="parent_answer_select_space"></span></div>
                </html>
            </field>
            <field name="hierarchy_ids" alias="question_ext.parent_answer_select" readOnly="false" table="question_ext" style="width:120px">
               <title>Parent Answer Condition</title>
               <tooltip>Choose one or more Answer Options from the Parent Question.   These are the values that, if selected for the Parent Question, will cause this Follow-Up Question/Action to occur.</tooltip>
                <action id="selectParentAnswer">
                    <title>Select</title>
                    <tooltip>Select Parent Answer Option</tooltip>  
                    <command type="callFunction" functionName="selectValueParentAnswer"/>
                </action>
            </field>            
            <field name="parent_answer_condition" table="question_ext" hidden="true"/> 
            <field name="sort_order_child" table="question_ext" style="width:120px" />
            <field name="is_required_child" table="question_ext" />            
            <field/>
            <field name="is_active" table="question_ext" hidden="false"/>               
            <field name="question_id" table="question_ext">
                <title>ID</title>
            </field>
            <field name="question_id" alias="question_ext.parent_id" table="question_ext" readOnly="true">
               <title>Parent Question ID</title>
            </field>
            <field name="hierarchy_ids" table="question_ext"/>
            <field name="edited_by" table="question_ext" readOnly="true" hidden="false"/>
            <field name="date_last_edit" table="question_ext" readOnly="true" hidden="false"/>        
            <field name="time_last_edit" table="question_ext" readOnly="true" hidden="false"/>           
            <field name="question_id_old" table="question_ext" readOnly="true" hidden="false"/>
        </panel>
    
    <panel type="grid" id="answerOptionsGrid" showIndexAndFilterOnLoad="false" dataSource="answerOptionsDs" showOnLoad="false" layoutRegion="formsRegion">
        <title>Answer Options</title>
        <instructions>Add or edit Multiple Choice Answer Options.</instructions>
        <action>
            <title>Add</title>
            <tooltip>Add a Multiple Choice Answer Option</tooltip>
            <command type="openDialog" panelId="answerOptionsForm" newRecord="true"/>
        </action>
        <event type="onClickItem">
            <command type="openDialog" panelId="answerOptionsForm"/>
        </event>
        <field controlType="image" id="editAnswer" imageName="/schema/ab-core/graphics/icons/view/edit.png">
            <tooltip>Edit Answer</tooltip>
            <command type="openDialog" panelId="answerOptionsForm"/>
        </field>               
        <field name="answer_option_text"/>
        <field name="sort_order"/>
        <field name="answer_option_id"/>
        <field name="question_id" hidden="true"/>        
    </panel>
    
    <panel type="form" id="answerOptionsForm" dataSource="answerOptionsDs" showOnLoad="false" hidden="true">
        <title>Define Answer Option</title>
        <action id="save" mainAction="true">
            <title>Save</title>
        </action>
        <action id="delete" hidden="${record['quest_answer_option_ext.answer_option_id'] == ''}">
            <title>Delete</title>
            <command type="closeDialog"/>
        </action>        
        <field name="answer_option_text"/>
        <field name="sort_order"/>
        <field name="answer_option_id"/>
        <field name="question_id"/>
    </panel>
            
    <panel type="form" id="deleteQuestionPanel" dataSource="questQuestionsDataSource" columns="2" showOnLoad="false" hidden="true" >    
        <title>Confirm Delete</title>
        <instructions>Delete</instructions>
        <action mainAction="true" id="removeOrDeleteQuestion">
            <title>OK</title>
            <command type="closeDialog"/>
        </action>
        <action>
            <title>Cancel</title>
            <command type="closeDialog"/>
        </action>        
        <field>
            <html>
                <input type="radio" id="selectDelete_remove" name="selectDelete_radio" value="remove" checked="checked"><span translatable="true">Delete from the selected Questionnaire only</span></input><br/>
                <input type="radio" id="selectDelete_delete" name="selectDelete_radio" value="delete" ><span translatable="true">Delete from all Questionnaires</span></input>
            </html>
        </field>
        <field name="questionnaire_id" table="quest_question_ext" hidden="true"/>
        <field name="question_id" table="quest_question_ext" hidden="true"/>
        <field name="question_type" table="question_ext" hidden="true"/>
    </panel>
    
    <panel type="console" id="confirmSaveOrCopyPanel" dataSource="questQuestionsDataSource" columns="2" showOnLoad="false" hidden="true" >    
        <title>Confirm Update</title>
        <instructions>Confirm Update</instructions>
        <action mainAction="true" id="saveOrCopyQuestion">
            <title>OK</title>
            <command type="closeDialog"/>
        </action>
        <action>
            <title>Cancel</title>
            <command type="closeDialog"/>
        </action>        
        <field>
            <html>
                <input type="radio" id="selectSave_copy" name="selectSave_radio" value="copy" checked="checked"><span translatable="true">Update the selected Questionnaire only</span></input><br/>
                <input type="radio" id="selectSave_save" name="selectSave_radio" value="save" ><span translatable="true">Update all Questionnaires</span></input>
            </html>
        </field>
        <field name="questionnaire_id" table="quest_question_ext" hidden="true"/>
        <field name="question_id" table="quest_question_ext" hidden="true"/>
    </panel>
    
</view>