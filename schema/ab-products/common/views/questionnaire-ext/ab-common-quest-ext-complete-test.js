
View.createController('SelectQuestionnaire', {
	questionnaire_id:0,
	survey_event_id:0,

    /**
     * When the user selects a questionnaire, render the Complete Questionnaire form.
     * @param row
     */
    questionnaireList_onClickItem: function(row) {
        this.questionnaire_id = row.getRecord().getValue('questionnaire_ext.questionnaire_id');
        var restriction = new Ab.view.Restriction();
        restriction.addClause('regrequirement.questionnaire_id', this.questionnaire_id);
        this.eventsList.refresh(restriction);
    },
    
    eventsList_onClickItem: function(row) {
    	this.survey_event_id = row.getRecord().getValue('activity_log.activity_log_id');
    	var questionnaireView = View.panels.get('questionnaireView').contentView;
		if (questionnaireView) {
			loadQuestionnairePanel();
		} else {
			setTimeout(loadQuestionnairePanel, 5000);
		}
    },

	questionnaireView_onSave: function() {
		this.questionnaireView_save(false);
	},
	
	questionnaireView_save: function(checkRequiredAnswers) {
		var updated = false;
		var questionnaireView = View.panels.get('questionnaireView').contentView;
		if (questionnaireView) {
			var updateQuestionnaireController = questionnaireView.controllers.get('UpdateQuestionnaire');
			updated = updateQuestionnaireController.saveQuestionnaire(this.survey_event_id, checkRequiredAnswers);
		}
		return updated;
	},
	
	questionnaireView_onSaveSubmit: function() {
		if (this.questionnaireView_save(true)) {
			var updateQuestionnaireController = View.panels.get('questionnaireView').contentView.controllers.get('UpdateQuestionnaire');
			updateQuestionnaireController.processQuestionnaire(this.questionnaire_id, this.survey_event_id);
		}
	}
});

function loadQuestionnairePanel() {
	/* get this view's controller */
	var controller = View.controllers.get('SelectQuestionnaire');
	var questionnaireView = View.panels.get('questionnaireView').contentView;
	if (questionnaireView) {
		var updateQuestionnaireController = questionnaireView.controllers.get('UpdateQuestionnaire');
		updateQuestionnaireController.loadQuestionnaireWithResults(controller.questionnaire_id, false, controller.survey_event_id);
	}
}
