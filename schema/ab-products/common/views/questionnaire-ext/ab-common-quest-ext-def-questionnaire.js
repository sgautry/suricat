/**
 * @author Eric_Maxfield@archibus.com
 * @Bali6 Extended Questionnaires
 */
var abCommonQuestExtQuestionnaireDefController = View.createController('abCommonQuestExtQuestionnaireDef', {
	questionnaire_id: '',
	hoverQuestionId: 0,
	hoverQuestionSortOrder: 0,
	highestSortOrder: 0,
	parent_question_id: 0,
	isChildQuestion: false,
	isChildActionQuestion: false,
    measurement: {
        unitTypePrefix: null,
        activityId: null
	},
    afterViewLoad: function () {
        var controller = this;
        this.collapseQuestionPanels(true, true, true);
        var grid = this.questionnaireList;
        grid.afterCreateCellContent = function (row, column, cellElement) {
            if (column.id === 'questionnaire_ext.questionnaire_title') {
                cellElement.style.fontWeight = 'bold';
            }
        };
        var gridList = this.questionsList;
        gridList.afterCreateCellContent = function (row, column, cellElement) {
            if (column.id === 'question_ext.question_text' || column.id === 'question_ext.question_tag') {
                cellElement.style.fontWeight = 'bold';
            }
            if (column.id === 'question_ext.unit_type' && valueExists(controller.measurement) && valueExists(controller.measurement.unitTypePrefix)) {
                var value = row.row.getFieldValue(column.id);
                if (valueExistsNotEmpty(value)) {
                    value = value.replace(controller.measurement.unitTypePrefix, '');
                    var contentElement = cellElement.childNodes[0];
                    if (contentElement)
                        contentElement.textContent = value;
                }
            }
        };
        var gridSelect = this.questionSelectPanel;
        gridSelect.afterCreateCellContent = function (row, column, cellElement) {
            if (column.id === 'question_ext.question_text' || column.id === 'question_ext.question_tag') {
                cellElement.style.fontWeight = 'bold';
            }
        };
        var gridCreate = this.questionCreatePanel;
        gridCreate.afterCreateCellContent = function (row, column, cellElement) {
            if (column.id === 'afm_tbls.question_text') {
                cellElement.style.fontWeight = 'bold';
            }
        };
        if (this.view.taskInfo.activityId !== 'AbRiskCompliance') {
            this.questionSelectTitle.showField('regrequirement.regulation', false);
            this.questionSelectTitle.showField('regrequirement.reg_program', false);
            this.questionSelectTitle.showField('regrequirement.reg_requirement', false);
            removeEllipses(this.questionSelectTitle.getFieldElement('regrequirement.regulation'));
            removeEllipses(this.questionSelectTitle.getFieldElement('regrequirement.reg_program'));
            removeEllipses(this.questionSelectTitle.getFieldElement('regrequirement.reg_requirement'));
        }
    },
	
	afterInitialDataFetch: function() {
		this.setQuestionTypes();
		
		this.questionsList.addParameter('yes', getMessage('yes'));
		this.questionsList.addParameter('no', getMessage('no'));
		this.questionsList.addParameter('short_form', getMessage('short_form'));
		this.questionsList.addParameter('long_form', getMessage('long_form'));
		
		this.questionForm.getFieldLabelElement('question_ext.activity_type').innerHTML +=  '<span class="required" name="question_ext.activity_type.asterisk" id="question_ext.activity_type.asterisk"></span>';
		this.questionForm.getFieldLabelElement('question_ext.parent_answer_select').innerHTML +=  '<span class="required" name="question_ext.parent_answer_select.asterisk" id="question_ext.parent_answer_select.asterisk">*</span>';
		this.questionForm.enableField('question_ext.parent_answer_select', false);
		
		var openerView = View.getOpenerView();
		if (openerView && openerView.parameters) {
			if (openerView.parameters.showAssignButton) this.questionnaireProfile.actions.get('assign').show(openerView.parameters.showAssignButton);
			if (openerView.parameters.assignButtonTitle) this.questionnaireProfile.actions.get('assign').setTitle(openerView.parameters.assignButtonTitle);
			if (openerView.parameters.showSelectButton) this.questionnaireReport.actions.get('select').show(openerView.parameters.showSelectButton);
			if (openerView.parameters.selectButtonTitle) this.questionnaireReport.actions.get('select').setTitle(openerView.parameters.selectButtonTitle);
			if (openerView.parameters.hideQuestionnaireList) {
				//View.getLayoutManager('nestedWest').setRegionSize('north', 80);
				if (openerView.parameters.openerController) {
					var openerQuestionnaireId = openerView.parameters.openerController.selected_questionnaire_id;
					this.questionnaireReport.refresh('questionnaire_ext.questionnaire_id = ' + openerQuestionnaireId);
					if (openerQuestionnaireId != 0) {
						this.refreshPanelsWithQuestionnaire(openerQuestionnaireId);
                    }
				}
			} else {
				this.refreshQuestionnaireList("1=1");
            }
            if (openerView.parameters.measurement) {
				this.measurement = openerView.parameters.measurement;
            }
		} else {
			this.refreshQuestionnaireList("1=1");
        }
	},
	
	refreshQuestionnaireList: function(restriction) {
		this.questionnaireList.refresh(restriction);
		if (this.questionnaireList.rows.length == 1) {
			var questionnaire_id = this.questionnaireList.rows[0]['questionnaire_ext.questionnaire_id'];
			this.refreshPanelsWithQuestionnaire(questionnaire_id);
		}
	},
	
	questionnaireProfile_afterRefresh: function() {
		var message = '';
		var openerView = View.getOpenerView();
		if (openerView && openerView.parameters && openerView.parameters.getAdditionalText) {
			message = openerView.parameters.getAdditionalText(this.questionnaire_id);
		}
		var innerHTML = '<td class="columnReportSpacer"> </td>';
		innerHTML += '<td colspan="10" id="questionnaireProfile_additionalText">' + message + '</td>';
		innerHTML += '<td class="columnReportSpacer"> </td>';
		$('questionnaireProfile_additionalText').parentNode.innerHTML = innerHTML;
	},
	
	questionsList_afterRefresh: function() {
		this.highestSortOrder = 0;
		var controller = this;
		this.questionsList.gridRows.each(function(row) {
			var record = row.getRecord(); 
            Ext.get(row.dom).on({
                'mouseover': function(e) {
                    controller.onMouseOver(row);
                },
                'mouseout': function(e) {
                    controller.onMouseOut(row);
                }
            });
            controller.highestSortOrder = Number(record.getValue('quest_question_ext.sort_order'));
		});
		this.highlightParentQuestionRow();		
	},
	
	highlightParentQuestionRow: function() {
		var controller = this;
		if (controller.parent_question_id == 0) return;
		var grid = this.questionsList;
		for (var i=0; i<grid.rows.length; i++) {
            var row = grid.rows[i];
            if(controller.parent_question_id == row['quest_question_ext.question_id']){
            	grid.selectRow(row.index);
            	break;
            }
        }
	},
	
	highlightQuestionnaireRow: function() {
		var controller = this;
		if (controller.questionnaire_id == '') return;
		var grid = this.questionnaireList;
		for (var i=0; i<grid.rows.length; i++) {
            var row = grid.rows[i];
            if(controller.questionnaire_id == row['questionnaire_ext.questionnaire_id']){
            	grid.selectRow(row.index);
            	break;
            }
        }
	},
	
	onMouseOver: function(row) {
		this.hoverQuestionId = row.getRecord().getValue('quest_question_ext.question_id');
		this.hoverQuestionSortOrder = Number(row.getRecord().getValue('quest_question_ext.sort_order'));
	},
	
	onMouseOut: function(row) {
		this.hoverQuestionId = 0;
	},
	
	questionSelectPanel_afterRefresh: function (panel) {
        var controller = this;
        panel.gridRows.each(function (row) {
            new GridControllerDragSource('questionSelectPanel', row, controller.afterDragCallback.createDelegate(this, ['questionSelectPanel'], true));
        });
    },
    
    questionCreatePanel_afterRefresh: function (panel) {
        var controller = this;
        panel.gridRows.each(function (row) {
            new GridControllerDragSource('questionCreatePanel', row, controller.afterDragCallback.createDelegate(this, ['questionCreatePanel'], true));
        });
    },
    
    afterDragCallback: function (dropped, dragFromPanelId) {
        var panel = View.panels.get(dragFromPanelId);
        if (dropped) {
            if ('questionSelectPanel' === dragFromPanelId || 'questionCreatePanel' === dragFromPanelId) {
                panel.refresh();
            }
            View.controllers.get('abCommonQuestExtQuestionnaireDef').refreshDragDrop();
        } else {
            // when dropped on same node prevent onclick
            var listener = panel.getEventListener('onClickNode');
            if (listener) {
            }
        }
    },
    
    refreshDragDrop: function () {
        Ext.dd.DDM.refreshCache(Ext.dd.DragDropMgr.ids);
    },
    
    /**
     * Handles drag over event over User Process grid. 
     * @return true if the drop is allowed.
     */
    questionsList_onDragOver: function(dragSource, data) {
        return true;  
    },
    
    /**
     * Handles drag drop event over User Process grid.
     */
    questionsList_onDragDrop: function(dragSource, data) {
    	var controller = this;
    	var sortOrder = this.highestSortOrder + 10;
    	if (this.hoverQuestionId != 0) sortOrder = this.hoverQuestionSortOrder + 10;
    	
        if ('questionCreatePanel' == data.selectPanelId) {
        	this.updateSortOrder(sortOrder);
        	this.addNewQuestion(data.question_type, sortOrder);
        } else {
        	var questions = getQuestionsInQuestionnaires(data.question_id, this.questionnaire_id);
        	if (questions.length > 0) {
        		View.confirm(getMessage('selectedQuestionExistsInQuestionnaire'), function(button){
        			if (button == 'yes') {
        				controller.updateSortOrder(sortOrder);
        				controller.performCopyAsNew(data.question_id, false, false, 'copyQuestion', data.question_id, sortOrder);
        			} else {
        				return;
        			}
        		});
        	} else {
        		this.updateSortOrder(sortOrder);
        		this.addQuestQuestion(data.question_id, sortOrder);
        	}
        }
    },
    
    addNewQuestion: function(question_type, sortOrder) {
    	var ds = View.dataSources.get('questionsDataSource');
		var record = new Ab.data.Record({
			'question_ext.question_text': getMessage('defaultQuestionText'),
			'question_ext.question_type': question_type,
			'question_ext.edited_by': View.user.name
		}, true);
		var record = ds.saveRecord(record);
    	this.parent_question_id = record.getValue('question_ext.question_id');
    	
    	var restriction = new Ab.view.Restriction();
		restriction.addClause('question_ext.question_id', this.parent_question_id);
		var questionRecord = ds.getRecord(restriction);
    	questionRecord.setValue('question_ext.hierarchy_ids', this.parent_question_id + '|');
    	ds.saveRecord(questionRecord);
    	
    	this.addQuestQuestion(this.parent_question_id, sortOrder);  
    	refreshHierTree('questionHierTree', this.parent_question_id);
        this.refreshQuestionForm(restriction);
    },
    
    addQuestQuestion: function(question_id, sortOrder) {
    	var controller = this;    	
    	var record = new Ab.data.Record({
            'quest_question_ext.questionnaire_id': controller.questionnaire_id,
            'quest_question_ext.question_id': question_id,
            'quest_question_ext.sort_order': sortOrder,
            'quest_question_ext.is_required': 0,
            'quest_question_ext.display_form': 'Short Form',
            'quest_question_ext.show_answer_text': 1,
            'quest_question_ext.question_prefix': ''
        }, true);
    	this.questQuestionsDataSource.saveRecord(record);
    	this.questionsList.refresh();
    	this.questionSelectPanel.refresh();
    },
	
	setQuestionTypes: function() {
		var records = this.questionElementDataSource.getRecords();
		for (var i=0;i<records.length;i++) {
			var record = records[i];
			var question_type = record.getValue('afm_tbls.question_type');
			this.questionCreatePanel.addParameter(question_type, getMessage('type_' + question_type));
			this.questionCreatePanel.addParameter('desc_' + question_type, getMessage('desc_' + question_type));
		}
	},
	
	afterSaveQuestionnaire: function(questionnaire_id, newRecord) {
		var openerView = View.getOpenerView();
		if (openerView && openerView.parameters && openerView.parameters.clearFilter) {
			if (newRecord) openerView.parameters.clearFilter();
		}		
		var openerView = View.getOpenerView();
		if (openerView && openerView.parameters && openerView.parameters.hideQuestionnaireList) 
			this.questionnaireReport.refresh();
		else {
			if (newRecord) this.questionnaireList.refresh("1=1");
			else this.questionnaireList.refresh();
		}
		this.refreshPanelsWithQuestionnaire(questionnaire_id);	
		View.closeDialog();
	},
	
	selectQuestionnaire: function(obj) {
		var questionnaire_id = obj.restriction['questionnaire_ext.questionnaire_id'];
		this.refreshPanelsWithQuestionnaire(questionnaire_id);
		if (questionAnswersExist(null, questionnaire_id)) {
			View.alert(getMessage('alertQuestionnaireActive'));
		}
	},

	refreshQuestionForm: function (restriction, newRecord) {
    	if (this.measurement && this.measurement.unitTypePrefix) {
            this.questionForm.addParameter('unit_type_display', "RTRIM(${sql.substr('question_ext.unit_type', " + this.measurement.unitTypePrefix.length + ", 24)})");
		}
		var createNewRecord = false;
    	if (valueExists(newRecord)) {
            createNewRecord = newRecord;
		}
        this.questionForm.refresh(restriction, createNewRecord);
	},
	
	refreshPanelsWithQuestionnaire: function(questionnaire_id) {
		this.parent_question_id = 0;
		this.questionnaire_id = questionnaire_id;
		this.highlightQuestionnaireRow();
		this.collapseQuestionPanels(true, true, true);
		
		var restriction = new Ab.view.Restriction();
		restriction.addClause('quest_question_ext.questionnaire_id', questionnaire_id);
		this.questionsList.refresh(restriction);
		this.questionnaireProfile.refresh('questionnaire_ext.questionnaire_id = ' + questionnaire_id);
		
		var questRestriction = new Ab.view.Restriction();
		questRestriction.addClause('questionnaire_ext.questionnaire_id', questionnaire_id);
		var questRecord = this.questionnairesDataSource.getRecord(questRestriction);
		this.questionnaireProfile.appendTitle(questRecord.getValue('questionnaire_ext.questionnaire_title'));
		
		this.questionCreatePanel.show();
		this.questionSelectTitle.refresh("1=2");
		//var questionSelectRestriction = "NOT EXISTS (SELECT 1 FROM quest_question_ext WHERE quest_question_ext.question_id = question_ext.question_id AND quest_question_ext.questionnaire_id = " + questionnaire_id + ")";
		this.questionSelectPanel.refresh("1=1");
		this.questionCreatePanel.refresh();		
	},
	
	questionnaireList_onCopyQuestionnaire: function(row) {
		var controller = this;
		var questionnaire_id = row.getFieldValue('questionnaire_ext.questionnaire_id');
		var questionnaire_title = row.getFieldValue('questionnaire_ext.questionnaire_title');
		var message = String.format(getMessage('copyQuestionnaire'), questionnaire_title);
		View.confirm(message, function(button){
			if (button == 'yes') {
				var new_questionnaire_id = copyQuestionnaire(questionnaire_id);
				copyQuestQuestions(questionnaire_id, new_questionnaire_id);
				var openerView = View.getOpenerView();
				if (openerView && openerView.parameters && openerView.parameters.clearFilter) {
					openerView.parameters.clearFilter();
				}
				controller.questionnaireList.refresh("1=1");
				controller.refreshPanelsWithQuestionnaire(new_questionnaire_id);
			} else {
				return;
			}
		});
		
	},
	
	/* DELETING A QUESTIONNAIRE
	 * 	TEST 1: Does any question in the questionnaire have answers?	
			- YES - If Answers exist for the Questionnaire, alert user and cancel delete		
			- NO - Get user confirmation for deleting the questionnaire and, if yes, continue to performDeleteQuestionnaire() */
	questionnaireList_onDeleteQuestionnaire: function(row) {
		var questionnaire_id = row.getFieldValue('questionnaire_ext.questionnaire_id');
		var questionnaire_title = row.getFieldValue('questionnaire_ext.questionnaire_title');
		
		if (questionAnswersExist(null, questionnaire_id)) {
			View.alert(getMessage('deleteQuestionnaireActive'));
			return;
		} else if (questionnaireAssignedToRequirement(questionnaire_id)) {
			View.alert(getMessage('questionnaireAssignedToRequirement'));
			return;
		}
		var message = String.format(getMessage('deleteQuestionnaire'), questionnaire_title);
		View.confirm(message, function(button){
			if (button == 'yes') {
				abCommonQuestExtQuestionnaireDefController.performDeleteQuestionnaire(questionnaire_id);
			} else {
				return;
			}
		});
	},
	
	/* TEST 2: Iterate through each questionnaire question.  Does each question exist in any other questionnaires as well?
			- YES - If a question exists in another questionnaire, only delete the quest_question_ext record from the selected questionnaire.  
			- NO - If a question is only in the selected questionnaire, delete the question from the quest_question_ext table 
					and then delete the question and any child questions from the question_ext table and delete any answer options.  
		After iterating through all questions, delete the questionnaire_ext record */
	performDeleteQuestionnaire: function(questionnaire_id) {
		View.openProgressBar(getMessage('deleting'));
		var restriction = new Ab.view.Restriction();
		restriction.addClause('quest_question_ext.questionnaire_id', questionnaire_id);
		var questQuestions = this.questQuestionsDs.getRecords(restriction);
		
		for (var i=0; i<questQuestions.length; i++) {
			var question_id = questQuestions[i].getValue('quest_question_ext.question_id');
			var questions = getQuestionsInQuestionnaires(question_id, null);
			if (questions.length == 1) {
				performRemoveQuestion(questionnaire_id, question_id);
				performDeleteQuestion(question_id);
			} else {
				performRemoveQuestion(questionnaire_id, question_id);
			}
		}
		
		var questRestriction = new Ab.view.Restriction();
		questRestriction.addClause('questionnaire_ext.questionnaire_id', questionnaire_id);
		var record = this.questionnairesDs.getRecord(questRestriction);
		this.questionnairesDs.deleteRecord(record);
		
		this.questionnaireList.refresh();
		this.questionnaire_id = '';
		this.questionsList.show(false);
		this.questionnaireProfile.show(false);
		this.questionCreatePanel.show(false);
		this.questionSelectPanel.show(false);
		this.questionSelectTitle.show(false);
		this.collapseQuestionPanels(true, true, true);
		View.closeProgressBar();
	},
	
	applyFilterRestrictionToQuestionnaires:function(restriction) {
		this.questionsList.show(false);
		this.questionnaireProfile.show(false);
		this.questionCreatePanel.show(false);
		this.questionSelectPanel.show(false);
		this.questionSelectTitle.show(false);
		this.collapseQuestionPanels(true, true, true);
		this.refreshQuestionnaireList(restriction);
	},
	
	/* DELETING A PARENT QUESTION FROM THE QUESTIONS LIST
	   	TEST 1: Do answers exist for this question in this questionnaire?
			- YES - If Answers exist for this question in this questionnaire, alert user and cancel delete.
			- NO - go to TEST 2

 		TEST 2: Do answers exist for this question in other questionnaires then?
			- YES - Get user confirmation for delete, and, if yes, only delete the quest_question_ext record.  
			- NO - go to TEST 3

 		TEST 3: Does the question exist in other Questionnaires?
			- NO - Get user confirmation for delete, and, if yes, delete the question from the quest_question_ext table and then delete the question and any child questions from the question_ext table and delete any answer options.
			- YES - show Confirm Delete dialog with message listing all questionnaire titles
    				CONFIRM DELETE DIALOG - Allow user to choose between deleting the question from this questionnaire only or from all questionnaires
						- THIS QUESTIONNAIRE ONLY - only delete the quest_question_ext record
						- ALL QUESTIONNAIRES - delete the question from the quest_question_ext table and then delete the question and any child questions from the question_ext table and delete any answer options.
	 */
	questionsList_onDeleteQuestion: function(row) {
		var controller = this;
		var question_id = row.getFieldValue('quest_question_ext.question_id');
		var isHeader = isHeaderQuestion(row);
		
		if (questionAnswersExist(question_id, this.questionnaire_id)) {
			View.alert(getMessage('deleteParentQuestionWithAnswers'));
		} else if (questionAnswersExist(question_id, null)) {
			var removeMsgId = 'removeParentQuestion';
			if (isHeader) removeMsgId += 'Header';
			View.confirm(getMessage(removeMsgId), function(button){
				if (button == 'yes') {
					View.openProgressBar(getMessage('removing'));
					performRemoveQuestion(controller.questionnaire_id, question_id);
					controller.questionSelectPanel.refresh();
					controller.questionsList.refresh();
					View.closeProgressBar();
					controller.collapseQuestionPanels(true, true, true);
				}
			});
		} else {
			var msgId = 'deleteParentQuestion';
			if (isHeader) msgId += 'Header';
			var questions = getQuestionsInQuestionnaires(question_id, null);
			if (questions.length > 1) {
				msgId += 'InOtherQuestionnaires';
				var restriction = new Ab.view.Restriction();
				restriction.addClause('quest_question_ext.question_id', question_id);
				restriction.addClause('quest_question_ext.questionnaire_id', this.questionnaire_id);
				this.deleteQuestionPanel.refresh(restriction);
				this.deleteQuestionPanel.setInstructions(getMessageListQuestionnaires(msgId, questions));
				this.deleteQuestionPanel.showInWindow({
					closeButton: false,
					width: 500,
					height: 200
				});
			} else {
				View.confirm(getMessage(msgId), function(button){
					if (button == 'yes') {
						View.openProgressBar(getMessage('deleting'));
						performDeleteQuestion(question_id);
						controller.questionSelectPanel.refresh();
						controller.questionsList.refresh();
						View.closeProgressBar();
						controller.collapseQuestionPanels(true, true, true);
					}
				});
			}
		}
	},
	
	deleteQuestionPanel_onRemoveOrDeleteQuestion: function() {
		var question_id = this.deleteQuestionPanel.getFieldValue('quest_question_ext.question_id');
		var msgId = 'removing';
		if ($('selectDelete_delete').checked) msgId = 'deleting';
		View.openProgressBar(getMessage(msgId));
		
		if ($('selectDelete_delete').checked) performDeleteQuestion(question_id);
		else performRemoveQuestion(this.questionnaire_id, question_id);	
		
		this.questionsList.refresh();
		this.questionSelectPanel.refresh();
		View.closeProgressBar();
		this.collapseQuestionPanels(true, true, true);
	},
	
	viewAvailQuestion: function(obj) {
		var question_id = obj.restriction['question_ext.question_id'];
		var question_type = this.questionsDataSource.getRecord(obj.restriction).getValue('question_ext.question_type');
		View.openDialog('ab-common-quest-ext-def-questionnaire-dtl.axvw', null, false, {
			'width':800,
			'question_id':question_id,
			'questionnaire_id':null,
			'question_type':question_type
		});
	},
	
	previewQuestionnaire: function() {
		var questionnaire_id = this.questionnaire_id;
		if (View.getOpenerView()) {
			View.getOpenerView().openDialog('ab-common-quest-ext-preview.axvw', null, false, {
				'questionnaireId': questionnaire_id
			});
		} else {
			View.openDialog('ab-common-quest-ext-preview.axvw', null, false, {
				'questionnaireId': questionnaire_id
			});
		}
	},
	
	updateSortOrder: function(sortOrder) {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('quest_question_ext.questionnaire_id', this.questionnaire_id);
		restriction.addClause('quest_question_ext.sort_order', sortOrder, '>=');
		var records = this.questQuestionsDs.getRecords(restriction);
		sortOrder += 10;
		for (var i=0;i<records.length;i++) {
			var record = records[i];
			record.setValue('quest_question_ext.sort_order', sortOrder);
			record.isNew = false;
			this.questQuestionsDs.saveRecord(record);
			sortOrder += 10;
		}
	},
	
	questionsList_onMoveUp: function(row) {
		var question_id = row.getFieldValue('quest_question_ext.question_id');
		var restriction = new Ab.view.Restriction();
		restriction.addClause('quest_question_ext.questionnaire_id', this.questionnaire_id);
		var records = this.questQuestionsDataSource.getRecords(restriction);

		var prevRecord = records[0];
		var record = null;
		for (var i=0;i<records.length;i++) {
			record = records[i];
			if (question_id == record.getValue('quest_question_ext.question_id')) break;
			prevRecord = record;
		}
		this.questionsList_onExchangeSort(prevRecord, record);
	},
	
	questionsList_onMoveDown: function(row) {
		var question_id = row.getFieldValue('quest_question_ext.question_id');
		var restriction = new Ab.view.Restriction();
		restriction.addClause('quest_question_ext.questionnaire_id', this.questionnaire_id);
		var records = this.questQuestionsDataSource.getRecords(restriction);

		var nextRecord = records[records.length-1];
		var record = null;
		for (var i=records.length-1;i>=0;i--) {
			record = records[i];
			if (question_id == record.getValue('quest_question_ext.question_id')) break;
			nextRecord = record;
		}
		this.questionsList_onExchangeSort(nextRecord, record);
	},
	
	questionsList_onExchangeSort: function(record1, record2) {		
		var record1SortOrder = Number(record1.getValue('quest_question_ext.sort_order'));
		var record2SortOrder = Number(record2.getValue('quest_question_ext.sort_order'));
		record1.setValue('quest_question_ext.sort_order', record2SortOrder);
		record2.setValue('quest_question_ext.sort_order', record1SortOrder);
		record1.isNew = false;
		record2.isNew = false;
		this.questQuestionsDataSource.saveRecord(record1);
		this.questQuestionsDataSource.saveRecord(record2);
		this.questionsList.refresh();
	},
	
	questionSelectTitle_onFilter: function() {
		var questionnaire_title = this.questionSelectTitle.getFieldValue('questionnaire_ext.questionnaire_title');
		var restriction = "1=1 ";
		//restriction += " AND NOT EXISTS (SELECT 1 FROM quest_question_ext WHERE quest_question_ext.question_id = question_ext.question_id AND quest_question_ext.questionnaire_id = " + this.questionnaire_id + ")";		
		if (questionnaire_title != '') restriction += " AND EXISTS(SELECT 1 FROM quest_question_ext LEFT OUTER JOIN questionnaire_ext ON questionnaire_ext.questionnaire_id = quest_question_ext.questionnaire_id WHERE quest_question_ext.question_id = question_ext.question_id AND questionnaire_ext.questionnaire_title  LIKE '%" + questionnaire_title + "%')";		
		
		var regulation = this.questionSelectTitle.getFieldValue('regrequirement.regulation');
		var reg_program = this.questionSelectTitle.getFieldValue('regrequirement.reg_program');
		var reg_requirement = this.questionSelectTitle.getFieldValue('regrequirement.reg_requirement');
		if (this.view.taskInfo.activityId == 'AbRiskCompliance' && (regulation != '' || reg_program != '' || reg_requirement != ''))  {
			restriction += " AND EXISTS(SELECT 1 FROM quest_question_ext LEFT OUTER JOIN regrequirement ON regrequirement.questionnaire_id = quest_question_ext.questionnaire_id WHERE quest_question_ext.question_id = question_ext.question_id ";
			if (regulation != '') restriction += " AND regrequirement.regulation = '" + regulation + "'";
			if (reg_program != '') restriction += " AND regrequirement.reg_program = '" + reg_program + "'";
			if (reg_requirement != '') restriction += " AND regrequirement.reg_requirement = '" + reg_requirement + "'";
			restriction += ")";
		}
		this.questionSelectPanel.refresh(restriction);
	},
	
	questionSelectTitle_onClear: function() {
		this.questionSelectTitle.clear();
		//var restriction = " NOT EXISTS (SELECT 1 FROM quest_question_ext WHERE quest_question_ext.question_id = question_ext.question_id AND quest_question_ext.questionnaire_id = " + this.questionnaire_id + ")";
		//this.questionSelectPanel.refresh(restriction);
	},
	
	/* Edit Question */
	
	questQuestionForm_onSave: function() {
		if (!isMultipleChoice()) {
			this.questQuestionForm.setFieldValue('quest_question_ext.display_format', 'Long Form');
			this.questQuestionForm.setFieldValue('quest_question_ext.show_answer_text', '1');
		}
		if (!this.questQuestionForm.save()) return;
		this.questionsList.refresh();
		if (isMultipleChoice()) copyHeaderAnswers(this.questQuestionForm, this.questionnaire_id, this.parent_question_id);
	},
	
	/* The only time the questionForm is a newRecord is when it is for a new child question/action.  
	 * The hierarchy id will have been filled in with the parent hierarchy_ids and upon save 
	 * the new child question_id will be appended to the end.  */
	questionForm_afterRefresh: function() {
        var question_id = this.questionForm.getFieldValue('question_ext.question_id');
        var question_type = this.questionForm.getFieldValue('question_ext.question_type');
        var hierarchy_ids = this.questionForm.getFieldValue('question_ext.hierarchy_ids');
        this.isChildQuestion = getIsChildQuestion(hierarchy_ids);
        if (this.questionForm.newRecord) {
            this.isChildQuestion = true;
        }
        if (question_type === 'action') {
            this.isChildActionQuestion = true;
        } else {
            this.isChildActionQuestion = false;
        }

        var isHeader = isHeaderQuestion(this.questionForm);
        if (this.isChildQuestion) {
            var parent_id = getSecondToLastHierId(hierarchy_ids);
            if (this.questionForm.newRecord) {
                parent_id = getLastHierId(hierarchy_ids);
            }
            this.questionForm.setFieldValue('question_ext.parent_id', parent_id);
        }

        this.questionForm.actions.get('deleteChildQuestion').show(this.isChildQuestion && !this.questionForm.newRecord);

        if (!this.isChildQuestion) {
            var questRestriction = new Ab.view.Restriction();
            questRestriction.addClause('quest_question_ext.questionnaire_id', this.questionnaire_id);
            questRestriction.addClause('quest_question_ext.question_id', question_id);
            this.questQuestionForm.refresh(questRestriction);
        } else {
            this.questQuestionForm.show(false);
        }

        refreshAnswerOptionsGrid(this.answerOptionsGrid, question_id);

        if (isHeader) {
            this.answerOptionsGrid.setInstructions(getMessage('headerAnswerInstructions'));
        } else {
            this.answerOptionsGrid.setInstructions(getMessage('multipleChoiceAnswerInstructions'));
        }

        if (isHeader) {
            this.collapseQuestionPanels(false, true, false);
        } else if (isMultipleChoice()) {
            this.collapseQuestionPanels(false, false, false);
        } else {
            this.collapseQuestionPanels(false, false, true);
        }

        setParentAnswerText(this.questionForm);
        setQuestionTitle(this.questionForm, this.questQuestionForm, question_id, false, isHeader, this.isChildQuestion, this.isChildActionQuestion);
        onChangeQuestionType();
	},
	
	/* UPDATING A PARENT QUESTION
		- Updates to a parent question include:
			1) Updating the parent question using the question form
			2) Adding, updating or deleting a child question
			3) Adding, updating or deleting an answer option for the parent question or for one of its child questions
		- Note: questionForm_onSave is called by saving either a parent question or a child question

		TEST 1: If it is a form update, validate form fields
			- NO - alert user and cancel update
			- YES - go to TEST 2

 		TEST 2: Do answers exist for the parent question in this questionnaire?
			- YES - If Answers exist for the parent question in this questionnaire, alert user and cancel update.
			- NO - go to TEST 3

 		TEST 3: Do answers exist for the parent question in other questionnaires then?
			- YES - Copy the parent question and all child questions and answer options.  Replace the quest_question_ext with the new question.  Make the update to the copied parent question.  Refresh the question form to the copy of the same question as was previously opened.
			- NO - go to TEST 4

 		TEST 4: Does the parent question exist in other Questionnaires?
			- NO - Make the update to the parent question.
			- YES - show Confirm Update dialog with message listing all questionnaire titles
					CONFIRM UPDATE DIALOG - Allow user to choose between updating the question in this questionnaire only or in all questionnaires
						- THIS QUESTIONNAIRE ONLY - Copy the parent question and all child questions and answer options.  Replace the quest_question_ext with the new question.  Make the update to the copied parent question.  Refresh the question form to the copy of the same question as was previously opened.
						- ALL QUESTIONNAIRES - Make the update to the parent question. */
	questionForm_onSave: function() {
		var controller = this;
		if (!beforeSaveQuestionForm(controller, 'questionForm')) return false;
		
		var question_id = this.questionForm.getFieldValue('question_ext.question_id');
		var updateType = 'updateParent';
		if (this.isChildQuestion) updateType = 'updateChild';
		if (this.isChildQuestion && this.questionForm.newRecord) updateType = 'addChild';
		
		if (questionAnswersExist(this.parent_question_id, this.questionnaire_id)) {
			View.alert(getMessage('updateParentQuestionWithAnswers'));
		} else if (questionAnswersExist(this.parent_question_id, null)) {
			this.performCopyAsNew(this.parent_question_id, true, true, updateType, question_id, null);
		} else {
			var questions = getQuestionsInQuestionnaires(this.parent_question_id, null);
			if (questions.length > 1) {
				var msgId = 'updateParentQuestionInOtherQuestionnaires';
				if (isHeaderQuestion(this.questionForm)) msgId += 'Header';
				this.confirmSaveOrCopyPanel.updateType = updateType;
				this.confirmSaveOrCopyPanel.updateId = question_id;
				this.confirmSaveOrCopyPanel.showInWindow({width:500, height:200, closeButton:false});
				this.confirmSaveOrCopyPanel.setInstructions(getMessageListQuestionnaires(msgId, questions));
			} else {
				this.questionForm_saveQuestion();
			}
		}
	},
	
	questionForm_saveQuestion: function() {
		var newRecord = this.questionForm.newRecord;
		
		if (!this.questionForm.save()) return false;
		var question_id = this.questionForm.getFieldValue('question_ext.question_id');	
		if (newRecord) {	
			var hierarchy_ids = addToHierarchyIds(this.questionForm, question_id);
			this.questionForm.setFieldValue('question_ext.hierarchy_ids', hierarchy_ids);
			this.questionForm.save();
			//if (!getIsChildQuestion(hierarchy_ids)) this.parent_question_id = question_id;
		} 
		//refreshHierTree('questionHierTree', this.parent_question_id);
		refreshTreeAndExpandParent('questionHierTree', newRecord, question_id, this.parent_question_id);
		this.questionForm_afterRefresh();
		this.questionsList.refresh();
		this.questionSelectPanel.refresh();
		return true;
	},	
	
	/* updateType: 'updateParent', 'updateChild', 'addChild', 'deleteChild', 'deleteAnswer', 'updateAnswer', 'addAnswer' */
	confirmSaveOrCopyPanel_onSaveOrCopyQuestion: function() {
		var updateType = this.confirmSaveOrCopyPanel.updateType;
		var updateId = this.confirmSaveOrCopyPanel.updateId;
		var useFormValues = (updateType.indexOf('delete') == 0)?false:true;
		
		if ($('selectSave_save').checked) { /* make change to all questionnaires, i.e. update the existing question record */
			if (updateType == 'deleteChild') {
				View.openProgressBar(getMessage('deleting'));
				performDeleteQuestion(updateId);	
				View.closeProgressBar(); 
			}			
			else if (updateType == 'deleteAnswer') {
				View.openProgressBar(getMessage('deleting'));
				deleteAnswerOption(updateId);
				this.answerOptionsGrid.refresh();
				this.answerOptionsForm.closeWindow();
				View.closeProgressBar(); 
			} else if (updateType == 'updateAnswer' || updateType == 'addAnswer') {
				this.answerOptionsForm.save();
				this.answerOptionsGrid.refresh();	
				this.answerOptionsForm.closeWindow();
			} else this.questionForm_saveQuestion();
			refreshHierTree('questionHierTree', this.parent_question_id);
		}
		else this.performCopyAsNew(this.parent_question_id, useFormValues, true, updateType, updateId, null);
	},
	
	questionsList_onCopyAsNew: function(row) {
		var controller = this;
		var old_question_id = row.getFieldValue('quest_question_ext.question_id');
		var message = getMessage('copyAsNew');
		View.confirm(message, function(button){
			if (button == 'yes') {
				controller.performCopyAsNew(old_question_id, false, false, 'copyQuestion', old_question_id, null);
			} else {
				return;
			}
		});
	},
	
	/* updateType: 'copyQuestion', 'updateParent', 'updateChild', 'addChild', 'deleteChild', 'deleteAnswer', 'updateAnswer', 'addAnswer' */
	performCopyAsNew: function(old_parent_question_id, useFormValues, replaceParentQuestion, updateType, updateId, sortOrder) {
		var controller = this;
		View.openProgressBar(getMessage('saving'));
		var childQuestionRecord = null;
		var parentQuestionRecord = null;
		var answerOptionRecord = null;
		
		if (useFormValues) { // store the entered values in a record but start copy with the parent question
			if (updateType == 'updateAnswer' || updateType == 'addAnswer') {
				answerOptionRecord = this.answerOptionsForm.getRecord();
			} else {
				if (beforeSaveQuestionForm(controller, 'questionForm')) {
					if (updateType == 'updateParent') parentQuestionRecord = this.questionForm.getRecord();
					else childQuestionRecord = this.questionForm.getRecord(); 
				} else return;
			}
		}
		this.parent_question_id = copyParentQuestion(controller, this.questionnaire_id, old_parent_question_id, parentQuestionRecord, replaceParentQuestion, sortOrder);
			
		this.copiedQuestionIds = [];
		this.copiedQuestionIds.push({
			'old_question_id': old_parent_question_id,
			'new_question_id': this.parent_question_id
		});
		View.updateProgressBar(1/4);
		
		this.copiedAnswerIds = [];
		this.copyAnswerOptions(old_parent_question_id, this.parent_question_id, answerOptionRecord);
		
		childQuestionRecord = this.copyChildQuestionsAndAnswers(old_parent_question_id, childQuestionRecord, answerOptionRecord);
		View.updateProgressBar(1/2);			
		this.copyChildHierarchyAndParentCondition();
		if (updateType == 'deleteChild') performDeleteQuestion(getNewCopiedId(this.copiedQuestionIds, 'question_id', updateId));
		else if (updateType == 'deleteAnswer') deleteAnswerOption(getNewCopiedId(this.copiedAnswerIds, 'answer_option_id', updateId));
		
		this.questionsList.refresh();
		this.questionSelectPanel.refresh();
		if (updateType == 'updateAnswer' || updateType == 'addAnswer') this.answerOptionsForm.closeWindow();
		refreshHierTree('questionHierTree', this.parent_question_id);
		
		var restriction = getRefreshQuestionRestriction(updateType, this.questionForm, this.parent_question_id, this.copiedQuestionIds, childQuestionRecord);
        this.refreshQuestionForm(restriction);
		View.closeProgressBar();
	},
	
	/* answerOptionRecord, if not null, contains form values for a new or updated record */
	copyAnswerOptions: function(old_question_id, new_question_id, answerOptionRecord) {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('quest_answer_option_ext.question_id', old_question_id);
		var records = this.answerOptionsDs.getRecords(restriction);
		var copiedAnswerOptionRecordFound = false;
		var new_answer_option_id = 0;
		for (var i=0;i<records.length;i++) {
			var record = records[i];
			var old_answer_option_id = record.getValue('quest_answer_option_ext.answer_option_id');
			if (answerOptionRecord && parseInt(answerOptionRecord.getValue('quest_answer_option_ext.answer_option_id')) == parseInt(old_answer_option_id)) {
				record = answerOptionRecord; //use the record that the user modified
				copiedAnswerOptionRecordFound = true;
			} 
			new_answer_option_id = copyAnswer(record, new_question_id);
			this.copiedAnswerIds.push({
				'old_answer_option_id': old_answer_option_id,
				'new_answer_option_id': new_answer_option_id
			});
		}
		if (answerOptionRecord && !copiedAnswerOptionRecordFound) {//this is a new answer option record added to the copied question
			new_answer_option_id = copyAnswer(answerOptionRecord, new_question_id);
			this.copiedAnswerIds.push({
				'old_answer_option_id': new_answer_option_id, //this is moot; because this was a new answer option record, there will be no instances of use of the new answer_option_id as a parent_answer_condition
				'new_answer_option_id': new_answer_option_id
			});
		}
	},
	
	/* childQuestionRecord or answerOptionRecord, if not null, contain form values for a new or updated record */
	copyChildQuestionsAndAnswers: function(parent_id, childQuestionRecord, answerOptionRecord) {		
		var restriction = new Ab.view.Restriction();
		restriction.addClause('question_ext.hierarchy_ids', parent_id + '|%', 'LIKE');
		restriction.addClause('question_ext.hierarchy_ids', parent_id + '|', '<>');
		var childRecords = this.questionsDataSource.getRecords(restriction);
		var copiedChildRecordFound = false;
		var new_question_id = 0;
		for (var i=0;i<childRecords.length;i++) {
			var record = childRecords[i];
			var old_question_id = record.getValue('question_ext.question_id');
			if (childQuestionRecord && parseInt(childQuestionRecord.getValue('question_ext.question_id')) == parseInt(old_question_id)) {
				record = childQuestionRecord;//use the record that the user modified
				copiedChildRecordFound = true;
			} 
			new_question_id = copyQuestion(record, false);
			this.copiedQuestionIds.push({
				'old_question_id': old_question_id,
				'new_question_id': new_question_id
			});
			this.copyAnswerOptions(old_question_id, new_question_id, answerOptionRecord);
		}
		if (childQuestionRecord && !copiedChildRecordFound) {//this is a new child record added to the copied question
			new_question_id = copyQuestion(childQuestionRecord, true);
			this.copiedQuestionIds.push({
				'old_question_id': new_question_id,//because this was a new child record and there was no old question_id, hierarchy was populated with the child's new question_id
				'new_question_id': new_question_id
			});
			childQuestionRecord.setValue('question_ext.question_id', new_question_id);
		}
		return childQuestionRecord;
	},
	
	/* copyChildHierarchyAndParentCondition - 
	 * 
	 * 1) We will do a global replace of all old ids in the hierarchy_ids with new copied ids.
	 * Note that we are updating the hierarchy_ids after all child records have been saved. 
	 * This is because the child hierarchy_ids contain multiple ids appearing on multiple levels.
	 * We will replace them all at once after they have all been created.
	 * 
	 * 2) Replace the parent_answer_condition with the new copied answer_option_id. 
	 */
	copyChildHierarchyAndParentCondition: function() {
		for (var i=1; i<this.copiedQuestionIds.length;i++) { // skip record 0 because that is the parent question, which already has an updated hierarchy and no parent condition value
			var old_question_id = this.copiedQuestionIds[i].old_question_id;
			var new_question_id = this.copiedQuestionIds[i].new_question_id;
			
			var restriction = new Ab.view.Restriction();
			restriction.addClause('question_ext.question_id', new_question_id);
			var record = this.questionsDataSource.getRecord(restriction);
			record.isNew = false;
			
			var hierarchy_ids = record.getValue('question_ext.hierarchy_ids');
			hierarchy_ids = replaceWithCopiedQuestionIds(hierarchy_ids, this.copiedQuestionIds);
			record.setValue('question_ext.hierarchy_ids', hierarchy_ids);
			
			var old_parent_answer_condition = record.getValue('question_ext.parent_answer_condition');
			var new_parent_answer_condition = getCopiedParentAnswerCondition(old_parent_answer_condition, this.copiedAnswerIds);
			if (new_parent_answer_condition != 0) record.setValue('question_ext.parent_answer_condition', new_parent_answer_condition);
			
			this.questionsDataSource.saveRecord(record);			
		}
	},
	
	/* DELETING A CHILD QUESTION
  	
		TEST 1: Do answers exist for the parent question in this questionnaire?
			- YES - If Answers exist for the parent question in this questionnaire, alert user and cancel delete.
			- NO - go to TEST 2

 		TEST 2: Do answers exist for the parent question in other questionnaires then?
			- YES - Copy the parent question and all child questions and answer options.  Replace the quest_question_ext with the new question.  
					Delete the child and any of its children and answer options from the copied parent question.  Refresh the question form to the copy of the parent question.
			- NO - go to TEST 3

 		TEST 3: Does the parent question exist in other Questionnaires?
			- NO - Delete the child question.  Refresh the question form to the parent question.
			- YES - show Confirm Update dialog with message listing all questionnaire titles
					CONFIRM UPDATE DIALOG - Allow user to choose between deleting the child from the question in this questionnaire only or in all questionnaires
						- THIS QUESTIONNAIRE ONLY - Copy the parent question and all child questions and answer options.  Replace the quest_question_ext with the new question.  
							Delete the child and any of its children and answer options from the copied parent question.  Refresh the question form to the copy of the parent question.
						- ALL QUESTIONNAIRES - Delete the child question. Refresh the question form to the parent question */
	deleteChildQuestion: function() {
		var controller = this;
		var child_question_id = this.questionForm.getFieldValue('question_ext.question_id');
		var confirmDeleteMsgId = 'deleteChildQuestion';
		if (this.isChildActionQuestion) confirmDeleteMsgId += 'Action';
		
		if (questionAnswersExist(this.parent_question_id, this.questionnaire_id)) {
			View.alert(getMessage('deleteParentQuestionWithAnswers'));
		} else if (questionAnswersExist(this.parent_question_id, null)) {
			View.confirm(getMessage(confirmDeleteMsgId), function(button){
				if (button == 'yes') {
					controller.performCopyAsNew(controller.parent_question_id, false, true, 'deleteChild', child_question_id, null);
				}
			});
		} else {
			var questions = getQuestionsInQuestionnaires(this.parent_question_id, null);
			if (questions.length > 1) {
				this.confirmSaveOrCopyPanel.updateType = 'deleteChild';
				this.confirmSaveOrCopyPanel.updateId = child_question_id;
				this.confirmSaveOrCopyPanel.showInWindow({width:500, height:200, closeButton:false});
				this.confirmSaveOrCopyPanel.setInstructions(getMessageListQuestionnaires('updateParentQuestionInOtherQuestionnaires', questions));
			} else {
				View.confirm(getMessage(confirmDeleteMsgId), function(button){
					if (button == 'yes') {
						View.openProgressBar(getMessage('deleting'));
						performDeleteQuestion(child_question_id);	
						View.closeProgressBar();
						refreshHierTree('questionHierTree', controller.parent_question_id);
						var restriction = new Ab.view.Restriction();
						restriction.addClause('question_ext.question_id', controller.parent_question_id);
                        controller.refreshQuestionForm(restriction);
					}
				});
			}
		}
	},
	
	/* DELETING AN ANSWER OPTION
	
		TEST 1: Do answers exist for the parent question in this questionnaire?
			- YES - If Answers exist for the parent question in this questionnaire, alert user and cancel delete.
			- NO - go to TEST 2

 		TEST 2: Do answers exist for the parent question in other questionnaires then?
			- YES - Copy the parent question and all child questions and answer options.  Replace the quest_question_ext with the new question.  
					Delete the answer option from the copied question.  Refresh the question form to the copy of the selected question.
			- NO - go to TEST 3

 		TEST 3: Does the parent question exist in other Questionnaires?
			- NO - Delete the answer option.  Refresh the question form.
			- YES - show Confirm Update dialog with message listing all questionnaire titles
					CONFIRM UPDATE DIALOG - Allow user to choose between deleting the answer option from the question in this questionnaire only or in all questionnaires
						- THIS QUESTIONNAIRE ONLY - Copy the parent question and all child questions and answer options.  Replace the quest_question_ext with the new question.  
							Delete the answer option from the copied question.  Refresh the question form to the copy of the selected question.
						- ALL QUESTIONNAIRES - Delete the answer option. Refresh the question form. */
	answerOptionsForm_onDelete: function() {
		var controller = this;
		var answer_option_id = this.answerOptionsForm.getFieldValue('quest_answer_option_ext.answer_option_id');
		
		if (questionAnswersExist(this.parent_question_id, this.questionnaire_id)) {
			View.alert(getMessage('deleteParentQuestionWithAnswers'));
		} else if (questionAnswersExist(this.parent_question_id, null)) {
			View.confirm(getMessage('deleteAnswerOption'), function(button){
				if (button == 'yes') {
					controller.performCopyAsNew(controller.parent_question_id, false, true, 'deleteAnswer', answer_option_id, null);
				}
			});
		} else {
			var questions = getQuestionsInQuestionnaires(this.parent_question_id, null);
			if (questions.length > 1) {
				this.confirmSaveOrCopyPanel.updateType = 'deleteAnswer';
				this.confirmSaveOrCopyPanel.updateId = answer_option_id;
				this.confirmSaveOrCopyPanel.showInWindow({width:500, height:200, closeButton:false});
				this.confirmSaveOrCopyPanel.setInstructions(getMessageListQuestionnaires('updateParentQuestionInOtherQuestionnaires', questions));
			} else {
				View.confirm(getMessage('deleteAnswerOption'), function(button){
					if (button == 'yes') {
						controller.answerOptionsForm.deleteRecord();				
						controller.answerOptionsGrid.refresh();
						refreshHierTree('questionHierTree', controller.parent_question_id);
						controller.answerOptionsForm.closeWindow();
					}
				});
			}
		}
	},
	
	/* ADDING OR UPDATING AN ANSWER OPTION

		TEST 1: Validate form fields
		- NO - show errors and cancel save
		- YES - go to TEST 2
	
		TEST 2: Do answers exist for the parent question in this questionnaire?
		- YES - If Answers exist for the parent question in this questionnaire, alert user and cancel save.
		- NO - go to TEST 3

		TEST 3: Do answers exist for the parent question in other questionnaires then?
		- YES - Copy the parent question and all child questions and answer options.  Replace the quest_question_ext with the new question.  
				Add or update the answer option to the copy of the selected question.  Refresh the question form to the copy of the selected question.
		- NO - go to TEST 4

		TEST 4: Does the parent question exist in other Questionnaires?
		- NO - Add or update the answer option.  Refresh the answer options grid.
		- YES - show Confirm Update dialog with message listing all questionnaire titles
				CONFIRM UPDATE DIALOG - Allow user to choose between adding or updating the answer option to the question in this questionnaire only or in all questionnaires
					- THIS QUESTIONNAIRE ONLY - Copy the parent question and all child questions and answer options.  Replace the quest_question_ext with the new question.  
						Add or update the answer option to the copy of the selected question.  Refresh the question form to the copy of the selected question.
					- ALL QUESTIONNAIRES - Add or update the answer option. Refresh the answer options grid. 
		STEP 5: Close the dialog. */
	answerOptionsForm_onSave: function() {
		var controller = this;
		if (!this.answerOptionsForm.canSave()) return false;
		
		var updateType = 'updateAnswer';
		if (this.answerOptionsForm.newRecord) updateType = 'addAnswer';	
		var answer_option_id = this.answerOptionsForm.getFieldValue('quest_answer_option_ext.answer_option_id');
		
		if (questionAnswersExist(this.parent_question_id, this.questionnaire_id)) {
			View.alert(getMessage('updateParentQuestionWithAnswers'));
		} else if (questionAnswersExist(this.parent_question_id, null)) {
			controller.performCopyAsNew(controller.parent_question_id, true, true, updateType, answer_option_id, null);
		} else {
			var questions = getQuestionsInQuestionnaires(this.parent_question_id, null);
			if (questions.length > 1) {
				this.confirmSaveOrCopyPanel.updateType = updateType;
				this.confirmSaveOrCopyPanel.updateId = answer_option_id;
				this.confirmSaveOrCopyPanel.showInWindow({width:500, height:200, closeButton:false});
				this.confirmSaveOrCopyPanel.setInstructions(getMessageListQuestionnaires('updateParentQuestionInOtherQuestionnaires', questions));
			} else {
				this.answerOptionsForm.save();
				this.answerOptionsGrid.refresh();
				refreshHierTree('questionHierTree', this.parent_question_id);
				this.answerOptionsForm.closeWindow();
			}
		}		
	},
	
	/* Add the child to the last-clicked item in tree.  If no item has been selected, use the top-level question. */
	questionHierTree_onAddChild: function(isAction) {
		var currentNode = this.questionHierTree.lastNodeClicked;
		if (currentNode == null) currentNode = this.questionHierTree.treeView.getRoot().children[0];
		var question_id = currentNode.data['question_ext.question_id'];
		
		if (!isMultipleChoice(question_id)) {
			View.alert(getMessage('notTypeMultiple'));
			return;
		}
		var question_type = 'freeform';
		if (isAction) question_type = 'action';
		var hierarchy_ids = getHierarchyIds(question_id);
		var restriction = new Ab.view.Restriction();
		restriction.addClause('question_ext.question_type', question_type);
		restriction.addClause('question_ext.hierarchy_ids', hierarchy_ids);
        this.refreshQuestionForm(restriction, true);
		if (isAction) {
			$('selectActionTypeSelector').value = 'serviceRequest';
			onChangeActionTypeSelector();
			this.questionForm.setFieldValue('question_ext.question_text', getMessage('defaultActionText'));
		}
	},

    /**
	 * APP-2359: it seems that collapseForms and collapseAnswerOptions always have the same value;
	 * I removed nested layout regions so that all three panels are in the same region,
	 * and the collapseAnswerOptions parameter is not unused.
     */
	collapseQuestionPanels: function(collapseForms, collapseTree, collapseAnswerOptions) {
		var mainLayout = View.getLayoutManager('mainLayout');
		var nestedCenter = View.getLayoutManager('nestedCenter');

		if (collapseForms) {
			mainLayout.collapseRegion('east');
			this.questionForm.show(false);
			this.questQuestionForm.show(false);
			this.answerOptionsGrid.show(false);
		} else {
			mainLayout.expandRegion('east');

            if (collapseAnswerOptions) {
                this.answerOptionsGrid.show(false);
            } else {
                this.answerOptionsGrid.show(true);
            }
		}

		if (collapseTree) {
			nestedCenter.collapseRegion('south');
			this.questionHierTree.show(false);
		} else {
			nestedCenter.expandRegion('south');
        }
	},
	
	questionnaireProfile_onProfile: function() {
		editQuestionnaire(this.questionnaire_id, false);
	},
	
	questionnaireList_onAddNew: function() {
		editQuestionnaire(null, true);
	},
	
	questionnaireList_onEditQuestionnaire: function(row) {
		editQuestionnaire(row.getRecord().getValue('questionnaire_ext.questionnaire_id'), false);
	},
    onSelectUnitType: function () {
		View.openDialog('ab-common-quest-ext-sel-unit.axvw', null, false , {
            unitTypePrefix: this.measurement.unitTypePrefix,
            activityId: this.measurement.activityId,
            unitType: 'bill_type',
			callback: function (record) {
                var questionForm = View.panels.get('questionForm');
                questionForm.setFieldValue('question_ext.unit_type_display', record.getValue('bill_type.bill_type_id_display'));
                questionForm.setFieldValue('question_ext.unit_type', record.getValue('bill_type.bill_type_id'));
                questionForm.setFieldValue('question_ext.unit_default', '');
                View.closeDialog();
			}
		});
	},
    onSelectUnitsUnit: function () {
		var unitTypeValue = this.questionForm.getFieldValue('question_ext.unit_type_display');
        View.openDialog('ab-common-quest-ext-sel-unit.axvw', null, false , {
            unitTypePrefix: this.measurement.unitTypePrefix,
            activityId: this.measurement.activityId,
            unitType: 'bill_unit',
            unitTypeValue: unitTypeValue,
            callback: function (record) {
            	var questionForm = View.panels.get('questionForm');
                questionForm.setFieldValue('question_ext.unit_type_display', record.getValue('bill_unit.bill_type_id_display'));
                questionForm.setFieldValue('question_ext.unit_type', record.getValue('bill_unit.bill_type_id'));
                questionForm.setFieldValue('question_ext.unit_default', record.getValue('bill_unit.bill_unit_id'));
                View.closeDialog();
            }
        });
	}
});

/**
 * Custom DD drag class that is used by grid to handle drag events.
 */
var GridControllerDragSource = function (panelId, row, callbackFn) {
    this.node = row;
    var id = '';
    var text = row.getFieldValue('afm_tbls.question_text');
    var type = row.getFieldValue('afm_tbls.question_type');
    if (panelId == 'questionSelectPanel') {
    	id = row.getFieldValue('question_ext.question_id');
    	text = row.getFieldValue('question_ext.question_text');
    	type = row.getFieldValue('question_ext.question_type');
    }
    this.dragData = {
    	selectPanelId: panelId,
        'question_id': id,
        'question_text': text,
        'question_type': type,
        dragState: 'new',
        position: 0
    };
    this.callbackFn = callbackFn;
    GridControllerDragSource.superclass.constructor.call(this, row.dom.children[0]);
};

Ext.extend(GridControllerDragSource, Ext.dd.DragSource, {
    scroll: false,
    
    onInitDrag: function (x, y) {
        var dragEl = '<div>' + this.dragData['question_id'] + ' ' + this.dragData['question_text'] + '</div>';
        dragEl.id = Ext.id();
        this.proxy.update(dragEl);
        this.onStartDrag(x, y);
        return true;
    },
    onDragEnter: function () {
       
    },
    onEndDrag: function () {
        
    }
});

function selectQuestion(context) {
	var controller = View.controllers.get('abCommonQuestExtQuestionnaireDef');
    var parentPanel = View.panels.get(context.command.parentPanelId);
    var selectedRecord = parentPanel.gridRows.get(parentPanel.selectedRowIndex).getRecord();
	var question_id = selectedRecord.getValue('quest_question_ext.question_id');
	controller.parent_question_id = question_id;
	
	refreshHierTree('questionHierTree', controller.parent_question_id);
	var restriction = new Ab.view.Restriction();
	restriction.addClause('question_ext.question_id', question_id);
    controller.refreshQuestionForm(restriction);
}

function onChangeQuestionType() {
	var controller = View.controllers.get('abCommonQuestExtQuestionnaireDef');	
	var form = View.panels.get('questionForm');	
	var question_type = form.getFieldValue('question_ext.question_type');
	
	form.fields.get('question_ext.activity_type').fieldDef.required = false;
	$('question_ext.activity_type.asterisk').innerHTML = '';
	Ext.get('selectQuestionTypeSelector').dom.parentNode.parentNode.style.display = '';
	Ext.get('selectActionTypeSelector').dom.parentNode.parentNode.style.display = 'none';
	Ext.get('selectMultipleChoiceType_radioSingle').dom.parentNode.parentNode.style.display = 'none';
	
	var questionFields = [];
	switch (question_type) {
		case 'multiple_choice_single':
	    	questionFields = [];
	    	$('selectMultipleChoiceType_radioSingle').checked = 'checked';
	    	Ext.get('selectMultipleChoiceType_radioSingle').dom.parentNode.parentNode.style.display = '';
	    	$('selectQuestionTypeSelector').value = 'multiple';
	        break;
	    case 'multiple_choice_multi':
	    	questionFields = [];
	    	$('selectMultipleChoiceType_radioMultiple').checked = 'checked';
	    	Ext.get('selectMultipleChoiceType_radioSingle').dom.parentNode.parentNode.style.display = '';
	    	$('selectQuestionTypeSelector').value = 'multiple';
	        break;
	    case 'freeform':
	    	questionFields = ['question_ext.freeform_width'];
	    	$('selectQuestionTypeSelector').value = 'freeform';
	        break;
	    case 'date':
	    	questionFields = ['question_ext.num_responses_max'];
	    	$('selectQuestionTypeSelector').value = 'date';
	        break;
	    case 'time':
	    	questionFields = ['question_ext.num_responses_max'];
	    	$('selectQuestionTypeSelector').value = 'time';
	        break;
	    case 'lookup':
	    	questionFields = ['question_ext.lookup_table', 'question_ext.lookup_field', 'question_ext.num_responses_max'];
	    	$('selectQuestionTypeSelector').value = 'lookup';
	        break;
	    case 'count':
	    	questionFields = ['question_ext.num_responses_max'];
	    	$('selectQuestionTypeSelector').value = 'count';
	        break;
	    case 'measurement':
	    	questionFields = ['question_ext.unit_type_display',  'question_ext.unit_default', 'question_ext.num_responses_max'];
	    	$('selectQuestionTypeSelector').value = 'measurement';
	        break;
	    case 'document':
	    	questionFields = ['question_ext.num_responses_max'];
	    	$('selectQuestionTypeSelector').value = 'document';
	        break;
	    case 'label':
	    	questionFields = [];
	    	Ext.get('selectQuestionTypeSelector').dom.parentNode.parentNode.style.display = 'none';
	    	break;
	    case 'action':
	    	questionFields = ['question_ext.question_type', 'question_ext.activity_type'];	    	
	    	form.fields.get('question_ext.activity_type').fieldDef.required = true;
	    	$('question_ext.activity_type.asterisk').innerHTML = '*';
	    	Ext.get('selectQuestionTypeSelector').dom.parentNode.parentNode.style.display = 'none';
	    	
	    	Ext.get('selectActionTypeSelector').dom.parentNode.parentNode.style.display = '';
	    	form.enableField('question_ext.activity_type', true);
	    	var activity_type = form.getFieldValue('question_ext.activity_type');
	    	if (activity_type == 'SERVICE DESK - MAINTENANCE') {
	    		$('selectActionTypeSelector').value = 'serviceRequest';
	    		form.enableField('question_ext.activity_type', false);
	    	}
	    	else if (isWorkflowRuleAction(activity_type)) $('selectActionTypeSelector').value = 'workflowRule';
	    	else $('selectActionTypeSelector').value = 'other';
	    	break;
	    default:
	    	break;
	}
	
	if (form.getFieldValue('question_ext.question_id_old')) questionFields.push('question_ext.question_id_old');
	
	if (controller.isChildQuestion) {		
		questionFields.push('question_ext.parent_answer_select');
		Ext.get('parent_answer_select_space').dom.parentNode.parentNode.style.display = '';
		if (!controller.isChildActionQuestion) {
			questionFields.push('question_ext.sort_order_child');
			questionFields.push('question_ext.is_required_child');
		}		
		if (form.newRecord) questionFields.push('question_ext.parent_id');
		else questionFields.push('question_ext.hierarchy_ids');
	} else Ext.get('parent_answer_select_space').dom.parentNode.parentNode.style.display = 'none';
	
	showQuestionFields(questionFields);
	showQuestQuestionFields();
}

function showQuestionFields(questionFields) {
	var controller = View.controllers.get('abCommonQuestExtQuestionnaireDef');
	var form = View.panels.get('questionForm');
	
	var permFields = ['question_ext.question_id', 'question_ext.question_text', 'question_ext.question_tag', 'question_ext.date_last_edit', 'question_ext.time_last_edit', 'question_ext.edited_by'];
	//var permFields = ['question_ext.is_active', 'question_ext.question_id', 'question_ext.question_text', 'question_ext.question_tag', 'question_ext.date_last_edit', 'question_ext.time_last_edit', 'question_ext.edited_by'];
	form.fields.each(function(field) {
		var id = field.getId();
		if (permFields.indexOf(id) >= 0 || questionFields.indexOf(id) >= 0) {
			form.showField(id, true);
		} else {
			form.showField(id, false);
		}
	});
	form.updateHeight();
}

function showQuestQuestionFields() {
	var controller = View.controllers.get('abCommonQuestExtQuestionnaireDef');
	var questQuestionForm = View.panels.get('questQuestionForm');
	Ext.get('showAnswerText_no').dom.parentNode.parentNode.style.display = 'none';
	
	questQuestionForm.showField('quest_question_ext.is_required', !isHeaderQuestion(controller.questionForm));
	questQuestionForm.showField('quest_question_ext.display_format', false);
	if (isMultipleChoice()) {
		if (!controller.isChildQuestion) {	
			questQuestionForm.showField('quest_question_ext.display_format', true);
			Ext.get('showAnswerText_no').dom.parentNode.parentNode.style.display = '';
			var show_answer_text = questQuestionForm.getFieldValue('quest_question_ext.show_answer_text');
			if (show_answer_text == '0') {
				$('showAnswerText_no').checked = true;
				questQuestionForm.enableField('quest_question_ext.display_format', false);
			} else {
				$('showAnswerText_no').checked = false;
				questQuestionForm.enableField('quest_question_ext.display_format', true);
			}
		}		
	}
	questQuestionForm.updateHeight();
}

function setMultipleChoiceType(question_type) {
	var form = View.panels.get('questionForm');
	form.setFieldValue('question_ext.question_type', question_type);
}

function onChangeQuestionTypeSelector() {
	var form = View.panels.get('questionForm');
	var value = $('selectQuestionTypeSelector').value;
	if (value == 'multiple') {
		if ($('selectMultipleChoiceType_radioSingle').checked) value = 'multiple_choice_single';
		else value = 'multiple_choice_multi';
	} 
	form.setFieldValue('question_ext.question_type', value);
	onChangeQuestionType();
}

function onChangeActionTypeSelector() {
	var form = View.panels.get('questionForm');
	var value = $('selectActionTypeSelector').value;
	switch(value) {
		case 'serviceRequest':
			form.enableField('question_ext.activity_type', false);
			form.setFieldValue('question_ext.activity_type', 'SERVICE DESK - MAINTENANCE');
			break;
		default:
			form.enableField('question_ext.activity_type', true);
			form.setFieldValue('question_ext.activity_type', '');
			break;
	}	
}

function questionHierTree_onSelectQuestion() {
	var controller = View.controllers.get('abCommonQuestExtQuestionnaireDef');
	var tree = View.panels.get('questionHierTree');
	var lastNodeClicked = tree.lastNodeClicked;
	var question_id = lastNodeClicked.data['question_ext.question_id'];
	
	var restriction = new Ab.view.Restriction();
	restriction.addClause('question_ext.question_id', question_id);
    controller.refreshQuestionForm(restriction);
}

function assignQuestionnaire() {
	var controller = View.controllers.get('abCommonQuestExtQuestionnaireDef');
	var openerView = View.getOpenerView();
	if (openerView && openerView.parameters && openerView.parameters.assignQuestionnaire) {
		openerView.parameters.assignQuestionnaire(controller.questionnaire_id);
	}
}

function selectQuestionnaire() {
	var controller = View.controllers.get('abCommonQuestExtQuestionnaireDef');
	var openerView = View.getOpenerView();
	if (openerView && openerView.parameters && openerView.parameters.selectQuestionnaire) {
		openerView.parameters.selectQuestionnaire();
	}
}

function questionsList_addQuestion(question_type) {
	var controller = View.controllers.get('abCommonQuestExtQuestionnaireDef');
	var sortOrder = controller.highestSortOrder + 10;
    controller.addNewQuestion(question_type, sortOrder);
}

function editQuestionnaire(questionnaire_id, newRecord) {
	var controller = View.controllers.get('abCommonQuestExtQuestionnaireDef');
	var restriction = new Ab.view.Restriction();
	if (questionnaire_id) restriction.addClause('questionnaire_ext.questionnaire_id', questionnaire_id);
	View.openDialog('ab-common-quest-ext-def-questionnaire-edit.axvw', restriction, newRecord, {
		callback: function(questionnaire_id, questionnaire_title, newRecord) {
			controller.afterSaveQuestionnaire(questionnaire_id, newRecord);
		}
	});	
}

function onActivityTypeSelval() {
	var controller = View.controllers.get('abCommonQuestExtQuestionnaireDef');
	var value = $('selectActionTypeSelector').value;
	var message = getMessage('activityType');
	var visibleFields = ['activitytype.activity_type','activitytype.description'];
	restriction = '1=1';
	if (value == 'workflowRule') {
		message = getMessage('activityTypeWorkflow');
		visibleFields = ['activitytype.activity_type','activitytype.action_wfr', 'activitytype.description'];
		restriction = 'activitytype.action_wfr IS NOT NULL';
	}
	View.selectValue('questionForm',
			message,
			['question_ext.activity_type'],
			'activitytype',
			['activitytype.activity_type'],
			visibleFields,
			restriction
	);
}

function removeEllipses(fieldEl) {
	var selectValueEl = Ext.get(fieldEl).next();
	if (selectValueEl && selectValueEl.id.indexOf('_selectValue') > 0) selectValueEl.remove();
}