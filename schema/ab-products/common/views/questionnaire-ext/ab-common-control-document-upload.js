/**
 * The document upload control allows users to:
 * - Upload documents or images to the database.
 * - Views uploaded documents or images.
 * - Delete uploaded documents or images from the database.
 *
 * This control uses the standard DocumentService to upload documents.
 * It does not require a stock Form panel.
 */
var DocumentUploadControl = Base.extend({

    /**
     * Configuration properties.
     */
    config: null,

    /**
     * The Question object that owns this control.
     */
    question: null,

    /**
     * The JQuery reference to the DOM file input element.
     */
    fileInput: null,

    /**
     * Array of entries for all documents previously saved for this question.
     * Each entry has these properties:
     * - answerId: the quest_answer_ext.answer_id value;
     * - fileName: the document file name.
     */
    savedDocuments: null,

    /**
     * Control constructor.
     * @param {config} The control configuration.
     */
    constructor: function(config) {
        this.config = config;
        this.question = config.question;
        this.savedDocuments = [];

        this.render();
    },

    /**
     * Updates the UI based on the internal state.
     */
    render: function() {
        var template = Handlebars.compile(
            '{{#each savedDocuments}}' +
                '<div style="padding-bottom:4px">' +
                    '<span class="documentLabel">{{fileName}}</span>' +
                    '<img id="questionDocumentView_{{../question.id}}_{{answerId}}" class="fieldButton documentButton" src="/archibus/schema/ab-core/graphics/icons/view/ab-icon-edit-file-show.png" ext:qtip="{{../config.viewTooltip}}" hspace="1" border="0">' +
                    '{{#unless ../config.disabled}}' +
                    '<img id="questionDocumentDelete_{{../../question.id}}_{{answerId}}" class="fieldButton documentButton" src="/archibus/schema/ab-core/graphics/icons/view/ab-icon-task-cancel.png" ext:qtip="{{../../config.deleteTooltip}}" hspace="1" border="0">' +
                    '{{/unless}}' +
                '</div>' +
            '{{else}}' +
            '{{#unless config.disabled}}' +
            '<input type="file" id="questionDocument_{{question.id}}"> </input>' +
            '{{/unless}}' +
            '{{/each}}'
        );

        var container = jQuery('#' + this.config.container);
        container.empty();
        container.append(template(this));

        var control = this;
        _.each(this.savedDocuments, function(savedDocument) {
            var viewButton = jQuery('#questionDocumentView_' + control.question.id + '_' + savedDocument.answerId);
            var deleteButton = jQuery('#questionDocumentDelete_' + control.question.id + '_' + savedDocument.answerId);

            viewButton.on('click', function(event) {
                control.onViewDocument(savedDocument);
            });
            deleteButton.on('click', function(event) {
                control.onDeleteDocument(savedDocument);
            });
        });

        this.fileInput = jQuery('#questionDocument_' + this.question.id);
    },

    /**
     * Adds a saved document.
     */
    addSavedDocument: function(answerId, fileName) {
        this.savedDocuments.push({
            answerId: answerId,
            fileName: fileName
        });

        this.render();
    },

    /**
     * Deletes a saved document.
     */
    deleteSavedDocument: function(answerId) {
        this.savedDocuments = _.reject(this.savedDocuments, function(savedDocument) {
            return savedDocument.answerId === answerId;
        });

        this.render();
    },

    /**
     * Returns the file name if the user has selected a file.
     */
    getSelectedFilename: function() {
        var fileName = '';
        if (this.fileInput.length > 0) {
            fileName = this.fileInput[0].value;
        //} else if (this.savedDocuments.length > 0) {
        //    fileName = this.savedDocuments[0].fileName;
        }

        return fileName;
    },

    /**
     * Checks in a new document. Called from application code.
     */
    checkInDocument: function(answerId) {
        var control = this;
        var fileName = '';
        if (this.fileInput.length > 0) {
            fileName = this.fileInput[0].files[0].name;
        //} else if (this.savedDocuments.length > 0) {
        //    fileName = this.savedDocuments[0].fileName;
        }

        DocumentService.checkinNewFile(
            this.fileInput[0],
            {'answer_id': answerId },
            'quest_answer_ext',
            'doc',
            fileName,
            '',
            '0', {
                callback: function() {
                    control.addSavedDocument.defer(500, control, [answerId, fileName]);
                    // this method can be called for multiple questions, so displaying a message per question is not appropriate
                    // DevExpress.ui.notify(getMessage('uploaded'), 'success');
                },
                errorHandler: function(m, e) {
                    // DevExpress.ui.notify(getMessage('uploadError'), 'error');
                }
            });
    },

    /**
     * Checks in a new document.
     */
    onViewDocument: function(savedDocument) {
        View.showDocument({'answer_id': savedDocument.answerId }, 'quest_answer_ext', 'doc', savedDocument.fileName, null);
    },

    /**
     * Deletes the already checked in document.
     */
    onDeleteDocument: function(savedDocument) {
        var control = this;

        if (this.question.isUploaded) {
            DocumentService.markDeleted(
                { 'answer_id': savedDocument.answerId },
                'quest_answer_ext',
                'doc', {
                    callback: function () {
                        control.deleteSavedDocument(savedDocument.answerId);
                        DevExpress.ui.notify(getMessage('deleted'), 'success');
                    },
                    errorHandler: function (m, e) {
                        DevExpress.ui.notify(getMessage('deleteError'), 'error');
                    }
                });
        }
    },

    /**
     * Sets the control height to specified value.
     * @param {height} The height in pixels.
     */
    setHeight: function(height) {},

    /**
     * Adds a listener to specified control event.
     * @param {eventName} The name of the control event, e.g. 'click' or 'locateBuilding'.
     * @param {listener} The listener function reference.
     */
    addEventListener: function(eventName, listener) {},

    /**
     * Removes listeners from specified control event.
     * @param {eventName} The name of the control event, e.g. 'click' or 'locateBuilding'.
     * @param {listener} The listener function reference. Optional. If not specified, the control removes all event listeners for this event.
     */
    removeEventListener: function(eventName) {}
});