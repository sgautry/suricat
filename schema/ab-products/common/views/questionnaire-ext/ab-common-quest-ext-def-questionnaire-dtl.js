var abCommonQuestExtQuestionnaireDefDtlController = View.createController('abCommonQuestExtQuestionnaireDefDtl', {
	questionnaire_id: null,
	parent_question_id: '',
	isChildQuestion: false,
	isChildActionQuestion: false,
	isHeader: false,
	multipleValueSeparator: Ab.form.Form.MULTIPLE_VALUES_SEPARATOR,
	
	afterInitialDataFetch: function() {
		this.questionForm.enableField('question_ext.parent_answer_select', false);
		this.questionnaire_id = View.parameters.questionnaire_id;
		if (View.parameters.question_type && View.parameters.question_type == 'label') this.isHeader = true;
		this.parent_question_id = View.parameters.question_id;
		this.refreshHierTree();
		
		var restriction = new Ab.view.Restriction();
		
		if (View.parameters.isChildQuestion) {
			restriction.addClause('question_ext.question_id', View.parameters.child_question_id);
			this.questionForm.refresh(restriction);
			this.setViewChildQuestion(View.parameters.child_question_id);
		} else {
			restriction.addClause('question_ext.question_id', this.parent_question_id);
			this.questionForm.refresh(restriction, false);
			this.setViewParentQuestion();
		}
		
		if (this.isHeader) View.setTitle(getMessage('viewHeaderTitle'));
	},
	
	setViewParentQuestion: function() {
		this.isChildQuestion = false;
		this.isChildActionQuestion = false;
		setQuestionTitle(this.questionForm, null, this.parent_question_id, false, this.isHeader, this.isChildQuestion, this.isChildActionQuestion);
		onChangeQuestionType();
	},
	
	setViewChildQuestion: function(question_id) {
		this.isChildQuestion = true;
		this.isChildActionQuestion = false;		
		var question_type = this.questionForm.getFieldValue('question_ext.question_type');
		if (question_type == 'action') this.isChildActionQuestion = true;		
		
		setParentAnswerText(this.questionForm);
		setQuestionTitle(this.questionForm, null, question_id, false, this.isHeader, this.isChildQuestion, this.isChildActionQuestion);
		onChangeQuestionType();
	},
	
    refreshHierTree: function() {
		this.questionHierTree.addParameter('questionId', this.parent_question_id);
		this.questionHierTree.addParameter('multipleValueSeparator', this.multipleValueSeparator);
		this.questionHierTree.refresh();
		var treePanel = View.panels.get('questionHierTree');
		onTreeExpandAll(treePanel, null);
		if (this.isHeader) {
			View.panels.get('questionHierTree').show(false, false);
			this.questionHierTree.setTitle('');
		} else {
			View.panels.get('questionHierTree').show(true, true);
			this.questionHierTree.setTitle(getMessage('panelTitleTree'));
		}
	}		
});

function onChangeQuestionType() {
	var controller = View.controllers.get('abCommonQuestExtQuestionnaireDefDtl');
	var form = View.panels.get('questionForm');
	var newRecord = form.newRecord;		
	var question_type = form.getFieldValue('question_ext.question_type');
	form.fields.get('question_ext.activity_type').fieldDef.required = false;
	//$('question_ext.activity_type.asterisk').innerHTML = '';
	Ext.get('selectQuestionTypeSelector').dom.parentNode.parentNode.style.display = '';
	Ext.get('selectMultipleChoiceType_radioSingle').dom.parentNode.parentNode.style.display = 'none';
	
	var questionFields = [];
	switch (question_type) {
		case 'multiple_choice_single':
	    	questionFields = [];
	    	$('selectMultipleChoiceType_radioSingle').checked = 'checked';
	    	Ext.get('selectMultipleChoiceType_radioSingle').dom.parentNode.parentNode.style.display = '';
	    	$('selectQuestionTypeSelector').value = 'multiple';
	        break;
	    case 'multiple_choice_multi':
	    	questionFields = [];
	    	$('selectMultipleChoiceType_radioMultiple').checked = 'checked';
	    	Ext.get('selectMultipleChoiceType_radioSingle').dom.parentNode.parentNode.style.display = '';
	    	$('selectQuestionTypeSelector').value = 'multiple';
	        break;
	    case 'freeform':
	    	questionFields = ['question_ext.freeform_width'];
	    	$('selectQuestionTypeSelector').value = 'freeform';
	        break;
	    case 'date':
	    	questionFields = [];
	    	$('selectQuestionTypeSelector').value = 'date';
	        break;
	    case 'time':
	    	questionFields = [];
	    	$('selectQuestionTypeSelector').value = 'time';
	        break;
	    case 'lookup':
	    	questionFields = ['question_ext.lookup_table', 'question_ext.lookup_field'];
	    	$('selectQuestionTypeSelector').value = 'lookup';
	        break;
	    case 'count':
	    	questionFields = [];
	    	$('selectQuestionTypeSelector').value = 'count';
	        break;
	    case 'measurement':
	    	questionFields = ['question_ext.unit_type', 'question_ext.unit_default'];
	    	$('selectQuestionTypeSelector').value = 'measurement';
	        break;
	    case 'document':
	    	questionFields = [];
	    	$('selectQuestionTypeSelector').value = 'document';
	        break;
	    case 'label':
	    	questionFields = [];
	    	Ext.get('selectQuestionTypeSelector').dom.parentNode.parentNode.style.display = 'none';
	        break;
	    case 'action':
	    	questionFields = ['question_ext.question_type', 'question_ext.activity_type'];	    	
	    	form.fields.get('question_ext.activity_type').fieldDef.required = true;
	    	//$('question_ext.activity_type.asterisk').innerHTML = '*';
	    	Ext.get('selectQuestionTypeSelector').dom.parentNode.parentNode.style.display = 'none';
	        break;
	    default:
	    	break;
	}
	var question_id_old = form.getFieldValue('question_ext.question_id_old');
	if (question_id_old) questionFields.push('question_ext.question_id_old');
	
	showQuestionFields(questionFields, newRecord);
}

function showQuestionFields(questionFields, newRecord) {
	var controller = View.controllers.get('abCommonQuestExtQuestionnaireDefDtl');
	var permFields = ['question_ext.question_id', 'question_ext.question_text', 'question_ext.question_tag', 'question_ext.is_active', 'question_ext.edited_by', 'question_ext.date_last_edit', 'question_ext.time_last_edit'];
	var form = View.panels.get('questionForm');
	var grid = View.panels.get('answerOptionsGrid');
	
	form.fields.each(function(field) {
		var id = field.getId();
		if (permFields.indexOf(id) >= 0 || questionFields.indexOf(id) >= 0) {
			form.showField(id, true);
		} else {
			form.showField(id, false);
		}
	});
	
	if (controller.isChildQuestion) {
		form.showField('question_ext.sort_order_child', true);
		form.showField('question_ext.is_required_child', true);
		form.showField('question_ext.parent_answer_select', true);
		Ext.get('parent_answer_select_space').dom.parentNode.parentNode.style.display = '';
		if (newRecord) {
			form.showField('question_ext.parent_id', true);
			form.showField('question_ext.hierarchy_ids', false);
		} else {
			form.showField('question_ext.hierarchy_ids', true);
		}
	} else {
		form.showField('question_ext.parent_answer_select', false);
		Ext.get('parent_answer_select_space').dom.parentNode.parentNode.style.display = 'none';
		form.showField('question_ext.hierarchy_ids', false);
	}
	
	if (controller.isHeader || isMultipleChoice()) {
		if (!newRecord) refreshAnswerOptionsGrid(grid, form.getFieldValue('question_ext.question_id'));
		else grid.show(false);
	} else {
		grid.show(false);
	}	 
	form.updateHeight();
}

function questionHierTree_onSelectQuestion() {
	var controller = View.controllers.get('abCommonQuestExtQuestionnaireDefDtl');
	var tree = View.panels.get('questionHierTree');
	var lastNodeClicked = tree.lastNodeClicked;
	var question_id = lastNodeClicked.data['question_ext.question_id'];
	var hierarchy_ids = lastNodeClicked.data['question_ext.hierarchy_ids'];
	
	var restriction = new Ab.view.Restriction();
	restriction.addClause('question_ext.question_id', question_id);
	controller.questionForm.refresh(restriction, false);
	
	var isChild = getIsChildQuestion(hierarchy_ids);
	if (isChild) controller.setViewChildQuestion(question_id);
	else controller.setViewParentQuestion();
}
