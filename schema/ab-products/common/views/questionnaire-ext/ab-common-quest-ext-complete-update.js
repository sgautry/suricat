View.createController('UpdateQuestionnaire', {
	multipleValueSeparator: Ab.form.Form.MULTIPLE_VALUES_SEPARATOR,
	activity_log_id: 2624,
	questionnaire: null,
	
	/**
     * Validates and saves answers for questionnaire.
     * @param activity_log_id The Survey Event ID.
     * @param checkRequiredAnswers Typically, required answers are only checked if the user is Submitting the questionnaire.
     */
	saveQuestionnaire: function(activity_log_id, checkRequiredAnswers) {
		this.questionnaire = View.controllers.get('CompleteQuestionnaire').questionnaire;
		if (this.questionnaire == null) return false;
		if (valueExistsNotEmpty(activity_log_id)) this.activity_log_id = activity_log_id;
		
		if (!this.validateAnswers(checkRequiredAnswers)) {
			View.alert(getMessage('invalidOrRequiredAnswer'));
			return false;
		}
		View.openProgressBar(getMessage('saving'));
		View.updateProgressBar(1/2);
		
		var controller = this;
		var questions = this.questionnaire.getAllQuestions();
		this.deleteAnswersToQuestionsNoLongerShown(this.activity_log_id, questions);
		
		_.each(questions, function(question, index) {
			var responses = question.getResponseValues();
			var answers = controller.getAnswers(question, activity_log_id);
			var answer = (answers.length > 0)?answers[0]:null;
			var count = 0;
			if (question.type === 'action' || question.type === 'document' || question.type === 'multiple_choice_single' || question.type === 'multiple_choice_multi') {				
				controller.saveAnswer(question, null, null, answer)
				count++;
			} else if (question.type === 'measurement') {
				for (var i=0; i<responses.length-1; i=i+2) {
					controller.saveAnswer(question, responses[i], responses[i+1], answer);
					count++;
					answer = (answers.length > count)?answers[count]:null;
				}
			} else {
				_.each(responses, function(response) {
					if (responses.length === 1 || valueExistsNotEmpty(response)) {
                        controller.saveAnswer(question, response, null, answer);
                        count++;
    					answer = (answers.length > count)?answers[count]:null;
					}
		        });
			}
			if (question.type != 'document') controller.deleteExtraAnswers(answers, count);
			View.updateProgressBar(0.5 + (0.5*index/questions.length));
        });
		View.closeProgressBar();
		return true;
	},
	
	validateAnswers: function(checkRequiredAnswers) {
		var controller = this;
		var validQuestionnaire = true;
		var questions = this.questionnaire.getAllQuestions();
		_.each(questions, function(question, index) {
			clearValidationResult(question);
			showTickmark(question, false);
			
			var validQuestion = true;
			if (question.type === 'multiple_choice_single' || question.type === 'multiple_choice_multi') {			
				if (!controller.validateAnswer(question, null, null)) validQuestion = false;				
			} else {
				_.each(question.getResponseValues(), function(response, index) {					
					if (!controller.validateAnswer(question, response, index)) validQuestion = false;
		        });				
			}
			if (validQuestion) {
				if (checkRequiredAnswers && !controller.checkRequiredAnswer(question)) validQuestion = false;
			}
			
			if (!validQuestion) validQuestionnaire = false;
			else showTickmark(question, true);
        });
		return validQuestionnaire;
	},
	
	checkRequiredAnswer: function(question) {
		var valid = true;
		if (parseInt(question.isRequired) == 1 && !question.isComplete()) {
			valid = false;
			addInvalidHtmlField(question, getMessage('required'));
		}
		return valid;
	},
	
	validateAnswer: function(question, response, index) {
		if (question.type === 'measurement' && (index%2 == 1)) return true;
		else if (question.type === 'multiple_choice_single' || question.type === 'multiple_choice_multi') {
			response = getMultipleChoiceAnswerOptions(question);
		}
		var isValid = true;
		
		switch(question.type) {
		    case 'date': case 'time':
		        if (!question.isValid()) {
		        	isValid = false;
		        	addInvalidHtmlField(question, getMessage('invalidValue'));
		        }		        
		        break;
		    case 'measurement': 
		        if (!question.isValid()) {
		        	isValid = false;
		        	addInvalidHtmlField(question, getMessage('invalidValue'));
		        } else {
		        	if (!checkMaxSize(response)) {
		        		isValid = false;
		        		addInvalidHtmlField(question, getMessage('maxSize'));
		        	}		        	
		        }	        
		        break;
		    default:
		        break;
		}
		return isValid;
	},
	
	deleteExtraAnswers: function(answers, count) {
		var ds = View.dataSources.get('answersDataSource');
		for (var i = count; i < answers.length; i++) {
			ds.deleteRecord(answers[i]);
		}
	},
	
	/**
     * Since the child questions may change when the user edits a parent question, 
     * we must delete any previously-saved answers belonging to a child question which is no longer displayed.  
     * @param activity_log_id.  Survey Event Id.
     * @param questions.  Questions array.
     */
	deleteAnswersToQuestionsNoLongerShown: function(activity_log_id, questions) {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('quest_answer_ext.survey_event_id', activity_log_id);
		var ds = View.dataSources.get('answersDataSource');
		var records = ds.getRecords(restriction);
		for (var i = 0; i < records.length; i++) {
			var question_id = records[i].getValue('quest_answer_ext.question_id');
			inner: for (var k = 0; k < questions.length; k++) {
				if (questions[k].id == question_id) break inner;
			}
			if (k >= questions.length) {
				ds.deleteRecord(records[i]);
			}
        }
	},
	
	saveAnswer: function(question, response, nextResponse, answer) {
		var questController = View.controllers.get('CompleteQuestionnaire');
	    var ds = View.dataSources.get('answersDataSource');
	    var currentDateObj = new Date();
	    var value_text = '';
	    var value_date = null;
	    var value_time = null;
	    var value_number = '';
	    var value_unit = '';
	    var value_integer = '';
	    var fileName = '';
	    
	    switch(question.type) {
		    case 'date':
		        value_date = response;
		        break;
		    case 'time':
		    	value_time = response;
		    	break;
		    case 'count':
		    	value_integer = response;
		    	break;
		    case 'measurement':
		    	value_number = response;
		    	value_unit = nextResponse;
		    	break;
		    case 'multiple_choice_single': case 'multiple_choice_multi':
		    	value_text = getMultipleChoiceAnswerOptions(question);
		    	break;
		    case 'document':
		    	fileName = getFileName(question);
		    	if (fileName == '') {
		    		// do not save the answer if the user has not provided a document
		    		return false;
                }
		    	break;
		    default:
		    	value_text = response;
		        break;
		}
	    
	    var questionRecord = View.dataSources.get('questionsDataSource').getRecord('question_ext.question_id = ' + question.id);
		var questionnaire_id = questController.questionnaire.id;
		var answerId = 0;

		if (valueExistsNotEmpty(answer)) {
			answerId = answer.getValue('quest_answer_ext.answer_id');
			answer.isNew = false;
			answer.setValue('quest_answer_ext.questionnaire_id', questionnaire_id);
			answer.setValue('quest_answer_ext.value_text', value_text);
			answer.setValue('quest_answer_ext.value_date', ds.formatValue('quest_answer_ext.value_date', value_date, false));
			answer.setValue('quest_answer_ext.value_time', ds.formatValue('quest_answer_ext.value_time', value_time, false));
			answer.setValue('quest_answer_ext.value_number', value_number);
			answer.setValue('quest_answer_ext.value_integer', value_integer);
			answer.setValue('quest_answer_ext.unit_type', question.unitType);
			answer.setValue('quest_answer_ext.unit', value_unit);
			answer.setValue('quest_answer_ext.doc', fileName);
			answer.setValue('quest_answer_ext.date_recorded', ds.formatValue('quest_answer_ext.date_recorded', currentDateObj, false));
			answer.setValue('quest_answer_ext.time_recorded', ds.formatValue('quest_answer_ext.time_recorded', currentDateObj, false));
		} else {
			answer = new Ab.data.Record({
				'quest_answer_ext.questionnaire_id': questionnaire_id,
				'quest_answer_ext.survey_event_id': this.activity_log_id,
				'quest_answer_ext.question_id': question.id,
				'quest_answer_ext.value_text': value_text,
				'quest_answer_ext.value_date': ds.formatValue('quest_answer_ext.value_date', value_date, false),
				'quest_answer_ext.value_time': ds.formatValue('quest_answer_ext.value_time', value_time, false),
				'quest_answer_ext.value_number': value_number,
				'quest_answer_ext.value_integer': value_integer,
				'quest_answer_ext.unit_type': question.unitType,
				'quest_answer_ext.unit': value_unit,
				'quest_answer_ext.doc': fileName,
				'quest_answer_ext.date_recorded': ds.formatValue('quest_answer_ext.date_recorded', currentDateObj, false),
				'quest_answer_ext.time_recorded': ds.formatValue('quest_answer_ext.time_recorded', currentDateObj, false)
			}, true);
		}	
		var record = ds.saveRecord(answer); // returns null record for existing answer
		if (record) answerId = record.getValue('quest_answer_ext.answer_id');
		if (question.type === 'document') {
			questController.checkInQuestionDocument(question, answerId);
			question.isUploaded = true;
		}
		return true;
	},
	
	/**
     * Loads and displays read-only questionnaire with answers.
     * @param questionnaireId The questionnaire ID.
     * @param disabled Boolean.  If true, all fields in Questionnaire are disabled in read-only mode.
     * @param surveyEventId activity_log_id
     */
	loadQuestionnaireWithResults: function(questionnaire_id, disabled, surveyEventId) {
	    var questController = View.controllers.get('CompleteQuestionnaire');
	    var controller = this;

	    questController.loadQuestionnaire(questionnaire_id, disabled, surveyEventId, function(question) {
	    	controller.setQuestionResponses(question, surveyEventId);        
	    });
	    questController.questionnaireForm.updateHeight();
	},
	
	setQuestionResponses: function(question, surveyEventId) {		
		var questController = View.controllers.get('CompleteQuestionnaire');
		var controller = this;
		
		if (question.type === 'multiple_choice_single' || question.type === 'multiple_choice_multi') {
			questController.loadChildQuestions(question, surveyEventId, function(childQuestion) {
				controller.setQuestionResponses(childQuestion, surveyEventId);
	        });
        } else {
        	var responses = controller.addQuestionResponses(question, surveyEventId);
        	if (responses.length > 0) question.setResponseValues(responses);
        }	
	},
	
	addQuestionResponses: function (question, surveyEventId) {
		var questController = View.controllers.get('CompleteQuestionnaire');
	    var answers = this.getAnswers(question, surveyEventId);
	    
		var responses = [];
		_.each(answers, function(answer, index) {
	       	if (answer.getValue('quest_answer_ext.question_id') === question.id) {        		
	       		switch(question.type) {
	        		case 'date':
	        			responses.push(answer.getValue('quest_answer_ext.value_date'));
	        			break;
	        		case 'time':
	        			responses.push(answer.getValue('quest_answer_ext.value_time'));
	        			break;
	        		case 'count':
	        			responses.push(answer.getValue('quest_answer_ext.value_integer'));
	        			break;
	        		case 'measurement':
	        			responses.push(answer.getValue('quest_answer_ext.value_number'));
	        			responses.push(answer.getValue('quest_answer_ext.unit'));
	        			break;
	        		case 'document':
	        			if (answer.getValue('quest_answer_ext.doc') != '') {
	        				question.isUploaded = true;
                            responses.push(answer.getValue('quest_answer_ext.answer_id'));
	        				responses.push(answer.getValue('quest_answer_ext.doc'));
                        }
	        			break;
	        		default:
	        			responses.push(answer.getValue('quest_answer_ext.value_text'));
	        			break;
	        	}
	       		if (index > 0) {
	       			questController.addQuestionResponse(question);
	       		}
	       	}
		});
	    return responses;
	},
	
	getAnswers: function (question, surveyEventId) {
		var restriction = new Ab.view.Restriction();
	    restriction.addClause('quest_answer_ext.survey_event_id', surveyEventId);
	    restriction.addClause('quest_answer_ext.question_id', question.id);
	    var answers = View.dataSources.get('answersDataSource').getRecords(restriction);
	    return answers;
	},
	
	/**
     * Opens the Building Ops Service Request dialog.
     * @param questionnaireId The questionnaire ID.
     * @param activity_log_id The Survey Event ID.
     * @param selectedAnswerOptionText.  The user-selected Answer Option that triggered a Service Request.
     */
	generateServiceRequest: function (questionnaire_id, activity_log_id, selectedAnswerOptionText) {
		var message = String.format(getMessage('confirmServiceRequest'), selectedAnswerOptionText);
		View.confirm(message, function(button){
	        if (button == 'yes') {
	        	View.openDialog('ab-bldgops-console-wr-create.axvw');
	        }
	    });	
	},
	
	processQuestionnaire: function (questionnaire_id, survey_event_id, callback) {
		try {
			var jobId = Workflow.startJob('AbCommonResources-QuestionnaireExtService-processQuestionnaireAnswers', parseInt(questionnaire_id), parseInt(survey_event_id));
			View.openJobProgressBar(getMessage('processingAnswers'), jobId, '', function(status) {
				if (status.jobFinished) {
                    if (valueExists(callback)) {
                        callback();
					}
				}
			});
		} catch (e) {
		    Workflow.handleError(e);
		}	
	}
});

function getMultipleChoiceAnswerOptions(question) {
	var controller = View.controllers.get('UpdateQuestionnaire');
	var ids = '';
	for (var i=0;i<question.selectedAnswerOptions.length;i++) {
		ids += (ids == '')?'':controller.multipleValueSeparator;
    	var option = question.selectedAnswerOptions[i];
    	ids += option.id;
    }
	return ids;
}

function addInvalidHtmlField(question, message) {
	var questTextTd = $('question_' + question.id);
	Ext.fly(questTextTd).addClass('questErrorInput');
    var errorTextElement = document.createElement('span');
    errorTextElement.className = 'questErrorText';
    if (question.type === 'multiple_choice_multi' || question.type === 'multiple_choice_single') {
    	errorTextElement.appendChild(document.createElement('br'));
    	errorTextElement.appendChild(document.createElement('br'));
    }
    errorTextElement.appendChild(document.createTextNode(message));    
    questTextTd.appendChild(errorTextElement);
}

function clearValidationResult(question) {
	var questTextTd = $('question_' + question.id);
	Ext.fly(questTextTd).removeClass('questErrorInput');
    var errorTextElements = Ext.query('.questErrorText', questTextTd);
    for (var e = 0; e < errorTextElements.length; e++) {
        questTextTd.removeChild(errorTextElements[e]);
    }
}

function checkMaxSize(value) {
	/*value = value + '';
	var pattern = strGroupingSeparator;
    var re = new RegExp(pattern, "g");
	value = value.replace(re, '');
	var size = value.length;
	var valid = true;
	if (size > 15) valid = false;*/
	var valid = true;
	if (value > 9999999999 || value < -9999999999) valid = false;
	return valid;
}

function showTickmark(question, show) {
	var tick = null;
	if (question.isChild) tick = $('question_' + question.id).getElementsByClassName('questionnaireGreenCheck')[0];
	else tick = $('question_prefix_' + question.id).getElementsByClassName('questionnaireGreenCheck')[0];
	if (show && question.isComplete()) tick.style.display = 'inline-block';
	else tick.style.display = 'none';
}

function getFileName(question) {
	var fullFileName = question.documentControl.getSelectedFilename();
	var temp = fullFileName.split("\\");
	if (fullFileName.indexOf("\\") < 0) temp = fullFileName.split("\/");
    return temp[temp.length - 1];
}
