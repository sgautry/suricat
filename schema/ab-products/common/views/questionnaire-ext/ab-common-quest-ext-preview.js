
View.createController('PreviewQuestionnaire', {

	afterInitialDataFetch: function() {
        var questionnaireId = 0;
        if (View.parameters && View.parameters.questionnaireId) questionnaireId = View.parameters.questionnaireId;
        var completeQuestionnaireController = View.controllers.get('CompleteQuestionnaire');
        completeQuestionnaireController.loadQuestionnaire(questionnaireId, false, null, null);
    }
});
