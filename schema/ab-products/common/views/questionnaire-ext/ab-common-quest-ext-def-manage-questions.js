/**
 * @author Eric_Maxfield@archibus.com
 * @Bali6 Extended Questionnaires
 */
var abCommonQuestExtDefManageQuestionsController = View.createController('abCommonQuestExtDefManageQuestionsController', {
	
	/**
	 * This event handler establishes form-wide behaviors after data is available
	 */	
	afterInitialDataFetch: function(){
        var titleObj1 = Ext.get('addNewViaGrid');
        var titleObj2 = Ext.get('addNewViaTree');
        
        titleObj1.on('click', this.showMenu, this, null);        
        titleObj2.on('click', this.showMenu, this, null);
    },
    
    /**
     *  This method displays a drop-down set of menu options for the "Add New" action button
     */
    showMenu: function(e, item){
        var menuItems = [];
        var menutitle_newMultipleChoiceSingle = getMessage("multipleChoiceSingle");
        var menutitle_newMultipleChoiceMulti = getMessage("multipleChoiceMulti");
        var menutitle_newFreeform = getMessage("freeform");
        var menutitle_newCount = getMessage("count");
        var menutitle_newMeasurement = getMessage("measurement");
        var menutitle_newDate = getMessage("date");
        var menutitle_newTime = getMessage("time");
        var menutitle_newLookup = getMessage("lookup");
        var menutitle_newDocument = getMessage("document");        
        var menutitle_newAction = getMessage("action");
        var menutitle_newLabel = getMessage("label");        
        
        menuItems.push({
            text: menutitle_newMultipleChoiceSingle,
            handler: this.onAddNewButtonPush.createDelegate(this, ['MULTI-SINGLE'])
        });
        menuItems.push({
            text: menutitle_newMultipleChoiceMulti,
            handler: this.onAddNewButtonPush.createDelegate(this, ['MULTI-MULTI'])
        });
        menuItems.push({
            text: menutitle_newFreeform,
            handler: this.onAddNewButtonPush.createDelegate(this, ['FREEFORM'])
        });
        menuItems.push({
            text: menutitle_newCount,
            handler: this.onAddNewButtonPush.createDelegate(this, ['COUNT'])
        });
        menuItems.push({
            text: menutitle_newMeasurement,
            handler: this.onAddNewButtonPush.createDelegate(this, ['MEASUREMENT'])
        });
        menuItems.push({
            text: menutitle_newDate,
            handler: this.onAddNewButtonPush.createDelegate(this, ['DATE'])
        });
        menuItems.push({
            text: menutitle_newTime,
            handler: this.onAddNewButtonPush.createDelegate(this, ['TIME'])
        });
        menuItems.push({
            text: menutitle_newLookup,
            handler: this.onAddNewButtonPush.createDelegate(this, ['LOOKUP'])
        });
        menuItems.push({
            text: menutitle_newDocument,
            handler: this.onAddNewButtonPush.createDelegate(this, ['DOCUMENT'])
        });
        menuItems.push({
            text: menutitle_newAction,
            handler: this.onAddNewButtonPush.createDelegate(this, ['ACTION'])
        });
        menuItems.push({
            text: menutitle_newLabel,
            handler: this.onAddNewButtonPush.createDelegate(this, ['LABEL'])
        });
        
        var menu = new Ext.menu.Menu({
            items: menuItems
        });
        menu.showAt(e.getXY());
        
    },
    
    /**
     * This event handler processes user selection from the "Add New" action options
     */
    onAddNewButtonPush: function(menuItemId){
    	var form = this.questionsForm;
    	form.clear();
    	var questionTypeField = 'question_ext.question_type'; 
        switch (menuItemId) {
            case "MULTI-SINGLE":
                form.setFieldValue(questionTypeField,'multiple_choice_single');
                break;
                
            case "MULTI-MULTI":
                form.setFieldValue(questionTypeField,'multiple_choice_multi');
                break;
                
            case "FREEFORM":
            	form.setFieldValue(questionTypeField,'freeform');
            	break;
            	
            case "COUNT":
            	form.setFieldValue(questionTypeField,'count');
            	break;
            	
            case "MEASUREMENT":
            	form.setFieldValue(questionTypeField,'measurement');
            	break;
            	
            case "DATE":
            	form.setFieldValue(questionTypeField,'date');
            	break;
            	
            case "TIME":
            	form.setFieldValue(questionTypeField,'time');
            	break;
            	
            case "LOOKUP":
            	form.setFieldValue(questionTypeField,'lookup');
            	break;
            	
            case "DOCUMENT":
            	form.setFieldValue(questionTypeField,'document');
            	break;
            	
            case "ACTION":
            	form.setFieldValue(questionTypeField,'action');
            	break;
            	
            case "LABEL":
            	form.setFieldValue(questionTypeField,'label');
            	break;
            	
        }
        var parentNode = this.getParentNode(this.questionsHierarchyTreePanel, this.questionsForm, true);
        var isChildQuestion = (parentNode.depth > 0);        
        this.setFormFieldsToDisplay(form.getFieldValue(questionTypeField),isChildQuestion);
        this.questionsForm.show(true);
    },
	
    setFormFieldsToDisplay : function(questionType,isChildQuestion) {
    	var form = this.questionsForm;
    	
    	// Hide all variable questions by default, then enable as required by specific cases
    	form.showField('question_ext.freeform_width',false);
    	form.showField('question_ext.lookup_table',false);
    	form.showField('question_ext.lookup_field',false);
    	form.showField('question_ext.unit_type',false);
    	form.showField('question_ext.unit_default',false);
    	form.showField('question_ext.activity_type',false);    	
    	
    	switch (questionType) {
	        case ("multiple_choice_single","multiple_choice_multi","count","date","time","document","label"):	        		        	
	            break;	        
	            
	        case "freeform":
	        	form.showField('question_ext.freeform_width',true);	        	
	        	break;
	        		        	
	        case "measurement":
	        	form.showField('question_ext.unit_type',true);
	        	form.showField('question_ext.unit_default',true);	        	
	        	break;
	        	
	        case "lookup":	        	
	        	form.showField('question_ext.lookup_table',true);
	        	form.showField('question_ext.lookup_field',true);	        	
	        	break;

	        case "action":	        	
	        	form.showField('question_ext.activity_type',true);	        		        	
	        	break;
    	}


   		form.showField('question_ext.parent_question_text',isChildQuestion);
   		form.showField('question_ext.parent_answer_condition',isChildQuestion);    		    	
    	
    },
    
	/**
	 * This event handler updates calculated form field values for current user name and date before saving a record.
	 * abCommonQuestExtDefManageQuestionsController_beforeSave
	 */
	questionsForm_beforeSave : function() {
		var form = this.questionsForm;		
		var ds = this.questionsDataSource;
		
		//Set Updated By User Name (user_name) value to current user's ID
		form.setFieldValue('question_ext.edited_by', this.view.user.name);
		
		//Set Date and Time Last Updated to current date and time 		
		var currentDateObj = new Date();
		var currentDate = ds.formatValue('question_ext.date_last_edit', currentDateObj, false);
		var currentTime = ds.formatValue('question_ext.time_last_edit', currentDateObj, false);
		form.setFieldValue('question_ext.date_last_edit', currentDate);
		form.setFieldValue('question_ext.time_last_edit', currentTime);

	},
	
	/**
	 * This event handler adjusts visibility of some fields according to hierarchical relationship
	 */
	questionsForm_beforeRefresh : function() {
		var parentNode = this.getParentNode(this.questionsHierarchyTreePanel, this.questionsForm, true);
		var isChildQuestion = (parentNode.depth > 0);  
        this.setFormFieldsToDisplay(this.questionsForm.getFieldValue('question_ext.question_type'),isChildQuestion);
	},
	
	
// Methods below this line are based on those written for ab-ac-edit.js
	
	
	/**
	 * get the parent node depending on the action done on the tree nodes
	 * 
	 */
	getParentNode: function(treePanel, detailsPanel, isNewRecord){
		var currentNode = treePanel.lastNodeClicked;
		if(isNewRecord){
			if(currentNode!=null){
				return treePanel.lastNodeClicked;
			}else{
				return treePanel.treeView.getRoot();
			}
			
		}else{
			var oldFieldValue = detailsPanel.getOldFieldValues()["question_ext.question_id"];
			if(currentNode != null){
				for(var i=0;i<currentNode.children.length;i++){
					var tmpNode = currentNode.children[i];
					if(tmpNode.data["question_ext.question_id"] == oldFieldValue){
						currentNode = tmpNode;
						break;
					}
				}
				return currentNode.parent;
			}else{
				return treePanel.treeView.getRoot();
			}
		}
		
    },
	
	/**
	 *  refresh tree based on parent node, expand the child 
	 *  in case the action is done on it
	 */
	refreshTree: function(parentNode){
		var treePanel = this.questionsHierarchyTreePanel;
		if(parentNode.isRoot()){
			treePanel.refresh();
		}else{
			treePanel.expandNode(parentNode);
		}
		
	},
	
	/**
	 * refresh tree panel and set the details panel invisible
	 */
	
	questionsHierarchyTreePanel_onRefresh: function(){
        this.questionsHierarchyTreePanel.refresh();
        this.questionsForm.show(false);
	},
	
    /**
     * action after adding a record using Add New button, 
     * the method checks if parent node is root, parent or a child:
     * hierarchy_ids will be updated accordingly
     */
	
	addNewRecord: function(parentNode, newRecordId){
		if(parentNode.isRoot()){
			this.questionsForm.setFieldValue("question_ext.hierarchy_ids", newRecordId + '|');
		}else{
			var hierarchyIds = parentNode.data["question_ext.hierarchy_ids"];
			this.questionsForm.setFieldValue("question_ext.hierarchy_ids", hierarchyIds + newRecordId + '|');
		}
		this.questionsForm.save();
	},
	
	/**
	 * called on Save action, when saving a record after modification
	 * updates all children nodes
	 * 
	 */	
	updateChildNodes: function(newRecordId, oldRecordId){
		var dataSource = this.questionsDataSource;
	    var restriction = "question_ext.hierarchy_ids LIKE '%" + oldRecordId + "|%'";
		var dataRecords = dataSource.getRecords(restriction);
		for(var i= 0; i < dataRecords.length; i++ ) {
			var record = dataRecords[i];
			var hierarchyId = record.getValue("question_ext.hierarchy_ids");
			record.setValue("question_ext.hierarchy_ids", hierarchyId.replace("|" + oldRecordId + "|", "|" + newRecordId + "|"));
			record.isNew = false;
			dataSource.saveRecord(record);
		}
	},
	
	/**
	 * method called on Delete action
	 * deletes all records and its children 
	 */	
	deleteNodes: function(detailsPanel){
		var dataSource = this.questionsDataSource;
		var recordForDel = detailsPanel.getFieldValue("question_ext.question_id");
	    var restriction = "question_ext.hierarchy_ids LIKE '%" + recordForDel + "|%'";
		var dataRecords = dataSource.getRecords(restriction);
		for(var i= 0; i < dataRecords.length; i++ ) {
			var record = dataRecords[i];
			dataSource.deleteRecord(record);
		}
	},
	
	/**
	 * Action when pressing Filter button
	 * added custom restrictions for database  
	 * Each filter option has also multiple selection options
	 * Check hierarchyIds of records for both parent and children
	 */	
	questionsFilter_onFilter: function(){
		var sqlRestriction = "";
		
		var acId = [];
		var filterConsole = this.questionsFilter;
		var treePanel = this.questionsHierarchyTreePanel;
		
		if(filterConsole.hasFieldMultipleValues('question_ext.question_id')){
			acId = filterConsole.getFieldMultipleValues('question_ext.question_id');
			
			for(var i=0;i<acId.length;i++){
				if(valueExistsNotEmpty(filterConsole.getFieldValue('question_ext.question_id'))){
					sqlRestriction +=  (valueExistsNotEmpty(sqlRestriction) ? (i==0 ? " AND " : " OR ") : "") + " question_ext.question_id LIKE '%" + acId[i]+"%' ";
					
				}
			}
			sqlRestriction = "(" + sqlRestriction +  ")";
		}else if(valueExistsNotEmpty(filterConsole.getFieldValue('question_ext.question_id'))){
			acId = filterConsole.getFieldValue('question_ext.question_id');
			sqlRestriction += (valueExistsNotEmpty(sqlRestriction) ? " AND " : "") + " question_ext.question_id LIKE '%"+ acId +"%' ";
		}
		
		var coaSourceId = [];
		
		if(filterConsole.hasFieldMultipleValues('ac.coa_source_id')){
			coaSourceId = filterConsole.getFieldMultipleValues('ac.coa_source_id');
			sqlRestriction += (valueExistsNotEmpty(sqlRestriction) ? " AND " : "") + " ac.coa_source_id IN ('" + coaSourceId.join("','")+"')";
		}else if(valueExistsNotEmpty(filterConsole.getFieldValue('ac.coa_source_id'))){
			coaSourceId = filterConsole.getFieldValue('ac.coa_source_id');
			sqlRestriction += (valueExistsNotEmpty(sqlRestriction) ? " AND " : "") + "ac.coa_source_Id LIKE '"+ coaSourceId +"%'";
		}
		
		var coaCostGroupId = [];
		
		if(this.questionsFilter.hasFieldMultipleValues('ac.coa_cost_group_id')){
			coaCostGroupId = filterConsole.getFieldMultipleValues('ac.coa_cost_group_id');
			sqlRestriction += (valueExistsNotEmpty(sqlRestriction) ? " AND " : "") + " ac.coa_cost_group_id IN ('" + coaCostGroupId.join("','")+"')";
		}else if(valueExistsNotEmpty(filterConsole.getFieldValue('ac.coa_cost_group_id'))){
			coaCostGroupId = filterConsole.getFieldValue('ac.coa_cost_group_id');
			sqlRestriction += (valueExistsNotEmpty(sqlRestriction) ? " AND " : "") + "ac.coa_cost_group_id LIKE '"+ coaCostGroupId +"%'";
		}
		
		var description = null;
		
		if(valueExistsNotEmpty(filterConsole.getFieldValue('ac.description'))){
			description = filterConsole.getFieldValue('ac.description');
			sqlRestriction += (valueExistsNotEmpty(sqlRestriction) ? " AND " : "") + "ac.description LIKE '%"+ description +"%'";
		}
		
		sqlRestriction += (valueExistsNotEmpty(sqlRestriction) ? " OR " : "") +" EXISTS (SELECT * FROM ac ${sql.as} ac_int where ac_int.hierarchy_ids LIKE '' ${sql.concat} LTRIM(RTRIM(question_ext.question_id)) ${sql.concat} '|%'"
			+ (valueExistsNotEmpty(sqlRestriction) ? " AND ( " : "")  + sqlRestriction.replace(/ac\./g, 'ac_int.') 
			+ (valueExistsNotEmpty(sqlRestriction) ? " )) " : ")")+
			" OR EXISTS (SELECT * FROM ac ${sql.as} ac_int where ac_int.hierarchy_ids LIKE '%' ${sql.concat} LTRIM(RTRIM(question_ext.question_id)) ${sql.concat} '|%'"
			+(valueExistsNotEmpty(sqlRestriction) ? " AND ( " : "") 
			+sqlRestriction.replace(/ac\./g, 'ac_int.') 
			+ (valueExistsNotEmpty(sqlRestriction) ? " )) " : ")");
		
		
		treePanel.addParameter('sqlRestriction', sqlRestriction);
		treePanel.refresh();
		
		this.questionsForm.show(false, true);
	
	},
	
	
	/**
	 * action for Save button, treated differently for update of a record
	 * or adding new record
	 */
	questionsForm_onSave: function(){
		var isNewRecord = this.questionsForm.newRecord;
		var oldRecordId = this.questionsForm.getOldFieldValues()["question_ext.question_id"];
		var newRecordId = this.questionsForm.getFieldValue("question_ext.question_id");
		var lastNode = this.questionsHierarchyTreePanel.lastNodeClicked;
		var parentNode = this.getParentNode(this.questionsHierarchyTreePanel, this.questionsForm, isNewRecord);
		
		if (this.questionsForm.save()) {
			if (!isNewRecord) {
				this.updateChildNodes(newRecordId, oldRecordId);
			}else{
				this.addNewRecord(parentNode, newRecordId);
			}
			this.refreshTree(parentNode);
//			this.questionsForm.show(false, true);
		}
	},
	
	
	/**
	 * action for Delete button, the message will be generic,
	 * the children of the selected record for deletion will also be deleted
	 */
	 questionsForm_onDelete: function() {
		var controller = this; 
		var dataSource = this.questionsDataSource;
		var treePanel = this.questionsHierarchyTreePanel;
		var detailsPanel = this.questionsForm;
		var record = this.questionsForm.getRecord();
		var questionIdFieldValue = record.getValue("question_ext.question_id");
		var node = treePanel.lastNodeClicked;
		var parentNode = this.getParentNode(treePanel, detailsPanel, false);
		
        if (!questionIdFieldValue) {
            return;
        }
		
        var deleteMessage="";
        var confirmMessage = getMessage("messageConfirmDelete").replace('{0}', questionIdFieldValue);
        
        View.confirm(confirmMessage, function(button){
                 if (button == 'yes') {
                     try {
                    	    controller.deleteNodes(detailsPanel);
                         	if(treePanel.lastNodeClicked != null && questionIdFieldValue === treePanel.lastNodeClicked.data["question_ext.question_id"]){
                         		treePanel.lastNodeClicked = null;
                         	}
                         	detailsPanel.show(false);
                         	controller.refreshTree(parentNode);
                    	    
                     } 
                     catch (e) {
                         var errMessage = getMessage("errorDelete").replace('{0}', questionIdFieldValue);
                         View.showMessage('error', errMessage, e.message, e.data);
                         return;
                     }
                 }
             })
	}
	
	

});