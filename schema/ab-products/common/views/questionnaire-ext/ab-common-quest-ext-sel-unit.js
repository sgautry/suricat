View.createController('abCommonQuestExtQuestionnaireSelUnitController', {
    unitTypePrefix: null,
    activityId: null,
    unitType: 'bill_type', //bill_type, bill_unit
    unitTypeValue: null,
    callbackMethod: null,
    afterViewLoad: function () {
        if (valueExists(View.parameters)) {
            if (valueExists(View.parameters.unitTypePrefix)) {
                this.unitTypePrefix = View.parameters.unitTypePrefix;
            }
            if (valueExists(View.parameters.activityId)) {
                this.activityId = View.parameters.activityId;
            }
            if (valueExists(View.parameters.unitType)) {
                this.unitType = View.parameters.unitType;
            }
            if (valueExists(View.parameters.unitTypeValue)) {
                this.unitTypeValue = View.parameters.unitTypeValue;
            }
            if (valueExists(View.parameters.callback)) {
                this.callbackMethod = View.parameters.callback;
            }
        }
    },
    afterInitialDataFetch: function () {
        if ('bill_type' === this.unitType) {
            this.showBillType();
        } else if ('bill_unit' === this.unitType) {
            this.showBillUnit();
        }
    },
    showBillType: function () {
        var unitTypePanel = View.panels.get('abCommonQuestExtSelUnitTypeGrid');
        var restriction = new Ab.view.Restriction();
        if (this.unitTypePrefix) {
            restriction.addClause('bill_type.bill_type_id', this.unitTypePrefix + '%', 'LIKE');
            unitTypePanel.addParameter('bill_type_id_display', "RTRIM(${sql.substr('bill_type.bill_type_id', " + this.unitTypePrefix.length + ", 24)})");
        }
        if (this.activityId) {
            restriction.addClause('bill_type.activity_id',this.activityId, '=');
        }
        unitTypePanel.refresh(restriction);
    },
    showBillUnit: function () {
        var unitPanel = View.panels.get('abCommonQuestExtSelUnitGrid');
        var restriction = new Ab.view.Restriction();
        if (this.unitTypePrefix) {
            restriction.addClause('bill_unit.bill_type_id', this.unitTypePrefix + '%', 'LIKE');
            unitPanel.addParameter('bill_type_id_display', "RTRIM(${sql.substr('bill_type.bill_type_id', " + this.unitTypePrefix.length + ", 24)})");
        }
        if (this.activityId) {
            unitPanel.addParameter('activityIdRestriction', "bill_type.activity_id='" + this.activityId + "'");
        }
        unitPanel.refresh(restriction);
        if (valueExists(this.unitTypeValue)) {
            unitPanel.setFilterValue('bill_unit.bill_type_id_display', this.unitTypeValue);
            unitPanel.refresh(restriction);
        }
    },
    onSelectUnit: function (context) {
        if (valueExists(this.callbackMethod)) {
            var parentPanel = View.panels.get(context.command.parentPanelId);
            if (parentPanel) {
                var selectedRow = parentPanel.gridRows.get(parentPanel.selectedRowIndex),
                    record = selectedRow.getRecord();
                this.callbackMethod(record);
            }
        }
    }
});

function selectUnit(context) {
    View.controllers.get('abCommonQuestExtQuestionnaireSelUnitController').onSelectUnit(context);
}