var commonQuestExtViewQuestionnaireController = View.createController('commonQuestExtViewQuestionnaire', {
	selected_questionnaire_id: null,
	selected_questionnaire_title: null,
	multipleValueSeparator: Ab.form.Form.MULTIPLE_VALUES_SEPARATOR,
	
	afterViewLoad: function() {	
		var controller = this;	
		
		var grid = this.questionnaireList;
		grid.afterCreateCellContent = function(row, column, cellElement) {
			if (column.id == 'questionnaire_ext.questionnaire_title')	{
				cellElement.style.fontWeight = 'bold';
			}
		};
	},

	selectQuestionnaire: function(obj) {
		var questionnaire_id = obj.restriction['questionnaire_ext.questionnaire_id'];
		this.selected_questionnaire_id = questionnaire_id;
		var restriction = new Ab.view.Restriction();
		restriction.addClause('questionnaire_ext.questionnaire_id', questionnaire_id);
		var record = View.dataSources.get('questionnairesDataSource').getRecord(restriction);
		this.selected_questionnaire_title = record.getValue('questionnaire_ext.questionnaire_title');
		
		var questionnaireView = View.panels.get('questionnaireView').contentView;
		if (questionnaireView) {
			loadQuestionnairePanel();
		} else {
			setTimeout(loadQuestionnairePanel, 5000);
		}
		this.questionnaireView.refresh();
		this.questionnaireView.show();

		this.questionsTree.addParameter('questionnaireId', questionnaire_id);
		this.questionsTree.addParameter('multipleValueSeparator', this.multipleValueSeparator);
		this.questionsTree.refresh();
		this.questionsTree.appendTitle(this.selected_questionnaire_title);
		this.questionsTree.show();
		onTreeExpandAll(this.questionsTree, null);
	}
});

function loadQuestionnairePanel() {
	var controller = View.controllers.get('commonQuestExtViewQuestionnaire');
	var questionnaireView = View.panels.get('questionnaireView').contentView;
	if (questionnaireView) {
		var completeQuestionnaireController = questionnaireView.controllers.get('CompleteQuestionnaire');
		completeQuestionnaireController.loadQuestionnaire(controller.selected_questionnaire_id, false, null, null);
	}
}

function questionsTree_onSelectQuestion() {
	var controller = View.controllers.get('commonQuestExtViewQuestionnaire');
	var tree = View.panels.get('questionsTree');
	var lastNodeClicked = tree.lastNodeClicked;
	var question_id = lastNodeClicked.data['question_ext.question_id'];
	
	var restriction = new Ab.view.Restriction();
	restriction.addClause('question_ext.question_id', question_id);
	var record = View.dataSources.get('questionsDataSource').getRecord(restriction);
	var question_type = record.getValue('question_ext.question_type');
	
	var parent_question_id = question_id;
	var hierarchy_ids = record.getValue('question_ext.hierarchy_ids');
	var isChildQuestion = getIsChildQuestion(hierarchy_ids);
	if (isChildQuestion) parent_question_id = hierarchy_ids.substring(0, hierarchy_ids.indexOf('|'));
	
	View.openDialog('ab-common-quest-ext-def-questionnaire-dtl.axvw', null, false, {
		'width':800,
		'question_id': parent_question_id,
		'isChildQuestion': isChildQuestion,
		'child_question_id':question_id,
		'questionnaire_id':controller.selected_questionnaire_id,
		'question_type':question_type
	});
}