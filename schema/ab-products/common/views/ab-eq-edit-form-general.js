var abEquipmentForm_tabGeneralController = View.createController('abEquipmentForm_tabGeneralController', {
    // selected equipment code
    eqId: null,

    // if is new record
    newRecord: false,

    // callback method
    callbackMethod: null,

    isSelectedFromReq: false,

    // id of the eq req record
    eqReqAutoNumber: null,

    selectedReqRecord: null,

    // parameters that come from eam receipt
    eamReceiptParameters: null,

    // custom action command
    customActionCommand: null,
    
    // check backward compatible to 23.1 schema
    isSchemaCompatible: false,
    afterViewLoad: function () {
    	this.isSchemaCompatible = valueExists(View.dataSources.get('ds_abEqAssetAttributes'));
        // hide field tooltip if SMS licence is not available
        if (!checkLicense('AbEAMSMSBuilderExtension')) {
            jQuery(this.abEqEditForm_General.getFieldCell('eq.builder_sms_level')).find('.fieldTooltipButton').hide();
        }
        if (valueExists(View.getOpenerView())
            && valueExists(View.getOpenerView().parameters)) {

            if (valueExists(View.getOpenerView().parameters.callback)) {
                this.callbackMethod = View.getOpenerView().parameters.callback;
            }

            if (valueExists(View.getOpenerView().parameters.eamReceiptParameters)) {
                this.eamReceiptParameters = View.getOpenerView().parameters.eamReceiptParameters;
                this.customActionCommand = View.getOpenerView().parameters.eamReceiptParameters.onClickActionEventHandler;
                // used for custom command
                this.view.parameters = View.getOpenerView().parameters;
            }
        }
    },

    afterInitialDataFetch: function () {
        this.newRecord = this.view.newRecord;
        if (valueExists(this.view.restriction)) {
            var restriction = this.view.restriction;
            var clause = restriction.findClause('eq.eq_id');
            if (clause) {
                this.eqId = clause.value;
            }
        }

        if (valueExists(this.customActionCommand)) {
            this.abEqEditForm_General.actions.get('customActionCommand').setTitle(this.eamReceiptParameters.actionTitle);
            if ("single" === this.eamReceiptParameters.createMode) {
                this.eamReceiptParameters.applyNextId = true;
                this.abEqEditForm_General.actions.get('customActionCommand').show(true);
                this.abEqEditForm_General.actions.get('clear').show(true);
                this.abEqEditForm_General.actions.get('cancel').show(false);
            } else {
                var selectedRecords = this.eamReceiptParameters.selectedRecords;
                this.abEqEditForm_General.actions.get('customActionCommand').show(selectedRecords.length > 1);
            }
            this.abEqEditForm_General.actions.get('delete').show(false);
        }
    },

    /**
     * Check if form values were changed.
     */
    isFieldValueChanged: function () {
        var isFormChanged = isFieldValueChangedOnForm(this.abEqEditForm_General);
        if (!isFormChanged) {
            isFormChanged = isFieldValueChangedOnForm(this.abDisposalInfo);
        }
        return isFormChanged;
    },

    /**
     * Save entire tab content.
     */
    saveTab: function () {
        return this.abEqEditForm_General_onSave();
    },

    /**
     * After refresh  event for general form.
     *
     */
    abEqEditForm_General_afterRefresh: function (equipmentFormPanel) {
        if (valueExists(this.eamReceiptParameters)) {
            if (equipmentFormPanel.newRecord && "single" === this.eamReceiptParameters.createMode && this.eamReceiptParameters.applyNextId) {
                this.eamReceiptParameters.applyNextId = false;
                equipmentFormPanel.setFieldValue("eq.eq_id", this.eamReceiptParameters.nextId);
            }
        }
        if (this.isSchemaCompatible) {
        	//refresh asset attributes
            this.abEqAssetAttributes.refresh(new Ab.view.Restriction({'eq_asset_attribute.eq_id': this.eqId, 'asset_attribute_std.asset_type': 'Equipment'}))
        }
        // refresh  disposal info form
        this.abDisposalInfo.refresh(equipmentFormPanel.restriction, equipmentFormPanel.newRecord);
        // load image
        if (valueExistsNotEmpty(equipmentFormPanel.getFieldValue('eqstd.doc_graphic'))) {
            equipmentFormPanel.showImageDoc('doc_graphic_image', 'eqstd.eq_std', 'eqstd.doc_graphic');
        }
    },
    beforeSave: function () {
        if (!this.newRecord) {
            var previousStatus = this.abEqEditForm_General.record.oldValues['eq.status'];
            var newStatus = this.abEqEditForm_General.getFieldValue('eq.status');
            if (valueExistsNotEmpty(newStatus) && newStatus !== previousStatus) {
                var currentDate = this.abEqEditForm_General.getDataSource().formatValue('eq.date_of_stat_chg', getCurrentDate(), true);
                this.abEqEditForm_General.setFieldValue('eq.date_of_stat_chg', currentDate);
            }
        }
    },
    /**
     * Save action  for general form.
     */
    abEqEditForm_General_onSave: function (afterSaveCallback) {
        var isSaved = false;
        if (this.abEqEditForm_General.canSave()
            && this.abDisposalInfo.canSave()) {
            // before save action
            this.beforeSave();
            // read disposal fields
            var disposalFieldValues = this.abDisposalInfo.getFieldValues();
            if (this.abEqEditForm_General.save()) {
                isSaved = true;
                // main for was saved --> set disposal fields
                setFormFields(this.abDisposalInfo, disposalFieldValues);

                if (this.abDisposalInfo.save()) {
                    // disposal form was saved --> perform after save action
                    afterSaveEquipment();

                    if (typeof(afterSaveCallback) === 'function') {
                        afterSaveCallback(isSaved);
                    }

                    return true;
                }
            }

            if (typeof(afterSaveCallback) === 'function') {
                afterSaveCallback(isSaved);
            }

        }
        return false;
    },

    /**
     * Cancel action for general form.
     */
    abEqEditForm_General_onCancel: function () {
        closeEquipmentForm();
    },

    /**
     * Delete action for general form.
     */
    abEqEditForm_General_onDelete: function () {
        var confirmDeleteMessage = getMessage('messageConfirmDelete').replace('{0}', this.eqId);
        var controller = this;

        View.confirm(confirmDeleteMessage, function (button) {
            if (button === 'yes') {
                try {
                    if (controller.abEqEditForm_General.deleteRecord()) {
                        afterDeleteEquipment();
                    }
                } catch (e) {
                    var errorMessage = getMessage("errorDelete").replace('{0}', controller.eqId);
                    View.showMessage('error', errorMessage, e.message, e.data);
                    return false;
                }
            }
        });

    },

    /**
     * On close event handler.
     */
    abEqEditForm_General_onClose: function () {
        this.abEqEditForm_General_onSave();
        closeEquipmentForm();
    },

    /**
     * Custom action command event handler.
     */
    abEqEditForm_General_onCustomActionCommand: function () {
        if (valueExists(this.customActionCommand)) {
            this.customActionCommand(this.abEqEditForm_General);
        }
    },

    onSelectEqRequirementRow: function (gridRow) {

        this.isSelectedFromReq = true;
        this.eqReqAutoNumber = gridRow.getFieldValue("eq_req_items.auto_number");

        var equipmentForm = this.abEqEditForm_General;

        var planningId = gridRow.getFieldValue("eq_req_items.planning_id");
        var blId = gridRow.getFieldValue("eq_req_items.bl_id");
        var description = gridRow.getFieldValue("eq_req_items.description");
        var dpId = gridRow.getFieldValue("eq_req_items.dp_id");
        var dvId = gridRow.getFieldValue("eq_req_items.dv_id");
        var flId = gridRow.getFieldValue("eq_req_items.fl_id");
        var mfr = gridRow.getFieldValue("eq_req_items.mfr");//general
        var modelno = gridRow.getFieldValue("eq_req_items.modelno");//general
        var rmId = gridRow.getFieldValue("eq_req_items.rm_id");
        var siteId = gridRow.getFieldValue("eq_req_items.site_id");

        var crtEqId = equipmentForm.getFieldValue('eq.eq_id');

        if (valueExistsNotEmpty(planningId) && !valueExistsNotEmtpy(crtEqId)) {
            equipmentForm.setFieldValue("eq.eq_id", planningId);
        }

        equipmentForm.setFieldValue("eq.description", description);
        equipmentForm.setFieldValue("eq.mfr", mfr);
        equipmentForm.setFieldValue("eq.modelno", modelno);
        equipmentForm.setFieldValue("eq.bl_id", blId);
        equipmentForm.setFieldValue("eq.dp_id", dpId);
        equipmentForm.setFieldValue("eq.dv_id", dvId);
        equipmentForm.setFieldValue("eq.fl_id", flId);
        equipmentForm.setFieldValue("eq.rm_id", rmId);
        equipmentForm.setFieldValue("eq.site_id", siteId);

        var currentComment = equipmentForm.getFieldValue('eq.comments');
        var addToComment = getMessage('textAddedFromRequirement').replace('{0}', getCurrentDate()).replace('{1}', getCurrentTime());
        currentComment += " " + addToComment;
        equipmentForm.setFieldValue("eq.comments", currentComment);
    }
});

/**
 * After Save  event handler.
 *
 */
function afterSaveEquipment() {
    var controller = View.controllers.get('abEquipmentForm_tabGeneralController');
    controller.eqId = controller.abEqEditForm_General.getFieldValue('eq.eq_id');
    if (controller.newRecord && !valueExists(controller.eamReceiptParameters)) {
        controller.newRecord = false;
    }

    if (valueExists(controller.callbackMethod)) {
        controller.callbackMethod();
    }

    // update eq req record
    if (controller.isSelectedFromReq) {
        var dsEqReq = controller.abEqEditForm_selectEqFromReq_ds;
        var restriction = new Ab.view.Restriction();
        restriction.addClause('eq_req_items.auto_number', controller.eqReqAutoNumber, '=');
        var record = dsEqReq.getRecord(restriction);
        record.setValue('eq_req_items.eq_id', controller.eqId);
        try {
            dsEqReq.saveRecord(record);
            controller.isSelectedFromReq = false;
            controller.eqReqAutoNumber = null;
        } catch (e) {
            Workflow.handleError(e);
            return false;
        }
    }

    // call common after save function
    afterSaveEquipmentCommon(controller.eqId);
}

/**
 * After Delete  event handler.
 *
 */
function afterDeleteEquipment() {
    var controller = View.controllers.get('abEquipmentForm_tabGeneralController');
    controller.eqId = null;
    controller.abEqEditForm_General.refresh(null, true);

    if (valueExists(controller.callbackMethod)) {
        controller.callbackMethod();
    }

    afterDeleteEquipmentCommon();

    closeEquipmentForm();
}

/**
 * On select equipment from requirement.
 * @param ctx command context
 * @returns
 */
function onSelectEqFromRequirement(ctx) {
    var controller = View.controllers.get('abEquipmentForm_tabGeneralController');
    var objGrid = View.panels.get(ctx.command.parentPanelId);
    var gridRow = objGrid.gridRows.get(objGrid.selectedRowIndex);
    controller.onSelectEqRequirementRow(gridRow);
    objGrid.closeWindow();
}

/**
 * Check license.
 * @param {string} licenseId - license to check
 * @returns {boolean} has license enabled
 */
function checkLicense (licenseId) {
    var hasLicense = false;
    AdminService.getProgramLicense({
    	async: false,
    	callback: function (licenseDTO) {
            for ( var i = 0; i < licenseDTO.licenses.length; i++ ) {
                var license = licenseDTO.licenses[i];
                if (licenseId === license.id) {
                    hasLicense = license.enabled;
                    break;
                }
            }
        },
        errorHandler: function (m, e) {
            View.showException(e);
        }
    });
    return hasLicense;
}