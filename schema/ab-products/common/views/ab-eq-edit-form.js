/**
 * Main controller for equipment edit form.
 */
var abEqEditController = View.createController('abEqEditController', {
	// selected equipment code
	equipmentId: null,

	// is view works in new record mode
	isNewRecord: true,

	// name of the selected tab
	selectedTabName: null,

	// callback method
	callbackMethod:  null,

	// used from EAM
	customActionCommand: null,

	// main view restriction
	openerRestriction:null,

	visibleFields: null,

	useAssetAttributes: false,

	afterViewLoad: function() {
		this.useAssetAttributes = valueExists(View.dataSources.get('abEamCompatible_eq_asset_attribute'));
		if (!this.useAssetAttributes) {
            var tabGeneral = this.abEquipmentForm_tabs.findTab('abEquipmentForm_tabGeneral');
            var fileName = tabGeneral.fileName.replace('ab-eq-edit-form-general.axvw', 'ab-eq-edit-form-general-no-attributes.axvw');
            tabGeneral.fileName = fileName;
            tabGeneral.config.fileName = fileName;
            tabGeneral.loadView();
		}
		// set tabs event listeners
		this.abEquipmentForm_tabs.addEventListener('beforeTabChange', onBeforeTabChange);
		this.abEquipmentForm_tabs.addEventListener('afterTabChange', onAfterTabChange);

		// callback method
		if(valueExists(View.parameters) && valueExists(View.parameters.callback)){
			this.callbackMethod = View.parameters.callback;
		}

		// EAM custom action command
		if(valueExists(View.parameters) && valueExists(View.parameters.eamReceiptParameters)){
			this.customActionCommand = View.parameters.eamReceiptParameters.onClickActionEventHandler;
		}

		if(valueExists(View.parameters) && valueExists(View.parameters.visibleFields)){
			this.abEquipmentForm_tabs.parameters.visibleFields = View.parameters.visibleFields;
		}

		if(valueExists(View.restriction)){
			this.openerRestriction = View.restriction;
		}

		//get input value for equipment code when is called in edit mode
		this.getInputEquipmentCode();

	},
	afterInitialDataFetch: function() {
		// set custom eq_id for testing
		//this.initializeViewVariables('EQ_NEW_006');

		this.setViewTitle();
		this.setTabs();
	},

	initializeViewVariables: function(eqId) {
		this.isNewRecord = !valueExistsNotEmpty(eqId);
		this.equipmentId = eqId;
	},

	// read input equipment code  when is called from another view
	getInputEquipmentCode: function() {
		if (valueExists(View.parameters) && valueExists(View.parameters.eqId)) {
			this.initializeViewVariables(View.parameters.eqId);
		} else if (valueExists(View.restriction)) {
			var eqIdClause = View.restriction.findClause('eq.eq_id');
			if (eqIdClause) {
				this.initializeViewVariables(eqIdClause.value);
			}
		} else if (valueExists(window.location.parameters)
				&& valueExists(window.location.parameters['eq_id'])) {
			this.initializeViewVariables(window.location.parameters['eq_id']);
		}
	},

	setViewTitle: function(){
		var title = null;
		if(valueExistsNotEmpty(this.equipmentId)) {
			title = getMessage('titleEditEquipment').replace('{0}', this.equipmentId);
		} else{
			title = getMessage('titleAddNewEquipment');
		}
		this.abEqEdit_title.setTitle(title);
	},

	setTabs: function(){
		var restriction = null;
		if (valueExistsNotEmpty(this.equipmentId)) {
			restriction = new Ab.view.Restriction();
			restriction.addClause('eq.eq_id', this.equipmentId , '=');
		}

		for (var i = 0; i < this.abEquipmentForm_tabs.tabs.length; i++) {
			var objTab = this.abEquipmentForm_tabs.tabs[i];
			objTab.newRecord = this.isNewRecord;

			if (objTab.selected) {
				if (this.selectedTabName == null) {
					this.selectedTabName = objTab.name;
					this.abEquipmentForm_tabs.selectTab(this.selectedTabName, restriction, this.isNewRecord);
				} else {
					this.abEquipmentForm_tabs.setTabRestriction(objTab.name, restriction);
				}
			} else {
				this.abEquipmentForm_tabs.setTabRestriction(objTab.name, restriction);
			}
		}
	},

	setTabsRestriction: function(){
		var restriction = null;
		if (valueExistsNotEmpty(this.equipmentId)) {
			restriction = new Ab.view.Restriction();
			restriction.addClause('eq.eq_id', this.equipmentId , '=');
		}
		for (var i = 0; i < this.abEquipmentForm_tabs.tabs.length; i++) {
			var objTab = this.abEquipmentForm_tabs.tabs[i];
			objTab.newRecord = this.isNewRecord;
			this.abEquipmentForm_tabs.setTabRestriction(objTab.name, restriction);
		}
	}

});

/**
 * On beforeTabChange listener.
 *
 * @param tabPanel tab panel reference
 * @param currentTabName current tab name
 * @param newTabName new tab name
 * @returns boolean
 */
function onBeforeTabChange(tabPanel, currentTabName, newTabName){
	var currentTab = tabPanel.findTab(currentTabName);
	var currentTabController = getTabController(currentTab);
	/*
	 * Check if something was changed and form must be saved.
	 */
	if (currentTabController.isFieldValueChanged()) {
		var isSaveRequired = confirm(getMessage('confirmSaveOnChangeTab'));
		if (isSaveRequired) {
			return currentTabController.saveTab();
		} else {
			return true;
		}
	} else {
		return true;
	}
}

/**
 * On afterTabChange listener.
 *
 * @param tabPanel tab panel reference
 * @param selectedTabName name of the selected tab page
 * @returns boolean
 */
function onAfterTabChange(tabPanel, selectedTabName){
	return true;
}

/**
 * Returns tab controller  for specified tab.
 * @param objTab tab object
 * @returns object
 */
function getTabController(objTab){
	var controllerName = objTab.name + "Controller";
	var controller = null;
	if (objTab.useFrame) {
		if(objTab.getContentFrame().View){
			controller = objTab.getContentFrame().View.controllers.get(controllerName);
		}
	}else{
		controller = View.controllers.get(controllerName);
	}
	return controller;
}