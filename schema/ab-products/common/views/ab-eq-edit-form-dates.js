var abEquipmentForm_tabDatesController = View.createController('abEquipmentForm_tabDatesController', {
	// selected equipment code
	eqId: null,
	// if is new record
	newRecord: false,
	
	// callback method 
	callbackMethod: null,
	
	afterViewLoad: function(){
		if (valueExists(View.getOpenerView()) 
				&& valueExists(View.getOpenerView().parameters)) {
			
			if (valueExists(View.getOpenerView().parameters.callback)) {
				this.callbackMethod = View.getOpenerView().parameters.callback;
			}
		}
	},
	
	afterInitialDataFetch: function () {
		this.newRecord = this.view.newRecord;
		if (valueExists(this.view.restriction)) {
			var restriction = this.view.restriction;
			var clause = restriction.findClause('eq.eq_id');
			if (clause) {
				this.eqId = clause.value;
			}
		}
	},
	/**
	 * Check if form values were changed.
	 */
	isFieldValueChanged: function(){
		return isFieldValueChangedOnForm(this.abEqEditForm_Dates);
	},
	
	/**
	 * Save entire tab content.
	 */
	saveTab: function(){
		return this.abEqEditForm_Dates_onSave();
	},
	
	abEqEditForm_Dates_onCancel: function(){
		closeEquipmentForm();
	},
	
	abEqEditForm_Dates_onSave: function() {
		var isSaved = false;
		if (this.abEqEditForm_Dates.canSave()) {
			
			if (this.abEqEditForm_Dates.save()) {
				isSaved =  true;
				if (valueExists(this.callbackMethod)) {
					this.callbackMethod();
				}
				
				afterSaveEquipmentCommon(this.eqId);
				
				if(typeof(afterSaveCallback) == 'function'){
					afterSaveCallback(isSaved);
				}
				return true;
			}

			if(typeof(afterSaveCallback) == 'function'){
				afterSaveCallback(isSaved);
			}
		}
		return false;
	}
});

