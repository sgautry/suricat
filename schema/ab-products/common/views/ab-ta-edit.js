
var abTaEditController = View.createController('abTaEditController',{
	
	/**
	 * restrict list to user selection
	 */
	afterInitialDataFetch: function(){
		/*if(valueExists(this.taDetailsForm)){
			this.taDetailsForm.setHidden(true);
		}*/
		
	}
});

function addNewFurniture(){
	var restriction = new Ab.view.Restriction();
	restriction.addClause("isNewRecord", true, "=");
	showFurnitureDetails(restriction, true);
}

function editFurniture(ctx){
	var taId = ctx.restriction['ta.ta_id'];
	var restriction = new Ab.view.Restriction();
	restriction.addClause("ta.ta_id", taId, "=");
	
	showFurnitureDetails(restriction, false);
	
}

function showFurnitureDetails(restriction, newRecord){
	var detailsPanel = View.panels.get('taDetailsForm');
	
	detailsPanel.parameters.newRecord = newRecord;
	if(detailsPanel){
		detailsPanel.loadView('ab-ta-edit-form.axvw', restriction, null);
	}
}
