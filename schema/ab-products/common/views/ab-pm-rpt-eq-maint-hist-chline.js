View.createController('showLine', {
    afterViewLoad: function () {
        var openerController = View.getOpenerView().controllers.get('abEqMaintenanceHistoryController');
        var panel = this.abEqMaintHistChlineByMonthChart;
        var restriction = openerController.consoleParam;
        if (valueExistsNotEmpty(openerController.dateStart)) {
            restriction = restriction + " AND wrhwr.date_completed >= ${sql.date('" + openerController.dateStart + "')}";
        }
        if (valueExistsNotEmpty(openerController.dateEnd)) {
            restriction = restriction + " AND wrhwr.date_completed <=${sql.date('" + openerController.dateEnd + "')}";
        }
        panel.addParameter('parentRestriction', restriction);
        panel.refresh();
    }
});
