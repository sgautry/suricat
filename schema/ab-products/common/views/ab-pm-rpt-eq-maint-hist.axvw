<view version="2.0">
	<title translatable="true">Equipment Maintenance History</title>
	<layout type="borderLayout" id="mainLayout">
		<north autoScroll="true" split="true" initialSize="130"/>
		<center autoScroll="true"/>
	</layout>
	<layout type="borderLayout" id="centerLayout" containingLayout="mainLayout" region="center">
		<west autoScroll="true" split="true" initialSize="30%"/>
		<center autoScroll="true"/>
	</layout>

	<message name="totalForEq" translatable="true">Total for Equipment Code:</message>
	<message name="countForEq" translatable="true">Count for Equipment Code:</message>
	<message name="avgForEq" translatable="true">Average for Equipment Code:</message>
	<message name="minForEq" translatable="true">Min for Equipment Code:</message>
	<message name="maxForEq" translatable="true">Max for Equipment Code:</message>
	<message name="historyPanelTitle" translatable="true">Maintenance History for:</message>
	<message name="analysisPanelTitle" translatable="true">End of Life Analysis for:</message>

	<js file="ab-pm-rpt-eq-maint-hist.js"/>

	<dataSource id="abEqMaintHistFilterHwrDs">
		<table name="wrhwr" role="main"/>
		<table name="eq" role="standard"/>
		<field table="wrhwr" name="site_id"/>
		<field table="wrhwr" name="bl_id"/>
		<field table="wrhwr" name="dv_id"/>
		<field table="wrhwr" name="dp_id"/>
		<field table="eq" name="eq_std"/>
		<field table="wrhwr" name="date_completed"/>
	</dataSource>
	<dataSource id="abEqMaintHistShowEqDs">
		<table name="eq" role="main"/>
		<table name="eqstd" role="standard"/>
		<field table="eq" name="eq_id"/>
		<field table="eq" name="eq_std"/>
		<field table="eqstd" name="category"/>
		<field table="eqstd" name="description"/>
		<field table="eq" name="asset_id" hidden="true"/>
		<field table="eq" name="condition"/>
		<parameter name="eqIDSets" dataType="verbatim" value=""/>
		<parameter name="consoleParam" dataType="verbatim" value=""/>
		<restriction type="sql" sql="(eq.eq_id='-1' ${parameters['eqIDSets']})  ${parameters['consoleParam']}"/>
	</dataSource>
	<dataSource id="abEqMaintHistGroupHwrDs" type="grouping">
		<table name="wrhwr" role="main"/>
		<field table="wrhwr" name="eq_id" groupBy="true"/>
		<sortField table="wrhwr" name="eq_id" ascending="true"/>
		<restriction type="sql" sql="wrhwr.eq_id IS NOT NULL"/>
	</dataSource>
	<dataSource id="abEqMaintHistShowHwrDs">
		<table name="wrhwr" role="main"/>
		<field table="wrhwr" name="wr_id"/>
		<field table="wrhwr" name="prob_type"/>
		<field table="wrhwr" name="cost_total"/>
		<field table="wrhwr" name="act_labor_hours"/>
		<field table="wrhwr" name="down_time"/>
		<field table="wrhwr" name="date_requested"/>
		<field table="wrhwr" name="date_completed"/>
		<field table="wrhwr" name="wo_id"/>
		<field table="wrhwr" name="cause_type"/>
		<field table="wrhwr" name="repair_type"/>
		<field table="wrhwr" name="pmp_id"/>
		<field table="wrhwr" name="cf_notes"/>
		<field table="wrhwr" name="description"/>
		<field table="wrhwr" name="eq_id"/>
		<restriction type="sql" sql="wrhwr.eq_id IS NOT NULL"/>
	</dataSource>
	<dataSource id="abEqMaintHistStatHwrDs" type="grouping">
		<table name="wrhwr" role="main"/>
		<field table="wrhwr" name="wr_id"/>
		<field table="wrhwr" name="eq_id" groupBy="true"/>
		<field name="sum_cost_total" formula="sum" baseField="wrhwr.cost_total" dataType="number" size="6" decimals="2"/>
		<field name="count_cost_total" formula="count" baseField="wrhwr.cost_total" dataType="number" size="6" decimals="0"/>
		<field name="avg_cost_total" formula="avg" baseField="wrhwr.cost_total" dataType="number" size="6" decimals="2"/>
		<field name="min_cost_total" formula="min" baseField="wrhwr.cost_total" dataType="number" size="6" decimals="2"/>
		<field name="max_cost_total" formula="max" baseField="wrhwr.cost_total" dataType="number" size="6" decimals="2"/>
		<field name="sum_act_labor_hours" formula="sum" baseField="wrhwr.act_labor_hours" dataType="number" size="6" decimals="2"/>
		<field name="sum_down_time" formula="sum" baseField="wrhwr.down_time" dataType="number" size="6" decimals="2"/>
		<restriction type="sql" sql="wrhwr.eq_id IS NOT NULL"/>
	</dataSource>
	<dataSource id="abEqMaintHistAnalysisDs">
		<table name="eq" role="main"/>
		<table name="dv" role="standard"/>
		<table name="servcont" role="standard"/>
		<!-- Asset Information -->
		<field table="eq" name="eq_id"/>
		<field table="eq" name="eq_std"/>
		<field table="eq" name="status"/>
		<field table="eq" name="use1"/>
		<field table="eq" name="condition"/>
		<field table="eq" name="csi_id"/>
		<field table="eq" name="asset_id"/>
		<!-- Location -->
		<field table="eq" name="site_id"/>
		<field table="eq" name="bl_id"/>
		<field table="eq" name="fl_id"/>
		<field table="eq" name="rm_id"/>
		<!-- Ownership -->
		<field table="dv" name="bu_id"/>
		<field table="eq" name="dv_id"/>
		<field table="eq" name="dp_id"/>
		<field table="eq" name="em_id"/>
		<!-- Lifecycle Metrics -->
		<field table="eq" name="criticality"/>
		<field table="eq" name="qty_life_expct"/>
		<field table="eq" name="vf_eq_age" dataType="number" size="12" decimals="1">
			<title>Equipment Age</title>
			<sql dialect="generic">
				(${sql.daysBeforeCurrentDate('eq.date_in_service')}) / 365.0
			</sql>
		</field>
		<field table="eq" name="vf_remaining_yrs" dataType="number" size="12" decimals="1">
			<title>Remaining Life</title>
			<sql dialect="generic">
				(eq.qty_life_expct - ((${sql.daysBeforeCurrentDate('eq.date_in_service')}) / 365.0))
			</sql>
		</field>
		<field table="eq" name="qty_MTBF"/>
		<field table="eq" name="qty_MTTR"/>
		<field table="eq" name="meter_usage_per_day"/>
		<field table="eq" name="qty_hrs_run_day"/>
		<field table="eq" name="vf_sum_downtime" dataType="number" size="6" decimals="2">
			<title>Downtime for past 12 months</title>
			<sql dialect="generic">
				(SELECT 
				    SUM(wrhwr.down_time) 
				 FROM wrhwr 
				    WHERE wrhwr.eq_id = eq.eq_id ${parameters['consoleDateParam']})
			</sql>
		</field>
		<field table="eq" name="vf_avg_downtime" dataType="number" size="6" decimals="2">
			<title>Average Annual Down Time</title>
			<sql dialect="generic">
				(SELECT 
				    AVG(wrhwr.down_time) 
				 FROM wrhwr 
				    WHERE wrhwr.eq_id = eq.eq_id ${parameters['consoleDateParam']})
			</sql>
		</field>
		<field name="vf_count_prev_maint" dataType="number" decimals="0">
			<title>Count for Preventive Maintenance</title>
			<sql dialect="generic">
				(SELECT 
				    COUNT(wrhwr.wr_id) 
				 FROM wrhwr 
				    WHERE wrhwr.prob_type = 'PREVENTIVE MAINT' AND wrhwr.eq_id = eq.eq_id ${parameters['consoleDateParam']})
			</sql>
		</field>
		<field name="vf_count_on_demand" dataType="number" decimals="0">
			<title>Count for On Demand</title>
			<sql dialect="generic">
				(SELECT 
				    COUNT(wrhwr.wr_id) 
				 FROM wrhwr 
				    WHERE wrhwr.prob_type &lt;&gt; 'PREVENTIVE MAINT' AND wrhwr.eq_id = eq.eq_id ${parameters['consoleDateParam']})
			</sql>
		</field>
		<field name="vf_total_count" dataType="number" decimals="0">
			<title>Total Work Count</title>
			<sql dialect="generic">
				((SELECT 
				    COUNT(wrhwr.wr_id) 
				  FROM wrhwr 
				    WHERE wrhwr.prob_type = 'PREVENTIVE MAINT' AND wrhwr.eq_id = eq.eq_id ${parameters['consoleDateParam']}) 
				 +
				 (SELECT 
				    COUNT(wrhwr.wr_id) 
				  FROM wrhwr 
				    WHERE wrhwr.prob_type &lt;&gt; 'PREVENTIVE MAINT' AND wrhwr.eq_id = eq.eq_id ${parameters['consoleDateParam']}))
			</sql>
		</field>
		<!-- Asset Dates -->
		<field table="eq" name="date_manufactured"/>
		<field table="eq" name="date_purchased"/>
		<field table="eq" name="date_installed"/>
		<field table="eq" name="date_in_service"/>
		<field table="eq" name="meter_last_read"/>
		<field table="eq" name="date_warranty_exp"/>
		<field table="servcont" name="date_expiration"/>
		<field table="eq" name="date_in_repair"/>
		<field table="eq" name="date_in_storage"/>
		<field table="eq" name="date_sold"/>
		<!-- Asset Costs -->
		<field table="eq" name="cost_purchase"/>
		<field table="eq" name="cost_replace"/>
		<field table="eq" name="cost_dep_value"/>
		<field name="vf_value_market" dataType="number" size="12" decimals="2">
			<title>Market Value</title>
			<sql dialect="generic">
				(SELECT 
				    eq_dep.value_current 
				 FROM eq_dep 
				    WHERE eq_dep.eq_id = eq.eq_id 
				        AND eq_dep.report_id = (SELECT TOP 1 dep_reports.report_id FROM dep_reports, eq_dep eq_dep2 WHERE eq_dep2.report_id=dep_reports.report_id AND dep_reports.last_date = (SELECT MAX(dep_reports.last_date) FROM dep_reports)))
			</sql>
			<sql dialect="oracle">
				(SELECT 
				    eq_dep.value_current 
				 FROM eq_dep
				    WHERE eq_dep.eq_id = eq.eq_id
				        AND eq_dep.report_id = (SELECT dep_reports.report_id FROM dep_reports, eq_dep eq_dep2 WHERE eq_dep2.report_id=dep_reports.report_id AND dep_reports.last_date = (SELECT MAX(dep_reports.last_date) FROM dep_reports) AND rownum=1))
			</sql>
		</field>
		<field name="vf_cost_prev_maint" dataType="number" decimals="2" currency="${project.budgetCurrency.code}">
			<title>Total Historical Cost for Preventive Maintenance</title>
			<sql dialect="generic">
				CASE 
				    WHEN (SELECT SUM(wrhwr.cost_total) FROM wrhwr WHERE wrhwr.prob_type = 'PREVENTIVE MAINT' AND wrhwr.eq_id = eq.eq_id ${parameters['consoleDateParam']}) IS NULL
				    THEN 0
				    ELSE (SELECT SUM(wrhwr.cost_total) FROM wrhwr WHERE wrhwr.prob_type = 'PREVENTIVE MAINT' AND wrhwr.eq_id = eq.eq_id ${parameters['consoleDateParam']})
				END
			</sql>
		</field>

		<field name="vf_cost_on_demand" dataType="number" decimals="2" currency="${project.budgetCurrency.code}">
			<title>Total Historical Cost for On Demand</title>
			<sql dialect="generic">
				CASE 
				    WHEN (SELECT SUM(wrhwr.cost_total) FROM wrhwr WHERE wrhwr.prob_type &lt;&gt; 'PREVENTIVE MAINT' AND wrhwr.eq_id = eq.eq_id ${parameters['consoleDateParam']}) IS NULL
				    THEN 0
				    ELSE (SELECT SUM(wrhwr.cost_total) FROM wrhwr WHERE wrhwr.prob_type &lt;&gt; 'PREVENTIVE MAINT' AND wrhwr.eq_id = eq.eq_id ${parameters['consoleDateParam']})
				END
			</sql>
		</field>
		<field name="vf_cost_total" dataType="number" decimals="2" currency="${project.budgetCurrency.code}">
			<title>Total Historical Maintenance Cost</title>
			<sql dialect="generic">
				CASE 
				    WHEN (SELECT SUM(wrhwr.cost_total) FROM wrhwr WHERE wrhwr.eq_id = eq.eq_id ${parameters['consoleDateParam']}) IS NULL
				    THEN 0
				    ELSE (SELECT SUM(wrhwr.cost_total) FROM wrhwr WHERE wrhwr.eq_id = eq.eq_id ${parameters['consoleDateParam']})
				END
			</sql>
		</field>
		<field name="vf_cost_12_months" dataType="number" decimals="2" currency="${project.budgetCurrency.code}">
			<title>Maintenance Cost for Past 12 Months</title>
			<sql dialect="generic">
				CASE 
				    WHEN (SELECT SUM(wrhwr.cost_total) FROM wrhwr WHERE wrhwr.eq_id = eq.eq_id AND wrhwr.date_requested &lt;= ${sql.currentDate} AND wrhwr.date_requested &gt;= ${sql.dateAdd('month', -12, sql.currentDate)} ${parameters['consoleDateParam']}) IS NULL
				    THEN 0
				    ELSE (SELECT SUM(wrhwr.cost_total)/12 FROM wrhwr WHERE wrhwr.eq_id = eq.eq_id AND wrhwr.date_requested &lt;= ${sql.currentDate} AND	wrhwr.date_requested &gt;= ${sql.dateAdd('month', -12, sql.currentDate)} ${parameters['consoleDateParam']})
				END
			</sql>
		</field>
        <field name="vf_avg_annual_cost" dataType="number" decimals="2" currency="${project.budgetCurrency.code}">
            <title>Avg. Annual Maintenance Cost</title>
			<sql dialect="generic">
				CASE
				    WHEN (SELECT (CONVERT(INT,DATEDIFF(second,MIN(wrhwr.date_completed),MAX(wrhwr.date_completed)) / 86400 ) / 365.0) FROM wrhwr WHERE wrhwr.eq_id = eq.eq_id AND wrhwr.date_completed IS NOT NULL ${parameters['consoleDateParam']}) = 0
				    THEN 0
				    ELSE (SELECT (SUM(wrhwr.cost_total) / (CONVERT(INT,DATEDIFF(second,MIN(wrhwr.date_completed),MAX(wrhwr.date_completed)) / 86400 ) / 365.0)) ${sql.as} cost_total FROM wrhwr WHERE wrhwr.eq_id = eq.eq_id ${parameters['consoleDateParam']})
				END
			</sql>
			<sql dialect="oracle">
				CASE
					WHEN (SELECT (TRUNC(TO_NUMBER(MAX(wrhwr.date_completed) - MIN(wrhwr.date_completed)) / 1 , 0)) FROM wrhwr WHERE wrhwr.eq_id = eq.eq_id AND wrhwr.date_completed IS NOT NULL ${parameters['consoleDateParam']}) = 0
					THEN 0
					ELSE (SELECT (SUM(wrhwr.cost_total) / TRUNC(TO_NUMBER(MAX(wrhwr.date_completed) - MIN(wrhwr.date_completed)) / 1 , 0)) ${sql.as} cost_total FROM wrhwr WHERE wrhwr.eq_id = eq.eq_id ${parameters['consoleDateParam']})
				END
			</sql>
        </field>
		<field name="vf_annual_service_cost" dataType="number" decimals="2" currency="${project.budgetCurrency.code}">
			<title>Annual Service Contract Cost</title>
			<sql dialect="generic">
				CASE 
				    WHEN (EXISTS (SELECT 1 FROM regrequirement, regloc, compliance_locations
				                    WHERE regrequirement.regulation=regloc.regulation
				                        AND regloc.location_id=compliance_locations.location_id
										AND regloc.reg_requirement IS NOT NULL
										AND regrequirement.date_start IS NOT NULL
										AND regrequirement.date_end IS NOT NULL
										AND compliance_locations.eq_id=eq.eq_id))
				    THEN (SELECT ${sql.dateDiffInterval('day','regrequirement.date_start','regrequirement.date_end')} / 365.0 
				            FROM regrequirement, regloc, compliance_locations
				                WHERE regrequirement.regulation=regloc.regulation
									AND regloc.location_id=compliance_locations.location_id
									AND regloc.reg_requirement IS NOT NULL
									AND compliance_locations.eq_id=eq.eq_id)
				    WHEN (EXISTS (SELECT 1 FROM regrequirement, regloc, regprogram, compliance_locations
				                    WHERE regrequirement.regulation=regprogram.regulation
										AND regrequirement.regulation= regloc.regulation
										AND regrequirement.reg_program=regprogram.reg_program
										AND regloc.location_id=compliance_locations.location_id
										AND regloc.reg_requirement IS NOT NULL
										AND regprogram.date_start IS NOT NULL
										AND regprogram.date_end IS NOT NULL
										AND compliance_locations.eq_id=eq.eq_id))
				    THEN (SELECT ${sql.dateDiffInterval('day','regprogram.date_start','regprogram.date_end')} / 365.0 
				            FROM regrequirement, regloc, regprogram, compliance_locations
								WHERE regrequirement.regulation=regprogram.regulation
									AND regrequirement.regulation= regloc.regulation
									AND regrequirement.reg_program=regprogram.reg_program
									AND regloc.location_id=compliance_locations.location_id
									AND regloc.reg_requirement IS NOT NULL
									AND compliance_locations.eq_id=eq.eq_id)
				    WHEN (EXISTS (SELECT 1 FROM compliance_contract_cost,regrequirement,regloc,compliance_locations
				                    WHERE compliance_contract_cost.reg_requirement=regloc.reg_requirement
										AND regrequirement.regulation= regloc.regulation
										AND regloc.location_id=compliance_locations.location_id
										AND regloc.reg_requirement IS NOT NULL
										AND compliance_contract_cost.location_id IS NULL
										AND compliance_contract_cost.cost_comm_budget > 0
										AND compliance_locations.eq_id=eq.eq_id))
				    THEN (SELECT SUM(compliance_contract_cost.cost_comm_budget) / 
                                    (SELECT COUNT(compliance_locations.eq_id) 
                                        FROM compliance_contract_cost,regrequirement,regloc,compliance_locations 
                                            WHERE regrequirement.regulation= regloc.regulation
                                                 AND compliance_contract_cost.reg_program=regloc.reg_program
                                                 AND regloc.location_id=compliance_locations.location_id
                                                 AND compliance_contract_cost.reg_requirement IS NOT NULL
                                                 AND regloc.reg_requirement IS NOT NULL
                                                 AND compliance_contract_cost.location_id IS NULL
                                                 AND regloc.reg_program IN (SELECT regloc.reg_program FROM regloc,compliance_locations 
                                                                            WHERE regloc.location_id=compliance_locations.location_id
                                                                                AND regloc.reg_requirement IS NOT NULL
                                                                                AND compliance_locations.eq_id=eq.eq_id))
				            FROM compliance_contract_cost,regrequirement,regloc,compliance_locations
								WHERE compliance_contract_cost.reg_requirement=regloc.reg_requirement
									AND regrequirement.regulation= regloc.regulation
									AND regloc.location_id=compliance_locations.location_id
									AND regloc.reg_requirement IS NOT NULL
									AND compliance_contract_cost.location_id IS NULL
									AND compliance_locations.eq_id=eq.eq_id
								GROUP BY compliance_contract_cost.cost_comm_budget)
				    WHEN (EXISTS (SELECT 1 FROM compliance_contract_cost,regrequirement,regloc,compliance_locations
				                    WHERE regrequirement.regulation= regloc.regulation
										AND compliance_contract_cost.reg_program=regloc.reg_program
										AND regloc.location_id=compliance_locations.location_id
										AND compliance_contract_cost.reg_requirement IS NOT NULL
										AND regloc.reg_requirement IS NOT NULL
										AND compliance_contract_cost.location_id IS NULL
										AND compliance_contract_cost.cost_comm_budget > 0
										AND compliance_locations.eq_id=eq.eq_id))
				    THEN (SELECT SUM(compliance_contract_cost.cost_comm_budget) / 
                                    (SELECT COUNT(compliance_locations.eq_id) 
                                        FROM compliance_contract_cost,regrequirement,regloc,compliance_locations 
                                            WHERE regrequirement.regulation= regloc.regulation
                                                 AND compliance_contract_cost.reg_program=regloc.reg_program
                                                 AND regloc.location_id=compliance_locations.location_id
                                                 AND compliance_contract_cost.reg_requirement IS NOT NULL
                                                 AND regloc.reg_requirement IS NOT NULL
                                                 AND compliance_contract_cost.location_id IS NULL
                                                 AND regloc.reg_program IN (SELECT regloc.reg_program FROM regloc,compliance_locations 
                                                                            WHERE regloc.location_id=compliance_locations.location_id
                                                                                AND regloc.reg_requirement IS NOT NULL
                                                                                AND compliance_locations.eq_id=eq.eq_id))
				            FROM compliance_contract_cost,regrequirement,regloc,compliance_locations
								WHERE regrequirement.regulation= regloc.regulation
									AND compliance_contract_cost.reg_program=regloc.reg_program
									AND regloc.location_id=compliance_locations.location_id
									AND compliance_contract_cost.reg_requirement IS NOT NULL
									AND regloc.reg_requirement IS NOT NULL
									AND compliance_contract_cost.location_id IS NULL
									AND compliance_locations.eq_id=eq.eq_id
								GROUP BY compliance_contract_cost.cost_comm_budget)
				    WHEN (EXISTS (SELECT 1 FROM regloc, regprogram, compliance_locations
									WHERE regloc.reg_program = regprogram.reg_program
                                        AND regloc.regulation = regprogram.regulation
									    AND regloc.location_id=compliance_locations.location_id
										AND regloc.reg_requirement IS NULL
										AND regprogram.date_start IS NOT NULL
										AND regprogram.date_end IS NOT NULL
										AND compliance_locations.eq_id=eq.eq_id))
				    THEN (SELECT ${sql.dateDiffInterval('day','regprogram.date_start','regprogram.date_end')} / 365.0 
				            FROM regloc, regprogram, compliance_locations
								WHERE regloc.reg_program = regprogram.reg_program
                                        AND regloc.regulation = regprogram.regulation
                                        AND regloc.location_id=compliance_locations.location_id
                                        AND regloc.reg_requirement IS NULL
									AND compliance_locations.eq_id=eq.eq_id)
				    WHEN (EXISTS (SELECT 1 FROM regloc, compliance_contract_cost, compliance_locations
									WHERE compliance_contract_cost.reg_program = regloc.reg_program
										AND regloc.location_id = compliance_locations.location_id
										AND compliance_contract_cost.reg_requirement IS NULL
										AND regloc.reg_requirement IS NULL
										AND compliance_contract_cost.location_id IS NULL
										AND compliance_contract_cost.cost_comm_budget > 0
										AND compliance_locations.eq_id=eq.eq_id))
				    THEN (SELECT SUM(compliance_contract_cost.cost_comm_budget) / 
				                    (SELECT COUNT(compliance_locations.eq_id) 
				                        FROM regloc, compliance_contract_cost, compliance_locations 
				                            WHERE compliance_contract_cost.reg_program = regloc.reg_program
												AND regloc.location_id = compliance_locations.location_id
												AND compliance_contract_cost.reg_requirement IS NULL
												AND regloc.reg_requirement IS NULL
												AND compliance_contract_cost.location_id IS NULL
				                                AND regloc.reg_program IN (SELECT regloc.reg_program FROM regloc, compliance_locations 
				                                                            WHERE regloc.location_id=compliance_locations.location_id
				                                                                AND regloc.reg_requirement IS NULL
				                                                                AND compliance_locations.eq_id=eq.eq_id))
				            FROM regloc, compliance_contract_cost, compliance_locations
								WHERE compliance_contract_cost.reg_program = regloc.reg_program
									AND regloc.location_id = compliance_locations.location_id
									AND compliance_contract_cost.reg_requirement IS NULL
									AND regloc.reg_requirement IS NULL
									AND compliance_contract_cost.location_id IS NULL
									AND compliance_locations.eq_id=eq.eq_id
								GROUP BY compliance_contract_cost.cost_comm_budget)
				    ELSE 0
				END
			</sql>
		</field>
		<!-- TODO <field table="eq" name="total_cost_ownership"><title>Total Cost of Ownership</title></field> -->
		<parameter name="consoleParam" dataType="verbatim" value=""/>
		<parameter name="consoleDateParam" dataType="verbatim" value=""/>
		<restriction type="sql" sql="1=1 ${parameters['consoleParam']}"/>
	</dataSource>

	<panel type="console" columns="3" id="abEqMaintenanceHistory_filterPanel" dataSource="abEqMaintHistFilterHwrDs" layout="mainLayout" region="north">
		<title>Filter</title>
		<action id="search">
			<title>Show</title>
		</action>
		<action id="clear">
			<title>Clear</title>
		</action>

		<field name="date_completed" alias="wrhwr.date_completed.from" table="wrhwr" readOnly="false">
			<title>Date Work Completed From</title>
		</field>
		<field name="date_completed" alias="wrhwr.date_completed.to" table="wrhwr" readOnly="false">
			<title>Date Work Completed To</title>
		</field>
		<field table="wrhwr" name="site_id" selectValueType="multiple"/>
		<field table="wrhwr" name="bl_id" selectValueType="multiple"/>
		<field table="wrhwr" name="dv_id" selectValueType="multiple"/>
		<field table="wrhwr" name="dp_id" selectValueType="multiple"/>
		<field table="eq" name="eq_std" selectValueType="multiple"/>
		<field/>
		<field/>
	</panel>

	<panel type="grid" id="abEqMaintenanceHistory_eqGrid" showOnLoad="false" dataSource="abEqMaintHistShowEqDs" layout="centerLayout" region="west">
		<title>Equipment</title>
		<sortField table="eq" name="eq_id" ascending="true"/>
		<action id="showChart">
			<title>Pie Chart</title>
			<command type="callFunction" functionName="showChart"/>
		</action>
		<action id="showLine">
			<title>Line Chart</title>
			<command type="callFunction" functionName="showLine"/>
		</action>

		<field table="eq" name="eq_id" controlType="link">
			<command type="callFunction" functionName="onSelectEquipment"/>
		</field>
		<field table="eq" name="eq_std" controlType="link">
			<command type="callFunction" functionName="onSelectEquipment"/>
		</field>
		<field table="eqstd" name="category" controlType="link">
			<command type="callFunction" functionName="onSelectEquipment"/>
		</field>
		<field table="eqstd" name="description" controlType="link">
			<command type="callFunction" functionName="onSelectEquipment"/>
		</field>
		<field table="eq" name="condition" controlType="link">
			<command type="callFunction" functionName="onSelectEquipment"/>
		</field>
		<field table="eq" name="asset_id" hidden="true"/>
	</panel>
	<!-- Customize column report. -->
	<style type="text/css">
		.columnReport table {margin-top: 6px}
		.reportTitle {font-weight: bold; background: #BCD8ED}
		.reportTitle + td {background: #BCD8ED}
		.columnReportLabel {text-align: left; border-left: 1px solid #BCD8ED}
		.columnReportValue {border-right: 1px solid #BCD8ED}
		.columnReportSpacer {padding: 5px !important; border: none !important;}
	</style>
	<tabs id="abEqMaintenanceHistoryTabs" tabRefreshPolicy="refreshOnLoad" layout="centerLayout" region="center" showOnLoad="false">
		<tab name="abEqMaintenanceHistory_assignment" selected="true" useFrame="true">
			<title>End of Life Analysis</title>
			<action id="export"></action>
			<!-- use 5 columns instead of 3 to increase the space between the columns -->
			<panel type="columnReport" columns="4" id="abEqMaintenanceHistory_analysis" dataSource="abEqMaintHistAnalysisDs" showOnLoad="false">
				<action id="export_analysis" type="menu" imageName="/schema/ab-core/graphics/icons/view/export.png">
					<action id="exportDOCX_analysis">
						<title>DOC</title>
						<command type="exportPanel" outputType="docx" panelId="abEqMaintenanceHistory_analysis"/>
					</action>
				</action>
				<field table="eq" name="info_title" dataType="text" labelClass="reportTitle">
					<title>Asset Information</title>
				</field>
				<field id="metrics_title" dataType="text" labelClass="reportTitle">
					<title>Lifecycle Metrics</title>
				</field>
				<field id="dates_title" dataType="text" labelClass="reportTitle">
					<title>Asset Dates</title>
				</field>
				<field id="costs_title" dataType="text" labelClass="reportTitle">
					<title>Asset Costs</title>
				</field>
				<field table="eq" name="eq_id"/>
				<field table="eq" name="criticality"/>
				<field table="eq" name="date_manufactured"/>
				<field table="eq" name="cost_purchase"/>
				<field table="eq" name="eq_std"/>
				<field table="eq" name="qty_life_expct"/>
				<field table="eq" name="date_purchased"/>
				<field table="eq" name="cost_replace"/>
				<field table="eq" name="status"/>
				<field/>
				<field table="eq" name="date_installed"/>
				<field table="eq" name="cost_dep_value">
					<title>Book Value</title>
				</field>
				<field table="eq" name="use1"/>
				<field table="eq" name="vf_eq_age" dataType="number" size="12" decimals="1">
					<title>Equipment Age (yrs)</title>
				</field>
				<field table="eq" name="date_in_service"/>
				<field name="vf_value_market" dataType="number" size="12" decimals="2">
					<title>Market Value</title>
				</field>
				<field table="eq" name="condition"/>
				<field table="eq" name="vf_remaining_yrs" dataType="number" size="12" decimals="1">
					<title>Remaining Life (yrs)</title>
				</field>
				<field table="eq" name="meter_last_read"/>
				<field/>
				<field table="eq" name="csi_id"/>
				<field/>
				<field/>
				<!-- TODO -->
				<field id="total_cost_ownership">
					<!-- <title>Total Cost of Ownership</title> -->
				</field>
				<field table="eq" name="asset_id"/>
				<field table="eq" name="qty_MTBF"/>
				<field table="eq" name="date_warranty_exp"/>
				<field/>
				<field/>
				<field table="eq" name="qty_MTTR"/>
				<field table="servcont" name="date_expiration"/>
				<field name="vf_annual_service_cost" dataType="number" decimals="2" currency="${project.budgetCurrency.code}">
					<title>Annual Service Contract Cost</title>
				</field>
				<field id="location_title" labelClass="reportTitle">
					<title>Location</title>
				</field>
				<field table="eq" name="meter_usage_per_day"/>
				<field/>
				<field/>
				<field table="eq" name="site_id"/>
				<field table="eq" name="qty_hrs_run_day"/>
				<field table="eq" name="date_in_repair"/>
				<field name="vf_cost_prev_maint" dataType="number" decimals="2" currency="${project.budgetCurrency.code}">
					<title>Total Historical Cost for Preventive Maintenance</title>
				</field>
				<field table="eq" name="bl_id"/>
				<field/>
				<field table="eq" name="date_in_storage"/>
				<field name="vf_cost_on_demand" dataType="number" decimals="2" currency="${project.budgetCurrency.code}">
					<title>Total Historical Cost for On Demand</title>
				</field>
				<field table="eq" name="fl_id"/>
				<field table="eq" name="vf_sum_downtime" dataType="number" size="6" decimals="2">
					<title>Downtime for past 12 months</title>
				</field>
				<field table="eq" name="date_sold"/>
				<field name="vf_cost_total" dataType="number" decimals="2" currency="${project.budgetCurrency.code}">
					<title>Total Historical Maintenance Cost</title>
				</field>
				<field table="eq" name="rm_id"/>
				<field table="eq" name="vf_avg_downtime" dataType="number" size="6" decimals="2">
					<title>Average Annual Down Time (hrs)</title>
				</field>
				<field/>
				<field/>
				<field/>
				<field/>
				<field/>
				<field name="vf_cost_12_months" dataType="number" decimals="2" currency="${project.budgetCurrency.code}">
					<title>Maintenance Cost for Past 12 Months</title>
				</field>
				<!-- Ownership -->
				<field id="ownership_title" labelClass="reportTitle">
					<title>Ownership</title>
				</field>
				<field name="vf_count_prev_maint" dataType="number" decimals="0">
					<title>Count for Preventive Maintenance</title>
				</field>
				<field/>
				<field name="vf_avg_annual_cost" dataType="number" decimals="2" currency="${project.budgetCurrency.code}">
					<title>Avg. Annual Maintenance Cost</title>
				</field>
				<field table="dv" name="bu_id"/>
				<field name="vf_count_on_demand" dataType="number" decimals="0">
					<title>Count for On Demand</title>
				</field>
				<field/>
				<field/>
				<field table="eq" name="dv_id"/>
				<field name="vf_total_count" dataType="number" decimals="0">
					<title>Total Work Count</title>
				</field>
				<field/>
				<field/>
				<field table="eq" name="dp_id"/>
				<field/>
				<field/>
				<field/>
				<field table="eq" name="em_id"/>
				<field/>
				<field/>
				<field/>
			</panel>
		</tab>
		<tab name="abEqMaintenanceHistory_maintenance">
			<title>Maintenance History</title>
			<panel type="grid" id="abEqMaintenanceHistory_historyReport" controlType="reportGrid" dataSource="abEqMaintHistShowHwrDs" showOnLoad="false">
				<title>Maintenance History</title>
				<action id="export_historyReport" type="menu" imageName="/schema/ab-core/graphics/icons/view/export.png">
					<action id="exportDOCX_historyReport">
						<title>DOC</title>
						<command type="exportPanel" outputType="docx" panelId="abEqMaintenanceHistory_historyReport"/>
					</action>
				</action>
				<sortField table="wrhwr" name="eq_id" ascending="true"/>
				<sortField table="wrhwr" name="wr_id" ascending="true"/>
				<field table="wrhwr" name="eq_id" hidden="true"/>
				<field table="wrhwr" name="wr_id"/>
				<field table="wrhwr" name="prob_type"/>
				<field table="wrhwr" name="cost_total"/>
				<field table="wrhwr" name="act_labor_hours"/>
				<field table="wrhwr" name="down_time"/>
				<field table="wrhwr" name="date_requested"/>
				<field table="wrhwr" name="date_completed"/>
				<field table="wrhwr" name="wo_id"/>
				<field table="wrhwr" name="cause_type"/>
				<field table="wrhwr" name="repair_type"/>
				<field table="wrhwr" name="pmp_id"/>
				<field table="wrhwr" name="cf_notes"/>
				<field table="wrhwr" name="description"/>
			</panel>
		</tab>
	</tabs>
</view>