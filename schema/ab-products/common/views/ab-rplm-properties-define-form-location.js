var abPropertiesDefineForm_tabLocationController = View.createController('abPropertiesDefineForm_tabLocationController', {
    // selected property code
    prId: null,
    // if is new record
    newRecord: false,

    geocodeControl: null,
    // check backward compatible to 23.1 schema
    isSchemaCompatible: false,
    afterViewLoad: function () {
        // check backward compatible
        this.isSchemaCompatible = valueExists(View.dataSources.get('custodianType_ds')) && this.checkLicense('AbAssetEAM');
        var controller = this;
        this.abDefineProperty_ownerCustodian.afterCreateCellContent = function (row, column, cellElement) {
            controller.customizeCustodianFields(row, column, cellElement);
        };
        this.abDefineProperty_otherCustodian.afterCreateCellContent = function (row, column, cellElement) {
            controller.customizeCustodianFields(row, column, cellElement);
        };
    },
    afterInitialDataFetch: function () {
        var restriction = new Ab.view.Restriction();
        var tabs = this.view.parentTab.parentPanel;
        var newRecord = tabs.parameters.newRecord;
        var tabsRestriction = tabs.parameters.restriction;

        if (newRecord) {
            this.abPropertiesDefineForm_location.newRecord = newRecord;
            this.abPropertiesDefineForm_location.show();
        } else {
            if (tabsRestriction) {
                if (tabsRestriction["property.pr_id"]) {
                    restriction.addClause('property.pr_id', tabsRestriction["property.pr_id"]);
                } else if (tabsRestriction.clauses && tabsRestriction.clauses[0]) {
                    restriction.addClause('property.pr_id', tabsRestriction.clauses[0].value);
                }
            }
            this.abPropertiesDefineForm_location.refresh(restriction);
        }
    },

    abPropertiesDefineForm_location_onSave: function () {
        var propertyForm = this.abPropertiesDefineForm_location;
        beforeSaveProperty(this);
        var isSaved = propertyForm.save();
        setTimeout(function () {
            if (isSaved) {
                afterSaveProperty(abPropertiesDefineForm_tabLocationController, propertyForm);
                propertyForm.refresh();
            }
        }, 1000);
        var tabs = View.getOpenerView().panels.get("abPropertiesDefineForm_tabs");
        if (tabs && valueExists(tabs.parameters) && valueExists(tabs.parameters.callback)) {
            this.callbackMethod = tabs.parameters.callback;
        }
        callCallbackMethod();
    },

    abPropertiesDefineForm_location_beforeRefresh: function () {
        var restriction = new Ab.view.Restriction();
        var tabs = this.view.parentTab.parentPanel;

        if (tabs) {
            var newRecord = tabs.parameters.newRecord;
            var tabsRestriction = tabs.parameters.restriction;

            if (valueExists(newRecord) && newRecord == true) {
                this.abPropertiesDefineForm_location.newRecord = newRecord;
            } else if (newRecord == false) {
                if (tabsRestriction) {
                    if (tabsRestriction["property.pr_id"]) {
                        restriction.addClause('property.pr_id', tabsRestriction["property.pr_id"]);
                    } else if (tabsRestriction.clauses && tabsRestriction.clauses[0]) {
                        restriction.addClause('property.pr_id', tabsRestriction.clauses[0].value);
                    }
                }
                this.abPropertiesDefineForm_location.restriction = restriction;
            }
        }
    },

    abPropertiesDefineForm_location_afterRefresh: function () {
        this.newRecord = this.abPropertiesDefineForm_location.newRecord;
        this.prId = this.abPropertiesDefineForm_location.getFieldValue('property.pr_id');
        if (this.isSchemaCompatible) {
            var ownerRestriction = new Ab.view.Restriction({'team.autonumbered_id': -1});
            var otherRestriction = new Ab.view.Restriction({'team.autonumbered_id': -1});
            if (valueExistsNotEmpty(this.prId)) {
                ownerRestriction = new Ab.view.Restriction();
                ownerRestriction.addClause('team.is_owner', 1, '=');
                ownerRestriction.addClause('team.pr_id', this.prId, '=');

                otherRestriction = new Ab.view.Restriction();
                otherRestriction.addClause('team.custodian_type', null, 'IS NOT NULL');
                otherRestriction.addClause('team.is_owner', 0, '=');
                otherRestriction.addClause('team.pr_id', this.prId, '=');
            }
            this.abDefineProperty_ownerCustodian.refresh(ownerRestriction, this.newRecord);
            this.abDefineProperty_otherCustodian.refresh(otherRestriction, this.newRecord);
        } else {
            this.abDefineProperty_ownerCustodian.show(false);
            this.abDefineProperty_otherCustodian.show(false);
        }
    },

    abPropertiesDefineForm_location_onLocate: function (context) {
        var me = this;
        var restriction = this.getCurrentPropertyRestriction();
        var record = this.abPropertiesDefineForm_location.getRecord();

        View.openDialog('ab-locate-asset.axvw', restriction, false, {
            width: 800,
            height: 600,
            record: record,
            assetType: 'property',
            callback: function () {
                refreshPropertyLocationPanel();
            }
        });
    },

    abPropertiesDefineForm_location_onGeocode: function () {
        if (this.geocodeControl === null) {
            this.initGeocodeControl();
        }

        var restriction = this.getCurrentPropertyRestriction();

        this.geocodeControl.geocode('ds_abPropertiesDefineForm',
            restriction, 'property',
            'property.pr_id',
            ['property.lat', 'property.lon'],
            ['property.address1', 'property.city_id', 'property.state_id', 'property.zip', 'property.ctry_id'],
            true);
    },

    initGeocodeControl: function () {
        this.geocodeControl = new Ab.arcgis.Geocoder();
        this.geocodeControl.callbackMethod = refreshPropertyLocationPanel;
    },

    getCurrentPropertyRestriction: function () {
        var record = this.abPropertiesDefineForm_location.record;
        var restriction = new Ab.view.Restriction();

        if (record) {
            restriction.addClause('property.pr_id', record.getValue('property.pr_id'), '=', 'OR');
        } else {
            restriction.addClause('property.pr_id', 'null', "=", "OR");
        }
        return restriction;
    },
    /**
     * Customize fields for custodian panels
     */
    customizeCustodianFields: function (row, column, cellElement) {
        if ('team.status' === column.id) {
            var filterEl = Ext.get(row.grid.getFilterInputId(column.id));
            var removeOptions = function (el, values) {
                for ( var x in values ) {
                    for ( var i = el.dom.length - 1; i >= 0; i-- ) {
                        if (x === el.dom[i].value) {
                            el.dom.remove(i);
                            break;
                        }
                    }
                }
            };
            removeOptions(filterEl, {'Archived': 'Archived', 'Removed': 'Removed'});
        }
    },
    onEditOwnerCustodian: function (row) {
        var autoNumberedId = row.row.getFieldValue('team.autonumbered_id');
        var restriction = new Ab.view.Restriction();
        restriction.addClause('team.autonumbered_id', autoNumberedId, '=');
        var controller = View.controllers.get('abPropertiesDefineForm_tabLocationController');

        View.openDialog('ab-eam-edit-custodian.axvw', restriction, false, {
            isOwner: true,
            assetType: 'property',
            assetCode: controller.prId,
            callback: function () {
                View.closeDialog();
                controller.abDefineProperty_ownerCustodian.refresh(controller.abDefineProperty_ownerCustodian.restriction);
            }
        });
    },

    abDefineProperty_ownerCustodian_onAddOwnerCustodian: function () {
        var selectedAsset = [{'asset_type': 'property', 'asset_id': this.prId}];
        var controller = this;
        this.addCustodianCommon(selectedAsset, true, true, function () {
            controller.abDefineProperty_ownerCustodian.refresh(controller.abDefineProperty_ownerCustodian.restriction);
        });
    },

    onEditOtherCustodian: function (row) {
        var autoNumberedId = row.row.getFieldValue('team.autonumbered_id');
        var restriction = new Ab.view.Restriction();
        restriction.addClause('team.autonumbered_id', autoNumberedId, '=');
        var controller = View.controllers.get('abPropertiesDefineForm_tabLocationController');

        View.openDialog('ab-eam-edit-custodian.axvw', restriction, false, {
            isOwner: false,
            assetType: 'property',
            assetCode: controller.prId,
            callback: function () {
                View.closeDialog();
                controller.abDefineProperty_otherCustodian.refresh(controller.abDefineProperty_otherCustodian.restriction);
            }
        });
    },

    abDefineProperty_otherCustodian_onAddOtherCustodian: function () {
        var selectedAsset = [{'asset_type': 'property', 'asset_id': this.prId}];
        var controller = this;
        this.addCustodianCommon(selectedAsset, false, true, function (closeDialog) {
            controller.abDefineProperty_otherCustodian.refresh(controller.abDefineProperty_otherCustodian.restriction);
        });
    },

    addCustodianCommon: function (selectedAsset, isOwner, newRecord, callbackFunction) {
        View.openDialog('ab-eam-assign-custodian.axvw', null, newRecord, {
            width: 1024,
            height: 800,
            isOwner: isOwner,
            selectedAssets: selectedAsset,
            useCustomRestriction: false,
            callback: function (addMultipleCustodians) {
                if (valueExists(callbackFunction)) {
                    callbackFunction();
                }
                if (!addMultipleCustodians) {
                    View.closeDialog();
                }
            }
        });
    },
    /**
     * Check license.
     * @param {string} licenseId - license to check
     * @returns {boolean} has license enabled
     */
    checkLicense: function (licenseId) {
        var hasLicense = false;
        AdminService.getProgramLicense({
            async: false,
            callback: function (licenseDTO) {
                for ( var i = 0; i < licenseDTO.licenses.length; i++ ) {
                    var license = licenseDTO.licenses[i];
                    if (licenseId === license.id) {
                        hasLicense = license.enabled;
                        break;
                    }
                }
            },
            errorHandler: function (m, e) {
                View.showException(e);
            }
        });
        return hasLicense;
    }
});

function refreshPropertyLocationPanel() {
    var propertyLocationPanel = abPropertiesDefineForm_tabLocationController.abPropertiesDefineForm_location;
    propertyLocationPanel.refresh(propertyLocationPanel.restriction);
}

function callCallbackMethod() {
    var controller = View.controllers.get('abPropertiesDefineForm_tabLocationController');
    if (valueExists(controller.callbackMethod)) {
        controller.callbackMethod();
    }
    return true;
}