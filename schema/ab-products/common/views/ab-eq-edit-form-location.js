var abEquipmentForm_tabLocationController = View.createController('abEquipmentForm_tabLocationController', {
    // selected equipment code
    eqId: null,
    // if is new record
    newRecord: false,
    // callback method
    callbackMethod: null,
    // check backward compatible to 23.1 schema
    isCustodianSchemaCompatible: false,
    isSiteOrgSchemaCompatible: false,
    afterViewLoad: function () {
        // check backward compatible
        this.isCustodianSchemaCompatible = schemaHasTables(['custodiantype']) && this.checkLicense('AbAssetEAM');
        this.isSiteOrgSchemaCompatible = schemaHasTables(['site_org']);
        // hide field tooltip if SMS licence is not available
        if (!checkLicense('AbEAMSMSBuilderExtension')) {
            jQuery(this.abEqEditForm_Location.getFieldCell('eq.campus_id')).find('.fieldTooltipButton').hide();
        }
        if (valueExists(View.getOpenerView())
            && valueExists(View.getOpenerView().parameters)) {
            if (valueExists(View.getOpenerView().parameters.callback)) {
                this.callbackMethod = View.getOpenerView().parameters.callback;
            }
        }
        this.abEqEditForm_Location.addEventListener("onAutoCompleteSelect", onAutoCompleteSelectValue);
        var controller = this;
        this.abEqEditForm_ownerCustodian.afterCreateCellContent = function (row, column, cellElement) {
            controller.customizeCustodianFields(row, column, cellElement);
        };
        this.abEqEditForm_otherCustodian.afterCreateCellContent = function (row, column, cellElement) {
            controller.customizeCustodianFields(row, column, cellElement);
        };
    },

    afterInitialDataFetch: function () {
        this.newRecord = this.view.newRecord;
        if (valueExists(this.view.restriction)) {
            var restriction = this.view.restriction;
            var clause = restriction.findClause('eq.eq_id');
            if (clause) {
                this.eqId = clause.value;
            }
        }
    },
    /**
     * Check if form values were changed.
     */
    isFieldValueChanged: function () {
        var isFormChanged = isFieldValueChangedOnForm(this.abEqEditForm_Location);
        if (!isFormChanged) {
            isFormChanged = isFieldValueChangedOnForm(this.abEqEditForm_affiliation);
        }
        return isFormChanged;
    },
    /**
     * Save entire tab content.
     */
    saveTab: function () {
        return this.abEqEditForm_Location_onSave();
    },

    abEqEditForm_Location_onCancel: function () {
        closeEquipmentForm();
    },

    abEqEditForm_Location_afterRefresh: function () {
        this.newRecord = this.abEqEditForm_Location.newRecord;
        if (valueExists(this.abEqEditForm_Location.restriction)) {
            var restriction = this.abEqEditForm_Location.restriction;
            var clause = restriction.findClause('eq.eq_id');
            if (clause) {
                this.eqId = clause.value;
            }
        }
        this.abEqEditForm_affiliation.addParameter('useSiteOrg', this.isSiteOrgSchemaCompatible);
        this.abEqEditForm_affiliation.refresh(this.abEqEditForm_Location.restriction, this.abEqEditForm_Location.newRecord);
        if (!this.isSiteOrgSchemaCompatible) {
            this.abEqEditForm_affiliation.showField('eq.org_unit', false);
        }
        if (this.isCustodianSchemaCompatible) {
            var ownerRestriction = new Ab.view.Restriction({'team.autonumbered_id': -1});
            var otherRestriction = new Ab.view.Restriction({'team.autonumbered_id': -1});
            if (valueExistsNotEmpty(this.eqId)) {
                ownerRestriction = new Ab.view.Restriction();
                ownerRestriction.addClause('team.is_owner', 1, '=');
                ownerRestriction.addClause('team.eq_id', this.eqId, '=');

                otherRestriction = new Ab.view.Restriction();
                otherRestriction.addClause('team.custodian_type', null, 'IS NOT NULL');
                otherRestriction.addClause('team.is_owner', 0, '=');
                otherRestriction.addClause('team.eq_id', this.eqId, '=');
            }
            this.abEqEditForm_ownerCustodian.refresh(ownerRestriction, this.newRecord);
            this.abEqEditForm_otherCustodian.refresh(otherRestriction, this.newRecord);
        } else {
            this.abEqEditForm_ownerCustodian.show(false);
            this.abEqEditForm_otherCustodian.show(false);
        }
    },

    abEqEditForm_Location_onSave: function () {
        var isSaved = false;
        if (this.abEqEditForm_Location.canSave()
            && this.abEqEditForm_affiliation.canSave()) {
            var buildingCode = this.abEqEditForm_Location.getFieldValue('eq.bl_id');
            var siteCode = this.abEqEditForm_Location.getFieldValue('eq.site_id');
            // validate site and building code
            if (!validateBuildingAndSite(buildingCode, siteCode)) {
                return false;
            }
            var affiliationFieldValues = this.abEqEditForm_affiliation.getFieldValues();

            if (this.abEqEditForm_Location.save()) {
                isSaved = true;
                // main for was saved --> set affiliation fields
                setFormFields(this.abEqEditForm_affiliation, affiliationFieldValues);

                if (this.abEqEditForm_affiliation.save()) {

                    afterSaveEquipmentCommon(this.eqId);

                    if (valueExists(this.callbackMethod)) {
                        this.callbackMethod();
                    }

                    if (typeof(afterSaveCallback) == 'function') {
                        afterSaveCallback(isSaved);
                    }
                    return true;
                }
            }

            if (typeof(afterSaveCallback) == 'function') {
                afterSaveCallback(isSaved);
            }
        }
        return false;
    },

    abEqEditForm_Location_onLocate: function (context) {
        var restriction = new Ab.view.Restriction();
        if (valueExistsNotEmpty(this.eqId)) {
            restriction.addClause('eq.eq_id', this.eqId, '=', 'OR');
        } else {
            restriction.addClause('eq.eq_id', '', 'IS NULL', 'OR');
        }

        var record = this.abEqEditForm_Location.getRecord();
        var controller = this;

        View.openDialog('ab-locate-asset.axvw', restriction, false, {
            width: 800,
            height: 600,
            record: record,
            assetType: 'eq',
            callback: function () {
                controller.abEqEditForm_Location.refresh(controller.abEqEditForm_Location.restriction);
            }
        });
    },
    /**
     * Customize fields for custodian panels
     */
    customizeCustodianFields: function (row, column, cellElement) {
        if ('team.status' === column.id) {
            var filterEl = Ext.get(row.grid.getFilterInputId(column.id));
            var removeOptions = function (el, values) {
                for ( var x in values ) {
                    for ( var i = el.dom.length - 1; i >= 0; i-- ) {
                        if (x === el.dom[i].value) {
                            el.dom.remove(i);
                            break;
                        }
                    }
                }
            };
            removeOptions(filterEl, {'Archived': 'Archived', 'Removed': 'Removed'});
        }
    },
    onEditOwnerCustodian: function (row) {
        var autoNumberedId = row.row.getFieldValue('team.autonumbered_id');
        var restriction = new Ab.view.Restriction();
        restriction.addClause('team.autonumbered_id', autoNumberedId, '=');
        var controller = View.controllers.get('abEquipmentForm_tabLocationController');

        View.openDialog('ab-eam-edit-custodian.axvw', restriction, false, {
            isOwner: true,
            assetType: 'eq',
            assetCode: controller.eqId,
            callback: function () {
                View.closeDialog();
                controller.abEqEditForm_ownerCustodian.refresh(controller.abEqEditForm_ownerCustodian.restriction);
            }
        });
    },

    abEqEditForm_ownerCustodian_onAddOwnerCustodian: function () {
        var selectedAsset = [{'asset_type': 'eq', 'asset_id': this.eqId}];
        var controller = this;
        this.addCustodianCommon(selectedAsset, true, true, function () {
            controller.abEqEditForm_ownerCustodian.refresh(controller.abEqEditForm_ownerCustodian.restriction);
        });
    },

    onEditOtherCustodian: function (row) {
        var autoNumberedId = row.row.getFieldValue('team.autonumbered_id');
        var restriction = new Ab.view.Restriction();
        restriction.addClause('team.autonumbered_id', autoNumberedId, '=');
        var controller = View.controllers.get('abEquipmentForm_tabLocationController');

        View.openDialog('ab-eam-edit-custodian.axvw', restriction, false, {
            isOwner: false,
            assetType: 'eq',
            assetCode: controller.eqId,
            callback: function () {
                View.closeDialog();
                controller.abEqEditForm_otherCustodian.refresh(controller.abEqEditForm_otherCustodian.restriction);
            }
        });
    },

    abEqEditForm_otherCustodian_onAddOtherCustodian: function () {
        var selectedAsset = [{'asset_type': 'eq', 'asset_id': this.eqId}];
        var controller = this;
        this.addCustodianCommon(selectedAsset, false, true, function () {
            controller.abEqEditForm_otherCustodian.refresh(controller.abEqEditForm_otherCustodian.restriction);
        });
    },

    addCustodianCommon: function (selectedAsset, isOwner, newRecord, callbackFunction) {
        View.openDialog('ab-eam-assign-custodian.axvw', null, newRecord, {
            width: 1024,
            height: 800,
            isOwner: isOwner,
            selectedAssets: selectedAsset,
            useCustomRestriction: false,
            callback: function (addMultipleCustodians) {
                if (valueExists(callbackFunction)) {
                    callbackFunction();
                }
                if (!addMultipleCustodians) {
                    View.closeDialog();
                }
            }
        });
    },
    /**
     * Check license.
     * @param {string} licenseId - license to check
     * @returns {boolean} has license enabled
     */
    checkLicense: function (licenseId) {
        var hasLicense = false;
        AdminService.getProgramLicense({
            async: false,
            callback: function (licenseDTO) {
                for ( var i = 0; i < licenseDTO.licenses.length; i++ ) {
                    var license = licenseDTO.licenses[i];
                    if (licenseId === license.id) {
                        hasLicense = license.enabled;
                        break;
                    }
                }
            },
            errorHandler: function (m, e) {
                View.showException(e);
            }
        });
        return hasLicense;
    }
});


function refreshEquipmentLocationPanel() {
    var equipmentLocationPanel = abEquipmentForm_tabLocationController.abEqEditForm_Location;
    equipmentLocationPanel.refresh(equipmentLocationPanel.restriction);
}
function callCallbackMethod() {
    var controller = View.controllers.get('abEquipmentForm_tabLocationController');
    if (valueExists(controller.callbackMethod)) {
        controller.callbackMethod();
    }
    return true;
}
/**
 * On change location field listener.
 * @param fieldName
 * @param newValue
 * @param oldValue
 */
function onChangeLocationField(fieldName, newValue, oldValue) {
    var form = View.panels.get('abEqEditForm_Location');
    var fieldElement = form.getFieldElement(fieldName);
    oldValue = oldValue || fieldElement.oldValue;
    var resetFormFields = valueExistsNotEmpty(oldValue) && newValue !== oldValue;

    switch (fieldName) {
        case 'eq.site_id':
            if (resetFormFields) {
                if (!valueExistsNotEmpty(newValue)) {
                    form.setFieldValue('eq.site_id', '');
                    form.setFieldValue('eq.pr_id', '');

                    form.setFieldValue('eq.geo_region_id', '');
                    form.setFieldValue('eq.ctry_id', '');
                    form.setFieldValue('eq.regn_id', '');
                    form.setFieldValue('eq.state_id', '');
                    form.setFieldValue('eq.city_id', '');
                }
                form.setFieldValue('eq.pr_id', '');
                resetLookupField(form, 'eq.pr_id');

                form.setFieldValue('eq.bl_id', '');
                resetLookupField(form, 'eq.bl_id');

                form.setFieldValue('eq.fl_id', '');
                form.setFieldValue('eq.rm_id', '');
            }
            break;
        case 'eq.pr_id':
            if (resetFormFields) {
                form.setFieldValue('eq.bl_id', '');
                resetLookupField(form, 'eq.bl_id');

                form.setFieldValue('eq.fl_id', '');
                form.setFieldValue('eq.rm_id', '');
            }
            break;
        case 'eq.bl_id':
            if (resetFormFields) {
                //
                if (valueExists(changedLocationFieldName) && ('eq.fl_id' === changedLocationFieldName || 'eq.rm_id' === changedLocationFieldName)) {
                    form.setFieldValue('eq.site_id', '');
                    resetLookupField(form, 'eq.site_id');

                    form.setFieldValue('eq.pr_id', '');
                    resetLookupField(form, 'eq.pr_id');

                    form.setFieldValue('eq.geo_region_id', '');
                    form.setFieldValue('eq.ctry_id', '');
                    form.setFieldValue('eq.regn_id', '');
                    form.setFieldValue('eq.state_id', '');
                    form.setFieldValue('eq.city_id', '');

                    changedLocationFieldName = null;
                } else {
                    form.setFieldValue('eq.fl_id', '');
                    form.setFieldValue('eq.rm_id', '');
                }
            }
            break;
        case 'eq.fl_id':
            if (resetFormFields) {
                form.setFieldValue('eq.rm_id', '');
            }
            break;
        case 'eq.ctry_id':
            setGeoRegion(form, fieldName, newValue);
            break;
    }
}
/**
 * Check if building floor and room fields were changed.
 * @type {string}
 */
var changedLocationFieldName;
/**
 * After select site event listener
 *
 * @param fieldName field name
 * @param newValue new value
 * @param oldValue old value
 */
function afterSelectSite(fieldName, newValue, oldValue) {
    if ('eq.site_id' === fieldName || 'eq.ctry_id' === fieldName) {
        onChangeLocationField(fieldName, newValue, oldValue);
    }
}
/**
 * After select property event listener
 *
 * @param fieldName field name
 * @param newValue new value
 * @param oldValue old value
 */
function afterSelectProperty(fieldName, newValue, oldValue) {
    if ('eq.pr_id' === fieldName || 'eq.ctry_id' === fieldName) {
        onChangeLocationField(fieldName, newValue, oldValue);
    }
}
/**
 * After select building event listener
 *
 * @param fieldName field name
 * @param newValue new value
 * @param oldValue old value
 */
function afterSelectBuilding(fieldName, newValue, oldValue) {
    if ('eq.bl_id' === fieldName || 'eq.ctry_id' === fieldName) {
        changedLocationFieldName = 'eq.bl_id';
        onChangeLocationField(fieldName, newValue, oldValue);
    }
}
/**
 * After select floor event listener
 *
 * @param fieldName field name
 * @param newValue new value
 * @param oldValue old value
 */
function afterSelectFloor(fieldName, newValue, oldValue) {
    if ('eq.bl_id' === fieldName || 'eq.fl_id' === fieldName) {
        changedLocationFieldName = 'eq.fl_id';
        onChangeLocationField(fieldName, newValue, oldValue);
    }
}
/**
 * After select room event listener
 *
 * @param fieldName field name
 * @param newValue new value
 * @param oldValue old value
 */
function afterSelectRoom(fieldName, newValue, oldValue) {
    if ('eq.bl_id' === fieldName) {
        changedLocationFieldName = 'eq.rm_id';
        onChangeLocationField(fieldName, newValue, oldValue);
    }
}
/**
 * On auto complete select event listener
 *
 * @param form form object
 * @param fieldName field name
 * @param selectedValue selected value
 *
 */
function onAutoCompleteSelectValue(form, fieldName, selectedValue) {
    onChangeLocationField(fieldName, selectedValue);
}
/**
 * Set geographic region.
 * @param form form panel
 * @param fieldName field name
 * @param selectedValue selected value
 */
function setGeoRegion(form, fieldName, selectedValue) {
    if ('eq.ctry_id' === fieldName) {
        if (valueExistsNotEmpty(selectedValue)) {
            var geoRegionId = getGeoRegionId(selectedValue);
            form.setFieldValue('eq.geo_region_id', geoRegionId);
        } else {
            form.setFieldValue('eq.geo_region_id', '');
        }
    }
}
/**
 * Reset lookup field value
 * @param form form panel
 * @param fieldName field name
 */
function resetLookupField(form, fieldName) {
    var findLookupFieldName = function (fieldId) {
        var lookupFieldName = '';
        var field = form.fields.get(fieldId);
        if (field && field.fieldDef && field.fieldDef.lookupName) {
            lookupFieldName = field.fieldDef.lookupName;
        }
        return lookupFieldName;
    };
    var lookupFieldName = findLookupFieldName(fieldName);
    if (valueExists(lookupFieldName)) {
        form.setFieldValue(lookupFieldName, '');
    }
}

/**
 * Check license.
 * @param {string} licenseId - license to check
 * @returns {boolean} has license enabled
 */
function checkLicense(licenseId) {
    var hasLicense = false;
    AdminService.getProgramLicense({
        async: false,
        callback: function (licenseDTO) {
            for ( var i = 0; i < licenseDTO.licenses.length; i++ ) {
                var license = licenseDTO.licenses[i];
                if (licenseId === license.id) {
                    hasLicense = license.enabled;
                    break;
                }
            }
        },
        errorHandler: function (m, e) {
            View.showException(e);
        }
    });
    return hasLicense;
}
