var abPropertiesDefineForm_ownershipTransactionController = View.createController('abPropertiesDefineForm_ownershipTransactionController', {
    callbackMethod: null,
    afterInitialDataFetch: function () {
        if (valueExists(this.view.parameters) && valueExists(this.view.parameters.callbackMethod)) {
            this.callbackMethod = this.view.parameters.callbackMethod;
        }
    },
    abPropertiesDefineForm_ownershipTransaction_onSave: function () {
        if (this.abPropertiesDefineForm_ownershipTransaction.save()) {
            this.addCostToProperty();
            callCallbackMethod();
        }
    },
    /**
     * Add cost purchase and date purchase to property. This values will also be recorded by asset transactions.
     */
    addCostToProperty: function () {
        var otRecord = this.abPropertiesDefineForm_ownershipTransaction.getRecord();
        var prId = otRecord.getValue('ot.pr_id'),
            costPurchase = otRecord.getValue('ot.cost_purchase'),
            datePurchase = otRecord.getValue('ot.date_purchase');
        var propertyDataSource = View.dataSources.get('ds_abProperties_purchase');
        var propertyRecord = propertyDataSource.getRecord(new Ab.view.Restriction({'property.pr_id': prId}));
        propertyRecord.setValue('property.cost_purchase', costPurchase);
        propertyRecord.setValue('property.date_purchase', datePurchase);
        propertyDataSource.saveRecord(propertyRecord);
    },
    abPropertiesDefineForm_ownershipTransaction_onDelete: function () {
        var form = this.abPropertiesDefineForm_ownershipTransaction;
        var dataSource = form.getDataSource();
        var record = form.record;
        View.confirm(getMessage('confirmDelete'), function (button) {
            if (button === 'yes') {
                try {
                    dataSource.deleteRecord(record);
                    callCallbackMethod();
                    View.closeThisDialog();
                } catch (e) {
                    var message = String.format(getMessage('errorDelete'));
                    View.showMessage('error', message, e.message, e.data);
                }
            }
        });
    }
});

function callCallbackMethod() {
    var controller = View.controllers.get('abPropertiesDefineForm_ownershipTransactionController');
    if (valueExists(controller.callbackMethod)) {
        controller.callbackMethod();
    }
    return true;
}
