/**
 * Check if field value was changed on specified form.
 * 
 * @param objForm form object 
 * @returns boolean
 */
function isFieldValueChangedOnForm(objForm){
	var oldFieldValues = objForm.getOldFieldValues();
	var fieldValues = objForm.getFieldValues();
	for(var fieldName in fieldValues){
		var newValue = fieldValues[fieldName];
		var oldValue = oldFieldValues[fieldName];
		var objField = objForm.fields.get(fieldName);

		if (!objField.hidden && !objField.fieldDef.readOnly) {
			if (valueExistsNotEmpty(newValue) 
					&& ( (valueExistsNotEmpty(oldValue) && newValue != oldValue) || !valueExistsNotEmpty(oldValue))) {
				return true;
			}
		}
	}
	return false;
}

/**
 * Set form  field values.
 *  
 * @param form form object
 * @param values object  with field values
 * 
 */
function setFormFields(form, values){
	for (var fieldName in values) {
		form.setFieldValue(fieldName, values[fieldName]);
	}
}

/**
 * Validate  building code against site code.
 * Check if exists bl.bl_id AND bl.site_id
 * 
 * @param buildingCode building  id 
 * @param siteCode site id
 * @returns boolean
 */
function validateBuildingAndSite(buildingCode, siteCode){
	if(valueExistsNotEmpty(buildingCode) && valueExistsNotEmpty(siteCode)){
		var parameters = {
			tableName: 'bl',
	        fieldNames: toJSON(['bl.bl_id', 'bl.site_id']),
	        restriction: toJSON(new Ab.view.Restriction({'bl.bl_id':buildingCode, 'bl.site_id':siteCode}))
		};
	    try {
	        var result = Workflow.call('AbCommonResources-getDataRecords', parameters);
	        if (result.data.records.length == 0){
				View.showMessage(getMessage('no_match_bl_site'));
				return false;
			}
	    } catch (e) {
	        Workflow.handleError(e);
			return false;
	    }
	}
	return true;
}

/**
 * returns Geo region is for specified country
 * @param ctryId country code
 * @returns string
 */
function getGeoRegionId(ctryId){
	var geoRegionId = null;
	if(valueExistsNotEmpty(ctryId)){
		var parameters = {
			tableName: 'ctry',
	        fieldNames: toJSON(['ctry.ctry_id', 'ctry.geo_region_id']),
	        restriction: toJSON(new Ab.view.Restriction({'ctry.ctry_id':ctryId}))
		};
	    try {
	        var result = Workflow.call('AbCommonResources-getDataRecord', parameters);
			if (result.code == 'executed'){
				if(valueExists(result.dataSet) 
						&& valueExistsNotEmpty(result.dataSet.getValue("ctry.geo_region_id"))){
					var geoRegionId = result.dataSet.getValue("ctry.geo_region_id");
				}
			}
	    } catch (e) {
	        Workflow.handleError(e);
			return null;
	    }
	}
	return geoRegionId;
}


/**
 * Close equipment  form. 
 * 
 */
function closeEquipmentForm(){
	var detailsPanel = View.getOpenerView().parentViewPanel;

	if(detailsPanel){
		detailsPanel.loadView('ab-blank.axvw', null, null);
	}else{
		if(View.getOpenerView().getParentDialog() && View.getOpenerView().panels.get('abEquipmentForm_tabs')){
			View.getOpenerView().getParentDialog().close();
		}
	}
}

/**
 * Refresh equipment list panel if exists.
 * 
 */
function refreshEquipmentList(tabsController){
	if (valueExists(tabsController)) {
		// get main view
		var masterView = tabsController.view.getOpenerView();
		if (valueExists(masterView)) {
			var eqListPanel = masterView.panels.get('eqListPanel');
			if (valueExists(eqListPanel)) {
				eqListPanel.refresh(eqListPanel.restriction);
			}
		}
	}
}

/**
 * After save equipment common function. 
 * Update tabs controller variables, set view title and tabs restriction.
 * 
 * @param equipmentId equipment code
 * @returns boolean
 */
function afterSaveEquipmentCommon(equipmentId){
	var eqTabsController = null;
	if (valueExists(View.controllers.get('abEqEditController'))) {
		eqTabsController = View.controllers.get('abEqEditController');
	} else if (valueExists(View.getOpenerView().controllers.get('abEqEditController'))) {
		eqTabsController = View.getOpenerView().controllers.get('abEqEditController');
	}
	
	if (valueExists(eqTabsController)) {
		refreshEquipmentList(eqTabsController);
		eqTabsController.initializeViewVariables(equipmentId);
		eqTabsController.setViewTitle();
		eqTabsController.setTabsRestriction();
	}
}

/**
 * After delete equipment  common function.
 * 
 */
function afterDeleteEquipmentCommon(){
	var eqTabsController = null;
	if (valueExists(View.controllers.get('abEqEditController'))) {
		eqTabsController = View.controllers.get('abEqEditController');
	} else if (valueExists(View.getOpenerView().controllers.get('abEqEditController'))) {
		eqTabsController = View.getOpenerView().controllers.get('abEqEditController');
	}
	
	refreshEquipmentList(eqTabsController);
}

/**
 * Get current date.
 * @returns
 */
function getCurrentDate() {
	var today = new Date();
	var day = today.getDate();
	var month = today.getMonth() + 1;
	var year = today.getFullYear();
	return FormattingDate(day, month, year, strDateShortPattern);
}

/**
 * Get current time.
 */
function getCurrentTime() {
	var returnedTime = "";
	var curDate = new Date();
	var hoursNow = curDate.getHours();
	var minsNow = curDate.getMinutes();
	returnedTime = FormattingTime(hoursNow, minsNow, "", "HH:MM");
	return returnedTime;
}


/**
 * Close furniture form. 
 * 
 */
function closeFurnitureForm(){
	var detailsPanel = View.getOpenerView().parentViewPanel;

	if(detailsPanel){
		detailsPanel.loadView('ab-blank.axvw', null, null);
	}else{
		if(View.getOpenerView().getParentDialog() && View.getOpenerView().panels.get('abFurnitureForm_tabs')){
			View.getOpenerView().getParentDialog().close();
		}
	}
}

/**
 * After save furniture common function. 
 * Update tabs controller variables, set view title and tabs restriction.
 * 
 * @param taId furniture code
 * @returns boolean
 */
function afterSaveFurnitureCommon(taId){
	var taTabsController = null;
	if (valueExists(View.controllers.get('abTaEditFormCtrl'))) {
		taTabsController = View.controllers.get('abTaEditFormCtrl');
	} else if (valueExists(View.getOpenerView().controllers.get('abTaEditFormCtrl'))) {
		taTabsController = View.getOpenerView().controllers.get('abTaEditFormCtrl');
	}
	
	if (valueExists(taTabsController)) {
		refreshFurnitureList(taTabsController);
		taTabsController.initializeViewVariables(taId);
		taTabsController.setViewTitle();
		taTabsController.setTabsRestriction();
	}
}

/**
 * After delete furniture common function.
 * 
 */
function afterDeleteFurnitureCommon(){
	var taTabsController = null;
	if (valueExists(View.controllers.get('abTaEditFormCtrl'))) {
		taTabsController = View.controllers.get('abTaEditFormCtrl');
	} else if (valueExists(View.getOpenerView().controllers.get('abTaEditFormCtrl'))) {
		taTabsController = View.getOpenerView().controllers.get('abTaEditFormCtrl');
	}
	
	refreshFurnitureList(taTabsController);
}


/**
 * Refresh equipment list panel if exists.
 * 
 */
function refreshFurnitureList(tabsController){
	if (valueExists(tabsController)) {
		// get main view
		var masterView = tabsController.view.getOpenerView();
		if (valueExists(masterView)) {
			var taListPanel = masterView.panels.get('taListPanel');
			if (valueExists(taListPanel)) {
				taListPanel.refresh(taListPanel.restriction);
			}
		}
	}
}



