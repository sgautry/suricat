var abFurnitureForm_tabGeneralController = View.createController('abFurnitureForm_tabGeneralController', {
    // selected furniture code
    taId: null,

    // if is new record
    newRecord: false,

    // callback method
    callbackMethod: null,

    isSelectedFromReq: false,

    // id of the eq req record
    eqReqAutoNumber: null,

    selectedReqRecord: null,

    // parameters that come from eam receipt
    eamReceiptParameters: null,

    // custom action command
    customActionCommand: null,

    afterViewLoad: function () {
        if (valueExists(View.getOpenerView())
            && valueExists(View.getOpenerView().parameters)) {

            if (valueExists(View.getOpenerView().parameters.callback)) {
                this.callbackMethod = View.getOpenerView().parameters.callback;
            }

            if (valueExists(View.getOpenerView().parameters.eamReceiptParameters)) {
                this.eamReceiptParameters = View.getOpenerView().parameters.eamReceiptParameters;
                this.customActionCommand = View.getOpenerView().parameters.eamReceiptParameters.onClickActionEventHandler;
                // used for custom command
                this.view.parameters = View.getOpenerView().parameters;
            }
        }
    },

    afterInitialDataFetch: function () {

        this.newRecord = this.view.newRecord;
        if (valueExists(this.view.restriction)) {
            var restriction = this.view.restriction;
            var clause = restriction.findClause('ta.ta_id');
            if (clause) {
                this.taId = clause.value;
            }
        }
        if (valueExists(this.customActionCommand)) {
            this.abTaEditForm_General.actions.get('customActionCommand').setTitle(this.eamReceiptParameters.actionTitle);
            if ("single" === this.eamReceiptParameters.createMode) {
                this.eamReceiptParameters.applyNextId = true;
                this.abTaEditForm_General.actions.get('customActionCommand').show(true);
                this.abTaEditForm_General.actions.get('clear').show(true);
                this.abTaEditForm_General.actions.get('cancel').show(false);
            } else {
                var selectedRecords = this.eamReceiptParameters.selectedRecords;
                this.abTaEditForm_General.actions.get('customActionCommand').show(selectedRecords.length > 1);
            }
            this.abTaEditForm_General.actions.get('delete').show(false);
        }
    },

    /**
     * Check if form values were changed.
     */
    isFieldValueChanged: function () {
        var isFormChanged = isFieldValueChangedOnForm(this.abTaEditForm_General);
        if (!isFormChanged) {
            isFormChanged = isFieldValueChangedOnForm(this.abDisposalInfo);
        }
        return isFormChanged;
    },

    /**
     * Save entire tab content.
     */
    saveTab: function () {
        return this.abTaEditForm_General_onSave();
    },

    /**
     * After refresh  event for general form.
     *
     */
    abTaEditForm_General_afterRefresh: function (furnitureFormPanel) {
        if (valueExists(this.eamReceiptParameters)) {
            if (furnitureFormPanel.newRecord && "single" === this.eamReceiptParameters.createMode && this.eamReceiptParameters.applyNextId) {
                this.eamReceiptParameters.applyNextId = false;
                furnitureFormPanel.setFieldValue("ta.ta_id", this.eamReceiptParameters.nextId);
            }
        }
        // refresh  disposal info form
        this.abDisposalInfo.refresh(furnitureFormPanel.restriction, furnitureFormPanel.newRecord);
        // load image
        if (valueExistsNotEmpty(furnitureFormPanel.getFieldValue('fnstd.doc_graphic'))) {
            furnitureFormPanel.showImageDoc('doc_graphic_image', 'fnstd.fn_std', 'fnstd.doc_graphic');
        }
    },

    /**
     * Save action  for general form.
     */
    abTaEditForm_General_onSave: function (afterSaveCallback) {
        var isSaved = false;
        if (this.abTaEditForm_General.canSave()
            && this.abDisposalInfo.canSave()) {
            if (this.abTaEditForm_General.save()) {
                isSaved = true;
                if (this.abDisposalInfo.save()) {
                    // disposal form was saved --> perform after save action
                    afterSaveFurniture();

                    if (typeof(afterSaveCallback) == 'function') {
                        afterSaveCallback(isSaved);
                    }

                    return true;
                }
            }

            if (typeof(afterSaveCallback) == 'function') {
                afterSaveCallback(isSaved);
            }
        }
        return false;
    },
    
    copyDisposalInformation: function (field) {
        var buildingForm = this.abTaEditForm_General;
        var disposalForm = this.abDisposalInfo;
        var buildingDisposalRecord = disposalForm.getRecord();
        if (field && 'abDisposalInfo_ta.disposal_type' === field.id) {
            buildingForm.setFieldValue('ta.disposal_type', buildingDisposalRecord.getValue('ta.disposal_type'));
            return;
        }
        if (field && 'abDisposalInfo_ta.comment_disposal' === field.id) {
            buildingForm.setFieldValue('ta.comment_disposal', buildingDisposalRecord.getValue('ta.comment_disposal'));
            return;
        }
        if (field && 'abDisposalInfo_ta.date_disposal' === field.id) {
            var dateDisposal = buildingForm.getDataSource().parseValue('ta.date_disposal', buildingDisposalRecord.getValue('ta.date_disposal'), false);
            var localizedDate = buildingForm.getDataSource().formatValue('ta.date_disposal', dateDisposal, true);
            buildingForm.setFieldValue('ta.date_disposal', localizedDate);
        }
    },

    /**
     * Cancel action for general form.
     */
    abTaEditForm_General_onCancel: function () {
        closeFurnitureForm();
    },

    /**
     * Delete action for general form.
     */
    abTaEditForm_General_onDelete: function () {
        var confimDeleteMessage = getMessage('messageConfirmDelete').replace('{0}', this.taId);
        var controller = this;

        View.confirm(confimDeleteMessage, function (button) {
            if (button == 'yes') {
                try {
                    if (controller.abTaEditForm_General.deleteRecord()) {
                        afterDeleteFurniture();
                    }
                } catch (e) {
                    var errorMessage = getMessage("errorDelete").replace('{0}', controller.taId);
                    View.showMessage('error', errorMessage, e.message, e.data);
                    return false;
                }
            }
        });

    },

    /**
     * On close event handler.
     */
    abTaEditForm_General_onClose: function () {
        this.abTaEditForm_General_onSave();
        closeEquipmentForm();
    },

    /**
     * Custom action command event handler.
     */
    abTaEditForm_General_onCustomActionCommand: function () {
        if (valueExists(this.customActionCommand)) {
            this.customActionCommand(this.abTaEditForm_General);
        }
    },

    onSelectEqRequirementRow: function (gridRow) {

        this.isSelectedFromReq = true;
        this.eqReqAutoNumber = gridRow.getFieldValue("eq_req_items.auto_number");

        var furnitureForm = this.abTaEditForm_General;

        var planningId = gridRow.getFieldValue("eq_req_items.planning_id");
        var blId = gridRow.getFieldValue("eq_req_items.bl_id");
        var dpId = gridRow.getFieldValue("eq_req_items.dp_id");
        var dvId = gridRow.getFieldValue("eq_req_items.dv_id");
        var flId = gridRow.getFieldValue("eq_req_items.fl_id");
        var rmId = gridRow.getFieldValue("eq_req_items.rm_id");

        var crtTaId = furnitureForm.getFieldValue('ta.ta_id');

        if (valueExistsNotEmpty(planningId) && !valueExistsNotEmpty(crtTaId)) {
            furnitureForm.setFieldValue("ta.ta_id", planningId);
        }

        furnitureForm.setFieldValue("ta.bl_id", blId);
        furnitureForm.setFieldValue("ta.dp_id", dpId);
        furnitureForm.setFieldValue("ta.dv_id", dvId);
        furnitureForm.setFieldValue("ta.fl_id", flId);
        furnitureForm.setFieldValue("ta.rm_id", rmId);
    }
});

/**
 * After Save  event handler.
 *
 */
function afterSaveFurniture() {
    var controller = View.controllers.get('abFurnitureForm_tabGeneralController');
    controller.taId = controller.abTaEditForm_General.getFieldValue('ta.ta_id');
    if (controller.newRecord && !valueExists(controller.eamReceiptParameters)) {
        controller.newRecord = false;
    }

    if (valueExists(controller.callbackMethod)) {
        controller.callbackMethod();
    }

    // update eq req record
    if (controller.isSelectedFromReq) {
        var dsEqReq = controller.abTaEditForm_selectTaFromReq_ds;
        var restriction = new Ab.view.Restriction();
        restriction.addClause('eq_req_items.auto_number', controller.eqReqAutoNumber, '=');
        var record = dsEqReq.getRecord(restriction);
        record.setValue('eq_req_items.ta_id', controller.taId);
        try {
            dsEqReq.saveRecord(record);
            controller.isSelectedFromReq = false;
            controller.eqReqAutoNumber = null;
        } catch (e) {
            Workflow.handleError(e);
            return false;
        }
    }

    // call common after save function
    afterSaveFurnitureCommon(controller.taId);
}

/**
 * After Delete  event handler.
 *
 */
function afterDeleteFurniture() {
    var controller = View.controllers.get('abFurnitureForm_tabGeneralController');
    controller.taId = null;
    controller.abTaEditForm_General.refresh(null, true);

    if (valueExists(controller.callbackMethod)) {
        controller.callbackMethod();
    }

    afterDeleteFurnitureCommon();

    closeFurnitureForm();
}

/**
 * On select furniture from requirement.
 * @param ctx command context
 * @returns
 */
function onSelectTaFromRequirement(ctx) {
    var controller = View.controllers.get('abFurnitureForm_tabGeneralController');
    var objGrid = View.panels.get(ctx.command.parentPanelId);
    var gridRow = objGrid.gridRows.get(objGrid.selectedRowIndex);
    controller.onSelectEqRequirementRow(gridRow);
    objGrid.closeWindow();
}