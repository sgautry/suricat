var abEquipmentForm_tabDocumentsController = View.createController('abEquipmentForm_tabDocumentsController', {
	// selected equipment code
	eqId: null,
	// if is new record
	newRecord: false,
	
	// callback method 
	callbackMethod: null,
	
	afterViewLoad: function(){
		if (valueExists(View.getOpenerView()) 
				&& valueExists(View.getOpenerView().parameters)) {
			
			if (valueExists(View.getOpenerView().parameters.callback)) {
				this.callbackMethod = View.getOpenerView().parameters.callback;
			}
		}
	},
	
    afterInitialDataFetch: function () {
		this.newRecord = this.view.newRecord;
		if (valueExists(this.view.restriction)) {
			var restriction = this.view.restriction;
			var clause = restriction.findClause('eq.eq_id');
			if (clause) {
				this.eqId = clause.value;
			}
		}
    },
	/**
	 * Check if form values were changed.
	 */
	isFieldValueChanged: function(){
		return isFieldValueChangedOnForm(this.abEqEditForm_Documents);
	},
	
	/**
	 * Save entire tab content.
	 */
	saveTab: function(){
		return this.abEqEditForm_Documents_onSave();
	},

	abEqEditForm_Documents_onCancel: function () {
		closeEquipmentForm();
    },
    
    abEqEditForm_Documents_afterRefresh: function(){
    	if (!this.abEqEditForm_Documents.newRecord) {
    		var restriction = new Ab.view.Restriction();
    		restriction.addClause('docs_assigned.eq_id', this.abEqEditForm_Documents.getFieldValue('eq.eq_id'), '=');
    		this.abEqEditForm_documentsGrid.refresh(restriction, false);
    	}
    },
    
    abEqEditForm_Documents_onSave: function () {
    	var isSaved = false;
    	if (this.abEqEditForm_Documents.canSave()) {
    		if (this.abEqEditForm_Documents.save()) {
    			isSaved = true;
    			
    			afterSaveEquipmentCommon(this.eqId);
    			
    			if(typeof(afterSaveCallback) == 'function'){
    				afterSaveCallback(isSaved);
    			}
    			return true;
    		}
    		
			if(typeof(afterSaveCallback) == 'function'){
				afterSaveCallback(isSaved);
			}
    	}
    	return false;
    },
    
    abEqEditForm_documentsGrid_onNew: function () {
        if (valueExistsNotEmpty(this.eqId)) {
            addEditDoc(null, this.eqId, getMessage('add_new'));
        }
    },
    
    abEqEditForm_documentsGrid_onEdit: function (row) {
        if (valueExistsNotEmpty(this.eqId)) {
            addEditDoc(row, this.eqId, getMessage('edit'));
        }
    },
    
    abEqEditForm_documentsGrid_onDelete: function (row) {
        var dataSource = this.ds_abEqEditFormAccociatedDocuments;
        var record = row.getRecord();
        var gridPanel = this.abEqEditForm_documentsGrid;
        View.confirm(getMessage('message_document_confirmdelete'), function (button) {
            if (button == 'yes') {
                try {
                    dataSource.deleteRecord(record);
                    gridPanel.refresh(gridPanel.restriction);
                }
                catch (e) {
                    var message = String.format(getMessage('error_delete'));
                    View.showMessage('error', message, e.message, e.data);
                }
            }
        })
    },
    
    abEqEditForm_documentsGrid_onView: function (row) {
        View.showDocument({'doc_id': row.getFieldValue('docs_assigned.doc_id')}, 'docs_assigned', 'doc', row.getFieldValue('docs_assigned.doc'));
    }
});

function addEditDoc(row, itemId, title) {
    View.openDialog('ab-eq-edit-form-add-edit-documents.axvw', null, true, {
        width: 800,
        height: 400,
        closeButton: true,
        afterInitialDataFetch: function (dialogView) {
            var dialogController = dialogView.controllers.get('addEditDocController');
            dialogController.itemId = itemId;
            dialogController.addEditDoc.setTitle(title)
            if (row != null) {
                dialogController.docId = row.getFieldValue('docs_assigned.doc_id');
                dialogController.addEditDoc.refresh({
                    'docs_assigned.doc_id': row.getFieldValue('docs_assigned.doc_id')
                }, false);
            }
            else {
                dialogController.docId = null;
                dialogController.addEditDoc.newRecord = true;
            }
            dialogController.refreshPanels = new Array('abEqEditForm_documentsGrid');
        }
    });
}

