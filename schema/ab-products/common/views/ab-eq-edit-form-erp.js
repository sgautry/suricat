var abEquipmentForm_tabERPController = View.createController('abEquipmentForm_tabERPController', {
	// selected equipment code
	eqId: null,
	// if is new record
	newRecord: false,
	
	callbackMethod: null,
	
	customActionCommand: null,
	
	afterViewLoad: function(){
		if (valueExists(View.getOpenerView()) 
				&& valueExists(View.getOpenerView().parameters)) {
			
			if (valueExists(View.getOpenerView().parameters.callback)) {
				this.callbackMethod = View.getOpenerView().parameters.callback;
			}
		}
	},
	
	afterInitialDataFetch: function () {
		this.newRecord = this.view.newRecord;
		if (valueExists(this.view.restriction)) {
			var restriction = this.view.restriction;
			var clause = restriction.findClause('eq.eq_id');
			if (clause) {
				this.eqId = clause.value;
			}
		}
	},

	/**
	 * Check if form values were changed.
	 */
	isFieldValueChanged: function(){
		return isFieldValueChangedOnForm(this.abEqEditForm_ERP);
	},
	
	/**
	 * Save entire tab content.
	 */
	saveTab: function(){
		return this.abEqEditForm_ERP_onSave();
	},
	
	abEqEditForm_ERP_onCancel: function(){
		closeEquipmentForm();
	},
	
	abEqEditForm_ERP_onSave: function() {
		var isSaved = false;
		if (this.abEqEditForm_ERP.canSave()) {

			if (this.abEqEditForm_ERP.save()) {
				isSaved = true;
				
				if (valueExists(this.callbackMethod)) {
					this.callbackMethod();
				}
				
				afterSaveEquipmentCommon(this.eqId);
				
				if(typeof(afterSaveCallback) == 'function'){
					afterSaveCallback(isSaved);
				}
				
				return true;
			}

			if(typeof(afterSaveCallback) == 'function'){
				afterSaveCallback(isSaved);
			}
			
		}
		return false;
	}
});

/**
 * Check license.
 * @param {string} licenseId - license to check
 * @returns {boolean} has license enabled
 */
function checkLicense(licenseId) {
    var hasLicense = false;
    AdminService.getProgramLicense({
        async: false,
        callback: function (licenseDTO) {
            for ( var i = 0; i < licenseDTO.licenses.length; i++ ) {
                var license = licenseDTO.licenses[i];
                if (licenseId === license.id) {
                    hasLicense = license.enabled;
                    break;
                }
            }
        },
        errorHandler: function (m, e) {
            View.showException(e);
        }
    });
    return hasLicense;
}

