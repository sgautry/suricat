var abEquipmentForm_tabCostsController = View.createController('abEquipmentForm_tabCostsController', {
	// selected equipment code
	eqId: null,
	// if is new record
	newRecord: false,
	
	// callback method 
	callbackMethod: null,
	
	afterViewLoad: function(){
		if (valueExists(View.getOpenerView()) 
				&& valueExists(View.getOpenerView().parameters)) {
			
			if (valueExists(View.getOpenerView().parameters.callback)) {
				this.callbackMethod = View.getOpenerView().parameters.callback;
			}
		}
	},
	
	afterInitialDataFetch: function () {
		this.newRecord = this.view.newRecord;
		if (valueExists(this.view.restriction)) {
			var restriction = this.view.restriction;
			var clause = restriction.findClause('eq.eq_id');
			if (clause) {
				this.eqId = clause.value;
			}
		}
	},
	
	/**
	 * Check if form values were changed.
	 */
	isFieldValueChanged: function(){
		var isFormChanged = isFieldValueChangedOnForm(this.abEqEditForm_Costs);
		if (!isFormChanged) {
			isFormChanged = isFieldValueChangedOnForm(this.abPuchaseInfo);
		}
		if (!isFormChanged) {
			isFormChanged = isFieldValueChangedOnForm(this.abWarrantyInfo);
		}
		return isFormChanged;
	},
	
	/**
	 * Save entire tab content.
	 */
	saveTab: function(){
		return this.abEqEditForm_Costs_onSave();
	},
	
	abEqEditForm_Costs_afterRefresh: function(){
		// refresh  purchase information form
		this.abPuchaseInfo.refresh(this.abEqEditForm_Costs.restriction, this.abEqEditForm_Costs.newRecord);
		// refresh warranty information form
		this.abWarrantyInfo.refresh(this.abEqEditForm_Costs.restriction, this.abEqEditForm_Costs.newRecord);
	},
	
	abEqEditForm_Costs_onCancel: function(){
		closeEquipmentForm();
	},
	
	abEqEditForm_Costs_onSave: function(afterSaveCallback) {
		var isSaved = false;
		if (this.abEqEditForm_Costs.canSave() 
				&& this.abPuchaseInfo.canSave() 
				&& this.abWarrantyInfo.canSave()) {
			
			// read abPuchaseInfo and abWarrantyInfo fields
			var purchaseFieldValues = this.abPuchaseInfo.getFieldValues();
			var warrantyFieldValues = this.abWarrantyInfo.getFieldValues();
			
			if (this.abEqEditForm_Costs.save()) {
				isSaved = true;

				// main for was saved --> set purchase info fields
				setFormFields(this.abPuchaseInfo, purchaseFieldValues);
				// main for was saved --> set warranty info fields
				setFormFields(this.abWarrantyInfo, warrantyFieldValues);
				
				if (this.abPuchaseInfo.save()) {
					
					if (this.abWarrantyInfo.save()) {

						if (valueExists(this.callbackMethod)) {
							this.callbackMethod();
						}
						
						afterSaveEquipmentCommon(this.eqId);
						
						if(typeof(afterSaveCallback) == 'function'){
							afterSaveCallback(isSaved);
						}
						
						return true;
					}
				} 
			}
			
			if(typeof(afterSaveCallback) == 'function'){
				afterSaveCallback(isSaved);
			}
		}
		return false;
	}
});

