var abTaEditFormCtrl = View.createController('abTaEditFormCtrl', {
	// selected furniture code
	taId: null,
	// is view works in new record mode
	isNewRecord: true,
	
	// name of the selected tab
	selectedTabName: null,
	
	// callback method
	callbackMethod:  null,
	
	// used from EAM 
	customActionCommand: null,
	
	// main view restriction
	openerRestriction:null,
	
	visibleFields: null,

	afterViewLoad: function(){
		// set tabs event listeners
		this.abFurnitureForm_tabs.addEventListener('beforeTabChange', onBeforeTabChange);
		this.abFurnitureForm_tabs.addEventListener('afterTabChange', onAfterTabChange);
		// callback method
		if(valueExists(View.parameters) && valueExists(View.parameters.callback)){
			this.callbackMethod = View.parameters.callback;
		}

		if(valueExists(View.parameters) && valueExists(View.parameters.eamReceiptParameters)){
			this.customActionCommand = View.parameters.eamReceiptParameters.onClickActionEventHandler;
		}
		
		// EAM custom action command
		if(valueExists(View.parameters) && valueExists(View.parameters.eamReceiptParameters)){
			this.customActionCommand = View.parameters.eamReceiptParameters.onClickActionEventHandler;
		}

		if(valueExists(View.parameters) && valueExists(View.parameters.visibleFields)){
			this.abEquipmentForm_tabs.parameters.visibleFields = View.parameters.visibleFields;
		}

		if(valueExists(View.restriction)){
			this.openerRestriction = View.restriction;
		}

		//get input value for equipment code when is called in edit mode
		this.getInputFurnitureCode();

	},
	
	afterInitialDataFetch: function(){
		// set custom ta_id for testing
		//this.initializeViewVariables('');
		
		this.setViewTitle();
		this.setTabs();
	},
	
	// read input equipment code  when is called from another view
	getInputFurnitureCode: function() {
		if (valueExists(View.parameters) && valueExists(View.parameters.taId)) {
			this.initializeViewVariables(View.parameters.taId);
		} else if (valueExists(View.restriction)) {
			var taIdClause = View.restriction.findClause('ta.ta_id');
			if (taIdClause) {
				this.initializeViewVariables(taIdClause.value);
			}
		} else if (valueExists(window.location.parameters) 
				&& valueExists(window.location.parameters['ta_id'])) {
			this.initializeViewVariables(window.location.parameters['ta_id']);
		} 
	},

	initializeViewVariables: function(taId) {
		this.isNewRecord = !valueExistsNotEmpty(taId);
		this.taId = taId;
	},

	setViewTitle: function(){
		var title = null;
		if(valueExistsNotEmpty(this.equipmentId)) {
			title = getMessage('titleEditFurniture').replace('{0}', this.taId);
		} else{
			title = getMessage('titleAddNewFurniture');
		}
		this.abTaEdit_title.setTitle(title);
	},
	
	setTabs: function(){
		var restriction = null;
		if (valueExistsNotEmpty(this.taId)) {
			restriction = new Ab.view.Restriction();
			restriction.addClause('ta.ta_id', this.taId , '=');
		}

		for (var i = 0; i < this.abFurnitureForm_tabs.tabs.length; i++) {
			var objTab = this.abFurnitureForm_tabs.tabs[i];
			objTab.newRecord = this.isNewRecord;

			if (objTab.selected) {
				if (this.selectedTabName == null) {
					this.selectedTabName = objTab.name;
					this.abFurnitureForm_tabs.selectTab(this.selectedTabName, restriction, this.isNewRecord);
				} else {
					this.abFurnitureForm_tabs.setTabRestriction(objTab.name, restriction);
				}
			} else {
				this.abFurnitureForm_tabs.setTabRestriction(objTab.name, restriction);
			}
		}
	},
	
	setTabsRestriction: function(){
		var restriction = null;
		if (valueExistsNotEmpty(this.taId)) {
			restriction = new Ab.view.Restriction();
			restriction.addClause('ta.ta_id', this.taId , '=');
		}
		for (var i = 0; i < this.abFurnitureForm_tabs.tabs.length; i++) {
			var objTab = this.abFurnitureForm_tabs.tabs[i];
			objTab.newRecord = this.isNewRecord;
			this.abFurnitureForm_tabs.setTabRestriction(objTab.name, restriction);
		}
	}
});


/**
 * Returns tab controller  for specified tab.
 * @param objTab tab object
 * @returns object
 */
function getTabController(objTab){
	var controllerName = objTab.name + "Controller";
	var controller = null;
	if (objTab.useFrame) {
		if(objTab.getContentFrame().View){
			controller = objTab.getContentFrame().View.controllers.get(controllerName);
		}
	}else{
		controller = View.controllers.get(controllerName);
	}
	return controller;
}

/**
 * On beforeTabChange listener.
 * 
 * @param tabPanel tab panel reference
 * @param currentTabName current tab name
 * @param newTabName new tab name
 * @returns boolean
 */
function onBeforeTabChange(tabPanel, currentTabName, newTabName){
	var currentTab = tabPanel.findTab(currentTabName);
	var currentTabController = getTabController(currentTab);
	/*
	 * Check if something was changed and form must be saved.
	 */
	if (currentTabController.isFieldValueChanged()) {
		var isSaveRequired = confirm(getMessage('confirmSaveOnChangeTab'));
		if (isSaveRequired) {
			return currentTabController.saveTab();
		} else {
			return true;
		}
	} else {
		return true;
	}
}

/**
 * On afterTabChange listener.
 * 
 * @param tabPanel tab panel reference
 * @param selectedTabName name of the selected tab page
 * @returns boolean
 */
function onAfterTabChange(tabPanel, selectedTabName){
	return true;
}
