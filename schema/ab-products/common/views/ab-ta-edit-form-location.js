var abFurnitureForm_tabLocationController = View.createController('abFurnitureForm_tabLocationController', {
    // selected equipment code
    taId: null,
    // if is new record
    newRecord: false,

    // callback method
    callbackMethod: null,
    // check backward compatible to 23.1 schema
    isSchemaCompatible: false,
    afterViewLoad: function () {
        // check backward compatible
        this.isSchemaCompatible = valueExists(View.dataSources.get('custodianType_ds')) && this.checkLicense('AbAssetEAM');
        if (valueExists(View.getOpenerView())
            && valueExists(View.getOpenerView().parameters)) {
            if (valueExists(View.getOpenerView().parameters.callback)) {
                this.callbackMethod = View.getOpenerView().parameters.callback;
            }
        }
        var controller = this;
        this.abTaEditForm_ownerCustodian.afterCreateCellContent = function (row, column, cellElement) {
            controller.customizeCustodianFields(row, column, cellElement);
        };
        this.abTaEditForm_otherCustodian.afterCreateCellContent = function (row, column, cellElement) {
            controller.customizeCustodianFields(row, column, cellElement);
        };
    },

    afterInitialDataFetch: function () {
        this.newRecord = this.view.newRecord;
        if (valueExists(this.view.restriction)) {
            var restriction = this.view.restriction;
            var clause = restriction.findClause('ta.ta_id');
            if (clause) {
                this.taId = clause.value;
            }
        }
    },

    /**
     * Check if form values were changed.
     */
    isFieldValueChanged: function () {
        var isFormChanged = isFieldValueChangedOnForm(this.abTaEditForm_Location);
        if (!isFormChanged) {
            isFormChanged = isFieldValueChangedOnForm(this.abTaEditForm_affiliation);
        }
        return isFormChanged;
    },

    /**
     * Save entire tab content.
     */
    saveTab: function () {
        return this.abTaEditForm_Location_onSave();
    },

    abTaEditForm_Location_onCancel: function () {
        closeFurnitureForm();
    },

    abTaEditForm_Location_afterRefresh: function () {
        this.newRecord = this.abTaEditForm_Location.newRecord;
        if (valueExists(this.abTaEditForm_Location.restriction)) {
            var restriction = this.abTaEditForm_Location.restriction;
            var clause = restriction.findClause('ta.ta_id');
            if (clause) {
                this.taId = clause.value;
            }
        }
        this.abTaEditForm_affiliation.refresh(this.abTaEditForm_Location.restriction, this.abTaEditForm_Location.newRecord);
        if (this.isSchemaCompatible) {
            var ownerRestriction = new Ab.view.Restriction({'team.autonumbered_id': -1});
            var otherRestriction = new Ab.view.Restriction({'team.autonumbered_id': -1});
            if (valueExistsNotEmpty(this.taId)) {
                ownerRestriction = new Ab.view.Restriction();
                ownerRestriction.addClause('team.is_owner', 1, '=');
                ownerRestriction.addClause('team.ta_id', this.taId, '=');

                otherRestriction = new Ab.view.Restriction();
                otherRestriction.addClause('team.custodian_type', null, 'IS NOT NULL');
                otherRestriction.addClause('team.is_owner', 0, '=');
                otherRestriction.addClause('team.ta_id', this.taId, '=');
            }
            this.abTaEditForm_ownerCustodian.refresh(ownerRestriction, this.newRecord);
            this.abTaEditForm_otherCustodian.refresh(otherRestriction, this.newRecord);
        } else {
            this.abTaEditForm_ownerCustodian.show(false);
            this.abTaEditForm_otherCustodian.show(false);
        }

    },

    abTaEditForm_Location_onSave: function () {
        var isSaved = false;
        if (this.abTaEditForm_Location.canSave()
            && this.abTaEditForm_affiliation.canSave()) {

            var affiliationFieldValues = this.abTaEditForm_affiliation.getFieldValues();

            if (this.abTaEditForm_Location.save()) {
                isSaved = true;
                // main for was saved --> set affiliation fields
                setFormFields(this.abTaEditForm_affiliation, affiliationFieldValues);

                if (this.abTaEditForm_affiliation.save()) {

                    afterSaveFurnitureCommon(this.taId);

                    if (valueExists(this.callbackMethod)) {
                        this.callbackMethod();
                    }

                    if (typeof(afterSaveCallback) == 'function') {
                        afterSaveCallback(isSaved);
                    }
                    return true;
                }
            }

            if (typeof(afterSaveCallback) == 'function') {
                afterSaveCallback(isSaved);
            }
        }
        return false;
    },

    abTaEditForm_Location_onLocate: function (context) {
        var restriction = new Ab.view.Restriction();
        if (valueExistsNotEmpty(this.taId)) {
            restriction.addClause('ta.ta_id', this.taId, '=', 'OR');
        } else {
            restriction.addClause('ta.ta_id', '', 'IS NULL', 'OR');
        }

        var record = this.abTaEditForm_Location.getRecord();
        var controller = this;

        View.openDialog('ab-locate-asset.axvw', restriction, false, {
            width: 800,
            height: 600,
            record: record,
            assetType: 'ta',
            callback: function () {
                controller.abTaEditForm_Location.refresh(controller.abTaEditForm_Location.restriction);
            }
        });
    },
    /**
     * Customize fields for custodian panels
     */
    customizeCustodianFields: function (row, column, cellElement) {
        if ('team.status' === column.id) {
            var filterEl = Ext.get(row.grid.getFilterInputId(column.id));
            var removeOptions = function (el, values) {
                for ( var x in values ) {
                    for ( var i = el.dom.length - 1; i >= 0; i-- ) {
                        if (x === el.dom[i].value) {
                            el.dom.remove(i);
                            break;
                        }
                    }
                }
            };
            removeOptions(filterEl, {'Archived': 'Archived', 'Removed': 'Removed'});
        }
    },
    onEditOwnerCustodian: function (row) {
        var autoNumberedId = row.row.getFieldValue('team.autonumbered_id');
        var restriction = new Ab.view.Restriction();
        restriction.addClause('team.autonumbered_id', autoNumberedId, '=');
        var controller = View.controllers.get('abFurnitureForm_tabLocationController');

        View.openDialog('ab-eam-edit-custodian.axvw', restriction, false, {
            isOwner: true,
            assetType: 'ta',
            assetCode: controller.taId,
            callback: function () {
                View.closeDialog();
                controller.abTaEditForm_ownerCustodian.refresh(controller.abTaEditForm_ownerCustodian.restriction);
            }
        });
    },

    abTaEditForm_ownerCustodian_onAddOwnerCustodian: function () {
        var selectedAsset = [{'asset_type': 'ta', 'asset_id': this.taId}];
        var controller = this;
        this.addCustodianCommon(selectedAsset, true, true, function () {
            controller.abTaEditForm_ownerCustodian.refresh(controller.abTaEditForm_ownerCustodian.restriction);
        });
    },

    onEditOtherCustodian: function (row) {
        var autoNumberedId = row.row.getFieldValue('team.autonumbered_id');
        var restriction = new Ab.view.Restriction();
        restriction.addClause('team.autonumbered_id', autoNumberedId, '=');
        var controller = View.controllers.get('abFurnitureForm_tabLocationController');

        View.openDialog('ab-eam-edit-custodian.axvw', restriction, false, {
            isOwner: false,
            assetType: 'ta',
            assetCode: controller.taId,
            callback: function () {
                View.closeDialog();
                controller.abTaEditForm_otherCustodian.refresh(controller.abTaEditForm_otherCustodian.restriction);
            }
        });
    },

    abTaEditForm_otherCustodian_onAddOtherCustodian: function () {
        var selectedAsset = [{'asset_type': 'ta', 'asset_id': this.taId}];
        var controller = this;
        this.addCustodianCommon(selectedAsset, false, true, function (closeDialog) {
            controller.abTaEditForm_otherCustodian.refresh(controller.abTaEditForm_otherCustodian.restriction);
        });
    },

    addCustodianCommon: function (selectedAsset, isOwner, newRecord, callbackFunction) {
        View.openDialog('ab-eam-assign-custodian.axvw', null, newRecord, {
            width: 1024,
            height: 800,
            isOwner: isOwner,
            selectedAssets: selectedAsset,
            useCustomRestriction: false,
            callback: function (addMultipleCustodians) {
                if (valueExists(callbackFunction)) {
                    callbackFunction();
                }
                if (!addMultipleCustodians) {
                    View.closeDialog();
                }
            }
        });
    },
    /**
     * Check license.
     * @param {string} licenseId - license to check
     * @returns {boolean} has license enabled
     */
    checkLicense: function (licenseId) {
        var hasLicense = false;
        AdminService.getProgramLicense({
            async: false,
            callback: function (licenseDTO) {
                for ( var i = 0; i < licenseDTO.licenses.length; i++ ) {
                    var license = licenseDTO.licenses[i];
                    if (licenseId === license.id) {
                        hasLicense = license.enabled;
                        break;
                    }
                }
            },
            errorHandler: function (m, e) {
                View.showException(e);
            }
        });
        return hasLicense;
    }
});

function refreshFurnitureLocationPanel() {
    var furnitureLocationPanel = abFurnitureForm_tabLocationController.abTaEditForm_Location;
    furnitureLocationPanel.refresh(furnitureLocationPanel.restriction);
}

function callCallbackMethod() {
    var controller = View.controllers.get('abFurnitureForm_tabLocationController');
    if (valueExists(controller.callbackMethod)) {
        controller.callbackMethod();
    }
    return true;
}
/**
 * On change location field listener.
 * @param fieldName filed name
 * @param newValue new value
 * @param oldValue old value
 */
function onChangeLocationField(fieldName, newValue, oldValue) {
    var form = View.panels.get('abTaEditForm_Location');
    oldValue = oldValue || form.getFieldElement(fieldName).oldValue;
    var resetFormFields = valueExistsNotEmpty(oldValue) && newValue !== oldValue;
    switch (fieldName) {
        case 'ta.bl_id':
            if (resetFormFields) {
                if (!valueExistsNotEmpty(newValue)) {
                    form.setFieldValue('ta.geo_region_id', '');
                    form.setFieldValue('ta.site_id', '');
                    form.setFieldValue('ta.pr_id', '');
                    form.setFieldValue('ta.ctry_id', '');
                    form.setFieldValue('ta.regn_id', '');
                    form.setFieldValue('ta.state_id', '');
                    form.setFieldValue('ta.city_id', '');
                }
                form.setFieldValue('ta.fl_id', '');
                form.setFieldValue('ta.rm_id', '');
            }
            break;
        case 'ta.fl_id':
            if (resetFormFields) {
                form.setFieldValue('ta.rm_id', '');
            }
            break;
        case 'ta.ctry_id':
            setGeoRegion(form, fieldName, newValue);
            break;
    }
}
/**
 * Set geographic region.
 * @param form form panel
 * @param fieldName field name
 * @param selectedValue selected value
 */
function setGeoRegion(form, fieldName, selectedValue) {
    if ('ta.ctry_id' === fieldName) {
        if (valueExistsNotEmpty(selectedValue)) {
            var geoRegionId = getGeoRegionId(selectedValue);
            form.setFieldValue('ta.geo_region_id', geoRegionId);
        } else {
            form.setFieldValue('ta.geo_region_id', '');
        }
    }
}

