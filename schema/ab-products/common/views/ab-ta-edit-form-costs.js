var abFurnitureForm_tabCostsController = View.createController('abFurnitureForm_tabCostsController', {
	// selected furniture code
	taId: null,
	// if is new record
	newRecord: false,
	
	// callback method 
	callbackMethod: null,
	
	afterViewLoad: function(){
		if (valueExists(View.getOpenerView()) 
				&& valueExists(View.getOpenerView().parameters)) {
			
			if (valueExists(View.getOpenerView().parameters.callback)) {
				this.callbackMethod = View.getOpenerView().parameters.callback;
			}
		}
	},
	
	afterInitialDataFetch: function () {
		this.newRecord = this.view.newRecord;
		if (valueExists(this.view.restriction)) {
			var restriction = this.view.restriction;
			var clause = restriction.findClause('ta.ta_id');
			if (clause) {
				this.taId = clause.value;
			}
		}
	},

	
	/**
	 * Check if form values were changed.
	 */
	isFieldValueChanged: function(){
		var isFormChanged = isFieldValueChangedOnForm(this.abTaEditForm_Costs);
		if (!isFormChanged) {
			isFormChanged = isFieldValueChangedOnForm(this.abPuchaseInfo);
		}
		if (!isFormChanged) {
			isFormChanged = isFieldValueChangedOnForm(this.abWarrantyInfo);
		}
		return isFormChanged;
	},
	
	/**
	 * Save entire tab content.
	 */
	saveTab: function(){
		return this.abTaEditForm_Costs_onSave();
	},

	abTaEditForm_Costs_afterRefresh: function(){
		// refresh  purchase information form
		this.abPuchaseInfo.refresh(this.abTaEditForm_Costs.restriction, this.abTaEditForm_Costs.newRecord);
		// refresh warranty information form
		this.abWarrantyInfo.refresh(this.abTaEditForm_Costs.restriction, this.abTaEditForm_Costs.newRecord);
	},
	
	abTaEditForm_Costs_onCancel: function(){
		closeFurnitureForm();
	},

	abTaEditForm_Costs_onSave: function(afterSaveCallback) {
		var isSaved = false;
		if (this.abTaEditForm_Costs.canSave() 
				&& this.abPuchaseInfo.canSave() 
				&& this.abWarrantyInfo.canSave()) {
			
			// read abPuchaseInfo and abWarrantyInfo fields
			var purchaseFieldValues = this.abPuchaseInfo.getFieldValues();
			var warrantyFieldValues = this.abWarrantyInfo.getFieldValues();
			
			if (this.abTaEditForm_Costs.save()) {
				isSaved = true;

				// main for was saved --> set purchase info fields
				setFormFields(this.abPuchaseInfo, purchaseFieldValues);
				// main for was saved --> set warranty info fields
				setFormFields(this.abWarrantyInfo, warrantyFieldValues);
				
				if (this.abPuchaseInfo.save()) {
					
					if (this.abWarrantyInfo.save()) {

						if (valueExists(this.callbackMethod)) {
							this.callbackMethod();
						}
						
						afterSaveFurnitureCommon(this.taId);
						
						if(typeof(afterSaveCallback) == 'function'){
							afterSaveCallback(isSaved);
						}
						
						return true;
					}
				} 
			}
			
			if(typeof(afterSaveCallback) == 'function'){
				afterSaveCallback(isSaved);
			}
		}
		return false;
	}
});
