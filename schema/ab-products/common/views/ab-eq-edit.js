
function addNewEquipment(){
	var restriction = new Ab.view.Restriction();
	restriction.addClause("isNewRecord", true, "=");
	showEqDetails(restriction, true);
}

function editEquipment(ctx){
	var eqId = ctx.restriction['eq.eq_id'];
	var restriction = new Ab.view.Restriction();
	restriction.addClause("eq.eq_id", eqId, "=");
	
	showEqDetails(restriction, false);
	
}

function showEqDetails(restriction, newRecord){
	var detailsPanel = View.panels.get('eqDetailsForm');
	
	detailsPanel.parameters.newRecord = newRecord;
	if(detailsPanel){
		detailsPanel.loadView('ab-eq-edit-form.axvw', restriction, null);
	}
}
