<view version="2.0">
	<js file="ab-schema-utils.js"/>
	<js file="ab-eq-edit-form-location.js"/>
	<js file="ab-edit-forms-common.js"/>
	<js url="http://js.arcgis.com/3.9/"/>
	<js file="ab-arcgis-geocoder.js"/>

	<message name="no_match_bl_site" translatable="true">Selected building is assigned to another site</message>
	<message name="msgSourceName_vn">Vendor</message>
	<message name="msgSourceName_contact">Contact</message>
	<message name="msgSourceName_em">Employee</message>

	<dataSource id="ds_abEqEditFormLocation">
		<table name="eq" role="main"/>
		<field name="ctry_id" dataType="text">
			<sql dialect="generic">
				(CASE
				WHEN eq.bl_id IS NOT NULL THEN (SELECT bl.ctry_id FROM bl WHERE eq.bl_id = bl.bl_id)
				WHEN eq.pr_id IS NOT NULL AND eq.bl_id IS NULL THEN (SELECT property.ctry_id FROM property WHERE eq.pr_id = property.pr_id)
				WHEN eq.site_id IS NOT NULL AND eq.bl_id IS NULL AND eq.pr_id IS NULL THEN (SELECT site.ctry_id FROM site WHERE eq.site_id = site.site_id)
				ELSE '' END)
			</sql>
		</field>
		<field name="regn_id" dataType="text">
			<sql dialect="generic">
				(CASE
				WHEN eq.bl_id IS NOT NULL THEN (SELECT bl.regn_id FROM bl WHERE eq.bl_id = bl.bl_id)
				WHEN eq.pr_id IS NOT NULL AND eq.bl_id IS NULL THEN (SELECT property.regn_id FROM property WHERE eq.pr_id = property.pr_id)
				WHEN eq.site_id IS NOT NULL AND eq.bl_id IS NULL AND eq.pr_id IS NULL THEN (SELECT site.regn_id FROM site WHERE eq.site_id = site.site_id)
				ELSE '' END)
			</sql>
		</field>
		<field name="state_id" dataType="text">
			<sql dialect="generic">
				(CASE
				WHEN eq.bl_id IS NOT NULL THEN (SELECT bl.state_id FROM bl WHERE eq.bl_id = bl.bl_id)
				WHEN eq.pr_id IS NOT NULL AND eq.bl_id IS NULL THEN (SELECT property.state_id FROM property WHERE eq.pr_id = property.pr_id)
				WHEN eq.site_id IS NOT NULL AND eq.bl_id IS NULL AND eq.pr_id IS NULL THEN (SELECT site.state_id FROM site WHERE eq.site_id = site.site_id)
				ELSE '' END)
			</sql>
		</field>
		<field name="city_id" dataType="text">
			<sql dialect="generic">
				(CASE
				WHEN eq.bl_id IS NOT NULL THEN (SELECT bl.city_id FROM bl WHERE eq.bl_id = bl.bl_id)
				WHEN eq.pr_id IS NOT NULL AND eq.bl_id IS NULL THEN (SELECT property.city_id FROM property WHERE eq.pr_id = property.pr_id)
				WHEN eq.site_id IS NOT NULL AND eq.bl_id IS NULL AND eq.pr_id IS NULL THEN (SELECT site.city_id FROM site WHERE eq.site_id = site.site_id)
				ELSE '' END)
			</sql>
		</field>
		<field name="eq_id"/>
		<field name="site_id"/>
		<field name="campus_id"/>
		<field name="pr_id"/>
		<field name="bl_id"/>
		<field name="fl_id"/>
		<field name="rm_id"/>
		<field name="loc_column"/>
		<field name="loc_bay"/>
		<field name="lat"/>
		<field name="lon"/>
		<field name="grp_uid"/>
		<field name="org_unit" dataType="text">
			<sql dialect="generic">
				(${sql.getRestriction('siteOrgGeneric', 'NULL')})
			</sql>
            <sql dialect="oracle">
                (${sql.getRestriction('siteOrgOracle', 'NULL')})
            </sql>
		</field>
		<field name="bu_id" dataType="text">
			<sql dialect="generic">
				(SELECT dv.bu_id FROM dv WHERE dv.dv_id = eq.dv_id)
			</sql>
		</field>
		<field name="dv_id"/>
		<field name="dp_id"/>
		<field name="em_id"/>
		<field name="geo_region_id" dataType="text">
			<sql dialect="generic">
				(CASE
				WHEN eq.bl_id IS NOT NULL THEN (SELECT ctry.geo_region_id FROM ctry, bl WHERE ctry.ctry_id = bl.ctry_id AND eq.bl_id = bl.bl_id)
				WHEN eq.pr_id IS NOT NULL AND eq.bl_id IS NULL THEN (SELECT ctry.geo_region_id FROM ctry, property WHERE ctry.ctry_id = property.ctry_id AND eq.pr_id = property.pr_id)
				WHEN eq.site_id IS NOT NULL AND eq.bl_id IS NULL AND eq.pr_id IS NULL THEN (SELECT ctry.geo_region_id FROM ctry, site WHERE ctry.ctry_id = site.ctry_id AND eq.site_id =
				site.site_id)
				ELSE '' END)
			</sql>
		</field>
		<parameter name="useSiteOrg" dataType="boolean" value="false"/>
		<restriction id="siteOrgGeneric" enabled="useSiteOrg" type="sql" sql="(SELECT TOP 1 org.org_id FROM site_org, org WHERE site_org.org_id=org.org_id AND site_org.site_id=eq.site_id ORDER BY org.hierarchy_ids DESC)"/>
		<restriction id="siteOrgOracle" enabled="useSiteOrg" type="sql" sql="(SELECT org_id FROM (SELECT org.org_id, site_org.site_id FROM site_org, org WHERE site_org.org_id=org.org_id ORDER BY org.hierarchy_ids DESC) org WHERE org.site_id=eq.site_id AND rownum=1)"/>
	</dataSource>

	<panel type="form" id="abEqEditForm_Location" dataSource="ds_abEqEditFormLocation" columns="2" showOnLoad="false" collapsed="false">
		<title>Location</title>
		<action id="save" mainAction="true">
			<title translatable="true">Save</title>
		</action>
		<action id="cancel">
			<title translatable="true">Cancel</title>
		</action>
		<action id="locate">
			<title translatable="true">Locate on Map</title>
		</action>
		<field name="eq_id" hidden="true"/>
		<field name="site_id" onchange="onChangeLocationField('eq.site_id', this.value, this.oldValue);this.oldValue = this.value;" onfocus="this.oldValue = this.value;">
			<action id="selectValue_site">
				<title translatable="false">...</title>
				<tooltip>Select Value</tooltip>
				<command type="selectValue" 
				    fieldNames="eq.site_id,eq.city_id,eq.state_id,eq.regn_id,eq.ctry_id" 
				    selectFieldNames="site.site_id,site.city_id,site.state_id,site.regn_id,site.ctry_id" 
				    visibleFieldNames="site.site_id,site.name,site.city_id,site.state_id,site.regn_id,site.ctry_id" 
				    actionListener="afterSelectSite">
					<title>Site Code</title>
				</command>
			</action>
		</field>
		<field name="geo_region_id" dataType="text" readOnly="true">
			<title translatable="true">Geo-Region Code</title>
		</field>
		<field name="campus_id">
			<tooltip>Section Detail Location in Builder SMS</tooltip>
		</field>
		<field name="ctry_id" dataType="text" readOnly="true">
			<title translatable="true">Country Code</title>
		</field>
		<field name="pr_id" onchange="onChangeLocationField('eq.pr_id', this.value, this.oldValue);this.oldValue = this.value;" onfocus="this.oldValue = this.value;">
			<action id="selectValue_property">
				<title translatable="false">...</title>
				<tooltip>Select Value</tooltip>
				<command type="selectValue" 
				    fieldNames="eq.pr_id,eq.site_id,eq.city_id,eq.state_id,eq.regn_id,eq.ctry_id" 
				    selectFieldNames="property.pr_id,property.site_id,property.city_id,property.state_id,property.regn_id,property.ctry_id" 
				    visibleFieldNames="property.pr_id,property.name,property.site_id,property.city_id,property.state_id,property.regn_id,property.ctry_id" 
				    actionListener="afterSelectProperty">
					<title>Property Code, Site Code</title>
				</command>
			</action>
		</field>
		<field name="regn_id" dataType="text" readOnly="true">
			<title translatable="true">Region Code</title>
		</field>
		<field name="bl_id" onchange="onChangeLocationField('eq.bl_id', this.value, this.oldValue);this.oldValue = this.value;" onfocus="this.oldValue = this.value;">
			<action id="selectValue_building">
				<title translatable="false">...</title>
				<tooltip>Select Value</tooltip>
				<command type="selectValue" 
				    fieldNames="eq.bl_id,eq.pr_id,eq.site_id,eq.city_id,eq.state_id,eq.regn_id,eq.ctry_id" 
				    selectFieldNames="bl.bl_id,bl.pr_id,bl.site_id,bl.city_id,bl.state_id,bl.regn_id,bl.ctry_id" 
				    visibleFieldNames="bl.bl_id,bl.name,bl.pr_id,bl.site_id,bl.city_id,bl.state_id,bl.regn_id,bl.ctry_id" 
				    actionListener="afterSelectBuilding">
					<title>Building Code, Property Code, Site Code</title>
				</command>
			</action>
		</field>
		<field name="state_id" dataType="text" readOnly="true">
			<title translatable="true">State Code</title>
		</field>
		<field name="fl_id" onchange="onChangeLocationField('eq.fl_id', this.value, this.oldValue);this.oldValue = this.value;" onfocus="this.oldValue = this.value;">
			<action id="selectValue_floor">
				<title translatable="false">...</title>
				<tooltip>Select Value</tooltip>
				<command type="selectValue" 
				    fieldNames="eq.bl_id,eq.fl_id" 
				    selectFieldNames="fl.bl_id,fl.fl_id" 
				    visibleFieldNames="fl.bl_id,fl.fl_id,fl.name" 
				    actionListener="afterSelectFloor">
					<title>Building Code, Floor Code</title>
				</command>
			</action>
		</field>
		<field name="city_id" dataType="text" readOnly="true">
			<title translatable="true">City Code</title>
		</field>
		<field name="rm_id" onchange="onChangeLocationField('eq.rm_id', this.value, this.oldValue);this.oldValue = this.value;" onfocus="this.oldValue = this.value;">
			<action id="selectValue_room">
				<title translatable="false">...</title>
				<tooltip>Select Value</tooltip>
				<command type="selectValue" 
				    fieldNames="eq.bl_id,eq.fl_id,eq.rm_id" 
				    selectFieldNames="rm.bl_id,rm.fl_id,rm.rm_id" 
				    visibleFieldNames="rm.bl_id,rm.fl_id,rm.rm_id,rm.rm_type,bl.name" 
				    actionListener="afterSelectRoom">
					<title>Building Code, Floor Code, Room Code</title>
				</command>
			</action>
		</field>
		<field name="lat"/>
		<field name="loc_column"/>
		<field name="lon"/>
		<field name="loc_bay"/>
		<field name="grp_uid"/>
	</panel>

	<panel type="form" id="abEqEditForm_affiliation" dataSource="ds_abEqEditFormLocation" columns="1" showOnLoad="false" collapsed="true">
		<title>Affiliation</title>
		<action id="save">
			<title>Save</title>
		</action>
		<field name="eq_id" hidden="true"/>
		<field name="org_unit" dataType="text" readOnly="true">
			<title>Organizational Unit </title>
		</field>
		<field name="bu_id" dataType="text" readOnly="true">
			<title translatable="true">Business Unit</title>
		</field>
		<field name="dv_id"/>
		<field name="dp_id"/>
		<field table="em" name="em_id"/>
	</panel>
	<dataSource id="ds_abEqEditFormCustodian">
		<table name="team" role="main"/>
        <table name="em" role="standard"/>
        <table name="contact" role="standard"/>
        <table name="vn" role="standard"/>
		<field table="team" name="autonumbered_id"/>
		<field table="team" name="custodian_name" dataType="text">
			<title>Custodian Name</title>
			<sql dialect="generic">
                (CASE
                    WHEN team.source_table = 'vn' AND vn.company IS NOT NULL THEN vn.company
                    WHEN team.source_table = 'vn' THEN team.vn_id
                    WHEN team.source_table = 'contact' AND (contact.name_first IS NOT NULL OR contact.name_last IS NOT NULL) THEN (contact.name_first ${sql.concat} ' ' ${sql.concat} contact.name_last)
                    WHEN team.source_table = 'contact' THEN team.contact_id
                    WHEN team.source_table = 'em' AND em.name IS NOT NULL THEN em.name
                    WHEN team.source_table = 'em' AND em.name IS NULL AND (em.name_first IS NOT NULL OR em.name_last IS NOT NULL) THEN (em.name_first ${sql.concat} ' ' ${sql.concat} em.name_last)
                    WHEN team.source_table = 'em' THEN team.em_id
                ELSE NULL END)
			</sql>
		</field>
		<field table="team" name="source_table"/>
		<field table="team" name="custodian_type"/>
		<field table="team" name="status"/>
		<field table="team" name="team_type"/>
		<field table="team" name="em_id"/>
		<field table="team" name="vn_id"/>
		<field table="team" name="contact_id"/>
		<field table="team" name="eq_id"/>
		<field table="team" name="is_owner"/>
		<field table="team" name="date_start"/>
		<field table="team" name="date_end"/>
        <field table="contact" name="contact_id"/>
        <field table="vn" name="vn_id"/>
        <field table="em" name="em_id"/>
		<restriction type="parsed">
			<clause table="team" name="team_type" value="Equipment" op="=" relop="and"/>
		</restriction>
	</dataSource>

	<panel type="grid" id="abEqEditForm_ownerCustodian" dataSource="ds_abEqEditFormCustodian" showOnLoad="false" collapsed="true">
		<sortField table="team" name="status" ascending="true"/>
		<sortField table="team" name="date_start" ascending="false"/>
		<title>Owner Custodians</title>
		<action id="addOwnerCustodian" imageName="/schema/ab-core/graphics/icons/view/add-icon.png">
			<tooltip>Add</tooltip>
		</action>
		<action type="menu" id="exportOwnerCustodian" imageName="/schema/ab-core/graphics/icons/view/export.png">
			<tooltip>Export</tooltip>
			<action id="docx">
				<title>DOCX</title>
				<command type="exportPanel" panelId="abEqEditForm_ownerCustodian" outputType="docx" orientation="landscape"/>
			</action>
			<action id="xls">
				<title>XLS</title>
				<command type="exportPanel" panelId="abEqEditForm_ownerCustodian" outputType="xls"/>
			</action>
		</action>
		<field id="editRow" controlType="image" imageName="/schema/ab-core/graphics/icons/view/edit.png" onclick="abEquipmentForm_tabLocationController.onEditOwnerCustodian" width="25px" style="width:25px;">
			<tooltip translatable="true">Edit</tooltip>
		</field>
		<field table="team" name="custodian_name" dataType="text">
			<title>Custodian Name</title>
		</field>
		<field name="custodian_type"/>
		<field name="status">
			<title>Custodian Status</title>
		</field>
		<field name="date_start">
			<title>Date Start</title>
		</field>
		<field name="date_end">
			<title>Date End</title>
		</field>
		<field name="autonumbered_id"/>
	</panel>

	<panel type="grid" id="abEqEditForm_otherCustodian" dataSource="ds_abEqEditFormCustodian" showOnLoad="false" collapsed="true">
		<sortField table="team" name="status" ascending="true"/>
		<sortField table="team" name="date_start" ascending="false"/>
		<title>Other Custodians</title>
		<action id="addOtherCustodian" imageName="/schema/ab-core/graphics/icons/view/add-icon.png">
			<tooltip>Add</tooltip>
		</action>
		<action type="menu" id="exportOtherCustodian" imageName="/schema/ab-core/graphics/icons/view/export.png">
			<tooltip>Export</tooltip>
			<action id="docx">
				<title>DOCX</title>
				<command type="exportPanel" panelId="abEqEditForm_otherCustodian" outputType="docx" orientation="landscape"/>
			</action>
			<action id="xls">
				<title>XLS</title>
				<command type="exportPanel" panelId="abEqEditForm_otherCustodian" outputType="xls"/>
			</action>
		</action>
		<field id="editRow" controlType="image" imageName="/schema/ab-core/graphics/icons/view/edit.png" onclick="abEquipmentForm_tabLocationController.onEditOtherCustodian" width="25px" style="width:25px;">
			<tooltip translatable="true">Edit</tooltip>
		</field>
		<field table="team" name="custodian_name" dataType="text">
			<title>Custodian Name</title>
		</field>
		<field name="custodian_type"/>
		<field name="status">
			<title>Custodian Status</title>
		</field>
		<field name="date_start">
			<title>Date Start</title>
		</field>
		<field name="date_end">
			<title>Date End</title>
		</field>
		<field name="autonumbered_id"/>
	</panel>


</view>