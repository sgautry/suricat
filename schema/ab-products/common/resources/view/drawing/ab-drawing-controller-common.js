View.createController('assetDrawingController', {
    /**
     * Refresh drawing when drawing config exists and passed as parameter (i.e. maximize)
     */
    afterInitialDataFetch: function () {
        var drawingConfig = null, drawings = null;
        if (valueExists(this.view.parameters) && valueExists(this.view.parameters.drawingConfig) && valueExists(this.view.parameters.drawings)) {
            drawingConfig = this.view.parameters.drawingConfig;
            drawings = this.view.parameters.drawings;
        } else if (valueExists(this.view.parentTab) && valueExists(this.view.parentTab.drawingConfig) && valueExists(this.view.parentTab.drawings)) {
            drawingConfig = this.view.parentTab.parameters.drawingConfig;
            drawings = this.view.parentTab.parameters.drawings;
        } else if (valueExists(this.view.parentViewPanel) && valueExists(this.view.parentViewPanel.drawingConfig) && valueExists(this.view.parentViewPanel.drawings)) {
            drawingConfig = this.view.parentViewPanel.drawingConfig;
            drawings = this.view.parentViewPanel.drawings;
        }
        if (drawingConfig && drawings) {
            var drawingControl = new AssetDrawingControl(drawingConfig);
            // Add event listeners to the control
            if (drawingConfig.listeners) {
                _.each(_.keys(drawingConfig.listeners), function (eventName) {
                    var listener = drawingConfig.listeners[eventName];
                    drawingControl.addEventListener(eventName, listener);
                });
            }
            drawingControl.showDrawings(drawings);
        }
    }
});
/**
 * Construct a Asset Drawing control.
 * @typedef {Object} AssetDrawingControl
 * @property {Object} config - Defines a configuration object passed from different applications which contains varies properties of the drawing.
 * @constructor
 */
var AssetDrawingControl = Base.extend({
        //Drawing options and actions
        config: null,
        //Drawing parameters.
        drawingParameters: null,
        //Drawing panel object
        drawingPanel: null,
        // A map of selected drawings
        drawings: {},
        //SVG drawing controller
        svgControl: null,
        //Drawing state by default is select. Useful to make distinction between several actions
        drawingState: 'select',
        //Select asset highlight color
        selectAssetHighlightColor: '#00ffff',
        // Default room highlight cssClass
        defaultRoomHighlightCss: 'zoomed-asset-default',
        //Default highlight asset color
        highlightAssetColor: '#FF9632',
        //List of connected assets
        connectAssets: {assetFrom: {}, assetTo: {}},
        //List of selected assets to query asset data
        selectedAssets: {},
        //Exists selected assets to query; set when query data action is done.
        existsAssetsToQuery: false,
        //Event Listeners
        eventListeners: null,
        //Drawing view container, used to maximize the view
        viewName: 'ab-drawing-controller-common.axvw',
        /**
         * @param {string} config.panel - Drawing panel.
         * @param {string} [config.blId] - Building id.
         * @param {string} [config.flId] - Floor id.
         * @param {string} [config.rmId] - Will highlight the room.
         * @param {string} [config.highlightAssetId] - Will highlight the asset.
         * @param {boolean} [config.zoom=true] - Zoom to room.
         * @param {boolean} [config.multipleSelectionEnabled=false] - Allows to select multiple assets.
         * @param {boolean} [config.showNavigation=true] - Display drawing navigation.
         * @param {boolean} [config.showPanelTitle=true] - Display panel title after the drawing is loaded; Title: Plan for building {0} and floor {1}.
         * @param {boolean} [config.print=false] - Display drawing print action.
         * @param {boolean} [config.reset=false] - Display drawing reset action.
         * @param {boolean} [config.maximize={showAsDialog=false, - Show view as dialog or not.
         *                                    viewName='ab-drawing-controller-common.axvw' - Custom view name to open drawing when maximize
         *                                    expand=function, - Expand view layout
         *                                    collapse=function}] - Collapse view layout
         * @param {string} [config.selectAssetHighlightColor='#00ffff'] - Highlight asset color when asset is selected.
         */
        constructor: function (config) {
            this.config = config;
            this.drawings = {};
            this.drawingParameters = this._createParameters(config);
            this.svgControl = new Drawing.DrawingControl(this.config.container, this.config.panel, this.drawingParameters);
            this._setDiagonalSelection();
            this.drawingPanel = View.panels.get(config.panel);
            this.selectAssetHighlightColor = this.config.selectAssetHighlightColor || '#00ffff';
            this.eventListeners = new Ext.util.MixedCollection(true);
            this._initActions();
        },
        /**
         * Show drawing.
         * @typedef {Object} drawing
         * @property {Object}  drawing.pkeyValues - Building id and floor id..
         * @property {string}  [drawing.drawingName] - Drawing name.
         * @property {Array.<Object>}  [drawing.findAssets] - List of assets to find.
         * @property {Array.<Object>}  [drawing.highlightAssets] - List of assets to highlight.
         * @example
         * {pkeyValues: {blId: blId, flId: flId},
         *  drawingName: drawingName,
         *  findAssets: [{assetType: 'rm',assetIds: [rmId],highlightCss: 'zoomed-asset-red-bordered',zoom: false}],
         *  highlightAssets: [{assetType: 'rm',assetIds: [rmId],color: 'blue']}}
         */
        showDrawing: function (drawing) {
            this.showDrawings([drawing]);
        },
        /**
         * Show drawing.
         * @param {Array.<drawing>} drawings - List of drawings.
         */
        showDrawings: function (drawings) {
            if (drawings) {
                this._setDrawingsParameters(drawings);
            }
            var showMultipleDrawings = drawings.length > 1;
            this._showDrawings(showMultipleDrawings);
        },
        /**
         * Unload a specific drawing.
         * @param {string} blId - building id.
         * @param {string} flId - floor id.
         * @param {string} [drawingName] - Drawing name. Optional, to be specified when exists more drawing on the same building and floor
         */
        unloadDrawing: function (blId, flId, drawingName) {
            var dwgName = drawingName || (valueExists(this.getDrawingConfiguration(blId, flId)) ? this.getDrawingConfiguration(blId, flId).drawingName : null);
            if (dwgName) {
                this.resetDrawingHighlights();
                this._unloadSvg(blId, flId, dwgName);
                var svgId = DrawingCommon.retrieveValidSvgId(this.config.container, dwgName);
                delete this.drawings[svgId];
                this._resetSelectedAssets();
                this._setPanelTitle();
                this.svgControl.getAddOn('InfoWindow').show(false);
                if (_.isEmpty(this.drawings)) {
                    this.clearDrawingPanel();
                }
            }
        },
        /**
         * Get drawing config parameters.
         * @returns {Array.<Object>} - Existing drawings parameters.
         */
        getDrawingsConfigParameters: function () {
            var drawingParams = [];
            _.each(this.drawings, function (drawingConfig) {
                drawingParams.push({
                    pkeyValues: drawingConfig.pkeyValues,
                    findAssets: drawingConfig.findAssets,
                    highlightAssets: drawingConfig.highlightAssets
                });
            });
            return drawingParams;
        },
        /**
         * Get drawing object by blId and flId.
         * @param {string} blId - Building id.
         * @param {string} flId - Floor id.
         */
        getDrawingConfiguration: function (blId, flId) {
            return _.find(this.drawings, function (drawing) {
                return drawing.pkeyValues.blId == blId && drawing.pkeyValues.flId == flId;
            });
        },
        /**
         * Set drawing panel title.
         * @param {string} title
         */
        setDrawingPanelTitle: function (title) {
            this.drawingPanel.setTitle(title);
        },
        /**
         * Add drawing panel action.
         * @param {Object} config - Action config
         */
        addAction: function (config) {
            if (config == null) {
                return;
            }
            if (this.drawingPanel.actions.get(config.id) != undefined) {
                this.drawingPanel.actions.get(config.id).setTitle(config.text);
            } else {
                this.drawingPanel.actions.add(config.id, new Ab.view.Action(this.drawingPanel, config));
            }
        },
        /**
         * Pass runtime restriction to highlight dataSource.
         * @param {Ab.view.Restriction} restriction
         */
        addHighlightParameterRestriction: function (restriction) {
            _.extend(this.drawingParameters['highlightParameters'][0], {
                'hs_rest': toJSON(restriction)
            });
        },
        /**
         * Pass runtime values to highlight dataSource parameters.
         * @param {Object} parameter
         */
        addHighlightDataSourceParameter: function (parameter) {
            _.extend(this.drawingParameters['highlightParameters'][0], {
                'hs_param': toJSON(parameter)
            });
        },
        /**
         * Show drawing panel actions.
         * @param {boolean} show
         */
        showDrawingPanelActions: function (show) {
            var reports = this.drawingPanel.actions.get('reports');
            if (reports) {
                reports.show(show);
            }
            var maximize = this.drawingPanel.actions.get('maximize');
            if (maximize) {
                maximize.show(show);
            }
            var reset = this.drawingPanel.actions.get('reset');
            if (reset) {
                reset.show(show);
            }
            var print = this.drawingPanel.actions.get('print');
            if (print) {
                print.show(show);
            }
        },
        /**
         * Reset selected assets.
         */
        resetSelectedDrawingAssets: function (svgId) {
            this.selectedAssets[svgId] = {};
        },
        /**
         * Reset drawings selected assets.
         */
        resetDrawingsSelectedAssets: function () {
            _.each(_.keys(this.selectedAssets), function (svgId) {
                this.resetSelectedDrawingAssets(svgId);
            }, this);
        },
        /**
         * Clear drawing panel with actions
         */
        clearDrawingPanel: function () {
            this.clearDrawings();
            this._resetSelectedAssets();
            this.showDrawingPanelActions(false);
        },
        /**
         * Clear drawing content.
         */
        clearDrawings: function () {
            this._unloadDrawings();
            this.disableDataSourceSelector(true);
        },
        /**
         * Reset all drawing highlights and set drawing state to select.
         */
        resetHighlight: function () {
            this.svgControl.getDrawingController().getController("HighlightController").resetAll();
            this.drawingState = 'select';
        },
        /**
         * Clear highlights for previously found assets from the filter console.
         */
        clearFilterAssets: function () {
            this.svgControl.getAddOn('AssetLocator').clearFoundAssets();
        },
        /**
         * Reset drawing highlight and clear asset tab restrictions.
         */
        resetDrawingHighlights: function () {
            //reset drawing highlight
            this.resetHighlight();
            // clear highlights for previously found assets
            this.clearFilterAssets();
        },
        /**
         * Disable data-source selector.
         * @param {boolean} disable
         */
        disableDataSourceSelector: function (disable) {
            jQuery('#selector_labels').attr('disabled', disable);
            jQuery('#selector_hilite').attr('disabled', disable);
            jQuery('#selector_IBorders').attr('disabled', disable);
        },
        /**
         * Adds a listener to specified control event.
         * @param {string} eventName - Name of the control event.
         * @param {Function} listener - Listener function reference.
         */
        addEventListener: function (eventName, listener) {
            if (this.eventListeners.containsKey(eventName)) {
                this.eventListeners.replace(eventName, listener);
            } else {
                this.eventListeners.add(eventName, listener);
            }
        },
        /**
         * Returns registered event listener.
         * @param {string} eventName - Event name, specific to the control type.
         */
        getEventListener: function (eventName) {
            var listener = this.eventListeners.get(eventName);
            if (!valueExists(listener)) {
                listener = null;
            }
            return listener;
        },
        getEventListeners: function () {
            return this.eventListeners.map;
        },
        /**
         * Removes listeners from specified control event.
         * @param {string} eventName - The name of the control event.
         */
        removeEventListener: function (eventName) {
            this.eventListeners.removeKey(eventName);
        },
        /**
         * Sets the control height to specified value.
         * @param {number} height: The height in pixels.
         */
        setHeight: function (height) {
        },
        /**
         * Show select assets on info window.
         */
        displaySelectWindowResult: function () {
            this.svgControl.getAddOn('InfoWindow').setText('Selected assets:');
            _.each(_.keys(this.selectedAssets), function (svgId) {
                this.svgControl.getAddOn('InfoWindow').appendText('<br><br>Svg id: ' + svgId);
                var selectedSvgAssets = this.selectedAssets[svgId];
                _.each(_.keys(selectedSvgAssets), function (assetType) {
                    this.svgControl.getAddOn('InfoWindow').appendText('<br>No. of selected ' + assetType + ': ' + selectedSvgAssets[assetType].length);
                }, this);
            }, this);
        },
        /**
         * @typedef {Object} drawingParameters
         * @property {Object} config - Configuration object passed from AssetDrawingControl.
         * @property {string} config.divId - Id of root <<div/> that holds all single svg drawing.
         * @property {Array} config.assetTypes - List of asset types; default click and contextmenu event is added for each asset.
         * @property {boolean} [config.allowMultipleDrawings=false] - Allows multiple drawings.
         * @property {boolean} [config.multipleSelectionEnabled=false] - Allows to select multiple assets.
         * @property {boolean} [config.bordersHighlightSelector=false] - Show border highlight selector.
         * @property {boolean} [config.borderSize=20] - Border highlight selector size.
         * @property {boolean} [config.showNavigation=true] - Display drawing navigation.
         * @property {LayersPopup} [config.layersPopup] - Set custom drawing layers.
         * @property {Array.<HighlightParameters>} [config.highlightParameters] - Set custom drawing highlight parameters; Contains the highlight and labels data-sources as well as the default one
         * @property {Array.<AssetTooltip>} [config.assetTooltip] - Set drawing asset tooltips.
         * @property {Array.<InfoWindow>} [config.infoWindow] - Set info window properties.
         * @returns {Ab.view.ConfigObject} Creates drawing parameters.
         * @private
         */
        _createParameters: function (config) {
            // define parameters to be used by server-side job
            var drawingParameters = new Ab.view.ConfigObject();
            drawingParameters['divId'] = config.container;
            if (config.topLayers) {
                drawingParameters['topLayers'] = config.topLayers;
            }
            drawingParameters['allowMultipleDrawings'] = valueExists(config.allowMultipleDrawings) ? (config.allowMultipleDrawings ? 'true' : 'false') : 'false';
            if (valueExists(config.allowMultipleDrawings) && config.allowMultipleDrawings) {
                if (valueExists(config.orderByColumn)) {
                    drawingParameters['orderByColumn'] = valueExists(config.orderByColumn) ? (config.orderByColumn ? 'true' : 'false') : 'true';
                }
            }
            drawingParameters['multipleSelectionEnabled'] = config.multipleSelectionEnabled ? 'true' : 'false';
            drawingParameters['addOnsConfig'] = {
                'AssetLocator': {divId: config.container}
            };
            if (config.showNavigation || true) {
                _.extend(drawingParameters['addOnsConfig'], {
                    'NavigationToolbar': {
                        divId: config.container
                    }
                });
            }
            /**
             * Set custom drawing highlight parameters; Contains the highlight and labels data-sources as well as the default one.
             * @typedef {Object} HighlightParameters
             * @property {string}  view_file - View file name.
             * @property {string}  hs_ds - Highlight data source.
             * @property {string}  label_ds - Label data source.
             * @property {string}  label_ht - Label height.
             * @property {boolean}  [showSelector=true] - show DataSourceSelector for Highlights and Labels.
             * @example
             * highlightParameters: [{
         *  'view_file': "ab-drawing-controller-common-ds.axvw",
         *  'hs_ds': "highlightDivisionsDs",
         *  'label_ds': 'labelNamesDs',
         *  'label_ht': '0.60',
         *  showSelector: false
         * }],
             */
            var highlightParameters = config.highlightParameters;
            if (highlightParameters) {
                drawingParameters['highlightParameters'] = highlightParameters;
                _.extend(drawingParameters['addOnsConfig'], {
                    'DatasourceSelector': {
                        panelId: highlightParameters[0].panelId || config.panel
                    }
                });
                var showSelector = getValueIfExists(highlightParameters[0].showSelector, true);
                if (showSelector) {
                    //enable legend panels
                    drawingParameters['legendPanel'] = config.legendPanel || 'legendGrid';
                    drawingParameters['borderLegendPanel'] = config.borderLegendPanel || 'borderLegendGrid';
                    //set custom text label for the legend panel.
                    View.panels.get(drawingParameters['legendPanel']).afterCreateCellContent = this._renderLegendLabel.createDelegate(this);
                    View.panels.get(drawingParameters['borderLegendPanel']).afterCreateCellContent = this._renderLegendLabel.createDelegate(this);
                    drawingParameters['bordersHighlightSelector'] = config.bordersHighlightSelector || false;
                    if (config.borderSize) {
                        drawingParameters['borderSize'] = config.borderSize;
                    }
                } else {
                    //TODO - for the moment this has no affect until the core control is fixed;
                    //TODO - add this parameters to highlightParameters[0]
                    drawingParameters['highlightSelector'] = false;
                    drawingParameters['labelSelector'] = false;
                    drawingParameters['bordersHighlightSelector '] = false;
                }
            }
            /**
             * Set custom drawing layers.
             * @typedef {Object} LayersPopup
             * @property {string}  layers - Available drawing layers.
             * @property {string}  defaultLayers - Default selected layers.
             * @example
             * layersPopup: {
         *   layers: "rm-assets;rm-labels;eq-assets;eq-labels;jk-assets;jk-labels;fp-assets;fp-labels;pn-assets;pn-labels;gros-assets;gros-labels;background",
         *   defaultLayers: "rm-assets;rm-labels;eq-assets;jk-assets;fp-assets;pn-assets;gros-assets"
         * }
             */
            var layersPopup = config.layersPopup;
            if (layersPopup) {
                _.extend(layersPopup, {divId: config.container});
                _.extend(drawingParameters['addOnsConfig'], {
                    'LayersPopup': layersPopup
                });
            }
            // Set events on each defined asset type.
            if (valueExists(config.assetTypes)) {
                var events = [];
                var onClickAsset = this._onClickAsset.createDelegate(this),
                    onContextMenuAsset = this._onContextMenuAsset.createDelegate(this);
                for ( var i = 0; i < config.assetTypes.length; i++ ) {
                    events.push({'eventName': 'click', 'assetType': config.assetTypes[i], 'handler': onClickAsset});
                    events.push({'eventName': 'contextmenu', 'assetType': config.assetTypes[i], 'handler': onContextMenuAsset});
                }
                drawingParameters['events'] = events;
                // On select window event
                _.extend(drawingParameters['addOnsConfig'], {
                    'SelectWindow': {
                        assetType: config.assetTypes.join(';'),
                        customEvent: this._onSelectWindow.createDelegate(this)
                    }
                });
            }
            /**
             * Set drawing asset tooltips.
             * @typedef {Object} AssetTooltip
             * @property {'rm'|'eq'|'jk'|'fp'|'pn'}  assetType - Asset type.
             * @property {string}  datasource - Available view data source.
             * @property {string}  fields - Database fields separated by ';'.
             * @example
             * assetTooltip: [
             *  {assetType: 'rm', datasource: 'tooltipRmDs', fields: 'rm.rm_id;rm.rm_std'},
             *  {assetType: 'eq', datasource: 'tooltipEqDs', fields: 'eq.eq_id;eq.eq_std;'},
             *  {assetType: 'jk', datasource: 'tooltipJkDs', fields: 'jk.jk_id;jk.jk_std'},
             *  {assetType: 'fp', datasource: 'tooltipFpDs', fields: 'fp.fp_id;fp.fp_std'},
             *  {assetType: 'pn', datasource: 'tooltipPnDs', fields: 'pn.pn_id;pn.pn_std'}
             * ]
             */
            var assetTooltip = config.assetTooltip;
            if (assetTooltip) {
                drawingParameters['showTooltip'] = 'true';
                _.extend(drawingParameters['addOnsConfig'], {
                    'AssetTooltip': {
                        handlers: assetTooltip
                    }
                });
            }
            /**
             * Set info window properties.
             * @typedef {Object} InfoWindow
             * @property {string}  width - with of the info window
             * @property {'top'|'bottom'}  position=bottom - Position.
             * @property {Function}  customEvent - Custom close info window. By default, it sets the drawingState=select.
             * @example
             * infoWindow: {
         *   width: '400px',
         *   position: 'top',
         *   customEvent: customCloseInfoWindow
         * }
             */
            var infoWindow = {
                width: '400px',
                position: 'bottom',
                customEvent: this._onCloseInfoWindow
            };
            if (config.infoWindow) {
                infoWindow = config.infoWindow;
            }
            _.extend(drawingParameters['addOnsConfig'], {
                'InfoWindow': infoWindow
            });
            return drawingParameters;
        },
        /**
         * Set drawings param.
         * @param {Array.<Object>} drawings - [{blId, flId, rmId, highlightAssetId, drawingName}]
         * @private
         */
        _setDrawingsParameters: function (drawings) {
            if (this.drawingParameters.allowMultipleDrawings !== 'true') {
                this.drawings = {};
            }
            for ( var i = 0; i < drawings.length; i++ ) {
                var pkeyValues = drawings[i].pkeyValues,
                    findAssets = drawings[i].findAssets,
                    highlightAssets = drawings[i].highlightAssets;
                var blId = pkeyValues.blId,
                    flId = pkeyValues.flId,
                    drawingName = drawings[i].drawingName;
                if (valueExistsNotEmpty(blId) && valueExistsNotEmpty(flId) && !valueExistsNotEmpty(drawingName)) {
                    var dwgRecord = this._getDrawingRecord(blId, flId);
                    drawingName = dwgRecord.getValue("rm.dwgname");
                }
                if (valueExistsNotEmpty(drawingName)) {
                    var svgId = DrawingCommon.retrieveValidSvgId(this.config.container, drawingName);
                    this.drawings[svgId] = {
                        pkeyValues: pkeyValues,
                        drawingName: drawingName,
                        findAssets: findAssets,
                        highlightAssets: highlightAssets
                    };
                }
            }
        },
        /**
         * Show drawing for each drawings.
         * @param {boolean} showMultipleDrawings - Show multiple drawings at once.
         * @private
         */
        _showDrawings: function (showMultipleDrawings) {
            if (_.isEmpty(this.drawings)) {
                this.svgControl.getDrawingController().getControl().unloadAll();
                this.clearDrawingPanel();
                this._setPanelTitle();
                this.svgControl.getAddOn('InfoWindow').show(false);
                this.clearFilterAssets();
                return;
            }
            _.each(this.drawings, function (drawingConfig, svgId) {
                this._showDrawing(drawingConfig, svgId);
            }, this);
            this._setPanelTitle();
            if (showMultipleDrawings) {
                var afterDrawingsLoaded = this.getEventListener('afterDrawingsLoaded');
                if (afterDrawingsLoaded && typeof afterDrawingsLoaded == 'function') {
                    afterDrawingsLoaded(this, this.drawings);
                }
            }
        },
        /**
         * Apply filter restriction and load drawing if found.
         * @private
         */
        _showDrawing: function (drawingConfig, svgId) {
            var pkeyValues = drawingConfig.pkeyValues;
            if (valueExistsNotEmpty(drawingConfig.drawingName)) {
                this._loadDrawing(drawingConfig, svgId);
                this.disableDataSourceSelector(false);
                this.showDrawingPanelActions(true);
            } else {
                View.showMessage(String.format(getMessage("noDrawingFoundBlFl"), pkeyValues.blId, pkeyValues.flId));
                this.clearDrawingPanel();
            }
        },
        /**
         * Drawing restriction.
         * @param {string} blId - Building id.
         * @param {string} flId - Floor id.
         * @returns {Ab.view.Restriction}
         * @private
         */
        _createDrawingRestriction: function (blId, flId) {
            var restriction = new Ab.view.Restriction();
            if (valueExistsNotEmpty(blId)) {
                restriction.addClause('rm.bl_id', blId);
            }
            if (valueExistsNotEmpty(flId)) {
                restriction.addClause('rm.fl_id', flId);
            }
            return restriction;
        },
        /**
         * Load svg drawing and locate room and highlight asset.
         * @param {Object} drawingConfig - Drawing parameters.
         * @param {string} svgId - Drawing svg id.
         * @private
         */
        _loadDrawing: function (drawingConfig, svgId) {
            var drawingSvgControl = this.svgControl.getSvgControl(drawingConfig.drawingName);
            // check if the drawing is already displayed
            if (valueExists(drawingSvgControl) && drawingSvgControl.isDrawingLoaded) {
                this._applyAssets(drawingConfig, svgId);
            } else {
                // reset highlighted assets
                this.svgControl.getDrawingController().getController("HighlightController").highlightedAssets = {};
                this._loadSvg(drawingConfig);
                this._applyAssets(drawingConfig, svgId);
                var afterDrawingLoaded = this.getEventListener('afterDrawingLoaded');
                if (afterDrawingLoaded && typeof afterDrawingLoaded == 'function') {
                    afterDrawingLoaded(this, svgId);
                }
            }
        },
        _applyAssets: function (drawingConfig, svgId) {
            // reset selected assets
            this.resetSelectedDrawingAssets(svgId);
            var pkeyValues = drawingConfig.pkeyValues;
            if (valueExistsNotEmpty(drawingConfig.findAssets)) {
                this._findAssets(pkeyValues.blId, pkeyValues.flId, drawingConfig.findAssets, svgId);
            }
            if (valueExistsNotEmpty(drawingConfig.highlightAssets)) {
                this._highlightAssets(pkeyValues.blId, pkeyValues.flId, drawingConfig.highlightAssets, svgId);
            }
        },
        /**
         * Loads svg drawing to panel div element and sets the drawing parameters.
         * @private
         */
        _loadSvg: function (drawingConfig) {
            var parameters = new Ab.view.ConfigObject();
            parameters['pkeyValues'] = {'bl_id': drawingConfig.pkeyValues.blId, 'fl_id': drawingConfig.pkeyValues.flId};
            if (valueExistsNotEmpty(drawingConfig.pkeyValues.filename)) {
                parameters['pkeyValues'].filename = drawingConfig.pkeyValues.filename;
            }
            parameters['drawingName'] = drawingConfig.drawingName;
            if (this.drawingParameters.allowMultipleDrawings === 'true') {
                parameters['orderByColumn'] = this.drawingParameters.orderByColumn;
            }
            this.svgControl.load(parameters);
        },
        /**
         * Set drawing panel title for singular or multiple drawings.
         * @private
         */
        _setPanelTitle: function () {
            if (this.config.showPanelTitle) {
                var customDrawingPanelTitleEvent = this.getEventListener('customPanelTitle');
                if (customDrawingPanelTitleEvent && typeof customDrawingPanelTitleEvent == 'function') {
                    customDrawingPanelTitleEvent(this, this.drawings);
                } else {
                    var dwgPanelTitle = getMessage('noDrawingFound');
                    if (_.keys(this.drawings).length == 1) {
                        var pkeyValues = _.values(this.drawings)[0].pkeyValues;
                        dwgPanelTitle = String.format(getMessage("dwgTitle"), pkeyValues.blId, pkeyValues.flId);
                    } else if (_.keys(this.drawings).length > 1) {
                        dwgPanelTitle = getMessage("dwgMultipleTitle");
                    }
                    this.setDrawingPanelTitle(dwgPanelTitle);
                }
            }
        },
        _findAssets: function (blId, flId, assets, svgId) {
            for ( var i = 0; i < assets.length; i++ ) {
                this._findAsset(blId, flId, assets[i], svgId);
            }
        },
        /**
         * Find room on drawing.
         * @param {string} blId - building id.
         * @param {string} flId - floor id.
         * @param {Object} asset - Asset object.
         * @param {string} svgId - Svg id.
         * @private
         */
        _findAsset: function (blId, flId, asset, svgId) {
            var assetIds = asset.assetIds;
            var assetsToFind = [];
            for ( var i = 0; i < assetIds.length; i++ ) {
                if (valueExistsNotEmpty(assetIds[i])) {
                    if ('rm' === asset.assetType) {
                        assetsToFind.push(blId + ';' + flId + ';' + assetIds[i]);
                    } else {
                        assetsToFind.push(assetIds[i]);
                    }
                }
            }
            if (assetsToFind.length > 0) {
                // you can choose how to highlight the asset
                var opts = {
                    // use the cssClass property to specify a css class
                    cssClass: asset.highlightCss || this.defaultRoomHighlightCss,
                    // use the removeStyle property to specify whether or not the fill should be removed (for example  cssClass: 'zoomed-asset-bordered'  and removeStyle: false
                    removeStyle: asset.removeStyle || true,
                    svgId: svgId
                };
                // don't zoom into a room if the zoomFactor is 0
                if (asset.zoom === false) {
                    opts.zoomFactor = 0;
                }
                this.svgControl.getAddOn('AssetLocator').findAssets(assetsToFind, opts);
            }
        },
        /**
         * Highlight assets.
         * @param {string} blId - building id.
         * @param {string} flId - floor id.
         * @param {Array.<Object>} assets - List of assets with properties.
         * @param {string} svgId - Svg id.
         * @private
         */
        _highlightAssets: function (blId, flId, assets, svgId) {
            this.resetHighlight();
            for ( var i = 0; i < assets.length; i++ ) {
                this._highlightAsset(blId, flId, assets[i], svgId);
            }
        },
        /**
         * Highlight asset.
         * @param {string} blId - building id.
         * @param {string} flId - floor id.
         * @param {Object} asset - Asset properties.
         * @param {string} svgId - Svg id.
         * @private
         */
        _highlightAsset: function (blId, flId, asset, svgId) {
            var assetIds = asset.assetIds;
            var assetsToHighlight = {};
            for ( var i = 0; i < assetIds.length; i++ ) {
                if (valueExistsNotEmpty(assetIds[i])) {
                    var assetId = assetIds[i];
                    if ('rm' === asset.assetType) {
                        assetId = blId + ';' + flId + ';' + assetIds[i];
                    }
                    assetsToHighlight[assetId] = {
                        svgId: svgId,
                        color: asset.color || this.highlightAssetColor,
                        persistFill: asset.persistFill || true,
                        overwriteFill: asset.overwriteFill || true,
                        ignorePersistFill: asset.ignorePersistFill || false
                    };
                }
            }
            if (!_.isEmpty(assetsToHighlight)) {
                this.svgControl.getController("HighlightController").highlightAssets(assetsToHighlight);
            }
        },
        /**
         * Removes the specified SVG drawing from layout.
         * @param blId - Building id.
         * @param flId - Floor id.
         * @param drawingName - Drawing name.
         * @private
         */
        _unloadSvg: function (blId, flId, drawingName) {
            var parameters = {};
            parameters['pkeyValues'] = {'bl_id': blId, 'fl_id': flId};
            parameters['drawingName'] = drawingName;
            this.svgControl.unload(parameters);
        },
        /**
         * Unload all drawings
         * @private
         */
        _unloadDrawings: function () {
            _.each(this.drawings, function (drawingConfig) {
                var pkeyValues = drawingConfig.pkeyValues;
                this.unloadDrawing(pkeyValues.blId, pkeyValues.flId, drawingConfig.drawingName);
            }, this);
        },
        /**
         * Enable diagonal border selections.
         * @private
         */
        _setDiagonalSelection: function () {
            var selectController = this.svgControl.drawingController.getController("SelectController"),
                diagonalSelection = this.config.diagonalSelection;
            if (valueExists(diagonalSelection)) {
                //enable diagonal border selection
                selectController.setDiagonalSelectionPattern(true);
                if (diagonalSelection.borderWidth) {
                    //set diagonal selection border size (default is 20)
                    selectController.setDiagonalSelectionBorderWidth(diagonalSelection.borderWidth);
                }
            }
        },
        /**
         * Initialize drawing panel default actions.
         * Maximize: maximize drawing in new dialog window.
         * Reset: reset drawing selections and highlights.
         * Print: print drawing to .pdf.
         * Reports: List of reports.
         * @private
         */
        _initActions: function () {
            if (this.config.maximize) {
                var showAsDialog = getValueIfExists(this.config.maximize.showAsDialog, true);
                var icon = getValueIfExists(this.config.maximize.icon, '/schema/ab-core/graphics/show.gif');
                if (!showAsDialog) {
                    icon = '/schema/ab-core/graphics/icons/view/maximize.png';
                }
                this.addAction({
                    id: 'maximize',
                    tooltip: getMessage(AssetDrawingControl.z_ACTION_MAXIMIZE),
                    icon: icon,
                    listener: this._maximize.createDelegate(this)
                });
            }
            if (this.config.print) {
                this.addAction({
                    id: 'print',
                    tooltip: getMessage(AssetDrawingControl.z_ACTION_PRINT),
                    icon: '/schema/ab-core/graphics/icons/printer.png',
                    listener: this._print.createDelegate(this)
                });
            }
            if (this.config.reset) {
                this.addAction({
                    id: 'reset',
                    tooltip: getMessage(AssetDrawingControl.z_ACTION_RESET),
                    icon: '/schema/ab-core/graphics/icons/view/refresh-icon.png',
                    listener: this._reset.createDelegate(this)
                });
            }
            var reports = this.config.reports;
            if (valueExists(reports)) {
                this.addAction({
                    id: 'reports',
                    tooltip: getMessage(AssetDrawingControl.z_ACTION_REPORTS),
                    icon: '/schema/ab-core/graphics/icons/view/gear.png',
                    type: 'menu'
                });
                var reportMenuAction = this.drawingPanel.actions.get('reports');
                for ( var i = 0; i < reports.length; i++ ) {
                    _.extend(reports[i], {
                        handler: reports[i].listener.createDelegate(this)
                    });
                    reportMenuAction.menu.addMenuItem(reports[i]);
                }
            }
        },
        /**
         * On click asset event. Triggered by the core drawing controller.
         * @param {Object} params - {svgId, assetId, assetType}.
         * @param {Drawing.DrawingControl} drawingController - drawing controller.
         * @param {Object} event - MouseEvent.
         * @private
         */
        _onClickAsset: function (params, drawingController, event) {
            var svgId = params['svgId'],
                selectedAssetId = params['assetId'],
                assetType = params['assetType'];
            var overwrite_onClickAssetEvent = this.getEventListener('onClickAsset');
            if (overwrite_onClickAssetEvent && typeof overwrite_onClickAssetEvent == 'function') {
                overwrite_onClickAssetEvent(this, svgId, selectedAssetId, assetType);
            } else {
                var onClickBeforeEvent = this.getEventListener('onClickAssetBefore');
                if (onClickBeforeEvent && typeof onClickBeforeEvent == "function") {
                    onClickBeforeEvent(this, svgId, selectedAssetId, assetType);
                }
                this._onSelectAssetEvent('click', svgId, assetType, selectedAssetId, drawingController);
            }
        },
        /**
         * On select window event.
         * @param selectAssets - Selected assets
         * @param {Drawing.DrawingControl} drawingController - drawing controller.
         * @private
         */
        _onSelectWindow: function (selectAssets, drawingController) {
            _.each(_.keys(selectAssets), function (svgId) {
                this.resetSelectedDrawingAssets(svgId);
                var assets = selectAssets[svgId];
                var selectedAssets = this._getSelectedAssets(svgId);
                for ( var i = 0; i < assets.length; i++ ) {
                    var selectedAssetId = assets[i];
                    var asset = drawingController.getController("AssetController").getAssetById(svgId, selectedAssetId);
                    var parentNodeId = asset.node().parentNode.id;
                    var assetType = parentNodeId.split('-assets')[0];
                    var assetId = this._getAssetId(selectedAssetId, drawingController);
                    selectedAssets[assetType] = this._getSelectedAssetType(svgId, assetType);
                    selectedAssets[assetType].push(assetId);
                }
                this.selectedAssets[svgId] = selectedAssets;
            }, this);
            this.displaySelectWindowResult();
        },
        /**
         * Common onClick event on all assets.
         * @param {'click'|'contextmenu'} eventName - Event name.
         * @param {string} svgId - SVG drawing id.
         * @param {string} assetType - Asset type.
         * @param {string} selectedAssetId - Selected asset id.
         * @param {Drawing.DrawingControl} drawingController - core drawing controller.
         * @private
         */
        _onSelectAssetEvent: function (eventName, svgId, assetType, selectedAssetId, drawingController) {
            var selectController = drawingController.getController("SelectController"),
                multipleSelectionOn = selectController.multipleSelectionOn,
                selectedAssets = selectController.selectedAssets[svgId];
            var selected = false;
            if (selectedAssets) {
                selected = !(selectedAssets.indexOf(selectedAssetId) == -1);
            }
            if ('contextmenu' === eventName && selected) {
                return; //if asset is already selected, don't select on context menu
            }
            selectController.toggleAssetSelection(svgId, selectedAssetId, this.selectAssetHighlightColor);
            selectedAssets = selectController.selectedAssets[svgId];
            selected = !(selectedAssets.indexOf(selectedAssetId) == -1);
            var assetId = this._getAssetId(selectedAssetId, drawingController);
            if (selected) {
                if (!multipleSelectionOn) {
                    this.resetDrawingsSelectedAssets();
                }
                var selectedSvgAssets = this._getSelectedAssets(svgId);
                selectedSvgAssets[assetType] = this._getSelectedAssetType(svgId, assetType);
                selectedSvgAssets[assetType].push(assetId);
                this.selectedAssets[svgId] = selectedSvgAssets;
            } else {
                var selectedAssetTypeList = this.selectedAssets[svgId][assetType];// Find and remove assetId from selectedAssets
                var index = selectedAssetTypeList.indexOf(assetId);
                if (index != -1) {
                    selectedAssetTypeList.splice(index, 1);
                }
            }
            var onClickAfterEvent = this.getEventListener('onClickAssetAfter');
            if (onClickAfterEvent && typeof onClickAfterEvent == "function") {
                onClickAfterEvent(this, svgId, assetId, assetType, this.selectedAssets, selected);
            }
        },
        /**
         * On right click context-menu asset.
         * @param {Object} params - {svgId, assetId, assetType}.
         * @param {Drawing.DrawingControl} drawingController - core drawing controller.
         * @param {Object} el - HTMLElement
         * @private
         */
        _onContextMenuAsset: function (params, drawingController, el) {
            var svgId = params['svgId'],
                selectedAssetId = params['assetId'],
                assetType = params['assetType'];
            var overwrite_onContextMenuEvent = this.getEventListener('onContextMenu');
            if (overwrite_onContextMenuEvent && typeof overwrite_onContextMenuEvent == 'function') {
                overwrite_onContextMenuEvent(this, svgId, selectedAssetId, assetType, el);
            } else {
                this._onSelectAssetEvent('contextmenu', svgId, assetType, selectedAssetId, drawingController);
                this._showContextActions(svgId, assetType, selectedAssetId, drawingController, [el.clientX, el.clientY]);
            }
        },
        /**
         * Create context menu.
         * @param {string} svgId - SVG drawing id.
         * @param {string} assetType - Asset type.
         * @param {string} selectedAssetId - Selected asset id.
         * @param {Drawing.DrawingControl} drawingController - core drawing controller.
         * @param {Array} xy - HTMLElement position.
         * @private
         */
        _showContextActions: function (svgId, assetType, selectedAssetId, drawingController, xy) {
            var actions = this.config.actions;
            if (actions) {
                var assetId = this._getAssetId(selectedAssetId, drawingController);
                for ( var i = 0; i < actions.length; i++ ) {
                    if (actions[i].visible && typeof actions[i].visible == "function") {
                        actions[i].hidden = !actions[i].visible.apply(this, [assetType]);
                    }
                    _.extend(actions[i], {
                        handler: actions[i].listener.createDelegate(this, [svgId, actions[i].id, assetType, assetId], false)
                    })
                }
                var showMenu = _.find(actions, function (action) {
                    return !action.hidden
                });
                if (showMenu) {
                    new Ext.menu.Menu({
                        items: actions
                    }).showAt(xy);
                }
            }
        },
        /**
         * Close info window. Reset drawing state to 'select'.
         * @param {AssetDrawingControl} drawingController - Asset drawing controller.
         * @private
         */
        _onCloseInfoWindow: function (drawingController) {
            this.drawingState = 'select';
        },
        /**
         * Return asset id from drawing controller.
         * @param {string} selectedAssetId - Selected asset id.
         * @param {Drawing.DrawingControl} drawingController - Asset drawing controller.
         * @returns {string} assetId - original asset id.
         * @private
         */
        _getAssetId: function (selectedAssetId, drawingController) {
            return drawingController.getController("AssetController").getOriginalAssetId(selectedAssetId);
        },
        /**
         * Get room drawing record.
         * @param blId - Building id.
         * @param flId - Floor id.
         * @returns {Ab.data.Record} record
         * @private
         */
        _getDrawingRecord: function (blId, flId) {
            return View.dataSources.get("abRoomDrawing_ds").getRecord(this._createDrawingRestriction(blId, flId));
        },
        /**
         * Get selected assets for a drawing and return a new object if nothing found.
         * @param svgId - Svg drawing id.
         * @returns {Object}
         * @private
         */
        _getSelectedAssets: function (svgId) {
            return this.selectedAssets[svgId] || (this.selectedAssets[svgId] = {});
        },
        /**
         * Get selected asset by asset type and return a new array if nothing found.
         * @param svgId - Svg drawing id.
         * @param assetType - Asset type.
         * @returns {Array}
         * @private
         */
        _getSelectedAssetType: function (svgId, assetType) {
            return this.selectedAssets[svgId][assetType] || [];
        },
        /**
         * Reset all selected assets.
         */
        _resetSelectedAssets: function () {
            this.selectedAssets = {};
            //reset the selectedAssets map
            this.svgControl.getController("SelectController").selectedAssets = {};
        },
        /**
         * Set legend text according the legend level value.
         * @param {Object} row
         * @param {Object} column
         * @param {Object} cellElement
         */
        _renderLegendLabel: function (row, column, cellElement) {
            var onRenderLegendLabel = this.getEventListener('onRenderLegendLabel');
            if (onRenderLegendLabel && typeof onRenderLegendLabel == "function") {
                onRenderLegendLabel(row, column, cellElement);
            }
        },
        /**
         * Maximize drawing panel.
         * [config.maximize={showAsDialog=false,  Show view as dialog or not.
         *                  viewName='ab-drawing-controller-common.axvw' - Custom view name to open drawing when maximize
         *                  expand=function, - Expand view layout
         *                  collapse=function}] - Collapse view layout
         * @private
         */
        _maximize: function () {
            var overwriteListener = this.getEventListener('maximize');
            if (overwriteListener && typeof overwriteListener == 'function') {
                overwriteListener(this);
            } else {
                var showAsDialog = getValueIfExists(this.config.maximize.showAsDialog, true);
                if (showAsDialog) {
                    var drawingConfig = _.clone(this.config);
                    drawingConfig.listeners = this.getEventListeners();
                    var viewName = drawingConfig.maximize.viewName || this.viewName;
                    delete drawingConfig.maximize;
                    //TODO check to open in opener view if used as frame
                    View.getOpenerView().openDialog(viewName, null, false, {
                        maximize: true,
                        drawingConfig: drawingConfig,
                        drawings: this.getDrawingsConfigParameters()
                    });
                } else {
                    var maximize = jQuery('#maximize button');
                    var isMaximize = maximize.data('isMaximize') || false;
                    if (isMaximize) {
                        var expand = this.config.maximize.expand;
                        if (expand && typeof expand == 'function') {
                            expand();
                            maximize.css('background-image', 'url(' + View.contextPath + "/schema/ab-core/graphics/icons/view/maximize.png" + ')');
                            maximize.data('isMaximize', false);
                            this.drawingPanel.actions.get('maximize').setTooltip(getMessage(AssetDrawingControl.z_ACTION_MAXIMIZE));
                        }
                    } else {
                        var collapse = this.config.maximize.collapse;
                        if (collapse && typeof collapse == 'function') {
                            collapse();
                            maximize.css('background-image', 'url(' + View.contextPath + "/schema/ab-core/graphics/icons/view/minimize.png" + ')');
                            maximize.data('isMaximize', true);
                            this.drawingPanel.actions.get('maximize').setTooltip(getMessage(AssetDrawingControl.z_ACTION_MINIMIZE));
                        }
                    }
                }
            }
        },
        /**
         * Reset drawing panel.
         * @private
         */
        _reset: function () {
            var overwriteListener = this.getEventListener('reset');
            if (overwriteListener && typeof overwriteListener == 'function') {
                overwriteListener(this);
            } else {
                this.resetDrawingHighlights();
            }
        },
        /**
         * Print drawing panel.
         * @private
         */
        _print: function () {
            var overwriteListener = this.getEventListener('print');
            if (overwriteListener && typeof overwriteListener == 'function') {
                overwriteListener(this);
            } else {
                if (!_.isEmpty(this.drawings)) {
                    // TODO print multiple drawings
                    if (_.keys(this.drawings).length == 1) {
                        var drawingName = _.values(this.drawings)[0].drawingName;
                        var pdfParameters = {};
                        pdfParameters.documentTemplate = "report-cadplan-imperial-landscape-17x22.docx";
                        pdfParameters.drawingName = drawingName;
                        pdfParameters.plan_type = "1 - ALLOCATION";
                        pdfParameters.drawingZoomInfo = {};
                        pdfParameters.drawingZoomInfo.image = this.svgControl.getImageBytes(drawingName);
                        this._doPrint(pdfParameters);
                    } else {
                        View.alert('Print is not supported for multiple drawings')
                    }
                }
            }
        },
        /**
         * Start job and print drawing
         * @param pdfParameters
         * @private
         */
        _doPrint: function (pdfParameters) {
            //TODO print parameters
            /*
             //dataSources defined in a separate axvw which is shared with drawing control axvw
             pdfParameters.dataSources = {viewName:view_file, required:highligtDS + ';' + labelDS + ';' + legendDS};
             pdfParameters.highlightDataSource = 'rm:'+highligtDS;
             pdfParameters.labelsDataSource = 'rm:'+labelDS;
             pdfParameters.labelHeight = "rm:" + label_ht;
             */
            //if showLegend=true, legend panel must be defined in ab-ex-dwg-rpt-pdf.axvw or active_plantypes.view_file
            //its dataSource id must be defined as highlight dataSource id (active_plantypes.hs_ds)  + "_legend"
            //its panel id must be defined as "panel_" + its dataSource id
            pdfParameters.showLegend = true;
            //defined in ab-ex-dwg-rpt-pdf.axvw
            /*var legendDS = highligtDS+ '_legend';
             //required legend panel, also defined in ab-ex-dwg-rpt-pdf.axvw
             pdfParameters.legends = {};
             pdfParameters.legends.required = 'panel_'+legendDS;
             */
            //provide symbol print java handler so that the core would invoke its draw() method during highlighting published emf
            pdfParameters.symbolsHandler = "com.archibus.app.solution.common.report.docx.SymbolsHandlerExample";
            var jobId = Workflow.startJob('AbSystemAdministration-generatePaginatedReport-buildDocxFromView', 'ab-ex-dwg-rpt-pdf.axvw', null, pdfParameters, null);
            View.openJobProgressBar("Please wait...", jobId, null, function (status) {
                var url = status.jobFile.url;
                window.open(url);
            });
        }
    },
    {
        z_ACTION_MAXIMIZE: 'actionMaximize',
        z_ACTION_MINIMIZE: 'actionMinimize',
        z_ACTION_RESET: 'actionReset',
        z_ACTION_PRINT: 'actionPrint',
        z_ACTION_REPORTS: 'actionReports'
    }
);