/**
 * View 360 controller.
 */
var View360Control = Base.extend({
    // controller configuration
    config: null,
    // viewer (image or video)
    viewer: null,
    // event listeners
    eventListeners: null,
    // allowed image types
    allowedImageTypes: ['jpg', 'png'],
    // allowed video types
    allowedVideoTypes: ['mp4', 'avi', 'mov', 'webm'],
    // documents object
    documents: {
        images: [],
        videos: []
    },
    constructor: function (config) {
        this.config = config;
        this.eventListeners = new Ext.util.MixedCollection(true);
    },
    /**
     * Set and init documents objects
     * @param {Array.<Ab.data.Record>} docRecords - decument records.
     */
    setDocuments: function (docRecords) {
        this.config.docRecords = docRecords;
        // reset documents object
        this.resetDocuments();
        // create documents object
        this.initRecords();
        // init documents selector
        this.initDocumentsSelector();
        // init viewer
        this.init();
    },
    /**
     * Create documents object.
     */
    initRecords: function () {
        var docRecords = this.config.docRecords;
        for ( var i = 0; i < docRecords.length; i++ ) {
            var document = {};
            var record = docRecords[i];
            var docId = record.getValue('docs_assigned.doc_id'),
                name = record.getValue('docs_assigned.name'),
                doc = record.getValue('docs_assigned.doc'),
                url = record.getValue('docs_assigned.url'),
                docAuthor = record.getValue('docs_assigned.doc_author'),
                description = record.getValue('docs_assigned.description');
            document.doc_id = docId;
            document.name = name;
            document.doc = doc;
            document.url = url;
            document.hasUrl = !!url;
            document.doc_author = docAuthor;
            document.description = description;
            document.keys = {
                'doc_id': docId,
                'doc': doc
            };
            document.tableAndName = ["docs_assigned", "doc"];
            var docNotes = record.getValue('docs_assigned.doc_note');
            if (valueExistsNotEmpty(docNotes)) {
                var notes = eval('(' + docNotes + ')');
                _.extend(document, notes);
            }
            var extension = doc.substring(doc.lastIndexOf('.') + 1).toLowerCase();
            if (this.isValidImageExtension(extension)) {
                this.documents.images.push(document);
            } else if (this.isValidVideoExtension(extension)) {
                this.documents.videos.push(document);
            } else if (valueExistsNotEmpty(url)) {
                this.documents.images.push(document);
            }
        }
    },
    /**
     * Init documents selector
     */
    initDocumentsSelector: function () {
        var documents = this.documents;
        var images = documents.images,
            videos = documents.videos;
        var divId = document.getElementById(this.config.panel + '_title');
        this.createImageSelector(divId, images);
        this.createVideoSelector(divId, videos);
    },
    /**
     * Create image selector
     * @param divId - Viewer title div.
     * @param {Array.<Object>} images - List of images
     */
    createImageSelector: function (divId, images) {
        this.appendSelector(divId, 'image-selector', getMessage(View360Control.z_VIEWER_IMAGE), images, this.onChangeViewer.createDelegate(this));
    },
    /**
     * Create video selector
     * @param divId - Viewer title div.
     * @param {Array.<Object>} videos - List of videos
     */
    createVideoSelector: function (divId, videos) {
        this.appendSelector(divId, 'video-selector', getMessage(View360Control.z_VIEWER_VIDEO), videos, this.onChangeViewer.createDelegate(this));
    },
    /**
     * Init image viewer or video viewer. If also images and videos exists, images are loaded first.
     */
    init: function () {
        this.resetViewer();
        var images = this.documents.images,
            videos = this.documents.videos;
        if (!_.isEmpty(images)) {
            var scenes = {};
            var localizedAuthor = getMessage(View360Control.z_VIEWER_BY_AUTHOR);
            var firstSceneId = null;
            for ( var i = 0; i < images.length; i++ ) {
                var image = images[i];
                if (valueExists(image.isDefault) && image.isDefault) {
                    firstSceneId = image.name;
                    if (image.hasUrl) {
                        firstSceneId = image.url;
                    }
                }
                var hotSpots = image.hotSpots;
                _.each(hotSpots, function (hotSpot) {
                    _.extend(hotSpot, {
                        clickHandlerFunc: this.onClickHotSpot.createDelegate(this),
                        clickHandlerArgs: hotSpot.sceneId
                    })
                }, this);
                var author = image.doc_author ? localizedAuthor + ' ' + image.doc_author : '';
                var jsonParameters = {};
                _.extend(jsonParameters, image, {
                    panorama: image.url,
                    title: image.name,
                    author: author,
                    description: image.description,
                    hotSpots: hotSpots,
                    autoLoad: valueExists(image.autoLoad) ? image.autoLoad : true
                });
                scenes[image.name] = jsonParameters;
            }
            var firstImage = valueExists(firstSceneId) ? this.getImage(firstSceneId) : images[0];
            if (!valueExistsNotEmpty(firstImage.url)) {
                var keys = firstImage.keys;
                var tableAndName = firstImage.tableAndName;
                this.getImageDocument(keys, tableAndName[0], tableAndName[1], this.initImageViewerCallback.createDelegate(this, [firstImage, scenes], true));
            } else {
                this.initImageViewer(firstImage.name, scenes);
            }
        } else if (!_.isEmpty(videos)) {
            var firstVideo = _.find(videos, function (video) {
                return (valueExists(video.isDefault) && video.isDefault);
            });
            firstVideo = firstVideo || videos[0];
            this.loadVideo(firstVideo);
        } else {
            this.afterViewerLoad(this.viewer);
        }
    },
    /**
     * Called after the image is retrieve from database.
     * @param {string} imageUrl - Image URL.
     * @param {Object} firstImage - first image to load.
     * @param {Object} scenes - Map of scenes.
     */
    initImageViewerCallback: function (imageUrl, firstImage, scenes) {
        var firstScene = scenes[firstImage.name];
        firstScene.panorama = imageUrl;
        this.initImageViewer(firstImage.name, scenes);
    },
    /**
     * Init image viewer.
     * @param {string} firstSceneId - First scene id.
     * @param {Object} scenes - Map of scenes.
     */
    initImageViewer: function (firstSceneId, scenes) {
        this.viewer = pannellum.viewer(this.config.container, {
            default: {
                firstScene: firstSceneId
            },
            scenes: scenes
        });
        // viewer listeners
        this.viewer.on('load', this.afterLoadImage.createDelegate(this));
        this.viewer.on('hotspotchange', this.onHotSpotChange.createDelegate(this));
        this.viewer.on('scenechange', this.onSceneChange.createDelegate(this));
        this.viewer.on('error', this.onViewError.createDelegate(this));
    },
    /**
     * On click hotspot event.
     * @param {Object} e - Mouse event.
     * @param {string} sceneId - Scene id to load.
     */
    onClickHotSpot: function (e, sceneId) {
        var image = this.getImage(sceneId);
        this.loadImage(image);
    },
    /**
     * Load image from database.
     * @param {Object} image - Image object.
     */
    loadImage: function (image) {
        var keys = image.keys;
        var tableAndName = image.tableAndName;
        if (image.hasUrl) {
            this.callbackLoadImage(image.url, image);
        } else {
            this.getImageDocument(keys, tableAndName[0], tableAndName[1], this.callbackLoadImage.createDelegate(this, [image], true));
        }
    },
    /**
     * Called after image is loaded.
     * @param {string} imageUrl - Image URL.
     * @param {Object} image - Image object to load.
     */
    callbackLoadImage: function (imageUrl, image) {
        var scenes = this.viewer.getConfig().scenes;
        var scene = scenes[image.name];
        scene.panorama = imageUrl;
        this.viewer.loadScene(image.name);
    },
    /**
     * Load video.
     * @param {Object} video - Video object to load.
     */
    loadVideo: function (video) {
        var keys = video.keys;
        var tableAndName = video.tableAndName;
        this.getVideoDocument(keys, tableAndName[0], tableAndName[1], video.name, this.callbackLoadVideo.createDelegate(this, [video], true));
    },
    /**
     * Called after the video is retrieve from database.
     * @param {string} videoUrl - Video URL.
     * @param {object} video - Video object.
     */
    callbackLoadVideo: function (videoUrl, video) {
        var videoEl = document.getElementById("video-ctrl");
        var extension = video.doc.substring(video.doc.lastIndexOf('.') + 1).toLowerCase();
        if (videoEl) {
            this.viewer.src({"type": 'video/' + extension, "src": videoUrl});
            this.viewer.play();
        } else {
            var container = document.getElementById(this.config.container);
            videoEl = document.createElement("video");
            videoEl.id = 'video-ctrl';
            videoEl.className = 'video-js vjs-default-skin vjs-big-play-centered';
            videoEl.setAttribute('height', container.clientHeight);
            videoEl.setAttribute('width', container.clientWidth);
            addSourceToVideo(videoEl, videoUrl, 'video/' + extension);
            container.appendChild(videoEl);
            this.viewer = videojs('video-ctrl', {
                controls: true,
                controlBar: {
                    fullscreenToggle: false,
                    volumeMenuButton: {
                        inline: false
                    },
                    liveDisplay: false
                },
                autoplay: this.config.autoLoad || true,
                preload: "auto",
                playbackRates: [1, 1.5, 2],
                dynamic: true,
                plugins: {
                    pannellum: {}
                }
            }).ready(function () {
                var myPlayer = this;
                // EXAMPLE: Start playing the video.
                // myPlayer.play();
            });
        }
        this.afterViewerLoad(this.viewer);
    },
    /**
     * Get image from documents object.
     * @param {string} value - name|url
     * @returns {Object} image object
     */
    getImage: function (value) {
        return _.find(this.documents.images, function (image) {
            if (image.hasUrl) {
                return value === image.url;
            } else {
                return value === image.name;
            }
        });
    },
    /**
     * Get video from documents object.
     * @param {string} fileName - File name.
     * @returns {Object} video object
     */
    getVideo: function (fileName) {
        return _.find(this.documents.videos, function (video) {
            return fileName === video.name;
        });
    },
    /**
     * Listener called after image is loaded.
     */
    afterLoadImage: function () {
        this.afterViewerLoad(this.viewer);
    },
    /**
     * Listener called after images and videos are loaded.
     */
    afterViewerLoad: function (viewer) {
        //console.log('onViewerReady');
        var afterViewerLoadListener = this.getEventListener('afterViewerLoad');
        if (afterViewerLoadListener && _.isFunction(afterViewerLoadListener)) {
            afterViewerLoadListener(viewer);
        }
    },
    /**
     * Event on change scene.
     * @param {string} sceneId - Scene id.
     */
    onSceneChange: function (sceneId) {
        //console.log('onSceneChange');
    },
    /**
     * On hotspot change event. Get image from database and loads the image.
     * @param {string} sceneId - Scene id.
     */
    onHotSpotChange: function (sceneId) {
        //console.log('onHotSpotChange');
        var image = this.getImage(sceneId);
        this.loadImage(image);
    },
    /**
     * Listener on image viewer on error.
     * @param e
     */
    onViewError: function (e) {
        console.log('Error! %s', e.message);
    },
    /**
     * Reset documents object.
     */
    resetDocuments: function () {
        this.documents = {
            images: [],
            videos: []
        }
    },
    /**
     * Reset viewer.
     */
    resetViewer: function () {
        if (valueExists(this.viewer)) {
            if (_.isFunction(this.viewer.id) && 'video-ctrl' === this.viewer.id()) {
                this.viewer.dispose();
            } else {
                this.viewer.destroy();
            }
            this.viewer = null;
        }
    },
    /**
     * Adds a listener to specified control event.
     * @param {string} eventName - Name of the control event.
     * @param {Function} listener - Listener function reference.
     */
    addEventListener: function (eventName, listener) {
        if (this.eventListeners.containsKey(eventName)) {
            this.eventListeners.replace(eventName, listener);
        } else {
            this.eventListeners.add(eventName, listener);
        }
    },
    /**
     * Returns registered event listener.
     * @param {string} eventName - Event name, specific to the control type.
     */
    getEventListener: function (eventName) {
        var listener = this.eventListeners.get(eventName);
        if (!valueExists(listener)) {
            listener = null;
        }
        return listener;
    },
    getEventListeners: function () {
        return this.eventListeners.map;
    },
    /**
     * Removes listeners from specified control event.
     * @param {string} eventName - The name of the control event.
     */
    removeEventListener: function (eventName) {
        this.eventListeners.removeKey(eventName);
    },
    /**
     * Sets the control height to specified value.
     * @param {number} height: The height in pixels.
     */
    setHeight: function (height) {
        //this.viewer.iframe.setAttribute('height', height);
    },
    /**
     * Creates image or video selector.
     * @param div - div to append to.
     * @param comboId - selector id.
     * @param label - label.
     * @param options - options
     * @param callback - callback functions.
     */
    appendSelector: function (div, comboId, label, options, callback) {
        if (div === null) {
            return;
        }
        var selectorId = 'selector-' + comboId;
        var selector = Ext.get('selector-' + comboId);
        if (!selector) {
            var pn = div.parentNode.parentNode;
            var cell = Ext.DomHelper.append(pn, {tag: 'td', id: 'selector-label-' + comboId});
            var tn = Ext.DomHelper.append(cell, '<p>' + label + '</p>', true);
            Ext.DomHelper.applyStyles(tn, "x-btn-text");
            cell = Ext.DomHelper.append(pn, {tag: 'td'});
            selector = Ext.DomHelper.append(cell, {tag: 'select', id: selectorId}, true);
        }
        var hasOptions = !_.isEmpty(options);
        if (hasOptions) {
            for ( var i = 0; i < options.length; i++ ) {
                var document = options[i],
                    text = document.name,
                    value = document.name;
                if (document.hasUrl) {
                    value = document.url;
                }
                var option = new Option(text, value);
                if (valueExists(options[i].isDefault) && options[i].isDefault) {
                    option.selected = true;
                }
                selector.dom.options[i] = option;
            }
        }
        this.showSelector(comboId, hasOptions);
        selector.dom.onchange = callback.createDelegate(this, [selector, comboId]);
    },
    /**
     * Show selector.
     * @param {string} comboId - Selector id.
     * @param {boolean} show - true to show
     */
    showSelector: function (comboId, show) {
        var selector = Ext.get('selector-' + comboId);
        if (selector) {
            selector.parent().setDisplayed(show);
            var label = Ext.get('selector-label-' + comboId);
            if (label) {
                label.setDisplayed(show);
            }
        }
    },
    setSelectorValue: function (comboId, value) {
        var selector = Ext.get('selector-' + comboId);
        if (selector) {
            selector.setValue(value);
        }
    },
    /**
     * On change viewer selector.
     * @param viewerSelector
     * @param comboId
     */
    onChangeViewer: function (viewerSelector, comboId) {
        var selectorValue = viewerSelector.getValue();
        if ('image-selector' === comboId) {
            var image = this.getImage(selectorValue);
            this.loadImage(image);
        } else if ('video-selector' === comboId) {
            var video = this.getVideo(selectorValue);
            this.loadVideo(video);
        }
    },
    /**
     * Check valid
     * @param extension
     * @returns {boolean}
     */
    isValidImageExtension: function (extension) {
        return _.contains(this.allowedImageTypes, extension);
    },
    isValidVideoExtension: function (extension) {
        return _.contains(this.allowedVideoTypes, extension);
    },
    getImageDocument: function (keys, docTableName, docFieldName, callbakFn) {
        DocumentService.getImage(keys, docTableName, docFieldName, '1', true, {
            callback: function (image) {
                var requestUrl = View.originalRequestURL;
                var host = requestUrl.indexOf("/archibus/");
                var url = requestUrl.slice(0, host) + image;
                callbakFn(url);
            },
            errorHandler: function (m, e) {
                View.showException(e);
            }
        });
    },
    getVideoDocument: function (keys, docTableName, docFieldName, docFileName, callbakFn) {
        DocumentService.show(keys, docTableName, docFieldName, docFileName, '', true, 'showDocument', {
            callback: function (fileTransfer) {
                callbakFn(fileTransfer);
            },
            errorHandler: function (m, e) {
                View.showException(e);
            }
        });
    }
}, {
    z_VIEWER_IMAGE: 'viewerImage',
    z_VIEWER_VIDEO: 'viewerVideo',
    z_VIEWER_BY_AUTHOR: 'viewerByAuthor'
});
function addSourceToVideo(element, src, type) {
    var source = document.createElement('source');
    source.src = src;
    source.type = type;
    element.appendChild(source);
}