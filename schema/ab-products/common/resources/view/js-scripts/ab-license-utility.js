/**
 * Active license object.
 */
var activeLicenseDTO = null;

// load active license object.
loadActiveLicense();

/**
 * Load current license and return current license object.
 */
function loadActiveLicense(){
	AdminService.getProgramLicense({
        callback: function(license) {
        	activeLicenseDTO = license;
        },
        errorHandler: function(m, e) {
            View.showException(e);
        }
    });
}

/**
 * Check if user has license for specified activity.
 * @param activityId activity id
 */
function hasLicenseForActivity(activityId){
	var isLicensed = false;
	if (valueExists(activeLicenseDTO)) {
		var license =  findLicense(activeLicenseDTO.licenses, activityId);
		if (valueExists(license)) {
			if ( license.group == 'option' 
				&& (license.concurrentUsers > 0 || license.concurrentUsers + license.namedUsers > 0 )) {
				isLicensed = true;
			} else if (license.group == 'activity' && license.enabled 
					&& (license.concurrentUsers > 0 || license.concurrentUsers + license.namedUsers > 0 )) {
				isLicensed = true;
			}
		}
	}
	return isLicensed;
}

/**
 * Find license by id
 * @param licenses  licenses array
 * @param id license id
 */
function findLicense(licenses, id){
    for (var i = 0; i < licenses.length; i++) {
    	var license = licenses[i];
    	if (license.id == id) {
    		return license;
    	}
    }
    return null;
}