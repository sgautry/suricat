/**
 * Controller for Space Express Console.
 *
 * Events:
 * app:space:express:console:changeMode
 * app:space:express:console:cancelAssignment
 */
var developMovesScenarios = View.createController('developMovesScenarios', {

	projectId:"",
	scenarioId:"",
	dateEnd:"",
	isoDateStart:"",
	tabTitle:"",
    
    /**
     * Sets the default mode.
     */
    afterInitialDataFetch: function() {
		var parentController;
		if(View.controllers.get('tabsController')){
			parentController = View.controllers.get('tabsController');
		} else if(View.getOpenerView().controllers.get('tabsController')){
			parentController = View.getOpenerView().controllers.get('tabsController');
		}

		if ( parentController && parentController.project_id &&  parentController.scenario_id ) {
			this.refresh(parentController.project_id, parentController.scenario_id);
		} 
		else {
			this.refresh( "project 1",  "scenario 1");
		}
    },

    refresh: function(projectId, scenarioId) {
		var firstLoad =  !this.projectId && !this.scenarioId; 
		
		if ( this.projectId!=projectId || this.scenarioId!=scenarioId ) {
			this.projectId = projectId;
			this.initialDate(projectId);
			this.scenarioId = scenarioId;

			if (!firstLoad) {
				this.trigger('app:mo:scenarios:console:refresh');
			}else{
				this.tabTitle=View.parentTab.title;
			}
				
		}
		View.parentTab.setTitle(this.tabTitle+": "+projectId+"-"+scenarioId);
    },

    initialDate: function(projectId) {
		if ( projectId ) {
			this.getEndDateDs.addParameter("projectId", this.projectId);
			var record = this.getEndDateDs.getRecord();
			this.dateEnd = record.getValue("project.date_end");
			this.isoDateStart = getIsoFormatDate(this.dateEnd);
		}
    },
    
    //add refresh button on employees to locate and teams to locate view 
	addRefreshButton: function(menuId, type, controller){

		if(controller.isRefreshIconCreated)
			return;
		var exportBtn = Ext.get(menuId).dom.parentNode.parentNode;
		var cell = Ext.DomHelper.insertFirst(exportBtn, {
			tag : 'td',
			id : 'tdRefresh'+type
		});
		Ext.DomHelper.append(cell, "<img id='imgRefresh'"+type+" src='/archibus/schema/ab-core/graphics/icons/view/refresh-icon.png' style='margin-left:5px;cursor:pointer;' title='"+getMessage("refresh_after_data_transfer_in")+"'/>", true);
		Ext.get("tdRefresh"+type).on("click", function(){
			//refresh grid
			controller.refreshAfterDataTransferIn();
			//refresh waiting room list
			if(type=="Employee"){
				spaceExpressConsoleDrawing.refreshEmployeeWaitingPanel();
			}
			
		});
	},
});