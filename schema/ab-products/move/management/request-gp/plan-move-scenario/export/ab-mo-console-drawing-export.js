/**
 * Controller for printing the drawing's PDF  report.
 *	  Added for 23.2	Move Scenarios Console 
 *	 @Author Zhang Yi

 * Events:
 */
var moConsoleDrawingExportCtrl = View.createController('moConsoleDrawingExportCtrl', {

	/**
     * Constructor.
     */
    afterCreate: function() {
		this.on('app:mo:scenarios:console:printCurrentInventoryPDF', this.printCurrentInventoryPDF);
    },
    
	/**
     * Print the drawing Pdf report.
     */
    printCurrentInventoryPDF: function(controller, printParameters) {

		var floors = printParameters.floors; 
    	var pdfRestrictions = {};
    	var pdfParameters = {};
    	pdfParameters.legends = {};
    
		//get highlight and label dataSource ids from drawing control
    	var viewName = 'ab-mo-console-ds-drawing.axvw';
    	var highligtDS = controller.drawingControl.config.highlightParameters[0]["hs_ds"];
    	var labelDS = controller.drawingControl.config.highlightParameters[0]["label_ds"];
    	var legendDs =  highligtDS+ '_legend';

		//prepare restriction from filter and selected floors
		var passedRestrictions = {	};
		passedRestrictions[highligtDS] =  new Ab.view.Restriction();
		passedRestrictions[highligtDS].addClause("rm.bl_id", floors[0]["bl_id"]);
		passedRestrictions[highligtDS].addClause("rm.fl_id", floors[0]["fl_id"]);

		//highlight dataSource is required for paginated report
    	if(highligtDS === '' || highligtDS === 'None'){
    		highligtDS = 'dummyDs';
    	}

		//pass drawingName to skip core's expensive process to get it
		//pdfParameters.drawingName = selectedFloor.dwgName;
		//dataSources defined in a separate axvw which is shared with drawing control axvw
		pdfParameters.dataSources = {viewName:viewName, required:highligtDS + ';' + labelDS};			
		pdfParameters.highlightDataSource = 'rm:'+highligtDS;
		if(labelDS !== '' && labelDS !== 'None'){
			pdfParameters.labelsDataSource = 'rm:'+labelDS;
		}
		
		if(highligtDS !== 'dummyDs'){
			//add legend dataSource to required list
			pdfParameters.dataSources.required = pdfParameters.dataSources.required +  ';' + legendDs;
			
			//required legend panel
			pdfParameters.legends.required = 'panel_'+legendDs;
		}

		var isPrintCurrent = this.setPrintParametersFromHoc(controller, pdfParameters, printParameters['hocParameters']);
			
		View.closeDialog();

		if (controller.backgroundSuffix) {
			pdfParameters['backgroundSuffix']=controller.backgroundSuffix.substring(1);
		}

		if (isPrintCurrent) {
			pdfParameters.documentTemplate = "/schema/ab-products/move/management/request-gp/plan-move-scenario/export/report-mo-console-template.docx";
			pdfParameters.drawingName = controller.selectedFloors[0].dwgname;
			pdfParameters.bl_id = controller.selectedFloors[0].bl_id;
			pdfParameters.fl_id = controller.selectedFloors[0].fl_id;

			pdfParameters.drawingZoomInfo = {};

			var scope = this;
			controller.drawingControl.getImageBytes(drawingName, function(image){
					pdfParameters.drawingZoomInfo.image = image;
					scope._doCurrentInventoryFloorReport(passedRestrictions, pdfParameters);
				}
			 );
		} 
		else {
			if(valueExistsNotEmpty(pdfParameters.highlightDataSource)){
				View.openPaginatedReportDialog("ab-mo-console-drawing-export-inv-pdf.axvw", passedRestrictions, pdfParameters);
			}			
		}
	},

	_doCurrentInventoryFloorReport: function(passedRestrictions, pdfParameters){
    	 var jobId = Workflow.startJob('AbSystemAdministration-generatePaginatedReport-buildDocxFromView', 'ab-mo-console-drawing-export-inv-pdf.axvw', passedRestrictions, pdfParameters, null);
    	 View.openJobProgressBar("Please wait...", jobId, null, function(status) {
     	   		var url  = status.jobFile.url;
     	   		window.open(url);
     	  });
	},

	/**
	 * Exports drawing image in PDF. Not be able to display restriction and title.
	 * A simplified paginated report with one single drawing image grasped from screen.
	 */
	printCurrentScenarioPDF: function(controller){
		if ( controller.selectedFloors && controller.selectedFloors.length>0 ) {
			var pdfRestrictions = {};
			var pdfParameters = {};
			pdfParameters.legends = {};
			pdfParameters.dataSources = {};	
		
			//get highlight and label dataSource ids from drawing control
			var highligtDS = controller.drawingControl.config.highlightParameters[0]["hs_ds"];
			var labelDS = controller.drawingControl.config.highlightParameters[0]["label_ds"];
			var legendDs =  highligtDS+ '_legend';;

			
			//prepare restriction from filter and selected floors
			var passedRestrictions = {	};
			passedRestrictions[highligtDS] =  new Ab.view.Restriction();
			passedRestrictions[highligtDS].addClause("rm_trial.bl_id", controller.selectedFloors[0]["bl_id"]);
			passedRestrictions[highligtDS].addClause("rm_trial.fl_id", controller.selectedFloors[0]["fl_id"]);

			//highlight dataSource is required for paginated report
			if(highligtDS === '' || highligtDS === 'None'){
				highligtDS = 'dummyDs';
			}
			
			if(highligtDS !== 'dummyDs'){
				//add legend dataSource to required list
				pdfParameters.dataSources.required = legendDs;
				
				//required legend panel
				pdfParameters.legends.required = 'panel_'+legendDs;
			}

			View.closeDialog();

			pdfParameters.documentTemplate = "/schema/ab-products/move/management/request-gp/plan-move-scenario/export/report-mo-console-template.docx";
			pdfParameters.drawingName = controller.selectedFloors[0].dwgname;
			pdfParameters.bl_id = controller.selectedFloors[0].bl_id;
			pdfParameters.fl_id = controller.selectedFloors[0].fl_id;

			pdfParameters.drawingZoomInfo = {};
			
			var scope = this;
			controller.drawingControl.getImageBytes(drawingName, function(image){
					pdfParameters.drawingZoomInfo.image = image;
					scope._doCurrentScenarioLayoutReport(passedRestrictions, pdfParameters);
				}
			 );
		}
	},

	_doCurrentScenarioLayoutReport: function(passedRestrictions, pdfParameters){
    	 var jobId = Workflow.startJob('AbSystemAdministration-generatePaginatedReport-buildDocxFromView', 'ab-mo-console-drawing-export-scenario-pdf.axvw', passedRestrictions, pdfParameters, null);
    	 View.openJobProgressBar("Please wait...", jobId, null, function(status) {
     	   		var url  = status.jobFile.url;
     	   		window.open(url);
     	  });
	},

	setPrintParametersFromHoc: function (controller, pdfParameters, hocParameters){
		var printCurrent = false;
		//prepare parameters from print option dialog
		if (hocParameters) {
			if (hocParameters['scale']) {
				pdfParameters['scale'] = hocParameters['scale'];
			}
			if (!hocParameters['usePublishedLabelHeight']) {
				pdfParameters['labelHeight'] = "rm:" + hocParameters['labelHeight'];
				pdfParameters['usePublishedLabelHeight'] = false;
			} else {
				pdfParameters['usePublishedLabelHeight'] = true;
			}
			if (hocParameters['tableLedgerShadingColor']) {
				pdfParameters['tableLedgerShadingColor'] = hocParameters['tableLedgerShadingColor'];
			}
			
			if ( hocParameters && hocParameters.selectionValues && hocParameters.selectionValues['zoomedInOption']==='yes' ) {
				printCurrent = true;
				//kb#3049720: if user select print by 'zoom' selection, then only print single drawings in drawing panel. 
				var floor = controller.selectedFloors[0];
				pdfParameters['bl_id'] = " rm.bl_id='"+floor.bl_id+"'";
				pdfParameters['fl_id'] = " rm.fl_id='"+floor.fl_id+"'";
				if ( hocParameters['drawingZoomInfo'] ) {
					pdfParameters['drawingZoomInfo'] = hocParameters['drawingZoomInfo'];
				}
			}
					
			if (hocParameters['labelLines']) {
				pdfParameters['labelLines'] = hocParameters['labelLines'];
			}
			
			if (hocParameters['hatchSize']) {
				pdfParameters['hatchSize'] = hocParameters['hatchSize'];
			}
		}
		return printCurrent;
	}
});