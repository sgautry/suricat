<view version="2.0">
	    <js file="ab-mo-console-copy-occupancy-scenario.js"/>
    <message name="selectBothGridsInfo" translatable="true">Please select one occupancy scenario and one move scenario.</message>
    <message name="title_building" translatable="true">Building</message>
    <message name="title_floor" translatable="true">Floor</message>
    <message name="title_room" translatable="true">Room</message>
    <message name="vacantOnly" translatable="true">Available Only</message>
    <message name="finished" translatable="true">Copy finished.</message>

    <layout id="mainLayout">
        <north initialSize="80" autoScroll="false" split="false"/>
        <center/>
    </layout>
    <layout id="subLayout" containingLayout="mainLayout" region="center">
        <west initialSize="50%" split="false"/>
        <center/>
    </layout>
    
    <dataSource id="occupancyScenarioLayoutsDs">
        <sql dialect="generic">
            SELECT distinct
                 mo_scenario_em.to_bl_id ${sql.as} to_bl_id, 
                 mo_scenario_em.to_fl_id ${sql.as} to_fl_id,
                 mo_scenario_em.filename ${sql.as} filename, 
                 afm_enterprise_graphics.dwg_name ${sql.as} vf_dwg_name, 
                 afm_enterprise_graphics.usage_subtype ${sql.as} vf_usage_subtype
            FROM mo_scenario_em, afm_enterprise_graphics 
            WHERE mo_scenario_em.filename=afm_enterprise_graphics.filename and 
                  mo_scenario_em.project_id=${parameters['projectId']} and mo_scenario_em.scenario_id=${parameters['scenarioId']} and 
                  (mo_scenario_em.em_id is not null or mo_scenario_em.team_id is not null) and mo_scenario_em.to_bl_id is not null and mo_scenario_em.to_fl_id is not null and mo_scenario_em.filename is not null
        </sql>
        <table name="mo_scenario_em" role="main" />
        <field name="filename" table="mo_scenario_em"/>
        <field name="to_bl_id" table="mo_scenario_em"/>
        <field name="to_fl_id" table="mo_scenario_em"/>
        <field name="vf_dwg_name" table="mo_scenario_em"/>
        <field name="vf_usage_subtype" table="mo_scenario_em"/>
        <parameter name="projectId" dataType="text" value=""/>
        <parameter name="scenarioId" dataType="text" value=""/>
    </dataSource>

    <dataSource id="moScenarioLayoutsDs">
        <sql dialect="generic">
            SELECT distinct
                 mo_scenario_em.to_bl_id ${sql.as} to_bl_id, 
                 mo_scenario_em.to_fl_id ${sql.as} to_fl_id,
                 mo_scenario_em.filename ${sql.as} filename, 
                 afm_enterprise_graphics.usage_type ${sql.as} vf_usage_type
            FROM mo_scenario_em, afm_enterprise_graphics 
            WHERE mo_scenario_em.filename=afm_enterprise_graphics.filename and 
                  mo_scenario_em.project_id=${parameters['projectId']} and mo_scenario_em.scenario_id=${parameters['scenarioId']} and
                  mo_scenario_em.to_bl_id is not null and mo_scenario_em.to_fl_id is not null and mo_scenario_em.filename is not null        
        </sql>
        <table name="mo_scenario_em" role="main" />
        <field name="filename" table="mo_scenario_em"/>
        <field name="to_bl_id" table="mo_scenario_em"/>
        <field name="to_fl_id" table="mo_scenario_em"/>
        <field name="usage_type" table="mo_scenario_em"/>
        <parameter name="projectId" dataType="text" value=""/>
        <parameter name="scenarioId" dataType="text" value=""/>
    </dataSource>  
    
    <dataSource id="getOccupancyScenariosDs">
        <table name="mo_scenario_em" role="main" />
        <field name="to_bl_id"/>
        <field name="to_fl_id"/>
        <field name="to_rm_id"/>
        <field name="em_id"/>
        <restriction type="sql" sql="mo_scenario_em.project_id=${parameters['projectId']} and mo_scenario_em.scenario_id=${parameters['scenarioId']}
        and mo_scenario_em.to_bl_id=${parameters['toBlId']} and mo_scenario_em.to_fl_id=${parameters['toFlId']} and mo_scenario_em.filename=${parameters['filename']}
        and mo_scenario_em.em_id is not null"/>
        <parameter name="projectId" dataType="text" value=""/>
        <parameter name="scenarioId" dataType="text" value=""/>
        <parameter name="toBlId" dataType="text" value=""/>
        <parameter name="toFlId" dataType="text" value=""/>
        <parameter name="filename" dataType="text" value=""/>
    </dataSource>

    <dataSource id="copyToMoveScenarioDs">
        <table name="mo_scenario_em" role="main" />
        <field name="project_id"/>
        <field name="scenario_id"/>
        <field name="to_bl_id"/>
        <field name="to_fl_id"/>
        <field name="to_rm_id"/>
        <field name="em_id"/>
        <field name="filename"/>
    </dataSource>

    <dataSource id="checkLocationsForLayoutScenarioDs">
        <table name="rm_trial" role="main" />
        <field name="bl_id"/>
        <field name="fl_id"/>
        <field name="rm_id"/>
        <restriction type="sql" sql="rm_trial.bl_id=${parameters['toBlId']} and rm_trial.fl_id=${parameters['toFlId']} and rm_trial.rm_id=${parameters['toRmId']}"/>
        <parameter name="toBlId" dataType="text" value=""/>
        <parameter name="toFlId" dataType="text" value=""/>
        <parameter name="toRmId" dataType="text" value=""/>
    </dataSource>
    
    <dataSource id="checkLocationsForInventoryFloorDs">
        <table name="rm" role="main" />
        <field name="bl_id"/>
        <field name="fl_id"/>
        <field name="rm_id"/>
        <restriction type="sql" sql="rm.bl_id=${parameters['toBlId']} and rm.fl_id=${parameters['toFlId']} and rm.rm_id=${parameters['toRmId']}"/>
        <parameter name="toBlId" dataType="text" value=""/>
        <parameter name="toFlId" dataType="text" value=""/>
        <parameter name="toRmId" dataType="text" value=""/>
    </dataSource>    

	<panel type="html" id="tipPanel" dataSource="none" layout="mainLayout" region="north">
        <instructions translatable="true">The list on the left are layouts that have been occupied in this move scenario. The layouts on the right are all layouts included in this move scenario. To copy an occupancy scenario from one layout to another, select the layout on the left with occupancy to copy (the \"source\"), and select the layout on the right into which the occupancy will be copied (the \"destination\"). Note that employees who occupy rooms that do not exist in the \"destination\" layout will return to the Employees to Locate list.</instructions>
	</panel>
	
	<panel type="grid" id="occupancyScenarioLayoutsGrid"  dataSource="occupancyScenarioLayoutsDs" showOnLoad="false" layout="subLayout" region="west" selectionEnabled="true" multipleSelectionEnabled="true">
        <title translatable="true">Layouts that Contain Occupants</title>
        <field table="mo_scenario_em" name="to_bl_id">
            <title>Building</title>
        </field>
        <field table="mo_scenario_em" name="to_fl_id">
            <title>Floor</title>
        </field>
        <field table="mo_scenario_em" name="filename"/>
    </panel>    
    
    <panel type="grid" id="moScenarioLayoutsGrid"  dataSource="moScenarioLayoutsDs" showOnLoad="false" layout="subLayout" region="center" selectionEnabled="true" multipleSelectionEnabled="true">
        <title translatable="true">Layouts in this Move Scenario</title>
        <action id="copyOccupancyScenario">
            <title>Copy</title>
        </action>
        <field table="mo_scenario_em" name="to_bl_id">
            <title>Building</title>
        </field>
        <field table="mo_scenario_em" name="to_fl_id">
            <title>Floor</title>
        </field>
        <field table="mo_scenario_em" name="filename"/>
    </panel>    
</view>