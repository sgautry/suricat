/**
 * The controller for the selected Teams.
 */
var spaceSelectedTeamsController = View.createController('spaceSelectedTeamsController', {
	
	projectId:"",
	
	scenarioId:"",
	
	asOfDate:"",
	
	/**
	 * Initialize the event listener refresh selected Teams grid.
	 */
	afterCreate: function() {
        this.on('app:mo:scenarios:console:refreshSelectedTeamsGrid', this.refreshSelectedTeamsGrid);
    },
	
	afterViewLoad:function() {
		var controller = this;
		this.selectedTeamsGrid.addEventListener('onMultipleSelectionChange', function(row) {
            controller.fireSelectedTeamRow(row);
        });
		
	},
	
	afterInitialDataFetch: function() {
		jQuery("#selectedTeamsGrid_layoutWrapper").removeClass();
		jQuery("#selectedTeamsGrid_head").removeClass();
		jQuery("#selectedTeamsGrid_title").hide();

		this.asOfDate = View.getOpenerView().dialogView.parameters.asOfDate;
		//get parameters from parent restriction
		this.projectId = View.getOpenerView().restriction['mo_scenario.project_id'];
		this.scenarioId = View.getOpenerView().restriction['mo_scenario.scenario_id'];
		
		this.refreshSelectedTeamsGrid();
	},
    
    refreshSelectedTeamsGrid: function() {
    	var clientRestrictions = View.parameters["clientRestrictions"];
    	this.selectedTeamsGrid.addParameter("team_rest1", clientRestrictions.team_rest1);
		this.selectedTeamsGrid.addParameter("team_rest2", clientRestrictions.team_rest2);
		this.selectedTeamsGrid.addParameter("projectId", this.projectId);
		this.selectedTeamsGrid.addParameter("scenarioId", this.scenarioId);
		this.selectedTeamsGrid.addParameter("asOfDate", this.asOfDate);
    	this.selectedTeamsGrid.refresh();
    },
	
	/**
	 * Handle the event when the checkbox next to the row is checked or unchecked.
	 */
	fireSelectedTeamRow: function(row) {
		var rows = this.selectedTeamsGrid.getSelectedRows();
		if (rows.length > 0) {
			this.trigger('app:mo:scenarios:console:refreshSelectedEmployeesGrid');
			this.trigger('app:mo:scenarios:console:refreshSelectedRoomsGrid');
			this.trigger('app:mo:scenarios:console:refreshSelectedTrialRoomsGrid');
		}
		this.trigger('app:mo:scenarios:console:onSelectedResourcesChanged', 'team', rows);
	},
	
	/**
	 * Disable the checkbox if the user is not in the SPACE-CONSOLE-ALL-ACCESS GROUP
	 */
	selectedTeamsGrid_afterRefresh: function() {
    	if (!View.user.isMemberOfGroup('SPACE-CONSOLE-ALL-ACCESS')) {
    		jQuery("#selectedTeamsGrid input[type=checkbox]").attr("disabled","true");
    	}
	}
});