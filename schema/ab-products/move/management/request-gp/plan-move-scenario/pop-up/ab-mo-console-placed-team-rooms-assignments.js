var abMoConsolePlacedTeamRoomAssignments = View.createController('abMoConsolePlacedTeamRoomAssignments', {
	
	projectId:"",
	
	scenarioId:"",
	
	afterViewLoad: function(){
		this.projectId = View.getOpenerView().getOpenerView().controllers.get("developMovesScenarios").projectId;
		this.scenarioId = View.getOpenerView().getOpenerView().controllers.get("developMovesScenarios").scenarioId;
	},
	
	afterInitialDataFetch:function(){
		this.placedTeamRoomAssignementsGrid.addParameter("projectId", this.projectId);
		this.placedTeamRoomAssignementsGrid.addParameter("scenarioId", this.scenarioId);
		
		this.placedTeamRoomAssignementsGrid.refresh();
	},
	
	placedTeamRoomAssignementsGrid_onRemovePlacedTeamRoomAssignment:function(row){
		try {
			Workflow.callMethod("AbMoveManagement-MoveService-removeAssignment", parseInt(row.record['mo_scenario_em.auto_number.key']), "team");

			this.placedTeamRoomAssignementsGrid.refresh();
		}
		catch (e) {
	    	Workflow.handleError(e);
		}		
	}
});