var abMoConsoleCopyOccupancyScenario = View.createController('abMoConsoleCopyOccupancyScenario', {   
	scenarioId:"",
	
	projectId:"",
	
	afterViewLoad: function(){
		this.projectId = View.getOpenerView().controllers.get("developMovesScenarios").projectId;
		this.scenarioId = View.getOpenerView().controllers.get("developMovesScenarios").scenarioId;
	},
	
	afterInitialDataFetch: function(){
		this.occupancyScenarioLayoutsGrid.addParameter("projectId", this.projectId);
		this.occupancyScenarioLayoutsGrid.addParameter("scenarioId", this.scenarioId);
		this.occupancyScenarioLayoutsGrid.refresh();
		
		this.moScenarioLayoutsGrid.addParameter("projectId", this.projectId);
		this.moScenarioLayoutsGrid.addParameter("scenarioId", this.scenarioId);
		this.moScenarioLayoutsGrid.refresh();
	},
	 
	occupancyScenarioLayoutsGrid_afterRefresh: function(){
		this.occupancyScenarioLayoutsGrid.enableSelectAll(false);
	},
	
	moScenarioLayoutsGrid_afterRefresh: function(){
		this.moScenarioLayoutsGrid.enableSelectAll(false);
	},
	
	occupancyScenarioLayoutsGrid_multipleSelectionColumn_onClick: function(row){
		var selected = row.isSelected();
		if(selected){
			this.occupancyScenarioLayoutsGrid.setAllRowsSelected(false);
			row.select(selected);
		}
	},
	
	moScenarioLayoutsGrid_multipleSelectionColumn_onClick: function(row){
		var selected = row.isSelected();
		if(selected){
			this.moScenarioLayoutsGrid.setAllRowsSelected(false);
			row.select(selected);
		}
	},
	moScenarioLayoutsGrid_onCopyOccupancyScenario: function(){
		//check both grids are checked
		var occupancyScenarioLayoutsRecords = this.occupancyScenarioLayoutsGrid.getSelectedRows();
		var moScenarioLayoutsRecords = this.moScenarioLayoutsGrid.getSelectedRows();
		
		if(occupancyScenarioLayoutsRecords.length==0||moScenarioLayoutsRecords.length==0){
			View.alert(getMessage("selectBothGridsInfo"));
			return;
		}
		
		//to scenario's filename
		var toFilename = moScenarioLayoutsRecords[0]["mo_scenario_em.filename"];		
		var toBlId = occupancyScenarioLayoutsRecords[0]["mo_scenario_em.to_bl_id"];
		var toFlId = occupancyScenarioLayoutsRecords[0]["mo_scenario_em.to_fl_id"];
		
		//from scenario's filename
		var fromFilename = occupancyScenarioLayoutsRecords[0]["mo_scenario_em.filename"];
		var fromBuilding = occupancyScenarioLayoutsRecords[0]["mo_scenario_em.to_bl_id"];
		var fromFloor = occupancyScenarioLayoutsRecords[0]["mo_scenario_em.to_fl_id"];
		var layout={};
		layout['mo_scenario_em.to_bl_id'] = fromBuilding;
		layout['mo_scenario_em.to_fl_id'] = fromFloor;
		layout['mo_scenario_em.filename'] = fromFilename;
		layout['mo_scenario_em.project_id'] = this.projectId;
		layout['mo_scenario_em.scenario_id'] =  this.scenarioId;

		try {
			var result = Workflow.callMethod('AbMoveManagement-MoveService-copyScenarioOccupancy', this.projectId, this.scenarioId, toFilename, toBlId, toFlId, layout);
			if ( result.code == "executed" ) {
				this.occupancyScenarioLayoutsGrid.refresh();
				this.moScenarioLayoutsGrid.refresh();
				View.alert(getMessage("finished"));
			}
		} 
		catch (e) {
			View.showMessage('error',e.message);
		}
	}	
});