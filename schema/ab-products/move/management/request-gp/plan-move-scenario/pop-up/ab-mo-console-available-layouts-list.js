var abMoConsoleAvailableLayoutsList = View.createController('abMoConsoleAvailableLayoutsList', {   
	scenarioId:"",
	projectId:"",

	afterViewLoad: function(){
		this.projectId = View.getOpenerView().controllers.get("developMovesScenarios").projectId;
		this.scenarioId = View.getOpenerView().controllers.get("developMovesScenarios").scenarioId;
	},
	
	availableLayoutsGrid_afterRefresh: function(param) {
		
		var param = param;
		this.availableLayoutsGrid.gridRows.each(function(row){
			
			// Bold or red-color the bl_id, fl_id that are associated with other scenarios.
			var renderType = row.record['mo_scenario_em.vf_renderType'];
			var columnNum = 5;
			switch(renderType)
			{
				case '11':
					for(var i=1;i<columnNum;i++){
						row.dom.cells[i].innerHTML = '<font style="font-weight:bold;color:red">'+row.dom.cells[i].innerText+'</font>';
					}
				  break;
				case '01':
					for(var i=1;i<columnNum;i++){
						row.dom.cells[i].innerHTML = '<font style="font-weight:bold">'+row.dom.cells[i].innerText+'</font>';
					}
				  break;
				case '10':
					for(var i=1;i<columnNum;i++){
						row.dom.cells[i].innerHTML = '<font style="color:red">'+row.dom.cells[i].innerText+'</font>';
					}
				  break;
			}
			
	   	});
	},
	
	availableLayoutsFilter_onFilterAvailableLayouts: function() {
		var blId = this.availableLayoutsFilter.getFieldValue("rm.bl_id");
		var flId = this.availableLayoutsFilter.getFieldValue("rm.fl_id");
		var rmId = this.availableLayoutsFilter.getFieldValue("rm.rm_id");
		var dvId = this.availableLayoutsFilter.getFieldValue("rm.dv_id");
		var dpId = this.availableLayoutsFilter.getFieldValue("rm.dp_id");
		
		this.availableLayoutsGrid.addParameter('blId', blId);
		this.availableLayoutsGrid.addParameter('flId', flId);
		this.availableLayoutsGrid.addParameter('rmId', rmId);
		this.availableLayoutsGrid.addParameter('dvId', dvId);
		this.availableLayoutsGrid.addParameter('dpId', dpId);
		
		this.availableLayoutsGrid.addParameter("projectId",this.projectId);
		this.availableLayoutsGrid.addParameter("scenarioId",this.scenarioId);
		this.availableLayoutsGrid.refresh();
    },
    
    availableLayoutsFilter_onClearLocations: function() {
    	this.availableLayoutsFilter.clear();
    },
    
    availableLayoutsGrid_onAddToScenario: function(){
    	
    	//Check whether layout scenario or inventory floor
    	var rows = this.availableLayoutsGrid.getSelectedRows();
    	if(rows.length==0){
    		View.alert(getMessage("select_one_item"));
    		return;
    	}
    	
    	var addedScenarios = this.checkBeforeAdd(rows);
    	if(addedScenarios.length>0){
    		var message = getMessage("addToScenarioCheckInfo").replace("{0}", addedScenarios.join());
    		View.alert(message);
    		return;
    	}
    		
    	//Add selected scenarios
    	this.addToScenario(rows);

    	//Refresh layout 
    	View.getOpenerView().panels.get("layoutsGrid").refresh();

    	//Close this pop-up
    	View.closeThisDialog();
    },
    
    checkBeforeAdd: function(rows){
    	var addedScenarios = [];
    	for (var i = 0; i < rows.length; i++) {
            var checkIfAddedToScenario = View.getOpenerView().dataSources.get('checkIfAddedToScenario');
            this.checkIfAddedToScenario.addParameter("projectId",View.getOpenerView().controllers.get("abMoConsoleLayoutsList").projectId);
            this.checkIfAddedToScenario.addParameter("scenarioId",View.getOpenerView().controllers.get("abMoConsoleLayoutsList").scenarioId);
            this.checkIfAddedToScenario.addParameter("filename",rows[i]['afm_enterprise_graphics.filename']);
            var recs = this.checkIfAddedToScenario.getRecords();
            if(recs.length>0){
            	addedScenarios.push(rows[i]['afm_enterprise_graphics.filename']);
            }
    	}
    	return addedScenarios;
    	
    },
    
    addToScenario: function(rows){
    	for (var i = 0; i < rows.length; i++) {
            var record = new Ab.data.Record({
    			'mo_scenario_em.project_id': this.projectId,
    			'mo_scenario_em.scenario_id': this.scenarioId,
    			'mo_scenario_em.to_bl_id': rows[i]['mo_scenario_em.to_bl_id'],
    			'mo_scenario_em.to_fl_id': rows[i]['mo_scenario_em.to_fl_id'],
    			'mo_scenario_em.filename': rows[i]['mo_scenario_em.filename']
    		}, true);
        	View.dataSources.get('addToScenarioDs').saveRecord(record);
    	}
    }
    
});