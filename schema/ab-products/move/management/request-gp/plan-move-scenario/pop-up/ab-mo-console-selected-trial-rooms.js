/**
 * The controller for the selected rooms.
 */
var spaceSelectedTrialRmsController = View.createController('spaceSelectedTrialRmsController', {
	
	/**
	 * Initialize the event listener refresh selected rooms grid.
	 */
	afterCreate: function() {
        this.on('app:mo:scenarios:console:refreshSelectedTrialRoomsGrid', this.refreshSelectedTrialRoomsGrid);
    },
	
	afterViewLoad:function() {
		var controller = this;
		this.selectedTrialRoomsGrid.addEventListener('onMultipleSelectionChange', function(row) {
            controller.fireSelectedTrialRoomRow(row);
        });
	},
	
	afterInitialDataFetch: function() {
		jQuery("#selectedTrialRoomsGrid_layoutWrapper").removeClass();
		jQuery("#selectedTrialRoomsGrid_head").removeClass();
		jQuery("#selectedTrialRoomsGrid_title").hide();
		
		this.refreshSelectedTrialRoomsGrid();
    },
    
    refreshSelectedTrialRoomsGrid: function() {
    	
    	var clientRestrictions = View.parameters["clientRestrictions"];
    	var layerName = View.parameters["layerName"];
		this.selectedTrialRoomsGrid.addParameter("rm_rest2", clientRestrictions.rm_rest2);
		this.selectedTrialRoomsGrid.addParameter("layerName", layerName);
    	this.selectedTrialRoomsGrid.refresh();
    },
	
	/**
	 * Handle the event when the checkbox next to the row is checked or unchecked.
	 */
	fireSelectedTrialRoomRow: function(row) {
		var rows = this.selectedTrialRoomsGrid.getSelectedRows();
		if (rows.length > 0) {
			this.trigger('app:mo:scenarios:console:refreshSelectedEmployeesGrid');
			this.trigger('app:mo:scenarios:console:refreshSelectedTeamsGrid');
		}
		this.trigger('app:mo:scenarios:console:onSelectedResourcesChanged', 'trialRoom', rows);
	},
	
	/**
	 * Disable the checkbox if the user is not in the SPACE-CONSOLE-ALL-ACCESS GROUP
	 */
	selectedTrialRoomsGrid_afterRefresh: function() {
    	if (!View.user.isMemberOfGroup('SPACE-CONSOLE-ALL-ACCESS')) {
    		jQuery("#selectedTrialRoomsGrid input[type=checkbox]").attr("disabled","true");
    	}
	}
});