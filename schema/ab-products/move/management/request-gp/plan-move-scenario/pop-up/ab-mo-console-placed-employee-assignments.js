var abMoConsolePlacedEmployeeAssignments = View.createController('abMoConsolePlacedEmployeeAssignments', {
	
	projectId:"",
	
	scenarioId:"",
	
	afterViewLoad: function(){
		this.projectId = View.getOpenerView().getOpenerView().controllers.get("developMovesScenarios").projectId;
		this.scenarioId = View.getOpenerView().getOpenerView().controllers.get("developMovesScenarios").scenarioId;
	},
	
	afterInitialDataFetch:function(){
		this.employeePlacedAssignmentsGrid.addParameter("projectId", this.projectId);
		this.employeePlacedAssignmentsGrid.addParameter("scenarioId", this.scenarioId);
		
		this.employeePlacedAssignmentsGrid.refresh();
	},
	
	employeePlacedAssignmentsGrid_onRemoveEmployeePlacedAssignment:function(row){
		try {
			Workflow.callMethod("AbMoveManagement-MoveService-removeAssignment", parseInt(row.record['mo_scenario_em.auto_number.key']), "employee");

			this.employeePlacedAssignmentsGrid.refresh();

			var emToLocateGrid = View.getOpenerView().getOpenerView().panels.get("emToLocateGrid");
			if (emToLocateGrid) {
				emToLocateGrid.refresh();
			}

			var drawingController = View.getOpenerView().getOpenerView().controllers.get("spaceExpressConsoleDrawing");
			if (drawingController) {
				drawingController.refreshEmployeeWaitingPanel();
			}
		}
		catch (e) {
	    	Workflow.handleError(e);
		}			
	}
});