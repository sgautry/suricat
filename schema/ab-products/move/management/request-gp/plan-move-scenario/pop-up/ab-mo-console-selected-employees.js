
/**
 * Controller for the selected employees from the details pop up window.
 */
var selectedRmsEmsController = View.createController('selectedRmsEmsController',{
	
	projectId:"",
	
	scenarioId:"",
	
	/**
	 * Initialize the event listener refresh selected rooms grid.
	 */
	afterCreate: function() {
        this.on('app:mo:scenarios:console:refreshSelectedEmployeesGrid', this.refreshSelectedEmployeesGrid);
    },
	
	afterInitialDataFetch: function() {
		jQuery("#selectedRmsEmsGrid_layoutWrapper").removeClass();
		jQuery("#selectedRmsEmsGrid_head").removeClass();
		jQuery("#selectedRmsEmsGrid_title").hide();
		
		this.refreshSelectedEmployeesGrid();
		
	},
	
	afterViewLoad:function() {
		var controller = this;
		this.selectedRmsEmsGrid.addEventListener('onMultipleSelectionChange', function(row) {
            controller.fireSelectedEmployeeRow(row);
        });
		
		//get parameters from parent restriction
		this.projectId = View.getOpenerView().restriction['mo_scenario.project_id'];
		this.scenarioId = View.getOpenerView().restriction['mo_scenario.scenario_id'];
	},
	
	refreshSelectedEmployeesGrid: function() {
		var clientRestrictions = View.parameters["clientRestrictions"];
		
		this.selectedRmsEmsGrid.addParameter("em_rest1", clientRestrictions.em_rest1);
		this.selectedRmsEmsGrid.addParameter("em_rest2", clientRestrictions.em_rest2);
		this.selectedRmsEmsGrid.addParameter("projectId", this.projectId);
		this.selectedRmsEmsGrid.addParameter("scenarioId", this.scenarioId);
		this.selectedRmsEmsGrid.refresh();
	},
	
	/**
	 * Handle the event when the checkbox next to the row is checked or unchecked.
	 */ 
	fireSelectedEmployeeRow: function(row) {
		var rows = this.selectedRmsEmsGrid.getSelectedRows();
		if (rows.length > 0) {
			this.trigger('app:mo:scenarios:console:refreshSelectedRoomsGrid');
			this.trigger('app:mo:scenarios:console:refreshSelectedTeamsGrid');
			this.trigger('app:mo:scenarios:console:refreshSelectedTrialRoomsGrid');
		}
		this.trigger('app:mo:scenarios:console:onSelectedResourcesChanged', 'employee', rows);
	},
	
	/**
	 * Disable the checkbox if the user is not in the SPACE-CONSOLE-ALL-ACCESS GROUP
	 */
	selectedRmsEmsGrid_afterRefresh: function(panel) {
    	if (!View.user.isMemberOfGroup('SPACE-CONSOLE-ALL-ACCESS')) {
    		jQuery("#selectedRmsEmsGrid input[type=checkbox]").attr("disabled","true");
    		return;
    	}
		//hide checkbox for records from mo_scenario_em table
    	panel.gridRows.each(function (row) {
            if (row.getRecord().getValue('em.vf_table_name') === 'mo_scenario_em') {
            	row.dom.cells[0].childNodes[0].hidden = true;
            }
        }, this);
	}
});