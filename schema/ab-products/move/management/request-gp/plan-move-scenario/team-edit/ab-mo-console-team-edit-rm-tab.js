/**
 * Controller for the Rooms tab.
 * @Author Jikai Xu
 *
 * Events:
 * app:teamspace:editteam:changedate
 */
var rmOnTeamController = View.createController('rmOnTeamController',{
	
	teamId:"",
	
	dateStartReq:"",
	
	asOfDate: "",
	
	projectId: "",
	
	afterViewLoad: function(){
		this.projectId = View.getOpenerView().controllers.get("teamEditController").projectId;
	},
	
    /**
     * Initializing
     */
	afterInitialDataFetch : function(){
		this.teamId =View.getOpenerView().parameters['teamId'];
		
		//fetch teamId from team properties when adding teams
		if(!valueExistsNotEmpty(this.teamId)){
			
			this.teamId = View.parentTab.parentPanel.tabs[0].
			getContentFrame().View.controllers.get('employeeOnTeamController').teamId;
			
		}
		
		this.projectId = View.getOpenerView().controllers.get("teamEditController").projectId;
		this.dateStartReq = View.getOpenerView().controllers.get("teamEditController").dateStartReq;
		this.asOfDate = View.getOpenerView().controllers.get("teamEditController").asOfDate;
		
		
		//display rooms on team and available tabs
		this.refreshRoomsTab();
		
		//add unit for area
		jQuery("#area").siblings("span").html(View.user.areaUnits.title);

		//disable 'add selected' button
//		this.rmAvailablePanel.actions.get("addSelected").enable(false);
		
		this.rmPanel.setTitle(getMessage("rooms_assigned_to_team")+" "+ this.teamId);
		
		//load rooms removed from team 
		this.displayRoomsRemovedFromTeam();
		
		//load rooms added to team
		this.displayRoomsAddedToTeam();
		
	},

	/**
	 * calculate the statistics
	 */
	calculateStatistics: function(selectedDate){
		var teamEditController = View.getOpenerView().controllers.get('teamEditController');
		
		//not for define teams
		if(teamEditController){
			teamEditController.calculateStatistics(selectedDate);
		}
		
	},
	
	/**
	 * delete the clicked item
	 */
	rmPanel_onRemove: function(row) {
			
		var selectedRms = [];
		var selectedRm = {'bl_id':row.record["rm_team.bl_id"],
        		'fl_id':row.record["rm_team.fl_id"],
        		'rm_id':row.record["rm_team.rm_id"]
        	};
		selectedRms.push(selectedRm);
		
		this.removeRoomsFromTeam(selectedRms);
	
	},
	
    /**
     * Remove multiple selected employees from a team
     */
    rmPanel_onRemoveSelected: function(){
    	
    	var rows = this.rmPanel.getSelectedGridRows();
		var len = rows.length;
		
		if(len>0){
			
			var selectedRms = [];
			for ( var i = 0; i < len; i++) {

		    	var selectedRm = {'bl_id':rows[i].record["rm_team.bl_id"],
		        		'fl_id':rows[i].record["rm_team.fl_id"],
		        		'rm_id':rows[i].record["rm_team.rm_id"]
		        	};
				selectedRms.push(selectedRm);
			}
	    	
			this.removeRoomsFromTeam(selectedRms);
		}
		
    	
    },	
    
    removeRoomsFromTeam: function(selectedRms){
    	
    	try {
			var result = Workflow.callMethod("AbMoveManagement-MoveService-removeRoomsFromTeam", selectedRms, this.teamId, this.projectId, 'Team', this.dateStartReq, this.asOfDate);
		}
		catch (e) {
	    	Workflow.handleError(e);
		}			
		
		//refresh
		this.displayRoomsOnTeam();
		this.displayRoomsRemovedFromTeam();
		
		//recalculate
		this.recalculateStatistics();
    },
	
    /**
     * change the add selected button's state according to selected rooms
     */
	rmAvailablePanel_onMultipleSelectionChange: function(row) {
		
	    var rows = this.rmAvailablePanel.getSelectedGridRows();
	    
	    if(rows.length>0){
	    	this.rmAvailablePanel.actions.get("addSelected").enable(true);
	    }else{
	    	this.rmAvailablePanel.actions.get("addSelected").enable(false);
	    }
	},
	
    /**
     * assign multiple selected rooms to a team
     */
    rmAvailablePanel_onAddSelected: function(){
    	var rows = this.rmAvailablePanel.getSelectedGridRows();
		var len = rows.length;
		var selectedRms = [];
		if(len>0){
			
			for ( var i = 0; i < len; i++) {

		    	var selectedRm = {'bl_id':rows[i].record["rm.bl_id"],
		        		'fl_id':rows[i].record["rm.fl_id"],
		        		'rm_id':rows[i].record["rm.rm_id"]
		        	};
				selectedRms.push(selectedRm);
			}
	    	
			this.assignRooms(selectedRms);
		}
    },
    
    /**
     * assign single room to a team
     */
    rmAvailablePanel_onAdd: function(row) {
    	var selectedBlId = row.record['rm.bl_id'];
    	var selectedFlId = row.record['rm.fl_id'];
    	var selectedRmId = row.record['rm.rm_id'];
    	var selectedRms = [{
    		'bl_id':selectedBlId,
    		'fl_id':selectedFlId,
    		'rm_id':selectedRmId
    	}];
    	this.assignRooms(selectedRms);
    	
    },
    
    assignRooms: function(selectedRms){
    	
    	try {
			var result = Workflow.callMethod("AbMoveManagement-MoveService-assignRoomsToTeam", selectedRms, this.teamId, this.projectId, 'Team', this.dateStartReq, this.asOfDate);
		}
		catch (e) {
	    	Workflow.handleError(e);
		}			
		//refresh
        this.displayRoomsAddedToTeam();
        this.displayAvailableRooms();
        
    	//recalculate
    	this.recalculateStatistics();
    },
    
    /**
     * refresh rooms tab
     * @param isInitial
     * @param isRecalculate
     */
	refreshRoomsTab: function(isInitial, isRecalculate) {
		
		View.openProgressBar(View.getLocalizedString(this.z_PROGRESS_MESSAGE),config = {
    	    interval: 500
		});
	
		this.refreshRoomsTabAfterOpenProgressBar.defer(500, this);
		
	},
	
    /**
     * refresh rooms tab
     * @param isInitial
     * @param isRecalculate
     */
	refreshRoomsTabAfterOpenProgressBar: function(){
		try {
			
			this.displayRoomsOnTeam();
			
			this.displayAvailableRooms();
				
			View.closeProgressBar();
			
		} catch (e) {
			View.closeProgressBar();
		}
	},
	
    /**
     * show rooms on team according to as of date
     */
    displayRoomsOnTeam: function(){
		
    	this.rmPanel.addParameter("asOfDate", this.asOfDate);
    	this.rmPanel.addParameter("editTeamId", this.teamId);
    	this.rmPanel.addParameter("projectId", this.projectId);
		this.rmPanel.refresh();
    },
    
	/**
	 * show available rooms
	 */
	displayAvailableRooms: function(){

    	this.rmAvailablePanel.addParameter("asOfDate", this.asOfDate);
    	this.rmAvailablePanel.addParameter("editTeamId", this.teamId);
    	this.rmAvailablePanel.addParameter("projectId", this.projectId);
		this.rmAvailablePanel.refresh();
	},
	
	
	/**
	 * show employees moving from team
	 */
	displayRoomsRemovedFromTeam: function(){
		
		var restriction = new Ab.view.Restriction();
		restriction.addClause('mo.from_bl_id', "", "IS NOT NULL");
		restriction.addClause('mo.from_fl_id', "", "IS NOT NULL");
		restriction.addClause('mo.from_rm_id', "", "IS NOT NULL");
		restriction.addClause('mo.to_team_id', this.teamId);
		restriction.addClause('mo.project_id', this.projectId);
		this.rmFromTeamPanel.addParameter("blId", "mo.from_bl_id");
		this.rmFromTeamPanel.addParameter("flId", "mo.from_fl_id");
		this.rmFromTeamPanel.addParameter("rmId", "mo.from_rm_id");
		this.rmFromTeamPanel.refresh(restriction);
	},
	
	/**
	 * show employees moving to team
	 */
	displayRoomsAddedToTeam: function(){
		
		var restriction = new Ab.view.Restriction();
		restriction.addClause('mo.to_bl_id', "", "IS NOT NULL");
		restriction.addClause('mo.to_fl_id', "", "IS NOT NULL");
		restriction.addClause('mo.to_rm_id', "", "IS NOT NULL");
		restriction.addClause('mo.to_team_id', this.teamId);
		restriction.addClause('mo.project_id', this.projectId);
		this.rmToTeamPanel.addParameter("blId", "mo.to_bl_id");
		this.rmToTeamPanel.addParameter("flId", "mo.to_fl_id");
		this.rmToTeamPanel.addParameter("rmId", "mo.to_rm_id");
		this.rmToTeamPanel.refresh(restriction);
		
	},
	
	/**
	 * refresh panel for edit teams
	 * @param asOfDate get from statistics panel
	 */
	refreshPanelsForEditTeams: function(asOfDate){
		this.asOfDate = asOfDate;
		this.clearNormalFields();
		this.rmFilterOptions.setFieldValue("rm_team.date_start",this.asOfDate);
		this.rmFilterOptions.setFieldValue("rm_team.date_end","");
		
		this.refreshRoomsTab(false, true);
		
	},
    
	/**
	 * recalculate statistics
	 * 
	 */
	recalculateStatistics: function(){
		var statisticsController = View.getOpenerView().controllers.get('statisticsController');
		statisticsController.calculateTeamStatistics();
	},
	
    /**
     * Remove rooms from Rooms Removed From Team panel(mo)
     */
	rmFromTeamPanel_onRevertSelected: function(){
		
		var rmIds = this.rmFromTeamPanel.getPrimaryKeysForSelectedRows();
		this.removeRoomsFromMo(rmIds);
		
		//refresh
		this.displayRoomsOnTeam();
		this.displayRoomsRemovedFromTeam();
		
		//recalculate
		this.recalculateStatistics();
    },
    
    /**
     * Remove employees from Rooms Added To Team panel(mo)
     */
    rmToTeamPanel_onRevertSelected: function(){
    	
		var rmIds = this.rmToTeamPanel.getPrimaryKeysForSelectedRows();
		this.removeRoomsFromMo(rmIds);
		
		//refresh
		this.displayAvailableRooms();
		this.displayRoomsAddedToTeam();
		 
		//recalculate
		this.recalculateStatistics();
    },
    
    removeRoomsFromMo: function(rmIds){

        try {
        	var len = rmIds.length;
        	
    		for ( var i = 0; i < len; i++) {
    			var record = new Ab.data.Record(rmIds[i], true);
    			
    			this.removeRoomFromDs.deleteRecord(record);
    		}		
    		
        } 
        catch (e) {
        	Workflow.handleError(e);
        }
    },
    
    rmFromTeamPanel_afterRefresh:function(){
    	this.convertVirtualAreaField(this.rmFromTeamPanel);
    },
    
    rmToTeamPanel_afterRefresh:function(){
    	this.convertVirtualAreaField(this.rmToTeamPanel);
    },
    
    convertVirtualAreaField:function(grid){
    	var conversionFactor=1.0;
    	//identify whether the units of database and system are the same
		if(View.user.displayUnits != View.project.units){
			
			conversionFactor= 1.0/parseFloat(View.user.areaUnits.conversionFactor);
			
	    	grid.gridRows.each(function(row) {
	    		var convertedValue = parseFloat(row.getRecord().getValue('mo.vf_area'))*conversionFactor;
	    		row.setFieldValue("mo.vf_area", convertedValue.toFixed(2));
	        });
		}

    },
    
    openPaginatedReport: function(){  
    	var restriction1 = new Ab.view.Restriction();
		restriction1.addClause('mo.from_bl_id', "", "IS NOT NULL");
		restriction1.addClause('mo.from_fl_id', "", "IS NOT NULL");
		restriction1.addClause('mo.from_rm_id', "", "IS NOT NULL");
		restriction1.addClause('mo.to_team_id', this.teamId);
		restriction1.addClause('mo.project_id', this.projectId);
		
    	var restriction2 = new Ab.view.Restriction();
		restriction2.addClause('mo.to_bl_id', "", "IS NOT NULL");
		restriction2.addClause('mo.to_fl_id', "", "IS NOT NULL");
		restriction2.addClause('mo.to_rm_id', "", "IS NOT NULL");
		restriction2.addClause('mo.to_team_id', this.teamId);
		restriction2.addClause('mo.project_id', this.projectId);
		
		var parameters = {};  
		parameters['editTeamId'] = this.teamId;
		parameters['projectId'] = this.projectId;
		parameters['asOfDate'] = this.asOfDate;
		
		//paired dataSourceId with Restriction objects
		var passedRestrictions = {'rooms_moving_from_team_ds': restriction1,'rooms_moving_to_team_ds': restriction2};
		
		//passing restrictions
		View.openPaginatedReportDialog("ab-mo-console-team-edit-rm-print.axvw", passedRestrictions, parameters);	
    }
});

