var abMoConsoleTeamEditStatisticsBuildingsController = View.createController('abMoConsoleTeamEditStatisticsBuildingsController', {
	
	asOfDate:"",
	
	projectId:"",
	
	teamId:"",

	afterViewLoad:function(){
		this.asOfDate = View.getOpenerView().controllers.get("statisticsController").asOfDate;
		this.projectId = View.getOpenerView().controllers.get("statisticsController").projectId;
		this.teamId = View.getOpenerView().controllers.get("statisticsController").teamId;
	},
	
	afterInitialDataFetch:function(){
		this.buildingListGrid.addParameter("projectId", this.projectId);
		this.buildingListGrid.addParameter("teamId", this.teamId);
		this.buildingListGrid.addParameter("dateCon", this.asOfDate);
		
		this.buildingListGrid.refresh();
	}
	
	
	
});