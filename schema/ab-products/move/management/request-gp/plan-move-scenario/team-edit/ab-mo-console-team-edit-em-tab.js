/**
 * Controller for the employees tab.
 * @Author Jikai Xu
 * Events:
 * app:teamspace:editteam:changedate
 */
var employeeOnTeamController = View.createController('employeeOnTeamController', {
	
	teamId:"",
	
	asOfDate: "",
	
	dateStartReq: "",
	
	projectId: "",
	
	//store the from date to check when adding employees
	fromDateInFilter:"",
	
	//store the to date to check when adding employees
	toDateInFilter:"",
	
	//to identify the phase for filter(from date input is empty when initializing)
	isInitial: false,
	
	afterViewLoad: function(){
		this.projectId = View.getOpenerView().controllers.get("teamEditController").projectId;
		this.dateStartReq = View.getOpenerView().controllers.get("teamEditController").dateStartReq;
		this.asOfDate = View.getOpenerView().controllers.get("teamEditController").asOfDate;
	},
	
	/**
	 * initializing
	 */
	afterInitialDataFetch : function(){
		
		if(valueExistsNotEmpty(View.getOpenerView().controllers.get('propertiesController'))){
			//for edit team's function
			this.teamId =View.getOpenerView().controllers.get('propertiesController').teamId;
			
		}
		
		//render the tab if in edit mode, or not render until a new team added
		if(valueExistsNotEmpty(this.teamId)){
			//display employees on team and available employees
			this.refreshEmployeesTab();
		}
		
		//load employees moving from team 
		this.displayEmployeesMovingFromTeam();
		
		//load employees moving to team
		this.displayEmployeesMovingToTeam();
		
		//disable 'add selected' button
		this.emAvailablePanel.actions.get("addSelected").enable(true);
		//this.appendUnassignedChbToTitleBar();
		
		this.emPanel.setTitle(getMessage("employees_on_team")+" "+ this.teamId);
		
	},

	/**
	 * notify teamEditController to calculate the statistics
	 */
	calculateStatistics: function(selectedDate){

		var teamEditController = View.getOpenerView().controllers.get('teamEditController');
		
		//not for define teams
		if(teamEditController){
			
			teamEditController.calculateStatistics(selectedDate);
		}
	},
	
	/**
	 * render the field of teams to show team list on employees on team panel
	 */
	emPanel_afterRefresh: function(){
		var employeeOnTeamController = this;
		for (var x=0, column; column = this.emPanel.columns[x]; x++) {
			if ( column.id==='team.teams' )	{
				if (!column.hidden){
					this.emPanel.gridRows.each( function(row) {
				   		var emId = row.getFieldValue('team.em_id');
						emId = employeeOnTeamController.getTeams(emId, false);
						row.setFieldValue("team.teams", emId );
				   	});
				}
			}
		}
	   	
	},
	
	/**
	 * render the field of teams to show team list on available employees
	 */
	emAvailablePanel_afterRefresh: function(){
		var employeeOnTeamController = this;
		for (var x=0, column; column = this.emAvailablePanel.columns[x]; x++) {
			if ( column.id==='em.teams' )	{
				if (!column.hidden){
					this.emAvailablePanel.gridRows.each( function(row) {
				   		var emId = row.getFieldValue('em.em_id');
						emId = employeeOnTeamController.getTeams(emId, true);
						row.setFieldValue("em.teams", emId );
				   	});
				}
			}
		}
		
	},
	
	/**
	 * remove one item on the grid
	 * @param row
	 */
	emPanel_onRemove: function(row) {
		var emId = row.record['team.em_id'];
 
		var selectedEmIds = [];
		selectedEmIds.push(emId);
		
		this.removeEmployeesFromTeam(selectedEmIds);
		
	},	   
	
	
    /**
     * Remove multiple selected employees from a team
     */
    emPanel_onRemoveSelected: function(){
    	
    	var rows = this.emPanel.getSelectedGridRows();
		var len = rows.length;
		
		if(len>0){
			var selectedEmIds = [];
			for ( var i = 0; i < len; i++) {
				selectedEmIds.push(rows[i].record["em.em_id"]);
			}

			this.removeEmployeesFromTeam(selectedEmIds);
		}
    	
    },	

    removeEmployeesFromTeam: function(selectedEmIds){
    	
    	try {
			var result = Workflow.callMethod("AbMoveManagement-MoveService-removeEmployeesFromTeam", selectedEmIds, this.teamId, this.projectId, 'Team', this.dateStartReq, this.asOfDate);
		}
		catch (e) {
	    	Workflow.handleError(e);
		}	
		
		//refresh
		this.displayEmployeesMovingFromTeam();
		this.displayEmployeesOnTeam();
		
		//recalculate
		this.recalculateStatistics();
    },
    
	/**
	 * set field value in the form for multiple records
	 */
	setFieldValue : function(fieldName, elementName, rows, fldHtmlId) {
		var fieldValue = rows[0].record[fieldName];
		for ( var i = 0; i < rows.length; i++) {
			if (rows[i].record[fieldName] != fieldValue) {
				fieldValue = attachAngleBracket(getMessage('varies'));
				break;
			}
		}
		if(fieldValue == attachAngleBracket(getMessage('varies')))
		{
			if(fldHtmlId.indexOf('date')>0||fldHtmlId.indexOf('pct_time')>0)
			{
				$(fldHtmlId).value = fieldValue;
				//kb# 3052896: set property onblur of date input to null to 
				//avoid resetting the ‘<VARIES>’ string to a current date when losing focus on the date input.
				$(fldHtmlId).onblur = null;
			}
			else
			{
				$(fldHtmlId).innerText = fieldValue;
			}
			
		}
		else
		{
			this.editMembershipDatePanel.setFieldValue(elementName, fieldValue, fieldValue, false);
		}
	},
	
	/**
	 * get the teams that an employee has been added to 
	 * @param emId
	 * @param isAvailablePanel
	 * @return teamId
	 */
	getTeams : function(emId, isAvailablePanel){
		
		var startDate = this.asOfDate;
		var endDate = "";
		
		var teamList = "";
		
		var emTeamsDs = View.dataSources.get('employeeTeamsDS');
		emTeamsDs.addParameter('emId', emId);
		if(isAvailablePanel){
			emTeamsDs.addParameter('teamIdRes', "1=1");
		}else{
			emTeamsDs.addParameter('teamIdRes', "team.team_id!='"+this.teamId+"'");
		}
		
		emTeamsDs.addParameter('fromDate', startDate);
		emTeamsDs.addParameter('toDate', endDate);
		
		var teams = emTeamsDs.getRecords();
		
		for (var i=0; i<teams.length; i++ ){
			teamList += teams[i].getValue('team.team_id')+",";
		}
		teamList = teamList.substring(0, teamList.length-1);
		
		return teamList;

	},
	
	/**
	 * display teams which an employee was assigned to
	 */
	getTeamList: function(panelId){
		var emPanel = View.panels.get(panelId);
		var index = emPanel.selectedRowIndex;
		var row = emPanel.gridRows.get(index);
		
		var teamRes=new Ab.view.Restriction();
		
		var emId = row.getFieldValue("em.em_id");
		var isAvailablePanel = false;
		if(panelId=="emPanel"){
			emId = row.getFieldValue("team.em_id");
			isAvailablePanel = false;
		}
		else if(panelId=="emAvailablePanel"){
			emId = row.getFieldValue("em.em_id");
			isAvailablePanel = true;
		}
		
		teamRes.addClause('team.em_id',emId,'=');
		teamRes.addClause('team.team_id', '', 'IS NOT NULL');
		
		View.openDialog("ab-sp-console-team-edit-em-teams.axvw", teamRes, false, {
			width: 1000,
			height: 800,
			title: getMessage("team_list"),
			closeButton: false
		});
	},	
	
    /**
     * add custom checkbox for unassigned to any team
     */
    appendUnassignedChbToTitleBar: function(){
    	var panelTitleNode = this.emAvailablePanel.getTitleEl().dom.parentNode.parentNode.parentNode;
    	var tr = Ext.DomHelper.append(panelTitleNode, {
    		tag : 'tr',
    		id : 'unassignedChbTr'
    	});
    	var cell = Ext.DomHelper.append(tr, {
    		tag : 'td',
    		id : 'unassignedChbTd'
    	});
    	var div = "<div class='x-toolbar x-small-editor panelToolbar' id='unassignedChbTitlebar'/>";
        var divCell = Ext.DomHelper.append(cell, div , true);
        var str = "<div class='checkbox-container'>"+"<input type='checkbox' id='emUnassigned'/>"+"<span translatable='true'>"+View.getLocalizedString(getMessage("unassigned_checkbox"))+"</span></div>";
        var unassignedChb = Ext.DomHelper.append(divCell, str , true);
        
        document.getElementById('emUnassigned').onclick=function(){
        	var isChecked = $('emUnassigned').checked;
        	var employeeOnTeamController = View.controllers.get("employeeOnTeamController");
        	employeeOnTeamController.getUnassignedEmployees();
        	
        };
    },
    
    /**
     * get unassigned employees
     */
    getUnassignedEmployees: function(){
    	
    	var isChecked = $('emUnassigned').checked;
    	var emAvailablePanel = View.panels.get("emAvailablePanel");
    	
        if(isChecked){
        	var dateRange = this.getDateRangeOfFilter();
    		var startDate = dateRange.startDate;
    		var endDate = dateRange.endDate;
    		//transfer sql statement in js to named filters in axvw
			emAvailablePanel.addParameter('unassigned', true);
        }
        else{
        	emAvailablePanel.addParameter('unassigned', false);
        }
        
        emAvailablePanel.refresh();
		emAvailablePanel.show();
    	
    },
    
    /**
     * change the add selected button's state according to selected employees
     */
    emAvailablePanel_onMultipleSelectionChange: function(row) {
		
	    var rows = this.emAvailablePanel.getSelectedGridRows();
	    
	    if(rows.length>0){
	    	this.emAvailablePanel.actions.get("addSelected").enable(true);
	    }else{
	    	this.emAvailablePanel.actions.get("addSelected").enable(false);
	    }
	},
	
    /**
     * assign multiple selected employees to a team
     */
    emAvailablePanel_onAddSelected: function(){
    	
    	var rows = this.emAvailablePanel.getSelectedGridRows();
		var len = rows.length;
		
		if(len>0){
			var selectedEmIds = [];
			
			for ( var i = 0; i < len; i++) {
				selectedEmIds.push(rows[i].record["em.em_id"]);
			}
			this.assignEmployees(selectedEmIds);
		}
    	
    },
    
    
    /**
     * assign single employee to a team
     */
    emAvailablePanel_onAdd: function(row) {

    	var selectedEmIds = [row.record['em.em_id']];
    	this.assignEmployees(selectedEmIds);
    	
    },
    
    assignEmployees: function(selectedEmIds){
    	try {
			var result = Workflow.callMethod("AbMoveManagement-MoveService-assignEmployeesToTeam", selectedEmIds, this.teamId, this.projectId, 'Team', this.dateStartReq, this.asOfDate);
		}
		catch (e) {
	    	Workflow.handleError(e);
		}			
		//refresh
		this.displayEmployeesMovingToTeam();
		this.displayAvailableEmployees();
    	//recalculate
    	this.recalculateStatistics();
    },
    
    /**
     * refresh employees tab
     * @param isInitial
     * @param isRecalculate
     */
	refreshEmployeesTab: function() {
		
		View.openProgressBar(View.getLocalizedString(this.z_PROGRESS_MESSAGE),config = {
	    	    interval: 500
    	});
		
		this.refreshEmployeesTabAfterOpenProgressBar.defer(500, this);
		
	},
	
    /**
     * refresh employees tab
     * @param isInitial
     * @param isRecalculate
     */
	refreshEmployeesTabAfterOpenProgressBar: function(){
		try {
			
			this.displayEmployeesOnTeam();
			
			this.displayAvailableEmployees();
			
			View.closeProgressBar();
		} catch (e) {
			View.closeProgressBar();
			View.showMessage('error', '', e.message, e.data);
		}
	},
	
	/**
     * show employees on team according to as of date
     */
    displayEmployeesOnTeam: function(){
		
    	this.emPanel.addParameter("asOfDate", this.asOfDate);
    	this.emPanel.addParameter("editTeamId", this.teamId);
    	this.emPanel.addParameter("projectId", this.projectId);
		this.emPanel.refresh();
    },
    
	/**
	 * show available employees
	 */
	displayAvailableEmployees: function(){

    	this.emAvailablePanel.addParameter("asOfDate", this.asOfDate);
    	this.emAvailablePanel.addParameter("editTeamId", this.teamId);
    	this.emAvailablePanel.addParameter("projectId", this.projectId);
		this.emAvailablePanel.refresh();
	},
	
	/**
	 * show employees moving from team
	 */
	displayEmployeesMovingFromTeam: function(){
		var restriction = new Ab.view.Restriction();
		restriction.addClause('mo.em_id', "", "IS NOT NULL");
		restriction.addClause('mo.from_team_id', this.teamId);
		restriction.addClause('mo.project_id', this.projectId);
		this.emFromTeamPanel.refresh(restriction);
		
	},
	
	/**
	 * show employees moving to team
	 */
	displayEmployeesMovingToTeam: function(){
		var restriction = new Ab.view.Restriction();
		restriction.addClause('mo.em_id', "", "IS NOT NULL");
		restriction.addClause('mo.to_team_id', this.teamId);
		restriction.addClause('mo.project_id', this.projectId);
		this.emToTeamPanel.refresh(restriction);
	},
	
	/**
	 * get employees records for preparing sql statement
	 */
	getEmployeesRecords: function(con){
		this.employee_on_team_ds.addParameter('conForTeamsTab', con);
		var records = this.employee_on_team_ds.getRecords();
		var emId = "";
		for(var i = 0; i < records.length; i++) {
			var record = records[i];
			emId += "'"+record.getValue('em.em_id')+"',";
		}
		return emId.substring(0,emId.length-1);
	},
	
    /**
     * get as of date from team space console's filter .
     */
	getCurrentAsOfDate: function(){
		var currentAsOfDate = "";
		var teamEditController = View.getOpenerView().controllers.get('teamEditController');
		if(valueExistsNotEmpty(teamEditController)){
			currentAsOfDate = teamEditController.asOfDate;
		}
		else{
			currentAsOfDate = getCurrentDateInISOFormat();
		}
		return currentAsOfDate;
	},
	
	/**
	 * invoked by define teams after select a new team and select this tab
	 * @param isInitial
	 * @param teamId
	 */
	refreshPanels: function(isInitial,teamId){
		this.teamId = teamId;
		this.refreshEmployeesTab(isInitial, true);
	},
	
	/**
	 * refresh panel for define teams
	 * @param isInitial
	 * @param teamId
	 */
	refreshPanelsForDefineTeams: function(isInitial, teamId){
		this.refreshPanels(isInitial, teamId);
		
		//clear filter fields after refresh
		this.clearFields(true);
		
	},
	
	/**
	 * get from date and to date of filter
	 * @return dateRange object
	 */
	getDateRangeOfFilter : function(){
		var dateRange = {};
		dateRange.startDate = "";
		dateRange.endDate = "";
    	return dateRange;
	},
	
	/**
	 * recalculate statistics
	 * 
	 */
	recalculateStatistics: function(){
		var statisticsController = View.getOpenerView().controllers.get('statisticsController');
		statisticsController.calculateTeamStatistics();
	},
	
    /**
     * Remove employees from Employees Moving From Team panel(mo)
     */
    emFromTeamPanel_onRevertSelected: function(){
		
		var emIds = this.emFromTeamPanel.getPrimaryKeysForSelectedRows();
		this.removeEmployeesFromMo(emIds);
		
		//refresh
		this.displayEmployeesMovingFromTeam();
		this.displayEmployeesOnTeam();
		
		//recalculate
		this.recalculateStatistics();
    },
    
    /**
     * Remove employees from Employees Moving To Team panel(mo)
     */
    emToTeamPanel_onRevertSelected: function(){
    	
		var emIds = this.emToTeamPanel.getPrimaryKeysForSelectedRows();
		this.removeEmployeesFromMo(emIds);
		
		//refresh
		this.displayEmployeesMovingToTeam();
		this.displayAvailableEmployees();
		
		//recalculate
		this.recalculateStatistics();
    },
    
    removeEmployeesFromMo: function(emIds){

        try {
        	var len = emIds.length;
        	
    		for ( var i = 0; i < len; i++) {
    			var record = new Ab.data.Record(emIds[i], true);
    			
    			this.removeEmployeeFromDs.deleteRecord(record);
    		}		
    		
        } 
        catch (e) {
        	Workflow.handleError(e);
        }
    },
    
    openPaginatedReport: function(){  
    	var restriction1 = new Ab.view.Restriction();
		restriction1.addClause('mo.em_id', "", "IS NOT NULL");
		restriction1.addClause('mo.from_team_id', this.teamId);
		restriction1.addClause('mo.project_id', this.projectId);
		
    	var restriction2 = new Ab.view.Restriction();
		restriction2.addClause('mo.em_id', "", "IS NOT NULL");
		restriction2.addClause('mo.to_team_id', this.teamId);
		restriction2.addClause('mo.project_id', this.projectId);
		
		//paired dataSourceId with Restriction objects
		var passedRestrictions = {'employees_moving_from_team_ds': restriction1, 'employees_moving_to_team_ds': restriction2};
		
		var parameters = {};  
		parameters['editTeamId'] = this.teamId;
		parameters['projectId'] = this.projectId;
		parameters['asOfDate'] = this.asOfDate;
		
		//passing restrictions
		View.openPaginatedReportDialog("ab-mo-console-team-edit-em-print.axvw", passedRestrictions, parameters);	
    }

});
