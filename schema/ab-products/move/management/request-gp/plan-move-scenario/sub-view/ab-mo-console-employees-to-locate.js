var abMoScenariosEmToLocate = View.createController('abMoScenariosEmToLocate', {   
	
	pendingAssignments: null,

	isRefreshIconCreated: false,
	
    afterCreate: function() {
        this.on('app:mo:scenarios:console:refresh', this.afterInitialDataFetch);
        this.on('app:mo:scenarios:console:afterUpdateEmployeeAssignment', this.afterUpdateEmployeeAssignment);
    },
    
    afterInitialDataFetch: function(){
    	//update records for employee in mo_scenario_em after layout scenario changed(rooms are re-numbered)
    	this.validLayoutLocation();
    	
    	this.loadEmToLocateGrid();
    	
    	this.addParamsForPendingAssignments();
    	
    	developMovesScenarios.addRefreshButton("exportEmployeesToLocateMenu", "Employee", this);
    	
    	this.isRefreshIconCreated = true;

    },
	
	refreshAfterDataTransferIn: function(){
		this.loadEmToLocateGrid();
	},
	
	/**
     * Enable/Disable check boxes for each row according to pending employee assignments.
     * @param pendingAssignments
     */
    afterUpdateEmployeeAssignment: function(pendingAssignments){
		this.emToLocateGrid.unselectAll();

		this.pendingAssignments = pendingAssignments;

		this.emToLocateGrid.gridRows.each( function(row) {
	   		var emId = row.getFieldValue('mo_scenario_em.em_id');
			row.dom.style.display="";
			if ( pendingAssignments && pendingAssignments.length>0 ) {
				for ( var i=0; i < pendingAssignments.models.length; i++ ) {
					var assignment = pendingAssignments.models[i];
					if (emId===assignment.attributes.em_id) {
						row.dom.style.display="none";
					}
				}
			}
	   	});
    },

	/**
     * Initializes the employee assignment mode when the user clicks on an employee row.
     * @param row
     */
	emToLocateGrid_onMultipleSelectionChange: function(row) {
        var employees = [];
        var rows = this.emToLocateGrid.getSelectedGridRows();
        for (var i = 0; i < rows.length; i++) {
            employees.push(this.createEmployeeAssignmentFromGridRow(rows[i]));
        }   
		this.trigger('app:mo:scenarios:console:beginAssignment', {
			type: 'employee',
			employees: employees
		});
    },
    
    
    /**
     * Creates a new pending assignment from an employee record.
     * @param row
     * @return {Object}
     */
    createEmployeeAssignmentFromGridRow: function(row) {
        return {
            type: 'employee',
            em_id: row.getFieldValue('mo_scenario_em.em_id'),
            bl_id: row.getFieldValue('em.bl_id'),
            fl_id: row.getFieldValue('em.fl_id'),
            rm_id: row.getFieldValue('em.rm_id'),
            to_bl_id: '',
            to_fl_id: '',
            to_rm_id: ''
        };
    },
        
    validLayoutLocation : function() {
		try {
			var result = Workflow.callMethod("AbMoveManagement-MoveService-validLayoutLocation", developMovesScenarios.projectId, developMovesScenarios.scenarioId);

			if ( result.code == "executed" && result.jsonExpression == "changed") {
				View.alert(getMessage("employeesBackToList"));
			}
		}
		catch (e) {
	    	Workflow.handleError(e);
		}			
    },
    
    loadEmToLocateGrid:function() {
    	this.emToLocateGrid.addParameter("projectId",developMovesScenarios.projectId);
    	this.emToLocateGrid.addParameter("scenarioId",developMovesScenarios.scenarioId);
    	this.emToLocateGrid.refresh();
    },

    addParamsForPendingAssignments:function(){
    	this.employeePendingAssignments.addParameter("projectId",developMovesScenarios.projectId);
    	this.employeePendingAssignments.addParameter("scenarioId",developMovesScenarios.scenarioId);
    },
    
    /**
     * method invoke when remove pending assignment button click.
     */
    employeePendingAssignments_onRemovePendingAssignment: function(row) {
        this.trigger('app:mo:scenarios:console:removeAssignment', {
            em_id: row.getFieldValue('em.em_id'),
            location: row.getFieldValue('em.to'),
    		bl_id: row.getFieldValue('em.to_bl_id'),
    		fl_id: row.getFieldValue('em.to_fl_id'),
    		rm_id: row.getFieldValue('em.to_rm_id')
        });
		this.emToLocateGrid.gridRows.each( function(emRow) {
			if ( emRow.getFieldValue('mo_scenario_em.em_id')===row.getFieldValue("em.em_id") ) {
				emRow.dom.style.display="";
			}
	   	});
    },

    /**
     * method invoke when cancel employee pending assignment event click.
     */
    employeePendingAssignments_onCancelEmployeePendingAssignments: function() {
        this.trigger('app:mo:scenarios:console:cancelEmployeeAssignment');
    },

    
    /**
     * method invoke when commit employee button click.
     */
    employeePendingAssignments_onCommitEmployeePendingAssignments: function() {
		this.trigger('app:mo:scenarios:console:commitAssignment');
    },
    
    beforeDataTransfer: function() {
    	var restriction = new Ab.view.Restriction();
		restriction.addClause('mo_scenario_em.project_id', developMovesScenarios.projectId);
		restriction.addClause('mo_scenario_em.scenario_id', developMovesScenarios.scenarioId);
		restriction.addClause('mo_scenario_em.em_id', "", "is not null");
		restriction.addClause('mo_scenario_em.team_id', "", "is null");
		this.employeeDataTransferGrid.restriction= restriction;
    }
});