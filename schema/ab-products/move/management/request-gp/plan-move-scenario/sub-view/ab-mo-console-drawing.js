﻿﻿/**
 * Controller for the drawing panel.
 *
 * Events:
 * app:mo:scenarios:console:commitAssignment
 * app:mo:scenarios:console:cancelAssignment
 * app:mo:scenarios:console:viewAssignment
 * app:mo:scenarios:console:selectRoom
 * app:mo:scenarios:console:viewSelectedRooms
 * app:mo:scenarios:console:moveEmployee
 * app:mo:scenarios:console:removeEmployeeWaiting
 */
var spaceExpressConsoleDrawing = View.createController('spaceExpressConsoleDrawing', {

    /**
     * The object that describes what rooms are assigned to: a department, a room type, or employees.
     */
    assignmentTarget: null,
 
    /**
     * The objects contains all the selected rooms.
     */
    selectedRooms: null,
    
    /**
     * Rooms for assignment.
     */
    assignedRooms: null,
    
    /**
     * Rooms for employees moving for highlighting.
     */
    movedEmployeesRooms: null,

    /**
     * The array of {bl_id, fl_id, dwgname} for displayed floors.
     */
    selectedFloors: null,

    /**
     * Location and occupancy restrictions applied from the filter panel.
     */
    filter: null,
    
    /**
     * filter for last time.
     */
    lastFilter: null,
    
    /**
     * Selected grid rows
     */
    selectedEmployeeRows: null,
	
	/**
	 * Number of selected grid rows
	 */
	totalSelectedEmployeeRows: 0,
	
	/**
	 * The current selected plan type.
	 */
	selectedPlanType: null,
	
	/**
	 * The current selected legend datasource.
	 */
	currentLegendDataSource: null,
	
	/**
	 * The current legend panel.
	 */
	currentLegendPanel: null,

	/**
	 * Suffix of drawing files according to user selected background and its option value as well.
	 */
	backgroundSuffix:null,
	backgroundOptionValue:null,

	originalHighlightTeamDs:null,
	originalLabelTeamDs:null,

    /**
     * Added for 23.2 html5 conversion
     */
	drawingControl:null,
 	assetPanelLoaded: false,

    /**
     * A sign to indicate if highlight border of rooms contain pending request.
     */
 	highlightPendingBorder: false,

     /**
     * Currnet markup's activity log id if exists.
     */
	markupActivityLogId: null,
     /**
     * Currnet option for shhow/hide markups.
     */
	markupOption: 0,

	pendingAssignments:[],
	/**
     * Constructor.
     */
    afterCreate: function() {
        this.initializeVariables();
        this.setFilter();
        this.initializeEventListener();
    },
    
    /**
     * After view is loaded, we will set the UI state and register event for the drawing panel.
     */
    afterViewLoad: function() {
		this.initialSvgDrawingCtrl();
		this.switchDataSourceSelectors();
		
        //change the number value to text label for the legend panel.
        this.legendGrid.afterCreateCellContent = setLegendLabel;
        this.borderLegendGrid.afterCreateCellContent = setLegendLabel;
    },

	afterInitialDataFetch: function() {
		this.initialWaitingRooms();
    },
    
    /**
     * Initialize the variables after the view is created.
     * Add by heqiang
     */
    initializeVariables: function() {
    	this.assignmentTarget = null;
        this.selectedFloors = [];
		// Reset all rooms and clear their current status.
    	this.selectedRooms = new Ab.space.express.console.Rooms();
    	this.assignedRooms = new Ab.space.express.console.Rooms();
    	this.movedEmployeesRooms = new Ab.space.express.console.Rooms();
    },
    
    /**
     * Register all the listener this controller handles.
     * Add by heqiang
     */
    initializeEventListener: function() {
        this.on('app:mo:scenarios:console:refresh', this.clearDrawing);
        this.on('app:mo:scenarios:console:refreshDrawing', this.refreshDrawing);
        this.on('app:mo:scenarios:console:selectLocation', this.addFloorPlan);
        this.on('app:mo:scenarios:console:removeAssignment', this.removeAssignment);
        this.on('app:mo:scenarios:console:afterBeginAssignment', this.afterBeginAssignment);
        this.on('app:mo:scenarios:console:afterClearAssignment', this.afterClearAssignment);
        this.on('app:mo:scenarios:console:afterChangeAssignmentTarget', this.afterChangeAssignmentTarget);
        this.on('app:mo:scenarios:console:cancelSelectedRooms', this.cancelSelectedRooms);
        this.on('app:mo:scenarios:console:cancelEmployeeAssignment', this.cancelEmployeeAssignment);
        this.on('app:mo:scenarios:console:commitRoomsAlteration', this.afterCommitRoomsAlteration);
        this.on('app:mo:scenarios:console:rollbackEmployeeAssignment', this.rollbackEmployeeAssignment);
        this.on('app:mo:scenarios:console:afterCommitPendingAssignment', this.afterCommitPendingAssignment);
        this.on('app:mo:scenarios:console:resetRoomForAssignment', this.resetRoomForAssignment);
        this.on('app:mo:scenarios:console:afterUpdateEmployeeAssignment', this.afterUpdateEmployeeAssignment);
	},
    
	initialSvgDrawingCtrl: function() {	
		// define parameters to be used by server-side job
    	var parameters = new Ab.view.ConfigObject();
		parameters['divId'] = "drawingDiv";
    	parameters['allowMultipleDrawings'] = 'true';
    	parameters['orderByColumn'] = 'true';

		parameters['highlightParameters'] = [{'view_file':"ab-mo-console-ds-drawing.axvw", 'hs_ds': "highlightNoneDs", 'label_ds':'labelEmployeesDs','label_ht': 0.6}];

    	parameters['showTooltip'] = true;
		parameters['addOnsConfig'] = { 
				'NavigationToolbar': {divId: "drawingDiv"},
				'MarkupControl': { divId: "drawingDiv",
								  panelId: "markupLegendPanel",
								  legendId: "markupLegendDiv",
								  legendTitle: getMessage("redlineSymbols"),
								  colorPickerId: 'markupLegendPanel_head',
								  onChange: this.onMarkupChange,
								  onDrop: this.onMarkupDrop,
								  onSave: this.onSave,
								  showAreaLabel: true
							   },
				'LayersPopup': {divId: "drawingDiv", customEvent: this.onLayerChanged},
				'DatasourceSelector': {panelId: "drawingPanel", afterSelectedDataSourceChanged: this.afterSelectedDataSourceChanged.bind(this)},
				'InfoWindow': {width: '400px', position: 'bottom', customEvent: this.onCloseInfoWindow},
				'AssetLocator': {divId: "drawingDiv"},
				//'SelectWindow': {assetType: "rm", customEvent: this.onSelectRoom},
				'AssetPanel': {
					divId: "drawingDiv",
					assetType: 'em',
					layer: 'rm_trial',		// (optional) if not specified, defaults to 'rm'
					title: getMessage("waitingRoom"), 
					dataSourceName: "employeesWaitingDS",
					collapsible: true, 
					size: {height: 130, width: 380}, 
					font: {fontFamily:'Arial', fontSize: 11},
					//highlightedOnly: true,		// if true, when attaching drop events, restrict to only locations that are highlighted from the hs_ds
					rowActions: [],
					onAssetLocationChange: this.onDropAsset.createDelegate(this)
				}
		};

		parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickRoom}, {'eventName': 'click', 'assetType' : 'rm_trial', 'handler' : this.onClickRoom}];
		
		//enable bordersHighlight datasource selector
		parameters['bordersHighlightSelector'] = true;
		//pass borderSize for border highlight
		parameters['borderSize'] = 18;
		
		//enable legend panels
		parameters['legendPanel'] = 'legendGrid';
		parameters['borderLegendPanel'] = 'borderLegendGrid';

		this.drawingControl = new Drawing.DrawingControl("drawingDiv", "drawingPanel", parameters);

    	var selectController = this.drawingControl.drawingController.getController("SelectController");
    	//enable diagonal border selection 
    	selectController.setDiagonalSelectionPattern(true);
    	//set diagonal selection border size (default is 20)
    	selectController.setDiagonalSelectionBorderWidth(15);
    	
    	//enable Multiple Selection
    	selectController.setMultipleSelection(true);

		this.initialDrawingActions();
	},

	/**
	* Initial employeesWaiting panel according to mode from side car
	*/
	initialWaitingRooms: function(enabled){
		this.loadEmployeesWaitingRoom();
		this.refreshEmployeeWaitingPanel();
	},

	/**
	* Added for 23.2 html drawing conversion: Show asset panel Employees Waiting Room.
	*/
	loadEmployeesWaitingRoom: function(){
		if (!this.assetPanelLoaded) {
			/*below code segment is a work around to show asset panel initially when no floor plan is added, can be removed after the core kb is fixed*/
			/* start work around*/
			var parameters = new Ab.view.ConfigObject();
			parameters['pkeyValues'] = {'bl_id':"HQ", 'fl_id': "17"};
			parameters['drawingName'] = 'HQ17';
			this.drawingControl.load(parameters);	
			this.drawingControl.unload(parameters);	
			/* end work around*/
			// load and display the asset panel
        	var restriction = new Ab.view.Restriction();
        	restriction.addClause('em.em_id', 'none', '='); 
			this.drawingControl.getAddOn("AssetPanel").loadAssetPanel(restriction);
			this.drawingControl.getAddOn("AssetPanel").showAssetPanel();
			this.assetPanelLoaded = true;
		} 
	},

	/**
	* Added for 23.2 html drawing conversion: load floor plan svg.
	*/
	loadSvg: function(row){
		if ( this.drawingPanel.actions.get('moveActionsMenu').actions.get("saveMarkups").enabled ) {
			View.alert(getMessage('markupNoSave'));
			// TODO: let user choose whether save or not???
		}

		if ( row.row.isSelected() )	{
			// determine to show scenario layout or invertory floor
			var layoutType = row.row.getFieldValue("mo_scenario_em.vf_usage_type");
			if ( this.drawingControl.layoutType!= layoutType) {
				this.drawingControl.layoutType = layoutType;
				this.switchDataSourceSelectors(layoutType);
			}
			this.drawingControl.getAddOn('LayersPopup').collapsed = true

			//initial mark up control
			this.resetMarkups();

			var isInventory = 	 ("Inventory"===layoutType);
			this.drawingPanel.actions.get('exportDrawingPanelMenu').actions.get("printOptionAction").enable(isInventory);
			this.drawingPanel.actions.get('exportDrawingPanelMenu').actions.get("exportLoadedDrawingToDOCX").enable(!isInventory);
		}
		else {
			// before unload current drawing and reload newly selected drawing, do some clear works.
			this.drawingPanel_onCancelSelectedRooms();

			var parameters = new Ab.view.ConfigObject();
			parameters['pkeyValues'] = {'bl_id':row.row.getFieldValue('mo_scenario_em.to_bl_id'), 'fl_id': row.row.getFieldValue('mo_scenario_em.to_fl_id'), 'filename': row.row.getFieldValue('mo_scenario_em.filename')};
			parameters['drawingName'] = row.row.getFieldValue('mo_scenario_em.vf_dwg_name');

			this.drawingControl.unload(parameters);
			this.markupLegendPanel.closeWindow();

			//reset status of action "Show Pendings"
			this.highlightPendingBorder = true;
			this.showCurrentPending();

			this.drawingPanel.actions.get('exportDrawingPanelMenu').actions.get("printOptionAction").enable(false);
			this.drawingPanel.actions.get('exportDrawingPanelMenu').actions.get("exportLoadedDrawingToDOCX").enable(false);
		}
	},
		
	/**
	* Add sql parameters to DataSources and then load drawing.
	*/
 	loadDrawing: function(parameters){
		this.drawingControl.config.highlightParameters[0].hs_param = toJSON({'projectId':developMovesScenarios.projectId, 
			'scenarioId':developMovesScenarios.scenarioId, 
			'asOfDate':developMovesScenarios.isoDateStart});

		// set layer for drag-drop
		var assetPanelConfig = this.drawingControl.config['addOnsConfig']['AssetPanel'];
		assetPanelConfig['layer'] = this.drawingControl.layoutType=='Inventory' ? 'rm' : 'rm_trial';	
		var assetPanelAddOn = this.drawingControl.drawingController.getAddOn('AssetPanel');
		if (assetPanelAddOn) {
			assetPanelAddOn.layer = this.drawingControl.layoutType=='Inventory' ? 'rm' : 'rm_trial'; 
		}

		this.drawingControl.load(parameters);
		
		// re-highlight assets (selected rooms, pending assignments, etc.
		this.reHighlightSelectedAssets();

		this.addDragDropEvent();
	},
		
 	clearDrawing: function(){
		if (this.selectedFloors && this.selectedFloors.length===1) {
    		var selectedFloor = this.selectedFloors[0];
			var parameters = new Ab.view.ConfigObject();
			parameters['pkeyValues'] = {'bl_id':selectedFloor.bl_id, 'fl_id': selectedFloor.fl_id};
			parameters['drawingName'] = selectedFloor.dwgname;

			this.drawingControl.unload(parameters);
		}
	},

	/**
	* According to layout type, switch highlight/label/boorder DataSources.
	*/
 	switchDataSourceSelectors: function(layoutType){
		if ( !layoutType )	{
			this.drawingControl.layoutType = 'Inventory';
		}

		var isTeamAssign = this.assignmentTarget && (this.assignmentTarget.type == 'team'); 

		var dsSelector = this.drawingControl.drawingController.getAddOn('DatasourceSelector');

		var hlDsNameIdsMap = dsSelector.getSelectorDatasources("DrawingControlHighlight");	
		this.switchSelectorOptions("selector_hilite", hlDsNameIdsMap, layoutType, isTeamAssign);
				
		var borderDsNameIdsMap = dsSelector.getSelectorDatasources("DrawingControlHighlight", 'IBorder');
		this.switchSelectorOptions("selector_IBorders", borderDsNameIdsMap, layoutType, isTeamAssign);
																	
		var labelDsNameIdsMap = dsSelector.getSelectorDatasources('DrawingControlLabels');		
		this.switchSelectorOptions("selector_labels", labelDsNameIdsMap, layoutType, isTeamAssign);

		this.switchHighlightParameters(layoutType, isTeamAssign);
	},

	/**
	* According to layout type, manually set options in drop-down list for highlight/label/boorder DataSources.
	*/
 	switchSelectorOptions: function(selectorId, dsMap, layoutType, isTeamAssign){
		var noneName;
		var noneDs = null;
		for (var name in dsMap) {
			var dsId =  dsMap[name];
			 if ( dsId.indexOf("None")>0 ) {
				 continue;
				 /*noneDs = dsId; 
				 noneName = name;
				jQuery("#"+selectorId+" option[value='"+dsId+"']").remove();
				*/
			 }
			else if ( isTeamAssign && dsId.indexOf("Assign")<=0
				|| !isTeamAssign && dsId.indexOf("Assign")>0
				|| 'Scenario'==layoutType && dsId.indexOf("Trial")<=0   &&  dsId.indexOf("None")<0
				|| 	'Inventory'==layoutType &&  dsId.indexOf("Trial")>0
				|| !layoutType && dsId.indexOf("Trial")>0  ){

				jQuery("#"+selectorId+" option[value='"+dsId+"']").remove();
			}
			else  if ( jQuery("#"+selectorId+" option[value='"+dsId+"']").length==0 ) {
				jQuery("#"+selectorId).append("<option value='"+dsId+"'>"+name+"</option>");
			}
		}
		//finally append the none option to the bottom of list
		if ( noneDs && noneName) {
			//jQuery("#"+selectorId).append("<option value='"+noneDs+"'>"+noneName+"</option>");
		}
	},

	/**
	* According to layout type and whether is team assign mode, change the html control's parameters.
	*/
	switchHighlightParameters: function(layoutType, isTeamAssign){
		if ( isTeamAssign ) {
			if ("Inventory"==layoutType ) {
				this.drawingControl.config.highlightParameters[0]["hs_ds"] = "highlightTeamAssignDs";
				this.drawingControl.config.highlightParameters[0]["label_ds"] = "labelTeamAssignDs";
				jQuery("#selector_hilite")[0].value="highlightTeamAssignDs";
				jQuery("#selector_labels")[0].value="labelTeamAssignDs";
			}
			else if ( "Scenario"==layoutType) {
				this.drawingControl.config.highlightParameters[0]["hs_ds"] = "highlightTrialTeamAssignDs";
				this.drawingControl.config.highlightParameters[0]["label_ds"] = "labelTrialTeamAssignDs";
				jQuery("#selector_hilite")[0].value="highlightTrialTeamAssignDs";
				jQuery("#selector_labels")[0].value="labelTrialTeamAssignDs";
			}
		}
		else if ( layoutType ) {
			this.setAccordingDs("hs_ds", "highlight");
			this.setAccordingDs("label_ds", "label");
		}		
	},

    setAccordingDs:function(dsParameterId, type){
		var oldDs = this.drawingControl.config.highlightParameters[0][dsParameterId];
		if ( oldDs.indexOf("None")<0 ) {
			var newDs = "";
			if (oldDs.indexOf("Trial")>=0) {
				newDs = oldDs.replace("Trial", "");					
			} else {
				newDs = oldDs.replace(type, type+"Trial");					
			}
			this.drawingControl.config.highlightParameters[0][dsParameterId] = newDs;
			if ("highlight"===type) {
				jQuery("#selector_hilite")[0].value=newDs;
			} else {
				jQuery("#selector_labels")[0].value=newDs;
				// for label DataSource, also add tooltip	manually.
				var dsSelector = this.drawingControl.drawingController.getAddOn('DatasourceSelector');
				dsSelector.addTooltips(newDs, dsSelector);
			}
		}
	},

    afterSelectedDataSourceChanged: function(comboId, comboValue, drawingController){   
		if ( this.selectedFloors.length===1 ) {
			this.addDragDropEvent();
		}
	},

    addDragDropEvent: function() {
		// attach drag and drop events to employee labels from 'labelEmployeesDs'
		var drawingController = this.drawingControl.drawingController;
		var datasourceSelectorAddOn = drawingController.getAddOn('DatasourceSelector');
		var labelDs = drawingController.config.highlightParameters[0].label_ds;

		if ( this.selectedFloors && this.selectedFloors[0] ) {
			var layerName = (this.selectedFloors[0].layoutType==="Inventory") ?  "rm" : "rm_trial";
			var drawingName = this.selectedFloors[0].dwgname;
			if ( drawingController.config.labelSelector === false 
				 || (drawingController.config.labelSelector === true 
					 && datasourceSelectorAddOn 
					 &&  ( labelDs === 'labelEmployeesDs' || labelDs === 'labelTrialEmployeesDs' )  
				 ) ) {
				this.drawingControl.getAddOn("AssetPanel").addDragDropEventsToLayer(layerName, drawingName);
				this.drawingControl.getAddOn("AssetPanel").addDragDropEventsToRooms(layerName, drawingName);    		
			}
		}
	},

	/**
     * Applies specified location and occupancy restrictions to the drawing panel.
     */
    refreshDrawing: function(filter) {
    	this.setFilter(filter);
		
        this.trigger('app:mo:scenarios:console:cancelSelectedRooms');
		this.clearDrawingSelections();

		for ( var i = 0; i < View.dataSources.length; i++ ) {
            var ds = View.dataSources.get(i);
            if (ds && ds.type === 'DrawingControlHighlight') {
                ds.addParameters(this.filter.parameters);
                ds.setRestriction(this.filter.restriction);
            }
            if (ds && ( ds.id === 'labelTeamDivisionsDs' || ds.id === 'labelTeamDepartmentsDs' || ds.id === 'labelTeamsDs' ) ) {
                ds.addParameters(this.filter.parameters);
            }
        }

        if (this.drawingControl.drawingController.isDrawingsLoaded) {
			this.reloadDrawings();
			this.clearDrawingSelections();
        }
    },
    		
    /**
     * Set the filter value to that comes from the location filter console.
     */
    setFilter: function(filter) {
        if (filter) {
            this.filter = filter;
			this.asOfDate = filter.parameters['asOfDate'];
        } else {
        	if (this.filter == null) {
        		this.filter = {
                        restriction: null,
                        parameters: {'asOfDate': this.asOfDate}
                    };
        	}
        }
    },
       
    /**
     * Called when the user selects or un-selects a floor in the Locations panel.
     * @param row
     */
    addFloorPlan: function(row) {
        this.onCheckLayout(row);

		if (this.backgroundSuffix){
			var opts = new DwgOpts();
			opts.backgroundSuffix =  this.backgroundSuffix;
			this.drawingPanel.addDrawing(row, opts);
		} else {
			 this.loadSvg(row) ;
		};

		if (row.row.isSelected() ) {
			if (this.selectedPlanType) {
				//this.openPlanTypesDialogCloseHandler(this.selectedPlanType)
			}
		}
    },
    
    /**
     * When the user select or unselect a row from the building-floor grid, we will push or pop the floor correspondingly.
     */
     onCheckLayout: function(row) {
        this.selectedFloors = _.reject(this.selectedFloors, function(selectedFloor) {
            return selectedFloor.bl_id === row.row.getFieldValue('mo_scenario_em.to_bl_id') 
				&& selectedFloor.fl_id === row.row.getFieldValue('mo_scenario_em.to_fl_id');
        });

        if (row.row.isSelected()) {
            this.selectedFloors.push({
                bl_id: row.row.getFieldValue('mo_scenario_em.to_bl_id') ,
                fl_id: row.row.getFieldValue('mo_scenario_em.to_fl_id') ,
                dwgname:  row.row.getFieldValue('mo_scenario_em.vf_dwg_name'),
                filename:  row.row.getFieldValue('mo_scenario_em.filename'),
                layerName: row.row.getFieldValue('mo_scenario_em.vf_usage_subtype'),
                layoutType: row.row.getFieldValue('mo_scenario_em.vf_usage_type')
            });
        }
    },
        
    /**
     * ReHighlight all the selected assets according to types.
     * add by heqiang
     */
    reHighlightSelectedAssets: function() {
		if (this.assignedRooms.length > 0) 
			this.reHighlightPendingAssignments();

		if (this.selectedRooms.length > 0) {
			//this.reHighlightSelectedRooms();
		}
		
		if (this.movedEmployeesRooms.length > 0) {
			this.reHighlightEmployeesMovedRooms();
		}
    },
        
    /**
     * ReHighlight the assignments in the session when user reopens the floor plan if has any.
     * add by heqiang
     */
    reHighlightPendingAssignments: function() {
    	for (var i = 0; i < this.selectedFloors.length; i++) {
    		var selectedFloor = this.selectedFloors[i];
    		var targetBlId = selectedFloor.bl_id;
    		var targetFlId = selectedFloor.fl_id;
    		var roomsArray = this.assignedRooms.targetToArray(targetBlId, targetFlId);
    		if (roomsArray.length > 0) {
        		if (this.drawingControl.drawingController.isDrawingsLoaded) {
    				for (var j = 0; j < this.assignedRooms.models.length; j++) {
    					var room = this.assignedRooms.models[j];
						var buildingId = room.get('bl_id');
						var floorId = room.get('fl_id');
						var teamColor = room.get('teamColor');
     					var dwgname = this.getDwgnameByBlIdAndFlId(buildingId, floorId);
    					this.highlightDrawingAsset( {
									bl_id:buildingId,
									fl_id: floorId,
									rm_id:room.get('rm_id'), 
									dwgname: dwgname,
									color: teamColor
								});
    				}			
				}
        	}
    	}
    },

    /**
     * ReHighlight assinged rooms in the session when user reopens the floor plan.
     * add by heqiang
     */
    reHighlightEmployeesMovedRooms: function() {
    	for ( var i = 0; i < this.selectedFloors.length; i++ ) {
    		var selectedFloor = this.selectedFloors[i];
    		var targetBlId = selectedFloor.bl_id;
    		var targetFlId = selectedFloor.fl_id;
    		var roomsArray = this.movedEmployeesRooms.targetToArray(targetBlId, targetFlId);
    		if (roomsArray.length > 0) {
    			if (this.drawingControl.drawingController.isDrawingsLoaded) {
    				for (var j = 0; j < roomsArray.length; j++) {
    					var room = roomsArray[j];
    					var dwgname = this.getDwgnameByBlIdAndFlId(room[0], room[1]);
    					this.highlightDrawingAsset( {
									bl_id:room[0],
									fl_id:room[1],
									rm_id:room[2], 
									dwgname: dwgname
								});
    				}
    			}
    		}
    	}
    },
    
    /**
     * After the user commits the selected rooms,we will reset the collection.
     */
    afterCommitRoomsAlteration: function() {
    	this.selectedRooms = new Ab.space.express.console.Rooms();
		this.reloadDrawings();
		this.clearDrawingSelections();
    },
    
    
    /**
     * When cancel employee assignment from the popup window, we should clear the rehighlighted rooms.
     */
    cancelEmployeeAssignment: function() {
    	this.clearAssignmentTarget();

		if (this.drawingControl) {
				this.reloadDrawings();
				this.clearDrawingSelections();
    	}

        this.trigger('app:mo:scenarios:console:cancelAssignment');
    },
    
    /**
     * Get dwgname with building id and floor id.
     */
    getDwgnameByBlIdAndFlId: function(buildingId, floorId) {
    	for ( var i = 0; i < this.selectedFloors.length; i++ ) {
    		var floor = this.selectedFloors[i];
    		if ( floor.bl_id.toUpperCase() == buildingId.toUpperCase() && floor.fl_id.toUpperCase() == floorId.toUpperCase() ) {
    			return floor.dwgname;
    		}
    	}
    	return null;
    },
    
    /**
     * Get file name with building id and floor id.
     */
    getFilenameByBlIdAndFlId: function(buildingId, floorId) {
    	for ( var i = 0; i < this.selectedFloors.length; i++ ) {
    		var floor = this.selectedFloors[i];
    		if ( floor.bl_id.toUpperCase() == buildingId.toUpperCase() && floor.fl_id.toUpperCase() == floorId.toUpperCase() ) {
    			return floor.filename;
    		}
    	}
    	return null;
    },

	/**
     * Returns true if specified floor is selected.
     * @param buildingId
     * @param floorId
     * @return {Boolean}
     */
    isFloorSelected: function(buildingId, floorId) {
        return valueExists(_.find(this.selectedFloors, function(selectedFloor) {
            return (selectedFloor.bl_id === buildingId && selectedFloor.fl_id === floorId);
        }));
    },
    	       
	reloadDrawings: function(){
 		var drawingController = this.drawingControl.getDrawingController(); 
		//var dsSelector = drawingController.getAddOn('DatasourceSelector');
		var parameters =  this.drawingControl.config;
		if ( !parameters.drawingName ) {
			parameters.drawingName = this.selectedFloors[0].dwgname;
		}
		drawingController.getControl().reload(parameters);	

		this.addDragDropEvent();
	},

	//Added for html5 drawing conversion: manually highlight given assets
	highlightDrawingAsset: function(row){
		var assetId =  row.bl_id+ ';' +row.fl_id+ ';' +row.rm_id;
		assetId = assetId + (  "Inventory"==this.drawingControl.layoutType ? "" : (";"+this.selectedFloors[0].layerName) );
		var svgId = 	 'drawingDiv-' + row.dwgname+'-svg';
		var highlightController = this.drawingControl.getDrawingController().getController("HighlightController");
		var color = (row.color ? row.color : 'yellow');
    	highlightController.highlightAsset(assetId, {svgId: svgId, color:color, persistFill: true, overwriteFill: true});

		var selectController = this.drawingControl.drawingController.getController("SelectController");
		var assetController = this.drawingControl.drawingController.getController("AssetController");
		selectController.diagonalSelection(assetController, svgId, assetId);
	},

	resetDrawingAsset: function(row){
		var highlightController = this.drawingControl.getDrawingController().getController("HighlightController"); 
    	highlightController.highlightAsset(row.bl_id+ ';' +row.fl_id+ ';' +row.rm_id, {svgId:  'drawingDiv-' + row.dwgname+'-svg', color: 'yellow', persistFill: true, overwriteFill: true});
		//var opts_selectable = new DwgOpts();
		//opts_selectable.rawDwgName = rows[i].dwgname;
		//opts_selectable.appendRec(rows[i].bl_id+ ';' +rows[i].fl_id+ ';' +rows[i].rm_id);
		//this.drawingPanel.setSelectColor('0xFFFF00'); //set to yellow for exist assignment
    	//highlightController.highlightAsset('SRL;03;365B', {svgId: 'svgDiv-srl03-svg', color: 'blue', persistFill: true, overwriteFill: true});
	},
	
    /**
     * Added for html5 drawing control conversion: clear selection on drawing.
     */
    unSelectDrawingAsset: function(room) {   	
		var selectController = this.drawingControl.drawingController.getController("SelectController");
		var assetController = this.drawingControl.drawingController.getController("AssetController");
		var highlightController = this.drawingControl.getDrawingController().getController("HighlightController");
		var found = false;
		var selectAssets = selectController.selectedAssets;
    	for ( var svgId in selectAssets ) {
    		if ( selectAssets[svgId].length >= 0 ){ 
				for ( var i=0; i< selectAssets[svgId].length; i++ )	{
					var assetId = selectAssets[svgId][i];
					var roomId = room[0][0]+";"+room[0][1]+";"+room[0][2];
					if ( (roomId==assetId) || ( assetId.indexOf( roomId+";")>=0)  ) {
						selectController.clearDiagonalSelection(assetController, svgId, assetId);
						highlightController.resetAsset(svgId,assetId);
						found = true;
					}
				}
			}
		}

		if ( !found ) {
			var svgId = ('drawingDiv-' + this.selectedFloors[0].dwgname + '-svg' );
			var assetId = (room[0][0]+";"+room[0][1]+";"+room[0][2]);
			assetId +=  (this.selectedFloors[0].layoutType==="Inventory") ?  "" : (";"+this.selectedFloors[0].layerName);
			selectController.clearDiagonalSelection(assetController, svgId, assetId);
			highlightController.resetAsset(svgId,assetId);
		}
	},

	/**
     * Added for html5 drawing control conversion: clear selection on drawing.
     */
    clearDrawingSelections: function() {   	
		var selectController = this.drawingControl.drawingController.getController("SelectController");
		var assetController = this.drawingControl.drawingController.getController("AssetController");
		var highlightController = this.drawingControl.getDrawingController().getController("HighlightController");
		highlightController.resetAll();
		var selectAssets = selectController.selectedAssets;
    	for ( var svgId in selectAssets ) {
    		if ( selectAssets[svgId].length >= 0 ){
				var length = selectAssets[svgId].length-1;
				for ( var i=length; i>=0; i-- )	{
					var assetId = selectAssets[svgId][i]; 
					selectController.toggleAssetSelection(svgId, assetId);
				}
			}
		}
	},

    onLayerChanged: function(checkBox) {   	
	},

    /**
     * Added for 23.2: click room handler of drawing control.
     */
    onClickRoom: function(params, drawingController){
		var thisController =   spaceExpressConsoleDrawing;

		var selected = thisController.isSelected(params, drawingController);
		var svgId = params['svgId'];
		var pk = 	params['assetId'].split(";");
        var room = { bl_id: pk[0], fl_id: pk[1], rm_id: pk[2], 
			svgId: params['svgId'], assetId: params['assetId'], 
			filename:thisController.getFilenameByBlIdAndFlId(pk[0], pk[1])  };

		var floor;
		if ( thisController.selectedFloors && thisController.selectedFloors[0] ) {
			floor = thisController.selectedFloors[0];
		}

		if ( thisController.assignmentTarget && thisController.assignmentTarget.type=="team") {
			thisController.onClickRoomInTeamAssignMode(room, selected, floor);
		} 
		else {
			thisController.onClickRoomInEmployeeMode(room, selected, floor);
		}
    },

    isSelected: function(params, drawingController) {														
    	var selectController = drawingController.getController("SelectController");
    	selectController.toggleAssetSelection(params['svgId'], params['assetId']);
    	var selectAssets = selectController.selectedAssets;
		var svgId = params['svgId'];
		var selected = false;
		if ( selectAssets && selectAssets[svgId] && selectAssets[svgId].length>0 ) {
			for ( var i=0; i< selectAssets[svgId].length; i++ )	{
				 if (  selectAssets[svgId][i] == params['assetId'] )
				   selected  = true;
			}
		}
		return selected;
	},

    setAssignTargetColor: function(room, selected) {
		if (selected) {
			var highlightController = this.drawingControl.getDrawingController().getController("HighlightController"); 
			highlightController.highlightAsset(room.assetId, {svgId:  room.svgId, color: this.assignmentTarget.color , persistFill: false, overwriteFill: false});
		}
		else {
			this.unSelectDrawingAsset(this.getAssetForRoom(room));
		}
	},

	/**
     * Process the click event in team-assign mode.
     * add by ZY.
     */
    onClickRoomInTeamAssignMode: function(room, selected, floor) {
		// check whether the room is occupiable for team.
		var record =( "Inventory"===floor.layoutType ? 
			abSpConsole_getRoomAssignableForTeam(room.bl_id, room.fl_id, room.rm_id, developMovesScenarios.projectId, developMovesScenarios.scenarioId, developMovesScenarios.isoDateStart)
		: abSpConsole_getTrialRoomAssignableForTeam(room.bl_id, room.fl_id, room.rm_id, developMovesScenarios.projectId, developMovesScenarios.scenarioId, developMovesScenarios.isoDateStart, this.selectedFloors[0].layerName));

		if ( record ) {
			var isRoomOccupiableForTeam = parseInt( 
				 "Inventory"===floor.layoutType ? record.getValue('rm.occupiable') : record.getValue('rm_trial.occupiable'));
			if ( isRoomOccupiableForTeam===1 ) {
				//this.drawingPanel.unselectAssets(this.getAssetForRoom(room));
				this.updateLocalRoomForReHighlight(this.assignedRooms, room, selected);
				room['selectDateStart'] = developMovesScenarios.isoDateStart;
				room['selectDateEnd'] = null;

				var roomCpapcity = parseInt( "Inventory"===floor.layoutType ? record.getValue('rm.capacity') : record.getValue('rm_trial.capacity')  );
				room['roomCpapcity'] = roomCpapcity;

				this.setAssignTargetColor(room, selected);

				this.trigger('app:mo:scenarios:console:selectRoom', room, selected, floor);
			} else {
				//this.drawingPanel.unselectAssets(this.getAssetForRoom(room));
				this.unSelectDrawingAsset(this.getAssetForRoom(room));
			}
		}
    },

	/**
     * Process the click event in employee mode.
     * add by heqiang
     */
    onClickRoomInEmployeeMode: function(room, selected, floor) {
    	if (this.assignmentTarget != null) {
    				
			//when selecting room
			if ( selected ) {
				//If users don't select any employee, we give a warning message.
				if (this.isProhibitAssignEmployee()) {
					View.alert(getMessage('noEmSelected'));
					this.unSelectDrawingAsset(this.getAssetForRoom(room));
					return;
				} 
			}
			else {
				// when check other employees and click same room again. 
				if ( !this.isProhibitAssignEmployee() ){
					this.processAssignEmployees(room, true, floor);
				}				
			}

			//check if the room is non occupiable.
			if (this.checkRoomIfNonOccupiable(room, selected, floor)) {
				var innerThis = this;
				View.confirm(getMessage('assignedRoomIsNonOccupiable'), function(button) {
					if (button == 'yes') {
						innerThis.processAssignEmployees(room, selected, floor);
						innerThis.updateLocalRoomForReHighlight(innerThis.movedEmployeesRooms, room, selected);
					} else {
						innerThis.unSelectDrawingAsset(innerThis.getAssetForRoom(room));	
					}
				});
			} else {
				this.processAssignEmployees(room, selected, floor);
				this.updateLocalRoomForReHighlight(this.movedEmployeesRooms, room, selected);
			}    		
    	} else {
    		this.updateLocalRoomForReHighlight(this.selectedRooms, room, selected);
    		this.trigger('app:mo:scenarios:console:selectRoom', room, selected, floor);
    	}
    },
    
    /**
     * To check if the room is non occupiable
     */
    checkRoomIfNonOccupiable: function(room, selected, floor) {
		if ( !selected ) {
			return false;
		}

		var occupiable = abSpConsole_getFieldValueFromOccupiableDs(room.bl_id, room.fl_id, room.rm_id, 'rmcat.occupiable', floor);
		if(isNaN(occupiable) || occupiable == 0) {
			return true;
		}
    	return false;
    },
    
    /**
     * If prohibit the user from assigning employees.
     */
    isProhibitAssignEmployee: function() {
    	var rows = this.emToLocateGrid.getSelectedRows();
    	return rows.length == 0;
    },
    
    /**
     * Process rooms when assign employees.
     */
    processAssignEmployees: function(room, selected, floor) {
    	var dwgname = this.getDwgnameByBlIdAndFlId(room.bl_id, room.fl_id);
    	
    	//if only one employee is selected, then show the employee's name on the drawing panel.
    	for(var i = 0 ; i < this.assignmentTarget.employees.length; i++) {
    		var em_id = this.assignmentTarget.employees[i].em_id;
    		var employeeLabel = [];
        	var labelArray = {field:'em.em_id', record:{'em.em_id':em_id, 'bl_id':room.bl_id, 'fl_id':room.fl_id, 'rm_id':room.rm_id}};
        	employeeLabel.push(labelArray);
        	//this.drawingPanel.addLabels(employeeLabel);

			this.addEmployeeLabelForPendingAssignment(em_id, room);
    	}
    	
    	//highlight the room.
		this.trigger('app:mo:scenarios:console:selectRoom', room, selected, floor);
    	if (selected) {
			this.highlightDrawingAsset( {
						bl_id:room.bl_id,
						fl_id:room.fl_id,
						rm_id:room.rm_id, 
						dwgname: dwgname
					});
    	}
    },
    
    /**
     * Check whether a room is assigned.
     */
    isRoomAssigned: function(room) {
    	//var currentPendingAssignments = this.drawingPanel.getSidecar().get('pendingAssignments');
    	for( var i = 0; i < currentPendingAssignments.models.length; i++) {
    		var assignment = currentPendingAssignments.models[i];
    		var bl_id = assignment.attributes.to_bl_id;
    		var fl_id = assignment.attributes.to_fl_id;
    		var rm_id = assignment.attributes.to_rm_id;
    		if (room.bl_id == bl_id && room.fl_id == fl_id && room.rm_id == rm_id) {
    			return true;
    		}
    	}
    	return false;
    },
    
    /**
     * Update the local copy of all assigned rooms.
     */
    updateLocalRoomForReHighlight: function(roomContainer, room, selected) {
    	if(selected) {
    		if (!roomContainer.findRoom(room)) {
				//store tream assign color for later re-highlighting 
				if (this.assignmentTarget && this.assignmentTarget.type=="team") {
					room.teamColor = this.assignmentTarget.color;
				}

    			roomContainer.addRoom(room);
    		}
    	} else {
    		roomContainer.removeRoom(room);
    	}
    },

	/**
     * The drawing calls this method when the user drags an employee from one room to another,
     * from a room to the asset panel, or from the asset panel to a room.
     * @param record
     * @param assetType
     * @return {Boolean}
     */
    onDropAsset: function(record, assetType) {
        if ( assetType == 'em' ) {
            var assignment = this.createEmployeeAssignmentFromDrawingRecord(record);
			var fromRoom = { bl_id: assignment.bl_id, fl_id: assignment.fl_id, rm_id: assignment.rm_id };
			var toRoom = { bl_id: assignment.to_bl_id, fl_id: assignment.to_fl_id, rm_id: assignment.to_rm_id };

			var emId = record.from['em.em_id'];
			if (assignment.to_bl_id && !this.isWaitingToLocateEmployee(emId) ) {
				return false;
			}            

            if (this.checkUserVPAPass(assignment)) {
            	if ( !assignment.to_bl_id ) {
					return this.dropToEmployeeWaitingPanel(assignment, fromRoom, toRoom);
				} else {
					this.removeEmployeeLabelForPendingAssignment(assignment.em_id, fromRoom);
					this.unSelectDrawingAsset(this.getAssetForRoom(fromRoom));
					return this.dropToDrawingPanel(assignment, toRoom, record);
            	}
            	// console is a 'read-only' version, for those users who do not have authority to make any edits or assignments
            	//this.disableButtonIfReadOnlyUserGroup();
            } 
			else {
            	alert(getMessage('dropEmPromptVPA'));
            	return false;
             }
        }
        return true;
    },
	  
    isWaitingToLocateEmployee: function(emId) {
		this.employeesWaitingDS.addParameter("projectId", developMovesScenarios.projectId);
		this.employeesWaitingDS.addParameter("scenarioId",  developMovesScenarios.scenarioId);
		var records = this.employeesWaitingDS.getRecords();
		for (var i = 0;  i < records.length; i++) {
			if (records[i].getValue('em.em_id')==emId) {
				return true;
			}
		}

		for ( var i=0; i < this.pendingAssignments.length; i++ ) {
			var assignment = this.pendingAssignments.models[i];
			if ( emId== assignment.attributes.em_id ) {
				return true;
			}
		}

		return false;
	},
	
	dropToEmployeeWaitingPanel: function(assignment, fromRoom, room) {
		var record = this.getPlacedEmployeeAssignment(assignment, fromRoom); 
		if ( record ) {
			this.removeEmployeePlacedAssignment(record.getValue("mo_scenario_em.auto_number"));
			this.removeEmployeeLabelForPendingAssignment(assignment.em_id, fromRoom);

			var room = { bl_id: record.getValue("em.bl_id"), fl_id: record.getValue("em.fl_id"), rm_id: record.getValue("em.rm_id") };
			this.addEmployeeLabelForPendingAssignment(assignment.em_id, room);

			var emToLocateGrid = View.panels.get("emToLocateGrid");
			if (emToLocateGrid) {
				emToLocateGrid.refresh();
			}
			this.refreshEmployeeWaitingPanel();
		}

		return false;
	},	
    
	getPlacedEmployeeAssignment: function(assignment, fromRoom, room) {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('mo_scenario_em.em_id', assignment.em_id, "=");
		restriction.addClause('mo_scenario_em.project_id', developMovesScenarios.projectId, "=");
		restriction.addClause('mo_scenario_em.scenario_id', developMovesScenarios.scenarioId, "=");
		restriction.addClause('mo_scenario_em.to_rm_id', fromRoom.rm_id, "=");
		restriction.addClause('mo_scenario_em.to_fl_id', fromRoom.fl_id, "=");
		restriction.addClause('mo_scenario_em.to_bl_id', fromRoom.bl_id, "=");
		var records = this.employeesPlacedDS.getRecords(restriction);

		if (records && records.length==1) {
			return records[0];
		} else 
			return null;
	},
		
	removeEmployeePlacedAssignment:function(numberId){
		try {
			Workflow.callMethod("AbMoveManagement-MoveService-removeAssignment", parseInt(numberId), "employee");

			var emToLocateGrid = View.panels.get("emToLocateGrid");
			if (emToLocateGrid) {
				emToLocateGrid.refresh();
			}

			this.refreshEmployeeWaitingPanel();
		}
		catch (e) {
	    	Workflow.handleError(e);
		}			
	},
    
    dropToDrawingPanel: function(assignment, room) {
		this.trigger('app:mo:scenarios:console:moveEmployee', assignment);

		if (this.movedEmployeesRooms.findRoom(room)) {
			this.movedEmployeesRooms.removeRoom(room);
		}

		this.updateLocalRoomForReHighlight(this.movedEmployeesRooms, room, true);
		
		var dwgname = this.getDwgnameByBlIdAndFlId(room.bl_id, room.fl_id);
		this.highlightDrawingAsset( {
					bl_id:room.bl_id,
					fl_id:room.fl_id,
					rm_id:room.rm_id, 
					dwgname: dwgname
				});

		this.addEmployeeLabelForPendingAssignment(assignment.em_id, room);

		return false;
		//this.refreshEmployeeWaitingPanel();
	},

    addEmployeeLabelForPendingAssignment: function(emId, room) {
		//var target = d3.select(room.bl_id+";"+room.fl_id+";"+room.rm_id+";"+this.selectedFloors[0].layerName);
    	var target = this.getRoomAsset(room)[0][0];
		var layer = (this.selectedFloors[0].layoutType==="Inventory") ?  "rm" : "rm_trial";
		if ( target ) {
			var assetPanelAddOn = this.drawingControl.getAddOn("AssetPanel");
			var newLabel = assetPanelAddOn.addLabelToLayer(emId, layer, target);
			newLabel.classed({'ap-panel-new-label' : true});
			assetPanelAddOn.addDragDropEventsToLayerItem(newLabel.node(), layer, {});
			assetPanelAddOn.addMouseOverEventToLayerItem(newLabel.node());
			assetPanelAddOn.addMouseOutEventToLayerItem(newLabel.node());
		}
	},

    removeEmployeeLabelForPendingAssignment: function(emId, room) {
		var assetPanel = this.drawingControl.getAddOn("AssetPanel");

		var layer = (this.selectedFloors[0].layoutType==="Inventory") ?  "rm" : "rm_trial";
		var layerLabelPrefix = assetPanel.LABEL_PREFIX + layer + '-';

		//var target = d3.select(room.bl_id+";"+room.fl_id+";"+room.rm_id+";"+this.selectedFloors[0].layerName);
    	var target = this.getRoomAsset(room)[0][0];
		if ( !target )
			return;

		var targetId = d3.select(target).attr("id");		//  g node rm, on the rm-assets/rm_trial-assets layer

		var rmLabelContainer = document.getElementById(layerLabelPrefix + targetId);   // rm-labels/rm_trial-assets layer
		
		var labels = d3.select(rmLabelContainer).selectAll("text")
		.filter(function(d, i){ 
			return  (d3.select(this).text() === emId);
		});
		labels.each(function(d,i){
			var label = d3.select(this).remove();
    	});		
	},

	/**
     * Refresh the employee waiting panel when the user do certain update operation.
     * add by heqiang
     */
    refreshEmployeeWaitingPanel: function(restriction) {
		this.employeesWaitingDS.addParameter("projectId", developMovesScenarios.projectId);
		this.employeesWaitingDS.addParameter("scenarioId",  developMovesScenarios.scenarioId);
		this.drawingControl.getAddOn("AssetPanel").refreshAssetPanel(restriction ? restriction : " 1=1 ");
		this.drawingControl.getAddOn("AssetPanel").syncAssetPanelDimensions();
    },
    
	/**
     * Refresh the employee waiting panel to be consistent with the Employees to Locate list.
     */
    afterUpdateEmployeeAssignment: function(pendingAssignments) {
		this.pendingAssignments = pendingAssignments;
		if ( pendingAssignments && pendingAssignments.length>0 ) {
			var restriction = new Ab.view.Restriction();
			for ( var i=0; i < pendingAssignments.models.length; i++ ) {
				var assignment = pendingAssignments.models[i];
				restriction.addClause('em.em_id', assignment.attributes.em_id, '!=');
			}
			this.refreshEmployeeWaitingPanel(restriction);
		}
    },

	/**
     * check if user VPA permit em move to dest room.
     */
    checkUserVPAPass: function(assignment) {
		//if move to waiting room. return true.
		if ( !assignment.to_rm_id ) {
			return true;   
		}

		var vpaCheckDs; 
		if ( "Scenario"===assignment.layoutType ) {
			vpaCheckDs = View.dataSources.get("userTrialRoomsDS");
		} 
		else {
			vpaCheckDs = View.dataSources.get("userRoomsDS");
		}
		vpaCheckDs.addParameter('bl_id', assignment.to_bl_id);
		vpaCheckDs.addParameter('fl_id', assignment.to_fl_id);
		vpaCheckDs.addParameter('rm_id', assignment.to_rm_id);
		var records = vpaCheckDs.getRecords();

		return (records && records.length > 0) ? true : false;
    },
    
    /**
     * Make console a 'read-only' version for those users who do not have authority to make any edits or assignments
     */
    disableButtonIfReadOnlyUserGroup: function() {
		this.drawingPanel.actionbar.actions.each(function (action) {
			if (action.id == 'commitPendingAssignments') {
				action.enable(View.user.isMemberOfGroup('SPACE-CONSOLE-ALL-ACCESS'));
			}
		});
    },
        
    /**
     * Updates the employee assignments in the waiting for specified new assignment.
     * @param assignment
     */
    updateEmployeesWaiting: function(assignment) {
        if ( !assignment.to_bl_id ) {
            // the user has dropped an employee onto the bus - add the assignment to the bus
        	var deletedRoom = { 
             		 bl_id: assignment.bl_id,
                     fl_id: assignment.fl_id,
                     rm_id: assignment.rm_id};
        	if (this.movedEmployeesRooms.findRoom(deletedRoom)) {
        		this.movedEmployeesRooms.removeRoom(deletedRoom);
        	}
        } else {
            // the user has dragged an employee from the bus, or between rooms -
            // if the assignment is on the bus, remove it from the bus
            //this.removeEmployeeWaiting(assignment);
            var addedRoom = { 
            		bl_id: assignment.to_bl_id,
                    fl_id: assignment.to_fl_id,
                    rm_id: assignment.to_rm_id};
            
            if (!this.movedEmployeesRooms.findRoom(addedRoom)) {
            	this.movedEmployeesRooms.addRoom(addedRoom);
            }
        }
    },

    /**
     * Removes specified employee from the waiting room, and removes the pending assignment.
     * @param assignment
     */
    removeEmployeeWaiting: function(assignment) {
        //this.drawingPanel.getSidecar().set('employeesWaiting', this.employeesWaiting);
    	//this.drawingPanel.getSidecar().save();
    },

    /**
     * Called when the user removes a pending assignment.
     */
    removeAssignment: function(assignment) {
        var rooms = this.getAssignedRoom(assignment);
        
       	this.assignedRooms.removeRoom(assignment);
       	this.movedEmployeesRooms.removeRoom(assignment);

		this.unSelectDrawingAsset(this.getAssetForRoom(assignment));

		this.removeEmployeeLabelForPendingAssignment(assignment.em_id, assignment);
    },
    
    /**
     * Obtain a rooms array from assignment.
     */
    getAssignedRoom: function(assignment) {
    	var rooms = [];
    	var room  = [];
    	room[0] = assignment.bl_id;
    	room[1] = assignment.fl_id;
    	room[2] = assignment.rm_id;
    	rooms.push(room);
    	return rooms;
    },
    
    /**
     * Obtain a rooms array from room.
     */
    getAssetForRoom: function(room) {
    	var rooms = [];
    	var roomArray  = [];
    	roomArray[0] = room.bl_id;
    	roomArray[1] = room.fl_id;
    	roomArray[2] = room.rm_id;
    	rooms.push(roomArray);
    	return rooms;
    },
    
	/**
     * clear assignment mode.
     */
    clearAssignmentTarget: function(isTeamAssignMode) {
        this.assignmentTarget = null;

		if ( isTeamAssignMode ) {
        	this.assignedRooms = new Ab.space.express.console.Rooms();
			this.setSelectionForTeamAssignMode(false);
		 }	
		 else {
        	this.movedEmployeesRooms = new Ab.space.express.console.Rooms();
		 }
    },
    
    /**
     * Called after the assignment has been initialized.
     */
    afterBeginAssignment: function(assignmentTarget, pendingAssignments) {
        this.assignmentTarget = assignmentTarget;
        this.setToAssign(assignmentTarget);
    },

    /**
     * Called after the user has selected a different assignment target (e.g. another department).
     * @param assignmentTarget
     */
    afterChangeAssignmentTarget: function(assignmentTarget) {
        this.assignmentTarget = assignmentTarget;
        this.setToAssign(assignmentTarget);
    },

    /**
     * Sets the assignment target for the drawing control.
     * @param assignmentTarget
     */
    setToAssign: function(assignmentTarget) {
		if(assignmentTarget.type === 'team') {
			this.setDrawingForTeamAssignMode();
		}

		if (assignmentTarget.hpattern) {
			assignmentTarget.color = this.getColorFromHpattern( assignmentTarget.hpattern );
		}
    },

     getColorFromHpattern: function(patternString) {
		var parameters = {
			"patternString": patternString
		};

		try {
			var result = Workflow.call('AbCommonResources-HighlightPatternService-decodePattern', parameters);
			var res = eval("(" + result.jsonExpression + ")");
			return "#"+res.rgbColor;
		} 
		catch (e) {
			Workflow.handleError(e);
			return null;
		}
	 },
    
    /**
     *	Added for 23.1: logics for team assign functionality, by ZY, 2016-01-19. 
	 *		- When the user clicks any Assign button in the Teams window, a form appears above the drawing control that asks the user for a Date Range. 
	 *		- The drawing control will display a checkbox called ��Show Available Floors�� when the user is in Assignment mode for teams. 
     */
    setDrawingForTeamAssignMode: function() {
		this.setSelectionForTeamAssignMode(true);

		this.reloadDrawings();

		// in case assign different team,  re-highlight previous selected rooms
		if (this.assignedRooms.length > 0) 
			this.reHighlightPendingAssignments();
	},

    /**
	*  Added for 23.1: logics for team assign functionality, by ZY, 2016-01-19.
     *		- While in Team Assignment mode, the Highlights drop-down will switch to None, and cannot be changed to a different highlight.  
	 *		- The Labels drop-down will automatically switch to Teams when the user is Assignment Mode, but the label must be modified to account for the date range.  The label will display the team code and date range o all teams that occupy a room during the selected date range.
	 *		- The Teams border will also check for all team assigning to rooms during the selected date range.  If multiple teams occupy a room during the date range, then use a dark grey color for the border.
	 *		- The user can choose different labels or borders while in assignment mode.
     */
	setSelectionForTeamAssignMode: function(isTeamAssign) {
		this.switchDataSourceSelectors(this.drawingControl.layoutType);

		document.getElementById('selector_IBorders').disabled = isTeamAssign;
		document.getElementById('selector_hilite').disabled = isTeamAssign;
		document.getElementById('selector_labels').disabled = isTeamAssign;
	},

	/**
     * Called after the assignment has been canceled or committed.
     */
    afterClearAssignment: function(isTeamAssignMode) {
		this.assignedRooms = new Ab.space.express.console.Rooms();
        if(!isTeamAssignMode) {
        	this.refreshEmployeeWaitingPanel();
        }
    	this.assignmentTarget = null;
        if (this.drawingControl.drawingController.isDrawingsLoaded) {
        	//after clear button, change back the previous drawing click statues.
            this.afterCommitPendingAssignment(isTeamAssignMode);
        }
    },

    /**
     * Return a created assignment form drawing record.
     */
    createEmployeeAssignmentFromDrawingRecord: function(record) {
		var isInventory = ("Inventory"==this.selectedFloors[0].layoutType);

		var assignment = {
            type: 'employee',
            to_bl_id: '',
            to_fl_id: '',
            to_rm_id: '',
			layoutType: this.selectedFloors[0].layoutType,
			layerName: this.selectedFloors[0].layerName
        };

        if (!record.from) {
            record.from = record;
        }

		assignment.em_id = record.from['em.em_id'];
		assignment.bl_id = isInventory ? record.from['rm.bl_id'] : record.from['rm_trial.bl_id'];
		assignment.fl_id = isInventory ? record.from['rm.fl_id'] : record.from['rm_trial.fl_id'];
		assignment.rm_id = isInventory ? record.from['rm.rm_id'] : record.from['rm_trial.rm_id'];

        if (!record.to) {
			record.to = record;
			assignment.to_bl_id = null;
			assignment.to_fl_id =  null;
			assignment.to_rm_id =  null;

		} else{
			assignment.to_bl_id = isInventory ? record.to['rm.bl_id'] : record.to['rm_trial.bl_id'];
			assignment.to_fl_id =  isInventory ? record.to['rm.fl_id'] : record.to['rm_trial.fl_id'];
			assignment.to_rm_id =  isInventory ? record.to['rm.rm_id'] : record.to['rm_trial.rm_id'];
			assignment.to_file_name =  this.getFilenameByBlIdAndFlId( assignment.to_bl_id,  assignment.to_fl_id);
		}

		if ( record.from['em.location'] ) {
			this.setLocationToAssignment(assignment, record.from['em.location']);
		}

        return assignment;
    },
    
    /**
     * Set location bl_id fl_id and rm_id to assignment.
     */
    setLocationToAssignment: function(assignment, location) {
    	if (location&&location.indexOf("-")!=-1) {
    		var locatioins = location.split("-");
    	}
        assignment.bl_id = locatioins[0];
        assignment.fl_id = locatioins[1];
        assignment.rm_id = locatioins[2];
    },
    
    /**
     * When a room is over capacity, the assignment should be canceled.
     */
    rollbackEmployeeAssignment: function(room) {
    	if (this.movedEmployeesRooms.findRoom(room)) {
    		this.movedEmployeesRooms.removeRoom(room);
    	}

		//comment below lines for html5 drawing control conversion
		this.reloadDrawings();
		this.clearDrawingSelections();
        this.reHighlightSelectedAssets();
    },

    /**
     * When an employee is moved second time by drag-drop, then reset room previously moved into and removed that room from list.
     */
    resetRoomForAssignment: function(room) {
    	if (this.movedEmployeesRooms.findRoom(room)) {
    		this.movedEmployeesRooms.removeRoom(room);
    	}

		var svgId = ('drawingDiv-' + this.selectedFloors[0].dwgname + '-svg' );
		var assetId = (room.bl_id+";"+room.fl_id+";"+room.rm_id);
		var assetController = this.drawingControl.drawingController.getController("AssetController");
		var assetNode = assetController.getAssetById(svgId, assetId);
		//d3.select(asset.node()).style("fill", this.highlightedAssets[assetId].defaultColor);
		d3.select(assetNode[0][0]).classed({"ap-highlighted-target": false});
    },

    /**
     * Return an room asset object.
     */
    getRoomAsset: function(room) {
		var svgId = ('drawingDiv-' + this.selectedFloors[0].dwgname + '-svg' );
		var assetId = (room.bl_id+";"+room.fl_id+";"+room.rm_id);
		assetId +=  (this.selectedFloors[0].layoutType==="Inventory") ?  "" : (";"+this.selectedFloors[0].layerName);

		var assetController = this.drawingControl.drawingController.getController("AssetController");
		var assetNode = assetController.getAssetById(svgId, assetId);
		//d3.select(asset.node()).style("fill", this.highlightedAssets[assetId].defaultColor);
		//d3.select(assetNode[0][0]).classed({"ap-highlighted-target": false});
		return assetNode;
    },	
    
    /**
     * view selected rooms in a pop up window.
     */
    drawingPanel_onViewDetails: function() {
        var anchor = this.drawingPanel.actionbar.toolbar.el.dom;
        this.trigger('app:mo:scenarios:console:viewSelectedRooms', anchor, { "dateStart": developMovesScenarios.isoDateStart, "layoutType": this.selectedFloors[0].layoutType, "layerName": this.selectedFloors[0].layerName} );
    },
    
    /**
     * Commit the assignment to WFR.
     */
    drawingPanel_onCommitPendingAssignments: function() {
		this.trigger('app:mo:scenarios:console:commitAssignment');
    },
    
    /**
     * We clear assignment target,clear drawing panel fills and refresh after the user commit pending assignments.
     */
    afterCommitPendingAssignment: function(isTeamAssignMode) {
    	this.clearAssignmentTarget(isTeamAssignMode);
		this.clearDrawingSelections();
		this.reloadDrawings();
    },
   
    /**
     * Cancel the pending assignments.
     */
    drawingPanel_onCancelPendingAssignments: function() {
    	this.clearAssignmentTarget();

		this.reloadDrawings();
		this.clearDrawingSelections();
        
        this.trigger('app:mo:scenarios:console:cancelAssignment');
    },
    
    /**
     * view the pending assignments in a popup window.
     */
    drawingPanel_onViewPendingAssignments: function() {
        var anchor = this.drawingPanel.actionbar.toolbar.el.dom;
        this.trigger('app:mo:scenarios:console:viewPendingAssignments', anchor);
    },
    
    /**
     * cancel the selected rooms when click the Clear button.
     */
    drawingPanel_onCancelSelectedRooms: function() { 
		this.clearDrawingSelections();

		this.selectedRooms = new Ab.space.express.console.Rooms();
    	//this.drawingPanel.refresh();
    	this.trigger('app:mo:scenarios:console:cancelSelectedRooms');
    },
    	
	/**
     * Open print option dialog for users to control print parameters.
     */
    openPrintOptionDialog: function() {
    	var thisController = this;
    	var printOptionDialog = View.openDialog("ab-mo-console-print-option-dialog.axvw", null, true, {
    		width:900,
    		height:600,
    		collapsible: false,
    		afterViewLoad: function(dialogView) {
    			var dialogController = dialogView.controllers.get("printOptionDialogController");
    			dialogController.callback = thisController.printOptionDialogCloseHandler.createDelegate(thisController);
    			dialogController.selectedPlanType = thisController.selectedPlanType;
    			dialogController.legendDataSource  = thisController.currentLegendDataSource;
    			dialogController.legendPanel = thisController.currentLegendPanel;
    			dialogController.selectionValues = thisController.selectionValues;
    		}
    	});
    },
    
	/**
     * When close the print option dialog and start to export pdf report.
     */
    printOptionDialogCloseHandler: function(parameters) {
		this.trigger('app:mo:scenarios:console:printCurrentInventoryPDF', this, 
			{ 'projectId':developMovesScenarios.projectId, 
				'scenarioId':developMovesScenarios.scenarioId, 
				'asOfDate': developMovesScenarios.isoDateStart, 
				'floors': this.selectedFloors, 
				'hocParameters': parameters
		});

		this.selectionValues = parameters.selectionValues;
    },
    
	/**
	 * Exports current drawing image in Docx. Supports to display passed restriction and title.
	 * Similar as Flash drawing control's export.
	 */
	onExportCurrentScenarioLayoutDrawing: function(){
		var controller = View.controllers.get('moConsoleDrawingExportCtrl');
		controller.printCurrentScenarioPDF( this);
	},

	/**
     * Open the plan types dialog to allow the users to select plan types option. 
     */
    openPlanTypesDialog: function() {
    	var thisController = this;
    	var planTypesDialog = View.openDialog("ab-mo-console-plan-types-dialog.axvw", null, true, {
    		width:500,
    		height:160,
    		collapsible: false,
    		afterViewLoad: function(dialogView) {
    			var dialogController = dialogView.controllers.get("planTypesGroupDialogController");
    			dialogController.callback = thisController.openPlanTypesDialogCloseHandler.createDelegate(thisController);
    			dialogController.initialOptionValue = thisController.selectedPlanType;
    		}
    	});
    },
    
    /**
     * Highlight by plan type.
     */
    openPlanTypesDialogCloseHandler: function(planType) {
    	if (planType != 'none') {
    		this.selectedPlanType = planType;
    		var activePlanTypeDs = View.dataSources.get('activePlanTypesDS');
    		var restriction = new Ab.view.Restriction();
    		restriction.addClause('active_plantypes.plan_type', planType, '=');
    		var records = activePlanTypeDs.getRecords(restriction);
    		if(records.length > 0) {
	    		var viewFile = records[0].getValue('active_plantypes.view_file');
	    		var highlightDsId = records[0].getValue('active_plantypes.hs_ds');
	    		var labelDsId = records[0].getValue('active_plantypes.label_ds'); 
	    		var legendDsId = records[0].getValue('active_plantypes.legend_ds');

	    		this.currentLegendDataSource = legendDsId;
	    		this.currentLegendPanel = legendDsId + '_panel'; 
	    		
	    		//this.drawingPanel.setDataSource('highlight', 'None');
	    		//this.drawingPanel.setDataSource('labels', 'None');
	    		//XXX: new changeDataSourceSelector() will just change the values without invoking unnecessary refresh call
	    		//this.drawingPanel.changeDataSourceSelector('highlight', 'None');
	    		//this.drawingPanel.changeDataSourceSelector('labels', 'None');
				document.getElementById('selector_labels').value = 'None';
				document.getElementById('selector_hilite').value = 'None';
	    		
	    		try{
	    			var realHighlightDs = View.dataSources.get(highlightDsId);
	    			if(!realHighlightDs) {
	    				//loadDataSourceFromFile will add the datasource to View.dataSources array.
	    				realHighlightDs = new Ab.data.loadDataSourceFromFile(viewFile, highlightDsId);
	    				realHighlightDs.fromFile = viewFile;
	    			}
	    			realHighlightDs.addParameters(this.filter.parameters);
	    			realHighlightDs.setRestriction(this.filter.restriction);
	    			
	    			var realLabelDs = View.dataSources.get(labelDsId);
	    			if(!realLabelDs) {
	    				realLabelDs = new Ab.data.loadDataSourceFromFile(viewFile, labelDsId);
	    				realLabelDs.fromFile = viewFile;
	    			}
	    			realLabelDs.addParameters(this.filter.parameters);
	    			realLabelDs.setRestriction(this.filter.restriction);
	    			
	    			var realLegendDs = View.dataSources.get(legendDsId);
	    			if (!realLegendDs) {
	    				realLegendDs = new Ab.data.loadDataSourceFromFile(viewFile, legendDsId);
	    				realLegendDs.fromFile = viewFile;
	    			}
	    			realLegendDs.addParameters(this.filter.parameters);
	    			realLegendDs.setRestriction(this.filter.restriction);
	    			
	    			//XXX: add new dataSources to View.dataSources for usage of printReport()   
	    			View.dataSources.add(highlightDsId, realHighlightDs);
	    			View.dataSources.add(labelDsId, realLabelDs);
	    			View.dataSources.add(legendDsId,realLegendDs);
	    			
		    		this.drawingPanel.currentHighlightDS = highlightDsId;
		    		this.drawingPanel.currentLabelsDS = labelDsId;
		    		//this.drawingPanel.refresh();
					this.reloadDrawings();
	    		} catch(e) {
	    			Workflow.handleError(e);
	    		}
    		}
    	}  else if (planType == 'none') {
			this.selectedPlanType = null;
			this.drawingPanel.currentHighlightDS = 'None';
			this.drawingPanel.currentLabelsDS = 'None';
			// this.drawingPanel.refresh();
			this.reloadDrawings();
		}
    },
    
    /**
     * Open the background dialog to allow the user to select background option.
     */
    openBackgroundDialog: function() {
    	var thisController = this;
    	var backgroundLayerDialog = View.openDialog("ab-mo-console-background-layer-dialog.axvw", null, true, {
    		width:650,
    		height:160,
    		collapsible: false,
    		afterViewLoad: function(dialogView) {
    			var dialogController = dialogView.controllers.get("backgroundLayerSelectController");
    			dialogController.callback = thisController.backgroundLayerDialogCloseHandler.createDelegate(thisController);
    			dialogController.initialOptionValue = thisController.backgroundOptionValue;
    		}
    	});
    },
    
    /**
     * Add background layer to the flash drawing.
     */
    backgroundLayerDialogCloseHandler: function(parameters) {
    	// store selected background file suffix as well as option value
		this.backgroundSuffix = parameters['rule_suffix'];
    	this.backgroundOptionValue = parameters['rule_id'];

    	// before clear the drawing panel, store selected floor-rows
    	var checkedLocation = this.locationsGrid.getSelectedRows();
    	
		// clear the drawing panel
		this.trigger('app:mo:scenarios:console:unselectfloors');

    	// re-check the previously cleared floor-rows as well as to show them in the drawings
		for (var i=0; i<checkedLocation.length; i++) {
			var row = checkedLocation[i];
			row.row.select(true);
			this.trigger('app:mo:scenarios:console:selectLocation', row);
		}
    },
			
    /**
     * Open placed assignments tabs.
     */
    viewPlacedAssignments:function(){
    	Ab.view.View.openDialog("ab-mo-console-placed-assignments.axvw", null, false, {
    		title : getMessage("placedAssignments"), 
    		collapsible: false, 
    		width:900, 
    		height:620
    	});
	},

   	initialDrawingActions: function(){
		this.drawingPanel.actions.get('moveActionsMenu').actions.get("saveMarkups").enable(false);

		this.drawingPanel.actions.get('exportDrawingPanelMenu').actions.get("printOptionAction").enable(false);
		this.drawingPanel.actions.get('exportDrawingPanelMenu').actions.get("exportLoadedDrawingToDOCX").enable(false);
	},

   	resetMarkups: function(){
		this.showMarkups = false; 
		this.loadMarkups(0);
		this.showMarkupWindow();
	},

	showMarkupWindow: function(){
           this.markupLegendPanel.showInWindow({
		    title: getMessage("markUp"), 
		    modal: false,
		    collapsible: true,
		    maximizable: false, 
		    width: 450, 
		    height: 250,
			closable: false
		});
	},

	showCurrentMarkups: function(){
		this.loadMarkups(1);
	},

   	showOtherMarkups: function(){
		this.loadMarkups(2);
	},

	hideMarkups: function(){
		this.loadMarkups(0);
	},

	loadMarkups: function(option){
		this.markupOption = option;
		if (option==0) {
			this.markupActivityLogId = 	 null;

			var parameters = this.constructMarkupParameter(option);
			this.loadDrawing(parameters);
		} 
		else {
			this.showAvailableMarkups(option);
		}
	},		

	constructMarkupParameter: function(option){
		var parameters = {};
		parameters['pkeyValues'] = {'bl_id':this.selectedFloors[0].bl_id, 'fl_id': this.selectedFloors[0].fl_id, 'filename': this.selectedFloors[0].filename};
		parameters['projectId'] = developMovesScenarios.projectId;
		parameters['loadOption'] = option;
		parameters['drawingName'] = this.selectedFloors[0].dwgname;
		parameters['layerName'] = (this.selectedFloors[0].layerName) ;
		return parameters;
	},		

   	showAvailableMarkups: function(option){
		if (this.selectedFloors && this.selectedFloors.length===1) {
			var markUpGrid = ( option==2 ? this.otherMarkupsGrid : this.currentMarkupsGrid );
    		var selectedFloor = this.selectedFloors[0];
			markUpGrid.addParameter("option", option);  
			markUpGrid.addParameter("dwgName", selectedFloor.dwgname);  
			markUpGrid.addParameter("layerName", selectedFloor.layerName);  
			markUpGrid.addParameter("projectId", developMovesScenarios.projectId); 
			
			markUpGrid.refresh();
			markUpGrid.showInWindow({
				anchor: this.drawingPanel.actions.get('moveActionsMenu').button.el.dom,
				closeButton: true,
				modal: true,
				collapsible: true, 
				maximizable: false, 
				width: 800, 
				height: 300
			});
			markUpGrid.resizeColumnWidths();
		}
		else {
			View.alert(getMessage("noLayout"));
		}
	},
				  
   	onSelectMarkup: function(option){
		var markUpGrid = ( option==2 ? this.otherMarkupsGrid : this.currentMarkupsGrid );
		var index = markUpGrid.selectedRowIndex;
		var row = markUpGrid.rows[index];
		
		this.markupActivityLogId = 	 row["activity_log.activity_log_id"];

		var parameters = this.constructMarkupParameter(this.markupOption);
		parameters["activityLogId"] = this.markupActivityLogId;
		if (option==2) {
			parameters["layerName"] = (row['afm_redlines.layer_name'] ? row['afm_redlines.layer_name'] : null);
		}
		this.loadDrawing(parameters);

		markUpGrid.closeWindow();
	},

   	onMarkupChange: function(node, drawingController){
		var drawingPanel = spaceExpressConsoleDrawing.drawingPanel;
		drawingPanel.actions.get('moveActionsMenu').actions.get("saveMarkups").enable(true);
   	},
   	
   	onMarkupDrop: function(node, drawingController){
		var drawingPanel = spaceExpressConsoleDrawing.drawingPanel;
		drawingPanel.actions.get('moveActionsMenu').actions.get("saveMarkups").enable(true);
   	},
	
   	saveMarkups: function(){
		if ( this.markupActivityLogId && this.markupOption!=2 ) {
			this.saveRedlineRecord(this.markupActivityLogId);
		}
		else {
			this.showActivityLogForm();
		}
	},

	showActivityLogForm: function() {
		this.activityLogForm.refresh(null, true);
		this.activityLogForm.showInWindow({
			anchor: this.drawingPanel.actions.get('moveActionsMenu').button.el.dom,
			closeButton: false,
			modal: true,
			collapsible: true, 
			maximizable: false, 
			width: 800, 
			height: 360
		});
	},
	
	activityLogForm_afterRefresh: function() {
		
		if ( this.activityLogForm.newRecord ) {
			this.activityLogForm.setFieldValue("activity_log.project_id",  developMovesScenarios.projectId);
			this.activityLogForm.setFieldValue("activity_log.created_by",  View.user.name);
			this.activityLogForm.setFieldValue("activity_log.bl_id", this.selectedFloors[0].bl_id);
			this.activityLogForm.setFieldValue("activity_log.fl_id", this.selectedFloors[0].fl_id);
			this.activityLogForm.setFieldValue("activity_log.activity_type", "PROJECT - TASK");
		}		
		
		//according to license determine if disable check box "create service request"
		document.getElementsByName("activityLogForm_createServiceRequest")[0].disabled=true;
		AdminService.getProgramLicense({
			callback: function(license) {
				var licenseIds = [];
				var licenses = license.licenses;
				
				//check AbBldgOpsHelpDesk license
				for(i in licenses){
					licenseIds.push(licenses[i].id);
					if ( licenses[i].enabled && ( licenses[i].id == 'AbBldgOpsHelpDesk') ) {
						document.getElementsByName("activityLogForm_createServiceRequest")[0].disabled=false;
						return true;
					}
				}

				View.alert(getMessage('noMoveLicense'));
				return false;
			},				
			errorHandler: function(m, e) {
				View.showException(e);
			}
		});
	},

	activityLogForm_onSaveActivityLog: function() {
		if ( this.activityLogForm.canSave() ) {
			var formSaved = this.activityLogForm.save();

			if ( formSaved ) {
				var pkFieldName = this.activityLogForm.getPrimaryKeyFields()[0];
				var activityLogId = this.activityLogForm.getPrimaryKeyFieldValues()[pkFieldName];
				//var activityLogId = this.activityLogForm.getFieldValue('activity_log.activity_log_id');
				if ( activityLogId ) {
					//if ( "create"=== this.activityLogForm.getCheckboxValues("createServiceRequest")[0]) {
					// call wfr submitRequest
					// If the Create Service Request checkbox is checked, then initiate the Service Desk WFR to create a request.
					if ( document.getElementsByName("activityLogForm_createServiceRequest")[0].checked ){
						try {
							var record =  ABHDC_getDataRecord2(this.activityLogForm);
							result = Workflow.callMethod('AbBldgOpsHelpDesk-RequestsService-submitRequest', activityLogId, record );
						}catch(e){
								Workflow.handleError(e);
							return;
						}
					}	
					this.saveRedlineRecord(activityLogId);
				}
			}
			this.activityLogForm.closeWindow();
		}
	},

	saveRedlineRecord: function( activityLogId ) {
    	var parameters = {};

    	parameters.activityLogId = activityLogId;
    	
		var pKeyValues = {};
    	pKeyValues['bl_id'] = this.selectedFloors[0].bl_id;
    	pKeyValues['fl_id'] = this.selectedFloors[0].fl_id;
		parameters.pKeyValues = pKeyValues;
    	
		parameters.drawingName =  this.selectedFloors[0].dwgname;
    	parameters.layerName =  this.selectedFloors[0].layerName;
    	
    	parameters.viewBox = this.drawingControl.getViewBox("drawingDiv", this.selectedFloors[0].dwgname);
    	
    	//call MarkupControl's save() in order to invoke the onSave() event handler
    	this.drawingControl.getAddOn("MarkupControl").save(parameters, true);

		// after save a new markup, set current markup's activity id to be just created one, and reset  current markupOption to '1'.
		this.markupActivityLogId = activityLogId;
		this.markupOption=1;

		this.drawingPanel.actions.get('moveActionsMenu').actions.get("saveMarkups").enable(false);
	},
		
   	onSave: function(params){
		/*
		if(params.autoNumber)
   			alert("activity_log.activity_log_id=" + params.activityLogId + "\nafm_redline.auto_number=" + params.autoNumber);
   		else
   			alert("activity_log.activity_log_id=" + params.activityLogId + "\nNo markups changed.");\
			*/
   	},

	updateScenario: function(){
		try{
			var result = Workflow.callMethod('AbMoveManagement-MoveService-updateMoveScenario', developMovesScenarios.projectId, developMovesScenarios.scenarioId, []);
			this.emToLocateGrid.refresh();
			this.teamToLocateGrid.refresh();
			this.refreshEmployeeWaitingPanel();
			this.layoutsGrid.refresh();
		}
		catch (e){
			Workflow.handleError(e);
		}	
	},
		
	showCurrentPending: function(){
		this.highlightPendingBorder = !this.highlightPendingBorder;

		if ( this.highlightPendingBorder ){
			if (this.selectedFloors && this.selectedFloors.length===1) {
				var selectedFloor = this.selectedFloors[0];
				
				this.rmPendingRequestsDs.addParameter("buildingId", selectedFloor.bl_id);  
				this.rmPendingRequestsDs.addParameter("floorId", selectedFloor.fl_id);  
				this.rmPendingRequestsDs.addParameter("projectId", developMovesScenarios.projectId);  
				this.rmPendingRequestsDs.addParameter("moveDate", developMovesScenarios.isoDateStart);  

				var pendingRooms = this.rmPendingRequestsDs.getRecords();			
				if (pendingRooms && pendingRooms.length > 0) {
					var rooms=[];
					for (var i = 0;  i < pendingRooms.length; i++) {
						var assetId =  pendingRooms[i].getValue("rmpct.bl_id")+";"+pendingRooms[i].getValue("rmpct.fl_id")+";"+pendingRooms[i].getValue("rmpct.rm_id");
						assetId = assetId + (  "Inventory"==this.drawingControl.layoutType ? "" : (";"+this.selectedFloors[0].layerName) );
						rooms.push(assetId);
					}
					var opts = { cssClass: 'zoomed-asset-red-bordered',		// use the cssClass property to specify a css class
						removeStyle: true,		// use the removeStyle property to specify whether or not the fill should be removed (for example  cssClass: 'zoomed-asset-bordered'  and removeStyle: false
						svgId:   'drawingDiv-' + selectedFloor.dwgname + '-svg',
						zoomFactor: 0
					};
					this.drawingControl.getAddOn("AssetLocator").findAssets(rooms, opts);
				}
			}
			this.drawingPanel.actions.get('moveActionsMenu').actions.get("showCurrentPending").setTitle(getMessage("hidePending"));
		 }
		else {
			this.drawingControl.getAddOn("AssetLocator").clearFoundAssets();
			this.drawingPanel.actions.get('moveActionsMenu').actions.get("showCurrentPending").setTitle(getMessage("showPending"));
		}
	},

	listPendingRequests: function(){
		if (this.selectedFloors && this.selectedFloors.length===1) {
    		var selectedFloor = this.selectedFloors[0];
			this.rmPendingRequestsGrid.addParameter("buildingId", selectedFloor.bl_id);  
			this.rmPendingRequestsGrid.addParameter("floorId", selectedFloor.fl_id);  
			this.rmPendingRequestsGrid.addParameter("projectId", developMovesScenarios.projectId);  
			this.rmPendingRequestsGrid.addParameter("moveDate", developMovesScenarios.isoDateStart);
			this.rmPendingRequestsGrid.refresh();
			this.rmPendingRequestsGrid.showInWindow({
				anchor: this.drawingPanel.actions.get('moveActionsMenu').button.el.dom,
				closeButton: true,
				modal: true,
				collapsible: true, 
				maximizable: false, 
				width: 800, 
				height: 600
			});
		}
	},
	
	updateProject: function(params){
		this.duplicateFloorDs.addParameter("projectId", developMovesScenarios.projectId);
		this.duplicateFloorDs.addParameter("scenarioId",  developMovesScenarios.scenarioId);
		var records =  this.duplicateFloorDs.getRecords();
		if ( records.length>0 ) {
			View.alert(getMessage('duplicateFloor'));
			return;
		}
		try{
			View.openProgressBar(getMessage('saving'));
			var result = Workflow.callMethod('AbMoveManagement-MoveService-updateMoveProject', developMovesScenarios.projectId, developMovesScenarios.scenarioId);
			View.closeProgressBar();
			View.showMessage(getMessage("msg_project_updated")+" "+ developMovesScenarios.scenarioId);
		}
		catch (e){
			View.closeProgressBar();
			Workflow.handleError(e);
		}	
	},

	copyOccupancy: function(){
		View.openDialog("ab-mo-console-copy-occupancy-scenario.axvw", null, false, {
			width: 1000,
			height: 800,
			title: getMessage("copyOccupancy"),
			closeButton: true
		});
	}
});

/**
 * Set legend text according the legend level value.
 * @param {Object} row
 * @param {Object} column
 * @param {Object} cellElement
 */
function setLegendLabel(row, column, cellElement) {
    var value = row[column.id];
    if (column.id == 'legend.value' && value != '') {
        var text = value;
        switch (value) {
        	case '0':
        		text = getMessage('legendLevel0');
        		break;
            case '1':
                text = getMessage('legendLevel1');
                break;
            case '2':
                text = getMessage('legendLevel2');
                break;
            case '3':
                text = getMessage('legendLevel3');
                break;
            case '4':
                text = getMessage('legendLevel4');
                break;
        }
        var contentElement = cellElement.childNodes[0];
        if (contentElement)
         contentElement.nodeValue = text;
    }
}
