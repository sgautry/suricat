var abMoConsoleLayoutsList = View.createController('abMoConsoleLayoutsList', {   
	// store current selected so that can make the status of layout grid's checked row is consistent with loaded drawing.
	currentSelectedRow: null, 
    
	afterCreate: function() {
        this.on('app:mo:scenarios:console:refresh', this.afterInitialDataFetch);
    },

	afterViewLoad:function(){
		var controller = this;
		var grid =  this.layoutsGrid;
		this.layoutsGrid.addEventListener('onMultipleSelectionChange', function(row) {

			if ( row.row.isSelected() ) {
				var rows = grid.getSelectedRows(); 
				for(var i=0;i< rows.length;i++){
					if ( rows[i]!=row) {
						rows[i].row.select(false);
						controller.trigger('app:mo:scenarios:console:selectLocation', rows[i]);
					}
				}
				controller.currentSelectedRow = row;
			}
			else {
				controller.currentSelectedRow = null;
			}
            controller.trigger('app:mo:scenarios:console:selectLocation', row);
        });
	},
	
	afterInitialDataFetch:function(){
		if ( developMovesScenarios && developMovesScenarios.projectId && developMovesScenarios.scenarioId ) {
			var restriction = new Ab.view.Restriction();
			restriction.addClause("mo_scenario_em.project_id", developMovesScenarios.projectId);
			restriction.addClause("mo_scenario_em.scenario_id", developMovesScenarios.scenarioId);
			this.layoutsGrid.refresh(restriction);
		}
	},
	
	layoutsGrid_afterRefresh: function(param) {
		//check whether it's a scenario layout; if yes then mark the row's bl_Id+fl_id+dwg_name+layer_name with red color
		var param = param;
		var foundChecked = false;
		this.layoutsGrid.gridRows.each( function(row) {
	   		var usage_type = row.getFieldValue('mo_scenario_em.vf_usage_type');
			if(usage_type=="Scenario"){
				var columnNum = row.dom.cells.length;
				for(var i=1;i<4;i++){
					row.dom.cells[i].innerHTML = '<font color="red">'+row.dom.cells[i].innerText+'</font>'
				}
			}

			if ( abMoConsoleLayoutsList.currentSelectedRow ){
				if ( row.record["mo_scenario_em.to_bl_id"]===abMoConsoleLayoutsList.currentSelectedRow["mo_scenario_em.to_bl_id"]
					&& row.record["mo_scenario_em.to_fl_id"]===abMoConsoleLayoutsList.currentSelectedRow["mo_scenario_em.to_fl_id"]
					&& row.record["mo_scenario_em.filename"]===abMoConsoleLayoutsList.currentSelectedRow["mo_scenario_em.filename"] ) {
					row.select();
					foundChecked = true;
				}
			}
	   	});

		if ( !foundChecked && abMoConsoleLayoutsList.currentSelectedRow ) {
			abMoConsoleLayoutsList.currentSelectedRow.row.select(false);
			abMoConsoleLayoutsList.trigger('app:mo:scenarios:console:selectLocation', abMoConsoleLayoutsList.currentSelectedRow);
			abMoConsoleLayoutsList.currentSelectedRow = null;
		}
		
		jQuery("#layoutsGrid_title").css({"font-size":17,"font-weight":"600"});
	},
	
	layoutsGrid_onRemoveLayout: function(row) {
		var me = this;
        if ( this.isLayoutInUse(row) ){ 
        	var message = getMessage('confirmMessage');
        	View.confirm(message, function(button){
                if (button == 'yes') {
					me.deleteSelectedLayout( row );
                }
            });
        } else {
			me.deleteSelectedLayout( row );
        } 
    },

	isLayoutInUse : function(row){        
		//use data source checkLayoutInUseDs to check whether this layout is in-use
        this.checkLayoutInUseDs.addParameter("toFlId", row.record['mo_scenario_em.to_fl_id']);
        this.checkLayoutInUseDs.addParameter("toBlId", row.record['mo_scenario_em.to_bl_id']);
        this.checkLayoutInUseDs.addParameter("filename", row.record['mo_scenario_em.filename']);
        this.checkLayoutInUseDs.addParameter("scenarioId",developMovesScenarios.scenarioId);
        this.checkLayoutInUseDs.addParameter("projectId",developMovesScenarios.projectId);

        var layouts = this.checkLayoutInUseDs.getRecords();
		return 	  ( layouts==null || layouts.length<=0 ) ? false: true;
	},
    
    deleteSelectedLayout: function(row){
		var layout={};
		layout['mo_scenario_em.to_bl_id'] = row.record['mo_scenario_em.to_bl_id'];
		layout['mo_scenario_em.to_fl_id'] = row.record['mo_scenario_em.to_fl_id'];
		layout['mo_scenario_em.filename'] = row.record['mo_scenario_em.filename'];
		layout['mo_scenario_em.project_id'] = developMovesScenarios.projectId;
		layout['mo_scenario_em.scenario_id'] =  developMovesScenarios.scenarioId;

 		try {
			Workflow.callMethod("AbMoveManagement-MoveService-deleteLayout", layout);

			View.panels.get('emToLocateGrid').refresh();
			this.layoutsGrid.refresh();
			//this.reCheckSelectedLayouts();
		}
		catch (e) {
	    	Workflow.handleError(e);
		}			
	}
});