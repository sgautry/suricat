var abMoScenariosTeamToLocate = View.createController('abMoScenariosTeamToLocate', {   
	
	isRefreshIconCreated: false,

    afterCreate: function() {
        this.on('app:mo:scenarios:console:refresh', this.afterInitialDataFetch);
    },

	afterInitialDataFetch: function(){
		
		this.loadTeamToLocateGrid();
		
		developMovesScenarios.addRefreshButton("exportTeamToLocateMenu", "Team", this);

		this.isRefreshIconCreated = true;

	},
	 
	refreshAfterDataTransferIn: function(){
		this.loadTeamToLocateGrid();
	},
	
    loadTeamToLocateGrid:function() {
    	this.teamToLocateGrid.addParameter("asOfDate", developMovesScenarios.isoDateStart);
		this.teamToLocateGrid.addParameter("projectId", developMovesScenarios.projectId);
		this.teamToLocateGrid.addParameter("scenarioId", developMovesScenarios.scenarioId);
		this.teamToLocateGrid.refresh();
    },
    
	/**
     * Assign rooms to a team.
     */
	teamToLocateGrid_onAssignTeam: function() {
    	var row = this.teamToLocateGrid.rows[this.teamToLocateGrid.selectedRowIndex];
    	this.trigger('app:mo:scenarios:console:beginAssignment',{
    		type:'team',
    		team_id:row['team_properties.team_id'],
    		team_name:row['team_properties.team_name'],
    		hpattern:row['team_properties.hpattern_acad'],
			date_start: developMovesScenarios.isoDateStart
    	});
    },
    
    /**
     * Commit team pending assignments.
     */
    teamPendingAssignmentPanel_onCommitTeamPendingAssignments: function() {
    	this.trigger('app:mo:scenarios:console:commitAssignment');
    },
    
    /**
     * Cancel team pending assignments.
     */
    teamPendingAssignmentPanel_onCancelTeamPendingAssignments: function() {
    	this.trigger('app:mo:scenarios:console:cancelAssignment');
    },
    
    /**
     * Remove a team assignment.
     */
    onRemoveTeamPendingAssignment: function() {
		var rowIndex = this.teamPendingAssignmentPanel.selectedRowIndex;
		var row = this.teamPendingAssignmentPanel.gridRows.get(rowIndex);
    	this.trigger('app:mo:scenarios:console:removeAssignment', {
    		bl_id: row.record['rm_team.bl_id'],
    		fl_id: row.record['rm_team.fl_id'],
    		rm_id: row.record['rm_team.rm_id'],
    		team_id: row.record['rm_team.team_id']
    	});
    },
    
    /**
     * get buildings for team.
     */
    teamToLocateGrid_afterRefresh:function(){
    	this.teamToLocateGrid.gridRows.each(function(row) {

			for(var i=0;i<row.panel.columns.length;i++){
				if(row.panel.columns[i].fullName=='team_properties.vf_bl_id'&&!row.panel.columns[i].hidden){
					var teamId = row.getFieldValue('team_properties.team_id');
					var asOfDate = developMovesScenarios.isoDateStart;
					var buildings = this.getBuildingList(teamId,asOfDate);
					row.setFieldValue("team_properties.vf_bl_id",buildings);
				}
			}
			
		}, this);
    	
    },
    
    /**
     * get building list.
     */
    getBuildingList: function (teamId,asOfDate){
    	
    	var building_ds = View.dataSources.get("building_ds");
    	building_ds.addParameter('teamId', teamId);
    	building_ds.addParameter('asOfDate', asOfDate);
    	building_ds.addParameter("projectId", developMovesScenarios.projectId);
    	building_ds.addParameter("scenarioId", developMovesScenarios.scenarioId);
    	var blRecords=building_ds.getRecords();
    	
    	var bl_id="";
    	
    	for(var i = 0; i < blRecords.length; i++) {
    		var blRecord = blRecords[i];
    		bl_id += blRecord.getValue('rm_team.bl_id')+",";
    	}
    	if ( valueExistsNotEmpty(bl_id) ) {
			return bl_id.substring(0,bl_id.length-1);
		}
    	return "";
    	
    },
    
    beforeDataTransfer: function() {
    	var restriction = new Ab.view.Restriction();
		restriction.addClause('mo_scenario_em.project_id', developMovesScenarios.projectId);
		restriction.addClause('mo_scenario_em.scenario_id', developMovesScenarios.scenarioId);
		restriction.addClause('mo_scenario_em.em_id', "", "is null");
		restriction.addClause('mo_scenario_em.team_id', "", "is not null");
		this.teamDataTransferGrid.restriction= restriction;
    }
});