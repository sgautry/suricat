var abMoAreasByOrganizationController = View.createController('abMoAreasByOrganizationController', {

	/**
     * Insert filter message span and checkbox.
     * Initialize the department tree.
     */
	afterInitialDataFetch: function() {
    	this.initialReport();
    	this.AreaByOrganization2dTable.refresh();
    	prepareCustomXls(this.AreaByOrganization2dTable);
    },
    
	initialReport: function(){
		var rows = View.getOpenerView().panels.get("list_abMoveScenario").getSelectedRows();
		for(var i=0; i < rows.length; i++){
			var param = {
				"projectId": rows[i]['mo_scenario.project_id'],
				"scenarioId": rows[i]['mo_scenario.scenario_id']
			}
			
			this.AreaByOrganization2dTable.addParameter("selectedScenario"+(i+1), rows[i]['mo_scenario.scenario_id']);
			this.AreaByOrganization2dTable.addParameter("projectId", rows[i]['mo_scenario.project_id']);
		}
		
		this.AreaByOrganization2dTable.addParameter("unassigned", getMessage("Unassigned"));
	},
	
	AreaByOrganization2dTable_afterRefresh:function(){
		getTotalResult("AreaByOrganization2dTable_table");
	}
});

	