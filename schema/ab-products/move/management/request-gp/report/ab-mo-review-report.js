var abMoReviewReport = View.createController('abMoReviewReport',{
	
	parentController: null,
	
	projectId:"",
	
	reportList:null,
	
	afterViewLoad: function(){
		if(View.controllers.get('tabsController')){
			this.parentController = View.controllers.get('tabsController');
		}else if(View.getOpenerView().controllers.get('tabsController')){
			this.parentController = View.getOpenerView().controllers.get('tabsController');
		}else if(View.getOpenerView().controllers.get('abEamCptProjConsoleCtrl')){
			//EAM Move Management Integration - Adding Review Move Scenarios to Project Proposal Console APP-1380
			this.parentController = View.getOpenerView().controllers.get('abEamCptProjConsoleCtrl');
		}
	},

	afterInitialDataFetch: function(){
		this.refresh();		
		this.createReportDropdownList();
	},
	
	refresh: function(){
		var restriction = new Ab.view.Restriction();
		if(valueExistsNotEmpty(this.parentController)&&valueExistsNotEmpty(this.parentController.project_id)){
			//for move console
			restriction.addClause('mo_scenario_em.project_id', this.parentController.project_id, '=');
		}else if(valueExistsNotEmpty(this.parentController)&&valueExistsNotEmpty(this.parentController.projectId)){
			//for EAM Move Management Integration
			restriction.addClause('mo_scenario_em.project_id', this.parentController.projectId, '=');
		}else {
			restriction.addClause('mo_scenario_em.project_id', "project 1", '=');
		}
		
		this.list_abMoveScenario.refresh(restriction);
		
		this.removeOpenedTabs();
	},

	createReportDropdownList: function(){
		var paginatedReportBtn = document.getElementById("showReportBtn").parentNode;
		var panelTitleNode = this.list_abMoveScenario.getTitleEl().dom.parentNode.parentNode;
		var cell = Ext.DomHelper.insertBefore(paginatedReportBtn, {
			tag : 'td',
			id : 'txtShowReport'
		});
		var tn = Ext.DomHelper.append(cell, getMessage("show_report"));
		Ext.DomHelper.applyStyles(tn, "x-btn-text");
		
		cell = Ext.DomHelper.insertBefore(paginatedReportBtn, {
			tag : 'td',
			id : 'reportList_options_td'
		});
		
		var options = Ext.DomHelper.append(cell, {
			tag : 'select',
			id : 'reportList_options'
		}, true);
		
		//add report by option list
		options.dom.options[0] = new Option(getMessage("areasByOrganization"), '0');
		options.dom.options[1] = new Option(getMessage("areasByOrganizationAndOccupiedAreasOnly"), '1');
		options.dom.options[2] = new Option(getMessage("areasByCategory"), '2');
		options.dom.options[3] = new Option(getMessage("areasByCategoryAndOccupiedAreasOnly"), '3');
		options.dom.options[4] = new Option(getMessage("totalOccupiableArea"), '4');
		options.dom.options[5] = new Option(getMessage("totalOccupiableAreaAndOccupiedAreasOnly"), '5');
		options.dom.options[6] = new Option(getMessage("totalUsableArea"), '6');
		options.dom.options[7] = new Option(getMessage("averageAreaPerEmployee"), '7');
		options.dom.options[8] = new Option(getMessage("ruRatio"), '8');
		options.dom.selectedIndex = 0;
	},

	getSelectedOption: function(){
		var reportList_options = Ext.get("reportList_options");
		return reportList_options.dom.options[reportList_options.dom.selectedIndex];
	},
	
	onShowReport: function(){
		this.showReportTabs.show(true);
		//selection is no more than three
		if(!this.checkSelectedScenarios()){
			return;
		}
		
		var currentOption = this.getSelectedOption();
		
		switch(currentOption.value){
		case "0":
			this.createAreasByOrganization();
			break;
		case "1":
			this.createAreasByOrganizationAndOccupiedAreasOnly();
			break;
		case "2":
			this.createAreasByCategory();
			break;
		case "3":
			this.createAreasByCategoryAndOccupiedAreasOnly();
			break;
		case "4":
			this.createTotalOccupiableArea();
			break;
		case "5":
			this.createTotalOccupiableAreaAndOccupiedAreasOnly();
			break;
		case "6":
			this.createTotalUsableArea();
			break;
		case "7":
			this.createAverageAreaPerEmployee();
			break;
		case "8":
			this. createRURatio();
			break;
		}
	},
	
	checkSelectedScenarios:function(){
		var rows = this.list_abMoveScenario.getSelectedGridRows();
		if(rows.length>3){
			View.alert(getMessage("selection_info"));
			return false;
		}
		return true;
	},
	
	removeOpenedTabs: function(){
		
		var tabs = this.showReportTabs.tabs;
		for(var i=tabs.length-1; i>0; i--){
			this.showReportTabs.closeTab(this.showReportTabs.tabs[i].name);
		}
	},

	createReports: function(viewName){
		var rows = this.list_abMoveScenario.getSelectedGridRows();
		if(rows.length==0){
			View.alert(getMessage("select_one_item"));
			return;
		}

		this.showReportTabs.createTab(viewName);
		
		var currentOption = this.getSelectedOption();
		
		this.showReportTabs.tabs[this.showReportTabs.tabs.length-1].setTitle(currentOption.label);
	},
	
	addTitleFromAction:function(index){
		if(valueExistsNotEmpty(index)){
			var title = this.list_abMoveScenario.actions.get("showScenarioReport").actions.get(index).config.text;
			this.list_abMoveScenario.setTitle(title);
		}
	},
	
	createAreasByOrganization: function(i){
		
		var viewName = "ab-mo-areas-by-organization-report.axvw";
		this.createReports(viewName);
		
	},

	createAreasByOrganizationAndOccupiedAreasOnly: function(i){
		
		var viewName = "ab-mo-areas-by-organization-and-occupied-areas-only-report.axvw";
		this.createReports(viewName);
		
	},
	
	createAreasByCategory: function(i){
		
		var viewName = "ab-mo-areas-by-category-report.axvw";
		this.createReports(viewName);
		
	},
	
	createAreasByCategoryAndOccupiedAreasOnly: function(i){

		var viewName = "ab-mo-areas-by-category-and-occupied-areas-only-report.axvw";
		
		this.createReports(viewName);
		
	},
	
	createTotalOccupiableArea: function(i){
		
		var viewName = "ab-mo-total-occupiable-area-report.axvw";
		
		this.createReports(viewName);
		
	},
	
	createTotalOccupiableAreaAndOccupiedAreasOnly: function(i){
		
		var viewName = "ab-mo-total-occupiable-area-and-occupied-areas-only-report.axvw";
		
		this.createReports(viewName);
		
	},
	
	createTotalUsableArea: function(i){
		
		var viewName = "ab-mo-total-usable-area-report.axvw";
		
		this.createReports(viewName);
		
	},
	
	createAverageAreaPerEmployee: function(i){
		
		var viewName = "ab-mo-average-area-per-employee-report.axvw";
		
		this.createReports(viewName);
		
	},
	
	createRURatio: function(i){
		
		var viewName = "ab-mo-ru-ratio-report.axvw";
		
		this.createReports(viewName);
		
	}
});

/**
 * used for paginated report as command function 
 * 
 * @param {Object} type - values 'group', 'single', 'scenario' 
 * @param {Object} commandObject
 */
function onPaginatedReport(type, commandObject){
	var panel = commandObject.getParentPanel();
	var projectId = "";
	var moveId = "";
	
	if(type == 'group'){
		projectId = panel.getFieldValue('project.project_id');
	}else if(type == 'single'){
		moveId = panel.getFieldValue('mo.mo_id');
	}else if(type == 'scenario'){
		var isAssigned = false;
		panel.gridRows.each(function(row){
			if(valueExistsNotEmpty(row.getRecord().getValue('mo_scenario_em.to_rm_id'))){
				isAssigned = true;
			}
		});
		if(panel.gridRows.length > 0 && isAssigned){
			var row = panel.gridRows.get(0);
			moveId = row.getRecord().getValue('mo_scenario_em.scenario_id');
			projectId = row.getRecord().getValue('mo_scenario_em.project_id');
		}else{
			View.showMessage(getMessage('error_no_data_rpt'));
			return;
		}
	}
	
	var result = Workflow.callMethod('AbMoveManagement-MoveService-onPaginatedReport', type, projectId, moveId);

    if (valueExists(result.jsonExpression) && result.jsonExpression != '') {
		result.data = eval('(' + result.jsonExpression + ')');
		var jobId = result.data.jobId;
		var url = 'ab-paginated-report-job.axvw?jobId=' + jobId;
		View.openDialog(url);
	}
}
