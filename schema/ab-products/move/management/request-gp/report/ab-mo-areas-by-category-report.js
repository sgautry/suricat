var abMoAreasByCategoryController = View.createController('abMoAreasByCategoryController', {

	/**
     * Initialize the report.
     */
	afterInitialDataFetch: function() {
    	this.initialReport();
    	this.AreaByCategory2dTable.refresh();
    	prepareCustomXls(this.AreaByCategory2dTable);
    },
    
	initialReport: function(){
		var rows = View.getOpenerView().panels.get("list_abMoveScenario").getSelectedRows();
		for(var i=0; i < rows.length; i++){
			var param = {
				"projectId": rows[i]['mo_scenario.project_id'],
				"scenarioId": rows[i]['mo_scenario.scenario_id']
			}
			
			this.AreaByCategory2dTable.addParameter("selectedScenario"+(i+1), rows[i]['mo_scenario.scenario_id']);
			this.AreaByCategory2dTable.addParameter("projectId", rows[i]['mo_scenario.project_id']);
		}
		
		this.AreaByCategory2dTable.addParameter("unassigned", getMessage("Unassigned"));
	},
	
	AreaByCategory2dTable_afterRefresh:function(){
		getTotalResult("AreaByCategory2dTable_table");
	}
});

	