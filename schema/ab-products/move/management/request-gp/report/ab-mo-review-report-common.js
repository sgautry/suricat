function getTotalResult(tableName){
	var table = document.getElementById(tableName);
	var rows = table.rows.length;
	var total=0,scenario1=0,scenario2=0,scenario3=0;
	var ds = View.dataSources.get("rm_area_ds");
	
	for(var i=3;i<rows;i++){
		
		var title = table.rows[i].cells[0].innerText;
		if(title.indexOf('/')<0){
			if(table.rows[i].cells[2]&&Number(ds.parseValue("rm.area",table.rows[i].cells[2].innerText,true))>0){
				total+=Number(ds.parseValue("rm.area",table.rows[i].cells[2].innerText,true));
			}
			if(table.rows[i].cells[3]&&Number(ds.parseValue("rm.area",table.rows[i].cells[3].innerText,true))>0){
				scenario1+=Number(ds.parseValue("rm.area",table.rows[i].cells[3].innerText,true));
			}
			if(table.rows[i].cells[4]&&Number(ds.parseValue("rm.area",table.rows[i].cells[4].innerText,true))>0){
				scenario2+=Number(ds.parseValue("rm.area",table.rows[i].cells[4].innerText,true));
			}
			if(table.rows[i].cells[5]&&Number(ds.parseValue("rm.area",table.rows[i].cells[5].innerText,true))>0){
				scenario3+=Number(ds.parseValue("rm.area",table.rows[i].cells[5].innerText,true));
			}
		}
	}
	
	if(table.rows[2].cells[2]){
		table.rows[2].cells[2].innerText = ds.formatValue("rm.area",total,true);
	}
	if(table.rows[2].cells[3]){
		table.rows[2].cells[3].innerText = ds.formatValue("rm.area",scenario1,true);;
	}
	if(table.rows[2].cells[4]){
		table.rows[2].cells[4].innerText = ds.formatValue("rm.area",scenario2,true); ;
	}
	if(table.rows[2].cells[5]){
		table.rows[2].cells[5].innerText = ds.formatValue("rm.area",scenario3,true); ;
	}
}

function prepareCustomXls(crossTable){
	crossTable.callXLSReportJob = function(reportTitle, restriction){
		var parameters = this.getParameters();
		parameters.handler="com.archibus.app.move.report.xls.Cutom2DCrossTabXLSBuilder"
		var reportViewName = this.viewDef.viewName + '.axvw';
		var jobId = Workflow.startJob(Ab.view.CrossTable.WORKFLOW_RULE_XLS_REPORT, reportViewName, this.dataSourceId, this.dataType, reportTitle, this.groupByFields, 
		this.calculatedFields, this.getDataSource().sortFieldDefs, this.rowDimensionDataSourceId, this.columnDimensionDataSourceId, this.rowMeasureFields, toJSON(restriction), parameters);
		return jobId;
	}
}
