var abMoAreasByOrganizationAndOccupiedAreasOnlyController = View.createController('abMoAreasByOrganizationAndOccupiedAreasOnlyController', {
	
	/**
     * Insert filter message span and checkbox.
     * Initialize the department tree.
     */
	afterInitialDataFetch: function() {
    	this.initialReport();
    	this.AreaByOrganizationAndOccupiedAreasOnly2dTable.refresh();
    	prepareCustomXls(this.AreaByOrganizationAndOccupiedAreasOnly2dTable);
    },
    
	initialReport: function(){
		var rows = View.getOpenerView().panels.get("list_abMoveScenario").getSelectedRows();
		for(var i=0; i < rows.length; i++){
			var param = {
				"projectId": rows[i]['mo_scenario.project_id'],
				"scenarioId": rows[i]['mo_scenario.scenario_id']
			}
			
			this.AreaByOrganizationAndOccupiedAreasOnly2dTable.addParameter("selectedScenario"+(i+1), rows[i]['mo_scenario.scenario_id']);
			this.AreaByOrganizationAndOccupiedAreasOnly2dTable.addParameter("projectId", rows[i]['mo_scenario.project_id']);
		}
		
		this.AreaByOrganizationAndOccupiedAreasOnly2dTable.addParameter("unassigned", getMessage("Unassigned"));
	},
	
	AreaByOrganizationAndOccupiedAreasOnly2dTable_afterRefresh:function(){
		getTotalResult("AreaByOrganizationAndOccupiedAreasOnly2dTable_table");
		
		
	}
});

	