var abMoAreasByCategoryAndOccupiedAreasOnlyController = View.createController('abMoAreasByCategoryAndOccupiedAreasOnlyController', {
	
	/**
     * Insert filter message span and checkbox.
     * Initialize the department tree.
     */
	afterInitialDataFetch: function() {
    	this.initialReport();
    	this.AreaByCategoryAndOccupiedAreasOnly2dTable.refresh();
    	prepareCustomXls(this.AreaByCategoryAndOccupiedAreasOnly2dTable);
    },
    
	initialReport: function(){
		var rows = View.getOpenerView().panels.get("list_abMoveScenario").getSelectedRows();
		for(var i=0; i < rows.length; i++){
			var param = {
				"projectId": rows[i]['mo_scenario.project_id'],
				"scenarioId": rows[i]['mo_scenario.scenario_id']
			}
			
			this.AreaByCategoryAndOccupiedAreasOnly2dTable.addParameter("selectedScenario"+(i+1), rows[i]['mo_scenario.scenario_id']);
			this.AreaByCategoryAndOccupiedAreasOnly2dTable.addParameter("projectId", rows[i]['mo_scenario.project_id']);
		}
		
		this.AreaByCategoryAndOccupiedAreasOnly2dTable.addParameter("unassigned", getMessage("Unassigned"));
	},
	
	AreaByCategoryAndOccupiedAreasOnly2dTable_afterRefresh:function(){
		getTotalResult("AreaByCategoryAndOccupiedAreasOnly2dTable_table");
	}
});

	