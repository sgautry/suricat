var abMoTotalOccupiableAreaAndOccupiedAreasOnlyController = View.createController('abMoTotalOccupiableAreaAndOccupiedAreasOnlyController', {

	/**
     * Insert filter message span and checkbox.
     * Initialize the department tree.
     */
	afterInitialDataFetch: function() {
    	this.initialReport();
    	this.totalOccupiableAreaAndOccupiedAreasOnly2dTable.refresh();
    },
    
	initialReport: function(){
		var rows = View.getOpenerView().panels.get("list_abMoveScenario").getSelectedRows();
		for(var i=0; i < rows.length; i++){
			var param = {
				"projectId": rows[i]['mo_scenario.project_id'],
				"scenarioId": rows[i]['mo_scenario.scenario_id']
			}
			
			this.totalOccupiableAreaAndOccupiedAreasOnly2dTable.addParameter("selectedScenario"+(i+1), rows[i]['mo_scenario.scenario_id']);
			this.totalOccupiableAreaAndOccupiedAreasOnly2dTable.addParameter("projectId", rows[i]['mo_scenario.project_id']);
		}
	}
});

	