var controller = View.createController('abEditMoveSelectValueDrawing_Controller', {

	svgControl: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
    
     //----------------event handle--------------------
    
    afterViewLoad: function(){
    	
		this.openerPanel = this.view.parameters['openerPanel'];
		
		// add Vacant Only restriction to the dataSource
		if(valueExistsNotEmpty(this.view.parameters['vacantOnly'])) {
			this.ds_abEditMovesSelectValueDrawing_rmHighlight.addParameter('vacantOnly', this.view.parameters['vacantOnly']);
		}
		
        this.blId = this.openerPanel.getFieldValue('mo.to_bl_id');
        this.flId = this.openerPanel.getFieldValue('mo.to_fl_id');
        this.rmId = this.openerPanel.getFieldValue('mo.to_rm_id');
        var restriction = new Ab.view.Restriction();
        restriction.addClause('rm.bl_id', this.blId, '=');
        restriction.addClause('rm.fl_id', this.flId, '=');
		var ds = View.dataSources.get("ds_ab-edit-moves-select-value-drawing_dwgname");
		var rmRecord = ds.getRecord(restriction);
		this.dwgname = rmRecord.getValue("rm.dwgname");
		
    	var parameters = new Ab.view.ConfigObject();
    	parameters['highlightParameters'] = [{'view_file':"ab-mo-edit-moves-select-value-drawing.axvw", 'hs_ds': "ds_abEditMovesSelectValueDrawing_rmHighlight", 'label_ds':'ds_ab-edit-moves-select-value-drawing_rmLabel'}];
    	parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
		parameters['drawingName'] = this.dwgname;
		parameters['bordersHighlightSelector'] = false; 
		parameters['highlightFilterSelector'] = false;
		parameters['divId'] = "svgDiv";
		parameters['showTooltip'] = 'true';
		parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'AssetLocator': {divId: "svgDiv"},
				'AssetTooltip': {handlers: [{assetType: 'rm', datasource: 'ds_ab-edit-moves-select-value-drawing_rmLabel', fields: 'rm.rm_id;rm.rm_std;rm.area;rm.rm_type;rm.dp_id'}]}};
		parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abEditMoveSelectValueDrawing_DrawingPanel", parameters);
		
		this.addParameterConfig(parameters);
		this.svgControl.load(parameters);	
		this.highlightAsset();
    },
    
    highlightAsset: function(){
		if (this.rmId) {
			
			// you can choose how to highlight the asset  	  
	    	var opts = { cssClass: 'zoomed-asset-red-bordered',		// use the cssClass property to specify a css class
	    				 removeStyle: true							// use the removeStyle property to specify whether or not the fill should be removed (for example  cssClass: 'zoomed-asset-bordered'  and removeStyle: false
				   	   };
	    	
	    	// don't zoom into a room if the zoomFactor is 0
	    	if (this.zoom === false) {
	    		opts.zoomFactor = 0;
	    	}

			this.svgControl.getAddOn('AssetLocator').findAssets([this.blId+';'+this.flId+';'+this.rmId], opts);
		}
    },
    
    onClickAsset: function(params, drawingController){
    	var arrayRoomIDs = params['assetId'].split(";");
		controller.openerPanel.setFieldValue('mo.to_rm_id' ,arrayRoomIDs[2]);
        View.closeThisDialog();
    },
    
    /**
     * Set hs_ds to drawing's highlight data source configuration.
     */
    addParameterConfig: function(parameters){
    	var ds = View.dataSources.get(parameters['highlightParameters'][0].hs_ds);
    	parameters['highlightParameters'][0].hs_param = toJSON(ds.parameters);
    }
});

