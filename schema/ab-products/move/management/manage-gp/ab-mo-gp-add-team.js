var addTeamMoveToProjectController = View.createController('addTeamMoveToProjectController', {

	projectId : '', 
	
	dateStartReq : '',
	
	dateToPerform : '',
	
	afterViewLoad: function(){
		
		this.projectId = View.parameters['projectId'];
		
		this.dateStartReq = View.parameters['dateStartReq'];
		
		this.dateToPerform = View.parameters['dateToPerform'];
	},
	
	afterInitialDataFetch : function(){
		this.refreshPanels();
	},
	
    /**
     * select one team property
     */
     teamPropertiesGrid_onAddSelected: function() {

    	var rows = View.panels.get('teamPropertiesGrid').getSelectedRows();
    	
    	for (var i = 0; i < rows.length; i++) {
    		 var teamId = rows[i]["team_properties.team_id"];
    		 var record = new Ab.data.Record({
                 'mo.to_team_id': teamId,
                 'mo.project_id': this.projectId,
                 'mo.date_start_req': this.dateStartReq,
                 'mo.date_to_perform': this.dateToPerform,
                 'mo.mo_type': 'Team'
             }, true);
    		 try {
    			 
                 this.mo_ds.saveRecord(record);
             } 
             catch (e) {
            	 Workflow.handleError(e);
             }
    	}
    	
    	var refreshTeamsTab = View.parameters['callBack'];
    	refreshTeamsTab(this.projectId);
    	
    	View.closeThisDialog();
    	
    	
    	
	},
	
    /**
     * Filters the employees.
     */
	selectTeamFilterOptions_onFilterTeams: function() {
		this.refreshPanels();
    },
    
    /**
     * refresh select team tab
     * 
     */
    refreshPanels: function() {
		var filterValues = this.getFieldsValueOfFilter();
		
		var parameters = this.composeParameters(filterValues);
		
		this.displaySelectTeamGrid(parameters);
		
	},

    /**
     * combine filter field value into an object
     */
    getFieldsValueOfFilter: function() {
    	var filterValues = {};
    	
    	filterValues.teamId = this.selectTeamFilterOptions.getFieldValue("team_properties.team_id");
    	filterValues.teamName = this.selectTeamFilterOptions.getFieldValue("team_properties.team_name");
    	filterValues.teamFunction = this.selectTeamFilterOptions.getFieldValue("team_properties.team_function");
    	filterValues.teamCategory = this.selectTeamFilterOptions.getFieldValue("team_properties.team_category");
    	filterValues.emId = this.selectTeamFilterOptions.getFieldValue("team.em_id");
    	filterValues.isTeamActive = $('teamActiveOnly').checked;
    	
    	return filterValues;
    },

    /**
     * get the sql statement according to the filter fields
     * @param filterValues
     * @return parameters
     */
	composeParameters: function(filterValues) {

		var parameters = {};
		
    	if (valueExistsNotEmpty(filterValues.teamId))
    	{
			var valObj = getFieldValueForQuery(filterValues.teamId);
			parameters.teamId = valObj.value;
			parameters.teamIdOp = valObj.operator;
    	}
    	if (valueExistsNotEmpty(filterValues.teamName))
    	{
			var valObj = getFieldValueForQuery(filterValues.teamName);
			parameters.teamName = valObj.value;
			parameters.teamNameOp = valObj.operator;
    	}
    	if (valueExistsNotEmpty(filterValues.teamFunction))
    	{
			var valObj = getFieldValueForQuery(filterValues.teamFunction);
			parameters.teamFunction = valObj.value;
			parameters.teamFunctionOp = valObj.operator;
    	}
    	if (valueExistsNotEmpty(filterValues.teamCategory))
    	{
			var valObj = getFieldValueForQuery(filterValues.teamCategory);
			parameters.teamCategory = valObj.value;
			parameters.teamCategoryOp = valObj.operator;
    	}
    	if (valueExistsNotEmpty(filterValues.emId))
    	{
    		var valObj = getFieldValueForQuery(filterValues.emId);
			parameters.emId = valObj.value;
			parameters.emIdOp = valObj.operator;
    	}

		parameters.hasTeamId = valueExistsNotEmpty(filterValues.teamId);
		parameters.hasTeamName = valueExistsNotEmpty(filterValues.teamName);
		parameters.hasTeamFunction = valueExistsNotEmpty(filterValues.teamFunction);
		parameters.hasTeamCategory = valueExistsNotEmpty(filterValues.teamCategory);
		parameters.hasEmId = valueExistsNotEmpty(filterValues.emId);
		parameters.hasTeamActive = filterValues.isTeamActive;
		
		return parameters;
	},
	
    /**
     * show team properties
     */
	displaySelectTeamGrid: function(parameters){
		
		this.teamPropertiesGrid.addParameter('teamIdOp', parameters.teamIdOp);
		this.teamPropertiesGrid.addParameter('teamId', parameters.teamId);
		this.teamPropertiesGrid.addParameter('teamNameOp', parameters.teamNameOp);
		this.teamPropertiesGrid.addParameter('teamName', parameters.teamName);
		this.teamPropertiesGrid.addParameter('teamFunctionOp', parameters.teamFunctionOp);
		this.teamPropertiesGrid.addParameter('teamFunction', parameters.teamFunction);
		this.teamPropertiesGrid.addParameter('teamCategoryOp', parameters.teamCategoryOp);
		this.teamPropertiesGrid.addParameter('teamCategory', parameters.teamCategory);
		this.teamPropertiesGrid.addParameter('emIdOp', parameters.emIdOp);
		this.teamPropertiesGrid.addParameter('emId', parameters.emId);
		this.teamPropertiesGrid.addParameter('hasTeamId', parameters.hasTeamId);
		this.teamPropertiesGrid.addParameter('hasTeamName', parameters.hasTeamName);
		this.teamPropertiesGrid.addParameter('hasTeamFunction', parameters.hasTeamFunction);
		this.teamPropertiesGrid.addParameter('hasTeamCategory', parameters.hasTeamCategory);
		this.teamPropertiesGrid.addParameter('hasEmId', parameters.hasEmId);
		this.teamPropertiesGrid.addParameter('hasTeamActive', parameters.hasTeamActive);
		this.teamPropertiesGrid.addParameter('projectId', this.projectId);
		this.teamPropertiesGrid.refresh();
    },
	
    /**
     * Clears the team properties filter.
     */
    selectTeamFilterOptions_onClearFields: function() {
    	this.selectTeamFilterOptions.clear();
        this.refreshPanels();
    }
    
});
