var abMoGroupEditCompleteTeamsController = View.createController('abMoGroupEditCompleteTeamsController', {
	
	projectId: "",
	
	grid_abMoGroupListCompleteTeam_mo_onEditTeam: function(row){
		var teamId = row.record['mo.to_team_id'];
		
		//use date_end_req as date_end
		var dateStartReq = row.record['mo.date_start_req.raw'];
		var dateToPerform = row.record['mo.date_to_perform.raw'];
		
		var projectId = row.record['mo.project_id'];;
		
		Ab.view.View.openDialog('ab-mo-console-team-edit.axvw', null, false, 
			{title : teamId, teamId:teamId, dateStartReq:dateStartReq, asOfDate:dateToPerform, projectId:projectId, maximize:true, callBack: abMoGroupEditCompleteTeamsController.refreshTeamsTab
		});  
	},
	
	refreshTeamsTab: function(projectId){
		
		if(!valueExistsNotEmpty(projectId)){
			var projectId = View.panels.items[0].getFieldValue("project.project_id");
		}
		
		var teamTab = View.panels.items[6];
		
		if(teamTab){
			
			var restriction = new Ab.view.Restriction();
		    restriction.addClause('mo.project_id', projectId);
		    
		    teamTab.restriction = restriction;
		    teamTab.refresh();
		}
	}


});

/**
 * afterRefresh event for "grid_abMoGroupListCompleteAsset_mo" panel - add to tab title the count of grid rows
 * @param {Object} gridPanel
 */
function grid_abMoGroupListCompleteTeam_mo_afterRefresh(gridPanel){
	var tabsPanel = (View.panels.items[View.panels.items.length-1].tabs)?View.panels.items[View.panels.items.length-1]:View.getOpenerView().panels.items[View.getOpenerView().panels.items.length-1];
	if (tabsPanel.findTab("abMoGroupEditComplete_team") ){
		var tab = tabsPanel.findTab("abMoGroupEditComplete_team");
		tab.setTitle(tab.getTitle().split(" (")[0] + " (" + gridPanel.gridRows.length + ")");
	}else if (tabsPanel.findTab("abMoGroupEditReviewData_team") ){
		var tab = tabsPanel.findTab("abMoGroupEditReviewData_team");
		tab.setTitle(tab.getTitle().split(" (")[0] + " (" + gridPanel.gridRows.length + ")");
	}else if (tabsPanel.findTab("abMoGroupEditReviewVoice_team") ){
		var tab = tabsPanel.findTab("abMoGroupEditReviewVoice_team");
		tab.setTitle(tab.getTitle().split(" (")[0] + " (" + gridPanel.gridRows.length + ")");
	}
}

