var abMoGroupEditTeamsController = View.createController('abMoGroupEditTeamsController', {
	
	projectId: "",
	
	teammo_list_onEditTeam: function(row){
		var teamId = row.record['mo.to_team_id'];
		
		//use date_end_req as date_end  
		var dateStartReq = row.record['mo.date_start_req.raw']?row.record['mo.date_start_req.raw']:row.record['mo.date_start_req'];
		var dateToPerform = row.record['mo.date_to_perform.raw']?row.record['mo.date_to_perform.raw']:row.record['mo.date_to_perform'];
		
		var projectId = row.record['mo.project_id'];;
		
		Ab.view.View.openDialog('ab-mo-console-team-edit.axvw', null, false, 
			{title : teamId, teamId:teamId, dateStartReq:dateStartReq, asOfDate:dateToPerform, projectId:projectId, maximize:true, callBack: abMoGroupEditTeamsController.refreshTeamsTab
		});  
	},

	teammo_list_onAddNew: function(row){
		
		this.projectId = View.panels.items[0].getFieldValue("project.project_id");
		
		var dateStartReq = View.panels.items[0].getFieldValue("project.date_start");
		
		var dateToPerform = View.panels.items[0].getFieldValue("project.date_end");
		
		
		Ab.view.View.openDialog('ab-mo-gp-add-team.axvw', null, false, 
			{title : getMessage("add_team_move_title"), projectId: this.projectId, dateStartReq:dateStartReq, dateToPerform:dateToPerform, callBack: abMoGroupEditTeamsController.refreshTeamsTab
		});  
		
	},
	
	teammo_list_onDelete: function(){
		
		this.projectId = View.panels.items[0].getFieldValue("project.project_id");
		
		var rows = this.teammo_list.getSelectedRows();
		var teams = [];
		
		for (var i = 0; i < rows.length; i++) {
   		 	teams.push(rows[i]["mo.to_team_id"]);
		}
		
		if(rows.length==0){
			return;
		}
		
		try {
			var result = Workflow.callMethod("AbMoveManagement-MoveService-deleteSelectedTeams", teams.toString(), this.projectId);
			
			if (result.code == 'executed') {
	            this.refreshTeamsTab();
	        }
		}
		catch (e) {
	    	Workflow.handleError(e);
		}	
	},
	
	refreshTeamsTab: function(projectId){
		
		if(!valueExistsNotEmpty(projectId)){
			var projectId = View.panels.items[0].getFieldValue("project.project_id");
		}
		
		var teamTab = View.panels.items[6];
		
		if(teamTab){
			
			var restriction = new Ab.view.Restriction();
		    restriction.addClause('mo.project_id', projectId);
		    
		    teamTab.restriction = restriction;
		    teamTab.refresh();
		}
	}


});
/**
 * afterRefresh event for "teammo_list" panel - add to tab title the count of grid rows
 * @param {Object} gridPanel
 */
function teammo_list_afterRefresh(gridPanel){
	var tabsPanel = (View.panels.items[View.panels.items.length-1].tabs)?View.panels.items[View.panels.items.length-1]:View.getOpenerView().panels.items[View.getOpenerView().panels.items.length-1];
	if (tabsPanel.findTab("team") ){
		var tab = tabsPanel.findTab("team");
		tab.setTitle(tab.getTitle().split(" (")[0] + " (" + gridPanel.gridRows.length + ")");
	}else if (tabsPanel.findTab("abMoGroupEditIssue_team") ){
		var tab = tabsPanel.findTab("abMoGroupEditIssue_team");
		tab.setTitle(tab.getTitle().split(" (")[0] + " (" + gridPanel.gridRows.length + ")");
	}else if (tabsPanel.findTab("abMoGroupEditReview_team") ){
		var tab = tabsPanel.findTab("abMoGroupEditReview_team");
		tab.setTitle(tab.getTitle().split(" (")[0] + " (" + gridPanel.gridRows.length + ")");
	}else if (tabsPanel.findTab("abMoGroupEditRoute_team") ){
		var tab = tabsPanel.findTab("abMoGroupEditRoute_team");
		tab.setTitle(tab.getTitle().split(" (")[0] + " (" + gridPanel.gridRows.length + ")");
	}
}
