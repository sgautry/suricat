var checkSchemaController = View.createController('checkSchemaController', {
	
	checkSchema:function(){
		var result = this.ds_checkSchema.getRecords();
		if(result.length==0){
			View.alert(getMessage('field_not_exist'));
			return false;
		}
		return true;
	}

});

