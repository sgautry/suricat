var fromToDrawingController = View.createController('fromToDrawingController', {
	
	svgControl: null,
	
	projectId: null,
	filterOption: 'emOnly',

	afterInitialDataFetch: function() {
        this.legendGrid.afterCreateCellContent = setLegendLabel;
		this.initialSvgControl();
		this.initialFloorsGrid();
		this.initialDropDownFilter();
	},

	initialSvgControl: function() {
    	// define parameters to be used by server-side job
    	var parameters = new Ab.view.ConfigObject();
		parameters['divId'] = "svgDiv";
    	parameters['allowMultipleDrawings'] = 'true';
    	parameters['orderByColumn'] = 'true';
		parameters['multipleSelectionEnabled'] = 'true';
		parameters['topLayers'] = 'rm';
		parameters['highlightParameters'] = [{'view_file':"ab-mo-gp-from-to-drawing-ds.axvw", 'hs_ds': "highlightByFromToDs", 'label_ds':'labelByFromToDs','label_ht': 0.6}];
		parameters['highlightParameters'][0].hs_param = toJSON({'projectId':this.projectId, 'filterOption':'emOnly'});
 
		parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
											'DatasourceSelector': {panelId: "svg_ctrls"},
											'InfoWindow': {width: '400px', position: 'bottom'}
									 };
		parameters['legendPanel'] = 'legendGrid';

    	// only construct the drawing control once
    	this.svgControl = new Drawing.DrawingControl("svgDiv", "svg_ctrls", parameters);	
	},

	initialFloorsGrid: function() {
		this.fromToFloorsGrid.addParameter("projectId", this.projectId);
		this.fromToFloorsGrid.addParameter("filterOption", this.filterOption);
		this.fromToFloorsGrid.refresh();

    	// overrides Grid.onChangeMultipleSelection to load a drawing
    	this.fromToFloorsGrid.addEventListener('onMultipleSelectionChange', function(row) {
    		
    		// get selected data from tree
    		var bl_id = row["fl.bl_id"];
    		var fl_id = row["fl.fl_id"];
    		var drawingName = row["fl.dwgname"];
    		
    		var checked = document.getElementById("fromToFloorsGrid_row" + row.index + "_multipleSelectionColumn").checked;
    		    		
    		// if previously selected bl and fl are the same as current selection, no need to load the drawing again
    		if (checked) {
    			// load relevant svg
    			fromToDrawingController.loadSvg(bl_id, fl_id, drawingName);
    		} else {
    			fromToDrawingController.unloadSvg(bl_id, fl_id, drawingName);
    		}
	    });
    	
		this.fromToFloorsGrid.multipleSelectionEnabled = false;
	},

	initialDropDownFilter: function() {
		// incude border highlight option to the drawing panel
		var panelTitleNode = document.getElementById('svg_ctrls_title').parentNode.parentNode;
		
		var cell = Ext.DomHelper.append(panelTitleNode, {
			tag : 'td',
			id : 'filterByTitle'
		});

		var tn = Ext.DomHelper.append(cell, '<p>' + getMessage('filterByTitle')+":" + '</p>', true);
		Ext.DomHelper.applyStyles(tn, "x-btn-text");

		cell = Ext.DomHelper.append(panelTitleNode, {
			tag : 'td',
			id : 'filterByOptions_td'
		});

		var options = Ext.DomHelper.append(cell, {
			tag : 'select',
			id : 'filterByOptions'
		}, true);
		
		options.dom.options[0] = new Option(getMessage('emOnly'), 'emOnly');
		options.dom.options[1] = new Option(getMessage('teamOnly'), 'teamOnly');
		options.dom.options[2] = new Option(getMessage('both'), 'both');
        
        options.on('change', this.selectFilterBy, this, {
			delay : 100,
			single : false
		});		
	},

	/**
	 * select color by option.
	 */
	selectFilterBy : function(e, option) {
		this.filterOption = option.value;

		this.svgControl.config.highlightParameters[0].hs_param = toJSON(
			{'projectId':this.projectId, 'filterOption':option.value});

		//reload the drawings
		var control = this.svgControl.drawingController.getControl();
		for(var drawingName in control.drawings){
			var parameters = this.svgControl.config;
			parameters['pkeyValues'] = control.drawings[drawingName].config['pkeyValues'];
			parameters['drawingName'] = drawingName;
			//parameters['callFromDatasourceSelector'] = 'true';	
			control.reload(parameters);
		}

		this.fromToFloorsGrid.addParameter("filterOption", this.filterOption);
		this.fromToFloorsGrid.refresh();
	},
		
	loadSvg: function(bl_id, fl_id, drawingName) {

		var parameters = new Ab.view.ConfigObject();
		parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id};
    	parameters['drawingName'] = drawingName;
		
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters); 	
    },
     
    unloadSvg: function(bl_id, fl_id, drawingName){
    	var parameters = {};
		parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id};
    	parameters['drawingName'] = drawingName;
    	this.svgControl.unload(parameters);    	
    },
		
	/**
     * Print the drawing Pdf report.
     */
    onExport: function() {

		var controller = this;
    	var pdfParameters = {};
    	pdfParameters.legends = {};
    
		//get highlight and label dataSource ids from drawing control
    	var viewName = 'ab-mo-gp-from-to-drawing-ds.axvw';
    	var highligtDS = controller.svgControl.config.highlightParameters[0]["hs_ds"];
    	var labelDS = controller.svgControl.config.highlightParameters[0]["label_ds"];
    	var legendDs =  highligtDS+ '_legend';

		//pass drawingName to skip core's expensive process to get it
		//pdfParameters.drawingName = selectedFloor.dwgName;
		//dataSources defined in a separate axvw which is shared with drawing control axvw
		pdfParameters.dataSources = {viewName:viewName, required:highligtDS + ';' + labelDS};			
		pdfParameters.highlightDataSource = 'rm:'+highligtDS;
		pdfParameters.labelsDataSource = 'rm:'+labelDS;
		
		//add legend dataSource to required list
		pdfParameters.dataSources.required = pdfParameters.dataSources.required +  ';' + legendDs;			
		//required legend panel
		pdfParameters.legends.required = 'panel_'+legendDs;
			
		View.closeDialog();

		pdfParameters['projectId'] = this.projectId;
		pdfParameters['filterOption'] = this.filterOption;
	
		var passedRestrictions = {	};
		var floorRestriction = new Ab.view.Restriction();
        this.fromToFloorsGrid.gridRows.each(function(row) {
			floorRestriction.addClause("rm.bl_id", row.getRecord().getValue('fl.bl_id'), "=", ")OR(");
			floorRestriction.addClause("rm.fl_id", row.getRecord().getValue('fl.fl_id'));
			floorRestriction.addClause("rm.dwgname", row.getRecord().getValue('fl.dwgname') );
        });
		passedRestrictions[highligtDS] =  floorRestriction;
		passedRestrictions[labelDS] =  floorRestriction;

		if(valueExistsNotEmpty(pdfParameters.highlightDataSource)){
			View.openPaginatedReportDialog("ab-mo-gp-from-to-drawing-export.axvw", passedRestrictions, pdfParameters);
		}
	}
});

/**
 * Set legend text according the legend level value.
 * @param {Object} row
 * @param {Object} column
 * @param {Object} cellElement
 */
function setLegendLabel(row, column, cellElement) {
    var value = row[column.id];
    if (column.id == 'legend.value' && value != '') {
        var text = value;
        switch (value) {
        	case '0':
        		text = getMessage('fromTo');
        		break;
            case '1':
                text = getMessage('to');
                break;
            case '2':
                text = getMessage('from');
                break;
        }
        var contentElement = cellElement.childNodes[0];
        if (contentElement)
         contentElement.nodeValue = text;
    }
}
