
/**
 * This controller is only used for ab-rr-rm-arrange-excluded.axvw.
 *  
 */
var abRrRmResNotAllowedController = View.createController("abRrRmResNotAllowedController",{
	
	afterInitialDataFetch: function() {
		var parentForm = View.getOpenerView().panels.get('rm_arrange_form');
		
		var excludedResources = parentForm.getFieldValue('rm_arrange.res_stds_not_allowed');
		var arrExcludedRes = [];
		
		if (!valueExists(excludedResources)) {
			excludedResources = "";		
		}
		arrExcludedRes = excludedResources.split(",");
		
		// Move throught the grid rows to disable the configuration of the parent window
	   	var grid = View.panels.get('excluded_config');   
	   	var gridRows = grid.gridRows;
		
		for (var i = 0; i < gridRows.length; i++) {
			var row = gridRows.items[i];
			var gridConfigId = row.getFieldValue('resource_std.resource_std');
			
			var selectionCheckbox = row.dom.firstChild.firstChild;
			
			for (var j=0; j < arrExcludedRes.length; j++) {
				if (arrExcludedRes[j] == "'"+gridConfigId+"'") {
					row.select(true);
				}
			}
	   	} 
	}
});

/**
 * This function is called when the button Save is clicked.
 */
function onSave() {
    var grid = View.getControl('','excluded_config');
	var dataRows = grid.getSelectedGridRows();
	var selectedResNotAllowed = '';
	
	//Move throught the grid rows to get selected configurations and put them between ' ' and separate then with commas
	for (var i = 0; i < dataRows.length; i++) {
        var dataRow = dataRows[i];
		var selectionCheckbox = dataRow.dom.firstChild.firstChild;
	    
		if (selectionCheckbox.disabled) {
			continue;
		}
		
		if (selectedResNotAllowed != '') {
			selectedResNotAllowed = selectedResNotAllowed + ',';
		}
		
		selectedResNotAllowed = selectedResNotAllowed + "'" + dataRow.getFieldValue('resource_std.resource_std') + "'";
    }
	//Assign the selected excluded configurations to the parent window
	var parentForm = View.getOpenerView().panels.get('rm_arrange_form');
	parentForm.setFieldValue('rm_arrange.res_stds_not_allowed', selectedResNotAllowed);
	//Close the window
	View.getOpenerView().closeDialog();
}

