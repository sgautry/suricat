var reservationDetailsController = View.createController("reservationDetailsController", {
	blId: null,
    flId: null,
    rmId: null,
    svgControl: null,
	
	afterInitialDataFetch: function() {
        var reservationId = this.detailsPanel.getFieldValue("reserve.res_id");
		try {
			var result = Workflow.callMethod('AbWorkplaceReservations-roomReservationService-getAttendeesResponseStatus', 
					parseInt(reservationId));
			if (result.code = 'executed') {
				var responses = result.data;
				for (var i = 0; i < responses.length; ++i) {
					// add a row
				 	var record = new Ab.data.Record(
						{
							'name' : responses[i]['name'],
							'email' : responses[i]['email'],
							'response' : getMessage(responses[i]['response'])
						}, true);
				 	var newRow = this.attendeesPanel.recordToRow(record)
				 	this.attendeesPanel.addRow(newRow);	 
				 	this.attendeesPanel.hasNoRecords = false;
				}
				this.attendeesPanel.build();
			}
		} catch (e) {
			Workflow.handleError(e);
		}
		
		if (this.roomPanel != undefined) {
			this.loadFloorPlan();
		}
        
		// KB 3051635 Add Location information in Resolve Conflicts details
		if (this.conflictedRoomPanel != undefined){
			var restriction = new Ab.view.Restriction();
			
			var firstReservationId = this.detailsPanel.getFieldValue("reserve.first_reserv");
			restriction.addClause("reserve_rm.res_id", firstReservationId);
	       
	        this.conflictedRoomPanel.refresh(restriction);
		}
	},  
    
    loadFloorPlan: function() {	
    	this.blId = this.roomPanel.getFieldValue('reserve_rm.bl_id');
        this.flId = this.roomPanel.getFieldValue('reserve_rm.fl_id');
        this.rmId = this.roomPanel.getFieldValue('reserve_rm.rm_id');
        
        // define parameters to be used by server-side job
    	var parameters = new Ab.view.ConfigObject();
    	parameters['pkeyValues'] = {'bl_id': this.blId, 'fl_id': this.flId};
		
    	this.svgControl = new Ab.svg.DrawingControl("svgDiv", "floorPlanPanel", parameters);
    	// load svg into the div element
    	this.svgControl.load("svgDiv", parameters);
    	// resize specified DOM element whenever the panel size changes
    	this.floorPlanPanel.setContentPanel(Ext.get('svgDiv'));
    	
    	this.floorPlanPanel.setTitle(this.blId + '-' + this.flId + '-' + this.rmId);
		
		var highLightId = this.blId + ';' + this.flId +';' + this.rmId;
		var opts = { cssClass: 'zoomed-asset-yellow-bordered' };
		this.svgControl.findAssets([highLightId], opts);
    }

});