/**
 * @author Guo
 * changed 2009-05-12 for KB3022990
 */
var controller = View.createController('abRrRmArrangeDetails_Controller', {
    blId: null,
    flId: null,
    rmId: null,
    svgControl: null,
    
    afterInitialDataFetch: function(){
		var restriction = new Ab.view.Restriction();
		
		for (var name in window.location.parameters) {
		    var value = window.location.parameters[name];
		    restriction.addClause(name, value);            
		}
       
        this.abRrRmArrangeDetails_RmArrange_Form.show(true);
        this.abRrRmArrangeDetails_RmArrange_Form.refresh(restriction);
        this.abRrRmArrangeDetails_FixResources_Grid.show(true);
        this.abRrRmArrangeDetails_FixResources_Grid.refresh(restriction);
    },
    
    abRrRmArrangeDetails_RmArrange_Form_afterRefresh: function(){
        this.loadFloorPlan();
    },
    
    loadFloorPlan: function() {	
    	this.blId = this.abRrRmArrangeDetails_RmArrange_Form.getFieldValue('rm_arrange.bl_id');
        this.flId = this.abRrRmArrangeDetails_RmArrange_Form.getFieldValue('rm_arrange.fl_id');
        this.rmId = this.abRrRmArrangeDetails_RmArrange_Form.getFieldValue('rm_arrange.rm_id');
        
        // define parameters to be used by server-side job
    	var parameters = new Ab.view.ConfigObject();
    	parameters['pkeyValues'] = {'bl_id': this.blId, 'fl_id': this.flId};
		
    	this.svgControl = new Ab.svg.DrawingControl("svgDiv", "floorPlanPanel", parameters);
    	// load svg into the div element
    	this.svgControl.load("svgDiv", parameters);
    	// resize specified DOM element whenever the panel size changes
    	this.floorPlanPanel.setContentPanel(Ext.get('svgDiv'));
    	
    	var regionPanel = this.floorPlanPanel.getLayoutRegionPanel();
    	var drawingPanel = this.floorPlanPanel;   	
    	regionPanel.addListener('resize', function(){
    		var height = drawingPanel.determineHeight();  		
	   		var drawingDiv = Ext.get('svgDiv');
            drawingDiv.setHeight(height);          
            drawingDiv.parent().setHeight(height); 
		});
    	
    	this.floorPlanPanel.setTitle(this.blId + '-' + this.flId + '-' + this.rmId);
		
		var highLightId = this.blId + ';' + this.flId +';' + this.rmId;
		var opts = { cssClass: 'zoomed-asset-yellow-bordered' };
		this.svgControl.findAssets([highLightId], opts);
    },
    
    abRrRmArrangeDetails_RmArrange_Form_onClose: function(){
        View.getOpenerView().closeDialog();
    }
});
