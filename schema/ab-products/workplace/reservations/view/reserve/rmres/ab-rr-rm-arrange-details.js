var controller = View.createController('abRrRoomArrangeDetails_Controller', {
    blId: null,
    flId: null,
    rmId: null,
    svgControl: null,
    
	afterInitialDataFetch: function() {
		this.loadFloorPlan();
	},
    
    loadFloorPlan: function() {	
    	this.blId = this.abRrRoomArrangeDetails_RoomArrange_Form.getFieldValue('rm_arrange.bl_id');
        this.flId = this.abRrRoomArrangeDetails_RoomArrange_Form.getFieldValue('rm_arrange.fl_id');
        this.rmId = this.abRrRoomArrangeDetails_RoomArrange_Form.getFieldValue('rm_arrange.rm_id');
        
        // define parameters to be used by server-side job
    	var parameters = new Ab.view.ConfigObject();
    	parameters['pkeyValues'] = {'bl_id': this.blId, 'fl_id': this.flId};
		
    	this.svgControl = new Ab.svg.DrawingControl("svgDiv", "floorPlanPanel", parameters);
    	// load svg into the div element
    	this.svgControl.load("svgDiv", parameters);
    	// resize specified DOM element whenever the panel size changes
    	this.floorPlanPanel.setContentPanel(Ext.get('svgDiv'));
    	
    	var regionPanel = this.floorPlanPanel.getLayoutRegionPanel();
    	var drawingPanel = this.floorPlanPanel;   	
    	regionPanel.addListener('resize', function(){
    		var height = drawingPanel.determineHeight();  		
	   		var drawingDiv = Ext.get('svgDiv');
            drawingDiv.setHeight(height);          
            drawingDiv.parent().setHeight(height); 
		});
	
    	this.floorPlanPanel.setTitle(this.blId + '-' + this.flId + '-' + this.rmId);
		
		var highLightId = this.blId + ';' + this.flId +';' + this.rmId;
		var opts = { cssClass: 'zoomed-asset-yellow-bordered' };
		this.svgControl.findAssets([highLightId], opts);
    }

});
