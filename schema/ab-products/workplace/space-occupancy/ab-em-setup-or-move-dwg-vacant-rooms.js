/**
 * @author Guo
 * changed 2009-05-12 for KB3021459
 */
var controller = View.createController('abEmSetupOrMoveDwgVacantRooms_Controller', {
    blId: null,
    flId: null,
    rmId: null,
	dwgname:null,

	svgControl: null,
	parameters: null,
	zoom: false, 
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
    //----------------event handle--------------------
    
    afterViewLoad: function(){
        this.abEmSetupOrMoveDwgVacantRooms_DrawingPanel.actions.get('close').setTitle(getMessage("close"));
        var drawingRestriction = opener.drawingRestriction;
        this.blId = drawingRestriction.blId;
        this.flId = drawingRestriction.flId;
        this.rmId = drawingRestriction.rmId;
        var restriction = new Ab.view.Restriction();
        restriction.addClause('rm.bl_id', this.blId, '=');
        restriction.addClause('rm.fl_id', this.flId, '=');
		var ds = View.dataSources.get("ds_ab-em-setup-or-move-dwg-vacant-rooms_dwgname");
		var rmRecord = ds.getRecord(restriction);
		this.dwgname = rmRecord.getValue("rm.dwgname");
        
    	//initialize svg control
        this.parameters = new Ab.view.ConfigObject();
        this.parameters['highlightParameters'] = [{'view_file':"ab-em-setup-or-move-dwg-vacant-rooms.axvw", 'hs_ds': "ds_ab-em-setup-or-move-dwg-vacant-rooms_rmHighlight", 'label_ds':'ds_ab-em-setup-or-move-dwg-vacant-rooms_rmLabel'}];
        this.parameters['bordersHighlightSelector'] = 'true'; 
        this.parameters['highlightFilterSelector'] = 'true';
        this.parameters['allowMultipleDrawings'] = 'false';
        this.parameters['divId'] = "svgDiv";
        this.parameters['orderByColumn'] = true;
        this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
        this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
        		'DatasourceSelector': {panelId: "abEmSetupOrMoveDwgVacantRooms_DrawingPanel"},
        		'AssetLocator': {divId: "svgDiv"},
        		'AssetTooltip': {handlers: [{assetType: 'rm', datasource: 'ds_ab-em-setup-or-move-dwg-vacant-rooms_rmLabel', fields: 'rm.rm_id;rm.rm_std;rm.area;rm.type_info;rm.dp_info'}]},
        		'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
        this.parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abEmSetupOrMoveDwgVacantRooms_DrawingPanel", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('instructionText'));
		
		this.loadSvg(this.blId, this.flId, this.dwgname);
    },
    
    abEmSetupOrMoveDwgVacantRooms_DrawingPanel_onClose: function(){
        window.close();
    },
    
	loadSvg: function(bl_id, fl_id, drawingName) {
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgname = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgname;
 
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
    	
   	},
   	
    onClickAsset: function(params, drawingController){
    	var arrayRoomIDs = params['assetId'].split(";");
        var rmFieldElement = opener.document.getElementById('mo.to_rm_id');
        if (!rmFieldElement) {
            rmFieldElement = opener.document.getElementsByName('mo.to_rm_id')[0];
        }
        rmFieldElement.value = arrayRoomIDs[2];
        window.close();
        rmFieldElement.focus();
    }
});
