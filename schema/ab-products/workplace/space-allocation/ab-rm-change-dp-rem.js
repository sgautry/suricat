var controller = View.createController('roomSelectorController', {
	
	user_dp_found : false,
	user_em_id : null,
	user_dv_id : null,
	user_dp_id : null,

	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
	afterViewLoad: function() {	
		this.rmChangeDpRem_grid.addEventListener('onMultipleSelectionChange', function(row) {
			var checked = document.getElementById("rmChangeDpRem_grid_row" + row.index + "_multipleSelectionColumn").checked;
    		
    		if (checked) {
    			// load relevant svg
    			controller.loadSvg(row["fl.bl_id"], row["fl.fl_id"], row["fl.dwgname"]);
    		} else {
    			controller.unloadSvg(row["fl.bl_id"], row["fl.fl_id"], row["fl.dwgname"]);
    			
    			var rows = View.panels.get('rmChangeDpRem_grid').getSelectedRows();
    			var length = rows.length;
    			if(length==0){
    				View.panels.get('rmChangeDpRem_report').show(false);
    			}
    			
    		}
	    });
    	this.user_em_id = Ab.view.View.user.employee.id;
    	this.user_dv_id = Ab.view.View.user.employee.organization.divisionId;
    	this.user_dp_id = Ab.view.View.user.employee.organization.departmentId; 			
    	if (this.user_em_id != '' && this.user_dv_id != '' && this.user_dp_id != '')
	    	this.user_dp_found = true;
    	
        //initialize svg control
		this.parameters = new Ab.view.ConfigObject();
		this.parameters['highlightParameters'] = [{'view_file':"ab-rm-change-dp-rem.axvw", 'hs_ds': "rmChangeDpRem_highlightDs", 'label_ds':'labelNamesDs'}];
		this.parameters['bordersHighlightSelector'] = 'true';
		this.parameters['borderSize'] = 18;
		this.parameters['legendPanel'] = "rmChangeDpRem_legendGrid";
		this.parameters['highlightFilterSelector'] = 'true';
		this.parameters['allowMultipleDrawings'] = 'true';
		this.parameters['multipleSelectionEnabled'] = 'false';
		this.parameters['divId'] = "svgDiv";
		this.parameters['orderByColumn'] = true;
		this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
		this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'DatasourceSelector': {panelId: "rmChangeDpRem_cadPanel"},
				'AssetLocator': {divId: "svgDiv"},
				'AssetTooltip': {handlers: [{assetType: 'rm', datasource: 'labelNamesDs', fields: 'rm.rm_id'}]},
				'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
		this.parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "rmChangeDpRem_cadPanel", this.parameters);
		
//		var selectController = this.svgControl.drawingController.getController("SelectController");
//    	//enable diagonal border selection 
//    	selectController.setDiagonalSelectionPattern(true);
//    	//set diagonal selection border size (default is 20)
//    	selectController.setDiagonalSelectionBorderWidth(20); 
//    	//enable Multiple Selection
//    	selectController.setMultipleSelection(false);
		
    }, 
    
	loadSvg: function(bl_id, fl_id, drawingName) {
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgName = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;

        // load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
   	},
   	
   	unloadSvg: function(bl_id, fl_id, drawingName){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id':fl_id};
        parameters['drawingName'] = drawingName;
    	this.svgControl.unload(parameters);

   	},
   	
   	onClickAsset: function(params, drawingController) {
   		var arrayRoomIDs = params['assetId'].split(";");
   	    
   		var report = View.panels.get('rmChangeDpRem_report');
   		var r = new Ab.view.Restriction();
   		r.addClause("rm.bl_id", arrayRoomIDs[0], "=", true);
   		r.addClause("rm.fl_id", arrayRoomIDs[1], "=", true);
   		r.addClause("rm.rm_id", arrayRoomIDs[2], "=", true);
   		report.refresh(r);
   		var dv_id = report.getFieldValue('rm.dv_id');
   		var dp_id = report.getFieldValue('rm.dp_id');
   		var releaseAction = report.actions.get('rmChangeDpRem_release');
   		var ctrl = View.controllers.get('roomSelectorController');
   		if (ctrl.user_dp_found) {
   			if (ctrl.user_dv_id == dv_id && ctrl.user_dp_id == dp_id) {
   				releaseAction.enable(true);
   				report.refresh();
   			} else { 
   				releaseAction.enable(false);
   				report.refresh();
   			}
   		}
   		else {
   			View.showMessage('message',getMessage('user_info_not_found'));
   			releaseAction.enable(false);
   			var claimAction = report.actions.get('rmChangeDpRem_claim');
   			claimAction.enable(false);
   			report.refresh();
   		}
   		
   		// toggle the room if clicked
    	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId'], 'yellow');
   	}
  
});

function claimSpace() {	
	var ds = View.dataSources.get('rmChangeDpRem_ds1'); 
	var report = View.panels.get('rmChangeDpRem_report');
	var record = report.getRecord();
	var ctrl = View.controllers.get('roomSelectorController');
	record.setValue('rm.dv_id', ctrl.user_dv_id);
	record.setValue('rm.dp_id', ctrl.user_dp_id);
	ds.saveRecord(record);
	report.refresh();
	var cadPanel = View.panels.get('rmChangeDpRem_cadPanel');
	cadPanel.refresh();
	reloadDrawings();
}

function releaseSpace() {
	var ds = View.dataSources.get('rmChangeDpRem_ds1'); 
	var report = View.panels.get('rmChangeDpRem_report');
	var record = report.getRecord();
	record.setValue('rm.dv_id', '');
	record.setValue('rm.dp_id', '');
	ds.saveRecord(record);
	report.refresh();
	var cadPanel = View.panels.get('rmChangeDpRem_cadPanel');
	cadPanel.refresh();
	reloadDrawings();
}

function reloadDrawings(){
	var rows = View.panels.get('rmChangeDpRem_grid').getSelectedRows();
	var length = rows.length;
	//clear all drawings
	for(var i=0;i<length;i++){
		var bl_id = rows[i]['fl.bl_id'];
		var fl_id = rows[i]['fl.fl_id'];
		var dwgname = rows[i]['fl.dwgname'];
		controller.unloadSvg(bl_id, fl_id, dwgname);
	}
	//reload all selected drawings
	for(var i=0;i<length;i++){
		var bl_id = rows[i]['fl.bl_id'];
		var fl_id = rows[i]['fl.fl_id'];
		var dwgname = rows[i]['fl.dwgname'];
		controller.loadSvg(bl_id, fl_id, dwgname);
	}	
}