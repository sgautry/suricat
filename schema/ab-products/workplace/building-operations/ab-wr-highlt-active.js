
var controller = View.createController('hlRmByStdController', {

    //----------------event handle--------------------
    afterViewLoad: function(){
    	this.svg_ctrls.setTitle(getMessage('drawingPanelTitle1'));
    },
    
    svgControl: null,
	
	loadSvg: function(bl_id, fl_id, drawingName) {
    	
    	// define parameters to be used by server-side job
    	var parameters = new Ab.view.ConfigObject();
    	parameters['highlightParameters'] = [{'view_file':"ab-wr-highlt-active.axvw", 'hs_ds': "ds_ab-sp-hl-rm-by-rmstd_drawing_rmHighlight", 'label_ds':'ds_ab-sp-hl-rm-by-rmstd_drawing_rmLabel'}];
    	parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id};
    	parameters['divId'] = "svgDiv";
		parameters['drawingName'] = drawingName;
		parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : onClickDrawingHandler}];
		parameters['addOnsConfig'] = { 'DatasourceSelector': {panelId: "svg_ctrls"}};
		
		this.svgControl = new Drawing.DrawingControl("svgDiv", "svg_ctrls", parameters);	
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);	
    	
    }
    
});



function showDrawing(){

    var buildingId = '';
    var floorId = '';
    var buildingDrawing = View.panels.get('ab-wr-highlt-active-select-floor');
    buildingId = buildingDrawing.rows[buildingDrawing.selectedRowIndex]["rm.bl_id"];
    floorId = buildingDrawing.rows[buildingDrawing.selectedRowIndex]["rm.fl_id"];
    var dwgname = buildingDrawing.rows[buildingDrawing.selectedRowIndex]["rm.dwgname"];
    
    var title = String.format(getMessage('drawingPanelTitle1') + "  " + buildingId + "-" + floorId);
    
    var restriction = new Ab.view.Restriction();
    restriction.addClause('rm.bl_id', buildingId, '=');
    restriction.addClause('rm.fl_id', floorId, '=');
    
    displayFloor(buildingId,floorId,dwgname, title);
    View.panels.get('abWrHighltActive_rmGrid').refresh(restriction);
}

function displayFloor(selectedBl, selectedFl,selectedDwgName, title){
	controller.loadSvg(selectedBl, selectedFl,selectedDwgName);	
	controller.svg_ctrls.setTitle(title);
}


//onclick  event


function onClickDrawingHandler(assetIds){
	var pk = assetIds.assetId.split(';');
    var blid = pk[0];
    var flid = pk[1];
    var rmid = pk[2];
    var restriction = new Ab.view.Restriction();
    
    restriction.addClause('wr.bl_id', blid, '=');
    restriction.addClause('wr.fl_id', flid, '=');
    restriction.addClause('wr.rm_id', rmid, '=');
    
    var rmDetailPanel = View.panels.get('abWrHighltActive_rmDetailPanel');
    rmDetailPanel.refresh(restriction);
    rmDetailPanel.show(true);
    rmDetailPanel.showInWindow({
    	closeButton: true,
    	width: 1000,
        height: 500
    });
        
}



