<view version="2.0">
    <dataSource id="afmEnterpriseGraphicsDs">
        <table name="afm_enterprise_graphics"/>
        <field name="autonumbered_id"/>
    </dataSource>
    <dataSource id="eqSysInvMasterDs">
        <sql dialect="generic">
            SELECT auto_number,
                eq_id_master,
                eq_id_depend,
                sort_order,
                system_level,
                (CASE WHEN eq_system.system_name IS NOT NULL THEN eq_system.system_name WHEN eq.use1 IS NOT NULL THEN eq.use1 ELSE eq.eq_std END) ${sql.as} system_name,
                (CASE WHEN eq.bl_id IS NOT NULL AND eq.fl_id IS NOT NULL 
                    AND EXISTS(SELECT 1 FROM rm WHERE rm.bl_id=eq.bl_id AND rm.fl_id = eq.fl_id AND rm.dwgname IS NOT NULL) 
                 THEN 1 ELSE 0 END) ${sql.as} has_drawing,
                (${sql.getRestriction('enterpriseGraphicsRestriction', '0')}) ${sql.as} has_3dbin,
                 (CASE WHEN eq.bl_id IS NOT NULL AND eq.fl_id IS NOT NULL AND eq.rm_id IS NOT NULL 
                    AND EXISTS(SELECT 1 FROM docs_assigned WHERE docs_assigned.bl_id=eq.bl_id AND docs_assigned.fl_id = eq.fl_id AND docs_assigned.rm_id=eq.rm_id 
                        AND (docs_assigned.doc IS NOT NULL OR docs_assigned.url IS NOT NULL))
                 THEN 1 ELSE 0 END) ${sql.as} has_360doc,
                '{label_html}' ${sql.as} vf_concatenated_node
            FROM eq_system INNER JOIN eq ON eq_system.eq_id_depend=eq.eq_id
        </sql>
        <table name="eq_system"/>
        <field name="system_name" dataType="text"/>
        <field name="auto_number" dataType="number"/>
        <field name="eq_id_depend" dataType="text"/>
        <field name="system_level" dataType="text"/>
        <field name="eq_id_master" dataType="text"/>
        <field name="sort_order" dataType="text"/>
        <field name="has_drawing" dataType="number" decimals='0'/>
        <field name="has_3dbin" dataType="number" decimals='0'/>
        <field name="has_360doc" dataType="number" decimals='0'/>
        <field name="vf_concatenated_node" dataType="text"/>
        <sortField name="sort_order"/>
        <sortField name="eq_id_depend"/>
        <sortField name="system_name"/>
        <restriction type="sql" sql="eq_id_depend=eq_id_master"/>
        <!-- Filter restrictions -->
        <restriction type="sql" sql="${parameters['filterRestriction']}"/>
        <parameter name="filterRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="useEnterpriseGraphics" dataType="boolean" value="false"/>
        <restriction id="enterpriseGraphicsRestriction" enabled="useEnterpriseGraphics" type="sql"
            sql="(CASE WHEN eq.bl_id IS NOT NULL
	                AND EXISTS(SELECT 1 FROM afm_enterprise_graphics WHERE afm_enterprise_graphics.bl_id=eq.bl_id
	                        AND afm_enterprise_graphics.filename IS NOT NULL 
	                        AND afm_enterprise_graphics.file_type='3DBIN') 
	              THEN 1 ELSE 0 END)"/>
    </dataSource>

    <dataSource id="eqSysInvDependDs">
        <sql dialect="generic">
            SELECT auto_number,
                eq_id_master,
                eq_id_depend,
                sort_order,
                system_level,
                (CASE WHEN eq_system.system_name IS NOT NULL THEN eq_system.system_name WHEN eq.use1 IS NOT NULL THEN eq.use1 ELSE eq.eq_std END) ${sql.as} system_name,
                (CASE WHEN eq.bl_id IS NOT NULL AND eq.fl_id IS NOT NULL 
                    AND EXISTS(SELECT 1 FROM rm WHERE rm.bl_id=eq.bl_id AND rm.fl_id = eq.fl_id AND rm.dwgname IS NOT NULL) 
                 THEN 1 ELSE 0 END) ${sql.as} has_drawing,
                (${sql.getRestriction('enterpriseGraphicsRestriction', '0')}) ${sql.as} has_3dbin,
                 (CASE WHEN eq.bl_id IS NOT NULL AND eq.fl_id IS NOT NULL AND eq.rm_id IS NOT NULL 
                    AND EXISTS(SELECT 1 FROM docs_assigned WHERE docs_assigned.bl_id=eq.bl_id AND docs_assigned.fl_id = eq.fl_id AND docs_assigned.rm_id=eq.rm_id 
                        AND (docs_assigned.doc IS NOT NULL OR docs_assigned.url IS NOT NULL))
                 THEN 1 ELSE 0 END) ${sql.as} has_360doc,
                '{label_html}' ${sql.as} vf_concatenated_node
            FROM eq_system INNER JOIN eq ON eq_system.eq_id_depend=eq.eq_id
        </sql>
        <table name="eq_system"/>
        <field name="system_name" dataType="text"/>
        <field name="auto_number" dataType="number"/>
        <field name="eq_id_master" dataType="text"/>
        <field name="eq_id_depend" dataType="text"/>
        <field name="system_level" dataType="text"/>
        <field name="sort_order" dataType="text"/>
        <field name="has_drawing" dataType="number" decimals='0'/>
        <field name="has_3dbin" dataType="number" decimals='0'/>
        <field name="has_360doc" dataType="number" decimals='0'/>
        <field name="vf_concatenated_node" dataType="text"/>
        <sortField name="sort_order"/>
        <sortField name="eq_id_depend"/>
        <sortField name="system_name"/>
        <restriction type="sql" sql=" eq_system.eq_id_master != eq_id_depend "/>
        <!-- Filter restrictions -->
        <restriction type="sql" sql="${parameters['filterRestriction']}"/>
        <parameter name="filterRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="useEnterpriseGraphics" dataType="boolean" value="false"/>
        <restriction id="enterpriseGraphicsRestriction" enabled="useEnterpriseGraphics" type="sql"
            sql="(CASE WHEN eq.bl_id IS NOT NULL
		                AND EXISTS(SELECT 1 FROM afm_enterprise_graphics WHERE afm_enterprise_graphics.bl_id=eq.bl_id
		                    AND afm_enterprise_graphics.filename IS NOT NULL
		                    AND afm_enterprise_graphics.file_type='3DBIN')
		           THEN 1 ELSE 0 END)"/>
    </dataSource>

    <dataSource id="eqSystemDs">
        <table name="eq_system"/>
        <field name="auto_number"/>
        <field name="eq_id_master"/>
        <field name="eq_id_depend"/>
        <field name="criticality_mission"/>
        <field name="criticality_function"/>
        <field name="system_name"/>
        <field name="stakeholder_type"/>
        <field name="system_level"/>
        <field name="system_bl_id"/>
        <field name="sort_order"/>
    </dataSource>
    
    <dataSource id="equipmentDs">
        <table name="eq" role="main"/>
        <table name="eqstd" role="standard"/>
        <field table="eq" name="eq_id"/>
        <field table="eq" name="eq_std"/>
        <field table="eq" name="status"/>
        <field table="eq" name="description"/>
        <field table="eq" name="use1"/>
        <field table="eq" name="dp_id"/>
        <field table="eq" name="dv_id"/>
        <field table="eq" name="bl_id"/>
        <field table="eq" name="fl_id"/>
        <field table="eq" name="rm_id"/>
        <field table="eqstd" name="description"/>
    </dataSource>

    <message name="filterRestrictionSql" translatable="false">
        (EXISTS(SELECT 1 FROM (
            SELECT
                eq_system1.eq_id_master ${sql.as} level1,
                eq_system1.eq_id_depend ${sql.as} level2,
                eq_system2.eq_id_depend ${sql.as} level3,
                eq_system3.eq_id_depend ${sql.as} level4,
                eq_system4.eq_id_depend ${sql.as} level5,
                eq_system5.eq_id_depend ${sql.as} level6,
                eq_system6.eq_id_depend ${sql.as} level7,
                eq_system7.eq_id_depend ${sql.as} level8,
                eq_system8.eq_id_depend ${sql.as} level9,
                eq_system9.eq_id_depend ${sql.as} level10
                FROM eq_system ${sql.as} eq_system1
                LEFT JOIN eq ${sql.as} eq1 ON eq_system1.eq_id_master=eq1.eq_id
                LEFT JOIN eq_system ${sql.as} eq_system2 ON eq_system1.eq_id_depend=eq_system2.eq_id_master
                LEFT JOIN eq ${sql.as} eq2 ON eq_system1.eq_id_depend=eq2.eq_id
                LEFT JOIN eq_system ${sql.as} eq_system3 ON eq_system2.eq_id_depend=eq_system3.eq_id_master
                LEFT JOIN eq ${sql.as} eq3 ON eq_system2.eq_id_depend=eq3.eq_id
                LEFT JOIN eq_system ${sql.as} eq_system4 ON eq_system3.eq_id_depend=eq_system4.eq_id_master
                LEFT JOIN eq ${sql.as} eq4 ON eq_system3.eq_id_depend=eq4.eq_id
                LEFT JOIN eq_system ${sql.as} eq_system5 ON eq_system4.eq_id_depend=eq_system5.eq_id_master
                LEFT JOIN eq ${sql.as} eq5 ON eq_system4.eq_id_depend=eq5.eq_id
                LEFT JOIN eq_system ${sql.as} eq_system6 ON eq_system5.eq_id_depend=eq_system6.eq_id_master
                LEFT JOIN eq ${sql.as} eq6 ON eq_system5.eq_id_depend=eq6.eq_id
                LEFT JOIN eq_system ${sql.as} eq_system7 ON eq_system6.eq_id_depend=eq_system7.eq_id_master
                LEFT JOIN eq ${sql.as} eq7 ON eq_system6.eq_id_depend=eq7.eq_id
                LEFT JOIN eq_system ${sql.as} eq_system8 ON eq_system7.eq_id_depend=eq_system8.eq_id_master
                LEFT JOIN eq ${sql.as} eq8 ON eq_system7.eq_id_depend=eq8.eq_id
                LEFT JOIN eq_system ${sql.as} eq_system9 ON eq_system8.eq_id_depend=eq_system9.eq_id_master
                LEFT JOIN eq ${sql.as} eq9 ON eq_system8.eq_id_depend=eq9.eq_id
                LEFT JOIN eq_system ${sql.as} eq_system10 ON eq_system9.eq_id_depend=eq_system10.eq_id_master
                LEFT JOIN eq ${sql.as} eq10 ON eq_system9.eq_id_depend=eq10.eq_id
                WHERE eq_system1.eq_id_master = eq_system1.eq_id_depend
                AND (${filterParameters})
            )eq WHERE (${levelRestriction})
        ))
    </message>
</view>