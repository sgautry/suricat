View.createController('eqSys360ViewerController', {
    viewer: null,
    viewer360: null,
    maximizeAction: null,
    assetParameters: null,
    documents: null,
    afterInitialDataFetch: function () {
        var parameters = null;
        if (valueExists(this.view.parameters) && valueExists(this.view.parameters.assetParameters)) {
            parameters = this.view.parameters.assetParameters;
        } else if (valueExists(this.view.parentTab) && valueExists(this.view.parentTab.assetParameters)) {
            parameters = this.view.parentTab.parameters.assetParameters;
        } else if (valueExists(this.view.parentViewPanel) && valueExists(this.view.parentViewPanel.assetParameters)) {
            parameters = this.view.parentViewPanel.assetParameters;
        }
        if (valueExists(parameters)) {
            var initViewer = valueExists(parameters.initViewer) ? parameters.initViewer : true;
            if (initViewer) {
                this.initViewer();
            }
            this.load360Viewer(parameters);
        }
        this.initMaximizeAction();
    },
    initViewer: function () {
        if (!valueExists(this.viewer)) {
            var views = [];
            views.push({
                viewName: 'ab-eq-system-drawing.axvw',
                title: getMessage('viewerDrawingTitle')
            });
            if (isEnterpriseGraphicsSchemaDefined) {
                views.push({
                    viewName: 'ab-eq-system-bim-3d.axvw',
                    title: getMessage('viewer3dTitle')
                });
            }
            views.push({
                viewName: 'ab-eq-system-360.axvw',
                title: getMessage('viewer360Title'),
                selected: true
            });
            this.viewer = new ViewerControl('viewer360Panel', {
                title: getMessage('viewerTitle'),
                views: views
            });
        }
    },
    initMaximizeAction: function () {
        if (!valueExists(this.maximizeAction)) {
            this.maximizeAction = new MaximizeActionControl('viewer360Panel', {
                viewController: 'eqSys360ViewerController',
                viewName: 'ab-eq-system-360.axvw'
            });
        }
    },
    load360Viewer: function (parameters) {
        this.setViewerPanelTitle('');
        this.assetParameters = parameters;
        var blId = parameters.getConfigParameter('blId'),
            flId = parameters.getConfigParameter('flId'),
            rmId = parameters.getConfigParameter('rmId');
        var docAssignRecords = [];
        if (valueExistsNotEmpty(blId) && valueExistsNotEmpty(flId) && valueExistsNotEmpty(rmId)) {
            var restriction = new Ab.view.Restriction({
                'docs_assigned.bl_id': blId,
                'docs_assigned.fl_id': flId,
                'docs_assigned.rm_id': rmId
            });
            docAssignRecords = View.dataSources.get('abCommonDocsAssignedDs').getRecords(restriction);
        }

        if (!valueExists(this.viewer360)) {
            this.viewer360 = View.createControl({
                control: 'View360Control',
                panel: 'viewer360Panel',
                container: 'viewer360Div',
                docTableName: 'docs_assigned',
                docFieldName: 'doc',
                keys: ['doc_id', 'doc'],
                listeners: {
                    afterViewerLoad: this.afterViewerLoadListener
                }
            });
        }
        this.viewer360.setDocuments(docAssignRecords);
    },
    afterViewerLoadListener: function (viewer) {
        var controller = View.controllers.get('eqSys360ViewerController');
        var blId = controller.assetParameters.getConfigParameter('blId'),
            flId = controller.assetParameters.getConfigParameter('flId'),
            rmId = controller.assetParameters.getConfigParameter('rmId');
        var title = blId + ' ' + flId + ' ' + rmId;
        if (valueExists(viewer)) {
            controller.setViewerPanelTitle(title);
        } else {
            controller.setViewerPanelTitle(String.format(getMessage('no360itemFound'), title));
        }
    },
    setViewerPanelTitle: function (title) {
        this.viewer360Panel.setTitle(title);
    }
});