/**
 * Display options controller.
 * Three panel with options: panelA, panelB and panelC.
 * Each panel has several options checkboxes.
 */
View.createController('displayOptionsController', {
    // panel region
    panelId: null,
    // panel options
    options: null,
    // current selected tree node
    crtTreeNode: null,
    //event triggered on select panel options.
    events: {
        'click [type="checkbox"][name="displayOptionsPanel_panelA"]': function (e) {
            this.applyOptions('panelA', e.currentTarget);
        },
        'click [type="checkbox"][name="displayOptionsPanel_panelB"]': function (e) {
            this.applyOptions('panelB', e.currentTarget);
        },
        'click [type="checkbox"][name="displayOptionsPanel_panelC"]': function (e) {
            this.applyOptions('panelC', e.currentTarget);
        }
    },
    /**
     * PanelB always has an option checked because it's the center region.
     */
    applyOptions: function (location, option) {
        var form = document.forms['displayOptionsPanel_form'];
        var buttonGroupName = this.displayOptionsPanel.getFieldElementName(location);
        var buttonGroup = form[buttonGroupName];
        if (buttonGroup) {
            if (buttonGroup.length) {
                for ( var i = 0; i < buttonGroup.length; i++ ) {
                    if (buttonGroup[i].value === option.value) {
                        var checked = option.checked;
                        if ('panelB' === location) {
                            checked = true;
                        }
                        buttonGroup[i].checked = checked;
                    } else {
                        buttonGroup[i].checked = false;
                    }
                }
            } else {
                buttonGroup.checked = false;
            }
        }
    },
    displayOptionsPanel_onSave: function () {
        this.saveOptionValue('panelA');
        this.saveOptionValue('panelB');
        this.saveOptionValue('panelC');
        DisplayPanelConfiguration.updatePanelDisplayConfig(this.panelId, this.options);
        this.displayOptionsPanel.closeWindow();
    },
    saveOptionValue: function (location) {
        var checkBoxesValues = this.displayOptionsPanel.getCheckboxValues(location);
        var value = '';
        if (checkBoxesValues.length > 0) {
            value = checkBoxesValues[0];
        }
        this.options[location]['panelType'] = value;
    },
    showDisplayOptions: function (panelId) {
        this.displayOptionsPanel.showInWindow({
            width: 750,
            height: 250,
            x: 300,
            y: 300,
            closeButton: false
        });
        this.panelId = panelId;
        this.options = DisplayPanelConfiguration.getDisplayOptions(this.panelId);
        this.applyCheckboxOptions('panelA');
        this.applyCheckboxOptions('panelB');
        this.applyCheckboxOptions('panelC');
    },
    applyCheckboxOptions: function (location) {
        var option = this.options[location];
        var visible = option['visible'];
        var fieldSetId = this.displayOptionsPanel.fields.get(location).fieldSetId;
        var trFieldCell = jQuery('#displayOptionsPanel_' + fieldSetId + '_body').parent();
        if (!isEnterpriseGraphicsSchemaDefined) {
            trFieldCell.find(':checkbox[value="3d"]').parent().hide();
        }
        if (valueExists(visible) && visible == false) {
            trFieldCell.hide();
        } else {
            trFieldCell.show();
        }
        this.displayOptionsPanel.setFieldValue(location, option['panelType']);
    },
    getViewPanelId: function (viewName, bim3dPanelId) {
        var panels = ['panelA', 'panelB', 'panelC'];
        var availablePanel = null;
        for ( var i = 0; i < panels.length; i++ ) {
            var panelName = 'abEqSysInfo_' + panels[i];
            var frame = this[panelName] && this[panelName].getContentFrame();
            if (frame) {
                var originalView = frame.View.originalRequestURL.substring(View.originalRequestURL.lastIndexOf('/') + 1);
                if (viewName === originalView || ('ab-eq-system-drawing.axvw' === viewName && 'ab-blank.axvw' === originalView && !valueExistsNotEmpty(bim3dPanelId))) {
                    availablePanel = panels[i];
                    break;
                }
            }
        }
        return availablePanel;
    }
});
/**
 * Display panels options.
 */
function configDisplayPanel(panelId) {
    View.controllers.get('displayOptionsController').showDisplayOptions(panelId);
}
/**
 * Configuration panels. Contains methods to add, update and display panels.
 */
DisplayPanelConfiguration = new (Base.extend({
    panels: new Ext.util.MixedCollection(),
    openerPanelId: null,
    addPanelDisplayConfig: function (panelId, options) {
        this.panels.add(panelId, options);
    },
    updatePanelDisplayConfig: function (panelId, options) {
        this.panels.replace(panelId, options);
    },
    getDisplayOptions: function (panelId) {
        var panel = this.panels.get(panelId);
        if (panel) {
            return panel;
        } else {
            return null;
        }
    },
    getOptionLocation: function (panelId, location) {
        var options = this.getDisplayOptions(panelId);
        return options[location];
    },
    initPanel: function (panelId, location) {
        this.openerPanelId = panelId;
        return new DisplayPanel(location, this.getOptionLocation(panelId, location));
    },
    displayPanels: function (panelId, restriction, crtTreeNode) {
        var panelA = this.initPanel(panelId, 'panelA');
        panelA.display(restriction, crtTreeNode);
        var panelB = this.initPanel(panelId, 'panelB');
        panelB.display(restriction, crtTreeNode);
        var panelC = this.initPanel(panelId, 'panelC');
        panelC.display(restriction, crtTreeNode);
        this.manageLayout(valueExistsNotEmpty(panelA.type), valueExistsNotEmpty(panelB.type), valueExistsNotEmpty(panelC.type));
    },
    resetPanels: function (panelId) {
        if (panelId === this.getOpenerPanelId()) {
            var panelA = this.initPanel(panelId, 'panelA');
            panelA.reset();
            var panelB = this.initPanel(panelId, 'panelB');
            panelB.reset();
            var panelC = this.initPanel(panelId, 'panelC');
            panelC.reset();
        }
    },
    getOpenerPanelId: function () {
        return this.openerPanelId;
    },
    displayDrawingPanel: function (panelId, eqId, action) {
        var parameters = new Ab.view.ConfigObject();
        parameters['eqId'] = eqId;
        parameters['action'] = action;
        var record = View.dataSources.get('eqCommonDs').getRecord(new Ab.view.Restriction({'eq.eq_id': eqId}));
        var blId = record.getValue('eq.bl_id');
        var flId = record.getValue('eq.fl_id');
        if (valueExistsNotEmpty(blId) && valueExistsNotEmpty(flId)) {
            parameters['blId'] = blId;
            parameters['flId'] = flId;
            if ('locate' === action) {
                parameters['rmId'] = record.getValue('eq.rm_id');
            }
        }
        parameters['zoom'] = 'highlight' !== action;
        if (panelId) {
            var panel = View.panels.get('abEqSysInfo_' + panelId);
            panel.assetParameters = parameters;
            var controller = panel.getContentFrame().View.controllers.get('eqSysDrawingController');
            // if drawing was already opened, just refresh the view
            if (controller) {
                controller.refreshDrawingLocation();
            } else {
                panel.loadView('ab-eq-system-drawing.axvw', null, null);
            }
            var layout = View.getLayoutManager('detailsLayout');
            if ('panelA' === panelId && layout.isRegionCollapsed('west')) {
                layout.expandRegion('west');
            } else if ('panelC' === panelId && layout.isRegionCollapsed('east')) {
                layout.expandRegion('east');
            }
        } else {
            View.openDialog('ab-eq-system-drawing.axvw', false, null, {
                assetParameters: parameters
            });
        }
    },
    displayBim3dPanel: function (panelId, eqId, action) {
        var parameters = new Ab.view.ConfigObject();
        parameters['eqId'] = eqId;
        parameters['action'] = action;
        var record = View.dataSources.get('eqCommonDs').getRecord(new Ab.view.Restriction({'eq.eq_id': eqId}));
        var blId = record.getValue('eq.bl_id');
        var flId = record.getValue('eq.fl_id');
        if (valueExistsNotEmpty(blId) && valueExistsNotEmpty(flId)) {
            parameters['blId'] = blId;
            parameters['flId'] = flId;
        }
        parameters['zoom'] = 'highlight' !== action;
        if (panelId) {
            var panel = View.panels.get('abEqSysInfo_' + panelId);
            panel.assetParameters = parameters;
            var controller = panel.getContentFrame().View.controllers.get('eqSysBim3dController');
            // if bim 3d panel was already opened, just load the model
            if (controller) {
                controller.loadModel(parameters);
            } else {
                panel.loadView('ab-eq-system-bim-3d.axvw', null, null);
            }
            var layout = View.getLayoutManager('detailsLayout');
            if ('panelA' === panelId && layout.isRegionCollapsed('west')) {
                layout.expandRegion('west');
            } else if ('panelC' === panelId && layout.isRegionCollapsed('east')) {
                layout.expandRegion('east');
            }
        } else {
            View.openDialog('ab-eq-system-bim-3d.axvw', false, null, {
                assetParameters: parameters
            });
        }
    },
    displayProfilePanel: function (panelId, eqId) {
        if (panelId) {
            var parameters = new Ab.view.ConfigObject();
            parameters['eqId'] = eqId;
            parameters['restriction'] = new Ab.view.Restriction({'eq.eq_id': eqId});
            var panel = View.panels.get('abEqSysInfo_' + panelId);
            panel.assetParameters = parameters;
            var controller = panel.getContentFrame().View.controllers.get('eqSysProfileController');
            // if profile was already opened, just refresh the view
            if (controller) {
                controller.afterInitialDataFetch();
            } else {
                panel.loadView('ab-eq-system-profile.axvw', null, null);
            }
            var layout = View.getLayoutManager('detailsLayout');
            if ('panelA' === panelId && layout.isRegionCollapsed('west')) {
                layout.expandRegion('west');
            } else if ('panelC' === panelId && layout.isRegionCollapsed('east')) {
                layout.expandRegion('east');
            }
        }
    },
    manageLayout: function (showPanelA, showPanelB, showPanelC) {
        var layout = View.getLayoutManager('detailsLayout');
        if (layout.isRegionCollapsed('west') && showPanelA) {
            layout.expandRegion('west');
        } else {
            if (!showPanelA) {
                layout.collapseRegion('west');
            }
        }
        if (layout.isRegionCollapsed('east') && showPanelC) {
            layout.expandRegion('east');
        } else {
            if (!showPanelC) {
                layout.collapseRegion('east');
            }
        }
    }
}));
/**
 * Display panel configuration.
 */
DisplayPanel = Base.extend({
    type: null,
    visible: null,
    panelConfig: null,
    constructor: function (id, options) {
        this.id = id;
        this.type = options['panelType'];
        this.visible = options['visible'];
        this.panelConfig = this.getPanelConfig(this.type);
    },
    getPanelConfig: function (panelType) {
        var viewName = '',
            controller = '';
        switch (panelType) {
            case 'inventory':
                viewName = 'ab-eq-system-inventory-tree.axvw';
                controller = 'eqSysInvTreeController';
                break;
            case 'profile':
                viewName = 'ab-eq-system-profile.axvw';
                controller = 'eqSysProfileController';
                break;
            case 'dependent':
                viewName = 'ab-eq-system-dependent.axvw';
                controller = 'eqSysDependentController';
                break;
            case 'dependency':
                viewName = 'ab-eq-system-dependency.axvw';
                controller = 'eqSysDependencyController';
                break;
            case 'drawing':
                viewName = 'ab-eq-system-drawing.axvw';
                controller = 'eqSysDrawingController';
                break;
            case '3d':
                viewName = 'ab-eq-system-bim-3d.axvw';
                controller = 'eqSysBim3dController';
                break;
            case '360':
                viewName = 'ab-eq-system-360.axvw';
                controller = 'eqSys360ViewerController';
                break;
        }
        return {
            viewName: viewName,
            viewController: controller
        };
    },
    getPanel: function () {
        return View.panels.get('abEqSysInfo_' + this.id);
    },
    display: function (eqId, crtTreeNode) {
        var panel = this.getPanel();
        if (valueExists(panel)) {
            if (this.visible && valueExistsNotEmpty(this.panelConfig.viewName)) {
                panel.assetParameters = this.getPanelParameters(eqId, crtTreeNode);
                var controller = panel.getContentFrame().View.controllers.get(this.panelConfig.viewController);
                // if view was already opened, just refresh the view
                if (valueExistsNotEmpty(controller) && controller && (controller.id.toLowerCase().indexOf(this.type) > 0)) {
                    if ('drawing' === this.type) {
                        panel.getContentFrame().View.controllers.get('eqSysDrawingController').refreshDrawingLocation();
                    } else {
                        controller.afterInitialDataFetch();
                    }
                } else {
                    panel.loadView(this.panelConfig.viewName, null, null);
                }
            } else {
                panel.loadView('ab-blank.axvw', null, null);
            }
        }
    },
    reset: function () {
        var panel = this.getPanel();
        var frame = panel.getContentFrame();
        if (frame && frame.View) {
            var viewName = frame.View.originalRequestURL.substring(View.originalRequestURL.lastIndexOf('/') + 1);
            if ('ab-blank.axvw' !== viewName) {
                panel.loadView('ab-blank.axvw', null, null);
            }
        }
    },
    getPanelParameters: function (eqId, crtTreeNode) {
        var parameters = new Ab.view.ConfigObject();
        parameters['eqId'] = eqId;
        parameters['crtTreeNode'] = crtTreeNode;
        switch (this.type) {
            case 'drawing':
            case '3d':
            case '360':
                var record = View.dataSources.get('eqCommonDs').getRecord(new Ab.view.Restriction({'eq.eq_id': eqId}));
                var blId = record.getValue('eq.bl_id'),
                    flId = record.getValue('eq.fl_id');
                parameters['blId'] = blId;
                parameters['flId'] = flId;
                if (valueExistsNotEmpty(blId) && valueExistsNotEmpty(flId)) {
                    parameters['rmId'] = record.getValue('eq.rm_id');
                }
                break;
        }
        return parameters;
    }
});