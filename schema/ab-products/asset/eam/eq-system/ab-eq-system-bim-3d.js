/**
 * Bim 3D controller.
 */
View.createController('eqSysBim3dController', {
    // bim3d controller
    bim3d: null,
    // building models
    buildingModels: {},
    // revit category ids
    afm_bim_categories: null,
    // info window
    infoWindow: null,
    // selected building
    currentBuilding: null,
    // selected equipment
    selectedEquipment: null,
    // passed asset parameters
    assetParameters: null,
    // isolate type none|equipment
    isolateType: 'equipment',
    // highlight type  - default system
    highlightType: 'system',
    // show hierarchy trees - default false
    showHierarchyTrees: false,
    // fly mode dependents - default false
    flyModeDependents: false,
    // fly mode dependencies - default false
    flyModeDependencies: false,
    // current selected data-source
    currentLabelDS: null,
    // with first-time loading the navigator will determine the best layout and orientation for the drawing, and re-initialize cached variables like scenes.
    isFirst: true,
    afterViewLoad: function () {
        // init viewer
        this.initViewer();
        //create 3d navigator object
        this.bim3d = new Ab.bim3d.Navigator('bim3d', null);
        //create 3d navigator info window
        this.infoWindow = new Ab.bim3d.InfoWindow({
            dicId: 'bim3d',
            position: 'bottom'
        });
        // init labels selector
        Ab.bim3d.Utility.appendSelector(
            document.getElementById('viewerPanel_title'),
            'labels',
            getMessage('labelsSelectorTitle'),
            'DrawingControlLabels',
            null,
            this.onLabelsSelector.createDelegate(this)
        );
        this.afm_bim_categories = this.ds_afm_bim_categories.getRecords();
        //use category ids rather than its names
        this.bim3d.setCategoryFieldName('CategoryId');
        // init actions
        this.initActions();
        // init maximize action
        this.initMaximizeAction();
        // init hierarchy trees
        this.initHierarchyTrees();
        //object click event listener
        this.bim3d.setObjectClickEventListener(this.onObjectClickEvent.createDelegate(this));
        //disable object double-click event
        this.bim3d.setObjectDoubleClickable(false);
        // set categories
        //this.setCategories();
        //context menus
        this.bim3d.contextMenus.setFilter(['hideSelected', 'hideSimilar', 'showAll', 'clearSelection']);
        this.bim3d.contextMenus.setShowListener(this.onContextMenuActions.createDelegate(this));
        //html panel resize
        this.bim3dPanel.syncHeight = this.resizePanel.createDelegate(this);
    },
    /**
     * Load model by asset parameters.
     */
    afterInitialDataFetch: function () {
        var parameters = null;
        if (valueExists(this.view.parameters) && valueExists(this.view.parameters.assetParameters)) {
            parameters = this.view.parameters.assetParameters;
        } else if (valueExists(this.view.parentTab) && valueExists(this.view.parentTab.assetParameters)) {
            parameters = this.view.parentTab.parameters.assetParameters;
        } else if (valueExists(this.view.parentViewPanel) && valueExists(this.view.parentViewPanel.assetParameters)) {
            parameters = this.view.parentViewPanel.assetParameters;
        }
        if (valueExists(parameters)) {
            this.loadModel(parameters);
        }
    },
    /**
     * Load bim model.
     * @param {Ab.view.ConfigObject} parameters - asset parameters.
     */
    loadModel: function (parameters) {
        this.assetParameters = parameters;
        var blId = parameters.getConfigParameter('blId'),
            flId = parameters.getConfigParameter('flId'),
            eqId = parameters.getConfigParameter('eqId');
        var currentBuilding = this.getBuildingId();
        if (currentBuilding !== blId) {
            this.uninitializeDrawing();
        }
        this.buildingModels[blId] = this.getBuildingModels(blId);
        if (_.isEmpty(this.buildingModels[blId])) {
            this.showInvalid3DModelMessage();
            this.uninitializeDrawing();
            return;
        }
        if (currentBuilding !== blId) {
            this.setModelCategories(blId);
        }
        this.setNavigatorPanelTitle(blId + ' ' + flId);
        if (this.currentBuilding === blId) {
            this.showEquipment(eqId, true, true);
            var action = parameters.getConfigParameter('action');
            if (valueExists(action) && 'locate' !== action) {
                // apply action on selected equipment from the profile view
                this.applyAction(eqId, action);
            }
            return;
        }
        if (this.currentBuilding !== blId && valueExists(this.bim3d.categoriesPanel)) {
            this.bim3d.categoriesPanel.clearOpenedJsons();
        }
        this.currentBuilding = blId;
        this.loadBuilding(blId, flId);
    },
    /**
     * Uninitialize drawing for reuse.
     */
    uninitializeDrawing: function () {
        this.buildingModels = {};
        this.currentBuilding = null;
        this.isFirst = true;
        this.infoWindow.show(false);
        if (this.bim3d.categoriesPanel) {
            this.bim3d.categoriesPanel.uninitialize();
            this.bim3d.categoriesPanel = null;
        }
        this.bim3d.clearSelection();
        this.bim3d.clearHighlight(true);
        this.bim3d.clearLabels();
        this.bim3d.uninitialize();
    },
    /**
     * Load building.
     * @param {string} blId - Building id.
     * @param {string} flId - Floor id.
     */
    loadBuilding: function (blId, flId) {
        //load shell
        var shell = this.getModelByFileName(blId, 'shell');
        if (shell) {
            this.bim3d.load(shell.fileName, this.isFirst, this.afterLoadShell.createDelegate(this, [shell, blId, flId], true));
        } else {
            this.afterLoadShell([], shell, blId, flId);
        }
    },
    /**
     * Called after the building shell is loaded.
     * @param {Array} scenes - Model scenes.
     * @param {string} blId - Building id.
     * @param {Object} shell - Shell object.
     * @param {string} flId - Floor id.
     */
    afterLoadShell: function (scenes, shell, blId, flId) {
        //turn off site from categories panel
        var site = this.getModelByFileName(blId, 'site');
        if (site) {
            this.bim3d.categoriesPanel.changeLinkStatusById(site.superCategory, false);
        }
        if (shell) {
            this.isFirst = false;
            //turn off furniture from categories panel
            this.bim3d.categoriesPanel.changeLinkStatusById('Furniture', false);
            //if shell is turn off from categories panel, hide shell
            if (!this.bim3d.categoriesPanel.changeLinkStatusById(shell.superCategory)) {
                this.bim3d.showCategories(shell.listToArray(shell.categories), null, false, false, false, scenes);
            }
        }
        // turn off floor elements from categories panel
        this.bim3d.categoriesPanel.loopTargetLinks('Floor', function (alink, id) {
            alink.className = 'inactive';
        });
        //load core
        var core = this.getModelByFileName(blId, 'core');
        if (core) {
            this.bim3d.load(core.fileName, this.isFirst, this.afterLoadCore.createDelegate(this, [core, blId, flId], true));
        } else {
            this.afterLoadCore([], core, blId, flId);
        }
    },
    /**
     * Called after the building core is loaded.
     * @param {Array} scenes - Model scenes.
     * @param {Object} core - Core object.
     * @param {string} blId - Building id.
     * @param {string} flId - Floor id.
     */
    afterLoadCore: function (scenes, core, blId, flId) {
        if (core) {
            this.isFirst = false;
            //if core is not active, hide it.
            if (!this.bim3d.categoriesPanel.changeLinkStatusById(core.superCategory)) {
                this.bim3d.showCategories(core.listToArray(core.categories), null, false, false, false, scenes);
            }
            //turn off discipline from categories panel
            this.bim3d.categoriesPanel.loopTargetLinks('Disciplines', function (alink, id) {
                alink.className = 'inactive';
            });
        }
        // build levels
        var levels = this.getLevels(blId);
        if (levels.levels.length > 0) {
            this.bim3d.categoriesPanel.buildLevels(levels);
        }
        var hvac = this.getModelByFileName(blId, 'hvac');
        if (hvac) {
            this.loadHvac(hvac);
        } else {
            var eqId = this.assetParameters.getConfigParameter('eqId');
            this.showEquipment(eqId, true, true);
        }
    },
    /**
     * Load building level.
     * @param {Object} level - Building level.
     * @param {boolean} doLabeling - Apply labeling.
     */
    loadLevel: function (level, doLabeling) {
        if (level) {
            this.loadLevelModel(level, doLabeling);
        }
    },
    /**
     * Load drawing.
     * @param {Object} level - Level.
     * @param {boolean} doLabeling - Apply labeling.
     */
    loadLevelModel: function (level, doLabeling) {
        var link = document.getElementById(this.bim3d.categoriesPanel.PREFIX + level.modelLevel);
        if (link && link.className === 'inactive') {
            var json = level.json;
            if (typeof json !== 'undefined' && json !== '' && json !== 'null' && this.bim3d.categoriesPanel._openedJsons.indexOf(json) < 0) {
                this.bim3d.categoriesPanel._openedJsons.push(json);
                // load level model
                this.bim3d.load(level.fileName, this.isFirst, this.afterLoadLevel.createDelegate(this, [level, doLabeling], true));
            } else {
                this.clickLevel(link, this.afterClickLevel.createDelegate(this, [doLabeling]));
            }
        }
    },
    /**
     * On click level.
     * @param link Level link.
     * @param callback
     */
    clickLevel: function (link, callback) {
        link.click();
        if (callback) {
            callback();
        }
    },
    afterClickLevel: function (doLabeling) {
        if (doLabeling && this.currentLabelDS !== null && this.currentLabelDS !== 'None') {
            this.labeling.defer(500, this);
        }
    },
    afterLoadLevel: function (scenes, level, doLabeling) {
        this.isFirst = false;
        if (doLabeling && this.currentLabelDS !== null && this.currentLabelDS !== 'None') {
            this.labeling.defer(500, this);
        }
        var excludedCategories = [];
        if (this.bim3d.categoriesPanel.isReady()) {
            //all not active categories on categories panel
            excludedCategories = this.bim3d.categoriesPanel.getAllCategoryCodes(false);
        } else {
            excludedCategories = this.getCategoryIds(['Ceilings', 'Floors', 'Curtain Panels', 'Walls', 'Lighting Fixtures', 'Doors']);
        }
        this.bim3d.showLevel(level.modelLevel, excludedCategories, true, true);
        this.bim3d.categoriesPanel.changeLinkStatusById(level.modelLevel, true);

    },
    /**
     * Load building hvac.
     * @param {Object} hvac - hvac object.
     */
    loadHvac: function (hvac) {
        this.bim3d.load(hvac.fileName, this.isFirst, this.afterLoadHvac.createDelegate(this, [hvac], true));
    },
    /**
     * Called after the building hvac was loaded and selects the equipment.
     * @param {Array} scenes - Model scenes.
     * @param {Object} hvac - hvac object.
     */
    afterLoadHvac: function (scenes, hvac) {
        this.isFirst = false;
        if (!this.bim3d.categoriesPanel.changeLinkStatusById(hvac.superCategory)) {
            this.bim3d.showCategories(hvac.listToArray(hvac.categories), null, true, false, true, scenes);
        }
        this.bim3d.categoriesPanel.changeLinkStatusById(hvac.superCategory, true);
        var eqId = this.assetParameters.getConfigParameter('eqId');
        if (eqId) {
            this.showEquipment(eqId, true, true);
        }
    },
    /**
     * Init viewer.
     */
    initViewer: function () {
        new ViewerControl('viewerPanel', {
            title: getMessage('viewerTitle'),
            views: [{
                viewName: 'ab-eq-system-drawing.axvw',
                title: getMessage('viewerDrawingTitle')
            }, {
                viewName: 'ab-eq-system-bim-3d.axvw',
                title: getMessage('viewer3dTitle'),
                selected: true
            }, {
                viewName: 'ab-eq-system-360.axvw',
                title: getMessage('viewer360Title')
            }]
        });
    },
    /**
     * Init viewer actions.
     */
    initActions: function () {
        this.initHighlightActions();
        this.initViewerActions();
    },
    /**
     * Init highlight actions.
     */
    initHighlightActions: function () {
        this.addAction('viewerPanel', {
            id: 'highlightActions',
            tooltip: getMessage('actionsHighlightTooltip'),
            icon: '/schema/ab-core/graphics/icons/view/trace.png',
            type: 'menu'
        });
        var actions = [];
        actions.push({
            text: getMessage('actionHighlightNone'),
            checked: false,
            group: 'highlight',
            handler: this.onHighlightCurrentEquipment.createDelegate(this, ['none'])
        }, {
            text: getMessage('actionHighlightEquipmentSystem'),
            checked: true,
            group: 'highlight',
            handler: this.onHighlightCurrentEquipment.createDelegate(this, ['system'])
        }, {
            text: getMessage('actionHighlightDependents'),
            checked: false,
            group: 'highlight',
            handler: this.onHighlightCurrentEquipment.createDelegate(this, ['dependents'])
        }, {
            text: getMessage('actionHighlightDependencies'),
            checked: false,
            group: 'highlight',
            handler: this.onHighlightCurrentEquipment.createDelegate(this, ['dependencies'])
        });
        actions.push({type: 'separator'});
        actions.push({
            text: getMessage('actionHierarchyTrees'),
            checked: false,
            handler: this.onShowHierarchyTrees.createDelegate(this)
        });
        var menuAction = this.viewerPanel.actions.get('highlightActions');
        for ( var i = 0; i < actions.length; i++ ) {
            if (actions[i].type && 'separator' === actions[i].type) {
                menuAction.menu.addSeparator();
            } else {
                menuAction.menu.addMenuItem(actions[i]);
            }
        }
    },
    /**
     * Init viewer tools actions.
     */
    initViewerActions: function () {
        this.addAction('viewerPanel', {
            id: 'viewerActions',
            tooltip: getMessage('actionsTooltip'),
            icon: '/schema/ab-core/graphics/icons/view/gear.png',
            type: 'menu'
        });
        var actions = [];
        actions.push({
            text: getMessage('actionLocateEquipment'),
            handler: this.onLocateEquipment.createDelegate(this)
        }, {
            text: getMessage('actionHighlightRoomsServed'),
            handler: this.onHighlightRoomsServed.createDelegate(this, [], false)
        });
        actions.push({type: 'separator'});
        actions.push({
            text: getMessage('actionIsolateNone'),
            checked: false,
            group: 'isolate',
            handler: this.onIsolate.createDelegate(this, ['none'])
        }, {
            text: getMessage('actionIsolateEquipment'),
            checked: true,
            group: 'isolate',
            handler: this.onIsolate.createDelegate(this, ['equipment'])
        });
        actions.push({type: 'separator'});
        actions.push({
            text: getMessage('actionGeneratePPT'),
            handler: this.onGeneratePpt.createDelegate(this)
        });
        var menuAction = this.viewerPanel.actions.get('viewerActions');
        for ( var i = 0; i < actions.length; i++ ) {
            if (actions[i].type && 'separator' === actions[i].type) {
                menuAction.menu.addSeparator();
            } else {
                menuAction.menu.addMenuItem(actions[i]);
            }
        }
        // update viewer scroll
        this.viewerPanel.afterLayout();
    },
    /**
     * Init maximize action.
     */
    initMaximizeAction: function () {
        new MaximizeActionControl('viewerPanel', {
            viewController: 'eqSysBim3dController',
            viewName: 'ab-eq-system-bim-3d.axvw'
        });
    },
    /**
     * Init hierarchy trees panels.
     */
    initHierarchyTrees: function () {
        this.eqSysDependencyTreePanel.updateRestrictionForLevel = function (parentNode, level, restriction) {
            if (level > 0) {
                restriction.removeClause('eq_system.auto_number');
                restriction.addClause('eq_system.eq_id_master', parentNode.data['eq_system.eq_id_depend'], '=');
            }
        };
        this.eqSysDependentTreePanel.updateRestrictionForLevel = function (parentNode, level, restriction) {
            if (level > 0) {
                restriction.removeClause('eq_system.auto_number');
                restriction.addClause('eq_system.eq_id_master', parentNode.data['eq_system.eq_id_depend'], '=');
            }
        };
    },
    /**
     * Create context menu actions.
     * @param {Array} menus - List of existing menu actions.
     */
    onContextMenuActions: function (menus) {
        //add custom menus based on object selection
        var asset = this.bim3d.getSelectedObjectAssetType(),
            primaryKey = this.bim3d.getSelectedObjectPrimaryKey();
        var userData = {};
        if (this.bim3d.selectedObject) {
            userData = this.bim3d.selectedObject.parent.userData;
        }
        if (asset === 'eq') {
            menus.push({
                label: getMessage('actionHighlightRoomsServed'), id: 'menu-highlight-rooms-served', actions: [function () {
                    View.controllers.get('eqSysBim3dController').onHighlightRoomsServed(primaryKey);
                }]
            });
            menus.push(null);
            menus.push({
                label: getMessage('actionHighlightEquipmentSystem'), id: 'menu-highlight-system', actions: [function () {
                    View.controllers.get('eqSysBim3dController').onHighlight(primaryKey, false, 'system');
                }]
            });
            menus.push(null);
            menus.push({
                label: getMessage('actionHighlightDependents'), id: 'menu-highlight-dependents', actions: [function () {
                    View.controllers.get('eqSysBim3dController').onHighlight(primaryKey, false, 'dependents');
                }]
            });
            menus.push(null);
            menus.push({
                label: getMessage('actionHighlightDependencies'), id: 'menu-highlight-dependencies', actions: [function () {
                    View.controllers.get('eqSysBim3dController').onHighlight(primaryKey, false, 'dependencies');
                }]
            });
            menus.push(null);
            menus.push({
                label: getMessage('actionViewProperties'), id: 'menu-view-properties', actions: [function () {
                    View.controllers.get('eqSysBim3dController').showPropertiesPanel(primaryKey, userData);
                }]
            });
            var assignedDocRecord = this.getAssignedDocument(primaryKey);
            if (valueExists(assignedDocRecord)) {
                var blId = assignedDocRecord.getValue('eq.bl_id'),
                    flId = assignedDocRecord.getValue('eq.fl_id'),
                    rmId = assignedDocRecord.getValue('eq.rm_id');
                if (valueExistsNotEmpty(blId) && valueExistsNotEmpty(flId) && valueExistsNotEmpty(rmId)) {
                    menus.push(null);
                    menus.push({
                        label: getMessage('actionShow360View'), id: 'menu-show-360', actions: [function () {
                            View.controllers.get('eqSysBim3dController').show360View(blId, flId, rmId);
                        }]
                    });
                }
            }
        }
    },
    /**
     * Get assigned document.
     * @param {string} equipmentId - Equipment id.
     * @returns {*|Ab.data.Record} - Equipment record.
     */
    getAssignedDocument: function (equipmentId) {
        return View.dataSources.get('eqDocsAssigned_ds').getRecord(new Ab.view.Restriction({'eq.eq_id': equipmentId}));
    },
    /**
     * Called when an object is clicked.
     * @param {string} id - Object id.
     * @param {string} category - Category.
     * @param {Object} level - level.
     * @param {string} assetType - Selected asset type.
     * @param {string} pk - Selected asset primary key.
     * @param {Object} userData - Selected asset user data.
     */
    onObjectClickEvent: function (id, category, level, assetType, pk, userData) {
        if ('eq' === assetType) {
            this.showEquipment(pk, true);
        } else {
            this.bim3d.clearSelection();
            this.bim3d.clearHighlight(true);
        }
    },
    /**
     * Show equipment on the bim model.
     * @param {string} eqId - Equipment id.
     * @param {boolean} refreshHierarchyTrees - true to refresh hierarchy trees.
     * @param {boolean} [zoomIn] - true to zoom in to asset.
     * @param {string} [highlightType] - Highlight type.
     */
    showEquipment: function (eqId, refreshHierarchyTrees, zoomIn, highlightType) {
        this.bim3d.startSpinning();
        this.selectedEquipment = eqId;
        if (refreshHierarchyTrees) {
            this.refreshHierarchyTrees(true);
        }
        this.bim3d.clearSelection();
        this.bim3d.clearHighlight(true);
        //clear active levels
        var activeLevels = this.bim3d.categoriesPanel.getLevels(true);
        for ( var i = 0; i < activeLevels.length; i++ ) {
            this.bim3d.showLevel(activeLevels[i], [], false, true);
            this.bim3d.categoriesPanel.changeLinkStatusById(activeLevels[i], false);
        }
        var flId = this.assetParameters.getConfigParameter('flId');
        // load level for selected equipment if floor exists
        if (valueExistsNotEmpty(flId)) {
            var blId = this.getBuildingId();
            this.loadLevel(this.getLevelByFloor(blId, flId), true);
        }
        // highlight equipment system
        this.highlightEquipmentSystem(eqId, highlightType);
        //isolates highlighted objects
        if ('equipment' === this.isolateType && this.bim3d.isolatedMaterials.length === 0) {
            if (!_.isEmpty(this.bim3d.highlightedObjects)) {
                this.bim3d.isolateByMaterial('ab-highlight');
            }
        }
        var equipmentFound = this.selectEquipment(eqId, zoomIn);
        if (equipmentFound) {
            this.infoWindow.show(false);
            if ('equipment' === this.isolateType && this.bim3d.isolatedMaterials.length === 0) {
                if (_.isEmpty(this.bim3d.highlightedObjects)) {
                    this.bim3d.isolateByMaterial('ab-selection');
                }
            }
        } else {
            this.bim3d.home();
            this.infoWindow.setText(String.format(getMessage('equipmentNotFound'), eqId));
        }
        this.bim3d.stopSpinning();
    },
    /**
     * Selects equipment.
     * @param {string} eqId - Equipment id.
     * @param {boolean} [zoomIn] - true to zoom in to asset.
     * @returns {boolean} true if equipment was found.
     */
    selectEquipment: function (eqId, zoomIn) {
        var equipmentFound = false;
        var scope = this;
        this.bim3d.getObjectByPrimaryKey(eqId, function (object) {
            equipmentFound = true;
            scope.bim3d.select(object.id, EqSysColorConfig['selectAssetHighlightColor']);
            scope.bim3d.render();
            if (zoomIn) {
                scope.bim3d.home();
                //zoom in
                scope.bim3d.zoomIn(object);
            }
        });
        return equipmentFound;
    },
    /**
     * Called from profile view if applies profile view actions.
     * @param {string} eqId - Selected equipment id.
     * @param {string} action - Action to apply. highlight|dependents|dependencies
     */
    applyAction: function (eqId, action) {
        if ('highlight' === action) {
            this.onHighlightRoomsServed(eqId);
        } else {
            this.highlightEquipmentSystem(eqId, action);
        }
    },
    /**
     * Highlight current equipment.
     * @param {string} highlightType - Highlight type.
     */
    onHighlightCurrentEquipment: function (highlightType) {
        this.highlightType = highlightType;
        this.onHighlight(this.selectedEquipment, true, highlightType);
    },
    /**
     * On highlight action.
     * @param {string} eqId - Equipment id.
     * @param {boolean} refreshHierarchyTrees - true to refresh hierarchy trees.
     * @param {string} highlightType - Highlight type.
     */
    onHighlight: function (eqId, refreshHierarchyTrees, highlightType) {
        this.showEquipment(eqId, refreshHierarchyTrees, false, highlightType);
    },
    /**
     * Highlight equipment system.
     * @param {string} eqId - Equipment id.
     * @param {string} [highlightType] - Highlight type.
     */
    highlightEquipmentSystem: function (eqId, highlightType) {
        highlightType = highlightType || this.highlightType;
        var assetsToHighlight = {};
        if ('system' === highlightType) {
            this.highlightDepend(eqId, assetsToHighlight);
            this.highlightDependency(eqId, assetsToHighlight);
        } else if ('dependents' === highlightType) {
            this.highlightDepend(eqId, assetsToHighlight);
        } else if ('dependencies' === highlightType) {
            this.highlightDependency(eqId, assetsToHighlight);
        }
        this.bim3d.highlightByPrimaryKeys(assetsToHighlight, null, true);
        // show model levels where equipment exists
        var floors = this.getEquipmentSystemFloors(assetsToHighlight);
        var blId = this.getBuildingId();
        _.each(floors, function (floorId) {
            this.loadLevel(this.getLevelByFloor(blId, floorId), false);
        }, this);
    },
    /**
     * Highlight depend.
     * @param {string} eqId - Equipment id.
     * @param {Object} assets - Assets to highlight.
     */
    highlightDepend: function (eqId, assets) {
        var dataSource = View.dataSources.get('traceDepend_ds');
        dataSource.clearParameters();
        dataSource.addParameter('eqDependId', eqId);
        var records = dataSource.getRecords();
        for ( var i = 0; i < records.length; i++ ) {
            for ( var levelIndex = 1; levelIndex < 10; levelIndex++ ) {
                var assetFrom = records[i].getValue('eq_system.level' + levelIndex);
                var assetTo = records[i].getValue('eq_system.level' + (levelIndex + 1));
                if (valueExistsNotEmpty(assetFrom) && valueExistsNotEmpty(assetTo)) {
                    this.bim3d.getObjectByPrimaryKey(assetTo, this.addAsset.createDelegate(this, [assetTo, assets], true));
                }
            }
        }
        return assets;
    },
    /**
     * Highlight dependency.
     * @param {string} eqId - Equipment id.
     * @param {Object} assets - Assets to highlight.
     */
    highlightDependency: function (eqId, assets) {
        var dataSource = View.dataSources.get('traceDependency_ds');
        dataSource.clearParameters();
        dataSource.addParameter('eqDependId', eqId);
        var records = dataSource.getRecords();
        for ( var i = 0; i < records.length; i++ ) {
            var assetFound = false;
            for ( var levelIndex = 8; levelIndex > 0; levelIndex-- ) {
                var assetFrom = records[i].getValue('eq_system.level' + levelIndex);
                var assetTo = records[i].getValue('eq_system.level' + (levelIndex + 1));
                if (valueExistsNotEmpty(assetFrom) && valueExistsNotEmpty(assetTo)) {
                    this.bim3d.getObjectByPrimaryKey(assetFrom, this.addAsset.createDelegate(this, [assetFrom, assets], true));
                    if (!assetFound) {
                        this.highlightDepend(assetFrom, assets);
                        assetFound = true;
                    }
                }
            }
        }
        return assets;
    },
    /**
     * Add asset to list of assets to highlight.
     * @param object - 3d object
     * @param {string} assetId Asset id.
     * @param {Object} assets - Assets to highlight.
     */
    addAsset: function (object, assetId, assets) {
        assets[assetId] = object.material.color.getStyle();
    },
    /**
     * Get equipment record.
     * @param {string} assetId Asset id.
     */
    getEquipmentRecord: function (assetId) {
        return View.dataSources.get('eqFloorLevelDS').getRecord(new Ab.view.Restriction({'eq.eq_id': assetId}));
    },
    /**
     * Get unique list of equipment system floors to enable level models.
     * @param {Object} highlightAssets - Assets to highlight.
     * @returns {Array.<string>} unique list of floors
     */
    getEquipmentSystemFloors: function (highlightAssets) {
        var uniqueAssets = _.uniq(_.keys(highlightAssets));
        var floors = [];
        _.each(uniqueAssets, function (assetId) {
            var assetDetail = this.getEquipmentRecord(assetId);
            floors.push(assetDetail.getValue('eq.fl_id'));
        }, this);
        return _.uniq(floors);
    },
    /**
     * Get highlight color desaturated by amount.
     * @param {string} colorCode - Color.
     * @param {number} desaturate - Amount to desaturate.
     */
    getHighlightColor: function (colorCode, desaturate) {
        var color = new THREE.Color(colorCode);
        var hsl = color.getHSL();
        if (desaturate === 10) desaturate = 0;
        hsl.s = (10 - desaturate) / 10;
        return color.setHSL(hsl.h, hsl.s, hsl.l).getStyle();
    },
    /**
     * Viewer menu action to display hierarchy tree panels.
     * @param {Object} action - Checkbox object.
     */
    onShowHierarchyTrees: function (action) {
        this.showHierarchyTrees = !action.checked;
        var layout = View.getLayoutManager('nestedLayout');
        if (this.showHierarchyTrees) {
            layout.expandRegion('east');
        } else {
            layout.collapseRegion('east');
        }
        this.refreshHierarchyTrees();
    },
    /**
     * Viewer menu action to locate selected inventory tree equipment.
     */
    onLocateEquipment: function () {
        var eqId = this.assetParameters.getConfigParameter('eqId');
        this.selectedEquipment = null;
        this.showEquipment(eqId, true, true);
    },
    /**
     * Highlight rooms served for equipment.
     * @param {string} eqId - Equipment id.
     */
    onHighlightRoomsServed: function (eqId) {
        eqId = eqId || this.assetParameters.getConfigParameter('eqId');
        var dataSource = View.dataSources.get('servedRooms_ds');
        dataSource.addParameter('levelRestriction', "level1='" + eqId + "'");
        dataSource.addParameter('eqRestriction', "eq.eq_id='" + eqId + "'");
        var records = View.dataSources.get('servedRooms_ds').getRecords();
        for ( var i = 0; i < records.length; i++ ) {
            var blId = records[i].getValue('eq.bl_id'),
                flId = records[i].getValue('eq.fl_id'),
                rmId = records[i].getValue('eq.rm_id');
            if (valueExistsNotEmpty(rmId)) {
                var pkValue = blId + ';' + flId + ';' + rmId;
                this.bim3d.getObjectByPrimaryKey(pkValue, this.highlightRooms.createDelegate(this, [], true));
            }
        }
        this.bim3d.home();
        this.infoWindow.setText(String.format(getMessage('afterHighlightRooms'), eqId));
    },
    /**
     * Highlight rooms transparent
     * @param {Object} object - 3d object.
     */
    highlightRooms: function (object) {
        if (!this.bim3d.highlightedObjects[object.id]) {
            this.bim3d.highlightedObjects[object.id] = object.material;
        }
        object.material = new THREE.MeshPhongMaterial({
            name: 'ab-highlight',
            color: EqSysColorConfig['highlightRoomColor'],
            ambient: object.material.ambient,
            shading: object.material.shading,
            opacity: object.material.opacity,
            transparent: object.material.transparent
        });
    },
    /**
     * Isolate by type.
     * @param {string} type - none|equipment
     */
    onIsolate: function (type) {
        this.isolateType = type;
        if ('none' === type) {
            this.bim3d.clearIsolated();
        } else if ('equipment' === type) {
            if (_.isEmpty(this.bim3d.highlightedObjects)) {
                this.bim3d.isolateByMaterial('ab-selection');
            } else {
                this.bim3d.isolateByMaterial('ab-highlight');
            }
            this.selectEquipment(this.selectedEquipment);
        }
    },
    /**
     * Prints 3D image in PPT.
     */
    onGeneratePpt: function () {
        var slides = [];
        var image = this.bim3d.getImageBytes();
        var title = this.viewerPanel.getTitle() + ' ' + String.format(getMessage('equipmentSystemPPTTitle'), this.selectedEquipment);
        slides.push({'title': title, 'images': [image]});
        var jobId = Workflow.startJob('AbSystemAdministration-generatePaginatedReport-generatePpt', slides, {});
        View.openJobProgressBar(getMessage('actionGeneratePPTMessage'), jobId, null, function (status) {
            var url = status.jobFile.url;
            window.open(url);
        });
    },
    /**
     * Refresh hierarchy trees by selected equipment.
     * @param {boolean} [resetFlyMode] - true to reset fly mode.
     */
    refreshHierarchyTrees: function (resetFlyMode) {
        this.eqSysDependencyTreePanel.addParameter("lvlDependRestriction", this.selectedEquipment);
        this.eqSysDependencyTreePanel.refresh();
        this.eqSysDependencyTreePanel.expandAll();
        this.eqSysDependentTreePanel.addParameter("lvlMasterRestriction", "eq_system.eq_id_depend='" + this.selectedEquipment + "'");
        this.eqSysDependentTreePanel.refresh();
        this.eqSysDependentTreePanel.expandAll();
        if (resetFlyMode) {
            this.resetFlyMode();
        }
    },
    /**
     * Show selected asset user data.
     * @param {string} eqId - Equipment id.
     * @param {Object} userData - Selected asset user data.
     */
    showPropertiesPanel: function (eqId, userData) {
        var propertiesPanel = View.panels.get("propertiesPanel");
        if (!propertiesPanel.isShownInWindow()) {
            propertiesPanel.showInWindow({
                title: getMessage('propertiesPanelTitle'),
                width: 300
            });
        } else {
            propertiesPanel.window.show();
        }
        propertiesPanel.clear();
        propertiesPanel.clearGridRows();
        if (userData) {
            var record;
            for ( var name in userData ) {
                record = new Ab.data.Record({
                    'properties.name': name,
                    'properties.value': userData[name]
                });
                propertiesPanel.addGridRow(record);
            }
            record = new Ab.data.Record({
                'properties.name': 'id',
                'properties.value': eqId + ''
            });
            propertiesPanel.addGridRow(record);
            propertiesPanel.update();
        }
    },
    /**
     * Opens 360 viewer for selected equipment location.
     * @param {string} blId - Building id.
     * @param {string} flId - Floor id.
     * @param {string} rmId - Room id.
     */
    show360View: function (blId, flId, rmId) {
        var parameters = new Ab.view.ConfigObject();
        parameters['blId'] = blId;
        parameters['flId'] = flId;
        parameters['rmId'] = rmId;
        parameters['initViewer'] = false;
        var opener = View;
        if (!valueExists(View.getOpenerView().dialog)) {
            opener = View.getOpenerView();
        }
        opener.openDialog('ab-eq-system-360.axvw', null, false, {
            assetParameters: parameters,
            width: 1200,
            height: 800
        });
    },
    /**
     * Enable/disable fly mode.
     * @param {string} type - flyModeDependents|flyModeDependencies
     */
    enableFlyMode: function (type) {
        if ('flyModeDependents' === type) {
            if (this.flyModeDependents) {
                this.addFlyControl('eqSysDependentTreePanel');
            } else {
                this.removeFlyControl('eqSysDependentTreePanel');
            }
        }
        if ('flyModeDependencies' === type) {
            if (this.flyModeDependencies) {
                this.addFlyControl('eqSysDependencyTreePanel');
            } else {
                this.removeFlyControl('eqSysDependencyTreePanel');
            }
        }
    },
    /**
     * Add hover control for a specific tree panel.
     * @param {string} treePanel - Tree panel id.
     */
    addFlyControl: function (treePanel) {
        var scope = this;
        var addHoverControl = function (objTree, node) {
            jQuery('#' + node.labelElId).hover(function () {
                var data = node.data;
                var eqId = data['eq_system.eq_id_depend'];
                scope.bim3d.getObjectByPrimaryKey(eqId, function (object) {
                    scope.bim3d.select(object.id, EqSysColorConfig['selectAssetHighlightColor']);
                    scope.bim3d.render();
                    //scope.bim3d.home();
                    //scope.bim3d.zoomIn(object);
                });
            });
            if (node.hasChildren()) {
                for ( var i = 0; i < node.children.length; i++ ) {
                    addHoverControl(objTree, node.children[i]);
                }
            }
        };
        var objTree = View.panels.get(treePanel);
        for ( var nodeCounter = 0; nodeCounter < objTree._nodes.length; nodeCounter++ ) {
            addHoverControl(objTree, objTree._nodes[nodeCounter], nodeCounter);
        }
    },
    /**
     * Removes hover control for a specific tree panel.
     * @param {string} treePanel - Tree panel id.
     */
    removeFlyControl: function (treePanel) {
        var removeHoverControl = function (objTree, node) {
            jQuery('#' + node.labelElId).unbind('mouseenter mouseleave');
            if (node.hasChildren()) {
                for ( var i = 0; i < node.children.length; i++ ) {
                    removeHoverControl(objTree, node.children[i]);
                }
            }
        };
        var objTree = View.panels.get(treePanel);
        for ( var nodeCounter = 0; nodeCounter < objTree._nodes.length; nodeCounter++ ) {
            removeHoverControl(objTree, objTree._nodes[nodeCounter]);
        }
    },
    /**
     * Resets fly mode to false if already exists.
     */
    resetFlyMode: function () {
        this.flyModeDependents = false;
        this.flyModeDependencies = false;
        var actionMenu = View.panels.get('eqSysDependentTreePanel').actions.get('toolsDependentMenu');
        var flyModeDependent = actionMenu.menu.items.get('flyModeDependent');
        if (flyModeDependent.checked) {
            flyModeDependent.setChecked(false);
            this.enableFlyMode('flyModeDependents');
        }
        actionMenu = View.panels.get('eqSysDependencyTreePanel').actions.get('toolsDependencyMenu');
        var flyModeDependency = actionMenu.menu.items.get('flyModeDependency');
        if (flyModeDependency.checked) {
            flyModeDependency.setChecked(false);
            this.enableFlyMode('flyModeDependencies');
        }
    },
    /**
     * On resize bim 3d panel.
     */
    resizePanel: function () {
        this.bim3d.resize();
        this.bim3d.zoomIn();
    },
    /**
     * Set navigation panel title.
     * @param {string} title - Title to set.
     */
    setNavigatorPanelTitle: function (title) {
        this.viewerPanel.setTitle(title);
    },
    /**
     * Add action to a panel.
     * @param {string} panelId - Panel id.
     * @param {Object} config - Action configuration.
     */
    addAction: function (panelId, config) {
        var panel = View.panels.get(panelId);
        if (!valueExists(config)) {
            return;
        }
        if (panel.actions.get(config.id) !== undefined) {
            panel.actions.get(config.id).setTitle(config.text);
        } else {
            panel.actions.add(config.id, new Ab.view.Action(panel, config));
        }
    },
    /**
     * On label selection change.
     * @param {Object} e - Mouse element.
     * @param {Object} combo - Selector element.
     */
    onLabelsSelector: function (e, combo) {
        var tmp = combo.value;
        if (combo.id.lastIndexOf('labels') >= 0) {
            if (tmp === this.currentLabelsDS) {
                return;
            }
            this.currentLabelDS = tmp;
            if (this.selectedFloorRow === null) {
                return;
            }
            if (tmp === 'None') {
                this.bim3d.clearLabels();
                this.bim3d.render();
                return;
            }
            if (this.currentLabelDS !== null) {
                //clear before labeling
                this.bim3d.clearLabels();
            }
            //label objects
            this.labeling();
        }
    },
    /**
     *  Label 3D objects
     */
    labeling: function () {
        this.bim3d.startSpinning();
        var labelDSObj = View.dataSources.get(this.currentLabelDS);
        this.doLabeling(labelDSObj);
    },
    /**
     * Apply label by data-source.
     * @param {Object} labelDSObj - Label data-source.
     */
    doLabeling: function (labelDSObj) {
        var fieldNames = labelDSObj.fieldDefs.keys;
        for ( var fieldIndex = fieldNames.length - 1; fieldIndex >= 0; fieldIndex-- ) {
            if (fieldNames[fieldIndex].indexOf('.bl_id') > 0 || fieldNames[fieldIndex].indexOf('.fl_id') > 0) {
                fieldNames.splice(fieldIndex, 1);
            }
        }
        var records = labelDSObj.getRecords(new Ab.view.Restriction({'rm.bl_id': this.currentBuilding}));
        if (records.length === 0) {
            //clear labels
            this.bim3d.clearLabels();
            this.bim3d.render();
            //stop the spinning
            this.bim3d.stopSpinning();
            return;
        }
        var record, labels = {};
        for ( var i = 0, ln = records.length; i < ln; i++ ) {
            record = records[i];
            var value = '';
            for ( var j = 0, lj = fieldNames.length; j < lj; j++ ) {
                value += record.getValue(fieldNames[j]);
                if (j !== lj - 1) {
                    value += '\n';
                }
            }
            labels[record.getValue('rm.bl_id') + ';' + record.getValue('rm.fl_id') + ';' + record.getValue('rm.rm_id')] = value;
        }
        if (Object.keys(labels).length > 0) {
            this.bim3d.clearLabels();
            //add labels to the navigator, the labels will be displayed
            this.bim3d.addLabels(this.getCategoryIds(['Rooms'])[0], labels, function () {
                View.controllers.get('eqSysBim3dController').bim3d.stopSpinning();
            });
        } else {
            this.bim3d.stopSpinning();
        }
        labels = null;
    },
    setModelCategories: function (blId) {
        var exteriorSuperCategories = [], superCategories = [];
        var shell = this.getModelByFileName(blId, 'shell');
        var core = this.getModelByFileName(blId, 'core');
        var site = this.getModelByFileName(blId, 'site');
        var hvac = this.getModelByFileName(blId, 'hvac');
        var plumbing = this.getModelByFileName(blId, 'plumbing');
        if (shell) {
            exteriorSuperCategories.push(this.buildCategories(shell));
        }
        if (core) {
            exteriorSuperCategories.push(this.buildCategories(core));
        }
        if (site) {
            var siteCategories = this.buildCategories(site, '{building}-site');
            siteCategories.codes = siteCategories.codes.concat(this.getCategoryIds([['Topography', 'Site', 'Roads', 'Parking', 'Property Lines', 'Pads']]));
            exteriorSuperCategories.push(siteCategories);
        }
        var disciplinesSuperCategories = [];
        if (hvac) {
            disciplinesSuperCategories.push(this.buildCategories(hvac, ''));
        }
        if (plumbing) {
            disciplinesSuperCategories.push(this.buildCategories(plumbing, '{building}-plumbing'));
        }
        if (exteriorSuperCategories.length > 0) {
            superCategories.push({
                title: 'Exterior/Core',
                name: 'Exterior',
                superCategories: exteriorSuperCategories
            });
            var floorElements = {
                title: 'Floor Elements',
                name: 'Floor',
                superCategories: [
                    {title: 'Interior Walls', name: 'Interior Walls', codes: this.getCategoryIds(['Walls']), functions: ['Interior'], dual: 'true', json: ''},
                    {title: 'Floors', name: 'Floors', dual: 'true', codes: this.getCategoryIds(['Floors']), functions: ['Interior'], json: ''},
                    {title: 'Ceilings', name: 'Ceilings', dual: 'true', codes: this.getCategoryIds(['Ceilings']), functions: ['Interior'], json: ''},
                    {title: 'Curtain Panels', name: 'Curtain Panels', codes: this.getCategoryIds(['Curtain Panels']), functions: ['Interior'], dual: 'true', json: ''},
                    {title: 'Doors', name: 'Doors', codes: this.getCategoryIds(['Doors']), functions: ['Interior'], dual: 'true', json: ''},
                    {title: 'Lighting', name: 'Lighting', codes: this.getCategoryIds(['Lighting Devices', 'Lighting Fixtures', 'Electrical Fixtures']), dual: 'false', functions: ['Interior'], json: '{building}-lighting'}
                ]
            };
            var assets = {
                title: 'Assets',
                name: 'Assets',
                superCategories: [
                    {title: 'Rooms', name: 'Rooms', codes: this.getCategoryIds(['Rooms']), functions: ['Interior'], json: ''},
                    {title: 'Equipment', name: 'Equipment', codes: this.getCategoryIds(['Fire Alarm Devices', 'Security Devices', 'Specialty Equipment']), functions: ['Interior'], json: ''},
                    {title: 'Furniture', name: 'Furniture', codes: this.getCategoryIds(['Furniture']), functions: ['Interior'], json: ''}
                ]
            };
            superCategories.push(floorElements);
            superCategories.push(assets);
        }
        if (disciplinesSuperCategories.length > 0) {
            superCategories.push({
                title: 'Disciplines',
                name: 'Disciplines',
                superCategories: disciplinesSuperCategories
            });
        }
        // reset categories if building model is changed
        if (this.bim3d.categoriesPanel) {
            this.bim3d.categoriesPanel.uninitialize();
            this.bim3d.categoriesPanel = null;
        }
        this.bim3d.setSuperCategories(superCategories);
        this.setCategoryClickListener(blId, shell);
    },
    setCategoryClickListener: function (blId) {
        // update json file name macros at runtime - important call!!!
        this.bim3d.categoriesPanel.setClickListener(this.onCategoryClick.createDelegate(this, [blId], true));
    },
    onCategoryClick: function (link, name, jsonObj, levelsObj, panel, blId) {
        if (typeof jsonObj !== 'undefined' && jsonObj !== null && jsonObj.json !== 'null' && jsonObj.json !== '') {
            jsonObj.json = jsonObj.json.replace('{building}', blId.toLowerCase());
        }
        //if synch Core and dual visibility Floors and Ceilings
        if ('Core' === name) {
            this.bim3d.categoriesPanel.changeLinkStatusById('Floors', link.className === 'active');
            this.bim3d.categoriesPanel.changeLinkStatusById('Ceilings', link.className === 'active');
        }
        if (link.name.indexOf('Floor') >= 0 || link.name.indexOf('Assets') >= 0) {
            var activeLevels = this.bim3d.categoriesPanel.getLevels(true);
            if (activeLevels.length > 0) {
                levelsObj.levels = activeLevels;
            }
        }
        var scope = this;
        if ('HVAC' === name || link.name.indexOf(this.bim3d.categoriesPanel.PREFIX + 'Levels') >= 0) {
            var hvac = this.getModelByFileName(blId, 'hvac');
            if (hvac) {
                var showHvac = this.bim3d.categoriesPanel.changeLinkStatusById('HVAC');
                this.bim3d.showCategories(hvac.listToArray(hvac.categories), null, showHvac, false, showHvac);
                // reload hvac after model level is selected
                panel.setJsonLoadListener(function (scenes, json, type, codes, functions, levels, show) {
                    scope.bim3d.showCategories(hvac.listToArray(hvac.categories), null, showHvac, false, showHvac);
                });
            }
        }
        if (link.id.indexOf('Lighting') >= 0 && jsonObj !== null && jsonObj.json !== 'null' && jsonObj.json !== '') {
            panel.setJsonLoadListener(function (scenes, json, type, codes, functions, levels, show) {
                scope.bim3d.showCategories(scope.bim3d.categoriesPanel.getAllCategoryCodes(true), levels, true, true, true, scenes);
            });
        }

    },
    /**
     * Build building models for a building with data from afm_3dbin_drawings_ds.
     * @param blId building id.
     * @returns {Array} List of models.
     */
    buildBuildingModels: function (blId) {
        var dataSource = View.dataSources.get('afm_3dbin_drawings_ds');
        var restriction = new Ab.view.Restriction();
        restriction.addClause('afm_enterprise_graphics.bl_id', blId, '=');
        var records = dataSource.getRecords(restriction);
        var models = [];
        for ( var i = 0; i < records.length; i++ ) {
            var record = records[i];
            var flId = record.getValue('afm_enterprise_graphics.fl_id');
            var modelLevel = record.getValue('afm_enterprise_graphics.model_level');
            var modelName = record.getValue('afm_enterprise_graphics.model_name');
            var drawingName = record.getValue('afm_enterprise_graphics.dwg_name');
            var fileType = record.getValue('afm_enterprise_graphics.file_type');
            var fileName = record.getValue('afm_enterprise_graphics.filename');
            var superCategory = record.getValue('afm_enterprise_graphics.super_cat');
            var categories = record.getValue('afm_enterprise_graphics.categories');
            var functions = record.getValue('afm_enterprise_graphics.functions');
            models.push({
                blId: blId,
                flId: flId,
                modelLevel: modelLevel,
                modelName: modelName,
                fileType: fileType,
                fileName: fileName,
                superCategory: superCategory,
                categories: categories,
                drawingName: drawingName,
                functions: functions,
                json: drawingName.toLowerCase(),
                listToArray: function (list) {
                    return list.split(';');
                }
            })
        }
        return models;
    },
    /**
     * Gets a list of category IDS by their names.
     */
    getCategoryIds: function (names) {
        var result = [];
        var records = this.afm_bim_categories, record, name;
        for ( var i = 0, ln = records.length; i < ln; i++ ) {
            record = records[i];
            name = record.getValue('afm_bim_categories.category_id');
            if (names.indexOf(name) >= 0) {
                result.push(record.getValue('afm_bim_categories.revit_id'));
            }
        }
        return result;
    },
    /**
     * Build category model.
     * @param model
     * @param [json]
     * @returns {{title, name, codes: *, functions: *, json: (*|string)}}
     */
    buildCategories: function (model, json) {
        json = json || 'null';
        return {
            title: model.superCategory,
            name: model.superCategory,
            codes: model.listToArray(model.categories),
            functions: model.listToArray(model.functions),
            json: json
        }
    },
    /**
     * Build level category.
     * @param model
     * @returns {{title, name, superCategories: Array}}
     */
    buildLevelCategories: function (model) {
        var superCategories = [];
        var categories = model.listToArray(model.categories);
        for ( var c = 0; c < categories.length; c++ ) {
            var category = categories[c];
            superCategories.push({
                title: category,
                name: category,
                codes: [category],
                functions: model.listToArray(model.functions),
                json: ''
            })
        }
        return {
            title: model.modelLevel,
            name: model.modelLevel,
            superCategories: superCategories
        }
    },
    /**
     * Build level for model.
     * @param model
     * @returns {{title, name, levels: Array}}
     */
    buildLevel: function (model) {
        var levels = [];
        var categories = model.listToArray(model.categories);
        for ( var c = 0; c < categories.length; c++ ) {
            var category = categories[c];
            levels.push({
                title: category,
                name: category,
                codes: [category],
                functions: model.listToArray(model.functions),
                json: ''
            })
        }
        return {
            title: model.modelLevel,
            name: model.modelLevel,
            levels: levels
        }
    },
    /**
     * Returns building models id.
     */
    getBuildingId: function () {
        return _.keys(this.buildingModels)[0];
    },
    /**
     * Return all building models.
     * @param blId building id.
     * @returns {Object.<Array>} Building object with list of models.
     */
    getBuildingModels: function (blId) {
        return this.buildingModels[blId] || this.resetBuildingModels(blId);
    },
    /**
     * Return building levels.
     * @param blId
     * @returns {{title: string, name: string, levels: Array}}
     */
    getLevels: function (blId) {
        var buildingModels = this.buildingModels[blId];
        var levelTitle = getMessage('floorLevelTitle');
        var levels = [];
        for ( var i = 0; i < buildingModels.length; i++ ) {
            var model = buildingModels[i];
            if (valueExistsNotEmpty(model.modelLevel)) {
                levels.push({
                    title: levelTitle + ' ' + model.flId,
                    name: model.modelLevel,
                    codes: [model.modelLevel],
                    functions: model.listToArray(model.functions),
                    json: model.drawingName.toLowerCase(),
                    flId: model.flId,
                    fileType: model.fileType,
                    fileName: model.fileName,
                    modelLevel: model.modelLevel
                });
            }
        }
        return {
            title: getMessage('levelsTitle'),
            name: 'Levels',
            levels: levels
        }
    },
    /**
     * Returns model by fileName.
     * @param blId
     * @param value
     * @returns {*}
     */
    getModelByFileName: function (blId, value) {
        return _.find(this.buildingModels[blId], function (model) {
            return model['fileName'].indexOf(value) >= 0;
        });
    },
    /**
     * Get level by floor id.
     * @param {string} blId - Building id.
     * @param {string} flId - Floor id.
     * @returns {*} Level object.
     */
    getLevelByFloor: function (blId, flId) {
        return _.find(this.buildingModels[blId], function (model) {
            return valueExistsNotEmpty(model.modelLevel) && model.flId === flId;
        });
    },
    /**
     * Reset building models for a building id.
     * @param blId building id.
     */
    resetBuildingModels: function (blId) {
        this.buildingModels = {};
        this.buildingModels[blId] = this.buildBuildingModels(blId)
    },
    /**
     * Show error 3D message.
     */
    showInvalid3DModelMessage: function () {
        var levelTitle = this.assetParameters.getConfigParameter('blId') + ' ' + this.assetParameters.getConfigParameter('flId');
        View.alert(String.format(View.getLocalizedString(this.bim3d.z_MESSAGE_NO_3D_MODEL), levelTitle));
    }
});

/**
 * Set fly mode.
 * @param {Object} action - Checkbox object.
 * @param {string} type - flyModeDependents|flyModeDependencies
 */
function setFlyMode(action, type) {
    var controller = View.controllers.get('eqSysBim3dController');
    controller[type] = action.checked;
    controller.enableFlyMode(type);
}

/**
 * On click dependency node handler event.
 * @param {Object} context - tree panel context.
 */
function onClickDependencyNodeHandler(context) {
    var controller = View.controllers.get('eqSysBim3dController');
    var crtTreeNode = View.panels.get(context.command.parentPanelId).lastNodeClicked;
    var eqId = crtTreeNode.data['eq_system.eq_id_depend'];
    controller.onHighlight(eqId, false);
}

/**
 * On click dependent node handler event.
 * @param {Object} context - tree panel context.
 */
function onClickDependentNodeHandler(context) {
    var controller = View.controllers.get('eqSysBim3dController');
    var crtTreeNode = View.panels.get(context.command.parentPanelId).lastNodeClicked;
    var eqId = crtTreeNode.data['eq_system.eq_id_depend'];
    controller.onHighlight(eqId, false);
}

/**
 * Set node label.
 * @param {Object} node - Tree node.
 */
function afterGeneratingTreeNode(node) {
    var depend = node.data['eq_system.eq_id_depend'];
    var systemLvl = node.data['eq_system.system_level'];
    if (node.data['eq_system.system_level.raw']) {
        systemLvl = node.data['eq_system.system_level.raw'];
    }
    var label = node.data['eq_system.system_name'] + ' (' + depend + ')';
    var nodeLabel = '<div class="nodeLabel">';
    var labelId = 'label' + node.id + '_0',
        labelClass = 'label color_' + systemLvl;
    var controller = View.controllers.get('eqSysBim3dController');
    if (controller && valueExistsNotEmpty(controller.selectedEquipment)) {
        if (depend === controller.selectedEquipment) {
            labelClass += ' highlight';
        }
    }
    nodeLabel += '<div id="' + labelId + '" class="' + labelClass + '"><span class="ygtvlabel">' + label + '</span></div>';
    nodeLabel += '</div>';
    node.label = nodeLabel;
}

/**
 *  Enables panel toolbar menu separator.
 */
Ext.menu.Separator = function (config) {
    Ext.menu.Separator.superclass.constructor.call(this, config);
};
Ext.extend(Ext.menu.Separator, Ext.menu.BaseItem, {
    itemCls: "x-menu-sep",
    hideOnClick: false,
    onRender: function (li) {
        var s = document.createElement("span");
        s.className = this.itemCls;
        s.innerHTML = "&#160;";
        this.el = s;
        li.addClass("x-menu-sep-li");
        Ext.menu.Separator.superclass.onRender.apply(this, arguments);
    }
});