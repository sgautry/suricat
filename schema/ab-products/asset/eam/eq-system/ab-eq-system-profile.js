View.createController('eqSysProfileController', {
    // selected equipment id
    eqId: null,
    /**
     * Load profile by asset parameters.
     */
    afterInitialDataFetch: function () {
        var parameters = null;
        if (valueExists(this.view.parentViewPanel) && valueExists(this.view.parentViewPanel.assetParameters)) {
            parameters = this.view.parentViewPanel.assetParameters;
            this.eqId = parameters.getConfigParameterIfExists('eqId');
            this.abProfileEquipment_form.refresh(new Ab.view.Restriction({'eq.eq_id': this.eqId}));
            this.teamPanel.refresh(new Ab.view.Restriction({'team.eq_id': this.eqId}));
            // backward compatible to 23.1 schema
            this.teamPanel.addParameter('useCustodian', isCustodianSchemaDefined);
            if (isCustodianSchemaDefined) {
                this.custodiansAssignedPanel.refresh(new Ab.view.Restriction({'team.eq_id': this.eqId}));
            } else {
                this.abEqSysProfileTabs.hideTab('abEqSysProfileTabs_custodians');
            }
            this.eqRmPanel.refresh(new Ab.view.Restriction({'eq_rm.eq_id': this.eqId}));
        }
    },
    /**
     * Set equipment image.
     */
    abProfileEquipment_form_afterRefresh: function (panel) {
        this.setImageLayout(panel.id);
        var hasImage = valueExistsNotEmpty(panel.getFieldValue('eqstd.doc_graphic'));
        panel.showField('image_field', hasImage);
        if (hasImage) {
            panel.showImageDoc('image_field', 'eqstd.eq_std', 'eqstd.doc_graphic');
        }
    },
    /**
     * Set image form layout.
     * @param {string} panelId - panel id.
     */
    setImageLayout: function (panelId) {
        var image = jQuery('#' + panelId + '_image_field');
        image.css('padding-left', '30px');
        image.parent().prev().remove();
        image.parent().attr('colspan', 2);
    },
    /**
     * On profile action.
     */
    abProfileEquipment_actions_onProfile: function () {
        View.getOpenerView().openDialog('ab-profile-equipment.axvw', this.abProfileEquipment_form.restriction, false, {
            width: 1024,
            height: 800,
            closeButton: true
        });
    },
    /**
     * On edit action.
     */
    abProfileEquipment_actions_onEdit: function () {
        View.getOpenerView().openDialog('ab-eq-edit-form.axvw', this.abProfileEquipment_form.restriction, false, {
            width: 1024,
            height: 800,
            closeButton: true,
            callback: function () {
                View.panels.get('abProfileEquipment_form').refresh();
            }
        });
    },
    /**
     * On create action.
     */
    abProfileEquipment_actions_onCreateAction: function () {
        var record = new Ab.data.Record({
            'bl.asset_type': 'eq',
            'bl.asset_id': this.eqId,
            'bl.site_id': this.abProfileEquipment_form.record.getValue('eq.site_id'),
            'bl.bl_id': this.abProfileEquipment_form.record.getValue('eq.bl_id'),
            'bl.fl_id': this.abProfileEquipment_form.record.getValue('eq.fl_id'),
            'bl.rm_id': this.abProfileEquipment_form.record.getValue('eq.rm_id')
        }, false);
        View.getOpenerWindow().assignAsset('eq', record);
    },
    /**
     * Add team member.
     */
    teamPanel_onAdd: function () {
        this.onShowTeamDetails(this.teamPanel.restriction, true);
    },
    /**
     * Show team member details.
     * @param selectedRecord
     */
    showTeamDetails: function (selectedRecord) {
        var pKeyValue = selectedRecord.getValue('team.autonumbered_id');
        this.onShowTeamDetails(new Ab.view.Restriction({'team.autonumbered_id': pKeyValue}), false);
    },
    onShowTeamDetails: function (restriction, newRecord) {
        View.getOpenerView().openDialog('ab-eq-system-team-support.axvw', restriction, newRecord, {
            closeButton: true,
            callback: function () {
                View.closeThisDialog();
                View.panels.get('teamPanel').refresh();
            }
        });
    },
    /**
     * Add room served.
     */
    eqRmPanel_onAdd: function () {
        this.showSpaceServed(new Ab.view.Restriction({'eq_rm.eq_id': this.eqId}), true);
    },
    /**
     * Show room served details.
     * @param selectedRecord
     */
    showSpaceServedDetails: function (selectedRecord) {
        var restriction = new Ab.view.Restriction({'eq_rm.eq_id': this.eqId});
        restriction.addClause('eq_rm.bl_fl_rm', selectedRecord.getValue('eq_rm.bl_fl_rm'));
        this.showSpaceServed(restriction, false);
    },
    showSpaceServed: function (restriction, newRecord) {
        View.getOpenerView().openDialog('ab-eq-system-space-served.axvw', restriction, newRecord, {
            closeButton: true,
            callback: function () {
                View.closeThisDialog();
                View.panels.get('eqRmPanel').refresh();
            }
        });
    },
    /**
     * Add custodian.
     * @param {boolean} isOwner true if owner.
     */
    addCustodianCommon: function (isOwner) {
        var selectedAssets = [];
        selectedAssets.push({
            "asset_type": 'eq',
            "asset_id": this.eqId
        });
        View.getOpenerView().openDialog('ab-eam-assign-custodian.axvw', null, true, {
            width: 1024,
            height: 800,
            isOwner: isOwner,
            selectedAssets: selectedAssets,
            callback: function (addMultipleCustodians) {
                View.panels.get('custodiansAssignedPanel').refresh();
                if (!addMultipleCustodians) {
                    View.getOpenerView().closeDialog();
                }
            }
        });
    },
    /**
     * Edit custodian.
     * @param {object} selectedRecord - Selected record.
     * @param {object} restriction - Selected record pkey restriction.
     */
    editCustodian: function (selectedRecord, restriction) {
        var isOwner = selectedRecord.getValue('team.is_owner') === "1";
        View.getOpenerView().openDialog('ab-eam-edit-custodian.axvw', restriction, false, {
            assetType: 'eq',
            assetCode: this.eqId,
            isOwner: isOwner,
            callback: function () {
                View.panels.get('custodiansAssignedPanel').refresh();
                View.getOpenerView().closeDialog();
            }
        });
    }
});
function selectTeamMember(context) {
    var parentPanel = View.panels.get(context.command.parentPanelId);
    var selectedRecord = parentPanel.gridRows.get(parentPanel.selectedRowIndex).getRecord();
    View.controllers.get('eqSysProfileController').showTeamDetails(selectedRecord);
}
function selectSpaceServed(row) {
    View.controllers.get('eqSysProfileController').showSpaceServedDetails(row.row.getRecord());
}
function onAssignToOwnerCustodian() {
    View.controllers.get('eqSysProfileController').addCustodianCommon(true);
}
function onAssignToOtherCustodian() {
    View.controllers.get('eqSysProfileController').addCustodianCommon(false);
}
function editCustodian(context) {
    var parentPanel = View.panels.get(context.command.parentPanelId);
    var selectedRecord = parentPanel.gridRows.get(parentPanel.selectedRowIndex).getRecord();
    View.controllers.get('eqSysProfileController').editCustodian(selectedRecord, context.command.restriction);
}
/**
 * For all of the actions below, the following applies if there is no viewer panel open:
 * If no viewer panel is open, and there is a panel available in the configuration, then it opens the 2D drawing in that panel.
 * If all of the panels are taken by non-viewer panels, then it opens the 2D Drawing in a dialog.
 * @param action
 */
function onProfileAction(action) {
    var controller = View.controllers.get('eqSysProfileController'),
        eqId = controller.abProfileEquipment_form.getFieldValue('eq.eq_id');
    var openerView = View.getOpenerView();
    var displayOptionsController = openerView.controllers.get('displayOptionsController');
    var bim3dPanelId = displayOptionsController.getViewPanelId('ab-eq-system-bim-3d.axvw'),
        drawingPanelId = displayOptionsController.getViewPanelId('ab-eq-system-drawing.axvw', bim3dPanelId);
    if (!valueExists(drawingPanelId) && !valueExists(bim3dPanelId)) {
        openerView.getWindow().DisplayPanelConfiguration.displayDrawingPanel(drawingPanelId, eqId, action);
    } else {
        if (valueExists(drawingPanelId)) {
            openerView.getWindow().DisplayPanelConfiguration.displayDrawingPanel(drawingPanelId, eqId, action);
        }
        if (valueExists(bim3dPanelId)) {
            openerView.getWindow().DisplayPanelConfiguration.displayBim3dPanel(bim3dPanelId, eqId, action);
        }
    }
}
