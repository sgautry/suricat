Ab.namespace('bim3d');
Ab.bim3d.InfoWindow = Base.extend({
    config: {},
    /**
     * information window <div> object
     */
    infoDiv: null,
    /**
     * reference to bim3d controller
     */
    bim3dController: null,
    constructor: function (config) {
        this.config = config;
        this.init();
    },
    init: function () {
        this.infoDiv = document.createElement('div');
        this.infoDiv.id = this.config.divId + "_infoWindow";
        this.infoDiv.className = 'info-window';
        this.infoDiv.style.width = (this.config.width ? this.config.width : "300px");
        //only set height if predefined in config.
        if (this.config.height) {
            this.infoDiv.style.height = this.config.height;
        }
        if (this.config.position == 'bottom') {
            this.infoDiv.style.bottom = 0;
        }
        if (this.config.customEvent) {
            this.customEvent = this.config.customEvent;
        }
        var closeButton = document.createElement('div');
        closeButton.className = 'close-button';
        this.infoDiv.appendChild(closeButton);
        var eventName = ('ontouchstart' in document.documentElement) ? 'touchstart' : 'click';
        var control = this;
        closeButton.addEventListener(eventName, function () {
            control.infoDiv.style.display = 'none';
            if (control.customEvent) {
                control.customEvent(control.drawingController);
            }
        }, false);
        var textNode = document.createElement("div");
        textNode.innerHTML = (this.config.initialMsg ? this.config.initialMsg : "");
        textNode.id = this.infoDiv.id + '_infoText';
        this.infoDiv.appendChild(textNode);
        var container = document.getElementById('bim3d');
        container.insertBefore(this.infoDiv, document.getElementsByTagName('canvas')[0]);
        // hide infoDiv initially, if no initial message.
        if (!this.config.initialMsg) {
            this.infoDiv.style.display = 'none';
        }
    },
    /**
     * Specify a given text for the infoWindow
     * @param text String
     */
    setText: function (text) {
        this.initInfoDiv();
        var node = document.getElementById(this.infoDiv.id + "_infoText");
        node.innerHTML = text;
        this.infoDiv.style.display = '';
    },
    /**
     * Apppend a given text for the infoWindow
     * @param text String
     */
    appendText: function (text) {
        this.initInfoDiv();
        var node = document.getElementById(this.infoDiv.id + "_infoText");
        node.innerHTML = node.innerHTML + text;
        this.infoDiv.style.display = '';
    },
    /**
     * Whether or not to show the infoWindow
     * @param bShow Boolean
     */
    show: function (bShow) {
        this.initInfoDiv();
        this.infoDiv.style.display = (bShow === true) ? '' : 'none';
    },
    initInfoDiv: function () {
        var infoDiv = document.getElementById(this.infoDiv.id);
        if (infoDiv == null) {
            this.init();
        }
    }
}, {});