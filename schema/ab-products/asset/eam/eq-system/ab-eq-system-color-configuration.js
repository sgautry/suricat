/**
 * Equipment System color configuration.
 * Used for drawings and bim 3D models.
 */
EqSysColorConfig = new (Base.extend({
    selectAssetHighlightColor: 'rgb(255, 0, 0)', //red
    highlightDependColor: 'rgb(0, 0, 255)',//blue
    highlightDependencyColor: 'rgb(0, 255, 0)',//green
    highlightRoomColor: 'rgb(255, 255, 0)'//yellow
}));