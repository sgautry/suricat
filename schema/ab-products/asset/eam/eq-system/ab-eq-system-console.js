View.createController("eqSystemConsoleTabsController", {
    filterRestriction: null,
    afterInitialDataFetch: function () {
        this.abEamEqSysConsoleTabs.addEventListener('afterTabChange', this.afterTabChangeHandler.createDelegate(this));
        for ( var name in window.location.parameters ) {
            if (name.indexOf('eq_id') >= 0) {
                this.abEamEqSysConsoleTabs.eq_id = window.location.parameters[name];
                break;
            }
        }
    },
    afterTabChangeHandler: function (tabsPanel, selectedTabName) {
        var selectedTab = tabsPanel.findTab(selectedTabName);
        if (selectedTab.isContentLoaded) {
            var controller;
            if ('abEamAssetDisposalConsoleTabs_reports' === selectedTabName) {
                controller = selectedTab.getContentFrame().View.controllers.get('abEamDisposalEvaluateCtrl');
            } else {
                controller = selectedTab.getContentFrame().View.controllers.get('eqSysAssignmentController');
                if (controller) {
                    controller.refreshAllPanels = true;
                }
            }
            // set filter restriction if was applied from another tab
            if (controller) {
                controller.filterController.copyFilterRestriction(this.filterRestriction);
                controller.filterController.setCollapsed(!this.filterRestriction.isEmpty());
                controller.filterController.abEamAssetFilter_onFilter();
            }
        }
    }
});