// Filter config options
var eqSysAnalysisFilterConfig = new Ext.util.MixedCollection();
eqSysAnalysisFilterConfig.addAll(
    {id: 'bl.asset_type', fieldConfig: {type: 'enumList', hidden: false, readOnly: true, values: null, dfltValue: 'eq', hasEmptyOption: false}},
    {id: 'bl.pending_action', fieldConfig: {type: 'enumList', hidden: true, readOnly: true, values: null, dfltValue: null, hasEmptyOption: true}},
    {id: 'deprec_value', fieldConfig: {type: 'number', hidden: false, optional: true, readOnly: false, values: null, dfltValue: null, hasEmptyOption: false}},
    {id: 'deprec_method', fieldConfig: {type: 'enumList', hidden: false, optional: true, readOnly: false, values: null, dfltValue: null, hasEmptyOption: true}},
    {id: 'deprec_value_type', fieldConfig: {type: 'enumList', hidden: false, optional: true, readOnly: false, values: null, dfltValue: null, hasEmptyOption: true}},
    {id: 'eq_system.criticality_function', fieldConfig: {type: 'enumList', hidden: false, optional: true, readOnly: false, values: null, dfltValue: null, hasEmptyOption: true}},
    {id: 'eq_system.criticality_mission', fieldConfig: {type: 'enumList', hidden: false, optional: true, readOnly: false, values: null, dfltValue: null, hasEmptyOption: true}},
    {id: 'eq_system.stakeholder_type', fieldConfig: {type: 'enumList', hidden: false, optional: true, readOnly: false, values: null, dfltValue: null, hasEmptyOption: true}}
);
/**
 * System analysis controller.
 */
View.createController('eqSysAnalysisController', {
    // on tree selected equipment
    selectedEquipmentId: null,
    // parameter passed to reports
    selectedSystemName: null,
    // filter console controller
    filterController: null,
    // main controller - tabs
    mainController: null,
    afterViewLoad: function () {
        // check backward compatible
        if (!isCustodianSchemaDefined) {
            eqSysAnalysisFilterConfig.add({id: 'custodian', fieldConfig: {type: 'text', hidden: true}});
        }
        this.eqSysInventoryTreePanel.addParameter('useEnterpriseGraphics', isEnterpriseGraphicsSchemaDefined);
        // get main Controller
        this.mainController = this.getMainController();
        // initialize filter config
        this.initializeFilterConfig();
        // set tree level node restriction
        this.updateRestrictionLevel();
        // config display panels
        this.configDisplayPanels();
        Ab.tree.TreeTableNode.prototype.getFirstCellStyle = function (index) {
            return '';
        };
    },
    afterInitialDataFetch: function () {
        if (valueExists(this.mainController)) {
            // set filter restriction if was applied from another tab
            this.filterController.setRestriction(this.mainController.filterRestriction);
        }
        // initialize filter
        this.filterController.initializeFilter();
        // initialize asset status
        this.filterController.onChangeAssetType();
        if (valueExists(this.mainController)) {
            if (this.mainController.id !== 'eqSystemConsoleTabsController') {
                // hide more action button
                this.filterController.hideMoreAction();
                // load initial data
                this.filterController.abEamAssetFilter_onFilter();
            } else {
                // init main controller filter restriction
                this.mainController.filterRestriction = new Ab.view.Restriction();
                // select the first element in the Inventory tree
                this.fireClickFirstNode();
            }
        }
    },
    fireClickFirstNode: function () {
        var firstNode = document.getElementsByClassName('nodeLabel')[0];
        if (valueExists(firstNode)) {
            var panelC = View.panels.get('abEqSysInfo_panelC'); // last panel
            // wait until last panel is loaded
            var runner = new Ext.util.TaskRunner();
            var task = {
                run: function () {
                    var contentFrameView = panelC.getContentFrame().View;
                    if (valueExists(contentFrameView) && contentFrameView.state === View.STATE_READY) {
                        runner.stop(task);
                        firstNode.click();
                    }
                },
                interval: 200
            };
            runner.start(task);
        }
    },
    initializeFilterConfig: function () {
        // place holder for custom actions
        if (!valueExists(this.filterController)) {
            this.filterController = this.getFilterController();
        }
        if (valueExists(this.mainController) && this.mainController.id !== 'eqSystemConsoleTabsController') {
            this.filterController.collapsedFields = false;
        }
        this.filterController.initializeConfigObjects(eqSysAnalysisFilterConfig);
        this.filterController.onFilterCallback = function (restriction) {
            View.controllers.get('eqSysAnalysisController').onFilter(restriction);
        };
        this.filterController.onClickActionButton1Handler = function (buttonElem) {
            View.controllers.get('eqSysAnalysisController').onClickReportMenu(buttonElem);
        };
        this.filterController.actionButton1Label = getMessage('buttonLabel_reports');
    },
    updateRestrictionLevel: function () {
        this.eqSysInventoryTreePanel.updateRestrictionForLevel = updateTreeRestrictionForLevel;
    },
    configDisplayPanels: function () {
        DisplayPanelConfiguration.addPanelDisplayConfig('eqSysInventoryTreePanel', {
            panelA: {visible: true, panelType: 'profile'},
            panelB: {visible: true, panelType: 'drawing'},
            panelC: {visible: true, panelType: ''}
        });
    },
    resetDisplayPanels: function () {
        DisplayPanelConfiguration.resetPanels('eqSysInventoryTreePanel');
    },
    onClickNode: function (panelId, eqId, systemName, crtTreeNode) {
        this.selectedEquipmentId = eqId;
        this.selectedSystemName = systemName;
        DisplayPanelConfiguration.displayPanels(panelId, eqId, crtTreeNode);
    },
    onClickReportMenu: function (buttonElem) {
        var reportMenuItem = new MenuItem({
            menuDef: {
                id: 'reportsMenu',
                type: 'menu',
                viewName: null,
                isRestricted: false,
                parameters: null
            },
            onClickMenuHandler: onClickMenu,
            onClickMenuHandlerRestricted: onClickMenuWithRestriction,
            submenu: abEqSystemsReportsMenu
        });
        reportMenuItem.build();
        var menu = new Ext.menu.Menu({items: reportMenuItem.menuItems, cls: 'filter-reports'});
        menu.show(buttonElem, 'tl-bl?');
    },
    /**
     * On filter event handler
     * @param restriction restriction object
     */
    onFilter: function (restriction) {
        //set current restriction  to main controller. This restriction is passed from tabs.
        if (valueExists(this.mainController)) {
            this.mainController.filterRestriction = this.filterController.getRestrictionWithoutDefaultValues(restriction);
        }
        // specific equipment system restriction
        var filterMasterRestrictionSql = '1=1';
        var filterParameters = getFilterLevelRestriction(restriction, true);
        if (valueExistsNotEmpty(filterParameters)) {
            var filterRestrictionSql = getMessage('filterRestrictionSql');
            filterMasterRestrictionSql = filterRestrictionSql.replace(/%{filterParameters}/g, filterParameters);
        }
        this.eqSysInventoryTreePanel.addParameter('filterRestriction', filterMasterRestrictionSql);
        // backward compatible to 23.1 schema
        this.eqSysInventoryTreePanel.addParameter('useEnterpriseGraphics', isEnterpriseGraphicsSchemaDefined);
        // refresh tree
        this.eqSysInventoryTreePanel.refresh();
        // reset panels to blank views when filter restriction is passed
        this.resetDisplayPanels();
        //TODO check if selected node is the same equipment id ??
        //if (restriction.findClause('bl.asset_id')) {
        // select the first element in the Inventory tree if filtered by equipment code
        // this.fireClickFirstNode();
        //}
    },
    getFilterController: function () {
        return View.controllers.get('abEamAssetFilterCtrl');
    },
    eqSysInventoryTreePanel_afterGeneratingTreeNode: function (node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    eqSysInventoryTreeLvl_2_afterGeneratingTreeNode: function (node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    eqSysInventoryTreeLvl_3_afterGeneratingTreeNode: function (node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    eqSysInventoryTreeLvl_4_afterGeneratingTreeNode: function (node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    eqSysInventoryTreeLvl_5_afterGeneratingTreeNode: function (node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    eqSysInventoryTreeLvl_6_afterGeneratingTreeNode: function (node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    eqSysInventoryTreeLvl_7_afterGeneratingTreeNode: function (node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    eqSysInventoryTreeLvl_8_afterGeneratingTreeNode: function (node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    eqSysInventoryTreeLvl_9_afterGeneratingTreeNode: function (node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    /**
     * Set tree node label and also a node separator.
     * The separator is displayed to drop an element on the same node level.
     */
    formatLabelNode: function (node) {
        var depend = node.data['eq_system.eq_id_depend'];
        var systemLvl = node.data['eq_system.system_level'];
        if (node.data['eq_system.system_level.raw']) {
            systemLvl = node.data['eq_system.system_level.raw'];
        }
        var label = node.data['eq_system.system_name'] + ' (' + depend + ')';
        //label += ' ' + node.data['eq_system.sort_order'];
        var nodeLabel = '<div class="nodeLabel">';
        var labelId = 'label' + node.id + '_0';
        var separatorId = 'separator' + node.id + '_0';
        nodeLabel += '<div id="' + labelId + '" class="label color_' + systemLvl + '"><span class="ygtvlabel">' + label + '</span></div>';
        nodeLabel += '<div id="' + separatorId + '" class="separator"></div>';
        nodeLabel += '</div>';
        label = node.label.replace('{label_html}', nodeLabel);
        // set action images
        var hasDrawing = '1' === node.data['eq_system.has_drawing'],
            has3dView = '1' === node.data['eq_system.has_3dbin'],
            has360View = '1' === node.data['eq_system.has_360doc'];
        this.formatActions(node, label, hasDrawing, has3dView, has360View);
    },
    formatActions: function (node, label, hasDrawing, has3dView, has360View) {
        var elem = jQuery(label);
        var idsToRemove = [];
        var iconAction = elem.find('#image1');
        if (!hasDrawing && iconAction.length) {
            idsToRemove.push(iconAction.parent().parent().attr('id'));
        }
        iconAction = elem.find('#image2');
        if (!has3dView && iconAction.length) {
            idsToRemove.push(iconAction.parent().parent().attr('id'));
        }
        iconAction = elem.find('#image3');
        if (!has360View && iconAction.length) {
            idsToRemove.push(iconAction.parent().parent().attr('id'));
        }
        var formatLabel = '';
        elem.each(function () {
            var el = jQuery(this);
            for ( var i = 0; i < idsToRemove.length; i++ ) {
                if (idsToRemove[i] === el.attr('id')) {
                    el.children().hide();
                }
            }
            formatLabel += this.outerHTML;
        });
        node.label = formatLabel;
    },
    eqSysInventoryTreePanel_onDrawingAction: function (node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    /**
     * Get main tabs controller.
     * @returns {*} tabs controller.
     */
    getMainController: function () {
        var controller = null;
        var openerView = this.view.getOpenerView();
        if (valueExists(openerView)) {
            if (valueExists(openerView.controllers.get('eqSystemConsoleTabsController'))) {
                controller = openerView.controllers.get('eqSystemConsoleTabsController');
            } else if (valueExists(openerView.controllers.get('abEamAssetConsoleController'))) {
                controller = openerView.controllers.get('abEamAssetConsoleController');
            } else if (valueExists(openerView.controllers.get('abEamDisposalConsoleCtrl'))) {
                controller = openerView.controllers.get('abEamDisposalConsoleCtrl');
            }
        }
        return controller;
    }
});
function onClickNodeHandler(context) {
    var controller = View.controllers.get('eqSysAnalysisController');
    var crtTreeNode = View.panels.get(context.command.parentPanelId).lastNodeClicked;
    var eqId = crtTreeNode.data['eq_system.eq_id_depend'],
        systemName = crtTreeNode.data['eq_system.system_name'];
    controller.onClickNode(context.command.parentPanelId, eqId, systemName, crtTreeNode);
}
/**
 * Set tree node label.
 * @param node
 */
function afterGeneratingTreeNode(node) {
    if (valueExists(node.data['eq_system.eq_id_depend'])
        && valueExists(node.data['eq_system.system_level'])) {
        var depend = node.data['eq_system.eq_id_depend'],
            systemLvl = node.data['eq_system.system_level'];
        if (node.data['eq_system.system_level.raw']) {
            systemLvl = node.data['eq_system.system_level.raw'];
        }
        var label = node.data['eq_system.system_name'] + ' (' + depend + ')';
        //label += ' ' + node.data['eq_system.sort_order'];
        var nodeLabel = '<div class="nodeLabel">',
            labelId = 'label' + node.id + '_0';
        nodeLabel += '<div id="' + labelId + '" class="label color_' + systemLvl + '"><span class="ygtvlabel">' + label + '</span></div>';
        nodeLabel += '</div>';
        node.label = nodeLabel;
    }
}
/**
 * Update tree restriction for level and set filter restriction.
 */
function updateTreeRestrictionForLevel(parentNode, level, restriction) {
    if (level > 0) {
        restriction.removeClause('eq_system.auto_number');
        restriction.addClause('eq_system.eq_id_master', parentNode.data['eq_system.eq_id_depend'], '=');
    }
    if (this.parameters.filterRestriction) {
        var filterRestriction = this.parameters.filterRestriction;
        var levelRestriction = '';
        for ( var i = (level + 1), length = 10; i < length; i++ ) {
            levelRestriction += 'eq.level' + i + '=eq_system.eq_id_depend ';
            if (i < (length - 1)) {
                levelRestriction += ' OR ';
            }
        }
        this.parameters.filterRestriction = filterRestriction.replace(/%{levelRestriction}/g, levelRestriction);
    }
}
/**
 * Open system assignment tab action in a new tab.
 * If tab is already opened, select the tab.
 * @param {string} fileName ab-eq-system-assignment.axvw
 */
function openSystemAssignmentTab(fileName) {
    var tabsControl = View.getControlsByType(parent, 'tabs')[0];
    if (tabsControl) {
        var existingTab = _.find(tabsControl.tabs, function (tab) {
            return tab.fileName === fileName;
        });
        if (valueExists(existingTab)) {
            tabsControl.selectTab(existingTab.name, null, false);
        } else {
            tabsControl.createTab(fileName, null, false);
        }
    }
}
/**
 * On reports action.
 */
function onClickMenu(menu) {
    if (valueExists(menu.viewName)) {
        var dialogConfig = {
            width: 1024,
            height: 800,
            closeButton: true
        };
        if (valueExists(menu.parameters)) {
            for ( var param in menu.parameters ) {
                if (param == 'title') {
                    dialogConfig[param] = getMessage(menu.parameters[param]);
                } else {
                    dialogConfig[param] = menu.parameters[param];
                }
            }
        }
        View.openDialog(menu.viewName, null, false, dialogConfig);
    }
}
/**
 * On reports action with restriction.
 */
function onClickMenuWithRestriction(menu) {
    if (valueExists(menu.viewName)) {
        var dialogConfig = {
            width: 1024,
            height: 800,
            closeButton: true
        };
        var controller = View.controllers.get('eqSysAnalysisController');
        if (valueExists(menu.parameters)) {
            var selectedEquipmentId = controller.selectedEquipmentId,
                selectedSystemName = controller.selectedSystemName;
            var filterRestriction = getFilterLevelRestriction(controller.filterController.getFilterRestriction(), false);
            for ( var param in menu.parameters ) {
                if ('selectedEquipmentId' === param) {
                    dialogConfig[param] = selectedEquipmentId;
                } else if ('selectedSystemName' === param) {
                    dialogConfig[param] = selectedSystemName;
                } else if ('filterRestriction' === param) {
                    dialogConfig[param] = filterRestriction;
                } else if ('title' === param) {
                    dialogConfig[param] = getMessage(menu.parameters[param]);
                } else {
                    dialogConfig[param] = menu.parameters[param];
                }
            }
        }
        _.extend(dialogConfig, {
            'selectedAssets': {
                'eq': [controller.selectedEquipmentId]
            }
        });
        View.openDialog(menu.viewName, null, false, dialogConfig);
    }
}