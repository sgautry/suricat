function afterSelectSystem(context) {
    if (View.parameters.callback) {
        var parentPanel = View.panels.get(context.command.parentPanelId);
        if (parentPanel) {
            var selectedRow = parentPanel.gridRows.get(parentPanel.selectedRowIndex),
                record = selectedRow.getRecord();
            var systemName = record.getValue('eq_system.system_name');
            View.parameters.callback(View.parameters.level, systemName);
        }
    }
}