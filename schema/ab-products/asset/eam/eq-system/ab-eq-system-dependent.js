View.createController('eqSysDependentController', {
    eqId: null,
    afterViewLoad: function () {
        this.eqSysDependentTreePanel.updateRestrictionForLevel = function (parentNode, level, restriction) {
            if (level > 0) {
                restriction.removeClause('eq_system.auto_number');
                restriction.addClause('eq_system.eq_id_master', parentNode.data['eq_system.eq_id_depend'], '=');
            }
        }
    },
    afterInitialDataFetch: function () {
        var parameters = null;
        if (valueExists(this.view.parentViewPanel) && valueExists(this.view.parentViewPanel.assetParameters)) {
            parameters = this.view.parentViewPanel.assetParameters;
            this.eqId = parameters.getConfigParameterIfExists('eqId');
            this.eqSysDependentTreePanel.addParameter("lvlMasterRestriction", "eq_system.eq_id_depend='" + this.eqId + "'");
            
            if(valueExists(this.view.parentViewPanel.optionalTreeNodeClickEvents)){
            	var optionalTreeNodeClickEvents = this.view.parentViewPanel.optionalTreeNodeClickEvents
            	_.each(this.eqSysDependentTreePanel._panelsData, function(panelData) {
            		for(var i = 0; i < optionalTreeNodeClickEvents.length; i++){
            			panelData.events.push(optionalTreeNodeClickEvents[i])
            		}
            	});
            }
            
            this.eqSysDependentTreePanel.refresh();
            this.eqSysDependentTreePanel.expandAll();
        }
    }
});
/**
 * Set node label.
 */
function afterGeneratingTreeNode(node) {
    var depend = node.data['eq_system.eq_id_depend'];
    var systemLvl = node.data['eq_system.system_level'];
    if (node.data['eq_system.system_level.raw']) {
        systemLvl = node.data['eq_system.system_level.raw'];
    }
    var label = node.data['eq_system.system_name'] + ' (' + depend + ')';
    var nodeLabel = '<div class="nodeLabel">';
    var labelId = 'label' + node.id + '_0',
        labelClass = 'label color_' + systemLvl;
    var controller = View.controllers.get('eqSysDependentController');
    if (controller && valueExistsNotEmpty(controller.eqId)) {
        if (depend === controller.eqId) {
            labelClass += ' highlight';
        }
    }
    nodeLabel += '<div id="' + labelId + '" class="' + labelClass + '"><span class="ygtvlabel">' + label + '</span></div>';
    nodeLabel += '</div>';
    node.label = nodeLabel;
}