var MaximizeActionControl = Base.extend({
        panel: null,
        viewController: null,
        iconMaximizeDialog: '/schema/ab-core/graphics/show.gif',
        iconMaximize: '/schema/ab-core/graphics/icons/view/maximize.png',
        iconMinimize: '/schema/ab-core/graphics/icons/view/minimize.png',
        constructor: function (panelName, config) {
            this.panel = View.panels.get(panelName);
            this.config = config;
            this.init();
        },
        init: function () {
            if (!valueExists(View.getOpenerView().dialog)) {
                var icon = this.iconMaximizeDialog,
                    tooltip = getMessage(MaximizeActionControl.z_ACTION_NEW_WINDOW);
                if (valueExists(View.getParentViewPanel()) && 'center' === View.getParentViewPanel().region) {
                    icon = this.iconMaximize;
                    tooltip = getMessage(MaximizeActionControl.z_ACTION_MAXIMIZE);
                }
                this.addAction({
                    id: 'maximize',
                    tooltip: tooltip,
                    icon: icon,
                    listener: this._maximize.createDelegate(this)
                });
            }
        },
        addAction: function (config) {
            if (config == null) {
                return;
            }
            if (this.panel.actions.get(config.id) !== undefined) {
                this.panel.actions.get(config.id).setTitle(config.text);
            } else {
                this.panel.actions.add(config.id, new Ab.view.Action(this.panel, config));
            }
        },
        _maximize: function () {
            if ('center' === View.getParentViewPanel().region) {
                var maximize = jQuery('#maximize button');
                var isMaximize = maximize.data('isMaximize') || false;
                if (isMaximize) {
                    this.onExpand();
                    maximize.css('background-image', 'url(' + View.contextPath + this.iconMaximize + ')');
                    maximize.data('isMaximize', false);
                    this.panel.actions.get('maximize').setTooltip(getMessage(MaximizeActionControl.z_ACTION_MAXIMIZE));
                } else {
                    this.onCollapse();
                    maximize.css('background-image', 'url(' + View.contextPath + this.iconMinimize + ')');
                    maximize.data('isMaximize', true);
                    this.panel.actions.get('maximize').setTooltip(getMessage(MaximizeActionControl.z_ACTION_MINIMIZE));
                }
            } else {
                var viewController = View.controllers.get(this.config.viewController);
                if (viewController) {
                    View.getOpenerView().openDialog(this.config.viewName, null, false, {
                        assetParameters: viewController.assetParameters,
                        maximize: true
                    });
                }
            }
        },
        onExpand: function () {
            var nestedLayout = View.getOpenerView().getLayoutManager('nestedLayout');
            nestedLayout.expandRegion('west');
            nestedLayout.recalculateLayout();
            var detailsLayout = View.getOpenerView().getLayoutManager('detailsLayout');
            _.each(detailsLayout.regions, function (region) {
                if (region.region !== View.parentViewPanel.region && detailsLayout.isRegionCollapsed(region.region) && region.toExpand) {
                    detailsLayout.expandRegion(region.region);
                }
            });
            detailsLayout.recalculateLayout();
        },
        onCollapse: function () {
            var nestedLayout = View.getOpenerView().getLayoutManager('nestedLayout');
            nestedLayout.collapseRegion('west');
            nestedLayout.recalculateLayout();
            var detailsLayout = View.getOpenerView().getLayoutManager('detailsLayout');
            _.each(detailsLayout.regions, function (region) {
                region.toExpand = !detailsLayout.isRegionCollapsed(region.region);
                if (region.region !== View.parentViewPanel.region && !detailsLayout.isRegionCollapsed(region.region)) {
                    detailsLayout.collapseRegion(region.region);
                }
            });
            detailsLayout.recalculateLayout();
        }
    },
    {
        z_ACTION_MAXIMIZE: 'maximizeActionMaximize',
        z_ACTION_MINIMIZE: 'maximizeActionMinimize',
        z_ACTION_NEW_WINDOW: 'maximizeActionNewWindow'
    }
);