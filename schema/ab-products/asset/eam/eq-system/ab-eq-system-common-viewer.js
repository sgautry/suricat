var ViewerControl = Base.extend({
        panel: null,
        selector: null,
        constructor: function (panelName, selector) {
            this.panelName = panelName;
            this.selector = selector;
            var panel = View.panels.get(panelName);
            panel.afterLayout = function () {
                var layoutManager = View.getLayoutManager('mainLayout');
                var regionPanel = layoutManager.getRegionPanel('north');
                var contentWidth = Ext.get(document.getElementById('viewerPanel_title')).parent().parent().getWidth();
                var regionPanelWidth = regionPanel.el.getWidth();
                if (contentWidth >= regionPanelWidth) {
                    layoutManager.setRegionSize('north', 48);
                } else {
                    layoutManager.setRegionSize('north', regionPanel.initialConfig.height);
                }
                layoutManager.updateScroller(regionPanel);
            };
            this.init();
        },

        init: function () {
            var divId = document.getElementById(this.panelName + '_title');
            if (this.selector) {
                var title = this.selector.title;
                if (!valueExistsNotEmpty(title)) {
                    title = ViewerControl.z_VIEWER_TITLE + ':';
                }
                this.appendSelector(divId, 'viewer', title, this.selector.views, this.onChangeViewer.createDelegate(this));
            }
        },
        appendSelector: function (div, comboId, prompt, options, callback) {
            if (div === null) {
                return;
            }
            var pn = div.parentNode.parentNode;
            var cell = Ext.DomHelper.append(pn, {tag: 'td'});
            var tn = Ext.DomHelper.append(cell, '<p>' + prompt + '</p>', true);
            Ext.DomHelper.applyStyles(tn, "x-btn-text");
            cell = Ext.DomHelper.append(pn, {tag: 'td'});
            var combo = Ext.DomHelper.append(cell, {tag: 'select', id: 'selector_' + comboId}, true);
            if (options && options.length > 0) {
                for ( var i = 0; i < options.length; i++ ) {
                    var option = new Option(options[i].title, options[i].viewName);
                    if (options[i].selected) {
                        option.selected = true;
                    }
                    combo.dom.options[i] = option;
                }
            }
            combo.dom.onchange = callback.createDelegate(this, [combo]);
        },
        onChangeViewer: function (viewerSelector) {
            var viewName = viewerSelector.getValue();
            var frame = View.parentViewPanel;
            if (frame) {
                frame.loadView(viewName, null, null);
            } else {
                window.location = viewName;
            }
        }
    },
    {
        z_VIEWER_TITLE: 'Viewer'
    }
);