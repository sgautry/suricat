/**
 * Drawing controller.
 */
View.createController('eqSysDrawingController', {
    // drawing controller
    drawingController: null,
    // drawing svg id
    drawingSvgId: null,
    // passed asset parameters
    assetParameters: null,
    // selected equipment id
    selectedEquipment: null,
    // selected action
    action: null,
    // trace type - default none
    traceType: 'none',
    // show hierarchy trees - default false
    showHierarchyTrees: false,
    // fly mode dependents - default false
    flyModeDependents: false,
    // fly mode dependencies - default false
    flyModeDependencies: false,
    afterViewLoad: function () {
        // init main viewer
        this.initViewer();
        // init drawing controller
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['rm', 'eq', 'jk', 'fp', 'pn'],
            topLayers: 'eq',
            highlightParameters: [{
                'view_file': "ab-eq-system-drawing.axvw",
                'hs_ds': "highlightNoneDs",
                'label_ds': 'labelNoneDs',
                'label_ht': '0.50',
                showSelector: false,
                panelId: 'viewerPanel'
            }],
            layersPopup: {
                layers: "rm-assets;rm-labels;eq-assets;eq-labels;jk-assets;jk-labels;fp-assets;fp-labels;pn-assets;pn-labels;gros-assets;gros-labels;background",
                defaultLayers: "rm-assets;rm-labels;eq-assets;gros-assets",
                collapsed: true
            },
            assetTooltip: [
                {assetType: 'rm', datasource: 'label_rm_ds', fields: 'rm.rm_id;rm.rm_std'},
                {assetType: 'eq', datasource: 'label_eq_ds', fields: 'eq.eq_id;eq.eq_std;'},
                {assetType: 'jk', datasource: 'label_jk_ds', fields: 'jk.jk_id;jk.jk_std'},
                {assetType: 'fp', datasource: 'label_fp_ds', fields: 'fp.fp_id;fp.fp_std'},
                {assetType: 'pn', datasource: 'label_pn_ds', fields: 'pn.pn_id;pn.pn_std'}
            ],
            actions: this.getContextMenuActions(),
            showPanelTitle: true,
            selectAssetHighlightColor: EqSysColorConfig['selectAssetHighlightColor'],
            listeners: {
                afterDrawingLoaded: this.afterDrawingLoadedListener,
                customPanelTitle: this.setDrawingPanelTitle,
                onClickAssetBefore: this.onClickAssetBeforeListener.createDelegate(this),
                onClickAssetAfter: this.onClickAssetAfterListener.createDelegate(this)
            }
        });
        // init viewer actions
        this.initActions();
        // init maximize action
        this.initMaximizeAction();
        // init hierarchy trees
        this.initHierarchyTrees();
    },
    /**
     * Load drawing by asset parameters.
     */
    afterInitialDataFetch: function () {
        // hide the drawing panel toolbar title
        this.drawingPanel.toolbar.hideParent = true;
        this.drawingPanel.toolbar.setVisible(false);
        // init asset parameters
        var parameters = null;
        if (valueExists(this.view.parameters) && valueExists(this.view.parameters.assetParameters)) {
            parameters = this.view.parameters.assetParameters;
        } else if (valueExists(this.view.parentTab) && valueExists(this.view.parentTab.assetParameters)) {
            parameters = this.view.parentTab.parameters.assetParameters;
        } else if (valueExists(this.view.parentViewPanel) && valueExists(this.view.parentViewPanel.assetParameters)) {
            parameters = this.view.parentViewPanel.assetParameters;
        }
        if (valueExists(parameters)) {
            this.refreshDrawing(parameters);
        }
    },
    /**
     * Refreshes the drawing with the selected equipment.
     * Find and zoom to equipment room and highlight the equipment.
     * @param {Ab.view.ConfigObject} parameters - asset parameters.
     */
    refreshDrawing: function (parameters) {
        this.assetParameters = parameters;
        this.action = parameters.getConfigParameter('action');
        this.selectedEquipment = parameters.getConfigParameterIfExists('eqId');
        this.drawingController.showDrawing({
            pkeyValues: {blId: parameters.getConfigParameter('blId'), flId: parameters.getConfigParameter('flId')},
            findAssets: [{
                assetType: 'rm',
                assetIds: [parameters.getConfigParameter('rmId')],
                zoom: parameters.getConfigParameter('zoom', true)
            }],
            highlightAssets: [{
                assetType: 'eq',
                assetIds: [this.selectedEquipment],
                persistFill: false,
                color: EqSysColorConfig['selectAssetHighlightColor']
            }]
        });
        if (!_.isEmpty(this.drawingController.drawings)) {
            this.showMissingHighlightAssets();
            // refresh hierarchy trees and resets fly mode if was enabled
            this.refreshHierarchyTrees(true);
            if (valueExists(this.action) && 'locate' !== this.action) {
                // apply action on selected equipment from the profile view
                this.applyAction(this.action);
            } else {
                // trace equipment system for the selected equipment
                this.traceEquipmentSystem(this.selectedEquipment);
            }
        }
    },
    /**
     * Init viewer.
     */
    initViewer: function () {
        var views = [];
        views.push({
            viewName: 'ab-eq-system-drawing.axvw',
            title: getMessage('viewerDrawingTitle'),
            selected: true
        });
        if (isEnterpriseGraphicsSchemaDefined) {
            views.push({
                viewName: 'ab-eq-system-bim-3d.axvw',
                title: getMessage('viewer3dTitle'),
                show: false
            });
        }
        views.push({
            viewName: 'ab-eq-system-360.axvw',
            title: getMessage('viewer360Title')
        });
        new ViewerControl('viewerPanel', {
            title: getMessage('viewerTitle'),
            views: views
        });
    },
    /**
     * Init viewer actions.
     */
    initActions: function () {
        this.initTraceActions();
        this.initViewerActions();
    },
    /**
     * Init trace actions.
     */
    initTraceActions: function () {
        this.addAction('viewerPanel', {
            id: 'traceActions',
            tooltip: getMessage('actionsTraceTooltip'),
            icon: '/schema/ab-core/graphics/icons/view/trace.png',
            type: 'menu'
        });
        var actions = [];
        actions.push({
            text: getMessage('actionTraceNone'),
            checked: true,
            group: 'trace',
            handler: this.onTraceCurrentEquipment.createDelegate(this, ['none', true])
        }, {
            text: getMessage('actionTraceEquipmentSystem'),
            checked: false,
            group: 'trace',
            handler: this.onTraceCurrentEquipment.createDelegate(this, ['system', true])
        }, {
            text: getMessage('actionTraceDependents'),
            checked: false,
            group: 'trace',
            handler: this.onTraceCurrentEquipment.createDelegate(this, ['dependents', true])
        }, {
            text: getMessage('actionTraceDependencies'),
            checked: false,
            group: 'trace',
            handler: this.onTraceCurrentEquipment.createDelegate(this, ['dependencies', true])
        });
        actions.push({type: 'separator'});
        actions.push({
            text: getMessage('actionHierarchyTrees'),
            checked: false,
            handler: this.onShowHierarchyTrees.createDelegate(this)
        });
        var menuAction = this.viewerPanel.actions.get('traceActions');
        for ( var i = 0; i < actions.length; i++ ) {
            if (actions[i].type && 'separator' === actions[i].type) {
                menuAction.menu.addSeparator();
            } else {
                menuAction.menu.addMenuItem(actions[i]);
            }
        }
    },
    /**
     * Init viewer tools actions.
     */
    initViewerActions: function () {
        this.addAction('viewerPanel', {
            id: 'viewerActions',
            tooltip: getMessage('actionsTooltip'),
            icon: '/schema/ab-core/graphics/icons/view/gear.png',
            type: 'menu'
        });
        var actions = [];
        actions.push({
            text: getMessage('actionLocateEquipment'),
            handler: this.onLocateEquipment.createDelegate(this)
        }, {
            text: getMessage('actionHighlightRoomsServed'),
            handler: this.onHighlightRoomsServed.createDelegate(this, [], false)
        });
        actions.push({type: 'separator'});
        actions.push({
            text: getMessage('actionGeneratePDF'),
            handler: this.onGeneratePdf.createDelegate(this)
        });
        var menuAction = this.viewerPanel.actions.get('viewerActions');
        for ( var i = 0; i < actions.length; i++ ) {
            if (actions[i].type && 'separator' === actions[i].type) {
                menuAction.menu.addSeparator();
            } else {
                menuAction.menu.addMenuItem(actions[i]);
            }
        }
        // update viewer scroll
        this.viewerPanel.afterLayout();
    },
    /**
     * Get context menu drawing actions.
     * @returns {Array} of actions.
     */
    getContextMenuActions: function () {
        var actions = [];
        actions.push({
            text: getMessage("actionHighlightRoomsServed"),
            id: 'highlight',
            visible: function (assetType) {
                return 'eq' === assetType;
            },
            listener: this.onHighlightRoomsCurrentEquipment.createDelegate(this)
        });
        actions.push({
            text: getMessage("actionTraceEquipmentSystem"),
            id: 'traceSystem',
            visible: function (assetType) {
                return 'eq' === assetType;
            },
            listener: this.onTraceCurrentEquipment.createDelegate(this, ['system'])
        });
        actions.push({
            text: getMessage("actionTraceDependents"),
            id: 'traceDepend',
            visible: function (assetType) {
                return 'eq' === assetType;
            },
            listener: this.onTraceCurrentEquipment.createDelegate(this, ['dependents'])
        });
        actions.push({
            text: getMessage("actionTraceDependencies"),
            id: 'traceDependency',
            visible: function (assetType) {
                return 'eq' === assetType;
            },
            listener: this.onTraceCurrentEquipment.createDelegate(this, ['dependencies'])
        });
        actions.push({
            text: getMessage("actionViewProfile"),
            id: 'profile',
            visible: function (assetType) {
                return 'eq' === assetType;
            },
            listener: this.onViewProfile.createDelegate(this)
        });
        actions.push({
            text: getMessage("actionAssignRooms"),
            id: 'assign',
            visible: function (assetType) {
                return 'rm' === assetType;
            },
            listener: this.onAssignRoomsToEquipment.createDelegate(this)
        });
        return actions;
    },
    /**
     * Init maximize action.
     */
    initMaximizeAction: function () {
        new MaximizeActionControl('viewerPanel', {
            viewController: 'eqSysDrawingController',
            viewName: 'ab-eq-system-drawing.axvw'
        });
    },
    /**
     * Init hierarchy trees panels.
     */
    initHierarchyTrees: function () {
        this.eqSysDependencyTreePanel.updateRestrictionForLevel = function (parentNode, level, restriction) {
            if (level > 0) {
                restriction.removeClause('eq_system.auto_number');
                restriction.addClause('eq_system.eq_id_master', parentNode.data['eq_system.eq_id_depend'], '=');
            }
        };
        this.eqSysDependentTreePanel.updateRestrictionForLevel = function (parentNode, level, restriction) {
            if (level > 0) {
                restriction.removeClause('eq_system.auto_number');
                restriction.addClause('eq_system.eq_id_master', parentNode.data['eq_system.eq_id_depend'], '=');
            }
        };
    },
    /**
     * After drawing is loaded, set the drawing svgId.
     * @param {AssetDrawingControl} drawingController.
     * @param {string} svgId - drawing svgId.
     */
    afterDrawingLoadedListener: function (drawingController, svgId) {
        View.controllers.get('eqSysDrawingController').drawingSvgId = svgId;
    },
    /**
     * Set drawing panel title.
     * @param {AssetDrawingControl} drawingController.
     * @param {Object} drawings - map of drawings.
     */
    setDrawingPanelTitle: function (drawingController, drawings) {
        var dwgPanelTitle = getMessage('noDrawingFound');
        var countDrawings = _.keys(drawings).length;
        if (countDrawings > 0) {
            var pkeyValues = _.values(drawings)[0].pkeyValues;
            if (_.keys(drawings).length == 1) {
                dwgPanelTitle = pkeyValues.blId + ' ' + pkeyValues.flId;
            } else if (_.keys(drawings).length > 1) {
                dwgPanelTitle = pkeyValues.blId;
            }
        }
        View.panels.get('viewerPanel').setTitle(dwgPanelTitle);
    },
    /**
     * Called before an asset is clicked and resets the drawing highlights.
     * @param {AssetDrawingControl} drawingController.
     */
    onClickAssetBeforeListener: function (drawingController) {
        drawingController.resetDrawingHighlights();
        drawingController.svgControl.getAddOn('InfoWindow').show(false);
    },
    /**
     * Called after an asset is selected. If asset is equipment, sets the selectedEquipment and trace it.
     * @param {AssetDrawingControl} drawingController.
     * @param {string} svgId - drawing svgId.
     * @param {string} selectedAssetId - selected asset id.
     * @param {string} assetType - selected asset type.
     * @param {Object} selectedAssets - map of selected objects.
     * @param {boolean} selected - asset is selected
     */
    onClickAssetAfterListener: function (drawingController, svgId, selectedAssetId, assetType, selectedAssets, selected) {
        if ('eq' === assetType && selected) {
            this.selectedEquipment = selectedAssetId;
            this.onTrace(selectedAssetId, false);
            this.refreshHierarchyTrees();
        }
    },
    /**
     *  Refresh drawing by location.
     *  Called from profile view.
     */
    refreshDrawingLocation: function () {
        if (this.view.parentViewPanel && this.view.parentViewPanel.assetParameters) {
            this.refreshDrawing(this.view.parentViewPanel.assetParameters);
        }
    },
    /**
     * Trace current equipment.
     * @param {string} traceType - Trace type. none|system|dependents|dependencies
     * @param {boolean} applyTraceType - Apply trace type when called from viewer trace action.
     */
    onTraceCurrentEquipment: function (traceType, applyTraceType) {
        if (applyTraceType) {
            this.traceType = traceType;
        }
        this.onTrace(this.selectedEquipment, true, traceType);
    },
    /**
     * Trace equipment.
     * @param {string} eqId - Equipment id.
     * @param {boolean} [refreshHierarchyTrees] - true to refresh the hierarchy tree panels
     * @param {string} [traceType] - Trace type.
     */
    onTrace: function (eqId, refreshHierarchyTrees, traceType) {
        if (refreshHierarchyTrees) {
            this.refreshHierarchyTrees(true);
        }
        this.traceEquipmentSystem(eqId, traceType);
    },
    /**
     * Trace equipment system based on trace type.
     * @param {string} eqId - Equipment id.
     * @param {string} [traceType] - Trace type.
     */
    traceEquipmentSystem: function (eqId, traceType) {
        traceType = traceType || this.traceType;
        if ('system' === traceType) {
            this.traceDepend(eqId, false);
            this.traceDependency(eqId, false);
            this.highlightEquipment(eqId, true, true, true);
            if (!this.showMissingTraceAssets()) {
                this.drawingController.svgControl.getAddOn('InfoWindow').setText(String.format(getMessage('afterTraceSystem'), eqId));
            }
        } else if ('dependents' === traceType) {
            this.traceDepend(eqId, true);
        } else if ('dependencies' === traceType) {
            this.traceDependency(eqId, true);
        }
    },
    /**
     * Viewer menu action to locate selected inventory tree equipment.
     */
    onLocateEquipment: function () {
        this.refreshDrawingLocation();
    },
    /**
     * Viewer menu action to highlight rooms served for selected inventory tree equipment.
     */
    onHighlightRoomsServed: function () {
        var eqId = this.assetParameters.getConfigParameter('eqId');
        this.onHighlightRooms(eqId);
    },
    /**
     * Highlight rooms served for selected equipment.
     */
    onHighlightRoomsCurrentEquipment: function () {
        this.onHighlightRooms(this.selectedEquipment);
    },
    /**
     * Viewer menu action to display hierarchy tree panels.
     * @param {Object} action - Checkbox object.
     */
    onShowHierarchyTrees: function (action) {
        this.showHierarchyTrees = !action.checked;
        var layout = View.getLayoutManager('nestedLayout');
        if (this.showHierarchyTrees) {
            layout.expandRegion('east');
        } else {
            layout.collapseRegion('east');
        }
        this.refreshHierarchyTrees();
    },
    /**
     * Refresh hierarchy trees by selected equipment.
     * @param {boolean} [resetFlyMode] - true to reset fly mode.
     */
    refreshHierarchyTrees: function (resetFlyMode) {
        this.eqSysDependencyTreePanel.addParameter("lvlDependRestriction", this.selectedEquipment);
        this.eqSysDependencyTreePanel.refresh();
        this.eqSysDependencyTreePanel.expandAll();
        this.eqSysDependentTreePanel.addParameter("lvlMasterRestriction", "eq_system.eq_id_depend='" + this.selectedEquipment + "'");
        this.eqSysDependentTreePanel.refresh();
        this.eqSysDependentTreePanel.expandAll();
        if (resetFlyMode) {
            this.resetFlyMode();
        }
    },
    /**
     * Show missing highlight assets on the drawing info window.
     */
    showMissingHighlightAssets: function () {
        var missingAssets = this.drawingController.svgControl.getController("HighlightController").getMissingAssets('highlight');
        var missingAssetsMsg = getMessage('equipmentNotFound') + ' ';
        var hasMissingAssets = false;
        if (missingAssets.assets.length > 0) {
            missingAssetsMsg += '[' + missingAssets.assets.toString() + ']';
            hasMissingAssets = true;
        }
        if (hasMissingAssets) {
            this.drawingController.svgControl.getDrawingController().getAddOn('InfoWindow').appendText(missingAssetsMsg);
            this.drawingController.svgControl.getDrawingController().getController("HighlightController").resetMissingAssets('highlight');
        } else {
            this.drawingController.svgControl.getDrawingController().getAddOn('InfoWindow').show(false);
        }
    },
    /**
     * Called from profile view if applies profile view actions.
     * @param {string} action - Action to apply. highlight|dependents|dependencies
     */
    applyAction: function (action) {
        if ('highlight' === action) {
            this.onHighlightRooms(this.selectedEquipment);
        } else if ('dependents' === action) {
            this.traceDepend(this.selectedEquipment, true);
        } else if ('dependencies' === action) {
            this.traceDependency(this.selectedEquipment, true);
        }
    },
    /**
     * Viewer menu action to generate PDF export of the drawing.
     */
    onGeneratePdf: function () {
        var drawingName = _.values(this.drawingController.drawings)[0].drawingName;
        var pdfParameters = {};
        pdfParameters.documentTemplate = "report-cadplan-imperial-landscape-17x22.docx";
        pdfParameters.drawingName = drawingName;
        pdfParameters.plan_type = "1 - ALLOCATION";
        pdfParameters.drawingZoomInfo = {};
        pdfParameters.drawingZoomInfo.image = this.drawingController.svgControl.getImageBytes(drawingName);
        pdfParameters.showLegend = true;
        pdfParameters.symbolsHandler = "com.archibus.app.solution.common.report.docx.SymbolsHandlerExample";
        var jobId = Workflow.startJob('AbSystemAdministration-generatePaginatedReport-buildDocxFromView', 'ab-ex-dwg-rpt-pdf.axvw', null, pdfParameters, null);
        View.openJobProgressBar(getMessage('actionGeneratePDFMessage'), jobId, null, function (status) {
            var url = status.jobFile.url;
            window.open(url);
        });
    },
    /**
     * Highlight rooms on drawing.
     * @param {string} selectedEquipment - Selected equipment id.
     */
    onHighlightRooms: function (selectedEquipment) {
        this.drawingController.resetDrawingHighlights();
        var dataSource = View.dataSources.get('servedRooms_ds');
        dataSource.addParameter('levelRestriction', "level1='" + selectedEquipment + "'");
        dataSource.addParameter('eqRestriction', "eq.eq_id='" + selectedEquipment + "'");
        var records = View.dataSources.get('servedRooms_ds').getRecords();
        for ( var i = 0; i < records.length; i++ ) {
            var blId = records[i].getValue('eq.bl_id');
            var flId = records[i].getValue('eq.fl_id');
            var rmId = records[i].getValue('eq.rm_id');
            if (valueExistsNotEmpty(rmId)) {
                var locator = blId + ';' + flId + ';' + rmId;
                this.drawingController.svgControl.getController("HighlightController").highlightAsset(locator, {
                    svgId: this.drawingSvgId,
                    color: EqSysColorConfig['highlightRoomColor'],
                    persistFill: true,
                    overwriteFill: true
                });
            }
        }
        this.highlightEquipment(selectedEquipment, true, true, false);
        this.drawingController.svgControl.getAddOn('InfoWindow').setText(String.format(getMessage('afterHighlightRooms'), this.selectedEquipment));
    },
    /**
     * Assign selected rooms for selected inventory tree equipment.
     */
    onAssignRoomsToEquipment: function () {
        var eqId = this.assetParameters.getConfigParameter('eqId');
        var dataSource = View.dataSources.get('abEqRm_ds'),
            selectedRooms = this.drawingController.selectedAssets[this.drawingSvgId]['rm'],
            drawing = this.drawingController.drawings[this.drawingSvgId];
        var pkeyValues = drawing.pkeyValues;
        var blId = pkeyValues.blId,
            flId = pkeyValues.flId;
        var bl_fl = blId + View.preferences.hierarchy_delim + flId;
        for ( var i = 0; i < selectedRooms.length; i++ ) {
            var rmId = selectedRooms[i].split(';')[2];
            var bl_fl_rm = bl_fl + View.preferences.hierarchy_delim + rmId;
            var restriction = new Ab.view.Restriction();
            restriction.addClause('eq_rm.eq_id', eqId);
            restriction.addClause('eq_rm.bl_fl_rm', bl_fl_rm);
            var record = dataSource.getRecord(restriction);
            if (record.isNew) {
                record.setValue('eq_rm.eq_id', eqId);
                record.setValue('eq_rm.bl_fl_rm', bl_fl_rm);
                record.setValue('eq_rm.bl_id', blId);
                record.setValue('eq_rm.fl_id', flId);
                record.setValue('eq_rm.rm_id', rmId);
                dataSource.saveRecord(record);
            }
        }
        this.drawingController.svgControl.getAddOn('InfoWindow').setText(String.format(getMessage('afterAssignedRooms'), eqId));
    },
    /**
     * View equipment profile.
     */
    onViewProfile: function () {
        var opener = View;
        if (!valueExists(View.getOpenerView().dialog)) {
            opener = View.getOpenerView();
        }
        opener.openDialog('ab-profile-equipment.axvw', new Ab.view.Restriction({'eq.eq_id': this.selectedEquipment}), false, {
            width: 1024,
            height: 800,
            closeButton: true
        });
    },
    /**
     * Trace depend system equipment.
     * @param {string} selectedEquipment - Selected equipment.
     * @param {boolean} highlightEquipment - true to highlight equipment.
     */
    traceDepend: function (selectedEquipment, highlightEquipment) {
        if (highlightEquipment) {
            this.drawingController.resetDrawingHighlights();
        }
        var dataSource = View.dataSources.get('traceDepend_ds');
        dataSource.clearParameters();
        dataSource.addParameter('eqDependId', selectedEquipment);
        var records = dataSource.getRecords();
        for ( var i = 0; i < records.length; i++ ) {
            for ( var levelIndex = 1; levelIndex < 10; levelIndex++ ) {
                var assetFrom = records[i].getValue('eq_system.level' + levelIndex),
                    assetTo = records[i].getValue('eq_system.level' + (levelIndex + 1));
                if (valueExistsNotEmpty(assetFrom) && valueExistsNotEmpty(assetTo)) {
                    this.trace(assetFrom, assetTo, EqSysColorConfig['highlightDependColor']);
                }
            }
        }
        if (highlightEquipment) {
            this.highlightEquipment(selectedEquipment, true, true, true);
            if (!this.showMissingTraceAssets()) {
                this.drawingController.svgControl.getAddOn('InfoWindow').setText(String.format(getMessage('afterTraceDepend'), selectedEquipment));
            }
        }
    },
    /**
     * Trace dependency system equipment.
     * @param {string} selectedEquipment - Selected equipment.
     * @param {boolean} highlightEquipment - true to highlight equipment.
     */
    traceDependency: function (selectedEquipment, highlightEquipment) {
        if (highlightEquipment) {
            this.drawingController.resetDrawingHighlights();
        }
        var dataSource = View.dataSources.get('traceDependency_ds');
        dataSource.clearParameters();
        dataSource.addParameter('eqDependId', selectedEquipment);
        var records = dataSource.getRecords();
        for ( var i = 0; i < records.length; i++ ) {
            for ( var levelIndex = 8; levelIndex > 0; levelIndex-- ) {
                var assetFrom = records[i].getValue('eq_system.level' + levelIndex),
                    assetTo = records[i].getValue('eq_system.level' + (levelIndex + 1));
                if (valueExistsNotEmpty(assetFrom) && valueExistsNotEmpty(assetTo)) {
                    this.trace(assetFrom, assetTo, EqSysColorConfig['highlightDependencyColor']);
                }
            }
        }
        if (highlightEquipment) {
            this.highlightEquipment(selectedEquipment, true, true, true);
            if (!this.showMissingTraceAssets()) {
                this.drawingController.svgControl.getAddOn('InfoWindow').setText(String.format(getMessage('afterTraceDependency'), selectedEquipment));
            }
        }
    },
    /**
     * Draw trace on drawing.
     * @param {string} assetFrom - Asset from id.
     * @param {string} assetTo - Asset to id.
     * @param {string} color - Trace color.
     */
    trace: function (assetFrom, assetTo, color) {
        this.drawingController.svgControl.getController("HighlightController").traceAssets(this.drawingSvgId, assetFrom, assetTo, color);
    },
    /**
     * Show missing trace assets.
     */
    showMissingTraceAssets: function () {
        var missingAssets = this.drawingController.svgControl.getController("HighlightController").getMissingAssets('trace');
        var missingAssetsMsg = getMessage('assetsNotFound');
        var hasError = false;
        if (missingAssets.assetFrom.length > 0) {
            missingAssetsMsg += '<br>' + getMessage('assetsNotFoundFrom') + ': ' + missingAssets.assetFrom.join(', ');
            hasError = true;
        }
        if (missingAssets.assetTo.length > 0) {
            missingAssetsMsg += '<br>' + getMessage('assetsNotFoundTo') + ': ' + missingAssets.assetTo.join(', ');
            hasError = true;
        }
        if (hasError) {
            this.drawingController.svgControl.getAddOn('InfoWindow').setText('<br>' + missingAssetsMsg);
            this.drawingController.svgControl.getController("HighlightController").resetMissingAssets('trace');
        }
        return hasError;
    },
    /**
     * Called after action is done to highlight selected equipment.
     * @param {string} selectedEquipment - Selected equipment.
     * @param {boolean} persistFill:   Can fill be changed by mouseover event? true - mouseover can not overwrite it, false otherwise.
     * @param {boolean} overwriteFill: will overwrite the existing color in highlight/select? true if yes, false otherwise.
     * @param {boolean} ignorePersistFill - true so that the mouse event will not clear the specified asset highlight
     */
    highlightEquipment: function (selectedEquipment, persistFill, overwriteFill, ignorePersistFill) {
        this.drawingController.svgControl.getController("HighlightController").highlightAsset(selectedEquipment, {
            svgId: this.drawingSvgId,
            color: EqSysColorConfig['selectAssetHighlightColor'],
            persistFill: persistFill,
            overwriteFill: overwriteFill,
            ignorePersistFill: ignorePersistFill
        });
    },
    /**
     * Selects equipment. Called from fly over mode.
     * @param {string} selectedEquipment - Selected equipment.
     */
    selectEquipment: function (selectedEquipment) {
        this.drawingController.svgControl.getController("SelectController").toggleAssetSelection(
            this.drawingSvgId,
            selectedEquipment + '-rect',
            EqSysColorConfig['selectAssetHighlightColor']
        );
    },
    /**
     * Enable/disable fly mode.
     * @param {string} type - flyModeDependents|flyModeDependencies
     */
    enableFlyMode: function (type) {
        if ('flyModeDependents' === type) {
            if (this.flyModeDependents) {
                this.addFlyControl('eqSysDependentTreePanel');
            } else {
                this.removeFlyControl('eqSysDependentTreePanel');
            }
        }
        if ('flyModeDependencies' === type) {
            if (this.flyModeDependencies) {
                this.addFlyControl('eqSysDependencyTreePanel');
            } else {
                this.removeFlyControl('eqSysDependencyTreePanel');
            }
        }
    },
    /**
     * Add hover control for a specific tree panel.
     * @param {string} treePanel - Tree panel id.
     */
    addFlyControl: function (treePanel) {
        var scope = this;
        var addHoverControl = function (objTree, node) {
            var data = node.data;
            var eqId = data['eq_system.eq_id_depend'];
            jQuery('#' + node.labelElId).hover(
                function () {
                    scope.selectEquipment(eqId);
                }
            );
            if (node.hasChildren()) {
                for ( var i = 0; i < node.children.length; i++ ) {
                    addHoverControl(objTree, node.children[i]);
                }
            }
        };
        var objTree = View.panels.get(treePanel);
        for ( var nodeCounter = 0; nodeCounter < objTree._nodes.length; nodeCounter++ ) {
            addHoverControl(objTree, objTree._nodes[nodeCounter], nodeCounter);
        }
    },
    /**
     * Removes hover control for a specific tree panel.
     * @param {string} treePanel - Tree panel id.
     */
    removeFlyControl: function (treePanel) {
        var removeHoverControl = function (objTree, node) {
            jQuery('#' + node.labelElId).unbind('mouseenter mouseleave');
            if (node.hasChildren()) {
                for ( var i = 0; i < node.children.length; i++ ) {
                    removeHoverControl(objTree, node.children[i]);
                }
            }
        };
        var objTree = View.panels.get(treePanel);
        for ( var nodeCounter = 0; nodeCounter < objTree._nodes.length; nodeCounter++ ) {
            removeHoverControl(objTree, objTree._nodes[nodeCounter]);
        }
    },
    /**
     * Resets fly mode to false if already exists.
     */
    resetFlyMode: function () {
        this.flyModeDependents = false;
        this.flyModeDependencies = false;
        var actionMenu = View.panels.get('eqSysDependentTreePanel').actions.get('toolsDependentMenu');
        var flyModeDependent = actionMenu.menu.items.get('flyModeDependent');
        if (flyModeDependent.checked) {
            flyModeDependent.setChecked(false);
            this.enableFlyMode('flyModeDependents');
        }
        actionMenu = View.panels.get('eqSysDependencyTreePanel').actions.get('toolsDependencyMenu');
        var flyModeDependency = actionMenu.menu.items.get('flyModeDependency');
        if (flyModeDependency.checked) {
            flyModeDependency.setChecked(false);
            this.enableFlyMode('flyModeDependencies');
        }
    },
    /**
     * Add action to a panel.
     * @param {string} panelId - Panel id.
     * @param {Object} config - Action configuration.
     */
    addAction: function (panelId, config) {
        var panel = View.panels.get(panelId);
        if (config == null) {
            return;
        }
        if (panel.actions.get(config.id) != undefined) {
            panel.actions.get(config.id).setTitle(config.text);
        } else {
            panel.actions.add(config.id, new Ab.view.Action(panel, config));
        }
    }
});
/**
 * Set fly mode.
 * @param {Object} action - Checkbox object.
 * @param {string} type - flyModeDependents|flyModeDependencies
 */
function setFlyMode(action, type) {
    var controller = View.controllers.get('eqSysDrawingController');
    controller[type] = action.checked;
    controller.enableFlyMode(type);
}
/**
 * On click dependency node handler event.
 * @param {Object} context - tree panel context.
 */
function onClickDependencyNodeHandler(context) {
    var controller = View.controllers.get('eqSysDrawingController');
    var crtTreeNode = View.panels.get(context.command.parentPanelId).lastNodeClicked;
    var eqId = crtTreeNode.data['eq_system.eq_id_depend'];
    controller.selectEquipment(eqId);
}
/**
 * On click dependent node handler event.
 * @param {Object} context - tree panel context.
 */
function onClickDependentNodeHandler(context) {
    var controller = View.controllers.get('eqSysDrawingController');
    var crtTreeNode = View.panels.get(context.command.parentPanelId).lastNodeClicked;
    var eqId = crtTreeNode.data['eq_system.eq_id_depend'];
    controller.selectEquipment(eqId);
}
/**
 * Set node label.
 * @param {Object} node - Tree node.
 */
function afterGeneratingTreeNode(node) {
    var depend = node.data['eq_system.eq_id_depend'];
    var systemLvl = node.data['eq_system.system_level'];
    if (node.data['eq_system.system_level.raw']) {
        systemLvl = node.data['eq_system.system_level.raw'];
    }
    var label = node.data['eq_system.system_name'] + ' (' + depend + ')';
    var nodeLabel = '<div class="nodeLabel">';
    var labelId = 'label' + node.id + '_0',
        labelClass = 'label color_' + systemLvl;
    var controller = View.controllers.get('eqSysDrawingController');
    if (controller && valueExistsNotEmpty(controller.selectedEquipment)) {
        if (depend === controller.selectedEquipment) {
            labelClass += ' highlight';
        }
    }
    nodeLabel += '<div id="' + labelId + '" class="' + labelClass + '"><span class="ygtvlabel">' + label + '</span></div>';
    nodeLabel += '</div>';
    node.label = nodeLabel;
}
/**
 *  Enables panel toolbar menu separator.
 */
Ext.menu.Separator = function (config) {
    Ext.menu.Separator.superclass.constructor.call(this, config);
};
Ext.extend(Ext.menu.Separator, Ext.menu.BaseItem, {
    itemCls: "x-menu-sep",
    hideOnClick: false,
    onRender: function (li) {
        var s = document.createElement("span");
        s.className = this.itemCls;
        s.innerHTML = "&#160;";
        this.el = s;
        li.addClass("x-menu-sep-li");
        Ext.menu.Separator.superclass.onRender.apply(this, arguments);
    }
});