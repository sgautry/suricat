View.createController('defOrgCtrl', {
    afterViewLoad: function(){
        // hide field tooltip if SMS licence is not available
        if (!checkLicense('AbEAMSMSBuilderExtension')) {
            jQuery(this.abDefOrg_detailsPanel.getFieldCell('org.cost_replace')).find('.fieldTooltipButton').hide();
        }
    },
    /**
     *  insert /update  org
     */
    abDefOrg_detailsPanel_onSave: function () {
        var isNew = this.abDefOrg_detailsPanel.newRecord;
        var oldId = this.abDefOrg_detailsPanel.getOldFieldValues()["org.org_id"];
        var newId = this.abDefOrg_detailsPanel.getFieldValue("org.org_id");
        var parentNode = this.getParent(this.abDefOrg_treePanel, this.abDefOrg_detailsPanel, isNew);
        this.setHierId(this.abDefOrg_detailsPanel, parentNode, "org.org_id", "org.hierarchy_ids");
        if (this.abDefOrg_detailsPanel.save()) {
            // update all childs
            if (!isNew) {
                this.updateChildNodes(newId, oldId);
            }
            this.refreshTreeAfterSave(parentNode, newId);
            this.abDefOrg_detailsPanel.show(false, true);
        }
    },
    /**
     * Set new value for hierId field
     * @param {Object} panel
     * @param {Object} parentNode
     * @param {Object} field
     * @param {Object} hierField
     */
    setHierId: function (panel, parentNode, field, hierField) {
        var newId = panel.getFieldValue(field);
        if (parentNode.isRoot()) {
            panel.setFieldValue(hierField, newId + '|');
        } else {
            panel.setFieldValue(hierField, parentNode.data[hierField] + newId + '|');
        }
    },
    /**
     * Update hierId for all child nodes of updated node
     */
    updateChildNodes: function (newId, oldId) {
        var dataSource = this.dsOrg;
        var restriction = "org.hierarchy_ids LIKE '%|" + oldId + "|%'";
        var records = dataSource.getRecords(restriction);
        for ( var i = 0; i < records.length; i++ ) {
            var record = records[i];
            var hierId = record.getValue("org.hierarchy_ids");
            record.setValue("org.hierarchy_ids", hierId.replace("|" + oldId + "|", "|" + newId + "|"));
            record.isNew = false;
            dataSource.saveRecord(record);
        }
    },
    /**
     * get parent for current selection
     * @param {Object} panel
     * @param {Object} editForm
     * @param {Object} isNew
     */
    getParent: function (panel, editForm, isNew) {
        if (panel.lastNodeClicked == null) {
            return panel.treeView.getRoot();
        } else {
            if (isNew) {
                return panel.lastNodeClicked;
            } else {
                var pkFieldValue = editForm.getOldFieldValues()["org.org_id"];
                var node = panel.lastNodeClicked;
                for ( var i = 0; i < panel.lastNodeClicked.children.length; i++ ) {
                    var tmp_node = panel.lastNodeClicked.children[i];
                    if (tmp_node.data["org.org_id"] === pkFieldValue) {
                        node = tmp_node;
                        break;
                    }
                }
                return node.parent;
            }
        }
    },
    /**
     * refresh tree after save
     * @param {Object} parentNode
     */
    refreshTreeAfterSave: function (parentNode, pkFieldValue) {
        var treeView = this.abDefOrg_treePanel;
        if (parentNode.isRoot()) {
            treeView.refresh();
        } else {
            treeView.expandNode(parentNode);
        }
    },
    /**
     * delete org
     */
    abDefOrg_detailsPanel_onDelete: function () {
        var controller = this;
        var dataSource = this.dsOrgDetails;
        var record = this.abDefOrg_detailsPanel.getRecord();
        var primaryFieldValue = record.getValue("org.org_id");
        var parentNode = this.getParent(this.abDefOrg_treePanel, this.abDefOrg_detailsPanel, false);
        var objTree = this.abDefOrg_treePanel;
        if (!primaryFieldValue) {
            return;
        }
        var confirmMessage = getMessage("messageConfirmDelete").replace('{0}', primaryFieldValue);
        View.confirm(confirmMessage, function (button) {
            if (button === 'yes') {
                try {
                    dataSource.deleteRecord(record);
                    if (objTree.lastNodeClicked != null && primaryFieldValue === objTree.lastNodeClicked.data["org.org_id"]) {
                        objTree.lastNodeClicked = null;
                    }
                    controller.abDefOrg_detailsPanel.show(false);
                    controller.refreshTreeAfterSave(parentNode);
                }
                catch (e) {
                    var errMessage = getMessage("errorDelete").replace('{0}', primaryFieldValue);
                    View.showMessage('error', errMessage, e.message, e.data);
                    return;
                }
            }
        })
    }
});

/**
 * remove hierarchy_ids from node label
 * @param {Object} treeNode
 */
function afterGeneratingTreeNode(treeNode) {
    var hierIds = treeNode.data["org.hierarchy_ids"];
    treeNode.label = treeNode.label.replace(hierIds, '');
}

/**
 * Check license.
 * @param {string} licenseId - license to check
 * @returns {boolean} has license enabled
 */
function checkLicense(licenseId) {
    var hasLicense = false;
    AdminService.getProgramLicense({
        async: false,
        callback: function (licenseDTO) {
            for ( var i = 0; i < licenseDTO.licenses.length; i++ ) {
                var license = licenseDTO.licenses[i];
                if (licenseId === license.id) {
                    hasLicense = license.enabled;
                    break;
                }
            }
        },
        errorHandler: function (m, e) {
            View.showException(e);
        }
    });
    return hasLicense;
}
