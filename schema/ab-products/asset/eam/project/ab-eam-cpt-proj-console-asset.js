/**
 * Controller definition
 */
View.createController('abEamCptProjAssetController', {
    // selected project id
    projectId: null,
    // sb_name is project name
    sbName: null,
    sbLevel: null,
    sbType: 'Space Forecast',
    sbRecord: null,
    tmpRecord: null,
    tmpProjectId: null,
    tmpWorkPkgId: null,
    // list of periods from 0 to 12
    periodFields: [],
    afterViewLoad: function () {
        var menuParent = Ext.get('abEamProj_list_reports');
        menuParent.on('click', this.onClickReportMenu, this, null);
        this.initPeriods();
        this.initAssetSummary();
    },
    afterInitialDataFetch: function () {
        // override export methods to customize the report title
        var scope = this;
        function customCallXLSReportJob(title, restriction, parameters) {
            var viewName = this.viewDef.viewName + '.axvw';
            parameters.printRestriction = false;
            return Workflow.startJob(Ab.grid.ReportGrid.WORKFLOW_RULE_XLS_REPORT, viewName, this.dataSourceId, this.title + ': ' + scope.sbName, this.getVisibleFieldDefs(), toJSON(restriction), parameters);
        }
        function customCallDOCXReportJob(title, restriction, parameters) {
            var viewName = this.viewDef.viewName + '.axvw';
            parameters.printRestriction = false;
            return Workflow.startJob(Ab.grid.ReportGrid.WORKFLOW_RULE_DOCX_REPORT, viewName, this.dataSourceId, this.title + ': ' + scope.sbName, this.getVisibleFieldDefs(), toJSON(restriction), parameters);
        }
        this.abEamAssetSbItems.callXLSReportJob = customCallXLSReportJob;
        this.abEamAssetSbItems.callDOCXReportJob = customCallDOCXReportJob;
        this.abEamAssetEqItems.callXLSReportJob = customCallXLSReportJob;
        this.abEamAssetEqItems.callDOCXReportJob = customCallDOCXReportJob;
    },
    /**
     * Always display summary actions without updating the vertical scroller.
     */
    afterLayout: function() {
        var layoutManager = View.getLayoutManager('nestedLayout');
        layoutManager.setRegionSize('north', 43);
        var regionPanel = layoutManager.getRegionPanel('north');
        regionPanel.scroller.scrollbars._prefs.verticalScrolling = false;
    },
    /**
     * Called after action summary panel is refreshed.
     * @param {Ab.view.Component} panel
     */
    abEamAssetSummaryActions_afterRefresh: function (panel) {
        if (!isEquipmentRequiredItemSchemaDefined) {
            View.showMessage(getMessage('backwardCompatibilityEqReqItemsMessage'));
            return;
        }
        var clause = panel.restriction.findClause('sb.sb_name');
        this.sbName = clause.value;
        this.projectId = this.getSelectedProjectId();
        this.initializeGlobals();
        var isSbDefined = valueExists(this.sbRecord);
        // set panel title
        var title = String.format(getMessage('titleAssetSummaryPanel'), this.projectId);
        this.abEamAssetSummaryActions.setTitle(title);
        // enable action
        this.enableAction(this.abEamAssetSummaryActions.actions.get('createSpaceBudget'), !isSbDefined);
        this.enableAction(this.abEamAssetSummaryActions.actions.get('addLocations'), isSbDefined);
        this.enableAction(this.abEamAssetSummaryActions.actions.get('selectPeriods'), isSbDefined);
        this.enableAction(this.abEamAssetSbItems.actions.get('add'), isSbDefined);
        this.enableAction(this.abEamAssetEqItems.actions.get('add'), isSbDefined);
        // refresh panels
        this.showAssetSummary(new Ab.view.Restriction({'sb.sb_name': this.sbName}));
        this.abEamAssetSbItems.refresh(new Ab.view.Restriction({'sb_items.sb_name': this.sbName}));
    },
    abEamAssetSbItems_afterRefresh: function () {
        this.abEamAssetEqItems.refresh(new Ab.view.Restriction({'eq_req_items.sb_name': this.sbName}));
    },
    /**
     * Init periods.
     * {Array.<Object>} {fieldName:fieldName, show:true, index:1}
     * By default, baseline (p00_value) and period 1 (p01_value) are displayed.
     */
    initPeriods: function () {
        var periodFields = [];
        for ( var i = 0; i <= 12; i++ ) {
            var val = i;
            if (i < 10) {
                val = '0' + i;
            }
            var show = i === 0 || i === 1;
            periodFields.push({
                fieldName: 'p' + val + '_value',
                show: show,
                index: i
            });
        }
        this.periodFields = periodFields;
    },
    /**
     * Init controller properties.
     */
    initializeGlobals: function () {
        if (valueExistsNotEmpty(this.sbName)) {
            this.sbRecord = null;
            this.sbLevel = null;
            // get sb record for current sb_name
            var record = getSpaceBudgetRecord(this.sbName);
            if (!record.isNew) {
                this.sbRecord = record;
                this.sbLevel = record.getValue('sb.sb_level');
                this.sbType = record.getValue('sb.sb_type');
            }
        }
    },
    /**
     * Init asset summary panel handlebar helpers.
     */
    initAssetSummary: function () {
        // localize value where value is translatable message defined in the view.
        Handlebars.registerHelper('localizeValue', function (value) {
            return getMessage(value);
        });
        // get and format a field record value
        Handlebars.registerHelper('getValue', function (record, fieldName) {
            var value = record.getValue(fieldName);
            return View.controllers.get('abEamCptProjAssetController').formatFieldValue(value, fieldName);
        });
        // calculate delta based on two periods
        Handlebars.registerHelper('calculateDelta', function (periods, fieldName) {
            var value1 = periods[0].record.getValue(fieldName),
                value2 = periods[1].record.getValue(fieldName);
            if (valueExists(value1) && valueExists(value2)) {
                var delta = Number(value2) - Number(value1);
                return View.controllers.get('abEamCptProjAssetController').formatFieldValue(delta, fieldName);
            }
        });
        // register partials
        Handlebars.registerPartial("deltaHeader", jQuery('#deltaHeader').html());
        Handlebars.registerPartial("periodHeader", jQuery('#periodHeader').html());
    },
    /**
     * Show asset summary.
     * @param {Ab.view.Restriction} restriction to apply.
     */
    showAssetSummary: function (restriction) {
        var sidecar = this.abEamAssetSelectPeriodsPanel.getSidecar();
        var periods = sidecar.get('periods') || this.periodFields;
        var dataSource = View.dataSources.get('abEamAssetSummary_ds');
        dataSource.clearParameters();
        var templatePeriods = [];
        _.each(periods, function (period) {
            if (period.show) {
                dataSource.addParameter('period', 'sb_items.' + period.fieldName);
                if ('p00_value' !== period.fieldName) {
                    dataSource.addParameter('period_eq_req_items', 'eq_req_items.' + period.fieldName + '=1');
                }
                period.record = dataSource.getRecord(restriction);
                period.label = period.index === 0 ? getMessage('infoSummaryText_periodBaselineLabel') : getMessage('infoSummaryText_periodLabel') + ' ' + period.index;
                templatePeriods.push(period);
            }
        });
        var showDelta = templatePeriods.length === 2;
        var noColumns = 4 + (templatePeriods.length * 3);
        if (showDelta) {
            noColumns += 3;
        }
        var parentElement = this.abEamAssetSummary.parentElement;
        // clear panel
        jQuery(parentElement).empty();
        this.abEamAssetSummary.refresh();
        // render template
        View.templates.get('mainTemplate').render({
            periods: templatePeriods,
            showDelta: showDelta,
            noColumns: noColumns
        }, parentElement);
    },
    /**
     * On open select periods action. If not periods are saved into the local storage, get the default periods.
     * @param {Object} button element
     */
    abEamAssetSummaryActions_onSelectPeriods: function (button) {
        this.abEamAssetSelectPeriodsPanel.showInWindow({
            modal: true,
            width: 500,
            height: 300,
            anchor: button.container,
            closeButton: true
        });
        var sidecar = this.abEamAssetSelectPeriodsPanel.getSidecar();
        var periods = sidecar.get('periods') || this.periodFields;
        _.each(periods, function (field) {
            $(field.fieldName).checked = field.show;
        });
    },
    /**
     * On select periods action.
     * @returns {boolean} false if no period was checked.
     */
    abEamAssetSelectPeriodsPanel_onSelectPeriods: function () {
        // The form requires that at least one Period checkbox be checked.
        var periodChecked = _.find(this.periodFields, function (period) {
            return $(period.fieldName).checked;
        });
        if (!valueExists(periodChecked)) {
            View.showMessage(getMessage('noPeriodSelected'));
            return false;
        }
        // set period fields
        var show = false;
        var abEamAssetSbItems = View.panels.get('abEamAssetSbItems'),
            abEamAssetEqItems = View.panels.get('abEamAssetEqItems');
        _.each(this.periodFields, function (period) {
            show = $(period.fieldName).checked;
            period.show = show;
            // update  grid columns
            abEamAssetSbItems.showColumn('sb_items.' + period.fieldName, show);
            abEamAssetSbItems.showColumn('sb_items.cost_of_requirement_p' + period.index, show);
            abEamAssetSbItems.showColumn('sb_items.area_of_requirement_p' + period.index, show);
            abEamAssetEqItems.showColumn('eq_req_items.' + period.fieldName, show);
        });
        this.updatePeriods();
        this.showAssetSummary(new Ab.view.Restriction({'sb.sb_name': this.sbName}));
        this.updatePanelPeriodsFields(abEamAssetSbItems);
        this.updatePanelPeriodsFields(abEamAssetEqItems);
        this.abEamAssetSelectPeriodsPanel.closeWindow();
    },
    /**
     *  Save periods to panel sidecar.
     */
    updatePeriods: function () {
        // save periods to panel sidecar
        var sidecar = View.panels.get('abEamAssetSelectPeriodsPanel').getSidecar();
        sidecar.set('periods', this.periodFields);
        sidecar.save();
    },
    /**
     * Save periods to panel sidecar.
     * @param panel
     */
    updatePanelPeriodsFields: function (panel) {
        // set side car fields
        var sidecar = panel.getSidecar();
        sidecar.set('columns', panel.columns);
        sidecar.save();
        // update grid
        panel.update();
    },
    /**
     * Format field value.
     * @param {string} fieldValue
     * @param {string} fieldName
     */
    formatFieldValue: function (fieldValue, fieldName) {
        var dataSource = View.dataSources.get('abEamAssetSummary_ds');
        var fieldDef = dataSource.fieldDefs.get(fieldName);
        return fieldDef.formatValue(fieldValue, true, false);
    },
    /**
     * Enable action.
     * @param {Ab.view.Action} action
     * @param {boolean} enable
     */
    enableAction: function (action, enable) {
        action.enabled = enable;
        action.forceDisable(!enable);
    },
    /**
     * On create requirements action.
     */
    abEamAssetSummaryActions_onCreateSpaceBudget: function () {
        var projectRecord = getProjectRecord(this.sbName);
        // set project name to project code when project name not exists
        saveProjectNameAsCode(projectRecord);
        var summary = (!projectRecord.isNew) ? projectRecord.getValue('project.summary') : null;
        var restriction = new Ab.view.Restriction();
        var isNewSbRecord = false;
        if (valueExists(this.sbRecord)) {
            restriction.addClause('sb.sb_name', this.sbName, '=');
            isNewSbRecord = false;
        } else {
            restriction.addClause('sb.sb_name', this.sbName, '=');
            restriction.addClause('sb.sb_desc', summary, '=');
            restriction.addClause('sb.sb_type', this.sbType, '=');
            restriction.addClause('sb.sb_as', 'st', '=');
            restriction.addClause('sb.sb_from', 'rm', '=');
            restriction.addClause('sb.level', '0', '=');
            isNewSbRecord = true;
        }
        View.openDialog('ab-eam-define-sb.axvw', restriction, isNewSbRecord, {
            width: 900,
            height: 800,
            closeButton: false,
            sbType: this.sbType,
            fieldDefs: {
                'sb_create_for': {
                    visible: true,
                    values: ['eq', 'fn']
                }
            },
            callback: function () {
                var controller = View.controllers.get('abEamCptProjAssetController');
                controller.abEamAssetSummaryActions.refresh(controller.view.restriction);
                View.closeDialog();
            }
        });
    },
    /**
     * On add baseline locations action.
     */
    abEamAssetSummaryActions_onAddLocations: function () {
        var restriction = new Ab.view.Restriction();
        restriction.addClause('sb.sb_name', this.sbName, '=');
        View.openDialog('ab-eam-sb-add-locations.axvw', restriction, false, {
            width: 900,
            height: 800,
            closeButton: false,
            isFromSpace: false,
            isFromAsset: true,
            fieldDefs: {
                'sb_create_for': {
                    visible: true,
                    values: ['eq', 'fn']
                }
            },
            callback: function () {
                var controller = View.controllers.get('abEamCptProjAssetController');
                controller.abEamAssetSummaryActions.refresh(controller.view.restriction);
                View.closeDialog();
            }
        });
    },
    abEamAssetSbItems_onClickItem: function (row) {
        var restriction = new Ab.view.Restriction();
        restriction.addClause('sb_items.auto_number', row.getFieldValue('sb_items.auto_number'));
        restriction.addClause('sb_items.sb_name', this.sbName);
        this.onEditSbItems(restriction, false);
    },
    abEamAssetSbItems_onAdd: function (row) {
        var restriction = new Ab.view.Restriction();
        restriction.addClause('sb_items.sb_name', this.sbName);
        this.onEditSbItems(restriction, true);
    },
    /**
     * On edit asset item action.
     * @param {Ab.view.Restriction} restriction
     * @param {boolean} newRecord
     */
    onEditSbItems: function (restriction, newRecord) {
        View.openDialog('ab-eam-define-sb-item-by-count.axvw', restriction, newRecord, {
            width: 900,
            height: 800,
            closeButton: true,
            callback: function () {
                var controller = View.controllers.get('abEamCptProjAssetController');
                controller.abEamAssetSummaryActions.refresh(controller.view.restriction);
                View.closeDialog();
            }
        });
    },
    abEamAssetEqItems_onClickItem: function (row) {
        var restriction = new Ab.view.Restriction();
        restriction.addClause('eq_req_items.auto_number', row.getFieldValue('eq_req_items.auto_number'));
        restriction.addClause('eq_req_items.sb_name', this.sbName);
        var assetType = row.getFieldValue('eq_req_items.asset_type');
        var isNewAsset = true;
        if ('eq' === assetType && valueExistsNotEmpty(row.getFieldValue('eq_req_items.eq_id'))) {
            isNewAsset = false
        } else if ('ta' === assetType && valueExistsNotEmpty(row.getFieldValue('eq_req_items.ta_id'))) {
            isNewAsset = false;
        }
        this.onEditEqReqItems(restriction, false, assetType, isNewAsset);
    },
    abEamAssetEqItems_onAdd: function (row) {
        var restriction = new Ab.view.Restriction();
        restriction.addClause('eq_req_items.sb_name', this.sbName);
        this.onEditEqReqItems(restriction, true, 'eq', true);
    },
    /**
     * On edit asset requirement action.
     * @param {Ab.view.Restriction}restriction
     * @param {boolean} newRecord
     * @param {string} assetType
     * @param {boolean} isNewAsset
     */
    onEditEqReqItems: function (restriction, newRecord, assetType, isNewAsset) {
        var controller = this;
        View.openDialog('ab-eam-define-eq-req-item.axvw', restriction, newRecord, {
            width: 900,
            height: 800,
            closeButton: true,
            assetType: assetType,
            isNewAsset: isNewAsset,
            periodFields: this.periodFields,
            callback: function () {
                controller.abEamAssetSummaryActions.refresh(controller.view.restriction);
                View.closeDialog();
            }
        });
    },
    onClickReportMenu: function (el) {
        var buttonElem = Ext.get(el.target);
        var reportMenuItem = new MenuItem({
            menuDef: {
                id: 'reportsMenu',
                type: 'menu',
                viewName: null,
                isRestricted: false,
                parameters: null
            },
            onClickMenuHandler: onClickMenu,
            onClickMenuHandlerRestricted: onClickMenuWithRestriction,
            submenu: abEamReportsCommonMenu
        });
        reportMenuItem.build();
        var menu = new Ext.menu.Menu({items: reportMenuItem.menuItems, cls: 'filter-reports'});
        menu.show(buttonElem, 'tl-bl?');
    },
    /**
     * Get main controller.
     * @returns {Ab.view.Controller}
     */
    getMainController: function () {
        var controller = null;
        if (valueExists(this.view.getOpenerView()) && valueExists(this.view.getOpenerView().controllers.get('abEamCptProjConsoleCtrl'))) {
            controller = this.view.getOpenerView().controllers.get('abEamCptProjConsoleCtrl');
        }
        return controller;
    },
    /**
     * Get selected project id.
     * @returns {null} if main controller is not defined.
     */
    getSelectedProjectId: function () {
        var mainController = this.getMainController();
        if (mainController) {
            return mainController.projectId;
        } else {
            return null;
        }
    },
    /**
     * On create asset action.
     * @param row
     */
    onAssignRow: function (row) {
        var controller = View.controllers.get('abEamCptProjAssetController');
        var record = row.row.getRecord();
        var gridPanel = row.grid;
        var rowIndex = row.row.getIndex();
        var buttonId = gridPanel.id + '_row' + rowIndex + '_assignRow';
        var button = document.getElementById(buttonId);
        controller.tmpRecord = record;
        var dialogConfig = {
            anchor: button,
            width: 600,
            height: 200,
            closeButton: false
        };
        controller.abEamAssetAddAction_form.setFieldValue('activity_log.project_id', controller.projectId);
        controller.abEamAssetAddAction_form.showInWindow(dialogConfig);
    },
    abEamAssetAddAction_form_onSave: function () {
        if (this.abEamAssetAddAction_form.canSave()) {
            var projectId = this.abEamAssetAddAction_form.getFieldValue('activity_log.project_id');
            var workPkgId = this.abEamAssetAddAction_form.getFieldValue('activity_log.work_pkg_id');
            var activityType = this.abEamAssetAddAction_form.getFieldValue('activity_log.activity_type');
            var restriction = new Ab.view.Restriction();
            restriction.addClause('activity_log.project_id', projectId, '=');
            restriction.addClause('activity_log.work_pkg_id', workPkgId, '=');
            restriction.addClause('activity_log.activity_type', activityType, '=');
            var assetType = this.tmpRecord.getValue('eq_req_items.asset_type');
            if ('eq' === assetType) {
                var eqId = this.tmpRecord.getValue('eq_req_items.eq_id');
                if (valueExistsNotEmpty(eqId)) {
                    restriction.addClause('activity_log.eq_id', eqId, '=');
                }
            } else {
                var taId = this.tmpRecord.getValue('eq_req_items.ta_id');
                if (valueExistsNotEmpty(taId)) {
                    restriction.addClause('activity_log.ta_id', taId, '=');
                }
            }
            var reqDescription = '';
            var planningId = this.tmpRecord.getValue('eq_req_items.planning_id');
            if (valueExistsNotEmpty(planningId)) {
                reqDescription += '\n' + getMessage('labelAssetPlanId') + ': ' + planningId;
            }
            var description = this.tmpRecord.getValue('eq_req_items.description');
            if (valueExistsNotEmpty(description)) {
                reqDescription += '\n' + getMessage('labelAssetStandard') + ': ' + description;
            }
            if (valueExistsNotEmpty(reqDescription)) {
                restriction.addClause('activity_log.description', reqDescription, '=');
            }
            this.abEamAssetAddAction_form.closeWindow();
            this.addProjectAction(restriction, true);
        }
    },
    addProjectAction: function (restriction, newRecord) {
        View.openDialog('ab-proj-mng-act-edit.axvw', restriction, newRecord, {
            width: 1024,
            height: 600,
            createWorkRequest: false,
            isCopyAsNew: false,
            showDocumentsPanel: true,
            panelsConfiguration: {
                'projMngActEdit_Progress': {
                    actions: [{id: 'showMore', hidden: true}, {id: 'showLess', hidden: true}],
                    fields: [
                        {name: 'activity_log.status'},
                        {name: 'activity_log.hours_est_baseline', required: true},
                        {name: 'activity_log.date_planned_for', required: true},
                        {name: 'activity_log.duration_est_baseline', required: true},
                        {name: 'activity_log.date_required'},
                        {name: 'activity_log.date_scheduled_end'}
                    ]
                },
                'projMngActEdit_Costs': {
                    actions: [{id: 'showMore', hidden: true}, {id: 'showLess', hidden: true}],
                    fields: [
                        {name: 'activity_log.cost_est_cap', required: true},
                        {name: 'activity_log.cost_estimated', required: true}
                    ]
                },
                'projMngActEdit_Details': {
                    fields: [
                        {name: 'activity_log.doc'},
                        {name: 'activity_log.description'},
                        {name: 'activity_log.created_by'},
                        {name: 'activity_log.date_requested'},
                        {name: 'activity_log.approved_by'},
                        {name: 'activity_log.date_approved'}
                    ]
                }
            }
        });
    }
});
function onClickMenu(menu) {
    if (valueExists(menu.viewName)) {
        var dialogConfig = {
            width: 1024,
            height: 800,
            closeButton: true
        };
        if (valueExists(menu.parameters)) {
            for ( var param in menu.parameters ) {
                if (menu.parameters.hasOwnProperty(param)) {
                    if ('title' === param) {
                        dialogConfig[param] = getMessage(menu.parameters[param]);
                    } else {
                        dialogConfig[param] = menu.parameters[param];
                    }
                }
            }
        }
        View.openDialog(menu.viewName, null, false, dialogConfig);
    }
}
function onClickMenuWithRestriction(menu) {
    // TODO : pass restriction to view name
    if (valueExists(menu.viewName)) {
        var dialogConfig = {
            width: 1024,
            height: 800,
            closeButton: true
        };
        if (valueExists(menu.parameters)) {
            for ( var param in menu.parameters ) {
                if (menu.parameters.hasOwnProperty(param)) {
                    if ('title' === param) {
                        dialogConfig[param] = getMessage(menu.parameters[param]);
                    } else {
                        dialogConfig[param] = menu.parameters[param];
                    }
                }
            }
        }
        View.openDialog(menu.viewName, null, false, dialogConfig);
    }
}
/**
 *  On Select value with Add new button
 * @param context command context
 */
function onSelectWorkPkgWithAddNew(context) {
    var panelId = context.command.parentPanelId;
    var parentPanel = View.panels.get(panelId);
    var projectId = parentPanel.getFieldValue('activity_log.project_id');
    var workPkgId = parentPanel.getFieldValue('activity_log.work_pkg_id');
    var controller = View.controllers.get('abEamCptProjAssetController');
    View.openDialog('ab-work-pkg-select-value-with-add-new.axvw', null, false, {
        width: 800,
        height: 800,
        selectValueType: 'grid',
        projectId: projectId,
        workPkgId: workPkgId,
        callback: function (rows) {
            var selectedProjectId = '',
                selectedWorkpkgId = '';
            controller.tmpProjectId = [];
            controller.tmpWorkPkgId = [];
            for ( var i = 0; i < rows.length; i++ ) {
                var row = rows[i];
                var tmpProjectId = row.getFieldValue('work_pkgs.project_id'),
                    tmpWorkPkgId = row.getFieldValue('work_pkgs.work_pkg_id');
                if (controller.tmpProjectId.indexOf(tmpProjectId) === -1) {
                    selectedProjectId += (selectedProjectId.length > 0 ? Ab.form.Form.MULTIPLE_VALUES_SEPARATOR : '') + tmpProjectId;
                }
                selectedWorkpkgId += (selectedWorkpkgId.length > 0 ? Ab.form.Form.MULTIPLE_VALUES_SEPARATOR : '') + tmpWorkPkgId;
                controller.tmpProjectId.push(tmpProjectId);
                controller.tmpWorkPkgId.push(tmpWorkPkgId);
            }
            parentPanel.setFieldValue('activity_log.project_id', selectedProjectId);
            parentPanel.setFieldValue('activity_log.work_pkg_id', selectedWorkpkgId);
        }
    });
}