/**
 * Controller definition.
 */
View.createController('abEamCptProjSpaceController', {
    // selected project id
    projectId: null,
    // sb_name is project_name
    sbName: null,
    sbLevel: null,
    sbType: 'Space Forecast',
    sbRecord: null,
    // list of periods from 0 to 12
    periodFields: [],
    afterViewLoad: function () {
        this.initPeriods();
        this.initSpaceSummary();
    },
    afterInitialDataFetch: function () {
        // override export methods to customize the report title
        var scope = this;
        this.abEamSpaceItems.callXLSReportJob = function (title, restriction, parameters) {
            var viewName = this.viewDef.viewName + '.axvw';
            parameters.printRestriction = false;
            return Workflow.startJob(Ab.grid.ReportGrid.WORKFLOW_RULE_XLS_REPORT, viewName, this.dataSourceId, this.title + ': ' + scope.sbName, this.getVisibleFieldDefs(), toJSON(restriction), parameters);
        };
        this.abEamSpaceItems.callDOCXReportJob = function (title, restriction, parameters) {
            var viewName = this.viewDef.viewName + '.axvw';
            parameters.printRestriction = false;
            return Workflow.startJob(Ab.grid.ReportGrid.WORKFLOW_RULE_DOCX_REPORT, viewName, this.dataSourceId, this.title + ': ' + scope.sbName, this.getVisibleFieldDefs(), toJSON(restriction), parameters);
        };
    },
    /**
     * Called after space summary panel is refreshed.
     * @param {Ab.view.Component} panel
     */
    abEamSpaceSummaryActions_afterRefresh: function (panel) {
        var clause = panel.restriction.findClause('sb_items.sb_name');
        this.sbName = clause.value;
        this.projectId = this.getSelectedProjectId();
        this.initializeGlobals();
        var isSbDefined = valueExists(this.sbRecord);
        // set panel title
        var title = String.format(getMessage('titleSpaceSummaryPanel'), this.projectId);
        this.abEamSpaceSummaryActions.setTitle(title);
        // enable action
        this.enableAction(this.abEamSpaceSummaryActions.actions.get('createSpaceBudget'), !isSbDefined);
        this.enableAction(this.abEamSpaceSummaryActions.actions.get('addLocations'), isSbDefined);
        this.enableAction(this.abEamSpaceSummaryActions.actions.get('selectPeriods'), isSbDefined);
        this.enableAction(this.abEamSpaceItems.actions.get('add'), isSbDefined);
        // refresh panels
        var restriction = new Ab.view.Restriction({'sb_items.sb_name': this.sbName});
        this.showSpaceSummary(restriction);
        this.abEamSpaceItems.refresh(restriction);
    },
    /**
     * Init periods.
     * {Array.<Object>} {fieldName:fieldName, show:true, index:1}
     * By default, baseline (p00_value) and period 1 (p01_value) are displayed.
     */
    initPeriods: function () {
        var periodFields = [];
        for ( var i = 0; i <= 12; i++ ) {
            var val = i;
            if (i < 10) {
                val = '0' + i;
            }
            var show = i === 0 || i === 1;
            periodFields.push({
                fieldName: 'p' + val + '_value',
                show: show,
                index: i
            });
        }
        this.periodFields = periodFields;
    },
    /**
     * Init controller properties.
     */
    initializeGlobals: function () {
        if (valueExistsNotEmpty(this.sbName)) {
            this.sbRecord = null;
            this.sbLevel = null;
            // get sb record for current sb_name
            var record = getSpaceBudgetRecord(this.sbName);
            if (!record.isNew) {
                this.sbRecord = record;
                this.sbLevel = record.getValue('sb.sb_level');
                this.sbType = record.getValue('sb.sb_type');
            }
        }
    },
    /**
     * Init space summary panel handlebar helpers.
     */
    initSpaceSummary: function () {
        // localize value where value is translatable message defined in the view.
        Handlebars.registerHelper('localizeValue', function (value) {
            return getMessage(value);
        });
        // get and format a field record value
        Handlebars.registerHelper('getValue', function (record, fieldName) {
            var value = record.getValue(fieldName);
            return View.controllers.get('abEamCptProjSpaceController').formatFieldValue(value, fieldName);
        });
        // calculate delta based on two periods
        Handlebars.registerHelper('calculateDelta', function (periods, fieldName) {
            var value1 = periods[0].record.getValue(fieldName),
                value2 = periods[1].record.getValue(fieldName);
            if (valueExists(value1) && valueExists(value2)) {
                var delta = Number(value2) - Number(value1);
                return View.controllers.get('abEamCptProjSpaceController').formatFieldValue(delta, fieldName);
            }
        });
        // register partials
        Handlebars.registerPartial("deltaHeader", jQuery('#deltaHeader').html());
        Handlebars.registerPartial("periodHeader", jQuery('#periodHeader').html());
    },
    /**
     * Show space summary.
     * @param {Ab.view.Restriction} restriction to apply.
     */
    showSpaceSummary: function (restriction) {
        var sidecar = this.abEamSpaceSelectPeriodsPanel.getSidecar();
        var periods = sidecar.get('periods') || this.periodFields;
        var dataSource = View.dataSources.get('abEamSpaceSummary_ds');
        dataSource.clearParameters();
        var templatePeriods = [];
        _.each(periods, function (period) {
            if (period.show) {
                dataSource.addParameter('period', 'sb_items.' + period.fieldName);
                period.record = dataSource.getRecord(restriction);
                period.label = period.index === 0 ? getMessage('infoSummaryText_periodBaselineLabel') : getMessage('infoSummaryText_periodLabel') + ' ' + period.index;
                templatePeriods.push(period);
            }
        });
        // show delta only when we have two columns
        var showDelta = templatePeriods.length === 2;
        var noColumns = 4 + (templatePeriods.length * 3);
        if (showDelta) {
            noColumns += 3;
        }
        var parentElement = this.abEamSpaceSummary.parentElement;
        // clear panel
        jQuery(parentElement).empty();
        this.abEamSpaceSummary.refresh();
        // render template
        View.templates.get('mainTemplate').render({
            periods: templatePeriods,
            showDelta: showDelta,
            noColumns: noColumns
        }, parentElement);
    },
    /**
     * On open select periods action. If not periods are saved into the local storage, get the default periods.
     * @param {Object} button element
     */
    abEamSpaceSummaryActions_onSelectPeriods: function (button) {
        this.abEamSpaceSelectPeriodsPanel.showInWindow({
            modal: true,
            width: 500,
            height: 300,
            anchor: button.container,
            closeButton: true
        });
        var sidecar = this.abEamSpaceSelectPeriodsPanel.getSidecar();
        var periods = sidecar.get('periods') || this.periodFields;
        _.each(periods, function (field) {
            $(field.fieldName).checked = field.show;
        });
    },
    /**
     * On select periods action.
     * @returns {boolean} false if no period was checked.
     */
    abEamSpaceSelectPeriodsPanel_onSelectPeriods: function () {
        // The form requires that at least one Period checkbox be checked.
        var periodChecked = _.find(this.periodFields, function (period) {
            return $(period.fieldName).checked;
        });
        if (!valueExists(periodChecked)) {
            View.showMessage(getMessage('noPeriodSelected'));
            return false;
        }
        // set period fields
        var show = false;
        var abEamSpaceItems = View.panels.get('abEamSpaceItems');
        _.each(this.periodFields, function (period) {
            show = $(period.fieldName).checked;
            period.show = show;
            // update grid columns
            abEamSpaceItems.showColumn('sb_items.' + period.fieldName, show);
            abEamSpaceItems.showColumn('sb_items.cost_of_requirement_p' + period.index, show);
            abEamSpaceItems.showColumn('sb_items.area_of_requirement_p' + period.index, show);
        });
        this.updatePeriods();
        this.showSpaceSummary(new Ab.view.Restriction({'sb_items.sb_name': this.sbName}));
        this.updatePanelPeriodsFields(abEamSpaceItems);
        this.abEamSpaceSelectPeriodsPanel.closeWindow();
    },
    /**
     *  Save periods to panel sidecar.
     */
    updatePeriods: function () {
        // save periods to panel sidecar
        var sidecar = View.panels.get('abEamSpaceSelectPeriodsPanel').getSidecar();
        sidecar.set('periods', this.periodFields);
        sidecar.save();
    },
    /**
     * Save periods to panel sidecar.
     * @param panel
     */
    updatePanelPeriodsFields: function (panel) {
        // set side car fields
        var sidecar = panel.getSidecar();
        sidecar.set('columns', panel.columns);
        sidecar.save();
        // update grid
        panel.update();
    },
    /**
     * Format field value.
     * @param {string} fieldValue
     * @param {string} fieldName
     */
    formatFieldValue: function (fieldValue, fieldName) {
        var dataSource = View.dataSources.get('abEamSpaceSummary_ds');
        var fieldDef = dataSource.fieldDefs.get(fieldName);
        return fieldDef.formatValue(fieldValue, true, false);
    },
    /**
     * Enable action.
     * @param {Ab.view.Action} action
     * @param {boolean} enable
     */
    enableAction: function (action, enable) {
        action.enabled = enable;
        action.forceDisable(!enable);
    },
    /**
     * On create requirements action.
     */
    abEamSpaceSummaryActions_onCreateSpaceBudget: function () {
        var projectRecord = getProjectRecord(this.sbName);
        //KB3049945 - set project name to project code when project name not exists
        saveProjectNameAsCode(projectRecord);
        var summary = (!projectRecord.isNew) ? projectRecord.getValue('project.summary') : null;
        var restriction = new Ab.view.Restriction();
        var isNewSbRecord = false;
        if (valueExists(this.sbRecord)) {
            restriction.addClause('sb.sb_name', this.sbName, '=');
            isNewSbRecord = false;
        } else {
            restriction.addClause('sb.sb_name', this.sbName, '=');
            restriction.addClause('sb.sb_desc', summary, '=');
            restriction.addClause('sb.sb_type', this.sbType, '=');
            restriction.addClause('sb.sb_as', 'st', '=');
            restriction.addClause('sb.sb_from', 'rm', '=');
            restriction.addClause('sb.level', '0', '=');
            isNewSbRecord = true;
        }
        View.openDialog('ab-eam-define-sb.axvw', restriction, isNewSbRecord, {
            width: 900,
            height: 800,
            closeButton: false,
            sbType: this.sbType,
            fieldDefs: {
                'sb_create_for': {
                    visible: true,
                    values: ['rm']
                }
            },
            callback: function () {
                var controller = View.controllers.get('abEamCptProjSpaceController');
                controller.abEamSpaceSummaryActions.refresh(controller.view.restriction);
                View.closeDialog();
            }
        });
    },
    /**
     * On add baseline locations action.
     */
    abEamSpaceSummaryActions_onAddLocations: function () {
        var restriction = new Ab.view.Restriction();
        restriction.addClause('sb.sb_name', this.sbName, '=');
        View.openDialog('ab-eam-sb-add-locations.axvw', restriction, false, {
            width: 900,
            height: 800,
            closeButton: false,
            isFromSpace: true,
            isFromAsset: false,
            fieldDefs: {
                'sb_create_for': {
                    visible: true,
                    values: ['rm']
                }
            },
            callback: function () {
                var controller = View.controllers.get('abEamCptProjSpaceController');
                controller.abEamSpaceSummaryActions.refresh(controller.view.restriction);
                View.closeDialog();
            }
        });
    },
    abEamSpaceItems_onClickItem: function (row) {
        var restriction = new Ab.view.Restriction();
        restriction.addClause('sb_items.auto_number', row.getFieldValue('sb_items.auto_number'));
        restriction.addClause('sb_items.sb_name', this.sbName);
        this.onEditSbItems(restriction, false);
    },
    abEamSpaceItems_onAdd: function () {
        var restriction = new Ab.view.Restriction();
        restriction.addClause('sb_items.sb_name', this.sbName);
        this.onEditSbItems(restriction, true);
    },
    /**
     * On edit space item action.
     * @param {Ab.view.Restriction} restriction
     * @param {boolean} newRecord
     */
    onEditSbItems: function (restriction, newRecord) {
        View.openDialog('ab-eam-define-sb-item.axvw', restriction, newRecord, {
            width: 900,
            height: 800,
            closeButton: true,
            callback: function () {
                var controller = View.controllers.get('abEamCptProjSpaceController');
                controller.abEamSpaceSummaryActions.refresh(controller.view.restriction);
                View.closeDialog();
            }
        });
    },
    /**
     * Get main controller.
     * @returns {Ab.view.Controller}
     */
    getMainController: function () {
        var controller = null;
        if (valueExists(this.view.getOpenerView()) && valueExists(this.view.getOpenerView().controllers.get('abEamCptProjConsoleCtrl'))) {
            controller = this.view.getOpenerView().controllers.get('abEamCptProjConsoleCtrl');
        }
        return controller;
    },
    /**
     * Get selected project id.
     * @returns {null} if main controller is not defined.
     */
    getSelectedProjectId: function () {
        var mainController = this.getMainController();
        if (mainController) {
            return mainController.projectId;
        } else {
            return null;
        }
    }
});