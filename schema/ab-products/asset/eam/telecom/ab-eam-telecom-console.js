View.createController('abEamTcController', {
    /**
     * Drawing controller.
     */
    drawingController: null,
    /**
     * Exists selected assets to query; set when query data action is done.
     */
    existsAssetsToQuery: false,
    /**
     * Default highlight color.
     */
    defaultHighlightColor: '#00ffff',
    afterViewLoad: function () {
        this.initDrawingControl();
    },
    /**
     * Init drawing control.
     */
    initDrawingControl: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['rm', 'eq', 'jk', 'fp', 'pn'],
            multipleSelectionEnabled: true,
            maximize: {
                showAsDialog: false,
                expand: function () {
                    var layout = View.getLayoutManager('mainLayout');
                    layout.expandRegion('north');
                    layout.expandRegion('west');
                    layout.recalculateLayout();
                },
                collapse: function () {
                    var layout = View.getLayoutManager('mainLayout');
                    layout.collapseRegion('north');
                    layout.collapseRegion('west');
                    layout.recalculateLayout();
                }
            },
            reset: true,
            print: true,
            highlightParameters: [{
                'view_file': "ab-eam-telecom-console-drawing-ds.axvw",
                'hs_ds': "highlightNoneDs",
                'label_ds': 'labelNoneDs',
                'label_ht': '0.60',
                showSelector: false
            }],
            layersPopup: {
                layers: "rm-assets;rm-labels;eq-assets;eq-labels;jk-assets;jk-labels;fp-assets;fp-labels;pn-assets;pn-labels;gros-assets;gros-labels;background",
                defaultLayers: "rm-assets;rm-labels;eq-assets;jk-assets;fp-assets;pn-assets;gros-assets"
            },
            assetTooltip: [
                {assetType: 'rm', datasource: 'label_rm_ds', fields: 'rm.rm_id;rm.rm_std'},
                {assetType: 'eq', datasource: 'label_eq_ds', fields: 'eq.eq_id;eq.eq_std;'},
                {assetType: 'jk', datasource: 'label_jk_ds', fields: 'jk.jk_id;jk.jk_std'},
                {assetType: 'fp', datasource: 'label_fp_ds', fields: 'fp.fp_id;fp.fp_std'},
                {assetType: 'pn', datasource: 'label_pn_ds', fields: 'pn.pn_id;pn.pn_std'}
            ],
            actions: [{
                text: getMessage("queryAssetDataAction"),
                id: 'query',
                listener: this.onQueryAssetDataAction.createDelegate(this)
            }, {
                text: getMessage("listConnectionsAction"),
                id: 'listConnections',
                visible: function (assetType) {
                    return 'rm' !== assetType;
                },
                listener: this.onListConnectionsAction.createDelegate(this)
            }, {
                text: getMessage("traceConnectionsAction"),
                id: 'traceConnections',
                visible: function (assetType) {
                    return 'rm' !== assetType;
                },
                listener: this.onTraceConnectionsAction.createDelegate(this)
            }, {
                text: getMessage("highlightConnectedRoomsAction"),
                id: 'highlightConnectedRooms',
                visible: function (assetType) {
                    return 'rm' !== assetType;
                },
                listener: this.onHighlightConnectedRoomsAction.createDelegate(this)
            }, {
                text: getMessage("connectAction"),
                id: 'connect',
                visible: function (assetType) {
                    return 'rm' !== assetType;
                },
                listener: this.onConnectAction.createDelegate(this)
            }],
            reports: [
                {
                    text: getMessage('highlightAvailableAllJacksReport'),
                    listener: this.highlightOpenJacks.createDelegate(this, [])
                }, {
                    text: getMessage('highlightAvailableDataJacksReport'),
                    listener: this.highlightOpenJacks.createDelegate(this, ['D'])
                }, {
                    text: getMessage('highlightAvailableVoiceJacksReport'),
                    listener: this.highlightOpenJacks.createDelegate(this, ['V'])
                }],
            infoWindow: {
                width: '400px',
                position: 'bottom',
                customEvent: this.onCloseInfoWindow.createDelegate(this)
            },
            listeners: {
                onClickAssetAfter: this.onClickAsset.createDelegate(this),
                reset: this.onReset.createDelegate(this)
            }
        });
    },
    afterInitialDataFetch: function () {
        jQuery('#drawingPanel').addClass('panel-light drawing-area');
        this.drawingController.clearDrawingPanel();
        this.drawingPanel.setTitle(getMessage('noFlSelected'));
    },
    /**
     * On click event after asset is selected.
     * @param {AssetDrawingControl} drawingController - Drawing controller.
     * @param {string} svgId - Drawing svg id.
     * @param {string} selectedAssetId - Selected asset id.
     * @param {string} assetType - Selected asset type.
     */
    onClickAsset: function (drawingController, svgId, selectedAssetId, assetType) {
        if (this.drawingController.drawingState === 'finishConnect') {
            // after connection is done, reset all highlights
            this.resetDrawingHighlights();
        }
        if (this.drawingController.drawingState == 'startConnect') {
            if (this.drawingController.svgControl.getController("SelectController").multipleSelectionOn) {
                this.drawingController.svgControl.getAddOn('InfoWindow').setText(getMessage("cannotConnectMultipleSelection"));
                this.resetDrawingHighlights();
                return;
            }
            this.drawingController.svgControl.getController("HighlightController").highlightAsset(selectedAssetId, {
                svgId: svgId,
                color: this.defaultHighlightColor,
                persistFill: true,
                overwriteFill: true
            });
            this.drawingController.connectAssets['assetTo'] = {
                assetType: assetType,
                assetId: selectedAssetId
            };
            this.drawingController.svgControl.getAddOn('InfoWindow').appendText('<br>' + getMessage('secondAssetSelected') + ': [' + selectedAssetId + ']');
            this.connectAssets(svgId, this.drawingController.connectAssets);
        }
    },
    /**
     * Triggered on console filter 'Show' action.
     */
    applyFilter: function () {
        var console = View.panels.get('abEamTelConsole');
        var buildingId = console.getFieldValue('eq.bl_id'),
            floorId = console.getFieldValue('eq.fl_id'),
            rmId = console.getFieldValue('eq.rm_id'),
            eqId = console.getFieldValue('eq.eq_id'),
            eqStd = console.getFieldValue('eq.eq_std'),
            emId = console.getFieldValue('eq.em_id');
        this.onFilter(buildingId, floorId, rmId, eqId, eqStd, emId);
    },
    /**
     * Filters the drawing and the details tabs based on console restrictions.
     */
    onFilter: function (blId, flId, rmId, eqId, eqStd, emId) {
        this.filterDrawing(blId, flId, rmId, eqId);
        this.filterAssetDetailTabs(blId, flId, rmId, eqId, eqStd, emId);
    },
    /**
     * Apply filter restriction and load drawing if found.
     */
    filterDrawing: function (blId, flId, rmId, eqId) {
        if (valueExistsNotEmpty(blId) && valueExistsNotEmpty(flId)) {
            this.loadDrawing(blId, flId, rmId, eqId);
        } else {
            View.showMessage(String.format(getMessage("noDrawingFound"), blId, flId));
            this.clearConsole();
        }
    },
    /**
     * Load drawing.
     * @param {string} blId - Building id.
     * @param {string} flId - Floor id.
     * @param {string} rmId - Room id. If exists, zoom the drawing on that room and highlight.
     * @param {string} eqId - Equipment id. If exists, highlight the equipment.
     */
    loadDrawing: function (blId, flId, rmId, eqId) {
        if (!valueExists(this.drawingController)) {
            this.initDrawingControl();
        }
        this.drawingController.showDrawing({
            pkeyValues: {blId: blId, flId: flId},
            findAssets: [{
                assetType: 'rm',
                assetIds: [rmId]
            }],
            highlightAssets: [{
                assetType: 'eq',
                assetIds: [eqId]
            }]
        });
    }
    ,
    /**
     * Unload drawing.
     * @param {string} blId - Building id.
     * @param {string} flId - Floor id.
     * @param {string} drawingName - Drawing name.
     */
    unloadDrawing: function (blId, flId, drawingName) {
        this.drawingController.unloadDrawing(blId, flId, drawingName);
    }
    ,
    /**
     * Filter asset details tabs based on console fields value.
     */
    filterAssetDetailTabs: function (blId, flId, rmId, eqId, eqStd, emId) {
        var detailTabs = this.getDetailsTab();
        var params = {
            blId: blId,
            flId: flId,
            rmId: rmId,
            eqId: eqId,
            eqStd: eqStd,
            emId: emId
        };
        var restriction = "";
        // for each asset tab set console restriction
        _.each(detailTabs.tabs, function (tab) {
            if ('abEamTelConsTabs_rm_em' === tab.name) {
                restriction = this.createFilterRestriction("rm", blId, flId, rmId, eqId, eqStd, emId);
                detailTabs.setTabRestriction(tab.name, restriction);
            } else if ('abEamTelConsTabs_eq' === tab.name || 'abEamTelConsTabs_eq_eqport' === tab.name) {
                restriction = this.createAssetRestriction("eq", blId, flId, rmId, eqId, eqStd);
                if (valueExistsNotEmpty(emId)) {
                    var emSql = " AND EXISTS (SELECT 1 FROM em WHERE em.bl_id = eq.bl_id AND em.fl_id = eq.fl_id AND em.rm_id = eq.rm_id AND {0})";
                    restriction += String.format(emSql, this.createEmployeeRestriction(emId));
                }
                detailTabs.setTabRestriction(tab.name, restriction);
            } else if ('abEamTelConsTabs_fp_jk' === tab.name) {
                restriction = this.createFilterRestriction("fp", blId, flId, rmId, eqId, eqStd, emId);
                detailTabs.setTabRestriction(tab.name, restriction);
            } else if ('abEamTelConsTabs_jk' === tab.name) {
                restriction = this.createFilterRestriction("jk", blId, flId, rmId, eqId, eqStd, emId);
                detailTabs.setTabRestriction(tab.name, restriction);
            } else if ('abEamTelConsTabs_pn_pnport' === tab.name) {
                restriction = this.createFilterRestriction("pn", blId, flId, rmId, eqId, eqStd, emId);
                detailTabs.setTabRestriction(tab.name, restriction);
            } else if ('abEamTelConsTabs_softinv' === tab.name) {
                var eqSql = " EXISTS (SELECT 1 FROM eq WHERE eq.eq_id = softinv.eq_id AND {0})";
                restriction = String.format(eqSql, this.createEquipmentRestriction(eqId, eqStd));
                detailTabs.setTabRestriction(tab.name, restriction);
            }
            tab.parameters = params;
        }, this);
        if (detailTabs.tabPanel.container.dom.style.display === "none") {
            detailTabs.show(true);
            detailTabs.tabPanel.onResize();
        }
        detailTabs.selectTab(detailTabs.selectedTabName);
        // hide tab child panel
        this.hideSelectedTabDetailChildPanel(detailTabs);
    },
    /**
     * Hide selected tab detail child panel.
     * @param {Ab.tabs.Tabs} tabPanel - Tab panel.
     */
    hideSelectedTabDetailChildPanel: function (tabPanel) {
        var selectedTab = tabPanel.findTab(tabPanel.selectedTabName);
        if (selectedTab.isContentLoaded) {
            var tabController = selectedTab.getContentFrame().View.controllers.get('telConsCommonController');
            if (valueExists(tabController["abEamTelConsDetailsChildList"])) {
                tabController["abEamTelConsDetailsChildList"].show(false);
            }
        }
    },
    /**
     * Selects and display asset tab with selected assets restrictions.
     * @param {string} svgId - Drawing svg id.
     * @param {string} actionId - Action id.
     * @param {string} assetType - Selected asset type.
     */
    onQueryAssetDataAction: function (svgId, actionId, assetType) {
        this.showAssetDetails(svgId, actionId, assetType);
    },
    /**
     * Creates a list connection report with all connections for the selected asset.
     * @param {string} svgId - Drawing svg id.
     * @param {string} actionId - Action id.
     * @param {string} assetType - Selected asset type.
     * @param {string} selectedAssetId - Selected asset id.
     */
    onListConnectionsAction: function (svgId, actionId, assetType, selectedAssetId) {
        this.onListConnections(svgId, assetType, selectedAssetId, this.highlightAssetOnAction.createDelegate(this, [svgId, 'listConnections', selectedAssetId]));
    },
    /**
     * Trace connections for selected asset.
     * @param {string} svgId - Drawing svg id.
     * @param {string} actionId - Action id.
     * @param {string} assetType - Selected asset type.
     * @param {string} selectedAssetId - Selected asset id.
     */
    onTraceConnectionsAction: function (svgId, actionId, assetType, selectedAssetId) {
        this.onTraceAssetsConnections(svgId, assetType, selectedAssetId, this.highlightAssetOnAction.createDelegate(this, [svgId, 'traceConnections', selectedAssetId, true]));
    },
    /**
     * Highlight connected rooms for selected asset.
     * @param {string} svgId - Drawing svg id.
     * @param {string} actionId - Action id.
     * @param {string} assetType - Selected asset type.
     * @param {string} selectedAssetId - Selected asset id.
     */
    onHighlightConnectedRoomsAction: function (svgId, actionId, assetType, selectedAssetId) {
        this.onHighlightConnectedRooms(svgId, assetType, selectedAssetId, this.highlightAssetOnAction.createDelegate(this, [svgId, 'highlightConnectedRooms', selectedAssetId, true]));
    },
    /**
     * Connect an asset with another asset.
     * @param {string} svgId - Drawing svg id.
     * @param {string} actionId - Action id.
     * @param {string} assetType - Selected asset type.
     * @param {string} selectedAssetId - Selected asset id.
     */
    onConnectAction: function (svgId, actionId, assetType, selectedAssetId) {
        this.drawingController.drawingState = 'startConnect';
        this.drawingController.connectAssets['assetFrom'] = {
            assetType: assetType,
            assetId: selectedAssetId
        };
        this.drawingController.svgControl.getController("HighlightController").highlightAsset(selectedAssetId, {
            svgId: svgId,
            color: this.defaultHighlightColor,
            persistFill: true,
            overwriteFill: true
        });
        this.drawingController.svgControl.getAddOn('InfoWindow').setText(getMessage('connectActionFirstAsset'));
        this.drawingController.svgControl.getAddOn('InfoWindow').appendText('<br>' + getMessage('firstAssetSelected') + ': [' + selectedAssetId + ']');
    },
    /**
     * On close info window event.
     */
    onCloseInfoWindow: function () {
        if (this.drawingController.drawingState == 'startConnect' || this.drawingController.drawingState == 'finishConnect') {
            this.resetDrawingHighlights();
        }
        this.drawingController.drawingState = 'select';
    },
    /**
     * Highlight open jacks by Telecom service type.
     * @param {string} tcService - Telecom service type.
     */
    highlightOpenJacks: function (tcService) {
        this.drawingController.resetHighlight();
        _.each(_.keys(this.drawingController.drawings), function (svgId) {
            var drawing = this.drawingController.drawings[svgId];
            var blId = drawing.pkeyValues.blId,
                flId = drawing.pkeyValues.flId;
            var result = Workflow.callMethod('AbAssetEAM-TelecomService-highlightRoomsWithOpenJacks', blId, flId, tcService || '');
            var roomIds = result.data;
            for ( var i = 0; i < roomIds.length; i++ ) {
                var assetId = blId + ';' + flId + ';' + roomIds[i];
                this.drawingController.svgControl.getController("HighlightController").highlightAsset(assetId, {
                    svgId: svgId,
                    color: 'yellow',
                    persistFill: true,
                    overwriteFill: true
                });
            }
        }, this);
    },
    /**
     * Selects and display asset tab details.
     * @param {string} svgId - Drawing svg id.
     * @param {string} actionId - Action id.
     * @param {string} assetType - Selected asset type.
     */
    showAssetDetails: function (svgId, actionId, assetType) {
        var detailTabs = this.getDetailsTab();
        var currentTab = detailTabs.findTab(detailTabs.selectedTabName);
        if (currentTab.isContentLoaded) {
            var selectedAssets = this.drawingController.selectedAssets[svgId];
            var drawingRestriction = {
                assetType: assetType,
                selectedAssets: selectedAssets
            };
            this.existsAssetsToQuery = !_.isEmpty(selectedAssets);
            var controller = currentTab.getContentFrame().View.controllers.get('telConsCommonController');
            detailTabs.clearParameters();
            detailTabs.addParameter('drawingRestriction', drawingRestriction);
            controller.queryAssetData();
        }
    },
    /**
     * Open connection form to connect two assets.
     * @param {string} svgId - Drawing svg id.
     * @param {Object} connectAssets - List of connected assets.
     */
    connectAssets: function (svgId, connectAssets) {
        var assetFrom = connectAssets['assetFrom'];
        var assetTo = connectAssets['assetTo'];
        if (!this.canConnect(assetFrom.assetType, assetTo.assetType)) {
            this.drawingController.svgControl.getAddOn('InfoWindow').setText(getMessage('connectionNotAllowed'));
            return;
        }
        if (!this.hasConnectionPorts(assetFrom)) {
            return;
        }
        var restrictionAsset = this.createConnectRestriction(assetFrom.assetType, assetFrom.assetId);
        var viewName = "ab-eam-telecom-console-connect.axvw";
        View.openDialog(viewName, restrictionAsset, false, {
            assetType: assetFrom.assetType,
            connectAssets: connectAssets,
            width: 780,
            height: 500,
            callback: function (connectAssets) {
                View.closeThisDialog();
                View.controllers.get('abEamTcController').traceConnectedAssets(svgId, connectAssets);
            }
        });
    },
    /**
     * Trace connected assets.
     * @param {string} svgId - Drawing svg id.
     * @param {Object} connectAssets - List of connected assets.
     */
    traceConnectedAssets: function (svgId, connectAssets) {
        var assetFrom = connectAssets['assetFrom'];
        var assetTo = connectAssets['assetTo'];
        this.drawingController.svgControl.getController("HighlightController").traceAssets(
            svgId,
            assetFrom.assetId,
            assetTo.assetId,
            'purple'
        );
        this.drawingController.connectAssets = [];
        this.drawingController.drawingState = 'finishConnect';
        this.drawingController.svgControl.getAddOn('InfoWindow').setText(getMessage('connectionDone'));
    },
    /**
     * Clears console restrictions drawing and asset details panels.
     * Trigger on 'Clear' action console.
     */
    clearConsole: function () {
        // clear drawing content
        this.drawingController.clearDrawingPanel();
        // reset drawing title
        this.drawingController.setDrawingPanelTitle(getMessage('noFlSelected'));
        // clear asset details
        this.clearAssetDetailRestriction(true);
    },
    /**
     * Reset drawing highlight and clear asset tab restrictions.
     */
    resetDrawingHighlights: function () {
        this.drawingController.resetDrawingHighlights();
        // if exists assets to query clear asset details restrictions
        if (this.existsAssetsToQuery) {
            this.clearAssetDetailRestriction();
        }
    },
    /**
     * On reset drawing action.
     */
    onReset: function () {
        this.resetDrawingHighlights();
        this.drawingController.svgControl.getAddOn('InfoWindow').show(false);
    },
    /**
     * Clears asset details tab restrictions.
     * @param {boolean} [hideAssetDetails]  - If exists, hide asset details tabs.
     */
    clearAssetDetailRestriction: function (hideAssetDetails) {
        var detailTabs = this.getDetailsTab();
        var currentTab = detailTabs.findTab(detailTabs.selectedTabName);
        if (currentTab.isContentLoaded) {
            var controller = currentTab.getContentFrame().View.controllers.get('telConsCommonController');
            controller.clearDrawingSelection();
        }
        if (hideAssetDetails) {
            detailTabs.tabPanel.container.dom.style.display = "none";
        }
        this.existsAssetsToQuery = false;
    },
    /**
     * Called after an asset action is done to highlight the selected asset.
     * @param {string} svgId - Drawing svg id.
     * @param {string} action - Action type.
     * @param {string} selectedAssetId - Selected asset id.
     * @param {boolean} isTrace - Used only for trace connections; the highlight selected asset is not cleared.
     */
    highlightAssetOnAction: function (svgId, action, selectedAssetId, isTrace) {
        var color = this.defaultHighlightColor;
        if ('traceConnections' === action) {
            color = 'orange';
        } else if ('highlightConnectedRooms' === action) {
            color = 'blue';
        }
        this.drawingController.svgControl.getController("HighlightController").highlightAsset(selectedAssetId, {
            svgId: svgId,
            color: color,
            persistFill: true,
            overwriteFill: true,
            ignorePersistFill: isTrace || false
        });
    },
    /**
     * List connection
     * @param {string} svgId - Drawing svg id.
     * @param {string} assetType - Asset type.
     * @param {string} selectedAssetId - Selected asset id.
     * @param {Function} selectAssetFn - Callback function.
     */
    onListConnections: function (svgId, assetType, selectedAssetId, selectAssetFn) {
        var restriction = new Ab.view.Restriction();
        restriction.addClause(assetType + "." + assetType + "_id", selectedAssetId, "=");
        if (("eq" === assetType && this.hasAssetPort("eq", "eqport", restriction))
            || ("pn" === assetType)
            || ("fp" === assetType)) {
            restriction = new Ab.view.Restriction();
            restriction.addClause('eq.asset_type', assetType);
            restriction.addClause('eq.asset_id', selectedAssetId);
            View.openDialog("ab-eam-telecom-console-select-ports.axvw", restriction, false, {
                assetType: assetType,
                assetId: selectedAssetId,
                actionTitle: getMessage("listConnectionsAction"),
                multipleTrace: true,
                width: 800,
                height: 500,
                callback: function (assetType, assetId, selectedRecords) {
                    View.closeDialog();
                    View.controllers.get('abEamTcController').listAssetConnections(svgId, assetType, assetId, selectedRecords, selectAssetFn);
                }
            });
        } else {
            this.listAssetConnections(svgId, assetType, selectedAssetId, [], selectAssetFn);
        }
    },
    /**
     * Opens view with list of asset connections.
     * @param {string} svgId - Drawing svg id.
     * @param {string} assetType - Asset type.
     * @param {string} assetId - Asset id.
     * @param {Array.<Ab.data.Record>} selectedRecords - Selected port records.
     * @param {Function} selectAssetFn - Callback function.
     */
    listAssetConnections: function (svgId, assetType, assetId, selectedRecords, selectAssetFn) {
        this.drawingController.resetHighlight();
        this.drawingController.svgControl.getAddOn('InfoWindow').setText(getMessage("listConnectionsStart"));
        var assetPorts = this.prepareAssetPorts(selectedRecords);
        try {
            var result = Workflow.callMethod('AbAssetEAM-TelecomService-listAssetConnections', assetType, assetId, assetPorts);
            if (result.code == 'executed') {
                var connections = result.data;
                View.openDialog("ab-eam-telecom-console-list-connections.axvw", null, false, {
                    width: 900,
                    height: 600,
                    connections: connections
                });
                selectAssetFn();
            }
        } catch (e) {
            Workflow.handleError(e);
        }
        this.drawingController.svgControl.getAddOn('InfoWindow').appendText('<br>' + getMessage("listConnectionsComplete"));
    },
    /**
     * Trace connections for selected asset.
     * @param {string} svgId - Drawing svg id.
     * @param {string} assetType - Asset type.
     * @param {string} assetId - Asset id.
     * @param {Function} selectAssetFn - Callback function.
     */
    onTraceAssetsConnections: function (svgId, assetType, assetId, selectAssetFn) {
        var restriction = new Ab.view.Restriction();
        restriction.addClause(assetType + "." + assetType + "_id", assetId, "=");
        if (("eq" === assetType && this.hasAssetPort("eq", "eqport", restriction))
            || ("pn" === assetType)
            || ("fp" === assetType)) {
            restriction = new Ab.view.Restriction();
            restriction.addClause('eq.asset_type', assetType);
            restriction.addClause('eq.asset_id', assetId);
            View.openDialog("ab-eam-telecom-console-select-ports.axvw", restriction, false, {
                assetType: assetType,
                assetId: assetId,
                actionTitle: getMessage("traceConnectionsAction"),
                multipleTrace: true,
                width: 800,
                height: 500,
                callback: function (assetType, assetId, selectedRecords) {
                    View.closeDialog();
                    View.controllers.get('abEamTcController').traceAssetConnections(svgId, assetType, assetId, selectedRecords, selectAssetFn);
                }
            });
        } else {
            this.traceAssetConnections(svgId, assetType, assetId, [], selectAssetFn);
        }
    },
    /**
     * Trace asset connections.
     * @param {string} svgId - Drawing svg id.
     * @param {string} assetType - Asset type.
     * @param {string} selectedAssetId - Selected asset id.
     * @param {Array.<Ab.data.Record>} selectedRecords - Selected port records.
     * @param {Function} selectAssetFn - Callback function.
     */
    traceAssetConnections: function (svgId, assetType, selectedAssetId, selectedRecords, selectAssetFn) {
        this.drawingController.resetHighlight();
        this.resetMissingAssets('trace');
        this.drawingController.svgControl.getAddOn('InfoWindow').setText(getMessage("traceConnectionsStart"));
        var assetPorts = this.prepareAssetPorts(selectedRecords);
        try {
            var result = Workflow.callMethod('AbAssetEAM-TelecomService-traceAssetConnections', assetType, selectedAssetId, assetPorts);
            if (result.code == 'executed') {
                var connectionsList = result.data;
                this.doTrace(svgId, connectionsList);
                var noConnPortsList = [];
                var hasConnections = true;
                for ( var i = 0; i < connectionsList.length; i++ ) {
                    var assetId = connectionsList[i]['assetId'];
                    var towardClientConnections = connectionsList[i].towardClientConnections;
                    var towardServerConnections = connectionsList[i].towardServerConnections;
                    if (towardClientConnections.length == 0 && towardServerConnections.length == 0) {
                        hasConnections = false;
                        if (valueExistsNotEmpty(connectionsList[i].assetPort)) {
                            noConnPortsList.push(connectionsList[i].assetPort);
                        }
                    }
                }
                if (assetPorts.length == 0 && !hasConnections) {
                    this.drawingController.svgControl.getAddOn('InfoWindow').appendText('<br>' + String.format(getMessage("noConnectionsToHighlight"), getMessage(assetType + "_highlight"), selectedAssetId))
                } else if (noConnPortsList.length > 0) {
                    this.drawingController.svgControl.getAddOn('InfoWindow').appendText('<br>' + String.format(getMessage("noConnectionsToHighlightPorts"), getMessage(assetType + "_highlight"), selectedAssetId, noConnPortsList.join()));
                }
                if (connectionsList.length > 0) {
                    selectAssetFn();
                    this.drawingController.drawingState = 'trace';
                }
                this.showMissingTraceAssets();
            }
        } catch (e) {
            Workflow.handleError(e);
        }
        this.drawingController.svgControl.getAddOn('InfoWindow').appendText('<br>' + getMessage("traceConnectionsComplete"));
    },
    /**
     * Trace assets.
     * @param {string} svgId - Drawing svg id.
     * @param {Array.<Object>} connections - List of connections.
     */
    doTrace: function (svgId, connections) {
        for ( var i = 0; i < connections.length; i++ ) {
            var assetId = connections[i]['assetId'];
            var towardClientConnections = connections[i].towardClientConnections;
            var towardServerConnections = connections[i].towardServerConnections;
            if (towardClientConnections.length > 0) {
                this.traceTowardClientConnections(svgId, assetId, towardClientConnections);
            }
            if (towardServerConnections.length > 0) {
                this.traceTowardServerConnections(svgId, assetId, towardServerConnections);
            }
        }
    },
    /**
     * Show missing trace assets.
     */
    showMissingTraceAssets: function () {
        var missingAssets = this.getMissingAssets('trace');
        var missingAssetsMsg = getMessage('assetsNotFound') + ':\n';
        var hasError = false;
        if (missingAssets.assetFrom.length > 0) {
            missingAssetsMsg += '\n' + getMessage('assetsNotFoundFrom') + ': [' + missingAssets.assetFrom.toString() + ']';
            hasError = true;
        }
        if (missingAssets.assetTo.length > 0) {
            missingAssetsMsg += '\n' + getMessage('assetsNotFoundTo') + ': [' + missingAssets.assetTo.toString() + ']';
            hasError = true;
        }
        if (hasError) {
            this.drawingController.svgControl.getAddOn('InfoWindow').appendText('<br>' + missingAssetsMsg);
            this.resetMissingAssets('trace');
        }
    },
    /**
     * Trace toward the client connections.
     * Case connectFrom is fp and is a missing asset, do trace with jack.
     * @param {string} svgId - Drawing svg id.
     * @param {string} assetId - Asset id.
     * @param {Array.<Object>} connections - List of connections.
     */
    traceTowardClientConnections: function (svgId, assetId, connections) {
        var connectTo = assetId;
        for ( var i = 0; i < connections.length; i++ ) {
            var connectFrom = connections[i]['connectedAssetId'];
            var connectedAssetPort = connections[i]['connectedAssetPort'];
            var connectedAssetType = connections[i]['connectedAssetType'];
            this.traceAssets(svgId, connectFrom, connectTo);
            var missingAssets = this.getMissingAssets('trace');
            if ('fp' === connectedAssetType && missingAssets.assetFrom.length > 0) {
                for ( var index = 0; index < missingAssets.assetFrom.length; index++ ) {
                    if (missingAssets.assetFrom[index] === connectFrom) {
                        this.traceAssets(svgId, connectedAssetPort, connectTo);
                        missingAssets.assetFrom.splice(index, 1);
                        connectFrom = connectedAssetPort;
                        break;
                    }
                }
            }
            var isMultiplexing = connections[i]['is_multiplexing'] || false;
            if (isMultiplexing) {
                var multiplexingConnections = connections[i]['multiplexingConnections'];
                this.doTrace(svgId, multiplexingConnections);
            }
            connectTo = connectFrom;
        }
    },
    /**
     * Trace toward the server connections.
     * Case connectTo is fp and is a missing asset, do trace with jack.
     * @param {string} svgId - Drawing svg id.
     * @param {string} assetId - Asset id.
     * @param {Array.<Object>} connections - List of connections.
     */
    traceTowardServerConnections: function (svgId, assetId, connections) {
        var connectFrom = assetId;
        for ( var i = 0; i < connections.length; i++ ) {
            var connectTo = connections[i]['connectedAssetId'];
            var connectedAssetPort = connections[i]['connectedAssetPort'];
            var connectedAssetType = connections[i]['connectedAssetType'];
            this.traceAssets(svgId, connectFrom, connectTo);
            var missingAssets = this.getMissingAssets('trace');
            if ('fp' === connectedAssetType && missingAssets.assetTo.length > 0) {
                for ( var index = 0; index < missingAssets.assetTo.length; index++ ) {
                    if (missingAssets.assetTo[index] === connectTo) {
                        this.traceAssets(svgId, connectFrom, connectedAssetPort);
                        missingAssets.assetTo.splice(index, 1);
                        connectTo = connectedAssetPort;
                        break;
                    }
                }
            }
            var isMultiplexing = connections[i]['is_multiplexing'] || false;
            if (isMultiplexing) {
                var multiplexingConnections = connections[i]['multiplexingConnections'];
                this.doTrace(svgId, multiplexingConnections);
            }
            connectFrom = connectTo;
        }
    },
    /**
     * Highlight and draw a line between two assets with the specified color.
     * @param {string} svgId - Drawing svg id.
     * @param {string} assetFrom - Asset from id.
     * @param {string} assetTo - Asset to id.
     */
    traceAssets: function (svgId, assetFrom, assetTo) {
        this.drawingController.svgControl.getController("HighlightController").traceAssets(svgId, assetFrom, assetTo, 'blue');
    },
    /**
     * Highlight connected rooms.
     * @param {string} svgId - Drawing svg id.
     * @param {string} assetType - Asset type.
     * @param {string} assetId - Asset id.
     * @param {Function} selectAssetFn - Callback function.
     */
    onHighlightConnectedRooms: function (svgId, assetType, assetId, selectAssetFn) {
        var restriction = new Ab.view.Restriction();
        restriction.addClause(assetType + "." + assetType + "_id", assetId, "=");
        if (("eq" === assetType && this.hasAssetPort("eq", "eqport", restriction))
            || ("pn" === assetType)
            || ("fp" === assetType)) {
            restriction = new Ab.view.Restriction();
            restriction.addClause('eq.asset_type', assetType);
            restriction.addClause('eq.asset_id', assetId);
            View.openDialog("ab-eam-telecom-console-select-ports.axvw", restriction, false, {
                assetType: assetType,
                assetId: assetId,
                actionTitle: getMessage("highlightConnectedRoomsAction"),
                multipleTrace: true,
                width: 800,
                height: 500,
                callback: function (assetType, assetId, selectedRecords) {
                    View.closeDialog();
                    View.controllers.get('abEamTcController').highlightConnectedRooms(svgId, assetType, assetId, selectedRecords, selectAssetFn);
                }
            });
        } else {
            this.highlightConnectedRooms(svgId, assetType, assetId, [], selectAssetFn);
        }
    },
    /**
     * Highlight connected room spaces on drawing.
     * @param {string} svgId - Drawing svg id.
     * @param {string} assetType - Asset type.
     * @param {string} assetId - Asset id.
     * @param {Array.<Ab.data.Record>} selectedRecords - Selected port records.
     * @param {Function} selectAssetFn - Callback function.
     */
    highlightConnectedRooms: function (svgId, assetType, assetId, selectedRecords, selectAssetFn) {
        this.drawingController.resetHighlight();
        this.drawingController.svgControl.getAddOn('InfoWindow').setText(getMessage("highlightConnectionsStart"));
        var assetPorts = this.prepareAssetPorts(selectedRecords);
        try {
            var result = Workflow.callMethod('AbAssetEAM-TelecomService-highlightConnectedRooms', assetType, assetId, assetPorts);
            if (result.code == 'executed') {
                var connectionsList = result.data;
                if (connectionsList && connectionsList.length > 0) {
                    this.doHighlightRooms(svgId, connectionsList);
                } else {
                    this.drawingController.svgControl.getAddOn('InfoWindow').setText(String.format(getMessage("noSpacesToHighlight"), getMessage(assetType + "_highlight"), assetId));
                }
                if (connectionsList.length > 0) {
                    selectAssetFn();
                }
            }
        } catch (e) {
            Workflow.handleError(e);
        }
        this.drawingController.svgControl.getAddOn('InfoWindow').appendText('<br>' + getMessage("highlightConnectionsComplete"));
    },
    /**
     * Highlight rooms.
     * @param {string} svgId - Drawing svg id.
     * @param {Array.<Object>} connections - List of connections.
     */
    doHighlightRooms: function (svgId, connections) {
        for ( var i = 0; i < connections.length; i++ ) {
            var towardClientConnections = connections[i].towardClientConnections;
            var towardServerConnections = connections[i].towardServerConnections;
            if (towardClientConnections.length > 0) {
                this.highlightRooms(svgId, towardClientConnections);
            }
            if (towardServerConnections.length > 0) {
                this.highlightRooms(svgId, towardServerConnections);
            }
        }
    },
    /**
     * Highlight rooms on drawing.
     * @param {string} svgId - Drawing svg id.
     * @param {Array.<Object>} connections - List of connections.
     */
    highlightRooms: function (svgId, connections) {
        for ( var i = 0; i < connections.length; i++ ) {
            var location = connections[i]['location'];
            var blId = location['blId'];
            var flId = location['flId'];
            var rmId = location['rmId'];
            var locator = blId + ';' + flId + ';' + rmId;
            this.drawingController.svgControl.getController("HighlightController").highlightAsset(locator, {
                svgId: svgId,
                color: 'yellow',
                persistFill: true,
                overwriteFill: true
            });
            var isMultiplexing = connections[i]['is_multiplexing'] || false;
            if (isMultiplexing) {
                var multiplexingConnections = connections[i]['multiplexingConnections'];
                this.doHighlightRooms(svgId, multiplexingConnections);
            }
        }
        this.drawingController.drawingState = 'highlightConnectedRooms';
    },
    /**
     * Check if asset has port.
     * @param {string} assetTypeTable - Asset type table.
     * @param {string} assetTypePortTable - Asset type port table.
     * @param {Ab.view.Restriction} restriction - Restriction to apply.
     * @returns {boolean} true if asset has ports.
     */
    hasAssetPort: function (assetTypeTable, assetTypePortTable, restriction) {
        var params = {
            tableName: assetTypePortTable,
            isDistinct: 'true',
            fieldNames: toJSON([assetTypePortTable + '.' + assetTypeTable + '_id']),
            restriction: toJSON(restriction)
        };
        try {
            var result = Workflow.call('AbCommonResources-getDataRecords', params);
            if (result.code == 'executed') {
                return (result.dataSet.records.length > 0);
            }
        } catch (e) {
            Workflow.handleError(e);
            return false;
        }
        return false;
    },
    /**
     * Check if selected asset has ports.
     * @param {Object} asset - Asset object.
     * @returns {boolean} true if asset has connection ports.
     */
    hasConnectionPorts: function (asset) {
        var hasConnectionPorts = true;
        var restriction = new Ab.view.Restriction();
        restriction.addClause(asset.assetType + "." + asset.assetType + "_id", asset.assetId, "=");
        if ("pn" === asset.assetType && !this.hasAssetPort("pn", "pnport", restriction)) {
            this.drawingController.svgControl.getAddOn('InfoWindow').appendText('<br>' + getMessage('noPortDefinedToConnect') + ': [' + asset.assetId + ']');
            hasConnectionPorts = false;
        } else if ("fp" === asset.assetType && !this.hasAssetPort("fp", "jk", restriction)) {
            this.drawingController.svgControl.getAddOn('InfoWindow').appendText('<br>' + getMessage('noJackDefinedToConnect') + ': [' + asset.assetId + ']');
            hasConnectionPorts = false;
        }
        return hasConnectionPorts;
    },
    /**
     * Check if selected assets can connect.
     * @param {string} fromAssetType - From asset type.
     * @param {string} toAssetType - To asset type.
     * @returns {boolean} true if connection allowed
     */
    canConnect: function (fromAssetType, toAssetType) {
        var canConnect = true;
        // only eq can connect to jk
        if (('jk' === fromAssetType || 'fp' === fromAssetType || 'pn' === fromAssetType) && ('jk' === toAssetType || 'fp' === toAssetType)) {
            canConnect = false;
        }
        // cannot connect to a room
        if ('rm' === fromAssetType || 'rm' === toAssetType) {
            canConnect = false;
        }
        return canConnect;
    },
    /**
     * Create connect restriction.
     * @param {string} assetType - Asset type.
     * @param {string} assetId - Asset id.
     * @returns {Ab.view.Restriction} - Connection restriction.
     */
    createConnectRestriction: function (assetType, assetId) {
        var restrictionView = new Ab.view.Restriction();
        restrictionView.addClause('eq.asset_type', ('pn' === assetType) ? "pnport" : assetType);
        restrictionView.addClause('eq.asset_code', assetId);
        return restrictionView;
    },
    /**
     * Reset missing assets.
     * @param {string} type - Asset type.
     */
    resetMissingAssets: function (type) {
        this.drawingController.svgControl.getController("HighlightController").resetMissingAssets(type);
    },
    /**
     * Prepare asset ports before calling the WRF rules for asset actions.
     * @param {Array.<Ab.data.Record>} selectedRecords - List of selected records.
     * @returns {Array.<Ab.data.Record>} list of assets with ports.
     */
    prepareAssetPorts: function (selectedRecords) {
        var assetPorts = [];
        for ( var i = 0; i < selectedRecords.length; i++ ) {
            var record = selectedRecords[i];
            var assetPort = record.getValue('eq.asset_port');
            assetPorts.push(assetPort);
        }
        return assetPorts;
    },
    /**
     * Get missing assets by type
     * @param {string} type - Type of action, can be 'trace', 'highlight', or not specified.
     * @returns {Object} missing assets in JSON format of:
     *             {trace: {assetFrom: [],assetTo: []}, highlight: {assets: []}},
     */
    getMissingAssets: function (type) {
        return this.drawingController.svgControl.getController("HighlightController").getMissingAssets(type);
    },
    /**
     * Get details tab.
     * @returns {Ab.tabs.Tabs} - Details tabs panel.
     */
    getDetailsTab: function () {
        return View.panels.get('drawingDetailsTabs').contentView.panels.get('abEamTelConsTabs');
    },
    /**
     * Create filter restriction for asset.
     * @param assetType - Asset type.
     * @param blId - Building id.
     * @param flId - Floor id.
     * @param rmId - Room id.
     * @param eqId - Equipment id.
     * @param eqStd - Equipment standard.
     * @param emId - Employee id.
     * @returns {*|string} Sql restriction
     */
    createFilterRestriction: function (assetType, blId, flId, rmId, eqId, eqStd, emId) {
        var restriction = this.createAssetRestriction(assetType, blId, flId, rmId);
        if (valueExistsNotEmpty(eqId) || valueExistsNotEmpty(eqStd)) {
            var eqSql = " AND EXISTS (SELECT 1 FROM eq WHERE eq.bl_id = " + assetType + ".bl_id " +
                "AND eq.fl_id = " + assetType + ".fl_id " +
                "AND eq.rm_id = " + assetType + ".rm_id " +
                "AND {0})";
            restriction += String.format(eqSql, this.createEquipmentRestriction(eqId, eqStd));
        }
        if (valueExistsNotEmpty(emId)) {
            var emSql = " AND EXISTS (SELECT 1 FROM em WHERE em.bl_id = " + assetType + ".bl_id " +
                "AND em.fl_id = " + assetType + ".fl_id " +
                "AND em.rm_id = " + assetType + ".rm_id " +
                "AND {0})";
            restriction += String.format(emSql, this.createEmployeeRestriction(emId));
        }
        return restriction;
    },
    /**
     * Created equipment restriction.
     * @param {string} eqId - Equipment id.
     * @param {string} eqStd - Equipment standard.
     * @returns {string} Sql restriction.
     */
    createEquipmentRestriction: function (eqId, eqStd) {
        var restriction = "1=1";
        if (valueExistsNotEmpty(eqId)) {
            restriction += " AND eq.eq_id='" + eqId + "'";
        }
        if (valueExistsNotEmpty(eqStd)) {
            restriction += " AND eq.eq_std='" + eqStd + "'";
        }
        return restriction;
    },
    /**
     * Created employee restriction.
     * @param {string} emId - Employee id.
     * @returns {string} Sql restriction.
     */
    createEmployeeRestriction: function (emId) {
        var restriction = "1=1";
        if (valueExistsNotEmpty(emId)) {
            restriction += " AND em.em_id='" + emId + "'";
        }
        return restriction;
    },
    /**
     * Create asset restriction.
     * @param assetType - Asset type.
     * @param blId - Building id.
     * @param flId - Floor id.
     * @param rmId - Room id.
     * @param eqId - Equipment id.
     * @param eqStd - Equipment standard.
     * @returns {string} SQL restriction
     */
    createAssetRestriction: function (assetType, blId, flId, rmId, eqId, eqStd) {
        var restriction = "1=1";
        if (valueExistsNotEmpty(blId)) {
            restriction += " AND " + assetType + ".bl_id='" + blId + "'";
        }
        if (valueExistsNotEmpty(flId)) {
            restriction += " AND " + assetType + ".fl_id='" + flId + "'";
        }
        if (valueExistsNotEmpty(rmId)) {
            restriction += " AND " + assetType + ".rm_id='" + rmId + "'";
        }
        if (valueExistsNotEmpty(eqId)) {
            restriction += " AND " + assetType + ".eq_id='" + eqId + "'";
        }
        if (valueExistsNotEmpty(eqStd)) {
            restriction += " AND " + assetType + ".eq_std='" + eqStd + "'";
        }
        return restriction;
    }
});