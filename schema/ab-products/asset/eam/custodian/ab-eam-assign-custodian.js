/**
 * Assign custodian controller: works for Change owner and Assign Custodian
 */
var abEamAssignCustodianController = View.createController('abEamAssignCustodianController', {
    // callback function
    callbackFunction: null,
    // is onwer
    isOwner: false,
    // array  with selected assets and asset types
    selectedAssets: [],
    // is new record
    newRecord: false,
    // is custom restriction is used to get the asset list
    useCustomRestriction: false,
    // custom restriction for asset list
    customRestriction: {},
    // restriction
    restriction: null,
    visibleForm: null,
    //source table after selected custodian
    sourceTable: null,
    // source record after selected custodian
    sourceRecord: null,
    // list of team.autonumbered_id after owner records are created
    teamPkeyIds: null,
    afterViewLoad: function () {
        this.newRecord = this.view.newRecord;
        this.newRecord = true;
        if (valueExists(this.view.restriction)) {
            this.restriction = this.view.restriction;
        }
        if (valueExists(this.view.parameters)) {
            if (valueExists(this.view.parameters.callback)) {
                this.callbackFunction = this.view.parameters.callback;
            }
            if (!valueExists(this.restriction) && valueExists(this.view.parameters.restriction)) {
                this.restriction = this.view.parameters.restriction;
            }
            if (valueExists(this.view.parameters.isOwner)) {
                this.isOwner = this.view.parameters.isOwner;
            }
            if (valueExists(this.view.parameters.selectedAssets)) {
                this.selectedAssets = this.view.parameters.selectedAssets;
            }
            if (valueExists(this.view.parameters.useCustomRestriction)) {
                this.useCustomRestriction = this.view.parameters.useCustomRestriction;
            }
            if (valueExists(this.view.parameters.customRestriction)) {
                this.customRestriction = this.view.parameters.customRestriction;
            }
        }
        var controller = this;
        // customize grid columns
        this.abEamAssignCustodianAssets_list.afterCreateCellContent = function (row, column, cellElement) {
            controller.customizeAssetRegistryFields('bl', row, column, cellElement);
        };
        this.abEamAssignCustodianOther_list.afterCreateCellContent = function (row, column, cellElement) {
            controller.customizeAssetRegistryFields('team', row, column, cellElement);
        };
        this.abEamAssignCustodianAssets_list.addParameter('useCustodian', true);
        if (this.isOwner) {
            this.visibleForm = this.abEamAssignCustodianOwner_form;
            View.panels.get('abEamAssignCustodianOther_form').show(false, true);
            View.panels.get('abEamAssignCustodianOwner_form').show(true, true);
            View.panels.get('abEamAssignCustodianOther_list').show(false);
            View.panels.get('abEamAssignCustodianAssets_list').selectionEnabled = false;
            var mainLayout = View.getLayoutManager('mainLayout');
            mainLayout.collapseRegion('south');
            this.visibleForm.addFieldEventListener('team.digital_signature', Ab.form.Form.DOC_EVENT_CHECKIN, this.copyDocument, this);
            this.visibleForm.addFieldEventListener('team.digital_signature', Ab.form.Form.DOC_EVENT_DELETE, this.deleteDocument, this);
            this.visibleForm.updateDocumentButtons = this.updateDocumentButtons.createDelegate(this.visibleForm);
        } else {
            this.visibleForm = this.abEamAssignCustodianOther_form;
            View.panels.get('abEamAssignCustodianOwner_form').show(false, true);
            View.panels.get('abEamAssignCustodianOther_form').show(true, true);
            View.panels.get('abEamAssignCustodianOther_list').show(true);
        }
    },
    afterInitialDataFetch: function () {
        var formTitle = "";
        if (this.isOwner) {
            this.visibleForm = this.abEamAssignCustodianOwner_form;
            formTitle = getMessage('titleAssignOwner');
        } else {
            this.visibleForm = this.abEamAssignCustodianOther_form;
            formTitle = getMessage('titleAssignOther');
        }
        this.visibleForm.refresh(this.restriction, this.newRecord);
        this.visibleForm.setTitle(formTitle);
        this.prepareAssetRegistryRestriction();
        this.abEamAssignCustodianAssets_list.addParameter('blTypeRestriction', this.customRestriction['blTypeRestriction']);
        this.abEamAssignCustodianAssets_list.addParameter('propertyTypeRestriction', this.customRestriction['propertyTypeRestriction']);
        this.abEamAssignCustodianAssets_list.addParameter('eqTypeRestriction', this.customRestriction['eqTypeRestriction']);
        this.abEamAssignCustodianAssets_list.addParameter('taTypeRestriction', this.customRestriction['taTypeRestriction']);
        this.abEamAssignCustodianAssets_list.addParameter('sqlTypeRestriction', this.customRestriction['sqlTypeRestriction']);
        this.abEamAssignCustodianAssets_list.refresh();
        if (!this.isOwner) {
            var firstGridRow = this.abEamAssignCustodianAssets_list.gridRows.get(0);
            if (valueExists(firstGridRow)) {
                this.onClickAsset(firstGridRow);
                this.abEamAssignCustodianAssets_list.selectRow(0);
            }
        }
    },
    onClickAsset: function (gridRow) {
        if (!this.isOwner) {
            var record = gridRow.getRecord();
            var assetType = record.getValue("bl.asset_type"),
                assetId = record.getValue("bl.asset_id"),
                assetStd = record.getValue("bl.asset_std");
            var otherRestriction = new Ab.view.Restriction();
            otherRestriction.addClause('team.team_type', 'Equipment', '=');
            otherRestriction.addClause('team.custodian_type', null, 'IS NOT NULL');
            otherRestriction.addClause('team.is_owner', 0, '=');
            if ('bl' === assetType) {
                otherRestriction.addClause('team.bl_id', assetId, '=');
            } else if ('property' === assetType) {
                otherRestriction.addClause('team.pr_id', assetId, '=');
            } else if ('eq' === assetType) {
                otherRestriction.addClause('team.eq_id', assetId, '=');
            } else if ('ta' === assetType) {
                otherRestriction.addClause('team.ta_id', assetId, '=');
            }
            this.abEamAssignCustodianOther_list.refresh(otherRestriction);
            var assetTitle = "{0} - {1}".replace('{0}', assetStd).replace('{1}', assetId);
            this.abEamAssignCustodianOther_list.setTitle(getMessage('titleOtherCustodians').replace('{0}', assetTitle));
        }
    },
    abEamAssignCustodianOther_form_afterRefresh: function () {
        this.abEamAssignCustodianOther_form.getFieldElement('custodian_name').readOnly = true;
    },
    abEamAssignCustodianOwner_form_afterRefresh: function (formPanel) {
        if (formPanel.newRecord) {
            formPanel.getFieldElement('custodian_name').readOnly = true;
        } else {
            this.setTeamMember(formPanel, this.sourceTable, this.sourceRecord);
            formPanel.enableField('custodian_name', false);
            formPanel.enableField('team.date_start', false);
            formPanel.enableField('team.notes', false);
            if (this.selectedAssets.length > 1) {
                var instructionsEl = formPanel.fields.get('team.digital_signature').getInstructionsEl();
                if (instructionsEl) {
                    instructionsEl.dom.innerHTML = getMessage('digitalSignatureInstructions');
                }
            }
            formPanel.actions.get('save').show(false);
        }
    },
    abEamAssignCustodianOther_form_onSave: function () {
        this.onSaveCommon(this.abEamAssignCustodianOther_form, true);
    },
    abEamAssignCustodianOther_form_onSaveAndClose: function () {
        this.onSaveCommon(this.abEamAssignCustodianOther_form, false);
    },
    abEamAssignCustodianOwner_form_onSave: function () {
        this.onSaveCommon(this.abEamAssignCustodianOwner_form, true);
    },
    onSaveCommon: function (panelForm, addMultipleCustodians) {
        if (this.isOwner) {
            if (panelForm.newRecord && panelForm.canSave()
                && this.isValidForm(panelForm, this.isOwner)) {
                var confirmMessage = this.prepareConfirmMessageNewOwner(panelForm);
                View.confirm(confirmMessage, function (button) {
                    if ('yes' === button) {
                        var dataRecord = panelForm.getOutboundRecord();
                        var controller = View.controllers.get('abEamAssignCustodianController');
                        controller.changeOwner(dataRecord, addMultipleCustodians);
                    }
                });
            }
        } else {
            if (panelForm.canSave() && this.isValidForm(panelForm, this.isOwner)) {
                var dataRecord = panelForm.getOutboundRecord();
                this.addCustodian(dataRecord, addMultipleCustodians);
            }
        }
    },
    prepareConfirmMessageNewOwner: function (panelForm) {
        var startDate = panelForm.getFieldValue('team.date_start');
        var objStartDate = panelForm.getDataSource().parseValue('team.date_start', startDate, false);
        var objTerminateDate = DateMath.add(objStartDate, DateMath.DAY, -1);
        var localizedStartDate = panelForm.getDataSource().formatValue('team.date_start', objStartDate, true);
        var localizedTerminateDate = panelForm.getDataSource().formatValue('team.date_start', objTerminateDate, true);
        return getMessage('confirmSaveNewOwner').replace('{0}', localizedStartDate).replace('{1}', localizedTerminateDate);
    },
    changeOwner: function (dataRecord, addMultipleCustodians) {
        try {
            var jobId = Workflow.startJob('AbAssetManagement-AssetManagementService-changeOwner', dataRecord, this.selectedAssets, this.useCustomRestriction, this.customRestriction);
            View.openJobProgressBar(getMessage('titleAssignOwner'), jobId, '', function (status) {
                if (status.jobStatusCode === 3) {
                    var teamPkeyIds = eval(status.jobProperties.teamPkeyIds);
                    var controller = View.controllers.get('abEamAssignCustodianController');
                    controller.refreshCustodianOwnerPanelAfterSave(teamPkeyIds);
                    if (valueExistsNotEmpty(controller.callbackFunction)) {
                        controller.callbackFunction(addMultipleCustodians);
                    }
                    View.closeProgressBar();
                }
            });
        } catch (e) {
            Workflow.handleError(e);
            return false;
        }
    },
    addCustodian: function (dataRecord, addMultipleCustodians) {
        try {
            // save using long running job
            var jobId = Workflow.startJob('AbAssetManagement-AssetManagementService-addCustodian', dataRecord, this.selectedAssets, this.useCustomRestriction, this.customRestriction);
            View.openJobProgressBar(getMessage('titleAssignOther'), jobId, '', function (status) {
                if (status.jobStatusCode === 3) {
                    var controller = View.controllers.get('abEamAssignCustodianController');
                    if (valueExistsNotEmpty(controller.callbackFunction)) {
                        controller.callbackFunction(addMultipleCustodians);
                    }
                    View.closeProgressBar();
                    if (addMultipleCustodians) {
                        controller.visibleForm.refresh(controller.restriction, true);
                        var custodianAssetsPanel = View.panels.get('abEamAssignCustodianAssets_list');
                        var gridRow = custodianAssetsPanel.gridRows.get(custodianAssetsPanel.selectedRowIndex);
                        if (valueExists(gridRow)) {
                            controller.onClickAsset(gridRow);
                            custodianAssetsPanel.selectRow(custodianAssetsPanel.selectedRowIndex);
                        }
                    }
                }
            });
        } catch (e) {
            Workflow.handleError(e);
            return false;
        }
    },
    refreshCustodianOwnerPanelAfterSave: function (teamPkeyIds) {
        if (valueExists(teamPkeyIds) && !_.isEmpty(teamPkeyIds)) {
            this.teamPkeyIds = teamPkeyIds;
            // get first id
            var teamId = teamPkeyIds[0];
            var restriction = new Ab.view.Restriction({'team.autonumbered_id': teamId});
            this.visibleForm.refresh(restriction, false);
            this.abEamAssignCustodianAssets_list.refresh();
        }
    },
    copyDocument: function (panel, fieldName, parameters) {
        var autonumberId = panel.getRecord().getValue('team.autonumbered_id');
        var teamPkeyIds = _.rest(this.teamPkeyIds); // remove first teamId
        if (teamPkeyIds.length > 0) {
            try {
                Workflow.callMethod('AbAssetManagement-AssetManagementService-copyDigitalSignatureDocument', autonumberId, teamPkeyIds, fieldName, parameters);
            } catch (e) {
                Workflow.handleError(e);
            }
        }
    },
    deleteDocument: function (panel, fieldName, parameters) {
        var autonumberId = panel.getRecord().getValue('team.autonumbered_id');
        var teamPkeyIds = _.rest(this.teamPkeyIds);  // remove first teamId
        if (teamPkeyIds.length > 0) {
            try {
                Workflow.callMethod('AbAssetManagement-AssetManagementService-deleteDigitalSignatureDocument', autonumberId, teamPkeyIds, fieldName, parameters);
            } catch (e) {
                Workflow.handleError(e);
            }
        }
    },
    isValidForm: function (objForm, isOwner) {
        if (isOwner) {
            // validate date start
            var dateStart = objForm.getFieldValue('team.date_start');
            var values = {
                "team.date_start": dateStart
            };
            try {
                var result = Workflow.callMethod('AbAssetManagement-AssetManagementService-validateNewOwner', values, this.selectedAssets, this.useCustomRestriction, this.customRestriction);
                if (result.code === 'executed') {
                    var jsonResult = JSON.parse(result.jsonExpression);
                    if (!jsonResult.valid) {
                        View.showMessage(getMessage('errInvalidOwnerStartDate'));
                        objForm.addInvalidField('team.date_start', '');
                        objForm.displayValidationResult();
                        return false;
                    }
                }
            } catch (e) {
                Workflow.handleError(e);
                return false;
            }
        } else {
            var startDate = objForm.getFieldValue('team.date_start');
            var endDate = objForm.getFieldValue('team.date_end');
            //check start date and end date
            if (valueExistsNotEmpty(startDate) && valueExistsNotEmpty(endDate)) {
                var objStartDate = objForm.getDataSource().parseValue('team.date_start', startDate, false);
                var objEndDate = objForm.getDataSource().parseValue('team.date_end', endDate, false);
                if (!DateMath.after(objEndDate, objStartDate)) {
                    View.showMessage(getMessage('errEndDateBeforeStart'));
                    objForm.addInvalidField('team.date_end', '');
                    objForm.displayValidationResult();
                    return false;
                }
            }
        }
        return true;
    },
    /**
     * Prepare asset registry restriction
     */
    prepareAssetRegistryRestriction: function () {
        if (!this.useCustomRestriction) {
            this.customRestriction['blTypeRestriction'] = "1=1";
            this.customRestriction['propertyTypeRestriction'] = "1=1";
            this.customRestriction['eqTypeRestriction'] = "1=1";
            this.customRestriction['taTypeRestriction'] = "1=1";
            this.customRestriction['sqlTypeRestriction'] = "1=1";
            var sqlAssets = [];
            for ( var i = 0; i < this.selectedAssets.length; i++ ) {
                var objAsset = this.selectedAssets[i]
                sqlAssets.push("(bl.asset_type = '{0}' AND bl.asset_id = '{1}')".replace('{0}', objAsset['asset_type']).replace('{1}', objAsset['asset_id']));
            }
            if (sqlAssets.length > 0) {
                this.customRestriction['sqlTypeRestriction'] = "(" + sqlAssets.join(" OR ") + ")";
            }
        } else {
            if (!valueExistsNotEmpty(this.customRestriction['blTypeRestriction'])) {
                this.customRestriction['blTypeRestriction'] = "1=1";
            }
            if (!valueExistsNotEmpty(this.customRestriction['eqTypeRestriction'])) {
                this.customRestriction['eqTypeRestriction'] = "1=1";
            }
            if (!valueExistsNotEmpty(this.customRestriction['propertyTypeRestriction'])) {
                this.customRestriction['propertyTypeRestriction'] = "1=1";
            }
            if (!valueExistsNotEmpty(this.customRestriction['taTypeRestriction'])) {
                this.customRestriction['taTypeRestriction'] = "1=1";
            }
            if (!valueExistsNotEmpty(this.customRestriction['sqlTypeRestriction'])) {
                this.customRestriction['sqlTypeRestriction'] = "1=1";
            }
        }
    },
    /**
     * Customize fields for asset registry panel
     */
    customizeAssetRegistryFields: function (table, row, column, cellElement) {
        var fieldValue = row.row.getFieldValue(column.id);
        if (table + '.asset_type' === column.id) {
            // doesn't have control type link
            cellElement.innerHTML = getMessage('asset_type_' + fieldValue);
        } else if (table + '.status' === column.id) {
            var filterEl = Ext.get(row.grid.getFilterInputId(column.id));
            var removeOptions = function (el, values) {
                for ( var x in values ) {
                    for ( var i = el.dom.length - 1; i >= 0; i-- ) {
                        if (x === el.dom[i].value) {
                            el.dom.remove(i);
                            break;
                        }
                    }
                }
            };
            removeOptions(filterEl, {'Archived': 'Archived', 'Removed': 'Removed'});
        }
    },
    afterSelectCustodianName: function (sourceTable, sourceRecord) {
        this.sourceTable = sourceTable;
        this.sourceRecord = new Ab.data.Record(sourceRecord.values);
        this.setTeamMember(this.visibleForm, sourceTable, sourceRecord);
    },
    // set team fields
    setTeamMember: function (objForm, sourceTable, record) {
        function getValueIfExistsNotEmpty(value, defaultValue) {
            return valueExistsNotEmpty(value) ? value : defaultValue;
        }
        objForm.clearImage('doc_graphic_image');
        objForm.setFieldValue('team.source_table', sourceTable);
        objForm.setFieldValue('team.status', 'Active');
        if ('em' === sourceTable) {
            objForm.setFieldValue('custodian_name', getValueIfExistsNotEmpty(record.getValue("em.name"), record.getValue("em.em_id")));
            objForm.setFieldValue('team.em_id', record.getValue("em.em_id"));
            objForm.setFieldValue('em.em_id', record.getValue("em.em_id"));
            objForm.setFieldValue('team.contact_id', '');
            objForm.setFieldValue('team.vn_id', '');
            objForm.setFieldValue('team.email_archive', record.getValue("em.email"));
            objForm.setFieldValue('team.cell_num_archive', record.getValue("em.cellular_number"));
            objForm.setFieldValue('team.phone_archive', record.getValue("em.phone"));
            objForm.setFieldValue('team.name_archive', record.getValue("em.name_last") + ', ' + record.getValue("em.name_first"));
            objForm.setFieldValue('em.em_photo', record.getValue("em.em_photo"));
            objForm.setFieldValue('team.source', getMessage('msgSourceName_em'));
        }
        if ('contact' === sourceTable) {
            objForm.setFieldValue('custodian_name', getValueIfExistsNotEmpty(record.getValue("contact.name"), record.getValue("contact.contact_id")));
            objForm.setFieldValue('team.contact_id', record.getValue("contact.contact_id"));
            objForm.setFieldValue('contact.contact_id', record.getValue("contact.contact_id"));
            objForm.setFieldValue('team.em_id', '');
            objForm.setFieldValue('team.vn_id', '');
            objForm.setFieldValue('team.email_archive', record.getValue("contact.email"));
            objForm.setFieldValue('team.cell_num_archive', record.getValue("contact.cellular_number"));
            objForm.setFieldValue('team.phone_archive', record.getValue("contact.phone"));
            objForm.setFieldValue('team.name_archive', record.getValue("contact.name_last") + ', ' + record.getValue("contact.name_first"));
            objForm.setFieldValue('contact.contact_photo', record.getValue("contact.contact_photo"));
            objForm.setFieldValue('team.source', getMessage('msgSourceName_contact'));
        }
        if ('vn' === sourceTable) {
            objForm.setFieldValue('custodian_name', getValueIfExistsNotEmpty(record.getValue("vn.company"), record.getValue("vn.vn_id")));
            objForm.setFieldValue('team.vn_id', record.getValue("vn.vn_id"));
            objForm.setFieldValue('vn.vn_id', record.getValue("vn.vn_id"));
            objForm.setFieldValue('team.em_id', '');
            objForm.setFieldValue('team.contact_id', '');
            objForm.setFieldValue('team.email_archive', record.getValue("vn.email"));
            objForm.setFieldValue('team.cell_num_archive', record.getValue("vn.alt_phone"));
            objForm.setFieldValue('team.phone_archive', record.getValue("vn.phone"));
            objForm.setFieldValue('team.name_archive', record.getValue("vn.contact"));
            objForm.setFieldValue('vn.vendor_photo', record.getValue("vn.vendor_photo"));
            objForm.setFieldValue('team.source', getMessage('msgSourceName_vn'));
        }
        objForm.setFieldValue('source', getMessage('msgSourceName_' + sourceTable));
        if (valueExistsNotEmpty(objForm.getFieldValue('team.contact_id'))) {
            if (valueExistsNotEmpty(objForm.getFieldValue('contact.contact_photo'))) {
                objForm.showImageDoc('doc_graphic_image', 'contact.contact_id', 'contact.contact_photo');
            }
        } else if (valueExistsNotEmpty(objForm.getFieldValue('team.em_id'))) {
            if (valueExistsNotEmpty(objForm.getFieldValue('em.em_photo'))) {
                objForm.showImageDoc('doc_graphic_image', 'em.em_id', 'em.em_photo');
            }
        } else if (valueExistsNotEmpty(objForm.getFieldValue('team.vn_id'))) {
            if (valueExistsNotEmpty(objForm.getFieldValue('vn.vendor_photo'))) {
                objForm.showImageDoc('doc_graphic_image', 'vn.vn_id', 'vn.vendor_photo');
            }
        }
    },
    /**
     * Overwrite form updateDocument. Show only checkIn, view and delete actions.
     */
    updateDocumentButtons: function () {
        var panelId = this.id;
        this.fields.each(function (field) {
            var fieldDef = field.fieldDef;
            if (fieldDef.isDocument) {
                var panel = field.panel;
                var fullName = fieldDef.fullName;
                var documentInput = panel.getFieldElement(fullName);
                documentInput.disabled = panel.newRecord;
                var documentValue = panel.getFieldValue(fullName);
                var documentExists = (documentValue !== '');
                var canShow = documentExists;
                var canCheckIn = (!documentExists && !fieldDef.readOnly && !panel.newRecord);
                var canCheckOut = (documentExists && !fieldDef.readOnly && !panel.newRecord);
                // checkin via file browser if showCheckInDialog attribute is false
                var doBrowseForCheckIn = canCheckIn && !fieldDef.showCheckInDialog;
                var prefix = panelId + '_' + fullName;
                panel.showElement(prefix + '_showDocument', canShow);
                panel.showElement(prefix + '_checkInNewDocument', canCheckIn && fieldDef.showCheckInDialog);
                panel.showElement(prefix + '_checkInNewDocumentVersion', false);
                panel.showElement(prefix + '_checkOutDocument', false);
                panel.showElement(prefix + '_lockDocument', false);
                panel.showElement(prefix + '_deleteDocument', canCheckOut);
                // turn on only one of text input with buttons or file input
                panel.showElement(prefix, !doBrowseForCheckIn);
                panel.showElement(prefix + '_browseDocument_div', doBrowseForCheckIn);
            }
        });
    }
});

/**
 * Select value for custodian name.
 */
function onSelectCustodianName() {
    View.openDialog('ab-team-member-select.axvw', null, false, {
        width: 1024,
        height: 600,
        callback: function (sourceTable, record) {
            var controller = View.controllers.get('abEamAssignCustodianController');
            controller.afterSelectCustodianName(sourceTable, record);
            View.closeDialog();
        }
    })
}

/**
 * Show custodians for selected asset
 *
 */
function showAssetCustodian(context) {
    var parentPanel = View.panels.get(context.command.parentPanelId);
    var controller = View.controllers.get('abEamAssignCustodianController');
    if (parentPanel) {
        var selectedRow = parentPanel.gridRows.get(parentPanel.selectedRowIndex);
        controller.onClickAsset(selectedRow);
    }
}

