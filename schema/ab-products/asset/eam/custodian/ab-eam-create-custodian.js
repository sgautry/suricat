/**
 * Assign custodian controller: works for Change owner and Assign Custodian
 */
var abEamAssignCustodianController = View.createController('abEamAssignCustodianController', {
    // callback function
    callbackFunction: null,
    sourceTable: null,
    // source record after selected custodian
    sourceRecord: null,
    afterViewLoad: function () {
        if (valueExists(this.view.parameters)) {
            if (valueExists(this.view.parameters.callback)) {
                this.callbackFunction = this.view.parameters.callback;
            }
        }
    },
    abCreateCustodian_form_onSave: function () {
        this.onSaveCommon(this.abCreateCustodian_form, true);
    },
    abCreateCustodian_form_onSaveAndClose: function () {
        this.onSaveCommon(this.abCreateCustodian_form, false);
    },
    onSaveCommon: function (panelForm, addMultipleCustodians) {
        if (panelForm.canSave() && this.isValidCustodian(panelForm)) {
            if ('OWNER' === panelForm.getFieldValue('team.custodian_type')) {
                panelForm.setFieldValue('team.is_owner', 1);
            }
            panelForm.save();
            this.updateCustodianData(panelForm);
            if (addMultipleCustodians) {
                panelForm.refresh({}, true);
                this.setTeamMember(this.abCreateCustodian_form, this.sourceTable, this.sourceRecord);
            }
            if (valueExistsNotEmpty(this.callbackFunction)) {
                this.callbackFunction(addMultipleCustodians);
            }
        }
    },
    isValidCustodian: function (panelForm) {
        var custodianName = panelForm.getFieldValue('custodian_name'),
            custodianType = panelForm.getFieldValue('team.custodian_type'),
            sourceTable = panelForm.getFieldValue('team.source_table');
        var custodianFieldBySource = {
            'vn': 'vn_id',
            'contact': 'contact_id',
            'em': 'em_id'
        };
        var fieldName = custodianFieldBySource[sourceTable];
        var restriction = "team.team_type = 'Equipment' AND team.custodian_type='" + custodianType + "'AND team." + fieldName + " = '" + custodianName + "'";
        var records = View.dataSources.get('abCreateCustodian_ds').getRecords(restriction);
        if (records.length > 0) {
            View.showMessage(String.format(getMessage('invalidCustodian'), custodianName, custodianType));
            return false;
        }
        return true;
    },
    /**
     * Update cell num and phone num for all team custodian entries.
     * @param {Object} panelForm Custodian form.
     */
    updateCustodianData: function (panelForm) {
        var custodianDataSource = View.dataSources.get('abCreateCustodian_ds'),
            custodianRecord = panelForm.getRecord();
        var sourceTable = custodianRecord.getValue('team.source_table'),
            cellNumArchive = custodianRecord.getValue('team.cell_num_archive'),
            phoneArchive = custodianRecord.getValue('team.phone_archive');
        var custodianFieldBySource = {
            'vn': 'vn_id',
            'contact': 'contact_id',
            'em': 'em_id'
        };
        var fieldName = custodianFieldBySource[sourceTable];
        var custodianName = custodianRecord.getValue('team.' + fieldName);
        var restriction = "team.team_type = 'Equipment' AND  team.source_table='" + sourceTable + "' AND team." + fieldName + " = '" + custodianName + "'";
        var records = custodianDataSource.getRecords(restriction);
        for ( var i = 0; i < records.length; i++ ) {
            var record = records[i];
            record.isNew = false;
            record.setValue('team.cell_num_archive', cellNumArchive);
            record.setValue('team.phone_archive', phoneArchive);
            custodianDataSource.saveRecord(record);
        }
    },
    afterSelectCustodianName: function (sourceTable, sourceRecord) {
        this.sourceTable = sourceTable;
        this.sourceRecord = new Ab.data.Record(sourceRecord.values);
        this.setTeamMember(this.abCreateCustodian_form, sourceTable, sourceRecord);
    },
    // set team fields
    setTeamMember: function (objForm, sourceTable, record) {
        function getValueIfExistsNotEmpty(value, defaultValue) {
            return valueExistsNotEmpty(value) ? value : defaultValue;
        }
        objForm.clearImage('doc_graphic_image');
        objForm.setFieldValue('team.source_table', sourceTable);
        objForm.setFieldValue('team.status', 'Active');
        if ('em' === sourceTable) {
            objForm.setFieldValue('custodian_name', getValueIfExistsNotEmpty(record.getValue("em.name"), record.getValue("em.em_id")));
            objForm.setFieldValue('team.em_id', record.getValue("em.em_id"));
            objForm.setFieldValue('em.em_id', record.getValue("em.em_id"));
            objForm.setFieldValue('team.contact_id', '');
            objForm.setFieldValue('team.vn_id', '');
            objForm.setFieldValue('team.email_archive', record.getValue("em.email"));
            objForm.setFieldValue('team.cell_num_archive', record.getValue("em.cellular_number"));
            objForm.setFieldValue('team.phone_archive', record.getValue("em.phone"));
            objForm.setFieldValue('team.name_archive', record.getValue("em.name_last") + ', ' + record.getValue("em.name_first"));
            objForm.setFieldValue('em.em_photo', record.getValue("em.em_photo"));
            objForm.setFieldValue('team.source', getMessage('msgSourceName_em'));
        }
        if ('contact' === sourceTable) {
            objForm.setFieldValue('custodian_name', getValueIfExistsNotEmpty(record.getValue("contact.name"), record.getValue("contact.contact_id")));
            objForm.setFieldValue('team.contact_id', record.getValue("contact.contact_id"));
            objForm.setFieldValue('contact.contact_id', record.getValue("contact.contact_id"));
            objForm.setFieldValue('team.em_id', '');
            objForm.setFieldValue('team.vn_id', '');
            objForm.setFieldValue('team.email_archive', record.getValue("contact.email"));
            objForm.setFieldValue('team.cell_num_archive', record.getValue("contact.cellular_number"));
            objForm.setFieldValue('team.phone_archive', record.getValue("contact.phone"));
            objForm.setFieldValue('team.name_archive', record.getValue("contact.name_last") + ', ' + record.getValue("contact.name_first"));
            objForm.setFieldValue('contact.contact_photo', record.getValue("contact.contact_photo"));
            objForm.setFieldValue('team.source', getMessage('msgSourceName_contact'));
        }
        if ('vn' === sourceTable) {
            objForm.setFieldValue('custodian_name', getValueIfExistsNotEmpty(record.getValue("vn.company"), record.getValue("vn.vn_id")));
            objForm.setFieldValue('team.vn_id', record.getValue("vn.vn_id"));
            objForm.setFieldValue('vn.vn_id', record.getValue("vn.vn_id"));
            objForm.setFieldValue('team.em_id', '');
            objForm.setFieldValue('team.contact_id', '');
            objForm.setFieldValue('team.email_archive', record.getValue("vn.email"));
            objForm.setFieldValue('team.cell_num_archive', record.getValue("vn.alt_phone"));
            objForm.setFieldValue('team.phone_archive', record.getValue("vn.phone"));
            objForm.setFieldValue('team.name_archive', record.getValue("vn.contact"));
            objForm.setFieldValue('vn.vendor_photo', record.getValue("vn.vendor_photo"));
            objForm.setFieldValue('team.source', getMessage('msgSourceName_vn'));
        }
        objForm.setFieldValue('source', getMessage('msgSourceName_' + sourceTable));
        if (valueExistsNotEmpty(objForm.getFieldValue('team.contact_id'))) {
            if (valueExistsNotEmpty(objForm.getFieldValue('contact.contact_photo'))) {
                objForm.showImageDoc('doc_graphic_image', 'contact.contact_id', 'contact.contact_photo');
            }
        } else if (valueExistsNotEmpty(objForm.getFieldValue('team.em_id'))) {
            if (valueExistsNotEmpty(objForm.getFieldValue('em.em_photo'))) {
                objForm.showImageDoc('doc_graphic_image', 'em.em_id', 'em.em_photo');
            }
        } else if (valueExistsNotEmpty(objForm.getFieldValue('team.vn_id'))) {
            if (valueExistsNotEmpty(objForm.getFieldValue('vn.vendor_photo'))) {
                objForm.showImageDoc('doc_graphic_image', 'vn.vn_id', 'vn.vendor_photo');
            }
        }
    }
});
/**
 * Select value for custodian name.
 */
function onSelectCustodianName() {
    View.openDialog('ab-team-member-select.axvw', null, false, {
        width: 1024,
        height: 600,
        callback: function (sourceTable, record) {
            var controller = View.controllers.get('abEamAssignCustodianController');
            controller.afterSelectCustodianName(sourceTable, record);
            View.closeDialog();
        }
    })
}