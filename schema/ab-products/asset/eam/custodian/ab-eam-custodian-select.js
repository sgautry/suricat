function afterSelectCustodian(context) {
    if (View.parameters.callback) {
        var parentPanel = View.panels.get(context.command.parentPanelId);
        if (parentPanel) {
            var selectedRow = parentPanel.gridRows.get(parentPanel.selectedRowIndex),
                record = selectedRow.getRecord();
            var custodianCode = record.getValue('team.custodian_code'),
                custodianName = record.getValue('team.custodian_name'),
                custodianType = record.getValue('team.custodian_type'),
                sourceTable = record.getValue('team.source_table');
            var custodianFieldBySource = {
                'vn': 'vn_id',
                'contact': 'contact_id',
                'em': 'em_id'
            };
            var fieldName = custodianFieldBySource[sourceTable];
            var custodian = {
                code: custodianCode,
                name: custodianName,
                type: custodianType,
                fieldName: fieldName
            };
            View.parameters.callback(custodian);
        }
    }
}