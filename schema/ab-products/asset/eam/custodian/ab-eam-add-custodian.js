var abAddCustodianController = View.createController('abAddCustodianController', {
    // if is called to add owner
    isOwner: false,
    // selected custodian name
    custodianName: null,
    // selected custodian type passed from asset custodians panel
    custodianType: null,
    //source table for selected custodian
    sourceTable: null,
    // selected custodian record (record come from custodian source table)
    custodianRecord: null,
    // callback function to be executed after save
    callbackFunction: null,
    // is all assets are selected
    allPagesSelected: false,
    /* object  with asset list restriction, including  index and smart search
     * Is passed to pop-up window Change Owner or Assign Custodian
     */
    assetRestriction: null,
    // array  with objects for selected assets {assetId, assetType}
    selectedAssets: null,
    // filter restriction
    filterSelection: {
        "asset_assigned": "",
        "asset_type": "",
        "custodian_status": ""
    },
    // source record after selected custodian
    sourceRecord: null,
    // list of team.autonumbered_id after owner records are created
    teamPkeyIds: null,
    afterViewLoad: function () {
        if (valueExists(this.view.parameters)) {
            if (valueExists(this.view.parameters.callback)) {
                this.callbackFunction = this.view.parameters.callback;
            }
            if (valueExists(this.view.parameters.isOwner)) {
                this.isOwner = this.view.parameters.isOwner;
            }
            if (valueExists(this.view.parameters.custodianName)) {
                this.custodianName = this.view.parameters.custodianName;
            }
            if (valueExists(this.view.parameters.custodianType)) {
                this.custodianType = this.view.parameters.custodianType;
            }
            if (valueExists(this.view.parameters.sourceTable)) {
                this.sourceTable = this.view.parameters.sourceTable;
            }
            if (valueExists(this.view.parameters.custodianRecord)) {
                this.custodianRecord = this.view.parameters.custodianRecord;
            }
        }
        // customize mini console
        this.abAddCustodianAssets_list.addEventListener('afterBuildHeader', this.abAddCustodianAssets_list_customizeHeader, this);
        this.abAddCustodianAssets_list.addEventListener('onMultipleSelectionChange', this.abAddCustodianAssets_list_onMultipleSelectionChange, this);
        var controller = this;
        // customize grid columns
        this.abAddCustodianAssets_list.afterCreateCellContent = function (row, column, cellElement) {
            controller.customizeAssetRegistryFields(row, column, cellElement);
        };
        this.abAddCustodianOwnerAssets_list.afterCreateCellContent = function (row, column, cellElement) {
            controller.customizeAssetRegistryFields(row, column, cellElement);
        };
        this.abAddCustodianOtherAssets_list.afterCreateCellContent = function (row, column, cellElement) {
            controller.customizeAssetRegistryFields(row, column, cellElement);
        };
        this.abAddCustodianAssets_list.addParameter('useCustodian', true);
        this.abAddCustodianOwner_form.addFieldEventListener('team.digital_signature', Ab.form.Form.DOC_EVENT_CHECKIN, this.copyDocument, this);
        this.abAddCustodianOwner_form.addFieldEventListener('team.digital_signature', Ab.form.Form.DOC_EVENT_DELETE, this.deleteDocument, this);
        this.abAddCustodianOwner_form.updateDocumentButtons = this.updateDocumentButtons.createDelegate(this.abAddCustodianOwner_form);
    },
    afterInitialDataFetch: function () {
        // show second tab
        this.abAddCustodianTabs.showTab('abAddCustodianTabs_owner', this.isOwner);
        this.abAddCustodianTabs.showTab('abAddCustodianTabs_custodian', !this.isOwner);
    },
    /**
     * On apply filter
     * called from onChange event from all console fields
     */
    onChangeFilter: function (field) {
        var fieldName = field.id.replace('abAddCustodian_filter_', '');
        var fieldValue = field.value;
        this.filterSelection[fieldName] = fieldValue;
        if ('asset_assigned' === fieldName && 'unassigned' === fieldValue) {
            this.abAddCustodian_filter.setFieldValue('custodian_status', '');
            this.filterSelection['custodian_status'] = '';
        }
        this.applyFilter();
    },
    applyFilter: function () {
        var blRestriction = " 1 = 1 ";
        var propertyRestriction = " 1 = 1 ";
        var eqRestriction = " 1 = 1 ";
        var taRestriction = " 1 = 1 ";
        var sqlRestriction = " 1 = 1 ";
        if (valueExistsNotEmpty(this.filterSelection['asset_type'])) {
            sqlRestriction = "bl.asset_type = '{0}'".replace('{0}', makeSafeSqlValue(this.filterSelection['asset_type']));
        }
        var restriction = "";
        if ('assigned' === this.filterSelection['asset_assigned']) {
            restriction = "EXISTS(SELECT team.autonumbered_id FROM team WHERE team.team_type = 'Equipment' AND team.custodian_type IS NOT NULL {0} {1})";
        } else if ('unassigned' === this.filterSelection['asset_assigned']) {
            restriction = "NOT EXISTS(SELECT team.autonumbered_id FROM team WHERE team.team_type = 'Equipment' AND team.custodian_type IS NOT NULL {1})";
        }
        var status = "";
        if ('active' === this.filterSelection['custodian_status']) {
            status = " AND team.status = 'Active' ";
        } else if ('inactive' === this.filterSelection['custodian_status']) {
            status = " AND team.status <> 'Active'"
        }
        if (restriction.length === 0 && status.length > 0) {
            restriction = "(EXISTS(SELECT team.autonumbered_id FROM team WHERE team.team_type = 'Equipment' AND team.custodian_type IS NOT NULL {0} {1}) " +
                "OR NOT EXISTS(SELECT team.autonumbered_id FROM team WHERE team.team_type = 'Equipment' AND team.custodian_type IS NOT NULL {2}))";
        }
        restriction = restriction.replace('{0}', status);
        if (restriction.length > 0) {
            blRestriction = restriction.replace('{1}', "AND team.bl_id = bl.bl_id").replace('{2}', "AND team.bl_id = bl.bl_id");
            propertyRestriction = restriction.replace('{1}', "AND team.pr_id = property.pr_id").replace('{2}', "AND team.pr_id = property.pr_id");
            eqRestriction = restriction.replace('{1}', "AND team.eq_id = eq.eq_id").replace('{2}', "AND team.eq_id = eq.eq_id");
            taRestriction = restriction.replace('{1}', "AND team.ta_id = ta.ta_id").replace('{2}', "AND team.ta_id = ta.ta_id");
        }
        this.abAddCustodianAssets_list.addParameter('blTypeRestriction', blRestriction);
        this.abAddCustodianAssets_list.addParameter('propertyTypeRestriction', propertyRestriction);
        this.abAddCustodianAssets_list.addParameter('eqTypeRestriction', eqRestriction);
        this.abAddCustodianAssets_list.addParameter('taTypeRestriction', taRestriction);
        this.abAddCustodianAssets_list.addParameter('sqlTypeRestriction', sqlRestriction);
        this.abAddCustodianAssets_list.refresh();
    },
    abAddCustodianAssets_list_afterRefresh: function () {
        // overwrite select all event
        var checkAllEl = Ext.get(this.abAddCustodianAssets_list.id + '_checkAll');
        if (valueExists(checkAllEl)) {
            controller = this;
            checkAllEl.on('click', function (event, el) {
                controller.onClickSelectAll(el.checked);
            });
        }
    },
    /**
     * On click select all checkbox.
     */
    onClickSelectAll: function (selected) {
        var controller = View.controllers.get('abAddCustodianController');
        if (selected) {
            View.confirm(getMessage('confirmSelectAllPages'), function (button) {
                if ('yes' === button) {
                    controller.allPagesSelected = true;
                    controller.getAssetListRestriction();
                } else {
                    controller.allPagesSelected = false;
                }
                controller.abAddCustodianAssets_list.selectAll(selected);
            });
        } else {
            controller.allPagesSelected = false;
            controller.abAddCustodianAssets_list.selectAll(selected);
        }
    },
    abAddCustodianAssets_list_onMultipleSelectionChange: function (row) {
        var controller = View.controllers.get('abAddCustodianController');
        if (!row.row.isSelected()
            && controller.allPagesSelected) {
            controller.allPagesSelected = false;
        }
    },
    abAddCustodianAssets_list_customizeHeader: function (panel, parentElement) {
        var columnIndex = 0;
        for ( var i = 0, col; col = panel.columns[i]; i++ ) {
            if ('bl.asset_type' === col.id && !col.hidden) {
                var headerRows = parentElement.getElementsByTagName("tr");
                // customize filter row if this exists
                var rowElement = document.getElementById(panel.id + '_filterRow');
                if (headerRows.length > 1 && valueExists(rowElement)) {
                    var filterInputId = panel.getFilterInputId(col.id);
                    this.updateMiniConsole(panel, rowElement, columnIndex, filterInputId);
                }
            }
            if (!col.hidden) {
                columnIndex++;
            }
        }
    },
    /**
     * Customize filter row asset type.
     *
     * @param panel grid panel
     * @param rowElement filter row element
     * @param columnIndex column index
     * @param filterInputId filter input id
     */
    updateMiniConsole: function (panel, rowElement, columnIndex, filterInputId) {
        var assetTypes = {
            'property': getMessage('asset_type_property'),
            'bl': getMessage('asset_type_bl'),
            'eq': getMessage('asset_type_eq'),
            'ta': getMessage('asset_type_ta')
        };
        var headerCells = rowElement.getElementsByTagName("th");
        var headerCell = headerCells[columnIndex];
        // remove input element if exists
        var inputElements = headerCell.getElementsByTagName("input");
        if (inputElements[filterInputId]) {
            headerCell.removeChild(inputElements[filterInputId]);
        }
        // add select element
        var i = 0;
        var input = document.createElement("select");
        input.options[i++] = new Option("", "", true);
        for ( var storedValue in assetTypes ) {
            input.options[i++] = new Option(assetTypes[storedValue], storedValue);
        }
        input.className = "inputField_box";
        // run filter when user click on one enum value
        Ext.EventManager.addListener(input, "change", panel.onFilter.createDelegate(panel));
        input.id = filterInputId;
        if (headerCell.childNodes.length > 0) {
            headerCell.insertBefore(input, headerCell.childNodes[0]);
        } else {
            headerCell.appendChild(input);
        }
    },
    /**
     * Customize fields for asset registry panel
     */
    customizeAssetRegistryFields: function (row, column, cellElement) {
        var fieldValue = row.row.getFieldValue(column.id);
        if ('bl.asset_type' === column.id) {
            // doesn't have control type link
            cellElement.innerHTML = getMessage('asset_type_' + fieldValue);
        }
        if ('bl.asset_id' === column.id) {
            // doesn't have control type link
            cellElement.style.fontWeight = 'bold';
        }
    },
    /**
     * Get selected assets. Returns an array of objects {assetType, assetId}
     * or an object with applied restriction when user selected all pages
     */
    getSelectedAssets: function () {
        var selectedRecords = this.abAddCustodianAssets_list.getSelectedRecords();
        if (selectedRecords.length > 0) {
            this.selectedAssets = [];
            for ( var i = 0; i < selectedRecords.length; i++ ) {
                var record = selectedRecords[i];
                this.selectedAssets.push({
                    "asset_type": record.getValue('bl.asset_type'),
                    "asset_id": record.getValue('bl.asset_id')
                });
            }
        }
    },
    getAssetListRestriction: function () {
        var refreshParameters = this.abAddCustodianAssets_list.getParametersForRefresh();
        this.assetRestriction = {};
        // read datasource parameters
        if (valueExistsNotEmpty(refreshParameters["blTypeRestriction"])) {
            this.assetRestriction["blTypeRestriction"] = refreshParameters["blTypeRestriction"];
        }
        if (valueExistsNotEmpty(refreshParameters["propertyTypeRestriction"])) {
            this.assetRestriction["propertyTypeRestriction"] = refreshParameters["propertyTypeRestriction"];
        }
        if (valueExistsNotEmpty(refreshParameters["eqTypeRestriction"])) {
            this.assetRestriction["eqTypeRestriction"] = refreshParameters["eqTypeRestriction"];
        }
        if (valueExistsNotEmpty(refreshParameters["taTypeRestriction"])) {
            this.assetRestriction["taTypeRestriction"] = refreshParameters["taTypeRestriction"];
        }
        var indexAndSmartSearch = [];
        if (valueExistsNotEmpty(refreshParameters["filterValues"])) {
            var filterValues = JSON.parse(refreshParameters["filterValues"]);
            for ( var i = 0; i < filterValues.length; i++ ) {
                var objFilterValue = filterValues[i];
                var sqlString = "(UPPER({0}) LIKE '%'||UPPER('{1}')||'%')".replace('{0}', objFilterValue['fieldName']).replace('{1}', objFilterValue['filterValue']);
                indexAndSmartSearch.push(sqlString);
            }
        }
        if (valueExistsNotEmpty(refreshParameters["indexField"])
            && valueExistsNotEmpty(refreshParameters["indexValue"])) {
            var sqlString = "(UPPER({0}) LIKE UPPER('{1}'))".replace('{0}', refreshParameters['indexField']).replace('{1}', refreshParameters['indexValue']);
            indexAndSmartSearch.push(sqlString);
        }
        var sqlTypeRestriction = "";
        if (valueExistsNotEmpty(refreshParameters["sqlTypeRestriction"])) {
            sqlTypeRestriction = "(" + refreshParameters["sqlTypeRestriction"] + ")";
        }
        if (indexAndSmartSearch.length > 0) {
            sqlTypeRestriction += (sqlTypeRestriction.length > 0 ? " AND " : "") + indexAndSmartSearch.join(' AND ');
        }
        if (valueExistsNotEmpty(sqlTypeRestriction)) {
            this.assetRestriction["sqlTypeRestriction"] = sqlTypeRestriction;
        }
    },
    abAddCustodian_filter_onNext: function () {
        var selectedRecords = this.abAddCustodianAssets_list.getSelectedRecords();
        if (selectedRecords.length === 0) {
            View.showMessage(getMessage('errNoAssetsSelected'));
            return false;
        }
        this.getSelectedAssets();
        this.getAssetListRestriction();
        var nextTabName = this.isOwner ? 'abAddCustodianTabs_owner' : 'abAddCustodianTabs_custodian';
        this.refreshTabPage(nextTabName);
        this.abAddCustodianTabs.selectTab(nextTabName, null, false, false, true);
    },
    refreshTabPage: function (tabName) {
        var objForm = null;
        var objGrid = null;
        if ('abAddCustodianTabs_owner' === tabName) {
            objForm = this.abAddCustodianOwner_form;
            objGrid = this.abAddCustodianOwnerAssets_list;
        } else {
            objForm = this.abAddCustodianOther_form;
            objGrid = this.abAddCustodianOtherAssets_list;
        }
        objGrid.addParameter('useCustodian', true);
        // refresh form
        objForm.refresh(null, true);
        if (valueExistsNotEmpty(this.custodianName)) {
            this.setTeamMember(objForm, this.sourceTable, this.custodianRecord);
        }
        // refresh grid
        this.prepareAssetRegistryRestriction();
        objGrid.addParameter('blTypeRestriction', this.assetRestriction['blTypeRestriction']);
        objGrid.addParameter('propertyTypeRestriction', this.assetRestriction['propertyTypeRestriction']);
        objGrid.addParameter('eqTypeRestriction', this.assetRestriction['eqTypeRestriction']);
        objGrid.addParameter('taTypeRestriction', this.assetRestriction['taTypeRestriction']);
        objGrid.addParameter('sqlTypeRestriction', this.assetRestriction['sqlTypeRestriction']);
        objGrid.refresh();
    },
    abAddCustodianOwner_form_afterRefresh: function (formPanel) {
        if (formPanel.newRecord) {
            // make custodian name no editable
            formPanel.getFieldElement('custodian_name').readOnly = true;
        } else {
            this.setTeamMember(formPanel, this.sourceTable, this.sourceRecord || this.custodianRecord);
            formPanel.enableField('custodian_name', false);
            formPanel.enableField('team.date_start', false);
            formPanel.enableField('team.notes', false);
            if (this.selectedAssets.length > 1) {
                var instructionsEl = formPanel.fields.get('team.digital_signature').getInstructionsEl();
                if (instructionsEl) {
                    instructionsEl.dom.innerHTML = getMessage('digitalSignatureInstructions');
                }
            }
            formPanel.actions.get('previous').show(false);
            formPanel.actions.get('save').show(false);
            formPanel.actions.get('cancel').show(false);
        }
    },
    /**
     * on click asset profile
     */
    onProfileRow: function (row) {
        var record = row.row.getRecord();
        var assetId = record.getValue('bl.asset_id');
        var assetType = record.getValue('bl.asset_type');
        var viewName = null;
        var restriction = new Ab.view.Restriction();
        if ('bl' === assetType) {
            viewName = 'ab-profile-building.axvw';
            restriction.addClause('bl.bl_id', assetId, '=');
        } else if ('property' === assetType) {
            viewName = 'ab-profile-property.axvw';
            restriction.addClause('property.pr_id', assetId, '=');
        } else if ('eq' === assetType) {
            viewName = 'ab-profile-equipment.axvw';
            restriction.addClause('eq.eq_id', assetId, '=');
        } else if ('ta' === assetType) {
            viewName = 'ab-profile-ta.axvw';
            restriction.addClause('ta.ta_id', assetId, '=');
        }
        View.openDialog(viewName, restriction, false, {
            width: 1050,
            height: 650,
            closeButton: true
        })
    },
    abAddCustodianOther_form_afterRefresh: function () {
        // make custodian name no editable
        this.abAddCustodianOther_form.getFieldElement('custodian_name').readOnly = true;
        if (valueExistsNotEmpty(this.custodianType)) {
            this.abAddCustodianOther_form.setFieldValue('team.custodian_type', this.custodianType);
        }
    },
    abAddCustodianOwner_form_onPrevious: function () {
        this.abAddCustodianTabs.selectTab('abAddCustodianTabs_assets', null, false, false, true);
    },
    abAddCustodianOther_form_onPrevious: function () {
        this.abAddCustodianTabs.selectTab('abAddCustodianTabs_assets', null, false, false, true);
    },
    abAddCustodianOwner_form_onSave: function () {
        this.onSaveCommon(this.abAddCustodianOwner_form);
    },
    abAddCustodianOther_form_onSave: function () {
        this.onSaveCommon(this.abAddCustodianOther_form);
    },
    onSaveCommon: function (panelForm) {
        if (this.isOwner) {
            if (panelForm.newRecord && panelForm.canSave()
                && this.isValidForm(panelForm, this.isOwner)) {
                var confirmMessage = this.prepareConfirmMessageNewOwner(panelForm);
                View.confirm(confirmMessage, function (button) {
                    if ('yes' === button) {
                        var dataRecord = panelForm.getOutboundRecord();
                        var controller = View.controllers.get('abAddCustodianController');
                        controller.changeOwner(dataRecord);
                    }
                });
            }
        } else {
            if (panelForm.canSave() && this.isValidForm(panelForm, this.isOwner)) {
                var dataRecord = panelForm.getOutboundRecord();
                this.addCustodian(dataRecord);
            }
        }
    },
    prepareConfirmMessageNewOwner: function (panelForm) {
        var startDate = panelForm.getFieldValue('team.date_start');
        var objStartDate = panelForm.getDataSource().parseValue('team.date_start', startDate, false);
        var objTerminateDate = DateMath.add(objStartDate, DateMath.DAY, -1);
        var localizedStartDate = panelForm.getDataSource().formatValue('team.date_start', objStartDate, true);
        var localizedTerminateDate = panelForm.getDataSource().formatValue('team.date_start', objTerminateDate, true);
        return getMessage('confirmSaveNewOwner').replace('{0}', localizedStartDate).replace('{1}', localizedTerminateDate);
    },
    changeOwner: function (dataRecord) {
        try {
            // save using long running job
            var jobId = Workflow.startJob('AbAssetManagement-AssetManagementService-changeOwner', dataRecord, this.selectedAssets, this.allPagesSelected, this.assetRestriction);
            View.openJobProgressBar(getMessage('messageChangeOwnerJob'), jobId, '', function (status) {
                if (status.jobStatusCode = 3) {
                    var teamPkeyIds = eval(status.jobProperties.teamPkeyIds);
                    var controller = View.controllers.get('abAddCustodianController');
                    controller.refreshCustodianOwnerPanelAfterSave(teamPkeyIds);
                    if (valueExistsNotEmpty(controller.callbackFunction)) {
                        controller.callbackFunction();
                    }
                    View.closeProgressBar();
                }
            });
        } catch (e) {
            Workflow.handleError(e);
            return false;
        }
    },
    addCustodian: function (dataRecord) {
        try {
            // save using long running job
            var jobId = Workflow.startJob('AbAssetManagement-AssetManagementService-addCustodian', dataRecord, this.selectedAssets, this.allPagesSelected, this.assetRestriction);
            View.openJobProgressBar(getMessage('messageAssignOtherJob'), jobId, '', function (status) {
                if (status.jobStatusCode = 3) {
                    var controller = View.controllers.get('abAddCustodianController');
                    if (valueExistsNotEmpty(controller.callbackFunction)) {
                        controller.callbackFunction();
                    }
                    View.closeProgressBar();
                    View.closeThisDialog();
                }
            });
        } catch (e) {
            Workflow.handleError(e);
            return false;
        }
    },
    refreshCustodianOwnerPanelAfterSave: function (teamPkeyIds) {
        if (valueExists(teamPkeyIds) && !_.isEmpty(teamPkeyIds)) {
            this.teamPkeyIds = teamPkeyIds;
            // get first id
            var teamId = teamPkeyIds[0];
            var restriction = new Ab.view.Restriction({'team.autonumbered_id': teamId});
            this.abAddCustodianOwner_form.refresh(restriction, false);
            this.abAddCustodianOwnerAssets_list.refresh();
        }
    },
    copyDocument: function (panel, fieldName, parameters) {
        var autonumberId = panel.getRecord().getValue('team.autonumbered_id');
        var teamPkeyIds = _.rest(this.teamPkeyIds); // remove first teamId
        if (teamPkeyIds.length > 0) {
            try {
                Workflow.callMethod('AbAssetManagement-AssetManagementService-copyDigitalSignatureDocument', autonumberId, teamPkeyIds, fieldName, parameters);
            } catch (e) {
                Workflow.handleError(e);
            }
        }
    },
    deleteDocument: function (panel, fieldName, parameters) {
        var autonumberId = panel.getRecord().getValue('team.autonumbered_id');
        var teamPkeyIds = _.rest(this.teamPkeyIds);  // remove first teamId
        if (teamPkeyIds.length > 0) {
            try {
                Workflow.callMethod('AbAssetManagement-AssetManagementService-deleteDigitalSignatureDocument', autonumberId, teamPkeyIds, fieldName, parameters);
            } catch (e) {
                Workflow.handleError(e);
            }
        }
    },
    isValidForm: function (objForm, isOwner) {
        if (isOwner) {
            // validate date start
            var dateStart = objForm.getFieldValue('team.date_start');
            var values = {
                "team.date_start": dateStart
            };
            try {
                var result = Workflow.callMethod('AbAssetManagement-AssetManagementService-validateNewOwner', values, this.selectedAssets, this.allPagesSelected, this.assetRestriction);
                if (result.code == 'executed') {
                    var jsonResult = JSON.parse(result.jsonExpression);
                    if (!jsonResult.valid) {
                        View.showMessage(getMessage('errInvalidOwnerStartDate'));
                        objForm.addInvalidField('team.date_start', '');
                        objForm.displayValidationResult();
                        return false;
                    }
                }
            } catch (e) {
                Workflow.handleError(e);
                return false;
            }
        } else {
            var startDate = objForm.getFieldValue('team.date_start');
            var endDate = objForm.getFieldValue('team.date_end');
            //check start date and end date
            if (valueExistsNotEmpty(startDate) && valueExistsNotEmpty(endDate)) {
                var objStartDate = objForm.getDataSource().parseValue('team.date_start', startDate, false);
                var objEndDate = objForm.getDataSource().parseValue('team.date_end', endDate, false);
                if (!DateMath.after(objEndDate, objStartDate)) {
                    View.showMessage(getMessage('errEndDateBeforeStart'));
                    objForm.addInvalidField('team.date_end', '');
                    objForm.displayValidationResult();
                    return false;
                }
            }
        }
        return true;
    },
    afterSelectCustodianName: function (sourceTable, sourceRecord) {
        this.sourceTable = sourceTable;
        this.sourceRecord = new Ab.data.Record(sourceRecord.values);
        var objCustodianForm = View.panels.get('abAddCustodianOther_form');
        if (this.isOwner) {
            objCustodianForm = View.panels.get('abAddCustodianOwner_form');
        }
        this.setTeamMember(objCustodianForm, sourceTable, sourceRecord);
    },
    // set team fields
    setTeamMember: function (objForm, sourceTable, record) {
        function getValueIfExistsNotEmpty(value, defaultValue) {
            return valueExistsNotEmpty(value) ? value : defaultValue;
        }
        objForm.clearImage('doc_graphic_image');
        objForm.setFieldValue('team.source_table', sourceTable);
        objForm.setFieldValue('team.status', 'Active');
        if ('em' === sourceTable) {
            objForm.setFieldValue('custodian_name', getValueIfExistsNotEmpty(record.getValue("em.name"), record.getValue("em.em_id")));
            objForm.setFieldValue('team.em_id', record.getValue("em.em_id"));
            objForm.setFieldValue('em.em_id', record.getValue("em.em_id"));
            objForm.setFieldValue('team.contact_id', '');
            objForm.setFieldValue('team.vn_id', '');
            objForm.setFieldValue('team.email_archive', record.getValue("em.email"));
            objForm.setFieldValue('team.cell_num_archive', record.getValue("em.cellular_number"));
            objForm.setFieldValue('team.phone_archive', record.getValue("em.phone"));
            objForm.setFieldValue('team.name_archive', record.getValue("em.name_last") + ', ' + record.getValue("em.name_first"));
            objForm.setFieldValue('em.em_photo', record.getValue("em.em_photo"));
            objForm.setFieldValue('team.source', getMessage('msgSourceName_em'));
        }
        if ('contact' === sourceTable) {
            objForm.setFieldValue('custodian_name', getValueIfExistsNotEmpty(record.getValue("contact.name"), record.getValue("contact.contact_id")));
            objForm.setFieldValue('team.contact_id', record.getValue("contact.contact_id"));
            objForm.setFieldValue('contact.contact_id', record.getValue("contact.contact_id"));
            objForm.setFieldValue('team.em_id', '');
            objForm.setFieldValue('team.vn_id', '');
            objForm.setFieldValue('team.email_archive', record.getValue("contact.email"));
            objForm.setFieldValue('team.cell_num_archive', record.getValue("contact.cellular_number"));
            objForm.setFieldValue('team.phone_archive', record.getValue("contact.phone"));
            objForm.setFieldValue('team.name_archive', record.getValue("contact.name_last") + ', ' + record.getValue("contact.name_first"));
            objForm.setFieldValue('contact.contact_photo', record.getValue("contact.contact_photo"));
            objForm.setFieldValue('team.source', getMessage('msgSourceName_contact'));
        }
        if ('vn' === sourceTable) {
            objForm.setFieldValue('custodian_name', getValueIfExistsNotEmpty(record.getValue("vn.company"), record.getValue("vn.vn_id")));
            objForm.setFieldValue('team.vn_id', record.getValue("vn.vn_id"));
            objForm.setFieldValue('vn.vn_id', record.getValue("vn.vn_id"));
            objForm.setFieldValue('team.em_id', '');
            objForm.setFieldValue('team.contact_id', '');
            objForm.setFieldValue('team.email_archive', record.getValue("vn.email"));
            objForm.setFieldValue('team.cell_num_archive', record.getValue("vn.alt_phone"));
            objForm.setFieldValue('team.phone_archive', record.getValue("vn.phone"));
            objForm.setFieldValue('team.name_archive', record.getValue("vn.contact"));
            objForm.setFieldValue('vn.vendor_photo', record.getValue("vn.vendor_photo"));
            objForm.setFieldValue('team.source', getMessage('msgSourceName_vn'));
        }
        if (valueExistsNotEmpty(sourceTable)) {
            objForm.setFieldValue('source', getMessage('msgSourceName_' + sourceTable));
        }
        if (valueExistsNotEmpty(objForm.getFieldValue('team.contact_id'))) {
            if (valueExistsNotEmpty(objForm.getFieldValue('contact.contact_photo'))) {
                objForm.showImageDoc('doc_graphic_image', 'contact.contact_id', 'contact.contact_photo');
            }
        } else if (valueExistsNotEmpty(objForm.getFieldValue('team.em_id'))) {
            if (valueExistsNotEmpty(objForm.getFieldValue('em.em_photo'))) {
                objForm.showImageDoc('doc_graphic_image', 'em.em_id', 'em.em_photo');
            }
        } else if (valueExistsNotEmpty(objForm.getFieldValue('team.vn_id'))) {
            if (valueExistsNotEmpty(objForm.getFieldValue('vn.vendor_photo'))) {
                objForm.showImageDoc('doc_graphic_image', 'vn.vn_id', 'vn.vendor_photo');
            }
        }
    },
    /**
     * Prepare asset registry restriction
     */
    prepareAssetRegistryRestriction: function () {
        if (this.assetRestriction == null) {
            this.assetRestriction = {};
        }
        if (this.selectedAssets == null) {
            this.selectedAssets = [];
        }
        if (!this.allPagesSelected) {
            this.assetRestriction['blTypeRestriction'] = "1=1";
            this.assetRestriction['propertyTypeRestriction'] = "1=1";
            this.assetRestriction['eqTypeRestriction'] = "1=1";
            this.assetRestriction['taTypeRestriction'] = "1=1";
            this.assetRestriction['sqlTypeRestriction'] = "1=1";
            var sqlAssets = [];
            for ( var i = 0; i < this.selectedAssets.length; i++ ) {
                var objAsset = this.selectedAssets[i]
                sqlAssets.push("(bl.asset_type = '{0}' AND bl.asset_id = '{1}')".replace('{0}', objAsset['asset_type']).replace('{1}', objAsset['asset_id']));
            }
            if (sqlAssets.length > 0) {
                this.assetRestriction['sqlTypeRestriction'] = "(" + sqlAssets.join(" OR ") + ")";
            }
        } else {
            if (!valueExistsNotEmpty(this.assetRestriction['blTypeRestriction'])) {
                this.assetRestriction['blTypeRestriction'] = "1=1";
            }
            if (!valueExistsNotEmpty(this.assetRestriction['eqTypeRestriction'])) {
                this.assetRestriction['eqTypeRestriction'] = "1=1";
            }
            if (!valueExistsNotEmpty(this.assetRestriction['propertyTypeRestriction'])) {
                this.assetRestriction['propertyTypeRestriction'] = "1=1";
            }
            if (!valueExistsNotEmpty(this.assetRestriction['taTypeRestriction'])) {
                this.assetRestriction['taTypeRestriction'] = "1=1";
            }
            if (!valueExistsNotEmpty(this.assetRestriction['sqlTypeRestriction'])) {
                this.assetRestriction['sqlTypeRestriction'] = "1=1";
            }
        }
    },
    /**
     * Overwrite form updateDocument. Show only checkIn, view and delete actions.
     */
    updateDocumentButtons: function () {
        var panelId = this.id;
        this.fields.each(function (field) {
            var fieldDef = field.fieldDef;
            if (fieldDef.isDocument) {
                var panel = field.panel;
                var fullName = fieldDef.fullName;
                var documentInput = panel.getFieldElement(fullName);
                documentInput.disabled = panel.newRecord;
                var documentValue = panel.getFieldValue(fullName);
                var documentExists = (documentValue !== '');
                var canShow = documentExists;
                var canCheckIn = (!documentExists && !fieldDef.readOnly && !panel.newRecord);
                var canCheckOut = (documentExists && !fieldDef.readOnly && !panel.newRecord);
                // checkin via file browser if showCheckInDialog attribute is false
                var doBrowseForCheckIn = canCheckIn && !fieldDef.showCheckInDialog;
                var prefix = panelId + '_' + fullName;
                panel.showElement(prefix + '_showDocument', canShow);
                panel.showElement(prefix + '_checkInNewDocument', canCheckIn && fieldDef.showCheckInDialog);
                panel.showElement(prefix + '_checkInNewDocumentVersion', false);
                panel.showElement(prefix + '_checkOutDocument', false);
                panel.showElement(prefix + '_lockDocument', false);
                panel.showElement(prefix + '_deleteDocument', canCheckOut);
                // turn on only one of text input with buttons or file input
                panel.showElement(prefix, !doBrowseForCheckIn);
                panel.showElement(prefix + '_browseDocument_div', doBrowseForCheckIn);
            }
        });
    }
});
/**
 * Select value for custodian name.
 */
function onSelectCustodianName() {
    View.openDialog('ab-team-member-select.axvw', null, false, {
        width: 1024,
        height: 600,
        callback: function (sourceTable, record) {
            var controller = View.controllers.get('abAddCustodianController');
            controller.afterSelectCustodianName(sourceTable, record);
            View.closeDialog();
        }
    })
}