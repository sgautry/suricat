/**
 * Edit owner and custodian.
 *
 * Input:
 *    - view restriction  for  (team.autonumbered_id)
 *  - assetType:  values(property, bl, eq, ta)
 *  - assetCode: selected asset id
 *  - isOwner: boolean if is in edit owner mode
 *
 * @author draghici
 */
var abEditCustodianController = View.createController('abEditCustodianController', {
    // callback function
    callbackFunction: null,
    // if is new record
    newRecord: false,
    // view restriction
    restriction: null,
    // if selected custodian is owner
    isOwner: false,
    // selected asset type
    assetType: null,
    // selected asset code
    assetCode: null,
    // form is read only mode
    isReadOnly: false,
    afterViewLoad: function () {
        this.newRecord = this.view.newRecord;
        if (valueExists(this.view.restriction)) {
            this.restriction = this.view.restriction;
        }
        if (valueExists(this.view.parameters)) {
            if (valueExists(this.view.parameters.callback)) {
                this.callbackFunction = this.view.parameters.callback;
            }
            if (!valueExists(this.restriction) && valueExists(this.view.parameters.restriction)) {
                this.restriction = this.view.parameters.restriction;
            }
            if (valueExists(this.view.parameters.assetType)) {
                this.assetType = this.view.parameters.assetType;
            }
            if (valueExists(this.view.parameters.assetCode)) {
                this.assetCode = this.view.parameters.assetCode;
            }
            if (valueExists(this.view.parameters.isOwner)) {
                this.isOwner = this.view.parameters.isOwner;
            }
            if (valueExists(this.view.parameters.isReadOnly)) {
                this.isReadOnly = this.view.parameters.isReadOnly;
            }
        }
        // set translatable parameters
        this.abEamEditCustodian_form.addParameter('sourceName_vn', getMessage('msgSourceName_vn'));
        this.abEamEditCustodian_form.addParameter('sourceName_contact', getMessage('msgSourceName_contact'));
        this.abEamEditCustodian_form.addParameter('sourceName_em', getMessage('msgSourceName_em'));
        this.abEamEditCustodian_form.addParameter('asset_type', this.assetType);
    },
    afterInitialDataFetch: function () {
        var formTitle = getMessage('titleEditCustodian');
        if (this.isOwner) {
            formTitle = getMessage('titleEditOwner');
        }
        this.updateFormDisplay();
        this.abEamEditCustodian_form.refresh(this.restriction, this.newRecord);
        if (this.isReadOnly) {
            formTitle = this.abEamEditCustodian_form.record.getValue('team.custodian_name');
            this.disableActionsAndFields();
        }
        this.abEamEditCustodian_form.setTitle(formTitle);
    },
    abEamEditCustodian_form_afterRefresh: function () {
        // make custodian name no editable
        this.abEamEditCustodian_form.getFieldElement('team.custodian_name').disabled = true;
        // load photo
        var sourceTable = this.abEamEditCustodian_form.getFieldValue('team.source_table')
        if (sourceTable === 'vn' && valueExistsNotEmpty(this.abEamEditCustodian_form.getFieldValue('vn.vendor_photo'))) {
            this.abEamEditCustodian_form.showImageDoc('doc_graphic_image', 'vn.vn_id', 'vn.vendor_photo');
        } else if (sourceTable === 'contact' && valueExistsNotEmpty(this.abEamEditCustodian_form.getFieldValue('contact.contact_photo'))) {
            this.abEamEditCustodian_form.showImageDoc('doc_graphic_image', 'contact.contact_id', 'contact.contact_photo');
        } else if (sourceTable === 'em' && valueExistsNotEmpty(this.abEamEditCustodian_form.getFieldValue('em.em_photo'))) {
            this.abEamEditCustodian_form.showImageDoc('doc_graphic_image', 'em.em_id', 'em.em_photo');
        }
        this.abEamEditCustodian_form.fields.get('team.status').removeOptions({'Archived': 'Archived', 'Removed': 'Removed'});
    },
    /**
     * Update form layout:
     *    - show/ hide fields
     *  - enable/disable fields
     */
    updateFormDisplay: function () {
        var isAssetDisposed = this.isAssetDisposed(),
            isActive = this.abEamEditCustodian_form.getFieldValue('team.status') === 'Active',
            status = this.abEamEditCustodian_form.getFieldValue('team.status');
        if (this.isOwner) {
            // action buttons
            this.abEamEditCustodian_form.actions.get('save').show(isActive);
            this.abEamEditCustodian_form.actions.get('cancel').show(isActive);
            this.abEamEditCustodian_form.actions.get('delete').show(false);
            // enable disable fields
            this.updateFieldDisplay(this.abEamEditCustodian_form, 'team.custodian_name', false, true, true);
            this.updateFieldDisplay(this.abEamEditCustodian_form, 'team.custodian_type', false, true, true);
            this.updateFieldDisplay(this.abEamEditCustodian_form, 'team.date_start', false, true, true);
            this.updateFieldDisplay(this.abEamEditCustodian_form, 'team.date_end', isActive && isAssetDisposed, !isActive || (isActive && isAssetDisposed), false);
            this.updateFieldDisplay(this.abEamEditCustodian_form, 'team.status', isActive, true, false);
            // show/ hide fields
            this.updateFieldDisplay(this.abEamEditCustodian_form, 'team.digital_signature', isActive, true, false);
            this.updateFieldDisplay(this.abEamEditCustodian_form, 'team.notes', isActive, true, false);
        } else {
            // enable disable fields
            this.updateFieldDisplay(this.abEamEditCustodian_form, 'team.custodian_name', true, true, true);
            this.updateFieldDisplay(this.abEamEditCustodian_form, 'team.custodian_type', true, true, true);
            this.updateFieldDisplay(this.abEamEditCustodian_form, 'team.date_start', true, true, true);
            this.updateFieldDisplay(this.abEamEditCustodian_form, 'team.date_end', true, true, 'Inactive' === status);
            // show/ hide fields
            this.updateFieldDisplay(this.abEamEditCustodian_form, 'team.digital_signature', false, false, false);
            this.updateFieldDisplay(this.abEamEditCustodian_form, 'team.notes', true, true, false);
        }
        //this.abEamEditCustodian_form.onModelUpdate();
        if (this.assetType === 'property' || this.assetType === 'bl') {
            this.abEamEditCustodian_form.setFieldLabel('team.asset_standard', '');
            this.abEamEditCustodian_form.setFieldValue('team.asset_standard', '');
        }
    },
    /**
     * Check if asset is disposed.
     * @returns {boolean} true if disposed.
     */
    isAssetDisposed: function () {
        var assetIsDisposed = false;
        var disposalStatus = {
            'property': ['SOLD', 'DISPOSED', 'DONATED'],//'SOLD', 'DISPOSED', 'DONATED'
            'bl': ['Disposed', 'Donated'],//'DISPOSED', 'DONATED'
            'eq': ['sold', 'disp', 'don', 'sto', 'miss'],//'Sold', 'Disposed', 'Donated', 'Stolen', 'Missing'
            'ta': ['sold', 'disp', 'don', 'sto']//'Sold', 'Disposed', 'Donated', 'Stolen'
        };
        var record = View.dataSources.get('abEamAssetRegistry_ds').getRecord(new Ab.view.Restriction({
            'bl.asset_type': this.assetType,
            'bl.asset_id': this.assetCode
        }));
        var status = record.getValue('bl.asset_status');
        if (valueExists(status) && _.contains(disposalStatus[this.assetType], status)) {
            assetIsDisposed = true;
        }
        return assetIsDisposed;
    },
    updateFieldDisplay: function (objForm, fieldName, enabled, visible, required) {
        if (valueExists(enabled) && !enabled) {
            objForm.enableField(fieldName, enabled);
            // make field readonly, not just disabled
            objForm.fields.get(fieldName).config.readOnly = 'true';
            objForm.fields.get(fieldName).fieldDef.readOnly = !enabled;
            var fieldCell = objForm.getFieldCell(fieldName);
            var fieldElement = objForm.getFieldElement(fieldName);
            if (fieldCell && fieldElement.tagName !== 'SELECT') {
                var id = "Show" + fieldCell.children[0].id;
                var showElem = document.createElement('SPAN');
                showElem.id = id;
                showElem.name = id;
                showElem.className = fieldCell.children[0].className;
                showElem.innerHTML = fieldCell.children[0].value;
                fieldCell.children[0].style.display = 'none';
                fieldCell.children[1].style.display = 'none';
                fieldCell.appendChild(showElem);
            }
        }
        if (valueExists(required)) {
            this.setFieldRequired(objForm, fieldName, required);
        }
        if (valueExists(visible) && !visible) {
            objForm.showField(fieldName, visible);
        }
    },
    setFieldRequired: function (objForm, fieldName, required) {
        var fieldDef = objForm.fields.get(fieldName).fieldDef;
        if (fieldDef.required && required) {
            return;
        }
        fieldDef.required = required;
        if (required) {
            Ext.DomHelper.append(objForm.id + '_' + fieldName + '_labelCell',
                '<span id="' + fieldName + '.required_star" class="required" name="' + fieldName + '.required_star">*</span>',
                true);
        } else if ($(fieldName + ".required_star")) {
            $(fieldName + ".required_star").remove();
        }
    },
    abEamEditCustodian_form_onCancel: function () {
        // cancel all unsaved changes and refresh the form
        this.abEamEditCustodian_form.refresh(this.restriction);
    },
    abEamEditCustodian_form_onSave: function () {
        if (this.abEamEditCustodian_form.canSave() && this.validateForm()) {
            if (this.abEamEditCustodian_form.save()) {
                if (valueExists(this.callbackFunction)) {
                    this.callbackFunction();
                }
            }
        }
    },
    /**
     * Set current date to date_end when status Inactive for other custodian or owner custodian and disposed asset
     * @param value Status
     */
    onChangeStatus: function (value) {
        if (!this.isOwner || (this.isOwner && this.isAssetDisposed())) {
            var isInactive = 'Inactive' === value;
            if (isInactive) {
                this.abEamEditCustodian_form.setFieldValue('team.date_end', getCurrentDateInISOFormat());
            } else {
                this.abEamEditCustodian_form.setFieldValue('team.date_end', '');
            }
            this.setFieldRequired(this.abEamEditCustodian_form, 'team.date_end', isInactive);
        }
    },
    onChangeDateEnd: function (value) {
        if (!this.isOwner && valueExistsNotEmpty(value)) {
            var objEndDate = this.abEamEditCustodian_form.getDataSource().parseValue('team.date_end', getDateWithISOFormat(value), false);
            var today = this.abEamEditCustodian_form.getDataSource().parseValue('team.date_end', getCurrentDateInISOFormat(), false);
            if (this.dateBeforeOrEqual(objEndDate, today)) {
                this.abEamEditCustodian_form.setFieldValue('team.status', 'Inactive');
                this.setFieldRequired(this.abEamEditCustodian_form, 'team.date_end', true);
            }
        } else if (this.isOwner && this.isAssetDisposed()) {
            this.abEamEditCustodian_form.setFieldValue('team.status', 'Inactive');
        }
    },
    /**
     * Check custom conditions before save.
     */
    validateForm: function () {
        var oldFieldValues = this.abEamEditCustodian_form.getOldFieldValues();
        var newStatus = this.abEamEditCustodian_form.getFieldValue('team.status');
        var oldStatus = oldFieldValues['team.status'];
        var startDate = this.abEamEditCustodian_form.getFieldValue('team.date_start');
        var endDate = this.abEamEditCustodian_form.getFieldValue('team.date_end');
        var custodianName = this.abEamEditCustodian_form.getFieldValue('team.custodian_name');
        var isAssetDisposed = this.isAssetDisposed();
        // validation when is owner
        if (this.isOwner) {
            if (oldStatus === 'Active' && newStatus === 'Inactive') {
                if (!isAssetDisposed) {
                    View.showMessage(getMessage('errDeactivateOwner'));
                    this.abEamEditCustodian_form.addInvalidField('team.status', '');
                    this.abEamEditCustodian_form.displayValidationResult();
                    return false;
                }
                if (!valueExistsNotEmpty(endDate)) {
                    View.showMessage(getMessage('errEndDateMissing'));
                    this.abEamEditCustodian_form.addInvalidField('team.date_end', '');
                    this.abEamEditCustodian_form.displayValidationResult();
                    return false;
                }
            }
            if (isAssetDisposed && newStatus !== 'Inactive') {
                View.showMessage(getMessage('errInvalidStatus'));
                this.abEamEditCustodian_form.addInvalidField('team.status', '');
                this.abEamEditCustodian_form.displayValidationResult();
                return false;
            }
            if (oldStatus === 'Inactive' && newStatus === 'Active'
                && isAssetAssignedToOwner(this.assetType, this.assetCode, custodianName)) {
                View.showMessage(getMessage('errInvalidOwnerAssigment'));
                return false;
            }
        } else {
            if (newStatus === 'Inactive' && !valueExistsNotEmpty(endDate)) {
                View.showMessage(getMessage('errEndDateMissing'));
                this.abEamEditCustodian_form.addInvalidField('team.date_end', '');
                this.abEamEditCustodian_form.displayValidationResult();
                return false;
            }
        }
        //check start date and end date
        if (valueExistsNotEmpty(startDate) && valueExistsNotEmpty(endDate)) {
            var objStartDate = this.abEamEditCustodian_form.getDataSource().parseValue('team.date_start', startDate, false);
            var objEndDate = this.abEamEditCustodian_form.getDataSource().parseValue('team.date_end', endDate, false);
            if (DateMath.before(objEndDate, objStartDate)) {
                View.showMessage(getMessage('errEndDateBeforeStart'));
                this.abEamEditCustodian_form.addInvalidField('team.date_end', '');
                this.abEamEditCustodian_form.displayValidationResult();
                return false;
            }
        }
        return true
    },
    abEamEditCustodian_form_onDelete: function () {
        var autonumberedId = this.abEamEditCustodian_form.getFieldValue('team.autonumbered_id');
        var record = new Ab.data.Record({
            'team.autonumbered_id': autonumberedId
        }, false);
        var controller = this;
        View.confirm(getMessage('confirmDeleteRecord'), function (button) {
            if (button === 'yes') {
                try {
                    controller.abEamEditCustodian_ds.deleteRecord(record);
                    if (valueExists(controller.callbackFunction)) {
                        controller.callbackFunction();
                    }
                } catch (e) {
                    Workflow.handleError(e);
                    return false;
                }
            }
        });
    },
    setTeamMember: function (sourceTable, record) {
        function getValueIfExistsNotEmpty(value, defaultValue) {
            return valueExistsNotEmpty(value) ? value : defaultValue;
        }
        this.abEamEditCustodian_form.clearImage('doc_graphic_image');
        this.abEamEditCustodian_form.setFieldValue('team.source_table', sourceTable);
        this.abEamEditCustodian_form.setFieldValue('team.status', 'Active');
        if ('em' === sourceTable) {
            this.abEamEditCustodian_form.setFieldValue('team.custodian_name', getValueIfExistsNotEmpty(record.getValue("em.name"), record.getValue("em.em_id")));
            this.abEamEditCustodian_form.setFieldValue('team.em_id', record.getValue("em.em_id"));
            this.abEamEditCustodian_form.setFieldValue('em.em_id', record.getValue("em.em_id"));
            this.abEamEditCustodian_form.setFieldValue('team.contact_id', '');
            this.abEamEditCustodian_form.setFieldValue('team.vn_id', '');
            this.abEamEditCustodian_form.setFieldValue('team.email_archive', record.getValue("em.email"));
            this.abEamEditCustodian_form.setFieldValue('team.cell_num_archive', record.getValue("em.cellular_number"));
            this.abEamEditCustodian_form.setFieldValue('team.phone_archive', record.getValue("em.phone"));
            this.abEamEditCustodian_form.setFieldValue('team.name_archive', record.getValue("em.name_last") + ', ' + record.getValue("em.name_first"));
            this.abEamEditCustodian_form.setFieldValue('em.em_photo', record.getValue("em.em_photo"));
            this.abEamEditCustodian_form.setFieldValue('team.source', getMessage('msgSourceName_em'));
        }
        if ('contact' === sourceTable) {
            this.abEamEditCustodian_form.setFieldValue('team.custodian_name', getValueIfExistsNotEmpty(record.getValue("contact.name"), record.getValue("contact.contact_id")));
            this.abEamEditCustodian_form.setFieldValue('team.contact_id', record.getValue("contact.contact_id"));
            this.abEamEditCustodian_form.setFieldValue('contact.contact_id', record.getValue("contact.contact_id"));
            this.abEamEditCustodian_form.setFieldValue('team.em_id', '');
            this.abEamEditCustodian_form.setFieldValue('team.vn_id', '');
            this.abEamEditCustodian_form.setFieldValue('team.email_archive', record.getValue("contact.email"));
            this.abEamEditCustodian_form.setFieldValue('team.cell_num_archive', record.getValue("contact.cellular_number"));
            this.abEamEditCustodian_form.setFieldValue('team.phone_archive', record.getValue("contact.phone"));
            this.abEamEditCustodian_form.setFieldValue('team.name_archive', record.getValue("contact.name_last") + ', ' + record.getValue("contact.name_first"));
            this.abEamEditCustodian_form.setFieldValue('contact.contact_photo', record.getValue("contact.contact_photo"));
            this.abEamEditCustodian_form.setFieldValue('team.source', getMessage('msgSourceName_contact'));
        }
        if ('vn' === sourceTable) {
            this.abEamEditCustodian_form.setFieldValue('team.custodian_name', getValueIfExistsNotEmpty(record.getValue("vn.company"), record.getValue("vn.vn_id")));
            this.abEamEditCustodian_form.setFieldValue('team.vn_id', record.getValue("vn.vn_id"));
            this.abEamEditCustodian_form.setFieldValue('vn.vn_id', record.getValue("vn.vn_id"));
            this.abEamEditCustodian_form.setFieldValue('team.em_id', '');
            this.abEamEditCustodian_form.setFieldValue('team.contact_id', '');
            this.abEamEditCustodian_form.setFieldValue('team.email_archive', record.getValue("vn.email"));
            this.abEamEditCustodian_form.setFieldValue('team.cell_num_archive', record.getValue("vn.alt_phone"));
            this.abEamEditCustodian_form.setFieldValue('team.phone_archive', record.getValue("vn.phone"));
            this.abEamEditCustodian_form.setFieldValue('team.name_archive', record.getValue("vn.contact"));
            this.abEamEditCustodian_form.setFieldValue('vn.vendor_photo', record.getValue("vn.vendor_photo"));
            this.abEamEditCustodian_form.setFieldValue('team.source', getMessage('msgSourceName_vn'));
        }
        // update source field
        if (valueExistsNotEmpty(sourceTable)) {
            this.abEamEditCustodian_form.setFieldValue('team.source', getMessage('msgSourceName_' + sourceTable));
        }
        if (valueExistsNotEmpty(this.abEamEditCustodian_form.getFieldValue('team.contact_id'))) {
            if (valueExistsNotEmpty(this.abEamEditCustodian_form.getFieldValue('contact.contact_photo'))) {
                this.abEamEditCustodian_form.showImageDoc('doc_graphic_image', 'contact.contact_id', 'contact.contact_photo');
            }
        } else if (valueExistsNotEmpty(this.abEamEditCustodian_form.getFieldValue('team.em_id'))) {
            if (valueExistsNotEmpty(this.abEamEditCustodian_form.getFieldValue('em.em_photo'))) {
                this.abEamEditCustodian_form.showImageDoc('doc_graphic_image', 'em.em_id', 'em.em_photo');
            }
        } else if (valueExistsNotEmpty(this.abEamEditCustodian_form.getFieldValue('team.vn_id'))) {
            if (valueExistsNotEmpty(this.abEamEditCustodian_form.getFieldValue('vn.vendor_photo'))) {
                this.abEamEditCustodian_form.showImageDoc('doc_graphic_image', 'vn.vn_id', 'vn.vendor_photo');
            }
        }
    },
    disableActionsAndFields: function () {
        var formPanel = View.panels.get('abEamEditCustodian_form');
        formPanel.fields.each(function (field) {
            formPanel.enableField(field.fieldDef.id, false);
        });
        formPanel.actions.each(function (action) {
            action.show(false);
        });
    },
    /**
     * Determines whether a given date is before or equal another date on the calendar.
     * @method before
     * @param {Date} date        The Date object to compare with the compare argument
     * @param {Date} compareTo   The Date object to use for the comparison
     * @return {boolean} true if the date occurs before or equal the compared date; false if not.
     */
    dateBeforeOrEqual: function (date, compareTo) {
        var ms = compareTo.getTime();
        if (date.getTime() <= ms) {
            return true;
        } else {
            return false;
        }
    }
});
/**
 * Select value for custodian name.
 * @returns
 */
function onSelectCustodianName() {
    View.openDialog('ab-team-member-select.axvw', null, false, {
        width: 1024,
        height: 600,
        callback: function (sourceTable, record) {
            View.closeDialog();
            View.controllers.get('abEditCustodianController').setTeamMember(sourceTable, record);
        }
    })
}
function isAssetAssignedToOwner(assetType, assetCode, custodianName) {
    var parameters = null;
    if (assetType === 'property') {
        parameters = {
            tableName: 'team',
            fieldNames: toJSON(['team.pr_id', 'team.is_owner']),
            restriction: toJSON(new Ab.view.Restriction({'team.pr_id': assetCode, 'team.status': 'Active', 'team.is_owner': 1}))
        };
    } else if (assetType === 'bl') {
        parameters = {
            tableName: 'team',
            fieldNames: toJSON(['team.bl_id', 'team.is_owner']),
            restriction: toJSON(new Ab.view.Restriction({'team.bl_id': assetCode, 'team.status': 'Active', 'team.is_owner': 1}))
        };
    } else if (assetType === 'ta') {
        parameters = {
            tableName: 'team',
            fieldNames: toJSON(['team.pr_id', 'team.is_owner']),
            restriction: toJSON(new Ab.view.Restriction({'team.ta_id': assetCode, 'team.status': 'Active', 'team.is_owner': 1}))
        };
    } else if (assetType === 'eq') {
        parameters = {
            tableName: 'team',
            fieldNames: toJSON(['team.pr_id', 'team.is_owner']),
            restriction: toJSON(new Ab.view.Restriction({'team.eq_id': assetCode, 'team.status': 'Active', 'team.is_owner': 1}))
        };
    }
    try {
        var result = Workflow.call('AbCommonResources-getDataRecord', parameters);
        if (result.code === 'executed') {
            if (valueExists(result.dataSet) && !result.dataSet.isNew) {
                return true;
            }
            return false;
        }
    } catch (e) {
        Workflow.handleError(e);
        return false;
    }
}