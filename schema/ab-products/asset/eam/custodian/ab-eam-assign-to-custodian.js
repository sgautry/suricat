/**
 * Assign asset to custodian controller.
 */
var abAssignToCustodianController = View.createController('abAssignToCustodianController', {
    // is all assets are selected
    allPagesSelected: false,
    /* object  with asset list restriction, including  index and smart search
     * Is passed to pop-up window Change Owner or Assign Custodian
     */
    assetRestriction: null,
    // array  with objects for selected assets {assetId, assetType}
    selectedAssets: null,
    // filter restriction
    filterSelection: {
        "asset_assigned": "",
        "asset_type": "",
        "custodian_status": ""
    },
    // selected asset record
    selectedAssetRecord: null,
    afterViewLoad: function () {
        // customize mini console
        this.abAssignToCustodianAssets_list.addEventListener('afterBuildHeader', this.abAssignToCustodianAssets_list_customizeHeader, this);
        this.abAssignToCustodianAssets_list.addEventListener('onMultipleSelectionChange', this.abAssignToCustodianAssets_list_onMultipleSelectionChange, this);
        this.abAssignToCustodianAssets_list.selectAll = this.onClickSelectAll.createDelegate(this, [this.abAssignToCustodianAssets_list], true);
        var controller = this;
        // customize grid columns
        this.abAssignToCustodianAssets_list.afterCreateCellContent = function (row, column, cellElement) {
            controller.customizeAssetRegistryFields(row, column, cellElement);
        };
        this.abAssignToCustodian_ownerList.afterCreateCellContent = function (row, column, cellElement) {
            controller.customizeCustodianFields(row, column, cellElement);
        };
        this.abAssignToCustodian_otherList.afterCreateCellContent = function (row, column, cellElement) {
            controller.customizeCustodianFields(row, column, cellElement);
        };
    },
    afterInitialDataFetch: function () {
        var selectedAssetIds = [];
        if (valueExists(View.parameters)) {
            if (valueExistsNotEmpty(View.parameters.viewName)) {
                View.setTitle(getMessage(View.parameters.viewName));
            }
            if (valueExists(View.parameters.isReadOnly) && View.parameters.isReadOnly) {
                this.abAssignToCustodianAssets_list.enableSelectAll(false);
                this.abAssignToCustodianAssets_list.showColumn('multipleSelectionColumn', false);
                this.abAssignToCustodian_filter.actions.get('assign').show(false);
                this.abAssignToCustodian_ownerList.actions.get('addOwner').show(false);
                this.abAssignToCustodian_otherList.actions.get('addOther').show(false);
            }
            if (valueExistsNotEmpty(View.parameters.assetType)) {
                var assetType = View.parameters["assetType"];
                this.abAssignToCustodian_filter.setFieldValue('asset_type', assetType);
                this.abAssignToCustodian_filter.setFieldValue('asset_assigned', 'assigned');
                this.abAssignToCustodian_filter.setFieldValue('custodian_status', 'active');
                this.filterSelection['asset_assigned'] = 'assigned';
                this.filterSelection['asset_type'] = View.parameters.assetType;
                this.filterSelection['custodian_status'] = 'active';
                var selectedAssets = View.parameters["selectedAssets"];
                if (valueExists(selectedAssets) && !_.isEmpty(selectedAssets)) {
                    selectedAssetIds = View.parameters["selectedAssets"][assetType];
                }
            }
        }
        this.applyFilter(selectedAssetIds);
    },
    /**
     * On apply filter
     * called from onChange event from all console fields
     */
    onChangeFilter: function (field) {
        var fieldName = field.id.replace('abAssignToCustodian_filter_', '');
        var fieldValue = field.value;
        this.filterSelection[fieldName] = fieldValue;
        if ('asset_assigned' === fieldName && 'unassigned' === fieldValue) {
            this.abAssignToCustodian_filter.setFieldValue('custodian_status', '');
            this.filterSelection['custodian_status'] = '';
        }
        this.applyFilter();
    },
    /**
     * Apply selected filter
     * @param {Array.<String>} selectedAssetIds - Selected asset ids.
     */
    applyFilter: function (selectedAssetIds) {
        var blRestriction = " 1 = 1 ";
        var propertyRestriction = " 1 = 1 ";
        var eqRestriction = " 1 = 1 ";
        var taRestriction = " 1 = 1 ";
        var sqlRestriction = " 1 = 1 ";
        if (valueExistsNotEmpty(this.filterSelection['asset_type'])) {
            sqlRestriction = "bl.asset_type = '{0}'".replace('{0}', makeSafeSqlValue(this.filterSelection['asset_type']));
        }
        var restriction = "";
        if ('assigned' === this.filterSelection['asset_assigned']) {
            restriction = "EXISTS(SELECT team.autonumbered_id FROM team WHERE team.team_type = 'Equipment' AND team.custodian_type IS NOT NULL {0} {1})";
        } else if ('unassigned' === this.filterSelection['asset_assigned']) {
            restriction = "NOT EXISTS(SELECT team.autonumbered_id FROM team WHERE team.team_type = 'Equipment' AND team.custodian_type IS NOT NULL {1})";
        }
        var status = "";
        if ('active' === this.filterSelection['custodian_status']) {
            status = " AND team.status = 'Active' ";
        } else if ('inactive' === this.filterSelection['custodian_status']) {
            status = " AND team.status <> 'Active'"
        }
        if (restriction.length === 0 && status.length > 0) {
            restriction = "(EXISTS(SELECT team.autonumbered_id FROM team WHERE team.team_type = 'Equipment' AND team.custodian_type IS NOT NULL {0} {1}) " +
                "OR NOT EXISTS(SELECT team.autonumbered_id FROM team WHERE team.team_type = 'Equipment' AND team.custodian_type IS NOT NULL {2}))";
        }
        restriction = restriction.replace('{0}', status);
        if (restriction.length > 0) {
            blRestriction = restriction.replace('{1}', "AND team.bl_id = bl.bl_id").replace('{2}', "AND team.bl_id = bl.bl_id");
            propertyRestriction = restriction.replace('{1}', "AND team.pr_id = property.pr_id").replace('{2}', "AND team.pr_id = property.pr_id");
            eqRestriction = restriction.replace('{1}', "AND team.eq_id = eq.eq_id").replace('{2}', "AND team.eq_id = eq.eq_id");
            taRestriction = restriction.replace('{1}', "AND team.ta_id = ta.ta_id").replace('{2}', "AND team.ta_id = ta.ta_id");
        }
        this.abAssignToCustodianAssets_list.addParameter('blTypeRestriction', blRestriction);
        this.abAssignToCustodianAssets_list.addParameter('propertyTypeRestriction', propertyRestriction);
        this.abAssignToCustodianAssets_list.addParameter('eqTypeRestriction', eqRestriction);
        this.abAssignToCustodianAssets_list.addParameter('taTypeRestriction', taRestriction);
        this.abAssignToCustodianAssets_list.addParameter('sqlTypeRestriction', sqlRestriction);
        this.abAssignToCustodianAssets_list.addParameter('useCustodian', true);
        this.abAssignToCustodianAssets_list.refresh();
        if (valueExists(selectedAssetIds) && !_.isEmpty(selectedAssetIds)) {
            // set panel miniconsole filter
            this.setPanelFilterValues('bl.asset_id', selectedAssetIds);
            this.abAssignToCustodianAssets_list.refresh();
            // select automatically first row
            var firstRow = this.abAssignToCustodianAssets_list.rows[0];
            if (valueExists(firstRow)) {
                this.onClickAsset(firstRow.row);
                this.abAssignToCustodianAssets_list.selectRow(firstRow.index);
            }
        } else {
            // hide details panels
            this.abAssignToCustodian_ownerList.show(false, true);
            this.abAssignToCustodian_otherList.show(false, true);
        }
    },
    /**
     * Set miniconsole filter panel
     * @param fieldName
     * @param selectedAssetIds
     */
    setPanelFilterValues: function (fieldName, selectedAssetIds) {
        if (selectedAssetIds.length === 1) {
            this.abAssignToCustodianAssets_list.setFilterValue(fieldName, selectedAssetIds[0]);
        } else {
            var assetIds = '\"' + selectedAssetIds.join(Ab.form.Form.MULTIPLE_VALUES_SEPARATOR) + '\"';
            this.abAssignToCustodianAssets_list.setFilterValue(fieldName, '{' + assetIds.replace(/, \u200C/gi, '\",\"') + '}');
        }
    },
    /**
     * On assign to owner custodian.
     * Called for selected asset record.
     */
    assignToOwner: function () {
        this.getSelectedAssets();
        this.addCustodianCommon(true, this.selectedAssets, this.allPagesSelected, this.assetRestriction, this.afterAssignToOwner.createDelegate(this));
    },
    /**
     * Called after assigned to owner.
     */
    afterAssignToOwner: function () {
        this.refreshView();
    },
    /**
     * On assign to other custodian.
     * Called for selected asset record.
     */
    assignToOther: function () {
        this.getSelectedAssets();
        this.addCustodianCommon(false, this.selectedAssets, this.allPagesSelected, this.assetRestriction, this.afterAssignToOther.createDelegate(this));
    },
    /**
     * Called after assigned to other.
     */
    afterAssignToOther: function () {
        this.refreshView();
    },
    /**
     * On add owner for single asset type.
     * Called for multiple selection.
     */
    abAssignToCustodian_ownerList_onAddOwner: function () {
        this.selectedAssets = [];
        this.selectedAssets.push({
            "asset_type": this.selectedAssetRecord.getValue('bl.asset_type'),
            "asset_id": this.selectedAssetRecord.getValue('bl.asset_id')
        });
        this.addCustodianCommon(true, this.selectedAssets, false, this.assetRestriction, this.afterAddOwner.createDelegate(this));
    },
    /**
     * Called after add owner.
     */
    afterAddOwner: function () {
        this.refreshView();
    },
    /**
     * On add other custodian for single asset type
     * Called for multiple selection.
     */
    abAssignToCustodian_otherList_onAddOther: function () {
        this.selectedAssets = [];
        this.selectedAssets.push({
            "asset_type": this.selectedAssetRecord.getValue('bl.asset_type'),
            "asset_id": this.selectedAssetRecord.getValue('bl.asset_id')
        });
        this.addCustodianCommon(false, this.selectedAssets, false, this.assetRestriction, this.afterAddOther.createDelegate(this));
    },
    /**
     * Called after add other.
     */
    afterAddOther: function () {
        this.abAssignToCustodian_otherList.refresh(this.abAssignToCustodian_otherList.restriction);
    },
    /**
     * Add custodian, common function
     */
    addCustodianCommon: function (isOwner, selectedAssets, allPagesSelected, assetRestriction, callbackFunction) {
        View.openDialog('ab-eam-assign-custodian.axvw', null, true, {
            width: 1024,
            height: 800,
            isOwner: isOwner,
            selectedAssets: selectedAssets,
            useCustomRestriction: allPagesSelected,
            customRestriction: assetRestriction,
            callback: function (addMultipleCustodians) {
                if (valueExists(callbackFunction)) {
                    callbackFunction();
                }
                if (!addMultipleCustodians) {
                    View.closeDialog();
                }
            }
        });
    },
    // refresh entire view, called from callback
    refreshView: function () {
        this.abAssignToCustodianAssets_list.refresh(this.abAssignToCustodianAssets_list.restriction);
        if (valueExistsNotEmpty(this.selectedAssetRecord)) {
            if (this.abAssignToCustodian_ownerList.visible) {
                this.abAssignToCustodian_ownerList.refresh(this.abAssignToCustodian_ownerList.restriction);
            }
            if (this.abAssignToCustodian_otherList.visible) {
                this.abAssignToCustodian_otherList.refresh(this.abAssignToCustodian_otherList.restriction);
            }
        }
    },
    /**
     * Get selected assets. Returns an array of objects {assetType, assetId}
     * or an object with applied restriction when user selected all pages
     */
    getSelectedAssets: function () {
        var selectedRecords = this.abAssignToCustodianAssets_list.getSelectedRecords();
        if (selectedRecords.length > 0) {
            this.selectedAssets = [];
            for ( var i = 0; i < selectedRecords.length; i++ ) {
                var record = selectedRecords[i];
                this.selectedAssets.push({
                    "asset_type": record.getValue('bl.asset_type'),
                    "asset_id": record.getValue('bl.asset_id')
                });
            }
        }
    },
    /**
     * On click select all checkbox.
     */
    onClickSelectAll: function (selected, grid) {
        var dataRows = grid.getDataRows();
        for ( var r = 0; r < dataRows.length; r++ ) {
            var dataRow = dataRows[r];
            var selectionCheckbox = dataRow.firstChild.firstChild;
            if (typeof selectionCheckbox.checked !== 'undefined') {
                if (selectionCheckbox.checked !== selected) {
                    selectionCheckbox.checked = selected;
                    // mark select all action
                    grid.rows[r].isSelectAll = selected;
                    grid.onChangeMultipleSelection(grid.rows[r]);
                    grid.rows[r].isSelectAll = false;
                }
            }
        }
        if (selected) {
            var controller = this;
            View.confirm(getMessage('confirmSelectAllPages'), function (button) {
                if ('yes' === button) {
                    controller.allPagesSelected = true;
                    controller.getAssetListRestriction();
                } else {
                    controller.allPagesSelected = false;
                }
            });
        } else {
            this.allPagesSelected = false;
        }
    },
    abAssignToCustodianAssets_list_onMultipleSelectionChange: function (row) {
        var controller = View.controllers.get('abAssignToCustodianController');
        if (!row.isSelectAll) {
            if (row.row.isSelected()) {
                row.grid.selectRow(row.index);
                this.onClickAsset(row.row);
            }
        }
        if (!row.row.isSelected() && controller.allPagesSelected) {
            controller.allPagesSelected = false;
        }
    },
    getAssetListRestriction: function () {
        var refreshParameters = this.abAssignToCustodianAssets_list.getParametersForRefresh();
        this.assetRestriction = {};
        // read datasource parameters
        if (valueExistsNotEmpty(refreshParameters["blTypeRestriction"])) {
            this.assetRestriction["blTypeRestriction"] = refreshParameters["blTypeRestriction"];
        }
        if (valueExistsNotEmpty(refreshParameters["propertyTypeRestriction"])) {
            this.assetRestriction["propertyTypeRestriction"] = refreshParameters["propertyTypeRestriction"];
        }
        if (valueExistsNotEmpty(refreshParameters["eqTypeRestriction"])) {
            this.assetRestriction["eqTypeRestriction"] = refreshParameters["eqTypeRestriction"];
        }
        if (valueExistsNotEmpty(refreshParameters["taTypeRestriction"])) {
            this.assetRestriction["taTypeRestriction"] = refreshParameters["taTypeRestriction"];
        }
        var indexAndSmartSearch = [];
        if (valueExistsNotEmpty(refreshParameters["filterValues"])) {
            var filterValues = JSON.parse(refreshParameters["filterValues"]);
            for ( var i = 0; i < filterValues.length; i++ ) {
                var objFilterValue = filterValues[i];
                indexAndSmartSearch.push("(UPPER({0}) LIKE '%'||UPPER('{1}')||'%')".replace('{0}', objFilterValue['fieldName']).replace('{1}', objFilterValue['filterValue']));
            }
        }
        if (valueExistsNotEmpty(refreshParameters["indexField"])
            && valueExistsNotEmpty(refreshParameters["indexValue"])) {
            indexAndSmartSearch.push("(UPPER({0}) LIKE UPPER('{1}'))".replace('{0}', refreshParameters['indexField']).replace('{1}', refreshParameters['indexValue']));
        }
        var sqlTypeRestriction = "";
        if (valueExistsNotEmpty(refreshParameters["sqlTypeRestriction"])) {
            sqlTypeRestriction = "(" + refreshParameters["sqlTypeRestriction"] + ")";
        }
        if (indexAndSmartSearch.length > 0) {
            sqlTypeRestriction += (sqlTypeRestriction.length > 0 ? " AND " : "") + indexAndSmartSearch.join(' AND ');
        }
        if (valueExistsNotEmpty(sqlTypeRestriction)) {
            this.assetRestriction["sqlTypeRestriction"] = sqlTypeRestriction;
        }
    },
    abAssignToCustodianAssets_list_customizeHeader: function (panel, parentElement) {
        var columnIndex = 0;
        for ( var i = 0, col; col = panel.columns[i]; i++ ) {
            if ('bl.asset_type' === col.id && !col.hidden) {
                var headerRows = parentElement.getElementsByTagName("tr");
                // customize filter row if this exists
                var rowElement = document.getElementById(panel.id + '_filterRow');
                if (headerRows.length > 1 && valueExists(rowElement)) {
                    var filterInputId = panel.getFilterInputId(col.id);
                    this.updateMiniConsole(panel, rowElement, columnIndex, filterInputId);
                }
            }
            if (!col.hidden) {
                columnIndex++;
            }
        }
    },
    /**
     * Customize filter row asset type.
     *
     * @param panel grid panel
     * @param rowElement filter row element
     * @param columnIndex column index
     * @param filterInputId filter input id
     */
    updateMiniConsole: function (panel, rowElement, columnIndex, filterInputId) {
        var assetTypes = {
            'property': getMessage('asset_type_property'),
            'bl': getMessage('asset_type_bl'),
            'eq': getMessage('asset_type_eq'),
            'ta': getMessage('asset_type_ta')
        };
        var headerCells = rowElement.getElementsByTagName("th");
        var headerCell = headerCells[columnIndex];
        // remove input element if exists
        var inputElements = headerCell.getElementsByTagName("input");
        if (inputElements[filterInputId]) {
            headerCell.removeChild(inputElements[filterInputId]);
        }
        // add select element
        var i = 0;
        var input = document.createElement("select");
        input.options[i++] = new Option("", "", true);
        for ( var storedValue in assetTypes ) {
            input.options[i++] = new Option(assetTypes[storedValue], storedValue);
        }
        input.className = "inputField_box";
        // run filter when user click on one enum value
        Ext.EventManager.addListener(input, "change", panel.onFilter.createDelegate(panel));
        input.id = filterInputId;
        if (headerCell.childNodes.length > 0) {
            headerCell.insertBefore(input, headerCell.childNodes[0]);
        } else {
            headerCell.appendChild(input);
        }
    },
    /**
     * Customize fields for asset registry panel
     */
    customizeAssetRegistryFields: function (row, column, cellElement) {
        var fieldValue = row.row.getFieldValue(column.id);
        if (column.id === 'bl.asset_type') {
            // doesn't have control type link
            cellElement.childNodes[0].innerHTML = getMessage('asset_type_' + fieldValue);
        } else if (column.id === 'bl.asset_id') {
            cellElement.style.fontWeight = 'bold';
        }
    },
    /**
     * Customize fields for custodian panels
     */
    customizeCustodianFields: function (row, column, cellElement) {
        var fieldValue = row.row.getFieldValue(column.id);
        if ('team.asset_type' === column.id) {
            // doesn't have control type link
            cellElement.childNodes[0].innerHTML = getMessage('asset_type_' + fieldValue);
        } else if ('team.status' === column.id) {
            var filterEl = Ext.get(row.grid.getFilterInputId(column.id));
            var removeOptions = function (el, values) {
                for ( var x in values ) {
                    for ( var i = el.dom.length - 1; i >= 0; i-- ) {
                        if (x === el.dom[i].value) {
                            el.dom.remove(i);
                            break;
                        }
                    }
                }
            };
            removeOptions(filterEl, {'Archived': 'Archived', 'Removed': 'Removed'});
        }
    },
    /**
     * on click asset registry row
     */
    onClickAsset: function (gridRow) {
        var record = gridRow.getRecord();
        var assetType = record.getValue("bl.asset_type"),
            assetId = record.getValue("bl.asset_id"),
            assetStd = record.getValue("bl.asset_std");
        this.selectedAssetRecord = record;
        var ownerRestriction = new Ab.view.Restriction();
        var otherRestriction = new Ab.view.Restriction();
        ownerRestriction.addClause('team.team_type', 'Equipment', '=');
        ownerRestriction.addClause('team.custodian_type', null, 'IS NOT NULL');
        ownerRestriction.addClause('team.is_owner', 1, '=');
        otherRestriction.addClause('team.team_type', 'Equipment', '=');
        otherRestriction.addClause('team.custodian_type', null, 'IS NOT NULL');
        otherRestriction.addClause('team.is_owner', 0, '=');
        if ('bl' === assetType) {
            ownerRestriction.addClause('team.bl_id', assetId, '=');
            otherRestriction.addClause('team.bl_id', assetId, '=');
        } else if ('property' === assetType) {
            ownerRestriction.addClause('team.pr_id', assetId, '=');
            otherRestriction.addClause('team.pr_id', assetId, '=');
        } else if ('eq' === assetType) {
            ownerRestriction.addClause('team.eq_id', assetId, '=');
            otherRestriction.addClause('team.eq_id', assetId, '=');
        } else if ('ta' === assetType) {
            ownerRestriction.addClause('team.ta_id', assetId, '=');
            otherRestriction.addClause('team.ta_id', assetId, '=');
        }
        // add custodian status restriction if is selected
        if (valueExistsNotEmpty(this.filterSelection['custodian_status'])) {
            var status = 'active' === this.filterSelection['custodian_status'] ? 'Active' : 'Inactive';
            ownerRestriction.addClause('team.status', status, '=');
            otherRestriction.addClause('team.status', status, '=');
        }
        var assetTitle = "{0} - {1}".replace('{0}', assetStd).replace('{1}', assetId);
        this.abAssignToCustodian_ownerList.refresh(ownerRestriction);
        this.abAssignToCustodian_ownerList.setTitle(getMessage('titleOwnerCustodians').replace('{0}', assetTitle));
        // refresh other custodian
        this.abAssignToCustodian_otherList.refresh(otherRestriction);
        this.abAssignToCustodian_otherList.setTitle(getMessage('titleOtherCustodians').replace('{0}', assetTitle));
    },
    /**
     * on click asset profile
     */
    onProfileRow: function (row) {
        var record = row.row.getRecord();
        var assetId = record.getValue('bl.asset_id');
        var assetType = record.getValue('bl.asset_type');
        var viewName = null;
        var restriction = new Ab.view.Restriction();
        if ('bl' === assetType) {
            viewName = 'ab-profile-building.axvw';
            restriction.addClause('bl.bl_id', assetId, '=');
        } else if ('property' === assetType) {
            viewName = 'ab-profile-property.axvw';
            restriction.addClause('property.pr_id', assetId, '=');
        } else if ('eq' === assetType) {
            viewName = 'ab-profile-equipment.axvw';
            restriction.addClause('eq.eq_id', assetId, '=');
        } else if ('ta' === assetType) {
            viewName = 'ab-profile-ta.axvw';
            restriction.addClause('ta.ta_id', assetId, '=');
        }
        View.openDialog(viewName, restriction, false, {
            width: 1050,
            height: 650,
            closeButton: true
        })
    }
});
/**
 * Show custodians for selected asset
 *
 */
function showAssetCustodian(context) {
    var parentPanel = View.panels.get(context.command.parentPanelId);
    var controller = View.controllers.get('abAssignToCustodianController');
    if (parentPanel) {
        var selectedRow = parentPanel.gridRows.get(parentPanel.selectedRowIndex);
        controller.onClickAsset(selectedRow);
    }
}
/**
 * On assign to owner custodian.
 *
 */
function onAssignToOwner() {
    var selectedRecords = View.panels.get('abAssignToCustodianAssets_list').getSelectedRecords();
    if (selectedRecords.length > 0) {
        View.controllers.get('abAssignToCustodianController').assignToOwner();
    } else {
        View.showMessage(getMessage('errNoAssetsSelected'));
        return false;
    }
}
/**
 * On assign to other custodian.
 *
 */
function onAssignToOther() {
    var selectedRecords = View.panels.get('abAssignToCustodianAssets_list').getSelectedRecords();
    if (selectedRecords.length > 0) {
        View.controllers.get('abAssignToCustodianController').assignToOther();
    } else {
        View.showMessage(getMessage('errNoAssetsSelected'));
        return false;
    }
}
/**
 * Edit custodian WFR - used for owner and other
 * @param context
 * @returns
 */
function editCustodian(context) {
    var selectedRecord = View.controllers.get('abAssignToCustodianController').selectedAssetRecord;
    var command = context.command;
    var parentPanel = View.panels.get(command.parentPanelId);
    var isOwner = 'abAssignToCustodian_ownerList' === command.parentPanelId;
    var restriction = command.restriction;
    var assetType = selectedRecord.getValue("bl.asset_type");
    var assetId = selectedRecord.getValue("bl.asset_id");
    View.openDialog('ab-eam-edit-custodian.axvw', restriction, false, {
        assetType: assetType,
        assetCode: assetId,
        isOwner: isOwner,
        callback: function () {
            parentPanel.refresh(parentPanel.restriction);
            View.closeThisDialog();
        }
    });
}