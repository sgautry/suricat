/**
 * Define custodians.
 */
var abDefineCustodiansController = View.createController('abDefineCustodiansController', {
    // selected custodian record
    selectedCustodianRecord: null,
    // filter restriction for custodian list
    custodianRestriction: null,
    //filter restriction for assigned assets list
    assignedAssetRestriction: null,
    afterViewLoad: function () {
        var controller = this;
        this.abDefineCustodiansList_panel.afterCreateCellContent = function (row, column, cellElement) {
            controller.customizeCustodianListFields(row, column, cellElement);
        };
        this.abDefineCustodiansList_panel.addEventListener('afterBuildHeader', this.abDefineCustodiansList_customizeHeader, this);
        this.abDefineCustodiansAssigned_panel.afterCreateCellContent = function (row, column, cellElement) {
            controller.customizeAssignedListFields(row, column, cellElement);
        };
        this.abDefineCustodiansList_panel.addParameter('source_vn', getMessage('labelSource_vn'));
        this.abDefineCustodiansList_panel.addParameter('source_contact', getMessage('labelSource_contact'));
        this.abDefineCustodiansList_panel.addParameter('source_em', getMessage('labelSource_em'));
        this.abDefineCustodiansList_panel.addParameter('labelAssetAssigned_yes', getMessage('labelAssetAssigned_yes'));
        this.abDefineCustodiansList_panel.addParameter('labelAssetAssigned_no', getMessage('labelAssetAssigned_no'));
        this.abDefineCustodiansAssigned_panel.addParameter('assetType_bl', getMessage('labelAssetType_bl'));
        this.abDefineCustodiansAssigned_panel.addParameter('assetType_property', getMessage('labelAssetType_property'));
        this.abDefineCustodiansAssigned_panel.addParameter('assetType_eq', getMessage('labelAssetType_eq'));
        this.abDefineCustodiansAssigned_panel.addParameter('assetType_ta', getMessage('labelAssetType_ta'));
        this.abDefineCustodiansFilter_panel.fields.get('team.status').removeOptions({'Archived': 'Archived', 'Removed': 'Removed'});
    },
    afterInitialDataFetch: function () {
        if (valueExists(View.parameters)) {
            if (valueExistsNotEmpty(View.parameters.viewName)) {
                View.setTitle(getMessage(View.parameters.viewName));
            }
            if (valueExists(View.parameters.isReadOnly) && View.parameters.isReadOnly) {
                this.abDefineCustodiansFilter_panel.actions.get('abDefineCustodiansFilter_panel_add').show(false);
                this.abDefineCustodiansAssigned_panel.actions.get('abDefineCustodiansAssigned_panel_add').show(false);
            }
            if (valueExistsNotEmpty(View.parameters.assetType)) {
                this.abDefineCustodiansFilter_panel.setFieldValue('asset_type', View.parameters.assetType);
            }
        }
        this.abDefineCustodiansFilter_panel_onFilter();
    },
    abDefineCustodiansFilter_panel_afterRefresh: function () {
        var assetType = this.abDefineCustodiansFilter_panel.getFieldValue('asset_type');
        if ('eq' === assetType || 'ta' === assetType) {
            this.abDefineCustodiansFilter_panel.fields.get('asset_standard').actions.get('selectValue_assetStandard').show(true);
            this.abDefineCustodiansFilter_panel.enableField('asset_standard', true);
        } else {
            this.abDefineCustodiansFilter_panel.setFieldValue('asset_standard', '');
            this.abDefineCustodiansFilter_panel.fields.get('asset_standard').actions.get('selectValue_assetStandard').show(false);
            this.abDefineCustodiansFilter_panel.enableField('asset_standard', false);
        }
    },
    /**
     * On apply filter
     * called from onChange event from all console fields
     */
    onChangeFilter: function (field) {
        var filterPanel = View.panels.get('abDefineCustodiansFilter_panel');
        var fieldName = field.id.replace('abDefineCustodiansFilter_panel_', '');
        var selectedValue = field.value;
        if ('asset_type' === fieldName) {
            if ('eq' === selectedValue || 'ta' === selectedValue) {
                filterPanel.fields.get('asset_standard').actions.get('selectValue_assetStandard').show(true);
                filterPanel.enableField('asset_standard', true);
            } else {
                filterPanel.enableField('asset_standard', false);
                filterPanel.fields.get('asset_standard').actions.get('selectValue_assetStandard').show(false);
            }
            filterPanel.setFieldValue('asset_standard', '');
        }
    },
    /**
     * Apply filter handler.
     */
    abDefineCustodiansFilter_panel_onFilter: function (panel, action) {
        var assetFieldByType = {
            'bl': 'bl_id',
            'property': 'pr_id',
            'eq': 'eq_id',
            'ta': 'ta_id'
        };
        var assignedAssetRestriction = "";
        var custodiansRestriction = "";
        var assetType = this.abDefineCustodiansFilter_panel.getFieldValue('asset_type');
        if (valueExistsNotEmpty(assetType)) {
            var fieldName = assetFieldByType[assetType];
            assignedAssetRestriction = "team.{0} IS NOT NULL".replace('{0}', fieldName);
            custodiansRestriction = "team.{0} IS NOT NULL".replace('{0}', fieldName);
        }
        var custodianStatus = this.abDefineCustodiansFilter_panel.getFieldValue('team.status');
        if (valueExistsNotEmpty(custodianStatus)) {
            assignedAssetRestriction += (assignedAssetRestriction.length > 0 ? " AND " : "") + "team.status = '{0}'".replace('{0}', custodianStatus);
            custodiansRestriction += (custodiansRestriction.length > 0 ? " AND " : "") + "team.status = '{0}'".replace('{0}', custodianStatus);
        }
        var custodianType = this.abDefineCustodiansFilter_panel.getFieldValue('team.custodian_type');
        if (valueExistsNotEmpty(custodianType)) {
            assignedAssetRestriction += (assignedAssetRestriction.length > 0 ? " AND " : "") + "team.custodian_type = '{0}'".replace('{0}', custodianType);
            custodiansRestriction += (custodiansRestriction.length > 0 ? " AND " : "") + "team.custodian_type = '{0}'".replace('{0}', custodianType);
        }
        var eqRestriction = "EXISTS(SELECT eq.eq_id FROM eq WHERE eq.eq_id = team.eq_id ";
        var taRestriction = "EXISTS(SELECT ta.ta_id FROM ta WHERE ta.ta_id = team.ta_id ";
        var contactRestriction = "EXISTS(SELECT contact.contact_id FROM contact WHERE contact.contact_id = team.contact_id ";
        var emRestriction = "EXISTS(SELECT em.em_id FROM em WHERE em.em_id = team.em_id ";
        var assetStandard = this.abDefineCustodiansFilter_panel.getFieldValue('asset_standard');
        var isEqRestriction = false;
        var isTaRestriction = false;
        var isContactRestriction = false;
        var isEmRestriction = false;
        if (valueExistsNotEmpty(assetStandard)
            && valueExistsNotEmpty(assetType)) {
            if ('eq' === assetType) {
                eqRestriction += " AND eq.eq_std = '{0}'".replace('{0}', assetStandard);
                isEqRestriction = true;
            } else if ('ta' === assetType) {
                taRestriction += " AND ta.fn_std = '{0}'".replace('{0}', assetStandard);
                isTaRestriction = true;
            }
        }
        var teamBlRestriction = "";
        if (valueExistsNotEmpty(this.abDefineCustodiansFilter_panel.getFieldValue('fl.bl_id'))) {
            var buildingIds = this.abDefineCustodiansFilter_panel.getFieldMultipleValues('fl.bl_id');
            eqRestriction += " AND eq.bl_id IN ('" + buildingIds.join("','") + "')";
            taRestriction += " AND ta.bl_id IN ('" + buildingIds.join("','") + "')";
            teamBlRestriction = " team.bl_id IN ('" + buildingIds.join("','") + "') ";
            contactRestriction += " AND contact.bl_id IN ('" + buildingIds.join("','") + "')";
            emRestriction += " AND em.bl_id IN ('" + buildingIds.join("','") + "')";
            isEqRestriction = true;
            isTaRestriction = true;
            isContactRestriction = true;
            isEmRestriction = true;
        }
        if (valueExistsNotEmpty(this.abDefineCustodiansFilter_panel.getFieldValue('fl.fl_id'))) {
            var floorIds = this.abDefineCustodiansFilter_panel.getFieldMultipleValues('fl.fl_id');
            eqRestriction += " AND eq.fl_id IN ('" + floorIds.join("','") + "')";
            taRestriction += " AND ta.fl_id IN ('" + floorIds.join("','") + "')";
            emRestriction += " AND em.fl_id IN ('" + floorIds.join("','") + "')";
            isEqRestriction = true;
            isTaRestriction = true;
            isEmRestriction = true;
        }
        if (valueExistsNotEmpty(this.abDefineCustodiansFilter_panel.getFieldValue('dp.dv_id'))) {
            var divisionIds = this.abDefineCustodiansFilter_panel.getFieldMultipleValues('dp.dv_id');
            eqRestriction += " AND eq.dv_id IN ('" + divisionIds.join("','") + "')";
            taRestriction += " AND ta.dv_id IN ('" + divisionIds.join("','") + "')";
            emRestriction += " AND em.dv_id IN ('" + divisionIds.join("','") + "')";
            isEqRestriction = true;
            isTaRestriction = true;
            isEmRestriction = true;
        }
        if (valueExistsNotEmpty(this.abDefineCustodiansFilter_panel.getFieldValue('dp.dp_id'))) {
            var departmentIds = this.abDefineCustodiansFilter_panel.getFieldMultipleValues('dp.dp_id');
            eqRestriction += " AND eq.dp_id IN ('" + departmentIds.join("','") + "')";
            taRestriction += " AND ta.dp_id IN ('" + departmentIds.join("','") + "')";
            emRestriction += " AND em.dp_id IN ('" + departmentIds.join("','") + "')";
            isEqRestriction = true;
            isTaRestriction = true;
            isEmRestriction = true;
        }
        eqRestriction += ")";
        taRestriction += ")";
        contactRestriction += ")";
        emRestriction += ")";
        if (!isEqRestriction) {
            eqRestriction = "";
        }
        if (!isTaRestriction) {
            taRestriction = "";
        }
        if (!isEmRestriction) {
            emRestriction = "";
        }
        if (!isContactRestriction) {
            contactRestriction = "";
        }
        var slqOrClauses = teamBlRestriction;
        if (isEqRestriction) {
            slqOrClauses += (slqOrClauses.length > 0 ? " OR " : "") + eqRestriction;
        }
        if (isTaRestriction) {
            slqOrClauses += (slqOrClauses.length > 0 ? " OR " : "") + taRestriction;
        }
        if (isEmRestriction) {
            slqOrClauses += (slqOrClauses.length > 0 ? " OR " : "") + emRestriction;
        }
        if (isContactRestriction) {
            slqOrClauses += (slqOrClauses.length > 0 ? " OR " : "") + contactRestriction;
        }
        if (slqOrClauses.length > 0) {
            slqOrClauses = "(" + slqOrClauses + ")";
            assignedAssetRestriction += (assignedAssetRestriction.length > 0 ? " AND " : "") + slqOrClauses;
            custodiansRestriction += (custodiansRestriction.length > 0 ? " AND " : "") + slqOrClauses;
        }
        if (custodiansRestriction.length === 0) {
            custodiansRestriction = " 1=1 ";
        }
        this.custodianRestriction = custodiansRestriction;
        this.assignedAssetRestriction = assignedAssetRestriction;
        this.abDefineCustodiansList_panel.addParameter('sqlRestriction', this.custodianRestriction);
        var custodianAssigned = this.abDefineCustodiansFilter_panel.getFieldValue('custodian_assigned');
        var isAssigned = "";
        if ('assigned' === custodianAssigned) {
            isAssigned = "team.is_assigned = 'yes'";
        } else if ('unassigned' === custodianAssigned) {
            isAssigned = "team.is_assigned = 'no'";
        }
        this.abDefineCustodiansList_panel.addParameter('isCustodianAssignedRestriction', isAssigned);
        this.abDefineCustodiansList_panel.refresh();
        this.abDefineCustodiansAssigned_panel.show(false, true);
    },
    abDefineCustodiansFilter_panel_onClear: function (panel, action) {
        this.abDefineCustodiansFilter_panel.clear();
    },
    /**
     * Remove custodian if is not assigned to any asset.
     * @param row Panel grid row.
     */
    abDefineCustodiansList_panel_onDeleteCustodian: function (row) {
        var record = row.getRecord();
        var custodianCode = record.getValue('team.custodian_code'),
            custodianType = record.getValue('team.custodian_type'),
            sourceTable = record.getValue('team.source_table');
        var custodianFieldBySource = {
            'vn': 'vn_id',
            'contact': 'contact_id',
            'em': 'em_id'
        };
        var fieldName = custodianFieldBySource[sourceTable];
        var sqlRestriction = "team.team_type = 'Equipment' AND team.custodian_type='" + custodianType + "'AND team." + fieldName + " = '" + custodianCode + "'"
            + " AND team.eq_id IS NULL AND team.ta_id IS NULL AND team.bl_id IS NULL AND team.pr_id IS NULL";
        var dataSource = View.dataSources.get('abDefineCustodiansAssigned_ds');
        var records = dataSource.getRecords(sqlRestriction);
        for ( var i = 0; i < records.length; i++ ) {
            dataSource.deleteRecord(records[i]);
        }
        this.refreshView();
    },
    /**
     * Customize grid cells
     */
    customizeCustodianListFields: function (row, column, cellElement) {
        if ('team.custodian_name' === column.id) {
            cellElement.childNodes[0].style.fontWeight = 'bold';
        } else if ('deleteCustodian' === column.id) {
            var isAssigned = row.row.getFieldValue('team.is_assigned');
            if ('yes' === isAssigned) {
                cellElement.innerHTML = '';
            }
        }
    },
    abDefineCustodiansList_customizeHeader: function (panel, parentElement) {
        var columnIndex = 0;
        for ( var i = 0, col; col = panel.columns[i]; i++ ) {
            if ('team.is_assigned_label' === col.id && !col.hidden) {
                var headerRows = parentElement.getElementsByTagName("tr");
                // customize filter row if this exists
                var rowElement = document.getElementById(panel.id + '_filterRow');
                if (headerRows.length > 1 && valueExists(rowElement)) {
                    var filterInputId = panel.getFilterInputId(col.id);
                    this.updateMiniConsole(panel, rowElement, columnIndex, filterInputId);
                }
            }
            if (!col.hidden) {
                columnIndex++;
            }
        }
    },
    /**
     * Customize filter row has asset assigned.
     *
     * @param panel grid panel
     * @param rowElement filter row element
     * @param columnIndex column index
     * @param filterInputId filter input id
     */
    updateMiniConsole: function (panel, rowElement, columnIndex, filterInputId) {
        var hasAssetAssigned = {
            'yes': getMessage('labelAssetAssigned_yes'),
            'no': getMessage('labelAssetAssigned_no')
        };
        var headerCells = rowElement.getElementsByTagName("th");
        var headerCell = headerCells[columnIndex];
        // remove input element if exists
        var inputElements = headerCell.getElementsByTagName("input");
        if (inputElements[filterInputId]) {
            headerCell.removeChild(inputElements[filterInputId]);
        }
        // add select element
        var i = 0;
        var input = document.createElement("select");
        input.options[i++] = new Option("", "", true);
        for ( var storedValue in hasAssetAssigned ) {
            input.options[i++] = new Option(hasAssetAssigned[storedValue], storedValue);
        }
        input.className = "inputField_box";
        // run filter when user click on one enum value
        Ext.EventManager.addListener(input, "change", panel.onFilter.createDelegate(panel));
        input.id = filterInputId;
        if (headerCell.childNodes.length > 0) {
            headerCell.insertBefore(input, headerCell.childNodes[0]);
        } else {
            headerCell.appendChild(input);
        }
    },
    customizeAssignedListFields: function (row, column, cellElement) {
        if ('team.vf_asset_id' === column.id) {
            // doesn't have control type link
            cellElement.style.fontWeight = 'bold';
        } else if ('team.status' === column.id) {
            var filterEl = Ext.get(row.grid.getFilterInputId(column.id));
            var removeOptions = function (el, values) {
                for ( var x in values ) {
                    for ( var i = el.dom.length - 1; i >= 0; i-- ) {
                        if (x === el.dom[i].value) {
                            el.dom.remove(i);
                            break;
                        }
                    }
                }
            };
            removeOptions(filterEl, {'Archived': 'Archived', 'Removed': 'Removed'});
        }
    },
    loadCustodianDetails: function (record) {
        this.selectedCustodianRecord = record;
        var custodianCode = record.getValue('team.custodian_code'),
            custodianType = record.getValue('team.custodian_type'),
            sourceTable = record.getValue('team.source_table');
        var custodianFieldBySource = {
            'vn': 'vn_id',
            'contact': 'contact_id',
            'em': 'em_id'
        };
        var fieldName = custodianFieldBySource[sourceTable];
        var sqlRestriction = "team.is_assigned = 'yes' AND team.team_type = 'Equipment' AND team.custodian_type='" + custodianType + "'AND team." + fieldName + " = '" + custodianCode + "'" +
            ( valueExistsNotEmpty(this.assignedAssetRestriction) ? " AND " + this.assignedAssetRestriction : "");
        this.abDefineCustodiansAssigned_panel.addParameter('sqlRestriction', sqlRestriction);
        this.abDefineCustodiansAssigned_panel.refresh();
        var menu = this.abDefineCustodiansAssigned_panel.actions.get('abDefineCustodiansAssigned_panel_add').menu;
        var itemActionOwner = menu.items.get('abDefineCustodiansAssigned_panel_assign_toOwner'),
            itemActionOther = menu.items.get('abDefineCustodiansAssigned_panel_assign_toOther');
        if ('OWNER' === custodianType) {
            itemActionOwner.enable();
            itemActionOther.disable();
        } else {
            itemActionOwner.disable();
            itemActionOther.enable();
        }
    },
    abDefineCustodiansAssigned_panel_afterRefresh: function () {
        if (valueExistsNotEmpty(this.selectedCustodianRecord)) {
            var custodianName = this.selectedCustodianRecord.getValue('team.custodian_name');
            var title = getMessage('titleAssetAssignedTo').replace('{0}', custodianName);
            this.abDefineCustodiansAssigned_panel.setTitle(title);
        }
    },
    onAddCustodians: function (parentPanelId, isOwner) {
        var custodianName = null, custodianCode = null, custodianType = null, sourceTable = null, custodianRecord = null;
        if ('abDefineCustodiansAssigned_panel' === parentPanelId) {
            custodianName = this.selectedCustodianRecord.getValue('team.custodian_name');
            custodianCode = this.selectedCustodianRecord.getValue('team.custodian_code');
            sourceTable = this.selectedCustodianRecord.getValue('team.source_table');
            custodianType = this.selectedCustodianRecord.getValue('team.custodian_type');
            custodianRecord = getCustodianDataFromSourceTable(custodianCode, sourceTable);
        }
        View.openDialog('ab-eam-add-custodian.axvw', null, false, {
            width: 1024,
            height: 800,
            isOwner: isOwner,
            custodianName: custodianName,
            custodianType: custodianType,
            sourceTable: sourceTable,
            custodianRecord: custodianRecord,
            callback: function () {
                View.controllers.get('abDefineCustodiansController').refreshView();
            }
        });
    },
    /**
     * Refresh entire view.
     */
    refreshView: function () {
        this.abDefineCustodiansList_panel.refresh(this.abDefineCustodiansList_panel.restriction);
        if (this.abDefineCustodiansAssigned_panel.visible) {
            this.abDefineCustodiansAssigned_panel.refresh(this.abDefineCustodiansAssigned_panel.restriction);
        }
    },
    onCreateCustodian: function () {
        View.openDialog('ab-eam-create-custodian.axvw', null, true, {
            width: 1024,
            height: 500,
            callback: function (addMultipleCustodians) {
                View.controllers.get('abDefineCustodiansController').refreshView();
                if (!addMultipleCustodians) {
                    View.closeDialog();
                }
            }
        });
    }
});

function showSelectValue_assetStandard() {
    var objFilter = View.panels.get('abDefineCustodiansFilter_panel');
    var assetType = objFilter.getFieldValue('asset_type');
    if ("eq" === assetType) {
        View.selectValue('abDefineCustodiansFilter_panel', getMessage('titleEquipmentStandard'), ['asset_standard'], 'eqstd', ['eqstd.eq_std'], ['eqstd.eq_std', 'eqstd.category', 'eqstd.description']);
    } else if ('ta' === assetType) {
        View.selectValue('abDefineCustodiansFilter_panel', getMessage('titleFurnitureStandard'), ['asset_standard'], 'fnstd', ['fnstd.fn_std'], ['fnstd.fn_std', 'fnstd.catalog_id', 'fnstd.category', 'fnstd.description']);
    }
}

/**
 * Load custodian details.
 * @param context command context
 *
 */
function loadCustodianDetails(context) {
    var controller = View.controllers.get('abDefineCustodiansController');
    var selectedRow = controller.abDefineCustodiansList_panel.gridRows.get(controller.abDefineCustodiansList_panel.selectedRowIndex);
    controller.loadCustodianDetails(selectedRow.getRecord());
}

/**
 * On add owner event handler
 * @param context command context
 *
 */
function onAssignToOwner(context) {
    var parentPanelId = context.command.parentPanelId;
    var controller = View.controllers.get('abDefineCustodiansController');
    controller.onAddCustodians(parentPanelId, true);
}

/**
 * On add custodian event handler
 * @param context command context
 *
 */
function onAssignToOther(context) {
    var parentPanelId = context.command.parentPanelId;
    var controller = View.controllers.get('abDefineCustodiansController');
    controller.onAddCustodians(parentPanelId, false);
}

/**
 * On create custodian event handler
 * @param context command context
 */
function onCreateCustodian(context) {
    var parentPanelId = context.command.parentPanelId;
    var controller = View.controllers.get('abDefineCustodiansController');
    controller.onCreateCustodian(parentPanelId, false);
}