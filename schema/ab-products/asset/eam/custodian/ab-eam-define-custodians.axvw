<view version="2.0">
    <js file="ab-eam-define-custodians.js"/>
    
    <title>Assign Custodians to Assets</title>
    
    <!-- Translatable messages -->
    <message name="labelSource_vn">Vendor</message>
    <message name="labelSource_contact">Contact</message>
    <message name="labelSource_em">Employee</message>

    <message name="labelAssetType_bl">Building</message>
    <message name="labelAssetType_property">Property</message>
    <message name="labelAssetType_eq">Equipment</message>
    <message name="labelAssetType_ta">Furniture</message>

    <message name="labelAssetAssigned_yes">Yes</message>
    <message name="labelAssetAssigned_no">No</message>

    <message name="titleEquipmentStandard">Equipment Standard</message>
    <message name="titleFurnitureStandard">Furniture Standard</message>
    <message name="titleAssetAssignedTo">Assets Assigned to Custodian - {0}</message>

    <message name="assetsByCustodianTitle">List of Assets by Custodian</message>
    
    <message name="addOwnerCustodianAction">Owner Custodian</message>
    <message name="addOtherCustodianAction">Other Custodian</message>

    <layout id="abDefineCustodian_layout">
        <west id="custodiansPos" initialSize="45%" split="true"/>
        <center id="assetPos"/>
    </layout>
    
    <panel type="view" id="abCustodianHelperDatasource" file="ab-eam-custodian-datasources.axvw"/>
    
    
    <dataSource id="abDefineCustodiansFilter_ds">
        <table name="team" role="main"/>
        <table name="fl" role="standard"/>
        <table name="dp" role="standard"/>
        <field table="team" name="status"/>
        <field table="team" name="custodian_type"/>
        <field table="fl" name="fl_id"/>
        <field table="fl" name="bl_id"/>
        <field table="dp" name="dv_id"/>
        <field table="dp" name="dp_id"/>
    </dataSource>

	<panel type="console" id="abDefineCustodiansFilter_panel" dataSource="abDefineCustodiansFilter_ds" columns="1" bodyCssClass="panel-light" layoutRegion="custodiansPos">
		<title>Asset Custodians</title>
		<action id="abDefineCustodiansFilter_panel_add" imageName="/schema/ab-core/graphics/icons/view/add-icon.png">
			<tooltip>Add Custodian</tooltip>
			<command type="callFunction" functionName="onCreateCustodian"/>
		</action>
		<action type="menu" id="abDefineCustodiansFilter_panel_export" imageName="/schema/ab-core/graphics/icons/view/export.png">
			<tooltip>Export</tooltip>
			<action id="abDefineCustodiansFilter_panel_export_xls">
				<title>XLS</title>
				<command type="exportPanel" panelId="abDefineCustodiansList_panel" outputType="xls"/>
			</action>
		</action>
		<fieldset layout="fluid">
			<field name="asset_type" id="asset_type" cssClass="shortField" showLabel="true" controlType="comboBox" onchange="abDefineCustodiansController.onChangeFilter(this);">
				<title>Asset Type</title>
				<option value="" translatable="false"></option>
				<option value="bl" translatable="true">Building</option>
				<option value="eq" translatable="true">Equipment</option>
				<option value="ta" translatable="true">Furniture</option>
				<option value="property" translatable="true">Property</option>
			</field>
			<field name="asset_standard" id="asset_standard" cssClass="shortField" showLabel="false" dataType="text" readOnly="true">
				<title>ASSET STANDARD</title>
				<action id="selectValue_assetStandard">
					<title>...</title>
					<tooltip>Select Value</tooltip>
					<command type="callFunction" functionName="showSelectValue_assetStandard"/>
				</action>
			</field>
			<action id="filter" mainAction="true">
				<title>Show</title>
			</action>
			<action id="clear">
				<title>Clear</title>
			</action>
		</fieldset>
		<fieldset layout="fluid">
			<field table="team" name="status" controlType="comboBox" cssClass="shortField" showLabel="true">
				<title>Custodian Status</title>
			</field>
			<field table="team" name="custodian_type" cssClass="shortField" showLabel="false"/>
		</fieldset>
		<fieldset layout="fluid">
			<field table="fl" name="bl_id" style="width: 90px;" cssClass="shortField" showLabel="false" selectValueType="multiple"/>
			<field table="fl" name="fl_id" style="width: 90px;" cssClass="shortField" showLabel="false" selectValueType="multiple"/>
			<field table="dp" name="dv_id" style="width: 90px;" cssClass="shortField" showLabel="false" selectValueType="multiple"/>
			<field table="dp" name="dp_id" style="width: 90px;" cssClass="shortField" showLabel="false" selectValueType="multiple"/>
		</fieldset>
		<fieldset layout="fluid">
			<field name="custodian_assigned" id="custodian_assigned" cssClass="shortField" showLabel="true" controlType="comboBox" onchange="abDefineCustodiansController.onChangeFilter(this);">
				<title>Show</title>
				<option value="" translatable="true">All Custodians</option>
				<option value="assigned" translatable="true">Assigned Custodians</option>
				<option value="unassigned" translatable="true">Unassigned Custodians</option>
			</field>
		</fieldset>
	</panel>
    
    <dataSource id="abDefineCustodiansList_ds">
        <sql dialect="generic">
			SELECT  
			    (CASE 
			        WHEN team_distinct.source_table = 'vn' THEN team_distinct.vn_id 
			        WHEN team_distinct.source_table = 'contact' THEN team_distinct.contact_id
			        WHEN team_distinct.source_table = 'em' THEN team_distinct.em_id
			      ELSE NULL END) ${sql.as} custodian_code,
                (CASE 
                    WHEN team_distinct.source_table = 'vn' AND vn.company IS NOT NULL THEN vn.company
                    WHEN team_distinct.source_table = 'vn' THEN team_distinct.vn_id
                    WHEN team_distinct.source_table = 'contact' AND (contact.name_first IS NOT NULL OR contact.name_last IS NOT NULL) THEN (contact.name_first ${sql.concat} ' ' ${sql.concat} contact.name_last)
                    WHEN team_distinct.source_table = 'contact' THEN team_distinct.contact_id
                    WHEN team_distinct.source_table = 'em' AND em.name IS NOT NULL THEN em.name
                    WHEN team_distinct.source_table = 'em' AND em.name IS NULL AND (em.name_first IS NOT NULL OR em.name_last IS NOT NULL) THEN (em.name_first ${sql.concat} ' ' ${sql.concat} em.name_last)
                    WHEN team_distinct.source_table = 'em' THEN team_distinct.em_id
                  ELSE NULL END) ${sql.as} custodian_name,
			    team_distinct.source_table ${sql.as} source_table,
			    team_distinct.custodian_type ${sql.as} custodian_type,
			    team_distinct.is_assigned ${sql.as} is_assigned,
			    team_distinct.is_assigned_label ${sql.as} is_assigned_label,
			    (CASE 
                    WHEN team_distinct.source_table = 'vn' THEN ${parameters['source_vn']} 
                    WHEN team_distinct.source_table = 'contact' THEN ${parameters['source_contact']}
                    WHEN team_distinct.source_table = 'em' THEN ${parameters['source_em']}
                  ELSE NULL END) ${sql.as} source,
			    (SELECT MAX(team.email_archive) 
    			        FROM team 
				    WHERE team.team_type = 'Equipment' AND team.custodian_type IS NOT NULL AND
				        team.source_table = team_distinct.source_table AND team.email_archive IS NOT NULL AND
				        ((team.source_table = 'em' AND team.em_id = team_distinct.em_id) 
				            OR (team.source_table = 'vn' AND team.vn_id = team_distinct.vn_id) 
				            OR (team.source_table = 'contact' AND team.contact_id = team_distinct.contact_id))) ${sql.as} email_archive,
			    (SELECT MAX(team.cell_num_archive) 
			            FROM team 
			        WHERE  team.team_type = 'Equipment' AND team.custodian_type IS NOT NULL AND
			             team.source_table = team_distinct.source_table AND team.cell_num_archive IS NOT NULL AND
			            ((team.source_table = 'em' AND team.em_id = team_distinct.em_id) 
			                OR (team.source_table = 'vn' AND team.vn_id = team_distinct.vn_id) 
			                OR (team.source_table = 'contact' AND team.contact_id = team_distinct.contact_id))) ${sql.as} cell_num_archive,
			    (SELECT MAX(team.phone_archive) 
			            FROM team 
			        WHERE team.team_type = 'Equipment' AND team.custodian_type IS NOT NULL AND 
			             team.source_table = team_distinct.source_table AND team.phone_archive IS NOT NULL AND
			            ((team.source_table = 'em' AND team.em_id = team_distinct.em_id) 
			                OR (team.source_table = 'vn' AND team.vn_id = team_distinct.vn_id) 
			                OR (team.source_table = 'contact' AND team.contact_id = team_distinct.contact_id))) ${sql.as} phone_archive
			FROM
				(SELECT DISTINCT
				    team.source_table ${sql.as} source_table,
				    team.custodian_type ${sql.as} custodian_type,
				    team.vn_id ${sql.as} vn_id,
				    team.em_id ${sql.as} em_id,
				    team.contact_id ${sql.as} contact_id,
				    (CASE WHEN (team.eq_id IS NULL AND team.ta_id IS NULL AND team.bl_id IS NULL AND team.pr_id IS NULL) THEN 'no' ELSE 'yes' END) ${sql.as} is_assigned,
				    (CASE WHEN (team.eq_id IS NULL AND team.ta_id IS NULL AND team.bl_id IS NULL AND team.pr_id IS NULL) THEN ${parameters['labelAssetAssigned_no']} ELSE ${parameters['labelAssetAssigned_yes']} END) ${sql.as} is_assigned_label
				FROM team 
				WHERE team.team_type = 'Equipment' 
				AND team.custodian_type IS NOT NULL AND ${parameters['sqlRestriction']}) ${sql.as} team_distinct
                   LEFT OUTER JOIN contact ON team_distinct.contact_id=contact.contact_id
                   LEFT OUTER JOIN vn ON team_distinct.vn_id=vn.vn_id
                   LEFT OUTER JOIN em ON team_distinct.em_id=em.em_id
        </sql>
        <table name="team"/>
        <field name="custodian_name" dataType="text">
            <title>Name</title>
        </field>
        <field name="custodian_code" dataType="text">
            <title>Custodian Code</title>
        </field>
        <field name="custodian_type" dataType="text">
            <title>Custodian Type</title>
        </field>
        <field name="source" dataType="text">
            <title>Source</title>
        </field>
        <field name="source_table" dataType="text">
            <title>Source Table</title>
        </field>
        <field name="email_archive" dataType="text">
            <title>Email Address</title>
        </field>
        <field name="cell_num_archive" dataType="text">
            <title>Cellular Number</title>
        </field>
        <field name="phone_archive" dataType="text">
            <title>Phone Number</title>
        </field>
        <field name="is_assigned_label" dataType="text">
            <title>Has Asset Assigned?</title>
        </field>
        <field name="is_assigned" dataType="text" hidden="true"/>
        <parameter name="sqlRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="isCustodianAssignedRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="source_vn" dataType="text" value="Vendor"/>
        <parameter name="source_contact" dataType="text" value="Contact"/>
        <parameter name="source_em" dataType="text" value="Employee"/>
        <parameter name="labelAssetAssigned_yes" dataType="text" value="Yes"/>
        <parameter name="labelAssetAssigned_no" dataType="text" value="No"/>
        <restriction type="sql" sql="${parameters['isCustodianAssignedRestriction']}"/>
    </dataSource>
    
    <panel type="grid" id="abDefineCustodiansList_panel" dataSource="abDefineCustodiansList_ds" selectionEnabled="true" showOnLoad="false" layoutRegion="custodiansPos">
        <sortField table="team" name="custodian_name" ascending="true"/>
        <event type="onClickItem">
            <command type="callFunction" functionName="loadCustodianDetails"/>
        </event>
        <field name="custodian_name" dataType="text">
            <title>Name</title>
        </field>
        <field name="custodian_type" dataType="text">
            <title>Custodian Type</title>
        </field>
        <field name="source" dataType="text">
            <title>Source</title>
        </field>
        <field name="email_archive" dataType="text">
            <title>Email Address</title>
        </field>
        <field name="cell_num_archive" dataType="text">
            <title>Cellular Number</title>
        </field>
        <field name="phone_archive" dataType="text">
            <title>Phone Number</title>
        </field>
        <field name="is_assigned_label" dataType="text">
            <title>Has Asset Assigned?</title>
        </field>
        <field name="is_assigned" dataType="text" hidden="true"/>
        <field id="deleteCustodian" controlType="image" imageName="delete.gif">
            <tooltip>Delete Custodian</tooltip>
        </field>
        <field name="custodian_code" dataType="text" hidden="true">
            <title>Custodian Code</title>
        </field>
    </panel>
        
    <dataSource id="abDefineCustodiansAssigned_ds">
        <sql dialect="generic">
            SELECT 
                (CASE 
                    WHEN team.bl_id IS NOT NULL THEN team.bl_id
                    WHEN team.pr_id IS NOT NULL THEN team.pr_id
                    WHEN team.eq_id IS NOT NULL THEN team.eq_id
                    WHEN team.ta_id IS NOT NULL THEN team.ta_id
                ELSE NULL END) ${sql.as} vf_asset_id,
                (CASE 
                    WHEN team.bl_id IS NOT NULL THEN ${parameters['assetType_bl']}
                    WHEN team.pr_id IS NOT NULL THEN ${parameters['assetType_property']}
                    WHEN team.eq_id IS NOT NULL THEN ${parameters['assetType_eq']}
                    WHEN team.ta_id IS NOT NULL THEN ${parameters['assetType_ta']}
                ELSE NULL END) ${sql.as} vf_asset_type,
                (CASE 
                    WHEN team.bl_id IS NOT NULL THEN NULL
                    WHEN team.pr_id IS NOT NULL THEN NULL
                    WHEN team.eq_id IS NOT NULL THEN (SELECT eq.eq_std FROM eq WHERE eq.eq_id = team.eq_id)
                    WHEN team.ta_id IS NOT NULL THEN (SELECT ta.fn_std FROM ta WHERE ta.ta_id = team.ta_id)
                ELSE NULL END) ${sql.as} vf_asset_standard,                
                (CASE 
                    WHEN team.source_table = 'vn' AND vn.company IS NOT NULL THEN vn.company
                    WHEN team.source_table = 'vn' THEN team.vn_id
                    WHEN team.source_table = 'contact' AND (contact.name_first IS NOT NULL OR contact.name_last IS NOT NULL) THEN (contact.name_first ${sql.concat} ' ' ${sql.concat} contact.name_last)
                    WHEN team.source_table = 'contact' THEN team.contact_id
                    WHEN team.source_table = 'em' AND em.name IS NOT NULL THEN em.name
                    WHEN team.source_table = 'em' AND em.name IS NULL AND (em.name_first IS NOT NULL OR em.name_last IS NOT NULL) THEN (em.name_first ${sql.concat} ' ' ${sql.concat} em.name_last)
                    WHEN team.source_table = 'em' THEN team.em_id
                ELSE NULL END) ${sql.as} vf_custodian_name,
                (CASE 
                    WHEN team.bl_id IS NOT NULL THEN team.bl_id
                    WHEN team.pr_id IS NOT NULL THEN NULL
                    WHEN team.eq_id IS NOT NULL THEN (SELECT eq.bl_id FROM eq WHERE eq.eq_id = team.eq_id)
                    WHEN team.ta_id IS NOT NULL THEN (SELECT ta.bl_id FROM ta WHERE ta.ta_id = team.ta_id)
                ELSE NULL END) ${sql.as} vf_bl_id,
                (CASE 
                    WHEN team.bl_id IS NOT NULL THEN NULL
                    WHEN team.pr_id IS NOT NULL THEN NULL
                    WHEN team.eq_id IS NOT NULL THEN (SELECT eq.fl_id FROM eq WHERE eq.eq_id = team.eq_id)
                    WHEN team.ta_id IS NOT NULL THEN (SELECT ta.fl_id FROM ta WHERE ta.ta_id = team.ta_id)
                ELSE NULL END) ${sql.as} vf_fl_id,
                (CASE 
                    WHEN team.bl_id IS NOT NULL THEN NULL
                    WHEN team.pr_id IS NOT NULL THEN NULL
                    WHEN team.eq_id IS NOT NULL THEN (SELECT eq.rm_id FROM eq WHERE eq.eq_id = team.eq_id)
                    WHEN team.ta_id IS NOT NULL THEN (SELECT ta.rm_id FROM ta WHERE ta.ta_id = team.ta_id)
                ELSE NULL END) ${sql.as} vf_rm_id,
                (CASE 
                    WHEN team.bl_id IS NOT NULL THEN NULL
                    WHEN team.pr_id IS NOT NULL THEN NULL
                    WHEN team.eq_id IS NOT NULL THEN (SELECT eq.dv_id FROM eq WHERE eq.eq_id = team.eq_id)
                    WHEN team.ta_id IS NOT NULL THEN (SELECT ta.dv_id FROM ta WHERE ta.ta_id = team.ta_id)
                ELSE NULL END) ${sql.as} vf_dv_id,
                (CASE 
                    WHEN team.bl_id IS NOT NULL THEN NULL
                    WHEN team.pr_id IS NOT NULL THEN NULL
                    WHEN team.eq_id IS NOT NULL THEN (SELECT eq.dp_id FROM eq WHERE eq.eq_id = team.eq_id)
                    WHEN team.ta_id IS NOT NULL THEN (SELECT ta.dp_id FROM ta WHERE ta.ta_id = team.ta_id)
                ELSE NULL END) ${sql.as} vf_dp_id,
                (CASE 
                    WHEN team.bl_id IS NOT NULL THEN NULL
                    WHEN team.pr_id IS NOT NULL THEN NULL
                    WHEN team.eq_id IS NOT NULL THEN (SELECT eq.em_id FROM eq WHERE eq.eq_id = team.eq_id)
                    WHEN team.ta_id IS NOT NULL THEN (SELECT ta.em_id FROM ta WHERE ta.ta_id = team.ta_id)
                ELSE NULL END) ${sql.as} vf_em_id,
                (CASE
                    WHEN (team.eq_id IS NULL AND team.ta_id IS NULL AND team.bl_id IS NULL AND team.pr_id IS NULL) THEN 'no'
                ELSE 'yes' END) ${sql.as} is_assigned,
                team.autonumbered_id ${sql.as} autonumbered_id,
                team.custodian_type ${sql.as} custodian_type,
                team.status ${sql.as} status,
                team.date_start ${sql.as} date_start,
                team.date_end ${sql.as} date_end,
                team.bl_id ${sql.as} bl_id,
                team.pr_id ${sql.as} pr_id,
                team.eq_id ${sql.as} eq_id,
                team.ta_id ${sql.as} ta_id,
                team.vn_id ${sql.as} vn_id,
                team.contact_id ${sql.as} contact_id,
                team.em_id ${sql.as} em_id,
                team.source_table ${sql.as} source_table,
                team.team_type ${sql.as} team_type
            FROM team
              LEFT OUTER JOIN contact ON team.contact_id=contact.contact_id
              LEFT OUTER JOIN vn ON team.vn_id=vn.vn_id
              LEFT OUTER JOIN em ON team.em_id=em.em_id
        </sql>
        <table name="team"/>
        <field name="autonumbered_id"/>
        <field name="vf_asset_id" dataType="text">
            <title>Asset Code</title>
        </field>
        <field name="vf_asset_type" dataType="text">
            <title>Asset Type</title>
        </field>
        <field name="vf_asset_standard" dataType="text">
            <title>Asset Standard</title>
        </field>
        <field name="vf_custodian_name" dataType="text">
            <title>Custodian Name</title>
        </field>
        <field name="custodian_type"/>
        <field name="status">
            <title>Custodian Status</title>
        </field>
        <field name="date_start">
            <title>Start Date</title>
        </field>
        <field name="date_end">
            <title>End Date</title>
        </field>
        <field name="vf_bl_id" dataType="text">
            <title>Building Code</title>
        </field>
        <field name="vf_fl_id" dataType="text">
            <title>Floor Code</title>
        </field>
        <field name="vf_rm_id" dataType="text">
            <title>Room Code</title>
        </field>
        <field name="vf_dv_id" dataType="text">
            <title>Division Code</title>
        </field>
        <field name="vf_dp_id" dataType="text">
            <title>Department Code</title>
        </field>
        <field name="vf_em_id" dataType="text">
            <title>Employee Code</title>
        </field>
        <field name="is_assigned" dataType="text">
            <title>Has Asset Assigned?</title>
        </field>
        
        <field name="bl_id"/>
        <field name="pr_id"/>
        <field name="eq_id"/>
        <field name="ta_id"/>
        <field name="vn_id"/>
        <field name="contact_id"/>
        <field name="em_id"/>
        
        <restriction type="sql" sql="${parameters['sqlRestriction']}"/>
        <parameter name="sqlRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="assetType_bl" dataType="text" value="Building"/>
        <parameter name="assetType_property" dataType="text" value="Property"/>
        <parameter name="assetType_eq" dataType="text" value="Equipment"/>
        <parameter name="assetType_ta" dataType="text" value="Furniture"/>
    </dataSource>
        
    <panel type="grid" id="abDefineCustodiansAssigned_panel" dataSource="abDefineCustodiansAssigned_ds" showOnLoad="false" layoutRegion="assetPos">
        <sortField table="team" name="status" ascending="true"/>
        <sortField table="team" name="vf_asset_type" ascending="true"/>
        <sortField table="team" name="vf_asset_id" ascending="true"/>
        <indexField table="team" name="vf_asset_id"/>
        
        <action type="menu" id="abDefineCustodiansAssigned_panel_add" imageName="/schema/ab-core/graphics/icons/view/add-icon.png">
            <tooltip>Add</tooltip>
            <action id="abDefineCustodiansAssigned_panel_assign_toOwner">
                <title>Owner Custodian</title>
                <command type="callFunction" functionName="onAssignToOwner"/>
            </action>
            <action id="abDefineCustodiansAssigned_panel_assign_toOther">
                <title>Other Custodian</title>
                <command type="callFunction" functionName="onAssignToOther"/>
            </action>
        </action>
        <action type="menu" id="abDefineCustodiansAssigned_panel_export" imageName="/schema/ab-core/graphics/icons/view/export.png">
            <tooltip>Export</tooltip>
            <action id="abDefineCustodiansAssigned_panel_xls">
                <title>XLS</title>
                <command type="exportPanel" panelId="abDefineCustodiansAssigned_panel" outputType="xls"/>
            </action>
        </action>
        <field name="vf_asset_id" dataType="text">
            <title>Asset Code</title>
        </field>
        <field name="vf_asset_type" dataType="text">
            <title>Asset Type</title>
        </field>
        <field name="vf_asset_standard" dataType="text">
            <title>Asset Standard</title>
        </field>
        <field name="vf_custodian_name" dataType="text">
            <title>Custodian Name</title>
        </field>
        <field name="custodian_type"/>
        <field name="status">
            <title>Custodian Status</title>
        </field>
        <field name="date_start">
            <title>Start Date</title>
        </field>
        <field name="date_end">
            <title>End Date</title>
        </field>
        <field name="vf_bl_id" dataType="text">
            <title>Building Code</title>
        </field>
        <field name="vf_fl_id" dataType="text">
            <title>Floor Code</title>
        </field>
        <field name="vf_rm_id" dataType="text">
            <title>Room Code</title>
        </field>
        <field name="vf_dv_id" dataType="text">
            <title>Division Code</title>
        </field>
        <field name="vf_dp_id" dataType="text">
            <title>Department Code</title>
        </field>
        <field name="vf_em_id" dataType="text">
            <title>Employee Code</title>
        </field>
        <field name="autonumbered_id"/>
    </panel>

    <!-- Add new custodian. -->
    <dataSource id="abCreateCustodian_ds">
        <table name="team" role="main"/>
        <table name="vn" role="standard"/>
        <table name="contact" role="standard"/>
        <table name="em" role="standard"/>
        <field table="team" name="autonumbered_id"/>
        <field table="team" name="vn_id"/>
        <field table="team" name="contact_id"/>
        <field table="team" name="em_id"/>
        <field table="team" name="custodian_type"/>
        <field table="team" name="source_table"/>
        <field table="team" name="is_owner"/>
        <field table="team" name="date_start"/>
        <field table="team" name="date_end"/>
        <field table="team" name="digital_signature"/>
        <field table="team" name="email_archive"/>
        <field table="team" name="cell_num_archive"/>
        <field table="team" name="phone_archive"/>
        <field table="team" name="notes"/>
        <field table="team" name="status"/>
        <field table="team" name="team_type"/>
        <field table="vn" name="vendor_photo"/>
        <field table="contact" name="contact_photo"/>
        <field table="em" name="em_photo"/>
        <field table="vn" name="vn_id"/>
        <field table="contact" name="contact_id"/>
        <field table="em" name="em_id"/>
    </dataSource>
    <panel type="form" id="abCreateCustodian_form" dataSource="abCreateCustodian_ds" columns="3" showOnLoad="false" hidden="true">
        <title>Add New Custodian</title>
        <action id="save" mainAction="true">
            <title>Save</title>
        </action>
        <action id="saveAndClose">
            <title>Save &amp; Close</title>
        </action>
        <!-- First row -->
        <field id="custodian_name" dataType="text" size="250" required="true">
            <title>Custodian Name</title>
            <action id="abEamAssignCustodianOther_form_selectValue_custodianName">
                <title translatable="false">...</title>
                <tooltip>Select Value</tooltip>
                <command type="callFunction" functionName="onSelectCustodianName"/>
            </action>
        </field>
        <field table="team" name="email_archive" readOnly="true"/>
        <field table="vn" name="vendor_photo" displayField="doc_graphic_image" hidden="true"/>
        <field table="contact" name="contact_photo" displayField="doc_graphic_image" hidden="true"/>
        <field table="em" name="em_photo" displayField="doc_graphic_image" hidden="true"/>
        <field id="doc_graphic_image" controlType="image" colspan="1" rowspan="5" width="100px"/>
        <!-- Second row -->
        <field table="team" name="custodian_type" required="true">
            <action id="selVal_custodian_type">
                <title translatable="true">...</title>
                <tooltip>Select Value</tooltip>
                <command type="selectValue" 
                    fieldNames="team.custodian_type" 
                    selectFieldNames="custodiantype.custodian_type" 
                    visibleFieldNames="custodiantype.custodian_type,custodiantype.description" 
                    restriction="custodiantype.custodian_type NOT IN ('OWNER')"/>
            </action>
        </field>
        <field table="team" name="cell_num_archive" readOnly="true"/>
        <field id="dummyField" dataType="text" readOnly="true"/>
        <!-- Third row -->
        <field table="team" name="phone_archive" readOnly="true"/>
        <field id="dummyField" dataType="text" readOnly="true"/>
        <!-- Fourth row -->
        <field id="source" dataType="text" readOnly="true">
            <title>Source</title>
        </field>
        <!-- Fifth row -->
        <field table="team" name="notes" colspan="2"/>
        <field id="dummyField" dataType="text" readOnly="true" colspan="3"/>
        <field id="dummyField" dataType="text" readOnly="true" colspan="3"/>
        <!-- hidden fields -->
        <field table="vn" name="vn_id" hidden="true"/>
        <field table="contact" name="contact_id" hidden="true"/>
        <field table="em" name="em_id" hidden="true"/>
        <field table="team" name="vn_id" hidden="true"/>
        <field table="team" name="contact_id" hidden="true"/>
        <field table="team" name="em_id" hidden="true"/>
        <field table="team" name="source_table" hidden="true"/>
        <field table="team" name="status" value="Active" hidden="true"/>
        <field table="team" name="is_owner" value="0" hidden="true"/>
        <field table="team" name="team_type" value="Equipment" hidden="true"/>
    </panel>
</view>