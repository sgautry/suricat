/**
 * Get custodian data from source table.
 * 
 * @param custodianName custodian name
 * @param sourceTable source table
 * @returns DataRecord
 */
function getCustodianDataFromSourceTable(custodianName, sourceTable){
	var dataSource = null;
	var pkFieldName = null;
	if ('em' == sourceTable) {
		dataSource = View.dataSources.get('abEmployee_ds');
		pkFieldName = 'em.em_id';
	} else if ('contact' == sourceTable) {
		dataSource = View.dataSources.get('abContact_ds');
		pkFieldName = 'contact.contact_id';
	} else if ('vn' == sourceTable) {
		dataSource = View.dataSources.get('abVendor_ds');
		pkFieldName = 'vn.vn_id';
	}
	try {
		var restriction = new Ab.view.Restriction();
		restriction.addClause(pkFieldName, custodianName, '=');
		var record = dataSource.getRecord(restriction);
		if (record.isNew) {
			record = null
		}
		return record;
	} catch (e) {
		Workflow.handleError(e);
		return false;
	}
}
