/**
 * EAM Exception Report restriction controller.
 *
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
View.createController('exceptionReportsRestrictionController', {
    restrictions: null,
    afterViewLoad: function () {
        if (valueExists(View.parameters) && valueExists(View.parameters.restrictions)) {
            this.restrictions = View.parameters.restrictions;
        }
    },
    afterInitialDataFetch: function () {
        if (this.restrictions) {
            var report = this.restrictions.report;
            if (report) {
                document.getElementById('selectedReport').textContent = report.title;
                if (valueExists(report.description)) {
                    document.getElementById('selectedReportDescription').textContent = report.description;
                }

            }
            var assetReportRestriction = this.restrictions.assetReportRestriction;
            if (assetReportRestriction) {
                var restriction = assetReportRestriction.restriction;
                if (restriction) {
                    this.clearRestrictions();
                    for ( var index = 0; index < restriction.clauses.length; index++ ) {
                        var clause = restriction.clauses[index];
                        this.generateRestriction(index, clause);
                    }
                }
            }
        }
    },
    generateRestriction: function (index, restriction) {
        var generateRestrictionTemplate = function (el, index, restriction) {
            var generatePanelTemplate = View.templates.get('restrictionTemplate');
            generatePanelTemplate.render({
                    index: index,
                    restriction: restriction
                },
                el,
                'after');
        };
        generateRestrictionTemplate(jQuery('#restrictionSummary')[0], index, restriction);
    },
    clearRestrictions: function () {
        var table = $('restrictionSummary');
        var rows = table.tBodies[0].rows.length;
        for ( var i = rows; i > 1; i-- ) {
            table.deleteRow(i - 1);
        }
    }
});