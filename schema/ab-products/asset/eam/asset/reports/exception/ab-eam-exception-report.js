/**
 * EAM Exception Report controller.
 *
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
View.extendController('exceptionReportController', exceptionReportBaseController, {
    dataStatisticsGridPanel: null,
    statistics: {},
    report: null,
    assetReportRestriction: null,
    assetRegistryFieldsMapping: {
        'cost_dep_value': {'bl': 'bl.value_book', 'eq': 'eq.cost_dep_value', 'ta': 'ta.cost_dep_value', 'property': 'property.value_book'},
        'cost_purchase': {'bl': 'ot.cost_purchase', 'eq': 'eq.cost_purchase', 'ta': 'ta.value_original', 'property': 'property.cost_purchase'},
        'cost_replace': {'bl': 'bl.value_market', 'eq': 'eq.cost_replace', 'ta': 'ta.value_replace', 'property': 'property.value_market'},
        'cost_val_market': {'bl': 'bl.value_market', 'eq': 'eq_dep.value_current', 'ta': 'ta.value_replace', 'property': 'property.value_market'},
        'value_salvage': {'bl': '', 'eq': 'eq.value_salvage', 'ta': 'ta.value_salvage', 'property': ''},
        'criticality': {'bl': 'bl.criticality', 'eq': 'eq.criticality', 'ta': 'ta.criticality', 'property': 'property.criticality'},
        'disposal_value': {'bl': 'ot.cost_selling', 'eq': 'eq.value_salvage', 'ta': 'ta.value_salvage', 'property': 'ot.cost_selling'}
    },
    setReportInstructions: function () {
        document.getElementById('reportInstructionsFilter').textContent = getMessage('reportInstructionsFilter');
        document.getElementById('reportInstructionsSelectReport').textContent = getMessage('reportInstructionsSelectReport');
        document.getElementById('reportInstructionsRestriction').textContent = getMessage('reportInstructionsRestriction');
        document.getElementById('reportInstructionsStatistics').textContent = getMessage('reportInstructionsStatistics');
        document.getElementById('selectedReport').textContent = getMessage('reportInstructionsSelectedReportNone');
    },
    initializeExceptionReportsActions: function () {
        var menuParent = Ext.get('selectReport');
        menuParent.on('click', this.showExceptionReports, this, null);
        var clearSelectedReport = Ext.get('clearSelectedReport');
        clearSelectedReport.set({'qtip': getMessage('reportClearTooltip')});
        this.enableResetButton(false);
        clearSelectedReport.on('click', this.resetExceptionReports, this, null);
        var showRestriction = Ext.get('showRestriction');
        showRestriction.on('click', this.showRestriction, this, null);
    },
    showExceptionReports: function () {
        var reportButton = Ext.get('selectReport');
        var reportMenuItem = new ExceptionReportMenuItem({
            menuDef: {
                id: 'exceptionReportsMenu',
                type: 'menu'
            },
            onClickMenuHandler: onClickExceptionReportMenu,
            submenu: exceptionReportsMenu
        });
        reportMenuItem.build();
        var menu = new Ext.menu.Menu({items: reportMenuItem.menuItems});
        menu.show(reportButton, 'tl-bl?');
    },
    resetExceptionReports: function (evt, button) {
        if (!Ext.get(button).hasClass('x-item-disabled')) {
            this.abEamAssetRegistry_list.addParameter(this.report.restriction, false);
            this.report = null;
            document.getElementById('selectedReport').textContent = getMessage('reportInstructionsSelectedReportNone');
            var visibleFields = this.getDefaultVisibleFields();
            this.updateDataStatisticsGrid(visibleFields);
            this.enableResetButton(false);
        }
    },
    enableResetButton: function (enable) {
        var clearSelectedReport = Ext.get('clearSelectedReport');
        if (enable) {
            clearSelectedReport.removeClass('x-item-disabled');
        } else {
            clearSelectedReport.addClass('x-item-disabled');
        }
    },
    createDataStatisticsGrid: function () {
        var visibleFields = this.getDefaultVisibleFields();
        var statisticsDivId = 'dataStatistics_div';
        var columns = [
            new Ab.grid.Column('title', getMessage("statisticsHeaderFieldTitle"), 'text', null, null, "1", "40%"),
            new Ab.grid.Column('id', getMessage("statisticsHeaderFieldId"), 'text', null, null, "1", "40%"),
            new Ab.grid.Column('sum', getMessage("statisticsHeaderSum"), 'checkbox', this.selectStatistic.createDelegate(this, [], true), null, "1", "10%"),
            new Ab.grid.Column('avg', getMessage("statisticsHeaderAvg"), 'checkbox', this.selectStatistic.createDelegate(this, [], true), null, "1", "10%")
        ];
        var configObj = new Ab.view.ConfigObject();
        configObj['rows'] = this.setDataStatisticsRows(visibleFields);
        configObj['columns'] = columns;
        // create new Grid component instance
        this.dataStatiscticsGridPanel = new Ab.grid.Grid(statisticsDivId, configObj);
        this.dataStatiscticsGridPanel.sortEnabled = false;
        this.dataStatiscticsGridPanel.build();
    },
    updateDataStatisticsGrid: function (visibleFields) {
        if (this.dataStatiscticsGridPanel) {
            this.statistics = {};
            this.dataStatiscticsGridPanel.rows = this.setDataStatisticsRows(visibleFields);
            this.dataStatiscticsGridPanel.build();
        }
    },
    setDataStatisticsRows: function (visibleFields) {
        var rows = [], assetType;
        if (valueExists(this.report)) {
            assetType = this.report.assetType;
        }
        var dataSource = View.dataSources.get('abEamAssetRegistry_ds');
        dataSource.fieldDefs.each(function (fieldDef) {
            if (fieldDef.isNumeric && _.contains(visibleFields, fieldDef.id)) {
                var fieldId = fieldDef.id.split('bl.').pop();
                if (assetType && 'allAssets' !== assetType) {
                    fieldId = this.assetRegistryFieldsMapping[fieldId][assetType];
                }
                if (valueExistsNotEmpty(fieldId)) {
                    rows.push({
                        title: fieldDef.title,
                        id: fieldId,
                        fieldDef: fieldDef
                    })
                }
            }
        }, this);
        return rows;
    },
    selectStatistic: function (row, column) {
        var fieldDef = row.fieldDef;
        this.statistics[fieldDef.id] = {
            showSum: $('dataStatistics_div_row' + row.index + '_sum').checked,
            showAvg: $('dataStatistics_div_row' + row.index + '_avg').checked
        }
    },
    addStatistics: function (parentElement, assetRegistryPanel) {
        var statistics = this.statistics;
        var currencySymbol = View.project.budgetCurrency.symbol;
        var mainTableName = assetRegistryPanel.getDataSource().mainTableName;
        var rowElementTotal = document.createElement('tr'),
            rowElementAvg = document.createElement('tr');
        for ( var c = 0; c < assetRegistryPanel.columns.length; c++ ) {
            var column = assetRegistryPanel.columns[c];
            if (column.hidden) {
                continue;
            }
            var statisticColumn = statistics[column.id];
            var cellElementTotal = document.createElement('td');
            cellElementTotal.className = 'totals';
            var cellElementAvg = document.createElement('td');
            cellElementAvg.className = 'totals';
            var name = valueExists(column.fullName) ? column.fullName : column.id;
            if (name.indexOf('.') !== -1) {
                name = name.substring(name.indexOf('.') + 1);
            }
            if (valueExists(statisticColumn)) {
                var totalValueLocalized = assetRegistryPanel.totals.getLocalizedValue(mainTableName + '.sum_' + name),
                    totalValue = Number(assetRegistryPanel.totals.getValue(mainTableName + '.sum_' + name)),
                    countOfRecords = Number(assetRegistryPanel.totals.getValue(mainTableName + '.count_of_records'));
                var totalAvg = totalValue / countOfRecords;
                var totalAvgLocalized = assetRegistryPanel.getDataSource().formatValue(column.id, totalAvg.toFixed(2), true);
                var totalHtml = '', avgHtml = '';
                if (statisticColumn.showSum) {
                    totalHtml = getMessage('statisticsFooterTotal') + ' ' + totalValueLocalized;
                }
                if (statisticColumn.showAvg) {
                    avgHtml = getMessage('statisticsFooterAvg') + ' ' + totalAvgLocalized;
                }
                cellElementTotal.innerHTML = totalHtml;
                cellElementAvg.innerHTML = avgHtml;
            }
            rowElementTotal.appendChild(cellElementTotal);
            rowElementAvg.appendChild(cellElementAvg);
        }
        parentElement.appendChild(rowElementTotal);
        parentElement.appendChild(rowElementAvg);
    },
    reportConfigHeaderPanel_onShowReport: function () {
        this.addExceptionReportsRestriction(this.assetReportRestriction);
        this.refreshAssetRegistryPanel(this.abEamAssetRegistry_list, this.assetReportRestriction);
    },
    setExceptionReport: function (report) {
        this.report = report;
        document.getElementById('selectedReport').textContent = this.getReportTitle();
        var visibleFields = this.getVisibleReportFields();
        this.updateDataStatisticsGrid(visibleFields);
        this.enableResetButton(true);
    },
    addExceptionReportsRestriction: function (assetReportRestriction) {
        if (valueExists(this.report)) {
            var reportRestrictionId = this.report.restriction;
            this.abEamAssetRegistry_list.addParameter(reportRestrictionId, true);
            assetReportRestriction.exceptionReportRestriction = "${sql.getRestriction('ab-eam-exception-report-definition.axvw', 'abExceptionReportRestrictions_" + this.report.assetType + "_ds', '" + reportRestrictionId + "')}";
        }
        this.assetReportRestriction = assetReportRestriction;
    },
    updateVisibleFields: function () {
        var visibleFields = this.getDefaultVisibleFields();
        if (valueExists(this.report)) {
            visibleFields = this.getVisibleReportFields();
        }
        this.updatePanelVisibleFields(visibleFields);
    },
    getVisibleReportFields: function () {
        var fieldsConfig = fieldsConfigDefinition.get(this.report.fieldsConfig.id);
        if (fieldsConfig) {
            return _.map(fieldsConfig.fields, function (fieldName) {
                if ('condition' === fieldName &&
                    (this.report.restriction.indexOf('missingData_assetState_condition') >= 0
                        || this.report.restriction.indexOf('missingData_assetState_all') >= 0
                        || this.report.restriction.indexOf('allAssets_missingData_assetState_asset_std_status_condition_description') >= 0)) {
                    // hide condition field since the UI displays the default value even when the field value is Null
                } else {
                    return 'bl.' + fieldName
                }
            }, this);
        }
        return null;
    },
    getDefaultVisibleFields: function () {
        return _.map(fieldsConfigDefinition.get('costs').fields, function (fieldName) {
            return 'bl.' + fieldName
        });
    },
    updatePanelVisibleFields: function (visibleFields) {
        var registryPanel = View.panels.get('abEamAssetRegistry_list');
        var startIndex = 4; // exclude action columns
        _.each(registryPanel.fieldDefs, function (fieldDef) {
            if ('image' !== fieldDef.controlType) {
                var show = _.contains(visibleFields, fieldDef.id);
                if (show) {
                    var index = _.indexOf(visibleFields, fieldDef.id);
                    registryPanel.setColumnDisplayOrder(fieldDef.id, index + startIndex);
                }
                registryPanel.showColumn(fieldDef.id, show);
            }
        });
        if (registryPanel.tableHeadElement !== null) {
            // update grid
            registryPanel.update();
        }
    },
    abEamAssetRegistry_list_afterRefresh: function (registryPanel) {
        if (valueExists(this.report) && valueExistsNotEmpty(this.report.id)) {
            var reportName = this.getReportTitle();
            registryPanel.setTitle(reportName);
        }
    },
    getReportTitle: function () {
        if (this.report) {
            return getMessage(this.report.id);
        }
        return null;
    },
    showRestriction: function () {
        var reportTitle = getMessage('reportInstructionsSelectedReportNone'),
            reportDescription = null;
        if (this.report) {
            reportTitle = this.report.title;
            reportDescription = this.report.description
        }
        var restrictions = {
            report: {
                title: reportTitle,
                description: reportDescription
            },
            assetReportRestriction: this.assetReportRestriction
        };
        View.openDialog('ab-eam-exception-report-restriction.axvw', null, false, {
            restrictions: restrictions,
            closeButton: true
        });
    }
});

function onClickExceptionReportMenu(menuItem) {
    if (valueExists(menuItem.id) && valueExists(menuItem.assetType) && valueExists(menuItem.category)) {
        View.controllers.get('exceptionReportController').setExceptionReport(menuItem);
    }
}