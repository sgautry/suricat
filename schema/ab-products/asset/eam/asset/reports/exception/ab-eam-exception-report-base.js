/**
 * EAM Exception Report base controller.
 *
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Filter configuration.
 * @type {Ext.util.MixedCollection}
 */
var reportFilterConfig = new Ext.util.MixedCollection();
reportFilterConfig.addAll(
    {id: 'bl.pending_action', fieldConfig: {type: 'enumList', hidden: true, readOnly: true, values: null, dfltValue: null, hasEmptyOption: false}},
    {id: 'deprec_method', fieldConfig: {type: 'enumList', hidden: false, readOnly: false, values: null, dfltValue: null, hasEmptyOption: true}},
    {id: 'deprec_value_type', fieldConfig: {type: 'enumList', hidden: false, readOnly: false, values: null, dfltValue: null, hasEmptyOption: true}},
    {id: 'deprec_value', fieldConfig: {type: 'number', hidden: true, readOnly: false, values: null, dfltValue: null, hasEmptyOption: false}},
    {id: 'eq_system.stakeholder_type', fieldConfig: {type: 'enumList', hidden: true, readOnly: true, values: null, dfltValue: null, hasEmptyOption: false}},
    {id: 'eq_system.criticality_function', fieldConfig: {type: 'enumList', hidden: true, readOnly: true, values: null, dfltValue: null, hasEmptyOption: false}},
    {id: 'eq_system.criticality_mission', fieldConfig: {type: 'enumList', hidden: true, readOnly: true, values: null, dfltValue: null, hasEmptyOption: false}}
);
var exceptionReportBaseController = Ab.view.Controller.extend({
    // filter console controller
    filterController: null,
    assetRegistryRestriction: null,
    // main controller - tabs
    mainController: null,
    afterViewLoad: function () {
        if (!isCustodianSchemaDefined) {
            reportFilterConfig.add({id: 'custodian', fieldConfig: {type: 'text', hidden: true}});
            this.abEamAssetRegistry_list.removeColumn(this.abEamAssetRegistry_list.findColumnIndex('bl.owner_custodian'));
        }
        // initialize filter config
        this.initializeFilterConfig();
        // customize mini console
        this.abEamAssetRegistry_list.addEventListener('afterBuildHeader', this.abEamAssetRegistry_list_customizeHeader, this);
        // customize grid columns
        this.abEamAssetRegistry_list.afterCreateCellContent = this.customizeAssetRegistryFields.createDelegate(this);
        this.abEamAssetRegistry_list.buildPostFooterRows = this.addStatistics.createDelegate(this, [this.abEamAssetRegistry_list], true);
        this.setReportInstructions();
        this.createDataStatisticsGrid();
    },
    afterInitialDataFetch: function () {
        // get main Controller
        this.mainController = this.getMainController();
        if (valueExists(this.mainController)) {
            // set filter restriction if was applied from another tab
            this.filterController.setRestriction(this.mainController.filterRestriction);
        }
        // initialize filter
        this.filterController.initializeFilter();
        // initialize asset status
        this.filterController.onChangeAssetType();
        if (valueExists(this.mainController)) {
            // load initial data
            this.filterController.abEamAssetFilter_onFilter();
        } else {
            // select the first element in the Inventory tree
            //this.fireClickFirstNode();
            this.filterController.abEamAssetFilter_onFilter();
        }
        this.initializeExceptionReportsActions();
    },
    /**
     * Always display report configuration header without updating the vertical scroller.
     */
    afterLayout: function() {
        var layoutManager = View.getLayoutManager('centerLayout');
        layoutManager.setRegionSize('north', 43);
        var regionPanel = layoutManager.getRegionPanel('north');
        regionPanel.scroller.scrollbars._prefs.verticalScrolling = false;
    },
    initializeFilterConfig: function () {
        // place holder for custom actions
        if (!valueExists(this.filterController)) {
            this.filterController = this.getFilterController();
        }
        this.filterController.initializeConfigObjects(reportFilterConfig);
        this.filterController.onFilterCallback = this.onFilter.createDelegate(this);
        this.filterController.onClickActionButton1Handler = this.onClickReportMenu.createDelegate(this);
        this.filterController.actionButton1Label = getMessage('buttonLabel_reports');
    },
    /**
     * On filter event handler
     * @param restriction restriction object
     */
    onFilter: function (restriction) {
        //set current restriction  to main controller. This restriction is passed from tabs.
        if (valueExists(this.mainController)) {
            this.mainController.filterRestriction = this.filterController.getRestrictionWithoutDefaultValues(restriction);
        }
        var controller = this;
        this.filterRestriction = restriction;
        var parameters = {
            'onClickClearSelectionHandler': function () {
                controller.onClickClearSelectionHandler();
            },
            'onClickNodeEventHandler': function (treeType, restriction) {
                controller.refreshFromTreeNode(treeType, restriction);
            }
        };
        var geoTreeParameters = getGeographicalTreeRestrictionFromAssetFilter(this.filterRestriction, parameters);
        this.abEamAssetManagementTree.findTab('abEamAssetManagementTreeGeo').parameters = geoTreeParameters;
        var locationTreeParameters = getLocationTreeRestrictionFromAssetFilter(this.filterRestriction, parameters);
        this.abEamAssetManagementTree.findTab('abEamAssetManagementTreeLocation').parameters = locationTreeParameters;
        var orgTreeParameters = getOrganizationTreeRestrictionFromAssetFilter(this.filterRestriction, parameters);
        this.abEamAssetManagementTree.findTab('abEamAssetManagementTreeOrg').parameters = orgTreeParameters;
        var projTreeParameters = getProjectTreeRestrictionFromAssetFilter(this.filterRestriction, parameters);
        this.abEamAssetManagementTree.findTab('abEamAssetManagementTreeProj').parameters = projTreeParameters;
        _.extend(parameters, {
            'filterRestriction': this.filterRestriction
        });
        this.abEamAssetManagementTree.findTab('abEamAssetManagementEqSystems').parameters = parameters;
        var selectedTabName = this.abEamAssetManagementTree.getSelectedTabName();
        this.abEamAssetManagementTree.refreshTab(selectedTabName);
        this.assetRegistryRestriction = getRestrictionForAssetRegistryFromFilter(restriction);
        this.addExceptionReportsRestriction(this.assetRegistryRestriction);
        this.refreshAssetRegistryPanel(this.abEamAssetRegistry_list, this.assetRegistryRestriction);
    },
    onClickClearSelectionHandler: function () {
        this.filterController.resetFilter();
    },
    refreshFromTreeNode: function (treeType, restriction) {
        var objAssetRegRestr = getRestrictionForAssetRegistry(treeType, restriction, this.assetRegistryRestriction);
        this.addExceptionReportsRestriction(objAssetRegRestr);
        this.refreshAssetRegistryPanel(this.abEamAssetRegistry_list, objAssetRegRestr);
    },
    refreshAssetRegistryPanel: function (objPanel, objAssetRegRestriction) {
        var objRestriction = objAssetRegRestriction.restriction;
        objPanel.addParameter('propertyTypeRestriction', valueExistsNotEmpty(objAssetRegRestriction.propertySql) ? objAssetRegRestriction.propertySql : "1=1");
        objPanel.addParameter('blTypeRestriction', valueExistsNotEmpty(objAssetRegRestriction.blSql) ? objAssetRegRestriction.blSql : "1=1");
        objPanel.addParameter('eqTypeRestriction', valueExistsNotEmpty(objAssetRegRestriction.eqSql) ? objAssetRegRestriction.eqSql : "1=1");
        objPanel.addParameter('taTypeRestriction', valueExistsNotEmpty(objAssetRegRestriction.taSql) ? objAssetRegRestriction.taSql : "1=1");
        objPanel.addParameter('sqlTypeRestriction', valueExistsNotEmpty(objAssetRegRestriction.sql) ? objAssetRegRestriction.sql : "1=1");
        objPanel.addParameter('exceptionReportRestriction', valueExistsNotEmpty(objAssetRegRestriction.exceptionReportRestriction) ? objAssetRegRestriction.exceptionReportRestriction : "1=1");
        objPanel.addParameter('useCustodian', isCustodianSchemaDefined);
        objPanel.addParameter('useCostDepreciation', isCostDepreciationSchemaDefined);
        this.updateVisibleFields();
        objPanel.refresh(objRestriction);
    },
    getFilterController: function () {
        return View.controllers.get('abEamAssetFilterCtrl');
    },
    getMainController: function () {
        var controller = null;
        var openerView = this.view.getOpenerView();
        if (valueExists(openerView)) {
            if (valueExists(openerView.controllers.get('eqSystemConsoleTabsController'))) {
                controller = openerView.controllers.get('eqSystemConsoleTabsController');
            } else if (valueExists(openerView.controllers.get('abEamAssetConsoleController'))) {
                controller = openerView.controllers.get('abEamAssetConsoleController');
            } else if (valueExists(openerView.controllers.get('abEamDisposalConsoleCtrl'))) {
                controller = openerView.controllers.get('abEamDisposalConsoleCtrl');
            }
        }
        return controller;
    },
    abEamAssetRegistry_list_onAssetAssign: function (row, panel) {
        var record = row.getRecord();
        var rowIndex = row.getIndex();
        var buttonId = panel.id + '_row' + rowIndex + '_assignRow';
        var button = document.getElementById(buttonId);
        var assetType = record.getValue('bl.asset_type');
        assignAsset(assetType, record, button);
    },
    abEamAssetRegistry_list_onAssetEdit: function (row, panel) {
        var record = row.getRecord();
        var gridPanel = row.grid;
        var assetType = record.getValue('bl.asset_type');
        var assetId = record.getValue('bl.asset_id');
        var viewName = '';
        var restriction = new Ab.view.Restriction();
        if (assetType == 'bl') {
            restriction.addClause('bl.bl_id', assetId, '=');
            viewName = 'ab-eam-def-geo-loc.axvw';
        } else if (assetType == 'property') {
            restriction.addClause('property.pr_id', assetId, '=');
            viewName = 'ab-rplm-properties-define-form.axvw';
        } else if (assetType == 'eq') {
            restriction.addClause('eq.eq_id', assetId, '=');
            viewName = 'ab-eq-edit-form.axvw';
        } else if (assetType == 'ta') {
            restriction.addClause('ta.ta_id', assetId, '=');
            viewName = 'ab-ta-edit-form.axvw';
        }
        this.editAsset(assetType, viewName, restriction, false, gridPanel);
    },
    editAsset: function (type, viewName, restriction, newRecord) {
        var controller = this;
        var dialogConfig = null;
        if (type == 'bl') {
            dialogConfig = {
                width: 1024,
                height: 800,
                closeButton: true,
                type: type,
                hideTabs: true,
                callback: function () {
                    controller.abEamAssetRegistry_list.refresh(controller.abEamAssetRegistry_list.restriction);
                }
            };
        } else {
            dialogConfig = {
                width: 1024,
                height: 800,
                closeButton: true,
                callback: function () {
                    controller.abEamAssetRegistry_list.refresh(controller.abEamAssetRegistry_list.restriction);
                }
            };
        }
        View.getOpenerView().openDialog(viewName, restriction, newRecord, dialogConfig);
    },
    abEamAssetRegistry_list_onAssetActivities: function (row, panel) {
        var record = row.getRecord();
        var assetType = record.getValue('bl.asset_type');
        var assetId = record.getValue('bl.asset_id');
        var restriction = new Ab.view.Restriction();
        restriction.addClause('bl.asset_type', assetType, '=');
        restriction.addClause('bl.asset_id', assetId, '=');
        View.openDialog('ab-eam-disposal-asset-summary.axvw', restriction, false, {
            applyActionStatusRestriction: true,
            width: 1024,
            heigth: 800,
            closeButton: true
        });
    },
    abEamAssetRegistry_list_onAssetProfile: function (row, panel) {
        var record = row.getRecord();
        var assetId = record.getValue('bl.asset_id');
        var assetType = record.getValue('bl.asset_type');
        var viewName = null;
        var restriction = new Ab.view.Restriction();
        if (assetType == 'bl') {
            viewName = 'ab-profile-building.axvw';
            restriction.addClause('bl.bl_id', assetId, '=');
        } else if (assetType == 'property') {
            viewName = 'ab-profile-property.axvw';
            restriction.addClause('property.pr_id', assetId, '=');
        } else if (assetType == 'eq') {
            viewName = 'ab-profile-equipment.axvw';
            restriction.addClause('eq.eq_id', assetId, '=');
        } else if (assetType == 'ta') {
            viewName = 'ab-profile-ta.axvw';
            restriction.addClause('ta.ta_id', assetId, '=');
        }
        View.getOpenerView().openDialog(viewName, restriction, false, {
            width: 1050,
            height: 650,
            closeButton: true
        })
    },
    abEamAssetRegistry_list_customizeHeader: function (panel, parentElement) {
        var columnIndex = 0;
        for ( var i = 0, col; col = panel.columns[i]; i++ ) {
            if (col.id == 'bl.asset_type' && !col.hidden) {
                var headerRows = parentElement.getElementsByTagName("tr");
                // customize filter row if this exists
                var rowElement = document.getElementById(panel.id + '_filterRow');
                if (headerRows.length > 1 && valueExists(rowElement)) {
                    var filterInputId = panel.getFilterInputId(col.id);
                    this.updateMiniConsole(panel, rowElement, columnIndex, filterInputId);
                }
            }
            if (!col.hidden) {
                columnIndex++;
            }
        }
    },
    /**
     * Customize filter row asset type.
     *
     * @param panel grid panel
     * @param rowElement filter row element
     * @param columnIndex column index
     * @param filterInputId filter input id
     */
    updateMiniConsole: function (panel, rowElement, columnIndex, filterInputId) {
        var assetTypes = {
            'property': getMessage('asset_type_property'),
            'bl': getMessage('asset_type_bl'),
            'eq': getMessage('asset_type_eq'),
            'ta': getMessage('asset_type_ta')
        };
        var headerCells = rowElement.getElementsByTagName("th");
        var headerCell = headerCells[columnIndex];
        // remove input element if exists
        var inputElements = headerCell.getElementsByTagName("input");
        if (inputElements[filterInputId]) {
            headerCell.removeChild(inputElements[filterInputId]);
        }
        // add select element
        var i = 0;
        var input = document.createElement("select");
        input.options[i++] = new Option("", "", true);
        for ( var storedValue in assetTypes ) {
            input.options[i++] = new Option(assetTypes[storedValue], storedValue);
        }
        input.className = "inputField_box";
        // run filter when user click on one enum value
        Ext.EventManager.addListener(input, "change", panel.onFilter.createDelegate(panel));
        input.id = filterInputId;
        if (headerCell.childNodes.length > 0) {
            headerCell.insertBefore(input, headerCell.childNodes[0]);
        } else {
            headerCell.appendChild(input);
        }
    },
    /**
     * Customize fields for asset registry panel
     */
    customizeAssetRegistryFields: function (row, column, cellElement) {
        if (column.id == 'bl.asset_type') {
            var value = cellElement.childNodes[0].text
            if (valueExistsNotEmpty(value)) {
                cellElement.childNodes[0].innerHTML = getMessage('asset_type_' + value);
            }
        } else if (column.id == 'bl.asset_status') {
            var status = row.row.getFieldValue('bl.asset_status');
            var assetType = row.row.getFieldValue('bl.asset_type');
            var localizedValue = this.getLocalizedAssetStatus(assetType, status);
            cellElement.childNodes[0].innerHTML = localizedValue;
        }
    },
    /**
     * Returns localized status value for asset.
     * @param type  asset type
     * @param value status value
     * @return string
     */
    getLocalizedAssetStatus: function (type, value) {
        var localizedValue = '';
        if (valueExistsNotEmpty(value)) {
            var assetStatusDs = this.abAssetRegistryStatus_ds;
            if (type == 'bl') {
                localizedValue = assetStatusDs.fieldDefs.get('bl.status').enumValues[value];
            } else if (type == 'property') {
                localizedValue = assetStatusDs.fieldDefs.get('property.status').enumValues[value];
            } else if (type == 'eq') {
                localizedValue = assetStatusDs.fieldDefs.get('eq.status').enumValues[value];
            } else if (type == 'ta') {
                localizedValue = assetStatusDs.fieldDefs.get('ta.status').enumValues[value];
            }
        }
        return localizedValue;
    },
    onClickReportMenu: function (buttonElem) {
        var reportMenuItem = new MenuItem({
            menuDef: {
                id: 'reportsMenu',
                type: 'menu',
                viewName: null,
                isRestricted: false,
                parameters: null
            },
            onClickMenuHandler: onClickMenu,
            onClickMenuHandlerRestricted: onClickMenuWithRestriction,
            submenu: abEamReportsCommonMenu
        });
        reportMenuItem.build();
        var menu = new Ext.menu.Menu({items: reportMenuItem.menuItems, cls: 'filter-reports'});
        menu.show(buttonElem, 'tl-bl?');
    }
});
function onClickMenu(menu) {
    if (valueExists(menu.viewName)) {
        var dialogConfig = {
            width: 1024,
            height: 800,
            closeButton: true
        };
        if (valueExists(menu.parameters)) {
            for ( var param in menu.parameters ) {
                if ('title' === param) {
                    dialogConfig[param] = getMessage(menu.parameters[param]);
                } else {
                    dialogConfig[param] = menu.parameters[param];
                }
            }
        }
        View.openDialog(menu.viewName, null, false, dialogConfig);
    }
}
function onClickMenuWithRestriction(menu) {
    if (valueExists(menu.viewName)) {
        var dialogConfig = {
            width: 1024,
            height: 800,
            closeButton: true
        };
        if (valueExists(menu.parameters)) {
            for ( var param in menu.parameters ) {
                if ('title' === param) {
                    dialogConfig[param] = getMessage(menu.parameters[param]);
                } else {
                    dialogConfig[param] = menu.parameters[param];
                }
            }
        }
        View.openDialog(menu.viewName, null, false, dialogConfig);
    }
}
