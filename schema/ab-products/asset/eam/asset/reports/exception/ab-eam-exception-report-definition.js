/**
 * EAM Exception Report reports definition.
 *
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
var exceptionReportsMenu = new Ext.util.MixedCollection();
View.createController('exceptionReportsSelectController', {
    reportLabel: 'exRpt',
    missingDataLabel: 'missingData',
    incorrectDataLabel: 'incorrectData',
    reportDescriptionLabel: 'description',
    $delimiter: '$',
    _delimiter: '_',
    afterViewLoad: function () {
        var reports = _.filter(_.keys(View.messages), function (message) {
            return message.indexOf(this.reportLabel + this.$delimiter) === 0;
        }, this);
        if (!_.isEmpty(reports)) {
            this.buildReports(reports);
        }
    },
    buildReports: function (reports) {
        var property = this.buildReportsForType(reports, 'property');
        var buildingReports = this.buildReportsForType(reports, 'bl');
        var equipmentReports = this.buildReportsForType(reports, 'eq');
        var furnitureReports = this.buildReportsForType(reports, 'ta');
        var allAssetsReports = this.buildReportsForType(reports, 'allAssets');
        exceptionReportsMenu.addAll([property, buildingReports, equipmentReports, furnitureReports, allAssetsReports]);
    },
    buildReportsForType: function (allReports, assetType) {
        var reportId = this.reportLabel + this.$delimiter + assetType;
        var reportsMenu = new Ext.util.MixedCollection();
        reportsMenu.add(reportId, {
            menuDef: {
                id: this.reportLabel + this._delimiter + assetType,
                type: 'menu'
            },
            submenu: new Ext.util.MixedCollection()
        });
        var reports = _.filter(allReports, function (report) {
            return report.indexOf(reportId) === 0;
        });
        if (!_.isEmpty(reports)) {
            var data = {};
            _.each(reports, function (report) {
                // Report pattern: exRpt$<assetType>$<reportType>$<category_order>$<fieldsConfig_order>$<reportFields>
                var properties = report.split(this.$delimiter);
                var reportType = properties[2];
                var categoryProperties = properties[3].split(this._delimiter);
                var category = {
                    id: categoryProperties[0],
                    order: categoryProperties[1]
                };
                var fieldsConfigProperties = properties[4].split(this._delimiter);
                var fieldsConfig = {
                    id: fieldsConfigProperties[0],
                    order: fieldsConfigProperties[1]
                };
                var fields = properties[5];
                //Restriction pattern: <assetType>_<reportType>_<category.id>_<fields>
                var restriction = assetType + this._delimiter + reportType + this._delimiter + category.id + this._delimiter + fields;
                var reportDescription = restriction + this._delimiter + this.reportDescriptionLabel,
                    description = null;
                if (valueExists(View.messages[reportDescription])) {
                    description = getMessage(reportDescription);
                }
                data[reportType] = data[reportType] || {};
                data[reportType][category.id] = data[reportType][category.id] || [];
                data[reportType][category.id].push({
                    menuDef: {
                        id: report,
                        prefix: this.reportLabel,
                        assetType: assetType,
                        reportType: reportType,
                        category: category,
                        fieldsConfig: fieldsConfig,
                        title: getMessage(report),
                        fields: fields,
                        restriction: restriction,
                        description: description
                    }
                });
            }, this);
            if (data[this.missingDataLabel]) {
                var missingDataReportsCollection = this.buildDataReports(data[this.missingDataLabel], this.missingDataLabel);
                reportsMenu.get(reportId).submenu.add(missingDataReportsCollection);
            }
            if (data[this.incorrectDataLabel]) {
                var incorrectDataReportsCollection = this.buildDataReports(data[this.incorrectDataLabel], this.incorrectDataLabel);
                reportsMenu.get(reportId).submenu.add(incorrectDataReportsCollection);
            }
        }
        return {
            menuDef: {
                id: this.reportLabel + this._delimiter + assetType,
                type: 'menu'
            },
            submenu: reportsMenu.get(reportId).submenu
        };
    },
    buildDataReports: function (reports, dataLabel) {
        var id = this.reportLabel + this._delimiter + dataLabel;
        var collection = {
            menuDef: {
                id: id,
                title: getMessage(id),
                type: 'menu'
            },
            submenu: new Ext.util.MixedCollection()
        };
        var sortedReports = _.sortBy(_.keys(reports), function (report) {
            var order = reports[report][0].menuDef.category.order;
            return Number(order);
        });
        _.each(sortedReports, function (categoryKey) {
            var categoryMenu = reports[categoryKey];
            var sortedCategory = _.sortBy(categoryMenu, function (menu) {
                return Number(menu.menuDef.fieldsConfig.order);
            });
            var categoryReports = new Ext.util.MixedCollection();
            categoryReports.addAll(sortedCategory);
            var id = this.reportLabel + this._delimiter + categoryKey;
            collection.submenu.add({
                menuDef: {
                    id: id,
                    title: getMessage(id),
                    type: 'menu'
                },
                submenu: categoryReports
            });
        }, this);
        return collection;
    }
});
/**
 * Menu item object.
 */
ExceptionReportMenuItem = Base.extend({
    // menu id
    id: null,
    // parent menu
    parent: null,
    // menu type
    type: null,
    // submenu
    submenu: null,
    // menu definition
    menuDef: null,
    // menu item config
    config: null,
    // menu items
    menuItems: [],
    // menu label
    label: null,
    // on click menu handler
    onClickMenuHandler: null,
    // contructor
    constructor: function (configObject) {
        this.config = configObject;
        Ext.apply(this, configObject);
        Ext.apply(this, configObject.menuDef);
        var parent = configObject['parent'];
        if (valueExists(parent)) {
            this.parent = parent;
        }
        var onClickMenuHandler = configObject['onClickMenuHandler'];
        if (valueExists(onClickMenuHandler)) {
            this.onClickMenuHandler = onClickMenuHandler;
        }
    },
    /**
     * Build menu item
     */
    build: function () {
        this.menuItems = [];
        var menuItem = null;
        if (this.id) {
            this.label = getMessage(this.id);
        }
        if (this.type && 'menu' === this.type) {
            // is submenu type
            var parent = this;
            if (this.submenu) {
                this.submenu.each(function (subMenuConfig) {
                    subMenuConfig['parent'] = parent;
                    subMenuConfig['onClickMenuHandler'] = parent.onClickMenuHandler;
                    var subMenu = new ExceptionReportMenuItem(subMenuConfig);
                    subMenu.build();
                });
            }
            if (valueExists(this.parent)) {
                menuItem = new Ext.menu.Item({
                    text: this.label,
                    menu: {items: this.menuItems}
                });
                this.parent.addMenu(menuItem);
            }
        } else {
            // is menu item type
            var onClickHandler = this.getOnClickHandler();
            var menuDef = this.menuDef;
            menuItem = new Ext.menu.Item({
                text: this.label,
                handler: onClickHandler.createDelegate(this, [menuDef])
            });
            if (valueExists(this.parent)) {
                this.parent.addMenu(menuItem);
            }
        }
    },
    // construct menu items
    buildMenuItems: function () {
    },
    getOnClickHandler: function () {
        return this.onClickMenuHandler;
    },
    setOnClickMenuHandler: function (handler) {
        this.onClickMenuHandler = handler;
    },
    addMenu: function (menu) {
        if (valueExists(menu)) {
            this.menuItems.push(menu);
        }
    }
}, {
    // label prefix - used to get translatable text
    labelPrefix: 'menuLabel_'
});
var fieldsConfigDefinition = new Ext.util.MixedCollection();
fieldsConfigDefinition.addAll(
    {
        id: 'standard',
        fields: ['asset_id', 'asset_type', 'asset_std', 'description', 'asset_status', 'condition', 'criticality', 'geo_region_id', 'ctry_id', 'regn_id', 'state_id', 'city_id', 'site_id', 'bl_id', 'fl_id', 'rm_id', 'dv_id', 'dp_id', 'cost_replace', 'cost_purchase']
    },
    {
        id: 'location',
        fields: ['asset_id', 'asset_type', 'asset_std', 'description', 'geo_region_id', 'ctry_id', 'regn_id', 'state_id', 'city_id', 'pr_id', 'site_id', 'bl_id', 'fl_id', 'rm_id', 'dv_id', 'dp_id', 'asset_status', 'condition', 'criticality', 'cost_dep_value', 'cost_val_market']
    },
    {
        id: 'assetState',
        fields: ['asset_id', 'asset_type', 'asset_std', 'description', 'asset_status', 'condition', 'criticality', 'geo_region_id', 'ctry_id', 'regn_id', 'state_id', 'city_id', 'site_id', 'bl_id', 'fl_id', 'rm_id', 'dv_id', 'dp_id', 'cost_dep_value', 'cost_val_market']
    },
    {
        id: 'organization',
        fields: ['asset_id', 'asset_type', 'asset_std', 'description', 'bu_id', 'dv_id', 'dp_id', 'ctry_id', 'state_id', 'city_id', 'pr_id', 'site_id', 'bl_id', 'fl_id', 'rm_id', 'asset_status', 'condition', 'criticality', 'cost_dep_value', 'cost_val_market']
    },
    {
        id: 'costs',
        fields: ['asset_id', 'asset_type', 'asset_std', 'description', 'cost_dep_value', 'cost_val_market', 'cost_replace', 'cost_purchase', 'value_salvage', 'asset_status', 'condition', 'criticality', 'geo_region_id', 'ctry_id', 'state_id', 'city_id', 'site_id', 'bl_id', 'fl_id', 'rm_id', 'dv_id', 'dp_id']
    },
    {
        id: 'dates',
        fields: ['asset_id', 'asset_type', 'asset_std', 'description', 'date_purchased', 'disposal_type', 'date_disposal', 'disposal_value', 'cost_purchase', 'asset_status', 'condition', 'criticality', 'ctry_id', 'state_id', 'city_id', 'site_id', 'bl_id', 'fl_id', 'rm_id', 'dv_id', 'dp_id']
    }
);