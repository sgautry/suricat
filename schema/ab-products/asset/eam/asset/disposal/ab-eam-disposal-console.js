var abEamDisposalConsoleCtrl = View.createController('abEamDisposalConsoleCtrl', {
    filterRestriction: null,
    afterInitialDataFetch: function () {
        this.abEamAssetDisposalConsoleTabs.addEventListener('afterTabChange', this.afterTabChangeHandler.createDelegate(this));
    },
    afterTabChangeHandler: function (tabsPanel, selectedTabName) {
        var selectedTab = tabsPanel.findTab(selectedTabName);
        if (selectedTab.isContentLoaded) {
            var controller;
            if ('abEamAssetDisposalConsoleTabs_analyze' === selectedTabName) {
                controller = selectedTab.getContentFrame().View.controllers.get('abEamDisposalAnalyzeCtrl');
            } else if ('abEamAssetDisposalConsoleTabs_evaluate' === selectedTabName) {
                controller = selectedTab.getContentFrame().View.controllers.get('abEamDisposalEvaluateCtrl');
            } else if ('abEamAssetDisposalConsoleTabs_manage' === selectedTabName) {
                controller = selectedTab.getContentFrame().View.controllers.get('abEamDisposalManageCtrl');
            } else if ('abEamAssetDisposalConsoleTabs_update' === selectedTabName) {
                controller = selectedTab.getContentFrame().View.controllers.get('abEamDisposalUpdateCtrl');
            } else if ('abEamAssetDisposalConsoleTabs_eqSystemAnalysis' === selectedTabName) {
                controller = selectedTab.getContentFrame().View.controllers.get('eqSysAnalysisController');
            } else if ('abEamAssetDisposalConsoleTabs_reports' === selectedTabName) {
                controller = selectedTab.getContentFrame().View.controllers.get('exceptionReportController');
            } else {
                controller = selectedTab.getContentFrame().View.controllers.get('eqSysAssignmentController');
                if (controller) {
                    controller.refreshAllPanels = true;
                }
            }
            // set filter restriction if was applied from another tab
            if (controller) {
                controller.filterController.copyFilterRestriction(this.filterRestriction);
                controller.filterController.setCollapsed(!this.filterRestriction.isEmpty());
                controller.filterController.abEamAssetFilter_onFilter();
            }
        }
    }
});