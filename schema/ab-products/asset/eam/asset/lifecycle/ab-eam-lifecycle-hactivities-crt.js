/**
 * Asset history activities controller.
 */
View.createController('abEamLifecycleHActivitiesCrtController', {
    // Asset type
    assetType: null,
    // Asset id.
    assetId: null,

    afterViewLoad: function () {
        this.abEamLifecycleHActivitiesCrt_form.addEventListener('afterGetData', this.afterGetDataEvent, this);
        // overwrite onClickItem listener
        this.abEamLifecycleHActivitiesCrt_form.onClickItem = this.onClickItem.createDelegate(this, [this.abEamLifecycleHActivitiesCrt_form], true);
    },
    afterInitialDataFetch: function () {
        this.abEamLifecycleCrtActivitiesRefresh.refresh();
    },
    /**
     * Refresh current view.
     */
    refreshView: function () {
        if (valueExistsNotEmpty(this.assetType) && valueExistsNotEmpty(this.assetId)) {
            var restriction = new Ab.view.Restriction();
            if ('bl' === this.assetType) {
                restriction.addClause('activity_log_hactivity_log.bl_id', this.assetId, '=');
                restriction.addClause('activity_log_hactivity_log.bl_id', null, 'IS NOT NULL', 'AND', false);
            } else if ('property' === this.assetType) {
                restriction.addClause('activity_log_hactivity_log.pr_id', this.assetId, '=');
                restriction.addClause('activity_log_hactivity_log.pr_id', null, 'IS NOT NULL', 'AND', false);
            } else if ('eq' === this.assetType) {
                restriction.addClause('activity_log_hactivity_log.eq_id', this.assetId, '=');
                restriction.addClause('activity_log_hactivity_log.eq_id', null, 'IS NOT NULL', 'AND', false);
            } else if ('ta' === this.assetType) {
                restriction.addClause('activity_log_hactivity_log.ta_id', this.assetId, '=');
                restriction.addClause('activity_log_hactivity_log.ta_id', null, 'IS NOT NULL', 'AND', false);
            }
            this.abEamLifecycleHActivitiesCrt_form.addParameter('operator', getMessage('operator'));
            this.abEamLifecycleHActivitiesCrt_form.addParameter('hasActivityLogRestriction', true);
            this.abEamLifecycleHActivitiesCrt_form.addParameter('hasMovesRestriction', true);
            this.abEamLifecycleHActivitiesCrt_form.addParameter('hasSurveyRestriction', true);
            this.abEamLifecycleHActivitiesCrt_form.addParameter('hasWorkRequestRestriction', true);
            this.abEamLifecycleHActivitiesCrt_form.addParameter('hasWasteOutRestriction', true);
            this.abEamLifecycleHActivitiesCrt_form.refresh(restriction);
        }
    },
    /**
     * Set passed parameters.
     */
    setInputParameters: function () {
        var restriction = null;
        if (valueExists(this.view.restriction)) {
            restriction = this.view.restriction;
        }
        if (valueExists(this.view.getParentTab())
            && valueExists(this.view.getParentTab().restriction)) {
            restriction = this.view.getParentTab().restriction;
        }
        if (valueExists(this.view.getOpenerView())
            && valueExists(this.view.getOpenerView().restriction)) {
            restriction = this.view.getOpenerView().restriction;
        }
        if (valueExists(restriction)) {
            var typeClause = restriction.findClause('bl.asset_type');
            if (typeClause) {
                this.assetType = typeClause.value;
            }
            var idClause = restriction.findClause('bl.asset_id');
            if (idClause) {
                this.assetId = idClause.value;
            }
        }
    },
    abEamLifecycleCrtActivitiesRefresh_afterRefresh: function () {
        if (isHistoryActivitySchemaDefined) {
            this.setInputParameters();
            this.refreshView();
        } else {
            View.showMessage(getMessage('backwardCompatibilityClosedActivitiesMessage'));
        }
    },
    /**
     * Set report labels.
     * @param {Ab.view.CrossTable} panel Cross table report panel.
     * @param {Object} dataSet - panel dataSet.
     */
    afterGetDataEvent: function (panel, dataSet) {
        // replace '0' value with '*'
        var records = dataSet.records;
        _.each(records, function (record) {
            var localizedValues = record.localizedValues;
            _.each(localizedValues, function (value, key) {
                if ('0' === value) {
                    localizedValues[key] = '*';
                }
            });
        });
        // change activity type label
        for ( var i = 0; i < dataSet.rowValues.length; i++ ) {
            dataSet.rowValues[i].l = ((valueExistsNotEmpty(dataSet.rowValues[i].n) && "(no value)" !== dataSet.rowValues[i].n) ? getMessage('labelActivityType_' + dataSet.rowValues[i].n) : getMessage('labelActivityType_empty'));
        }
    },
    /**
     * Called when onClickItem is called.
     * @param {string} assetType bl|eq|ta|property
     * @param {string} assetId Asset id.
     * @param {string} transactionTypeName Transaction type name.
     * @param {string} [statusColumn] status_cancelled|status_completed|status_closed
     */
    onDrillDownEvent: function (assetType, assetId, transactionTypeName, statusColumn) {
        if (!valueExistsNotEmpty(transactionTypeName)) {
            transactionTypeName = 'other';
        }
        var transactionType = this.getTransactionType(transactionTypeName);
        var drillDownView = this.getDrillDownByTransactionType(transactionType, assetType);
        if (valueExistsNotEmpty(drillDownView)) {
            var restriction = this.getDrillDownRestriction(assetType, assetId, transactionTypeName, statusColumn);
            View.getOpenerView().openDialog(drillDownView, restriction, false, {
                width: 1024,
                height: 800,
                closeButton: true,
                afterViewLoad: function (dialogView) {
                    dialogView.panels.each(function (panel) {
                        if (panel.showOnLoad) {
                            panel.addParameter('applySharedRestriction', true);
                            panel.refresh(panel.restriction);
                        }
                    });
                }
            });
        }
    },
    getDrillDownByTransactionType: function (transactionType, assetType) {
        return transactionType.drillDown[assetType];
    },
    getTransactionType: function (transactionTypeName) {
        return _.find(this.transactionType, function (transaction, key) {
            return key === transactionTypeName;
        });
    },
    /**
     * Get drill down restriction
     * @param {string} assetType Asset type.
     * @param {string} assetId Asset id.
     * @param {string} transactionType Transaction type.
     * @param {string} statusColumn Status column.
     * @returns {Ab.view.Restriction} restriction.
     */
    getDrillDownRestriction: function (assetType, assetId, transactionType, statusColumn) {
        var fieldNameByType = {'bl': 'bl_id', 'property': 'pr_id', 'eq': 'eq_id', 'ta': 'ta_id'};
        var restriction = new Ab.view.Restriction();
        switch (transactionType) {
            case 'project_action':
            case 'assessment':
            case 'sustainability_assessment':
            case 'commissioning':
            case 'clean_bl':
            case 'other':
                restriction.addClause('activity_log_hactivity_log.' + fieldNameByType[assetType], assetId, '=');
                if (valueExists(statusColumn)) {
                    this.appendStatusRestriction(transactionType, assetType, statusColumn, restriction);
                } else {
                    restriction.addClause('activity_log_hactivity_log.status', ['COMPLETED', 'COMPLETED-V', 'CLOSED', 'CANCELLED'], 'IN');
                }
                this.appendOptionalRestriction(transactionType, restriction);
                break;
            case 'sd_furniture':
                restriction.addClause('activity_log_hactivity_log.' + fieldNameByType[assetType], assetId, '=');
                restriction.addClause('activity_log_hactivity_log.activity_type', 'SERVICE DESK - FURNITURE', '=');
                this.appendStatusRestriction(transactionType, assetType, statusColumn, restriction);
                this.appendOptionalRestriction(transactionType, restriction);
                break;
            case 'sd_group_move':
                restriction.addClause('activity_log_hactivity_log.' + fieldNameByType[assetType], assetId, '=');
                restriction.addClause('activity_log_hactivity_log.activity_type', 'SERVICE DESK - GROUP MOVE', '=');
                this.appendStatusRestriction(transactionType, assetType, statusColumn, restriction);
                this.appendOptionalRestriction(transactionType, restriction);
                break;
            case 'sd_individual_move':
                restriction.addClause('activity_log_hactivity_log.' + fieldNameByType[assetType], assetId, '=');
                restriction.addClause('activity_log_hactivity_log.activity_type', 'SERVICE DESK - INDIVIDUAL MOVE', '=');
                this.appendStatusRestriction(transactionType, assetType, statusColumn, restriction);
                this.appendOptionalRestriction(transactionType, restriction);
                break;
            case 'compliance':
                restriction.addClause('compliance_locations.' + fieldNameByType[assetType], assetId, '=');
                this.appendStatusRestriction(transactionType, assetType, statusColumn, restriction);
                this.appendOptionalRestriction(transactionType, restriction);
                break;
            case 'work_request':
                restriction.addClause('wrhwr.' + fieldNameByType[assetType], assetId, '=');
                this.appendStatusRestriction(transactionType, assetType, statusColumn, restriction);
                this.appendOptionalRestriction(transactionType, restriction);
                break;
            case 'moves':
                restriction.addClause('mo_' + assetType + '.' + fieldNameByType[assetType], assetId, '=');
                this.appendStatusRestriction(transactionType, assetType, statusColumn, restriction);
                this.appendOptionalRestriction(transactionType, restriction);
                break;
            case 'survey':
                restriction.addClause(assetType + '_audit.' + fieldNameByType[assetType], assetId, '=');
                this.appendStatusRestriction(transactionType, assetType, statusColumn, restriction);
                this.appendOptionalRestriction(transactionType, restriction);
                break;
            case 'waste_out':
                restriction.addClause('waste_out.' + fieldNameByType[assetType], assetId, '=');
                restriction.addClause('waste_out.status', ['D'], 'IN');
                this.appendOptionalRestriction(transactionType, restriction);
                break;
        }
        return restriction;
    },
    /**
     * Appends restriction for status column.
     * @param {string} transactionType Transaction type.
     * @param {string} assetType Asset type.
     * @param {string} statusColumn Status column.
     * @param {Ab.view.Restriction} restriction Restriction to append to.
     */
    appendStatusRestriction: function (transactionType, assetType, statusColumn, restriction) {
        switch (transactionType) {
            case 'project_action':
            case 'assessment':
            case 'sustainability_assessment':
            case 'commissioning':
            case 'clean_bl':
            case 'other':
            case 'sd_furniture':
            case 'sd_group_move':
            case 'sd_individual_move':
            case 'compliance':
                if (valueExists(statusColumn)) {
                    if ('status_cancelled' === statusColumn) {
                        restriction.addClause('activity_log_hactivity_log.status', ['CANCELLED'], 'IN');
                    } else if ('status_completed' === statusColumn) {
                        restriction.addClause('activity_log_hactivity_log.status', ['COMPLETED','COMPLETED-V'], 'IN');
                    } else if ('status_closed' === statusColumn) {
                        restriction.addClause('activity_log_hactivity_log.status', ['CLOSED'], 'IN');
                    }
                } else {
                    restriction.addClause('activity_log_hactivity_log.status', ['COMPLETED', 'COMPLETED-V', 'CLOSED', 'CANCELLED'], 'IN');
                }
                break;
            case 'work_request':
                if (valueExists(statusColumn)) {
                    if ('status_cancelled' === statusColumn) {
                        restriction.addClause('wrhwr.status', ['Can', 'Rej', 'S'], 'IN');
                    } else if ('status_completed' === statusColumn) {
                        restriction.addClause('wrhwr.status', ['Com'], 'IN');
                    } else if ('status_closed' === statusColumn) {
                        restriction.addClause('wrhwr.status', ['Clo'], 'IN');
                    }
                } else {
                    restriction.addClause('wrhwr.status', ['Com', 'Clo', 'Rej', 'S', 'Can'], 'IN');
                }
                break;
            case 'moves':
                if ('status_cancelled' === statusColumn) {
                    restriction.addClause('mo_' + assetType + '.status', ['can'], 'IN');
                } else if ('status_closed' === statusColumn) {
                    restriction.addClause('mo_' + assetType + '.status', ['mov'], 'IN');
                }
                break;
            case 'survey':
                if ('status_completed' === statusColumn) {
                    restriction.addClause('survey.status', ['Completed'], 'IN');
                } else if ('status_closed' === statusColumn) {
                    restriction.addClause('survey.status', ['Closed'], 'IN');
                }
                break;
        }
    },
    /**
     * Appends optional restriction for activity type.
     * @param {string} transactionType Transaction type.
     * @param {Ab.view.Restriction} restriction Restriction to append to.
     */
    appendOptionalRestriction: function (transactionType, restriction) {
        var sharedDataSource = getMessage('sharedDataSource');
        switch (transactionType) {
            case 'project_action':
            case 'assessment':
            case 'sustainability_assessment':
            case 'commissioning':
            case 'clean_bl':
            case 'other':
            case 'sd_furniture':
            case 'sd_group_move':
            case 'sd_individual_move':
            case 'compliance':
                restriction.sql = "${sql.getRestriction('ab-eam-lifecycle-hactivities-shared-datasources.axvw', '" + sharedDataSource + "', 'activityLogRestriction')}";
                break;
            case 'work_request':
                restriction.sql = "${sql.getRestriction('ab-eam-lifecycle-hactivities-shared-datasources.axvw', '" + sharedDataSource + "', 'workRequestRestriction')}";
                break;
            case 'moves':
                restriction.sql = "${sql.getRestriction('ab-eam-lifecycle-hactivities-shared-datasources.axvw', '" + sharedDataSource + "', 'movesRestriction')}";
                break;
            case 'survey':
                restriction.sql = "${sql.getRestriction('ab-eam-lifecycle-hactivities-shared-datasources.axvw', '" + sharedDataSource + "', 'surveyRestriction')}";
                break;
            case 'waste_out':
                restriction.sql = "${sql.getRestriction('ab-eam-lifecycle-hactivities-shared-datasources.axvw', '" + sharedDataSource + "', 'wasteOutRestriction')}";
                break;
        }
    },
    enableSharedDataSourceParameters: function (sharedDataSource) {
        var panel = View.panels.get(sharedDataSource + '_panel');
        panel.addParameter('hasActivityLogRestriction', true);
        panel.addParameter('hasMovesRestriction', true);
        panel.addParameter('hasSurveyRestriction', true);
        panel.addParameter('hasWorkRequestRestriction', true);
        panel.addParameter('hasWasteOutRestriction', true);
        panel.refresh();
    },
    /**
     * On drill down event
     * @param {string} id command context.
     * @param {Ab.view.CrossTable} panel crossTable panel
     */
    onClickItem: function (id, panel) {
        var restriction = panel.getRestrictionFromId(id);
        var activityTypeClause = restriction.findClause('activity_log_hactivity_log.activity_type');
        if (activityTypeClause) {
            var transactionTypeName = activityTypeClause.value;
            var columnId = id.split(';')[0];
            var parsedId = panel.parsedClickableId(id);
            if (parsedId.indexOf('_row') === 0) {
                columnId = panel.groupByFields[0].id;
            }
            var statusColumn = this.getColumnStatus(columnId);
            var hasRecords = $(id).text !== '*';
            if (hasRecords) {
                this.onDrillDownEvent(this.assetType, this.assetId, transactionTypeName, statusColumn);
            }
        }
    },
    /**
     * Return column status id.
     * @param {string} columnId
     * @returns {string} Status column.
     */
    getColumnStatus: function (columnId) {
        var statusColumn = null;
        if (valueExists(columnId) && columnId.indexOf('status_') > 0) {
            statusColumn = columnId.split('activity_log_hactivity_log.')[1];
        }
        return statusColumn;
    },
    transactionType: {
        'project_action': {
            drillDown: {
                'bl': 'ab-eam-lifecycle-proj-action-history.axvw',
                'eq': 'ab-eam-lifecycle-proj-action-history.axvw',
                'ta': 'ab-eam-lifecycle-proj-action-history.axvw',
                'property': 'ab-eam-lifecycle-proj-action-history.axvw'
            },
        },
        'assessment': {
            drillDown: {
                'bl': 'ab-eam-lifecycle-assessment-history.axvw',
                'eq': 'ab-eam-lifecycle-assessment-history.axvw',
                'ta': 'ab-eam-lifecycle-assessment-history.axvw',
                'property': 'ab-eam-lifecycle-assessment-history.axvw'
            }
        },
        'sustainability_assessment': {
            drillDown: {
                'bl': 'ab-eam-lifecycle-sust-assessment-history.axvw',
                'eq': 'ab-eam-lifecycle-sust-assessment-history.axvw',
                'ta': 'ab-eam-lifecycle-sust-assessment-history.axvw',
                'property': 'ab-eam-lifecycle-sust-assessment-history.axvw'
            }
        },
        'commissioning': {
            drillDown: {
                'bl': 'ab-eam-lifecycle-commissioning-history.axvw',
                'eq': 'ab-eam-lifecycle-commissioning-history.axvw',
                'ta': 'ab-eam-lifecycle-commissioning-history.axvw',
                'property': 'ab-eam-lifecycle-commissioning-history.axvw'
            }

        },
        'clean_bl': {
            drillDown: {
                'bl': 'ab-eam-lifecycle-clean-building-history.axvw',
                'eq': 'ab-eam-lifecycle-clean-building-history.axvw',
                'ta': 'ab-eam-lifecycle-clean-building-history.axvw',
                'property': 'ab-eam-lifecycle-clean-building-history.axvw'
            }
        },
        'other': {
            drillDown: {
                'bl': 'ab-eam-lifecycle-other-action-history.axvw',
                'eq': 'ab-eam-lifecycle-other-action-history.axvw',
                'ta': 'ab-eam-lifecycle-other-action-history.axvw',
                'property': 'ab-eam-lifecycle-other-action-history.axvw'
            }
        },
        'sd_furniture': {
            drillDown: {
                'bl': 'ab-eam-lifecycle-sd-action-history.axvw',
                'eq': 'ab-eam-lifecycle-sd-action-history.axvw',
                'ta': 'ab-eam-lifecycle-sd-action-history.axvw',
                'property': 'ab-eam-lifecycle-sd-action-history.axvw'
            }
        },
        'sd_group_move': {
            drillDown: {
                'bl': 'ab-eam-lifecycle-sd-action-history.axvw',
                'eq': 'ab-eam-lifecycle-sd-action-history.axvw',
                'ta': 'ab-eam-lifecycle-sd-action-history.axvw',
                'property': 'ab-eam-lifecycle-sd-action-history.axvw'
            }
        },
        'sd_individual_move': {
            drillDown: {
                'bl': 'ab-eam-lifecycle-sd-action-history.axvw',
                'eq': 'ab-eam-lifecycle-sd-action-history.axvw',
                'ta': 'ab-eam-lifecycle-sd-action-history.axvw',
                'property': 'ab-eam-lifecycle-sd-action-history.axvw'
            }
        },
        'compliance': {
            drillDown: {
                'bl': 'ab-eam-lifecycle-compliance-history.axvw',
                'eq': 'ab-eam-lifecycle-compliance-history.axvw',
                'ta': '',
                'property': 'ab-eam-lifecycle-compliance-history.axvw',
            }
        },
        'work_request': {
            drillDown: {
                'bl': 'ab-eam-lifecycle-wr.axvw',
                'eq': 'ab-eam-lifecycle-wr.axvw',
                'ta': '',
                'property': ''
            }
        },
        'moves': {
            drillDown: {
                'bl': '',
                'eq': 'ab-eam-lifecycle-mo-eq.axvw',
                'ta': 'ab-eam-lifecycle-mo-ta.axvw',
                'property': ''
            }
        },
        'survey': {
            drillDown: {
                'bl': '',
                'eq': 'ab-eam-lifecycle-eq-audit.axvw',
                'ta': 'ab-eam-lifecycle-ta-audit.axvw',
                'property': ''
            }
        },
        'waste_out': {
            drillDown: {
                'bl': 'ab-eam-lifecycle-waste-out.axvw',
                'eq': 'ab-eam-lifecycle-waste-out.axvw',
                'ta': '',
                'property': 'ab-eam-lifecycle-waste-out.axvw'
            }
        }
    }
});