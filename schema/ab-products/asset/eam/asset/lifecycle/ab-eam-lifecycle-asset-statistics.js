/**
 * Asset statistics controller.
 */
View.createController('assetStatisticsController', {
    hideConditionLegend: true,
    hideStatusLegend: true,
    filterRestriction: null,
    afterViewLoad: function () {
        // set chart label text
        this.eqByCondition_chart.chartControl.chart.canvas.labelText = "[[title]]";
        this.eqByStatus_chart.chartControl.chart.canvas.labelText = "[[title]]";
    },
    afterInitialDataFetch: function () {
        // add statistics panel tooltip
        this.addTooltip('statisticsPanel', 'eq.vf_cost_life_expct');
        if (valueExists(View.getOpenerView()) && View.getOpenerView().controllers.get('abEamAssetOptimizationController')) {
            // if asset statistics panel is used as a collapsible panel, refresh the panel with filter values and location tree selection
            var controller = View.getOpenerView().controllers.get('abEamAssetOptimizationController');
            var filterRestriction = controller.filterRestriction;
            if (filterRestriction) {
                this.filter(filterRestriction, controller.selectedTreeNode);
                // hide legend on load
                this.eqByCondition_chart.chartControl.showLegend(!this.hideConditionLegend);
                this.eqByStatus_chart.chartControl.showLegend(!this.hideStatusLegend);
            }
        }
    },
    /**
     * Show/hide legend panel.
     */
    eqByCondition_chart_onShowHideLegend: function () {
        this.hideConditionLegend = !this.hideConditionLegend;
        this.eqByCondition_chart.chartControl.showLegend(!this.hideConditionLegend);
        this.eqByCondition_chart.syncHeight();
    },
    /**
     * Show/hide legend panel.
     */
    eqByStatus_chart_onShowHideLegend: function () {
        this.hideStatusLegend = !this.hideStatusLegend;
        this.eqByStatus_chart.chartControl.showLegend(!this.hideStatusLegend);
        this.eqByStatus_chart.syncHeight();
    },
    /**
     * Show asset statistics details.
     * @param {Ab.view.Restriction} restriction - chart clicked object restriction
     * @param title - details panel title to set
     */
    showDetails: function (restriction, title) {
        var opener = View;
        if (!valueExists(View.getOpenerView().dialog)) {
            opener = View.getOpenerView();
        }
        var consoleRestriction = this.filterRestriction;
        opener.openDialog('ab-eam-lifecycle-asset-statistics-details.axvw', null, false, {
            width: 1200,
            height: 600,
            closeButton: true,
            afterInitialDataFetch: function (dialogView) {
                var detailPanel = dialogView.panels.get('assetStatisticsDetailsPanel');
                detailPanel.addParameter('filterRestriction', consoleRestriction || '1=1');
                detailPanel.refresh(restriction);
                detailPanel.setTitle(title);
            }
        });
    },
    /**
     * Apply filter and tree selection restriction.
     * @param {Ab.view.Restriction}filterRestriction - Filter restriction.
     * @param {Object} selectedTreeNode - Selected tree node restriction.
     */
    filter: function (filterRestriction, selectedTreeNode) {
        if (valueExistsNotEmpty(filterRestriction)) {
            var restriction = this.getSqlRestriction(filterRestriction);
            this.filterRestriction = restriction;
        }
        if (selectedTreeNode && selectedTreeNode.treeType) {
            var nodeRestriction = this.getTreeRestriction(selectedTreeNode);
            if (nodeRestriction && nodeRestriction.restriction) {
                var restriction = this.getSqlRestriction(nodeRestriction.restriction);
                this.filterRestriction += (valueExistsNotEmpty(this.filterRestriction) ? " AND " : "") + restriction;
            }
            if (nodeRestriction && nodeRestriction.sql) {
                this.filterRestriction += nodeRestriction.sql;
            }
        }
        this.refreshStatisticsPanels();
    },
    /**
     * Refresh all statistics panels and apply filter restriction.
     */
    refreshStatisticsPanels: function () {
        this.statisticsPanel.addParameter('filterRestriction', this.filterRestriction || '1=1');
        this.eqByCondition_chart.addParameter('filterRestriction', this.filterRestriction || '1=1');
        this.eqByStatus_chart.addParameter('filterRestriction', this.filterRestriction || '1=1');
        this.statisticsPanel.restriction = null;
        this.eqByCondition_chart.restriction = null;
        this.eqByStatus_chart.restriction = null;
        this.statisticsPanel.refresh();
        this.eqByCondition_chart.refresh();
        this.eqByStatus_chart.refresh();
    },
    /**
     * Add tooltip on form field when field is readonly.
     * @param {string} panelName Panel name.
     * @param {string} fieldName Field name.
     * @param {string}[tooltip] Optional, if not defined, the tooltip defined in the view form will be displayed.
     */
    addTooltip: function (panelName, fieldName, tooltip) {
        var imgElement = document.createElement("img");
        imgElement.id = panelName + "_" + fieldName;
        imgElement.src = this.view.contextPath + "/schema/ab-core/graphics/icons/help.png";
        imgElement.className = 'fieldTooltipButton';
        var tooltipTag = (Ext.isIE) ? 'title' : 'ext:qtip';
        tooltip = tooltip || View.panels.get(panelName).fields.get(fieldName).fieldDef.tooltip;
        imgElement.setAttribute(tooltipTag, tooltip);
        Ext.get(imgElement).on("click", function () {
            View.alert(tooltip);
        });
        var parent = View.panels.get(panelName).getFieldCell(fieldName);
        parent.appendChild(imgElement);
    },
    /**
     * Get SQL restriction based on console filter and selected asset node from tree panels.
     * @param {Ab.view.Restriction} filterRestriction - filter restriction
     * @returns {string} - sql restriction
     */
    getSqlRestriction: function (filterRestriction) {
        var restriction = "";
        var clause = filterRestriction.findClause('bl.asset_id');
        if (clause) {
            restriction += " eq.eq_id " + getSqlClauseValue(clause.op, clause.value);
        }
        clause = filterRestriction.findClause('bl.ctry_id');
        if (clause) {
            restriction += (valueExistsNotEmpty(restriction) ? " AND " : "") + " (EXISTS(SELECT 1 FROM bl WHERE bl.bl_id=eq.bl_id AND bl.ctry_id " + getSqlClauseValue(clause.op, clause.value) + " ))";
        }
        clause = filterRestriction.findClause('bl.state_id');
        if (clause) {
            restriction += (valueExistsNotEmpty(restriction) ? " AND " : "") + " (EXISTS(SELECT 1 FROM bl WHERE bl.bl_id=eq.bl_id AND bl.state_id " + getSqlClauseValue(clause.op, clause.value) + " ))";
        }
        clause = filterRestriction.findClause('bl.city_id');
        if (clause) {
            restriction += (valueExistsNotEmpty(restriction) ? " AND " : "") + " (EXISTS(SELECT 1 FROM bl WHERE bl.bl_id=eq.bl_id AND bl.city_id " + getSqlClauseValue(clause.op, clause.value) + " ))";
        }
        clause = filterRestriction.findClause('bl.site_id');
        if (clause) {
            restriction += (valueExistsNotEmpty(restriction) ? " AND " : "") + " eq.site_id " + getSqlClauseValue(clause.op, clause.value);
        }
        clause = filterRestriction.findClause('bl.bl_id');
        if (clause) {
            restriction += (valueExistsNotEmpty(restriction) ? " AND " : "") + " eq.bl_id " + getSqlClauseValue(clause.op, clause.value);
        }
        clause = filterRestriction.findClause('bl.fl_id');
        if (clause) {
            restriction += (valueExistsNotEmpty(restriction) ? " AND " : "") + " eq.fl_id " + getSqlClauseValue(clause.op, clause.value);
        }
        clause = filterRestriction.findClause('bl.rm_id');
        if (clause) {
            restriction += (valueExistsNotEmpty(restriction) ? " AND " : "") + " eq.rm_id " + getSqlClauseValue(clause.op, clause.value);
        }
        clause = filterRestriction.findClause('bl.project_id');
        if (clause) {
            restriction += (valueExistsNotEmpty(restriction) ? " AND " : "") +
                "EXISTS(SELECT activity_log.activity_log_id FROM activity_log WHERE activity_log.eq_id = eq.eq_id AND activity_log.project_id " + getSqlClauseValue(clause.op, clause.value) + ")";
        }
        clause = filterRestriction.findClause('bl.asset_type');
        if (clause && 'eq' === clause.value) {
            clause = filterRestriction.findClause('bl.asset_std');
            if (clause) {
                restriction += (valueExistsNotEmpty(restriction) ? " AND " : "") + " eq.eq_std " + getSqlClauseValue(clause.op, clause.value);
            }
            clause = filterRestriction.findClause('bl.asset_id');
            if (clause) {
                restriction += (valueExistsNotEmpty(restriction) ? " AND " : "") + " eq.eq_id " + getSqlClauseValue(clause.op, clause.value);
            }
            clause = filterRestriction.findClause('bl.asset_status');
            if (clause) {
                restriction += (valueExistsNotEmpty(restriction) ? " AND " : "") + " eq.status " + getSqlClauseValue(clause.op, clause.value);
            }
        }
        clause = filterRestriction.findClause('custodian');
        if (clause) {
            var assignedAssets = filterRestriction.custodian.assignedAssets;
            var custodianValues = assignedAssets['eq'];
            if (!_.isEmpty(custodianValues)) {
                restriction += (valueExistsNotEmpty(restriction) ? " AND " : "") + " eq.eq_id IN ('" + makeSafeSqlValue(custodianValues).join("', '") + "')";
            } else {
                restriction += (valueExistsNotEmpty(restriction) ? " AND " : "") + " 1=2 ";
            }
        }
        if (!_.isEmpty(filterRestriction.system)) {
            restriction += (valueExistsNotEmpty(restriction) ? " AND " : "") + " eq.eq_id IN ('" + makeSafeSqlValue(filterRestriction.system).join("', '") + "')";
        }
        return restriction;
    },
    /**
     *Get restriction object.
     * @returns {Object} - {'restriction': objRestriction, 'sql': sqlRestriction}
     */
    getTreeRestriction: function (selectedTreeNode) {
        var treeType = selectedTreeNode.treeType,
            nodeRestriction = selectedTreeNode.nodeRestriction;
        var objRestriction = new Ab.view.Restriction();
        var sqlRestriction = null;
        if ('geographical' === treeType || 'location' === treeType) {
            for ( var i = 0; i < nodeRestriction.clauses.length; i++ ) {
                var tmpObjClause = nodeRestriction.clauses[i];
                if ('geo_region.geo_region_id' === tmpObjClause.name) {
                    sqlRestriction = "(EXISTS (SELECT 1 FROM ctry, bl WHERE ctry.ctry_id = bl.ctry_id AND eq.bl_id = bl.bl_id AND ctry.geo_region_id" + getSqlClauseValue(tmpObjClause.op, tmpObjClause.value) + "))";
                } else {
                    var clauseName = 'bl' + tmpObjClause.name.substring(tmpObjClause.name.indexOf('.'));
                    objRestriction.addClause(clauseName, tmpObjClause.value, tmpObjClause.op, tmpObjClause.relOp);
                }
            }
        } else if ('organization' === treeType) {
            var genSql = "(1=1 ";
            for ( var i = 0; i < nodeRestriction.clauses.length; i++ ) {
                var tmpObjClause = nodeRestriction.clauses[i];
                if ('bu.bu_id' === tmpObjClause.name) {
                    genSql += " AND EXISTS(SELECT dv.dv_id FROM dv WHERE dv.dv_id = eq.dv_id AND dv.bu_id " + getSqlClauseValue(tmpObjClause.op, tmpObjClause.value) + " )";
                } else {
                    genSql += " AND eq" + tmpObjClause.name.substring(tmpObjClause.name.indexOf('.')) + " " + getSqlClauseValue(tmpObjClause.op, tmpObjClause.value);
                }
            }
            genSql += ")";
            if (nodeRestriction.clauses.length > 0) {
                sqlRestriction = "(" + genSql + ")";
            }
        } else if ('project' === treeType) {
            //restrict using activity_log table
            sqlRestriction = "EXISTS(SELECT activity_log.activity_log_id FROM activity_log WHERE activity_log.eq_id = eq.eq_id  AND"
            sqlRestriction += " EXISTS(SELECT project.project_id FROM project WHERE project.is_template = 0 AND project.project_id = activity_log.project_id";
            var activityLogStr = "";
            for ( var i = 0; i < nodeRestriction.clauses.length; i++ ) {
                var tmpObjClause = nodeRestriction.clauses[i];
                if ('activity_log.activity_log_id' === tmpObjClause.name) {
                    activityLogStr += tmpObjClause.name + " " + getSqlClauseValue(tmpObjClause.op, tmpObjClause.value);
                } else if ('work_pkgs.work_pkg_id' === tmpObjClause.name) {
                    sqlRestriction += " AND EXISTS(SELECT work_pkgs.work_pkg_id FROM work_pkgs WHERE work_pkgs.project_id = project.project_id";
                    sqlRestriction += " AND " + tmpObjClause.name + " " + getSqlClauseValue(tmpObjClause.op, tmpObjClause.value) + ")";
                } else {
                    sqlRestriction += " AND project" + tmpObjClause.name.substring(tmpObjClause.name.indexOf('.')) + " " + getSqlClauseValue(tmpObjClause.op, tmpObjClause.value);
                }
            }
            sqlRestriction += ")";
            if (valueExistsNotEmpty(activityLogStr)) {
                sqlRestriction += "AND " + activityLogStr;
            }
            sqlRestriction += ")";
        } else if ('systems' === treeType) {
            var clause = nodeRestriction.findClause('eq.eq_id');
            sqlRestriction = " eq.eq_id " + getSqlClauseValue(clause.op, clause.value);
        }
        return {'restriction': objRestriction, 'sql': sqlRestriction};
    },
    /**
     * On maximize panel.
     */
    statisticsPanel_onMaximize: function () {
        var filterRestriction = this.filterRestriction;
        View.getOpenerView().openDialog('ab-eam-lifecycle-asset-statistics.axvw', null, false, {
            maximize: true,
            afterInitialDataFetch: function (dialogView) {
                dialogView.panels.get('statisticsPanel').actions.get('maximize').show(false);
                var controller = dialogView.controllers.get('assetStatisticsController');
                controller.filterRestriction = filterRestriction;
                controller.refreshStatisticsPanels();
            }
        });
    }
});

/**
 * On click chart object.
 * @param {Object} obj
 */
function showAssetConditionDetails(obj) {
    var condition = obj.selectedChartData['eq.condition'];
    var enumValues = View.dataSources.get(obj.chart.dataSourceId).fieldDefs.get('eq.condition').enumValues;
    var localizedValue = enumValues[condition];
    var title = getMessage('detailsConditionTitle') + ': ' + localizedValue;
    View.controllers.get('assetStatisticsController').showDetails(obj.restriction, title);
}

/**
 * On click chart object.
 * @param {Object} obj
 */
function showAssetStatusDetails(obj) {
    var status = obj.selectedChartData['eq.status'];
    var enumValues = View.dataSources.get(obj.chart.dataSourceId).fieldDefs.get('eq.status').enumValues;
    var localizedValue = enumValues[status];
    var title = getMessage('detailsStatusTitle') + ': ' + localizedValue;
    View.controllers.get('assetStatisticsController').showDetails(obj.restriction, title);
}