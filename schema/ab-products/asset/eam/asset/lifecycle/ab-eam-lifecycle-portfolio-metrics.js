View.createController('portfolioMetricController', {
    financialMetrics: ['eam_roce_all_monthly', 'eam_rona_all_monthly', 'eam_fixed_asset_turnover_all_monthly'],
    riskMetrics: ['eam_CurrentReplacementValueIndex_Portfolio_percent_monthly', 'ops_FacilityConditionIndex_percent_monthly', 'eam_equipment_utilization_percent_annually'],
    afterInitialDataFetch: function () {
        if (valueExists(View.parameters) && valueExists(View.parameters.maximizePanelId)) {
            var panelId = View.parameters.maximizePanelId;
            View.panels.get(panelId).actions.get('maximize').show(false);
            if ('metricsFinancialPanel' === panelId) {
                this.financialMetrics = View.parameters.financialMetrics;
                this.showFinancialPanel();
            } else if ('metricsRiskPanel' === panelId) {
                this.riskMetrics = View.parameters.riskMetrics;
                this.showRiskPanel();
                var layout = View.getLayoutManager('mainLayout');
                layout.collapseRegion('north');
            }
        } else {
            this.showFinancialPanel();
            this.showRiskPanel();
        }
    },
    showFinancialPanel: function () {
    	this.metricsFinancialPanel.restriction = null;
        this.metricsFinancialPanel.addParameter('metricRestriction', this.createMetricRestriction(this.financialMetrics));
        this.metricsFinancialPanel.refresh();
    },
    showRiskPanel: function () {
    	this.metricsRiskPanel.restriction = null;
        this.metricsRiskPanel.addParameter('metricRestriction', this.createMetricRestriction(this.riskMetrics));
        this.metricsRiskPanel.refresh();
    },
    metricsFinancialPanel_onMaximize: function () {
        this.maximize('metricsFinancialPanel');
    },
    metricsRiskPanel_onMaximize: function () {
        this.maximize('metricsRiskPanel');
    },
    maximize: function (panelId) {
        View.getOpenerView().openDialog('ab-eam-lifecycle-portfolio-metrics.axvw', null, false, {
            maximize: true,
            maximizePanelId: panelId,
            financialMetrics: this.financialMetrics,
            riskMetrics: this.riskMetrics
        });
    },
    selectMetrics: function (panelId) {
        var metricPanel = View.panels.get(panelId);
        var availableMetrics = [];
        metricPanel.gridRows.each(function (row) {
            availableMetrics.push(row.getRecord().getValue('afm_metric_definitions.metric_name'));
        });
        var metricRestriction = ('metricsFinancialPanel' === panelId) ? " afm_metric_definitions.metric_name LIKE ('eam_%')" : "(afm_metric_definitions.metric_name LIKE ('eam_%') OR afm_metric_definitions.metric_name LIKE ('ops_%'))";
        View.getOpenerView().openDialog('ab-eam-lifecycle-portfolio-metrics-select.axvw', null, true, {
            availableMetrics: availableMetrics,
            metricRestriction: metricRestriction,
            panelId: panelId,
            callback: function (panelId, availableMetrics) {
                View.controllers.get('portfolioMetricController').refreshMetricPanel(panelId, availableMetrics);
            }
        });
    },
    refreshMetricPanel: function (panelId, availableMetrics) {
        if ('metricsFinancialPanel' === panelId) {
            this.financialMetrics = availableMetrics;
        } else {
            this.riskMetrics = availableMetrics;
        }
        var panel = View.panels.get(panelId);
        var metricRestriction = this.createMetricRestriction(availableMetrics);
        panel.addParameter('metricRestriction', metricRestriction);
        panel.refresh();
    },
    createMetricRestriction: function (availableMetrics) {
        return "metric_name IN ('" + availableMetrics.join("','") + "')";
    }
});