/**
 * Asset current activities controller.
 */
View.createController('abEamLifecycleActivitiesCrtController', {
    // Asset type
    assetType: null,
    // Asset id.
    assetId: null,
    afterViewLoad: function () {
        this.abEamLifecycleActivitiesCrt_form.addEventListener('afterGetData', this.afterGetDataEvent, this);
        // overwrite onClickItem listener
        this.abEamLifecycleActivitiesCrt_form.onClickItem = this.onClickItem.createDelegate(this, [this.abEamLifecycleActivitiesCrt_form], true);
    },
    afterInitialDataFetch: function () {
        this.abEamLifecycleCrtActivitiesRefresh.refresh();
    },
    /**
     * Refresh current view.
     */
    refreshView: function () {
        if (valueExistsNotEmpty(this.assetType) && valueExistsNotEmpty(this.assetId)) {
            var restriction = new Ab.view.Restriction();
            if ('bl' === this.assetType) {
                restriction.addClause('activity_log.bl_id', this.assetId, '=');
                restriction.addClause('activity_log.bl_id', null, 'IS NOT NULL', 'AND', false);
            } else if ('property' === this.assetType) {
                restriction.addClause('activity_log.pr_id', this.assetId, '=');
                restriction.addClause('activity_log.pr_id', null, 'IS NOT NULL', 'AND', false);
            } else if ('eq' === this.assetType) {
                restriction.addClause('activity_log.eq_id', this.assetId, '=');
                restriction.addClause('activity_log.eq_id', null, 'IS NOT NULL', 'AND', false);
            } else if ('ta' === this.assetType) {
                restriction.addClause('activity_log.ta_id', this.assetId, '=');
                restriction.addClause('activity_log.ta_id', null, 'IS NOT NULL', 'AND', false);
            }
            this.abEamLifecycleActivitiesCrt_form.refresh(restriction);
            // if called from ab-eam-disposal-asset-summary.axvw
            if (View.getOpenerView().hasTitle()) {
            	View.getOpenerView().setTitle(String.format(getMessage('titleAssetSummary_' + this.assetType), this.assetId));
            }
        }
    },
    /**
     * Set passed parameters.
     */
    setInputParameters: function () {
        var restriction = null;
        if (valueExists(this.view.restriction)) {
            restriction = this.view.restriction;
        } else if (valueExists(this.view.getParentTab())
            && valueExists(this.view.getParentTab().restriction)) {
            restriction = this.view.getParentTab().restriction;
        } else if (valueExists(this.view.getOpenerView())
            && valueExists(this.view.getOpenerView().restriction)) {
            restriction = this.view.getOpenerView().restriction;
        }
        if (valueExists(restriction)) {
            var typeClause = restriction.findClause('bl.asset_type');
            if (typeClause) {
                this.assetType = typeClause.value;
            }
            var idClause = restriction.findClause('bl.asset_id');
            if (idClause) {
                this.assetId = idClause.value;
            }
        }
    },
    abEamLifecycleCrtActivitiesRefresh_afterRefresh: function () {
        this.setInputParameters();
        this.refreshView();
    },
    /**
     * Set report labels.
     * @param {Ab.view.CrossTable} panel Cross table report panel.
     * @param {Object} dataSet - panel dataSet.
     */
    afterGetDataEvent: function (panel, dataSet) {
        // replace '0' value with '*'
        var records = dataSet.records;
        _.each(records, function (record) {
            var localizedValues = record.localizedValues;
            _.each(localizedValues, function (value, key) {
                if ('0' === value) {
                    localizedValues[key] = '*';
                }
            });
        });
        // change activity type label
        for ( var i = 0; i < dataSet.rowValues.length; i++ ) {
            dataSet.rowValues[i].l = ((valueExistsNotEmpty(dataSet.rowValues[i].n) && "(no value)" !== dataSet.rowValues[i].n) ? getMessage('labelActivityType_' + dataSet.rowValues[i].n) : getMessage('labelActivityType_empty'));
        }
    },
    /**
     * Called when onClickItem is called.
     * @param {string} assetType bl|eq|ta|property
     * @param {string} assetId Asset id.
     * @param {string} activityType Activity type.
     * @param {string} [statusColumn] status_created|status_requested|status_on_hold|status_approved|status_issued|status_rejected
     */
    onDrillDownEvent: function (assetType, assetId, activityType, statusColumn) {
        var drillDownByActivityType = {
            'project_action': {'bl': 'ab-eam-lifecycle-proj-action.axvw', 'property': 'ab-eam-lifecycle-proj-action.axvw', 'eq': 'ab-eam-lifecycle-proj-action.axvw', 'ta': 'ab-eam-lifecycle-proj-action.axvw'},
            'assessment': {'bl': 'ab-eam-lifecycle-assessment.axvw', 'property': 'ab-eam-lifecycle-assessment.axvw', 'eq': 'ab-eam-lifecycle-assessment.axvw', 'ta': 'ab-eam-lifecycle-assessment.axvw'},
            'sustainability_assessment': {'bl': 'ab-eam-lifecycle-sust-assessment.axvw', 'property': 'ab-eam-lifecycle-sust-assessment.axvw', 'eq': 'ab-eam-lifecycle-sust-assessment.axvw', 'ta': 'ab-eam-lifecycle-sust-assessment.axvw'},
            'clean_bl': {'bl': 'ab-eam-lifecycle-clean-building.axvw', 'property': 'ab-eam-lifecycle-clean-building.axvw', 'eq': 'ab-eam-lifecycle-clean-building.axvw', 'ta': 'ab-eam-lifecycle-clean-building.axvw'},
            'commissioning': {'bl': 'ab-eam-lifecycle-commissioning.axvw', 'property': 'ab-eam-lifecycle-commissioning.axvw', 'eq': 'ab-eam-lifecycle-commissioning.axvw', 'ta': 'ab-eam-lifecycle-commissioning.axvw'},
            'compliance': {'bl': 'ab-eam-lifecycle-compliance.axvw', 'property': 'ab-eam-lifecycle-compliance.axvw', 'eq': 'ab-eam-lifecycle-compliance.axvw', 'ta': ''},
            'other': {'bl': 'ab-eam-lifecycle-other-action.axvw', 'property': 'ab-eam-lifecycle-other-action.axvw', 'eq': 'ab-eam-lifecycle-other-action.axvw', 'ta': 'ab-eam-lifecycle-other-action.axvw'},
            'sd_furniture': {'bl': 'ab-eam-lifecycle-sd-action.axvw', 'property': 'ab-eam-lifecycle-sd-action.axvw', 'eq': 'ab-eam-lifecycle-sd-action.axvw', 'ta': 'ab-eam-lifecycle-sd-action.axvw'},
            'sd_group_move': {'bl': 'ab-eam-lifecycle-sd-action.axvw', 'property': 'ab-eam-lifecycle-sd-action.axvw', 'eq': 'ab-eam-lifecycle-sd-action.axvw', 'ta': 'ab-eam-lifecycle-sd-action.axvw'},
            'sd_individual_move': {'bl': 'ab-eam-lifecycle-sd-action.axvw', 'property': 'ab-eam-lifecycle-sd-action.axvw', 'eq': 'ab-eam-lifecycle-sd-action.axvw', 'ta': 'ab-eam-lifecycle-sd-action.axvw'},
            'work_request': {'bl': 'ab-eam-lifecycle-wr.axvw', 'property': '', 'eq': 'ab-eam-lifecycle-wr.axvw', 'ta': ''},
            'moves': {'bl': '', 'property': '', 'eq': 'ab-eam-lifecycle-mo-eq.axvw', 'ta': 'ab-eam-lifecycle-mo-ta.axvw'},
            'survey': {'bl': '', 'property': '', 'eq': 'ab-eam-lifecycle-eq-audit.axvw', 'ta': 'ab-eam-lifecycle-ta-audit.axvw'},
            'waste_out': {'bl': 'ab-eam-lifecycle-waste-out.axvw', 'property': 'ab-eam-lifecycle-waste-out.axvw', 'eq': 'ab-eam-lifecycle-waste-out.axvw', 'ta': ''}
        };
        if (!valueExistsNotEmpty(activityType)) {// other
            activityType = 'other';
        }
        var drillDownView = drillDownByActivityType[activityType][assetType];
        if (valueExistsNotEmpty(drillDownView)) {
            var restriction = this.getDrillDownRestriction(assetType, assetId, activityType, statusColumn);
            View.getOpenerView().openDialog(drillDownView, restriction, false, {
                width: 1050,
                height: 650,
                closeButton: true
            });
        }
    },
    /**
     * Get drill down restriction
     * @param {string} assetType Asset type.
     * @param {string} assetId Asset id.
     * @param {string} activityType Activity type.
     * @param {string} statusColumn Status column.
     * @returns {Ab.view.Restriction} restriction.
     */
    getDrillDownRestriction: function (assetType, assetId, activityType, statusColumn) {
        var fieldNameByType = {'bl': 'bl_id', 'property': 'pr_id', 'eq': 'eq_id', 'ta': 'ta_id'};
        var restriction = new Ab.view.Restriction();
        switch (activityType) {
            case 'project_action':
            case 'assessment':
            case 'sustainability_assessment':
            case 'commissioning':
            case 'clean_bl':
            case 'other':
                restriction.addClause('activity_log.' + fieldNameByType[assetType], assetId, '=');
                restriction.addClause('activity_log.status', ['COMPLETED', 'COMPLETED-V', 'CLOSED', 'CANCELLED'], 'NOT IN');
                this.appendStatusRestriction(activityType, assetType, statusColumn, restriction);
                break;
            case 'sd_furniture':
                restriction.addClause('activity_log.' + fieldNameByType[assetType], assetId, '=');
                restriction.addClause('activity_log.activity_type', 'SERVICE DESK - FURNITURE', '=');
                this.appendStatusRestriction(activityType, assetType, statusColumn, restriction);
                break;
            case 'sd_group_move':
                restriction.addClause('activity_log.' + fieldNameByType[assetType], assetId, '=');
                restriction.addClause('activity_log.activity_type', 'SERVICE DESK - GROUP MOVE', '=');
                this.appendStatusRestriction(activityType, assetType, statusColumn, restriction);
                break;
            case 'sd_individual_move':
                restriction.addClause('activity_log.' + fieldNameByType[assetType], assetId, '=');
                restriction.addClause('activity_log.activity_type', 'SERVICE DESK - INDIVIDUAL MOVE', '=');
                this.appendStatusRestriction(activityType, assetType, statusColumn, restriction);
                break;
            case 'compliance':
                restriction.addClause('compliance_locations.' + fieldNameByType[assetType], assetId, '=');
                this.appendStatusRestriction(activityType, assetType, statusColumn, restriction);
                break;
            case 'work_request':
                restriction.addClause('wrhwr.' + fieldNameByType[assetType], assetId, '=');
                this.appendStatusRestriction(activityType, assetType, statusColumn, restriction);
                break;
            case 'moves':
                restriction.addClause('mo_' + assetType + '.' + fieldNameByType[assetType], assetId, '=');
                this.appendStatusRestriction(activityType, assetType, statusColumn, restriction);
                break;
            case 'survey':
                restriction.addClause(assetType + '_audit.' + fieldNameByType[assetType], assetId, '=');
                this.appendStatusRestriction(activityType, assetType, statusColumn, restriction);
                break;
            case 'waste_out':
                restriction.addClause('waste_out.' + fieldNameByType[assetType], assetId, '=');
                restriction.addClause('waste_out.status', ['G', 'A', 'S'], 'IN');
                break;
        }
        return restriction;
    },
    /**
     * Appends restriction for status column.
     * @param {string} activityType Activity type.
     * @param {string} assetType Asset type.
     * @param {string} statusColumn Status column.
     * @param {Ab.view.Restriction} restriction Restriction to append to.
     */
    appendStatusRestriction: function (activityType, assetType, statusColumn, restriction) {
        switch (activityType) {
            case 'project_action':
            case 'assessment':
            case 'sustainability_assessment':
            case 'commissioning':
            case 'clean_bl':
            case 'other':
            case 'sd_furniture':
            case 'sd_group_move':
            case 'sd_individual_move':
            case 'compliance':
                if ('status_created' === statusColumn) {
                    restriction.addClause('activity_log.status', ['N/A', 'CREATED'], 'IN');
                } else if ('status_requested' === statusColumn) {
                    restriction.addClause('activity_log.status', ['REQUESTED', 'BUDGETED'], 'IN');
                } else if ('status_on_hold' === statusColumn) {
                    restriction.addClause('activity_log.status', ['IN PROCESS-H'], 'IN');
                } else if ('status_approved' === statusColumn) {
                    restriction.addClause('activity_log.status', ['APPROVED'], 'IN');
                } else if ('status_issued' === statusColumn) {
                    restriction.addClause('activity_log.status', ['PLANNED', 'SCHEDULED', 'IN PROGRESS', 'TRIAL'], 'IN');
                } else if ('status_rejected' === statusColumn) {
                    restriction.addClause('activity_log.status', ['REJECTED', 'STOPPED'], 'IN');
                }
                break;
            case 'work_request':
                if (valueExists(statusColumn)) {
                    if ('status_requested' === statusColumn) {
                        restriction.addClause('wrhwr.status', ['R'], 'IN');
                    } else if ('status_on_hold' === statusColumn) {
                        restriction.addClause('wrhwr.status', ['Rev', 'HA', 'HL', 'HP'], 'IN');
                    } else if ('status_approved' === statusColumn) {
                        restriction.addClause('wrhwr.status', ['A', 'AA'], 'IN');
                    } else if ('status_issued' === statusColumn) {
                        restriction.addClause('wrhwr.status', ['I'], 'IN');
                    } else if ('status_rejected' === statusColumn) {
                        restriction.addClause('wrhwr.status', ['S', 'Rej'], 'IN');
                    }
                } else {
                    restriction.addClause('wrhwr.status', ['R', 'Rev', 'A', 'AA', 'I', 'HP', 'HA', 'HL', 'S', 'Rej'], 'IN');
                }
                break;
            case 'moves':
                if ('status_requested' === statusColumn) {
                    restriction.addClause('mo_' + assetType + '.status', ['req'], 'IN');
                } else if ('status_issued' === statusColumn) {
                    restriction.addClause('mo_' + assetType + '.status', ['iss'], 'IN');
                }
                break;
            case 'survey':
                if ('status_on_hold' === statusColumn) {
                    restriction.addClause('survey.status', ['On Hold'], 'IN');
                } else if ('status_issued' === statusColumn) {
                    restriction.addClause('survey.status', ['Issued'], 'IN');
                }
                break;
        }
    },
    /**
     * On drill down event
     * @param {string} id command context.
     * @param {Ab.view.CrossTable} panel crossTable panel
     */
    onClickItem: function (id, panel) {
        var restriction = panel.getRestrictionFromId(id);
        var activityTypeClause = restriction.findClause('activity_log.activity_type');
        if (activityTypeClause) {
            var activityType = activityTypeClause.value;
            var columnId = id.split(';')[0];
            var parsedId = panel.parsedClickableId(id);
            if (parsedId.indexOf('_row') === 0) {
                columnId = panel.groupByFields[0].id;
            }
            var statusColumn = this.getColumnStatus(columnId);
            var hasRecords = $(id).text !== '*';
            if (hasRecords) {
                this.onDrillDownEvent(this.assetType, this.assetId, activityType, statusColumn);
            }
        }
    },
    /**
     * Return column status id.
     * @param {string} columnId
     * @returns {string} Status column.
     */
    getColumnStatus: function (columnId) {
        var statusColumn = null;
        if (valueExists(columnId) && columnId.indexOf('status_') > 0) {
            statusColumn = columnId.split('activity_log.')[1];
        }
        return statusColumn;
    }
});