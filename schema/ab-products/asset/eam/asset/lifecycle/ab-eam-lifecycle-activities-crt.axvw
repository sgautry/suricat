<view version="2.0">
    <js file="ab-eam-lifecycle-activities-crt.js"/>
    <!-- translatable messages -->
    <message name="labelActivityType_project_action" translatable="true">Project Actions</message>
    <message name="labelActivityType_assessment" translatable="true">Assessments</message>
    <message name="labelActivityType_sustainability_assessment" translatable="true">Sustainability Assessments</message>
    <message name="labelActivityType_clean_bl" translatable="true">Clean Building</message>
    <message name="labelActivityType_commissioning" translatable="true">Commissioning</message>
    <message name="labelActivityType_empty" translatable="true">Other</message>
    <message name="labelActivityType_moves" translatable="true">Moves</message>
    <message name="labelActivityType_survey" translatable="true">Surveys</message>
    <message name="labelActivityType_work_request" translatable="true">Work Request</message>
    <message name="labelActivityType_waste_out" translatable="true">Waste out</message>
    <message name="labelActivityType_sd_furniture" translatable="true">Service Desk Furniture</message>
    <message name="labelActivityType_sd_group_move" translatable="true">Service Desk Group Move</message>
    <message name="labelActivityType_sd_individual_move" translatable="true">Service Desk Individual Move</message>
    <message name="labelActivityType_compliance" translatable="true">Compliance</message>
    <message name="titleAssetSummary_property" translatable="true">Asset Summary For Property Asset: {0}</message>
    <message name="titleAssetSummary_bl" translatable="true">Asset Summary For Building Asset: {0}</message>
    <message name="titleAssetSummary_eq" translatable="true">Asset Summary For Equipment Asset: {0}</message>
    <message name="titleAssetSummary_ta" translatable="true">Asset Summary For Furniture Asset: {0}</message>
    <!-- Helper panel to refresh the activities panel. -->
    <panel type="html" id="abEamLifecycleCrtActivitiesRefresh" dataSource="none" showOnLoad="false" hidden="true">
        <html></html>
    </panel>
    <!-- Current activities data-source. -->
    <dataSource id="abEamLifecycleActivitiesCrt_ds" type="grouping">
        <sql dialect="generic">
            SELECT 
                (CASE 
                    WHEN (activity_log.project_id IS NOT NULL AND project.project_type NOT IN ('ASSESSMENT', 'ASSESSMENT - ENVIRONMENTAL', 'ASSESSMENT - HAZMAT', 'COMMISSIONING'))
                        THEN 'project_action'
                    WHEN (activity_log.project_id IS NOT NULL AND project.project_type='ASSESSMENT - ENVIRONMENTAL')
                        THEN 'sustainability_assessment'
                    WHEN (activity_log.project_id IS NOT NULL AND project.project_type='ASSESSMENT')
                        THEN 'assessment'
                    WHEN (activity_log.project_id IS NOT NULL AND (project.project_type='ASSESSMENT - HAZMAT' OR activity_log.activity_type LIKE '%HAZMAT%'))
                        THEN 'clean_bl'
                    WHEN (activity_log.project_id IS NOT NULL AND project.project_type='COMMISSIONING')
                        THEN 'commissioning'
                    WHEN (activity_log.activity_type='SERVICE DESK - FURNITURE' AND activity_log.wr_id IS NULL)
                        THEN 'sd_furniture'
                    WHEN (activity_log.activity_type='SERVICE DESK - GROUP MOVE' AND activity_log.wr_id IS NULL)
                        THEN 'sd_group_move'
                    WHEN (activity_log.activity_type='SERVICE DESK - INDIVIDUAL MOVE' AND activity_log.wr_id IS NULL)
                        THEN 'sd_individual_move'
                    ELSE ''    
                END) ${sql.as} activity_type,
                ${sql.convertToString('activity_log.activity_log_id')} ${sql.as} item_id,
                (CASE WHEN (activity_log.status IN ('N/A','CREATED')) THEN 1 ELSE 0 END) ${sql.as} status_created,
                (CASE WHEN (activity_log.status IN ('REQUESTED','BUDGETED')) THEN 1 ELSE 0 END) ${sql.as} status_requested,
                (CASE WHEN (activity_log.status IN ('IN PROCESS-H')) THEN 1 ELSE 0 END) ${sql.as} status_on_hold,
                (CASE WHEN (activity_log.status IN ('APPROVED')) THEN 1 ELSE 0 END) ${sql.as} status_approved,
                (CASE WHEN (activity_log.status IN ('PLANNED','SCHEDULED','IN PROGRESS','TRIAL')) THEN 1 ELSE 0 END) ${sql.as} status_issued,
                (CASE WHEN (activity_log.status IN ('REJECTED','STOPPED')) THEN 1 ELSE 0 END) ${sql.as} status_rejected,
                activity_log.bl_id ${sql.as} bl_id,
                activity_log.pr_id ${sql.as} pr_id,
                activity_log.eq_id ${sql.as} eq_id,
                activity_log.ta_id ${sql.as} ta_id
            FROM activity_log LEFT OUTER JOIN project ON project.is_template = 0 AND activity_log.project_id = project.project_id
                WHERE activity_log.activity_type NOT IN ('SERVICE DESK - MAINTENANCE', 'COMPLIANCE - EVENT') AND activity_log.status NOT IN ('COMPLETED', 'COMPLETED-V', 'CLOSED', 'CANCELLED')
            UNION 
            SELECT 
                'compliance' ${sql.as} activity_type,
                ${sql.convertToString('activity_log.activity_log_id')} ${sql.as} item_id,
                (CASE WHEN (activity_log.status IN ('N/A','CREATED')) THEN 1 ELSE 0 END) ${sql.as} status_created,
                (CASE WHEN (activity_log.status IN ('REQUESTED','BUDGETED')) THEN 1 ELSE 0 END) ${sql.as} status_requested,
                (CASE WHEN (activity_log.status IN ('IN PROCESS-H')) THEN 1 ELSE 0 END) ${sql.as} status_on_hold,
                (CASE WHEN (activity_log.status IN ('APPROVED')) THEN 1 ELSE 0 END) ${sql.as} status_approved,
                (CASE WHEN (activity_log.status IN ('PLANNED','SCHEDULED','IN PROGRESS','TRIAL')) THEN 1 ELSE 0 END) ${sql.as} status_issued,
                (CASE WHEN (activity_log.status IN ('REJECTED','STOPPED')) THEN 1 ELSE 0 END) ${sql.as} status_rejected,
                compliance_locations.bl_id ${sql.as} bl_id,
                compliance_locations.pr_id ${sql.as} pr_id,
                compliance_locations.eq_id ${sql.as} eq_id,
                NULL ${sql.as} ta_id
            FROM activity_log LEFT OUTER JOIN compliance_locations ON activity_log.location_id=compliance_locations.location_id
                WHERE activity_log.activity_type='COMPLIANCE - EVENT' AND activity_log.status NOT IN ('COMPLETED', 'COMPLETED-V', 'CLOSED', 'CANCELLED')
            UNION 
            SELECT 
                'moves' ${sql.as} activity_type,
                ${sql.convertToString('mo_eq.eq_id')} ${sql.as} item_id,
                0 ${sql.as} status_created,
                (CASE WHEN (mo_eq.status IN ('req')) THEN 1 ELSE 0 END) ${sql.as} status_requested,
                0 ${sql.as} status_on_hold,
                0 ${sql.as} status_approved,
                (CASE WHEN (mo_eq.status IN ('iss')) THEN 1 ELSE 0 END) ${sql.as} status_issued,
                0 ${sql.as} status_rejected,
                NULL ${sql.as} bl_id,
                NULL ${sql.as} pr_id,
                mo_eq.eq_id ${sql.as} eq_id,
                NULL  ${sql.as} ta_id
            FROM mo_eq LEFT OUTER JOIN mo ON mo.mo_id = mo_eq.mo_id
                WHERE mo_eq.status NOT IN ('mov','can') AND mo.status NOT IN ('Completed-Not Ver','Completed-Verified','Closed')
            UNION 
            SELECT 
                'moves' ${sql.as} activity_type,
                ${sql.convertToString('mo_ta.ta_id')} ${sql.as} item_id,
                0 ${sql.as} status_created,
                (CASE WHEN (mo_ta.status IN ('req')) THEN 1 ELSE 0 END) ${sql.as} status_requested,
                0 ${sql.as} status_on_hold,
                0 ${sql.as} status_approved,
                (CASE WHEN (mo_ta.status IN ('iss')) THEN 1 ELSE 0 END) ${sql.as} status_issued,
                0 ${sql.as} status_rejected,
                NULL ${sql.as} bl_id,
                NULL ${sql.as} pr_id,
                NULL ${sql.as} eq_id,
                mo_ta.ta_id ${sql.as} ta_id
            FROM mo_ta LEFT OUTER JOIN mo ON mo.mo_id = mo_ta.mo_id
                WHERE mo_ta.status NOT IN ('mov','can') AND mo.status NOT IN ('Completed-Not Ver','Completed-Verified','Closed')  
            UNION 
            SELECT 
                'survey' ${sql.as} activity_type,
                ${sql.convertToString('eq_audit.eq_id')} ${sql.as} item_id,
                0 ${sql.as} status_created,
                0 ${sql.as} status_requested,
                (CASE WHEN (survey.status IN ('On Hold')) THEN 1 ELSE 0 END) ${sql.as} status_on_hold,
                0 ${sql.as} status_approved,
                (CASE WHEN (survey.status IN ('Issued')) THEN 1 ELSE 0 END) ${sql.as} status_issued,
                0 ${sql.as} status_rejected,
                NULL ${sql.as} bl_id,
                NULL ${sql.as} pr_id,
                eq_audit.eq_id ${sql.as} eq_id,
                NULL ${sql.as} ta_id
            FROM eq_audit LEFT OUTER JOIN survey ON survey.survey_id = eq_audit.survey_id 
                WHERE survey.status IN ('Issued','On Hold')
            UNION 
            SELECT 
                'survey' ${sql.as} activity_type,
                ${sql.convertToString('ta_audit.ta_id')} ${sql.as} item_id,
                0 ${sql.as} status_created,
                0 ${sql.as} status_requested,
                (CASE WHEN (survey.status IN ('On Hold')) THEN 1 ELSE 0 END) ${sql.as} status_on_hold,
                0 ${sql.as} status_approved,
                (CASE WHEN (survey.status IN ('Issued')) THEN 1 ELSE 0 END) ${sql.as} status_issued,
                0 ${sql.as} status_rejected,
                NULL ${sql.as} bl_id,
                NULL ${sql.as} pr_id,
                NULL ${sql.as} eq_id,
                ta_audit.ta_id ${sql.as} ta_id
            FROM ta_audit LEFT OUTER JOIN survey ON survey.survey_id = ta_audit.survey_id 
                WHERE survey.status IN ('Issued','On Hold')
            UNION 
            SELECT 
                'work_request' ${sql.as} activity_type,
                ${sql.convertToString('wr.wr_id')} ${sql.as} item_id,
                0 ${sql.as} status_created,
                (CASE WHEN (wr.status IN ('R')) THEN 1 ELSE 0 END) ${sql.as} status_requested,
                (CASE WHEN (wr.status IN ('Rev','HA','HL','HP')) THEN 1 ELSE 0 END) ${sql.as} status_on_hold,
                (CASE WHEN (wr.status IN ('A','AA')) THEN 1 ELSE 0 END) ${sql.as} status_approved,
                (CASE WHEN (wr.status IN ('I')) THEN 1 ELSE 0 END) ${sql.as} status_issued,
                (CASE WHEN (wr.status IN ('S','Rej')) THEN 1 ELSE 0 END) ${sql.as} status_rejected,
                wr.bl_id ${sql.as} bl_id,
                NULL ${sql.as} pr_id,
                wr.eq_id ${sql.as} eq_id,
                NULL ${sql.as} ta_id
            FROM wr
                WHERE wr.status IN ('R','Rev','A','AA','I','HP','HA','HL','S','Rej')
            UNION 
            SELECT 
                'waste_out' ${sql.as} activity_type,
                ${sql.convertToString('waste_out.waste_id')} ${sql.as} item_id,
                0 ${sql.as} status_created,
                0 ${sql.as} status_requested,
                0 ${sql.as} status_on_hold,
                0 ${sql.as} status_approved,
                (CASE WHEN (waste_out.status IN ('G','A','S')) THEN 1 ELSE 0 END) ${sql.as} status_issued,
                0 ${sql.as} status_rejected,
                waste_out.bl_id ${sql.as} bl_id,
                waste_out.pr_id ${sql.as} pr_id,
                waste_out.eq_id ${sql.as} eq_id,
                NULL ${sql.as} ta_id
            FROM waste_out
                WHERE waste_out.status IN ('G','A','S') 
        </sql>
        <table name="activity_log"/>
        <field name="activity_type" dataType="text" groupBy="true">
            <title>Transaction Type</title>
        </field>
        <field name="activities_count" formula="count" baseField="activity_log.item_id" dataType="number" size="9" decimals="0">
            <title>Total</title>
        </field>
        <field name="status_created" formula="sum" baseField="activity_log.status_created" dataType="number" size="9" decimals="0">
            <title>Created</title>
        </field>
        <field name="status_requested" formula="sum" baseField="activity_log.status_requested" dataType="number" size="9" decimals="0">
            <title>Requested</title>
        </field>
        <field name="status_on_hold" formula="sum" baseField="activity_log.status_on_hold" dataType="number" size="9" decimals="0">
            <title>On Hold</title>
        </field>
        <field name="status_approved" formula="sum" baseField="activity_log.status_approved" dataType="number" size="9" decimals="0">
            <title>Approved</title>
        </field>
        <field name="status_issued" formula="sum" baseField="activity_log.status_issued" dataType="number" size="9" decimals="0">
            <title>Issued</title>
        </field>
        <field name="status_rejected" formula="sum" baseField="activity_log.status_rejected" dataType="number" size="9" decimals="0">
            <title>Rejected</title>
        </field>
        <parameter name="sqlRestriction" dataType="verbatim" value="1=1"/>
        <restriction type="sql" sql="${parameters['sqlRestriction']}"/>
    </dataSource>
    <!-- Cross table panel. -->
    <panel type="crossTable" id="abEamLifecycleActivitiesCrt_form" dataSource="abEamLifecycleActivitiesCrt_ds" showOnLoad="false">
         <title>Count by Transaction Type</title>
         <!-- Drill-down action. -->
         <event type="onClickItem">
            <!-- Used only to add link, do now remove. The onClickItem listener is overwrote in .js -->
         </event>
    </panel>
</view>