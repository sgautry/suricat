View.createController('portfolioMetricSelectController', {
    availableMetrics: null,
    metricRestriction: null,
    afterViewLoad: function () {
        this.metricsPanel.addEventListener('onMultipleSelectionChange', this.onChangeMultipleSelection.createDelegate(this));
    },
    afterInitialDataFetch: function () {
        this.availableMetrics = View.parameters.availableMetrics;
        this.metricRestriction = View.parameters.metricRestriction;
        this.metricsPanel.addParameter('metricRestriction', this.metricRestriction);
        this.metricsPanel.refresh();
        if (this.availableMetrics.length == 1) {
            this.metricsPanel.setFilterValue('afm_metric_definitions.metric_name', this.availableMetrics[0]);
        } else {
            var metricIds = '\"' + this.availableMetrics.join(Ab.form.Form.MULTIPLE_VALUES_SEPARATOR) + '\"';
            this.metricsPanel.setFilterValue('afm_metric_definitions.metric_name', '{' + metricIds.replace(/, \u200C/gi, '\",\"') + '}');
        }
        this.metricsPanel.refresh();
    },
    metricsPanel_afterRefresh: function (panel) {
        // select all available metric rows
        panel.gridRows.each(function (row) {
            for ( var i = 0; i < this.availableMetrics.length; i++ ) {
                if (this.availableMetrics[i] === row.getRecord().getValue('afm_metric_definitions.metric_name')) {
                    row.select(true);
                }
            }
        }, this);
    },
    onChangeMultipleSelection: function (row) {
        var metricId = row.row.getRecord().getValue('afm_metric_definitions.metric_name');
        if (row.row.isSelected()) {
            // add to available metrics
            this.addAvailableMetric(metricId);
        } else {
            // remove from available metrics
            this.removeAvailableMetric(metricId);
        }
    },
    addAvailableMetric: function (metricId) {
        this.availableMetrics.push(metricId);
    },
    removeAvailableMetric: function (metricId) {
        var index = _.indexOf(this.availableMetrics, metricId);
        if (index !== -1) {
            this.availableMetrics.splice(index, 1);
        }
    },
    metricsPanel_onSelect: function () {
        var panelId = View.parameters.panelId;
        View.parameters.callback(panelId, this.availableMetrics);
        View.closeThisDialog();
    }
});