var abEamAssetConsoleController = View.createController('abEamAssetConsoleController', {
    queryParameterNames: ['bl_id', 'pr_id', 'project_id', 'eq_id'],
    queryParameters: null,
    isDemoMode: false,
    filterRestriction: null,
    afterViewLoad: function () {
        this.isDemoMode = isInDemoMode();
        this.queryParameters = readQueryParameters(this.queryParameterNames);
    },
    afterInitialDataFetch: function () {
        this.abEamAssetLifecycleConsoleTabs.addEventListener('afterTabChange', this.afterTabChangeHandler.createDelegate(this));
        // select "Asset Lifecycle Management" tab when is opened from SFA console
        if (!isEmptyObject(this.queryParameters)) {
            this.abEamAssetLifecycleConsoleTabs.selectTab('abEamAssetLifecycleConsoleTabs_management');
        }
    },
    afterTabChangeHandler: function (tabsPanel, selectedTabName) {
        var selectedTab = tabsPanel.findTab(selectedTabName);
        if (selectedTab.isContentLoaded) {
            var controller;
            if ('abEamAssetLifecycleConsoleTabs_registry' === selectedTabName) {
                controller = selectedTab.getContentFrame().View.controllers.get('abEamAssetRegistryController');
            } else if ('abEamAssetLifecycleConsoleTabs_management' === selectedTabName) {
                controller = selectedTab.getContentFrame().View.controllers.get('abEamAssetManagementController');
            } else if ('abEamAssetLifecycleConsoleTabs_optimization' === selectedTabName) {
                controller = selectedTab.getContentFrame().View.controllers.get('abEamAssetOptimizationController');
            } else if ('abEamAssetLifecycleConsoleTabs_eqSystemAnalysis' === selectedTabName) {
                controller = selectedTab.getContentFrame().View.controllers.get('eqSysAnalysisController');
            } else if ('abEamAssetLifecycleConsoleTabs_reports' === selectedTabName) {
                controller = selectedTab.getContentFrame().View.controllers.get('exceptionReportController');
            } else {
                controller = selectedTab.getContentFrame().View.controllers.get('eqSysAssignmentController');
                if (controller) {
                    controller.refreshAllPanels = true;
                }
            }
            // set filter restriction if was applied from another tab
            if (controller) {
                controller.filterController.copyFilterRestriction(this.filterRestriction);
                controller.filterController.setCollapsed(!this.filterRestriction.isEmpty());
                controller.filterController.abEamAssetFilter_onFilter();
            }
        }
    }
});