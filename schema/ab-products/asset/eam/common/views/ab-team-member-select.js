View.createController('teamSelectMemberController', {
    callbackMethod: null,
    afterViewLoad: function () {
        if (valueExists(this.view.parameters) && valueExists(this.view.parameters.callback)) {
            this.callbackMethod = this.view.parameters.callback;
        }
        this.abTeamSelectMemberTeamTree.addParameter('sourceName_vn', getMessage('msgSourceName_vn'));
        this.abTeamSelectMemberTeamTree.addParameter('sourceName_contact', getMessage('msgSourceName_contact'));
        this.abTeamSelectMemberTeamTree.addParameter('sourceName_em', getMessage('msgSourceName_em'));
    },
    afterInitialDataFetch: function () {
        this.abTeamSelectContact_form.show(false, true);
    },
    onSelectRowRecord: function (sourceTable, record) {
        if (valueExists(this.callbackMethod)) {
            this.callbackMethod(sourceTable, record);
        }
    },
    abTeamSelectMemberContactPanel_onAddNew: function () {
        this.abTeamSelectContact_form.refresh(null, true);
        this.view.getLayoutManager('page2Layout').expandRegion('north');
    },
    abTeamSelectContact_form_onSelect: function () {
        this.onSelectRowRecord('contact', this.abTeamSelectContact_form.getRecord());
    },
    abTeamSelectContact_form_onSave: function () {
        if (this.abTeamSelectContact_form.canSave()) {
            if (this.abTeamSelectContact_form.save()) {
                // refresh grid panel
                this.abTeamSelectMemberContactPanel.refresh(this.abTeamSelectMemberContactPanel.restriction);
            }
        }
    },
    abTeamSelectContact_form_onCancel: function () {
        this.view.getLayoutManager('page2Layout').collapseRegion('north');
        this.abTeamSelectContact_form.refresh(null, true);
    },
    abTeamSelectMemberVnPanel_onAddNew: function () {
        this.abTeamSelectVendor_form.refresh(null, true);
        this.view.getLayoutManager('page3Layout').expandRegion('north');
    },
    abTeamSelectVendor_form_onSelect: function () {
        this.onSelectRowRecord('contact', this.abTeamSelectVendor_form.getRecord());
    },
    abTeamSelectVendor_form_onSave: function () {
        if (this.abTeamSelectVendor_form.canSave()) {
            if (this.abTeamSelectVendor_form.save()) {
                // refresh grid panel
                this.abTeamSelectMemberVnPanel.refresh(this.abTeamSelectMemberVnPanel.restriction);
            }
        }
    },
    abTeamSelectVendor_form_onCancel: function () {
        this.view.getLayoutManager('page3Layout').collapseRegion('north');
        this.abTeamSelectVendor_form.refresh(null, true);
    }
});

function selectMember(context) {
    var parentPanel = View.panels.get(context.command.parentPanelId);
    var sourceTable = View.dataSources.get(parentPanel.config.dataSourceId).mainTableName;
    var selectedRecord = parentPanel.gridRows.get(parentPanel.selectedRowIndex).getRecord();
    View.controllers.get('teamSelectMemberController').onSelectRowRecord(sourceTable, selectedRecord);
}

/**
 * On click tree node handler.
 *
 */
function onClickNodeHandler(context) {
    // read member information from source table, not from team
    var objParentPanel = View.panels.get(context.command.parentPanelId);
    var sourceTable = objParentPanel.lastNodeClicked.data['team.source_table'];
    var restriction = new Ab.view.Restriction();
    var objDataSource = null;
    if (sourceTable == 'em') {
        objDataSource = View.dataSources.get('abteamSelectMemberEm_ds');
        restriction.addClause('em.em_id', objParentPanel.lastNodeClicked.data['team.em_id'], '=');
    } else if (sourceTable == 'contact') {
        objDataSource = View.dataSources.get('abteamSelectMemberContact_ds');
        restriction.addClause('contact.contact_id', objParentPanel.lastNodeClicked.data['team.contact_id'], '=');
    } else if (sourceTable == 'vn') {
        objDataSource = View.dataSources.get('abteamSelectMemberVn_ds');
        restriction.addClause('vn.vn_id', objParentPanel.lastNodeClicked.data['team.vn_id'], '=');
    }
    var selectedRecord = objDataSource.getRecord(restriction);
    View.controllers.get('teamSelectMemberController').onSelectRowRecord(sourceTable, selectedRecord);
}