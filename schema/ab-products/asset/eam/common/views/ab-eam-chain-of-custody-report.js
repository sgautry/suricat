View.createController('abChainOfCustodyController', {
    // filter restriction - default is assigned
    filterSelection: {
        "asset_assigned": "assigned",
        "asset_type": ""
    },
    assetAssignedValue: "SELECT team.autonumbered_id FROM team WHERE team.team_type = 'Equipment' AND team.custodian_type IS NOT NULL AND team.is_owner=1 {0}",
    afterViewLoad: function () {
        // customize mini console
        this.abAssetRegistry_list.addEventListener('afterBuildHeader', this.abAssetRegistry_list_customizeHeader, this);
        // customize grid columns
        this.abAssetRegistry_list.afterCreateCellContent = this.customizeAssetRegistryFields.createDelegate(this);
    },
    afterInitialDataFetch: function () {
        this.abAssetRegistry_filter.setFieldValue('asset_assigned', 'assigned');
        var selectedAssetIds = [];
        if (valueExists(View.parameters)) {
            if (valueExistsNotEmpty(View.parameters["assetType"])) {
                var assetType = View.parameters["assetType"];
                this.abAssetRegistry_filter.setFieldValue('asset_type', assetType);
                this.filterSelection['asset_type'] = assetType;
                var selectedAssets = View.parameters["selectedAssets"];
                if (valueExists(selectedAssets) && !_.isEmpty(selectedAssets)) {
                    selectedAssetIds = View.parameters["selectedAssets"][assetType];
                }
            }
        }
        this.applyFilter(selectedAssetIds);
    },
    /**
     * On apply filter
     * called from onChange event from all console fields
     */
    onChangeFilter: function (field) {
        var fieldName = field.id.replace('abAssetRegistry_filter_', '');
        this.filterSelection[fieldName] = field.value;
        this.applyFilter();
    },
    /**
     * Apply selected filter
     * @param {Array.<String>} selectedAssetIds - Selected asset ids.
     */
    applyFilter: function (selectedAssetIds) {
        var blRestriction, propertyRestriction, eqRestriction, taRestriction, sqlRestriction = " 1 = 1 ";
        var restriction = "";
        if (valueExistsNotEmpty(this.filterSelection['asset_type'])) {
            sqlRestriction = "bl.asset_type = '{0}'".replace('{0}', makeSafeSqlValue(this.filterSelection['asset_type']));
        }
        if ('assigned' === this.filterSelection['asset_assigned']) {
            restriction = "EXISTS(" + this.assetAssignedValue + ")";
        } else if ('unassigned' === this.filterSelection['asset_assigned']) {
            restriction = "NOT EXISTS(" + this.assetAssignedValue + ")";
        }
        if (valueExistsNotEmpty(restriction)) {
            blRestriction = String.format(restriction, "AND team.bl_id = bl.bl_id");
            propertyRestriction = String.format(restriction, "AND team.pr_id = property.pr_id");
            eqRestriction = String.format(restriction, "AND team.eq_id = eq.eq_id");
            taRestriction = String.format(restriction, "AND team.ta_id = ta.ta_id");
        }
        this.abAssetRegistry_list.addParameter('blTypeRestriction', blRestriction);
        this.abAssetRegistry_list.addParameter('propertyTypeRestriction', propertyRestriction);
        this.abAssetRegistry_list.addParameter('eqTypeRestriction', eqRestriction);
        this.abAssetRegistry_list.addParameter('taTypeRestriction', taRestriction);
        this.abAssetRegistry_list.addParameter('sqlTypeRestriction', sqlRestriction);
        this.abAssetRegistry_list.addParameter('useCustodian', true);
        this.abAssetRegistry_list.refresh();
        if (valueExists(selectedAssetIds) && !_.isEmpty(selectedAssetIds)) {
            // set panel miniconsole filter
            this.setPanelFilterValues('bl.asset_id', selectedAssetIds);
            this.abAssetRegistry_list.refresh();
            // select automatically first row
            var firstRow = this.abAssetRegistry_list.rows[0];
            if (valueExists(firstRow)) {
                var record = firstRow.row.getRecord();
                this.showAssetProfile(record.getValue('bl.asset_type'), record.getValue('bl.asset_id'));
                this.abAssetRegistry_list.selectRow(firstRow.index);
            }
        }
    },
    /**
     * Set miniconsole filter panel
     * @param fieldName
     * @param selectedAssetIds
     */
    setPanelFilterValues: function (fieldName, selectedAssetIds) {
        if (selectedAssetIds.length === 1) {
            this.abAssetRegistry_list.setFilterValue(fieldName, selectedAssetIds[0]);
        } else {
            var assetIds = '\"' + selectedAssetIds.join(Ab.form.Form.MULTIPLE_VALUES_SEPARATOR) + '\"';
            this.abAssetRegistry_list.setFilterValue(fieldName, '{' + assetIds.replace(/, \u200C/gi, '\",\"') + '}');
        }
    },
    /**
     * Show asset profile.
     * @param {string} assetType - Asset type.
     * @param {string} assetId - Asset id.
     */
    showAssetProfile: function (assetType, assetId) {
        var config = null;
        if ('bl' === assetType) {
            config = {
                viewName: 'ab-profile-building.axvw',
                fieldName: 'bl_id'
            };
        } else if ('eq' === assetType) {
            config = {
                viewName: 'ab-profile-equipment.axvw',
                fieldName: 'eq_id'
            };
        } else if ('ta' === assetType) {
            config = {
                viewName: 'ab-profile-ta.axvw',
                fieldName: 'ta_id'
            };
        } else if ('property' === assetType) {
            config = {
                viewName: 'ab-profile-property.axvw',
                fieldName: 'pr_id'
            };
        }
        if (config) {
            var profilePanel = View.panels.get('abAssetProfileView');
            profilePanel.parameters.custodianRestriction = {
                'team.is_owner': 1
            };
            profilePanel.loadView(config.viewName + '?' + config.fieldName + '=' + assetId + '&isTeamOwner=true');
        }
    },
    abAssetRegistry_list_customizeHeader: function (panel, parentElement) {
        var columnIndex = 0;
        for ( var i = 0, col; col = panel.columns[i]; i++ ) {
            if ('bl.asset_type' === col.id && !col.hidden) {
                var headerRows = parentElement.getElementsByTagName("tr");
                // customize filter row if this exists
                var rowElement = document.getElementById(panel.id + '_filterRow');
                if (headerRows.length > 1 && valueExists(rowElement)) {
                    var filterInputId = panel.getFilterInputId(col.id);
                    this.updateMiniConsole(panel, rowElement, columnIndex, filterInputId);
                }
            }
            if (!col.hidden) {
                columnIndex++;
            }
        }
    },
    /**
     * Customize filter row asset type.
     *
     * @param panel grid panel
     * @param rowElement filter row element
     * @param columnIndex column index
     * @param filterInputId filter input id
     */
    updateMiniConsole: function (panel, rowElement, columnIndex, filterInputId) {
        var assetTypes = {
            'property': getMessage('asset_type_property'),
            'bl': getMessage('asset_type_bl'),
            'eq': getMessage('asset_type_eq'),
            'ta': getMessage('asset_type_ta')
        };
        var headerCells = rowElement.getElementsByTagName("th");
        var headerCell = headerCells[columnIndex];
        // remove input element if exists
        var inputElements = headerCell.getElementsByTagName("input");
        if (inputElements[filterInputId]) {
            headerCell.removeChild(inputElements[filterInputId]);
        }
        // add select element
        var i = 0;
        var input = document.createElement("select");
        input.options[i++] = new Option("", "", true);
        for ( var storedValue in assetTypes ) {
            input.options[i++] = new Option(assetTypes[storedValue], storedValue);
        }
        input.className = "inputField_box";
        // run filter when user click on one enum value
        Ext.EventManager.addListener(input, "change", panel.onFilter.createDelegate(panel));
        input.id = filterInputId;
        if (headerCell.childNodes.length > 0) {
            headerCell.insertBefore(input, headerCell.childNodes[0]);
        } else {
            headerCell.appendChild(input);
        }
    },
    /**
     * Customize fields for asset registry panel
     */
    customizeAssetRegistryFields: function (row, column, cellElement) {
        var fieldValue = row.row.getFieldValue(column.id);
        if ('bl.asset_type' === column.id) {
            // doesn't have control type link
            cellElement.childNodes[0].innerHTML = getMessage('asset_type_' + fieldValue);
        } else if ('bl.asset_id' === column.id) {
            cellElement.childNodes[0].style.fontWeight = 'bold';
        }
    }
});
/**
 * Show custodians for selected asset
 *
 */
function showAssetProfile(context) {
    var parentPanel = View.panels.get(context.command.parentPanelId);
    if (parentPanel) {
        var selectedRow = parentPanel.gridRows.get(parentPanel.selectedRowIndex);
        var record = selectedRow.getRecord();
        var assetType = record.getValue("bl.asset_type"),
            assetId = record.getValue("bl.asset_id");
        View.controllers.get('abChainOfCustodyController').showAssetProfile(assetType, assetId);
    }
}