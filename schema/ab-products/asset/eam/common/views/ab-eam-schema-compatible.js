/**
 * Common variables used for EAM to check backward compatible with 23.1 schema.
 */
/**
 * Check if custodiantype is defined in schema and has licence Enterprise Asset Management.
 * @type {boolean}
 */
var isCustodianSchemaDefined = false;
/**
 * Check if depreciation cost fields are defined in schema.
 * @type {boolean}
 */
var isCostDepreciationSchemaDefined = false;
/**
 * Check if afm_enterprise_graphics is defined in schema.
 * @type {boolean}
 */
var isEnterpriseGraphicsSchemaDefined = false;
/**
 * Check if EAM workflow rule is defined in schema.
 * Used to dispose assets.
 * @type {boolean}
 */
var isEamWorkflowRuleDefined = false;
/**
 * Check if activity_log_hactivity_log has specific fields defined in schema.
 * Used for Recently Closed Activities and Historical Activities reports.
 * @type {boolean}
 */
var isHistoryActivitySchemaDefined = false;
/**
 * Check if eq_req_items has specific fields defined in schema.
 * Used for Asset Requirements tab in Project Proposal Console.
 * @type {boolean}
 */
var isEquipmentRequiredItemSchemaDefined = false;
View.createController('abEamSchemaCompatibleController', {
    afterViewLoad: function () {
        isCustodianSchemaDefined = valueExists(View.dataSources.get('abEamCompatible_custodiantype_ds')) && this.hasAssetEAMLicense();
        isCostDepreciationSchemaDefined = function () {
            return schemaHasFields(['eq.date_dep_closing_month', 'ta.cost_dep_value', 'ta.date_dep_closing_month']);
        }();
        isEnterpriseGraphicsSchemaDefined = valueExists(View.dataSources.get('abEamCompatible_afm_enterprise_graphics_ds'));
        isEamWorkflowRuleDefined = valueExists(View.dataSources.get('abEamCompatible_afm_wf_rules_ds').getRecord().getValue('afm_wf_rules.activity_id'));
        isHistoryActivitySchemaDefined = function () {
            return schemaHasFields(['activity_log_hactivity_log.location_id', 'activity_log_hactivity_log.ta_id']);
        }();
        isEquipmentRequiredItemSchemaDefined = function () {
            return schemaHasFields([
                'eq_req_items.p01_value', 'eq_req_items.p02_value', 'eq_req_items.p03_value',
                'eq_req_items.p04_value', 'eq_req_items.p05_value', 'eq_req_items.p06_value',
                'eq_req_items.p07_value', 'eq_req_items.p08_value', 'eq_req_items.p09_value',
                'eq_req_items.p10_value', 'eq_req_items.p11_value', 'eq_req_items.p12_value']);
        }();
    },
    /**
     * Check if Enterprise Asset Management license exists and is enabled.
     * @returns {boolean} has license enabled
     */
    hasAssetEAMLicense: function () {
        var hasLicense = false;
        AdminService.getProgramLicense({
            async: false,
            callback: function (licenseDTO) {
                for ( var i = 0; i < licenseDTO.licenses.length; i++ ) {
                    var license = licenseDTO.licenses[i];
                    if ('AbAssetEAM' === license.id) {
                        hasLicense = license.enabled;
                        break;
                    }
                }
            },
            errorHandler: function (m, e) {
                View.showException(e);
            }
        });
        return hasLicense;
    }
});