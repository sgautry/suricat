<view version="2.0">
	<title>Property Profile</title>
	<report/>
	<dataSource id="abProfileProperty_ds">
		<table name="property"/>
		<field name="pr_id"/>
		<field name="prop_photo"/>
		<field name="image_map"/>
		<field name="comments"/>
		<field name="description"/>
		<field name="services"/>
		<field name="name"/>
		<field name="property_type"/>
		<field name="use1"/>
		<field name="zoning"/>
		<field name="status"/>
		<field name="contact1"/>
		<field name="contact2"/>
		<field name="ac_id"/>
		<field name="unit"/>
		<field name="site_id"/>
		<field name="regn_id"/>
		<field name="ctry_id"/>
		<field name="vicinity"/>
		<field name="lat"/>
		<field name="lon"/>
		<field name="area_manual" decimals="0"/>
		<field name="area_cad" decimals="0"/>
		<field name="area_bl_rentable" decimals="0"/>
		<field name="area_bl_est_rentable" decimals="0"/>
		<field name="area_lease_neg" decimals="0"/>
		<field name="area_land_acres" decimals="0"/>
		<field name="qty_no_bldgs"/>
		<field name="qty_no_spaces"/>
		<field name="date_market_val"/>
		<field name="date_book_val"/>
		<field name="pct_own"/>
		<field name="qty_occupancy"/>
		<field name="vf_address1" dataType="text">
			<sql dialect="generic">
				(property.address1 ${sql.concat} (CASE WHEN property.address2 IS NULL THEN '' ELSE ', ' END) ${sql.concat} property.address2)
			</sql>
		</field>
		<field name="vf_address2" dataType="text">
			<sql dialect="generic">
				((SELECT city.name FROM city WHERE city.city_id = property.city_id) ${sql.concat} (CASE WHEN property.state_id IS NULL THEN '' ELSE ', ' END) ${sql.concat} property.state_id ${sql.concat} ' ' ${sql.concat} property.zip)
			</sql>
		</field>
		<field name="vf_owner_custodian" dataType="text">
			<sql dialect="generic">
				(SELECT
					(CASE
                        WHEN team.source_table = 'vn' AND vn.company IS NOT NULL THEN vn.company
                        WHEN team.source_table = 'vn' THEN team.vn_id
                        WHEN team.source_table = 'contact' AND (contact.name_first IS NOT NULL OR contact.name_last IS NOT NULL) THEN (contact.name_first ${sql.concat} ' ' ${sql.concat} contact.name_last)
                        WHEN team.source_table = 'contact' THEN team.contact_id
                        WHEN team.source_table = 'em' AND em.name IS NOT NULL THEN em.name
                        WHEN team.source_table = 'em' AND em.name IS NULL AND (em.name_first IS NOT NULL OR em.name_last IS NOT NULL) THEN (em.name_first ${sql.concat} ' ' ${sql.concat} em.name_last)
                        WHEN team.source_table = 'em' THEN team.em_id
					 ELSE NULL END)
				 FROM team
                    LEFT OUTER JOIN contact ON team.contact_id=contact.contact_id
                    LEFT OUTER JOIN vn ON team.vn_id=vn.vn_id
                    LEFT OUTER JOIN em ON team.em_id=em.em_id 
				        WHERE team.is_owner = 1 AND team.pr_id = property.pr_id AND team.team_type = 'Equipment' AND team.status = 'Active')
			</sql>
		</field>
		<field name="vf_value_market" dataType="number" size="20" decimals="0" currency="${user.userCurrency.code}">
			<sql dialect="generic">
				(property.value_market * ${sql.exchangeRateFromBudgetToUserForDate('Budget', 'property.date_market_val')})
			</sql>
		</field>
		<field name="vf_value_book" dataType="number" size="20" decimals="0" currency="${user.userCurrency.code}">
			<sql dialect="generic">
				(property.value_book * ${sql.exchangeRateFromBudgetToUserForDate('Budget', 'property.date_book_val')})
			</sql>
		</field>
		<field name="vf_cost_purchase" dataType="number" size="20" decimals="0" currency="${user.userCurrency.code}">
			<sql dialect="generic">
				(SELECT
				    ot.cost_purchase * ${sql.exchangeRateFromBudgetToUserForDate('Budget', 'ot.date_purchase')}
				 FROM ot
				    WHERE ot.ot_id = (SELECT MAX(x.ot_id) FROM ot x WHERE x.pr_id = property.pr_id AND x.status = 'Owned'))
			</sql>
		</field>
		<field name="vf_date_purchase" dataType="date">
			<sql dialect="generic">
				(SELECT
				    ot.date_purchase
				 FROM ot
				    WHERE ot.ot_id = (SELECT MAX(x.ot_id) FROM ot x WHERE x.pr_id = property.pr_id AND x.status = 'Owned'))
			</sql>
		</field>
		<field name="vf_dummy" dataType="text">
			<sql dialect="generic">''</sql>
		</field>
	</dataSource>
	<dataSource id="abProfilePropertyCustodian_ds">
		<table name="team" role="main"/>
        <table name="em" role="standard"/>
        <table name="contact" role="standard"/>
        <table name="vn" role="standard"/>
        <table name="custodiantype" role="standard"/>
		<field table="team" name="vf_custodian_name" dataType="text" size="250">
			<sql dialect="generic">
				(CASE
                    WHEN team.source_table = 'vn' AND vn.company IS NOT NULL THEN vn.company
                    WHEN team.source_table = 'vn' THEN team.vn_id
                    WHEN team.source_table = 'contact' AND (contact.name_first IS NOT NULL OR contact.name_last IS NOT NULL) THEN (contact.name_first ${sql.concat} ' ' ${sql.concat} contact.name_last)
                    WHEN team.source_table = 'contact' THEN team.contact_id
                    WHEN team.source_table = 'em' AND em.name IS NOT NULL THEN em.name
                    WHEN team.source_table = 'em' AND em.name IS NULL AND (em.name_first IS NOT NULL OR em.name_last IS NOT NULL) THEN (em.name_first ${sql.concat} ' ' ${sql.concat} em.name_last)
                    WHEN team.source_table = 'em' THEN team.em_id
				 ELSE NULL END)
			</sql>
		</field>
        <field table="custodiantype" name="custodian_type"/>
        <field table="custodiantype" name="custodian_type_01"/>
        <field table="custodiantype" name="custodian_type_02"/>
        <field table="custodiantype" name="custodian_type_03"/>
        <field table="custodiantype" name="custodian_type_ch"/>
        <field table="custodiantype" name="custodian_type_de"/>
        <field table="custodiantype" name="custodian_type_es"/>
        <field table="custodiantype" name="custodian_type_fr"/>
        <field table="custodiantype" name="custodian_type_it"/>
        <field table="custodiantype" name="custodian_type_jp"/>
        <field table="custodiantype" name="custodian_type_ko"/>
        <field table="custodiantype" name="custodian_type_nl"/>
        <field table="custodiantype" name="custodian_type_no"/>
        <field table="custodiantype" name="custodian_type_zh"/>
        <field name="status"/>
        <field name="email_archive"/>
        <field name="cell_num_archive"/>
        <field name="date_start"/>
        <field name="date_end"/>
		<field name="pr_id"/>
        <field table="contact" name="contact_id"/>
        <field table="vn" name="vn_id"/>
        <field table="em" name="em_id"/>
        <sortField name="status" ascending="true"/>
        <sortField name="is_owner" ascending="false"/>
        <sortField name="date_start" ascending="false"/>
		<restriction type="sql" sql="team.team_type='Equipment' AND team.custodian_type IS NOT NULL"/>
	</dataSource>
	<panel type="paginatedReport" id="abProfileProperty_details1" dataSource="abProfileProperty_ds" format="table" role="data">
		<field name="pr_id"/>
		<field name="description"/>
		<field name="comments"/>
		<field name="services"/>
		<field name="prop_photo"/>
		<field name="image_map"/>
	</panel>
	<panel type="paginatedReport" id="abProfileProperty_details2" dataSource="abProfileProperty_ds" columns="2" format="column" role="data">
		<field name="name"/>
		<field name="site_id"/>
		<field name="property_type"/>
		<field name="vf_address1">
			<title>Address</title>
		</field>
		<field name="use1"/>
		<field name="vf_address2"/>
		<field name="zoning"/>
		<field name="regn_id"/>
		<field name="status"/>
		<field name="ctry_id"/>
		<field name="contact1"/>
		<field name="vicinity"/>
		<field name="contact2"/>
		<field name="lat"/>
		<field name="ac_id"/>
		<field name="lon"/>
		<field name="unit"/>
		<field name="vf_dummy"/>
		<field name="vf_owner_custodian">
			<title>Owner Custodian</title>
		</field>

		<field name="vf_dummy"/>
		<field name="vf_dummy"/>
		<field name="vf_dummy"/>

		<field name="area_manual"/>
		<field name="vf_value_book">
			<title>Value - Book</title>
		</field>
		<field name="area_cad"/>
		<field name="date_book_val"/>
		<field name="area_bl_rentable"/>
		<field name="vf_value_market">
			<title>Value - Market</title>
		</field>
		<field name="area_lease_neg"/>
		<field name="date_market_val"/>
		<field name="area_land_acres"/>
		<field name="vf_cost_purchase">
			<title>Purchase Price</title>
		</field>
		<field name="qty_no_bldgs"/>
		<field name="vf_date_purchase">
			<title>Date Purchase</title>
		</field>
		<field name="qty_no_spaces"/>
		<field name="pct_own"/>
		<field name="qty_occupancy"/>
	</panel>
	<panel type="paginatedReport" id="abProfileProperty_custodian" dataSource="abProfilePropertyCustodian_ds" format="table" role="data">
		<field name="vf_custodian_name">
			<title>Asset Custodian</title>
		</field>
		<field table="custodiantype" name="custodian_type"/>
		<field name="status">
			<title>Status</title>
		</field>
		<field name="email_archive">
			<title>Email</title>
		</field>
		<field name="cell_num_archive"/>
		<field name="date_start">
			<title>Start Date</title>
		</field>
		<field name="date_end">
			<title>End Date</title>
		</field>
	</panel>
</view>