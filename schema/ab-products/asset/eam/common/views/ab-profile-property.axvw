<view version="2.0">
	<layout type="borderLayout" id="mainLayout">
		<north id="northPos" initialSize="30px"/>
		<west id="westPos" initialSize="290px"/>
		<center id="centerPos"/>
	</layout>
	<message name="propertyProfileTitle">Property Profile</message>
    <message name="ownerDetailsPanelTitle">Custodian Details</message>
    <message name="ownerCustodianTitle">Owner Custodian</message>
    <message name="custodianDetailsPanelTitle">Custodian Details</message>
    <!-- Check backward compatible to 23.1 schema -->
    <panel type="view" id="abEamCheckSchemaCompatibleView" file="ab-eam-schema-compatible.axvw"/>
	<dataSource id="abProfileProperty_ds">
		<table name="property" role="main"/>
		<field name="pr_id"/>
		<field name="prop_photo"/>
		<field name="image_map"/>
		<field name="comments"/>
		<field name="description"/>
		<field name="services"/>
		<field name="name"/>
		<field name="property_type"/>
		<field name="use1"/>
		<field name="zoning"/>
		<field name="status"/>
		<field name="contact1"/>
		<field name="contact2"/>
		<field name="ac_id"/>
		<field name="unit"/>
		<field name="vf_owner_custodian" dataType="text">
			<title>Owner Custodian</title>
			<sql dialect="generic">
                 (${sql.getRestriction('ownerCustodian', 'NULL')})
			</sql>
		</field>
		<field name="site_id"/>
		<field name="vf_address1" dataType="text">
			<title>Address</title>
			<sql dialect="generic">
				(property.address1 ${sql.concat} (CASE WHEN property.address2 IS NULL THEN '' ELSE ', ' END) ${sql.concat} property.address2)
			</sql>
		</field>
		<field name="vf_address2" dataType="text">
			<title></title>
			<sql dialect="generic">
				((SELECT city.name FROM city WHERE city.city_id = property.city_id) ${sql.concat} (CASE WHEN property.state_id IS NULL THEN '' ELSE ', ' END) ${sql.concat} property.state_id ${sql.concat} ' ' ${sql.concat} property.zip)
			</sql>
		</field>
		<field name="regn_id"/>
		<field name="ctry_id"/>
		<field name="vicinity"/>
		<field name="lat"/>
		<field name="lon"/>
		<field name="area_manual" decimals="0"/>
		<field name="area_cad" decimals="0"/>
		<field name="area_bl_rentable" decimals="0"/>
		<field name="area_bl_est_rentable" decimals="0"/>
		<field name="area_lease_neg" decimals="0"/>
		<field name="area_land_acres" decimals="0"/>
		<field name="qty_no_bldgs"/>
		<field name="qty_no_spaces"/>
		<field name="qty_occupancy"/>
		<field name="value_market" dataType="number" size="20" decimals="0" currency="${user.userCurrency.code}">
			<sql dialect="generic">
				(property.value_market * ${sql.exchangeRateFromBudgetToUserForDate('Budget', 'property.date_market_val')})
			</sql>
		</field>
		<field name="value_book" dataType="number" size="20" decimals="0" currency="${user.userCurrency.code}">
			<sql dialect="generic">
				(property.value_book * ${sql.exchangeRateFromBudgetToUserForDate('Budget', 'property.date_book_val')})
			</sql>
		</field>
		<field name="date_market_val"/>
		<field name="date_book_val"/>
		<field name="cost_purchase" dataType="number" size="20" decimals="0" currency="${user.userCurrency.code}">
			<title>Purchase Price</title>
			<sql dialect="generic">
				(SELECT
				    ot.cost_purchase * ${sql.exchangeRateFromBudgetToUserForDate('Budget', 'ot.date_purchase')}
				 FROM ot
				    WHERE ot.ot_id = (SELECT MAX(x.ot_id) FROM ot x WHERE x.pr_id = property.pr_id AND x.status = 'Owned'))
			</sql>
		</field>
		<field name="date_purchase" dataType="date">
			<title>Date Purchase</title>
			<sql dialect="generic">
				(SELECT
				    ot.date_purchase
				 FROM ot
				    WHERE ot.ot_id = (SELECT MAX(x.ot_id) FROM ot x WHERE x.pr_id = property.pr_id AND x.status = 'Owned'))
			</sql>
		</field>
		<field name="pct_own"/>
        <parameter name="useCustodian" dataType="boolean" value="false"/>
        <restriction id="ownerCustodian" enabled="useCustodian" type="sql" sql="
            (SELECT
                (CASE
	                WHEN team.source_table = 'vn' AND vn.company IS NOT NULL THEN vn.company
	                WHEN team.source_table = 'vn' THEN team.vn_id
	                WHEN team.source_table = 'contact' AND (contact.name_first IS NOT NULL OR contact.name_last IS NOT NULL) THEN (contact.name_first ${sql.concat} ' ' ${sql.concat} contact.name_last)
	                WHEN team.source_table = 'contact' THEN team.contact_id
	                WHEN team.source_table = 'em' AND em.name IS NOT NULL THEN em.name
	                WHEN team.source_table = 'em' AND em.name IS NULL AND (em.name_first IS NOT NULL OR em.name_last IS NOT NULL) THEN (em.name_first ${sql.concat} ' ' ${sql.concat} em.name_last)
	                WHEN team.source_table = 'em' THEN team.em_id
                ELSE NULL END)
              FROM team
				LEFT OUTER JOIN contact ON team.contact_id=contact.contact_id
				LEFT OUTER JOIN vn ON team.vn_id=vn.vn_id
				LEFT OUTER JOIN em ON team.em_id=em.em_id     
                    WHERE team.is_owner=1 AND team.pr_id=property.pr_id AND team.team_type='Equipment' AND team.status='Active')"/>
	</dataSource>
	<panel type="html" id="abProfileProperty_title" layoutRegion="northPos">
		<title></title>
		<action type="menu" id="export" imageName="/schema/ab-core/graphics/icons/view/export.png">
			<tooltip>Export</tooltip>
			<action id="paginatedReport">
				<title>DOCX</title>
				<command type="callFunction" functionName="View.controllers.get('abProfilePropCtrl').onExport('DOCX')"/>
			</action>
		</action>
	</panel>
	<panel type="form" id="abProfilePropertyPhoto_form" dataSource="abProfileProperty_ds" columns="1" labelsPosition="top" showOnLoad="false" layoutRegion="westPos">
		<field name="prop_photo" displayField="image_field" hidden="true"/>
		<field id="image_field" controlType="image" width="250px"/>
		<field name="image_map" displayField="image_map_field" hidden="true"/>
		<field id="image_map_field" controlType="image" width="250px"/>
		<field name="pr_id" readOnly="true"/>
		<field name="comments" width="250px" readOnly="true"/>
		<field name="description" width="250px" readOnly="true"/>
		<field name="services" width="250px" readOnly="true"/>
	</panel>
	<panel type="form" id="abProfileProperty_form" dataSource="abProfileProperty_ds" columns="2" showOnLoad="false" layoutRegion="centerPos">
		<fieldset>
			<title>Details</title>
			<field name="name" readOnly="true"/>
			<field name="property_type" readOnly="true"/>
			<field name="use1" readOnly="true"/>
			<field name="zoning" readOnly="true"/>
			<field name="status" readOnly="true"/>
			<field name="contact1" readOnly="true"/>
			<field name="contact2" readOnly="true" hidden="${record['property.contact2'].length == 0}"/>
			<field name="ac_id" readOnly="true" hidden="${record['property.ac_id'].length == 0}"/>
			<field name="unit" readOnly="true" hidden="${record['property.unit'].length == 0}"/>
			<field name="vf_owner_custodian" dataType="text" readOnly="true" hidden="${record['property.vf_owner_custodian'].length == 0}">
				<title>Owner Custodian</title>
			</field>
		</fieldset>
		<fieldset>
			<title>Location</title>
			<field name="site_id" readOnly="true"/>
			<field name="vf_address1" dataType="text" readOnly="true" hidden="${record['property.vf_address1'].length == 0}">
				<title>Address</title>
			</field>
			<field name="vf_address2" dataType="text" readOnly="true" hidden="${record['property.vf_address2'].length == 0}">
				<title></title>
			</field>
			<field name="regn_id" readOnly="true" hidden="${record['property.regn_id'].length == 0}"/>
			<field name="ctry_id" readOnly="true"/>
			<field name="vicinity" readOnly="true" hidden="${record['property.vicinity'].length == 0}"/>
			<field name="lat" readOnly="true"/>
			<field name="lon" readOnly="true"/>
		</fieldset>
		<fieldset>
			<title>Size and Occupancy</title>
			<field name="area_manual" decimals="0" readOnly="true"/>
			<field name="area_cad" decimals="0" readOnly="true"/>
			<field name="area_bl_rentable" decimals="0" readOnly="true" hidden="${record['property.area_bl_rentable'] == 0.0}"/>
			<field name="area_bl_est_rentable" decimals="0" readOnly="true" hidden="${record['property.area_bl_est_rentable'] == 0.0}"/>
			<field name="area_lease_neg" decimals="0" readOnly="true" hidden="${record['property.area_lease_neg'] == 0.0}"/>
			<field name="area_land_acres" decimals="0" readOnly="true" hidden="${record['property.area_land_acres'] == 0 || record['property.area_land_acres'].length == 0}"/>
			<field name="qty_no_bldgs" readOnly="true"/>
			<field name="qty_no_spaces" readOnly="true"/>
			<field name="qty_occupancy" readOnly="true"/>
		</fieldset>
		<fieldset>
			<title>Value</title>
			<field name="value_book" dataType="number" size="20" decimals="0" readOnly="true" currency="${user.userCurrency.code}">
				<title>Value - Book</title>
			</field>
			<field name="date_book_val" readOnly="true"/>
			<field name="value_market" dataType="number" size="20" decimals="0" readOnly="true" currency="${user.userCurrency.code}">
				<title>Value - Market</title>
			</field>
			<field name="date_market_val" readOnly="true"/>
			<field name="cost_purchase" dataType="number" size="20" decimals="0" readOnly="true" currency="${user.userCurrency.code}">
				<title>Purchase Price</title>
			</field>
			<field name="date_purchase" dataType="date" readOnly="true">
				<title>Date Purchase</title>
			</field>
			<field name="pct_own" readOnly="true"/>
		</fieldset>
	</panel>
	<dataSource id="abProfilePropertyCustodian_ds">
		<table name="team" role="main"/>
        <table name="em" role="standard"/>
        <table name="contact" role="standard"/>
        <table name="vn" role="standard"/>
		<field table="team" name="custodian_name" dataType="text" size="250">
			<sql dialect="generic">
				(CASE
                    WHEN team.source_table = 'vn' AND vn.company IS NOT NULL THEN vn.company
                    WHEN team.source_table = 'vn' THEN team.vn_id
                    WHEN team.source_table = 'contact' AND (contact.name_first IS NOT NULL OR contact.name_last IS NOT NULL) THEN (contact.name_first ${sql.concat} ' ' ${sql.concat} contact.name_last)
                    WHEN team.source_table = 'contact' THEN team.contact_id
                    WHEN team.source_table = 'em' AND em.name IS NOT NULL THEN em.name
                    WHEN team.source_table = 'em' AND em.name IS NULL AND (em.name_first IS NOT NULL OR em.name_last IS NOT NULL) THEN (em.name_first ${sql.concat} ' ' ${sql.concat} em.name_last)
                    WHEN team.source_table = 'em' THEN team.em_id
				 ELSE NULL END)
			</sql>
		</field>
        <field table="team" name="autonumbered_id"/>
        <field table="team" name="custodian_type"/>
        <field table="team" name="status"/>
        <field table="team" name="email_archive"/>
        <field table="team" name="cell_num_archive"/>
        <field table="team" name="date_start"/>
        <field table="team" name="date_end"/>
		<field table="team" name="pr_id"/>
        <field table="contact" name="contact_id"/>
        <field table="vn" name="vn_id"/>
        <field table="em" name="em_id"/>
        <parameter name="isTeamStatusActive" dataType="boolean" value="false"/>
        <parameter name="isTeamOwner" dataType="boolean" value="false"/>
        <restriction type="sql" enabled="isTeamStatusActive" sql="team.status='Active'"/>
        <restriction type="sql" enabled="isTeamOwner" sql="team.is_owner=1"/>
        <restriction type="sql" sql="team.team_type='Equipment' AND team.custodian_type IS NOT NULL"/>
	</dataSource>
	<panel type="grid" controlType="reportGrid" id="abProfilePropertyCustodian_list" dataSource="abProfilePropertyCustodian_ds" showOnLoad="false" showIfNoData="false" layoutRegion="centerPos">
        <event type="onClickItem">
            <command type="callFunction" functionName="showCustodian"/>
        </event>
		<sortField table="team" name="status" ascending="true"/>
		<sortField table="team" name="is_owner" ascending="false"/>
		<sortField table="team" name="date_start" ascending="false"/>
		<field table="team" name="custodian_name" dataType="text" size="250">
			<title>Asset Custodian</title>
		</field>
		<field table="team" name="custodian_type"/>
		<field table="team" name="status">
			<title>Status</title>
		</field>
		<field table="team" name="email_archive">
			<title>Email</title>
		</field>
		<field table="team" name="cell_num_archive"/>
		<field table="team" name="date_start">
			<title>Start Date</title>
		</field>
		<field table="team" name="date_end">
			<title>End Date</title>
		</field>
		<field table="team" name="pr_id" hidden="true"/>
        <field table="team" name="autonumbered_id" hidden="true"/>
	</panel>
	<panel type="form" id="abProfilePropertyMap_form" dataSource="abProfileProperty_ds" columns="1" showOnLoad="false" hidden="true" layoutRegion="centerPos">
		<field name="image_map" displayField="image_map_field" hidden="true"/>
		<field id="image_map_field" controlType="image"/>
		<field name="pr_id" hidden="true"/>
	</panel>
    <css file="ab-profile-style.css"/>
	<js file="ab-query-parameters-utility.js"/>
	<js file="ab-profile-property.js"/>
</view>