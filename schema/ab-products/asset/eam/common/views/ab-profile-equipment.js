View.createController('abProfileEquipmentCtrl', {
    // query parameters
    queryParameterNames: ['eq_id', 'isTeamOwner'],
    queryParameters: null,
    isDemoMode: false,
    // selected equipment code
    eqId: null,
    // passed parameter for custodian list
    isTeamOwner: false,
    isSiteOrgSchemaCompatible: false,
    afterViewLoad: function () {
        var openerDialog = View.getOpenerView().dialog;
        if (valueExists(openerDialog) && !valueExistsNotEmpty(openerDialog.title)) {
            openerDialog.setTitle(getMessage('equipmentProfileTitle'));
        }
        this.isSiteOrgSchemaCompatible = schemaHasTables(['site_org']);
        // hide field tooltip if SMS licence is not available
        if (checkLicense('AbEAMSMSBuilderExtension')) {
            this.addTooltip('abProfileEquipment_form', 'eq.campus_id');
            this.addTooltip('abProfileEquipment_form', 'eq.builder_sms_level');
        }
    },
    afterInitialDataFetch: function () {
        this.isDemoMode = isInDemoMode();
        this.queryParameters = readQueryParameters(this.queryParameterNames);
        // check for existing query parameters
        if (valueExists(this.queryParameters) && !_.isEmpty(this.queryParameters)) {
            this.eqId = this.queryParameters['eq_id'];
            this.isTeamOwner = !!this.queryParameters['isTeamOwner'] || false;
        } else if (valueExists(this.view.restriction)) {
            var clause = this.view.restriction.findClause('eq.eq_id');
            if (clause) {
                this.eqId = clause.value;
            }
        }
        this.showEquipment();
    },
    /**
     * Show equipment.
     */
    showEquipment: function () {
        if (valueExistsNotEmpty(this.eqId)) {
            var restriction = new Ab.view.Restriction({'eq.eq_id': this.eqId});
            this.abProfileEquipment_form.addParameter('useCustodian', isCustodianSchemaDefined);
            this.abProfileEquipment_form.addParameter('useSiteOrg', this.isSiteOrgSchemaCompatible);
            if (!this.isSiteOrgSchemaCompatible) {
                this.abProfileEquipment_form.showField('eq.vf_org_unit', false);
            }
            this.abProfileEquipmentPhoto_form.refresh(restriction, false);
            this.abProfileEquipment_form.refresh(restriction, false);
            this.abProfileEquipment_title.setTitle("{0} - {1}".replace("{0}", this.abProfileEquipment_form.getFieldValue("eq.eq_std")).replace("{1}", this.eqId));
            if (isCustodianSchemaDefined) {
                var applyTeamOwner = this.isTeamOwner;
                if (applyTeamOwner) {
                    this.abProfileEquipmentCustodian_list.addParameter('isTeamOwner', applyTeamOwner);
                    var custodianNameColumn = _.find(this.abProfileEquipmentCustodian_list.columns, function (column) {
                        return 'team.custodian_name' === column.id;
                    });
                    if (custodianNameColumn) {
                        custodianNameColumn.name = getMessage('ownerCustodianTitle');
                        this.abProfileEquipmentCustodian_list.showColumn('team.custodian_type', false);
                    }
                }
                this.abProfileEquipmentCustodian_list.addParameter('isTeamStatusActive', !applyTeamOwner);
                this.abProfileEquipmentCustodian_list.refresh(new Ab.view.Restriction({'team.eq_id': this.eqId}));
            } else {
                this.abProfileEquipmentCustodian_list.show(false);
            }
        }
    },
    abProfileEquipmentPhoto_form_afterRefresh: function (eqPhotoForm) {
        // show equipment asset attributes
        this.showAssetAttributes();
        // load image
        if (valueExistsNotEmpty(eqPhotoForm.getFieldValue('eq.survey_photo_eq'))) {
            eqPhotoForm.showImageDoc('image_field', 'eq.eq_id', 'eq.survey_photo_eq');
            eqPhotoForm.setFieldLabel('image_label', getMessage('labelEqSurveyPhoto'));
        } else if (valueExistsNotEmpty(eqPhotoForm.getFieldValue('eqstd.doc_graphic'))) {
            eqPhotoForm.showImageDoc('image_field', 'eqstd.eq_std', 'eqstd.doc_graphic');
            eqPhotoForm.setFieldLabel('image_label', getMessage('labelEqStandardPhoto'));
        } else {
            eqPhotoForm.showField('image_field', false);
        }
        if (valueExistsNotEmpty(eqPhotoForm.getFieldValue('eq.survey_redline_eq'))) {
            eqPhotoForm.showImageDoc('redline_field', 'eq.eq_id', 'eq.survey_redline_eq');
        } else {
            eqPhotoForm.showField('redline_field', false);
        }
    },
    showAssetAttributes: function () {
        if (valueExistsNotEmpty(this.eqId)) {
            if(valueExists(View.dataSources.get('ds_abEqAssetAttributes'))) {
                var records = View.dataSources.get('ds_abEqAssetAttributes').getRecords(new Ab.view.Restriction({
                    'eq_asset_attribute.eq_id': this.eqId,
                    'asset_attribute_std.asset_type': 'Equipment'
                }));
                var attributes = '';
                for ( var i = 0; i < records.length; i++ ) {
                    var title = records[i].getValue('asset_attribute_std.title');
                    var value = records[i].getValue('eq_asset_attribute.value');
                    attributes += title + ': ' + value + '<br>';
                }
                if (valueExistsNotEmpty(attributes)) {
                    jQuery('#eqAttributesDiv').html(attributes);
                } else {
                    jQuery('#abProfileEquipmentPhoto_form_eqAssetAttributes_labelCell').parent().hide();
                }
            } else {
                jQuery('#abProfileEquipmentPhoto_form_eqAssetAttributes_labelCell').parent().hide();
            }
        }
    },
    /**
     * On export menu action.
     * @param {string} outputType PDF|DOCX
     */
    onExport: function (outputType) {
        if ('DOCX' === outputType) {
            this.onExportToDocx();
        }
    },
    /**
     * On export to DOC handler.
     *
     */
    onExportToDocx: function () {
        if (isCustodianSchemaDefined) {
            View.openPaginatedReportDialog('ab-profile-equipment-pgrp.axvw', {
                'abProfileEquipment_ds': this.abProfileEquipment_form.restriction,
                'abProfileEquipmentCustodian_ds': this.getCustodianPanelRestriction()
            });
        } else {
            View.showMessage(getMessage('backwardCompatibilityMessage'));
        }
    },
    getCustodianPanelRestriction: function () {
        var custodianPanel = View.panels.get('abProfileEquipmentCustodian_list');
        var custodianRestriction = custodianPanel.restriction;
        if (valueExists(custodianPanel.parameters)) {
            var isTeamOwner = custodianPanel.parameters.isTeamOwner;
            var isTeamStatusActive = custodianPanel.parameters.isTeamStatusActive;
            if (isTeamOwner) {
                custodianRestriction.addClause('team.is_owner', '1');
            }
            if (isTeamStatusActive) {
                custodianRestriction.addClause('team.status', 'Active');
            }
        }
        return custodianRestriction;
    },
    showCustodian: function (selectedRecord, restriction) {
        var isOwner = selectedRecord.getValue('team.is_owner') === "1";
        View.openDialog('ab-eam-edit-custodian.axvw', restriction, false, {
            assetType: 'eq',
            assetCode: this.eqId,
            isOwner: isOwner,
            isReadOnly: true,
            title: getMessage('custodianDetailsPanelTitle')
        });
    },
    /**
     * Add tooltip on form field when field is readonly.
     * @param {string} panelName Panel name.
     * @param {string} fieldName Field name.
     * @param {string}[tooltip] Optional, if not defined, the tooltip defined in the view form will be displayed.
     */
    addTooltip: function (panelName, fieldName, tooltip) {
        var imgElement = document.createElement("img");
        imgElement.id = panelName + "_" + fieldName;
        imgElement.src = this.view.contextPath + "/schema/ab-core/graphics/icons/help.png";
        imgElement.className = 'fieldTooltipButton';
        var tooltipTag = (Ext.isIE) ? 'title' : 'ext:qtip';
        tooltip = tooltip || View.panels.get(panelName).fields.get(fieldName).fieldDef.tooltip;
        imgElement.setAttribute(tooltipTag, tooltip);
        Ext.get(imgElement).on("click", function () {
            View.alert(tooltip);
        });
        var parent = View.panels.get(panelName).getFieldCell(fieldName);
        parent.appendChild(imgElement);
    }
});

function showCustodian(context) {
    var parentPanel = View.panels.get(context.command.parentPanelId);
    var selectedRecord = parentPanel.gridRows.get(parentPanel.selectedRowIndex).getRecord();
    View.controllers.get('abProfileEquipmentCtrl').showCustodian(selectedRecord, context.command.restriction);
}

/**
 * Check license.
 * @param {string} licenseId - license to check
 * @returns {boolean} has license enabled
 */
function checkLicense(licenseId) {
    var hasLicense = false;
    AdminService.getProgramLicense({
        async: false,
        callback: function (licenseDTO) {
            for ( var i = 0; i < licenseDTO.licenses.length; i++ ) {
                var license = licenseDTO.licenses[i];
                if (licenseId === license.id) {
                    hasLicense = license.enabled;
                    break;
                }
            }
        },
        errorHandler: function (m, e) {
            View.showException(e);
        }
    });
    return hasLicense;
}