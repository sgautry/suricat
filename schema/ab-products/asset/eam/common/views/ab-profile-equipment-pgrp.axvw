<view version="2.0">
	<title>Equipment Profile</title>
	<report/>
	<dataSource id="abProfileEquipment_ds">
		<table name="eq" role="main"/>
		<table name="eqstd" role="standard"/>
		<field table="eq" name="eq_id"/>
		<field table="eq" name="survey_photo_eq"/>
		<field table="eqstd" name="eq_std"/>
		<field table="eqstd" name="doc_graphic"/>
		<field table="eqstd" name="category"/>
		<field table="eqstd" name="description"/>
		<field table="eq" name="description"/>
		<field table="eq" name="comments"/>
		<field table="eq" name="survey_comments"/>
		<field table="eq" name="survey_redline_eq"/>
		<field table="eq" name="eq_std"/>
		<field table="eq" name="num_serial"/>
		<field table="eq" name="subcomponent_of"/>
		<field table="eq" name="use1"/>
		<field table="eq" name="site_id"/>
		<field table="eq" name="bl_id"/>
		<field table="eq" name="fl_id"/>
		<field table="eq" name="rm_id"/>
		<field table="eq" name="loc_column"/>
		<field table="eq" name="loc_bay"/>
		<field table="eq" name="pr_id"/>
		<field table="eq" name="lat"/>
		<field table="eq" name="lon"/>
		<field table="eq" name="dv_id"/>
		<field table="eq" name="dp_id"/>
		<field table="eq" name="em_id"/>
		<field table="eq" name="date_installed"/>
		<field table="eq" name="date_in_service"/>
		<field table="eq" name="status"/>
		<field table="eq" name="condition"/>
		<field table="eq" name="date_of_stat_chg"/>
		<field table="eq" name="date_purchased"/>
		<field table="eq" name="cost_purchase"/>
		<field table="eq" name="cost_replace"/>
        <field table="eq" name="criticality"/>
        <field table="eq" name="doc_sop"/>
        <field table="eq" name="doc_eop"/>
        <field table="eq" name="doc_mop"/>
        <field table="eq" name="doc_loto"/>
        <field table="eq" name="doc_other"/>
        <field table="eq" name="vf_modelno" dataType="text">
            <sql dialect="generic">
                (CASE WHEN eq.modelno IS NULL THEN (SELECT eqstd.modelno FROM eqstd WHERE eqstd.eq_std = eq.eq_std) ELSE eq.modelno END)
            </sql>
        </field>
        <field table="eq" name="vf_csi_id" dataType="text">
            <sql dialect="generic">
                (CASE WHEN eq.csi_id IS NULL THEN (SELECT eqstd.csi_id FROM eqstd WHERE eqstd.eq_std = eq.eq_std) ELSE eq.csi_id END)
            </sql>
        </field>
        <field table="eq" name="vf_bu" dataType="text">
            <sql dialect="generic">
                (SELECT dv.bu_id FROM dv WHERE dv.dv_id = eq.dv_id)
            </sql>
        </field>
        <field table="eq" name="vf_mfr" dataType="text">
            <sql dialect="generic">
                (CASE WHEN eq.mfr IS NULL THEN (SELECT eqstd.mfr FROM eqstd WHERE eqstd.eq_std = eq.eq_std) ELSE eq.mfr END)
            </sql>
        </field>
		<field table="eq" name="vf_dep_value" dataType="number" size="20" decimals="2">
            <sql dialect="generic">
                (SELECT eq_dep.value_current 
                    FROM eq_dep 
                 WHERE eq_dep.eq_id = eq.eq_id
                    AND eq_dep.report_id =(SELECT TOP 1 dep_reports.report_id FROM dep_reports, eq_dep eq_dep2 WHERE eq_dep2.report_id=dep_reports.report_id AND dep_reports.last_date = (SELECT MAX(dep_reports.last_date) FROM dep_reports)))
            </sql>
            <sql dialect="oracle">
                (SELECT eq_dep.value_current 
                    FROM eq_dep 
                 WHERE eq_dep.eq_id = eq.eq_id
                    AND eq_dep.report_id =(SELECT dep_reports.report_id FROM dep_reports, eq_dep eq_dep2 WHERE eq_dep2.report_id=dep_reports.report_id AND dep_reports.last_date = (SELECT MAX(dep_reports.last_date) FROM dep_reports) AND rownum=1))
            </sql>
		</field>
        <field table="eq" name="vf_owner_custodian" dataType="text">
            <sql dialect="generic">
                (SELECT
                    (CASE
                        WHEN team.source_table = 'vn' AND vn.company IS NOT NULL THEN vn.company
                        WHEN team.source_table = 'vn' THEN team.vn_id
                        WHEN team.source_table = 'contact' AND (contact.name_first IS NOT NULL OR contact.name_last IS NOT NULL) THEN (contact.name_first ${sql.concat} ' ' ${sql.concat} contact.name_last)
                        WHEN team.source_table = 'contact' THEN team.contact_id
                        WHEN team.source_table = 'em' AND em.name IS NOT NULL THEN em.name
                        WHEN team.source_table = 'em' AND em.name IS NULL AND (em.name_first IS NOT NULL OR em.name_last IS NOT NULL) THEN (em.name_first ${sql.concat} ' ' ${sql.concat} em.name_last)
                        WHEN team.source_table = 'em' THEN team.em_id
                     ELSE NULL END)
                 FROM team
                    LEFT OUTER JOIN contact ON team.contact_id=contact.contact_id
                    LEFT OUTER JOIN vn ON team.vn_id=vn.vn_id
                    LEFT OUTER JOIN em ON team.em_id=em.em_id
                        WHERE team.is_owner = 1 AND team.eq_id = eq.eq_id AND team.team_type = 'Equipment' AND team.status = 'Active')
            </sql>
        </field>
		<field name="vf_dummy" dataType="text">
			<sql dialect="generic">''</sql>
		</field>
	</dataSource>
	<dataSource id="abProfileEquipmentCustodian_ds">
		<table name="team" role="main"/>
        <table name="em" role="standard"/>
        <table name="contact" role="standard"/>
        <table name="vn" role="standard"/>
        <table name="custodiantype" role="standard"/>
		<field name="vf_custodian_name" dataType="text" size="250">
			<sql dialect="generic">
				(CASE
                    WHEN team.source_table = 'vn' AND vn.company IS NOT NULL THEN vn.company
                    WHEN team.source_table = 'vn' THEN team.vn_id
                    WHEN team.source_table = 'contact' AND (contact.name_first IS NOT NULL OR contact.name_last IS NOT NULL) THEN (contact.name_first ${sql.concat} ' ' ${sql.concat} contact.name_last)
                    WHEN team.source_table = 'contact' THEN team.contact_id
                    WHEN team.source_table = 'em' AND em.name IS NOT NULL THEN em.name
                    WHEN team.source_table = 'em' AND em.name IS NULL AND (em.name_first IS NOT NULL OR em.name_last IS NOT NULL) THEN (em.name_first ${sql.concat} ' ' ${sql.concat} em.name_last)
                    WHEN team.source_table = 'em' THEN team.em_id
				 ELSE NULL END)
			</sql>
		</field>
        <field table="custodiantype" name="custodian_type"/>
        <field table="custodiantype" name="custodian_type_01"/>
        <field table="custodiantype" name="custodian_type_02"/>
        <field table="custodiantype" name="custodian_type_03"/>
        <field table="custodiantype" name="custodian_type_ch"/>
        <field table="custodiantype" name="custodian_type_de"/>
        <field table="custodiantype" name="custodian_type_es"/>
        <field table="custodiantype" name="custodian_type_fr"/>
        <field table="custodiantype" name="custodian_type_it"/>
        <field table="custodiantype" name="custodian_type_jp"/>
        <field table="custodiantype" name="custodian_type_ko"/>
        <field table="custodiantype" name="custodian_type_nl"/>
        <field table="custodiantype" name="custodian_type_no"/>
        <field table="custodiantype" name="custodian_type_zh"/>
        <field name="status"/>
        <field name="email_archive"/>
        <field name="cell_num_archive"/>
        <field name="date_start"/>
        <field name="date_end"/>
		<field name="eq_id"/>
        <field table="contact" name="contact_id"/>
        <field table="vn" name="vn_id"/>
        <field table="em" name="em_id"/>
        <sortField name="status" ascending="true"/>
        <sortField name="is_owner" ascending="false"/>
        <sortField name="date_start" ascending="false"/>
		<restriction type="sql" sql="team.team_type='Equipment' AND team.custodian_type IS NOT NULL"/>
	</dataSource>
	<panel type="paginatedReport" id="abProfileEquipment_details1" dataSource="abProfileEquipment_ds" format="table" role="data">
		<field table="eq" name="eq_id"/>
		<field table="eqstd" name="category">
			<title>Category</title>
		</field>
		<field table="eqstd" name="description">
			<title>Equipment Standard Description</title>
		</field>
		<field table="eq" name="description">
			<title>Equipment Description</title>
		</field>
		<field table="eq" name="comments"/>
		<field table="eq" name="survey_comments"/>
		<field table="eqstd" name="doc_graphic"/>
		<field table="eq" name="survey_photo_eq"/>
		<field table="eq" name="survey_redline_eq"/>
	</panel>
	<panel type="paginatedReport" id="abProfileEquipment_details2" dataSource="abProfileEquipment_ds" format="column" columns="2" role="data">
		<field table="eq" name="eq_id"/>
		<field table="eq" name="site_id"/>
		<field table="eq" name="eq_std"/>
		<field table="eq" name="bl_id"/>
		<field table="eq" name="vf_mfr">
		  <title>Manufacturer</title>
		</field>
		<field table="eq" name="fl_id"/>
		<field table="eq" name="vf_modelno">
		<title>Model Number</title>
		</field>
		<field table="eq" name="rm_id"/>
		<field table="eq" name="num_serial"/>
		<field table="eq" name="loc_column"/>
		<field table="eq" name="subcomponent_of"/>
		<field table="eq" name="loc_bay"/>
		<field table="eq" name="use1"/>
		<field table="eq" name="pr_id"/>
		<field table="eq" name="vf_csi_id">
		  <title>Classification Code</title>
		</field>
		<field table="eq" name="lat"/>
		<field name="vf_dummy"/>
		<field table="eq" name="lon"/>

		<field name="vf_dummy"/>
		<field name="vf_dummy"/>

		<field table="eq" name="vf_bu">
		  <title>Business Unit</title>
		</field>
		<field table="eq" name="date_installed"/>
		<field table="eq" name="dv_id"/>
		<field table="eq" name="date_in_service"/>
		<field table="eq" name="dp_id"/>
		<field table="eq" name="status"/>
		<field table="eq" name="em_id"/>
		<field table="eq" name="condition"/>
		<field table="eq" name="vf_owner_custodian">
		  <title>Owner Custodian</title>
		</field>
		<field table="eq" name="date_of_stat_chg"/>
		<field table="eq" name="date_purchased"/>
		<field table="eq" name="cost_purchase"/>
		<field table="eq" name="cost_replace"/>
		<field table="eq" name="vf_dep_value">
		  <title>Depreciated Value</title>
		</field>
		<field table="eq" name="criticality"/>
        <!-- Documents -->
		<field table="eq" name="doc_sop"/>
		<field table="eq" name="doc_eop"/>
		<field table="eq" name="doc_mop"/>
		<field table="eq" name="doc_loto"/>
		<field table="eq" name="doc_other"/>
	</panel>
	<panel type="paginatedReport" id="abProfileEquipment_custodian" dataSource="abProfileEquipmentCustodian_ds" format="table" role="data">
		<field name="vf_custodian_name">
			<title>Asset Custodian</title>
		</field>
		<field table="custodiantype" name="custodian_type"/>
		<field name="status">
			<title>Status</title>
		</field>
		<field name="email_archive">
			<title>Email</title>
		</field>
		<field name="cell_num_archive"/>
		<field name="date_start">
			<title>Start Date</title>
		</field>
		<field name="date_end">
			<title>End Date</title>
		</field>
	</panel>
</view>