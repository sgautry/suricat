View.createController('abProfileFurnitureCtrl', {
    // query parameters
    queryParameterNames: ['ta_id', 'isTeamOwner'],
    queryParameters: null,
    isDemoMode: false,
    // selected furniture code
    taId: null,
    // passed parameter for custodian list
    isTeamOwner: false,
    afterViewLoad: function () {
        var openerDialog = View.getOpenerView().dialog;
        if (valueExists(openerDialog) && !valueExistsNotEmpty(openerDialog.title)) {
            openerDialog.setTitle(getMessage('furnitureProfileTitle'));
        }
    },
    afterInitialDataFetch: function () {
        this.isDemoMode = isInDemoMode();
        this.queryParameters = readQueryParameters(this.queryParameterNames);
        // check for existing query parameters
        if (valueExists(this.queryParameters) && !_.isEmpty(this.queryParameters)) {
            this.taId = this.queryParameters['ta_id'];
            this.isTeamOwner = !!this.queryParameters['isTeamOwner'] || false;
        } else if (valueExists(this.view.restriction)) {
            var clause = this.view.restriction.findClause('ta.ta_id');
            if (clause) {
                this.taId = clause.value;
            }
        }
        this.showFurniture();
    },
    /**
     * Show furniture.
     */
    showFurniture: function () {
        if (valueExistsNotEmpty(this.taId)) {
            var restriction = new Ab.view.Restriction({'ta.ta_id': this.taId});
            this.abProfileTa_form.addParameter('useCustodian', isCustodianSchemaDefined);
            this.abProfileTaPhoto_form.refresh(restriction, false);
            this.abProfileTa_form.refresh(restriction, false);
            this.abProfileTa_title.setTitle("{0} - {1}".replace("{0}", this.abProfileTa_form.getFieldValue("ta.fn_std")).replace("{1}", this.taId));
            if (isCustodianSchemaDefined) {
                var applyTeamOwner = this.isTeamOwner;
                if (applyTeamOwner) {
                    this.abProfileTaCustodian_list.addParameter('isTeamOwner', applyTeamOwner);
                    var custodianNameColumn = _.find(this.abProfileTaCustodian_list.columns, function (column) {
                        return 'team.custodian_name' === column.id;
                    });
                    if (custodianNameColumn) {
                        custodianNameColumn.name = getMessage('ownerCustodianTitle');
                    }
                }
                this.abProfileTaCustodian_list.addParameter('isTeamStatusActive', !applyTeamOwner);
                this.abProfileTaCustodian_list.refresh(new Ab.view.Restriction({'team.ta_id': this.taId}));
            } else {
                this.abProfileTaCustodian_list.show(false);
            }
        }
    },
    abProfileTaPhoto_form_afterRefresh: function (taPhotoForm) {
        // load image
        if (valueExistsNotEmpty(taPhotoForm.getFieldValue('fnstd.doc_graphic'))) {
            taPhotoForm.showImageDoc('image_field', 'fnstd.fn_std', 'fnstd.doc_graphic');
        } else {
            taPhotoForm.showField('image_field', false);
        }
    },
    /**
     * On export menu action.
     * @param {string} outputType PDF|DOCX
     */
    onExport: function (outputType) {
        if ('DOCX' === outputType) {
            this.onExportToDocx();
        }
    },
    /**
     * On export to DOC handler.
     *
     */
    onExportToDocx: function () {
        if (isCustodianSchemaDefined) {
            var restriction = this.abProfileTa_form.restriction;
            View.openPaginatedReportDialog('ab-profile-ta-pgrp.axvw', {
                'abProfileTa_ds': restriction,
                'abProfileTaCustodian_ds': this.getCustodianPanelRestriction()
            });
        } else {
            View.showMessage(getMessage('backwardCompatibilityMessage'));
        }
    },
    getCustodianPanelRestriction: function () {
        var custodianPanel = View.panels.get('abProfileTaCustodian_list');
        var custodianRestriction = custodianPanel.restriction;
        if (valueExists(custodianPanel.parameters)) {
            var isTeamOwner = custodianPanel.parameters.isTeamOwner;
            var isTeamStatusActive = custodianPanel.parameters.isTeamStatusActive;
            if (isTeamOwner) {
                custodianRestriction.addClause('team.is_owner', '1');
            }
            if (isTeamStatusActive) {
                custodianRestriction.addClause('team.status', 'Active');
            }
        }
        return custodianRestriction;
    },
    showCustodian: function (selectedRecord, restriction) {
        var isOwner = selectedRecord.getValue('team.is_owner') === "1";
        View.openDialog('ab-eam-edit-custodian.axvw', restriction, false, {
            assetType: 'ta',
            assetCode: this.taId,
            isOwner: isOwner,
            isReadOnly: true,
            title: getMessage('custodianDetailsPanelTitle')
        });
    }
});

function showCustodian(context) {
    var parentPanel = View.panels.get(context.command.parentPanelId);
    var selectedRecord = parentPanel.gridRows.get(parentPanel.selectedRowIndex).getRecord();
    View.controllers.get('abProfileFurnitureCtrl').showCustodian(selectedRecord, context.command.restriction);
}