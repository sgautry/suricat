View.createController('abEamFloorPlanController', {
    drawingController: null,
    afterViewLoad: function () {
        if (valueExists(this.view.parameters) && valueExists(this.view.parameters.drawingConfig)) {
            // do nothing, let the drawing controller handle this
        } else {
            this.drawingController = View.createControl({
                control: 'AssetDrawingControl',
                panel: 'drawingPanel',
                container: 'svgDiv',
                assetTypes: ['rm', 'eq', 'jk', 'fp', 'pn'],
                diagonalSelection: {borderWidth: 10},
                highlightParameters: [{
                    'view_file': "ab-drawing-controller-common-ds.axvw",
                    'hs_ds': "highlightDivisionsDs",
                    'label_ds': 'labelNamesDs',
                    'label_ht': '0.60'
                }],
                layersPopup: {
                    layers: "rm-assets;rm-labels;eq-assets;eq-labels;jk-assets;jk-labels;fp-assets;fp-labels;pn-assets;pn-labels;gros-assets;gros-labels;background",
                    defaultLayers: "rm-assets;rm-labels;eq-assets;jk-assets;fp-assets;pn-assets;gros-assets;background"
                },
                assetTooltip: [
                    {assetType: 'rm', datasource: 'tooltipRmDs', fields: 'rm.rm_id;rm.rm_std'},
                    {assetType: 'eq', datasource: 'tooltipEqDs', fields: 'eq.eq_id;eq.eq_std'},
                    {assetType: 'jk', datasource: 'tooltipJkDs', fields: 'jk.jk_id;jk.jk_std'},
                    {assetType: 'fp', datasource: 'tooltipFpDs', fields: 'fp.fp_id;fp.fp_std'},
                    {assetType: 'pn', datasource: 'tooltipPnDs', fields: 'pn.pn_id;pn.pn_std'}
                ],
                showPanelTitle: false,
                maximize: {
                    viewName: 'ab-eam-floor-plan.axvw'
                },
                listeners: {
                    // Set legend text according the legend level value.
                    onRenderLegendLabel: function (row, column, cellElement) {
                        var value = row[column.id];
                        if (column.id == 'legend.value' && value != '') {
                            var text = value;
                            switch (value) {
                                case '0':
                                    text = getMessage('legendLevel0');
                                    break;
                                case '1':
                                    text = getMessage('legendLevel1');
                                    break;
                                case '2':
                                    text = getMessage('legendLevel2');
                                    break;
                                case '3':
                                    text = getMessage('legendLevel3');
                                    break;
                                case '4':
                                    text = getMessage('legendLevel4');
                                    break;
                            }
                            var contentElement = cellElement.childNodes[0];
                            if (contentElement) {
                                contentElement.nodeValue = text;
                            }
                        }
                    }
                }
            });
        }
    },
    afterInitialDataFetch: function () {
        if (valueExists(this.view.restriction)) {
            this.applyDrawingRestriction(this.view.restriction);
        }
    },
    applyDrawingRestriction: function (restriction) {
        var location = this.getLocation(restriction);
        var blId = location.blId,
            flId = location.flId;
        var existsAssetParameters = (View.parameters && View.parameters.assetParameters) || false;
        if (!existsAssetParameters) {
            this.showDrawing(blId, flId);
        }
    },
    getLocation: function (restriction) {
        var blId = null;
        if (restriction && restriction.findClause('fl.bl_id')) {
            blId = restriction.findClause('fl.bl_id').value;
        } else if (restriction && restriction.findClause('rm.bl_id')) {
            blId = restriction.findClause('rm.bl_id').value;
        }
        var flId = null;
        if (restriction && restriction.findClause('fl.fl_id')) {
            flId = restriction.findClause('fl.fl_id').value;
        } else if (restriction && restriction.findClause('rm.fl_id')) {
            flId = restriction.findClause('rm.fl_id').value;
        }
        return {
            blId: blId,
            flId: flId
        }
    },
    /**
     * Refresh drawing by location.
     * Called from other views.
     * @param restriction
     */
    refreshDrawingLocation: function (restriction) {
        var location = this.getLocation(restriction);
        var blId = location.blId,
            flId = location.flId;
        this.showDrawing(blId, flId);
    },
    /**
     * Show drawing.
     * @param blId
     * @param flId
     */
    showDrawing: function (blId, flId) {
        this.drawingController.showDrawing({
            pkeyValues: {blId: blId, flId: flId}
        });
    }
});