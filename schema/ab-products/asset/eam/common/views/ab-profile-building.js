View.createController('abProfileBlCtrl', {
    // query parameters
    queryParameterNames: ['bl_id', 'isTeamOwner'],
    queryParameters: null,
    isDemoMode: false,
    // selected building code
    blId: null,
    // passed parameter for custodian list
    isTeamOwner: false,
    afterViewLoad: function () {
        var openerDialog = View.getOpenerView().dialog;
        if (valueExists(openerDialog) && !valueExistsNotEmpty(openerDialog.title)) {
            openerDialog.setTitle(getMessage('buildingProfileTitle'));
        }
    },
    afterInitialDataFetch: function () {
        this.isDemoMode = isInDemoMode();
        this.queryParameters = readQueryParameters(this.queryParameterNames);
        // check for existing query parameters
        if (valueExists(this.queryParameters) && !_.isEmpty(this.queryParameters)) {
            this.blId = this.queryParameters['bl_id'];
            this.isTeamOwner = !!this.queryParameters['isTeamOwner'] || false;
        } else if (valueExists(this.view.restriction)) {
            var clause = this.view.restriction.findClause('bl.bl_id');
            if (clause) {
                this.blId = clause.value;
            }
        }
        this.showBuilding();
    },
    /**
     * Show building.
     */
    showBuilding: function () {
        if (valueExistsNotEmpty(this.blId)) {
            var restriction = new Ab.view.Restriction({'bl.bl_id': this.blId});
            this.abProfileBuilding_form.addParameter('useCustodian', isCustodianSchemaDefined);
            this.abProfileBuildingPhoto_form.refresh(restriction, false);
            this.abProfileBuilding_form.refresh(restriction, false);
            this.abProfileBuilding_title.setTitle("{0} - {1}".replace("{0}", this.abProfileBuilding_form.getFieldValue("bl.name")).replace("{1}", this.blId));
            if (isCustodianSchemaDefined) {
                var applyTeamOwner = this.isTeamOwner;
                if (applyTeamOwner) {
                    this.abProfileBuildingCustodian_list.addParameter('isTeamOwner', applyTeamOwner);
                    var custodianNameColumn = _.find(this.abProfileBuildingCustodian_list.columns, function (column) {
                        return 'team.custodian_name' === column.id;
                    });
                    if (custodianNameColumn) {
                        custodianNameColumn.name = getMessage('ownerCustodianTitle');
                    }
                }
                this.abProfileBuildingCustodian_list.addParameter('isTeamStatusActive', !applyTeamOwner);
                this.abProfileBuildingCustodian_list.refresh(new Ab.view.Restriction({'team.bl_id': this.blId}));
            } else {
                this.abProfileBuildingCustodian_list.show(false);
            }
        }
    },
    abProfileBuildingPhoto_form_afterRefresh: function (blPhotoForm) {
        // load image
        if (valueExistsNotEmpty(blPhotoForm.getFieldValue('bl.bldg_photo'))) {
            blPhotoForm.showImageDoc('image_field', 'bl.bl_id', 'bl.bldg_photo');
        } else {
            blPhotoForm.showField('image_field', false);
        }
    },
    /**
     * On export menu action.
     * @param {string} outputType PDF|DOCX
     */
    onExport: function (outputType) {
        if ('DOCX' === outputType) {
            this.onExportToDocx();
        }
    },
    /**
     * On export to DOC handler.
     *
     */
    onExportToDocx: function () {
        if (isCustodianSchemaDefined) {
            View.openPaginatedReportDialog('ab-profile-building-pgrp.axvw', {
                'abProfileBuilding_ds': this.abProfileBuilding_form.restriction,
                'abProfileBuildingCustodian_ds': this.getCustodianPanelRestriction()
            });
        } else {
            View.showMessage(getMessage('backwardCompatibilityMessage'));
        }
    },
    getCustodianPanelRestriction: function () {
        var custodianPanel = View.panels.get('abProfileBuildingCustodian_list');
        var custodianRestriction = custodianPanel.restriction;
        if (valueExists(custodianPanel.parameters)) {
            var isTeamOwner = custodianPanel.parameters.isTeamOwner;
            var isTeamStatusActive = custodianPanel.parameters.isTeamStatusActive;
            if (isTeamOwner) {
                custodianRestriction.addClause('team.is_owner', '1');
            }
            if (isTeamStatusActive) {
                custodianRestriction.addClause('team.status', 'Active');
            }
        }
        return custodianRestriction;
    },
    showCustodian: function (selectedRecord, restriction) {
        var isOwner = selectedRecord.getValue('team.is_owner') === "1";
        View.openDialog('ab-eam-edit-custodian.axvw', restriction, false, {
            assetType: 'bl',
            assetCode: this.blId,
            isOwner: isOwner,
            isReadOnly: true,
            title: getMessage('custodianDetailsPanelTitle')
        });
    }
});
function showCustodian(context) {
    var parentPanel = View.panels.get(context.command.parentPanelId);
    var selectedRecord = parentPanel.gridRows.get(parentPanel.selectedRowIndex).getRecord();
    View.controllers.get('abProfileBlCtrl').showCustodian(selectedRecord, context.command.restriction);
}