View.createController('abProfilePropCtrl', {
    // query parameters
    queryParameterNames: ['pr_id', 'isTeamOwner'],
    queryParameters: null,
    isDemoMode: false,
    // selected property code
    prId: null,
    // passed parameter for custodian list
    isTeamOwner: false,
    afterViewLoad: function () {
        var openerDialog = View.getOpenerView().dialog;
        if (valueExists(openerDialog) && !valueExistsNotEmpty(openerDialog.title)) {
            openerDialog.setTitle(getMessage('propertyProfileTitle'));
        }
    },
    afterInitialDataFetch: function () {
        this.isDemoMode = isInDemoMode();
        this.queryParameters = readQueryParameters(this.queryParameterNames);
        // check for existing query parameters
        if (valueExists(this.queryParameters) && !_.isEmpty(this.queryParameters)) {
            this.prId = this.queryParameters['pr_id'];
            this.isTeamOwner = !!this.queryParameters['isTeamOwner'] || false;
        } else if (valueExists(this.view.restriction)) {
            var clause = this.view.restriction.findClause('property.pr_id');
            if (clause) {
                this.prId = clause.value;
            }
        }
        this.showProperty();
    },
    /**
     * Show property.
     */
    showProperty: function () {
        if (valueExistsNotEmpty(this.prId)) {
            var restriction = new Ab.view.Restriction({'property.pr_id': this.prId});
            this.abProfilePropertyPhoto_form.refresh(restriction, false);
            this.initImageMapField();
            this.abProfileProperty_form.addParameter('useCustodian', isCustodianSchemaDefined);
            this.abProfileProperty_form.refresh(restriction, false);
            this.abProfileProperty_title.setTitle("{0} - {1}".replace("{0}", this.abProfileProperty_form.getFieldValue("property.name")).replace("{1}", this.prId));
            if (isCustodianSchemaDefined) {
                var applyTeamOwner = this.isTeamOwner;
                if (applyTeamOwner) {
                    this.abProfilePropertyCustodian_list.addParameter('isTeamOwner', applyTeamOwner);
                    var custodianNameColumn = _.find(this.abProfilePropertyCustodian_list.columns, function (column) {
                        return 'team.custodian_name' === column.id;
                    });
                    if (custodianNameColumn) {
                        custodianNameColumn.name = getMessage('ownerCustodianTitle');
                    }
                }
                this.abProfilePropertyCustodian_list.addParameter('isTeamStatusActive', !applyTeamOwner);
                this.abProfilePropertyCustodian_list.refresh(new Ab.view.Restriction({'team.pr_id': this.prId}));
            } else {
                this.abProfilePropertyCustodian_list.show(false);
            }
        }
    },
    abProfilePropertyPhoto_form_afterRefresh: function (propertyPhotoForm) {
        // load property image
        if (valueExistsNotEmpty(propertyPhotoForm.getFieldValue('property.prop_photo'))) {
            propertyPhotoForm.showImageDoc('image_field', 'property.pr_id', 'property.prop_photo');
        } else {
            propertyPhotoForm.showField('image_field', false);
        }
        // load image map
        if (valueExistsNotEmpty(propertyPhotoForm.getFieldValue('property.image_map'))) {
            var imageFile = propertyPhotoForm.getFieldValue('property.image_map').toLowerCase();
            propertyPhotoForm.showImageFile('image_map_field', View.project.projectGraphicsFolder + "/" + imageFile);
        } else {
            propertyPhotoForm.showField('image_map_field', false);
        }
    },
    /**
     * Set onclick event and style.
     */
    initImageMapField: function () {
        var field = this.abProfilePropertyPhoto_form.fields.get('image_map_field');
        field.dom.onclick = this.openImageMapInDialog.createDelegate(this);
        field.dom.style.cursor = 'pointer';
    },
    /**
     * Open image map in dialog
     */
    openImageMapInDialog: function () {
        this.abProfilePropertyMap_form.refresh(new Ab.view.Restriction({'property.pr_id': this.prId}));
        if (valueExistsNotEmpty(this.abProfilePropertyMap_form.getFieldValue('property.image_map'))) {
            var imageFile = this.abProfilePropertyMap_form.getFieldValue('property.image_map').toLowerCase();
            this.abProfilePropertyMap_form.showImageFile('image_map_field', View.project.projectGraphicsFolder + "/" + imageFile);
        }
        this.abProfilePropertyMap_form.showInWindow({
            width: 800,
            height: 800,
            closeButton: true,
            collapsible: false
        });
    },
    /**
     * On export menu action.
     * @param {string} outputType PDF|DOCX
     */
    onExport: function (outputType) {
        if ('DOCX' === outputType) {
            this.onExportToDocx();
        }
    },
    /**
     * On export to DOC handler.
     *
     */
    onExportToDocx: function () {
        if (isCustodianSchemaDefined) {
            View.openPaginatedReportDialog('ab-profile-property-pgrp.axvw', {
                'abProfileProperty_ds': this.abProfileProperty_form.restriction,
                'abProfilePropertyCustodian_ds': this.getCustodianPanelRestriction()
            });
        } else {
            View.showMessage(getMessage('backwardCompatibilityMessage'));
        }
    },
    getCustodianPanelRestriction: function () {
        var custodianPanel = View.panels.get('abProfilePropertyCustodian_list');
        var custodianRestriction = custodianPanel.restriction;
        if (valueExists(custodianPanel.parameters)) {
            var isTeamOwner = custodianPanel.parameters.isTeamOwner;
            var isTeamStatusActive = custodianPanel.parameters.isTeamStatusActive;
            if (isTeamOwner) {
                custodianRestriction.addClause('team.is_owner', '1');
            }
            if (isTeamStatusActive) {
                custodianRestriction.addClause('team.status', 'Active');
            }
        }
        return custodianRestriction;
    },
    showCustodian: function (selectedRecord, restriction) {
        var isOwner = selectedRecord.getValue('team.is_owner') === "1";
        View.openDialog('ab-eam-edit-custodian.axvw', restriction, false, {
            assetType: 'property',
            assetCode: this.prId,
            isOwner: isOwner,
            isReadOnly: true,
            title: getMessage('custodianDetailsPanelTitle')
        });
    }
});

function showCustodian(context) {
    var parentPanel = View.panels.get(context.command.parentPanelId);
    var selectedRecord = parentPanel.gridRows.get(parentPanel.selectedRowIndex).getRecord();
    View.controllers.get('abProfilePropCtrl').showCustodian(selectedRecord, context.command.restriction);
}