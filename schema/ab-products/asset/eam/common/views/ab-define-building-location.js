var abDefineBuilding_tabLocationController = View.createController('abDefineBuilding_tabLocationController', {
    // selected building code
    blId: null,

    geocodeControl: null,

    callbackMethod: null,

    restriction: null,

    newRecord: null,
    // check backward compatible to 23.1 schema
    isSchemaCompatible: false,
    afterViewLoad: function () {
        // check backward compatible
        this.isSchemaCompatible = valueExists(View.dataSources.get('custodianType_ds')) && this.checkLicense('AbAssetEAM');
        var controller = this;
        this.abDefineBuilding_ownerCustodian.afterCreateCellContent = function (row, column, cellElement) {
            controller.customizeCustodianFields(row, column, cellElement);
        };
        this.abDefineBuilding_otherCustodian.afterCreateCellContent = function (row, column, cellElement) {
            controller.customizeCustodianFields(row, column, cellElement);
        };
    },
    afterInitialDataFetch: function () {
        var tabs = View.getOpenerView().panels.get("abDefineBuilding_tabs");
        if (tabs && valueExists(tabs.parameters)) {
            if (valueExists(tabs.parameters.callback)) {
                this.callbackMethod = tabs.parameters.callback;
            }
        }

        this.refreshRestriction();
    },

    refreshRestriction: function () {
        var tabs = View.getOpenerView().panels.get("abDefineBuilding_tabs");

        if (tabs && valueExists(tabs.restriction)) {
            this.restriction = tabs.restriction;
        }
        if (tabs && valueExists(tabs.newRecord)) {
            this.newRecord = tabs.newRecord;
        }

        if (this.newRecord) {
            this.abDefineBuilding_location.refresh(null, this.newRecord);
        } else {
            this.abDefineBuilding_location.refresh(this.restriction);
        }
    },

    abDefineBuilding_location_afterRefresh: function () {
        this.newRecord = this.abDefineBuilding_location.newRecord;
        this.blId = this.abDefineBuilding_location.getFieldValue('bl.bl_id');

        if (this.isSchemaCompatible) {
            var ownerRestriction = new Ab.view.Restriction({'team.autonumbered_id': -1});
            var otherRestriction = new Ab.view.Restriction({'team.autonumbered_id': -1});
            if (valueExistsNotEmpty(this.blId)) {
                ownerRestriction = new Ab.view.Restriction();
                ownerRestriction.addClause('team.is_owner', 1, '=');
                ownerRestriction.addClause('team.bl_id', this.blId, '=');

                otherRestriction = new Ab.view.Restriction();
                otherRestriction.addClause('team.custodian_type', null, 'IS NOT NULL');
                otherRestriction.addClause('team.is_owner', 0, '=');
                otherRestriction.addClause('team.bl_id', this.blId, '=');
            }
            this.abDefineBuilding_ownerCustodian.refresh(ownerRestriction, this.newRecord);
            this.abDefineBuilding_otherCustodian.refresh(otherRestriction, this.newRecord);
        } else {
            this.abDefineBuilding_ownerCustodian.show(false);
            this.abDefineBuilding_otherCustodian.show(false);
        }
    },

    abDefineBuilding_location_onLocate: function (context) {
        var restriction = this.restriction;
        var record = this.abDefineBuilding_location.getRecord();

        View.openDialog('ab-locate-asset.axvw', restriction, false, {
            width: 800,
            height: 600,
            record: record,
            assetType: 'bl',
            callback: function () {
                refreshBuildingLocationPanel();
            }
        });
    },

    abDefineBuilding_location_onGeocode: function () {
        if (this.geocodeControl === null) {
            this.initGeocodeControl();
        }

        var restriction = this.restriction;

        this.geocodeControl.geocode('ds_abDefineBuildingLocation',
            restriction, 'bl',
            'bl.bl_id',
            ['bl.lat', 'bl.lon'],
            ['bl.address1', 'bl.city_id', 'bl.state_id', 'bl.zip', 'bl.ctry_id'],
            true);
    },

    initGeocodeControl: function () {
        this.geocodeControl = new Ab.arcgis.Geocoder();
        this.geocodeControl.callbackMethod = refreshBuildingLocationPanel;
    },

    afterSaveBuilding: function () {
        setNewRestrictionForTabs();
        abDefineBuilding_tabLocationController.refreshRestriction();
        refreshTitle();
        callCallbackMethod();
    },

    /**
     * Customize fields for custodian panels
     */
    customizeCustodianFields: function (row, column, cellElement) {
        if ('team.status' === column.id) {
            var filterEl = Ext.get(row.grid.getFilterInputId(column.id));
            var removeOptions = function (el, values) {
                for ( var x in values ) {
                    for ( var i = el.dom.length - 1; i >= 0; i-- ) {
                        if (x === el.dom[i].value) {
                            el.dom.remove(i);
                            break;
                        }
                    }
                }
            };
            removeOptions(filterEl, {'Archived': 'Archived', 'Removed': 'Removed'});
        }
    },
    onEditOwnerCustodian: function (row) {
        var autoNumberedId = row.row.getFieldValue('team.autonumbered_id');
        var restriction = new Ab.view.Restriction();
        restriction.addClause('team.autonumbered_id', autoNumberedId, '=');
        var controller = View.controllers.get('abDefineBuilding_tabLocationController');

        View.openDialog('ab-eam-edit-custodian.axvw', restriction, false, {
            isOwner: true,
            assetType: 'bl',
            assetCode: controller.blId,
            callback: function () {
                View.closeDialog();
                controller.abDefineBuilding_ownerCustodian.refresh(controller.abDefineBuilding_ownerCustodian.restriction);
            }
        });
    },

    abDefineBuilding_ownerCustodian_onAddOwnerCustodian: function () {
        var selectedAsset = [{'asset_type': 'bl', 'asset_id': this.blId}];
        var controller = this;
        this.addCustodianCommon(selectedAsset, true, true, function () {
            controller.abDefineBuilding_ownerCustodian.refresh(controller.abDefineBuilding_ownerCustodian.restriction);
        });
    },

    onEditOtherCustodian: function (row) {
        var autoNumberedId = row.row.getFieldValue('team.autonumbered_id');
        var restriction = new Ab.view.Restriction();
        restriction.addClause('team.autonumbered_id', autoNumberedId, '=');
        var controller = View.controllers.get('abDefineBuilding_tabLocationController');

        View.openDialog('ab-eam-edit-custodian.axvw', restriction, false, {
            isOwner: false,
            assetType: 'bl',
            assetCode: controller.blId,
            callback: function () {
                View.closeDialog();
                controller.abDefineBuilding_otherCustodian.refresh(controller.abDefineBuilding_otherCustodian.restriction);
            }
        });
    },

    abDefineBuilding_otherCustodian_onAddOtherCustodian: function () {
        var selectedAsset = [{'asset_type': 'bl', 'asset_id': this.blId}];
        var controller = this;
        this.addCustodianCommon(selectedAsset, false, true, function () {
            controller.abDefineBuilding_otherCustodian.refresh(controller.abDefineBuilding_otherCustodian.restriction);
        });
    },

    addCustodianCommon: function (selectedAsset, isOwner, newRecord, callbackFunction) {
        View.openDialog('ab-eam-assign-custodian.axvw', null, newRecord, {
            width: 1024,
            height: 800,
            isOwner: isOwner,
            selectedAssets: selectedAsset,
            useCustomRestriction: false,
            callback: function (addMultipleCustodians) {
                if (valueExists(callbackFunction)) {
                    callbackFunction(closeDialog);
                }
                if (!addMultipleCustodians) {
                    View.closeDialog();
                }
            }
        });
    },
    /**
     * Check license.
     * @param {string} licenseId - license to check
     * @returns {boolean} has license enabled
     */
    checkLicense: function (licenseId) {
        var hasLicense = false;
        AdminService.getProgramLicense({
            async: false,
            callback: function (licenseDTO) {
                for ( var i = 0; i < licenseDTO.licenses.length; i++ ) {
                    var license = licenseDTO.licenses[i];
                    if (licenseId === license.id) {
                        hasLicense = license.enabled;
                        break;
                    }
                }
            },
            errorHandler: function (m, e) {
                View.showException(e);
            }
        });
        return hasLicense;
    }
});

function refreshBuildingLocationPanel() {
    var buildingLocationPanel = abDefineBuilding_tabLocationController.abDefineBuilding_location;
    buildingLocationPanel.refresh(buildingLocationPanel.restriction);
}

function callCallbackMethod() {
    var controller = View.controllers.get('abDefineBuilding_tabLocationController');
    if (valueExists(controller.callbackMethod)) {
        controller.callbackMethod();
    }
    return true;
}

function setNewRestrictionForTabs() {
    var form = abDefineBuilding_tabLocationController.abDefineBuilding_location;
    setRestrictionForTabs(abDefineBuilding_tabLocationController, form);
}