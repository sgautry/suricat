<view version="2.0">
    <css file="ab-eam-asset-filter.css"/>
    <js file="ab-eam-asset-filter.js"/>
    <js file="ab-eam-common.js"/>

    <message name="titleAssetStd_eq" translatable="true">Equipment Standard</message>
    <message name="titleAssetStd_ta" translatable="true">Furniture Standard</message>
    <message name="titleAssetStd_bl" translatable="true">Building Standard</message>
    <message name="titleAssetStd_property" translatable="true">Property Standard</message>
    <message name="titleAssetCode_bl" translatable="true">Building</message>
    <message name="titleAssetCode_eq" translatable="true">Equipment</message>
    <message name="titleAssetCode_ta" translatable="true">Furniture</message>
    <message name="titleAssetCode_property" translatable="true">Property</message>
    <message name="btnLabelMore" translatable="true">More</message>
    <message name="btnLabelLess" translatable="true">Less</message>
    <message name="titleSelectVal_custodian" translatable="true">Select Value - Custodian</message>
    <message name="titleSelectVal_system_System" translatable="true">Select Value - System</message>
    <message name="titleSelectVal_system_Assembly" translatable="true">Select Value - Assembly</message>

    <dataSource id="abAssetFilter_ds" applyVpaRestrictions="false">
        <sql dialect="generic">
            SELECT
                bl.bl_id ${sql.as} asset_id,
                'bl' ${sql.as} asset_type,
                bl.status ${sql.as} asset_status,
                NULL ${sql.as} asset_std,
                NULL ${sql.as} project_id,
                (SELECT ctry.geo_region_id FROM ctry WHERE ctry.ctry_id=bl.ctry_id) ${sql.as} geo_region_id,
                bl.ctry_id ${sql.as} ctry_id,
                bl.state_id ${sql.as} state_id,
                bl.city_id ${sql.as} city_id,
                bl.site_id ${sql.as} site_id,
                bl.bl_id ${sql.as} bl_id,
                NULL ${sql.as} fl_id,
                NULL ${sql.as} rm_id,
                NULL ${sql.as} bu_id,
                NULL ${sql.as} dv_id,
                NULL ${sql.as} dp_id,
                bl.pending_action ${sql.as} pending_action
            FROM bl
            UNION
            SELECT
                eq.eq_id ${sql.as} asset_id,
                'eq' ${sql.as} asset_type,
                eq.status ${sql.as} asset_status,
                eq.eq_std ${sql.as} asset_std,
                NULL ${sql.as} project_id,
                (SELECT ctry.geo_region_id FROM ctry, bl WHERE ctry.ctry_id=bl.ctry_id AND eq.bl_id=bl.bl_id) ${sql.as} geo_region_id,
                (SELECT bl.ctry_id FROM bl WHERE bl.bl_id=eq.bl_id) ${sql.as} ctry_id,
                (SELECT bl.state_id FROM bl WHERE bl.bl_id=eq.bl_id) ${sql.as} state_id,
                (SELECT bl.city_id FROM bl WHERE bl.bl_id=eq.bl_id) ${sql.as} city_id,
                eq.site_id ${sql.as} site_id,
                eq.bl_id ${sql.as} bl_id,
                eq.fl_id ${sql.as} fl_id,
                eq.rm_id ${sql.as} rm_id,
                (SELECT dv.bu_id FROM dv WHERE dv.dv_id=eq.dv_id) ${sql.as} bu_id,
                eq.dv_id ${sql.as} dv_id,
                eq.dp_id ${sql.as} dp_id,
                eq.pending_action ${sql.as} pending_action
            FROM eq
            UNION
            SELECT
                ta.ta_id ${sql.as} asset_id,
                'ta' ${sql.as} asset_type,
                ta.status ${sql.as} asset_status,
                ta.fn_std ${sql.as} asset_std,
                NULL ${sql.as} project_id,
                (SELECT ctry.geo_region_id FROM ctry, bl WHERE ctry.ctry_id=bl.ctry_id AND ta.bl_id=bl.bl_id) ${sql.as}
                geo_region_id,
                (SELECT bl.ctry_id FROM bl WHERE bl.bl_id = ta.bl_id) ${sql.as} ctry_id,
                (SELECT bl.state_id FROM bl WHERE bl.bl_id = ta.bl_id) ${sql.as} state_id,
                (SELECT bl.city_id FROM bl WHERE bl.bl_id = ta.bl_id) ${sql.as} city_id,
                (SELECT bl.site_id FROM bl WHERE bl.bl_id = ta.bl_id) ${sql.as} site_id,
                ta.bl_id ${sql.as} bl_id,
                ta.fl_id ${sql.as} fl_id,
                ta.rm_id ${sql.as} rm_id,
                (SELECT dv.bu_id FROM dv WHERE dv.dv_id=ta.dv_id) ${sql.as} bu_id,
                ta.dv_id ${sql.as} dv_id,
                ta.dp_id ${sql.as} dp_id,
                ta.pending_action ${sql.as} pending_action
            FROM ta
            UNION
            SELECT
                property.pr_id ${sql.as} asset_id,
                'property' ${sql.as} asset_type,
                property.status ${sql.as} asset_status,
                NULL ${sql.as} asset_std,
                NULL ${sql.as} project_id,
                (SELECT ctry.geo_region_id FROM ctry WHERE ctry.ctry_id = property.ctry_id) ${sql.as} geo_region_id,
                property.ctry_id ${sql.as} ctry_id,
                property.state_id ${sql.as} state_id,
                property.city_id ${sql.as} city_id,
                property.site_id ${sql.as} site_id,
                NULL ${sql.as} bl_id,
                NULL ${sql.as} fl_id,
                NULL ${sql.as} rm_id,
                NULL ${sql.as} bu_id,
                NULL ${sql.as} dv_id,
                NULL ${sql.as} dp_id,
                property.pending_action ${sql.as} pending_action
            FROM property
        </sql>
        <table name="bl" role="main"/>
        <table name="project" role="standard"/>
        <table name="work_pkgs" role="standard"/>
        <table name="activity_log" role="standard"/>
        <table name="eq_system" role="standard"/>
        <field table="bl" name="asset_id" dataType="text" size="16">
            <title>Asset Code</title>
        </field>
        <field table="bl" name="asset_type" dataType="text" size="16">
            <title>Asset Type</title>
        </field>
        <field table="bl" name="asset_status" dataType="text" size="16">
            <title>Asset Status</title>
        </field>
        <field table="bl" name="asset_std" dataType="text" size="24">
            <title>Asset Standard</title>
        </field>
        <field table="bl" name="project_id" dataType="text" size="32">
            <title>Project ID</title>
        </field>
        <field table="bl" name="geo_region_id" dataType="text">
            <title>Geo-Region ID</title>
        </field>
        <field table="bl" name="ctry_id"/>
        <field table="bl" name="state_id"/>
        <field table="bl" name="city_id"/>
        <field table="bl" name="site_id"/>
        <field table="bl" name="bl_id"/>
        <field table="bl" name="fl_id" dataType="text">
            <title>Floor Code</title>
        </field>
        <field table="bl" name="rm_id" dataType="text">
            <title>Room Code</title>
        </field>
        <field table="bl" name="bu_id" dataType="text" size="16">
            <title>Business Unit</title>
        </field>
        <field table="bl" name="dv_id" dataType="text" size="16">
            <title>Division Code</title>
        </field>
        <field table="bl" name="dp_id" dataType="text" size="16">
            <title>Department Code</title>
        </field>
        <field table="bl" name="pending_action"/>
        <field table="project" name="program_id"/>
        <field table="work_pkgs" name="work_pkg_id"/>
        <field table="activity_log" name="activity_log_id"/>
        <field table="activity_log" name="csi_id"/>
        <field table="eq_system" name="system_name" alias="system_system_name"/>
        <field table="eq_system" name="system_name" alias="assembly_system_name"/>
        <field table="eq_system" name="stakeholder_type"/>
        <field table="eq_system" name="criticality_function"/>
        <field table="eq_system" name="criticality_mission"/>
    </dataSource>

    <panel type="console" id="abEamAssetFilter" dataSource="abAssetFilter_ds" showOnLoad="false" columns="4" bodyCssClass="panel-light asset-filter">
        <title>Asset Selection Filter</title>
        <action id="clear">
            <title>Clear</title>
        </action>
        <action id="filter" mainAction="true">
            <title>Filter</title>
        </action>
        <action id="toggleMoreFields" hidden="true">
            <title>More</title>
        </action>
        <action id="actionButton1" type="menu" hidden="true">
            <title></title>
        </action>
        <!-- First row -->
        <fieldset layout="fluid">
            <field table="bl" name="geo_region_id" dataType="text" style="width:75px" showLabel="false">
                <title>GEO-REGION</title>
                <action id="selVal_geo_region_id">
                    <title translatable="true">...</title>
                    <tooltip>Select Value</tooltip>
                    <command type="selectValue" 
                       selectValueType="multiple" 
                       fieldNames="bl.geo_region_id" 
                       selectFieldNames="geo_region.geo_region_id" 
                       visibleFieldNames="geo_region.geo_region_id,geo_region.geo_region_name"/>
                </action>
            </field>
            <field table="bl" name="ctry_id" style="width:75px" showLabel="false" selectValueType="multiple"/>
            <field table="bl" name="state_id" style="width:75px" showLabel="false" selectValueType="multiple"/>
            <field table="bl" name="city_id" style="width:75px" showLabel="false" selectValueType="multiple"/>
        </fieldset>
        <fieldset layout="fluid">
            <field table="bl" name="bu_id" dataType="text" style="width:95px" showLabel="false">
                <title>BUSINESS UNIT</title>
                <action id="selVal_bu_id">
                    <title translatable="true">...</title>
                    <tooltip>Select Value</tooltip>
                    <command type="selectValue" 
                       selectValueType="multiple" 
                       fieldNames="bl.bu_id" 
                       selectFieldNames="bu.bu_id" 
                       visibleFieldNames="bu.bu_id,bu.name"/>
                </action>
            </field>
            <field table="bl" name="dv_id" dataType="text" style="width:95px" showLabel="false">
                <title>DIVISION</title>
                <action id="selVal_dv_id">
                    <title translatable="true">...</title>
                    <tooltip>Select Value</tooltip>
                    <command type="selectValue" 
                       selectValueType="multiple" 
                       fieldNames="bl.bu_id,bl.dv_id" 
                       selectFieldNames="dv.bu_id,dv.dv_id" 
                       visibleFieldNames="dv.bu_id,dv.dv_id,dv.name" 
                       restriction=""/>
                </action>
            </field>
            <field table="bl" name="dp_id" dataType="text" style="width:95px" showLabel="false">
                <title>DEPARTMENT</title>
                <action id="selVal_dp_id">
                    <title translatable="true">...</title>
                    <tooltip>Select Value</tooltip>
                    <command type="selectValue" 
                       selectValueType="multiple" 
                       fieldNames="bl.dv_id,bl.dp_id" 
                       selectFieldNames="dp.dv_id,dp.dp_id" 
                       visibleFieldNames="dp.dv_id,dp.dp_id,dp.name" 
                       restriction=""/>
                </action>
            </field>
        </fieldset>
        <fieldset layout="fluid">
            <field table="bl" name="asset_type" cssClass="shortField" labelClass="placeholder" showLabel="false" controlType="comboBox" onchange="View.controllers.get('abEamAssetFilterCtrl').onChangeAssetType()">
                <title>ASSET TYPE</title>
                <option value="" translatable="false"></option>
                <option value="property" translatable="true">Property</option>
                <option value="bl" translatable="true">Building</option>
                <option value="eq" translatable="true">Equipment</option>
                <option value="ta" translatable="true">Furniture</option>
            </field>
        </fieldset>
        <fieldset layout="fluid">
            <field table="activity_log" name="csi_id" style="width:95px" showLabel="false">
                <action id="selVal_csi_id">
                    <title translatable="true">...</title>
                    <tooltip>Select Value</tooltip>
                    <command type="selectValue" 
                       selectValueType="hierTree" 
                       fieldNames="activity_log.csi_id" 
                       selectFieldNames="csi.csi_id" 
                       visibleFieldNames="csi.csi_id,csi.description"/>
                </action>
            </field>
            <field table="bl" name="asset_std" dataType="text" style="width:95px" showLabel="false" selectValueType="multiple">
                <title>STANDARD</title>
                <action id="selVal_asset_std">
                    <title translatable="false">...</title>
                    <tooltip>Select Value</tooltip>
                    <command type="callFunction" functionName="View.controllers.get('abEamAssetFilterCtrl').onSelectAssetStandard()"/>
                </action>
            </field>
            <field table="bl" name="asset_id" dataType="text" size="16" style="width:95px" showLabel="false">
                <title>ASSET CODE</title>
                <action id="selVal_asset_id">
                    <title translatable="false">...</title>
                    <tooltip>Select Value</tooltip>
                    <command type="callFunction" functionName="View.controllers.get('abEamAssetFilterCtrl').onSelectAssetCode()"/>
                </action>
            </field>
        </fieldset>
        <!-- Second row -->
        <fieldset layout="fluid">
            <field table="bl" name="site_id" style="width:75px" showLabel="false" selectValueType="multiple"/>
            <field table="bl" name="bl_id" style="width:75px" showLabel="false" selectValueType="multiple"/>
            <field table="bl" name="fl_id" dataType="text" style="width:75px" showLabel="false">
                <title>FLOOR</title>
                <action id="selVal_fl_id">
                    <title translatable="true">...</title>
                    <tooltip>Select Value</tooltip>
                    <command type="selectValue" 
                       selectValueType="multiple" 
                       fieldNames="bl.bl_id,bl.fl_id" 
                       selectFieldNames="fl.bl_id,fl.fl_id" 
                       visibleFieldNames="fl.bl_id,fl.fl_id" 
                       restriction=""/>
                </action>
            </field>
            <field table="bl" name="rm_id" dataType="text" style="width:75px" showLabel="false">
                <title>ROOM</title>
                <action id="selVal_rm_id">
                    <title translatable="true">...</title>
                    <tooltip>Select Value</tooltip>
                    <command type="selectValue" 
                       selectValueType="multiple" 
                       fieldNames="bl.bl_id,bl.fl_id,bl.rm_id" 
                       selectFieldNames="rm.bl_id,rm.fl_id,rm.rm_id" 
                       visibleFieldNames="rm.bl_id,rm.fl_id,rm.rm_id" 
                       restriction=""/>
                </action>
            </field>
        </fieldset>
        <fieldset layout="fluid">
            <field table="project" name="program_id" style="width:95px" showLabel="false" selectValueType="multiple">
                <title>PROGRAM</title>
            </field>
            <field table="bl" name="project_id" dataType="text" size="32" style="width:95px" showLabel="false">
                <title>PROJECT</title>
                <action id="selVal_project_id">
                    <title translatable="true">...</title>
                    <tooltip>Select Value</tooltip>
                    <command type="selectValue" 
                       selectValueType="multiple" 
                       fieldNames="bl.project_id" 
                       selectFieldNames="project.project_id"
                       visibleFieldNames="project.project_id,project.project_name" 
                       restriction="project.is_template = 0"/>
                </action>
            </field>
            <field table="work_pkgs" name="work_pkg_id" style="width:95px" showLabel="false" selectValueType="multiple">
                <title>WORK PACKAGE</title>
            </field>
        </fieldset>
        <fieldset layout="fluid">
            <field table="bl" name="asset_status" cssClass="shortField" labelClass="placeholder" showLabel="false" controlType="comboBox" selectValueType="multiple">
                <title>ASSET STATUS</title>
            </field>
        </fieldset>
        <fieldset layout="fluid">
            <field id="custodian" alias="custodian" dataType="text" style="width:95px" showLabel="false" selectValueType="multiple">
                <title>CUSTODIAN</title>
                <action id="selVal_custodian">
                    <title translatable="true">...</title>
                    <tooltip>Select Custodians</tooltip>
                    <command type="callFunction" functionName="View.controllers.get('abEamAssetFilterCtrl').onSelectCustodian()"/>
                </action>
            </field>
            <field table="eq_system" name="system_name" alias="system_system_name" style="width:95px" showLabel="false">
                <title>SYSTEM</title>
                <action id="selVal_system_system_name">
                    <title translatable="true">...</title>
                    <tooltip>Select System</tooltip>
                    <command type="callFunction" functionName="View.controllers.get('abEamAssetFilterCtrl').onSelectSystem('System')"/>
                </action>
            </field>
            <field table="eq_system" name="system_name" alias="assembly_system_name" style="width:95px" showLabel="false">
                <title>ASSEMBLY</title>
                <action id="selVal_assembly_system_name">
                    <title translatable="true">...</title>
                    <tooltip>Select Assembly</tooltip>
                    <command type="callFunction" functionName="View.controllers.get('abEamAssetFilterCtrl').onSelectSystem('Assembly')"/>
                </action>
            </field>
        </fieldset>
        <!-- Third row -->
        <fieldset layout="fluid">
        </fieldset>
        <fieldset layout="fluid">
            <field table="eq_system" name="criticality_mission" cssClass="shortField" labelClass="placeholder" showLabel="false" controlType="comboBox">
                <title>MISSION CRITICALITY</title>
            </field>
            <field table="eq_system" name="criticality_function" cssClass="shortField" labelClass="placeholder" showLabel="false" controlType="comboBox">
                <title>FUNCTIONAL CRITICALITY</title>
            </field>
        </fieldset>
        <fieldset layout="fluid">
            <field table="eq_system" name="stakeholder_type" cssClass="shortField" labelClass="placeholder" showLabel="false" controlType="comboBox">
                <title>STAKEHOLDER</title>
            </field>
            <field table="bl" name="pending_action" cssClass="shortField" labelClass="placeholder" showLabel="false" controlType="comboBox"/>
        </fieldset>
        <fieldset layout="fluid">
            <field id="deprec_method" cssClass="shortField" labelClass="placeholder" showLabel="false" controlType="comboBox">
                <title>DEPRECIATION</title>
                <option value="" translatable="false"></option>
                <option value="DDB" translatable="true">Double-Declining Balance</option>
                <option value="PCT" translatable="true">Percentage</option>
                <option value="SL" translatable="true">Straight-Line</option>
                <option value="SYD" translatable="true">Sum-of-Years-Digits</option>
            </field>
            <field id="deprec_value_type" cssClass="shortField" labelClass="placeholder" showLabel="false" controlType="comboBox" onchange="View.controllers.get('abEamAssetFilterCtrl').onChangeDepreciationValue()">
                <title>DEPRECIATED VALUE</title>
                <option value="" translatable="false"></option>
                <option value="zero" translatable="true">Equals zero</option>
                <option value="non_zero" translatable="true">Non zero</option>
                <option value="greater_than" translatable="true">Greater than:</option>
            </field>
            <field id="deprec_value" dataType="number" decimals="2" style="width:50px" showLabel="false">
                <title>VALUE</title>
            </field>
        </fieldset>
    </panel>
    <!-- Helper data sources -->
    <dataSource id="abAssetStatus_ds">
        <table name="bl" role="main"/>
        <table name="property" role="main"/>
        <table name="eq" role="main"/>
        <table name="ta" role="main"/>
        <field table="bl" name="status"/>
        <field table="property" name="status"/>
        <field table="eq" name="status"/>
        <field table="ta" name="status"/>
    </dataSource>
    <dataSource id="abEquipment_ds">
        <table name="eq" role="main"/>
        <field name="eq_id" table="eq"/>
        <field name="site_id" table="eq"/>
        <field name="bl_id" table="eq"/>
        <field name="fl_id" table="eq"/>
        <field name="rm_id" table="eq"/>
    </dataSource>
    <dataSource id="abAssetFilter_eqSystemDs">
        <sql dialect="generic">
            SELECT 
                eq_id_depend,
                (CASE WHEN eq_system.system_name IS NOT NULL THEN eq_system.system_name WHEN eq.use1 IS NOT NULL THEN eq.use1 ELSE eq.eq_std END) ${sql.as} system_name
            FROM eq_system INNER JOIN eq ON eq_system.eq_id_depend=eq.eq_id
        </sql>
        <table name="eq_system"/>
        <field name="system_name" dataType="text"/>
        <field name="eq_id_depend" dataType="text"/>
    </dataSource>
    <dataSource id="abCustodiansAssigned_ds">
        <sql dialect="generic">
            SELECT
                team.autonumbered_id ${sql.as} autonumbered_id,
                team.custodian_type ${sql.as} custodian_type,
                team.status ${sql.as} status,
                team.vn_id ${sql.as} vn_id,
                team.contact_id ${sql.as} contact_id,
                team.em_id ${sql.as} em_id,
                team.source_table ${sql.as} source_table,
                team.team_type ${sql.as} team_type,
                (CASE
                    WHEN team.bl_id IS NOT NULL THEN team.bl_id
                    WHEN team.pr_id IS NOT NULL THEN team.pr_id
                    WHEN team.eq_id IS NOT NULL THEN team.eq_id
                    WHEN team.ta_id IS NOT NULL THEN team.ta_id
                ELSE NULL END) ${sql.as} asset_id,
                (CASE
                    WHEN team.bl_id IS NOT NULL THEN ${parameters['assetType_bl']}
                    WHEN team.pr_id IS NOT NULL THEN ${parameters['assetType_property']}
                    WHEN team.eq_id IS NOT NULL THEN ${parameters['assetType_eq']}
                    WHEN team.ta_id IS NOT NULL THEN ${parameters['assetType_ta']}
                ELSE NULL END) ${sql.as} asset_type,
                (CASE
                    WHEN team.source_table = 'vn' THEN team.vn_id
                    WHEN team.source_table = 'contact' THEN team.contact_id
                    WHEN team.source_table = 'em' THEN team.em_id
                ELSE NULL END) ${sql.as} custodian_name
            FROM team
        </sql>
        <table name="team"/>
        <field name="autonumbered_id"/>
        <field name="asset_id" dataType="text"/>
        <field name="asset_type" dataType="text"/>
        <field name="custodian_name" dataType="text"/>
        <field name="custodian_type"/>
        <field name="status"/>
        <restriction type="sql" sql="team.team_type='Equipment' AND team.status='Active' AND team.custodian_type IS NOT NULL AND ${parameters['sqlRestriction']}"/>
        <parameter name="sqlRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="assetType_bl" dataType="text" value="bl"/>
        <parameter name="assetType_property" dataType="text" value="property"/>
        <parameter name="assetType_eq" dataType="text" value="eq"/>
        <parameter name="assetType_ta" dataType="text" value="ta"/>
    </dataSource>
    <!-- Helpder data source -->
    <dataSource id="abAssetFilter_isOracleDs">
        <table name="afm_tbls" role="main"/>
        <sql dialect="oracle">
            SELECT 1 AS table_name FROM dual
        </sql>
        <sql dialect="generic">
            SELECT 0 AS table_name
        </sql>
        <field table="afm_tbls" name="table_name"/>
    </dataSource>
</view>