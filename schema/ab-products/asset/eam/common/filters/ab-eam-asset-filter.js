View.createController('abEamAssetFilterCtrl', {
    // filter basic config object
    filterConfig: null,
    // callback function for onFilter event
    onFilterCallback: null,
    // restriction with current values
    restriction: null,
    // action button handler
    onClickActionButton1Handler: null,
    actionButton1Label: null,
    // collapse filter panel
    collapsed: false,
    // collapse more fields
    collapsedFields: true,
    collapsedFieldsHeight: 20,
    // custodian field
    custodian: {},
    // system field - list of equipment ids
    system: [],
    afterViewLoad: function () {
        // add collapsed button
        this.abEamAssetFilter.toolbar.insertButton(0, {
            id: this.abEamAssetFilter.id + '_collapse',
            cls: 'x-btn-icon',
            icon: View.contextPath + '/schema/ab-core/graphics/icons/tri-opened.png',
            listeners: {
                'click': this.toggleCollapsed.createDelegate(this)
            }
        });
    },
    /**
     * Initialize all config objects.
     * @param filterConfig
     */
    initializeConfigObjects: function (filterConfig) {
        this.filterConfig = filterConfig;
    },
    /**
     * Set filter restriction.
     * @param restriction
     */
    setRestriction: function (restriction) {
        if (valueExists(restriction)) {
            this.restriction = restriction;
        }
    },
    /**
     * Initialize filter.
     */
    initializeFilter: function () {
        // customize filter panel
        this.customizeFilterPanel(this.filterConfig, this.restriction);
        // show more fields if fields are collapsed
        this.showMoreFields(!this.collapsedFields);
        // show filter panel
        this.abEamAssetFilter.show();
        // if restriction was passed, collapse the panel
        if (valueExists(this.restriction) && !this.restriction.isEmpty()) {
            this.setCollapsed(true);
        }
        this.managePassedParameters();
        // manage onClick action1 button
        if (valueExists(this.onClickActionButton1Handler)) {
            this.abEamAssetFilter.actions.get('actionButton1').setTitle(this.actionButton1Label);
            this.abEamAssetFilter.actions.get('actionButton1').show(true);
            // TODO find a general solution
            var menuParent = Ext.get('actionButton1');
            menuParent.on('click', this.abEamAssetFilter_onActionButton1, this, null);
        }
    },
    //TODO open from target view
    managePassedParameters: function () {
        //URL passed eq_id, see ab-eq-system-console.js
        if (this.view.parentTab && this.view.parentTab.parentPanel) {
            var tabs = this.view.parentTab.parentPanel;
            if (tabs.eq_id && tabs.eq_id !== '') {
                var restriction = new Ab.view.Restriction();
                restriction.addClause('eq.eq_id', tabs.eq_id);
                var records = this.abEquipment_ds.getRecords(restriction);
                var record = records[0];
                var bl_id = record.getValue('eq.bl_id');
                if (records && records.length === 1 && bl_id !== undefined && bl_id !== null) {
                    this.abEamAssetFilter.setFieldValue('bl.bl_id', bl_id);
                    var fl_id = record.getValue('eq.fl_id');
                    var rm_id = record.getValue('eq.rm_id');
                    if (fl_id !== undefined && fl_id !== null && fl_id !== ''
                        && rm_id !== undefined && rm_id !== null && rm_id !== '') {
                        this.abEamAssetFilter.setFieldValue('bl.fl_id', fl_id);
                        this.abEamAssetFilter.setFieldValue('bl.rm_id', rm_id);
                    }
                    //filter
                    this.abEamAssetFilter_onFilter();
                    //collapse some panels to highlight the rest
                    var layout = View.getLayoutManager('mainLayout');
                    if (layout) {
                        layout.collapseRegion('north');
                    }
                    layout = View.getLayoutManager('inventoryLayout');
                    if (layout) {
                        layout.collapseRegion('west');
                    }
                }
                tabs.eq_id = null;
            }
        }
    },
    /**
     * Customize filter panel fields.
     * @param {Object} fieldsConfig
     * @param {Ab.view.Restriction} restriction
     */
    customizeFilterPanel: function (fieldsConfig, restriction) {
        var panel = this.abEamAssetFilter;
        panel.fields.each(function (field) {
            if (valueExists(field.fieldDef)) {
                var dfltValue = '';
                if (valueExists(fieldsConfig)
                    && valueExists(fieldsConfig.get(field.fieldDef.id))) {
                    var fieldConfig = fieldsConfig.get(field.fieldDef.id).fieldConfig;
                    if ('enumList' === fieldConfig.type) {
                        this.customizeDropDownField(panel, field.fieldDef.id, fieldConfig);
                    } else if ('checkbox' === fieldConfig.type) {
                        this.customizeCheckboxField(panel, field.fieldDef.id, fieldConfig);
                    } else {
                        this.customizeField(panel, field.fieldDef.id, fieldConfig);
                    }
                    dfltValue = fieldConfig.dfltValue;
                }
                this.addComboBoxPlaceholder(field);
                if (valueExists(restriction)) {
                    var clause = restriction.findClause(field.fieldDef.id);
                    if (clause && clause.value) {
                        if (valueExistsNotEmpty(dfltValue)) {
                            panel.setFieldValue(field.fieldDef.id, dfltValue);
                            if ('bl.asset_type' === field.fieldDef.id) {
                                this.onChangeAssetType();
                                jQuery(field.dom).trigger('change');
                            }
                        } else {
                            if (_.isArray(clause.value)) { //selectValueType = multiple
                                panel.setFieldMultipleValues(field.fieldDef.id, clause.value);
                            } else {
                                panel.setFieldValue(field.fieldDef.id, clause.value);
                                if ('bl.asset_status' === field.fieldDef.id) {
                                    jQuery(field.dom).trigger('change');
                                }
                            }
                            if ('bl.asset_type' === field.fieldDef.id) {
                                this.onChangeAssetType();
                                jQuery(field.dom).trigger('change');
                            }
                        }
                    }
                }
            }
        }, this);
    },
    /**
     * Copy filter restriction from one tab to another.
     * @param {Ab.view.Restriction} restriction - Passed filter restriction.
     */
    copyFilterRestriction: function (restriction) {
        // if restriction has fields with default values, remove this fields from restriction object
        restriction = this.getRestrictionWithoutDefaultValues(restriction);
        var filterPanel = View.panels.get('abEamAssetFilter');
        // reset filter fields and set default values
        filterPanel.clear();
        this.clearFieldsPlaceholder();
        if (valueExists(this.filterConfig)) {
            this.filterConfig.each(function (field) {
                if (valueExists(field.fieldConfig)) {
                    this.setDefaultValueForField(filterPanel, field.id, field.fieldConfig);
                }
            }, this);
        }
        // check is asset type exists and set status and asset standard fields
        var assetTypeClause = restriction.findClause('bl.asset_type');
        if (valueExists(assetTypeClause)) {
            var enableAssetFields = valueExists(assetTypeClause) && valueExists(assetTypeClause.value);
            filterPanel.enableField('bl.asset_status', enableAssetFields);
            filterPanel.enableField('bl.asset_std', enableAssetFields);
        }
        // set restriction values
        filterPanel.fields.each(function (field) {
            if (valueExists(restriction)) {
                var clause = restriction.findClause(field.fieldDef.id);
                if (clause && clause.value) {
                    if (_.isArray(clause.value)) { //selectValueType = multiple
                        filterPanel.setFieldMultipleValues(field.fieldDef.id, clause.value);
                    } else {
                        filterPanel.setFieldValue(field.fieldDef.id, clause.value);
                        if ('bl.asset_status' === field.fieldDef.id) {
                            jQuery(field.dom).trigger('change');
                        }
                    }
                    if ('bl.asset_type' === field.fieldDef.id) {
                        // check asset type
                        this.onChangeAssetType();
                        jQuery(field.dom).trigger('change');
                    }
                }
            }
        }, this);
        // check depreciation value type
        this.onChangeDepreciationValue();
    },
    /**
     * Use to pass restrictions from one tab to another.
     * If restriction has fields with default values, remove this fields from restriction object.
     */
    getRestrictionWithoutDefaultValues: function (restriction) {
        this.filterConfig.each(function (field) {
            var fieldConfig = field.fieldConfig;
            var clause = restriction.findClause(field.id);
            if (clause && clause.name && clause.name === field.id) {
                if (valueExistsNotEmpty(fieldConfig.dfltValue)) {
                    if ('bl.asset_type' === field.id) {
                        var clauseAssetStatus = restriction.findClause('bl.asset_status');
                        var clauseAssetStandard = restriction.findClause('bl.asset_std');
                        if (valueExists(clauseAssetStatus) || valueExists(clauseAssetStandard)) {
                            // DO NOT REMOVE ASSET TYPE
                        } else {
                            restriction.removeClause(clause.name);
                        }
                        if (fieldConfig.dfltValue !== clause.value) {
                            restriction.removeClause(clause.name);
                            restriction.removeClause('bl.asset_status');
                            restriction.removeClause('bl.asset_standard');
                        }
                    } else {
                        restriction.removeClause(clause.name);
                    }
                }
            }
        }, this);
        return restriction;
    },
    /**
     * On filter action.
     * @returns {boolean} false is filter is not validated.
     */
    abEamAssetFilter_onFilter: function () {
        if (!this.validateFilter()) {
            return false;
        }
        var restriction = this.getFilterRestriction();
        this.restriction = restriction;
        // call callback function
        if (this.onFilterCallback) {
            this.onFilterCallback(restriction);
        }
    },
    /**
     * On clear action.
     */
    abEamAssetFilter_onClear: function () {
        var controller = this;
        // clear basic
        var filterPanel = View.panels.get('abEamAssetFilter');
        filterPanel.clear();
        this.clearFieldsPlaceholder();
        if (valueExists(this.filterConfig)) {
            this.filterConfig.each(function (field) {
                if (valueExists(field.fieldConfig)) {
                    controller.setDefaultValueForField(filterPanel, field.id, field.fieldConfig);
                }
            });
        }
        this.onChangeAssetType();
        this.onChangeDepreciationValue();
    },
    /**
     * Resets filter to default values and trigger the filter action.
     */
    resetFilter: function () {
        // called to reset and apply default filter
        this.abEamAssetFilter_onClear();
        this.abEamAssetFilter_onFilter();
    },
    /**
     * Customize status field when asset type is changed.
     */
    onChangeAssetType: function () {
        var assetType = this.abEamAssetFilter.getFieldValue('bl.asset_type');
        var statusField = this.abEamAssetFilter.fields.get('bl.asset_status');
        if (valueExistsNotEmpty(assetType)) {
            // customize asset status field
            var enumValues = this.abAssetStatus_ds.fieldDefs.get(assetType + ".status").enumValues;
            if (statusField) {
                // remove all options
                statusField.clearOptions();
                statusField.addOption('', statusField.fieldDef.title);
                // add new options
                for ( var opt in enumValues ) {
                    statusField.addOption(opt, enumValues[opt]);
                }
                this.abEamAssetFilter.enableField('bl.asset_status', true);
                var $el = jQuery(statusField.dom);
                if (!$el.val()) {
                    $el.addClass("placeholder");
                }
            }
            // enable asset standard field
            this.abEamAssetFilter.setFieldValue('bl.asset_std', '');
            this.abEamAssetFilter.enableField('bl.asset_std', true);
        } else {
            if (statusField) {
                // remove all options
                statusField.clearOptions();
                statusField.addOption('', statusField.fieldDef.title);
                //this.addComboBoxPlaceholder(statusField);
                this.abEamAssetFilter.enableField('bl.asset_status', false);
            }
            this.abEamAssetFilter.setFieldValue('bl.asset_std', '');
            this.abEamAssetFilter.enableField('bl.asset_std', false);
        }
    },
    /**
     * Customize depreciation fields when depreciation is changed.
     */
    onChangeDepreciationValue: function () {
        var filterPanel = View.panels.get('abEamAssetFilter');
        var selectedValue = filterPanel.getFieldValue('deprec_value_type');
        if ('greater_than' === selectedValue) {
            filterPanel.showField('deprec_value', true);
            filterPanel.setFieldValue('deprec_value', '0.00');
        } else {
            filterPanel.showField('deprec_value', false);
        }
    },
    /**
     * Add comboBox placeholder field title.
     * @param field - comboBox field
     */
    addComboBoxPlaceholder: function (field) {
        // customize comboBox fields placeholders
        var hasPlaceholder = 'placeholder' === field.fieldDef.labelClass;
        if (hasPlaceholder && 'comboBox' === field.fieldDef.controlType) {
            var $el = jQuery(field.dom);
            var title = field.fieldDef.title.trim().toUpperCase();
            if (!$el.val()) {
                $el.addClass("placeholder");
                $el.attr('qtip', title);
            }
            var firstOption = $el.find('option:first');
            if (firstOption.length) {
                firstOption.text(title);
            } else {
                $el.append(jQuery('<option>', {value: '', text: title}));
            }
            $el.on("change", function () {
                if (!$el.val()) {
                    $el.addClass("placeholder");
                } else {
                    $el.removeClass("placeholder");
                }
                $el.attr('qtip', $el.find('option:selected').text());
            });
        }
    },
    /**
     * Clear fields placeholders.
     */
    clearFieldsPlaceholder: function () {
        this.abEamAssetFilter.fields.each(function (field) {
            var hasPlaceholder = 'placeholder' === field.fieldDef.labelClass;
            if (hasPlaceholder && 'comboBox' === field.fieldDef.controlType) {
                var $el = jQuery(field.dom);
                var firstOption = $el.find('option:first');
                if (firstOption.length && !valueExistsNotEmpty(firstOption.text())) {
                    field.dom.remove(0);
                }
                if (!$el.val()) {
                    $el.addClass("placeholder");
                }
                $el.attr('qtip', $el.find('option:selected').text());
            }
        }, this);
    },
    /**
     * Toggle More fields action.
     * @param action
     */
    abEamAssetFilter_onToggleMoreFields: function (action) {
        this.showMoreFields(this.collapsedFields);
        this.collapsedFields = !this.collapsedFields;
        action.setText(this.collapsedFields ?
            getMessage('btnLabelMore') : getMessage('btnLabelLess'));
    },
    /**
     * Toggle collapsed.
     */
    toggleCollapsed: function () {
        this.setCollapsed(!this.collapsed);
    },
    /**
     * Set panel collapsed.
     * @param {boolean} collapsed
     */
    setCollapsed: function (collapsed) {
        this.abEamAssetFilter.showElement(this.abEamAssetFilter.parentElement, !collapsed);
        this.setCollapsedIcon(collapsed);
        this.setCollapsedLayout(collapsed);
        this.enableCollapsedAction('filter', collapsed);
        this.enableCollapsedAction('clear', collapsed);
        this.enableCollapsedAction('toggleMoreFields', collapsed);
        this.collapsed = collapsed;
    },
    enableCollapsedAction: function (actionId, collapsed) {
        var action = this.abEamAssetFilter.actions.get(actionId);
        action.enabled = !collapsed;
        action.forceDisable(collapsed);
    },
    /**
     * Set collapsed icon.
     * @param {boolean} collapsed
     */
    setCollapsedIcon: function (collapsed) {
        var icon = View.contextPath + '/schema/ab-core/graphics/icons/tri-opened.png';
        if (collapsed) {
            icon = View.contextPath + '/schema/ab-core/graphics/icons/tri-closed.png';
        }
        var toolbar = this.abEamAssetFilter.toolbar;
        if (toolbar) {
            if (collapsed) {
                toolbar.el.addClass('collapsed');
            } else {
                toolbar.el.removeClass('collapsed');
            }
            var buttonParentEl = Ext.get(this.abEamAssetFilter.id + '_collapse');
            var buttonEl = buttonParentEl.child('button', true);
            buttonEl.style.backgroundImage = 'url(' + icon + ')';
        }
    },
    /**
     * Set collapsed layout.
     * @param {boolean} collapsed
     */
    setCollapsedLayout: function (collapsed) {
        var layout = View.getLayoutManager('mainLayout');
        if (collapsed) {
            var collapsedPanelHeight = Ext.get(this.abEamAssetFilter.getWrapperElementId()).getHeight();
            layout.setRegionSize('north', collapsedPanelHeight + 8);
        } else {
            var initialSize = layout.getRegion('north').height + this.collapsedFieldsHeight;
            layout.setRegionSize('north', initialSize);
        }
    },
    /**
     * Show optional fields.
     * @param {boolean} show
     */
    showMoreFields: function (show) {
        var layout = View.getLayoutManager('mainLayout');
        var initialSize = layout.getRegion('north').height;
        if (show) {
            this.collapsedFieldsHeight = 20;
            layout.setRegionSize('north', initialSize + this.collapsedFieldsHeight);
        } else {
            this.collapsedFieldsHeight = 0;
            layout.setRegionSize('north', initialSize);
        }
        this.abEamAssetFilter.fields.each(function (field) {
            if (valueExists(field.fieldDef)) {
                if (valueExists(this.filterConfig)
                    && valueExists(this.filterConfig.get(field.fieldDef.id))) {
                    var fieldConfig = this.filterConfig.get(field.fieldDef.id).fieldConfig;
                    if (fieldConfig.optional && !fieldConfig.hidden) {
                        this.abEamAssetFilter.showField(field.fieldDef.id, show);
                    }
                }
            }
        }, this);
        this.onChangeDepreciationValue();
    },
    /**
     * Show more action button.
     */
    showMoreAction: function () {
        var moreAction = this.abEamAssetFilter.actions.get('toggleMoreFields');
        if (moreAction.button.hidden) {
            this.abEamAssetFilter.actions.get('toggleMoreFields').show(true);
        }
    },
    /**
     * Hide more action button.
     */
    hideMoreAction: function () {
        this.abEamAssetFilter.actions.get('toggleMoreFields').show(false);
    },
    /**
     * Read filter values and return restriction object.
     * @returns {Ab.view.Restriction} filter restriction.
     */
    getFilterRestriction: function () {
        var restriction = new Ab.view.Restriction();
        // get basic filter fields
        var filterPanel = this.abEamAssetFilter;
        this.system = [];
        filterPanel.fields.each(function (field) {
            if (valueExists(field.fieldDef)
                && valueExistsNotEmpty(filterPanel.getFieldValue(field.fieldDef.id))) {
                var value = filterPanel.getFieldValue(field.fieldDef.id);
                if (filterPanel.hasFieldMultipleValues(field.fieldDef.id)) {
                    var values = filterPanel.getFieldMultipleValues(field.fieldDef.id);
                    restriction.addClause(field.fieldDef.id, values, 'IN');
                } else {
                    if ('custodian' === field.fieldDef.id) {
                        var custodian = this.custodian;
                        if (_.isEmpty(this.custodian) && valueExists(this.restriction) && !_.isEmpty(this.restriction.custodian)) {
                            custodian = this.restriction.custodian;
                        } else {
                            custodian = this.custodian;
                        }
                        var assignedAssets = this.getCustodianAssignedAssets(custodian);
                        if (valueExists(assignedAssets)) {
                            custodian.assignedAssets = assignedAssets;
                        } else {
                            custodian.name = value;
                            custodian.assignedAssets = {'bl': ['-1']}; // set dummy value
                        }
                        restriction.custodian = custodian;
                    } else if ('system_system_name' === field.fieldDef.id || 'assembly_system_name' === field.fieldDef.id) {
                        this.system = _.union(this.system, this.getSystemAssignedAssets(value));
                        restriction.system = this.system;
                    }
                    if ('bl.asset_id' === field.fieldDef.id) {
                        restriction.addClause(field.fieldDef.id, value, 'LIKE');
                    } else {
                        restriction.addClause(field.fieldDef.id, value, '=');
                    }
                }
            }
        }, this);
        return restriction;
    },
    /**
     * Select asset code.
     */
    onSelectAssetCode: function () {
        var assetType = this.abEamAssetFilter.getFieldValue('bl.asset_type');
        var title = getMessage('titleAssetCode_' + assetType);
        if ('bl' === assetType) {
            View.selectValue('abEamAssetFilter', title, ['bl.asset_id'], assetType,
                ['bl.bl_id'], ['bl.bl_id', 'bl.name', 'bl.city_id', 'bl.state_id', 'bl.ctry_id'], null, null, null, null, null, 800, 600);
        } else if ('eq' === assetType) {
            View.selectValue('abEamAssetFilter', title, ['bl.asset_id'], assetType,
                ['eq.eq_id'], ['eq.eq_id', 'eq.description', 'eq.bl_id', 'eq.fl_id', 'eq.rm_id'], null, null, null, null, null, 800, 600);
        } else if ('ta' === assetType) {
            View.selectValue('abEamAssetFilter', title, ['bl.asset_id'], assetType,
                ['ta.ta_id'], ['ta.ta_id', 'ta.fn_std', 'ta.bl_id', 'ta.fl_id', 'ta.rm_id'], null, null, null, null, null, 800, 600);
        } else if ('property' === assetType) {
            View.selectValue('abEamAssetFilter', title, ['bl.asset_id'], assetType,
                ['property.pr_id'], ['property.pr_id', 'property.name', 'property.city_id', 'property.state_id', 'property.ctry_id'], null, null, null, null, null, 800, 600);
        }
    },
    /**
     * Select asset standard action.
     */
    onSelectAssetStandard: function () {
        var assetType = this.abEamAssetFilter.getFieldValue('bl.asset_type');
        var title = getMessage('titleAssetStd_' + assetType);
        if ('eq' === assetType) {
            View.selectValue('abEamAssetFilter', title, ['bl.asset_std'], 'eqstd',
                ['eqstd.eq_std'], ['eqstd.eq_std', 'eqstd.category', 'eqstd.description'], null, null, null, null, null, 800, 600, 'multiple');
        } else if ('ta' === assetType) {
            View.selectValue('abEamAssetFilter', title, ['bl.asset_std'], 'fnstd',
                ['fnstd.fn_std'], ['fnstd.fn_std', 'fnstd.catalog_id', 'fnstd.category', 'fnstd.description'], null, null, null, null, null, 800, 600, 'multiple');
        } else if ('bl' === assetType) {
            View.selectValue('abEamAssetFilter', title, ['bl.asset_std'], 'bl',
                ['bl.use1'], ['bl.use1'], null, null, null, null, null, 800, 600, 'multiple', null, "[{'sortOrder':1,'fieldName':'bl.use1'}]");
        } else if ('property' === assetType) {
            View.selectValue('abEamAssetFilter', title, ['bl.asset_std'], 'property',
                ['property.use1'], ['property.use1'], null, null, true, null, null, 800, 600, 'multiple', null, "[{'sortOrder':1,'fieldName':'property.use1'}]");
        }
    },
    /**
     * Select custodian action.
     */
    onSelectCustodian: function () {
        var title = getMessage('titleSelectVal_custodian');
        View.openDialog('ab-eam-custodian-select.axvw', null, false, {
            title: title,
            callback: this.afterSelectCustodian
        });
    },
    /**
     * After select custodian action.
     */
    afterSelectCustodian: function (custodian) {
        View.controllers.get('abEamAssetFilterCtrl').setCustodian(custodian);
        View.closeDialog();
    },
    /**
     * Set custodian.
     * @param {Object} custodian {name: custodianName; type: custodianType; fieldName: custodianField}
     */
    setCustodian: function (custodian) {
        this.abEamAssetFilter.setFieldValue('custodian', custodian.name);
        this.custodian = custodian;
    },
    /**
     * Select system action.
     */
    onSelectSystem: function (level) {
        var restriction = new Ab.view.Restriction({'eq_system.system_level': level});
        var title = getMessage('titleSelectVal_system_' + level);
        View.openDialog('ab-eq-system-level-select.axvw', restriction, false, {
            title: title,
            level: level.toLowerCase(),
            callback: this.afterSelectSystem
        });
    },
    /**
     * After select system action.
     */
    afterSelectSystem: function (level, system) {
        View.controllers.get('abEamAssetFilterCtrl').setSystem(level, system);
        View.closeDialog();
    },
    /**
     * Set system.
     * @param {string} level system|assembly
     * @param {string} systemName system name
     */
    setSystem: function (level, systemName) {
        this.abEamAssetFilter.setFieldValue(level + '_system_name', systemName);
    },
    /**
     * Get assigned assets for custodian.
     * @param custodian
     * @returns {Object} map of asset types with assigned asset ids.
     */
    getCustodianAssignedAssets: function (custodian) {
        var assignedAssets = null;
        if (valueExistsNotEmpty(custodian.fieldName) && valueExistsNotEmpty(custodian.code) && valueExistsNotEmpty(custodian.type)) {
            var sqlRestriction = "team." + custodian.fieldName + " = '" + custodian.code + "'" +
                " AND team.custodian_type='" + custodian.type + "'";
            var dataSource = View.dataSources.get('abCustodiansAssigned_ds');
            dataSource.clearParameters();
            dataSource.addParameter('sqlRestriction', sqlRestriction);
            var records = dataSource.getRecords();
            assignedAssets = {};
            for ( var i = 0; i < records.length; i++ ) {
                var assetId = records[i].getValue('team.asset_id'),
                    assetType = records[i].getValue('team.asset_type');
                if (!assignedAssets[assetType]) {
                    assignedAssets[assetType] = [];
                }
                assignedAssets[assetType].push(assetId);
            }
        }
        return assignedAssets;
    },
    /**
     * Get system assigned equipments.
     * @param systemName
     * @returns {Array} List of equipment ids.
     */
    getSystemAssignedAssets: function (systemName) {
        var assignedEquipments = [];
        if (valueExistsNotEmpty(systemName)) {
            var sqlRestriction = "eq_system.system_name = '" + systemName + "'";
            var dataSource = View.dataSources.get('abAssetFilter_eqSystemDs');
            var records = dataSource.getRecords(sqlRestriction);
            for ( var i = 0; i < records.length; i++ ) {
                var eqId = records[i].getValue('eq_system.eq_id_depend');
                assignedEquipments.push(eqId);
            }
        }
        return assignedEquipments;
    },
    /**
     * On action1 button.
     */
    abEamAssetFilter_onActionButton1: function () {
        if (valueExists(this.onClickActionButton1Handler)) {
            var buttonElement = Ext.get('actionButton1');
            this.onClickActionButton1Handler(buttonElement);
        }
    },
    /**
     * Set field value.
     * @param fieldName
     * @param fieldValue
     */
    setFieldValue: function (fieldName, fieldValue) {
        this.abEamAssetFilter.setFieldValue(fieldName, fieldValue);
    },
    /**
     * Validate filter settings.
     * @returns {boolean}
     */
    validateFilter: function () {
        return true;
    },
    /**
     * Set default value for customized fields.
     * @param panel
     * @param fieldId
     * @param fieldConfig
     */
    setDefaultValueForField: function (panel, fieldId, fieldConfig) {
        if ('checkbox' === fieldConfig.type) {
            $('chk_' + fieldId).checked = (valueExists(fieldConfig.dfltValue) && fieldConfig.dfltValue == 'checked');
        } else {
            if (valueExists(fieldConfig.dfltValue)) {
                panel.setFieldValue(fieldId, fieldConfig.dfltValue);
            }
        }
    },
    /**
     * Customize field.
     */
    customizeField: function (panel, fieldId, fieldConfig) {
        if (fieldConfig.hidden) {
            panel.showField(fieldId, false);
            var fieldEl = panel.getFieldElement(fieldId);
            // disable field action
            panel.showElement(fieldEl.nextSibling, false);
            return true;
        }
        if (fieldConfig.optional) {
            this.showMoreAction();
        }
        // set default value
        if (valueExists(fieldConfig.dfltValue)) {
            panel.setFieldValue(fieldId, fieldConfig.dfltValue);
        }
        // disable field if required
        if (fieldConfig.readOnly) {
            panel.enableField(fieldId, false);
        }
    },
    /**
     * Customize drop-down field.
     * @param panel
     * @param fieldId
     * @param {Object} fieldConfig {
     *                  type: enumList,
     *                  hidden: false,
     *                  readOnly: false,
     *                  dfltValue: 'allOpen',
     *                  values: {'Proposed': 'Proposed', 'Requested': 'Requested'},
     *                  hasEmptyOption: false
	 *                }
     * @returns {boolean} true if field is hidden
     */
    customizeDropDownField: function (panel, fieldId, fieldConfig) {
        var field = panel.fields.get(fieldId);
        if (fieldConfig.hidden) {
            panel.showField(fieldId, false);
            return true;
        }
        if (fieldConfig.optional) {
            this.showMoreAction();
        }
        // customize field option
        if (valueExists(fieldConfig.values)) {
            // remove all options
            field.clearOptions();
            // add new options
            for ( var opt in fieldConfig.values ) {
                var optTitle = valueExistsNotEmpty(fieldConfig.values[opt]) ? getMessage(fieldConfig.values[opt]) : field.config.enumValues[opt];
                field.addOption(opt, optTitle);
            }
        }
        if (!fieldConfig.hasEmptyOption) {
            field.removeOptions({'': ''});
        }
        // set default value
        if (valueExists(fieldConfig.dfltValue)) {
            panel.setFieldValue(fieldId, fieldConfig.dfltValue);
        }
        // disable field if required
        if (fieldConfig.readOnly) {
            panel.enableField(fieldId, false);
        }
    },
    /**
     * Customize checkbox field
     * @param panel
     * @param fieldId
     * @param fieldConfig
     * @returns {boolean}  true if field is hidden
     */
    customizeCheckboxField: function (panel, fieldId, fieldConfig) {
        if (fieldConfig.hidden) {
            panel.showField(fieldId, false);
            return true;
        }
        if (fieldConfig.optional) {
            this.showMoreAction();
        }
        // set default value
        if (valueExists(fieldConfig.dfltValue)) {
            $('chk_' + fieldId).checked = 'checked' === fieldConfig.dfltValue;
        }
    }
});