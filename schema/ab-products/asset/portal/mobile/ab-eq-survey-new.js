var abEqSurveyNewCtrl = View.createController('eqNewSurveyController', {
	//user name returned after survey creation
	userName: null,
	currentSurveyId: null,
	custodianName: '',
	sourceTable: '',
	
	afterViewLoad: function() {
		
		//hide all the buttons at title bar.
		if(Ext.get("alterButton")!=null)
			Ext.get("alterButton").dom.hidden = true;
	
		if(Ext.get("favoritesButton")!=null)
			Ext.get("favoritesButton").dom.hidden = true;
		
		if(Ext.get("printButton")!=null)
			Ext.get("printButton").dom.hidden = true;
		
		if(Ext.get("emailButton")!=null)
			Ext.get("emailButton").dom.hidden = true;
		
		if(Ext.get("loggingButton")!=null)
			Ext.get("loggingButton").dom.hidden = true;
		
		this.abEqSurveyCustodian_list.addParameter('source_em', getMessage('labelSource_em'));
		this.abEqSurveyCustodian_list.addParameter('source_contact', getMessage('labelSource_contact'));
		this.abEqSurveyCustodian_list.addParameter('source_vn', getMessage('labelSource_vn'));
	},
	
	eqNewSurvey_form_afterRefresh: function(){
		// make custodian name non editable
		this.eqNewSurvey_form.getFieldElement('survey.custodian_name').disabled = true;
	}

});

function onCreateSurvey() {
	var surveyForm = View.panels.get('eqNewSurvey_form');
	
	var survey_id = surveyForm.getFieldValue('survey.survey_id');
	var performed_by = surveyForm.fields.get('survey.em_id').dom.value;
	var survey_date = surveyForm.getFieldValue('survey.survey_date');
	var description = surveyForm.getFieldValue('survey.description');
	var bl_id = surveyForm.getFieldValue('survey.bl_id');
	var fl_id = surveyForm.getFieldValue('survey.fl_id');
	var dv_id = surveyForm.getFieldValue('survey.dv_id');
	var dp_id = surveyForm.getFieldValue('survey.dp_id');
	var eq_std = surveyForm.getFieldValue('survey.eq_std');
	
	surveyForm.clearValidationResult();
	if(!surveyForm.validateFields()){
		return;
	}
	
	var emDs = View.dataSources.get('emUser_ds');
	var dsRestriction = new Ab.view.Restriction();
	dsRestriction.addClause("em.em_id", makeLiteral(performed_by), "=");
	
	var records = emDs.getRecords(dsRestriction);
	
    if(records==null || records.length<1){
    	surveyForm.validationResult.valid = false;
    	surveyForm.validationResult.message = getMessage('errorInvalidEmployee1') + "[" + performed_by +"]. " + getMessage('errorInvalidEmployee2');
    	surveyForm.validationResult.invalidFields['survey.em_id'] = "";
    	surveyForm.displayValidationResult();
    	return false;
    } else {
    	var result = null;
	   	try {
		   	result = Workflow.callMethod('AbAssetManagement-AssetMobileService-createSurvey', survey_id, survey_date,
					performed_by, description);
       	}catch (e) {
     		if (e.code=='ruleFailed'){
       		  View.showMessage(e.message);
       		}else{
     		  Workflow.handleError(e);
     		}
     		return;
       	}	 
		if (result.code == 'executed') {
			//creatSurvey returns the user name
			abEqSurveyNewCtrl.userName = result.message;
			
			updateSurveyField(survey_id, 'survey.survey_fields', getEqFieldsActivityParamValue());
			
			importEquipmentToSurvey();
			
			abEqSurveyNewCtrl.currentSurveyId = survey_id;
			var tabPanel = View.panels.get('abEqSurveyNew_tabs'); 
			tabPanel.selectTab('abEqSurveyNew_tab2');
		}
    }
}

function importEquipmentToSurvey(){
	var surveyForm = View.panels.get('eqNewSurvey_form');
	
	var survey_id = surveyForm.getFieldValue('survey.survey_id');
	var bl_id = surveyForm.getFieldValue('survey.bl_id');
	var fl_id = surveyForm.getFieldValue('survey.fl_id');
	var dv_id = surveyForm.getFieldValue('survey.dv_id');
	var dp_id = surveyForm.getFieldValue('survey.dp_id');
	var eq_std = surveyForm.getFieldValue('survey.eq_std');
	var custodian_name = abEqSurveyNewCtrl.custodianName;
	var source_table = abEqSurveyNewCtrl.sourceTable;
	
	
	var wfrParameters = {
			'survey_id' : survey_id,
			'bl_id': bl_id,
			'fl_id': fl_id,
			'dv_id': dv_id,
			'dp_id': dp_id,
			'user_name': abEqSurveyNewCtrl.userName,
			'eq_std': eq_std,
			'custodian_name': custodian_name,
			'source_table': source_table
	}
	
	
	try {
		result = Workflow.callMethod('AbAssetManagement-AssetMobileService-importEquipmentToSurvey2', wfrParameters);
    }catch (e) {
    	if (e.code=='ruleFailed'){
    		View.showMessage(e.message);
    	}else{
    		Workflow.handleError(e);
     	}
     	return;
    }	
       	
    if (result.code == 'executed') {
		 //if no record exists in eq table, ask user if they like to create a new survey without any records in eq_audit table.
		 if(result.value<1){
			 View.confirm(getMessage('noEqRecordsConfirmMessage'), function(button) {
			    if (button == 'yes') {
			    	 // new survey is created, close the dialog
			    	refreshSurveyAndTasksGrids();
				} else {
					// delete the new survey, 
					try {
						result = Workflow.callMethod('AbAssetManagement-AssetMobileService-deleteSurvey', survey_id);
				    }catch (e) {
				    	if (e.code=='ruleFailed'){
				    		View.showMessage(e.message);
				    	}else{
				    		Workflow.handleError(e);
				     	}
				     	return;
				    }	
				    if (result.code == 'executed') {
				    	refreshSurveyAndTasksGrids();
						
						//close dialog
						View.getOpenerView().closeDialog();
					}
				}
			});
		 } else {
			 refreshSurveyAndTasksGrids();
		 }
	 }
}

function refreshSurveyAndTasksGrids(){
	View.getOpenerView().panels.get('eqSurveyGrid_grid').refresh();
	//clear the task grid
	View.getOpenerView().panels.get('eqSurveyTasksGrid_grid').refresh("1!=1");
}

function updateSurveyField(surveyId, fieldName, fieldValue){
	var surveyDs = abEqSurveyNewCtrl.eqNewSurvey_ds;
	
	var restriction = new Ab.view.Restriction();
	restriction.addClause('survey.survey_id', surveyId, '=');
	
	var surveyRecord = surveyDs.getRecord(restriction);
	if(surveyRecord && fieldName && fieldValue){
		surveyRecord.setValue(fieldName, fieldValue);
		surveyRecord.isNew = false;
		surveyDs.saveRecord(surveyRecord);
	}
}

function makeLiteral(value){
	return "'" + value.replace(/\'/g, "''") +"'";
}

/**
 * Select value for custodian name.
 * 
 */
function onSelectCustodianName(){
	var objGrid = View.panels.get('abEqSurveyCustodian_list');
	var gridRow = objGrid.gridRows.get(objGrid.selectedRowIndex);
	var record = gridRow.getRecord();
	var objForm = View.panels.get('eqNewSurvey_form');
	objForm.setFieldValue('survey.custodian_name', record.getValue('team.custodian_name'));
	objForm.setFieldValue('survey.source_table', record.getValue('team.source_table'));
	abEqSurveyNewCtrl.custodianName = record.getValue('team.custodian_name');
	abEqSurveyNewCtrl.sourceTable = record.getValue('team.source_table');
	objGrid.closeWindow();
}

/**
 * Clear custodian name field.
 * 
 */
function clearCustodianName(){
	var objGrid = View.panels.get('abEqSurveyCustodian_list');
	var objForm = View.panels.get('eqNewSurvey_form');
	objForm.setFieldValue('survey.custodian_name', '');
	objForm.setFieldValue('survey.source_table', '');
	abEqSurveyNewCtrl.custodianName = '';
	abEqSurveyNewCtrl.sourceTable = '';
	objGrid.closeWindow();
}

/**
 * Check license.
 * @param {string} licenseId - license to check
 * @returns {boolean} has license enabled
 */
function checkLicense(licenseId) {
    var hasLicense = false;
    AdminService.getProgramLicense({
        async: false,
        callback: function (licenseDTO) {
            for ( var i = 0; i < licenseDTO.licenses.length; i++ ) {
                var license = licenseDTO.licenses[i];
                if (licenseId === license.id) {
                    hasLicense = license.enabled;
                    break;
                }
            }
        },
        errorHandler: function (m, e) {
            View.showException(e);
        }
    });
    return hasLicense;
}