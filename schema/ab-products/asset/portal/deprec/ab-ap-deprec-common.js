/**
 * Calls the WFR for recalculation depreciation
 * for equipments or tagged furniture, depending on the parameter <forWhat>
 * @param {String} forWhat Values in 'eq','ta'
 * @param {String} panelToRefresh
 */
function recalculateDepreciation(forWhat, panelToRefresh){
	// confirm recalculation
	var message = getMessage('confirmRecalculation');
	var jobName = "AbCommonResources-DepreciationService-updateEquipmentDepreciation";

	switch(forWhat) {
		case 'eq':
			jobName = "AbCommonResources-DepreciationService-updateEquipmentDepreciation";
			break;
		case 'ta':
		    jobName = "AbCommonResources-DepreciationService-updateFurnitureDepreciation";
			break;
		default: 
			break;
	}


	View.confirm(message, function(button) {
		if (button == 'yes') {
			try {
			    var jobId = Workflow.startJob(jobName);
				View.openJobProgressBar(getMessage('calculateMessage'), jobId, '', function(status) {
					if(View.panels.get(panelToRefresh).visible){
						View.panels.get(panelToRefresh).refresh();
					}
			    });
			} 
			catch (e) {
				Workflow.handleError(e);
			}
		}
	});
}
