View.createController('fnPlansController', {
    drawingController: null,
    afterViewLoad: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['rm'],
            assetTooltip: [
                {assetType: 'rm', datasource: 'tooltipRmDs', fields: 'rm.rm_id;rm.rm_std'}
            ],
            layersPopup: {
                layers: "rm-assets;rm-labels;gros-labels;background",
                defaultLayers: "rm-assets;rm-labels;gros-assets;background"
            },
            listeners: {
                afterDrawingLoaded: function (drawingController, svgId) {
                    drawingController.drawingPanel.setTitle(getMessage("drwPanelTitle"));
                    var drawing = drawingController.drawings[svgId],
                        pkeyValues = drawing.pkeyValues;
                    var buildingId = pkeyValues.blId;
                    var floorId = pkeyValues.flId;
                    var drawingName = drawing.drawingName;
                    var restriction = new Ab.view.Restriction({
                        'rm.bl_id': buildingId,
                        'rm.fl_id': floorId,
                        'rm.dwgname': drawingName
                    });
                    var records = View.dataSources.get('abFnPlansDrwHighlight_ds').getRecords(restriction);
                    for ( var i = 0; i < records.length; i++ ) {
                        var record = records[i];
                        var id = record.getValue('rm.bl_id') + ";" + record.getValue('rm.fl_id') + ";" + record.getValue('rm.rm_id');
                        drawingController.svgControl.getController("HighlightController").highlightAsset(id, {
                            svgId: svgId,
                            color: 'yellow'
                        });
                    }
                },
                onClickAssetAfter: function (drawingController, svgId, selectedAssetId, assetType, selectedAssets, selected) {
                    if ('rm' === assetType) {
                        if (selected) {
                            var drawing = drawingController.drawings[svgId],
                                pkeyValues = drawing.pkeyValues;
                            var blId = pkeyValues.blId,
                                flId = pkeyValues.flId;
                            drawingController.svgControl.getAddOn('InfoWindow').setText(String.format(getMessage('selectedRoomMsg'), blId, flId, selectedAssetId));
                            var restriction = new Ab.view.Restriction({
                                'fn.bl_id': blId,
                                'fn.fl_id': flId,
                                'fn.rm_id': selectedAssetId.split(';')[2]
                            });
                            View.panels.get('abFnPlansSummary_panel').refresh(restriction);
                            View.panels.get('abFnPlansList_panel').refresh(restriction);
                        } else {
                            drawingController.svgControl.getAddOn('InfoWindow').show(false);
                            View.panels.get('abFnPlansSummary_panel').show(false);
                            View.panels.get('abFnPlansList_panel').show(false);
                        }
                    }
                }
            }
        });
    },
    loadDrawingPanel: function () {
        //hide details panels
        this.abFnPlansSummary_panel.show(false);
        this.abFnPlansList_panel.show(false);
        //get selected building and floor and show drawing
        var currentNode = View.panels.get('abFnPlansTreeBl_panel').lastNodeClicked;
        this.drawingController.showDrawing({
            pkeyValues: {blId: currentNode.parent.data['bl.bl_id'], flId: currentNode.data['fl.fl_id']}
        });
    }
});