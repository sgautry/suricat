View.createController('policyController', {
    /**
     * Check if end date is greater than start date
     * @returns {boolean} true if form can save
     */
    formPolicyEdit_Edit_beforeSave: function () {
        var canSave = true;
        var form = View.panels.get('formPolicyEdit_Edit');
        var ds = form.getDataSource();
        var localizedStartDate = form.getFieldValue('policy.date_start');
        var localizedEndDate = form.getFieldValue('policy.date_end');
        if (valueExistsNotEmpty(localizedStartDate) && valueExistsNotEmpty(localizedEndDate)) {
            var startDate = ds.parseValue('policy.date_start', localizedStartDate, false);
            var endDate = ds.parseValue('policy.date_end', localizedEndDate, false);
            if (startDate >= endDate) {
                form.fields.get('policy.date_end').setInvalid(getMessage('dates_invalid_text'));
                canSave = false;
            }
        }
        return canSave;
    }
});