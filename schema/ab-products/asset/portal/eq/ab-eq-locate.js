View.createController('abEqLocate_controller', {
    drawingController: null,
    afterViewLoad: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTooltip: [
                {assetType: 'rm', datasource: 'rmLabelDs', fields: 'rm.rm_id;rm.rm_cat;rm.rm_type;rm.area'},
                {assetType: 'eq', datasource: 'tooltipEqDs', fields: 'eq.eq_id;eq.eq_std;'},
                {assetType: 'jk', datasource: 'tooltipJkDs', fields: 'jk.jk_id;jk.jk_std'},
                {assetType: 'fp', datasource: 'tooltipFpDs', fields: 'fp.fp_id;fp.fp_std'},
                {assetType: 'pn', datasource: 'tooltipPnDs', fields: 'pn.pn_id;pn.pn_std'}
            ],
            layersPopup: {
                layers: "rm-assets;rm-labels;eq-assets;eq-labels;jk-assets;jk-labels;fp-assets;fp-labels;pn-assets;pn-labels;gros-assets;gros-labels;background",
                defaultLayers: "rm-assets;rm-labels;eq-assets;jk-assets;fp-assets;pn-assets;gros-assets;background"
            },
            showPanelTitle: true,
            listeners: {
                afterDrawingLoaded: function (drawingController, svgId) {
                    View.controllers.get('abEqLocate_controller').highlightRoomAssets(drawingController, svgId);
                }
            }
        });
    },
    abEqLocate_console_onShow: function () {
        var restriction = this.abEqLocate_console.getFieldRestriction();
        var sqlRestriction = "1 = 1";
        var clauses = restriction.clauses;
        for ( var i = 0; i < clauses.length; i++ ) {
            sqlRestriction += " AND " + clauses[i].name + " " + clauses[i].op + " '" + clauses[i].value + "'";
        }
        this.abEqLocate_roomGrid.addParameter("consoleRestriction", sqlRestriction);
        this.abEqLocate_roomGrid.refresh();
    },
    showFloorDrawing: function () {
        var roomGrid = View.panels.get('abEqLocate_roomGrid');
        var selectedRowIndex = roomGrid.selectedRowIndex,
            rows = roomGrid.rows;
        var buildingId = rows[selectedRowIndex]['rm.bl_id'],
            floorId = rows[selectedRowIndex]['rm.fl_id'],
            roomId = rows[selectedRowIndex]['rm.rm_id'],
            eqId = this.abEqLocate_console.getFieldValue('eq.eq_id');
        this.refreshAssetPanels(buildingId, floorId, roomId);
        this.highlightRoom(buildingId, floorId, roomId, eqId);
    },
    highlightRoom: function (buildingId, floorId, roomId, eqId) {
        this.drawingController.showDrawing({
            pkeyValues: {blId: buildingId, flId: floorId},
            findAssets: [{
                assetType: 'rm',
                assetIds: [roomId]
            }],
            highlightAssets: [{
                assetType: 'eq',
                assetIds: [eqId],
                color: 'blue'
            }]
        });
    },
    highlightRoomAssets: function (drawingController, svgId) {
        this.highlightAssets('abEqLocate_eqList', drawingController, svgId, 'blue');
        this.highlightAssets('abEqLocate_fpList', drawingController, svgId, 'green');
        this.highlightAssets('abEqLocate_jkList', drawingController, svgId, 'red');
        this.highlightAssets('abEqLocate_pbList', drawingController, svgId, 'brown');
        this.highlightAssets('abEqLocate_pnList', drawingController, svgId, 'magenta');
    },
    highlightAssets: function (panelName, drawingController, svgId, color) {
        var assetsToHighlight = {};
        var grid = View.panels.get(panelName);
        grid.gridRows.each(function (row) {
            var record = row.getRecord();
            var assetId = record.getValue(grid.primaryKeyIds[0]);
            assetsToHighlight[assetId] = {
                svgId: svgId,
                color: color,
                persistFill: false,
                overwriteFill: true
            };
        });
        if (!_.isEmpty(assetsToHighlight)) {
            drawingController.svgControl.getController("HighlightController").highlightAssets(assetsToHighlight);
        }
    },
    refreshAssetPanels: function (buildingId, floorId, roomId) {
        var restriction = new Ab.view.Restriction({
            'rm.bl_id': buildingId,
            'rm.fl_id': floorId,
            'rm.rm_id': roomId
        });
        this.abEqLocate_buttons.refresh(restriction);
        this.abEqLocate_emList.refresh(restriction);
        this.abEqLocate_eqList.refresh(restriction);
        this.abEqLocate_fpList.refresh(restriction);
        this.abEqLocate_jkList.refresh(restriction);
        this.abEqLocate_pbList.refresh(restriction);
        this.abEqLocate_pnList.refresh(restriction);
    }
});