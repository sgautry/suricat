View.createController('abApJkPlansCtrl', {
    drawingController: null,
    afterViewLoad: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['rm', 'jk'],
            highlightParameters: [{
                'view_file': "ab-ap-jk-plans.axvw",
                'hs_ds': "ds_abApJkPlans_highlight",
                'label_ds': 'ds_abApJkPlans_label',
                showSelector: false
            }],
            assetTooltip: [
                {assetType: 'rm', datasource: 'ds_abApJkPlans_rmTooltip', fields: 'rm.rm_id;rm.rm_std'},
                {assetType: 'jk', datasource: 'ds_abApJkPlans_jkDetails', fields: 'jk.jk_id;jk.jk_std;jk.em_id;jk.tc_use;jk.tc_use_status'}
            ],
            layersPopup: {
                layers: "rm-assets;rm-labels;jk-assets;jk-labels;gros-labels;background",
                defaultLayers: "rm-assets;rm-labels;jk-assets;jk-labels;gros-assets;background"
            },
            listeners: {
                afterDrawingLoaded: function (drawingController, svgId) {
                    drawingController.svgControl.getController("HighlightController").highlightAssetsByType('jk', {});
                    var currentNode = View.panels.get('panel_abApJkPlans_blTree').lastNodeClicked,
                        buildingId = currentNode.parent.data['bl.bl_id'],
                        floorId = currentNode.data['bl.state_id'],
                        selectedFlName = currentNode.data['bl.name'];
                    drawingController.drawingPanel.setTitle(String.format(getMessage("title_dwg"), buildingId, floorId, selectedFlName));
                }
            }
        });
    },
    showJacks: function () {
        var currentNode = View.panels.get('panel_abApJkPlans_blTree').lastNodeClicked;
        var buildingId = currentNode.parent.data['bl.bl_id'];
        var floorId = currentNode.data['bl.state_id'];
        this.showDrawing(buildingId, floorId);
        this.refreshJacksPanel(buildingId, floorId);
    },
    showDrawing: function (buildingId, floorId) {
        this.drawingController.showDrawing({
            pkeyValues: {blId: buildingId, flId: floorId}
        });
    },
    refreshJacksPanel: function (buildingId, floorId) {
        var restriction = new Ab.view.Restriction({
            'jk.bl_id': buildingId,
            'jk.fl_id': floorId
        });
        this.panel_abApJkPlans_jkDetails.refresh(restriction);
    },
    /**
     * Generate paginated report for user selection
     */
    panel_abApJkPlans_console_onPaginatedReport: function () {
        var siteId = this.panel_abApJkPlans_console.getFieldValue("bl.site_id");
        var blId = this.panel_abApJkPlans_console.getFieldValue("bl.bl_id");
        var parameters = null;
        var consoleRestrictionForJk = "";
        if (valueExistsNotEmpty(siteId)) {
            consoleRestrictionForJk += "(jk.bl_id IN (SELECT DISTINCT bl_id FROM bl WHERE site_id='" + siteId + "'))";
        }
        if (valueExistsNotEmpty(blId)) {
            consoleRestrictionForJk += (consoleRestrictionForJk != "" ? " AND " : "");
            consoleRestrictionForJk += "jk.bl_id = '" + blId + "'";
        }
        if (consoleRestrictionForJk != "") {
            consoleRestrictionForJk = "(" + consoleRestrictionForJk + ")";
            parameters = {
                'consoleRestrictionForJk': consoleRestrictionForJk
            };
        }
        View.openPaginatedReportDialog('ab-ap-jk-plans-pgrp.axvw', null, parameters);
    }
});