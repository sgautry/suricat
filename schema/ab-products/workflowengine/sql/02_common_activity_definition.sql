-- Common activities

-- // MSSQL SELECT 'Script executed in ' + DB_NAME() + ' on ' + CAST(GETDATE() AS varchar) AS ScriptExecutedin SET XACT_ABORT ON;
-- // ORACLE SELECT 'Script executed in ' || SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA') || ' on ' || TO_CHAR(SYSTIMESTAMP(1)) AS ScriptExecutedin FROM dual SET XACT_ABORT ON;


-- afm_activities
INSERT INTO afm_activities (activity_id, title, version, adn_mfr)
  VALUES ('WorkflowEngine', 'Graphical Workflow Engine', 1, 'ASC-RO Workflow Engine');
	
-- afm_wf_rules
INSERT INTO afm_wf_rules (activity_id, rule_id, rule_type, is_active, description, xml_rule_props)
  VALUES ('WorkflowEngine', 'WorkflowEngineService', 'Message', 1, 'ASC-RO Workflow Engine','<xml_rule_properties description=""><eventHandlers><eventHandler class="com.archibus.we.WorkflowService" method=""><inputs/></eventHandler></eventHandlers></xml_rule_properties>');
	
-- afm_products
INSERT INTO afm_products (product_id, title, version, summary, display_order, icon_small)
  VALUES ('WorkflowEngine', 'Workflow Engine', 1, 'Create Graphical Workflows', 31000, 'ab-icon-wf32.png');

-- afm_processes
INSERT INTO afm_processes (activity_id, process_id, title, display_order, is_active, process_type, summary)
  VALUES ('WorkflowEngine', 'Manage Graphical Workflow', 'Workflow Engine', 100, 1, 'WEB', 'Manage Graphical Workflow');
	
-- afm_ptasks
INSERT INTO afm_ptasks (activity_id, process_id, task_id, display_order, task_file, task_type)
  VALUES ('WorkflowEngine', 'Manage Graphical Workflow', 'Define Roles', 10, 'ab-wf-roles.axvw', 'WEB URL');
INSERT INTO afm_ptasks (activity_id, process_id, task_id, display_order, task_file, task_type)
  VALUES ('WorkflowEngine', 'Manage Graphical Workflow', 'Define Messages', 20, 'ab-wf-messages.axvw', 'WEB URL');
INSERT INTO afm_ptasks (activity_id, process_id, task_id, display_order, task_file, task_type)
  VALUES ('WorkflowEngine', 'Manage Graphical Workflow', 'Define Notifications', 30, 'ab-wf-notifications.axvw', 'WEB URL');
INSERT INTO afm_ptasks (activity_id, process_id, task_id, display_order, task_file, task_type)
  VALUES ('WorkflowEngine', 'Manage Graphical Workflow', 'Define Workflow', 40, 'ab-wf-define-types.axvw', 'WEB URL');
INSERT INTO afm_ptasks (activity_id, process_id, task_id, display_order, task_file, task_type)
  VALUES ('WorkflowEngine', 'Manage Graphical Workflow', 'Define Workflow Management Views', 50, 'ab-wf-mng-view.axvw', 'WEB URL');
INSERT INTO afm_ptasks (activity_id, process_id, task_id, display_order, task_file, task_type)
  VALUES ('WorkflowEngine', 'Manage Graphical Workflow', 'User Help', 55, 'ab-products/workflowengine/help/index.htm', 'WEB URL');  

-- afm_actprods
INSERT INTO afm_actprods (product_id, activity_id, activity_cat_id)
  VALUES ('WorkflowEngine', 'WorkflowEngine', 'NONE');
  
-- afm_roleprocs for ACTIVITY LICENSEE role
INSERT INTO afm_roleprocs (transfer_status, role_name, activity_id, process_id)
VALUES ('NO CHANGE', 'ACTIVITY LICENSEE', 'WorkflowEngine', 'Manage Graphical Workflow');