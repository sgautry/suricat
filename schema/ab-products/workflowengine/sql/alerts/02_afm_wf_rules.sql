INSERT INTO afm_wf_rules (rule_id,
                          activity_id,
                          rule_type,
                          is_active,
                          xml_rule_props,
                          xml_sched_props,
                          description)
     VALUES ('sendWorkflowAlerts',
             'WorkflowEngine',
             'Scheduled',
             1,
             '<xml_rule_properties description=""><eventHandlers><eventHandler class="com.archibus.we.sched.WorkflowScheduledService" method="sendWorkflowAlerts"><inputs/></eventHandler></eventHandlers></xml_rule_properties>',
             '<xml_schedule_properties>    <schedule startTime="01-01-2004 12:05:00" endTime="" runOnStartup="true">          <simple repeatCount="-1" repeatInterval="900"/>       </schedule>   </xml_schedule_properties>',
             'ASC-RO Workflow Engine');
commit;