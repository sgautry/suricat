delete from afm_flds where table_name = 'wf_mng_alerts';
delete from afm_flds_lang where table_name = 'wf_mng_alerts';
delete from afm_tbls where table_name = 'wf_mng_alerts';
insert into afm_tbls(table_name,comments,afm_module,title,is_sql_view) values('wf_mng_alerts','ASC-RO Workflow Engine',1,'Workflow Alerts',0);
insert into afm_flds(table_name,field_name,ml_heading,data_type,afm_size,decimals,allow_null,dflt_val,primary_key,afm_type,string_format,num_format,validate_data,is_tc_traceable,comments,is_atxt,afm_module) values('wf_mng_alerts','alert_id','Alert ID',4,4,0,0,'AUTOINCREMENT',1,2050,25,0,1,0,'ASC-RO Workflow Engine',0,1);
insert into afm_flds(table_name,field_name,ml_heading,data_type,afm_size,decimals,allow_null,primary_key,afm_type,string_format,num_format,validate_data,is_tc_traceable,comments,is_atxt,afm_module) values('wf_mng_alerts','alert_name','Alert Name',12,64,0,0,0,2050,5,0,1,0,'ASC-RO Workflow Engine',0,1);
insert into afm_flds(table_name,field_name,ml_heading,data_type,afm_size,decimals,allow_null,primary_key,ref_table,afm_type,string_format,num_format,validate_data,is_tc_traceable,comments,is_atxt,afm_module) values('wf_mng_alerts','block_id','Block Id',12,32,0,1,0,'wf_blocks',2050,5,0,1,0,'ASC-RO Workflow Engine',0,1);
insert into afm_flds(table_name,field_name,ml_heading,data_type,afm_size,decimals,allow_null,primary_key,afm_type,string_format,num_format,validate_data,is_tc_traceable,comments,is_atxt,afm_module) values('wf_mng_alerts','color','Alert Color',1,8,0,0,0,2050,5,0,1,0,'ASC-RO Workflow Engine',0,1);
insert into afm_flds(table_name,field_name,ml_heading,data_type,afm_size,decimals,allow_null,dflt_val,primary_key,afm_type,string_format,num_format,validate_data,is_tc_traceable,comments,is_atxt,afm_module) values('wf_mng_alerts','duration','Pending Duration',4,10,0,0,'0',0,2050,5,0,1,0,'ASC-RO Workflow Engine',0,1);
insert into afm_flds(table_name,field_name,ml_heading,data_type,afm_size,decimals,allow_null,primary_key,ref_table,afm_type,string_format,num_format,validate_data,is_tc_traceable,comments,is_atxt,afm_module) values('wf_mng_alerts','mng_id','Mng Id',4,4,0,0,0,'wf_mng_view',2050,25,0,1,0,'ASC-RO Workflow Engine',0,1);
insert into afm_flds(table_name,field_name,ml_heading,data_type,afm_size,decimals,allow_null,primary_key,ref_table,afm_type,string_format,num_format,validate_data,is_tc_traceable,comments,is_atxt,afm_module) values('wf_mng_alerts','notification_id','Notification Code',12,64,0,0,0,'wf_notifications',2050,10,0,1,0,'ASC-RO Workflow Engine',0,1);
insert into afm_flds(table_name,field_name,ml_heading,data_type,afm_size,decimals,allow_null,primary_key,ref_table,afm_type,string_format,num_format,validate_data,is_tc_traceable,comments,is_atxt,afm_module) values('wf_mng_alerts','role','Role Notified',12,64,0,0,0,'wf_role',2050,5,0,1,0,'ASC-RO Workflow Engine',0,1);
insert into afm_flds(table_name,field_name,ml_heading,data_type,afm_size,decimals,allow_null,dflt_val,primary_key,afm_type,enum_list,string_format,num_format,validate_data,is_tc_traceable,comments,is_atxt,afm_module) values('wf_mng_alerts','unit','Duration Unit',1,8,0,0,'hour',0,2050,'day;Days;hour;Hours;minute;Minutes',5,0,1,0,'ASC-RO Workflow Engine',0,1);
insert into afm_flds(table_name,field_name,ml_heading,data_type,afm_size,decimals,allow_null,primary_key,afm_type,string_format,num_format,validate_data,is_tc_traceable,comments,is_atxt,afm_module) values('wf_mng_alerts','wf_type','Workflow Type',12,64,0,1,0,2050,10,0,1,0,'ASC-RO Workflow Engine',0,1);
commit;
--drop table wf_mng_alerts;
create table wf_mng_alerts(
alert_id int not null,
alert_name varchar2(64) not null,
block_id varchar2(32),
color char(8) not null,
duration int default 0 not null,
mng_id int not null,
notification_id varchar2(64) not null,
role varchar2(64) not null,
unit char(8) default 'hour' not null,
wf_type varchar2(64)
);
alter table wf_mng_alerts add constraint wf_mng_alerts_PK primary key (alert_id);
alter table wf_mng_alerts add constraint wf_mng_alerts_wf_blocks foreign key (block_id,wf_type) references wf_blocks(block_id,wf_type) ;
alter table wf_mng_alerts add constraint wf_mng_alerts_wf_mng_view foreign key (mng_id) references wf_mng_view(mng_id) ;
alter table wf_mng_alerts add constraint wf_mng_alerts_wf_notifications foreign key (notification_id) references wf_notifications(notification_id) ;
alter table wf_mng_alerts add constraint wf_mng_alerts_wf_role foreign key (role) references wf_role(role) ;

CREATE SEQUENCE AFM.WF_MNG_ALERTS_S
START WITH 1
MAXVALUE 999999999999999999999999999
MINVALUE 1
NOCYCLE
CACHE 20
ORDER;
/
CREATE OR REPLACE TRIGGER AFM.WF_MNG_ALERTS_T
BEFORE INSERT ON AFM.WF_MNG_ALERTS FOR EACH ROW
WHEN (NEW.ALERT_ID IS NULL)
BEGIN
SELECT WF_MNG_ALERTS_S.NEXTVAL INTO :NEW.ALERT_ID FROM DUAL;
END;
/ 

commit;