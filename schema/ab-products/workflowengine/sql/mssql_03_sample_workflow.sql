--workflow table
INSERT INTO afm.afm_tbls (table_name, comments, afm_module, title, is_sql_view, title_ch, title_de, title_es, title_fr, title_it, title_jp, title_ko, title_nl, title_no, title_zh, title_01, title_02, title_03, default_view, table_type, transfer_status)
VALUES ('wf_requests', NULL, 1, 'Worflow Requests', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PROJECT DATA', 'PENDING')
GO

INSERT INTO afm.afm_flds (table_name, field_name, afm_type, allow_null, comments, data_type, decimals, dep_cols, dflt_val, edit_group, edit_mask, enum_list, is_atxt, max_val, min_val, ml_heading, afm_module, num_format, primary_key, ref_table, review_group, afm_size, sl_heading, string_format, is_tc_traceable, field_grouping, attributes, validate_data, transfer_status)
VALUES ('wf_requests', 'ac_id', 2050, 1, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Account Code', 1, 0, 0, 'ac', NULL, 32, NULL, 10, 0, NULL, NULL, 1, 'NO ACTION')
GO

INSERT INTO afm.afm_flds (table_name, field_name, afm_type, allow_null, comments, data_type, decimals, dep_cols, dflt_val, edit_group, edit_mask, enum_list, is_atxt, max_val, min_val, ml_heading, afm_module, num_format, primary_key, ref_table, review_group, afm_size, sl_heading, string_format, is_tc_traceable, field_grouping, attributes, validate_data, transfer_status)
VALUES ('wf_requests', 'bl_id', 2050, 1, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Building
Code', 5, 0, 0, 'bl', NULL, 8, NULL, 10, 3, NULL, NULL, 1, 'NO ACTION')
GO

INSERT INTO afm.afm_flds (table_name, field_name, afm_type, allow_null, comments, data_type, decimals, dep_cols, dflt_val, edit_group, edit_mask, enum_list, is_atxt, max_val, min_val, ml_heading, afm_module, num_format, primary_key, ref_table, review_group, afm_size, sl_heading, string_format, is_tc_traceable, field_grouping, attributes, validate_data, transfer_status)
VALUES ('wf_requests', 'block_id', 2050, 0, NULL, 12, 0, 'block_id', NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Block Id', 1, 0, 0, 'wf_blocks', NULL, 32, NULL, 5, 0, NULL, NULL, 1, 'NO ACTION')
GO

INSERT INTO afm.afm_flds (table_name, field_name, afm_type, allow_null, comments, data_type, decimals, dep_cols, dflt_val, edit_group, edit_mask, enum_list, is_atxt, max_val, min_val, ml_heading, afm_module, num_format, primary_key, ref_table, review_group, afm_size, sl_heading, string_format, is_tc_traceable, field_grouping, attributes, validate_data, transfer_status)
VALUES ('wf_requests', 'comments', 2050, 1, NULL, 12, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Comments', 1, 0, 0, NULL, NULL, 4000, NULL, 40, 0, NULL, NULL, 1, 'NO ACTION')
GO

INSERT INTO afm.afm_flds (table_name, field_name, afm_type, allow_null, comments, data_type, decimals, dep_cols, dflt_val, edit_group, edit_mask, enum_list, is_atxt, max_val, min_val, ml_heading, afm_module, num_format, primary_key, ref_table, review_group, afm_size, sl_heading, string_format, is_tc_traceable, field_grouping, attributes, validate_data, transfer_status)
VALUES ('wf_requests', 'cost_estimated', 2050, 0, NULL, 2, 2, NULL, '0', NULL, NULL, NULL, 0, NULL, NULL, 'Estimated Cost', 5, 1, 0, NULL, NULL, 12, NULL, 25, 0, NULL, NULL, 1, 'NO ACTION')
GO

INSERT INTO afm.afm_flds (table_name, field_name, afm_type, allow_null, comments, data_type, decimals, dep_cols, dflt_val, edit_group, edit_mask, enum_list, is_atxt, max_val, min_val, ml_heading, afm_module, num_format, primary_key, ref_table, review_group, afm_size, sl_heading, string_format, is_tc_traceable, field_grouping, attributes, validate_data, transfer_status)
VALUES ('wf_requests', 'cost_final', 2050, 0, NULL, 2, 2, NULL, '0', NULL, NULL, NULL, 0, NULL, NULL, 'Final Cost', 5, 1, 0, NULL, NULL, 12, NULL, 25, 0, NULL, NULL, 1, 'NO ACTION')
GO

INSERT INTO afm.afm_flds (table_name, field_name, afm_type, allow_null, comments, data_type, decimals, dep_cols, dflt_val, edit_group, edit_mask, enum_list, is_atxt, max_val, min_val, ml_heading, afm_module, num_format, primary_key, ref_table, review_group, afm_size, sl_heading, string_format, is_tc_traceable, field_grouping, attributes, validate_data, transfer_status)
VALUES ('wf_requests', 'date_requested', 2050, 0, NULL, 9, 0, NULL, 'CURRENT', NULL, NULL, NULL, 0, NULL, NULL, 'Date Requested', 1, 0, 0, NULL, NULL, 8, NULL, 5, 0, NULL, NULL, 1, 'NO ACTION')
GO

INSERT INTO afm.afm_flds (table_name, field_name, afm_type, allow_null, comments, data_type, decimals, dep_cols, dflt_val, edit_group, edit_mask, enum_list, is_atxt, max_val, min_val, ml_heading, afm_module, num_format, primary_key, ref_table, review_group, afm_size, sl_heading, string_format, is_tc_traceable, field_grouping, attributes, validate_data, transfer_status)
VALUES ('wf_requests', 'date_required', 2050, 1, NULL, 9, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Date Required', 5, 0, 0, NULL, NULL, 8, NULL, 5, 0, NULL, NULL, 1, 'NO ACTION')
GO

INSERT INTO afm.afm_flds (table_name, field_name, afm_type, allow_null, comments, data_type, decimals, dep_cols, dflt_val, edit_group, edit_mask, enum_list, is_atxt, max_val, min_val, ml_heading, afm_module, num_format, primary_key, ref_table, review_group, afm_size, sl_heading, string_format, is_tc_traceable, field_grouping, attributes, validate_data, transfer_status)
VALUES ('wf_requests', 'description', 2050, 1, NULL, 12, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Description', 1, 0, 0, NULL, NULL, 200, NULL, 40, 0, NULL, NULL, 1, 'NO ACTION')
GO

INSERT INTO afm.afm_flds (table_name, field_name, afm_type, allow_null, comments, data_type, decimals, dep_cols, dflt_val, edit_group, edit_mask, enum_list, is_atxt, max_val, min_val, ml_heading, afm_module, num_format, primary_key, ref_table, review_group, afm_size, sl_heading, string_format, is_tc_traceable, field_grouping, attributes, validate_data, transfer_status)
VALUES ('wf_requests', 'doc1', 2165, 1, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Document', 1, 0, 0, NULL, NULL, 64, NULL, 5, 0, NULL, '<root>
<documentManagement maxDocumentSize="10000000" versioningOn="true" autoNameFile="true">
  <lockingOptions lockOnCheckout="true" clearLocksOnCheckIn="true"/>
 </documentManagement>
</root>', 1, 'NO ACTION')
GO

INSERT INTO afm.afm_flds (table_name, field_name, afm_type, allow_null, comments, data_type, decimals, dep_cols, dflt_val, edit_group, edit_mask, enum_list, is_atxt, max_val, min_val, ml_heading, afm_module, num_format, primary_key, ref_table, review_group, afm_size, sl_heading, string_format, is_tc_traceable, field_grouping, attributes, validate_data, transfer_status)
VALUES ('wf_requests', 'doc2', 2165, 1, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Document', 1, 0, 0, NULL, NULL, 64, NULL, 5, 0, NULL, '<root>
<documentManagement maxDocumentSize="10000000" versioningOn="true" autoNameFile="true">
  <lockingOptions lockOnCheckout="true" clearLocksOnCheckIn="true"/>
 </documentManagement>
</root>', 1, 'NO ACTION')
GO

INSERT INTO afm.afm_flds (table_name, field_name, afm_type, allow_null, comments, data_type, decimals, dep_cols, dflt_val, edit_group, edit_mask, enum_list, is_atxt, max_val, min_val, ml_heading, afm_module, num_format, primary_key, ref_table, review_group, afm_size, sl_heading, string_format, is_tc_traceable, field_grouping, attributes, validate_data, transfer_status)
VALUES ('wf_requests', 'doc3', 2165, 1, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Document', 1, 0, 0, NULL, NULL, 64, NULL, 5, 0, NULL, '<root>
<documentManagement maxDocumentSize="10000000" versioningOn="true" autoNameFile="true">
  <lockingOptions lockOnCheckout="true" clearLocksOnCheckIn="true"/>
 </documentManagement>
</root>', 1, 'NO ACTION')
GO

INSERT INTO afm.afm_flds (table_name, field_name, afm_type, allow_null, comments, data_type, decimals, dep_cols, dflt_val, edit_group, edit_mask, enum_list, is_atxt, max_val, min_val, ml_heading, afm_module, num_format, primary_key, ref_table, review_group, afm_size, sl_heading, string_format, is_tc_traceable, field_grouping, attributes, validate_data, transfer_status)
VALUES ('wf_requests', 'doc4', 2165, 1, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Document', 1, 0, 0, NULL, NULL, 64, NULL, 5, 0, NULL, '<root>
<documentManagement maxDocumentSize="10000000" versioningOn="true" autoNameFile="true">
  <lockingOptions lockOnCheckout="true" clearLocksOnCheckIn="true"/>
 </documentManagement>
</root>', 1, 'NO ACTION')
GO

INSERT INTO afm.afm_flds (table_name, field_name, afm_type, allow_null, comments, data_type, decimals, dep_cols, dflt_val, edit_group, edit_mask, enum_list, is_atxt, max_val, min_val, ml_heading, afm_module, num_format, primary_key, ref_table, review_group, afm_size, sl_heading, string_format, is_tc_traceable, field_grouping, attributes, validate_data, transfer_status)
VALUES ('wf_requests', 'dp_id', 2050, 1, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Department Code', 1, 0, 0, 'dp', NULL, 16, NULL, 10, 0, NULL, NULL, 1, 'NO ACTION')
GO

INSERT INTO afm.afm_flds (table_name, field_name, afm_type, allow_null, comments, data_type, decimals, dep_cols, dflt_val, edit_group, edit_mask, enum_list, is_atxt, max_val, min_val, ml_heading, afm_module, num_format, primary_key, ref_table, review_group, afm_size, sl_heading, string_format, is_tc_traceable, field_grouping, attributes, validate_data, transfer_status)
VALUES ('wf_requests', 'dv_id', 2050, 1, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Division Code', 1, 0, 0, 'dv', NULL, 16, NULL, 10, 0, NULL, NULL, 1, 'NO ACTION')
GO

INSERT INTO afm.afm_flds (table_name, field_name, afm_type, allow_null, comments, data_type, decimals, dep_cols, dflt_val, edit_group, edit_mask, enum_list, is_atxt, max_val, min_val, ml_heading, afm_module, num_format, primary_key, ref_table, review_group, afm_size, sl_heading, string_format, is_tc_traceable, field_grouping, attributes, validate_data, transfer_status)
VALUES ('wf_requests', 'request_id', 2050, 1, NULL, 4, 0, NULL, 'AUTOINCREMENT', NULL, NULL, NULL, 0, NULL, NULL, 'Request ID', 1, 0, 1, NULL, NULL, 6, NULL, 25, 0, NULL, NULL, 1, 'NO ACTION')
GO

INSERT INTO afm.afm_flds (table_name, field_name, afm_type, allow_null, comments, data_type, decimals, dep_cols, dflt_val, edit_group, edit_mask, enum_list, is_atxt, max_val, min_val, ml_heading, afm_module, num_format, primary_key, ref_table, review_group, afm_size, sl_heading, string_format, is_tc_traceable, field_grouping, attributes, validate_data, transfer_status)
VALUES ('wf_requests', 'requestor', 2050, 1, NULL, 1, 0, 'requestor', NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Requested By', 1, 0, 0, 'em', NULL, 35, NULL, 10, 0, NULL, NULL, 1, 'NO ACTION')
GO

INSERT INTO afm.afm_flds (table_name, field_name, afm_type, allow_null, comments, data_type, decimals, dep_cols, dflt_val, edit_group, edit_mask, enum_list, is_atxt, max_val, min_val, ml_heading, afm_module, num_format, primary_key, ref_table, review_group, afm_size, sl_heading, string_format, is_tc_traceable, field_grouping, attributes, validate_data, transfer_status)
VALUES ('wf_requests', 'status', 2050, 1, NULL, 12, 0, NULL, 'N/A', NULL, NULL, 'N/A;N/A;CREATED;CREATED;REQUESTED;REQUESTED;ENQUIRY-CALL;ENQUIRY CALL;APPROVED;APPROVED;REJECTED;REJECTED;BUDGETED;BUDGETED;BUDGETED-A;BUDGET APPROVED;PLANNED;PLANNED;SCHEDULED;SCHEDULED;CANCELLED;CANCELLED;IN PROGRESS;IN PROGRESS;STOPPED;STOPPED;COMPLETED;COMPLETED;COMPLETED-V;COMPLETED AND VERIFIED;CLOSED;CLOSED', 0, NULL, NULL, 'Status', 1, 0, 0, NULL, NULL, 32, NULL, 5, 0, NULL, NULL, 1, 'NO ACTION')
GO

INSERT INTO afm.afm_flds (table_name, field_name, afm_type, allow_null, comments, data_type, decimals, dep_cols, dflt_val, edit_group, edit_mask, enum_list, is_atxt, max_val, min_val, ml_heading, afm_module, num_format, primary_key, ref_table, review_group, afm_size, sl_heading, string_format, is_tc_traceable, field_grouping, attributes, validate_data, transfer_status)
VALUES ('wf_requests', 'time_requested', 2050, 0, NULL, 10, 0, NULL, 'CURRENT', NULL, NULL, NULL, 0, NULL, NULL, 'Time Requested', 1, 0, 0, NULL, NULL, 1, NULL, 5, 0, NULL, NULL, 1, 'NO ACTION')
GO

INSERT INTO afm.afm_flds (table_name, field_name, afm_type, allow_null, comments, data_type, decimals, dep_cols, dflt_val, edit_group, edit_mask, enum_list, is_atxt, max_val, min_val, ml_heading, afm_module, num_format, primary_key, ref_table, review_group, afm_size, sl_heading, string_format, is_tc_traceable, field_grouping, attributes, validate_data, transfer_status)
VALUES ('wf_requests', 'time_required', 2050, 1, NULL, 10, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Time Required', 1, 0, 0, NULL, NULL, 1, NULL, 5, 0, NULL, NULL, 1, 'NO ACTION')
GO

INSERT INTO afm.afm_flds (table_name, field_name, afm_type, allow_null, comments, data_type, decimals, dep_cols, dflt_val, edit_group, edit_mask, enum_list, is_atxt, max_val, min_val, ml_heading, afm_module, num_format, primary_key, ref_table, review_group, afm_size, sl_heading, string_format, is_tc_traceable, field_grouping, attributes, validate_data, transfer_status)
VALUES ('wf_requests', 'wf_type', 2050, 0, NULL, 12, 0, 'wf_type', NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Workflow Type', 1, 0, 0, 'wf', NULL, 64, NULL, 10, 0, NULL, NULL, 1, 'NO ACTION')
GO


CREATE TABLE afm.wf_requests
	(
	status         VARCHAR (32) DEFAULT ('N/A') NULL,
	time_required  DATETIME DEFAULT (NULL) NULL,
	time_requested DATETIME DEFAULT (getdate()) NOT NULL,
	request_id     INT IDENTITY NOT NULL,
	description    VARCHAR (200) DEFAULT (NULL) NULL,
	cost_estimated NUMERIC (12, 2) DEFAULT ((0.0)) NOT NULL,
	cost_final     NUMERIC (12, 2) DEFAULT ((0.0)) NOT NULL,
	comments       VARCHAR (4000) CONSTRAINT DF_wf_requests_comments_default DEFAULT (NULL) NULL,
	dp_id          CHAR (16) DEFAULT (NULL) NULL,
	block_id       VARCHAR (32) DEFAULT (NULL) NOT NULL,
	wf_type        VARCHAR (64) DEFAULT (NULL) NOT NULL,
	date_required  DATETIME DEFAULT (NULL) NULL,
	date_requested DATETIME DEFAULT (getdate()) NOT NULL,
	ac_id          CHAR (32) DEFAULT (NULL) NULL,
	doc1           CHAR (64) DEFAULT (NULL) NULL,
	requestor      CHAR (35) DEFAULT (NULL) NULL,
	doc2           CHAR (64) DEFAULT (NULL) NULL,
	dv_id          CHAR (16) DEFAULT (NULL) NULL,
	doc3           CHAR (64) DEFAULT (NULL) NULL,
	doc4           CHAR (64) DEFAULT (NULL) NULL,
	bl_id          CHAR (8) DEFAULT (NULL) NULL,
	CONSTRAINT wf_requests_PK PRIMARY KEY (request_id),
	CONSTRAINT wf_requests_dp_id FOREIGN KEY (dv_id, dp_id) REFERENCES afm.dp (dv_id, dp_id),
	CONSTRAINT wf_requests_bl_id FOREIGN KEY (bl_id) REFERENCES afm.bl (bl_id),
	CONSTRAINT wf_requests_dv_id FOREIGN KEY (dv_id) REFERENCES afm.dv (dv_id),
	CONSTRAINT wf_requests_ac_id FOREIGN KEY (ac_id) REFERENCES afm.ac (ac_id),
	CONSTRAINT wf_requests_wf_type FOREIGN KEY (wf_type) REFERENCES afm.wf (wf_type),
	CONSTRAINT wf_requests_requestor FOREIGN KEY (requestor) REFERENCES afm.em (em_id)
	)
GO

--roles and tasks

INSERT INTO afm.afm_roleprocs (transfer_status, role_name, activity_id, process_id)
VALUES ('NO CHANGE', 'EXECUTIVE MANAGER (ACP)', 'WorkflowEngine', 'Manage Graphical Workflow')
GO
INSERT INTO afm.afm_roleprocs (transfer_status, role_name, activity_id, process_id)
VALUES ('NO CHANGE', 'OPS SUPERVISOR (ACP)', 'WorkflowEngine', 'Manage Graphical Workflow')
GO
INSERT INTO afm.afm_roleprocs (transfer_status, role_name, activity_id, process_id)
VALUES ('NO CHANGE', 'FIELD TECHNICIAN (ACP)', 'WorkflowEngine', 'Manage Graphical Workflow')
GO

INSERT INTO afm.afm_ptasks (comments, display_order, help_link, hot_user_name, icon_large, icon_small, iframe_height, iframe_width, internal_use1, is_hotlist, security_group, task_01, task_02, task_03, task_action, task_ch, task_de, task_es, task_file, task_fr, task_it, task_jp, task_ko, task_nl, task_no, task_type, task_zh, transfer_status, view_type_override, activity_id, process_id, task_id)
VALUES (NULL, 70, NULL, NULL, NULL, 'ab-icon-task.gif', 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ab-wf-itrepairtravelrequest-create.axvw', NULL, NULL, NULL, NULL, NULL, NULL, 'WEB URL', NULL, 'NO CHANGE', 'N/A', 'WorkflowEngine', 'Manage Graphical Workflow', 'Create Requests')
GO

INSERT INTO afm.afm_ptasks (comments, display_order, help_link, hot_user_name, icon_large, icon_small, iframe_height, iframe_width, internal_use1, is_hotlist, security_group, task_01, task_02, task_03, task_action, task_ch, task_de, task_es, task_file, task_fr, task_it, task_jp, task_ko, task_nl, task_no, task_type, task_zh, transfer_status, view_type_override, activity_id, process_id, task_id)
VALUES (NULL, 80, NULL, NULL, NULL, 'ab-icon-task.gif', 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ab-wf-travelrequest-manage.axvw', NULL, NULL, NULL, NULL, NULL, NULL, 'WEB URL', NULL, 'NO CHANGE', 'N/A', 'WorkflowEngine', 'Manage Graphical Workflow', 'Manage Requests')
GO

INSERT INTO afm.afm_ptasks (comments, display_order, help_link, hot_user_name, icon_large, icon_small, iframe_height, iframe_width, internal_use1, is_hotlist, security_group, task_01, task_02, task_03, task_action, task_ch, task_de, task_es, task_file, task_fr, task_it, task_jp, task_ko, task_nl, task_no, task_type, task_zh, transfer_status, view_type_override, activity_id, process_id, task_id)
VALUES (NULL, 60, NULL, NULL, NULL, 'ab-icon-task.gif', 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LABEL', NULL, 'NO CHANGE', 'N/A', 'WorkflowEngine', 'Manage Graphical Workflow', '-')
GO

INSERT INTO afm.wf (wf_type, activity_id, main_table, security_groups, folder_path, description)
VALUES ('TRAVEL REQUEST', 'WorkflowEngine', 'wf_requests', NULL, 'Travel Request', NULL)
GO


INSERT INTO afm.wf_role (role, table_name, return_field_name, query)
VALUES ('CFO', 'em', 'em_id', 'select em_id from em where em_id=''AFMDEMO5''')
GO

INSERT INTO afm.wf_role (role, table_name, return_field_name, query)
VALUES ('Manager', 'em', 'em_id', 'select em_id from em where em_id=''AFMDEMO5''')
GO

INSERT INTO afm.wf_role (role, table_name, return_field_name, query)
VALUES ('REQUESTOR', 'wf_requests', 'requestor', 'select requestor from wf_requests where request_id=${wf_requests.request_id}')
GO

INSERT INTO afm.wf_role (role, table_name, return_field_name, query)
VALUES ('Superior', 'em', 'em_id', 'select em_id from em where em_id=''AFMDEMO5''')
GO

INSERT INTO afm.wf_role (role, table_name, return_field_name, query)
VALUES ('Supplier 1', 'em', 'em_id', 'select em_id from em where em_id=''PAGE, BILLY''')
GO

INSERT INTO afm.wf_role (role, table_name, return_field_name, query)
VALUES ('Supplier 2', 'em', 'em_id', 'Select em_id from em where em_id=''ALLBURG''')
GO


INSERT INTO afm.messages (message_text, message_text_01, message_text_02, message_text_03, message_text_ch, message_text_de, message_text_es, message_text_fr, message_text_it, message_text_jp, message_text_ko, message_text_nl, message_text_no, message_text_zh, customized, description, is_rich_msg_format, transfer_status, activity_id, referenced_by, message_id)
VALUES ('Status of Request with id ${wf_requests.request_id} is waiting for your action.<br\>
Requestor: ${wf_requests.requestor}<br\>
Request Type: ${wf_requests.wf_type}<br\>
Description: ${wf_requests.description}<br\>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'Notification for request status change', 1, 'NO CHANGE', 'WorkflowEngine', 'GWE_STATUS_CHANGE', 'NOTIFICATION_STEP_BODY')
GO

INSERT INTO afm.messages (message_text, message_text_01, message_text_02, message_text_03, message_text_ch, message_text_de, message_text_es, message_text_fr, message_text_it, message_text_jp, message_text_ko, message_text_nl, message_text_no, message_text_zh, customized, description, is_rich_msg_format, transfer_status, activity_id, referenced_by, message_id)
VALUES ('Status Change for Request ${wf_requests.request_id}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'Notification for request status change', 1, 'NO CHANGE', 'WorkflowEngine', 'GWE_STATUS_CHANGE', 'NOTIFICATION_STEP_TITLE')
GO

INSERT INTO afm.wf_notifications (notification_id, activity_id, notify_subject_refby, notify_subject_id, notify_message_refby, notify_message_id, description)
VALUES ('GWE_STATUS_CHANGE', 'WorkflowEngine', 'GWE_STATUS_CHANGE', 'NOTIFICATION_STEP_TITLE', 'GWE_STATUS_CHANGE', 'NOTIFICATION_STEP_BODY', NULL)
GO


INSERT INTO afm.wf_blocks (block_id, wf_type, obj_type, role, security_groups, custom_action, view_name, init_next_actions, show_log, offset, description, show_diagram)
VALUES ('1', 'TRAVEL REQUEST', 'StartEvent', NULL, NULL, NULL, 'ab-wf-travelrequest-1.axvw', 1, 1, '{"left":130,"top":260}', 'Create Travel Request', 0)
GO

INSERT INTO afm.wf_blocks (block_id, wf_type, obj_type, role, security_groups, custom_action, view_name, init_next_actions, show_log, offset, description, show_diagram)
VALUES ('10', 'TRAVEL REQUEST', 'UserTask', 'CFO', NULL, NULL, 'ab-wf-travelrequest-2.axvw', 0, 1, '{"left":1398,"top":257}', 'CFO Approval', 1)
GO

INSERT INTO afm.wf_blocks (block_id, wf_type, obj_type, role, security_groups, custom_action, view_name, init_next_actions, show_log, offset, description, show_diagram)
VALUES ('11', 'TRAVEL REQUEST', 'ExclusiveGateway', NULL, NULL, NULL, NULL, 0, 1, '{"left":1235,"top":256}', NULL, 0)
GO

INSERT INTO afm.wf_blocks (block_id, wf_type, obj_type, role, security_groups, custom_action, view_name, init_next_actions, show_log, offset, description, show_diagram)
VALUES ('12', 'TRAVEL REQUEST', 'ParallelGateway', NULL, NULL, NULL, NULL, 0, 1, '{"left":1589,"top":419}', NULL, 0)
GO

INSERT INTO afm.wf_blocks (block_id, wf_type, obj_type, role, security_groups, custom_action, view_name, init_next_actions, show_log, offset, description, show_diagram)
VALUES ('13', 'TRAVEL REQUEST', 'UserTask', 'Supplier 1', NULL, NULL, 'ab-wf-travelrequest-supplier1.axvw', 0, 1, '{"left":1796,"top":341}', 'Upload Details for Accomodation', 1)
GO

INSERT INTO afm.wf_blocks (block_id, wf_type, obj_type, role, security_groups, custom_action, view_name, init_next_actions, show_log, offset, description, show_diagram)
VALUES ('14', 'TRAVEL REQUEST', 'UserTask', 'Supplier 2', NULL, NULL, 'ab-wf-travelrequest-supplier2.axvw', 0, 1, '{"left":1800,"top":525}', 'Upload Details for Catering', 1)
GO

INSERT INTO afm.wf_blocks (block_id, wf_type, obj_type, role, security_groups, custom_action, view_name, init_next_actions, show_log, offset, description, show_diagram)
VALUES ('15', 'TRAVEL REQUEST', 'ParallelGateway', NULL, NULL, NULL, NULL, 0, 1, '{"left":1968,"top":439}', NULL, 0)
GO

INSERT INTO afm.wf_blocks (block_id, wf_type, obj_type, role, security_groups, custom_action, view_name, init_next_actions, show_log, offset, description, show_diagram)
VALUES ('16', 'TRAVEL REQUEST', 'UserTask', 'REQUESTOR', NULL, NULL, 'ab-wf-travelrequest-4.axvw', 0, 1, '{"left":2173,"top":438}', 'Close Request', 1)
GO

INSERT INTO afm.wf_blocks (block_id, wf_type, obj_type, role, security_groups, custom_action, view_name, init_next_actions, show_log, offset, description, show_diagram)
VALUES ('17', 'TRAVEL REQUEST', 'EndEvent', 'REQUESTOR', NULL, NULL, 'ab-wf-travelrequest-2.axvw', 0, 1, '{"left":2373,"top":444}', 'Closed', 0)
GO

INSERT INTO afm.wf_blocks (block_id, wf_type, obj_type, role, security_groups, custom_action, view_name, init_next_actions, show_log, offset, description, show_diagram)
VALUES ('18', 'TRAVEL REQUEST', 'EndEvent', NULL, NULL, NULL, NULL, 0, 1, '{"left":1786,"top":147}', 'Rejected', 0)
GO

INSERT INTO afm.wf_blocks (block_id, wf_type, obj_type, role, security_groups, custom_action, view_name, init_next_actions, show_log, offset, description, show_diagram)
VALUES ('19', 'TRAVEL REQUEST', 'ExclusiveGateway', NULL, NULL, NULL, NULL, 0, 1, '{"left":1626,"top":79}', 'Approve Request?', 0)
GO

INSERT INTO afm.wf_blocks (block_id, wf_type, obj_type, role, security_groups, custom_action, view_name, init_next_actions, show_log, offset, description, show_diagram)
VALUES ('2', 'TRAVEL REQUEST', 'UserTask', 'REQUESTOR', NULL, NULL, 'ab-wf-travelrequest-1.axvw', 0, 1, '{"left":332,"top":256}', 'Submit Request', 1)
GO

INSERT INTO afm.wf_blocks (block_id, wf_type, obj_type, role, security_groups, custom_action, view_name, init_next_actions, show_log, offset, description, show_diagram)
VALUES ('20', 'TRAVEL REQUEST', 'ExclusiveGateway', NULL, NULL, NULL, NULL, 0, 1, '{"left":1506,"top":257}', 'Approve Request?', 0)
GO

INSERT INTO afm.wf_blocks (block_id, wf_type, obj_type, role, security_groups, custom_action, view_name, init_next_actions, show_log, offset, description, show_diagram)
VALUES ('3', 'TRAVEL REQUEST', 'ExclusiveGateway', NULL, NULL, NULL, NULL, 0, 1, '{"left":506,"top":256}', 'Submit Request?', 0)
GO

INSERT INTO afm.wf_blocks (block_id, wf_type, obj_type, role, security_groups, custom_action, view_name, init_next_actions, show_log, offset, description, show_diagram)
VALUES ('4', 'TRAVEL REQUEST', 'EndEvent', NULL, NULL, NULL, 'ab-wf-travelrequest-1.axvw', 0, 1, '{"left":507,"top":409}', 'Cancelled', 0)
GO

INSERT INTO afm.wf_blocks (block_id, wf_type, obj_type, role, security_groups, custom_action, view_name, init_next_actions, show_log, offset, description, show_diagram)
VALUES ('5', 'TRAVEL REQUEST', 'UserTask', 'Superior', NULL, NULL, 'ab-wf-travelrequest-2.axvw', 0, 1, '{"left":738,"top":256}', 'Superior Approver', 1)
GO

INSERT INTO afm.wf_blocks (block_id, wf_type, obj_type, role, security_groups, custom_action, view_name, init_next_actions, show_log, offset, description, show_diagram)
VALUES ('6', 'TRAVEL REQUEST', 'ExclusiveGateway', NULL, NULL, NULL, NULL, 0, 1, '{"left":903,"top":256}', 'Approve Request?', 0)
GO

INSERT INTO afm.wf_blocks (block_id, wf_type, obj_type, role, security_groups, custom_action, view_name, init_next_actions, show_log, offset, description, show_diagram)
VALUES ('7', 'TRAVEL REQUEST', 'EndEvent', NULL, NULL, NULL, 'ab-wf-travelrequest-2', 0, 1, '{"left":908,"top":419}', 'Rejected', 0)
GO

INSERT INTO afm.wf_blocks (block_id, wf_type, obj_type, role, security_groups, custom_action, view_name, init_next_actions, show_log, offset, description, show_diagram)
VALUES ('8', 'TRAVEL REQUEST', 'ServiceTask', NULL, NULL, NULL, NULL, 0, 1, '{"left":1108,"top":256}', 'Evaluate Estimated Costs', 0)
GO

INSERT INTO afm.wf_blocks (block_id, wf_type, obj_type, role, security_groups, custom_action, view_name, init_next_actions, show_log, offset, description, show_diagram)
VALUES ('9', 'TRAVEL REQUEST', 'UserTask', 'Manager', NULL, NULL, 'ab-wf-travelrequest-2.axvw', 0, 1, '{"left":1391,"top":79}', 'Department Manager Approval', 1)
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('1', 'TRAVEL REQUEST', '1', '2', NULL, NULL, NULL, '["11,0.5,1,0,0,0,right","2LeftMiddle"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('10', 'TRAVEL REQUEST', '6', '7', NULL, NULL, NULL, '["60.5,1,0,1,0,0,bottom","7TopCenter"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('11', 'TRAVEL REQUEST', '6', '2', NULL, NULL, NULL, '["60.5,0,0,-1,0,0,top","2TopCenter"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('12', 'TRAVEL REQUEST', '20', '18', NULL, NULL, NULL, '["200.5,0,0,-1,0,0,top","18BottomCenter"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('13', 'TRAVEL REQUEST', '5', '6', NULL, NULL, NULL, '["51,0.5,1,0,0,0,right","6LeftMiddle"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('14', 'TRAVEL REQUEST', '9', '19', NULL, NULL, NULL, '["91,0.5,1,0,0,0,right","19LeftMiddle"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('15', 'TRAVEL REQUEST', '19', '12', NULL, NULL, NULL, '["190.5,1,0,1,0,0,bottom","12TopCenter"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('16', 'TRAVEL REQUEST', '10', '20', NULL, NULL, NULL, '["101,0.5,1,0,0,0,right","20LeftMiddle"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('17', 'TRAVEL REQUEST', '19', '18', NULL, NULL, NULL, '["191,0.5,1,0,0,0,right","18TopCenter"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('18', 'TRAVEL REQUEST', '20', '12', NULL, NULL, NULL, '["201,0.5,1,0,0,0,right","12TopCenter"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('19', 'TRAVEL REQUEST', '13', '15', NULL, NULL, NULL, '["131,0.5,1,0,0,0,right","15TopCenter"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('2', 'TRAVEL REQUEST', '12', '14', NULL, NULL, NULL, '["121,0.5,1,0,0,0,right","14LeftMiddle"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('20', 'TRAVEL REQUEST', '6', '8', NULL, NULL, NULL, '["61,0.5,1,0,0,0,right","8LeftMiddle"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('21', 'TRAVEL REQUEST', '8', '11', NULL, NULL, NULL, '["81,0.5,1,0,0,0,right","11LeftMiddle"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('22', 'TRAVEL REQUEST', '11', '10', NULL, NULL, NULL, '["111,0.5,1,0,0,0,right","10LeftMiddle"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('23', 'TRAVEL REQUEST', '12', '13', NULL, NULL, NULL, '["121,0.5,1,0,0,0,right","13LeftMiddle"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('24', 'TRAVEL REQUEST', '11', '12', NULL, NULL, NULL, '["110.5,1,0,1,0,0,bottom","12LeftMiddle"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('3', 'TRAVEL REQUEST', '2', '3', NULL, NULL, NULL, '["21,0.5,1,0,0,0,right","3LeftMiddle"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('4', 'TRAVEL REQUEST', '3', '4', NULL, NULL, NULL, '["30.5,1,0,1,0,0,bottom","4TopCenter"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('5', 'TRAVEL REQUEST', '14', '15', NULL, NULL, NULL, '["141,0.5,1,0,0,0,right","15BottomCenter"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('6', 'TRAVEL REQUEST', '15', '16', NULL, NULL, NULL, '["151,0.5,1,0,0,0,right","16LeftMiddle"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('7', 'TRAVEL REQUEST', '3', '5', NULL, NULL, NULL, '["31,0.5,1,0,0,0,right","5LeftMiddle"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('8', 'TRAVEL REQUEST', '16', '17', NULL, NULL, NULL, '["161,0.5,1,0,0,0,right","17LeftMiddle"]')
GO

INSERT INTO afm.wf_connections (conn_id, wf_type, source_id, target_id, overlays, label_text, anchors, uuids)
VALUES ('9', 'TRAVEL REQUEST', '11', '9', NULL, NULL, NULL, '["110.5,0,0,-1,0,0,top","9LeftMiddle"]')
GO

INSERT INTO afm.wf_connection_action (action_id, conn_id, wf_type, action_name, status, is_main_action, condition_type, validate_fields, eval_srv_task, append_comments, use_confirm_msg, use_comments, condition, confirm_msg)
VALUES ('1', '1', 'TRAVEL REQUEST', 'Save', 'CREATED', 1, '', 0, 0, 0, 0, 0, NULL, NULL)
GO

INSERT INTO afm.wf_connection_action (action_id, conn_id, wf_type, action_name, status, is_main_action, condition_type, validate_fields, eval_srv_task, append_comments, use_confirm_msg, use_comments, condition, confirm_msg)
VALUES ('10', '2', 'TRAVEL REQUEST', 'AUTOMATIC', 'BUDGETED-A', 0, 'expression', 0, 0, 0, 0, 0, '1=1', NULL)
GO

INSERT INTO afm.wf_connection_action (action_id, conn_id, wf_type, action_name, status, is_main_action, condition_type, validate_fields, eval_srv_task, append_comments, use_confirm_msg, use_comments, condition, confirm_msg)
VALUES ('11', '19', 'TRAVEL REQUEST', 'Complete', 'COMPLETED', 0, '', 1, 0, 1, 1, 1, NULL, 'Complete Request?')
GO

INSERT INTO afm.wf_connection_action (action_id, conn_id, wf_type, action_name, status, is_main_action, condition_type, validate_fields, eval_srv_task, append_comments, use_confirm_msg, use_comments, condition, confirm_msg)
VALUES ('12', '5', 'TRAVEL REQUEST', 'Complete', 'COMPLETED', 1, '', 1, 0, 1, 0, 1, '1=1', 'Complete?')
GO

INSERT INTO afm.wf_connection_action (action_id, conn_id, wf_type, action_name, status, is_main_action, condition_type, validate_fields, eval_srv_task, append_comments, use_confirm_msg, use_comments, condition, confirm_msg)
VALUES ('13', '6', 'TRAVEL REQUEST', 'AUTOMATIC', 'COMPLETED-V', 0, 'default', 0, 0, 0, 0, 0, '1=1', NULL)
GO

INSERT INTO afm.wf_connection_action (action_id, conn_id, wf_type, action_name, status, is_main_action, condition_type, validate_fields, eval_srv_task, append_comments, use_confirm_msg, use_comments, condition, confirm_msg)
VALUES ('14', '8', 'TRAVEL REQUEST', 'Close', 'CLOSED', 1, '', 1, 0, 1, 0, 1, '1=1', 'Close Request?')
GO

INSERT INTO afm.wf_connection_action (action_id, conn_id, wf_type, action_name, status, is_main_action, condition_type, validate_fields, eval_srv_task, append_comments, use_confirm_msg, use_comments, condition, confirm_msg)
VALUES ('15', '10', 'TRAVEL REQUEST', 'Reject', 'REJECTED', 0, '', 1, 0, 1, 1, 1, '1=1', 'Reject Request?')
GO

INSERT INTO afm.wf_connection_action (action_id, conn_id, wf_type, action_name, status, is_main_action, condition_type, validate_fields, eval_srv_task, append_comments, use_confirm_msg, use_comments, condition, confirm_msg)
VALUES ('16', '7', 'TRAVEL REQUEST', 'Submit', 'REQUESTED', 1, '', 1, 0, 0, 1, 0, NULL, 'Submit Request?')
GO

INSERT INTO afm.wf_connection_action (action_id, conn_id, wf_type, action_name, status, is_main_action, condition_type, validate_fields, eval_srv_task, append_comments, use_confirm_msg, use_comments, condition, confirm_msg)
VALUES ('17', '20', 'TRAVEL REQUEST', 'Approve', 'APPROVED', 1, '', 1, 0, 0, 1, 0, NULL, 'Approve Request?')
GO

INSERT INTO afm.wf_connection_action (action_id, conn_id, wf_type, action_name, status, is_main_action, condition_type, validate_fields, eval_srv_task, append_comments, use_confirm_msg, use_comments, condition, confirm_msg)
VALUES ('18', '9', 'TRAVEL REQUEST', 'AUTOMATIC', 'PLANNED', 0, 'expression', 0, 0, 0, 0, 0, 'cost_estimated >= 500 and cost_estimated < 1000', NULL)
GO

INSERT INTO afm.wf_connection_action (action_id, conn_id, wf_type, action_name, status, is_main_action, condition_type, validate_fields, eval_srv_task, append_comments, use_confirm_msg, use_comments, condition, confirm_msg)
VALUES ('19', '15', 'TRAVEL REQUEST', 'Approve', 'BUDGETED-A', 1, '', 0, 0, 0, 1, 1, NULL, 'Approve Request?')
GO

INSERT INTO afm.wf_connection_action (action_id, conn_id, wf_type, action_name, status, is_main_action, condition_type, validate_fields, eval_srv_task, append_comments, use_confirm_msg, use_comments, condition, confirm_msg)
VALUES ('2', '4', 'TRAVEL REQUEST', 'Cancel', 'CANCELLED', 0, '', 0, 0, 1, 1, 1, NULL, 'Cancel Request?')
GO

INSERT INTO afm.wf_connection_action (action_id, conn_id, wf_type, action_name, status, is_main_action, condition_type, validate_fields, eval_srv_task, append_comments, use_confirm_msg, use_comments, condition, confirm_msg)
VALUES ('20', '18', 'TRAVEL REQUEST', 'Approve', 'BUDGETED-A', 1, '', 0, 0, 0, 1, 0, NULL, 'Approve Request?')
GO

INSERT INTO afm.wf_connection_action (action_id, conn_id, wf_type, action_name, status, is_main_action, condition_type, validate_fields, eval_srv_task, append_comments, use_confirm_msg, use_comments, condition, confirm_msg)
VALUES ('21', '12', 'TRAVEL REQUEST', 'Reject', 'REJECTED', 0, '', 0, 0, 1, 1, 1, NULL, 'Reject Request?')
GO

INSERT INTO afm.wf_connection_action (action_id, conn_id, wf_type, action_name, status, is_main_action, condition_type, validate_fields, eval_srv_task, append_comments, use_confirm_msg, use_comments, condition, confirm_msg)
VALUES ('22', '17', 'TRAVEL REQUEST', 'Reject', 'REJECTED', 0, '', 0, 0, 1, 1, 1, NULL, 'Reject Request?')
GO

INSERT INTO afm.wf_connection_action (action_id, conn_id, wf_type, action_name, status, is_main_action, condition_type, validate_fields, eval_srv_task, append_comments, use_confirm_msg, use_comments, condition, confirm_msg)
VALUES ('3', '11', 'TRAVEL REQUEST', 'Send Back', 'ENQUIRY-CALL', 0, '', 0, 0, 1, 0, 1, NULL, 'Sned Back the Request?')
GO

INSERT INTO afm.wf_connection_action (action_id, conn_id, wf_type, action_name, status, is_main_action, condition_type, validate_fields, eval_srv_task, append_comments, use_confirm_msg, use_comments, condition, confirm_msg)
VALUES ('5', '22', 'TRAVEL REQUEST', 'AUTOMATIC', 'PLANNED', 0, 'expression', 0, 0, 0, 0, 0, 'cost_estimated >= 1000', NULL)
GO

INSERT INTO afm.wf_connection_action (action_id, conn_id, wf_type, action_name, status, is_main_action, condition_type, validate_fields, eval_srv_task, append_comments, use_confirm_msg, use_comments, condition, confirm_msg)
VALUES ('8', '24', 'TRAVEL REQUEST', 'AUTOMATIC', 'BUDGETED-A', 0, 'default', 0, 0, 0, 0, 0, NULL, NULL)
GO

INSERT INTO afm.wf_connection_action (action_id, conn_id, wf_type, action_name, status, is_main_action, condition_type, validate_fields, eval_srv_task, append_comments, use_confirm_msg, use_comments, condition, confirm_msg)
VALUES ('9', '23', 'TRAVEL REQUEST', 'AUTOMATIC', 'BUDGETED-A', 0, 'expression', 0, 0, 0, 0, 0, '1=1', NULL)
GO

INSERT INTO afm.wf_connection_events (event_id, conn_id, wf_type, event_type, condition, notification_id, role, update_value, update_ref, update_field, rule_id, activity_id)
VALUES ('1', '11', 'TRAVEL REQUEST', 'notification', NULL, 'GWE_STATUS_CHANGE', 'REQUESTOR', NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO afm.wf_connection_events (event_id, conn_id, wf_type, event_type, condition, notification_id, role, update_value, update_ref, update_field, rule_id, activity_id)
VALUES ('1', '2', 'TRAVEL REQUEST', 'notification', NULL, 'GWE_STATUS_CHANGE', 'Supplier 2', NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO afm.wf_connection_events (event_id, conn_id, wf_type, event_type, condition, notification_id, role, update_value, update_ref, update_field, rule_id, activity_id)
VALUES ('1', '22', 'TRAVEL REQUEST', 'notification', NULL, 'GWE_STATUS_CHANGE', 'CFO', NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO afm.wf_connection_events (event_id, conn_id, wf_type, event_type, condition, notification_id, role, update_value, update_ref, update_field, rule_id, activity_id)
VALUES ('1', '23', 'TRAVEL REQUEST', 'notification', NULL, 'GWE_STATUS_CHANGE', 'Supplier 1', NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO afm.wf_connection_events (event_id, conn_id, wf_type, event_type, condition, notification_id, role, update_value, update_ref, update_field, rule_id, activity_id)
VALUES ('1', '6', 'TRAVEL REQUEST', 'notification', NULL, 'GWE_STATUS_CHANGE', 'REQUESTOR', NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO afm.wf_connection_events (event_id, conn_id, wf_type, event_type, condition, notification_id, role, update_value, update_ref, update_field, rule_id, activity_id)
VALUES ('1', '7', 'TRAVEL REQUEST', 'notification', NULL, 'GWE_STATUS_CHANGE', 'Superior', NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO afm.wf_connection_events (event_id, conn_id, wf_type, event_type, condition, notification_id, role, update_value, update_ref, update_field, rule_id, activity_id)
VALUES ('1', '8', 'TRAVEL REQUEST', 'notification', NULL, 'GWE_STATUS_CHANGE', 'Superior', NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO afm.wf_connection_events (event_id, conn_id, wf_type, event_type, condition, notification_id, role, update_value, update_ref, update_field, rule_id, activity_id)
VALUES ('1', '9', 'TRAVEL REQUEST', 'notification', NULL, 'GWE_STATUS_CHANGE', 'Manager', NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO afm.wf_mng_view (wf_types, main_table, activity_id, mng_type, process_id, view_name)
VALUES ('TRAVEL REQUEST', 'wf_requests', 'WorkflowEngine', 'Manage', 'Manage Graphical Workflow', 'ab-wf-travelrequest-manage.axvw')
GO

INSERT INTO afm.wf_mng_view (wf_types, main_table, activity_id, mng_type, process_id, view_name)
VALUES ('TRAVEL REQUEST', 'wf', 'WorkflowEngine', 'Create', 'Manage Graphical Workflow', 'ab-wf-itrepairtravelrequest-create.axvw')
GO
