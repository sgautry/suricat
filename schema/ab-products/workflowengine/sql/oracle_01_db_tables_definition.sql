CREATE TABLE wf (
  wf_type varchar2(64) DEFAULT NULL NOT NULL,
  activity_id varchar2(32) DEFAULT NULL NOT NULL,
  main_table varchar2(32) DEFAULT NULL NOT NULL,
  security_groups varchar2(1000) DEFAULT NULL NULL,
  folder_path varchar2(128) DEFAULT NULL NULL,
  description varchar2(512) DEFAULT NULL NULL
);
ALTER TABLE wf ADD CONSTRAINT wf_PK PRIMARY KEY (wf_type);

CREATE TABLE wf_action_log (
  log_id number(10) NOT NULL,
  wf_type varchar2(64) DEFAULT NULL NOT NULL,
  table_name varchar2(32) DEFAULT NULL NOT NULL,
  field_name varchar2(32) DEFAULT NULL NOT NULL,
  pkey_value varchar2(64) DEFAULT NULL NOT NULL,
  block_id varchar2(32) DEFAULT NULL NOT NULL,
  view_name varchar2(200) DEFAULT NULL NULL,
  status varchar2(64) DEFAULT NULL NULL,
  role varchar2(64) DEFAULT NULL NULL,
  action_state varchar2(12) DEFAULT 'status' NULL,
  action varchar2(64) DEFAULT NULL NULL,
  em_id varchar2(35) DEFAULT NULL NULL,
  user_name varchar2(64) DEFAULT NULL NULL,
  date_created DATE DEFAULT TRUNC(SYSDATE) NOT NULL ,
  time_created DATE DEFAULT SYSDATE NOT NULL ,
  date_response DATE DEFAULT NULL NULL,
  time_response DATE DEFAULT NULL NULL,
  event_type varchar2(16) DEFAULT NULL NULL,
  notification_id varchar2(64) DEFAULT NULL NULL,
  condition varchar2(2000) DEFAULT NULL NULL,
  update_value varchar2(512) DEFAULT NULL NULL,
  rule_id varchar2(64) DEFAULT NULL NULL,
  email_sent number(5) DEFAULT 0 NOT NULL,
  comments varchar2(2000) DEFAULT NULL NULL
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE wf_action_log_seq START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER wf_action_log_seq_tr
 BEFORE INSERT ON wf_action_log FOR EACH ROW
 WHEN (NEW.log_id IS NULL)
BEGIN
 SELECT wf_action_log_seq.NEXTVAL INTO :NEW.log_id FROM DUAL;
END;
/
ALTER TABLE wf_action_log ADD CONSTRAINT wf_action_log_PK PRIMARY KEY (log_id);

CREATE TABLE wf_blocks (
  block_id varchar2(32) DEFAULT NULL NOT NULL,
  wf_type varchar2(64) DEFAULT NULL NOT NULL,
  obj_type char(32) DEFAULT 'start-event' NULL,
  role varchar2(64) DEFAULT NULL NULL,
  security_groups varchar2(1000) DEFAULT NULL NULL,
  custom_action varchar2(64) DEFAULT NULL NULL,
  view_name varchar2(200) DEFAULT NULL NULL,
  init_next_actions number(5) DEFAULT 0 NOT NULL,
  show_log number(5) DEFAULT 1 NULL,
  show_diagram number(5) DEFAULT 0 NOT NULL,
  offset varchar2(64) DEFAULT NULL NULL,
  description varchar2(200) DEFAULT NULL NULL
);
ALTER TABLE wf_blocks ADD CONSTRAINT wf_blocks_PK PRIMARY KEY (block_id, wf_type);

CREATE TABLE wf_blocks_execution (
  exec_id number(10) NOT NULL,
  wf_type varchar2(64) DEFAULT NULL NOT NULL,
  table_name varchar2(32) DEFAULT NULL NOT NULL,
  field_name varchar2(32) DEFAULT NULL NOT NULL,
  pkey_value varchar2(32) DEFAULT NULL NOT NULL,
  block_id varchar2(32) DEFAULT NULL NOT NULL,
  start_exec_id varchar2(32) DEFAULT NULL NOT NULL,
  end_exec_id varchar2(32) DEFAULT NULL NOT NULL,
  exec_type varchar2(12) DEFAULT 'parallel' NOT NULL,
  executed number(5) DEFAULT 0 NOT NULL
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE wf_blocks_execution_seq START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER wf_blocks_execution_seq_tr
 BEFORE INSERT ON wf_blocks_execution FOR EACH ROW
 WHEN (NEW.exec_id IS NULL)
BEGIN
 SELECT wf_blocks_execution_seq.NEXTVAL INTO :NEW.exec_id FROM DUAL;
END;
/
ALTER TABLE wf_blocks_execution ADD CONSTRAINT wf_blocks_execution_PK PRIMARY KEY (exec_id);


CREATE TABLE wf_connection_action (
  action_id varchar2(32) DEFAULT NULL NOT NULL,
  conn_id varchar2(32) DEFAULT NULL NOT NULL,
  wf_type varchar2(64) DEFAULT NULL NOT NULL,
  action_name varchar2(64) DEFAULT NULL NOT NULL,
  status varchar2(16) DEFAULT NULL NOT NULL,
  is_main_action number(5) DEFAULT 0 NOT NULL,
  condition_type varchar2(12) DEFAULT 'default' NOT NULL,
  validate_fields number(5) DEFAULT 1 NULL,
  eval_srv_task number(5) DEFAULT 0 NULL,
  append_comments number(5) DEFAULT 0 NULL,
  use_confirm_msg number(5) DEFAULT 0 NULL,
  use_comments number(5) DEFAULT 0 NULL,
  condition varchar2(2000) DEFAULT NULL NULL,
  confirm_msg varchar2(200) DEFAULT NULL NULL
);
ALTER TABLE wf_connection_action ADD CONSTRAINT wf_connection_action_PK PRIMARY KEY (action_id, conn_id, wf_type);

CREATE TABLE wf_connection_events (
  event_id varchar2(32) DEFAULT NULL NOT NULL,
  conn_id varchar2(32) DEFAULT NULL NOT NULL,
  wf_type varchar2(64) DEFAULT NULL NOT NULL,
  event_type varchar2(16) DEFAULT 'notification' NOT NULL,
  condition varchar2(100) DEFAULT NULL NULL,
  notification_id varchar2(64) DEFAULT NULL NULL,
  role varchar2(64) DEFAULT NULL NULL,
  update_value varchar2(256) DEFAULT NULL NULL,
  update_ref varchar2(256) DEFAULT NULL NULL,
  update_field varchar2(256) DEFAULT NULL NULL,
  rule_id varchar2(64) DEFAULT NULL NULL,
  activity_id varchar2(32) DEFAULT NULL NULL
);
ALTER TABLE wf_connection_events ADD CONSTRAINT wf_connection_events_PK PRIMARY KEY (event_id, conn_id, wf_type);

CREATE TABLE wf_connections (
  conn_id varchar2(32) DEFAULT NULL NOT NULL,
  wf_type varchar2(64) DEFAULT NULL NOT NULL,
  source_id varchar2(32) DEFAULT NULL NOT NULL,
  target_id varchar2(32) DEFAULT NULL NOT NULL,
  overlays varchar2(1000) DEFAULT NULL NULL,
  label_text varchar2(64) DEFAULT NULL NULL,
  anchors varchar2(200) DEFAULT NULL NULL,
  uuids varchar2(200) DEFAULT NULL NULL
);
ALTER TABLE wf_connections ADD CONSTRAINT wf_connections_PK PRIMARY KEY (conn_id, wf_type);

CREATE TABLE wf_mng_view (
  mng_id number(10) NOT NULL,
  wf_types varchar2(2000) DEFAULT NULL NOT NULL,
  main_table varchar2(32) DEFAULT NULL NOT NULL,
  activity_id varchar2(32) DEFAULT NULL NOT NULL,
  mng_type varchar2(6) DEFAULT 'Create' NOT NULL,
  process_id varchar2(32) DEFAULT NULL NULL,
  view_name varchar2(200) DEFAULT NULL NULL
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE wf_mng_view_seq START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER wf_mng_view_seq_tr
 BEFORE INSERT ON wf_mng_view FOR EACH ROW
 WHEN (NEW.mng_id IS NULL)
BEGIN
 SELECT wf_mng_view_seq.NEXTVAL INTO :NEW.mng_id FROM DUAL;
END;
/
ALTER TABLE wf_mng_view ADD CONSTRAINT wf_mng_view_PK PRIMARY KEY (mng_id);


CREATE TABLE wf_notifications (
  notification_id varchar2(64) DEFAULT NULL NOT NULL,
  activity_id varchar2(32) DEFAULT NULL NULL,
  notify_subject_refby varchar2(64) DEFAULT NULL NULL,
  notify_subject_id varchar2(64) DEFAULT NULL NULL,
  notify_message_refby varchar2(64) DEFAULT NULL NULL,
  notify_message_id varchar2(64) DEFAULT NULL NULL,
  description varchar2(200) DEFAULT NULL NULL
);
ALTER TABLE wf_notifications ADD CONSTRAINT wf_notifications_PK PRIMARY KEY (notification_id);


CREATE TABLE wf_role (
  role varchar2(64) DEFAULT NULL NOT NULL,
  table_name varchar2(32) DEFAULT NULL NULL,
  return_field_name varchar2(64) DEFAULT NULL NULL,
  query varchar2(4000) DEFAULT NULL NULL
);
ALTER TABLE wf_role ADD CONSTRAINT wf_role_PK PRIMARY KEY (role);