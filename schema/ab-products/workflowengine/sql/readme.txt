/* ====  Instructions on how to run  Workflow Engine database scripts. ==== */
Please run the scripts in the following order:

1. 01_common_shema_definition.sql
2. 02_common_activity_definition.sql
3. For specific database: <<db_type>>_01_db_tables_definition.sql
4. For specific database: <<db_type>>_02_db_tables_constraints.sql

