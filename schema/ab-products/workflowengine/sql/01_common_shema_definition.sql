-- Create Schema definition

-- // MSSQL SELECT 'Script executed in ' + DB_NAME() + ' on ' + CAST(GETDATE() AS varchar) AS ScriptExecutedin SET XACT_ABORT ON;
-- // ORACLE SELECT 'Script executed in ' || SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA') || ' on ' || TO_CHAR(SYSTIMESTAMP(1)) AS ScriptExecutedin FROM dual SET XACT_ABORT ON;

-- workflow table
INSERT INTO afm_tbls (table_name, title, comments)
  VALUES ('wf', 'Workflow', 'ASC-RO Workflow Engine');
INSERT INTO afm_tbls (table_name, title, comments)
  VALUES ('wf_blocks', 'Workflow Block', 'ASC-RO Workflow Engine');
INSERT INTO afm_tbls (table_name, title, comments)
  VALUES ('wf_action_log', 'Workflow Action Log', 'ASC-RO Workflow Engine');
INSERT INTO afm_tbls (table_name, title, comments)
  VALUES ('wf_blocks_execution', 'Workflow Blocks Execution', 'ASC-RO Workflow Engine');
INSERT INTO afm_tbls (table_name, title, comments)
  VALUES ('wf_connection_action', 'Workflow Connection Action', 'ASC-RO Workflow Engine');
INSERT INTO afm_tbls (table_name, title, comments)
  VALUES ('wf_connection_events', 'Workflow Connection Events', 'ASC-RO Workflow Engine');
INSERT INTO afm_tbls (table_name, title, comments)
  VALUES ('wf_connections', 'Workflow Connections', 'ASC-RO Workflow Engine');
INSERT INTO afm_tbls (table_name, title, comments)
  VALUES ('wf_mng_view', 'Workflow Management Views', 'ASC-RO Workflow Engine');
INSERT INTO afm_tbls (table_name, title, comments)
  VALUES ('wf_notifications', 'Workflow Notifications', 'ASC-RO Workflow Engine');
INSERT INTO afm_tbls (table_name, title, comments)
  VALUES ('wf_role', 'Workflow Role', 'ASC-RO Workflow Engine');

-- workflow fields

INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf', 'activity_id', 'Activity', 0, 1, 32, 'afm_activities', NULL, 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf', 'description', 'Description', 0, 12, 512, NULL, NULL, 1, 40, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf', 'folder_path', 'Folder Path', 0, 12, 128, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf', 'main_table', 'Main Table', 0, 12, 32, 'afm_tbls', 'main_table', 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf', 'security_groups', 'Security Groups', 0, 1, 1000, NULL, NULL, 1, 10, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf', 'wf_type', 'Workflow Type', 1, 12, 64, NULL, NULL, 0, 10, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'action', 'Action', 0, 12, 64, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'action_state', 'Status', 0, 12, 12, NULL, NULL, 1, 5, 'status', 'status;STATUS ACTION;pending;PENDING ACTION;pending-par;PENDING PARALLEL ACTION;event;EVENT ACTION;lost;LOST ACTION;lost-par;LOST PARALLEL ACTION', 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'block_id', 'Block Id', 0, 12, 32, NULL, NULL, 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'view_name', 'View Name', 0, 12, 200, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'comments', 'Comments', 0, 12, 2000, NULL, NULL, 1, 40, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'condition', 'Condition', 0, 12, 2000, NULL, NULL, 1, 40, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'date_created', 'Date Created', 0, 9, 16, NULL, NULL, 0, 5, 'CURRENT', NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'date_response', 'Date Responded', 0, 9, 16, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'em_id', 'Response By Employee Name', 0, 1, 35, 'em', NULL, 1, 10, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'email_sent', 'Email Sent?', 0, 5, 1, NULL, NULL, 0, 25, '0', '0;No;1;Yes', 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'event_type', 'Event Type', 0, 12, 16, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'field_name', 'Primary Key Field', 0, 1, 32, 'afm_flds', NULL, 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'log_id', 'Log Id', 1, 4, 10, NULL, NULL, 0, 25, 'AUTOINCREMENT', NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'notification_id', 'Notification Code', 0, 12, 64, NULL, NULL, 1, 10, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'pkey_value', 'Primary Key Value', 0, 12, 64, NULL, NULL, 0, 25, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'role', 'Role', 0, 12, 64, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'rule_id', 'Rule Id', 0, 12, 64, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'status', 'Status', 0, 12, 64, NULL, NULL, 1, 10, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'table_name', 'Table', 0, 1, 32, 'afm_tbls', NULL, 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'time_created', 'Time Created', 0, 10, 16, NULL, NULL, 0, 5, 'CURRENT', NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'time_response', 'Time Responded To', 0, 10, 16, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'update_value', 'Update Field Value', 0, 12, 512, NULL, NULL, 1, 40, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'user_name', 'Response By User Name', 0, 1, 64, 'afm_users', NULL, 1, 10, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_action_log', 'wf_type', 'Workflow Type', 0, 12, 64, 'wf', 'wf_type', 0, 10, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks', 'block_id', 'Block Id', 1, 12, 32, NULL, NULL, 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks', 'custom_action', 'Custom Action', 0, 12, 64, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks', 'description', 'Description', 0, 12, 200, NULL, NULL, 1, 40, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks', 'init_next_actions', 'Init Next Actions?', 0, 5, 1, NULL, NULL, 0, 25, '0', '0;No;1;Yes', 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks', 'obj_type', 'Type', 0, 1, 32, NULL, NULL, 1, 5, 'start-event', 'start-event;Start Event;intermediate-event;Intermediate Event;end-event;End Event;exclusive-gateway;Exclusive Gateway;parallel-gateway;Parallel Gateway;user-task;User Task;service-task;Service Task', 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks', 'offset', 'Offset', 0, 12, 64, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks', 'role', 'Role', 0, 12, 64, 'wf_role', NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks', 'security_groups', 'Security Groups', 0, 12, 1000, NULL, NULL, 1, 10, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks', 'show_log', 'Show Log?', 0, 5, 1, NULL, NULL, 1, 25, '0', '0;No;1;Yes', 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks', 'show_diagram', 'Show Diagram?', 0, 5, 1, NULL, NULL, 1, 25, '0', '0;No;1;Yes', 'ASC-RO Workflow Engine');  
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks', 'view_name', 'View Name', 0, 12, 200, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks', 'wf_type', 'Workflow Type', 2, 12, 64, 'wf', 'wf_type', 0, 10, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks_execution', 'block_id', 'Block Id', 0, 12, 32, 'wf_blocks', 'block_id;wf_type', 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks_execution', 'end_exec_id', 'End Execution Id', 0, 12, 32, 'wf_blocks', 'end_exec_id;wf_type', 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks_execution', 'exec_id', 'Execution Id', 1, 4, 6, NULL, NULL, 0, 25, 'AUTOINCREMENT', NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks_execution', 'exec_type', 'Execution Type', 0, 12, 12, NULL, NULL, 0, 5, 'parallel', 'parallel;Parallel;inclusive;Inclusive', 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks_execution', 'executed', 'Is Executed', 0, 5, 1, NULL, NULL, 0, 25, '0', '0;No;1;Yes', 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks_execution', 'field_name', 'Primary Key Field', 0, 1, 32, 'afm_flds', NULL, 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks_execution', 'pkey_value', 'Primary Key Value', 0, 12, 32, NULL, NULL, 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks_execution', 'start_exec_id', 'Start Execution Id', 0, 12, 32, 'wf_blocks', 'start_exec_id;wf_type', 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks_execution', 'table_name', 'Table', 0, 1, 32, 'afm_tbls', NULL, 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_blocks_execution', 'wf_type', 'WF Type', 0, 12, 64, 'wf', 'wf_type', 0, 10, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_action', 'action_id', 'Action Id', 1, 12, 32, NULL, NULL, 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_action', 'action_name', 'Action Name', 0, 12, 64, NULL, NULL, 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_action', 'append_comments', 'Append Comments?', 0, 5, 1, NULL, NULL, 1, 25, '0', '0;No;1;Yes', 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_action', 'condition', 'Condition', 0, 12, 2000, NULL, NULL, 1, 40, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_action', 'condition_type', 'Condition Type', 0, 12, 12, NULL, NULL, 0, 5, 'default', 'default;Default Flow;method;Java Method;expression;Evaluate Expression', 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_action', 'confirm_msg', 'Confirm Message', 0, 12, 200, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_action', 'conn_id', 'Connection Id', 2, 12, 32, 'wf_connections', 'conn_id;wf_type', 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_action', 'eval_srv_task', 'Evaluate Service Task?', 0, 5, 1, NULL, NULL, 1, 25, '0', '0;No;1;Yes', 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_action', 'is_main_action', 'Main Action?', 0, 5, 1, NULL, NULL, 0, 25, '0', '0;No;1;Yes', 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_action', 'status', 'Status', 0, 12, 16, NULL, NULL, 0, 10, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_action', 'use_comments', 'Use Comments?', 0, 5, 1, NULL, NULL, 1, 25, '0', '0;No;1;Yes', 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_action', 'use_confirm_msg', 'Use Confirmation Message?', 0, 5, 1, NULL, NULL, 1, 25, '0', '0;No;1;Yes', 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_action', 'validate_fields', 'Validate Fields?', 0, 5, 1, NULL, NULL, 1, 25, '1', '0;No;1;Yes', 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_action', 'wf_type', 'Workflow Type', 3, 12, 64, 'wf', 'wf_type', 0, 10, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_events', 'activity_id', 'Activity Id', 0, 1, 32, 'afm_wf_rules', NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_events', 'condition', 'Condition', 0, 12, 100, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_events', 'conn_id', 'Connection Id', 2, 12, 32, 'wf_connections', 'conn_id;wf_type', 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_events', 'event_id', 'Event Id', 1, 12, 32, NULL, NULL, 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_events', 'event_type', 'Event Type', 0, 12, 16, NULL, NULL, 0, 5, 'notification', 'notification;notification;update;update;runRule;runRule', 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_events', 'notification_id', 'Notification Code', 0, 12, 64, 'wf_notifications', NULL, 1, 10, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_events', 'role', 'Role', 0, 12, 64, 'wf_role', NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_events', 'rule_id', 'Rule Id', 0, 1, 64, 'afm_wf_rules', NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_events', 'update_field', 'Update Field', 0, 12, 256, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_events', 'update_ref', 'Reference Field', 0, 12, 256, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_events', 'update_value', 'Update Value', 0, 12, 256, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connection_events', 'wf_type', 'Workflow Type', 3, 12, 64, 'wf', 'wf_type', 0, 10, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connections', 'anchors', 'Anchors', 0, 12, 200, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connections', 'conn_id', 'Connection Id', 1, 12, 32, NULL, NULL, 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connections', 'label_text', 'Label Text', 0, 12, 64, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connections', 'overlays', 'Overlays', 0, 12, 1000, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connections', 'source_id', 'Source Id', 0, 12, 32, 'wf_blocks', 'source_id;wf_type', 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connections', 'target_id', 'Target Id', 0, 12, 32, 'wf_blocks', 'target_id;wf_type', 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connections', 'uuids', 'UuIds', 0, 12, 200, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_connections', 'wf_type', 'Workflow Type', 2, 12, 64, 'wf', 'wf_type', 0, 10, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_mng_view', 'activity_id', 'Activity', 0, 1, 32, 'afm_activities', 'activity_id', 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_mng_view', 'main_table', 'Main Table', 0, 1, 32, 'afm_tbls', 'main_table', 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_mng_view', 'mng_id', 'Id', 1, 4, 4, NULL, NULL, 0, 25, 'AUTOINCREMENT', NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_mng_view', 'mng_type', 'View Type', 0, 12, 6, NULL, NULL, 0, 5, 'Create', 'Create;Create;Manage;Manage', 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_mng_view', 'process_id', 'Process or Role', 0, 12, 32, 'afm_processes', 'process_id', 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_mng_view', 'view_name', 'View Name', 0, 12, 200, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_mng_view', 'wf_types', 'Workflow Types', 0, 12, 2000, NULL, NULL, 0, 40, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_notifications', 'activity_id', 'Application ID', 0, 1, 32, 'afm_activities', NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_notifications', 'description', 'Description', 0, 12, 200, NULL, NULL, 1, 40, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_notifications', 'notification_id', 'Notification Code', 1, 12, 64, NULL, NULL, 0, 10, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_notifications', 'notify_message_id', 'Notification Body ID', 0, 1, 64, 'messages', 'activity_id;notify_message_refby;notify_message_id', 1, 10, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_notifications', 'notify_message_refby', 'Notification Message Body Reference', 0, 1, 64, NULL, NULL, 1, 10, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_notifications', 'notify_subject_id', 'Notification Subject ID', 0, 1, 64, 'messages', 'activity_id;notify_subject_refby;notify_subject_id', 1, 10, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_notifications', 'notify_subject_refby', 'Notification Message Reference', 0, 1, 64, NULL, NULL, 1, 10, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_role', 'query', 'SQL Query', 0, 12, 4000, NULL, NULL, 1, 40, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_role', 'return_field_name', 'Return Field Name', 0, 12, 64, NULL, NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_role', 'role', 'Role', 1, 12, 64, NULL, NULL, 0, 5, NULL, NULL, 'ASC-RO Workflow Engine');
INSERT INTO afm_flds (table_name, field_name, ml_heading, primary_key, data_type, afm_size, ref_table, dep_cols, allow_null, string_format, dflt_val, enum_list, comments)
  VALUES ('wf_role', 'table_name', 'Table Name', 0, 1, 32, 'afm_tbls', NULL, 1, 5, NULL, NULL, 'ASC-RO Workflow Engine');