SELECT
  'Script executed in ' + DB_NAME() + ' on ' + CAST(GETDATE() AS varchar) AS ScriptExecutedin
SET XACT_ABORT ON;
GO
CREATE TABLE wf (
  wf_type varchar(64) NOT NULL DEFAULT NULL,
  activity_id char(32) NOT NULL DEFAULT NULL,
  main_table char(32) NOT NULL DEFAULT NULL,
  security_groups varchar(1000) NULL DEFAULT NULL,
  folder_path varchar(128) NULL DEFAULT NULL,
  description varchar(512) NULL DEFAULT NULL
);
GO
ALTER TABLE wf ADD CONSTRAINT wf_PK PRIMARY KEY (wf_type);
GO
--***RUN UNDER SECURITY ROLE***
GRANT ALL ON afm.wf TO afm;
GO
CREATE TABLE wf_action_log (
  log_id integer IDENTITY NOT NULL,
  wf_type varchar(64) NOT NULL DEFAULT NULL,
  table_name char(32) NOT NULL DEFAULT NULL,
  field_name char(32) NOT NULL DEFAULT NULL,
  pkey_value varchar(64) NOT NULL DEFAULT NULL,
  block_id varchar(32) NOT NULL DEFAULT NULL,
  view_name varchar(200) NULL DEFAULT NULL,
  status varchar(64) NULL DEFAULT NULL,
  role varchar(64) NULL DEFAULT NULL,
  action_state varchar(12) NULL DEFAULT 'status',
  action varchar(64) NULL DEFAULT NULL,
  em_id char(35) NULL DEFAULT NULL,
  user_name char(64) NULL DEFAULT NULL,
  date_created datetime NOT NULL DEFAULT GETDATE(),
  time_created datetime NOT NULL DEFAULT GETDATE(),
  date_response datetime NULL DEFAULT NULL,
  time_response datetime NULL DEFAULT NULL,
  event_type varchar(16) NULL DEFAULT NULL,
  notification_id varchar(64) NULL DEFAULT NULL,
  condition varchar(2000) NULL DEFAULT NULL,
  update_value varchar(512) NULL DEFAULT NULL,
  rule_id varchar(64) NULL DEFAULT NULL,
  email_sent smallint NOT NULL DEFAULT 0,
  comments varchar(2000) NULL DEFAULT NULL
);
GO
ALTER TABLE wf_action_log ADD CONSTRAINT wf_action_log_PK PRIMARY KEY (log_id);
GO
--***RUN UNDER SECURITY ROLE***
GRANT ALL ON afm.wf_action_log TO afm;
GO
CREATE TABLE wf_blocks (
  block_id varchar(32) NOT NULL DEFAULT NULL,
  wf_type varchar(64) NOT NULL DEFAULT NULL,
  obj_type char(32) NULL DEFAULT 'start-event',
  role varchar(64) NULL DEFAULT NULL,
  security_groups varchar(1000) NULL DEFAULT NULL,
  custom_action varchar(64) NULL DEFAULT NULL,
  view_name varchar(200) NULL DEFAULT NULL,
  init_next_actions smallint NOT NULL DEFAULT 0,
  show_log smallint NULL DEFAULT 1,
  show_diagram smallint NOT NULL DEFAULT 0,
  offset varchar(64) NULL DEFAULT NULL,
  description varchar(200) NULL DEFAULT NULL
);
GO
ALTER TABLE wf_blocks ADD CONSTRAINT wf_blocks_PK PRIMARY KEY (block_id, wf_type);
GO
--***RUN UNDER SECURITY ROLE***
GRANT ALL ON afm.wf_blocks TO afm;
GO
CREATE TABLE wf_blocks_execution (
  exec_id integer IDENTITY NOT NULL,
  wf_type varchar(64) NOT NULL DEFAULT NULL,
  table_name char(32) NOT NULL DEFAULT NULL,
  field_name char(32) NOT NULL DEFAULT NULL,
  pkey_value varchar(32) NOT NULL DEFAULT NULL,
  block_id varchar(32) NOT NULL DEFAULT NULL,
  start_exec_id varchar(32) NOT NULL DEFAULT NULL,
  end_exec_id varchar(32) NOT NULL DEFAULT NULL,
  exec_type varchar(12) NOT NULL DEFAULT 'parallel',
  executed smallint NOT NULL DEFAULT 0
);
GO
ALTER TABLE wf_blocks_execution ADD CONSTRAINT wf_blocks_execution_PK PRIMARY KEY (exec_id);
GO
--***RUN UNDER SECURITY ROLE***
GRANT ALL ON afm.wf_blocks_execution TO afm;
GO
CREATE TABLE wf_connection_action (
  action_id varchar(32) NOT NULL DEFAULT NULL,
  conn_id varchar(32) NOT NULL DEFAULT NULL,
  wf_type varchar(64) NOT NULL DEFAULT NULL,
  action_name varchar(64) NOT NULL DEFAULT NULL,
  status varchar(16) NOT NULL DEFAULT NULL,
  is_main_action smallint NOT NULL DEFAULT 0,
  condition_type varchar(12) NOT NULL DEFAULT 'default',
  validate_fields smallint NULL DEFAULT 1,
  eval_srv_task smallint NULL DEFAULT 0,
  append_comments smallint NULL DEFAULT 0,
  use_confirm_msg smallint NULL DEFAULT 0,
  use_comments smallint NULL DEFAULT 0,
  condition varchar(2000) NULL DEFAULT NULL,
  confirm_msg varchar(200) NULL DEFAULT NULL
);
GO
ALTER TABLE wf_connection_action ADD CONSTRAINT wf_connection_action_PK PRIMARY KEY (action_id, conn_id, wf_type);
GO
--***RUN UNDER SECURITY ROLE***
GRANT ALL ON afm.wf_connection_action TO afm;
GO
CREATE TABLE wf_connection_events (
  event_id varchar(32) NOT NULL DEFAULT NULL,
  conn_id varchar(32) NOT NULL DEFAULT NULL,
  wf_type varchar(64) NOT NULL DEFAULT NULL,
  event_type varchar(16) NOT NULL DEFAULT 'notification',
  condition varchar(100) NULL DEFAULT NULL,
  notification_id varchar(64) NULL DEFAULT NULL,
  role varchar(64) NULL DEFAULT NULL,
  update_value varchar(256) NULL DEFAULT NULL,
  update_ref varchar(256) NULL DEFAULT NULL,
  update_field varchar(256) NULL DEFAULT NULL,
  rule_id char(64) NULL DEFAULT NULL,
  activity_id char(32) NULL DEFAULT NULL
);
GO
ALTER TABLE wf_connection_events ADD CONSTRAINT wf_connection_events_PK PRIMARY KEY (event_id, conn_id, wf_type);
GO
--***RUN UNDER SECURITY ROLE***
GRANT ALL ON afm.wf_connection_events TO afm;
GO
CREATE TABLE wf_connections (
  conn_id varchar(32) NOT NULL DEFAULT NULL,
  wf_type varchar(64) NOT NULL DEFAULT NULL,
  source_id varchar(32) NOT NULL DEFAULT NULL,
  target_id varchar(32) NOT NULL DEFAULT NULL,
  overlays varchar(1000) NULL DEFAULT NULL,
  label_text varchar(64) NULL DEFAULT NULL,
  anchors varchar(200) NULL DEFAULT NULL,
  uuids varchar(200) NULL DEFAULT NULL
);
GO
ALTER TABLE wf_connections ADD CONSTRAINT wf_connections_PK PRIMARY KEY (conn_id, wf_type);
GO
--***RUN UNDER SECURITY ROLE***
GRANT ALL ON afm.wf_connections TO afm;
GO
CREATE TABLE wf_mng_view (
  mng_id integer IDENTITY NOT NULL,
  wf_types varchar(2000) NOT NULL DEFAULT NULL,
  main_table char(32) NOT NULL DEFAULT NULL,
  activity_id char(32) NOT NULL DEFAULT NULL,
  mng_type varchar(6) NOT NULL DEFAULT 'Create',
  process_id varchar(32) NULL DEFAULT NULL,
  view_name varchar(200) NULL DEFAULT NULL
);
GO
ALTER TABLE wf_mng_view ADD CONSTRAINT wf_mng_view_PK PRIMARY KEY (mng_id);
GO
--***RUN UNDER SECURITY ROLE***
GRANT ALL ON afm.wf_mng_view TO afm;
GO
CREATE TABLE wf_notifications (
  notification_id varchar(64) NOT NULL DEFAULT NULL,
  activity_id char(32) NULL DEFAULT NULL,
  notify_subject_refby char(64) NULL DEFAULT NULL,
  notify_subject_id char(64) NULL DEFAULT NULL,
  notify_message_refby char(64) NULL DEFAULT NULL,
  notify_message_id char(64) NULL DEFAULT NULL,
  description varchar(200) NULL DEFAULT NULL
);
GO
ALTER TABLE wf_notifications ADD CONSTRAINT wf_notifications_PK PRIMARY KEY (notification_id);
GO
--***RUN UNDER SECURITY ROLE***
GRANT ALL ON afm.wf_notifications TO afm;
GO
CREATE TABLE wf_role (
  role varchar(64) NOT NULL DEFAULT NULL,
  table_name char(32) NULL DEFAULT NULL,
  return_field_name varchar(64) NULL DEFAULT NULL,
  query varchar(4000) NULL DEFAULT NULL
);
GO
ALTER TABLE wf_role ADD CONSTRAINT wf_role_PK PRIMARY KEY (role);
GO
--***RUN UNDER SECURITY ROLE***
GRANT ALL ON afm.wf_role TO afm;
GO