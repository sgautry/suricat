/**
 * @class Ab.wf.Action
 * @constructor
 */
Ab.namespace('wf');
var actionController = null;
Ab.wf.Action = Base.extend({
    actionController: null,
    block: null, //this contains all the info, connections, action and events
    beforeActionListener: null,
    afterActionListener: null,
    customActionListener: null,
    requiredDocFields: [],
    wfRule: 'WorkflowEngine-WorkflowEngineService-',
    dot: '.',
    constructor: function (panel, params) {
        actionController = this;
        this.panel = panel;
        this.wfType = params.wfType;
        this.objType = params.objType;
        this.mainTable = params.tableName;
        this.blockId = params.blockId;
        this.wfBlockId = params.wfBlockId; // used for diagram
        this.pkValue = params.pkValue || 0;
        this.edit = params.edit;
        this.preview = params.preview || false;
        this.init();
    },
    onBeforeActionListener: function (beforeActionListener) {
        this.beforeActionListener = beforeActionListener;
    },
    onAfterActionListener: function (afterActionListener) {
        this.afterActionListener = afterActionListener;
    },
    onCustomActionListener: function (customActionListener) {
        this.customActionListener = customActionListener;
    },
    init: function () {
        this.pkFieldName = getValue('afm_flds', 'afm_flds.field_name', "(afm_flds.table_name='" + this.mainTable + "' AND afm_flds.primary_key=1)");
        this.block = this.initBlock();
        if (this.block) {
            this.initActions();
            this.initFields();
        }
    },
    initBlock: function () {
        var result = {};
        try {
            result = Workflow.callMethod(this.wfRule + 'getBlockData', this.blockId, this.wfType);
            if (result.code == 'executed') {
                return result.data;
            }
        } catch (e) {
            Workflow.handleError(e);
            return null;
        }
    },
    initFields: function () {
        this.panel.setFieldValue(this.mainTable + '.wf_type', this.wfType);
    },
    initActions: function () {
        if (this.edit) {
            this.addPanelActions(this.block, 'true', this.block.initNextActions);
            if (this.block.initNextActions) {
                this.addPanelActions(this.block.nextBlock, 'false', false);
            }
        }
        if (valueExistsNotEmpty(this.block.customAction)) {
            this.addCustomAction(this.block, 'true');
        }
        if (!this.preview) {
            if (this.block.showLog) {
                var enabled = (valueExists(this.pkValue) && this.pkValue > 0) ? 'true' : 'false';
                this.addLogAction(this.block, enabled);
            }
            if (this.block.showDiagram) {
                this.addDiagramAction(this.block, 'true');
            }
        }
    },
    addPanelActions: function (block, enabled, enableNextActions) {
        if (valueExists(block)) {
            var connections = block.connections;
            for ( var i = 0; i < connections.length; i++ ) {
                var action = connections[i].action;
                if (!_.isEmpty(action)) {
                    action.enableNextActions = enableNextActions || false;
                    this.createAction(connections[i], enabled);
                }
            }
            if (valueExistsNotEmpty(block.customAction)) {
                this.addCustomAction(block, enabled);
            }
            if (block.showDiagram) {
                this.addDiagramAction(block, enabled);
            }
        }
    },
    addCustomAction: function (block, enabled) {
        var cfgAction = {
            enabled: enabled,
            hidden: 'false',
            id: 'action_custom',
            mainAction: false,
            listener: this.onActionCustom.createDelegate(this),
            text: getMessage(block.customAction)
        };
        this.addAction(cfgAction);
    },
    addLogAction: function (block, enabled) {
        var cfgAction = {
            enabled: enabled,
            hidden: 'false',
            id: 'action_log',
            mainAction: false,
            listener: this.onActionLog.createDelegate(this),
            text: Ab.view.View.getLocalizedString(Ab.wf.Action.z_MESSAGE_ACTION_LOG)
        };
        this.addAction(cfgAction);
    },
    addDiagramAction: function (block, enabled) {
        var cfgAction = {
            enabled: enabled,
            hidden: 'false',
            id: 'action_diagram',
            mainAction: false,
            listener: this.onShowDiagram.createDelegate(this),
            text: Ab.view.View.getLocalizedString(Ab.wf.Action.z_MESSAGE_DIAGRAM)
        };
        this.addAction(cfgAction);
    },
    createAction: function (connection, enabled) {
        var cfgAction = {
            enabled: enabled,
            hidden: 'false',
            id: connection.action.name,
            mainAction: connection.action.mainAction || false,
            text: getMessage(connection.action.name)
        };
        if (!this.preview) {
            _.extend(cfgAction, {
                listener: this.onAction.createDelegate(this, [connection])
            });
        }
        this.addAction(cfgAction);
    },
    addAction: function (config) {
        if (config == null) {
            return;
        }
        if (this.panel.actions.get(config.id) != undefined) {
            this.panel.actions.get(config.id).setTitle(config.text);
        } else {
            this.panel.actions.add(config.id, new Ab.view.Action(this.panel, config));
        }
    },
    onAction: function (connection) {
        var action = connection.action;
        if (this.pkValue == 0) { // only for create request
            this.panel.setFieldValue(this.mainTable + '.status', action.status);
            this.panel.setFieldValue(this.mainTable + '.block_id', action.nextBlockId);
            action.validateFields = true;
            if (action.enableNextActions) {
                // do not validate document required fields when request is first created
                View.panels.each(function (panel) {
                    if ('form' === panel.type) {
                        panel.fields.each(function (field) {
                            if (field.fieldDef.isDocument && field.fieldDef.required) {
                                field.fieldDef.required = false;
                                this.requiredDocFields.push(field);
                            }
                        }, this);
                    }
                }, this);
            }
        }
        this.panel.setFieldValue(this.mainTable + '.wf_type', this.wfType);
        var beforeActionListener = this.beforeActionListener;
        var continueFlow = {continueFlow: true};
        if (beforeActionListener) {
            action.connId = connection.connId;
            continueFlow = beforeActionListener(action);
        }
        if (continueFlow.continueFlow) {
            if (action.validateFields) {
                continueFlow.continueFlow = this.validateFields();
            }
            if (continueFlow.continueFlow) {
                if (action.useConfirmMsg && valueExists(action.confirmMsg)) {
                    var actionControl = this;
                    View.confirm(getMessage(action.confirmMsg), function (button) {
                        if (button == 'yes') {
                            actionControl._onAction(connection.connId, action);
                        }
                    });
                } else {
                    this._onAction(connection.connId, action);
                }
            } else {
                return;
            }
        } else {
            var message = continueFlow.message;
            if (valueExistsNotEmpty(message)) {
                View.showMessage(message);
                return;
            }
        }
    },
    validateFields: function () {
        var canSave = true;
        View.panels.each(function (panel) {
            if ('form' === panel.type && !panel.canSave()) {
                canSave = false;
            }
        });
        return canSave;
    },
    _onAction: function (connId, action) {
        if (action.useComments) {
            this.addComments(action, function (comments) {
                actionController.doAction(connId, action, comments);
            });
            return;
        } else {
            this.doAction(connId, action);
        }
    },
    addComments: function (action, callbackToAction) {
        View.openDialog('ab-wf-action-comments.axvw', null, false, {
            width: 600,
            height: 260,
            closeButton: false,
            maximize: false,
            param: {
                action: getMessage(action.name)
            },
            callback: function (comments) {
                View.closeDialog();
                return callbackToAction(comments);
            }
        });
    },
    doAction: function (connId, action, comments) {
        this.panel.newRecord = (this.pkValue == 0);
        var record = this.getPanelsRecord();
        record = this.removeVirtualFields(record);
        if (action.appendComments) {
            record.values[this.mainTable + '.comments'] = '';
            record.oldValues[this.mainTable + '.comments'] = '';
        }
        var savedRecordResult = this.saveAction(connId, action, record, comments);
        if (!_.isEmpty(savedRecordResult)) {
            this.afterAction(savedRecordResult, action);
        }
    },
    getPanelsRecord: function () {
        var record = this.panel.getOutboundRecord();
        var values = {}, oldValues = {};
        var valuesAppended = false;
        // multiple form panels
        View.panels.each(function (panel) {
            if ('form' === panel.type && panel.id !== this.panel.id) {
                var panelRecord = panel.getOutboundRecord();
                _.extend(values, panelRecord.values, record.values);
                _.extend(oldValues, panelRecord.oldValues, record.oldValues);
				valuesAppended = true;
            }
        }, this);
        // single form panel
        if (!valuesAppended) {
            values = record.values;
            oldValues = record.oldValues;
        }
        record.values = values;
        record.oldValues = oldValues;
        return record;
    },
    saveAction: function (connId, action, record, comments) {
        View.openProgressBar(Ab.view.View.getLocalizedString(Ab.wf.Action.z_MESSAGE_PROGRESS));
        var result = {};
        try {
            comments = comments || '';
            result = Workflow.callMethod(this.wfRule + 'saveAction', this.blockId, this.wfType, connId, action.actionId, record, comments);
        } catch (e) {
            View.closeProgressBar();
            Workflow.handleError(e);
            return;
        }
        return result;
    },
    afterAction: function (savedRecordResult, action) {
        //var returnedValues =  eval('(' + resultAction.jsonExpression + ')');
        if (savedRecordResult.code = "executed") {
            var savedRecord = savedRecordResult.dataSet;
            this.panel.setRecord(savedRecord);
            this.pkValue = savedRecord.getValue(this.mainTable + '.' + this.pkFieldName);
            this.blockId = savedRecord.getValue(this.mainTable + '.block_id');
            this.wfBlockId = this.blockId;
            var continueFlow = true;
            var afterActionListener = this.afterActionListener;
            if (afterActionListener) {
                continueFlow = afterActionListener(action, savedRecord);
            }
            if (continueFlow) {
                View.panels.each(function (panel) {
                    if (this.acceptedPanelType(panel) && this.panel.id !== panel.id) {
                        var panelRestriction = new Ab.view.Restriction();
                        panelRestriction.addClause(this.mainTable + this.dot + this.pkFieldName, this.pkValue);
                        panel.newRecord = false;
                        panel.refresh(panelRestriction);
                    }
                }, this);
                if (action.enableNextActions) {
                    var actions = this.panel.actions.keys;
                    this.enableActions(this.panel, actions, true);
                    View.closeProgressBar();
                    View.showMessage(savedRecordResult.message);
                    // turn required fields on
                    var resetRequiredDocFields = false;
                    _.each(this.requiredDocFields, function (field) {
                        field.fieldDef.required = true;
                        resetRequiredDocFields = true;
                    });
                    if (resetRequiredDocFields) {
                        this.requiredDocFields = [];
                    }
                } else {
                    //disable fields and actions
                    if (typeof(disableViewCustomActionsAndButtons) == 'function') {
                        disableViewCustomActionsAndButtons();
                    } else {
                        disableViewActionsAndButtons();
                    }
                    View.closeProgressBar();
                    View.showMessage(savedRecordResult.message);
                    wfActionsControl.wfParams.edit = false;
                }
            } else {
                View.closeProgressBar();
            }
        } else {
            View.closeProgressBar();
            View.showMessage(savedRecordResult.message);
            return;
        }
    },
    onActionLog: function () {
        if (this.pkValue > 0) {
            var restriction = new Ab.view.Restriction();
            restriction.addClause("wf_action_log.wf_type", this.wfType, "=");
            restriction.addClause("wf_action_log.table_name", this.mainTable, "=");
            restriction.addClause("wf_action_log.field_name", this.pkFieldName, "=");
            restriction.addClause("wf_action_log.pkey_value", this.pkValue, "=");
            var viewName = "ab-wf-action-log.axvw";
            if (typeof(getViewActionLog) == 'function') {
                viewName = getViewActionLog();
            }
            View.openDialog(viewName, restriction, false, {});
        }
    },
    onShowDiagram: function () {
        if (valueExists(this.wfBlockId)) {
            View.openDialog("/archibus/schema/ab-products/workflowengine/views/define/workflow/designer/ab-wf-flowchart-diagram.axvw", null, false, {
                wfType: this.wfType,
                mainTable: this.mainTable,
                blockId: this.wfBlockId
            });
        } else {
            View.showMessage(Ab.view.View.getLocalizedString(Ab.wf.Action.z_MESSAGE_NO_DIAGRAM));
            return;
        }
    },
    onActionCustom: function () {
        var customActionListener = this.customActionListener;
        if (customActionListener) {
            customActionListener(this.pkValue);
        }
    },
    enableFields: function (panel, fieldsArray, enable) {
        if (panel.type == 'form') {
            for ( var index = 0; index < fieldsArray.length; index++ ) {
                var fieldName = fieldsArray[index];
                try {
                    if (panel.fields.get(fieldName).fieldDef.isDocument) {
                        this.readOnlyFieldDocument(panel, fieldName, !enable);
                    } else {
                        panel.enableField(fieldName, enable);
                        var field = panel.fields.get(fieldName);
                        if (field != null) {
                            field.actions.each(function (action) {
                                action.show(enable);
                            });
                        }
                    }
                } catch (e) {
                }
            }
        }
    },
    showFields: function (panel, fieldsArray, show) {
        if (panel.type == 'form') {
            for ( var index = 0; index < fieldsArray.length; index++ ) {
                var fieldName = fieldsArray[index];
                try {
                    panel.showField(fieldName, show);
                } catch (e) {
                }
            }
        }
    },
    readOnlyFieldDocument: function (panel, fieldName, readOnly) {
        var field = panel.fields.get(fieldName);
        if (field != null) {
            field.fieldDef.readOnly = readOnly;
            panel.updateDocumentButtons();
        }
    },
    showActions: function (panel, actionsArray, show) {
        for ( var index = 0; index < actionsArray.length; index++ ) {
            var action = actionsArray[index];
            if (action === "action_log" || action === "action_custom" || action === 'action_diagram') {
                panel.actions.get(action).show(true); //always show
            } else {
                panel.actions.get(action).show(show);
            }
        }
    },
    enableActions: function (panel, actionsArray, enable) {
        for ( var index = 0; index < actionsArray.length; index++ ) {
            var action = actionsArray[index];
            panel.actions.get(action).enableButton(enable);
        }
    },
    acceptedPanelType: function (panel) {
        return ('form' === panel.type || 'grid' === panel.type) && valueExists(panel.config) && !valueExists(panel.window);
    },
    removeVirtualFields: function (record) {
        var result = {};
        try {
            result = Workflow.callMethod(this.wfRule + 'removeVirtualFields', record);
        } catch (e) {
            if (e.code == 'ruleFailed') {
                View.showMessage(e.message);
            } else {
                Workflow.handleError(e);
            }
            return;
        }
        if (result.code == 'executed') {
            return result.data;
        }
    }
}, {
    // @begin_translatable
    z_MESSAGE_ACTION_LOG: "Action Log",
    z_MESSAGE_DIAGRAM: "Diagram",
    z_MESSAGE_NO_DIAGRAM: "Cannot process diagram",
    z_MESSAGE_PROGRESS: "Processing request...",
    // @end_translatable
    disableViewActionsAndButtons: function () {
        var controller = this;
        View.panels.each(function (panel) {
            if ('form' === panel.type) {
                var fields = panel.fields.keys,
                    actions = panel.actions.keys;
                controller.prototype.enableFields(panel, fields, false);
                controller.prototype.showActions(panel, actions, false);
                if (valueExists(panel.fieldsets)) {
                    panel.fieldsets.each(function (fieldSet) {
                        var actions = fieldSet.actions.keys;
                        controller.prototype.showActions(fieldSet, actions, false);
                    });
                }
            } else if ('grid' === panel.type) {
                var actions = panel.actions.keys;
                controller.prototype.showActions(panel, actions, false);
            }
        });
    }
});
var wfActionsControl = View.createController('wfActionsControl', {
    wfParams: {},
    preview: false,
    afterViewLoad: function () {
        var parentView = View.getOpenerView();
        if (parentView && parentView.wfParams) {// select from list
            this.wfParams = parentView.wfParams;
        } else if (View.parameters && View.parameters.wfParams) {
            this.wfParams = View.parameters.wfParams;
        } else if (View.parameters && typeof (View.parameters.getParameters == "function")) {//called from View.openDialog
            this.wfParams = View.parameters.getParameters();
        } else {
            this.wfParams = getParameters();//called on direct link to create/open request
        }
        var actionPanel = View.panels.get('requestPanel');
        if (!valueExists(actionPanel)) {
            for ( var i = 0; i < View.panels.length; i++ ) {
                if ("form" === View.panels.get(i).type) {
                    actionPanel = View.panels.get(i);
                    break;
                }
            }
        }
        this.preview = this.wfParams.preview || false;
        var wfAction = new Ab.wf.Action(actionPanel, this.wfParams);
        if (!this.preview) {
            var newRecord = (wfAction.pkValue == 0);
            View.panels.each(function (panel) {
                if (wfAction.acceptedPanelType(panel)) {
                    var panelRestriction = new Ab.view.Restriction(),
                        ds = View.dataSources.get(panel.dataSourceId),
                        mainTableName = ds.mainTableName,
                        primaryKeyFields = ds.config.primaryKeyFields;
                    for ( var i = 0; i < primaryKeyFields.length; i++ ) {
                        var fieldName = primaryKeyFields[i].split(mainTableName + wfAction.dot)[1];
                        if (fieldName === wfAction.pkFieldName) {
                            panelRestriction.addClause(mainTableName + wfAction.dot + fieldName, wfAction.pkValue);
                            break;
                        }
                    }
                    panel.restriction = panelRestriction;
                    if ('form' === panel.type && 'StartEvent' === wfAction.objType) {
                        panel.newRecord = newRecord;
                    }
                }
            });
            if (this.wfParams.edit) {
                if (typeof(beforeAction) == 'function') {
                    wfAction.onBeforeActionListener(beforeAction);
                }
                if (typeof(afterAction) == 'function') {
                    wfAction.onAfterActionListener(afterAction);
                }
                if (typeof(customAction) == 'function') {
                    wfAction.onCustomActionListener(customAction);
                }
            }
        } else {
            View.panels.each(function (panel) {
                if (wfAction.acceptedPanelType(panel)) {
                    panel.restriction = "1=0";
                }
            });
        }
    },
    afterInitialDataFetch: function () {
        var edit = false;
        if (!edit) {
            if (typeof(disableViewCustomActionsAndButtons) == 'function') {
                disableViewCustomActionsAndButtons();
            } else {
                disableViewActionsAndButtons();
            }
        }
    }
});
function getValue(table_name, field_name, restriction) {
    var parameters = {
        tableName: table_name,
        fieldNames: toJSON([field_name]),
        restriction: restriction
    };
    var result = Ab.workflow.Workflow.runRuleAndReturnResult('AbCommonResources-getDataRecords', parameters);
    var record = result.data.records[0];
    return record ? record[field_name] : '';
}
function disableViewActionsAndButtons() {
    Ab.wf.Action.disableViewActionsAndButtons();
}