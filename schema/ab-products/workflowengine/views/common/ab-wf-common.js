WfMessages = new (Base.extend(
    {
        // @begin_translatable
        z_INVALID_BLOCK: "The workflow was changed and you cannot proceed with the next step, you need to start the workflow again. The last view accessible for the user will be opened. Do you want to proceed?",
        z_INVALID_VIEW: "The workflow was changed and you cannot proceed with the next step, you need to start the workflow again. The last view accessible for the user was not found and the request cannot be opened.",
        z_INVALID_BLOCK_DATA: "Block data cannot be found or invalid block data.",
        z_NO_VIEW_DEFINED: "No view defined on the workflow for this step.",
        z_START_BLOCK_DEFINED: "No start step defined on the workflow."
        // @end_translatable
    }));
function WF_create(panelName) {
    var preview = false;
    if (valueExists(View.parameters) && valueExists(View.parameters.wfParams)) {
        preview = View.parameters.wfParams.preview || false;
    }
    if (!preview) {
        var panel = View.panels.get(panelName);
        var row = panel.rows[panel.selectedRowIndex].row;
        var wfType = row.getFieldValue("wf.wf_type"),
            mainTable = row.getFieldValue("wf.main_table"),
            objType = 'StartEvent';
        var restriction = new Ab.view.Restriction();
        restriction.addClause("wf_blocks.wf_type", wfType);
        restriction.addClause("wf_blocks.obj_type", objType);
        var blocksDS = View.dataSources.get("blockDS");
        var blockRecord = blocksDS.getRecord(restriction);
        if (valueExists(blockRecord)) {
            var blockId = blockRecord.getValue('wf_blocks.block_id');
            var viewName = blockRecord.getValue('wf_blocks.view_name');
            if (valueExistsNotEmpty(viewName)) {
                var param = {
                    wfType: wfType,
                    objType: objType,
                    blockId: blockId,
                    tableName: mainTable,
                    edit: true
                };
                if (View.parentTab) {
                    View.parentTab.parentPanel.createTabWithoutRefresh(viewName, null, true, param);
                } else {
                    panel.parentTab.parentPanel.createTabWithoutRefresh(viewName, null, true, param);
                }
            } else {
                View.showMessage(View.getLocalizedString(WfMessages.z_NO_VIEW_DEFINED));
                return;
            }
        } else {
            View.showMessage(View.getLocalizedString(WfMessages.z_START_BLOCK_DEFINED));
            return;
        }
    }
}
function WF_open(panelName, mainTable) {
    var preview = false;
    if (valueExists(View.parameters) && valueExists(View.parameters.wfParams)) {
        preview = View.parameters.wfParams.preview || false;
    }
    if (!preview) {
        var panel = View.panels.get(panelName);
        var row = panel.rows[panel.selectedRowIndex].row;
        var wfType = row.getFieldValue(mainTable + ".wf_type"),
            wfBlockId = row.getFieldValue(mainTable + ".block_id"),
            blockId = wfBlockId,
            pkValue = row.getFieldValue(panel.primaryKeyIds[0]);
        if (typeof(findBlockByPkeyValue) == 'function') {
            blockId = findBlockByPkeyValue(pkValue);
        }
        var blockData = checkWorkflowBlock(blockId, wfType, mainTable, pkValue);
        if (blockData) {
            if (blockData.validBlock) {
                if (valueExistsNotEmpty(blockData.viewName)) {
                    var restriction = new Ab.view.Restriction();
                    restriction.addClause(panel.primaryKeyIds[0], pkValue);
                    var param = {
                        wfType: wfType,
                        blockId: blockData.blockId,
                        wfBlockId: wfBlockId,
                        tableName: mainTable,
                        pkValue: pkValue,
                        edit: blockData.canEdit
                    };
                    if (View.parentTab) {
                        View.parentTab.parentPanel.createTabWithoutRefresh(blockData.viewName, restriction, false, param);
                    } else {
                        panel.parentTab.parentPanel.createTabWithoutRefresh(blockData.viewName, restriction, false, param);
                    }
                } else {
                    View.showMessage(View.getLocalizedString(WfMessages.z_NO_VIEW_DEFINED));
                    return;
                }
            } else {
                if (valueExistsNotEmpty(blockData.viewName)) {
                    View.confirm(View.getLocalizedString(WfMessages.z_INVALID_BLOCK), function (action) {
                        if ('yes' === action) {
                            var param = {
                                wfType: wfType,
                                blockId: blockData.blockId,
                                wfBlockId: wfBlockId,
                                tableName: mainTable,
                                pkValue: pkValue,
                                edit: false
                            };
                            if (View.parentTab) {
                                View.parentTab.parentPanel.createTabWithoutRefresh(blockData.viewName, restriction, false, param);
                            } else {
                                panel.parentTab.parentPanel.createTabWithoutRefresh(blockData.viewName, restriction, false, param);
                            }
                        }
                    });
                } else {
                    View.showMessage(View.getLocalizedString(WfMessages.z_INVALID_VIEW));
                }
                return;
            }
        } else {
            View.showMessage(View.getLocalizedString(WfMessages.z_INVALID_BLOCK_DATA));
            return;
        }
    }
}
function getBlockRecord(wfType, blockId) {
    var blocksDS = View.dataSources.get("blockDS");
    var restriction = new Ab.view.Restriction();
    restriction.addClause("wf_blocks.wf_type", wfType);
    restriction.addClause("wf_blocks.block_id", blockId);
    return blocksDS.getRecord(restriction);
}
function getBlockConnections(wfType, blockId) {
    var blocksDS = View.dataSources.get("connectionsDS");
    var restriction = new Ab.view.Restriction();
    restriction.addClause("wf_connections.wf_type", wfType);
    restriction.addClause("wf_connections.source_id", blockId);
    return blocksDS.getRecords(restriction);
}
function getBlockExecution(wfType, pkValue, blockId) {
    var blocksDS = View.dataSources.get("blocksExecutionDS");
    var restriction = new Ab.view.Restriction();
    restriction.addClause("wf_blocks_execution.wf_type", wfType);
    restriction.addClause("wf_blocks_execution.pkey_value", pkValue);
    restriction.addClause("wf_blocks_execution.block_id", blockId);
    return blocksDS.getRecord(restriction);
}
function checkWorkflowBlock(blockId, wfType, mainTable, pkValue) {
    var result = {};
    try {
        result = Workflow.callMethod('WorkflowEngine-WorkflowEngineService-checkWorkflowBlock', blockId, wfType, mainTable, pkValue);
        if (result.code == 'executed') {
            return result.data;
        }
    } catch (e) {
        Workflow.handleError(e);
        return null;
    }
    return null;
}

function showAlertLegend() {
	var taskFile = View.originalRequestURL.substr(View.originalRequestURL.lastIndexOf('/')+1);
    var restriction = new Ab.view.Restriction();
    restriction.addClause("wf_mng_view.view_name", taskFile);
    var panel = View.panels.get('wfAlertLegendGrid');
    panel.showInWindow({closeButton: false, x: 400, y: 200, width: 400, height: 200, modal: false});
    panel.refresh(restriction);
}

function wfGridAfterRefresh(panel) {
	var taskFile = View.originalRequestURL.substr(View.originalRequestURL.lastIndexOf('/')+1);
    var restriction = new Ab.view.Restriction();
    restriction.addClause("wf_mng_view.view_name", taskFile);
    var alerts = View.dataSources.get('alertDS').getRecords(restriction);
    if (alerts.length > 0) {
        var colorField = getColorColumnId(panel.columns);
        if (valueExistsNotEmpty(colorField)) {
            for (var i = 0; i < panel.rows.length; i++) {
                var row = panel.rows[i];
                var color = row.row.getFieldValue(colorField);
                if (valueExistsNotEmpty(color)) {
                    var rowEl = Ext.get(panel.rows[i].row.dom);
                    rowEl.setStyle('background-color', '#' + color);
                }
            }
        }
    }
}

function getColorColumnId(columns) {
    var column = null;
    for (var i = 0; i < columns.length; i++) {
        if (columns[i].id.indexOf('.color') != -1) {
            column = columns[i].id;
            break;
        }
    }
    return column;
}