var tabCtrl = View.createController('tabsController', {
    resTabs: null,

    afterInitialDataFetch: function () {
        this.resTabs = View.getControlsByType(self, 'tabs')[0];
        this.resTabs.addEventListener('afterTabChange', onTabsChange);
    },

//    onTabsChange: function (tabPanel, selectedTabName) {
//        var selectedTab = tabPanel.findTab(selectedTabName);
//        if (selectedTab.isContentLoaded) {
//            var controller = selectedTab.getContentFrame().View.controllers.get('manageSelectController');
//            if (valueExistsNotEmpty(controller)) {
//            	controller.requestReportGrid.refresh();
//            }
//        }
//    }
});

Ab.tabs.Tab.prototype.createTabPanelWithoutLoad = function (callback) {
    // create managed iframe
    this.frame = new Ext.ux.ManagedIFrame({
        autoCreate: {
            id: this.name + '_frame',
            width: '100%', height: '1000'
        }
    });
    this.frame.removeClass('x-managed-iframe');
    this.frame.setStyle('border', 'none');

    // create Ext.Panel with the iframe as content
    var tabPanel = this.parentPanel.tabPanel.add({
        id: this.name,
        title: this.title,
        contentEl: this.frame,
        autoWidth: true,
        autoHeight: true,
        border: false,
        closable: true
    });
    this.tabPanel = tabPanel;
    this.id = this.name;
    this.callback = callback;
    // load view content into iframe
    callback();
};

Ab.tabs.Tabs.prototype.createTabWithoutRefresh = function (viewName, restriction, newRecord, params) {
    // if there is existing tab with the same restriction, do not create another instance
    if (valueExists(restriction)) {
        var existingTab = this.findTabByRestriction(restriction);
        if (existingTab != null) {
            this.selectTab(existingTab.name, restriction, newRecord);
            return;
        }
    }

    this.tabCreateCounter = this.tabCreateCounter + 1;
    var tabName = 'page_' + this.tabCreateCounter;
    var tabTitle = '&nbsp;&nbsp;&nbsp;&nbsp;';

    // create Tab object
    var tab = new Ab.tabs.Tab({
        name: tabName,
        title: tabTitle,
        fileName: viewName,
        selected: false,
        enabled: true,
        hidden: false,
        useParentRestriction: true,
        isDynamic: true,
        useFrame: true
    });

    this.addTab(tab);
    var tabs = this;
    View.wfParams = params;
    tab.createTabPanelWithoutLoad(function () {
        tabs.selectTab(tabName, restriction, newRecord);
    });
};
