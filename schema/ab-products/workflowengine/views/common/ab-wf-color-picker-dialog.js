var collorPickerCtrl = View.createController('collorPickerCtrl', {
    openerView: null,
    currentColor: null,

    afterViewLoad: function () {
        this.openerView = View.getOpenerView();
    },

    afterInitialDataFetch: function () {
        if (this.openerView.parameters && this.openerView.parameters['currentColor']) {
            currentColor(this.openerView.parameters['currentColor']);
        }
    }
});

function currentColor(colorStr) {
    if (colorStr.indexOf('#') == -1) {
        document.getElementById("colorDis").style.backgroundColor = '#' + colorStr;
    } else {
        document.getElementById("colorDis").style.backgroundColor = colorStr;
    }
    document.getElementById("colorHexDis").value = colorStr.toUpperCase();
}

function clickColor(colorStr) {
    document.getElementById("colorHexDis").value = colorStr.toUpperCase();
    if (collorPickerCtrl.openerView.parameters && collorPickerCtrl.openerView.parameters['afterSelectColor']) {
        var fn = collorPickerCtrl.openerView.parameters['afterSelectColor'];
        fn(colorStr.toUpperCase());
    }
    View.closeThisDialog();
}