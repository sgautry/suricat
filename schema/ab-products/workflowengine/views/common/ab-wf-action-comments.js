var commentsController = View.createController('commentsController', {
    afterInitialDataFetch: function () {
        var parameters = View.parameters.param;
        var commentsPanel = View.panels.get('commentsPanel');
        commentsPanel.setTitle(commentsPanel.title + " " + getMessage(parameters.action));
    },

    commentsPanel_onOk: function () {
        var comments = $('comments').value;
        if (!valueExistsNotEmpty(comments)) {
            View.showMessage(getMessage('noComments'));
            return;
        }
        View.parameters.callback(comments);
    }
});