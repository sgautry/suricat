var viewDefControl = View.createController('viewDefControl', {
    viewDefController: null,
    wfMngRecord: null,
    afterInitialDataFetch: function () {
        this.initViewDefinition();
    },
    initViewDefinition: function () {
        var tabs = View.getControl('', 'wfMngViewTabs');
        var viewDef = {
            wfTypes: tabs.wfTypes,
            mainTable: tabs.mainTable,
            activityId: tabs.activityId,
            mngType: tabs.mngType,
            fileName: tabs.viewName,
            taskId: tabs.taskId,
            viewType: tabs.viewType
        };
        this.setViewDef(viewDef);
        this.wfMngRecord = View.dataSources.get('wfMngViews_DS').getRecord(View.restriction);
    },
    setViewDef: function (viewDef) {
        this.viewDefController = new WorkflowEngine.ViewDef.Controller(viewDef, function (initView) {
            if (initView) {
                this.setDefaultRestrictions();
                viewDefControl.drawViewPanels();
            }
        });
        this.viewDefController.initView();
    },
    drawViewPanels: function () {
        this.resetViewPanels();
        jQuery('#viewName').text(this.viewDefController.fileName);
        if (valueExistsNotEmpty(this.viewDefController.fileName)) {
            jQuery('#startOver').hide();
        } else {
            $('viewTitle').value = 'Title for View';
            this.viewDefController.setViewTitle('Title for View');
        }
        this.viewDefPanel.setTitle(getMessage('designerViewTitle') + " - " + this.viewDefController.config.mngType);
        this.generatePanels();
    },
    resetViewPanels: function () {
        jQuery('#viewPanels').empty();//remove all children
    },
    generatePanels: function () {
        var view = this.viewDefController.viewObj;
        var title = view.title;
        if (title != undefined) {
            jQuery('#viewTitle').val(WorkflowEngine.ViewDef.Controller.replaceXMLAmp(title));
        }
        for (var index = 0; index < view.tableGroups.length; index++) {
            this.generatePanel(index, view.tableGroups[index].title);
            this.markPanel(index);
        }
    },
    generatePanel: function (index, name) {
        var generatePanel = function (el, id, title) {
            var generatePanelTemplate = View.templates.get('panelTemplate');
            generatePanelTemplate.render({
                    id: id,
                    title: title,
                    index: index
                },
                el,
                'after');
        };
        generatePanel(jQuery('#viewPanels')[0], 'panel-' + index, name);
    },
    markPanel: function (index) {
        var view = this.viewDefController.viewObj;
        jQuery('#dataTable-panel-' + index).html(this.viewDefController.config.mainTable);
        jQuery('#standardTable-panel-' + index).html(this.viewDefController.getStandardTables(index).toString());
        this.markCheckList('data-panel-' + index, view.tableGroups[index]);
        this.markButtons(index, view.tableGroups[index]);
        this.checkPanelTitle(index);
    },
    createPanel: function () {
        var view = this.viewDefController.viewObj;
        var name = getMessage('new_panel_title');
        this.viewDefController.addNewPanel();
        var index = view.tableGroups.length - 1;
        this.generatePanel(index, name);
        this.markPanel(index);
    },
    //TODO refactor
    markCheckList: function (id, tgrp) {
        // fields
        var fld = $(id + '-Fld');
        if (this.checkIfHasMainField(tgrp.fields, tgrp.tables[0].table_name)) {
            fld.innerHTML = "";
            this.addIcon(fld);
        } else {
            fld.style.color = "red";
        }
        // restrictions
        var rst = $(id + '-Rst');
        if ((tgrp.parsedRestrictionClauses) && (tgrp.parsedRestrictionClauses.length > 0)) {
            rst.innerHTML = "";
            this.addIcon(rst);
        } else {
            rst.style.color = "green";
        }
    },
    markButtons: function (id, tgrp) {
        var isConsole = tgrp.panelType === 'console';
        var setRestrictionButton = $('setRestriction-panel-' + id);
        if (isConsole) {
            setRestrictionButton.disabled = isConsole;
            $('data-panel-' + id + '-Rst').style.display = 'none';
        }
    },
    checkIfHasMainField: function (fields, table) {
        if ((fields != undefined) && (fields != '')) {
            for (var i = 0; i < fields.length; i++) {
                if (fields[i].table_name == table) {
                    return true;
                }
            }
        }
        return false;
    },
    startOver: function () {
        View.confirm(getMessage('startOver'), function () {
            View.controllers.get('viewDefControl').viewDefController.startOver(function () {
                viewDefControl.drawViewPanels();
                viewDefControl.saveViewName();
            });
        });
    },
    closeManageTab: function () {
        var tabs = View.getControl('','wfTabs');
        tabs.closeTab();
    },
    saveViewTitle: function () {
        this.viewDefController.setViewTitle(jQuery('#viewTitle').val());
    },
    addIcon: function (element) {
        var img = document.createElement("img");
        img.setAttribute("src", "/archibus/schema/ab-core/graphics/icons/tick.png");
        img.setAttribute("border", "0");
        img.setAttribute("align", "middle");
        img.setAttribute("valign", "right");
        element.appendChild(img);
    },
    checkPanelTitle: function (index) {
        var title = this.viewDefController.viewObj.tableGroups[index].title;
        jQuery('.title>p').eq(index).text(title);
    },
    saveViewName: function (viewName) {
        var record = this.wfMngRecord;
        record.setValue('wf_mng_view.view_name', viewName);
        View.dataSources.get('wfMngViews_DS').saveRecord(record);
    },
    openPanelEditor: function (panelId) {
        this.viewDefController.openPanelEditor(panelId);
    },
    openPanelRestriction: function (panelId) {
        this.viewDefController.openPanelRestriction(panelId);
    },
    saveAndPreview: function () {
        this.viewDefController.saveAndPreview(function (viewName) {
            View.closeDialog();
            viewDefControl.saveViewName(viewName);
            jQuery('#viewName').text(viewName);
            var tabPanel = View.getControl('', 'wfMngViewTabs');
            var tab = tabPanel.findTab('wfMngViewDefineTab');
            var controller = tab.getContentFrame().View.controllers.get('wfMngViewDefineController');
            controller.wfMngViewForm.setFieldValue('wf_mng_view.view_name', viewName);
            controller.wfMngViewForm.enableField('wf_mng_view.view_name', false);
            controller.wfMngViewForm.enableField('wf_mng_view.mng_type', false);
        });
    }
});

