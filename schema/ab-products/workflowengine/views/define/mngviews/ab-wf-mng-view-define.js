var wfMngViewDefineController = View.createController('wfMngViewDefineController', {
    wfTypes: null,
    afterInitialDataFetch: function () {
        var parentView = View.getView('parent');
        if (parentView.newRecord) {
            this.wfMngViewForm.newRecord = true;
        }
        this.wfMngViewForm.refresh(parentView.restriction);
        if (!valueExists(parentView.restriction.length) || parentView.restriction.length > 0) {
            this.wfAlertsGrid.refresh(parentView.restriction);
        }
    },
    wfMngViewForm_afterRefresh: function (panel) {
        this.setMainTable(panel.getFieldValue('wf_mng_view.mng_type'));
    },
    wfMngViewForm_onSave: function () {
        var panel = View.panels.get('wfMngViewForm');
        if (panel.save()) {
            this.wfTypes = panel.getFieldMultipleValues("wf_mng_view.wf_types");
            var mngType = panel.getRecord().getValue("wf_mng_view.mng_type"),
                mainTable = panel.getRecord().getValue("wf_mng_view.main_table"),
                activityId = panel.getRecord().getValue("wf_mng_view.activity_id"),
                viewName = panel.getRecord().getValue("wf_mng_view.view_name");
            var tabs = View.getControl('', 'wfMngViewTabs');
            tabs.wfTypes = this.wfTypes;
            tabs.mainTable = mainTable;
            tabs.activityId = activityId;
            tabs.mngType = mngType;
            tabs.viewName = viewName;
            tabs.viewType = 'grid';
            tabs.selectTab('wfMngViewDesignerTab', panel.restriction);
        }
    },
    wfAlertsGrid_onAddNew: function () {
        this.wfAlertsForm.newRecord = true;
        this.wfAlertsForm.refresh();
        this.wfAlertsForm.setFieldValue('wf_mng_alerts.mng_id', this.wfMngViewForm.getFieldValue('wf_mng_view.mng_id'));
        Ext.get('colorDisplay').setStyle('background-color', '');
        this.wfAlertsForm.showInWindow({
            x: 400,
            y: 200,
            width: 700,
            height: 200,
            closeButton: true,
            modal: true
        });
    },

    wfAlertsForm_onSave: function () {
        if (!valueExistsNotEmpty(this.wfAlertsForm.getFieldValue('wf_mng_alerts.user_task'))) {
            this.wfAlertsForm.setFieldValue('wf_mng_alerts.wf_type', '');
            this.wfAlertsForm.setFieldValue('wf_mng_alerts.block_id', '');
        }
        if (this.wfAlertsForm.save()) {
            this.wfAlertsForm.closeWindow();
            this.wfAlertsGrid.refresh();
        }
    },

    wfAlertsForm_afterRefresh: function () {
        var color = this.wfAlertsForm.getFieldValue('wf_mng_alerts.color');
        if (!this.wfAlertsForm.newRecord && valueExistsNotEmpty(color)) {
            Ext.get('colorDisplay').setStyle('background-color', '#' + color);
        }
    },

    selectWorkflowTypes: function () {
        //this.wfTypeSelectionPanel.refresh();
        this.wfTypeSelectionPanel.showInWindow({closeButton: true, x: 400, y: 200, modal: true});
        this.filterWorkflowType();
        this.wfTypeSelectionPanel.refresh();
    },
    filterWorkflowType: function () {
        this.wfTypeSelectionPanel.clearAllFilterValues();
        var wfTypes = this.wfMngViewForm.getFieldValue('wf_mng_view.wf_types');
        if (valueExistsNotEmpty(wfTypes)) {
            var _tmp = wfTypes.split(',');
            if (_tmp.length > 1) {
                var wfTypesValues = '\"' + _tmp.join(Ab.form.Form.MULTIPLE_VALUES_SEPARATOR) + '\"';
                var values = '{' + wfTypesValues.replace(/, \u200C/gi, '\",\"') + '}';
                this.wfTypeSelectionPanel.setFilterValue("wf.wf_type", values);
            } else {
                this.wfTypeSelectionPanel.setFilterValue("wf.wf_type", wfTypes);
            }
        }
    },
    wfTypeSelectionPanel_onSaveSelected: function () {
        var selectedRecords = this.wfTypeSelectionPanel.getSelectedRecords();
        if (selectedRecords.length == 0) {
            View.showMessage(getMessage('noRecSelected'));
            return;
        }
        this.addSelectedRecordsToLocationFilter(selectedRecords);
        this.wfTypeSelectionPanel.closeWindow();
    },
    addSelectedRecordsToLocationFilter: function (selectedRecords) {
        var locations = [];
        for (var i = 0; i < selectedRecords.length; i++) {
            var wfType = selectedRecords[i].getValue('wf.wf_type');
            locations.push(wfType);
        }
        this.wfMngViewForm.setFieldValue('wf_mng_view.wf_types', locations.join(','));
        this.wfMngViewForm.setFieldValue('wf_mng_view.activity_id', selectedRecords[0].getValue('wf.activity_id'));
        var mngType = this.wfMngViewForm.getFieldValue('wf_mng_view.mng_type');
        if ('Create' === mngType) {
            this.wfMngViewForm.setFieldValue('wf_mng_view.main_table', 'wf');
        } else {
            this.wfMngViewForm.setFieldValue('wf_mng_view.main_table', selectedRecords[0].getValue('wf.main_table'));
        }
    },
    setMainTable: function (value) {
        if ('Manage' === value) {
            var wfTypes = this.wfMngViewForm.getFieldValue("wf_mng_view.wf_types");
            var _tmp = wfTypes.split(',');
            if (_tmp.length > 0 && _tmp[0] != '') {
                var restriction = new Ab.view.Restriction();
                restriction.addClause('wf.wf_type', _tmp, 'IN');
                var record = View.dataSources.get('wf_DS').getRecord(restriction);
                var mainTable = record.getValue('wf.main_table'),
                    activityId = record.getValue('wf.activity_id');
                this.wfMngViewForm.setFieldValue('wf_mng_view.main_table', mainTable);
                this.wfMngViewForm.setFieldValue('wf_mng_view.activity_id', activityId);
            }
        } else {
            this.wfMngViewForm.setFieldValue('wf_mng_view.main_table', 'wf');
        }
    }
});

function openColorPicker() {
    if (!valueExists(View.parameters)) {
        View.parameters = {};
    }
    View.parameters.currentColor = wfMngViewDefineController.wfAlertsForm.getFieldValue('wf_mng_alerts.color');
    View.parameters.afterSelectColor = afterSelectColor;
    View.openDialog('ab-wf-color-picker-dialog.axvw', null, true, {
        width: 290,
        height: 300,
        closeButton: true
    });
}

function afterSelectColor(color) {
    wfMngViewDefineController.wfAlertsForm.setFieldValue('wf_mng_alerts.color', color);
    Ext.get('colorDisplay').setStyle('background-color', '#' + color);
}