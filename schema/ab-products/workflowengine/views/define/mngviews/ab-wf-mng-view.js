View.createController('wfMngViewController', {
    afterInitialDataFetch: function () {
        this.wfMngViewsList.addParameter('isPublished', getMessage('isPublished'));
        this.wfMngViewsList.addParameter('notPublished', getMessage('notPublished'));
        this.wfMngViewsList.refresh();
    },
    publishView: function () {
        var panel = View.panels.get('wfMngViewsList');
        var selectedRecord = panel.rows[panel.selectedRowIndex].row.getRecord();
        var activityId = selectedRecord.getValue('wf_mng_view.activity_id'),
            processId = selectedRecord.getValue('wf_mng_view.process_id'),
            viewName = selectedRecord.getValue('wf_mng_view.view_name');
        if (valueExistsNotEmpty(activityId) && valueExistsNotEmpty(viewName)) {
            var tabPanel = View.panels.get('wfTabs');
            var restriction = new Ab.view.Restriction();
            restriction.addClause('afm_ptasks.activity_id', activityId);
            restriction.addClause('afm_ptasks.process_id', processId);
            restriction.addClause('afm_ptasks.task_file', viewName);
            tabPanel.createTab('ab-wf-mng-view-add-to-pnav.axvw', restriction, false);
        } else {
            View.showMessage(getMessage('noViewDefined'));
        }
    }
});