View.createController('wfMngViewPublishController', {
    activityId: null,
    processId: null,
    taskFile: null,
    afterInitialDataFetch: function () {
        this.refreshPanels();
        $('afmPtasks_list').setAttribute('indexNewRow', '-1');
    },
    refreshPanels: function () {
        this.activityId = View.restriction.findClause('afm_ptasks.activity_id').value;
        this.processId = View.restriction.findClause('afm_ptasks.process_id').value;
        this.taskFile = View.restriction.findClause('afm_ptasks.task_file').value;
        View.setTitle(getMessage("publishTabTitle") + ": " + this.taskFile);
        this.afmPtasks_existing_list.refresh(View.restriction);
        this.afmPtasks_list.refresh(new Ab.view.Restriction({
            'afm_ptasks.activity_id': this.activityId,
            'afm_ptasks.process_id': this.processId
        }));
    },
    afmPtasks_list_afterRefresh: function (panel) {
        var controller = this;
        panel.gridRows.each(function (row) {
            var taskFile = row.getRecord().getValue('afm_ptasks.task_file');
            if (taskFile !== controller.taskFile) {
                row.actions.get("edit").show(false);
            }
        });
    },
    addPnav: function (command) {
        var parentPanel = command.getParentPanel();
        var restriction = new Ab.view.Restriction(command.getRestriction());
        this.afmPtasks_form.refresh(restriction, true);
        this.afmPtasks_form.setFieldValue('afm_ptasks.task_file', this.taskFile);
        this.afmPtasks_form.setFieldValue('afm_ptasks.display_order', this.calculateDisplayOrder(parentPanel));
        this.afmPtasks_form.showInWindow({
            anchor: command.context.target,
            closeButton: true
        });
    },
    editPnav: function (command) {
        var restriction = new Ab.view.Restriction(command.getRestriction());
        this.afmPtasks_form.refresh(restriction);
        this.afmPtasks_form.setFieldValue('afm_ptasks.task_file', this.taskFile);
        this.afmPtasks_form.showInWindow({
            anchor: command.context.target,
            closeButton: true
        });
    },
    onChangeHotList: function (value) {
        this.afmPtasks_form.setFieldValue('afm_ptasks.hot_user_name', ('1' === value ? View.user.name : ''));
    },
    calculateDisplayOrder: function (pTasksListPanel) {
        var selectedRowIndex = pTasksListPanel.selectedRowIndex;
        var rows = pTasksListPanel.rows;
        var displayOrder = this.getDisplayOrder(rows[selectedRowIndex].row);
        var nextRowIndex = selectedRowIndex + 1;
        var nextDisplayOrder = displayOrder;
        if (selectedRowIndex === rows.length - 1) {
            nextDisplayOrder = displayOrder + 20; // always add 10
        } else {
            nextDisplayOrder = this.getDisplayOrder(rows[nextRowIndex].row);
        }
        return (displayOrder + Math.ceil((nextDisplayOrder - displayOrder) / 2));
    },
    getDisplayOrder: function (row) {
        return Number(row.getRecord().getValue('afm_ptasks.display_order'));
    },
    afmPtasks_form_onSave: function () {
        if (this.afmPtasks_form.save()) {
            this.afmPtasks_form.closeWindow();
            this.refreshPanels();
        }
    }
});