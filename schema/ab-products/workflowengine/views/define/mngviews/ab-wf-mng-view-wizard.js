var wizardMngViewController = View.createController("wizardMngViewController", {
    mng_id: null,
    afterInitialDataFetch: function () {
        var tabs = View.panels.get("wfMngViewTabs");
        if (View.newRecord) {
            Ab.view.View.setTitle(getMessage("new_wf_mng_view"));
        } else {
            this.mng_id = View.restriction["wf_mng_view.mng_id"];
            Ab.view.View.setTitle(getMessage("wf_mng_view") + ' # ' + this.mng_id);
        }
        tabs.addEventListener('afterTabChange', this.afterChange);
        tabs.enableTab('wfMngViewDesignerTab', false);
    },
    afterChange: function (tabPanel, selectedTabName) {
        if (selectedTabName === 'wfMngViewDefineTab') {
            tabPanel.enableTab('wfMngViewDesignerTab', false);
        } else {
        	var tab = tabPanel.findTab(selectedTabName);
        	if (tab.isContentLoaded) {
                var controller = tab.getContentFrame().View.controllers.get('viewDefControl');
                controller.initViewDefinition();
            }
        }
    }
});