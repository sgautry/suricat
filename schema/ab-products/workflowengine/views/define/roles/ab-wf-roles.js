var wfRolesController = View.createController("wfRolesController", {
    onViewResults: function (row) {
        var record = row.row.getRecord();
        var role = record.getValue('wf_role.role'),
        	query = record.getValue('wf_role.query');
        try {
            var result = Workflow.callMethod('WorkflowEngine-WorkflowEngineService-getRoleList', role);
            if (result.code = 'executed') {
                var lookupPanel = View.panels.get('lookupPanel');
                lookupPanel.clear();
                lookupPanel.setFieldValue('query.sql', query);
                lookupPanel.setFieldValue('query.result', result.data + "");
                lookupPanel.enableField('query.sql', false);
                lookupPanel.enableField('query.result', false);
                lookupPanel.showInWindow({
                    width: 800,
                    height: 200,
                    x: 200,
                    y: 200
                });
            }
        } catch (e) {
            Workflow.handleError(e);
        }
    }
});

