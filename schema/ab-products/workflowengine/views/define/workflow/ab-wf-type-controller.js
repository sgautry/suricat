WorkflowEngine.WorkflowDefController = Base.extend({
    tabs: null,
    wfType: null,
    constructor: function (tabs) {
        this.tabs = tabs;
        this.tabs.addEventListener('afterTabChange', this.afterTabChange);
        this.tabs.enableTab('wfParametersTab', false);
        this.tabs.enableTab('wfDesignerTab', false);
    },
    init: function (restriction) {
        if (_.isEmpty(restriction)) {
            View.setTitle(getMessage("new_wf_type"));
        } else {
            this.wfType = restriction["wf.wf_type"];
            View.setTitle(getMessage("wf_type") + ' ' + this.wfType);
        }
    },
    afterTabChange: function (tabPanel, selectedTabName) {
        var tab = tabPanel.findTab(selectedTabName);
        switch (selectedTabName) {
            case 'wfTypeTab':
                tabPanel.enableTab('wfDesignerTab', false);
                break;
            case 'wfParametersTab':
                if (tab.isContentLoaded) {
                    tab.getContentFrame().View.controllers.get('wfParametersController').initWorkflowController();
                }
                break;
            case 'wfDesignerTab':
                if (tab.isContentLoaded) {
                    tab.getContentFrame().View.controllers.get('wfController').refreshWorkflowController();
                }
                break;
        }
    },
    setWorkflowTitle: function (title) {
        View.setTitle(title);
    },
    selectTab: function (tabName) {
        this.tabs.selectTab(tabName);
    },
    refreshWorkflowPanel: function (panel) {
        if (this.wfType) {
            panel.refresh(new Ab.view.Restriction({'wf.wf_type': this.wfType}));
        } else {
            panel.refresh({}, true); // new record
        }
    },
    getData: function (parameters) {
        var result = Ab.workflow.Workflow.runRuleAndReturnResult('AbCommonResources-getDataRecords', parameters);
        if (result.code == 'executed') {
            return result.data;
        } else {
            Workflow.handleError(result);
        }
    }
});