/**
 * Workflow Engine global namespace.
 */
var WorkflowEngine = window.WorkflowEngine || {};
WorkflowEngine.namespace = function (sNameSpace) {
    if (!sNameSpace || !sNameSpace.length) {
        return null;
    }
    var levels = sNameSpace.split(".");
    var currentNS = WorkflowEngine;
    for (var i = (levels[0] == "WorkflowEngine") ? 1 : 0; i < levels.length; ++i) {
        currentNS[levels[i]] = currentNS[levels[i]] || {};
        currentNS = currentNS[levels[i]];
    }
    return currentNS;};

