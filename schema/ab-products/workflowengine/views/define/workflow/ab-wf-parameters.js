View.createController('wfParametersController', {
    workflowController: null,
    requiredFields: ['wf_type', 'block_id', 'color', 'status', 'comments'],
    definedRequiredFields: [],
    afterInitialDataFetch: function () {
        this.initWorkflowController();
    },
    initWorkflowController: function () {
        this.workflowController = View.getOpenerView().controllers.get('wfTabsController').getControl();
        this.workflowController.refreshWorkflowPanel(this.wfTypeForm);
        this.checkRequiredFields();
    },
    checkRequiredFields: function () {
        this.definedRequiredFields = [];
        var restriction = new Ab.view.Restriction();
        restriction.addClause('afm_flds.table_name', this.workflowController.mainTable);
        restriction.addClause('afm_flds.field_name', this.requiredFields, 'IN');
        var param = {
            tableName: 'afm_flds',
            fieldNames: toJSON(['afm_flds.field_name', 'afm_flds.enum_list']),
            restriction: toJSON(restriction)
        };
        var records = this.workflowController.getData(param).records;
        var isReadOnly = true;
        for (var i = 0; i < this.requiredFields.length; i++) {
            var fieldName = this.requiredFields[i];
            var requiredField = this.getRequiredFieldDataRecord(records, fieldName);
            var defined = valueExists(requiredField),
                enumList = [];
            if ('wf_type' === fieldName && defined) {
                isReadOnly = this.requestsAreCreated();
            }
            if (defined) {
                enumList = requiredField['afm_flds.enum_list'];
                enumList = this.createEnumListObject(enumList, isReadOnly);
            }
            this.definedRequiredFields.push({
                fieldName: fieldName,
                defined: defined,
                enumList: enumList
            })
        }
        this.markRequiredFields();
        this.mandatoryFieldsPanel.show(true);
        this.updateActions();
    },
    updateActions: function (updateFields) {
        var fieldsNotDefined = this.fieldsNotDefined();
        this.enableAction(this.wfTypeForm.actions.get('designer'), !fieldsNotDefined);
        updateFields = updateFields || false;
        this.enableAction(this.mandatoryFieldsPanel.actions.get('updateFields'), fieldsNotDefined || updateFields);
    },
    enableAction: function (action, enable) {
        action.enabled = enable;
        action.forceDisable(!enable);
    },
    requestsAreCreated: function () {
        var param = {
            tableName: this.workflowController.mainTable,
            fieldNames: toJSON([this.workflowController.mainTable + '.wf_type']),
            restriction: this.workflowController.mainTable + '.wf_type IS NOT NULL'
        };
        var records = this.workflowController.getData(param).records;
        return records.length > 0;
    },
    markRequiredFields: function () {
        _.each(this.definedRequiredFields, function (field) {
            this.markRequiredField(field);
        }, this);
    },
    markRequiredField: function (field) {
        var requiredEl = jQuery(this.mandatoryFieldsPanel.getFieldCell(field.fieldName));
        requiredEl.empty();
        if (field.defined) {
            requiredEl.append(this.markExistsField());
        } else {
            requiredEl.append(this.markNotExistsField());
        }
        if ('status' === field.fieldName) {
            var enumList = this.getStatusEnumList(field.enumList);
            requiredEl.append(this.getStatusEl(enumList.join(';')));
        }
    },
    wfTypeForm_onDesigner: function () {
        this.workflowController.selectTab('wfDesignerTab');
    },
    defineStatus: function (button) {
        var statusEnumList = jQuery("#statusEnumList");
        statusEnumList.empty();
        this.generateStatus();
        this.statusFieldPanel.showInWindow({
            anchor: button.context.el.dom,
            width: 600,
            height: 500,
            closeButton: true
        });
        statusEnumList.sortable({
            placeholder: 'placeholder',
            handle: '.drag'
        });
    },
    changeStatusValue: function (type, el) {
        var index = el.id.split(type + '-')[1];
        if ('data-value' === type) {
            var displayValue = jQuery('#display-value-' + index);
            if (!valueExistsNotEmpty(displayValue.val())) {
                displayValue.val(el.value);
            }
        } else {
            var dataValue = jQuery('#data-value-' + index);
            if (!valueExistsNotEmpty(dataValue.val())) {
                dataValue.val(el.value);
            }
        }
    },
    getRequiredField: function (fieldName) {
        return _.where(this.definedRequiredFields, {fieldName: fieldName})[0];
    },
    generateStatus: function () {
        var status = this.getRequiredField('status');
        var enumList = status.enumList;
        for (var index = 0; index < enumList.length; index++) {
            this.generateFieldEnumList(index, enumList[index]);
        }
    },
    generateFieldEnumList: function (index, enumValue) {
        var generateTemplate = function (el, index, enumValue) {
            var isReadOnly = enumValue.isReadOnly || false;
            var generatePanelTemplate = View.templates.get('statusTemplate');
            generatePanelTemplate.render({
                    index: index,
                    enumValue: enumValue,
                    tooltip: getMessage('tooltipSortStatusValues')
                },
                el,
                'after');
            if (isReadOnly) {
                jQuery('#data-value-' + index).attr('readonly', true);
                jQuery('#display-value-' + index).attr('readonly', true);
            }
        };
        generateTemplate(jQuery('#statusEnumList')[0], index, enumValue);
    },
    addNewStatusEnumValue: function () {
        var newIndex = this.getMaxId();
        this.generateFieldEnumList(newIndex, {
            dataValue: '',
            displayValue: '',
            isReadOnly: false
        });
    },
    saveStatusEnumValue: function () {
        var statusField = this.getRequiredField('status');
        statusField.enumList = this.getSortedStatusEnumList();
        this.markRequiredField(statusField);
        this.updateActions(true);
        this.statusFieldPanel.closeWindow();
    },
    getSortedStatusEnumList: function () {
        var sortedFields = [];
        var statusEnumList = jQuery("#statusEnumList").sortable('toArray');
        for (var i = 0; i < statusEnumList.length; i++) {
            var statusEl = jQuery('#' + statusEnumList[i]);
            var dataValueEl = statusEl.find('.dataValue'),
                displayValueEl = statusEl.find('.displayValue');
            var dataValue = dataValueEl.val(),
                displayValue = displayValueEl.val(),
                isReadOnly = dataValueEl.is('[readonly]') && displayValueEl.is('[readonly]');
            if (valueExistsNotEmpty(dataValue) && valueExistsNotEmpty(displayValue)) {
                sortedFields.push({
                    dataValue: dataValue,
                    displayValue: displayValue,
                    isReadOnly: isReadOnly
                })
            }
        }
        return sortedFields;
    },
    updateFields: function () {
        try {
            var jobName = "WorkflowEngine-WorkflowEngineService-updateMandatoryFields";
            var jobId = Workflow.startJob(jobName, this.workflowController.mainTable, this.definedRequiredFields);
            View.openJobProgressBar(getMessage('createSchemaDefinitions'), jobId, '', function (status) {
                if (status.jobFinished == true) {
                    if (valueExists(status.jobProperties.updateSchema) && status.jobProperties.updateSchema === "true") {
                        View.controllers.get('wfParametersController').startCompareJob();
                    } else {
                        View.controllers.get('wfParametersController').displayInfoFields();
                    }
                }
            });
        } catch (e) {
            Workflow.handleError(e);
        }
    },
    startCompareJob: function () {
        try {
            SchemaUpdateWizardService.startCompareJob(this.workflowController.mainTable, false, false, false, false,
                {
                    callback: function (jobId) {
                        View.openJobProgressBar(getMessage('startCompareJob'), jobId, '', function (status) {
                            if (status.jobFinished == true) {
                                View.controllers.get('wfParametersController').startUpdateSchemaJob();
                            }
                        });
                    },
                    errorHandler: function (m, e) {
                        View.showException(e);
                    }
                });
        } catch (e) {
            Workflow.handleError(e);
        }
    },
    startUpdateSchemaJob: function () {
        try {
            SchemaUpdateWizardService.startUpdateSchemaJob(true, true, false, false, false, '',
                {
                    callback: function (jobId) {
                        View.openJobProgressBar(getMessage('startUpdateSchemaJob'), jobId, '', function (status) {
                            if (status.jobFinished == true) {
                                View.controllers.get('wfParametersController').displayInfoFields();
                            }
                        });
                    },
                    errorHandler: function (m, e) {
                        View.showException(e);
                    }
                });
        } catch (e) {
            Workflow.handleError(e);
        }
    },
    displayInfoFields: function () {
        this.checkRequiredFields();
    },
    fieldsNotDefined: function () {
        return _.where(this.definedRequiredFields, {defined: false}).length > 0;
    },
    markExistsField: function () {
        return '<img src="/archibus/schema/ab-core/graphics/icons/tick.png" qtip="Field defined in table ' + this.workflowController.mainTable + '">';
    },
    markNotExistsField: function () {
        return '<div class="inputField" style="color:red;font-weight:bold">' + getMessage('requiredField') + '</div>';
    },
    getStatusEl: function (values) {
        return '<div class="defaultEditForm_textareaABData_readonly" style="width:100%;display:inherit">' + values + '</div>';
    },
    getRequiredFieldDataRecord: function (records, fieldName) {
        var i = records.length;
        while (i--) {
            if (records[i]['afm_flds.field_name'] === fieldName) {
                return records[i];
            }
        }
        return null;
    },
    getStatusEnumList: function (enumList) {
        var enumListValues = [];
        for (var i = 0; i < enumList.length; i++) {
            var dataValue = enumList[i].dataValue,
                displayValue = enumList[i].displayValue;
            if (valueExistsNotEmpty(dataValue) && valueExistsNotEmpty(displayValue)) {
                enumListValues.push(dataValue);
                enumListValues.push(displayValue);
            }
        }
        return enumListValues;
    },
    getMaxId: function () {
        return (jQuery("#statusEnumList").sortable('toArray').length);
    },
    createEnumListObject: function (enumList, isReadOnly) {
        var list = [];
        var splitList = enumList.split(";");
        var items = splitList.length / 2 - splitList.length % 2;
        for (var i = 0; i < items; i++) {
            var dataValue = splitList[i * 2];
            var displayValue = splitList[i * 2 + 1];
            list.push({
                dataValue: dataValue,
                displayValue: displayValue,
                isReadOnly: isReadOnly
            });
        }
        return list;
    }
});