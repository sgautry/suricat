View.createController('wfTypeController', {
    workflowController: null,
    afterInitialDataFetch: function () {
        this.initWorkflowController();
    },
    initWorkflowController: function () {
        this.workflowController = View.getOpenerView().controllers.get('wfTabsController').getControl();
        this.workflowController.refreshWorkflowPanel(this.wfTypeForm);
    },
    wfTypeForm_onSave: function () {
        var panel = this.wfTypeForm;
        if (panel.save()) {
            var record = panel.getRecord();
            var wfType = record.getValue("wf.wf_type");
            this.workflowController.wfType = wfType;
            this.workflowController.activityId = record.getValue("wf.activity_id");
            this.workflowController.mainTable = record.getValue("wf.main_table");
            this.workflowController.folderPath = record.getValue("wf.folder_path");
            this.workflowController.setWorkflowTitle(getMessage("wf_type") + ' ' + wfType);
            this.workflowController.selectTab('wfParametersTab');
        }
    }
});