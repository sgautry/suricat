View.createController("wfTabsController", {
    workflowController: null,
    afterInitialDataFetch: function () {
        this.workflowController = new WorkflowEngine.WorkflowDefController(View.panels.get("wfDefineTabs"));
        this.workflowController.init(View.restriction);
    },
    getControl: function () {
        return this.workflowController;
    }
});