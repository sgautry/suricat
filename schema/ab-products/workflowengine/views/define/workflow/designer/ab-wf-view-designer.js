var viewDefControl = View.createController('viewDefControl', {
    viewDefController: null,
    events: {
        'click .panel-define .delete': function (event) {
            var id = event.target.id.split("_");
            viewDefControl.deleteViewPanel(id[0]);
        }
    },
    setViewDef: function (viewDef) {
        this.viewDefController = new WorkflowEngine.ViewDef.Controller(viewDef, function (initView) {
            if (initView) {
                viewDefControl.drawViewPanels();
            }
        });
        this.viewDefController.initView();
    },
    drawViewPanels: function () {
        this.viewDefPanel.setTitle(this.viewDefPanel.getTitle() + " - " + this.viewDefController.config.type);
        this.generatePanels();
    },
    generatePanel: function (index, name) {
        var generatePanel = function (el, id, title) {
            var generatePanelTemplate = View.templates.get('panelTemplate');
            generatePanelTemplate.render({
                    id: id,
                    title: title,
                    index: index
                },
                el,
                'after');
        };
        generatePanel(jQuery('#viewPanels')[0], 'panel-' + index, name);
    },
    generatePanels: function () {
        var view = this.viewDefController.viewObj;
        var title = view.title;
        if (valueExists(title)) {
            jQuery('#viewTitle').val(WorkflowEngine.ViewDef.Controller.replaceXMLAmp(title));
        }
        for (var index = 0; index < view.tableGroups.length; index++) {
            this.generatePanel(index, view.tableGroups[index].title);
            this.markPanel(index);
        }
    },
    markPanel: function (index) {
        var view = this.viewDefController.viewObj;
        jQuery('#dataTable-panel-' + index).html(this.viewDefController.config.mainTable);
        jQuery('#standardTable-panel-' + index).html(this.viewDefController.getStandardTables(index).toString());
        this.markCheckList('data-panel-' + index, view.tableGroups[index]);
        this.checkPanelTitle(index);
    },
    createPanel: function () {
        var view = this.viewDefController.viewObj;
        var name = getMessage('new_panel_title');
        this.viewDefController.addNewPanel();
        var index = view.tableGroups.length - 1;
        this.generatePanel(index, name);
        this.markPanel(index);
    },
    deleteViewPanel: function (panelId) {
        var index = Number(panelId.split('-')[1]);
        var view = this.viewDefController.viewObj;
        view.tableGroups.splice(index, 1);
        jQuery('#viewPanels').empty();//remove all children
        this.generatePanels();//recreate panels
    },
    //TODO refactor
    markCheckList: function (id, tgrp) {
        // fields
        var fld = $(id + '-Fld');
        if (this.checkIfHasMainField(tgrp.fields, tgrp.tables[0].table_name)) {
            fld.innerHTML = "";
            this.addIcon(fld);
        } else {
            fld.style.color = "red";
        }
        // restrictions
        var rst = $(id + '-Rst');
        if ((tgrp.parsedRestrictionClauses) && (tgrp.parsedRestrictionClauses.length > 0)) {
            rst.innerHTML = "";
            this.addIcon(rst);
        } else {
            rst.style.color = "green";
        }
    },
    checkIfHasMainField: function (fields, table) {
        if ((fields != undefined) && (fields != '')) {
            for (var i = 0; i < fields.length; i++) {
                if (fields[i].table_name == table) {
                    return true;
                }
            }
        }
        return false;
    },
    addIcon: function (element) {
        var image_path = "/archibus/schema/ab-core/graphics/icons/tick.png";
        var src = image_path;
        var img = document.createElement("img");
        img.setAttribute("src", src);
        img.setAttribute("border", "0");
        img.setAttribute("align", "middle");
        img.setAttribute("valign", "right");
        element.appendChild(img);
    },
    saveViewTitle: function () {
        this.viewDefController.setViewTitle(jQuery('#viewTitle').val());
    },
    checkPanelTitle: function (index) {
        var title = this.viewDefController.viewObj.tableGroups[index].title;
        jQuery('.title>p').eq(index).text(title);
    },
    openPanelEditor: function (panelId) {
        this.viewDefController.openPanelEditor(panelId);
    },
    openPanelRestriction: function (panelId) {
        this.viewDefController.openPanelRestriction(panelId);
    },
    saveAndPreview: function () {
        this.viewDefController.saveAndPreview();
    }
});