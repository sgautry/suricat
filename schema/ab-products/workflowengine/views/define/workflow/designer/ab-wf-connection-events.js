var eventsController = View.createController('eventsController', {
    events: [],
    selectedRow: null,
    selectedField: {},
    _DOT: '.',
    afterInitialDataFetch: function () {
        this.events = View.parameters.events;
        var columns = [
            new Ab.grid.Column('event_type', getMessage('event_type'), 'link', this.editEvent.createDelegate(this)),
            new Ab.grid.Column('condition', getMessage('condition'), 'link', this.editEvent.createDelegate(this)),
            new Ab.grid.Column('notification_id', getMessage('notification_id'), 'link', this.editEvent.createDelegate(this)),
            new Ab.grid.Column('role', getMessage('role'), 'link', this.editEvent.createDelegate(this)),
            new Ab.grid.Column('update_ref', getMessage('update_ref'), 'link', this.editEvent.createDelegate(this)),
            new Ab.grid.Column('update_field', getMessage('update_field'), 'link', this.editEvent.createDelegate(this)),
            new Ab.grid.Column('update_value', getMessage('update_value'), 'link', this.editEvent.createDelegate(this)),
            new Ab.grid.Column('rule_id', getMessage('rule_id'), 'link', this.editEvent.createDelegate(this)),
            new Ab.grid.Column('activity_id', getMessage('activity_id'), 'link', this.editEvent.createDelegate(this))];
        var rows = jQuery.extend(true, [], this.events);
        var configObj = new Ab.view.ConfigObject();
        configObj['rows'] = rows;
        configObj['columns'] = columns;
        configObj['viewDef'] = '';
        configObj['sortEnabled'] = false;
        configObj['selectionEnabled'] = true;
        configObj['multipleSelectionEnabled'] = true;
        this.grid = new Ab.grid.ReportGrid('events', configObj);
        if (rows.length == 0) {
            this.grid.hasNoRecords = true;
        }
        this.grid.build();
    },
    createEvent: function (type) {
        var panel = View.panels.get(type + 'Form');
        panel.clear();
        panel.showInWindow({
            width: 600,
            height: 400,
            x: 300,
            y: 100,
            closeButton: true
        });
        this.selectedField = {};
        this.resetFields();
        panel.setFieldValue("event_id", this.getNewId());
        panel.setFieldValue("conn_id", View.parameters.connId);
        panel.setFieldValue("wf_type", View.parameters.wfType);
        panel.showField("event_id", false);
        panel.showField("conn_id", false);
        panel.showField("wf_type", false);

        panel.actions.get('actionSave').button.el.dom.onclick = null;
        panel.actions.get('actionSave').button.el.dom.onclick = this.saveForm.createDelegate(this, [panel, true]);
    },
    saveForm: function (panel, newRecord) {
        var updateField = '';
        if (!_.isEmpty(this.selectedField)) {
            updateField = this.selectedField['tableName'] + this._DOT + this.selectedField['updateField'];
        }
        if (newRecord) {
            var record = {
                'event_id': panel.getFieldValue('event_id'),
                'conn_id': panel.getFieldValue('conn_id'),
                'wf_type': panel.getFieldValue('wf_type'),
                'event_type': panel.getFieldValue('event_type'),
                'condition': panel.getFieldValue('condition'),
                'notification_id': panel.getFieldValue('notification_id'),
                'role': panel.getFieldValue('role'),
                'update_ref': panel.getFieldValue('updateRefField'),
                'update_field': updateField,
                'update_value': this.getUpdateFieldValue(),
                'rule_id': panel.getFieldValue('rule_id'),
                'activity_id': panel.getFieldValue('activity_id')
            };
            this.grid.addGridRow(record);
            this.grid.update();
            this.events.push(_.omit(record, ['grid', 'row', 'index']));
        } else {
            var record = this.grid.gridRows.get(this.grid.selectedRowIndex).record;
            record['event_type'] = panel.getFieldValue('event_type');
            record['condition'] = panel.getFieldValue('condition');
            record['notification_id'] = panel.getFieldValue('notification_id');
            record['role'] = panel.getFieldValue('role');
            record['update_ref'] = panel.getFieldValue('updateRefField');
            record['update_field'] = updateField;
            record['update_value'] = this.getUpdateFieldValue();
            record['rule_id'] = panel.getFieldValue('rule_id');
            record['activity_id'] = panel.getFieldValue('activity_id');
            this.grid.update();
            var event = this.events[this.grid.selectedRowIndex];
            event['event_type'] = panel.getFieldValue('event_type');
            event['condition'] = panel.getFieldValue('condition');
            event['notification_id'] = panel.getFieldValue('notification_id');
            event['update_ref'] = panel.getFieldValue('updateRefField');
            event['update_field'] = updateField;
            event['update_value'] = this.getUpdateFieldValue();
            event['role'] = panel.getFieldValue('role');
            event['rule_id'] = panel.getFieldValue('rule_id');
            event['activity_id'] = panel.getFieldValue('activity_id');
        }
        View.parameters.callback(this.events);
        panel.closeWindow();
    },
    editEvent: function (row) {
        this.selectedRow = row;
        var type = row.event_type;
        var panel = View.panels.get(type + 'Form');
        panel.clear();
        panel.showInWindow({
            width: 600,
            height: 400,
            x: 300,
            y: 100,
            closeButton: true
        });
        this.selectedField = {};
        this.resetFields();

        panel.setFieldValue("event_id", row.event_id);
        panel.setFieldValue("conn_id", row.conn_id);
        panel.setFieldValue("wf_type", row.wf_type);
        panel.showField("event_id", false);
        panel.showField("conn_id", false);
        panel.showField("wf_type", false);
        panel.setFieldValue("event_type", row.event_type);
        panel.setFieldValue("condition", row.condition);
        panel.setFieldValue("notification_id", row.notification_id);
        panel.setFieldValue("role", row.role);
        panel.setFieldValue("updateRefField", row.update_ref);
        panel.setFieldValue("updateField", row.update_field);
        panel.setFieldValue("rule_id", row.rule_id);
        panel.setFieldValue("activity_id", row.activity_id);
        if ('update' === type) {
            panel.showField("enumFieldValue", false);
            panel.showField("numberFieldValue", false);
            panel.showField("dateFieldValue", false);
            panel.showField("timeFieldValue", false);
            this.setSelectedField(row.update_ref, row.update_field);
            this.displayFieldValue(this.selectedField['enumList'], row.update_value);
        }
        panel.actions.get('actionSave').button.el.dom.onclick = null;
        panel.actions.get('actionSave').button.el.dom.onclick = this.saveForm.createDelegate(this, [panel, false]);
    },
    //updateRefField
    setSelectedField: function (updateRefField, updateFieldValue) {
        var splitValue = updateFieldValue.split(this._DOT);
        var tableName = splitValue[0],
            fieldName = splitValue[1];
        var record = this.fieldDef_DS.getRecord(new Ab.view.Restriction({
            'afm_flds.table_name': tableName,
            'afm_flds.field_name': fieldName
        }));
        this.selectedField = {
            updateRefField: updateRefField,
            tableName: record.getValue('afm_flds.table_name'),
            updateField: record.getValue('afm_flds.field_name'),
            mlHeading: record.getValue('afm_flds.ml_heading'),
            dataType: record.getValue('afm_flds.data_type'),
            size: Number(record.getValue('afm_flds.afm_size')),
            decimals: Number(record.getValue('afm_flds.decimals')),
            enumList: record.getValue('afm_flds.enum_list')
        };
    },
    resetFields: function () {
        var panel = View.panels.get('updateForm');
        //reset values
        panel.setFieldValue("updateField", '');
        panel.setFieldValue("textFieldValue", '');
        panel.setFieldValue("numberFieldValue", '');
        panel.setFieldValue("dateFieldValue", '');
        panel.setFieldValue("timeFieldValue", '');
        panel.setFieldValue("enumFieldValue", '');
        panel.setFieldValue("whereRestriction", '');
        //hide fields
        panel.showField("enumFieldValue", false);
        panel.showField("numberFieldValue", false);
        panel.showField("dateFieldValue", false);
        panel.showField("timeFieldValue", false);
    },
    deleteEvent: function () {
        var rows = [], events = [];
        var selectedRecords = this.grid.getSelectedGridRows();
        if (selectedRecords.length < 1) {
            View.showMessage(getMessage("noRecSelected"));
            return;
        }
        if (selectedRecords.length == this.grid.rows.length) {
            this.grid.clearGridRows();
        } else {
            for ( var i = 0; i < this.grid.rows.length; i++ ) {
                var row = this.grid.rows[i];
                if (!row.row.isSelected()) {
                    rows.push(row);
                    events.push(_.omit(row, ['grid', 'row', 'index']));
                }
            }
        }
        this.grid.rows = rows;
        this.grid.update();
        View.parameters.events = this.events = events;
        View.parameters.callback(this.events);
    },
    getNewId: function () {
        var id = 0;
        for ( var i = 0; i < this.events.length; i++ ) {
            var eventId = Number(this.events[i]['event_id']);
            if (eventId > id) {
                id = eventId;
            }
        }
        return id + 1;
    },
    createSelectOptions: function (field, enumList) {
        var splitList = enumList.split(";");
        var items = splitList.length / 2 - splitList.length % 2;
        for ( var i = 0; i < items; i++ ) {
            field.addOption(splitList[i * 2], splitList[i * 2 + 1]);
        }
    },
    displayFieldValue: function (enumListValue, updateValue) {
        var isEnum = valueExistsNotEmpty(enumListValue);
        if (isEnum) {
            var enumField = this.updateForm.fields.get('enumFieldValue');
            enumField.clearOptions();
            this.createSelectOptions(enumField, enumListValue);
        }
        this.adjustFieldValue(isEnum, updateValue);
        this.setReferenceRestriction();
    },
    adjustFieldValue: function (show, updateValue) {
        var panel = View.panels.get('updateForm');
        panel.setFieldValue('updateField', this.selectedField['tableName'] + this._DOT + this.selectedField['updateField']);
        panel.setFieldValue('textFieldValue', '');
        panel.setFieldValue('numberFieldValue', '');
        panel.setFieldValue('dateFieldValue', '');
        panel.setFieldValue('timeFieldValue', '');
        panel.showField('textFieldValue', false);
        panel.showField('numberFieldValue', false);
        panel.showField('dateFieldValue', false);
        panel.showField('timeFieldValue', false);
        if (show) {
            panel.showField('enumFieldValue', true);
            if (updateValue) {
                panel.setFieldValue('enumFieldValue', updateValue);
            }
        } else {
            panel.showField('enumFieldValue', false);
            var dataType = this.selectedField['dataType'],
                size = Number(this.selectedField['size']),
                decimals = Number(this.selectedField['decimals']);
            if (dataType === '9') { // Date
                panel.showField('dateFieldValue', true);
                if (updateValue) {
                    panel.setFieldValue('dateFieldValue', updateValue);
                }
            } else if (dataType === '10') { // Time
                panel.showField('timeFieldValue', true);
                if (updateValue) {
                    panel.setFieldValue('timeFieldValue', updateValue);
                }
            } else if (dataType === '2' || dataType === '4' || dataType === '5' || dataType === '6' || dataType === '7' || dataType === '8') {//2;Numeric;4;Integer;5;Smallint;6;Float;7;Real;8;Double;
                panel.showField('numberFieldValue', true);
                panel.fields.get('numberFieldValue').fieldDef.decimals = decimals;
                panel.fields.get('numberFieldValue').fieldDef.size = size;
                if (updateValue) {
                    panel.setFieldValue('numberFieldValue', updateValue);
                }
            } else {// Varchar, Char, LongVarchar
                panel.showField('textFieldValue', true);
                panel.fields.get('textFieldValue').fieldDef.size = size;
                jQuery(panel.getFieldElement('textFieldValue')).attr('maxlength', size);
                jQuery(panel.getFieldElement('textFieldValue')).attr('size', size);
                if (updateValue) {
                    panel.setFieldValue('textFieldValue', updateValue);
                }
            }
        }
    },
    getUpdateFieldValue: function () {
        var panel = View.panels.get('updateForm');
        var updateValue = panel.getFieldValue('textFieldValue');
        if (!valueExistsNotEmpty(updateValue)) {
            updateValue = panel.getFieldValue('numberFieldValue');
        }
        if (!valueExistsNotEmpty(updateValue)) {
            updateValue = panel.getFieldValue('dateFieldValue');
        }
        if (!valueExistsNotEmpty(updateValue)) {
            updateValue = panel.getFieldValue('timeFieldValue');
        }
        if (!valueExistsNotEmpty(updateValue)) {
            updateValue = panel.getFieldValue('enumFieldValue');
        }
        return updateValue;
    },
    setReferenceRestriction: function () {
        this.updateForm.setFieldValue('whereRestriction', this.getReferenceRestriction());
    },
    getReferenceRestriction: function () {
        var referenceRestriction = '';
        var tableName = this.selectedField['tableName'],
            updateField = this.selectedField['updateRefField'];
        var depCols = this.getDependencyColumns();
        if (valueExistsNotEmpty(depCols)) {
            updateField = depCols;
        }
        var records = this.afmFldsPk_DS.getRecords(new Ab.view.Restriction({'afm_flds.table_name': tableName}));
        var tmp = updateField.split(';'),
            useSameField = records.length > 1 && !valueExistsNotEmpty(depCols);
        _.each(records, function (record, index) {
            var fieldName = record.getValue('afm_flds.field_name');
            var refField = tmp[index];
            if (useSameField) {
                refField = fieldName;
            }
            referenceRestriction += View.parameters.mainTable + this._DOT + refField + ' = ' + tableName + this._DOT + fieldName;
            if (index < records.length - 1) {
                referenceRestriction += ' AND ';
            }
        }, this);
        return referenceRestriction;
    },
    getDependencyColumns: function () {
        var record = this.fieldDef_DS.getRecord(new Ab.view.Restriction({
            'afm_flds.table_name': View.parameters.mainTable,
            'afm_flds.field_name': this.selectedField['updateRefField']
        }));
        return record.getValue('afm_flds.dep_cols');
    }
});
function afterSelectReferenceField(fieldName, selectedValue) {
    var controller = View.controllers.get('eventsController');
    var selectedField = controller.selectedField;
    selectedField[fieldName] = selectedValue;
    if ('updateRefField' === fieldName) {
        controller.resetFields();
    }
}
function afterSelectUpdateField(fieldName, selectedValue, previousValue, rawValue) {
    // reset max value to enable enum_list selection
    Ab.grid.SelectValue.MAX_VALUE_LENGTH = 850;
    var controller = View.controllers.get('eventsController');
    var selectedField = controller.selectedField;
    if ('dataType' === fieldName || 'size' === fieldName) {
        selectedValue = rawValue;
    }
    selectedField[fieldName] = selectedValue;
    if ('enumList' === fieldName) {
        controller.displayFieldValue(selectedValue);
    }
}
function selectFieldUpdateValue() {
    var controller = View.controllers.get('eventsController');
    var fieldName = controller.selectedField['updateField'],
        dataType = controller.selectedField['dataType'];
    if (valueExistsNotEmpty(fieldName)) {
        var tableName = controller.selectedField['tableName'],
            refTable = controller.selectedField['refTable'];
        if (valueExistsNotEmpty(refTable)) {
            tableName = refTable;
        }
        var tfJsonObject = eval("['" + tableName + "." + fieldName + "']");
        View.selectValue({
            formId: 'updateForm',
            title: controller.selectedField['mlHeading'],
            fieldNames: ['textFieldValue'],
            selectTableName: tableName,
            selectFieldNames: tfJsonObject,
            visibleFieldNames: tfJsonObject,
            showIndex: false,
            restriction: tableName + "." + fieldName + " IS NOT NULL ",
            selectValueType: 'grid',
            width: 900,
            height: 400
        });
    } else {
        View.showMessage(getMessage("noFieldName"));
    }
}