View.createController('wfController', {
    afterInitialDataFetch: function () {
        this.initWorkflowController();
    },
    initWorkflowController: function () {
        var config = this.getConfig();
        this.workflowController.flowchartControl = new WorkflowEngine.FlowchartControl('flowchartDiv', 'flow', 'console', config);
        this.workflowController.flowchartControl.load();
    },
    refreshWorkflowController: function () {
        var config = this.getConfig();
        this.workflowController.flowchartControl.refresh(config);
    },
    getConfig: function () {
        this.workflowController = View.getOpenerView().controllers.get('wfTabsController').getControl();
        var config = new Ab.view.ConfigObject();
        config['wfType'] = this.workflowController.wfType;
        config['activityId'] = this.workflowController.activityId;
        config['mainTable'] = this.workflowController.mainTable;
        config['folderPath'] = this.workflowController.folderPath;
        config['panAndZoom'] = this.workflowController;
        return config;
    },
    showHelp: function () {
        View.openDialog(View.contextPath + '/schema/ab-products/workflowengine/help/index.htm', null, false, {
            width: 1024,
            height: 800
        });
    }
});