View.createController('wfDiagramController', {
    afterInitialDataFetch: function () {
        var config = new Ab.view.ConfigObject();
        config['wfType'] = View.parameters['wfType'];
        config['mainTable'] = View.parameters['mainTable'];
        config['blockId'] = View.parameters['blockId'];
        config['panAndZoom'] = true;
        var flowchartDiagramControl = new WorkflowEngine.FlowchartDiagramControl('flowchartDiv', 'flow', config);
        flowchartDiagramControl.load();
    }
});