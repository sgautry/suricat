View.createController('wfRestrictionController', {
    selectedField: {},
    restrictions: [],
    afterViewLoad: function () {
        this.mainTable = View.parameters.mainTable;
        this.blockId = View.parameters.blockId;
        this.mngType = View.parameters.mngType;
        this.noOfTableFields = View.parameters.noOfTableFields;
        this.viewObj = View.parameters.viewObj;
        this.panelIndex = View.parameters.panelIndex;
    },
    afterInitialDataFetch: function () {
        this.restSelectPanel.clear();
        this.restSelectPanel.showField('enumFieldValue', false);
        this.initRestrictions();
    },
    initRestrictions: function () {
        this.restrictions = this.viewObj.tableGroups[this.panelIndex].parsedRestrictionClauses || [];
        this.generateRestrictions();
    },
    selectField: function () {
        // reset max value to enable enum_list selection
        Ab.grid.SelectValue.MAX_VALUE_LENGTH = 850;
        var restriction = this.getFieldsRestriction();
        View.selectValue({
            formId: 'restSelectPanel',
            title: getMessage('fieldNameSelectValueTitle'),
            fieldNames: ['tableName', 'fieldName', 'mlHeading', 'dataType', 'refTable', 'enumList'],
            selectTableName: 'afm_flds',
            selectFieldNames: ['afm_flds.table_name', 'afm_flds.field_name', 'afm_flds.ml_heading', 'afm_flds.data_type', 'afm_flds.ref_table', 'afm_flds.enum_list'],
            visibleFieldNames: ['afm_flds.table_name', 'afm_flds.field_name', 'afm_flds.ml_heading'],
            showIndex: false,
            restriction: restriction,
            actionListener: this.afterSelectFieldName,
            width: 900,
            height: 400
        });
    },
    afterSelectFieldName: function (fieldName, selectedValue, previousValue) {
        var controller = View.controllers.get('wfRestrictionController');
        var selectedField = controller.selectedField;
        selectedField[fieldName] = selectedValue;
        if ('enumList' === fieldName) {
            var isEnum = valueExistsNotEmpty(selectedValue);
            if (isEnum) {
                var enumField = View.panels.get('restSelectPanel').fields.get('enumFieldValue');
                enumField.clearOptions();
                controller.createSelectOptions(enumField, selectedValue);
            }
            controller.adjustOperator(selectedField['dataType']);
            controller.adjustFieldValue(isEnum);
        }
    },
    selectFieldValue: function () {
        var fieldName = this.selectedField['fieldName'],
            dataType = this.selectedField['dataType'];
        if (valueExistsNotEmpty(fieldName)) {
            if (dataType == 'Date') {
                Calendar.getController('textFieldValue', '/archibus/schema/ab-system/graphics');
            } else {
                var tableName = this.selectedField['tableName'],
                    refTable = this.selectedField['refTable'];
                if (valueExistsNotEmpty(refTable)) {
                    tableName = refTable;
                }
                var tfJsonObject = eval("['" + tableName + "." + fieldName + "']");
                var selectValueType = 'grid';
                if (this.isMultipleSelection()) {
                    selectValueType = 'multiple';
                }
                View.selectValue({
                    formId: 'restSelectPanel',
                    title: this.selectedField['mlHeading'],
                    fieldNames: ['textFieldValue'],
                    selectTableName: tableName,
                    selectFieldNames: tfJsonObject,
                    visibleFieldNames: tfJsonObject,
                    showIndex: false,
                    restriction: tableName + "." + fieldName + " IS NOT NULL ",
                    selectValueType: selectValueType,
                    width: 900,
                    height: 400
                });
            }
        } else {
            View.showMessage(getMessage("noFieldName"));
        }
    },
    createSelectOptions: function (field, enumList) {
        var splitList = enumList.split(";");
        var items = splitList.length / 2 - splitList.length % 2;
        for (var i = 0; i < items; i++) {
            field.addOption(splitList[i * 2], splitList[i * 2 + 1]);
        }
    },
    adjustFieldValue: function (show) {
        var panel = View.panels.get('restSelectPanel');
        panel.setFieldValue('textFieldValue', '');
        var operator = panel.getFieldValue('operator');
        if (this.checkNullValue()) {
            panel.showField('enumFieldValue', false);
            panel.showField('textFieldValue', true);
            panel.enableField('textFieldValue', false);
            panel.setFieldValue('textFieldValue', '');
        } else {
            panel.enableField('textFieldValue', true);
            panel.showField('enumFieldValue', show);
            panel.showField('textFieldValue', !show);
            if (show) {
                jQuery(panel.getFieldElement('enumFieldValue')).prop('multiple', this.isMultipleSelection());
            }
        }
    },
    isMultipleSelection: function () {
        var panel = View.panels.get('restSelectPanel');
        var operator = panel.getFieldValue('operator');
        return ('IN' === operator || 'NOT IN' === operator);
    },
    checkNullValue: function () {
        var panel = View.panels.get('restSelectPanel');
        var operator = panel.getFieldValue('operator');
        return ('IS NULL' === operator || 'IS NOT NULL' === operator);
    },
    getFieldsRestriction: function () {
        var restriction = '';
        var fields = this.viewObj.tableGroups[this.panelIndex].fields;
        for (var i = 0; i < fields.length; i++) {
            var tableName = fields[i].table_name,
                fieldName = fields[i].field_name;
            if (i != 0) {
                restriction += " OR ";
            }
            restriction += "(afm_flds.table_name = '" + tableName + "' AND afm_flds.field_name = '" + fieldName + "')";
        }
        return restriction;
    },
    adjustOperator: function (dataType) {
        this.restSelectPanel.setFieldValue('operator', '=');
        var operatorEl = this.restSelectPanel.getFieldElement('operator');
        var index, option;
        for (index = 0; index < operatorEl.options.length; index++) {
            operatorEl.options[index].disabled = false;
        }
        switch (dataType) {
            case 'Date':
                for (index = 0; index < operatorEl.options.length; index++) {
                    option = operatorEl.options[index];
                    if (option.value == 'IN' || option.value == 'NOT IN' || option.value == 'LIKE' || option.value == 'NOT LIKE') {
                        option.disabled = true;
                    }
                }
                break;
            case 'Numeric':
                for (index = 0; index < operatorEl.options.length; index++) {
                    option = operatorEl.options[index];
                    if (option.value == 'IN' || option.value == 'NOT IN' || option.value == 'LIKE' || option.value == 'NOT LIKE') {
                        option.disabled = true;
                    }
                }
                break;
            default:
                for (index = 0; index < operatorEl.options.length; index++) {
                    option = operatorEl.options[index];
                    if (option.value == '>' || option.value == '<' || option.value == '>=' || option.value == '<=') {
                        option.disabled = true;
                    }
                }
        }
    },
    checkOperatorValue: function (value) {
        this.restSelectPanel.setFieldValue('textFieldValue', '');
        this.adjustFieldValue(valueExistsNotEmpty(this.selectedField.enumList));
    },
    createRestriction: function () {
        var selectedField = this.selectedField;
        if (_.isEmpty(selectedField)) {
            View.showMessage(getMessage("noFieldName"));
            return;
        }
        var panel = View.panels.get('restSelectPanel');
        var isEnum = valueExistsNotEmpty(selectedField.enumList),
            isMultipleSelection = this.isMultipleSelection();
        var fieldValue = '',
            textFieldValue = panel.getFieldValue('textFieldValue'),
            enumFieldValue = panel.getFieldValue('enumFieldValue');
        if (isEnum) {
            fieldValue = enumFieldValue;
            if (isMultipleSelection) {
                var multipleSelectValues = jQuery(panel.getFieldElement('enumFieldValue')).val();
                fieldValue = multipleSelectValues.join();
            }
        } else {
            fieldValue = textFieldValue;
        }
        if (!valueExistsNotEmpty(fieldValue) && !this.checkNullValue()) {
            View.showMessage(getMessage("noFieldValue"));
            return;
        }
        var conjunction = panel.getFieldValue('conjunction');
        var operator = panel.getFieldValue('operator');
        var restriction = {
            table_name: selectedField['tableName'],
            field_name: selectedField['fieldName'],
            relop: conjunction,
            op: operator,
            value: fieldValue
        };
        this.addRestriction(restriction);
    },
    addRestriction: function (restriction) {
        this.restrictions.push(restriction);
        this.generateRestrictions();
    },
    generateRestrictions: function () {
        this.clearRestrictions();
        for (var index = 0; index < this.restrictions.length; index++) {
            this.generateRestriction(index, this.restrictions[index]);
        }
    },
    generateRestriction: function (index, restriction) {
        var generateRestrictionTemplate = function (el, index, restriction) {
            var generatePanelTemplate = View.templates.get('restrictionTemplate');
            generatePanelTemplate.render({
                    index: index,
                    restriction: restriction
                },
                el,
                'after');
        };
        generateRestrictionTemplate(jQuery('#restrictions')[0], index, restriction);
    },
    removeRestriction: function (index) {
        this.restrictions.splice(Number(index), 1);
        this.generateRestrictions();
    },
    clearRestrictions: function (reset) {
        jQuery('#restrictions').empty();
        if (reset) {
            this.restrictions = [];
        }
    },
    saveRestrictions: function () {
        if (this.restrictions.length == 0) {
            View.showMessage(getMessage('noRestrictionsFound'));
            return;
        }
        this.viewObj.tableGroups[this.panelIndex].parsedRestrictionClauses = this.restrictions;
        var openerViewControl = View.getOpenerView().controllers.get('viewDefControl');
        openerViewControl.markPanel(this.panelIndex);
        View.closeThisDialog();
    }
});