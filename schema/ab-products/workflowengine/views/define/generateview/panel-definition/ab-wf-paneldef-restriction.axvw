<view version="2.0">
    <layout id="mainLayout">
        <north id="restrictionSelectRegion" initialSize="40%" split="true"/>
        <center id="restrictionRegion"/>
    </layout>

    <message name="remove" translatable="true">Remove</message>
    <message name="up" translatable="true">Up</message>
    <message name="dn" translatable="true">Down</message>
    <message name="fieldNameSelectValueTitle" translatable="true">Field Name</message>
    <message name="noField" translatable="true">Please select required fields.</message>
    <message name="noFieldName" translatable="true">Please select field.</message>
    <message name="noFieldValue" translatable="true">Please enter or select a field value.</message>
    <message name="noRestrictionsFound" translatable="true">Cannot save, no restriction was created.</message>

    <panel type="view" id="restrictionTemplate" file="ab-wf-viewdef-restriction-template.axvw"/>
    <panel type="form" id="restSelectPanel" dataSource="none" columns="5" layoutRegion="restrictionSelectRegion">
        <title translatable="true">Set Restriction</title>
        <instructions>In order to create a restriction, please select a field and then the field value and press the [b]Add[/b] button.[br][li]When the selected field has a reference table defined, you can select values from that table[/li][li]When the selected field has enumeration list defined, you can select those list values from the select box[/li][li]When the operator value is IN or NOT IN, you can select multiple field values[/li]</instructions>
        <action id="add">
            <title translatable="true">Add</title>
            <command type="callFunction" functionName="View.controllers.get('wfRestrictionController').createRestriction()"/>
        </action>
        <action id="save" mainAction="true">
            <title translatable="true">Save</title>
            <command type="callFunction" functionName="View.controllers.get('wfRestrictionController').saveRestrictions()"/>
        </action>
        <action id="close">
            <title translatable="true">Close</title>
            <command type="closeDialog"/>
        </action>
        <field id="conjunction" controlType="comboBox">
            <title translatable="true">Conjunction</title>
            <option value="AND">
                <span translatable="true">AND</span>
            </option>
            <option value="OR">
                <span translatable="true">OR</span>
            </option>
            <option value=")AND(">
                <span translatable="true">)AND(</span>
            </option>
            <option value=")OR(">
                <span translatable="true">)OR(</span>
            </option>
        </field>
        <field id="mlHeading" dataType="text" size="200" onfocus="blur()">
            <title translatable="true">Field</title>
            <action>
                <title>...</title>
                <command type="callFunction" functionName="View.controllers.get('wfRestrictionController').selectField()"/>
            </action>
        </field>
        <field id="operator" controlType="comboBox" onchange="View.controllers.get('wfRestrictionController').checkOperatorValue(this.value)">
            <title translatable="true">Operator</title>
            <option translatable="false" value="=">=</option>
            <option translatable="false" value="&gt;">&gt;</option>
            <option translatable="false" value="&lt;">&lt;</option>
            <option translatable="false" value="&gt;=">&gt;=</option>
            <option translatable="false" value="&lt;=">&lt;=</option>
            <option translatable="false" value="!=">!=</option>
            <option translatable="true" value="LIKE">LIKE</option>
            <option translatable="true" value="NOT LIKE">NOT LIKE</option>
            <option translatable="true" value="IS NULL">IS NULL</option>
            <option translatable="true" value="IS NOT NULL">IS NOT NULL</option>
            <option translatable="true" value="IN">IN</option>
            <option translatable="true" value="NOT IN">NOT IN</option>
        </field>
        <field id="textFieldValue" dataType="text" size="200">
            <title translatable="true">Field Value</title>
            <action>
                <title>...</title>
                <command type="callFunction" functionName="View.controllers.get('wfRestrictionController').selectFieldValue()"/>
            </action>
        </field>
        <field id="enumFieldValue" controlType="comboBox">
            <title translatable="true">Field Value</title>
        </field>
    </panel>

    <panel type="html" id="restrictionsPanel" layoutRegion="restrictionRegion">
        <action id="reset">
            <title translatable="true">Reset</title>
            <command type="callFunction" functionName="View.controllers.get('wfRestrictionController').clearRestrictions(true)"/>
        </action>
        <html>
            <table id="restrictionSummary" class="panelReport">
                <thead>
                    <tr>
                        <th class="headerTitleText conjunction" translatable="true">Conjunction</th>
                        <th class="headerTitleText field" translatable="true">Field</th>
                        <th class="headerTitleText operator" translatable="true">Operator</th>
                        <th class="headerTitleText value" translatable="true">Value</th>
                        <th class="headerTitleText action">&#160;</th>
                    </tr>
                </thead>
            </table>
            <div class="panelReport">
                <table id="restrictionsTable" class="panelReport">
                    <tbody id="restrictions">&#160;</tbody>
                </table>
            </div>
        </html>
    </panel>

    <js file="ab-wf-paneldef-restriction.js"/>
</view>