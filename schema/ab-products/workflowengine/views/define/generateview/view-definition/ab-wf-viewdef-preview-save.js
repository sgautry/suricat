View.createController('wfSaveViewController', {
    viewDefController: null,
    afterViewLoad: function () {
        this.viewDefController = View.parameters.viewDefController;
    },
    afterInitialDataFetch: function () {
        this.loadSave();
    },
    loadSave: function () {
        var form = View.panels.get('saveViewPanel');
        form.setFieldValue('activity_id', this.viewDefController.config.activityId);
        form.setFieldValue('folderPath', this.viewDefController.folderPath || 'manager');
        var uniqueFileName = this.viewDefController.uniqueFileName || '';
        uniqueFileName = uniqueFileName.split('_')[0] + ".axvw";
        form.setFieldValue("fileName", uniqueFileName);
    },
    saveView: function () {
        var form = View.panels.get('saveViewPanel');
        var activityId = form.getFieldValue('activity_id');
        if (form.canSave()) {
            // otherwise
            var pattern = String(this.viewDefController.patternRestriction);
            var newFileName = form.getFieldValue('fileName');
            var textFileName = form.getFieldValue('fileName');
            var ext = textFileName.substring(textFileName.length - 5, textFileName.length);
            var regExObj = /[^\w|\.|\-]+/gi;
            if (ext.toLowerCase() != ".axvw") {
                View.showMessage(getMessage("wrongExt"));
            } else if (textFileName.match(regExObj)) {
                View.showMessage(getMessage("invalidFileName"));
            } else {
                // save primary view
                var uniqueFileName = this.viewDefController.uniqueFileName;
                var patternCamelCase = this.viewDefController.constructor.convertToCamelCase(pattern);
                var newFileNameCamelCase = this.viewDefController.constructor.convertToCamelCase(newFileName);
                var customFolderPath = form.getFieldValue('folderPath').replace(/\\/g, "\\\\");
                var parameters = {
                    'oldfileName': uniqueFileName + ".axvw",
                    'newfileName': newFileName,
                    'activity': activityId,
                    'process': 'wf-steps',
                    'patternCamelCase': patternCamelCase,
                    'newFileNameCamelCase': newFileNameCamelCase,
                    'customFolderPath': customFolderPath
                };
                try {
                    var result = Ab.workflow.Workflow.runRuleAndReturnResult('WorkflowEngine-WorkflowEngineService-moveAndRenameFile', parameters);
                    if (result.code == "executed") {
                        var message = getMessage('newFileSaved') + result.message;
                        var newViewPath = message;
                        this.viewDefController.textFileName = form.getFieldValue('fileName');
                        View.showMessage(newViewPath);
                        View.alert(newViewPath, function () {
                            View.parameters.callback(newFileName);
                        });
                    } else {
                        View.showMessage(result.message);
                    }
                } catch (e) {
                    Workflow.handleError(e);
                }
            }
        }
    }
});