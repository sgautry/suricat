View.createController('wfPreviewControl', {
    viewDefController: null,
    viewExt: ".axvw",
    afterViewLoad: function () {
        this.viewDefController = View.parameters.viewDefController;
        this.viewDefController.previewController = this;
    },
    afterInitialDataFetch: function () {
        this.onLoadPreview();
    },
    onLoadPreview: function () {
        // delete old temp file
        this.deletePrimaryTempFile();
        // if a file needs to be converted, convert first.  otherwise, it is a new view, so directly process it.
        this.viewDefController.processView();
    },
    deletePrimaryTempFile: function () {
        if (valueExistsNotEmpty(this.viewDefController.uniqueFileName)) {
            try {
                Workflow.runRuleAndReturnResult('AbSystemAdministration-deleteFile', {
                    'fileName': this.viewDefController.uniqueFileName + this.viewExt
                });
            } catch (e) {
                Workflow.handleError(e);
            }
        }
    },
    showFileContent: function (fileToConvert, convertedFileName, convertedFileContents, convertedFilePath) {
        if (this.setPreviewPanel(fileToConvert, convertedFileName, convertedFileContents, convertedFilePath)) {
            this.setSavePanel(fileToConvert);
        }
    },
    setPreviewPanel: function (fileToConvert, convertedFileName, convertedFileContents, convertedFilePath) {
        // if a file was converted, display file path.  if not, leave empty
        jQuery('#fileToConvert').text(fileToConvert);
        jQuery('#fileConverted').text(convertedFilePath);
        jQuery('#editFileConverted').text(convertedFilePath);
        jQuery("#displayView").attr('onclick', 'View.controllers.get(\'wfPreviewControl\').displayView(' + "'" + convertedFileName + ".axvw'" + ')');
        jQuery('#convertedFileContents').text(convertedFileContents);
        return valueExistsNotEmpty(convertedFileName);
    },
    setSavePanel: function (fileToConvert) {
        var panel = View.panels.get('saveViewPanel');
        panel.setFieldValue('activity_id', this.viewDefController.config.activityId);
        panel.setFieldValue('folderPath', this.viewDefController.config.folderPath || 'manager');
        var fileName = this.viewDefController.uniqueFileName || '';
        fileName = fileName.split('_')[0];
        if (valueExistsNotEmpty(fileToConvert)) {
            fileName = fileToConvert.split(this.viewExt)[0];
        }
        jQuery("#fileName").val(fileName);
        jQuery("#extension").text(this.viewExt);
        panel.show(true);
    },
    editView: function (buttonEl) {
        this.editorViewPanel.showInWindow({
            anchor: buttonEl,
            width: 800,
            height: 600
        });
        var convertedFileContentsEl = jQuery('#convertedFileContents');
        convertedFileContentsEl.height(this.editorViewPanel.determineAvailableHeight() - 32);
        convertedFileContentsEl.css('width', '100%');
        this.editorViewPanel.updateHeight();
    },
    displayView: function (fileName) {
        var fileExt = fileName.substring((fileName.length - 5), (fileName.length));
        if (fileExt.toUpperCase() == ".AXVW") {
            View.openDialog(fileName, null, true, {
                wfParams: {
                    wfType: this.viewDefController.config.wfType,
                    blockId: this.viewDefController.config.blockId,
                    tableName: this.viewDefController.mainTable,
                    edit: true,
                    preview: true
                }
            });
        } else {
            alert(getMessage("onlyAXVW"));
        }
    },
    saveChanges: function () {
        this.viewDefController.saveViewEditor(jQuery('#convertedFileContents').val());
        this.editorViewPanel.closeWindow();
    },
    saveView: function () {
        var form = View.panels.get('saveViewPanel');
        var activityId = form.getFieldValue('activity_id');
        if (form.canSave()) {
            // otherwise
            var newFileName = jQuery("#fileName").val();
            var regExObj = /^[a-zA-Z0-9_\-]+$/gi; // only alphanumeric underscore and hyphen
            if (newFileName.match(regExObj)) {
                // save primary view
                var uniqueFileName = this.viewDefController.uniqueFileName;
                var folderPath = form.getFieldValue('folderPath').replace(/\\/g, "\\\\");
                var parameters = {
                    'oldfileName': uniqueFileName + this.viewExt,
                    'newfileName': newFileName + this.viewExt,
                    'activity': activityId,
                    'folderPath': folderPath
                };
                try {
                    var result = Ab.workflow.Workflow.runRuleAndReturnResult('WorkflowEngine-WorkflowEngineService-moveAndRenameFile', parameters);
                    if (result.code == "executed") {
                        var message = String.format(getMessage('newFileSaved'), result.message);
                        newFileName = result.value;
                        if (!View.isDevelopmentMode ) {
                            this.preloadView(newFileName);
                        }
                        this.viewDefController.fileName = newFileName;
                        View.alert(message, function () {
                            View.parameters.callback(newFileName);
                        });
                    } else {
                        View.showMessage(result.message);
                    }
                } catch (e) {
                    Workflow.handleError(e);
                }
            } else {
                View.showMessage(getMessage("invalidFileName"));
            }
        }
    },
    preloadView: function (viewName) {
        if (valueExists(viewName)) {
            try {
                Workflow.callMethod('AbSystemAdministration-ConfigHandlers-preloadView', viewName);
            } catch (e) {
                Workflow.handleError(e);
            }
        }
    }
});