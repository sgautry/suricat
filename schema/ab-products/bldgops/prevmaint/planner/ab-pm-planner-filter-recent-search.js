/**
 * Controller for the PM Planner Filter Recent Search
 */
var pmPlannerRecentSearchController = View.createController('pmPlannerRecentSearchController', {

	/**
	 * Recent munu Ext.menu.Menu object
	 */
	recentMenu : null,

	/**
	 * When the 'Recent Search Menu' button is pressed in the Filter Panel
	 */
	pmSchedulesFilter_onRecentSearchMenu : function() {
		this.displayRecentSearches();
	},

	/**
	 * Displays the recent searches list.
	 */
	displayRecentSearches : function() {
		var controller = this;

		if (!this.recentMenu) {
			this.recentMenu = new Ext.menu.Menu({ autoWidth: function() {
	            // KB 3043295, 3041580: override Ext.menu.Menu method that does not work well on IE
	            var el = this.el;
	            if (!el) {
	                return;
	            }
	            var w = this.width;
	            if (w) {
	                el.setWidth(w);
	            }
	        } });
		}
		this.recentMenu.removeAll();

		// add recent items from side car
		var sidecar = this.pmSchedulesFilter.getSidecar();
		_.each(sidecar.get('searchValues'), function(oriSearchValue, index) {
			var searchValue = [];
			
			var fieldNames = sidecar.get('searchFieldNames')[index];
			for(var i=0;i<fieldNames.length;i++){
				searchValue.push(_.clone(oriSearchValue[i]));
			}
				
			var text = searchValue.toString().replace(',,',',');
			controller.recentMenu.addItem(new Ext.menu.Item({
				text : text,
				listeners : {
					click : controller.onSelectRecentSearch.createDelegate(controller, [ index ])
				}
			}));
		});

		//KB3050063 - An extra symbol is shown next to 'Recent' button when there is no value in the dropdown list.
		jQuery('.x-shadow').hide();
		jQuery('.x-menu').hide();
		if (this.recentMenu.items.length > 0) {
			this.recentMenu.show(Ext.get('recentSearchMenu'),
					'tl-bl?');
		}
		
	},

	/**
	 * Adds new search to the recent searches sidecar.
	 */
	addRecentSearch : function() {
		// get current search field names and values
		var search = this.getCurrentSearch();

		// add current search to side car
		this.addSearchToSideCar(search);

	},

	/**
	 * get selected field names and values in the filter
	 */
	getCurrentSearch : function() {
		// all the fields in the filter
		var allFieldNames = pmPlannerFilterController.filterFields;

		// get not empty field name and values
		var searchFieldNames = [];
		var searchValues = [];
		var controller = this;
		_.each(allFieldNames, function(fieldName) {
			var fieldValue = controller.pmSchedulesFilter.getFieldValue(fieldName);
			
			if (!valueExistsNotEmpty(fieldValue)) {
				fieldValue = controller.optionalFilterPanel.getFieldValue(fieldName);
			}
			
			if (valueExistsNotEmpty(fieldValue)) {
				searchFieldNames.push(fieldName);
				searchValues.push(fieldValue);
			}
		});

		var search = {};
		search['searchFieldNames'] = searchFieldNames;
		search['searchValues'] = searchValues;

		return search;
	},

	/**
	 * Add given search to side car
	 */
	addSearchToSideCar : function(search) {
		// if filter is not empty, add it to the side car
		if (search.searchFieldNames.length > 0) {
			// side cart
			var sidecar = this.pmSchedulesFilter.getSidecar();

			// get search field names and values stored in the side car
			var searchFieldNames = valueExistsNotEmpty(sidecar.get('searchFieldNames')) ? sidecar.get('searchFieldNames') : [];
			var searchValues = valueExistsNotEmpty(sidecar.get('searchValues')) ? sidecar.get('searchValues') : [];

			// temp array used for eliminate duplicates and re-order
			var newSearchFieldNames = [];
			var newSearchValues = [];

			// eliminate duplicates from this list.
			for ( var i = 0; i < searchFieldNames.length; i++) {
				if (JSON.stringify(search.searchFieldNames) != JSON.stringify(searchFieldNames[i]) || JSON.stringify(search.searchValues) != JSON.stringify(searchValues[i])) {
					newSearchFieldNames.push(searchFieldNames[i]);
					newSearchValues.push(searchValues[i]);
				}
			}

			// put the most recent searches at the top of the list
			newSearchFieldNames.unshift(search.searchFieldNames);
			newSearchValues.unshift(search.searchValues);

			// save the new search the side car
			sidecar.set('searchFieldNames', newSearchFieldNames);
			sidecar.set('searchValues', newSearchValues);
			sidecar.save();
		}
	},

	/**
	 * Selects a recent search.
	 * 
	 * @param search
	 *            Ab.view.Restriction.
	 */
	onSelectRecentSearch : function(index) {
		// clear filter values
		pmPlannerFilterController.clearFilterValues();

		// set selected recent search
		this.setSelectedRecentSearch(index);

		// refresh the PM Planner control
		pmPlannerFilterController.pmSchedulesFilter_onFilter();
	},

	/**
	 * Set the selected recent search values in the filter
	 */
	setSelectedRecentSearch : function(index) {
		// get stored search field names and field values
		var sidecar = this.pmSchedulesFilter.getSidecar();
		var searchFieldNames = sidecar.get('searchFieldNames')[index];
		var searchValues = sidecar.get('searchValues')[index];

		// set the values in the filter
		var controller = this;
		_.each(searchFieldNames, function(fieldName, i) {
			controller.pmSchedulesFilter.setFieldValue(fieldName, searchValues[i], null, false);
			controller.optionalFilterPanel.setFieldValue(fieldName, searchValues[i], null, false);
		});
	}
});