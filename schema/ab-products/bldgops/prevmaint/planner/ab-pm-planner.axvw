<!-- The master view for the PM Planner application.
     Functional specification: https://confluence.archibus.zone/display/AP/View+Specification+-+PM+Planner. -->
<view version="2.0">
    <title>PM Planner</title>

    <!--  Additional third-part libraries for the Bali6 grid control. -->
    <js file="dx.archibus.js"/>
    <css file="dx.common.css"/>
    <css file="dx.light.compact.css"/>
    <css file="ab-pm-planner.css"/>

    <!--  Bali6 grid control. -->
    <js file="ab-grid-control-grid-rows-selection.js"/>
    <js file="ab-grid-control-column-configurator.js"/>
    <js file="ab-grid-control-configurator.js"/>
    <js file="ab-grid-control-datastore.js"/>
    <js file="ab-grid-control.js"/>

    <!--  Bali6 scheduler control. -->
    <js file="ab-pm-planner-view.js"/>
    <js file="ab-pm-planner-view-tools.js"/>
    <js file="ab-pm-planner-view-grouping.js"/>
    <js file="ab-pm-planner-view-calendar.js"/>
    <js file="ab-pm-planner-view-calendar-columns.js"/>
    <js file="ab-pm-planner-view-calendar-week.js"/>
    <js file="ab-pm-planner-view-cost-information.js"/>
    <js file="ab-pm-planner-cost-labor-ds.js"/>
    <js file="ab-bldgops-labor-scheduler-timeline-model.js"/>
    <js file="ab-pm-planner-labor-analysis-capacity-model.js"/>

    <js file="ab-pm-planner-control.js"/>

    <!-- Code that creates the scheduler control. -->
    <js file="ab-pm-planner.js"/>

    <js file="ab-common-recurring-pattern.js"/>

    <layout type="borderLayout" id="mainLayout">
        <north id="filterLayout" initialSize="50" split="false"/>
        <center id="gridRegion" autoScroll="true"/>
    </layout>
    
    <!-- Cost and Labor DataSources-->
    <panel type="view" id="costLaborDataSources" file="ab-pm-planner-cost-labor-ds.axvw"/>
    
    <!-- Data source for the grid control. -->
    <dataSource id="exportDataDS">

          <sql>
                SELECT pms.pms_id,pms.pmp_id,pms.eq_id,pms.site_id,pms.bl_id,pms.fl_id,pms.rm_id,pms.dv_id,pms.dp_id,pms.pm_group,pms.sla_work_team_id,pms.criticality,
                pms.int1_recurrence,pms.int2_recurrence,pms.int3_recurrence,pms.int4_recurrence,pms.interval_1,pms.interval_2,pms.interval_3,pms.interval_4,pms.interval_freq,pms.interval_type,
                pms.date_last_completed,pms.date_first_todo,pmp.tr_id, eq.eq_std as eq_std, pms.reg_program, regprogram.vn_id as vn_id,
                RTRIM(pms.bl_id)${sql.concat}'-'${sql.concat}RTRIM(pms.fl_id) ${sql.concat}'-'${sql.concat}RTRIM(pms.rm_id) as vf_location,
                '' as vf_interval,
                case when ${parameters['groupField']} IS NULL THEN '(N/A)' ELSE  ${parameters['groupField']} END as groupValue,
                '' as totalLaborText,
                0 as week_1, 0 as week_2, 0 as week_3, 0 as week_4, 0 as week_5, 0 as week_6, 0 as week_7, 0 as week_8, 0 as week_9, 0 as week_10,
                0 as week_11, 0 as week_12, 0 as week_13, 0 as week_14, 0 as week_15, 0 as week_16, 0 as week_17, 0 as week_18, 0 as week_19, 0 as week_20,
                0 as week_21, 0 as week_22, 0 as week_23, 0 as week_24, 0 as week_25, 0 as week_26, 0 as week_27, 0 as week_28, 0 as week_29, 0 as week_30,
                0 as week_31, 0 as week_32, 0 as week_33, 0 as week_34, 0 as week_35, 0 as week_36, 0 as week_37, 0 as week_38, 0 as week_39, 0 as week_40,
                0 as week_41, 0 as week_42, 0 as week_43, 0 as week_44, 0 as week_45, 0 as week_46, 0 as week_47, 0 as week_48, 0 as week_49, 0 as week_50,
                0 as week_51, 0 as week_52, 0 as week_53
                FROM pms
                LEFT OUTER JOIN eq ON pms.eq_id = eq.eq_id
                LEFT OUTER JOIN pmp ON pms.pmp_id = pmp.pmp_id
                LEFT OUTER JOIN regprogram ON pms.reg_program = regprogram.reg_program and pms.regulation = regprogram.regulation
                WHERE ${sql.getRestriction('ab-pm-planner-shared-restriction.axvw', 'sharedPmsDS', 'consoleRestriction')}

            </sql>

            <table name="pms" role="main"/>

            <field table="pms" name="pms_id" dataType="integer"/>
            <field table="pms" name="pmp_id" dataType="text"/>
            <field table="pms" name="eq_id" dataType="text"/>
            <field table="pms" name="site_id" dataType="text"/>
            <field table="pms" name="bl_id" dataType="text"/>
            <field table="pms" name="fl_id" dataType="text"/>
            <field table="pms" name="rm_id" dataType="text"/>
            <field table="pms" name="dv_id" dataType="text"/>
            <field table="pms" name="dp_id" dataType="text"/>
            <field table="pms" name="pm_group" dataType="text"/>
            <field table="pms" name="sla_work_team_id" dataType="text"/>
            <field table="pms" name="criticality" dataType="text"/>

            <field table="pms" name="int1_recurrence" dataType="text"/>
            <field table="pms" name="int2_recurrence" dataType="text"/>
            <field table="pms" name="int3_recurrence" dataType="text"/>
            <field table="pms" name="int4_recurrence" dataType="text"/>
            <field table="pms" name="interval_1" dataType="integer"/>
            <field table="pms" name="interval_2" dataType="integer"/>
            <field table="pms" name="interval_3" dataType="integer"/>
            <field table="pms" name="interval_4" dataType="integer"/>
            <field table="pms" name="interval_freq" dataType="integer"/>
            <field table="pms" name="interval_type" dataType="text"/>
            <field table="pms" name="date_last_completed" dataType="text"/>
            <field table="pms" name="date_first_todo" dataType="text"/>
            <field name="tr_id" dataType="text"/>
            <field name="eq_std" dataType="text"/>
            <field name="tr_id" dataType="text"/>
            <field name="reg_program" dataType="text"/>
            <field name="vn_id" dataType="text"/>

            <field name="week_1" dataType="number"/>
            <field name="week_2" dataType="number"/>
            <field name="week_3" dataType="number"/>
            <field name="week_4" dataType="number"/>
            <field name="week_5" dataType="number"/>
            <field name="week_6" dataType="number"/>
            <field name="week_7" dataType="number"/>
            <field name="week_8" dataType="number"/>
            <field name="week_9" dataType="number"/>
            <field name="week_10" dataType="number"/>
            <field name="week_11" dataType="number"/>
            <field name="week_12" dataType="number"/>
            <field name="week_13" dataType="number"/>
            <field name="week_14" dataType="number"/>
            <field name="week_15" dataType="number"/>
            <field name="week_16" dataType="number"/>
            <field name="week_17" dataType="number"/>
            <field name="week_18" dataType="number"/>
            <field name="week_19" dataType="number"/>
            <field name="week_20" dataType="number"/>
            <field name="week_21" dataType="number"/>
            <field name="week_22" dataType="number"/>
            <field name="week_23" dataType="number"/>
            <field name="week_24" dataType="number"/>
            <field name="week_25" dataType="number"/>
            <field name="week_26" dataType="number"/>
            <field name="week_27" dataType="number"/>
            <field name="week_28" dataType="number"/>
            <field name="week_29" dataType="number"/>
            <field name="week_30" dataType="number"/>
            <field name="week_31" dataType="number"/>
            <field name="week_32" dataType="number"/>
            <field name="week_33" dataType="number"/>
            <field name="week_34" dataType="number"/>
            <field name="week_35" dataType="number"/>
            <field name="week_36" dataType="number"/>
            <field name="week_37" dataType="number"/>
            <field name="week_38" dataType="number"/>
            <field name="week_39" dataType="number"/>
            <field name="week_40" dataType="number"/>
            <field name="week_41" dataType="number"/>
            <field name="week_42" dataType="number"/>
            <field name="week_43" dataType="number"/>
            <field name="week_44" dataType="number"/>
            <field name="week_45" dataType="number"/>
            <field name="week_46" dataType="number"/>
            <field name="week_47" dataType="number"/>
            <field name="week_48" dataType="number"/>
            <field name="week_49" dataType="number"/>
            <field name="week_50" dataType="number"/>
            <field name="week_51" dataType="number"/>
            <field name="week_52" dataType="number"/>
            <field name="week_53" dataType="number"/>
            <field name="groupValue" dataType="text"/>
            <field name="totalLaborText" dataType="text"/>
            <field name="vf_location" dataType="text"/>
            <field name="vf_interval" dataType="text"/>
            <parameter name="groupField" dataType="verbatim" value="eq.eq_std"/>
    </dataSource>
    
    <!-- Data source for the grid control. -->
    <dataSource id="exportDataByTradeDS">

          <sql>
                SELECT pms.pms_id,pms.pmp_id,pms.eq_id,pms.site_id,pms.bl_id,pms.fl_id,pms.rm_id,pms.dv_id,pms.dp_id,pms.pm_group,pms.sla_work_team_id,pms.criticality,
                pms.int1_recurrence,pms.int2_recurrence,pms.int3_recurrence,pms.int4_recurrence,pms.interval_1,pms.interval_2,pms.interval_3,pms.interval_4,pms.interval_freq,pms.interval_type,
                pms.date_last_completed,pms.date_first_todo,pmp.tr_id, eq.eq_std as eq_std,
                RTRIM(pms.bl_id)${sql.concat}'-'${sql.concat}RTRIM(pms.fl_id) ${sql.concat}'-'${sql.concat}RTRIM(pms.rm_id) as vf_location,
                '' as vf_interval,
                case when pmp.tr_id IS NULL THEN '(N/A)' ELSE  pmp.tr_id END as groupValue,
                '' as totalLaborText,
                0 as week_1, 0 as week_2, 0 as week_3, 0 as week_4, 0 as week_5, 0 as week_6, 0 as week_7, 0 as week_8, 0 as week_9, 0 as week_10,
                0 as week_11, 0 as week_12, 0 as week_13, 0 as week_14, 0 as week_15, 0 as week_16, 0 as week_17, 0 as week_18, 0 as week_19, 0 as week_20,
                0 as week_21, 0 as week_22, 0 as week_23, 0 as week_24, 0 as week_25, 0 as week_26, 0 as week_27, 0 as week_28, 0 as week_29, 0 as week_30,
                0 as week_31, 0 as week_32, 0 as week_33, 0 as week_34, 0 as week_35, 0 as week_36, 0 as week_37, 0 as week_38, 0 as week_39, 0 as week_40,
                0 as week_41, 0 as week_42, 0 as week_43, 0 as week_44, 0 as week_45, 0 as week_46, 0 as week_47, 0 as week_48, 0 as week_49, 0 as week_50,
                0 as week_51, 0 as week_52, 0 as week_53
                FROM pms
                LEFT OUTER JOIN eq ON pms.eq_id = eq.eq_id
                LEFT OUTER JOIN pmp ON pms.pmp_id = pmp.pmp_id
                WHERE (pmp.tr_id IS NOT NULL OR (pmp.tr_id IS NULL AND NOT EXISTS(SELECT 1 FROM pmpstr WHERE pmpstr.pmp_id = pmp.pmp_id)) ) AND 
                ${sql.getRestriction('ab-pm-planner-shared-restriction.axvw', 'sharedPmsDS', 'consoleRestrictionWithoutTrade')} AND ${sql.getRestriction('pmp.tr_id','1=1')}
                
               union
            
	           select pms.pms_id,pms.pmp_id,pms.eq_id,pms.site_id,pms.bl_id,pms.fl_id,pms.rm_id,pms.dv_id,pms.dp_id,pms.pm_group,pms.sla_work_team_id,pms.criticality,
	           pms.int1_recurrence,pms.int2_recurrence,pms.int3_recurrence,pms.int4_recurrence,pms.interval_1,pms.interval_2,pms.interval_3,pms.interval_4,pms.interval_freq,pms.interval_type,
	           pms.date_last_completed,pms.date_first_todo,pmpstr.tr_id, eq.eq_std as eq_std,
	             RTRIM(pms.bl_id)${sql.concat}'-'${sql.concat}RTRIM(pms.fl_id) ${sql.concat}'-'${sql.concat}RTRIM(pms.rm_id) as vf_location,
                '' as vf_interval,
                pmpstr.tr_id as groupValue,
                '' as totalLaborText,
                0 as week_1, 0 as week_2, 0 as week_3, 0 as week_4, 0 as week_5, 0 as week_6, 0 as week_7, 0 as week_8, 0 as week_9, 0 as week_10,
                0 as week_11, 0 as week_12, 0 as week_13, 0 as week_14, 0 as week_15, 0 as week_16, 0 as week_17, 0 as week_18, 0 as week_19, 0 as week_20,
                0 as week_21, 0 as week_22, 0 as week_23, 0 as week_24, 0 as week_25, 0 as week_26, 0 as week_27, 0 as week_28, 0 as week_29, 0 as week_30,
                0 as week_31, 0 as week_32, 0 as week_33, 0 as week_34, 0 as week_35, 0 as week_36, 0 as week_37, 0 as week_38, 0 as week_39, 0 as week_40,
                0 as week_41, 0 as week_42, 0 as week_43, 0 as week_44, 0 as week_45, 0 as week_46, 0 as week_47, 0 as week_48, 0 as week_49, 0 as week_50,
                0 as week_51, 0 as week_52, 0 as week_53
	           FROM pms
	           LEFT OUTER JOIN eq ON pms.eq_id = eq.eq_id 
	           LEFT OUTER JOIN pmp ON pmp.pmp_id=pms.pmp_id
	           join pmpstr on pmpstr.pmp_id = pmp.pmp_id
	           where ${sql.getRestriction('ab-pm-planner-shared-restriction.axvw', 'sharedPmsDS', 'consoleRestrictionWithoutTrade')} AND ${sql.getRestriction('pmpstr.tr_id','1=1')}

            </sql>

            <table name="pms" role="main"/>

            <field table="pms" name="pms_id" dataType="integer"/>
            <field table="pms" name="pmp_id" dataType="text"/>
            <field table="pms" name="eq_id" dataType="text"/>
            <field table="pms" name="site_id" dataType="text"/>
            <field table="pms" name="bl_id" dataType="text"/>
            <field table="pms" name="fl_id" dataType="text"/>
            <field table="pms" name="rm_id" dataType="text"/>
            <field table="pms" name="dv_id" dataType="text"/>
            <field table="pms" name="dp_id" dataType="text"/>
            <field table="pms" name="pm_group" dataType="text"/>
            <field table="pms" name="sla_work_team_id" dataType="text"/>
            <field table="pms" name="criticality" dataType="text"/>

            <field table="pms" name="int1_recurrence" dataType="text"/>
            <field table="pms" name="int2_recurrence" dataType="text"/>
            <field table="pms" name="int3_recurrence" dataType="text"/>
            <field table="pms" name="int4_recurrence" dataType="text"/>
            <field table="pms" name="interval_1" dataType="integer"/>
            <field table="pms" name="interval_2" dataType="integer"/>
            <field table="pms" name="interval_3" dataType="integer"/>
            <field table="pms" name="interval_4" dataType="integer"/>
            <field table="pms" name="interval_freq" dataType="integer"/>
            <field table="pms" name="interval_type" dataType="text"/>
            <field table="pms" name="date_last_completed" dataType="text"/>
            <field table="pms" name="date_first_todo" dataType="text"/>
            <field name="tr_id" dataType="text"/>
            <field name="eq_std" dataType="text"/>

            <field name="week_1" dataType="number"/>
            <field name="week_2" dataType="number"/>
            <field name="week_3" dataType="number"/>
            <field name="week_4" dataType="number"/>
            <field name="week_5" dataType="number"/>
            <field name="week_6" dataType="number"/>
            <field name="week_7" dataType="number"/>
            <field name="week_8" dataType="number"/>
            <field name="week_9" dataType="number"/>
            <field name="week_10" dataType="number"/>
            <field name="week_11" dataType="number"/>
            <field name="week_12" dataType="number"/>
            <field name="week_13" dataType="number"/>
            <field name="week_14" dataType="number"/>
            <field name="week_15" dataType="number"/>
            <field name="week_16" dataType="number"/>
            <field name="week_17" dataType="number"/>
            <field name="week_18" dataType="number"/>
            <field name="week_19" dataType="number"/>
            <field name="week_20" dataType="number"/>
            <field name="week_21" dataType="number"/>
            <field name="week_22" dataType="number"/>
            <field name="week_23" dataType="number"/>
            <field name="week_24" dataType="number"/>
            <field name="week_25" dataType="number"/>
            <field name="week_26" dataType="number"/>
            <field name="week_27" dataType="number"/>
            <field name="week_28" dataType="number"/>
            <field name="week_29" dataType="number"/>
            <field name="week_30" dataType="number"/>
            <field name="week_31" dataType="number"/>
            <field name="week_32" dataType="number"/>
            <field name="week_33" dataType="number"/>
            <field name="week_34" dataType="number"/>
            <field name="week_35" dataType="number"/>
            <field name="week_36" dataType="number"/>
            <field name="week_37" dataType="number"/>
            <field name="week_38" dataType="number"/>
            <field name="week_39" dataType="number"/>
            <field name="week_40" dataType="number"/>
            <field name="week_41" dataType="number"/>
            <field name="week_42" dataType="number"/>
            <field name="week_43" dataType="number"/>
            <field name="week_44" dataType="number"/>
            <field name="week_45" dataType="number"/>
            <field name="week_46" dataType="number"/>
            <field name="week_47" dataType="number"/>
            <field name="week_48" dataType="number"/>
            <field name="week_49" dataType="number"/>
            <field name="week_50" dataType="number"/>
            <field name="week_51" dataType="number"/>
            <field name="week_52" dataType="number"/>
            <field name="week_53" dataType="number"/>
            <field name="groupValue" dataType="text"/>
            <field name="totalLaborText" dataType="text"/>
            <field name="vf_location" dataType="text"/>
            <field name="vf_interval" dataType="text"/>
            <parameter name="groupField" dataType="verbatim" value="eq.eq_std"/>
    </dataSource>

    <dataSource id="groupByTradeDataDS">
    
        <sql>
            SELECT pms.pms_id,pms.pmp_id,pms.eq_id,pms.site_id,pms.bl_id,pms.fl_id,pms.rm_id,pms.dv_id,pms.dp_id,pms.pm_group,pms.sla_work_team_id,pms.criticality,
            pms.int1_recurrence,pms.int2_recurrence,pms.int3_recurrence,pms.int4_recurrence,pms.interval_1,pms.interval_2,pms.interval_3,pms.interval_4,pms.interval_freq,pms.interval_type,
            pms.date_last_completed,pms.date_first_todo,pmp.tr_id, eq.eq_std 
            FROM pms
            LEFT OUTER JOIN eq ON pms.eq_id = eq.eq_id
            LEFT OUTER JOIN pmp ON pms.pmp_id = pmp.pmp_id
            where (pmp.tr_id IS NOT NULL OR (pmp.tr_id IS NULL AND NOT EXISTS(SELECT 1 FROM pmpstr WHERE pmpstr.pmp_id = pmp.pmp_id)) ) AND 
            ${sql.getRestriction('ab-pm-planner-shared-restriction.axvw', 'sharedPmsDS', 'consoleRestrictionWithoutTrade')} AND ${sql.getRestriction('pmp.tr_id','1=1')}
            
            union
            
            select pms.pms_id,pms.pmp_id,pms.eq_id,pms.site_id,pms.bl_id,pms.fl_id,pms.rm_id,pms.dv_id,pms.dp_id,pms.pm_group,pms.sla_work_team_id,pms.criticality,
            pms.int1_recurrence,pms.int2_recurrence,pms.int3_recurrence,pms.int4_recurrence,pms.interval_1,pms.interval_2,pms.interval_3,pms.interval_4,pms.interval_freq,pms.interval_type,
            pms.date_last_completed,pms.date_first_todo,pmpstr.tr_id, eq.eq_std 
            FROM pms
            LEFT OUTER JOIN eq ON pms.eq_id = eq.eq_id 
            LEFT OUTER JOIN pmp ON pmp.pmp_id=pms.pmp_id
            join pmpstr on pmpstr.pmp_id = pmp.pmp_id
            where ${sql.getRestriction('ab-pm-planner-shared-restriction.axvw', 'sharedPmsDS', 'consoleRestrictionWithoutTrade')} AND ${sql.getRestriction('pmpstr.tr_id','1=1')}
        </sql>

        <table name="pms" role="main"/>

        <field table="pms" name="pms_id" dataType="integer"/>
        <field table="pms" name="pmp_id" dataType="text"/>
        <field table="pms" name="eq_id" dataType="text"/>
        <field table="pms" name="site_id" dataType="text"/>
        <field table="pms" name="bl_id" dataType="text"/>
        <field table="pms" name="fl_id" dataType="text"/>
        <field table="pms" name="rm_id" dataType="text"/>
        <field table="pms" name="dv_id" dataType="text"/>
        <field table="pms" name="dp_id" dataType="text"/>
        <field table="pms" name="pm_group" dataType="text"/>
        <field table="pms" name="sla_work_team_id" dataType="text"/>
        <field table="pms" name="criticality" dataType="text"/>

        <field table="pms" name="int1_recurrence" dataType="text"/>
        <field table="pms" name="int2_recurrence" dataType="text"/>
        <field table="pms" name="int3_recurrence" dataType="text"/>
        <field table="pms" name="int4_recurrence" dataType="text"/>
        <field table="pms" name="interval_1" dataType="text"/>
        <field table="pms" name="interval_2" dataType="text"/>
        <field table="pms" name="interval_3" dataType="text"/>
        <field table="pms" name="interval_4" dataType="text"/>
        <field table="pms" name="interval_freq" dataType="text"/>
        <field table="pms" name="interval_type" dataType="text"/>
        <field table="pms" name="date_last_completed" dataType="date"/>
        <field table="pms" name="date_first_todo" dataType="date"/>
        <field name="tr_id" dataType="text"/>
        <field table="eq" name="eq_std" dataType="text"/>
        
        <!-- - Filter optional restrictions, enabled by filter values  -->
        <parameter name="pmp.tr_id" dataType="verbatim" value=""/>
        <restriction id="pmp.tr_id" type="sql" enabled="pmp.tr_id" sql="pmp.tr_id IN(${parameters['pmp.tr_id']})"/>
        <restriction id="pmpstr.tr_id" type="sql" enabled="pmp.tr_id" sql="pmpstr.tr_id IN (${parameters['pmp.tr_id']})"/>
    </dataSource>

    <!-- Data source for the grid control. -->
    <dataSource id="dataDS">

        <table name="pms" role="main"/>
        <table name="eq" role="standard"/>
        <table name="pmp" role="standard"/>
        <table name="regprogram" role="standard"/>

        <field table="pms" name="pms_id" dataType="integer"/>
        <field table="pms" name="pmp_id" dataType="text"/>
        <field table="pms" name="eq_id" dataType="text"/>
        <field table="pms" name="site_id" dataType="text"/>
        <field table="pms" name="bl_id" dataType="text"/>
        <field table="pms" name="fl_id" dataType="text"/>
        <field table="pms" name="rm_id" dataType="text"/>
        <field table="pms" name="dv_id" dataType="text"/>
        <field table="pms" name="dp_id" dataType="text"/>
        <field table="pms" name="pm_group" dataType="text"/>
        <field table="pms" name="sla_work_team_id" dataType="text"/>
        <field table="pms" name="sla_servcont_id" dataType="text"/>
        <field table="pms" name="criticality" dataType="text"/>

        <field table="pms" name="int1_recurrence" dataType="text"/>
        <field table="pms" name="int2_recurrence" dataType="text"/>
        <field table="pms" name="int3_recurrence" dataType="text"/>
        <field table="pms" name="int4_recurrence" dataType="text"/>
        <field table="pms" name="interval_1" dataType="text"/>
        <field table="pms" name="interval_2" dataType="text"/>
        <field table="pms" name="interval_3" dataType="text"/>
        <field table="pms" name="interval_4" dataType="text"/>
        <field table="pms" name="interval_freq" dataType="text"/>
        <field table="pms" name="interval_type" dataType="text"/>

        <field table="pms" name="date_last_completed" dataType="date"/>
        <field table="pms" name="date_first_todo" dataType="date"/>

        <field table="eq" name="eq_std"/>
        <field table="pmp" name="tr_id"/>
        <field table="pms" name="reg_program"/>
        <field table="regprogram" name="vn_id"/>
        <field name="none" dataType="text">
           <sql dialect="generic">''</sql>
        </field>
        
        <!-- - Filter optional restrictions, enabled by filter values  -->
        <restriction type="sql" sql="${sql.getRestriction('ab-pm-planner-shared-restriction.axvw', 'sharedPmsDS', 'consoleRestriction')}"/>
    </dataSource>

    <dataSource id="worksDS">
        <sql>
            SELECT pmsd.pms_id,pmsd.date_todo,pmsd.date_completed,pmsd.date_latest,pmsd.date_earliest,pmsd.wo_id,pmsd.is_cancelled,pms.eq_id,pms.bl_id,pms.site_id,eq.eq_std,pmp.tr_id,pms.sla_work_team_id, pmp.pmp_id,
                   pms.fl_id,pms.rm_id,pms.dv_id, pms.dp_id, pms.pm_group, pms.criticality, pms.reg_program, regprogram.vn_id
            FROM pmsd
            LEFT OUTER JOIN pms ON pmsd.pms_id=pms.pms_id
            LEFT OUTER JOIN eq ON pms.eq_id = eq.eq_id
            LEFT OUTER JOIN pmp ON pms.pmp_id = pmp.pmp_id
            LEFT OUTER JOIN regprogram ON pms.reg_program = regprogram.reg_program and pms.regulation = regprogram.regulation
            WHERE ${sql.getRestriction('ab-pm-planner-shared-restriction.axvw', 'sharedPmsDS', 'consoleRestriction')}
        </sql>
        <table name="pmsd" role="main"/>
        <table name="pms" role="standard"/>
        <table name="eq" role="standard"/>
        <field table="pmsd" name="pms_id" />
        <field table="pmsd" name="date_todo" />
        <field table="pmsd" name="date_completed" />
        <field table="pmsd" name="date_latest" />
        <field table="pmsd" name="date_earliest" />
        <field table="pmsd" name="wo_id" />
        <field table="pmsd" name="is_cancelled" />
        <field table="pms" name="pmp_id"/>
        <field table="pms" name="eq_id" />
        <field table="pms" name="bl_id"/>
        <field table="pms" name="site_id"/>
        <field table="pms" name="sla_work_team_id"/>
        <field table="pms" name="reg_program"/>
        <field table="eq" name="eq_std"/>
        <field table="pmp" name="tr_id"/>
        <field table="regprogram" name="vn_id"/>
    </dataSource>
    
    <dataSource id="groupByTradesWorksDS">
        <sql>
            SELECT pmsd.pms_id,pmsd.date_todo,pmsd.date_completed,pmsd.date_latest,pmsd.date_earliest,pmsd.wo_id,pmsd.is_cancelled,pmp.tr_id
            FROM pmsd
            LEFT OUTER JOIN pms ON pmsd.pms_id=pms.pms_id
            LEFT OUTER JOIN pmp ON pms.pmp_id = pmp.pmp_id
            where ${sql.getRestriction('ab-pm-planner-shared-restriction.axvw', 'sharedPmsDS', 'consoleRestrictionWithoutTrade')} AND ${sql.getRestriction('pmp.tr_id','1=1')} 
            
            union
            
            select pmsd.pms_id,pmsd.date_todo,pmsd.date_completed,pmsd.date_latest,pmsd.date_earliest,pmsd.wo_id,pmsd.is_cancelled,pmpstr.tr_id
            FROM pmsd
            LEFT OUTER JOIN pms ON pmsd.pms_id=pms.pms_id
            LEFT OUTER JOIN pmp ON pms.pmp_id = pmp.pmp_id
            join pmpstr on pmpstr.pmp_id = pmp.pmp_id
            where ${sql.getRestriction('ab-pm-planner-shared-restriction.axvw', 'sharedPmsDS', 'consoleRestrictionWithoutTrade')} AND ${sql.getRestriction('pmpstr.tr_id','1=1')}
            
        </sql>
        <table name="pmsd" role="main"/>
        <table name="pms" role="standard"/>
        <field table="pmsd" name="pms_id" dataType="integer"/>
        <field table="pmsd" name="date_todo" dataType="date"/>
        <field table="pmsd" name="date_completed" dataType="date"/>
        <field table="pmsd" name="date_latest" dataType="date"/>
        <field table="pmsd" name="date_earliest" dataType="date"/>
        <field table="pmsd" name="wo_id" dataType="integer"/>
        <field table="pmsd" name="is_cancelled" dataType="integer"/>
        <field table="pms" name="tr_id" dataType="text"/>

        <parameter name="pmp.tr_id" dataType="verbatim" value=""/>
        <restriction id="pmp.tr_id" type="sql" enabled="pmp.tr_id" sql="pmp.tr_id IN(${parameters['pmp.tr_id']})"/>
        <restriction id="pmpstr.tr_id" type="sql" enabled="pmp.tr_id" sql="pmpstr.tr_id IN (${parameters['pmp.tr_id']})"/>

    </dataSource>

    <!-- Filter Panel-->
    <panel type="view" id="plannerFilterView" file="ab-pm-planner-filter.axvw"/>

    <tabs id="plannerTabs" layoutRegion="gridRegion">
        <tab name="weeklyPlannerTab" selected="true">
            <title>Weekly Planner</title>
            <!-- HTML panel where the grid control is displayed. -->
            <panel type="html" id="gridPanel" layoutRegion="gridRegion">
                <html>
                    <div id="schedulerContainer"></div>
                </html>
            </panel>
        </tab>
        <tab name="financialImpactTab" file="ab-pm-planner-financial-impact.axvw">
            <title>Financial Impact</title>
        </tab>
        <tab name="laborAnalysisTab" file="ab-pm-planner-labor-analysis.axvw">
            <title>Labor Analysis</title>
        </tab>
    </tabs>
    <message name="EstimatedLaborHours">Estimated Labor Hours</message>
    <message name="EstimatedCost">Estimated Cost</message>
    <message name="Cost">Cost</message>
    <message name="TotalCosts">Total Costs</message>
    <message name="LaborHours">Labor Hours</message>
    <message name="TotalLaborHours">Total Labor Hours</message>
    <message name="SelectFields">Select Fields</message>
    <message name="SelectGroup">Group by</message>
    <message name="SelectWeek">Display</message>
    <message name="Weeks">weeks</message>
    <message name="EquipmentStandard">Equipment Standard</message>
    <message name="Building">Building</message>
    <message name="Trade">Trade</message>
    <message name="Site">Site</message>
    <message name="WorkTeam">Work Team</message>
    <message name="ServiceContract">Service Contract</message>
    <message name="ServiceContractProvider">Service Contract Provider</message>
    <message name="PMProcedure">PM Procedure</message>
    <message name="Calendar">Calendar</message>
    <message name="None">None</message>
	<message name="PastPMScheduleDates">Past PM Schedule Statistics</message>
	<message name="Month">Month</message>
	<message name="EquipmentSystem">Equipment System</message>

	<message name="Monday">Mon.</message>
	<message name="Tuesday">Tue.</message>
	<message name="Wednesday">Wed.</message>
	<message name="Thursday">Thu.</message>
	<message name="Friday">Fri.</message>
	<message name="Saturday">Sat.</message>
	<message name="Sunday">Sun.</message>

	<message name="NA">N/A</message>
	<message name="CompletedOnTime">Completed On Time</message>
	<message name="CompletedLate">Completed Late</message>
	<message name="Missed">Missed</message>
	<message name="OnTime">% on time</message>
	<message name="Generated">Generated</message>
	<message name="Planned">Planned</message>
	<message name="Scheduled">scheduled</message>

	<message name="Multiple">Multiple</message>
	<message name="Manual">Manual</message>
	<message name="Meter">Meter</message>
	<message name="Every">Every</message>
	<message name="Days">Days</message>
	<message name="Weeks">Weeks</message>
	<message name="Months">Months</message>
	<message name="Quarters">Quarters</message>
	<message name="Years">Years</message>
	<message name="Miles">Miles</message>
	<message name="Hours">Hours</message>

	<message name="EQStandardColumn">EQ Standard</message>
	<message name="PMScheduleCodeColumn">PM Schedule Code</message>
	<message name="EquipmentCodeColumn">Equipment Code</message>
	<message name="SiteCodeColumn">Site Code</message>
	<message name="LocationColumn">Location</message>
	<message name="IntervalColumn">Interval</message>
	<message name="PMProcedureColumn">PM Procedure</message>
	<message name="PMScheduleGroupColumn">PM Schedule Group</message>
	<message name="DateOfLastPMColumn">Date of Last PM</message>
	<message name="DateForFirstPMColumn">Date for First PM</message>

	<message name="Loading">Loading...</message>

	<message name="MonthU">month</message>
	<message name="Disable">Disable</message>
	<message name="WeekU">week</message>

</view>