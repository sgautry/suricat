/**
 * Controller for Labor Analysis shortage report.
 */
View.createController('laborAnalysisShortagesController', {

    /**
     * Event handler for Show button.
     */
    selectWorkTeamPanel_onShow: function() {
        // clear the old shortage report before showing new
        jQuery("#schedulerContainer").empty();

        // get required work team code and validate empty
        var workTeamId = this.selectWorkTeamPanel.getFieldValue('work_team.work_team_id');
        this.workTeamId = workTeamId;
        if (!workTeamId) {
            View.showMessage(getMessage('noWorkTeam'));
            return;
        }
        // reset the panel title to append current work team code
        this.gridPanel.setTitle(String.format(getMessage('laborShortagesPanel'), workTeamId));

        // show loading spiral panel
        var loadPanel = this._createLoadPanel();
        loadPanel.show();

        // load shortage data and show report
        var scope = this;
        setTimeout(function() {
            var laborDS = new PMPlanner.CostLaborDataSource();
            laborDS.loadShortages(workTeamId);
            scope.showShortageGrid(laborDS);
            loadPanel.hide();
            scope._removeLoadPanel();
        }, 10);

    },

    /**
     * show shortage report use dxDataGrid.
     */
    showShortageGrid: function(laborDS) {

        // prepare data
        var controller = this;
        var parentFilterPanel = View.getOpenerView().panels.get('pmSchedulesFilter');
        var parentFilterController = View.getOpenerView().controllers.get('pmPlannerFilterController');
        var pmPlannerController = View.getOpenerView().controllers.get('pmPlannerController');
        var gridData = [];
        for (var i = 0; i < laborDS.shortageTradesArray.length; i++) {
            var data = {};
            data['trId'] = laborDS.shortageTradesArray[i];
            for (var j = 0; j < laborDS.shortageWeeksArray.length; j++) {
                var week = laborDS.shortageWeeksArray[j];
                var value = null;
                var remaningHours = laborDS.getTradeHoursByWeekId(laborDS.shortageTradesArray[i], week.weekIndex, laborDS.HOUR_TYPE_REMAINING_PM);
                if (remaningHours < 0) {
                    value = -remaningHours;
                }
                data['W' + week.weekIndex] = {
                    week: week,
                    value: value
                };
            }
            gridData.push(data);
        }

        // confirm grid columns
        if (gridData.length > 0) {
            var columns = [];
            columns.push({
                dataField: 'trId',
                caption: getMessage('tradeCode'),
                cellTemplate: function(container, options) {
                    container.append(options.value);
                    container.css('cursor', 'pointer');
                    container.on('click', function() {
                        for (var j = 0; j < laborDS.shortageWeeksArray.length; j++) {
                            var week = laborDS.shortageWeeksArray[j];
                            var key = 'W' + week.weekIndex;
                            value = options.data[key].value;
                            if (value != null && value > 0) {
                                pmPlannerController._scheduler.updateCalendar(options.data[key]['week'].date);
                                break;
                            }
                        }

                        parentFilterPanel.setFieldValue('pmp.tr_id', options.value);
                        parentFilterPanel.setFieldValue('pms.sla_work_team_id', controller.workTeamId);
                        parentFilterController.pmSchedulesFilter_onFilter();
                    });
                },
                width: 120
            });
            for (var j = 0; j < laborDS.shortageWeeksArray.length; j++) {
                var week = laborDS.shortageWeeksArray[j];
                var column = {
                    dataField: 'W' + week.weekIndex,
                    cellTemplate: function(container, options) {
                        container.append(options.value['value']);
                        container.css('cursor', 'pointer');
                        container.on('click', function() {
                            pmPlannerController._scheduler.updateCalendar(options.value['week'].date);
                            parentFilterPanel.setFieldValue('pmp.tr_id', options.data['trId']);
                            parentFilterPanel.setFieldValue('pms.sla_work_team_id', controller.workTeamId);
                            parentFilterController.pmSchedulesFilter_onFilter();
                        });
                    },
                    width: 50
                }
                columns.push(column);
            }

            // instance the grid
            jQuery("#schedulerContainer").dxDataGrid({
                dataSource: gridData,
                columns: columns,
                columnAutoWidth: false,
                sorting: {
                    mode: 'none'
                }
            });
        }

    },

    /**
     * create loading spiral panel
     */
    _createLoadPanel: function() {
        jQuery('#schedulerContainer').append(jQuery('<div id="plannedLoadingPanel"></div>'));
        jQuery("#plannedLoadingPanel").dxLoadPanel({
            message: getMessage('Loading'),
            visible: false
        });
        return jQuery("#plannedLoadingPanel").dxLoadPanel("instance");
    },

    /**
     * remove loading spiral panel
     */
    _removeLoadPanel: function() {
        jQuery("#plannedLoadingPanel").remove();
    }

});
