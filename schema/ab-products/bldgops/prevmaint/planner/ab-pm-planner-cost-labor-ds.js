/**
 * DataSource to calculate PM Schedule cost and labor hours
 */
PMPlanner.CostLaborDataSource = Base.extend({

    /**
     * Main PM Planner view
     */
    _pmPlannerView: null,

    /**
     * all filtered pmsd, grouped by pms_id
     */
    _works: {},

    /**
     * all filtered trades
     */
    _trades: {},

    /**
     * trade week statistics: Planned Hours, PM Capacity, Remaining Hours
     */
    _tradeStatistics: {},

    /**
     * group by week statistics: Cost, Labor Hours
     */
    _groupStatistics: {},

    /**
     * Week total statistics: Cost, Labor Hours
     */
    _totalStatistics: {},

    /**
     * Average trade week On demand hours
     */
    _averageOnDemandHours: {},

    /**
     * Cost and Labor calculation datasource
     */
    _costLaborDS: null,

    /**
     * Trades query datasource
     */
    _tradeDS: null,

    /**
     * Trades assigned to PM Schedule query datasource
     */
    _pmsTradeDS: null,

    /**
     * Trades average week on demand hours calculation datasource
     */
    _averageOnDemandHoursDS: null,

    /**
     * Craftspersons query datasource
     */
    _craftspersonsDataSource: null,

    /**
     * Constructor
     * @param pmPlannerView PM Planner control view object
     */
    constructor: function(pmPlannerView) {

        // initialize dataSources
        this._pmPlannerView = pmPlannerView;
        this._works = {};
        this._costLaborDS = View.dataSources.get('costLaborDS');
        this._costLaborDS.addParameter('excludeCancelDate', true);
        this._tradeDS = View.dataSources.get('tradeDS');
        this._pmsTradeDS = View.dataSources.get('pmsTradeDS');
        this._averageOnDemandHoursDS = View.dataSources.get('averageOnDemandHoursDS');
        this._craftspersonsDataSource = View.dataSources.get('craftspersonsDataSource');

        // Extend the Date prototype to add new methods getWeek(), nextWeek(), prevWeek()
        Date.prototype.getWeek = function() {
            return this.format('W');
        };
        Date.prototype.nextWeek = function(count) {
            var dayCount = 7;
            if (count) {
                dayCount = count * 7;
            }
            this.setDate(this.getDate() + dayCount);
        };
        Date.prototype.prevWeek = function(count) {
            var weekCount = 7;
            if (count) {
                weekCount = count * 7;
            }
            this.setDate(this.getDate() - weekCount);
        };

        // register this datasource to on event 'app:bldgops:pmPlanner:updateCostAndLaborRecord'
        var eventAggregator = View.eventAggregator;
        eventAggregator.on('app:bldgops:pmPlanner:updateCostAndLaborRecord', this.updateCostAndLaborRecord, this);
    },

    /**
     * Update cost and labor record
     * @param record pmsd record
     */
    updateCostAndLaborRecord: function(record) {

        // if Show cost and Show Labor checkbox not checked, return to do nothing
        if (!this._pmPlannerView._costInformation._showCosts && !this._pmPlannerView._costInformation._showLabor) {
            return;
        }

        // get old date and new date
        var pmsId = record.getValue(PMPlanner.DataSource.DataField_PmsId);
        var oldDateTodo = record.oldValues[PMPlanner.DataSource.DataField_DateTodo];
        var newDateTodo = record.getValue(PMPlanner.DataSource.DataField_DateTodo);

        // if date_todo not changed, then we don't need update the statistics
        if (PMPlanner.Tools.formatDate(oldDateTodo) == PMPlanner.Tools.formatDate(newDateTodo)) {
            return;
        }

        if (this._works.hasOwnProperty(pmsId)) {
            // find all works in the same PM schedule code
            var pmsWorks = this._works[pmsId];

            for (var i = 0; i < pmsWorks.length; i++) {
                var work = pmsWorks[i];
                if (PMPlanner.Tools.formatDate(oldDateTodo) == PMPlanner.Tools.formatDate(work.dateTodo)) {
                    var id = work.groupName + '_' + work.week;
                    // find the old date work, update group week cost and labor statistics to minus current date cost and labor hours
                    if (this._groupStatistics.hasOwnProperty(id)) {
                        this._groupStatistics[id].displayedLaborHours = this._groupStatistics[id].displayedLaborHours - work.displayedLaborHours;
                        this._groupStatistics[id].displayedCosts = this._groupStatistics[id].displayedCosts - work.displayedCosts;
                    }

                    // find the new date work, update group week cost and labor statistics to add current date cost and labor hours
                    id = work.groupName + '_' + PMPlanner.Calendar.WeekColumn.formatWeek(newDateTodo);
                    if (this._groupStatistics.hasOwnProperty(id)) {
                        this._groupStatistics[id].displayedLaborHours = this._groupStatistics[id].displayedLaborHours + work.displayedLaborHours;
                        this._groupStatistics[id].displayedCosts = this._groupStatistics[id].displayedCosts + work.displayedCosts;
                    } else {
                        this._groupStatistics[id] = {};
                        this._groupStatistics[id].displayedLaborHours = work.displayedLaborHours;
                        this._groupStatistics[id].displayedCosts = work.displayedCosts;
                    }

                    id = work.week;
                    // find the old date work, update total week cost and labor statistics to minus current date cost and labor hours
                    if (this._totalStatistics.hasOwnProperty(id)) {
                        this._totalStatistics[id].displayedLaborHours = this._totalStatistics[id].displayedLaborHours - work.displayedLaborHours;
                        this._totalStatistics[id].displayedCosts = this._totalStatistics[id].displayedCosts - work.displayedCosts;
                    }

                    // find the new date work, update total week cost and labor statistics to add current date cost and labor hours
                    id = PMPlanner.Calendar.WeekColumn.formatWeek(newDateTodo);
                    if (this._totalStatistics.hasOwnProperty(id)) {
                        this._totalStatistics[id].displayedLaborHours = this._totalStatistics[id].displayedLaborHours + work.displayedLaborHours;
                        this._totalStatistics[id].displayedCosts = this._totalStatistics[id].displayedCosts + work.displayedCosts;
                    } else {
                        this._totalStatistics[id] = {};
                        this._totalStatistics[id].displayedLaborHours = work.displayedLaborHours;
                        this._totalStatistics[id].displayedCosts = work.displayedCosts;
                    }

                    var trades = this._pmsTrades[pmsId];
                    var scope = this;
                    var weekIndex = oldDateTodo.getWeek();
                    var newWeekIndex = newDateTodo.getWeek();
                    try {
                        if (trades) {
                            jQuery.each(trades, function(index, trObject) {
                                // find the old date work, update trade week statistics to minus current date labor hours
                                if (scope._tradeStatistics.hasOwnProperty(weekIndex)) {
                                    scope._tradeStatistics[weekIndex].totalPlannerHours = scope._tradeStatistics[weekIndex].totalPlannerHours - trObject.hours;
                                }

                                var id = trObject.trId + '_' + weekIndex;
                                if (scope._tradeStatistics.hasOwnProperty(id)) {
                                    scope._tradeStatistics[id].totalPlannerHours = scope._tradeStatistics[id].totalPlannerHours - trObject.hours;
                                }

                                // find the new date work, update trade week statistics to add current date labor hours
                                if (scope._tradeStatistics.hasOwnProperty(newWeekIndex)) {
                                    scope._tradeStatistics[newWeekIndex].totalPlannerHours = scope._tradeStatistics[newWeekIndex].totalPlannerHours + trObject.hours;
                                }

                                var id = trObject.trId + '_' + newWeekIndex;
                                if (scope._tradeStatistics.hasOwnProperty(id)) {
                                    scope._tradeStatistics[id].totalPlannerHours = scope._tradeStatistics[id].totalPlannerHours + trObject.hours;
                                }

                            });
                        }

                    } catch (e) {
                        console.log(e);
                    }

                    // update the stored date_todo value
                    work.dateTodo = newDateTodo;
                    work.week = PMPlanner.Calendar.WeekColumn.formatWeek(newDateTodo);

                    // update UI values in weekly calendar tab and Labor Analysis tab
                    this._pmPlannerView.updateFinanceStatistics();
                    this._pmPlannerView.callEvent('onAfterCostLaborDSLoad', []);

                    break;
                }
            }
        }
    },

    /**
     * get cost in single work
     * @param id pms schedule code
     * @param week week number
     */
    getCostInWork: function(id, week) {
        var value = 'NaN';
        var listValues = this._works[id];
        for (var i = 0; i < listValues.length; i++) {
            if (listValues[i].week == week) {
                value = listValues[i].estimateCosts;
                break;
            }
        }
        return value;
    },

    /**
     * get labor hours in single work
     * @param id pms schedule code
     * @param week week number
     */
    getLaborHoursInWork: function(id, week) {
        var tradeHours = {};
        if (this._pmsTrades.hasOwnProperty(id)) {
            var tradeHoursArray = this._pmsTrades[id];
            for (var i = 0; i < tradeHoursArray.length; i++) {
                var trId = tradeHoursArray[i].trId;
                var hours = tradeHoursArray[i].hours;
                if (tradeHours.hasOwnProperty(trId)) {
                    tradeHours[trId] = tradeHours[trId] + hours;
                } else {
                    tradeHours[trId] = hours;
                }
            }
        }

        return tradeHours;
    },

    /**
     * get group week cost
     * @param groupValue group field value
     * @param week week number
     */
    getCostInWeek: function(groupValue, week) {
        return this.getNumberValueFromOb(this._groupStatistics[groupValue + '_' + week], 'displayedCosts');
    },

    /**
     * get group week labor hours
     * @param groupValue group field value
     * @param week week number
     */
    getLaborHoursInWeek: function(groupValue, week) {
        return this.getNumberValueFromOb(this._groupStatistics[groupValue + '_' + week], 'displayedLaborHours');
    },

    /**
     * get group week cost
     * @param groupValue group field value
     * @param week week number
     */
    getTotalCostInWeek: function(week) {
        return this.getNumberValueFromOb(this._totalStatistics[week], 'displayedCosts');
    },

    /**
     * get total week labor hours
     * @param week week number
     */
    getTotalLaborHoursInWeek: function(week) {
        return this.getNumberValueFromOb(this._totalStatistics[week], 'displayedLaborHours');
    },

    /**
     * get trade week planned labor hours
     * @param trade trade code
     * @param week week number
     */
    getTradePlannedHoursInWeek: function(trade, weekIndex) {
        return this.getNumberValueFromOb(this._tradeStatistics[trade + '_' + weekIndex], 'totalPlannerHours');
    },

    /**
     * get trade week PM capacity
     * @param trade trade code
     * @param week week number
     */
    getTradePmCapacityInWeek: function(trade, weekIndex) {
        return this.getNumberValueFromOb(this._tradeStatistics[trade + '_' + weekIndex], 'pmCapacity');
    },

    /**
     * get trade week PM capacity
     * @param trade trade code
     * @param week week number
     */
    getTotalTradePlannedHoursInWeek: function(weekIndex) {
        return this.getNumberValueFromOb(this._tradeStatistics[weekIndex], 'totalPlannerHours');
    },

    /**
     * get total trade week PM capacity
     * @param week week number
     */
    getTotalTradePmCapacityInWeek: function(weekIndex) {
        return this.getNumberValueFromOb(this._tradeStatistics[weekIndex], 'pmCapacity');
    },

    /**
     * helper method to get number value
     * @param week week number
     */
    getNumberValueFromOb: function(ob, prop) {
        var value = 0;
        if (valueExists(ob) && valueExists(ob[prop])) {
            value = ob[prop];
        }

        return value.toFixed(2);
    },

    /**
     * load all works in datasource and calculate statistics
     * @param start start date
     * @param end end date
     */
    _load: function(start, end) {

        // clear all value before load
        this._works = {};
        this._trades = {};
        this._pmsTrades = {};
        this._tradeStatistics = {};
        this._groupStatistics = {};
        this._totalStatistics = {};

        // if Show cost and Show Labor checkbox not checked, return to do nothing
        if (!this._pmPlannerView._costInformation._showCosts && !this._pmPlannerView._costInformation._showLabor) {
            return;
        }

        // load all trades that assigned to filtered PM schedules
        var records = this._pmsTradeDS.getDataSet().records;
        for (var i = 0; i < records.length; i++) {
            var record = records[i];
            var pmsId = record.getValue('pmpstr.pms_id');
            var trId = record.getValue('pmpstr.tr_id');
            var hours = this._parseNumber(record.getValue('pmpstr.hours_req'));
            var id = record.getValue('pmpstr.tr_id') + '_' + record.getValue('pmpstr.pms_id');
            if (!this._pmsTrades.hasOwnProperty(pmsId)) {
                this._pmsTrades[pmsId] = [];
            }

            this._pmsTrades[pmsId].push({
                'trId': trId,
                'hours': hours
            });
        }

        // load PM capacity for all trades in date range
        this._addPmCapacityInTradeStatistics(start, end);

        // query all pmsd to calculate cost and labor hours
        var dataRangeRestriction = this._pmPlannerView.getDataSource()._configRestriction(PMPlanner.DataSource.DataField_DateTodo, start, end);
        var records = this._costLaborDS.getDataSet(dataRangeRestriction, {}).records;
        for (var i = 0; i < records.length; i++) {
            var record = records[i];
            var dateTodo = record.getValue(PMPlanner.DataSource.DataField_DateTodo);
            var groupFieldName = DataGridSource.parseGridFieldName(this._pmPlannerView.getDataSource()._groupField).fieldName;
            if (groupFieldName == 'eq.eq_std') {
                groupFieldName = 'pms.eq_std';
            } else if (groupFieldName == 'pmp.tr_id') {
                groupFieldName = 'pms.tr_id';
            }
            var work = {
                pmsId: record.getValue(PMPlanner.DataSource.DataField_PmsId),
                groupName: record.getValue(groupFieldName),
                dateTodo: dateTodo,
                week: PMPlanner.Calendar.WeekColumn.formatWeek(dateTodo),
                estimateLaborHours: this._parseNumber(record.getValue('pmsd.vf_est_labor_hours')),
                estimateCosts: this._parseNumber(record.getValue('pmsd.vf_est_cost')),
                actualLaborHours: this._parseNumber(record.getValue('pmsd.vf_actual_hours')),
                actualCosts: this._parseNumber(record.getValue('pmsd.vf_actual_cost')),
                displayedLaborHours: record.getValue('pmsd.vf_pmsd_status') == 'Work Order Completed' || record.getValue('pmsd.vf_pmsd_status') == 'Completed Late' ? this._parseNumber(record
                        .getValue('pmsd.vf_actual_hours')) : this._parseNumber(record.getValue('pmsd.vf_est_labor_hours')),
                displayedCosts: record.getValue('pmsd.vf_pmsd_status') == 'Work Order Completed' || record.getValue('pmsd.vf_pmsd_status') == 'Completed Late' ? this._parseNumber(record
                        .getValue('pmsd.vf_actual_cost')) : this._parseNumber(record.getValue('pmsd.vf_est_cost')),
                record: record
            };

            // stored every single work
            this._addWork(work);

            // update group statistics
            this._addWorkInGroupStatistics(work);

            // update total statistics
            this._addWorkInTotalStatistics(work);

            // update trade statistics
            this._addWorkInTradeStatistics(work);
        }

        // calculate trade average on demand hours
        this._calculateAverageOnDemandHours();

        // trigger event onAfterCostLaborDSLoad
        this._pmPlannerView.callEvent('onAfterCostLaborDSLoad', []);

    },

    /**
     * calculate trade average on demand hours
     * @param workTeamId work team code
     */
    _calculateAverageOnDemandHours: function(workTeamId) {
        this._averageOnDemandHours = {};
        var records = this._averageOnDemandHoursDS.getDataSet(null, {}).records;
        for (var i = 0; i < records.length; i++) {
            var trId = records[i].getValue('cf.tr_id');
            var avgOnDemandhours = records[i].getValue('cf.avg_od_hours');
            if (!this._averageOnDemandHours.hasOwnProperty(trId)) {
                this._averageOnDemandHours[trId] = parseFloat(avgOnDemandhours);
            } else {
                this._averageOnDemandHours[trId] = parseFloat(avgOnDemandhours) + this._averageOnDemandHours[trId];
            }
        }
    },

    /**
     * Load work team shortages data
     * @param workTeamId work team code
     */
    loadShortages: function(workTeamId) {
        // clear all value before load
        this._works = {};
        this._pmsTrades = {};
        this._tradeStatistics = {};
        this._groupStatistics = {};
        this._totalStatistics = {};
        var trades = {};

        // set work team parameter
        var parameters = View.getOpenerView().dataSources.get('costLaborDS').parameters;
        this._pmsTradeDS.parameters = parameters;
        this._costLaborDS.parameters = parameters;
        this._tradeDS.parameters = parameters;
        this._averageOnDemandHoursDS.parameters = parameters;
        this._craftspersonsDataSource.parameters = parameters;

        this._pmsTradeDS.addParameter('pms.sla_work_team_id', "'" + workTeamId + "'");
        this._costLaborDS.addParameter('pms.sla_work_team_id', "'" + workTeamId + "'");
        this._tradeDS.addParameter('pms.sla_work_team_id', "'" + workTeamId + "'");
        this._averageOnDemandHoursDS.addParameter('pms.sla_work_team_id', "'" + workTeamId + "'");
        this._craftspersonsDataSource.addParameter('pms.sla_work_team_id', "'" + workTeamId + "'");

        // load all trades that assigned to filtered PM schedules
        var records = this._pmsTradeDS.getDataSet().records;
        for (var i = 0; i < records.length; i++) {
            var record = records[i];
            var pmsId = record.getValue('pmpstr.pms_id');
            var trId = record.getValue('pmpstr.tr_id');
            var hours = this._parseNumber(record.getValue('pmpstr.hours_req'));
            var id = record.getValue('pmpstr.tr_id') + '_' + record.getValue('pmpstr.pms_id');
            if (!this._pmsTrades.hasOwnProperty(pmsId)) {
                this._pmsTrades[pmsId] = [];
            }

            this._pmsTrades[pmsId].push({
                'trId': trId,
                'hours': hours
            });

            if (!trades.hasOwnProperty(trId)) {
                trades[trId] = trId;
            }
        }

        // load PM capacity for all trades in future 52 week from current week
        this._add52WeekPmCapacityInTradeStatistics(trades);

        var startWeekObj = new PMPlanner.Calendar.WeekColumn({
            date: new Date()
        });
        var endWeekObj = new PMPlanner.Calendar.WeekColumn({
            date: DateMath.add(new Date(), DateMath.WEEK, 51)
        });
        var restriction = new Ab.view.Restriction();
        restriction.addClause('pmsd.date_todo', PMPlanner.Tools.formatDate(startWeekObj.getDateStart()), '&gt;=');
        restriction.addClause('pmsd.date_todo', PMPlanner.Tools.formatDate(endWeekObj.getDateEnd()), '&lt;=');
        this._costLaborDS.addParameter('pms.sla_work_team_id', "'" + workTeamId + "'");
        var records = this._costLaborDS.getDataSet(restriction, {}).records;
        for (var i = 0; i < records.length; i++) {
            var record = records[i];
            var dateTodo = record.getValue(PMPlanner.DataSource.DataField_DateTodo);
            var work = {
                pmsId: record.getValue(PMPlanner.DataSource.DataField_PmsId),
                groupName: record.getValue('pms.sla_work_team_id'),
                dateTodo: dateTodo,
                week: PMPlanner.Calendar.WeekColumn.formatWeek(dateTodo),
                estimateLaborHours: this._parseNumber(record.getValue('pmsd.vf_est_labor_hours')),
                estimateCosts: this._parseNumber(record.getValue('pmsd.vf_est_cost')),
                actualLaborHours: this._parseNumber(record.getValue('pmsd.vf_actual_hours')),
                actualCosts: this._parseNumber(record.getValue('pmsd.vf_actual_cost')),
                displayedLaborHours: record.getValue('pmsd.vf_pmsd_status') == 'Work Order Completed' || record.getValue('pmsd.vf_pmsd_status') == 'Completed Late' ? this._parseNumber(record
                        .getValue('pmsd.vf_actual_hours')) : this._parseNumber(record.getValue('pmsd.vf_est_labor_hours')),
                displayedCosts: record.getValue('pmsd.vf_pmsd_status') == 'Work Order Completed' || record.getValue('pmsd.vf_pmsd_status') == 'Completed Late' ? this._parseNumber(record
                        .getValue('pmsd.vf_actual_cost')) : this._parseNumber(record.getValue('pmsd.vf_est_cost')),
                record: record
            };

            // stored every single work
            this._addWork(work);
            // update trade statistics
            this._addWorkInTradeStatistics(work);
        }

        // calculate trade average on demand hours
        this._calculateAverageOnDemandHours();

        // find all shortage trades in weeks
        var shortageWeeks = new Ext.util.MixedCollection();
        var shortageTades = new Ext.util.MixedCollection();
        for ( var trId in trades) {
            var dateStart = new Date();
            for (var w = 1; w < 53; w++) {
                if (w > 1) {
                    dateStart.nextWeek();
                }
                var weekIndex = dateStart.getWeek();
                // if remaining hours < 0 , this trade week is in shortage
                if (this.getTradeHoursByWeekId(trId, weekIndex, this.HOUR_TYPE_REMAINING_PM) < 0) {
                    if (!shortageWeeks.getKey(w)) {
                        shortageWeeks.add(w, {
                            weekIndex: weekIndex,
                            date: new Date(dateStart.getTime())
                        });
                    }

                    if (!shortageTades.get(trId)) {
                        shortageTades.add(trId, trId);
                    }
                }
            }
        }

        // sort the weeks
        this.shortageWeeksArray = [];
        var scopse = this;
        shortageWeeks.keySort();
        shortageWeeks.each(function(week) {
            scopse.shortageWeeksArray.push(week);
        });

        this.shortageTradesArray = [];
        shortageTades.each(function(trId) {
            scopse.shortageTradesArray.push(trId);
        });

        // sort the trades
        this.shortageTradesArray.sort();

    },

    /**
     * Helper method to parse number value
     * @param value
     */
    _parseNumber: function(value) {
        var val = 0.00;
        if (valueExistsNotEmpty(value)) {
            val = parseFloat(value);
        }
        return val;
    },

    /**
     * store work in this._works
     * @param work
     */
    _addWork: function(work) {
        if (!this._works.hasOwnProperty(work.pmsId)) {
            this._works[work.pmsId] = [];
        }
        this._works[work.pmsId].push(work);
    },

    /**
     * update group statistics
     * @param work
     */
    _addWorkInGroupStatistics: function(work) {
        var groupFieldName = DataGridSource.parseGridFieldName(this._pmPlannerView.getDataSource()._groupField).fieldName;
        if (groupFieldName == 'pms.tr_id') {
            var pmsId = work.pmsId;
            var trades = this._pmsTrades[pmsId];
            var tradesObj = {};
            jQuery.each(trades, function(index, trObject) {
                if (!tradesObj.hasOwnProperty(trObject.trId)) {
                    tradesObj[trObject.trId] = trObject.trId;
                }
            });

            var scope = this;
            try {
                if (trades) {
                    jQuery.each(tradesObj, function(trId, trId) {
                        var id = trId + '_' + work.week;
                        if (scope._groupStatistics.hasOwnProperty(id)) {
                            scope._groupStatistics[id].displayedLaborHours = scope._groupStatistics[id].displayedLaborHours + work.displayedLaborHours;
                            scope._groupStatistics[id].displayedCosts = scope._groupStatistics[id].displayedCosts + work.displayedCosts;
                        } else {
                            scope._groupStatistics[id] = {};
                            scope._groupStatistics[id].displayedLaborHours = work.displayedLaborHours;
                            scope._groupStatistics[id].displayedCosts = work.displayedCosts;
                        }
                    });
                }

            } catch (e) {
                console.log(e);
            }
        } else {
            var id = work.groupName + '_' + work.week;
            if (this._groupStatistics.hasOwnProperty(id)) {
                this._groupStatistics[id].displayedLaborHours = this._groupStatistics[id].displayedLaborHours + work.displayedLaborHours;
                this._groupStatistics[id].displayedCosts = this._groupStatistics[id].displayedCosts + work.displayedCosts;
            } else {
                this._groupStatistics[id] = {};
                this._groupStatistics[id].displayedLaborHours = work.displayedLaborHours;
                this._groupStatistics[id].displayedCosts = work.displayedCosts;
            }
        }
    },

    /**
     * update total statistics
     * @param work
     */
    _addWorkInTotalStatistics: function(work) {
        var id = work.week;
        if (this._totalStatistics.hasOwnProperty(id)) {
            this._totalStatistics[id].displayedLaborHours = this._totalStatistics[id].displayedLaborHours + work.displayedLaborHours;
            this._totalStatistics[id].displayedCosts = this._totalStatistics[id].displayedCosts + work.displayedCosts;
        } else {
            this._totalStatistics[id] = {};
            this._totalStatistics[id].displayedLaborHours = work.displayedLaborHours;
            this._totalStatistics[id].displayedCosts = work.displayedCosts;
        }
    },

    /**
     * update 52 week pm capacity statistics
     * @param trades
     */
    _add52WeekPmCapacityInTradeStatistics: function(trades) {
        var scheduleModle = new CraftspersonCapacityModel();
        var startWeekObj = new PMPlanner.Calendar.WeekColumn({
            date: new Date()
        });
        var endWeekObj = new PMPlanner.Calendar.WeekColumn({
            date: DateMath.add(new Date(), DateMath.WEEK, 51)
        });
        scheduleModle.loadSchedule(null, startWeekObj.getDateStart(), endWeekObj.getDateEnd());

        var scope = this;

        for ( var trId in trades) {
            var dateStart = new Date();
            for (var w = 1; w < 53; w++) {
                if (w > 1) {
                    dateStart.nextWeek();
                }
                var weekIndex = dateStart.getWeek();

                var id = trId + '_' + weekIndex;

                var weekObj = new PMPlanner.Calendar.WeekColumn({
                    date: dateStart
                });
                var pmCapacityInWeek = scheduleModle.getTradeHours(trId, weekObj.getDateStart(), weekObj.getDateEnd());

                if (scope._tradeStatistics.hasOwnProperty(weekIndex)) {
                    scope._tradeStatistics[weekIndex].pmCapacity = scope._tradeStatistics[weekIndex].pmCapacity + pmCapacityInWeek;
                } else {
                    scope._tradeStatistics[weekIndex] = {};
                    scope._tradeStatistics[weekIndex].pmCapacity = pmCapacityInWeek;
                    scope._tradeStatistics[weekIndex].totalPlannerHours = 0;
                }

                if (scope._tradeStatistics.hasOwnProperty(id)) {
                    scope._tradeStatistics[id].pmCapacity = scope._tradeStatistics[i].pmCapacity + pmCapacityInWeek;
                } else {
                    scope._tradeStatistics[id] = {};
                    scope._tradeStatistics[id].pmCapacity = pmCapacityInWeek;
                    scope._tradeStatistics[id].totalPlannerHours = 0;
                }
            }
        }
    },

    /**
     * update pm capacity statistics
     * @param trades
     */
    _addPmCapacityInTradeStatistics: function(start, end) {
        var calendarView = this._pmPlannerView._calendarView;
        var scheduleModle = new CraftspersonCapacityModel();
        scheduleModle.loadSchedule(null, start, end);

        this.weeks = [];
        for (var i = 0; i < calendarView._displayCountWeek; i++) {
            var WeekObj = new PMPlanner.Calendar.WeekColumn({
                date: DateMath.add(new Date(start.getTime()), DateMath.WEEK, i)
            });
            this.weeks.push(WeekObj);
        }

        var scope = this;
        var records = this._tradeDS.getDataSet().records;
        for (var i = 0; i < records.length; i++) {
            var record = records[i];
            var trId = record.getValue('tr.tr_id');
            scope._trades[trId] = trId;
            jQuery.each(scope.weeks, function(index, weekObj) {
                if (weekObj.getDate() != null) {
                    var weekIndex = weekObj._date.getWeek();
                    var id = trId + '_' + weekIndex;
                    var pmCapacityInWeek = scheduleModle.getTradeHours(trId, weekObj.getDateStart(), weekObj.getDateEnd());

                    if (scope._tradeStatistics.hasOwnProperty(weekIndex)) {
                        scope._tradeStatistics[weekIndex].pmCapacity = scope._tradeStatistics[weekIndex].pmCapacity + pmCapacityInWeek;
                    } else {
                        scope._tradeStatistics[weekIndex] = {};
                        scope._tradeStatistics[weekIndex].pmCapacity = pmCapacityInWeek;
                        scope._tradeStatistics[weekIndex].totalPlannerHours = 0;
                    }

                    if (scope._tradeStatistics.hasOwnProperty(id)) {
                        scope._tradeStatistics[id].pmCapacity = scope._tradeStatistics[id].pmCapacity + pmCapacityInWeek;
                    } else {
                        scope._tradeStatistics[id] = {};
                        scope._tradeStatistics[id].pmCapacity = pmCapacityInWeek;
                        scope._tradeStatistics[id].totalPlannerHours = 0;
                    }
                }
            });
        }
    },

    /**
     * update trade statistics
     * @param work
     */
    _addWorkInTradeStatistics: function(work) {
        var weekIndex = work.dateTodo.getWeek();
        var pmsId = work.pmsId;
        var trades = this._pmsTrades[pmsId];
        var scope = this;
        try {
            if (trades) {
                jQuery.each(trades, function(index, trObject) {
                    if (scope._tradeStatistics.hasOwnProperty(weekIndex)) {
                        scope._tradeStatistics[weekIndex].totalPlannerHours = scope._tradeStatistics[weekIndex].totalPlannerHours + trObject.hours;
                    }

                    var id = trObject.trId + '_' + weekIndex;
                    if (scope._tradeStatistics.hasOwnProperty(id)) {
                        scope._tradeStatistics[id].totalPlannerHours = scope._tradeStatistics[id].totalPlannerHours + trObject.hours;
                    }

                });
            }

        } catch (e) {
            console.log(e);
        }
    },

    /**
     * get trade hours by week id
     */
    getTradeHoursByWeekId: function(tradeId, weekIndex, hourType, isTotal) {
        var hour = 0;
        if (isTotal) {
            if (hourType == this.HOUR_TYPE_TOTAL_PLANNED) {
                hour = this.getTotalTradePlannedHoursInWeek(weekIndex);
            } else if (hourType == this.HOUR_TYPE_PM_CAPACITY) {
                hour = this.getTotalTradePmCapacityInWeek(weekIndex);
            } else if (hourType == this.HOUR_TYPE_REMAINING_PM) {
                hour = this.getTotalTradePmCapacityInWeek(weekIndex) - this.getTotalAverageOnDemandHours() - this.getTotalTradePlannedHoursInWeek(weekIndex);
            } else if (hourType == this.HOUR_TYPE_AVERAGE_ON_DEMAND) {
                hour = this.getTotalAverageOnDemandHours();
            }
        } else {

            if (hourType == this.HOUR_TYPE_TOTAL_PLANNED) {
                hour = this.getTradePlannedHoursInWeek(tradeId, weekIndex);
            } else if (hourType == this.HOUR_TYPE_PM_CAPACITY) {
                hour = this.getTradePmCapacityInWeek(tradeId, weekIndex);
            } else if (hourType == this.HOUR_TYPE_REMAINING_PM) {
                hour = this.getTradePmCapacityInWeek(tradeId, weekIndex) - this.getAverageOnDemandHoursOfTrade(tradeId) - this.getTradePlannedHoursInWeek(tradeId, weekIndex);
            } else if (hourType == this.HOUR_TYPE_AVERAGE_ON_DEMAND) {
                hour = this.getAverageOnDemandHoursOfTrade(tradeId);
            }

        }

        if (typeof hour == 'undefined' || hour == 0) {
            hour = parseFloat(0).toFixed(2);
        } else {
            hour = parseFloat(hour).toFixed(2);
        }

        return hour;
    },

    /**
     * get average on demand hours by trade code
     */
    getAverageOnDemandHoursOfTrade: function(trId) {
        var averageOnDemandHours = 0;
        if (this._averageOnDemandHours.hasOwnProperty(trId)) {
            averageOnDemandHours = this._averageOnDemandHours[trId];
        }
        return averageOnDemandHours;
    },

    /**
     * get total average on demand hours
     */
    getTotalAverageOnDemandHours: function() {
        var totalValue = 0;
        for ( var trId in this._averageOnDemandHours) {
            if (this._trades.hasOwnProperty(trId)) {
                totalValue += this._averageOnDemandHours[trId];
            }
        }

        return totalValue;
    },

    HOUR_TYPE_AVERAGE_ON_DEMAND: 'HOUR_TYPE_AVERAGE_ON_DEMAND',
    HOUR_TYPE_TOTAL_PLANNED: 'HOUR_TYPE_TOTAL_PLANNED',
    HOUR_TYPE_PM_CAPACITY: 'HOUR_TYPE_PM_CAPACITY',
    HOUR_TYPE_REMAINING_PM: 'HOUR_TYPE_REMAINING_PM'

});