/**
 * Labor Analysis tab calendar cell height
 */
PMPlanner.Calendar.CELL_HEIGHT = 3;

/**
 * Labor Analysis tab calendar cell width
 */
PMPlanner.Calendar.CELL_WIDTH = 73;

/**
 * Labor Analysis tab view
 */
PMPlannerLaborAnalysis = {

    /**
     * Base class for Labor Analysis tab view.
     */
    View: Base.extend({

        /**
         * Constructor.
         * @param configUser - User configuration to work with the control.
         */
        constructor: function(configUser) {
            var eventManager = new PMPlanner.EventManager();
            this.addEvent = function(eventName, callback, scope) {
                eventManager.addEventListener(eventName, callback, scope);
            };
            this.callEvent = function(eventName, fields) {
                return eventManager.callEvent(eventName, fields);
            };
            this._configUser = configUser;
            this._calendarView = new PMPlannerLaborAnalysis.CalendarView(configUser.columns, this);
            this._createViewGrid();
            View.getOpenerView().controllers.get('pmPlannerController')._scheduler.setLinkedView(this);
        },

        /**
         * Get grid object, which is the original object DevExtreme
         * @returns {null}
         */
        getInstanceGrid: function() {
            return this._instanceGrid;
        },

        /**
         * Get a calendar object to work with columns and data
         * @returns {PMPlanner.Calendar.View|*|PMPlannerLaborAnalysis.CalendarView}
         */
        getCalendarView: function() {
            return this._calendarView;
        },

        /**
         * Get source data to work with data from the database
         * @returns {null}
         */
        getDataSource: function() {
            return null;
        },

        /**
         * Get DOM for grid
         * @returns {*}
         */
        getDOMGrid: function() {
            return jQuery('#' + this._configUser.container + 'Grid');
        },

        /**
         * To perform grid creation and rendering of the calendar
         * @private
         */
        _createViewGrid: function() {
            var date = View.getOpenerView().PMPlannerView._calendarView._dataSource.getFirstWeekColumn().getDate(), gridID = this._configUser.container + 'Grid', container = jQuery('#'
                    + this._configUser.container), gridColumns = this._calendarView.configureGridColumns(date);

            container.append(PMPlanner.Tools.Render.div({
                id: gridID
            }));

            var scope = this;
            this._ARCHIBUSGrid = View.createControl({
                control: 'DataGrid',
                panel: 'gridPanel',
                container: gridID,
                dataSource: 'tradeDS',
                loadPanelText: PMPlanner.Constants.TEXT_Loading,
                showFilter: false,
                showSort: false,
                pageSize: 100,
                rowAlternationEnabled: false,
                autoExpandAll: false,
                columns: gridColumns,
                columnReordering: false,
                columnResizing: false,
                events: {
                    onGroupCellPrepared: function(option, row) {
                        scope.callEvent('onGroupCellPrepared', [ option ]);
                    },
                    onCellPrepared: function(option, row) {
                        scope.callEvent('onCellPrepared', [ option ]);
                    },
                    onHeaderCellPrepared: function(option) {
                        scope.callEvent('onHeaderCellPrepared', [ option ]);
                    },
                    onRowPrepared: function(option) {
                        scope.callEvent('onAfterRenderGroupRow', [ option ]);
                    },
                    afterLoadedData: function() {
                        scope.callEvent('onLoadViewData', [ this.getGroupID() ]);
                    },
                    afterRefresh: function() {
                        scope.callEvent('onAfterViewLoad', []);
                        scope.callEvent('onAfterRenderGrid', []);
                    }
                }
            });
            this._instanceGrid = jQuery('#' + gridID).dxDataGrid('instance');
        }

    }),

    /**
     * Extend the PMPlanner.Calendar.View to remove the unused part for Labor Analysis tab
     */
    CalendarView: PMPlanner.Calendar.View.extend({

        /**
         * Constructor.
         * @param configUser - User configuration to work with the control.
         * @param view - PMPlannerLaborAnalysis view
         */
        constructor: function(configUser, view) {
            this.inherit(configUser, view);
            this._costLaborDataSource = View.getOpenerView().PMPlannerView.getDataSource()._costLaborDataSource;
        },

        /**
         * after group cell prepared.
         * @param option - Option.
         */
        _groupCellPrepared: function(option) {
            var column = this._dataSource.getColumn(option.column.dataField);
            if (column != null) {
                column.stylingGroupCell(option);
            }
        },

        /**
         * Update matrix starting from a new date .
         * @param date - start date.
         */
        _updateMatrix: function(date) {
            var linkedViewDate = new Date(date.getTime());
            var loadPanel = PMPlanner.Tools.createLoadPanel(this._view);
            loadPanel.show();
            var grid = this._view.getInstanceGrid();
            var scope = this;
            setTimeout(function() {
                var doUpdate = scope._updateLinkedView(linkedViewDate);
                grid.beginUpdate();
                scope._updateGridColumns(date);
                grid.endUpdate();
                if (!doUpdate) {
                    loadPanel.hide();
                    PMPlanner.Tools.removeLoadPanel();
                }
            }, 10);
        },

        /**
         * load view data.
         */
        _loadViewData: function() {

        },

        /**
         * after view load.
         */
        _afterLoadView: function() {

            // remove the grouping close arrow icon
            jQuery("img[src$='treeClose.jpg']").remove();

            // remove the unused table header
            var headerRow = jQuery('.dx-header-row').first();
            headerRow.children().eq(0).attr("style", "border-right-width:0px!important");
            headerRow.children().eq(2).empty();

            // add statistic rows
            var lastDataRow = null;
            var lastSecondRow = null;
            var scope = this;
            jQuery('.dx-group-row').each(function() {
                var currrentDataRow = jQuery(this);
                scope.addStatisticRow(currrentDataRow, scope._costLaborDataSource.HOUR_TYPE_PM_CAPACITY);
                lastDataRow = scope.addStatisticRow(currrentDataRow, scope._costLaborDataSource.HOUR_TYPE_REMAINING_PM);
                lastSecondRow = scope.addStatisticRow(currrentDataRow, scope._costLaborDataSource.HOUR_TYPE_TOTAL_PLANNED);
                scope.addStatisticRow(currrentDataRow, scope._costLaborDataSource.HOUR_TYPE_AVERAGE_ON_DEMAND);

            });

            // add total row
            if (lastDataRow) {
                scope.addTotalStatisticRow(lastDataRow, lastSecondRow, scope._costLaborDataSource.HOUR_TYPE_REMAINING_PM);
                scope.addTotalStatisticRow(lastDataRow, lastSecondRow, scope._costLaborDataSource.HOUR_TYPE_TOTAL_PLANNED);
                scope.addTotalStatisticRow(lastDataRow, lastSecondRow, scope._costLaborDataSource.HOUR_TYPE_AVERAGE_ON_DEMAND);
                scope.addTotalStatisticRow(lastDataRow, lastSecondRow, scope._costLaborDataSource.HOUR_TYPE_PM_CAPACITY);
            }

            jQuery('.dx-datagrid-summary-item').css('text-align', 'right');
        },

        /**
         * add specific statistic row.
         * @param currentRow - current row object.
         * @param hourType - hour type.
         */
        addStatisticRow: function(currentRow, hourType) {
            var statisticTitle = this.getStatisticTitle(hourType);

            currentRow.children().eq(1).text(currentRow.children().eq(1).text().replace(getMessage('tradeCode') + ': ', ''));
            currentRow.attr('trade_code', currentRow.children().eq(1).text());

            var row = currentRow;
            if (hourType != this._costLaborDataSource.HOUR_TYPE_PM_CAPACITY) {
                var newRow = currentRow.clone();
                newRow.insertAfter(currentRow);
                row = newRow;
                newRow.children().eq(1).text('');
                newRow.children().eq(2).text(statisticTitle);

                if (hourType == this._costLaborDataSource.HOUR_TYPE_REMAINING_PM) {
                    newRow.children().each(function() {
                        var currentCell = jQuery(this);
                        currentCell.css('border-bottom', '2px solid #D0D0D0');
                    });
                }
            } else {
                var tradeColumnEl = row.children().eq(1);
                tradeColumnEl.attr('colspan', '1');
                jQuery('<td style="color:blue">' + statisticTitle + '</td>').insertAfter(tradeColumnEl);
            }

            this.showHoursToStatisticRow(row, hourType);

            return row;
        },

        /**
         * get specific statistic title.
         * @param hourType - hour type.
         */
        getStatisticTitle: function(hourType) {
            var statisticTitle = getMessage('Planned_PM_Hours');
            if (hourType == this._costLaborDataSource.HOUR_TYPE_REMAINING_PM) {
                statisticTitle = getMessage('Remaining_Available_Hours');
            } else if (hourType == this._costLaborDataSource.HOUR_TYPE_PM_CAPACITY) {
                statisticTitle = getMessage('Total_Craftsperson_Standard_Hours');
            } else if (hourType == this._costLaborDataSource.HOUR_TYPE_AVERAGE_ON_DEMAND) {
                statisticTitle = getMessage('Average_On_Demand_Hours');
            }

            return statisticTitle;
        },

        /**
         * add specific total statistic row.
         * @param lastDataRow - last row object.
         * @param lastSecondRow - last second object.
         * @param hourType - hour type.
         */
        addTotalStatisticRow: function(lastDataRow, lastSecondRow, hourType) {
            var statisticTitle = this.getStatisticTitle(hourType);

            var newRow = lastSecondRow.clone();
            newRow.insertAfter(lastDataRow);
            newRow.children().eq(1).text('');
            newRow.children().eq(2).text(statisticTitle);

            this.showHoursToStatisticRow(newRow, hourType, true);
        },

        /**
         * get hours and show in the UI.
         * @param currentRow - current row object.
         * @param isTotal - is total row.
         * @param hourType - hour type.
         */
        showHoursToStatisticRow: function(currentRow, hourType, isTotal) {
            var controller = this;
            currentRow.children().each(function() {
                var currentCell = jQuery(this);
                if (currentCell.attr('aria-label') && currentCell.attr('aria-label').indexOf('Column W') != -1) {
                    var weekIndex = currentCell.attr('aria-label').split(',')[0].replace('Column W', '');
                    var hours = controller.getTradeHoursByWeekId(currentRow.attr('trade_code'), weekIndex, hourType, isTotal);
                    var formateValue = View.dataSources.get('worksDS').formatValue('pmsd.total_unit', hours, true);
                    if (hourType == controller._costLaborDataSource.HOUR_TYPE_REMAINING_PM && hours < 0) {
                        formateValue = '<span style="color:red">' + formateValue + '</span>'
                    }
                    currentCell.children().first().empty().append(formateValue);
                }
            });

            currentRow.attr('hour_type', hourType);
            if (isTotal) {
                currentRow.attr('is_total_row', 'true');
            } else {
                currentRow.attr('is_total_row', 'false');
            }

        },

        /**
         * Update hours and show in the UI.
         * @param currentRow - current row object.
         */
        updateHoursToStatisticRow: function(currentRow) {
            var hourType = currentRow.attr('hour_type');
            var is_total_row = currentRow.attr('is_total_row');
            var controller = this;
            currentRow.children().each(function() {
                var currentCell = jQuery(this);
                if (currentCell.attr('aria-label') && currentCell.attr('aria-label').indexOf('Column W') != -1) {
                    var weekIndex = currentCell.attr('aria-label').split(',')[0].replace('Column W', '');
                    var hours = controller.getTradeHoursByWeekId(currentRow.attr('trade_code'), weekIndex, hourType, is_total_row == 'true');
                    if (hourType == controller._costLaborDataSource.HOUR_TYPE_REMAINING_PM && hours < 0) {
                        hours = '<span style="color:red">' + hours + '</span>'
                    }
                    currentCell.children().first().empty().append(hours);
                }
            });
        },

        /**
         * get trade hours by week.
         * @param currentRow - current row object.
         */
        getTradeHoursByWeekId: function(tradeId, weekIndex, hourType, isTotal) {
            return this._costLaborDataSource.getTradeHoursByWeekId(tradeId, weekIndex, hourType, isTotal);
        }

    })
};
