/**
 * Lab analysis tab controller.
 */
View.createController('laborAnalysisTabController', {

    /**
     * Lab analysis tab view.
     */
    _view: null,

    /**
     * After this view load.
     */
    afterViewLoad: function() {
        jQuery.extend(PMPlanner.Constants, {
            TEXT_Loading: getMessage('Loading'),
            WEEK_DAY: [ getMessage('Sunday'), getMessage('Monday'), getMessage('Tuesday'), getMessage('Wednesday'), getMessage('Thursday'), getMessage('Friday'), getMessage('Saturday') ]
        });

        // register response for main view events
        var mainViewEventAggregator = View.getOpenerView().getOpenerView().eventAggregator;
        mainViewEventAggregator.on('app:bldgops:pmPlanner:setFilterParameter', this.setFilterParameter, this);
        mainViewEventAggregator.on('app:bldgops:pmPlanner:filter', this.filterLaborAnalysisTab, this);
        mainViewEventAggregator.on('app:bldgops:pmPlanner:afterCostLaborDSLoad', this.updateUIValues, this);

        // config columns
        var columnDefinitions = [ {
            id: 'tr_id',
            fieldName: 'tr.tr_id',
            visible: true,
            groupBy: true,
            type: 'string',
            title: getMessage('tradeCode'),
            width: 150
        }, {
            id: 'tr_id',
            fieldName: 'tr.tr_id',
            title: getMessage('tradeCode'),
            visible: true,
            type: 'string',
            width: 150
        }, {
            id: 'hour_type',
            fieldName: 'tr.hour_type',
            visible: true,
            type: 'string',
            width: 220
        } ];

        // apply restriction when first load
        View.getOpenerView().controllers.get('pmPlannerFilterController').applyFilterRestrictions();

        // load the Lab analysis tab view
        this._view = new PMPlannerLaborAnalysis.View({
            container: 'schedulerContainer',
            columns: columnDefinitions,
            dataSource: 'tradeDS'
        });
    },

    /**
     * set filter parameters in current view.
     */
    setFilterParameter: function(fieldName, fieldValue) {
        this.tradeDS.addParameter(fieldName, fieldValue);
    },

    /**
     * Update UI values.
     */
    updateUIValues: function() {
        var calendarView = this._view._calendarView;
        var loadPanel = jQuery("#plannedLoadingPanel");
        jQuery('.dx-group-row').each(function() {
            var currrentDataRow = jQuery(this);
            calendarView.updateHoursToStatisticRow(currrentDataRow);
        });

        loadPanel.hide();
        PMPlanner.Tools.removeLoadPanel();

    },

    /**
     * filter the Lab analysis tab view.
     */
    filterLaborAnalysisTab: function(panel) {
        var grid = this._view.getInstanceGrid();
        var loadPanel = PMPlanner.Tools.createLoadPanel(this._view);
        loadPanel.show();
        var scope = this;
        setTimeout(function() {
            grid.refresh();
            scope.updateUIValues();
        }, 100);
    }
});