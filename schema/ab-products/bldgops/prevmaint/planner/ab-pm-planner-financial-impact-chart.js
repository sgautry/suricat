/**
 * Controller for Financial impact tab first chart panel.
 */
var abBldgopsReportWrBudgetAndCostChartController = View.createController('abBldgopsReportWrBudgetAndCostChartController', {

    /**
     * After this view Initialized.
     */
    afterInitialDataFetch: function() {

        // register response for main view events
        this.abBldgopsReportWrBudgetAndCostChart.chartControl.chart.config.dataTipsFormat = 'compact';
        var mainViewEventAggregator = View.getOpenerView().getOpenerView().eventAggregator;
        mainViewEventAggregator.on('app:bldgops:pmPlanner:setFilterParameter', this.setFilterParameter, this);
        mainViewEventAggregator.on('app:bldgops:pmPlanner:filter', this.refreshChartPanel, this);
        mainViewEventAggregator.on('app:bldgops:pmPlanner:afterModifyMonths', this.afterModifyMonths, this);

        // set default monthNumber
        var sideCarPanel = View.getOpenerView().getOpenerView().panels.get('pmSchedulesFilter');
        var monthNumber = sideCarPanel.getSidecar().get('monthNumber');
        if (!valueExists(monthNumber)) {
            sideCarPanel.getSidecar().set('monthNumber', '12');
            sideCarPanel.getSidecar().save();
        }

        // set default group by
        var groupBy = this.groupBySettingPanel.getSidecar().get('groupBy');
        if (!valueExists(groupBy)) {
            this.groupBySettingPanel.getSidecar().set('groupBy', 'bl_id');
            this.groupBySettingPanel.getSidecar().save();
        }

        // show month number in UI
        this.setDisplayeMonthNumber();

        // show group by in UI
        this.setDisplayeGroupBy();

        // apply restriction when first load
        View.getOpenerView().getOpenerView().controllers.get('pmPlannerFilterController').applyFilterRestrictions();

        // load the chart panel
        this.refreshChartPanel();

    },

    /**
     * set filter parameters in current view.
     */
    setFilterParameter: function(fieldName, fieldValue) {
        this.abBldgopsReportWrBudgetAndCostChart.addParameter(fieldName, fieldValue);
    },

    /**
     * set chart panel parameters.
     */
    setPanelParameter: function() {
        var sideCarPanel = View.getOpenerView().getOpenerView().panels.get('pmSchedulesFilter');
        var dateStart = DateMath.findMonthStart(new Date());
        var dateEnd = DateMath.findMonthEnd(DateMath.add(dateStart, DateMath.MONTH, parseInt(sideCarPanel.getSidecar().get('monthNumber')) - 1));
        var panel = this.abBldgopsReportWrBudgetAndCostChart;

        this.groupOptionPlan = 'pms.' + this.groupBySettingPanel.getSidecar().get('groupBy');
        if ('ac_id' == this.groupBySettingPanel.getSidecar().get('groupBy')) {
            this.groupOptionPlan = 'pmp.ac_id';
        } else if ('dp_id' == this.groupBySettingPanel.getSidecar().get('groupBy')) {
            this.groupOptionPlan = "RTRIM(pms.dv_id)${sql.concat}'-'${sql.concat}RTRIM(pms.dp_id)";
        }

        this.groupOptionBudget = 'budget_item.' + this.groupBySettingPanel.getSidecar().get('groupBy');

        if ('dp_id' == this.groupBySettingPanel.getSidecar().get('groupBy')) {
            this.groupOptionBudget = "RTRIM(budget_item.dv_id)${sql.concat}'-'${sql.concat}RTRIM(budget_item.dp_id)";
        }

        panel.addParameter('groupOptionPlan', this.groupOptionPlan);
        panel.addParameter('groupOptionBudget', this.groupOptionBudget);
        panel.addParameter('whereBudget', this.groupOptionPlan + ' = ' + this.groupOptionBudget);
        panel.addParameter('dateStart', getIsoFormatDate(dateStart));
        panel.addParameter('dateEnd', getIsoFormatDate(dateEnd));
        panel.addParameter('scheduledWorkCostsParam', getMessage('scheduledWorkCostsParam'));
        panel.addParameter('budgetedCostsParam', getMessage('budgetedCostsParam'));
    },

    /**
     * refresh the chart paenl
     */
    refreshChartPanel: function() {
        // set chart panel parameters
        this.setPanelParameter();

        // show and refresh the chart
        var panel = this.abBldgopsReportWrBudgetAndCostChart;
        panel.refresh();
        panel.show(true);

        // show loading spiral panel
        var loadPanel = this._createLoadPanel();
        loadPanel.show();
        var scope = this;
        setTimeout(function() {
            scope._removeLoadPanel();
        }, 100);
    },

    /**
     * create loading spiral panel
     */
    _createLoadPanel: function() {
        jQuery('#abBldgopsReportWrBudgetAndCostChart').append(jQuery('<div id="plannedLoadingPanel"></div>'));
        jQuery("#plannedLoadingPanel").dxLoadPanel({
            message: getMessage('Loading'),
            visible: false
        });
        return jQuery("#plannedLoadingPanel").dxLoadPanel("instance");
    },

    /**
     * remove loading spiral panel
     */
    _removeLoadPanel: function() {
        jQuery("#plannedLoadingPanel").remove();
    },

    /**
     * Modify Month number.
     */
    modifyMonths: function(action) {
        var sideCarPanel = View.getOpenerView().getOpenerView().panels.get('pmSchedulesFilter');
        jQuery("input[type=radio][name=monthNumber][value=" + sideCarPanel.getSidecar().get('monthNumber') + "]").attr("checked", true);
        this.monthNumberSettingPanel.showInWindow({
            width: 450,
            height: 200,
            anchor: $('gearMenu'),
            title: '',
            closeButton: true
        });
    },

    /**
     * Save month number.
     */
    monthNumberSettingPanel_onSaveMonthNumber: function(action) {
        var sideCarPanel = View.getOpenerView().getOpenerView().panels.get('pmSchedulesFilter');
        var value = jQuery("input[name='monthNumber']:checked").val();
        sideCarPanel.getSidecar().set('monthNumber', value);
        sideCarPanel.getSidecar().save();
        View.getOpenerView().getOpenerView().eventAggregator.trigger('app:bldgops:pmPlanner:afterModifyMonths');
        this.monthNumberSettingPanel.closeWindow();
    },

    /**
     * after save month number.
     */
    afterModifyMonths: function(action) {
        this.setDisplayeMonthNumber();
        this.refreshChartPanel();
    },

    /**
     * Show month number in UI.
     */
    setDisplayeMonthNumber: function() {
        var sideCarPanel = View.getOpenerView().getOpenerView().panels.get('pmSchedulesFilter');
        var monthNumber = sideCarPanel.getSidecar().get('monthNumber');

        var title = '';
        if ('1' == monthNumber) {
            title = String.format(getMessage('monthNumberSettingPanelTitle1'), monthNumber);
        } else {
            title = String.format(getMessage('monthNumberSettingPanelTitle'), monthNumber);
        }
        this.abBldgopsReportWrBudgetAndCostChart.actions.get('gearMenu').actions.get('modifyMonth').setTitle(title);
    },

    /**
     * Modify Group By.
     */
    modifyGroupBy: function(action) {
        jQuery("input[type=radio][name=groupBy][value=" + this.groupBySettingPanel.getSidecar().get('groupBy') + "]").attr("checked", true);
        this.groupBySettingPanel.showInWindow({
            width: 450,
            height: 200,
            anchor: $('gearMenu'),
            title: getMessage('groupBySettingPanelTitle'),
            closeButton: true
        });
    },

    /**
     * Save Group By.
     */
    groupBySettingPanel_onSaveGroupBy: function(action) {
        var value = jQuery("input[name='groupBy']:checked").val();
        this.groupBySettingPanel.getSidecar().set('groupBy', value);
        this.groupBySettingPanel.getSidecar().save();
        this.setDisplayeGroupBy();
        this.groupBySettingPanel.closeWindow();
        this.refreshChartPanel();
    },

    /**
     * Show Group by in UI.
     */
    setDisplayeGroupBy: function() {
        var groupBy = this.groupBySettingPanel.getSidecar().get('groupBy');
        this.abBldgopsReportWrBudgetAndCostChart.actions.get('gearMenu').actions.get('modifyGroupBy').setTitle(getMessage('groupBySettingPanelTitle') + ' ' + getMessage(groupBy));
    }

})

/**
 * Event handler to click chart
 */
function onPlanBarChartClick(obj) {
    var selectedGroupValue = obj.selectedChartData['ac.groupValue'];
    var planAndBudget = obj.selectedChartData['selectedSecondaryGrouping'].key;
    var controller = View.controllers.get("abBldgopsReportWrBudgetAndCostChartController");
    var chart = View.panels.get('abBldgopsReportWrBudgetAndCostChart');

    if (planAndBudget == getMessage('scheduledWorkCostsParam')) {
        var pmsdGrid = View.panels.get('pmsdGrid');
        pmsdGrid.addParameter('useDateRange', true);
        pmsdGrid.addParameter('excludeCancelDate', true);
        pmsdGrid.parameters = chart.parameters;

        var res = controller.groupOptionPlan + "='" + selectedGroupValue + "'";
        if (controller.groupOptionPlan.indexOf('ac_id') != -1) {
            res = ' EXISTS(SELECT 1 FROM pmp WHERE pmp.pmp_id = pms.pmp_id AND ' + controller.groupOptionPlan + "='" + selectedGroupValue + "')";
        }
        pmsdGrid.refresh(res);
        pmsdGrid.showInWindow({
            width: 1000,
            height: 800,
            title: getMessage('pmsDateGridTitle'),
            closeButton: true
        });
    }

    else if (planAndBudget == getMessage('budgetedCostsParam')) {
        if (View.dialog == null) {
            var res = "budget_item.cost_cat_id = 'MAINT - PREVENTIVE EXPENSE' AND (budget_item.date_start IS NULL OR budget_item.date_start &lt;= ${sql.date('" + chart.parameters['dateEnd']
                    + "')} ) AND (budget_item.date_end IS NULL OR budget_item.date_end  &gt;= ${sql.date('" + chart.parameters['dateStart'] + "')})" + " AND (" + controller.groupOptionBudget + "='"
                    + selectedGroupValue + "' " + ")"

            var filterSiteCode = chart.parameters['pms.site_id'];
            var filterBlCode = chart.parameters['pms.bl_id'];
            var filterAcCode = chart.parameters['pmp.ac_id'];
            if (filterSiteCode) {
                res += ' AND (EXISTS(SELECT 1 FROM bl WHERE bl.bl_id = budget_item.bl_id AND bl.site_id IN(' + filterSiteCode + ')))';
            }

            if (filterBlCode) {
                res += ' AND (budget_item.bl_id IN(' + filterBlCode + '))';
            }

            if (filterAcCode) {
                res += ' AND (budget_item.ac_id IN(' + filterAcCode + '))';
            }
            View.openDialog('ab-bldgops-report-budget-details-pop.axvw', res);
        }
    }
}
