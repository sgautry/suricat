/**
 * Contains methods for working with Planner and PM additional function:
 * the possibility of performing groups, updating the selected block, to show financial statistics.
 */
var PMPlannerControl = Base.extend({

    constructor: function (config) {
        var view = new PMPlanner.View(config);

        /**
         * Sets the control height to specified value.
         * @param height - The height in pixels.
         */
        this.setHeight = function (height) {
            view.setHeight(height);
        };
        /**
         * Group columns in the grid with the specified name.
         * @param option - {groupBy: the name of the grouping field; restriction: data for working with the data source;}
         */
        this.groupBy = function (option) {
            view.groupBy(option);
        };
        /**
         * To perform the show Costs
         * @param show - TRUE if show and FALSE if not show Costs.
         */
        this.showCosts = function (show) {
            view.showCosts(show);
        };
        /**
         * To perform the show Labor
         * @param show - TRUE if show and FALSE if not show Labor.
         */
        this.showLabor = function (show) {
            view.showLabor(show);
        };
        /**
         * Upgrade selected pm scheduled date in the calendar.
         * Called when you want to update the selected unit without restarting view.
         * @param record - data to update
         */
        this.updateSelectedPmScheduledDate = function (key, date) {
            view.updateSelectedPmScheduledDate(key, date);
        };
        this.updatePmScheduledDates = function (key) {
            view.updatePmScheduledDates(key);
        };
        this.refresh = function (option) {
            view.refresh(option);
        };
        /**
         * Get from date in the calendar view
         */
        this.getCalendarFromDate = function () {
            return view.getCalendarFromDate();
        };
        /**
         * Update the calendar and all work on it
         * @param startDate - date from which updating the calendar
         */
        this.updateCalendar = function (startDate) {
            view.updateCalendar(startDate);
        };
        /**
         * Get visible columns
         * @returns {*}
         */
        this.getVisibleColumns = function () {
            return view.getVisibleColumns();
        };
        /**
         * Get the ID of the field on which grouping
         * @returns {*}
         */
        this.getGroupByField = function() {
            return DataGridSource.parseGridFieldName(view.getDataSource()._groupField);
        };        
        /**
         * Set linked view with Labor Analysis tab calendar view.
         * @param linkedView - the calenar view object of Labor Analysis tab
         */
        this.setLinkedView = function(linkedView) {
            view.linkedView = linkedView;
            linkedView.linkedView = view;
        };
        /**
         * Updating the data in the lines financial statistics. Data is retrieved from the database of financial statistics
         */
        this.updateFinanceStatistics = function () {
            view.updateFinanceStatistics();
        };
    }

});


