/**
 * Extend WorkScheduleModel to implement different calculation logic for cf available hours.
 */
var CraftspersonCapacityModel = WorkScheduleModel.extend({

    /**
     * Constructor.
     * @param {config} The control configuration.
     */
    constructor: function(config) {
        this.inherit(config);
    },

    /**
     * Loads the schedule for specified list of resources. Triggers the afterRefresh event.
     * @param {restriction} Ab.data.Restriction to restrict the craftspersons data source, or null.
     * @param dateStart The first date of the date range.
     * @param dateEnd The last date of the date range.
     */
    loadSchedule: function(restriction, startDate, endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.craftspersonsById = {};
        this.craftspersons.length = 0;

        var currentDate = new Date();
        var autoNumber = 0;
        var model = this;

        // load craftsperson
        var records = View.dataSources.get('craftspersonsDataSource').getRecords(restriction);
        _.each(records, function(record) {
            var cfId = record.getValue('cf.cf_id');
            var name = record.getValue('cf.name');
            var trId = record.getValue('cf.tr_id');

            var craftsperson = {
                autoNumber: autoNumber++,
                id: cfId,
                text: name,
                trade: trId,
                type: 'craftsperson',
                totals: {
                    availableHours: 0
                }
            }
            model.craftspersons.push(craftsperson);
            model.craftspersonsById[cfId] = craftsperson;
        });

        // load craftsperson regular schedules
        this._loadCraftspersonSchedules(startDate, endDate);
    },

    /**
     * Updates totals for specified craftsperson for specified date range. If the data range includes today's date, updates totals starting from today and ending at the end date.
     */
    updateCraftspersonTotals: function(craftsperson, startDate, endDate) {

        startDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), 0, 0, 0);

        craftsperson.totals.availableHours = 0;
        for (var date = new Date(startDate.getTime()); date.between(startDate, endDate); date = date.add(Date.DAY, 1)) {
            // get exceptions.
            var scheduleExceptions = _.filter(this.craftspersonScheduleVariances, function(scheduleException) {
                return scheduleException.appliesTo(craftsperson.id, date);
            });

            var schedule = this.findApplicableSchedule(craftsperson, date);
            if (!schedule) {
                // APP-998: The 'STANDARD' variances should be included in craftsperson totals available hours if user has no regular schedule.
                schedule = new CraftspersonSchedule();
            }

            var availableHours = schedule.getAvailableHours(date, scheduleExceptions);

            if (date.between(startDate, endDate)) {
                craftsperson.totals.availableHours += availableHours;
            }
        }

        if (craftsperson.totals.availableHours != 0) {
            craftsperson.totals.availableHours = parseFloat(craftsperson.totals.availableHours.toFixed(2));
        }
    },

    /**
     * get trade available hours.
     */
    getTradeHours: function(trId, dateStart, dateEnd) {
        var model = this;
        _.each(this.craftspersons, function(craftsperson) {
            model.updateCraftspersonTotals(craftsperson, dateStart, dateEnd);
        });

        var tradeHours = 0;
        _.each(this.craftspersons, function(craftsperson) {
            if (craftsperson.trade == trId) {
                tradeHours += parseFloat(craftsperson.totals.availableHours);
            }
        });

        return tradeHours;
    }

});