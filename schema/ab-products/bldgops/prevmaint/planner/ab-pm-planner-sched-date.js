
Date.prototype.getWeek = function() {
    return this.format('W');
};
Date.prototype.nextWeek = function (count) {
    var dayCount = 7;
    if (count) {
        dayCount  = count * 7;
    }
    this.setDate(this.getDate() + dayCount);
};
Date.prototype.prevWeek = function (count) {
    var weekCount = 7;
    if (count) {
        weekCount  = count * 7;
    }
    this.setDate(this.getDate() - weekCount);
};

/**
 * Controller for PM schedule date pop up.
 */
pmScheduleDateController = View.createController('pmScheduleDateController', {
    /**
     * parent selected PM scheudle code.
     */
    pmsId: null,
    
    /**
     * the old date_todo
     */
    dateBeforeChange: null,
    
    /**
     * After this view Initialized.
     */
    afterInitialDataFetch: function() {

        if (!this.pmsdGrid) {
            var pmsId = this.pmsdForm.getFieldValue('pmsd.pms_id');
            var pmsRestriction = new Ab.view.Restriction();
            pmsRestriction.addClause('pms.pms_id', pmsId, '=');
            this.pmsForm.refresh(pmsRestriction);

            var woId = this.pmsdForm.getFieldValue('pmsd.wo_id');
            if (woId) {
                var wrRestriction = new Ab.view.Restriction();
                wrRestriction.addClause('wrhwr.wo_id', woId, '=');
                this.wrGrid.refresh(wrRestriction);
                this.wrGrid.appendTitle(': ' + woId);
                this.wrGrid.setTitle(this.wrGrid.config.title + ': ' + woId);
                this.wrGrid.setPropertyOverridden('title');
            }
        } else {

            if (View.getOpenerView().panels.get('editPmScheduleTabs')) {
                this.pmsForm.show(false);
                this.resFromPmPlanner = View.getOpenerView().getOpenerView().resFromPmPlanner;
                this.pmsId = this.resFromPmPlanner.clauses[0].value;
            } else {
                this.pmsdGrid.actions.get('addNew').show(false);
                this.pmsdGrid.actions.get('delete').show(false);
                this.actionPanel.show(false);

                this.resFromPmPlanner = View.getOpenerView().resFromPmPlanner;
                var pmsRestriction = new Ab.view.Restriction();
                pmsRestriction.addClause('pms.pms_id', this.resFromPmPlanner.clauses[0].value, '=');
                this.pmsForm.refresh(pmsRestriction);
            }

            this.pmsdGrid.refresh(this.resFromPmPlanner);
            if (this.pmsdGrid.rows.length > 0) {
                $('pmsdGrid_row0_pmsd.date_todo').onclick();
            } else {
                jQuery('#pmsdForm_layoutWrapper').hide();
            }
        }
    },

    /**
     * Cancel Date.
     */
    pmsdForm_afterRefresh: function() {
        jQuery('#pmsdForm_layoutWrapper').show();
        if (this.pmsdForm.newRecord) {
            this.pmsdForm.enableField('pmsd.date_todo', true);
            this.pmsdForm.enableField('pmsd.date_latest', true);
            this.pmsdForm.enableField('pmsd.date_earliest', true);
            this.pmsdForm.actions.get('saveDate').show(true);
            this.pmsdForm.actions.get('cancelDate').show(false);
            this.pmsdForm.actions.get('reschedule').show(false);
            this.wrGrid.show(false);
            return;
        }

        this.showButtonByPmDateStatus();

        var canEditDate = 'Planned' == this.pmsdForm.record.getValue('pmsd.vf_pmsd_status');
        this.pmsdForm.enableField('pmsd.date_todo', canEditDate);
        this.pmsdForm.enableField('pmsd.date_latest', canEditDate);
        this.pmsdForm.enableField('pmsd.date_earliest', canEditDate);

        var woId = this.pmsdForm.getFieldValue('pmsd.wo_id');
        if (woId) {
            var wrRestriction = new Ab.view.Restriction();
            wrRestriction.addClause('wrhwr.wo_id', woId, '=');
            this.wrGrid.refresh(wrRestriction);
            this.wrGrid.appendTitle(': ' + woId);
            this.wrGrid.setTitle(this.wrGrid.config.title + ': ' + woId);
            this.wrGrid.setPropertyOverridden('title');
        } else {
            this.wrGrid.show(false);
        }
        
        this.dateBeforeChange = this.pmsdForm.getFieldValue('pmsd.date_todo');
    },

    /**
     * Add new date.
     */
    pmsdGrid_onAddNew: function() {
        this.pmsdForm.refresh(this.resFromPmPlanner, true);
    },

    /**
     * Delete Selected date.
     */
    pmsdGrid_onDelete: function() {
        var row, record, rows = this.pmsdGrid.getSelectedRows();

        for (var i = 0; i < rows.length; i++) {
            if (rows[i]['pmsd.wo_id']) {
                View.showMessage(getMessage('woCannotDeleted'));
                return;
            }
        }

        rows = this.pmsdGrid.getPrimaryKeysForSelectedRows();
        for (var i = 0; i < rows.length; i++) {
            row = rows[i];
            record = new Ab.data.Record({
                'pmsd.pms_id': rows[i]["pmsd.pms_id"],
                'pmsd.date_todo': rows[i]["pmsd.date_todo"]
            }, false);
            this.costLaborDS.deleteRecord(record);
        }

        if (rows.length > 0) {
            this.wrGrid.show(false);
            jQuery('#pmsdForm_layoutWrapper').hide();
            this.pmsdGrid.refresh();
            this.refreshParentCalendar();
        }
    },

    /**
     * Save Date.
     */
    pmsdForm_onSaveDate: function() {
        if(!this.validateForm()){
            return;
        }
        this.pmsdForm.save();
        
        var oldDateTodo = this.dateBeforeChange;
        var restriction = new Ab.view.Restriction();
        restriction.addClause('pmsd.pms_id', this.pmsdForm.getFieldValue('pmsd.pms_id'), '=');
        restriction.addClause('pmsd.date_todo', this.pmsdForm.getFieldValue('pmsd.date_todo'), '=');
        this.pmsdForm.refresh(restriction);
        if (this.pmsdGrid) {
            this.pmsdGrid.refresh();
            this.refreshParentCalendar();
        }else{
            var record = this.pmsdForm.getRecord();
            record.oldValues['pmsd.date_todo'] = Date.parseDate(oldDateTodo, 'Y-m-d');
            this.updateSelectedPmScheduledDateInCalendar(record);
            View.closeThisDialog();
        }
    },
    
    /**
     * validate date form.
     */
    validateForm: function() {
        var form = this.pmsdForm;
        var costLaborDS = this.costLaborDS;
        var dateCurrent = this.pmsdForm.getFieldValue('pmsd.date_todo');
        var dateEarliest = this.pmsdForm.getFieldElement('pmsd.date_earliest').value;
        var dateLatest = this.pmsdForm.getFieldElement('pmsd.date_latest').value;
        
        if (dateEarliest && dateLatest && compareLocalizedDates(dateLatest,dateEarliest)){
			View.showMessage(getMessage('EarliestdateAfterLatestDate'));
			return false;
		}
        
        if(!form.newRecord && this.dateBeforeChange != dateCurrent){
            var restriction = new Ab.view.Restriction();
            restriction.addClause('pmsd.pms_id', this.pmsdForm.getFieldValue('pmsd.pms_id'), '=');
            restriction.addClause('pmsd.date_todo', this.dateBeforeChange, '>');
            restriction.addClause('pmsd.date_todo', dateCurrent, '<=');
            
            if(costLaborDS.getRecords(restriction).length>0){
                View.showMessage(getMessage('cannotMovePastAfterNextDate'));
                return false;
            }
            
            restriction = new Ab.view.Restriction();
            restriction.addClause('pmsd.pms_id', this.pmsdForm.getFieldValue('pmsd.pms_id'), '=');
            restriction.addClause('pmsd.date_todo', this.dateBeforeChange, '<');
            restriction.addClause('pmsd.date_todo', dateCurrent, '>=');
            
            if(costLaborDS.getRecords(restriction).length>0){
                View.showMessage(getMessage('cannotMoveEarlierPriorDdate'));
                return false;
            }
        }
        
        return true;
    },

    /**
     * Cancel Date.
     */
    pmsdForm_onCancelDate: function() {
        this.pmsdForm.setFieldValue('pmsd.is_cancelled', 1);
        this.pmsdForm.save();
        this.pmsdForm.refresh();
        if (this.pmsdGrid) {
            this.pmsdGrid.refresh();
            this.refreshParentCalendar();
        }else{
            var record = this.pmsdForm.getRecord();
            record.oldValues['pmsd.is_cancelled'] = 0;
            this.updateSelectedPmScheduledDateInCalendar(record);
        }
        
    },

    /**
     * Reschedule Date.
     */
    pmsdForm_onReschedule: function() {
        this.pmsdForm.setFieldValue('pmsd.is_cancelled', 0);
        this.pmsdForm.save();
        this.pmsdForm.refresh();
        if (this.pmsdGrid) {
            this.pmsdGrid.refresh();
            this.refreshParentCalendar();
        }else{
            var record = this.pmsdForm.getRecord();
            record.oldValues['pmsd.is_cancelled'] = 1;
            this.updateSelectedPmScheduledDateInCalendar(record);
        }
        
    },
    
    /**
     * Delete Date.
     */
    pmsdForm_onDeleteDate: function() {
        var form = this.pmsdForm;
        var parameters = form.getParameters(true);
        var oldFieldValues = toJSON({'pmsd.pms_id':form.getFieldValue('pmsd.pms_id'), 'pmsd.date_todo':form.getFieldValue('pmsd.date_todo')});
        parameters.oldFieldValues = oldFieldValues;
        parameters.fieldValues = oldFieldValues;

        // call WFR to save form values
        var result = Workflow.runRuleAndReturnResult(form.deleteWorkflowRuleId, parameters, form.afterDelete,form);
        if (result.code == 'executed') {
            form.displayValidationResult(result);
        } 
        else {
            form.validationResult.valid = false;
            form.displayValidationResult(result);
        }
        
        this.refreshParentCalendar();
        
        if (this.pmsdGrid) {
            this.pmsdGrid.refresh();
            form.show(false);
        }else{
            View.closeThisDialog();
        }
        
    },
    
    /**
     * Refresh parent calendar.
     */
    refreshParentCalendar: function() {
        var filterController = null;
        if (View.getOpenerView().panels.get('editPmScheduleTabs')) {
            filterController = View.getOpenerView().getOpenerView().controllers.get('pmPlannerFilterController');
        }else{
            filterController = View.getOpenerView().controllers.get('pmPlannerFilterController');
        }
        
        filterController.pmSchedulesFilter_onFilter();
    },
    
    /**
     * Update selected pm schedule date in calendar.
     */
    updateSelectedPmScheduledDateInCalendar: function(record) {
        var pmPlannerController = null;
        if (View.getOpenerView().panels.get('editPmScheduleTabs')) {
            pmPlannerController = View.getOpenerView().getOpenerView().controllers.get('pmPlannerController');
        }else{
            pmPlannerController = View.getOpenerView().controllers.get('pmPlannerController');
        }
        
        pmPlannerController.updateSelectedPmScheduledDate(record);
    },

    /**
     * Show buttons by PM date Status.
     */
    showButtonByPmDateStatus: function() {
        var status = this.pmsdForm.record.getValue('pmsd.vf_pmsd_status');
        var woId = this.pmsdForm.record.getValue('pmsd.wo_id');
        this.pmsdForm.actions.get('saveDate').show('Planned' == status);
        this.pmsdForm.actions.get('cancelDate').show('Planned' == status);
        this.pmsdForm.actions.get('reschedule').show('Cancelled' == status);
        this.pmsdForm.actions.get('deleteDate').show(!valueExistsNotEmpty(woId));
    },

    /**
     * Edit PM Schedule.
     */
    pmsForm_onEditPMSchedule: function() {
        var pmsId = this.pmsForm.getFieldValue('pms.pms_id');
        var eqId = this.pmsForm.getFieldValue('pms.eq_id');
        var pmsRestriction = new Ab.view.Restriction();
        pmsRestriction.addClause('pms.pms_id', pmsId, '=');

        View.resFromPmPlanner = pmsRestriction;
        View.pmpType = eqId != '' ? 'EQ' : 'HK';
        View.openDialog('ab-pm-def-sched-form-mpsl.axvw');
    },

    /**
     * Generate new PM dates.
     */
    actionPanel_onGeneratePmDates: function() {
        var dateFrom = getIsoFormatDate(new Date());
        var dateTo = getIsoFormatDate(DateMath.add(new Date(), 'D', 14));

        var dateRangePanel = View.panels.get("dateRangePanel");
        dateRangePanel.show(true);
        dateRangePanel.showInWindow({
            title: getMessage('dateRangePanelTitle'),
            width: 550,
            height: 200
        });
        dateRangePanel.setFieldValue('dateS', dateFrom);
        dateRangePanel.setFieldValue('dateE', dateTo);
    },

    /**
     * Call WFR to Generate new PM dates.
     */
    dateRangePanel_onSubmit: function() {
        var controller = this;
        var dateRange = View.panels.get("dateRangePanel");
        var dateStart = dateRange.getFieldValue("dateS");
        var dateEnd = dateRange.getFieldValue("dateE");
        if (dateStart && dateEnd) {
            if (dateRangeInterval(dateStart, dateEnd) < 0) {
                View.showMessage(getMessage('error_date_range'));
                return;
            }
            if (dateRangeInterval(dateStart, getIsoFormatDate(new Date())) > 0) {
                View.showMessage(getMessage('error_datefrom_early'));
                return;
            }
        } else {
            View.showMessage(getMessage('error_date_range'));
            return;
        }
        var pmsidRestriction = " pms.pms_id = " + this.pmsId;
        var result = {};
        
        try {
            result = Workflow.callMethod('AbBldgOpsPM-PmEventHandler-checkPmsdModified', dateStart, dateEnd, pmsidRestriction.replace('pms.', 'pmsd.'));
        }  catch (e) {
            Workflow.handleError(e);
            return;
        }
        
        if(result.value){
            View.confirm(getMessage("confirmRetainModifiedDates"), function (button) {
                if (button === 'yes') {
                    try {
                        result = Workflow.callMethod('AbBldgOpsPM-PmEventHandler-PmScheduleGenerator',  dateStart, dateEnd, pmsidRestriction,true, false);
                    } 
                    catch (e) {
                        Workflow.handleError(e);
                    }
                    
                    View.showMessage(getMessage('startJobToGenerateScheduleDates'));
                    
                    dateRange.closeWindow();
                    var pmsdGrid = controller.pmsdGrid;
                    setTimeout(function() {
                        pmsdGrid.refresh();
                        controller.refreshParentCalendar();
                    },100);
                    
                }else{
                    try {
                        result = Workflow.callMethod('AbBldgOpsPM-PmEventHandler-PmScheduleGenerator',  dateStart, dateEnd, pmsidRestriction,true, true);
                    } 
                    catch (e) {
                        Workflow.handleError(e);
                    }
                    
                    View.showMessage(getMessage('startJobToGenerateScheduleDates'));
                    
                    dateRange.closeWindow();
                    var pmsdGrid = controller.pmsdGrid;
                    setTimeout(function() {
                        pmsdGrid.refresh();
                        controller.refreshParentCalendar();
                    },100);
                }
            });
            
        }else{
            // This method serve as a WFR to call a long running job generating schedule dates for specified date range and PM Schedules, file='PreventiveMaintenanceCommonHandler.java'
            try {
                result = Workflow.callMethod('AbBldgOpsPM-PmEventHandler-PmScheduleGenerator',  dateStart, dateEnd, pmsidRestriction,true, true);
            } 
            catch (e) {
                Workflow.handleError(e);
            }
            
            View.showMessage(getMessage('startJobToGenerateScheduleDates'));
            
            dateRange.closeWindow();
            var pmsdGrid = this.pmsdGrid;
            setTimeout(function() {
                pmsdGrid.refresh();
                controller.refreshParentCalendar();
            },100);
        }
        
    }

});

/**
 * Interval days betwen two date.
 */
function dateRangeInterval(startDate, endDate){
    var sDate = new Date(startDate.replace(/\-/g, "/"));
    var eDate = new Date(endDate.replace(/\-/g, "/"));
    var drDays = (eDate.getTime() - sDate.getTime()) / 3600 / 1000 / 24;
    return drDays;
}