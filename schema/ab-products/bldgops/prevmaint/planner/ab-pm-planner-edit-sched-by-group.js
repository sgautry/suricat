/**
 * Controller for edit schedule by group panel actions.
 */
pmPlannerEditByGroupController = View.createController('pmPlannerEditByGroupController', {
    /**
     * After this view Initialized.
     */
    afterInitialDataFetch: function() {
        this.setDefaultDateRange();
        this.editScheduleDateByGroupActionPanel.showField('dateToDo', false);
        this.editScheduleDateByGroupActionPanel.showField('dateLatest', false);
        this.editScheduleDateByGroupActionPanel.showField('dateEarliest', false);
    },

    /**
     * set default data range in the form.
     */
    setDefaultDateRange: function() {
        var openerView = View.getOpenerView();
        var calendarFromDate = openerView.calendarFromDate;
        var dateFrom = new Date();

        if (DateMath.after(calendarFromDate, dateFrom)) {
            dateFrom = calendarFromDate
        }

        this.editScheduleDateByGroupActionPanel.setFieldValue('dateFrom', getIsoFormatDate(dateFrom));
        this.editScheduleDateByGroupActionPanel.setFieldValue('dateTo', getIsoFormatDate(DateMath.add(dateFrom, DateMath.MONTH, 3)));
        this.editScheduleDateByGroupActionPanel.setFieldValue('dateToDo', getIsoFormatDate(dateFrom));
    },

    /**
     * Event handler for Submit button.
     */
    editScheduleDateByGroupActionPanel_onSubmit: function() {
        if (this.editScheduleDateByGroupActionPanel.canSave()) {
            var action = jQuery("input[name='editDatesAction']:checked").val();
            if (action == 'GenerateScheduleDates') {
                this.generateScheduleDates();
            }else if (action == 'AddNewDate') {
                this.addNewPmDates();
            }else if (action == 'CancelDate') {
                this.cancelPmDates();
            }else if (action == 'RestoreDate') {
                this.restorePmDates();
            }else if (action == 'DeleteDate') {
                this.deletePmDates();
            }
        }
    },

    /**
     * Event handler for Generate radio option.
     */
    generateScheduleDates: function() {
        var controller = this;
        var dateRange = View.panels.get("editScheduleDateByGroupActionPanel");
        var dateStart = dateRange.getFieldValue("dateFrom");
        var dateEnd = dateRange.getFieldValue("dateTo");
        if (dateStart && dateEnd) {
            if (dateRangeInterval(dateStart, dateEnd) < 0) {
                View.showMessage(getMessage('error_date_range'));
                return;
            }
            if (dateRangeInterval(dateStart, getIsoFormatDate(new Date())) > 0) {
                View.showMessage(getMessage('error_datefrom_early'));
                return;
            }

        }
        
        var pmsidRestriction = this.getPmsIdRestriction();
        var result = {};

        try {
            result = Workflow.callMethod('AbBldgOpsPM-PmEventHandler-checkPmsdModified', dateStart, dateEnd, pmsidRestriction.replace('pms.', 'pmsd.'));
        } catch (e) {
            Workflow.handleError(e);
            return;
        }

        if (result.value) {
            View.confirm(getMessage("confirmRetainModifiedDates"), function(button) {
                if (button === 'yes') {
                    try {
                        result = Workflow.callMethod('AbBldgOpsPM-PmEventHandler-PmScheduleGenerator', dateStart, dateEnd, pmsidRestriction, true, false);
                    } catch (e) {
                        Workflow.handleError(e);
                    }
                    
                    View.showMessage(getMessage('startJobToGenerateScheduleDates'));
                    
                    setTimeout(function() {
                        controller.refreshParentCalendar();
                    },100);

                } else {
                    try {
                        result = Workflow.callMethod('AbBldgOpsPM-PmEventHandler-PmScheduleGenerator', dateStart, dateEnd, pmsidRestriction, true, true);
                    } catch (e) {
                        Workflow.handleError(e);
                        return;
                    }
                    
                    View.showMessage(getMessage('startJobToGenerateScheduleDates'));
                    
                    setTimeout(function() {
                        controller.refreshParentCalendar();
                    },100);
                }
            });

        } else {
            // This method serve as a WFR to call a long running job generating schedule dates for specified date range and PM Schedules, file='PreventiveMaintenanceCommonHandler.java'
            try {
                result = Workflow.callMethod('AbBldgOpsPM-PmEventHandler-PmScheduleGenerator', dateStart, dateEnd, pmsidRestriction, true, true);
            } catch (e) {
                Workflow.handleError(e);
                return;
            }
            
            View.showMessage(getMessage('startJobToGenerateScheduleDates'));
            
            setTimeout(function() {
                controller.refreshParentCalendar();
            },100);
        }
        
    },
    

    /**
     * Event handler for Add new Date radio option.
     */
    addNewPmDates: function() {
        var dateRange = View.panels.get("editScheduleDateByGroupActionPanel");
        var dateToDo = dateRange.getFieldValue("dateToDo");
        var dateLatest = dateRange.getFieldValue("dateLatest");
        var dateEarliest = dateRange.getFieldValue("dateEarliest");
        if (dateRangeInterval(dateToDo, getIsoFormatDate(new Date())) > 0) {
            View.showMessage(getMessage('error_pmDate_early'));
            return;
        }
        
        var dateEarliestValue = dateRange.getFieldElement('dateEarliest').value;
        var dateLatestValue = dateRange.getFieldElement('dateLatest').value;
        
        if (dateEarliestValue && dateLatestValue && compareLocalizedDates(dateLatestValue,dateEarliestValue)){
            View.showMessage(getMessage('EarliestdateAfterLatestDate'));
            return false;
        }
        
        var pmsidRestriction = this.getPmsIdRestriction();
        var result = {};

        try {
            result = Workflow.callMethod('AbBldgOpsPM-PmEventHandler-addPmDatesByGroup', dateToDo, dateLatest, dateEarliest, pmsidRestriction);
        } catch (e) {
            Workflow.handleError(e);
            return;
        }
        
        View.showMessage(getMessage('addPmDatesSuccess'));
        this.refreshParentCalendar();
    },
    
    /**
     * Event handler for Cancel radio option.
     */
    cancelPmDates: function() {
        var dateRange = View.panels.get("editScheduleDateByGroupActionPanel");
        var dateStart = dateRange.getFieldValue("dateFrom");
        var dateEnd = dateRange.getFieldValue("dateTo");
        if (dateStart && dateEnd) {
            if (dateRangeInterval(dateStart, dateEnd) < 0) {
                View.showMessage(getMessage('error_date_range'));
                return;
            }
            
            if (dateRangeInterval(dateStart, getIsoFormatDate(new Date())) > 0) {
                View.showMessage(getMessage('error_datefrom_early'));
                return;
            }
        }
        
        var pmsidRestriction = this.getPmsIdRestriction();
        var result = {};

        try {
            result = Workflow.callMethod('AbBldgOpsPM-PmEventHandler-cancelPmDatesByGroup', dateStart, dateEnd, pmsidRestriction.replace('pms.', 'pmsd.'));
        } catch (e) {
            Workflow.handleError(e);
            return;
        }
        
        View.showMessage(getMessage('cancelPmDatesSuccess'));
        
        this.refreshParentCalendar();
    },
    
    /**
     * Event handler for restore radio option.
     */
    restorePmDates: function() {
        var dateRange = View.panels.get("editScheduleDateByGroupActionPanel");
        var dateStart = dateRange.getFieldValue("dateFrom");
        var dateEnd = dateRange.getFieldValue("dateTo");
        if (dateStart && dateEnd) {
            if (dateRangeInterval(dateStart, dateEnd) < 0) {
                View.showMessage(getMessage('error_date_range'));
                return;
            }
            
            if (dateRangeInterval(dateStart, getIsoFormatDate(new Date())) > 0) {
                View.showMessage(getMessage('error_datefrom_early'));
                return;
            }
        }
        
        var result = {};

        var pmsidRestriction = this.getPmsIdRestriction();
        try {
            result = Workflow.callMethod('AbBldgOpsPM-PmEventHandler-restorePmDatesByGroup', dateStart, dateEnd, pmsidRestriction.replace('pms.', 'pmsd.'));
        } catch (e) {
            Workflow.handleError(e);
            return;
        }
        
        View.showMessage(getMessage('restorePmDatesSuccess'));
        
        this.refreshParentCalendar();
    },
    
    /**
     * Event handler for Delete radio option.
     */
    deletePmDates: function() {
        var dateRange = View.panels.get("editScheduleDateByGroupActionPanel");
        var dateStart = dateRange.getFieldValue("dateFrom");
        var dateEnd = dateRange.getFieldValue("dateTo");
        if (dateStart && dateEnd) {
            if (dateRangeInterval(dateStart, dateEnd) < 0) {
                View.showMessage(getMessage('error_date_range'));
                return;
            }
            
            if (dateRangeInterval(dateStart, getIsoFormatDate(new Date())) > 0) {
                View.showMessage(getMessage('error_datefrom_early'));
                return;
            }
        }
        
        var pmsidRestriction = this.getPmsIdRestriction();
        var result = {};

        try {
            result = Workflow.callMethod('AbBldgOpsPM-PmEventHandler-deletePmDatesByGroup', dateStart, dateEnd, pmsidRestriction.replace('pms.', 'pmsd.'));
        } catch (e) {
            Workflow.handleError(e);
            return;
        }
        
        View.showMessage(getMessage('deletePmDatesSuccess'));
        this.refreshParentCalendar();
    },
    
    /**
     * Refresh parent calendar.
     */
    refreshParentCalendar: function() {
        var filterController = View.getOpenerView().controllers.get('pmPlannerFilterController');
        filterController.pmSchedulesFilter_onFilter();
    },
    
    
    /**
     * Helper method to get parent view pms_id restriction.
     */
    getPmsIdRestriction: function() {
        var pmsRecords = View.getOpenerView().resFromPmPlannerByGroup;
        var pmsidRestriction = " pms.pms_id IN (";
        if(pmsRecords.length>0){
            pmsidRestriction =  pmsidRestriction+ pmsRecords[0]['pms.pms_id'];
            for (var j = 1; j < pmsRecords.length; j++) {
               pmsidRestriction = pmsidRestriction+","  ;
               pmsidRestriction = pmsidRestriction+pmsRecords[j]['pms.pms_id'] ;
            }
        }
        pmsidRestriction = pmsidRestriction+ ") ";
        
        return pmsidRestriction;
    }
});

/**
 * Event handler to change the radio option.
 */
function changeDatesAction() {
    var form = View.panels.get('editScheduleDateByGroupActionPanel');
    pmPlannerEditByGroupController.setDefaultDateRange();

    var action = jQuery("input[name='editDatesAction']:checked").val();
    if (action == 'AddNewDate') {
        form.showField('dateToDo', true);
        form.showField('dateLatest', true);
        form.showField('dateEarliest', true);
        form.showField('dateFrom', false);
        form.showField('dateTo', false);
    } else {
        form.showField('dateToDo', false);
        form.showField('dateLatest', false);
        form.showField('dateEarliest', false);
        form.showField('dateFrom', true);
        form.showField('dateTo', true);
    }
}

/**
 * Interval days betwen two date.
 */
function dateRangeInterval(startDate, endDate){
    var sDate = new Date(startDate.replace(/\-/g, "/"));
    var eDate = new Date(endDate.replace(/\-/g, "/"));
    var drDays = (eDate.getTime() - sDate.getTime()) / 3600 / 1000 / 24;
    return drDays;
}