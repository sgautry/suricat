/**
 * Controller for Financial impact tab by month chart panel.
 */
var abBldgopsReportWrBudgetAndCostMonthChartController = View.createController('abBldgopsReportWrBudgetAndCostMonthChartController', {

    /**
     * After this view Initialized.
     */
    afterInitialDataFetch: function() {
        this.abBldgopsReportWrBudgetAndCostByMonthChart.chartControl.chart.config.dataTipsFormat = 'compact';

        // register response for main view events
        var mainViewEventAggregator = View.getOpenerView().getOpenerView().eventAggregator;
        mainViewEventAggregator.on('app:bldgops:pmPlanner:setFilterParameter', this.setFilterParameter, this);
        mainViewEventAggregator.on('app:bldgops:pmPlanner:filter', this.refreshChartPanel, this);
        mainViewEventAggregator.on('app:bldgops:pmPlanner:afterModifyMonths', this.afterModifyMonths, this);

        // set default monthNumber
        var sideCarPanel = View.getOpenerView().getOpenerView().panels.get('pmSchedulesFilter');
        var monthNumber = sideCarPanel.getSidecar().get('monthNumber');
        if (!valueExists(monthNumber)) {
            sideCarPanel.getSidecar().set('monthNumber', '12');
            sideCarPanel.getSidecar().save();
        }

        // show default month number
        this.setDisplayeMonthNumber();

        // apply restriction when first load
        View.getOpenerView().getOpenerView().controllers.get('pmPlannerFilterController').applyFilterRestrictions();

        // load the chart panel
        this.refreshChartPanel();

    },

    /**
     * set filter parameters in current view.
     */
    setFilterParameter: function(fieldName, fieldValue) {
        this.abBldgopsReportWrBudgetAndCostByMonthChart.addParameter(fieldName, fieldValue);
    },

    /**
     * set chart panel parameters.
     */
    setPanelParameter: function() {
        var sideCarPanel = View.getOpenerView().getOpenerView().panels.get('pmSchedulesFilter');
        var dateStart = DateMath.findMonthStart(new Date());
        var dateEnd = DateMath.findMonthEnd(DateMath.add(dateStart, DateMath.MONTH, parseInt(sideCarPanel.getSidecar().get('monthNumber')) - 1));
        var panel = this.abBldgopsReportWrBudgetAndCostByMonthChart;
        panel.addParameter('dateStart', getIsoFormatDate(dateStart));
        panel.addParameter('dateEnd', getIsoFormatDate(dateEnd));
        panel.addParameter('scheduledWorkCostsParam', getMessage('scheduledWorkCostsParam'));
        panel.addParameter('budgetedCostsParam', getMessage('budgetedCostsParam'));
    },

    /**
     * refresh the chart paenl
     */
    refreshChartPanel: function() {

        // set chart panel parameters
        this.setPanelParameter();

        // show and refresh the chart
        var panel = this.abBldgopsReportWrBudgetAndCostByMonthChart;
        panel.show(true);
        panel.refresh();

        // show loading spiral panel
        var loadPanel = this._createLoadPanel();
        loadPanel.show();
        var scope = this;
        setTimeout(function() {
            scope._removeLoadPanel();
        }, 100);
    },

    /**
     * create loading spiral panel
     */
    _createLoadPanel: function() {
        jQuery('#abBldgopsReportWrBudgetAndCostByMonthChart').append(jQuery('<div id="plannedLoadingPanel"></div>'));
        jQuery("#plannedLoadingPanel").dxLoadPanel({
            message: getMessage('Loading'),
            visible: false
        });
        return jQuery("#plannedLoadingPanel").dxLoadPanel("instance");
    },

    /**
     * remove loading spiral panel
     */
    _removeLoadPanel: function() {
        jQuery("#plannedLoadingPanel").remove();
    },

    /**
     * Modify Month number.
     */
    modifyMonths: function(action) {
        var sideCarPanel = View.getOpenerView().getOpenerView().panels.get('pmSchedulesFilter');
        jQuery("input[type=radio][name=monthNumber][value=" + sideCarPanel.getSidecar().get('monthNumber') + "]").attr("checked", true);
        this.monthNumberSettingPanel.showInWindow({
            width: 450,
            height: 200,
            anchor: $('gearMenu'),
            title: '',
            closeButton: true
        });
    },

    /**
     * Save month number.
     */
    monthNumberSettingPanel_onSaveMonthNumber: function(action) {
        var sideCarPanel = View.getOpenerView().getOpenerView().panels.get('pmSchedulesFilter');
        var value = jQuery("input[name='monthNumber']:checked").val();
        sideCarPanel.getSidecar().set('monthNumber', value);
        sideCarPanel.getSidecar().save();
        View.getOpenerView().getOpenerView().eventAggregator.trigger('app:bldgops:pmPlanner:afterModifyMonths');
        this.monthNumberSettingPanel.closeWindow();
    },

    /**
     * after save month number.
     */
    afterModifyMonths: function(action) {
        this.setDisplayeMonthNumber();
        this.refreshChartPanel();
    },

    /**
     * Show month number in UI.
     */
    setDisplayeMonthNumber: function() {
        var sideCarPanel = View.getOpenerView().getOpenerView().panels.get('pmSchedulesFilter');
        var monthNumber = sideCarPanel.getSidecar().get('monthNumber');
        var title = '';
        if ('1' == monthNumber) {
            title = String.format(getMessage('monthNumberSettingPanelTitle1'), monthNumber);
        } else {
            title = String.format(getMessage('monthNumberSettingPanelTitle'), monthNumber);
        }
        this.abBldgopsReportWrBudgetAndCostByMonthChart.actions.get('gearMenu').actions.get('modifyMonth').setTitle(title);
    }

})

/**
 * Event handler to click chart
 */
function onPlanMonthBarChartClick(obj) {
    var monthValue = obj.selectedChartData['afm_cal_dates.month'];
    var planAndBudget = obj.selectedChartData['selectedSecondaryGrouping'].key;
    var controller = View.controllers.get("abBldgopsReportWrBudgetAndCostMonthChartController");
    var chart = View.panels.get('abBldgopsReportWrBudgetAndCostByMonthChart');

    if (planAndBudget == getMessage('scheduledWorkCostsParam')) {
        var pmsdGrid = View.panels.get('pmsdGrid');
        pmsdGrid.parameters = chart.parameters;
        pmsdGrid.addParameter('excludeCancelDate', true);
        var res = " ${sql.yearMonthOf('pmsd.date_todo')} ='" + monthValue + "' ";
        pmsdGrid.refresh(res);
        pmsdGrid.showInWindow({
            width: 1000,
            height: 800,
            title: getMessage('pmsDateGridTitle'),
            closeButton: true
        });
    } else if (planAndBudget == getMessage('budgetedCostsParam')) {
        if (View.dialog == null) {
            var budgetMonthRes = "budget_item.cost_cat_id = 'MAINT - PREVENTIVE EXPENSE' AND ( budget_item.date_start is not null and budget_item.date_end is not null "
                    + " AND ${sql.yearMonthOf('budget_item.date_start')} &lt;='" + monthValue + "' " + " AND  ${sql.yearMonthOf('budget_item.date_end')} &gt;='" + monthValue + "' "
                    + " or budget_item.date_start is null and budget_item.date_end is null " + " or  budget_item.date_start is null and ${sql.yearMonthOf('budget_item.date_end')} &gt;='" + monthValue
                    + "' " + " or  budget_item.date_end is null  and ${sql.yearMonthOf('budget_item.date_start')} &lt;='" + monthValue + "' )";

            var filterSiteCode = chart.parameters['pms.site_id'];
            var filterBlCode = chart.parameters['pms.bl_id'];
            var filterAcCode = chart.parameters['pmp.ac_id'];
            if (filterSiteCode) {
                budgetMonthRes += ' AND (EXISTS(SELECT 1 FROM bl WHERE bl.bl_id = budget_item.bl_id AND bl.site_id IN(' + filterSiteCode + ')))';
            }

            if (filterBlCode) {
                budgetMonthRes += ' AND (budget_item.bl_id IN(' + filterBlCode + '))';
            }

            if (filterAcCode) {
                budgetMonthRes += ' AND (budget_item.ac_id IN(' + filterAcCode + '))';
            }

            View.openDialog('ab-bldgops-report-budget-details-pop.axvw', budgetMonthRes);
        }

    }
}