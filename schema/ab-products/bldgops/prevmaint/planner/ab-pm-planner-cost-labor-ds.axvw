<!-- PM Planner Labor Analysis view. -->
<view version="2.0">

   <!-- Data source to query trades -->
   <dataSource id="tradeDS">
       <table name="tr" />
       <field name="tr_id" />
       <parameter name="pmp.tr_id" dataType="verbatim" value=""/>
       <restriction type="sql" enabled="pmp.tr_id" sql="tr.tr_id IN(${parameters['pmp.tr_id']})"/>
   </dataSource>
   
   <!-- Data source to query Average On Demand Hours -->
   <dataSource id="averageOnDemandHoursDS">
       <sql>
           SELECT ${sql.isNull('SUM(CF_HOURS.hours_total)/52','0')} ${sql.as} avg_od_hours, cf.work_team_id, cf.tr_id FROM  
               (select wrcf.cf_id, wrcf.wr_id, wrcf.hours_total from wrcf union all select hwrcf.cf_id, hwrcf.wr_id, hwrcf.hours_total from hwrcf) CF_HOURS
			LEFT JOIN cf ON cf.cf_id = CF_HOURS.cf_id
			LEFT JOIN wrhwr ON wrhwr.wr_id = CF_HOURS.wr_id 
			LEFT JOIN probtype ON wrhwr.prob_type = probtype.prob_type AND probtype.prob_type != 'PREVENTIVE MAINT'
			WHERE ${sql.daysBeforeCurrentDate('wrhwr.date_completed')} &lt; 365 AND probtype.prob_class = 'OD' AND ${sql.getRestriction('consoleRestriction','1=1')} 
			GROUP BY cf.work_team_id, cf.tr_id
        </sql>
        
       <table name="cf" />
       <field name="tr_id" dataType="text" />
       <field name="work_team_id" dataType="text"/>
       <field name="avg_od_hours" dataType="number" decimals="2"/>
       
       <restriction id="consoleRestriction" type="sql" sql=" 1=1
           AND ${sql.getRestriction('pms.site_id','1=1')} 
            AND ${sql.getRestriction('pms.bl_id','1=1')}  
                 AND ${sql.getRestriction('pms.fl_id','1=1')} 
                  AND ${sql.getRestriction('pms.rm_id','1=1')} 
                   AND ${sql.getRestriction('pms.dv_id','1=1')} 
                    AND ${sql.getRestriction('pms.dp_id','1=1')} 
                      AND ${sql.getRestriction('pms.sla_work_team_id','1=1')} 
                        AND ${sql.getRestriction('pmp.ac_id','1=1')} 
         "/>
        
        <!-- - Filter optional restrictions, enabled by filter values  -->
        <parameter name="pmp.ac_id" dataType="verbatim" value=""/>
        <restriction id="pmp.ac_id" type="sql" enabled="pmp.ac_id" sql="wrhwr.ac_id IN(${parameters['pmp.ac_id']})"/>
        <parameter name="pms.site_id" dataType="verbatim" value=""/>
        <restriction id="pms.site_id" type="sql" enabled="pms.site_id" sql="wrhwr.site_id IN(${parameters['pms.site_id']})"/>
        <parameter name="pms.bl_id" dataType="verbatim" value=""/>
        <restriction  id="pms.site_id" type="sql" enabled="pms.bl_id" sql="wrhwr.bl_id IN(${parameters['pms.bl_id']})"/>
        <parameter name="pms.fl_id" dataType="verbatim" value=""/>
        <restriction id="pms.fl_id" type="sql" enabled="pms.fl_id" sql="wrhwr.fl_id IN(${parameters['pms.fl_id']})"/>
        <parameter name="pms.rm_id" dataType="verbatim" value=""/>
        <restriction id="pms.rm_id" type="sql" enabled="pms.rm_id" sql="wrhwr.rm_id IN(${parameters['pms.rm_id']})"/>
        <parameter name="pms.dv_id" dataType="verbatim" value=""/>
        <restriction id="pms.dv_id" type="sql" enabled="pms.dv_id" sql="wrhwr.dv_id IN(${parameters['pms.dv_id']})"/>
        <parameter name="pms.dp_id" dataType="verbatim" value=""/>
        <restriction id="pms.dp_id"  type="sql" enabled="pms.dp_id" sql="wrhwr.dp_id IN(${parameters['pms.dp_id']})"/> 
        <parameter name="pms.sla_work_team_id" dataType="verbatim" value=""/>
        <restriction id="pms.sla_work_team_id" type="sql" enabled="pms.sla_work_team_id" sql="cf.work_team_id IN(${parameters['pms.sla_work_team_id']})"/>
        <parameter name="pmp.tr_id" dataType="verbatim" value=""/>
        <restriction type="sql" enabled="pmp.tr_id" sql="cf.tr_id IN(${parameters['pmp.tr_id']})"/>
   </dataSource>
   
   <!-- Data source to query all trades assigned to PM schedule -->
   <dataSource id="pmsTradeDS">
       <sql>
            select pmpstr.tr_id, pms.pms_id, pmpstr.hours_req
            FROM pms 
            LEFT OUTER JOIN pmp ON pmp.pmp_id=pms.pmp_id
            join pmpstr on pmpstr.pmp_id = pmp.pmp_id
            where ${sql.getRestriction('ab-pm-planner-shared-restriction.axvw', 'sharedPmsDS', 'consoleRestrictionWithoutTrade')} AND ${sql.getRestriction('pmpstr.tr_id','1=1')}
            
            union all
            
            select  pmp.tr_id, pms.pms_id, CASE WHEN pms.hours_calc > 0 THEN pms.hours_calc ELSE pms.hours_est END AS hours_req
            FROM pms 
            LEFT OUTER JOIN pmp ON pmp.pmp_id=pms.pmp_id
            where pmp.tr_id IS NOT NULL AND ${sql.getRestriction('ab-pm-planner-shared-restriction.axvw', 'sharedPmsDS', 'consoleRestrictionWithoutTrade')} AND ${sql.getRestriction('pmp.tr_id','1=1')}
        </sql>
        
       <table name="pmpstr" />
       <field name="tr_id" dataType="text"/>
       <field name="pms_id" dataType="integer"/>
       <field name="hours_req" dataType="number" decimals="2"/>
       
        <!-- - Filter optional restrictions, enabled by filter values  -->
        <parameter name="pmp.tr_id" dataType="verbatim" value=""/>
        <restriction id="pmp.tr_id" type="sql" enabled="pmp.tr_id" sql="pmp.tr_id IN(${parameters['pmp.tr_id']})"/>
        <restriction id="pmpstr.tr_id" type="sql" enabled="pmp.tr_id" sql="pmpstr.tr_id IN (${parameters['pmp.tr_id']})"/>
   </dataSource>
    
   <!-- Data source to query all cost and hours in every pmsd record -->
   <dataSource id="costLaborDS">
        <table name="pmsd" role="main"/>
        <table name="pms" role="standard"/>
        <field table="pmsd" name="pms_id"/>
        <field table="pmsd" name="date_todo"/>
        <field table="pmsd" name="date_latest"/>
        <field table="pmsd" name="date_earliest"/>
        <field table="pmsd" name="date_orig_todo"/>
        <field table="pmsd" name="wo_id"/>
        <field table="pmsd" name="date_completed"/>
        <field table="pmsd" name="is_cancelled"/>
        <field table="pms" name="pmp_id"/>
        <field table="pms" name="eq_id"/>
        <field table="pms" name="bl_id"/>
        <field table="pms" name="site_id"/>
        <field table="pms" name="sla_work_team_id"/>
        <field table="pms" name="dv_id"/>
        <field table="pms" name="dp_id"/>
        <field table="pms" name="eq_std" dataType="text">
            <sql dialect="generic">
                (SELECT eq.eq_std FROM eq WHERE eq.eq_id = pms.eq_id)
            </sql>
        </field>
        <field table="pms" name="tr_id" dataType="text">
            <sql dialect="generic">
                (SELECT pmp.tr_id FROM pmp WHERE pmp.pmp_id = pms.pmp_id)
            </sql>
        </field>
        <field name="ac_id" dataType="text">
            <sql dialect="generic">
                (SELECT pmp.ac_id FROM pmp WHERE pmp.pmp_id = pms.pmp_id)
            </sql>
        </field>
        <field name="location" dataType="text">
            <sql dialect="generic">
                pms.bl_id${sql.concat}'-'${sql.concat}pms.fl_id${sql.concat}'-'${sql.concat}pms.rm_id
            </sql>
        </field>
        
        <field table="pmsd" name="vf_pmsd_status" dataType="text">
            <sql dialect="generic">
                CASE WHEN pmsd.is_cancelled = 1 THEN 'Cancelled'
                     WHEN ${sql.isNull('pmsd.date_latest','pmsd.date_todo')} &gt;= ${sql.currentDate} AND pmsd.wo_id IS NULL THEN 'Planned'
                     WHEN pmsd.wo_id IS NOT NULL and pmsd.date_completed IS NULL and ${sql.isNull('pmsd.date_latest','pmsd.date_todo')} &gt;= ${sql.currentDate} THEN 'Work Order Generated'
                     WHEN pmsd.date_completed IS NOT NULL and pmsd.date_completed &lt;= ${sql.isNull('pmsd.date_latest','pmsd.date_todo')} THEN 'Work Order Completed'
                     WHEN pmsd.date_completed IS NOT NULL and pmsd.date_completed &gt; ${sql.isNull('pmsd.date_latest','pmsd.date_todo')} THEN 'Completed Late'
                     WHEN pmsd.wo_id IS NOT NULL and pmsd.date_completed IS NULL and ${sql.isNull('pmsd.date_latest','pmsd.date_todo')} &lt; ${sql.currentDate} THEN 'Missed (Work Order Not Completed)'
                     WHEN pmsd.wo_id IS NULL and ${sql.isNull('pmsd.date_latest','pmsd.date_todo')}  &lt; ${sql.currentDate} THEN 'Missed (Work Order Not Generated)'
                ELSE '' END
            </sql>
        </field>
        <field table="pmsd" name="vf_est_labor_hours" dataType="number" decimals="2">
            <sql dialect="generic">
              ${sql.isNull ("
                (CASE WHEN pms.hours_calc > 0 THEN pms.hours_calc ELSE pms.hours_est END) + (CASE WHEN EXISTS(SELECT 1 FROM pmpstr WHERE pmpstr.pmp_id = pms.pmp_id) THEN (SELECT sum(pmpstr.hours_req) FROM pmpstr WHERE pmpstr.pmp_id = pms.pmp_id) ELSE 0 END)
               ", "0")}         
            </sql>
        </field>
        <field table="pmsd" name="vf_est_cost" dataType="number" decimals="2">
            <sql dialect="generic">
               ${sql.isNull (" 
                    (CASE WHEN pms.hours_calc > 0 
                                    THEN pms.hours_calc * (CASE WHEN (SELECT tr.rate_hourly FROM pmp LEFT OUTER JOIN tr ON pmp.tr_id=tr.tr_id WHERE pmp.pmp_id = pms.pmp_id) IS NULL THEN 0 ELSE (SELECT tr.rate_hourly FROM pmp LEFT OUTER JOIN tr ON pmp.tr_id=tr.tr_id WHERE pmp.pmp_id = pms.pmp_id) END )
                                    ELSE pms.hours_est * (CASE WHEN (SELECT tr.rate_hourly FROM pmp LEFT OUTER JOIN tr ON pmp.tr_id=tr.tr_id WHERE pmp.pmp_id = pms.pmp_id)  IS NULL THEN 0 ELSE (SELECT tr.rate_hourly FROM pmp LEFT OUTER JOIN tr ON pmp.tr_id=tr.tr_id WHERE pmp.pmp_id = pms.pmp_id) END )
                     END) + 
                     
                     
                    (CASE WHEN EXISTS(SELECT 1 FROM pmpstr WHERE pmpstr.pmp_id = pms.pmp_id) THEN (SELECT sum(pmpstr.hours_req* tr.rate_hourly) FROM pmpstr LEFT OUTER JOIN tr ON pmpstr.tr_id=tr.tr_id WHERE pmpstr.pmp_id = pms.pmp_id) ELSE 0 END)
                 ", "0")}
                
                + 
                
                ${sql.isNull (" (SELECT sum(pmpspt.qty_required* (CASE WHEN (select pt_store_loc_pt.cost_unit_avg from pt_store_loc_pt where pt_store_loc_pt.part_id =  pmpspt.part_id AND pt_store_loc_pt.pt_store_loc_id = pms.pt_store_loc_id) is null then (select pt.cost_unit_avg from pt where pt.part_id =  pmpspt.part_id) else (select pt_store_loc_pt.cost_unit_avg from pt_store_loc_pt where pt_store_loc_pt.part_id =  pmpspt.part_id AND pt_store_loc_pt.pt_store_loc_id = pms.pt_store_loc_id) end))  FROM pmpspt WHERE pmpspt.pmp_id = pms.pmp_id) ", "0")}
                
                +
                
                ${sql.isNull (" (SELECT sum(pmpstt.hours_req* tt.rate_hourly) FROM pmpstt LEFT OUTER JOIN tt ON pmpstt.tool_type=tt.tool_type WHERE pmpstt.pmp_id = pms.pmp_id) ", "0")}
            </sql>
            <sql dialect="sqlserver">
               ${sql.isNull (" 
                    (CASE WHEN pms.hours_calc > 0 
                                    THEN pms.hours_calc * (CASE WHEN (SELECT tr.rate_hourly FROM pmp LEFT OUTER JOIN tr ON pmp.tr_id=tr.tr_id WHERE pmp.pmp_id = pms.pmp_id) IS NULL THEN 0 ELSE (SELECT tr.rate_hourly FROM pmp LEFT OUTER JOIN tr ON pmp.tr_id=tr.tr_id WHERE pmp.pmp_id = pms.pmp_id) END )
                                    ELSE pms.hours_est * (CASE WHEN (SELECT tr.rate_hourly FROM pmp LEFT OUTER JOIN tr ON pmp.tr_id=tr.tr_id WHERE pmp.pmp_id = pms.pmp_id)  IS NULL THEN 0 ELSE (SELECT tr.rate_hourly FROM pmp LEFT OUTER JOIN tr ON pmp.tr_id=tr.tr_id WHERE pmp.pmp_id = pms.pmp_id) END )
                     END) + 
                     
                     
                    (CASE WHEN EXISTS(SELECT 1 FROM pmpstr WHERE pmpstr.pmp_id = pms.pmp_id) THEN (SELECT sum(pmpstr.hours_req* tr.rate_hourly) FROM pmpstr LEFT OUTER JOIN tr ON pmpstr.tr_id=tr.tr_id WHERE pmpstr.pmp_id = pms.pmp_id) ELSE 0 END)
                 ", "0")}
                
                + 
                
                ${sql.isNull (" (SELECT sum(x.pt_cost) FROM (SELECT (pmpspt.qty_required* (CASE WHEN (select pt_store_loc_pt.cost_unit_avg from pt_store_loc_pt where pt_store_loc_pt.part_id =  pmpspt.part_id AND pt_store_loc_pt.pt_store_loc_id = pms.pt_store_loc_id) is null then (select pt.cost_unit_avg from pt where pt.part_id =  pmpspt.part_id) else (select pt_store_loc_pt.cost_unit_avg from pt_store_loc_pt where pt_store_loc_pt.part_id =  pmpspt.part_id AND pt_store_loc_pt.pt_store_loc_id = pms.pt_store_loc_id) end)) AS pt_cost FROM pmpspt WHERE pmpspt.pmp_id = pms.pmp_id) X) ", "0")}
                
                +
                
                ${sql.isNull (" (SELECT sum(pmpstt.hours_req* tt.rate_hourly) FROM pmpstt LEFT OUTER JOIN tt ON pmpstt.tool_type=tt.tool_type WHERE pmpstt.pmp_id = pms.pmp_id) ", "0")}
            </sql>
        </field>
        <field table="pmsd" name="vf_actual_hours" dataType="number" decimals="0">
            <sql dialect="generic">
                ${sql.isNull (" (SELECT sum(wrcf.hours_straight) + sum(wrcf.hours_over) + sum(wrcf.hours_double) FROM wrcf LEFT OUTER JOIN wr ON wrcf.wr_id=wr.wr_id WHERE wr.wo_id = pmsd.wo_id and wr.pms_id = pmsd.pms_id) ", "0")}
                +
                ${sql.isNull (" (SELECT sum(hwrcf.hours_straight) + sum(hwrcf.hours_over) + sum(hwrcf.hours_double) FROM hwrcf LEFT OUTER JOIN hwr ON hwrcf.wr_id=hwr.wr_id WHERE hwr.wo_id = pmsd.wo_id and hwr.pms_id = pmsd.pms_id) ", "0")}
                
            </sql>
        </field>
        <field table="pmsd" name="vf_actual_cost" dataType="number" decimals="2">
            <sql dialect="generic">
                ${sql.isNull (" (SELECT sum(wrhwr.cost_total) FROM wrhwr WHERE wrhwr.wo_id = pmsd.wo_id and wrhwr.pms_id = pmsd.pms_id) ", "0")}
            </sql>
        </field>        
        
        <restriction type="sql" sql="${sql.getRestriction('ab-pm-planner-shared-restriction.axvw', 'sharedPmsDS', 'consoleRestriction')}"/>
        <parameter name="excludeCancelDate" dataType="boolean" value="false"/>
        <restriction type="sql" enabled="excludeCancelDate" sql="pmsd.is_deferred = 0 AND pmsd.is_cancelled = 0"/>
        <parameter name="useDateRange" dataType="boolean" value="false"/>
        <parameter name="dateStart" dataType="verbatim" value="2010-01-01"/>
        <parameter name="dateEnd" dataType="verbatim" value="2010-12-31"/>
        <restriction type="sql" enabled="useDateRange"  sql=" ${sql.yearMonthDayOf('pmsd.date_todo')} &gt;= '${parameters['dateStart']}'  AND ${sql.yearMonthDayOf('pmsd.date_todo')} &lt;= '${parameters['dateEnd']}' "/>
   </dataSource>
    
   
   <!-- Data source to query craftspersons. -->
   <dataSource id="craftspersonsDataSource">
        <table name="cf"/>
        <field name="cf_id"/>
        <field name="name"/>
        <field name="tr_id"/>
        
        <parameter name="pmp.tr_id" dataType="verbatim" value=""/>
        <restriction type="sql" enabled="pmp.tr_id" sql="cf.tr_id IN(${parameters['pmp.tr_id']})"/>
        <parameter name="pms.sla_work_team_id" dataType="verbatim" value=""/>
        <restriction type="sql" enabled="pms.sla_work_team_id" sql="cf.work_team_id IN(${parameters['pms.sla_work_team_id']})"/>

        <sortField name="tr_id" ascending="true"/>
        <sortField name="name" ascending="true"/>
    </dataSource>

    <!-- Data source to query craftsperson schedules. -->
    <dataSource id="craftspersonSchedulesDataSource">
        <table name="cf_schedules_assign"/>
        <table name="cf_schedules" role="standard"/>
        <field name="auto_number"/>
        <field name="cf_schedule_id"/>
        <field name="cf_id"/>
        <field name="is_seasonal"/>
        <field name="date_start"/>
        <field name="date_end"/>
        <field name="schedule_type" table="cf_schedules"/>
        <field name="schedule_name" table="cf_schedules"/>
    </dataSource>

    <!-- Data source to query craftsperson schedule days. -->
    <dataSource id="craftspersonScheduleDaysDataSource">
        <table name="cf_schedules_days"/>

        <field name="cf_schedule_id"/>
        <field name="day_of_week"/>
        <field name="time_start"/>
        <field name="time_end"/>
        <field name="rate_type"/>
    </dataSource>

    <!-- Data source to query craftsperson schedule variances. -->
    <dataSource id="craftspersonScheduleVariancesDataSource">
        <table name="cf_schedules_variances"/>

        <field name="cf_id"/>
        <field name="date_start"/>
        <field name="date_end"/>
        <field name="time_start"/>
        <field name="time_end"/>
        <field name="sched_vars_type"/>
    </dataSource>

</view>