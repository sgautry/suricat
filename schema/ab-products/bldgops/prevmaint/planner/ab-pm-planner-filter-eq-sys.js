/**
 * System analysis controller.
 */
View.createController('eqSysAnalysisController', {

    /**
     * After this view load.
     */
    afterViewLoad: function() {
        // set tree level node restriction
        this.updateRestrictionLevel();
    },

    /**
     * After this view Initialized.
     */
    afterInitialDataFetch: function() {
        this.eqSysInventoryTreePanel.refresh();
    },

    /**
     * Equipment system tree level restriction.
     */
    updateRestrictionLevel: function() {
        this.eqSysInventoryTreePanel.updateRestrictionForLevel = updateTreeRestrictionForLevel;
    },

    /**
     * Format tree node after generate tree level 1 node.
     */
    eqSysInventoryTreePanel_afterGeneratingTreeNode: function(node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    /**
     * Format tree node after generate tree level 2 node.
     */
    eqSysInventoryTreeLvl_2_afterGeneratingTreeNode: function(node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    /**
     * Format tree node after generate tree level 3 node.
     */
    eqSysInventoryTreeLvl_3_afterGeneratingTreeNode: function(node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    /**
     * Format tree node after generate tree level 4 node.
     */
    eqSysInventoryTreeLvl_4_afterGeneratingTreeNode: function(node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    /**
     * Format tree node after generate tree level 5 node.
     */
    eqSysInventoryTreeLvl_5_afterGeneratingTreeNode: function(node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    /**
     * Format tree node after generate tree level 6 node.
     */
    eqSysInventoryTreeLvl_6_afterGeneratingTreeNode: function(node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    /**
     * Format tree node after generate tree level 7 node.
     */
    eqSysInventoryTreeLvl_7_afterGeneratingTreeNode: function(node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    /**
     * Format tree node after generate tree level 8 node.
     */
    eqSysInventoryTreeLvl_8_afterGeneratingTreeNode: function(node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    /**
     * Format tree node after generate tree level 9 node.
     */
    eqSysInventoryTreeLvl_9_afterGeneratingTreeNode: function(node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },

    /**
     * Set tree node label and also a node separator. The separator is displayed to drop an element on the same node level.
     */
    formatLabelNode: function(node) {
        var depend = node.data['eq_system.eq_id_depend'];
        var systemLvl = node.data['eq_system.system_level'];
        if (node.data['eq_system.system_level.raw']) {
            systemLvl = node.data['eq_system.system_level.raw'];
        }
        var label = node.data['eq_system.system_name'] + ' (' + depend + ')';
        var nodeLabel = '<div class="nodeLabel">';
        var labelId = 'label' + node.id + '_0';
        var separatorId = 'separator' + node.id + '_0';
        nodeLabel += '<div id="' + labelId + '" class="label color_' + systemLvl + '"><span class="ygtvlabel">' + label + '</span></div>';
        nodeLabel += '<div id="' + separatorId + '" class="separator"></div>';
        nodeLabel += '</div>';
        label = node.label.replace('{label_html}', nodeLabel);
        node.label = label;
    }

});

/**
 * Event handler to click tree nodes
 */
function onClickNodeHandler(context) {
    var controller = View.controllers.get('eqSysAnalysisController');
    var crtTreeNode = View.panels.get(context.command.parentPanelId).lastNodeClicked;
    var eqId = crtTreeNode.data['eq_system.eq_id_depend'];
    var parentFilterPanel = View.getOpenerView().panels.get('pmSchedulesFilter');
    var parentFilterController = View.getOpenerView().controllers.get('pmPlannerFilterController');
    parentFilterPanel.setFieldValue('eq.eq_sys_name', eqId);
    parentFilterController.pmSchedulesFilter_onFilter();
    View.closeThisDialog();
}

/**
 * Update tree restriction for level and set filter restriction.
 */
function updateTreeRestrictionForLevel(parentNode, level, restriction) {
    if (level > 0) {
        restriction.removeClause('eq_system.auto_number');
        restriction.addClause('eq_system.eq_id_master', parentNode.data['eq_system.eq_id_depend'], '=');
    }
    if (this.parameters.filterRestriction) {
        var filterRestriction = this.parameters.filterRestriction;
        var levelRestriction = '';
        for (var i = (level + 1), length = 10; i < length; i++) {
            levelRestriction += 'eq.level' + i + '=eq_system.eq_id_depend ';
            if (i < (length - 1)) {
                levelRestriction += ' OR ';
            }
        }
        this.parameters.filterRestriction = filterRestriction.replace(/%{levelRestriction}/g, levelRestriction);
    }
}