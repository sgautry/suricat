PMPlanner.Calendar.WeekCellNew = Base.extend({

    _view: null,
    _works: null,

    _id: null,
    _cell: null,

    constructor: function (devExpressOption, cellOption) {
        this._initializationFields(devExpressOption, cellOption);
        this._clean();
        this._render();
    },

    _initializationFields: function (devExpressOption, cellOption) {
        var dataId = parseInt(devExpressOption.key),
            dataWeek = devExpressOption.column.id;

        this._id = 'pmPlannedJob_' + dataId + dataWeek + '_' + cellOption.view.getCalendarView().getDataSource().getCounter();
        cellOption.view.getCalendarView().getDataSource().incCounter();
        this._cell = devExpressOption.cellElement;

        this._view = cellOption.view;
        this._works = cellOption.works;
        this._rowIndex = devExpressOption.rowIndex;
    },

    _render: function () {
        if (this._works.length == 1) {
            this._renderSingleWork();
            return;
        }
        this._renderMultiWorks();
    },

    _clean: function () {
        this._cell.html('');
        this._cell.css({
            padding: 0
        });
        if (this._cell.hasClass('blockDatesPastWeek_missed'))
            this._cell.removeClass('blockDatesPastWeek_missed');
        if (this._cell.hasClass('blockDatesPastWeek_completed_on_completed'))
            this._cell.removeClass('blockDatesPastWeek_completed_on_completed');
        if (this._cell.hasClass('blockDatesPastWeek_completed_late_completed'))
            this._cell.removeClass('blockDatesPastWeek_completed_late_completed');
    },

    _renderSingleWork: function () {
        var work = this._works[0];
        switch (work.status) {
            case PMPlanner.DataSource.PastJobs_Missed:
                this._renderPastSingleWork('blockDatesPastWeek_missed',PMPlanner.Constants.IMG_WARNING);
                break;
            case PMPlanner.DataSource.PastJobs_CompletedOnTime:
                this._renderPastSingleWork('blockDatesPastWeek_completed_on_completed',PMPlanner.Constants.IMG_COMPLETED);
                break;
            case PMPlanner.DataSource.PastJobs_CompletedLate:
                this._renderPastSingleWork('blockDatesPastWeek_completed_late_completed',PMPlanner.Constants.IMG_COMPLETED);
                break;
            default:
                this._renderFutureWeek();
                break;
        }
    },

    _renderPastSingleWork: function (cssClass, image) {
        var left = PMPlanner.Calendar.CELL_WIDTH / 2 - 7;
        if (image == PMPlanner.Constants.IMG_COMPLETED) {
            left += 5;
        }

        this._cell.addClass(cssClass);
        this._cell.append(PMPlanner.Tools.Render.img({
            image: image,
            cssClass: 'blockDatesPastWeek_image',
            cssFields: {
                'margin-left': left + 'px',
                'margin-top': '11px'
            }
        }));
        this._cell.on('click', this._onClickCell());
    },

    _renderMultiWorks: function () {
        if (this._isMultiStatusesWeek()){
            this._renderMultiStatusesWeek();
            return;
        }
        this._renderFutureWeek();
    },

    _isMultiStatusesWeek: function () {
        var result = false;
        for (var i = 0; i < this._works.length; i++) {
            var work = this._works[i];
            if (work.status != PMPlanner.DataSource.FutureJobs_Planned &&
                work.status != PMPlanner.DataSource.FutureJobs_Generated &&
                work.status != PMPlanner.DataSource.FutureJobs_Cancelled) {
                result = true;
                break;
            }
        }
        return result;
    },

    _renderMultiStatusesWeek: function () {
        var multiStatusesBlock = PMPlanner.Tools.Render.div();

        this._renderCompletedWorks(multiStatusesBlock);
        this._renderFutureWorks(multiStatusesBlock);
        this._renderMissedWorks(multiStatusesBlock);

        this._cell.css('cursor', 'pointer');
        this._cell.append(multiStatusesBlock);
        this._cell.on('click', this._onClickCell());
    },

    _getCountCompletedWorks: function () {
        var result = 0;
        this._works.forEach(function (work) {
            if (work.status == PMPlanner.DataSource.PastJobs_CompletedOnTime ||
                work.status == PMPlanner.DataSource.PastJobs_CompletedLate)
                result++;
        });
        return result;
    },
    _getCountMissedWorks: function () {
        var result = 0;
        this._works.forEach(function (work) {
            if (work.status == PMPlanner.DataSource.PastJobs_Missed)
                result++;
        });
        return result;
    },
    _getCountFutureWorks: function () {
        var result = 0;
        this._works.forEach(function (work) {
            if (work.status == PMPlanner.DataSource.FutureJobs_Planned ||
                work.status == PMPlanner.DataSource.FutureJobs_Generated)
                result++;
        });
        return result;
    },

    _renderCompletedWorks: function (div) {
        var count = this._getCountCompletedWorks();
        if (count == 0)
            return;

        var completedBlock = PMPlanner.Tools.Render.div({
            cssClass: 'groupPastWeek_completed',
            cssFields: {
                float: 'left'
            }
        });
        completedBlock.append(PMPlanner.Tools.Render.img({
            image: PMPlanner.Constants.IMG_COMPLETED,
            cssClass: 'groupPastWeek_completed_imgCheck'
        }));
        completedBlock.append(PMPlanner.Tools.Render.text({
            text: count,
            cssClass: 'groupPastWeek_completed_text',
            cssFields: {
                'padding-left': '17px'
            }
        }));
        div.append(completedBlock);
    },
    _renderMissedWorks: function (div) {
        var count = this._getCountMissedWorks();
        if (count == 0)
            return;

        var missedBlock = PMPlanner.Tools.Render.div({
            cssClass: 'groupPastWeek_missed'
        });
        missedBlock.append(PMPlanner.Tools.Render.div({
            cssClass: 'groupPastWeek_missed_triangle',
            cssFields: {
                'margin-top': '-7px !important'
            }
        }));
        missedBlock.append(PMPlanner.Tools.Render.text({
            text: count,
            cssClass: 'groupPastWeek_missed_text'
        }));
        div.append(missedBlock);
    },
    _renderFutureWorks: function (div) {
        var count = this._getCountFutureWorks();
        if (count == 0)
            return;

        var countCompletedWorks = this._getCountCompletedWorks();
        var configText, configSquare;
        if (countCompletedWorks == 0) {
            configText = {
                float: 'left',
                'margin-top': '17px',
                'margin-left': '3px'
            };
            configSquare = {
                float: 'left',
                'margin-top': '18px',
                'margin-left': '3px'
            };
        } else {
            configText = {
                float: 'left',
                'margin-left': '-5px',
                'margin-bottom': '-40px',
                'margin-top': '17px'
            };
            configSquare = {
                float: 'left',
                'margin-top': '18px',
                'margin-left': '-24px'
            };
        }

        var futureBlock = PMPlanner.Tools.Render.div();
        futureBlock.append(PMPlanner.Tools.Render.div({
            cssClass: 'groupFutureWeek_square',
            cssFields: configSquare
        }));
        futureBlock.append(PMPlanner.Tools.Render.text({
            text: count,
            cssClass: 'groupFutureWeek_text',
            cssFields: configText
        }));
        div.append(futureBlock);
    },

    _renderFutureWeek: function () {
        var week = PMPlanner.Tools.Render.div({
            id: this._id,
            cssClass: 'blockDatesFutureWeek'
        });

        this._cell.append(week);
        for (var i = 1; i <= 7; i++) {
            var work = PMPlanner.Tools.getWork(this._works, i);
            var select = this._isSelectDay(work);
            new PMPlanner.Calendar.WeekDay({
                id: this._id + i + '_' + this._view.getCalendarView().getDataSource().getCounter(),
                weekDomElement: week,
                rowIndex: this._rowIndex,
                view: this._view,
                work: work,
                select: select
            });
        }
    },

    _isSelectDay: function (work) {
        var result = false;
        var selectWork = this._view.getCalendarView().getDataSource().getSelectBlock();
        if (selectWork != null && work != null) {
            result = work.id == selectWork._work.id &&
                    work.groupName == selectWork._work.groupName &&
                    work.date.getTime() == selectWork._work.date.getTime();
        }
        return result;
    },


    _onClickCell: function () {
        var scope = this;
        return function () {
            var ds = scope._view.getCalendarView().getDataSource();
            var selectBlock = ds.getSelectBlock();
            if (selectBlock != null) {
                selectBlock.deselect();
                ds.deselectBlock();
            }

            var records = [];
            for (var i = 0; i < scope._works.length; i++) {
                records.push(scope._works[i].record);
            }
            scope._view.callEvent('onEditPastPmScheduleDate', [records]);
        };
    }

});

PMPlanner.Calendar.WeekDay = Base.extend({

    _id: null,
    _view: null,
    _work: null,
    _select: null,
    _rowIndex: null,

    _countClick: null,
    _onClickBracket: null,
    _dayDomElement: null,


    constructor: function (option) {
        this._id = option.id;
        this._view = option.view;
        this._work = option.work;
        this._select = option.select;

        this._countClick = 0;
        this._onClickBracket = true;

        this._dayDomElement = null;
        this._rowIndex = option.rowIndex;

        if (this._id == null)
            return;

        this._view.getCalendarView().getDataSource().incCounter();
        this._render(option.weekDomElement);
    },

    select: function () {
        if (!this._onClickBracket) {
            this._onClickBracket = true;
            return;
        }


        if (this._countClick == 0) {
            this._selectDay();
            this._countClick++;
            return;
        }
        if (this._countClick == 1) {
            this._view.callEvent('onEditPmScheduleDate', [this._work.record]);
            this._countClick++;
            return;
        }
        if (this._countClick == 2) {
            this.deselect();
            this._countClick = 0;
        }
    },

    deselect: function () {
        if(!this._dayDomElement)
            return;
        this._dehighlightBlock();
        this._removeArrows();
        this._countClick = 0;
        this._select = false;
        this._view.getCalendarView().getDataSource().deselectBlock();
    },

    updateSelect: function () {
        if(!this._dayDomElement)
            return;
        this._removeArrows();
        this._selectDay();
    },

    _render: function (weekDomElement) {
        var scope = this;
        var day = PMPlanner.Tools.Render.div({
            id: this._id,
            cssFields: {
                float: 'left',
                width: (100/7) +'%',
                height: 36
            }
        });
        day.on('click', this._onClick());

        weekDomElement.append(day);
        if (this._work == null) {
            return;
        }

        switch (this._work.status) {
            case PMPlanner.DataSource.FutureJobs_Planned:
                day.addClass('blockDatesFutureWeek_open_work');
                break;
            case PMPlanner.DataSource.FutureJobs_Generated:
                day.addClass('blockDatesFutureWeek_work');
                break;
            case PMPlanner.DataSource.FutureJobs_Cancelled:
                day.addClass('blockDatesFutureWeek_canceled');
                day.append(PMPlanner.Tools.Render.img({
                    image: PMPlanner.Constants.IMG_CANCELED
                }));
                break;
        }

        this._tooltip = new PMPlanner.Tooltip({
            elem: day,
            cellID: this._id,
            tooltipID: this._id + 'Tooltip',
            onRenderText: function (contentElement) {
                contentElement.append(PMPlanner.Tools.Render.text({
                    text: PMPlanner.Tooltip.formatDate(scope._work.date, scope._view._configUser.dataSource),
                    cssFields: {
                        'text-align': 'left'
                    }
                }));

                if (scope._view.isShowLabor()){
                    var tradeHours = scope._view.getDataSource().getLaborHoursInWork(scope._work.id, scope._work.week);
                    var tradeCount = 0;
                    jQuery.each(tradeHours, function(trId, hours) {
                        tradeCount++;

                    });
                    if(tradeCount == 0){
                        contentElement.append(PMPlanner.Tools.Render.text({
                            text: PMPlanner.Constants.TEXT_EstimatedLaborHoursZERO,
                            cssFields: {
                                'text-align': 'left'
                            }
                        }));
                    } else if(tradeCount == 1){
                        jQuery.each(tradeHours, function(trId, hours) {
                            contentElement.append(PMPlanner.Tools.Render.text({
                                text: PMPlanner.Constants.TEXT_EstimatedLaborHours + ' - ' + trId + ': ' + PMPlanner.Tools.formattedNumeric(hours.toFixed(2)),
                                cssFields: {
                                    'text-align': 'left'
                                }
                            }));
                        });

                    } else{
                        contentElement.append(PMPlanner.Tools.Render.text({
                            text: PMPlanner.Constants.TEXT_EstimatedLaborHours,
                            cssFields: {
                                'text-align': 'left'
                            }
                        }));

                        jQuery.each(tradeHours, function(trId, hours) {
                            contentElement.append(PMPlanner.Tools.Render.text({
                                text: ' --- ' + trId + ': ' + PMPlanner.Tools.formattedNumeric(hours.toFixed(2)),
                                cssFields: {
                                    'text-align': 'left'
                                }
                            }));
                        });
                    }
                }

                if (scope._view.isShowCosts())
                    contentElement.append(PMPlanner.Tools.Render.text({
                        text: PMPlanner.Constants.TEXT_EstimatedCost + ': ' + PMPlanner.Constants.TEXT_CurrencySymbol + PMPlanner.Tools.formattedNumeric(scope._view.getDataSource().getCostInWork(scope._work.id, scope._work.week)),
                        cssFields: {
                            'text-align': 'left'
                        }
                    }));
            }
        });
        this._dayDomElement = day;
        if (this._select) {
            setTimeout(function () {
                scope.select();
            },100);

        }

    },

    _onClick: function () {
        var scope = this;
        return function () {
            if (!scope._select) {
                var ds = scope._view.getCalendarView().getDataSource();
                var selectBlock = ds.getSelectBlock();
                if (selectBlock != null){
                    selectBlock.deselect();
                    ds.deselectBlock();
                }
            }
            if (scope._work != null)
                scope.select();

        };
    },

    _selectDay: function () {
        this._select = true;
        this._highlightBlock();
        this._view.getCalendarView().getDataSource().selectBlock(this);

        if (this._work.status == PMPlanner.DataSource.FutureJobs_Generated ||
            this._work.status == PMPlanner.DataSource.FutureJobs_Cancelled)
            return;

        this._renderArrows();
        this._renderBrackets();


    },

    _highlightBlock: function () {
        if (this._work.status == PMPlanner.DataSource.FutureJobs_Cancelled) {
            var img = this._dayDomElement.find('img');
            img.attr('src', PMPlanner.Constants.IMG_CANCELED_SELECT);
        } else {
            this._dayDomElement.addClass('blockDatesFutureWeek_highlight');
        }
    },
    _dehighlightBlock: function () {
        if (this._work.status == PMPlanner.DataSource.FutureJobs_Cancelled) {
            var img = this._dayDomElement.find('img');
            img.attr('src', PMPlanner.Constants.IMG_CANCELED);
        } else {
            this._dayDomElement.removeClass('blockDatesFutureWeek_highlight');
        }
    },

    _renderArrows: function () {
        var isMove = this._isMove(),
            scope = this;
        if (isMove.left) {
            var prevArrow = PMPlanner.Tools.Render.img({
                image: PMPlanner.Constants.IMG_ARROW_PREV,
                cssClass: 'blockDatesFutureWeek_arrow_prev',
                onClick: function () {
                    var record = scope._work.record;
                    scope._view.callEvent('onMoveWeekDay', [scope, 'prev']);
                    scope._view.callEvent('onAfterPmScheduleDateChanged', [record]);
                }
            });

            this._dayDomElement.append(prevArrow);
            var cssLeft = prevArrow[0].offsetLeft;
            var leftArrow = cssLeft - 23;
            prevArrow.css('left', leftArrow + 'px');
        }

        if (isMove.right) {
            this._dayDomElement.append(PMPlanner.Tools.Render.img({
                image: PMPlanner.Constants.IMG_ARROW_NEXT,
                cssClass: 'blockDatesFutureWeek_arrow_next',
                onClick: function () {
                    var record = scope._work.record;
                    scope._view.callEvent('onMoveWeekDay', [scope, 'next']);
                    scope._view.callEvent('onAfterPmScheduleDateChanged', [record]);
                }
            }));
        }
    },

    _renderBrackets: function () {
        var dataEarliest = this._work.dateEarliest,
            dataLatest = this._work.dateLatest,
            dateCurrent = this._work.date,
            isRenderLeftBracket = true,
            isRenderRightBracket = true,
            scope = this;

        if (dataEarliest == '')
            isRenderLeftBracket = false;
        if (dataLatest == '')
            isRenderRightBracket = false;

        if (!isRenderLeftBracket && !isRenderRightBracket)
            return;

        if (isRenderLeftBracket) {
            this._renderBracketInDate(dataEarliest, {
                image: PMPlanner.Constants.IMG_BRACKET_PREV,
                cssClass: 'bracket_dataLatest_equal',
                onClick: function () {
                    scope._onClickBracket = false;
                    if (dateCurrent.getTime() == dataEarliest.getTime())
                        scope._onClickBracket = true;
                }
            });
        }

        if (isRenderRightBracket) {
            var bracket = this._renderBracketInDate(dataLatest, {
                image: PMPlanner.Constants.IMG_BRACKET_NEXT,
                cssClass: 'bracket_dataLatest_equal',
                onClick: function () {
                    scope._onClickBracket = false;
                    if (dateCurrent.getTime() == dataLatest.getTime())
                        scope._onClickBracket = true;
                }
            });
            var leftBracket = bracket[0].offsetLeft + PMPlanner.Calendar.CELL_WIDTH / 7;
            bracket.css('left', leftBracket + 'px');
        }
    },

    _renderBracketInDate: function (date, option) {
        var cellLatest = this._view.callEvent('onFindCellObjectInGrid', [this._rowIndex, date]);
        var bracket = PMPlanner.Tools.Render.img(option);
        if (cellLatest != null) {
            var dayLatest = date.getDay() - 1;
            if (dayLatest == -1) {
                dayLatest = 6;
            }

            this._dayDomElement.append(bracket);
            var cssLeft = cellLatest[0].offsetLeft;
            var leftBracket = parseInt(cssLeft) + (PMPlanner.Calendar.CELL_WIDTH / 7 * dayLatest);
            bracket.css('left', leftBracket + 'px');
        }

        return bracket;
    },

    _removeArrows: function () {
        if(this._work.status == PMPlanner.DataSource.FutureJobs_Cancelled)
            return;
        
        var imgs = this._dayDomElement.find('img');
        if (imgs)
            imgs.remove();
    },

    _isMove: function () {
        var result = {};

        var nextWeek = new Date(this._work.date.getTime());
        nextWeek.nextWeek();
        var prevWeek = new Date(this._work.date.getTime());
        prevWeek.prevWeek();

        var prevColumn = this._getColumn(PMPlanner.Calendar.WeekColumn.formatWeek(prevWeek)),
            nextColumn = this._getColumn(PMPlanner.Calendar.WeekColumn.formatWeek(nextWeek));

        result.left = this._isMoveInPrevWeek(prevColumn);
        result.right = this._isMoveInNextWeek(nextColumn);

        return result;
    },

    _getColumn: function (columnWeek) {
        var result = null;
        var weekColumns = this._view.getCalendarView().getDataSource().getWeekColumns();

        for (var week in  weekColumns) {
            var weekColumn = weekColumns[week];
            if (columnWeek == weekColumn.getFormat()) {
                result = weekColumn;
                break;
            }
        }

        return result;
    },

    _isMoveInPrevWeek: function (prevColumn) {
        if (prevColumn == null || !prevColumn.isFutureWeek())
            return false;

        return this._checkWorksInWeek(prevColumn, 'prev');

    },

    _isMoveInNextWeek: function (nextColumn) {
        if (nextColumn == null)
            return false;

        return this._checkWorksInWeek(nextColumn, 'next');
    },

    _checkWorksInWeek: function (column, direction) {
        var works = this._view.getDataSource().getWork(this._work.id, column.getFormat());
        var currentDate = this._work.date,
            date,
            compare,
            i;

        if (works != null) {
            for (i = 0; i < works.length; i++) {
                date = works[i].date;
                var newDate = new Date(currentDate.getTime());

                if(direction == 'next') {
                    newDate.nextWeek();
                    compare = this._compareFullDate(newDate, date);
                    if (compare == 'equal' || compare == 'more')
                        return false;

                } else {
                    newDate.prevWeek();
                    compare = this._compareFullDate(newDate, date);
                    if (compare == 'equal' || compare == 'less')
                        return false;
                }
            }
        }

        var currentWorks = this._view.getDataSource().getWork(this._work.id, this._work.week);
        if (currentWorks != null) {
            for (i = 0; i < currentWorks.length; i++) {
                date = currentWorks[i].date;

                if(direction == 'next') {
                    compare = this._compareFullDate(currentDate, date);
                    if (compare == 'less')
                        return false;

                } else {
                    compare = this._compareFullDate(currentDate, date);
                    if (compare == 'more')
                        return false;
                }
            }
        }
        return true;
    },

    _compareFullDate: function (first, second) {
        var result = this._compareDate(first, second, 'Y');
        if (result == 'equal') {
            result = this._compareDate(first, second, 'n');
            if (result == 'equal') {
                result = this._compareDate(first, second, 'j');
            }
        }
        return result;
    },

    _compareDate: function (first, second, scale) {
        var result = 'none';
        var firstValue = parseInt(first.format(scale));
        var secondValue = parseInt(second.format(scale));
        if (firstValue == secondValue) {
            result = 'equal';
        } else if (firstValue > secondValue) {
            result = 'more';
        } else {
            result = 'less';
        }

        return result;
    },


});