PMPlanner.Calendar = {

    View: Base.extend({

        _countUserColumns: null,
        _view: null,
        _isClickArrow: null,

        _monthColumns: null,
        _weekColumns: null,

        _displayCountWeek: null,
        _displayCountMonth: null,

        _arrRowIndex: null,

        constructor: function (columnCustoms, view) {


            this._view = view;
            this._columnCustoms = columnCustoms;
            this._viewDataSource = view.getDataSource();
            this._countUserColumns = columnCustoms.length;
            this._dataSource = new PMPlanner.Calendar.DataSource();
            this._displayCountWeek = PMPlanner.Calendar.View.COUNT_WEEK_DISPLAY;
            this._displayCountMonth = PMPlanner.Calendar.View.COUNT_MONTH;
            this._isClickArrow = false;
            this._groupTotalStatistics = {};
            this._arrRowIndex = {};

            var sidecar = View.panels.get('gridPanel').getSidecar();
            var displayWeeks = sidecar.get('displayWeeks');
            if (valueExists(displayWeeks)) {
                this._displayCountWeek = displayWeeks;
                this._displayCountMonth = displayWeeks == 16?7:6;
            }

            Date.prototype.getWeek = function() {
                return this.format('W');
            };
            Date.prototype.nextWeek = function (count) {
                var dayCount = 7;
                if (count) {
                    dayCount  = count * 7;
                }
                this.setDate(this.getDate() + dayCount);
            };
            Date.prototype.prevWeek = function (count) {
                var weekCount = 7;
                if (count) {
                    weekCount  = count * 7;
                }
                this.setDate(this.getDate() - weekCount);
            };

            this._addEvents();
        },

        getDataSource: function () {
            return this._dataSource;
        },

        getRowIndex: function (dataKey) {
            return this._arrRowIndex[dataKey];
        },

        setCountWeekForDisplay: function (count) {
            this._displayCountWeek = count;
            if (count == 16) {
                this._displayCountMonth = 7;
            } else {
                this._displayCountMonth = 6;
            }

            this._view.getDataSource().clean();
            this._view.getDataSource().shouldQueryData();
            this._view.getDataSource().shouldQueryStatisticsData();
            jQuery('#' + this._view._configUser.container +'Grid').remove();
            this._view._createViewGrid();
        },

        getCountWeeksForDisplay: function () {
            return this._displayCountWeek;
        },

        configureGridColumns: function (dateStart) {
            var gridColumns = [];

            for (var i = 0; i < this._columnCustoms.length; i++) {
                var column = jQuery.extend({}, this._columnCustoms[i]);
                column.view = this._view;
                if (column.fieldName)
                    column.fieldName = DataGridSource.configGridFieldName(this._view._configUser.dataSource, column.fieldName);

                gridColumns.push(this._columnCustoms[i]);
                this._dataSource.addTextColumn(column);
            }

           // var date1 = new Date(2014, 0, 14);
            var date = PMPlanner.Calendar.WeekColumn.getDayInWeek(new Date(),0);
            date.prevWeek(4);

            if(dateStart){
                date = dateStart;
            }

            var weekCount = 0,
                weekIndex = 1,
                columnIndex = this._countUserColumns;

            for (var monthIndex = 0; monthIndex < this._displayCountMonth; monthIndex++) {
                var month = date.getMonth();
                var index = 0;

                var optionMonth = {
                    monthIndex: monthIndex,
                    columnIndex: columnIndex,
                    view: this._view,
                    date: new Date(date),
                    firstMonth: false,
                    lastMonth: false
                };
                if (monthIndex == 0) {
                    optionMonth['firstMonth'] = true;
                }

                if (weekCount >= this._displayCountWeek) {
                    optionMonth['date'] = null;
                }
                this._dataSource.addMonthColumn(optionMonth);
                columnIndex++;

                var optionWeek = {
                    weekIndex: weekIndex,
                    monthIndex: monthIndex,
                    columnIndex: columnIndex,
                    view: this._view,
                    date: new Date(date)
                };
                if (weekCount >= this._displayCountWeek) {
                    optionWeek['date'] = null;
                }

                this._dataSource.addWeekColumn(optionWeek);
                columnIndex++;

                var config = this._configureMonthColumn(monthIndex, weekIndex);
                if (weekCount < this._displayCountWeek) {
                    weekCount++;
                    date.nextWeek();
                } else {
                    config['visible'] = false;
                }
                gridColumns.push(config);
                weekIndex++;
                index++;

                for (;index < PMPlanner.Calendar.View.COUNT_WEEK_IN_MONTH; index++) {
                    optionWeek = {
                        weekIndex: weekIndex,
                        monthIndex: monthIndex,
                        columnIndex: columnIndex,
                        view: this._view,
                        date: new Date(date)
                    };
                    if (date.getMonth() != month || weekCount >= this._displayCountWeek) {
                        optionWeek['date'] = null;
                    }
                    this._dataSource.addWeekColumn(optionWeek);
                    columnIndex++;
                    var weekConfig = this._dataSource.getConfigWeekColumn(weekIndex);
                    if (date.getMonth() != month || weekCount >= this._displayCountWeek) {
                        weekConfig['visible'] = false;
                    } else {
                        weekCount++;
                        date.nextWeek();
                        this._dataSource.incrementWeekInMonthColumn(monthIndex);
                    }
                    gridColumns.push(weekConfig);

                    weekIndex++;
                }

                if (weekCount >= this._displayCountWeek) {
                    this._dataSource.getMonthColumn(monthIndex).isLastMonth(true);
                }
            }
            return gridColumns;
        },

        _updateTotalStatistics: function (countMonth) {
            var scope = this;
            var loadPanel = PMPlanner.Tools.createLoadPanel(this._view);
            loadPanel.show();
            setTimeout(function () {
                scope._view.getDataSource().setCountMonthForStatistics(countMonth);
                scope._view.getDataSource().loadStatistics();
                for (var key in scope._groupTotalStatistics) {
                    scope._groupTotalStatistics[key].update();
                }
                scope._view.getObjectToolsMenu().renderMenu();
                loadPanel.hide();
                PMPlanner.Tools.removeLoadPanel();
            }, 100);


        },

        _addEvents: function () {
            this._view.addEvent('onHeaderCellPrepared', this._headerCellPrepared, this);
            this._view.addEvent('onGroupCellPrepared', this._groupCellPrepared, this);
            this._view.addEvent('onCellPrepared', this._cellPrepared, this);
            this._view.addEvent('onLoadViewData', this._loadViewData, this);
            this._view.addEvent('onAfterViewLoad', this._afterLoadView, this);

            this._view.addEvent('onClickNextWeeks', this._clickNextWeeks, this);
            this._view.addEvent('onClickPrevWeeks', this._clickPrevWeeks, this);

            this._view.addEvent('onMoveWeekDay', this._moveWeekDay, this);
            this._view.addEvent('onFindCellObjectInGrid', this._findCellObjectInGrid, this);
        },
        _headerCellPrepared: function (option) {
            var column = this._dataSource.getColumn(option.column.dataField);
            if (column != null)
                column.stylingHeader(option);
            else  {
                if (option.column.icon && option.column.icon == 'edit') {
                    this._view.getObjectToolsMenu().renderView(option);
                }
                option.cellElement.addClass(PMPlanner.Calendar.TextColumn.CSS_HEADER_NONE);
            }
        },
        _groupCellPrepared: function (option) {
            var column = this._dataSource.getColumn(option.column.dataField);
            if (column != null) {
                column.stylingGroupCell(option);
                var cellColumn = column.renderStatistics(option);
                if (column.getType() == 'text') {
                    this._groupTotalStatistics[option.data.key] =cellColumn;
                }
            }
        },
        _cellPrepared: function (option) {
            var key = parseInt(option.key);
            if (!this._arrRowIndex.hasOwnProperty(key))
                this._arrRowIndex[key] = option.rowIndex;

            var column = this._dataSource.getColumn(option.column.dataField);
            if (column != null) {
                column.stylingDataCell(option);
                column.renderCell(option);
            }
        },
        _clickNextWeeks: function () {
            this._isClickArrow = true;
			
            var column = this._dataSource.getFirstWeekColumn();
			var date = PMPlanner.Calendar.WeekColumn.getDayInWeek(column.getDate(),0);
            date.nextWeek(PMPlanner.Calendar.View.COUNT_NEXT_WEEK);
			
            this._updateMatrix(date);
        },
        _clickPrevWeeks: function () {
            this._isClickArrow = true;
			
            var column = this._dataSource.getFirstWeekColumn();			
			var date = PMPlanner.Calendar.WeekColumn.getDayInWeek(column.getDate(),0);
            date.prevWeek(PMPlanner.Calendar.View.COUNT_NEXT_WEEK);
			
            this._updateMatrix(date);
        },
		
        _updateMatrix: function (date) {
            var linkedViewDate = new Date(date.getTime());
            var loadPanel = PMPlanner.Tools.createLoadPanel(this._view);
            loadPanel.show();
            var scope = this;
            var grid = this._view.getInstanceGrid();
            setTimeout(function() {
                grid.beginUpdate();
                scope._updateDataSource(date);
                scope._updateGridColumns(date);
                grid.endUpdate();
                scope._updateLinkedView(linkedViewDate);
                loadPanel.hide();
                PMPlanner.Tools.removeLoadPanel();
            }, 100);
        },
        
        _updateLinkedView: function (date) {
            var doAction = false;
            if(this._view.linkedView && PMPlanner.Calendar.WeekColumn.formatWeek(date) != this._view.linkedView._calendarView._dataSource.getFirstWeekColumn().getFormat()){
                this._view.linkedView.getCalendarView()._updateMatrix(date);
                doAction= true;
            }
            return doAction;
        },
        
        _updateDataSource: function (date) {
            this._viewDataSource.shouldQueryData();
            
            var start = PMPlanner.Calendar.WeekColumn.getDayInWeek(date,0);
            var end = new Date(date.getTime());
            end.nextWeek(this._displayCountWeek - 1);
            end = PMPlanner.Calendar.WeekColumn.getDayInWeek(end,6);
            
            this._viewDataSource.load(start, end);
        },

        _updateGridColumns: function (date) {
            var weekCount = 0;
            var weekIndex = 1;


            for (var monthIndex = 0; monthIndex < this._displayCountMonth; monthIndex++) {
                var month = date.getMonth(),
                    columnMonth = this._dataSource.getMonthColumn(monthIndex),
                    monthDate = new Date(date.getTime()),
                    lastMonth = false;

                this._dataSource.resetCountWeek(monthIndex);

                if (weekCount >= this._displayCountWeek) {
                    columnMonth.update(null);
                } else {
                    columnMonth.update(monthDate, monthIndex == 0, lastMonth);
                }


                for (var index = 0; index < PMPlanner.Calendar.View.COUNT_WEEK_IN_MONTH; index++) {
                    var columnWeek = this._dataSource.getWeekColumn(weekIndex);
                    weekIndex++;
                    if (date.getMonth() != month || weekCount >= this._displayCountWeek) {
                        columnWeek.update(null);
                    }
                    else {
                        this._dataSource.incrementWeekInMonthColumn(monthIndex);
                        columnWeek.update(date);
                        date.nextWeek();
                        weekCount++;
                    }
                }
                if (weekCount >= this._displayCountWeek) {
                    columnMonth.isLastMonth(true);
                }

            }
        },
        _configureMonthColumn: function (monthIndex, weekIndex) {
            var monthConfig = this._dataSource.getConfigMonthColumn(monthIndex);
            var weekConfig = this._dataSource.getConfigWeekColumn(weekIndex);
            jQuery.extend(true, weekConfig, monthConfig);
            return weekConfig;
        },
        _loadViewData: function (groupBy) {
            this._arrRowIndex = {};
            this._viewDataSource.setGroupingField(groupBy);
            var firstColumn = this._dataSource.getFirstWeekColumn();
            var lastColumn = this._dataSource.getLastWeekColumn();
            var end = new Date(lastColumn.getDate().getTime());
            end.setDate(end.getDate() + 6);
            this._viewDataSource.load(firstColumn.getDate(), end);			
        },
        _afterLoadView: function () {
            var selectDay = this._dataSource.getSelectBlock();
            if (selectDay != null) {
                selectDay.updateSelect();
            }
        },
        _moveWeekDay: function (dayObject, positionMoving) {
            var work = jQuery.extend(true, {}, dayObject._work);
            var grid = this._view.getInstanceGrid();
            var workDate = new Date(work.date.getTime());

            if (positionMoving == 'next') {
                workDate.nextWeek();
            } else {
                workDate.prevWeek();
            }

            work.date = workDate;
            work.week = PMPlanner.Calendar.WeekColumn.formatWeek(workDate);
            work.record.setValue('pmsd.date_todo', workDate);

            this.getDataSource().selectBlock(new PMPlanner.Calendar.WeekDay({
                id: null,
                work: work
            }));
            this._view.getDataSource().updateWork(dayObject._work, work);
            this._updateStatistics(dayObject, work);
            this._view.callEvent('onAfterPmScheduleDateChanged', [work.record]);
            grid.beginUpdate();
            grid.repaintRows([dayObject._rowIndex]);
            grid.endUpdate();
        },

        _findCellObjectInGrid: function (rowIndex, date) {
            var week = PMPlanner.Calendar.WeekColumn.formatWeek(date),
                dataField = null;

            var grid = this._view.getInstanceGrid();

            for (var i = 0; i < grid.columnCount(); i++) {
                var gridColumn = grid.columnOption(i);
                var column = this._dataSource.getColumn(gridColumn.dataField);
                if (column != null && column.getType() == 'week' && column.getFormat() == week) {
                    dataField = gridColumn.dataField;
                    if (!column.isFutureWeek())
                        dataField = null;
                    break;
                }
            }

            if (dataField == null)
                return null;

            return this._view.getInstanceGrid().getCellElement(rowIndex, dataField);
        },

        _updateStatistics: function (dayObject, work) {
            var weekColumns = this._dataSource.getWeekColumns();

            for (var week in weekColumns) {
                var column = weekColumns[week];
                var statistics = null;
                if (column.getFormat() == dayObject._work.week) {
                    statistics = column.getFutureStatistics(dayObject._work.groupName);
                    statistics.update();
                }
                if (column.getFormat() == work.week) {
                    statistics = column.getFutureStatistics(work.groupName);
                    statistics.update();
                }
            }
        },

    },{
        COUNT_MONTH: 7,
        COUNT_WEEK_IN_MONTH: 6,
        COUNT_WEEK_DISPLAY: 16,
        COUNT_NEXT_WEEK: 4
    }),

    ToolsMenu: Base.extend({

        _columnOption: null,
        _view: null,
        _buttonElement: null,

        _groupBy: null,

        constructor: function (view) {
            this._view = view;
            this._groupBy = PMPlanner.Constants.TEXT_MENU_GROUPBY_EquipmentStandard;
            this._dataSourceGroupBy = [{
                'id': 'eq_std',
                'name': PMPlanner.Constants.TEXT_MENU_GROUPBY_EquipmentStandard
            }, {
                'id': 'tr_id',
                'name': PMPlanner.Constants.TEXT_MENU_GROUPBY_Trade
            }, {
                'id': 'bl_id',
                'name': PMPlanner.Constants.TEXT_MENU_GROUPBY_Building
            }, {
                'id': 'site_id',
                'name': PMPlanner.Constants.TEXT_MENU_GROUPBY_Site
            }, {
                'id': 'sla_work_team',
                'name': PMPlanner.Constants.TEXT_MENU_GROUPBY_WorkTeam
            }, {
                'id': 'vn_id',
                'name': PMPlanner.Constants.TEXT_MENU_GROUPBY_ServiceContractProvider
            }, {
                'id': 'reg_program',
                'name': PMPlanner.Constants.TEXT_MENU_GROUPBY_ServiceContract
            }, {
                'id': 'pmp_id',
                'name': PMPlanner.Constants.TEXT_MENU_GROUPBY_PMProcedure
            }, {
                'id': 'none',
                'name': PMPlanner.Constants.TEXT_MENU_GROUPBY_None
            }];
            
            var hasComplianceLicense = PMPlanner.Tools.hasComplianceLicense();
            if(!hasComplianceLicense){
                this._dataSourceGroupBy = [{
                    'id': 'eq_std',
                    'name': PMPlanner.Constants.TEXT_MENU_GROUPBY_EquipmentStandard
                }, {
                    'id': 'tr_id',
                    'name': PMPlanner.Constants.TEXT_MENU_GROUPBY_Trade
                }, {
                    'id': 'bl_id',
                    'name': PMPlanner.Constants.TEXT_MENU_GROUPBY_Building
                }, {
                    'id': 'site_id',
                    'name': PMPlanner.Constants.TEXT_MENU_GROUPBY_Site
                }, {
                    'id': 'sla_work_team',
                    'name': PMPlanner.Constants.TEXT_MENU_GROUPBY_WorkTeam
                }, {
                    'id': 'pmp_id',
                    'name': PMPlanner.Constants.TEXT_MENU_GROUPBY_PMProcedure
                }, {
                    'id': 'none',
                    'name': PMPlanner.Constants.TEXT_MENU_GROUPBY_None
                }];
            }
        },

        renderView: function (columnOption) {
            var button = PMPlanner.Tools.Render.div({
                id: 'pmPlannedSelectColumnsButton',
                cssFields: {
                    'margin-top': '37px',
                    cursor: 'pointer'
                }
            });
            this._buttonElement = button;
            columnOption.cellElement.css('padding', '0');
            columnOption.cellElement.append(button);
        },

        renderMenu: function(){
            var scope = this;
            var countMonth = scope._view.getDataSource().getCountMonthForStatistics(),
                textMonth = countMonth + ' ' + PMPlanner.Constants.TEXT_MENU_PastPMScheduleDates_month;

            if (countMonth == null) {
                textMonth= 'Disable';
            }

            var countWeeks = scope._view.getCalendarView().getCountWeeksForDisplay();

            jQuery('#pmPlannedSelectColumnsButton').dxMenu({
                hideSubmenuOnMouseLeave: false,
                displayExpr: "name",
                dataSource: [
                    {
                        template: function (option) {
                            return PMPlanner.Tools.Render.img({
                                image: PMPlanner.Constants.IMG_SETTINGS
                            });
                        },
                        items: [
                            {
                                id: 'groupBy',
                                name: PMPlanner.Constants.TEXT_MENU_GroupBy + ' ' + scope._groupBy
                            },
                            {
                                id: 'selectColumns',
                                name: PMPlanner.Constants.TEXT_MENU_SelectColumns
                            }, {
                                id: 'selectWeek',
                                name: PMPlanner.Constants.TEXT_MENU_SelectWeek + ' ' + countWeeks + ' ' + PMPlanner.Constants.TEXT_MENU_SelectWeek_weeks
                            }, {
                                id: 'selectStatistics',
                                name: PMPlanner.Constants.TEXT_MENU_PastPMScheduleDates + ' - ' + textMonth
                            }, {
                                id: 'exportXLS',
                                name: 'XLS'
                            }
                        ]

                    }
                ],
                onItemClick: function (option) {
                    switch (option.itemData.id) {
                        case 'selectColumns':
                            var popup = jQuery('#pmPlannerSelectColumnsWindow').get(0);
                            if (!popup) {
                                scope._renderWindow({
                                    id: 'pmPlannerSelectColumnsWindow',
                                    title: PMPlanner.Constants.TEXT_MENU_SelectColumns,
                                    content: scope._renderViewForSelectColumns(),
                                    width: 250,
                                    height: 315
                                });
                            } else {
                                jQuery('#pmPlannerSelectColumnsWindow').dxPopup('instance').show();
                            }
                            break;
                        case 'selectWeek':
                            var popup = jQuery('#pmPlannerSelectWeekWindow').get(0);
                            if (!popup) {
                                scope._renderWindow({
                                    id: 'pmPlannerSelectWeekWindow',
                                    title: PMPlanner.Constants.TEXT_MENU_SelectWeek,
                                    content: scope._renderViewForSelectWeek(),
                                    width: 250,
                                    height: 90
                                });
                            } else {
                                jQuery('#pmPlannerSelectWeekWindow').dxPopup('instance').show();
                            }
                            break;
                        case 'groupBy':
                            var popup = jQuery('#pmPlannerGroupBy').get(0);
                            if (!popup) {
                                scope._renderWindow({
                                    id: 'pmPlannerGroupBy',
                                    title: PMPlanner.Constants.TEXT_MENU_GroupBy,
                                    content: scope._renderViewForGroupBy(),
                                    width: 250,
                                    height: 100
                                });
                            } else {
                                jQuery('#pmPlannerGroupBy').dxPopup('instance').show();
                            }
                            break;
						case 'selectStatistics':
                            var popup = jQuery('#pmPlannerSelectMonthWindow').get(0);
                            if (!popup) {
                                scope._renderWindow({
                                    id: 'pmPlannerSelectMonthWindow',
                                    title: PMPlanner.Constants.TEXT_MENU_PastPMScheduleDates,
                                    content: scope._renderViewForSelectMonth(),
                                    width: 250,
                                    height: 130
                                });
                            } else {
                                jQuery('#pmPlannerSelectMonthWindow').dxPopup('instance').show();
                            }
                            break;
                        case 'exportXLS':
                            scope._view.callEvent('onExportXLS');
                            break;
                    }

                }
            });
        },

        _renderWindow: function (option) {
            var window = PMPlanner.Tools.Render.div({
                id:  option.id
            });
            this._view.getDOMGrid().append(window);
            jQuery('#' + option.id).dxPopup({
                title: option.title,
                visible: true,
                width: option.width,
                height: option.height,
                position: {
                    my: "left top",
                    at: "bottom",
                    of: "#pmPlannedSelectColumnsButton"
                },
                contentTemplate: option.content
            });

        },

        _renderViewForSelectColumns: function () {
            var scope = this;
            return function (contentTemplate) {
                var scrollView = PMPlanner.Tools.Render.div({
                    id:  'pmPlannerScrollView'
                });
                var main = PMPlanner.Tools.Render.div({});
                var checkboxes = [];
                var columns = scope._view._configUser.columns;
                for (var i = 0; i < columns.length; i++) {
                    var column = columns[i];
                    if (column.fieldName) {
                        var checkbox = new scope.Checkbox(column, scope._view, i);
                        checkbox.render(main);
                        checkboxes.push(checkbox);
                    }
                }
                scrollView.html(main);
                contentTemplate.append(scrollView);

                for (var j = 0; j < checkboxes.length; j++) {
                    checkboxes[j].initialization();
                }

                jQuery("#pmPlannerScrollView").dxScrollView({
                    scrollByContent: true,
                    scrollByThumb: true,
                    showScrollbar: "onScroll",

                });
            };
        },

        _renderViewForSelectWeek: function () {
            var scope = this;
            return function (contentTemplate) {
                var main = PMPlanner.Tools.Render.div({});
                var radioGroup = PMPlanner.Tools.Render.div({
                    id: 'pmPlannerRadioGroup'
                });
                main.append(radioGroup);
                contentTemplate.append(main);

                jQuery("#pmPlannerRadioGroup").dxRadioGroup({
                    items: ['12 ' + PMPlanner.Constants.TEXT_WeekU, '16 ' + PMPlanner.Constants.TEXT_WeekU],
                    value: scope._view.getCalendarView()._displayCountWeek + ' ' + PMPlanner.Constants.TEXT_WeekU,
                    onValueChanged: function (option) {
                        var countWeeks = 8;
                        switch (option.value) {
                            case '12 ' + PMPlanner.Constants.TEXT_WeekU:
                                countWeeks = 12;
                                break;
                            case '16 ' + PMPlanner.Constants.TEXT_WeekU:
                                countWeeks = 16;
                                break;
                        }
                        scope._saveDisplayWeeks(countWeeks);
                        scope._view.getCalendarView().setCountWeekForDisplay(countWeeks);
                    }
                });

            }
        },

        _renderViewForGroupBy: function () {
            var scope = this;
            return function (contentTemplate) {
                var main = PMPlanner.Tools.Render.div({});
                var groupBy = PMPlanner.Tools.Render.div({
                    id: 'pmPlannerSelectBox'
                });
                main.append(groupBy);
                contentTemplate.append(main);

                jQuery("#pmPlannerSelectBox").dxSelectBox({
                    dataSource: new DevExpress.data.ArrayStore({
                        data: scope._dataSourceGroupBy,
                        key: "id"
                    }),
                    displayExpr: "name",
                    valueExpr: "id",
                    value: 'eq_std',
                    onValueChanged: function(data) {
                        scope._view.groupBy({
                            groupBy: data.value
                        });
                        for (var i = 0; i < scope._dataSourceGroupBy.length; i++ ) {
                            var group = scope._dataSourceGroupBy[i];
                            if (group.id == data.value) {
                                scope._groupBy = group.name;
                                break;
                            }
                        }
                    }
                });
            };
        },
		
		_renderViewForSelectMonth: function () {
			var scope = this;
			return function (contentTemplate) {
				var main = PMPlanner.Tools.Render.div({});
                var radioGroup = PMPlanner.Tools.Render.div({
                    id: 'pmPlannerRadioGroupMonth'
                });
                main.append(radioGroup);
                contentTemplate.append(main);

                jQuery("#pmPlannerRadioGroupMonth").dxRadioGroup({
                    items: ['3 ' + PMPlanner.Constants.TEXT_MonthU,
                        '6 ' + PMPlanner.Constants.TEXT_MonthU,
                        '12 ' + PMPlanner.Constants.TEXT_MonthU,
                        PMPlanner.Constants.TEXT_Disable
                    ],
                    value: scope._view.getDataSource().getCountMonthForStatistics() + ' ' + PMPlanner.Constants.TEXT_MonthU,
                    onValueChanged: function (option) {
                        var countMonth = 8;
                        switch (option.value) {
                            case '3 ' + PMPlanner.Constants.TEXT_MonthU:
                                countMonth = 3;
                                break;
                            case '6 ' + PMPlanner.Constants.TEXT_MonthU:
                                countMonth = 6;
                                break;
                            case '12 ' + PMPlanner.Constants.TEXT_MonthU:
                                countMonth = 12;
                                break;
                            case PMPlanner.Constants.TEXT_Disable:
                                countMonth = null;
                                break;
                        }
                        scope._savePastPMScheduleDates(countMonth);
                        scope._view.getCalendarView()._updateTotalStatistics(countMonth);
                    }
                });
			}
		},

        _savePastPMScheduleDates: function (countMonth) {
            var sidecar = View.panels.get('gridPanel').getSidecar();
            sidecar.set('pastPMScheduleDates', countMonth);
            sidecar.save();
        },

        _saveDisplayWeeks: function (countWeeks) {
            var sidecar = View.panels.get('gridPanel').getSidecar();
            sidecar.set('displayWeeks', countWeeks);
            sidecar.save();
        },

        Checkbox: Base.extend({

            constructor: function (userColumn, view, index) {
                this._userColumn = userColumn;
                this._view = view;
                this._index = index
            },

            render: function (main) {
                var field = PMPlanner.Tools.Render.div({
                    cssFields: {
                        padding: '5px'
                    }
                });
                field.append(PMPlanner.Tools.Render.div({
                    id: 'pmPlannerSelectColumnsWindowCheckbox' + this._index,
                    cssFields:{
                        float: 'left'
                    }
                }));

                field.append(PMPlanner.Tools.Render.text({
                    text: this._userColumn.title,
                    cssFields: {
                        'margin-left': '20px',
                    }
                }));
                main.append(field);
            },

            initialization: function () {
                var columnField = DataGridSource.configGridFieldName(this._view._configUser.dataSource, this._userColumn.fieldName);
                var columnOption = this._view.getInstanceGrid().columnOption(columnField);
                var scope = this;
                jQuery('#pmPlannerSelectColumnsWindowCheckbox' + this._index).dxCheckBox({
                    value: columnOption.visible,
                    onValueChanged: function(data) {

                        scope._view.getInstanceGrid().columnOption(columnField, 'visible', data.value);

                    }
                });
            }

        })
    }),

    DataSource: Base.extend({

        _columnsMonth: null,
        _columnsWeek: null,
        _columnsText: null,

        _selectBlock: null,
        _counterWorksID: null,


        constructor: function () {
            this._columnsMonth = {};
            this._columnsWeek = {};
            this._columnsText = {};
            this._selectBlock = null;
            this._counterWorksID = 0;
        },

        incCounter: function () {
            this._counterWorksID++;
        },

        getCounter: function () {
            return this._counterWorksID;
        },


        addTextColumn: function (option) {
            if (!option.fieldName) {
                return;
            }

            this._columnsText[option.fieldName] = new PMPlanner.Calendar.TextColumn(option);
        },

        removeTextColumn: function (fieldName) {
            delete this._columnsText[fieldName];
        },

        addMonthColumn: function (option) {
            this._columnsMonth['group_month_' + option.monthIndex] = new PMPlanner.Calendar.MonthColumn(option);
        },

        getMonthColumn: function (index) {
            return this._columnsMonth['group_month_' + index];
        },

        getConfigMonthColumn: function (index) {
            return this._columnsMonth['group_month_' + index].config();
        },

        incrementWeekInMonthColumn: function (index) {
            return this._columnsMonth['group_month_' + index].incrementWeekInMonthColumn();
        },

        resetCountWeek: function (index) {
            return this._columnsMonth['group_month_' + index].resetCountWeek();
        },

        addWeekColumn: function (option) {
            this._columnsWeek['week_' + option.weekIndex] = new PMPlanner.Calendar.WeekColumn(option);
        },

        getWeekColumn: function (index) {
            return this._columnsWeek['week_' + index];
        },

        getConfigWeekColumn: function (index) {
            return this._columnsWeek['week_' + index].config();
        },

        getWeekColumns: function () {
            return this._columnsWeek;
        },

        getColumn: function (fieldName) {
            if (this._columnsMonth.hasOwnProperty(fieldName)) {
                return this._columnsMonth[fieldName];
            }
            if (this._columnsWeek.hasOwnProperty(fieldName)) {
                return this._columnsWeek[fieldName];
            }
            if (this._columnsText.hasOwnProperty(fieldName)) {
                return this._columnsText[fieldName];
            }

            return null;
        },

        getTextColumnCount: function () {
            return this._getCountElementInObj(this._columnsText);
        },

        getFirstWeekColumn: function () {
            for (var key in this._columnsWeek) {
                var column = this._columnsWeek[key];
                if (column.getDate() != null) {
                    return column;
                    break;
                }
            }
            return null;
        },

        getLastWeekColumn: function () {
            var result = null;
            for (var key in this._columnsWeek) {
                var column = this._columnsWeek[key];
                if (column.getDate() != null) {
                    result = column;
                }
            }
            return result;
        },

        getCountPastWeek: function () {
            var result = 0;
            for (var key in this._columnsWeek) {
                var column = this._columnsWeek[key];
                if (column.getDate() == null) continue;
                if (column.isFutureWeek())
                    break;
                result++;
            }
            return result;
        },

        getCountFutureWeek: function () {
            var result = 0;
            for (var key in this._columnsWeek) {
                var column = this._columnsWeek[key];
                if (column.getDate() == null) continue;
                if (column.isFutureWeek())
                    result++;

            }
            return result;
        },

        eachWeekColumn: function (callback) {
            jQuery.each(this._columnsWeek, callback);
        },

        selectBlock: function (dayObject) {
            this._selectBlock = dayObject;
        },

        deselectBlock: function () {
            this._selectBlock = null;
        },

        getSelectBlock: function () {
            return this._selectBlock;
        }

    }),
 
    CELL_WIDTH: 70,
    CELL_HEIGHT: 25
   
};


