/**
 * Controller for Filter panel actions.
 */
pmPlannerFilterController = View.createController('pmPlannerFilterController', {

    /**
     * Filter panel fields.
     */
    filterFields: [ 'pmp.ac_id', 'eq.eq_sys_name', 'pms.site_id', 'pms.bl_id', 'eq.eq_std', 'pmp.tr_id', 'pms.pmp_id', 'pms.fl_id', 'pms.rm_id', 'pms.dv_id', 'pms.dp_id', 'pms.pms_id',
            'pms.pm_group', 'pms.eq_id', 'pms.sla_work_team_id', 'pmpspt.part_id', 'pms.criticality', 'regprogram.vn_id', 'pms.reg_program', 'inHouseOnly', 'outSourceOnly' ],

    /**
     * Used for export XLS for print restriction.
     */
    printableRestriction: [],

    /**
     * After this controller created
     */
    afterCreate: function() {
        jQuery('#analyseLaborShortages').hide();
        jQuery('#gearMenu').css({
            'position': 'absolute',
            'right': '20px'
        });

        this.on('app:bldgops:pmPlanner:setPrintRestriction', this.setPrintRestriction);
    },

    /**
     * After this view load
     */
    afterViewLoad: function() {
        var hasComplianceLicense = PMPlanner.Tools.hasComplianceLicense();
        if (!hasComplianceLicense) {
            jQuery('#optionalFilterPanel_regprogram\\.vn_id').hide();
            jQuery('#optionalFilterPanel_regprogram\\.vn_id_selectValue').remove();
            jQuery('#optionalFilterPanel_pms\\.reg_program').hide();
            jQuery('#optionalFilterPanel\\.pms\\.reg_program_selectValue').remove();
            jQuery("input[name='service_source']").hide();
            jQuery(".service_source_text").hide();
        }
    },

    /**
     * Filters the list of PM schedules.
     */
    pmSchedulesFilter_onFilter: function() {
        this.collapseOptionalFilterPanel();
        this.applyFilterRestrictions();
        this.trigger('app:bldgops:pmPlanner:filter');
        // add current filter to recent search
        pmPlannerRecentSearchController.addRecentSearch();
    },

    /**
     * Apply Filter Restriction.
     */
    applyFilterRestrictions: function() {
        var controller = this;
        controller.printableRestriction = [];
        _.each(controller.filterFields, function(fieldName) {
            var fieldValue = controller.pmSchedulesFilter.getFieldMultipleValues(fieldName).join("','");
            if (fieldValue == '') {
                fieldValue = controller.optionalFilterPanel.getFieldMultipleValues(fieldName).join("','");
            }

            if ('inHouseOnly' == fieldName) {
                var value = jQuery("input[name='service_source']:checked").val();
                fieldValue = value == 'inHouseOnly';
            } else if ('outSourceOnly' == fieldName) {
                var value = jQuery("input[name='service_source']:checked").val();
                fieldValue = value == 'outSourceOnly';
            } else {
                if (!fieldValue == '') {
                    fieldValue = "'" + fieldValue + "'";
                }
            }

            controller.setDataSourcesParameters(fieldName, fieldValue);
            controller.trigger('app:bldgops:pmPlanner:setFilterParameter', fieldName, fieldValue);
            controller.trigger('app:bldgops:pmPlanner:setPrintRestriction', fieldName, fieldValue);
        });
    },

    /**
     * Set print restriction.
     */
    setPrintRestriction: function(fieldName, fieldValue) {
        var controller = this;
        if ('inHouseOnly' == fieldName && fieldValue) {
            this.printableRestriction.push({
                'title': getMessage('inHouseOnly'),
                'value': getMessage('trueText')
            });
        } else if ('outSourceOnly' == fieldName && fieldValue) {
            this.printableRestriction.push({
                'title': getMessage('outSourceOnly'),
                'value': getMessage('trueText')
            });
        } else if ('eq.eq_sys_name' == fieldName && fieldValue) {
            this.printableRestriction.push({
                'title': getMessage('eqSystem'),
                'value': fieldValue
            });
        } else if ('pmpspt.part_id' == fieldName && fieldValue) {
            this.printableRestriction.push({
                'title': getMessage('partCode'),
                'value': fieldValue
            });
        } else if ('pmp.ac_id' == fieldName && fieldValue) {
            this.printableRestriction.push({
                'title': getMessage('acCode'),
                'value': fieldValue
            });
        } else if ('pms.criticality' == fieldName && fieldValue) {
            this.printableRestriction.push({
                'title': controller.dataDS.fieldDefs.get(fieldName).title,
                'value': controller.dataDS.fieldDefs.get(fieldName).enumValues[fieldValue.replace(/'/g, "")]
            });
        } else {
            if (fieldValue) {
                this.printableRestriction.push({
                    'title': controller.dataDS.fieldDefs.get(fieldName).title,
                    'value': fieldValue
                });
            }
        }
    },

    /**
     * Sets the dataSource parameters.
     */
    setDataSourcesParameters: function(fieldName, fieldValue) {
        var controller = this;
        controller.dataDS.addParameter(fieldName, fieldValue);
        controller.groupByTradeDataDS.addParameter(fieldName, fieldValue);
        controller.worksDS.addParameter(fieldName, fieldValue);
        controller.groupByTradesWorksDS.addParameter(fieldName, fieldValue);
        controller.pmsTradeDS.addParameter(fieldName, fieldValue);
        controller.costLaborDS.addParameter(fieldName, fieldValue);
        controller.craftspersonsDataSource.addParameter(fieldName, fieldValue);
        controller.averageOnDemandHoursDS.addParameter(fieldName, fieldValue);
        controller.tradeDS.addParameter(fieldName, fieldValue);
    },

    /**
     * Show or hide optional filter panel
     */
    pmSchedulesFilter_onMoreOptions: function(panel, action) {
        this.optionalFilterPanel.toggleCollapsed();
        action.setTitle(this.optionalFilterPanel.collapsed ? getMessage('filterMore') : getMessage('filterLess'));
        this.moreAction = action;
    },

    /**
     * Clear filter panel
     */
    pmSchedulesFilter_onClear: function(panel, action) {

        // clear input values in filter panels
        this.clearFilterValues();

        // refresh the PM schedule control
        this.pmSchedulesFilter_onFilter();

    },

    /**
     * Clear filter panel
     */
    pmSchedulesFilter_onAnalyseLaborShortages: function(panel, action) {
        View.openDialog('ab-pm-planner-labor-analysis-shortages.axvw');
    },

    /**
     * Show Cost.
     */
    showCosts: function(action) {
        this.trigger('app:bldgops:pmPlanner:showCosts', action.checked);
    },

    /**
     * Show Labor.
     */
    showLabor: function(action) {
        this.trigger('app:bldgops:pmPlanner:showLabor', action.checked);
    },

    /**
     * collapse the optional filter panel
     */
    collapseOptionalFilterPanel: function() {
        if (!this.optionalFilterPanel.collapsed) {
            this.optionalFilterPanel.toggleCollapsed();
            this.moreAction.setTitle(getMessage('filterMore'));
        }
    },

    /**
     * Clear input values in filter panels.
     */
    clearFilterValues: function() {
        this.pmSchedulesFilter.clear();
        this.optionalFilterPanel.clear();
    }

});

/**
 * Event handler to open equipment system tree
 */
function openEqSystemTree(context) {
    var dialogConfig = {
        width: 800,
        height: 600
    };
    View.openDialog('ab-pm-planner-filter-eq-sys.axvw', null, false, dialogConfig);
}