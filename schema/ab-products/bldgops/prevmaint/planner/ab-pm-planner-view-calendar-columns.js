/**
 * The text column of the grid performs the following functions: styling of the cells, of the groups, header and information cells
 */
PMPlanner.Calendar.TextColumn = Base.extend({

    /**
     * Column option
     */
    _option: null,
    /**
     * Column type
     */
    _type: 'text',
    /**
     * Constructor
     * @param option - column option
     */
    constructor: function (option) {
        this._option = option;
        this._type = 'text';
    },
    /**
     * Adjust css classes for header
     * @param devExpressOption - dev Express option for cell grid
     */
    stylingHeader: function (devExpressOption) {
        var cell = devExpressOption.cellElement;
        this._devExpressCell = cell;
        cell.css('cursor', 'default');
        if (devExpressOption.column.cssClass == "dx-command-expand") {
            cell.addClass(PMPlanner.Calendar.TextColumn.CSS_HEADER_NONE);
            return;
        }
        cell.addClass(PMPlanner.Calendar.TextColumn.CSS_HEADER);
    },
    /**
     * Adjust css classes for group cell
     * @param devExpressOption - dev Express option for cell grid
     */
    stylingGroupCell: function (devExpressOption) {
        var cell = devExpressOption.cellElement;
        cell.css('vertical-align', 'middle');
    },
    /**
     * Adjust css classes for data cell
     * @param devExpressOption - dev Express option for cell grid
     */
    stylingDataCell: function (devExpressOption) {
        var cell = devExpressOption.cellElement;
        cell.css('border-right', '1px solid #ddd');
    },
    /**
     * The statistics display with the cell
     * @param devExpressOption - dev Express option for cell grid
     */
    renderStatistics: function (devExpressOption) {
       return new PMPlanner.GroupTotalStatistics(devExpressOption, this._option.view);
    },
    /**
     * Displaying text information and adding new items
     * @param devExpressOption - dev Express option for cell grid
     */
    renderCell: function (devExpressOption) {
        var scope = this;
        var cell = devExpressOption.cellElement;

        cell.on('click', function () {
            var selectBlock = scope._option.view.getCalendarView().getDataSource().getSelectBlock();
            if (selectBlock != null) {
                selectBlock.deselect();
            }
        });
    },
    /**
     * Get the type of the column
     */
    getType: function () {
        return this._type;
    }

},{
    CSS_HEADER: 'header_column_data',
    CSS_HEADER_NONE: 'header_column_data_none'
});
/**
 * The month column of the grid performs the following functions: styling of the cells, of the groups, header and information cells
 */
PMPlanner.Calendar.MonthColumn = Base.extend({

    _date: null,
    _monthIndex: null,
    _columnIndex: null,
    _view: null,
    _weekCount: null,
    _firstMonth: null,
    _lastMonth: null,
    /**
     * Column type
     */
    _type: 'month',

    /** Constructor
     * @param option - column option
     */
    constructor: function (option) {
        if (option.date == null) {
            this._date = null;
        } else {
            this._date = new Date(option.date.getFullYear(), option.date.getMonth());
        }
        this._monthIndex = option.monthIndex;
        this._columnIndex = option.columnIndex;
        this._view = option.view;

        this._firstMonth = option.firstMonth;
        this._lastMonth = option.lastMonth;

        this._type = 'month';
    },
    /**
     * Get config for create new column based on this
     * @returns {{groupColumnID: string, groupColumnTitle: string}}
     */
    config: function () {
        var config = {
            groupColumnID: PMPlanner.Calendar.MonthColumn.ID_MONTH + this._monthIndex,
            groupColumnTitle: 'false'
        };
        if (this._date != null) {
            config['groupColumnTitle'] = arrMonthNames[this._date.getMonth()] + ' ' + this._date.getFullYear();
        }
        return config;
    },
    /**
     * Column shows the last month?
     * @param flag - yes/no
     */
    isLastMonth: function (flag) {
        this._lastMonth = flag;
    },
    /**
     * To increase the count of weeks in month
     */
    incrementWeekInMonthColumn: function () {
        this._weekCount++;
    },
    /**
     * To reset the count    of weeks in a month
     */
    resetCountWeek: function () {
        this._weekCount = 0;
    },
    /**
     * To update data on the month
     * @param date - start date of the month
     * @param firstMonth - is first month in calendar
     * @param lastMonth - is last month in calendar
     */
    update: function (date, firstMonth, lastMonth) {
        if (date == null) {
            this._date = null;
            this._view.getInstanceGrid().columnOption(this._columnIndex, 'visible', false);
            this._firstMonth = false;
            this._lastMonth = false;
        } else {
            this._date = new Date(date.getFullYear(), date.getMonth());
            this._view.getInstanceGrid().columnOption(this._columnIndex, 'caption', arrMonthNames[date.getMonth()] + ' ' + date.getFullYear());
            this._view.getInstanceGrid().columnOption(this._columnIndex, 'visible', true);
            this._firstMonth = firstMonth;
            this._lastMonth = lastMonth;
        }
    },
    /**
     * Adjust css classes for header
     * @param devExpressOption - dev Express option for cell grid
     */
    stylingHeader: function (devExpressOption) {
        var scope = this;
        var cell = devExpressOption.cellElement;
        cell.html('');
        cell.css('cursor', 'pointer');
        cell.addClass(PMPlanner.Calendar.MonthColumn.CSS_HEADER);
        cell.on('click', function () {
            if (scope._view.getCalendarView()._isClickArrow) {
                scope._view.getCalendarView()._isClickArrow = false;
                return;
            }

            var popup = jQuery('#pmPlannerCalendarSelectDate').get(0);
            if (!popup) {
                var window = PMPlanner.Tools.Render.div({
                    id:  'pmPlannerCalendarSelectDateView'
                });

                scope._view.getDOMGrid().append(window);

                jQuery('#pmPlannerCalendarSelectDateView').dxPopup({
                    title: PMPlanner.Constants.Calendar,
                    visible: true,
                    width: 400,
                    height: 400,
                    position: {
                        my: "left top",
                        at: "top",
                        of: "#" + scope._view._configUser.container +'Grid'
                    },
                    contentTemplate: function (contentTemplate) {
                        var main = PMPlanner.Tools.Render.div({});
                        var groupBy = PMPlanner.Tools.Render.div({
                            id: 'pmPlannerCalendarSelectDate'
                        });
                        main.append(groupBy);
                        contentTemplate.append(main);
                        var first = true;
                        jQuery("#pmPlannerCalendarSelectDate").dxCalendar({
                            value: new Date(),
                            disabled: false,
                            width: 375,
                            height: 350,
                            minZoomLevel: 'month',
                            maxZoomLevel: 'month',
                            firstDayOfWeek: 0,
                            cellTemplate: function (itemData, itemIndex, itemElement) {
                                if (!first && itemIndex == 0)
                                    scope._changeMonthForCalendar();
                                return itemData.text;
                            },
                            onValueChanged: function(data) {
                                scope._view.getCalendarView()._updateMatrix(data.value);
                                jQuery('#pmPlannerCalendarSelectDateView').dxPopup('instance').hide();
                            },
                        });
                        scope._changeMonthForCalendar();
                        first = false;
                    }
                });
                jQuery('#pmPlannerCalendarSelectDateView').dxPopup('instance').show();


            } else {
                jQuery('#pmPlannerCalendarSelectDateView').dxPopup('instance').show();
            }
        });

        var isAddArrow = this._renderArrow(cell, devExpressOption.column.caption);
        if (!isAddArrow)
            this._renderHeaderText(cell, devExpressOption.column.caption);
    },
    /**
     * Adjust css classes for group cell
     * @param devExpressOption - dev Express option for cell grid
     */
    stylingGroupCell: function (devExpressOption) {
        var cell = devExpressOption.cellElement;
        cell.css('vertical-align', 'middle');
    },
    /**
     * Adjust css classes for data cell
     * @param devExpressOption - dev Express option for cell grid
     */
    stylingDataCell: function (devExpressOption) {
        var cell = devExpressOption.cellElement;
        cell.css('border-right', '1px solid #ddd');
    },
    /**
     * The statistics display with the cell
     * @param devExpressOption - dev Express option for cell grid
     */
    renderStatistics: function (devExpressOption) { },
    /**
     * Displaying text information and adding new items
     * @param devExpressOption - dev Express option for cell grid
     */
    renderCell: function (devExpressOption) { },
    /**
     * Get the type of the column
     */
    getType: function () {
        return this._type;
    },
    /**
     * Render arrow in column In first month render arrow for click prev weeks. In lasr month render arrow for click next weeks.
     * @param cell -  header cell
     * @param title - month name
     * @returns {boolean}
     * @private
     */
    _renderArrow: function (cell, title) {
        var result = false;
        var scope = this;
        if (this._lastMonth) {
            if (this._weekCount > 1)
                this._renderText(cell, title, 0);
            cell.append(PMPlanner.Tools.Render.img({
                image: PMPlanner.Constants.IMG_SELECT_NEXT_WEEK,
                id: PMPlanner.Calendar.MonthColumn.ID_RIGHT_ARROW,
                cssClass: PMPlanner.Calendar.MonthColumn.CSS_RIGHT_ARROW,
                onClick: function () {
                    scope._clickRightArrow();
                }
            }));
            result = true;
        }
        if (this._firstMonth) {
            cell.append(PMPlanner.Tools.Render.img({
                image: PMPlanner.Constants.IMG_SELECT_PREV_WEEK,
                id: PMPlanner.Calendar.MonthColumn.ID_LEFT_ARROW,
                cssClass: PMPlanner.Calendar.MonthColumn.CSS_LEFT_ARROW,
                onClick: function () {
                    scope._clickLeftArrow();
                }
            }));
            if (this._weekCount > 1)
                this._renderText(cell, title, 0);
            result = true;
        }
        return result;
    },
    /**
     * Render column header
     * @param cell -  header cell
     * @param title - month name
     * @private
     */
    _renderHeaderText: function (cell, title) {
        var weekPadding = PMPlanner.Calendar.CELL_WIDTH / 8;
        if (this._weekCount > 1) {

            var day = this._date.getDay();
            if (day == 0) {
                day = 6;
            } else {
                day -= 1;
            }
            this._renderText(cell, title, weekPadding * day);

        } else {
            this._renderText(cell, title, 0);
        }
    },
    /**
     * Render text for header column
     * @param cell -  header cell
     * @param title - month name
     * @param padding - the indentation depending on the number of days
     * @private
     */
    _renderText: function (cell, title, padding) {
        cell.append(PMPlanner.Tools.Render.text({
            text: title,
            cssFields: {
                float: 'left',
                'padding-top': '4px',
                'padding-left': padding
            }
        }));
    },
    /**
     * Click on image for switch prev weeks
     * @private
     */
    _clickLeftArrow: function () {
        this._view.callEvent('onClickPrevWeeks', []);
    },
    /**
     * Click on image for switch next weeks
     * @private
     */
    _clickRightArrow: function () {
        this._view.callEvent('onClickNextWeeks', []);
    },
    /**
     * Change localization months in calendar toggle the display of dates
     * @private
     */
    _changeMonthForCalendar: function () {
        var element = jQuery('#pmPlannerCalendarSelectDate').find('span').get(0);
        if (element) {
            var month = element.textContent.split(' ');
            var newText = '';
            switch (month[0].toLowerCase()) {
                case 'january': newText = arrMonthNames[0]; break;
                case 'february': newText = arrMonthNames[1]; break;
                case 'march': newText = arrMonthNames[2]; break;
                case 'april': newText = arrMonthNames[3]; break;
                case 'may': newText = arrMonthNames[4]; break;
                case 'june': newText = arrMonthNames[5]; break;
                case 'july': newText = arrMonthNames[6]; break;
                case 'august':newText = arrMonthNames[7]; break;
                case 'september': newText = arrMonthNames[8]; break;
                case 'october': newText = arrMonthNames[9]; break;
                case 'november': newText = arrMonthNames[10]; break;
                case 'december': newText = arrMonthNames[11]; break;
            }

            newText += ' ' + month[1];
            element.outerText = newText;
        }
    }
}, {
    ID_MONTH: 'group_month_',
    CSS_HEADER: 'header_month',
    ID_LEFT_ARROW: 'leftArrow',
    ID_RIGHT_ARROW: 'rightArrow',
    CSS_LEFT_ARROW: 'toolbar_leftArrow',
    CSS_RIGHT_ARROW: 'toolbar_rightArrow',
});

PMPlanner.Calendar.WeekColumn = Base.extend({

    _date: null,
    _weekIndex: null,
    _monthIndex: null,
    _columnIndex: null,
    _viewManager: null,
    _futureStatistics: null,
    _type: 'week',

    constructor: function (option) {
        if (option.date == null) {
            this._date = null;
        } else {
            this._date = new Date(option.date.getTime());
        }
        this._weekIndex = option.weekIndex;
        this._monthIndex = option.monthIndex;
        this._columnIndex = option.columnIndex;
        this._viewManager = option.view;

        this._futureStatistics = {};

        this._type = 'week';
    },

    getDate: function () {
        if (this._date == null) {
            return null;
        }
        return new Date(this._date.getTime());
    },

    getFormat: function () {
        return this._formatWeek();
    },

    getFutureStatistics: function (id) {
        return this._futureStatistics[id];
    },

    getType: function () {
        return this._type;
    },

    config: function () {
        var config = {
            title: '',
            type: 'scheduler',
            width: PMPlanner.Calendar.CELL_WIDTH,
            id: PMPlanner.Calendar.WeekColumn.ID_WEEK + this._weekIndex,
            fieldName: PMPlanner.Calendar.WeekColumn.ID_WEEK + this._weekIndex,
            groupColumnID: PMPlanner.Calendar.MonthColumn.ID_MONTH + this._monthIndex,
            visible: true
        };
        if (this._date != null) {
            config['title'] = 'W' + this._date.getWeek();
        }

        return config;
    },

    update: function (date) {
        if (date == null) {
            this._date = null;
            this._viewManager.getInstanceGrid().columnOption(this._columnIndex, 'visible', false);
        } else {
            this._date = new Date(date.getTime());
            this._viewManager.getInstanceGrid().columnOption(this._columnIndex, 'caption', 'W' + date.getWeek());
            this._viewManager.getInstanceGrid().columnOption(this._columnIndex, 'visible', true);
        }
    },

    stylingHeader: function (devExpressOption) {
        var cell = devExpressOption.cellElement;
        cell.css({
            'padding': '0px',
            'cursor': 'pointer'
        });
        cell.addClass(PMPlanner.Calendar.WeekColumn.CSS_HEADER);
        cell.attr('id', PMPlanner.Calendar.WeekColumn.ID_WEEK + this._weekIndex);
        cell.html('');
        this._highlightMissingWeek(cell);
        this._renderTitle(cell, devExpressOption.column.caption);
        PMPlanner.Tools.highlightCurrentWeek(devExpressOption, this._date);
        this._renderTooltip(devExpressOption);
    },

    stylingGroupCell: function (devExpressOption) {
        var cell = devExpressOption.cellElement;
        cell.css({
            'border-bottom': '1px solid #ddd',
            'border-left': '1px solid #ddd',
            height: PMPlanner.Calendar.CELL_HEIGHT + 'px'
        });
        PMPlanner.Tools.highlightCurrentWeek(devExpressOption, this._date);
    },

    stylingDataCell: function (devExpressOption) {
        var cell = devExpressOption.cellElement;
        cell.css({
            'border-bottom': '1px solid #ddd',
            'border-left': '1px solid #ddd',
            height: PMPlanner.Calendar.CELL_HEIGHT + 'px'
        });
        PMPlanner.Tools.highlightCurrentWeek(devExpressOption, this._date);
    },

    renderStatistics: function (devExpressOption) {
        var option = {
            view: this._viewManager,
            week: this._formatWeek()
        };

        var key = devExpressOption.data.key;
        if (!this._futureStatistics.hasOwnProperty(key)) {
            this._futureStatistics[key] = {};
        }
        var block = new PMPlanner.GroupStatistics(devExpressOption, option);
        this._futureStatistics[key]= block;
    },

    renderCell: function (devExpressOption) {
        var week = this._formatWeek();
        var id = parseInt(devExpressOption.data['pms(pms)*pms_id']);
        var worksData = this._viewManager.getDataSource().getWork(id ,week);
        if (worksData != null) {
            var cellOption = {
                id: 'w' + this._weekIndex + '_' + id,
                view: this._viewManager,
                data: worksData,
                cellData: {
                    id: id,
                    week: week
                }
            };
            new PMPlanner.Calendar.WeekCellNew(devExpressOption, {
                view: this._viewManager,
                works: worksData
            });

        } else {
            var scope = this;
            var cell = devExpressOption.cellElement;

            cell.on('click', function () {
                var selectBlock = scope._viewManager.getCalendarView().getDataSource().getSelectBlock();
                if (selectBlock != null) {
                    selectBlock.deselect();
                }
            });
        }
    },

    isFutureWeek: function () {
        return PMPlanner.Calendar.WeekColumn.isFutureWeek(this._date);
    },

    isCurrentWeek: function () {
        var current = PMPlanner.Calendar.WeekColumn.formatWeek(new Date());
        var format = this.getFormat();
        if (current ==  format) {
            return true;
        }
        return false;

    },

    _highlightMissingWeek: function (cell) {
        if (this._viewManager.getDataSource() && this._viewManager.getDataSource().hasMissingWorks(this.getFormat())) {
            cell.append(PMPlanner.Tools.Render.div({
                cssFields:{
                    'border-top': '4px solid #AD0000',
                    'padding-top': '5px'
                }
            }));
        }
    },

    _renderTitle: function (cell, title) {
        cell.append(PMPlanner.Tools.Render.text({
            text: title,
            cssFields: {
                'padding-bottom': '10px'
            }
        }));
    },

    _renderTooltip: function (devExpressOption) {
        if (this._date == null)
            return;

        var text = this._getDataRange();
        new PMPlanner.Tooltip({
            elem: devExpressOption.cellElement,
            cellID: PMPlanner.Calendar.WeekColumn.ID_WEEK + this._weekIndex,
            tooltipID: PMPlanner.Calendar.WeekColumn.ID_HEADER_TOOLTIP + this._weekIndex,
            onRenderText: function (contentElement) {
                contentElement.append(PMPlanner.Tools.Render.text({
                    text: text
                }));
            }
        });
    },

    _getDataRange: function () {
        var firstDay = this.getDateStart();
        var lastDay = this.getDateEnd();
        return PMPlanner.Tooltip.formatDate(firstDay) + ' - ' + PMPlanner.Tooltip.formatDate(lastDay);
    },

    getDateStart: function () {
        return PMPlanner.Calendar.WeekColumn.getDayInWeek(this._date, 0);
    },

    getDateEnd: function () {
        return PMPlanner.Calendar.WeekColumn.getDayInWeek(this._date, 6);
    },

    _formatWeek: function () {
        if (this._date == null) {
            return null;
        }
        return PMPlanner.Calendar.WeekColumn.formatWeek(this._date);
    }

}, {
    ID_WEEK: 'week_',
    ID_HEADER_TOOLTIP: 'week_tooltip_',
    CSS_HEADER: 'header_week',
    CSS_HEADER_MISSED: 'header_week_missed',

    formatWeek: function (date) {
        return date.format('Y-W');
    },

    getDayInWeek: function  (date,day) {
        var firstDay = new Date(date);
        firstDay.setDate(firstDay.getDate() - (date.getDay() -1));
        var result = new Date(firstDay);
        result.setDate(result.getDate()  + day);
        return result;
    },

    isFutureWeek: function (date) {
        var currentDate =  new Date(),
            currentWeek = currentDate.getWeek(),
            currentYear = currentDate.getFullYear();

        if (currentDate.getMonth() == 0 && currentDate.getWeek() > 6) {
            currentYear -= 1;
        }

        var yearDate = date.getFullYear();
        if (date.getMonth() == 0 && date.getWeek() > 6) {
            yearDate -= 1;
        }

        if (yearDate > currentYear) {
            return true;
        } else {
            if (yearDate == currentYear) {
                if (date.getWeek() >= currentWeek) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }
});