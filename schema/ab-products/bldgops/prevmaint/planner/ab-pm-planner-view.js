/**
 * Contain the basic data structures to work with PM Planned: data source, tooltip, calendar data, event manager.
 * @type {{View: void, EventManager: void, DataSource: void, Date: void, Tooltip: void}}
 */
PMPlanner = {

    /**
     * Base class for work with grid and calendar.
     */
    View: Base.extend({

        /**
         * User configuration to work with the control.
         */
        _configUser: null,
        /**
         * The grid object, which is generated from the control "Grid".
         */
        _ARCHIBUSGrid: null,
        /**
         * The grid object, which is the original object DevExtreme.
         */
        _instanceGrid: null,
        /**
         * Data source the data from the database.
         */
        _dataSource: null,
        /**
         * Contains basic information about the grouping fields.
         */
        _groupPanel: null,

        /**
         * Constructor.
         * @param configUser - User configuration to work with the control.
         */
        constructor: function (configUser) {
            this._configUser = configUser;
            this._groupPanel = {
                'eq_std': {
                    field: 'pms(eq)*eq_std',
                    title: 'EQ Standard'
                },
                'tr_id': {
                    field: 'pms(pms)*tr_id',
                    title: 'Trade'
                },
                'bl_id': {
                    field: 'pms(pms)*bl_id',
                    title: 'Building'
                },
                'site_id': {
                    field: 'pms(pms)*site_id',
                    title: 'Site'
                },
                'sla_work_team': {
                    field: 'pms(pms)*sla_work_team_id',
                    title: 'Work Team'
                },
                'reg_program': {
                    field: 'pms(pms)*reg_program',
                    title: 'Compliance Program Code'
                },
                'vn_id': {
                    field: 'pms(regprogram)*vn_id',
                    title: 'Compliance Program Code'
                },
                'pmp_id': {
                    field: 'pms(pms)*pmp_id',
                    title: 'PM Procedure'
                },'none': {
                    field: 'pms(pms)*none',
                    title: 'N/A'
                }
            };

            if (!String.prototype.includes) {
                String.prototype.includes = function(search, start) {
                    if (typeof start !== 'number') {
                        start = 0;
                    }

                    if (start + search.length > this.length) {
                        return false;
                    } else {
                        return this.indexOf(search, start) !== -1;
                    }
                };
            }
            if (!('remove' in Element.prototype)) {
                Element.prototype.remove = function() {
                    if (this.parentNode) {
                        this.parentNode.removeChild(this);
                    }
                };
            }

            if (!Array.prototype.find) {
                Object.defineProperty(Array.prototype, 'find', {
                    value: function(predicate) {
                        // 1. Let O be ? ToObject(this value).
                        if (this == null) {
                            throw new TypeError('"this" is null or not defined');
                        }

                        var o = Object(this);

                        // 2. Let len be ? ToLength(? Get(O, "length")).
                        var len = o.length >>> 0;

                        // 3. If IsCallable(predicate) is false, throw a TypeError exception.
                        if (typeof predicate !== 'function') {
                            throw new TypeError('predicate must be a function');
                        }

                        // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
                        var thisArg = arguments[1];

                        // 5. Let k be 0.
                        var k = 0;

                        // 6. Repeat, while k < len
                        while (k < len) {
                            // a. Let Pk be ! ToString(k).
                            // b. Let kValue be ? Get(O, Pk).
                            // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
                            // d. If testResult is true, return kValue.
                            var kValue = o[k];
                            if (predicate.call(thisArg, kValue, k, o)) {
                                return kValue;
                            }
                            // e. Increase k by 1.
                            k++;
                        }

                        // 7. Return undefined.
                        return undefined;
                    }
                });
            }

            this._ARCHIBUSGrid = null;
            this._instanceGrid = null;

            this._dataSource = new PMPlanner.DataSource();
            this._toolsMenu = new PMPlanner.Calendar.ToolsMenu(this);

            var eventManager = new PMPlanner.EventManager();
            this.addEvent = function (eventName, callback, scope) {
                eventManager.addEventListener(eventName, callback, scope);
            };
            this.callEvent = function (eventName, fields) {
                return eventManager.callEvent(eventName, fields);
            };
            this._addEvents();
            this._calendarView = new PMPlanner.Calendar.View(configUser.columns, this);
            this._costLaborDataSource = new PMPlanner.CostLaborDataSource(this);
            this._dataSource._costLaborDataSource = this._costLaborDataSource;
            this._costInformation = new PMPlanner.FinanceInformation({
                view: this                
            });
            this._createViewGrid();
            
            View.PMPlannerView = this;
        },
        /**
         * Sets the control height to specified value.
         * @param height - The height in pixels.
         */
        setHeight: function (height) {
            this._ARCHIBUSGrid.setHeight(height);
        },        
        /**
         * Get current calendar from date.
         */
        getCalendarFromDate: function () {
            return this.getCalendarView().getDataSource().getFirstWeekColumn().getDateStart();
        },
        /**
         * Get visible columns .
         */
        getVisibleColumns: function () {
            var visibleColumns = [];
            var columns = this._configUser.columns;
            for (var i = 0; i < columns.length; i++) {
                var column = columns[i];
                if (column.fieldName) {
                    var columnField = DataGridSource.configGridFieldName(this._configUser.dataSource, column.fieldName);
                    var columnOption = this.getInstanceGrid().columnOption(columnField);
                    if(columnOption.visible){
                        visibleColumns.push(column);
                    }
                }
            }
            return visibleColumns;
        },
		/**
		 * Update the calendar and display the work from the specified date
		 * @param startDate - date to start displaying data.
		 */
        updateCalendar: function (startDate) {
            this._calendarView._updateMatrix(startDate);
        },
        /**
         * Group columns in the grid with the specified name
         * @param option - {groupBy: the name of the grouping field; restriction: data for working with the data source;}
         */
        groupBy: function (option) {
            var loadPanel = PMPlanner.Tools.createLoadPanel(this);
            loadPanel.show();
            this._dataSource.shouldQueryData();
            this._dataSource.shouldQueryStatisticsData();
            var userColumn = this._isGroupingUserColumn(option.groupBy);
            var groupColumn = this._getGroupColumn(option.groupBy);

            this._ARCHIBUSGrid.beginUpdate();

            if (userColumn == null && groupColumn != null) {
                this._ARCHIBUSGrid.addColumn({
                    caption: this._groupPanel[option.groupBy].title,
                    dataField:  this._groupPanel[option.groupBy].field
                });
                this._calendarView.getDataSource().addTextColumn({
                    fieldName: this._groupPanel[option.groupBy].field,
                    view: this
                });
            }
            if (this._isRemoveGroupColumn(groupColumn)) {
                this._instanceGrid.deleteColumn(groupColumn.dataField);
                this._calendarView.getDataSource().removeTextColumn(groupColumn.dataField);
            }
            
            
            var oldDataSource = this._ARCHIBUSGrid._config.dataSource;

            switch (option.groupBy) {
                case 'tr_id':
                    this._ARCHIBUSGrid._config.dataSource = 'groupByTradeDataDS';
                    break;
                default:
                    this._ARCHIBUSGrid._config.dataSource = 'dataDS';
                    break;
            }
            
            if(oldDataSource != this._ARCHIBUSGrid._config.dataSource){
                this._ARCHIBUSGrid._dataSource._viewDataSource = View.dataSources.get(this._ARCHIBUSGrid._config.dataSource);
                this._ARCHIBUSGrid.refresh();
            }

            this._ARCHIBUSGrid.groupBy(this._groupPanel[option.groupBy].field);
            this._ARCHIBUSGrid.endUpdate();
            loadPanel.hide();
            PMPlanner.Tools.removeLoadPanel();
        },
        /**
         * To perform the show Costs
         * @param show - TRUE if show and FALSE if not show Costs
         */
        showCosts: function (show) {
            this._costInformation.showCosts(show);
        },
        /**
         * To perform the show Labor
         * @param show - TRUE if show and FALSE if not show Labor
         */
        showLabor: function (show) {
            this._costInformation.showLabor(show);
        },
        /**
         * Updating the data in the lines financial statistics. Data is retrieved from the database of financial statistics
         */
        updateFinanceStatistics: function () {
            this._costInformation.update();
        },
        /**
         * Update selected pm scheduled date in the calendar.
         * Called when you want to update the selected unit without restarting view.
         * @param key - the key data in the data source
         * @param date - date of work
         */
        updateSelectedPmScheduledDate: function (key, date) {
            var record = this._dataSource.loadRecord(key, date),
                selectBlock = this.getCalendarView().getDataSource().getSelectBlock(),
                workDate = record.getValue(PMPlanner.DataSource.DataField_DateTodo),
                ollWorkData = selectBlock._work,
                rowIndex = selectBlock._rowIndex,
                newWorkData = this._dataSource.createWork(record);

            this._dataSource.updateWork(ollWorkData, newWorkData);
            this.getCalendarView()._updateStatistics(selectBlock, newWorkData);
            this.getCalendarView().getDataSource().selectBlock(new PMPlanner.Calendar.WeekDay({
                id: null,
                work: newWorkData
            }));

            var grid = this._instanceGrid;
            grid.beginUpdate();
            grid.repaintRows([rowIndex]);
            this._dataSource.shouldQueryStatisticsData();
            this.getCalendarView()._updateTotalStatistics(this._dataSource.getCountMonthForStatistics());
            grid.endUpdate();
        },

        updatePmScheduledDates: function (key) {
            var records = this._dataSource.loadRecords(key);
            this._dataSource.cleanKeyAndAddWorks(key, records);
            
            var rowIndex = this.getCalendarView().getRowIndex(key);
            var grid = this._instanceGrid;
            grid.beginUpdate();
            grid.repaintRows([rowIndex]);
            this.getCalendarView()._updateTotalStatistics(this._dataSource.getCountMonthForStatistics());
            grid.endUpdate();
        },
		/**
         * To upgrade the data in the grid according to specified options
         * @param option - option to update
         */        
        refresh: function (option) {
            this._dataSource.shouldQueryData();
            this._dataSource.shouldQueryStatisticsData();
            this._ARCHIBUSGrid.refresh(option.restriction);
            if (option.groupBy != '')
                this.groupBy(option);
        },
		/**
         * Is it necessary to show Costs
         * @returns {*}
         */
        isShowCosts: function () {
            return this._costInformation.isShowCosts();
        },
        /**
         * Is it necessary to show Labor
         * @returns {*}
         */
        isShowLabor: function () {
            return this._costInformation.isShowLabor();
        },
        /**
         * Get a calendar object to work with columns and data
         * @returns {PMPlanner.Calendar.View|*|PMPlannerLaborAnalysis.CalendarView}
         */
        getCalendarView: function () {
            return this._calendarView;
        },
        /**
         * Get source data to work with data from the database
         * @returns {null}
         */
        getDataSource: function () {
            return this._dataSource;
        },
        /**
         * Get grid object, which is the original object DevExtreme
         * @returns {null}
         */
        getInstanceGrid: function () {
            return  this._instanceGrid;
        },
        /**
         * Get DOM for grid
         * @returns {*}
         */
        getDOMGrid: function () {
            return jQuery('#' + this._configUser.container +'Grid');
        },
        /**
         * Get object contains fields and methods for tools menu
         * @returns {*}
         */
        getObjectToolsMenu: function () {
            return this._toolsMenu;
        },
        /**
         * To perform grid creation and rendering of the calendar
         * @private
         */
        _createViewGrid: function () {
            var gridID = this._configUser.container +'Grid',
                container = jQuery('#' + this._configUser.container),
                gridColumns = this._calendarView.configureGridColumns();

            container.append(PMPlanner.Tools.Render.div({
                id: gridID
            }));

            var scope = this;
            window.onresize = function () {
                setTimeout(function () {
                    scope.callEvent('onAfterViewLoad', []);
                },100);
            };
            this._ARCHIBUSGrid = View.createControl({
                control: 'DataGrid',
                panel: 'gridPanel',
                container: gridID,
                dataSource: 'dataDS',
                loadPanelText: PMPlanner.Constants.TEXT_Loading,
                showFilter: false,
                showSort: false,
                showFooter: true,
                pageSize: 100,
                rowAlternationEnabled: false,
                autoExpandAll: false,
                columns: gridColumns,
                columnReordering: false,
                columnResizing: false,
                events: {
                    onGroupCellPrepared: function (option, row) {
                        scope.callEvent('onGroupCellPrepared', [option]);
                    },
                    onCellPrepared: function (option, row) {
                        scope.callEvent('onCellPrepared', [option]);
                    },
                    onHeaderCellPrepared: function (option) {
                        scope.callEvent('onHeaderCellPrepared', [option]);
                    },
                    onRowPrepared: function (option) {
                        scope.callEvent('onAfterRenderGroupRow', [option]);
                    },
                    afterLoadedData: function () {
                        scope.callEvent('onLoadViewData', [this.getGroupID()]);
                    },
                    afterRefresh: function () {
                        scope.callEvent('onAfterRenderGrid', []);
                        setTimeout(function () {
                            scope.callEvent('onAfterViewLoad', []);
                        },100);

                        scope._toolsMenu.renderMenu();
                        scope._instanceGrid.updateDimensions();
                        var textWidth = function(scope){
                            var sensor = jQuery('<div />').css({margin: 0, padding: 0});
                            jQuery(scope).append(sensor);
                            var width = sensor.width();
                            sensor.remove();
                            return width;
                        };


                        scope._wrapeWords('groupStatistics_char_bar_text', 90);
                        scope._wrapeWords('groupStatistics_text_future', 140);
                    }
                }
            });
            this._instanceGrid = jQuery('#' + gridID).dxDataGrid('instance');
        },
        /**
         * To add custom events to the event manager
         * @private
         */
        _addEvents: function () {
            var events = this._configUser.events;
            for (var eventName in events) {
                this.addEvent(eventName, events[eventName], this);
            }
        },
        /**
         * To obtain the object column was in grid and which is a group
         * @param group - group name
         * @returns DevExtreme object column
         * @private
         */
        _getGroupColumn: function (group) {
            var result = null;
            for (var i = 0; i < this._instanceGrid.columnCount(); i++) {
                var gridColumn = this._instanceGrid.columnOption(i);
                if (gridColumn.groupIndex >= 0 && gridColumn.groupIndex != null) {
                    if (gridColumn.dataField.includes(group))
                        return null;
                    return gridColumn;
                }
            }
            return result;
        },
        /**
         * To validate that grouping the data in a custom column and return the object column
         * @param groupBy - group name
         * @returns DevExtreme object column
         * @private
         */
        _isGroupingUserColumn: function (groupBy) {
            var userColumns = this._configUser.columns;
            var userColumn = null;
            for (var i = 0; i < userColumns.length; i++) {
                var column = userColumns[i];
                if (column['fieldName'] && typeof column['fieldName'] != 'object') {
                    var split = column['fieldName'].split('.');
                    if (split[1] == groupBy) {
                        userColumn = column;
                        break;
                    }
                }
            }

            return userColumn;
        },
        /**
         * Is remove the column which was performed grouping the data
         * @param groupColumn - group name
         * @returns {boolean}
         * @private
         */
        _isRemoveGroupColumn: function (groupColumn) {
            if (groupColumn == null)
                return false;

            var userColumns = this._configUser.columns;
            for (var i = 0; i < userColumns.length; i++) {
                var column = userColumns[i];
                if (column['fieldName'] && typeof column['fieldName'] != 'object') {
                    var dataField = DataGridSource.parseGridFieldName(groupColumn.dataField).fieldName;
                    if (column['fieldName'] == dataField) {
                        return false;
                    }
                }
            }
            return true;
        },

        _wrapeWords: function (id, width) {
            var elements = jQuery('.' + id);
            for (var i = 0; i < elements.length; i++) {
                var element = elements[i];
                var text = element.textContent;
                var words = text.split("").reverse(),
                    word,
                    line = [];
                while (word = words.pop()) {
                    line.push(word);
                    element.textContent = line.join("") + '...';
                    var alength = element.scrollWidth;
                    if (alength >  width) {
                        element.textContent = line.join("") + '...';
                        element.fullText = text;
                        var jquery = jQuery('#' + element.id);
                        new PMPlanner.Tooltip({
                            elem: jquery,
                            cellID: element.id,
                            tooltipID: element.id + 'Tooltip',
                            onRenderText: function (contentElement) {
                                var jquery = jQuery(this._options.target)[0];
                                contentElement.append(PMPlanner.Tools.Render.text({
                                    text: jquery.fullText
                                }));
                            }
                        });

                        break;
                    }
                    element.textContent = line.join("");
                }

            }
        }
    }),

    /**
     * The events manager of the aircraft company which allows you to trigger the necessary events.
     */
    EventManager: Base.extend({
        _eventsMap: {},
        /*
         * Constructor
         */
        constructor: function () {
            this._eventsMap = {};
        },
        /*
         * Add event
         *  @eventName: the name of the events
         *  @callback: function
         *  @scope: the scope of the area events
         */
        addEventListener: function (eventName, callback, scope) {
            if (typeof this._eventsMap[eventName] == "undefined"){
                this._eventsMap[eventName] = {};
            }
            this._eventsMap[eventName] = {
                function: callback,
                scope: scope
            };
        },
        /*
         * Call event
         *  @eventName: the name of the events
         *  @fields: variables functions
         */
        callEvent: function (eventName, fields) {
            if (typeof eventName == "string") {
                eventName = {type: eventName};
            }
            var listeners = this._eventsMap[eventName.type];
            if (listeners) {
                var fun = listeners.function;
                return fun.apply(listeners.scope, fields);
            }
        }
    }),

    /**
     * Data source the data from the database. Allows updating, maintaining and editing local data.
     */
    DataSource: Base.extend({

        /**
         * The data source that contains data for display pm data
         */
        _worksDS: null,
        /**
         * The field on which grouping is performed on the data in the table
         */
        _groupField: null,
        /**
         * Flag to indicate whether to re-load data
         */
        _hasLoadData: null,
        /**
         * Flag to indicate whether to re-load data for statistics
         */
        _hasLoadStatistics: null,
        /**
         * Contains the maximum number of works for future weeks to display in fractions work
         */
        _maxCountFutureWork: null,
        /**
         * Contains data for display works and statistics
         */
        _groupTotalStatistics: null,
        /**
         * Contains data for display finance
         */
        _groupFinance: null,
        /**
         * The data source to work with data financial statistics
         */
        _costLaborDataSource: null,
        /**
         * The count of the month for which you want to show statistics
         */
        _countMonthForLoadStatistics: null,
        /**
         * Constructor
         */
        constructor: function () {
            this._groupTotalStatistics = {};
            this._groupFinance = {};
            this._groupField = null;
            this._worksDS = View.dataSources.get('worksDS');
            this._groupByTradesWorksDS = View.dataSources.get('groupByTradesWorksDS');
            this._hasLoadData = true;
            this._hasLoadStatistics = true;
            this._countMonthForLoadStatistics = 3;
            var sidecar = View.panels.get('gridPanel').getSidecar();
            var pastPMScheduleDates = sidecar.get('pastPMScheduleDates');
            if (valueExists(pastPMScheduleDates)) {
                this._countMonthForLoadStatistics = pastPMScheduleDates;
            }
            this._maxCountFutureWork = 0;
        },
        /**
         * Need load new data from data sources
         */
        shouldQueryData: function () {
            this._hasLoadData = true;
        },
        /**
         * Need load new data for statistics from data sources
         */
        shouldQueryStatisticsData: function () {
            this._hasLoadStatistics = true;
        },
        /**
         * Set the field on which grouping is performed in the grid
         * @param field - field group by
         */
        setGroupingField: function (field) {
            this._groupField = field;
        },
        /**
         * Set field the count of months for loading statistics
         * @param count
         */
        setCountMonthForStatistics: function (count) {
            this._countMonthForLoadStatistics = count;
        },
        /**
         * Get field the count of months for loading statistics
         * @returns {null}
         */
		getCountMonthForStatistics: function () {
			return this._countMonthForLoadStatistics;
		},
        /**
         * Load data in the specified date range
         * @param start - start date
         * @param end - end date
         */
        load: function (start, end) {
            this._start = start;
            this._end = end;
            if (this._hasLoadData) {
                var restriction = this._configRestriction(PMPlanner.DataSource.DataField_DateTodo, start, end);
                this._statistics = {};
                this._works = {};
                this._load(restriction);
                if (this._hasLoadStatistics)
                    this.loadStatistics();

                this._costLaborDataSource._load(start, end);
                this._hasLoadData = false;
            }
        },
        /**
         * Load data for statistics past months
         */
		loadStatistics: function () {
            var start = new Date();
            start = start.add('mo', -this._countMonthForLoadStatistics);
            var end = new Date();
			var restriction = this._configRestriction(PMPlanner.DataSource.DataField_DateTodo, start, end);
			this._groupTotalStatistics = {};
			this._loadStatistics(restriction);
            this._hasLoadStatistics = false;
		},
        loadRecord: function (key, date) {
            var restriction = new Ab.view.Restriction();
            restriction.addClause('pmsd.pms_id', key, '=');
            restriction.addClause('pmsd.date_todo', PMPlanner.Tools.formatDate(date), '=');

            var dataSource = this._getDataSource();
            return dataSource.getRecord(restriction, {});
        },
        loadRecords: function (key) {
            var restriction = this._configRestriction(PMPlanner.DataSource.DataField_DateTodo, this._start, this._end);
            restriction.addClause('pmsd.pms_id', key, '=');

            var dataSource = this._getDataSource();
            return dataSource.getDataSet(restriction, {}).records;
        },
        /**
         * Get statistics for the specified group
         * @param groupName - group name
         * @returns {{}}
         */
        getTotalStatistics: function (groupName) {
            var group = this._groupTotalStatistics[groupName];
            var total = {};
            var futureTotal = {};
            if (!group) {
                total = this._configureGroupData();
            } else {
                total = this._groupTotalStatistics[groupName];
            }
            if(this._statistics[groupName]){
                futureTotal = this._statistics[groupName].total;
                total[PMPlanner.DataSource.FutureJobs_Cancelled] = futureTotal[PMPlanner.DataSource.FutureJobs_Cancelled];
                total[PMPlanner.DataSource.FutureJobs_Generated] = futureTotal[PMPlanner.DataSource.FutureJobs_Generated];
                total[PMPlanner.DataSource.FutureJobs_Planned] = futureTotal[PMPlanner.DataSource.FutureJobs_Planned];
            }
            return total;
        },
        /**
         * Get statistics for the specified group in week
         * @param groupName - group name
         * @param week - week name
         * @returns {{}}
         */
        getWeekStatistics: function (groupName, week) {
            var result = {};
            var group = this._statistics[groupName];
            if (!group) {
                result = this._configureGroupData();
            } else {
                var weekData = this._statistics[groupName][week];
                if (!weekData)
                    result = this._configureGroupData();
                else
                    result = weekData;
            }

            return result;
        },
        /**
         * Get the maximum amount of work in future weeks
         * @returns {null}
         */
        getMaxCountFutureWork: function () {
            return this._maxCountFutureWork;
        },
        /**
         * Get costs for work
         * @param id - key data
         * @param week - week name
         * @returns {string}
         */
        getCostInWork: function (id, week) {
            return this._costLaborDataSource.getCostInWork(id, week);
        },
        /**
         * Get labor hours for work
         * @param id - key data
         * @param week - week name
         * @returns {*}
         */
        getLaborHoursInWork: function (id, week) {
            return this._costLaborDataSource.getLaborHoursInWork(id, week);
        },
        /**
         * Get costs for the specified week for all time and for group
         * @param id - group name
         * @param week - week name
         * @returns {string}
         */
        getCostInWeek: function (id, week) {
            return this._costLaborDataSource.getCostInWeek(id, week);
        },
        /**
         * To get labor hours for the specified week for all time and for group
         * @param id - group name
         * @param week - week name
         * @returns {string}
         */
        getLaborHoursInWeek: function (id, week) {
            return this._costLaborDataSource.getLaborHoursInWeek(id, week);
        },
        /**
         * Get costs for the specified week for all time and for all groups
         * @param week - week name
         * @returns {string}
         */
        getTotalCostInWeek: function (week) {
            return this._costLaborDataSource.getTotalCostInWeek(week);
        },
        /**
         * Get labor hours for the specified week for all time and for all groups
         * @param week - week name
         * @returns {*}
         */
        getTotalLaborHoursInWeek: function (week) {
            return this._costLaborDataSource.getTotalLaborHoursInWeek(week);
        },
        /**
         * Get work for the specified key and the weeks in which the registered block
         * @param id - key data
         * @param week - week name
         * @returns {*}
         */
        getWork: function (id, week) {
            var result = [];
            var works = this._works[id];
            if(!works)
                return null;

            //for trade grouping, the work may duplicate for same schedule code, one schedule
            //may contain multiple trade, so use grouping name to avoid duplicate job count
            var groupName = '';
            if(works.length > 0){
                groupName = works[0].groupName
            }

            for (var i = 0; i < works.length; i++) {
                var workWeek = works[i].week;
                if (workWeek == week  && works[i].groupName == groupName) {
                    result.push(works[i]);
                }
            }
            if (result.length == 0) {
                return null;
            }

            return result;
        },
        /**
         * Get empty setting the statistics to work
         * @returns {*}
         */
        getEmptyStatistics: function () {
            return this._configureGroupData();
        },
        /**
         * To perform an update operation in a local database and change field values in the statistics
         * @param oldWork - the old data on the work
         * @param newWork - the new data on the work
         */
        updateWork: function (oldWork, newWork) {
            var id = oldWork.record.getValue(PMPlanner.DataSource.DataField_PmsId);
            var works = this._works[id];

            for (var i = 0; i < works.length; i++) {
                var workDate = works[i].date;
                if (workDate.getTime() == oldWork.date.getTime()) {
                    this._works[id][i] = newWork;
                    break;
                }
            }

            this._statistics[oldWork.groupName][oldWork.week][oldWork.status] -= 1;
            if (!this._statistics[oldWork.groupName].hasOwnProperty(newWork.week)) {
                this._statistics[oldWork.groupName][newWork.week] = this._configureGroupData();
            }
            this._statistics[oldWork.groupName][newWork.week][newWork.status] += 1;
        },
		/**
         * To clear data in statistics
         */  
		clean: function () {
            this._groupTotalStatistics = {};
        },

        hasMissingWorks: function (week) {
            for (var groupName in this._statistics) {
                var group = this._statistics[groupName];
                var groupWeek = group[week];
                if (groupWeek && groupWeek[PMPlanner.DataSource.PastJobs_Missed] != 0)
                    return true;
            }
            return false;
        },
        createWork: function (record) {
            var groupField = DataGridSource.parseGridFieldName(this._groupField).fieldName;
            var groupValue = '';
            if (groupField != 'pms.none') {
                groupValue = record.getValue(groupField);
            }
            var workDate = record.getValue(PMPlanner.DataSource.DataField_DateTodo),
                dateLatest = record.getValue(PMPlanner.DataSource.DataField_DateLatest);
            if (dateLatest == '') {
                dateLatest = record.getValue(PMPlanner.DataSource.DataField_DateTodo);
            }
            var workData = {
                id: record.getValue(PMPlanner.DataSource.DataField_PmsId),
                groupName: groupValue,
                date: workDate,
                dateEarliest: record.getValue(PMPlanner.DataSource.DataField_DateEarliest),
                dateLatest: dateLatest,
                week: PMPlanner.Calendar.WeekColumn.formatWeek(workDate),
                record: record
            };
            workData['status'] = this.getWorkStatus(workData);
            workData['dateLatest'] = record.getValue(PMPlanner.DataSource.DataField_DateLatest);
            if (workData.status == PMPlanner.DataSource.PastJobs_CompletedLate || workData.status == PMPlanner.DataSource.PastJobs_CompletedOnTime) {
                workData['date'] = record.getValue(PMPlanner.DataSource.DataField_DateCompleted);
                workData['week'] = PMPlanner.Calendar.WeekColumn.formatWeek(workData['date']);
            }

            return workData;
        },
        cleanKeyAndAddWorks: function (key, records) {
            var works = this._works[key];
            works.forEach(function (work) {
                this._statistics[work.groupName][work.week][work.status] -= 1;
                this._statistics[work.groupName].total[work.status] -= 1;
            }, this) ;

            this._works[key] = [];

            records.forEach(function (record) {
                var workData = this.createWork(record);
                this._addWork(workData);
                this._addWorkInStatistics(workData);
                this._statistics[workData.groupName].total[workData.status] += 1;
            }, this);
        },

        /**
         * To configure the data query to the data source to specify the range of output data from the data source
         * @param field - field name
         * @param start - the start date of the range
         * @param end - the end date of the range
         * @returns {Ab.view.Restriction}
         * @private
         */       
        _configRestriction: function (field, start, end) {
            var restriction = new Ab.view.Restriction();
            if (start != null)
                restriction.addClause(field, PMPlanner.Tools.formatDate(start), '&gt;=');
            if (end != null)
                restriction.addClause(field, PMPlanner.Tools.formatDate(end), '&lt;=');
            return restriction;
        },
        /**
         * To configure empty setting the statistics to work
         * @returns {{}}
         * @private
         */
        _configureGroupData: function () {
            var result = {};
            result[PMPlanner.DataSource.PastJobs_CompletedOnTime] = 0;
            result[PMPlanner.DataSource.PastJobs_CompletedLate] = 0;
            result[PMPlanner.DataSource.PastJobs_Missed] = 0;
            result[PMPlanner.DataSource.FutureJobs_Generated] = 0;
            result[PMPlanner.DataSource.FutureJobs_Planned] = 0;
            result[PMPlanner.DataSource.FutureJobs_Cancelled] = 0;

            return result;
        },
        /**
         * To find the maximum count of future works in the loaded data
         * @private
         */
        _findMaxCountFutureWork: function () {
            var max = 0;
            for (var key in this._statistics) {
                var total = this._statistics[key].total;

                var generated = total[PMPlanner.DataSource.FutureJobs_Generated],
                    planned = total[PMPlanner.DataSource.FutureJobs_Planned];

                if (max < generated)
                    max = generated;
                if (max < planned)
                    max = planned;
            }

            this._maxCountFutureWork = max;
        },
		/**
         * To load data from a data source and to calculate statically for groups and weeks
		 * @param restriction - load options data
         * @private
         */
        _load: function (restriction) {
            var dataSource = this._getDataSource();
            var records = dataSource.getDataSet(restriction, {}).records;

            for (var i = 0; i < records.length; i++) {
                var record = records[i];
                var workData = this.createWork(record);

                this._addWork(workData);
                this._addWorkInStatistics(workData);
            }
            this._addTotalStatistics();
        },
        /**
         * To load data from a data source and to calculate statically for groups
         * @param restriction
         * @private
         */
		_loadStatistics: function (restriction) {
            var dataSource = this._getDataSource();
            var records = dataSource.getDataSet(restriction, {}).records,
                groupField = DataGridSource.parseGridFieldName(this._groupField).fieldName;
			
			for (var i = 0; i < records.length; i++) {
				var record = records[i];
                var workData = this.createWork(record);
                var groupValue = '';
                if (groupField != 'pms.none') {
                    groupValue = record.getValue(groupField);
                }

				if (!this._groupTotalStatistics[groupValue])
					this._groupTotalStatistics[groupValue] = this._configureGroupData();
				this._groupTotalStatistics[groupValue][workData['status']] += 1;
			}

		},
		/**
		 * Save work in an array of works with the same identifiers
		 * @param work - object for work
         * @private
         */
        _addWork: function (work) {
            if (!this._works.hasOwnProperty(work.id)) {
                this._works[work.id] = [];
            }
            this._works[work.id].push(work);
        },
		/**
		 * Add work with statistics of the past week on which the work
		 * @param work - object for work
         * @private
         */
        _addWorkInStatistics: function (work) {
            if (!this._statistics.hasOwnProperty(work.groupName)) {
                this._statistics[work.groupName] = {};
                this._statistics[work.groupName].total = this._configureGroupData();
            }

            if (!this._statistics[work.groupName].hasOwnProperty(work.week)) {
                this._statistics[work.groupName][work.week] = this._configureGroupData();
            }

            this._statistics[work.groupName][work.week][work.status] += 1;
        },
        /**
         * Add work with statistics of the future week on which the work
         * @private
         */
        _addTotalStatistics: function () {
            for (var group in this._statistics) {
                var workGroup = this._statistics[group];
                for (var week in workGroup) {
                    var weekStatistics = workGroup[week];

                    workGroup.total[PMPlanner.DataSource.PastJobs_CompletedOnTime] += weekStatistics[PMPlanner.DataSource.PastJobs_CompletedOnTime];
                    workGroup.total[PMPlanner.DataSource.PastJobs_CompletedLate] += weekStatistics[PMPlanner.DataSource.PastJobs_CompletedLate];
                    workGroup.total[PMPlanner.DataSource.PastJobs_Missed] += weekStatistics[PMPlanner.DataSource.PastJobs_Missed];
                    workGroup.total[PMPlanner.DataSource.FutureJobs_Generated] += weekStatistics[PMPlanner.DataSource.FutureJobs_Generated];
                    workGroup.total[PMPlanner.DataSource.FutureJobs_Planned] += weekStatistics[PMPlanner.DataSource.FutureJobs_Planned];
                    workGroup.total[PMPlanner.DataSource.FutureJobs_Cancelled] += weekStatistics[PMPlanner.DataSource.FutureJobs_Cancelled];
                }
            }
            this._findMaxCountFutureWork();
        },
		/**
		 * Get the status of works depending on the data specified in the object from the database
		 * @param work - object for work
         */
        getWorkStatus: function (work) {
            var dateCompleted = work.record.getValue(PMPlanner.DataSource.DataField_DateCompleted),
                woId = work.record.getValue(PMPlanner.DataSource.DataField_WoId),
                isCancelled = work.record.getValue(PMPlanner.DataSource.DataField_IsCancelled);

            var compareCurrentDate = this._compareFullDate(work.dateLatest, new Date());

            if (isCancelled == "1")
                return PMPlanner.DataSource.FutureJobs_Cancelled;

            if (compareCurrentDate == 'less' && woId == "")
                return PMPlanner.DataSource.PastJobs_Missed;

            if (dateCompleted != "") {
                var compare = this._compareFullDate(dateCompleted, work.dateLatest);
                if (compare == 'equal' || compare == 'less')
                    return PMPlanner.DataSource.PastJobs_CompletedOnTime;

                if (compare == 'more')
                    return PMPlanner.DataSource.PastJobs_CompletedLate;

            } else {
                if (compareCurrentDate == 'less' && woId != "")
                    return PMPlanner.DataSource.PastJobs_Missed;

                if (woId != "")
                    return PMPlanner.DataSource.FutureJobs_Generated;

                return PMPlanner.DataSource.FutureJobs_Planned;
            }

            return PMPlanner.DataSource.NoneJobs;
        },

        _compareFullDate: function (first, second) {
            var result = this._compareDate(first, second, 'Y');
            if (result == 'equal') {
                result = this._compareDate(first, second, 'n');
                if (result == 'equal') {
                    result = this._compareDate(first, second, 'j');
                }
            }
            return result;
        },

        _compareDate: function (first, second, scale) {
            var result = 'none';
            var firstValue = parseInt(first.format(scale));
            var secondValue = parseInt(second.format(scale));
            if (firstValue == secondValue) {
                result = 'equal';
            } else if (firstValue > secondValue) {
                result = 'more';
            } else {
                result = 'less';
            }

            return result;
        },
        _getDataSource: function () {
            var ds;
            var groupField = DataGridSource.parseGridFieldName(this._groupField).fieldName;
            if('pms.tr_id' == groupField){
                ds = this._groupByTradesWorksDS;
            }else{
                ds = this._worksDS;
            }
            return ds;
        }
    }, {
        DataField_Count: 'pmsd.pmsd_count',
        DataField_Week: 'pmsd.week',
        DataField_DateTodo: 'pmsd.date_todo',
        DataField_PmsId: 'pmsd.pms_id',
        DataField_DateCompleted: 'pmsd.date_completed',
        DataField_DateLatest: 'pmsd.date_latest',
        DataField_WoId: 'pmsd.wo_id',
        DataField_DateEarliest: 'pmsd.date_earliest',
        DataField_IsCancelled: 'pmsd.is_cancelled',

        PastJobs_CompletedOnTime: 'pastJobs-completedOnTime',
        PastJobs_CompletedLate: 'pastJobs-completedLate',
        PastJobs_Missed: 'pastJobs-missed',
        FutureJobs_Generated: 'futureJobs-generated',
        FutureJobs_Planned: 'futureJobs-planned',
        FutureJobs_Cancelled: 'futureJobs-cancelled',
        NoneJobs: 'noneJobs'
    }),    
	/**
	 * Tooltip the calling user when it's hover on the given element. The tooltip text is given if the user holds the mouse over the element for more than 2 seconds
	 */
    Tooltip: Base.extend({

        _option: null,

        constructor: function (option) {
            this._option = option;
            this._elem = option.elem;
            this._initFields();
            this._addTooltip(this._option);

            var scope = this;

            this._onMouseOver = function (event) {
                if (scope._isOverElement) {
                    return;
                }
                scope._isOverElement = true;
                scope._pX = event.pageX;
                scope._pY = event.pageY;
                scope._pTime = Date.now();
                scope._elem.on('mousemove', scope._onMouseMove);
                scope._checkSpeedInterval = setInterval(scope._trackSpeed, scope._options.interval);
            };
            this._onMouseOut  = function (event) {
                var t = scope._elem.find(event.relatedTarget);
                if (!event.relatedTarget || scope._elem.find(event.relatedTarget).length == 0) {
                    scope._isOverElement = false;
                    scope._elem.off('mousemove', this._onMouseMove);
                    clearInterval(scope._checkSpeedInterval);
                    if (scope._isHover) {
                        scope._options.out.call(scope._elem, event);
                        scope._isHover = false;
                    }
                }
            };
            this._onMouseMove = function (event) {
                scope._cX = event.pageX;
                scope._cY = event.pageY;
                scope._cTime = Date.now();
                var headerTitle = scope._elem.children();
                headerTitle.attr('title', ' ');
            };
            this._trackSpeed = function (event) {
                var speed;

                if (!scope._cTime || scope._cTime == scope._pTime) {
                    speed = 0;
                } else {
                    speed = Math.sqrt(Math.pow(scope._pX - scope._cX, 2) + Math.pow(scope._pY - scope._cY, 2)) / (scope._cTime - scope._pTime);
                }

                if (speed < scope._options.sensitivity) {
                    clearInterval(scope._checkSpeedInterval);
                    scope._isHover = true;
                    scope._options.over.call(scope._elem, event);
                } else {
                    scope._pX = scope._cX;
                    scope._pY = scope._cY;
                    scope._pTime = scope._cTime;
                }
            };

            this._elem.on("mouseover", this._onMouseOver);
            this._elem.on("mouseout", this._onMouseOut);
        },
		/**
		 * Show tooltip
		 */
        show: function () {
            jQuery('#' + this._option.tooltipID).dxTooltip("instance").show();
        },
		/**
		 * Hide tooltip
		 */
        hide: function () {
            //jQuery('#' + this._option.tooltipID).dxTooltip("instance").hide();
            jQuery('#' + this._option.tooltipID).remove();
        },
        /**
         * Remove tooltip
         */
        remove: function () {
            this._elem.css('cursor', 'default');
            this._elem.off("mouseover");
            this._elem.off("mouseout");
            jQuery('#' + this._option.tooltipID).remove();
        },
		/**
		 * Initialize private fields of the class
		 */
        _initFields: function () {
            this._options = {
                interval: 100,
                sensitivity: 0.1
            };
            this._cX = 0;
            this._cY = 0;
            this._pX = 0;
            this._pY = 0;
            this._cTime = 0;
            this._pTime = 0;
            this._checkSpeedInterval = null;
            this._isOverElement = false;
            this._isHover = false;
        },
		/**
		 * To create a tooltip generated by devExpress
		 * @param option - {tooltipID: id dom element tooltip; cellID: id dom element for which is displayed tooltip; onRenderText: function for renderind text in the tooltip}
		 */
        _addTooltip: function (option) {
            var tooltipID = option.tooltipID;
            if (option.tooltipID.includes(' ')) {
                tooltipID = option.tooltipID.replace(' ', '_');
                option.tooltipID = tooltipID;
            }

            var tooltip = PMPlanner.Tools.Render.div({
                id: tooltipID
            });
            tooltip.dxTooltip({
                target: '#' + option.cellID,
                position: 'top',
                contentTemplate: option.onRenderText
            });
            this._elem.append(tooltip);

            this._options.over = function() {
                jQuery('#' + tooltipID).dxTooltip("instance").show();
            };
            this._options.out = function() {
                jQuery('#' + tooltipID).dxTooltip("instance").hide();
            };
        }

    },{
        /**
         * To execute the date format: "day of the week"_"date"
         * @param date - date for formating
         * @returns {string}
         */
        formatDate: function(date) {
            return PMPlanner.Constants.WEEK_DAY[date.getDay()] +
                ' ' +
                View.dataSources.get('worksDS').formatValue(PMPlanner.DataSource.DataField_DateTodo, date, true);
        }
    }),

    Constants: {
        IMG_SELECT_NEXT_WEEK: '/archibus/schema/ab-products/bldgops/prevmaint/planner/right-arrow.png',
        IMG_SELECT_PREV_WEEK: '/archibus/schema/ab-products/bldgops/prevmaint/planner/left-arrow.png',
        IMG_COMPLETED: '/archibus/schema/ab-products/bldgops/prevmaint/planner/check-symbol.png',
        IMG_WARNING: '/archibus/schema/ab-products/bldgops/prevmaint/planner/warning.png',
        IMG_CANCELED: '/archibus/schema/ab-products/bldgops/prevmaint/planner/divide-mathematical-sign.png',
        IMG_CANCELED_SELECT: '/archibus/schema/ab-products/bldgops/prevmaint/planner/divide-mathematical-sign-select.png',
        IMG_ARROW_NEXT: '/archibus/schema/ab-products/bldgops/prevmaint/planner/next-week.png',
        IMG_ARROW_PREV: '/archibus/schema/ab-products/bldgops/prevmaint/planner/prev-week.png',
        IMG_BRACKET_NEXT: '/archibus/schema/ab-products/bldgops/prevmaint/planner/brackets-grouping-symbol-right.png',
        IMG_BRACKET_PREV: '/archibus/schema/ab-products/bldgops/prevmaint/planner/brackets-grouping-symbol-left.png',
        IMG_SETTINGS: '/archibus/schema/ab-products/bldgops/prevmaint/planner/settings.png',
    }
};
