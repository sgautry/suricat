/**
 * Outputted the financial weekly statistics for each group and for the entire table. In the grid add a new row which shows the necessary information
 */
PMPlanner.FinanceInformation = Base.extend({

    /**
     * Main view
     */
    _view: null,
    /**
     * An array containing the indexes of the rows after which to add data
     */
    _groupRowIndexes: null,
    /**
     * An array containing the IDs of the created rows
     */
    _arrRowsId: null,
    /**
     * Need to show costs
     */
    _showCosts: null,
    /**
     * Need to show labor
     */
    _showLabor: null,
    /**
     * Constructor
     * @param option
     */
    constructor: function (option) {
        this._view = option.view;
        this._calendarDataSource = this._view.getCalendarView().getDataSource();
        this._initFields();
        this._addEvents();
    },
	/**
	 * To perform the show Costs
	 * @param show - TRUE if show and FALSE if not show Costs
	 */
    showCosts: function (show) {
        this._showCosts = show;
    },
	/**
	 * To perform the show Labor
	 * @param show - TRUE if show and FALSE if not show Labor
	 */
    showLabor: function (show) {
        this._showLabor = show;
    },
	/**
	 * Is the option to show costs
	 */
    isShowCosts: function () {
        return this._showCosts;
    },
	/**
	 * Is the option to show labor hours
	 */
    isShowLabor: function () {
        return this._showLabor;
    },

    refresh: function () {
        for (var i = 0; i < this._arrRowsId.length; i++) {
            jQuery('#' + this._arrRowsId[i]).remove();
        }
        this._afterRenderGrid();
    },

    update: function () {
        var scope = this;

        this._arrRowsId.forEach(function (id) {
            var row = jQuery('#' + id);
            var child = row.children();
            for (var i = 0; i < child.length; i++) {
                var td = child[i];
                var week = td.getAttribute('week');
                if (week) {
                    var split = id.split('_');
                    var html = '';
                    var value = 0;
                    switch (split[0]) {
                        case 'groupCost':
                            value = scope._formattedNumeric(scope._view.getDataSource().getCostInWeek(split[1], week));
                            html = '<div style="padding-right: 2px; text-align: right;">' + PMPlanner.Constants.TEXT_CurrencySymbol + value + '</div>';
                            break;
                        case 'groupLabor':
                            value = scope._formattedNumeric(scope._view.getDataSource().getLaborHoursInWeek(split[1], week));
                            html = '<div style="padding-right: 2px; text-align: right;">' + value + '</div>';
                            break;
                        case 'totalCost':
                            value = scope._formattedNumeric(scope._view.getDataSource().getTotalCostInWeek(week));
                            html = '<div style="padding-right: 2px; text-align: right;">' + PMPlanner.Constants.TEXT_CurrencySymbol + value + '</div>';
                            break;
                        case 'totalLabor':
                            value = scope._formattedNumeric(scope._view.getDataSource().getTotalLaborHoursInWeek(week));
                            html = '<div style="padding-right: 2px; text-align: right;">' + value + '</div>';
                            break;
                    }
                    td.innerHTML = html;
                }
            }
        });
    },

	/**
	 * Initialize class fields
	 */
    _initFields: function () {
        this._groupRowIndexes = [];
        this._arrRowsId = [];
        this._showCosts = false;
        this._showLabor = false;
    },
	/**
	 * Add a custom event class
	 */
    _addEvents: function () {
        this._view.addEvent('onAfterRenderGrid', this._afterRenderGrid, this);
        this._view.addEvent('onAfterRenderGroupRow', this._afterRenderGroupRow, this);
    },
	/**
	 * Get the configuration of rows displayed in the grid for groups
	 */
    _getGroupRowsConfig: function () {
        var scope = this;
        var result = [];

        if (this._showCosts)
            result.push({
                rowIndex: 0,
                rowId: function (id) {
                    return 'groupCost_' + id;
                },
                gridType: PMPlanner.FinanceInformation.GridType_Rows,
                title: {
                    colSpan: scope._calculateCountColumnsGrid(),
                    cssClass: PMPlanner.FinanceInformation.CssClass_TitleGroupColumn,
                    text: '<div style="padding-right: 10px;">' + PMPlanner.Constants.TEXT_Cost + ':</div>'
                },
                weekCell: [{
                    colSpan: 1,
                    cssClass: PMPlanner.FinanceInformation.CssClass_WeekCell,
                    currentWeek: false,
                    text: function (id, week) {
                        var value = scope._formattedNumeric(scope._view.getDataSource().getCostInWeek(id, week)) ;
                        return '<div style="padding-right: 2px; text-align: right;">' + PMPlanner.Constants.TEXT_CurrencySymbol + value + '</div>';
                    }
                }]

            });

        if (this._showLabor)
            result.push({
                rowIndex: 0,
                rowId: function (id) {
                    return 'groupLabor_' + id;
                },
                gridType: PMPlanner.FinanceInformation.GridType_Rows,
                title: {
                    colSpan: scope._calculateCountColumnsGrid(),
                    cssClass: PMPlanner.FinanceInformation.CssClass_TitleGroupColumn,
                    text: '<div style="padding-right: 10px;">' + PMPlanner.Constants.TEXT_LaborHours + ':</div>'
                },
                weekCell: [{
                    colSpan: 1,
                    cssClass: PMPlanner.FinanceInformation.CssClass_WeekCell,
                    currentWeek: false,
                    text: function (id, week) {
                        var value = scope._formattedNumeric(scope._view.getDataSource().getLaborHoursInWeek(id, week));
                        return '<div style="padding-right: 2px; text-align: right;">' + value + '</div>';
                    }
                }]
            });


        if (this._showLabor || this._showCosts)
            result.push({
                rowIndex: 0,
                rowId: function (id) {
                    return 'financeStatisticsLast';
                },
                gridType: PMPlanner.FinanceInformation.GridType_Rows,
                title: {
                    colSpan: scope._calculateCountColumnsGrid(),
                    cssClass: PMPlanner.FinanceInformation.CssClass_LastRow
                }
            });
        

        return result;
    },
    /**
	 * Get the configuration of rows displayed in the grid for footer 
	 */
	_getConfigFooterRows: function () {
        var scope = this;
        var result = [];

        if (this._showCosts)
            result.push({
                rowIndex: 0,
                rowId: function (id) {
                    return 'totalCost';
                },
                gridType: PMPlanner.FinanceInformation.GridType_Footer,
                title: {
                    colSpan: scope._calculateCountColumnsGrid(),
                    cssClass: PMPlanner.FinanceInformation.CssClass_TitleGroupColumn,
                    text: '<div style="padding-right: 10px;">' + PMPlanner.Constants.TEXT_TotalCosts + ': </div>'
                },
                weekCell: [{
                    colSpan: 1,
                    cssClass: PMPlanner.FinanceInformation.CssClass_WeekCell,
                    currentWeek: false,
                    text: function (id, week) {
                        var value = scope._formattedNumeric(scope._view.getDataSource().getTotalCostInWeek(week));
                        return '<div style="padding-right: 2px; text-align: right;">' + PMPlanner.Constants.TEXT_CurrencySymbol + value + '</div>';
                    }
                }]

            });

        if (this._showLabor)
            result.push({
                rowIndex: 0,
                rowId: function (id) {
                    return 'totalLabor';
                },
                gridType: PMPlanner.FinanceInformation.GridType_Footer,
                title: {
                    colSpan: scope._calculateCountColumnsGrid(),
                    cssClass: PMPlanner.FinanceInformation.CssClass_TitleGroupColumn,
                    text: '<div style="padding-right: 10px;">' + PMPlanner.Constants.TEXT_TotalLaborHours + ': </div>'
                },
                weekCell: [{
                    colSpan: 1,
                    cssClass: PMPlanner.FinanceInformation.CssClass_WeekCell,
                    currentWeek: false,
                    text: function (id, week) {
                        var value = scope._formattedNumeric(scope._view.getDataSource().getTotalLaborHoursInWeek(week)) ;
                        return '<div style="padding-right: 2px; text-align: right;">' + value + '</div>';
                    }
                }]

            });

        return result;
    },
	/**
	 * The event method running after the rendering of the row groups. Save indexes of table rows for groups
	 * @param devExpressOption 
	 */ 
    _afterRenderGroupRow: function (devExpressOption) {
        if (devExpressOption.rowType == 'group' && devExpressOption.isExpanded) {
            var dataGroup = {
                id: devExpressOption.data.key,
                rowIndex: devExpressOption.rowIndex
            };
            this._groupRowIndexes.push(dataGroup);
        }
    },
	/**
	 * The event method is executed after drawing all the grid rows and displays additional rows for financial statistics
	 */ 
    _afterRenderGrid: function () {
        this._arrRowsId = [];
        this._renderGroupFinance();
        this._renderFooterFinance();

    },
	/**
	 * To perform the drawing of lines to display of financial statistics for the footer grid
	 */
    _renderFooterFinance: function () {
        var grid = this._getGrid(PMPlanner.FinanceInformation.GridType_Footer);
        while (true) {
            if (grid.rows.length != 0)
                grid.deleteRow(0);
            else
                break;
        }


        grid.parentElement.style.padding = 0;

        var config = this._getConfigFooterRows();
        if (config.length == 0)
            return;

        for (var i = 0; i < config.length; i++) {
            config[i].rowIndex = i;
        }
        this._renderRows(config);
    },
	/**
	 * To perform the drawing of lines to display of financial statistics for the group
	 */
    _renderGroupFinance: function () {
        var config = this._getGroupRowsConfig();
        if (config.length == 0) {
            this._groupRowIndexes = [];
            return;
        }

        if (this._groupRowIndexes.length != 0) {
            for (var i = 0; i < this._groupRowIndexes.length; i++) {
                var dataGroup = this._groupRowIndexes[i];
                var rowIndex = dataGroup.rowIndex + 1;
                if (i != 0) {
                    rowIndex += ((config.length -1) * i) + 1;
                }

                for (var j = 0; j < config.length; j++) {
                    config[j].rowIndex = rowIndex + j;
                    config[j].id = dataGroup.id;
                }
                this._renderRows(config);
            }
        }
        this._groupRowIndexes = [];
    },
	/**
     * To perform rending lines for financial statistics, depending on a given configuration
	 * @param config - based configuration which are drawing the lines 
	 */
    _renderRows: function (config) {
        for (var i = 0; i < config.length; i++) {
            var rowConfig = config[i];
            var columnOption = this._configureRow(rowConfig),
                grid = this._getGrid(rowConfig.gridType);

            this._createRow(rowConfig.rowIndex, rowConfig.rowId.apply(null, [rowConfig.id]), grid, columnOption);
        }
    },
	/**
	 * Perform the configuration of the row and fill with data to create a string
	 * @param config - based configuration which are drawing the lines 
	 */
    _configureRow: function (config) {
        var result = [{ colSpan: 1 }];
        if (config.title) {
            result.push(config.title);
        }

        if (config.weekCell) {
            var weeksCell = config.weekCell;

            if (weeksCell.length == 1) {
                this._calendarDataSource.eachWeekColumn(function (propertyName, weekObj) {
                    if (weekObj.getDate() != null) {
                        var configCell = jQuery.extend(true, {}, weeksCell[0]);
                        configCell.text = configCell.text.apply(null, [config.id, weekObj.getFormat()]);
                        configCell.currentWeek = weekObj.isCurrentWeek();
                        configCell.attr = {week: weekObj.getFormat() };

                        if (weekObj.isFutureWeek()) {
                            configCell.cssClass += ' ' + PMPlanner.FinanceInformation.CssClass_FutureCell;
                        }
                        result.push(configCell);
                    }
                });
            } else {
                for (var i = 0; i < weeksCell.length; i++) {
                    var configCell = weeksCell[i];
                    if (configCell.colSpan != 0)
                        result.push(configCell);
                }
            }
        }

        return result;

    },
	/**
	 * Create string and add it to the grid
	 * @param rowIndex - the index of the row after which need to add new rows
	 * @param grid - obejct to the grid
     * * @param id - id for row
	 * @param columnsOption - columns data
	 */
    _createRow: function (rowIndex, id, grid, columnsOption) {
        var rowId = PMPlanner.Tools.createCorrectID(id);
        var rowDom = jQuery('#' + rowId).children();
        if (rowDom.length != 0)
            return;

        var row = grid.insertRow(rowIndex);
        row.id = rowId;
        row.className = PMPlanner.FinanceInformation.CssClass_Row;
        this._arrRowsId.push(id);

        var countTitleCell = 0;
        for (var i = 0; i < columnsOption.length; i++) {
            var column = columnsOption[i];
            if (column.colSpan && column.colSpan == 0)
                continue;

            var cell = row.insertCell(i + countTitleCell);
            var className = PMPlanner.FinanceInformation.CssClass_Cell;

            if (column.colSpan)
                cell.colSpan = column.colSpan;


            if (column.attr)
                for (var key in column.attr)
                    cell.setAttribute(key, column.attr[key]);

            if (column.cssClass)
                className += ' ' + column.cssClass;
            if (column.currentWeek) {
                className += ' ' + PMPlanner.FinanceInformation.CssClass_CurrentWeek;
            }
            cell.className = className;
            if (column.text)
                cell.innerHTML = column.text;
        }
    },
	/**
     * To the grid object which will be added a new rows	
	 */
    _getGrid: function (gridType) {
        var grid = null;

        switch (gridType) {
            case PMPlanner.FinanceInformation.GridType_Rows:
                grid = this._view.getDOMGrid().find('table')[1];
                break;
            case PMPlanner.FinanceInformation.GridType_Footer:
                grid = this._view.getDOMGrid().find('table')[2];
                break;
        }

        return grid;
    },
	/**
	 * To calculate the number of columns that are visible to the user in the grid
	 */
    _calculateCountColumnsGrid: function () {
        var grid = this._view.getInstanceGrid();
        var configUser = this._view._configUser;
        var result = 1;
        for (var i = 0; i < grid.columnCount(); i++) {
            var gridColumn = grid.columnOption(i);
            if (!gridColumn.groupIndex && gridColumn.groupIndex != 0 && gridColumn.visible == true) {
                for (var j = 0; j < configUser.columns.length; j++) {
                    if (gridColumn.id && gridColumn.id == configUser.columns[j].id) {
                        result++;
                        break;
                    }

                }
            }
        }

        return result;
    },

    _formattedNumeric: function (value) {
        return PMPlanner.Tools.formattedNumeric(value);
    }

}, {
    CssClass_Cell: 'information_cell',
    CssClass_Row: 'information_row',
    CssClass_CurrentWeek: 'highlightCurrentWeek',
    CssClass_TitleGroupColumn: 'information_titleGroupColumn',
    CssClass_TitleFooterColumn: 'information_titleFooterColumn',
    CssClass_WeekCell: 'information_weekColumn',
    CssClass_LastRow: 'information_lastRow',
    CssClass_FutureCell: 'information_future_cell',

    GridType_Rows: 'rowsGrid',
    GridType_Footer: 'footerGrid',

    RowType_Group_Cost: 'groupTitle_cost',
    RowType_Group_LaborHours: 'groupTitle_laborHours',
    RowType_Group_LastRos: 'group_lastRow',
});
