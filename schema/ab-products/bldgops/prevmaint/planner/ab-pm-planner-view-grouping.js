/**
 * Contains a method for displaying total statistics and update it when loading new data
 */
PMPlanner.GroupTotalStatistics = Base.extend({
    /**
     * Group name
     */
    _title: null,
    /**
     * Block ID displays statistics for the past weeks
     */
    _charBarID: null,
    /**
     * Block ID displays statistics for the future weeks
     */
    _blocksBarID: null,
    /**
     * Cell in grid
     */
    _devExpressCell: null,
    /**
     * Option for cell
     */
    _devExpressOption: null,
    /**
     * Main view
     */
    _view: null,
    /**
     * The data from the database to display statistics
     */
    _statisticsGroup: null,
    /**
     * Contains the maximum number of works for future weeks to display in fractions work
     */
    _maxCountFutureWork: null,
    /**
     * Constructor
     * @param devExpressOption - option for cell
     * @param view - main view
     */
    constructor: function (devExpressOption, view) {
        this._title = devExpressOption.text;
        if (this._title == '') {
            this._title = PMPlanner.Constants.TEXT_N_A;
        }

        this._devExpressCell = devExpressOption.cellElement;
        this._charBarID = PMPlanner.Tools.createCorrectID('charBar' + devExpressOption.text);
        this._blocksBarID = PMPlanner.Tools.createCorrectID('blocksBar' + devExpressOption.text);
        this._view = view;
        this._devExpressOption = devExpressOption;

        this._statisticsGroup = view.getDataSource().getTotalStatistics(devExpressOption.text);
        this._maxCountFutureWork = view.getDataSource().getMaxCountFutureWork();

        this.render();
    },
    /**
     * To render the statistics in the required form
     */
    render: function () {
        this._renderTitle();
        this._renderStatisticsForPrevWeeks();
        this._renderStatisticsForFutureWeeks();

        var scope = this;
        this._devExpressCell.on('mousemove', function() {
            var headerTitle = scope._devExpressCell.children();
            headerTitle.attr('title', ' ');
        });
        this._devExpressCell.on('click', function () {
            var option = scope._devExpressOption;
            var columnName = DataGridSource.parseGridFieldName(option.column.dataField);
            scope._view.callEvent('onEditMultiplePmSchedules', [columnName.fieldName, option.data.key]);
        });
        this._devExpressCell.css('cursor', 'pointer');
    },
    /**
     * To update the statistics and redraws the blocks
     */
    update: function () {
        var groupName = this._title;
        if (groupName == PMPlanner.Constants.TEXT_N_A) {
            groupName = '';
        }
        this._statisticsGroup = this._view.getDataSource().getTotalStatistics(groupName);
        this._devExpressCell.html('');
        this.render();
    },
    /**
     * To render group title
     * @private
     */
    _renderTitle: function () {
        var scope = this;
        var header = PMPlanner.Tools.Render.text({
            cssClass: 'groupStatistics_title',
            text:this._title.toUpperCase()
        });
        this._devExpressCell.html('');
        this._devExpressCell.append(header);
    },
    /**
     * To render the statistics for past weeks
     * @private
     */
    _renderStatisticsForPrevWeeks: function () {
        var scope = this;
        var total = this._statisticsGroup[PMPlanner.DataSource.PastJobs_Missed] +
            this._statisticsGroup[PMPlanner.DataSource.PastJobs_CompletedLate] +
            this._statisticsGroup[PMPlanner.DataSource.PastJobs_CompletedOnTime];
        if (this._charBarID.includes(' ')) {
            this._charBarID = this._charBarID.replace(' ', '_');
        }

        var charBar = PMPlanner.Tools.Render.div({
            id: this._charBarID,
            cssClass: 'groupStatistics_char_bar'
        });

        var totalWidth = total,
            completedOnTimeWidth = 0,
            missedWidth = 0,
            completedLateWidth = 0;

        if (this._statisticsGroup[PMPlanner.DataSource.PastJobs_CompletedOnTime] != 0) {

            completedOnTimeWidth = parseInt(this._statisticsGroup[PMPlanner.DataSource.PastJobs_CompletedOnTime] / total * 100);
            if (completedOnTimeWidth < 20) {
                totalWidth = total - this._statisticsGroup[PMPlanner.DataSource.PastJobs_CompletedOnTime] + total * 0.2;
            }
            charBar.append(PMPlanner.Tools.Render.img({
                image: PMPlanner.Constants.IMG_COMPLETED,
                cssClass: 'groupStatistics_char_bar_check'
            }));
        }

        missedWidth = parseInt(this._statisticsGroup[PMPlanner.DataSource.PastJobs_Missed] / totalWidth * 100);
        completedLateWidth = parseInt(this._statisticsGroup[PMPlanner.DataSource.PastJobs_CompletedLate] / totalWidth * 100);

        charBar.append(this._renderArea(
            missedWidth,
            'groupStatistics_char_bar_missing'
        ));
        charBar.append(this._renderArea(
            completedLateWidth,
            'groupStatistics_char_bar_expired'
        ));
        this._devExpressCell.append(charBar);
        var percent = (this._statisticsGroup[PMPlanner.DataSource.PastJobs_CompletedOnTime] / total) * 100;
        if (isNaN(percent)) {
            percent = 0;
        }

        this._devExpressCell.append(PMPlanner.Tools.Render.text({
            id: this._charBarID + '_past_text',
            text:  parseInt(percent) + PMPlanner.Constants.TEXT_On_Time,
            cssClass: 'groupStatistics_char_bar_text'
        }));

        new PMPlanner.Tooltip({
            elem: charBar,
            cellID: this._charBarID,
            tooltipID: this._charBarID + 'Tooltip',
            onRenderText: function (contentElement) {
                contentElement.append(PMPlanner.Tools.Render.text({
                    text: PMPlanner.Constants.TEXT_CompletedOnTime + ': ' + scope._statisticsGroup[PMPlanner.DataSource.PastJobs_CompletedOnTime]
                }));
                contentElement.append(PMPlanner.Tools.Render.text({
                    text: PMPlanner.Constants.TEXT_CompletedLate + ': ' + scope._statisticsGroup[PMPlanner.DataSource.PastJobs_CompletedLate]
                }));
                contentElement.append(PMPlanner.Tools.Render.text({
                    text: PMPlanner.Constants.TEXT_Missed + ': ' + scope._statisticsGroup[PMPlanner.DataSource.PastJobs_Missed]
                }));
            }
        });

    },
    /**
     * Get <div> for area to display how many percent is the data of the same type
     * @param width - the size of the item
     * @param cssClass - style type statistics
     * @returns {*}
     * @private
     */
    _renderArea: function (width, cssClass) {
        return PMPlanner.Tools.Render.div({
            cssClass: cssClass,
            cssFields: {
                width: width + '%'
            }
        });
    },
    /**
     * To render the statistics for future weeks
     * @private
     */
    _renderStatisticsForFutureWeeks: function () {
        var scope = this;

        var text = PMPlanner.Tools.Render.text({
            id: this._blocksBarID + '_text_future',
            text: (parseInt(this._statisticsGroup[PMPlanner.DataSource.FutureJobs_Planned]) + parseInt(this._statisticsGroup[PMPlanner.DataSource.FutureJobs_Generated])) + ' ' + PMPlanner.Constants.TEXT_Scheduled,
            cssClass: 'groupStatistics_text_future'
        });

        this._devExpressCell.append(text);
        if (this._blocksBarID.includes(' ')) {
            this._blocksBarID = this._blocksBarID.replace(' ', '_');
        }

        var futureStatistics = PMPlanner.Tools.Render.div({
            id: this._blocksBarID,
            cssClass: 'groupStatistics_future_statistics',
            cssFields: {
                height: (PMPlanner.Calendar.CELL_HEIGHT - 5) + 'px'
            }
        });

        futureStatistics.append(this._renderStatistics(
            'groupStatistics_future_generated_line',
            'groupStatistics_future_generated_block',
            PMPlanner.DataSource.FutureJobs_Generated
        ));
        futureStatistics.append(this._renderStatistics(
            'groupStatistics_future_planned_line',
            'groupStatistics_future_planned_block',
            PMPlanner.DataSource.FutureJobs_Planned
        ));

        this._devExpressCell.append(futureStatistics);

        new PMPlanner.Tooltip({
            elem: futureStatistics,
            cellID: this._blocksBarID,
            tooltipID: this._blocksBarID + 'Tooltip',
            onRenderText: function (contentElement) {
                contentElement.append(PMPlanner.Tools.Render.text({
                    text: PMPlanner.Constants.TEXT_Generated + ': ' + scope._statisticsGroup[PMPlanner.DataSource.FutureJobs_Generated]
                }));
                contentElement.append(PMPlanner.Tools.Render.text({
                    text: PMPlanner.Constants.TEXT_Planned + ': ' + scope._statisticsGroup[PMPlanner.DataSource.FutureJobs_Planned]
                }));
            }
        });
    },
    /**
     * Render custom element to display the count of future works in accordance with the maximum amount of work
     * @param cssLine - style line type statistics
     * @param cssBlock - style type statistics
     * @param field - status works
     * @returns {*}
     * @private
     */
    _renderStatistics: function (cssLine, cssBlock, field) {
        var statistics = PMPlanner.Tools.Render.div({
            cssClass: cssLine,
            cssFields: {
                height: (PMPlanner.Calendar.CELL_HEIGHT - 5) / 2 + 'px'
            }
        });
        var count = parseInt(this._statisticsGroup[field] / this._maxCountFutureWork * 100 ) / 5;
        if (count == 0 && this._statisticsGroup[field] >= 1)
            count = 1;
        var blocks = PMPlanner.Tools.Render.div();
        for (var i = 0; i < count; i++) {
            var block = this._renderBlock(cssBlock);
            blocks.append(block);
        }
        statistics.append(blocks);
        return statistics;
    },
    /**
     * Render block of data for future weeks
     * @param cssClass - style type statistics
     * @returns {*}
     * @private
     */
    _renderBlock: function (cssClass) {
        var block = PMPlanner.Tools.Render.div({
            cssFields: {
                float: 'left',
                width: '5%'
            }
        });
        block.append(PMPlanner.Tools.Render.div({
            cssClass: cssClass,
            cssFields: {
                height: (PMPlanner.Calendar.CELL_HEIGHT - 5) / 2 - 2 + 'px'
            }
        }));
        block.append(PMPlanner.Tools.Render.div({
            cssClass: 'groupStatistics_gap_block',
            cssFields: {
                height: (PMPlanner.Calendar.CELL_HEIGHT - 5) / 2 + 'px'
            }
        }));

        return block;
    }
});

/**
 * Contains a method for displaying total statistics and update it when move work
 */
PMPlanner.GroupStatistics = Base.extend({

    /**
     * Option for cell
     */
    _devExpressOption: null,
    /**
     * Data source the data from the database. Allows updating, maintaining and editing local data.
     */
    _viewDataSource: null,
    /**
     * eek number in year
     */
    _week: null,
    /**
     * The data from the database to display statistics
     */
    _statistics: null,
    /**
     * Constructor
     * @param devExpressOption - option for cell
     * @param option - option for view
     */
    constructor: function (devExpressOption, option) {
        this._devExpressOption = devExpressOption;
        this._viewDataSource = option.view.getDataSource();
        this._week = option.week;
        this._statistics = this._viewDataSource.getWeekStatistics(this._devExpressOption.data.key, this._week);
        this._render();
    },
    /**
     * To update the statistics and redraws the blocks
     */
    update: function () {
        var cell = this._devExpressOption.cellElement;
        var objDiv = cell.find('div');
        for (var i = 0; i < objDiv.length; i++) {
            objDiv[i].remove();
        }
        this._statistics = this._viewDataSource.getWeekStatistics(this._devExpressOption.data.key, this._week);
        this._render();
    },
    /**
     * Render statistics for week
     * @private
     */
    _render: function () {
        var cell = this._devExpressOption.cellElement;
        cell.css('padding', '0');
        cell.on('mousemove', function() {
            var headerTitle = cell.children();
            headerTitle.attr('title', ' ');
        });

        if (this._hasAddCompletedBlock()) {
            cell.append(this._renderCompletedData());
        }

        if (this._hasAddFutureBlock()) {
            cell.append(this._renderFutureBlock(!this._hasAddPastBlocks()));
        }

        if (this._hasAddMissedBlock()) {
            cell.append(this._renderMissedData());
        }

    },
    /**
     * Render statistics for completed works
     * @returns {*}
     * @private
     */
    _renderCompletedData: function () {
        var optionBlock = {
            cssClass: 'groupPastWeek_completed'
        };

        var completedBlock = PMPlanner.Tools.Render.div(optionBlock);
        completedBlock.append(PMPlanner.Tools.Render.img({
            image: PMPlanner.Constants.IMG_COMPLETED,
            cssClass: 'groupPastWeek_completed_imgCheck'
        }));
        var value = this._statistics[PMPlanner.DataSource.PastJobs_CompletedOnTime] + this._statistics[PMPlanner.DataSource.PastJobs_CompletedLate];
        var config = {
            text: value,
            cssClass: 'groupPastWeek_completed_text'
        };
        if (value > 99) {
            config['cssFields'] = {
                'font-size': '9px',
                'padding-top': '1px'
            };
        }
        completedBlock.append(PMPlanner.Tools.Render.text(config));

        return completedBlock;
    },
    /**
     * Render statistics for missed works
     * @returns {*}
     * @private
     */
    _renderMissedData: function () {
        var missedBlock = PMPlanner.Tools.Render.div({
            cssClass: 'groupPastWeek_missed'
        });
        missedBlock.append(PMPlanner.Tools.Render.div({
            cssClass: 'groupPastWeek_missed_triangle'
        }));
        var value = this._statistics[PMPlanner.DataSource.PastJobs_Missed];
        var config = {
            text: this._statistics[PMPlanner.DataSource.PastJobs_Missed],
            cssClass: 'groupPastWeek_missed_text'
        };
        if (value > 99) {
            config['cssFields'] = {
                'font-size': '9px',
                'padding-top': '2.5px'
            };
        }
        missedBlock.append(PMPlanner.Tools.Render.text(config));

        return missedBlock;
    },
    /**
     * Render statistics for future works
     * @param isFutureWeek
     * @returns {string}
     * @private
     */
    _renderFutureBlock: function (isFutureWeek) {
        var total = this._statistics[PMPlanner.DataSource.FutureJobs_Generated] + this._statistics[PMPlanner.DataSource.FutureJobs_Planned];

        var futureBlock = '';
        if (isFutureWeek) {
            futureBlock = PMPlanner.Tools.Render.div({
                cssFields: {
                    'margin-top': (PMPlanner.Calendar.CELL_HEIGHT / 2 - 4) + 'px' ,
                    padding: 0
                }
            });
            futureBlock.append(PMPlanner.Tools.Render.div({
                cssClass: 'groupFutureWeek_square',
                cssFields: {
                    float: 'left',
                    'margin-left': (PMPlanner.Calendar.CELL_WIDTH / 2 - 15) + 'px'

                }
            }));
            futureBlock.append(PMPlanner.Tools.Render.text({
                text: total,
                cssClass: 'groupFutureWeek_text',
                cssFields: {
                    'margin-left': (PMPlanner.Calendar.CELL_WIDTH / 2 + 5) + 'px'
                }
            }));


        } else {
            futureBlock = PMPlanner.Tools.Render.div({
                cssFields: {
                    'margin-top': '20px' ,
                    padding: 0
                }
            });
            futureBlock.append(PMPlanner.Tools.Render.div({
                cssClass: 'groupFutureWeek_square',
                cssFields: {
                    float: 'left',
                    'margin-left': '3px'
                }
            }));
            futureBlock.append(PMPlanner.Tools.Render.text({
                text: total,
                cssClass: 'groupFutureWeek_text',
                cssFields: {
                    'margin-left': '19px',
                    'margin-bottom': '-40px'
                }
            }));
        }

        return futureBlock;
    },
    /**
     * Has need to add a block for completed works
     * @returns {boolean}
     * @private
     */
    _hasAddCompletedBlock: function () {
        var result = false;
        if (this._statistics[PMPlanner.DataSource.PastJobs_CompletedOnTime] != 0 || this._statistics[PMPlanner.DataSource.PastJobs_CompletedLate] != 0) {
            result = true;
        }
        return result;
    },
    /**
     * Has need to add a block for missed works
     * @returns {boolean}
     * @private
     */
    _hasAddMissedBlock: function () {
        var result = false;
        if (this._statistics[PMPlanner.DataSource.PastJobs_Missed] != 0) {
            result = true;
        }
        return result;
    },
    /**
     * Has need to add a block for future works
     * @returns {boolean}
     * @private
     */
    _hasAddFutureBlock: function () {
        var result = false;
        if (this._statistics[PMPlanner.DataSource.FutureJobs_Generated] != 0 || this._statistics[PMPlanner.DataSource.FutureJobs_Planned] != 0) {
            result = true;
        }
        return result;
    },
    /**
     * Has need to add a block for past works
     * @returns {boolean}
     * @private
     */
    _hasAddPastBlocks: function () {
        var result = false;
        if (this._hasAddCompletedBlock() || this._hasAddMissedBlock()) {
            result = true;
        }
        return result;
    }
});

