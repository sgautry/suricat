
PMPlanner.Tools = {
    Render: {
        text: function (option) {
            var text = jQuery('<div>'+ option.text +'</div>');
            if (option.id)
                text.attr('id', option.id);
            if (option.cssClass)
                text.addClass(option.cssClass);
            if (option.cssFields)
                text.css(option.cssFields);

            return text;
        },
        div: function (option) {
            var div = jQuery('<div></div>');
            if(!option)
                return div;
            if (option.id)
                div.attr('id', option.id);
            if (option.cssClass)
                div.addClass(option.cssClass);
            if (option.cssFields)
                div.css(option.cssFields);

            return div;
        },
        img: function (option) {
            var img = jQuery('<img src="' + option.image + '"/>');
            if (option.id)
                img.attr('id', option.id);
            if (option.cssClass)
                img.addClass(option.cssClass);
            if (option.onClick)
                img.on('dxclick', function () {
                    option.onClick.call(option.scope);
                });
            if (option.onMove) {
                img.on('mousemove', function () {
                    option.onMove.call(option.scope);
                });
            }
            if (option.onOver) {
                img.on('mouseover', function () {
                    option.onOver.call(option.scope);
                });
            }
            if (option.onOut) {
                img.on('mouseout', function () {
                    option.onOut.call(option.scope);
                });
            }

            if (option.cssFields)
                img.css(option.cssFields);

            return img;
        },
    },

    highlightCurrentWeek: function (option, date) {
        var currentDate = new Date();
        var week = currentDate.getWeek(),
            year = currentDate.getFullYear();

        var columnWeek = date.getWeek();
        var columnYear = date.getFullYear();

        if (columnYear == year && columnWeek == week) {
            option.cellElement.css({'border-left': '3px solid #000'});
        }
       
    },
   
    formatDate: function (date) {
        var dd = date.getDate();
        if (dd < 10)
            dd = '0' + dd;

        var mm = date.getMonth() + 1;
        if (mm < 10)
            mm = '0' + mm;

        var yy = date.getFullYear();

        return yy + '-' + mm + '-' + dd;
    },

    getWork: function (works, weekDay) {
        var result = null;
        for (var i = 0; i < works.length; i++) {
            var work = works[i];
            if (work.date.format('N') == weekDay) {
                result = work;
                break;
            }
        }
        return result;
    },

    createLoadPanel: function (view) {
        view.getDOMGrid().append(jQuery('<div id="plannedLoadingPanel"></div>'));
        jQuery("#plannedLoadingPanel").dxLoadPanel({
            message: PMPlanner.Constants.TEXT_Loading,
            visible: false
        });
        return jQuery("#plannedLoadingPanel").dxLoadPanel("instance");
    },

    removeLoadPanel: function () {
        jQuery("#plannedLoadingPanel").remove();
    },

    createCorrectID: function (id) {
        var result = id.replace('.','_');
        result.replace(' ','_');
        result.replace(':','_');
        return result;
    },
    
    /**
     * Check compliance license
     */
    hasComplianceLicense: function (id) {
        var hasComplianceLicense = false;
        AdminService.getProgramLicense({
            async: false,
            callback: function(licenseDTO) {
                for (var i = 0; i < licenseDTO.licenses.length; i++) {
                    var license = licenseDTO.licenses[i];
                    if ("AbRiskCompliance" === license.id) {
                        hasComplianceLicense = license.enabled;
                        break;
                    }
                }
            },
            errorHandler: function(m, e) {
                View.showException(e);
            }
        });

        return hasComplianceLicense;
    },

    formattedNumeric: function (value) {
        return View.dataSources.get('costLaborDS').formatValue('pmsd.vf_est_labor_hours', value, true);
    }

};