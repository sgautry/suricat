/*
 * Controller for the test view for the Bali6 advanced grid control.
 * It shows how to initialize the grid control.
 */
View.createController('pmPlannerController', {

    afterViewLoad: function() {
        jQuery.extend(PMPlanner.Constants, {
            TEXT_EstimatedLaborHoursZERO: getMessage('EstimatedLaborHours') + ': 0.00',
            TEXT_EstimatedLaborHours: getMessage('EstimatedLaborHours'),
            TEXT_EstimatedCost: getMessage('EstimatedCost'),
            TEXT_Cost: getMessage('Cost'),
            TEXT_TotalCosts: getMessage('TotalCosts'),
            TEXT_LaborHours: getMessage('LaborHours'),
            TEXT_TotalLaborHours: getMessage('TotalLaborHours'),
            TEXT_MENU_SelectColumns: getMessage('SelectFields'),
            TEXT_MENU_GroupBy: getMessage('SelectGroup'),
            TEXT_MENU_PastPMScheduleDates: getMessage('PastPMScheduleDates'),
            TEXT_MENU_PastPMScheduleDates_month: getMessage('Month'),
            TEXT_MENU_SelectWeek: getMessage('SelectWeek'),
            TEXT_MENU_SelectWeek_weeks: getMessage('Weeks'),
            TEXT_MENU_GROUPBY_EquipmentStandard: getMessage('EquipmentStandard'),
            TEXT_MENU_GROUPBY_Trade: getMessage('Trade'),
            TEXT_MENU_GROUPBY_Building: getMessage('Building'),
            TEXT_MENU_GROUPBY_Site: getMessage('Site'),
            TEXT_MENU_GROUPBY_WorkTeam: getMessage('WorkTeam'),
            TEXT_MENU_GROUPBY_ServiceContract: getMessage('ServiceContract'),
            TEXT_MENU_GROUPBY_ServiceContractProvider: getMessage('ServiceContractProvider'),
            TEXT_MENU_GROUPBY_PMProcedure: getMessage('PMProcedure'),
            TEXT_MENU_GROUPBY_None: getMessage('None'),
            TEXT_MENU_GROUPBY_EquipmentSystem: getMessage('EquipmentSystem'),
            TEXT_CurrencySymbol: View.user.userCurrency.symbol,
            TEXT_Calendar: getMessage('Calendar'),
            WEEK_DAY: [getMessage('Sunday'),
                getMessage('Monday'),
                getMessage('Tuesday'),
                getMessage('Wednesday'),
                getMessage('Thursday'),
                getMessage('Friday'),
                getMessage('Saturday')
            ],
            TEXT_N_A: getMessage('NA'),

            TEXT_On_Time: getMessage('OnTime'),
            TEXT_CompletedOnTime: getMessage('CompletedOnTime'),
            TEXT_CompletedLate: getMessage('CompletedLate'),
            TEXT_Missed: getMessage('Missed'),
            TEXT_Scheduled: getMessage('Scheduled'),
            TEXT_Generated: getMessage('Generated'),
            TEXT_Planned: getMessage('Planned'),

            TEXT_Multiple: getMessage('Multiple'),
            TEXT_Manual: getMessage('Manual'),
            TEXT_Meter: getMessage('Meter'),
            TEXT_Every: getMessage('Every'),
            TEXT_Days: getMessage('Days'),
            TEXT_Weeks: getMessage('Weeks'),
            TEXT_Months: getMessage('Months'),
            TEXT_Quarters: getMessage('Quarters'),
            TEXT_Years: getMessage('Years'),
            TEXT_Miles: getMessage('Miles'),
            TEXT_Hours: getMessage('Hours'),

            TEXT_EQStandardColumn: getMessage('EQStandardColumn'),
            TEXT_PMScheduleCodeColumn: getMessage('PMScheduleCodeColumn'),
            TEXT_EquipmentCodeColumn: getMessage('EquipmentCodeColumn'),
            TEXT_SiteCodeColumn: getMessage('SiteCodeColumn'),
            TEXT_LocationColumn: getMessage('LocationColumn'),
            TEXT_IntervalColumn: getMessage('IntervalColumn'),
            TEXT_PMProcedureColumn: getMessage('PMProcedureColumn'),
            TEXT_PMScheduleGroupColumn: getMessage('PMScheduleGroupColumn'),
            TEXT_DateOfLastPMColumn: getMessage('DateOfLastPMColumn'),
            TEXT_DateForFirstPMColumn: getMessage('DateForFirstPMColumn'),
            TEXT_Loading: getMessage('Loading'),
            TEXT_MonthU: getMessage('MonthU'),
            TEXT_WeekU: getMessage('WeekU'),
            TEXT_Disable: getMessage('Disable')
        });
        var controller = this;
        var columnDefinitions = [ {
            id: 'edit',
            icon: 'edit',
            width: 50,
            listener: function(row, cell) {
                controller.trigger('app:bldgops:pmPlanner:editPmSchedule', row.getRecord());
            }
        }, {
            id: 'eq_std',
            fieldName: 'eq.eq_std',
            title: PMPlanner.Constants.TEXT_EQStandardColumn,
            visible: true,
            type: 'string',
            groupBy: true,
            minWidth: 100

        }, {
            id: 'pms_id',
            fieldName: 'pms.pms_id',
            title: PMPlanner.Constants.TEXT_PMScheduleCodeColumn,
            visible: false,
            sortOrder: 'asc',
            type: 'string',
            width: 50

        }, {
            id: 'eq_id',
            fieldName: 'pms.eq_id',
            title: PMPlanner.Constants.TEXT_EquipmentCodeColumn,
            type: 'string',
            //groupBy: true,
            minWidth: 100
        }, {
            id: 'site_id',
            fieldName: 'pms.site_id',
            title: PMPlanner.Constants.TEXT_SiteCodeColumn,
            type: 'string',
            minWidth: 100
        }, {
            id: 'location',
            fieldName: {
                name: 'vf_location',
                baseField: [ 'pms.bl_id', 'pms.fl_id', 'pms.rm_id' ],
                displayFormat: '{0}-{1}-{2}'
            },
            title: PMPlanner.Constants.TEXT_LocationColumn,
            type: 'string',
            minWidth: 100
        }, {
            id: 'interval',
            fieldName: {
                name: 'vf_interval',
                displayFormat: function (record) {
                    var result = '';
                    var interval_type = record['pms.interval_type'];
                    var index = parseInt(record['pms.interval_freq']);
                    switch (interval_type) {
                        case 'r':
                            var isMultiple = false;
                            for(var i=1;i<=4;i++){
                                if (record['pms.int'+ i +'_recurrence'] != '') {
                                    if(result == ''){
                                        var recurringPatternObj = new Ab.recurring.RecurringPattern({
                                            xmlPattern: record['pms.int' + index + '_recurrence']
                                        });
                                        result = recurringPatternObj.getDescription();
                                    }else{
                                        isMultiple = true;
                                        break;
                                    }
                                }
                            }
                            
                            if(isMultiple){
                                result =PMPlanner.Constants.TEXT_Multiple;
                            }else{
                                if(result == ''){
                                    result = PMPlanner.Constants.TEXT_MENU_GROUPBY_None;
                                }
                            }
                            break;
                        case 'a':
                            result = PMPlanner.Constants.TEXT_Manual;
                            break;
                        case 'e':
                            result = PMPlanner.Constants.TEXT_Meter + ': ' + parseInt(record['pms.interval_'+ index]);
                            break;
                        case 'd':
                            result = PMPlanner.Constants.TEXT_Every + ' ' + parseInt(record['pms.interval_'+ index]) + ' ' + PMPlanner.Constants.TEXT_Days;
                            break;
                        case 'ww':
                            result = PMPlanner.Constants.TEXT_Every + ' ' + parseInt(record['pms.interval_'+ index]) + ' ' + PMPlanner.Constants.TEXT_Weeks;
                            break;
                        case 'm':
                            result = PMPlanner.Constants.TEXT_Every + ' ' + parseInt(record['pms.interval_'+ index]) + ' ' + PMPlanner.Constants.TEXT_Months;
                            break;
                        case 'q':
                            result = PMPlanner.Constants.TEXT_Every + ' ' + parseInt(record['pms.interval_'+ index]) + ' ' + PMPlanner.Constants.TEXT_Quarters;
                            break;
                        case 'yyyy':
                            result = PMPlanner.Constants.TEXT_Every + ' ' + parseInt(record['pms.interval_'+ index]) + ' ' + PMPlanner.Constants.TEXT_Years;
                            break;
                        case 'i':
                            result = PMPlanner.Constants.TEXT_Every + ' ' + parseInt(record['pms.interval_'+ index]) + ' ' + PMPlanner.Constants.TEXT_Miles;
                            break;
                        case 'h':
                            result = PMPlanner.Constants.TEXT_Every + ' ' + parseInt(record['pms.interval_'+ index]) + ' ' + PMPlanner.Constants.TEXT_Hours;
                            break;
                    }
                    return result;
                }
            },
            title: PMPlanner.Constants.TEXT_IntervalColumn,
            type: 'string',
            width: 150
        }, {
            id: 'pmp_id',
            fieldName: 'pms.pmp_id',
            title: PMPlanner.Constants.TEXT_PMProcedureColumn,
            type: 'string',
            minWidth: 150
        }, {
            id: 'pm_group',
            fieldName: 'pms.pm_group',
            title: PMPlanner.Constants.TEXT_PMScheduleGroupColumn,
            visible: false,
            type: 'string',
            minWidth: 100
        }, {
            id: 'date_last_completed',
            fieldName: 'pms.date_last_completed',
            title: PMPlanner.Constants.TEXT_DateOfLastPMColumn,
            visible: false,
            type: 'string',
            minWidth: 100
        }, {
            id: 'date_first_todo',
            fieldName: 'pms.date_first_todo',
            title: PMPlanner.Constants.TEXT_DateForFirstPMColumn,
            visible: false,
            type: 'string',
            minWidth: 100
        }];

        if (!this.gridPanel.defaultDetermineHeight) {
            this.gridPanel.defaultDetermineHeight = this.gridPanel.determineHeight;
            this.gridPanel.determineHeight = function (el) {
                return controller.gridPanel.defaultDetermineHeight(el) - 30;
            }
        }

        this._scheduler = View.createControl({
            control: 'PMPlannerControl',
            panel: 'gridPanel',
            container: 'schedulerContainer',
            dataSource: 'dataDS',
            columns: columnDefinitions,
            events: {
                onEditPmScheduleDate: function(record) {
                    controller.trigger('app:bldgops:pmPlanner:editPmScheduleDate', record);
                },
                onEditPastPmScheduleDate: function(records) {
                    controller.trigger('app:bldgops:pmPlanner:editPastPmScheduleDate', records);
                },
                onEditMultiplePmSchedules: function (fieldName, fieldValue) {
                    controller.trigger('app:bldgops:pmPlanner:editMultiplePmSchedules', fieldName, fieldValue);
                },
                onAfterPmScheduleDateChanged: function(record) {
                    controller.trigger('app:bldgops:pmPlanner:afterPmScheduleDateChanged', record);
                },                
                onAfterCostLaborDSLoad: function() {
                    controller.trigger('app:bldgops:pmPlanner:afterCostLaborDSLoad');
                },
                onExportXLS: function() {
                    controller.trigger('app:bldgops:pmPlanner:exportXLS');
                }
            }
        });

        this.on('app:bldgops:pmPlanner:showPmSchedules', function(option) {
            controller._scheduler.refresh(option);
        });
        this.on('app:bldgops:pmPlanner:editPmSchedule', this.editPmSchedule);
        this.on('app:bldgops:pmPlanner:editPmScheduleDate', this.editPmScheduleDate);
        this.on('app:bldgops:pmPlanner:editPastPmScheduleDate', this.editPastPmScheduleDate);
        this.on('app:bldgops:pmPlanner:afterPmScheduleDateChanged', this.afterPmScheduleDateChanged);
        this.on('app:bldgops:pmPlanner:showCosts', this.showCosts);
        this.on('app:bldgops:pmPlanner:showLabor', this.showLabor);
        this.on('app:bldgops:pmPlanner:filter', this.filterWeeklyPlannerTab);
        this.on('app:bldgops:pmPlanner:editMultiplePmSchedules', this.editScheduleByGroup);
        this.on('app:bldgops:pmPlanner:exportXLS', this.exportXLS);
        this.on('app:bldgops:pmPlanner:updateSelectedPmScheduledDate', this.updateSelectedPmScheduledDate);

        this.plannerTabs.hideTab('financialImpactTab');
        this.plannerTabs.hideTab('laborAnalysisTab');
        
        //APP-1559 - hide PM Planner: hide Weekly Planner tab header if it is the only visible tab
        jQuery(".x-tab-panel-header").hide();

        this.removeScroller.defer(500, this);
    },

    /**
     * Removes scroll bars from the center layout region.
     */
    removeScroller: function() {
        var regionPanel = View.getLayoutManager('mainLayout').getRegionPanel('center');
        if (regionPanel.scroller) {
            regionPanel.scroller.detach();
            regionPanel.scroller = null;

            jQuery('#gridRegion .jssb-x').remove();
            jQuery('#gridRegion .jssb-y').remove();
            jQuery('#mainLayout_center_div').removeClass('jssb-content');

            this.gridPanel.isScrollInLayout = function() {
                return false;
            }
        }
    },
    
    /**
     * Filter Weekly Planner tab.
     */
    filterWeeklyPlannerTab: function() {        
        this.trigger('app:bldgops:pmPlanner:showPmSchedules', {
            restriction: new Ab.view.Restriction(),
            groupBy: ''
        });
    },

    /**
     * Open dialog to edit PM Schedule.
     */
    editPmSchedule: function(record) {
        var restriction = new Ab.view.Restriction();
        restriction.addClause('pms.pms_id', record.getValue('pms.pms_id'));
        View.resFromPmPlanner = restriction;
        View.pmpType = record.getValue('pms.eq_id') != '' ? 'EQ' : 'HK';
        var dialogConfig = {
            width: 1000,
            height: 600
        };
        View.openDialog('ab-pm-planner-edit-sched.axvw', restriction, false, dialogConfig);
    },

    /**
     * Open dialog to edit PM Schedule date.
     */
    editPmScheduleDate: function(record) {
        var restriction = new Ab.view.Restriction();
        restriction.addClause('pmsd.pms_id', record.getValue('pmsd.pms_id'));
        restriction.addClause('pmsd.date_todo', PMPlanner.Tools.formatDate(record.getValue('pmsd.date_todo')));
        View.openDialog('ab-pm-planner-sched-date.axvw', restriction);
    },

    /**
     * Open dialog to edit PM Schedule date.
     */
    editPastPmScheduleDate: function(records) {
        if (records.length > 0) {
            var restriction = new Ab.view.Restriction();
            restriction.addClause('pmsd.pms_id', records[0].getValue('pmsd.pms_id'));
            restriction.addClause('pmsd.date_todo', PMPlanner.Tools.formatDate(records[0].getValue('pmsd.date_todo')), '=', ')AND(');

            for (var i = 1; i < records.length; i++) {
                var record = records[i];
                restriction.addClause('pmsd.date_todo', PMPlanner.Tools.formatDate(record.getValue('pmsd.date_todo')), '=', 'OR');
            }

            if (records.length == 1) {
                View.openDialog('ab-pm-planner-sched-date.axvw', restriction);
            } else {
                View.resFromPmPlanner = restriction;
                var dialogConfig = {
                    width: 1000,
                    height: 600
                };
                View.openDialog('ab-pm-planner-sched-dates.axvw', restriction, false, dialogConfig);
            }
        }
    },
    
    /**
     * Save record after calendar edit the pmsd record.
     */
    afterPmScheduleDateChanged: function(record) {
        record.isNew = false;
        
        //APP-1526 - re-set the date field to make sure the WFR can parse the correct value
        this.processDateFieldValues(record);
        
        //save the changes
        this.worksDS.saveRecord(record);
        
        //trigger event to update cost and labor record
        this.trigger('app:bldgops:pmPlanner:updateCostAndLaborRecord', record);
        
        //APP-1481 - update the oldvalues after save to make sure can change date by moving arrow  multiple times without refresh
        record.oldValues['pmsd.date_todo'] = record.values['pmsd.date_todo'];
    },
    
    /**
     * update selected PM Schedule date.
     */
    updateSelectedPmScheduledDate: function(record) {
        this._scheduler.updateSelectedPmScheduledDate(record.getValue("pmsd.pms_id"), record.getValue("pmsd.date_todo"));
        //trigger event to update cost and labor record
        this.trigger('app:bldgops:pmPlanner:updateCostAndLaborRecord', record);
    },
    
    /**
     * Process the date field values.
     */
    processDateFieldValues: function(record) {
        var dateFileds = ['pmsd.date_todo','pmsd.date_latest','pmsd.date_earliest','pmsd.date_completed','pmsd.date_orig_todo'];
        for (var i = 0; i < dateFileds.length; i++) {
            var fieldName = dateFileds[i];
            this.processDateFieldValue(record, fieldName);
        }
    },
    
    /**
     * Process the date field value.
     */
    processDateFieldValue: function(record, fieldName) {
        if(record.values[fieldName]){
            record.values[fieldName] = new Date(record.values[fieldName].getTime());
        }
        if(record.oldValues[fieldName]){
            record.oldValues[fieldName] = new Date(record.oldValues[fieldName].getTime());
        }
    },

    /**
     * Show Costs.
     */
    showCosts: function(showCosts) {
        this._scheduler.showCosts(showCosts);        
        this.trigger('app:bldgops:pmPlanner:showPmSchedules', {
            restriction: new Ab.view.Restriction(),
            groupBy: ''
        });

        if(!showCosts){
            this.plannerTabs.selectTab('weeklyPlannerTab');
        }
        this.plannerTabs.showTab('financialImpactTab', showCosts);
        //APP-1559 - hide PM Planner: hide Weekly Planner tab header if it is the only visible tab
        if(this.plannerTabs.tabs[1].forcedHidden && this.plannerTabs.tabs[2].forcedHidden){
            jQuery(".x-tab-panel-header").hide();
        }else{
            jQuery(".x-tab-panel-header").show();
        }
    },

    /**
     * Show Labor.
     */
    showLabor: function(showLabor) {
        jQuery('#analyseLaborShortages').show();
        this._scheduler.showLabor(showLabor);        
        this.trigger('app:bldgops:pmPlanner:showPmSchedules', {
            restriction: new Ab.view.Restriction(),
            groupBy: ''
        });

        if(!showLabor){
            jQuery('#analyseLaborShortages').hide();
            this.plannerTabs.selectTab('weeklyPlannerTab');
        }
        this.plannerTabs.showTab('laborAnalysisTab', showLabor);
        //APP-1559 - hide PM Planner: hide Weekly Planner tab header if it is the only visible tab
        if(this.plannerTabs.tabs[1].forcedHidden && this.plannerTabs.tabs[2].forcedHidden){
            jQuery(".x-tab-panel-header").hide();
        }else{
            jQuery(".x-tab-panel-header").show();
        }
    },
    
    /**
     * Edit Schedule by Group.
     */
    editScheduleByGroup: function(groupName, groupValue) {
        var restriction = new Ab.view.Restriction();
        if(groupName != 'pms.none'){
            restriction.addClause(groupName, groupValue, '=');
        }
        
        var ds = this.dataDS;
        if(groupName =='pms.tr_id'){
            ds = this.groupByTradeDataDS;
        }

        var records = ds.getDataSet(restriction, {}).records;
        if(records.length>0){
            var resFromPmPlannerByGroup = [];
            for(var i = 0; i< records.length; i++){
                resFromPmPlannerByGroup.push(records[i].values)
            }
            
            View.calendarFromDate =  this._scheduler.getCalendarFromDate();
            View.resFromPmPlannerByGroup =  resFromPmPlannerByGroup;
            var dialogConfig = {
                width: 1000,
                height: 600
            };
            View.openDialog('ab-pm-planner-edit-sched-by-group.axvw', null, false, dialogConfig);
        }
       
    },
    
    /**
     * Export 52 week XLS.
     */
    exportXLS: function() {
        var scope = this;
        var command = new Ab.command.exportPanel({});
        
        var createExportingPanel =  function() {
            jQuery('#schedulerContainer').append(jQuery('<div id="plannedExportingPanel"></div>'));
            jQuery("#plannedExportingPanel").dxLoadPanel({
                message: View.getLocalizedString(command.z_PROGRESS_MESSAGE) + '...',
                visible: true
            });
            return jQuery("#plannedExportingPanel").dxLoadPanel("instance");
        };

        var removeExportingPanel =  function() {
            jQuery("#plannedExportingPanel").remove();
        };
        
        createExportingPanel();
        setTimeout(function() {
           
            var jobId = null;
            
            var calendarFromDate = getIsoFormatDate(scope._scheduler.getCalendarFromDate());
            var visibleColumns = scope._scheduler.getVisibleColumns();
            var visibleFieldDefs = [];
            for(var i=0;i<visibleColumns.length;i++){
                if(visibleColumns[i].id == 'location' || visibleColumns[i].id == 'interval'){
                    visibleFieldDefs.push({id:'pms.' + visibleColumns[i].fieldName.name, title:visibleColumns[i].title, isNumeric: false});
                }else{
                    if(visibleColumns[i].fieldName == 'eq.eq_std'){
                        visibleFieldDefs.push({id:'pms.eq_std', title:visibleColumns[i].title, isNumeric: false});
                    }else{
                        visibleFieldDefs.push({id:visibleColumns[i].fieldName, title:visibleColumns[i].title, isNumeric: false});
                    }
                }
            }
            var parameters = scope.dataDS.parameters;
            parameters.calendarFromDate = calendarFromDate;
            parameters.viewName = 'ab-pm-planner.axvw';
            parameters.pmsDataSourceID = 'exportDataDS';
            parameters.pmsdDataSourceID = 'costLaborDS';
            parameters.printableRestriction = pmPlannerFilterController.printableRestriction;
            
            var groupFieldName = scope._scheduler.getGroupByField().fieldName;
            if(groupFieldName == 'pms.tr_id'){
                groupFieldName = 'pmp.tr_id';
                parameters.pmsDataSourceID = 'exportDataByTradeDS';
            }
            var groupByField = {id:groupFieldName,title: scope.dataDS.fieldDefs.get(groupFieldName).title, isNumeric: false};
            if(groupFieldName == 'pms.none'){
                groupByField.id = "''";
            }
            
            // start job to generate XLS report
            try {
                result = Workflow.callMethod('AbBldgOpsPM-PmEventHandler-startJobToGenerate52WeekPlan', groupByField , visibleFieldDefs, parameters);
            } catch (e) {
                Workflow.handleError(e);
                removeExportingPanel();
                return;
            }
            if (valueExists(result.jsonExpression) && result.jsonExpression != '') {
                result.data = eval('(' + result.jsonExpression + ')');
                jobId = result.data.jobId;
            }

            // display report
            if (jobId != null) {
                command.displayReport(jobId);
            }
            
            removeExportingPanel();
            
        }, 5);
    }
    
});