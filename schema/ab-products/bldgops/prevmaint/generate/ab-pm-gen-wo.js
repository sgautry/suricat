View.createController('abPmGenWoController',{
    afterInitialDataFetch: function(){
        //APP-1387:Check database schema for Bali6.
        if(!Workflow.callMethod('AbBldgOpsOnDemandWork-WorkRequestService-checkSchemaExisting','pms', 'days_late').value){
            jQuery('body').hide();
            alert(getMessage('udpateSchemaMessage'));
            return;
        }
    }
});