
/**
 * Check if custodiantype is defined in schema.
 * @type {boolean}
 */
var isCustodianSchemaDefined = false;

/**
 * System analysis controller.
 */
View.createController('eqSysAnalysisController', {
    // on tree selected equipment
    selectedEquipmentId: null,
    // parameter passed to reports
    selectedSystemName: null,

    afterViewLoad: function() {
        // set tree level node restriction
        this.updateRestrictionLevel();
        // config display panels
        this.configDisplayPanels();

        this.eqSysInventoryTreePanel.actions.get('display').show(false);
        this.eqSysInventoryTreePanel.actions.get('assign').show(false);

    },

    afterInitialDataFetch: function() {
        var parentAssignPMController = View.getOpenerView().controllers.get('assignPM');
        parentAssignPMController.systemTabController = this;
        this.onFilter(parentAssignPMController.genRestrictionFromFilterForSystemTab());
    },

    updateRestrictionLevel: function() {
        this.eqSysInventoryTreePanel.updateRestrictionForLevel = updateTreeRestrictionForLevel;
    },
    configDisplayPanels: function() {
        DisplayPanelConfiguration.addPanelDisplayConfig('eqSysInventoryTreePanel', {
            panelA: {
                visible: true,
                panelType: 'dependent'
            },
            panelB: {
                visible: true,
                panelType: 'dependency'
            },
            panelC: {
                visible: false,
                panelType: 'profile'
            }
        });

        View.panels.get('abEqSysInfo_panelA').optionalTreeNodeClickEvents = [ {
            type: 'onClickNode',
            commands: [ {
                'type': 'callFunction',
                'functionName': "View.getOpenerView().controllers.get('eqSysAnalysisController').onClickDependentNode(this.context)"
            } ]
        } ];
        View.panels.get('abEqSysInfo_panelB').optionalTreeNodeClickEvents = [ {
            type: 'onClickNode',
            commands: [ {
                'type': 'callFunction',
                'functionName': "View.getOpenerView().controllers.get('eqSysAnalysisController').onClickDependencyNode(this.context)"
            } ]
        } ];
    },
    onClickNode: function(panelId, eqId, systemName, crtTreeNode) {
        this.selectedEquipmentId = eqId;
        this.selectedSystemName = systemName;
        DisplayPanelConfiguration.displayPanels(panelId, eqId, crtTreeNode);
        this.afterClickEquipmentNode(eqId);
    },

    onClickDependentNode: function(context) {
        var crtTreeNode = View.panels.get('abEqSysInfo_panelA').contentView.panels.get(context.command.parentPanelId).lastNodeClicked;
        var eqId = crtTreeNode.data['eq_system.eq_id_depend'];
        this.afterClickEquipmentNode(eqId);
    },

    onClickDependencyNode: function(context) {
        var crtTreeNode = View.panels.get('abEqSysInfo_panelB').contentView.panels.get(context.command.parentPanelId).lastNodeClicked;
        var eqId = crtTreeNode.data['eq_system.eq_id_depend'];
        this.afterClickEquipmentNode(eqId);
    },

    afterClickEquipmentNode: function(eqId) {
        var restriction = new Ab.view.Restriction();
        restriction.addClause("eq.eq_id", eqId, "=");
        var eqStd = this.equipmentDs.getRecord(restriction).getValue('eq.eq_std');
        View.getOpenerView().controllers.get('assignPM').systemEqId = eqId;
        View.getOpenerView().controllers.get('assignPM').systemEqStd = eqStd;
        View.getOpenerView().controllers.get('assignPM').showPmpAndScheduleByEquipments(eqId + ';', eqStd + ';');
    },

    /**
     * On filter event handler
     * @param restriction restriction object
     */
    onFilter: function(restriction) {
        var filterMasterRestrictionSql = '1=1';
        var filterParameters = getFilterLevelRestriction(restriction, true);

        var parentAssignPMController = View.getOpenerView().controllers.get('assignPM');
        if (parentAssignPMController.getNoProcedureCheckBoxValue() == true) {
            var result = '';
            for (var i = 1; i <= 10; i++) {
                var restrictionSql = ' NOT EXISTS (SELECT 1 FROM pms WHERE pms.eq_id = eq' + i + '.eq_id) ';
                if (valueExistsNotEmpty(restrictionSql)) {
                    result += " (";
                    result += restrictionSql;
                    result += " )";
                    if (i < 10) {
                        result += ' AND ';
                    }
                }
            }

            if (valueExistsNotEmpty(filterParameters)) {
                filterParameters = '(' + filterParameters + ')' + " AND (" + result + ")";
            } else {
                filterParameters = " (" + result + ")";
            }
        }

        if (valueExistsNotEmpty(filterParameters)) {
            var filterRestrictionSql = getMessage('filterRestrictionSql');
            filterMasterRestrictionSql = filterRestrictionSql.replace(/%{filterParameters}/g, filterParameters);
        }
        this.eqSysInventoryTreePanel.addParameter('filterRestriction', filterMasterRestrictionSql);
        this.eqSysInventoryTreePanel.refresh();
    },

    eqSysInventoryTreePanel_afterGeneratingTreeNode: function(node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    eqSysInventoryTreeLvl_2_afterGeneratingTreeNode: function(node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    eqSysInventoryTreeLvl_3_afterGeneratingTreeNode: function(node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    eqSysInventoryTreeLvl_4_afterGeneratingTreeNode: function(node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    eqSysInventoryTreeLvl_5_afterGeneratingTreeNode: function(node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    eqSysInventoryTreeLvl_6_afterGeneratingTreeNode: function(node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    eqSysInventoryTreeLvl_7_afterGeneratingTreeNode: function(node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    eqSysInventoryTreeLvl_8_afterGeneratingTreeNode: function(node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    eqSysInventoryTreeLvl_9_afterGeneratingTreeNode: function(node) {
        View.controllers.get('eqSysAnalysisController').formatLabelNode(node);
    },
    /**
     * Set tree node label and also a node separator. The separator is displayed to drop an element on the same node level.
     */
    formatLabelNode: function(node) {
        var depend = node.data['eq_system.eq_id_depend'];
        var systemLvl = node.data['eq_system.system_level'];
        if(node.data['eq_system.system_level.raw']){
            systemLvl = node.data['eq_system.system_level.raw'];
        }
        var label = node.data['eq_system.system_name'] + ' (' + depend + ')';
        // label += ' ' + node.data['eq_system.sort_order'];
        var nodeLabel = '<div class="nodeLabel">';
        var labelId = 'label' + node.id + '_0';
        var separatorId = 'separator' + node.id + '_0';
        nodeLabel += '<div id="' + labelId + '" class="label color_' + systemLvl + '"><span class="ygtvlabel">' + label + '</span></div>';
        nodeLabel += '<div id="' + separatorId + '" class="separator"></div>';
        nodeLabel += '</div>';
        label = node.label.replace('{label_html}', nodeLabel);
        // set action images
        var hasDrawing = '1' === node.data['eq_system.has_drawing'], has3dView = '1' === node.data['eq_system.has_3dbin'], has360View = '1' === node.data['eq_system.has_360doc'];
        this.formatActions(node, label, hasDrawing, has3dView, has360View);
    },
    formatActions: function(node, label, hasDrawing, has3dView, has360View) {
        var elem = jQuery(label);
        var idsToRemove = [];
        var iconAction = elem.find('#image1');
        if (!hasDrawing && iconAction.length) {
            idsToRemove.push(iconAction.parent().parent().attr('id'));
        }
        iconAction = elem.find('#image2');
        if (!has3dView && iconAction.length) {
            idsToRemove.push(iconAction.parent().parent().attr('id'));
        }
        iconAction = elem.find('#image3');
        if (!has360View && iconAction.length) {
            idsToRemove.push(iconAction.parent().parent().attr('id'));
        }
        var formatLabel = '';
        elem.each(function(index) {
            var el = jQuery(this);
            if (index == 0) {
                el.attr('colspan', idsToRemove.length + 1);
            }
            var label = this.outerHTML;
            for (var i = 0; i < idsToRemove.length; i++) {
                if (idsToRemove[i] === el.attr('id')) {
                    label = ''
                }
            }
            formatLabel += label;
        });
        node.label = formatLabel;
        if (!has360View) {
            node.position = node.position - idsToRemove.length;
        }
    }
});
function onClickNodeHandler(context) {
    var controller = View.controllers.get('eqSysAnalysisController');
    var crtTreeNode = View.panels.get(context.command.parentPanelId).lastNodeClicked;
    var eqId = crtTreeNode.data['eq_system.eq_id_depend'], systemName = crtTreeNode.data['eq_system.system_name'];
    controller.onClickNode(context.command.parentPanelId, eqId, systemName, crtTreeNode);
}
/**
 * Set tree node label.
 * @param node
 */
function afterGeneratingTreeNode(node) {
    if (valueExists(node.data['eq_system.eq_id_depend']) && valueExists(node.data['eq_system.system_level'])) {
        var depend = node.data['eq_system.eq_id_depend'], systemLvl = node.data['eq_system.system_level'];
        var label = node.data['eq_system.system_name'] + ' (' + depend + ')';
        // label += ' ' + node.data['eq_system.sort_order'];
        var nodeLabel = '<div class="nodeLabel">', labelId = 'label' + node.id + '_0';
        nodeLabel += '<div id="' + labelId + '" class="label color_' + systemLvl + '"><span class="ygtvlabel">' + label + '</span></div>';
        nodeLabel += '</div>';
        node.label = nodeLabel;
    }
}
/**
 * Update tree restriction for level and set filter restriction.
 */
function updateTreeRestrictionForLevel(parentNode, level, restriction) {
    if (level > 0) {
        restriction.removeClause('eq_system.auto_number');
        restriction.addClause('eq_system.eq_id_master', parentNode.data['eq_system.eq_id_depend'], '=');
    }
    if (this.parameters.filterRestriction) {
        var filterRestriction = this.parameters.filterRestriction;
        var levelRestriction = '';
        for (var i = (level + 1), length = 10; i < length; i++) {
            levelRestriction += 'eq.level' + i + '=eq_system.eq_id_depend ';
            if (i < (length - 1)) {
                levelRestriction += ' OR ';
            }
        }
        this.parameters.filterRestriction = filterRestriction.replace(/%{levelRestriction}/g, levelRestriction);
    }
}
/**
 * On reports action.
 */
function onClickMenu(menu) {
    if (valueExists(menu.viewName)) {
        var dialogConfig = {
            width: 1024,
            height: 800,
            closeButton: true
        };
        if (valueExists(menu.parameters)) {
            for ( var param in menu.parameters) {
                if (param == 'title') {
                    dialogConfig[param] = getMessage(menu.parameters[param]);
                } else {
                    dialogConfig[param] = menu.parameters[param];
                }
            }
        }
        View.openDialog(menu.viewName, null, false, dialogConfig);
    }
}
/**
 * On reports action with restriction.
 */
function onClickMenuWithRestriction(menu) {
    if (valueExists(menu.viewName)) {
        var dialogConfig = {
            width: 1024,
            height: 800,
            closeButton: true
        };
        if (valueExists(menu.parameters)) {
            var controller = View.controllers.get('eqSysAnalysisController')
            var selectedEquipmentId = controller.selectedEquipmentId, selectedSystemName = controller.selectedSystemName;
            var filterRestriction = getFilterLevelRestriction(controller.filterController.getFilterRestriction(), false);
            for ( var param in menu.parameters) {
                if (param == 'selectedEquipmentId') {
                    dialogConfig[param] = selectedEquipmentId;
                } else if (param == 'selectedSystemName') {
                    dialogConfig[param] = selectedSystemName;
                } else if (param == 'filterRestriction') {
                    dialogConfig[param] = filterRestriction;
                } else if ('title') {
                    dialogConfig[param] = getMessage(menu.parameters[param]);
                } else {
                    dialogConfig[param] = menu.parameters[param];
                }
            }
        }
        View.openDialog(menu.viewName, null, false, dialogConfig);
    }
}
