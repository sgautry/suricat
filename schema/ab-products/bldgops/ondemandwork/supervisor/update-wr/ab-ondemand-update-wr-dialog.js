
var controller = View.createController('abOndemandUpdateWrDialogController', {

	svgControl: null,
	
	afterViewLoad : function() {
		 //get location information from parent controller 
		var c=View.getOpenerView().controllers.items[0];
		var blId = c.locArray[0];
		var flId = c.locArray[1];
		var rmId = c.locArray[2];
		
		this.loadSvg(blId, flId);
		// you can choose how to highlight the asset  	  
		var opts = { cssClass: 'zoomed-asset-red-bordered',		// use the cssClass property to specify a css class
				     zoomFactor: 0,
					 removeStyle: true							// use the removeStyle property to specify whether or not the fill should be removed (for example  cssClass: 'zoomed-asset-bordered'  and removeStyle: false
			   	   };
		
		this.svgControl.getAddOn('AssetLocator').findAssets([blId + ';' + flId + ';' + rmId], opts);
		this.svgControl.getAddOn('AssetLocator').infoBar.show(false);
		this.svg_ctrls.setTitle(getMessage('drawingPanelTitle1'));
	},
	
	 loadSvg: function(blId, flId) {
	    	
		// define parameters to be used by server-side job
		var parameters = new Ab.view.ConfigObject();
		parameters['pkeyValues'] = {'bl_id':blId, 'fl_id':flId};
		parameters['bordersHighlightSelector'] = false; 
		parameters['highlightFilterSelector'] = false;
		parameters['divId'] = "svgDiv";
		parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
									   'AssetLocator': {divId: "svgDiv"}
									 };
		parameters['drawingName'] = this.retrieveDrawingName(blId, flId);
									 
		this.svgControl = new Drawing.DrawingControl("svgDiv", "svg_ctrls", parameters);	
		
		// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
		this.svgControl.load(parameters);	
		
		// resize specified DOM element whenever the panel size changes
		this.svg_ctrls.setContentPanel(Ext.get('svgDiv'));
	},
	
    retrieveDrawingName: function(blId, flId){
    	var drwingDS = Ab.data.createDataSourceForFields({
	        id: 'svgDrawingControl_afmDwgs_Ds',
	        tableNames: ['afm_dwgs'],
	        fieldNames: ['afm_dwgs.dwg_name','afm_dwgs.space_hier_field_values']
	     });
    	 
    	 var dwgName = '';
     	
    	 if(blId && flId){
    		var restriction = new Ab.view.Restriction();
    		restriction.addClause("afm_dwgs.space_hier_field_values", blId + ";" + flId);
        	var records = drwingDS.getRecords(restriction);
        	if(records!=null && records.length > 0 && records[0]!=null){
        		dwgName = records[0].getValue("afm_dwgs.dwg_name");
        	}
	    }
         
        return dwgName;
	}
});

