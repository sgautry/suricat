var treeConsoleRestriction = '';
var controller = View.createController("reviewRedlinesArchitectController",{
	
	labelDataSourceViewName: '',
	highlightDataSourceViewName: '',
	labelDataSourceName:'',
	highlightDataSourceName:'',
	labelsLoaded: false,
	highlightLoaded: false,
	dwgname: '',
	redlines: '',
	position:'',
	applyHLDSWithLabelDS: false,
	consoleRestriction: '',
	
	afterLayout: function(){
		jQuery('#redlineLegendPanel_layoutWrapper').hide();
		jQuery('#planTypeHighlightPanel_layoutWrapper').hide();
		jQuery('#svgDrawingUpload_layoutWrapper').hide();
		
		RedlineSvg.prototype.attachRedlineEvents = function () {
			//overwrite the attachRedlineEvents method to do nothing to make sure the redlines in this view is readOnly
		}
	},
	
	/**
	 * Add afterLoad eventListeners to hidden views for loading the label and highlight datasources
	 */
	afterViewLoad:function(){
		
		 this.treePanel_dwgs.createRestrictionForLevel = function(parentNode, level) {
			 var consolePanel = View.panels.get("consolePanel");
			 var dateRequestedFrom = consolePanel.getFieldElement("afm_redlines.date_created.from").value;
			 var dateRequestedTo = consolePanel.getFieldElement("afm_redlines.date_created.to").value;
			 
			// validate the date range 
			if (valueExistsNotEmpty(dateRequestedFrom) && valueExistsNotEmpty(dateRequestedTo)) {
				// the compareLocalizedDates() function expects formatted (displayed) date values
				if (compareLocalizedDates(dateRequestedTo,dateRequestedFrom)){
					// display the error message defined in AXVW as message element
					alert(getMessage('error_date_range'));
					return;
				}
			}

			 var restriction = new Ab.view.Restriction();
			 if(level == 0){	
				 var dwg_name = consolePanel.getFieldValue('afm_redlines.dwg_name');
				 if(valueExistsNotEmpty(dwg_name)){
					 dwg_name = dwg_name.toUpperCase();
					restriction.addClause('afm_dwgs.dwg_name', "%"+dwg_name+"%", 'LIKE');
				 } else {
					 if(valueExistsNotEmpty(dateRequestedFrom) || valueExistsNotEmpty(dateRequestedTo) || valueExistsNotEmpty(consolePanel.getFieldValue("activity_log_hactivity_log_hactivity_log.site_id"))
						 || valueExistsNotEmpty(consolePanel.getFieldValue("activity_log_hactivity_log.bl_id")) || valueExistsNotEmpty(consolePanel.getFieldValue("activity_log_hactivity_log.fl_id"))
						 || valueExistsNotEmpty(consolePanel.getFieldValue("activity_log_hactivity_log.prob_type")) || valueExistsNotEmpty(consolePanel.getFieldValue("activity_log_hactivity_log.activity_type"))){
						 treeConsoleRestriction = "0=0";
						 if(valueExistsNotEmpty(dateRequestedFrom)){
							 treeConsoleRestriction += " AND afm_redlines.date_created > '" +consolePanel.getFieldValue("afm_redlines.date_created.from")+"'";
							 
						 }
						 if(valueExistsNotEmpty(dateRequestedTo)){
							 treeConsoleRestriction += " AND afm_redlines.date_created < '" +consolePanel.getFieldValue("afm_redlines.date_created.to")+"'";
						 }
						 if(valueExistsNotEmpty(consolePanel.getFieldValue("activity_log_hactivity_log.site_id"))){
							 treeConsoleRestriction += " AND activity_log_hactivity_log.site_id LIKE '%" +consolePanel.getFieldValue("activity_log_hactivity_log.site_id")+"%'";
						 }
						 if(valueExistsNotEmpty(consolePanel.getFieldValue("activity_log_hactivity_log.bl_id"))){
							 treeConsoleRestriction += " AND activity_log_hactivity_log.bl_id LIKE '%" +consolePanel.getFieldValue("activity_log_hactivity_log.bl_id")+"%'";
						 }
						 if(valueExistsNotEmpty(consolePanel.getFieldValue("activity_log_hactivity_log.fl_id"))){
							 treeConsoleRestriction += " AND activity_log_hactivity_log.fl_id LIKE '%"+consolePanel.getFieldValue('activity_log_hactivity_log.fl_id')+"%'";
						 }
						 if(valueExistsNotEmpty(consolePanel.getFieldValue("activity_log_hactivity_log.prob_type"))){
							 treeConsoleRestriction += " AND activity_log_hactivity_log.prob_type LIKE '%" +consolePanel.getFieldValue("activity_log_hactivity_log.prob_type")+"%'";
						 }
						 if(valueExistsNotEmpty(consolePanel.getFieldValue("activity_log_hactivity_log.activity_type"))){
							 treeConsoleRestriction += " AND activity_log_hactivity_log.activity_type LIKE '%" +consolePanel.getFieldValue("activity_log_hactivity_log.activity_type")+"%'";
						 }
						 var dwgDS = View.dataSources.get("reviewRedlinesTree_dwgsDS");
						 var records = dwgDS.getRecords(" EXISTS (SELECT dwg_name FROM afm_redlines WHERE afm_dwgs.dwg_name = afm_redlines.dwg_name AND " +
						 		" afm_redlines.activity_log_id IN (SELECT activity_log_id FROM activity_log_hactivity_log WHERE "+treeConsoleRestriction+"))");
						 if(records.length == 0){
							 restriction.addClause('afm_dwgs.dwg_name','NULL','=');
						 } else {
							 for(var i=0;i<records.length;i++){
								 restriction.addClause('afm_dwgs.dwg_name',records[i].values['afm_dwgs.dwg_name'].toUpperCase(),'=','OR');
							 }
						 }
						 
					 }
				 } 
			 } else if (level == 1){
				 restriction.addClause('afm_redlines.dwg_name', parentNode.data['afm_dwgs.dwg_name'].toUpperCase(), '=');
				 
					
				 if(valueExistsNotEmpty(dateRequestedFrom)){
					 restriction.addClause('afm_redlines.date_created',consolePanel.getFieldValue("afm_redlines.date_created.from"),'&gt;=');
				 }
				 if(valueExistsNotEmpty(dateRequestedTo)){
					 restriction.addClause('afm_redlines.date_created',consolePanel.getFieldValue("afm_redlines.date_created.to"),'&lt;=');
				 }
				 if(valueExistsNotEmpty(consolePanel.getFieldValue("activity_log_hactivity_log.site_id"))){
					 restriction.addClause('activity_log_hactivity_log.site_id',"%"+consolePanel.getFieldValue('activity_log_hactivity_log.site_id')+"%",'LIKE');
				 }
				 if(valueExistsNotEmpty(consolePanel.getFieldValue("activity_log_hactivity_log.bl_id"))){
					 restriction.addClause('activity_log_hactivity_log.bl_id',"%"+consolePanel.getFieldValue('activity_log_hactivity_log.bl_id')+"%",'LIKE');
				 }
				 if(valueExistsNotEmpty(consolePanel.getFieldValue("activity_log_hactivity_log.fl_id"))){
					 restriction.addClause('activity_log_hactivity_log.fl_id',"%"+consolePanel.getFieldValue('activity_log_hactivity_log.fl_id')+"%",'LIKE');
				 }
				 if(valueExistsNotEmpty(consolePanel.getFieldValue("activity_log_hactivity_log.prob_type"))){
					 restriction.addClause('activity_log_hactivity_log.prob_type',"%"+consolePanel.getFieldValue('activity_log_hactivity_log.prob_type')+"%",'LIKE');
				 }
				 if(valueExistsNotEmpty(consolePanel.getFieldValue("activity_log_hactivity_log.activity_type"))){
					 restriction.addClause('activity_log_hactivity_log.activity_type',"%"+consolePanel.getFieldValue('activity_log_hactivity_log.activity_type')+"%",'LIKE');
				 }
			 }
			 return restriction;
		 }
	},
	
	/**
	 * Filter tree.
	 */
	consolePanel_onShow:function(){	
		var dateRequestedFrom = this.consolePanel.getFieldElement("afm_redlines.date_created.from").value;
		var dateRequestedTo = this.consolePanel.getFieldElement("afm_redlines.date_created.to").value;
		
		if (dateRequestedFrom!='' && dateRequestedTo!='') {
			if (!compareLocalizedDates(dateRequestedFrom,dateRequestedTo)){
				alert(getMessage('error_date_range'));
				return;
			}
		}
		this.treePanel_dwgs.refresh();
	
	},
	
	/**
	 * Clear console panel
	 */
	consolePanel_onClear: function(){
		this.consolePanel.setFieldValue("activity_log_hactivity_log.site_id",'');
		this.consolePanel.setFieldValue("activity_log_hactivity_log.bl_id",'');
		this.consolePanel.setFieldValue("activity_log_hactivity_log.fl_id",'');
		this.consolePanel.setFieldValue("activity_log_hactivity_log.prob_type",'');		
		this.consolePanel.setFieldValue("afm_redlines.date_requested.from",'');	
		this.consolePanel.setFieldValue("afm_redlines.date_requested.to",'');
		this.consolePanel.setFieldValue("afm_redlines.dwgname",'');
		
		this.consolePanel_onShow();
	}
	
});

/**
 * Function called when leave record (activity_log - afm_redlines) is clicked in the tree
 * @param object
 */
function showRedlinedDrawing(object){
	
	var records = View.dataSources.get('reviewRedlinesConsoleDS').getRecords(object.restriction);
	if(records.length>0){
		var blId = records[0].getValue('activity_log_hactivity_log.bl_id');
		var flId = records[0].getValue('activity_log_hactivity_log.fl_id');
		var parameters = new Ab.view.ConfigObject();
		parameters['planTypeGroup'] = 'Standard Space Highlights';
		parameters['redlineLegend'] = {panelId: 'redlineLegendPanel', divId: 'redlineLegendDiv', colorPickerId: 'redlineLegendPanel_head'};
		parameters['planTypeHighlight'] = {panelId: 'planTypeHighlightPanel', divId: 'planTypeHighlightDiv'};
		// load SVG from server and display in SVG panel's  <div id="drawingDiv">    	
		var drawingControlEx = new Ab.svg.MarkupDrawingControl("drawingDiv", "drawingPanel", parameters); 
		// define parameters to be used by server-side job
		var parameters = new Ab.view.ConfigObject();
		parameters['activityLogId'] = records[0].getValue('afm_redlines.activity_log_id');;
		parameters['pkeyValues'] = {'bl_id':blId, 'fl_id':flId};
		// load the floorplan
		drawingControlEx.filterHighlight = 'none',
		drawingControlEx.load("drawingDiv", parameters);
	}
	
}

function afterGeneratingTreeNode(treeNode){
	//XXX: treeNode has all info about tree object such as treeNode.level being Ab.tree.TreeLevel object 
	if(treeNode.depth == 0){
		var labelText = treeNode.label;
		if(valueExistsNotEmpty(treeConsoleRestriction)){
			var ds = View.dataSources.get('reviewRedlinesTree_redlinesDS');
			var records = ds.getRecords("afm_redlines.dwg_name = '"+treeNode.data['afm_dwgs.dwg_name'].toUpperCase() +"' AND " + treeConsoleRestriction);
			labelText += "<span id='ext-gen133' class='ygtvlabel'> ("+records.length+")</span>";
			
		} else {
			labelText += "<span id='ext-gen133' class='ygtvlabel'> ("+treeNode.data['afm_dwgs.nr_redlines']+")</span>";
		}
		treeNode.setUpLabel(labelText);
	}  	
}