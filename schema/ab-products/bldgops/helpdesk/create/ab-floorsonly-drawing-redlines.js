var floorsOnlyController = View.createController('floorsOnlyController', {
	locArray: [],
	
	drawingControlEx: null,
	
	
	afterViewLoad: function() {
		
		this.drawingPanel.actions.get('saveRedmarks').show(false);
		
    	// overrides Grid.onChangeMultipleSelection to load a drawing
    	this.floorsonly_floors.addEventListener('onMultipleSelectionChange', function(row) {
    		var opts = null;
    		floorsOnlyController.locArray[0] = row['rm.bl_id'];
    		floorsOnlyController.locArray[1] = row['rm.fl_id'];
    		
    		var parameters = new Ab.view.ConfigObject();
	    	parameters['planTypeGroup'] = 'Standard Space Highlights';
	    	parameters['redlineLegend'] = {panelId: 'redlineLegendPanel', divId: 'redlineLegendDiv', colorPickerId: 'redlineLegendPanel_head'};
	    	parameters['planTypeHighlight'] = {panelId: 'planTypeHighlightPanel', divId: 'planTypeHighlightDiv'};
	    	// load SVG from server and display in SVG panel's  <div id="drawingDiv">    	
	    	var drawingControlEx = new Ab.svg.MarkupDrawingControl("drawingDiv", "drawingPanel", parameters); 
	    	// define parameters to be used by server-side job
	    	var parameters = new Ab.view.ConfigObject();
	    	//parameters['activityLogId'] = activityLogId;
	    	parameters['pkeyValues'] = {'bl_id':row['rm.bl_id'], 'fl_id':row['rm.fl_id']};
	    	parameters['activityLogId'] = -1;
	    	// load the floorplan
	    	drawingControlEx.filterHighlight = 'none',
	    	drawingControlEx.load("drawingDiv", parameters);
	    	floorsOnlyController.drawingControlEx = drawingControlEx;
	    	floorsOnlyController.drawingPanel.setTitle(getMessage('createRedlines'));
	    	floorsOnlyController.drawingPanel.actions.get('saveRedmarks').show(true);
	    	floorsOnlyController.locArray[0] = row['rm.bl_id'];
	    	floorsOnlyController.locArray[1] = row['rm.fl_id'];
	    });
	},
	
	/**
	 * save redmarks as Service request
	 */
	drawingPanel_onSaveRedmarks: function(){
		try{
    	    //capture image and save it to the activity_log table
             var imageCapture = new ImageCapture();
            imageCapture.captureImage(floorsOnlyController.drawingControlEx.divId, false, floorsOnlyController.attachRedlinesImageToServiceRequest.createDelegate(this, [this.drawingControlEx], 2));
		} catch(e){
	    		View.showMessage('error', '', e.message, e.data);
		}
	},
	
	attachRedlinesImageToServiceRequest: function(imageContent, control){
		if( !imageContent)
			return;
		
		var restriction = new Ab.view.Restriction();
		restriction.addClause('activity_log.bl_id',this.locArray[0]);
		restriction.addClause('activity_log.fl_id',this.locArray[1]);
		
		//open dialog with create request view
		View.openDialog("ab-helpdesk-request-create.axvw",restriction,true,{
			//callback function, called after saving basic request information, to attach redlines to service request
	        callback: function(activityLogId) {
	            control.filterHighlight = null;
	            control.svgActions.uploadPanel.setFieldValue("afm_redlines.activity_log_id", activityLogId);
	            control.save();
	        },redlining:true
		});
		
	}
	
});






