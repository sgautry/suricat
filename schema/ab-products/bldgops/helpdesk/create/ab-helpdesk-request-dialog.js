/**
 * @author Guo
 */
var controller = View.createController('hlRmByStdController', {

    //----------------event handle--------------------
    afterViewLoad: function(){
    	this.svg_ctrls.setTitle(getMessage('drawingPanelTitle1'));
    },
    
    svgControl: null,
	
	loadSvg: function(bl_id, fl_id, drawingName) {
    	
    	// define parameters to be used by server-side job
    	var parameters = new Ab.view.ConfigObject();
    	parameters['highlightParameters'] = [{'view_file':"ab-helpdesk-request-dialog.axvw", 'hs_ds': "abHelpdeskRequestDialog_rmHighlight", 'label_ds':'abHelpdeskRequestDialog_rmLabel'}];
    	parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id};
    	parameters['drawingName'] = drawingName;
    	parameters['divId'] = "svgDiv";
    	parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : onClickDrawingHandler}];
		
		this.svgControl = new Drawing.DrawingControl("svgDiv", "svg_ctrls", parameters);	
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);	
    	
    },
});

/**
 * event handler when click the floor level of the tree
 */
function onFlTreeClick(ob){
    var currentNode = View.panels.get('abHelpdeskRequestDialogTreeSite').lastNodeClicked;
    var blId = currentNode.parent.data['bl.bl_id'];
    var flId = currentNode.data['fl.fl_id'];
    View.dataSources.get('abHelpdeskRequestDialog_rmHighlight').addParameter('rmStd', "rm.rm_std IS NOT NULL");
    
    var title = String.format(getMessage('drawingPanelTitle2'), blId + "-" + flId);
    displayFloor(currentNode, title);
  }
  
/**
 * display floor drawing for highlight report
 * @param {Object} drawingPanel
 * @param {Object} res
 * @param {String} title
 */
function displayFloor(currentNode, title){
    var blId = getValueFromTreeNode(currentNode, 'bl.bl_id');
    var flId = getValueFromTreeNode(currentNode, 'fl.fl_id');
    var dwgName = getValueFromTreeNode(currentNode, 'fl.dwgname');
    controller.loadSvg(blId, flId, dwgName);	
    controller.svg_ctrls.setTitle(title);
}

/**
 * get value from tree node
 * @param {Object} treeNode
 * @param {String} fieldName
 */
function getValueFromTreeNode(treeNode, fieldName){
    var value = null;
    if (treeNode.data[fieldName]) {
        value = treeNode.data[fieldName];
        return value;
    }
    if (treeNode.parent.data[fieldName]) {
        value = treeNode.parent.data[fieldName];
        return value;
    }
    if (treeNode.parent.parent.data[fieldName]) {
        value = treeNode.parent.parent.data[fieldName];
        return value;
    }
    if (treeNode.parent.parent.parent.data[fieldName]) {
        value = treeNode.parent.parent.parent.data[fieldName];
        return value;
    }
    return value;
}

/**
 * event handler when click room in the drawing panel
 */
function onClickDrawingHandler(assetIds, drawingController){
	var c=View.getOpenerView().controllers.items[0];
	    pk = assetIds.assetId.split(';');
		c.locArray[0]=pk[0];
		c.locArray[1]=pk[1];
		c.locArray[2]=pk[2];
		c.setLocationPram();
}
