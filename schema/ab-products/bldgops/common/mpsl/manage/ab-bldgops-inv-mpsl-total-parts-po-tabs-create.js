var createPoSingleCtrl=View.createController('createPoSingleCtrl',{
	//storage location code
	partStoreLocationCode: "",
	//part code
	partId: "",

	afterViewLoad: function(){
		this.purchaseOrderCreateForm.fields.get("po.vn_id").actions.get(0).command.commands[0].beforeSelect = this.beforeSelectVnIdByPartId.createDelegate(this);
	},
	
	afterInitialDataFetch: function(){
		var openerView=View.getOpenerView();
		//register current view controller to parent tabs controller.
		this.registerTabCtrl(openerView);
		
		this.partStoreLocationCode=openerView.parameters['partStoreCode'];
		this.partId=openerView.parameters['partId'];
		this.setFieldValueAfterViewLoad();
	},
	
	/**
	 * register current tab view to parent tabs controller.
	 * @param {openerView} opener tabs view.
	 */
	registerTabCtrl: function(openerView){
		var tabsCtrl=openerView.controllers.items[0];
		tabsCtrl.tabCtrls["createPurchaseOrderTab"]=this;
	},
	
	/**
	 * set field value to parent tabs controller before tab selected.
	 * @param {parentTabsCtrl} parent tabs controller.
	 */
	beforeTabSelect: function(parentTabsCtrl){
		var canChange=true;
		
		parentTabsCtrl.transQty=this.purchaseOrderCreateForm.getFieldValue('po_line.quantity');
		parentTabsCtrl.unitCost=this.purchaseOrderCreateForm.getFieldValue('po_line.unit_cost');
		parentTabsCtrl.comments=this.purchaseOrderCreateForm.getFieldValue('po.comments');
		
		return canChange;
	},
	
	/**
	 * re-set field value after tab selected.
	 * @param {parentTabsCtrls} parent tabs controller.
	 */
	afterTabSelect: function(parentTabsCtrl){
		this.resetFieldsValueAfterTabChange(parentTabsCtrl.transQty,parentTabsCtrl.unitCost,parentTabsCtrl.comments);
	},
	
	/**
	 * set field value after view first load.
	 */
	setFieldValueAfterViewLoad: function(){
		this.setValueByPartCodeAndStoreLocInfo();
		//If multiple records exist in the PV table for the selected Part Code, then default to the record with the lowest rank that is not 0
		this.setDefaultVnIdAndOtherInfor();
		//set requestor name to default login user.
		this.setDefaultRequestorName();
	},
	
	/**
	 * KB#3051967
	 * Refilling items is not necessary whatever create new requisition or add to exsiting requisition
	 */
	resetFieldsValueAfterTabChange: function(transQty,unitCost,comments){
		var ds=this.purchaseOrderCreateForm.getDataSource();
		this.purchaseOrderCreateForm.setFieldValue('po_line.quantity',ds.formatValue('po_line.quantity',transQty,true));
		this.purchaseOrderCreateForm.setFieldValue('po_line.unit_cost',ds.formatValue('po_line.unit_cost',unitCost,true));
		this.purchaseOrderCreateForm.setFieldValue('po.comments',comments);
		calculateTotalCost();
	},

	/**
	 * Set value to supply requisition edit form by part code and part storage location code
	 */
	setValueByPartCodeAndStoreLocInfo: function(){
		//Clear supply requisition form
		this.purchaseOrderCreateForm.clear();
		//Set value to create supply requisition panel
		this.purchaseOrderCreateForm.setFieldValue('po.receiving_location',this.partStoreLocationCode);
		this.purchaseOrderCreateForm.setFieldValue('po_line.partCode',this.partId);
		this.purchaseOrderCreateForm.setFieldValue('po_line.qtyUnderstocked',0);
		this.purchaseOrderCreateForm.setFieldValue('po_line.quantity',0);
		this.purchaseOrderCreateForm.setFieldValue('po_line.unit_cost',0);
		this.purchaseOrderCreateForm.setFieldValue('po_line.totalCost',0);
		//set form field value by storage location code and part code
		this.setPartRecordByStorageLocationIdAndPartId(this.partStoreLocationCode,this.partId);
	},
	
	/**
	 * set requestor name to default login user.
	 */
	setDefaultRequestorName: function(){
		var currentUser=Ab.view.View.user.employee.id;
		this.purchaseOrderCreateForm.setFieldValue('po.em_id',currentUser);
	},
	
	/**
	 * Submit purchase order information
	 */
	purchaseOrderCreateForm_onSubmit: function(){
		//validate on purchase order create form field value
		var validateResult=this.purchaseOrderCreateFormValidate();
		if(!validateResult){
			return;
		}
		
		//get field value from form panel
		var vnId=this.purchaseOrderCreateForm.getFieldValue('po.vn_id');
		var receivingLoc=this.purchaseOrderCreateForm.getFieldValue('po.receiving_location');
		var catNo=this.purchaseOrderCreateForm.getFieldValue('po_line.catno');
		var tansQty=this.purchaseOrderCreateForm.getFieldValue('po_line.quantity');
		var unitCost=this.purchaseOrderCreateForm.getFieldValue('po_line.unit_cost');
		var comments=this.purchaseOrderCreateForm.getFieldValue('po.comments');
		var acId=this.purchaseOrderCreateForm.getFieldValue('po.ac_id');
		var poNumber=this.purchaseOrderCreateForm.getFieldValue('po.po_number');
		var source=this.purchaseOrderCreateForm.getFieldValue('po.source');
		var emId=this.purchaseOrderCreateForm.getFieldValue('po.em_id');

		//prepare workflow rule parameter
		var poRecords=[];
		poRecords[0]=new Object();
		poRecords[0]['receivingLoc']=receivingLoc;
		poRecords[0]['vnId']=vnId;
		poRecords[0]['acId']=acId;
		poRecords[0]['poNumber']=poNumber;
		poRecords[0]['source']=source;
		poRecords[0]['comments']=comments;
		poRecords[0]['emId']=emId;
		
		var poLineRecords = [];
		poLineRecords[0]=new Object();
		poLineRecords[0]['po_line.catno']=catNo;
		poLineRecords[0]['po_line.quantity']=tansQty;
		poLineRecords[0]['po_line.unit_cost']=unitCost;
		poLineRecords[0]['po_line.part_id']=this.partId;
		poLineRecords[0]['po_line.description']="";
		
		//call workflow rule
		try{
			var result=Workflow.callMethod('AbBldgOpsBackgroundData-BldgopsPartInventoryService-createNewPurchaseOrder',poRecords,poLineRecords);
			if(result.code=="executed"){
				//refresh view panel after workflow rule finish
				View.getOpenerView().getOpenerView().panels.get('abBldgopsReportPartsInvertoryStorageLocationGrid').refresh();
				View.getOpenerView().getOpenerView().closeDialog();
			}
		}catch(e){
			Workflow.handleError(e);
		}
		
	},
	/**
	 * validate purchase order create form field value.
	 */
	purchaseOrderCreateFormValidate: function(){
		var canSave=this.purchaseOrderCreateForm.canSave();
		if(canSave){
			var tansQty=this.purchaseOrderCreateForm.getFieldValue('po_line.quantity');
			var unitCost=this.purchaseOrderCreateForm.getFieldValue('po_line.unit_cost');
			//transfer quantity must greater than 0
			tansQty=parseInt(tansQty);
			if(tansQty<1){
				View.alert(getMessage("transactionQuantityGreaterThanZeroMsg"));
				return false;
			}
			//unit cost must greater than 0
			var unitCost=parseFloat(unitCost);
			if(unitCost<=0){
				View.alert(getMessage("unitCostGreaterThanZeroMsg"));
				return false;
			}
			return true;
		}else{
			return false;
		}
	},
	
	/**
	 * Get part record by storage location id and part id.
	 * @param storageLocationId Storage Location Id.
	 * @param partId Part code.
	 */
	setPartRecordByStorageLocationIdAndPartId: function(storageLocationId,partId){
	    var ptStorageLocationDs=View.dataSources.get('dialogCreatePurchaseOrderPtDS');
		var res=new Ab.view.Restriction();
		res.addClause('pt_store_loc_pt.pt_store_loc_id',storageLocationId,'=');
		res.addClause('pt_store_loc_pt.part_id',partId,'=');
		
		var ptRecords=ptStorageLocationDs.getRecords(res);
		
		if(ptRecords.length>0){
			var ptRecord=ptRecords[0];
			this.purchaseOrderCreateForm.setFieldValue('po_line.partDescription',ptRecord.getValue('pt.description'));
			
			var qtyOnOrder=ptRecord.getValue('pt_store_loc_pt.qty_to_order');
			var unitCost=ptRecord.getValue('pt_store_loc_pt.cost_unit_std');
			if(!valueExistsNotEmpty(qtyOnOrder)){
				qtyOnOrder=0;
			}
			var fieldDef=this.purchaseOrderCreateForm.fields.get('po_line.unit_cost').fieldDef;
			this.purchaseOrderCreateForm.setFieldValue('po_line.qtyUnderstocked',ptStorageLocationDs.formatValue('pt_store_loc_pt.qty_to_order',qtyOnOrder,true));
			this.purchaseOrderCreateForm.setFieldValue('po_line.unit_cost',fieldDef.formatValue(unitCost,true));
		}else{
			this.purchaseOrderCreateForm.setFieldValue('po_line.qtyUnderstocked',0);
			this.purchaseOrderCreateForm.setFieldValue('po_line.unit_cost',0);
			this.purchaseOrderCreateForm.setFieldValue('po_line.totalCost',0);
		}
	},
	
	/**
	 * Click cancel button to close the dialog view form
	 */
	purchaseOrderCreateForm_onCancel: function(){
		View.getOpenerView().getOpenerView().closeDialog();
	},
	
	/**
	 * Get catlog number by Vendor Code and Part Code.
	 * @param {vnId} Vendor Code
	 * @param {partId} Part Code
	 * @return {catNo} Catlog Number
	 */
	getCatlogNoByVnIdAndPartId: function(vnId,partId){
		var restriction = new Ab.view.Restriction();
		restriction.addClause("pv.vn_id", vnId, '=');
		restriction.addClause("pv.part_id", partId, '=');
   		var parameters = {
   			tableName: "pv",
   			fieldNames: "[pv.vn_id,pv.part_id,pv.vn_pt_num]",
   			restriction: toJSON(restriction) 
   		};     		
     	var result = Workflow.runRuleAndReturnResult('AbCommonResources-getDataRecords', parameters); 
     	var catNo="";
     	if (result.code == 'executed') {
     		if (result.data.records.length == 1){
     			if(valueExistsNotEmpty(result.data.records[0]['pv.vn_pt_num'])){
     				catNo=result.data.records[0]['pv.vn_pt_num'];
     			}else{
     				catNo=partId;
     			}
     		}
     	}
     	return catNo;
	},
	
	/**
	 * calculate total cost value after quanity and unitCost field content are changed.
	 */
	calculateTotalCost: function(quntity,unitCost){
		var totalCost=0;
		//format quantity
		if(valueExistsNotEmpty(quntity)){
			quntity=parseFloat(quntity);
		}else{
			quntity=0;
		}
		//format unitCost
		if(valueExistsNotEmpty(unitCost)){
			unitCost=parseFloat(unitCost);
		}else{
			unitCost=0;
		}
		
		totalCost=quntity*unitCost;
		
		return totalCost.toFixed(2);
	},
	
	/**
	 * set restriction before vendor code selectValue dialog show.
	 * @param {command} SelectValue command object.
	 */
	beforeSelectVnIdByPartId: function(command){
		var partId=this.partId;
		var selectValueRes="pv.part_id='"+partId+"'";
		
		command.dialogRestriction = selectValueRes;
	},
	
	/**
	 * If multiple records exist in the PV table for the selected Part Code, then default to the record with the lowest rank that is not 0.
	 */
	getDefaultVnId: function(){
		var vnId="";
		var pvDs=View.dataSources.get('pvLowestDs');
		var pvRes=new Ab.view.Restriction();
			pvRes.addClause('pv.part_id',this.partId,'=');
		var pvRecords=pvDs.getRecords(pvRes);
		
		if(pvRecords.length>0){
			vnId=pvRecords[0].getValue('pv.vn_id');
		}
		
		return vnId;
	},
	
	/**
	 * Set Default Vendor code when view load.
	 */
	setDefaultVnIdAndOtherInfor: function(){
		var vnId=this.getDefaultVnId();
		if(valueExistsNotEmpty(vnId)){
			var catNo=this.getCatlogNoByVnIdAndPartId(vnId,this.partId);
			if(!valueExistsNotEmpty(catNo)){
				catNo=this.partId;
			}
			this.purchaseOrderCreateForm.setFieldValue('po.vn_id',vnId);
			this.purchaseOrderCreateForm.setFieldValue('po_line.catno',catNo);
		}
	}
});

/**
 * Vendor select value action listener.
 * 
 * @param fieldName Field name
 * @param selectValue Select value
 * @param previousValue Previous value
 */
function vnSelectListener(fieldName,selectValue,previousValue){
	if(fieldName=='po.vn_id'){
		var ptId=View.panels.get('purchaseOrderCreateForm').getFieldValue('po_line.partCode');
		var catNo=View.controllers.get('createPoSingleCtrl').getCatlogNoByVnIdAndPartId(selectValue,ptId);
		if(!valueExistsNotEmpty(catNo)){
			catNo=ptId;
		}
		
		View.panels.get('purchaseOrderCreateForm').setFieldValue('po.vn_id',selectValue);
		View.panels.get('purchaseOrderCreateForm').setFieldValue('po_line.catno',catNo);
	}
}

/**
 * calculate total cost.
 */
function calculateTotalCost(){
	var quanity=View.panels.get('purchaseOrderCreateForm').getFieldValue('po_line.quantity');
	var unitCost=View.panels.get('purchaseOrderCreateForm').getFieldValue('po_line.unit_cost');
	var totalCost=View.controllers.get('createPoSingleCtrl').calculateTotalCost(quanity,unitCost);
	var fieldDef=View.panels.get('purchaseOrderCreateForm').fields.get('po_line.unit_cost').fieldDef;
	View.panels.get('purchaseOrderCreateForm').setFieldValue('po_line.totalCost',fieldDef.formatValue(totalCost,true));
}