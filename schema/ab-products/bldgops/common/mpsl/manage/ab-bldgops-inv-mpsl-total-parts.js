var totalPartsInvCtrl=View.createController('totalPartsInvCtrl',{
	//Define part code
	partCode: "",

	afterInitialDataFetch: function(){
		var panelTitleNode = this.abBldgopsReportPartsInvertoryGrid.getTitleEl().dom.parentNode.parentNode;
    	var cbxTdElement = Ext.DomHelper.append(panelTitleNode, {
    		tag : 'td',
    		id : 'understockedCbx'
    	});
    	var cbxElement="<input type='checkbox' id='cbxUnderstocked' onclick='onclickCbx();'/>&nbsp;&nbsp;<b>"+getMessage('isUnderstocked')+"</b></input>";
    	Ext.DomHelper.append(cbxTdElement,cbxElement,true);
	},
	
	/**
	 * Show and refresh part inventory storage location by part code
	 */
	showPartsInventoryStorageLocation: function(){
		var selectRowIndex=this.abBldgopsReportPartsInvertoryGrid.selectedRowIndex;
		var selectRowRecord=this.abBldgopsReportPartsInvertoryGrid.gridRows.get(selectRowIndex).getRecord();
		var partId=selectRowRecord.getValue('pt.part_id');
		if(valueExistsNotEmpty(partId)){
			this.partCode=partId;
			var partRes=new Ab.view.Restriction();
			partRes.addClause('pt_store_loc_pt.part_id',partId,'=');
			this.abBldgopsReportPartsInvertoryStorageLocationGrid.refresh(partRes);
		}
	},
	
	/**
	 * After Refresh。
	 */
	abBldgopsReportPartsInvertoryStorageLocationGrid_afterRefresh: function(){
		//kb#3053972 hidden column title of radio button column.
		var grid=this.abBldgopsReportPartsInvertoryStorageLocationGrid;
		var columns = grid.columns;
		var headerRow=grid.headerRows[0];
		for(var i=0;i<columns.length;i++){
			if(columns[i].id=='pt_store_loc_pt.radioCheckItem'){
				headerRow.childNodes[i].innerHTML="";
			}
		}
		this.abBldgopsReportPartsInvertoryStorageLocationGrid.setTitle(String.format(getMessage('partInventoryStorageLocationTitle'),this.partCode));
	},
	
	/**
	 * Show Update physical Count dialog。
	 */
	abBldgopsReportPartsInvertoryStorageLocationGrid_onUpdatePhsicalCount: function(){
		var selectRowIndex=this.getGridSelectedRowIndex();
		if(selectRowIndex!=-1){
			var selectRowRecord=this.abBldgopsReportPartsInvertoryStorageLocationGrid.gridRows.get(selectRowIndex).getRecord();
			//Show update physical count page in dialog
			var storageLocationCode=selectRowRecord.getValue('pt_store_loc_pt.pt_store_loc_id');
			var partId=selectRowRecord.getValue('pt_store_loc_pt.part_id');
			var physicalQuantityCount=selectRowRecord.getValue('pt_store_loc_pt.qty_physical_count');
			var physicalCountDialogRes=new Ab.view.Restriction();
				physicalCountDialogRes.addClause('pt_store_loc_pt.pt_store_loc_id',storageLocationCode,'=');
				physicalCountDialogRes.addClause('pt_store_loc_pt.part_id',partId,'=');
			
			this.partInventoryMpiwUpdatePhysicalCountDialog.refresh(physicalCountDialogRes);
			this.partInventoryMpiwUpdatePhysicalCountDialog.showInWindow({
				x: 400,
				y: 300,
				width: 800, 
	            height: 500,
	            modal:true,
	            closeButton:true
			});
			this.partInventoryMpiwUpdatePhysicalCountDialog.setFieldValue('pt_store_loc_pt.date_of_last_cnt',"");
			$('chkBoxAdjust').checked=false;
		}else{
			View.alert(getMessage('mustSelectARowMsg'));
		}
	},
	
	/**
	 * Create Supply Requisition
	 */
	abBldgopsReportPartsInvertoryStorageLocationGrid_onCreateSupplyRequisition: function(){
		var selectRowIndex=this.getGridSelectedRowIndex();
		if(selectRowIndex!=-1){
			var selectRowRecord=this.abBldgopsReportPartsInvertoryStorageLocationGrid.gridRows.get(selectRowIndex).getRecord();
			//Show create supply requisition page in dialog
			var partStoreCode=selectRowRecord.getValue('pt_store_loc_pt.pt_store_loc_id');
			var partId=selectRowRecord.getValue('pt_store_loc_pt.part_id');

			this.openCreateSupplyRequistionViewDialog(partStoreCode,partId);
			
		}else{
			View.alert(getMessage('mustSelectARowMsg'));
		}
	},
	
	/**
	 * Open create supply requisition view dialog.
	 */
	openCreateSupplyRequistionViewDialog: function(partStoreCode,partId){
		//Show supply requisition edit dialog form
		var dialogConfig = {
				width:1000,
				height:600,
				partStoreCode: partStoreCode,
				partId: partId,
				title: getMessage('supplyReqDialogTitle'),
				closeButton: false 
		};
		View.openDialog("ab-bldgops-inv-mpsl-total-parts-req-tabs.axvw", null, false, dialogConfig);
	},
	
	/**
	 * Create Purchase Order.
	 */
	abBldgopsReportPartsInvertoryStorageLocationGrid_onCreatePurchaseOrder: function(){
		var selectRowIndex=this.getGridSelectedRowIndex();
		if(selectRowIndex!=-1){
			var selectRowRecord=this.abBldgopsReportPartsInvertoryStorageLocationGrid.gridRows.get(selectRowIndex).getRecord();
			//Show create supply requisition page in dialog
			var partStoreCode=selectRowRecord.getValue('pt_store_loc_pt.pt_store_loc_id');
			var partId=selectRowRecord.getValue('pt_store_loc_pt.part_id');
			//get vendor code by selected part code.
			var partRes=new Ab.view.Restriction();
			partRes.addClause('pt.part_id',partId,'=');
			var partRecord=this.abBldgopsReportPartsInvertoryDS.getRecord(partRes);
			var vendorCode=partRecord.getValue('pt.vnCode');
			if(!valueExistsNotEmpty(vendorCode)){
				View.alert(String.format(getMessage('notHaveVendorForSingleMsg'),partId));
				return;
			}
			this.openCreatePurchaseOrderViewDialog(partStoreCode,partId);
			
		}else{
			View.alert(getMessage('mustSelectARowMsg'));
		}
	},
	
	/**
	 * Open create purchase order view dialog.
	 */
	openCreatePurchaseOrderViewDialog: function(partStoreCode,partId){
		//Show supply requisition edit dialog form
		var dialogConfig = {
				width:1000,
				height:600,
				partStoreCode: partStoreCode,
				partId: partId,
				title: getMessage('purchaseOrderDialogTitle'),
				closeButton: false
		};
		View.openDialog("ab-bldgops-inv-mpsl-total-parts-po-tabs.axvw", null, false, dialogConfig);
	},
	
	/**
	 * Do Update Physical Count.
	 */
	doUpdatePhysicalCount: function(){
		var me=this;
		var canSave=this.validateFieldValueBeforeUpdatePhysicalCount();
		if(canSave){
			//prepare parameters for WFR.
			var partId=this.partInventoryMpiwUpdatePhysicalCountDialog.getFieldValue('pt_store_loc_pt.part_id');
			var storeLocId=this.partInventoryMpiwUpdatePhysicalCountDialog.getFieldValue('pt_store_loc_pt.pt_store_loc_id');
			var physicalCount=this.partInventoryMpiwUpdatePhysicalCountDialog.getFieldValue('pt_store_loc_pt.qty_physical_count');
			var isAdjustButtonChecked=$('chkBoxAdjust').checked;
			var partCodeRecords = [];
				partCodeRecords[0]=new Object();
				partCodeRecords[0]["pt.part_id"]=partId;
			//call WFR to update physical count.
			try {
				var result=Workflow.callMethod('AbBldgOpsBackgroundData-BldgopsPartInventoryService-updatePhysicalCount', storeLocId,partCodeRecords,parseFloat(physicalCount),isAdjustButtonChecked);
				if(result.code=="executed"){
					//Refresh part storage location by part code which clicked before
					var ptLocationPanelRes=new Ab.view.Restriction();
						ptLocationPanelRes.addClause('pt_store_loc_pt.part_id',me.partCode,'=');
					me.abBldgopsReportPartsInvertoryStorageLocationGrid.refresh(ptLocationPanelRes);
					me.abBldgopsReportPartsInvertoryGrid.refresh();
					//Close Dialog
					me.partInventoryMpiwUpdatePhysicalCountDialog.closeWindow();
				}
			}catch (e) {
				Workflow.handleError(e);
			}
		}
	},
	
	/**
	 * Validate on form field before update physical count.
	 */
	validateFieldValueBeforeUpdatePhysicalCount: function(){
		var canSave=this.partInventoryMpiwUpdatePhysicalCountDialog.canSave();
		if(canSave){
			var physicalCount=this.partInventoryMpiwUpdatePhysicalCountDialog.getFieldValue('pt_store_loc_pt.qty_physical_count');
				physicalCount=parseFloat(physicalCount);
			if(physicalCount<0){
				View.alert(getMessage("transactionQuantityGreaterThanZeroMsg"));
				return false;
			}
		}else{
			return false
		}
		
		return true;
	},
	
	/**
	 * Get part description by part code
	 * @param partCode Part code
	 * @return partDescription Part description
	 */
	getPartDescriptionByPartCode: function(partCode){
		var partDescription="";
		var partDs=View.dataSources.get('abBldgopsReportPartsInvertoryDS');
		var partRes=new Ab.view.Restriction();
		partRes.addClause('pt.part_id',partCode,'=');
		var partRecord=partDs.getRecord(partRes);
		if(!partRecord.isNew){
			partDescription=partRecord.getValue('pt.description');
		}
		return partDescription;
	},
	
	/**
	 * Show edit part-storage location form
	 */
	showEditStorageLocationForm: function(){
		var selectRowIndex=this.abBldgopsReportPartsInvertoryStorageLocationGrid.selectedRowIndex;
		var selectRowRec=this.abBldgopsReportPartsInvertoryStorageLocationGrid.gridRows.get(selectRowIndex).getRecord();
		var partStoreLocId=selectRowRec.getValue('pt_store_loc_pt.pt_store_loc_id');
		var partId=selectRowRec.getValue("pt_store_loc_pt.part_id");
		View.openDialog('ab-bldgops-inv-mpsl-pt-store-loc-detail-dialog.axvw', null, false, {
            width: 800,
            height: 600,
            partStoreLocId: partStoreLocId,
            partId: partId,
            partInventoryListPanel: 'abBldgopsReportPartsInvertoryStorageLocationGrid',
            actionType:'edit',
            closeButton: true
        });
	},
	
	/**
	 * Show edit part-storage location form
	 */
	addPartToStorageLocation: function(){
		var partId=this.partCode;
		View.openDialog('ab-bldgops-inv-mpsl-pt-store-loc-detail-dialog.axvw', null, false, {
            width: 800,
            height: 600,
            partStoreLocId: '',
            partId: partId,
            partInventoryListPanel: 'abBldgopsReportPartsInvertoryStorageLocationGrid',
            actionType:'addnew-byPart',
            closeButton: true
        });
	},
	
	/**
	 * show vendor code list by part code selected.
	 * @param partId Part Code.
	 */
	showVnCodeListByPartCode: function(){
		var rowIndex=this.abBldgopsReportPartsInvertoryGrid.selectedRowIndex;
		var partId=this.abBldgopsReportPartsInvertoryGrid.gridRows.get(rowIndex).getRecord().getValue('pt.part_id');
		var pvRes=new Ab.view.Restriction();
		pvRes.addClause('pv.part_id',partId,'=');
		
		View.panels.get('vendorCodeListDialog').showInWindow({
			width: 500,
			height:400,
			modal:true,
			title: String.format(getMessage('vendorDialogTitle'),partId)
		});
		View.panels.get('vendorCodeListDialog').refresh(pvRes);
	},
	
	/**
	 * Open map view dialog when click Map button.
	 */
	abBldgopsReportPartsInvertoryStorageLocationGrid_onBtnMap: function(){
		var controller=totalPartsInvCtrl;
		var partId=this.partCode;
		View.openDialog('ab-bldgops-inv-mpsl-total-parts-map.axvw', null, false, {
            width: 1100,
            height: 900,
            partId: partId,
            title: String.format(getMessage('storageLocationMapDialogTitle'),partId),
            closeButton: true,
            callback: function(partStoreCode,type){
            	View.closeDialog();
            	if(type=="Purchase"){
        			//call open create purchase order dialog form method in opener view.
            		controller.openCreatePurchaseOrderViewDialog(partStoreCode,partId);
        		}
        		if(type=="Requistion"){
        			//call open supply requisition edit dialog form method in opener view.
        			controller.openCreateSupplyRequistionViewDialog(partStoreCode,partId);
        		}
            }
        });
	},
	
	/**
	 * Show dialog when click Estimated and Not Reserved field.
	 */
	openEstimateAndNotReservedReport: function(){
		var rowIndex=this.abBldgopsReportPartsInvertoryStorageLocationGrid.selectedRowIndex;
		var partId=this.abBldgopsReportPartsInvertoryStorageLocationGrid.gridRows.get(rowIndex).getRecord().getValue('pt_store_loc_pt.part_id');
		var storageLocId=this.abBldgopsReportPartsInvertoryStorageLocationGrid.gridRows.get(rowIndex).getRecord().getValue('pt_store_loc_pt.pt_store_loc_id');
		
		View.openDialog("ab-bldgops-inv-mpsl-estimate-notreserved.axvw",null,false,{
			width: 800,
            height: 500,
            partId:partId,
            storageLocId:storageLocId,
            title: String.format(getMessage('estimateAndNotReservedDialogTitle'),storageLocId,partId)
		});
	},
	
	/**
	 * Show dialog when click Qty. On Order field.
	 */
	showQtyOnOrderGrid: function(){
		var rowIndex=this.abBldgopsReportPartsInvertoryStorageLocationGrid.selectedRowIndex;
		var partId=this.abBldgopsReportPartsInvertoryStorageLocationGrid.gridRows.get(rowIndex).getRecord().getValue('pt_store_loc_pt.part_id');
		var storageLocId=this.abBldgopsReportPartsInvertoryStorageLocationGrid.gridRows.get(rowIndex).getRecord().getValue('pt_store_loc_pt.pt_store_loc_id');
		this.poLineItemListForm.showInWindow({
			width:800,
			height:600,
			modal:true
		});
		this.poLineItemListForm.show(true);
		this.poLineItemListForm.addParameter('receivingLocationParam',storageLocId);
		this.poLineItemListForm.addParameter('partCodeParam',partId);
		this.poLineItemListForm.refresh();
	},
	
	/**
	 * Show dialog when click Qty. In Transit From field.
	 */
	showQtyInTransitFromDialog: function(){
		var rowIndex=this.abBldgopsReportPartsInvertoryStorageLocationGrid.selectedRowIndex;
		var partId=this.abBldgopsReportPartsInvertoryStorageLocationGrid.gridRows.get(rowIndex).getRecord().getValue('pt_store_loc_pt.part_id');
		var storageLocId=this.abBldgopsReportPartsInvertoryStorageLocationGrid.gridRows.get(rowIndex).getRecord().getValue('pt_store_loc_pt.pt_store_loc_id');
		this.supplyReqItemListByFromGrid.showInWindow({
			width:800,
			height:600,
			modal:true
		});
		this.supplyReqItemListByFromGrid.show(true);
		this.supplyReqItemListByFromGrid.addParameter('ptStoreLocFromParam',storageLocId);
		this.supplyReqItemListByFromGrid.addParameter('partCodeParam',partId);
		this.supplyReqItemListByFromGrid.refresh();
	},
	
	/**
	 * Show dialog when click Qty. In Transit To field.
	 */
	showQtyInTransitToDialog: function(){
		var rowIndex=this.abBldgopsReportPartsInvertoryStorageLocationGrid.selectedRowIndex;
		var partId=this.abBldgopsReportPartsInvertoryStorageLocationGrid.gridRows.get(rowIndex).getRecord().getValue('pt_store_loc_pt.part_id');
		var storageLocId=this.abBldgopsReportPartsInvertoryStorageLocationGrid.gridRows.get(rowIndex).getRecord().getValue('pt_store_loc_pt.pt_store_loc_id');
		this.supplyReqItemListByToGrid.showInWindow({
			width:800,
			height:600,
			modal:true
		});
		this.supplyReqItemListByToGrid.show(true);
		this.supplyReqItemListByToGrid.addParameter('ptStoreLocToParam',storageLocId);
		this.supplyReqItemListByToGrid.addParameter('partCodeParam',partId);
		this.supplyReqItemListByToGrid.refresh();
	},
	
	/**
	 * Get radio button checked row index.
	 */
	getGridSelectedRowIndex: function(){
		var rowIndex=-1;
		var gridRows=this.abBldgopsReportPartsInvertoryStorageLocationGrid.gridRows.items;
		for(var i=0;i<gridRows.length;i++){
			var row=gridRows[i];
			var radioEle=$('abBldgopsReportPartsInvertoryStorageLocationGrid_row'+i+'_pt_store_loc_pt.radioCheckItem');
			if(valueExistsNotEmpty(radioEle)){
				if(radioEle.checked){
					rowIndex=i;
					break;
				}
			}else{
				rowIndex=-1;
				break;
			}
			
		}
		return rowIndex;
	}
		
});

/**
 * Show understocked part list after Understocked checkBox checked.
 */
function onclickCbx(){
	var checkBoxChecked=$('cbxUnderstocked').checked;
	if(checkBoxChecked){
		View.panels.get('abBldgopsReportPartsInvertoryGrid').addParameter('checkedUnderstockbutton',true);
	}else{
		View.panels.get('abBldgopsReportPartsInvertoryGrid').addParameter('checkedUnderstockbutton',false);	
	}
	View.panels.get('abBldgopsReportPartsInvertoryGrid').refresh();
	View.panels.get('abBldgopsReportPartsInvertoryStorageLocationGrid').show(false);
}