/**
 * map Controller of Parts By Location view.
 * 
 * @author Jia
 * @since 24.1
 */
var mapController = mapCommonCtrl.extend({
	//override parent value to register current map view
	mapType: MapTypes.MAP_PARTS_BY_LOC,
	
	//not use API marker popup.
	usePopup: false,
	
	mapDataType: "",
	
	/********console form field value:begin***********/
	
	//Part Code of console form field
	partCode: "",
	//Part Classfication of console form field
	partClass: "",
	// Part Store Location Code of console form field
	storeLocId: "",
	//Part Store Location Site of console form field
	siteId: "",
	//Quantity Avaliable of console form field
	qtyOnHand: "",
	
	/********console form field value:end***********/
	
	afterInitialDataFetch : function() {
		//get field value from console form record
		this.getFieldValueFromConsoleRecord(View.parameters['consoleRecord']);
		//show map marker
		this.showMapMarkers();
	},
	
	/**
	 * Get field value from console form record.
	 * 
	 * @param {record} Console form record
	 */
	getFieldValueFromConsoleRecord: function(consoleRecord){
		this.partCode="";//Part Code
		this.partClass=""; //Part Classfication
		this.storeLocId=""; // Part Store Location Code
		this.siteId=""; //Part Store Location Site
		this.qtyOnHand="";// Quantity Avaliable
		
		if(valueExistsNotEmpty(consoleRecord)){
			this.partCode = consoleRecord.getValue('pt.part_id');
			this.partClass = consoleRecord.getValue('pt.class');
			this.storeLocId = consoleRecord.getValue('pt_store_loc.pt_store_loc_id');
			this.siteId = consoleRecord.getValue('pt_store_loc.site_id');
			this.qtyOnHand = consoleRecord.getValue('pt_store_loc_pt.qty_on_hand');
		}
	},
	
	/**
	 * Show building map marker.
	 */
	showMapMarkers : function() {
		//Clear Markers
		this.mapControl.clearMarkers();
		//If value of parts exists, create marker and restriction from part storage location table.
		if(valueExistsNotEmpty(this.partCode)||valueExistsNotEmpty(this.partClass)||valueExistsNotEmpty(this.qtyOnHand)){
			//if mapLegendType="loc_pt", then show map legend contains quantity information
			this.mapDataType=MapDataTypes.PSLP;
			this.mapControl.showMarkerLegend();

		}else{
			//if mapDataType="loc", then show map legend not contains quantity information.
			this.mapDataType=MapDataTypes.PSL;
			this.mapControl.hideMarkerLegend();
		}
		
		this.setMapMarkerDsParameter();
		this.createMapMarkerProperty(this.mapDataType);
		this.mapControl.showMarkers('mapMarkerDs', null);
		
	},
	
	/*********************************************show map marker*****************************************************/
	
	/**
	 * set map marker dataSource parameter
	 */
	setMapMarkerDsParameter: function(){
		
		this.mapMarkerDs.addParameter('partResBasicForPartMap',false);
		this.mapMarkerDs.addParameter('storageLocResForStoreLocId',false);
		this.mapMarkerDs.addParameter('storageLocResForSiteId',false);
		this.mapMarkerDs.addParameter('partResForPartCode',false);
		this.mapMarkerDs.addParameter('partResForPartClass',false);
		this.mapMarkerDs.addParameter('storageLocResForStoreLocId',false);
		this.mapMarkerDs.addParameter('storageLocResForSiteId',false);
		
		//map for part
		if(this.mapDataType==MapDataTypes.PSLP){
			this.mapMarkerDs.addParameter('partResBasicForPartMap',true);
			
			if(valueExistsNotEmpty(this.storeLocId)||(valueExistsNotEmpty(this.siteId))){
				if(valueExistsNotEmpty(this.storeLocId)){
					this.mapMarkerDs.addParameter('storeLocId',this.storeLocId);
					this.mapMarkerDs.addParameter('partResForStoreLocId',true);
					this.mapMarkerDs.addParameter('storageLocResForStoreLocId',true);
				}
				if(valueExistsNotEmpty(this.siteId)){
					this.mapMarkerDs.addParameter('siteId',this.siteId);
					this.mapMarkerDs.addParameter('partResForSiteId',true);
					this.mapMarkerDs.addParameter('storageLocResForSiteId',true);
				}
			}
			if(valueExistsNotEmpty(this.qtyOnHand)){
				this.mapMarkerDs.addParameter('qtyAvaliableParameter',parseFloat(this.qtyOnHand));
			}else{
				this.mapMarkerDs.addParameter('qtyAvaliableParameter','='+0);
			}
			if(valueExistsNotEmpty(this.partCode)){
				this.mapMarkerDs.addParameter('partCode',this.partCode);
				this.mapMarkerDs.addParameter('partResForPartCode',true);
			}
			if(valueExistsNotEmpty(this.partClass)){
				this.mapMarkerDs.addParameter('partClass',this.partClass);
				this.mapMarkerDs.addParameter('partResForPartClass',true);
			}
		}else{
			//map for storage location
			if(valueExistsNotEmpty(this.storeLocId)||(valueExistsNotEmpty(this.siteId))){
				if(valueExistsNotEmpty(this.storeLocId)){
					this.mapMarkerDs.addParameter('storeLocId',this.storeLocId);
					this.mapMarkerDs.addParameter('storageLocResForStoreLocId',true);
				}
				if(valueExistsNotEmpty(this.siteId)){
					this.mapMarkerDs.addParameter('siteId',this.siteId);
					this.mapMarkerDs.addParameter('storageLocResForSiteId',true);
				}
			}
		}
	},
	
	/**
	 * Create map marker property by part store location table.
	 * @param {mapDataType} 
	 * 
	 */
	createMapMarkerProperty: function(mapDataType){
		//Create Part Store Location Marker
		// create property markers
        var mapMarkerDataSource = 'mapMarkerDs';
        var mapMarkerKeyFields = ['bl.bl_id'];
        var mapMarkerGeometryFields = ['bl.lon', 'bl.lat'];
        var mapMarkerTitleField = 'bl.bl_id';
        var mapMarkerContentFields = ['bl.bl_id','bl.colorTypeForLocPt','bl.colorTypeForLoc'];
		//Get locator color
		var locatorColor=this.getMapMarkerColor();
		//Get thematic Buckets 
		var thematicBuckets=this.getMakerThematicBuckets();
		var mapMarkerProperties=null;
		
		colorbrewer['FP-YRG']={3:[]};
		colorbrewer['FP-YRG'][3]=locatorColor;
		
		//define thematic field by mapDataType value
		var thematicField="";
		if(mapDataType==MapDataTypes.PSLP){
			//show Yellow/Red/Green color 
			thematicField="bl.colorTypeForLocPt";
		}else{
			//only show Green color
			thematicField='bl.colorTypeForLoc';
		}
		var isUsePopup=this.usePopup;
		//build map marker properties
		mapMarkerProperties = {
	        radius: 10,
	        //stroke properties
			stroke: true,
		    strokeColor: '#fff',
		    strokeWeight: 1,
	        // required for thematic markers
			renderer: 'thematic-class-breaks',
			thematicField: thematicField,
			thematicClassBreaks: thematicBuckets,
			colorBrewerClass: 'FP-YRG',
			useClusters: false,
			usePopup: isUsePopup
	   };
		//create map marker
        this.mapControl.createMarkers(
        	mapMarkerDataSource, 
        	mapMarkerKeyFields,
        	mapMarkerGeometryFields,
        	mapMarkerTitleField,
        	mapMarkerContentFields,
        	mapMarkerProperties
        );
	},
	
	/**
	 * Get part storage location color by console record.
	 * 
	 * @return locatorsColor Locator color
	 */
	getMapMarkerColor: function(){
		var mapMarkerColor=[MarkerColors.GREEN,MarkerColors.RED,MarkerColors.YELLOW];
		return mapMarkerColor;
	},
	
	/**
	 * get part storage location thematic buckets by console record.
	 * 
	 * @return thematicBuckets Thematic buckets
	 */
	getMakerThematicBuckets: function(){
		var thematicBuckets=[1.5,2.5];
		return thematicBuckets;
	},
	
	/**
	 * @override
	 * Click map marker event handler.
	 * 
	 * @param {assetId} Building Code
	 * @param {featrue} Map marker feature
	 * 
	 */
	onMarkerClick: function(assetId, feature){
		this.mapControl.clearSelectedMarkers();
		this.displayMarkerPopupContent(assetId);
	},
	
	/*********************************************show map marker popup*****************************************************/
	/**
	 * Display marker popup content when click map marker.
	 * 
	 * @param blId Building Code
	 * 
	 */
	displayMarkerPopupContent: function(blId){
		var blId=blId;
		var storeLocRes=new Ab.view.Restriction();
			storeLocRes.addClause('pt_store_loc.bl_id',blId,'=');
		//get all store	location in the building.
		this.setPopupContentDsParameter();
		var storeLocRecords=this.mapPopupContentDs.getRecords(storeLocRes);
		//get map marker popup content by Storage Location records and Building Code.
		var htmlContent=this.getMarkerPopupContent(storeLocRecords,blId);
		
		this.mapControl.showMarkerInfoWindow(htmlContent);
	},
	
	/**
	 * Get marker popup content dataSource parameter.
	 */
	setPopupContentDsParameter: function(){
		this.mapPopupContentDs.addParameter('storageLocResForStoreLocId',false);
		this.mapPopupContentDs.addParameter('storageLocResForSiteId',false);
		this.mapPopupContentDs.addParameter('partResForPartCode',false);
		this.mapPopupContentDs.addParameter('partResForPartClass',false);
		this.mapPopupContentDs.addParameter('storageLocResForStoreLocId',false);
		this.mapPopupContentDs.addParameter('storageLocResForSiteId',false);
		
		if(this.mapDataType==MapDataTypes.PSLP){
			if(valueExistsNotEmpty(this.storeLocId)||(valueExistsNotEmpty(this.siteId))){
				if(valueExistsNotEmpty(this.storeLocId)){
					this.mapPopupContentDs.addParameter('storeLocId',this.storeLocId);
					this.mapPopupContentDs.addParameter('partResForStoreLocId',true);
					this.mapPopupContentDs.addParameter('storageLocResForStoreLocId',true);
				}
				if(valueExistsNotEmpty(this.siteId)){
					this.mapPopupContentDs.addParameter('siteId',this.siteId);
					this.mapPopupContentDs.addParameter('partResForSiteId',true);
					this.mapPopupContentDs.addParameter('storageLocResForSiteId',true);
				}
			}
			if(valueExistsNotEmpty(this.partCode)){
				this.mapPopupContentDs.addParameter('partCode',this.partCode);
				this.mapPopupContentDs.addParameter('partResForPartCode',true);
			}
			if(valueExistsNotEmpty(this.partClass)){
				this.mapPopupContentDs.addParameter('partClass',this.partClass);
				this.mapPopupContentDs.addParameter('partResForPartClass',true);
			}
		}else{
			if(valueExistsNotEmpty(this.storeLocId)||(valueExistsNotEmpty(this.siteId))){
				if(valueExistsNotEmpty(this.storeLocId)){
					this.mapPopupContentDs.addParameter('storeLocId',this.storeLocId);
					this.mapPopupContentDs.addParameter('storageLocResForStoreLocId',true);
				}
				if(valueExistsNotEmpty(this.siteId)){
					this.mapPopupContentDs.addParameter('siteId',this.siteId);
					this.mapPopupContentDs.addParameter('storageLocResForSiteId',true);
				}
			}
		}
	},

	/**
	 * Get map marker popup content by retrived records.
	 * 
	 * @param {storeLocRecords} storage location records for one building, because one building may contain multiple storage location.
	 */
	getMarkerPopupContent: function(storeLocRecords,blId){
		var htmlContent="<table>";
		//create popup title
		htmlContent+=this.getMarkerPopupHtmlContentTitle(blId);
		
		for(var i=0;i<storeLocRecords.length;i++){
			var record=storeLocRecords[i];
			var storeLocId=record.getValue('pt_store_loc.pt_store_loc_id');
			var storeLocName=record.getValue('pt_store_loc.pt_store_loc_name');
			var qtyOnHand=record.getValue('pt_store_loc.vfQtyOnHand');
			var qtyToOrder=record.getValue('pt_store_loc.vfQtyToOrder');
			//get HTML popup content
			if(this.mapDataType==MapDataTypes.PSLP){
				htmlContent+=this.getMarkerPopupHtmlContentForLocPt(blId,storeLocId,storeLocName,qtyOnHand,qtyToOrder);
			}else{
				htmlContent+=this.getMarkerPopupHtmlContentForLoc(blId,storeLocId,storeLocName);
			}
		}
		htmlContent+"</table>";
		
		return htmlContent;
	},
	
	/**
	 * Get map marker popup content title.
	 */
	getMarkerPopupHtmlContentTitle: function(title){
		var htmlContent="";
			htmlContent+="<tr>";
			htmlContent+="<td colspan='2' class='leaflet-map-marker-infowindow-title'>"+title+"</td>"
			htmlContent+="</tr>";
		return htmlContent;
	},
	
	/**
	 * if this.mapDataType='loc_pt',
	 * map content show field Building Code/Storage Location Code/Storage Location Name/Quantity Available/Quantity Understocked
	 * 
	 * @param {blId} Building Code
	 * @param {storeLocId} Storage Location Code
	 * @param {storeLocName} Storage Location Name
	 * @param {qtyOnHand} Quantity Available
	 * @param {qtyToOrder} Quantity Understocked
	 * 
	 */
	getMarkerPopupHtmlContentForLocPt: function(blId,storeLocId,storeLocName,qtyOnHand,qtyToOrder){
		var htmlContent="";
			htmlContent+="<tr>";
			htmlContent+="<td class='leaflet-map-marker-infowindow-text-title'>"+storeLocId+"</td>";
			htmlContent+="<td class='leaflet-map-marker-infowindow-text'>"+storeLocName+"</td>";
			htmlContent+="</tr>";
			htmlContent+="<tr>";
			htmlContent+="<td class='leaflet-map-marker-infowindow-text-title'>"+getMessage('qtyOnHandLabel')+"</td>";
			htmlContent+="<td class='leaflet-map-marker-infowindow-text'>"+qtyOnHand+"</td>";
			htmlContent+="</tr>";
			htmlContent+="<tr>";
			htmlContent+="<td class='leaflet-map-marker-infowindow-text-title'>"+getMessage('qtyToOrderLabel')+"</td>";
			htmlContent+="<td class='leaflet-map-marker-infowindow-text'>"+qtyToOrder+"</td>";
			htmlContent+="</tr>";
			//Add popup action to map marker popup window
			htmlContent+="<tr>";
			htmlContent+='<td><span class="leaflet-popup-action" id="PopupPurchaseAction"><a href="javascript: void(0);" onclick="markerPurchaseActionEvent(\''+storeLocId+'\')">'+getMessage('purchaseLinkTitle')+'</a></span></td>';
			htmlContent+='<td><span class="leaflet-popup-action" id="PopupRequisitionAction"><a href="javascript: void(0);" onclick="markerRequisitionActionEvent(\''+storeLocId+'\')">'+getMessage('requisitionLinkTitle')+'</a></span></td>';
			htmlContent+="</tr>";
			//separator
			htmlContent+="<tr>";
			htmlContent+='<td colspan="2"><p></p></td>';
			htmlContent+="</tr>";
			
		return htmlContent;
	},
	
	/**
	 * get map marker content if not need to show Quantity field.
	 * 
	 * @param {blId} Building Code
	 * @param {storeLocId} Storage Location Code
	 * @param {storeLocName} Storage Locaton Name
	 */
	getMarkerPopupHtmlContentForLoc: function(blId,storeLocId,storeLocName){
		var htmlContent="";
			htmlContent+="<tr>";
			htmlContent+="<td class='leaflet-map-marker-infowindow-text-title'>"+storeLocId+"</td>";
			htmlContent+="<td class='leaflet-map-marker-infowindow-text'>"+storeLocName+"</td>";
			htmlContent+="</tr>";
			//Add popup action to map marker popup window
			htmlContent+="<tr cospan='2'>";
			htmlContent+='<td><span class="leaflet-popup-action" id="PopupAction"><a href="javascript: void(0);" onclick="markerSelectEvent(\''+storeLocId+'\')">'+getMessage('selectLinkTitle')+'</a></span></td>';
			htmlContent+="</tr>";
			
			htmlContent+="<tr>";
			htmlContent+='<td colspan="2"><p></p></td>';
			htmlContent+="</tr>";

		return htmlContent;
	},
	
	
	/**
	 * Show selected location action using callback to show Requisition dialog or Purchase dialog.
	 */
	showSelectLocationAction : function(keyValue,type) {
		View.parameters.callback(keyValue,type);
	}
});

/**
 * Marker action event listener.
 * @param keyValue Key Value
 */
function markerSelectEvent(keyValue){
	//Replace keyValue by inner HTML of pop-up content title.
	var storageLocId=keyValue;
	View.getOpenerView().controllers.get('partByLocCtrl').consoleForm_onClear();
	View.getOpenerView().panels.get('consoleForm').setFieldValue('pt_store_loc.pt_store_loc_id',storageLocId);
	View.getOpenerView().controllers.get('partByLocCtrl').consoleForm_onFilter();
	View.getOpenerView().controllers.get('partByLocCtrl').showPartInventory(storageLocId,"");
	View.getOpenerView().closeDialog();
}
