
/**
 * Call workflow calcEqPtUsePerYr for calculate part equipment use per year
 */
function calcEqPtUsePerYr(){
    try {
        Workflow.callMethod('AbBldgOpsBackgroundData-calculateWorkResourceValues-CalcEqPtUsePerYr');
    } 
    catch (e) {
        Workflow.handleError(e);
    }
    View.panels.get('abBldgopsReportPartsWhereUsedPartGrid').refresh();
	View.alert(getMessage('calculateAlertMessage'));
}
