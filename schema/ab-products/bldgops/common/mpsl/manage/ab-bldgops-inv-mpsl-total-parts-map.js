/**
 * map controller of Total Parts Inventory view.
 * 
 * @author Jia
 * @since 24.1
 */
var mapController = mapCommonCtrl.extend({
	
	//register map view
	mapType: "total_parts_invenoty_map",
	
	//not use default framework API marker popup.
	usePopup:false,
	
	// Part Code passed from opener view.
	partId: "",

	afterInitialDataFetch : function() {
		this.partId=View.parameters["partId"];
		this.showStorageLocationMarkers();
	},
	
	/**
	 * Show storage location map marker.
	 */
	showStorageLocationMarkers : function() {
		//Add parameter or restriction to datasource
		this.setPtStoreLocRestriction();
		//create map marker properties
		this.createPtStoreLocMarkerProperty();
		
		this.mapControl.showMarkers('mapMarkerDs', null);
		this.mapControl.showMarkerLegend();
	},

	/**
	 * Create Part storage location marker property by part store location table.
	 * 
	 * @param {ptStoreLocDataSource} Part storage location datasource
	 * @param {ptStoreLocKeyFields} Part storage location primary key fields
	 * @param {ptStoreLocGeometryFields} Part storage location geometry fields
	 * @param {ptStoreLocTitleField} Part storage location title fields
	 * @param {ptStoreLocContentFields} Part storage location content fields
	 */
	createPtStoreLocMarkerProperty: function(){
		//Create Part Store Location Marker
		// create property markers
        var mapMarkerDataSource = 'mapMarkerDs';
        var mapMarkerKeyFields = ['bl.bl_id'];
        var mapMarkerGeometryFields = ['bl.lon', 'bl.lat'];
        var mapMarkerTitleField = 'bl.bl_id';
        var mapMarkerContentFields = [ 'bl.bl_id','bl.lon','bl.lat','bl.markerColorType'];

		//Get locator color
		var locatorColor=this.getPtStoreLocatorColor();
		//Get thematic Buckets 
		var thematicBuckets=this.getPtStoreThematicBuckets();
		var mapMarkerProperties=null;
		
		colorbrewer['FP-YRG']={3:[]};
		colorbrewer['FP-YRG'][3]=locatorColor;
		var isUsePopup=this.usePopup;
		mapMarkerProperties = {
	        radius: 10,
	        //stroke properties
			stroke: true,
		    strokeColor: '#fff',
		    strokeWeight: 1.0,
	        // required for thematic markers
			renderer: 'thematic-class-breaks',
			thematicField: 'bl.markerColorType',
			thematicClassBreaks: thematicBuckets,
			colorBrewerClass: 'FP-YRG',
			// enable marker clusters
			useClusters: false,
			usePopup: isUsePopup
	   };  

        this.mapControl.createMarkers(
        	mapMarkerDataSource, 
        	mapMarkerKeyFields,
        	mapMarkerGeometryFields,
        	mapMarkerTitleField,
        	mapMarkerContentFields,
        	mapMarkerProperties
        );
	},
	
	/**
	 * Get part storage location 
	 */
	setPtStoreLocRestriction: function(){
		this.mapMarkerDs.addParameter('partId',this.partId);
	},

	/**
	 * Get part storage location color by console record.
	 * 
	 * @param consoleRecord Console record
	 * @return locatorsColor Locator color
	 */
	getPtStoreLocatorColor: function(){
		var locatorsColor=[MarkerColors.GREEN,MarkerColors.RED,MarkerColors.YELLOW];
		return locatorsColor;
	},
	
	/**
	 * get part storage location thematic buckets by console record.
	 * 
	 * @param {consoleRecord} Console record
	 * @return {thematicBuckets} Thematic buckets
	 */
	getPtStoreThematicBuckets: function(consoleRecord){
		var thematicBuckets=[1.5,2.5];
		return thematicBuckets;
	},
	
	/**
	 * The behavior of the Select link in the pop-up will depend on the filter criteria. 
	 * (1). If the user filtered on a Part Code, then the Select link will close the Find Parts window and will populate the Add Part form with the Part Code and the Storage Location Code. 
	 * 	 If the user had also filtered on a quantity available, that value will also be copied to the Add Part form.
	 * (2). If the user had not filtered on a Part Code, then the Select link will switch the view back to the Parts Inventory tab, and also filter on the selected Storage Location Code.
	 *   The user can the choose one of the Parts available in that storage location from the grid.
	 *   
	 * @param {keyValue} map marker key value
	 * @param {type} Requisition action or Purchase action
	 *
	 */
	showSelectLocationAction : function(keyValue,type) {
		View.parameters.callback(keyValue,type);
	},
	
	/**
	 * @override
	 * Click map marker event handler.
	 * 
	 * @param {assetId} Building Code
	 * @param {featrue} Map marker feature
	 * 
	 */
	onMarkerClick: function(assetId, feature){
		this.mapControl.clearSelectedMarkers();
		this.displayMarkerPopupContent(assetId);
	},
	
	/**
	 * Display marker popup content when click map marker.
	 * 
	 * @param blId Building Code
	 * @returns
	 */
	displayMarkerPopupContent: function(blId){
		var blId=blId;
		
		var storeLocRes=new Ab.view.Restriction();
			storeLocRes.addClause('pt_store_loc.bl_id',blId,'=');
		//get all store	location in the building.
		this.mapPopupContentDs.addParameter('partId',this.partId);
		var storeLocRecords=this.mapPopupContentDs.getRecords(storeLocRes);
		
		var htmlContent="<table>";
			//create popup title
			htmlContent+=this.getMarkerPopupHtmlContentTitle(blId);
		for(var i=0;i<storeLocRecords.length;i++){
			var record=storeLocRecords[i];
			var storeLocId=record.getValue('pt_store_loc.pt_store_loc_id');
			var storeLocName=record.getValue('pt_store_loc.pt_store_loc_name');
			var qtyOnHand=record.getValue('pt_store_loc.vfQtyOnHand');
			var qtyToOrder=record.getValue('pt_store_loc.vfQtyToOrder');
			//get HTML popup content
			htmlContent+=this.getMarkerPopupHtmlContent(blId,storeLocId,storeLocName,qtyOnHand,qtyToOrder);
		}
		htmlContent+"</table>"
		this.mapControl.showMarkerInfoWindow(htmlContent);
	},
	
	/**
	 * Get map marker popup content title.
	 */
	getMarkerPopupHtmlContentTitle: function(title){
		var htmlContent="";
			htmlContent+="<tr>";
			htmlContent+="<td colspan='2' class='leaflet-map-marker-infowindow-title'>"+title+"</td>"
			htmlContent+="</tr>";
		return htmlContent;
	},
	
	/**
	 * Get map maker popup content.
	 * 
	 */
	getMarkerPopupHtmlContent: function(blId,storeLocId,storeLocName,qtyOnHand,qtyToOrder){
		var htmlContent="";
			htmlContent+="<tr>";
			htmlContent+="<td class='leaflet-map-marker-infowindow-text-title'>"+storeLocId+"</td>";
			htmlContent+="<td class='leaflet-map-marker-infowindow-text'>"+storeLocName+"</td>";
			htmlContent+="</tr>";
			htmlContent+="<tr>";
			htmlContent+="<td class='leaflet-map-marker-infowindow-text-title'>"+getMessage('qtyOnHandLabel')+"</td>";
			htmlContent+="<td class='leaflet-map-marker-infowindow-text'>"+qtyOnHand+"</td>";
			htmlContent+="</tr>";
			htmlContent+="<tr>";
			htmlContent+="<td class='leaflet-map-marker-infowindow-text-title'>"+getMessage('qtyToOrderLabel')+"</td>";
			htmlContent+="<td class='leaflet-map-marker-infowindow-text'>"+qtyToOrder+"</td>";
			htmlContent+="</tr>";
			//Add popup action to map marker popup window
			htmlContent+="<tr>";
			htmlContent+='<td><span class="leaflet-popup-action" id="PopupPurchaseAction"><a href="javascript: void(0);" onclick="markerPurchaseActionEvent(\''+storeLocId+'\')">'+getMessage('purchaseLinkTitle')+'</a></span></td>';
			htmlContent+='<td><span class="leaflet-popup-action" id="PopupRequisitionAction"><a href="javascript: void(0);" onclick="markerRequisitionActionEvent(\''+storeLocId+'\')">'+getMessage('requisitionLinkTitle')+'</a></span></td>';
			htmlContent+="</tr>";
			
			htmlContent+="<tr>";
			htmlContent+='<td colspan="2"><p></p></td>';
			htmlContent+="</tr>";
			
		return htmlContent;
	}
});