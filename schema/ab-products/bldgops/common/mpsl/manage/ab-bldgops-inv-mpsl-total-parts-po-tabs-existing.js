var toExistingPoSingleCtrl=View.createController('toExistingPoSingleCtrl',{
	//storage location code
	partStoreLocationCode: "",
	//part code
	partId: "",
	//Order Value of selected purchase order.
	selectedRowOrderValue: 0,
	
	afterInitialDataFetch: function(){
		var openerView=View.getOpenerView();
		//register current view controller to parent tabs controller
		this.registerTabCtrl(openerView);
		//get opener view parameters
		this.partStoreLocationCode=openerView.parameters['partStoreCode'];
		this.partId=openerView.parameters['partId'];
		//refresh purchase order list panel
		this.refreshPurchaseOrderList();
		//set form default value after view load by part code and storage location code.
		this.setValueByPartCodeAndStoreLocInfo();
		
		if(valueExistsNotEmpty(this.partStoreLocationCode)&&valueExistsNotEmpty(this.partId)){
			this.setPartRecordByStorageLocationIdAndPartId(this.partStoreLocationCode,this.partId);
		}
		//if purchase order grid rows length is 0, then hidden form panel.
		if(this.getExistingPurchaseOrderListRowLength()==0){
			this.purchaseOrderCreateForm.show(false);
		}else{
			this.afterTabSelect(openerView.controllers.items[0]);
		}
	},
	
	/**
	 * register current tab view to parent tabs controller.
	 */
	registerTabCtrl: function(openerView){
		var tabsCtrl=openerView.controllers.items[0];
		tabsCtrl.tabCtrls["addToExistingPurchaseOrderTab"]=this;
	},
	
	/**
	 * set field value to parent tabs controller before tab selected.
	 * @param {parentTabCtrl} parent Tabs controller.
	 */
	beforeTabSelect: function(parentTabsCtrl){
		var canChange=true;
        if(this.getExistingPurchaseOrderListRowLength()>0){
        	parentTabsCtrl.transQty=this.purchaseOrderCreateForm.getFieldValue('po_line.quantity');
        	parentTabsCtrl.unitCost=this.purchaseOrderCreateForm.getFieldValue('po_line.unit_cost');
        	parentTabsCtrl.comments=this.purchaseOrderCreateForm.getFieldValue('po.comments');
        }
        
        return canChange;
	},
	
	/**
	 * reset field value after tab selected.
	 * @param {parentTabsCtrls} parent tabs controller.
	 */
	afterTabSelect: function(parentTabsCtrl){
		this.resetFieldsValueAfterTabChange(parentTabsCtrl.transQty,parentTabsCtrl.unitCost,parentTabsCtrl.comments);
	},
	
	/**
	 * KB#3051967
	 * Refilling items is not necessary whatever create new requisition or add to exsiting requisition
	 */
	resetFieldsValueAfterTabChange: function(transQty,unitCost,comments){
		if(this.getExistingPurchaseOrderListRowLength()>0){
			var ds=this.purchaseOrderCreateForm.getDataSource();
			var unitCostFiedDef=this.purchaseOrderCreateForm.fields.get('po_line.unit_cost').fieldDef;
			this.purchaseOrderCreateForm.setFieldValue('po_line.quantity',ds.formatValue('po_line.quantity',transQty,true));
			this.purchaseOrderCreateForm.setFieldValue('po_line.unit_cost',unitCostFiedDef.formatValue(unitCost,true));
			this.purchaseOrderCreateForm.setFieldValue('po.comments',comments);
		}
		calculateTotalCost()
	},
	
	/**
	 * get grid row length of purchase order grid panel.
	 */
	getExistingPurchaseOrderListRowLength: function(){
		return this.existsPurchaseOrdersList.gridRows.length;
	},
	
	/**
	 * Set value to supply requisition edit form by part code and part storage location code
	 */
	setValueByPartCodeAndStoreLocInfo: function(){
		//Clear supply requisition form
		this.purchaseOrderCreateForm.clear();
		
		this.purchaseOrderCreateForm.setFieldValue('po_line.partCode',this.partId);
		this.purchaseOrderCreateForm.setFieldValue('po_line.qtyUnderstocked',0);
		this.purchaseOrderCreateForm.setFieldValue('po_line.quantity',0);
		this.purchaseOrderCreateForm.setFieldValue('po_line.unit_cost',0);
		this.purchaseOrderCreateForm.setFieldValue('po_line.totalCost',0);
		this.purchaseOrderCreateForm.setFieldValue('po_line.updatedtotalCost',0);
		
		this.setPartRecordByStorageLocationIdAndPartId(this.partStoreLocationCode,this.partId);
		
	},
	
	/**
	 * refresh purchase order list panel by storage location code and part code.
	 */
	refreshPurchaseOrderList: function(){
		var partStorageLocation=this.partStoreLocationCode;
		var partId=this.partId;
		if(valueExistsNotEmpty(partStorageLocation)&&valueExistsNotEmpty(partId)){
			this.existsPurchaseOrdersList.addParameter('storageLocId',partStorageLocation);
			this.existsPurchaseOrdersList.addParameter('partCode',partId);
			this.existsPurchaseOrdersList.addParameter('hasPartCodeAndStorageLocId',true);
			this.existsPurchaseOrdersList.addParameter('notHasPartCodeAndStorageLocId',false);
		}else{
			this.existsPurchaseOrdersList.addParameter('hasPartCodeAndStorageLocId',false);
			this.existsPurchaseOrdersList.addParameter('notHasPartCodeAndStorageLocId',true);
		}
		this.existsPurchaseOrdersList.refresh();
	},
	
	/**
	 * Submit purchase order information
	 */
	purchaseOrderCreateForm_onSubmit: function(){
		var selectedRowIndex=this.existsPurchaseOrdersList.selectedRowIndex;
		if(selectedRowIndex==-1){
			View.alert(getMessage('mustSelectARowMsg'));
			return;
		}
		//validate form field value
		var validateResult=this.purchaseOrderCreateFormValidate();
		if(!validateResult){
			return;
		}
	
		var selectedRowRecord=this.existsPurchaseOrdersList.gridRows.get(selectedRowIndex).getRecord();
		var poId=selectedRowRecord.getValue('po.po_id');
			poId=parseInt(poId);
		var vnId=selectedRowRecord.getValue('po.vn_id');
		
		//get field value from form panel.
		var receivingLoc=this.purchaseOrderCreateForm.getFieldValue('po.receiving_location');
		var catNo=this.purchaseOrderCreateForm.getFieldValue('po_line.catno');
		var tansQty=this.purchaseOrderCreateForm.getFieldValue('po_line.quantity');
		var unitCost=this.purchaseOrderCreateForm.getFieldValue('po_line.unit_cost');
		var comments=this.purchaseOrderCreateForm.getFieldValue('po.comments');
		
		//prepare workflow rule parameters.
		var poLineRecords = [];
		poLineRecords[0]=new Object();
		poLineRecords[0]['po_line.catno']=catNo;
		poLineRecords[0]['po_line.quantity']=tansQty;
		poLineRecords[0]['po_line.unit_cost']=unitCost;
		poLineRecords[0]['po_line.part_id']=this.partId;
		poLineRecords[0]['po_line.description']=comments;
		try{
			var result=Workflow.callMethod('AbBldgOpsBackgroundData-BldgopsPartInventoryService-addToExistingPurchaseOrder',poId,vnId,poLineRecords);
			if(result.code=="executed"){
				View.getOpenerView().getOpenerView().panels.get('abBldgopsReportPartsInvertoryStorageLocationGrid').refresh();
				View.getOpenerView().getOpenerView().closeDialog();
			}
		}catch(e){
			Workflow.handleError(e);
		}
	},
	
	/**
	 * validate form field value
	 */
	purchaseOrderCreateFormValidate: function(){
		var canSave=this.purchaseOrderCreateForm.canSave();
		if(canSave){
			var tansQty=this.purchaseOrderCreateForm.getFieldValue('po_line.quantity');
			var unitCost=this.purchaseOrderCreateForm.getFieldValue('po_line.unit_cost');
			//transfer quantity must be greater than 0
			tansQty=parseFloat(tansQty);
			if(tansQty<1){
				View.alert(getMessage("transactionQuantityGreaterThanZeroMsg"));
				return false;
			}
			//unit cost must be greater than 0
			var unitCost=parseFloat(unitCost);
			if(unitCost<=0){
				View.alert(getMessage("unitCostGreaterThanZeroMsg"));
				return false;
			}
			
			return true;
		}else{
			return false;
		}
		
	},
	
	/**
	 * purchase order list panel afterRefresh event handler.
	 */
	existsPurchaseOrdersList_afterRefresh: function(){
		var controllerThis=this;
		this.existsPurchaseOrdersList.gridRows.each(function(row){
			var poCheckRadio=row.actions.get('poCheckRadio');
			var index=row.record.index;
			var radioElement=$('existsPurchaseOrdersList_row'+index+'_poCheckRadio');
			radioElement.setAttribute("onchange", "javascript:onClickRadioButton("+index+")");
		});
		//reset the grid footer message after grid refresh.
		this.existsPurchaseOrdersList.setFooter(getMessage('noMoreRecordToDisplayMsg'));
	},

	/**
	 * Get part record by storage location id and part id.
	 * @param {storageLocationId} Storage Location Id.
	 * @param {partId} Part code.
	 */
	setPartRecordByStorageLocationIdAndPartId: function(storageLocationId,partId){
		var ptStorageLocationDs=View.dataSources.get('dialogCreatePurchaseOrderPtDS');
		var res=new Ab.view.Restriction();
		res.addClause('pt_store_loc_pt.pt_store_loc_id',storageLocationId,'=');
		res.addClause('pt_store_loc_pt.part_id',partId,'=');
		var ptRecords=ptStorageLocationDs.getRecords(res);
		
		if(ptRecords.length>0){
			var ptRecord=ptRecords[0];
			this.purchaseOrderCreateForm.setFieldValue('po_line.partDescription',ptRecord.getValue('pt.description'));
			var qtyOnOrder=ptRecord.getValue('pt_store_loc_pt.qty_to_order');
			var unitCost=ptRecord.getValue('pt_store_loc_pt.cost_unit_std');
			if(!valueExistsNotEmpty(qtyOnOrder)){
				qtyOnOrder=0;
			}
			var unitCostFiedDef=this.purchaseOrderCreateForm.fields.get('po_line.unit_cost').fieldDef;
			this.purchaseOrderCreateForm.setFieldValue('po_line.qtyUnderstocked',ptStorageLocationDs.formatValue('pt_store_loc_pt.qty_on_hand',qtyOnOrder,true));
			this.purchaseOrderCreateForm.setFieldValue('po_line.unit_cost',unitCostFiedDef.formatValue(unitCost,true));
		}
	},
	
	/**
	 * Click cancel button to close the dialog view form
	 */
	purchaseOrderCreateForm_onCancel: function(){
		View.getOpenerView().getOpenerView().closeDialog();
	},
	
	/**
	 * get catlog number from pv table by vendor code and part code.
	 * @param {vnId} vendor code
	 * @partId {partId} part code
	 */
	getCatlogNoByVnIdAndPartId: function(vnId,partId){
		var restriction = new Ab.view.Restriction();
		restriction.addClause("pv.vn_id", vnId, '=');
		restriction.addClause("pv.part_id", partId, '=');
   		var parameters = {
   			tableName: "pv",
   			fieldNames: "[pv.vn_id,pv.part_id,pv.vn_pt_num]",
   			restriction: toJSON(restriction) 
   		};     		
     	var result = Workflow.runRuleAndReturnResult('AbCommonResources-getDataRecords', parameters); 
     	var catNo="";
     	if (result.code == 'executed') {
     		if (result.data.records.length == 1){
     			if(valueExistsNotEmpty(result.data.records[0]['pv.vn_pt_num'])){
     				catNo=result.data.records[0]['pv.vn_pt_num'];
     			}else{
     				catNo=partId;
     			}
     			
     		}
     	}
     	
     	return catNo;
	},
	/**
	 * calculate total cost by Transfer Quantity and Unit Cost field value after their content is changed.
	 * @param {quantity} Transfer Quantity.
	 * @param {unitCost} Unit Cost
	 */
	calculateTotalCost: function(quntity,unitCost){
		var totalCost=0;
		
		if(valueExistsNotEmpty(quntity)){
			quntity=parseFloat(quntity);
		}else{
			quntity=0;
		}
		
		if(valueExistsNotEmpty(unitCost)){
			unitCost=parseFloat(unitCost);
		}else{
			unitCost=0;
		}
		
		totalCost=quntity*unitCost;
		
		return totalCost.toFixed(2);
	}
});

/**
 * Click event handler of radio button in Purchase Order list panel.
 * @param {rowIndex} selected row index.
 */
function onClickRadioButton(rowIndex){
	var vnId=View.panels.get('existsPurchaseOrdersList').gridRows.get(rowIndex).getRecord().getValue('po.vn_id');
	var unitCostFieldDef=View.panels.get('purchaseOrderCreateForm').fields.get('po_line.unit_cost').fieldDef;
	var ptId=View.panels.get('purchaseOrderCreateForm').getFieldValue('po_line.partCode');
	var catNo=View.controllers.get('toExistingPoSingleCtrl').getCatlogNoByVnIdAndPartId(vnId,ptId);
	if(!valueExistsNotEmpty(catNo)){
		catNo=ptId;
	}
	View.panels.get('purchaseOrderCreateForm').setFieldValue('po_line.catno',catNo);
	//calculate updated total 
	var orderValue=View.panels.get('existsPurchaseOrdersList').gridRows.get(rowIndex).getRecord().getValue('po.orderValue');
	var totalCost=View.panels.get('purchaseOrderCreateForm').getFieldValue('po_line.totalCost');
	if(valueExistsNotEmpty(orderValue)){
		orderValue=parseFloat(orderValue);
	}else{
		orderValue=0;
	}
	View.controllers.get('toExistingPoSingleCtrl').selectedRowOrderValue=orderValue;
	if(valueExistsNotEmpty(totalCost)){
		totalCost=parseFloat(totalCost);
	}else{
		totalCost=0;
	}
	var updatedTotalCost=(orderValue+totalCost).toFixed(2);
	View.panels.get('purchaseOrderCreateForm').setFieldValue('po_line.updatedtotalCost',unitCostFieldDef.formatValue(updatedTotalCost,true));
}

/**
 * calculate total cost value after quantity or unit cost field change.
 */
function calculateTotalCost(){
	var panel=View.panels.get('purchaseOrderCreateForm');
	var ds=panel.getDataSource();
	var quanity=panel.getFieldValue('po_line.quantity');
	var unitCost=panel.getFieldValue('po_line.unit_cost');
	var totalCost=View.controllers.get('toExistingPoSingleCtrl').calculateTotalCost(quanity,unitCost);
	var unitCostFieldDef=panel.fields.get('po_line.totalCost').fieldDef;
	panel.setFieldValue('po_line.totalCost',unitCostFieldDef.formatValue(totalCost,true));
	
	var selectedOrderValue=View.controllers.get('toExistingPoSingleCtrl').selectedRowOrderValue;
	
	var updatedTotalCost=(parseFloat(totalCost)+parseFloat(selectedOrderValue)).toFixed(2);
	
	panel.setFieldValue('po_line.updatedtotalCost',unitCostFieldDef.formatValue(updatedTotalCost,true));
}