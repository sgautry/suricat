var toExistingReqSingleCtrl=View.createController('toExistingReqSingleCtrl',{
	partStoreLocationCode: "",
	partId: "",
	openerTabView: null,
	
	afterInitialDataFetch: function(){
		var openerView=View.getOpenerView();
		this.openerTabView=openerView;
		//Register current view to parent tabs view.
		this.registerTabController(openerView);
		this.partId=openerView.parameters['partId'];
		
		this.afterTabSelect(openerView.controllers.items[0]);
	},
	
	/**
	 * register current view controller to parent tabs controller.
	 * @param {openerView} Current opener view.
	 */
	registerTabController: function(openerView){
		var tabsCtrl=openerView.controllers.items[0];
		tabsCtrl.tabCtrls["addToExistingSupplyReqTab"]=this;
	},
	/**
	 * set crrent view field value to parent tabs controller before tab select.
	 * @param {parentTabsCtrl} Parent Tabs controller.
	 */
	beforeTabSelect: function(parentTabsCtrl){
		canChange=true;
		
		if(this.getExistingSupplyRequisitionListRowLength()>0){
			parentTabsCtrl.transQty=this.existsSupplyReqTransForm.getFieldValue('it.trans_quantity');
    		parentTabsCtrl.comments=this.existsSupplyReqTransForm.getFieldValue('it.comments');
        }
	     
	    return canChange;
	},
	/**
	 * re-set field value after tab select.
	 * @param {parentTabsCtrl} Parent Tabs controller.
	 */
	afterTabSelect: function(parentTabsCtrl){
		this.reloadReqListByStoreLocId(parentTabsCtrl.storagelocId);
		this.resetFieldsValueAfterTabChange(parentTabsCtrl.transQty,parentTabsCtrl.comments);
	},
	
	/**
	 * re-filter existing supply requisition grid by From Storage Location field value of first tab. 
	 */
	reloadReqListByStoreLocId: function(storageLocId){
		//set storage location code and part code.
		this.partStoreLocationCode=storageLocId;
		//only display all supply requisition of selected From Storage Location.
		this.exsitsSupplyReqListPanel.addParameter('fromStorageLocation',storageLocId);
		this.exsitsSupplyReqListPanel.addParameter('hasFromStorageLocation',true);
		this.exsitsSupplyReqListPanel.refresh();
		//If supply requisition grid row length is 0, then hidden the form panel.
		if(this.getExistingSupplyRequisitionListRowLength()==0){
			this.existsSupplyReqTransForm.show(false);
		}else{
			this.existsSupplyReqTransForm.show(true);
			this.setValueByPartCodeAndStoreLocInfo(storageLocId,this.partId);
		}
	},
	/**
	 * KB#3051967
	 * Refilling items is not necessary whatever create new requisition or add to exsiting requisition
	 */
	resetFieldsValueAfterTabChange: function(transQty,comments){ 
		if(this.getExistingSupplyRequisitionListRowLength()>0){
			var ds=this.existsSupplyReqTransForm.getDataSource();
			this.existsSupplyReqTransForm.setFieldValue('it.trans_quantity',ds.formatValue('it.trans_quantity',transQty,true));
			this.existsSupplyReqTransForm.setFieldValue('it.comments',comments);
		}
	},
	
	/**
	 * Set value to supply requisition edit form by part code and part storage location code.
	 */
	setValueByPartCodeAndStoreLocInfo: function(storeLocId,partId){
		//Clear supply requisition form
		this.existsSupplyReqTransForm.clear();
		
		//Set value to create supply requisition panel
		var supplyReqPtDs=View.dataSources.get('dialogCreateSupplyReqPtDS');
		var res=new Ab.view.Restriction();
		res.addClause('pt_store_loc_pt.pt_store_loc_id',storeLocId,'=');
		res.addClause('pt_store_loc_pt.part_id',partId,'=');
		
		var supplyRecord=supplyReqPtDs.getRecord(res);
		if(!supplyRecord.isNew){
			var description=supplyRecord.getValue('pt.description');
			var qtyOnHand=supplyRecord.getValue('pt_store_loc_pt.qty_on_hand');

			this.existsSupplyReqTransForm.setFieldValue('it.part_id',partId);
			this.existsSupplyReqTransForm.setFieldValue('pt.description',description);
			this.existsSupplyReqTransForm.setFieldValue('pt.qty_on_hand',supplyReqPtDs.formatValue('pt_store_loc_pt.qty_on_hand',qtyOnHand,true));
		}
	},
	
	/**
	 * get grid row length of supply requisition grid panel.
	 */
	getExistingSupplyRequisitionListRowLength: function(){
		return this.exsitsSupplyReqListPanel.gridRows.length;
	},
	
	/**
	 * do submit.
	 */
	existsSupplyReqTransForm_onSubmit: function(){
		var selectedSupplyReqRowIndex=this.exsitsSupplyReqListPanel.selectedRowIndex;
		//must select at least one row when submit
		if(selectedSupplyReqRowIndex==-1){
			View.alert(getMessage('mustSelectAtLeastOneSupplyReqRowMsg'));
			return;
		}
		
		//validate form field value
		var validateResult=this.existsSupplyReqTransFormValidate();
		if(!validateResult){
			return;
		}
		
		//get field value form supply requisition list panel
		var selectedRowRecord=this.exsitsSupplyReqListPanel.gridRows.get(selectedSupplyReqRowIndex).getRecord();
		var supplyReqId=selectedRowRecord.getValue('supply_req.supply_req_id');
		var tansQty=this.existsSupplyReqTransForm.getFieldValue('it.trans_quantity');
		var qtyAvailable=this.existsSupplyReqTransForm.getFieldValue('pt.qty_on_hand');
		var comments=this.existsSupplyReqTransForm.getFieldValue('it.comments');
		
		//prepare workflow rule parameters.
		var itRecords = [];
			itRecords[0]=new Object();
			itRecords[0]['it.part_id']=this.partId;
			itRecords[0]['it.trans_quantity']=tansQty;
			itRecords[0]['it.comments']=comments;
		
		//call submit workflow rule.
		try{
			var result=Workflow.callMethod('AbBldgOpsBackgroundData-BldgopsPartInventoryService-addPartsToExistingSupplyReq',supplyReqId,itRecords);
			if(result.code=="executed"){
				//refresh view panels after workflow rule finish
				View.getOpenerView().getOpenerView().panels.get('abBldgopsReportPartsInvertoryStorageLocationGrid').refresh();
				View.getOpenerView().getOpenerView().closeDialog();
			}
		}catch(e){
			Workflow.handleError(e);
		}
	},
	
	/**
	 * validate 'existsSupplyReqTransForm' form field value.
	 */
	existsSupplyReqTransFormValidate: function(){
		var canSave=this.existsSupplyReqTransForm.canSave();
		if(canSave){
			var tansQty=this.existsSupplyReqTransForm.getFieldValue('it.trans_quantity');
			var qtyAvailable=this.existsSupplyReqTransForm.getFieldValue('pt.qty_on_hand');
			if(valueExistsNotEmpty(qtyAvailable)){
				qtyAvailable=parseFloat(qtyAvailable);
			}else{
				qtyAvailable=0;
			}
			//Check Transfer Quantity field is number type and greater than 0
			tansQty=parseFloat(tansQty);
			if(tansQty<=0){
				View.alert(getMessage("transactionQuantityGreaterThanZeroMsg"));
				return false;
			}else{
				if(tansQty>qtyAvailable){
					View.alert(getMessage("doNotTransferMoreThanAvailableMsg"));
					return false;
				}
			}
			return true;
		}else{
			return false;
		}
	},
	
	/**
	 * Click cancel button, and close dialog view
	 */
	existsSupplyReqTransForm_onCancel: function(){
		View.getOpenerView().getOpenerView().closeDialog();
	},

	/**
	 * reset the grid footer message after grid refresh.
	 */
	exsitsSupplyReqListPanel_afterRefresh: function(){
		this.exsitsSupplyReqListPanel.setFooter(getMessage('noMoreRecordToDisplayMsg'));
	}
	
});