var partByLocCtrl=View.createController('partByLocCtrl',{
	//Get console form record when click Show button in the console form
	consoleRecord: null,
	//Get understocked field's check status.
	isUnderstockedChecked: false,
	//Get part storage location code when click part storage location list rows link.
	ptStorageLocationcode: "",
	//Selected multiple parts list.
	selectedPartCodeArray: [],
	
	afterInitialDataFetch: function(){
		//Disable Quantity Avaliable default value
		var qtyOnHandFieldElement=this.consoleForm.fields.get('pt_store_loc_pt.qty_on_hand');
		qtyOnHandFieldElement.fieldDef.defaultValue='';
	},
	
	/**
	 * Event handler when click Show button in console form
	 */
	consoleForm_onFilter: function(){
		//Get console form record
		this.consoleRecord=this.consoleForm.getRecord();
		
		var partId=this.consoleForm.getFieldValue('pt.part_id');
		var partClass=this.consoleForm.getFieldValue('pt.class');
		var partStoreLocId=this.consoleForm.getFieldValue('pt_store_loc.pt_store_loc_id');
		var siteId=this.consoleForm.getFieldValue('pt_store_loc.site_id');
		var quantityAvaliable=this.consoleForm.getFieldValue('pt_store_loc_pt.qty_on_hand');
		var vnCode=this.consoleForm.getFieldValue('pt_store_loc_pt.vnCode');
		
		this.isUnderstockedChecked=$('understockedCheckbox').checked;
		
		//disable the console restriction switch.
		this.partStorageLocationListPanel.addParameter('hasPartId',false);
		this.partStorageLocationListPanel.addParameter('hasPartClass',false);
		this.partStorageLocationListPanel.addParameter('hasStoreLocId',false);
		this.partStorageLocationListPanel.addParameter('hasSiteId',false);
		this.partStorageLocationListPanel.addParameter('hasQuantityAvaliable',false);
		this.partStorageLocationListPanel.addParameter('hasVnCode',false);
		this.partStorageLocationListPanel.addParameter('hasCheckedUnderstockButton',false);

		//Set parameter by console form field value.
		if(valueExistsNotEmpty(partId)){
			this.partStorageLocationListPanel.addParameter('partId',partId);
			this.partStorageLocationListPanel.addParameter('hasPartId',true);
		}
		if(valueExistsNotEmpty(partClass)){
			this.partStorageLocationListPanel.addParameter('partClass',partClass);
			this.partStorageLocationListPanel.addParameter('hasPartClass',true);
		}
		if(valueExistsNotEmpty(partStoreLocId)){
			this.partStorageLocationListPanel.addParameter('storeLocId',partStoreLocId);
			this.partStorageLocationListPanel.addParameter('hasStoreLocId',true);
		}
		if(valueExistsNotEmpty(siteId)){
			this.partStorageLocationListPanel.addParameter('siteId',siteId);
			this.partStorageLocationListPanel.addParameter('hasSiteId',true);
		}
		if(valueExistsNotEmpty(quantityAvaliable)){
			this.partStorageLocationListPanel.addParameter('quantityAvaliable',quantityAvaliable);
			this.partStorageLocationListPanel.addParameter('hasQuantityAvaliable',true);
		}
		if(valueExistsNotEmpty(vnCode)){
			this.partStorageLocationListPanel.addParameter('vnCode',vnCode);
			this.partStorageLocationListPanel.addParameter('hasVnCode',true);
		}
		//If understock checkbox is checked,then select storage location where Quantity Understocked >0
		if(this.isUnderstockedChecked){
			this.partStorageLocationListPanel.addParameter('hasCheckedUnderstockButton',true);
		}

		this.partInventoryListPanel.show(false);
		this.partStorageLocationListPanel.refresh();
		
	},
	
	/**
	 * Show parts inventory list by storage location
	 */
	showPartsInventoryByStorageLocation: function(){
		var selectRowIndex=this.partStorageLocationListPanel.selectedRowIndex;
		var selectRowRecord=this.partStorageLocationListPanel.gridRows.get(selectRowIndex).getRecord();
		var ptStoreLocId=selectRowRecord.getValue('pt_store_loc.pt_store_loc_id');
		var vnCode=this.consoleForm.getFieldValue('pt_store_loc_pt.vnCode');
		
		this.showPartInventory(ptStoreLocId,vnCode);
	},
	
	/**
	 * Show part by storage location and vendor.
	 * @param ptStoreLocId Storage Location code
	 * @param vnCode Vendor Code.
	 */
	showPartInventory: function(ptStoreLocId,vnCode){
		//Set part storage location code to controller variable.
		this.ptStorageLocationcode=ptStoreLocId;
		var ptRes=new Ab.view.Restriction();
		//Get field from  console form record
		var partId="";//part code
		var partClass="";//part classification
		var quantityAvaliable=""; // quantity available
		if(valueExistsNotEmpty(this.consoleRecord)){
			partId=this.consoleRecord.getValue('pt.part_id');
			partClass=this.consoleRecord.getValue('pt.class');
			quantityAvaliable=this.consoleRecord.getValue('pt_store_loc_pt.qty_on_hand');
		}
		
		//turn off the restriction named filter switch
		this.partInventoryListPanel.addParameter('hasPartId',false);
		this.partInventoryListPanel.addParameter('hasStoreLocId',false);
		this.partInventoryListPanel.addParameter('hasPartClass',false);
		this.partInventoryListPanel.addParameter('hasQuantityAvaliable',false);
		this.partInventoryListPanel.addParameter('hasVnCode',false);
		this.partInventoryListPanel.addParameter('hasCheckedUnderstockButton',false);
		
		//Set parameter by console form field value
		if(valueExistsNotEmpty(ptStoreLocId)){
			this.partInventoryListPanel.addParameter('storeLocId',ptStoreLocId);
			this.partInventoryListPanel.addParameter('hasStoreLocId',true);
		}
		if(valueExistsNotEmpty(partId)){
			this.partInventoryListPanel.addParameter('partId',partId);
			this.partInventoryListPanel.addParameter('hasPartId',true);
		}
		if(valueExistsNotEmpty(partClass)){
			this.partInventoryListPanel.addParameter('partClass',partClass);
			this.partInventoryListPanel.addParameter('hasPartClass',true);
		}
		if(valueExistsNotEmpty(quantityAvaliable)){
			this.partInventoryListPanel.addParameter('quantityAvaliable',quantityAvaliable);
			this.partInventoryListPanel.addParameter('hasQuantityAvaliable',true);
		}
		if(valueExistsNotEmpty(vnCode)){
			this.partInventoryListPanel.addParameter('vnCode',vnCode);
			this.partInventoryListPanel.addParameter('hasVnCode',true);
		}
		if(this.isUnderstockedChecked){
			this.partInventoryListPanel.addParameter('hasCheckedUnderstockButton',true);
		}
		this.partInventoryListPanel.show(true);
		this.partInventoryListPanel.refresh();
		//Set part inventory list panel's title by part storage location code
		this.partInventoryListPanel.setTitle(String.format(getMessage('partInventoryTitle'),ptStoreLocId));
	},
	
	partInventoryListPanel_afterRefresh: function(){
		this.partInventoryListPanel.setTitle(String.format(getMessage('partInventoryTitle'),this.ptStorageLocationcode));
	},
	
	/**
	 * Clear console form
	 */
	consoleForm_onClear: function(){
		this.consoleForm.clear();
		$('understockedCheckbox').checked=false;
		//filter should automatically run when the user clicks the Clear button
		this.consoleForm_onFilter();
	},
	
	/**
	 * Click Update physical count button
	 */
	partInventoryListPanel_onUpdatePhsicalCount: function(){
		var selectRowRecords=this.partInventoryListPanel.getSelectedRecords();
		
		if(selectRowRecords.length<1){
			View.alert(getMessage('mustSelectARowMsg'));
			return;
		}
		//Reset selected part code array
		this.selectedPartCodeArray=[];
		for(var i=0;i<selectRowRecords.length;i++){
			var record=selectRowRecords[i];
			var partId=record.getValue('pt_store_loc_pt.part_id');
			this.selectedPartCodeArray.push(partId);
		}
		this.physicalQtyEditPanel.showInWindow({
			x: 400,
			y: 300,
			width: 800, 
            height: 300,
            modal:true,
            closeButton:true
		});
		this.physicalQtyEditPanel.refresh(null,true);
		//Unchecked checkbox
		$('chkBoxAdjust').checked=false;
	},
	
	/**
	 * Click create supply requisition button
	 */
	partInventoryListPanel_onCreateSupplyRequisition: function(){
		var selectRowRecords=this.partInventoryListPanel.getSelectedRecords();
		if(selectRowRecords.length<1){
			View.alert(getMessage('mustSelectARowMsg'));
			return;
		}
		//Reset selected part code array
		this.selectedPartCodeArray=[];
		for(var i=0;i<selectRowRecords.length;i++){
			var record=selectRowRecords[i];
			var partId=record.getValue('pt_store_loc_pt.part_id');
			this.selectedPartCodeArray.push(partId);
		}
		var partStorageLocId=this.ptStorageLocationcode;
		var partList=this.selectedPartCodeArray;
		this.openCreateSupplyRequistionViewDialog(partStorageLocId,partList);
	},
	
	/**
	 * Open create supply requisition view dialog.
	 */
	openCreateSupplyRequistionViewDialog: function(partStorageLocId,partList){
		//Show supply requisition edit dialog form
		var dialogConfig = {
				width:1000,
				height:600,
				partStoreCode: partStorageLocId,
				partIdList: partList,
				title: getMessage('supplyReqDialogTitle'),
				closeButton: false
		};
		//Show create supply requisition view in dialog
		View.openDialog('ab-bldgops-inv-mpsl-by-loc-req-tabs.axvw', null, false,dialogConfig );
	},
	
	/**
	 * Create Purchase Order.
	 */
	partInventoryListPanel_onCreatePurchaseOrder: function(){
		var selectRowRecords=this.partInventoryListPanel.getSelectedRecords();

		if(selectRowRecords.length<1){
			View.alert(getMessage('mustSelectARowMsg'));
			return;
		}
		//check if selected parts have vendor.
		var allHaveVendor=true;
		//Get first part that does not have vendor code from selected parts list.
		var firstPartNotHaveVendor="";
		
		//Reset selected part code array
		this.selectedPartCodeArray=[];
		for(var i=0;i<selectRowRecords.length;i++){
			var record=selectRowRecords[i];
			var partId=record.getValue('pt_store_loc_pt.part_id');
			var vendorCode=record.getValue('pt_store_loc_pt.vnCode');
			if(!valueExistsNotEmpty(vendorCode)){
				allHaveVendor=false;
				//Get first part that does not have vendor code from selected parts list.
				if(!valueExistsNotEmpty(firstPartNotHaveVendor)){
					firstPartNotHaveVendor=partId;
				}
			}else{
				this.selectedPartCodeArray.push(partId);
			}
			
		}
		
		if(!allHaveVendor){
			if(selectRowRecords.length==1){
				View.alert(String.format(getMessage('notHaveVendorForSingleMsg'),selectRowRecords[0].getValue('pt_store_loc_pt.part_id')));
			}else{
				View.alert(String.format(getMessage('notHaveVendorForSingleMsg'),firstPartNotHaveVendor));
			}
			return;
		}
		
		var partStorageLocId=this.ptStorageLocationcode;
		var partList=this.selectedPartCodeArray;
		this.openCreatePurchaseOrderViewDialog(partStorageLocId,partList);
	},
	
	/**
	 * Open create purchase order view dialog.
	 */
	openCreatePurchaseOrderViewDialog: function(partStorageLocId,partList){
		//check if selected parts have common vendor code.
		var vnRecords=this.getVnIdArrayRecordsByPartsId(partList);
		if(vnRecords.length==0){
			var partsListString="";
			for(var i=0;i<this.selectedPartCodeArray.length;i++){
				partsListString+=this.selectedPartCodeArray[i];
				if(i!=(this.selectedPartCodeArray.length-1)){
					partsListString+=";";
				}
			}
			View.alert(String.format(getMessage('notHaveCommonVendorMsg'),partsListString));
			return;
		}
		//Show supply requisition edit dialog form
		var dialogConfig = {
				width:1200,
				height:600,
				partStoreCode: partStorageLocId,
				partIdList: partList,
				title: getMessage('purchaseOrderDialogTitle'),
				closeButton: false
		};
		//Show create supply requisition view in dialog
		View.openDialog('ab-bldgops-inv-mpsl-by-loc-po-tabs.axvw', null, false,dialogConfig );
	},
	
	/**
	 * Get common vendor by selected part list
	 * @param partList Selected part list
	 * @return pvRecords Records of common vendor
	 */
	getVnIdArrayRecordsByPartsId: function(partList){
		var partIdList=partList;
		var vnDsParameter="1=0";
		for(var i=0;i<partIdList.length;i++){
			var partId=partIdList[i];
			if(i==0){
				vnDsParameter="";
				vnDsParameter+=" part_id='"+partId+"' "
			}else{
				vnDsParameter+=" intersect "
				vnDsParameter+=" (select distinct vn_id as vnId from pv where part_id='"+partId+"')"
			}
		}
		View.dataSources.get('vnDs').addParameter('pvRes',vnDsParameter);
		return View.dataSources.get('vnDs').getRecords();
	},
	
	/**
	 * Update Physical Count.
	 */
	doUpdatePhysicalCount: function(){
		var me=this;
		var canSave=this.validateFormFieldBeforeUpdatePhysicalCount();
		if(canSave){
			//prepare paremeters for WFR.
			var storeLocId=this.ptStorageLocationcode;
			var partCodeRecords = [];
			for(var i=0;i<this.selectedPartCodeArray.length;i++){
				partCodeRecords[i]=new Object();
				partCodeRecords[i]["pt.part_id"]=this.selectedPartCodeArray[i];
			}
			var phsicalCount=this.physicalQtyEditPanel.getFieldValue('pt_store_loc_pt.qty_physical_count');
			//the flag of whether adjust quantity available count from quantity physical count.
			var isAdjustButtonChecked=$('chkBoxAdjust').checked;
			//call WFR to update physical count.
			try {
				var result=Workflow.callMethod('AbBldgOpsBackgroundData-BldgopsPartInventoryService-updatePhysicalCount', storeLocId,partCodeRecords,parseFloat(phsicalCount),isAdjustButtonChecked);
				if(result.code=="executed"){
					//Refresh part inventory list
					var patInvPanelRes=new Ab.view.Restriction();
					patInvPanelRes.addClause('pt_store_loc_pt.pt_store_loc_id',me.ptStorageLocationcode,'=');
					me.partInventoryListPanel.refresh(patInvPanelRes);
					//Close dialog window
					me.physicalQtyEditPanel.closeWindow();
				}
			}catch (e) {
				Workflow.handleError(e);
			}
		}
	},
	
	/**
	 * Validate on form fields before update physical count.
	 */
	validateFormFieldBeforeUpdatePhysicalCount: function(){
		var canSave=this.physicalQtyEditPanel.canSave();
		if(canSave){
			var physicalCount=this.physicalQtyEditPanel.getFieldValue('pt_store_loc_pt.qty_physical_count');
			//Check Transfer Quantity field is number type and greater than 0
				physicalCount=parseFloat(physicalCount);
			if(physicalCount<0){
				View.alert(getMessage("transactionQuantityGreaterThanZeroMsg"));
				return false;
			}
		}else{
			return false;
		}
		
		return true;
	},
	
	/**
	 * Show edit part-storage location form
	 */
	showEditStorageLocationForm: function(){
		var selectRowIndex=this.partInventoryListPanel.selectedRowIndex;
		var selectRowRec=this.partInventoryListPanel.gridRows.get(selectRowIndex).getRecord();
		var partStoreLocId=selectRowRec.getValue('pt_store_loc_pt.pt_store_loc_id');
		var partId=selectRowRec.getValue("pt_store_loc_pt.part_id");
		View.openDialog('ab-bldgops-inv-mpsl-pt-store-loc-detail-dialog.axvw', null, false, {
            width: 800,
            height: 600,
            partStoreLocId: partStoreLocId,
            partId: partId,
            partInventoryListPanel: 'partInventoryListPanel',
            actionType:'edit',
            closeButton: true
        });
	},
	
	/**
	 * Show dialog of Add Parts To Storage Location
	 */
	addPartToStorageLocation: function(){
		var partStoreLocId=this.ptStorageLocationcode;
		View.openDialog('ab-bldgops-inv-mpsl-pt-store-loc-detail-dialog.axvw', null, false, {
            width: 800,
            height: 600,
            partStoreLocId: partStoreLocId,
            partId: '',
            partInventoryListPanel: 'partInventoryListPanel',
            actionType:'addnew-byLocation',
            closeButton: true
        });
	},
	
	/**
	 * show vendor code list by part code selected.
	 * @param partId Part Code.
	 */
	showVnCodeListByPartCode: function(){
		var rowIndex=this.partInventoryListPanel.selectedRowIndex;
		var partId=this.partInventoryListPanel.gridRows.get(rowIndex).getRecord().getValue('pt_store_loc_pt.part_id');
		var pvRes=new Ab.view.Restriction();
			pvRes.addClause('pv.part_id',partId,'=');
		View.panels.get('vendorCodeListDialog').showInWindow({
			width: 500,
			height:400,
			modal:true,
			title: String.format(getMessage('vendorDialogTitle'),partId)
		});
		View.panels.get('vendorCodeListDialog').refresh(pvRes);
	},
	
	/**
	 * Open map view dialog when click Map button.
	 */
	partStorageLocationListPanel_onShowMap: function(){
		var controller=this;
		var consoleRecord=this.consoleForm.getRecord();
		var partList=[];
		if(valueExistsNotEmpty(consoleRecord.getValue('pt.part_id'))){
			partList.push(consoleRecord.getValue('pt.part_id'));
		}
		View.openDialog('ab-bldgops-inv-mpsl-by-loc-map.axvw', null, false, {
            width: 1100,
            height: 900,
            consoleRecord: consoleRecord,
            title: getMessage('storageLocationMapDialogTitle'),
            closeButton: true,
            callback: function(partStoreCode,type){
            	View.closeDialog();
            	if(type=="Purchase"){
        			//call open create purchase order dialog form method in opener view.
            		controller.openCreatePurchaseOrderViewDialog(partStoreCode,partList);
        		}
        		if(type=="Requistion"){
        			//call open supply requisition edit dialog form method in opener view.
        			controller.openCreateSupplyRequistionViewDialog(partStoreCode,partList);
        		}
            }
        });
	},
	
	/**
	 * show dialog of Estimate and Not Reserved Report.
	 */
	openEstimateAndNotReservedReport: function(){
		var rowIndex=this.partInventoryListPanel.selectedRowIndex;
		var partId=this.partInventoryListPanel.gridRows.get(rowIndex).getRecord().getValue('pt_store_loc_pt.part_id');
		var storageLocId=this.partInventoryListPanel.gridRows.get(rowIndex).getRecord().getValue('pt_store_loc_pt.pt_store_loc_id');
		View.openDialog("ab-bldgops-inv-mpsl-estimate-notreserved.axvw",null,false,{
			width: 800,
            height: 500,
            partId:partId,
            storageLocId:storageLocId,
            title: String.format(getMessage('estimateAndNotReservedDialogTitle'),storageLocId,partId)
		});
	},
	
	/**
	 * show detail dialog of Qty On Order field.
	 */
	showQtyOnOrderGrid: function(){
		var rowIndex=this.partInventoryListPanel.selectedRowIndex;
		var partId=this.partInventoryListPanel.gridRows.get(rowIndex).getRecord().getValue('pt_store_loc_pt.part_id');
		var storageLocId=this.partInventoryListPanel.gridRows.get(rowIndex).getRecord().getValue('pt_store_loc_pt.pt_store_loc_id');
		this.poLineItemListForm.showInWindow({
			width:800,
			height:600,
			modal:true
		});
		this.poLineItemListForm.show(true);
		this.poLineItemListForm.addParameter('receivingLocationParam',storageLocId);
		this.poLineItemListForm.addParameter('partCodeParam',partId);
		this.poLineItemListForm.refresh();
	},
	
	/**
	 * show detail dialog of Qty In Transit From field.
	 */
	showQtyInTransitFromDialog: function(){
		var rowIndex=this.partInventoryListPanel.selectedRowIndex;
		var partId=this.partInventoryListPanel.gridRows.get(rowIndex).getRecord().getValue('pt_store_loc_pt.part_id');
		var storageLocId=this.partInventoryListPanel.gridRows.get(rowIndex).getRecord().getValue('pt_store_loc_pt.pt_store_loc_id');
		this.supplyReqItemListByFromGrid.showInWindow({
			width:800,
			height:600,
			modal:true
		});
		this.supplyReqItemListByFromGrid.show(true);
		this.supplyReqItemListByFromGrid.addParameter('ptStoreLocFromParam',storageLocId);
		this.supplyReqItemListByFromGrid.addParameter('partCodeParam',partId);
		this.supplyReqItemListByFromGrid.refresh();
	},
	
	/**
	 * show detail dialog of Qty In Transit To field.
	 */
	showQtyInTransitToDialog: function(){
		var rowIndex=this.partInventoryListPanel.selectedRowIndex;
		var partId=this.partInventoryListPanel.gridRows.get(rowIndex).getRecord().getValue('pt_store_loc_pt.part_id');
		var storageLocId=this.partInventoryListPanel.gridRows.get(rowIndex).getRecord().getValue('pt_store_loc_pt.pt_store_loc_id');
		this.supplyReqItemListByToGrid.showInWindow({
			width:800,
			height:600,
			modal:true
		});
		this.supplyReqItemListByToGrid.show(true);
		this.supplyReqItemListByToGrid.addParameter('ptStoreLocToParam',storageLocId);
		this.supplyReqItemListByToGrid.addParameter('partCodeParam',partId);
		this.supplyReqItemListByToGrid.refresh();
	}
});