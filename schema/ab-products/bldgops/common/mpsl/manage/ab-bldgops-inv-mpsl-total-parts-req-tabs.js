var totalPartsReqTabCtrl=abPtInvMpslCommonTabsCtrl.extend({
	//storage location code used by sub tab view.
	storagelocId: "",
	//transfer quantity used by sub tab view.
	transQty: 0,
	//transfer comments field used by sub tab view.
	comments:"",
	//current tabs panel Id,override parent tabs controller value.
	tabsPanelId: 'supplyReqTabs',
	
	afterViewLoad: function(){
		this.inherit();
		//set storage location code of current view parameter.
		this.storagelocId=View.parameters['partStoreCode'];
	}
});

