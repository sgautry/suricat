var createReqSingleCtrl=View.createController('createReqSingleCtrl',{
	partStoreLocationCode: "",
	partId: "",
	//open select store location selectvalue field from 'it.pt_store_loc_from' or 'it.pt_store_loc_to'
	selectStoreLocType:"",
	openerTabView: null,
	
	afterInitialDataFetch: function(){
		var openerView=View.getOpenerView();
		this.openerTabView=openerView;
		//register current tab to parent tab controller.
		this.registerTabController(openerView);
		this.partId=openerView.parameters['partId'];
		
		this.afterTabSelect(openerView.controllers.items[0]);
	},
	
	/**
	 * register current view controller to parent tabs controller.
	 * @param {openerView} Current opener view.
	 */
	registerTabController: function(openerView){
		var tabsCtrl=openerView.controllers.items[0];
		tabsCtrl.tabCtrls["createSupplyReqTab"]=this;
	},
	
	/**
	 * set field value to parent tabs controller before tab selected.
	 * @param {parentTabsCtrl} Parent Tabs controller.
	 */
	beforeTabSelect: function(parentTabsCtrl){
		canChange=true;
		parentTabsCtrl.transQty=this.getTransQtyFieldValue();
		parentTabsCtrl.comments=this.getCommentsFieldValue();
		var fromStorageLoction=this.getStoreLocFromValue();
	     if(!valueExistsNotEmpty(fromStorageLoction)){
	        View.alert(getMessage('storageLocationFromNotEmptyMsg'));
	        canChange=false;
	     }else{
	    	 parentTabsCtrl.storagelocId=fromStorageLoction;
	     }
	     
	     return canChange;
	},
	
	/**
	 * re-set view field value after tab selected.
	 * @param {parentTabCtrl} Parent Tabs controller.
	 */
	afterTabSelect: function(parentTabCtrl){
		this.setValueByPartCodeAndStoreLocInfo(parentTabCtrl.storagelocId);
		this.resetFieldsValueAfterTabChange(parentTabCtrl.transQty,parentTabCtrl.comments);
	},
	
	/**
	 * KB#3051967
	 * Refilling items is not necessary whatever create new requisition or add to exsiting requisition
	 */
	resetFieldsValueAfterTabChange: function(transQty,comments){
		var ds=this.supplyRequisitionCreateForm.getDataSource();
		this.supplyRequisitionCreateForm.setFieldValue('it.trans_quantity',ds.formatValue('it.trans_quantity',transQty,true));
		this.supplyRequisitionCreateForm.setFieldValue('it.comments',comments);
	},
	
	/**
	 * Set value to supply requisition edit form by part code and part storage location code
	 */
	setValueByPartCodeAndStoreLocInfo: function(storageLocId){
		this.partStoreLocationCode=storageLocId;
		//Clear supply requisition form
		this.supplyRequisitionCreateForm.clear();
		
		//Set value to create supply requisition panel
		var supplyReqPtDs=View.dataSources.get('dialogCreateSupplyReqPtDS');
		var res=new Ab.view.Restriction();
		res.addClause('pt_store_loc_pt.pt_store_loc_id',storageLocId,'=');
		res.addClause('pt_store_loc_pt.part_id',this.partId,'=');
		
		var supplyRecord=supplyReqPtDs.getRecord(res);
		if(!supplyRecord.isNew){
			var description=supplyRecord.getValue('pt.description');
			var qtyOnHand=supplyRecord.getValue('pt_store_loc_pt.qty_on_hand');

			this.supplyRequisitionCreateForm.setFieldValue('it.pt_store_loc_from',storageLocId);
			this.supplyRequisitionCreateForm.setFieldValue('it.part_id',this.partId);
			this.supplyRequisitionCreateForm.setFieldValue('pt.description',description);
			this.supplyRequisitionCreateForm.setFieldValue('pt.qty_on_hand',supplyReqPtDs.formatValue('pt_store_loc_pt.qty_on_hand',qtyOnHand,true));
			this.supplyRequisitionCreateForm.setFieldValue('it.trans_quantity',0);
		}
	},
	
	/**
	 * get Transfer Quantity field value.
	 */
	getTransQtyFieldValue: function(){
		return this.supplyRequisitionCreateForm.getFieldValue('it.trans_quantity');
	},
	
	/**
	 * get Comments field value.
	 */
	getCommentsFieldValue: function(){
		return this.supplyRequisitionCreateForm.getFieldValue('it.comments');
	},
	
	/**
	 * get Storage Location From field value.
	 */
	getStoreLocFromValue: function(){
		return this.supplyRequisitionCreateForm.getFieldValue('it.pt_store_loc_from');
	},
	
	/**
	 * Submit supply requisition information
	 */
	supplyRequisitionCreateForm_onSubmit: function(){
		//validate form field.
		var checkResult=this.supplyRequisitionCreateFormValidate();
		if(!checkResult){
			return;
		}
		
		var storageLocationFrom=this.supplyRequisitionCreateForm.getFieldValue('it.pt_store_loc_from');
		var storageLocationTo=this.supplyRequisitionCreateForm.getFieldValue('it.pt_store_loc_to');
		var partId=this.supplyRequisitionCreateForm.getFieldValue('it.part_id');
		var tansQty=this.supplyRequisitionCreateForm.getFieldValue('it.trans_quantity');
		var qtyAvailable=this.supplyRequisitionCreateForm.getFieldValue('pt.qty_on_hand');
		var comments=this.supplyRequisitionCreateForm.getFieldValue('it.comments');
		
		//Do Submit
		var itRecords = [];
		//Create new inventory transaction record object
		itRecords[0]=new Object();
		itRecords[0]['it.part_id']=partId;
		itRecords[0]['it.trans_quantity']=tansQty;
		itRecords[0]['it.comments']=comments;
		//call submit workflow rule
		try{
			var result=Workflow.callMethod('AbBldgOpsBackgroundData-BldgopsPartInventoryService-createSupplyReq',storageLocationFrom,storageLocationTo,comments,itRecords);
			if(result.code=="executed"){
				View.getOpenerView().getOpenerView().panels.get('abBldgopsReportPartsInvertoryStorageLocationGrid').refresh();
				View.getOpenerView().getOpenerView().closeDialog();
			}
		}catch(e){
			Workflow.handleError(e);
		}
	},
	
	/**
	 * validate 'supplyRequisitionCreateForm' form field value.
	 */
	supplyRequisitionCreateFormValidate: function(){
		var canSave=this.supplyRequisitionCreateForm.canSave();
		
		if(canSave){
			//do custom validation if form can save.
			var storageLocationFrom=this.supplyRequisitionCreateForm.getFieldValue('it.pt_store_loc_from');
			var storageLocationTo=this.supplyRequisitionCreateForm.getFieldValue('it.pt_store_loc_to');
			var partId=this.supplyRequisitionCreateForm.getFieldValue('it.part_id');
			var tansQty=this.supplyRequisitionCreateForm.getFieldValue('it.trans_quantity');
			var qtyAvailable=this.supplyRequisitionCreateForm.getFieldValue('pt.qty_on_hand');
			
			if(valueExistsNotEmpty(qtyAvailable)){
				qtyAvailable=parseFloat(qtyAvailable);
			}else{
				qtyAvailable=0;
			}
			
			if(valueExistsNotEmpty(storageLocationFrom)){
				if(storageLocationFrom==storageLocationTo){
					View.alert(getMessage("storageLocationNameNotSameMsg"));
					return false;
				}
			}

			tansQty=parseFloat(tansQty);
			if(tansQty<=0){
				View.alert(getMessage("transactionQuantityGreaterThanZeroMsg"));
				return false;
			}else{
				if(tansQty>qtyAvailable){
					View.alert(getMessage('doNotTransferMoreThanAvailableMsg'));
					return false;
				}	
			}
			
			return true;
			
		}else{
			return false;
		}
	},
	
	/**
	 * Check Storage location is exists in the part storage location table.
	 * @param storagelocationcode Part storage location code
	 * @return isPtStoreLocExsits True if part storage location exists, else false
	 */
	checkStorageLocationInDs: function(storagelocationcode){
		var isPtStoreLocExsits=false;
		
		var ptStoreLocRes=new Ab.view.Restriction();
			ptStoreLocRes.addClause('pt_store_loc.pt_store_loc_id',storagelocationcode,'=');
		var ptStoreLocDs=View.dataSources.get('dialogCreateSupplyReqStorageLocationDs');
		var length=ptStoreLocDs.getRecords(ptStoreLocRes).length;
		if(length!=0){
			isPtStoreLocExsits=true;
		}
		
		return isPtStoreLocExsits;
	},
	
	/**
	 * Click cancel button to close the dialog view form
	 */
	supplyRequisitionCreateForm_onCancel: function(){
		View.getOpenerView().getOpenerView().closeDialog();
	},
	
	/**
	 * show From Storage Location Dialog to implement location field (Building Code-Floor Code-Room Code)
	 */
	openStorageLocFromSelectValueDialog: function(storeLocField){
		var fieldTitle=this.supplyRequisitionCreateForm.fields.get(storeLocField).fieldDef.title;
		//KB#3053343 Select Value- From Storage Location should only give storages that have part code
		if(storeLocField=='it.pt_store_loc_from'){
			this.storageLocationDialog.addParameter('partCode',this.partId);
			this.storageLocationDialog.addParameter('hasFromStorageLoc',true);
		}else{
			this.storageLocationDialog.addParameter('hasFromStorageLoc',false);
		}
		this.storageLocationDialog.showInWindow({
			modal:true,
			width:600,
			height:400,
			title: getMessage('selectValueDialogTitle')+"-"+fieldTitle
		});
		var restriction=new Ab.view.Restriction();
		this.storageLocationDialog.clearAllFilterValues();
		this.storageLocationDialog.refresh(restriction);
		
		this.selectStoreLocType=storeLocField;
	},
	
	/**
	 * select storage location code from From Storage Location field dialog panel.
	 */
	selectStorageLocationFromSelectValueDialog: function(){
		var selectIndex=this.storageLocationDialog.selectedRowIndex;
		var selectRowRecord=this.storageLocationDialog.gridRows.get(selectIndex).getRecord();
		var storageLocId=selectRowRecord.getValue('pt_store_loc.pt_store_loc_id');
		
		if(this.selectStoreLocType=='it.pt_store_loc_from'){
			this.supplyRequisitionCreateForm.setFieldValue('it.pt_store_loc_from',storageLocId);
			//Re-set form value
			this.setValueByPartCodeAndStoreLocInfo(storageLocId);
		}else{
			this.supplyRequisitionCreateForm.setFieldValue('it.pt_store_loc_to',storageLocId);
		}

		this.storageLocationDialog.closeWindow();
	}
});