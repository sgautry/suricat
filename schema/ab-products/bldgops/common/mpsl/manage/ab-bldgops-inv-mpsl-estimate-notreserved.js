var estimateAndNotReservedReportController=View.createController('estimateAndNotReservedReportController',{
	afterInitialDataFetch: function(){
		var partId=View.parameters["partId"];
		var storageLocId=View.parameters["storageLocId"];
		
		this.estimatePartRpt.addParameter('partCode',partId);
		this.estimatePartRpt.addParameter('hasPartCode',true);
		this.estimatePartRpt.addParameter('storageLocId',storageLocId);
		this.estimatePartRpt.addParameter('hasStorageLocId',true);
		
		this.estimatePartRpt.refresh();
	}
});