/**
 * Define PM Schedule controller for MPSL
 */
var defPMSchedController = View.createController('defPMSched', {

    /**
     * pmp_type : 'EQ' or 'HK'
     */
    pmpType: "EQ",

    /**
     * opened from what view: 'assign', 'forecast' or 'define'
     */
    openFrom: "define",
    // record passed from forecast report views
    pmsRecord: null,
    // restriction passed from Assign Procedure view
    resFromAsign: null,

    // loaded pm schedule records
    records: '',

    isPmsDetailsHidden: false,

    oldPmsValue: '',

    checkAll: false,

    // localized text for 'varies', added for kb#3038652
    variesText: "",
    // 'varies' value hold in client which is consistent with server side string value, added for kb#3038652
    variesValue: "<varies>",
    // use for decide if that is a varies value.
    variesRegexp: /[>,<]/gi,

    afterViewLoad: function() {
        // set equipment system tree level node restriction
        this.updateEqSystemTreeRestrictionLevel();
        
        var hasComplianceLicense = false;
        AdminService.getProgramLicense({
            async: false,
            callback: function(licenseDTO) {
                for (var i = 0; i < licenseDTO.licenses.length; i++) {
                    var license = licenseDTO.licenses[i];
                    if ("AbRiskCompliance" === license.id) {
                        hasComplianceLicense = license.enabled;
                        break;
                    }
                }
            },
            errorHandler: function(m, e) {
                View.showException(e);
            }
        });
        
        if(!hasComplianceLicense){
            jQuery('#pms_service_contract_layoutWrapper').hide();
        }
    },

    updateEqSystemTreeRestrictionLevel: function() {
        if (this.eqSysInventoryTreePanel) {
            // default hide layout of equipment system
            View.getLayoutManager('nested_west_center').collapseRegion('west');
            this.eqSysInventoryTreePanel.updateRestrictionForLevel = updateTreeRestrictionForLevel;
        }
    },

    afterInitialDataFetch: function() {
        //APP-1387:Check database schema for Bali6.
        if(!Workflow.callMethod('AbBldgOpsOnDemandWork-WorkRequestService-checkSchemaExisting','pms', 'location_id').value){
            jQuery('body').hide();
            alert(getMessage('udpateSchemaMessage'));
            return;
        }
        // initial locailzed text of 'varies'
        this.variesText = "<" + getMessage('varies') + ">";

        // initial task priority field
        // setTaskPriorityOptions();
        if (valueExistsNotEmpty(View.getOpenerView())) {
            this.pmsRecord = View.getOpenerView().PMSrecords;
            this.resFromAsign = View.getOpenerView().resFromAsign;
            this.resFromPmPlannerByGroup = View.getOpenerView().resFromPmPlannerByGroup;
            this.resFromPmPlanner = View.getOpenerView().resFromPmPlanner;
            if (View.getOpenerView() && View.getOpenerView().getOpenerView() && View.getOpenerView().getOpenerView().resFromPmPlanner) {
                this.resFromPmPlanner = View.getOpenerView().getOpenerView().resFromPmPlanner;
            }
        }

        if (this.resFromPmPlannerByGroup != undefined) {
            // for view 52 weeks forecast reports
            this.openFrom = "pmPlannerByGroup";
            this.initialForPmPlannerByGroup();
            jQuery('#copy').hide();
            
            var newRow1 = jQuery('<tr class="fieldRow">');
            jQuery('#pms_eq_basic_pms\\.criticality_labelCell').appendTo(newRow1);
            jQuery('#pms_eq_basic_pms\\.criticality_fieldCell').appendTo(newRow1);
            jQuery('#pms_eq_basic_pms\\.dv_id_labelCell').appendTo(newRow1);
            jQuery('#pms_eq_basic_pms\\.dv_id_fieldCell').appendTo(newRow1);
            var newRow2 = jQuery('<tr class="fieldRow">');
            jQuery('#pms_eq_basic_pms\\.pm_group_labelCell').appendTo(newRow2);
            jQuery('#pms_eq_basic_pms\\.pm_group_fieldCell').appendTo(newRow2);
            jQuery('#pms_eq_basic_pms\\.dp_id_labelCell').appendTo(newRow2);
            jQuery('#pms_eq_basic_pms\\.dp_id_fieldCell').appendTo(newRow2);
            
            jQuery('#pms_eq_basic_body').empty();
            jQuery('#pms_eq_basic_body').append(newRow1);
            jQuery('#pms_eq_basic_body').append(newRow2);
            
            jQuery('#pms_service_contract_layoutWrapper').hide();
            this.pms_info.setTitle(getMessage('editByGroupTitle'));
            
            
        } else if (this.resFromPmPlanner != undefined) {
            // for view 52 weeks forecast reports
            this.openFrom = "pmPlanner";
            this.pmpType = View.getOpenerView().pmpType;
            if (View.getOpenerView() && View.getOpenerView().getOpenerView() && View.getOpenerView().getOpenerView().pmpType) {
                this.pmpType = View.getOpenerView().getOpenerView().pmpType;
            }
            this.initialForPmPlannerView();
        } else if (this.resFromAsign != undefined) {
            // for view 52 weeks forecast reports
            this.openFrom = "assign";
            this.initialForAssignProcedure();
        } else if (this.pmsRecord != undefined) {
            // for assign schedule
            this.openFrom = "forecast";
            this.initialForForecastReport();
        } else {
            // for define schedule
            this.initialForDefineSchedule();
        }

        // Set 'selectRecurrenceFreqTabs' listener
        this.selectRecurrenceFreqTabs.addEventListener('afterTabChange', afterRecurrenceFreqTabChange);

        // we only use tabs header, so will remove tab content space and make sure the tab not update height when switch tab
        this.selectRecurrenceFreqTabs.updateHeight = function() {
        };
        jQuery('#int1_recurrence').parent().remove();
        jQuery('#int2_recurrence').parent().remove();
        jQuery('#int3_recurrence').parent().remove();
        jQuery('#int4_recurrence').parent().remove();

        // we don't use one in recurring toolkit
        jQuery('#recurringType_once').hide();
        jQuery('#control_note').hide();
        jQuery('#total').parent().parent().hide();
        

        this.addViewButtons();

    },

    addViewButtons: function() {
        jQuery('#Showpms_service_contract_pms\\.location_id_numeric').attr("class", 'inputField_cell');
        jQuery('#Showpms_service_contract_pms\\.cost_code_numeric').attr("class", 'inputField_cell');
        jQuery('#Showpms_other_pms\\.nactive_numeric').attr("class", 'inputField_cell');
        var controller = this;
        var template = '<button class="x-btn-text" type="button">' + getMessage('viewButtonName') + '</button>';
        var fields = [ 'pms_eq_basic_pms\\.pmp_id', 'pms_rm_basic_pms\\.pmp_id', 'pms_service_contract_pms\\.regulation','pms_service_contract_pms\\.reg_requirement', 'pms_service_contract_pms\\.reg_program',
                'pms_service_contract_pms\\.location_id', 'pms_service_contract_regprogram\\.vn_id', 'pms_service_contract_pms\\.cost_code', 'pms_other_pms\\.nactive' ];
        _.each(fields, function(field) {
            var value = controller.getFieldValueByFieldElementName(field);
            var viewButtons = jQuery('#' + field + '_viewButton');
            if (viewButtons.length == 0 && valueExistsNotEmpty(value) && value !== '0') {
                var viewButton = jQuery(template);
                viewButton.attr('id', field.replace('\\', '') + '_viewButton');
                viewButton.on("click", controller.onViewButton.createDelegate(controller, [ field ]));
                jQuery('#' + field + '_fieldCell').append(viewButton);
            }
        });
    },

    onViewButton: function(field) {
        if (field.indexOf('pmp_id') != -1) {
            var pmpId = this.getFieldValueByFieldElementName(field);
            if (pmpId) {
                var restriction = new Ab.view.Restriction();
                restriction.addClause('pmp.pmp_id', pmpId, '=');
                View.openDialog('ab-pm-def-sched-mpsl-pmp-pop-up.axvw', restriction, false, 0, 0, 800, 600);
            }

        }else if (field.indexOf('regulation') != -1) {
            var restriction = {
                    'regulation.regulation': this.pms_service_contract.getFieldValue('pms.regulation')
                };
                View.openDialog(' ab-comp-rpt-regulation-form.axvw', restriction, false, 0, 0, 1100, 500);
        }else if (field.indexOf('reg_requirement') != -1) {
            var restriction = {
                'regrequirement.regulation': this.pms_service_contract.getFieldValue('pms.regulation'),
                'regrequirement.reg_program': this.pms_service_contract.getFieldValue('pms.reg_program'),
                'regrequirement.reg_requirement': this.pms_service_contract.getFieldValue('pms.reg_requirement')
            };
            View.controllers.get(0).isReport = true;
            View.openDialog('ab-comp-contract-rpt-requirement-form.axvw', restriction, false, 0, 0, 1100, 500);
        } else if (field.indexOf('reg_program') != -1) {
            var restriction = {
                'regprogram.regulation': this.pms_service_contract.getFieldValue('pms.regulation'),
                'regprogram.reg_program': this.getFieldValueByFieldElementName(field)
            };
            View.openDialog('ab-comp-contract-rpt-program-form.axvw', restriction, false, 0, 0, 800, 400);
        } else if (field.indexOf('location_id') != -1) {
            var restriction = {
                'compliance_locations.location_id': this.getFieldValueByFieldElementName(field)
            };
            View.openDialog('ab-pm-def-sched-mpsl-loc-pop-up.axvw', restriction, false, 0, 0, 800, 400);
        } else if (field.indexOf('vn_id') != -1) {
            var restriction = {
                'vn.vn_id': this.getFieldValueByFieldElementName(field)
            };
            View.openDialog('ab-pm-def-sched-mpsl-vn-pop-up.axvw', restriction, false, 0, 0, 800, 400);
        } else if (field.indexOf('cost_code') != -1) {
            var restriction = {
                'compliance_contract_cost.cost_code': this.getFieldValueByFieldElementName(field)
            };
            View.openDialog('ab-pm-def-sched-mpsl-cost-pop-up.axvw', restriction, false, 0, 0, 800, 400);
        } else if (field.indexOf('nactive') != -1) {
            var pmsId = this.pms_info.getFieldValue('pms.pms_id');
            View.openDialog('ab-pm-def-sched-mpsl-wo-pop-up.axvw', 'exists(select 1 from wr where wr.wo_id = wo.wo_id and date_completed IS NULL and wr.pms_id = ' + pmsId + ')')
        }
    },

    getFieldValueByFieldElementName: function(field) {
        var value = '';
        if (field.indexOf('pmp_id') != -1) {
            value = 'EQ' == this.pmpType ? this.pms_eq_basic.getFieldValue('pms.pmp_id') : this.pms_rm_basic.getFieldValue('pms.pmp_id');
        } else if (field.indexOf('regulation') != -1) {
            value = this.pms_service_contract.getFieldValue('pms.regulation');
        } else if (field.indexOf('reg_requirement') != -1) {
            value = this.pms_service_contract.getFieldValue('pms.reg_requirement');
        } else if (field.indexOf('reg_program') != -1) {
            value = this.pms_service_contract.getFieldValue('pms.reg_program');
        } else if (field.indexOf('location_id') != -1) {
            value = this.pms_service_contract.getFieldValue('pms.location_id');
        } else if (field.indexOf('vn_id') != -1) {
            value = this.pms_service_contract.getFieldValue('regprogram.vn_id');
        } else if (field.indexOf('cost_code') != -1) {
            value = this.pms_service_contract.getFieldValue('pms.cost_code');
        } else if (field.indexOf('nactive') != -1) {
            value = this.pms_other.getFieldValue('pms.nactive');
        }
        return value;
    },

    initialForAssignProcedure: function() {
        this.pmpType = View.getOpenerView().pmpType;
        if ('EQ' == this.pmpType) {
            this.eq_procedure_select.refresh(this.resFromAsign);
        } else {
            this.SelectEquipmentLocationProcedure.selectTab('rm_procedure');
            this.rm_procedure_select.show(true);
            this.rm_procedure_select.refresh(this.resFromAsign);
        }
        View.getLayoutManager('mainLayout').collapseRegion('west');
        jQuery('#main_west-xcollapsed').remove();
    },
    
    initialForPmPlannerByGroup: function() {
        this.records = this.resFromPmPlannerByGroup;
        onCheckMutlipleRows(this.records);
        // from 23.2 show Recurring Frequency pattern or old Frequency fields based on the current interval type
        showFrequencyBaseOnIntervalType();
    },

    initialForPmPlannerView: function() {
        this.records = [ {
            'pms.pms_id': this.resFromPmPlanner.clauses[0].value
        } ];
        onCheckSingleRow(this.records);
        // from 23.2 show Recurring Frequency pattern or old Frequency fields based on the current interval type
        showFrequencyBaseOnIntervalType();
    },

    initialForForecastReport: function() {
        // kb:3024435
        // this.eq_procedure_select.addEventListener('onMultipleSelectionChange', eq_procedure_onClick);
        this.eq_procedure_select.selectAll = this.selectAll;
        this.rm_procedure_select.selectAll = this.selectAll;
        this.addSelectionHandler();

        this.showForecastPmShedules();

        this.dsAbPmDefSchedPms_filterConsole.show(false);
        var layout1 = View.getLayoutManager('nested_west');
        layout1.collapseRegion('north');
    },

    initialForDefineSchedule: function() {
        this.SelectEquipmentLocationProcedure.addEventListener('beforeTabChange', this.beforeTabChange);
        // this.eq_procedure_select.addEventListener('onMultipleSelectionChange', eq_procedure_onClick);
        this.eq_procedure_select.selectAll = this.selectAll;
        this.rm_procedure_select.selectAll = this.selectAll;
        this.addSelectionHandler();

        document.getElementById("noschedule").checked = false;
        this.pms_info.enableButton('save', false);
        this.pms_info.enableButton('copy', false);

        this.initialConsoleFields();
    },

    initialConsoleFields: function() {
        $("dsAbPmDefSchedPms_filterConsole_pms.bl_id").value = "";
        $("dsAbPmDefSchedPms_filterConsole_pms.fl_id").value = "";
        $("dsAbPmDefSchedPms_filterConsole_pms.rm_id").value = "";
        $("dsAbPmDefSchedPms_filterConsole_eq.eq_std").value = "";
        $("dsAbPmDefSchedPms_filterConsole_pms.pmp_id").value = "";
    },

    addSelectionHandler: function() {
        this.eq_procedure_select.addEventListener('onMultipleSelectionChange', eq_procedure_onClick);
        this.rm_procedure_select.addEventListener('onMultipleSelectionChange', rm_procedure_onClick);
    },

    removeSelectionHandler: function() {
        this.eq_procedure_select.addEventListener('onMultipleSelectionChange', null);
        this.rm_procedure_select.addEventListener('onMultipleSelectionChange', null);
    },

    // For solving the performance issue, override the reportGrid.selectAll() function.
    selectAll: function(selected) {
        var ctrl = defPMSchedController;
        if ('EQ' == ctrl.pmpType) {
            ctrl.eq_procedure_select.addEventListener('onMultipleSelectionChange', null);
            ctrl.eq_procedure_select.setAllRowsSelected(selected);
            ctrl.eq_procedure_select.addEventListener('onMultipleSelectionChange', eq_procedure_onClick);
            eq_procedure_onClick();
        } else {
            ctrl.rm_procedure_select.addEventListener('onMultipleSelectionChange', null);
            ctrl.rm_procedure_select.setAllRowsSelected(selected);
            ctrl.rm_procedure_select.addEventListener('onMultipleSelectionChange', rm_procedure_onClick);
            rm_procedure_onClick();
        }
    },

    // the function is for get pms that the dialog will display
    showForecastPmShedules: function() {
        var pmsRestr = createObjectFromPmsRecord(this.pmsRecord);
        // kb:3024805
        var result = {};
        try {
            result = Workflow.callMethod('AbBldgOpsPM-PmEventHandler-getMultiPMSRecords', pmsRestr);
            if (result.code == "executed") {
                var pmsIds = eval('(' + result.jsonExpression + ')');
                var restrictionTab = new Ab.view.Restriction();
                if (pmsIds.length > 0) {
                    var pmsIdArray = new Array();
                    for (var r = 0; r < pmsIds.length; r++) {
                        pmsIdArray[r] = pmsIds[r];
                    }
                    restrictionTab.addClause('pms.pms_id', pmsIdArray, 'IN');
                    // $('eq_procedure_select_checkAll').checked = true;
                    // $('rm_procedure_select_checkAll').checked = true;
                } else {// kb:3024526
                    restrictionTab.addClause('pms.pms_id', -1, '=');
                }
                this.eq_procedure_select.refresh(restrictionTab);
                this.eq_procedure_select.selectAll(true);
                $('eq_procedure_select_checkAll').checked = true;
                this.rm_procedure_select.refresh(restrictionTab);
            }
        } catch (e) {
            Workflow.handleError(e);
        }
    },

    /**
     * Update the style of the rows in equipment or location grid panel after the two grid panel refresh
     * @param {boolean} isEq
     * @param {String} gridPanelID
     */
    eq_procedure_select_afterRefresh: function() {
        this.afterRefreshProcedureGrid('eq_procedure_select');
        // after refresh select grid, determine to enable the 'Copy' action or not
        enableCopyButton(View.panels.get("eq_procedure_select"));
    },

    rm_procedure_select_afterRefresh: function() {
        this.afterRefreshProcedureGrid('rm_procedure_select');
        enableCopyButton(View.panels.get("rm_procedure_select"));
    },

    /**
     * Update the style of the rows in equipment or location grid panel after the two grid panel refresh
     * @param {boolean} isEq
     * @param {String} gridPanelID
     */
    afterRefreshProcedureGrid: function(gridId) {
        this.updateStyle(gridId);

        // add a listener for all check button
        if (this.openFrom == 'assign') {
            var checkAllEl = Ext.get(gridId + '_checkAll');
            var panel = View.panels.get(gridId);
            if (valueExists(checkAllEl)) {
                this.selectAll(true);
            }
            // kb:3024435
            this.setAllRowsDisabledSelected(panel);
        }
    },

    updateStyle: function(gridId) {
        // var gridPanelID = isEq ? "eq_procedure_select" : "rm_procedure_select";
        var grid = View.panels.get(gridId);
        for (var i = 0; i < grid.rows.length; i++) {
            this.updateOneRowStyle(grid.rows[i]);
            for (var j = 0; j < this.records.length; j++)
                if (grid.rows[i]['pms.pms_id'] == this.records[j]['pms.pms_id'])
                    grid.rows[i].row.dom.firstChild.firstChild.checked = true;
        }
    },

    // kb :3024161
    updateOneRowStyle: function(row) {
        var rowEl = Ext.get(row.row.dom);
        if (row['pms.interval_1'] != 0 || row['pms.interval_2'] != 0 || row['pms.interval_3'] != 0 || row['pms.interval_4'] != 0
                || row['pms.int1_recurrence'] != '' || row['pms.int2_recurrence'] != '' || row['pms.int3_recurrence'] != '' || row['pms.int4_recurrence'] != '') {
            rowEl.setStyle('font-weight', 'bold');
        } else {
            rowEl.setStyle('font-weight', 'normal');
        }
    },

    // disable the checked
    setAllRowsDisabledSelected: function(panel) {
        // get switch value, default == true
        var dataRows = panel.getDataRows();
        for (var r = 0; r < dataRows.length; r++) {
            var str = panel.id + "_row" + r + "_multipleSelectionColumn";
            document.getElementById(str).disabled = true;
        }
        document.getElementById(panel.id + '_checkAll').disabled = true;
    },

    beforeSavePmSchedule: function(pmsValue) {
        var intervalType = pmsValue['pms.interval_type'];
        if (intervalType == 'r') {
            var recurrenceArray = [];
            if (valueExistsNotEmpty(pmsValue['pms.int1_recurrence'])) {
                recurrenceArray.push(new RecurringPattern(pmsValue['pms.int1_recurrence']));
            }
            if (valueExistsNotEmpty(pmsValue['pms.int2_recurrence'])) {
                recurrenceArray.push(new RecurringPattern(pmsValue['pms.int2_recurrence']));
            }
            if (valueExistsNotEmpty(pmsValue['pms.int3_recurrence'])) {
                recurrenceArray.push(new RecurringPattern(pmsValue['pms.int3_recurrence']));
            }
            if (valueExistsNotEmpty(pmsValue['pms.int4_recurrence'])) {
                recurrenceArray.push(new RecurringPattern(pmsValue['pms.int4_recurrence']));
            }

            if (recurrenceArray.length > 1) {
                var isOverlap = false;
                var dateMap = {};
                for (var i = 0; i < recurrenceArray.length; i++) {
                    if (recurrenceArray[i].isSeasonal == '0') {
                        View.showMessage(getMessage('mustSeasonal'));
                        return false;
                    }

                    var startMonth = parseInt(recurrenceArray[i].seasonalStartMonth);
                    var startMonthDate = parseInt(recurrenceArray[i].seasonalStartMonthDate);

                    var endMonth = parseInt(recurrenceArray[i].seasonalEndMonth);
                    var endMonthDate = parseInt(recurrenceArray[i].seasonalEndMonthDate);

                    if (endMonth < startMonth) {
                        endMonth = endMonth + 12;
                    }

                    var maxDate = 31;
                    for (var m = startMonth; m <= endMonth; m++) {
                        if (m == endMonth) {
                            maxDate = endMonthDate;
                        }
                        for (var d = startMonthDate; d <= maxDate; d++) {
                            var key = m + '-' + d;
                            if (m > 12) {
                                key = (m - 12) + '-' + d;
                            }
                            if (!dateMap.hasOwnProperty(key)) {
                                dateMap[key] = key;
                            } else {
                                isOverlap = true;
                                View.showMessage(getMessage('seasonalOverlap'));
                                return false;
                            }
                        }
                    }
                }
            }

            var fixed = pmsValue['pms.fixed'];
            if (fixed == '0') {
                for (var i = 0; i < recurrenceArray.length; i++) {
                    if ((recurrenceArray[i].type == 'week' && recurrenceArray[i].value1.indexOf('1') != -1) || (recurrenceArray[i].type == 'month' && recurrenceArray[i].value1 != '')
                            || recurrenceArray[i].type == 'year') {
                        View.showMessage(getMessage('mustFixed'));
                        return false;
                    }
                }
            }
        }
        
        return true;
    },
    
    pms_info_onSave: function() {
        // Save multi-loading pms records file='PreventiveMaintenanceCommonHandler.java'
        try {
            // get the syncronized record
            var pmsValue = getMultiplePmsValue();
            var isValid = this.beforeSavePmSchedule(pmsValue);
            if(isValid){
                var result = Workflow.callMethod('AbBldgOpsPM-PmEventHandler-saveMultiPMSRecords', pmsValue, this.records);
                if (result.code == 'executed') {
                    this.afterSavePmSchedule();
                    this.oldPmsValue = pmsValue;
                } else {
                    Workflow.handleError(result);
                }
            }
            
        } catch (e) {
            Workflow.handleError(e);
        }
    },

    afterSavePmSchedule: function() {
        // show text message in the form
        var message = getMessage('formSaved');
        this.pms_info.displayTemporaryMessage(message);

        // after the details be saved ,refresh the tag in below three status , from assign ,from view 52 weeks,define pm schedule
        // 1
        // for view 52 weeks
        if (this.openFrom == 'assign' || this.openFrom == 'pmPlanner' || this.openFrom == 'pmPlannerByGroup') {
            // this.isAlertSaved = true;
            // this.eq_procedure_select.refresh(resFromAsign);
        }// 2
        else if (this.openFrom == 'forecast') {
            // when afterSave ='true';updateStyle()could excuted the checked old records again ,or didn't excute it.
            // this.showPassedIntoPmsRecord(this.pmsRecord);
        }// 3
        else {
            var locPocGrid = View.panels.get('rm_procedure_select');
            locPocGrid.refresh(locPocGrid.restriction);
            var eqPocGrid = View.panels.get('eq_procedure_select');
            eqPocGrid.refresh(eqPocGrid.restriction);

        }
        // records = '';
    },

    /*
     * Copy current loaded record in PM schedule forms as a new pms recrod, then save it and load it in form. Refresh select grid and set new row as selected.
     */
    pms_info_onCopy: function() {
        // Add new pms record
        this.pms_info.refresh(null, true);

        var selectGrid;
        // For equipment, copy pmp_id and eq_id from previous loaded pms record to new one
        if (this.pmpType == 'EQ') {
            this.pms_info.setFieldValue("pms.pmp_id", this.pms_eq_basic.getFieldValue("pms.pmp_id"));
            this.pms_info.setFieldValue("pms.eq_id", this.pms_eq_basic.getFieldValue("pms.eq_id"));
            this.pms_info.setFieldValue("pms.pt_store_loc_id", this.pms_other.getFieldValue("pms.pt_store_loc_id"));
            this.pms_info.save();
            selectGrid = this.eq_procedure_select;
        }
        // For housekeeping, copy site_id, bl_id.fl_id, rm_id and pmp_id from previous loaded pms record to new one
        else {
            this.pms_info.setFieldValue("pms.pmp_id", this.pms_rm_basic.getFieldValue("pms.pmp_id"));
            this.pms_info.setFieldValue("pms.site_id", this.pms_rm_basic.getFieldValue("pms.site_id"));
            this.pms_info.setFieldValue("pms.bl_id", this.pms_rm_basic.getFieldValue("pms.bl_id"));
            this.pms_info.setFieldValue("pms.fl_id", this.pms_rm_basic.getFieldValue("pms.fl_id"));
            this.pms_info.setFieldValue("pms.rm_id", this.pms_rm_basic.getFieldValue("pms.rm_id"));
            this.pms_info.setFieldValue("pms.pt_store_loc_id", this.pms_other.getFieldValue("pms.pt_store_loc_id"));
            this.pms_info.save();
            selectGrid = this.rm_procedure_select;
        }

        var newPmsId = this.pms_info.getFieldValue("pms.pms_id");
        if (!selectGrid) {
            this.records = [ {
                'pms.pms_id': newPmsId
            } ];
            onCheckSingleRow(this.records);
            return;
        }

        // Refresh select grid to show new row
        selectGrid.refresh();
        // Get pms_id from new pms record
        var newRowIndex = -1;
        // loop through grid rows to get index of new row and unselect other rows
        selectGrid.gridRows.each(function(row) {
            var pmsId = row.getRecord().getValue('pms.pms_id');
            if (newPmsId == pmsId) {
                newRowIndex = row.record.index;
                // row.dom.firstChild.firstChild.checked = true;
            } else {
                row.dom.firstChild.firstChild.checked = false;
            }
        });
        // set new row selected in grid by calling proper API, thus active proper event and its handler function
        if (newRowIndex >= 0) {
            selectGrid.selectRowChecked(newRowIndex, true);
        }
    },

    beforeTabChange: function(tabPanel, selectedTabName, newTabName) {

        if ('define' == defPMSchedController.openFrom) {
            if (newTabName == "eq_procedure") {
                defPMSchedController.pmpType = 'EQ';
                View.panels.get('eq_procedure_select').refresh();
            } else if (newTabName == "rm_procedure") {
                defPMSchedController.pmpType = 'HK';
                View.panels.get('rm_procedure_select').refresh();
            }
        }
        if ('assign' != defPMSchedController.openFrom) {
            defPMSchedController.hideAllPmsForms();
            defPMSchedController.isPmsDetailsHidden = true;
        }
    },

    hideAllPmsForms: function() {
        View.panels.get('pms_info').show(false);
        View.panels.get('pms_eq_basic').show(false);
        View.panels.get('pms_rm_basic').show(false);
        View.panels.get('pms_schedule').show(false);
        View.panels.get('pms_service_contract').show(false);
        View.panels.get('pms_other').show(false);
        View.panels.get('editRecurringRule').show(false);
        jQuery('#selectRecurrenceFreqTabs_layoutWrapper').hide();
    },

    /*
     * addRestrictionForEqProcedureSelect: function(restriction){ restriction.addClause('pms.interval_1', 0, '&gt;', 'AND'); restriction.addClause('pms.interval_2', 0, '&gt;', 'AND');
     * restriction.addClause('pms.interval_3', 0, '&gt;', 'AND'); restriction.addClause('pms.interval_4', 0, '&gt;', 'AND'); this.eq_procedure_select.refresh(restriction); },
     */

    // ----------------------- private function -----------------------------------------------------
    /**
     * syncronize records of multi panels while in same DataSource to one record.
     * @param {ab.data.record} kbItem
     */
    syncPanelPmsRecord: function() {
        var item = this.pms_info.getRecord();

        View.panels.each(function(panel) {
            if ((panel.getRecord) && (panel.visible)) {
                panel.getRecord();
                panel.fields.each(function(field) {
                    item.setValue(field.getFullName(), field.getStoredValue());
                });
            }
        });
        return item;
    },

    /**
     * show message in the top row of this form
     * @param {string} message
     */
    showInformationInForm: function(controller, panel, message) {
        var messageCell = panel.getMessageCell();
        messageCell.dom.innerHTML = "";

        var messageElement = Ext.DomHelper.append(messageCell, '<p>' + message + '</p>', true);
        messageElement.addClass('formMessage');
        messageElement.setVisible(true, {
            duration: 1
        });
        messageElement.setHeight(20, {
            duration: 1
        });
        if (message) {
            panel.dismissMessage.defer(3000, controller, [ messageElement ]);
        }
    },

    dsAbPmDefSchedPms_filterConsole_onFilter: function() {
        this.onFiter();
    },

    // kb:3024292
    onFiter: function() {
        var rmRestriction = this.getRestriction();
        this.addLocationRestriction(rmRestriction, 'pms');
        View.panels.get('rm_procedure_select').refresh(rmRestriction);

        // 3024292 . For equipment, the restriction should be combination of equal to eq_std and pmp_id; for Location, the restriction should be combination of equal to bl_id, fl_id, rm _id and
        // pmp_id.
        var eqRestriction = this.getRestriction();
        this.addLocationRestriction(eqRestriction, 'eq');
        var pmsEqstd = this.dsAbPmDefSchedPms_filterConsole.getFieldValue("eq.eq_std");
        if (pmsEqstd != '') {
            eqRestriction.addClause('eq.eq_std', pmsEqstd, '=', '))AND((');
        }
        View.panels.get('eq_procedure_select').refresh(eqRestriction);

        if (this.checkAll) {
            $('eq_procedure_select_checkAll').checked = true;
            $('rm_procedure_select_checkAll').checked = true;
        }
    },

    getRestriction: function() {
        // var intervalFreq = (this.ds_ab-pm-def-sched_pms_console.getRecord()).getValue("pms.interval_freq");
        var restriction = new Ab.view.Restriction();

        if (document.getElementById("noschedule").checked) {
            restriction.addClause('pms.interval_type', 'r', '!=');
            restriction.addClause('pms.interval_1', 0, '=');
            restriction.addClause('pms.interval_2', 0, '=');
            restriction.addClause('pms.interval_3', 0, '=');
            restriction.addClause('pms.interval_4', 0, '=');
            
            restriction.addClause('pms.interval_type', 'r', '=', ')OR(');
            restriction.addClause('pms.int1_recurrence', '', 'IS NULL');
            restriction.addClause('pms.int2_recurrence', '', 'IS NULL');
            restriction.addClause('pms.int3_recurrence', '', 'IS NULL');
            restriction.addClause('pms.int4_recurrence', '', 'IS NULL');
        }

        var console = this.dsAbPmDefSchedPms_filterConsole;
        var pmsPmpId = console.getFieldValue("pms.pmp_id");
        if (pmsPmpId != '') {
            restriction.addClause('pms.pmp_id', pmsPmpId, '=', '))AND((');
        }

        return restriction;
    },

    addLocationRestriction: function(restriction, tableName) {
        var console = this.dsAbPmDefSchedPms_filterConsole;
        var pmsBlId = console.getFieldValue("pms.bl_id");
        var pmsFlId = console.getFieldValue("pms.fl_id");
        var pmsRmId = console.getFieldValue("pms.rm_id");
        if (pmsBlId != '') {
            restriction.addClause(tableName + '.bl_id', pmsBlId, '=', '))AND((');
        }
        if (pmsFlId != '') {
            restriction.addClause(tableName + '.fl_id', pmsFlId, '=', '))AND((');
        }
        if (pmsRmId != '') {
            restriction.addClause(tableName + '.rm_id', pmsRmId, '=', '))AND((');
        }
    },
    
    //clear the filter
    dsAbPmDefSchedPms_filterConsole_onClear: function(){
        this.initialConsoleFields();
        document.getElementById("noschedule").checked = false;
    },

    // Show equipment system tree
    dsAbPmDefSchedPms_filterConsole_onShowEqSysTree: function() {
        var layout = View.getLayoutManager('nested_west_center');
        layout.expandRegion('west');
    }
});

// when on pms was choose ,action name is 'save',else 'save all records'
function resetTitle(records) {

    var title = getMessage('title');
    var titleSaveAll = getMessage('titleSaveAll');

    var grid = View.panels.get('pms_info');
    var action = grid.actions.get('save');

    if (records.length == 1 || records == '') {
        action.setTitle(title);
    } else {
        action.setTitle(titleSaveAll);
    }
}

function isFormChanged(newPmsValue, oldPmsValue) {
    var isChanged = false;
    if (!defPMSchedController['isPmsDetailsHidden']) {
        if (oldPmsValue != '') {
            if (oldPmsValue["pms.dv_id"] != newPmsValue["pms.dv_id"] || oldPmsValue["pms.dp_id"] != newPmsValue["pms.dp_id"] || oldPmsValue["pms.pm_group"] != newPmsValue["pms.pm_group"]
                    || oldPmsValue["pms.date_first_todo"] != newPmsValue["pms.date_first_todo"] || oldPmsValue["pms.date_next_alt_todo"] != newPmsValue["pms.date_next_alt_todo"]
                    || oldPmsValue["pms.hours_est"] != newPmsValue["pms.hours_est"] || oldPmsValue["pms.interval_type"] != newPmsValue["pms.interval_type"]
                    || oldPmsValue["pms.fixed"] != newPmsValue["pms.fixed"] || oldPmsValue["pms.interval_freq"] != newPmsValue["pms.interval_freq"]
                    || oldPmsValue["pms.interval_1"] != newPmsValue["pms.interval_1"] || oldPmsValue["pms.interval_2"] != newPmsValue["pms.interval_2"]
                    || oldPmsValue["pms.interval_3"] != newPmsValue["pms.interval_3"] || oldPmsValue["pms.interval_4"] != newPmsValue["pms.interval_4"]
                    || oldPmsValue["pms.criticality"] != newPmsValue["pms.criticality"] || oldPmsValue["pms.days_late"] != newPmsValue["pms.days_late"]
                    || oldPmsValue["pms.days_early"] != newPmsValue["pms.days_early"] || oldPmsValue["pms.due_date_from"] != newPmsValue["pms.due_date_from"]
                    || oldPmsValue["pms.int1_recurrence"] != newPmsValue["pms.int1_recurrence"] || oldPmsValue["pms.int2_recurrence"] != newPmsValue["pms.int2_recurrence"]
                    || oldPmsValue["pms.int3_recurrence"] != newPmsValue["pms.int3_recurrence"] || oldPmsValue["pms.int4_recurrence"] != newPmsValue["pms.int4_recurrence"]
                    || oldPmsValue["pms.total_unit"] != newPmsValue["pms.total_unit"] || oldPmsValue["pms.units"] != newPmsValue["pms.units"]
                    || oldPmsValue["pms.comments"] != newPmsValue["pms.comments"] || oldPmsValue["pms.priority"] != newPmsValue["pms.priority"]) {
                isChanged = true;
            }
        }
    }
    return isChanged;
}

function selectRowByRecords(grid) {
    for (var i = 0; i < grid.rows.length; i++)
        if ('assign' != defPMSchedController['openFrom'])
            for (var j = 0; j < defPMSchedController['records'].length; j++)
                if (grid.rows[i]['pms.pms_id'] == defPMSchedController['records'][j]['pms.pms_id'])
                    grid.rows[i].row.dom.firstChild.firstChild.checked = true;
}

function selectAllRows(grid, YesOrNo) {
    for (var i = 0; i < grid.rows.length; i++) {
        grid.rows[i].row.dom.firstChild.firstChild.checked = YesOrNo;
    }
}

function eq_procedure_onClick(pmsRecord, resFromAsign) {
    onClickProcedureRow("eq_procedure_select");
}

function rm_procedure_onClick() {
    onClickProcedureRow("rm_procedure_select");
}

function onClickProcedureRow(panelId) {
    if (!confirmDoingCheckRows(panelId))
        return;
    // Set default storage location code from part storage location table
    var mainWarehouse = getMainStorageLocation();

    if (valueExistsNotEmpty(mainWarehouse)) {
        // doing check row(s)
        doingCheckRows(panelId);

        resetVariableAfterCheckRows(panelId);

        // after selection change in select grid, determine to enable the 'Copy' action or not
        enableCopyButton(View.panels.get(panelId));

        var warehouse = View.panels.get('pms_other').getFieldValue('pms.pt_store_loc_id');
        if (!valueExistsNotEmpty(warehouse)) {
            View.panels.get('pms_other').setFieldValue('pms.pt_store_loc_id', mainWarehouse);
        }
    } else {
        View.alert(getMessage('mainWarehouseNotExistsMsg'));
    }

}
/**
 * Get main storage location, by default , the Main Storage Location is "MAIN"
 * @returns {String}
 */
function getMainStorageLocation() {
    var mainStorageLocation = "MAIN";

    return mainStorageLocation;

}

function confirmDoingCheckRows(gridId) {
    // kb:3024401
    var confirmed = true;
    var newPmsValue = getMultiplePmsValue();
    var oldPmsValue = defPMSchedController['oldPmsValue'];
    // if (pmsRecord == undefined || resFromAsign == undefined || pmsRecord != undefined || (resFromAsign != undefined && defPMSchedController['isAlertSaved'] == false)) {
    if (newPmsValue != undefined && oldPmsValue != undefined && isFormChanged(newPmsValue, oldPmsValue)) {
        var recordsChange = getMessage('recordsChange');
        confirmed = window.confirm(recordsChange);
        if (!confirmed) {
            // kb:3024400
            var grid = View.panels.get(panelId);
            if (defPMSchedController['checkAll']) {
                $(gridId + '_checkAll').checked = true;
                selectAllRows(grid, true);
            } else {
                $(gridId + '_checkAll').checked = false;
                selectAllRows(grid, false);
                // grid.setAllRowsSelected(false);
                selectRowByRecords(grid);
            }
        }
    }
    return confirmed;
}

function doingCheckRows(panelId) {
    var panel = View.panels.get(panelId);
    defPMSchedController['records'] = getPrimaryKeysForSelectedRows(panel);
    var rows = panel.getSelectedRows();
    // kb:3024411
    if (defPMSchedController['records'].length == 0) {
        onCheckNoneRow();
    } else if (defPMSchedController['records'].length == 1) {
        onCheckSingleRow(rows);
    } else {
        onCheckMutlipleRows(rows);
    }

    defPMSchedController.addViewButtons();

    // from 23.2 show Recurring Frequency pattern or old Frequency fields based on the current interval type
    showFrequencyBaseOnIntervalType();
}

function onCheckNoneRow() {
    if (defPMSchedController['records'].length == 0) {
        defPMSchedController.hideAllPmsForms();
        defPMSchedController['isPmsDetailsHidden'] = true;
        if (defPMSchedController['openFrom'] == 'forecast') {
            $("instructions").innerHTML = "";
        }
        return;
    }
}

function onCheckSingleRow(rows) {
    var restriction = new Ab.view.Restriction();
    var pmsId = rows[0]['pms.pms_id'];
    restriction.addClause("pms.pms_id", pmsId, "=", true);
    refreshPmsPanelForSingleRecord(restriction);

    initialFieldsAfterRefreshPmsPanelForSingleRecord();
    updateIntervalTypeOptions();
}

function initialFieldsAfterRefreshPmsPanelForSingleRecord() {
    var pmsEqBasic = View.panels.get('pms_eq_basic');
    var pmsRmBasic = View.panels.get('pms_rm_basic');
    var pmsSchedule = View.panels.get('pms_schedule')
    var pmsOther = View.panels.get('pms_other');

    var openerController = View.controllers.get("defPMSched");
    var isEq = ("EQ" == openerController.pmpType) ? true : false;
    if (isEq) {
        pmsEqBasic.showField('pms.pms_id', true);
        pmsEqBasic.showField('pms.eq_id', true);
    } else {
        pmsRmBasic.showField('pms.site_id', true);
        pmsRmBasic.showField('pms.bl_id', true);
        pmsRmBasic.showField('pms.fl_id', true);
        pmsRmBasic.showField('pms.rm_id', true);
    }

    pmsSchedule.showField('pms.date_last_completed', true);
    pmsSchedule.showField('pms.date_next_todo', true);
    pmsSchedule.showField('pms.hours_calc', true);
    pmsOther.showField('pms.meter_last_pm', true);
    pmsOther.showField('pms.nactive', true);
    defPMSchedController['isPmsDetailsHidden'] = false;
}

function onCheckMutlipleRows(rows) {
    var records = defPMSchedController['records']
    var result = {};
    // This workflow is for compare the field value ,if it show the same value that each field of
    // all records ,return the value ,or show string 'varies' Text file='PreventiveMaintenanceCommonHandler.java'
    try {
        result = Workflow.callMethod('AbBldgOpsPM-PmEventHandler-loadMultiPMSRecords', records);
    } catch (e) {
        Workflow.handleError(e);
    }

    if (result.code == 'executed') {
        var pmsValue = eval('(' + result.jsonExpression + ')');
        // set the value when load the pmstable'panel
        setMultiplePmsValue(pmsValue, rows[0]['pms.pms_id']);
        // hiddenOnlyField('pms_schedule', 'pms.date_last_completed');
    } else {
        Workflow.handleError(result);
    }
    defPMSchedController['isPmsDetailsHidden'] = false;
    updateIntervalTypeOptions();
}

function resetVariableAfterCheckRows(panelId) {
    // reset value of variable 'checkAll'
    if ($(panelId + '_checkAll').checked) {
        defPMSchedController['checkAll'] = true;
    } else {
        defPMSchedController['checkAll'] = false;
    }

    // reset value of variable 'variable'
    defPMSchedController['oldPmsValue'] = getMultiplePmsValue();

    // kb:3024583
    if (defPMSchedController['openFrom'] == 'forecast') {
        $("instructions").innerHTML = getMessage("instructions1") + "<br>" + getMessage("instructions2") + "<br>&nbsp;" + getMessage("instructions21") + "<br>" + getMessage("instructions3")
                + "<br>&nbsp;" + getMessage("instructions31");
    }

    resetTitle(defPMSchedController['records']);

}

/*
 * function hiddenOnlyField(panel, fieldName){ View.panels.get('pms_schedule').fields.get('pms.interval_4').fieldDef.required = true; }
 */

function getPrimaryKeysForSelectedRows(grid) {

    var selectedRows = new Array();

    var dataRows = grid.getDataRows();
    for (var r = 0; r < dataRows.length; r++) {
        var dataRow = dataRows[r];

        var selectionCheckbox = dataRow.firstChild.firstChild;
        if (selectionCheckbox.checked) {
            var rowKeys = grid.getPrimaryKeysForRow(grid.rows[r]);
            selectedRows.push(rowKeys);
        }
    }
    return selectedRows;
}

/*
 * function rm_procedure_onClick(){ resetTitle(''); //1 get pms_id var grid = View.panels.get('rm_procedure_select'); var selectedRow = grid.rows[grid.selectedRowIndex]; var pmsID =
 * selectedRow["pms.pms_id"]; updateIntervalTypeOptions(true); //2 refresh the form panels var restriction = new Ab.view.Restriction(); restriction.addClause("pms.pms_id", pmsID, "=", true);
 * refreshPmsPanel(restriction, "HK"); if (defPMSchedController['openFrom'] == 'forecast') { $("instructions").innerHTML = getMessage("instructions1") + "<br>" + getMessage("instructions2") + "<br>&nbsp;&nbsp;" +
 * getMessage("instructions21") + "<br>" + getMessage("instructions3") + "<br>&nbsp;&nbsp;" + getMessage("instructions31"); } }
 */

// update the intervalTypefield of pms
function updateIntervalTypeOptions() {
    var intervalType = View.panels.get("pms_schedule").getFieldElement("pms.interval_type");
    var options = intervalType.options;
    if ('HK' == View.controllers.get("defPMSched").pmpType) {
        if (getIndexofValue("i", options) != -1) {
            intervalType.remove(getIndexofValue("i", options));
            intervalType.remove(getIndexofValue("h", options));
            intervalType.remove(getIndexofValue("e", options));
        }
    } else {
        if (getIndexofValue("i", options) == -1) {
            if (getIndexofValue("a", options) != -1) {
                intervalType.remove(getIndexofValue("a", options));
            }
            var option = new Option(getMessage("miles"), "i");
            intervalType.options.add(option);
            option = new Option(getMessage("hours"), "h");
            intervalType.options.add(option);
            option = new Option(getMessage("meter"), "e");
            intervalType.options.add(option);
            option = new Option(getMessage("manual"), "a");
            intervalType.options.add(option);
        }
    }

}

function getIndexofValue(value, options) {
    for (var i = 0; i < options.length; i++) {
        if (options[i].value == value) {
            return i;
        }
    }
    return -1;
}

function refreshPmsPanelForSingleRecord(restriction) {
    if (!restriction) {
        return;
    }

    var pmsInfoPanel = View.panels.get('pms_info');
    pmsInfoPanel.refresh(restriction);
    var record = pmsInfoPanel.getRecord();

    var pmsEqBasicPanel = View.panels.get('pms_eq_basic');
    pmsEqBasicPanel.setRecord(record);
    var pmsRmBasicPanel = View.panels.get('pms_rm_basic');
    pmsRmBasicPanel.setRecord(record);
    var pmsSchedulePanel = View.panels.get('pms_schedule');
    pmsSchedulePanel.setRecord(record);
    var pmsServiceContractPanel = View.panels.get('pms_service_contract');
    pmsServiceContractPanel.setRecord(record);
    var pmsOtherPanel = View.panels.get('pms_other');
    pmsOtherPanel.setRecord(record);

    // $("taskPriority").value = record.values["pms.priority"];

    var openerController = View.controllers.get("defPMSched");
    var isEq = ("EQ" == openerController.pmpType) ? true : false;
    pmsEqBasicPanel.show(isEq);
    pmsRmBasicPanel.show(!isEq);
    pmsSchedulePanel.show(true);
    pmsServiceContractPanel.show(true);
    pmsOtherPanel.show(true);

    openerController.showInformationInForm(openerController, pmsInfoPanel, "");
}

function setMultiplePmsValue(pmsValue, pmsId) {
    beforeSetMultiplePmsValue();

    var restriction = new Ab.view.Restriction();
    restriction.addClause("pms.pms_id", pmsId, "=", true);
    refreshPmsPanelForSingleRecord(restriction);

    setPmsFieldValues(pmsValue);
}

function beforeSetMultiplePmsValue() {
    // kb:3024381 ,it was caused by new build change the showfield method
    var pmpType = View.controllers.get("defPMSched").pmpType;
    if ('EQ' == pmpType) {
        var pmsEqBasic = View.panels.get('pms_eq_basic');
        pmsEqBasic.showField('pms.pms_id', false);
        pmsEqBasic.showField('pms.eq_id', false);
    } else {
        var pmsRmBasic = View.panels.get('pms_rm_basic');
        pmsRmBasic.showField('pms.pms_id', false);
        pmsRmBasic.showField('pms.site_id', false);
        pmsRmBasic.showField('pms.bl_id', false);
        pmsRmBasic.showField('pms.fl_id', false);
        pmsRmBasic.showField('pms.rm_id', false);
    }

    var pmsSchedule = View.panels.get('pms_schedule')
    pmsSchedule.showField('pms.date_last_completed', false);
    pmsSchedule.showField('pms.date_next_todo', false);
    pmsSchedule.showField('pms.hours_calc', false);

    var pmsOther = View.panels.get('pms_other')
    pmsOther.showField('pms.meter_last_pm', false);
    pmsOther.showField('pms.nactive', false);

    // when isPmsDetailsHidden =false ,say the detail hidden ,so didn't compare the new and old value
    defPMSchedController['isPmsDetailsHidden'] = false;

    // initial set interval type list
    updateIntervalTypeOptions();
}

function setPmsFieldValues(pmsValue) {
    var variesValue = View.controllers.get("defPMSched").variesValue;
    var variesText = View.controllers.get("defPMSched").variesText;

    if ('EQ' == defPMSchedController.pmpType) {
        setSimpleFieldValue("pms_eq_basic_pms.dv_id", pmsValue.dv_id, variesValue, variesText);
        setSimpleFieldValue("pms_eq_basic_pms.dp_id", pmsValue.dp_id, variesValue, variesText);
        setSimpleFieldValue("pms_eq_basic_pms.pm_group", pmsValue.pm_group, variesValue, variesText);
    } else {
        setSimpleFieldValue("pms_rm_basic_pms.dv_id", pmsValue.dv_id, variesValue, variesText);
        setSimpleFieldValue("pms_rm_basic_pms.dp_id", pmsValue.dp_id, variesValue, variesText);
        setSimpleFieldValue("pms_rm_basic_pms.pm_group", pmsValue.pm_group, variesValue, variesText);
    }

    setDateFieldValue("pms_schedule_pms.date_first_todo", pmsValue.date_first_todo, variesValue, variesText);
    setDateFieldValue("pms_schedule_pms.date_next_alt_todo", pmsValue.date_next_alt_todo, variesValue, variesText);

    setOptionFieldValue("pms_schedule_pms.fixed", pmsValue.fixed, variesValue, variesText);
    setOptionFieldValue("pms_schedule_pms.interval_type", pmsValue.interval_type, variesValue, variesText);
    // setOptionFieldValue("taskPriority", pmsValue.priority, variesValue, variesText);
    setOptionFieldValue("pms_schedule_pms.interval_freq", pmsValue.interval_freq, variesValue, variesText);

    setSimpleFieldValue("pms_schedule_pms.hours_est", pmsValue.hours_est, variesValue, variesText);
    setSimpleFieldValue("pms_schedule_pms.interval_1", pmsValue.interval_1, variesValue, variesText);
    setSimpleFieldValue("pms_schedule_pms.interval_2", pmsValue.interval_2, variesValue, variesText);
    setSimpleFieldValue("pms_schedule_pms.interval_3", pmsValue.interval_3, variesValue, variesText);
    setSimpleFieldValue("pms_schedule_pms.interval_4", pmsValue.interval_4, variesValue, variesText);
    setSimpleFieldValue("pms_schedule_pms.int1_recurrence", pmsValue.int1_recurrence, variesValue, variesText);
    setSimpleFieldValue("pms_schedule_pms.int2_recurrence", pmsValue.int2_recurrence, variesValue, variesText);
    setSimpleFieldValue("pms_schedule_pms.int3_recurrence", pmsValue.int3_recurrence, variesValue, variesText);
    setSimpleFieldValue("pms_schedule_pms.int4_recurrence", pmsValue.int4_recurrence, variesValue, variesText);

    setSimpleFieldValue("pms_eq_basic_pms.criticality", pmsValue.criticality, variesValue, variesText);
    setSimpleFieldValue("pms_schedule_pms.due_date_from", pmsValue.due_date_from, variesValue, variesText);
    setSimpleFieldValue("pms_schedule_pms.days_late", pmsValue.days_late, variesValue, variesText);
    setSimpleFieldValue("pms_schedule_pms.days_early", pmsValue.days_early, variesValue, variesText);

    setSimpleFieldValue("pms_other_pms.total_unit", pmsValue.total_unit, variesValue, variesText);
    setSimpleFieldValue("pms_other_pms.units", pmsValue.units, variesValue, variesText);
    setSimpleFieldValue("pms_other_pms.comments", pmsValue.comments, variesValue, variesText);
}

function setSimpleFieldValue(fieldElementId, actualValue, variesValue, variesText) {
    if (actualValue == variesValue) {
        if (fieldElementId == 'pms_schedule_pms.int1_recurrence' || fieldElementId == 'pms_schedule_pms.int2_recurrence' || fieldElementId == 'pms_schedule_pms.int3_recurrence'
                || fieldElementId == 'pms_schedule_pms.int4_recurrence') {
            $(fieldElementId).value = variesValue;
        } else {
            $(fieldElementId).value = variesText;
        }

    } else {
        $(fieldElementId).value = actualValue;
    }
}

function setDateFieldValue(fieldElementId, actualValue, variesValue, variesText) {
    var tempValue = actualValue;
    if (actualValue != variesValue && actualValue != '') {
        var actualValueArr = actualValue.split('-');
        var day = parseInt(actualValueArr[2], 10);
        var month = parseInt(actualValueArr[1], 10);
        var year = parseInt(actualValueArr[0], 10);

        // kb:3037348,by comments about date format problem.
        tempValue = returnFromIsoDate(day, month, year);
        tempValue.trim();
    }
    setSimpleFieldValue(fieldElementId, tempValue, variesValue, variesText);
}

function setOptionFieldValue(fieldElementId, actualValue, variesValue, variesText) {
    if (actualValue == variesValue) {
        var selectElement = document.getElementById(fieldElementId);
        if (!isExistsVaries(selectElement)) {
            var option = new Option(variesText, variesValue);
            selectElement.options.add(option);
            selectElement.value = variesValue;
        }
        selectElement.value = variesValue;
    }
}

/**
 * Reback date format from iso date by arrDateShortPattern;
 * @param day
 * @param month
 * @param year
 * @returns {String}
 */
function returnFromIsoDate(day, month, year) {
    var date = "";
    for (var i = 0; i < 3; i++) {
        var temp = arrDateShortPattern[i];
        if (temp != null) {
            if (i != 0) {
                if (temp.indexOf("Y") >= 0) {
                    date = date + "/" + year;
                } else if (temp.indexOf("M") >= 0) {
                    date = date + "/" + month;
                } else if (temp.indexOf("D") >= 0) {
                    date = date + "/" + day;
                }
            } else {
                if (temp.indexOf("Y") >= 0) {
                    date = year;
                } else if (temp.indexOf("M") >= 0) {
                    date = month;
                } else if (temp.indexOf("D") >= 0) {
                    date = day;
                }
            }
        }
    }
    return date;
}

// use for the selectlist ,if it return true ,proveing the selectlist has '<varies>' option
function isExistsVaries(selectElement) {
    if (selectElement.length > 0) {
        if (selectElement.options[selectElement.length - 1].value == View.controllers.get("defPMSched").variesValue) {
            return true;
        }
    }
}

// get the pmsvalue when save the selected pms records
function getMultiplePmsValue() {

    var pmsBasic = null;
    if ("EQ" == defPMSchedController.pmpType) {
        var pmsBasic = View.panels.get("pms_eq_basic");
    } else {
        var pmsBasic = View.panels.get("pms_rm_basic");
    }
    var dv_id = pmsBasic.getFieldValue("pms.dv_id");
    var dp_id = pmsBasic.getFieldValue("pms.dp_id");
    var pm_group = pmsBasic.getFieldValue("pms.pm_group");

    var pmsschedule = View.panels.get("pms_schedule");
    var date_last_completed = pmsschedule.getFieldValue("pms.date_last_completed");
    var date_next_todo = pmsschedule.getFieldValue("pms.date_next_todo");

    var date_first_todo = $("pms_schedule_pms.date_first_todo").value;
    var datefirsttodo_regexp = date_first_todo.match(defPMSchedController.variesRegexp);
    if (datefirsttodo_regexp == null) {
        date_first_todo = getDateWithISOFormat(date_first_todo);
    }

    var date_next_alt_todo = $("pms_schedule_pms.date_next_alt_todo").value;
    var date_next_alt_todo_regexp = date_next_alt_todo.match(defPMSchedController.variesRegexp);
    if (date_next_alt_todo_regexp == null) {
        date_next_alt_todo = getDateWithISOFormat(date_next_alt_todo);
    }

    var hours_calc = pmsschedule.getFieldValue("pms.hours_calc");
    var hours_est = pmsschedule.getFieldValue("pms.hours_est");
    var interval_type = pmsschedule.getFieldValue("pms.interval_type");
    var fixed = pmsschedule.getFieldValue("pms.fixed");
    var interval_freq = pmsschedule.getFieldValue("pms.interval_freq");
    var interval_1 = '';
    var interval_2 = '';
    var interval_3 = '';
    var interval_4 = '';
    var int1_recurrence = '';
    var int2_recurrence = '';
    var int3_recurrence = '';
    var int4_recurrence = '';

    if (interval_type == 'r') {
        setCurrentRecurrenceFreqTabValueToHiddenField();
        int1_recurrence = pmsschedule.getFieldValue("pms.int1_recurrence");
        int2_recurrence = pmsschedule.getFieldValue("pms.int2_recurrence");
        int3_recurrence = pmsschedule.getFieldValue("pms.int3_recurrence");
        int4_recurrence = pmsschedule.getFieldValue("pms.int4_recurrence");

    } else {
        interval_1 = pmsschedule.getFieldValue("pms.interval_1");
        interval_2 = pmsschedule.getFieldValue("pms.interval_2");
        interval_3 = pmsschedule.getFieldValue("pms.interval_3");
        interval_4 = pmsschedule.getFieldValue("pms.interval_4");
    }

    var pmsother = View.panels.get("pms_other");
    var nactive = pmsother.getFieldValue("pms.nactive");
    var total_unit = pmsother.getFieldValue("pms.total_unit");
    var units = pmsother.getFieldValue("pms.units");
    var comments = pmsother.getFieldValue("pms.comments");
    var pmpUnitsHour = pmsother.getFieldValue("pmp.units_hour");

    var ptStoreLoc = pmsother.getFieldValue("pms.pt_store_loc_id");
    // var priority = $("taskPriority").value;

    var fieldAndValueArray = [ [ 'dv_id', dv_id ], [ 'dp_id', dp_id ], [ 'pm_group', pm_group ], [ 'date_first_todo', date_first_todo ], [ 'date_next_alt_todo', date_next_alt_todo ],
            [ 'hours_est', hours_est ], [ 'interval_type', interval_type ], [ 'fixed', fixed ], [ 'interval_freq', interval_freq ], [ 'interval_1', interval_1 ], [ 'interval_2', interval_2 ],
            [ 'interval_3', interval_3 ], [ 'interval_4', interval_4 ], [ 'int1_recurrence', int1_recurrence ], [ 'int2_recurrence', int2_recurrence ], [ 'int3_recurrence', int3_recurrence ],
            [ 'int4_recurrence', int4_recurrence ], [ 'criticality', pmsBasic.getFieldValue("pms.criticality") ], [ 'due_date_from', pmsschedule.getFieldValue("pms.due_date_from") ],
            [ 'days_late', pmsschedule.getFieldValue("pms.days_late") ], [ 'days_early', pmsschedule.getFieldValue("pms.days_early") ], [ 'total_unit', total_unit ], [ 'units', units ], [ 'comments', comments ],
            [ 'priority', "1" ], [ 'pt_store_loc_id', ptStoreLoc ],["units_hour", pmpUnitsHour] ];

    return setFieldValueForGivenField(fieldAndValueArray);
}

/**
 * Convert paramter when field value from form is varies.
 * @param fieldAndValueArray
 * @returns {Object}
 */
function setFieldValueForGivenField(fieldAndValueArray) {
    var pmsValue = new Object();
    for (var i = 0; i < fieldAndValueArray.length; i++) {

        var field = fieldAndValueArray[i][0];
        var value = fieldAndValueArray[i][1];

        if (field == 'int1_recurrence' || field == 'int2_recurrence' || field == 'int3_recurrence' || field == 'int4_recurrence') {
            pmsValue["pms." + field] = value;

        } else {
            if (value.match(defPMSchedController.variesRegexp) != null) {
                pmsValue["pms." + field] = View.controllers.get("defPMSched").variesValue;
            } else {
                pmsValue["pms." + field] = value;
            }
        }

    }
    return pmsValue;
}

function createObjectFromPmsRecord(pmsRecord) {
    var pmsRestr = new Object();

    pmsRestr["pmpstr.tr_id"] = pmsRecord['pmpstr.tr_id'];
    pmsRestr["pmressum.date_todo.to"] = pmsRecord['pmressum.date_todo.to'];
    pmsRestr["pmressum.date_todo.from"] = pmsRecord['pmressum.date_todo.from'];
    pmsRestr["pmsd.date_todo"] = pmsRecord['pmsd.date_todo'];
    pmsRestr["weekormonth"] = pmsRecord['weekormonth'];

    if (pmsRecord['pms.pm_group'] != "") {
        pmsRestr["pms.pm_group"] = pmsRecord['pms.pm_group'];
    }
    if (pmsRecord['pms.bl_id'] != "") {
        pmsRestr["pms.bl_id"] = pmsRecord['pms.bl_id'];
    }
    if (pmsRecord['pms.fl_id'] != "") {
        pmsRestr["pms.fl_id"] = pmsRecord['pms.fl_id'];
    }
    if (pmsRecord['pms.site_id'] != "") {
        pmsRestr["pms.site_id"] = pmsRecord['pms.site_id'];
    }
    if (pmsRecord['pmp.tr_id'] != "") {
        pmsRestr["pmp.tr_id"] = pmsRecord['pmp.tr_id'];
    }

    return pmsRestr;
}

/*
 * Enable 'Copy' action only when select one grid row ( pms record).
 */
function enableCopyButton(selectGrid) {
    var selections = selectGrid.getSelectedRows().length;
    if (selections == 1) {
        View.panels.get("pms_info").enableButton("copy", true);
    } else {
        View.panels.get("pms_info").enableButton("copy", false);
    }
}

/*
 * Show frequency base on the interval type.
 */
function showFrequencyBaseOnIntervalType() {
    if (defPMSchedController['records'].length == 0) {
        return;
    }

    View.panels.get("editRecurringRule").show(false);
    jQuery('#selectRecurrenceFreqTabs_layoutWrapper').hide();
    var form = View.panels.get("pms_schedule");
    var intervalType = form.getFieldValue('pms.interval_type');

    // if intervalType is Recurrence Pattern, then show Recurrence Toolkit.
    if (intervalType == 'r') {
        form.showField('pms.interval_1', false);
        form.showField('pms.interval_2', false);
        form.showField('pms.interval_3', false);
        form.showField('pms.interval_4', false);
        form.showField('pms.interval_freq', false);
        setRecurringPattern();

    } else if (intervalType != '<varies>') {
        // if intervalType is not Recurrence Pattern, then show old Frequency fields
        form.showField('pms.interval_1', true);
        form.showField('pms.interval_2', true);
        form.showField('pms.interval_3', true);
        form.showField('pms.interval_4', true);
        form.showField('pms.interval_freq', true);
    } else {
        // if varies, hide all fields related to interval
        form.showField('pms.interval_1', false);
        form.showField('pms.interval_2', false);
        form.showField('pms.interval_3', false);
        form.showField('pms.interval_4', false);
        form.showField('pms.interval_freq', false);
    }

}

/*
 * Show frequency base on the interval type.
 */
function setRecurringPattern() {
    var form = View.panels.get("pms_schedule");
    var recurringPatternCtrl = View.controllers.get("abRecurringPatternCtrl");
    var pattern1 = form.getFieldValue("pms.int1_recurrence");
    var pattern2 = form.getFieldValue("pms.int2_recurrence");
    var pattern3 = form.getFieldValue("pms.int3_recurrence");
    var pattern4 = form.getFieldValue("pms.int4_recurrence");
    if (pattern1 != '') {
        View.panels.get("selectRecurrenceFreqTabs").selectTab('int1_recurrence');
        recurringPatternCtrl.setRecurringPattern(pattern1);
    } else if (pattern2 != '') {
        View.panels.get("selectRecurrenceFreqTabs").selectTab('int2_recurrence');
        recurringPatternCtrl.setRecurringPattern(pattern2);
    } else if (pattern3 != '') {
        View.panels.get("selectRecurrenceFreqTabs").selectTab('int3_recurrence');
        recurringPatternCtrl.setRecurringPattern(pattern3);
    } else if (pattern4 != '') {
        View.panels.get("selectRecurrenceFreqTabs").selectTab('int4_recurrence');
        recurringPatternCtrl.setRecurringPattern(pattern4);
    } else {
        View.panels.get("selectRecurrenceFreqTabs").selectTab('int1_recurrence');
        recurringPatternCtrl.setRecurringPattern('');
    }

    jQuery('#selectRecurrenceFreqTabs_layoutWrapper').show();

    jQuery('#selectRecurrenceFreqTabs_layoutWrapper').height(40);
}

/**
 * 'afterTabChange' event listener.
 * @param {Object} tabPanel
 * @param {Object} newTabName
 */
function afterRecurrenceFreqTabChange(tabPanel, newTabName) {
    var form = View.panels.get("pms_schedule");
    var recurringPatternCtrl = View.controllers.get("abRecurringPatternCtrl");

    // store current tab recurring pattern to hidden field
    var currentTabPattern = recurringPatternCtrl.getRecurringPattern();
    form.setFieldValue('pms.' + tabPanel.previouslySelectedTabName, currentTabPattern);

    // load new recurring pattern field to selected Frequency tab
    var newTabPattern = form.getFieldValue('pms.' + newTabName);
    recurringPatternCtrl.setRecurringPattern(newTabPattern);
}

/**
 * 'afterTabChange' event listener.
 * @param {Object} tabPanel
 * @param {Object} newTabName
 */
function setCurrentRecurrenceFreqTabValueToHiddenField() {
    var form = View.panels.get("pms_schedule");
    var recurringPatternCtrl = View.controllers.get("abRecurringPatternCtrl");
    var currentTabPattern = recurringPatternCtrl.getRecurringPattern();
    form.setFieldValue('pms.' + View.panels.get("selectRecurrenceFreqTabs").selectedTabName, currentTabPattern);
}

/**
 * Update tree restriction for level and set filter restriction.
 */
function updateTreeRestrictionForLevel(parentNode, level, restriction) {
    if (level > 0) {
        restriction.removeClause('eq_system.auto_number');
        restriction.addClause('eq_system.eq_id_master', parentNode.data['eq_system.eq_id_depend'], '=');
    }
    if (this.parameters.filterRestriction) {
        var filterRestriction = this.parameters.filterRestriction;
        var levelRestriction = '';
        for (var i = (level + 1), length = 10; i < length; i++) {
            levelRestriction += 'eq.level' + i + '=eq_system.eq_id_depend ';
            if (i < (length - 1)) {
                levelRestriction += ' OR ';
            }
        }
        this.parameters.filterRestriction = filterRestriction.replace(/%{levelRestriction}/g, levelRestriction);
    }
}

/**
 * Set tree node label.
 * @param node
 */
function afterGeneratingTreeNode(node) {
    if (valueExists(node.data['eq_system.eq_id_depend']) && valueExists(node.data['eq_system.system_level'])) {
        var depend = node.data['eq_system.eq_id_depend'], systemLvl = node.data['eq_system.system_level'];
        var label = node.data['eq_system.system_name'] + ' (' + depend + ')';
        // label += ' ' + node.data['eq_system.sort_order'];
        var nodeLabel = '<div class="nodeLabel">', labelId = 'label' + node.id + '_0';
        nodeLabel += '<div id="' + labelId + '" class="label color_' + systemLvl + '"><span class="ygtvlabel">' + label + '</span></div>';
        nodeLabel += '</div>';
        node.label = nodeLabel;
    }
}

function onClickNodeHandler(context) {
    var crtTreeNode = View.panels.get(context.command.parentPanelId).lastNodeClicked;
    var eqId = "'" + crtTreeNode.data['eq_system.eq_id_depend'] + "'";
    var restriction = "EXISTS(SELECT 1 FROM (SELECT"
                +" eq_system1.eq_id_depend ${sql.as} level1,"
                +" eq_system2.eq_id_depend ${sql.as} level2,"
                +" eq_system3.eq_id_depend ${sql.as} level3,"
                +" eq_system4.eq_id_depend ${sql.as} level4,"
                +" eq_system5.eq_id_depend ${sql.as} level5,"
                +" eq_system6.eq_id_depend ${sql.as} level6,"
                +" eq_system7.eq_id_depend ${sql.as} level7,"
                +" eq_system8.eq_id_depend ${sql.as} level8,"
                +" eq_system9.eq_id_depend ${sql.as} level9,"
                +" eq_system10.eq_id_depend ${sql.as} level10"
                +" FROM eq_system ${sql.as} eq_system1"
                +" LEFT JOIN eq_system ${sql.as} eq_system2 ON eq_system1.eq_id_depend=eq_system2.eq_id_master and eq_system2.eq_id_depend != eq_system1.eq_id_depend" 
                +" LEFT JOIN eq_system ${sql.as} eq_system3 ON eq_system2.eq_id_depend=eq_system3.eq_id_master and eq_system3.eq_id_depend != eq_system2.eq_id_depend" 
                +" LEFT JOIN eq_system ${sql.as} eq_system4 ON eq_system3.eq_id_depend=eq_system4.eq_id_master and eq_system4.eq_id_depend != eq_system3.eq_id_depend" 
                +" LEFT JOIN eq_system ${sql.as} eq_system5 ON eq_system4.eq_id_depend=eq_system5.eq_id_master and eq_system5.eq_id_depend != eq_system4.eq_id_depend"
                +" LEFT JOIN eq_system ${sql.as} eq_system6 ON eq_system5.eq_id_depend=eq_system6.eq_id_master and eq_system6.eq_id_depend != eq_system5.eq_id_depend"
                +" LEFT JOIN eq_system ${sql.as} eq_system7 ON eq_system6.eq_id_depend=eq_system7.eq_id_master and eq_system7.eq_id_depend != eq_system6.eq_id_depend"
                +" LEFT JOIN eq_system ${sql.as} eq_system8 ON eq_system7.eq_id_depend=eq_system8.eq_id_master and eq_system8.eq_id_depend != eq_system7.eq_id_depend"
                +" LEFT JOIN eq_system ${sql.as} eq_system9 ON eq_system8.eq_id_depend=eq_system9.eq_id_master and eq_system9.eq_id_depend != eq_system8.eq_id_depend"
                +" LEFT JOIN eq_system ${sql.as} eq_system10 ON eq_system9.eq_id_depend=eq_system10.eq_id_master and eq_system10.eq_id_depend != eq_system9.eq_id_depend"
                +" WHERE eq_system1.eq_id_depend =" +　eqId
            +" ) eq_sys WHERE (level1 = eq.eq_id OR level2 = eq.eq_id OR level3 = eq.eq_id OR level4 = eq.eq_id OR level5 = eq.eq_id OR level6 = eq.eq_id OR level7 = eq.eq_id"
            +" OR level8 = eq.eq_id OR level9 = eq.eq_id OR level10 = eq.eq_id))";
    
    View.panels.get('eq_procedure_select').refresh(restriction);

}