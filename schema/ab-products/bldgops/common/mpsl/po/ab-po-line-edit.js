var poLineMangeCtrl=View.createController('poLineMangeCtrl',{
	//Part code in console form.
	selectPartCode: "",
	//Receiving location.
	selectPoId: "",
	//Selected Vendor Code.
	selectVnId:"",
	selectReceivingLocation: "",
	//Po status when selected
	poStatus: "",
	//purchase order status dom
	statusOptionDom: null,
	//purchase order line status dom
	poLineStatusOptionDom: null,
	
	afterViewLoad: function(){
		this.statusOptionDom = jQuery(this.purchaseOrderInfo.fields.get('po.status').dom);
		this.poLineStatusOptionDom = jQuery(this.abPoLineEditForm.fields.get('po_line.status').dom);
		this.abPoLineEditForm.fields.get("po_line.catno").actions.get(0).command.commands[0].beforeSelect = this.beforeCatNoSelectCf.createDelegate(this);
		this.abPoLineEditForm.fields.get("po_line.catno").actions.get(0).command.commands[0].autoComplete=false;
	},
	
	afterInitialDataFetch: function(){
		this.purchaseOrderListPanel.addParameter('checkedReceivedOrNotOnly',true);
		this.purchaseOrderListPanel.refresh();
	},

	/**
     * Called before click select value of po_line.catno
     */
    beforeCatNoSelectCf : function(command) {
        var restriction = new Ab.view.Restriction();
        restriction.addClause('pv.vn_id',this.selectVnId,'=');
        restriction.addClause('pv.vn_pt_num',null,'IS NOT NULL');
        command.dialogRestriction = restriction;
    },
    /**
     * Clone Purchase Order Status field drop down list item.
     */
	purchaseOrderInfo_beforeRefresh: function(){
		var newStatusOption = this.statusOptionDom.clone();
		newStatusOption.replaceAll(this.purchaseOrderInfo.fields.get('po.status').dom);
		this.purchaseOrderInfo.fields.get('po.status').dom = newStatusOption.get(0);
	},
	
	/**
	 * Clone Purchase order line status field drop down list item.
	 */
	abPoLineEditForm_beforeRefresh: function(){
		var newStatusOption = this.poLineStatusOptionDom.clone();
		newStatusOption.replaceAll(this.abPoLineEditForm.fields.get('po_line.status').dom);
		this.abPoLineEditForm.fields.get('po_line.status').dom = newStatusOption.get(0);
	},
	
	/**
	 * event handler after panel refresh.
	 */
	purchaseOrderInfo_afterRefresh: function(){
		this.purchaseOrderInfo.setTitle(String.format(getMessage('poEditInfoTitle'),this.selectPoId));
	},
	
	/**
	 * Show or hide big bad filter
	 */
	consoleForm_onBtnMoreOrLess : function() {
		this.consoleMoreForm.toggleCollapsed();
		var action=this.consoleForm.actions.get('moreOrLess');
			action.setTitle(this.consoleMoreForm.collapsed ? getMessage('filterMore') : getMessage('filterLess'));
	},
	
	/**
	 * Click Show button to refresh the purchase order list based on the filter criteria.
	 */
	consoleForm_onFilter: function(){
		
		if(!this.consoleMoreForm.collapsed){
			this.consoleMoreForm.toggleCollapsed();
			this.consoleForm.actions.get('moreOrLess').setTitle(getMessage('filterMore'));
		}
		var poNumber=this.consoleForm.getFieldValue('po.po_number');
		var vnId=this.consoleForm.getFieldValue('po.vn_id');
		var receiveLoc=this.consoleForm.getFieldValue('po.receiving_location');
		var partCode=this.consoleForm.getFieldValue('po.vfPartId');
		var dateRequestFrom=this.consoleMoreForm.getFieldValue('date_request_from');
		var dateRequestTo=this.consoleMoreForm.getFieldValue('date_request_to');
		var acId=this.consoleMoreForm.getFieldValue('po.ac_id');
		var poStatus=this.consoleMoreForm.getFieldValue('po.status');
		var isUnderstockedChecked=$('receivedCkbx').checked;

		if(valueExistsNotEmpty(dateRequestFrom)&&valueExistsNotEmpty(dateRequestTo)){
			if (compareISODates(dateRequestTo, dateRequestFrom)){
				View.alert(getMessage('dateRequestedFromCannotEarlierThanDateRequestedToMsg'));
				return;
			}
		}
		var poRestriction=new Ab.view.Restriction();
		if(valueExistsNotEmpty(poNumber)){
			poRestriction.addClause('po.po_number',poNumber+'%','LIKE');
		}
		if(valueExistsNotEmpty(vnId)){
			poRestriction.addClause('po.vn_id',vnId+'%','LIKE');
		}
		if(valueExistsNotEmpty(receiveLoc)){
			poRestriction.addClause('po.receiving_location',receiveLoc+'%','LIKE');
		}
		if(valueExistsNotEmpty(partCode)){
			this.selectPartCode=partCode;
			this.purchaseOrderListPanel.addParameter('partId',partCode);
			this.purchaseOrderListPanel.addParameter('hasPartIdOnly',true);
		}else{
			this.purchaseOrderListPanel.addParameter('hasPartIdOnly',false);
			this.selectPartCode="";
		}
		if(valueExistsNotEmpty(dateRequestFrom)){
			poRestriction.addClause('po.date_request',dateRequestFrom,'&gt;=');
		}
		if(valueExistsNotEmpty(dateRequestTo)){
			poRestriction.addClause('po.date_request',dateRequestTo,'&lt;=');
		}
		if(valueExistsNotEmpty(acId)){
			poRestriction.addClause('po.ac_id',acId+'%','LIKE');
		}
		if(valueExistsNotEmpty(poStatus)){
			poRestriction.addClause('po.status',poStatus,'=');
		}
		if(isUnderstockedChecked){
			this.purchaseOrderListPanel.addParameter('checkedReceivedOrNotOnly',true);
		}else{
			this.purchaseOrderListPanel.addParameter('checkedReceivedOrNotOnly',false);
		}
		
		this.purchaseOrderListPanel.refresh(poRestriction);
		
		//Hidden purchase order edit froms
		this.purchaseOrderInfo.show(false);
		this.purchaseOrderDetail.show(false);
		this.shippingAndBillingForm.show(false);
		
		//Hidden purchase order item edit forms
		this.abPoLineEditForm.show(false);
		
		this.abPoLineList.show(false);
	},
	
	/**
	 * Clear console form fields value.
	 */
	consoleForm_onClear: function(){
		this.consoleForm.clear();
		this.consoleMoreForm.clear();
		this.selectPartCode="";
		$('receivedCkbx').checked=true;
	},
	
	/**
	 * click purchase order list item to show purchase order detail and purchase order line list. 
	 */
	onClickPurchaseOrderList: function(){
		var selectRowIndex=this.purchaseOrderListPanel.selectedRowIndex;
		var selectRowRecord=this.purchaseOrderListPanel.gridRows.get(selectRowIndex).getRecord();
		
		var poId=selectRowRecord.getValue('po.po_id');
		this.selectPoId=poId;
		var vnId=selectRowRecord.getValue('po.vn_id');
		this.selectVnId=vnId;
		var receiveLocation=selectRowRecord.getValue('po.receiving_location');
		//Set receiving location to global environment.
		this.selectReceivingLocation=receiveLocation;
		var poStatus=selectRowRecord.getValue('po.status');
		this.poStatus=poStatus;
		
		var poRes=new Ab.view.Restriction();
		poRes.addClause('po.po_id',poId,'=');
		
		var poLineRes=new Ab.view.Restriction();
		poLineRes.addClause('po_line.po_id',poId,'=');
		
		//Hidden purchase order item panel
		this.abPoLineEditForm.show(false);
		
		this.purchaseOrderInfo.refresh(poRes);
		this.purchaseOrderInfo.setTitle(String.format(getMessage('poEditInfoTitle'),poId));

		this.purchaseOrderDetail.refresh(poRes);
		this.shippingAndBillingForm.refresh(poRes);
		
		this.enablePurchaseOrderFormFields();
		this.abPoLineList.addParameter('poId',poId);
		if(valueExistsNotEmpty(this.selectPartCode)){
			this.abPoLineList.addParameter('partCode',this.selectPartCode);
			this.abPoLineList.addParameter('hasPartCode', true);
		}else{
			this.abPoLineList.addParameter('hasPartCode', false);
		}
		
		this.abPoLineList.refresh(poLineRes);
		this.abPoLineList.setTitle(String.format(getMessage('poLineListPanelTitle'),poId,vnId,receiveLocation));
	},
	
	/**
	 * Enable or disable purchase order form by purchase order status.
	 */
	enablePurchaseOrderFormFields: function(){
		var poStatus=this.purchaseOrderInfo.getFieldValue('po.status');
		var enableFields;
		//disable all panel fields and actions when initial.
		this.purchaseOrderDetail.actions.get('approved').show(false);
		this.purchaseOrderDetail.actions.get('reject').show(false);
		this.abPoLineList.actions.get('addNew').enable(false);
		
		enableFields=['po.vn_id','po.receiving_location','po.status','po.comments'];
		enablePanelFields('purchaseOrderInfo',enableFields,false);

		enableFields=['po.ac_id','po.po_number','po.source','po.federal_tax','po.em_id','po.state_tax','po.shipping'];
		enablePanelFields('purchaseOrderDetail',enableFields,false);
		
		enableFields=['po.shipping_em_id','po.billing_em_id','po.ship_address1','po.bill_address1','po.ship_address2','po.bill_address2','po.ship_city_id','po.bill_city_id','po.ship_state_id','po.bill_state_id','po.ship_zip','po.bill_zip'];
		enablePanelFields('shippingAndBillingForm',enableFields,false);
		
		if(poStatus=="Requested"||poStatus=="Rejected"){
			enablePanelFields('purchaseOrderInfo',['po.vn_id','po.receiving_location'],true);
			enablePanelFields('purchaseOrderDetail',['po.ac_id','po.po_number','po.source','','po.state_tax','po.shipping','po.em_id'],true);
			
			this.abPoLineList.actions.get('addNew').enable(true);
			//Users who are in the “PO APPROVER” security group will have the ability to approve or reject purchase orders.
			//This is done in the Edit Purchase Order panel.  Buttons for Approve and Reject will appear if the Order Details panel if the status = Requested
			if(poStatus=="Requested"){
				if(View.user.isMemberOfGroup('PO APPROVER')){
					this.purchaseOrderDetail.actions.get('approved').show(true);
					this.purchaseOrderDetail.actions.get('reject').show(true);
				}
			}
		}else{
			//KB#3052063 The following fields should be editable if the purchase order status is Requested or Rejected, of if the user is in the PO APPROVER security group.
			if(View.user.isMemberOfGroup('PO APPROVER')){
				enablePanelFields('purchaseOrderDetail',['po.ac_id','po.po_number','po.source'],true);
			}
		}
		
		if(poStatus=="Rejected"||poStatus=="Approved"||poStatus=="Issued"){
			this.purchaseOrderInfo.enableField('po.status',true);
			if(poStatus=='Approved'){
				this.abPoLineList.actions.get('addNew').enable(true);
			}
		}
		
		if(poStatus!="Received"&&poStatus!="Partially Received"&&poStatus!="Error"){
			enablePanelFields('purchaseOrderInfo',['po.comments'],true);
			enableFields=['po.shipping_em_id','po.billing_em_id','po.ship_address1','po.bill_address1','po.ship_address2','po.bill_address2','po.ship_city_id','po.bill_city_id','po.ship_state_id','po.bill_state_id','po.ship_zip','po.bill_zip'];
			enablePanelFields('shippingAndBillingForm',enableFields,true);
		}
		
		//disable addnew button if one or more po line item has been received.
		if(this.hasPoItemsReceivedorErrors()){
			this.abPoLineList.actions.get('addNew').enable(false);
		}
		//Hidden purchase order status field drop down list item.
		this.hiddeStatusFieldDropDownListItem();
	},
	
	/**
	 * Hidden purchase order status field drop down list item.
	 */
	hiddeStatusFieldDropDownListItem: function(){
		//Remove purchase order status of 'Received' or 'Partially Received' or 'Error'
		var poStatusField=this.purchaseOrderInfo.fields.get('po.status').dom;
		
		for(var i=poStatusField.length-1;i>0;i--){
			if(poStatusField[i].value=='Received'||poStatusField[i].value=='Partially Received'||poStatusField[i].value=='Error'){
				if(this.poStatus!='Received'&&this.poStatus!='Partially Received'&&this.poStatus!='Error'){
					poStatusField.remove(i);
				}
			}else{
				//If po status is 'Rejected' , 'Issued' status should not display in PO status field.
				if(poStatusField[i].value=='Issued'){
					if(this.poStatus=="Rejected"){
						poStatusField.remove(i);
					}
				}else if(poStatusField[i].value=='Rejected'){
					if(this.poStatus!="Rejected"){
						poStatusField.remove(i);
					}
				}else if(poStatusField[i].value=='Approved'){
					if(this.poStatus!="Approved"){
						poStatusField.remove(i);
					}
				}	
			}
		}
	},

	/**
	 * Event handler on click purchase order list item.
	 */
	onClickPurchaseOrderListItem: function(){
		var selectRowIndex=this.abPoLineList.selectedRowIndex;
		var selectRowRecord=this.abPoLineList.gridRows.get(selectRowIndex).getRecord();
		
		var poId=selectRowRecord.getValue('po_line.po_id');
		var poLineId=selectRowRecord.getValue('po_line.po_line_id');
		
		var poLineRes=new Ab.view.Restriction();
		poLineRes.addClause('po_line.po_id',poId,'=');
		poLineRes.addClause('po_line.po_line_id',poLineId,'=');
		
		//Hidden purchase order edit froms
		this.purchaseOrderInfo.show(false);
		this.purchaseOrderDetail.show(false);
		this.shippingAndBillingForm.show(false);
		//add parameter poId to purchase order line edit form.
		this.abPoLineEditForm.addParameter('poId',poId);
		//Show purchase order item edit forms
		this.abPoLineEditForm.refresh(poLineRes,false);
	},
	
	/**
	 * Event handler after panel refresh.
	 */
	abPoLineEditForm_afterRefresh: function(){
		this.enablePurchaseOrderItemFormFields();
		
		var poStatus=this.getPoStatusByPoId(this.selectPoId);
		//Remove purchase order line item status of 'Received' or 'Error'
		var poLineStatusField=this.abPoLineEditForm.fields.get('po_line.status').dom;
		if(poStatus!='Issued'){
			for(var i=poLineStatusField.length-1;i>0;i--){
				if(poLineStatusField[i].value=='Error'||poLineStatusField[i].value=='Received'){
					poLineStatusField.remove(i);
				}
			}
		}
	},
	
	getPoStatusByPoId: function(poId){
		var poRes=new Ab.view.Restriction();
			poRes.addClause('po.po_id',poId,'=');
		
		return this.abPoStatusDs.getRecord(poRes).getValue('po.status');
	},
	
	/**
	 * Enable or disable panel fields and actions based on purchase order status.
	 *
	 */
	enablePurchaseOrderItemFormFields: function(){
		var status=this.abPoLineEditForm.getFieldValue('po_line.status');
		var enableFields;
		this.abPoLineEditForm.actions.get('save').enable(false);
		this.abPoLineEditForm.actions.get('delete').enable(false);
		
		enableFields=['po_line.status','po_line.catno','po_line.description','po_line.quantity','po_line.unit_cost','po_line.em_id'];
		enablePanelFields('abPoLineEditForm',enableFields,false);
		
		if(status!='Received'&&status!='Error'){
			this.abPoLineEditForm.actions.get('save').enable(true);
			enableFields=['po_line.em_id','po_line.status'];
			enablePanelFields('abPoLineEditForm',enableFields,true);
			if(status=='Not Yet Issued'){
				this.abPoLineEditForm.actions.get('delete').enable(true);
				
				enableFields=['po_line.catno','po_line.description','po_line.quantity','po_line.unit_cost'];
				enablePanelFields('abPoLineEditForm',enableFields,true);
			}
		}
		
		if(this.hasPoItemsReceivedorErrors()){
			this.abPoLineEditForm.actions.get('delete').enable(false);
		}
	},
	
	/**
	 * Click 'Receive' button in purchase order line list.
	 */
	onclickPurchaseOrderItemReceivedButton: function(){
		var selectRowIndex=this.abPoLineList.selectedRowIndex;
		var selectRowRecord=this.abPoLineList.gridRows.get(selectRowIndex).getRecord();
		var poId=selectRowRecord.getValue('po_line.po_id');
		var poLineId=selectRowRecord.getValue('po_line.po_line_id');
		var partCode=selectRowRecord.getValue('po_line.vfPartId');
		var receivingLocation=this.selectReceivingLocation;
		var transQty=selectRowRecord.getValue('po_line.quantity');
		var unitCost=selectRowRecord.getValue('po_line.unit_cost');
		
		transQty=parseFloat(transQty);
		unitCost=parseFloat(unitCost);
		
		//get shipping and tax
		var perShippingAndTax=this.getPerShippingAndTaxFromPoLine(poId,poLineId,transQty);
		
		View.openProgressBar();
		//setPurchaseOrderTobeReceivedOrError(poId, poLineId, partCode,transQty, status);
		try{
			//add shipping and tax to current unit cost
			unitCost=parseFloat(unitCost)+parseFloat(perShippingAndTax);
			var result = Workflow.callMethod("AbBldgOpsBackgroundData-BldgopsPartInventoryService-setPurchaseOrderTobeReceivedOrError", poId, poLineId,partCode,receivingLocation,transQty,unitCost,"Received");
			if(result.code=="executed"){
				View.panels.get('abPoLineList').refresh();
				View.panels.get('purchaseOrderListPanel').refresh();
				
				//Hidden purchase order edit froms
				this.purchaseOrderInfo.show(false);
				this.purchaseOrderDetail.show(false);
				this.shippingAndBillingForm.show(false);
				
				//Hidden purchase order item edit forms
				this.abPoLineEditForm.show(false);
				
				this.hiddePanelIfNotExistPurchaseOrderItemNotReceived(poId);
				View.closeProgressBar();
			}
		}catch (e) {
			Workflow.handleError(e); 
			View.closeProgressBar();
		}
		
	},
	
	/**
	 * Hidden Purchase Order item panel if do not exists purchase order item in('Received','Error').
	 */
	hiddePanelIfNotExistPurchaseOrderItemNotReceived: function(poId){
		var poLineDs=View.dataSources.get('abPoLineEditPoLineDeleteDs');
		var poLineRes=new Ab.view.Restriction();
			poLineRes.addClause('po_line.po_id',poId,'=');
			poLineRes.addClause('po_line.status',['Received','Error'],'NOT IN');

		var poRecordLength=poLineDs.getRecords(poLineRes).length;
		
		if(poRecordLength==0){
			this.abPoLineList.show(false);
			this.abPoLineEditForm.show(false);
		}
	},
	/**
	 * check whether has po line item in Received or Error.
	 */
	hasPoItemsReceivedorErrors: function(){
		var hasReceivedRecords=false;
		var poLineDs=View.dataSources.get('abPoLineEditPoLineDeleteDs');
		var poLineRes=new Ab.view.Restriction();
			poLineRes.addClause('po_line.po_id',this.selectPoId,'=');
			poLineRes.addClause('po_line.status',['Received','Error'],'IN');
			
		var receivedPoRecordsLength=poLineDs.getRecords(poLineRes).length;
		
		if(receivedPoRecordsLength>0){
			hasReceivedRecords=true;
		}
		
		return hasReceivedRecords;
	},
	
	/**
	 * Save purchase order line information.
	 */
	abPoLineEditForm_onSave: function(){
		var canSave=this.abPoLineEditFormValidate();
		if(canSave){
			var poId=this.abPoLineEditForm.getFieldValue('po_line.po_id');
			var poLineId=this.abPoLineEditForm.getFieldValue('po_line.po_line_id');
			var transQty=this.abPoLineEditForm.getFieldValue('po_line.quantity');
			var unitCost=this.abPoLineEditForm.getFieldValue('po_line.unit_cost');
			var poLineRecords=this.buildPoLineJSONArrayForWFR();
			var isNew=this.abPoLineEditForm.newRecord;
			try{ 
				View.openProgressBar();
				
				var result = Workflow.callMethod("AbBldgOpsBackgroundData-BldgopsPartInventoryService-savePurchaseOrderLineItem", poLineRecords,isNew);
				if(result.code=="executed"){
					View.panels.get('abPoLineList').refresh();
					View.panels.get('purchaseOrderListPanel').refresh();
					View.panels.get('abPoLineEditForm').refresh();
					this.hiddePanelIfNotExistPurchaseOrderItemNotReceived(poId);
					View.closeProgressBar();
				}
			}catch (e) {
				View.closeProgressBar();
				Workflow.handleError(e); 
			}
		}
	},
	
	buildPoLineJSONArrayForWFR: function(){
		var poLineRecords=[];
			poLineRecords[0]=new Object();
		var poLineFormFields=["po_line.po_id","po_line.po_line_id","po_line.status","po_line.catno","po_line.description","po_line.quantity","po_line.unit_cost","po_line.em_id","po_line.vfPartId"];
		setFieldValueToJSONArrayObject("abPoLineEditForm",poLineRecords[0],poLineFormFields);
		
		return poLineRecords;
	},
	
	/**
	 * Validate on purchase order line edit form fields value.
	 */
	abPoLineEditFormValidate: function(){
		var canSave=this.abPoLineEditForm.canSave();
		if(canSave){
			var transQty=this.abPoLineEditForm.getFieldValue('po_line.quantity');
			var unitCost=this.abPoLineEditForm.getFieldValue('po_line.unit_cost');
			var catNo=this.abPoLineEditForm.getFieldValue('po_line.catno');
			
			var checkResult=this.checkCatNotExistsInPvTable(catNo);
			if(!checkResult){
				View.alert(getMessage('catNoMustExsitsInPvMsg'));
				return false;
			}
			
			if(parseFloat(transQty)<=0||parseFloat(unitCost)<=0){
				View.alert(getMessage('quantityAndCostMustGreaterThan0Msg'));
				return false;
			}
		}else{
			return false;
		}
		
		return true;
		
	},
	/**
	 * Get shipping and Tax from Purchase order line.
	 * 
	 * @param {poId} Purchase order code.
	 * @param {poLineId} Purchase order line code
	 * @param {transQty} Transaction Quantity
	 */
	getPerShippingAndTaxFromPoLine: function(poId,poLineId,transQty){
		var poLineDs=View.dataSources.get('abPoLineEditPoLineDs');
		var poLineRes=new Ab.view.Restriction();
			poLineRes.addClause('po_line.po_id',poId,'=');
			poLineRes.addClause('po_line.po_line_id',poLineId,'=');
		poLineDs.addParameter('poId',poId);
		var poLineRecord=poLineDs.getRecord(poLineRes);
		//get shipping and tax
		var shippingAndTax=poLineRecord.getValue('po_line.vfShippingTaxes');
		//get per quantity shipping and Tax
		var perShippingAndTax=0;
		var quantity=transQty==0?9999999999:transQty;
		perShippingAndTax=(parseFloat(shippingAndTax)/quantity).toFixed(2);
		
		return perShippingAndTax;
	},
	
	/**
	 * Check whether catlog number for the selected vendor exists in pv table or not.
	 * 
	 * @catNo Catlog number.
	 */
	checkCatNotExistsInPvTable: function(catNo){
		var result=false;
		var pvDs=View.dataSources.get('pvDs');
		var pvRes=new Ab.view.Restriction();
		pvRes.addClause('pv.vn_pt_num',catNo,'=');
		pvRes.addClause('pv.vn_id',this.selectVnId,'=');
		var pvRecordsLength=pvDs.getRecords(pvRes).length;
		
		if(pvRecordsLength>0){
			result=true;
		}
		return result;
	},
	
	/**
	 * Click add new button to add a new purchase order line record.
	 */
	abPoLineList_onAddNew: function(){
		this.abPoLineEditForm.refresh(null,true);
		this.abPoLineEditForm.setFieldValue('po_line.po_id',this.selectPoId);
		this.abPoLineEditForm.enableField('po_line.status',false);
		
		var nextPoLineNumber=this.getNextPoLineNumberByPoId(this.selectPoId);
		this.abPoLineEditForm.setFieldValue('po_line.po_line_id',nextPoLineNumber);
		this.abPoLineEditForm.setFieldValue('po_line.em_id',View.user.employee.id);
		//Hidden purchase order edit forms
		this.purchaseOrderInfo.show(false);
		this.purchaseOrderDetail.show(false);
		this.shippingAndBillingForm.show(false);
	},
	
	/**
	 * event handler after purchase order line list panel refresh.
	 */
	abPoLineList_afterRefresh: function(){
		this.abPoLineList.setTitle(String.format(getMessage('poLineListPanelTitle'),this.selectPoId,this.selectVnId,this.selectReceivingLocation));
		this.abPoLineList.gridRows.each(function(row){
			var rowRecord=row.getRecord();
			var status=rowRecord.getValue('po_line.status');
			var poStatus=rowRecord.getValue('po.status');
			if(status=='Received'||status=='Error'){
				row.actions.get('receive').enable(false);
			}else{
				if(poStatus!='Issued'){
					row.actions.get('receive').enable(false);
				}else{
					row.actions.get('receive').enable(true);
				}
			}
		});
	},
	
	/**
	 * Delete purchase order line.
	 * 
	 */
	abPoLineEditForm_onDelete: function(){
		View.confirm(getMessage('makeSureDeletePoLineItemMsg'),function(button){
			if(button=='yes'){
				var poId=parseInt(View.panels.get('abPoLineEditForm').getFieldValue('po_line.po_id'));
				var poLineId=parseInt(View.panels.get('abPoLineEditForm').getFieldValue('po_line.po_line_id'));
				try{
					var result=Workflow.callMethod("AbBldgOpsBackgroundData-BldgopsPartInventoryService-removeItemFromPoLine", poId,poLineId);
					if(result.code=="executed"){
						View.panels.get('abPoLineEditForm').show(false);
						View.panels.get('abPoLineList').refresh();
						View.panels.get('purchaseOrderListPanel').refresh();
						if(View.panels.get('abPoLineList').gridRows.length==0){
							View.panels.get('abPoLineList').show(false);
						}
					}
				}catch (e) {
					Workflow.handleError(e);
				}
			}
		});
	},
	
	/**
	 * Click cancel button in purchase order line edit form.
	 */
	abPoLineEditForm_onCancel: function(){
		this.abPoLineEditForm.show(false);
	},
	
	/**
	 * Save purchase order record.
	 */
	purchaseOrderInfo_onSave: function(){
		var me=this;
		var isOrderInfoFormCanSave=this.purchaseOrderInfo.canSave();
		var isOrderDetailFormCanSave=this.purchaseOrderDetail.canSave();
		var isShippingAndBllingFormCanSave=this.shippingAndBillingForm.canSave();
		var poId=this.purchaseOrderInfo.getFieldValue('po.po_id');
			poId=parseInt(poId);
		var afterStatus=this.purchaseOrderInfo.getFieldValue("po.status");
		if(isOrderInfoFormCanSave==true&&isOrderDetailFormCanSave==true&&isShippingAndBllingFormCanSave==true){
			//Copy field value from Order Detail form and Shipping And Billing form to Purchase Order form.
			var poRecord=this.buildPurchaseOrderJSONArrayForWFR(afterStatus);
			try{ 
				View.openProgressBar();
				var result = Workflow.callMethod("AbBldgOpsBackgroundData-BldgopsPartInventoryService-updatePurchaseOrderInfor", poRecord);
				if(result.code=="executed"){
					me.poStatus=afterStatus;
					me.refreshPanelsAfterSaveOrApprove(poId);
					View.closeProgressBar();
				}
			}catch (e) {
				Workflow.handleError(e);
				View.closeProgressBar();
			}
		}
	},
	
	/**
	 *Build purchase order JSONArray for WFR.
	 *
	 *@param {afterStatus} Status to be changed.
	 */
	buildPurchaseOrderJSONArrayForWFR: function(afterStatus){
		var purchaseOrderRecord=[];
			purchaseOrderRecord[0]=new Object();
		var purchaseOrderInforFormFields=["po.po_id","po.vn_id","po.receiving_location","po.status","po.date_paid","po.comments"];
		setFieldValueToJSONArrayObject("purchaseOrderInfo",purchaseOrderRecord[0],purchaseOrderInforFormFields);

		var purcahseOrderDetailFormFields=["po.ac_id","po.po_number","po.source","po.federal_tax","po.em_id","po.state_tax","po.shipping"];
		setFieldValueToJSONArrayObject("purchaseOrderDetail",purchaseOrderRecord[0],purcahseOrderDetailFormFields);

		var shippingAndBillingFromFields=["po.shipping_em_id","po.billing_em_id","po.ship_address1","po.bill_address1","po.ship_address2","po.bill_address2","po.ship_city_id","po.bill_city_id","po.ship_state_id","po.bill_state_id","po.ship_zip","po.bill_zip"];
		setFieldValueToJSONArrayObject("shippingAndBillingForm",purchaseOrderRecord[0],shippingAndBillingFromFields);
		
		if(afterStatus=='Approved'||afterStatus=='Rejected'){
			purchaseOrderRecord[0]["po.status"]=afterStatus;
		}
			
		return purchaseOrderRecord;
	},
	
	/**
	 * Delete purchase order record.
	 */
	purchaseOrderInfo_onDelete: function(){
		var poId=View.panels.get('purchaseOrderInfo').getFieldValue('po.po_id');
		
		var hasItem=this.hasItemAlreadyReceived(poId);
		if(!hasItem){
			View.confirm(getMessage('makeSureDeletePoLineItemMsg'),function(button){
				if(button=='yes'){
					var poRes=new Ab.view.Restriction();
						poRes.addClause('po.po_id',poId,'=');
					var poDs=View.dataSources.get('abPoStatusDs');
					var poRecord=poDs.getRecord(poRes);
						poDs.deleteRecord(poRecord);
					View.panels.get('purchaseOrderInfo').show(false);
					View.panels.get('purchaseOrderDetail').show(false);
					View.panels.get('shippingAndBillingForm').show(false);
					View.panels.get('purchaseOrderListPanel').refresh();
					View.panels.get('abPoLineList').show(false);
				}
			});
		}else{
			View.alert(getMessage('purchaseOrderCantDeletedMsg'));
		}
	},
	
	/**
	 * Check weather exists item already received for selected supply requisition.
	 */
	hasItemAlreadyReceived: function(poId){
		var result=false;
		var poLineDs=View.dataSources.get('abPoLineEditPoLineDeleteDs');
		var poLineRes=new Ab.view.Restriction();
			poLineRes.addClause('po_line.po_id',poId,'=');
			poLineRes.addClause('po_line.status',['Received','Error'],'IN');
		var poLineRecords=poLineDs.getRecords(poLineRes);
		
		if(poLineRecords.length>0){
			result=true;
		}
		return result;
	},
	
	/**
	 * Click cancel button in purchase order form panel.
	 */
	purchaseOrderInfo_onCancel: function(){
		View.panels.get('purchaseOrderInfo').show(false);
		View.panels.get('purchaseOrderDetail').show(false);
		View.panels.get('shippingAndBillingForm').show(false);
	},
	
	/**
	 * Approve or Reject Purchase Order with purchase order status.
	 * 
	 * @param {beforeStatus} Status before change
	 * @param {afterStatus} Status after change
	 */
	approveOrRejectPurchaseOrder: function(beforeStatus,afterStatus){
		var me=this;
		var isOrderInfoFormCanSave=this.purchaseOrderInfo.canSave();
		var isOrderDetailFormCanSave=this.purchaseOrderDetail.canSave();
		var isShippingAndBllingFormCanSave=this.shippingAndBillingForm.canSave();
		var poId=this.purchaseOrderInfo.getFieldValue('po.po_id');
			poId=parseInt(poId);
		
		if(isOrderInfoFormCanSave==true&&isOrderDetailFormCanSave==true&&isShippingAndBllingFormCanSave==true){
			//Copy field value from Order Detail form and Shipping And Billing form to Purchase Order form.
			var poRecord=this.buildPurchaseOrderJSONArrayForWFR(afterStatus);
			try{ 
				View.openProgressBar();
				var result = Workflow.callMethod("AbBldgOpsBackgroundData-BldgopsPartInventoryService-updatePurchaseOrderInfor", poRecord);
				if(result.code=="executed"){
					me.poStatus=afterStatus;
					me.refreshPanelsAfterSaveOrApprove(poId);
					View.closeProgressBar();
				}
			}catch (e) {
				Workflow.handleError(e);
				View.closeProgressBar();
			}
		}
	},
	
	/**
	 * Refresh panels after approve or reject purchase order.
	 * 
	 * @param {poId} Purchase Order Code.
	 */
	refreshPanelsAfterSaveOrApprove: function(poId){
		var poRes=new Ab.view.Restriction();
			poRes.addClause('po.po_id',poId,'=');
		View.panels.get('purchaseOrderInfo').refresh(poRes);
		View.panels.get('purchaseOrderDetail').refresh(poRes);
		View.panels.get('shippingAndBillingForm').refresh(poRes);
		View.panels.get('purchaseOrderListPanel').refresh();
		View.panels.get('abPoLineList').refresh();
		
		this.enablePurchaseOrderFormFields();
	},
	
	/**
	 * Open dialog to export report data to PDF file or DOCX file.
	 * 
	 * @param {fileType} Export type, pdf or docx.
	 */
	openExportPdfDialog: function(fileType){
		var selectRowRecords=this.purchaseOrderListPanel.getSelectedRecords();
		if(selectRowRecords.length==0){
			View.alert(getMessage("exportSupplyReqMsg"));
		}else{
			var poIdsInConditionClause="po.po_id in (";
			for(var i=0;i<selectRowRecords.length;i++){
				poIdsInConditionClause += selectRowRecords[i].getValue('po.po_id');
				if(i!=selectRowRecords.length-1){
					poIdsInConditionClause += ","
				}
			}
			poIdsInConditionClause +=")";
			var pdfParameters = {};
				pdfParameters['poIds']=poIdsInConditionClause;
			if(fileType=="pdf"){
				View.openPaginatedReportDialog("ab-po-line-edit-report-pdf.axvw", null, pdfParameters);
			}
			
			if(fileType=="docx"){
				View.openPaginatedReportDialog("ab-po-line-edit-report-docx.axvw", null, pdfParameters);
			}
		}
	},
	
	/**
	 * the field P.O. Line Number should be read-only and should be set to the next available number for that Purchase Order.
	 * @param poId Purchase Order Code
	 * @return nextPoLineNumber Next available number
	 */
	getNextPoLineNumberByPoId: function(poId){
		var nextPoLineNumber=1;
		var maxPoLineDs=View.dataSources.get('getMaxPoLineItemIdDs');
			maxPoLineDs.addParameter('poId',poId);
		var record=maxPoLineDs.getRecord();
		var maxPoLineNumber=record.getValue('po.maxItemId');
		
		if(valueExistsNotEmpty(maxPoLineNumber)){
			nextPoLineNumber=parseInt(maxPoLineNumber)+1;
		}
		return nextPoLineNumber;
	}
	
});

/**
 * Approved button click event handler
 */
function onclickBtnApproved(){
	poLineMangeCtrl.approveOrRejectPurchaseOrder('Requested','Approved');
}

/**
 * Reject button click event handler
 */
function onclickBtnReject(){
	poLineMangeCtrl.approveOrRejectPurchaseOrder('Requested','Rejected');
}

/**
 * Calculate total line cost.
 */
function calculateLineCost(){
	var totalCost=0;
	var quantity=View.panels.get('abPoLineEditForm').getFieldValue('po_line.quantity');
	if(!valueExistsNotEmpty(quantity)){
		quantity=0;
	}else{
		quantity=parseFloat(quantity);
	}
	var unitCost=View.panels.get('abPoLineEditForm').getFieldValue('po_line.unit_cost');
	if(!valueExistsNotEmpty(unitCost)){
		unitCost=0;
	}else{
		unitCost=parseFloat(unitCost);
	}
	totalCost=(quantity*unitCost).toFixed(2);
	var unitCostFieldDef=View.panels.get('abPoLineEditForm').fields.get('po_line.unit_cost').fieldDef;
	View.panels.get('abPoLineEditForm').setFieldValue('po_line.vfLineCost',unitCostFieldDef.formatValue(totalCost,true));
}