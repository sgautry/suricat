/**
 * map controller for Find Parts map view
 * 
 * @author Jia
 */
var mapController = mapCommonCtrl.extend({

	//override parent map Type value to register current map view.
	mapType: "find_part_map",
	//override parent value that not use default marker popup.
	usePopup: false,
	//this value specify whether legend content contains quantity field or not .
	mapDataType: "",
	
	/*************************************Parent console form field value********************************************/
	//Part Code in console form
	partCode: "",
	//Part Classfication in console form
	partClass: "",
	//Part Description in console form
	partDescription: "",
	// Part Store Location Code in cosole form
	storeLocId: "",
	//Part Store Location Site in console form
	siteId: "",
	// Building Code in console form
	blId: "",
	// Quantity Avaliable in console form
	qtyOnHand: "",

	afterInitialDataFetch : function() {
		//register current controller.
		var parentController=View.getOpenerView().controllers.get('findPartCtrl');
		parentController.registerCtrl(this);
		//set the console record
		this.getFieldValueFromConsoleRecord(View.getOpenerView().panels.get('consoleForm').getRecord());
		var workRequestIds=View.getOpenerView().getOpenerView().workRequestIds;
		//show map legend
		this.mapControl.showMarkerLegend();
		//Show work requests markers
		this.showMapMarkerOfWorkRequests(workRequestIds);
		//show building markers
		this.showMapMarkerOfBuilding();
	},
	
	/**
	 * Show map control via the console form filter criteria
	 */
	refreshDataOnFilter : function(record) {
		var workRequestIds=View.getOpenerView().getOpenerView().workRequestIds;
		
		this.getFieldValueFromConsoleRecord(record);
		//Show markers in the map control
		//Clear Markers
		this.mapControl.clearMarkers();
		this.mapControl.showMarkerLegend();
		this.mapControl.hideMarkerInfoWindow();
		//show work requests markers
		this.showMapMarkerOfWorkRequests(workRequestIds);
		//show building markers
		this.showMapMarkerOfBuilding();
	},
	
	/**
	 * refresh map view when click parent controller clear button.
	 */
	refrehDataOnClear: function(record){
		this.refreshDataOnFilter(record);
	},
	
	/**
	 * Get field value from console form record.
	 * 
	 * @param {record} Console form record
	 */
	getFieldValueFromConsoleRecord: function(consoleRecord){
		this.partCode="";//Part Code
		this.partClass=""; //Part Classfication
		this.partDescription=""; //Part Description
		this.storeLocId=""; // Part Store Location Code
		this.siteId=""; //Part Store Location Site
		this.blId=""; // Building Code
		this.qtyOnHand="";// Quantity Avaliable
		
		if(valueExistsNotEmpty(consoleRecord)){
			this.partCode = consoleRecord.getValue('pt.part_id');
			this.partClass = consoleRecord.getValue('pt.class');
			this.partDescription=consoleRecord.getValue('pt.description');
			this.storeLocId = consoleRecord.getValue('pt_store_loc_pt.pt_store_loc_id');
			this.siteId = consoleRecord.getValue('pt_store_loc.site_id');
			this.blId = consoleRecord.getValue('pt_store_loc.bl_id');
			this.qtyOnHand = consoleRecord.getValue('pt_store_loc_pt.qty_on_hand');
		}
	},
	
	/**
	 * Show building map marker.
	 */
	showMapMarkerOfBuilding : function() {
		
		//If value of parts exists, create marker and restriction from part storage location table.
		if(valueExistsNotEmpty(this.partCode)||valueExistsNotEmpty(this.partClass)||valueExistsNotEmpty(this.partDescription)){
			//show marker popup contains part quantity information.
			this.mapDataType=MapDataTypes.PSLP;
		}else{
			//show marker popup not contain part quantity information , only contains storage location information.
			this.mapDataType=MapDataTypes.PSL;
		}
		//Set parameter to map marker dataSource
		this.setMapMarkerDsParameter();
		
		this.createMapMarkerProperty(this.mapDataType);
		this.mapControl.showMarkers('mapMarkerDs', null);
		
	},
	
	/**********************************************create map marker********************************************/
	
	/**
	 * set map marker dataSource parameter
	 */
	setMapMarkerDsParameter: function(){
		this.mapMarkerDs.addParameter('partResBasicForPartMap',false);
		this.mapMarkerDs.addParameter('partResForStoreLocId',false);
		this.mapMarkerDs.addParameter('partResForSiteId',false);
		this.mapMarkerDs.addParameter('partResForBlId',false);
		this.mapMarkerDs.addParameter('partResForPartCode',false);
		this.mapMarkerDs.addParameter('partResForPartClass',false);
		this.mapMarkerDs.addParameter('partResForPartDescription',false);
		
		this.mapMarkerDs.addParameter('storageLocResForStoreLocId',false);
		this.mapMarkerDs.addParameter('storageLocResForSiteId',false);
		this.mapMarkerDs.addParameter('storageLocResForBlId',false);
		
		if(this.mapDataType==MapDataTypes.PSLP){
			this.mapMarkerDs.addParameter('partResBasicForPartMap',true);
			
			if(valueExistsNotEmpty(this.storeLocId)||(valueExistsNotEmpty(this.siteId))||valueExistsNotEmpty(this.blId)){
				if(valueExistsNotEmpty(this.storeLocId)){
					this.mapMarkerDs.addParameter('storeLocId',this.storeLocId);
					this.mapMarkerDs.addParameter('partResForStoreLocId',true);
					this.mapMarkerDs.addParameter('storageLocResForStoreLocId',true);
				}
				if(valueExistsNotEmpty(this.siteId)){
					this.mapMarkerDs.addParameter('siteId',this.siteId);
					this.mapMarkerDs.addParameter('partResForSiteId',true);
					this.mapMarkerDs.addParameter('storageLocResForSiteId',true);
				}
				if(valueExistsNotEmpty(this.blId)){
					this.mapMarkerDs.addParameter('blId',this.blId);
					this.mapMarkerDs.addParameter('partResForBlId',true);
					this.mapMarkerDs.addParameter('storageLocResForBlId',true);
				}
			}
			if(valueExistsNotEmpty(this.qtyOnHand)){
				this.mapMarkerDs.addParameter('qtyAvaliableParameter','='+parseFloat(this.qtyOnHand));
			}else{
				this.mapMarkerDs.addParameter('qtyAvaliableParameter',0);
			}
			if(valueExistsNotEmpty(this.partCode)){
				this.mapMarkerDs.addParameter('partCode',this.partCode);
				this.mapMarkerDs.addParameter('partResForPartCode',true);
			}
			if(valueExistsNotEmpty(this.partClass)){
				this.mapMarkerDs.addParameter('partClass',this.partClass);
				this.mapMarkerDs.addParameter('partResForPartClass',true);
			}
			if(valueExistsNotEmpty(this.partDescription)){
				this.mapMarkerDs.addParameter('partDescription',this.partClass);
				this.mapMarkerDs.addParameter('partResForPartDescription',true);
			}
			
		}else{
			if(valueExistsNotEmpty(this.storeLocId)||(valueExistsNotEmpty(this.siteId))){
				if(valueExistsNotEmpty(this.storeLocId)){
					this.mapMarkerDs.addParameter('storeLocId',this.storeLocId);
					this.mapMarkerDs.addParameter('storageLocResForStoreLocId',true);
				}
				if(valueExistsNotEmpty(this.siteId)){
					this.mapMarkerDs.addParameter('siteId',this.storeLocId);
					this.mapMarkerDs.addParameter('storageLocResForSiteId',true);
				}
			}
		}
	},
	
	/**
	 * Create Part storage location marker property by part store location table.
	 * 
	 * @param ptStoreLocDataSource Part storage location datasource
	 * @param ptStoreLocKeyFields Part storage location primary key fields
	 * @param ptStoreLocGeometryFields Part storage location geometry fields
	 * @param ptStoreLocTitleField Part storage location title fields
	 * @param ptStoreLocContentFields Part storage location content fields
	 */
	createMapMarkerProperty: function(mapDataType){
		//Create Part Store Location Marker
		// create property markers
        var mapMarkerDataSource = 'mapMarkerDs';
        var mapMarkerKeyFields = ['bl.bl_id'];
        var mapMarkerGeometryFields = ['bl.lon', 'bl.lat'];
        var mapMarkerTitleField = 'bl.bl_id';
        var mapMarkerContentFields = [ 'bl.bl_id','bl.colorTypeForLocPt','bl.colorTypeForLoc'];
        
		//Get locator color
		var locatorColor=this.getMapMarkerColor();
		//Get thematic Buckets 
		var thematicBuckets=this.getMapMarkerThematicBuckets();
		var mapMarkerProperties=null;
		//crate colorbrewer object
		colorbrewer['FP-YRG']={3:[]};
		colorbrewer['FP-YRG'][3]=locatorColor;
		//get thematic fields
		var thematicFields='';
		if(mapDataType==MapDataTypes.PSLP){
			thematicFields='bl.colorTypeForLocPt';
		}else{
			thematicFields='bl.colorTypeForLoc';
		}
		
		var isUsePopup=this.usePopup;
		mapMarkerProperties = {
	        radius: 10,
	        //stroke properties
			stroke: true,
		    strokeColor: '#fff',
		    strokeWeight: 1.0,
	        // required for thematic markers
			renderer: 'thematic-class-breaks',
			thematicField: thematicFields,
			thematicClassBreaks: thematicBuckets,
			colorBrewerClass: 'FP-YRG',
			useClusters: false,
			usePopup: isUsePopup
	   };

        this.mapControl.createMarkers(
        	mapMarkerDataSource, 
        	mapMarkerKeyFields,
        	mapMarkerGeometryFields,
        	mapMarkerTitleField,
        	mapMarkerContentFields,
        	mapMarkerProperties
        );
	},

	/**
	 * Get part storage location color by console record.
	 */
	getMapMarkerColor: function(){
		var locatorsColor=[MarkerColors.GREEN,MarkerColors.RED,MarkerColors.YELLOW];
		
		return locatorsColor;
	},
	
	/**
	 * get part storage location thematic buckets.
	 */
	getMapMarkerThematicBuckets: function(){
		var thematicBuckets=[1.5,2.5];
		return thematicBuckets;
	},
	
	/**
	 * Override parent method to click marker to display marker popup.
	 * 
	 * @param {assetId} Building Code
	 * @param {featrue} Map marker feature
	 */
	onMarkerClick: function(assetId, feature){
		//Clear selected marker
		this.mapControl.clearSelectedMarkers();
		//display map marker pop-up content
		this.displayMarkerPopupContent(assetId);
	},
	
	/******************************************create map marker popup content***************************************/
	
	/**
	 * set parameter to map marker popup content datasource.
	 */
	setPopupContentDsParameter: function(){
		this.mapPopupContentDs.addParameter('partResForStoreLocId',false);
		this.mapPopupContentDs.addParameter('partResForSiteId',false);
		this.mapPopupContentDs.addParameter('partResForBlId',false);
		this.mapPopupContentDs.addParameter('partResForPartCode',false);
		this.mapPopupContentDs.addParameter('partResForPartClass',false);
		this.mapPopupContentDs.addParameter('partResForPartDescription',false);
		
		this.mapPopupContentDs.addParameter('storageLocResForStoreLocId',false);
		this.mapPopupContentDs.addParameter('storageLocResForSiteId',false);
		this.mapPopupContentDs.addParameter('storageLocResForBlId',false);
		//map pop-up content for part storage location part.
		if(this.mapDataType==MapDataTypes.PSLP){
			
			if(valueExistsNotEmpty(this.storeLocId)||(valueExistsNotEmpty(this.siteId))||((valueExistsNotEmpty(this.blId)))){
				if(valueExistsNotEmpty(this.storeLocId)){
					this.mapPopupContentDs.addParameter('storeLocId',this.storeLocId);
					this.mapPopupContentDs.addParameter('partResForStoreLocId',true);
					this.mapPopupContentDs.addParameter('storageLocResForStoreLocId',true);
				}
				if(valueExistsNotEmpty(this.siteId)){
					this.mapPopupContentDs.addParameter('siteId',this.siteId);
					this.mapPopupContentDs.addParameter('partResForSiteId',true);
					this.mapPopupContentDs.addParameter('storageLocResForSiteId',true);
				}
				if(valueExistsNotEmpty(this.blId)){
					this.mapPopupContentDs.addParameter('blId',this.blId);
					this.mapPopupContentDs.addParameter('partResForBlId',true);
					this.mapPopupContentDs.addParameter('storageLocResForBlId',true);
				}
			}
			if(valueExistsNotEmpty(this.partCode)){
				this.mapPopupContentDs.addParameter('partCode',this.partCode);
				this.mapPopupContentDs.addParameter('partResForPartCode',true);
			}
			if(valueExistsNotEmpty(this.partClass)){
				this.mapPopupContentDs.addParameter('partClass',this.partClass);
				this.mapPopupContentDs.addParameter('partResForPartClass',true);
			}
			if(valueExistsNotEmpty(this.partDescription)){
				this.mapPopupContentDs.addParameter('partDescription',this.partDescription);
				this.mapPopupContentDs.addParameter('partResForPartDescription',true);
			}
			
		}else{
			//map pop-up content only for storage location.
			if(valueExistsNotEmpty(this.storeLocId)||(valueExistsNotEmpty(this.siteId))||((valueExistsNotEmpty(this.blId)))){
				if(valueExistsNotEmpty(this.storeLocId)){
					this.mapPopupContentDs.addParameter('storeLocId',this.storeLocId);
					this.mapPopupContentDs.addParameter('storageLocResForStoreLocId',true);
				}
				if(valueExistsNotEmpty(this.siteId)){
					this.mapPopupContentDs.addParameter('siteId',this.siteId);
					this.mapPopupContentDs.addParameter('storageLocResForSiteId',true);
				}
				if(valueExistsNotEmpty(this.blId)){
					this.mapPopupContentDs.addParameter('blId',this.blId);
					this.mapPopupContentDs.addParameter('storageLocResForBlId',true);
				}
			}
		}
	},

	/**
	 * Display marker popup content
	 */
	displayMarkerPopupContent: function(blId){
		var blId=blId;
		var storeLocRes=new Ab.view.Restriction();
			storeLocRes.addClause('pt_store_loc.bl_id',blId,'=');
		//get all store	location in the building.
		this.setPopupContentDsParameter();
		var storeLocRecords=this.mapPopupContentDs.getRecords(storeLocRes);
		//get map marker content
		var htmlContent=this.getMarkerPopupContent(storeLocRecords,blId);
		
		this.mapControl.showMarkerInfoWindow(htmlContent);
	},
	
	/**
	 * Get map marker popup content.
	 * 
	 * @param {records} Storage Location Records in one building.
	 * @param {blId} Building Code
	 */
	getMarkerPopupContent: function(records,blId){
		var htmlContent="<table>";
		//create popup title
		htmlContent+=this.getMarkerPopupHtmlContentTitle(blId);
		
		for(var i=0;i<records.length;i++){
			var record=records[i];
			var storeLocId=record.getValue('pt_store_loc.pt_store_loc_id');
			var storeLocName=record.getValue('pt_store_loc.pt_store_loc_name');
			var qtyOnHand=record.getValue('pt_store_loc.vfQtyOnHand');
			//get HTML popup content
			if(this.mapDataType==MapDataTypes.PSLP){
				htmlContent+=this.getMarkerPopupHtmlContentForLocPt(blId,storeLocId,storeLocName,qtyOnHand);
			}else{
				htmlContent+=this.getMarkerPopupHtmlContentForLoc(blId,storeLocId,storeLocName);
			}
		}
		htmlContent+"</table>";
		
		return htmlContent;
	},
	
	/**
	 * Get map marker popup content title.
	 */
	getMarkerPopupHtmlContentTitle: function(title){
		var htmlContent="";
			htmlContent+="<tr>";
			htmlContent+="<td colspan='2' class='leaflet-map-marker-infowindow-title'>"+title+"</td>"
			htmlContent+="</tr>";
		return htmlContent;
	},
	
	/**
	 * if this.mapDataType='loc_pt',
	 * map content show field Building Code/Storage Location Code/Storage Location Name/Quantity Available/Quantity Understocked
	 * 
	 * @param {blId} Building Code
	 * @param {storeLocId} Storage Location Code
	 * @param {storeLocName} Storage Location Name
	 * @param {qtyOnHand} Quantity Available
	 * 
	 */
	getMarkerPopupHtmlContentForLocPt: function(blId,storeLocId,storeLocName,qtyOnHand){
		var htmlContent="";
			htmlContent+="<tr>";
			htmlContent+="<td class='leaflet-map-marker-infowindow-text-title'>"+storeLocId+"</td>";
			htmlContent+="<td class='leaflet-map-marker-infowindow-text'>"+storeLocName+"</td>";
			htmlContent+="</tr>";
			htmlContent+="<tr>";
			htmlContent+="<td class='leaflet-map-marker-infowindow-text-title'>"+getMessage('qtyOnHandLabel')+"</td>";
			htmlContent+="<td class='leaflet-map-marker-infowindow-text'>"+qtyOnHand+"</td>";
			htmlContent+="</tr>";
			//Add popup action to map marker popup window
			htmlContent+="<tr colspan='2'>";
			htmlContent+='<td><span class="leaflet-popup-action" id="PopupAction"><a href="javascript: void(0);" onclick="markerActionEvent(\''+storeLocId+'\')">'+getMessage('selectLinkTitle')+'</a></span></td>';
			htmlContent+="</tr>";
			htmlContent+="<tr>";
			htmlContent+='<td colspan="2"><p></p></td>';
			htmlContent+="</tr>";
			
		return htmlContent;
	},
	
	/**
	 * get map marker content if not need to show Quantity field.
	 * 
	 * @param {blId} Building Code
	 * @param {storeLocId} Storage Location Code
	 * @param {storeLocName} Storage Locaton Name
	 */
	getMarkerPopupHtmlContentForLoc: function(blId,storeLocId,storeLocName){
		var htmlContent="";
			htmlContent+="<tr>";
			htmlContent+="<td class='leaflet-map-marker-infowindow-text-title'>"+storeLocId+"</td>";
			htmlContent+="<td class='leaflet-map-marker-infowindow-text'>"+storeLocName+"</td>";
			htmlContent+="</tr>";
			//Add popup action to map marker popup window
			htmlContent+="<tr cospan='2'>";
			htmlContent+='<td><span class="leaflet-popup-action" id="PopupAction"><a href="javascript: void(0);" onclick="markerActionEvent(\''+storeLocId+'\')">'+getMessage('selectLinkTitle')+'</a></span></td>';
			htmlContent+="</tr>";
			
			htmlContent+="<tr>";
			htmlContent+='<td colspan="2"><p></p></td>';
			htmlContent+="</tr>";
			
			
		return htmlContent;
	},
	
	/*************************************************diaplay work request map marker********************************/
	
	/**
	 * Marker work request location.
	 */
	showMapMarkerOfWorkRequests: function(workRequestIds){
		if(workRequestIds.length>0){
			//Create map marker property.
			this.createWrMarkerProperty();
			
			var wrListToShow=this.getWrListToShowOnMap(workRequestIds);
			if(wrListToShow.length>0){
				var wrRestriction=this.getWrRestrictionByWrId(wrListToShow);
				this.mapControl.showMarkers('workRequestDS', wrRestriction);
			}
		}
	},
	
	/**
	 * Get work request marker property
	 */
	createWrMarkerProperty: function(){
		// create property markers
        var wrDataSource = 'workRequestDS';
        var wrKeyFields = ['wr.wr_id'];
        var wrGeometryFields = ['bl.lon', 'bl.lat'];
        var wrTitleField = 'wr.bl_id';
        var wrContentFields = ['wr.wr_id'];
        var wrColor=MarkerColors.WR_COLOR;
        var isUsePopup=this.usePopup;
		var wrMarkerProperties = {
			renderer: 'simple',
			radius: 10,
			fillColor: wrColor,
			//stroke properties
			stroke: true,
			strokeColor: '#fff',
			strokeWeight: 1.0,
			usePopup: isUsePopup
		}; 
		
		this.mapControl.createMarkers(
			wrDataSource, 
			wrKeyFields,
			wrGeometryFields,
			wrTitleField,
			wrContentFields,
			wrMarkerProperties
	    );
	},
	
	/**
	 * Locator work request location and center map by work request code.
	 */
	locateWorkRequestOnMap: function(){
		var workRequestIds=View.getOpenerView().getOpenerView().workRequestIds;
		if(workRequestIds.length>0){
			var wrRes=new Ab.view.Restriction();
			wrRes.addClause('wr.wr_id',workRequestIds,'IN');
			var records=this.workRequestDS.getRecords(wrRes);
			var passWorResquestIds=[];
			for(var i=0;i<records.length;i++){
				var record=records[i];
				var lat=record.getValue('bl.lat');
				var lon=record.getValue('bl.lon');
				
				if(valueExistsNotEmpty(lat)&&valueExistsNotEmpty(lon)){
					passWorResquestIds.push(record.getValue('wr.wr_id'));
				}
			}
			if(passWorResquestIds.length>0){
				//Marker work request location by work request code with white color 
				this.showMapMarkerOfWorkRequests(passWorResquestIds);
			}
		}
	},

	/**
	 * only show workRequests not exists in visible part storage location on the map.
	 * @param workRequestIds WorkRequest Code.
	 * @return notDuplicateWorkRequestIds WorkRequest Array that do not contains the work request duplicate with storage location.
	 */
	getWrListToShowOnMap: function(workRequestIds){
		var notDuplicateWorkRequestIds=[];
		
		var wrDs=View.dataSources.get('workRequestDS');
		var wrRes=new Ab.view.Restriction();
			wrRes.addClause('wr.wr_id',workRequestIds,'IN');
		var wrRecords=wrDs.getRecords(wrRes);
		
		//get all map marker
		this.setMapMarkerDsParameter();
		var mapMarkerRecords=this.mapMarkerDs.getRecords();

		for(var i=0;i<wrRecords.length;i++){
			var canShow=true;
			var wrRecord=wrRecords[i];
			var wrId=wrRecord.getValue('wr.wr_id');
			var wrBlId=wrRecord.getValue('wr.bl_id');
			
			for(var j=0;j<mapMarkerRecords.length;j++){
				var markerRecord=mapMarkerRecords[j];
				var markerBlId=markerRecord.getValue('bl.bl_id');
				
				if(wrBlId==markerBlId){
					canShow=false;
				}
			}
			if(canShow){
				notDuplicateWorkRequestIds.push(wrId);
			}
		}
		return notDuplicateWorkRequestIds;
	},
	
	/**
	 * Get restriction to show work requests.
	 * 
	 * @param {workRequestIds} WorkRequest list
	 */
	getWrRestrictionByWrId: function(workRequestIds){
		var wrMarkerRestriction=new Ab.view.Restriction();
		wrMarkerRestriction.addClause('wr.wr_id',workRequestIds,'IN');
		
		return wrMarkerRestriction;
	},
	
	/**
	 * The behavior of the Select link in the pop-up will depend on the filter criteria. 
	 * (1). If the user filtered on a Part Code, then the Select link will close the Find Parts window and will populate the Add Part form with the Part Code and the Storage Location Code. 
	 * 	 If the user had also filtered on a quantity available, that value will also be copied to the Add Part form.
	 * (2). If the user had not filtered on a Part Code, then the Select link will switch the view back to the Parts Inventory tab, and also filter on the selected Storage Location Code.
	 *   The user can the choose one of the Parts available in that storage location from the grid.
	 *
	 */
	showSelectLocationAction : function(keyValue) {
		//Get part store location code
		var ptStorePtId=keyValue;
		//Get part code from the console form in the opener view
		var partId=this.partCode;
		var qtyOnHand=this.qtyOnHand;
		//If the user filtered on a Part Code, then the Select link will close the Find Parts window and will populate the Add Part form with the Part Code and the Storage Location Code
		if(valueExistsNotEmpty(partId)){
			var openerPanel=View.getOpenerView().getOpenerView().parameterPanel;
			//Check if opener view panel is exists
			if(valueExistsNotEmpty(openerPanel)){
				openerPanel.setFieldValue('wrpt.part_id',partId);
				openerPanel.setFieldValue('wrpt.pt_store_loc_id',ptStorePtId);
				if(valueExistsNotEmpty(qtyOnHand)){
					openerPanel.setFieldValue('wrpt.qty_estimated',qtyOnHand);
				}
				//Close dialog
				View.getOpenerView().getOpenerView().closeDialog();
			}

		}else{
			var consoleForm=View.getOpenerView().panels.get('consoleForm');
				consoleForm.clear();
				consoleForm.setFieldValue('pt_store_loc_pt.pt_store_loc_id',ptStorePtId);
			View.getOpenerView().controllers.get('findPartCtrl').refreshGridAfterMapLocClick(ptStorePtId);
		}
	}
});


/**
 * Marker action event listener.
 * @param keyValue Key Value
 */
function markerActionEvent(keyValue){
	//Replace keyValue by inner HTML of pop-up content title.
	mapController.showSelectLocationAction(keyValue);
}