<view version="2.0">
    <!-- leaflet js library -->
    <css url="//cdn.jsdelivr.net/leaflet/0.7.3/leaflet.css" />
    <js  url="//cdn.jsdelivr.net/leaflet/0.7.3/leaflet.js" />
    <!-- leaflet clusters -->
    <js  file="leaflet-markercluster.js"/> 
    <css file="leaflet-MarkerCluster.Default.css" />
    <css file="leaflet-MarkerCluster.css" />
    <!-- esri leaflet -->
    <js url="//cdn.jsdelivr.net/leaflet.esri/1.0.0-rc.8/esri-leaflet.js" />
    <!-- ab leaflet map -->
    <js  file="ab-leaflet-map.js"/>
    <css file="ab-leaflet-map.css"/>
    <js file="colorbrewer.js"/>
    
    <js file="ab-bldgops-inv-mpsl-common-map.js"/>
    <js file="ab-bldgops-find-parts-map-tab.js"/>
    
    <message name="selectLinkTitle" translatable="true">Select</message>
    <message name="mapUnavailableLabel" translatable="true">Unavailable</message>
    <message name="mapPartlyAvailableLabel" translatable="true">Partially Available</message>
    <message name="mapAvailableLabel" translatable="true">Available</message>
    <message name="workRequestLabel" translatable="true">Work Request</message>
    <message name="qtyOnHandLabel" translatable="true">Quantity Available</message>

    <!-- map marker datasource -->
    <dataSource id="mapMarkerDs">
        <table name="bl" role="main"/>
        <field table="bl" name="bl_id"/>
        <field table="bl" name="lat"/>
        <field table="bl" name="lon"/>
        <!-- case:1: The green marker represents buildings where all the storage locations have quantity available greater than 0, and quantity understocked = 0. -->
        <!-- case:2: The red marker represents buildings where all the storage locations have quantity available <= 0 of the selected part. -->
        <!-- case:3: The yellow marker represents all other buildings that contain the part.  This will show buildings that have a mix of in-stock and understocked storage locations, and/or locations that have quantity available > 0 but quantity understocked > 0. -->
        <field name="colorTypeForLocPt" dataType="number" decimals="2">
            <sql dialect="generic">
                case 
                  when exists(
                    select pt_store_loc_id
                        from pt_store_loc where pt_store_loc.bl_id=bl.bl_id
                        and 
                        (select ${sql.isNull("sum(qty_on_hand)","0")} from pt_store_loc_pt where pt_store_loc_pt.pt_store_loc_id=pt_store_loc.pt_store_loc_id and ${sql.getRestriction('partRes')}) &gt;${parameters['qtyAvaliableParameter']}
                  )
                  then 1
                  when exists(
                    select pt_store_loc_id
                        from pt_store_loc where pt_store_loc.bl_id=bl.bl_id
                        and 
                        (select ${sql.isNull("sum(qty_on_hand)","0")} from pt_store_loc_pt where pt_store_loc_pt.pt_store_loc_id=pt_store_loc.pt_store_loc_id and ${sql.getRestriction('partRes')})&gt;0
                        and 
                        (select ${sql.isNull("sum(qty_on_hand)","0")} from pt_store_loc_pt where pt_store_loc_pt.pt_store_loc_id=pt_store_loc.pt_store_loc_id and ${sql.getRestriction('partRes')})&lt;${parameters['qtyAvaliableParameter']}
                  )
                  then 3
                  else 2 end
            </sql>
        </field>
        <field name="colorTypeForLoc" dataType="number" decimals="2">
            <sql dialect="generic">1</sql>
        </field>
        <parameter name="qtyAvaliableParameter" dataType="verbatim" value="0"/>
        <parameter name="partRes" dataType="verbatim" value="1=1"/>
        <parameter name="storageLocRes" dataType="verbatim" value="1=1"/>
        <restriction type="sql" sql="bl.lat is not null and bl.lon is not null and exists(select 1 from pt_store_loc where pt_store_loc.bl_id=bl.bl_id and ${parameters['storageLocRes']})"/>
        
        <restriction id="partRes" type="sql" sql="
            ${sql.getRestriction('partResBasicForPartMap','1=1')}
            AND
            ${sql.getRestriction('partResForStoreLocId','1=1')}
            AND
            ${sql.getRestriction('partResForSiteId','1=1')}
            AND
            ${sql.getRestriction('partResForBlId','1=1')}
            AND
            ${sql.getRestriction('partResForPartCode','1=1')}
            AND
            ${sql.getRestriction('partResForPartClass','1=1')}
            AND
            ${sql.getRestriction('partResForPartDescription','1=1')}
        "/>
        
        <restriction id="storageLocRes" type="sql" sql="
            ${sql.getRestriction('storageLocResForStoreLocId','1=1')}
            AND
            ${sql.getRestriction('storageLocResForSiteId','1=1')}
            AND
            ${sql.getRestriction('storageLocResForBlId','1=1')}
        "/>
        
        <parameter name="storeLocId" dataType="verbatim" value=""/>
        <parameter name="siteId" dataType="verbatim" value=""/>
        <parameter name="blId" dataType="verbatim" value=""/>
        <parameter name="partCode" dataType="verbatim" value=""/>
        <parameter name="partClass" dataType="verbatim" value=""/>
        <parameter name="partDescription" dataType="verbatim" value=""/>
        
        <!-- part restriction sub parameters and sub restrictions -->
        <parameter name="partResBasicForPartMap" dataType="boolean" value="false"/>
        <restriction id="partResBasicForPartMap" type="sql" enabled="partResBasicForPartMap" sql="pt_store_loc_pt.pt_store_loc_id in (select pt_store_loc_id from pt_store_loc where pt_store_loc.bl_id=bl.bl_id)"/>
        
        <parameter name="partResForStoreLocId" dataType="boolean" value="false"/>
        <restriction id="partResForStoreLocId" type="sql" enabled="partResForStoreLocId" sql="pt_store_loc_pt.pt_store_loc_id='${parameters['storeLocId']}'"/>
        
        <parameter name="partResForSiteId" dataType="boolean" value="false"/>
        <restriction id="partResForSiteId" type="sql" enabled="partResForSiteId" sql="pt_store_loc_pt.pt_store_loc_id in (select pt_store_loc_id from pt_store_loc where pt_store_loc.site_id='${parameters['siteId']}')"/>
        
        <parameter name="partResForBlId" dataType="boolean" value="false"/>
        <restriction id="partResForBlId" type="sql" enabled="partResForBlId" sql="pt_store_loc_pt.pt_store_loc_id in (select pt_store_loc_id from pt_store_loc where pt_store_loc.bl_id='${parameters['blId']}')"/>
        
        <parameter name="partResForPartCode" dataType="boolean" value="false"/>
        <restriction id="partResForPartCode" type="sql" enabled="partResForPartCode" sql="pt_store_loc_pt.part_id='${parameters['partCode']}'"/>
        
        <parameter name="partResForPartClass" dataType="boolean" value="false"/>
        <restriction id="partResForPartClass" type="sql" enabled="partResForPartClass" sql="pt_store_loc_pt.part_id in (select part_id from pt where class='${parameters['partClass']}')"/>
        
        <parameter name="partResForPartDescription" dataType="boolean" value="false"/>
        <restriction id="partResForPartDescription" type="sql" enabled="partResForPartDescription" sql="pt_store_loc_pt.part_id in (select part_id from pt where description='${parameters['partDescription']}')"/>
        
        <!-- storage location sub restrictions and sub parameters -->
        <parameter name="storageLocResForStoreLocId" dataType="boolean" value="false"/>
        <restriction id="storageLocResForStoreLocId" type="sql" enabled="storageLocResForStoreLocId" sql="pt_store_loc.pt_store_loc_id='${parameters['storeLocId']}'"/>
        
        <parameter name="storageLocResForSiteId" dataType="boolean" value="false"/>
        <restriction id="storageLocResForSiteId" type="sql" enabled="storageLocResForSiteId" sql="pt_store_loc.site_id='${parameters['siteId']}'"/>
        
        <parameter name="storageLocResForBlId" dataType="boolean" value="false"/>
        <restriction id="storageLocResForBlId" type="sql" enabled="storageLocResForBlId" sql="pt_store_loc.bl_id ='${parameters['blId']}'"/>
    </dataSource>
    
    <!-- map marker popup content datasource -->
    <dataSource id="mapPopupContentDs">
        <table name="pt_store_loc" role="main"/>
        <table name="bl" role="standard"/>
        <field table="pt_store_loc" name="pt_store_loc_id"/>
        <field table="pt_store_loc" name="pt_store_loc_name"/>
        <field table="pt_store_loc" name="pt_store_loc_desc"/>
        <field table="pt_store_loc" name="site_id"/>
        <field table="pt_store_loc" name="bl_id"/>
        <field name="vfQtyOnHand" dataType="number" decimals="2">
            <title translatable="true">Quantity Available</title>
            <sql dialect="generic">
                (select ${sql.isNull("sum(qty_on_hand)","0")} from pt_store_loc_pt where pt_store_loc_pt.pt_store_loc_id=pt_store_loc.pt_store_loc_id and ${sql.getRestriction('partRes')})
            </sql>
        </field>
        <field table="bl" name="lat"/>
        <field table="bl" name="lon"/>
        <restriction type="sql" sql="bl.lon is not null and bl.lat is not null and ${sql.getRestriction('storageLocRes')}"/>
        
        <restriction id="partRes" type="sql" sql=" 
            ${sql.getRestriction('partResForStoreLocId','1=1')}
            AND 
            ${sql.getRestriction('partResForSiteId','1=1')}
            AND 
            ${sql.getRestriction('partResForBlId','1=1')}
            AND 
            ${sql.getRestriction('partResForPartCode','1=1')}
            AND
            ${sql.getRestriction('partResForPartClass','1=1')}
            AND
            ${sql.getRestriction('partResForPartDescription','1=1')}
        "/>
        
        <restriction id="storageLocRes" type="sql" sql="
            ${sql.getRestriction('storageLocResForStoreLocId','1=1')}
            AND
            ${sql.getRestriction('storageLocResForSiteId','1=1')}
            AND
            ${sql.getRestriction('storageLocResForBlId','1=1')}
        "/>
        
        <parameter name="storeLocId" dataType="verbatim" value=""/>
        <parameter name="siteId" dataType="verbatim" value=""/>
        <parameter name="blId" dataType="verbatim" value=""/>
        <parameter name="partCode" dataType="verbatim" value=""/>
        <parameter name="partClass" dataType="verbatim" value=""/>
        <parameter name="partDescription" dataType="verbatim" value=""/>
        
        <!-- part restriction sub parameters and sub restrictions -->
        
        <parameter name="partResForStoreLocId" dataType="boolean" value="false"/>
        <restriction id="partResForStoreLocId" type="sql" enabled="partResForStoreLocId" sql="pt_store_loc_pt.pt_store_loc_id='${parameters['storeLocId']}'"/>
        
        <parameter name="partResForSiteId" dataType="boolean" value="false"/>
        <restriction id="partResForSiteId" type="sql" enabled="partResForSiteId" sql="pt_store_loc_pt.pt_store_loc_id in (select pt_store_loc_id from pt_store_loc where pt_store_loc.site_id='${parameters['siteId']}')"/>
        
        <parameter name="partResForBlId" dataType="boolean" value="false"/>
        <restriction id="partResForBlId" type="sql" enabled="partResForBlId" sql="pt_store_loc_pt.pt_store_loc_id in (select pt_store_loc_id from pt_store_loc where pt_store_loc.bl_id='${parameters['blId']}')"/>
        
        <parameter name="partResForPartCode" dataType="boolean" value="false"/>
        <restriction id="partResForPartCode" type="sql" enabled="partResForPartCode" sql="part_id='${parameters['partCode']}'"/>
        
        <parameter name="partResForPartClass" dataType="boolean" value="false"/>
        <restriction id="partResForPartClass" type="sql" enabled="partResForPartClass" sql="part_id in (select part_id from pt where class='${parameters['partClass']}')"/>
        
        <parameter name="partResForPartDescription" dataType="boolean" value="false"/>
        <restriction id="partResForPartDescription" type="sql" enabled="partResForPartDescription" sql="part_id in (select part_id from pt where description='${parameters['partDescription']}')"/>
        
        <!-- storage location restriction sub parameters and sub restrictions -->
        
        <parameter name="storageLocResForStoreLocId" dataType="boolean" value="false"/>
        <restriction id="storageLocResForStoreLocId" type="sql" enabled="storageLocResForStoreLocId" sql="pt_store_loc.pt_store_loc_id='${parameters['storeLocId']}'"/>
        
        <parameter name="storageLocResForSiteId" dataType="boolean" value="false"/>
        <restriction id="storageLocResForSiteId" type="sql" enabled="storageLocResForSiteId" sql="pt_store_loc.site_id='${parameters['siteId']}'"/>
        
        <parameter name="storageLocResForBlId" dataType="boolean" value="false"/>
        <restriction id="storageLocResForBlId" type="sql" enabled="storageLocResForBlId" sql="pt_store_loc.bl_id='${parameters['blId']}'"/>
    </dataSource>
    
    <!-- work request datasource -->
    <dataSource id="workRequestDS">
        <table name="wr" role="main"/>
        <table name="bl" role="standard"/>
        <field table="wr" name="wr_id"/>
        <field table="wr" name="bl_id"/>
        <field table="wr" name="fl_id"/>
        <field table="wr" name="rm_id"/>
        <field table="bl" name="lat"/>
        <field table="bl" name="lon"/>
        <restriction type="sql" sql="bl.lon is not null and bl.lat is not null"/>
    </dataSource>

    <panel type="html" id="mapPanel">
          <title translatable="true">Map</title>
          
          <html>
               <div id="mapDiv"></div>
          </html>
    </panel>    
</view>