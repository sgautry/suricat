var findPartCtrl=View.createController('findPartCtrl',{
	//Define work request code got from opener select value form
	workRequestIds:[],
	//the store of  registered child controllers.
	childCtrls: [],
	
	/**
	 * Register child controller
	 */
	registerCtrl: function(controller){
		this.childCtrls.push(controller);
	},
	
	afterInitialDataFetch: function(){
		//get work request Ids
		this.workRequestIds=View.getOpenerView().workRequestIds;
		
		if(this.workRequestIds.length>0){
			var wrLocation=this.getWorkRequestLocationByWrId(this.workRequestIds);
			if(valueExistsNotEmpty(wrLocation)){
				$('workrequest_location').innerHTML=wrLocation;
			}else{
				$('workrequest_locate_img').hidden=true;
			}
		}else{
			$('workrequest_locate_img').hidden=true;
		}
		//Disable Quantity Avaliable default value
		var qtyOnHandFieldElement=this.consoleForm.fields.get('pt_store_loc_pt.qty_on_hand');
		qtyOnHandFieldElement.fieldDef.defaultValue='';
	},
	
	/**
	 * Bind event when click show button
	 */
	consoleForm_onFilter: function(){
		var record=this.consoleForm.getRecord();
		var partCode=record.getValue('pt.part_id');
		var partClass=record.getValue('pt.class');
		var partDescription=record.getValue('pt.description');
		var whQuatity=record.getValue('pt_store_loc_pt.qty_on_hand');
		
		
		//The Quantity Available field requires that at least one of the Part Code/Part Classification fields also be populated.
		if(valueExistsNotEmpty(whQuatity)){
			if((!valueExistsNotEmpty(partCode))&&(!valueExistsNotEmpty(partClass))&&(!valueExistsNotEmpty(partDescription))){
				View.alert(getMessage('quantity_need_other_field'));
				return;
			}
			var isNaNCheck=isNaN(whQuatity);
			if(isNaNCheck){
				View.alert(getMessage('quntity_number'));
				return;
			}else{
				if(parseFloat(whQuatity)<=0){
					View.alert(getMessage('qtyOnHandShouldGreaterThan0'));
					return;
				}
			}
		}
		//call children controller's method
		for(var i=0;i<this.childCtrls.length;i++){
			if(typeof(this.childCtrls[i].refreshDataOnFilter)=="function"){
				this.childCtrls[i].refreshDataOnFilter(record);
			}
		}
	},
	
	/**
	 * Clear the console form when click Clear button.
	 */
	consoleForm_onClear: function(){
		this.consoleForm.clear();
		//trriger event listener
		var record=this.consoleForm.getRecord();
		//this.trigger("app:operation:express:mpiw:onClearButtonClicked",record);
		
		//call children controller's method to re-set data.
		for(var i=0;i<this.childCtrls.length;i++){
			if(typeof(this.childCtrls[i].refrehDataOnClear)=="function"){
				this.childCtrls[i].refrehDataOnClear(record);
			}
		}
	},
	
	/**
	 * Get work request location by work request code
	 * @param workrequestID work request code
	 */
	getWorkRequestLocationByWrId: function(workrequestIDs){
		var res=new Ab.view.Restriction();
		res.addClause('wr.wr_id',workrequestIDs,'IN');
		var records=this.wrptDS.getRecords(res);
		//Define part storage location
		var displayLocations="";
		for(var i=0;i<records.length;i++){
			var record=records[i];
			var blId=record.getValue('wr.bl_id');
			var flId=record.getValue('wr.fl_id');
			var rmId=record.getValue('wr.rm_id');
			
			var wrLocation="";
			
			if(valueExistsNotEmpty(blId)){
				wrLocation=blId;
			}
			if(valueExistsNotEmpty(flId)){
				wrLocation=wrLocation+"-"+flId;
			}
			if(valueExistsNotEmpty(rmId)){
				wrLocation=wrLocation+"-"+rmId;
			}
			
			if(i!=records.length-1){
				wrLocation+=';';
			}
			
			displayLocations+=wrLocation;
		}

		return displayLocations;
	},
	
	/**
	 * Refresh grid data after map marker click
	 */
	refreshGridAfterMapLocClick: function(storeLocId){
		for(var i=0;i<this.childCtrls.length;i++){
			if(typeof(this.childCtrls[i].refreshGridAfterMapMarkerClick)=="function"){
				this.partInventoryLocationTabs.selectTab('partInventoryListTab');
				this.childCtrls[i].refreshGridAfterMapMarkerClick(storeLocId);
			}
		}
	}
});

/**
 * Trriger data event to  Locate icon to Zoom and Lacate work request on map view.
 */
function onLocateWorkRequest(){
	for(var i=0;i<findPartCtrl.childCtrls.length;i++){
		if(typeof(findPartCtrl.childCtrls[i].locateWorkRequestOnMap)=="function"){
			findPartCtrl.childCtrls[i].locateWorkRequestOnMap();
		}
	}
}