var findPartListTabCtrl=View.createController('findPartListTabCtrl',{
	
	afterInitialDataFetch: function(){
		var parentController=View.getOpenerView().controllers.get('findPartCtrl');
		//register current controller to parent controller.
		parentController.registerCtrl(this);
	},
	
	/**
	 * Call function when click show button.
	 * 
	 * @param record Console form record
	 */
	refreshDataOnFilter: function(record){
		//Get value from parameter record
		var partCode=record.getValue('pt.part_id');
		var partClass=record.getValue('pt.class');
		var partDescription=record.getValue('pt.description');
		var whCode=record.getValue('pt_store_loc_pt.pt_store_loc_id');
		var whSite=record.getValue('pt_store_loc.site_id');
		var blId=record.getValue('pt_store_loc.bl_id');
		var whQuatity=record.getValue('pt_store_loc_pt.qty_on_hand');
		//Define restriction to refresh the part inventory list panel
		var res=new Ab.view.Restriction();
		
		if(valueExistsNotEmpty(partCode)){
			res.addClause('pt_store_loc_pt.part_id',partCode,'=');
		}
		if(valueExistsNotEmpty(partClass)){
			res.addClause('pt.class',partClass,'=');
		}
		if(valueExistsNotEmpty(partDescription)){
			res.addClause('pt.description',partDescription,'=');
		}
		if(valueExistsNotEmpty(whCode)){
			res.addClause('pt_store_loc_pt.pt_store_loc_id',whCode,'=');
		}
		if(valueExistsNotEmpty(whSite)){
			res.addClause('pt_store_loc.site_id',whSite,'=');
		}
		if(valueExistsNotEmpty(blId)){
			res.addClause('pt_store_loc.bl_id',blId,'=');
		}
		if(valueExistsNotEmpty(whQuatity)){
			res.addClause('pt_store_loc_pt.qty_on_hand',whQuatity,'&gt;=');
		}
		this.ptInventoryList.refresh(res);
	},
	
	/**
	 * Refresh grid data when click clear button
	 */
	refrehDataOnClear: function(record){
		this.refreshDataOnFilter(record);
	},
	
	/**
	 * Show form when add purchased parts to inventory.
	 */
	ptInventoryList_onAddNewPart: function(){
		
		this.addNewPartToInventory.showInWindow({
			width: 500,
			height:300,
			title: getMessage('addPurchaseDialogTitle')
		});
		this.addNewPartToInventory.clear();
		this.addNewPartToInventory.show(true,true);
	},
	
	/**
	 * Close window when click the Cancel button.
	 */
	addNewPartToInventory_onCancel: function(){
		this.addNewPartToInventory.closeWindow();
	},
	
	/**
	 * After clicking Save, the user is brought back to the Find Parts form, 
	 * with filter fields populated with the Part Code, Warehouse Code, and Quantity that the user chose in the Purchase form.
	 */
	addNewPartToInventory_onSave: function(){
		if (!this.ValidatePartInventoryRecord()) {
			//Display validation result
			this.addNewPartToInventory.displayValidationResult();
			return;
		}
		var part_id=this.addNewPartToInventory.getFieldValue('pt_store_loc_pt.part_id');
		var partStorageLocationId=this.addNewPartToInventory.getFieldValue('pt_store_loc_pt.pt_store_loc_id');
		var Qty=parseFloat(this.addNewPartToInventory.getFieldValue('pt_store_loc_pt.qty_on_hand'));
		var Price=parseFloat(this.addNewPartToInventory.getFieldValue('pt_store_loc_pt.cost_unit_last'));
		var acId=this.addNewPartToInventory.getFieldValue('pt_store_loc_pt.acId');
		var invAction='Add_new';
		//Update the WFR to Add part storage location code
		try{ 
			var result = Workflow.callMethod("AbBldgOpsBackgroundData-calculateWorkResourceValues-updatePartsAndITForMPSL", part_id, Qty, Price, invAction,'',partStorageLocationId,acId);
			if(result.code='executed'){
				View.panels.get('addNewPartToInventory').closeWindow();
				View.panels.get('ptInventoryList').refresh();
			}
		}catch (e) {
			Workflow.handleError(e);
		}
	},
	
	/**
	 * When select a part inventory list row , then set part code and part store location code to the opener view.
	 */
	selectPartRow: function(){
		var selectedRowIndex=this.ptInventoryList.selectedRowIndex;
		//Get the selected row record.
		var selectRowRecord=this.ptInventoryList.gridRows.get(selectedRowIndex).getRecord();
		var partId=selectRowRecord.getValue('pt_store_loc_pt.part_id');
		var ptStoreLocId=selectRowRecord.getValue('pt_store_loc_pt.pt_store_loc_id');
		var openerPanel=View.getOpenerView().getOpenerView().parameterPanel;
		
		//If opener panel is exists ,then set part code and part store location code to the opener view
		if(valueExistsNotEmpty(openerPanel)){
			if(valueExistsNotEmpty(partId)){
				openerPanel.setFieldValue('wrpt.part_id',partId);
			}
			if(valueExistsNotEmpty(ptStoreLocId)){
				openerPanel.setFieldValue('wrpt.pt_store_loc_id',ptStoreLocId);
			}
			View.getOpenerView().getOpenerView().closeDialog();
		}
	},
	
	/**
	 * Validate Part Inventory Record.
	 */
	ValidatePartInventoryRecord: function() {
        var canSave = true;
        var part = this.addNewPartToInventory.getRecord();
		var form = this.addNewPartToInventory;
		//Clear validation result
		form.clearValidationResult();
        //part_id must be filled in and valid
        var part_id = part.getValue('pt_store_loc_pt.part_id');
        if (!valueExistsNotEmpty(part_id)) {        	
            form.fields.get('pt_store_loc_pt.part_id').setInvalid(getMessage('InvalidPartCodeMsg'));
            canSave = false;
        }else {
        	if(!valid_ID("pt","part_id",part_id)) {
	            form.fields.get('pt_store_loc_pt.part_id').setInvalid(getMessage('InvalidPartCodeMsg'));
	            canSave = false;        		
        	}
        }
        var pt_store_loc_id=part.getValue('pt_store_loc_pt.pt_store_loc_id');
        if(!valueExistsNotEmpty(pt_store_loc_id)){
        	form.fields.get('pt_store_loc_pt.pt_store_loc_id').setInvalid(getMessage('StorageLocationCodeMustBeEnteredMsg'));
        	canSave=false;
        }else{
        	if(!valid_ID("pt_store_loc","pt_store_loc_id",pt_store_loc_id)) {
	            form.fields.get('pt_store_loc_pt.pt_store_loc_id').setInvalid(getMessage('InvalidPartStorageLocationCodeMsg'));
	            canSave = false;        		
        	}
        }
        //quantity must be filled in and valid
        var quantity = part.getValue('pt_store_loc_pt.qty_on_hand');
        if (!valueExistsNotEmpty(quantity) || Number(quantity) < 0||isNaN(quantity)) {        	
            form.fields.get('pt_store_loc_pt.qty_on_hand').setInvalid(getMessage('InvalidPartQtyMsg'));
            canSave = false;
        }          
        //price must be filled in and valid
		var price = part.getValue('pt_store_loc_pt.cost_unit_last');	
        if (!valueExistsNotEmpty(price)) {   
            form.fields.get('pt_store_loc_pt.cost_unit_last').setInvalid(getMessage('PartPriceMustBeEnteredMsg'));
            canSave = false;
        }else {
        	if (isNaN(price)) {
	            form.fields.get('pt_store_loc_pt.cost_unit_last').setInvalid(getMessage('InvalidPartPriceMsg'));
	            canSave = false;	        		
        	}else {
        		if (Number(price) < 0) {
		            form.fields.get('pt_store_loc_pt.cost_unit_last').setInvalid(getMessage('InvalidPartPriceMsg'));
		            canSave = false;	        		
        		}
        	}
        }
        
        return canSave;		
	},
	
	/**
	 * refresh first tab grid after click map marker.
	 * 
	 * @param {ptStorePtId} storage location code
	 */
	refreshGridAfterMapMarkerClick: function(ptStorePtId){
		var res=new Ab.view.Restriction();
			res.addClause('pt_store_loc_pt.pt_store_loc_id',ptStorePtId,'=');
		this.ptInventoryList.refresh(res);
	}
});

/**
 * Part code selectvalue action listener.
 * @param fieldName Fields Name.
 * @param selectValue Selected Value.
 * @param previousValue Previous Value.
 */

function partCodeSelectListener(fieldName,selectValue,previousValue){
	if(fieldName=="pt_store_loc_pt.part_id"){
		var abBldgopsAdjustInvForm=View.panels.get('addNewPartToInventory');
		var partId=selectValue;
		//KB#3053036 automatically set unit of issue field by selected part code.
		getUnitsIssueByPartCode(partId);
		abBldgopsAdjustInvForm.setFieldValue('pt_store_loc_pt.part_id',partId);
	}
}
/**
 * Get Units of Issue by part code.
 * @param partCode Part Code.
 */
function getUnitsIssueByPartCode(partCode){
	var partRes=new Ab.view.Restriction();
		partRes.addClause('pt.part_id',partCode,'=');
	var ptDs=View.dataSources.get('abBldgopsAdjustInvFormDS');
	var ptRecord=ptDs.getRecord(partRes);
	var unitIssue=ptRecord.getValue('pt.units_issue');
	
	View.panels.get('addNewPartToInventory').setFieldValue('pt.units_issue',unitIssue);
		
}