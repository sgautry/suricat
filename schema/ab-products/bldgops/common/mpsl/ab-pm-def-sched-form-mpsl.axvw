<view version="2.0">
    <message name="errorSave" translatable="true">Could not save the PM Schedule.</message>
    <message name="formSaved" translatable="true">Form Saved Successfully</message>
    <message name="miles" translatable="true">Miles</message>
    <message name="hours" translatable="true">Hours</message>
    <message name="meter" translatable="true">Meter</message>
    <message name="manual" translatable="true">Manual</message>
    <message name="varies" translatable="true">varies</message>
    <message name="title" translatable="true">Save</message>
    <message name="titleSaveAll" translatable="true">Save to All Selected</message>
    <message name="recordsChange" translatable="true">You have made modifications that have not been saved.  Do you wish to continue?</message>
    <message name="instructions1" translatable="true">To adjust the date that trade hours are shown:</message>
    <message name="instructions2" translatable="true">- For Fixed schedules , change the Date for First PM, Interval</message>
    <message name="instructions21" translatable="true">Type,Freq.Interval values,or Current Frequency.</message>
    <message name="instructions3" translatable="true">- For Floating schedules , change the Interval Type, Freq,</message>
    <message name="instructions31" translatable="true">Interval values,or Current Frequency.</message>
    <message name="mainWarehouseNotExistsMsg" translatable="true">Main storage location must not be empty</message>
    <message name="viewButtonName" translatable="true">View</message>
    <message name="mustSeasonal" translatable="true">Each recurrence pattern should be seasonal.</message>
    <message name="seasonalOverlap" translatable="true">Please make sure seasonal not overlapping.</message>
    <message name="mustFixed" translatable="true">It should be fixed schedule for weekly, monthly and yearly recurrence pattern.</message>
    <message name="editByGroupTitle" translatable="true">Edit PM Schedules</message>
    
    <dataSource id="ds_ab-pm-def-sched_pms">
        <table name="pms" role="main"/>
        <table name="pmp" role="standard"/>
        <table name="regprogram" role="standard"/>
        <field table="pms" name="pms_id"/>
        <field table="pms" name="pmp_id"/>
        <field table="pms" name="pm_group"/>
        <field table="pms" name="eq_id"/>
        <field table="pms" name="site_id"/>
        <field table="pms" name="bl_id"/>
        <field table="pms" name="fl_id"/>
        <field table="pms" name="rm_id"/>
        <field table="pms" name="comments"/>
        <field table="pms" name="date_first_todo"/>
        <field table="pms" name="date_last_completed"/>
        <field table="pms" name="date_next_alt_todo"/>
        <field table="pms" name="date_next_todo"/>
        <field table="pms" name="dv_id"/>
        <field table="pms" name="dp_id"/>
        <field table="pms" name="hours_calc"/>
        <field table="pms" name="hours_est"/>
        <field table="pms" name="interval_freq"/>
        <field table="pms" name="interval_1"/>
        <field table="pms" name="interval_2"/>
        <field table="pms" name="interval_3"/>
        <field table="pms" name="interval_4"/>
        <field table="pms" name="interval_type"/>
        <field table="pms" name="meter_last_pm"/>
        <field table="pms" name="nactive"/>
        <field table="pms" name="priority"/>
        <field table="pms" name="total_unit"/>
        <field table="pms" name="units"/>
        <field table="pms" name="fixed"/>
        <field table="pms" name="pt_store_loc_id"/>
        <field table="pms" name="criticality"/>
        <field table="pms" name="regulation"/>
        <field table="pms" name="reg_program"/>
        <field table="pms" name="reg_requirement"/>
        <field table="pms" name="location_id"/>
        <field table="pms" name="sla_work_team_id"/>
        <field table="pms" name="days_late"/>
        <field table="pms" name="days_early"/>
        <field table="pms" name="due_date_from"/>
        <field table="pms" name="int1_recurrence"/>
        <field table="pms" name="int2_recurrence"/>
        <field table="pms" name="int3_recurrence"/>
        <field table="pms" name="int4_recurrence"/>
        <field table="regprogram" name="vn_id"/>
        <field table="pms" name="cost_code"/>
        <field table="pmp" name="units_hour"/>
    </dataSource>
    
    <panel type="form" id="pms_info" dataSource="ds_ab-pm-def-sched_pms" region="center">
        <title translatable="true">Edit PM Schedule</title>
        <action id="copy">
            <title translatable="true">Copy</title>
        </action>
        <action id="save">
            <title translatable="true">Save</title>
        </action>
        <field table="pms" name="pms_id" hidden="true"/>
        <field table="pms" name="pmp_id" hidden="true"/>
        <field table="pms" name="eq_id" hidden="true"/>
        <field table="pms" name="site_id" hidden="true"/>
        <field table="pms" name="bl_id" hidden="true"/>
        <field table="pms" name="fl_id" hidden="true"/>
        <field table="pms" name="rm_id" hidden="true"/>
        <field table="pms" name="pt_store_loc_id" hidden="true"/>
    </panel>
    
    <panel type="html" id="panelE">
        
        <html>
            <div id="instructions" class="text" style="font-size: 9pt"></div>
        </html>
    </panel>
    
    <panel type="form" id="pms_eq_basic" dataSource="ds_ab-pm-def-sched_pms" columns="2" showOnLoad="false" selectionEnabled="true" region="center">
        <title translatable="true">Basic Information</title>
        <field table="pms" name="pms_id"/>
        <field table="pms" name="dv_id"/>
        <field table="pms" name="pmp_id" readOnly="true"/>
        <field table="pms" name="dp_id"/>
        <field table="pms" name="eq_id" readOnly="true"/>
        <field table="pms" name="pm_group"/>
        <field table="pms" name="criticality"/>
        <field table="pms" name="sla_work_team_id"/>
    </panel>
    
    <panel type="form" id="pms_rm_basic" dataSource="ds_ab-pm-def-sched_pms" columns="2" showOnLoad="false" selectionEnabled="true" region="center">
        <title translatable="true">Basic Information</title>
        <field table="pms" name="pms_id"/>
        <field table="pms" name="dv_id"/>
        <field table="pms" name="pmp_id" readOnly="true"/>
        <field table="pms" name="dp_id"/>
        <field/>
        <field table="pms" name="pm_group"/>
        <field table="pms" name="site_id" readOnly="true"/>
        <field table="pms" name="fl_id" readOnly="true"/>
        <field table="pms" name="bl_id" readOnly="true"/>
        <field table="pms" name="rm_id" readOnly="true"/>
        <field table="pms" name="criticality"/>
        <field table="pms" name="sla_work_team_id"/>
        
    </panel>
    
    <panel type="form" id="pms_service_contract" dataSource="ds_ab-pm-def-sched_pms" columns="2" showOnLoad="false" selectionEnabled="true" region="center">
        <title translatable="true">Service Contract</title>
        <field table="pms" name="regulation"/>
        <field table="pms" name="reg_requirement">
            <title>Term Code</title>
        </field>
        <field table="pms" name="reg_program">
            <title>Compliance Contract Code</title>
        </field>
        <field table="pms" name="location_id"/>
        <field table="regprogram" name="vn_id" readOnly="true"/>
        <field table="pms" name="cost_code" readOnly="true"/>
    </panel>
    
    <panel type="form" id="pms_schedule" dataSource="ds_ab-pm-def-sched_pms" columns="2" showOnLoad="false" selectionEnabled="true" region="center">
        <title translatable="true">Schedule Information</title>
        <field table="pms" name="date_last_completed"/>
        <field table="pms" name="hours_calc"/>
        <field table="pms" name="date_first_todo"/>
        <field table="pms" name="hours_est"/>
        <field table="pms" name="date_next_alt_todo"/>
        <field table="pms" name="days_late"/>
        <field table="pms" name="date_next_todo"/>
        <field table="pms" name="days_early"/>
        <field table="pms" name="interval_type" onchange="showFrequencyBaseOnIntervalType()"/>
        <field table="pms" name="due_date_from"/>
        <field table="pms" name="interval_freq"/>
        <field table="pms" name="fixed"/>
        <field table="pms" name="interval_1"/>
        <field table="pms" name="interval_2"/>
        <field table="pms" name="interval_3"/>
        <field table="pms" name="interval_4"/>
        <field table="pms" name="int1_recurrence" hidden="true"/>
        <field table="pms" name="int2_recurrence" hidden="true"/>
        <field table="pms" name="int3_recurrence" hidden="true"/>
        <field table="pms" name="int4_recurrence" hidden="true"/>
    </panel>
    
    <tabs id="selectRecurrenceFreqTabs" workflow="free" tabRefreshPolicy="never" region="center" showOnLoad="false">
        <tab name="int1_recurrence" selected="true" useFrame="false">
            <title translatable="true">Frequency 1</title>
        </tab>
        <tab name="int2_recurrence" selected="false" useFrame="false">
            <title translatable="true">Frequency 2</title>
        </tab>
        <tab name="int3_recurrence" selected="false" useFrame="false">
            <title translatable="true">Frequency 3</title>
        </tab>
        <tab name="int4_recurrence" selected="false" useFrame="false">
            <title translatable="true">Frequency 4</title>
        </tab>
    </tabs>
    
    <panel type="view" id="recurringPatternPanel" file="ab-common-recurring-pattern-edit.axvw" layoutRegion="xxx" showOnLoad="false" />
    
    <panel type="form" id="pms_other" dataSource="ds_ab-pm-def-sched_pms" columns="3" showOnLoad="false" labelsPosition="top" selectionEnabled="true" region="center">
        <title translatable="true">Other Information</title>
        
        <field table="pms" name="pt_store_loc_id"/>
        <field table="pms" name="total_unit"/>
        <field table="pms" name="units"/>
        <field table="pms" name="comments"/>
        <field table="pms" name="meter_last_pm"/>
        <field table="pms" name="nactive"/>
        <field table="pmp" name="units_hour" hidden="true"/>
    </panel>
    
    <js file="ab-pm-def-sched-mpsl.js"/>
    <js file="ab-pm-common.js"/>
</view>
