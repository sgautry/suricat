/**
 * Common map controller.
 * 
 * by Jia
 */
var mapCommonCtrl=View.createController('mapCommonCtrl',{
	// Define Map Controller
	mapControl : null,
	
	//this value means that which map view using this controller, it is override by sub map view.MarkerColorMarkerColor
	mapType:"",
	//by default , use framework API marker popup.
	usePopup: true,
	
	/**
	 * Initial leaflet component
	 */
	afterViewLoad : function() {
		var configObject = new Ab.view.ConfigObject();
		configObject.mapImplementation = 'Esri';
    	configObject.basemap = View.getLocalizedString('World Light Gray Canvas');
		this.mapControl = new Ab.leaflet.Map('mapPanel', 'mapDiv', configObject);
		//add map event listener if usePopup is false.
		if(!this.usePopup){
			this.mapControl.mapClass.addEventListener('markerClick', this.onMarkerClick, this);
			this.mapControl.mapClass.addEventListener('mapClick', this.onMapClick, this);
		}
	},
	
	/**
	 * map marker click event handler. if user use this property should set [usePopup: false] into marker properties.
	 * 
	 * @param {assetId} Building Code
	 * @param {featrue} Map marker feature
	 */
	onMarkerClick: function(assetId, feature){},
	
	/**
	 * map click event handler, it always used to hide the legend popup and marker popup.
	 */
	onMapClick: function(){
		this.mapControl.clearSelectedMarkers();
        this.mapControl.hideMarkerInfoWindow();
	},
	
	/**
	 * Show Marker legend
	 */
	showLegend: function(){
		mapController.mapControl.showMarkerLegend();
	}
});

/********************************************************Common methods and properties of MPSL map view controller******************************************************/
//Define map maker color properties
var MarkerColors={
	RED: "#ff0000",
	YELLOW: "#ffff00",
	GREEN: "#00ff00",
	//work request maker color,bule
	WR_COLOR:"#4a86e8"
};
//Define map legend type,this various
var MapDataTypes={
		//in this mode:
		//(1).map marker show all storage location;
		//(2).all map marker color is green;
		//(3).the map marker popup content field only show Storage Location Code /Storage Location Name fields and 'Select' Action.
		PSL: "loc",
		
		//in this mode:
		//(1).map show marker based on console form part field
		//(2).map color show red/green/yellow color based on console form
		//(3).map popup content show Quantity Available/Quantity Understocked fields.
		//(4).map marker popup content action show 'Requistion' and 'Purchase'
		PSLP: "loc_pt",	
};

//Define Map Type enum list, subview should overide mapType value using item of this enum list.
var MapTypes={
		//total parts inventory map view
		MAP_TOTAL_PARTS: 'total_parts_invenoty_map',
		//parts by location map view
		MAP_PARTS_BY_LOC: 'part_by_location_map',
		//find parts map view
		MAP_FIND_PARTS: 'find_part_map'
};
/**
 * @Override
 * override framework API to re-build map legend content by map type.
 */
Ab.leaflet.Base.prototype._updateLegendContent=function(markerProperties) {
	//console.log('updateLegendContent...');  
	var htmlContent;
	
	switch(mapCommonCtrl.mapType){
		case MapTypes.MAP_PARTS_BY_LOC: 
			htmlContent=getLegendContentOfByLocationMap();
			break;
		case MapTypes.MAP_TOTAL_PARTS:
			htmlContent=getLegendContentOfTotalPartsInventoryMap();
			break;
		case MapTypes.MAP_FIND_PARTS:
			htmlContent=getLegendOfFindPartsMap();
			break;
	}
    
    $(this.divId +'_leafletLegendContent').innerHTML = htmlContent;
}

/**
 * Get customized map legend content of Parts By Location view.
 * @return {String} Customized HTML legend content
 */
function getLegendContentOfByLocationMap(){
	var htmlContent;
    //Discussed with Burke that if Quantity Available value in console form not empty, the legend similar with Find Parts Map legend. else , similar with The first tab Map legend.
    if(valueExistsNotEmpty(mapController.qtyOnHand)){
    	htmlContent = "<table>";
        htmlContent += '<tr><td style="background-color:#00ff00; width:25px"></td><td class="leafletLegendLabel"><b>&nbsp;&nbsp;&nbsp;'+getMessage('mapAvailableLabel')+'</b></td></tr>';   
        htmlContent += '<tr><td></td><td></td></tr>';
        htmlContent += '<tr><td style="background-color:#ffff00; width:25px"></td><td class="leafletLegendLabel"><b>&nbsp;&nbsp;&nbsp;'+getMessage('mapPartlyAvailableLabel')+'</b></td></tr>';          
        htmlContent += '<tr><td></td><td></td></tr>';
        htmlContent += '<tr><td style="background-color:#ff0000; width:25px"></td><td class="leafletLegendLabel"><b>&nbsp;&nbsp;&nbsp;'+getMessage('mapUnavailableLabel')+'</b></td></tr>';   
        htmlContent += "</table>";
    }else{
    	htmlContent = "<table>";
        htmlContent += '<tr><td style="background-color:#00ff00; width:25px"></td><td class="leafletLegendLabel"><b>&nbsp;&nbsp;&nbsp;'+getMessage('mapGreenLabel')+'</b></td></tr>';   
        htmlContent += '<tr><td></td><td></td></tr>';
        htmlContent += '<tr><td style="background-color:#ffff00; width:25px"></td><td class="leafletLegendLabel"><b>&nbsp;&nbsp;&nbsp;'+getMessage('mapYellowLabel')+'</b></td></tr>';          
        htmlContent += '<tr><td></td><td></td></tr>';
        htmlContent += '<tr><td style="background-color:#ff0000; width:25px"></td><td class="leafletLegendLabel"><b>&nbsp;&nbsp;&nbsp;'+getMessage('mapRedLabel')+'</b></td></tr>';   
        htmlContent += "</table>";
    }
    
    return htmlContent;
}

/**
 * Get customized map legend of Total Parts Inventory view.
 * 
 * @returns {String} Customized HTML legend content
 */
function getLegendContentOfTotalPartsInventoryMap(){
	var htmlContent;
		htmlContent = "<table>";
		htmlContent += '<tr><td style="background-color:#00ff00; width:25px"></td><td class="leafletLegendLabel"><b>&nbsp;&nbsp;&nbsp;'+getMessage('mapGreenLabel')+'</b></td></tr>';   
		htmlContent += '<tr><td></td><td></td></tr>';
		htmlContent += '<tr><td style="background-color:#ffff00; width:25px"></td><td class="leafletLegendLabel"><b>&nbsp;&nbsp;&nbsp;'+getMessage('mapYellowLabel')+'</b></td></tr>';          
		htmlContent += '<tr><td></td><td></td></tr>';
		htmlContent += '<tr><td style="background-color:#ff0000; width:25px"></td><td class="leafletLegendLabel"><b>&nbsp;&nbsp;&nbsp;'+getMessage('mapRedLabel')+'</b></td></tr>';   
		htmlContent += "</table>";
		
	return htmlContent;
}

function getLegendOfFindPartsMap(){
	var htmlContent;
    htmlContent = "<table>";
    
    if(mapController.mapDataType==MapDataTypes.PSL){
    	htmlContent += '<tr><td style="background-color:#00ff00; width:25px"></td><td class="leafletLegendLabel"><b>&nbsp;&nbsp;&nbsp;'+getMessage('mapAvailableLabel')+'</b></td></tr>';   
        htmlContent += '<tr><td></td><td></td></tr>';
        htmlContent += '<tr><td style="background-color:#4a86e8; width:25px"></td><td class="leafletLegendLabel"><b>&nbsp;&nbsp;&nbsp;'+getMessage('workRequestLabel')+'</b></td></tr>';
        
    }else{
    	
        htmlContent += '<tr><td style="background-color:#00ff00; width:25px"></td><td class="leafletLegendLabel"><b>&nbsp;&nbsp;&nbsp;'+getMessage('mapAvailableLabel')+'</b></td></tr>';   
        htmlContent += '<tr><td></td><td></td></tr>';
        htmlContent += '<tr><td style="background-color:#ffff00; width:25px"></td><td class="leafletLegendLabel"><b>&nbsp;&nbsp;&nbsp;'+getMessage('mapPartlyAvailableLabel')+'</b></td></tr>';          
        htmlContent += '<tr><td></td><td></td></tr>';
        htmlContent += '<tr><td style="background-color:#ff0000; width:25px"></td><td class="leafletLegendLabel"><b>&nbsp;&nbsp;&nbsp;'+getMessage('mapUnavailableLabel')+'</b></td></tr>'; 
        htmlContent += '<tr><td></td><td></td></tr>';
        htmlContent += '<tr><td style="background-color:#4a86e8; width:25px"></td><td class="leafletLegendLabel"><b>&nbsp;&nbsp;&nbsp;'+getMessage('workRequestLabel')+'</b></td></tr>';
        
    }
    
    htmlContent += "</table>";
    
    return htmlContent;
}

/**
 * Marker purchase action event listener.
 * @param keyValue Key Value
 */
function markerPurchaseActionEvent(keyValue){
	//Replace keyValue by inner HTML of pop-up content title.
	mapController.showSelectLocationAction(keyValue,'Purchase');
}

/**
 * Marker requisition action event listener.
 * @param keyValue Key Value
 */
function markerRequisitionActionEvent(keyValue){
	//Replace keyValue by inner HTML of pop-up content title.
	mapController.showSelectLocationAction(keyValue,'Requistion');
}

