/**
 * MPSL tabs supper controller.
 * 
 * @author Jia
 * @since 24.1
 */
var abPtInvMpslCommonTabsCtrl=View.createController('abPtInvMpslCommonTabsCtrl',{
	//register tab view and controller to this object.
	tabCtrls: {},
	//tabs panel ID of sub tabs view. every sub view inherit this value.
	tabsPanelId: null,
	
	/**
	 * register Tab view change event. Such as , beforeTabChange/ afterTabChange
	 * @param {tabsPanel} Tabs Panel Id
	 */
	afterViewLoad: function(){
		var tabsPanel=View.panels.get(this.tabsPanelId);
		tabsPanel.addEventListener('afterTabChange', this.afterTabChange.createDelegate(this));
		tabsPanel.addEventListener('beforeTabChange',this.beforeTabChange.createDelegate(this));
	},
	
	/**
	 * before tab change.
	 */
	beforeTabChange: function(tabPanel, currentTabName, newTabName){
		var canChange=true;
		//find current tab.
		var currentTab = tabPanel.findTab(currentTabName);
		if (currentTab.isContentLoaded) {
			var controller = this.tabCtrls[currentTabName];
			canChange =controller.beforeTabSelect(this);
		}
		
		return canChange;
	},
	/**
	 * after tab change.
	 */
	afterTabChange: function(tabPanel, newTabName){
		var currentTab = tabPanel.findTab(newTabName);
	    if (currentTab.isContentLoaded) {
	    	var controller=this.tabCtrls[newTabName];
	    	controller.afterTabSelect(this);
	    }
	}
});