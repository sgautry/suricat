/**********************************************Common Calculate Methods********************************************************/
/**
 * Call workflow CalculateInventoryUsage for calculate part inventory
 */
function calculateInventoryUsage(){
	// kb 3042748 add Status bars and confirmation messages 
    View.openProgressBar(View.getLocalizedString(this.z_PROGRESS_MESSAGE));  
	try {
		Workflow.callMethod('AbBldgOpsBackgroundData-calculateWorkResourceValues-CalculateInventoryUsageForMPSL');
		View.closeProgressBar();
		View.alert(getMessage('calculateAlertMessage'));
	}catch(e){
		Workflow.handleError(e);
		View.closeProgressBar();
		return;
	}
}


/***********************************************Common Util Methods***********************************************/
/**
 * Check the input value is integer type.
 * 
 * @param {str} input value
 */
function isInt(str){
	var reg = /^(-|\+)?\d+$/ ;
	return reg.test(str);
}

/**
 * Enable or disable fields in special panel.
 * 
 * @panelId fields panel Id.
 * @fieldsArray Fields name array
 * @enable true or false
 */
function enablePanelFields(panelId,fieldsArray,enable){
	var isEnable=true;
	if(valueExistsNotEmpty(enable)){
		isEnable=enable;
	}
	_.each(fieldsArray,function(field){
		View.panels.get(panelId).enableField(field,isEnable);
	});
}


/**
 * Helper method: Validates ID
 * Check if field value exist in the data table.
 */
function valid_ID(tableName, fieldName, fieldValue){
	var isValid = false;
	
	var restriction = new Ab.view.Restriction();
	restriction.addClause(tableName + "." + fieldName, fieldValue, '=');
		var parameters = {
			tableName: tableName,
			fieldNames: "[" + tableName + '.' + fieldName + "]",
			restriction: toJSON(restriction) 
		};     		
 	var result = Workflow.runRuleAndReturnResult('AbCommonResources-getDataRecords', parameters);     		
 	if (result.code == 'executed') {
 		if (result.data.records.length == 1) isValid = true;
 	}	
    return isValid;
}

/**
 * Set field value to JSONArray object.
 * 
 * @param {panelId} Panel Id.
 * @param {JSONArray} JSON array.
 * @param {index} Array index.
 * @param {fieldNames} Field Names array.
 */
function setFieldValueToJSONArrayObject(panelId,JSONArray,fieldNames){
	_.each(fieldNames,function(fieldName){
		JSONArray[fieldName]=View.panels.get(panelId).getFieldValue(fieldName);
	});
}