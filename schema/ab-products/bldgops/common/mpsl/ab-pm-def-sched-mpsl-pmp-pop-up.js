/**
 * View PMP details controller
 */
var viewPMPDetailsController = View.createController('viewPMPDetailsController', {

    afterViewLoad: function() {
        // config pmp tree node
        this.configTreeNodeForPmpTree();

    },

    afterInitialDataFetch: function() {
        var restriction = new Ab.view.Restriction();
        restriction.addClause('pmp.pmp_id', this.pmpInfo.getFieldValue('pmp.pmp_id'), '=');
        this.pmp_tree.refresh(restriction);
        View.setTitle(View.title + ': ' + this.pmpInfo.getFieldValue('pmp.pmp_id'))
    },

    configTreeNodeForPmpTree: function() {
        var pmpTreePanel = View.panels.get('pmp_tree');
        pmpTreePanel.setTreeNodeConfigForLevel(0, [ {
            text: getMessage("stepUpper")
        }, {
            fieldName: 'pmps.pmps_id'
        }, {
            fieldName: 'pmps.instructions',
            length: 40
        } ]);
        pmpTreePanel.setTreeNodeConfigForLevel(1, [ {
            fieldName: 'pmpstr.res_type',
            pkCssClass: 'true'
        }, {
            fieldName: 'pmpstr.res_id',
            pkCssClass: 'true'
        }, {
            fieldName: 'pmpstr.res_req_value'
        } ]);
    }

});
