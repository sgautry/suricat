var LaborSchedulerWorkRequestDetailControl = View.createController('LaborSchedulerWorkRequestDetailControl', {
    
    wrId: null,
    
    afterInitialDataFetch: function() {
        var wrId = View.parameters['wrId'];
        this.wrId = wrId;
        if(valueExistsNotEmpty(wrId)){
            this.showPanels(wrId);
        }
        
        //If this view is opened from task detail pop-up view (laborScheduleTaskDetailCtrl), then hide all panels.
        var isSubView = View.controllers.get('laborScheduleTaskDetailCtrl');
        if(isSubView){
            this.hidePanels();
        }
    },
    
    /**
     * Show all panels and refresh by selected work request code. 
     */
    showPanels: function(wrId){
        var restriction=new Ab.view.Restriction();
        restriction.addClause('wr.wr_id',wrId,'=');
        this.panel_request.refresh(restriction);
        this.panel_location.refresh(restriction);
        this.panel_equipment.refresh(restriction);
        this.panel_description.refresh(restriction);
        var tradesRestriction = new Ab.view.Restriction();
        tradesRestriction.addClause('wrtr.wr_id',wrId,'=');
        this.panel_trades.refresh(tradesRestriction);
        var craftspersonsRestriction = new Ab.view.Restriction();
        craftspersonsRestriction.addClause('wrcf.wr_id',wrId,'=');
        this.panel_craftspersons.refresh(craftspersonsRestriction);
        this.panel_estimation.refresh(restriction);
        this.panel_escalation.refresh(restriction);
    },
    
    /**
     * Hide all panel.
     */
    hidePanels: function(){
        this.panel_request.show(false);
        this.panel_location.show(false);
        this.panel_equipment.show(false);
        this.panel_description.show(false);
        this.panel_trades.show(false);
        this.panel_craftspersons.show(false);
        this.panel_estimation.show(false);
        this.panel_escalation.show(false);
    },
    
    panel_request_afterRefresh: function(){
        if(this.wrId){
            var relatedWorkRequests = this.getRelatedWorkRequests(this.wrId);
            this.showRelatedWorkRequests(relatedWorkRequests);
        }
    },

    /**
     * Reset form fields after form refresh.
     */
    panel_description_afterRefresh: function() {
        var probType = this.panel_description.getFieldValue('wr.prob_type');
        if (probType === 'PREVENTIVE MAINT') {
            this.panel_description.showField('wr.pmp_id', true);
            this.panel_description.showField('wr.pms_id', true);
        } else {
            this.panel_description.showField('wr.pmp_id', false);
            this.panel_description.showField('wr.pms_id', false);
        }
    },

    /**
     * Load related work requests.
     */
    getRelatedWorkRequests: function(wrId) {
        var relatedRequests = [];
        var result = {};
        try {
            result = Workflow.callMethod('AbBldgOpsOnDemandWork-WorkRequestService-getRelatedWorkRequests', parseInt(wrId));
        } catch (e) {
            Workflow.handleError(e);
        }

        var controller = this;

        if (result.code == 'executed') {
            relatedRequests = eval('(' + result.jsonExpression + ')');
        }

        return relatedRequests;
    },

    /**
     * Show related work requests and add click event to every work request.
     */
    showRelatedWorkRequests: function(relatedWorkRequests) {
        if (relatedWorkRequests.length > 0) {
            for (var i = 0; i < relatedWorkRequests.length; i++) {
                var wrId = relatedWorkRequests[i];
                jQuery('<tr><td><span class="relatedRequestLink">' + wrId + '</span></td></tr>').appendTo(jQuery('#relatedRequestsTable'));
            }

            jQuery('.relatedRequestLink').click(function() {
                var openerView = View.getOpenerView();
                var relatedId = jQuery(this).text();
                var restriction = new Ab.view.Restriction();
                restriction.addClause('wr.wr_id', relatedId);

                var relatedView = 'ab-bldgops-labor-scheduler-wr-info.axvw';
                var wrRecords = View.dataSources.get('wrDS').getRecords(restriction);
                if (wrRecords.length == 0) {
                    relatedView = 'ab-bldgops-labor-scheduler-wr-info-archived.axvw';
                }
                openerView.closeDialog();
                // open a new dialog.
                openerView.openDialog(relatedView, null, false, {
                    width: 1000,
                    height: 600,
                    wrId:relatedId
                });
            })
        }
    }
});