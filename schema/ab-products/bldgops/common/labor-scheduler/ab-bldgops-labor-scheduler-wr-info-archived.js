var LaborSchedulerWorkRequestArchivedDetailControl = View.createController('LaborSchedulerWorkRequestArchivedDetailControl', {
    /**
     * Work request code from view parameters.
     */
    wrId: null,
    
    afterViewLoad: function() {
        var wrId = View.parameters['wrId'];
        if(valueExistsNotEmpty(wrId)){
            this.showPanels(wrId);
            var relatedWorkRequests = this.getRelatedWorkRequests(wrId);
            this.showRelatedWorkRequests(relatedWorkRequests);
        }
    },
    
    /**
     * Show all panels and refresh by selected work request code. 
     */
    showPanels: function(wrId){
        var restriction=new Ab.view.Restriction();
        restriction.addClause('hwr.wr_id',wrId,'=');
        this.panel_request.refresh(restriction);
        this.panel_location.refresh(restriction);
        this.panel_equipment.refresh(restriction);
        this.panel_description.refresh(restriction);
        this.panel_estimation.refresh(restriction);
        this.panel_escalation.refresh(restriction);
    },

    /**
     * Reset field after panel refresh.
     */
    panel_description_afterRefresh: function() {
        // If the Problem Type = Preventive Maintenance then display the fields PM Procedure and PM Schedule
        var probType = this.panel_description.getFieldValue('hwr.prob_type');
        if (probType === 'PREVENTIVE MAINT') {
            this.panel_description.showField('hwr.pmp_id', true);
            this.panel_description.showField('hwr.pms_id', true);
        } else {
            this.panel_description.showField('hwr.pmp_id', false);
            this.panel_description.showField('hwr.pms_id', false);
        }
    },

    /**
     * Load related work requests.
     */
    getRelatedWorkRequests: function(wrId) {
        var relatedRequests = [];
        var result = {};
        try {
            result = Workflow.callMethod('AbBldgOpsOnDemandWork-WorkRequestService-getRelatedWorkRequests', parseInt(wrId));
        } catch (e) {
            Workflow.handleError(e);
        }

        var controller = this;

        if (result.code == 'executed') {
            relatedRequests = eval('(' + result.jsonExpression + ')');
        }

        return relatedRequests;
    },

    /**
     * Show related work requests and add click event to every work request.
     */
    showRelatedWorkRequests: function(relatedWorkRequests) {
        if (relatedWorkRequests.length > 0) {
            for (var i = 0; i < relatedWorkRequests.length; i++) {
                var wrId = relatedWorkRequests[i];
                jQuery('<tr><td><span class="relatedRequestLink">' + wrId + '</span></td></tr>').appendTo(jQuery('#relatedRequestsTable'));
            }

            jQuery('.relatedRequestLink').click(function() {
                var openerView = View.getOpenerView();
                var relatedId = jQuery(this).text();
                var restriction = new Ab.view.Restriction();
                restriction.addClause('wr.wr_id', relatedId);

                var relatedView = 'ab-bldgops-labor-scheduler-wr-info.axvw';
                var wrRecords = View.dataSources.get('wrDS').getRecords(restriction);
                if (wrRecords.length == 0) {
                    relatedView = 'ab-bldgops-labor-scheduler-wr-info-archived.axvw';
                }
                openerView.closeDialog();
                
                openerView.openDialog(relatedView, null, false, {
                    width: 1000,
                    height: 600,
                    wrId:relatedId
                });
            })
        }
    }
});