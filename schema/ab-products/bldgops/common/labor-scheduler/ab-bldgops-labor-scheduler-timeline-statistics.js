/**
 * Calculates schedule statistics for one craftsperson and specified date range, e.g. 6 months from current date.
 *
 * See https://confluence.archibus.zone/pages/viewpage.action?spaceKey=AP&title=Prorated+Unscheduled+Hours+-+Algorithm+Outline.
 *
 * To calculate statistics for the calculation period, e.g. 6 months:
 * 1. Call addUnscheduledTask() for all unscheduled tasks that are due within the calculation time period.
 * 2. Call calculateSegmentStartDates().
 * 3. Call addScheduledTask() for all scheduled tasks within the calculation time period.
 * 4. Call addStandardHours() for all standard hours available in the craftsperson schedule.
 * 5. Call prorateUnscheduledSegments().
 * (order is important)
 * (call this sequence after the view loads, or if the user updates or removes an unscheduled task)
 *
 * To obtain unscheduled segments for the displayed time range, e.g. current week:
 * 1. Call getUnscheduledSegmentsForDateRange().
 *
 * To obtain statistics for the displayed time range, e.g. current week:
 * 1. Call getStatisticsForDateRange().
 */
WorkScheduleCraftspersonStatistics = Base.extend({

    /**
     * Calculation start date.
     */
    startDate: null,

    /**
     * Calculation end date.
     */
    endDate: null,

    /**
     * Array of WorkScheduleTaskSegment objects for unscheduled tasks, ordered by date due.
     */
    taskSegments: null,

    /**
     * Constructor.
     */
    constructor: function(startDate, endDate) {
        //start date should not be earlier than current date.
        var today = new Date();
            currentDate = new Date(today.getFullYear(),today.getMonth(),today.getDate(),0,0,0);
        if(startDate.getTime()<currentDate.getTime()){
            startDate = currentDate;
        }
        this.startDate = startDate;
        this.endDate = endDate;
        this.taskSegments = [];
    },

    /**
     * Adds an unscheduled task.
     * @param task An unscheduled task.
     */
    addUnscheduledTask: function(task) {
        var today = new Date();
            today = new Date(today.getFullYear(),today.getMonth(),today.getDate(),0,0,0);
        //Only add unscheduled task that due date after today.    
        if(task.dueDate.getTime()>= today.getTime()){
            var dueTasks = _.find(this.taskSegments, function(dueTask) {
                return (task.dueDate.getTime() === dueTask.dueDate.getTime());
            });

            if (!dueTasks) {
                dueTasks = new WorkScheduleTaskSegment(task.dueDate);
                this.taskSegments.push(dueTasks);
            }

            dueTasks.tasks.push(task);
        }
    },

    /**
     * Removes an unscheduled task.
     * @param task An unscheduled task.
     */
    removeUnscheduledTask: function(task) {
        var taskSegment = _.find(this.taskSegments, function(dueTask) {
            return (task.dueDate.getTime() === dueTask.dueDate.getTime());
        });

        if (taskSegment) {
            taskSegment.tasks = _.filter(taskSegment.tasks,function(dueTask) {
                return !(dueTask.wrId == task.wrId && dueTask.cfId == task.cfId && dueTask.startDate.getTime() == task.startDate.getTime())
            });
        }
    },

    /**
     * Calculates start dates for all segments.
     */
    calculateSegmentStartDates: function() {
        this.taskSegments = _.sortBy(this.taskSegments, 'dueDate');
        var today = new Date();
            today = new Date(today.getFullYear(),today.getMonth(),today.getDate(),0,0,0);
        var segmentStartDate = today;
        _.each(this.taskSegments, function(segment) {
            segment.setStartDate(segmentStartDate);
            segmentStartDate = segment.dueDate.add(Date.DAY, 1);
        });
    },

    /**
     * Adds a scheduled task hours to affected unscheduled segments.
     * @param task A scheduled task.
     */
    addScheduledHours: function(task) {
        var startTime = task.startDate.getTime();
        var endTime = task.endDate.getTime();
        
        _.each(this.taskSegments, function(segment) {
            //Segment dueDate should add one day, because due date always like '2017-07-21 00:00:00', so some task in July 21 can not add scheduled hours.
            var dueDate = segment.dueDate.add(Date.DAY,1);
            if (startTime <= dueDate) {
                // if the task starts before the unscheduled segment's due date, then SOME of its hours count as scheduled for this segment
                if (endTime <= dueDate) {
                    // if the task ends before the unscheduled segment's due date, then ALL of its hours count as scheduled for this segment
                    segment.addScheduledHours(hoursBetween(task.startDate, task.endDate), task.startDate);
                    // since all hours are used up, increment the time by one year so that the task does not count against the next segment
                    startTime = task.startDate.add(Date.YEAR, 1).getTime();
                } else {
                    // if the task ends before the unscheduled segment's due date, then add the hours before the segment's due date
                    segment.addScheduledHours(hoursBetween(task.startDate, dueDate), task.startDate);
                    // and prepare the start time for the next segment
                    startTime = dueDate;
                }
            }
        });
    },

    /**
     * Adds standard hours for specified date.
     * @param date The date in the craftsperson schedule.
     * @param standardHours The number of standard hours on that date.
     */
    addStandardHours: function(date, standardHours) {
        var dayStart = getDayStart(date);

        var segment = _.find(this.taskSegments, function(task) {
            return (dayStart.getTime() <= task.dueDate.getTime());
        });

        if (segment) {
            segment.addStandardHours(standardHours, dayStart);
        }
    },

    /**
     * Prorates unscheduled hours from each future segment into all preceding segments.
     */
    prorateUnscheduledSegments: function() {
        // calculate available hours for each segment, based on available hours of all preceding segments
        var precedingAvailableHours = 0;
        _.each(this.taskSegments, function(segment) {
            precedingAvailableHours += segment.calculateAvailableHours(precedingAvailableHours);
        });

        // prorate unscheduled hours for each segment to all segments that precede it
        _.each(this.taskSegments, function(segment, index, allSegments) {
            var precedingSegments = allSegments.slice(0, index);

            _.each(precedingSegments, function (precedingSegment) {
                precedingSegment.addProratedUnscheduledHours(segment);
            });

            segment.updatedRemainingUnscheduledHours();
        });

        // calculate percentage available
        _.each(this.taskSegments, function(segment) {
            segment.calculatePercentAvailable();
            segment.calculatePercentAllocated();
        });
    },

    /**
     * Returns unscheduled segments that are due within specified date range.
     * @param startDate
     * @param endDate
     */
    getUnscheduledSegmentsForDateRange: function(startDate, endDate) {
        // find first and last segments in the displayed date range
        var firstSegmentIndex = _.findIndex(this.taskSegments, function(segment) {
            return segment.dueDate.getTime() >= startDate.getTime();
        });
        var lastSegmentIndex = _.findLastIndex(this.taskSegments, function(segment) {
            return segment.dueDate.getTime() <= endDate.getTime();
        });
        var segments = this.taskSegments.slice(firstSegmentIndex, lastSegmentIndex + 1);

        // if there are extra days in the displayed date range after the last segment, add a filler segment
        if (segments.length == 0 || segments[segments.length - 1].dueDate.getTime() < endDate.getTime()) {
            var fillerSegment = new WorkScheduleTaskSegment(endDate);
            var fillerSegmentStartDate = startDate;
            if (segments.length > 0) {
                fillerSegmentStartDate = segments[segments.length - 1].dueDate.add(Date.DAY, 1);
            }
            fillerSegment.setStartDate(fillerSegmentStartDate);
            fillerSegment.type="fillerSegment";
            segments.push(fillerSegment);
        }

        return segments;
    },

    /**
     * Returns a "virtual" segment that contains statistics for specified date range.
     * @param startDate
     * @param endDate
     */
    getStatisticsForDateRange: function(startDate, endDate, craftspersonTotals) {
        var statistics = new WorkScheduleTaskSegment(endDate);

        // first segments due in the displayed date range
        var firstSegmentIndex = _.findIndex(this.taskSegments, function(segment) {
            return segment.dueDate.getTime() >= startDate.getTime();
        });

        // last segment due before the displayed date range
        var lastSegmentIndex = _.findIndex(this.taskSegments, function(segment) {
            return segment.dueDate.getTime() > endDate.getTime();
        });
        
        //If there is no segments after the display date range , so we should get the last segment index before the display date range.
        if(lastSegmentIndex == -1){
            lastSegmentIndex = _.findLastIndex(this.taskSegments, function(segment) {
                return segment.dueDate.getTime() <= endDate.getTime();
            });
        }
        
        var segments = [];
        if(firstSegmentIndex != -1 && lastSegmentIndex != -1){
            segments = this.taskSegments.slice(firstSegmentIndex, lastSegmentIndex + 1);
        }
        
        if (segments.length > 0 ) {
            // if the last segment spans the end of the date range
            var lastSegment = segments[segments.length - 1];
            if (lastSegment.dueDate.getTime() > endDate.getTime()) {
                // split it at the last day of the range, and replace
                segments[segments.length - 1] = lastSegment.createSplitSegment(lastSegment.startDate,endDate);
            }
            
            // if the first segment span the start of the date range
            var firstSegment = segments[0];
            if(firstSegment.startDate.getTime() < startDate.getTime()){
                //split it at the fist day of the range, and replace
                segments[0] = firstSegment.createSplitSegment(startDate,firstSegment.dueDate);
            }
        }
        // calculate the statistics
        //APP-2032:The weekly statistics are supposed to reflect all the remaining days in the visible week.
        statistics.standardHours = craftspersonTotals.availableHours;
        statistics.availableHours = craftspersonTotals.availableHours - craftspersonTotals.scheduledHours;
        statistics.scheduledHours = craftspersonTotals.scheduledHours;
        
        _.each(segments, function(segment) {
            statistics.unscheduledHours += segment.unscheduledHours;
            statistics.proratedUnscheduledHours += segment.proratedUnscheduledHours;
        });

        if (segments.length === 0) {
            statistics.unscheduledHours = 0;
            statistics.proratedUnscheduledHours = 0;
        }

        statistics.calculatePercentAvailable();
        statistics.calculatePercentAllocated();

        return statistics;
    }
});

/**
 * Calculates statistics for unscheduled tasks that are due on the same date.
 */
WorkScheduleTaskSegment = Base.extend({

    /**
     * Start date.
     */
    startDate: null,

    /**
     * Date due.
     */
    dueDate: null,

    /**
     * The array of task objects.
     */
    tasks: null,
    
    /**
     * The total estimated hours for all unscheduled tasks.
     * 
     * Why create this variable:
     * 1.unschedule hours label in unscheduled task bar should display unscheduled hours which not be prorated.
     * 2.we can also get this value by using unscheduledHours+proratedUnscheduledHours, but if they have decimals, 
     *   this may cause precision is not accurate after use toFixed() method.
     */
    initialUnscheduledHours: 0,

    /**
     * The total remaining estimated hours for all unscheduled tasks after prorated.
     */
    unscheduledHours: 0,

    /**
     * Total standard hours in the schedule for this task segment.
     */
    standardHours: 0,

    /**
     * Daily array of standard hours.
     */
    dailyStandardHours: null,

    /**
     * Total scheduled task hours overlapping this task segment.
     */
    scheduledHours: 0,

    /**
     * Daily array of scheduled hours.
     */
    dailyScheduleHours: null,

    /**
     * Available hours for this segment: standard hours - scheduled hours.
     */
    availableHours: 0,

    /**
     * Total available hours for this segment and all preceding segments.
     */
    totalAvailableHours: 0,

    /**
     * Prorated unscheduled hours.
     */
    proratedUnscheduledHours: 0,

    /**
     * Percentage of available time taken by prorated unscheduled work.
     * Prorated unscheduled Hours divided by available hours.
     * This is used to set the color of the unscheduled task bar.
     */
    percentAvailable: 0,

    /**
     * Percentage of time taken by all work.
     * Sum of scheduled hours and prorated unscheduled hours, divided by standard hours.
     * This is used for the craftsperson statistics for each week/month.
     */
    percentAllocated: 0,

    /**
     * Constructor.
     * @param dueDate
     */
    constructor: function(dueDate) {
        this.dueDate = dueDate;
        this.tasks = [];
    },

    /**
     * Sets the start date.
     * @param startDate
     */
    setStartDate: function(startDate) {
        this.startDate = startDate;
        this.standardHours = 0;
        this.scheduledHours = 0;
        this.proratedUnscheduledHours = 0;
        this.dailyStandardHours = new WorkScheduleDailyStore(this.startDate, this.dueDate);
        this.dailyScheduledHours = new WorkScheduleDailyStore(this.startDate, this.dueDate);
    },

    /**
     * Adds craftsperson available hours for this segment.
     * @param standardHours
     */
    addStandardHours: function(standardHours, date) {
        this.standardHours += standardHours;
        this.dailyStandardHours.addValue(standardHours, date);
    },

    /**
     * Adds scheduled hours for this segment.
     * @param scheduledHours
     */
    addScheduledHours: function(scheduledHours, date) {
        this.scheduledHours += scheduledHours;
        this.dailyScheduledHours.addValue(scheduledHours, date);
    },

    /**
     * Calculate available hours for this segment, taking into account available hours of all preceding segments.
     */
    calculateAvailableHours: function(precedingAvailableHours) {
        this.unscheduledHours = _.reduce(this.tasks, function(memo, task) {
            return memo + task.estimatedHours;
        }, 0);
        
        this.initialUnscheduledHours = this.unscheduledHours;

        this.availableHours = this.standardHours - this.scheduledHours;
        if (this.availableHours < 0) {
            this.availableHours = 0;
        }

        this.totalAvailableHours = precedingAvailableHours + this.availableHours;

        return this.availableHours;
    },

    /**
     * Adds prorated hours from a future segment.
     * @param futureSegment
     */
    addProratedUnscheduledHours: function(futureSegment) {
        //add a division by zero check.
        var proratedHours = 0;
        if(futureSegment.totalAvailableHours != 0){
            proratedHours = futureSegment.unscheduledHours * (this.availableHours / futureSegment.totalAvailableHours);
        }
        
        this.proratedUnscheduledHours += proratedHours;
    },

    /**
     * After this segment's unscheduled hours have been prorated to preceding segments, reduce unscheduled hours by prorated amount.
     */
    updatedRemainingUnscheduledHours: function() {
        //Add a division by zero check.
        var ratio = 0;
        if(this.totalAvailableHours == 0){
            if(this.availableHours == 0){
                ratio = 1;
            }else{
                ratio = 0;
            }
        }else{
            ratio = this.availableHours / this.totalAvailableHours;
        }
        this.unscheduledHours = this.unscheduledHours * ratio;
        
    },

    /**
     * Calculates the percentage of available time taken by prorated unscheduled work.
     * @returns {number}
     */
    calculatePercentAvailable: function() {
        this.percentAvailable = 0;
        if(this.availableHours != 0){
            this.percentAvailable = (this.unscheduledHours + this.proratedUnscheduledHours) / this.availableHours;
        }
    },

    /**
     * Calculates the percentage of standard time taken by all work.
     * @returns {number}
     */
    calculatePercentAllocated: function() {
        this.percentAllocated = 0;
        if(this.standardHours!=0){
            this.percentAllocated = (this.scheduledHours + this.unscheduledHours + this.proratedUnscheduledHours) / this.standardHours;
        }
    },

    /**
     * Creates a segment representing the first N days of this segment.
     * @param startDate Start date of the new segment.
     * @param endDate End date of the new segment.
     */
    createSplitSegment: function(startDate,endDate) {
        var splitSegment = new WorkScheduleTaskSegment(endDate);
        splitSegment.startDate = startDate;
        
        splitSegment.dailyStandardHours = this.dailyStandardHours;
        splitSegment.dailyScheduledHours = this.dailyScheduledHours;
        
        splitSegment.standardHours = this.dailyStandardHours.getTotalValueForDateRange(startDate, endDate);
        splitSegment.scheduledHours = this.dailyScheduledHours.getTotalValueForDateRange(startDate, endDate);

        splitSegment.availableHours = splitSegment.standardHours - splitSegment.scheduledHours;
        splitSegment.initialUnscheduledHours = this.unscheduledHours;
        
        //get prorated unscheduled hours via the ratio of the available hours.
        var ratio = 0;
        if (this.availableHours != 0 && splitSegment.availableHours > 0) {
            ratio = splitSegment.availableHours / this.availableHours;
        }
        if (ratio > 1) {
            ratio = 1;
        }
        splitSegment.proratedUnscheduledHours = this.proratedUnscheduledHours * ratio;
        
        //the unscheduled hours should also be splitted by the ratio.
        splitSegment.unscheduledHours = this.unscheduledHours * ratio;
		
		//recalculate percent available and allocated hours.
		splitSegment.calculatePercentAvailable();
        splitSegment.calculatePercentAllocated();
        return splitSegment;
    }
});

/**
 * Accumulates and provides access to daily numeric values.
 */
WorkScheduleDailyStore = Base.extend({

    /**
     * The start date.
     */
    startDate: null,

    /**
     * The array of daily values.
     */
    values: null,

    /**
     * Constructor.
     * @param startDate
     * @param endDate
     */
    constructor: function(startDate, endDate) {
        this.startDate = startDate;

        var values = [];
        var days = daysBetween(startDate, endDate) + 1;
        _.times(days, function() {
            values.push(0);
        });
        this.values = values;
    },

    /**
     * Adds a value for specified date.
     * @param value
     * @param date
     */
    addValue: function(value, date) {
        var index = this._getDateIndex(date);
        //If value in this.values array is NaN, then set a default value 0 to avoid incorrect calculation result.
        if(isNaN(this.values[index])){
            this.values[index] = 0;
        }
        this.values[index] += value;
    },

    /**
     * Gets accumulated value for specified date.
     * @param date
     */
    getValue: function(date) {
        var value = 0;
        var index = this._getDateIndex(date);
        if (this.values.length > index && index >= 0) {
            value = this.values[index];
        }
        return value;
    },

    /**
     * Returns total accumulated values for a date range.
     * @param startDate
     * @param endDate
     */
    getTotalValueForDateRange: function(startDate, endDate) {
        var totalValue = 0;
		//Add a NaN value check.
        for (var date = new Date(startDate.getTime());date.between(startDate, endDate);date = date.add(Date.DAY, 1)) {
            var value = this.getValue(date);
            if(!isNaN(value)){
                totalValue += value;
            }
            
        }

        return totalValue;
    },

    /**
     * Returns the index of specified date in the array.
     * @param date
     * @private
     */
    _getDateIndex: function(date) {
        return daysBetween(this.startDate, date);
    }
});

/**
 * Returns a date that has the same year, month, and day as specified date, and time = 00.00:00.
 * @param date
 * @returns {Date}
 */
function getDayStart(date) {
    var dayStart = new Date(date.getTime());
    dayStart.setHours(0, 0, 0);
    return dayStart;
}
