/**
 * The Single Craftsperson schedule calendar control implements https://confluence.archibus.zone/display/PLAT/HTML+Scheduling+Control.
 */
SingleCraftspersonWorkScheduleControl = Base.extend({

    /**
     * The configuration object.
     */
    config: null,

    /**
     * The dxScheduler control instance.
     */
    scheduler: null,

    /**
     * The WorkScheduleModel instance.
     */
    model: null,

    /**
     * Restriction for the schedule.
     */
    restriction: null,

    /**
     * converted view start date.
     */
    viewStartDate: null,

    /**
     * converted view end date.
     */
    viewEndDate: null,
    
    /**
     * Cached latest clicked cell date from month view or week/workweek view. If this property has value,then Day timeline view should display this day.
     */
    latestClickedCellDate: null,

    /**
     * Control constructor.
     * @param {config} The control configuration.
     */
    constructor: function(config) {
        this.config = config;
        this.model = this.config.model;

        this.initializeScheduler();
    },

    /**
     * Sets the control height to specified value.
     * @param {height} The height in pixels.
     */
    setHeight: function(height) {
        this.height=height;
    },

    /**
     * Adds a listener to specified control event.
     * @param {eventName} The name of the control event, e.g. 'click' or 'locateBuilding'.
     * @param {listener} The listener function reference.
     */
    addEventListener: function(eventName, listener) {
    },

    /**
     * Removes listeners from specified control event.
     * @param {eventName} The name of the control event, e.g. 'click' or 'locateBuilding'.
     * @param {listener} The listener function reference. Optional. If not specified, the control removes all event listeners for this event.
     */
    removeEventListener: function(eventName) {
    },

    /**
     * Initializes the dxScheduler control with customizations required for the Labor Scheduling Board.
     */
    initializeScheduler: function() {
        var control = this;

        var previousTrade = null;

        var scheduler = jQuery("#singleLaborWorkScheduleContainer").dxScheduler({
            dataSource: this.model.scheduledTasks,
            useDropDownViewSwitcher: false,
            views: [ "month", "week", "workWeek", "day" ],
            currentView: "workWeek",
            currentDate: this.config.startDate,
            showAllDayPanel: true,
            firstDayOfWeek: 1,
            startDayHour: 0,
            endDayHour: 24,
            cellDuration: 60,
            width: "100%",
            height:"100%",
            groups: [ "cfId" ],
            editing: {
                // disallow user create time blocks by click or type enter button, only allow create by drag and drop event.
                allowAdding: false
            },
            resources: [ {
                field: "cfId",
                dataSource: this.model.craftspersons,
                label: "Craftsperson",
                useColorAsDefault: false
            } ],
            //crossScrollingEnabled: true,

            // template display the appointment tooltip.
            appointmentTooltipTemplate: function(data, container) {
                var markup = control.getTooltipTemplate(data);
                return markup;
            },

            // display the time scale (hours) based on user's locale.
            timeCellTemplate: function(itemData, itemIndex, itemElement) {
                //APP-1468:Use the locale to determine which format to display time with U.S.format or 24-hours format.
                var formattedTime=itemData.text.replace('AM', 'a').replace('PM', 'p');
                jQuery("<div>").text(formattedTime).appendTo(itemElement);
            },
            
            // display craftsperson name ,if name is empty, then display craftsperson code.
            resourceCellTemplate: function(itemData, itemIndex, itemElement) {
                var craftspersonName=valueExistsNotEmpty(itemData.text)?itemData.text:itemData.id;
                jQuery("<div>").text(craftspersonName).appendTo(itemElement);
            },

            // display unavailable hours as diagonal stripes
            dataCellTemplate: function(cellData, index, container) {
                if (cellData.groups) {
                    var cfId = cellData.groups.cfId;

                    container.on('dragover', function(event) {
                        event.preventDefault();
                    });
                    container.on('drop', function(event) {
                        event.preventDefault();

                        var dragData = event.originalEvent.dataTransfer.getData("text");
                        var wrcfData = JSON.parse(dragData);
                        var wrId = wrcfData.wr_id;
                        var description = wrcfData.description;
                        // To indicate schedule task or unscheduled task.
                        var dragType = wrcfData.dragType;
                        var isScheduled = wrcfData.isScheduled;
                        
                        //Set original field value when unschedule task, use those fields to update original record in database.
                        if(dragType=='unschedule' || dragType == 'missedWork'){
                            var oldDateAssigned = new Date(Number(wrcfData.dateAssigned));
                            var oldTimeAssigned = new Date(Number(wrcfData.timeAssigned));
                            var dueDate = new Date(Number(wrcfData.dueDate));
                            var oldCfId = wrcfData.cfId;
                            var estimatedHours = parseFloat(wrcfData.estimatedHours);

                            //get split tasks and create scheduled tasks.
                            var tasks = getSplitedAssignments(wrId,cfId,cellData.startDate,estimatedHours);
                            if(tasks){
                                //delete original task
                                if(dragType=='unschedule'){
                                    control.model.deleteUnscheduledTaskByPrimaryKey(wrId,oldCfId,oldDateAssigned,oldTimeAssigned);
                                }
                                if(dragType == 'missedWork'){
                                    //set original assignment complete and then create a new assignment if assignment drag from missed work list.
                                    control.model.setAssignmentCompleted(wrId,oldCfId,oldDateAssigned,oldTimeAssigned,isScheduled);
                                }
                                for(var i=0;i<tasks.length;i++){
                                    tasks[i].description = description;
                                    tasks[i].taskType = 'TASK';
                                    tasks[i].dragType=dragType;
                                    scheduler.addAppointment(tasks[i]);
                                }
                            }
                            
                        }else{
                            //split assignment into multiple days according to the Trade Estimation.
                            var estimateHours = getEstimateHoursByPrimaryTrade(wrId,cfId);
                            var tasks = getSplitedAssignments(wrId,cfId,cellData.startDate,estimateHours);
                            if(tasks){
                                for(var i=0;i<tasks.length;i++){
                                    tasks[i].description = description;
                                    tasks[i].taskType = 'TASK';
                                    scheduler.addAppointment(tasks[i]);
                                }
                            }
                        }
                        

                    });
                }

                return jQuery('<div>').text(cellData.text);
            },

            /**
             * Define a custom HTML template for the task.
             */
            appointmentTemplate: function(task, itemIndex, itemElement) {
                // set default zIndex properties for appointment added through dragDrap event.
                jQuery(itemElement).parent()[0].style.zIndex = 150;
                var html = '';
                if (task.taskType === 'TASK') {
                    //APP-2078:display the last four digits of the Work Request Code instead of the entire code. 
                    var truncattedWrId = task.wrId;
                    if(task.wrId.length > 4){
                        truncattedWrId = task.wrId.substr(task.wrId.length-4);
                    }
                    html = '<div>' + truncattedWrId + '</div><div>' + task.description + '</div>';
                } else if (task.taskType === 'STANDARD') {
                    itemElement.addClass('standard-hours');
                } else if (task.taskType === 'OVERTIME') {
                    itemElement.addClass('overtime-hours');
                } else if (task.taskType === 'DOUBLETIME') {
                    itemElement.addClass('doubletime-hours');
                } else if (task.type === 'VARIANCE') {
                    if (task.taskType != 'STANDARD' && task.taskType != 'OVERTIME' && task.taskType != 'DOUBLETIME' && task.taskType != 'ON SITE') {
                        itemElement.addClass('unavailable-hours');
                    }

                    if (task.taskType == 'ON SITE') {
                        html="<div>"+task.siteId +'-'+task.blId+"</div>";
                        // reset site visit time block height.
                        itemElement.addClass('sitevisit-hours');
                    }

                }

                return jQuery(html);
            },
            
            /**
             * Triggered when the user adding scheduled work requests to dxScheduler control by dragging and dropping.
             * @param event
             */
            onAppointmentAdding: function(event) {
                var validatePassed = validateAppointment(event.appointmentData);
                if(validatePassed){
                    var editOptions = getWrcfRecordEditOptions(event.appointmentData);
                    if(editOptions.allowUpdating){
                        control.model.createScheduledTask(event.appointmentData);
                    }else{
                        View.alert(editOptions.updateAlertText);
                        event.cancel = true;
                    }
                }else{
                    event.cancel=true;
                }
            },
            /**
             * Triggered after user added scheduled work requests to dxScheduler control by dragging and dropping.
             * @param event
             */
            onAppointmentAdded: function(event) {
                control.trigger('app:bldgops:laborScheduler:afterWorkRequestScheduled', event.appointmentData);
            },

            /**
             * Triggered when the user changes the appointment using either the standard dxScheduler appointment dialog, or by dragging or resizing the appointment.
             * @param event
             */
            onAppointmentUpdating: function(event) {
                var isScheduled=event.newData.isScheduled;
                var validatePassed=true;
                if(isScheduled!='0'){
                    validatePassed = validateAppointment(event.newData);
                }
                    
                if(validatePassed){
                    var editOptions = getWrcfRecordEditOptions(event.oldData);
                    if(editOptions.allowUpdating){
                        // update estimate hours during update.
                        event.newData.estimatedHours = hoursBetween(event.newData.startDate, event.newData.endDate);
                        control.model.updateTask(event.newData, event.oldData);
                    }else{
                        View.alert(editOptions.updateAlertText);
                    }
                    
                }else{
                    event.cancel=true;
                } 
            },

            /**
             * Triggered after the user changes the appointment using either the standard dxScheduler appointment dialog, or by dragging or resizing the appointment.
             * @param event
             */
            onAppointmentUpdated: function(event) {
                control.trigger('app:bldgops:laborScheduler:afterWorkRequestUpdated', event.appointmentData);
            },

            /**
             * Triggered before the user delete the appointment using either the standard dxScheduler appointment dialog, or by dragging or resizing the appointment.
             * @param event
             */
            onAppointmentDeleting: function(event) {
                var editOptions = getWrcfRecordEditOptions(event.appointmentData);
                // if unschedule one task , only delete this appointment on client side, and update record on server side.
                if (event.appointmentData.actionType == 'unscheduled') {
                    if(editOptions.allowUpdating){
                        control.model.unscheduleTask(event.appointmentData);
                    }else{
                        View.alert(editOptions.updateAlertText);
                        event.cancel = true;
                    }
                    
                } else {
                    if(editOptions.allowDeleting){
                        control.model.deleteTask(event.appointmentData);
                    }else{
                        View.alert(editOptions.deleteAlertText);
                        event.cancel = true;
                    }
                }
            },

            /**
             * Triggered after the user deleted the appointment using either the standard dxScheduler appointment dialog, or by dragging or resizing the appointment.
             * @param event
             */
            onAppointmentDeleted: function(event) {
                control.trigger('app:bldgops:laborScheduler:afterWorkRequestDeleted', event.appointmentData);
            },

            /**
             * Event handler current date change and current view change.
             */
            onOptionChanged: function(event) {
                if (event.name === 'currentDate') {
                    control.loadSchedule(event.value, control.scheduler.option('currentView'));
                    control.latestClickedCellDate = event.value;
                }
                if (event.name == 'currentView') {
                    if(control.latestClickedCellDate){
                        control.scheduler.option('currentDate',control.latestClickedCellDate);
                    }
                    control.loadSchedule(control.scheduler.option('currentDate'), event.value);
                }
            },

            /**
             * Triggered after appointment rendered, to set appointment UI style.
             */
            onAppointmentRendered: function(event) {
                var appointmentText='';
                if(valueExistsNotEmpty(event.appointmentData.text)){
                    appointmentText=event.appointmentData.text;
                }
                var formattedDate=control.formatTooltipDate(event.appointmentData.startDate,event.appointmentData.endDate,false);
                
                if (event.appointmentData.type == 'REGULAR') {
                    event.appointmentElement[0].style['opacity'] = 0.1;
                    jQuery(event.appointmentElement)[0].style['z-index'] = 10;
                }
                if (event.appointmentData.type == 'VARIANCE') {
                    event.appointmentElement[0].style['opacity'] = 1;
                    jQuery(event.appointmentElement)[0].style['z-index'] = 30;
                }
                if (event.appointmentData.taskType == 'TASK') {
                    jQuery(event.appointmentElement)[0].style['z-index'] = 150;
                }
                if (event.appointmentData.taskType == 'ON SITE') {
                    appointmentText=getMessage('siteVisitText')+': '+event.appointmentData.siteId + '-' +event.appointmentData.blId;
                    var allDayDuration=this.appointmentTakesAllDay(event.appointmentData,0,24);
                    if(!allDayDuration){
                        jQuery(event.appointmentElement).width('30px');
                    }
                    jQuery(event.appointmentElement)[0].style['z-index'] = 50;
                }
                
                //show help text when mouse hover the appointment.
                event.appointmentElement.prop('title', appointmentText+' ('+formattedDate+')');
            },
            /**
             * Fires after a view cell has been clicked. to cache the latest cell date the user clicked.
             */
            onCellClick: function(e){
                //APP-1345:The day they had clicked on last should be the day or week that appears in the next timeline.
                control.latestClickedCellDate=e.cellData.startDate;
            }
        }).dxScheduler("instance");

        // placeholder for inter-scheduler drag and drop
        jQuery('.dx-scheduler-date-table-cell').on('dxdrop', function(event) {
        });

        // jQuery('.dx-scheduler-view-switcher').hide();
        // jQuery('.dx-scheduler-view-switcher-label').hide();

        this.scheduler = scheduler;

        /**
         * @override. Override API rederingStrategy prototype method to avoid appointment overlap side by side.
         */
        Object.getPrototypeOf(this.scheduler._appointments._renderingStrategy)._getSimpleAppointmentGeometry = function(coordinates) {
            var width = this._getAppointmentMaxWidth() - 20, height = coordinates.height, top = coordinates.top, left = coordinates.left;
            return {
                height: height,
                width: width,
                top: top,
                left: left
            }
        };

        // User ARCHIBUS panel control instead of third-party control default pop-up window.
        Object.getPrototypeOf(this.scheduler)._showAppointmentPopup = function(appointmentData, showButtons, processTimeZone) {
            control.currentEditAppointmentData = appointmentData;
            var wrId = appointmentData.wrId;
            var cfId = appointmentData.cfId;
            var dateAssigned = View.dataSources.get('tasksDataSource').formatValue("wrcf.date_assigned", appointmentData.startDate, false);
            var timeAssigned = View.dataSources.get('tasksDataSource').formatValue("wrcf.time_assigned", appointmentData.startDate, false);
            var res = new Ab.view.Restriction();
            res.addClause('wrcf.wr_id', wrId, '=');
            res.addClause('wrcf.cf_id', cfId, '=');
            res.addClause('wrcf.date_assigned', dateAssigned, '=');
            res.addClause('wrcf.time_assigned', timeAssigned, '=');
            
            var editOptions = getWrcfRecordEditOptions(appointmentData);

            View.openDialog('ab-bldgops-labor-scheduler-timeline-task-detail-popup.axvw',null,false,{
                wrId:wrId,
                //use this parameter to indicate which scheduler open the task detail pop-up.
                schedulerType:'signleCraftspersonScheudler',
                editOptions : editOptions,
                taskRestriction:res
            });
        }
    },

    /**
     * Get view start date.
     */
    getViewStartDate: function(currentDate, currentView) {
        var startViewDate = currentDate;
        if (currentView == 'week' || currentView == 'workWeek') {
            startViewDate = getFirstWeekDate(currentDate, this.scheduler.option('firstDayOfWeek'));
        }
        if (currentView == 'month') {
            startViewDate = new Date(startViewDate.getFullYear(),startViewDate.getMonth(),1,0,0,0);
        }
        
        startViewDate=new Date(startViewDate.getFullYear(),startViewDate.getMonth(),startViewDate.getDate(),0,0,0);

        return startViewDate;
    },

    /**
     * Get view end date.
     */
    getViewEndDate: function(startViewDate, currentView) {
        var endViewDate = null;
        if (currentView == 'day') {
            endViewDate = startViewDate.add(Date.DAY, 0);
        }

        if (currentView == 'workWeek') {
            endViewDate = startViewDate.add(Date.DAY, 4);
        }

        if (currentView == 'week') {
            endViewDate = startViewDate.add(Date.DAY, 6);
        }

        if (currentView == 'month') {
            var daysInMonth = this.getDaysInMonth(startViewDate.getFullYear(),startViewDate.getMonth());
            endViewDate = startViewDate.add(Date.DAY, daysInMonth-1);
        }
        
        endViewDate=new Date(endViewDate.getFullYear(),endViewDate.getMonth(),endViewDate.getDate(),0,0,0);

        return endViewDate;
    },
    
    /**
     * Get total days for specific year and month.
     * 
     * @param year Specific year.
     * @param month Specific month.
     */
    getDaysInMonth: function(year,month){
        month = parseInt(month)+1;
        var temp = new Date(year,month,0);
        return temp.getDate();
    },
    
    /**
     * Load schedule control with the loading progress bar.
     */
    loadSchedule: function(startDate, currentView){
        View.openProgressBar();
        this.loadScheduleControl.defer(50,this,[startDate, currentView]);
    },

    /**
     * Loads the schedule for specified list of resources. Triggers the afterRefresh event.
     */
    loadScheduleControl: function(startDate, currentView) {
        if (!startDate) {
            startDate = this.scheduler.getStartViewDate();
        }

        if (!currentView) {
            currentView = this.scheduler.option('currentView');
        }
        
        //Fixed issue that if current view is 'timelineWorkWeek', current day is Sunday or Saturday,then start day should changed to be scheduler control start date.
        if(currentView == 'workWeek'){
            if(startDate.getDay()  == 6||startDate.getDay() == 0){
                startDate = this.scheduler.getStartViewDate();
            }
        }
        
        // reset scheduler current date by start date.
        if(this.scheduler.option('currentDate').getTime() != startDate.getTime()){
            this.scheduler.option('currentDate', startDate);
        }

        var startViewDate = this.getViewStartDate(startDate, currentView);
        var endViewDate = this.getViewEndDate(startViewDate, currentView);

        this.viewStartDate = startViewDate;
        this.viewEndDate = endViewDate;

        // load schedule tasks.
        this.model.currentView = currentView;
        this.model.loadSchedule(this.restriction, startViewDate, endViewDate);
        this.scheduler.repaint();

        // render statistics message for single craftsperson schedule view.
        this.trigger('app:bldgops:laborScheduler:renderStatistics');
        
        jQuery('span:contains("Month")').text(getMessage('timelineMonth'));
        //Select span element that contains 'Week' text but not contains the 'Work Week' text.
        jQuery('span:contains("Week"):not(:contains("Work Week"))').text(getMessage('timelineWeek'));
        jQuery('span:contains("Work Week")').text(getMessage('timelineWorkWeek'));
        jQuery('span:contains("Day")').text(getMessage('timelineDay'));
        jQuery('.dx-scheduler-all-day-title').text(getMessage('timelineAllDay'));
        
        View.closeProgressBar();
    },

    /**
     * Get customized appointment tooltip template.
     */
    getTooltipTemplate: function(data) {
        var $tooltip = jQuery("<div>").addClass("dx-scheduler-appointment-tooltip"),
        startDate = data.startDate, endDate = data.endDate, description = data.description;
        var wrRecord = this.model.getWorkRequestRecord(data.wrId);
        var dueDate = wrRecord.getValue('wr.date_escalation_completion');
        var dateAssigned = wrRecord.getValue('wr.date_assigned');
        var siteId = wrRecord.getValue('wr.site_id');
        var blId = wrRecord.getValue('wr.bl_id');
        var flId = wrRecord.getValue('wr.fl_id');
        var rmId = wrRecord.getValue('wr.rm_id');
        var eqId = wrRecord.getValue('wr.eq_id');
        var eqStd = wrRecord.getValue('eq.eq_std');
        var pmpId = wrRecord.getValue('wr.pmp_id');
        var pmsId = wrRecord.getValue('wr.pms_id');
        var critiality = wrRecord.getValue('pms.criticality');
        var priority = wrRecord.getValue('wr.priority');
        var priorityLabel = wrRecord.getValue('wr.priority_label');
        var problemType = wrRecord.getValue('wr.prob_type');
        var trades = this.model.getWorkRequestTrades(data.wrId);
        
        var tableElement=jQuery("<table>").attr("width","100%");
        var row1Element = jQuery("<tr>").attr("width","100%");
        //work request code.
        jQuery("<td>").append(jQuery("<div>").text(data.wrId).addClass("dx-scheduler-appointment-tooltip-title")).appendTo(row1Element);
        //priority label.
        jQuery("<td>").append(jQuery("<div>").text(Ext.util.Format.ellipsis(priorityLabel, 25))).appendTo(row1Element);
        
        var rowProbTypeElement = jQuery("<tr>").attr("width","100%");
        if(problemType){
            jQuery("<td>").attr("colspan",'2').append(jQuery("<div>").text(Ext.util.Format.ellipsis(problemType, 50))).appendTo(rowProbTypeElement);
        }
        
        var row2Element = jQuery("<tr>").attr("width","100%");
        //start date and end date
        jQuery("<td>").append(jQuery("<div>").addClass("dx-scheduler-appointment-tooltip-date").text(this.formatTooltipDate(startDate, endDate, false))).appendTo(row2Element);
        //Due date.
        if(dueDate){
            jQuery("<td>").append(jQuery("<div>").text(getMessage('dueDate').replace('{0}',getIsoFormatDate(dueDate)))).appendTo(row2Element);
        }else{
            jQuery("<td>").appendTo(row2Element);
        }
        
        var row3Element = jQuery("<tr>").attr("width","100%");
        //description
        if(description){
            jQuery("<td>").attr("colspan",'2').append(jQuery("<div>").text(Ext.util.Format.ellipsis(description, 50))).appendTo(row3Element);
        }
        
        var row4Element =jQuery("<tr>").attr("width","100%");
        //location.
        var row4Col1 = jQuery("<td>");
        if(blId){
            row4Col1.append(jQuery("<div>").text(siteId+'-'+blId+'-'+flId+'-'+rmId))
        }
        row4Col1.appendTo(row4Element);
        //pmp and pms code.
        var row4Col2 = jQuery("<td>");
        if(pmpId){
            row4Col2.append(jQuery("<div>").text(pmpId+'-'+pmsId+'-'+critiality));
        }
        row4Col2.appendTo(row4Element);
        
        var row5Element = jQuery("<tr>").attr("width","100%");
        var row5Col1 = jQuery("<td>");
        if(eqId){
            row5Col1.append(jQuery("<div>").text(eqId));
        }
        row5Col1.appendTo(row5Element);
        var row5Col2 = jQuery("<td>");
        if(eqStd){
            row5Col2.append(jQuery("<div>").text(eqStd));
        }
        row5Col2.appendTo(row5Element);
        
        var row6Element = jQuery("<tr>").attr("width","100%");
        if(trades){
            jQuery("<td>").attr("colspan",'2').append(jQuery("<div>").text(trades)).appendTo(row6Element);
        }

        row1Element.appendTo(tableElement);
        rowProbTypeElement.appendTo(tableElement);
        row2Element.appendTo(tableElement);
        row3Element.appendTo(tableElement);
        row4Element.appendTo(tableElement);
        row5Element.appendTo(tableElement);
        row6Element.appendTo(tableElement);
        
        tableElement.appendTo($tooltip);
        
        var $buttons = jQuery("<div>").addClass("dx-scheduler-appointment-tooltip-buttons").appendTo($tooltip);
        if (this.scheduler._editing.allowDeleting) {
            this.getDeleteButton(data).appendTo($buttons)
        }

        this.getEditButton(data).appendTo($buttons);
        this.getUnscheduleButton(data).appendTo($buttons);
        return $tooltip;
    },

    /**
     * Format date for appointment tooltip popup window to display.
     */
    formatTooltipDate: function(startDate, endDate, isAllDay) {
        var formatType = "month" !== this.scheduler.option("currentView") && (startDate.getDate() == endDate.getDate()) ? "TIME" : "DATETIME", formattedString = "";
        if (isAllDay) {
            formatType = "DATE"
        }
        //Format date and time string by using the ARCHIBUS API.
        var dateFormatDs = View.dataSources.get('dateTimeFormateDs');
        
        var formattedStartString = "";
        var formattedEndString = "";
        
        if(formatType == "DATE"){
            formattedStartString = dateFormatDs.formatValue('wrcf.date_assigned',startDate,true);
            formattedEndString = dateFormatDs.formatValue('wrcf.date_assigned',endDate,true);
        }
        
        if(formatType == "DATETIME"){
            formattedStartString = dateFormatDs.formatValue('wrcf.date_assigned',startDate,true)+' '+dateFormatDs.formatValue('wrcf.time_assigned',startDate,true);
            formattedEndString = dateFormatDs.formatValue('wrcf.date_assigned',endDate,true)+' '+dateFormatDs.formatValue('wrcf.time_assigned',endDate,true);
        }
        
        if(formatType == "TIME"){
            formattedStartString = dateFormatDs.formatValue('wrcf.time_assigned',startDate,true);
            formattedEndString = dateFormatDs.formatValue('wrcf.time_assigned',endDate,true);
        }
        return formattedStartString+'-'+formattedEndString;
    },

    /**
     * Get delete button element for appointment tooltip popup window.
     */
    getDeleteButton: function(data) {
        var that = this;
        var ele = jQuery("<div>").dxButton({
            icon: "trash",
            onClick: function() {
                that.scheduler.deleteAppointment(data);
                that.scheduler.hideAppointmentTooltip();
            }
        });

        return ele;
    },

    /**
     * Get edit button element for appointment tooltip popup window.
     */
    getEditButton: function(data) {
        var that = this, allowUpdating = that.scheduler._editing.allowUpdating;
        var editButton = jQuery("<div>").dxButton({
            icon: allowUpdating ? "edit" : "",
            text: getMessage('edit'),
            onClick: function() {
                that.scheduler.showAppointmentPopup(data, false);
                that.scheduler.hideAppointmentTooltip();
            }
        });

        return editButton;
    },

    /**
     * Get unscheduled button element for appointment tooltip popup window.
     */
    getUnscheduleButton: function(data) {
        var that = this, allowUpdating = that.scheduler._editing.allowUpdating;
        var editButton = jQuery("<div>").dxButton({
            icon: "",
            text: getMessage('Unschedule'),
            onClick: function() {
                data.actionType = 'unscheduled';
                that.scheduler.deleteAppointment(data);
                that.scheduler.hideAppointmentTooltip();
            }
        });

        return editButton;
    },
    
    /**
     * get date by appointment item time text. like '12:00 AM' to Date Object with 24-hours format.
     */
    getDateByItemTimeText: function(timeText){
        var currentDate = new Date();
        var hours = parseInt(timeText.split(':')[0]);
        if(timeText.indexOf('AM') >0 && hours == 12){
            hours = 0;
        }
        if(timeText.indexOf('PM') >0){
            if(hours != 12){
                hours = hours + 12;
            }
        }
        
        return new Date(currentDate.getFullYear(),currentDate.getMonth(),currentDate.getDate(),hours,0,0);
        
    }
});

/**
 * Get first week date.
 */
function getFirstWeekDate(date, firstDayOfWeek) {
    var delta = (date.getDay() - firstDayOfWeek + 7) % 7;
    var result = new Date(date);
    result.setDate(date.getDate() - delta);
    return result
}

/**
 * Helper functions.
 */

function isFriday(date) {
    var day = date.getDay();
    return day === 5;
}