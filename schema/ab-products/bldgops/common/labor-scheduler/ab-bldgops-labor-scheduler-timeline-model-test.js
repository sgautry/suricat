/**
 * Unit test functions for the Labor Scheduler timeline model.
 */
var LaborSchedulerTimelineTest = {

    /**
     * Tests CraftspersonSchedule.getAvailableWholeHours.
     */
    testAvailableWholeHours: function() {
        var today = new Date();
        console.log('Test date: ' + today);

        var controller = View.controllers.get('laborSchedulingBoard-workSchedule');
        var schedule = controller.scheduler.model.craftspersonSchedules[1];
        console.log('Craftsperson: ' + schedule.cfId);

        var availableHours = schedule.getAvailableWholeHours(today, []);
        console.log('Available Hours: ' + availableHours);

        if (availableHours != 10) {
            throw 'Incorrect available hours';
        }

        console.log('Test getAvailableHours: success!');
    },

    /**
     * Tests CraftspersonSchedule.getAvailableHours.
     */
    testAvailableHours: function() {
        var today = new Date();
        console.log('Test date: ' + today);

        var controller = View.controllers.get('laborSchedulingBoard-workSchedule');
        var schedule = controller.scheduler.model.craftspersonSchedules[1];
        console.log('Craftsperson: ' + schedule.cfId);

        var availableHours = schedule.getAvailableHours(today, []);
        console.log('Available Hours: ' + availableHours);

        if (availableHours != 10) {
            throw 'Incorrect available hours and minutes';
        }

        console.log('Test getAvailableHoursAndMinutes: success!');
    }
};