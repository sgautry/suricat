/**
 * The example controller.
 */
var LaborSchedulingBoardWorkRequestsController = View.createController('laborSchedulingBoard-workRequests', {

    /**
     * The array of loaded work requests.
     */
    workRequests: null,
    
    /**
     * Selected work requests when click the multiple selected checkbox.
     */
    selectedWorkRequests: [],

    afterViewLoad: function() {
        this.workRequests = [];

        this.on('app:bldgops:laborScheduler:showWorkRequests', this.showWorkRequests);
        
        //Add checkbox and action bar at the top of the work request list panel.
        this.buildWorkRequestListHeaderElement();
    },

    /**
     * Queries and displays work requests for specified restriction.
     * @param restriction
     */
    showWorkRequests: function(restriction) {
        this.workRequestsPanel.parentElement.innerHTML = '';
        this.workRequestsTotalsPanel.parentElement.innerHTML = '';

        // query work requests
        var dataSource = this.workRequestsDataSource;
        var workRequestRecords = dataSource.getRecords(restriction);
        this.workRequests = _.map(workRequestRecords, function(record) {
            return {
                wr_id: record.getValue('wr.wr_id'),
                description: Ext.util.Format.ellipsis(record.getValue('wr.description'), 90),
                wholeDescription:record.getValue('wr.description'),
                date_escalation_completion: dataSource.formatValue('wr.date_escalation_completion', record.getValue('wr.date_escalation_completion')),
                date_assigned: dataSource.formatValue('wr.date_assigned', record.getValue('wr.date_assigned')),
                priority_label: record.getValue('wr.priority_label'),
                site_id: dataSource.formatLookupValue('wr.site_id', record.getValue('wr.site_id'), record.values),
                bl_id: dataSource.formatLookupValue('wr.bl_id', record.getValue('wr.bl_id'), record.values),
                fl_id: record.getValue('wr.fl_id'),
                rm_id: record.getValue('wr.rm_id'),
                eq_id: record.getValue('wr.eq_id'),
                eq_std: record.getValue('eq.eq_std'),
                pmp_id: record.getValue('wr.pmp_id'),
                pms_id: record.getValue('wr.pms_id'),
                criticality: record.getValue('pms.criticality'),
                trades: record.getValue('wr.trades'),
                prob_type: record.getValue('wr.prob_type')
            };
        });

        var groupedWorkRequests = _.groupBy(this.workRequests, 'prob_type');
        var problemTypes = _.keys(groupedWorkRequests);
        problemTypes = _.sortBy(problemTypes, function(name) {
            return name;
        });
        var workRequestsByProblemType = [];
        _.each(problemTypes, function(problemType) {
            workRequestsByProblemType.push({
                problemType: problemType,
                workRequests: groupedWorkRequests[problemType]
            });
        });

        // query trades per work request
        var workRequestTrades = this.getWorkRequestTradesByWorkRequests(this.workRequests);
        _.each(this.workRequests, function(workRequest) {
            workRequest.trades = _.reduce(workRequestTrades, function(trades, workRequestTrade) {
                var wr_id = workRequestTrade.getValue('wrtr.wr_id');
                if (wr_id === workRequest.wr_id) {
                    if (trades !== '') {
                        trades = trades + ', ';
                    }
                    trades = trades + workRequestTrade.getValue('wrtr.tr_id');
                }
                return trades;
            }, '');
        });

        // calculate totals
        var totalWorkRequests = workRequestRecords.length;
        var totalWorkRequestHours = _.reduce(workRequestTrades, function(hours, workRequestTrade) {
            return hours + Number(workRequestTrade.getValue('wrtr.hours_est'));
        }, 0);

        // display work requests
        var template = View.templates.get('workRequestsTemplate');
        template.render({
            workRequestsByProblemType: workRequestsByProblemType
        }, this.workRequestsPanel.parentElement);

        // display work request totals
        template = View.templates.get('workRequestsTotalsTemplate');
        template.render({
            totalWorkRequests: totalWorkRequests,
            totalWorkRequestHours: totalWorkRequestHours
        }, this.workRequestsTotalsPanel.parentElement);

        // update vertical scroll bar
        this.workRequestsPanel.updateHeight();
        
        //update action bar at the top of the work requests list panel.
        $('checkAllWr').checked=false;
        this.selectedWorkRequests.length = 0;
        updateActionBar();
    },
    
    /**
     * Get work request trades by the restrict work requests array.
     */
    getWorkRequestTradesByWorkRequests: function(workRequests){
        var workRequestTrades = this.workRequestTradesDataSource.getRecords();
        var matchWorkRequestTrades = _.filter(workRequestTrades,function(workRequestTrade){
            var workRequestId = workRequestTrade.getValue('wrtr.wr_id');
            var matchWorkRequest = _.find(workRequests,function(workRequest){
                return workRequest.wr_id == workRequestId;
            });
            
            if(matchWorkRequest){
                return true;
            }else{
                return false;
            }
        });
        
        return matchWorkRequestTrades;
    },
    
    /**
     * build work request list header checbox and action bar element.
     */
    buildWorkRequestListHeaderElement: function(){
        var me=this;
        //Add a check box to selected all work requests.
        jQuery('#workRequestsActionBarPanel').append(jQuery("<input>").attr('type','checkbox').css('margin-left','15px').attr('id','checkAllWr').bind('click',this.onCheckAllWorkRequest));
        
        //add action bar to work request list panel.
        var actionBar=new Ab.view.Actionbar('workRequestsActionBarPanel',this.workRequestsActionBarPanel);
        actionBar.addAction({
            id: 'AssignWorkRequest',
            text: getMessage('assignText'),
            tooltip: getMessage('assignButtonTooltip'),
            listener: this.assignSelectedWorkRequests.createDelegate(this)
        });
        actionBar.show();
    },
    
    /**
     * Assign selected work request.
     */
    assignSelectedWorkRequests: function(scope){
        View.panels.get('assignDetailPanel').showInWindow({
            width: 400,
            height: 300,
            modal: true,
            x: 600,
            y: 300,
            selectedWorkRequests: this.selectedWorkRequests,
            title: getMessage('assignCraftsperson')
        });
        View.panels.get('assignDetailPanel').show(true);
        View.panels.get('assignDetailPanel').clear();
    },
    
    /**
     * Check all work request.
     */
    onCheckAllWorkRequest: function(checked){
        var checked = $('checkAllWr').checked;
        if(checked){
            jQuery('.problemTypeCheckBox').each(function(){
                onCheckedProblemTypeCheckBox(this,checked);
            });
        }else{
            LaborSchedulingBoardWorkRequestsController.selectedWorkRequests.length = 0;
            //unchecked all problem type checkbox.
            jQuery('.problemTypeCheckBox').each(function(){
                this.checked =false;
            });
            jQuery('.workRequestCheckbox').each(function(){
                this.checked =false;
            });
        }
        //update action bar selected count and assign action button.
        updateActionBar();
    },
});

/**
 * Expands or collapses work requests for specified problem type.
 * @param problemTypeBox
 */
function toggleWorkRequestGroup(problemTypeBox) {
    problemTypeBox = jQuery(problemTypeBox);
    var next = problemTypeBox.next();
    if (next.is(':visible')) {
        next.hide();
        problemTypeBox.addClass('collapsed');
        problemTypeBox.removeClass('expanded');
    } else {
        next.show();
        problemTypeBox.removeClass('collapsed');
        problemTypeBox.addClass('expanded');
    }

    View.panels.get('workRequestsPanel').updateHeight();
}

/**
 * Select problem type level.
 * @param problemCheckBox
 * @returns
 */
function onSelectedProblemType(problemCheckBox){
    var checked = problemCheckBox.checked;

    onCheckedProblemTypeCheckBox(problemCheckBox,checked);
    
    //update action bar selected count and assign action button.
    updateActionBar();
}

/**
 * Select 
 * @param problemCheckBox
 * @param checked
 * @returns
 */
function onCheckedProblemTypeCheckBox(problemCheckBox, checked){
    problemCheckBox.checked =checked;
    var selectedWorkRequests = _.filter(LaborSchedulingBoardWorkRequestsController.workRequests,function(workRequest){
        if(workRequest.prob_type == problemCheckBox.value){
            return true;
        }else{
            return false;
        }
    });
    
    _.each(selectedWorkRequests,function(workRequest){
        $('checkBox_'+workRequest.wr_id).checked=checked;
        //update selected work request, add or remove from cache array.
        updateSelectedWorkRequest(workRequest,checked);
    });
}

/**
 * Select specify work rquest to cache it in array and calculate the multiple selected count.
 * @param wrId
 * @returns
 */
function onSelectedWorkRequest(wrId){
    var checked = $('checkBox_'+wrId).checked;
    var workRequest = _.find(LaborSchedulingBoardWorkRequestsController.workRequests,function(workRequest){
        if(workRequest.wr_id == wrId){
            return this;
        }
    });
    //update selected work request, add or remove from cache array.
    updateSelectedWorkRequest(workRequest,checked);
    //update action bar selected count and assign action button.
    updateActionBar();
}

/**
 * Update selected work, add or remove selected work request from cache array.
 * @param workRequest
 * @param checked
 * @returns
 */
function updateSelectedWorkRequest(workRequest,checked){
    if(checked){
        //If work request has been already added. then should exclude this work request.
        if(!this.workReqeustAlreadyExists(LaborSchedulingBoardWorkRequestsController.selectedWorkRequests,workRequest)){
            LaborSchedulingBoardWorkRequestsController.selectedWorkRequests.push(workRequest);
        }
    }else{
        LaborSchedulingBoardWorkRequestsController.selectedWorkRequests=_.filter(LaborSchedulingBoardWorkRequestsController.selectedWorkRequests,function(selectedWorkRequest){
            if(selectedWorkRequest.wr_id!=workRequest.wr_id){
                return true;
            }else{
                return false;
            }
        });
    }
}

/**
 * Check whether work request already exists in selected work reqeusts array.
 * @param selectedWorkRequests
 * @param workReqeust
 * @returns
 */
function workReqeustAlreadyExists(selectedWorkRequests,workReqeust){
    var exists = false;
    _.each(selectedWorkRequests,function(selectedWorkReqeust){
        if(selectedWorkReqeust.wr_id == workReqeust.wr_id){
            exists =true;
        }
    });
    
    return exists;
}

/**
 * Update action bar selected count or 
 * @returns
 */
function updateActionBar(){
    View.panels.get('workRequestsActionBarPanel').actionbar.updateSelected(LaborSchedulingBoardWorkRequestsController.selectedWorkRequests.length);
    if(LaborSchedulingBoardWorkRequestsController.selectedWorkRequests.length > 0){
        View.panels.get('workRequestsActionBarPanel').actionbar.getAction('AssignWorkRequest').show(true);
    }else{
        View.panels.get('workRequestsActionBarPanel').actionbar.getAction('AssignWorkRequest').show(false);
    }
}


/**
 * Opens the Edit Work Request dialog.
 * @param wrId
 */
function editWorkRequest(wrId) {
    LaborSchedulingBoardWorkRequestsController.trigger('app:bldgops:laborScheduler:editWorkRequestDetails', wrId);
}

/**
 * Open the work request details dialog.
 * @param {wrId} Work request code.
 * @returns
 */
function showWorkRequestDetails(wrId){
    View.openDialog("ab-bldgops-labor-scheduler-wr-info.axvw",null,false,{
        wrId:wrId
    });
}

/**
 * Starts drag operation for specified work request.
 * @param wrId
 */
function startWorkRequestDrag(event, wrId, description, dueDate) {
    var workRequest = _.find(LaborSchedulingBoardWorkRequestsController.workRequests, function(wr) {
        return wr.wr_id === '' + wrId;
    });

    if (workRequest) {
        var dragData = JSON.stringify(workRequest);
        event.dataTransfer.setData('text', dragData);

        if (event.dataTransfer.setDragImage) {
            var dragEl = jQuery('<div id="workRequestDragImage_' + wrId + '" class="workRequestDragImage">Work request: ' + wrId + '</div>')[0];
            event.currentTarget.appendChild(dragEl);
            event.dataTransfer.setDragImage(dragEl, 0, 0);
        }
    }
}

/**
 * Ends drag operation for specified work request.
 * @param wrId
 */
function endWorkRequestDrag(event, wrId) {
    jQuery('#workRequestDragImage_' + wrId).remove();
}

/**
 * Stop the event bubble.
 * 1. Used to stop toggle the problem type box when select the checkbox in problem type box.
 * @param e
 * @returns
 */
function stopParentEvent(e){
    if ( e && e.stopPropagation ){
        e.stopPropagation();
    }else{
        //For IE browser.
        window.event.cancelBubble = true;
        return false;
    } 
}
