/**
 * The example controller.
 */
var laborScheduler=View.createController('laborScheduler', {
    // tab control cache for mutiple craftsperson schedule tab and single craftsperson schedule tab.
    tabCtrls: {},
    // store the selected single craftsperson code.
    selectedSingleCraftsperson: '',

    moreAction: null,
    
    /**
     * Selected scheduler view date
     */
    selectedScheduleDate: new Date(),
    /**
     * After view loads.
     */
    afterViewLoad: function() {
        this.on('app:bldgops:laborScheduler:selectCraftsperson', this.onSelectCraftsperson);
        // set css style to more options filter panel.
        this.setFilterFieldCSS();
    },
    /**
     * Initialize the timeline control.
     */
    afterInitialDataFetch: function() {
        //APP-1501:When the Labor Scheduler window opens from Ops Console view, the filter should automatically uncheck the Unscheduled checkbox.
        if(valueExistsNotEmpty(View.parameters)&&valueExistsNotEmpty(View.parameters['selectedWrIds'])){
            $('unscheduled').checked = false;
        }
        var restriction = laborSchedulerWorkRequestFilterRestrictionCrtl.getFilterRestriction();
        this.trigger('app:bldgops:laborScheduler:showWorkRequests', restriction);
    },

    /**
     * Filters the list of work requests.
     */
    workRequestsFilter_onFilterWorkRequests: function() {
        var restriction = laborSchedulerWorkRequestFilterRestrictionCrtl.getFilterRestriction();

        if (!this.workRequestMoreOptionsFilter.collapsed) {
            this.workRequestMoreOptionsFilter.toggleCollapsed();
            this.adjustMoreOptionPanelBorderStyle();
            this.moreAction.setTitle(getMessage('filterMore'));
        }
        
        laborScheduleWorkRequestRecentSearchCtrl.addRecentSearch();

        // var restriction = this.workRequestsFilter.getFieldRestriction();
        this.trigger('app:bldgops:laborScheduler:showWorkRequests', restriction);
    },

    /**
     * Clears the filter applied to the list of work requests.
     */
    workRequestsFilter_onClearWorkRequests: function() {
        this.clearWorkRequestFilter();
        $('unscheduled').checked = true;
        // clear work request more options filter fields.
        this.clearWorkRequestMoreOptionsFilter();
        var restriction = laborSchedulerWorkRequestFilterRestrictionCrtl.getFilterRestriction();
        this.trigger('app:bldgops:laborScheduler:showWorkRequests', restriction);
    },

    /**
     * Tap on 'More' action to collapsed or expand more options filter form panel.
     */
    workRequestsFilter_onMoreOptions: function(panel, action) {
        
        this.workRequestMoreOptionsFilter.toggleCollapsed();
        this.adjustMoreOptionPanelBorderStyle();
        action.setTitle(this.workRequestMoreOptionsFilter.collapsed ? getMessage('filterMore') : getMessage('filterLess'));
        this.moreAction = action;
        
    },
    
    /**
     * APP-1535:Hide more option panel border if panel if collapsed.
     */
    adjustMoreOptionPanelBorderStyle: function(){
        if(this.workRequestMoreOptionsFilter.collapsed){
            jQuery('#workRequestMoreOptionsFilter').removeClass("morePanelBorder");
        }else{
            jQuery('#workRequestMoreOptionsFilter').addClass("morePanelBorder");
        }
    },

    /**
     * Set css of the filter field, to set placeholder for some fields.
     */
    setFilterFieldCSS: function() {
        var fields = [ 'wr.date_requested.from', 'wr.date_requested.to', 'wr.date_assigned.from', 'wr.date_assigned.to', 'wr.wr_id', 'wr.wo_id', 'wrpt.part_id', 'wr.priority_label' ];
        for (var i = 0; i < fields.length; i++) {
            var el = Ext.get('workRequestMoreOptionsFilter_' + fields[i]);
            el.addClass('shortField');
            el.dom.placeholder = this.workRequestMoreOptionsFilter.fields.get(fields[i]).fieldDef.title;
        }
    },

    /**
     * Displays specified craftsperson's schedule.
     * @param cfId
     * @param currentDate Selected scheduler view start date from multiple craftsperson scheduler view.
     */
    onSelectCraftsperson: function(cfId ,currentDate) {
        this.selectedSingleCraftsperson = cfId;
        this.selectedScheduleDate = currentDate;
        var craftspersonScheduleTabName = "craftspersonScheduleTab";
        this.workScheduleTabs.selectTab(craftspersonScheduleTabName, null);

        var currentTab = this.workScheduleTabs.findTab(craftspersonScheduleTabName);

        if (currentTab.isContentLoaded) {
            var controller = this.tabCtrls[craftspersonScheduleTabName];
            controller.afterCraftspersonTabSelected(this.selectedSingleCraftsperson,this.selectedScheduleDate);
        }
    },

    /**
     * Clear work request easy filter.
     */
    clearWorkRequestFilter: function() {
        this.workRequestsFilter.clear();
        $('escalated').checked = false;
        $('unscheduled').checked = false;
    },

    /**
     * Clear all big filter values and set default values
     */
    clearWorkRequestMoreOptionsFilter: function() {
        this.workRequestMoreOptionsFilter.clear();
        Ext.get('worktype').dom.selectedIndex = 0;
        Ext.get('wr.returned').dom.checked = false;
        Ext.get('operator').dom.selectedIndex = 0;
        Ext.get('wr.cost_est_total').dom.value = '';
        var priorityOpts = document.getElementsByName('wr.priority');
        for (var i = 0; i < priorityOpts.length; i++) {
            priorityOpts[i].checked = false;
        }
        var statusOpts = document.getElementsByName('wr.status');
        for (var i = 0; i < statusOpts.length; i++) {
            statusOpts[i].checked = false;
        }

        var statusOpts = document.getElementsByName('wrpt.status');
        for (var i = 0; i < statusOpts.length; i++) {
            statusOpts[i].checked = false;
        }

        showCostEst('');
    }

});

/**
 * Depending upon operator, show and set the focus to the cost_est_total field
 * @param operator operator ie ("IS NULL", ">", "<", etc.)
 */
function showCostEst(operator) {
    if (operator == '' || operator == 'IS NULL' || operator == 'IS NOT NULL') {
        Ext.get('wr.cost_est_total').dom.disabled = true;
    } else {
        Ext.get('wr.cost_est_total').dom.disabled = false;
        setFocusOnField('workRequestMoreOptionsFilter', 'wr.cost_est_total');
    }
}

/**
 * Selected event listener after select room code.
 * @param fieldName
 * @param selectedValue
 * @param previousValue
 * @returns
 */
function afterSelectRmId(fieldName, selectedValue, previousValue) {
    if (fieldName == 'wr.bl_id') {
        View.panels.get('workRequestsFilter').setFieldValue('wr.bl_id', selectedValue);
    }
    if (fieldName == 'wr.fl_id') {
        View.panels.get('workRequestMoreOptionsFilter').setFieldValue('wr.fl_id', selectedValue);
    }
}

/**
 * Set focus to field
 * @param panelId id of the panel
 * @param id id of the field to show
 */
function setFocusOnField(panelId, id) {
    var el = View.panels.get(panelId).getFieldElement(id);
    if (el) {
        el.focus();
    }
}