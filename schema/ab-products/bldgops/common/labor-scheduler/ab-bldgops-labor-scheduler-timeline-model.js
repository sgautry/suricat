/**
 * The Labor Board control implements https://confluence.archibus.zone/display/PLAT/HTML+Scheduling+Control.
 */
WorkScheduleModel = Base.extend({

    /**
     * The configuration object.
     */
    config: null,

    /**
     * The array of craftspersons displayed in the scheduler.
     */
    craftspersons: null,

    /**
     * The map containing the same craftspersons, keyed by ID, for fast access.
     */
    craftspersonsById: null,

    /**
     * The map of all craftsperson schedules, keyed by schedule ID.
     */
    craftspersonSchedules: null,

    /**
     * The array of all craftsperson schedule variances.
     */
    craftspersonScheduleVariances: null,

    /**
     * The array of scheduled tasks displayed for the date range in the scheduler, for all craftspersons.
     */
    scheduledTasks: null,

    /**
     * The array of assigned unscheduled tasks for the date range displayed in the scheduler, for all craftspersons.
     */
    assignedUnscheduledTasks: null,

    /**
     * The start date of the date range of the model.
     */
    startDate: null,

    /**
     * The end date of the date range of the model.
     */
    endDate: null,

    /**
     * Constructor.
     * @param {config} The control configuration.
     */
    constructor: function(config) {
        this.config = config;
        this.craftspersons = [];
        this.scheduledTasks = [];
        this.assignedUnscheduledTasks = [];
        this.craftspersonSchedules = {};
        this.craftspersonScheduleVariances = [];
    },

    /**
     * Loads the schedule for specified list of resources. Triggers the afterRefresh event.
     * @param {restriction} Ab.data.Restriction to restrict the craftspersons data source, or null.
     * @param dateStart The first date of the date range.
     * @param dateEnd The last date of the date range.
     */
    loadSchedule: function(restriction, startDate, endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.craftspersonsById = {};
        this.craftspersons.length = 0;
        
        var currentDate = new Date();
        var autoNumber = 0;
        var model = this;
        
        // load holidays for all sites (country and region), reload when site is selected
        // the user should be restricted to a country and region, so local holidays are loaded
        holidays=loadHolidays(null,this.startDate.getFullYear());
        
        var records = View.dataSources.get('craftspersonsDataSource').getRecords(restriction);
        _.each(records, function(record) {
            var color = '#337AB7';
            var cfId = record.getValue('cf.cf_id');
            var name = record.getValue('cf.name');
            var trId = record.getValue('cf.tr_id');

            var craftsperson = {
                autoNumber: autoNumber++,
                id: cfId,
                text: name,
                trade: trId,
                type: 'craftsperson',
                color: color,
                totals: {
                    availableHours: 0,
                    scheduledHours: 0,
                    //sum of unscheduled hours with prorate unschduled hours in the visiable date range.
                    assignedUnscheduledHoursWithProrateHours: 0,
                    //sum of unscheduled hours without prorate unscheduled hours.
                    assignedUnscheduledHours: 0,
                    proratedUnscheduledHours: 0,
                    remainingHours: 0,
                    percentAllocated: 0,
                    //sum of Scheduled Hours, Assigned Unscheduled Hours, and Prorated Unscheduled Hours
                    assignedHours: 0
                    
                },
                statistics: new WorkScheduleCraftspersonStatistics(startDate, endDate)
            }
            model.craftspersons.push(craftsperson);
            model.craftspersonsById[cfId] = craftsperson;
        });

        this.scheduledTasks.length = 0;
        this.assignedUnscheduledTasks.length = 0;
        
        //get new start date to expand the start date of the display date range.
        var newStartDate = this.getNewStartDate(startDate);

        this._loadTasks(newStartDate, endDate.add(Date.MONTH, 6), true, false, this.scheduledTasks);
        this._loadTasks(newStartDate, endDate.add(Date.MONTH, 6), false, true, this.assignedUnscheduledTasks);
        
        this._loadCraftspersonSchedules(startDate, endDate);
        this._createCraftspersonTasks(startDate, endDate);

        // for each craftsperson and day within the visible range:
        // - find the applicable schedule
        // - calculate the number of available hours
        // - adjust hours per schedule details
        // - calculate totals

        // update craftsperson totals after load all time blocks.
        _.each(model.craftspersons, function(craftsperson) {
            if (craftsperson.type === 'craftsperson') {
                model.updateCraftspersonTotals(craftsperson, startDate, endDate);
            }
        });
    },

    /**
     * Creates a new scheduled task in the database.
     * @param task
     */
    createScheduledTask: function(task) {
        var updateTaskDataSource = View.dataSources.get('updateTaskDataSource');
        var record = new Ab.data.Record();
        record.isNew = true;
        record.setValue('wrcf.cf_id', task.cfId);
        record.setValue('wrcf.wr_id', task.wrId);
        record.setValue('wrcf.date_assigned', task.startDate);
        record.setValue('wrcf.time_assigned', task.startDate);
        record.setValue('wrcf.hours_est', hoursBetween(task.startDate, task.endDate));
        updateTaskDataSource.saveRecord(record);
        this.calculateSummaryAfterWorkRequestCraftspersonChanged(task,true);
    },
    
    /**
     * Call workflow rule to calculate summary after work request craftsperson deleted.
     * @param wrId
     * @param cfId
     * @param dateAssigned
     * @param timeAssigned
     *
     */
    calculateSummaryAfterWorkRequestCraftspersonDeleted: function(wrId,cfId,dateAssigned,timeAssigned){
        var record = this.formatJsonObjectByTask(wrId,cfId,dateAssigned,timeAssigned);
        Workflow.callMethod('AbBldgOpsOnDemandWork-WorkRequestService-afterDeletedWorkRequestCraftsperson', record);
    },
    
    /**
     * Call workflow rule to re-calculate the work request summary values after work request craftsperson saved. 
     */
    calculateSummaryAfterWorkRequestCraftspersonChanged: function(task,isNew){
        var fieldValues = this.formatJsonObjectByTask(task.wrId,task.cfId,task.startDate,task.startDate);
        Workflow.callMethod('AbBldgOpsOnDemandWork-WorkRequestService-afterLaborSchedulerTaskSaved', fieldValues,isNew);
    },
    
    /**
     * Format work request craftsperon primary keys fields as a json object as parameter of workflow rule.
     * @param wrId
     * @param cfId
     * @param dateAssigned
     * @param timeAssigned
     */
    formatJsonObjectByTask: function(wrId,cfId,dateAssigned,timeAssigned){
        var record = new Ab.data.Record();
            record.setValue('wrcf.cf_id', cfId);
            record.setValue('wrcf.wr_id', wrId);
            record.setValue('wrcf.date_assigned', dateAssigned);
            record.setValue('wrcf.time_assigned', timeAssigned);
        return View.dataSources.get('updateTaskDataSource').processOutboundRecord(record).values;
    },

    /**
     * Creates a new unscheduled task in the database.
     * @param task
     */
    createUnscheduledTask: function(task) {
        var currentDate=new Date();
        var updateTaskDataSource = View.dataSources.get('updateTaskDataSource');
        var record = new Ab.data.Record();
        record.isNew = true;
        record.setValue('wrcf.cf_id', task.cfId);
        record.setValue('wrcf.wr_id', task.wrId);
        record.setValue('wrcf.date_assigned', task.startDate);
        record.setValue('wrcf.time_assigned', task.startDate);
        record.setValue('wrcf.hours_est', task.estimatedHours);
        record.setValue('wrcf.is_scheduled', 0);
        updateTaskDataSource.saveRecord(record);

        //assignment with empty due date will be excluded from calculation and display on the Unscheduled Task Bar.
        var dueDate=this.getDueDateByWrId(task.wrId);
        if(dueDate){
            this._addUnscheduledTask(task);
        }
        
        //re-calculate summary by using workflow rule.
        this.calculateSummaryAfterWorkRequestCraftspersonChanged(task,true);
    },

    /**
     * Update scheduled task to unscheduled task.
     */
    unscheduleTask: function(task) {
        var updateTaskDataSource = View.dataSources.get('updateTaskDataSource');
        var record = new Ab.data.Record();
        record.isNew = false;
        record.setValue('wrcf.cf_id', task.cfId);
        record.setValue('wrcf.wr_id', task.wrId);
        var currentDate=new Date();
        currentDate.setMilliseconds(0);
        record.setValue('wrcf.date_assigned', currentDate);
        record.setValue('wrcf.time_assigned', currentDate);
        record.setValue('wrcf.is_scheduled', '0');
        record.setOldValue('wrcf.cf_id', task.cfId);
        record.setOldValue('wrcf.wr_id', task.wrId);
        record.setOldValue('wrcf.date_assigned', task.startDate);
        record.setOldValue('wrcf.time_assigned', task.startDate);
        updateTaskDataSource.saveRecord(record);
        
        //assignment with empty due date will be excluded from calculation and display on the Unscheduled Task Bar.
        var dueDate=this.getDueDateByWrId(task.wrId);
        if(dueDate){
            var unscheduledTask = {
                cfId: task.cfId,
                wrId: task.wrId,
                startDate: currentDate,
                endDate: currentDate,
                estimatedHours: hoursBetween(task.startDate, task.endDate),
                dueDate: dueDate
            };
            
            this._addUnscheduledTask(unscheduledTask);
        }
    },

    /**
     * update tasks after unscheduled.
     */
    _addUnscheduledTask: function(task) {
        // add unschedule task to global assigned Unscheduled Tasks array.
        this.assignedUnscheduledTasks.push(task);
        var craftsperson = this.craftspersonsById[task.cfId];
        if(craftsperson){
            craftsperson.statistics.addUnscheduledTask(task);
            this.updateCraftspersonTotals(craftsperson, this.startDate, this.endDate);
        }
    },

    /**
     * Updates an existing scheduled task in the database.
     * @param task
     */
    updateTask: function(task, oldTask) {
        var updateTaskDataSource = View.dataSources.get('updateTaskDataSource');
        var formattedDateAssigned=updateTaskDataSource.formatValue('wrcf.date_assigned',oldTask.startDate,false);
        var formattedTimeAssigned=updateTaskDataSource.formatValue('wrcf.time_assigned',oldTask.startDate,false);
        var taskRes=new Ab.view.Restriction();
            taskRes.addClause('wrcf.cf_id',oldTask.cfId,'=');
            taskRes.addClause('wrcf.wr_id', oldTask.wrId,'=');
            taskRes.addClause('wrcf.date_assigned', formattedDateAssigned, '=');
            taskRes.addClause('wrcf.time_assigned', formattedTimeAssigned, '=');
        var taskRecords=updateTaskDataSource.getRecords(taskRes);
        
        if(taskRecords.length > 0){
            var taskRecord=taskRecords[0];
            var isScheduled = taskRecord.getValue('wrcf.is_scheduled');
            taskRecord.isNew=false;
            taskRecord.setValue('wrcf.cf_id', task.cfId);
            taskRecord.setValue('wrcf.date_assigned', task.startDate);
            taskRecord.setValue('wrcf.time_assigned', task.startDate);
            taskRecord.setValue('wrcf.hours_est', task.estimatedHours);
            //APP-1104: avoid undefined value for work type and comments when resize the time block by drag and drop.
            if(valueExistsNotEmpty(task.workType)){
                taskRecord.setValue('wrcf.work_type', task.workType);
            }
            if(task.comments != undefined){
                taskRecord.setValue('wrcf.comments',task.comments);
            }
            updateTaskDataSource.saveRecord(taskRecord);
            
            if(isScheduled == '0'){
                var dueDate=this.getDueDateByWrId(task.wrId);
                if(dueDate){
                    this._removeUnscheduledTask(oldTask.wrId,oldTask.cfId,oldTask.startDate,oldTask.startDate,dueDate);
                    this._addUnscheduledTask(task);
                }
            }
            this.calculateSummaryAfterWorkRequestCraftspersonChanged(task,false);
        }
    },

    /**
     * Delete an existing task in database.
     * @param task 
     */
    deleteTask: function(task) {
        var updateTaskDataSource = View.dataSources.get('updateTaskDataSource');
        var record = new Ab.data.Record();
        record.isNew = false;
        record.setValue('wrcf.cf_id', task.cfId);
        record.setValue('wrcf.wr_id', task.wrId);
        record.setValue('wrcf.date_assigned', task.startDate);
        record.setValue('wrcf.time_assigned', task.startDate);
        record.setOldValue('wrcf.cf_id', task.cfId);
        record.setOldValue('wrcf.wr_id', task.wrId);
        record.setOldValue('wrcf.date_assigned', task.startDate);
        record.setOldValue('wrcf.time_assigned', task.startDate);
        updateTaskDataSource.deleteRecord(record);
        
        this.calculateSummaryAfterWorkRequestCraftspersonDeleted(task.wrId,task.cfId,task.startDate,task.startDate);
    },
    
    /**
     * Delete an existing task in database.
     * @param task 
     */
    deleteTaskByPrimaryKey: function(wrId,cfId,dateAssigned,timeAssigned) {
        var updateTaskDataSource = View.dataSources.get('updateTaskDataSource');
        var record = new Ab.data.Record();
        record.isNew = false;
        record.setValue('wrcf.cf_id', cfId);
        record.setValue('wrcf.wr_id', wrId);
        record.setValue('wrcf.date_assigned', dateAssigned);
        record.setValue('wrcf.time_assigned', timeAssigned);
        updateTaskDataSource.deleteRecord(record);
        
        this.calculateSummaryAfterWorkRequestCraftspersonDeleted(wrId,cfId,dateAssigned,timeAssigned);
    },

    /**
     * Loads tasks within specified date range.
     * @param dateStart The first date of the date range.
     * @param dateEnd The last date of the date range.
     * @param includeScheduledTasks True to load scheduled tasks.
     * @param includeAssignedUnscheduledTasks True to load assigned unscheduled tasks.
     * @param includeOverdueUnscheduledTasks Ture to load overdue unscheduled tasks before Start Date if current date in visible date range.
     * @param tasks The array of tasks to populate.
     * @private
     */
    _loadTasks: function(startDate, endDate, includeScheduledTasks, includeAssignedUnscheduledTasks, tasks) {
        var tasksDataSource = View.dataSources.get('tasksDataSource');
        var startDateParameter = getIsoFormatDate(startDate);
        var endDateParameter = getIsoFormatDate(endDate);

        tasksDataSource.addParameter('includeScheduledHours', includeScheduledTasks);
        tasksDataSource.addParameter('includeAssignedUnscheduledHours', includeAssignedUnscheduledTasks);
        tasksDataSource.addParameter('dateStart', startDateParameter);
        
        if(includeScheduledTasks || includeAssignedUnscheduledTasks){
            tasksDataSource.addParameter('dateEnd', endDateParameter);
        }

        var records = tasksDataSource.getRecords();

        var model = this;
        var numberOfScheduledJobsPerPersonPerDay = {};

        _.each(records, function(record) {
            var cfId = record.getValue('wrcf.cf_id');
            var wrId = record.getValue('wrcf.wr_id');
            var description = record.getValue('wr.description');
            var startDate = record.getValue('wrcf.date_assigned');
            var startTime = record.getValue('wrcf.time_assigned');
            var dueDate = record.getValue('wr.date_escalation_completion');
            var estimatedHours = parseFloat(record.getValue('wrcf.hours_est'));

            var craftsperson = model.craftspersonsById[cfId];

            if (valueExists(craftsperson)) {
                var key = cfId + startDate.toString();
                var numberOfScheduledJobs = numberOfScheduledJobsPerPersonPerDay[key];
                if (numberOfScheduledJobs) {
                    numberOfScheduledJobs += 1;
                } else {
                    numberOfScheduledJobs = 1;
                }
                numberOfScheduledJobsPerPersonPerDay[key] = numberOfScheduledJobs;

                if (numberOfScheduledJobs <= model.config.maxScheduledJobsPerPersonPerDay || includeAssignedUnscheduledTasks) {
                    var dateStart = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), startTime.getHours(), startTime.getMinutes(), startTime.getSeconds());
                    dateStart.setMilliseconds(0);
                    var dateEnd = dateStart.add(Date.SECOND, estimatedHours*60*60);
                    
                    var task = {
                        cfId: cfId,
                        wrId: wrId,
                        description: description,
                        startDate: dateStart,
                        endDate: dateEnd,
                        dueDate: dueDate,
                        estimatedHours: estimatedHours,
                        taskType: 'TASK',
                        text: wrId
                    };

                    tasks.push(task);

                    if (includeAssignedUnscheduledTasks) {
                        // Work Requests Assignments that do not contain a Due Date and Estimated Hours will be excluded from calculation and display on the Unscheduled Task Bar.
                        if (dueDate) {
                            craftsperson.statistics.addUnscheduledTask(task);
                        }
                    }
                }
            }
        });
    },

    /**
     * Loads craftsperson schedules and calculates totals per craftsperson. See https://confluence.archibus.zone/display/AP/Specification+-+Labor+Scheduling+Board, section "4.1.3 Craftsperson Rows -
     * Statistics".
     * @private
     */
    _loadCraftspersonSchedules: function(startDate, endDate) {
        var model = this;

        model.craftspersonSchedules = {};
        var scheduleRecords = View.dataSources.get('craftspersonSchedulesDataSource').getRecords();
        _.each(scheduleRecords, function(record) {
            var schedule = new CraftspersonSchedule(record);
            model.craftspersonSchedules[schedule.id] = schedule;
        });

        var dayRecords = View.dataSources.get('craftspersonScheduleDaysDataSource').getRecords();

        _.each(model.craftspersonSchedules, function(schedule) {
            var scheduleId = schedule.scheduleId;
            _.filter(dayRecords, function(dayRecord) {
                if (dayRecord.getValue('cf_schedules_days.cf_schedule_id') === scheduleId) {
                    schedule.addDay(new CraftspersonScheduleDay(dayRecord));
                }

            });
        });

        model.craftspersonScheduleVariances = [];
        var exceptionRecords = View.dataSources.get('craftspersonScheduleVariancesDataSource').getRecords();
        _.each(exceptionRecords, function(record) {
            var variance = new CraftspersonScheduleVariance(record);
            model.craftspersonScheduleVariances.push(variance);
        });
    },

    /**
     * Updates totals for specified craftsperson for specified date range.
     * If the data range includes today's date, updates totals starting from today and ending at the end date.
     */
    updateCraftspersonTotals: function(craftsperson, startDate, endDate) {
        var endDateMidnight = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate(), 23, 59, 59);
        var today = new Date();
        if (today.between(startDate, endDateMidnight)) {
            startDate = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
        }

        this.updateCraftspersonTotalsForDateRange(craftsperson, startDate, endDate);
    },

    /**
     * Updates totals for specified craftsperson for specified date range.
     */
    updateCraftspersonTotalsForDateRange: function(craftsperson, startDate, endDate) {
        craftsperson.statistics.calculateSegmentStartDates();

        craftsperson.totals.scheduledHours = _.reduce(this.scheduledTasks, function(memo, task) {
            if (task.cfId === craftsperson.id && task.taskType == 'TASK') {
                craftsperson.statistics.addScheduledHours(task);

                if (task.endDate.getTime() <= endDate.add(Date.DAY, 1).getTime() && task.endDate.getTime() >= startDate.getTime()) {
                    memo = memo + hoursBetween(task.startDate, task.endDate);
                }
            }
            return memo;
        }, 0);

        craftsperson.totals.assignedUnscheduledHours = _.reduce(this.assignedUnscheduledTasks, function(memo, task) {
            if (task.cfId === craftsperson.id && task.dueDate.getTime() <= endDate.getTime() && task.dueDate.getTime()>= startDate.getTime()) {
                return memo + task.estimatedHours;
            } else {
                return memo;
            }
        }, 0);

        craftsperson.totals.availableHours = 0;
		
		//get new start date to expand the start date of the display date range.
        var newStartDate = this.getNewStartDate(startDate);
        
        for (var date = newStartDate; date.between(newStartDate, endDate.add(Date.MONTH, 2)); date = date.add(Date.DAY, 1)) {
            //get exceptions.
            var scheduleExceptions = _.filter(this.craftspersonScheduleVariances, function(scheduleException) {
                return scheduleException.appliesTo(craftsperson.id, date);
            });
            var schedule = this.findApplicableSchedule(craftsperson, date);
            if (!schedule) {
                //APP-998: The 'STANDARD' variances should be included in craftsperson totals available hours if user has no regular schedule.
                schedule=new CraftspersonSchedule();
            }

            var availableHours = schedule.getAvailableHours(date, scheduleExceptions);
            craftsperson.statistics.addStandardHours(date, availableHours);

            if (date.between(startDate, endDate)) {
                craftsperson.totals.availableHours += availableHours;
            }
        }
        
        if (craftsperson.totals.availableHours !=0) {
            craftsperson.totals.availableHours = parseFloat(craftsperson.totals.availableHours.toFixed(2));
        }

        craftsperson.statistics.prorateUnscheduledSegments();
        var weekStatistics = craftsperson.statistics.getStatisticsForDateRange(startDate, endDate, craftsperson.totals);

        craftsperson.totals.assignedHours = Number((weekStatistics.scheduledHours + weekStatistics.unscheduledHours + weekStatistics.proratedUnscheduledHours).toFixed(2));
        craftsperson.totals.remainingHours = Number((weekStatistics.standardHours - craftsperson.totals.assignedHours).toFixed(2));
        craftsperson.totals.proratedUnscheduledHours = Number(weekStatistics.proratedUnscheduledHours.toFixed(2));
        craftsperson.totals.assignedUnscheduledHoursWithProrateHours = weekStatistics.unscheduledHours + weekStatistics.proratedUnscheduledHours;
        if (weekStatistics.availableHours == 0) {
            craftsperson.totals.percentAllocated = 'N/A';
        } else {
            craftsperson.totals.percentAllocated = parseInt((weekStatistics.percentAllocated * 100).toFixed(0));
        }
    },
    
    /**
     * Get a new start date before start date of the display date range.
     * This value based on today and the day one month ago. the longest time is one month.
     * @param startDate Start date of the display date range.
     */
    getNewStartDate: function(startDate){
        var today = new Date();
            today = new Date(today.getFullYear(),today.getMonth(),today.getDate(),0,0,0);
        var newStartDate = today;
        var startDateMonthAgo = startDate.add(Date.MONTH,-1);
        if(newStartDate.getTime() < startDateMonthAgo.getTime()){
            newStartDate = startDateMonthAgo;
        }
        
        return newStartDate;
    },

    /**
     * Creates appointments that display craftsperson schedules and variances.
     * @param startDate
     * @param endDate
     * @private
     */
    _createCraftspersonTasks: function(startDate, endDate) {
        var model = this;
        
        // schedules
        _.each(this.craftspersons,function(craftsperson){
            for (var date = new Date(startDate.getTime()); date.between(startDate, endDate); date = date.add(Date.DAY, 1)) {
                //APP-1382:Only one schedule should be apply if specify date has multiple schedules.
                var schedule = model.findApplicableSchedule(craftsperson, date);
                if(schedule){
                    //get all schedule data records for the specify date.
                    var days = schedule.getDays(date);
                    _.each(days, function(day) {
                        model.scheduledTasks.push({
                            cfId: schedule.cfId,
                            startDate: new Date(date.getFullYear(), date.getMonth(), date.getDate(), day.startTime.getHours(), day.startTime.getMinutes(), day.startTime.getSeconds()),
                            endDate: new Date(date.getFullYear(), date.getMonth(), date.getDate(), day.endTime.getHours(), day.endTime.getMinutes(), day.endTime.getSeconds()),
                            taskType: day.rateType,
                            type: 'REGULAR',
                            text:day.rateType,
                            disabled: true
                        });
                    });
                }
            } 
        });

        // schedule variances
        _.each(model.craftspersonScheduleVariances, function(variance) {
            model.scheduledTasks.push({
                cfId: variance.cfId,
                startDate: new Date(variance.startDate.getFullYear(), variance.startDate.getMonth(), variance.startDate.getDate(), variance.startTime.getHours(), variance.startTime.getMinutes(), variance.startTime.getSeconds()),
                endDate: new Date(variance.endDate.getFullYear(), variance.endDate.getMonth(), variance.endDate.getDate(), variance.endTime.getHours(), variance.endTime.getMinutes(), variance.endTime.getSeconds()),
                taskType: variance.varianceType,
                type: 'VARIANCE',
                text: variance.varianceType,
                siteId:variance.siteId,
                blId:variance.blId,
                disabled: true
            });
        });
    },

    /**
     * Finds a schedule that applies to specified craftsperson on specified date.
     * @param craftsperson The craftsperson record.
     * @param date The date.
     */
    findApplicableSchedule: function(craftsperson, date) {
        var schedule = null;

        var applicableSeasonalSchedule = null;
        var applicableNonSeasonalSchedule = null;

        _.each(this.craftspersonSchedules, function(schedule) {
            if (schedule.appliesTo(craftsperson.id, date)) {
                if (schedule.isSeasonal) {
                    applicableSeasonalSchedule = schedule;
                } else {
                    applicableNonSeasonalSchedule = schedule;
                }
            }
        });

        if (applicableNonSeasonalSchedule) {
            schedule = applicableNonSeasonalSchedule;
        } else if (applicableSeasonalSchedule) {
            schedule = applicableSeasonalSchedule;
        }

        return schedule;
    },
    
    /**
     * Get due date by work request code.
     */
    getDueDateByWrId: function(wrId){
        var dueDate='';
        var res=new Ab.view.Restriction();
        res.addClause('wr.wr_id',wrId,'=');
        var wrRecord=View.dataSources.get('updateWorkRequestDataSource').getRecord(res);
        dueDate=wrRecord.getValue('wr.date_escalation_completion');
        return dueDate;
    },
    
    /**
     * Get specify work request record.
     */
    getWorkRequestRecord: function(wrId){
        var wrRestriction = new Ab.view.Restriction();
            wrRestriction.addClause('wr.wr_id',wrId,'=');
        var wrRecord=View.dataSources.get('workRequestDataSource').getRecord(wrRestriction);
        return wrRecord;
    },
    
    /**
     * Get work request trades,union with ','.
     */
    getWorkRequestTrades: function(wrId){
        // query trades per work request
        var restriction =new Ab.view.Restriction();
            restriction.addClause('wrtr.wr_id',wrId,'=');
        var workRequestTrades = View.dataSources.get('workRequestTradesDataSource').getRecords(restriction);
        return trades = _.reduce(workRequestTrades, function(trades, workRequestTrade) {
            if (trades !== '') {
                trades = trades + ', ';
            }
            trades = trades + workRequestTrade.getValue('wrtr.tr_id');
            return trades;
        }, '');
    },
    
    /**
     * Assign work request, this function used to assign multiple selected work request.
     */
    assignWorkRequest: function(workRequestId, cfId, estimateHours, workType){
        var updateTaskDataSource = View.dataSources.get('updateTaskDataSource');
        var record = new Ab.data.Record();
        record.isNew = true;
        record.setValue('wrcf.wr_id',workRequestId);
        record.setValue('wrcf.cf_id',cfId);
        record.setValue('wrcf.date_assigned',new Date());
        record.setValue('wrcf.time_assigned',new Date());
        record.setValue('wrcf.hours_est',estimateHours);
        record.setValue('wrcf.work_type',workType);
        record.setValue('wrcf.is_scheduled','0');
        updateTaskDataSource.saveRecord(record);
    },
    
    /**
     * Delete unscheduled task.
     */
    deleteUnscheduledTask: function(task){
        var updateTaskDataSource = View.dataSources.get('updateTaskDataSource');
        var record = new Ab.data.Record();
        record.isNew = false;
        record.setValue('wrcf.wr_id', task.oldWrId);
        record.setValue('wrcf.cf_id', task.oldCfId);
        record.setValue('wrcf.date_assigned', task.oldDateAssigned);
        record.setValue('wrcf.time_assigned', task.oldTimeAssigned);
        updateTaskDataSource.deleteRecord(record);
        var dueDate=this.getDueDateByWrId(task.wrId);
        //remove task from unscheduled array.
        if(dueDate){
            this._removeUnscheduledTask(task.wrId,task.oldCfId,task.oldDateAssigned,task.oldTimeAssigned,dueDate);
        }
        
        this.calculateSummaryAfterWorkRequestCraftspersonDeleted(task.oldWrId,task.oldCfId,task.oldDateAssigned,task.oldTimeAssigned);
    },
    
    /**
     * Delete unscheduled task by the passed primary key values.
     */
    deleteUnscheduledTaskByPrimaryKey: function(wrId,cfId,dateAssigned,timeAssigned){
        var updateTaskDataSource = View.dataSources.get('updateTaskDataSource');
        var record = new Ab.data.Record();
        record.isNew = false;
        record.setValue('wrcf.wr_id', wrId);
        record.setValue('wrcf.cf_id', cfId);
        record.setValue('wrcf.date_assigned', dateAssigned);
        record.setValue('wrcf.time_assigned', timeAssigned);
        updateTaskDataSource.deleteRecord(record);
        var dueDate=this.getDueDateByWrId(wrId);
        //remove task from unscheduled array.
        if(dueDate){
            this._removeUnscheduledTask(wrId,cfId,dateAssigned,timeAssigned,dueDate);
        }
        
        this.calculateSummaryAfterWorkRequestCraftspersonDeleted(wrId,cfId,dateAssigned,timeAssigned);
    },
    
    /**
     * Remove unscheduled task.
     * 1. remove task from model.assignedUnscheduledTasks array.
     * 2. remove task from segment statistics.
     * 3. call updatedCraftspersonTotals() to recalculate craftsperson totals.
     */
    _removeUnscheduledTask: function(wrId,cfId,dateAssigned,timeAssigned,dueDate) {
        
        //Remove unscheduled task from assignedUnscheduledTasks array.
        var startDate = new Date(dateAssigned.getFullYear(), dateAssigned.getMonth(), dateAssigned.getDate(), timeAssigned.getHours(), timeAssigned.getMinutes(), timeAssigned.getSeconds());
        startDate.setMilliseconds(0);
        
        this.assignedUnscheduledTasks = _.filter(this.assignedUnscheduledTasks, function(unscheduledTask) {
            return !(unscheduledTask.wrId == wrId && unscheduledTask.cfId == cfId && startDate.getTime() == unscheduledTask.startDate.getTime());
        });
        
        //Remove unscehduled task from segment statistics.
        var craftsperson = this.craftspersonsById[cfId];
        if(craftsperson){
            var deleteTask = {
                wrId: wrId,
                cfId: cfId,
                startDate: startDate,
                dueDate: dueDate
            }
            if(dueDate){
                craftsperson.statistics.removeUnscheduledTask(deleteTask);
            }   
        }
    }
});

TimeSegment = Base.extend({

    /**
     * Start time.
     */
    startTime: null,

    /**
     * End time.
     */
    endTime: null,

    /**
     * Constructor.
     */
    constructor: function(startTime, endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    },

    /**
     * Returns true if this time segment overlaps another time segment.
     * @param another
     */
    overlaps: function(another) {
        //return (this.endTime >= another.startTime && this.startTime <= another.endTime)
        //APP-2595:Fixed issue that the Standard Hours shows incorrectly cause by overlap check logic.
        return !(this.endTime <= another.startTime || this.startTime >= another.endTime);
    },

    /**
     * Returns an object that represents combined time of this time segment and another one.
     * @param another
     */
    merge: function(another) {
        var earliestStartTime = this.startTime < another.startTime ? this.startTime : another.startTime;
        var latestEndTime = this.endTime > another.endTime ? this.endTime : another.endTime;

        return new TimeSegment(earliestStartTime, latestEndTime);
    },

    /**
     * Split this segments into smaller segments that do not overlap with another segment.
     * @param another
     */
    split: function(another) {
        var newSegments = [];

        if (this.endTime > another.startTime && this.startTime < another.startTime) {
            newSegments.push(new TimeSegment(this.startTime, another.startTime));
        }
        if (this.startTime < another.endTime && this.endTime > another.endTime) {
            newSegments.push(new TimeSegment(another.endTime, this.endTime));
        }

        return newSegments;
    },

    /**
     * Returns the number of available hours.
     */
    getDuration: function() {
        return (this.endTime.getTime() - this.startTime.getTime()) / (1000 * 60 * 60);
    }
});

/**
 * Encapsulates information about a craftsperson schedule for a specific day of the week.
 */
CraftspersonScheduleDay = TimeSegment.extend({

    /**
     * 'MONDAY', 'TUESDAY', and so on.
     */
    dayOfWeek: '',

    /**
     * 'STANDARD', 'OVERTIME', or 'DOUBLETIME'
     */
    rateType: '',

    /**
     * Constructor.
     */
    constructor: function(record) {
        this.inherit(record.getValue('cf_schedules_days.time_start'), record.getValue('cf_schedules_days.time_end'));
        this.dayOfWeek = record.getValue('cf_schedules_days.day_of_week');
        this.rateType = record.getValue('cf_schedules_days.rate_type');
    },

    /**
     * Returns true if the day schedule is applicable for specific time.
     * @param cfId
     * @param date
     */
    appliesTo: function(date) {
        return (this.dayOfWeek === fullyDayName(date) && date.getHours() >= this.startTime.getHours() && date.getHours() < this.endTime.getHours());
    },

    /**
     * Returns the number of available hours.
     */
    getAvailableHours: function() {
        return this.endTime.getHours() - this.startTime.getHours();
    }
});

/**
 * Encapsulates information about a craftsperson schedule variance.
 */
CraftspersonScheduleVariance = TimeSegment.extend({

    /**
     * The craftsperson ID.
     */
    cfId: '',

    /**
     * Date when the exception starts.
     */
    startDate: null,

    /**
     * Date when the exception ends.
     */
    endDate: null,
    

    /**
     * 'STANDARD', 'BUSY', or 'PERSONAL'
     */
    varianceType: '',
    
    /**
     * Site Code, only has value when variance type is ON SITE.
     */
    siteId: '',
    
    /**
     * Building Code, only has value when variance type is ON SITE.
     */
    blId: '', 

    /**
     * Constructor.
     */
    constructor: function(record) {
        var dateStart = record.getValue('cf_schedules_variances.date_start');
        var timeStart = record.getValue('cf_schedules_variances.time_start');
        var dateEnd = record.getValue('cf_schedules_variances.date_end');
        var timeEnd = record.getValue('cf_schedules_variances.time_end');
        var start_date_time = new Date(dateStart.getFullYear(),dateStart.getMonth(),dateStart.getDate(),timeStart.getHours(),timeStart.getMinutes(),timeStart.getSeconds());
        var end_date_time = new Date(dateEnd.getFullYear(),dateEnd.getMonth(),dateEnd.getDate(),timeEnd.getHours(),timeEnd.getMinutes(),timeEnd.getSeconds());
        
        this.inherit(start_date_time, end_date_time);
        this.cfId = record.getValue('cf_schedules_variances.cf_id');
        this.startDate=dateStart
        this.endDate=dateEnd;
        this.varianceType = record.getValue('cf_schedules_variances.sched_vars_type');
        this.siteId = record.getValue('cf_schedules_variances.site_id');
        this.blId = record.getValue('cf_schedules_variances.bl_id');
    },

    /**
     * Returns true if the schedule is applicable for a specific craftsperson and date.
     * @param cfId
     * @param date
     */
    appliesTo: function(cfId, date) {
        var result = false;
        if (this.cfId === cfId && date.getFullYear()==this.startDate.getFullYear() && date.getMonth()==this.startDate.getMonth() && date.getDate()==this.startDate.getDate()) {
            result = this;
        }

        return result;
    },

    /**
     * Returns true if the schedule is applicable for a specific craftsperson and date and time.
     * @param cfId
     * @param date
     */
    appliesToHour: function(cfId, date) {
        var result = false;

        var start = new Date(this.startDate.getTime());
        start.setHours(this.startTime.getHours());

        var end = new Date(this.endDate.getTime());
        end.setHours(this.endTime.getHours() - 1);

        if (this.cfId === cfId && date.between(start, end)) {
            result = this;
        }

        return result;
    },

    /**
     * Returns the number of available hours.
     */
    getHours: function() {
        //return this.endTime.getHours() - this.startTime.getHours();
        return ((this.endTime.getTimes() - this.startTime.getTimes())/(60*60*1000)).toFixed(2);
    }
});

/**
 * Encapsulates information about a single craftsperson schedule. A schedule is defined as a set of records in the cf_schedule table.
 */
CraftspersonSchedule = Base.extend({

    /**
     * The unique schedule ID.
     */
    id: '',

    /**
     * referenced schedule ID.
     */
    scheduleId: '',

    /**
     * The craftsperson ID.
     */
    cfId: '',

    /**
     * Schedule type: CUSTOM SCHEDULE or SHIFT DEFINITION.
     */
    type: '',

    /**
     * Schedule name.
     */
    name: '',

    /**
     * Date when the schedule starts. If null, the schedule is open-started.
     */
    startDate: null,

    /**
     * Date when the schedule ends. If null, the schedule is open-ended.
     */
    endDate: null,

    /**
     * True if the schedule is seasonal.
     */
    isSeasonal: false,

    /**
     * Array of CraftspersonScheduleDay objects. One object defines a schedule for a specific day and pay rate.
     */
    days: null,

    /**
     * Array of CraftspersonScheduleVariance objects. One object defines an exception for a time interval.
     */
    exceptions: null,

    /**
     * Constructor.
     */
    constructor: function(record) {
        //APP-998:support constructor with no parameters.
        if(!Ext.isEmpty(record)){
            this.id = record.getValue('cf_schedules_assign.auto_number');
            this.scheduleId = record.getValue('cf_schedules_assign.cf_schedule_id');
            this.cfId = record.getValue('cf_schedules_assign.cf_id');
            this.startDate = record.getValue('cf_schedules_assign.date_start');
            if (!this.startDate) {
                this.startDate = new Date(1, 1);
            }
            this.endDate = record.getValue('cf_schedules_assign.date_end');
            if (!this.endDate) {
                this.endDate = new Date(2999, 1);
            }
            this.isSeasonal = record.getValue('cf_schedules_assign.is_seasonal')=='1'?true:false;

            this.type = record.getValue('cf_schedules.schedule_type');
            this.name = record.getValue('cf_schedules.schedule_name');
        }
        
        this.days=[];
    },

    /**
     * Adds a day to the schedule.
     * @param day
     */
    addDay: function(day) {
        this.days.push(day);
    },

    /**
     * Returns true if the schedule is applicable for a specific craftsperson and date.
     * @param cfId
     * @param date
     */
    appliesTo: function(cfId, date) {
        var result = false;
        
        var startDate = new Date(this.startDate.getTime());
        var endDate = new Date(this.endDate.getTime());
        
        if (this.isSeasonal) {
            startDate.setFullYear(date.getFullYear());
            endDate.setFullYear(date.getFullYear());
            //To get the year of start date and end date based on the current date.
            if(endDate.getTime()<startDate.getTime()){
                //If current date less than end date, then start date should be set to last year of current date. otherwise, it should be set to current year of current date.
                if(date.getTime() <= endDate.getTime()){
                    startDate.setFullYear(date.getFullYear()-1);
                }else{
                    endDate.setFullYear(date.getFullYear()+1);
                }
            }
        }

        if (this.cfId === cfId && date.between(startDate, endDate)) {
            result = this;
        }

        return result;
    },

    /**
     * Returns true if the schedule is applicable for specific date.
     * @param date
     */
    appliesToDate: function(date) {
        var result = false;

        var startDate = new Date(this.startDate.getTime());
        var endDate = new Date(this.endDate.getTime());
        
        if (this.isSeasonal) {
            startDate.setFullYear(date.getFullYear());
            endDate.setFullYear(date.getFullYear());
            if(endDate.getTime()<startDate.getTime()){
                endDate.setFullYear(date.getFullYear()+1);
            }
        }
        
        if (date.between(startDate, endDate)) {
            result = this;
        }

        return result;
    },

    /**
     * Returns an array of schedule day objects for day of the week matching specified date.
     * @param date
     */
    getDays: function(date) {
        var name = fullyDayName(date);

        return _.filter(this.days, function(day) {
            var match = day.dayOfWeek === name;
            if (match) {
                day.startTime.setFullYear(date.getFullYear());
                day.startTime.setMonth(date.getMonth());
                day.startTime.setDate(date.getDate());
                day.endTime.setFullYear(date.getFullYear());
                day.endTime.setMonth(date.getMonth());
                day.endTime.setDate(date.getDate());
            }

            return match;
        });
    },

    /**
     * Returns the number of available hours for specified date.
     */
    getAvailableWholeHours: function(date, scheduleExceptions) {
        // 24 hour array, 1 means the hour is available
        var hours = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];

        // helper function to set available hours in the array
        var setAvailableHours = function(startHour, endHour, availability) {
            for (var h = startHour; h <= endHour; h++) {
                hours[h] = availability;
            }
        }

        // set available hours for schedule days
        var days = this.getDays(date);
        _.each(days, function(day) {
            // only regular STANDARD rate type is available.
            if (day.rateType === 'STANDARD') {
                setAvailableHours(day.startTime.getHours(), day.endTime.getHours() - 1, 1);
            }
        });

        // set available or unavailable hours for schedule exceptions
        _.each(scheduleExceptions, function(scheduleException) {
            // site visit variances should not affect the calculation.
            if (scheduleException.varianceType !== 'ON SITE') {
                var availability = (scheduleException.varianceType === 'STANDARD' ? 1 : 0);
                setAvailableHours(scheduleException.startTime.getHours(), scheduleException.endTime.getHours() - 1, availability);
            }

        });

        // calculate and return the total available hours
        return _.reduce(hours, function(memo, hour) {
            return memo + hour;
        });
    },

    /**
     * Returns the number of available hours and minutes for specified date.
     * The result is a number. E.g. 8.25 means 8 hours and 15 minutes.
     *
     * Pseudo-code:
     * 1. Find all available and unavailable segments.
     * 2. Merge overlapping available and overlapping unavailable segments.
     * 3. For each unavailable segment:
     * 3.1. Find available segments that this unavailable segment overlaps.
     * 3.2. Split these available segments into smaller segments that do not overlap with this unavailable segment.
     * 4. Total the duration of all remaining available segments.
     *
     * Example:
     * A1 available 8:00-17:00
     * A2 available 15:00-18:00
     * A3 available 20:00-24:00
     * U1 unavailable 16:00-16:30
     * U2 unavailable 21:00-21:30
     *
     * Example steps:
     * 1. Available segments A1, A2, A3. Unavailable segments U1, U2.
     * 2. Available segments A1 and A2 overlap, merge them into one segment: A4 8:00-18:00. No overlapping unavailable segments.
     * 3. For U1:
     * 3.1. U1 overlaps A4.
     * 3.2. Split A4 into two available segments: A5 8:00-16:00 and A6 16:30-18:00.
     * 3. For U2:
     * 3.1. U2 overlaps A3.
     * 3.2. Split A3 into two available segments: A7 20:00-21:00 and A8 21:30-24:00.
     * 4. Remaining available segments: A5, A6, A7, A8. Total duration: 13h 00m.
     */
    getAvailableHours: function(date, scheduleExceptions) {
        // 1. Find all available and unavailable segments.
        var availableSegments = _.filter(this.getDays(date), function(day) {
            return day.rateType === 'STANDARD';
        });
        
        //'ON SITE' type variances should be excluded in exception array.
        scheduleExceptions=_.filter(scheduleExceptions,function(exception){
            return exception.varianceType !== 'ON SITE';
        });
        
        var unavailableSegments = _.filter(scheduleExceptions, function(scheduleException) {
            return scheduleException.varianceType !== 'STANDARD';
        });
        availableSegments = _.union(availableSegments, _.difference(scheduleExceptions, unavailableSegments));

        // 2. Merge overlapping available and overlapping unavailable segments.
        var mergeOverlappingSegments = function(segments) {
            var newSegments = segments;

            var newSegment = null;
            var oldSegment1 = null;
            var oldSegment2 = null;
            _.each(segments, function(segment1) {
                _.each(segments, function(segment2) {
                    if (segment1 !== segment2 && segment1.overlaps(segment2)) {
                        newSegment = segment1.merge(segment2);
                        oldSegment1 = segment1;
                        oldSegment2 = segment2;
                    }
                });
            });
            if (newSegment) {
                newSegments = _.difference(segments, [oldSegment1, oldSegment2]);
                newSegments.push(newSegment);

                newSegments = mergeOverlappingSegments(newSegments);
            }

            return newSegments;
        }
        availableSegments = mergeOverlappingSegments(availableSegments);
        unavailableSegments = mergeOverlappingSegments(unavailableSegments);

        // 3. For each unavailable segment:
        _.each(unavailableSegments, function(unavailableSegment) {

            // 3.1. Find available segments that this unavailable segment overlaps.
            var availableSegmentsToSplit = _.filter(availableSegments, function(availableSegment) {
                return unavailableSegment.overlaps(availableSegment);
            });

            // 3.2. Split these available segments into smaller segments that do not overlap with this unavailable segment.
            availableSegments = _.difference(availableSegments, availableSegmentsToSplit);
            _.each(availableSegmentsToSplit, function(segment) {
                availableSegments = _.union(availableSegments, segment.split(unavailableSegment));
            });
        });

        // 4. Total the duration of all remaining available segments.
        return _.reduce(availableSegments, function(memo, availableSegment) {
            return memo + availableSegment.getDuration();
        }, 0);
    }
});

/**
 * Help function to calculate the day difference between two dates.
 * @param startDate
 * @param endDate
 * @returns {number}
 */
function daysBetween(startDate, endDate) {
    var oneDay = 1000 * 60 * 60 * 24;

    var date1ms = startDate.getTime();
    var date2ms = endDate.getTime();

    return Math.floor((date2ms - date1ms) / oneDay);
}

/**
 * Help function to calculate the hour difference between two dates.
 * @param startDate
 * @param endDate
 * @returns {number}
 */
function hoursBetween(startDate, endDate) {
    var oneHour = 1000 * 60 * 60;

    var date1ms = startDate.getTime();
    var date2ms = endDate.getTime();

    return parseFloat(((date2ms - date1ms) / oneHour).toFixed(2));
}

/**
 * Returns the abbreviate name of the day of the week by date number, the day name need to keep the same with the enum list of cf_schedules_days.day_of_week, day name do not need to localized.
 * @param date
 * @returns {string}
 */
function dayName(date) {
    var dayName = '';
    switch (date.getDay()) {
    case 0:
        dayName = 'SUN';
        break;
    case 1:
        dayName = 'MON';
        break;
    case 2:
        dayName = 'TUE';
        break;
    case 3:
        dayName = 'WED';
        break;
    case 4:
        dayName = 'THU';
        break;
    case 5:
        dayName = 'FRI';
        break;
    case 6:
        dayName = 'SAT';
        break;
    }

    return dayName;
}

/**
 * Returns the fully name of the day of the week by date number, the day name need to keep the same with the enum list of cf_schedules_days.day_of_week, day name do not need to localized.
 * @param date
 * @returns {string}
 */
function fullyDayName(date) {
    var dayName = '';
    switch (date.getDay()) {
    case 0:
        dayName = 'SUNDAY';
        break;
    case 1:
        dayName = 'MONDAY';
        break;
    case 2:
        dayName = 'TUESDAY';
        break;
    case 3:
        dayName = 'WEDNESDAY';
        break;
    case 4:
        dayName = 'THURSDAY';
        break;
    case 5:
        dayName = 'FRIDAY';
        break;
    case 6:
        dayName = 'SATURDAY';
        break;
    }

    return dayName;
}