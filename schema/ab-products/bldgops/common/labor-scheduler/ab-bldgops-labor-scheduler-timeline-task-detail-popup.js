var laborScheduleTaskDetailCtrl = View.createController('laborScheduleTaskDetailCtrl', {

    wrId: null,
    
    schedulerType:null,
    
    /**
     * Edit options of the selected appointment data.
     */
    editOptions : null,
    
    /**
     * This value indicate that whether clicked the 'Copy' button to create another time blocks.
     */
    copyMode: false,

    afterInitialDataFetch: function() {
        this.schedulerType = View.parameters['schedulerType'];
        this.editOptions = View.parameters['editOptions'];
        
        var taskRestriction = View.parameters['taskRestriction'];
        this.taskDetailsForm.refresh(taskRestriction);

        // hide work request information panels by default.
        showWorkRequestInformationPanels(false);
        
        this.enableWrcfDetailFieldsByEditOptions(this.editOptions);
    },
    
    /**
     * Set field value after view refresh.
     */
    taskDetailsForm_afterRefresh: function(){
        var isScheduled=this.taskDetailsForm.getFieldValue('wrcf.is_scheduled');
        selectIsScheduledRadio(isScheduled);
    },

    /**
     * Save task record.
     */
    taskDetailsForm_onSave: function() {
        var isScheduled = this.taskDetailsForm.getFieldValue('wrcf.is_scheduled');
        if(isScheduled=='0'){
            var today = new Date();
            var dateAssigned = this.taskDetailsForm.getDataSource().formatValue('wrcf.date_assigned',today,true);
            var timeAssigned = this.taskDetailsForm.getDataSource().formatValue('wrcf.time_assigned',today.add(Date.HOUR,1),true);
            this.taskDetailsForm.setFieldValue('wrcf.date_assigned',dateAssigned);
            this.taskDetailsForm.setFieldValue('wrcf.time_assigned',timeAssigned);
        }
        
        if (this.validateTaskBeforeSave(this.copyMode)) {
            var updateRecord = this.taskDetailsForm.getRecord();
            var openerController=null;
            if(this.schedulerType=='multipleCraftspersonScheudler'){
                openerController=View.getOpenerView().controllers.get('laborSchedulingBoard-workSchedule');
            }
            
            if(this.schedulerType=='signleCraftspersonScheudler'){
                openerController=View.getOpenerView().controllers.get('laborSchedulingBoard-craftspersonScheduleCtrl');
            }
            
            if(valueExists(openerController)){
                if(this.copyMode){
                    //add new task.
                    openerController.trigger('app:bldgops:laborScheduler:onCopyNewTask', updateRecord);
                }else{
                    //update task.
                    openerController.trigger('app:bldgops:laborScheduler:onSaveTaskDetail', updateRecord);
                }
                
            }
            
            View.getOpenerView().closeDialog();
        }
    },
    
    /**
     * Validate form before form saved.
     */
    validateTaskBeforeSave: function(isNew){
        var validated = true;
        if(this.taskDetailsForm.canSave()){
            if(isNew){
                var updateRecord = this.taskDetailsForm.getRecord();
                var wrId = this.taskDetailsForm.getFieldValue('wrcf.wr_id');
                var cfId = this.taskDetailsForm.getFieldValue('wrcf.cf_id');
                var dateAssigned = this.taskDetailsForm.getDataSource().formatValue('wrcf.date_assigned',updateRecord.getValue('wrcf.date_assigned'),false);
                var timeAssigned =  this.taskDetailsForm.getDataSource().formatValue('wrcf.time_assigned',updateRecord.getValue('wrcf.time_assigned'),false);
                
                var res = new Ab.view.Restriction();
                    res.addClause('wrcf.wr_id',wrId,'=');
                    res.addClause('wrcf.cf_id',cfId,'=');
                    res.addClause('wrcf.date_assigned',dateAssigned,'=');
                    res.addClause('wrcf.time_assigned',timeAssigned,'=');
                var records = View.dataSources.get('taskDataSource').getRecords(res);
                if(records.length>0){
                    validated = false;
                    View.alert(getMessage('duplicatedAssignmentMessage'));
                }
            }
            
        }else{
            validated = false;
        }
        
        return validated;
    },

    /**
     * Close task detail dialog.
     */
    taskDetailsForm_onCancel: function() {
        View.getOpenerView().closeDialog();
    },
    
    
    /**
     * Copy task information.
     * 
     * The user can click the Copy button in order to create another work request assignment for this same work request.  
     * This will provide a quick way to assign additional craftspeople to the same work request at the same time.  
     * The copy button will clear out the Craftsperson Code field, but leave the other field values the same as they were.  
     * The user can then choose a different craftsperson, and optionally change any of the other fields.
     */
    taskDetailsForm_onCopy: function(){
        this.changeToCopyMode();
    },
    
    /**
     * Remove selected
     */
    taskDetailsForm_onRemove: function(){
        var openerController=null;
        if(this.schedulerType=='multipleCraftspersonScheudler'){
            openerController=View.getOpenerView().controllers.get('laborSchedulingBoard-workSchedule');
        }
        
        if(this.schedulerType=='signleCraftspersonScheudler'){
            openerController=View.getOpenerView().controllers.get('laborSchedulingBoard-craftspersonScheduleCtrl');
        }
        View.confirm(getMessage('removeSelectedUnscheduledTaskMessage'),function(button){
           if(button == 'yes'){
               var updateRecord = View.panels.get('taskDetailsForm').getRecord();
               var isScheduled = updateRecord.oldValues['wrcf.is_scheduled'];
               
               if(isScheduled == '0' || isScheduled == '1'){
                   openerController.trigger('app:bldgops:laborScheduler:onRemoveAssignment',isScheduled);
               }
               
               View.getOpenerView().closeDialog();
           } 
        });
    },
    
    /**
     * Change to copy mode.
     */
    changeToCopyMode: function(){
        this.copyMode=true;
        //$('scheduledRadio').checked=true;
        //selectIsScheduledRadio('1');
        //this.taskDetailsForm.setFieldValue('wrcf.cf_id','');
        this.taskDetailsForm.actions.get('remove').enable(false);
    },
    
    /**
     * Enable work request assignment fields by edit options.
     */
    enableWrcfDetailFieldsByEditOptions: function(editOptions){
        this.enableUpdateAssignmentDetail(editOptions.allowUpdating);
        this.enableDeleteAssignment(editOptions.allowDeleting);
        
        //set instruction text
        var instructionText = "";
        if(!editOptions.allowUpdating){
            instructionText = getMessage(editOptions.updateAlertText);
        }
        
        if(!editOptions.allowDeleting){
            instructionText = getMessage(editOptions.deleteAlertText);
        }
         
        this.taskDetailsForm.setInstructions(replaceHTMLPlaceholders(instructionText));
        
        
    },
    
    /**
     * Enable update assignment detail or not.
     * 
     * @param {allowUpdate}
     */
    enableUpdateAssignmentDetail: function(allowUpdate){
        this.taskDetailsForm.enableField('wrcf.cf_id',allowUpdate);
        this.taskDetailsForm.enableField('wrcf.date_assigned',allowUpdate);
        this.taskDetailsForm.enableField('wrcf.time_assigned',allowUpdate);
        this.taskDetailsForm.enableField('wrcf.work_type',allowUpdate);
        this.taskDetailsForm.enableField('wrcf.hours_est',allowUpdate);
        this.taskDetailsForm.enableField('wrcf.comments',allowUpdate);
        this.taskDetailsForm.enableField('wrcf.description',allowUpdate);
        this.taskDetailsForm.enableField('wrcf.date_escalation_completion',allowUpdate);
        
        Ext.get('scheduledRadio').dom.disabled = !allowUpdate;
        Ext.get('unscheduledRadio').dom.disabled = !allowUpdate;
        
        this.taskDetailsForm.actions.get('save').enable(allowUpdate);
        this.taskDetailsForm.actions.get('copy').enable(allowUpdate);
        
        
    },
    
    /**
     * Enable delete assignment.
     * @param {allowDelete}
     */
    enableDeleteAssignment: function(allowDelete){
        this.taskDetailsForm.actions.get('remove').enable(allowDelete);
    }
});

/**
 * @override Override 'Ab.view.Component.afterSetCollapsed' function to hide or show other panels after set panel collapsed.
 */
Ab.view.Component.prototype.afterSetCollapsed = function() {
    var me = this;
    if (this.id == 'workRequestInformationDetailPanel') {
        if (this.collapsed) {
            showWorkRequestInformationPanels(false);
        } else {
            showWorkRequestInformationPanels(true);
        }
    }
}

/**
 * show or hide work request information panels.
 * @param visible True|false.
 * @returns
 */
function showWorkRequestInformationPanels(visible) {
    View.panels.get('panel_request').show(visible);
    View.panels.get('panel_location').show(visible);
    View.panels.get('panel_equipment').show(visible);
    View.panels.get('panel_description').show(visible);
    View.panels.get('panel_trades').show(visible);
    View.panels.get('panel_craftspersons').show(visible);
    View.panels.get('panel_estimation').show(visible);
    View.panels.get('panel_escalation').show(visible);
}

function selectIsScheduledRadio(isScheduled){
    if(isScheduled=='1'){
        $('scheduledRadio').checked=true;
    }else{
        $('unscheduledRadio').checked=true;
    }
    var panel=View.panels.get('taskDetailsForm');
    var showFields=isScheduled=='1'?true:false;
    panel.setFieldValue('wrcf.is_scheduled',isScheduled);
    panel.showField('wrcf.date_assigned',showFields);
    panel.showField('wrcf.time_assigned',showFields);
    
}
