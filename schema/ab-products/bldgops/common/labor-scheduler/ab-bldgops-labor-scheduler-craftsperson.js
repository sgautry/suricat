var craftspersonScheduleCtrl = View.createController('laborSchedulingBoard-craftspersonScheduleCtrl', {
    /**
     * Selected Craftsperson.
     */
    selectedCraftsperson: '',

    /**
     * Craftsperson that compared with.
     */
    compareToCraftperson: '',

    /**
     * Scheduler instance.
     */
    scheduler: null,

    /**
     * Unscheduled task list items, used to find unscheduled task information more faster.
     */
    unscheduledTaskListItems: [],
    
    /**
     * Missed work list items, used to find missed work information more faster.
     */
    missedWorkListItems: [],
    
    /**
     * Opener View Controller.
     */
    openerViewController: null,
    
    /**
     * apply current scheduler view selected date from multiple scheduler view.
     */
    currentSelectedDate: null,

    afterViewLoad: function() {
        var locale = View.user.locale.slice(0, 2);
        DevExpress.localization.locale(locale);
        // bind event listener to render statistics message.
        this.on('app:bldgops:laborScheduler:renderStatistics', this.renderStataisticsMessage);

        this.on('app:bldgops:laborScheduler:afterWorkRequestUpdated', this.afterWorkRequestsUpdated);
        this.on('app:bldgops:laborScheduler:afterWorkRequestScheduled', this.afterWorkRequestScheduled);
        this.on('app:bldgops:laborScheduler:afterWorkRequestDeleted', this.afterWorkRequestDeleted);
        this.on('app:bldgops:laborScheduler:onSaveTaskDetail',this.onSaveTaskDetail);
        this.on('app:bldgops:laborScheduler:onCopyNewTask',this.onCopyNewTask);
        this.on('app:bldgops:laborScheduler:onRemoveAssignment',this.onRemoveAssignment);
        
        // register current tab controller to parent labor scheduler controller.
        this.openerViewController = View.getOpenerView().controllers.get('laborScheduler');
        this.selectedCraftsperson = this.openerViewController.selectedSingleCraftsperson;
        this.currentSelectedDate = this.openerViewController.selectedScheduleDate;
        this.registerTabController();

        this.scheduler = View.createControl({
            control: 'SingleCraftspersonWorkScheduleControl',
            container: 'singleLaborWorkScheduleContainer',
            panel: 'singleLaborWorkSchedule',
            maxScheduledJobsPerPersonPerDay: 3,
            model: new CraftspersonWorkScheduleModel({
                maxScheduledJobsPerPersonPerDay: 3
            }),
            startDate: this.currentSelectedDate
        });
        
    },

    afterInitialDataFetch: function() {
        // refresh all panels after view load
        this.refreshPanelsByCraftsperson();
    },
    
    /**
     * Register current tab controller to parent controller.
     */
    registerTabController: function() {
        this.openerViewController.tabCtrls['craftspersonScheduleTab'] = this;
    },

    /**
     * Refresh panels and set selected craftsperson after tab changed.
     */
    afterCraftspersonTabSelected: function(cfId, currentDate) {
        this.selectedCraftsperson = cfId;
        this.compareToCraftperson='';
        this.currentSelectedDate=currentDate;
        this.refreshPanelsByCraftsperson();
    },

    /**
     * Refresh view panels by selected craftsperson.
     */
    refreshPanelsByCraftsperson: function() {
        this.refreshCraftspersonForm();
        this.loadCraftspersonSchedule();
        this.refreshCraftspersonUnscheduleTaskList()
    },

    /**
     * Refresh craftsperson form panel by selected craftsperson.
     */
    refreshCraftspersonForm: function() {
        this.craftspersonForm.clear();
        this.craftspersonForm.setTitle(getMessage('craftspersonDataFormTitle').replace('{0}', this.selectedCraftsperson));
    },

    /**
     * Load single craftsperson schedule control.
     */
    loadCraftspersonSchedule: function() {
        var restriction = new Ab.view.Restriction();
        restriction.addClause('cf.cf_id', this.selectedCraftsperson, '=');

        // get craftsperson to compare with current selected craftsperon.
        if (valueExistsNotEmpty(this.compareToCraftperson)) {
            restriction.addClause('cf.cf_id', this.compareToCraftperson, '=', 'OR');
        }
        this.scheduler.restriction = restriction;
        this.scheduler.loadSchedule(this.currentSelectedDate);
    },

    /**
     * Refresh craftsperson unschedule task list by selected craftsperson.
     */
    refreshCraftspersonUnscheduleTaskList: function() {
        var restriction = new Ab.view.Restriction();
        restriction.addClause('wrcf.cf_id', this.selectedCraftsperson, '=');
        this.unscheduledWorkList.refresh(restriction);
        this.missedWorkList.refresh(restriction);
    },

    /**
     * Back single craftsperson schedule view to multiple craftsperson schedule view.
     */
    craftspersonForm_onBackToWork: function() {
        this.openerViewController.trigger('app:bldgops:laborScheduler:onBackToMultipleWorkSchedule', this.scheduler.viewStartDate);
    },

    /**
     * Compare current selected craftsperson with other craftsperson.
     */
    craftspersonForm_onCompare: function() {
        // compared craftsperson should not be empty.
        this.compareToCraftperson = this.craftspersonForm.getFieldValue('cf.cf_id');
        this.loadCraftspersonSchedule();
    },

    /**
     * Clear the compared craftsperson field value and re-load the scheduler control.
     */
    craftspersonForm_onClear: function() {
        this.craftspersonForm.clear();
        this.compareToCraftperson = "";
        this.loadCraftspersonSchedule();
    },

    /**
     * Get end date of this week.
     */
    getEndDateOfThisWeek: function() {
        var endDate = null;
        var currentDate = new Date();
        if (currentDate.getDay() == '0') {
            endDate = currentDate();
        } else {
            var i = 1;
            while (i < 7) {
                var endWeekDate = currentDate.add(Date.DAY, i);
                if (endWeekDate.getDay() == '0') {
                    endDate = endWeekDate;
                    break;
                }
                i++;
            }
        }
        return endDate;
    },

    /**
     * Add ondragstart and ondragend event listener when unscheduled list refresh.
     */
    unscheduledWorkList_afterRefresh: function() {
        var me = this;
        this.unscheduledTaskListItems.length = 0;
        var rows = this.unscheduledWorkList.gridRows.items;
        _.each(rows, function(row) {
            var rowRecord = row.getRecord();
            var wrId = rowRecord.getValue('wrcf.wr_id');
            var cfId = rowRecord.getValue('wrcf.cf_id');
            var description = rowRecord.getValue('wr.description');
            var dateAssigned = rowRecord.getValue('wrcf.date_assigned').getTime();
            var timeAssigned = rowRecord.getValue('wrcf.time_assigned').getTime();
            var dueDate=rowRecord.getValue('wr.date_escalation_completion');
            var estimatedHours = rowRecord.getValue('wrcf.hours_est');
            if(dueDate.isDate){
                dueDate=dueDate.getTime();
            }else{
                dueDate='';
            }
            row.dom.draggable = true;
            row.dom.setAttribute('ondragstart', "startDragUnscheduleListItem(event,'" + wrId + "','" + cfId + "'," + dateAssigned + "," + timeAssigned + ",'"+dueDate+"','"+estimatedHours+"')");
            row.dom.setAttribute('ondragend', "endDragUnscheduleListItem(event,'" + wrId + "')");

            // cache unscheduled task list items in first page, used to get information faster.
            me.unscheduledTaskListItems.push({
                wr_id: wrId,
                cf_id: cfId,
                description: description
            });
        });
        
        //Add statistics at the bottom of the grid.
        if(this.unscheduledWorkList.totals.getValue('wrcf.count_of_records') !=='0'){
            this.appendGridFootElement(this.unscheduledWorkList);
        }
    },
    
    /**
     * Add ondragstart and ondragend event listener when unscheduled list refresh.
     */
    missedWorkList_afterRefresh: function() {
        var me = this;
        this.missedWorkListItems.length = 0;
        var rows = this.missedWorkList.gridRows.items;
        _.each(rows, function(row) {
            var rowRecord = row.getRecord();
            var wrId = rowRecord.getValue('wrcf.wr_id');
            var cfId = rowRecord.getValue('wrcf.cf_id');
            var description = rowRecord.getValue('wr.description');
            var dateAssigned = rowRecord.getValue('wrcf.date_assigned').getTime();
            var timeAssigned = rowRecord.getValue('wrcf.time_assigned').getTime();
            var dueDate=rowRecord.getValue('wr.date_escalation_completion');
            var isScheduled=rowRecord.getValue('wrcf.is_scheduled');
            var estimatedHours = rowRecord.getValue('wrcf.hours_est');
            if(dueDate.isDate){
                dueDate=dueDate.getTime();
            }else{
                dueDate='';
            }
            row.dom.draggable = true;
            row.dom.setAttribute('ondragstart', "startDragMissedWorkItem(event,'" + wrId + "','" + cfId + "'," + dateAssigned + "," + timeAssigned + ",'"+dueDate+"','"+isScheduled+"','"+estimatedHours+"')");
            row.dom.setAttribute('ondragend', "endDragMissedWorkItem(event,'" + wrId + "')");

            // cache unscheduled task list items in first page, used to get information faster.
            me.missedWorkListItems.push({
                wr_id: wrId,
                cf_id: cfId,
                description: description
            });
        });
        
        //Add statistics at the bottom of the grid.
        if(this.missedWorkList.totals.getValue('wrcf.count_of_records') !=='0'){
            this.appendGridFootElement(this.missedWorkList);
        } 
    },
    
    /**
     * Append customized element to grid foot element.
     */
    appendGridFootElement: function(gridPanel){
        var totalEstimateHours=gridPanel.totals.values['wrcf.sum_hours_est'];
        var totalRecordCount=gridPanel.totals.values['wrcf.count_of_records'];
        var footElement=jQuery(gridPanel.tableFootElement);
        Ext.get(gridPanel.id+'_count').dom.style.display="none";
        var totalRecordsTDElement=jQuery('<span>').text(getMessage('totalRecords').replace('{0}',totalRecordCount));
        var estimateHoursSpaceElement=jQuery('<span>').attr('width','40px').html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
        var estimateHoursTDElement=jQuery('<span>').text(getMessage('totalEstimateHours').replace('{0}',formatNumberValue(totalEstimateHours)));
        var estimateHoursElement=jQuery("<td>").attr('class','count').attr('id','unschedule_total_estimate_hours').append(totalRecordsTDElement).append(estimateHoursSpaceElement).append(estimateHoursTDElement);
        var totalEstimateTRElement=jQuery("<tr>").append(estimateHoursElement)
        totalEstimateTRElement.appendTo(footElement);
    },

    /**
     * Render craftsperson statistics message.
     */
    renderStataisticsMessage: function() {
        var me = this;
        var craftspersons = this.scheduler.model.craftspersons;
        jQuery('#statisticsComponent').html('');
        var today = new Date();
            today = new Date(today.getFullYear(),today.getMonth(),today.getDate(),0,0,0);
        if(this.scheduler.viewEndDate.getTime() >= today.getTime()){
            _.each(craftspersons, function(craftsperson) {
                var component = me.getSingleCraftspersonStatisticsContent(craftsperson);
                component.appendTo(jQuery('#statisticsComponent'));
            });
        }
    },
    
    /**
     * Get single craftsperson statistics content.
     * @param craftsperson
     */
    getSingleCraftspersonStatisticsContent: function(craftsperson){
        var component = jQuery('<div>').addClass('statisticsComponent');
        var viewDateRange = this.getSchedulerViewDate(this.scheduler.viewStartDate, this.scheduler.viewEndDate);
        var craftspersonName=valueExistsNotEmpty(craftsperson.text)?craftsperson.text:craftsperson.id;
        jQuery("<div>").addClass('statisticsTitle').text(craftspersonName).appendTo(component);
        jQuery("<div>").text(viewDateRange).appendTo(component);
        if(craftsperson.totals.availableHours <= 0){
            jQuery("<div>").text(getMessage('scheduledHoursMessage').replace('{0}', formatNumberValue(craftsperson.totals.scheduledHours))).appendTo(component);
            var totalUnscheduledHours = craftsperson.totals.assignedUnscheduledHours + craftsperson.totals.proratedUnscheduledHours;
            jQuery("<div>").text(getMessage('unscheduledHoursDueThisPeriodMessage').replace('{0}', formatNumberValue(craftsperson.totals.assignedUnscheduledHours))).appendTo(component);
        }else{
            jQuery("<div>").text(getMessage('availableHoursMessage').replace('{0}', formatNumberValue(craftsperson.totals.availableHours))).appendTo(component);
            jQuery("<div>").text(getMessage('scheduledHoursMessage').replace('{0}', formatNumberValue(craftsperson.totals.scheduledHours))).appendTo(component);
            var totalUnscheduledHours = Number(craftsperson.totals.assignedUnscheduledHoursWithProrateHours.toFixed(2));
            jQuery("<div>").text(getMessage('assignedUnscheduledHoursMessage').replace('{0}', formatNumberValue(totalUnscheduledHours)).replace('{1}',formatNumberValue(craftsperson.totals.assignedUnscheduledHours))).appendTo(component);
            //jQuery("<div>").text(getMessage('proratedUnscheduledHoursMessage').replace("{0}",craftsperson.totals.proratedUnscheduledHours)).appendTo(component);
            jQuery("<div>").text(getMessage('remainingHoursMessage').replace("{0}",formatNumberValue(craftsperson.totals.remainingHours))).appendTo(component);
            var percentAllocateText='N/A';
            if(craftsperson.totals.percentAllocated != 'N/A'){
                percentAllocateText=craftsperson.totals.percentAllocated + '%';
            }
            jQuery("<div>").text(getMessage('percentAllocatedMessage').replace("{0}",percentAllocateText)).appendTo(component);
        }
        
        
        return component;
    },

    /**
     * Triggered after work requests updated.
     * @param{appointmentData} Appointment Data.
     */
    afterWorkRequestsUpdated: function(task) {
        var craftsperson = this.scheduler.model.craftspersonsById[task.cfId];
        if(craftsperson){
            this.scheduler.model.updateCraftspersonTotals(craftsperson, this.scheduler.model.startDate, this.scheduler.model.endDate);
            // render statistics message.
            this.renderStataisticsMessage();
        }
        //APP-1501: Refresh selected work requests if opener view is Ops Console View.
        this.refreshOpsConsoleWrListItem();
    },

    /**
     * Triggered after work request scheduled by drag and drop.
     */
    afterWorkRequestScheduled: function(task) {
        var craftsperson = this.scheduler.model.craftspersonsById[task.cfId];
        if(craftsperson){
            this.scheduler.model.updateCraftspersonTotals(craftsperson, this.scheduler.model.startDate, this.scheduler.model.endDate);
            this.renderStataisticsMessage();
        }
        
        this.refreshCraftspersonUnscheduleTaskList();
        //APP-1501: Refresh selected work requests if opener view is Ops Console View.
        this.refreshOpsConsoleWrListItem();
    },

    /**
     * Triggered after work request deleted.
     */
    afterWorkRequestDeleted: function(task) {
        var craftsperson = this.scheduler.model.craftspersonsById[task.cfId];
        if(craftsperson){
            this.scheduler.model.updateCraftspersonTotals(craftsperson, this.scheduler.model.startDate, this.scheduler.model.endDate);
            this.renderStataisticsMessage();
        }
        this.refreshCraftspersonUnscheduleTaskList();
        //APP-1501: Refresh selected work requests if opener view is Ops Console View.
        this.refreshOpsConsoleWrListItem();
    },
    
    /**
     * APP-Refresh work requests list item in ops console view.
     */
    refreshOpsConsoleWrListItem: function(){
        var openerOpsConsoleView = View.getOpenerView().getOpenerView();
        if(openerOpsConsoleView){
            var opsConsoleWrFilterController = openerOpsConsoleView.controllers.get('wrFilter');
            if(opsConsoleWrFilterController){
                opsConsoleWrFilterController.wrFilter_onFilter();
            }
            
            var opsConsoleWrListActionController = openerOpsConsoleView.controllers.get('opsConsoleWrListActionController');
            if(opsConsoleWrListActionController){
                opsConsoleWrListActionController.keepReqeustsSelectedAfterRefresh();
            }
        }
    },

    /**
     * Get scheduler control view formatted start date and end date.
     */
    getSchedulerViewDate: function(viewStartDate, viewEndDate) {
        //APP-1272:The starting date for the date range that is displayed should be the current day.
        var today = new Date();
            today = new Date(today.getFullYear(),today.getMonth(),today.getDate(),0,0,0);
        if(today.between(viewStartDate,viewEndDate)){
            viewStartDate = today;
        }
        var formattedDate = null;
        this.scheduler.scheduler.fire("formatDates", {
            startDate: viewStartDate,
            endDate: viewEndDate,
            formatType: 'DATE',
            callback: function(result) {
                formattedDate = result
            }
        });

        return formattedDate;
    },

    /**
     * Save task details form fields.
     */
    onSaveTaskDetail: function(record) {
        var originalAppointmentData = this.scheduler.currentEditAppointmentData;
        var changedAppointmentData = _.clone(originalAppointmentData);
        
        var openedPanelId = originalAppointmentData.openedPanelId;

        var cfId = record.getValue("wrcf.cf_id");
        var originalCfId = originalAppointmentData.cfId;
        var dateAssigned = record.getValue('wrcf.date_assigned');
        var timeAssigned = record.getValue('wrcf.time_assigned');
        var estimatedHours = parseFloat(record.getValue('wrcf.hours_est'));
        var workType = record.getValue('wrcf.work_type');
        var comments = record.getValue('wrcf.comments');
        var description = record.getValue('wr.description');
        
        var isScheudled=record.getValue("wrcf.is_scheduled");
        var originalIsScheduled=record.oldValues["wrcf.is_scheduled"];
        var dueDate=record.getValue('wr.date_escalation_completion');
        var startDate = new Date(dateAssigned.getFullYear(), dateAssigned.getMonth(), dateAssigned.getDate(), timeAssigned.getHours(), timeAssigned.getMinutes(), timeAssigned.getSeconds());
        changedAppointmentData.startDate = startDate;
        var endDate = startDate.add(Date.SECOND, estimatedHours * 60 * 60);
        changedAppointmentData.endDate = endDate;
        changedAppointmentData.cfId = cfId;
        changedAppointmentData.estimatedHours = estimatedHours;
        changedAppointmentData.comments = comments;
        changedAppointmentData.workType = workType;
        changedAppointmentData.dueDate = dueDate;
        
        if(isScheudled=='0' && originalIsScheduled=='1'){
            originalAppointmentData.actionType='unscheduled';
            this.scheduler.scheduler.deleteAppointment(originalAppointmentData);
            
        }else if(isScheudled=='1' && originalIsScheduled=='0'){
            var validate = validateAppointment(changedAppointmentData);
            if(validate){
                changedAppointmentData.oldCfId=originalAppointmentData.cfId;
                changedAppointmentData.oldWrId=originalAppointmentData.wrId;
                changedAppointmentData.oldDateAssigned=originalAppointmentData.startDate;
                changedAppointmentData.oldTimeAssigned=originalAppointmentData.startDate;
                changedAppointmentData.taskType='TASK';
                changedAppointmentData.description=Ext.util.Format.ellipsis(description, 90);
                this.scheduler.model.deleteUnscheduledTask(changedAppointmentData);
                this.scheduler.scheduler.addAppointment(changedAppointmentData);
            }
            
        }else if(isScheudled == '0'){
            //update unscheduled tasks.
            this.scheduler.model.updateTask(changedAppointmentData,originalAppointmentData);
            var craftsperson = this.scheduler.model.craftspersonsById[cfId];
            if(craftsperson){
                this.afterWorkRequestsUpdated(changedAppointmentData);
            }
            
            if(originalCfId!==cfId){
                var originalCraftsperson = this.scheduler.model.craftspersonsById[originalCfId];
                //this.scheduler.model.updateCraftspersonTotals(originalCraftsperson, this.scheduler.model.startDate, this.scheduler.model.endDate);
                if(originalCraftsperson){
                   this.afterWorkRequestsUpdated(originalAppointmentData);
                }
            }
            
        }else {
            // update appointment.
            if(openedPanelId == 'missedWorkList'){
                changedAppointmentData.taskType='TASK';
                changedAppointmentData.description=Ext.util.Format.ellipsis(description, 90);
                var validate = validateAppointment(changedAppointmentData);
                if(validate){
                    this.scheduler.scheduler.addAppointment(changedAppointmentData);
                    this.scheduler.model.setAssignmentCompleted(originalAppointmentData.wrId,originalAppointmentData.cfId,originalAppointmentData.startDate,originalAppointmentData.startDate,originalIsScheduled);
                }
                
            }else{
                this.scheduler.scheduler.updateAppointment(originalAppointmentData, changedAppointmentData);
            }
            if(originalCfId!==cfId){
                var originalCraftsperson = this.scheduler.model.craftspersonsById[originalCfId];
                //this.scheduler.model.updateCraftspersonTotals(originalCraftsperson, this.scheduler.model.startDate, this.scheduler.model.endDate);
                if(originalCraftsperson){
                   this.afterWorkRequestsUpdated(originalAppointmentData);
                }
            }
        }
        
        this.afterWorkRequestScheduled(changedAppointmentData);
    },
    
    /**
     * copy task to create a new task.
     */
    onCopyNewTask: function(record){
        var cfId=record.getValue('wrcf.cf_id');
        var wrId=record.getValue('wrcf.wr_id');
        var workType = record.getValue('wrcf.work_type');
        var comments = record.getValue('wrcf.comments');
        var description=record.getValue('wr.description');
        var dateAssigned =record.getValue('wrcf.date_assigned');
        var timeAssigned =record.getValue('wrcf.time_assigned');
        var estimatedHours = parseFloat(record.getValue('wrcf.hours_est'));
        var isScheudled=record.getValue("wrcf.is_scheduled");
        var dueDate=record.getValue('wr.date_escalation_completion');
        var startDate = new Date(dateAssigned.getFullYear(), dateAssigned.getMonth(), dateAssigned.getDate(), timeAssigned.getHours(), timeAssigned.getMinutes(), timeAssigned.getSeconds());
        var endDate = startDate.add(Date.SECOND, estimatedHours * 60 * 60);
        var task = {
                cfId: cfId,
                wrId: wrId,
                description: description,
                comments: comments,
                workType: workType,
                startDate: startDate,
                endDate: endDate,
                dueDate: dueDate,
                estimatedHours: estimatedHours,
                taskType: 'TASK'
        };
        
        if(isScheudled == '1'){
            this.scheduler.scheduler.addAppointment(task);
        }
        if(isScheudled == '0'){
            var editOptions = getWrcfRecordEditOptions(task);
            if(editOptions.allowAdding){
                var craftsperson = this.scheduler.model.craftspersonsById[cfId];
                this.scheduler.model.createUnscheduledTask(task);
                if(craftsperson){
                    this.afterWorkRequestScheduled(task);
                }
            }else{
                View.alert(editOptions.addAlertText);
            }
        }
    },
    
    /**
     * open assignment schedule detail pop-up.
     */
    openAssignmentDetailPopup: function(panelId){
        var panel = View.panels.get(panelId);
        var selectedRowIndex=panel.selectedRowIndex;
        var selectedRowRecord=panel.gridRows.get(selectedRowIndex).getRecord();
        var wrId=selectedRowRecord.getValue('wrcf.wr_id');
        var cfId=selectedRowRecord.getValue('wrcf.cf_id');
        var dateAssigned=this.unscheduledWorkList.getDataSource().formatValue('wrcf.date_assigned',selectedRowRecord.getValue('wrcf.date_assigned'),false);
        var timeAssigned=this.unscheduledWorkList.getDataSource().formatValue('wrcf.time_assigned',selectedRowRecord.getValue('wrcf.time_assigned'),false);
        var res = new Ab.view.Restriction();
        res.addClause('wrcf.wr_id', wrId, '=');
        res.addClause('wrcf.cf_id', cfId, '=');
        res.addClause('wrcf.date_assigned', dateAssigned, '=');
        res.addClause('wrcf.time_assigned', timeAssigned, '=');
        
        this.scheduler.currentEditAppointmentData=this.getOriginalAppointment(selectedRowRecord);
        //set a flag to indicate where the pop-up panel opened from.
        this.scheduler.currentEditAppointmentData.openedPanelId = panelId;
        //get edit options.
        var editOptions = getWrcfRecordEditOptions(this.scheduler.currentEditAppointmentData);
        
        View.openDialog('ab-bldgops-labor-scheduler-timeline-task-detail-popup.axvw',null,false,{
            wrId:wrId,
            //use this parameter to indicate which scheduler open the task detail pop-up.
            schedulerType:'signleCraftspersonScheudler',
            editOptions : editOptions,
            taskRestriction:res,
            //Panel id that dialog opened from.
            openedPanelId: panelId
        });
    },
    
    /**
     * Get original appointment by selected row record.
     * 
     * @param selectedRowRecord - Selected row record.
     */
    getOriginalAppointment: function(selectedRowRecord){
        var dateAssigned=selectedRowRecord.getValue('wrcf.date_assigned');
        var timeAssigned=selectedRowRecord.getValue('wrcf.time_assigned');
        var dueDate=selectedRowRecord.getValue('wr.date_escalation_completion');
        var estimatedHours = selectedRowRecord.getValue('wrcf.hours_est');
        var startDate=new Date(dateAssigned.getFullYear(), dateAssigned.getMonth(), dateAssigned.getDate(), timeAssigned.getHours(), timeAssigned.getMinutes(), timeAssigned.getSeconds());
        var endDate = startDate.add(Date.SECOND,estimatedHours*60*60);
        var task ={
                cfId: selectedRowRecord.getValue('wrcf.cf_id'),
                wrId: selectedRowRecord.getValue('wrcf.wr_id'),
                startDate: startDate,
                endDate: endDate,
                dueDate: dueDate
        }
        
        return task;
    },
    
    /**
     * Remove unschedule task list item.
     */
    onRemoveWorkTaskListItem: function(panelId){
        var me = this;
        var panel = View.panels.get(panelId);
        var selectedIndex = panel.selectedRowIndex;
        var selectedRowRecord = panel.gridRows.get(selectedIndex).getRecord();
        View.confirm(getMessage('removeSelectedTaskMessage'),function(button){
            if(button=='yes'){
                var wrId=selectedRowRecord.getValue('wrcf.wr_id');
                var cfId=selectedRowRecord.getValue('wrcf.cf_id');
                var dateAssigned=selectedRowRecord.getValue('wrcf.date_assigned');
                var timeAssigned=selectedRowRecord.getValue('wrcf.time_assigned');
                var dueDate=selectedRowRecord.getValue('wr.date_escalation_completion');
                var isScheduled = selectedRowRecord.getValue('wrcf.is_scheduled');
                
                var startDate=new Date(dateAssigned.getFullYear(),dateAssigned.getMonth(),dateAssigned.getDate(),timeAssigned.getHours(),timeAssigned.getMinutes(),timeAssigned.getSeconds());
                var deletedTask= {
                        wrId: wrId,
                        cfId: cfId,
                        startDate: startDate,
                        oldWrId: wrId,
                        oldCfId: cfId,
                        oldDateAssigned: dateAssigned,
                        oldTimeAssigned: timeAssigned,
                        dueDate: dueDate
                }
                
                //get edit options.
                var editOptions = getWrcfRecordEditOptions(deletedTask);
                if(!editOptions.allowDeleting){
                    View.alert(editOptions.deleteAlertText);
                    return;
                }
                
                craftspersonScheduleCtrl.scheduler.model.deleteUnscheduledTask(deletedTask);
                craftspersonScheduleCtrl.afterWorkRequestDeleted(deletedTask);
            }
        });
    },
    
    /**
     * Remove scheduled assignment.
     */
    onRemoveAssignment: function(isScheduled){
        var task = this.scheduler.currentEditAppointmentData;
        if(isScheduled == '1'){
            this.scheduler.scheduler.deleteAppointment(task);
        }
        
        if(isScheduled == '0'){
            var dueDate = this.scheduler.model.getDueDateByWrId(task.wrId);
            var deletedTask= {
                    wrId: task.wrId,
                    cfId: task.cfId,
                    startDate: task.startDate,
                    oldWrId: task.wrId,
                    oldCfId: task.cfId,
                    oldDateAssigned: task.startDate,
                    oldTimeAssigned: task.startDate,
                    dueDate: dueDate
            }
            craftspersonScheduleCtrl.scheduler.model.deleteUnscheduledTask(deletedTask);
            craftspersonScheduleCtrl.afterWorkRequestDeleted(deletedTask);
        }
    }
});
/**
 * Starts drag operation for unschedule work request.
 * @param wrId
 * @param description
 */
function startDragUnscheduleListItem(event, wrId, cfId, dateAssigned, timeAssigned, dueDate, estimatedHours) {
    var unscheduledTask = _.find(craftspersonScheduleCtrl.unscheduledTaskListItems, function(wr) {
        return wr.wr_id === '' + wrId;
    });
    var transferData={
            dragType:  'unschedule',
            wr_id: wrId,
            cfId: cfId,
            description: unscheduledTask.description,
            dateAssigned: dateAssigned,
            timeAssigned: timeAssigned,
            dueDate: dueDate,
            estimatedHours: estimatedHours
    }
    //Use JSON string to transfer data, this approach can avoid error on IE browser, because IE browser can't support passing multiple object properties via the drag transfer object.
    transferData = JSON.stringify(transferData);
    event.dataTransfer.setData('text', transferData);
    
    //IE browser does not support custom drag image.
    if(event.dataTransfer.setDragImage){
        var dragEl = jQuery('<div id="unscheduleTaskDragImage_' + wrId + '" class="unscheduleListItemDragImage">Work request:</br> ' + wrId + '</div>')[0];
        event.currentTarget.appendChild(dragEl);
        event.dataTransfer.setDragImage(dragEl, 0, 0);
    }
};

/**
 * Starts drag operation for unschedule work request.
 * @param wrId
 * @param description
 */
function startDragMissedWorkItem(event, wrId, cfId, dateAssigned, timeAssigned, dueDate, isScheduled,estimatedHours) {
    var unscheduledTask = _.find(craftspersonScheduleCtrl.missedWorkListItems, function(wr) {
        return wr.wr_id === '' + wrId;
    });
    var transferData={
            dragType:  'missedWork',
            wr_id: wrId,
            cfId: cfId,
            description: unscheduledTask.description,
            dateAssigned: dateAssigned,
            timeAssigned: timeAssigned,
            dueDate: dueDate,
            isScheduled: isScheduled,
            estimatedHours: estimatedHours
    }
    //Use JSON string to transfer data, this approach can avoid error on IE browser, because IE browser can't support passing multiple object properties via the drag transfer object.
    transferData = JSON.stringify(transferData);
    event.dataTransfer.setData('text', transferData);
    
    //IE browser does not support custom drag image.
    if(event.dataTransfer.setDragImage){
        var dragEl = jQuery('<div id="unscheduleTaskDragImage_' + wrId + '" class="unscheduleListItemDragImage">Work request:</br> ' + wrId + '</div>')[0];
        event.currentTarget.appendChild(dragEl);
        event.dataTransfer.setDragImage(dragEl, 0, 0);
    }
};

/**
 * Ends drag operation for unschedule work request.
 * @param wrId
 */
function endDragUnscheduleListItem(event, wrId) {
    jQuery('#unscheduleTaskDragImage_' + wrId).remove();
}

/**
 * Ends drag operation for missed work item.
 * @param wrId
 */
function endDragMissedWorkItem(event, wrId) {
    jQuery('#unscheduleTaskDragImage_' + wrId).remove();
}

/**
 * After select compared craftsperson, then reload scheduler control.
 * @param fieldName
 * @param newValue
 * @param oldValue
 * @returns
 */
function afterSelectComparedCraftsperson(fieldName, newValue, oldValue) {
    if (fieldName == 'cf.cf_id') {
        if (newValue != craftspersonScheduleCtrl.compareToCraftperson) {
            craftspersonScheduleCtrl.compareToCraftperson = newValue;
            craftspersonScheduleCtrl.loadCraftspersonSchedule();
        }
    }
}
