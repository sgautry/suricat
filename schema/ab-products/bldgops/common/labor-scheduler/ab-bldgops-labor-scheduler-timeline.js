/**
 * The Labor Scheduler controller.
 */
var workScheduleCtrl=View.createController('laborSchedulingBoard-workSchedule', {

    /**
     * The WorkScheduleControl instance.
     */
    scheduler: null,
    
    /**
     * Scheduler view default start date, by default is current date.
     */
    defaultStartDate : new Date(),

    afterViewLoad: function() {
        var locale = View.user.locale.slice(0, 2);
        DevExpress.localization.locale(locale);

        //override core tabs API to hide tabs header element and update tab content height.
        this.hideTabsHeaderAndUpdateContentHeight();

        this.scheduler = View.createControl({
            control: 'WorkScheduleControl',
            container: 'workScheduleContainer',
            panel: 'workSchedule',
            maxScheduledJobsPerPersonPerDay: 3,
            model: new WorkScheduleModel({
                maxScheduledJobsPerPersonPerDay: 3
            }),
            startDate: this.defaultStartDate
        });
        this.on('app:bldgops:laborScheduler:showWorkSchedule', this.showWorkSchedule);
        this.on('app:bldgops:laborScheduler:onBackToMultipleWorkSchedule', this.onBackToMultipleWorkSchedule);
        this.on('app:bldgops:laborScheduler:afterWorkRequestUpdated', this.afterWorkRequestsUpdated);
        this.on('app:bldgops:laborScheduler:afterWorkRequestScheduled', this.afterWorkRequestScheduled);
        this.on('app:bldgops:laborScheduler:afterWorkRequestDeleted', this.afterWorkRequestDeleted);
        
        this.on('app:bldgops:laborScheduler:onSaveTaskDetail',this.onSaveTaskDetail);
        this.on('app:bldgops:laborScheduler:onCopyNewTask',this.onCopyNewTask);
        this.on('app:bldgops:laborScheduler:onRemoveAssignment',this.onRemoveAssignment);
        
        this.scheduler.loadSchedule(this.defaultStartDate);
    },

    /**
     * Queries and displays work schedule for specified restriction.
     * @param restriction
     */
    showWorkSchedule: function(restriction) {
        this.scheduler.restriction = restriction;
        this.scheduler.loadSchedule();
    },

    /**
     * Filters the list of work requests.
     */
    workScheduleFilter_onFilterWorkSchedule: function() {
        this.setWorkScheduleFitlerRestrictions();
        var addtionalRestriction = null;
        this.trigger('app:bldgops:laborScheduler:showWorkSchedule', addtionalRestriction);
    },
    
    /**
     * Set restrictions to craftsperson datasource for work schedule control.
     */
    setWorkScheduleFitlerRestrictions: function(){
        this.clearCraftspersonDataSourceParameters();
        
        var cfId=this.workScheduleFilter.getFieldValue("cf.cf_id");
        var trId=this.workScheduleFilter.getFieldValue('cf.tr_id');
        var workTeamId=this.workScheduleFilter.getFieldValue('cf.work_team_id');
        var siteId=this.workScheduleFilter.getFieldValue('wr.site_id');
        var blId=this.workScheduleFilter.getFieldValue('wr.bl_id');
        
        if(valueExistsNotEmpty(cfId)){
            this.craftspersonsDataSource.addParameter("hasCraftspersonCode",true);
            this.craftspersonsDataSource.addParameter("craftspersonCode",cfId);
        }
        
        if(valueExistsNotEmpty(trId)){
            this.craftspersonsDataSource.addParameter("hasTradeCode",true);
            this.craftspersonsDataSource.addParameter("tradeCode",trId);
        }
        
        if(valueExistsNotEmpty(workTeamId)){
            this.craftspersonsDataSource.addParameter("hasWorkTeamId",true);
            this.craftspersonsDataSource.addParameter("workTeamId",workTeamId);
        }
        
        if(valueExistsNotEmpty(siteId) || valueExistsNotEmpty(blId)){
            this.craftspersonsDataSource.addParameter("hasSiteOrBuilding",true);
            this.craftspersonsDataSource.addParameter("siteId",siteId);
            var viewStartDate=this.scheduler.startDate;
            var formattedStartDate=getIsoFormatDate(viewStartDate);
            var viewEndDate=this.scheduler.endDate.add(Date.DAY,1);
            var formattedEndDate=getIsoFormatDate(viewEndDate);
            this.craftspersonsDataSource.addParameter("viewStart",formattedStartDate);
            this.craftspersonsDataSource.addParameter("viewEnd",formattedEndDate);
            
            if(valueExistsNotEmpty(siteId)){
                this.craftspersonsDataSource.addParameter("hasSiteId",true);
                this.craftspersonsDataSource.addParameter("siteId",siteId);
            }else{
                this.craftspersonsDataSource.addParameter("hasSiteId",false);
            }
            
            if(valueExistsNotEmpty(blId)){
                this.craftspersonsDataSource.addParameter("hasBuildingId",true);
                this.craftspersonsDataSource.addParameter("buildingId",blId);
            }else{
                this.craftspersonsDataSource.addParameter("hasBuildingId",false);
            }
        }
    },
    
    /**
     * Clear craftsperson datasource parameters.
     */
    clearCraftspersonDataSourceParameters: function(){
        this.craftspersonsDataSource.addParameter("hasCraftspersonCode",false);
        this.craftspersonsDataSource.addParameter("hasTradeCode",false);
        this.craftspersonsDataSource.addParameter("hasWorkTeamId",false);
        this.craftspersonsDataSource.addParameter("hasSiteOrBuilding",false);
    },

    /**
     * Clears the filter applied to the list of work requests.
     */
    workScheduleFilter_onClearWorkSchedule: function() {
        this.clearCraftspersonDataSourceParameters();
        this.workScheduleFilter.clear();
        this.trigger('app:bldgops:laborScheduler:showWorkSchedule', null);
    },

    /**
     * Event handler on back from single craftsperson schedule view to multiple craftsperson schedule view.
     */
    onBackToMultipleWorkSchedule: function(selectedDate) {
        this.defaultStartDate=selectedDate;
        this.workScheduleTabs.selectTab('workScheduleTab', null);
        this.scheduler.scheduler.option('currentDate',this.defaultStartDate);
    },

    /**
     * Trigger after assigned task updated.
     */
    afterWorkRequestsUpdated: function(task) {
        var craftsperson = this.scheduler.model.craftspersonsById[task.cfId];
        this.updateCraftsperson(craftsperson);
        //APP-1501: Refresh selected work requests if opener view is Ops Console View.
        this.refreshOpsConsoleWrListItem();
    },

    /**
     * Trigger after work request record schedule.
     */
    afterWorkRequestScheduled: function(task) {
        var craftsperson = this.scheduler.model.craftspersonsById[task.cfId];
        this.updateCraftsperson(craftsperson);
        //APP-1501: Refresh selected work requests if opener view is Ops Console View.
        this.refreshOpsConsoleWrListItem();
    },

    /**
     * Trigger after assigned task deleted.
     */
    afterWorkRequestDeleted: function(task) {
        var craftsperson = this.scheduler.model.craftspersonsById[task.cfId];
        this.updateCraftsperson(craftsperson);
        //APP-1501: Refresh selected work requests if opener view is Ops Console View.
        this.refreshOpsConsoleWrListItem();
    },
    
    /**
     * Refresh work requests list item in ops console view.
     */
    refreshOpsConsoleWrListItem: function(){
        var openerOpsConsoleView = View.getOpenerView();
        if(openerOpsConsoleView){
            var opsConsoleWrListController = openerOpsConsoleView.controllers.get('opsConsoleWrListController');
            if(opsConsoleWrListController){
                opsConsoleWrListController.resetSelectedRowIcons();
            }
            
            if(openerOpsConsoleView.panels.get('wrList')){
                openerOpsConsoleView.panels.get('wrList').refresh();
            }
            
            
            var opsConsoleWrListActionController = openerOpsConsoleView.controllers.get('opsConsoleWrListActionController');
            if(opsConsoleWrListActionController){
                opsConsoleWrListActionController.keepReqeustsSelectedAfterRefresh();
            }
        }
    },

    /**
     * update craftsperson totals and render craftsperson statistics after work requests updated.
     * @param{cfId} Craftsperson who need to be updated.
     */
    updateCraftsperson: function(craftsperson) {
        if(craftsperson){
            this.scheduler.model.updateCraftspersonTotals(craftsperson, this.scheduler.startDate, this.scheduler.endDate);
            this.scheduler.renderCraftsperson(craftsperson);
        }
    },

    /**
     * Save task details form fields.
     */
    onSaveTaskDetail: function(record) {
        var originalAppointmentData = this.scheduler.currentEditAppointmentData;
        var changedAppointmentData = _.clone(originalAppointmentData);

        var cfId = record.getValue("wrcf.cf_id");
        var originalCfId = originalAppointmentData.cfId;
        var dateAssigned =record.getValue('wrcf.date_assigned');
        var timeAssigned =record.getValue('wrcf.time_assigned');
        var estimatedHours = parseFloat(record.getValue('wrcf.hours_est'));
        var workType = record.getValue('wrcf.work_type');
        var comments = record.getValue('wrcf.comments');
        var isScheudled=record.getValue("wrcf.is_scheduled");
        var originalIsScheduled=record.oldValues["wrcf.is_scheduled"];
        var dueDate=record.getValue('wr.date_escalation_completion');
        var startDate = new Date(dateAssigned.getFullYear(), dateAssigned.getMonth(), dateAssigned.getDate(), timeAssigned.getHours(), timeAssigned.getMinutes(), timeAssigned.getSeconds());
        changedAppointmentData.startDate = startDate;
        var endDate = startDate.add(Date.SECOND, estimatedHours * 60 * 60);
        changedAppointmentData.endDate = endDate;
        changedAppointmentData.estimatedHours = estimatedHours;
        changedAppointmentData.comments = comments;
        changedAppointmentData.workType = workType;
        changedAppointmentData.cfId = cfId;
        changedAppointmentData.dueDate = dueDate;
        
        if(isScheudled=='0' && originalIsScheduled=='1'){
            //change scheduled to unscheduled.
            originalAppointmentData.actionType='unscheduled';
            this.scheduler.scheduler.deleteAppointment(originalAppointmentData);
        }else if(isScheudled=='1' && originalIsScheduled=='0'){
            var validate = validateAppointment(changedAppointmentData);
            if(validate){
                changedAppointmentData.oldCfId=originalAppointmentData.cfId;
                changedAppointmentData.oldWrId=originalAppointmentData.wrId;
                changedAppointmentData.oldDateAssigned=originalAppointmentData.startDate;
                changedAppointmentData.oldTimeAssigned=originalAppointmentData.startDate;
                changedAppointmentData.taskType='TASK';
                changedAppointmentData.description='';
                this.scheduler.model.deleteUnscheduledTask(changedAppointmentData);
                this.scheduler.scheduler.addAppointment(changedAppointmentData);
            }
        }else if(isScheudled == '0'){
            //update unscheduled tasks.
            this.scheduler.model.updateTask(changedAppointmentData,originalAppointmentData);
            var craftsperson = this.scheduler.model.craftspersonsById[cfId];
            if(craftsperson){
                this.scheduler.renderCraftsperson(craftsperson);
            }
            
            if(originalCfId!==cfId){
                var originalCraftsperson = this.scheduler.model.craftspersonsById[originalCfId];
                //this.scheduler.model.updateCraftspersonTotals(originalCraftsperson, this.scheduler.model.startDate, this.scheduler.model.endDate);
                if(originalCraftsperson){
                    this.scheduler.renderCraftsperson(originalCraftsperson);
                }
            }
            
        }else{
            this.scheduler.scheduler.updateAppointment(originalAppointmentData, changedAppointmentData);
            //update original craftsperson statistic.
            if(originalCfId!==cfId){
                var originalCraftsperson = this.scheduler.model.craftspersonsById[originalCfId];
                //this.scheduler.model.updateCraftspersonTotals(originalCraftsperson, this.scheduler.model.startDate, this.scheduler.model.endDate);
                if(originalCraftsperson){
                    this.scheduler.renderCraftsperson(originalCraftsperson);
                }
            }
        }
    },
    
    /**
     * copy task to create a new task.
     */
    onCopyNewTask: function(record){
        var cfId=record.getValue('wrcf.cf_id');
        var wrId=record.getValue('wrcf.wr_id');
        var workType = record.getValue('wrcf.work_type');
        var comments = record.getValue('wrcf.comments');
        var description=record.getValue('wr.description');
        var dateAssigned =record.getValue('wrcf.date_assigned');
        var timeAssigned =record.getValue('wrcf.time_assigned');
        var estimatedHours = parseFloat(record.getValue('wrcf.hours_est'));
        var isScheudled = record.getValue("wrcf.is_scheduled");
        var dueDate=record.getValue('wr.date_escalation_completion');
        var startDate = new Date(dateAssigned.getFullYear(), dateAssigned.getMonth(), dateAssigned.getDate(), timeAssigned.getHours(), timeAssigned.getMinutes(), timeAssigned.getSeconds());
        var endDate = startDate.add(Date.SECOND, estimatedHours * 60 * 60);
        var task = {
                cfId: cfId,
                wrId: wrId,
                description: description,
                comments: comments,
                workType: workType,
                startDate: startDate,
                endDate: endDate,
                estimatedHours: estimatedHours,
                dueDate: dueDate,
                taskType: 'TASK'
            };
        if(isScheudled == '1'){
            this.scheduler.scheduler.addAppointment(task);
        }
        
        if(isScheudled == '0'){
            var editOptions = getWrcfRecordEditOptions(task);
            if(editOptions.allowAdding){
                var craftsperson = this.scheduler.model.craftspersonsById[cfId];
                this.scheduler.model.createUnscheduledTask(task);
                if(craftsperson){
                    this.scheduler.renderCraftsperson(craftsperson);
                }
            }else{
                View.alert(editOptions.addAlertText);
            }
        }
    },
    
    /**
     * Remove scheduled assignment.
     */
    onRemoveAssignment: function(isScheduled){
        if(isScheduled == '1'){
            var originalAppointmentData = this.scheduler.currentEditAppointmentData;
            this.scheduler.scheduler.deleteAppointment(originalAppointmentData);
        }
        
        if(isScheduled == '0'){
            var originalAppointmentData = this.scheduler.currentEditAppointmentData;
            var dueDate = this.scheduler.model.getDueDateByWrId(originalAppointmentData.wrId);
            var deletedTask= {
                    wrId: originalAppointmentData.wrId,
                    cfId: originalAppointmentData.cfId,
                    startDate: originalAppointmentData.startDate,
                    oldWrId: originalAppointmentData.wrId,
                    oldCfId: originalAppointmentData.cfId,
                    oldDateAssigned: originalAppointmentData.startDate,
                    oldTimeAssigned: originalAppointmentData.startDate,
                    dueDate: dueDate
            }
            this.scheduler.model.deleteUnscheduledTask(deletedTask);
            this.afterWorkRequestDeleted(deletedTask);
        }
        
    },
    
    /**
     * Assign selected work request to unscheduled task.
     * @param workRequestsIds Ids of selected work requests.
     * @param cfId Craftsperson code.
     * @param estimateHours
     * @param workType
     */
    assignDetailPanel_onAssign: function(){
        var workRequestsController = View.controllers.get('laborSchedulingBoard-workRequests');
        var me=this;
        var cfId = this.assignDetailPanel.getFieldValue('wrcf.cf_id');
        var estimateHours = this.assignDetailPanel.getFieldValue('wrcf.hours_est');
        //Check whether Estimated Hours is empty or not, if it is empty, then get trade estimated hours by selected work reqeust and craftsperson trade.
        var isEstimateHoursEmpty = !valueExistsNotEmpty(estimateHours);
        var workType = this.assignDetailPanel.getFieldValue('wrcf.work_type');
        if(this.assignDetailPanel.canSave()){
            _.each(workRequestsController.selectedWorkRequests,function(workRequest){
                //APP-2123:If no hours are entered, Estimated Hours will be set to the value of Trade Estimations where the craftsperson's Primary Trade matches the trade of the Trade Estimation.
                if(isEstimateHoursEmpty){
                    estimateHours = me.getTradeEstimatedHours(workRequest.wr_id,cfId);
                }
                me.scheduler.model.assignWorkRequest(workRequest.wr_id, cfId, estimateHours, workType);
            });
            
            this.workScheduleTabs.selectTab('workScheduleTab', null);
            this.workScheduleFilter_onFilterWorkSchedule();
            this.assignDetailPanel.closeWindow();
        }
        
    },
    
    /**
     * Get primary trade estimated hours of the selected craftsperson.
     * Estimated Hours will be set to the value of Trade Estimations where the craftsperson's Primary Trade matches the trade of the Trade Estimation.
     * @param wrId - Work reqeust code. 
     * @param cfId - Craftsperson code.
     */
    getTradeEstimatedHours: function(wrId,cfId){
        var estimatedHours = 0;
        var cfRes = new Ab.view.Restriction();
            cfRes.addClause('cf.cf_id',cfId,'=');
        var cfRecord=this.allCraftspersonDs.getRecord(cfRes);
        var primaryTrade = cfRecord.getValue('cf.tr_id');
        if(valueExistsNotEmpty(primaryTrade)){
            estimatedHours = this.getWorkReqeustTradeEstimatedHours(wrId,primaryTrade);
        }
        
        return estimatedHours;
    },
    
    /**
     * Get estimated hours in wrtr table for specific work request and trade.
     * 
     * @param wrId - work request code.
     * @param trId - trade code.
     */
    getWorkReqeustTradeEstimatedHours: function(wrId,trId){
        var estimatedHours = 0;
        var wrtrRestriction = new Ab.view.Restriction();
        wrtrRestriction.addClause('wrtr.wr_id',wrId,'=');
        wrtrRestriction.addClause('wrtr.tr_id',trId,'=');
        var wrtrRecords = this.wrtrDs.getRecords(wrtrRestriction);
        if(wrtrRecords.length > 0){
            estimatedHours = wrtrRecords[0].getValue('wrtr.hours_est');
        }
        
        return estimatedHours;
    },
    
    /**
     * Close multiple assign detail pop-up.
     */
    assignDetailPanel_onCancel: function(){
        this.assignDetailPanel.closeWindow();
    },
    
    showTaskDetail: function(){
        var panel = View.panels.get('unscheduledTaskList');
        var selectedRowIndex=panel.selectedRowIndex;
        var selectedRowRecord=panel.gridRows.get(selectedRowIndex).getRecord();
        var wrId=selectedRowRecord.getValue('wrcf.wr_id');
        var cfId=selectedRowRecord.getValue('wrcf.cf_id');
        var dateAssigned=this.unscheduledTaskList.getDataSource().formatValue('wrcf.date_assigned',selectedRowRecord.getValue('wrcf.date_assigned'),false);
        var timeAssigned=this.unscheduledTaskList.getDataSource().formatValue('wrcf.time_assigned',selectedRowRecord.getValue('wrcf.time_assigned'),false);
        var res = new Ab.view.Restriction();
        res.addClause('wrcf.wr_id', wrId, '=');
        res.addClause('wrcf.cf_id', cfId, '=');
        res.addClause('wrcf.date_assigned', dateAssigned, '=');
        res.addClause('wrcf.time_assigned', timeAssigned, '=');
        
        this.scheduler.currentEditAppointmentData=this.getOriginalAppointment(selectedRowRecord);
        //get edit options.
        var editOptions = getWrcfRecordEditOptions(this.scheduler.currentEditAppointmentData);
        
        View.openDialog('ab-bldgops-labor-scheduler-timeline-task-detail-popup.axvw',null,false,{
            wrId:wrId,
            //use this parameter to indicate which scheduler open the task detail pop-up.
            schedulerType:'multipleCraftspersonScheudler',
            editOptions : editOptions,
            taskRestriction:res
        });
        
        this.unscheduledTaskList.closeWindow();
    },
    
    /**
     * Get original appointment by selected row record.
     */
    getOriginalAppointment: function(selectedRowRecord){
        var dateAssigned=selectedRowRecord.getValue('wrcf.date_assigned');
        var timeAssigned=selectedRowRecord.getValue('wrcf.time_assigned');
        var dueDate=selectedRowRecord.getValue('wr.date_escalation_completion');
        var startDate=new Date(dateAssigned.getFullYear(), dateAssigned.getMonth(), dateAssigned.getDate(), timeAssigned.getHours(), timeAssigned.getMinutes(), timeAssigned.getSeconds());
        var task ={
                cfId: selectedRowRecord.getValue('wrcf.cf_id'),
                wrId: selectedRowRecord.getValue('wrcf.wr_id'),
                startDate: startDate,
                dueDate: dueDate
        }
        
        return task;
    },
    
    /**
     * Overide methods in ab-tabs.js file to adjust tab height to hide tab header element and update content height.
     */
    hideTabsHeaderAndUpdateContentHeight: function(){
        var controller = this;
        /**
         * Override show method based on includeHeader parameter to show or hide tab header element.
         */
        Object.getPrototypeOf(this.workScheduleTabs).show = function(show, includeHeader){
            var parentEl = Ext.get(this.getWrapperElementId());
            // KB 3039173: the layout wrapper element does not have the display property in IE8
            // if tabs are nested inside tabs and both levels use frames
            if (parentEl && parentEl.dom.style.display!=="none") {
                parentEl.setDisplayed(show);
                if(includeHeader==false){
                    jQuery(".x-tab-panel-header").hide();
                }else{
                    jQuery("x-tab-panel-header").show();
                }
            }
            if (show) {
                this.syncHeight();
            }
        };
        
        /**
         * Override getTabStripHeight to set tab trip height is 0;
         */
        Object.getPrototypeOf(this.workScheduleTabs).getTabStripHeight = function(){
            return 0;
        };
        
        
        _.each(this.workScheduleTabs.tabs, function(tab){
            tab.parentPanel = controller.workScheduleTabs;
        });
        
        this.workScheduleTabs.show(true,false);
    }
});

/**
 * The Labor Board control implements https://confluence.archibus.zone/display/PLAT/HTML+Scheduling+Control.
 */
WorkScheduleControl = Base.extend({

    /**
     * The configuration object.
     */
    config: null,

    /**
     * The dxScheduler control instance.
     */
    scheduler: null,

    /**
     * The WorkScheduleModel instance.
     */
    model: null,

    /**
     * Restriction for the schedule.
     */
    restriction: null,

    /**
     * Start view date.
     */
    startDate: null,

    /**
     * End view date.
     */
    endDate: null,

    /**
     * Current edit appointment data.
     */
    currentEditAppointmentData: null,

    /**
     * Default estimate hours of per date cell when drag and drop work request on date cell. This value based on this.config.displayHours value
     */
    defaultEstimateHours: null,

    /**
     * Set start day hour.
     */
    startDayHour: null,

    /**
     * End day hour.
     */
    endDayHour: null,

    /**
     * Cell duration.
     */
    cellDuration: null,

    /**
     * Control constructor.
     * @param {config} The control configuration.
     */
    constructor: function(config) {
        this.config = config;
        this.model = this.config.model;

        this.initializeScheduler();
    },

    /**
     * Sets the control height to specified value.
     * @param {height} The height in pixels.
     */
    setHeight: function(height) {
        this.height = height;
    },

    /**
     * Adds a listener to specified control event.
     * @param {eventName} The name of the control event, e.g. 'click' or 'locateBuilding'.
     * @param {listener} The listener function reference.
     */
    addEventListener: function(eventName, listener) {
    },

    /**
     * Removes listeners from specified control event.
     * @param {eventName} The name of the control event, e.g. 'click' or 'locateBuilding'.
     * @param {listener} The listener function reference. Optional. If not specified, the control removes all event listeners for this event.
     */
    removeEventListener: function(eventName) {
    },

    /**
     * Initializes the dxScheduler control with customizations required for the Labor Scheduling Board.
     */
    initializeScheduler: function() {
        var control = this;
        //cache the craftsperson person code before appointment data update.
        var cfIdBeforeUpdate= '';
        // configure day start and end hours
        this.setDayHours(12);

        var scheduler = jQuery("#workScheduleContainer").dxScheduler({
            dataSource: this.model.scheduledTasks,
            useDropDownViewSwitcher: false,
            views: [ "timelineWorkWeek","timelineWeek" ],
            currentView: "timelineWorkWeek",
            currentDate: this.config.startDate,
            firstDayOfWeek: 1,
            startDayHour: this.startDayHour,
            endDayHour: this.endDayHour,
            cellDuration: this.cellDuration,
            width: "100%",
            height: function () {
                return control.height -46;
            },
            groups: [ "cfId" ],
            editing: {
                // disallow user create time blocks by click or type enter button, only allow create by drag and drop event.
                allowAdding: false
            },
            resources: [ {
                field: "cfId",
                dataSource: this.model.craftspersons,
                label: "Craftsperson",
                useColorAsDefault: true
            } ],
            crossScrollingEnabled: true,

            // display the secondary time scale (hours) as vertical text
            timeCellTemplate: function(itemData, itemIndex, itemElement) {
                //APP-1468:Use the locale to determine which format to display time with U.S.format or 24-hours format.
                var formattedTime=itemData.text.replace('AM', 'a').replace('PM', 'p');
                itemElement.parent().height(50);
                jQuery("<div>").text(formattedTime).css("transform", "rotate(-90deg)").appendTo(itemElement);

                if (itemData.date.getHours() === 8) {
                    itemElement.addClass('first-hour');
                }
            },

            // add unscheduled hours to each resource row
            resourceCellTemplate: function(cellData, index, container) {
                var craftsperson = cellData.data;

                container.attr('id', craftsperson.autoNumber + '_rowHeader');

                // when the user clicks on the craftsperson cell, trigger an event to display craftsperson schedule
                container.css('cursor', 'pointer');
                container.on('click', function(event) {
                    var currentSelectedDate=control.startDate;
                    control.trigger('app:bldgops:laborScheduler:selectCraftsperson', craftsperson.id, currentSelectedDate);
                });
            },

            // display unavailable hours as diagonal stripes
            dataCellTemplate: function(cellData, index, container) {
                if (cellData.groups) {
                    var cfId = cellData.groups.cfId;

                    if (cellData.startDate.getHours() === control.startDayHour) {
                        container.addClass('first-hour');
                    }

                    container.on('dragover', function(event) {
                        event.preventDefault();
                    });
                    container.on('drop', function(event) {
                        event.preventDefault();

                        var dragData = event.originalEvent.dataTransfer.getData('text');
                        var workRequest = JSON.parse(dragData);
                        //split assignment into multiple days according to the Trade Estimation.
                        var estimateHours = getEstimateHoursByPrimaryTrade(workRequest.wr_id,cfId);
                        var tasks=getSplitedAssignments(workRequest.wr_id,cfId,cellData.startDate,estimateHours);
                        if(tasks){
                            for(var i=0;i<tasks.length;i++){
                                tasks[i].description = workRequest.description;
                                tasks[i].taskType = 'TASK';
                                scheduler.addAppointment(tasks[i]);
                            }
                        }
                    });
                }

                return jQuery('<div>').text(cellData.text);
            },

            // template display the appointment tooltip.
            appointmentTooltipTemplate: function(data, container) {
                var markup = control.getTooltipTemplate(data);
                return markup;
            },

            /**
             * Define a custom HTML template for the task.
             */
            appointmentTemplate: function(task, itemIndex, itemElement) {
                // set default zIndex for all appointments.
                jQuery(itemElement).parent()[0].style.zIndex = 150;
                var html = '';
                if (task.taskType === 'TASK') {
                    //APP-2078:display the last four digits of the Work Request Code instead of the entire code. 
                    var truncattedWrId = task.wrId;
                    if(task.wrId.length > 4){
                        truncattedWrId = task.wrId.substr(task.wrId.length-4);
                    }
                    html = '<div>' + truncattedWrId + '</div><div>' + task.description + '</div>';
                } else if (task.taskType === 'STANDARD') {
                    itemElement.addClass('standard-hours');
                } else if (task.taskType === 'OVERTIME') {
                    itemElement.addClass('overtime-hours');
                } else if (task.taskType === 'DOUBLETIME') {
                    itemElement.addClass('doubletime-hours');
                } else if (task.type === 'VARIANCE') {
                    if (task.taskType != 'STANDARD' && task.taskType != 'OVERTIME' && task.taskType != 'DOUBLETIME' && task.taskType != 'ON SITE') {
                        itemElement.addClass('unavailable-hours');
                    }
                    if (task.taskType == 'ON SITE') {
                        html="<div>"+task.siteId +'-'+task.blId+"</div>";
                        // reset site visit time block height.
                        itemElement.addClass('sitevisit-hours');
                    }
                }

                return jQuery(html);
            },

            /**
             * Triggered when the user adding scheduled work requests to dxScheduler control by dragging and dropping.
             * @param event
             */
            onAppointmentAdding: function(event) {
                var validatePassed = validateAppointment(event.appointmentData);
                if(validatePassed){
                    var editOptions = getWrcfRecordEditOptions(event.appointmentData);
                    if(editOptions.allowAdding){
                        control.model.createScheduledTask(event.appointmentData);
                    }else{
                        View.alert(editOptions.addAlertText);
                        event.cancel=true;
                    }
                }else{
                    event.cancel=true;
                }
            },
            /**
             * Triggered after user added scheduled work requests to dxScheduler control by dragging and dropping.
             * @param event
             */
            onAppointmentAdded: function(event) {
                control.trigger('app:bldgops:laborScheduler:afterWorkRequestScheduled', event.appointmentData);
            },
            /**
             * Triggered when the user changes the appointment using either the standard dxScheduler appointment dialog, or by dragging or resizing the appointment.
             * @param event
             */
            onAppointmentUpdating: function(event) {
                var validatePassed = validateAppointment(event.newData);
                cfIdBeforeUpdate = event.oldData.cfId;
                if(validatePassed){
                    var editOptions = getWrcfRecordEditOptions(event.oldData);
                    if(editOptions.allowUpdating){
                        // update estimate hours during update.
                        event.newData.estimatedHours = hoursBetween(event.newData.startDate, event.newData.endDate);
                        control.model.updateTask(event.newData, event.oldData);
                    }else{
                        View.alert(editOptions.updateAlertText);
                        event.cancel=true;
                    }
                }else{
                    event.cancel=true;
                }
            },

            /**
             * Triggered after the user changes the appointment using either the standard dxScheduler appointment dialog, or by dragging or resizing the appointment.
             * @param event
             */
            onAppointmentUpdated: function(event) {
                //update the original craftsperson totals after appointment updated if craftsperson is changed.
                if(valueExistsNotEmpty(cfIdBeforeUpdate)){
                    if(cfIdBeforeUpdate!=event.appointmentData.cfId){
                        var originalCraftsperson = control.model.craftspersonsById[cfIdBeforeUpdate];
                        if(originalCraftsperson){
                            control.model.updateCraftspersonTotals(originalCraftsperson, control.model.startDate, control.model.endDate);
                        }
                        cfIdBeforeUpdate='';
                    }
                }
                control.trigger('app:bldgops:laborScheduler:afterWorkRequestUpdated', event.appointmentData);
            },

            /**
             * Triggered during the user deleting the appointment. or by dragging or resizing the appointment.
             * @param event
             */
            onAppointmentDeleting: function(event) {
                var editOptions = getWrcfRecordEditOptions(event.appointmentData);
                
                if (event.appointmentData.actionType == 'unscheduled') {
                    if(editOptions.allowUpdating){
                        control.model.unscheduleTask(event.appointmentData);
                    }else{
                        View.alert(editOptions.updateAlertText);
                        event.cancel=true;
                    }
                    
                } else {
                    if(editOptions.allowDeleting){
                        control.model.deleteTask(event.appointmentData);
                    }else{
                        View.alert(editOptions.deleteAlertText);
                        event.cancel=true;
                    }
                }
            },

            /**
             * Triggered after the user deleted the appointment. or by dragging or resizing the appointment.
             * @param event
             */
            onAppointmentDeleted: function(event) {
                control.trigger('app:bldgops:laborScheduler:afterWorkRequestDeleted', event.appointmentData);
            },

            /**
             * Trigger on schedule control option change.
             */
            onOptionChanged: function(event) {
                if (event.name === 'currentDate') {
                    control.loadSchedule(event.value);
                }
                
                if (event.name == 'currentView') {
                    control.loadSchedule(control.scheduler.option('currentDate'), event.value);
                }
            },

            /**
             * Triggered after appointment rendered, to set appointment UI style.
             */
            onAppointmentRendered: function(event) {
                var appointmentText='';
                if(valueExistsNotEmpty(event.appointmentData.text)){
                    appointmentText=event.appointmentData.text;
                }
                var formattedDate=control.formatTooltipDate(event.appointmentData.startDate,event.appointmentData.endDate,false);
                
                if (event.appointmentData.type == 'REGULAR') {
                    event.appointmentElement[0].style['opacity'] = 0.1;
                    jQuery(event.appointmentElement)[0].style['z-index'] = 10;
                }
                if (event.appointmentData.type == 'VARIANCE') {
                    event.appointmentElement[0].style['opacity'] = 1;
                    jQuery(event.appointmentElement)[0].style['z-index'] = 30;
                }
                if (event.appointmentData.taskType == 'TASK') {
                    jQuery(event.appointmentElement)[0].style['z-index'] = 150;
                }
                if (event.appointmentData.taskType == 'ON SITE') {
                    appointmentText=getMessage('siteVisitText')+': '+event.appointmentData.siteId + '-' +event.appointmentData.blId;
                    jQuery(event.appointmentElement).height('30px');
                    jQuery(event.appointmentElement)[0].style['z-index'] = 50;
                }
                
                //show help text when mouse hover the appointment.
                event.appointmentElement.prop('title', appointmentText+' ('+formattedDate+')');
            }
        }).dxScheduler("instance");

        // placeholder for inter-scheduler drag and drop
        jQuery('.dx-scheduler-date-table-cell').on('dxdrop', function(event) {
        });

        jQuery('.dx-scheduler-view-switcher').hide();
        jQuery('.dx-scheduler-view-switcher-label').hide();

        this.scheduler = scheduler;

        // overide core prototype to allow appointments overlapping top and lower,instead of side by side.
        Object.getPrototypeOf(this.scheduler._appointments._renderingStrategy)._customizeAppointmentGeometry = function(coordinates) {
            var cellHeight = (this._defaultHeight || this.getAppointmentMinSize()) - 50, height = cellHeight;
            if (height > 100) {
                height = 100
            }

            // SK: when setting trade rows to be not as high as craftsperson rows, the code above produced negative appointment height.
            // We need to discuss a more robust approach to appointment geometry.
            // Temporary change below:
            //
            // JIA: set appointment a little narrow than craftsperson row height, to let user drag and drop work request on blank space.
            // Because current no another better solution to allow user drop on a existing time block.
            height = 56;

            var top = coordinates.top;
            return {
                height: height,
                width: coordinates.width,
                top: top,
                left: coordinates.left
            }
        };

        // User ARCHIBUS panel control instead of third-party control default pop-up window.
        Object.getPrototypeOf(this.scheduler)._showAppointmentPopup = function(appointmentData, showButtons, processTimeZone) {
            control.currentEditAppointmentData = appointmentData;
            var wrId = appointmentData.wrId;
            var cfId = appointmentData.cfId;
            var dateAssigned = View.dataSources.get('tasksDataSource').formatValue("wrcf.date_assigned", appointmentData.startDate, false);
            var timeAssigned = View.dataSources.get('tasksDataSource').formatValue("wrcf.time_assigned", appointmentData.startDate, false);
            var res = new Ab.view.Restriction();
            res.addClause('wrcf.wr_id', wrId, '=');
            res.addClause('wrcf.cf_id', cfId, '=');
            res.addClause('wrcf.date_assigned', dateAssigned, '=');
            res.addClause('wrcf.time_assigned', timeAssigned, '=');
            
            var editOptions = getWrcfRecordEditOptions(appointmentData);
            
            View.openDialog('ab-bldgops-labor-scheduler-timeline-task-detail-popup.axvw',null,false,{
                wrId:wrId,
                //use this parameter to indicate which scheduler open the task detail pop-up.
                schedulerType:'multipleCraftspersonScheudler',
                editOptions : editOptions,
                taskRestriction:res
            });
        }
    },

    /**
     * Set start day hour and end day hour by display hours.
     * @param {displayHours} Per day display hours. 12 hours or 24 hours.
     */
    setDayHours: function(displayHours) {
        if (displayHours == 24) {
            this.defaultEstimateHours = 2;
            this.startDayHour = 0;
            this.endDayHour = 24;
        }

        if (displayHours == 12) {
            this.defaultEstimateHours = 1;
            this.startDayHour = 8;
            this.endDayHour = 20;
        }

        this.cellDuration = this.defaultEstimateHours * 60;
    },

    /**
     * Loads the schedule for specified list of resources. Triggers the afterRefresh event.
     */
    loadSchedule: function(startDate, currentView) {
        View.openProgressBar();
        this.loadScheduleControl.defer(50,this,[startDate, currentView])
    },
    
    /**
     * Load Schedule control.
     */
    loadScheduleControl: function(startDate, currentView){
        if (!startDate) {
            startDate = this.scheduler.getStartViewDate();
        }
        if (!currentView) {
            currentView = this.scheduler.option('currentView');
        }
        
        //Fixed issue that if current view is 'timelineWorkWeek', current day is Sunday or Saturday,then start day should changed to be scheduler control start date.
        if(currentView == 'timelineWorkWeek'){
            if(startDate.getDay()  == 6||startDate.getDay() == 0){
                startDate = this.scheduler.getStartViewDate();
            }
        }
        this.startDate = this.getViewStartDate(startDate);
        this.endDate = this.getViewEndDate(this.startDate, currentView);
        //reset craftsperson datasource filter restriction.
        workScheduleCtrl.setWorkScheduleFitlerRestrictions();
        
        this.model.loadSchedule(this.restriction, this.startDate, this.endDate);
        this.scheduler.repaint();
        //render all craftsperson statistics and unschedule task bar after scheduler repaint, because row height will be changed only after repaint.
        this.renderAllCraftspersons(this.model.craftspersons);
        
        this.appendSwitchDayHoursButton();

        jQuery('span:contains("Timeline Week")').text(getMessage('timelineWeek'));
        jQuery('span:contains("Timeline Work Week")').text(getMessage('timelineWorkWeek'));

        View.closeProgressBar();
    },
    
    /**
     * Render a basic background color for unschedule task bar when load craftsperson resource data for all craftspersons in model.
     */
    renderAllCraftspersons: function(craftspersons){
        var me = this;
        _.each(craftspersons,function(craftsperson){
            me.renderBasicBgColorForUnscheduleTaskBar(craftsperson);
            me.renderCraftsperson(craftsperson);
        });
    },

    /**
     * Get converted view start date.
     */
    getViewStartDate: function(currentDate) {
        return getFirstWeekDate(currentDate, this.scheduler.option('firstDayOfWeek'));
    },

    /**
     * Get converted view end date.
     */
    getViewEndDate: function(startViewDate, currentView) {
        var endDate = startViewDate;
        if(currentView == 'timelineWorkWeek'){
            endDate = endDate.add(Date.DAY, 4);
        }
        if(currentView == 'timelineWeek'){
            endDate = endDate.add(Date.DAY, 6);
        }
        return endDate;
    },

    /**
     * Get customized appointment tooltip template.
     */
    getTooltipTemplate: function(data) {
        var $tooltip = jQuery("<div>").addClass("dx-scheduler-appointment-tooltip");
        startDate = data.startDate, endDate = data.endDate, description = data.description;
        var wrRecord = this.model.getWorkRequestRecord(data.wrId);
        var dueDate = wrRecord.getValue('wr.date_escalation_completion');
        var dateAssigned = wrRecord.getValue('wr.date_assigned');
        var siteId = wrRecord.getValue('wr.site_id');
        var blId = wrRecord.getValue('wr.bl_id');
        var flId = wrRecord.getValue('wr.fl_id');
        var rmId = wrRecord.getValue('wr.rm_id');
        var eqId = wrRecord.getValue('wr.eq_id');
        var eqStd = wrRecord.getValue('eq.eq_std');
        var pmpId = wrRecord.getValue('wr.pmp_id');
        var pmsId = wrRecord.getValue('wr.pms_id');
        var critiality = wrRecord.getValue('pms.criticality');
        var priority = wrRecord.getValue('wr.priority');
        var priorityLabel = wrRecord.getValue('wr.priority_label');
        var problemType = wrRecord.getValue('wr.prob_type');
        var trades = this.model.getWorkRequestTrades(data.wrId);
        
        var tableElement=jQuery("<table>").attr("width","100%");
        var row1Element = jQuery("<tr>").attr("width","100%");
        //work request code.
        jQuery("<td>").append(jQuery("<div>").text(data.wrId).addClass("dx-scheduler-appointment-tooltip-title")).appendTo(row1Element);
        //priority label.
        jQuery("<td>").append(jQuery("<div>").text(Ext.util.Format.ellipsis(priorityLabel, 25))).appendTo(row1Element);
        
        var rowProbTypeElement = jQuery("<tr>").attr("width","100%");
        if(problemType){
            jQuery("<td>").attr("colspan",'2').append(jQuery("<div>").text(Ext.util.Format.ellipsis(problemType, 50))).appendTo(rowProbTypeElement);
        }
        
        var row2Element = jQuery("<tr>").attr("width","100%");
        //start date and end date
        jQuery("<td>").append(jQuery("<div>").addClass("dx-scheduler-appointment-tooltip-date").text(this.formatTooltipDate(startDate, endDate, false))).appendTo(row2Element);
        //Due date.
        if(dueDate){
            jQuery("<td>").append(jQuery("<div>").text(getMessage('dueDate').replace('{0}',getIsoFormatDate(dueDate)))).appendTo(row2Element);
        }else{
            jQuery("<td>").appendTo(row2Element);
        }
        
        var row3Element = jQuery("<tr>").attr("width","100%");
        //description
        if(description){
            jQuery("<td>").attr("colspan",'2').append(jQuery("<div>").text(Ext.util.Format.ellipsis(description, 50))).appendTo(row3Element);
        }
        
        var row4Element =jQuery("<tr>").attr("width","100%");
        //location.
        var row4Col1 = jQuery("<td>");
        if(blId){
            row4Col1.append(jQuery("<div>").text(siteId+'-'+blId+'-'+flId+'-'+rmId))
        }
        row4Col1.appendTo(row4Element);
        //pmp and pms code.
        var row4Col2 = jQuery("<td>");
        if(pmpId){
            row4Col2.append(jQuery("<div>").text(pmpId+'-'+pmsId+'-'+critiality));
        }
        row4Col2.appendTo(row4Element);
        
        var row5Element = jQuery("<tr>").attr("width","100%");
        var row5Col1 = jQuery("<td>");
        if(eqId){
            row5Col1.append(jQuery("<div>").text(eqId));
        }
        row5Col1.appendTo(row5Element);
        var row5Col2 = jQuery("<td>");
        if(eqStd){
            row5Col2.append(jQuery("<div>").text(eqStd));
        }
        row5Col2.appendTo(row5Element);
        
        var row6Element = jQuery("<tr>").attr("width","100%");
        if(trades){
            jQuery("<td>").attr("colspan",'2').append(jQuery("<div>").text(trades)).appendTo(row6Element);
        }

        row1Element.appendTo(tableElement);
        rowProbTypeElement.appendTo(tableElement);
        row2Element.appendTo(tableElement);
        row3Element.appendTo(tableElement);
        row4Element.appendTo(tableElement);
        row5Element.appendTo(tableElement);
        row6Element.appendTo(tableElement);
        
        tableElement.appendTo($tooltip);
        
        var $buttons = jQuery("<div>").addClass("dx-scheduler-appointment-tooltip-buttons").appendTo($tooltip);
        if (this.scheduler._editing.allowDeleting) {
            this.getDeleteButton(data).appendTo($buttons)
        }

        this.getEditButton(data).appendTo($buttons);
        this.getUnscheduleButton(data).appendTo($buttons);
        return $tooltip;
    },

    /**
     * Format date for appointment tooltip popup window to display.
     */
    formatTooltipDate: function(startDate, endDate, isAllDay) {
        var formatType = "month" !== this.scheduler.option("currentView") && sameDate(startDate,endDate) ? "TIME" : "DATETIME", formattedString = "";
        if (isAllDay) {
            formatType = "DATE"
        }
        
        //Format date and time string by using the ARCHIBUS API.
        var dateFormatDs = View.dataSources.get('dateTimeFormateDs');
        
        var formattedStartString = "";
        var formattedEndString = "";
        
        if(formatType == "DATE"){
            formattedStartString = dateFormatDs.formatValue('wrcf.date_assigned',startDate,true);
            formattedEndString = dateFormatDs.formatValue('wrcf.date_assigned',endDate,true);
        }
        
        if(formatType == "DATETIME"){
            formattedStartString = dateFormatDs.formatValue('wrcf.date_assigned',startDate,true)+' '+dateFormatDs.formatValue('wrcf.time_assigned',startDate,true);
            formattedEndString = dateFormatDs.formatValue('wrcf.date_assigned',endDate,true)+' '+dateFormatDs.formatValue('wrcf.time_assigned',endDate,true);
        }
        
        if(formatType == "TIME"){
            formattedStartString = dateFormatDs.formatValue('wrcf.time_assigned',startDate,true);
            formattedEndString = dateFormatDs.formatValue('wrcf.time_assigned',endDate,true);
        }
        return formattedStartString+'-'+formattedEndString;
    },

    /**
     * Get delete button element for appointment tooltip popup window.
     */
    getDeleteButton: function(data) {
        var that = this;
        var ele = jQuery("<div>").dxButton({
            icon: "trash",
            onClick: function() {
                that.scheduler.deleteAppointment(data);
                that.scheduler.hideAppointmentTooltip();
            }
        });

        return ele;
    },

    /**
     * Get edit button element for appointment tooltip popup window.
     */
    getEditButton: function(data) {
        var that = this, allowUpdating = that.scheduler._editing.allowUpdating;
        var editButton = jQuery("<div>").dxButton({
            icon: allowUpdating ? "edit" : "",
            text: getMessage('edit'),
            onClick: function() {
                that.scheduler.showAppointmentPopup(data, false);
                that.scheduler.hideAppointmentTooltip();
            }
        });

        return editButton;
    },

    /**
     * Get unscheduled button element for appointment tooltip popup window.
     */
    getUnscheduleButton: function(data) {
        var that = this, allowUpdating = that.scheduler._editing.allowUpdating;
        var editButton = jQuery("<div>").dxButton({
            icon: "",
            text: getMessage('Unschedule'),
            onClick: function() {
                data.actionType = 'unscheduled';
                that.scheduler.deleteAppointment(data);
                that.scheduler.hideAppointmentTooltip();
            }
        });

        return editButton;
    },

    /**
     * Append switch day hours buttons to dxScheduler control navigator bar.
     */
    appendSwitchDayHoursButton: function() {
        var schedulerNavigator = jQuery(".dx-scheduler-navigator");
        this.getDayHoursButton(12).addClass('switchDayHoursButton').appendTo(schedulerNavigator);
        this.getDayHoursButton(24).addClass('switchDayHoursButton').appendTo(schedulerNavigator);
    },

    /**
     * Get switch day hours buttons.
     * @param {displayHours} Display Hours.
     */
    getDayHoursButton: function(displayHours) {
        var that = this;
        var text = "";
        if (displayHours == 12) {
            text = getMessage("12");
        }
        if (displayHours == 24) {
            text = getMessage("24");
        }
        var editButton = jQuery("<div>").dxButton({
            icon: "",
            text: getMessage(text),
            onClick: function() {
                View.openProgressBar();
                that.switchDayHours.defer(50,that,[displayHours]);
            }
        });

        return editButton;
    },

    /**
     * Switch scheduler display day hours.
     */
    switchDayHours: function(displayHours) {
        this.setDayHours(displayHours);
        this.scheduler.option({"startDayHour":this.startDayHour,"endDayHour":this.endDayHour,"cellDuration":this.cellDuration});
        this.renderAllCraftspersons(this.model.craftspersons);
        View.closeProgressBar();
    },
    
    /**
     * Render a basic background color for craftsperson unschedule bar only craftsperson resource data first load.
     * Spec:
     * If the craftspersons' regular hours are not defined, then the Unscheduled Task bar will still display the blocks and estimated hours of Unscheduled Tasks, but no color shading of that time will appear. 
     */
    renderBasicBgColorForUnscheduleTaskBar: function(craftsperson, container){
        if (!container) {
            container = jQuery('#' + craftsperson.autoNumber + '_rowHeader');
        }
        var index = container.parent().parent().index();
        var rowEl = jQuery('.dx-scheduler-date-table-row').eq(index);
        
        //total display days in visible date range.
        var currentDate = new Date();
            currentDate = new Date(currentDate.getFullYear(),currentDate.getMonth(),currentDate.getDate(),0,0,0);
        var totalDays = 5;
        var currentView = this.scheduler.option('currentView');
        if(currentView == 'timelineWeek'){
            totalDays = 7;
            WorkScheduleControl.TIMELINE_WIDTH = 1680;
        }else{
            WorkScheduleControl.TIMELINE_WIDTH = 1200;
        }
        var unscheduledTaskBarWidth = WorkScheduleControl.TIMELINE_WIDTH;
        if(currentDate.between(this.startDate, this.endDate)){
            unscheduledTaskBarWidth = WorkScheduleControl.TIMELINE_WIDTH - (currentDate.getDay()-1) *240;
        }
         
        this.renderUnscheduledTaskSegment(craftsperson, index, rowEl, 0, unscheduledTaskBarWidth, 'rgba(51, 122, 183, 0.1)');
    },

    /**
     * Renders craftsperson name and totals in the resource row header.
     * @param craftsperson
     */
    renderCraftsperson: function(craftsperson, container) {
        if (!container) {
            container = jQuery('#' + craftsperson.autoNumber + '_rowHeader');
        }

        container[0].innerHTML = '';
        //If craftsperson has no name, then display craftsperson code.
        var craftspersonName=valueExistsNotEmpty(craftsperson.text)?craftsperson.text:craftsperson.id;
        container.append(jQuery('<div>').text(craftspersonName));
        container.append(jQuery('<div>').addClass('resource-totals-trade').text(craftsperson.trade));
        
        //APP-1272: If the user is looking at a prior week in the schedule, then the statistics do not need to be displayed at all.
        var currentDate = new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate(),0,0,0);
        if(this.endDate.getTime() >= currentDate.getTime()){
            if(craftsperson.totals.availableHours <= 0){
                //APP-1724:If a craftsperson has no Standard Hours for the visible week (or the remainder of the current week), then modify the statistics. Only display "X Hours Scheduled", which is the sum of estimated hours for Scheduled work requests for that time period.
                container.append(jQuery('<div>').addClass('resource-totals').text(formatNumberValue(craftsperson.totals.scheduledHours) + ' ' + getMessage('scheduledHoursMessage')));
            }else{
                //APP-1201: only show the available/assigned/percent allocated in statistics.
                container.append(jQuery('<div>').addClass('resource-totals').text(formatNumberValue(craftsperson.totals.availableHours) + ' ' + getMessage('availableHoursMessage')));
                //APP-1201:Modify the craftsperson statistics in the Multi-Craftsperson view of the Labor Scheduler.
                container.append(jQuery('<div>').addClass('resource-totals').text(formatNumberValue(craftsperson.totals.assignedHours) + ' ' + getMessage('assignedHoursMessage')));
                var percentAllocateText='N/A';
                if(craftsperson.totals.percentAllocated != 'N/A'){
                    percentAllocateText=craftsperson.totals.percentAllocated + '%';
                }
                container.append(jQuery('<div>').addClass('resource-totals').text(percentAllocateText + ' ' + getMessage('perentAllocatedMessage')))
            }
            
        }
        
        container.css('height', 110);

        var index = container.parent().parent().index();
        var rowEl = jQuery('.dx-scheduler-date-table-row').eq(index);
        this.removeAllExistingTaskSegment(craftsperson,rowEl);
        this.renderUnscheduledTaskSegments(craftsperson, index, rowEl);
    },
    
    /**
     * Remove existing task segment.
     */
    removeAllExistingTaskSegment: function(craftsperson,rowEl){
        var taskSegments = rowEl.find('.unscheduled-hours-panel');
        _.each(taskSegments,function(segment){
            if(segment.id.charAt(segment.id.length-1)!='_'){
                //Remove element and all it's children.
                jQuery(segment).detach();
            }
        });
    },

    /**
     * Renders unscheduled task segments for specified craftsperson.
     * @param craftsperson The craftsperson object.
     * @param craftspersonIndex The 0-based index of the craftsperson.
     * @param craftspersonRowEl The scheduler row element for the craftsperson.
     */
    renderUnscheduledTaskSegments: function(craftsperson, craftspersonIndex, craftspersonRowEl) {
        //total display days in visible date range.
        var totalDays = 5;
        var currentView = this.scheduler.option('currentView');
        if(currentView == 'timelineWeek'){
            totalDays = 7;
            WorkScheduleControl.TIMELINE_WIDTH = 1680;
        }else{
            WorkScheduleControl.TIMELINE_WIDTH = 1200;
        }
        
        var control = this;
        var x = 0;
        var currentDate = new Date();
            currentDate = new Date(currentDate.getFullYear(),currentDate.getMonth(),currentDate.getDate(),0,0,0);
        var segments = craftsperson.statistics.getUnscheduledSegmentsForDateRange(control.startDate, control.endDate);
        var remainingFillerAvailable = this.getFillerSegmentAvailableHours(craftsperson.totals,segments);
        _.each(segments, function(segment) {
            //Only display segments after the start date of the display date range.
            var startDate = segment.startDate;
            if(segment.startDate.getTime() <= control.startDate.getTime()){
                startDate = control.startDate;
            }
            
            if(currentDate.between(control.startDate, control.endDate)){
                startDate = segment.startDate.getTime() <= currentDate.getTime()?currentDate:segment.startDate;
            }
            
            var percentAvailable = segment.percentAvailable;
            if(segment.type=="fillerSegment"){
                if(remainingFillerAvailable>0){
                    if(craftsperson.totals.assignedUnscheduledHoursWithProrateHours && craftsperson.totals.assignedUnscheduledHoursWithProrateHours >0){
                        percentAvailable = (craftsperson.totals.assignedHours - craftsperson.totals.proratedUnscheduledHours - craftsperson.totals.assignedUnscheduledHours)/remainingFillerAvailable;
                    }
                    
                }
            }
            
            var daysToRender = daysBetween(startDate, segment.dueDate) + 1;
            var width = WorkScheduleControl.TIMELINE_WIDTH * (daysToRender / totalDays);
            var color = 'rgba(51, 122, 183, ' + percentAvailable + ')';
            control.renderUnscheduledTaskSegment(craftsperson, craftspersonIndex, craftspersonRowEl, x, width, color, segment);
            x += width;
        });
    },
    
    /**
     * Get filler segement
     */
    getFillerSegmentAvailableHours: function(craftpsersonTotals,segments){
        var remainingAvailableHours = craftpsersonTotals.availableHours;
        _.each(segments,function(segment){
            if(segment.type!="fillerSegment"){
                remainingAvailableHours-=segment.availableHours;
            }
        });
        return remainingAvailableHours;
    },

    renderUnscheduledTaskSegment: function(craftsperson, craftspersonIndex, craftspersonRowEl, x, width, backgroundColor, segment) {
        //APP-1777:Unscheduled Task Bar should not display before the current day.
        var currentDate = new Date();
            currentDate = new Date(currentDate.getFullYear(),currentDate.getMonth(),currentDate.getDate(),0,0,0);
        //set margin left width value.
        var width_margin_left = 0;
        if(currentDate.getTime()>this.endDate.getTime()){
            return;
        }
        if(currentDate.between(this.startDate, this.endDate)){
            width_margin_left = (currentDate.getDay()-1)*240;
        }
        if(width <= 0){
            return;
        }

        var unscheduledHoursPanel = document.createElement('div');
        if (segment) {
            unscheduledHoursPanel.id = craftsperson.id + '_' + dayName(segment.dueDate);
        } else {
            unscheduledHoursPanel.id = craftsperson.id + '_';
        }
        unscheduledHoursPanel.className = 'unscheduled-hours-panel';
        unscheduledHoursPanel.style.zIndex = 1000;
        unscheduledHoursPanel.style.top = (craftspersonIndex * craftspersonRowEl.height()+85) + 'px';
        unscheduledHoursPanel.style.left = x + 'px';
        unscheduledHoursPanel.style.width = width + 'px';
        unscheduledHoursPanel.style.backgroundColor = backgroundColor;
        unscheduledHoursPanel.style.marginLeft = width_margin_left + 'px';

        if (segment && segment.dueDate.between(this.startDate, this.endDate) && segment.initialUnscheduledHours > 0) {
            var unscheduledHoursLabel = document.createElement('span');
            unscheduledHoursLabel.className = 'unscheduled-hours-label';
            unscheduledHoursLabel.appendChild(document.createTextNode('Unscheduled hours due ' + dayName(segment.dueDate) + ':'));
            
            //APP-1679:Add click event to unscheduled hours label to open a popup of unscheduled tasks. 
            jQuery(unscheduledHoursLabel).css("cursor","pointer").on('click',function(event){
                control.openUnscheduledTaskPopup(craftsperson.id,getIsoFormatDate(segment.dueDate));
            });
            
            var unscheduledHoursIcon = document.createElement('span');
            unscheduledHoursIcon.className = 'unscheduled-hours-icon';
            //Unscheduled icon text should display unscheduled hours add proratedUnscheduledHours if proratedUnscheduledHours exists.

            unscheduledHoursIcon.appendChild(document.createTextNode(segment.initialUnscheduledHours));

            unscheduledHoursPanel.appendChild(unscheduledHoursIcon);
            unscheduledHoursPanel.appendChild(unscheduledHoursLabel);
        } else {
            unscheduledHoursPanel.appendChild(document.createElement('span'));
        }

        craftspersonRowEl.append(unscheduledHoursPanel);

        var control = this;
        
        jQuery(unscheduledHoursPanel).on('dragover', function(event) {
            event.preventDefault();
        });
        jQuery(unscheduledHoursPanel).on('drop', function(event) {
            event.preventDefault();
            var currentDate=new Date();
            currentDate.setMilliseconds(0);
            var dragData = event.originalEvent.dataTransfer.getData('text');
            var workRequest = JSON.parse(dragData);
            var dueDate = workRequest.date_escalation_completion;
            if (dueDate) {
                dueDate = View.dataSources.get('workRequestsDataSource').parseValue('wr.date_escalation_completion', dueDate);
            }
            //APP-2657:retain Estimated Hours when scheduling an unscheduled work request.
            var tradeEstimatedHours = parseFloat(workScheduleCtrl.getTradeEstimatedHours(workRequest.wr_id,craftsperson.id));
            if(tradeEstimatedHours<=0){
                tradeEstimatedHours = 1;
            }
            var task = {
                cfId: craftsperson.id,
                wrId: workRequest.wr_id,
                startDate: currentDate,                                                                                                                                                                                                                                                                                                                                                       
                endDate: currentDate.add(Date.HOUR,1),
                dueDate: dueDate,
                estimatedHours: tradeEstimatedHours
            };

            try {
                var editOptions = getWrcfRecordEditOptions(task);
                if(editOptions.allowAdding){
                    control.model.createUnscheduledTask(task);
                    control.renderCraftsperson(craftsperson);

                    var formattedDueDate = View.dataSources.get('workRequestsDataSource').formatValue('wr.date_escalation_completion', dueDate);
                    var message = getMessage('assignedToMessage') + ' ' + craftsperson.text + ', ' + getMessage('assignedDueMessage') + ' ' + formattedDueDate;
                    DevExpress.ui.notify(message, 'info', 5000);
                }else{
                    View.alert(editOptions.addAlertText);
                }
                

            } catch (e) {
                var message = getMessage('assignErrorMessage');
                DevExpress.ui.notify(message, 'error', 5000);
            }
        });

        return unscheduledHoursPanel;
    },
    
    /**
     * Open unschedule task pop-up panel.
     * 
     * @param cfId Craftsperson Code
     * @param dueDate Unscheduled Task Due Date.
     */
    openUnscheduledTaskPopup: function(cfId,dueDate){
        var unscheduledTaskPanel = View.panels.get('unscheduledTaskList');
        unscheduledTaskPanel.addParameter('dueDate',dueDate);
        unscheduledTaskPanel.addParameter('cfId',cfId);
        unscheduledTaskPanel.show(true);
        unscheduledTaskPanel.showInWindow({
            x:600,
            y:300,
            width: 800,
            height: 400,
            title: getMessage('unscheduledTasksTitle'),
            modal: true
        });
        unscheduledTaskPanel.refresh();
    }
}, {
    /**
     * The width of the timeline in pixels.
     */
    TIMELINE_WIDTH: 1200
});

/**
 * Get first week date.
 */
function getFirstWeekDate(date, firstDayOfWeek) {
    var delta = (date.getDay() - firstDayOfWeek + 7) % 7;
    var result = new Date(date);
    result.setDate(date.getDate() - delta);
    result.setHours(0,0,0,0);
    return result
}

var sameDate = function(date1, date2) {
    return sameMonthAndYear(date1, date2) && date1.getDate() === date2.getDate()
};
var sameMonthAndYear = function(date1, date2) {
    return sameYear(date1, date2) && date1.getMonth() === date2.getMonth()
};
var sameYear = function(date1, date2) {
    return date1 && date2 && date1.getFullYear() === date2.getFullYear()
};


