
//***********************************Methods for validation*********************************//

holidays = {}; // this is map with iso-date strings / names
/**
 * Validate appointment.
 */
function validateAppointment(task) {
    var validatePass = true;
    // assigned task should not earlier than current time.
    if (task.startDate.getTime() < new Date().getTime()) {
        View.alert(getMessage('workRequestShouldBeAssignedAfterCurrentDate'));
        return false;
    }
    // validate service window.
    validatePass = validateServiceWindow(task);
    return validatePass;
}

/**
 * Validation before split appointment.
 * @param wrId
 * @param startDate
 * @returns
 */
function validateAppointmentBeforeSplit(wrId,startDate){
    var validatePass = true;
    // assigned task should not earlier than current time.
    if (startDate.getTime() < new Date().getTime()) {
        View.alert(getMessage('workRequestShouldBeAssignedAfterCurrentDate'));
        return false;
    }
    //get service window
    var serviceWindow = getServiceWindow(wrId);
    // check if this is a service window day and check duration
    var serviceWindowHours = serviceWindow.getAvailableTime(startDate);
    var serviceWindowStartTime = serviceWindow.parseTime(serviceWindow.startTime);
    var serviceWindowEndTime = serviceWindow.parseTime(serviceWindow.endTime);
    
    // validate holiday.
    var allowWorkOnHolidays = isAllowWorkOnHoliday(wrId);
    if(useHolidays(allowWorkOnHolidays)){
        if(isHoliday(startDate)){
            View.alert(getMessage('validateHoliday').replace('{0}', getIsoFormatDate(startDate)));
            return false;
        }
    }
    
    //validate working day
    var weekday = startDate.getDay();
    if (serviceWindow.days[weekday] == 0){
        View.alert(getMessage('validateWorkingDay').replace('{0}', getIsoFormatDate(startDate)));
        return false;
    }
    var startHours = startDate.getHours();
    if(serviceWindowStartTime!==serviceWindowEndTime){
        if(startHours < serviceWindowStartTime || startHours >= serviceWindowEndTime){
            View.alert(getMessage('validateServiceWindowStartEndTime').replace('{0}', serviceWindow.getStartTime()).replace('{1}', serviceWindow.getEndTime()));
            return false;
        }
    }
    
    
    return validatePass;
}

/**
 * Validation for Service Window.
 */
function validateServiceWindow(task) {
    var validatePass = true;

    var startDate = task.startDate;
    var estimateHours = hoursBetween(task.startDate, task.endDate);
    //get service window
    var serviceWindow = getServiceWindow(task.wrId);
    
    // check if this is a service window day and check duration
    var serviceWindowHours = serviceWindow.getAvailableTime(task.startDate);
    
    if (serviceWindowHours == 0) {
        validatePass = false;
        View.alert(getMessage('validateWorkingDay').replace('{0}', getIsoFormatDate(task.startDate)));
    } else if (!serviceWindow.checkTimes(startDate, estimateHours) ||serviceWindowHours < 0) { // check against service window start and end time
        validatePass = false;
        View.alert(getMessage('validateServiceWindowStartEndTime').replace('{0}', serviceWindow.getStartTime()).replace('{1}', serviceWindow.getEndTime()));
    } else if (estimateHours > serviceWindowHours) {
        validatePass = false;
        View.alert(getMessage('validateServiceWindowHours').replace('{0}', estimateHours).replace('{1}', serviceWindowHours));
    }  else if (estimateHours <= 0) {
        validatePass = false;
        View.alert(getMessage('validateDuration'));
    }
    
    var allowWorkOnHolidays = isAllowWorkOnHoliday(task.wrId);
    if (useHolidays(allowWorkOnHolidays)) {
        var taskDates = getTaskDates(task.startDate, task.endDate);
        for (var i = 0; i < taskDates.length; i++) {
            if (isHoliday(taskDates[i])) {
                validatePass = false;
                View.alert(getMessage('validateHoliday').replace('{0}', getIsoFormatDate(taskDates[i])));
                break;
            }
        }
    }

    return validatePass;
}

/**
 * Get service window object based on work request.
 * @param {wrId} work request code.
 * @returns
 */
function getServiceWindow(wrId){
    // validate service window.
    var res = new Ab.view.Restriction();
        res.addClause('wr.wr_id', wrId, '=');
    var record = View.dataSources.get('workRequestDataSource').getRecord(res);
    var days = eval('(' + "[" + record.getValue('wr.serv_window_days') + "]" + ')');
    var serviceWindowStart = View.dataSources.get('workRequestDataSource').formatValue('wr.serv_window_start', record.getValue('wr.serv_window_start'), false);
    var serviceWindowEnd = View.dataSources.get('workRequestDataSource').formatValue('wr.serv_window_end', record.getValue('wr.serv_window_end'), false);
    var startTime = getTimeValue(serviceWindowStart);
    var endTime = getTimeValue(serviceWindowEnd);
    var serviceWindow = new ServiceWindow(days, startTime, endTime);
    
    return serviceWindow;
}

/**
 * Get result that whether is allow work on holiday or not.
 * @param wrId work request code.
 * @returns
 */
function isAllowWorkOnHoliday(wrId){
    var res = new Ab.view.Restriction();
    res.addClause('wr.wr_id', wrId, '=');
    var record = View.dataSources.get('workRequestDataSource').getRecord(res);
    // check if we have to use holidays.
    var allowWorkOnHolidays = record.getValue('wr.allow_work_on_holidays');
    return allowWorkOnHolidays;
}

/**
 * Calendar functions for holiday and weekend.
 */
function isHoliday(date) {
    if (date === undefined)
        return false;
    if (holidays[getIsoFormatDate(date)] === undefined)
        return false;
    return true;
}

/**
 * Load holidays.
 */
function loadHolidays(site_id, year) {
    var siteId = site_id;

    if (!valueExists(site_id)) {
        siteId = "0";
    }
    var result = {};
    try {
        result = Workflow.callMethod("AbBldgOpsOnDemandWork-Schedule-getHolidays", siteId, year);
    } catch (e) {
        Workflow.handleError(e);
    }

    if (result.code == 'executed') {
        return result.data;
    } else {
        Workflow.handleError(result);
    }
}

/*
 * Check whether we use use Holiday Calendar. If work is allowed on holidays, we don't have to load the Holiday calendar or take into account holidays.
 */
function useHolidays(allowWorkOnHolidays) {
    return allowWorkOnHolidays == '0';
}

/**
 * Get task days.
 */
function getTaskDates(startDate, endDate) {
    var dates = [];
    var startDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());
    var endDate = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate());
    for (start = startDate; start.getTime() <= endDate.getTime(); start = start.add(Date.DAY, 1)) {
        dates.push(start);
    }

    return dates;
}

/**
 * Get the time part of a ISO datetime string.
 * @param {String} value
 * @return {String} in HH:mm.ss.SSS format
 */
function getTimeValue(value) {
    if (value.indexOf(" ") > 0) { // take the last part
        return value.substr(value.indexOf(" ") + 1);
    } else {
        return value;
    }
}

/**
 * SLA Service Window.
 */
ServiceWindow = Base.extend({
    days: [],
    startTime: null,
    endTime: null,

    constructor: function(days, startTime, endTime) {
        this.days = days;
        this.startTime = startTime == "" ? "00:00" : getTimeValue(startTime); // startTime in "15:00" format
        this.endTime = endTime == "" ? "23:59" : getTimeValue(endTime);
        if (endTime == "00:00.00.000") // bug in Web Central
            this.endTime = "23:59";
    },

    getStartTime: function() {
        if (this.startTime.indexOf('.') == 0)
            return this.startTime;
        var timeParts = this.startTime.split('.');
        return timeParts[0];
    },

    getEndTime: function() {
        if (this.endTime.indexOf('.') == 0)
            return this.endTime;
        var timeParts = this.endTime.split('.');
        return timeParts[0];
    },

    parseTime: function(time) {
        if (time.indexOf(":") < 1) {
            return 0.0;
        }
        var timeParts = time.split(':');
        return parseInt(timeParts[0], 10) + parseInt(timeParts[1], 10) / 60;
    },

    getTimeValue: function(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        return hours + minutes / 60;
    },

    /*
     * check start and end time
     */

    checkTimes: function(startDate, duration) {
        var weekday = startDate.getDay();
        if (this.days[weekday] == 0)
            return false;

        var startTime = this.getTimeValue(startDate);
        var endTime = startTime + parseInt(duration, 10); // duration may be passed as String

        var startValue = this.parseTime(this.startTime);
        var endValue = this.parseTime(this.endTime);

        // If user use the 24 hours, then the startTime equal with the endTime.
        if (startValue !== endValue) {
            if (startTime < startValue)
                return false;
            if (endTime > endValue)
                return false;
        }
        return true;
    },

    // get the time available according the service window
    // return floating number hours
    getAvailableTime: function(date) {
        var weekday = date.getDay();
        if (this.days[weekday] == 0)
            return 0.0;

        var startHours = date.getHours();
        var startValue = this.parseTime(this.startTime);
        var endValue = this.parseTime(this.endTime);
        if (startValue != endValue) {
            if (startHours > startValue)
                startValue = startHours;
        }

        return endValue - startValue == 0 ? 24 : endValue - startValue;
    },
    
    /**
     * Get the whole service window hours.
     */
    getServiceWindowHours: function(){
        var startValue = this.parseTime(this.startTime);
        var endValue = this.parseTime(this.endTime);

        return endValue - startValue == 0 ? 24 : endValue - startValue;
    }

});


/**
 * Get work request craftspersons(wrcf) record edit option to determine that whether wrcf can be Added/Updated/Deleted or not.
 * 
 * @param {appointmentData} selected appointment Data.
 */
function getWrcfRecordEditOptions(appointmentData){
    var wrcfDs=View.dataSources.get('updateTaskDataSource');

    var wrId=appointmentData.wrId;
    var cfId=appointmentData.cfId;
    var dateAssigned=wrcfDs.formatValue('wrcf.date_assigned',appointmentData.startDate,false);
    var timeAssigned=wrcfDs.formatValue('wrcf.time_assigned',appointmentData.startDate,false);
    
    return getWrcfEditOption(wrId,cfId,dateAssigned,timeAssigned);
}

/**
 * Get work request craftsperson assignment edit option.
 * @param wrId
 * @param cfId
 * @param dateAssigned
 * @param timeAssigned
 * @returns
 */
function getWrcfEditOption(wrId,cfId,dateAssigned,timeAssigned){
    var wrcfDs=View.dataSources.get('updateTaskDataSource');
    var craftspersonWrDs=View.dataSources.get('craftspersonWorkRequestDs');
    var editOptions={
        allowAdding: true,
        allowDeleting: true,
        allowUpdating: true,
        //help text to alert when have no add/delete/update permission.
        addAlertText: '',
        deleteAlertText: '',
        updateAlertText: ''
    };
    
    var wrRestriction=new Ab.view.Restriction();
    wrRestriction.addClause('wr.wr_id',wrId,'='); 
    
    var wrcfRestriction=new Ab.view.Restriction();
    wrcfRestriction.addClause('wrcf.wr_id',wrId,'=');
    wrcfRestriction.addClause('wrcf.cf_id',cfId,'=');
    wrcfRestriction.addClause('wrcf.date_assigned',dateAssigned,'=');
    wrcfRestriction.addClause('wrcf.time_assigned',timeAssigned,'=');
    
    var wrcfRecord=wrcfDs.getRecord(wrcfRestriction);
    
    var status = wrcfRecord.getValue('wr.status');
    var actualHours = wrcfRecord.getValue('wrcf.hours_straight');
    var doubleHours = wrcfRecord.getValue('wrcf.hours_double');
    var overHours = wrcfRecord.getValue('wrcf.hours_over');
    
    //check whether user has permission to make changes for the selected work request or not.
    var hasChangePermission=craftspersonWrDs.getRecords(wrRestriction).length>0?true:false;
    if(hasChangePermission){
      //get application parameter, if = 0, then make the resource panels read-only if estimate step is completed.
        var editEstimationAndScheduling = View.activityParameters['AbBldgOpsOnDemandWork-EditEstAndSchedAfterStepComplete'];
        var isSchedulingCompleted = false;
        if(editEstimationAndScheduling == '0'){
            isSchedulingCompleted = Workflow.callMethod('AbBldgOpsOnDemandWork-WorkRequestService-isEstimateOrSchedulingCompleted',wrId,'scheduling').value;
            if(isSchedulingCompleted){
                editOptions.allowAdding=false;
                editOptions.addAlertText= getMessage('stepCompletedMessage');
            }
        }
        
        if(isSchedulingCompleted){
            editOptions.allowUpdating=false;
            editOptions.allowDeleting=false;
            editOptions.deleteAlertText = getMessage('stepCompletedMessage');
            editOptions.updateAlertText = getMessage('stepCompletedMessage');
        }
        
        if(actualHours>0 || doubleHours>0 || overHours>0 ){
            editOptions.allowDeleting=false;
            editOptions.deleteAlertText= getMessage('hasPerformWorkHoursMessage');
        }
        
        if(status!="A" && status!="AA"){
            var statusEnumValue = wrcfDs.fieldDefs.get('wr.status').enumValues[status];
            //KB3042844 - disable Remove action after work request issued
            editOptions.allowDeleting=false;
            editOptions.deleteAlertText= getMessage('statusNotAorAAMessage').replace('{0}',statusEnumValue);
        }
    }else{
        editOptions.allowAdding=false;
        editOptions.allowUpdating=false;
        editOptions.allowDeleting=false;
        editOptions.addAlertText = editOptions.updateAlertText = editOptions.deleteAlertText = getMessage('hasNoPermissionMessage');
    }
    
    return editOptions;
}

/**
 * Split assignment.
 * @returns
 */
function getSplitedAssignments(wrId,cfId,startDate,estimateHours){
    if(!validateAppointmentBeforeSplit(wrId,startDate)){
        return;
    }
    var tasks = [];
    var serviceWindow = getServiceWindow(wrId);
    
    var serviceWindowStartTime = serviceWindow.parseTime(serviceWindow.startTime);
    var serviceWindowEndTime = serviceWindow.parseTime(serviceWindow.endTime);
    var serviceWindowHours = serviceWindow.getServiceWindowHours();
    var allowWorkOnHolidays = isAllowWorkOnHoliday(wrId);
    
    var start_date = startDate;
    var start_hour = startDate.getHours();
    var availableHoursPerDay = (serviceWindowStartTime==serviceWindowEndTime)? 24 : (serviceWindowEndTime-serviceWindowStartTime);
   
    if(availableHoursPerDay!=24){
        while(estimateHours>0){
            if(!isWorkingDay(start_date,serviceWindow,allowWorkOnHolidays)){
                start_date = start_date.add(Date.DAY,1);
                continue;
            }
            
            //service window limit
            var appointment_date_start;
            var appointment_date_end;
            var appointment_estimate_hours=0;
            //the fist day assignment start time should based on user's drop time.
            if(start_date.getTime()==startDate.getTime()){
                appointment_date_start = start_date;
                if(appointment_date_start.getHours()+estimateHours > serviceWindowEndTime){
                    appointment_date_end = new Date(appointment_date_start.getFullYear(),appointment_date_start.getMonth(),appointment_date_start.getDate(),serviceWindowEndTime,0,0);
                    appointment_estimate_hours = serviceWindowEndTime -appointment_date_start.getHours();
                }else{
                    appointment_date_end = appointment_date_start.add(Date.SECOND,estimateHours*60*60);
                    appointment_estimate_hours =estimateHours;
                }
            }else{
                if(estimateHours > serviceWindowHours){
                    appointment_date_start = new Date(start_date.getFullYear(),start_date.getMonth(),start_date.getDate(),serviceWindowStartTime,0,0);
                    appointment_date_end = new Date(start_date.getFullYear(),start_date.getMonth(),start_date.getDate(),serviceWindowEndTime,0,0);
                    appointment_estimate_hours = serviceWindowHours;
                }else{
                    appointment_date_start = new Date(start_date.getFullYear(),start_date.getMonth(),start_date.getDate(),serviceWindowStartTime,0,0);
                    appointment_date_end = appointment_date_start.add(Date.SECOND,estimateHours*60*60);
                    appointment_estimate_hours = estimateHours;
                }
            }
            
            tasks.push({
                cfId: cfId,
                wrId: wrId,
                startDate: appointment_date_start,
                endDate: appointment_date_end,
                estimatedHours:appointment_estimate_hours
            });
            estimateHours = estimateHours - appointment_estimate_hours;
            start_date = start_date.add(Date.DAY,1);
        }
    }else{
        //allow 24 hours.
        var appointment_date_start=start_date;
        var appointment_date_end;
        var appointment_estimate_hours=0;
        
        for(date= appointment_date_start; estimateHours>0; date=getNextWorkingDay(date,serviceWindow,allowWorkOnHolidays)){
            
            var totalAvailableHoursPerDay = 24;
            //get task start date.
            if(appointment_date_start.getTime()==startDate.getTime()){
                appointment_date_start = start_date;
                
            }else{
                appointment_date_start = new Date(appointment_date_start.getFullYear(),appointment_date_start.getMonth(),appointment_date_start.getDate(),0,0,0);
            }
            
            //get total available hours per day.
            if(date.getTime() == startDate.getTime()){
                totalAvailableHoursPerDay = 24-appointment_date_start.getHours();
            }else{
                totalAvailableHoursPerDay = 24;
            }
               
            var assignHours = estimateHours-appointment_estimate_hours;
            var remainingHours = totalAvailableHoursPerDay-assignHours;
            
            if(remainingHours >= assignHours){
                appointment_date_end = appointment_date_start.add(Date.SECOND,estimateHours*60*60);
                appointment_estimate_hours =estimateHours;
                tasks.push({
                    cfId: cfId,
                    wrId: wrId,
                    startDate: appointment_date_start,
                    endDate: appointment_date_end,
                    estimatedHours:appointment_estimate_hours
                });
                estimateHours -= appointment_estimate_hours;
                break;
            }else{
                if(date.add(Date.DAY,1).getTime() !== getNextWorkingDay(date,serviceWindow,allowWorkOnHolidays).getTime()){
                    appointment_date_end = new Date(date.getFullYear(),date.getMonth(),date.getDate(),24,0,0);
                    appointment_estimate_hours =hoursBetween(appointment_date_start,appointment_date_end);

                    tasks.push({
                        cfId: cfId,
                        wrId: wrId,
                        startDate: appointment_date_start,
                        endDate: appointment_date_end,
                        estimatedHours:appointment_estimate_hours
                    });
                    appointment_date_start = getNextWorkingDay(date,serviceWindow,allowWorkOnHolidays);
                    
                    estimateHours -= appointment_estimate_hours;
                    appointment_estimate_hours = 0;
                }else{
                    appointment_estimate_hours +=totalAvailableHoursPerDay;
                }
            }
            
        }
    }
    
    return tasks;
}

/**
 * Help function to calculate the hour difference between two dates.
 * @param startDate
 * @param endDate
 * @returns {number}
 */
function hoursBetween(startDate, endDate) {
    var oneHour = 1000 * 60 * 60;

    var date1ms = startDate.getTime();
    var date2ms = endDate.getTime();

    return parseFloat(((date2ms - date1ms) / oneHour).toFixed(2));
}

/**
 * Get next working day.
 */
function getNextWorkingDay(currentDate,serviceWindow,allowWorkOnHolidays){
    var nextWorkDay =currentDate;
    var addToAdd =1;
    while(true){
        var nextDay = currentDate.add(Date.DAY,addToAdd);
        if(isWorkingDay(nextDay,serviceWindow,allowWorkOnHolidays)){
            nextWorkDay = nextDay;
            break;
        }else{
            addToAdd +=1;
        }
    }
    
    return nextWorkDay;
}

function isWorkingDay(date,serviceWindow,allowWorkOnHolidays){
    var result = true;
    var weekday = date.getDay();
    if (serviceWindow.days[weekday] == 0){
        result = false;
    }
    if(useHolidays(allowWorkOnHolidays)){
        if(isHoliday(date)){
            result = false;
        }
    }
    return result;
}

/**
 * Get estimate hours based on primary trade estimations.
 * @param cfId - Craftsperson Code
 * @param wrId - Work request code.
 * @returns
 */
function getEstimateHoursByPrimaryTrade(wrId,cfId){
    var estimateHours = 1;
    
    //get estimated hours to the work request labor assignment if the Trade of estimation matches the Primary Trade of the Craftsperson.
    var estimateHours = getWorkRequestRemainningTradeEstimateHours(wrId,cfId);
    if(estimateHours <= 0){
        estimateHours =1;
    }
    
    return estimateHours;
}

/**
 * Get primary trade of selected craftsperson.
 * @param cfId
 * @returns
 */
function getPrimaryTradeByCraftsperonId(cfId){
    var tradeCode = "";
    var restriction = new Ab.view.Restriction();
        restriction.addClause("cf.cf_id", cfId, '=');
    var parameters = {
        tableName: "cf",
        fieldNames: "[cf.cf_id,cf.tr_id]",
        restriction: toJSON(restriction) 
    };          
    var result = Workflow.runRuleAndReturnResult('AbCommonResources-getDataRecords', parameters);
    if (result.code == 'executed') {
        if (result.data.records.length > 0){
            if(valueExistsNotEmpty(result.data.records[0]['cf.tr_id'])){
                tradeCode = result.data.records[0]['cf.tr_id'];
            }
        }
    }
    
    return tradeCode;
}

/**
 * Get work request remaining estimate hours.
 * @param wrId
 * @param cfId
 * @returns
 */
function getWorkRequestRemainningTradeEstimateHours(wrId,cfId){
    var remainningHours = 0;
    //get primary trade of selected craftsperson.
    var cfPrimaryTrade = getPrimaryTradeByCraftsperonId(cfId);
    if(valueExistsNotEmpty(cfPrimaryTrade)){
        var tradeEstimateHours = parseFloat(getWorkRequestTradeEstimateHours(wrId,cfPrimaryTrade));
        var assignedHours = parseFloat(getTotalAssignedHours(wrId,cfPrimaryTrade));
        remainningHours = tradeEstimateHours - assignedHours;
    }

    return remainningHours;
}

/**
 * Get work request trade estimate hours of specific trade code.
 * @param wrId
 * @param trId
 * @returns
 */
function getWorkRequestTradeEstimateHours(wrId,trId){
    var estimateHours = 0;
    var restriction = new Ab.view.Restriction();
        restriction.addClause("wrtr.wr_id", wrId, '=');
        restriction.addClause("wrtr.tr_id", trId, '=');
    var parameters = {
        tableName: "wrtr",
        fieldNames: "[wrtr.wr_id,wrtr.tr_id,wrtr.hours_est]",
        restriction: toJSON(restriction) 
    };          
    var result = Workflow.runRuleAndReturnResult('AbCommonResources-getDataRecords', parameters);
    if (result.code == 'executed') {
        if (result.data.records.length > 0){
            if(valueExistsNotEmpty(result.data.records[0]['wrtr.hours_est'])){
                estimateHours = result.data.records[0]['wrtr.hours_est'];
            }
        }
    }
    return estimateHours;
}

/**
 * Get total assigned hours.
 * 
 * @param {wrId} Work request code.
 * @param {tradeId} Trade Code.
 */
function getTotalAssignedHours(wrId,tradeId){
    var assignedTotalHoursDs = View.dataSources.get('workRequestEstimateTotalHours');
    var totalAssignedHours = 0;
    var restriction = new Ab.view.Restriction();
        restriction.addClause('wrcf.wr_id',wrId,'=');
        restriction.addClause('cf.tr_id',tradeId,'=');
    var assignedTotalRecords = assignedTotalHoursDs.getRecords(restriction);
    if(assignedTotalRecords.length > 0){
        totalAssignedHours = assignedTotalRecords[0].getValue('wrcf.total_estimate_hours');
        if(!valueExistsNotEmpty(totalAssignedHours)){
            totalAssignedHours = 0;
        }
    }
    
    return totalAssignedHours;
}

/**
 * Format number value by local string.
 */
function formatNumberValue(value){
    var result = value;
    var taskDataSource  = View.dataSources.get('tasksDataSource');
    if(valueExistsNotEmpty(value)&&!isNaN(value)){
        value = Number(value);
        result = taskDataSource.formatValue('wrcf.hours_est',value);
    }
    return result;
}
