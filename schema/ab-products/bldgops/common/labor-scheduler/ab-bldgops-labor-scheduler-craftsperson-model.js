CraftspersonWorkScheduleModel = WorkScheduleModel.extend({
    /**
     * Selected craftserson from multiple schedule view.
     */
    selectedCraftsperson: null,
    /**
     * Craftsperson that compared with selected craftsperson.
     */
    comparedCraftsperson: null,

    /**
     * Current selected scheduler control view.
     */
    currentView: null,

    /**
     * Original scheduled tasks contain regular schedules. this variable is used to caculate craftsperson total for single craftsperson month view.
     */
    originalScheduledTasksWithRegularSchedules: null,
    /**
     * Constructor.
     * @param {config} The control configuration.
     */
    constructor: function(config) {
        this.inherit(config);
    },
    
    /**
     * Set craftsperson scheduled assignment completed.
     */
    setAssignmentCompleted: function(wrId,cfId,dateAssigned,timeAssigned,isScheduled){
        var updateMissedWorkDs = View.dataSources.get('updateMissedTaskDataSource');
        var record = new Ab.data.Record();
        record.isNew = false;
        record.setValue('wrcf.wr_id',wrId);
        record.setValue('wrcf.cf_id',cfId);
        record.setValue('wrcf.date_assigned',dateAssigned);
        record.setValue('wrcf.time_assigned',timeAssigned);
        record.setValue('wrcf.status','Complete');
        record.setOldValue('wrcf.wr_id',wrId);
        record.setOldValue('wrcf.cf_id',cfId);
        record.setOldValue('wrcf.date_assigned',dateAssigned);
        record.setOldValue('wrcf.time_assigned',timeAssigned);
        
        updateMissedWorkDs.saveRecord(record);
        
        //remove unscheduled task from cache array.
        if(isScheduled == '0'){
            // remove this task from unscheduled array.
            var dueDate=this.getDueDateByWrId(wrId);
            if(dueDate){
                this._removeUnscheduledTask(wrId,cfId,dateAssigned,timeAssigned);
            }
        }
    },

    /**
     * Move regular schedules for month view.
     */
    _moveRegularSchedulesForMonthView: function(cfId, startDate, endDate) {
        var craftsperson = this.craftspersonsById[cfId];
        if (this.currentView == 'month') {
            this.scheduledTasks.length = 0;
            this.copyArrayObject(this.originalScheduledTasksWithRegularSchedules, this.scheduledTasks);
        }
    },

    /**
     * @override.
     */
    loadSchedule: function(restriction, startDate, endDate) {
        this.inherit(restriction, startDate, endDate);

        if (this.currentView == 'month') {
            var scheduledTasksWithoutRegularScheduleTypes = _.filter(this.scheduledTasks, function(scheduleTask) {
                var result = false;
                if (scheduleTask.taskType != 'STANDARD' && scheduleTask.taskType != 'OVERTIME' && scheduleTask.taskType != 'DOUBLETIME') {
                    result = this;
                }
                return result;
            });
            this.originalScheduledTasksWithRegularSchedules = this.scheduledTasks;
            this.scheduledTasks.length = 0;
            this.copyArrayObject(scheduledTasksWithoutRegularScheduleTypes, this.scheduledTasks);
        }
    },

    /**
     * Filter craftsperson and only use the record when craftsperson.type='craftsperson'.
     */
    filterCraftsperson: function() {

        this.craftspersons = _.filter(this.craftspersons, function(craftsperson) {
            var result = false;
            if (craftsperson.type === 'craftsperson') {
                result = this;
            }
            return result;
        });
    },
    /**
     * Get day of week by day number.
     * @param {number} day of week.
     */
    getDayOfWeekByNumber: function(number) {
        var dayOfWeek = '';
        switch (number) {
        case 0:
            dayOfWeek = 'SUNDAY';
            break;
        case 1:
            dayOfWeek = 'MONDAY';
            break;
        case 2:
            dayOfWeek = 'TUESDAY';
            break;
        case 3:
            dayOfWeek = 'WEDNESDAY';
            break;
        case 4:
            dayOfWeek = 'THURSDAY';
            break;
        case 5:
            dayOfWeek = 'FRIDAY';
            break;
        case 6:
            dayOfWeek = 'SATURDAY';
            break;
        }

        return dayOfWeek;
    },

    /**
     * Copy array items to target array object.
     */
    copyArrayObject: function(array, targetArray) {
        for (var i = 0; i < array.length; i++) {
            targetArray.push(array[i]);
        }
    }
});