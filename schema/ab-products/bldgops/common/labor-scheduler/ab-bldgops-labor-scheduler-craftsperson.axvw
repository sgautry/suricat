<!-- This view displays a single craftsperson schedule.
     Functional specification: https://confluence.archibus.zone/display/AP/Specification+-+Labor+Scheduling+Board.
     Items 4.2 - 4.6. -->
<view version="2.0">
    <js file="dx.archibus.js"/>
    <css file="dx.common.css"/>
    <css file="dx.light.compact.css"/>
    
    <js file="Intl.complete.js"/>
    <js file="devextreme-intl.js"/>
    
    <css file="ab-bldgops-labor-scheduler-craftsperson-scheduler-control.css"/>
    <js file="ab-bldgops-labor-scheduler-craftsperson.js"/>
    <js file="ab-bldgops-labor-scheduler-craftsperson-scheduler-control.js"/>
    <js file="ab-bldgops-labor-scheduler-timeline-model.js"/>
    <js file="ab-bldgops-labor-scheduler-craftsperson-model.js"/>
    <js file="ab-bldgops-labor-scheduler-timeline-statistics.js"/>
    <js file="ab-bldgops-labor-scheduler-craftsperson-unschedules-filter-recent-search.js"/>
    <js file="ab-bldgops-labor-scheduler-util.js"/>
    
    <!-- Localized messages. -->
    
    <message name="siteVisitText" translatable="true">Site Visit</message>
    <message name="craftspersonDataFormTitle" translatable="true">Craftsperson Data: {0}</message>
    <message name="removeSelectedTaskMessage" translatable="true">Remove this selected task?</message>
    
    <!-- Messages for craftsperson view statistics -->
    <message name="availableHoursMessage">{0} Standard Hours</message>
    <message name="scheduledHoursMessage">{0} Scheduled Hours</message>
    <message name="assignedUnscheduledHoursMessage">{0} Unscheduled Hours({1} due this period)</message>
    <message name="remainingHoursMessage">{0} Remaining Hours</message>
    <message name="proratedUnscheduledHoursMessage">{0} Additional Prorated Unscheduled Hours</message>
    <message name="percentAllocatedMessage">{0} allocated</message>
    <message name="unscheduledHoursDueThisPeriodMessage">{0} Unscheduled Hours due this period</message>
    
    <!-- Messages for validate SLA service window. -->
    <message name="validateWorkingDay" translatable="true">This day ({0}) is not a working day according the Service Window</message>
    <message name="validateDuration" translatable="true">Duration must be greater than 0</message>
    <message name="validateEntryExists" translatable="true">Entry already exists for this day</message>
    <message name="validateServiceWindowHours" translatable="true">More hours to plan ({0}) than Service Window Hours ({1}) for a day</message>
    <message name="validateServiceWindowStartEndTime" translatable="true">Start and end must be in Service Window (between {0} and {1} hour)</message>
    <message name="validateHoliday" translatable="true">{0} is a holiday.</message>
    <message name="workRequestShouldBeAssignedAfterCurrentDate" translatable="true">Work request should not be assigned earlier than current time.</message>
    
    <message name="totalEstimateHours" translatable="true">Estimated Hours: {0}</message>
    <message name="totalRecords" translatable="true">Total Records: {0}</message>
    
    <message name="edit" translatable="true">Edit</message>
    <message name="Unschedule" translatable="true">Unschedule</message>
    
    <!-- Message for validate permissions when add/delete/update. -->
    <message name="stepCompletedMessage" translatable="true">This assignment can only be modified by the appointed Scheduler.</message>
    <message name="statusNotAorAAMessage" translatable="true">Work Request Status is {0}. You cannot modify work request assignments unless the Work Request Status is Approved or Assigned to Work Order.</message>
    <message name="hasPerformWorkHoursMessage" translatable="true">Cannot modify this Work Request Assignment because the craftsperson has already performed work.</message>
    <message name="hasNoPermissionMessage" translatable="true">User has no permission to modify this assignment.</message>
    
    <!-- Message for appointment pop-up. -->
    <message name="dueDate" translatable="true">Due: {0}</message>
    <message name="performDate" translatable="true">Perform: {0}</message>
    
    <!-- Message for DevExtreme schedule control -->
    <message name="timelineMonth" translatable="true">Month</message>
    <message name="timelineWeek" translatable="true">Week</message>
    <message name="timelineWorkWeek" translatable="true">Work Week</message>
    <message name="timelineDay" translatable="true">Day</message>
    <message name="timelineAllDay" translatable="true">All Day</message>
    
    <layout type="borderLayout" id="mainLayout">
        <center/>
        <east initialSize="440" split="true"/>
    </layout>
    
    <layout type="borderLayout" id="mainCenterLayout" containingLayout="mainLayout" region="center">
        <north id="scheduleConsoleRegion" initialSize="70" split="false"/>
        <center id="scheduleControlRegion"/>
        <south id="scheduleStatisticsRegion" initialSize="120" split="false"/>
    </layout>
    
    <layout type="borderLayout" id="mainEastLayout" containingLayout="mainLayout" region="east">
        <north id="unscheduledWorkRegion" initialSize="50%" split="true"/>
        <center id="missedWorkRegion"/>
    </layout>
    
    <style type="text/css">
        .unscheduleListItemDragImage {
		    position: absolute;
		    margin-left: 0px;
		    width: 112px;
		    background-color: rgb(51, 122, 183);
		    color: white;
		    padding: 8px;
		}
		.statisticsTitle {
		    font-weight:bold;
		}
		
		.statisticsComponent {
		    height:100%;
		    width: 50%;
		    float:left;
		}
    </style>

    <!-- Data source to query craftspersons. -->
    <dataSource id="craftspersonsDataSource">
        <table name="cf"/>
        <field name="cf_id"/>
        <field name="name"/>
        <field name="tr_id"/>

        <sortField name="tr_id" ascending="true"/>
        <sortField name="name" ascending="true"/>
    </dataSource>

    <!-- Data source to query scheduled and unscheduled tasks. -->
    <dataSource id="tasksDataSource">
        <!-- Set this parameter to true to include scheduled tasks into the query. -->
        <parameter name="includeScheduledHours" dataType="boolean" value="true"/>
        <!-- Set this parameter to true to include assigned unscheduled tasks into the query. -->
        <parameter name="includeAssignedUnscheduledHours" dataType="boolean" value="false"/>
        <!-- Set the dateStart and dateEnd parameters to query tasks within the dateRange. -->
        <parameter name="dateStart" dataType="date" value=""/>
        <parameter name="dateEnd" dataType="date" value=""/>

        <table name="wrcf" />
        <table name="wr" role="standard"/>
        <table name="cf" role="standard"/>
        <field name="cf_id"/>
        <field name="date_assigned"/>
        <field name="time_assigned"/>
        <field name="hours_est"/>
        <field name="wr_id"/>
        <field name="work_type"/>
        <field name="comments"/>
        <field name="status"/>
        <field name="date_escalation_completion" table="wr"/>
        <field name="description" table="wr"/>
        <!-- Use cf.tr_id field to apply the schedule filter restriction. -->
        <field name="tr_id" table="cf"/>

        <sortField name="cf_id" ascending="true" />
        <sortField name="date_start" ascending="true" />

        <restriction type="sql" enabled="includeScheduledHours" sql="wrcf.is_scheduled = 1 AND wrcf.status = 'Active' AND wrcf.date_assigned &gt;= ${parameters['dateStart']} AND wrcf.date_assigned &lt;= ${parameters['dateEnd']} AND ${sql.timestamp('wrcf.date_assigned', 'wrcf.time_assigned')} &gt;= ${sql.currentDate}"/>
        <restriction type="sql" enabled="includeAssignedUnscheduledHours" sql="wrcf.is_scheduled = 0  AND wr.date_escalation_completion &gt;= ${parameters['dateStart']} AND wr.date_escalation_completion &lt;= ${parameters['dateEnd']} AND wr.date_escalation_completion &gt;= ${sql.currentDate}"/>
    </dataSource>

    <!-- Data source to update work requests. -->
    <dataSource id="updateWorkRequestDataSource">
        <table name="wr"/>

        <field name="wr_id"/>
        <field name="description"/>
        <field name="date_escalation_completion"/>
    </dataSource>

    <!-- Data source to save scheduled tasks. -->
    <dataSource id="updateTaskDataSource">
        <table name="wrcf" />
        <table name="wr" role="standard"/>

        <field name="cf_id"/>
        <field name="wr_id"/>
        <field name="date_assigned"/>
        <field name="time_assigned"/>
        <field name="hours_est"/>
        <field name="is_scheduled"/>
        <field name="task_order"/>
        <field name="work_type"/>
        <field name="comments"/>
        <field name="hours_straight"/>
        <field name="hours_double"/>
        <field name="hours_over"/>
        <field name="status" table="wr"/>
        <field name="date_escalation_completion" table="wr"/>
    </dataSource>
    
    <dataSource id="updateMissedTaskDataSource">
        <table name="wrcf" />

        <field name="cf_id"/>
        <field name="wr_id"/>
        <field name="date_assigned"/>
        <field name="time_assigned"/>
        <field name="status"/>
    </dataSource>

    <!-- Data source to query craftsperson schedules. -->
    <dataSource id="craftspersonSchedulesDataSource">
        <table name="cf_schedules_assign"/>
        <table name="cf_schedules" role="standard"/>
        <field name="auto_number"/>
        <field name="cf_schedule_id"/>
        <field name="cf_id"/>
        <field name="is_seasonal"/>
        <field name="date_start"/>
        <field name="date_end"/>
        <field name="schedule_type" table="cf_schedules"/>
        <field name="schedule_name" table="cf_schedules"/>
    </dataSource>

    <!-- Data source to query craftsperson schedule days. -->
    <dataSource id="craftspersonScheduleDaysDataSource">
        <table name="cf_schedules_days"/>

        <field name="cf_schedule_id"/>
        <field name="day_of_week"/>
        <field name="time_start"/>
        <field name="time_end"/>
        <field name="rate_type"/>
        
        <!-- Restriction to getting scheduled days for specified craftspersons -->
        <parameter name="includeCraftsperson" dataType="verbatim" value="true"/>
        <parameter name="craftspesonCode" dataType="verbatim" value="text"/>
        <restriction type="sql" enabled="includeCraftsperson" sql="exists(select 1 from cf_schedules_assign where cf_schedules_assign.cf_id ='${parameters['craftspesonCode']}' AND cf_schedules_assign.cf_schedule_id=cf_schedules_days.cf_schedule_id)"/>
    </dataSource>

    <!-- Data source to query craftsperson schedule variances. -->
    <dataSource id="craftspersonScheduleVariancesDataSource">
        <table name="cf_schedules_variances"/>

        <field name="cf_id"/>
        <field name="date_start"/>
        <field name="date_end"/>
        <field name="time_start"/>
        <field name="time_end"/>
        <field name="sched_vars_type"/>
        <field name="site_id"/>
        <field name="bl_id"/>
    </dataSource>
    
    <!-- Unscheduled Task DataSource -->
    <dataSource id="unscheduledTaskDataSource">
        <table name="wrcf" role="main"/>
        <table name="wr" role="standard"/>
        <field table="wrcf" name="cf_id"/>
        <field table="wrcf" name="wr_id"/>
        <field table="wr" name="site_id"/>
        <field table="wr" name="bl_id"/>
        <field table="wr" name="fl_id"/>
        <field table="wr" name="rm_id"/>
        <field name="location" dataType="text">
            <sql dialect="generic">
                wr.bl_id${sql.concat}'-'${sql.concat}wr.fl_id${sql.concat}'-'${sql.concat}wr.rm_id
            </sql>
        </field>
        <field table="wr" name="description"/>
        <field table="wr" name="prob_type"/>
        <field table="wr" name="priority_label"/>
        <field table="wr" name="date_escalation_completion"/>
        <field table="wrcf" name="date_assigned"/>
        <field table="wrcf" name="time_assigned"/>
        <field table="wrcf" name="hours_est" showTotals="true"/>
        <field table="wrcf" name="is_scheduled"/>
        <restriction type="sql" sql="wrcf.status='Active' and wrcf.is_scheduled='0' and (wr.date_escalation_completion &gt;= ${sql.currentDate} OR wr.date_escalation_completion IS NULL)"/>
    </dataSource>
    
    <!--Missed work DataSource -->
    <dataSource id="missedTaskDataSource">
        <table name="wrcf" role="main"/>
        <table name="wr" role="standard"/>
        <field table="wrcf" name="cf_id"/>
        <field table="wrcf" name="wr_id"/>
        <field table="wr" name="site_id"/>
        <field table="wr" name="bl_id"/>
        <field table="wr" name="fl_id"/>
        <field table="wr" name="rm_id"/>
        <field name="location" dataType="text">
            <sql dialect="generic">
                wr.bl_id${sql.concat}'-'${sql.concat}wr.fl_id${sql.concat}'-'${sql.concat}wr.rm_id
            </sql>
        </field>
        <field table="wr" name="description"/>
        <field table="wr" name="prob_type"/>
        <field table="wr" name="priority_label"/>
        <field table="wr" name="date_escalation_completion"/>
        <field table="wrcf" name="date_assigned"/>
        <field table="wrcf" name="time_assigned"/>
        <field table="wrcf" name="hours_est" showTotals="true"/>
        <field table="wrcf" name="is_scheduled"/>
        <restriction type="sql" sql="wrcf.status='Active' AND ((wrcf.is_scheduled ='0' AND wr.date_escalation_completion &lt; ${sql.currentDate} AND wr.date_escalation_completion IS NOT NULL) OR (wrcf.is_scheduled ='1' and ${sql.timestamp('wrcf.date_assigned', 'wrcf.time_assigned')} &lt; ${sql.currentDate}))"/>
    </dataSource>
    
    <dataSource id="workRequestDataSource">
        <table name="wr" role="main"/>
        <table name="eq" role="standard"/>
        <table name="pms" role="standard"/>
        <field table="wr" name="wr_id"/>
        <field table="wr" name="tr_id"/>
        <field table="wr" name="description"/>
        <field table="wr" name="date_escalation_completion"/>
        <field table="wr" name="date_assigned"/>
        <field table="wr" name="priority"/>
        <field table="wr" name="priority_label"/>
        <field table="wr" name="site_id"/>
        <field table="wr" name="bl_id"/>
        <field table="wr" name="fl_id"/>
        <field table="wr" name="rm_id"/>
        <field table="wr" name="eq_id"/>
        <field table="eq" name="eq_std" />
        <field table="wr" name="pmp_id"/>
        <field table="wr" name="pms_id"/>
        <field table="pms" name="criticality" />
        <field table="wr" name="prob_type"/>
        <field table="wr" name="serv_window_days"/>
        <field table="wr" name="serv_window_start"/>
        <field table="wr" name="serv_window_end"/>
        <field table="wr" name="allow_work_on_holidays"/>
    </dataSource>
    
    <!-- The data source for work request trades. -->
    <dataSource id="workRequestTradesDataSource">
        <table name="wrtr" role="main"/>
        <table name="wr" role="standard"/>
        <field table="wrtr" name="wr_id"/>
        <field table="wrtr" name="tr_id"/>
        <field table="wrtr" name="hours_est"/>
        <field table="wr" name="site_id" />
        <field table="wr" name="bl_id"/>
        <field table="wr" name="prob_type"/>
        <field table="wr" name="eq_id"/>
    </dataSource>
    
    <dataSource id="workRequestEstimateTotalHours" type="grouping">
        <table name="wrcf" role="main"/>
        <table name="cf" role="standard"/>
        <field table="wrcf" name="wr_id" groupBy="true"/>
        <field table="cf" name="tr_id" groupBy="true"/>
        <field name="total_estimate_hours" formula="sum" baseField="wrcf.hours_est" dataType="number" size="10" decimals="2"/>
    </dataSource>
    
    <!-- This datasource only be used to check whether user has permission to make changes to the selected work request -->
    <dataSource id="craftspersonWorkRequestDs">
        <table name="wr" role="main"/>
        <field table="wr" name="wr_id"/>
        <restriction type="sql" sql="(wr.supervisor = ${sql.literal(user.employee.id)} OR (wr.supervisor IS NULL AND wr.work_team_id in (select cf_work_team.work_team_id from cf_work_team,cf where cf.email=${sql.literal(user.email)} AND cf_work_team.cf_id=cf.cf_id AND cf.is_supervisor='1'))) AND wr.status IN ('A', 'AA', 'I')"/>
    </dataSource>
    
    <!-- Datasource to format date and time values by user locale. -->
    <dataSource id="dateTimeFormateDs">
        <table name="wrcf" role="main"/>
        <field table="wrcf" name="wr_id"/>
        <field table="wrcf" name="cf_id"/>
        <field table="wrcf" name="date_assigned"/>
        <field table="wrcf" name="time_assigned"/>
    </dataSource>

    <panel type="console" id="craftspersonForm" dataSource="craftspersonsDataSource" layoutRegion="scheduleConsoleRegion">
        <title>Craftsperson Data</title>
        <action id="backToWork">
            <title>Back</title>
        </action>
        <fieldset layout="fluid">
            <field table="cf" name="cf_id">
                <title translatable="true">Compare to</title>
                <action id="abEamDefAssetByCountEq_form_selValEqStd">
                <title translatable="false">...</title>
                <tooltip>Select Value</tooltip>      
                <command type="selectValue"
                        fieldNames="cf.cf_id"
                        selectFieldNames="cf.cf_id"
                        visibleFieldNames="cf.cf_id,cf.name,cf.tr_id"
                        actionListener="afterSelectComparedCraftsperson"
                        restriction="cf.in_house='1' AND (cf.work_team_id IS NULL OR exists (select 1 from cf_work_team where cf_work_team.cf_id=cf.cf_id and cf_work_team.work_team_id in (select cf_work_team.work_team_id from cf_work_team,cf where cf_work_team.cf_id=cf.cf_id AND cf.email=${sql.literal(user.email)} AND cf.is_supervisor='1')))"
                        >
                    <title>Select Craftsperson</title>  
                </command>         
            </action>
            </field>
            <action id="clear">
               <title>Clear</title>
            </action>
            <action id="compare" mainAction="true">
               <title>Compare</title>
            </action>
         </fieldset>
    </panel>
    
    <!-- Work schedule control for single craftsperson. -->
    <panel id="singleLaborWorkSchedule" type="html" layoutRegion="scheduleControlRegion">
        <html>
            <div id="singleLaborWorkScheduleContainer" class="scheduler">
            </div>
        </html>
    </panel>
    
    <!-- Craftspersons statistics. -->
    <panel id="craftspersonScheudleStatisticsPanel" type="html" layoutRegion="scheduleStatisticsRegion">
        <html>
            <div id="statisticsComponent"></div>
        </html>
    </panel>
    
    <panel type="grid" id="unscheduledWorkList" dataSource="unscheduledTaskDataSource" showCounts="true" layoutRegion="unscheduledWorkRegion" showOnLoad="false" showIndex="false" recordLimit="30">
        <title translatable="true">Unscheduled Work</title>
        <action id="toolsMenuForScheduledWork" type="menu" imageName="/schema/ab-core/graphics/icons/view/gear.png">
             <title></title>
             <action>
                  <title>Select fields</title>
                  <command type="selectFields" panelId="unscheduledWorkList"/>
             </action>
        </action>
        <event type="onClickItem">
            <command type="callFunction" functionName="craftspersonScheduleCtrl.openAssignmentDetailPopup('unscheduledWorkList')"/>
        </event>
        <indexField table="wr" name="prob_type"/>
        <field id="remove" controlType="image" imageName="schema/ab-products/bldgops/common/graphics/delete-blue.png"> 
             <tooltip translatable="true">Remove Assignment</tooltip>
             <command type="callFunction" functionName="craftspersonScheduleCtrl.onRemoveWorkTaskListItem('unscheduledWorkList')"/>
        </field>
        <field table="wrcf" name="cf_id" hidden="true" controlType="link"/>
        <field table="wrcf" name="wr_id" hidden="true" controlType="link"/>
        <field table="wr" name="description" hidden="true" controlType="link"/>
        <field table="wr" name="prob_type" controlType="link"/>
        <field name="location" dataType="text" controlType="link">
            <title translatable="true">Location</title>
        </field>
        <field table="wr" name="priority_label" controlType="link"/>
        <field table="wrcf" name="hours_est" hidden="true" controlType="link"/>
        <field table="wr" name="date_escalation_completion" controlType="link">
            <title translatable="true">Due Date</title>
        </field>
        <field table="wrcf" name="is_scheduled" hidden="true"/>
    </panel>
    
    <panel type="grid" id="missedWorkList" dataSource="missedTaskDataSource" showCounts="true" layoutRegion="missedWorkRegion" showOnLoad="false" showIndex="false" recordLimit="30">
        <title translatable="true">Missed Work</title>
        <action id="toolsMenuForMissedWork" type="menu" imageName="/schema/ab-core/graphics/icons/view/gear.png">
             <title></title>
             <action>
                  <title>Select fields</title>
                  <command type="selectFields" panelId="missedWorkList"/>
             </action>
        </action>
        <event type="onClickItem">
            <command type="callFunction" functionName="craftspersonScheduleCtrl.openAssignmentDetailPopup('missedWorkList')"/>
        </event>
        <indexField table="wr" name="prob_type"/>
        <field id="remove" controlType="image" imageName="schema/ab-products/bldgops/common/graphics/delete-blue.png"> 
             <tooltip translatable="true">Remove Assignment</tooltip>
             <command type="callFunction" functionName="craftspersonScheduleCtrl.onRemoveWorkTaskListItem('missedWorkList')"/>
        </field>
        <field table="wrcf" name="cf_id" hidden="true" controlType="link"/>
        <field table="wrcf" name="wr_id" hidden="true" controlType="link"/>
        <field table="wr" name="description" hidden="true" controlType="link"/>
        <field table="wr" name="prob_type" controlType="link"/>
         <field name="location" dataType="text" controlType="link">
            <title translatable="true">Location</title>
        </field>
        <field table="wr" name="priority_label" controlType="link"/>
        <field table="wrcf" name="hours_est" hidden="true" controlType="link"/>
        <field table="wr" name="date_escalation_completion" controlType="link">
            <title translatable="true">Due Date</title>
        </field>
        <field table="wrcf" name="is_scheduled" hidden="true" controlType="link"/>
    </panel>
</view>