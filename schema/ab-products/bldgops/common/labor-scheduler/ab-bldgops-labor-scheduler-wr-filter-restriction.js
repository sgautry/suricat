var laborSchedulerWorkRequestFilterRestrictionCrtl = View.createController('laborSchedulerWorkRequestFilterRestrictionCrtl',{
    /**
     * Get restriction from filter fields.
     */
    getFilterRestriction: function() {
        var filterRestriction = "1=1";
        var consoleMoreOptionsFormPanel = View.panels.get('workRequestMoreOptionsFilter');
        var escalatedRes = this.getEscalatedRes();
        var workTypeRes = this.getWorkTypeRes();
        var statusRes = this.getStatusRes(consoleMoreOptionsFormPanel);
        var partStatusRes = this.getPartStatusRes(consoleMoreOptionsFormPanel);
        var costRes = this.getCostRes();
        var dateRequiredRes = this.getDateRes(consoleMoreOptionsFormPanel, 'wr.date_requested');
        var dateAssignedRes = this.getDateRes(consoleMoreOptionsFormPanel, 'wr.date_assigned');
        var returnedRes = this.getReturnedRes();
        var descriptionRes = this.getDescriptionRes(consoleMoreOptionsFormPanel);
        var eqStdRes = this.getEquipmentStandardRes(consoleMoreOptionsFormPanel);

        var unscheduledRes = this.getUnscheduledRes();

        filterRestriction += escalatedRes
                + workTypeRes
                + statusRes
                + partStatusRes
                + costRes
                + dateRequiredRes
                + dateAssignedRes
                + returnedRes
                + descriptionRes
                + eqStdRes
                + unscheduledRes
                + " AND "
                + this.addRestrictionFromFieldArray("workRequestMoreOptionsFilter", [ 'wr.fl_id', 'wr.rm_id', 'wr.requestor', 'wr.dv_id', 'wr.dp_id', 'wr.eq_id', 'wr.wr_id',
                        'wr.wo_id', 'wr.work_team_id', 'wrcf.cf_id', 'wr.pmp_id', 'wr.pms_id', 'wrpt.part_id', 'wr.priority_label' ]) + " AND "
                + this.addRestrictionFromFieldArray("workRequestsFilter", [ 'wr.site_id', 'wr.bl_id', 'wr.tr_id', 'wr.prob_type' ]);

        //APP-1501: Append restriction of selected work reqeusts from Ops Console view.
        if(valueExistsNotEmpty(View.parameters)&&valueExistsNotEmpty(View.parameters['selectedWrIds'])){
            var selectedWorkRequestsIds = View.parameters['selectedWrIds'];
            if(selectedWorkRequestsIds){
                filterRestriction += this.getSelectedWorkRequestsFromOpsConsole(selectedWorkRequestsIds);
            }
        }
        
        return filterRestriction;
    },

    /**
     * get escalated restriction in big filter
     */
    getEscalatedRes: function() {
        var escalatedRes = '';
        // escalated
        var escalated = Ext.get('escalated').dom.checked;
        if (escalated) {
            escalatedRes += " AND (wr.escalated_completion = 1 OR wr.escalated_response = 1)";
        }

        return escalatedRes;
    },

    /**
     * get work type restriction in big filter
     */
    getWorkTypeRes: function() {
        var workTypeRes = '';
        // work type
        var workType = this.getSelectBoxValue('worktype');
        if (workType == 'OD') {
            workTypeRes += " AND wr.prob_type != 'PREVENTIVE MAINT'";
        } else if (workType == 'PM') {
            workTypeRes += " AND wr.prob_type = 'PREVENTIVE MAINT'";
        }

        return workTypeRes;
    },

    /**
     * get status restriction in big filter
     */
    getStatusRes: function(consolePanel) {
        var statusRes = '';
        // status
        var statusValues = consolePanel.getCheckboxValues('wr.status');
        var statuses = [];
        for (var i = 0; i < statusValues.length; i++) {
            var status = statusValues[i];
            if (status == 'H') {
                statuses.push('HA');
                statuses.push('HL');
                statuses.push('HP');
            } else {
                statuses.push(status);
            }
        }
        if (statuses.length > 0) {
            var statusInClause = "('" + statuses[0] + "'";
            for (i = 1; i < statuses.length; i++) {
                statusInClause += " ,'" + statuses[i] + "'";
            }
            statusInClause += ")";
            statusRes += " AND wr.status IN " + statusInClause;
        }

        return statusRes;
    },

    /**
     * get part status restriction in more option filter.
     */
    getPartStatusRes: function(consolePanel) {
        var statusRes = '';
        // status
        var statusValues = consolePanel.getCheckboxValues('wrpt.status');
        var statuses = [];
        for (var i = 0; i < statusValues.length; i++) {
            var status = statusValues[i];
            statuses.push(status);
        }
        if (statuses.length > 0) {
            var statusInClause = "('" + statuses[0] + "'";
            for (i = 1; i < statuses.length; i++) {
                statusInClause += " ,'" + statuses[i] + "'";
            }
            statusInClause += ")";
            statusRes += " AND exists(select 1 from wrpt where wrpt.wr_id=wr.wr_id and wrpt.status IN " + statusInClause + ')';
        }

        return statusRes;
    },

    /**
     * Get equipment standard restriction for more options filter.
     */
    getEquipmentStandardRes: function(consolePanel) {
        var eqStdRes = '';
        var eqStdValues = consolePanel.getFieldMultipleValues('eq.eq_std');
        for (var i = 0; i < eqStdValues.length; i++) {
            var eqStd = eqStdValues[i];
            if (eqStd != '') {
                eqStdRes += " OR exists(select 1 from eq where eq.eq_std like '" + eqStd + "%' and eq.eq_id=wr.eq_id)";
            }
        }

        if (eqStdRes) {

            eqStdRes = ' AND (' + eqStdRes.substring(3) + ')';

        }

        return eqStdRes;
    },

    /**
     * get cost restriction in big filter
     */
    getCostRes: function() {
        var costRes = '';
        // cost estimated
        var operator = this.getSelectBoxValue('operator');
        var cost = Ext.get('wr.cost_est_total').dom.value;
        if (operator != '') {
            if (operator == 'IS NULL' || operator == 'IS NOT NULL') {
                costRes += " AND wr.cost_est_total " + operator;
            } else if (cost != '') {
                costRes += " AND wr.cost_est_total " + operator + cost;
            }
        }

        return costRes;
    },

    /**
     * get date field restriction in big filter
     */
    getDateRes: function(consolePanel, dateFieldName) {
        var dateRes = '';
        var dateFrom = consolePanel.getFieldValue(dateFieldName + ".from");
        var dateTo = consolePanel.getFieldValue(dateFieldName + ".to");
        if (valueExistsNotEmpty(dateFrom) && valueExistsNotEmpty(dateTo)) {
            var objDateFrom = consolePanel.getDataSource().parseValue(dateFieldName + ".from", dateFrom, false);
            var objDateTo = consolePanel.getDataSource().parseValue(dateFieldName + ".to", dateTo, false);
        }

        // add the date comparison clauses
        if (valueExistsNotEmpty(dateFrom)) {
            dateRes += " AND " + dateFieldName + ">=${sql.date('" + dateFrom + "')}";
        }
        if (valueExistsNotEmpty(dateTo)) {
            dateRes += " AND " + dateFieldName + "<=${sql.date('" + dateTo + "')}";
        }

        return dateRes;
    },

    /**
     * get Returned restriction in big filter
     */
    getReturnedRes: function() {
        var returnedRes = '';
        // returned
        var returned = Ext.get('wr.returned').dom.checked;
        if (returned) {
            returnedRes += " AND (exists(select 1 from wrcf where wrcf.wr_id = wr.wr_id and wrcf.status='Returned'))";
        }

        return returnedRes;
    },

    /**
     * get description restriction in big filter
     */
    getDescriptionRes: function(consolePanel) {
        var descriptionRes = '';
        // escalated
        var description = consolePanel.getFieldValue('wr.description');
        if (valueExistsNotEmpty(description)) {

            if (description.length > 1 && description.substring(0, 1) == "'" && description.substring(description.length - 1, description.length) == "'") {
                descriptionRes += " AND (wr.description='" + description.substring(1, description.length - 1).replace(/\'/g, "''") + "')";
            } else {
                descriptionRes += " AND (wr.description like '%" + description.replace(/\'/g, "''") + "%')";
            }
        }

        return descriptionRes;
    },

    /**
     * get unscheduled restriction in less option filter.
     */
    getUnscheduledRes: function() {
        var unscheduledRes = '';
        var unscheduled = Ext.get('unscheduled').dom.checked;
        if (unscheduled) {
            unscheduledRes += " AND (not exists(select 1 from wrcf where wrcf.wr_id=wr.wr_id)  or exists(select 1 from wrtr where wrtr.wr_id=wr.wr_id and ${sql.isNull('wrtr.hours_est','0')} > (select ${sql.isNull('sum(hours_est)','0')} from wrcf,cf where wrcf.wr_id=wrtr.wr_id and cf.cf_id=wrcf.cf_id and cf.tr_id=wrtr.tr_id)))";
        }

        return unscheduledRes;
    },

    /**
     * add restriction from field array
     */
    addRestrictionFromFieldArray: function(panelId, fieldArray) {
        var restriction = '1=1';
        for (var i = 0; i < fieldArray.length; i++) {
            var fieldName = fieldArray[i];
            var queryParameter = View.panels.get(panelId).getFieldQueryParameter(fieldName);

            if (queryParameter != " IS NOT NULL") {
                restriction += " AND ";

                if (fieldName == 'wrcf.cf_id') {
                    restriction += ' EXISTS(SELECT 1 FROM wrcf WHERE wrcf.wr_id = wr.wr_id AND ' + fieldName + queryParameter + ')';
                } else if (fieldName == 'wrpt.part_id') {
                    restriction += ' EXISTS(SELECT 1 FROM wrpt WHERE wrpt.wr_id = wr.wr_id AND ' + fieldName + queryParameter + ')';
                } else if (fieldName == 'wr.tr_id') {
                    fieldName = 'wrtr.tr_id';
                    restriction += ' EXISTS(SELECT 1 FROM wrtr WHERE wrtr.wr_id = wr.wr_id AND ' + fieldName + queryParameter + ')';
                } else {
                    restriction += fieldName + queryParameter;
                }
            }
        }

        return restriction;
    },
    
    /**
     * Get restriction of selected work request from Ops console view.
     */
    getSelectedWorkRequestsFromOpsConsole: function(selectedWorkRequestsIds){
        var opsConsoleSelectedWorkRequestsRes = '';
        if (selectedWorkRequestsIds.length > 0) {
            var workReqeustsInClause = "('" + selectedWorkRequestsIds[0] + "'";
            for (i = 1; i < selectedWorkRequestsIds.length; i++) {
                workReqeustsInClause += " ,'" + selectedWorkRequestsIds[i] + "'";
            }
            workReqeustsInClause += ")";
            opsConsoleSelectedWorkRequestsRes += " AND (wr.wr_id IN " + workReqeustsInClause+")";
        }
        
        return opsConsoleSelectedWorkRequestsRes;
    },

    /**
     * Helper for getting the value of a drop down box
     * @param id Id of the drop down
     */
    getSelectBoxValue: function(id) {
        var el = Ext.get(id).dom;
        return el.options[el.selectedIndex].value;
    }                
});