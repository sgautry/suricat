View.createController('defSlaPriorityController', {

	/**
	 * Save Priority Form.
	 */
	slaPriorityForm_onSave : function() {
		if(this.checkDefaultPriority()){
			this.slaPriorityForm.save();
		}
		
		this.slaPriorityList.refresh();
	},
	
	/**
	 * check default Priority.
	 */
	checkDefaultPriority : function() {
		var form = this.slaPriorityForm;
		var isDeaultOD = form.getFieldValue('helpdesk_sla_priority.is_default_od');
		var isDeaultPM = form.getFieldValue('helpdesk_sla_priority.is_default_pm');
		var priorityId = form.getFieldValue('helpdesk_sla_priority.priority_id');
		
		if(isDeaultOD == '1'){
			var restriction = new Ab.view.Restriction();
			restriction.addClause('helpdesk_sla_priority.is_default_od', '1', '=');
			if(priorityId){
				restriction.addClause('helpdesk_sla_priority.priority_id', priorityId, '!=');
			}
			
			var records = this.slaPriorityDS.getRecords(restriction);
			if(records.length > 0){
				View.showMessage(getMessage('duplicateDefaultPriority'));
				return false;
			}
		}
		
		if(isDeaultPM == '1'){
			var restriction = new Ab.view.Restriction();
			restriction.addClause('helpdesk_sla_priority.is_default_pm', '1', '=');
			if(priorityId){
				restriction.addClause('helpdesk_sla_priority.priority_id', priorityId, '!=');
			}
			
			var records = this.slaPriorityDS.getRecords(restriction);
			if(records.length > 0){
				View.showMessage(getMessage('duplicateDefaultPriority'));
				return false;
			}
		}
		
		return true;
	}
});
