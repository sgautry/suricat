/**
 * Controller for the bulk work request update.
 */
var wrBulkUpdate=View.createController('wrBulkUpdate', {

	records : null,
	
	/**
	 * After view loaded.
	 */
	afterViewLoad : function() {
		// KB3016857 -Allow craftspersons to be members of more than one team
		var cfSelectValueRestriction = 'cf.work_team_id IS NULL OR cf.work_team_id IN (select cf.work_team_id from cf where cf.email= ${sql.literal(user.email)})';
		if (Workflow.callMethod('AbBldgOpsOnDemandWork-WorkRequestService-checkSchemaExisting', 'cf_work_team', 'cf_id').value) {
			cfSelectValueRestriction = "cf.work_team_id IS NULL OR cf.cf_id IN (SELECT cf_work_team.cf_id FROM cf_work_team WHERE cf_work_team.work_team_id IN (SELECT cf_work_team.work_team_id FROM cf_work_team,cf where cf_work_team.cf_id = cf.cf_id and cf.email= ${sql.literal(user.email)}))";
		}
		
		this.wrcfForm.fields.get("wrcf.cf_id").actions.get(0).command.commands[0].dialogRestriction = cfSelectValueRestriction;
	},

	/**
	 * After initial data fetch. collapse some panels.
	 */
	afterInitialDataFetch : function() {
		var isPlanner = this.cfPlannerDS.getRecord().getValue('cf.is_planner');
		
		//KB#3054493 Show equipment status and equipment condition fields dropdown list items.
		this.setOptionItemToEqFields();
		
		// set field values for multiple work request
		var wrRecords = View.getOpenerView().WRrecords;
		if (valueExistsNotEmpty(wrRecords)) {
			this.setValueForMultipleWr(wrRecords);
		}
		
		var selectedWrRecordsForAction = View.getOpenerView().controllers.get("opsConsoleWrListActionController").selectedWrRecordsForAction;
		for(var i=0;i<selectedWrRecordsForAction.length;i++){
			if(selectedWrRecordsForAction[i].getValue('wr.isRequestSupervisor') == 'false' && isPlanner !='1' ){
				this.wrUpdates.actions.get('addCf').show(false);
				break;
			}
		}
		
		for(var i=0;i<selectedWrRecordsForAction.length;i++){
			if(selectedWrRecordsForAction[i].getValue('wr.isRequestSupervisor') == 'false' && selectedWrRecordsForAction[i].getValue('wr.isRequestCraftsperson') == 'true' ){
				jQuery('#forwardIssuedRequest').hide();
				break;
			}
		}
		
		
		
	},
	
	/**
	 * forward all selected work requests
	 */
	wrUpdates_onForwardIssuedRequest : function() {
		var wrRecords = this.records;
		var wrIdList = [];
		if (valueExistsNotEmpty(wrRecords)) {
			for ( var i = 0; i < wrRecords.length; i++) {
				var record = wrRecords[i];
				var wrId = record.getValue('wr.wr_id');
				wrIdList.push(parseInt(wrId));
			}
			
			this.forwardForm.forwardIssuedRequest = true;
			this.forwardForm.wrIdList = wrIdList;
			jQuery('#forwardForm_forward_comments_labelCell').parent().show();
			this.forwardForm.showInWindow({
				x : 10,
				y : 100,
				modal : true,
				width : 600,
				height : 300,
				title : getMessage('forwordFormTitle')
			});
		}

	},

	/**
	 * update selected work request
	 */
	wrUpdates_onUpdateRequest : function() {
		var wrRecords = this.records;
		if (valueExistsNotEmpty(wrRecords)) {
			for ( var i = 0; i < wrRecords.length; i++) {
				var record = wrRecords[i];
				var wrId = record.getValue('wr.wr_id');
				var status = record.getValue('wr.status');
				record = this.updateValueForEdit(record);

				try {
					Workflow.callMethod('AbBldgOpsOnDemandWork-WorkRequestService-updateWorkRequestStatus', record.values, status);
				} catch (e) {
					Workflow.handleError(e);
				}
				
				//save Equipment status and Equipment condition field value when those value not equal with '<VARIES>'
				var eqId=record.getValue('wr.eq_id');
				var eqFieldContainer=$('wrUpdates_vf_Bulk_EqStatus_labelCell').parentElement.style.display;
				if(valueExistsNotEmpty(eqId)&&eqFieldContainer!="none"){
					var eqStatus=$('bulk_EqStatus').options[$('bulk_EqStatus').selectedIndex].value;
					var eqCondition=$('bulk_EqCondition').options[$('bulk_EqCondition').selectedIndex].value;
					var eqRes=new Ab.view.Restriction();
						eqRes.addClause('eq.eq_id',eqId,'=');
					
					//get original eq status and condition field value in database.
					var oldStatus=this.getFieldValueFromDs('eq','status','eqInforDs',eqRes);
					var oldCondition=this.getFieldValueFromDs('eq','condition','eqInforDs',eqRes);
					
					if(oldStatus!=eqStatus||oldCondition!=eqCondition){
						var eqRecord=this.eqInforDs.getRecord(eqRes);
						if(oldStatus!=eqStatus&&eqStatus!='varies'){
							eqRecord.setValue('eq.status',eqStatus);
						}
						if(oldCondition!=eqCondition&&eqCondition!='varies'){
							eqRecord.setValue('eq.condition',eqCondition);
						}
						
						this.eqInforDs.saveRecord(eqRecord);
					}
					
				}
			}
		}

		View.closeThisDialog();
	},
	
	/**
	 * get field value from dataSource.
	 * 
	 * @param {tableName} table name
	 * @param {fieldName} field name
	 * @param {dataSourceName} DataSource Name
	 * @param {res} Restriction
	 */
	getFieldValueFromDs: function(tableName,fieldName,dataSourceName,res){
		var result='';
		var records=View.dataSources.get(dataSourceName).getRecords(res);
		if(records.length>0){
			result=records[0].getValue(tableName+'.'+fieldName);
		}
		
		return result;
	},

	/**
	 * set field values for multiple work request
	 */
	setValueForMultipleWr : function(wrRecords) {
		var restriction = new Ab.view.Restriction();
		if (wrRecords.length > 1) {
			var wrIds = [];
			for ( var i = 0; i < wrRecords.length; i++) {
				wrIds.push(wrRecords[i]['wr.wr_id']);
			}
			restriction.addClause('wr.wr_id', wrIds, 'IN');
		} else {
			restriction.addClause('wr.wr_id', wrRecords[0]['wr.wr_id'], '=');
		}

		this.records = this.wrDetailsDS.getRecords(restriction);

		var fields = [ 'wr.ac_id', 'wr.cause_type', 'wr.repair_type', 'wr.cf_notes' ];
		for ( var i = 0; i < fields.length; i++) {
			this.setFieldValue(fields[i], this.records);
		}
		
		//KB#3054493 Add fields to the Work Request Details form: Equipment Status and Equipment Condition.
		this.setFieldValuesOfEq(this.records);
	},

	/**
	 * set field value in the form for multiple records
	 */
	setFieldValue : function(fieldName, records) {
		var fieldValue = records[0].getValue(fieldName);
		for ( var i = 0; i < records.length; i++) {
			if (records[i].getValue(fieldName) != fieldValue) {
				fieldValue = '<VARIES>';
				break;
			}
		}

		this.wrUpdates.setFieldValue(fieldName, fieldValue);
	},
	/**
	 * Add fields to the Work Request Details form: Equipment Status and Equipment Condition.
	 */
	setFieldValuesOfEq: function(records){
		var hasEqId=false;
		var eqStatus=this.getEqStatusFieldValue(records[0].getValue("wr.eq_id"));
		var eqCondition=this.getEqConditionFieldValue(records[0].getValue("wr.eq_id"));
		for(var i=0; i<records.length;i++){
			var eqId=records[i].getValue('wr.eq_id');
			if(valueExistsNotEmpty(eqId)){
				hasEqId=true;
			}
			
			//check whether all records have the same field value,if not the same ,set fields option {'varies','<VARIES>'}
			if(eqStatus!=this.getEqStatusFieldValue(eqId)){
				eqStatus='varies';
			}
			
			if(eqCondition!=this.getEqConditionFieldValue(eqId)){
				eqCondition='varies';
			}	
		}
		
		if(hasEqId){
			//show eq status and eq condition field value.
			$('wrUpdates_vf_Bulk_EqStatus_labelCell').parentElement.style.display="";
			$('wrUpdates_vf_Bulk_EqCondition_labelCell').parentElement.style.display="";
			this.setSelectedOptionValue($('bulk_EqStatus'),eqStatus);
			this.setSelectedOptionValue($('bulk_EqCondition'),eqCondition);
		}else{
			//hide eq status and eq condition field value.
			$('wrUpdates_vf_Bulk_EqStatus_labelCell').parentElement.style.display="none";
			$('wrUpdates_vf_Bulk_EqCondition_labelCell').parentElement.style.display="none";
		}
	},
	
	/**
	 * Set drop down list control element option item value.
	 */
	setSelectedOptionValue: function(selectElement,selectedValue){
		var options=selectElement.options;
		for(var i=0;i<options.length;i++){
			if(selectElement.options[i].value==selectedValue){
				selectElement.selectedIndex=i;
				break;
			}
		}
	},
	/**
	 * Get equipment status field value by equipment code.
	 * @param {eqId} Equipment Code.
	 */
	getEqStatusFieldValue: function(eqId){
		var eqStatus="";
		if(valueExistsNotEmpty(eqId)){
			var res=new Ab.view.Restriction();
				res.addClause('eq.eq_id',eqId,'=');
			var eqRecord=this.eqInforDs.getRecord(res);
			eqStatus=eqRecord.getValue('eq.status');
		}
		return eqStatus;
	},
	
	/**
	 * Get equipment condition field value by equipment code.
	 * @param {eqId} Equipment Code.
	 */
	getEqConditionFieldValue: function(eqId){
		var eqCondition="";
		if(valueExistsNotEmpty(eqId)){
			var res=new Ab.view.Restriction();
				res.addClause('eq.eq_id',eqId,'=');
			var eqRecord=this.eqInforDs.getRecord(res);
			eqCondition=eqRecord.getValue('eq.condition');
		}
		return eqCondition;
	},

	/**
	 * update value for multiple edit panel
	 */
	updateValueForEdit : function(record) {
		var fields = [ 'wr.ac_id', 'wr.cause_type', 'wr.repair_type', 'wr.cf_notes' ];
		for ( var i = 0; i < fields.length; i++) {
			var fieldValue = this.wrUpdates.getFieldValue(fields[i]);
			if (fieldValue != '<VARIES>') {
				record.setValue(fields[i], fieldValue);
			}
		}

		return this.wrDetailsDS.processOutboundRecord(record);
	},
	
	/**
	 * Template function to get selected record from dialog from
	 */
	openFindPartDialog: function(){
		var form = View.panels.get('wrptForm');
		var wrIds = [];
		
		var wrRecords = View.getOpenerView().WRrecords;
		
		if (wrRecords.length > 1) {
			for ( var i = 0; i < wrRecords.length; i++) {
				wrIds.push(wrRecords[i]['wr.wr_id']);
			}
		} else {
			wrIds.push(wrRecords[0]['wr.wr_id']);
		}
		
		//Define parameter panel
		View.parameterPanel=View.panels.get('wrptForm');
		//Test 'XC' building
		View.workRequestIds= wrIds;
		
		View.openDialog('ab-bldgops-find-parts.axvw',null,false,{maximize: true, title: getMessage('findPartDialogTitle'), width:1200, height:1000});
	},
	
	/**
	 * Add dropdown list items.
	 */
	setOptionItemToEqFields: function(){
		var statusEnumListItems=this.eqInforDs.fieldDefs.get('eq.status').enumValues;
		var conditionEnumListItems=this.eqInforDs.fieldDefs.get('eq.condition').enumValues;
		var eqStatusElement=$('bulk_EqStatus');
		var eqConditionElement=$('bulk_EqCondition');
		//add field enum list to status field.
		for(var statusKey in statusEnumListItems){
			var option=new Option(statusEnumListItems[statusKey],statusKey);
			eqStatusElement.options.add(option);
		}
		//add field enum list to condition field
		for(var conditionKey in conditionEnumListItems){
			var option=new Option(conditionEnumListItems[conditionKey],conditionKey);
			eqConditionElement.options.add(option);
		}
		
		//set a <VARIES> option to Equipment status and Equipment condition field.
		eqStatusElement.options.add(new Option('<VARIES>','varies'));
		eqConditionElement.options.add(new Option('<VARIES>','varies'));
	}
});

/**
 * check date range.
 */
function checkDateRange() {
	var form = View.panels.get('wrcfForm');
	var dateStart = form.getFieldValue('wrcf.date_start');
	var timeStart = form.getFieldValue('wrcf.time_start');
	var dateEnd = form.getFieldValue('wrcf.date_end');
	var timeEnd = form.getFieldValue('wrcf.time_end');
	var wrcfDS = View.dataSources.get('wrcfDS');
	
	var isValid = true;
	
	if(dateStart && dateEnd){
		if(compareLocalizedDates(form.getFieldElement("wrcf.date_end").value, form.getFieldElement("wrcf.date_start").value)){
			isValid = false;
			View.showMessage(getMessage('errorDateRange'));
		}else if(!compareLocalizedDates(form.getFieldElement("wrcf.date_end").value, form.getFieldElement("wrcf.date_start").value) && !compareLocalizedDates(form.getFieldElement("wrcf.date_start").value, form.getFieldElement("wrcf.date_end").value)){
			if(timeStart && timeEnd && DateMath.before(wrcfDS.parseValue('wrcf.time_end',$("StoredwrcfForm_wrcf.time_end").value,false),wrcfDS.parseValue('wrcf.time_end',$("StoredwrcfForm_wrcf.time_start").value,false))){
				isValid = false;
				View.showMessage(getMessage('errorTimeRange'));
			}
		}
	}
	
	return isValid;
	
}

/**
 * Bulk update for all selected work requests
 */
function runBulkUpdate(form, tableName, ruleId) {
	var panel = View.panels.get(form);
	var fields = panel.getFieldValues();
	
	if (tableName == 'wrcf') {
		//check data start and date end 
		if(!checkDateRange()){
			return false;
		}
	}
	
	//KB3041184 - From Fred - We are going to omit using Other Resource Type for now, 
	//because it requires more sample data that we can probably safely remove from the Quick-Start process 
	if(form == 'wrotherForm'){
		//KB3042802 - Many user ask add Other Resource Type, so add other resource type again, comment out below code
		//fields['wr_other.other_rs_type'] = ' ';
	}else{
		if(!panel.canSave()){
			return false;
		}
	}
	
	var result = {};
	var bulkUpdateController = View.controllers.get('wrBulkUpdate');
	var wrRecords = bulkUpdateController.records;
	
	if (valueExistsNotEmpty(wrRecords)) {
		var actualHours = 0;
		var doubleHours = 0;
		var overTimeHours = 0;
		
		var isDivideTime = false;
		
		//for update craftsperson, check isDivideTime check box, if true, divide hours to every work request
		if (tableName == 'wrcf') {
			isDivideTime = $('divideTime').checked;
			if (isDivideTime) {
				actualHours = fields['wrcf.hours_straight'];
				doubleHours = fields['wrcf.hours_double'];
				overTimeHours = fields['wrcf.hours_over'];
				if (actualHours) {
					actualHours = actualHours / wrRecords.length;
				}
				if (doubleHours) {
					doubleHours = doubleHours / wrRecords.length;
				}
				if (overTimeHours) {
					overTimeHours = overTimeHours / wrRecords.length;
				}
			}
		}

		//Call wfr to update every record
		for ( var i = 0; i < wrRecords.length; i++) {
			var record = wrRecords[i];
			var wrId = record.getValue('wr.wr_id');
			var values = WRBU_Clone(fields);
			values[tableName + ".wr_id"] = wrId;
			
			//reset the craftsperson actual hour of isDivideTime is checked
			if (tableName == 'wrcf') {
				if (isDivideTime) {
					values['wrcf.hours_straight'] = actualHours;
					values['wrcf.hours_double'] = doubleHours;
					values['wrcf.hours_over'] = overTimeHours;
				}
				
				//KB3041457 - consider a way to change the bulk Update feature to update existing wrcf records	
				checkExistingWrcf(values);
			}
			
			try {
				result = Workflow.callMethod(ruleId, values);
			} catch (e) {
				panel.validationResult.valid = false;
				panel.displayValidationResult(e);
				return false;
			}
		}
	}
	
	View.getOpenerView().panels.get('wrList').refresh();
	View.getOpenerView().controllers.get('opsConsoleWrListActionController').keepReqeustsSelectedAfterRefresh();
	panel.closeWindow();
}

/**
 * Check if existing wrcf match to update - KB3041457
 */
function checkExistingWrcf(wrcfValues) {
	
	var restriction = new Ab.view.Restriction();
	restriction.addClause('wrcf.cf_id',wrcfValues['wrcf.cf_id'], '=');
	restriction.addClause('wrcf.wr_id',wrcfValues['wrcf.wr_id'], '=');
	var wrcfDS = View.dataSources.get('wrcfDS');
	var wrcfRecords = wrcfDS.getRecords(restriction);
	var existingRecord = null;
	if(wrcfRecords.length>0){
		for(var i=0;i<wrcfRecords.length;i++){
			var wrcfRecord = wrcfDS.processOutboundRecord(wrcfRecords[i]);
			var actualHours = formatNumber(wrcfRecord.getValue('wrcf.hours_straight'));
			var doubleHours = formatNumber(wrcfRecord.getValue('wrcf.hours_double'));
			var overTimeHours = formatNumber(wrcfRecord.getValue('wrcf.hours_over'));
			var dateStart = wrcfRecord.getValue('wrcf.date_start');
			var timeStart = wrcfRecord.getValue('wrcf.time_start');
			var dateEnd = wrcfRecord.getValue('wrcf.date_end');
			var timeEnd = wrcfRecord.getValue('wrcf.time_end');
			var workType = wrcfRecord.getValue('wrcf.work_type');
			
			if(((actualHours =='0.00' && wrcfValues['wrcf.hours_straight']!='0.00') || wrcfValues['wrcf.hours_straight']=='0.00')
					&& ((doubleHours =='0.00' && wrcfValues['wrcf.hours_double']!='0.00') || wrcfValues['wrcf.hours_double']=='0.00')
					&& ((overTimeHours =='0.00' && wrcfValues['wrcf.hours_over']!='0.00') || wrcfValues['wrcf.hours_over']=='0.00')
					&& ((dateStart =='' && valueExistsNotEmpty(wrcfValues['wrcf.date_start'])) || !valueExistsNotEmpty(wrcfValues['wrcf.date_start']))
					&& ((timeStart =='' && valueExistsNotEmpty(wrcfValues['wrcf.time_start'])) || !valueExistsNotEmpty(wrcfValues['wrcf.time_start']))
					&& ((dateEnd =='' && valueExistsNotEmpty(wrcfValues['wrcf.date_end'])) || !valueExistsNotEmpty(wrcfValues['wrcf.date_end']))
					&& ((timeEnd =='' && valueExistsNotEmpty(wrcfValues['wrcf.time_end'])) || !valueExistsNotEmpty(wrcfValues['wrcf.time_end']))
					&& ((workType =='UnSp' && wrcfValues['wrcf.work_type']!='UnSp') || wrcfValues['wrcf.work_type']=='UnSp')
			){
				existingRecord = wrcfRecord;
				break;
			}
			
		}
		
		if(existingRecord){
			wrcfValues['wrcf.date_assigned'] =  existingRecord.getValue('wrcf.date_assigned');
			wrcfValues['wrcf.time_assigned'] =  existingRecord.getValue('wrcf.time_assigned');
			wrcfValues['wrcf.hours_straight'] =  formatNumber(existingRecord.getValue('wrcf.hours_straight'))=='0.00'? wrcfValues['wrcf.hours_straight'] :existingRecord.getValue('wrcf.hours_straight') ;
			wrcfValues['wrcf.hours_double'] =  formatNumber(existingRecord.getValue('wrcf.hours_double'))=='0.00'? wrcfValues['wrcf.hours_double'] :existingRecord.getValue('wrcf.hours_double') ;
			wrcfValues['wrcf.hours_over'] =  formatNumber(existingRecord.getValue('wrcf.hours_over'))=='0.00'? wrcfValues['wrcf.hours_over'] :existingRecord.getValue('wrcf.hours_over') ;
			
			if(valueExistsNotEmpty(wrcfValues['wrcf.date_start'])){
				wrcfValues['wrcf.date_start'] =  existingRecord.getValue('wrcf.date_start')==''? wrcfValues['wrcf.date_start'] :existingRecord.getValue('wrcf.date_start') ;
			}
			if(valueExistsNotEmpty(wrcfValues['wrcf.time_start'])){
				wrcfValues['wrcf.time_start'] =  existingRecord.getValue('wrcf.time_start')==''? wrcfValues['wrcf.time_start'] :existingRecord.getValue('wrcf.time_start') ;
			}
			if(valueExistsNotEmpty(wrcfValues['wrcf.date_end'])){
				wrcfValues['wrcf.date_end'] =  existingRecord.getValue('wrcf.date_end')==''? wrcfValues['wrcf.date_end'] :existingRecord.getValue('wrcf.date_end') ;
			}
			if(valueExistsNotEmpty(wrcfValues['wrcf.time_end'])){
				wrcfValues['wrcf.time_end'] =  existingRecord.getValue('wrcf.time_end')==''? wrcfValues['wrcf.time_end'] :existingRecord.getValue('wrcf.time_end') ;
			}
			
			wrcfValues['wrcf.work_type'] =  existingRecord.getValue('wrcf.work_type')=='UnSp'? wrcfValues['wrcf.work_type'] :existingRecord.getValue('wrcf.work_type') ;
		}
	}
}

function WRBU_Clone(originalObject) {
	var newObject = {};
	
	for ( var key in originalObject )
    {
	    if ( typeof(originalObject[key]) == 'object')
        { 
            newObject[key] = WRBU_Clone(originalObject[key]);
        }
        else
        {
            newObject[key] = originalObject[key];
        }
    }
    return newObject;
}

function formatNumber(value) {
	if(value == '0'){
		return '0.00'
	}else{
		return value;
	}
}


/**
 * Over write core API to open Add new dialog and close select value dialog.
 */
Ab.grid.SelectValue.prototype.onAddNew = function() {
	var parameters = Ab.view.View.selectValueParameters;
	var title = parameters.title;
	View.closeDialog();
	View.openDialog(this.addNewDialog, null, false, {
		x : 100,
		y : 100,
		width : 850,
		height : 800,
		title : this.getLocalizedString(Ab.grid.SelectValue.z_TITLE_ADD_NEW) + ' ' + title,
		useAddNewSelectVDialog : false,
		closeButton : false
	});
}