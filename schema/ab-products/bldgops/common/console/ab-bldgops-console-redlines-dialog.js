/**
 * called by ab-bldgops-console-redlines-dialog.axvw
 */

var opsConsoleRedLineController = View.createController('opsConsoleRedLineController', {
	
	drawingControlEx: null,
	
	afterViewLoad: function(){
		
		var c=View.getOpenerView().controllers.items[0];
		if(valueExistsNotEmpty(c) && valueExistsNotEmpty(c.locArray)){
			var blId = c.locArray[0];
			var flId = c.locArray[1];
			var rmId = c.locArray[2];
			var activityLogId = parseInt(c.activityLogId);
			
			var parameters = new Ab.view.ConfigObject();
	    	parameters['planTypeGroup'] = 'Standard Space Highlights';
	    	parameters['redlineLegend'] = {panelId: 'redlineLegendPanel', divId: 'redlineLegendDiv', colorPickerId: 'redlineLegendPanel_head'};
	    	parameters['planTypeHighlight'] = {panelId: 'planTypeHighlightPanel', divId: 'planTypeHighlightDiv'};
	    	// load SVG from server and display in SVG panel's  <div id="drawingDiv">    	
	    	var drawingControlEx = new Ab.svg.MarkupDrawingControl("drawingDiv", "drawingPanel", parameters); 
	    	// define parameters to be used by server-side job
	    	var parameters = new Ab.view.ConfigObject();
	    	parameters['activityLogId'] = activityLogId;
	    	parameters['pkeyValues'] = {'bl_id':blId, 'fl_id':flId, 'rm_id': rmId};
	    	// load the floorplan
	    	drawingControlEx.filterHighlight = 'none',
	    	drawingControlEx.load("drawingDiv", parameters);
	    	this.drawingControlEx = drawingControlEx;
		}
    },
    
    drawingPanel_onSaveRedline: function(){
    	this.drawingControlEx.filterHighlight = null;
    	
    	//hide close button and x button of the pop up dialog to avoid saving not completed and close the dialog manually 
    	jQuery(window.parent.document).find('#closeButton').hide();
    	var closeXbuttons = jQuery(window.parent.document).find('.x-tool-close');
        jQuery(closeXbuttons[closeXbuttons.length - 1]).hide();
        
    	this.drawingControlEx.save();
    	
    	afterSaveRedline = function(){
    		var c=View.getOpenerView().controllers.items[0];
        	Workflow.callMethod('AbBldgOpsOnDemandWork-WorkRequestService-copyRedlineDocFromServiceReqeustToWorkRequest', parseInt(c.activityLogId), parseInt(c.workRequestId));
    		c.refreshDocsPanel();
    	}
    	
    	afterSaveRedline.defer(3000);
    }
});







