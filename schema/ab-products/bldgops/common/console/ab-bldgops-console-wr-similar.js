function onSubmit() {
    var openView = View.getOpenerView();
    openView.closeDialog();
    var controller = openView.controllers.get("wrCreateController");
    if (controller.isSaveAction) {
        controller.saveRequest();
    } else {
        controller.submitRequest();
    }
}

var similarWrController = View.createController("similarWrController", {

    afterInitialDataFetch: function() {
        var openView = View.getOpenerView();
        var controller = openView.controllers.get("wrCreateController");
        if (controller.isSaveAction) {
            this.actionPanel.actions.get('saveOrSubmit').setTitle(getMessage('saveButtonText'));
        }
    }

});