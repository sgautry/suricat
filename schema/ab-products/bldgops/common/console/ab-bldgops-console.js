/**
 * Controller for the Operation Console.
 */
var opsExpressConsoleCtrl =  View.createController('opsExpressConsoleCtrl', {

	/**
	 * Role name, possible values: client|approver|step completer|supervisor|craftsperson.
	 */
	roleName : null,

	/**
	 * Initialize the controller state.
	 */
	afterViewLoad : function() {
		this.roleName = getConsoleRoleName();
	},
	
	afterInitialDataFetch: function(){
	    //APP-1387:Check database schema for Bali6.
        if(!Workflow.callMethod('AbBldgOpsOnDemandWork-WorkRequestService-checkSchemaExisting','helpdesk_sla_priority', 'priority_number').value){
            jQuery('body').hide();
            alert(getMessage('udpateSchemaMessage'));
            return;
        }
	}
	
});