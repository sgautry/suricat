View.createController('addNewDocuments', {

    afterInitialDataFetch: function() {
        var closeXbuttons = jQuery(window.parent.document).find('.x-tool-close');
        jQuery(closeXbuttons[closeXbuttons.length - 1]).hide();
    },

    addDocsForm_onCloseDocPanel: function() {
        var form = this.addDocsForm;
        if (form.getFieldValue('activity_log.doc1') || form.getFieldValue('activity_log.doc2') || form.getFieldValue('activity_log.doc3') || form.getFieldValue('activity_log.doc4')) {
            View.getOpenerView().controllers.get('wrCreateController').showAttachedDocInfor(true);
        }else{
            View.getOpenerView().controllers.get('wrCreateController').showAttachedDocInfor(false);
        }
        View.closeThisDialog();
    }

});
