/**
 * Controller of showing work request floor plan
 */
View.createController('ShowFloorPlanController', {
	
	svgControl: null,

	afterViewLoad : function() {
		var openerView = View.getOpenerView();
		var dwgRestriction = View.getOpenerView().dialogRestriction;
		var blId = dwgRestriction.findClause('rm.bl_id').value;
		var flId = dwgRestriction.findClause('rm.fl_id').value;
		var rmId = dwgRestriction.findClause('rm.rm_id').value;
		
		this.loadSvg(blId, flId, [
                                 {'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickRoom}]);
		// you can choose how to highlight the asset  	  
    	var opts = { cssClass: 'zoomed-asset-red-bordered',		// use the cssClass property to specify a css class
    			     zoomFactor: 0,
    				 removeStyle: true							// use the removeStyle property to specify whether or not the fill should be removed (for example  cssClass: 'zoomed-asset-bordered'  and removeStyle: false
			   	   };
    	
		this.svgControl.getAddOn('AssetLocator').findAssets([blId + ';' + flId + ';' + rmId], opts);
		this.svgControl.getAddOn('AssetLocator').infoBar.show(false);
	},
	
	 loadSvg: function(blId, flId) {
	    	
    	// define parameters to be used by server-side job
    	var parameters = new Ab.view.ConfigObject();
    	parameters['pkeyValues'] = {'bl_id':blId, 'fl_id':flId};
		parameters['bordersHighlightSelector'] = false; 
		parameters['highlightFilterSelector'] = false;
		parameters['divId'] = "svgDiv";
		parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
									   'AssetLocator': {divId: "svgDiv"}
									 };
		parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickRoom}];
		parameters['drawingName'] = this.retrieveDrawingName(blId, flId);
									 
		this.svgControl = new Drawing.DrawingControl("svgDiv", "svg_ctrls", parameters);	
		
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);	
   	
    	// resize specified DOM element whenever the panel size changes
    	this.svg_ctrls.setContentPanel(Ext.get('svgDiv'));
    },
    
    onClickRoom: function(assetIds, drawingController){
    	// call callback method
		if (View.parameters.callback) {
			View.parameters.callback(assetIds.assetId.split(';')[2]);
			// The .defer method used here is required for proper functionality with Firefox 2
			View.closeThisDialog.defer(100, View);
		}
    },
    
    retrieveDrawingName: function(blId, flId){
    	var drwingDS = Ab.data.createDataSourceForFields({
	        id: 'svgDrawingControl_afmDwgs_Ds',
	        tableNames: ['afm_dwgs'],
	        fieldNames: ['afm_dwgs.dwg_name','afm_dwgs.space_hier_field_values']
	     });
    	 
    	 var dwgName = '';
     	
    	 if(blId && flId){
    		var restriction = new Ab.view.Restriction();
    		restriction.addClause("afm_dwgs.space_hier_field_values", blId + ";" + flId);
        	var records = drwingDS.getRecords(restriction);
        	if(records!=null && records.length > 0 && records[0]!=null){
        		dwgName = records[0].getValue("afm_dwgs.dwg_name");
        	}
	    }
         
        return dwgName;
    }
});
