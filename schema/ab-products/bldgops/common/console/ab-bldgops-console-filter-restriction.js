/**
 * Controller for the Operation Console filter restriction
 */
var opsConsoleFilterRestrictionController = View.createController('opsConsoleFilterRestrictionController', {

	/**
	 * Controller for the category grid (ie.opsConsoleWrListController)
	 */
	wrListController : null,
	
	/**
     * check schema exists work_team.cf_assign field
     */
    isSchemaExistsCfAssignField : false,
	
	/**
	 * Check schema and set restriction base on schema.
	 */
	checkSchemaAndSetRestriction : function() {
		if(Workflow.callMethod('AbBldgOpsOnDemandWork-WorkRequestService-checkSchemaExisting','work_team', 'cf_assign').value){
		    this.isSchemaExistsCfAssignField = true;
		}
		
		if(Workflow.callMethod('AbBldgOpsOnDemandWork-WorkRequestService-checkSchemaExisting','cf_work_team', 'cf_id').value){
		    this.wrList.addParameter('schemaExistsForCfWorkTeamTable', true);
		    this.wrList.addParameter('schemaNotExistsForCfWorkTeamTable', false);
		}
		
		if(Workflow.callMethod('AbBldgOpsOnDemandWork-WorkRequestService-checkSchemaExisting','wr', 'time_on_hold').value){
		    this.wrList.addParameter('time_on_hold', 'wr.time_on_hold');
        }
		
	},
	
	/**
	 * After view loads
	 */
	afterViewLoad : function() {
		this.wrListController = View.controllers.get('opsConsoleWrListController');
		this.setWorkflowSubstitutes();
	},
	
	/**
	 * Set workflow substitutes
	 */
	setWorkflowSubstitutes : function() {
		this.wrList.addParameter('emWorkflowSubstitutes', Workflow.callMethod('AbBldgOpsHelpDesk-RequestsService-getWorkflowSubstitutes','em_id').message);
		this.wrList.addParameter('cfWorkflowSubstitutes', Workflow.callMethod('AbBldgOpsHelpDesk-RequestsService-getWorkflowSubstitutes','cf_id').message);
	},
	
	/**
     * Set restrictions.
     */
    setRestrictions : function() {
        this.setRoleRestrictions();
        this.setFilterRestrictions();
        this.setUrlRestriction();
    },
	
	/**
	 * set permanent restriction by role
	 */
    setRoleRestrictions : function() {
        
        this.wrList.addParameter('useRoleRestriction', true);
        
        // role name
        var roleName = View.controllers.get('opsExpressConsoleCtrl').roleName;
		// get what to show
		var whatToShow = this.getWhatToShow(roleName);
		
		if (whatToShow == 'myApproved') {
			this.wrList.addParameter('useRoleRestriction', false);
			return;
		}

		if (roleName == 'supervisor') {
		    this.wrList.addParameter('isSupervisor', true);
		    this.wrList.addParameter('isCraftsperson', true);
		} else if (roleName == 'craftsperson') {
		    this.wrList.addParameter('isCraftsperson', true);
		    if(this.isSchemaExistsCfAssignField){
		        this.wrList.addParameter('useSelfAssign', '1');
		    }
        }
		
		//KB3047261 - load pending steps based on application parameter
		var BldgOpsConsoleLoadPendingSteps = View.activityParameters['AbBldgOpsOnDemandWork-BldgOpsConsoleLoadPendingSteps'];
		if(typeof BldgOpsConsoleLoadPendingSteps == 'undefined' || (valueExists(BldgOpsConsoleLoadPendingSteps) && BldgOpsConsoleLoadPendingSteps == '1')){
		    this.wrList.addParameter('loadPendingSteps', true);
		    this.wrList.addParameter('usePendingStepRestriction', false);
			this.wrList.addParameter('leftJoinPendingSteps', "LEFT OUTER JOIN wr_step_waiting ON ");
			if(Workflow.callMethod('AbBldgOpsOnDemandWork-WorkRequestService-checkSchemaExisting','helpdesk_step_log', 'rejected_step').value){
				this.wrList.addParameter('useRejectedStep', true);
			}else{
			    this.wrList.addParameter('useRejectedStep', false);
			}
		
		}else{
			if (whatToShow == 'pendingSteps') {
			    this.wrList.addParameter('loadPendingSteps', true);
			    this.wrList.addParameter('usePendingStepRestriction', true);
				this.wrList.addParameter('leftJoinPendingSteps', "LEFT OUTER JOIN wr_step_waiting ON ");
				if(Workflow.callMethod('AbBldgOpsOnDemandWork-WorkRequestService-checkSchemaExisting','helpdesk_step_log', 'rejected_step').value){
				    this.wrList.addParameter('useRejectedStep', true);
				}else{
	                this.wrList.addParameter('useRejectedStep', false);
	            }
				
			}else{
			    this.wrList.addParameter('loadPendingSteps', false);
			    this.wrList.addParameter('usePendingStepRestriction', false);
			    this.wrList.addParameter('useRejectedStep', false);
				//remove the pending steps in the query
				this.wrList.addParameter('leftJoinPendingSteps', "");
			}
		}
	},
	
	/**
	 * Set parameters based on filter selections.
	 */
	setFilterRestrictions : function() {
		// set filter restrictions
		this.setProbTypeRes();
        this.setWorkTypeRes();
        this.setStatusRes();
        this.setPartStatusRes();
        this.setCostRes();
        this.setDateRes('wr.date_requested');
        this.setDateRes('wr.date_assigned');
        this.setEscalatedRes();
        this.setReturnedRes();
        this.setDescriptionRes();
        
        this.addRestrictionFromFieldArray("bigBadFilter", 
                [ 'wr.priority_label', 'wrcf.cf_id', 'wrpt.part_id', 'eq.eq_std', 'wr.work_team_id','wr.dv_id', 'wr.dp_id', 'wr.eq_id', 'wr.wr_id', 'wr.wo_id', 'wr.requestor', 'wr.rm_id' , 'wrtr.tr_id','wr.pmp_id','wr.pms_id'  ]);
        this.addRestrictionFromFieldArray("wrFilter", [ 'wr.site_id','wr.bl_id', 'wr.fl_id']);
		
        // role name
        var roleName = View.controllers.get('opsExpressConsoleCtrl').roleName;
		// get what to show
		var whatToShow = this.getWhatToShow(roleName);

		// apply appropriate record limit for "top 10", "last 10", "all"
		this.wrList.recordLimit = (whatToShow == 'newest' || whatToShow == 'oldest' || whatToShow == 'near') ? 10 : 0;

		// set appropriate parameters based on whatToShow selection
		this.setParameterBaseOnWhatToShow(whatToShow);
		// get what to group
		var whatToGroup = this.getWhatToGroup(roleName);

		// set group by configuration
		this.setGroupByConfiguration(whatToGroup, whatToShow);

		// determine what columns to show, based on what to group
		this.determineColumnsToShow(whatToGroup);
	},

	/**
	 * Set parameters based on url parameter.
	 */
	setUrlRestriction : function() {
	    this.wrList.addParameter('urlWrId', '');
	    this.wrList.addParameter('urlActivityLogId', '');
	    this.wrList.addParameter('urlStepCode', '');
		if(valueExists(window.location.parameters)){
			var code = window.location.parameters["code"];
			if (valueExists(code)) {
			    this.wrList.addParameter('urlStepCode', code);
			}

			var activityLogId = window.location.parameters["activity_log_id"];
			if (valueExists(activityLogId)) {
			    this.wrList.addParameter('urlActivityLogId', activityLogId);
			}

			var wrId = window.location.parameters["wr_id"];
			if (valueExists(wrId)) {
			    this.wrList.addParameter('urlWrId', wrId);
			}
		}
	},
	
	/**
	 * get problem type restriction in easy filter
	 */
	setProbTypeRes : function() {
		var probTypeRes = '';
		// work type
		var probTypes = this.wrFilter.getFieldMultipleValues('wr.prob_type');
		for ( var i = 0; i < probTypes.length; i++) {
			var probType = probTypes[i];
			if (probType != '') {
				probTypeRes += " OR wr.prob_type like '" + probType + "%'";
			}
		}

		if (probTypeRes) {

			probTypeRes =  probTypeRes.substring(3);

		}else{
		    probTypeRes = ' 1=1 '
		}

		this.wrList.addParameter('probTypeRes', probTypeRes);
	},

	/**
	 * get work type restriction in big filter
	 */
	setWorkTypeRes : function() {
	    this.wrList.addParameter('isOD', '1');
	    this.wrList.addParameter('isPM', '1');
		// work type
		var workType = this.getSelectBoxValue('bigBadFilter_worktype');
		if (workType == 'OD') {
		    this.wrList.addParameter('isPM', '0');
		} else if (workType == 'PM') {
		    this.wrList.addParameter('isOD', '0');
		}
	},

	/**
	 * get status restriction in big filter
	 */
	setStatusRes : function() {
	    var allStatus = '';
		// status
		var statusValues = this.bigBadFilter.getCheckboxValues('wr.status');
		var statuses = [];
		for ( var i = 0; i < statusValues.length; i++) {
			var status = statusValues[i];
			if (status == 'H') {
				statuses.push('HA');
				statuses.push('HL');
				statuses.push('HP');
			} else {
				statuses.push(status);
			}
		}
		if (statuses.length > 0) {
			var statusInClause = "'" + statuses[0] + "'";
			for (i = 1; i < statuses.length; i++) {
				statusInClause += " ,'" + statuses[i] + "'";
			}
			
			allStatus = statusInClause;
		}

		this.wrList.addParameter('wr.status', allStatus);
	},
	
	/**
	 * get part status restriction in big filter
	 */
	setPartStatusRes : function() {
		var allStatus = '';
		// status
		var statusValues = this.bigBadFilter.getCheckboxValues('wrpt.status');
		var statuses = [];
		for ( var i = 0; i < statusValues.length; i++) {
			var status = statusValues[i];
			statuses.push(status);
		}
		if (statuses.length > 0) {
			var statusInClause = "'" + statuses[0] + "'";
			for (i = 1; i < statuses.length; i++) {
				statusInClause += " ,'" + statuses[i] + "'";
			}
			allStatus = statusInClause;
		}

		this.wrList.addParameter('wrpt.status', allStatus);
	},

	/**
	 * get cost restriction in big filter
	 */
	setCostRes : function() {
		// cost estimated
		var operator = this.getSelectBoxValue('bigBadFilter_operator');
		var cost = Ext.get('bigBadFilter_wr.cost_est_total').dom.value;
		if (operator != '') {
		    this.wrList.addParameter('operator', operator);
			if (cost != '') {
			    this.wrList.addParameter('wr.cost_est_total', cost);;
			}
		}else{
		    this.wrList.addParameter('operator', '');
		}
	},

	/**
	 * get date field restriction in big filter
	 */
	setDateRes : function(dateFieldName) {
		var dateFrom = this.bigBadFilter.getFieldValue(dateFieldName + ".from");
		var dateTo = this.bigBadFilter.getFieldValue(dateFieldName + ".to");
		if (valueExistsNotEmpty(dateFrom) && valueExistsNotEmpty(dateTo)) {
			var objDateFrom = this.bigBadFilter.getDataSource().parseValue(dateFieldName + ".from", dateFrom, false);
			var objDateTo = this.bigBadFilter.getDataSource().parseValue(dateFieldName + ".to", dateTo, false);
		}

		// add the date comparison clauses
		if (valueExistsNotEmpty(dateFrom)) {
		    this.wrList.addParameter(dateFieldName + ".from", "${sql.date('" + dateFrom + "')}");
		}else{
		    this.wrList.addParameter(dateFieldName + ".from", '');
		}
		if (valueExistsNotEmpty(dateTo)) {
		    this.wrList.addParameter(dateFieldName + ".to", "${sql.date('" + dateTo + "')}");
		}else{
		    this.wrList.addParameter(dateFieldName + ".to", '');
		}
	},

	/**
	 * get escalated restriction in big filter
	 */
	setEscalatedRes : function() {
		// escalated
		var escalated = Ext.get('bigBadFilter_wr.escalated').dom.checked;
		this.wrList.addParameter("escalated", escalated);
	},
	
	/**
	 * get Returned restriction in big filter
	 */
	setReturnedRes : function() {
		// returned
		var returned = Ext.get('bigBadFilter_wr.returned').dom.checked;
		this.wrList.addParameter("returned", returned);
	},
	
	/**
	 * get description restriction in big filter
	 */
	setDescriptionRes : function() {
		// description
		var description = this.bigBadFilter.getFieldValue('wr.description');
		if(valueExistsNotEmpty(description)){
			
			if (description.length>1 && description.substring(0,1)=="'" && description.substring(description.length-1,description.length)=="'") {
			    this.wrList.addParameter("wr.description", "='" + description.substring(1,description.length-1).replace(/\'/g, "''") + "'");
			}else{
				this.wrList.addParameter("wr.description", "like '%" + description.replace(/\'/g, "''") + "%'");
			}
		}else{
		    this.wrList.addParameter("wr.description", '');
		}

	},
	
	/**
	 * add restriction from field array
	 */
	addRestrictionFromFieldArray : function(panelId, fieldArray) {
		for ( var i = 0; i < fieldArray.length; i++) {
			var fieldName = fieldArray[i];
			var queryParameter = View.panels.get(panelId).getFieldQueryParameter(fieldName);
			if (queryParameter != " IS NOT NULL") {
			    if(fieldName == 'wr.priority_label'){
			        this.wrList.addParameter(fieldName, " like '%" + View.panels.get(panelId).getFieldValue(fieldName) + "%'");
			    }else{
			        this.wrList.addParameter(fieldName, queryParameter);
			    }
			    
			}else{
			    this.wrList.addParameter(fieldName, '');
			}
		}
	},

	/**
	 * get what to show base on role name
	 */
	getWhatToShow : function(roleName) {
		var whatToShowBoxId = 'wrFilter_wr_whatToShow';

		if (roleName == 'client' || roleName == 'approver' || roleName == 'step completer') {
			whatToShowBoxId = 'wrFilter_activity_log_whatToShow';
		}

		var whatToShow = this.getSelectBoxValue(whatToShowBoxId);
		this.wrFilter.getSidecar().set('whatToShow', whatToShow);
		this.wrFilter.getSidecar().save();

		return whatToShow;
	},

	/**
	 * get what to group base on role name
	 */
	getWhatToGroup : function(roleName) {
		var whatToGroupBoxId = 'wrFilter_wr_whatToGroup';

		if (roleName == 'client' || roleName == 'approver' || roleName == 'step completer') {
			whatToGroupBoxId = 'wrFilter_activity_log_whatToGroup';
		}

		var whatToGroup = this.getSelectBoxValue(whatToGroupBoxId);

		return whatToGroup;
	},

	/**
	 * Set parameter base on what to show selection
	 */
	setParameterBaseOnWhatToShow : function(whatToShow) {
		this.wrList.addParameter('sortBy', "ORDER BY wr.wr_id DESC");
		this.wrList.addParameter('top10', "TOP 100000");
		this.wrList.addParameter("showMyRequest", false);
		this.wrList.addParameter("showUnAssignedRequest", false);
		this.wrList.addParameter("showNearRequest", false);
		this.wrList.addParameter("showRequiringMyApprovalRequest", false);
		this.wrList.addParameter("showMyApprovedRequest", false);
		
		if (whatToShow == 'all') {
			this.wrList.addParameter('oracleTop10', "");
		} else if (whatToShow == 'esc') {
			this.wrList.addParameter('oracleTop10', "");
			this.wrList.addParameter("escalated", true);
		} else if (whatToShow == 'my') {
			this.wrList.addParameter('oracleTop10', "");
			this.wrList.addParameter("showMyRequest", true);
		} else if (whatToShow == 'unassigned') {
			this.wrList.addParameter('oracleTop10', "");
			this.wrList.addParameter("showUnAssignedRequest", true);
		} else if (whatToShow == 'newest') {
			this.wrList.addParameter('top10', "TOP 10");
			this.wrList.addParameter('oracleTop10', " WHERE (rownum >=1 and rownum<=10 )");
			this.wrList.addParameter('sortBy', "ORDER BY wr.date_requested DESC, wr.time_requested DESC, wr.wr_id DESC");
		} else if (whatToShow == 'oldest') {
			this.wrList.addParameter('top10', "TOP 10");
			this.wrList.addParameter('oracleTop10', " WHERE (rownum >=1 and rownum<=10 )");
			this.wrList.addParameter('sortBy', "ORDER BY wr.date_requested ASC, wr.time_requested ASC, wr.wr_id ASC");
		} else if (whatToShow == 'near') {
			this.wrList.addParameter('top10', "TOP 10");
			this.wrList.addParameter('oracleTop10', " WHERE (rownum >=1 and rownum<=10 )");
			this.wrList.addParameter('sortBy', "ORDER BY wr.date_escalation_completion ASC, wr.time_escalation_completion ASC, wr.wr_id ASC");
			this.wrList.addParameter("showNearRequest", true);
		} else if (whatToShow == 'requiringMyApproval') {
			this.wrList.addParameter('oracleTop10', "");
			this.wrList.addParameter("showRequiringMyApprovalRequest", true);
		} else if (whatToShow == 'myApproved') {
			this.wrList.addParameter('oracleTop10', "");
			this.wrList.addParameter("showMyApprovedRequest", true);
		}
	},

	/**
	 * Set group by configuration based on group by datasource.
	 * 
	 * @param groupByDS
	 *            id of datasource for which to group by
	 */
	setGroupByConfiguration : function(groupByDS, whatToShow) {
		var showWithoutGroupings = (groupByDS == 'none') ? true : false;
		if (showWithoutGroupings == true) {

			// show without groupings (ie. as a regular grid)
			this.wrListController.groupBy = {
				fieldName : '',
				order : [],
				categoryDataSourceId : '',
				setCategoryDataSouceById : false,
				showWithoutGroupings : true
			};
			
			this.wrList.categoryField = '';

		} else {

			// show as category grid
			var categoryDataSource = View.dataSources.get(groupByDS);
			var fieldName = categoryDataSource.fieldDefs.items[0].fullName;
			var order = [];

			if (groupByDS == 'statusDS') {
				order = this.wrListController.statusOrderArray;
			}

			this.wrListController.groupBy = {
				fieldName : fieldName,
				order : order,
				countField: 'wr.wr_count',
				categoryDataSourceId : groupByDS,
				setCategoryDataSouceById : true,
				showWithoutGroupings : false,
				getStyleForCategory : this.wrListController.getStyleForCategory
			};
		}

		this.addGroupByStatusClause();
		if (groupByDS == 'cfIdDS') {
            if (whatToShow == 'myApproved') {
                this.wrList.addParameter('mainTable', 'wrhwr wr LEFT OUTER JOIN wrcf ON wrcf.wr_id=wr.wr_id');
            } else {
                this.wrList.addParameter('mainTable', 'wr LEFT OUTER JOIN wrcf ON wrcf.wr_id=wr.wr_id');
            }

            this.wrList.addParameter('assignToFromWrcf', true);
            this.wrList.addParameter('assignToFromWr', false);
        } else {
            if (whatToShow == 'myApproved') {
                this.wrList.addParameter('mainTable', 'wrhwr wr');
            } else {
                this.wrList.addParameter('mainTable', 'wr');
            }
            
            this.wrList.addParameter('assignToFromWrcf', false);
            this.wrList.addParameter('assignToFromWr', true);
        }
        
        var ds = View.dataSources.get(groupByDS);
        if(ds){
            ds.parameters = this.wrList.parameters;
        }
        
		this.wrList.setCategoryConfiguration(this.wrListController.groupBy);
	},

	/**
	 * Grouping by status returns only certains statuses. Keep this consistant while changing the group by
	 */
	addGroupByStatusClause : function() {
		var groupByStatusList = this.getArrayValuesAsInClause(this.wrListController.statusOrderArray);
		this.wrList.addParameter('groupByStatusList', groupByStatusList);
	},

	/**
	 * Get status order as an "IN" clause
	 * 
	 * @return statusClause
	 */
	getArrayValuesAsInClause : function(arr) {
		var clause = "";
		for ( var i = 0; i < arr.length; i++) {
			if (i > 0) {
				clause += ", ";
			}
			clause += "'" + arr[i] + "'";
		}
		return clause;
	},

	/**
	 * Determine which columns to show/hide based on group by selection
	 * 
	 * @param groupByDS
	 *            id of datasource for which to group by
	 */
	determineColumnsToShow : function(groupBy) {
		//this.showDefaultColumns();
		switch (groupBy) {
		case 'statusDS':
			this.wrList.showColumn('wr.status', false);
			break;

		case 'blIdDS':
			this.wrList.showColumn('wr.location', false);
			break;

		case 'probTypeDS':
			this.wrList.showColumn('wr.prob_type', false);
			break;

		default:

		}

		this.wrList.updateHeader();
	},

	/**
	 * Show prob_type, location, status columns as default
	 */
	showDefaultColumns : function() {
		this.wrList.showColumn('wr.prob_type', true);
		this.wrList.showColumn('wr.location', true);
		this.wrList.showColumn('wr.status', true);
	},

	/**
	 * Helper for getting the value of a drop down box
	 * 
	 * @param id
	 *            Id of the drop down
	 */
	getSelectBoxValue : function(id) {
		var el = Ext.get(id).dom;
		return el.options[el.selectedIndex].value;
	}

});

String.prototype.replaceAll = function(search, replacement){
	var i = this.indexOf(search);
	var object = this;
	
	while (i > -1){
		object = object.replace(search, replacement); 
		i = object.indexOf(search);
	}
	return object;
}

Ab.grid.Category.prototype.setCategoryProperties =  function(parameters){
	if (valueExistsNotEmpty(this.categoryField)) {
	
		parameters.categoryDataSourceId = this.getCategoryDataSourceId();
		parameters.categoryFields = [this.getCategoryFieldDef(this.categoryField)];
		//XXX: 
		for(var i=0;i<this.getVisibleFieldDefs().length; i++){
			parameters.categoryFields.push({});
		}
		if(this.isCustomCategoryOrder){
			parameters.categoryOrder = this.categoryOrder;
		}
		parameters.categoryColors = this.categoryColors;
	}
}


/**
 * Action Listener after select location fields.
 */
function afterSelectPriorityOptions(fieldName, selectedValue, previousValue) {
    
    if(fieldName!= 'priority_level_hiddenField'){
        setTimeout(function (){
            var form = View.panels.get('bigBadFilter');
            form.setFieldValue(fieldName, form.getFieldValue('priority_level_hiddenField') + ' - ' + selectedValue);
        }, 100);
    }
}