var opsMaintenanceHistoryController=View.createController('opsMaintenanceHistoryController',{
	/**
	 * Get equipment maintenance history list.
	 */
	getMaintenanceHistoryInfo: function(eqId){
		if(valueExistsNotEmpty(eqId)){
			var restriction=new Ab.view.Restriction();
			restriction.addClause('wrhwr.eq_id',eqId,'=');
			this.wrEqMaintHistGrid.refresh(restriction);
		}
	},
	
	/**
	 * Open work request details pop-up panel.
	 */
	openWrDetailsPanel: function(row){
		var selectedRowIndex=this.wrEqMaintHistGrid.selectedRowIndex;
		var wrId=this.wrEqMaintHistGrid.gridRows.get(selectedRowIndex).getRecord().getValue('wrhwr.wr_id');
		if(valueExistsNotEmpty(wrId)){
		    var isArchived=this.isWorkRequestArchived(wrId);
		    var targetView=null;
		    if(isArchived){
		        var restriction = new Ab.view.Restriction();
                    restriction.addClause('hwr.wr_id', wrId);
                targetView="ab-bldgops-console-wr-details-archived.axvw";
		    }else{
		        var restriction = new Ab.view.Restriction();
                    restriction.addClause('wr.wr_id', wrId);
                targetView="ab-bldgops-console-wr-details.axvw";
		    }
		    
			//ab-bldgops-console-wr-details-related.axvw
			View.getOpenerView().openDialog(targetView, restriction, false, {
				width : 1000,
				height : 600,
				closeButton : false,
				isDialog : true,
				collapsible : false,
				title : getMessage('workRequestDetailsTitle')
			});
		}
	},
	
	/**
	 * Check whether work request is archived or not.
	 */
	isWorkRequestArchived: function(wrId){
	    var isArchived=true;
	    var restriction = new Ab.view.Restriction();
            restriction.addClause('wr.wr_id', wrId,'=');

        var parameters = {
                tableName: 'wr',
                fieldNames: toJSON(['wr.wr_id']),
                restriction: toJSON(restriction)
        };

        try{
            var result = Workflow.call('AbCommonResources-getDataRecords', parameters);
            if(result.code == "executed" && result.data.records.length > 0){
                isArchived=false;
            }
        }catch (e){
            Workflow.handleError(e);
        }
        
        return isArchived;
	}
});