var abBldgopsReportWrLocController = View.createController('abBldgopsReportWrLocController', {
    otherRes:'1=1',
	fieldsArraysForRestriction: new Array(['wr.site_id'],['wr.bl_id'],['wr.dv_id'],['wr.dp_id'], ['wr.prob_type', 'like'], ['eq.eq_std'], ['wr.eq_id'], ['wr.supervisor'],['wr.work_team_id']),
    afterViewLoad: function(){
        this.svg_ctrls.setTitle(getMessage('drawingPanelTitle1'));
    },
    
    svgControl: null,
	
	loadSvg: function(bl_id, fl_id, drawingName) {
    	
    	// define parameters to be used by server-side job
    	var parameters = new Ab.view.ConfigObject();
    	parameters['highlightParameters'] = [{'view_file':"ab-bldgops-report-wr-loc.axvw", 'hs_ds': "abBldgopsReportWrLocdrawingHighlightDS", 'label_ds':'abBldgopsReportWrLocdrawingLabelDS1'}];
    	parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id};
		parameters['addOnsConfig'] = { 'DatasourceSelector': {panelId: "svg_ctrls"}};
		parameters['divId'] = "svgDiv";
		parameters['drawingName'] = drawingName;
		
		this.svgControl = new Drawing.DrawingControl("svgDiv", "svg_ctrls", parameters);	
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
    	this.svgControl.currentDrawingName = drawingName;
    	
    },
    
    //export docx for drawing 
    svg_ctrls_onExportDOCX:function(){
    	var jobId = Workflow.startJob('AbSystemAdministration-generatePaginatedReport-buildDocxFromChart', 'ab-bldgops-report-wr-loc.axvw', 
    			AmCharts.export({},{}).toByteArray({data: this.svgControl.getImageBytes(this.svgControl.currentDrawingName)}),View.title, {version: Ab.view.View.version, outputType:'docx'});
   	    if(jobId != null){
   	    	var command = new Ab.command.exportPanel({});
   	    	command.displayReport(jobId);
		}
	},

	//Show tree by console
	abBldgopsReportWrLocConsole_onClear:function(){
		setDefaultValueForHtmlField(['worktype'],['both']);
		this.abBldgopsReportWrLocConsole.clear();
	 },
	 
    abBldgopsReportWrLocConsole_onShowTree: function() {
		this.abBldgopsReportWrLocGrid.show(false);
        var console = this.abBldgopsReportWrLocConsole;
        this.otherRes = getRestrictionStrFromConsole(console, this.fieldsArraysForRestriction);
        var selectedEL;
        selectedEL = document.getElementById("worktype");
        var workType = selectedEL.options[selectedEL.selectedIndex].value;
        if (workType == 'ondemand') {
            this.otherRes =this.otherRes+ " AND wr.prob_type!='PREVENTIVE MAINT' ";
        }
        else 
            if (workType == 'pm') {
                this.otherRes =this.otherRes+ " AND wr.prob_type='PREVENTIVE MAINT' ";
            }
        this.abBldgopsReportWrLocBlTree.addParameter('consolePram', this.otherRes);
        this.abBldgopsReportWrLocBlTree.refresh();
        d3.select("#svgDiv").node().innerHTML = '<div class="empty-drawing-txt">' + '' + '</div>';  
        this.svg_ctrls.setTitle(getMessage('drawingPanelTitle1'));
    }
});

function generateReport(){
	abBldgopsReportWrLocController.abBldgopsReportWrLocConsole_onShowTree;
	var wrRes = '';

    if (abBldgopsReportWrLocController.otherRes) {
		wrRes ='and '+abBldgopsReportWrLocController.otherRes;
    }else{
		wrRes = ' and 1 = 1 ';
	}
    View.openPaginatedReportDialog("ab-bldgops-report-wr-loc-print.axvw" ,null, {
                'consolePram': wrRes
            });
}

/**
 * event handler when click the floor level of the tree
 */
function onFlTreeClick(ob){
	View.panels.get('abBldgopsReportWrLocGrid').show(true);
	var controller=View.controllers.get("abBldgopsReportWrLocController");
    var currentNode = View.panels.get('abBldgopsReportWrLocBlTree').lastNodeClicked;
    var blId = currentNode.parent.data['bl.bl_id'];
    var flId = currentNode.data['fl.fl_id'];
    View.dataSources.get('abBldgopsReportWrLocdrawingHighlightDS').addParameter('consolePram', controller.otherRes);
	
	View.dataSources.get('abBldgopsReportWrLocdrawingLabelDS1').addParameter('consolePram', controller.otherRes);
	View.dataSources.get('abBldgopsReportWrLocdrawingLabelDS2').addParameter('consolePram', controller.otherRes);
	View.dataSources.get('abBldgopsReportWrLocdrawingLabelDS3').addParameter('consolePram', controller.otherRes);
	View.dataSources.get('abBldgopsReportWrLocdrawingLabelDS4').addParameter('consolePram', controller.otherRes);
	View.dataSources.get('abBldgopsReportWrLocdrawingLabelDS5').addParameter('consolePram', controller.otherRes);
    
    var title = String.format(getMessage('drawingPanelTitle2'), blId + "-" + flId);
    displayFloor(currentNode, title);
	var res = new Ab.view.Restriction();
	res.addClause("wr.bl_id",blId, "=");
    res.addClause("wr.fl_id", flId, "=");
	View.panels.get('abBldgopsReportWrLocGrid').addParameter('consolePram', controller.otherRes);
	View.panels.get('abBldgopsReportWrLocGrid').refresh(res);
}
/**
 * display floor drawing for highlight report
 * @param {Object} drawingPanel
 * @param {Object} res
 * @param {String} title
 */
function displayFloor(currentNode, title){
    var blId = getValueFromTreeNode(currentNode, 'bl.bl_id');
    var flId = getValueFromTreeNode(currentNode, 'fl.fl_id');
    var dwgName = getValueFromTreeNode(currentNode, 'fl.dwgname');
    abBldgopsReportWrLocController.loadSvg(blId, flId, dwgName);	
    
    abBldgopsReportWrLocController.svg_ctrls.setTitle(title);
}

/**
 * get value from tree node
 * @param {Object} treeNode
 * @param {String} fieldName
 */
function getValueFromTreeNode(treeNode, fieldName){
    var value = null;
    if (treeNode.data[fieldName]) {
        value = treeNode.data[fieldName];
        return value;
    }
    if (treeNode.parent.data[fieldName]) {
        value = treeNode.parent.data[fieldName];
        return value;
    }
    if (treeNode.parent.parent.data[fieldName]) {
        value = treeNode.parent.parent.data[fieldName];
        return value;
    }
    if (treeNode.parent.parent.parent.data[fieldName]) {
        value = treeNode.parent.parent.parent.data[fieldName];
        return value;
    }
    return value;
}
