/**
 * Controller for the Craftperson Edit.
 */
var craftpersonEditCtrl = View.createController('craftpersonEditCtrl', {

    tabCtrls: {},

    selectedCfId: "",

    afterViewLoad: function() {
        var tabsPanel = View.panels.get('cfDetailTabs');
        tabsPanel.addEventListener('afterTabChange', this.afterTabChange.createDelegate(this));
        tabsPanel.addEventListener('beforeTabChange',this.beforeTabChange.createDelegate(this));
    },
    
    beforeTabChange: function(tabPanel, currentTabName, newTabName){
        var canChange=true;
        if(newTabName=='dailyScheduleTab'){
            //APP-1387:Check database schema for Bali6.
            if(!Workflow.callMethod('AbBldgOpsOnDemandWork-WorkRequestService-checkSchemaExisting','cf_schedules', 'cf_schedule_id').value){
                alert(getMessage('udpateSchemaMessage'));
                canChange =false;
            }
        }
        return canChange;
    },

    /**
     * To refresh the daily schedule panel after tab change. 
     */
    afterTabChange: function(tabPanel,newTabName) {
        if(newTabName=='dailyScheduleTab'){
            var currentTab = tabPanel.findTab(newTabName);
            if (currentTab.isContentLoaded) {
                var controller=this.tabCtrls[newTabName];
                controller.afterSelectCf(this.selectedCfId);
            }
        }
    },

    /**
     * event handler of select craftperson.
     */
    onSelectCraftsperson: function() {
        var selectedRowIndex = this.treePanel.selectedRowIndex;
        var selectedRowRecord = this.treePanel.gridRows.get(selectedRowIndex).getRecord();
        var cfId = selectedRowRecord.getValue('cf.cf_id');
        
        var res = new Ab.view.Restriction();
        res.addClause('cf.cf_id', cfId, '=');
        // set selected craftsperson.
        this.selectedCfId = cfId;
        var tabs = View.panels.get('cfDetailTabs');
        tabs.show(true);
        tabs.selectTab('generalDetailTab', res);

        for ( var tabName in this.tabCtrls) {
            if(tabName='generalDetailTab'){
                this.tabCtrls[tabName].afterSelectCf(cfId);
            }
        }

        this.enableDailyScheduleTab(true);
        
        this.craftspersonTabTitlePanel.show(true);
        this.craftspersonTabTitlePanel.setTitle(getMessage('cfTabsTitle').replace('{0}',cfId));
    },

    /**
     * Add new craftsperson.
     */
    onAddNewCraftsperson: function() {
        this.craftspersonTabTitlePanel.show(true);
        this.craftspersonTabTitlePanel.setTitle(getMessage('cfTabsAddNewTitle'));
        var tabs = View.panels.get('cfDetailTabs');
        tabs.show(true);
        tabs.selectTab('generalDetailTab', null, true);
        this.tabCtrls['generalDetailTab'].onAddNewCf();
        this.enableDailyScheduleTab(false);
        this.selectedCfId = "";
    },

    enableDailyScheduleTab: function(isShow) {
        var tabs = View.panels.get('cfDetailTabs');
        tabs.enableTab('dailyScheduleTab', isShow);
    }
});