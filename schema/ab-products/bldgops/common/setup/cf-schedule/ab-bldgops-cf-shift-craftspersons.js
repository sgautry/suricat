var shiftCraftspersonsCtrl = View.createController('shiftCraftspersonsCtrl', {

    selectedScheduleId: null,
    
    //translated VARIES message.
    variesMessage: '',

    afterInitialDataFetch: function() {
        //Build translated VARIES message.
        this.variesMessage = '< '+getMessage('variesMessage') +' >';
        
        var openerController = View.getOpenerView().controllers.get('shiftManagerCtrl');
        // register this sub-controller to opener view controller.
        this.registerController(openerController);
        this.selectedScheduleId=openerController.selectedScheduleId;
        this.refreshCraftspersonPanels();
        this.initialAssignedDateControlItems('assign_start_day.month','assign_end_day.month');
        this.initialAssignedDateControlItems('unassign_start_day.month','unassign_end_day.month');
    },

    /**
     * Register controller to opener view controller.
     */
    registerController: function(openerController) {
        openerController.tabCtrls['shiftCraftspersonsTab'] = this;
    },

    /**
     * refresh panels after selected shift from opener view.
     * @param {scheduleId} Selected craftsperson schedule code.
     */
    selectedShift: function(scheduleId) {
        this.selectedScheduleId = scheduleId;
        this.refreshCraftspersonPanels();
    },
    
    /**
     * Refresh craftsperson panels.
     */
    refreshCraftspersonPanels: function(){
        this.assignedCraftspersonList.show(true);
        this.refreshCraftspersonAssignList(this.selectedScheduleId);
        
        this.assignedDetailForm.show(true);
        this.showAssignedCraftspersonDetail(null);
        
        this.unassignedCraftspersonList.show(true);
        this.unassignedCraftspersonList.addParameter('withoutAssignedPram',true);
        this.unassignedCraftspersonList.addParameter('selectedScheduleId',this.selectedScheduleId);
        this.refreshUnassignedCraftspersonList();
        
        this.unassignedDetailForm.show(true);
        this.showUnassignedCraftspersonDetail(null);
        
        this.workTeamsList.show(false);
        
        View.getOpenerView().panels.get('shiftsListPanel').refresh();
    },
    
    /**
     * Refresh assigned craftsperson list.
     */
    refreshCraftspersonAssignList: function(scheduleId){
        var res=new Ab.view.Restriction();
            res.addClause('cf_schedules_assign.cf_schedule_id',scheduleId,'=');
        this.assignedCraftspersonList.refresh(res);
    },
    
    refreshUnassignedCraftspersonList: function(){
        this.unassignedCraftspersonList.refresh();
    },
    
    /**
     * Show details of assigned rows multiple selection change.
     */
    assignedCraftspersonList_onMultipleSelectionChange: function(row) {
        var selectedRows = this.assignedCraftspersonList.getSelectedGridRows();
        this.showAssignedCraftspersonDetail(selectedRows);
    },
    
    /**
     * Show details of unassigned rows multiple selection change.
     */
    unassignedCraftspersonList_onMultipleSelectionChange: function(){
        var selectedRows=this.unassignedCraftspersonList.getSelectedGridRows();
        this.showUnassignedCraftspersonDetail(selectedRows);
    },
    
    /**
     * On checked Is Seasonal checkbox in assigned craftsperson detail form.
     */
    onAssignedIsSeasonalCheckboxChanged: function(){
        var isSeasonal=$('isAssignedSesonalCbx').checked?'1':'0';

        this.switchAssignedDateControlsByIsSeasonalValue(isSeasonal);
        
        if(isSeasonal=='1'){
            var startDate=this.assignedDetailForm.getFieldValue('cf_schedules_assign.date_start');
            var endDate=this.assignedDetailForm.getFieldValue('cf_schedules_assign.date_end');
            if(valueExistsNotEmpty(startDate)){
                startDate=this.assignedDetailForm.getDataSource().parseValue('cf_schedules_assign.date_start',startDate,false);
            }
            if(valueExistsNotEmpty(endDate)){
                endDate=this.assignedDetailForm.getDataSource().parseValue('cf_schedules_assign.date_end',endDate,false);
            }
            
            this.setAssignedMonthAndDayOfSeasonal(startDate,endDate);
        }
    },
    
    /**
     * On checked Is Seasonal checkbox in Unassigned craftsperson detail form.
     */
    onUnassignedIsSeasonalCheckboxChanged: function(){
        var isSeasonal=$('isUnassignedSesonalCbx').checked?'1':'0';
        // switch date controls.
        this.switchUnassignedDateControlsByIsSeasonalValue(isSeasonal);
        // set value to seasonal date control.
        if(isSeasonal=='1'){
            var startDate=this.unassignedDetailForm.getFieldValue('cf_schedules_assign.date_start');
            var endDate=this.unassignedDetailForm.getFieldValue('cf_schedules_assign.date_end');
            if(valueExistsNotEmpty(startDate)){
                startDate=this.unassignedDetailForm.getDataSource().parseValue('cf_schedules_assign.date_start',startDate,false);
            }
            if(valueExistsNotEmpty(endDate)){
                endDate=this.unassignedDetailForm.getDataSource().parseValue('cf_schedules_assign.date_end',endDate,false);
            }
            this.setUnassignedMonthAndDayOfSeasonal(startDate,endDate);
        }
    },
    
    /**
     * Save assigned form.
     */
    assignedDetailForm_onSave: function(){
        var selectedAssignedRecors=this.assignedCraftspersonList.getSelectedRecords();
        if(selectedAssignedRecors.length<1){
            View.alert(getMessage('selectAtLeastOneAssignedCfMessage'));
            return;
        }
        var validatePass=this.validateAssignedFormFieldsBeforeSave(selectedAssignedRecors);
        
        if(validatePass){
            var assignDs=this.cfScheduleAssignDs;
            var isSeasonal=$('isAssignedSesonalCbx').checked?'1':'0';
            var dateStart=this.assignedDetailForm.getFieldValue('cf_schedules_assign.date_start');
            var dateEnd=this.assignedDetailForm.getFieldValue('cf_schedules_assign.date_end');
            _.each(selectedAssignedRecors,function(record){
                var autoNumber=record.getValue('cf_schedules_assign.auto_number');
                var assignRes=new Ab.view.Restriction();
                    assignRes.addClause('cf_schedules_assign.auto_number',autoNumber,'=');
                var assignRecord=assignDs.getRecord(assignRes);
                assignRecord.isNew=false;
                assignRecord.setValue('cf_schedules_assign.date_start',dateStart);
                assignRecord.setValue('cf_schedules_assign.date_end',dateEnd);
                assignRecord.setValue('cf_schedules_assign.is_seasonal',isSeasonal);
                assignDs.saveRecord(assignRecord);
            });
            
            // refresh craftsperson assigned panels after saved.
            this.refreshCraftspersonPanels();
        }
    },
    
    /**
     * Remove the selected assigned schedules.
     */
    assignedDetailForm_onRemove: function(){
        var me=this;
        
        var selectedAssignedRowsRecors=this.assignedCraftspersonList.getSelectedRecords();
        if(selectedAssignedRowsRecors.length<1){
            View.alert(getMessage('selectAtLeastOneAssignedCfMessage'));
            return;
        }
        View.confirm(getMessage('deleteAllShiftsAssignmentsMessage'),function(button){
            if(button=="yes"){
                _.each(selectedAssignedRowsRecors,function(record){
                    me.removeCraftspersonAssignement(record);
                });
                
                // refresh craftsperson assigned panels after saved.
                me.refreshCraftspersonPanels();
            }
        });
    },
   
    assignedDetailForm_onCancel: function(){
        this.refreshCraftspersonPanels();
    },
    
    /**
     * On remove selected assignment by clicking the 'Remove' button.
     */
    onRemoveSelectedAssignment: function(){
        var me=this;
        var selectedRowIndex=this.assignedCraftspersonList.selectedRowIndex;
        var selectedRowRecord=this.assignedCraftspersonList.gridRows.get(selectedRowIndex).getRecord();
        View.confirm(getMessage('deleteShiftAssignmentsMessage'),function(button){
            if(button=='yes'){
                me.removeCraftspersonAssignement(selectedRowRecord);
                // refresh craftsperson assigned panels after saved.
                me.refreshCraftspersonPanels();
            }
        });
        
    },
    
    /**
     * Remove craftsperson shift assignment.
     * @param {record} Craftsperson assignment record.
     */
    removeCraftspersonAssignement: function(record){
        record.isNew=false;
        this.cfScheduleAssignDs.deleteRecord(record);
    },
    
    /**
     * Add selected craftsperson to specify shift.
     */
    unassignedDetailForm_onAddSelected: function(){
        var me=this;
        var selectedCraftspersonRecords=this.unassignedCraftspersonList.getSelectedRecords();
        if(selectedCraftspersonRecords.length<1){
            View.alert(getMessage('selectAtLeastOneCfMessage'));
            return;
        }
        // validate form fields.
        var isPassed=this.validateUnassignedFormFieldsBeforeAdded(selectedCraftspersonRecords);
        if(isPassed){
            var isSeasonal=$('isUnassignedSesonalCbx').checked?'1':'0';
            var dateStart=this.unassignedDetailForm.getFieldValue('cf_schedules_assign.date_start');
            var dateEnd=this.unassignedDetailForm.getFieldValue('cf_schedules_assign.date_end');
            
            _.each(selectedCraftspersonRecords,function(record){
                var cfId=record.getValue('cf.cf_id');
                me.addNewCraftspersonAssignment(cfId,me.selectedScheduleId,isSeasonal,dateStart,dateEnd);
            });
            
            // refresh craftsperson assigned panels after saved.
            me.refreshCraftspersonPanels();
        }
    },
    
    /**
     * Add new shift assignment for single selected craftsperson by clicking the 'Add' button in grid row.
     */
    onAddNewShiftAssignment: function(){
        var selectRowIndex=this.unassignedCraftspersonList.selectedRowIndex;
        var selectedCfRecord=this.unassignedCraftspersonList.gridRows.get(selectRowIndex).getRecord();
        var cfAssignedId=null;
        var cfId=selectedCfRecord.getValue('cf.cf_id');
        var dateStart='';
        var dateEnd='';
        var isSeasonal='0';
        var overlapCheckPass = this.validateSingleCfDateRangeOverlap(cfAssignedId, cfId, dateStart, dateEnd, isSeasonal);
        if(!overlapCheckPass){
            View.alert(getMessage('singleCraftspersonScheduleOverlapMessage'));
            return;
        }
        this.addNewCraftspersonAssignment(cfId,this.selectedScheduleId,isSeasonal,dateStart,dateEnd);
        // refresh craftsperson assigned panels after saved.
        this.refreshCraftspersonPanels();
    },
    
    /**
     * Add new craftsperson assignment.
     * @param {cfId} Craftsperson Code.
     * @param {scheduleId} Crafsperson Schedule Code.
     * @param {isSeasonal} Is Seasonal?
     * @param {dateStart} Start Date.
     * @param {dateEnd} End Date.
     */
    addNewCraftspersonAssignment: function(cfId,scheduleId,isSeasonal,dateStart,dateEnd){
        var scheduleAssignRecord = new Ab.data.Record();
        scheduleAssignRecord.isNew = true;
        scheduleAssignRecord.oldValues = new Object();
        scheduleAssignRecord.setValue('cf_schedules_assign.cf_id', cfId);
        scheduleAssignRecord.setValue('cf_schedules_assign.cf_schedule_id', scheduleId);
        scheduleAssignRecord.setValue('cf_schedules_assign.is_seasonal', isSeasonal);
        scheduleAssignRecord.setValue('cf_schedules_assign.date_start', dateStart);
        scheduleAssignRecord.setValue('cf_schedules_assign.date_end', dateEnd);
        this.cfScheduleAssignDs.saveRecord(scheduleAssignRecord);
    },
    
    /**
     * Validate craftsperson schedule date range overlap before save.
     */
    validateAssignedFormFieldsBeforeSave: function(selectedAssignedRecords) {
        var isPass = true;
        var newIsSeasonal = $('isAssignedSesonalCbx').checked ? '1' : '0';
        // set date value from new date control if schedule is seasonal.
        this.setDateValuesByIsSeasonal('assignedDetailForm','assign_start_day.month','assign_start_day.day','assign_end_day.month','assign_end_day.day',newIsSeasonal);
        var newStartDate = this.assignedDetailForm.getFieldValue('cf_schedules_assign.date_start');
        var newEndDate = this.assignedDetailForm.getFieldValue('cf_schedules_assign.date_end');

        // check date fields in form.
        var newStartDateObj = newStartDate;
        if (valueExistsNotEmpty(newStartDateObj)) {
            newStartDateObj = new Date(newStartDateObj);
            newStartDateObj.setHours(0, 0, 0, 0);
        }
        var newDateEndObj = newEndDate;
        if (valueExistsNotEmpty(newDateEndObj)) {
            newDateEndObj = new Date(newDateEndObj);
            newDateEndObj.setHours(0, 0, 0, 0);
        }
        var dateFieldsCheckResult = checkDateFieldsBeforeSave(newStartDateObj, newDateEndObj, newIsSeasonal);
        if (!dateFieldsCheckResult) {
            isValidate = false;
            return;
        }

        // validate schedules overlapping.
        if (!this.validateAssignedMultipleCfDateRangeOverlap(selectedAssignedRecords, newStartDate, newEndDate, newIsSeasonal)) {
            isPass = false;
        }
        return isPass;
    },
    
    /**
     * Validate craftsperson schedule date range overlap before save.
     */
    validateUnassignedFormFieldsBeforeAdded: function(selectedCraftspersonRecords) {
        var isPass = true;
        var newIsSeasonal = $('isUnassignedSesonalCbx').checked ? '1' : '0';
        // set date value from new date control if schedule is seasonal.
        this.setDateValuesByIsSeasonal('unassignedDetailForm','unassign_start_day.month','unassign_start_day.day','unassign_end_day.month','unassign_end_day.day',newIsSeasonal);
        var newStartDate = this.unassignedDetailForm.getFieldValue('cf_schedules_assign.date_start');
        var newEndDate = this.unassignedDetailForm.getFieldValue('cf_schedules_assign.date_end');

        // check date fields in form.
        var newStartDateObj = newStartDate;
        if (valueExistsNotEmpty(newStartDateObj)) {
            newStartDateObj = new Date(newStartDateObj);
            newStartDateObj.setHours(0, 0, 0, 0);
        }
        var newDateEndObj = newEndDate;
        if (valueExistsNotEmpty(newDateEndObj)) {
            newDateEndObj = new Date(newDateEndObj);
            newDateEndObj.setHours(0, 0, 0, 0);
        }
        var dateFieldsCheckResult = checkDateFieldsBeforeSave(newStartDateObj, newDateEndObj, newIsSeasonal);
        if (!dateFieldsCheckResult) {
            isValidate = false;
            return;
        }

        // validate schedules overlapping.
        if (!this.validateUnssignedMultipleCfDateRangeOverlap(selectedCraftspersonRecords, newStartDate, newEndDate, newIsSeasonal)) {
            isPass = false;
        }
        return isPass;
    },
    
    /**
     * Set date values based on Is Seasonal field value.
     */
    setDateValuesByIsSeasonal: function(panelId,startMonthControlId,startDayControlId,endMonthControlId,endDayControlId,isSeasonal){
        if(isSeasonal=='1'){
            var currentDate=new Date();
            var startMonth=$(startMonthControlId).value;
                startMonth=valueExistsNotEmpty(startMonth)?parseInt(startMonth):'';
            var startDate=$(startDayControlId).value;
                startDate=valueExistsNotEmpty(startDate)?parseInt(startDate):'';
            var endMonth=$(endMonthControlId).value;
                endMonth=valueExistsNotEmpty(endMonth)?parseInt(endMonth):'';
            var endDate=$(endDayControlId).value;
                endDate=valueExistsNotEmpty(endDate)?parseInt(endDate):'';
            
            if(valueExistsNotEmpty(startMonth)){
                if(valueExistsNotEmpty(startDate)){
                    var newStartDate=new Date(currentDate.getFullYear(),startMonth,startDate);
                    var formattedStartDate=getIsoFormatDate(newStartDate);
                    View.panels.get(panelId).setFieldValue('cf_schedules_assign.date_start',formattedStartDate);
                }
            }else{
                View.panels.get(panelId).setFieldValue('cf_schedules_assign.date_start','');
            }
            
            if(valueExistsNotEmpty(endMonth)){
                if(valueExistsNotEmpty(endDate)){
                    var year=currentDate.getFullYear();
                    if(endMonth<startMonth||(endMonth==startMonth&&endDate<startDate)){
                        year+=1;
                    }
                    var newEndDate=new Date(year,endMonth,endDate);
                    var formattedEndDate=getIsoFormatDate(newEndDate);
                    View.panels.get(panelId).setFieldValue('cf_schedules_assign.date_end',formattedEndDate);
                }
            }else{
                View.panels.get(panelId).setFieldValue('cf_schedules_assign.date_end','');
            }
            
        }
    },
    
    /**
     * Validation selected craftsperson assignments date range overlapping.
     */
    validateAssignedMultipleCfDateRangeOverlap: function(selectedAssignedRecords, dateStart, dateEnd, isSeasonal) {
        var me = this;
        var validatePassed = true;
        var notPassedCfArray = [];
        var cfAssignedId=null;
        _.each(selectedAssignedRecords, function(record) {
            cfAssignedId = record.getValue('cf_schedules_assign.auto_number');
            var cfId = record.getValue('cf_schedules_assign.cf_id');
            var overlapCheckPass = me.validateSingleCfDateRangeOverlap(cfAssignedId, cfId, dateStart, dateEnd, isSeasonal);
            if (!overlapCheckPass) {
                validatePassed = false;
                notPassedCfArray.push(cfId);
            }
        });

        if (!validatePassed) {
            var basicMessage = getMessage('multipleScheduleDateRangeOverlapMessage');
            _.each(notPassedCfArray, function(notPassCfId) {
                basicMessage += (notPassCfId + ";");
            });
            View.alert(basicMessage);
        }

        return validatePassed;
    },
    
    /**
     * Validation selected unassigned craftsperson date range overlapping.
     */
    validateUnssignedMultipleCfDateRangeOverlap: function(selectedCraftspersonRecords, dateStart, dateEnd, isSeasonal) {
        var me = this;
        var validatePassed = true;
        var notPassedCfArray = [];
        var cfAssignedId=null;
        _.each(selectedCraftspersonRecords, function(record) {
            var cfId = record.getValue('cf.cf_id');
            var overlapCheckPass = me.validateSingleCfDateRangeOverlap(cfAssignedId, cfId, dateStart, dateEnd, isSeasonal);
            if (!overlapCheckPass) {
                validatePassed = false;
                notPassedCfArray.push(cfId);
            }
        });

        if (!validatePassed) {
            var basicMessage = getMessage('multipleScheduleDateRangeOverlapMessage');
            _.each(notPassedCfArray, function(notPassCfId) {
                basicMessage += (notPassCfId + ";");
            });
            View.alert(basicMessage);
        }

        return validatePassed;
    },
    
    /**
     * Validation single craftsperson schedule date range overlap.
     * @param {currentSelectedAssignedId} Current assigned id of selected craftsperson.
     */
    validateSingleCfDateRangeOverlap: function(selectedAssignedId, cfId, dateStart, dateEnd, isSeasonal) {
        var isOverlappedPassed;
        // call common validation
        if (valueExistsNotEmpty(dateStart)) {
            dateStart = new Date(dateStart);
            dateStart.setHours(0, 0, 0, 0);
        }
        if (valueExistsNotEmpty(dateEnd)) {
            dateEnd = new Date(dateEnd);
            dateEnd.setHours(0, 0, 0, 0);
        }
        
        var cfRes=new Ab.view.Restriction();
            cfRes.addClause('cf_schedules_assign.cf_id',cfId,'=');
        var cfAssignedRecords=this.cfScheduleAssignDs.getRecords(cfRes);    
        // check date range overlap before add.
        isOverlappedPassed = validateScheduleDateRangeOverlap(cfAssignedRecords, selectedAssignedId , dateStart, dateEnd, isSeasonal);

        return isOverlappedPassed;
    },
    
    
    /**
     * Formatted start date and end date value for seasonal schedule.
     */
    assignedCraftspersonList_afterGetData: function(panel, dataSet){
        // format schedule assigned date for seasonal scheudle.
        var me=this;
        var gridRows=this.assignedCraftspersonList.gridRows.items;
        var ds=this.assignedCraftspersonList.getDataSource();
        _.each(dataSet.records,function(record){
            var isSeasonal=record['cf_schedules_assign.is_seasonal.raw'];
            
            if(isSeasonal=='1'){
                var dateStartFormattedValue = record['cf_schedules_assign.date_start.raw']?record['cf_schedules_assign.date_start.raw']:record['cf_schedules_assign.date_start'];
                var dateEndFormattedValue = record['cf_schedules_assign.date_end.raw']?record['cf_schedules_assign.date_end.raw']:record['cf_schedules_assign.date_end'];
                
                var dateStart=ds.parseValue('cf_schedules_assign.date_start',dateStartFormattedValue,false);
                var dateEnd=ds.parseValue('cf_schedules_assign.date_end',dateEndFormattedValue,false);
                if(valueExistsNotEmpty(dateStart)){
                    var displayValue=me.getFormattedDateForSeasonalSchedule(dateStart);
                    record['cf_schedules_assign.date_start']=displayValue;
                }
                
                if(valueExistsNotEmpty(dateEnd)){
                    var displayValue=me.getFormattedDateForSeasonalSchedule(dateEnd);
                    record['cf_schedules_assign.date_end']=displayValue;
                }
            }
        });
    },
    
    /**
     * Formatted start date and end date value for craftsperson shifts.
     */
    craftspersonShifts_afterGetData: function(panel, dataSet){
        var me=this;
        // format schedule assigned date for seasonal scheudle.
        var gridRows=this.craftspersonShifts.gridRows.items;
        var ds=this.craftspersonShifts.getDataSource();
        _.each(dataSet.records,function(record){
            var isSeasonal=record['cf_schedules_assign.is_seasonal.raw'];
            
            if(isSeasonal=='1'){
                
                var dateStartFormattedValue = record['cf_schedules_assign.date_start.raw']?record['cf_schedules_assign.date_start.raw']:record['cf_schedules_assign.date_start'];
                var dateEndFormattedValue = record['cf_schedules_assign.date_end.raw']?record['cf_schedules_assign.date_end.raw']:record['cf_schedules_assign.date_end'];
                
                var dateStart=ds.parseValue('cf_schedules_assign.date_start',dateStartFormattedValue,false);
                var dateEnd=ds.parseValue('cf_schedules_assign.date_end',dateEndFormattedValue,false);
                if(valueExistsNotEmpty(dateStart)){
                    var displayValue=me.getFormattedDateForSeasonalSchedule(dateStart);
                    record['cf_schedules_assign.date_start']=displayValue;
                }
                
                if(valueExistsNotEmpty(dateEnd)){
                    var displayValue=me.getFormattedDateForSeasonalSchedule(dateEnd);
                    record['cf_schedules_assign.date_end']=displayValue;
                }
            }
        });
    },
    
    /**
     * Get formatted date value for seasonal schedule.
     * @param {date} Date object.
     * @return Formatted date value, only show month and day , like 'July 8'
     */
    getFormattedDateForSeasonalSchedule: function(date){
        var displayValue='';
        var startMonth=date.getMonth();
        var startDay=date.getDate();
        var monthName=getMonthName(startMonth);
        return monthName+' '+startDay;
    },

    /**
     * Show craftsperson assigned schedule details by selected rows.
     */
    showAssignedCraftspersonDetail: function(rows) {
        if(rows && rows.length>0){
            this.setSelectedAssignedCraftsperonCode(rows);
            this.setSelectedAssignedCraftspersonWorkTeam(rows);
            this.setAssignedStartDate(rows);
            this.setAssignedEndDate(rows);
            this.setSelectedAssignedSeasonal(rows);
        }else{
            this.assignedDetailForm.clear();
            $('isAssignedSesonalCbx').checked = false;
            this.switchAssignedDateControlsByIsSeasonalValue('0');
        }
    },
    
    /**
     * Show assign form detail after selected unassigned record.
     */
    showUnassignedCraftspersonDetail: function(rows){
        if(rows && rows.length>0){
            this.setSelectedUnassignedCraftspersonCode(rows);
            this.setSelectedUnassignedCraftspersonWorkTeam(rows);
        }else{
            this.unassignedDetailForm.clear();
            $('isUnassignedSesonalCbx').checked = false;
            this.switchUnassignedDateControlsByIsSeasonalValue('0');
        }
    },
    
    /**
     * Show work team list for selected assigned craftsperson.
     */
    showWorkTeamsForAssignedCf: function(){
        var selectedRowIndex=this.assignedCraftspersonList.selectedRowIndex;
        var cfId=this.assignedCraftspersonList.gridRows.get(selectedRowIndex).getRecord().getValue('cf_schedules_assign.cf_id');
        this.showCraftspersonWorkTeamPopup(cfId);
    },
    
    /**
     * Show work team list for selected unassigned craftseprsons.
     */
    showWorkTeamsForUnassignedCf: function(){
        var selectedRowIndex=this.unassignedCraftspersonList.selectedRowIndex;
        var cfId=this.unassignedCraftspersonList.gridRows.get(selectedRowIndex).getRecord().getValue('cf.cf_id');
        this.showCraftspersonWorkTeamPopup(cfId);
    },

    /**
     * Set Craftsperson Code field value by selected rows.
     */
    setSelectedAssignedCraftsperonCode: function(rows) {
        var craftspersonCode = rows.length > 0 ? rows[0].getRecord().getValue('cf_schedules_assign.cf_id') : '';
        for (var i = 0; i < rows.length; i++) {
            var rowRecord = rows[i].getRecord();
            if (rowRecord.getValue('cf_schedules_assign.cf_id') != craftspersonCode) {
                craftspersonCode = this.variesMessage;
                break;
            }
        }
        this.assignedDetailForm.setFieldValue('cf_schedules_assign.cf_id', craftspersonCode);
    },
    
    /**
     * Set craftsperson code for unassigned form.
     */
    setSelectedUnassignedCraftspersonCode: function(rows){
        var craftspersonCode = rows.length > 0 ? rows[0].getRecord().getValue('cf.cf_id') : '';
        for (var i = 0; i < rows.length; i++) {
            var rowRecord = rows[i].getRecord();
            if (rowRecord.getValue('cf.cf_id') != craftspersonCode) {
                craftspersonCode = this.variesMessage;
                break;
            }
        }
        this.unassignedDetailForm.setFieldValue('cf_schedules_assign.cf_id', craftspersonCode);
    },

    /**
     * Set Work Team field value by selected rows.
     */
    setSelectedAssignedCraftspersonWorkTeam: function(rows) {
        var workTeamCode = rows.length > 0 ? rows[0].getRecord().getValue('cf_schedules_assign.vfWorkTeamCode') : '';
        for (var i = 0; i < rows.length; i++) {
            var rowRecord = rows[i].getRecord();
            if (rowRecord.getValue('cf_schedules_assign.vfWorkTeamCode') != workTeamCode) {
                workTeamCode = this.variesMessage;
                break;
            }
        }
        this.assignedDetailForm.setFieldValue('cf.work_team_id', workTeamCode);
    },
    
    /**
     * Set Work Team field value by selected rows.
     */
    setSelectedUnassignedCraftspersonWorkTeam: function(rows) {
        var workTeamCode = rows.length > 0 ? rows[0].getRecord().getValue('cf.vfWorkTeamCode') : '';
        for (var i = 0; i < rows.length; i++) {
            var rowRecord = rows[i].getRecord();
            if (rowRecord.getValue('cf.vfWorkTeamCode') != workTeamCode) {
                workTeamCode = this.variesMessage;
                break;
            }
        }
        this.unassignedDetailForm.setFieldValue('cf.work_team_id', workTeamCode);
    },

    /**
     * Set Is Seasonal checkbox status after selected assigned record.
     */
    setSelectedAssignedSeasonal: function(rows) {
        var isSeasonal = rows.length > 0 ? rows[0].getRecord().getValue('cf_schedules_assign.is_seasonal') : '0';
        for (var i = 0; i < rows.length; i++) {
            var rowRecord = rows[i].getRecord();
            if (rowRecord.getValue('cf_schedules_assign.is_seasonal') != isSeasonal) {
                isSeasonal = '0';
                break;
            }
        }
        if (isSeasonal == '1') {
            $('isAssignedSesonalCbx').checked = true;
        } else {
            $('isAssignedSesonalCbx').checked = false;
        }
        
        // switch to date control by Is Seasonal field value.
        this.switchAssignedDateControlsByIsSeasonalValue(isSeasonal);
        
        // set month control and date control value.
        if(isSeasonal == '1'){
            var startDate=this.assignedDetailForm.getFieldValue('cf_schedules_assign.date_start');
            var endDate=this.assignedDetailForm.getFieldValue('cf_schedules_assign.date_end');
            if(valueExistsNotEmpty(startDate)){
                startDate=this.assignedDetailForm.getDataSource().parseValue('cf_schedules_assign.date_start',startDate,false);
            }
            if(valueExistsNotEmpty(endDate)){
                endDate=this.assignedDetailForm.getDataSource().parseValue('cf_schedules_assign.date_end',endDate,false);
            }
            
            this.setAssignedMonthAndDayOfSeasonal(startDate,endDate);
        }
    },

    /**
     * set start date for selected assigned rows.
     */
    setAssignedStartDate: function(rows) {
        var startDate = "";
        if(rows.length>0){
            startDate=rows[0].getRecord().getValue('cf_schedules_assign.date_start');
            if(valueExistsNotEmpty(startDate)){
                startDate=getIsoFormatDate(startDate);
            }
        }
        for (var i = 1; i < rows.length; i++) {
            var rowStartDate=rows[i].getRecord().getValue('cf_schedules_assign.date_start');
            if(valueExistsNotEmpty(rowStartDate)){
                rowStartDate=getIsoFormatDate(rowStartDate);
            }
            if(rowStartDate!=startDate){
                startDate="";
            }
        }
        this.assignedDetailForm.setFieldValue('cf_schedules_assign.date_start',startDate);
    },
    
    /**
     * set start date for selected assigned rows.
     */
    setAssignedEndDate: function(rows) {
        var endDate = "";
        if(rows.length>0){
            endDate=rows[0].getRecord().getValue('cf_schedules_assign.date_end');
            if(valueExistsNotEmpty(endDate)){
                endDate=getIsoFormatDate(endDate);
            }
        }
        for (var i = 1; i < rows.length; i++) {
            var rowEndDate=rows[i].getRecord().getValue('cf_schedules_assign.date_end');
            if(valueExistsNotEmpty(rowEndDate)){
                rowEndDate=getIsoFormatDate(rowEndDate);
            }
            if(rowEndDate!=endDate){
                endDate="";
            }
        }
        this.assignedDetailForm.setFieldValue('cf_schedules_assign.date_end',endDate);
    },
    
    /**
     * Show craftsperson work teams list popup by selected craftsperson.
     * @param {cfId} Craftsperson Code.
     */
    showCraftspersonWorkTeamPopup: function(cfId){
        var cfRes=new Ab.view.Restriction();
            cfRes.addClause('cf_work_team.cf_id',cfId,'=');
        this.workTeamsList.show(true);
        this.workTeamsList.showInWindow({
            width:500,
            height:600,
            x:300,
            modal:true,
            title: getMessage('cfWorkTeamTitle').replace('{0}',cfId)
        });
        this.workTeamsList.refresh(cfRes);
    },
    
    /**
     * Show craftsperson shifts popup.
     */
    showCfShiftsPopup: function(){
        var selectedRowIndex=this.unassignedCraftspersonList.selectedRowIndex;
        var selectedRowRecord=this.unassignedCraftspersonList.gridRows.get(selectedRowIndex).getRecord();
        var cfId=selectedRowRecord.getValue('cf.cf_id');
        if(valueExistsNotEmpty(cfId)){
            var cfRes=new Ab.view.Restriction();
            cfRes.addClause('cf_work_team.cf_id',cfId,'=');
            this.craftspersonShifts.show(true);
            this.craftspersonShifts.showInWindow({
                width:500,
                height:600,
                x:300,
                modal:true,
                title: getMessage('cfSfhitsPopupTitle').replace('{0}',cfId)
            });
            this.craftspersonShifts.refresh(cfRes);
        }
    },
    
    /**
     * Switch date field control by is seasonal value.
     */
    switchAssignedDateControlsByIsSeasonalValue: function(isSeasonal){
        var hideNewDateControl=true;
        if(isSeasonal=='1'){
            this.assignedDetailForm.showField('cf_schedules_assign.date_start',false);
            this.assignedDetailForm.showField('cf_schedules_assign.date_end',false);
            hideNewDateControl=false;
        }else{
            this.assignedDetailForm.showField('cf_schedules_assign.date_start',true);
            this.assignedDetailForm.showField('cf_schedules_assign.date_end',true);
            hideNewDateControl=true;
        }
        
        if(hideNewDateControl){
            Ext.get('assignedDetailForm_cf_schedules_assign.assignStartDayControl_labelCell').hide();
            Ext.get('assignedDetailForm_cf_schedules_assign.assignStartDayControl_fieldCell').hide();
            Ext.get('assignedDetailForm_cf_schedules_assign.assignEndDayControl_labelCell').hide();
            Ext.get('assignedDetailForm_cf_schedules_assign.assignEndDayControl_fieldCell').hide();
        }else{
            Ext.get('assignedDetailForm_cf_schedules_assign.assignStartDayControl_labelCell').show();
            Ext.get('assignedDetailForm_cf_schedules_assign.assignStartDayControl_fieldCell').show();
            Ext.get('assignedDetailForm_cf_schedules_assign.assignEndDayControl_labelCell').show();
            Ext.get('assignedDetailForm_cf_schedules_assign.assignEndDayControl_fieldCell').show();
        }
        
    },
    
    /**
     * Switch date field control by is seasonal value.
     */
    switchUnassignedDateControlsByIsSeasonalValue: function(isSeasonal){
        var hideNewDateControl=true;
        if(isSeasonal=='1'){
            this.unassignedDetailForm.showField('cf_schedules_assign.date_start',false);
            this.unassignedDetailForm.showField('cf_schedules_assign.date_end',false);
            hideNewDateControl=false;
        }else{
            this.unassignedDetailForm.showField('cf_schedules_assign.date_start',true);
            this.unassignedDetailForm.showField('cf_schedules_assign.date_end',true);
            hideNewDateControl=true;
        }
        
        if(hideNewDateControl){
            Ext.get('unassignedDetailForm_cf_schedules_assign.unassignStartDayControl_labelCell').hide();
            Ext.get('unassignedDetailForm_cf_schedules_assign.unassignStartDayControl_fieldCell').hide();
            Ext.get('unassignedDetailForm_cf_schedules_assign.unassignEndDayControl_labelCell').hide();
            Ext.get('unassignedDetailForm_cf_schedules_assign.unassignEndDayControl_fieldCell').hide();
        }else{
            Ext.get('unassignedDetailForm_cf_schedules_assign.unassignStartDayControl_labelCell').show();
            Ext.get('unassignedDetailForm_cf_schedules_assign.unassignStartDayControl_fieldCell').show();
            Ext.get('unassignedDetailForm_cf_schedules_assign.unassignEndDayControl_labelCell').show();
            Ext.get('unassignedDetailForm_cf_schedules_assign.unassignEndDayControl_fieldCell').show();
        }
        
    },
    
    /**
     * Initial month select fields of customized start date and end date field.
     * @param {startMonthControlId} Start month control id.
     * @param {endMonthControlId} End month control id.
     */
    initialAssignedDateControlItems: function(startMonthControlId,endMonthControlId){
        var startMonthControl=jQuery($(startMonthControlId));
        var endMonthControl=jQuery($(endMonthControlId));
        var monthItems=getMonthItems();
        jQuery('<option>').attr('value','').text('').appendTo(startMonthControl);
        jQuery('<option>').attr('value','').text('').appendTo(endMonthControl)
        _.each(monthItems,function(monthObject){
            jQuery('<option>').attr('value',monthObject.key).text(monthObject.value).appendTo(startMonthControl);
            jQuery('<option>').attr('value',monthObject.key).text(monthObject.value).appendTo(endMonthControl);
        });
    },
    
    /**
     * Set month value.
     */
    setMonthControlValue: function(monthControlId,monthValue,dayControlId){
        $(monthControlId).value=monthValue;
        
        if(valueExistsNotEmpty(dayControlId)){
            this.initialDaysItems(dayControlId,monthValue);
        }
    },
    
    /**
     * Set days value.
     */
    setDayControlValue: function(dayControlId,value){
        $(dayControlId).value=value;
    },
    
    /**
     * Initial day control item.
     */
    initialDaysItems: function(dayControlId,monthValue){
        $(dayControlId).innerHTML='';
        var daysControl=jQuery($(dayControlId));
        jQuery('<option>').attr('value','').text('').appendTo(daysControl);
        if(valueExistsNotEmpty(monthValue)){
            var days=getDaysByMonth(monthValue);
            _.each(days,function(dayObject){
                jQuery('<option>').attr('value',dayObject.key).text(dayObject.value).appendTo(daysControl);
            });
        }
    },
    
    /**
     * Set month control and day control value.
     */
    setMonthAndDayValue: function(monthControlId,dayControlId,monthValue,dayValue){
        this.setMonthControlValue(monthControlId,monthValue,dayControlId);
        this.setDayControlValue(dayControlId,dayValue);
    },
    
    /**
     * Set month control and day control value for seasonal schedule of assigned form.
     */
    setAssignedMonthAndDayOfSeasonal: function(startDate,endDate){
        var startMonth='',endMonth='',startDay='',endDay='';
        if(valueExistsNotEmpty(startDate)){
            startMonth=startDate.getMonth();
            startDay=startDate.getDate();
        }
        
        if(valueExistsNotEmpty(endDate)){
            endMonth=endDate.getMonth();
            endDay=endDate.getDate();
        }
        
        this.setMonthControlValue('assign_start_day.month',startMonth,'assign_start_day.day');
        this.setDayControlValue('assign_start_day.day',startDay);
        
        this.setMonthControlValue('assign_end_day.month',endMonth,'assign_end_day.day');
        this.setDayControlValue('assign_end_day.day',endDay);
    },
    
    /**
     * Set month control and day control value for seasonal schedule of assigned form.
     */
    setUnassignedMonthAndDayOfSeasonal: function(startDate,endDate){
        var startMonth='',endMonth='',startDay='',endDay='';
        if(valueExistsNotEmpty(startDate)){
            startMonth=startDate.getMonth();
            startDay=startDate.getDate();
        }
        
        if(valueExistsNotEmpty(endDate)){
            endMonth=endDate.getMonth();
            endDay=endDate.getDate();
        }
        
        this.setMonthControlValue('unassign_start_day.month',startMonth,'unassign_start_day.day');
        this.setDayControlValue('unassign_start_day.day',startDay);
        
        this.setMonthControlValue('unassign_end_day.month',endMonth,'unassign_end_day.day');
        this.setDayControlValue('unassign_end_day.day',endDay);
    }
});

function onCheckedAssignedIsSeasonalCheckbox(){
    shiftCraftspersonsCtrl.onAssignedIsSeasonalCheckboxChanged();
}

function onCheckedUnassignedIsSeasonalCheckbox(){
    shiftCraftspersonsCtrl.onUnassignedIsSeasonalCheckboxChanged();
}

function onSelectMonthValue(monthControlId,dayControlId){
    var selectedOptionIndex = $(monthControlId).selectedIndex;
    var month=$(monthControlId).options[selectedOptionIndex].value;
    shiftCraftspersonsCtrl.setMonthControlValue(monthControlId,month,dayControlId);
}