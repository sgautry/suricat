var cfDailyScheduleControl = View.createController('cfDailyScheduleControl', {
    
    // default selected rate type.
    defaultSelectedRateType: 'STANDARD',
    // flag to indicate in add new mode or not.
    addNewMode: false,
    
    selectedCfId: null,
    //=================Schedule detail form global variables =========//
    //selected assign record.
    selectedAssignRecord: null,
    //selected shift.
    selectedShift: null,
    // selected schedule type.
    selectedScheduleType: null,
    
    //====================Scheduler control global variables==========//
    // scheduler control.
    scheduler: null,
    // Array of deleted scheduled works,used to delete server side data.
    deletedScheduledWorks: [],
    
    //=====================Rate type grid control global variables================//
    // selected rate type to assign.
    selectedRateType: null,
    // rate type grid control.
    rateTypeGridControl: null,


    afterViewLoad: function() {
        var locale = View.user.locale.slice(0, 2);
        DevExpress.localization.locale(locale);
        this.on('ab:bldgops:labor:schedule:setTotalHoursForRateTypeList',this.setTotalHoursForRateTypeList);
        this.selectedRateType = this.defaultSelectedRateType;
        // Initialized schedule control.
        this.scheduler = View.createControl({
            control: 'DailyScheduleControl',
            container: 'scheduler',
            panel: 'workSchedule',
            openerController: this,
            scheduledWorkDataSource: 'cfSchedulesDaysDs',
            listeners: {

            }
        });
    },

    afterInitialDataFetch: function() {
        // get selected craftsperson.
        // register current control
        var openerView = View.getOpenerView();
        if (openerView) {
            this.registerTabCtrl(openerView);
            var openerController = openerView.controllers.items[0];
            this.selectedCfId = openerController.selectedCfId;
            //initial the customized start date and end date fields items.
            this.initialDateControlItems();
            this.refreshViewPanels();
        }
        
       
    },

    /**
     * Register current control to opener view controller.
     * @param{openerView} opener view.
     */
    registerTabCtrl: function(openerView) {
        var openerCtrl = openerView.controllers.items[0];
        openerCtrl.tabCtrls['dailyScheduleTab'] = this;
    },

    /**
     * Event handler after select craftsperson .this method is called by opener view controller.
     * @param{cfId} selected craftsperson code.
     */
    afterSelectCf: function(cfId) {
        this.selectedCfId = cfId;
        this.refreshViewPanels();
        
    },

    /**
     * Refresh view panels after select craftsperson.
     */
    refreshViewPanels: function() {
        this.scheduleListPanel.addParameter('cfId', this.selectedCfId);
        this.scheduleListPanel.refresh();
        
        var currentDateAppliedScheduleAssignId=getCurrentApplyScheduleAssignedIdForCf('cfScheduleAssignWithoutPramDs',this.selectedCfId);
        if(valueExistsNotEmpty(currentDateAppliedScheduleAssignId)){
            //show the existing scheudle detail.
            var record=this.getAssignedRecord(currentDateAppliedScheduleAssignId);
            this.refreshPanelsByAssignmentRecord(record);
        }else{
            //switch to add new mode.
            this.refreshPanelsByAssignmentRecord(null);
        }
    },
    
    /**
     * replace date display value like 'July - 6' for seasonal schedule rows.
     */
    scheduleListPanel_afterGetData: function(panel, dataSet){
        // format schedule assigned date for seasonal scheudle.
        var gridRows=this.scheduleListPanel.gridRows.items;
        var ds=this.scheduleListPanel.getDataSource();
        _.each(dataSet.records,function(record){
            var isSeasonal=record['cf_schedules_assign.is_seasonal.raw'];
            
            if(isSeasonal=='1'){
                var dateStart=ds.parseValue('cf_schedules_assign.date_start',record['cf_schedules_assign.date_start.raw'],false);
                var dateEnd=ds.parseValue('cf_schedules_assign.date_end',record['cf_schedules_assign.date_end.raw'],false);
                if(valueExistsNotEmpty(dateStart)){
                    var displayValue='';
                    var startMonth=dateStart.getMonth();
                    var startDay=dateStart.getDate();
                    var monthName=getMonthName(startMonth);
                    displayValue=monthName+' '+startDay;
                    record['cf_schedules_assign.date_start']=displayValue;
                }
                
                if(valueExistsNotEmpty(dateEnd)){
                    var displayValue='';
                    var EndMonth=dateEnd.getMonth();
                    var EndDay=dateEnd.getDate();
                    var monthName=getMonthName(EndMonth);
                    displayValue=monthName+' '+EndDay;
                    record['cf_schedules_assign.date_end']=displayValue;
                }
            }
        });
    },
    
    /**
     * Initial month select fields of customized start date and end date field.
     */
    initialDateControlItems: function(){
        var startMonthControl=jQuery($('start_day.month'));
        var endMonthControl=jQuery($('end_day.month'));
        var monthItems=getMonthItems();
        jQuery('<option>').attr('value','').text('').appendTo(startMonthControl);
        jQuery('<option>').attr('value','').text('').appendTo(endMonthControl)
        _.each(monthItems,function(monthObject){
            jQuery('<option>').attr('value',monthObject.key).text(monthObject.value).appendTo(startMonthControl);
            jQuery('<option>').attr('value',monthObject.key).text(monthObject.value).appendTo(endMonthControl);
        });
    },

    /**
     * Schedule list item click event handler.
     */
    onClickCfSchedule: function() {
        var selectedRowIndex = this.scheduleListPanel.selectedRowIndex;
        var assignRecord = this.scheduleListPanel.gridRows.get(selectedRowIndex).getRecord();
        this.addNewMode=false;
        
        this.refreshPanelsByAssignmentRecord(assignRecord);
    },
    
    /**
     * Refresh panels by selected assignment record.
     */
    refreshPanelsByAssignmentRecord: function(assignRecord){
        if(assignRecord){
            this.addNewMode=false;
            var scheduleId=assignRecord.getValue('cf_schedules.cf_schedule_id');
        }else{
            this.addNewMode=true;
            this.switchDateFieldsControlByIsSeasonalValue('0');
        }
        
        //set global variables by selected assign record.
        this.setGolbalVariables(assignRecord);
        
        //refresh schedule details panel.
        this.cfScheduleDetailPanel.show(true);
        this.refreshCraftspersonScheduleDetailPanel();
        
        //refresh rate type list panel.
        this.rateTypeListPanel.show(true);
        this.rateTypeGridControl = this.createRateTypeListControl();
        this.refreshRateTypeGrid();
        
        //show and refresh scheduler control.
        this.scheduleControlPanel.show(true);
        this.loadCalendarControl(assignRecord);
    },
    
    /**
     * Set global variables by selected assignment record.
     */
    setGolbalVariables: function(assignRecord){
        if(assignRecord){
            this.selectedAssignRecord=assignRecord;
            var scheduleId=assignRecord.getValue('cf_schedules_assign.cf_schedule_id');
            var scheduleType=assignRecord.getValue('cf_schedules.schedule_type');
            this.selectedScheduleType=scheduleType;
            if(scheduleType=='SHIFT DEFINITION'){
                this.selectedShift=scheduleId;
            }else{
                this.selectedShift=null;
            }
            this.selectedRateType=this.defaultSelectedRateType;
        }else{
            // initial schedule detail form
            this.selectedAssignRecord=null;
            this.selectedShift=null;
            this.selectedScheduleType=null;
            this.deletedScheduledWorks.length=0;
        }
    },
    
    /**
     * Refresh craftsperson schedule details panel.
     */
    refreshCraftspersonScheduleDetailPanel: function(){
        var assignmentRecord=this.selectedAssignRecord;
        $('selectShiftButton').value=getMessage('selectShiftButtonText');
        if(assignmentRecord){
            //update form fields.
            this.cfScheduleDetailPanel.actions.get('delete').forceDisable(false);
            this.updateScheduleDetailFormFields(this.selectedAssignRecord);
        }else{
            //clear form fields.
            this.cfScheduleDetailPanel.actions.get('delete').forceDisable(true);
            this.clearScheduleDetailsFormFields();
        }
    },
    
    /**
     * Clear craftsperson schedule detail form.
     */
    clearScheduleDetailsFormFields: function(){
        this.onChangeScheduleType("CUSTOM SCHEDULE");
        this.cfScheduleDetailPanel.setFieldValue('cf_schedules_assign.cf_schedule_id', '');
        this.cfScheduleDetailPanel.setFieldValue('cf_schedules.schedule_name', '');
        this.cfScheduleDetailPanel.setFieldValue('cf_schedules_assign.date_end', '');
        this.cfScheduleDetailPanel.setFieldValue('cf_schedules_assign.date_start', '');
        $('isSesonalCbx').checked = false;
    },
    
    /**
     * Update schedule detail form fields.
     */
    updateScheduleDetailFormFields: function(assignmentRecord){
        var scheduleId = assignmentRecord.getValue('cf_schedules_assign.cf_schedule_id');
        var scheduleName = assignmentRecord.getValue('cf_schedules.schedule_name');
        var dateStart=assignmentRecord.getValue('cf_schedules_assign.date_start');
        if(valueExistsNotEmpty(dateStart)){
            dateStart=this.cfSchedulesAssignDs.formatValue('cf_schedules_assign.date_start', dateStart,true);
        }
        var dateEnd = assignmentRecord.getValue('cf_schedules_assign.date_end');
        if(valueExistsNotEmpty(dateEnd)){
            dateEnd=this.cfSchedulesAssignDs.formatValue('cf_schedules_assign.date_end', dateEnd,true);
        }
        var isSeasonal = assignmentRecord.getValue('cf_schedules_assign.is_seasonal');
        var scheduleType = assignmentRecord.getValue('cf_schedules.schedule_type');

        this.cfScheduleDetailPanel.setFieldValue('cf_schedules.schedule_name', scheduleName);
        this.cfScheduleDetailPanel.setFieldValue('cf_schedules_assign.date_start', dateStart);
        this.cfScheduleDetailPanel.setFieldValue('cf_schedules_assign.date_end', dateEnd);
        
        if (isSeasonal == "1") {
            $('isSesonalCbx').checked = true;
        } else {
            $('isSesonalCbx').checked = false;
        }
        
        // set schedule type fields checked or not.
        var radioButtons = document.getElementsByName('scheduleType');
        for (var i = 0; i < radioButtons.length; i++) {
            if (radioButtons[i].value == scheduleType) {
                radioButtons[i].checked = true;
            }
        }
        this.enableFormFieldsByScheduleType(scheduleType);
        
        //swith field controls to display start and end date.
        this.switchAndSetDateFieldControls(isSeasonal,assignmentRecord.getValue('cf_schedules_assign.date_start'),assignmentRecord.getValue('cf_schedules_assign.date_end'));
        
    },
    
    /**
     * Switch date field control by is seasonal value.
     */
    switchDateFieldsControlByIsSeasonalValue: function(isSeasonal){
        if(isSeasonal=='1'){
            this.cfScheduleDetailPanel.showField('cf_schedules_assign.date_start',false);
            this.cfScheduleDetailPanel.showField('cf_schedules_assign.date_end',false);
            Ext.get('cfScheduleDetailPanel_cf_schedules_assign.startDayControlIgoreYear_labelCell').show();
            Ext.get('cfScheduleDetailPanel_cf_schedules_assign.startDayControlIgoreYear_fieldCell').show();
            Ext.get('cfScheduleDetailPanel_cf_schedules_assign.endDayControlIgnoreYear_labelCell').show();
            Ext.get('cfScheduleDetailPanel_cf_schedules_assign.endDayControlIgnoreYear_fieldCell').show();
        }else{
            this.cfScheduleDetailPanel.showField('cf_schedules_assign.date_start',true);
            this.cfScheduleDetailPanel.showField('cf_schedules_assign.date_end',true);
            Ext.get('cfScheduleDetailPanel_cf_schedules_assign.startDayControlIgoreYear_labelCell').hide();
            Ext.get('cfScheduleDetailPanel_cf_schedules_assign.startDayControlIgoreYear_fieldCell').hide();
            Ext.get('cfScheduleDetailPanel_cf_schedules_assign.endDayControlIgnoreYear_labelCell').hide();
            Ext.get('cfScheduleDetailPanel_cf_schedules_assign.endDayControlIgnoreYear_fieldCell').hide();
        }
    },
    
    /**
     * Switch and set start date and end date field controls.
     */
    switchAndSetDateFieldControls: function(isSeasonal,dateStart,dateEnd){
        this.switchDateFieldsControlByIsSeasonalValue(isSeasonal);
        var startMonth='';
        var startDate='';
        if(valueExistsNotEmpty(dateStart)){
            startMonth=valueExistsNotEmpty(dateStart)?dateStart.getMonth():'';
            startDate=valueExistsNotEmpty(dateStart)?dateStart.getDate():'';
        }
        var endMonth='';
        var endDate='';
        if(valueExistsNotEmpty(dateEnd)){
            endMonth=valueExistsNotEmpty(dateEnd)?dateEnd.getMonth():'';
            endDate=valueExistsNotEmpty(dateEnd)?dateEnd.getDate():'';
        }
        
        this.setMonthValue('start_day.month',startMonth);
        this.setDayValue('start_day.day',startDate);
        this.setMonthValue('end_day.month',endMonth);
        this.setDayValue('end_day.day',endDate);
    },
    
    /**
     * Set month value.
     */
    setMonthValue: function(monthFieldName,month){
        $(monthFieldName).value=month;
        var dayFieldName='';
        if(monthFieldName=='start_day.month'){
            dayFieldName='start_day.day';
        }
        
        if(monthFieldName=='end_day.month'){
            dayFieldName='end_day.day';
        }
        
        if(valueExistsNotEmpty(dayFieldName)){
            this.initialDaysItems(dayFieldName,month);
        }
    },
    
    /**
     * Initial days item based on month value.
     */
    initialDaysItems: function(dayFieldName,month){
        $(dayFieldName).innerHTML='';
        var daysControl=jQuery($(dayFieldName));
        jQuery('<option>').attr('value','').text('').appendTo(daysControl);
        if(valueExistsNotEmpty(month)){
            var days=getDaysByMonth(month);
            _.each(days,function(dayObject){
                jQuery('<option>').attr('value',dayObject.key).text(dayObject.value).appendTo(daysControl);
            });
        }
    },
    
    /**
     * Set days value.
     */
    setDayValue: function(dayFieldName,value){
        $(dayFieldName).value=value;
    },

    /**
     * Set form fields enable or not based on schedule type.
     * @param {scheduleType} selected schedule type.
     */
    enableFormFieldsByScheduleType: function(scheduleType) {
        var formPanel = this.cfScheduleDetailPanel;

        if (scheduleType == 'SHIFT DEFINITION') {
            $('selectShiftButton').removeAttribute('disabled');
            //formPanel.setFieldValue('cf_schedules.schedule_name', "");
            formPanel.enableField('cf_schedules.schedule_name', false);
        }
        if (scheduleType == 'CUSTOM SCHEDULE') {
            //this.selectedShiftDefinition = null;
            $('selectShiftButton').setAttribute('disabled', true);
            formPanel.enableField('cf_schedules.schedule_name', true);
        }
    },
    
    /**
     * Create rate type list control.
     */
    createRateTypeListControl: function() {
        // create rate type grid control.
        var grid = createRateTypeGridListControl('rateTypeListGrid_control', 'cfDailySchedule', this);
        grid.showColumn('vfRateType',false);
        grid.build();
        return grid;
    },

    /**
     * Refresh rate type grid
     */
    refreshRateTypeGrid: function() {
        this.rateTypeListPanel.show(true);
        this.setFieldValuesToGridRows(this.rateTypeGridControl);
    },
    
    /**
     * Set total hours for rate type list after schedule control content ready.
     */
    setTotalHoursForRateTypeList: function() {
        var me = this;
        if (valueExists(this.scheduler) && valueExists(this.rateTypeGridControl)) {
            var gridRowsItems = this.rateTypeGridControl.gridRows.items;
            _.each(gridRowsItems, function(row) {
                me.getTotalWeeklyHours(row);
            });
        }
    },
    
    /**
     * Get total weekly hours.
     * @param {row} grid row element
     */
    getTotalWeeklyHours: function(row) {
        var me = this;
        var rowRateType = row.record['vfRateType'];
        var scheduledWorks = this.scheduler.scheduledWork;
        var totalHour = 0;
        _.each(scheduledWorks, function(appointmentData) {
            var dateStart = appointmentData.startDate;
            var dateEnd = appointmentData.endDate;
            var rateType = appointmentData.rateType;
            if (rateType == rowRateType) {
                var hour = Math.abs(dateEnd - dateStart) / (1000 * 60 * 60);
                totalHour += hour;
            }
        });
        row.cells.get('vfTotalWeeklyHours').dom.innerHTML = '' + totalHour.toFixed(2);
    },
    
    /**
     * On click Assign button.
     */
    onClickAssignButton: function(row) {
        var me = this;
        var rateType = row['vfRateType'];
        // reset global parameter.
        this.selectedRateType = rateType;
        // reset the background color of assign button
        var gridRowsItems = this.rateTypeGridControl.gridRows.items;
        _.each(gridRowsItems, function(row) {
            me.setAssignButtonsBgColor(row, me.selectedRateType);
        });
    },
    
    /**
     * Load scheduler control by selected assignment record.
     */
    loadCalendarControl: function(assignmentRecord){
        var canEdit=true;
        if(assignmentRecord){
            var scheduleId=assignmentRecord.getValue('cf_schedules_assign.cf_schedule_id');
            var scheduleType=assignmentRecord.getValue('cf_schedules.schedule_type');
            canEdit=scheduleType=='CUSTOM SCHEDULE'?true:false;
            this.loadScheduleWorksByScheduleId(scheduleId);
        }else{
            this.clearCalendarControl();
        }
        
        //enable or disable calendar control base on the scheduleType.
        this.scheduler.enableScheduleControl(canEdit);
    },

    /**
     * Load schedule control.
     */
    loadScheduleWorksByScheduleId: function(scheduleId, scheduleType) {
        var res = new Ab.view.Restriction();
            res.addClause('cf_schedules_days.cf_schedule_id', scheduleId, '=');

        this.scheduler.loadScheduledWork(res);
        this.deletedScheduledWorks.length = 0;
    },
    
    /**
     * Load a blank schedule control to clear the scheduler control.
     */
    clearCalendarControl: function(){
        this.scheduler.loadBlankScheduleControl();
        this.deletedScheduledWorks.length = 0;
    },

    /**
     * TODO: move this save logic to JAVA file. Save schedule detail.
     */
    cfScheduleDetailPanel_onSave: function() {
        var me = this;
        
        if (!this.validateScheduleDetail()) {
            return;
        }
        
        var scheduleDs = this.cfSchedulesDs;
        var scheduleDaysDs = this.cfSchedulesDaysDs;
        var cfId = this.selectedCfId;
        var scheduleId;
        var assignId;
        var scheduleName = this.cfScheduleDetailPanel.getFieldValue('cf_schedules.schedule_name');
        var dateStart = this.cfScheduleDetailPanel.getFieldValue('cf_schedules_assign.date_start');
        var dateEnd = this.cfScheduleDetailPanel.getFieldValue('cf_schedules_assign.date_end');
        var isSeasonal = $('isSesonalCbx').checked == true ? '1' : '0';
        var scheduleType = this.selectedScheduleType;
        var scheduledWorks = this.scheduler.scheduledWork;
        var originalScheduleType=null;

        View.openProgressBar();
        // insert or update schedule.
        if (this.addNewMode) {
            if (this.selectedScheduleType == 'CUSTOM SCHEDULE') {
                // insert new schedule.
                scheduleId=this.addNewSchedule(scheduleName,scheduleType);
            }
            if (this.selectedScheduleType == 'SHIFT DEFINITION') {
                scheduleId = this.selectedShift;
            }
            // insert new schedule assign record.
            assignId=this.addNewScheduleAssign(scheduleId,cfId,dateStart,dateEnd,isSeasonal);
            
        } else {
            //get original value from global variables.
            originalScheduleType=this.selectedAssignRecord.getValue('cf_schedules.schedule_type');
            scheduleId = this.selectedAssignRecord.getValue('cf_schedules_assign.cf_schedule_id');
            assignId = this.selectedAssignRecord.getValue('cf_schedules_assign.auto_number');
            //cache the schedule that be replaced.
            var changedScheduleId=null;
            
            if(this.selectedScheduleType == 'CUSTOM SCHEDULE'){
                if(originalScheduleType == 'SHIFT DEFINITION'||valueExistsNotEmpty(this.selectedShift)){
                    changedScheduleId=scheduleId;
                    scheduleId=this.addNewSchedule(scheduleName,this.selectedScheduleType);
                }else{
                    this.updateSchedule(scheduleId,scheduleName,scheduleType);
                }
            }else{
                scheduleId=this.selectedShift;
            }
            // update schedule assign record.
            this.updateScheduleAssign(assignId,scheduleId,dateStart,dateEnd,isSeasonal);
            //delete days and schedule if the custom schedule has been replace by a new schedule.
            if(valueExistsNotEmpty(changedScheduleId) && originalScheduleType!=='SHIFT DEFINITION'){
                this.deleleChangedCustomSchedule(changedScheduleId);
            }
        }
        
        //handle all scheduled timeblocks.
        if(me.selectedScheduleType == 'CUSTOM SCHEDULE'){
            // handler all time blocks
            _.each(scheduledWorks, function(appointmentData) {
                var serverId = appointmentData.serverId;
                // add new record if convert shift reference to custom schedule.
                if(me.addNewMode || valueExistsNotEmpty(changedScheduleId)){
                    serverId = "";
                    me.deletedScheduledWorks.length=0;
                }
                if (valueExistsNotEmpty(serverId)) {
                    me.updateScheduleDays(appointmentData);
                } else {
                    me.addNewScheduleDays(scheduleId,appointmentData);
                }
            });
            // delete all scheduled works that marked as delete from server side .
            for (var i = 0; i < this.deletedScheduledWorks.length; i++) {
                var serverId = this.deletedScheduledWorks[i];
                var res = new Ab.view.Restriction();
                res.addClause('cf_schedules_days.auto_number', serverId, '=');
                var deleteRecord = scheduleDaysDs.getRecord(res);
                scheduleDaysDs.deleteRecord(deleteRecord);
            }
        }
        
        if(me.selectedScheduleType == 'CUSTOM SCHEDULE'){
            this.selectedShift=null;
        }
        //refresh panels after saved.
        this.afterScheduleDetailFormSaved(assignId);
        View.closeProgressBar();
    },
    
    /**
     * Set date values based on Is Seasonal field value.
     */
    setDateValuesByIsSeasonal: function(isSeasonal){
        if(isSeasonal=='1'){
            var currentDate=new Date();
            var startMonth=$('start_day.month').value;
                startMonth=valueExistsNotEmpty(startMonth)?parseInt(startMonth):'';
            var startDate=$('start_day.day').value;
                startDate=valueExistsNotEmpty(startDate)?parseInt(startDate):'';
            var endMonth=$('end_day.month').value;
                endMonth=valueExistsNotEmpty(endMonth)?parseInt(endMonth):'';
            var endDate=$('end_day.day').value;
                endDate=valueExistsNotEmpty(endDate)?parseInt(endDate):'';
            
            if(valueExistsNotEmpty(startMonth)){
                if(valueExistsNotEmpty(startDate)){
                    var newStartDate=new Date(currentDate.getFullYear(),startMonth,startDate);
                    var formattedStartDate=this.cfScheduleDetailPanel.getDataSource().formatValue('cf_schedules_assign.date_start',newStartDate);
                    this.cfScheduleDetailPanel.setFieldValue('cf_schedules_assign.date_start',formattedStartDate);
                }
            }else{
                this.cfScheduleDetailPanel.setFieldValue('cf_schedules_assign.date_start','');
            }
            
            if(valueExistsNotEmpty(endMonth)){
                if(valueExistsNotEmpty(endDate)){
                    var year=currentDate.getFullYear();
                    if(endMonth<startMonth||(endMonth==startMonth&&endDate<startDate)){
                        year+=1;
                    }
                    var newEndDate=new Date(year,endMonth,endDate);
                    var formattedEndDate=this.cfScheduleDetailPanel.getDataSource().formatValue('cf_schedules_assign.date_end',newEndDate);
                    this.cfScheduleDetailPanel.setFieldValue('cf_schedules_assign.date_end',formattedEndDate);
                }
            }else{
                this.cfScheduleDetailPanel.setFieldValue('cf_schedules_assign.date_end','');
            }
            
        }
    },
    
    /**
     * Refresh panels after schedule detail form saved.
     * @param {assignId} schedule assign primary key. cf_schedules_assign.auto_number.
     */
    afterScheduleDetailFormSaved: function(assignId){
        // refresh schedule list panel.
        this.scheduleListPanel.addParameter('cfId', this.selectedCfId);
        this.scheduleListPanel.refresh();
        //refresh other panels
        var record=this.getAssignedRecord(assignId);
        this.refreshPanelsByAssignmentRecord(record);
    },
    
    /**
     * Get scheduled assign record by specified primary key.
     */
    getAssignedRecord: function(assignId){
        var res=new Ab.view.Restriction();
            res.addClause('cf_schedules_assign.auto_number',assignId,'=');
        var records=this.cfScheduleAssignWithoutPramDs.getRecords(res);
        if(records.length>0){
            return records[0];
        }else{
            return null;
        }
    },
    
    /**
     * Validate schedule form and records.
     */
    validateScheduleDetail: function() {
        var isValidate = true;
        var scheduleName = this.cfScheduleDetailPanel.getFieldValue('cf_schedules.schedule_name');
        var selectedScheduleType = this.selectedScheduleType;
        if ((!valueExistsNotEmpty(this.selectedShift)) && (selectedScheduleType == 'SHIFT DEFINITION')) {
            View.alert(getMessage('selectShiftMessage'));
            isValidate = false;
            return;
        }

        if (this.scheduler.scheduledWork.length < 1) {
            View.alert(getMessage('createTimeBlocksMessage'));
            isValidate = false;
            return;
        }
        var newIsSeasonal = $('isSesonalCbx').checked == true ? '1' : '0';
        //convert start date and end date by using different date control based on the Is Seasonal field value.
        this.setDateValuesByIsSeasonal(newIsSeasonal);
        
        // check date fields in form fields
        var newDateStart = this.cfScheduleDetailPanel.getFieldValue('cf_schedules_assign.date_start');
        var newDateEnd = this.cfScheduleDetailPanel.getFieldValue('cf_schedules_assign.date_end');
        
        
        
        if (valueExistsNotEmpty(newDateStart)) {
            newDateStart = new Date(newDateStart);
            newDateStart.setHours(0, 0, 0, 0);
        }
        if (valueExistsNotEmpty(newDateEnd)) {
            newDateEnd = new Date(newDateEnd);
            newDateEnd.setHours(0, 0, 0, 0);
        }
        // check date fields in form.
        var dateFieldsCheckResult = checkDateFieldsBeforeSave(newDateStart, newDateEnd, newIsSeasonal);
        if (!dateFieldsCheckResult) {
            isValidate = false;
            return;
        }

        // date range can not overlap with other schedule.
        var selectedAssignedId='';
        if(valueExistsNotEmpty(this.selectedAssignRecord)){
            selectedAssignedId = this.selectedAssignRecord.getValue('cf_schedules_assign.auto_number');
        }
        
        var cfId = this.selectedCfId;
        var cfRes=new Ab.view.Restriction();
            cfRes.addClause('cf_schedules_assign.cf_id',cfId,'=');
        var cfAssignedRecords=this.cfScheduleAssignWithoutPramDs.getRecords(cfRes);

        var isOverlappedPassed = validateScheduleDateRangeOverlap(cfAssignedRecords, selectedAssignedId, newDateStart, newDateEnd, newIsSeasonal);

        if (!isOverlappedPassed) {
            View.alert(getMessage('schedulesCannotOverlapMessage'));
            isValidate = false;
            return;
        }

        return isValidate
    },
    
    /**
     * Delete schedule.
     */
    cfScheduleDetailPanel_onDelete: function() {
        var me = this;
        View.confirm(getMessage('deleteScheduleMessage'), function(button) {
            if (button == 'yes') {
                var assignedId=me.selectedAssignRecord.getValue('cf_schedules_assign.auto_number');
                var scheduleId=me.selectedAssignRecord.getValue('cf_schedules_assign.cf_schedule_id');
                var scheduleType=me.selectedAssignRecord.getValue('cf_schedules.schedule_type');
                // delete all schedule days if schedule is not shift definition.
                if (scheduleType == 'CUSTOM SCHEDULE') {
                    me.deleteScheduleDays(scheduleId);
                }

                // delete all assignments of this selected schedule.
                me.deleteScheduleAssignRecord(assignedId);

                // delete schedule if this schedule doesn't have any other assignments items.
                if (scheduleType == 'CUSTOM SCHEDULE') {
                    var res = new Ab.view.Restriction();
                        res.addClause('cf_schedules_assign.cf_schedule_id', scheduleId, '=');
                    var assignRecords = View.dataSources.get('cfScheduleAssignWithoutPramDs').getRecords(res);
                    if (assignRecords.length < 1) {
                        // delete this schedule.
                        me.deleteSchedule(scheduleId);
                    }
                }
                // change to add new mode.
                me.refreshPanelsByAssignmentRecord(null);
                // refresh schedule list panel
                View.panels.get('scheduleListPanel').addParameter('cfId', me.selectedCfId);
                View.panels.get('scheduleListPanel').refresh();
            }
        });
    },

    /**
     * Cancel schedule.
     */
    cfScheduleDetailPanel_onCancel: function() {
        // hide other panel.
        View.panels.get('cfScheduleDetailPanel').show(false);
        View.panels.get('rateTypeListPanel').show(false);
        View.panels.get('scheduleControlPanel').show(false);
    },

    /**
     * Update Schedule.
     */
    updateSchedule: function(scheduleId,scheduleName,scheduleType){
        // update schedule.
        var scheduleRes = new Ab.view.Restriction();
        scheduleRes.addClause('cf_schedules.cf_schedule_id', scheduleId, '=');
        var scheduleRecord = this.cfSchedulesDs.getRecord(scheduleRes);
        scheduleRecord.setValue('cf_schedules.schedule_name', scheduleName);
        scheduleRecord.setValue('cf_schedules.schedule_type', scheduleType);
        this.cfSchedulesDs.saveRecord(scheduleRecord);
    },
    
    /**
     * Update schedule assign.
     */
    updateScheduleAssign: function(assignId,scheduleId,dateStart,dateEnd,isSeasonal){
        var scheduleAssignRes = new Ab.view.Restriction();
            scheduleAssignRes.addClause('cf_schedules_assign.auto_number', assignId, '=');
        var scheduleAssignRecord = this.cfScheduleAssignWithoutPramDs.getRecord(scheduleAssignRes);
            scheduleAssignRecord.setValue('cf_schedules_assign.cf_schedule_id', scheduleId);
            scheduleAssignRecord.setValue('cf_schedules_assign.date_start', dateStart);
            scheduleAssignRecord.setValue('cf_schedules_assign.date_end', dateEnd);
            scheduleAssignRecord.setValue('cf_schedules_assign.is_seasonal', isSeasonal);
        this.cfScheduleAssignWithoutPramDs.saveRecord(scheduleAssignRecord);
    },
    
    /**
     * Deleted custom schedule and it's days that have been changed.
     */
    deleleChangedCustomSchedule: function(changedScheuleId){
        if(valueExistsNotEmpty(changedScheuleId)){
            //delete original custom schedule with its items.
            this.deleteScheduleDays(changedScheuleId);
            this.deleteSchedule(changedScheuleId);
        }
    },
    
    /**
     * Update schedule Days.
     */
    updateScheduleDays: function(appointmentData){
        var serverId = appointmentData.serverId;
        var timeStart = appointmentData.startDate;
        var timeEnd = appointmentData.endDate;
        var rateType = appointmentData.rateType;
        var dayOfWeek = getDayOfWeekByNumber(timeStart.getDay());
        // Update the existing scheduled work
        var scheduleDaysDs = this.cfSchedulesDaysDs;
        var res = new Ab.view.Restriction();
        res.addClause('cf_schedules_days.auto_number', serverId, '=');
        var record = scheduleDaysDs.getRecord(res);
        if (!record.isNew) {
            record.setValue('cf_schedules_days.time_start', timeStart);
            record.setValue('cf_schedules_days.time_end', timeEnd);
            record.setValue('cf_schedules_days.day_of_week', dayOfWeek);
            record.setValue('cf_schedules_days.rate_type', rateType);
            scheduleDaysDs.saveRecord(record);
        }
    },
    
    /**
     * Add new schedule days.
     */
    addNewScheduleDays: function(scheduleId,appointmentData){
        var timeStart = appointmentData.startDate;
        var timeEnd = appointmentData.endDate;
        var rateType = appointmentData.rateType;
        var dayOfWeek = getDayOfWeekByNumber(timeStart.getDay());
        // Insert new scheduled work.
        var scheduleDaysDs = this.cfSchedulesDaysDs;
        var record = new Ab.data.Record();
        record.isNew = true;
        record.oldValues = new Object();
        record.setValue('cf_schedules_days.cf_schedule_id', scheduleId);
        record.setValue('cf_schedules_days.day_of_week', dayOfWeek);
        record.setValue('cf_schedules_days.rate_type', rateType);
        record.setValue('cf_schedules_days.time_start', timeStart);
        record.setValue('cf_schedules_days.time_end', timeEnd);
        scheduleDaysDs.saveRecord(record);
    },
    
    /**
     * Add new schedule.
     * 
     * @param scheduleName Schedule Name.
     * @param scheduleType Schedule Type.
     */
    addNewSchedule: function(scheduleName,scheduleType){
        var scheduleRecord = new Ab.data.Record();
        scheduleRecord.isNew = true;
        scheduleRecord.setValue('cf_schedules.schedule_name', scheduleName);
        scheduleRecord.setValue('cf_schedules.schedule_type', 'SHIFT DEFINITION');
        var resultRecord = this.cfSchedulesDs.saveRecord(scheduleRecord);
        var scheduleId=  resultRecord.getValue('cf_schedules.cf_schedule_id');
        //APP-1757: update schedule_type to workaround that ORACLE and MS-SQL error when scheduleName is empty.
        var res=new Ab.view.Restriction();
            res.addClause('cf_schedules.cf_schedule_id',scheduleId,'=');
        var record = this.cfSchedulesDs.getRecord(res);
            record.setValue('cf_schedules.schedule_type',scheduleType);
        this.cfSchedulesDs.saveRecord(record);
        return scheduleId;
    },
    
    /**
     * Add new schedule assign record.
     */
    addNewScheduleAssign: function(scheduleId,cfId,dateStart,dateEnd,isSeasonal){
        var scheduleAssignRecord = new Ab.data.Record();
        scheduleAssignRecord.isNew = true;
        scheduleAssignRecord.oldValues = new Object();
        scheduleAssignRecord.setValue('cf_schedules_assign.cf_schedule_id', scheduleId);
        scheduleAssignRecord.setValue('cf_schedules_assign.cf_id', cfId)
        scheduleAssignRecord.setValue('cf_schedules_assign.date_start', dateStart);
        scheduleAssignRecord.setValue('cf_schedules_assign.date_end', dateEnd);
        scheduleAssignRecord.setValue('cf_schedules_assign.is_seasonal', isSeasonal);
        var savedAssignRecord = this.cfScheduleAssignWithoutPramDs.saveRecord(scheduleAssignRecord);
        var assignId = savedAssignRecord.getValue('cf_schedules_assign.auto_number');
        
        return assignId;
    },
    
    /**
     * Delete specified schedule.
     * @param {scheduleId} Craftsperson Schedule Code.
     */
    deleteSchedule: function(scheduleId){
        var restriction=new Ab.view.Restriction();
        restriction.addClause('cf_schedules.cf_schedule_id',scheduleId,'=');
        var scheduleRecord=this.cfSchedulesDs.getRecord(restriction);
        this.cfSchedulesDs.deleteRecord(scheduleRecord); 
    },
    
    /**
     * Delete specified schedule assigns records.
     */
    deleteScheduleAssigns: function(scheduleId){
        var me=this;
        var restriction=new Ab.view.Restriction();
            restriction.addClause('cf_schedules_assign.cf_schedule_id',scheduleId,'=');
        var scheduleAssignRecords=me.cfScheduleAssignWithoutPramDs.getRecords(restriction);
        _.each(scheduleAssignRecords,function(record){
            me.cfScheduleAssignWithoutPramDs.deleteRecord(record); 
        });
    },
    
    /**
     * Delete specified schedule assigns records.
     */
    deleteScheduleAssignRecord: function(autoNumber){
        var me=this;
        var restriction=new Ab.view.Restriction();
            restriction.addClause('cf_schedules_assign.auto_number',autoNumber,'=');
        var scheduleAssignRecords=me.cfScheduleAssignWithoutPramDs.getRecords(restriction);
        _.each(scheduleAssignRecords,function(record){
            me.cfScheduleAssignWithoutPramDs.deleteRecord(record); 
        });
    },
    
    /**
     * Delete specified schedule days records.
     */
    deleteScheduleDays: function(scheduleId){
        var me=this;
        var restriction=new Ab.view.Restriction();
            restriction.addClause('cf_schedules_days.cf_schedule_id',scheduleId,'=');
        var scheduleDaysRecords=me.cfSchedulesDaysDs.getRecords(restriction);
        _.each(scheduleDaysRecords,function(record){
            me.cfSchedulesDaysDs.deleteRecord(record); 
        });
    },

    /**
     * Select shift defination from select shifts pop-up.
     */
    onSelectShiftDefination: function() {
        var selectedRowIndex = this.shiftsDefinationPanel.selectedRowIndex;
        var selectedRowRecord = this.shiftsDefinationPanel.gridRows.get(selectedRowIndex).getRecord();
        var scheduleId = selectedRowRecord.getValue('cf_schedules.cf_schedule_id');
        this.selectedShift = scheduleId;
        
        var scheduleName = selectedRowRecord.getValue('cf_schedules.schedule_name');
        // Re-set shift Name value , and clear other form fields.
        this.cfScheduleDetailPanel.setFieldValue('cf_schedules.schedule_name', scheduleName);
        // Clear the schedule control and re-load the shift define records on schedule control.
        
        //load schedule by schedule id.
        this.loadScheduleWorksByScheduleId(scheduleId);

        this.shiftsDefinationPanel.closeWindow();
    },

    /**
     * On Click add new button to add new craftsperson schedule.
     */
    scheduleListPanel_onAddNew: function() {
        this.refreshPanelsByAssignmentRecord(null)
    },

    /**
     * Do refresh operation after change the rate type.
     */
    onChangeScheduleType: function(newScheduleType) {
        this.selectedScheduleType = newScheduleType;
        var radioButtons = document.getElementsByName('scheduleType');
        for (var i = 0; i < radioButtons.length; i++) {
            if (radioButtons[i].value == this.selectedScheduleType) {
                radioButtons[i].checked = true;
            }
        }
        this.enableFormFieldsByScheduleType(this.selectedScheduleType);
        this.enableScheduleControlByScheduleType(this.selectedScheduleType);
    },
    
    /**
     * Enable schedule control editable by schedule type.
     * @param{scheduleType|string} Schedule Type.
     */
    enableScheduleControlByScheduleType: function(scheduleType) {
        if (scheduleType == 'SHIFT DEFINITION') {
            this.scheduler.enableScheduleControl(false);
        }
        if (scheduleType == 'CUSTOM SCHEDULE') {
            this.scheduler.enableScheduleControl(true);
        }
    },

    

    /**
     * Set legend color and assign button background color after grid build.
     */
    setFieldValuesToGridRows: function(grid) {
        var me = this;
        var gridRowsItems = grid.gridRows.items;
        _.each(gridRowsItems, function(row) {
            me.setCfRate(row, me.selectedCfId);
            // set background color for legend field
            me.setRateTypeLegendColor(row);
            // set default background color of selected rate type.
            me.setAssignButtonsBgColor(row, me.selectedRateType);
        });
    },

    /**
     * Set rate for rate type list of selected craftsperson.
     * @param {row} Grid row object.
     * @param {cfId} Select craftsperson code.
     */
    setCfRate: function(row, cfId) {
        var rateType = row.record['vfRateType'];
        this.cfRateDs.addParameter('cfId', cfId);
        var cfRateRecords = this.cfRateDs.getRecords(null);
        var rate = 0;
        if (cfRateRecords.length > 0) {
            switch (rateType) {
            case 'STANDARD':
                rate = cfRateRecords[0].getValue('cf.rate_hourly');
                break;
            case 'OVERTIME':
                rate = cfRateRecords[0].getValue('cf.rate_over');
                break;
            case 'DOUBLETIME':
                rate = cfRateRecords[0].getValue('cf.rate_double');
                break;
            }
        } else {
            rate = 0;
        }
        row.cells.get('vfRate').dom.innerHTML = rate;
    },

    /**
     * Set rate type list legend field background color.
     * @param {row} grid row element
     */
    setRateTypeLegendColor: function(row) {
        var rateType = row.record['vfRateType'];
        var color;
        switch (rateType) {
        case 'STANDARD':
            color = '#00CC00';
            break;
        case 'OVERTIME':
            color = '#CC0000';
            break;
        case 'DOUBLETIME':
            color = '#9900CC';
            break;
        }
        // get the id="legend" cell for this row (Ab.grid.Cell object)
        var cell = row.cells.get('legend');
        // set cell background color
        Ext.get(cell.dom).setStyle('background-color', color);
    },

    /**
     * set assign button backgound color.
     */
    setAssignButtonsBgColor: function(row, selectedRateType) {
        var rateType = row.record['vfRateType'];
        if (rateType == selectedRateType) {
            var cell = row.cells.get('assignButton').dom.childNodes[0].style.backgroundColor = "#FF6633";
        } else {
            var cell = row.cells.get('assignButton').dom.childNodes[0].style.backgroundColor = "";
        }
    }
});

/**
 * Schedule type onchange event handler.
 * @returns
 */
function scheduleTypeChangeHandler() {
    var radioButtons = document.getElementsByName('scheduleType');
    for (var i = 0; i < radioButtons.length; i++) {
        if (radioButtons[i].checked) {
            cfDailyScheduleControl.selectedScheduleType = radioButtons[i].value;
        }
    }
    cfDailyScheduleControl.enableFormFieldsByScheduleType(cfDailyScheduleControl.selectedScheduleType);
    cfDailyScheduleControl.enableScheduleControlByScheduleType(cfDailyScheduleControl.selectedScheduleType);
}

/**
 * Shift defination button click event handler.
 * @returns
 */
function onClickSelectShiftButton() {
    var shiftDefinationPanel = View.panels.get('shiftsDefinationPanel');
    shiftDefinationPanel.show(true);
    shiftDefinationPanel.showInWindow({
        width: 400,
        height: 500
    });
    shiftDefinationPanel.refresh(null);
}

/**
 * On click the check box to switch start date and end date controls.
 * @returns
 */
function onCheckIsSeasonalCheckbox(){
    var panel=View.panels.get('cfScheduleDetailPanel');
    var isSeasonal=$('isSesonalCbx').checked?'1':'0';
    var dateStart=null;
    var dateEnd=null;
    if(isSeasonal=='1'){
        dateStart=panel.getFieldValue('cf_schedules_assign.date_start');
        dateEnd=panel.getFieldValue('cf_schedules_assign.date_end');
        if(valueExistsNotEmpty(dateStart)){
            dateStart=panel.getDataSource().parseValue('cf_schedules_assign.date_start',dateStart,false);
        }
        
        if(valueExistsNotEmpty(dateEnd)){
            dateEnd=panel.getDataSource().parseValue('cf_schedules_assign.date_end',dateEnd,false);
        }
    }
    
    
    cfDailyScheduleControl.switchAndSetDateFieldControls(isSeasonal,dateStart,dateEnd);
}

/**
 * Trigger on select month drop down list item.
 * @param fieldName
 * @returns
 */
function onSelectMonthValue(fieldName){
    var selectedOptionIndex=$(fieldName).selectedIndex;
    var month=$(fieldName).options[selectedOptionIndex].value;
    cfDailyScheduleControl.setMonthValue(fieldName,month);
}
