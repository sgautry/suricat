/**
 * Base Craftsperson Weekly based Schedule control.
 * @author Jia
 */
var BaseScheduleControl = Base.extend({
    /**
     * Client index, used to identify all the scheduled works that created on schedule control, even if not saved to database.
     */
    clientIndex: null,
    /**
     * The configuration object.
     */
    config: null,
    /**
     * The array of records for craftspeople displayed in the scheduler.
     */
    resources: null,

    /**
     * The dxScheduler control instance.
     */
    scheduler: null,

    /**
     * The array of records for scheduled work displayed in the scheduler.
     */
    scheduledWork: null,
    /**
     * Default start day hour.
     */
    defaultStartDayHour: 0,

    /**
     * Default end day hour.
     */
    defaultEndDayHour: 23,

    /**
     * Control constructor.
     * @param {config} The control configuration.
     */
    constructor: function(config) {
        this.config = config;
        this.resources = [];
        this.scheduledWork = [];

        // Load the timeline.
        this.initializeScheduler(new Date());
    },

    /**
     * Sets the control height to specified value.
     * @param {height} The height in pixels.
     */
    setHeight: function(height) {
    },
    /**
     * Adds a listener to specified control event.
     * @param {eventName} The name of the control event, e.g. 'click' or 'locateBuilding'.
     * @param {listener} The listener function reference.
     */
    addEventListener: function(eventName, listener) {
    },

    /**
     * Removes listeners from specified control event.
     * @param {eventName} The name of the control event, e.g. 'click' or 'locateBuilding'.
     * @param {listener} The listener function reference. Optional. If not specified, the control removes all event listeners for this event.
     */
    removeEventListener: function(eventName) {
    },

    /**
     * Initializes the dxScheduler control with customizations required for the Labor Scheduling Board.
     * @param {weekStart} Date of the week start.
     */
    initializeScheduler: function(currentDate) {
        // schedule controller.
        var control = this;
        // schedule container panel div id.
        var container = this.config.container;
        // opener controller.
        var openerController = this.config.openerController;
        // schedule start day hour.
        var startDayHour = valueExistsNotEmpty(this.config.startDayHour) ? this.config.startDayHour : this.defaultStartDayHour;
        // schedule end day hour.
        var endDayHour = valueExistsNotEmpty(this.config.endDayHour) ? this.config.endDayHour : this.defaultEndDayHour;

        // initial the schedule control.
        var scheduler = jQuery("#" + container).dxScheduler({
            dataSource: this.scheduledWork,
            views: [ "week" ],
            currentView: "week",
            currentDate: currentDate,
            useDropDownViewSwitcher: false,
            showAllDayPanel: false,
            firstDayOfWeek: 0,
            startDayHour: startDayHour,
            endDayHour: endDayHour,
            cellDuration:60,
            resources: [ {
                field: "rateType",
                allowMultiple: false,
                dataSource: this.resources,
                label: getMessage('rateType')
            } ],
            width: "100%",
            
            // display the time scale based on user's locale.
            timeCellTemplate: function(itemData, itemIndex, itemElement) {
                //APP-1468:Use the locale to determine which format to display time with U.S.format or 24-hours format.
                var formattedTime=itemData.text.replace('AM', 'a').replace('PM', 'p');
                jQuery("<div>").text(formattedTime).appendTo(itemElement);
            },
            
            appointmentTooltipTemplate: function(data, container) {
                return control.getTooltipTemplate(data);
            },
            
            /**
             * The template to be used for rendering appointments.
             */
            appointmentTemplate: function(itemData,itemIndex,itemElement){
                var formattedRateType = View.dataSources.get(control.config.scheduledWorkDataSource).formatValue('cf_schedules_days.rate_type',itemData.rateType);
                
                var appointmentTitle = jQuery("<div>").addClass("dx-scheduler-appointment-title").text(formattedRateType);
                var formattedDateString = control._formatTooltipDate(itemData.startDate,itemData.endDate,false);
                var appointmentDetail = jQuery("<div>").addClass('dx-scheduler-appointment-content-details');
                var appointmentDate = jQuery("<div>").addClass("dx-scheduler-appointment-content-date").text(formattedDateString);
                appointmentDate.appendTo(appointmentDetail);
                
                appointmentTitle.appendTo(itemElement);
                appointmentDetail.appendTo(itemElement);
                //return content;
            },
            /**
             * Fired after appointment added.
             */
            onAppointmentAdding: function(event) {
                var craftspersonWorksDs = View.dataSources.get(control.config.scheduledWorkDataSource);
                // validate time block
                if(!control.validateTimeBlock(event.appointmentData)){
                    event.cancel = true;
                }else{
                    event.appointmentData['clientId'] = control.clientIndex++;
                    event.appointmentData['rateType'] = openerController.selectedRateType;
                }
                
            },

            /**
             * Fired when appointment updating.
             */
            onAppointmentUpdating: function(event) {
                if(!control.validateTimeBlock(event.newData)){
                    event.cancel = true;
                }
            },

            /**
             * Fired after appointment added.
             */
            onAppointmentDeleted: function(event) {
                var serverId = event.appointmentData.serverId;
                if (valueExistsNotEmpty(serverId)) {
                    var inArray = checkElementInArray(serverId, openerController.deletedScheduledWorks);
                    if (!inArray) {
                        openerController.deletedScheduledWorks.push(serverId);
                    }
                }
            },

            /**
             * Fired after appointment detail form panel created.
             */
            onAppointmentFormCreated: function(data) {
                var form = data.form;
                var startDate = data.appointmentData.startDate;
                var endDate = data.appointmentData.endDate;
                var rateType = data.appointmentData.rateType;
                if(!valueExistsNotEmpty(rateType)){
                    rateType=openerController.selectedRateType;
                }
                
                var craftspersonWorksDs = View.dataSources.get(control.config.scheduledWorkDataSource);

                form.option("title", getMessage('editScheduleWorkDetailTitle'));
                form.option("items", [ {
                    label: {
                        text: getMessage('timeStart')
                    },
                    dataField: "startDate",
                    editorType: "dxDateBox",
                    editorOptions: {
                        value: startDate,
                        type: "time",
                        onValueChanged: function(args) {
                            form.option("formData").startDate = args.value;
                        }
                    }
                },{
                    label: {
                        text: getMessage('timeEnd')
                    },
                    dataField: "endDate",
                    editorType: "dxDateBox",
                    editorOptions: {
                        value: endDate,
                        type: "time",
                        onValueChanged: function(args) {
                            form.option("formData").endDate = args.value;
                        }
                    }
                }, {
                    label: {
                        text: getMessage('rateType')
                    },
                    editorType: "dxSelectBox",
                    editorOptions: {
                        items: [ {
                            id: 'STANDARD',
                            text: craftspersonWorksDs.formatValue('cf_schedules_days.rate_type','STANDARD')
                        }, {
                            id: 'OVERTIME',
                            text: craftspersonWorksDs.formatValue('cf_schedules_days.rate_type','OVERTIME')
                        }, {
                            id: 'DOUBLETIME',
                            text: craftspersonWorksDs.formatValue('cf_schedules_days.rate_type','DOUBLETIME')
                        } ],
                        displayExpr: "text",
                        valueExpr: "id",
                        // Set current selected rate type.
                        value: rateType,
                        readOnly: true
                    }
                }]);
                
                //change buttons text to use ARCHIBUS translatable message.
                var buttons = data.component._popup.option('buttons');  
                if(buttons.length == 2){
                    buttons[0].options = { text: getMessage('Done') };
                    buttons[1].options = { text: getMessage('Cancel') };
                    data.component._popup.option('buttons', buttons);
                }    
                
                
            },

            // Format date cell column header.
            dateCellTemplate: function(itemData, itemIndex, itemElement) {
                var day = itemData.date.getDay();
                var text = itemData.text;
                switch (day) {
                case 0:
                    text = getMessage('sundayMessage');
                    break;
                case 1:
                    text = getMessage('mondayMessage');
                    break;
                case 2:
                    text = getMessage('tuesdayMessage');
                    break;
                case 3:
                    text = getMessage('wednesdayMessage');
                    break;
                case 4:
                    text = getMessage('thursdayMessage');
                    break;
                case 5:
                    text = getMessage('fridayMessage');
                    break;
                case 6:
                    text = getMessage('saturdayMessage');
                    break;
                }
                itemElement[0].innerHTML = text;
                itemElement[0].title="";
            },

            /**
             * If data displayed by the widget is specified using a DataSource instance, the contentReady event fires each time the load() method of the DataSource instance is called as well as when
             * the widget content is ready or an appointment is modified.
             */
            onContentReady: function(event) {
                openerController.trigger('ab:bldgops:labor:schedule:setTotalHoursForRateTypeList');
                //openerController.setTotalHoursForRateTypeList();
            }

        }).dxScheduler("instance");

        // hide schedule headers to avoid user change Year/Month/Date.
        jQuery(".dx-scheduler-header").css("display", "none");

        this.scheduler = scheduler;

        /**
         * check whether element in array or not.
         * @param {value} value.
         * @param {array} Array.
         */
        function checkElementInArray(value, array) {
            var inArray = false;
            for (var i = 0; i < array.length; i++) {
                if (array[i] == value) {
                    inArray = true;
                    break;
                }
            }
            return inArray;
        }
    },

    /**
     * Load Schedule Control
     */
    loadScheduledWork: function(restriction) {
        var me = this;
        // Initial rate type resource data.
        me.intialResourcesData();
        me.clientIndex = 1;
        // Initial scheduled work data.
        me.scheduledWork.length = 0;
        var craftspersonWorksDs = View.dataSources.get(this.config.scheduledWorkDataSource);
        var scheduledWorkRecords = craftspersonWorksDs.getRecords(restriction);

        _.each(scheduledWorkRecords, function(record) {
            var serverId = record.getValue('cf_schedules_days.auto_number');
            var timeStart = record.getValue('cf_schedules_days.time_start');
            var timeEnd = record.getValue('cf_schedules_days.time_end');
            var dayOfWeek = record.getValue('cf_schedules_days.day_of_week');
            var rateType = record.getValue('cf_schedules_days.rate_type');
            var formatRateType = craftspersonWorksDs.formatValue('cf_schedules_days.rate_type',rateType);
            if (timeStart !== '' && timeEnd !== '') {
                // get date according from current date and day of week.
                var dateByWeek = me.getDateByDayofWeek(dayOfWeek);
                me.scheduledWork.push({
                    // this client id is used to identify all the scheduled works , even if not saved to datasource.
                    clientId: me.clientIndex++,
                    serverId: serverId,
                    rateType: rateType,
                    text: formatRateType,
                    startDate: new Date(dateByWeek.getFullYear(), dateByWeek.getMonth(), dateByWeek.getDate(), timeStart.getHours(), timeStart.getMinutes(), timeStart.getSeconds()),
                    endDate: new Date(dateByWeek.getFullYear(), dateByWeek.getMonth(), dateByWeek.getDate(), timeEnd.getHours(), timeEnd.getMinutes(), timeEnd.getSeconds())
                });
            }
        });

        this.scheduler.repaint();
        jQuery(".dx-scheduler-header").css("display", "none");
        if(this.scheduler._$popup){
            this.scheduler._$popup=null;
            this.scheduler._cleanPopup();
        }
        
    },

    /**
     * Load a blank schedule control without any time block.
     */
    loadBlankScheduleControl: function() {
        var me = this;
        // Initial rate type resource data.
        me.intialResourcesData();
        me.clientIndex = 1;
        // Initial scheduled work data.
        me.scheduledWork.length = 0;
        this.scheduler.repaint();
        jQuery(".dx-scheduler-header").css("display", "none");
        this.enableScheduleControl(true);
        if(this.scheduler._$popup){
            this.scheduler._$popup=null;
            this.scheduler._cleanPopup();
        }
        
    },

    /**
     * Initial resources data.
     */
    intialResourcesData: function() {
        var craftspersonWorksDs = View.dataSources.get(this.config.scheduledWorkDataSource);
        this.resources.length = 0;
        this.resources.push({
            id: 'STANDARD',
            text: craftspersonWorksDs.formatValue('cf_schedules_days.rate_type','STANDARD'),
            color: '#00CC00'
        });
        this.resources.push({
            id: 'OVERTIME',
            text: craftspersonWorksDs.formatValue('cf_schedules_days.rate_type','OVERTIME'),
            color: '#CC0000'
        });
        this.resources.push({
            id: 'DOUBLETIME',
            text: craftspersonWorksDs.formatValue('cf_schedules_days.rate_type','DOUBLETIME'),
            color: '#9900CC'
        });
    },

    /**
     * Get date object by day-of-week value.
     * @param {dayOfWeek} day of week
     */
    getDateByDayofWeek: function(dayOfWeek) {
        var date = new Date();
        var dayofWeek = -1;
        switch (dayOfWeek) {
        case 'SUNDAY':
            dayofWeek = 0;
            break;
        case 'MONDAY':
            dayofWeek = 1;
            break;
        case 'TUESDAY':
            dayofWeek = 2;
            break;
        case 'WEDNESDAY':
            dayofWeek = 3;
            break;
        case 'THURSDAY':
            dayofWeek = 4;
            break;
        case 'FRIDAY':
            dayofWeek = 5;
            break;
        case 'SATURDAY':
            dayofWeek = 6;
            break;
        }
        if (dayofWeek != -1) {
            date.setDate(date.getDate() + (dayofWeek - date.getDay()));
        }

        return date;
    },
    
    /**
     * Validate time block during add or update.
     */
    validateTimeBlock: function(appointmentData){
        var result=true;
        var clientId=appointmentData.clientId;
        var startDate=appointmentData.startDate;
        var endDate=appointmentData.endDate;
        
        //Start Date must be earlier than End Date.
        if(startDate.getTime()>=endDate.getTime()){
            result=false;
            View.alert(getMessage('startDateMustBeEarlierThanEndDateMessage'));
            return;
        }
        
        //One time block can't acorss multiple days.
        if(startDate.getDay() !== endDate.getDay()){
            result=false;
            View.alert(getMessage('timeBlockCannotAcrossMultipleDaysMessage'));
            return;
        }
        
        //Time block can't overlap with other time blocks.
        var hasOverlappedWorks=this.hasOverlappedScheduledWorks(clientId,startDate,endDate);
        if(hasOverlappedWorks){
            result=false;
            View.alert(getMessage('scheduleTimeBlocksCannotOverlapMessage'));
            return;
        }
        
        return result;
    },
    

    /**
     * Find scheduledWork to avoid time blocks overlap.
     * @param {appointmentClientId} Updating appointment client id.
     * @param {startDate}
     */
    hasOverlappedScheduledWorks: function(appointmentClientId, startDate, endDate) {
        var hasOverlapped = false;
        // day of changed appointment.
        var dayOfChangedAppointment = startDate.getDay();
        var schedulerdWorks = this.scheduledWork;
        _.each(schedulerdWorks, function(record) {

            if (record.startDate.getDay() == dayOfChangedAppointment && record.clientId != appointmentClientId) {
                if (!((startDate.getTime() >= record.endDate.getTime()) || (endDate.getTime() <= record.startDate.getTime()))) {
                    hasOverlapped = true;
                }
            }
        });

        return hasOverlapped;
    },
    
    /**
     * Enable Schedule Control.
     * @param {enable|Boolean}
     */
    enableScheduleControl: function(enable) {
        this.scheduler.option('editing', enable)
    },
    
    /*****************************Customized the appointment tooltip.**************/
    
    /**
     * Get customized appointment tooltip template.
     */
    getTooltipTemplate: function(data) {
        var craftspersonWorksDs = View.dataSources.get(this.config.scheduledWorkDataSource);
        var $tooltip = jQuery("<div>").addClass("dx-scheduler-appointment-tooltip");
        startDate = data.startDate, endDate = data.endDate, description = data.description,
        startDateTimeZone = this.scheduler.fire("getField", "startDateTimeZone", data),
        endDateTimeZone = this.scheduler.fire("getField", "endDateTimeZone", data);
        startDate = this.scheduler.fire("convertDateByTimezone", startDate, startDateTimeZone);
        endDate = this.scheduler.fire("convertDateByTimezone", endDate, endDateTimeZone);
        var formattedRateType=craftspersonWorksDs.formatValue('cf_schedules_days.rate_type',data.rateType);
        jQuery("<div>").text(formattedRateType).addClass("dx-scheduler-appointment-tooltip-title").appendTo($tooltip);
        jQuery("<div>").addClass("dx-scheduler-appointment-tooltip-date").text(this._formatTooltipDate(startDate, endDate, false)).appendTo($tooltip);

        var $buttons = jQuery("<div>").addClass("dx-scheduler-appointment-tooltip-buttons").appendTo($tooltip);
        if (this.scheduler._editing.allowDeleting) {
            this._getDeleteButton(data).appendTo($buttons);
        }

        this._getEditButton(data).appendTo($buttons);
        return $tooltip;
    },

    /**
     * Format date for appointment tooltip popup window to display.
     */
    _formatTooltipDate: function(startDate, endDate, isAllDay) {
        var formattedString="";
        var formatType = "month" !== this.scheduler.option("currentView") && sameDate(startDate,endDate) ? "TIME" : "DATETIME", formattedString = "";
        if (isAllDay) {
            formatType = "DATE"
        }
        
        //Format date and time string by using the ARCHIBUS API.
        var dateFormatDs = View.dataSources.get('dateTimeFormateDs');
        
        var formattedStartString = "";
        var formattedEndString = "";
        
        if(formatType == "DATE"){
            formattedStartString = dateFormatDs.formatValue('wrcf.date_assigned',startDate,true);
            formattedEndString = dateFormatDs.formatValue('wrcf.date_assigned',endDate,true);
        }
        
        if(formatType == "DATETIME"){
            formattedStartString = dateFormatDs.formatValue('wrcf.date_assigned',startDate,true)+' '+dateFormatDs.formatValue('wrcf.time_assigned',startDate,true);
            formattedEndString = dateFormatDs.formatValue('wrcf.date_assigned',endDate,true)+' '+dateFormatDs.formatValue('wrcf.time_assigned',endDate,true);
        }
        
        if(formatType == "TIME"){
            formattedStartString = dateFormatDs.formatValue('wrcf.time_assigned',startDate,true);
            formattedEndString = dateFormatDs.formatValue('wrcf.time_assigned',endDate,true);
        }
        
        return formattedStartString +'-'+ formattedEndString;
    },

    /**
     * Get delete button element for appointment tooltip popup window.
     */
    _getDeleteButton: function(data) {
        var that = this;
        var ele = jQuery("<div>").dxButton({
            icon: "trash",
            onClick: function() {
                that.scheduler.deleteAppointment(data);
                that.scheduler.hideAppointmentTooltip();
            }
        });

        return ele;
    },

    /**
     * Get edit button element for appointment tooltip popup window.
     */
    _getEditButton: function(data) {
        var that = this, allowUpdating = that.scheduler._editing.allowUpdating;
        var text = allowUpdating? getMessage('Edit'): getMessage('View');
        var editButton = jQuery("<div>").dxButton({
            icon: allowUpdating ? "edit" : "",
            text: text,
            onClick: function() {
                //APP-1341
                if(that.scheduler._$popup){
                    that.scheduler._$popup=null;
                    that.scheduler._cleanPopup();
                }
                that.scheduler.showAppointmentPopup(data, false, data);
                that.scheduler.hideAppointmentTooltip();
            }
        });

        return editButton;
    },
    
    /**
     * get date by appointment item time text. like '12:00 AM' to Date Object with 24-hours format.
     */
    getDateByItemTimeText: function(timeText){
        var currentDate = new Date();
        var hours = parseInt(timeText.split(':')[0]);
        if(timeText.indexOf('AM') >0 && hours == 12){
            hours = 0;
        }
        if(timeText.indexOf('PM') >0){
            if(hours != 12){
                hours = hours + 12;
            }
        }
        
        return new Date(currentDate.getFullYear(),currentDate.getMonth(),currentDate.getDate(),hours,0,0); 
    }
});

var sameDate = function(date1, date2) {
    return sameMonthAndYear(date1, date2) && date1.getDate() === date2.getDate()
};
var sameMonthAndYear = function(date1, date2) {
    return sameYear(date1, date2) && date1.getMonth() === date2.getMonth()
};
var sameYear = function(date1, date2) {
    return date1 && date2 && date1.getFullYear() === date2.getFullYear()
};