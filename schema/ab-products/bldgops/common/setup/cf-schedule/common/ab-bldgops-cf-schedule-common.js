//**************************Schedule Control Common Methods***************************//
/**
 * Get day of week by day number.
 * @param {number} day of week.
 */
function getDayOfWeekByNumber(number) {
    var dayOfWeek = '';
    switch (number) {
    case 0:
        dayOfWeek = 'SUNDAY';
        break;
    case 1:
        dayOfWeek = 'MONDAY';
        break;
    case 2:
        dayOfWeek = 'TUESDAY';
        break;
    case 3:
        dayOfWeek = 'WEDNESDAY';
        break;
    case 4:
        dayOfWeek = 'THURSDAY';
        break;
    case 5:
        dayOfWeek = 'FRIDAY';
        break;
    case 6:
        dayOfWeek = 'SATURDAY';
        break;
    }

    return dayOfWeek;
}
// *************************Rate Type Grid Control Common Methods**********************//
/**
 * Get Rate Type grid rows.
 * @param {id} Grid div element id.
 * @param {rows} Grid rows.
 * @param {columns} Grid columns
 */
function createRateTypeGridListControl(id, rateTypeGridType, scope) {
    var rows = getRateTypeGridRows(rateTypeGridType);
    var columns = getRateTypeGridColumns(rateTypeGridType, scope);
    var configObj = new Ab.view.ConfigObject();
    configObj['rows'] = rows;
    configObj['columns'] = columns;

    // create new Grid component instance
    var grid = new Ab.grid.ReportGrid(id, configObj);
    return grid;
}

/**
 * Get rate type grid rows.
 * @param {rateTypeGridType} grid type
 */
function getRateTypeGridRows(rateTypeGridType) {
    var rows = [];
    if (rateTypeGridType == 'cfShiftSchedule') {
        rows = getGridRowsForCfShiftSchedule();
    }

    if (rateTypeGridType == 'cfDailySchedule') {
        rows = getGridRowsForCfDailySchedule();
    }

    return rows;
}

/**
 * Get rate type grid columns.
 * @param rateTypeGridType
 * @param scope Controller sope that using this rate type grid control.
 * @returns
 */
function getRateTypeGridColumns(rateTypeGridType, scope) {
    var columns = [];
    if (rateTypeGridType == 'cfShiftSchedule') {
        columns = getGridColumnsForCfShiftSchedule(scope);
    }

    if (rateTypeGridType == 'cfDailySchedule') {
        columns = getGridColumnsForCfDailySchedule(scope);
    }
    return columns;
}

/**
 * Get grid rows for craftsperson shift schdule view.
 * @returns
 */
function getGridRowsForCfShiftSchedule() {
    var formatDs = View.dataSources.get('rateTypeFormatDs');
    var rows = [];
    rows[0] = new Object();
    rows[0]['vfRateType.message'] = formatDs.formatValue('cf_schedules_days.rate_type','STANDARD');
    rows[0]['vfRateType'] = 'STANDARD';
    rows[0]['vfTotalWeeklyHours'] = '0';

    rows[1] = new Object();
    rows[1]['vfRateType.message'] = formatDs.formatValue('cf_schedules_days.rate_type','OVERTIME');
    rows[1]['vfRateType'] = 'OVERTIME';
    rows[1]['vfTotalWeeklyHours'] = '0';

    rows[2] = new Object();
    rows[2]['vfRateType.message'] = formatDs.formatValue('cf_schedules_days.rate_type','DOUBLETIME');;
    rows[2]['vfRateType'] = 'DOUBLETIME';
    rows[2]['vfTotalWeeklyHours'] = '0';

    return rows;
}

/**
 * Get grid rows for craftsperson daily schedule view.
 * @returns
 */
function getGridRowsForCfDailySchedule() {
    var formatDs = View.dataSources.get('rateTypeFormatDs');
    var rows = [];
    rows[0] = new Object();
    rows[0]['vfRateType.message'] = formatDs.formatValue('cf_schedules_days.rate_type','STANDARD');
    rows[0]['vfRateType'] = 'STANDARD';
    rows[0]['vfRate'] = '0';
    rows[0]['vfTotalWeeklyHours'] = '0';

    rows[1] = new Object();
    rows[1]['vfRateType.message'] = formatDs.formatValue('cf_schedules_days.rate_type','OVERTIME');
    rows[1]['vfRateType'] = 'OVERTIME';
    rows[1]['vfRate'] = '0';
    rows[1]['vfTotalWeeklyHours'] = '0';

    rows[2] = new Object();
    rows[2]['vfRateType.message'] = formatDs.formatValue('cf_schedules_days.rate_type','DOUBLETIME');
    rows[2]['vfRateType'] = 'DOUBLETIME';
    rows[2]['vfRate'] = '0';
    rows[2]['vfTotalWeeklyHours'] = '0';

    return rows;
}

/**
 * Get grid columns for craftsperson shift schedule view.
 */
function getGridColumnsForCfShiftSchedule(scope) {
    var columns = [ new Ab.grid.Column('vfRateType.message', getMessage('rateType'), 'text'),new Ab.grid.Column('vfRateType', getMessage('rateType'), 'text'), new Ab.grid.Column('vfTotalWeeklyHours', getMessage('totalWeeklyHours'), 'number'),
            new Ab.grid.Column('legend', getMessage('legend'), 'text'), new Ab.grid.Column('assignButton', '', 'button', scope.onClickAssignButton.createDelegate(scope)) ];
    columns[4].text = getMessage('assignMessage');
    return columns;
}

/**
 * Get grid columns for craftsperson daily schedule view.
 */
function getGridColumnsForCfDailySchedule(scope) {
    var columns = [ new Ab.grid.Column('vfRateType.message', getMessage('rateType'), 'text'),new Ab.grid.Column('vfRateType', getMessage('rateType'), 'text'), new Ab.grid.Column('vfRate', getMessage('rate'), 'number'),
            new Ab.grid.Column('vfTotalWeeklyHours', getMessage('totalWeeklyHours'), 'number'), new Ab.grid.Column('legend', getMessage('legend'), 'text'),
            new Ab.grid.Column('assignButton', '', 'button', scope.onClickAssignButton.createDelegate(scope)) ];
    columns[5].text = getMessage('assignMessage');
    return columns;
}

// ******************************Common Date Range Overlap Validation Methods******************************//

/**
 * Validation for checking the overlap for schedule date range.
 * @param {dataSourceId} Schedule DataSource Id.
 * @param {selectedScheduleRecord} Selected schedule record.
 * @param {cfId} Craftsperson Code.
 * @param {dateStart} Start Date.Date Object.
 * @param {dateEnd} End Date. Date Object
 * @returns {true|false}
 */
function validateScheduleDateRangeOverlap(cfAssignedRecords,selectedAssignedId,newDateStart, newDateEnd, newIsSeasonal) {
    var isValidatePassed = true;
    // 1.If has any exists records, if not, then validate pass.
    if (cfAssignedRecords.length > 0) {
        // check permanent schedule without
        var permanentPassed = checkOverlapForScheduleDateRange(cfAssignedRecords, selectedAssignedId, newDateStart, newDateEnd, newIsSeasonal);
        if (!permanentPassed) {
            isValidatePassed = false;
            return isValidatePassed;
        }
    }
    return isValidatePassed;
}

/**
 * Check permanent schedule
 * @param {assignedScheduleRecords} Assigned Schedule Records
 * @param selectedScheduleRecord
 * @param dateStart
 * @param dateEnd
 * @param isSeasonal
 * @returns
 */
function checkOverlapForScheduleDateRange(assignedScheduleRecords, cfAssignedId, newDateStart, newDateEnd, newIsSeasonal) {
    var isPassed = true;
    _.each(assignedScheduleRecords, function(assignedScheduleRecord) {
        var assignedDateStart = assignedScheduleRecord.getValue('cf_schedules_assign.date_start');
        var assignedDateEnd = assignedScheduleRecord.getValue('cf_schedules_assign.date_end');
        var assignedIsSeasonal = assignedScheduleRecord.getValue('cf_schedules_assign.is_seasonal');
        // ignore current assigned rec
        if (!isSelectedScheduleAssignEqualWithOtherSchedule(assignedScheduleRecord, cfAssignedId)) {
            var isChecked = checkSingleRecordDateRangeOverlap(assignedDateStart, assignedDateEnd, assignedIsSeasonal, newDateStart, newDateEnd, newIsSeasonal);
            if (!isChecked) {
                isPassed = false;
                return isPassed;
            }
        }
    });

    return isPassed;
}

/**
 * assigned schedule record should not equal with selected schedule record. If update record ,then ignore this selected schedule.
 * @param {assignedScheduleRecord} assigned schedule record of selected craftsperson.
 * @param {selectedCfAssignedId} Selected schedule assign id of craftsperson.
 */
function isSelectedScheduleAssignEqualWithOtherSchedule(assignedScheduleRecord, selectedCfAssignedId) {
    var isEqual = false;

    if (valueExistsNotEmpty(selectedCfAssignedId)) {
        var assignedId = assignedScheduleRecord.getValue('cf_schedules_assign.auto_number');
        if (assignedId == selectedCfAssignedId) {
            isEqual = true;
        }
    }

    return isEqual;
}

/**
 * Check schedule date range overlap for single record.
 */
function checkSingleRecordDateRangeOverlap(assignedDateStart, assignedDateEnd, assignedIsSeasonal, newDateStart, newDateEnd, newIsSeasonal) {
    var isPassed = true;
    
    //no two seasonal schedules can overlap.
    if(newIsSeasonal=='1' && assignedIsSeasonal=='1'){
        var year=new Date().getFullYear();
        assignedDateStart.setFullYear(year);
        var assignedDateEndYear=getEndYearForSeasonalSchedule(year,assignedDateStart,assignedDateEnd);
        assignedDateEnd.setFullYear(assignedDateEndYear);
        
        newDateStart.setFullYear(year);
        var newDateEndYear=getEndYearForSeasonalSchedule(year,newDateStart,newDateEnd);
        newDateEnd.setFullYear(newDateEndYear);
        
        // check whether overlapped if date start and date end field are both not empty.
        var isOverlapped = checkOverlappedForMonthAndDay(assignedDateStart, assignedDateEnd, newDateStart, newDateEnd);
        if (isOverlapped) {
            isPassed = false;
        }
    }
    
    //no two non-seasonal schedules can overlap.
    if(newIsSeasonal=='0' && assignedIsSeasonal=='0'){
        if(!newDateStart){
            newDateStart=new Date(1,1);
        }
        if(!newDateEnd){
            newDateEnd=new Date(2999,1);
        }
        if(!assignedDateStart){
            assignedDateStart=new Date(1,1);
        }
        if(!assignedDateEnd){
            assignedDateEnd=new Date(2999,1);
        }
        
        if(!(newDateStart.getTime() > assignedDateEnd.getTime() || newDateEnd.getTime() < assignedDateStart.getTime())){
            isPassed = false;
        }
    }

    return isPassed;
}

/**
 * Compare month1-day1 with month2-day2. if month2,day2 greater than month1,day1 then return true,else return false.
 * @param month1
 * @param day1
 * @param month2
 * @param day2
 * @returns
 */
function checkOverlappedForMonthAndDay(assignedDateStart, assignedDateEnd, newDateStart, newDateEnd) {
    var isOverlapped = false;

    if (!(assignedDateStart.getTime() > newDateEnd.getTime() || assignedDateEnd.getTime() < newDateStart.getTime())) {
        isOverlapped = true;
    }

    return isOverlapped;
}

/**
 * Check date fields before save.
 * @param dateStart
 * @param dateEnd
 * @param isSeasonal
 * @returns
 */
function checkDateFieldsBeforeSave(dateStart, dateEnd, isSeasonal) {
    var result = true;

    if (valueExistsNotEmpty(dateEnd) && !valueExistsNotEmpty(dateStart)) {
        View.alert(getMessage('startDateCannotEmptyIfEndDateNotEmptyMessage'));
        result = false;
    }

    if (isSeasonal == '0') {
        if (valueExistsNotEmpty(dateStart) && valueExistsNotEmpty(dateEnd)) {
            if (dateStart.getTime() >= dateEnd.getTime()) {
                View.alert(getMessage('startDateShouldEalierThanEndDateMessage'));
                result = false;
            }
        }

    } else {
        if(valueExistsNotEmpty(dateStart) && valueExistsNotEmpty(dateEnd)){
            if(dateStart.getMonth()==dateEnd.getMonth() && dateStart.getDate() == dateEnd.getDate()){
                View.alert(getMessage('startDateCannotTheSameWithEndDateMessage'));
                result=false;
            }
        }else{
            View.alert(getMessage('permanentScheduleCannotSeasonalMessage'));
            result = false;
        }
    }

    return result;

}

/**
 * get end year of seasonal record.
 */
function getEndYearForSeasonalSchedule(year,dateStart,dateEnd) {
    var endYear=year;
    if(dateEnd.getMonth()<dateStart.getMonth()){
        endYear=year+1;
    }else if(dateEnd.getMonth()==dateStart.getMonth() && dateEnd.getDate()<dateStart.getDate()){
        endYear=year+1;
    }
    
    return endYear;
}

// **************************Other View Common Methods***************************//
/**
 * Get schedule name by schedule id.
 * @param {scheduleId} Craftsperson Schedule Id.
 * @returns {Schedule Name|String}
 */
function getScheduleNameByScheduleId(dataSourceId, scheduleId) {
    var scheduleName = "";
    var restriction = new Ab.view.Restriction();
    restriction.addClause('cf_schedules.cf_schedule_id', scheduleId, '=');
    var scheduleRecords = View.dataSources.get(dataSourceId).getRecords(restriction);
    if (scheduleRecords.length > 0) {
        scheduleName = scheduleRecords[0].getValue('cf_schedules.schedule_name');
    }

    return scheduleName;
}

//**************************Customized start/End date control methods**************************************//

/**
 * Get month items.
 * @returns
 */
function getMonthItems(){
    var monthItems=[
        {key:0,value:arrMonthNames[0]},
        {key:1,value:arrMonthNames[1]},
        {key:2,value:arrMonthNames[2]},
        {key:3,value:arrMonthNames[3]},
        {key:4,value:arrMonthNames[4]},
        {key:5,value:arrMonthNames[5]},
        {key:6,value:arrMonthNames[6]},
        {key:7,value:arrMonthNames[7]},
        {key:8,value:arrMonthNames[8]},
        {key:9,value:arrMonthNames[9]},
        {key:10,value:arrMonthNames[10]},
        {key:11,value:arrMonthNames[11]}
    ];
    return monthItems;
}

/**
 * Get month name by month 
 * @param month
 * @returns
 */
function getMonthName(month){
    var monthNames=[
               arrMonthNames[0],
               arrMonthNames[1],
               arrMonthNames[2],
               arrMonthNames[3],
               arrMonthNames[4],
               arrMonthNames[5],
               arrMonthNames[6],
               arrMonthNames[7],
               arrMonthNames[8],
               arrMonthNames[9],
               arrMonthNames[10],
               arrMonthNames[11]
             ];
    return monthNames[month];
}

/**
 * Get days for the specify month.
 * @param month
 * @returns
 */
function getDaysByMonth(month){
    var days=[];
    var daysCount=0;
    if(valueExistsNotEmpty(month)){
        month=parseInt(month);
        month=month+1;
        if(month==2){
            daysCount=28;
        }else if(month==1||month==3||month==5||month==7||month==8||month==10||month==12){
            daysCount=31;
        }else if(month==4||month==6||month==9||month==11){
            daysCount=30;
        }
    }
    for(var i=0;i<daysCount;i++){
        days.push({key:i+1,value:i+1});
    }
    
    return days;
}


/**
 * Get current applied scheudle assigned Id for selected craftsperson.
 * @param cfId Craftsperson Code.
 * @returns Schedule Assigned primary key.
 */
function getCurrentApplyScheduleAssignedIdForCf(dataSourceId,cfId){
    var currentDate=new Date();
    var res=new Ab.view.Restriction();
        res.addClause('cf_schedules_assign.cf_id',cfId,'=');
    var assignedRecords=View.dataSources.get(dataSourceId).getRecords(res);
    var assignedId=null;
    for(var i=0;i<assignedRecords.length;i++){
        var record=assignedRecords[i];
        var autoNumber=record.getValue('cf_schedules_assign.auto_number');
        var isSeasonal=record.getValue('cf_schedules_assign.is_seasonal');
        var dateStart=record.getValue('cf_schedules_assign.date_start');
        var dateEnd=record.getValue('cf_schedules_assign.date_end');
        
        if(isSeasonal=='0'){
            var isInPeriod=this.isCurrentDateInTimePeriod(dateStart,dateEnd);
            if(isInPeriod){
                assignedId=autoNumber;
                break;
            }
        }
        
        //Seasonal Schedule. only compare month and day time.
        if(isSeasonal=='1'){
            if((dateStart.getMonth()>dateEnd.getMonth())||(dateStart.getMonth()==dateEnd.getMonth() && dateStart.getDate()>dateEnd.getDate())){
                //format the seasonal date period in the current year.
                var startDate1=new Date(currentDate.getFullYear(),1,1);
                var endDate1=new Date(currentDate.getFullYear(),dateEnd.getMonth(),dateEnd.getDate());
                
                var startDate2=new Date(currentDate.getFullYear(),dateStart.getMonth(),dateStart.getDate());
                var endDate2=new Date(currentDate.getFullYear(),12,31); 
                
                var isInPeriod=this.isCurrentDateInTimePeriod(startDate1,endDate1);
                if(!isInPeriod){
                    isInPeriod=this.isCurrentDateInTimePeriod(startDate2,endDate2);
                }
                
                if(isInPeriod){
                    assignedId=autoNumber;
                    break;
                }
            }else{
                var startDate=new Date(currentDate.getFullYear(),dateStart.getMonth(),dateStart.getDate());
                var endDate=new Date(currentDate.getFullYear(),dateEnd.getMonth(),dateEnd.getDate());
                var isInPeriod=this.isCurrentDateInTimePeriod(startDate,endDate);
                if(isInPeriod){
                    assignedId=autoNumber;
                    break;
                }
            }
        }
    }
    
    return assignedId; 
}

/**
 * Is current date in specified time period.
 * @param startDate
 * @param endDate
 * @returns true|false.
 */
function isCurrentDateInTimePeriod(startDate,endDate){
    var inPeriod=false;
    var currentDate=new Date();
    if(!startDate && !endDate){
        inPeriod=true;
    }else if(!startDate){
        if(currentDate.getTime()<endDate.getTime()){
            inPeriod=true;
        }
    }else if(!endDate){
        if(currentDate.getTime()>startDate.getTime()){
            inPeriod=true;
        }
    }else{
        if(currentDate.getTime()>=startDate.getTime() && currentDate.getTime()<=endDate.getTime()){
            inPeriod=true;
        }
    }
    
    return inPeriod;
}

