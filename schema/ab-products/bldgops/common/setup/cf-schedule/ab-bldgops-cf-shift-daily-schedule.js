var shiftScheduleCtrl = View.createController('shiftScheduleCtrl', {
    openerController: null,
    // Default selected rate type.
    defaultSelectedRateType: 'STANDARD',
    // Selected rate type to assign.
    selectedRateType: '',
    // Schedule control.
    scheduler: null,
    // selected schedule id from opener view.
    scheduleId: "",
    // Flag to indicate this view in add new mode or not.
    isNewMode: true,
    // Rate type list control object.
    rateTypeGridControl: null,
    // Array of deleted scheduled works,used to delete server side data.
    deletedScheduledWorks: [],

    afterViewLoad: function() {
        var locale = View.user.locale.slice(0, 2);
        DevExpress.localization.locale(locale);
        this.on('ab:bldgops:labor:schedule:setTotalHoursForRateTypeList', this.setTotalHoursForRateTypeList);
        this.selectedRateType = this.defaultSelectedRateType;

        // Initialized schedule control.
        this.scheduler = View.createControl({
            control: 'ShiftScheduleControl',
            container: 'scheduler',
            panel: 'scheduleControlPanel',
            openerController: this,
            scheduledWorkDataSource: 'cfScheduleDaysDs',
            listeners: {
            // add other listener here.
            }
        });

    },

    afterInitialDataFetch: function() {
        this.openerController = View.getOpenerView().controllers.get('shiftManagerCtrl');
        // register tab controller.
        this.registerController(this.openerController);
        // get selected schedule id from opener view.
        this.scheduleId = this.openerController.selectedScheduleId;

        this.showCraftspersonShift(this.scheduleId);
    },

    /**
     * Show craftsperson shift panels.
     * @param {scheduleId} Selected craftsperson shift code.
     */
    showCraftspersonShift: function(scheduleId) {
        // Build rate type schedule control.
        this.deletedScheduledWorks.length = 0;
        this.selectedRateType = this.defaultSelectedRateType;
        this.rateTypeGridControl = this.createRateTypeListControl();
        this.scheduleInfoFormPanel.clear();

        if (valueExistsNotEmpty(scheduleId)) {
            this.isNewMode = false;
            this.scheduleInfoFormPanel.actions.get('delete').forceDisable(false);
            // set form fields value.
            var scheduleName = getScheduleNameByScheduleId('cfSchedulesDs', scheduleId);
            this.scheduleInfoFormPanel.setFieldValue('cf_schedules.schedule_name', scheduleName);
            // load schedule control
            this.loadScheduleControl();
        } else {
            this.isNewMode = true;
            this.scheduleInfoFormPanel.actions.get('delete').forceDisable(true);
            // load blank schedule control in addnew mode.
            this.scheduler.loadBlankScheduleControl();
        }
    },

    /**
     * Register controller to opener view controller.
     */
    registerController: function(openerController) {
        openerController.tabCtrls['shiftDailyScheduleTab'] = this;
    },

    /**
     * After selected a shift from opener shift list.
     */
    selectedShift: function(scheduleId) {
        this.scheduleId = scheduleId;
        this.showCraftspersonShift(scheduleId);
    },

    /**
     * Load Schedule Control.
     */
    loadScheduleControl: function() {
        var res = new Ab.view.Restriction();
        res.addClause('cf_schedules_days.cf_schedule_id', this.scheduleId, '=');
        this.scheduler.loadScheduledWork(res);
        this.deletedScheduledWorks.length = 0;
    },

    /**
     * Do actions below when click assign button.
     * @param {row} selected row object.
     */
    onClickAssignButton: function(row) {
        var me = this;
        var rateType = row['vfRateType'];
        // reset global parameter.
        this.selectedRateType = rateType;
        // reset the background color of assign button
        var gridRowsItems = this.rateTypeGridControl.gridRows.items;
        _.each(gridRowsItems, function(row) {
            me.setAssignButtonsBgColor(row, rateType);
        });
    },

    /**
     * Set total hours for rate type list after schedule control content ready.
     */
    setTotalHoursForRateTypeList: function() {
        var me = this;
        if (valueExists(this.scheduler) && valueExists(this.rateTypeGridControl)) {
            var gridRowsItems = this.rateTypeGridControl.gridRows.items;
            _.each(gridRowsItems, function(row) {
                me.getTotalWeeklyHours(row);
            });
        }
    },

    /**
     * Get total weekly hours.
     * @param {row} grid row element
     */
    getTotalWeeklyHours: function(row) {
        var me = this;
        var rowRateType = row.record['vfRateType'];
        var scheduledWorks = this.scheduler.scheduledWork;
        var totalHour = 0;
        _.each(scheduledWorks, function(appointmentData) {
            var dateStart = appointmentData.startDate;
            var dateEnd = appointmentData.endDate;
            var rateType = appointmentData.rateType;

            if (rateType == rowRateType) {
                var hour = Math.abs(dateEnd - dateStart) / (1000 * 60 * 60);

                totalHour += hour;
            }
        });
        row.cells.get('vfTotalWeeklyHours').dom.innerHTML = '' + totalHour.toFixed(2);
    },

    /**
     * Save shift defination.
     */
    scheduleInfoFormPanel_onSave: function() {
        var me = this;
        if (!this.validateFormFieldsBeforeSave()) {
            return;
        }
        var newScheduleName = this.scheduleInfoFormPanel.getFieldValue('cf_schedules.schedule_name');
        // update or create new schedule base on isNewMode.
        if (this.isNewMode) {
            // Insert new scheduled work.
            var scheduleType = 'SHIFT DEFINITION';
            var scheduleId = this.addNewSchedule(newScheduleName, scheduleType);
            this.scheduleId = scheduleId;
        } else {
            // update schedule name.
            this.updateScheduleName(this.scheduleId, newScheduleName);
        }
        // create or update Shift Definition records.
        this.saveScheduleTimeBlocks(this.scheduler.scheduledWork);
        // delete all scheduled works that marked as delete from server side .
        this.deleteScheduleWorks(this.deletedScheduledWorks);

        this.afterShiftSaved(this.scheduleId, newScheduleName);
    },

    /**
     * Do after shift saved.
     */
    afterShiftSaved: function(scheduleId, scheduleName) {
        // re-set global schedule id.
        this.openerController.trigger('bldgops:craftsperson:manage:shifts:afterUpdatedShift', scheduleId,scheduleName);
    },

    /**
     * Add new craftsperson schedule.
     * @param {scheduleName} craftsperson schedule name.
     * @param {scheduleType} craftsperson schedule type.
     */
    addNewSchedule: function(scheduleName, scheduleType) {
        var scheduleRecord = new Ab.data.Record();
        scheduleRecord.isNew = true;
        scheduleRecord.oldValues = new Object();
        scheduleRecord.setValue('cf_schedules.schedule_name', scheduleName);
        scheduleRecord.setValue('cf_schedules.schedule_type', scheduleType);
        var resultRecord = this.cfSchedulesDs.saveRecord(scheduleRecord);
        return resultRecord.getValue('cf_schedules.cf_schedule_id');
    },

    /**
     * Update schedule name.
     */
    updateScheduleName: function(scheduleId, scheduleName) {
        var scheduleRes = new Ab.view.Restriction();
        scheduleRes.addClause('cf_schedules.cf_schedule_id', scheduleId, '=');
        var scheduleRecord = this.cfSchedulesDs.getRecord(scheduleRes);
        scheduleRecord.setValue('cf_schedules.schedule_name', scheduleName);
        this.cfSchedulesDs.saveRecord(scheduleRecord);
    },

    /**
     * Update schedule day record.
     */
    updateScheduleDay: function(serverId, timeStart, timeEnd, dayOfWeek, rateType) {
        var record = new Ab.data.Record();
        record.isNew = false;
        record.setOldValue('cf_schedules_days.auto_number', serverId);
        record.setValue('cf_schedules_days.auto_number', serverId)
        record.setValue('cf_schedules_days.time_start', timeStart);
        record.setValue('cf_schedules_days.time_end', timeEnd);
        record.setValue('cf_schedules_days.day_of_week', dayOfWeek);
        record.setValue('cf_schedules_days.rate_type', rateType);
        this.cfScheduleDaysDs.saveRecord(record);
    },

    /**
     * Add new schedule day record.
     */
    addNewScheduleDay: function(scheduleId, timeStart, timeEnd, dayOfWeek, rateType) {
        var record = new Ab.data.Record();
        record.isNew = true;
        record.oldValues = new Object();
        record.setValue('cf_schedules_days.cf_schedule_id', scheduleId);
        record.setValue('cf_schedules_days.time_start', timeStart);
        record.setValue('cf_schedules_days.time_end', timeEnd);
        record.setValue('cf_schedules_days.day_of_week', dayOfWeek);
        record.setValue('cf_schedules_days.rate_type', rateType);

        this.cfScheduleDaysDs.saveRecord(record);
    },

    /**
     * Save or update time blocks.
     */
    saveScheduleTimeBlocks: function(appointments) {
        var me = this;
        var scheduledWorks = appointments;
        var scheduleDaysDs = this.cfScheduleDaysDs;
        _.each(scheduledWorks, function(appointmentData) {
            var serverId = appointmentData.serverId;
            var timeStart = appointmentData.startDate;
            var timeEnd = appointmentData.endDate;
            var rateType = appointmentData.rateType;
            var dayOfWeek = getDayOfWeekByNumber(timeStart.getDay());

            if (valueExistsNotEmpty(serverId)) {
                // Update the existing scheduled days record.
                me.updateScheduleDay(serverId, timeStart, timeEnd, dayOfWeek, rateType);
            } else {
                // Insert new scheduled days record.
                me.addNewScheduleDay(me.scheduleId, timeStart, timeEnd, dayOfWeek, rateType);
            }
        });
    },

    /**
     * Delete schedule time blocks from cache array.
     * @param {deletedScheduledWorks} The local array to cache the delete scheduled time blocks.
     */
    deleteScheduleWorks: function(deletedScheduledWorks) {
        // delete all scheduled works that marked as delete from server side .
        for (var i = 0; i < this.deletedScheduledWorks.length; i++) {
            var serverId = this.deletedScheduledWorks[i];
            var res = new Ab.view.Restriction();
            res.addClause('cf_schedules_days.auto_number', serverId, '=');
            var deleteRecord = this.cfScheduleDaysDs.getRecord(res);

            this.cfScheduleDaysDs.deleteRecord(deleteRecord);
        }
    },

    /**
     * Validate form fields.
     */
    validateFormFieldsBeforeSave: function() {
        var isValidate = true;
        var scheduleName = this.scheduleInfoFormPanel.getFieldValue('cf_schedules.schedule_name');
        // schedule name can not be empty.
        if (!valueExistsNotEmpty(scheduleName)) {
            View.alert(getMessage('scheduleNameCannotEmptyMsg'));
            isValidate = false;
            return;
        }

        if (this.scheduler.scheduledWork.length < 1) {
            View.alert(getMessage('noTimeBlocksOnScheduleControlMsg'));
            isValidate = false;
            return;
        }
        return isValidate;
    },

    /**
     * Delete the shift defination.
     */
    scheduleInfoFormPanel_onDelete: function() {
        var me = this;
        var scheduleId = this.scheduleId;
        var cfSchedulesDs = this.cfSchedulesDs;
        var cfScheduleDaysDs = this.cfScheduleDaysDs;
        var cfScheduleAssignDs = this.cfSchedulesAssignDs;
        View.confirm(getMessage('deleteShiftMessage'), function(button) {
            if (button == 'yes') {

                // delete all schedule days records.
                var scheduleDaysRes = new Ab.view.Restriction();
                scheduleDaysRes.addClause('cf_schedules_days.cf_schedule_id', scheduleId, '=');

                var scheduleDaysRecords = cfScheduleDaysDs.getRecords(scheduleDaysRes);
                _.each(scheduleDaysRecords, function(record) {
                    cfScheduleDaysDs.deleteRecord(record);
                });

                // delete all schedule assign records.
                var scheduleAssignRes = new Ab.view.Restriction();
                scheduleAssignRes.addClause('cf_schedules_assign.cf_schedule_id', scheduleId, '=');

                var scheduleAssignRecords = cfScheduleAssignDs.getRecords(scheduleAssignRes);
                _.each(scheduleAssignRecords, function(record) {
                    cfScheduleAssignDs.deleteRecord(record);
                });

                // delete schedule.
                var scheduleRes = new Ab.view.Restriction();
                scheduleRes.addClause('cf_schedules.cf_schedule_id', scheduleId, '=');
                cfSchedulesDs.deleteRecord(cfSchedulesDs.getRecord(scheduleRes));

                // refresh opener view panels after delete.
                me.openerController.trigger('bldgops:craftsperson:manage:shifts:afterDeletedShift');

                View.getOpenerView().closeDialog();
            }
        })
    },

    /**
     * Cancel define shift definition.
     */
    scheduleInfoFormPanel_onCancel: function() {
        this.openerController.switchToAddNewMode();
    },

    /**
     * Create rate type list control.
     */
    createRateTypeListControl: function() {
        // create rate type grid control.
        var grid = createRateTypeGridListControl('rateTypeListGrid_control', 'cfShiftSchedule', this);
        grid.showColumn('vfRateType',false);
        grid.build();
        this.setColorAfterRateTypeGridBuild(grid);

        return grid;
    },

    /**
     * Set legend color and assign button background color after grid build.
     * @param {gird} Rate Type Grid.
     */
    setColorAfterRateTypeGridBuild: function(grid) {
        var me = this;
        var gridRowsItems = grid.gridRows.items;
        _.each(gridRowsItems, function(row) {
            // set background color for legend field
            me.setRateTypeLegendColor(row);
            // set default background color of selected rate type.
            me.setAssignButtonsBgColor(row, me.selectedRateType);
        });
    },

    /**
     * Set rate type list legend field background color.
     * @param {row} grid row element
     */
    setRateTypeLegendColor: function(row) {
        var rateType = row.record['vfRateType'];
        var color;
        switch (rateType) {
        case 'STANDARD':
            color = '#00CC00';
            break;
        case 'OVERTIME':
            color = '#CC0000';
            break;
        case 'DOUBLETIME':
            color = '#9900CC';
            break;
        }
        // get the id="legend" cell for this row (Ab.grid.Cell object)
        var cell = row.cells.get('legend');
        // set cell background color
        Ext.get(cell.dom).setStyle('background-color', color);
    },
    /**
     * set assign button backgound color.
     */
    setAssignButtonsBgColor: function(row, selectedRateType) {
        var rateType = row.record['vfRateType'];
        if (rateType == selectedRateType) {
            var cell = row.cells.get('assignButton').dom.childNodes[0].style.backgroundColor = "#FF6633";
        } else {
            var cell = row.cells.get('assignButton').dom.childNodes[0].style.backgroundColor = "";
        }
    }
});