/**
 * Schedule detail buttons color
 */
holidayColor = '#ffd966';
vacationColor = '#3d85c6';
sickLeaveColor = '#a61c00';
personalColor = '#cccccc';
busyColor = '#666666';
otherColor = '#ff6f00';
siteVistColor = 'transparent';
standardRateColor = '#00CC00';
overTimeRateColor = '#CC0000';
doubleTimeRateColor = '#9900CC';

var scheduleDetailsCtrl = View.createController('scheduleDetailsCtrl', {
    /**
     * The WorkScheduleControl instance.
     */
    scheduler: null,

    // Selected schedule details type.
    selectedScheduleDetailType: 'OTHER',
    
    afterViewLoad: function() {
        var locale = View.user.locale.slice(0, 2);
        DevExpress.localization.locale(locale);

        this.scheduler = View.createControl({
            control: 'TimeLineScheduleControl',
            container: 'workScheduleContainer',
            panel: 'workSchedule',
            openerController: this,
            craftspersonDataSourceId: 'cfDs',
            scheduleVariancesDataSourceId: 'cfScheduleVariancesDs',
            scheduleAssignDataSourceId: 'cfSchedulesAssignDs',
            scheduleDaysDataSourceId: 'cfSchedulesDaysDs',
            startDate: new Date(2017, 1, 1)
        });
        //hide site code and building code fields.
        showConsoleFormLocationFields(false);
        
    },

    afterInitialDataFetch: function() {
        //show variance type grid control.
        this.showVarianceTypeGrid();
        //show scheduler control.
        this.showWorkSchedule();
    },

    /**
     * Queries and displays work schedule for specified restriction.
     * @param restriction
     */
    showWorkSchedule: function() {
        var consoleRecord = this.consoleForm.getRecord();
        this.scheduler.openerViewConsoleRecord = consoleRecord;
        this.scheduler.loadSchedule();
    },

    /**
     * Event listener on filter schedule variances.
     */
    onFilterScheduleVariances: function() {
        // show schedule.
        this.showWorkSchedule();
    },

    /**
     * Event handler when click on clear button in console form.
     */
    onClearConsoleForm: function() {
        this.consoleForm.clear();
        showConsoleFormLocationFields(false);
        this.showWorkSchedule();
    },

    /**
     * Show schedule variance buttons.
     */
    showVarianceTypeGrid: function() {
        var scheduleDetailButtonsElement = $('scheduleDetailsButtons');
        // create schedule button.
        this.addHeadingTextToVarianceType('unavailableTimesHeaderText',getMessage("UnavailableTimes"),getMessage('unavailableHelpText'));
        var unavilableTimesGrid = this.getScheduleButtonsGrid(['HOLIDAY','VACATION','SICK LEAVE','PERSONAL','OTHER'],'unavailableTimesGrid');
        unavilableTimesGrid.showColumn('varianceType',false);
        unavilableTimesGrid.build();
        
        this.addHeadingTextToVarianceType('availableTimesHeaderText',getMessage('AvailableTimes'),getMessage("availableHelpText"));
        var availableTimesGrid = this.getScheduleButtonsGrid(['STANDARD','OVERTIME','DOUBLETIME'],'availableTimesGrid');
        availableTimesGrid.showColumn('varianceType',false);
        availableTimesGrid.build();
        
        this.addHeadingTextToVarianceType('siteVisitsHeaderText',getMessage('SiteVisits'),getMessage('siteVisitHelpText'));
        var siteVisitGrid = this.getScheduleButtonsGrid(['ON SITE'],'siteVisitsGrid');
        siteVisitGrid.showColumn('varianceType',false);
        siteVisitGrid.build();

        this.setVarianceTypeGridLegendColor('unavailableTimesGrid');
        this.setVarianceTypeGridLegendColor('availableTimesGrid');
        this.setVarianceTypeGridLegendColor('siteVisitsGrid');
        this.setVarianceTypeAddButtonsBgColor();
    },
    
    /**
     * Set legend color and assign button background color after grid build.
     * @param {gird} Rate Type Grid.
     */
    setVarianceTypeGridLegendColor: function(panelId) {
        var me = this;
        var gridRowsItems = View.panels.get(panelId).gridRows.items;
        _.each(gridRowsItems, function(row) {
            // set background color for legend field
            me.setVarianceTypeLegendColor(row);
        });
    },
    
    /**
     * Set variance type grid legend color
     */
    setVarianceTypeLegendColor: function(row){
        var varianceType = row.record['varianceType'];
        var legendColor = '';
        switch(varianceType){
            case 'HOLIDAY': legendColor=holidayColor;
            break;
            case 'VACATION': legendColor=vacationColor;
            break;
            case 'SICK LEAVE': legendColor=sickLeaveColor;
            break;
            case 'PERSONAL': legendColor=personalColor;
            break;
            case 'OTHER': legendColor=otherColor;
            break;
            case 'STANDARD': legendColor=standardRateColor;
            break;
            case 'OVERTIME': legendColor=overTimeRateColor;
            break;
            case 'DOUBLETIME': legendColor=doubleTimeRateColor;
            break;
            case 'ON SITE': legendColor=siteVistColor;
            break;
        }
        
        // get the id="legend" cell for this row (Ab.grid.Cell object)
        var cell = row.cells.get('legend');
        // set cell background color
        Ext.get(cell.dom).setStyle('background-color', legendColor);
        
        //set site visit legend border color.
        if(varianceType == 'ON SITE'){
            Ext.get(cell.dom).setStyle('border-style', 'solid');
            Ext.get(cell.dom).setStyle('border-width', '2px');
            Ext.get(cell.dom).setStyle('border-color', 'orange');
        }
    },
    
    /**
     * set assign button backgound color.
     */
    setVarianceTypeAddButtonsBgColor: function() {
        var unavailableTimesGrid = View.panels.get('unavailableTimesGrid');
        this.setVarianceTypeGridAddButtonColor(unavailableTimesGrid.gridRows);
        
        var availableTimesGrid = View.panels.get('availableTimesGrid');
        this.setVarianceTypeGridAddButtonColor(availableTimesGrid.gridRows);
        
        var siteVisitsGrid = View.panels.get('siteVisitsGrid');
        this.setVarianceTypeGridAddButtonColor(siteVisitsGrid.gridRows);
    },
    
    /**
     * set background color to add button in variance type grid.
     * @param gridRows
     */
    setVarianceTypeGridAddButtonColor: function(gridRows){
        var me=this;
        var rowItems = gridRows.items;
        _.each(rowItems, function(row) {
            var rateType = row.record['varianceType'];
            if (rateType == me.selectedScheduleDetailType) {
                var cell = row.cells.get('addButton').dom.childNodes[0].style.backgroundColor = "#FF6633";
            } else {
                var cell = row.cells.get('addButton').dom.childNodes[0].style.backgroundColor = "";
            }
        });
    },
    
    /**
     * Get grid control for unavailable times.
     * @param {varianceTypes}
     */
    getScheduleButtonsGrid: function(varianceTypes,gridPanelDivId){
        var rows = [];
        
        for(var i=0;i<varianceTypes.length;i++){
            rows[i] = new Object();
            rows[i]['varianceType'] =varianceTypes[i];
            rows[i]['varianceType.text'] =this.cfScheduleVariancesDs.formatValue('cf_schedules_variances.sched_vars_type',varianceTypes[i]);
        }

        var columns = [ 
                        new Ab.grid.Column('varianceType.text', getMessage('varianceType'), 'text'), 
                        new Ab.grid.Column('varianceType', getMessage('varianceType'), 'text'), 
                        new Ab.grid.Column('legend', getMessage('legend'), 'text'), 
                        new Ab.grid.Column('addButton', '', 'button', this.onClickVarianceTypeAddButton.createDelegate()) 
                      ];
        columns[3].text = getMessage('add');
        var configObj = new Ab.view.ConfigObject();
        configObj['rows'] = rows;
        configObj['columns'] = columns;

        // create new Grid component instance
        var grid = new Ab.grid.ReportGrid(gridPanelDivId, configObj);
        return grid;
    },
    
    /**
     * Event handler of clicking variance type add button.
     */
    onClickVarianceTypeAddButton: function(row){
        var varianceType=row.row.record['varianceType'];
        scheduleDetailsCtrl.selectedScheduleDetailType=varianceType;
        scheduleDetailsCtrl.setVarianceTypeAddButtonsBgColor();
    },

    /**
     * Add schedule details buttons.
     */
    addScheduleButton: function(containerElement, buttonId, buttonText, buttonColor) {
        var vacationButton = document.createElement("div");
        vacationButton.setAttribute('id', buttonId);
        vacationButton.setAttribute('class', 'scheduleButton');
        vacationButton.style.backgroundColor = buttonColor;
        vacationButton.setAttribute("onclick", "onClickScheduleButton(this)");
        if (buttonId == 'ON_SITE') {
            vacationButton.style.borderStyle = 'solid';
            vacationButton.style.borderWidth = '2px';
            vacationButton.style.borderColor = 'orange';
            //vacationButton.style.marginTop = '40px';
            vacationButton.style.fontWeight = 'bold';
        }
        var vacationText = document.createTextNode(buttonText);
        vacationButton.appendChild(vacationText);
        containerElement.appendChild(vacationButton);
    },
    
    /**
     * Add heading text to schedule variance type.
     */
    addHeadingTextToVarianceType: function(id,text,helpText){
        var container=jQuery('#'+id+'');
        var helpTextElement=jQuery("<div>").addClass("varianceTypeHelpText").text(text);
        var helpIconImgElement=jQuery("<img>").attr('title',helpText).attr("src","/archibus/schema/ab-core/graphics/icons/help.png").bind("click",function(){
            View.alert(helpText);
        }).appendTo(helpTextElement);
        helpTextElement.appendTo(container);
    },

    /**
     * Add new schedule variance.
     * @return {cfScheduleVarsId|string} The primary key of saved record.
     */
    addNewScheduleVariance: function(appointmentData) {
        var scheduleVarianceRecord = new Ab.data.Record();
        scheduleVarianceRecord.isNew = true;
        scheduleVarianceRecord.oldValues = new Object();
        scheduleVarianceRecord.setValue('cf_schedules_variances.cf_id', appointmentData.cfId);
        var startDate = getIsoFormatDate(appointmentData.startDate);
        scheduleVarianceRecord.setValue('cf_schedules_variances.date_start', startDate);
        var endDate = getIsoFormatDate(appointmentData.endDate);
        scheduleVarianceRecord.setValue('cf_schedules_variances.date_end', endDate);
        scheduleVarianceRecord.setValue('cf_schedules_variances.time_start', appointmentData.startDate);
        scheduleVarianceRecord.setValue('cf_schedules_variances.time_end', appointmentData.endDate);
        scheduleVarianceRecord.setValue('cf_schedules_variances.sched_vars_type', this.getAppointmentDataValue(appointmentData.scheduledVarType));
        scheduleVarianceRecord.setValue('cf_schedules_variances.description', this.getAppointmentDataValue(appointmentData.description));
        if (appointmentData.scheduledVarType == 'ON SITE') {
            scheduleVarianceRecord.setValue('cf_schedules_variances.site_id', this.getAppointmentDataValue(appointmentData.siteId));
            scheduleVarianceRecord.setValue('cf_schedules_variances.bl_id', this.getAppointmentDataValue(appointmentData.blId));
        }

        var savedRecord = this.cfScheduleVariancesDs.saveRecord(scheduleVarianceRecord);
        var cfScheduleVarsId = savedRecord.getValue('cf_schedules_variances.cf_sched_vars_id');
        return cfScheduleVarsId;
    },

    /**
     * Get appointment data value to avoid undefined value.
     */
    getAppointmentDataValue: function(value) {
        var result = value;
        if (!valueExistsNotEmpty(value)) {
            result = "";
        }
        return result;
    },
    /**
     * Delete schedule variance record by primary key.
     * @param {cfScheduleVarsId} Primary key.
     */
    deleteScheduleVariance: function(cfScheduleVarsId) {
        var res = new Ab.view.Restriction();
        res.addClause('cf_schedules_variances.cf_sched_vars_id', cfScheduleVarsId, '=');
        var scheduledVarianceRecord = this.cfScheduleVariancesDs.getRecord(res);
        this.cfScheduleVariancesDs.deleteRecord(scheduledVarianceRecord);
    },

    /**
     * Update schedule variances by appointment data.
     */
    updateScheduleVariance: function(appointmentData) {
        var cfScheduleVarsId = appointmentData.serverId;
        var res = new Ab.view.Restriction();
        res.addClause('cf_schedules_variances.cf_sched_vars_id', cfScheduleVarsId, '=');
        var scheduleVarsRecord = this.cfScheduleVariancesDs.getRecord(res);
        var startDateFormatted = getIsoFormatDate(appointmentData.startDate);
        scheduleVarsRecord.setValue('cf_schedules_variances.date_start', startDateFormatted);
        var endDateFormatted = getIsoFormatDate(appointmentData.endDate);
        scheduleVarsRecord.setValue('cf_schedules_variances.date_end', endDateFormatted);
        scheduleVarsRecord.setValue('cf_schedules_variances.time_start', appointmentData.startDate);
        scheduleVarsRecord.setValue('cf_schedules_variances.description', appointmentData.description);
        scheduleVarsRecord.setValue('cf_schedules_variances.time_end', appointmentData.endDate);
        scheduleVarsRecord.setValue('cf_schedules_variances.cf_id', this.getAppointmentDataValue(appointmentData.cfId));
        scheduleVarsRecord.setValue('cf_schedules_variances.sched_vars_type', this.getAppointmentDataValue(appointmentData.scheduledVarType));
        if (appointmentData.scheduledVarType == 'ON SITE') {
            scheduleVarsRecord.setValue('cf_schedules_variances.site_id', this.getAppointmentDataValue(appointmentData.siteId));
            scheduleVarsRecord.setValue('cf_schedules_variances.bl_id', this.getAppointmentDataValue(appointmentData.blId));
        }

        this.cfScheduleVariancesDs.saveRecord(scheduleVarsRecord);
    }

});

/**
 * Change variance type field value in console form.
 * @param value
 * @returns
 */
function changeScheduleVarianceType(value){
    var consolePanel=View.panels.get('consoleForm');
    consolePanel.setFieldValue('cf_schedules_variances.site_id','');
    consolePanel.setFieldValue('cf_schedules_variances.bl_id','');
    var showLocationFields=(value=='ON SITE')?true:false;
    this.showConsoleFormLocationFields(showLocationFields);
}

/**
 * Show or hidden site code and building code in console form based on the selected value of variance type.
 * @param isShow
 * @returns
 */
function showConsoleFormLocationFields(isShow){
    if(isShow){
        $('consoleForm_cf_schedules_variances.site_id').style.display = "";
        $('consoleForm.cf_schedules_variances.site_id_selectValue').style.display = "";
        $('consoleForm_cf_schedules_variances.bl_id').style.display = "";
        $('consoleForm.cf_schedules_variances.bl_id_selectValue').style.display = "";
    }else{
        $('consoleForm_cf_schedules_variances.site_id').style.display = "none";
        $('consoleForm.cf_schedules_variances.site_id_selectValue').style.display = "none";
        $('consoleForm_cf_schedules_variances.bl_id').style.display = "none";
        $('consoleForm.cf_schedules_variances.bl_id_selectValue').style.display = "none";
    }
}