var shiftManagerCtrl = View.createController('shiftManagerCtrl', {
    /**
     * Selected Schedule Code.
     */
    selectedScheduleId: null,
    
    /**
     * Register child tab controllers.
     */
    tabCtrls: {},
    
    afterViewLoad: function(){
        this.on('bldgops:craftsperson:manage:shifts:afterUpdatedShift',this.afterUpdatedSelectedShift);
        this.on('bldgops:craftsperson:manage:shifts:afterDeletedShift',this.afterDeletedShift)
        var tabsPanel = View.panels.get('cfShiftsTabs');
        tabsPanel.addEventListener('afterTabChange', this.afterTabChange.createDelegate(this));
    },


    /**
     * Click shift list items.
     */
    onSelectShift: function() {
        var selectedRowIndex = this.shiftsListPanel.selectedRowIndex;
        var selectedRowRecord = this.shiftsListPanel.gridRows.get(selectedRowIndex).getRecord();
        var scheduleId = selectedRowRecord.getValue('cf_schedules.cf_schedule_id');
        var scheduleName=selectedRowRecord.getValue('cf_schedules.schedule_name');
        this.shiftTitlePanel.setTitle(getMessage('editShiftTitle').replace('{0}',scheduleName));
        this.afterSelectedShift(scheduleId);
    },
    
    /**
     * Refresh panels after update/add selected shift
     */
    afterUpdatedSelectedShift: function(scheduleId,scheduleName){
        this.shiftsListPanel.refresh();
        this.shiftTitlePanel.setTitle(getMessage('editShiftTitle').replace('{0}',scheduleName));
        this.afterSelectedShift(scheduleId);
    },
    
    /**
     * Refresh shift list after deleted shift.
     */
    afterDeletedShift: function(){
        this.shiftsListPanel.refresh();
        this.switchToAddNewMode();
    },
    
    /**
     * Do after selected shift.
     */
    afterSelectedShift: function(scheduleId){
        this.selectedScheduleId = scheduleId;
        this.cfShiftsTabs.selectTab('shiftCraftspersonsTab');
        var craftspersonsTab=this.cfShiftsTabs.findTab('shiftCraftspersonsTab');
        if(craftspersonsTab.isContentLoaded){
            this.tabCtrls['shiftCraftspersonsTab'].selectedShift(scheduleId);  
        }
    },
    
    /**
     * after tab change.
     */
    afterTabChange: function(tabPanel, newTabName){
        if(newTabName=='shiftDailyScheduleTab'){
            var currentTab = tabPanel.findTab(newTabName);
            if (currentTab.isContentLoaded) {
                var controller=this.tabCtrls[newTabName];
                controller.selectedShift(this.selectedScheduleId);
            }
        }
    },
    
    /**
     * Add new shift.
     */
    shiftsListPanel_onAddNew: function(){
        this.switchToAddNewMode();
    },
    
    /**
     * Switch to add new mode.
     */
    switchToAddNewMode: function(){
        this.selectedScheduleId=null;
        this.cfShiftsTabs.selectTab('shiftDailyScheduleTab');
        this.cfShiftsTabs.enableTab('shiftCraftspersonsTab',false);
        if(this.tabCtrls['shiftDailyScheduleTab']){
            this.tabCtrls['shiftDailyScheduleTab'].selectedShift(null);
        }
        //set title 
        this.shiftTitlePanel.setTitle(getMessage('addShiftTitle'));
    }

});