/**
 * Schedule controller that used by Manage Craftsperson shift view.
 * @author Jia
 */
var ShiftScheduleControl = BaseScheduleControl.extend({
    /**
     * @override Constructor.
     * @param {config} The control configuration.
     */
    constructor: function(config) {
        this.inherit(config);
    }
});