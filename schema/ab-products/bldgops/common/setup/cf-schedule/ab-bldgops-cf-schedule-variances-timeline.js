/**
 * The Labor Board control implements https://confluence.archibus.zone/display/PLAT/HTML+Scheduling+Control.
 */
TimeLineScheduleControl = Base.extend({
    /**
     * First day of week, by default is SUNDAY.
     */
    firstDayOfWeek: 1,
    /**
     * The configuration object.
     */
    config: null,

    /**
     * The dxScheduler control instance.
     */
    scheduler: null,

    /**
     * Restriction for the schedule.
     */
    restriction: null,

    /**
     * Schedule type resources data.
     */
    resources: [],

    /**
     * Craftspersons data.
     */
    craftspersons: [],

    /**
     * Schedule variances data.
     */
    scheduleVariances: [],

    /**
     * Opener view controller.
     */
    openerViewController: null,

    /**
     * Site array,used to show site list in Site Code field of appointment edit pop-up form.
     */
    siteList: [],
    /**
     * Building array,used to show site list in Building Code field of appointment edit pop-up form.
     */
    buildingList: [],

    /**
     * timeline view start date.
     */
    viewStartDate: null,

    /**
     * timeline view end date.
     */
    viewEndDate: null,

    /**
     * console form record in opener view console form.
     */
    openerViewConsoleRecord: null,
    /**
     * Start Day Hour.
     */
    startDayHour:0,
    /**
     * End Day Hour.
     */
    endDayHour:24,
    /**
     * Cell Duration.
     */
    cellDuration:2*60,

    /**
     * Control constructor.
     * @param {config} The control configuration.
     */
    constructor: function(config) {
        this.config = config;
        this.model = this.config.model;
        this.resources = [];
        this.craftspersons = [];
        this.scheduleVariances = [];
        this.openerViewController = this.config.openerController;
        // initialize the timeline control.
        this.initializeScheduler();
    },

    /**
     * Sets the control height to specified value.
     * @param {height} The height in pixels.
     */
    setHeight: function(height) {
    },

    /**
     * Adds a listener to specified control event.
     * @param {eventName} The name of the control event, e.g. 'click' or 'locateBuilding'.
     * @param {listener} The listener function reference.
     */
    addEventListener: function(eventName, listener) {
    },

    /**
     * Removes listeners from specified control event.
     * @param {eventName} The name of the control event, e.g. 'click' or 'locateBuilding'.
     * @param {listener} The listener function reference. Optional. If not specified, the control removes all event listeners for this event.
     */
    removeEventListener: function(eventName) {
    },

    /**
     * Initializes the dxScheduler control with customizations required for the Labor Scheduling Board.
     */
    initializeScheduler: function() {
        var control = this;
        var scheduler = jQuery("#workScheduleContainer").dxScheduler({
            dataSource: control.scheduleVariances,
            views: [ "timelineWorkWeek", "timelineWeek", "timelineDay", "timelineMonth" ],
            currentView: "timelineWorkWeek",
            currentDate: new Date(),
            firstDayOfWeek: control.firstDayOfWeek,
            startDayHour: control.startDayHour,
            endDayHour: control.endDayHour,
            cellDuration: control.cellDuration,
            // enable vertical scroll bar.
            crossScrollingEnabled: true,
            groups: [ "cfId" ],
            resources: [ {
                field: "scheduledVarType",
                allowMultiple: false,
                dataSource: control.resources,
                label: "Schedules Variance Type",
                useColorAsDefault: true
            }, {
                field: "cfId",
                allowMultiple: false,
                dataSource: control.craftspersons,
                label: "Craftsperson Code"
            } ],
            width: "100%",
            height: "calc(100% - 10px)",

            // display the secondary time scale (hours) as vertical text based on user's locale.
            timeCellTemplate: function(itemData, itemIndex, itemElement) {
                //APP-1468:Use the locale to determine which format to display time with U.S.format or 24-hours format.
                var formattedTime=itemData.text.replace('AM', 'a').replace('PM', 'p');
                itemElement.parent().height(50);
                jQuery("<div>").text(formattedTime).css("transform", "rotate(-90deg)").appendTo(itemElement);
                
                if (itemData.date.getHours() === 8) {
                    itemElement.addClass('first-hour');
                }
            },
            
            appointmentTooltipTemplate: function(data, container) {
                return control.getTooltipTemplate(data);
            },
            
            // add unscheduled hours to each resource row
            resourceCellTemplate: function(cellData, index, container) {
                container.css("height",'100px');
                container.append(jQuery("<div>").text(cellData.id));
            },
            /**
             * The template to be used for rendering appointments.
             * @param {itemData} The appointment object to be rendered.
             * @param {itemIndex}
             * @param {itemElement}
             */
            appointmentTemplate: function(itemData, itemIndex, itemElement) {
                
                // Regular schedule time blocks should not show the text.
                if (itemData.scheduleType !== "regular") {
                    var formattedVarianceType= View.dataSources.get('cfScheduleVariancesDs').formatValue('cf_schedules_variances.sched_vars_type',itemData.scheduledVarType);
                    var content = formattedVarianceType;
                    // time block show the Site Code and Building Code when schedule variance type is 'ON SITE'.
                    if (itemData.scheduledVarType == 'ON SITE') {
                        //JIRA:APP-909:avoid site visit time block label overlap with other labels.
                        itemElement.css('padding-top',30);
                        var siteId = valueExistsNotEmpty(itemData.siteId) ? itemData.siteId : "";
                        var blId = valueExistsNotEmpty(itemData.blId) ? itemData.blId : "";
                        content = siteId + '-' + blId;
                    }
                    return jQuery('<div>' + content + '</div>');
                }
            },
            /**
             * resourceCellTemplate: function(cellData, index, container) { container.css('height', 76); var cfId=cellData.data.id; container.append(jQuery('<div>').text(cfId)); },
             */

            /**
             * Triggered during appointment adding.
             * @param event
             */
            onAppointmentAdding: function(event) {
                // format schedule type from view selected schedule type to database schedule variance type.
                if (!valueExistsNotEmpty(event.appointmentData['scheduledVarType'])) {
                    event.appointmentData['scheduledVarType'] = control.openerViewController.selectedScheduleDetailType;
                }
                var siteId = event.appointmentData.siteId;
                //APP-1425:To workaround fix issue that cannot get site code from appointmentData after selcted a building when add new appointment.
                if(!siteId){
                    var blId = event.appointmentData.blId;
                    if(blId){
                        event.appointmentData.siteId = control.getSiteIdByBuildingCode(blId);
                    }
                }
                // validate appointment.
                var validateResult = control.validateAppointmentData(event.appointmentData);
                if (!validateResult) {
                    event.cancel = true;
                } else {
                    // Save new appointment data and set returned primary key to current appointment.
                    var serverId = control.openerViewController.addNewScheduleVariance(event.appointmentData);
                    event.appointmentData['serverId'] = serverId;
                }
            },

            /**
             * Triggered during appointment deleting. to delete appointment from server side database.
             * @param event
             */
            onAppointmentDeleting: function(event) {
                // delete the appointment in database.
                control.openerViewController.deleteScheduleVariance(event.appointmentData.serverId);
            },

            /**
             * Triggered during appointment updating.
             * @param event
             */
            onAppointmentUpdating: function(event) {
                //clear building code if the site of the selected building is not the selected site.
                //use this approach fix third-party scheduler control can not clear appointment data fields issue.
                var newBlId = event.newData.blId;
                if(newBlId){
                    siteIdOfBuilding = control.getSiteIdByBuildingCode(newBlId);
                    if(siteIdOfBuilding!==event.newData.siteId){
                        event.newData.blId='';
                    }
                }
                // Validate whether current appointment overlap with other appointments.
                var validateResult = control.validateAppointmentData(event.newData);
                if (!validateResult) {
                    event.cancel = true;
                }
            },
            /**
             * triggered after appointment updated. to update appointment on server side database.
             * @param event
             */
            onAppointmentUpdated: function(event) {
                // update the appointment in database.
                control.openerViewController.updateScheduleVariance(event.appointmentData);
            },

            /**
             * re-load schedule data after currentDate and currentView options changed.
             * @param event
             */
            onOptionChanged: function(event) {
                if (event.name == 'currentDate') {
                    // re-load schedule after current date changed.
                    control.loadSchedule(event.value, control.scheduler._options.currentView);
                }
                if (event.name == 'currentView') {
                    // re-load schedule after current view changed.
                    control.loadSchedule(control.viewStartDate, event.value);
                }
            },

            /**
             * Triggered after appointment rendered, to set appointment UI style.
             */
            onAppointmentRendered: function(event) {
                var appointmentText=event.appointmentData.text;
                var formattedDate=control.formatTooltipDate(event.appointmentData.startDate,event.appointmentData.endDate,false);
                
                if (event.appointmentData.scheduleType == 'regular') {
                    event.appointmentElement[0].style['opacity'] = 0.1;
                    // Use z-index property to make regular schedule as background data.
                    jQuery(event.appointmentElement)[0].style['z-index'] = 50;
                } else {
                    jQuery(event.appointmentElement).height('70px');

                    if (event.appointmentData.scheduledVarType == 'ON SITE') {
                        //help text
                        appointmentText=getMessage(getMessage('SiteVisit')+': '+event.appointmentData.siteId+ '-' +event.appointmentData.blId);
                        
                        jQuery(event.appointmentElement).height('60px');
                        var appointmentElement = jQuery(event.appointmentElement)[0];
                        appointmentElement.style['border-color'] = 'orange';
                        appointmentElement.style['border-style'] = 'solid';
                        appointmentElement.style['color'] = 'black';
                        // let ON SITE schedules at the top of other schedules.
                        appointmentElement.style['z-index'] = 90;
                    } else if (event.appointmentData.scheduledVarType == 'BUSY') {
                        jQuery(event.appointmentElement)[0].style['z-index'] = 85;
                        jQuery(event.appointmentElement).height('65px');
                    } else {
                        jQuery(event.appointmentElement)[0].style['z-index'] = 60;
                    }
                }
                
                //show help text with formatted date when mouse hover the appointment.
                event.appointmentElement.prop('title', appointmentText+' ('+formattedDate+')');
            },

            /**
             * Define a custom task edit form.
             * @param data
             */
            onAppointmentFormCreated: function(data) {
                var form = data.form, task = data.appointmentData;
                var appointmentData = data.appointmentData;
                if(data.appointmentData.siteId){
                    control.loadBuildingData(data.appointmentData.siteId);
                }else{
                    control.loadBuildingData();
                }
                var scheduledVarType = task.scheduledVarType;
                if (!valueExistsNotEmpty(scheduledVarType)) {
                    scheduledVarType = control.openerViewController.selectedScheduleDetailType;
                }
                
                
                form.option("items", [ {
                    label: {
                        text: getMessage('scheduleVarianceType')
                    },
                    name: "scheduledVarType",
                    editorType: "dxSelectBox",
                    editorOptions: {
                        value: scheduledVarType,
                        readOnly: false,
                        items: control.getVarianceTypesByResources(),
                        displayExpr: 'text',
                        valueExpr: "id",
                        onItemClick: function(data) {
                            control.showLocationFields(form, data.itemData.id);
                        }
                    }

                }, {
                    label: {
                        text: getMessage('site')
                    },
                    name: "siteId",
                    editorType: "dxLookup",
                    visible: true,
                    editorOptions: {
                        displayExpr: 'text',
                        valueExpr: "id",
                        dataSource: control.siteList,
                        value: task.siteId,
                        searchPlaceholder: getMessage('search'),
                        placeholder: getMessage('select'),
                        cancelButtonText: getMessage('cancel'),
                        readOnly: false,
                        onItemClick: function(item) {
                            var data = task;
                            control.loadBuildingData(item.itemData.id);
                            var buildingIdOptions = form.itemOption("blId");
                            var siteIdOptions = form.itemOption("siteId");
                            siteIdOptions.editorOptions.value=item.itemData.id;
                            buildingIdOptions.editorOptions.dataSource=control.buildingList;
                            buildingIdOptions.editorOptions.value='';
                            form.itemOption("blId",buildingIdOptions);
                            form.itemOption("siteId",siteIdOptions);
                        }
                    },
                }, {
                    label: {
                        text: getMessage('building')
                    },
                    name: "blId",
                    editorType: "dxLookup",
                    visible: true,
                    editorOptions: {
                        displayExpr: 'text',
                        valueExpr: "id",
                        searchPlaceholder: getMessage('search'),
                        placeholder: getMessage('select'),
                        cancelButtonText: getMessage('cancel'),
                        dataSource: control.buildingList,
                        value: task.blId,
                        readOnly: false,
                        onItemClick: function(item) {
                            var data=task;
                            control.loadBuildingData(item.itemData.siteId);
                            var buildingIdOptions = form.itemOption("blId");
                            var siteIdOptions = form.itemOption("siteId");
                            siteIdOptions.editorOptions.value=item.itemData.siteId;
                            
                            buildingIdOptions.editorOptions.dataSource=control.buildingList;
                            buildingIdOptions.editorOptions.value=item.itemData.id;
                            form.itemOption("blId",buildingIdOptions);
                            form.itemOption("siteId",siteIdOptions);
                        }
                    },
                }, {
                    label: {
                        text: getMessage('description')
                    },
                    name: "description",
                    editorType: "dxTextArea",
                    editorOptions: {
                        height: '150px',
                        value: valueExistsNotEmpty(task.description)?task.description:'',
                        readOnly: false
                    },
                }, {
                    label: {
                        text: getMessage('startDate')
                    },
                    name: "startDate",
                    dataField: "startDate",
                    editorType: "dxDateBox",
                    editorOptions: {
                        applyButtonText: getMessage('done'),
                        cancelButtonText: getMessage('cancel'),
                        value: task.startDate,
                        type: "datetime",
                        onOpened: function(e){
                            //APP-2935:make Today button in dxDateBox control pop-up greekable.
                            jQuery('span:contains("Today")').text(getMessage('today'));
                        }
                    }
                }, {
                    label: {
                        text: getMessage('endDate')
                    },
                    name: "endDate",
                    dataField: "endDate",
                    editorType: "dxDateBox",
                    showTodayButton:false,
                    editorOptions: {
                        applyButtonText: getMessage('done'),
                        cancelButtonText: getMessage('cancel'),
                        value: task.endDate,
                        type: "datetime",
                        onOpened: function(e){
                            //APP-2935:make Today button in dxDateBox control pop-up greekable.
                            jQuery('span:contains("Today")').text(getMessage('today'));
                        }
                    }
                }, {
                    label: {
                        text: getMessage('craftsperson')
                    },
                    editorType: "dxSelectBox",
                    dataField: 'cfId',
                    editorOptions: {
                        items: control.craftspersons,
                        displayExpr: 'text',
                        valueExpr: "id",
                        value: task.cfId,

                    }
                } ]);
                // set field validation property.
                form.itemOption("scheduledVarType", {
                    validationRules: [ {
                        type: 'required',
                        message: getMessage('scheduleVarianceTypeIsRequiredMessage')
                    } ]
                });
                form.itemOption("cfId", {
                    validationRules: [ {
                        type: 'required',
                        message: getMessage('craftspersonIsRequiredMessage')
                    } ]
                });
                form.itemOption("startDate", {
                    validationRules: [ {
                        type: 'required',
                        message: getMessage('startDateIsRequiredMessage')
                    } ]
                });
                form.itemOption("endDate", {
                    validationRules: [ {
                        type: 'required',
                        message: getMessage('endDateIsRequiredMessage')
                    } ]
                });

                control.showLocationFields(form, scheduledVarType);
                
                //change buttons text to use ARCHIBUS translatable message.
                var buttons = data.component._popup.option('buttons');  
                if(buttons.length == 2){
                    buttons[0].options = { text: getMessage('done') };
                    buttons[1].options = { text: getMessage('cancel') };
                    data.component._popup.option('buttons', buttons);
                }    
                
            }

        }).dxScheduler("instance");

        jQuery('.dx-scheduler-view-switcher').hide();
        jQuery('.dx-scheduler-view-switcher-label').hide();

        this.scheduler = scheduler;

        // overide core prototype to allow appointments overlapping top and lower,instead of side by side.
        Object.getPrototypeOf(this.scheduler._appointments._renderingStrategy)._customizeAppointmentGeometry = function(coordinates) {
            var cellHeight = (this._defaultHeight || this.getAppointmentMinSize()) - 20, height = cellHeight;
            if (height > 100) {
                height = 100
            }
            var top = coordinates.top;
            return {
                height: height,
                width: coordinates.width,
                top: top,
                left: coordinates.left
            }
        };
        
        //load site data and building data.
        this.loadSiteData();
        this.loadBuildingData();
    },
    
    /**
     * Loads the schedule for specified list of resources with loading progress bar.
     */
    loadSchedule: function(startDate, currentView){
        View.openProgressBar();
        this.loadScheduleControl.defer(50,this,[startDate, currentView]);
    },

    /**
     * Loads the schedule for specified list of resources.
     */
    loadScheduleControl: function(startDate, currentView) {
        if (!currentView) {
            currentView = this.scheduler._options.currentView;
        }
        // set view start date and end date.
        this.setViewStartAndEndDate(startDate, currentView);

        // initial rate type data.
        this.loadTimelineResourceData();
        // initial craftspersons data.
        var craftspersonRes = this.getCraftspersonRestrictionFromConsoleRecord(this.openerViewConsoleRecord);
        this.loadCraftspersonsData(craftspersonRes);

        // clear schedule variances array.
        this.scheduleVariances.length = 0;
        // load regular schedule data if current view is not timelineMonth.
        if (currentView != 'timelineMonth') {
            var regularScheduleRes = this.getRegularScheduleRestrictionFromConsoleRecord(this.openerViewConsoleRecord);
            this.copyArrayObject(this.getRegularScheduleData(regularScheduleRes, this.viewStartDate, this.viewEndDate), this.scheduleVariances);
        }

        // initial schedule variances data, concat with regular schedule data.
        var scheduleVarianceRes = this.getScheduleVarianceRestrictionFromConsoleRecord(this.openerViewConsoleRecord);
        var scheduleVarianceList = [];
        if (currentView == 'timelineMonth') {
            var monthViewStartDate = new Date(this.viewStartDate.getFullYear(), this.viewStartDate.getMonth(), 1);
            scheduleVarianceList = this.getScheduleVarianceData(scheduleVarianceRes, monthViewStartDate, this.viewEndDate);
        } else {
            scheduleVarianceList = this.getScheduleVarianceData(scheduleVarianceRes, this.viewStartDate, this.viewEndDate);
        }
        this.copyArrayObject(scheduleVarianceList, this.scheduleVariances);

        // re-paint.
        this.scheduler.repaint();
        
        this.scheduler._$popup=null;
        this.scheduler._cleanPopup();
        
        this.appendSwitchDayHoursButton();

        jQuery('span:contains("Timeline Day")').text(getMessage('timelineDay'));
        jQuery('span:contains("Timeline Week")').text(getMessage('timelineWeek'));
        jQuery('span:contains("Timeline Work Week")').text(getMessage('timelineWorkWeek'));
        jQuery('span:contains("Timeline Month")').text(getMessage('timelineMonth'));

        View.closeProgressBar();
    },
    
    /**
     * Append switch day hours buttons to dxScheduler control navigator bar.
     */
    appendSwitchDayHoursButton: function() {
        var schedulerNavigator = jQuery(".dx-scheduler-navigator");
        this.getDayHoursButton(12).addClass('switchDayHoursButton').appendTo(schedulerNavigator);
        this.getDayHoursButton(24).addClass('switchDayHoursButton').appendTo(schedulerNavigator);
    },
    
    /**
     * Get switch day hours buttons.
     * @param {displayHours} Display Hours.
     */
    getDayHoursButton: function(displayHours) {
        var that = this;
        var text = "";
        if (displayHours == 12) {
            text = getMessage("12");
        }
        if (displayHours == 24) {
            text = getMessage("24");
        }
        var editButton = jQuery("<div>").dxButton({
            icon: "",
            text: getMessage(text),
            onClick: function() {
                View.openProgressBar();
                that.switchDayHours.defer(50,that,[displayHours]);
            }
        });

        return editButton;
    },

    /**
     * Switch scheduler display day hours.
     */
    switchDayHours: function(displayHours) {
        this.setDayHours(displayHours);
        this.scheduler.option({"startDayHour":this.startDayHour,"endDayHour":this.endDayHour,"cellDuration":this.cellDuration});
        View.closeProgressBar();
    },
    
    /**
     * Set start day hour and end day hour by display hours.
     * @param {displayHours} Per day display hours. 12 hours or 24 hours.
     */
    setDayHours: function(displayHours) {
        if (displayHours == 24) {
            this.startDayHour = 0;
            this.endDayHour = 24;
            this.cellDuration=2*60;
        }

        if (displayHours == 12) {
            this.defaultEstimateHours = 1;
            this.startDayHour = 8;
            this.endDayHour = 20;
            this.cellDuration=60;
        }
    },
    /**
     * set current date by console record.
     */
    getCurrentDateByConsoleRecord: function(consoleRecord) {
        var currentDate = consoleRecord.getValue('cf_schedules_variances.date_start');
        if (!valueExistsNotEmpty(currentDate)) {
            currentDate = this.scheduler.getStartViewDate();
        }

        this.scheduler.option('currentDate', currentDate);
        return currentDate;
    },

    /**
     * Set view start and end date. because sometimes {this.scheduler.getStartViewDate()} function return value is not correct view time start.
     * @param {startDate} Selected start date when changed the currentDate option.
     */
    setViewStartAndEndDate: function(startDate, currentView) {
        if (!startDate) {
            // this.viewStartDate = this.scheduler.getStartViewDate();
            this.viewStartDate = this.getCurrentDateByConsoleRecord(this.openerViewConsoleRecord);
        } else {
            this.viewStartDate = startDate;
        }

        if (currentView == 'timelineDay') {
            this.viewEndDate = this.viewStartDate.add(Date.DAY, 1);
        }

        // get current view date by first day of week,because event.value is not the view start date if current view is timelineweek or timelineworkweek.
        if (currentView == 'timelineWeek' || currentView == 'timelineWorkWeek') {
            this.viewStartDate = this.getStartDateOfViewByCurrentDate(this.viewStartDate,this.firstDayOfWeek);
            // set end date.
            if (currentView == 'timelineWorkWeek') {
                this.viewEndDate = this.viewStartDate.add(Date.DAY, 4);
            }
            if (currentView == 'timelineWeek') {
                this.viewEndDate = this.viewStartDate.add(Date.DAY, 6);
            }
        }

        // Fix issue that after startViewDate changed, month view can not get data between month start to current start view date.
        if (currentView == 'timelineMonth') {
            // this.viewStartDate=new Date(this.viewStartDate.getFullYear(),this.viewStartDate.getMonth(),1);
            this.viewEndDate = this.viewEndDate.add(Date.DAY, 31);
        }
    },

    /**
     * Initialized rate type data.
     */
    loadTimelineResourceData: function() {
        this.resources.length = 0;
        this.resources.push({
            id: 'HOLIDAY',
            text: getMessage('Holiday'),
            color: holidayColor
        });
        this.resources.push({
            id: 'SICK LEAVE',
            text: getMessage('SickLeave'),
            color: sickLeaveColor
        });
        this.resources.push({
            id: 'PERSONAL',
            text: getMessage('Personal'),
            color: personalColor
        });
        this.resources.push({
            id: 'VACATION',
            text: getMessage('Vacation'),
            color: vacationColor
        });
        this.resources.push({
            id: 'ON SITE',
            text: getMessage('OnSite'),
            color: siteVistColor
        });
        this.resources.push({
            id: 'BUSY',
            text: getMessage('Busy'),
            color: busyColor
        });
        this.resources.push({
            id: 'OTHER',
            text: getMessage('Other'),
            color: otherColor
        });
        this.resources.push({
            id: 'STANDARD',
            text: getMessage('Standard'),
            color: standardRateColor
        });
        this.resources.push({
            id: 'OVERTIME',
            text: getMessage('OverTime'),
            color: overTimeRateColor
        });
        this.resources.push({
            id: 'DOUBLETIME',
            text: getMessage('DoubleTime'),
            color: doubleTimeRateColor
        });
    },

    /**
     * Get variances items from time line resources.
     */
    getVarianceTypesByResources: function() {
        var varianceItems = [];
        for (var i = 0; i < this.resources.length; i++) {
            if (this.resources[i].id !== 'BUSY') {
                varianceItems.push(this.resources[i]);
            }
        }

        return varianceItems;
    },

    /**
     * Show or hide location field.
     */
    showLocationFields: function(form, scheduleVarType) {
        // show or hide Site Code field and Building Code field.
        var siteField = form.getEditor('siteId');
        var blField = form.getEditor('blId');

        if (scheduleVarType == 'ON SITE') {
            jQuery(siteField._$element).parent().parent().parent().show();
            jQuery(blField._$element).parent().parent().parent().show();
        } else {
            jQuery(siteField._$element).parent().parent().parent().hide();
            jQuery(blField._$element).parent().parent().parent().hide();
        }
    },

    /**
     * Get restriction for craftsperson from opener view console form record.
     */
    getCraftspersonRestrictionFromConsoleRecord: function(consoleFormRecord) {
        var restriction = new Ab.view.Restriction();
        if (valueExistsNotEmpty(consoleFormRecord)) {
            var cfId = consoleFormRecord.getValue('cf_schedules_variances.cf_id');
            if (valueExistsNotEmpty(cfId)) {
                restriction.addClause('cf.cf_id', cfId, '=');
            }
        }
        return restriction;
    },

    /**
     * Initialize craftspersons data.
     */
    loadCraftspersonsData: function(restriction) {
        var me = this;
        me.craftspersons.length = 0;
        var i = 0;
        var craftspersonsDsName = me.config.craftspersonDataSourceId;
        var craftspersonsRecords = View.dataSources.get(craftspersonsDsName).getRecords(restriction);
        _.each(craftspersonsRecords, function(record) {
            var cfId = record.getValue('cf.cf_id');
            var cfName = record.getValue('cf.name');
            me.craftspersons.push({
                id: cfId,
                text: cfId
            });
        });
    },

    /**
     * Get schedule variance restriction from opener view console form record.
     */
    getScheduleVarianceRestrictionFromConsoleRecord: function(consoleRecord) {
        var restriction = new Ab.view.Restriction();
        if (valueExistsNotEmpty(consoleRecord)) {
            var cfId = consoleRecord.getValue('cf_schedules_variances.cf_id');
            var schedVarsType = consoleRecord.getValue('cf_schedules_variances.sched_vars_type');
            var siteId = consoleRecord.getValue('cf_schedules_variances.site_id');
            var blId = consoleRecord.getValue('cf_schedules_variances.bl_id');
            if (valueExistsNotEmpty(cfId)) {
                restriction.addClause('cf_schedules_variances.cf_id', cfId, '=');
            }
            if (valueExistsNotEmpty(schedVarsType)) {
                restriction.addClause('cf_schedules_variances.sched_vars_type', schedVarsType, '=');
            }
            if (valueExistsNotEmpty(siteId)) {
                restriction.addClause('cf_schedules_variances.site_id', siteId, '=');
            }
            if (valueExistsNotEmpty(blId)) {
                restriction.addClause('cf_schedules_variances.bl_id', blId, '=');
            }
        }

        return restriction;
    },
    /**
     * Load schedule variances data.
     */
    getScheduleVarianceData: function(restriction, startDate, endDate) {
        var me = this;
        var scheduleVariancesTemp = [];
        // me.scheduleVariances.length=0;
        var scheduleVarianceDsName = me.config.scheduleVariancesDataSourceId;

        var scheduleVarianceDs = View.dataSources.get(scheduleVarianceDsName);
        scheduleVarianceDs.addParameter('dateStart', getIsoFormatDate(startDate));
        scheduleVarianceDs.addParameter('dateEnd', getIsoFormatDate(endDate));
        scheduleVarianceDs.addParameter('hasDateRange', true);

        var scheduleVariancesRecords = scheduleVarianceDs.getRecords(restriction);

        _.each(scheduleVariancesRecords, function(record) {
            var cfScheduledVarsId = record.getValue('cf_schedules_variances.cf_sched_vars_id');
            var cfId = record.getValue('cf_schedules_variances.cf_id');
            var dateStart = record.getValue('cf_schedules_variances.date_start');
            var dateEnd = record.getValue('cf_schedules_variances.date_end');
            var timeStart = record.getValue('cf_schedules_variances.time_start');
            var timeEnd = record.getValue('cf_schedules_variances.time_end');
            var scheduledVarType = record.getValue('cf_schedules_variances.sched_vars_type');
            var localizedScheduledVarType = scheduleVarianceDs.formatValue('cf_schedules_variances.sched_vars_type',scheduledVarType);
            var description = record.getValue('cf_schedules_variances.description');
            var siteId = record.getValue('cf_schedules_variances.site_id');
            var blId = record.getValue('cf_schedules_variances.bl_id');
            var disable = false;
            if (scheduledVarType == 'BUSY') {
                disable = true;
            }
            scheduleVariancesTemp.push({
                serverId: cfScheduledVarsId,
                cfId: cfId,
                siteId: siteId,
                blId: blId,
                scheduledVarType: scheduledVarType,
                description: description,
                startDate: new Date(dateStart.getFullYear(), dateStart.getMonth(), dateStart.getDate(), timeStart.getHours(), timeStart.getMinutes(), timeStart.getSeconds()),
                endDate: new Date(dateEnd.getFullYear(), dateEnd.getMonth(), dateEnd.getDate(), timeEnd.getHours(), timeEnd.getMinutes(), timeEnd.getSeconds()),
                disabled: disable,
                // use text property to show the tooltip.
                text: localizedScheduledVarType
            });
        });

        return scheduleVariancesTemp;
    },

    /**
     * Get regular schedule restriction from console record.
     * @param {consoleRecord} Console form record in opener view.
     */
    getRegularScheduleRestrictionFromConsoleRecord: function(consoleRecord) {
        var restriction = new Ab.view.Restriction();
        if (valueExistsNotEmpty(consoleRecord)) {
            var cfId = consoleRecord.getValue('cf_schedules_variances.cf_id');
            if (valueExistsNotEmpty(cfId)) {
                restriction.addClause('cf_schedules_assign.cf_id', cfId, '=');
            }
        }
        return restriction;
    },

    /**
     * Load regular schedule data.
     */
    getRegularScheduleData: function(restriction, startDate, endDate) {
        var me = this;
        var regularSchedules = [];
        // schedule assigns records.
        var scheduleAssignsRecords = View.dataSources.get(me.config.scheduleAssignDataSourceId).getRecords(restriction);
        // schedule days records.
        var scheduleDaysRecords = View.dataSources.get(me.config.scheduleDaysDataSourceId).getRecords();

        _.each(this.craftspersons,function(craftsperson){
            for(date=startDate;date.getTime()<=endDate.getTime();date=date.add(Date.DAY,1)){
                var cfId=craftsperson.id;
                //get applicable schedule assigned record.
                var appliedAssignRecord=me.getApplicableScheduleAssign(cfId,scheduleAssignsRecords,date);
                if(appliedAssignRecord){
                    var currentDayOfWeek=me.getDayOfWeekByNumber(date.getDay());
                    var scheduleDays=_.filter(scheduleDaysRecords,function(dayRecord){
                        if(dayRecord.getValue('cf_schedules_days.cf_schedule_id') == appliedAssignRecord.getValue('cf_schedules_assign.cf_schedule_id')){
                            return true;
                        }
                    });
                    
                    _.each(scheduleDays,function(dayRecord){
                        var dayOfWeek=dayRecord.getValue('cf_schedules_days.day_of_week');
                        if(dayOfWeek == currentDayOfWeek){
                            var timeStart = dayRecord.getValue('cf_schedules_days.time_start');
                            var timeEnd = dayRecord.getValue('cf_schedules_days.time_end');
                            var rateType = dayRecord.getValue('cf_schedules_days.rate_type');
                            
                            regularSchedules.push({
                                serverId: '',
                                cfId: cfId,
                                // To identify the difference between regular schedule and schedule variance.
                                scheduleType: 'regular',
                                scheduledVarType: rateType,
                                text: rateType,
                                decription: '',
                                startDate: new Date(date.getFullYear(), date.getMonth(), date.getDate(), timeStart.getHours(), timeStart.getMinutes(), timeStart.getSeconds()),
                                endDate: new Date(date.getFullYear(), date.getMonth(), date.getDate(), timeEnd.getHours(), timeEnd.getMinutes(), timeEnd.getSeconds()),
                                disabled: true
                            });
                        }
                    });
                }
            }
        });

        return regularSchedules;
    },
    
    /**
     * Get appliable schedule assign record.
     * 
     * (1).First look at any non-seasonal schedules (where is_seasonal = 0) to see if the Craftsperson Calendar Date falls between the Start and End date of any schedule.  
     * If so, then that schedule will apply.  A schedule with no Start/End dates will take precedence over all other schedules.  
     * If there are no non-seasonal schedules that apply, then any applicable seasonal schedules will be used.  
     * Again, the system will check to see which seasonal schedule applies during the Craftsperson Calendar Date by comparing the day/month of the Craftsperson Calendar Date to the day and month values of each seasonal schedule in cf_schedules_assign.
     * 
     */
    getApplicableScheduleAssign: function(cfId,scheduleAssignRecords,date){
        var schedule=null;
        var applicableNonSeasonalAssign=null;
        var applicableSeasonalAssign=null;
        
        _.each(scheduleAssignRecords,function(record){
           if(record.getValue('cf_schedules_assign.cf_id')==cfId){
               var dateStart=record.getValue('cf_schedules_assign.date_start');
               var dateEnd=record.getValue('cf_schedules_assign.date_end');
               var isSeasonal=record.getValue('cf_schedules_assign.is_seasonal');
               
               if(!dateStart){
                   dateStart=new Date(1,1);
               }
               
               if(!dateEnd){
                   dateEnd=new Date(2999,1);
               }
               
               if(isSeasonal=='1'){
                   dateStart.setFullYear(date.getFullYear());
                   dateEnd.setFullYear(date.getFullYear());
                   //If Start Date < End Date, then End Date should be changed to next year than Start Date.
                   if(dateEnd.getTime()<dateStart.getTime()){
                       //If current date less than end date, then start date should be set to last year of current date. otherwise, it should be set to current year of current date.
                       if(date.getTime() <= dateEnd.getTime()){
                           dateStart.setFullYear(date.getFullYear()-1);
                       }else{
                           dateEnd.setFullYear(date.getFullYear()+1);
                       }
                   }
               }
               
               if(date.between(dateStart,dateEnd)){
                   if(isSeasonal=='1'){
                       applicableSeasonalAssign=record;
                   }else{
                       applicableNonSeasonalAssign=record;
                   }
               }
           }
        });
        
        if(applicableNonSeasonalAssign){
            schedule=applicableNonSeasonalAssign;
        }else if(applicableSeasonalAssign){
            schedule=applicableSeasonalAssign;
        }
        
        return schedule;
    },

    /**
     * load site table data.
     */
    loadSiteData: function() {
        var me = this;
        var records = this.loadDataFromDataSource('site', [ 'site.site_id', 'site.name' ], new Ab.view.Restriction());
        me.siteList.length = 0;
        _.each(records, function(record) {
            me.siteList.push({
                id: record.getValue('site.site_id'),
                text: record.getValue('site.site_id')
            });
        });
    },
    
    /**
     * Load building data.
     */
    loadBuildingData: function(siteId) {
        var me = this;
        var restriction=new Ab.view.Restriction();
        if(siteId){
            restriction.addClause('bl.site_id',siteId,'=');
        }
        var records = this.loadDataFromDataSource('bl', [ 'bl.bl_id', 'bl.name','bl.site_id'], restriction);
        me.buildingList.length = 0;
        _.each(records, function(record) {
            me.buildingList.push({
                id: record.getValue('bl.bl_id'),
                text: record.getValue('bl.bl_id'),
                siteId:record.getValue('bl.site_id')
            });
        });
    },
    
    /**
     * Get siteId by building code.
     */
    getSiteIdByBuildingCode: function(blId){
        var me = this;
        var siteId = '';
        var restriction=new Ab.view.Restriction();
        if(blId){
            restriction.addClause('bl.bl_id',blId,'=');
        }
        var records = this.loadDataFromDataSource('bl', [ 'bl.bl_id','bl.site_id'], restriction);
        if(records.length>0){
            siteId = records[0].getValue('bl.site_id');
        }
        
        return siteId;
    },
    
    /**
     * Load table data.
     */
    loadDataFromDataSource: function(tableName, fields, restriction) {
        var parameters = {
            tableName: tableName,
            fieldNames: toJSON(fields),
            restriction: toJSON(restriction),
            recordLimit: 0
        };
        var result = Workflow.call('AbCommonResources-getDataRecords', parameters);
        return result.dataSet.records;
    },

    /**
     * Get real start date of view by current date.
     * @param {currentDate} Selected current date of currentDate option.
     */
    getStartDateOfViewByCurrentDate: function(date,firstDayOfWeek) {
        var delta = (date.getDay() - firstDayOfWeek + 7) % 7;
        var result = new Date(date);
        result.setDate(date.getDate() - delta);
        return result
    },

    /**
     * Validate appointment data before appointment added or updated.
     */
    validateAppointmentData: function(appointmentData) {
        var result = true;
        var scheduleVarsType = appointmentData.scheduledVarType;
        var siteId = appointmentData.siteId;
        if (scheduleVarsType == 'ON SITE' && (!valueExistsNotEmpty(siteId))) {
            View.alert(getMessage('siteCodeRequiredMessage'));
            result = false;
            return;
        }
        // check time blocks overlapping.
        if (this.validateTimeBlocksOverlapping(appointmentData)) {
            result = false;
            return;
        }

        // Start date must be earlier than end date.
        if (appointmentData.startDate.getTime() >= appointmentData.endDate.getTime()) {
            View.alert(getMessage('endDateTimeCannotEarlyThanStartDateTimeMessage'));
            result = false;
            return;
        }

        return result;
    },

    /**
     * Get day of week by day number.
     * @param {number} day of week.
     */
    getDayOfWeekByNumber: function(number) {
        var dayOfWeek = '';
        switch (number) {
        case 0:
            dayOfWeek = 'SUNDAY';
            break;
        case 1:
            dayOfWeek = 'MONDAY';
            break;
        case 2:
            dayOfWeek = 'TUESDAY';
            break;
        case 3:
            dayOfWeek = 'WEDNESDAY';
            break;
        case 4:
            dayOfWeek = 'THURSDAY';
            break;
        case 5:
            dayOfWeek = 'FRIDAY';
            break;
        case 6:
            dayOfWeek = 'SATURDAY';
            break;
        }

        return dayOfWeek;
    },

    /**
     * Validation of time blocks overlapping.
     * @param {appointmentData} Timeline control appointment data object.
     */
    validateTimeBlocksOverlapping: function(appointmentData) {
        var hasOverlappingTimeBlocks = false;

        var startDate = appointmentData.startDate;
        var endDate = appointmentData.endDate;
        var scheduledVarType = appointmentData.scheduledVarType;
        var scheduleType = appointmentData.scheduleType;
        var cfId = appointmentData.cfId;
        var serverId = appointmentData.serverId;
        // get all time blocks of specify craftsperson.
        var timeBlocks = this.getTimeBlocksByCfId(cfId);

        if (scheduledVarType == 'ON SITE') {
            // ON SITE time block can not overlap with other ON SITE time blocks.
            for (var i = 0; i < timeBlocks.length; i++) {
                var targetTimeBlock = timeBlocks[i];
                var targetStartDate = targetTimeBlock.startDate;
                var targetEndDate = targetTimeBlock.endDate;
                // compare serverId when updating appointment to except appointment it self.
                if (targetTimeBlock.scheduledVarType == 'ON SITE' && serverId != targetTimeBlock.serverId) {
                    var isOverlapping = this.checkOverlappingTimeBlock(startDate, endDate, targetStartDate, targetEndDate);
                    if (isOverlapping) {
                        View.alert(getMessage('onSiteTimeBlocksCannotOverlapMessage'));
                        hasOverlappingTimeBlocks = true;
                        break;
                    }
                }
            }
        } else if (scheduledVarType == 'BUSY') {
            // ON SITE time block can not overlap with other ON SITE time blocks.
            for (var i = 0; i < timeBlocks.length; i++) {
                var targetTimeBlock = timeBlocks[i];
                var targetStartDate = targetTimeBlock.startDate;
                var targetEndDate = targetTimeBlock.endDate;
                // compare serverId when updating appointment to except appointment it self.
                if (targetTimeBlock.scheduledVarType == 'BUSY' && serverId != targetTimeBlock.serverId) {
                    var isOverlapping = this.checkOverlappingTimeBlock(startDate, endDate, targetStartDate, targetEndDate);
                    if (isOverlapping) {
                        View.alert(getMessage('busyTimeBlocksCannotOverlapMessage'));
                        hasOverlappingTimeBlocks = true;
                        break;
                    }
                }
            }
        } else {
            // check other variance type schedule.
            for (var i = 0; i < timeBlocks.length; i++) {
                var targetTimeBlock = timeBlocks[i];
                var targetStartDate = targetTimeBlock.startDate;
                var targetEndDate = targetTimeBlock.endDate;
                // other schedule variance type can not overlap with each other un-regular schedule time blocks, except for the ON SITE type.
                if (targetTimeBlock.scheduleType != 'regular') {
                    if (targetTimeBlock.scheduledVarType != 'ON SITE' && targetTimeBlock.scheduledVarType != 'BUSY' && serverId != targetTimeBlock.serverId) {
                        var isOverlapping = this.checkOverlappingTimeBlock(startDate, endDate, targetStartDate, targetEndDate);
                        if (isOverlapping) {
                            View.alert(getMessage('timeblocksCannotOverlapWithEachOtherMessage'));
                            hasOverlappingTimeBlocks = true;
                            break;
                        }
                    }

                } else {
                    // A Schedule Variance time block of the type Standard, Overtime, and Doubletime cannot overlap with a Standard Craftsperson Schedule time block of the same type.
                    if ((scheduledVarType == 'STANDARD' && targetTimeBlock.scheduledVarType == 'STANDARD') || (scheduledVarType == 'OVERTIME' && targetTimeBlock.scheduledVarType == 'OVERTIME')
                            || (scheduledVarType == 'DOUBLETIME' && targetTimeBlock.scheduledVarType == 'DOUBLETIME')) {
                        if (serverId != targetTimeBlock.serverId) {
                            var isOverlapping = this.checkOverlappingTimeBlock(startDate, endDate, targetStartDate, targetEndDate);
                            if (isOverlapping) {
                                View.alert(getMessage('regularTimeBlockCannotOverlapTheSameTypeMessage'));
                                hasOverlappingTimeBlocks = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        return hasOverlappingTimeBlocks;
    },

    /**
     * Get all time blocks for specify craftsperson.
     * @param {cfId} Craftsperson Code.
     */
    getTimeBlocksByCfId: function(cfId) {
        var timeblocks = [];
        // get regular schedules.
        var regularScheduleCfRes = new Ab.view.Restriction();
        regularScheduleCfRes.addClause('cf_schedules_assign.cf_id', cfId, '=');
        timeblocks = this.getRegularScheduleData(regularScheduleCfRes, this.viewStartDate, this.viewEndDate);
        var variancesScheduleCfRes = new Ab.view.Restriction();
        variancesScheduleCfRes.addClause('cf_schedules_variances.cf_id', cfId, '=');
        timeblocks = timeblocks.concat(this.getScheduleVarianceData(regularScheduleCfRes, this.viewStartDate, this.viewEndDate));

        return timeblocks;
    },

    /**
     * Find scheduledWork to avoid time blocks overlap.
     * @param {appointmentClientId} Updating appointment client id.
     * @param {startDate}
     */
    checkOverlappingTimeBlock: function(startDate, endDate, targetStartDate, targetEndDate) {
        var isOverlapping = false;
        if (!((startDate.getTime() >= targetEndDate.getTime()) || (endDate.getTime() <= targetStartDate.getTime()))) {
            isOverlapping = true;
        }

        return isOverlapping;
    },

    /**
     * Copy array items to target array object.
     */
    copyArrayObject: function(array, targetArray) {
        for (var i = 0; i < array.length; i++) {
            targetArray.push(array[i]);
        }
    },
    
    /**
     * Format date by using the third-party scheduler API.
     */
    formatTooltipDate: function(startDate, endDate, isAllDay) {
        var formatType = "month" !== this.scheduler.option("currentView") && (startDate.getDate() == endDate.getDate()) ? "TIME" : "DATETIME", formattedString = "";
        if (isAllDay) {
            formatType = "DATE"
        }
        //Format date and time string by using the ARCHIBUS API.
        var dateFormatDs = View.dataSources.get('dateTimeFormateDs');
        
        var formattedStartString = "";
        var formattedEndString = "";
        
        if(formatType == "DATE"){
            formattedStartString = dateFormatDs.formatValue('wrcf.date_assigned',startDate,true);
            formattedEndString = dateFormatDs.formatValue('wrcf.date_assigned',endDate,true);
        }
        
        if(formatType == "DATETIME"){
            formattedStartString = dateFormatDs.formatValue('wrcf.date_assigned',startDate,true)+' '+dateFormatDs.formatValue('wrcf.time_assigned',startDate,true);
            formattedEndString = dateFormatDs.formatValue('wrcf.date_assigned',endDate,true)+' '+dateFormatDs.formatValue('wrcf.time_assigned',endDate,true);
        }
        
        if(formatType == "TIME"){
            formattedStartString = dateFormatDs.formatValue('wrcf.time_assigned',startDate,true);
            formattedEndString = dateFormatDs.formatValue('wrcf.time_assigned',endDate,true);
        }
        
        return formattedStartString +'-'+ formattedEndString;
    },
    
/*****************************Customized the appointment tooltip.**************/
    
    /**
     * Get customized appointment tooltip template.
     */
    getTooltipTemplate: function(data) {
        var scheduleVarianceDs = View.dataSources.get('cfScheduleVariancesDs');
        var $tooltip = jQuery("<div>").addClass("dx-scheduler-appointment-tooltip");
        startDate = data.startDate, endDate = data.endDate, description = data.description,
        startDateTimeZone = this.scheduler.fire("getField", "startDateTimeZone", data),
        endDateTimeZone = this.scheduler.fire("getField", "endDateTimeZone", data);
        startDate = this.scheduler.fire("convertDateByTimezone", startDate, startDateTimeZone);
        endDate = this.scheduler.fire("convertDateByTimezone", endDate, endDateTimeZone);
        var formattedVarianceType=scheduleVarianceDs.formatValue('cf_schedules_variances.sched_vars_type',data.scheduledVarType);
        jQuery("<div>").text(formattedVarianceType).addClass("dx-scheduler-appointment-tooltip-title").appendTo($tooltip);
        jQuery("<div>").addClass("dx-scheduler-appointment-tooltip-date").text(this.formatTooltipDate(startDate, endDate, false)).appendTo($tooltip);

        var $buttons = jQuery("<div>").addClass("dx-scheduler-appointment-tooltip-buttons").appendTo($tooltip);
        if (this.scheduler._editing.allowDeleting) {
            this._getDeleteButton(data).appendTo($buttons);
        }

        this._getEditButton(data).appendTo($buttons);
        return $tooltip;
    },

    /**
     * Get delete button element for appointment tooltip popup window.
     */
    _getDeleteButton: function(data) {
        var that = this;
        var ele = jQuery("<div>").dxButton({
            icon: "trash",
            onClick: function() {
                that.scheduler.deleteAppointment(data);
                that.scheduler.hideAppointmentTooltip();
            }
        });

        return ele;
    },

    /**
     * Get edit button element for appointment tooltip popup window.
     */
    _getEditButton: function(data) {
        var that = this, allowUpdating = that.scheduler._editing.allowUpdating;
        var text = allowUpdating? getMessage('Edit'): getMessage('View');
        var editButton = jQuery("<div>").dxButton({
            icon: allowUpdating ? "edit" : "",
            text: text,
            onClick: function() {
                //APP-1341
                if(that.scheduler._$popup){
                    that.scheduler._$popup=null;
                    that.scheduler._cleanPopup();
                }
                that.scheduler.showAppointmentPopup(data, false, data);
                that.scheduler.hideAppointmentTooltip();
            }
        });

        return editButton;
    },
});