/**
 * Schedule controller used by Define Craftsperson Schedule view.
 * @author Jia
 */
var DailyScheduleControl = BaseScheduleControl.extend({
    /**
     * @override Constructor.
     * @param {config} The control configuration.
     */
    constructor: function(config) {
        this.inherit(config);
    }
});