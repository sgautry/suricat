var gisController = View.createController('gisController', {

	mapCustomFillColors : ['#2979ad','#6b3c9c','#399e31','#9cc7d6','#add384','#ff7d00','#ff9a9c','#f7ba6b','#c6aece','#f7dd6b'],

	// leaflet map controll
	map : null,

	// flag of is valid license of gis
	isValidLicense : true,
	resFromShowFunction:'',

  	locMetricDashCtrl:null,

	afterInitialDataFetch: function(){
		initialDashCtrl(this);
		//gisController.mapPanel.show(false);
	},

	refreshChart:function(){
		var consoleCtrl = this.locMetricDashCtrl.consoleCtrl;
		var treeRes = this.locMetricDashCtrl.treeCtrl.treeRes;
		this.showSelectedBuildings(treeRes+" AND "+consoleCtrl.blIdRes+" AND "+consoleCtrl.siteIdRes);
	},

	afterViewLoad: function(){		
		this.initializeMap();
	},
	
	
	/**
	 * initialize map object
	 */
	initializeMap : function() {
		var configObject = new Ab.view.ConfigObject();
		configObject.mapImplementation = 'Esri';
		configObject.basemap = View.getLocalizedString('World Light Gray Canvas');
		this.map = new Ab.leaflet.Map('mapPanel', 'map', configObject);
		
		// basemap layer menu
	    var basemapLayerMenu = gisController.mapPanel.actions.get('basemapLayerMenu');
	    basemapLayerMenu.clear();
	    var basemapLayers = gisController.map.getBasemapLayerList();
	    for (var i=0; i<basemapLayers.length; i++){
	       basemapLayerMenu.addAction(i, basemapLayers[i], this.switchBasemapLayer);
	    }
	},

	switchBasemapLayer: function(item) {
    	//switch the map layer based on the passed in layer name.
    	gisController.map.switchBasemapLayer(item.text);
    },

	/**
	 * Show selected buildings on map.
	 * 
	 * @param {Array}
	 *            items - selected building id's
	 */
	showSelectedBuildings : function(res) {
		gisController.resFromShowFunction='';
		gisController.resFromShowFunction=res;
		gisController.mapPanel.show(true);
		gisController.createMapMarkers();
		gisController.showMapMarkers(gisController.resFromShowFunction);
	},

	
	createMapMarkers: function(){
		var mapMarkerDataSource = 'bl_ds';
        var mapMarkerKeyFields = ['bl.bl_id'];
        var mapMarkerGeometryFields = ['bl.lon', 'bl.lat'];
        var mapMarkerTitleField = 'bl.bl_id';
        var mapMarkerContentFields = ['bl.address1', 'bl.name', 'bl.site_id', 'bl.city_id', 'bl.state_id', 'bl.ctry_id', 'bl.use1'];
        
        var mapThematicBuckets=[];
        
        var data =  eval("(" + this.locMetricDashCtrl.pieRawData + ")");
		mapThematicBuckets.length = 0;
		for (var i=0; i<data.length;i++) {
		  mapThematicBuckets.push(data[i]['bl.use1']);
		}
		//build customized color for colorbrewer.
		colorbrewer['FP-YRG']={10:gisController.mapCustomFillColors}
        var mapMarkerProperties={
            radius: 10,
            //stroke properties
        	stroke: true,
        	strokeColor: '#fff',
        	strokeWeight: 1.0,
        	// required for thematic markers
        	renderer: 'thematic-unique-values',
        	thematicField: 'bl.use1',
        	uniqueValues: [],
        	colorBrewerClass: 'FP-YRG'
        };
        
        gisController.map.createMarkers(
            mapMarkerDataSource,
            mapMarkerKeyFields,
            mapMarkerGeometryFields,
            mapMarkerTitleField,
            mapMarkerContentFields,
            mapMarkerProperties
        ); 
        
        // legend menu
    	var legendObj = Ext.get('showLegend'); 
	    legendObj.on('click', this.showLegend, this, null);
	},
	
	showMapMarkers: function(res){
		 gisController.map.showMarkers('bl_ds', res); 
	},
	
	showLegend: function(){
		gisController.map.showMarkerLegend();
	}
});