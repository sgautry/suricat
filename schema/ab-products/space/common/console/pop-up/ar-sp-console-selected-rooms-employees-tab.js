var selectedRoomsEmployeesTabController = View.createController('selectedRoomsEmployeesTabController', {
	
	isMultiRoomFormVisible: false,
	isSingleRoomFormVisible: false,
	
	variesValue: '',
	
	/**
	 * Initialize the event listener to edit selected rooms or employees.
	 */
	afterCreate: function() {
        this.on('app:space:express:console:onSelectedResourcesChanged', this.onSelectedResourcesChanged);
    },
    
    /**
     * Enable field actions of multiple room form.
     */
    afterInitialDataFetch: function() {
		this.variesValue = '<'+ getMessage('variesText') +'>';
    	this.multipleRoomForm.enableFieldActions("employee_capacity", false);
    	this.multipleRoomForm.enableFieldActions("total_room_area", false);
    },
    
    /**
     * As for certain rows selected, the app decides which editing form should be displayed. 
     */
    onSelectedResourcesChanged: function(type, rows) {
    	
    	if (type === 'room') {
    		this.displayEditingRoomForm(rows);
    	} else if (type === 'team') {
    		this.displayEditingTeamForm(rows);
    	} else {
    		View.alert(getMessage('noSuchResource'));
    		View.alert("no such resource.Only support room, employee and teams.");
    	}
    },
    
    //  ------- STRAT Logics for editing room(s).  

	/**
     * Display editing form for rooms.
     */
    displayEditingRoomForm: function(rows) {
    	if (rows.length == 0) {
    		this.hideAllEditForms();
    	} else {
    		if (rows.length == 1) {
    			this.editSingleRoom(rows[0]);
    		} else {
    			this.displayDifferentValues(rows);
    		}
    	}

		// after show edit form, need to resize the layout region.
		var layout = View.getLayoutManager('selectedRoomsLayout');
		layout.recalculateLayout();
},
    
    /**
	 * Edit a room which is selected through the checkbox.
	 */
	editSingleRoom: function(row) {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('rm.bl_id',row['rm.bl_id']);
		restriction.addClause('rm.fl_id',row['rm.fl_id']);
		restriction.addClause('rm.rm_id',row['rm.rm_id']);
		if (!this.isSingleRoomFormVisible) {
			this.hideAllEditForms();
			this.isSingleRoomFormVisible = true;
		}
		this.singleRoomForm.refresh(restriction);
		this.singleRoomForm.show(true);
	},

    displayDifferentValues: function(rows) {
    	if (!this.isMultiRoomFormVisible) {
    		this.hideAllEditForms();
    		this.isMultiRoomFormVisible = true;
    		this.multipleRoomForm.show(true);
    	}
		//Set the total room area
		var totalRoomArea = 0.0;
		var differentFields = [];
		for(var i = 0; i < rows.length; i++) {
			var rmAreaValue = rows[i]['rm.area'];
			if (valueExistsNotEmpty(rows[i]["rm.area.raw"])) {
			    rmAreaValue = rows[i]["rm.area.raw"];
			}
			totalRoomArea = totalRoomArea + parseFloat(rmAreaValue);
		}
		this.multipleRoomForm.setFieldValue('total_room_area', insertGroupingSeparator(totalRoomArea.toFixed(2), true));
		this.initializeMultiRoomForm(rows);
		this.setVariesFields(rows);
    },
    
    initializeMultiRoomForm: function(rows) {
    	this.multipleRoomForm.setFieldValue('rm.dv_id', rows[0]['rm.dv_id']);
		this.multipleRoomForm.setFieldValue('rm.dp_id', rows[0]['rm.dp_id']);
		this.multipleRoomForm.setFieldValue('rm.rm_cat', rows[0]['rm.rm_cat']);
		this.multipleRoomForm.setFieldValue('rm.rm_type', rows[0]['rm.rm_type']);
		this.multipleRoomForm.setFieldValue('employee_capacity', rows[0]['rm.cap_em']);
		this.multipleRoomForm.setFieldValue('rm.valuable', rows[0]['rm.valuable']);
		this.multipleRoomForm.setFieldValue('rm.valo_comment', rows[0]['rm.valo_comment']);
    },
    
    setVariesFields: function(rows) {
    	var formDv = this.multipleRoomForm.getFieldValue('rm.dv_id');
    	var formDp = this.multipleRoomForm.getFieldValue('rm.dp_id');
    	var formCat = this.multipleRoomForm.getFieldValue('rm.rm_cat');
    	var formType = this.multipleRoomForm.getFieldValue('rm.rm_type');
    	var formCapEm = this.multipleRoomForm.getFieldValue('employee_capacity');
    	var formValuable = this.multipleRoomForm.getFieldValue('rm.valuable');
    	var formValoComment = this.multipleRoomForm.getFieldValue('rm.valo_comment');
    	
    	for(var i = 1; i < rows.length; i++) {
        	
    		var dataRow = rows[i];
    		if (formValuable != this.variesValue) {
    			if (formValuable != dataRow['rm.valuable']) {
    				this.multipleRoomForm.setFieldValue('rm.valuable', this.variesValue);
    			}
    		}
    		
    		if (formValoComment != this.variesValue) {
    			if (formValoComment != dataRow['rm.valo_comment']) {
    				this.multipleRoomForm.setFieldValue('rm.valo_comment', this.variesValue);
    			}
    		}
    		
    		if (formDv != this.variesValue) {
    			if (formDv != dataRow['rm.dv_id']) {
    				this.multipleRoomForm.setFieldValue('rm.dv_id', this.variesValue);
    			}
    		}
    		
    		if (formDp != this.variesValue) {
    			if (formDp != dataRow['rm.dp_id']) {
    				this.multipleRoomForm.setFieldValue('rm.dp_id', this.variesValue);
    			}
    		}
    		
    		if (formCat != this.variesValue) {
    			if (formCat != dataRow['rm.rm_cat']) {
    				this.multipleRoomForm.setFieldValue('rm.rm_cat', this.variesValue);
    			}
    		}
    		
    		if (formType != this.variesValue) {
    			if (formType != dataRow['rm.rm_type']) {
    				this.multipleRoomForm.setFieldValue('rm.rm_type', this.variesValue);
    			}
    		}
    		
    		if (formCapEm != this.variesValue) {
    			if (formCapEm != dataRow['rm.cap_em']) {
    				this.multipleRoomForm.setFieldValue('employee_capacity', this.variesValue);
    			}
    		}
    	}
    },
    	
    /**
	 * call the workflow rule to save multiple rooms for editing.
	 */
	multipleRoomForm_onSaveMultiRoom: function() {
		/*var validation = this.validateEmCapacity(this.multipleRoomForm.getFieldElement('employee_capacity'));
		if(!validation){
			return;
		}
		*/
		var newValueArray = this.getNewFormValueForRoomsBulkEdit();
		var rooms = [];
		var rows = this.selectedRoomsGrid.getSelectedRows();
		for (var i = 0; i < rows.length; i++) {
			var row = rows[i];
			var blId = row['rm.bl_id'];
			var flId = row['rm.fl_id'];
			var roomId = row['rm.rm_id'];
			var roomJsonArray = {'rm.bl_id':blId, 'rm.fl_id':flId, 'rm.rm_id':roomId};
			rooms.push(roomJsonArray);
		}
		
		try{
			Workflow.callMethod("AbSpaceRoomInventoryBAR-SpaceExpressService-updateMultipleRooms", rooms, newValueArray);
			this.refreshRoomsGrid();
		} catch (e) {
			//The user may just set division or room category but not department or room type, we display a error message.
			this.multipleRoomForm.displayTemporaryMessage(e.message, 4000);
		}
	},
	
	/**
	 * Get the new value for bulk rooms edit.
	 */
	getNewFormValueForRoomsBulkEdit: function() {
		var selectedRoomsGrid = View.panels.get('selectedRoomsGrid');
		var rows = selectedRoomsGrid.getSelectedRows();
		var inputDivisionId = this.multipleRoomForm.getFieldValue('rm.dv_id');
		var inputDepartmentId = this.multipleRoomForm.getFieldValue('rm.dp_id');
		var inputRoomCategory = this.multipleRoomForm.getFieldValue('rm.rm_cat');
		var inputRoomType = this.multipleRoomForm.getFieldValue('rm.rm_type');
		var employeeCapacity = this.multipleRoomForm.getFieldValue('employee_capacity');
		var inputValuable = this.multipleRoomForm.getFieldValue('rm.valuable');
		var inputComment = this.multipleRoomForm.getFieldValue('rm.valo_comment');

		inputDivisionId = inputDivisionId.replace("<DIFF>","<VARIES>");
		inputDepartmentId = inputDepartmentId.replace("<DIFF>","<VARIES>");
		inputRoomCategory = inputRoomCategory.replace("<DIFF>","<VARIES>");
		inputRoomType = inputRoomType.replace("<DIFF>","<VARIES>");
		employeeCapacity = employeeCapacity.replace("<DIFF>","<VARIES>");
		inputValuable = inputValuable.replace("<DIFF>","<VARIES>");
		inputComment = inputComment.replace("<DIFF>","<VARIES>");
		
		var newValueArray = [
				{'fieldName':'rm.dv_id','fieldValue':inputDivisionId},
				{'fieldName':'rm.dp_id','fieldValue':inputDepartmentId},
				{'fieldName':'rm.rm_cat','fieldValue':inputRoomCategory},
				{'fieldName':'rm.rm_type','fieldValue':inputRoomType},
				{'fieldName':'rm.cap_em','fieldValue':employeeCapacity},
				{'fieldName':'rm.valuable','fieldValue':inputValuable},
				{'fieldName':'rm.valo_comment','fieldValue':inputComment}
		];
		return newValueArray;
	},
	
	/**
	 * Refresh the rooms grid after update operation.
	 */
	refreshRoomsGrid: function() {
		this.hideAllEditForms();
		this.selectedRoomsGrid.refresh();
		this.updateDrawingPanel();
	},

	/**
	 * Delete multiple employees.
	 */
	deleteMultipleRooms: function() {
        var rows = this.selectedRoomsGrid.getSelectedRows();
		var thisCtrl = selectedRoomsEmployeesTabController;
		View.confirm(getMessage("deleteRooms"), function(result) {
			if (result == 'yes') {
				for (var i = 0; i < rows.length; i++) {
					var record = new Ab.data.Record({
						'rm.bl_id': rows[i]["rm.bl_id"],
						'rm.fl_id': rows[i]["rm.fl_id"],
						'rm.rm_id': rows[i]["rm.rm_id"]
					}, true);
					try {
						thisCtrl.selectedViewedRoomsDS.deleteRecord(record);
					} 
					catch (e) {
						var message = getMessage('errorDeleteRoom');
						View.showMessage('error', message+": "+rows[i]["rm.bl_id"]+'-'+rows[i]["rm.fl_id"]+'-'+rows[i]["rm.rm_id"], e.message, e.data);
						return;
					}
				}
				thisCtrl.multipleRoomForm.show(false);
				thisCtrl.selectedRoomsGrid.refresh();
				thisCtrl.updateDrawingPanel();
			} 
		});

	},

    //  ------- END Logics for editing room(s).  

	
	//  ------- Common Functions Section.  
    /**
     * Hide all editing forms.
     */
    hideAllEditForms: function() {
    	this.singleRoomForm.show(false);
		this.multipleRoomForm.show(false);
		this.isSingleRoomFormVisible = false;
		this.isMultiRoomFormVisible = false;

	},

	/**
	 * Compared two rows of employee grid and return the different value's fields.
	 * Any field that has been compared with a different value will not been compared again.
	 */
	compareRows: function(comparedFields, baseRow, comparedRow, escapedFields) {
		var difFields = [];
		for (var i = 0 ; i < comparedFields.length; i++) {
			var escape = false;
			for (var j = 0; j < escapedFields.length; j++) {
				if (comparedFields[i]==escapedFields[j]) {
					escape = true;
				}
			}
			if (!escape) {
				var fieldName = comparedFields[i];
				if (baseRow[fieldName] != comparedRow[fieldName]) {
					difFields.push(fieldName);
				}
			}
		}
		return difFields;
	},

	/**
	 * After save, delete or cancel operation, need to update drawing panel's action bar and floor plan inside it.
	 */
	updateDrawingPanel: function() {
		var spConsoleCtrl = View.getOpenerView().controllers.get('spaceExpressConsole');
		spConsoleCtrl.trigger('app:space:express:console:cancelSelectedRooms');
		spConsoleCtrl.trigger('app:space:express:console:commitRoomsAlteration');
	}
});