
/**
 * Controller that handles all assignments.
 *
 * Saves pending assignments into the drawing panel sidecar so that the user can finish the assignments during the next session.
 *
 * Events:
 * app:space:express:console:afterBeginAssignment
 * app:space:express:console:afterClearAssignment
 * app:space:express:console:afterChangeAssignmentTarget
 */
var spaceExpressConsoleAssignment = View.createController('spaceExpressConsoleAssignment', {
    
    /**
     * A copy of the parent view mode.
     */
    mode: '',
    /**
     * The object that describes what rooms are assigned to: a department, a room type, or employees.
     */
    assignmentTarget: null,

    /**
     * The Ab.space.express.console.Assignments collection of pending assignments.
     */
    pendingAssignments: null,

    /**
     * The Ab.space.express.console.Rooms collection of selected rooms.
     */
    selectedRooms: null,

    /**
     * A array[room, countEm] for save a set of assets and related count employee. 
     * object : assignedEmCount: {room: '', count: 0},
     */
    roomCountArray: [],

    /**
     * The current asOfDate.
     */
    asOfDate: null,

    /**
     * Constructor.
     */
    afterCreate: function() {
        this.clearAssignment();
        this.on('app:space:express:console:selectLocation', this.updateStatus);
        this.on('app:space:express:console:beginAssignment', this.beginAssignment);
        this.on('app:space:express:console:commitAssignment', this.commitAssignment);
        this.on('app:space:express:console:cancelAssignment', this.cancelAssignment);
        this.on('app:space:express:console:removeAssignment', this.removeAssignment);
        this.on('app:space:express:console:viewAssignment', this.viewAssignment);
        this.on('app:space:express:console:selectRoom', this.selectRoom);
        this.on('app:space:express:console:moveEmployee', this.moveEmployee);
        this.on('app:space:express:console:addEmployeeWaiting', this.addEmployeeWaiting);
        this.on('app:space:express:console:removeEmployeeWaiting', this.removeEmployeeWaiting);
        this.on('app:space:express:console:unassignEmployees', this.unassignEmployees);
        this.on('app:space:express:console:viewSelectedRooms', this.viewSelectedRooms);
        this.on('app:space:express:console:changeMode', this.afterChangeMode);
        this.on('app:space:express:console:cancelSelectedRooms', this.cancelSelectedRooms);
        this.on('app:space:express:console:changeAttributeTab', this.changeAssignTarget);
        this.on('app:space:express:console:updateEmployeeAssignment', this.updateEmployeeAssignment);
        this.on('app:space:express:console:clearPendingAssignments', this.clearPendingAssignments);
    },

    /**
     * Loads pending assignments from the previous user session, if any.
     * re-establish the same assignment mode if the sidecar contains previously saved assignment target
     */
    afterInitialDataFetch: function() {
        this.restorePendingAssignmentsIfExist();
    },

    /**
     * Reset mode aftet the change mode event.
     */
    afterChangeMode: function(mode) {
        if (valueExists(mode)) {
            this.mode = mode;
        }
    },
    
    /**
     * Restore any pending assignment if exists.
     */
    restorePendingAssignmentsIfExist: function() {
        this.drawingPanel.getSidecar().load();
        // if the sidecar contains previously saved assignment target, re-establish the same assignment mode
        var assignmentTarget = this.drawingPanel.getSidecar().get('assignmentTarget');
        var pendingAssignmentsValue = this.drawingPanel.getSidecar().get('pendingAssignments');
        
        var restoredAssignments = null;
        if (pendingAssignmentsValue) {
            restoredAssignments = new Ab.space.express.console.Assignments(pendingAssignmentsValue);
        }
        
        this.assignmentTarget = null;
        this.setPendingAssignments(null);
        if (assignmentTarget) {
            if (restoredAssignments && restoredAssignments.length > 0) {
                 this.assignmentTarget = assignmentTarget;
                 this.setPendingAssignments(restoredAssignments);
                 this.beginAssignment(assignmentTarget);
            } 
        }
        
        this.updateStatus();
    },
    
    /**
     * Sets pending assignments and set a listener that would save pending assignments to the sidecar on any change.
     */
    setPendingAssignments: function(pendingAssignments) {
        if (pendingAssignments) {
            this.pendingAssignments = pendingAssignments;
        } else {
            this.pendingAssignments = new Ab.space.express.console.Assignments();
        }
        // set a listener that would save pending assignments to the sidecar on any change
        this.pendingAssignments.on('add', this.onPendingAssignmentsChanged, this);
    },

    /**
     * Called after items are added or removed from the pending assignments collection.
     */
    onPendingAssignmentsChanged: function() {
        this.drawingPanel.getSidecar().set('assignmentTarget', this.assignmentTarget);
        this.drawingPanel.getSidecar().set('pendingAssignments', this.pendingAssignments);
        this.drawingPanel.getSidecar().save();
    },

    /**
     * Clears all pending assignments.
     */
    clearAssignment: function() {
        this.assignmentTarget = null;
        this.pendingAssignments = new Ab.space.express.console.Assignments();
        this.selectedRooms = new Ab.space.express.console.Rooms();
        this.setPendingAssignments();
        View.closePanelWindows();
    },

    /**
     * Begins new assignment operation.
     * Keep pending assignments if the assignment operation has the same type, e.g. the user
     * has already assigned some rooms to a department, and has selected another department.
     * @param assignmentTarget
     */
    beginAssignment: function(assignmentTarget) {
        if (!this.assignmentTarget || this.assignmentTarget.type != assignmentTarget.type) {
            this.beginNewAssignment(assignmentTarget);
        } else {
            this.beginSameAssignment(assignmentTarget);
        }
    },
    
    /**
     * To begin a new attribute assignment.
     */
    beginNewAssignment : function(assignmentTarget) {
        //warn the user if he or she changes assign target
        if (this.pendingAssignments.length > 0) {
            var warnMessage = getMessage('changeAssignTarget');
            var innerThis = this;
            View.confirm(warnMessage, function(button) {
                if (button == 'yes') {
                    innerThis.clearAssignment();
                    innerThis.trigger('app:space:express:console:refreshDrawing');
                    innerThis.trigger('app:space:express:console:afterBeginAssignment', assignmentTarget, innerThis.pendingAssignments);
                    innerThis.assignmentTarget = assignmentTarget;
                    innerThis.updateStatus();
                }
            });
        } else {
            this.trigger('app:space:express:console:afterBeginAssignment', assignmentTarget, this.pendingAssignments);
            this.assignmentTarget = assignmentTarget;
            this.updateStatus();
        }
    },
    
    /**
     * The assigned attribute is the same as the existed.
     */
    beginSameAssignment : function(assignmentTarget) {
        var tmpAssignment = assignmentTarget;
        if (assignmentTarget != null) {
            if(assignmentTarget.type == 'employee') {
                //fix the load error.
                var employees = assignmentTarget.employees;
                if(employees && employees.length == 0 && this.pendingAssignments.length == 0) {
                    tmpAssignment = null;
                }
            }
        }
        
        this.assignmentTarget = tmpAssignment;
        
        if (this.assignmentTarget == null) {
            this.trigger('app:space:express:console:afterClearAssignment');
        } else {
            this.trigger('app:space:express:console:afterBeginAssignment', this.assignmentTarget, this.pendingAssignments);
        }
        this.updateStatus();
    },
    
    /**
     * Cancels the current assignment operation and clears all pending assignments.
     */
    cancelAssignment: function() {
        //set the 'this.pendingAssignments' to empty.
        var isTeamAssignMode = ('team'===this.assignmentTarget.type); 
        this.clearAssignment();
        this.roomCountArray = [];
        this.refreshGrid();
        
        //set empty value to 'pendingAssignments'.
        this.drawingPanel.getSidecar().set('pendingAssignments', this.pendingAssignments);
        this.drawingPanel.getSidecar().save();
        
        this.updateStatus();
        this.trigger('app:space:express:console:afterClearAssignment', isTeamAssignMode);
    },
    
    /**
     * cancel the selected rooms.
     */
    cancelSelectedRooms: function() {
        this.selectedRooms = new Ab.space.express.console.Rooms();
        this.updateStatus();
    },
    
    /**
     * Refresh grid when cancel assignments or submit assignments.
     * Modified by heqiang to unhighlight the selected node but not refresh it.
     */
    refreshGrid: function() {
        var tabName =  this.attributeTabs.getSelectedTabName();
        
        var unselectedNode = null;
        if (tabName=='categoriesTab') {
           unselectedNode = this.categoriesTree.lastNodeClicked;
           this.categoriesTree.refresh();
        }
        if (unselectedNode) {
            //kb#3052233: work around, since after refreh tree, the tree control's lastNodeClicked turns to be null.
            unselectedNode.treeControl.lastNodeClicked = unselectedNode;
            unselectedNode.highlightNode(false);
        }
        
    },
    
    /**
     * When change assignment target, clear assignment target and pending assignments when user switch among the tabs.
     */
    changeAssignTarget: function() {
        this.assignmentTarget = null;
        this.pendingAssignments = new Ab.space.express.console.Assignments();
        this.drawingPanel.getSidecar().set('pendingAssignments', this.pendingAssignments);
        this.drawingPanel.getSidecar().save();
        this.trigger('app:space:express:console:refreshDrawing');
        this.updateStatus();
    },

    /**
     * Commits the current assignment operation and clears all pending assignments.
     */
    commitAssignment: function() {
        var assignments = this.getAssignmentsForWFR();
        try {
            if ( 'spaceMode' == this.mode ) {
                Workflow.callMethod("AbSpaceRoomInventoryBAR-SpaceExpressService-commitSpaceAssignments", assignments);
                this.afterCommitAssignment();
            } 
        } catch (e) {
            Workflow.handleError(e); 
        }
        
    },

    afterCommitAssignment: function() {
        var isTeamAssignMode = ('team'===this.assignmentTarget.type); 
        this.clearAssignment();
        this.roomCountArray = [];

        this.drawingPanel.getSidecar().set('pendingAssignments', this.pendingAssignments);
        this.drawingPanel.getSidecar().save();

        this.updateStatus();
        //refresh grid to show updated records.
        this.refreshGrid();
        this.trigger('app:space:express:console:afterClearAssignment', isTeamAssignMode);
    },
    
    /**
     * Get assignments Array as a WFR parameter.
     */
    getAssignmentsForWFR: function() {
        var assignments = [];
        for ( var i=0; i < this.pendingAssignments.models.length; i++ ) {
            var assignment = this.pendingAssignments.models[i];
            //construct record object from assignment
            var record = {};
            record['bl_id'] = assignment.attributes.bl_id;
            record['fl_id'] = assignment.attributes.fl_id;
            record['rm_id'] = assignment.attributes.rm_id;
            record['em_id'] = assignment.attributes.em_id==undefined ? "" : assignment.attributes.em_id;
            record['to_bl_id'] = assignment.attributes.to_bl_id==undefined ? "" : assignment.attributes.to_bl_id;
            record['to_fl_id'] = assignment.attributes.to_fl_id==undefined ? "":assignment.attributes.to_fl_id;
            record['to_rm_id'] = assignment.attributes.to_rm_id==undefined ? "" : assignment.attributes.to_rm_id;
            record['dv_id'] = assignment.attributes.to_dv_id==undefined ? "" : assignment.attributes.to_dv_id;
            record['dp_id'] = assignment.attributes.to_dp_id==undefined ? "" : assignment.attributes.to_dp_id;
            record['rm_cat'] = assignment.attributes.to_rm_cat==undefined ? "" : assignment.attributes.to_rm_cat;
            record['rm_type']= assignment.attributes.to_rm_type==undefined ? "" : assignment.attributes.to_rm_type;
            record['rm_std'] = assignment.attributes.rm_std == undefined ? "" : assignment.attributes.rm_std;
            record['team_id'] = assignment.attributes.team_id ? assignment.attributes.team_id : "";
            record['date_start'] = assignment.attributes.date_start ? assignment.attributes.date_start : "";
            record['date_end'] = assignment.attributes.date_end ? assignment.attributes.date_end : "";
            //set attribute 'spaceAssignmentType' to division/department/category or type.
            record['spaceAssignmentType'] = this.assignmentTarget.type;

            assignments.push(record);
        }
        return assignments;
    },

    /**
     * Removes specified pending assignment.
     * @param assignment
     */
    removeAssignment: function(assignment) {
        this.pendingAssignments.removeAssignment(assignment);
        if (this.pendingAssignments.length == 0) {
            // the last pending assignment has been removed - cancel the assignment operation
            this.cancelAssignment();
        } else {
            this.viewAssignment();
        }

        this.updateStatus();
    },
    
    /**
     * When remove employee assignment, decrease the assignment count.
     */
    decreaseRoomCount: function(assignment) {
        var currentLocation = assignment.location;
        var pattern = new RegExp('-','g');
        currentLocation = currentLocation.replace(pattern, '');
        for (var i = 0; i < this.roomCountArray.length; i++) {
            var location = this.roomCountArray[i].room;
            if (currentLocation == location) {
                this.roomCountArray[i].count--;
                break;
            }
        }
    },
    
    /**
     * Clear  all pending assignments.
     */
    clearPendingAssignments: function() {
        this.pendingAssignments = new Ab.space.express.console.Assignments();
        this.trigger('app:space:express:console:afterBeginAssignment', this.assignmentTarget, this.pendingAssignments);
        this.updateStatus();
    },
    
    
    /**
     * Updates the action bar.
     */
    updateStatus: function() {
        var status = this.getAssignmentStatus();
        this.drawingPanel.actionbar.setTitle(status);
        var showAssignActions = (status != '');
        var action = this.drawingPanel.actionbar.actions.get('cancelPendingAssignments');
        action.show(this.inAssignmentMode() && showAssignActions);
        action.enableButton(true);

        action = this.drawingPanel.actionbar.actions.get('viewPendingAssignments');
        action.setTitle(getMessage('viewPendingAssignments') + '&nbsp;(' + this.pendingAssignments.length + ')');
        action.show(this.inAssignmentMode() && showAssignActions);
        action.enableButton(true);

        action = this.drawingPanel.actionbar.actions.get('commitPendingAssignments');
        action.show(this.inAssignmentMode() && showAssignActions);
        // console is a 'read-only' version, for those users who do not have authority to make any edits or assignments
        action.enableButton(View.user.isMemberOfGroup('SPACE-CONSOLE-ALL-ACCESS'));

        action = this.drawingPanel.actionbar.actions.get('viewDetails');
        action.show(!this.inAssignmentMode() && this.selectedRooms.length > 0);
        action.enableButton(true);
        
        action = this.drawingPanel.actionbar.actions.get('cancelSelectedRooms');
        action.show(!this.inAssignmentMode() && this.selectedRooms.length > 0);
        action.enableButton(true);
    },

    /**
     * Returns true if the assignment operation is underway.
     * @return {Boolean}
     */
    inAssignmentMode: function() {
        return this.assignmentTarget !== null;
    },

    /**
     * Returns a user-friendly message that describes the current assignment operation.
     * @return {String}
     */
    getAssignmentStatus: function() {
        var status = '';
        if (this.inAssignmentMode()) {
            status = this.getMessageInAssignmentMode();
        } else {
            if (this.selectedRooms.length > 0) {
                status = this.selectedRooms.length + '&nbsp;' + getMessage('selectedRoomCount');
                var selectedRoomData = this.getSelectedRoomsTotals();
                status += ',&nbsp;' + selectedRoomData.area + '&nbsp;' + View.user.areaUnits.title;
            }
        }
        return status;
    },
    
    /**
     * In AssignmentMode, returns a user-friendly message that describes the current assignment operation.
     * @return {String}
     */
    getMessageInAssignmentMode: function() {
        var status = '';
        switch (this.assignmentTarget.type) {
            
            case 'category':
                status = getMessage('categoryAssignmentMode') + '&nbsp;' + this.assignmentTarget.rm_cat;
                break;
            case 'type':
                status = getMessage('categoryTypeAssignmentMode') + '&nbsp;' + this.assignmentTarget.rm_type;
                break;            
        }
        return status;
    },

    /**
     * Called when the user clicks on a room in the drawing control.
     * @param room
     * @param selected
     */
    selectRoom: function(room, selected) {
         if (this.inAssignmentMode()) {
             this.selectRoomInAssignmentMode(room, selected);
         } else {
             if (selected) {
                if (!this.selectedRooms.findRoom(room)) {
                    this.selectedRooms.addRoom(room);
                }
             } else {
                 this.selectedRooms.removeRoom(room);
             }
         }
        
         this.updateStatus();
    },
    
    /**
     * In AssignmentMode, call when the user clicks on a room in the drawing control.
     * @param room
     * @param selected
     */
    selectRoomInAssignmentMode: function(room, selected) {
        var innerThis = this;
        var handleAssignment = function(assignment) {
            if (selected) {
                innerThis.pendingAssignments.addAssignment(assignment);
            } else {
                innerThis.pendingAssignments.removeAssignment(assignment);
            }
        };
        
        switch (this.assignmentTarget.type) {
            case 'category':
                handleAssignment(this.getAssignmentForRoomCat(room));
                break;
            case 'type':
                handleAssignment(this.getAssignmentForRoomCat(room));
                break;
        }
    },

    
    /**
     * get assignment for room category.
     */
    getAssignmentForRoomCat: function(room) {
        return {
            bl_id: room.bl_id,
            fl_id: room.fl_id,
            rm_id: room.rm_id,
            to_rm_cat: this.assignmentTarget.rm_cat,
            to_rm_type: this.assignmentTarget.rm_type
        }
    },
    
    /**
     * Open the pop-up window of selected rooms.
     */
    viewSelectedRooms: function(anchor, asOfDate) {
        var restriction = this.selectedRooms.createRestriction();
        var hasTeamSchema = View.controllers.get('spaceExpressConsole').hasTeamSchema;
        var showTeams = View.controllers.get('spaceExpressConsole').showTeams;
        var popView = 'ar-sp-console-selected-rooms-employees-tab.axvw';
        var popTitle =  getMessage("selectedRooms");
        /*if ( hasTeamSchema && showTeams===1 ){
            popView = 'ab-sp-console-selected-rooms-employees-teams-tab.axvw';
            popTitle = getMessage("selectedRoomTeamsTitle");
        }
        */
        Ab.view.View.openDialog(popView, restriction, false, {
            title : popTitle, 
            collapsible: false, 
            width:900, 
            height:620,
            asOfDate: asOfDate
        });
    },
    
    /**
     * Get the total number of selected rooms
     */
    getSelectedRoomsTotals: function() {
        var selectedRoomData = {
            count: 0,
            area: 0.0,
            headcount: 0
        };

        try {
            var restriction = this.selectedRooms.createRestriction();
            var record = this.selectedRoomsDS.getRecord(restriction);
            selectedRoomData.count = this.selectedRoomsDS.formatValue('rm.total_rooms', record.getValue('rm.total_rooms'), true);
            selectedRoomData.area = this.selectedRoomsDS.formatValue('rm.total_area', record.getValue('rm.total_area'), true);
            selectedRoomData.headcount = this.selectedRoomsDS.formatValue('rm.total_headcount', record.getValue('rm.total_headcount'), true);
        } catch (e) {
            Workflow.handleError(e);
        }

        return selectedRoomData;
    },
    
    /**
     * View the assignments according to the specified type.
     */
    viewAssignment: function(anchor) {
        if (this.inAssignmentMode()) {
            switch (this.assignmentTarget.type) {
                case 'category':
                    this.viewTypePendingAssignments(anchor,'category');
                    break;
                case 'type':
                    this.viewTypePendingAssignments(anchor);
                    break;
            }
        }
    },
    
    getNullDefaultRestriction: function() {
        var restriction = new Ab.view.Restriction();
        if(this.assignmentTarget.type == 'em') {
            restriction.addClause('em.em_id', 'None', '=');
        } else {
            restriction.addClause('rm.bl_id', 'None', '=');
            restriction.addClause('rm.fl_id', 'None', '=');
            restriction.addClause('rm.rm_id', 'None', '=');
        }
        if(this.pendingAssignments.length > 0) {
            restriction = this.pendingAssignments.createRestriction();
        }
        return restriction;
    },

    /**
     * Displays room type pending assignments.
     */
    viewTypePendingAssignments: function(anchor,type) {
        // show the list of pending assignments in an overlay
        var restriction = this.getNullDefaultRestriction();
        this.categoryPendingAssignments.showInWindow({
            anchor: anchor,
            width: 800,
            title: getMessage('pendingTypeAssignmentsTitle'),
            restriction: restriction
        });

        // format custom grid columns
        var fromTemplate = _.template("{{rm_cat}}-{{rm_type}}");
        var toTemplate = _.template("{{to_rm_cat}}-{{to_rm_type}}");

        var controller = this;
        
        this.categoryPendingAssignments.gridRows.each(function(row) {
            var assignment = controller.pendingAssignments.findAssignment({
                bl_id: row.getFieldValue('rm.bl_id'),
                fl_id: row.getFieldValue('rm.fl_id'),
                rm_id: row.getFieldValue('rm.rm_id')
            });
            var from = fromTemplate({
                rm_cat: row.getFieldValue('rm.rm_cat'),
                rm_type: row.getFieldValue('rm.rm_type')
            });
            var to = assignment.get('to_rm_type') ? toTemplate(assignment.toJSON()) : '';
            if (type=="category") {
                from = row.getFieldValue('rm.rm_cat');
                to = assignment.get('to_rm_cat');
            }
            row.setFieldValue('rm.from', from);
            row.setFieldValue('rm.to', to);
        });

        // make sure grid columns sized to fill the overlay width
        this.categoryPendingAssignments.resizeColumnWidths();
    }
    
});
