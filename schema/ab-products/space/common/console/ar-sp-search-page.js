var requestListController = View.createController("listController", {
    filter_console_afterRefresh : function() {
        top.window.location.parameters["BL_CODE"] = null;
        top.window.location.parameters["CONTRACT_CODE"] = null;
        top.window.location.parameters["LEASE_CODE"] = null;
        top.window.location.parameters["SHORTBL_CODE"] = null;
        top.window.location.parameters["BL_NAME"] = null;
        top.window.location.parameters["SITE_CODE"] = null;
        top.window.location.parameters["SHORTBLID"] = null;
        top.window.location.parameters["SITE_NAME"] = null;
    },

    tempConsole_afterRefresh : function() {
        top.window.location.parameters["BL_CODE"] = null;
        top.window.location.parameters["CONTRACT_CODE"] = null;
        top.window.location.parameters["LEASE_CODE"] = null;
        top.window.location.parameters["SHORTBL_CODE"] = null;
        top.window.location.parameters["BL_NAME"] = null;
        top.window.location.parameters["SITE_CODE"] = null;
        top.window.location.parameters["SHORTBLID"] = null;
        top.window.location.parameters["SITE_NAME"] = null;
    }

});

function openConsole(){
    var grid = View.panels.get('buildingList');
    var row = grid.rows[grid.selectedRowIndex];
    var blId = row["bl.bl_id"];
    var shortBlId = row["bl.short_bl_id"];
    var siteId = row["bl.site_id"];
    var blName = row["bl.bl_name"];
    var siteName = row["site.name"];
    top.window.location.parameters["BL_CODE"] = blId;    
    top.window.location.parameters["SHORTBL_CODE"] = shortBlId+' - '+blName;    
    top.window.location.parameters["BL_NAME"] = blName;    
    top.window.location.parameters["SITE_CODE"] = siteId+' - '+siteName;    
    top.window.location.parameters["SHORTBLID"] = shortBlId;    
    top.window.location.parameters["SITE_NAME"] = siteName;    
    var tabPanel = View.getView('parent').panels.get('spaceTabs');
    tabPanel.selectTab("spaceConsoleTab");
}

function applyRestriction(){
    var consolePanel = View.panels.get('filter_console');
    var gridPanel = View.panels.get('buildingList');
    var blId = consolePanel.getFieldValue('bl.short_bl_id');
    var siteId = consolePanel.getFieldValue('bl.site_id');
    var agId = consolePanel.getFieldValue('bl.ag_id');
    var uicId = consolePanel.getFieldValue('fl.uic_id');
    var cityId = consolePanel.getFieldValue('city.city_id');
    var regnId = consolePanel.getFieldValue('city.regn_id');
    var stateId = consolePanel.getFieldValue('bl.state_id');
    var lsId = consolePanel.getFieldValue('ls.ls_id');
    var contId = consolePanel.getFieldValue('ls.cont_id');
    var restriction = "";
    var condition = " and";
    var condRest = false;
    
    if(blId){
        if(condRest){
            restriction += condition;
        }
        restriction += " bl.short_bl_id='"+blId+"'";
        condRest = true;
    }
    if(siteId){
        if(condRest){
            restriction += condition;
        }
        restriction += " bl.site_id='"+siteId+"'";
        condRest = true;
    }        
    if(agId){
        if(condRest){
            restriction += condition;
        }
        restriction += " bl.ag_id='"+agId+"'";
        condRest = true;
    }
    if(uicId){
        if(condRest){
            restriction += condition;
        }
        restriction += " bl.bl_id in (select fl.bl_id from fl where fl.uic_id='"+uicId+"')";
        condRest = true;
    }
    if(cityId){
        if(condRest){
            restriction += condition;
        }
        restriction += " bl.city_id='"+cityId+"'";
        condRest = true;
    }
    if(regnId){
        if(condRest){
            restriction += condition;
        }
        restriction += " bl.regn_id='"+regnId+"'";
        condRest = true;
    }
    if(stateId){
        if(condRest){
            restriction += condition;
        }   
        restriction += " bl.state_id='"+stateId+"'";
        condRest = true;
    }
    if(lsId){
        if(condRest){
            restriction += condition;
        }   
        restriction += " bl.bl_id in (select bl.bl_id from bl inner join fl on fl.bl_id=bl.bl_id inner join rm";
        restriction += " on rm.fl_id=fl.fl_id and rm.bl_id=fl.bl_id and rm.ls_id='"+lsId+"')";
        condRest = true;
        top.window.location.parameters["LEASE_CODE"] = lsId;  
    }
    if(contId){
        if(condRest){
            restriction += condition;
        }
        restriction += " bl.bl_id in (select bl.bl_id from bl inner join fl on fl.bl_id=bl.bl_id inner join rm";
        restriction += " on rm.fl_id=fl.fl_id and rm.bl_id=fl.bl_id and rm.ls_id in (select ls_id from ls where cont_id='"+contId+"'))";
        condRest = true;
        top.window.location.parameters["CONTRACT_CODE"] = contId; 
    }

    gridPanel.refresh(restriction);
}

function clearRestriction(){
    var gridPanel = View.panels.get('buildingList');
    var restriction = new Ab.view.Restriction();
    gridPanel.refresh(restriction);
    top.window.location.parameters["BL_CODE"] = '';
    top.window.location.parameters["BL_NAME"] = '';
    top.window.location.parameters["SITE_CODE"] = '';
    top.window.location.parameters["LEASE_CODE"] = '';    
    top.window.location.parameters["CONTRACT_CODE"] = '';  
    top.window.location.parameters["SHORTBL_CODE"] = '';
    top.window.location.parameters["SHORTBLID"] = '';
    top.window.location.parameters["SITE_NAME"] = '';
}
