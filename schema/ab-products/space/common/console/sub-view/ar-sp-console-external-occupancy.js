var spaceExpressConsoleOrganizations1 = View.createController('spaceExpressConsoleOrganizations1', {

    /**
     * location and occupancy restrictions applied from the filter panel.
     * this is custom filter use only for current Rmstd tab.
     */
    rmFilter: null,
    
    /**
     * Last activity id.
     */
    lastExternalActId:null,

    /**
     * Last sub activity id.
     */
    lastExternalSubActId:null,
    
    /**
     * Remember what was clicked and toggle on and off the tabs tree.
     */
    lastExtrnalClicked:null,

    /**
     * Maps DOM events to controller methods.
     */
    events: {
        'click #extOccpRestrictToLocation': function() {
            this.onCheckEventExternal();
        }
    },
    
   /**
     * Constructor.
     */
    afterCreate: function() {
        this.on('app:space:express:console:orgFilter', this.refreshExternal);
    },

    afterViewLoad: function() {
        var externalOccupancyTreePanel = this.subActivityTree;
        externalOccupancyTreePanel._updateTreeNodeChildren = function(node) {
            return;
        }
        
    	this.insertExternalOccupancyFilterMessage();
    },
    
    /**
     * Insert filter message span and checkbox.
     * Add by heqiang
     */
    insertExternalOccupancyFilterMessage: function() {
    	var template = _.template('<td class="checkbox-container" id="extOccpRestriction"><input type="checkbox" id="extOccpRestrictToLocation" checked="false"/><span id="extOccpResMessageSpan">{{restrictToLocation}}</span></td>');
        Ext.DomHelper.insertHtml('afterBegin', this.subActivityTree.toolbar.tr,template(View.messages));
        Ext.fly('extOccpRestriction').setDisplayed(false);
    },
    
    /**
     * Applies the filter restriction to tree.
     */
    refreshExternal: function(filter) {
    	if (filter) {
    		this.rmFilter =  jQuery.extend(true, {}, filter);
    		abSpConsole_refreshDataFromFilter(this.subActivityTree, this.rmFilter, null);  
    	} 
	 	abSpConsole_refreshDataFromFilter(this.subActivityTree, this.rmFilter, null);
    	abSpConsole_toggleFromFilter('extOccpRestrictToLocation', ['extOccpRestriction'], 'extOccpResMessageSpan',
    			this.rmFilter.searchValuesString);
       
    },
    
    /**
     * Handle the checked or unchecked event of the filter checkbox.
     */
    onCheckEventExternal: function() {
    	abSpConsole_toggleFromCheckEvent('extOccpRestrictToLocation', ['extOccpRestriction'], 'extOccpResMessageSpan', this.rmFilter.searchValuesString);
    	abSpConsole_refreshDataFromCheckEvent('extOccpRestrictToLocation', this.subActivityTree, this.rmFilter, null);
    }

});

/**
 * Make use of the activity node content the user clicked in the activity tree to filter the drawing panel.
 */
function filterDrawingByActivity() {
    var currentNode = View.panels.get('subActivityTree').lastNodeClicked;
    var act_id = currentNode.data['ar_tiers_act.act_id'];
    
    //add the highlights toggle on and off to control the filter with or without the parameter.
    with (spaceExpressConsoleOrganizations1) {
    	if (rmFilter.parameters['rmList'] && rmFilter.parameters['rmList'].indexOf("rm.bl_id || rm.fl_id || rm.rm_id IN") != -1) {//the parameters has been set before
        	if (lastExternalActId == act_id) {//the user toggle on and off
        		if (lastExtrnalClicked == 'activity') {//the last clicked node is activity        			      	
        			rmFilter.parameters['rmList'] = " 1=1 ";        			
        			currentNode.highlightNode(false);
        		} else {
        			rmFilter.parameters['rmList'] = " rm.bl_id || rm.fl_id || rm.rm_id IN (select rm.bl_id || rm.fl_id || rm.rm_id from rm inner join ls on rm.ls_id=ls.ls_id inner join ar_tiers_ens ON ls.tn_name_ext = ar_tiers_ens.ens_id inner join ar_tiers_ssact ON ar_tiers_ens.ssact_id = ar_tiers_ssact.ssact_id inner join ar_tiers_act ON ar_tiers_ssact.act_id = ar_tiers_act.act_id where ar_tiers_act.act_id = '"+makeLiteral(act_id)+"')";
        		}
        	} else {
				rmFilter.parameters['rmList'] = " rm.bl_id || rm.fl_id || rm.rm_id IN (select rm.bl_id || rm.fl_id || rm.rm_id from rm inner join ls on rm.ls_id=ls.ls_id inner join ar_tiers_ens ON ls.tn_name_ext = ar_tiers_ens.ens_id inner join ar_tiers_ssact ON ar_tiers_ens.ssact_id = ar_tiers_ssact.ssact_id inner join ar_tiers_act ON ar_tiers_ssact.act_id = ar_tiers_act.act_id where ar_tiers_act.act_id = '"+makeLiteral(act_id)+"')";
        		lastExternalActId = act_id;
        	}
        } else {//the parameters has not been set before,just set the activity
			rmFilter.parameters['rmList'] = " rm.bl_id || rm.fl_id || rm.rm_id IN (select rm.bl_id || rm.fl_id || rm.rm_id from rm inner join ls on rm.ls_id=ls.ls_id inner join ar_tiers_ens ON ls.tn_name_ext = ar_tiers_ens.ens_id inner join ar_tiers_ssact ON ar_tiers_ens.ssact_id = ar_tiers_ssact.ssact_id inner join ar_tiers_act ON ar_tiers_ssact.act_id = ar_tiers_act.act_id where ar_tiers_act.act_id = '"+makeLiteral(act_id)+"')";
        	lastExternalActId = act_id;
        }
        lastExtrnalClicked = 'activity';
        trigger('app:space:express:console:refreshDrawing', rmFilter);
    }
}

/**
 *  Make use of the sub activity node content the user clicked in the activity tree to filter the drawing panel.
 */
function filterDrawingBySubActitvity() {
	var currentNode = View.panels.get('subActivityTree').lastNodeClicked;
	var act_id = currentNode.parent.data['ar_tiers_act.act_id'];
	var ssact_id = currentNode.data['ar_tiers_ssact.ssact_id'];
	
    //add the highlights toggle on and off to control the filter with or without the parameter.
	var drawingFilter = spaceExpressConsoleOrganizations1.rmFilter;
	with(spaceExpressConsoleOrganizations1) {
		if (lastExternalSubActId == ssact_id) {
			if (lastExternalActId == act_id) {
				if (lastExtrnalClicked == 'subActivity') {
					if (rmFilter.parameters['rmList'] && rmFilter.parameters['rmList'].indexOf('1=1') != -1) {
						rmFilter.parameters['rmList'] = " rm.bl_id || rm.fl_id || rm.rm_id IN (select rm.bl_id || rm.fl_id || rm.rm_id from rm inner join ls on rm.ls_id=ls.ls_id inner join ar_tiers_ens ON ls.tn_name_ext = ar_tiers_ens.ens_id inner join ar_tiers_ssact ON ar_tiers_ens.ssact_id = ar_tiers_ssact.ssact_id where ar_tiers_ssact.ssact_id = '"+makeLiteral(ssact_id)+"' and ar_tiers_ssact.act_id ='"+makeLiteral(act_id)+"')";
					} else {
						rmFilter.parameters['rmList'] = " 1=1 ";						
						currentNode.highlightNode(false);
					}
				} else {
					rmFilter.parameters['rmList'] = " rm.bl_id || rm.fl_id || rm.rm_id IN (select rm.bl_id || rm.fl_id || rm.rm_id from rm inner join ls on rm.ls_id=ls.ls_id inner join ar_tiers_ens ON ls.tn_name_ext = ar_tiers_ens.ens_id inner join ar_tiers_ssact ON ar_tiers_ens.ssact_id = ar_tiers_ssact.ssact_id where ar_tiers_ssact.ssact_id= '"+makeLiteral(ssact_id)+"' and ar_tiers_ssact.act_id ='"+makeLiteral(act_id)+"')";					
				}
			} else {
				rmFilter.parameters['rmList'] = " rm.bl_id || rm.fl_id || rm.rm_id IN (select rm.bl_id || rm.fl_id || rm.rm_id from rm inner join ls on rm.ls_id=ls.ls_id inner join ar_tiers_ens ON ls.tn_name_ext = ar_tiers_ens.ens_id inner join ar_tiers_ssact ON ar_tiers_ens.ssact_id = ar_tiers_ssact.ssact_id inner join ar_tiers_act ON ar_tiers_ssact.act_id = ar_tiers_act.act_id where ar_tiers_act.act_id = '"+makeLiteral(act_id)+"')";
				lastExternalActId = act_id;
			}
		} else {
			rmFilter.parameters['rmList'] = " rm.bl_id || rm.fl_id || rm.rm_id IN (select rm.bl_id || rm.fl_id || rm.rm_id from rm inner join ls on rm.ls_id=ls.ls_id inner join ar_tiers_ens ON ls.tn_name_ext = ar_tiers_ens.ens_id inner join ar_tiers_ssact ON ar_tiers_ens.ssact_id = ar_tiers_ssact.ssact_id where ar_tiers_ssact.ssact_id= '"+makeLiteral(ssact_id)+"' and ar_tiers_ssact.act_id ='"+makeLiteral(act_id)+"')";
			lastExternalActId = act_id;
			lastExternalSubActId = ssact_id
		}
		lastExtrnalClicked = 'subActivity';
		trigger('app:space:express:console:refreshDrawing', rmFilter);
	}
}

function exportExternalOccupancyTreeToXLS() {
	doExternalOccupancyTreeCustomExport(Ab.grid.ReportGrid.WORKFLOW_RULE_XLS_REPORT, 'xls');
}

function doExternalOccupancyTreeCustomExport(workflowRuleName, outputType) {
	var firstLevelDataSourceId = 'activity_tree_ds_exp';
	var secondLevelDataSourceId = 'sub_activity_tree_ds';
	var subActivityTree = View.panels.get("subActivityTree");
	
	var hasRes = Ext.getDom('extOccpRestrictToLocation').checked ? true: false;
	var restrictParam = {parameters: {'bl_id': spaceExpressConsoleOrganizations1.rmFilter.parameters['bl_id']}};
    var parameters = hasRes ? spaceExpressConsoleOrganizations1.rmFilter.parameters: restrictParam.parameters;
    var restriction = spaceExpressConsoleOrganizations1.rmFilter.restriction;
    var printableRestrictions = getPrintableRestrictions(restriction, hasRes);
    
    parameters.printRestriction = true;
	parameters.printableRestriction = printableRestrictions;
	parameters.categoryDataSourceId = firstLevelDataSourceId;
	parameters.categoryFields = getCategoryFieldsArrayForTreePanel(subActivityTree, firstLevelDataSourceId, 0);
    parameters.categoryFieldDrillDownParametersPattern = [{name:"activityClause", pattern:"ar_tiers_act.act_id='%s'"}, {name:"activityClauseTbRm", pattern:"ar_tiers_ssact.act_id='%s'"}];
    parameters.categoryFieldDrillDownRestrictionPattern = "ar_tiers_ssact.act_id='%s'";

    var subTreeCategoryFields = getCategoryFieldsArrayForTreePanel(subActivityTree, secondLevelDataSourceId, 1);
    subTreeCategoryFields.splice(0,1);
    var fieldDefs = spaceExpressConsoleCategories.activity_tree_ds_exp.fieldDefs;
    fieldDefs.items[0].title = 'Activity/SubActivity';
	var orgReport = 'External Occupancy';
	var jobId = Workflow.startJob(workflowRuleName, 
									subActivityTree._viewFile,  
			 						secondLevelDataSourceId, 
			 						orgReport, 
			 						subTreeCategoryFields,
			 						'', 
			 						parameters);
	
	doExportPanel(jobId, outputType);
}