/**
 * pop up site and drawing panel
 */
var popUpSiteController = View.createController('popUpSiteController', {

	svgControl: null,
	
    /**
     * Add click event for building layer on the drawing panel and get the object of super view.
     */
	afterViewLoad: function() {	
		// define parameters to be used by server-side job
    	var parameters = new Ab.view.ConfigObject();
		parameters['divId'] = "svgDiv";
		parameters['allowMultipleDrawings'] = 'false';
		parameters['highlightParameters'] = [{'view_file':"ab-sp-console-popup-site.axvw", 'hs_ds': "", 'label_ds':'labelBlDs'}];
    	parameters['orderByColumn'] = 'true';
		parameters['showTooltip'] = 'true' ;
		parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
									   'AssetTooltip': {handlers: [{assetType: 'bl', datasource: 'labelBlDs', fields: 'site.site_id;site.name;bl.bl_id;bl.name;bl.use1;'}]}
									 };
		parameters['events'] = [{'eventName': 'click', 'assetType' : 'bl', 'handler' : this.onClickAsset}];
    	// only construct the drawing control once
    	this.svgControl = new Drawing.DrawingControl("svgDiv", "svg_ctrls", parameters);	
    	
    	this.openerView = View.getOpenerView().controllers.get('spaceExpressConsoleLocations');
	},
	
	afterInitialDataFetch: function(){
		var siteList = this.site_ds.getRecords();
		
		//show drawing directly if only one item
		if(siteList.length==1){
			var dwgname = siteList[0].getValue('bl.dwgname');
			var siteId = siteList[0].getValue('site.site_id');
			var blList =  this.getBuildingIdList(siteId);
			
			this.loadSvg(dwgname);
			
			this.openerView.filterLocationList(blList);
		}
	},
	
    /**
     * item click.
     */
	siteGrid_onClickItem: function(row) {
		var record = row.record;
		var dwgname = record['bl.dwgname'];
		var siteId = record['site.site_id'];
		var blList =  this.getBuildingIdList(siteId);
		
		this.loadSvg(dwgname);

		this.openerView.filterLocationList(blList);
    },

	loadSvg: function(dwgname) {

		var parameters = new Ab.view.ConfigObject();
    	parameters['pkeyValues'] = {'bl_id':'', 'fl_id': ''};
    	parameters['drawingName'] = dwgname;
		parameters['showTooltip'] = 'true';

    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);	
    	
    },
    
    onClickAsset: function(params, drawingController){
    	// toggle the room if clicked
    	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId']);
		var blList = [params['assetId']];
		popUpSiteController.filterLocationList(blList);
    },
    
    /**
     * get an array of building code.
     */
    getBuildingIdList: function(siteId){
    	
		var buildingDs = View.dataSources.get("building_site_ds");
		buildingDs.addParameter('siteId', siteId);
		var buildingIdList = buildingDs.getRecords();
		var arrBuildingId = [];
		//convert to a pure array
		_.each(buildingIdList, function (buildingId) {
			arrBuildingId.push(buildingId.getValue("bl.bl_id"));
		});
		return arrBuildingId;
	},
	
    /**
     * filter location list and close the pop up view.
     */
	filterLocationList: function(blList) {
		
		this.openerView.filterLocationList(blList);
		
		View.closeThisDialog();
    }
    
});

