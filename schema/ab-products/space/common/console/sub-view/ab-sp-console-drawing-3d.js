/**
 * Added 3D Navigator feature to 23.2 Space Console, on Marth, 2017. 
 * By Zhang Yi.
 */
var spaceExpressConsoleDrawing3D = View.createController('spaceExpressConsoleDrawing3D', {
	bim3d: null,
	images: [],
	selectedFloorRow: null,
	openedInfo:null,
	currentBuilding: null,
	currentHighlightDS: null,
	currentLabelDS: null,
	floorsSelectorPanel: null,
	openedLevelCodes: null,

    // revit category ids
    afm_bim_categories: null,

	/**
     * Whether show current 3d drawing panel.
     */
	show: true,

    /**
     * Checked floor rows without 3d model.
     */
	none3dRows: [],

	/**
     * Sign to indicate if current 3d navigator control has already loaded a drawing. 
	 * The reason to add this sign is becuase if check a floor on 2d mode which means the 3d drawing panel is hiden, 
	 * the loadFloorPlan function will run into an error and thus the 3d navigator control won't work anymore even later switching to 3d mode manually. 
	 * So when 'neverLoaded' is true, then don't perform drawing load operation and directly return from loadFloorPlan. 
	 * In show3D funciton to manually load the drawing in 3d navigator control for first time and set neverLoaded to false. 
     */
	neverLoaded: true,
	
    /**
     * Filter parameters from filter panel.
     */
	filter: null,

	afterCreate: function() { 
		//register JS events: name and event call 
		this.on('app:space:express:console:selectLocation', this.loadFloorPlan);
	    this.on('app:space:express:console:show3D', this.show3D);
	    //bim space console 
	    this.on('app:space:express:console:bim:updateHighlight', this.updateHighlight);					   
	    this.on('app:space:express:console:locateEmployeeOrRoom', this.locateRoom);					   
        this.on('app:space:express:console:loadAndHighLightSelectedEmployee', this.loadAndHighlightSelectedEmployee);
        this.on('app:space:express:console:loadAndHighLightSelectedRoom', this.loadAndHighlightSelectedRoom);
        this.on('app:space:express:console:highlightSelectedRoom', this.locateRoom);

        this.on('app:space:express:console:refreshDrawing', this.refreshDrawing);
	},

	initial3D: function() { 
		this.initialVariables();
		this.initialSelectors();
		this.initialBim3d();
		this.initialDrawingPanel3D();
    },							

//	----------------STRAT Initial 3d drawing control, 3d drawing panel and current 3d drawing controller's  variables  ----------------.

	initialVariables: function(){
		this.openedInfo = {};
		this.openedLevelCodes = {};
		this.floorsSelectorPanel = View.panels.get("locationsGrid");
	},


    initialSelectors: function(){
		//create isolate, highlight, label and  plan type selectors
		var titleDiv = document.getElementById('drawingPanel3D_title');
		Ab.bim3d.Utility.appendSelector(titleDiv, 'isolate', "Isolates:", '', [new Option('Rooms', '-2000160'), new Option('Furniture', '-2000080')], this.isolate.bind(this));

		{//add highlights selector
			var highlightOptionsValue = ['highlightDivisionsDs', 'highlightDepartmentsDs', 'highlightCategoriesDs', 
				'highlightTypesDs', 'highlightSuperCategoriesDs', 'highlightRoomStandard', 'highlightEmployeesDs'];
			var highlightOptionsText = ['highlightDivisionsDsText', 'highlightDepartmentsDsText', 'highlightCategoriesDsText', 
				'highlightTypesDsText', 'highlightSuperCategoriesDsText', 'highlightStandardsDsText', 'highlightOccupancyDsText'];
			var highlightOptions = [];
			for(var i=0; i<highlightOptionsValue.length; i++){
				highlightOptions.push(new Option(getMessage(highlightOptionsText[i]), highlightOptionsValue[i]));
			}
			Ab.bim3d.Utility.appendSelector(titleDiv, 'hilite3d', "Highlights:", '',  highlightOptions,  this.dataSourceUpadte.bind(this));
		}


		{//add label selector
			var labelOptionsValue = ['labelDivisionsDs', 'labelDepartmentDs', 'labelCategoriesDs', 
				'labelTypesDs', 'labelSuperCategoriesDs', 'labelRoomStandardDs', 'labelEmployeesDs', 
				'labelRoomNumberDs'];
			var labelOptionsText = ['highlightDivisionsDsText', 'highlightDepartmentsDsText', 'highlightCategoriesDsText', 
				'highlightTypesDsText', 'highlightSuperCategoriesDsText', 'highlightStandardsDsText', 'labelEmployeeDsText', 
				'labelRoomNumberDsText'];
			var labelOptions = [];
			for(var i=0; i<labelOptionsValue.length; i++){
				labelOptions.push(new Option(getMessage(labelOptionsText[i]), labelOptionsValue[i]));
			}
			Ab.bim3d.Utility.appendSelector(titleDiv, 'labels3d', "Labels:", '', labelOptions, this.dataSourceUpadte.bind(this));
		}

		{
			//add plan type selector
			var planTypeOptions = [], planTypes=[];
			var dataSource = View.dataSources.get('planTypeGroupsDs');
			var records = dataSource.getRecords(), record;
			for(var i=0, ln=records.length; i<ln; i++){
				record = records[i];
				planTypeOptions.push(new Option(record.getValue('active_plantypes.title'), record.getValue('plantype_groups.plan_type')));
				planTypes.push({id: record.getValue('plantype_groups.plan_type'), view_file: record.getValue('active_plantypes.view_file'), hs_ds:  record.getValue('active_plantypes.hs_ds'), label_ds: record.getValue('active_plantypes.label_ds')});
			}
			Ab.bim3d.Utility.appendSelector(titleDiv, 'planTypes', "Plan Types:", '', planTypeOptions, this._doPlanType.bind(this, planTypes));
		}
	 },

    initialBim3d: function(){
		//create 3d navigator object
		this.bim3d = new Ab.bim3d.Navigator('bim3d');
		
		//initial categories panel
		this.initialBim3dCategoriesPanel();

		//update json file name macros at runtime - important call!!!
		this.initialBim3dCategoriesListener();	
		
		//initial context menu and listeners of bim3d
		this.initialBim3dContextMenu();	
		
		//disable viewer control's click or double click
		this.bim3d.setObjectDoubleClickable(false);
		//this.bim3d.setObjectClickable(false);
   },
		
    initialBim3dCategoriesPanel: function(){
        //use category ids rather than its names
        this.bim3d.setCategoryFieldName('CategoryId');

		//categories panel
		this.bim3d.setSuperCategories([{title:'Exterior/Core', name:'Exterior', superCategories:[ {title:'Site', name: 'Site', codes:['-2001340','-2001260','-2001220','-2001180', '-2001263'], json:'{building}-site'}, {title:'Shell',  name: 'Shell', codes:['-2000035','-2000011','-2000170', '-2000023', '-2000151', '-2000171', '-2001330'], functions:['Exterior', 'Foundation', 'Retaining', 'Coreshaft'], json:'null'}, {title:'Core',  name: 'Core',  codes:['-2000032','-2000120','-2000126','-2000038'], json:'null'}]},  
		             {title:'Disciplines',  name: 'Disciplines', superCategories:[{title:'HVAC',  name: 'HVAC',   codes:['-2008000','-2008010','-2008015','-2008020','-2008013','-2001140'], json:'{building}-hvac'},{title:'Piping', name: 'Piping',   codes:['-2008050','-2008043','-2001160', '-2008055', '-2008049', '-2008044'], json:'{building}-plumbing'}]},
	 	             {title:'Floor Elements', name: 'Floor',  superCategories:[{title:'Interior Walls',  name: 'Interior Walls',   codes:['-2000011'], functions:['Interior'], dual:'true', json:''}, {title:'Floors',  name: 'Floors',  dual:'true',  codes:['-2000032'], functions:['Interior'], json:''}, {title:'Ceilings', name: 'Ceilings', dual:'true',  codes:['-2000038'], functions:['Interior'], json:''} , {title:'Curtain Panels',  name: 'Curtain Panels',  codes:['-2000170'], functions:['Interior'], dual:'true', json:''} , {title:'Doors',  name: 'Doors',  codes:['-2000023'], functions:['Interior'], dual:'true', json:''}, {title:'Lighting',  name: 'Lighting',  codes:['-2008087', '-2001120', '-2001060'], dual:'false', functions:['Interior'],json:'{building}-lighting'}]},
	 	             {title:'Assets',  name:'Assets', superCategories:[{title:'Rooms',   name: 'Rooms', codes:['-2000160'], functions:['Interior'],json:''},{title:'Equipment',    name: 'Equipment', codes: ['-2008085','-2008079', '-2001350' ], functions:['Interior'], json:''},{title:'Furniture',  name: 'Furniture',  codes: ['-2000080'], functions:['Interior'], json:''}]}
	 	            ]);
	},

    initialBim3dCategoriesListener: function(){
		var scope = this;
	
		scope.bim3d.categoriesPanel.setClickListener(function(link, name, jsonObj, levelsObj, panel){
			if(typeof jsonObj !== 'undefined' && jsonObj !== null && jsonObj.json !== 'null' && jsonObj.json !== ''){
				jsonObj.json = jsonObj.json.replace('{building}', scope.currentBuilding);
			}
			
			//if synch Core and dual visibility Floors and Ceilings  
			if(name === 'Core'){
				scope.bim3d.categoriesPanel.changeLinkStatusById('Floors', link.className === 'active');
				scope.bim3d.categoriesPanel.changeLinkStatusById('Ceilings', link.className === 'active');
			}

			//get opened levels for the navigator if users click any items over Floor Elements and Assets
			if(link.name.indexOf('Floor') >=0  || link.name.indexOf('Assets') >=0 ){
				var row, rows = scope.floorsSelectorPanel.rows, levelCodes=[];
				for(var i=0, ln=rows.length; i<ln; i++){
					row = rows[i];
					if(row.row.isSelected()){
						levelCodes.push(scope.openedLevelCodes[row['rm.fl_id']]);
					}
				}
				levelsObj.levels = levelCodes;
			}
			
			if(link.id.indexOf('Lighting') >=0 && jsonObj !== null && jsonObj.json !== 'null' && jsonObj.json !== ''){
				panel.setJsonLoadListener(function(scenes, json, type, codes, functions, levels, show){
					scope.bim3d.showCategories(scope.bim3d.categoriesPanel.getAllCategoryCodes(true), levels, true, true, true, scenes);
				});
			}
		});
	},

	initialBim3dContextMenu: function(){
		var scope = this;
		//listener for context menu click event
		scope.bim3d.contextMenus.setClickListener(function(actionId){
			 if(actionId.indexOf('clearSelection') >= 0 ){
				 scope.closeRmDetailReportDialog();
			 }
		});
		
		//disable some context menu
		this.bim3d.contextMenus.setFilter(['hideSelected', 'hideSimilar', 'showAll']);
		
		//demo only, add custom context menus and make them show or hide depending on selected object's type
		this.bim3d.contextMenus.setShowListener(function(menus){
			//add custom menus based on object selection
			var asset = scope.bim3d.getSelectedObjectAssetType();
			var primaryKey = scope.bim3d.getSelectedObjectPrimaryKey();
			if(primaryKey){
				primaryKey = primaryKey.split(';');
				if(asset ==='rm'){
					menus.push(null);
					menus.push( {label:'Show Room Contents', id:'showroomcontents_rm', actions:[function() {
                		 var url = 'ab-revit-overlay-rm.axvw?fieldName=rm.bl_id&fieldValue='+primaryKey[0]+'&fieldName2=rm.fl_id&fieldValue2='+primaryKey[1]+'&fieldName3=rm.rm_id&fieldValue3='+primaryKey[2];
                		 View.openDialog(url, '', false, {width : 1200, height : 800});
                		 //window.open(url, '_blank', 'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,width=600,height=600');
	                 }]});
				}else if(asset ==='eq'){
					menus.push(null);
					menus.push( {label:'Show Equipment Information', id:'ShowEquipmentSystemInformation_eq', actions:[function() { 
                		 var url = 'ab-eq-system-console.axvw?eq.eq_id='+primaryKey[0];
                		 View.openDialog(url, '', false, {width : 1200, height : 800});
                		 //window.open(url, '_blank', 'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,width=600,height=600');
	                 }]} );
					menus.push(null);
					menus.push( {label:'Create Work Request', id:'CreateWorkRequest_eq', actions:[function() { 
                		 var url = 'ab-bldgops-console-wr-create.axvw?eq.eq_id='+primaryKey[0];
                		 View.openDialog(url, '', false, {width : 1200, height : 800});
                		 //window.open(url, '_blank', 'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,width=600,height=600');
	                 }]} );
			    }
			}
		});
	},

	initialDrawingPanel3D: function(){
		var scope = this;
		this.drawingPanel3D.setContentPanel(Ext.get('bim3d'));
		//html panel resize
		this.drawingPanel3D.syncHeight = function(){
			scope.bim3d.resize();
		}
	},

//	----------------END Initial 3d navigator control, 3d drawing panel and current 3d drawing controller's  variables  ----------------.

	
//	----------------START event handler functions registered for 3d navigator control----------------.
	closeRmDetailReportDialog: function(){
    	var reportView = View.panels.get("rm_detail_report");
    	if(reportView){
    		reportView.closeWindow();
    	}
    },
        
 	/**
 	 * called by isolate selector
 	 */
 	isolate: function(e, combo){
 		var scope = this;
 		var value = combo.value;
 		switch(value) {
	 	    case 'None':
	 	    	scope.bim3d.clearIsolated();
	 	        break;
	 	    case 'Highlight':
	 	    	scope.bim3d.isolateByMaterial('ab-highlight');
	 	    	break;
	 	    case '-2000160':
	 	    	scope.bim3d.isolate([value]);
	 	    	break;
	 	   case '-2000080':
	 	    	scope.bim3d.isolate([value]);
	 	    	break;
	 	   case 'Selected':
	 		  scope.bim3d.isolateByMaterial('ab-selection');
	 	    	break;
	 	    default:
	 	       //do nothing
 		} 
 	},
 	
 	/**
 	 * called by highlight and label selectors
 	 */
 	dataSourceUpadte: function(e, combo){
 		var scope = this;
 		var tmp = combo.value;
 		if (combo.id.lastIndexOf('hilite3d') >= 0) {
			if (tmp === scope.currentHighlightDS){
				return;
			}
			scope.currentHighlightDS = tmp;
			if(scope.selectedFloorRow === null){
	 			return;
	 		}
			if(tmp === 'None'){
				scope.bim3d.clearHighlight(true);
				return;
			}
			if(scope.currentHighlightDS !== null){
				//clear before highlighting
				scope.bim3d.clearHighlight(true);
			}
			var restriction = new Ab.view.Restriction();
	     	restriction.addClause('rm.bl_id', scope.selectedFloorRow['rm.bl_id'] );
	     	//highlight objects
	     	scope.bim3d.thematicHighlight('rm', 'ab-sp-console-ds-highlights.axvw', scope.currentHighlightDS, toJSON(restriction), null);
			
 		}else if (combo.id.lastIndexOf('labels3d') >= 0) {
	    	if (tmp === this.currentLabelsDS){
	    		return;
	    	}
	    	scope.currentLabelDS = tmp;
	    	if(scope.selectedFloorRow === null){
	 			return;
	 		}
	    	
	    	if(tmp === 'None'){
				scope.bim3d.clearLabels();
				scope.bim3d.render();
				return;
			}
	    	
	    	if(scope.currentLabelDS !== null){
				//clear before labeling
				scope.bim3d.clearLabels();
			}
	    	//label objects
	    	scope._labeling();
 		}
 	},
 	
 	//called by plan type selector
 	_doPlanType: function(planTypes, e, combo){
 		var scope = this;
 		if(scope.selectedFloorRow === null){
			return;
		}
		if(combo.value === 'None'){
			scope.bim3d.clearLabels();
			scope.bim3d.clearHighlight(true);
			return;
		}
		for(var i=0, ln=planTypes.length; i<ln; i++){
			if(combo.value === planTypes[i].id){
				scope.bim3d.startSpinning();
				var restriction = new Ab.view.Restriction();
		     	restriction.addClause('rm.bl_id', scope.selectedFloorRow['rm.bl_id'] );
		     	//highlight objects
		     	scope.bim3d.thematicHighlight('rm', planTypes[i].view_file, planTypes[i].hs_ds, toJSON(restriction), null);
		     	//get label dataSource object
		     	var labelDS = Ab.data.loadDataSourceFromFile(planTypes[i].view_file, planTypes[i].label_ds);
		     	scope.bim3d.clearLabels();
		     	scope._doLabeling(labelDS);
				break;
			}
		}
 	},
 	
 	//label 3D objects
 	_labeling: function(){
 		this.bim3d.startSpinning();
 		var labelDSObj  = View.dataSources.get(this.currentLabelDS);
    	this._doLabeling(labelDSObj );
 	},

 	_doLabeling: function(labelDSObj){
 		var scope = this;
 		var fieldNames = labelDSObj.fieldDefs.keys;
    	for(var i=fieldNames.length - 1; i>=0; i--){
    		if(fieldNames[i].indexOf('.bl_id') > 0 || fieldNames[i].indexOf('.fl_id') > 0){
    			fieldNames.splice(i, 1);
    		}
    	}
 		var restriction = new Ab.view.Restriction();
    	restriction.addClause('rm.bl_id', scope.selectedFloorRow['rm.bl_id'], '=');
    	var records = labelDSObj.getRecords(restriction);
    	if(records.length === 0){
    		//clear labels
    		scope.bim3d.clearLabels();
			scope.bim3d.render();
			
    		//stop the spinning
    		scope.bim3d.stopSpinning();
    		return;
    	}
    	var record, labels={};
    	for(var i=0, ln=records.length; i<ln; i++){
    		record = records[i];
    		var value='';
    		for(var j=0, lj=fieldNames.length; j<lj; j++){
    			value += record.getValue(fieldNames[j]);
    			if(j !== lj - 1){
    				value += '\n';
    			}
    		}
    		
    		labels[record.getValue('rm.bl_id') +';'+ record.getValue('rm.fl_id') +';'+  record.getValue('rm.rm_id')] = value;
    	}
    	if(Object.keys(labels).length > 0){
    		//add labels to the navigator, the labels will be displayed
    		scope.bim3d.addLabels('-2000160', labels, function(){
    			scope.bim3d.stopSpinning();
    		});
    	}else{
    		scope.bim3d.stopSpinning();
    	}
    	labels = null;
 	},

 	
 	_getSelectedFloorSelectPanelRestriction: function(bl_id, rows, selectedRows){
 		var restriction = new Ab.view.Restriction();
 		restriction.addClause('rm.bl_id', bl_id, '=');
		
		for(var i=0, ln=rows.length; i<ln; i++){
			if(rows[i].row.isSelected()){
				selectedRows.push(rows[i]);
			}
		}
		for(var i=0, ln=selectedRows.length; i<ln; i++){
			if(i === 0){
				restriction.addClause('rm.fl_id', selectedRows[i]['rm.fl_id'], '=', ') AND (');
			}else{
				restriction.addClause('rm.fl_id', selectedRows[i]['rm.fl_id'], '=', 'OR');
			}
		}
		if(selectedRows.length === 0){
			restriction = new Ab.view.Restriction();
			restriction.addClause('rm.bl_id', '', 'IS NULL');
		}
		return restriction;
 	},
//	----------------END event handler functions registered for 3d navigator control----------------.
 	
//	----------------START event handler functions registered for current 3d drawing controller----------------.
 	/*
 	 * Loads specified floor plan.  Most codes are to synchronize visibilities.
 	 */
    loadFloorPlan: function(row, dps){
		if ( this.neverLoaded )	{
			return;
		}

    	var highlightRestriction = new Ab.view.Restriction();
    	highlightRestriction.addClause('rm.bl_id', row['rm.bl_id'], '=');
    	//highlightRestriction.addClause('rm.fl_id', row['rm.fl_id'], '=');
    	var selector = document.getElementById('selector_hilite3d');
    	if(selector!==null){
			this.currentHighlightDS = selector.value;
		}
    	if(typeof dps !== 'undefined' && dps[0] !== ''){
    		highlightRestriction.addClause('rm.dp_id', dps, 'IN');
    		if(selector!==null){
    			selector.value = 'highlightDepartmentsDs';
    			this.currentHighlightDS = 'highlightDepartmentsDs';
    		}
    	}
    	    	
    	//get floor drawing name from table afm_enterprise_graphics
    	var dwgDatasource  = View.dataSources.get('3d_drawings_ds');
    	var restriction = new Ab.view.Restriction();
    	restriction.addClause('afm_enterprise_graphics.bl_id', row['rm.bl_id'], '=');
    	restriction.addClause('afm_enterprise_graphics.fl_id', row['rm.fl_id'], '=');
		restriction.addClause("afm_enterprise_graphics.file_type", '3DBIN', '=');
    	var record = dwgDatasource.getRecord(restriction);
    	var model_level = record.getValue('afm_enterprise_graphics.model_level');
    	var file_type = record.getValue('afm_enterprise_graphics.file_type');
    	if(model_level === null || model_level === '' ||  model_level === undefined || '3DBIN'!=file_type){
    		//row.row.unselect()
			if ( row.row.isSelected() && this.show ) {
				this.none3dRows.push(row);
				View.alert( String.format(View.getLocalizedString(this.bim3d.z_MESSAGE_NO_3D_MODEL), row['rm.bl_id']+row['rm.fl_id']) );
				//Ab.view.View.showMessage('error', String.format(View.getLocalizedString(this.bim3d.z_MESSAGE_NO_3D_MODEL), row['rm.bl_id']+row['rm.fl_id']) );
			}
    		return;
    	}

		// when uncheck a floor, remove it from none3dRows.
		if (!row.row.isSelected()) {
			if ( this.none3dRows && this.none3dRows.length>0 ) {
				var newNone3dRows = [];
				for (var i=0;  i<this.none3dRows.length; i++) {
					var row1 = this.none3dRows[i];
					if ( row1['rm.bl_id']!=row['rm.bl_id'] || row1['rm.fl_id']!=row['rm.fl_id'] ) {
						newNone3dRows.push(row1);
					}
				}
				this.none3dRows = newNone3dRows;
			}
		}
    	    	
    	this.closeRmDetailReportDialog();
    	this.selectedFloorRow = row;
		{
			//switch building case to un-select the rest
			var selectedBuilding = this.selectedFloorRow['rm.bl_id'];
			var rows = this.floorsSelectorPanel.rows;
			for(var i=0, ln=rows.length; i<ln; i++){
				if(rows[i]['rm.bl_id'] !== selectedBuilding){
					//rows[i].row.unselect();
				}
			}
		}
		
		var selectedRows = [];
		var restriction =  this._getSelectedFloorSelectPanelRestriction(row['rm.bl_id'], rows, selectedRows);
    	this.trigger('app:bim:rooms:example:openSelectRoomPanel', restriction);
    	
    	var scope = this;
    	var bl_id = row['rm.bl_id'].toLowerCase();
    	var fl_id = row['rm.fl_id'].toLowerCase();
    	      	
      	var excludedCategories = [];
      	if(scope.bim3d.categoriesPanel.isReady()){
      		//all not active categories on categories panel
      		excludedCategories = scope.bim3d.categoriesPanel.getAllCategoryCodes(false);
      	}else{
      		//default
	      	//var excludedCategories = ['Ceilings', 'Floors', 'Curtain Panels', 'Walls', 'Lighting Fixtures', 'Doors'];    	
      		excludedCategories = ['-2000038', '-2000032', '-2000170', '-2000011', '-2001120', '-2000023', '-2000151', '-2000171', '-2001330'];
      	}
    	
    	//level code from afm_enterprise_graphics table
    	this.openedLevelCodes[row['rm.fl_id']] = model_level;
    	var levelCode = model_level.toUpperCase();
    	
		if(bl_id in scope.openedInfo && scope.openedInfo[bl_id].indexOf(fl_id) >=0){
			//floor panel is already loaded
			if(scope.currentHighlightDS !== null){
				if(scope.currentHighlightDS !== 'None'  && scope.currentHighlightDS !==''){
					//highlight based on if highlight selector
					scope.bim3d.thematicHighlight('rm', 'ab-sp-console-ds-highlights.axvw', scope.currentHighlightDS,  toJSON(highlightRestriction), null);
				}
			}
			
			//show or hide the level
			if( !row.row.isSelected()){
				excludedCategories = [];
			}

			scope.bim3d.showLevel(levelCode, excludedCategories, row.row.isSelected(), true);

			return;
		}
    	
    	var file_name = record.getValue('afm_enterprise_graphics.filename').toLowerCase();
    	if(scope.currentBuilding !== bl_id){
    		//switch building case
    		scope.bim3d.categoriesPanel.clearOpenedJsons();
    		
        	this.bim3d.resize(false);
        	
    		//not opened or switched
    		scope.currentBuilding =  bl_id;
    		if(bl_id in scope.openedInfo){
    			//opened before
    			if(scope.currentHighlightDS !== null){
    				//clear before highlighting
    				scope.bim3d.clearHighlight(true);
    			}
    		}
    		
			scope.openedInfo[bl_id] = [fl_id];
			//load shell
			scope.bim3d.load(bl_id+'-shell', true, function(scenes){
				//turn off site from categories panel
				scope.bim3d.categoriesPanel.changeLinkStatusById('Site', false);
				//if shell is turn off from categories panel, hide shell
				if(!scope.bim3d.categoriesPanel.changeLinkStatusById('Shell')){
					scope.bim3d.showCategories(['-2000035','-2000011','-2000170', '-2000023', '-2000151', '-2000171', '-2001330'], null, false, false, false, scenes);
				}
				if(scope.bim3d.calls === 1){
					//first time load a building, turn off floor elements from categories panel
					scope.bim3d.categoriesPanel.loopTargetLinks('Floor', function(alink, id){
						alink.className = 'inactive';
				     });
				}
				//load core
				scope.bim3d.load(bl_id+'-core', false, function(scenes){
					//if core is not active, hide it.
					if(!scope.bim3d.categoriesPanel.changeLinkStatusById('Core')){
						scope.bim3d.showCategories(['-2000032','-2000120','-2000126','-2000038'], null, false, false, false, scenes);
					}
					//turn off discipline from categories panel
					scope.bim3d.categoriesPanel.loopTargetLinks('Disciplines', function(alink, id){
						alink.className = 'inactive';
				     });
					
					//load the level
					scope.bim3d.load(file_name, false, function(scenes){
						if(scope.currentHighlightDS !==null && scope.currentHighlightDS !=='None' && scope.currentHighlightDS !==''){
							//highlight
							scope.bim3d.thematicHighlight('rm', 'ab-sp-console-ds-highlights.axvw', scope.currentHighlightDS,  toJSON(highlightRestriction), null);
						}
						if(scope.currentLabelDS !==null && scope.currentLabelDS !=='None'){
							//labeling: defer a little while so browser can breathe
							scope._labeling.defer(100, scope);
						}
						//show specified level without showing its interior walls, curtain panels and doors.
						scope.bim3d.showLevel(levelCode, excludedCategories, true, true);		
					});
				});
			});
    	}else{
    		//building is already  loaded and the floor plan is not loaded yet
    		var floors = scope.openedInfo[bl_id];
    		if(floors.indexOf(file_name) >= 0){
    			//opened
    			//scope.trigger('app:bim:rooms:example:openSelectRoomPanel', row);
    		}else{
    			//not opened
    			floors.push(file_name);
    			floors.push(fl_id);
    			//load the level
    			scope.bim3d.load(file_name, false, function(scenes){
    				if(scope.currentHighlightDS !==null && scope.currentHighlightDS !=='None'  && scope.currentHighlightDS !==''){
    					scope.bim3d.thematicHighlight('rm', 'ab-ex-bim-3d-rm-example-navigator.axvw', scope.currentHighlightDS,  toJSON(highlightRestriction), null);
					}
    				if(scope.currentLabelDS !==null && scope.currentLabelDS !=='None'){
    					//labeling: defer a little while so browser can breathe
        				scope._labeling.defer(100, scope);
        			}
    				scope.bim3d.showLevel(levelCode, excludedCategories, true, true);
				});
    		}
    	}
    },
   
 	updateHighlight: function(){
 		var scope = this;

		var highlightParameters =  (scope.filter && scope.filter.parameters) ? scope.filter.parameters : null;

		var labelDSObj = null; 
		if (scope.currentLabelDS) {
			labelDSObj  = View.dataSources.get(this.currentLabelDS);
			if (labelDSObj) {
				labelDSObj.addParameters(highlightParameters);
			}
		}
				
		if (scope.floorsSelectorPanel) {
			var checkedRows = this.floorsSelectorPanel.getSelectedRows();
			for (var i=0; i<checkedRows.length; i++) {
				var row = checkedRows[i];
				var restriction = new Ab.view.Restriction();
				restriction.addClause('rm.bl_id', row['rm.bl_id'] );

				scope.bim3d.thematicHighlight('rm', 'ab-sp-console-ds-highlights.axvw', scope.currentHighlightDS,  toJSON(restriction), highlightParameters);
			}
			
			if ( labelDSObj ) {
				scope.bim3d.clearLabels();
				scope._doLabeling(labelDSObj);
			}
		}
 	},

    /**
     * Highlight room for selected employee to locate.
     */
    loadAndHighlightSelectedEmployee: function(employee) {
    	var row = this.employeeGrid.rows[this.employeeGrid.selectedRowIndex];
	    this.loadFloorPlan(row);
	   
	    this.locateRoom(row);
    },
	
    /**
     * Load drawing plan for locate room.
     */
    loadAndHighlightSelectedRoom: function(room) {
    	var row = this.roomsGrid.rows[this.roomsGrid.selectedRowIndex];
    	this.loadFloorPlan(row);

		this.locateRoom(row);
    },

	locateRoom: function(row){
		if (this.show && this.bim3d) {
			var scope = this;
			var pkValue = row['rm.bl_id'] ? ( row['rm.bl_id'] + ';' + row['rm.fl_id'] + ';' + row['rm.rm_id'])  : (row.bl_id+";"+row.fl_id+";"+row.rm_id) ;
			//set selected object
			scope.bim3d.getObjectByPrimaryKey(pkValue, function(object){
				//select
				scope.bim3d.select(object.id);
				scope.bim3d.render();
				//zoom in
				//scope.bim3d.home();
				//scope.bim3d.zoomIn(object);
			});
		}
	},
 	
    show3D: function(){
		this.show = true;
		this.drawingPanel3D.show(true);

		if ( this.neverLoaded ) {
			var self = this; 
			jQuery.getScript('/archibus/schema/ab-core/controls/bim3d/ab-bim-3d-navigator.js').done( function(){ 
				self.initial3D(); 
				self.neverLoaded = false; 
				var checkedRows = self.floorsSelectorPanel.getSelectedRows(); 
				for (var i=0; i<checkedRows.length; i++) { 
					var row = checkedRows[i]; 
					self.loadFloorPlan(row); 
				} 
			}); 
		}	

	  /*
		// if existing previously checked rows without 3d model, show alert message.
		if ( this.none3dRows.length>0 ) {
			var ids = "";
			for (var i=0;  i<this.none3dRows.length; i++) {
				var row = this.none3dRows[i];
				ids +=	row['rm.bl_id'] + row['rm.fl_id']+"; ";
			}
			Ab.view.View.showMessage('error', String.format(View.getLocalizedString(this.bim3d.z_MESSAGE_NO_3D_MODEL), ids) );
		}
	*/
	},

    switchTo2D: function(){
		this.show = false;
		this.drawingPanel3D.show(false);
        this.trigger('app:space:express:console:show2D');
	},

    /**
     * Applies filter and refresh 3d Drawing.
     */
    refreshDrawing: function(filter) {
		if ( filter ) {
			this.filter = filter;
		}

		if ( this.show ) {
			this.updateHighlight();
		}
    },
//	----------------END event handler functions registered for current 3d drawing controller----------------.
});




