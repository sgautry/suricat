﻿/**
 * Controller for the drawing panel.
 *
 * TODO: save and restore the employeesWaiting collection into the sidecar.
 * See the pendingAssignments collection in ab-sp-console-assignments.js.
 *
 * Events:
 * app:space:express:console:commitAssignment
 * app:space:express:console:cancelAssignment
 * app:space:express:console:viewAssignment
 * app:space:express:console:selectRoom
 * app:space:express:console:viewSelectedRooms
 * app:space:express:console:moveEmployee
 * app:space:express:console:removeEmployeeWaiting
 */
jQuery.noConflict(); 
var spaceExpressConsoleDrawing = View.createController('spaceExpressConsoleDrawing', {

    /**
     * A copy of the parent view mode.
     */
    mode: '',

    /**
     * The object that describes what rooms are assigned to: a department, a room type, or employees.
     */
    assignmentTarget: null,

    /**
     * The Ab.space.express.console.Assignments collection of pending assignments.
     */
    pendingAssignments: null,
    
    /**
     * The objects contains all the selected rooms.
     */
    selectedRooms: null,
    
    /**
     * Rooms for assignment.
     */
    assignedRooms: null,
    
    /**
     * Rooms for employees moving for highlighting.
     */
    movedEmployeesRooms: null,

    /**
     * The array of {bl_id, fl_id, dwgname} for displayed floors.
     */
    selectedFloors: null,

    /**
     * The array of em_id values for employees that are currently on the bus.
     */
    employeesWaiting: null,

    /**
     * Set to true after the employee panel has been created.
     */
    assetPanelCreated: false,

    /**
     * Location and occupancy restrictions applied from the filter panel.
     */
    filter: null,
    
    /**
     * filter for last time.
     */
    lastFilter: null,
    
    /**
     * Selected grid rows
     */
    selectedEmployeeRows: null,
	
	/**
	 * Number of selected grid rows
	 */
	totalSelectedEmployeeRows: 0,
	
	/**
	 * The flag to indicate whether the user is locate an employee or a room.
	 */
	isLocateEvent: false,
	
	/**
	 * The stored team id for locate.
	 */
	locateTeamId: null,

	/**
	 * The element for the check box of show room standard.
	 */
	openRoomStandardAction:null,
	
	/**
	 * An action element copy of room standard for performance.
	 */
	copyOfRoomStandardElement:null,
	
	/**
	 * The current selected plan type.
	 */
	selectedPlanType: null,
	
	/**
	 * The current selected legend datasource.
	 */
	currentLegendDataSource: null,
	
	/**
	 * The current legend panel.
	 */
	currentLegendPanel: null,

	/**
	 * Suffix of drawing files according to user selected background and its option value as well.
	 */
	backgroundSuffix:null,
	backgroundOptionValue:null,

	/**
	 * Current asOfDate .
	 */
	asOfDate:getCurrentDateInISOFormat(),

	/**
	 * Current selected start date.
	 */
	selectDateStart:null,
	
	/**
	 * Current selected end date.
	 */
	selectDateEnd:null,

	originalHighlightTeamDs:null,
	originalLabelTeamDs:null,

    /**
     * For 23.1: sign of whether 'Team Space' function is enabled
     */
	isTeamSpaceEnabled: false, 

    /**
     * Added for 23.2 html5 conversion
     */
    drawingControl:null,
    assetPanelLoaded: false,

    /**
     * Added for 3D Navigator feature: whether show current 2d drawing panel.
     */
    show: true,
    /**
     * Added for 3D Navigator feature: whether has 3d license.
     */
    has3dLicense: false,

    /**
     * Constructor.
     */
    afterCreate: function() {
        this.initializeVariables();
        this.initializeEventListener();
    },
    
    /**
     * After view is loaded, we will set the UI state and register event for the drawing panel.
     */
    afterViewLoad: function() {
        this.initialSvgDrawingCtrl();
        //$('bim3d').style.display="none";
        //$('drawingDiv').style.display="none";
        //this.initial3dDrawingCtrl();
        
        //change the number value to text label for the legend panel.
        this.legendGrid.afterCreateCellContent = setLegendLabel;
        this.borderLegendGrid.afterCreateCellContent = setLegendLabel;
    },
    applyGroupSecurity:function(){
        var user  = View.user;
        var restrictedHM = ['Aucun','highlightOccupancyType-0','highlightOccupancyType-1','highlightOccupancyType-2','highlightOccupancyType-3','highlightOccupancyType-3-int','highlightOccupancyType-3-ext','highlightCategoriesDs'];
        var restrictedLabel = ['labelImpressionPlanDs','labelPhysiqueDs','labelOccupantDs','LabelParDefaut'];
        var mepTechniqueLabel = ['labelContratEuroDs','labelContratFactureDS','labelBailPvEuroDs','labelBailPvFactureDS'];
        var mepTechniqueHM = ['highlightTauxEffort','highlightTauxCharge','highlightCoutGlobal'];
        var membervalo = user.isMemberOfGroup('SNCF-GC-VALO-VISIBILITY');
        var memberFullHM = user.isMemberOfGroup('SNCF-GC-ALL-HIGHLIGHTS');
        var memberFullLabel = user.isMemberOfGroup('SNCF-GC-ALL-LABELS');
        var memberNoValoVisibilityHM = user.isMemberOfGroup('SNCF-GC-NOVALO-HIGHLIGHTS');
        var memberNoValoVisibilityLabel = user.isMemberOfGroup('SNCF-GC-NOVALO-LABELS');
        var memberFullLabel = user.isMemberOfGroup('SNCF-GC-ALL-LABELS');
        var selector_labels = jQuery("#selector_labels option");
        var selector_hilite = jQuery('#selector_hilite option');
        // MEP technique JIRA SNCF 240
        if(memberNoValoVisibilityLabel){
            for(var i=0;i<selector_labels.length;i++){
                if(mepTechniqueLabel.indexOf(selector_labels[i].value) > -1){
                    selector_labels[i].parentNode.removeChild(selector_labels[i]);
                    
                }
                if(selector_labels[i].value =='Aucun'){
                    selector_labels[i].parentNode.removeChild(selector_labels[i]);
                }
            }
        }else{
            // In this case, user can see all label
            if(memberFullLabel){
                for(var i=0;i<selector_labels.length;i++){
                    if(selector_labels[i].value.indexOf("LabelParDefaut")>-1){
                        selector_labels[i].text="Aucun";
                    }
                    if(selector_labels[i].value =='Aucun'){
                        selector_labels[i].parentNode.removeChild(selector_labels[i]);
                    }
                    
                    console.log(selector_labels[i]);
                }
            }else{
                // On restreint la liste ....
                for(var i=0;i<selector_labels.length;i++){
                    if(restrictedLabel.indexOf(selector_labels[i].value) === -1){
                        selector_labels[i].parentNode.removeChild(selector_labels[i]);
                        
                    }
                    if(selector_labels[i].value.indexOf("LabelParDefaut")>-1){
                        selector_labels[i].text="Aucun";
                    }
                }
            }
            
        }
        if(memberNoValoVisibilityHM){
            for(var i=0;i<selector_hilite.length;i++){
                if(mepTechniqueHM.indexOf(selector_hilite[i].value) > -1){
                    selector_hilite[i].parentNode.removeChild(selector_hilite[i]);
                }
            }
        }else{
            if(!memberFullHM){
                for(var i=0;i<selector_hilite.length;i++){
                    if(restrictedHM.indexOf(selector_hilite[i].value) === -1){
                        selector_hilite[i].parentNode.removeChild(selector_hilite[i]);
                    }
                }
            }
        }
        
        
    },

    afterInitialDataFetch: function() {
        document.getElementById('drawingPanel_title').style.display='none';
        this.initialWaitingRooms();
        
        this.switchHighlightSelections();
        this.applyGroupSecurity();
    },
    
    /**
     * Initialize the variables after the view is created.
     * Add by heqiang
     */
    initializeVariables: function() {
        this.assignmentTarget = null;
        this.selectedFloors = [];
        this.employeesWaiting = [];
        this.resetRoomsData();
        this.setFilter();
        this.initial3dLicense();
    },
    
    /**
     * Dertermine whether currently has the 3D Navigator License and storte the result to local boolean variable has3dLicense.
     */
    initial3dLicense: function() {
        var me = this;
        AdminService.getProgramLicense({
            callback: function(license) {
                var licenseIds = [];
                var licenses = license.licenses;
                
                //check AbBldgOpsHelpDesk license
                for(i in licenses){
                    licenseIds.push(licenses[i].id);
                    if ( licenses[i].enabled && ( licenses[i].id == 'AbWebCentral3DNavigator') ) {
                        me.has3dLicense = true;
                        return;
                    }
                }
            },              
            errorHandler: function(m, e) {
                View.showException(e);
            }
        });
    },

    /**
     * Register all the listener this controller handles.
     * Add by heqiang
     */
    initializeEventListener: function() {
        this.on('app:space:express:console:refreshDrawing', this.refreshDrawing);
        this.on('app:space:express:console:selectLocation', this.addFloorPlan);
        this.on('app:space:express:console:changeMode', this.changeMode);
        this.on('app:space:express:console:addEmployeeWaiting', this.addEmployeeWaiting);
        this.on('app:space:express:console:unassignEmployees', this.unassignEmployees);
        this.on('app:space:express:console:removeAssignment', this.removeAssignment);
        this.on('app:space:express:console:afterBeginAssignment', this.afterBeginAssignment);
        this.on('app:space:express:console:afterClearAssignment', this.afterClearAssignment);
        this.on('app:space:express:console:afterChangeAssignmentTarget', this.afterChangeAssignmentTarget);
        this.on('app:space:express:console:loadAndHighLightSelectedEmployee', this.loadAndHighlightSelectedEmployee);
        this.on('app:space:express:console:loadAndHighLightSelectedRoom', this.loadAndHighlightSelectedRoom);
        this.on('app:space:express:console:highlightSelectedRoom', this.highlightSelectedRoom);
        this.on('app:space:express:console:highlightTeamRooms', this.highlightTeamRooms);
        this.on('app:space:express:console:cancelSelectedRooms', this.cancelSelectedRooms);
        this.on('app:space:express:console:cancelEmployeeAssignment', this.cancelEmployeeAssignment);
        this.on('app:space:express:console:commitRoomsAlteration', this.afterCommitRoomsAlteration);
        this.on('app:space:express:console:rollbackEmployeeAssignment', this.rollbackEmployeeAssignment);
        this.on('app:space:express:console:afterCommitPendingAssignment', this.afterCommitPendingAssignment);
        this.on('app:space:express:console:enableTeamSpace', this.enableTeamSpace);
        this.on('app:space:express:console:show2D', this.show2D);

        // Space Utilization: To enable Space Utilization functionaliy: remove "//" from start of below code line 
        // this.on('app:space:express:console:refreshUtilization', this.refreshDrawingForUtil); 
    },
    
    initialSvgDrawingCtrl: function() { 
        //set draggable assets to employee and selection pattern
        // comment out for html5 conversion
        //this.drawingPanel.enableLiteDisplay(false);
        //this.drawingPanel.setDraggableAssets(['em']);
        //this.drawingPanel.setDiagonalSelectionPattern(true);
        
        //set callback function when special event that we are interested in occurs.
        // comment out for html5 conversion
        //this.drawingPanel.addEventListener('onclick', this.onClickRoom.createDelegate(this));
        //this.drawingPanel.addEventListener('ondwgload', this.reHighlightSelectedAssets.createDelegate(this));
        //this.drawingPanel.addEventListener('onMultipleSelectionChange', this.onDrawingPanelMultipleSelectionChange.createDelegate(this));
        //this.drawingPanel.addEventListener('onselecteddatasourcechanged', this.handleSelectDataSource.createDelegate(this));
        //this.doAppendRuleSet();

        // define parameters to be used by server-side job
        var defaultLayers=View.activityParameters['AbSpaceRoomInventoryBAR-SpaceConsoleDefaultLayers'];
        var LayersPopup={divId: "drawingDiv"};
        if(defaultLayers!='-'){
            LayersPopup.defaultLayers=defaultLayers;
        }
        var parameters = new Ab.view.ConfigObject();
        parameters['divId'] = "drawingDiv";
        parameters['allowMultipleDrawings'] = 'true';
        parameters['orderByColumn'] = 'true';
        parameters['highlightParameters'] = [{'view_file':"ar-sp-console-ds-highlights.axvw", 'hs_ds': "highlightNoneDs", 'label_ds':'LabelParDefaut','label_ht': 0.6, 'selection_ds':'selectionAllRooms_VPA_DS'}];

        parameters['showTooltip'] = true;
        parameters['addOnsConfig'] = { 
                'NavigationToolbar': {divId: "drawingDiv"},
                'LayersPopup': LayersPopup,
                'DatasourceSelector': {panelId: "drawingPanel", afterSelectedDataSourceChanged: this.afterSelectedDataSourceChanged.bind(this), beforeSelectedDataSourceChanged: this.beforeSelectedDataSourceChanged.bind(this) },
                'InfoWindow': {width: '400px', position: 'bottom', customEvent: this.onCloseInfoWindow},
                'AssetLocator': {divId: "drawingDiv"},
                'SelectWindow': {assetType: "rm", customEvent: this.onSelectMultipleRooms},
                'AssetPanel': {
                    divId: "drawingDiv",
                    assetType: 'em',
                    title: "Waiting Room", 
                    dataSourceName: "employeesWaitingDS",
                    collapsible: true, 
                    size: {height: 130, width: 380}, 
                    font: {fontFamily:'Arial', fontSize: 11},
                    // highlightedOnly: true,       // if true, when attaching drop events, restrict to only locations that are highlighted from the hs_ds
                    rowActions: [
                        {type:'em', title:"Delete", icon: Ab.view.View.contextPath + "/schema/ab-core/graphics/icons/view/delete.png", listener: this.deleteWaitingEmployee}],
                    onAssetLocationChange: this.onDropAsset.createDelegate(this)
                }
        };

        parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickRoom}];
        
        //enable bordersHighlight datasource selector
        parameters['bordersHighlightSelector'] = false;
        //pass borderSize for border highlight
        parameters['borderSize'] = 18;
        
        //enable legend panels
        parameters['legendPanel'] = 'legendGrid';
        parameters['borderLegendPanel'] = 'borderLegendGrid';
        parameters['selectionMode'] = '1';

        this.drawingControl = new Drawing.DrawingControl("drawingDiv", "drawingPanel", parameters);         
        var selectController = this.drawingControl.drawingController.getController("SelectController");
        //enable diagonal border selection 
        selectController.setDiagonalSelectionPattern(true);
        //set diagonal selection border size (default is 20)
        selectController.setDiagonalSelectionBorderWidth(15);
        //enable Multiple Selection
        selectController.setMultipleSelection(true);

        this.drawingPanel.actions.get("3dSwitch").enable(false);
    },

    /**
    * Initial employeesWaiting panel according to mode from side car
    */
    initialWaitingRooms: function(enabled){
        this.drawingPanel.getSidecar().load();
        this.mode = this.modeSelector.getSidecar().get('mode');     

        //The waiting room can't be shown in IE.Need special treatment.
        if (this.mode === "employeeMode") {
            this.loadEmployeesWaitingRoom();
            var employeesWaiting = this.drawingPanel.getSidecar().get('employeesWaiting');
            this.employeesWaiting = employeesWaiting ? employeesWaiting : [];
            this.refreshEmployeeWaitingPanel();
        }
    },

    /**
    * Added for 23.2 html drawing conversion: Show asset panel Employees Waiting Room.
    */
    loadEmployeesWaitingRoom: function(){
        if (!this.assetPanelLoaded) {
            /*below code segment is a work around to show asset panel initially when no floor plan is added, can be removed after the core kb is fixed*/
            /* start work around*/
            var parameters = new Ab.view.ConfigObject();
            parameters['pkeyValues'] = {'bl_id':"HQ", 'fl_id': "17"};
            parameters['drawingName'] = 'HQ17';
            this.drawingControl.load(parameters);   
            this.drawingControl.unload(parameters); 
            /* end work around*/
            // load and display the asset panel
            var restriction = new Ab.view.Restriction();
            restriction.addClause('em.em_id', 'none', '='); 
            this.drawingControl.getAddOn("AssetPanel").loadAssetPanel(restriction);
            this.drawingControl.getAddOn("AssetPanel").showAssetPanel();
            this.assetPanelLoaded = true;
        } 
    },

    loadSvg: function(row){
        var parameters = new Ab.view.ConfigObject();
        parameters['pkeyValues'] = {'bl_id':row.row.getFieldValue('rm.bl_id'), 'fl_id': row.row.getFieldValue('rm.fl_id')};
        parameters['drawingName'] = row.row.getFieldValue('rm.dwgname');
        if ( row.row.isSelected() ) {
            if ( this.show ) {
                this.drawingControl.getAddOn('LayersPopup').collapsed = true
                this.drawingControl.load(parameters);   
                this.addDragDropEvent(parameters['drawingName']);
            }
        }
        else {
            this.drawingControl.unload(parameters); 
        }
    },

    /**
     * When switch mode, clears the selected rooms and pending assignments.
     */
    changeMode: function(mode) {
        if (valueExists(mode)) {
            this.mode = mode;
        }
        //Reset all the rooms selection.
        this.resetRoomsData();
        
        //switch highlight and label option according to mode.
        this.switchOptionForMode();
        
        //Do some operation related to concrete mode.
        this.handleModeAction();

        this.initialWaitingRooms();
    },
    
    /**
     * Applies specified location and occupancy restrictions to the drawing panel.
     */
    refreshDrawing: function(filter) {
        this.setFilter(filter);
        
        // kb#3052729: need to reset the Select Date info when changing the as of date not in 'team assign' mode. 
        if ( (!this.assignmentTarget || this.assignmentTarget.type != 'team') && $('showButton')) {
            this.selectDateStart=this.asOfDate;
            this.selectDateEnd=null;  
            var showButtonTitle = getMessage("highlightFrom")+" "+this.selectDateStart+(this.selectDateEnd? (" "+getMessage("highlightTo")+" "+this.selectDateEnd):" ");
            $('showButton').innerText = showButtonTitle;
        }

        this.trigger('app:space:express:console:cancelSelectedRooms');
        this.clearDrawingSelections();

        this.drawingControl.config.highlightParameters[0].hs_param = toJSON(this.filter.parameters);      
         var highlightDS = this.drawingPanel.currentHighlightDS;
        
        var leasePanel = View.panels.get("leasePanel");
        if (highlightDS == 'highlightLeaseResiliationDate') {
            if(this.leasePanel)
            {
                var resiliationDate = this.leasePanel.getFieldValue("ls.date_resiliation");
                var ds = View.dataSources.get('highlightLeaseResiliationDate');

                ds.addParameter("dateToFilter", "TO_CHAR(to_date('"+resiliationDate+"','YYYY-MM-DD'),'YYYY-MM-DD')");
                this.filter.parameters['dateToFilter'] = "TO_CHAR(to_date('"+resiliationDate+"','YYYY-MM-DD'),'YYYY-MM-DD')";            
            }
        }

        if (this.drawingControl.drawingController.isDrawingsLoaded) {
            this.reloadDrawings();
            this.clearDrawingSelections();
        }
    },
            
    // Space Utilization: To enable Space Utilization functionaliy: remove the line "/*" below 'Space utilization - Start'; and the line '*/' above the 'Space utilization - End'.
    /**
     * Applies specified from and to date/time for highlight drawing by Utilization.
     */
    // Space utilization - Start
    /*
    refreshDrawingForUtil: function(parameters) {
        var spaceHighlightDs = $("selector_hilite").value; 
        if (this.drawingControl.drawingController.isDrawingsLoaded && spaceHighlightDs=='highlightUtilizationDs' ) {
            this.filter.parameters['fromDate'] = parameters['fromDate'];
            this.filter.parameters['toDate'] = parameters['toDate'];
            this.filter.parameters['fromTime'] = parameters['fromTime'];
            this.filter.parameters['toTime'] = parameters['toTime'];
            this.drawingControl.config.highlightParameters[0].hs_param = toJSON(this.filter.parameters);      
            this.reloadDrawings();
            this.clearDrawingSelections();
        }
    },
     */ 
    // Space utilization - End

    /**
     * Set the filter value to that comes from the location filter console.
     */
    setFilter: function(filter) {
        if (filter) {
            this.filter = filter;
            this.asOfDate = filter.parameters['asOfDate'];
        } else {
            if (this.filter == null) {
                this.filter = {
                        restriction: null,
                        parameters: {'asOfDate': this.asOfDate}
                    };
            }
        }
    },
    
    /**
     * When a user enters a special mode, we need to do some action that is usable in this mode.
     */
    handleModeAction: function() {
        if (this.mode == 'employeeMode') {
            // comment out for html5 conversion
            //this.drawingPanel.setDraggableAssets(['em']);
            if ( this.drawingControl && this.assetPanelLoaded ) {
                this.drawingControl.getAddOn("AssetPanel").showAssetPanel();
            }
        } else {
            if (this.filter) {
                this.filter.restriction = new Ab.view.Restriction();
            }
            //this.drawingPanel.setDraggableAssets(null);
            if ( this.drawingControl && this.assetPanelLoaded ) {
                this.drawingControl.getAddOn("AssetPanel").closeAssetPanel();
            }
        }
        
        this.trigger('app:space:express:console:reselectLocationsForDrawing');
        
        if (!this.isLocateEvent && this.drawingControl.drawingController.isDrawingsLoaded) {
            this.reHighlightSelectedAssets();
        }
    },
    
    /**
     * Switch the status of the gear icon above drawing panel.
     */
    switchRoomStandardIconStatus: function(enabled) {
        var innerThis = this;
        if (this.copyOfRoomStandardElement == null) {
            this.drawingPanel.actions.each(function(action) {
                if(action.id == 'drawingMenu') {
                    innerThis.copyOfRoomStandardElement = action;
                    action.enable(enabled);
                }
            });
        } else {
            this.copyOfRoomStandardElement.enable(enabled);
        }
    },
    
    /**
     * Reset all rooms and clear their current status.
     */
    resetRoomsData: function() {
        this.selectedRooms = new Ab.space.express.console.Rooms();
        this.assignedRooms = new Ab.space.express.console.Rooms();
        this.movedEmployeesRooms = new Ab.space.express.console.Rooms();
    },

    /**
     * As for mode, we will change the highlight or labels option according to it.
     */
    switchOptionForMode: function() {
        if (this.mode == 'employeeMode') {
            this.setDrawingDataSource("selector_labels", "labelEmployeesDs");
            if (this.drawingControl.drawingController.isDrawingsLoaded) {
                // this.drawingPanel.setDataSource('labels', 'labelEmployeesDs');
            }
        } else {
            var spaceHighlightDs = $("selector_hilite").value; 
            //this.drawingPanel.currentHighlightDS;
            if (!spaceHighlightDs || spaceHighlightDs=='' || spaceHighlightDs=='highlightNoneDs') {
                //comment for html5 conversion
                //this.drawingPanel.setDataSource('highlight', 'highlightDivisionsDs');
                //this.setDrawingDataSource("selector_hilite", "highlightDivisionsDs");
            }
        }
    },

    /**
     * Handle the multiple room selection when hold the ctrl key and drag the mouse and then make selections on drawing panel.
     * add by heqiang
     */
    onDrawingPanelMultipleSelectionChange: function() {
        var drawingPanel = View.getControl('', 'drawingPanel')
        var selectedAssetsMap = drawingPanel.getMultipleSelectedAssets();
        if (selectedAssetsMap.length > 0) {
            for ( var i = 0 ;i < selectedAssetsMap.length; i++ ) {
                this.onClickRoom(selectedAssetsMap[i].pks,  selectedAssetsMap[i].selected);
            }
        }
    },

    imageExist: function(url) {
        var http = new XMLHttpRequest();
        http.open('HEAD', url, false);
        http.send();
        return http.status != 404;
    },

    /**
     * Called when the user selects or un-selects a floor in the Locations panel.
     * @param row
     */
    addFloorPlan: function(row) {
        this.fireBuildingCheckEvent(row);
        this.loadSvg(row);

        if (row.row.isSelected() ) {
            if (this.selectedPlanType) {
                this.openPlanTypesDialogCloseHandler(this.selectedPlanType)
            }
        }

        //this.refreshDrawing();
        if (this.drawingControl.drawingController.isDrawingsLoaded && this.mode == 'employeeMode') {
            if ( this.drawingControl && this.assetPanelLoaded ) {
                this.drawingControl.getAddOn("AssetPanel").showAssetPanel();
            }
        }
        View.closeProgressBar();
        
    },
    
    /**
     * When the user select or unselect a row from the building-floor grid, we will push or pop the floor correspondingly.
     */
    fireBuildingCheckEvent: function(row) {
        var buildingId = row.row.getFieldValue('rm.bl_id');
        var floorId = row.row.getFieldValue('rm.fl_id');
        var dwgname = row.row.getFieldValue('rm.dwgname');
        this.selectedFloors = _.reject(this.selectedFloors, function(selectedFloor) {
            return selectedFloor.bl_id === buildingId && selectedFloor.fl_id === floorId;
        });
        if (row.row.isSelected()) {
            this.selectedFloors.push({
                bl_id: buildingId,
                fl_id: floorId,
                dwgname: dwgname
            });
        }

        this.enable3D();
    },

    /**
     * When the user selects the highlight datasource or label datasource, we will keep the former highlighted rooms.
     * Add by heqiang 
     */
    handleSelectDataSource: function() {
        console.log("HANDLESELECT DATASOURCE");
        this.selectedPlanType = null;
        this.currentLegendDataSource = null;
        this.currentLegendPanel = null;
        
        if (this.drawingControl.drawingController.isDrawingsLoaded) {
            this.LabelValorisable();
            this.HeatMapValorisable();
            for (var i = 0; i < View.dataSources.length; i++) {
                var ds = View.dataSources.get(i);
                if (ds && ds.type === 'DrawingControlHighlight') {
                    ds.addParameters(this.filter.parameters);
                    ds.setRestriction(this.filter.restriction);
                }
            }
            var highlightDS = this.drawingPanel.currentHighlightDS;
            var labelsDS = this.drawingPanel.currentLabelsDS;

             var leasePanel = View.panels.get("leasePanel");
            if (highlightDS == 'highlightLeaseResiliationDate') {
                leasePanel.show(true);
                leasePanel.setInstructions(getMessage('resiliationInstruction'));
                
                if(this.leasePanel)
                {
                    var resiliationDate = this.leasePanel.getFieldValue("ls.date_resiliation");
                    var ds = View.dataSources.get('highlightLeaseResiliationDate');

                    ds.addParameter("dateToFilter", "TO_CHAR(to_date('"+resiliationDate+"','YYYY-MM-DD'),'YYYY-MM-DD')");
                    this.filter.parameters['dateToFilter'] = "TO_CHAR(to_date('"+resiliationDate+"','YYYY-MM-DD'),'YYYY-MM-DD')";
                }
            }
            else
            {
                leasePanel.setFieldValue("ls.date_resiliation","${sql.currentDate}");
                leasePanel.show(false);
            }
            

            if (highlightDS == '') {
                highlightDS = 'highlightCategoriesDs';
            }
            if (labelsDS == '') {
                labelsDS = 'labelImpressionPlanDs';
            }
            
            this.drawingPanel.setDataSource('highlight', highlightDS);
            this.drawingPanel.setDataSource('labels', labelsDS);
            //this.drawingPanel.clearPersistFills();
            this.clearDrawingSelections();
            
            //after the user change the highlight ds, we should keep all the selections the user made.
            this.reHighlightSelectedAssets.defer(1, this);
            }else{
                var highlightDS = this.drawingPanel.currentHighlightDS;
                var leasePanel = View.panels.get("leasePanel");
                this.LabelValorisable();
                this.HeatMapValorisable();
                if (highlightDS == 'highlightLeaseResiliationDate') {
                    leasePanel.show(true);
                    leasePanel.setInstructions(getMessage('resiliationInstruction'));
                    if(this.leasePanel)
                    {
                        var resiliationDate = this.leasePanel.getFieldValue("ls.date_resiliation");
                        var ds = View.dataSources.get('highlightLeaseResiliationDate');

                        ds.addParameter("dateToFilter", "TO_CHAR(to_date('"+resiliationDate+"','YYYY-MM-DD'),'YYYY-MM-DD')");
                        this.filter.parameters['dateToFilter'] = "TO_CHAR(to_date('"+resiliationDate+"','YYYY-MM-DD'),'YYYY-MM-DD')";
                    }
                }
                else
                {
                    leasePanel.show(false);
                }
                View.closeProgressBar();
            }
        

        if ( this.locateTeamId ){
            this.highlightTeamRooms(this.locateTeamId, null, this.drawingPanel.currentHighlightDS);
        }
    },
    HeatMapValorisable:function(){
        console.log("HEATMAP VALORISABLE");
        var hmWithValo = ['highlightTauxEffort','highlightTauxCharge','highlightCoutGlobal'];
        var canSeeParam = (View.user.isMemberOfGroup('SNCF-GC-VALO-VISIBILITY'))? "1=1":"1=0";
        //var hmDS = this.drawingPanel.currentHighlightDS;
        var hmDS = document.getElementById('selector_hilite').value;
        if(hmWithValo.indexOf(hmDS)> -1){
            var ds = View.dataSources.get(hmDS);
            ds.addParameter("canSeeParam", canSeeParam);
        }
    },
    LabelValorisable : function(){
        var billingPanel = View.panels.get("billingPanel");
        var factu = document.getElementById("billingPanel_ls.annee_facturation");
        if(factu.value.length =0){
            factu.value = "2018";
        }
        var labelsWithValo = ['labelContratDs','labelContratEuroDs','labelContratFactureDS','labelBailPvEuroDs','labelBailPvFactureDS'];
        //var labelDS = this.drawingPanel.currentLabelsDS;
        var labelDS =document.getElementById('selector_labels').value;
        var canSeeParam = (View.user.isMemberOfGroup('SNCF-GC-VALO-VISIBILITY'))? "1=1":"1=0";
        if(labelDS.indexOf('Facture')>-1){
            billingPanel.show(true);
            billingPanel.setInstructions(getMessage('facInstruction'));
            var ds = View.dataSources.get(labelDS);
            ds.addParameter("canSeeParam", canSeeParam);
            ds.addParameter("billing_year", "'"+factu.value+"'");
            this.filter.parameters['billing_year'] = "'"+factu.value+"'";   
        }else{

            billingPanel.show(false);
            if(labelsWithValo.indexOf(labelDS)> -1){
                var ds = View.dataSources.get(labelDS);
                ds.addParameter("canSeeParam", canSeeParam);
            }
        }
    },
    
    /**
     * ReHighlight all the selected assets according to types.
     * add by heqiang
     */
    reHighlightSelectedAssets: function() {
        if (this.mode === 'spaceMode') {
            if (this.assignedRooms.length > 0) 
                this.reHighlightPendingAssignments();
            if (this.selectedRooms.length > 0)
                this.reHighlightSelectedRooms();
        } else {
            if (this.selectedRooms.length > 0) {
                this.reHighlightSelectedRooms();
            }
            if (this.movedEmployeesRooms.length > 0) {
                this.reHighlightEmployeesMovedRooms();
            }
        }
    View.closeProgressBar();
    },
    
    
    /**
     * ReHighlight the selected rooms in the session when user reopens the floor plan if has any.
     * add by heqiang
     */
    reHighlightSelectedRooms: function() {
        for ( var i = 0; i < this.selectedFloors.length; i++ ) {
            var selectedFloor = this.selectedFloors[i];
            var targetBlId = selectedFloor.bl_id;
            var targetFlId = selectedFloor.fl_id;
            var roomsArray = this.selectedRooms.targetToArray(targetBlId, targetFlId);
            if (roomsArray.length > 0) {
                if (this.drawingControl.drawingController.isDrawingsLoaded) {
                    //this.drawingPanel.selectAssets(roomsArray);
                    this.highlightRoomArray(roomsArray);
                }
            }
        }
    },
    
    /**
     * ReHighlight the assignments in the session when user reopens the floor plan if has any.
     * add by heqiang
     */
    reHighlightPendingAssignments: function() {
        for (var i = 0; i < this.selectedFloors.length; i++) {
            var selectedFloor = this.selectedFloors[i];
            var targetBlId = selectedFloor.bl_id;
            var targetFlId = selectedFloor.fl_id;
            var roomsArray = this.assignedRooms.targetToArray(targetBlId, targetFlId);
            if (roomsArray.length > 0) {
                if (this.drawingControl.drawingController.isDrawingsLoaded) {
                    this.highlightRoomArray(roomsArray);
                    //this.drawingPanel.selectAssets(roomsArray);
                }
            }
        }
    },

    /**
     * ReHighlight assinged rooms in the session when user reopens the floor plan.
     * add by heqiang
     */
    reHighlightEmployeesMovedRooms: function() {
        for ( var i = 0; i < this.selectedFloors.length; i++ ) {
            var selectedFloor = this.selectedFloors[i];
            var targetBlId = selectedFloor.bl_id;
            var targetFlId = selectedFloor.fl_id;
            var roomsArray = this.movedEmployeesRooms.targetToArray(targetBlId, targetFlId);
            if (roomsArray.length > 0) {
                if (this.drawingControl.drawingController.isDrawingsLoaded) {
                    this.highlightRoomArray(roomsArray);
                }
            }
        }
    },
    
    highlightRoomArray: function(roomsArray) {
        for (var j = 0; j < roomsArray.length; j++) {
            var room = roomsArray[j];
            var dwgname = this.getDwgnameByBlIdAndFlId(room[0], room[1]);
            this.reHighlightRoom( {
                        bl_id:room[0],
                        fl_id:room[1],
                        rm_id:room[2], 
                        dwgname: dwgname
                    });
        }
    },
        
    /**
     * After the user commits the selected rooms,we will reset the collection.
     */
    afterCommitRoomsAlteration: function() {
        this.selectedRooms = new Ab.space.express.console.Rooms();
        this.reloadDrawings();
        this.clearDrawingSelections();
        //comment below lines for html5 drawing control conversion
        //this.drawingPanel.clearPersistFills();
       //this.drawingPanel.refresh();
    },
    
    
    /**
     * When cancel employee assignment from the popup window, we should clear the rehighlighted rooms.
     */
    cancelEmployeeAssignment: function() {
        this.clearAssignmentTarget();
        this.employeesWaiting = [];
        this.drawingPanel.getSidecar().set('employeesWaiting', this.employeesWaiting);
        if (!this.drawingControl) {
            return;
        }
        //comment below lines for html5 drawing control conversion
        //this.drawingPanel.clearPersistFills();
        //this.drawingPanel.refresh();
        this.reloadDrawings();
        this.clearDrawingSelections();
        this.trigger('app:space:express:console:cancelAssignment');
    },
    
    /**
     * Get dwgname with building id and floor id.
     */
    getDwgnameByBlIdAndFlId: function(buildingId, floorId) {
        for ( var i = 0; i < this.selectedFloors.length; i++ ) {
            var floor = this.selectedFloors[i];
            if (floor.bl_id == buildingId && floor.fl_id == floorId) {
                return floor.dwgname;
            }
        }
        return null;
    },
    
    /**
     * Returns true if specified floor is selected.
     * @param buildingId
     * @param floorId
     * @return {Boolean}
     */
    isFloorSelected: function(buildingId, floorId) {
        return valueExists(_.find(this.selectedFloors, function(selectedFloor) {
            return (selectedFloor.bl_id === buildingId && selectedFloor.fl_id === floorId);
        }));
    },
    
    /**
     * Invoke highlight rooms for selected employee.  zoom in if only one employee selected.
     */
    loadAndHighlightSelectedEmployee: function(employee) {
        if (this.show ) {
            //when locating an employee. Change highlight drop-down to "None".
           // this.drawingPanel.setDataSource('highlight', 'None');
            this.setDrawingDataSource("selector_hilite", "highlightNoneDs");
            this.setDrawingDataSource("selector_labels", "labelEmployeesDs");
            
            //if location clicked. set this varibalbe true and it doesn't refresh drawing in 'ondwgload'.
            this.isLocateEvent = true;

            //var row = this.employeeGrid.rows[this.employeeGrid.selectedRowIndex];
            //this.fireBuildingCheckEvent(row);
           
            this.highlightDrawingAsset(employee);
            //zoom in the selected room, use defer to wait the drawing load complete.
            this.zoomIntoRoom(employee.bl_id,employee.fl_id, employee.rm_id, employee.dwgname);
            // set the sign 'isLocateEvent' to false at the end of locating  room, add a delay to make sure this variable is changed after locate execution completed.
            this.setIsLocateEvent(false);       
        }
    },
    
    /**
     * Load drawing plan for selected room and zoom in.
     */
    loadAndHighlightSelectedRoom: function(room) {
        if (this.show) {
            //when locating employees, change highlight drop-down to "None".
            // this.drawingPanel.setDataSource('highlight', 'None');
            this.setDrawingDataSource("selector_hilite", "highlightNoneDs");
            this.setDrawingDataSource("selector_labels", "labelEmployeesDs");
            
            // set the sign 'isLocateEvent' to true at the begining of locating the room, so that to prevent the refresh of drawing when 'ondwgload'.
            this.isLocateEvent = true;
            
            //var row = this.roomsGrid.rows[this.roomsGrid.selectedRowIndex];
            //this.fireBuildingCheckEvent(row);
            
            this.highlightDrawingAsset(room);
            //zoom in the selected room, use defer to wait the drawing load complete.
            this.zoomIntoRoom(room.bl_id,room.fl_id, room.rm_id, room.dwgname);   
            // set the sign 'isLocateEvent' to false at the end of locating  room, add a delay to make sure this variable is changed after locate execution completed.
            this.setIsLocateEvent(false);
        }
    },
    
    /**
     * Set method for boolean sign 'isLocateEvent'
     */
    setIsLocateEvent: function(flag) {
        this.isLocateEvent = flag;
    },
    
    //Added for html5 drawing conversion: manually set highlight/border/label datasources; meanwhile set proper option in selection list and reload the drawing
    setDrawingDataSource: function(selectorId, dataSourceId){ 
        document.getElementById(selectorId).value = dataSourceId;
          
        var dsSelector = this.drawingControl.drawingController.getAddOn('DatasourceSelector');
        var combo = document.getElementById(dataSourceId);
        dsSelector.onSelectedDatasourceChanged(dsSelector, combo);

         this.addDragDropEvent();
    },
    
    reloadDrawings: function(){
        this.drawingControl.config.highlightParameters[0].hs_param = toJSON(this.filter.parameters);      
        var drawingController = this.drawingControl.getDrawingController(); 
        //var dsSelector = drawingController.getAddOn('DatasourceSelector');
    
        var params = this.drawingControl.config;
        //reload all loaded drawings with new parameters
        drawingController.getControl().reload(params);
        
        this.addDragDropEvent();
    },

    afterSelectedDataSourceChanged: function(comboId, comboValue, drawingController){   
        this.selectedPlanType = null;
         spaceExpressConsoleDrawing.addDragDropEvent();
    },
    beforeSelectedDataSourceChanged: function(comboId, comboValue, drawingController){
        if (this.drawingControl.drawingController.isDrawingsLoaded) {
            this.LabelValorisable();
            this.HeatMapValorisable();
            var highlightDs =document.getElementById('selector_hilite').value;
             var leasePanel = View.panels.get("leasePanel");
            if (highlightDs == 'highlightLeaseResiliationDate') {
                leasePanel.show(true);
                leasePanel.setInstructions(getMessage('resiliationInstruction'));
                
                if(this.leasePanel)
                {
                    var resiliationDate = this.leasePanel.getFieldValue("ls.date_resiliation");
                    var ds = View.dataSources.get('highlightLeaseResiliationDate');

                    ds.addParameter("dateToFilter", "TO_CHAR(to_date('"+resiliationDate+"','YYYY-MM-DD'),'YYYY-MM-DD')");
                    this.filter.parameters['dateToFilter'] = "TO_CHAR(to_date('"+resiliationDate+"','YYYY-MM-DD'),'YYYY-MM-DD')";
                }
            }
            else
            {
                leasePanel.setFieldValue("ls.date_resiliation","${sql.currentDate}");
                leasePanel.show(false);
            }

        }
        else{
                var leasePanel = View.panels.get("leasePanel");
                this.LabelValorisable();
                this.HeatMapValorisable();
                var highlightDs =document.getElementById('selector_hilite').value;
                if (highlightDs == 'highlightLeaseResiliationDate') {
                    leasePanel.show(true);
                    leasePanel.setInstructions(getMessage('resiliationInstruction'));
                    if(this.leasePanel)
                    {
                        var resiliationDate = this.leasePanel.getFieldValue("ls.date_resiliation");
                        var ds = View.dataSources.get('highlightLeaseResiliationDate');

                        ds.addParameter("dateToFilter", "TO_CHAR(to_date('"+resiliationDate+"','YYYY-MM-DD'),'YYYY-MM-DD')");
                        this.filter.parameters['dateToFilter'] = "TO_CHAR(to_date('"+resiliationDate+"','YYYY-MM-DD'),'YYYY-MM-DD')";
                    }
                }
                else
                {
                    leasePanel.show(false);
                }
            }

        this.drawingControl.config.highlightParameters[0].hs_param = toJSON(this.filter.parameters);


    },

    addDragDropEvent: function(dwgName) {
        // attach drag and drop events to employee labels from 'labelEmployeesDs'
        var drawingController = this.drawingControl.drawingController;
        var datasourceSelectorAddOn = drawingController.getAddOn('DatasourceSelector');
        var labelDs = drawingController.config.highlightParameters[0].label_ds;
        if ( drawingController.config.labelSelector === false 
             || (drawingController.config.labelSelector === true 
                 && datasourceSelectorAddOn 
                 &&  labelDs === 'labelEmployeesDs'
                 && this.mode == 'employeeMode') )  {
            if (dwgName){
                this.drawingControl.getAddOn("AssetPanel").addDragDropEventsToLayer("rm", dwgName);
                this.drawingControl.getAddOn("AssetPanel").addDragDropEventsToRooms("rm", dwgName);         
            } else {
                for ( var i=0; i<this.selectedFloors.length; i++){
                    var drawingName = this.selectedFloors[i].dwgname;
                    this.drawingControl.getAddOn("AssetPanel").addDragDropEventsToLayer("rm", drawingName);
                    this.drawingControl.getAddOn("AssetPanel").addDragDropEventsToRooms("rm", drawingName);         
                }
            }
        }
    },

    //Added for html5 drawing conversion: manually highlight given assets
    highlightDrawingAsset: function(row){
        var highlightController = this.drawingControl.getDrawingController().getController("HighlightController"); 
        highlightController.highlightAsset(row.bl_id+ ';' +row.fl_id+ ';' +row.rm_id, {svgId:  'drawingDiv-' + row.dwgname.toUpperCase()+'-svg', color: 'yellow', persistFill: true, overwriteFill: true});
        //var opts_selectable = new DwgOpts();
        //opts_selectable.rawDwgName = rows[i].dwgname;
        //opts_selectable.appendRec(rows[i].bl_id+ ';' +rows[i].fl_id+ ';' +rows[i].rm_id);
        //this.drawingPanel.setSelectColor('0xFFFF00'); //set to yellow for exist assignment
        //highlightController.highlightAsset('SRL;03;365B', {svgId: 'svgDiv-srl03-svg', color: 'blue', persistFill: true, overwriteFill: true});
    },

    /**
     * Zoom in   the given room in drawing plan
     */
    zoomIntoRoom: function(bl_id,fl_id,rm_id,dwgname) {
        var drawingController = this.drawingControl.getDrawingController(); 
        //var panZoomController = drawingController.getController("PanZoomController");
        try {
            //panZoomController.zoomIn();       
            var assetLocator = drawingController.getAddOn("AssetLocator"); 
            // you can choose how to highlight the asset      
            var opts = { cssClass: 'zoomed-asset-red-bordered',     // use the cssClass property to specify a css class
                         removeStyle: true,                         // use the removeStyle property to specify whether or not the fill should be removed (for example  cssClass: 'zoomed-asset-bordered'  and removeStyle: false
                         svgId:   'drawingDiv-' + dwgname.toUpperCase()+'-svg'
                        };
            
            // don't zoom into a room if the zoomFactor is 0
            //alert(controller.zoomed)
            if (drawingController.zoom === false) {
                opts.zoomFactor = 0;
            }
            assetLocator.findAssets([bl_id+';'+fl_id+';'+rm_id], opts);
    }   catch (e)   {
            var alertMessage =  String.format(getMessage('noDrawingForZoom'), dwgname);
            View.alert(alertMessage);
            return;
        }
    },
    
    /**
     * Added for html5 drawing control conversion: clear selection on drawing.
     */
    unSelectDrawingAsset: function(room) {      
        var selectController = this.drawingControl.drawingController.getController("SelectController");
        var assetController = this.drawingControl.drawingController.getController("AssetController");
        var selectAssets = selectController.selectedAssets;
        for ( var svgId in selectAssets ) {
            if ( selectAssets[svgId].length >= 0 ){ 
                for ( var i=0; i< selectAssets[svgId].length; i++ ) {
                    var assetId = selectAssets[svgId][i];
                    if (assetId== (room[0][0]+";"+room[0][1]+";"+room[0][2])) {
                        selectController.clearDiagonalSelection(assetController, svgId, assetId);
                    }
                }
            }
        }
    },

    /**
     * Added for html5 drawing control conversion: clear selection on drawing.
     */
    clearDrawingSelections: function() {    
        var selectController = this.drawingControl.drawingController.getController("SelectController");
        var assetController = this.drawingControl.drawingController.getController("AssetController");
        var highlightController = this.drawingControl.getDrawingController().getController("HighlightController");
        highlightController.resetAll();
        var selectAssets = selectController.selectedAssets;
        for ( var svgId in selectAssets ) {
            if ( selectAssets[svgId].length >= 0 ){ 
                for ( var i=0; i< selectAssets[svgId].length; i++ ) {
                    var assetId = selectAssets[svgId][i]; 
                    selectController.clearDiagonalSelection(assetController, svgId, assetId);
                }
            }
        }
    },

     /**
     * Called after location button click. Highlight rooms which contains selected rows.
     */
    highlightSelectedRoom: function(room) {
        //only locate to room when currently is in 2d mode.
        if ( this.show ) {
            //when locating an employee. Change highlight drop-down to "None".
           // this.drawingPanel.setDataSource('highlight', 'None');
            this.setDrawingDataSource("selector_hilite", "highlightNoneDs");

            // this.drawingPanel.clearHighlights();
            var highlightController =  this.drawingControl.getDrawingController().getController("HighlightController");
            var options = {'svgId': 'drawingDiv-' + room.dwgname.toUpperCase()+'-svg', 'color': null, 'persistFill': false, 'overwriteFill' : true};
            highlightController.clearAssetsByType("rm", options);

            this.highlightDrawingAsset(room);

            this.zoomIntoRoom(room.bl_id, room.fl_id, room.rm_id, room.dwgname);
        }
    },

    /**
     * Added for 23.1 Team Space: Load drawing plans for selected team.
     */
    highlightTeamRooms: function(teamId, floorRows, highlightDs) {
        if ( !this.locateTeamId || this.locateTeamId!=teamId ) {
            //when locating team, change highlight drop-down to "None".
            //this.drawingPanel.setDataSource('highlight', 'None');
            //this.drawingPanel.setDataSource('labels', 'labelTeamsDs');
            this.setDrawingDataSource("selector_hilite", "highlightTeamDs");
            this.setDrawingDataSource("selector_labels", "labelTeamsDs");

            // set the sign 'isLocateEvent' to true at the begining of locating the room, so that to prevent the refresh of drawing when 'ondwgload'.
            //this.drawingPanel.currentHighlightDS = "highlightTeamDs";
            View.dataSources.get('highlightTeamDs').addParameter('teamIdRestriction', "rm_team.team_id='"+teamId+"'");
            //this.drawingPanel.refresh();
            this.reloadDrawings();
            this.locateTeamId = teamId;
        } 
        else {
            if (highlightDs) {
                //this.drawingPanel.setDataSource('highlight', highlightDs);
                this.setDrawingDataSource("selector_hilite", highlightDs);

            } else {
                //this.drawingPanel.setDataSource('highlight', 'highlightTeamDs');
                this.setDrawingDataSource("selector_hilite", "highlightTeamDs");
            }
            View.dataSources.get('highlightTeamDs').addParameter('teamIdRestriction', ' 1=1 ');
            //this.drawingPanel.refresh();
            this.reloadDrawings();
            this.locateTeamId = null;
        }
    },

    // comment below function for html5 conversion
    /**
     * Event handler when click room in the drawing panel
     * @param {Object} pk
     * @param {boolean} selected
     */
     /*
    onClickRoom: function(pk, selected) {
        var room = { bl_id: pk[0], fl_id: pk[1], rm_id: pk[2] };
        
        if (this.mode === 'spaceMode') {
            this.onClickRoomInSpaceMode(room, selected);
        } else {
            if ( this.assignmentTarget && this.assignmentTarget.type=="team") {
                this.onClickRoomInTeamAssignMode(room, selected);
            } 
            else {
                this.onClickRoomInEmployeeMode(room, selected);
            }
        }
    },
    */

    onSelectMultipleRooms: function(selectAssets, drawingController){
        for (var key in selectAssets) {
            //assetsSelected[key] = selectAssets[key];
            for ( var i=0; i<selectAssets[key].length; i++ ) {
                spaceExpressConsoleDrawing.processSingleRoomAsset(selectAssets[key][i], key, true);
            }
        }
    
        //setInforWindowContent(drawingController, assetsSelected);
    },
    
    onClickRoom: function(params, drawingController){
        var selectController = drawingController.getController("SelectController");
        selectController.toggleAssetSelection(params['svgId'], params['assetId']);
        //drawingControl.InfoWindow.setText("Selected rooms:<br> ")\;
        var selectAssets = selectController.selectedAssets;
        var svgId = params['svgId'];
        var selected = false;
        if ( selectAssets && selectAssets[svgId] && selectAssets[svgId].length>0 ) {
            for ( var i=0; i< selectAssets[svgId].length; i++ ) {
                 if (  selectAssets[svgId][i] == params['assetId'] )
                   selected  = true;
            }
        }

        spaceExpressConsoleDrawing.processSingleRoomAsset(params['assetId'], svgId, selected);
    },  

    processSingleRoomAsset: function(assetId, svgId,  selected) {
        var pk = assetId.split(";");
        var room = { bl_id: pk[0], fl_id: pk[1], rm_id: pk[2], svgId: svgId, assetId: assetId  };
        var thisController =   spaceExpressConsoleDrawing;
        if (thisController.mode === 'spaceMode') {
            thisController.onClickRoomInSpaceMode(room, selected);
        } else {
            if ( thisController.assignmentTarget && thisController.assignmentTarget.type=="team") {
                thisController.onClickRoomInTeamAssignMode(room, selected);
            } 
            else {
                thisController.onClickRoomInEmployeeMode(assetId, svgId, room, selected);
            }
        }   
    },

    /**
     * Process the click event in space mode.
     * add by heqiang
     */
    onClickRoomInSpaceMode: function(room, selected) {
        //in assign model, highlight the room and add room to assignment container.
        if (this.assignmentTarget != null) {
            this.setAssignTargetColor(room, selected);
            //this.updateLocalRoomForReHighlight(this.assignedRooms, room, selected);
        } else {
            this.updateLocalRoomForReHighlight(this.selectedRooms, room, selected);
        }
        this.trigger('app:space:express:console:selectRoom', room, selected);
    },

    /**
     * Process the click event in team-assign mode.
     * add by ZY.
     */
    onClickRoomInTeamAssignMode: function(room, selected) {
        // check whether the room is occupiable for team.
        var rmRecord = abSpConsole_getRoomFromTeamOccupiableDs(room.bl_id, room.fl_id, room.rm_id, this.selectDateStart, this.selectDateEnd);
        if ( rmRecord ) {
            var isRoomOccupiableForTeam = parseInt( rmRecord.getValue('rm.occupiable') );
            if ( isRoomOccupiableForTeam===1 ) {
                this.setAssignTargetColor(room, selected);

                this.updateLocalRoomForReHighlight(this.assignedRooms, room, selected);
                room['selectDateStart'] = this.selectDateStart;
                room['selectDateEnd'] = this.selectDateEnd;

                var roomCpapcity = parseInt( rmRecord.getValue('rm.capacity') );
                room['roomCpapcity'] = roomCpapcity;

                this.trigger('app:space:express:console:selectRoom', room, selected);
            } else {
                this.drawingPanel.unselectAssets(this.getAssetForRoom(room));
            }
        }
    },

    /**
     * Process the click event in employee mode.
     * add by heqiang
     */
    onClickRoomInEmployeeMode: function(assetId, svgId, room, selected) {
        if (this.assignmentTarget != null) {
            
            // this.drawingPanel.unselectAssets(this.getAssetForRoom(room));
            this.unSelectDrawingAsset(this.getAssetForRoom(room));
            
            //If users don't select any employee, we give a warning message.
            if (this.isProhibitAssignEmployee()) {
                View.alert("Please first select an employee, then click on a room");
                return;
            } 
            
            //check if the room is non occupiable.
            if (this.checkRoomIfNonOccupiable(room, selected)) {
                var innerThis = this;
                View.confirm(getMessage('assignedRoomIsNonOccupiable'), function(button) {
                    if (button == 'yes') {
                        innerThis.processAssignEmployees(assetId, svgId, room, selected);
                        innerThis.updateLocalRoomForReHighlight(innerThis.movedEmployeesRooms, room, selected);
                    }
                });
            } else {
                this.processAssignEmployees(assetId, svgId, room, selected);
                this.updateLocalRoomForReHighlight(this.movedEmployeesRooms, room, selected);
            }
        } else {
            this.updateLocalRoomForReHighlight(this.selectedRooms, room, selected);
            this.trigger('app:space:express:console:selectRoom', room, selected);
        }
    },
    
    setAssignTargetColor: function(room, selected) {
        if (selected) {
            var highlightController = this.drawingControl.getDrawingController().getController("HighlightController"); 
            highlightController.highlightAsset(room.assetId, {svgId:  room.svgId, color: this.assignmentTarget.color , persistFill: false, overwriteFill: false});
        }
        else {
            room.dwgname=    this.getDwgnameByBlIdAndFlId(room.bl_id, room.fl_id);
            this.unHighlightRoom(room);     
        }
    },
    
    /**
     * To check if the room is non occupiable
     */
    checkRoomIfNonOccupiable: function(room, selected) {
        var occupiable = abSpConsole_getFieldValueFromOccupiableDs(room.bl_id, room.fl_id, room.rm_id, 'rmcat.occupiable');
        if(isNaN(occupiable) || occupiable == 0) {
            return true;
        }
        return false;
    },
    
    /**
     * If prohibit the user from assigning employees.
     */
    isProhibitAssignEmployee: function() {
        var rows = this.employeeGrid.getSelectedRows();
        return rows.length == 0;
    },
    
    /**
     * Process rooms when assign employees.
     */
    processAssignEmployees: function(assetId, svgId, room, selected) {

        // this.drawingPanel.unselectAssets(this.getAssetForRoom(room));
        this.unSelectDrawingAsset(this.getAssetForRoom(room));

        var appendRec = room.bl_id + ';' + room.fl_id + ';' + room.rm_id;
        var dwgname = this.getDwgnameByBlIdAndFlId(room.bl_id, room.fl_id);
        
        //if only one employee is selected, then show the employee's name on the drawing panel.
        for(var i = 0 ; i < this.assignmentTarget.employees.length; i++) {
            var em_id = this.assignmentTarget.employees[i].em_id;
            var employeeLabel = [];
            var labelArray = {field:'em.em_id', record:{'em.em_id':em_id, 'bl_id':room.bl_id, 'fl_id':room.fl_id, 'rm_id':room.rm_id}};
            employeeLabel.push(labelArray);
            //this.drawingPanel.addLabels(employeeLabel);
            this.addEmployeeLabelForPendingAssignment(assetId, svgId, em_id, room);
        }
        
        //highlight the room.
        if (selected) {
            this.trigger('app:space:express:console:selectRoom', room, selected);
            this.reHighlightRoom( {
                        bl_id:room.bl_id,
                        fl_id:room.fl_id,
                        rm_id:room.rm_id, 
                        dwgname: dwgname
                    });
        }
    },
    
    addEmployeeLabelForPendingAssignment: function(assetId, svgId, emId, room) {
        //var target = d3.select(room.bl_id+";"+room.fl_id+";"+room.rm_id+";"+this.selectedFloors[0].layerName);
        var layer = "rm" ;
        var assetController = this.drawingControl.drawingController.getController("AssetController");
        var assetNode = assetController.getAssetById(svgId, assetId);
        var target = assetNode[0][0];

        if ( target ) {
            var assetPanelAddOn = this.drawingControl.getAddOn("AssetPanel");
            var newLabel = assetPanelAddOn.addLabelToLayer(emId, layer, target);
            newLabel.classed({'ap-panel-new-label' : true});
            assetPanelAddOn.addDragDropEventsToLayerItem(newLabel.node(), layer, {});
            assetPanelAddOn.addMouseOverEventToLayerItem(newLabel.node());
            assetPanelAddOn.addMouseOutEventToLayerItem(newLabel.node());
        }
    },

    /**
     * Check whether a room is assigned.
     */
    isRoomAssigned: function(room) {
        var currentPendingAssignments = this.drawingPanel.getSidecar().get('pendingAssignments');
        for( var i = 0; i < currentPendingAssignments.models.length; i++) {
            var assignment = currentPendingAssignments.models[i];
            var bl_id = assignment.attributes.to_bl_id;
            var fl_id = assignment.attributes.to_fl_id;
            var rm_id = assignment.attributes.to_rm_id;
            if (room.bl_id == bl_id && room.fl_id == fl_id && room.rm_id == rm_id) {
                return true;
            }
        }
        return false;
    },
    
    /**
     * Update the local copy of all assigned rooms.
     */
    updateLocalRoomForReHighlight: function(roomContainer, room, selected) {
        if(selected) {
            if (!roomContainer.findRoom(room)) {
                roomContainer.addRoom(room);
            }
        } else {
            roomContainer.removeRoom(room);
        }
    },

    /**
     * Deletes asset record when the asset panel row delete action is selected
     * 
     * @param {Object} rowRecord 
     * @param {String} dataSourceName 
     */
    deleteWaitingEmployee: function (rowRecord, dataSourceName){
        var assignment = spaceExpressConsoleDrawing.createEmployeeAssignmentFromDrawingRecord(rowRecord);
        spaceExpressConsoleDrawing.removeEmployeeWaiting(assignment);
        spaceExpressConsoleDrawing.refreshEmployeeWaitingPanel();
        spaceExpressConsoleDrawing.trigger('app:space:express:console:removeEmployeeWaiting', assignment);
        //View.alert(toJSON(rowRecord) +" || datasouce name: "+ dataSourceName);
    },

    /**
     * The drawing calls this method when the user drags an employee from one room to another,
     * from a room to the asset panel, or from the asset panel to a room.
     * @param record
     * @param assetType
     * @return {Boolean}
     */
    onDropAsset: function(record, assetType) {
        if (assetType == 'em') {
            var assignment = this.createEmployeeAssignmentFromDrawingRecord(record);
            var em_id = assignment.em_id;
            var from_bl_id = assignment.bl_id;
            var from_fl_id = assignment.fl_id;
            var from_rm_id = assignment.rm_id;
            var bl_id = assignment.to_bl_id;
            var fl_id = assignment.to_fl_id;
            var rm_id = assignment.to_rm_id;
            
            if ( from_bl_id==bl_id && from_fl_id==fl_id && from_rm_id==rm_id ) {
                return false;
            }
            
            var room = {
                bl_id: bl_id,
                fl_id: fl_id,
                rm_id: rm_id
            };
            if (this.checkUserVPAPass(assignment)) {
                
                if (rm_id != '') {
                    this.trigger('app:space:express:console:moveEmployee', assignment);
                    if (!this.movedEmployeesRooms.findRoom(room)) {
                        this.movedEmployeesRooms.addRoom(room);
                    }
                    this.removeEmployeeWaiting(assignment);
                    var dwgName = this.getDwgnameByBlIdAndFlId(bl_id, fl_id);
                    this.reHighlightRoom( {
                                bl_id:bl_id,
                                fl_id:fl_id,
                                rm_id:rm_id, 
                                dwgname: dwgName
                            });
                } else {
                    //If the user drag an employee from room to waiting room, just refresh the waiting panel.
                    this.trigger('app:space:express:console:moveEmployee', assignment);
                    if (this.movedEmployeesRooms.findRoom(room)) {
                        this.movedEmployeesRooms.removeRoom(room);
                    }
                    this.employeesWaiting.push(assignment.em_id);
                    this.drawingPanel.getSidecar().set('employeesWaiting', this.employeesWaiting);
                    this.drawingPanel.getSidecar().save();
                    this.refreshEmployeeWaitingPanel();
                }
                // console is a 'read-only' version, for those users who do not have authority to make any edits or assignments
                this.disableButtonIfReadOnlyUserGroup();

                // unhighlight from room
                var fromDwgName = this.getDwgnameByBlIdAndFlId(from_bl_id, from_fl_id);
                if ( fromDwgName )  {
                    this.unHighlightRoom({
                        bl_id:from_bl_id,
                        fl_id: from_fl_id,
                        rm_id:from_rm_id, 
                        dwgname: fromDwgName
                    });
                }
            } else {
                alert(getMessage('dropEmPromptVPA'));
                return false;
             }
        }
        return true;
    },
    
    /**
     * Refresh the employee waiting panel when the user do certain update operation.
     * add by heqiang
     */
    refreshEmployeeWaitingPanel: function() {
        var restriction = new Ab.view.Restriction();
        if (this.employeesWaiting && this.employeesWaiting.length > 0) {
            restriction.addClause('em.em_id', this.employeesWaiting, 'IN');
        } else {
            restriction.addClause('em.em_id', 'none', '=');
        }
        this.drawingControl.getAddOn("AssetPanel").refreshAssetPanel(restriction);
    },
    
    /**
     * check if user VPA permit em move to dest room.
     */
    checkUserVPAPass: function(assignment) {
        var bl_id = assignment.to_bl_id;
        var fl_id = assignment.to_fl_id;
        var rm_id = assignment.to_rm_id;
        //if move to waiting room. return true.
        if (rm_id == '') {
            return true;   
        }
        var userRoomsDS = View.dataSources.get("userRoomsDS");
        userRoomsDS.addParameter('bl_id', bl_id);
        userRoomsDS.addParameter('fl_id', fl_id);
        userRoomsDS.addParameter('rm_id', rm_id);
        var records = userRoomsDS.getRecords();
        if(records != null && records.length > 0){
            return true;
        }
        return false;   
    },
    
    /**
     * Make console a 'read-only' version for those users who do not have authority to make any edits or assignments
     */
    disableButtonIfReadOnlyUserGroup: function() {
        this.drawingPanel.actionbar.actions.each(function (action) {
            if (action.id == 'commitPendingAssignments') {
                action.enable(View.user.isMemberOfGroup('SPACE-CONSOLE-ALL-ACCESS'));
            }
        });
    },
    
    /**
     * Highlight room by passed-in parameter appendRec.
     */
    reHighlightRoom: function(room) {
        this.highlightDrawingAsset(room);
    },
    
    /**
     * Highlight room by passed-in parameter appendRec.
     */
    unHighlightRoom: function(room) {
        var svgId = ('drawingDiv-' + room.dwgname + '-svg' );
        var assetId = (room.bl_id+";"+room.fl_id+";"+room.rm_id);
        
        var selectController = this.drawingControl.drawingController.getController("SelectController");
        var assetController = this.drawingControl.drawingController.getController("AssetController");
        var highlightController = this.drawingControl.getDrawingController().getController("HighlightController");


        selectController.clearDiagonalSelection(assetController, svgId, assetId);
        //selectController.toggleAssetSelection(svgId, assetId);
        highlightController.resetAsset(svgId,assetId);
    },

    /**
     * Updates the employee assignments in the waiting for specified new assignment.
     * @param assignment
     */
    updateEmployeesWaiting: function(assignment) {
        if (assignment.to_bl_id === '') {
            // the user has dropped an employee onto the bus - add the assignment to the bus
            var deletedRoom = { 
                     bl_id: assignment.bl_id,
                     fl_id: assignment.fl_id,
                     rm_id: assignment.rm_id};
            if (this.movedEmployeesRooms.findRoom(deletedRoom)) {
                this.movedEmployeesRooms.removeRoom(deletedRoom);
            }
            this.employeesWaiting.push(assignment.em_id);
            this.drawingPanel.getSidecar().set('employeesWaiting', this.employeesWaiting);
            this.drawingPanel.getSidecar().save();
        } else {
            // the user has dragged an employee from the bus, or between rooms -
            // if the assignment is on the bus, remove it from the bus
            this.removeEmployeeWaiting(assignment);
            var addedRoom = { 
                    bl_id: assignment.to_bl_id,
                    fl_id: assignment.to_fl_id,
                    rm_id: assignment.to_rm_id};
            
            if (!this.movedEmployeesRooms.findRoom(addedRoom)) {
                this.movedEmployeesRooms.addRoom(addedRoom);
            }
        }
    },

    /**
     * Removes specified employee from the waiting room, and removes the pending assignment.
     * @param assignment
     */
    removeEmployeeWaiting: function(assignment) {
        this.employeesWaiting = _.reject(this.employeesWaiting, function(em_id) {
            return assignment.em_id === em_id;
        });
        this.drawingPanel.getSidecar().set('employeesWaiting', this.employeesWaiting);
        this.drawingPanel.getSidecar().save();
    },

    /**
     * Adds specified employees to waiting room.
     * @param assignments
     */
    addEmployeeWaiting: function(assignments) {
        for (var i = 0; i < assignments.length; i++) {
            this.updateEmployeesWaiting(assignments[i]);
        }
        this.refreshEmployeeWaitingPanel();
    },

    /**
     * Called when the user removes a pending assignment.
     */
    removeAssignment: function(assignment) {
        this.removeEmployeeWaiting(assignment);
        this.refreshEmployeeWaitingPanel();
        var rooms = this.getAssignedRoom(assignment);
        
        if (this.mode == 'spaceMode') {
            this.assignedRooms.removeRoom(assignment);
        } 
        else {
            this.movedEmployeesRooms.removeRoom(assignment);
        }

        //this.drawingPanel.unselectAssets(rooms);
        if ( this.drawingControl && this.drawingControl.drawingController && this.drawingControl.drawingController.isDrawingsLoaded ) {
            this.unSelectDrawingAsset(this.getAssetForRoom(assignment));
            
            var dwgName = this.getDwgnameByBlIdAndFlId(assignment.bl_id, assignment.fl_id);
            this.unHighlightRoom({
                bl_id:assignment.bl_id,
                fl_id: assignment.fl_id,
                rm_id:assignment.rm_id, 
                dwgname: dwgName
            });
        }
    },
    
    /**
     * Obtain a rooms array from assignment.
     */
    getAssignedRoom: function(assignment) {
        var rooms = [];
        var room  = [];
        room[0] = assignment.bl_id;
        room[1] = assignment.fl_id;
        room[2] = assignment.rm_id;
        rooms.push(room);
        return rooms;
    },
    
    /**
     * Obtain a rooms array from room.
     */
    getAssetForRoom: function(room) {
        var rooms = [];
        var roomArray  = [];
        roomArray[0] = room.bl_id;
        roomArray[1] = room.fl_id;
        roomArray[2] = room.rm_id;
        rooms.push(roomArray);
        return rooms;
    },

    /**
     * Immediately unassigns specified employees. If there are pending assignments for some employees, removes them.
     * @param assignments
     */
    unassignEmployees: function(assignments) {
        for (var i = 0; i < assignments.length; i++) {
            this.removeEmployeeWaiting(assignments[i]);
        }
        this.refreshEmployeeWaitingPanel();
    },

    showSelectDateBar: function(isShow) {
        if ( jQuery("#drawingPanel_selectDateBar") ) {
            if (isShow){
                jQuery("#drawingPanel_selectDateBar").removeClass("x-hide-display");
                jQuery("#filterFloor").removeClass("x-hide-display");
            }
            else {
                jQuery("#drawingPanel_selectDateBar").addClass("x-hide-display");
                jQuery("#filterFloor").addClass("x-hide-display");
            }

            // kb#3052542: every time show/hide the 'Select Date' in drawing's toolbar, uncheck the 'Show Available Floors' checkbox and clear that restriction from the Locations grid.
            if ( $("filterFloorCheckbox") ) {
                $("filterFloorCheckbox").checked = false;
                this.trigger('app:space:express:console:clearFilterFloorForTeamAssign', this.selectDateStart, this.selectDateEnd);
            }           
        }
    },
    
    /**
     * clear assignment mode.
     */
    clearAssignmentTarget: function(isTeamAssignMode) {
        // 23.1 when quit from team assign mode, hide the Fiter floor check box and restore the selections of highlight/labels
        this.showSelectDateBar(false);

        if ( isTeamAssignMode ) {
            this.restoreSelectionForTeamAssignMode();
         }

        this.assignmentTarget = null;
        if (this.mode === 'spaceMode') {
            this.assignedRooms = new Ab.space.express.console.Rooms();
        } else {
            this.movedEmployeesRooms = new Ab.space.express.console.Rooms();
        }
    },
    
    /**
     * Called after the assignment has been initialized.
     */
    afterBeginAssignment: function(assignmentTarget, pendingAssignments) {
        this.pendingAssignments = pendingAssignments;
        this.setToAssign(assignmentTarget);
        this.assignmentTarget = assignmentTarget;
    },

    /**
     * Called after the user has selected a different assignment target (e.g. another department).
     * @param assignmentTarget
     */
    afterChangeAssignmentTarget: function(assignmentTarget) {
        this.assignmentTarget = assignmentTarget;
        this.setToAssign(assignmentTarget);
    },

    /**
     * Sets the assignment target for the drawing control.
     * @param assignmentTarget
     */
    setToAssign: function(assignmentTarget) {
        //For 23.1 Team Space
        if (     assignmentTarget.type === 'team' ){
            if ( $('drawingPanel_selectDateBar') ){
                this.showSelectDateBar(true);
            } else {
                this.appendSelectDateToActionBar();
            }
        } 
        else if ( $('drawingPanel_selectDateBar') ) {
            this.showSelectDateBar(false);
        }

        if ( this.drawingControl && this.drawingControl.drawingController && this.drawingControl.drawingController.isDrawingsLoaded ) {
            if (assignmentTarget.type === 'division') {
                //this.drawingPanel.setToAssign("rm.dv_id", assignmentTarget.dv_id);
            } else if (assignmentTarget.type === 'department') {
                 //this.drawingPanel.setToAssign("rm.dp_id", assignmentTarget.dp_id);
            } else if (assignmentTarget.type === 'category') {
                //this.drawingPanel.setToAssign("rm.rm_cat", assignmentTarget.rm_cat);
            } else if (assignmentTarget.type === 'type') {
                //this.drawingPanel.setToAssign("rm.rm_type", assignmentTarget.rm_type);
            } else if (assignmentTarget.type === 'standard') {
                //this.drawingPanel.setToAssign("rm.rm_std", assignmentTarget.rm_std);
            } else if(assignmentTarget.type === 'team') {
                //this.drawingPanel.setToAssign("rm.team_id", assignmentTarget.team_id);
            }
        }

        if(assignmentTarget.type === 'team') {
            this.setDrawingForTeamAssignMode();
        }

        if (assignmentTarget.hpattern) {
            assignmentTarget.color = this.getColorFromHpattern( assignmentTarget.hpattern );
        }
    },

     getColorFromHpattern: function(patternString) {
        var parameters = {
            "patternString": patternString
        };

        try {
            var result = Workflow.call('AbCommonResources-HighlightPatternService-decodePattern', parameters);
            var res = eval("(" + result.jsonExpression + ")");
            return "#"+res.rgbColor;
        } 
        catch (e) {
            Workflow.handleError(e);
            return null;
        }
     },
    
    /**
     *  Added for 23.1: logics for team assign functionality, by ZY, 2016-01-19. 
     *      - When the user clicks any Assign button in the Teams window, a form appears above the drawing control that asks the user for a Date Range. 
     *      - The drawing control will display a checkbox called "Show Available Floors" when the user is in Assignment mode for teams. 
     */
    setDrawingForTeamAssignMode: function() {
        if ( !this.isAlreadyInTeamAssignMode() ) {
            this.setSelectionForTeamAssignMode();
        }
    },

     /**
     *  Return true if current existing pending assignments is for team. 
     */
   isAlreadyInTeamAssignMode: function() {
        var isTeamPending = false;
        if ( this.pendingAssignments && this.pendingAssignments.length>0 ) {
            var assignment = this.pendingAssignments.models[0];
            if (assignment.attributes.team_id) {
                isTeamPending = true;
            }
        }
        return isTeamPending;
    },

    /**
     *  Added for 23.1: logics for team assign functionality, by ZY, 2016-01-19.
     *      - This will filter the Locations list for floors that have enough available capacity to satisfy the team��s target ratio.  
     *      - The purpose of this is to give the user a quick way of finding floors that have room for the team without requiring the user to split the team into multiple floors.
     */
    bindEventsForFilterFloorCheckBox: function() {
        //Bind text changed event.
        var thisController = this;
        jQuery('#filterFloorCheckbox').on('change', function() {  
            var isChecked = jQuery("#filterFloorCheckbox")[0].checked;
            if (isChecked) {
                var requiredFloorCapcity = thisController.calculateRequiredFloorCapcity();
                thisController.trigger('app:space:express:console:filterFloorsForTeamAssign', thisController.selectDateStart, thisController.selectDateEnd, requiredFloorCapcity);
            } else {
                thisController.trigger('app:space:express:console:clearFilterFloorForTeamAssign', thisController.selectDateStart, thisController.selectDateEnd);
            }
        });
    },

    /**
     *  Added for 23.1: logics for team assign functionality, by ZY, 2016-01-21. 
     *    - Determine the number of additional seats that the team requires to meet the target seat ratio. 
     */
    calculateRequiredFloorCapcity: function() {
        var teamId = this.assignmentTarget.team_id;
        View.dataSources.get('teamsDS').addParameters(this.filter.parameters);
        var teamRecord = View.dataSources.get('teamsDS').getRecord("team_properties.team_id='"+teamId+"'");
        if (teamRecord) {
            var emCount = teamRecord.getValue('team_properties.vf_em_count');
            var ratio = teamRecord.getValue('team_properties.em_seat_ratio_tgt');
            var cap = teamRecord.getValue('team_properties.vf_cap_em');
            if ( ratio==0 ){
                return 0;
            } else {
                var requiredCap = emCount/ratio - cap;
                return Math.ceil(requiredCap);
            }
        } else {
            return 0;
        }
    },  

    /**
    *  Added for 23.1: logics for team assign functionality, by ZY, 2016-01-19.
     *      - While in Team Assignment mode, the Highlights drop-down will switch to None, and cannot be changed to a different highlight.  
     *      - The Labels drop-down will automatically switch to Teams when the user is Assignment Mode, but the label must be modified to account for the date range.  The label will display the team code and date range o all teams that occupy a room during the selected date range.
     *      - The Teams border will also check for all team assigning to rooms during the selected date range.  If multiple teams occupy a room during the date range, then use a dark grey color for the border.
     *      - The user can choose different labels or borders while in assignment mode.
     */
    setSelectionForTeamAssignMode: function() {
        //this.setDrawingDataSource("selector_hilite", "highlightTeamAssignDs");
        //this.setDrawingDataSource("selector_labels", "labelTeamsAssignDs");
        this.switchHighlightSelections(true);

        document.getElementById("selector_hilite").value = "highlightTeamAssignDs";
        document.getElementById("selector_labels").value = "labelTeamsAssignDs";          
        
        var dsSelector = this.drawingControl.drawingController.getAddOn('DatasourceSelector');
        var combo = document.getElementById("selector_hilite");
        dsSelector.onSelectedDatasourceChanged(dsSelector, combo);

        document.getElementById('selector_IBorders').disabled = true;
        document.getElementById('selector_hilite').disabled = true;
    },

    restoreSelectionForTeamAssignMode: function() {
        this.switchHighlightSelections();

        this.setDrawingDataSource("selector_hilite", "highlightTeamDs");
        this.setDrawingDataSource("selector_labels", "labelTeamsDs");

        document.getElementById('selector_hilite').disabled = false;
        document.getElementById('selector_IBorders').disabled = false;

        // this.drawingPanel.refresh();
        //kb#3051870: update as of date with selected date_start back.
        if ( this.selectDateStart && this.asOfDate != this.selectDateStart ) {
            //also need to set new as of date back to filter in location view and do filter.
            this.trigger('app:space:express:console:asOfDateUpdate', this.selectDateStart);
        }
    },

    showSelectDateForm: function(el) {
        if ( this.selectDateStart ) {
            this.selectDateForm.setFieldValue('team.date_start', this.selectDateStart);
        } 
        else {
            this.selectDateForm.setFieldValue('team.date_start', '');
        }

        if ( this.selectDateEnd ) {
            this.selectDateForm.setFieldValue('team.date_end', this.selectDateEnd);
        } 
        else {
            this.selectDateForm.setFieldValue('team.date_end', '');
        }

        this.selectDateForm.newSelectDateStart=this.selectDateStart;
        this.selectDateForm.newSelectDateEnd=this.selectDateEnd;

        this.selectDateForm.showInWindow({
            anchor: el,
            width: 600,
            height: 250,
            title: getMessage('selectDateTitle'),
            closable: false,
            modal: true
        });
    },

    /**
    *  Added for 23.1: logics for team assign functionality, by ZY, 2016-01-19.
     *      - If the user changes the Start Date or End Date field while there are pending assignments, and click the 'Show' button, then generate a warning message:
     *      - 'WARNING: changing the date range will remove all pending Team Space assignments.  Do you wish to continue?' [OK/Cancel]
     *      - If the user clicks OK, the system will clear out any pending assignments.
     */
    selectDateForm_onShow: function() {
        if ( this.selectDateForm.newSelectDateEnd && this.selectDateForm.newSelectDateEnd<this.selectDateForm.newSelectDateStart) {
            View.alert(getMessage("selectDateError"));
            return;
        }

        var isDateChanged = ( this.selectDateForm.newSelectDateStart!=this.selectDateStart || this.selectDateForm.newSelectDateEnd!=this.selectDateEnd );
        if ( isDateChanged ){
            if ( this.pendingAssignments && this.pendingAssignments.length>0 ){
                var thisController = this;
                View.confirm(getMessage("changeDate"), function(result) {
                    if (result === 'yes') {
                        thisController.trigger('app:space:express:console:clearPendingAssignments');
                        thisController.selectDateStart = thisController.selectDateForm.newSelectDateStart; 
                        thisController.selectDateEnd = thisController.selectDateForm.newSelectDateEnd; 
                        var showButtonTitle = getMessage("highlightFrom")+" "+thisController.selectDateStart+(thisController.selectDateEnd? (" "+getMessage("highlightTo")+" "+thisController.selectDateEnd):" ");
                        $('showButton').innerText = showButtonTitle;
                        thisController.setSelectionForTeamAssignMode();
                    } else {
                        return;
                    }
                });
            } else {
                this.selectDateStart = this.selectDateForm.newSelectDateStart; 
                this.selectDateEnd = this.selectDateForm.newSelectDateEnd; 
                var showButtonTitle = getMessage("highlightFrom")+" "+this.selectDateStart+(this.selectDateEnd? (" "+getMessage("highlightTo")+" "+this.selectDateEnd):" ");
                $('showButton').innerText = showButtonTitle;
                this.setSelectionForTeamAssignMode();
            }
        }
        this.selectDateForm.closeWindow();

        // also need to set new as of date back to filter in location view and do filter.
        if ( this.selectDateStart && this.asOfDate != this.selectDateStart ) {
            this.trigger('app:space:express:console:asOfDateUpdate', this.selectDateStart);
        }
    },
        
    /**
    *  Added for 23.1: logics for team assign functionality, by ZY, 2016-01-19.
     *      - store the changed select date start and date end.
     */
    onSelectDateChange: function(field) {
        if ('selectDateForm_team.date_start'===field.id){
            this.selectDateForm.newSelectDateStart = this.selectDateForm.getFieldValue('team.date_start'); 
        } else {
            this.selectDateForm.newSelectDateEnd = this.selectDateForm.getFieldValue('team.date_end'); 
        }
    },

    /**
     * Called after the assignment has been canceled or committed.
     */
    afterClearAssignment: function(isTeamAssignMode) {
        this.employeesWaiting = [];
        this.drawingPanel.getSidecar().set('employeesWaiting', this.employeesWaiting);
        this.drawingPanel.getSidecar().save();

        this.assignedRooms = new Ab.space.express.console.Rooms();
        if(this.mode == 'employeeMode' && !isTeamAssignMode) {
            this.refreshEmployeeWaitingPanel();
        }
        this.assignmentTarget = null;
        if (this.drawingControl.drawingController.isDrawingsLoaded) {
            //after clear button, change back the previous drawing click statues.
            //this.drawingPanel.clearAssignCache(false);
            this.afterCommitPendingAssignment(isTeamAssignMode);
            
            //hide asset panel if it's spaceMode.
            if (this.mode === 'spaceMode') {
                if ( this.drawingControl && this.assetPanelLoaded) {
                    this.drawingControl.getAddOn("AssetPanel").closeAssetPanel();
                }
            }
        } else {
            this.showSelectDateBar(false);
        }
    },

    /**
     * Return a created assignment form drawing record.
     */
    createEmployeeAssignmentFromDrawingRecord: function(record) {

        var assignment = {
            type: 'employee',
            to_bl_id: '',
            to_fl_id: '',
            to_rm_id: ''
        };

        if (!record.from) {
            record.from = record;
        }

        assignment.em_id = record.from['em.em_id'];
        assignment.bl_id = record.from['rm.bl_id'];
        assignment.fl_id = record.from['rm.fl_id'];
        assignment.rm_id = record.from['rm.rm_id'];
        if (record.to) {
            assignment.to_bl_id = record.to['rm.bl_id'];
            assignment.to_fl_id = record.to['rm.fl_id'];
            assignment.to_rm_id = record.to['rm.rm_id'];  
        }

        if ( record.from['em.location'] ) {
            this.setLocationToAssignment(assignment, record.from['em.location']);
        }

        return assignment;
    },
    
    /**
     * Set location bl_id fl_id and rm_id to assignment.
     */
    setLocationToAssignment: function(assignment, location) {
        if (location&&location.indexOf("-")!=-1) {
            var locatioins = location.split("-");
        }
        assignment.bl_id = locatioins[0];
        assignment.fl_id = locatioins[1];
        assignment.rm_id = locatioins[2];
    },
    
    /**
     * When a room is over capacity, the assignment should be canceled.
     */
    rollbackEmployeeAssignment: function(room) {
        if (this.movedEmployeesRooms.findRoom(room)) {
            this.movedEmployeesRooms.removeRoom(room);
        }

        //comment below lines for html5 drawing control conversion
        //this.drawingPanel.clearPersistFills();
        //this.drawingPanel.refresh();
        this.reloadDrawings();
        this.clearDrawingSelections();
        this.reHighlightSelectedAssets.defer(1, this);
    },
    
    /**
     * view selected rooms in a pop up window.
     */
    drawingPanel_onViewDetails: function() {
        var anchor = this.drawingPanel.actionbar.toolbar.el.dom;
        this.trigger('app:space:express:console:viewSelectedRooms', anchor, this.asOfDate);
    },
    
    /**
     * Commit the assignment to WFR.
     */
    drawingPanel_onCommitPendingAssignments: function() {
        this.trigger('app:space:express:console:commitAssignment');
    },
    
    /**
     * We clear assignment target,clear drawing panel fills and refresh after the user commit pending assignments.
     */
    afterCommitPendingAssignment: function(isTeamAssignMode) {
        this.clearAssignmentTarget(isTeamAssignMode);
        //comment below lines for html5 drawing control conversion
        //this.drawingPanel.clearPersistFills();
        //this.drawingPanel.refresh();
        this.clearDrawingSelections();
        this.reloadDrawings();
    },
   
    /**
     * Cancel the pending assignments.
     */
    drawingPanel_onCancelPendingAssignments: function() {
        this.clearAssignmentTarget();

        //comment below lines for html5 drawing control conversion
        //this.drawingPanel.clearPersistFills();
        //this.drawingPanel.refresh();
        this.reloadDrawings();
        this.clearDrawingSelections();
        
        this.employeesWaiting = [];
        this.drawingPanel.getSidecar().set('employeesWaiting', this.employeesWaiting);
        this.drawingPanel.getSidecar().save();
        this.trigger('app:space:express:console:cancelAssignment');
    },
    
    /**
     * view the pending assignments in a popup window.
     */
    drawingPanel_onViewPendingAssignments: function() {
        var anchor = this.drawingPanel.actionbar.toolbar.el.dom;
        this.trigger('app:space:express:console:viewAssignment', anchor, this.asOfDate);
    },
    
    /**
     * cancel the selected rooms when click the Clear button.
     */
    drawingPanel_onCancelSelectedRooms: function() { 
        this.clearDrawingSelections();

        this.selectedRooms = new Ab.space.express.console.Rooms();
        //this.drawingPanel.refresh();
        this.trigger('app:space:express:console:cancelSelectedRooms');
    },
    drawingPanel_beforeRefresh:function(){
        console.log("drawingPanel refresh");
    },
    /**
     * Export the floors to paginate report.
     */
    onExportDrawing: function(exportType) {
        if ( !$('selector_hilite') ||  !$('selector_hilite').value ) {
            View.alert(getMessage('noExportForLocate'));
            return;
        }
        
        if ( $('selector_hilite').value=="highlightNoneDs" ) {
            View.alert(getMessage('highlightNoneExport'));
            return;
        }       
                                     
        var me=this;
        if  ( this.locationsGrid.rows.length>10 ){
            View.confirm(getMessage("muchFloors"), function(result) {
                if (result == 'yes') {
                    me.exportDrawing(exportType);
                } else {
                    return;
                }
            });
        } 
        else {
            this.exportDrawing(exportType);
        }
    },
    
    exportDrawing:function(exportType){
        if (exportType == 'pdf') {
            this.onExportingDrawingPdf();
        } else if (exportType == 'docx') {
            this.onExportingDrawingDOCX();
        } else {
            View.showMessage( getMessage("notSupportedExportingFormat") );
        }       
    },
    
    /**
     * Export the drawings in the location list to pdf format.
     */
    onExportingDrawingPdf: function(parameters) {
        if ("highlightNoneDs" == this.drawingPanel.currentHighlightDS) {
            View.showMessage( getMessage("noHighlight") );
        } else {
            //this.trigger('app:space:express:console:printPDF', this.filter, this.drawingPanel, this.selectedFloors, parameters);
            var controller = View.controllers.get('spaceExpressConsoleDrawingExportCtrl');
            controller.printReport(this,'pdf', parameters);
        }
    },
    
    /**
     * Export the drawings in the location list to docx format.
     */
    onExportingDrawingDOCX: function() {
        if ("highlightNoneDs" == this.drawingPanel.currentHighlightDS) {
            View.showMessage( getMessage("noHighlightDOCX") );
        } else {
            this.trigger('app:space:express:console:printDOCX', this);
        }
    },
    
    /**
     * Exports current drawing image in Docx. Supports to display passed restriction and title.
     * Similar as Flash drawing control's export.
     */
    onExportCurrentDrawingDOCX: function(){
        var controller = View.controllers.get('spaceExpressConsoleDrawingExportCtrl');
        controller.printReport( this, 'docx', null, true);
    },

    /**
     * Open print option dialog for users to control print parameters.
     */
    openPrintOptionDialog: function() {
        var thisController = this;
        var printOptionDialog = View.openDialog("ab-sp-console-print-option-dialog.axvw", null, true, {
            width:900,
            height:600,
            collapsible: false,
            afterViewLoad: function(dialogView) {
                var dialogController = dialogView.controllers.get("printOptionDialogController");
                dialogController.callback = thisController.printOptionDialogCloseHandler.createDelegate(thisController);
                dialogController.selectedPlanType = thisController.selectedPlanType;
                dialogController.legendDataSource  = thisController.currentLegendDataSource;
                dialogController.legendPanel = thisController.currentLegendPanel;
                dialogController.selectionValues = thisController.selectionValues;
            }
        });
    },
    
    /**
     * When close the print option dialog and start to export pdf report.
     */
    printOptionDialogCloseHandler: function(parameters) {
        if ( !$('selector_hilite') ||  !$('selector_hilite').value ) {
            View.alert(getMessage('noExportForLocate'));
            return;
        }

        parameters.selectionValues['zoomedInOption']==='no'
        if ( parameters.selectionValues['zoomedInOption']==='no' && $('selector_hilite').value==="highlightNoneDs" ) {
            View.alert(getMessage('highlightNoneExport'));
            return;
        }       

        var me=this;

        // for 23.2, merge the Backhground layer selection into 'Ad Hoc report' dialog.
        this.backgroundLayerDialogCloseHandler(parameters.selectionValues['backgroundLayerOption']);

        if  ( this.locationsGrid.rows.length>10 && ( parameters.selectionValues['zoomedInOption']==='no') ){
            View.confirm(getMessage("muchFloors"), function(result) {
                if (result == 'yes') {
                    me.onExportingDrawingPdf(parameters);
                } else {
                    return;
                }
            });
        } 
        else {
            this.onExportingDrawingPdf(parameters);
        }

        this.selectionValues = parameters.selectionValues;
    },

    /**
     * Open the plan types dialog to allow the users to select plan types option. 
     */
    openPlanTypesDialog: function() {
        var thisController = this;
        var planTypesDialog = View.openDialog("ab-sp-console-plan-types-dialog.axvw", null, true, {
            width:500,
            height:160,
            collapsible: false,
            afterViewLoad: function(dialogView) {
                var dialogController = dialogView.controllers.get("planTypesGroupDialogController");
                dialogController.callback = thisController.openPlanTypesDialogCloseHandler.createDelegate(thisController);
                dialogController.initialOptionValue = thisController.selectedPlanType;
            }
        });
    },
    
    /**
     * Highlight by plan type.
     */
    openPlanTypesDialogCloseHandler: function(planType) {
        if (planType != 'none') {
            this.selectedPlanType = planType;
            var activePlanTypeDs = View.dataSources.get('activePlanTypesDS');
            var restriction = new Ab.view.Restriction();
            restriction.addClause('active_plantypes.plan_type', planType, '=');
            var records = activePlanTypeDs.getRecords(restriction);
            if(records.length > 0) {
                this.setHighlightParameterByPlanType(records[0]);
            }
            
            jQuery("#selector_hilite").val('highlightNoneDs');
            jQuery("#selector_IBorders").val('iBorderHighlightNoneDs');
            jQuery("#selector_labels").val('labelNoneDs');

        }  else if (planType == 'none') {
            this.selectedPlanType = null;
            this.drawingPanel.currentHighlightDS = 'None';
            this.drawingPanel.currentLabelsDS = 'None';
            // this.drawingPanel.refresh();
            this.drawingControl.config.plan_type = null;
            //this.drawingControl.config.setConfigParameter("plan_type", null);
            this.reloadDrawings();
        }
    },

    setHighlightParameterByPlanType: function(record) {
        var planType = record.getValue('active_plantypes.plan_type');
        this.drawingControl.config.plan_type = planType;
        //this.drawingControl.config.setConfigParameter("plan_type", planType);
        
        this.reloadDrawings();
    },
        
    /**
     * Add background layer to the flash drawing.
     */
    backgroundLayerDialogCloseHandler: function(parameters) {
        // store selected background file suffix as well as option value
        this.backgroundSuffix = parameters['rule_suffix'];
    },

    /**
     * Added for 23.1: according to current status of the teams tab, show/hide related options of Highlight/Label/Border for team space.
     */
    enableTeamSpace: function(enabled) {
        this.isTeamSpaceEnabled = enabled;
        this.switchHighlightSelections();
    },
        
    /**
    * Added for 23.1: Show/Hide highlight/border/label select options related to 'Team Space' functionality by default.
    */
    switchHighlightSelections: function(isTeamAssign){
        if ( !this.drawingControl ) {
            return;
        }
        var dsSelector = this.drawingControl.drawingController.getAddOn('DatasourceSelector');

        var hlDsNameIdsMap = dsSelector.getSelectorDatasources("DrawingControlHighlight");  
        this.switchDsSelectorOptions("selector_hilite", hlDsNameIdsMap, this.isTeamSpaceEnabled, isTeamAssign);

        var borderDsNameIdsMap = dsSelector.getSelectorDatasources("DrawingControlHighlight", 'IBorder');
        this.switchDsSelectorOptions("selector_IBorders", borderDsNameIdsMap, this.isTeamSpaceEnabled, isTeamAssign);
                                                                    
        var labelDsNameIdsMap = dsSelector.getSelectorDatasources('DrawingControlLabels');      
        this.switchDsSelectorOptions("selector_labels", labelDsNameIdsMap, this.isTeamSpaceEnabled, isTeamAssign);
    },


    /**
    * According to layout type, manually set options in drop-down list for highlight/label/boorder DataSources.
    */
    switchDsSelectorOptions: function(selectorId, dsMap, isTeamEnabled, isTeamAssign){
        for (var name in dsMap) {
            var dsId =  dsMap[name];
             if ( dsId.indexOf("None")>0 ) {
                 continue;
             }
            else if ( !isTeamAssign && dsId.indexOf("Assign")>0 || !isTeamEnabled && dsId.indexOf("Team")>0 ) {

                jQuery("#"+selectorId+" option[value='"+dsId+"']").remove();
            }
            else  if ( jQuery("#"+selectorId+" option[value='"+dsId+"']").length==0 ) {
                jQuery("#"+selectorId).append("<option value='"+dsId+"'>"+name+"</option>");
            }
        }
    },

    /**
    * Added for 23.1 Team Space: add 'Select Date' UI to action bar or drawing panel.
    */
    appendSelectDateToActionBar: function(){
        var tdNode = Ext.get('drawingPanel_actionbar').dom.childNodes[0].childNodes[0].childNodes[0].childNodes[0];

        tdCell = Ext.DomHelper.insertBefore(tdNode,  {
            tag : 'td',
            id : 'filterFloor'
        });
        var str = "<input type='checkbox' id='filterFloorCheckbox' style='margin-left:15px;'/>";
        var filterFloorCheckbox = Ext.DomHelper.append(tdCell, str , true);
        var label = Ext.DomHelper.append(tdCell,  "<label for='filterFloorLabel' style='margin-right:25px;'><b translatable='true'>"+" "+getMessage("filterFloorTitle")+"</b><img src='/archibus/schema/ab-core/graphics/icons/help.png' style='margin-left:5px;' title='"+getMessage("helpAvailableFloor")+"'/></label>", true);
        this.bindEventsForFilterFloorCheckBox();
        
        this.selectDateStart = this.asOfDate;

        var showButtonCell = Ext.DomHelper.insertBefore(tdCell,  {
            tag : 'td',
            id : 'drawingPanel_selectDateBar'
        });
        var showButtonTitle = getMessage("highlightFrom")+" "+this.selectDateStart+(this.selectDateEnd? (" "+getMessage("highlightTo")+" "+this.selectDateEnd):" ");
        var showButtonHtml = "<table style='margin-left:15px;' border='0' cellpadding='0' cellspacing='0' class='x-btn-wrap x-btn mainAction ' id='showForDate'><tbody><tr><td class='x-btn-left'><i>&nbsp;</i></td><td class='x-btn-center'><em unselectable='on'><button class='x-btn-text' type='button' id='showButton'>"+showButtonTitle+"</button></em></td><td class='x-btn-right'><i>&nbsp;</i></td></tr></tbody></table>";
        Ext.DomHelper.append(showButtonCell, showButtonHtml, true);

        var thisController = this;
        document.getElementById('showForDate').onclick = function() {
            thisController.showSelectDateForm(this);
        }
    },

    show2D: function(){
        this.show = true;
        this.drawingPanel.show(true);

        // TODO: load floors 2d drawings
        for ( var i=0; i<this.selectedFloors.length; i++){
            var drawingName = this.selectedFloors[i].dwgname;
            var loadedDrawings = this.drawingControl.drawingController.control.drawings; 
            if ( !loadedDrawings || !loadedDrawings[drawingName] ) {
                var parameters = new Ab.view.ConfigObject();
                parameters['pkeyValues'] = {'bl_id': this.selectedFloors[i].bl_id, 'fl_id':  this.selectedFloors[i].fl_id};
                parameters['drawingName'] = drawingName;
                this.drawingControl.load(parameters);
            }
        }

        this.enable3D();
    },

    /**
     * The system checks for 3D enterprise graphics files by looking at table afm_enterprise_graphics for records of file_type = "3DBIN" and for the bl_id and fl_id values of  the loaded 2D floor. 
     */
    enable3D: function() {
        if ( this.selectedFloors && this.selectedFloors.length>0 && this.has3dLicense ) {
            var restriction = new Ab.view.Restriction();
            for ( var i = 0; i < this.selectedFloors.length; i++ ) {
                var selectedFloor = this.selectedFloors[i];
                if ( i===0 ) {
                    restriction.addClause('afm_enterprise_graphics.fl_id', selectedFloor.fl_id, '=');   
                } else {
                    restriction.addClause('afm_enterprise_graphics.fl_id', selectedFloor.fl_id, '=', ') or ('); 
                }
                restriction.addClause('afm_enterprise_graphics.bl_id', selectedFloor.bl_id, '='); 
                restriction.addClause("afm_enterprise_graphics.file_type", '3DBIN', '=');
            }

            var records = View.dataSources.get("3d_drawings_ds").getRecords(restriction);
            if (records && records.length>0) { 
                this.drawingPanel.actions.get("3dSwitch").enableButton(true);
            }   
            else {
                this.drawingPanel.actions.get("3dSwitch").enableButton(false);
            }
        } 
        else {
            this.drawingPanel.actions.get("3dSwitch").enableButton(false);
        }
    },

    switchTo3D: function(){
        this.show = false;
        this.drawingPanel.show(false);
        this.trigger('app:space:express:console:show3D');
    },
     leasePanel_onHighlightByDateResiliationAction : function()
    {
        //this.handleSelectDataSource();
        var highlightDataSourceId= document.getElementById('selector_hilite').value ;
          
        var dsSelector = this.drawingControl.drawingController.getAddOn('DatasourceSelector');
        var combo = document.getElementById('selector_hilite');
        dsSelector.onSelectedDatasourceChanged(dsSelector, combo);

         this.addDragDropEvent();

    }   ,

    billingPanel_onSetYear : function()
    {
        //this.handleSelectDataSource();
        var highlightDataSourceId= document.getElementById('selector_labels').value ;
          
        var dsSelector = this.drawingControl.drawingController.getAddOn('DatasourceSelector');
        var combo = document.getElementById('selector_labels');
        dsSelector.onSelectedDatasourceChanged(dsSelector, combo);

    }  
});

/**
 * Set legend text according the legend level value.
 * @param {Object} row
 * @param {Object} column
 * @param {Object} cellElement
 */
function setLegendLabel(row, column, cellElement) {
    var value = row[column.id];
    if (column.id == 'legend.value' && value != '') {
        var text = value;
        switch (value) {
            case '0':
                text = getMessage('legendLevel0');
                break;
            case '1':
                text = getMessage('legendLevel1');
                break;
            case '2':
                text = getMessage('legendLevel2');
                break;
            case '3':
                text = getMessage('legendLevel3');
                break;
            case '4':
                text = getMessage('legendLevel4');
                break;
        }
        var contentElement = cellElement.childNodes[0];
        if (contentElement)
         contentElement.nodeValue = text;
    }
}


