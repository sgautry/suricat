/**
 * pop up map view 
 */
var popUpMapController = View.createController('popUpMapController', {
	
	openerView: null,
	
    /**
     * Get the object of super view, initiate map configuration and load map.
     */
	afterInitialDataFetch: function() {
		
		this.openerView = View.getOpenerView().controllers.get('spaceExpressConsoleLocations');
		
        this.initialMapConfig();
        
        this.loadBuildingMap();
		
        
    },
    	 
    /**
     *  Initiate map configuration.
     */
    initialMapConfig: function(){
    	
    	var configObject = new Ab.view.ConfigObject();
    	configObject.mapImplementation = 'Esri';
		this.mapControl = new Ab.leaflet.Map('mapPanel', 'mapDiv', configObject);
		
    	var dataSource = 'building_map_ds';
		var keyFields = ['bl.bl_id'];
		var geometryFields = ['bl.lon', 'bl.lat'];
		var titleField = 'bl.name';
		var contentFields = ['bl.bl_id', 'bl.name', 'bl.area_rm', 'bl.count_max_occup', 'bl.count_occup'];
		
		var markerProperties = {
			// optional
			renderer: 'simple',
			radius: 7,
			fillColor: '#e41a1c',
			fillOpacity: 0.90,
			stroke: true,
			strokeColor: '#fff',
			strokeWeight: 1.0,
			markerActionTitle: 'Select',
			markerActionCallback: popUpMapController.selectBuildingAndClose 
		};	

		this.mapControl.createMarkers(
			dataSource,
			keyFields,
			geometryFields,
			titleField,
			contentFields,
			markerProperties
		);
		
    },

    /**
     * Filter buildings and show on map.
     */
    mapFilterOptions_onFilterBuildings: function() {
		
		var siteId = this.mapFilterOptions.getFieldValue("site.site_id");
		var blId = this.mapFilterOptions.getFieldValue("bl.bl_id");
		
		this.loadBuildingMap(siteId, blId);
    },
    
    /**
     *   Load buildings on map with restrictions.
     */
    loadBuildingMap: function(siteId, blId){
    	
    	var restriction = new Ab.view.Restriction();
		var blDs = View.dataSources.get("building_map_ds");  
		
    	if(valueExistsNotEmpty(blId)){
    		//site and bl or only bl
    		restriction.addClause('bl.bl_id', blId, "=");
    		
    	}else if(valueExistsNotEmpty(siteId)&&!valueExistsNotEmpty(blId)){
    		//only site
    		restriction.addClause('bl.site_id', siteId, "=");
    	}else{
    		//neither site nor building
    		if(valueExistsNotEmpty(View.parameters.buildings)){
    			//meet the filter criteria if building field populated on Locations view
    			var buildings = View.parameters.buildings.split(', ‌');
    			for (var i = 0; i < buildings.length; i++) {
    				restriction.addClause('bl.bl_id', trim(buildings[i]), "=", "OR");
    			}
    			
    		}else{
    			//load all buildings
    			var blRecords = blDs.getRecords();
        		if (blRecords.length !== 0) {	
        			// create a restriction based on filter results
        			for (var i = 0; i < blRecords.length; i++) {
        				restriction.addClause('bl.bl_id', blRecords[i].getValue('bl.bl_id'), "=", "OR");
        			}
        		}
    		}
    			
    		
    	}
    		
		// show the markers on the map
		this.mapControl.showMarkers('building_map_ds', restriction);
    },

    /**
     *   Clear the fields of filter.
     */
    mapFilterOptions_onClearFields: function() {
    	this.mapFilterOptions.setFieldValue("site.site_id","");
    	this.mapFilterOptions.setFieldValue("bl.bl_id","");
		
    },
    
    /**
     *   Click on building's tooltip and close the pop up view.
     */
    selectBuildingAndClose: function(key) {
    	  
        var blId = key;
        var blList = [blId];
        popUpMapController.openerView.filterLocationList(blList);
        View.closeThisDialog();
      }
});

