/**
 * @author Guo
 */
var filterBlId;
var filterDvId;
var filterDpId;

var controller = View.createController('hlRmByDpController', {

	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
    //----------------event handle--------------------
    afterViewLoad: function(){
        this.abSpHlRmByDpPerFl_SumGrid.buildPostFooterRows = addTotalRowForSummaryPanel;
        this.abSpHlRmByDpPerFl_BlTree.createRestrictionForLevel = createResForTreeLevel3ByDwgname;
        
        //initialize svg control
		this.parameters = new Ab.view.ConfigObject();
		this.parameters['highlightParameters'] = [{'view_file':"ab-sp-hl-rm-by-dp-per-fl.axvw", 'hs_ds': "ds_ab-sp-hl-rm-by-dp-per-fl_drawing_rmHighlight", 'label_ds':'ds_ab-sp-hl-rm-by-dp-per-fl_drawing_rmLabel'}];
		this.parameters['bordersHighlightSelector'] = 'true'; 
		this.parameters['highlightFilterSelector'] = 'true';
		this.parameters['allowMultipleDrawings'] = 'false';
		this.parameters['divId'] = "svgDiv";
		this.parameters['orderByColumn'] = true;
		this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
		this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'DatasourceSelector': {panelId: "abSpHlRmByDpPerFl_DrawingPanel"},
				'AssetLocator': {divId: "svgDiv"},
				'AssetTooltip': {handlers: [{assetType: 'rm', datasource: 'ds_ab-sp-hl-rm-by-dp-per-fl_drawing_rmLabel', fields: 'rm.rm_id;rm.dv_id;rm.dp_id;rm.area'}]},
				'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
		this.parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpHlRmByDpPerFl_DrawingPanel", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));

    },

    abSpHlRmByDpPerFl_SumGrid_afterRefresh: function(){
        //set color for every row according the drawing
        resetColorFieldValue('abSpHlRmByDpPerFl_SumGrid', 'abSpHlRmByDpPerFl_DrawingPanel', 'dp.dp_id', 'dp.hpattern_acad', 'abSpHlRmByDpPerFl_SumGrid_legend');
    },
    
    abSpHlRmByDpPerFl_filterConsole_onShowTree: function(){
        filterBlId = this.abSpHlRmByDpPerFl_filterConsole.getFieldValue('rm.bl_id');
        filterDvId = this.abSpHlRmByDpPerFl_filterConsole.getFieldValue('rm.dv_id');
        filterDpId = this.abSpHlRmByDpPerFl_filterConsole.getFieldValue('rm.dp_id');
        
        if (filterBlId) {
            this.abSpHlRmByDpPerFl_BlTree.addParameter('blId', " = " + "'" + filterBlId + "'");
        }
        else {
            this.abSpHlRmByDpPerFl_BlTree.addParameter('blId', "IS NOT NULL");
        }
        
        if (filterDvId) {
            this.abSpHlRmByDpPerFl_BlTree.addParameter('dvId', "rm.dv_id = " + "'" + filterDvId + "' AND ");
        }
        else {
            this.abSpHlRmByDpPerFl_BlTree.addParameter('dvId', "");
        }
        
        if (filterDpId) {
            this.abSpHlRmByDpPerFl_BlTree.addParameter('dpId', " = " + "'" + filterDpId + "'");
        }
        else {
            this.abSpHlRmByDpPerFl_BlTree.addParameter('dpId', "IS NOT NULL");
        }
        
        this.abSpHlRmByDpPerFl_BlTree.refresh();
        this.unloadSvg();
        this.closeDetailPanel();
        this.abSpHlRmByDpPerFl_DrawingPanel.lastLoadedBldgFloor = null;
        this.abSpHlRmByDpPerFl_SumGrid.clear();
        this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
    },
    
	loadSvg: function(bl_id, fl_id, drawingName) {
		
		addParameterConfig(this.parameters);
		
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgName = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
        
		//svgControl object used in ab-sp-common.js
		svgControl = this.svgControl;
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
   	},
   	
   	unloadSvg: function(){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
    	this.svgControl.unload(parameters);

   	},
   	
    onClickAsset: function(params, drawingController){
    	var arrayRoomIDs = params['assetId'].split(";");
    	var restriction = new Ab.view.Restriction();
        restriction.addClause("rm.bl_id", arrayRoomIDs[0], "=", true);
        restriction.addClause("rm.fl_id", arrayRoomIDs[1], "=", true);
        restriction.addClause("rm.rm_id", arrayRoomIDs[2], "=", true);
         
        var rmDetailPanel = View.panels.get('abSpHlRmByDpPerFl_RmDetailPanel');
        rmDetailPanel.refresh(restriction);
        rmDetailPanel.show(true);
        rmDetailPanel.showInWindow({
        	width: 500,
        	height: 250
        });
         
        // toggle the room if clicked
    	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId']);
    },
    
    closeDetailPanel: function(){
        var rmDetailPanel = View.panels.get('abSpHlRmByDpPerFl_RmDetailPanel');
        rmDetailPanel.closeWindow();
    }
});

function generateReport(){
    var filterPanel = View.panels.get("abSpHlRmByDpPerFl_filterConsole");
    var filterBlId = filterPanel.getFieldValue('rm.bl_id');
    var filterDvId = filterPanel.getFieldValue('rm.dv_id');
    var filterDpId = filterPanel.getFieldValue('rm.dp_id');
    var parameter = " 1=1 ";
	var restriction1 = new Ab.view.Restriction(); 
    if (filterBlId) {
        parameter += " and rm.bl_id='" + filterBlId + "'";
		restriction1.addClause('rm.bl_id', filterBlId, '=');
    }
    if (filterDvId) {
        parameter += " and rm.dv_id='" + filterDvId + "'";
		restriction1.addClause('rm.dv_id', filterDvId, '=');
    }
    if (filterDpId) {
        parameter += " and rm.dp_id='" + filterDpId + "'";
		restriction1.addClause('rm.dp_id', filterDpId, '=');
    }
   // View.openDialog("ab-paginated-report-job.axvw?viewName=ab-sp-hl-rm-by-dp-per-fl-prnt.axvw" + restriction)
	var passedRestrictions = {'ds_ab-sp-hl-rm-by-dp-per-fl-prnt_drawing_rmHighlight': restriction1, 'ds_ab-sp-hl-rm-by-dp-per-fl-prnt_grid_rm':restriction1};
	View.openPaginatedReportDialog("ab-sp-hl-rm-by-dp-per-fl-prnt.axvw", passedRestrictions, {'dvdpLegend':parameter});
    
}

/**
 * event handler when click the floor level of the tree
 */
function onFlTreeClick(ob){
    var drawingPanel = View.panels.get('abSpHlRmByDpPerFl_DrawingPanel');
    var currentNode = View.panels.get('abSpHlRmByDpPerFl_BlTree').lastNodeClicked;
    var blId = currentNode.parent.data['bl.bl_id'];
    var flId = currentNode.data['fl.fl_id'];
    var highlightResc = "";
    
    if (filterDvId) {
        highlightResc += "rm.dv_id = '" + filterDvId + "'";
    }
    if (filterDpId) {
        if (highlightResc) {
            highlightResc += " AND ";
        }
        highlightResc += "rm.dp_id = '" + filterDpId + "'";
    }
    if (!highlightResc) {
        highlightResc = "rm.dp_id IS NOT NULL";
    }
    View.dataSources.get('ds_ab-sp-hl-rm-by-dp-per-fl_drawing_rmHighlight').addParameter('rmDp', highlightResc);
    
    var title = String.format(getMessage('drawingPanelTitle2'), blId + '-' + flId);
    displayFloor(drawingPanel, currentNode, title);
    
    var summaryRes = new Ab.view.Restriction();
    summaryRes.addClauses(ob.restriction);
    summaryRes.addClause("rm.dwgname", currentNode.data['fl.dwgname'], "=");
    if (filterDvId) {
        summaryRes.addClause("dp.dv_id", filterDvId, "=");
    }
    if (filterDpId) {
        summaryRes.addClause("dp.dp_id", filterDpId, "=");
    }
    
    View.panels.get('abSpHlRmByDpPerFl_SumGrid').refresh(summaryRes);
}

/**
 * event handler when click the rm type level of the tree
 */
function onDpTreeClick(ob){
    var drawingPanel = View.panels.get('abSpHlRmByDpPerFl_DrawingPanel');
    var currentNode = View.panels.get('abSpHlRmByDpPerFl_BlTree').lastNodeClicked;
    
    var dpId = currentNode.data['rm.dp_id'];
    var dvId = currentNode.data['rm.dv_id'];
    var blId = currentNode.parent.parent.data['bl.bl_id'];
    var flId = currentNode.parent.data['fl.fl_id'];
    
    var restriction = new Ab.view.Restriction();
    restriction.addClause("rm.bl_id", blId, "=");
    restriction.addClause("rm.fl_id", flId, "=");
    
    View.dataSources.get('ds_ab-sp-hl-rm-by-dp-per-fl_drawing_rmHighlight').addParameter('rmDp', "rm.dv_id = " + "'" + dvId + "' AND " + " rm.dp_id = " + "'" + dpId + "'");
    
    var title = String.format(getMessage('drawingPanelTitle3'), blId + "-" + flId, dvId + "-" + dpId);
    displayFloor(drawingPanel, currentNode, title);
    
    restriction.addClause("rm.dwgname", currentNode.parent.data['fl.dwgname'], "=");
    restriction.addClause("dp.dv_id", dvId, "=");
    restriction.addClause("dp.dp_id", dpId, "=");
    View.panels.get('abSpHlRmByDpPerFl_SumGrid').refresh(restriction);
}

/**
 * event handler lisenner after create the tree node lable
 */
function afterGeneratingTreeNode(treeNode){
    resetDpTreeNodeLable(treeNode, 2);
}
