/**
 * @author Guo
 */
var filterBlId;
var filterRmCat;

var controller = View.createController('hlRmByCatAndTypeController', {

	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
    //----------------event handle--------------------
    afterViewLoad: function(){
        this.abSpHlRmByRmcatRmtype_TypeSumGrid.buildPostFooterRows = addTotalRowForSummaryPanel;
        this.abSpHlRmByRmcatRmtype_BlTree.createRestrictionForLevel = createRestrictionForLevel;
        this.abSpHlRmByRmcatRmtype_CatSumGrid.show(false);
        this.abSpHlRmByRmcatRmtype_TypeSumGrid.show(true);
        
        //initialize svg control
		this.parameters = new Ab.view.ConfigObject();
		this.parameters['highlightParameters'] = [{'view_file':"ab-sp-hl-rm-by-rmcat-rmtype.axvw", 'hs_ds': "ds_ab-sp-hl-rm-by-rmcat-rmtype_drawing_rmHighlight", 'label_ds':'ds_ab-sp-hl-rm-by-rmcat-rmtype_drawing_rmLabel'}];
		this.parameters['bordersHighlightSelector'] = 'true';
		this.parameters['borderSize'] = 18;
		this.parameters['highlightFilterSelector'] = 'true';
		this.parameters['allowMultipleDrawings'] = 'false';
		this.parameters['divId'] = "svgDiv";
		this.parameters['orderByColumn'] = true;
		this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
		this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'DatasourceSelector': {panelId: "abSpHlRmByRmcatRmtype_DrawingPanel"},
				'AssetLocator': {divId: "svgDiv"},
				'AssetTooltip': {handlers: [{assetType: 'rm', datasource: 'ds_ab-sp-hl-rm-by-rmcat-rmtype_drawing_rmLabel', fields: 'rm.rm_id;rm.rm_cat;rm.rm_type;rm.area'}]},
				'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
		this.parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpHlRmByRmcatRmtype_DrawingPanel", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
    },
    
    abSpHlRmByRmcatRmtype_TypeSumGrid_afterRefresh: function(){
        resetColorFieldValue('abSpHlRmByRmcatRmtype_TypeSumGrid', 'abSpHlRmByRmcatRmtype_DrawingPanel', 'rmtype.rm_type', 'rmtype.hpattern_acad', 'abSpHlRmByRmcatRmtype_TypeSumGrid_legend');
    },
    
    abSpHlRmByRmcatRmtype_CatSumGrid_afterRefresh: function(){
        resetColorFieldValue('abSpHlRmByRmcatRmtype_CatSumGrid', 'abSpHlRmByRmcatRmtype_DrawingPanel', 'rmcat.rm_cat', 'rmcat.hpattern_acad', 'abSpHlRmByRmcatRmtype_CatSumGrid_legend');
    },
    
    abSpHlRmByRmcatRmtype_filterConsole_onShowTree: function(){
        filterBlId = this.abSpHlRmByRmcatRmtype_filterConsole.getFieldValue('rm.bl_id');
        filterRmCat = this.abSpHlRmByRmcatRmtype_filterConsole.getFieldValue('rm.rm_cat');
        
        if (filterBlId) {
            this.abSpHlRmByRmcatRmtype_BlTree.addParameter('blId', " = " + "'" + filterBlId + "'");
        }
        else {
            this.abSpHlRmByRmcatRmtype_BlTree.addParameter('blId', "IS NOT NULL");
        }
        
        if (filterRmCat) {
            this.abSpHlRmByRmcatRmtype_BlTree.addParameter('rmCat', " = " + "'" + filterRmCat + "'");
        }
        else {
            this.abSpHlRmByRmcatRmtype_BlTree.addParameter('rmCat', "IS NOT NULL");
        }
        this.abSpHlRmByRmcatRmtype_BlTree.refresh();
        this.unloadSvg();
        this.closeDetailPanel();
        this.abSpHlRmByRmcatRmtype_DrawingPanel.lastLoadedBldgFloor = null;
        this.abSpHlRmByRmcatRmtype_TypeSumGrid.clear();
        this.abSpHlRmByRmcatRmtype_CatSumGrid.clear();
        this.abSpHlRmByRmcatRmtype_CatSumGrid.show(false);
        this.abSpHlRmByRmcatRmtype_TypeSumGrid.show(true);
        this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
    },
    
	loadSvg: function(bl_id, fl_id, drawingName) {
		
		addParameterConfig(this.parameters);
		
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgName = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
        
		//svgControl object used in ab-sp-common.js
		svgControl = this.svgControl;
		
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
    	
   	},
   	
   	unloadSvg: function(){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
    	this.svgControl.unload(parameters);
    	
   	},
   	
    onClickAsset: function(params, drawingController){
    	var arrayRoomIDs = params['assetId'].split(";");
    	var restriction = new Ab.view.Restriction();
        restriction.addClause("rm.bl_id", arrayRoomIDs[0], "=", true);
        restriction.addClause("rm.fl_id", arrayRoomIDs[1], "=", true);
        restriction.addClause("rm.rm_id", arrayRoomIDs[2], "=", true);
         
        var rmDetailPanel = View.panels.get('abSpHlRmByRmcatRmtype_RmDetailPanel');
        rmDetailPanel.refresh(restriction);
        rmDetailPanel.show(true);
        rmDetailPanel.showInWindow({
        	width: 500,
        	height: 250
        });
         
        // toggle the room if clicked
    	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId']);
    },
    
    closeDetailPanel: function(){
        var rmDetailPanel = View.panels.get('abSpHlRmByRmcatRmtype_RmDetailPanel');
        rmDetailPanel.closeWindow();
    }
});

/**
 * event handler when click the floor level of the tree
 * @param {Object} ob
 */
function onFlTreeClick(ob){
    var currentNode = View.panels.get('abSpHlRmByRmcatRmtype_BlTree').lastNodeClicked;
    var blId = currentNode.parent.data['bl.bl_id'];
    var flId = currentNode.data['fl.fl_id'];
    
    if (filterRmCat) {
        View.dataSources.get('ds_ab-sp-hl-rm-by-rmcat-rmtype_drawing_rmHighlight').addParameter('rmType', "rm.rm_type IS NOT NULL AND rm.rm_cat = '" + filterRmCat + "'");
    }
    else {
        View.dataSources.get('ds_ab-sp-hl-rm-by-rmcat-rmtype_drawing_rmHighlight').addParameter('rmType', "rm.rm_type IS NOT NULL");
    }
    
    var restriction = new Ab.view.Restriction();
    restriction.addClause("rm.bl_id", blId, "=");
    restriction.addClause("rm.fl_id", flId, "=");
    restriction.addClause("rm.dwgname", currentNode.data['fl.dwgname'], "=");
    var drawingPanel = View.panels.get('abSpHlRmByRmcatRmtype_DrawingPanel');
    var title = String.format(getMessage('drawingPanelTitle2'), blId + "-" + flId);
    drawingPanel.currentHighlightDS = 'ds_ab-sp-hl-rm-by-rmcat-rmtype_drawing_rmHighlight';
    controller.parameters['highlightParameters'][0].hs_ds="ds_ab-sp-hl-rm-by-rmcat-rmtype_drawing_rmHighlight";
    displayFloor(drawingPanel, currentNode, title);
    
    if (filterRmCat) {
        restriction.addClause('rm.rm_cat', filterRmCat, '=', 'AND', true);
    }
    
    View.panels.get('abSpHlRmByRmcatRmtype_CatSumGrid').show(false);
    var typeSumGrid = View.panels.get('abSpHlRmByRmcatRmtype_TypeSumGrid');
    typeSumGrid.show(true);
    typeSumGrid.refresh(restriction);
}

/**
 * event handler when click the rm category level of the tree
 * @param {Object} ob
 */
function onRmCatTreeClick(ob){
    var drawingPanel = View.panels.get('abSpHlRmByRmcatRmtype_DrawingPanel');
    var currentNode = View.panels.get('abSpHlRmByRmcatRmtype_BlTree').lastNodeClicked;
    
    var rmCat = currentNode.data['rm.rm_cat'];
    var blId = currentNode.parent.parent.data['bl.bl_id'];
    var flId = currentNode.parent.data['fl.fl_id'];
    
    var restriction = new Ab.view.Restriction();
    restriction.addClause("rm.bl_id", blId, "=");
    restriction.addClause("rm.fl_id", flId, "=");
    restriction.addClause("rm.dwgname", currentNode.parent.data['fl.dwgname'], "=");
    View.dataSources.get('ds_ab-sp-hl-rm-by-rmcat-rmtype_drawing_rmHighlight1').addParameter('rmCat', "rmcat.rm_cat = '" + rmCat + "'");
    var drawingPanel = View.panels.get('abSpHlRmByRmcatRmtype_DrawingPanel');
    var title = String.format(getMessage('drawingPanelTitle3'), blId + "-" + flId, rmCat);
    drawingPanel.currentHighlightDS = 'ds_ab-sp-hl-rm-by-rmcat-rmtype_drawing_rmHighlight1';
    controller.parameters['highlightParameters'][0].hs_ds="ds_ab-sp-hl-rm-by-rmcat-rmtype_drawing_rmHighlight1";
    displayFloor(drawingPanel, currentNode, title);
    
    restriction.addClause("rmtype.rm_cat", rmCat, "=");
    View.panels.get('abSpHlRmByRmcatRmtype_TypeSumGrid').show(false);
    var catSumGrid = View.panels.get('abSpHlRmByRmcatRmtype_CatSumGrid');
    catSumGrid.show(true);
    catSumGrid.refresh(restriction);
}

/**
 * event handler when click the rm type level of the tree
 * @param {Object} ob
 */
function onRmTypeTreeClick(ob){
    var drawingPanel = View.panels.get('abSpHlRmByRmcatRmtype_DrawingPanel');
    var currentNode = View.panels.get('abSpHlRmByRmcatRmtype_BlTree').lastNodeClicked;
    
    var rmType = currentNode.data['rm.rm_type'];
    var blId = currentNode.parent.parent.parent.data['bl.bl_id'];
    var flId = currentNode.parent.parent.data['fl.fl_id'];
    var rmCat = currentNode.parent.data['rm.rm_cat'];
    var restriction = new Ab.view.Restriction();
    restriction.addClause("rm.bl_id", blId, "=");
    restriction.addClause("rm.fl_id", flId, "=");
    restriction.addClause("rm.dwgname", currentNode.parent.parent.data['fl.dwgname'], "=");
    View.dataSources.get('ds_ab-sp-hl-rm-by-rmcat-rmtype_drawing_rmHighlight').addParameter('rmType', "rm.rm_cat = " + "'" + rmCat + "'" + " AND rm.rm_type = " + "'" + rmType + "'");
    var drawingPanel = View.panels.get('abSpHlRmByRmcatRmtype_DrawingPanel');
    var title = String.format(getMessage('drawingPanelTitle4'), blId + '-' + flId, rmCat, rmType);
    drawingPanel.currentHighlightDS = 'ds_ab-sp-hl-rm-by-rmcat-rmtype_drawing_rmHighlight';
    controller.parameters['highlightParameters'][0].hs_ds="ds_ab-sp-hl-rm-by-rmcat-rmtype_drawing_rmHighlight";
    displayFloor(drawingPanel, currentNode, title);
    
    restriction.addClause("rmtype.rm_type", rmType, "=");
    restriction.addClause("rmtype.rm_cat", rmCat, "=");
    
    View.panels.get('abSpHlRmByRmcatRmtype_CatSumGrid').show(false);
    var typeSumGrid = View.panels.get('abSpHlRmByRmcatRmtype_TypeSumGrid');
    typeSumGrid.show(true);
    typeSumGrid.refresh(restriction);
}

function createRestrictionForLevel(parentNode, level){
    var restriction = null;
    if (level == 2) {
        restriction = new Ab.view.Restriction();
        restriction.addClause('rm.dwgname', parentNode.data['fl.dwgname'], '=', 'AND', true);
        restriction.addClause('rm.bl_id', parentNode.getPrimaryKeyValue('fl.bl_id'), '=', 'AND', true);
        restriction.addClause('rm.fl_id', parentNode.getPrimaryKeyValue('fl.fl_id'), '=', 'AND', true);
    }
    
    if (level == 3) {
        restriction = new Ab.view.Restriction();
        restriction.addClause('rm.rm_cat', parentNode.data['rm.rm_cat'], '=', 'AND', true);
        restriction.addClause('rm.dwgname', parentNode.parent.data['fl.dwgname'], '=', 'AND', true);
        restriction.addClause('rm.bl_id', parentNode.parent.getPrimaryKeyValue('fl.bl_id'), '=', 'AND', true);
        restriction.addClause('rm.fl_id', parentNode.parent.getPrimaryKeyValue('fl.fl_id'), '=', 'AND', true);
    }
    
    return restriction;
}
