var controller = View.createController('abSpAsgnRmstdToRm_Control', {
    initialized: false,
    
	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
    afterViewLoad: function(){
        
        this.abSpAsgnRmstdToRm_rmstdAssignGrid.sortEnabled = false;
		//fix KB3025721 by Guo Jiangtao 2010-02-08
		refreshLegendGrid.defer(200);
		
        //initialize svg control
		this.parameters = new Ab.view.ConfigObject();
		this.parameters['highlightParameters'] = [{'view_file':"ab-sp-asgn-rmstd-to-rm.axvw", 'hs_ds': "ds_ab-sp-assgn-rmstd-to-rm_drawing_rmHighlight", 'label_ds':'ds_ab-sp-assgn-rmstd-to-rm_drawing_rmLabel'}];
		this.parameters['bordersHighlightSelector'] = 'true'; 
		this.parameters['highlightFilterSelector'] = 'true';
		this.parameters['allowMultipleDrawings'] = 'false';
		this.parameters['multipleSelectionEnabled'] = 'true';
		this.parameters['divId'] = "svgDiv";
		this.parameters['orderByColumn'] = true;
		this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
		this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'DatasourceSelector': {panelId: "abSpAsgnRmstdToRm_drawingPanel"},
				'AssetLocator': {divId: "svgDiv"},
				'AssetTooltip': {handlers: [{assetType: 'rm', datasource: 'ds_ab-sp-assgn-rmstd-to-rm_drawing_rmLabel', fields: 'rm.rm_id;rm.rm_std'}]},
				'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
		this.parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpAsgnRmstdToRm_drawingPanel", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('selectFloor'));
    },
    
    abSpAsgnRmstdToRm_rmstdGrid_afterRefresh: function(){
       
        // Set the default colors to use based on the ones in the grid
        // This is done so that the drawing control uses the same colors
        var rows = this.abSpAsgnRmstdToRm_rmstdGrid.rows;
        var opacity = this.svgControl.drawingController.config.defaultConfig.highlights.assigned.fill.opacity;
        this.abSpAsgnRmstdToRm_rmstdGrid.setColorOpacity(opacity);
        
        for (var i = 0; i < rows.length; i++) {
            var val = rows[i]['rmstd.rm_std'];
            var color = '';
            var hpval = rows[i]['rmstd.hpattern_acad'];
            if (hpval.length) 
                color = gAcadColorMgr.getRGBFromPattern(hpval, true);
            else {
                color = gAcadColorMgr.getColorFromValue('rmstd.rm_std', val, true);
                //rows[i].row.dom.bgColor
				//fix KB3026955 by Guo Jiangtao 2010-04-19
                var cellEl = Ext.get(rows[i].row.cells.get('abSpAsgnRmstdToRm_rmstdLegend').dom.firstChild);
                cellEl.setStyle('background-color', color);
                cellEl.setOpacity(opacity);
            }
            gAcadColorMgr.setColor('rmstd.rm_std', val, color);
        }
        
        if (!this.initialized) {
            this.initialized = true;
            this.abSpAsgnRmstdToRm_rmstdGrid.update();
        }
    },
    
    abSpAsgnRmstdToRm_filterConsole_onShowTree: function(){
        var filterBlId = this.abSpAsgnRmstdToRm_filterConsole.getFieldValue('rm.bl_id');
        var filterRmStd = this.abSpAsgnRmstdToRm_filterConsole.getFieldValue('rm.rm_std');
        var blTreeRes = new Ab.view.Restriction();
        var rmStdTreeRes = new Ab.view.Restriction();
        
        if (filterBlId) {
            blTreeRes.addClause("bl.bl_id", filterBlId, "=");
        }
        
        if (filterRmStd) {
            rmStdTreeRes.addClause("rmstd.rm_std", filterRmStd, "=");
        }
        this.unloadSvg();
        this.abSpAsgnRmstdToRm_drawingPanel.isLoadedDrawing = false;
        this.svgControl.getAddOn('InfoWindow').setText(getMessage('selectFloor'));
        
        this.abSpAsgnRmstdToRm_rmstdAssignGrid.removeRows(0);
        this.abSpAsgnRmstdToRm_rmstdAssignGrid.update();
        
        this.abSpAsgnRmstdToRm_blTree.refresh(blTreeRes);
        this.abSpAsgnRmstdToRm_rmstdGrid.refresh(rmStdTreeRes);
        
        rmStdId = hpattern_acad = "";
    },
    
	loadSvg: function(bl_id, fl_id, drawingName) {
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgname = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgname;
        
		//svgControl object used in ab-sp-common.js
		svgControl = this.svgControl;
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
    	
   	},
   	
   	unloadSvg: function(){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgname;
    	this.svgControl.unload(parameters);

   	},
   	
    onClickAsset: function(params, drawingController){
    
    	if(!hpattern_acad||!rmStdId)
    		return;
    	
    	var arrayRoomIDs = params['assetId'].split(";");
    	var selected = isRoomSelected(params, drawingController);
    	
    	var grid = View.panels.get("abSpAsgnRmstdToRm_rmstdAssignGrid");
        drawingRoomClickHandler(arrayRoomIDs, selected, grid, 'rm.rm_std', rmStdId)
        controller.svgControl.getAddOn('InfoWindow').setText(getMessage('selectRm').replace("{0}", rmStdId));
    	
    	drawingController.getController("SelectController").setMultipleSelection(true);
    	if(selected)
    		drawingController.getController("SelectController").highlightSelectedAssets(params['svgId'], [params['assetId']], assignColor);
    	else
		{
    		drawingController.getController("HighlightController").clearAsset(params['assetId'], {'svgId': params['svgId'],  'persistFill': false, 'overwriteFill' : true});
    		var selectedAssets = drawingController.getController("SelectController").selectedAssets[params.svgId];
    		for(var i=0;i<selectedAssets.length;i++)
    		{
    			if(selectedAssets[i] == params.assetId){
    				selectedAssets.splice(i, 1);
    			}
    		}
		}
    
    },
    
    reloadSvg: function(){
    	this.svgControl.drawingController.getControl().reload(this.parameters);
    },
    
    closeDetailPanel: function(){

    },
});

var rmStdId;
var assignColor;
var hpattern_acad;

/**
 * check the room is selected or not
 */
function isRoomSelected(params, drawingController){
	var selectedAssets = drawingController.getController("SelectController").selectedAssets[params.svgId];
	if(valueExistsNotEmpty(selectedAssets)){
		for(var i=0;i<selectedAssets.length;i++)
		if(selectedAssets[i] == params.assetId)
		{
			return false;
		}
	}
	return true;
}

/**
 * event handler when click row of the grid abSpAsgnRmstdToRm_rmstdGrid.
 * @param {Object} row
 */
function onRmStdSelected(row){
    var drawingPanel = View.panels.get('abSpAsgnRmstdToRm_drawingPanel');
    rmStdId = row['rmstd.rm_std'];
    hpattern_acad = row['rmstd.hpattern_acad'];
    if (drawingPanel.isLoadedDrawing) {
    	assignColor = getRgbColorByPattern(hpattern_acad);
        controller.svgControl.getAddOn('InfoWindow').setText(getMessage('selectRm').replace("{0}", rmStdId));
    }
    else {
        View.showMessage(getMessage('noFloorSelected'));
    }
}

/**
 * event handler when click tree node of floor level for the tree abSpAsgnRmstdToRm_blTree.
 * @param {Object} ob
 */
function onFlTreeClick(ob){
	var currentNode = View.panels.get('abSpAsgnRmstdToRm_blTree').lastNodeClicked;
    var drawingPanel = View.panels.get('abSpAsgnRmstdToRm_drawingPanel');
    var grid = View.panels.get('abSpAsgnRmstdToRm_rmstdAssignGrid');
    flTreeClickHandler(currentNode, drawingPanel, grid);
    drawingPanel.isLoadedDrawing = true;
    this.svgControl.getAddOn('InfoWindow').setText(getMessage('selectStd'));
}

/**
 * event handler when click rooms of the drawing panel.
 * @param {Object} pk
 * @param {boolean} selected
 */
function onDrawingRoomClicked(pk, selected){
    var grid = View.panels.get("abSpAsgnRmstdToRm_rmstdAssignGrid");
    drawingRoomClickHandler(pk, selected, grid, 'rm.rm_std', rmStdId)
    this.svgControl.getAddOn('InfoWindow').setText(getMessage('selectRm').replace("{0}", rmStdId));
}

/**
 * event handler when click button 'revert all'.
 */
function resetAssignmentCtrls(){
    var drawingPanel = View.panels.get('abSpAsgnRmstdToRm_drawingPanel');
    var grid = View.panels.get("abSpAsgnRmstdToRm_rmstdAssignGrid");
    resetAssignment(drawingPanel, grid);
    this.svgControl.getAddOn('InfoWindow').setText(getMessage('selectStd'));
    
    rmStdId = hpattern_acad = "";
}

/**
 * event handler when click button 'save'.
 */
function saveAllChanges(){
    var dsChanges = View.dataSources.get("ds_ab-sp-assgn-rmstd-to-rm_drawing_rmHighlight");
    var drawingPanel = View.panels.get('abSpAsgnRmstdToRm_drawingPanel');
    var grid = View.panels.get("abSpAsgnRmstdToRm_rmstdAssignGrid");
    saveChange(drawingPanel, grid, dsChanges, ['rm.rm_std'], true);
    this.svgControl.getAddOn('InfoWindow').setText(getMessage('selectStd'));
}

/**
 * refresh legend grid.
 */
function refreshLegendGrid(){
    var grid = View.panels.get("abSpAsgnRmstdToRm_rmstdGrid");
	grid.refresh();
}
