/**
 * @author ZY
 */

//Guo added 2009-07-23 to fix KB3023605
var abSpDefRmStd_Control = View.createController('abSpDefRmStd_Control', {

    panelId: "",
    fieldId: "",
	
	detailsPanel_afterRefresh: function(){
        this.detailsPanel.enableField("rmstd.hpattern_acad", false);
		this.detailsPanel.enableFieldActions("rmstd.hpattern_acad", true);
		},
		
		/**
		 * Calculate std_area be	fore we save record.
		 */
		detailsPanel_beforeSave:function(){
		var form=this.detailsPanel;
	   if(form){
			var width=form.getFieldValue("rmstd.width");
			var length=form.getFieldValue("rmstd.length");
			var area=width*length;
			area = Math.round(area*100)/100;
			// kb#3052387: convert area's decimal seperator manually 
			if ( strDecimalSeparator != sNeutralDecimalSeparator ) {
				area = area.toString().replace(sNeutralDecimalSeparator, strDecimalSeparator);
			}
			form.setFieldValue("rmstd.std_area",area);
		}    
    },

    openDialogBox: function(panelName,fieldName){
    panelId = panelName;
    fieldId = fieldName;
    View.hpatternPanel = View.panels.get(panelName);
    View.hpatternField = fieldName;
    View.patternString = View.hpatternPanel.getFieldValue(fieldName);
    var thisController = this;
    var dialog = View.openDialog('ab-hpattern-dialog.axvw', null, false, {
            closeButton: false,
            width: 700,
            height: 530,
        
            // this callback function will be called after the dialog view is loaded
            afterViewLoad: function(dialogView) {
                // access the dialog controller property
                var dialogController = dialogView.controllers.get('setHighlightPattern_Controller');
    
                // set the dialog controller onClose callback                
                dialogController.onClose = thisController.dialog_onClose.createDelegate(thisController);
            },
    });
    
    },
  
    dialog_onClose: function(dialogController){
      showColors(panelId,fieldId);
    }

});

/**
 * event handler when click the select value button for the field rmstd.hpattern_acad
 */
function selectHpattern(){
    var currentView =  View.controllers.get('abSpDefRmStd_Control');
    currentView.openDialogBox('detailsPanel','rmstd.hpattern_acad');
}



