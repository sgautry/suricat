/**
 * @author Guo
 */

var controller = View.createController('abSpHlRmByDpPerFlTransTransController', {

	blConsoleRes : '1=1',
	rmpctConsoleRes : '1=1',
	date : '',
	dateToAddField:'rmpct.date_start',
	dateToUpdateField:'rmpct.date_start',
	console:null,
	timeLine:null,

	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgName:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
	// ----------------event handle--------------------
	afterViewLoad : function() {
		
		//initialize svg control
		this.parameters = new Ab.view.ConfigObject();
		this.parameters['highlightParameters'] = [{'view_file':"ab-sp-hl-rm-by-dp-per-fl-trans.axvw", 'hs_ds': "abSpShareDSForHlRmByDpPerFl_rmHighlightDS", 'label_ds':'abSpShareDSForHlRmByDpPerFl_rmLabelDS'}];
		this.parameters['bordersHighlightSelector'] = 'true'; 
		this.parameters['highlightFilterSelector'] = 'true';
		this.parameters['allowMultipleDrawings'] = 'false';
		this.parameters['divId'] = "svgDiv";
		this.parameters['orderByColumn'] = true;
		this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
		this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'DatasourceSelector': {panelId: "abSpHlRmByDpPerFlTrans_DrawingPanel"},
				'AssetLocator': {divId: "svgDiv"},
				'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
		this.parameters['events'] = [{'eventName': 'mouseover', 'assetType' : 'rm', 'handler' : this.onMouseOverAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpHlRmByDpPerFlTrans_DrawingPanel", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle'));
		
	},
	    

	afterInitialDataFetch : function() {
		this.date = getCurrentDate();
		this.abSpHlRmByDpPerFlTrans_console.setFieldValue('rmpct.date_start', this.date);
		this.console=this.abSpHlRmByDpPerFlTrans_console;
		this.timeLine=this.timeLineButton;
		this.dateAction = this.timeLine.actions.get('currentDate');
		this.dateAction.setTitle(this.console.getFieldElement(this.dateToAddField).value);
		this.date=this.abSpHlRmByDpPerFlTrans_console.getFieldValue('rmpct.date_start');
		this.resetDataViewBorder();
		setTimeTitle(this.timeLine);
	},

	resetDataViewBorder : function() {
		var templatehtml = this.abSpHlRmByDpPerFlTrans_rmAttribute.dataView.levels[0].bodyXTemplate.html;
		var lastTrIndex = templatehtml.lastIndexOf('<TR>');
		if (lastTrIndex == -1) {
			lastTrIndex = templatehtml.lastIndexOf('<tr>');
		}
		var newTemplatehtml = (templatehtml.substring(0, lastTrIndex) + "<TR class=\"last\">" + templatehtml.substring(lastTrIndex + 4)).replace(/first/g, 'columnReportLabel').replace(/fill/g, 'columnReportValue');
		this.abSpHlRmByDpPerFlTrans_rmAttribute.dataView.levels[0].bodyXTemplate = new Ext.XTemplate(newTemplatehtml);
	},

	abSpHlRmByDpPerFlTrans_rmSummaryByDp_afterRefresh : function() {
		resetColorFieldValue('abSpHlRmByDpPerFlTrans_rmSummaryByDp', 'abSpHlRmByDpPerFlTrans_DrawingPanel', 'dp.dp_id', 'dp.hpattern_acad', 'abSpHlRmByDpPerFlTrans_rmSummaryByDp_legend');
	},

	abSpHlRmByDpPerFlTrans_console_onShowTree : function() {
		var console = this.abSpHlRmByDpPerFlTrans_console;
		var blId = console.getFieldValue('rmpct.bl_id');
		var dvId = console.getFieldValue('rmpct.dv_id');
		var dpId = console.getFieldValue('rmpct.dp_id');
		this.date = console.getFieldValue('rmpct.date_start');
		this.blConsoleRes = '1=1';
		this.rmpctConsoleRes = '1=1';

		if (blId) {
			this.blConsoleRes = "bl.bl_id =" + literal(blId)
		}
		if (dvId) {
			this.rmpctConsoleRes += " and rmpct.dv_id =" + literal(dvId)
		}
		if (dpId) {
			this.rmpctConsoleRes += " and rmpct.dp_id =" + literal(dpId)
		}

		this.resetHighlights();
		this.abSpHlRmByDpPerFlTrans_flTree.refresh();
		this.dateAction = this.timeLine.actions.get('currentDate');
		this.dateAction.setTitle(this.date);
		this.date=this.console.getFieldValue('rmpct.date_start');

	},

	/**
	 * event handler when click the floor level of the tree
	 * 
	 */
	resetHighlights : function() {
		setParameters();
		if (this.svgControl.drawingController.isDrawingsLoaded) {
			this.reloadSvg();
			showStatisticsGrid();
		}
	},
	
	afterTimeButtonClick : function(){
		this.resetHighlights();
	},	
	
	loadSvg: function(bl_id, fl_id, drawingName) {
		
		addParameterConfig(this.parameters);
		
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgName = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;

		//svgControl object used in ab-sp-common.js
		svgControl = this.svgControl;
		
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
   	},
 
   	reloadSvg: function(){
   		
   		addParameterConfig(this.parameters);
   		this.parameters['drawingName'] = this.dwgName;
   		this.svgControl.drawingController.getControl().reload(this.parameters);	
   	},
   	
   	unloadSvg: function(){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
    	this.svgControl.unload(parameters);

   	},
   	
   	onMouseOverAsset: function(params, drawingController){
    	var arrayRoomIDs = params['assetId'].split(";");
    	var restriction = new Ab.view.Restriction();
        restriction.addClause("rmpct.bl_id", arrayRoomIDs[0], "=", true);
        restriction.addClause("rmpct.fl_id", arrayRoomIDs[1], "=", true);
        restriction.addClause("rmpct.rm_id", arrayRoomIDs[2], "=", true);
		View.panels.get('abSpHlRmByDpPerFlTrans_rmAttribute').refresh(restriction);
    }
	
});

/**
 * event handler when click the floor level of the tree
 * 
 * @param {Object}
 *            ob
 */
function onFlTreeClick(ob) {
	var currentNode = View.panels.get('abSpHlRmByDpPerFlTrans_flTree').lastNodeClicked;
	controller.blId = currentNode.parent.data['bl.bl_id'];
	controller.flId = currentNode.data['fl.fl_id'];

	var drawingPanel = View.panels.get('abSpHlRmByDpPerFlTrans_DrawingPanel');
	setParameters();

	var title = String.format(getMessage('drawingPanelTitle2'), controller.blId + "-" + controller.flId);
	var dwgName = currentNode.data['fl.dwgname'];
	controller.loadSvg(controller.blId, controller.flId, dwgName);
	drawingPanel.isLoadDrawing = true;
	controller.svgControl.getAddOn('InfoWindow').setText(title); 
	showStatisticsGrid();
}

//function onChangeHighlightDS(type) {
//	if (controller.abSpHlRmByDpPerFlTrans_DrawingPanel.isLoadDrawing && type == 'highlight') {
//		showStatisticsGrid();
//	}
//}

function setParameters() {
	controller.date = controller.abSpHlRmByDpPerFlTrans_console.getFieldValue('rmpct.date_start');
	var list = ['abSpShareDSForHlRmByDpPerFl_rmHighlightDS', 'abSpShareDSForHlRmByDpPerFl_rmLabelDS', 'abSpHlRmByDpPerFlTrans_rmSummaryByDp', 'abSpShareDSForRmAttributeDS'];
	for ( var i = 0; i < list.length; i++) {
		var control = View.dataSources.get(list[i]);
		if (!control) {
			control = View.panels.get(list[i]);
		}
		control.addParameter('date', controller.date);
		control.addParameter('rmpctConsoleRes', controller.rmpctConsoleRes);
	}
	var treeRes = controller.blConsoleRes;
	if(controller.rmpctConsoleRes){
		treeRes = treeRes +" and " + controller.rmpctConsoleRes;
	}
	controller.abSpHlRmByDpPerFlTrans_flTree.addParameter('blConsoleRes', treeRes);
}

function showStatisticsGrid() {
	var grid = View.panels.get('abSpHlRmByDpPerFlTrans_rmSummaryByDp');
	grid.addParameter('blId', controller.blId);
	grid.addParameter('flId', controller.flId);
	grid.refresh();
}


function literal(value) {
	return "'" + value + "'"
}

function generateReport(){
    var filterPanel = View.panels.get("abSpHlRmByDpPerFlTrans_console");
    var filterBlId = filterPanel.getFieldValue('rmpct.bl_id');
    var filterDvId = filterPanel.getFieldValue('rmpct.dv_id');
    var filterDpId = filterPanel.getFieldValue('rmpct.dp_id');
    var restriction = "1=1";
    if (filterBlId) {
        restriction += " and rmpct.bl_id='" + filterBlId + "'";
    }
    if (filterDvId) {
        restriction += " and rmpct.dv_id='" + filterDvId + "'";
    }
    if (filterDpId) {
        restriction += " and rmpct.dp_id='" + filterDpId + "'";
    }
    
    View.openPaginatedReportDialog("ab-sp-hl-rm-by-dp-per-fl-trans-prnt.axvw", null, {
        'date': controller.date,
        'rmpctConsoleRes': restriction
    });
}


