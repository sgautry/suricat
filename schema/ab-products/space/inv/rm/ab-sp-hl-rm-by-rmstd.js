/**
 * @author Guo
 */
var controller = View.createController('hlRmByStdController', {

	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
    //----------------event handle--------------------
    afterViewLoad: function(){
        this.abSpHlRmByRmStd_SumGrid.buildPostFooterRows = addTotalRowForSummaryPanel;
        this.abSpHlRmByRmStd_BlTree.createRestrictionForLevel = createResForTreeLevel3ByDwgname;
        
        //initialize svg control
		this.parameters = new Ab.view.ConfigObject();
		this.parameters['highlightParameters'] = [{'view_file':"ab-sp-hl-rm-by-rmstd.axvw", 'hs_ds': "ds_ab-sp-hl-rm-by-rmstd_drawing_rmHighlight", 'label_ds':'ds_ab-sp-hl-rm-by-rmstd_drawing_rmLabel'}];
		this.parameters['bordersHighlightSelector'] = 'true';
		this.parameters['borderSize'] = 18;
		this.parameters['highlightFilterSelector'] = 'true';
		this.parameters['allowMultipleDrawings'] = 'false';
		this.parameters['divId'] = "svgDiv";
		this.parameters['orderByColumn'] = true;
		this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
		this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'DatasourceSelector': {panelId: "abSpHlRmByRmStd_DrawingPanel"},
				'AssetLocator': {divId: "svgDiv"},
				'AssetTooltip': {handlers: [{assetType: 'rm', datasource: 'ds_ab-sp-hl-rm-by-rmstd_drawing_rmLabel', fields: 'rm.rm_id;rm.rm_std;rm.area'}]},
				'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
		this.parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpHlRmByRmStd_DrawingPanel", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
		
    },
       

    abSpHlRmByRmStd_SumGrid_afterRefresh: function(){
        //set color for every row according the drawing
        resetColorFieldValue('abSpHlRmByRmStd_SumGrid', 'abSpHlRmByRmStd_DrawingPanel', 'rmstd.rm_std', 'rmstd.hpattern_acad', 'abSpHlRmByRmStd_SumGrid_legend');
    },
    
    abSpHlRmByRmStd_filterConsole_onShowTree: function(){
        var filterBlId = this.abSpHlRmByRmStd_filterConsole.getFieldValue('rm.bl_id');
        
        if (filterBlId) {
            this.abSpHlRmByRmStd_BlTree.addParameter('blId', " = " + "'" + filterBlId + "'");
        }
        else {
            this.abSpHlRmByRmStd_BlTree.addParameter('blId', "IS NOT NULL");
        }
        
        this.abSpHlRmByRmStd_BlTree.refresh();
        this.unloadSvg();
        this.closeDetailPanel();
        this.abSpHlRmByRmStd_DrawingPanel.lastLoadedBldgFloor = null;
        this.abSpHlRmByRmStd_SumGrid.clear();
        this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
    },
    
	loadSvg: function(bl_id, fl_id, drawingName) {
		
		addParameterConfig(this.parameters);
		
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgName = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
        
		//svgControl object used in ab-sp-common.js
		svgControl = this.svgControl;
		
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
    	
   	},
   	
   	unloadSvg: function(){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
    	this.svgControl.unload(parameters);
    	
   	},
   	
    onClickAsset: function(params, drawingController){
    	var arrayRoomIDs = params['assetId'].split(";");
    	var restriction = new Ab.view.Restriction();
        restriction.addClause("rm.bl_id", arrayRoomIDs[0], "=", true);
        restriction.addClause("rm.fl_id", arrayRoomIDs[1], "=", true);
        restriction.addClause("rm.rm_id", arrayRoomIDs[2], "=", true);
         
        var rmDetailPanel = View.panels.get('abSpHlRmByRmStd_RmDetailPanel');
        rmDetailPanel.refresh(restriction);
        rmDetailPanel.show(true);
        rmDetailPanel.showInWindow({
        	width: 500,
        	height: 250
        });
         
        // toggle the room if clicked
    	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId']);
    },
    
    closeDetailPanel: function(){
        var rmDetailPanel = View.panels.get('abSpHlRmByRmStd_RmDetailPanel');
        rmDetailPanel.closeWindow();
    }
});

/**
 * event handler when click the floor level of the tree
 */
function onFlTreeClick(ob){
    var drawingPanel = View.panels.get('abSpHlRmByRmStd_DrawingPanel');
    var currentNode = View.panels.get('abSpHlRmByRmStd_BlTree').lastNodeClicked;
    var blId = currentNode.parent.data['bl.bl_id'];
    var flId = currentNode.data['fl.fl_id'];
    View.dataSources.get('ds_ab-sp-hl-rm-by-rmstd_drawing_rmHighlight').addParameter('rmStd', "rm.rm_std IS NOT NULL");
    
    var title = String.format(getMessage('drawingPanelTitle2'), blId + "-" + flId);
    displayFloor(drawingPanel, currentNode, title);
    var restriction = new Ab.view.Restriction();
    restriction.addClause("rm.bl_id", blId, "=");
    restriction.addClause("rm.fl_id", flId, "=");
    restriction.addClause("rm.dwgname", currentNode.data['fl.dwgname'], "=");
    View.panels.get('abSpHlRmByRmStd_SumGrid').refresh(restriction);
}

/**
 * event handler when click the rm standard level of the tree
 */
function onRmStdTreeClick(ob){
    var drawingPanel = View.panels.get('abSpHlRmByRmStd_DrawingPanel');
    var currentNode = View.panels.get('abSpHlRmByRmStd_BlTree').lastNodeClicked;
    
    var rmStdId = currentNode.data['rm.rm_std'];
    var blId = currentNode.parent.parent.data['bl.bl_id'];
    var flId = currentNode.parent.data['fl.fl_id'];
    
    var restriction = new Ab.view.Restriction();
    restriction.addClause("rm.bl_id", blId, "=");
    restriction.addClause("rm.fl_id", flId, "=");
    restriction.addClause("rm.dwgname", currentNode.parent.data['fl.dwgname'], "=");
    View.dataSources.get('ds_ab-sp-hl-rm-by-rmstd_drawing_rmHighlight').addParameter('rmStd', "rm.rm_std = " + "'" + rmStdId + "'");
    
    var title = String.format(getMessage('drawingPanelTitle3'), blId + "-" + flId, rmStdId);
    displayFloor(drawingPanel, currentNode, title);
    
    restriction.addClause("rmstd.rm_std", rmStdId, "=");
    View.panels.get('abSpHlRmByRmStd_SumGrid').refresh(restriction);
}
