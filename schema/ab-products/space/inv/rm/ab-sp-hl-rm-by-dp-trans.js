/**
 * @author Guo
 */
var controller = View.createController('aSpHlRmByDpTrans_Controller', {
	dvId : '',
	dpId : '',
	date : '',
	dateToAddField:'rmpct.date_start',
	dateToUpdateField:'rmpct.date_start',
	console:null,
	timeLine:null,

	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,

	// ----------------event handle--------------------
	afterViewLoad : function() {
		this.aSpHlRmByDpTrans_selectFlGrid.addEventListener('onMultipleSelectionChange', function(row) {
			controller.setParameters();
			
            var checked = document.getElementById("aSpHlRmByDpTrans_selectFlGrid_row" + row.index + "_multipleSelectionColumn").checked;
    		
    		if (checked) {
    			// load relevant svg
    			controller.loadSvg(row["rm.bl_id"], row["rm.fl_id"], row["rm.dwgname"]);
    		} else {
    			controller.unloadSvg(row["rm.bl_id"], row["rm.fl_id"], row["rm.dwgname"]);
    		}
    		
		});
		
        //initialize svg control
		this.parameters = new Ab.view.ConfigObject();
		this.parameters['highlightParameters'] = [{'view_file':"ab-sp-hl-rm-by-dp-trans.axvw", 'hs_ds': "abSpShareDSForHlRmByDpPerFl_rmHighlightDS", 'label_ds':'abSpShareDSForHlRmByDpPerFl_rmLabelDS'}];
		this.parameters['bordersHighlightSelector'] = 'true'; 
		this.parameters['highlightFilterSelector'] = 'true';
		this.parameters['allowMultipleDrawings'] = 'true';
		this.parameters['multipleSelectionEnabled'] = 'true';
		this.parameters['divId'] = "svgDiv";
		this.parameters['orderByColumn'] = true;
		this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
		this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'DatasourceSelector': {panelId: "aSpHlRmByDpTrans_floorPlan"},
				'AssetLocator': {divId: "svgDiv"},
				'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
		this.parameters['events'] = [{'eventName': 'mouseover', 'assetType' : 'rm', 'handler' : this.onMouseOverAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "aSpHlRmByDpTrans_floorPlan", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle'));
		
	},

       

	afterInitialDataFetch : function() {
		this.date = getCurrentDate();
		this.aSpHlRmByDpTrans_Console.setFieldValue('rmpct.date_start', this.date);
		//this.setParameters();
		this.aSpHlRmByDpTrans_selectFlGrid.refresh();
		this.aSpHlRmByDpTrans_selectFlGrid.enableSelectAll(false);
		this.resetDataViewBorder();
		this.console=this.aSpHlRmByDpTrans_Console;
		this.timeLine=this.timeLineButton;
		this.dateAction = this.timeLine.actions.get('currentDate');
		this.dateAction.setTitle(this.console.getFieldElement(this.dateToAddField).value);
		this.date=this.console.getFieldValue('rmpct.date_start');
		setTimeTitle(this.timeLine);
	},

	resetDataViewBorder : function() {
		var templatehtml = this.aSpHlRmByDpTrans_rmAttribute.dataView.levels[0].bodyXTemplate.html;
		var lastTrIndex = templatehtml.lastIndexOf('<TR>');
		if (lastTrIndex == -1) {
			lastTrIndex = templatehtml.lastIndexOf('<tr>');
		}
		var newTemplatehtml = (templatehtml.substring(0, lastTrIndex) + "<TR class=\"last\">" + templatehtml.substring(lastTrIndex + 4)).replace(/first/g, 'columnReportLabel').replace(/fill/g, 'columnReportValue');
		this.aSpHlRmByDpTrans_rmAttribute.dataView.levels[0].bodyXTemplate = new Ext.XTemplate(newTemplatehtml);
	},

	/**
	 * onclick event for show action in the console
	 */
	aSpHlRmByDpTrans_Console_onShowDpGrid : function() {
		this.unloadAllSvg();
		this.console=this.aSpHlRmByDpTrans_Console;
		this.timeLine=this.timeLineButton;
		this.dateAction = this.timeLine.actions.get('currentDate');
		this.dateAction.setTitle(this.console.getFieldElement(this.dateToAddField).value);
		this.date=this.console.getFieldValue('rmpct.date_start');
		this.aSpHlRmByDpTrans_selectFlGrid.clear();
		this.setDrawingTitle();

		this.setParameters();
		
		this.aSpHlRmByDpTrans_dvTree.refresh();
		this.aSpHlRmByDpTrans_selectFlGrid.refresh();
	},

	setParameters : function() {
		var rmpctConsoleRes = '1=1';
		var consoleDvId = this.aSpHlRmByDpTrans_Console.getFieldValue('rmpct.dv_id');
		var consoleDpId = this.aSpHlRmByDpTrans_Console.getFieldValue('rmpct.dp_id');
		var consoleBlId = this.aSpHlRmByDpTrans_Console.getFieldValue('rmpct.bl_id');
		this.date = this.aSpHlRmByDpTrans_Console.getFieldValue('rmpct.date_start');
		if (consoleDvId) {
			this.aSpHlRmByDpTrans_dvTree.addParameter('dvRes', "=" + literal(consoleDvId));
			rmpctConsoleRes += " and rmpct.dv_id =" + literal(consoleDvId);
		} else {
			this.aSpHlRmByDpTrans_dvTree.addParameter('dvRes', " IS NOT NULL ");
		}
		if (consoleDpId) {
			this.aSpHlRmByDpTrans_dvTree.addParameter('dpRes', "=" + literal(consoleDpId));
			rmpctConsoleRes += " and rmpct.dp_id =" + literal(consoleDpId);
		} else {
			this.aSpHlRmByDpTrans_dvTree.addParameter('dpRes', " IS NOT NULL ");
		}
		
		
		if (this.dvId) {
			rmpctConsoleRes += " and rmpct.dv_id =" + literal(this.dvId);
		}
		if (this.dpId) {
			rmpctConsoleRes += " and rmpct.dp_id =" + literal(this.dpId);
		}
		if (consoleBlId) {
			rmpctConsoleRes += " and rmpct.bl_id =" + literal(consoleBlId);
		}

		this.aSpHlRmByDpTrans_selectFlGrid.addParameter('date', this.date);
		this.aSpHlRmByDpTrans_selectFlGrid.addParameter('rmpctConsoleRes', rmpctConsoleRes);
		this.abSpShareDSForHlRmByDpPerFl_rmHighlightDS.addParameter('date', this.date);
		this.abSpShareDSForHlRmByDpPerFl_rmHighlightDS.addParameter('rmpctConsoleRes', rmpctConsoleRes);
		this.abSpShareDSForHlRmByDpPerFl_rmLabelDS.addParameter('date', this.date);
		this.abSpShareDSForHlRmByDpPerFl_rmLabelDS.addParameter('rmpctConsoleRes', rmpctConsoleRes);
		this.abSpShareDSForRmAttributeDS.addParameter('date', this.date);
		this.abSpShareDSForRmAttributeDS.addParameter('rmpctConsoleRes', rmpctConsoleRes);
	},

	/**
	 * set drawing panel title
	 */
	setDrawingTitle : function() {
		if (this.dvId || this.dpId) {
			this.svgControl.getAddOn('InfoWindow').setText(String.format(getMessage('drawingPanelTitle1'), this.dvId + "-" + this.dpId));
		} else {
			this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle'));
		}
	},
	
	resetHighlights : function() {
		this.setParameters();
		if (this.svgControl.drawingController.isDrawingsLoaded) {
			this.reloadSvg();
		}
	},
	afterTimeButtonClick : function(){
		this.resetHighlights();
	},
	
	loadSvg: function(bl_id, fl_id, drawingName) {
		
		addParameterConfig(this.parameters);
		
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgName = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
        
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
   	},
  
   	reloadSvg: function(){
   		
   		addParameterConfig(this.parameters);
   		this.parameters['drawingName'] = this.dwgName;
   		this.svgControl.drawingController.getControl().reload(this.parameters);	
   	},
   	
   	unloadSvg: function(bl_id, fl_id, drawingName){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id':fl_id};
        parameters['drawingName'] = drawingName;
    	this.svgControl.unload(parameters);

   	},
   	unloadAllSvg:function(){
   		var rows = this.aSpHlRmByDpTrans_selectFlGrid.getSelectedRows();
   		_.each(rows, function(row){
   			controller.unloadSvg(row['rm.bl_id'], row['rm.fl_id'], row['rm.dwgname']);
   		});
   	},
   	
   	onMouseOverAsset: function(params, drawingController){
    	var arrayRoomIDs = params['assetId'].split(";");
    	var restriction = new Ab.view.Restriction();
        restriction.addClause("rmpct.bl_id", arrayRoomIDs[0], "=", true);
        restriction.addClause("rmpct.fl_id", arrayRoomIDs[1], "=", true);
        restriction.addClause("rmpct.rm_id", arrayRoomIDs[2], "=", true);
		View.panels.get('aSpHlRmByDpTrans_rmAttribute').refresh(restriction);
		
    }
   	

});

/**
 * event handler when click the department level of the tree
 * 
 * @param {Object}
 *            ob
 */
function onDpTreeClick(ob) {
	
	controller.unloadAllSvg();
	
	var currentNode = controller.aSpHlRmByDpTrans_dvTree.lastNodeClicked;
	controller.dvId = currentNode.parent.data['dv.dv_id'];
	controller.dpId = currentNode.data['dp.dp_id'];

	controller.setParameters();

	var grid = controller.aSpHlRmByDpTrans_selectFlGrid;
	grid.refresh();

	var drawingPanel = controller.aSpHlRmByDpTrans_floorPlan;
	
	controller.setDrawingTitle();
	
}


function literal(value) {
	return "'" + value + "'"
}
