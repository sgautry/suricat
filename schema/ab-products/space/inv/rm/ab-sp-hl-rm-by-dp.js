/**
 * @author Guo
 */
var controller = View.createController('hlRmByDpController', {

	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
    //----------------event handle--------------------
    afterViewLoad: function(){
        this.abSpHlRmByDp_DrawingPanel.addEventListener('ondwgload', setDrawingTitle);
        this.abSpHlRmByDp_flGrid.addEventListener('onMultipleSelectionChange', function(row){
            var highlightResc = "1=1";
            if (dvId) {
                highlightResc += " AND rm.dv_id = '" + dvId + "'";
            }
            else {
                highlightResc += " AND rm.dv_id IS NOT NULL";
            }
            
            if (dpId) {
                highlightResc += " AND rm.dp_id = '" + dpId + "'";
            }
            else {
                highlightResc += " AND rm.dp_id IS NOT NULL";
            }
            
            View.dataSources.get('ds_ab-sp-hl-rm-by-dp_drawing_rmHighlight').addParameter('rmDp', highlightResc);
            
            var checked = document.getElementById("abSpHlRmByDp_flGrid_row" + row.index + "_multipleSelectionColumn").checked;
    		
    		if (checked) {
    			// load relevant svg
    			controller.loadSvg(row["rm.bl_id"], row["rm.fl_id"], row["rm.dwgname"]);
    		} else {
    			//remove selected one drawing
    			controller.unloadSvg(row["rm.bl_id"], row["rm.fl_id"], row["rm.dwgname"]);
    			controller.closeDetailPanel();
    		}
        });
        
        //initialize svg control
		this.parameters = new Ab.view.ConfigObject();
		this.parameters['highlightParameters'] = [{'view_file':"ab-sp-hl-rm-by-dp.axvw", 'hs_ds': "ds_ab-sp-hl-rm-by-dp_drawing_rmHighlight", 'label_ds':'ds_ab-sp-hl-rm-by-dp_drawing_rmLabel'}];
		this.parameters['bordersHighlightSelector'] = 'true'; 
		this.parameters['highlightFilterSelector'] = 'true';
		this.parameters['allowMultipleDrawings'] = 'true';
		this.parameters['multipleSelectionEnabled'] = 'true';
		this.parameters['divId'] = "svgDiv";
		this.parameters['orderByColumn'] = true;
		this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
		this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'DatasourceSelector': {panelId: "abSpHlRmByDp_DrawingPanel"},
				'AssetLocator': {divId: "svgDiv"},
				'AssetTooltip': {handlers: [{assetType: 'rm', datasource: 'ds_ab-sp-hl-rm-by-dp_drawing_rmLabel', fields: 'rm.rm_id;rm.dv_id;rm.dp_id;rm.area'}]},
				'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
		this.parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpHlRmByDp_DrawingPanel", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle'));

    },
         
    afterInitialDataFetch: function(){
        this.abSpHlRmByDp_flGrid.enableSelectAll(false);
    },
    
    abSpHlRmByDp_filterConsole_onShowDpGrid: function(){
        dvId = this.abSpHlRmByDp_filterConsole.getFieldValue('rm.dv_id');
        dpId = this.abSpHlRmByDp_filterConsole.getFieldValue('rm.dp_id');
        blId = this.abSpHlRmByDp_filterConsole.getFieldValue('rm.bl_id');
        var dvRes = " IS NOT NULL";
        var dpRes = " IS NOT NULL";
        var blRes = " IS NOT NULL";
        if (dvId) {
            dvRes = " = '" + dvId + "'";
        }
        if (dpId) {
            dpRes = " = '" + dpId + "'";
        }
        if (blId) {
            blRes = " = '" + blId + "'";
        }
        
        //remove all selected drawings
        this.unloadSvg();
        this.closeDetailPanel();
        setDrawingTitle();
        
        this.abSpHlRmByDp_dvTree.addParameter('dvRes', dvRes);
        this.abSpHlRmByDp_dvTree.addParameter('dpRes', dpRes);
        this.abSpHlRmByDp_dvTree.refresh();
        
        this.abSpHlRmByDp_flGrid.addParameter('dvRes', dvRes);
        this.abSpHlRmByDp_flGrid.addParameter('dpRes', dpRes);
        this.abSpHlRmByDp_flGrid.addParameter('blRes', blRes);
        this.abSpHlRmByDp_flGrid.refresh();
    },
    
	loadSvg: function(bl_id, fl_id, drawingName) {
		
		addParameterConfig(this.parameters);
		
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgName = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
        
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
   	},
   	
   	unloadSvg: function(blId, flId, dwgname){
   		if(valueExistsNotEmpty(blId)&&valueExistsNotEmpty(flId)&&valueExistsNotEmpty(flId)){
   			var parameters = {};
   	        parameters['pkeyValues'] = {'bl_id':blId, 'fl_id':flId};
   	        parameters['drawingName'] = dwgname;
   	    	this.svgControl.unload(parameters);
   		}else{
   			var rows = this.abSpHlRmByDp_flGrid.getSelectedGridRows()
   			for (var i=0;i<rows.length;i++){
   				var blId = rows[i].record["rm.bl_id"];
   				var flId = rows[i].record["rm.fl_id"];
   				var dwgname = rows[i].record["rm.dwgname"];
   				var parameters = {};
   	   	        parameters['pkeyValues'] = {'bl_id':blId, 'fl_id':flId};
   	   	        parameters['drawingName'] = dwgname;
   	   	    	this.svgControl.unload(parameters);
   			}
   		}
   		

   	},
   	
    onClickAsset: function(params, drawingController){
    	var arrayRoomIDs = params['assetId'].split(";");
    	var restriction = new Ab.view.Restriction();
        restriction.addClause("rm.bl_id", arrayRoomIDs[0], "=", true);
        restriction.addClause("rm.fl_id", arrayRoomIDs[1], "=", true);
        restriction.addClause("rm.rm_id", arrayRoomIDs[2], "=", true);
         
        var rmDetailPanel = View.panels.get('abSpHlRmByDp_RmDetailPanel');
        rmDetailPanel.refresh(restriction);
        rmDetailPanel.show(true);
        rmDetailPanel.showInWindow({
        	width: 500,
        	height: 250
        });
         
        // toggle the room if clicked
    	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId']);
    },
    
    closeDetailPanel: function(){
        var rmDetailPanel = View.panels.get('abSpHlRmByDp_RmDetailPanel');
        rmDetailPanel.closeWindow();
    }
});

var dvId;
var dpId;
var blId;

/**
 * event handler when click the department level of the tree
 * @param {Object} ob
 */
function onDpTreeClick(ob){
    //remove all selected drawings
    controller.unloadSvg();
    
    var currentNode = View.panels.get('abSpHlRmByDp_dvTree').lastNodeClicked;
    dvId = currentNode.parent.data['dv.dv_id'];
    dpId = currentNode.data['dp.dp_id'];
    var dvRes = " = '" + dvId + "'";
    var dpRes = " = '" + dpId + "'";
    var blRes = " IS NOT NULL";
    if (blId) {
        blRes = " = '" + blId + "'";
    }
    var grid = View.panels.get('abSpHlRmByDp_flGrid');
    grid.addParameter('dvRes', dvRes);
    grid.addParameter('dpRes', dpRes);
    grid.addParameter('blRes', blRes);
    grid.refresh();
    
	controller.closeDetailPanel();
    setDrawingTitle();
}

/**
 * set drawing panel title
 */
function setDrawingTitle(){
    if (dvId || dpId) {
        controller.svgControl.getAddOn('InfoWindow').setText(String.format(getMessage('drawingPanelTitle1'), dvId + "-" + dpId));
    }
    else {
    	controller.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle'));
    }
}
