var abSpAsgnRmDpToRm_Controller = View.createController('abSpAsgnRmDpToRm_Controller', {
	onclickedFlObj:'',
	
	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	selectableOpt: null,
    afterViewLoad: function(){
        
    	controller = this;
    	
        //initialize svg control
		this.parameters = new Ab.view.ConfigObject();
		this.parameters['highlightParameters'] = [{'view_file':"ab-sp-asgn-dv-dp-to-rm.axvw", 'hs_ds': "ds_ab-sp-asgn-dv-dp-to-rm_drawing_rmHighlight", 'label_ds':'ds_ab-sp-asgn-dv-dp-to-rm_drawing_rmLabel'}];
		this.parameters['bordersHighlightSelector'] = 'true';
		this.parameters['borderSize'] = 18;
		this.parameters['highlightFilterSelector'] = 'true';
		this.parameters['allowMultipleDrawings'] = 'false';
		this.parameters['multipleSelectionEnabled'] = 'true';
		this.parameters['selectionMode'] = '1';
		this.parameters['assignMode'] = '2';
		this.parameters['divId'] = "svgDiv";
		this.parameters['orderByColumn'] = true;
		this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
		this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'DatasourceSelector': {panelId: "abSpAsgnDvDpToRm_drawingPanel"},
				'AssetLocator': {divId: "svgDiv"},
				'AssetTooltip': {handlers: [{assetType: 'rm', datasource: 'ds_ab-sp-asgn-dv-dp-to-rm_drawing_rmLabel', fields: 'rm.rm_id;rm.dv_id;rm.dp_id'}]},
				'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
		this.parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpAsgnDvDpToRm_drawingPanel", this.parameters);
		
    },
    
    abSpAsgnDvDpToRm_filterConsole_onShowTree: function() {
        var filterBlId = this.abSpAsgnDvDpToRm_filterConsole.getFieldValue('rm.bl_id');
        var filterDvId = this.abSpAsgnDvDpToRm_filterConsole.getFieldValue('rm.dv_id');
        var filterDpId = this.abSpAsgnDvDpToRm_filterConsole.getFieldValue('rm.dp_id');
        var blTreeRes = new Ab.view.Restriction();
        var dvRes = " IS NOT NULL";
        var dpRes = " IS NOT NULL";
        
        if (filterBlId) {
            blTreeRes.addClause("bl.bl_id", filterBlId, "=");
        }
        if (filterDvId) {
            dvRes = " = '" + filterDvId + "'";
        }
        if (filterDpId) {
            dpRes = " = '" + filterDpId + "'";
        }
        
        this.unloadSvg();
        this.abSpAsgnDvDpToRm_drawingPanel.isLoadedDrawing = false;
        this.svgControl.getAddOn('InfoWindow').setText(getMessage('selectFloor'));
        
        this.abSpAsgnDvDpToRm_dpAssignGrid.removeRows(0);
        this.abSpAsgnDvDpToRm_dpAssignGrid.update();
        
        this.abSpAsgnDvDpToRm_blTree.refresh(blTreeRes);
        this.abSpAsgnDvDpToRm_dvTree.addParameter('dvRes', dvRes);
        this.abSpAsgnDvDpToRm_dvTree.addParameter('dpRes', dpRes);
        this.abSpAsgnDvDpToRm_dvTree.refresh();
        
        dvId = dpId = "";
    },
    
    onDwgLoaded: function() {
    	if(dvId && dpId) {
            var title = String.format(getMessage('selectRm'), dvId + "-" + dpId);
            this.svgControl.getAddOn('InfoWindow').setText(title); 
//    		this.abSpAsgnDvDpToRm_drawingPanel.setToAssign("dp.dp_id", dpId);
    	} else {
    		this.svgControl.getAddOn('InfoWindow').setText(getMessage('selectDp'));
    	}
    },
    
	loadSvg: function(bl_id, fl_id, drawingName) {
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgname = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgname;
        
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
    	
   	},
   	
   	unloadSvg: function(){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgname;
    	this.svgControl.unload(parameters);
    	
   	},
   	
    onClickAsset: function(params, drawingController){
    	
    	var selectable = controller.selectableOpt[params['assetId']];
    	if(!selectable||!dvId||!dpId||!hpattern_acad)
    		return;
    	
    	var arrayRoomIDs = params['assetId'].split(";");
    	var selected = isRoomSelected(params, drawingController);
    	
    	if (dvId && dpId) {
    		var grid = View.panels.get("abSpAsgnDvDpToRm_dpAssignGrid");
    		extendDrawingRoomClickHandler(arrayRoomIDs, selected, grid, 'rm.dv_id', dvId, 'rm.dp_id', dpId,'rm.dv_name', dvName, 'rm.dp_name', dpName);
            var title = String.format(getMessage('selectRm'), dvId + "-" + dpId);
            controller.svgControl.getAddOn('InfoWindow').setText(title); 
    	} else {
    		if(!dpId) {
    			controller.svgControl.getAddOn('InfoWindow').setText(getMessage('selectDp')); 
    		}
    	}
    	
    	drawingController.getController("SelectController").setMultipleSelection(true);
    	if(selected)
    		drawingController.getController("SelectController").highlightSelectedAssets(params['svgId'], [params['assetId']], assignColor);
    	else
		{
    		drawingController.getController("HighlightController").clearAsset(params['assetId'], {'svgId': params['svgId'],  'persistFill': false, 'overwriteFill' : true});
    		var selectedAssets = drawingController.getController("SelectController").selectedAssets[params.svgId];
    		for(var i=0;i<selectedAssets.length;i++)
    		{
    			if(selectedAssets[i] == params.assetId){
    				selectedAssets.splice(i, 1);
    			}
    		}
		}
    		
    },
    
    reloadSvg: function(){
    	this.svgControl.drawingController.getControl().reload(this.parameters);
    },
   
    closeDetailPanel: function(){
        
    }
});

function isRoomSelected(params, drawingController){
	var selectedAssets = drawingController.getController("SelectController").selectedAssets[params.svgId];
	if(valueExistsNotEmpty(selectedAssets)){
		for(var i=0;i<selectedAssets.length;i++)
		if(selectedAssets[i] == params.assetId)
		{
			return false;
		}
	}
	return true;
}

var dvId;
var dpId;
var dvName;
var dpName;
var hpattern_acad;
var assignColor;
var controller;
/**
 * event handler when click tree node of dp level for the tree abSpAsgnDvDpToRm_dvTree.
 * @param {Object} ob
 */
function onDpTreeClick(ob){
    var drawingPanel = View.panels.get('abSpAsgnDvDpToRm_drawingPanel');
    var currentNode = View.panels.get('abSpAsgnDvDpToRm_dvTree').lastNodeClicked;
    dvId = currentNode.parent.data['dv.dv_id'];
    dpId = currentNode.data['dp.dp_id'];
    hpattern_acad = currentNode.data['dp.hpattern_acad'];
    dvName = currentNode.parent.data['dv.name'];
    dpName = currentNode.data['dp.name'];
    enableColorAssignment();
    if (drawingPanel.isLoadedDrawing) {
//        drawingPanel.setToAssign("dp.dp_id", dpId);
    	assignColor = getRgbColorByPattern(hpattern_acad);
        var title = String.format(getMessage('selectRm'), dvId + "-" + dpId);
        controller.svgControl.getAddOn('InfoWindow').setText(title); 
    } else {
        View.showMessage(getMessage('noFloorSelected'));
    }
    setSelectability(abSpAsgnRmDpToRm_Controller.onclickedFlObj.restriction);
}

/**
 * Get the departments to reset color.
 * @param divisionId
 * @param departmentId
 */
function getDepartmentRecordsForColorReseting(divisionId, departmentId) {
	var dpRestriction = new Ab.view.Restriction();
	dpRestriction.addClause("dp.dv_id", divisionId, "=");
	dpRestriction.addClause("dp.dp_id", departmentId, "=");
	var dpRecords = View.dataSources.get('ds_ab-sp-asgn-dv-dp-to-rm_dp').getRecords(dpRestriction);
	return dpRecords;
}

/**
 * event handler when click tree node of floor level for the tree abSpAsgnDvDpToRm_blTree.
 * @param {Object} ob
 */
function onFlTreeClick(ob){
	
	abSpAsgnRmDpToRm_Controller.onclickedFlObj=ob;
    var currentNode = View.panels.get('abSpAsgnDvDpToRm_blTree').lastNodeClicked;
    var drawingPanel = View.panels.get('abSpAsgnDvDpToRm_drawingPanel');
    var grid = View.panels.get('abSpAsgnDvDpToRm_dpAssignGrid');
    setSelectability(ob.restriction);
    flTreeClickHandler(currentNode, drawingPanel, grid);
    drawingPanel.isLoadedDrawing = true;
    
    abSpAsgnRmDpToRm_Controller.onDwgLoaded();
}

/**
 * event handler when click button 'revert all'.
 */
function resetAssignmentCtrls(){
    var drawingPanel = View.panels.get('abSpAsgnDvDpToRm_drawingPanel');
    var grid = View.panels.get("abSpAsgnDvDpToRm_dpAssignGrid");
    resetAssignment(drawingPanel, grid);
    if(dvId && dpId) {
//    	drawingPanel.setToAssign("dp.dp_id", dpId);
        var title = String.format(getMessage('selectRm'), dvId + "-" + dpId);
        controller.svgControl.getAddOn('InfoWindow').setText(title); 
    } else {
    	controller.svgControl.getAddOn('InfoWindow').setText(getMessage('selectDp')); 
    }

	rmTypeId = rmCatId = hpattern_acad = "";
	controller.svgControl.getAddOn('InfoWindow').setText(getMessage('selectDp'));
}

/**
 * event handler when click button 'save'.
 */
function saveAllChanges(){
    var dsChanges = View.dataSources.get("ds_ab-sp-asgn-dv-dp-to-rm_drawing_rmLabel");
    var drawingPanel = View.panels.get('abSpAsgnDvDpToRm_drawingPanel');
    var grid = View.panels.get("abSpAsgnDvDpToRm_dpAssignGrid");
    saveChange(drawingPanel, grid, dsChanges, ['rm.dv_id', 'rm.dp_id'], false);
    if(dvId && dpId) {
        var title = String.format(getMessage('selectRm'), dvId + "-" + dpId);
        controller.svgControl.getAddOn('InfoWindow').setText(title); 
//		drawingPanel.setToAssign("dp.dp_id", dpId);
    } else {
    	controller.svgControl.getAddOn('InfoWindow').setText(getMessage('selectDp')); 
    }
}

/**
 * event handler lisenner after create the tree node lable
 */
function afterGeneratingTreeNode(treeNode){
    addLegendToTreeNode('abSpAsgnDvDpToRm_dvTree', treeNode, 1, 'dp', 'dp.dp_id');
}

/**
 * set unoccupiable room unselected.
 * @param {Object} restriction
 */
function setSelectability(restriction){
    var drawingPanel = View.panels.get('abSpAsgnDvDpToRm_drawingPanel')
    var rmRecords = View.dataSources.get('ds_ab-sp-rm_occupiable').getRecords(restriction);
    controller.selectableOpt = {};
    for (var i = 0; i < rmRecords.length; i++) {
        var record = rmRecords[i];
        var supercat = record.getValue('rmcat.supercat');
        var blId = record.getValue('rm.bl_id');
        var flId = record.getValue('rm.fl_id');
        var rmId = record.getValue('rm.rm_id');
        
        //kb:3030349,by comments (JIANBING 2012-08-09 11:16)1. In view ab-sp-asgn-dv-dp-to-rm.axvw, 
        //I am not able to assign dv-dp to service area. User should be able to assign dv-dp to sevice area. 
        var assetId = blId + ';' + flId + ';' + rmId;
        if (supercat == 'VERT') {
        	controller.selectableOpt[assetId] = false;
        }else{
        	controller.selectableOpt[assetId] = true;
        }
    }
}

function enableColorAssignment() {
	if(dvId && dpId) {
		var dpRecord = getDepartmentRecordsForColorReseting(dvId,dpId);
	    resetAssgnColor('dp.hpattern_acad', dpRecord, 'dp.dv_id', dvId, 'dp.dp_id', dpId);
	}
}
