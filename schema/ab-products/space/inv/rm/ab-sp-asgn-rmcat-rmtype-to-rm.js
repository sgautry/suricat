var controller = View.createController('abSpAsgnRmCatRmTypeToRm_Control', {

	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
    afterViewLoad: function(){
        
        //initialize svg control
		this.parameters = new Ab.view.ConfigObject();
		this.parameters['highlightParameters'] = [{'view_file':"ab-sp-asgn-rmcat-rmtype-to-rm.axvw", 'hs_ds': "ds_ab-sp-asgn-rmcat-rmtype-to-rm_drawing_rmHighlight", 'label_ds':'ds_ab-sp-asgn-rmcat-rmtype-to-rm_drawing_rmLabel'}];
		this.parameters['bordersHighlightSelector'] = 'true'; 
		this.parameters['highlightFilterSelector'] = 'true';
		this.parameters['allowMultipleDrawings'] = 'false';
		this.parameters['multipleSelectionEnabled'] = 'true';
		this.parameters['divId'] = "svgDiv";
		this.parameters['orderByColumn'] = true;
		this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
		this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'DatasourceSelector': {panelId: "abSpAsgnRmcatRmTypeToRm_drawingPanel"},
				'AssetLocator': {divId: "svgDiv"},
				'AssetTooltip': {handlers: [{assetType: 'rm', datasource: 'ds_ab-sp-asgn-rmcat-rmtype-to-rm_drawing_rmLabel', fields: 'rm.rm_id;rm.rm_cat;rm.rm_type'}]},
				'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
		this.parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpAsgnRmcatRmTypeToRm_drawingPanel", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('selectFloor'));
		
    },

    abSpAsgnRmcatRmTypeToRm_filterConsole_onShowTree: function(){
        var filterBlId = this.abSpAsgnRmcatRmTypeToRm_filterConsole.getFieldValue('rm.bl_id');
        var filterRmCat = this.abSpAsgnRmcatRmTypeToRm_filterConsole.getFieldValue('rm.rm_cat');
        var blTreeRes = new Ab.view.Restriction();
        var rmCatTreeRes = new Ab.view.Restriction();
        
        if (filterBlId) {
            blTreeRes.addClause("bl.bl_id", filterBlId, "=");
        }
        
        if (filterRmCat) {
            rmCatTreeRes.addClause("rmcat.rm_cat", filterRmCat, "=");
        }
        
        this.unloadSvg();
        this.abSpAsgnRmcatRmTypeToRm_drawingPanel.isLoadedDrawing = false;
        this.svgControl.getAddOn('InfoWindow').setText(getMessage('selectFloor'));
        
        this.abSpAsgnRmcatRmTypeToRm_rmtypeAssignGrid.removeRows(0);
        this.abSpAsgnRmcatRmTypeToRm_rmtypeAssignGrid.update();
        
        this.abSpAsgnRmcatRmTypeToRm_blTree.refresh(blTreeRes);
        this.abSpAsgnRmcatRmTypeToRm_rmcatTree.refresh(rmCatTreeRes);
        
        rmTypeId = rmCatId = hpattern_acad = "";
    },
    
	loadSvg: function(bl_id, fl_id, drawingName) {
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgname = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgname;
        
		//svgControl object used in ab-sp-common.js
		svgControl = this.svgControl;
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
    	
   	},
   	
   	unloadSvg: function(){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgname;
    	this.svgControl.unload(parameters);

   	},
   	
    onClickAsset: function(params, drawingController){
    
    	if(!hpattern_acad||!rmTypeId||!rmCatId)
    		return;
    	
    	var arrayRoomIDs = params['assetId'].split(";");
    	var selected = isRoomSelected(params, drawingController);
    	
    	var grid = View.panels.get("abSpAsgnRmcatRmTypeToRm_rmtypeAssignGrid");
    	drawingRoomClickHandler(arrayRoomIDs, selected, grid, 'rm.rm_cat', rmCatId, 'rm.rm_type', rmTypeId);
    	
    	drawingController.getController("SelectController").setMultipleSelection(true);
    	if(selected)
    		drawingController.getController("SelectController").highlightSelectedAssets(params['svgId'], [params['assetId']], assignColor);
    	else
		{
    		drawingController.getController("HighlightController").clearAsset(params['assetId'], {'svgId': params['svgId'],  'persistFill': false, 'overwriteFill' : true});
    		var selectedAssets = drawingController.getController("SelectController").selectedAssets[params.svgId];
    		for(var i=0;i<selectedAssets.length;i++)
    		{
    			if(selectedAssets[i] == params.assetId){
    				selectedAssets.splice(i, 1);
    			}
    		}
		}
    
    },
    
    reloadSvg: function(){
    	this.svgControl.drawingController.getControl().reload(this.parameters);
    },
    
    closeDetailPanel: function(){

    }

});

var rmTypeId;
var rmCatId;
var assignColor;
var hpattern_acad;

/**
 * check the room is selected or not
 */
function isRoomSelected(params, drawingController){
	var selectedAssets = drawingController.getController("SelectController").selectedAssets[params.svgId];
	if(valueExistsNotEmpty(selectedAssets)){
		for(var i=0;i<selectedAssets.length;i++)
		if(selectedAssets[i] == params.assetId)
		{
			return false;
		}
	}
	return true;
}

/**
 * event handler when click tree node of room type level for the tree abSpAsgnRmcatRmTypeToRm_rmcatTree.
 * @param {Object} ob
 */
function onRmTypeTreeClick(ob){
    var drawingPanel = View.panels.get('abSpAsgnRmcatRmTypeToRm_drawingPanel');
    var currentNode = View.panels.get('abSpAsgnRmcatRmTypeToRm_rmcatTree').lastNodeClicked;
    rmTypeId = currentNode.data['rmtype.rm_type'];
    rmCatId = currentNode.parent.data['rmcat.rm_cat'];
    hpattern_acad = currentNode.data['rmtype.hpattern_acad'];
    
    var rmTypeRecord = getRmTypeRecordsForColorReseting(rmCatId, rmTypeId);
    resetAssgnColor('rmtype.hpattern_acad', rmTypeRecord, 'rmtype.rm_cat', rmCatId, 'rmtype.rm_type', rmTypeId);
    if (drawingPanel.isLoadedDrawing) {

    	assignColor = getRgbColorByPattern(hpattern_acad);
        var title = String.format(getMessage('selectRm'), rmCatId + "-" + rmTypeId);
        controller.svgControl.getAddOn('InfoWindow').setText(title);
    }
    else {
        View.showMessage(getMessage('noFloorSelected'));
    }
}

function getRmTypeRecordsForColorReseting(roomCategory, roomType) {
	var restriction = new Ab.view.Restriction();
	restriction.addClause("rmtype.rm_cat", roomCategory, "=");
	restriction.addClause("rmtype.rm_type", roomType, "=");
	var rmTypeRecord = View.dataSources.get('ds_ab-sp-asgn-rmcat-rmtype-to-rm_rmtype').getRecords(restriction);
	return rmTypeRecord;
}

/**
 * event handler when click tree node of floor level for the tree abSpAsgnRmcatRmTypeToRm_blTree.
 * @param {Object} ob
 */
function onFlTreeClick(ob){
	var currentNode = View.panels.get('abSpAsgnRmcatRmTypeToRm_blTree').lastNodeClicked;
    var drawingPanel = View.panels.get('abSpAsgnRmcatRmTypeToRm_drawingPanel');
    var grid = View.panels.get('abSpAsgnRmcatRmTypeToRm_rmtypeAssignGrid');
    flTreeClickHandler(currentNode, drawingPanel, grid);
    drawingPanel.isLoadedDrawing = true;
    
    controller.svgControl.getAddOn('InfoWindow').setText(getMessage('selectType'));
}

/**
 * event handler when click button 'revert all'.
 */
function resetAssignmentCtrls(){
    var drawingPanel = View.panels.get('abSpAsgnRmcatRmTypeToRm_drawingPanel');
    var grid = View.panels.get("abSpAsgnRmcatRmTypeToRm_rmtypeAssignGrid");
    resetAssignment(drawingPanel, grid);
    controller.svgControl.getAddOn('InfoWindow').setText(getMessage('selectType'));
    
    rmTypeId = rmCatId = hpattern_acad = "";
}

/**
 * event handler when click button 'save'.
 */
function saveAllChanges(){
    var dsChanges = View.dataSources.get("ds_ab-sp-asgn-rmcat-rmtype-to-rm_drawing_rmLabel");
    var drawingPanel = View.panels.get('abSpAsgnRmcatRmTypeToRm_drawingPanel');
    var grid = View.panels.get("abSpAsgnRmcatRmTypeToRm_rmtypeAssignGrid");
    saveChange(drawingPanel, grid, dsChanges, ['rm.rm_cat', 'rm.rm_type'], true);

    controller.svgControl.getAddOn('InfoWindow').setText(getMessage('selectType'));
}

/**
 * event handler lisenner after create the tree node lable
 */
function afterGeneratingTreeNode(treeNode){
    addLegendToTreeNode('abSpAsgnRmcatRmTypeToRm_rmcatTree', treeNode, 1, 'rmtype', 'rmtype.rm_type');
}
