/**
 * @author Guo
 */

var controller = View.createController('abSpHlRmByRmcatTmtypeTransController', {

	blConsoleRes : '1=1',
	rmpctConsoleRes : '1=1',
	date : '',
	dateToAddField:'rmpct.date_start',
	dateToUpdateField:'rmpct.date_start',
	console:null,
	timeLine:null,
	
	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,

	// ----------------event handle--------------------
	afterViewLoad : function() {
		
        //initialize svg control
		this.parameters = new Ab.view.ConfigObject();
		this.parameters['highlightParameters'] = [{'view_file':"ab-sp-hl-rm-by-rmcat-rmtype-trans.axvw", 'hs_ds': "abSpShareDSForHlRmpctForCatType_rmHighlightCatDS", 'label_ds':'abSpShareDSForHlRmpctForCatType_rmLabelCatDS'}];
		this.parameters['bordersHighlightSelector'] = 'true';
		this.parameters['borderSize'] = 18;
		this.parameters['highlightFilterSelector'] = 'true';
		this.parameters['allowMultipleDrawings'] = 'false';
		this.parameters['divId'] = "svgDiv";
		this.parameters['orderByColumn'] = true;
		this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
		this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'DatasourceSelector': {panelId: "abSpHlRmByRmcatRmtypeTrans_DrawingPanel"},
				'AssetLocator': {divId: "svgDiv"},
				'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
		this.parameters['events'] = [{'eventName': 'mouseover', 'assetType' : 'rm', 'handler' : this.onMouseOverAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpHlRmByRmcatRmtypeTrans_DrawingPanel", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
		
		var selectorId = 'selector_' + 'hilite';
		var combo = Ext.get(selectorId);
		combo.dom.onchange = onChangeHighlightDS.createDelegate(this, [this, combo]);
		
	},

	afterInitialDataFetch : function() {
		this.date = getCurrentDate();
		this.abSpHlRmByRmcatRmtypeTrans_console.setFieldValue('rmpct.date_start', this.date);
		this.resetDataViewBorder();
		this.console=this.abSpHlRmByRmcatRmtypeTrans_console;
		this.timeLine=this.timeLineButton;
		this.dateAction = this.timeLine.actions.get('currentDate');
		this.dateAction.setTitle(this.console.getFieldElement(this.dateToAddField).value);
		this.date=this.console.getFieldValue('rmpct.date_start');
		setTimeTitle(this.timeLine);
		
	},
	
	resetDataViewBorder : function() {
		var templatehtml = this.abSpHlRmByRmcatRmtypeTrans_rmAttribute.dataView.levels[0].bodyXTemplate.html;
		var lastTrIndex = templatehtml.lastIndexOf('<TR>');
		if (lastTrIndex == -1) {
			lastTrIndex = templatehtml.lastIndexOf('<tr>');
		}
		var newTemplatehtml = (templatehtml.substring(0, lastTrIndex) + "<TR class=\"last\">" + templatehtml.substring(lastTrIndex + 4)).replace(/first/g, 'columnReportLabel').replace(/fill/g, 'columnReportValue');
		this.abSpHlRmByRmcatRmtypeTrans_rmAttribute.dataView.levels[0].bodyXTemplate = new Ext.XTemplate(newTemplatehtml);
	},

	abSpHlRmByRmcatRmtypeTrans_rmSummaryByType_afterRefresh : function() {
		resetColorFieldValue('abSpHlRmByRmcatRmtypeTrans_rmSummaryByType', 'abSpHlRmByRmcatRmtypeTrans_DrawingPanel', 'rmtype.rm_type', 'rmtype.hpattern_acad', 'abSpHlRmByRmcatRmtypeTrans_rmSummaryByType_legend');
	},

	abSpHlRmByRmcatRmtypeTrans_rmSummaryByCat_afterRefresh : function() {
		resetColorFieldValue('abSpHlRmByRmcatRmtypeTrans_rmSummaryByCat', 'abSpHlRmByRmcatRmtypeTrans_DrawingPanel', 'rmcat.rm_cat', 'rmcat.hpattern_acad', 'abSpHlRmByRmcatRmtypeTrans_rmSummaryByCat_legend');
	},

	abSpHlRmByRmcatRmtypeTrans_console_onShowTree : function() {
		var console = this.abSpHlRmByRmcatRmtypeTrans_console;
		var blId = console.getFieldValue('rmpct.bl_id');
		var rmCat = console.getFieldValue('rmpct.rm_cat');
		this.date = console.getFieldValue('rmpct.date_start');
		this.blConsoleRes = '1=1';
		this.rmpctConsoleRes = '1=1';

		if (blId) {
			this.blConsoleRes = "bl.bl_id =" + literal(blId)
		}
		if (rmCat) {
			this.rmpctConsoleRes += " and rmpct.rm_cat =" + literal(rmCat)
		}
		this.resetHighlights();
		this.abSpHlRmByRmcatRmtypeTrans_flTree.refresh();
		this.dateAction = this.timeLine.actions.get('currentDate');
		this.dateAction.setTitle(this.date);
		this.date=this.console.getFieldValue('rmpct.date_start');
	},

	/**
	 * event handler when click the floor level of the tree
	 * 
	 */
	resetHighlights : function() {
		setParameters();
		if (this.abSpHlRmByRmcatRmtypeTrans_DrawingPanel.isLoadDrawing) {
			
			this.reloadSvg();
			showStatisticsGrid();
		}
	},
	afterTimeButtonClick : function(){
		this.resetHighlights();
	},
	
	loadSvg: function(bl_id, fl_id, drawingName) {
		
		addParameterConfig(this.parameters);
		
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgName = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
        
		//svgControl object used in ab-sp-common.js
		svgControl = this.svgControl;
		
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
    	
   	},

   	reloadSvg: function(){
   		
   		addParameterConfig(this.parameters);
   		this.parameters['drawingName'] = this.dwgName;
   		this.svgControl.drawingController.getControl().reload(this.parameters);	
   	},
   	
   	unloadSvg: function(){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
    	this.svgControl.unload(parameters);
    	
   	},
   	
   	onMouseOverAsset: function(params, drawingController){
    	var arrayRoomIDs = params['assetId'].split(";");
    	var restriction = new Ab.view.Restriction();
        restriction.addClause("rmpct.bl_id", arrayRoomIDs[0], "=", true);
        restriction.addClause("rmpct.fl_id", arrayRoomIDs[1], "=", true);
        restriction.addClause("rmpct.rm_id", arrayRoomIDs[2], "=", true);
         
		View.panels.get('abSpHlRmByRmcatRmtypeTrans_rmAttribute').refresh(restriction);
		
    }

});

/**
 * event handler when click the floor level of the tree
 * 
 * @param {Object}
 *            ob
 */
function onFlTreeClick(ob) {
	var currentNode = View.panels.get('abSpHlRmByRmcatRmtypeTrans_flTree').lastNodeClicked;
	controller.blId = currentNode.parent.data['bl.bl_id'];
	controller.flId = currentNode.data['fl.fl_id'];

	var drawingPanel = View.panels.get('abSpHlRmByRmcatRmtypeTrans_DrawingPanel');
	setParameters();

	var title = String.format(getMessage('drawingPanelTitle2'), controller.blId + "-" + controller.flId);
	var dwgName = currentNode.data['fl.dwgname'];
	controller.loadSvg(controller.blId, controller.flId, dwgName);
	drawingPanel.isLoadDrawing = true;
	controller.svgControl.getAddOn('InfoWindow').setText(title);
	showStatisticsGrid();
}

function onChangeHighlightDS(selectorControl, combo) {
	if (controller.abSpHlRmByRmcatRmtypeTrans_DrawingPanel.isLoadDrawing) {
		var selectedValue = combo.dom.value;
		var drawingPanel = View.panels.get('abSpHlRmByRmcatRmtypeTrans_DrawingPanel');
		drawingPanel.currentHighlightDS = selectedValue;
		showStatisticsGrid();
	}
}

function setParameters() {
	controller.date = controller.abSpHlRmByRmcatRmtypeTrans_console.getFieldValue('rmpct.date_start');
	var list = ['abSpShareDSForHlRmpctForCatType_rmHighlightCatDS', 
	            'abSpShareDSForHlRmpctForCatType_rmHighlightCatAndTypeDS', 
	            'abSpShareDSForHlRmpctForCatType_rmLabelCatDS', 
	            'abSpShareDSForHlRmpctForCatType_rmLabelCatAndTypeDS', 
	            'abSpHlRmByRmcatRmtypeTrans_rmSummaryByCat', 
	            'abSpHlRmByRmcatRmtypeTrans_rmSummaryByType', 
	            'abSpShareDSForRmAttributeDS'];
	for ( var i = 0; i < list.length; i++) {
		var control = View.dataSources.get(list[i]);
		if (!control) {
			control = View.panels.get(list[i]);
		}
		control.addParameter('date',  controller.date);
		control.addParameter('rmpctConsoleRes', controller.rmpctConsoleRes);
	}
	controller.abSpHlRmByRmcatRmtypeTrans_flTree.addParameter('blConsoleRes', controller.blConsoleRes);
}

function showStatisticsGrid() {
	View.panels.get('abSpHlRmByRmcatRmtypeTrans_rmSummaryByCat').show(false);
	View.panels.get('abSpHlRmByRmcatRmtypeTrans_rmSummaryByType').show(false);
	var grid = getGridFromDrawingHighlight();
	grid.show(true);
	grid.addParameter('blId', controller.blId);
	grid.addParameter('flId', controller.flId);
	grid.refresh();
}

function getGridFromDrawingHighlight() {
	var grid = View.panels.get('abSpHlRmByRmcatRmtypeTrans_rmSummaryByCat');
	var drawingPanel = View.panels.get('abSpHlRmByRmcatRmtypeTrans_DrawingPanel');
	if (drawingPanel.currentHighlightDS == 'abSpShareDSForHlRmpctForCatType_rmHighlightCatAndTypeDS') {
		grid = View.panels.get('abSpHlRmByRmcatRmtypeTrans_rmSummaryByType');
	}

	return grid;
}


function literal(value) {
	return "'" + value + "'"
}
