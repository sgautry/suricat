/**
 * @author Guo
 */

var controller = View.createController('abSpHlRmByAttributeController', {
	
	/**
	 * selected building code in the floor tree
	 */
	blId : '',
	
	/**
	 * selected floor code in the floor tree
	 */
	flId : '',
	
	/**
	 * selected floor plan dwg name
	 */
	dwgName : '',
	
	/**
	 * selected border highlight option value
	 */
	borderHighlightOption : '',
	
	borderHighlightAtrrituteArray : new Ext.util.MixedCollection(),
	
	/**
	 * building restriction in the console
	 */
	blConsoleRes : '1=1',
	
	/**
	 * rmpct table restriction from console
	 */
	rmpctConsoleRes : '1=1',

	/**
	 * date value from console, using ISO format like '2011-07-20'
	 */
	date : '',
	dateToAddField:'rmpct.date_start',
	dateToUpdateField:'rmpct.date_start',
	console:null,
	timeLine:null,
	
	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
	/**
	 * pending request only flag
	 */
	pendingRequestOnly : '0',

	// ----------------event handle--------------------
	afterViewLoad : function() {
		// set drawing panel event handler
		this.abSpHlRmByAttribute_floorPlan.addEventListener('onload', onLoadHandler);
		
        //initialize svg control
		this.parameters = new Ab.view.ConfigObject();
		this.parameters['highlightParameters'] = [{'view_file':"ab-sp-hl-rm-by-attribute.axvw", 'hs_ds': "abSpShareDSForHlRmpctForCatType_rmHighlightCatDS", 'label_ds':'abSpShareDSForHlRmpctForCatType_rmLabelCatDS'}];
		this.parameters['bordersHighlightSelector'] = 'true';
		this.parameters['borderSize'] = 18;
		this.parameters['highlightFilterSelector'] = 'true';
		this.parameters['allowMultipleDrawings'] = 'false';
		this.parameters['divId'] = "svgDiv";
		this.parameters['orderByColumn'] = true;
		this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
		this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'DatasourceSelector': {panelId: "abSpHlRmByAttribute_floorPlan"},
				'AssetLocator': {divId: "svgDiv"},
				'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
		this.parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickAsset},
		                             {'eventName': 'contextmenu', 'assetType' : 'rm', 'handler' : this.onContextMenuRoom}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpHlRmByAttribute_floorPlan", this.parameters);
		
		var selectController = this.svgControl.drawingController.getController("SelectController");
    	//enable diagonal border selection 
    	selectController.setDiagonalSelectionPattern(true);
    	//set diagonal selection border size (default is 20)
    	selectController.setDiagonalSelectionBorderWidth(20); 
    	//enable Multiple Selection
    	selectController.setMultipleSelection(false);
    	
		var selectorIdHilite = 'selector_' + 'hilite';
		var comboHilite = Ext.get(selectorIdHilite);
		comboHilite.dom.onchange = onChangeHighlightDS.createDelegate(this, [this, comboHilite]);
		
		var selectorIdIBorders = 'selector_' + 'IBorders';
		var comboIBorders = Ext.get(selectorIdIBorders);
		comboIBorders.dom.onchange = this.changeBorderHighlightOption.createDelegate(this, [this, comboIBorders]);
		
		View.panels.get('abSpHlRmByAttribute_floorPlan').currentHighlightDS = "abSpShareDSForHlRmpctForCatType_rmHighlightCatDS";
	},
	   

	afterInitialDataFetch : function() {
		
		//set default date to current date
		this.date = getCurrentDate();
		this.abSpHlRmByAttribute_console.setFieldValue('rmpct.date_start', this.date);
		this.console=this.abSpHlRmByAttribute_console;
		this.timeLine=this.timeLineButton;
		setTimeTitle(this.timeLine);
		this.dateAction = this.timeLine.actions.get('currentDate');
		this.dateAction.setTitle(this.console.getFieldElement(this.dateToAddField).value);
		this.date=this.console.getFieldValue('rmpct.date_start');
		//hide the layout used for border higlight option
		var layoutManager = View.getLayoutManager('nested_east');
		if (!layoutManager.isRegionCollapsed('south')) {
			layoutManager.collapseRegion('south');
		}

		//reset the two date view panel border to make it cross all columns
		this.resetDataVieSeperator(this.abSpHlRmByAttribute_rmAttribute);
		
		//set data source parameters
		setParameters();
		
		//refresh and show floor tree
		this.abSpHlRmByAttribute_flTree.refresh();
	},

	/**
	 * reset the date view panel border to make it cross all columns
	 * @param {Object} ob
	 */
	resetDataVieSeperator : function(panel) {
		//get the default boday template
		var templatehtml = panel.dataView.levels[0].bodyXTemplate.html;
		//get the last tr
		var lastTrIndex = templatehtml.lastIndexOf('<TR>');
		if (lastTrIndex == -1) {
			lastTrIndex = templatehtml.lastIndexOf('<tr>');
		}
		//set the last tr class to 'last',
		var newTemplatehtml = (templatehtml.substring(0, lastTrIndex) + "<TR class=\"last\">" + templatehtml.substring(lastTrIndex + 4)).replace(/first/g, 'columnReportLabel').replace(/fill/g, 'columnReportValue');
		//reset the body template
		panel.dataView.levels[0].bodyXTemplate = new Ext.XTemplate(newTemplatehtml);
	},

	/**
	 * show action event handler in console panel
	 */
	abSpHlRmByAttribute_console_onShow : function() {
		//get value from the console
		var pendingRequestOnly = $('pendingRequestOnly').checked;
		
		//set restriction based on the console value
		this.blConsoleRes =  getRestrictionStrFromConsole(this.abSpHlRmByAttribute_console, new Array(['rmpct.bl_id','=','bl.bl_id']));
		this.rmpctConsoleRes =getRestrictionStrFromConsole(this.abSpHlRmByAttribute_console, new Array(['rmpct.dv_id','=','rmpct.dv_id'],['rmpct.dp_id','=','rmpct.dp_id'],['rmpct.rm_cat','=','rmpct.rm_cat']));
		
		if (pendingRequestOnly) {
			this.pendingRequestOnly = '1';
		}else{
			this.pendingRequestOnly = '0';
		}
		
		//set datasouce parameters and reset the drawing panel highlight
		this.resetHighlights();
		
		//show floor tree after set parameter in resetHighlights();
		this.abSpHlRmByAttribute_flTree.refresh();
		
		//kb#3036556: refresh timeline button title and set selected date value
		this.dateAction.setTitle(this.console.getFieldElement(this.dateToAddField).value);
		this.date=this.console.getFieldValue('rmpct.date_start');
		
	},

	/**
	 * Clear action event handler in console panel
	 */
	abSpHlRmByAttribute_console_onClear : function() {
		this.abSpHlRmByAttribute_console.clear();
		this.date = getCurrentDate();
		this.abSpHlRmByAttribute_console.setFieldValue('rmpct.date_start', this.date);
	},

	/**
	 * event handle when change the border highlight option
	 */
	changeBorderHighlightOption : function(selectorControl, combo) {
		var selectedValue = combo.dom.value;
		this.borderHighlightOption = selectedValue;
		this.resetHighlights();
	},

	/**
	 * open Assign Room Attributes and Occupancy view
	 */
	abSpHlRmByAttribute_rmAttribute_onAssign : function() {
		var restriction = new Ab.view.Restriction();
		View.blId = this.blId;
		View.flId = this.flId;
		View.openDialog('ab-sp-asgn-rm-attributes.axvw', null, true, {
	        width: 1000,
	        height: 530
	    });
	},
	
	/**
	 * reset the display value of Occupancy title to support specified sort and add total row
	 */
	abSpHlRmByAttribute_rmSummary6_afterRefresh : function() {
		var totalCount = 0;
		var totalArea = 0.00;
		var totalCapacity = 0;
		var totalOccupy = 0;
		var totalAvail = 0;
		var totals = new Ab.data.Record();
		var grid = this.abSpHlRmByAttribute_rmSummary6;
		for (i = 0; i < grid.gridRows.length; i++) {
	        var row = grid.gridRows.items[i];
	        totalCount += parseInt(row.getFieldValue('rm.count_rm'));
	        totalArea += parseFloat(row.getFieldValue('rm.area_rm_total'));
	        totalCapacity += parseInt(row.getFieldValue('rm.cap_em'));
	        totalOccupy += parseInt(row.getFieldValue('rm.count_em'));
	        totalAvail += parseInt(row.getFieldValue('rm.available'));

	        var occupancy = row.getFieldValue('rm.occupancy');
	        if(occupancy=='1'){
	        	row.setFieldValue('rm.occupancy', getMessage('Nonoccupiable'))
	        }else
	        if(occupancy=='2'){
	        	row.setFieldValue('rm.occupancy', getMessage('Vacant'))
	        }else
	        if(occupancy=='3'){
	        	row.setFieldValue('rm.occupancy', getMessage('Available'))
	        }else
	        if(occupancy=='4'){
	        	row.setFieldValue('rm.occupancy', getMessage('FullyOccupied'))
	        }else
	        if(occupancy=='5'){
	        	row.setFieldValue('rm.occupancy', getMessage('OverOccupied'))
	        }
		}
		totals.setValue('rm.sum_count_rm',insertGroupingSeparator(totalCount+"",true,true));
		totals.setValue('rm.sum_area_rm_total',insertGroupingSeparator(totalArea.toFixed(2)+"",true,true));
		totals.setValue('rm.sum_cap_em',insertGroupingSeparator(totalCapacity+"",true,true));
		totals.setValue('rm.sum_count_em',insertGroupingSeparator(totalOccupy+"",true,true));
		totals.setValue('rm.sum_available',insertGroupingSeparator(totalAvail+"",true,true));
		totals.localizedValues = totals.values;
		grid.totals = totals;
		grid.buildTotalsFooterRow(grid.tableFootElement);
	},

    /**
	 * and add total row for grid abSpHlRmByAttribute_rmSummary8
	 */
	abSpHlRmByAttribute_rmSummary8_afterRefresh : function() {
		var totalCount = 0;
		var totalArea = 0.00;
		var totals = new Ab.data.Record();
		var grid = this.abSpHlRmByAttribute_rmSummary8;
		for (i = 0; i < grid.gridRows.length; i++) {
	        var row = grid.gridRows.items[i];
	        totalCount += parseInt(row.getFieldValue('rmcat.count_rm'));
	        totalArea += parseFloat(row.getFieldValue('rmcat.area_rm_total'));
		}
		totals.setValue('rmcat.sum_count_rm',insertGroupingSeparator(totalCount+"",true,true));
		totals.setValue('rmcat.sum_area_rm_total',insertGroupingSeparator(totalArea.toFixed(2)+"",true,true));
		totals.localizedValues = totals.values;
		grid.totals = totals;
		grid.buildTotalsFooterRow(grid.tableFootElement);
	},

	/**
	 * Reset the floor plan highlight
	 */
	resetHighlights : function() {
		//set datasource parameter
		setParameters();
		if (this.abSpHlRmByAttribute_floorPlan.isLoadDrawing) {
			
			//reset the border highlight
			setBorderHighlights.defer(100);
			
			this.reloadSvg();
			//show statictics grid base on the current highlight datasource
			showStatisticsGrid();
		}
	},
	afterTimeButtonClick : function(){
		this.resetHighlights();
	},
	
	loadSvg: function(bl_id, fl_id, drawingName) {
		
		addParameterConfig(this.parameters);
		
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgName = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;

        // load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
   	},
   	
   	unloadSvg: function(bl_id, fl_id, drawingName){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id':fl_id};
        parameters['drawingName'] = drawingName;
    	this.svgControl.unload(parameters);

   	},
   	
   	onClickAsset: function(params, drawingController) {
   		var arrayRoomIDs = params['assetId'].split(";");
   	    
   		var restriction = new Ab.view.Restriction();
		restriction.addClause("rmpct.bl_id", arrayRoomIDs[0], "=", true);
		restriction.addClause("rmpct.fl_id", arrayRoomIDs[1], "=", true);
		restriction.addClause("rmpct.rm_id", arrayRoomIDs[2], "=", true);
		View.panels.get('abSpHlRmByAttribute_rmAttribute').refresh(restriction);
   		
   		// toggle the room if clicked
    	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId']);
   	},
   	
    reloadSvg: function(){
    	var drawingPanel = View.panels.get('abSpHlRmByAttribute_floorPlan');
		this.parameters.highlightParameters[0].hs_ds = drawingPanel.currentHighlightDS;
		this.parameters['drawingName'] = this.dwgName;
		addParameterConfig(this.parameters);
		
    	this.svgControl.drawingController.getControl().reload(this.parameters);
    },
    
    onContextMenuRoom: function(params, drawingController,e){
    	ob = params.assetId.split(";");
    	var x = e.clientX;
    	var y = e.clientY;
    	controller.rightClickMenu.showInWindow( {x: x, y: y, width: 150, height: 150, title: params.assetId, modal: false});

    }

});

var ob;

/**
 * event handler when click the building level of the tree
 * 
 * @param {Object} ob
 */
function onBlTreeClick(ob) {
	var currentNode = View.panels.get('abSpHlRmByAttribute_flTree').lastNodeClicked;
	controller.blId = currentNode.data['bl.bl_id'];
	View.panels.get('abSpHlRmByAttribute_flStatistics').show(false);
	var blStatisticsPanel = View.panels.get('abSpHlRmByAttribute_blStatistics');
	var restriction = new Ab.view.Restriction();
	restriction.addClause("bl.bl_id", controller.blId, "=", true);
	blStatisticsPanel.refresh(restriction);
}

/**
 * event handler when click the floor level of the tree
 * 
 * @param {Object} ob
 */
function onFlTreeClick(ob) {
	var currentNode = View.panels.get('abSpHlRmByAttribute_flTree').lastNodeClicked;
	controller.blId = currentNode.parent.data['bl.bl_id'];
	controller.flId = currentNode.data['fl.fl_id'];
	//add to fix KB3036418 - make sure it work when dwgname is not a concatenation of bl_id+fl_id
	controller.dwgName = currentNode.data['fl.dwgname'];

	View.panels.get('abSpHlRmByAttribute_blStatistics').show(false);
	var flStatisticsPanel = View.panels.get('abSpHlRmByAttribute_flStatistics');
	var restriction = new Ab.view.Restriction();
	restriction.addClause("fl.bl_id", controller.blId, "=", true);
	restriction.addClause("fl.fl_id", controller.flId, "=", true);
	flStatisticsPanel.refresh(restriction);

	var drawingPanel = View.panels.get('abSpHlRmByAttribute_floorPlan');
	setParameters();

	var title = String.format(getMessage('drawingPanelTitle2'), controller.blId + "-" + controller.flId);
//	controller.svgControl.getAddOn('InfoWindow').setText(title);
	setPanelTitle("abSpHlRmByAttribute_floorPlan",title);
	
	controller.loadSvg(controller.blId, controller.flId, controller.dwgName);
	drawingPanel.isLoadDrawing = true;
	showStatisticsGrid();
	setBorderHighlights.defer(100);
}

/**
 * set datasource parameters
 */
function setParameters() {
	controller.date = controller.abSpHlRmByAttribute_console.getFieldValue('rmpct.date_start');
	var list = ['abSpShareDSForHlRmByDpPerFl_rmHighlightDS', 
	            'abSpShareDSForHlRmpctForCatType_rmHighlightCatDS', 
	            'abSpShareDSForHlRmpctForCatType_rmHighlightCatAndTypeDS', 
	            'iBorder_abSpShareDSForHlRmByDpPerFl_rmHighlightDS', 
	            'iBorder_abSpShareDSForHlRmpctForCatType_rmHighlightCatDS', 
	            'iBorder_abSpShareDSForHlRmpctForCatType_rmHighlightCatAndTypeDS', 
	            'iBorderRmHlByDV',
		        'rmHlBySuperCategory',
	            'rmLabelBySuperCategory',
	            'rmHlByOccupancy', 
	            'iBorderRmHlByPendingRequest',
	            'abSpShareDSForHlRmByDpPerFl_rmLabelDS', 
	            'abSpShareDSForHlRmpctForCatType_rmLabelCatDS', 
	            'abSpShareDSForHlRmpctForCatType_rmLabelCatAndTypeDS', 
	            'rmLabelByDV', 
	            'rmLabelByEm', 
	            'iBorder_abSpHlRmByAttribute_rmBorderHighlightDS', 
	            'abSpHlRmByAttribute_rmPendingRequestHighlightDS',
	            'abSpHlRmByAttribute_rmSummary1', 
	            'abSpHlRmByAttribute_rmSummary3', 
	            'abSpHlRmByAttribute_rmSummary4', 
	            'abSpHlRmByAttribute_rmSummary5', 
	            'abSpHlRmByAttribute_rmSummary6', 
	            'abSpHlRmByAttribute_rmSummary7',
	            'abSpHlRmByAttribute_rmSummary8',
	            'abSpHlRmByAttribute_rmSummaryDS1', 
	            'abSpHlRmByAttribute_rmSummaryDS3', 
	            'abSpHlRmByAttribute_rmSummaryDS4', 
	            'abSpHlRmByAttribute_rmSummaryDS5', 
	            'abSpHlRmByAttribute_rmSummary6DS', 
	            'abSpHlRmByAttribute_rmSummary7DS',
	            'abSpHlRmByAttribute_rmSummaryDS8',
	            'abSpShareDSForRmAttributeDS',
	            'roomWithActivityLogDS',
	            'abSpHlRmByAttribute_pendingRequestSelect',
	            'abSpHlRmByAttribute_flTree',
	            'iBorderRmHlByStd',
	            'rmHlByStd',
	            'rmHlByDV',
	            'rmHlByPendingRequest'];
	for ( var i = 0; i < list.length; i++) {
		var control = View.dataSources.get(list[i]);
		if (!control) {
			control = View.panels.get(list[i]);
		}
		control.addParameter('date', controller.date);
		control.addParameter('rmpctConsoleRes', controller.rmpctConsoleRes);
		control.addParameter('pendingRequestOnly', controller.pendingRequestOnly);
	}

	var ds = controller.iBorder_abSpHlRmByAttribute_rmBorderHighlightDS;
	ds.addParameter('blId', controller.blId);
	ds.addParameter('flId', controller.flId);
	ds = controller.abSpHlRmByAttribute_rmPendingRequestHighlightDS;
	ds.addParameter('blId', controller.blId);
	ds.addParameter('flId', controller.flId);
	controller.abSpHlRmByAttribute_flTree.addParameter('blConsoleRes', controller.blConsoleRes);
	var summaryGrid6 = controller.abSpHlRmByAttribute_rmSummary6;
	summaryGrid6.addParameter('Non-occupiable', '1');
	summaryGrid6.addParameter('Vacant', '2');
	summaryGrid6.addParameter('Available', '3');
	summaryGrid6.addParameter('Fully-Occupied', '4');
	summaryGrid6.addParameter('Over-Occupied', '5');
}

/**
 * show statistics grid base on the the current highlight datasource
 */
function showStatisticsGrid() {
	View.panels.get('abSpHlRmByAttribute_rmSummary1').show(false);
	View.panels.get('abSpHlRmByAttribute_rmSummary2').show(false);
	View.panels.get('abSpHlRmByAttribute_rmSummary3').show(false);
	View.panels.get('abSpHlRmByAttribute_rmSummary4').show(false);
	View.panels.get('abSpHlRmByAttribute_rmSummary5').show(false);
	View.panels.get('abSpHlRmByAttribute_rmSummary6').show(false);
	View.panels.get('abSpHlRmByAttribute_rmSummary7').show(false);
	View.panels.get('abSpHlRmByAttribute_rmSummary8').show(false);
	var grid = getGridFromDrawingHighlight(View.panels.get('abSpHlRmByAttribute_floorPlan').currentHighlightDS);
	if (grid) {
        // KB 3039077: notify the core that only one grid panel is visible
        grid.setSingleVisiblePanel(true);
		grid.show(true);
		grid.addParameter('blId', controller.blId);
		grid.addParameter('flId', controller.flId);
		grid.refresh();
	}
}

/**
 * set border highlight of the floor plan base on the border highlight option
 */
function setBorderHighlights() {
	var layoutManager = View.getLayoutManager('nested_east');
	var drawingPanel = controller.abSpHlRmByAttribute_floorPlan;
	var borderHighlightOption = controller.borderHighlightOption;
	
	if (drawingPanel.isLoadDrawing && borderHighlightOption) {
		if (layoutManager.isRegionCollapsed('south')) {
			layoutManager.expandRegion('south');
		}
		
		if(borderHighlightOption=='iBorder_abSpHlRmByAttribute_rmBorderHighlightDS'){
			highlightBorderByOccupy();
		}else{
			highlightBorderByAttribute(borderHighlightOption);
		}
	} else {
		if (!layoutManager.isRegionCollapsed('south')) {
			layoutManager.collapseRegion('south');
		}
	}
}

function highlightBorderByOccupy(){
	$('abSpHlRmByAttribute_floorPlan_border_legendDiv').style.display = '';
	$('abSpHlRmByAttribute_floorPlan_border_legend_by_attributeDiv').style.display = 'none';
}

function highlightBorderByAttribute(borderHighlightOption){
	$('abSpHlRmByAttribute_floorPlan_border_legendDiv').style.display = 'none';
	$('abSpHlRmByAttribute_floorPlan_border_legend_by_attributeDiv').style.display = '';
	createBorderHighlightAttributeLegend(borderHighlightOption);
}

function createBorderHighlightAttributeLegend(borderHighlightOption){
	var innerHTML = '';
	if(borderHighlightOption == 'iBorderRmHlByPendingRequest'){
		var grid = getGridFromDrawingHighlight(borderHighlightOption);
		
		if(grid){
			grid.addParameter('blId', controller.blId);
			grid.addParameter('flId', controller.flId);
			grid.refresh();
			grid.show(true)
			innerHTML = $('abSpHlRmByAttribute_rmSummary7').innerHTML;
		}
	}else{
		var grid = getGridFromDrawingHighlight(borderHighlightOption);
		if(grid){
			var ds = View.dataSources.get(grid.dataSourceId);
		    ds.addParameter('blId', controller.blId);
		    ds.addParameter('flId', controller.flId);
		    var records = ds.getRecords();
		    innerHTML = '<table class="panelReport">';
			for ( var i = 0; i < records.length; i++) {
				var color = 'dddddd';
				if(records[i].getValue(ds.mainTableName+'.hpattern_acad')){
					color = gAcadColorMgr.getRGBFromPatternForGrid(records[i].getValue(ds.mainTableName+'.hpattern_acad'), true);
				}
				innerHTML+='<tr class="dataRow"><td class="color" width=""><div style="width:100%;height:16px;background-color:#'+color +';"></div></td>'+
				           '<td translatable="true" class="text" width="80%">'+records[i].getValue(ds.mainTableName+'.asset_id')+'</td></tr>'
			}
			innerHTML+='</table>';
		}
		
	}
	
    
	$('abSpHlRmByAttribute_floorPlan_border_legend_by_attributeDiv').innerHTML = innerHTML;
}


/**
 * get and return correct statistic grid base on current highlight datasource
 */
function getGridFromDrawingHighlight(currentHighlightDS) {
	var grid = null;
	if (currentHighlightDS == 'abSpShareDSForHlRmByDpPerFl_rmHighlightDS'||currentHighlightDS == 'iBorder_abSpShareDSForHlRmByDpPerFl_rmHighlightDS') {
		grid = View.panels.get('abSpHlRmByAttribute_rmSummary1');
	}
	if (currentHighlightDS == 'rmHlByStd'|| currentHighlightDS == 'iBorderRmHlByStd') {
		grid = View.panels.get('abSpHlRmByAttribute_rmSummary2');
	}
	if (currentHighlightDS == 'abSpShareDSForHlRmpctForCatType_rmHighlightCatAndTypeDS'||currentHighlightDS == 'iBorder_abSpShareDSForHlRmpctForCatType_rmHighlightCatAndTypeDS') {
		grid = View.panels.get('abSpHlRmByAttribute_rmSummary3');
	}
	if (currentHighlightDS == 'abSpShareDSForHlRmpctForCatType_rmHighlightCatDS'||currentHighlightDS == 'iBorder_abSpShareDSForHlRmpctForCatType_rmHighlightCatDS') {
		grid = View.panels.get('abSpHlRmByAttribute_rmSummary4');
	}
	if (currentHighlightDS == 'rmHlByDV'||currentHighlightDS=='iBorderRmHlByDV') {
		grid = View.panels.get('abSpHlRmByAttribute_rmSummary5');
	}
	if (currentHighlightDS == 'rmHlByOccupancy'||currentHighlightDS == 'iBorder_abSpHlRmByAttribute_rmBorderHighlightDS') {
		grid = View.panels.get('abSpHlRmByAttribute_rmSummary6');
	}
	if (currentHighlightDS == 'rmHlByPendingRequest'||currentHighlightDS=='iBorderRmHlByPendingRequest') {
		grid = View.panels.get('abSpHlRmByAttribute_rmSummary7');
	}
	if (currentHighlightDS == 'rmHlBySuperCategory') {
		grid = View.panels.get('abSpHlRmByAttribute_rmSummary8');
	}

	return grid;
}

function onChangeHighlightDS(selectorControl, combo) {
	if (controller.abSpHlRmByAttribute_floorPlan.isLoadDrawing) {
		var selectedValue = combo.dom.value;
		var drawingPanel = View.panels.get('abSpHlRmByAttribute_floorPlan');
		drawingPanel.currentHighlightDS = selectedValue;
		setBorderHighlights.defer(100);
		showStatisticsGrid();
		
		controller.reloadSvg();
	}
}


/**
 * add sigle quot to given value
 */
function literal(value) {
	return "'" + value + "'"
}

/**
 * re-write the method Ab.view.DataView.prototype.renderBegin to support customized font size to keep it consistent with the form panel
 */
Ab.view.DataView.prototype.renderBegin = function() {
	this.evaluationContext = {};
	if (this.parentPanel) {
		this.evaluationContext = this.parentPanel.createEvaluationContext();
	}

	this.buffer = '<table cellspacing="0" class="dataView" style="font-size:11px;">';

	if (this.headerTemplate) {

		// if custom callback is provided, call it
		var data = {};
		if (this.getHeaderData) {
			data = this.getHeaderData();
		}

		// evaluate Ext.XTemplate {} expressions
		var html = this.headerXTemplate.apply(data);

		// evaluate WebCentral ${} expressions
		html = View.evaluateString(html, this.evaluationContext);

		this.buffer = this.buffer + html;
	}
}

/**
 * max the floor plan panel
 */
function maxFloor(){
	var layout=View.getLayoutManager('main');
	layout.collapseRegion('north');
	layout.collapseRegion('west');
	controller.abSpHlRmByAttribute_floorPlan.actions.get('max').show(false);
	controller.abSpHlRmByAttribute_floorPlan.actions.get('normal').show(true);
	View.getView('parent').defaultLayoutManager.collapseRegion('west');

	if(controller.abSpHlRmByAttribute_floorPlan.isLoadDrawing){
		resizeDrawingPanel.defer(100);
	}
}

/**
 * normal the floor plan panel
 */
function normalFloor(){
	var layout=View.getLayoutManager('main');
	layout.expandRegion('north');
	layout.expandRegion('west');
	controller.abSpHlRmByAttribute_floorPlan.actions.get('max').show(true);
	controller.abSpHlRmByAttribute_floorPlan.actions.get('normal').show(false);
	View.getView('parent').defaultLayoutManager.expandRegion('west');

	if(controller.abSpHlRmByAttribute_floorPlan.isLoadDrawing){
		resizeDrawingPanel.defer(100);
	}
}

/**
 * reset the size of the drawing panel
 */
function resizeDrawingPanel(){
//	FABridge.abDrawing.root().autoScale(false);
}

/**
 * onload event handler of the drawing panel
 */
function onLoadHandler() {
	// Construct the right click menu items
	var item0 = {
		title : getMessage('editRoom'),
		handler : "editRoom"
	};
	var item1 = {
		title : getMessage('shareRoom'),
		handler : "shareRoom"
	};
	var item3 = {
		title : getMessage('editRequest'),
		handler : "editRequest"
	};
	var items = [item0, item1, item3];

	View.panels.get('abSpHlRmByAttribute_floorPlan').setRightClickMenu(items);
}

/**
 * edit room menu event handler of the drawing right click menu
 */
function editRoom() {
	if(ob.length > 0){
		//KB3037857 - set refresh restriction for pop up define view to parent view to avoid load error when user group is 'REV' 
		View.editRoomRestriction = createRestrictionFromClickObject(ob);
		View.openDialog('ab-sp-def-loc-rm.axvw', null, true, {
	        width: 1000,
	        height: 530
	    });
	} 
}

/**
 * share room menu event handler of the drawing right click menu
 */
function shareRoom() {
	if(ob.length > 0){
		View.dateINConsole = controller.date;
		View.openDialog('ab-sp-alloc-pct.axvw', createRestrictionFromClickObject(ob), true, {
	        width: 1000,
	        height: 530
	    });
	} 
}

/**
 * edit Request menu event handler of the drawing right click menu
 */
function editRequest() {
	if(ob.length > 0){
		var restriction = createRestrictionFromClickObject(ob);
		var ds = View.dataSources.get('iBorderRmHlByPendingRequest');
		var record = ds.getRecord(restriction);
		if(record.getValue('rm.hpattern_acad')!=''){
			ds = View.dataSources.get('roomWithActivityLogDS');
			var records = ds.getRecords(restriction);
			//below remove repeat 'activity_log_id' record form records.
			var activityLogIds = [];
			for(var i = 0; i<records.length; i++){
				var activity_log_id = records[i].getValue('rm.activity_log_id');
				var flag = true;
				for(var j = 0; j<activityLogIds.length; j++){
					if(activity_log_id==activityLogIds[j]){
						flag = false;
					}
				}
				if(flag){
					activityLogIds.push(activity_log_id);
				}
			}
			if(activityLogIds.length>0){
				if(activityLogIds.length == 1){
					openIssueView(records[0]);
				}else{
					var grid = controller.abSpHlRmByAttribute_pendingRequestSelect;
					grid.refresh(restriction);
					grid.showInWindow({
						width: 500, 
						height: 300
					});
				}
			}else{
				View.showMessage(getMessage('notEditPendingRequest'));
			}
		}else{
			View.showMessage(getMessage('noPendingRequest'));
		}
	} 
}

function selectPendingRequestEdit(){
	var grid = controller.abSpHlRmByAttribute_pendingRequestSelect;
	var record = grid.gridRows.get(grid.selectedRowIndex).getRecord();
	grid.closeWindow();
	openIssueView(record);
}

function openIssueView(record){
	if(record.getValue('rm.status')=='0'){
		View.showMessage(getMessage('requestedPendingRequest'));
		return;
	}else{
		var activityLogrestriction = new Ab.view.Restriction();
		activityLogrestriction.addClause("activity_log.activity_log_id", record.getValue('rm.activity_log_id'), "=");
		ds = View.dataSources.get('activityLogDS');
		View.activityType = ds.getRecord(activityLogrestriction).getValue('activity_log.activity_type');
		
		View.openDialog('ab-helpdesk-request-update-tabs.axvw', activityLogrestriction, true, {
	        width: 1000,
	        height: 530
	    });
	}
}

/**
 * create restriction from the click event object
 */
function createRestrictionFromClickObject(ob){
	var restriction = new Ab.view.Restriction();
	restriction.addClause("rm.bl_id", ob[0], "=", true);
	restriction.addClause("rm.fl_id", ob[1], "=", true);
	restriction.addClause("rm.rm_id", ob[2], "=", true);
	return restriction;
	
}