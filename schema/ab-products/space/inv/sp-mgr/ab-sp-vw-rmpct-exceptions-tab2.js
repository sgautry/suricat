/**
 * @author Lei
 */

var abSpVwRmpctExceptionsController2 = View.createController('abSpVwRmpctExceptionsController2', {
	filterBlId:'',
	filterFlId:'',
	optionIndex:0,
	sqlParam:'',
	
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
    afterViewLoad: function(){
        
		 //initialize svg control
		this.parameters = new Ab.view.ConfigObject();
		this.parameters['highlightParameters'] = [{'view_file':"ab-sp-vw-rmpct-exceptions-tab2.axvw", 'hs_ds':"highlightDS1", 'label_ds':'labelDS1'}];
		this.parameters['bordersHighlightSelector'] = 'true'; 
		this.parameters['highlightFilterSelector'] = 'true';
		this.parameters['allowMultipleDrawings'] = 'false';
		this.parameters['divId'] = "svgDiv";
		this.parameters['orderByColumn'] = true;
		this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
		this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'DatasourceSelector': {panelId: "abSpVwRmpctExceptionsDrawingPanel"},
				'AssetLocator': {divId: "svgDiv"},
				'AssetTooltip': {handlers: [{assetType: 'rm', datasource: 'labelDS1', fields: 'rm.rm_id;rm.dv_id;rm.dp_id;rm.prorate'}]},
				'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
		this.parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpVwRmpctExceptionsDrawingPanel", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
   },
   
	   
	/**
	 * After data fetch
	 */
	afterInitialDataFetch: function(){
		$('abSpVwRmpctExceptionsConsole_field_gen1_labelCell').style.display='none';
		$('allocatedType').style.display='none';
		getExceptionsList();
    },
    abSpVwRmpctExceptionsConsole_afterRefresh : function(){
    	this.abSpVwRmpctExceptionsConsole.clear();
    },
    /**
     * Show tree by console clauses
     */
    abSpVwRmpctExceptionsConsole_onShow: function(){
    	showTreeByExceptionType();
    	
        this.filterBlId = this.abSpVwRmpctExceptionsConsole.getFieldValue('rm.bl_id');
        this.filterFlId = this.abSpVwRmpctExceptionsConsole.getFieldValue('rm.fl_id');
        
        refreshTree();
    },
    
	loadSvg: function(bl_id, fl_id, drawingName) {
		
		addParameterConfig(this.parameters);
		
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgName = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
        
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
    	
   	},
   	
   	unloadSvg: function(){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
    	this.svgControl.unload(parameters);

   	},
   	
    onClickAsset: function(params, drawingController){
    	var arrayRoomIDs = params['assetId'].split(";");
    	var restriction = new Ab.view.Restriction();
        restriction.addClause("rm.bl_id", arrayRoomIDs[0], "=", true);
        restriction.addClause("rm.fl_id", arrayRoomIDs[1], "=", true);
        restriction.addClause("rm.rm_id", arrayRoomIDs[2], "=", true);
         
        var rmDetailPanel = View.panels.get('abSpHlComnRm_RmDetailPanel');
        rmDetailPanel.refresh(restriction);
        rmDetailPanel.show(true);
        rmDetailPanel.showInWindow({
        	width: 500,
        	height: 250
        });
         
        // toggle the room if clicked
    	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId']);
    }
});

/**
 * Refresh tree and clear drawing panel
 */
function refreshTree(){
	var c=abSpVwRmpctExceptionsController2;
	var treePanel=c.abSpVwRmpctExceptionsTree;
    
    if (c.filterBlId) {
    	treePanel.addParameter('blId', " = " + "'" + c.filterBlId + "'");
    }
    else {
    	treePanel.addParameter('blId', "IS NOT NULL");
    }
    
    if (c.filterFlId) {
    	treePanel.addParameter('flId', "= " + "'" + c.filterFlId + "'  ");
    }
    else {
    	treePanel.addParameter('flId', "IS NOT NULL");
    }
    if (c.sqlParam) {
    	treePanel.addParameter('sqlParam', c.sqlParam);
    }
    
    treePanel.refresh();
    
    c.unloadSvg();
    c.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
    c.abSpVwRmpctExceptionsDrawingPanel.lastLoadedBldgFloor = null;
    c.abSpVwRmpctExceptionsTab2RmDetail.clear();
}



/**
 * Show Exceptions By Exception Type
 */
function showTreeByExceptionType(fromWhere){
	var c=abSpVwRmpctExceptionsController2;
	c.filterBlId = c.abSpVwRmpctExceptionsConsole.getFieldValue('rm.bl_id');
	c.filterFlId = c.abSpVwRmpctExceptionsConsole.getFieldValue('rm.fl_id');
	
	var itemSelect = $('exceptions');
	var exceptionsValue=itemSelect.value;
	
	var itemSelect2 = $('allocatedType');
	var value2=itemSelect2.value;
	
	c.optionIndex=getOptionIndex(itemSelect,exceptionsValue);
	
		//refreshReportByException(c,optionIndex,bl_id,fl_id);

	if(c.optionIndex==8){
		if(fromWhere==0){
			$('abSpVwRmpctExceptionsConsole_field_gen1_labelCell').style.display='';
			$('allocatedType').style.display='';
			getAllocationTypeList();
		}
		var optionIndex2=getOptionIndex(itemSelect2,value2);
		if(optionIndex2==0){
			c.optionIndex=80;
		}else if(optionIndex2==1){
			c.optionIndex=81;
		}else{
			c.optionIndex=82;
		}
		//load list
	}else{
		$('abSpVwRmpctExceptionsConsole_field_gen1_labelCell').style.display='none';
		$('allocatedType').style.display='none';
	}
	
	c.sqlParam=getParamSql(c.optionIndex);
	setParameterForPanel('tree', c.optionIndex);
	
	
	
	
}

/**
 * panelType is tree|drawing
 */
function setParameterForPanel(panelType,optionIndex){
	var c=abSpVwRmpctExceptionsController2;
	
	if(panelType=='tree'){
		refreshTree();
        
	}else if(panelType=='drawing'){
		c.highlightDS1.addParameter('sqlParam', c.sqlParam);
		c.abSpVwRmpctExceptionsTab2RmDetail.addParameter('sqlParam',c.sqlParam);
	}
}

/**
 * event handler when click the floor level of the tree
 */
function onFlTreeClick(ob){
	var c=abSpVwRmpctExceptionsController2;
    var drawingPanel = View.panels.get('abSpVwRmpctExceptionsDrawingPanel');
    var currentNode = View.panels.get('abSpVwRmpctExceptionsTree').lastNodeClicked;
    var blId = currentNode.parent.data['bl.bl_id'];
    var flId = currentNode.data['fl.fl_id'];
    var highlightResc = "";
    setParameterForPanel('drawing', abSpVwRmpctExceptionsController2.optionIndex);
    var title = String.format(getMessage('drawingPanelTitle2'), blId + '-' + flId);
    displayFloor(drawingPanel, currentNode, title);
    
   var res = new Ab.view.Restriction();
	res.addClauses(ob.restriction);
	var optionIndex=abSpVwRmpctExceptionsController2.optionIndex;
	//var panelArr=[c.rmpctReport1,c.rmpctReport2,c.rmpctReport3,c.rmpctReport4,c.rmpctReport5,c.rmpctReport6,c.rmpctReport7,c.rmpctReport8,c.rmpctReport9];
	var panelArr=[c.rmpctReport1,c.rmpctReport2,c.rmpctReport3,c.abSpVwRmpctExceptionsTab2RmDetail];
	
	for(var i=0;i<panelArr.length;i++){
		panelArr[i].show(false);
	}
	
	if(optionIndex==0){
		c.rmpctReport1.refresh(res,true);
	}else if(optionIndex==1){
		c.rmpctReport2.refresh(res,true);
	}else if(optionIndex==2){
		c.rmpctReport3.refresh(res,true);
	}else {
		View.panels.get('abSpVwRmpctExceptionsTab2RmDetail').refresh(res);
	}
	
}



/**
 * display floor drawing for highlight report
 * @param {Object} drawingPanel
 * @param {Object} res
 * @param {String} title
 */
function displayFloor(drawingPanel, currentNode, title){
    var blId = getValueFromTreeNode(currentNode, 'bl.bl_id');
    var flId = getValueFromTreeNode(currentNode, 'fl.fl_id');
    var dwgName = getValueFromTreeNode(currentNode, 'fl.dwgname');
    abSpVwRmpctExceptionsController2.loadSvg(blId, flId, dwgName);
    abSpVwRmpctExceptionsController2.svgControl.getAddOn('InfoWindow').setText(title);
    
}

/**
 * get value from tree node
 * @param {Object} treeNode
 * @param {String} fieldName
 */
function getValueFromTreeNode(treeNode, fieldName){
    var value = null;
    if (treeNode.data[fieldName]) {
        value = treeNode.data[fieldName];
        return value;
    }
    if (treeNode.parent.data[fieldName]) {
        value = treeNode.parent.data[fieldName];
        return value;
    }
    if (treeNode.parent.parent.data[fieldName]) {
        value = treeNode.parent.parent.data[fieldName];
        return value;
    }
    if (treeNode.parent.parent.parent.data[fieldName]) {
        value = treeNode.parent.parent.parent.data[fieldName];
        return value;
    }
    return value;
}

