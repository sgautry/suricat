/**
 * @author Guo
 */
var controller = View.createController('hlDpRmPerFl_Controller', {

	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
    //----------------event handle--------------------
    afterViewLoad: function(){
//        this.abSpHlDpRmPerFl_DrawingPanel.appendInstruction("default", "", getMessage('drawingPanelTitle1'));
//        this.abSpHlDpRmPerFl_DrawingPanel.addEventListener('onclick', onClickDrawingHandler);
    	
        //initialize svg control
		this.parameters = new Ab.view.ConfigObject();
		this.parameters['highlightParameters'] = [{'view_file':"ab-sp-hl-dp-rm-per-fl.axvw", 'hs_ds': "ds_ab-sp-hl-dp-rm-per-fl_drawing_rmHighlight", 'label_ds':'ds_ab-sp-hl-dp-rm-per-fl_drawing_rmLabel'}];
		this.parameters['bordersHighlightSelector'] = 'true'; 
		this.parameters['highlightFilterSelector'] = 'true';
		this.parameters['allowMultipleDrawings'] = 'false';
		this.parameters['divId'] = "svgDiv";
		this.parameters['orderByColumn'] = true;
		this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
		this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'DatasourceSelector': {panelId: "abSpHlDpRmPerFl_DrawingPanel"},
				'AssetLocator': {divId: "svgDiv"},
				'AssetTooltip': {handlers: [{assetType: 'rm', datasource: 'ds_ab-sp-hl-dp-rm-per-fl_drawing_rmLabel', fields: 'rm.rm_id;rm.dv_id;rm.dp_id;rm.area'}]},
				'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
		this.parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpHlDpRmPerFl_DrawingPanel", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));    	
    },
    
    abSpHlDpRmPerFl_SumGrid_afterRefresh: function(){
        //set color for every row according the drawing
        resetColorFieldValue('abSpHlDpRmPerFl_SumGrid', 'abSpHlDpRmPerFl_DrawingPanel', 'dp.dp_id', 'dp.hpattern_acad', 'abSpHlDpRmPerFl_SumGrid_legend');
    },
    
	loadSvg: function(bl_id, fl_id, drawingName) {
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgName = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
        
		//svgControl object used in ab-sp-common.js
		svgControl = this.svgControl;
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
    	
    	this.svgControl.getAddOn('InfoWindow').setText(String.format(getMessage('drawingPanelTitle2'), this.blId + "-" + this.flId));
   	},
   	
   	unloadSvg: function(){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
    	this.svgControl.unload(parameters);
    	
   	},
   	
    onClickAsset: function(params, drawingController){
    	var arrayRoomIDs = params['assetId'].split(";");
    	var restriction = new Ab.view.Restriction();
        restriction.addClause("rm.bl_id", arrayRoomIDs[0], "=", true);
        restriction.addClause("rm.fl_id", arrayRoomIDs[1], "=", true);
        restriction.addClause("rm.rm_id", arrayRoomIDs[2], "=", true);
         
        var rmDetailPanel = View.panels.get('abSpHlDpRmPerFl_RmDetailPanel');
        rmDetailPanel.refresh(restriction);
        rmDetailPanel.show(true);
        rmDetailPanel.showInWindow({
            width: 500,
            height: 250
        });
         
        // toggle the room if clicked
    	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId']);
    },
    
    abSpHlDpRmPerFl_filterConsole_onShowTree: function(){
        this.unloadSvg();
        this.closeDetailPanel();
        this.abSpHlDpRmPerFl_SumGrid.clear();
        this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
    },
    
    closeDetailPanel: function(){
    	var rmDetailPanel = View.panels.get('abSpHlDpRmPerFl_RmDetailPanel');
        rmDetailPanel.closeWindow();
    }
    
});

/**
 * event handler when click the floor level of the tree
 * @param {Object} ob
 */
function onFlTreeClick(ob){
    var drawingPanel = View.panels.get('abSpHlDpRmPerFl_DrawingPanel');
    var currentNode = View.panels.get('abSpHlDpRmPerFl_BlTree').lastNodeClicked;
    var blId = currentNode.parent.data['bl.bl_id'];
    var flId = currentNode.data['fl.fl_id'];
    var dvId = View.user.employee.organization.divisionId;
    var dpId = View.user.employee.organization.departmentId;
    var title = String.format(getMessage('drawingPanelTitle2'), blId + '-' + flId, dvId + "-" + dpId);
    displayFloor(drawingPanel, currentNode, title);
    
    var restriction = new Ab.view.Restriction();
    restriction.addClause("rm.bl_id", blId, "=");
    restriction.addClause("rm.fl_id", flId, "=");
    restriction.addClause("rm.dwgname", currentNode.data['fl.dwgname'], "=");
    View.panels.get('abSpHlDpRmPerFl_SumGrid').refresh(restriction);
}
