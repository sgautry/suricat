var controller = View.createController('abSpAsgnUserDpToRm_Control', {

	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	selectableOpt: {}, 
	
    afterViewLoad: function(){
        dvId = View.user.employee.organization.divisionId;
        dpId = View.user.employee.organization.departmentId;
        
    	//initialize svg control
        this.parameters = new Ab.view.ConfigObject();
        this.parameters['highlightParameters'] = [{'view_file':"ab-sp-asgn-user-dp-to-rm.axvw", 'hs_ds': "ds_ab-sp-asgn-user-dp-to-rm_drawing_rmHighlight", 'label_ds':'ds_ab-sp-asgn-user-dp-to-rm_drawing_rmLabel'}];
        this.parameters['bordersHighlightSelector'] = 'true'; 
        this.parameters['highlightFilterSelector'] = 'true';
        this.parameters['allowMultipleDrawings'] = 'false';
        this.parameters['divId'] = "svgDiv";
        this.parameters['orderByColumn'] = true;
        this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
        this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
        		'DatasourceSelector': {panelId: "abSpAsgnUserDpToRm_drawingPanel"},
        		'AssetLocator': {divId: "svgDiv"},
        		'AssetTooltip': {handlers: [{assetType: 'rm', datasource: 'ds_ab-sp-asgn-user-dp-to-rm_drawing_rmLabel', fields: 'rm.rm_id;rm.dv_id;rm.dp_id'}]},
        		'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
        this.parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpAsgnUserDpToRm_drawingPanel", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('selectFloor'));
		
    	var selectController = this.svgControl.drawingController.getController("SelectController");
    	//enable diagonal border selection 
    	selectController.setDiagonalSelectionPattern(true);
    	//set diagonal selection border size (default is 20)
    	selectController.setDiagonalSelectionBorderWidth(40);
    	
    	//enable Multiple Selection
    	selectController.setMultipleSelection(true);
		
    },
        
    abSpAsgnUserDpToRm_filterConsole_onShowFlTree: function(){
        var blId = this.abSpAsgnUserDpToRm_filterConsole.getFieldValue("rm.bl_id");
        var flId = this.abSpAsgnUserDpToRm_filterConsole.getFieldValue("rm.fl_id");
        var blRes = "bl.bl_id IS NOT NULL";
        var flRes = "fl.fl_id IS NOT NULL";
        if (blId && flId) {
            blRes = "bl.bl_id ='" + blId + "' AND EXISTS (SELECT 1 FROM fl WHERE fl.bl_id = bl.bl_id AND fl.fl_id ='" + flId + "')";
            flRes = "fl.fl_id ='" + flId + "'";
        }
        if (blId && !flId) {
            blRes = "bl.bl_id ='" + blId + "'";
        }
        if (!blId && flId) {
            blRes = "EXISTS (SELECT 1 FROM fl WHERE fl.bl_id = bl.bl_id AND fl.fl_id ='" + flId + "')";
            flRes = "fl.fl_id ='" + flId + "'";
        }
        this.abSpAsgnUserDpToRm_blTree.addParameter('blRes', blRes);
        this.abSpAsgnUserDpToRm_blTree.addParameter('flRes', flRes);
        this.abSpAsgnUserDpToRm_blTree.refresh();
        this.unloadSvg();
        this.svgControl.getAddOn('InfoWindow').setText(getMessage('selectFloor'));
        this.abSpAsgnUserDpToRm_dpAssignGrid.removeRows(0);
        this.abSpAsgnUserDpToRm_dpAssignGrid.update();
        resetAssignmentGrid();
        
        dvId = dpId = "";
    },

    abSpAsgnUserDpToRm_dpAssignGrid_onClaim: function(){
        saveChangesByActionType('claim');
    },
    
    abSpAsgnUserDpToRm_dpAssignGrid_onRelease: function(){
        saveChangesByActionType('release');
    },

	loadSvg: function(bl_id, fl_id, drawingName) {
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgName = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
        
		//svgControl object used in ab-sp-common.js
		svgControl = this.svgControl;
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
    	
   	},
   	
   	unloadSvg: function(){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
    	this.svgControl.unload(parameters);
    	
   	},
 
    reloadSvg: function(){
    	this.svgControl.drawingController.getControl().reload(this.parameters);
    },
   
    onClickAsset: function(params, drawingController){
    	
    	var selectable = controller.selectableOpt[params['assetId']];
    	if(!selectable||!dvId||!dpId)
    		return;
    	
    	var selected = isRoomSelected(params, drawingController);
    	var arrayRoomIDs = params['assetId'].split(";");
        
    	// toggle the room if clicked
    	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId']);
    	
    	var grid = View.panels.get("abSpAsgnUserDpToRm_dpAssignGrid");
        var releaseAction = grid.actions.get('release');
        var claimAction = grid.actions.get('claim');
        if (selected && !actionType) {
            var ds = View.dataSources.get('ds_ab-sp-asgn-user-dp-to-rm_drawing_rmHighlight');
            var roomDvId = getDbRoomVal(ds, arrayRoomIDs[0], arrayRoomIDs[1], arrayRoomIDs[2], 'rm.dv_id');
            if (roomDvId) {
                releaseAction.show(true);
                claimAction.show(false);
                actionType = 'release';
            } else {
                releaseAction.show(false);  
                claimAction.show(true);
                actionType = 'claim';
            } 
            setSelectabilityByActionType();
        }
        drawingRoomClickHandler(arrayRoomIDs, selected, grid, 'rm.dv_id', dvId, 'rm.dp_id', dpId);
        if (actionType && grid.rows.length == 0) {
            resetAssignmentGrid();
        }
        
        controller.svgControl.getAddOn('InfoWindow').setText(getMessage('selectRm'));

    }
    
});

function isRoomSelected(params, drawingController){
	var selectedAssets = drawingController.getController("SelectController").selectedAssets[params.svgId];
	if(valueExistsNotEmpty(selectedAssets)){
		for(var i=0;i<selectedAssets.length;i++)
		if(selectedAssets[i] == params.assetId)
		{
			return false;
		}
	}
	return true;
}

var dvId;
var dpId;
var blId = "";
var flId = "";
var actionType = null;

/**
 * event handler when click tree node of floor level for the tree abSpAsgnUserDpToRm_blTree.
 * @param {Object} ob
 */
function onFlTreeClick(ob){
    var currentNode = View.panels.get('abSpAsgnUserDpToRm_blTree').lastNodeClicked;
    var drawingPanel = View.panels.get('abSpAsgnUserDpToRm_drawingPanel');
    var grid = View.panels.get('abSpAsgnUserDpToRm_dpAssignGrid');
    flTreeClickHandler(currentNode, drawingPanel, grid);
    blId = ob.restriction.clauses[0].value;
    flId = ob.restriction.clauses[1].value;
    resetAssignmentGrid();
    disableClickingOnGreyRooms();
    controller.svgControl.getAddOn('InfoWindow').setText(getMessage('selectRm'));
}

/**
 * reset the assignment grid panel hidden the actions.
 */
function resetAssignmentGrid(){
    var grid = View.panels.get("abSpAsgnUserDpToRm_dpAssignGrid");
    actionType = null;
    grid.actions.get('release').show(false);
    grid.actions.get('claim').show(false);
    
    setSelectabilityByActionType();
}

/**
 * set selection ability by actionType .
 */
function setSelectabilityByActionType(){
    var ds = View.dataSources.get('ds_ab-sp-asgn-user-dp-to-rm_drawing_rmHighlight');
    var res = " AND rm.bl_id='" + blId + "' AND rm.fl_id='" + flId + "'"
    var claimRecords = ds.getRecords('rm.dv_id IS NULL AND rm.dp_id IS NULL' + res);
    var releaseRecords = ds.getRecords('rm.dv_id IS NOT NULL AND rm.dp_id IS NOT NULL' + res);
    var isClaim = true;
    var isRelease = true;
    if (actionType == 'claim') {
        isRelease = false;
    }
    if (actionType == 'release') {
        isClaim = false;
    }
    setSelectability(claimRecords, isClaim);
    setSelectability(releaseRecords, isRelease);
}


/**
 * set selection ability.
 * @param {Array} records
 * @param {boolean} iSelectable
 */
function setSelectability(records, iSelectable) {
    var assignedAssetIds = getAssignedAssetIds();
    for (var i = 0; i < records.length; i++) {
        var record = records[i];
        var blId = record.getValue('rm.bl_id');
        var flId = record.getValue('rm.fl_id');
        var rmId = record.getValue('rm.rm_id');
        var assetId = blId + ';' + flId + ';' + rmId;
        
        controller.selectableOpt[assetId] = iSelectable;
        //Standard Archibus 23.2 bug fix
        //var svgId = 'svgDiv-'+ blId.toLowerCase()+flId +'-svg';
        var svgId = '';
        if(iSelectable&&assignedAssetIds.indexOf(assetId)<0){
        	//do not highlight already assigned assets
        	controller.svgControl.getDrawingController().getController("HighlightController").highlightAsset(assetId, {svgId: svgId, color: 'yellow', persistFill: true, overwriteFill: true});
    		
        }
    }
}

/**
 * get assigned assets
 * @param {String} assetId 
 */
function getAssignedAssetIds(){
	var highlightDs = View.dataSources.get('ds_ab-sp-asgn-user-dp-to-rm_drawing_rmHighlight');
    var res = " AND rm.bl_id='" + this.blId + "' AND rm.fl_id='" + this.flId + "'"
    var assginedRecords = highlightDs.getRecords('rm.dv_id IS NOT NULL AND rm.dp_id IS NOT NULL' + res);
    var assetIds = [];
    for (var i = 0; i < assginedRecords.length; i++) {
    	var blId = assginedRecords[i].getValue('rm.bl_id');
        var flId = assginedRecords[i].getValue('rm.fl_id');
        var rmId = assginedRecords[i].getValue('rm.rm_id');
        var assetId = blId + ';' + flId + ';' + rmId;
        assetIds.push(assetId);
    }
    return assetIds.join(",");
}

/**
 * save changes according action type
 * @param {String} actionType 'claim' or 'release'
 */
function saveChangesByActionType(actionType){
    var dsChanges = View.dataSources.get("ds_ab-sp-asgn-user-dp-to-rm_drawing_rmLabel");
    var drawingPanel = View.panels.get('abSpAsgnUserDpToRm_drawingPanel');
    var grid = View.panels.get("abSpAsgnUserDpToRm_dpAssignGrid");
    if (actionType == 'release') {
        for (i = 0; i < grid.gridRows.length; i++) {
            var record = grid.gridRows.items[i];
            record.setFieldValue('rm.dv_id', '');
            record.setFieldValue('rm.dp_id', '');
        }
    }
    saveChange(drawingPanel, grid, dsChanges, ['rm.dv_id', 'rm.dp_id'], false);
    resetAssignmentGrid();
    controller.svgControl.getAddOn('InfoWindow').setText(getMessage('selectRm'));
    disableClickingOnGreyRooms();
}

/**
 * Prohibit users from clicking on the rooms which don't belong to the users.
 */
function disableClickingOnGreyRooms() {
	var dsNotClicked = View.dataSources.get('ds_ab-sp-asgn-user-dp-to-rm_drawing_rmNotClicked');
	var dsCanClicked = View.dataSources.get('ds_ab-sp-asgn-user-dp-to-rm_drawing_rmCanClicked');
	var res = " rm.bl_id='" + blId + "' AND rm.fl_id='" + flId + "'";
	var cannotClickedRecords = dsNotClicked.getRecords(res);
	var canClickedRecords = dsCanClicked.getRecords(res);
	setSelectability(cannotClickedRecords, false);
	setSelectability(canClickedRecords, true);
	
}
