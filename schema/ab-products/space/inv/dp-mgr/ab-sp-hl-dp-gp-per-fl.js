/**
 * @author Guo
 */
var controller = View.createController('hlDpGpPerFl_Controller', {
	
	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
    //----------------event handle--------------------
    afterViewLoad: function(){
    	
    	//initialize svg control
        this.parameters = new Ab.view.ConfigObject();
        this.parameters['highlightParameters'] = [{'view_file':"ab-sp-hl-dp-gp-per-fl.axvw", 'hs_ds': "ds_ab-sp-hl-dp-gp-per-fl_drawing_gpHighlight", 'label_ds':'ds_ab-sp-hl-dp-gp-per-fl_drawing_gpLabel'}];
        this.parameters['bordersHighlightSelector'] = 'true'; 
        this.parameters['highlightFilterSelector'] = 'true';
        this.parameters['allowMultipleDrawings'] = 'false';
        this.parameters['divId'] = "svgDiv";
        this.parameters['orderByColumn'] = true;
        this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
        this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
        		'DatasourceSelector': {panelId: "abSpHlDpGpPerFl_DrawingPanel"},
        		'AssetLocator': {divId: "svgDiv"},
        		'AssetTooltip': {handlers: [{assetType: 'gp', datasource: 'ds_ab-sp-hl-dp-gp-per-fl_drawing_gpLabel', fields: 'gp.gp_id;gp.dv_id;gp.dp_id;gp.area'}]},
        		'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
        this.parameters['events'] = [{'eventName': 'click', 'assetType' : 'gp', 'handler' : this.onClickAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpHlDpGpPerFl_DrawingPanel", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
		
    },
    

    abSpHlDpGpPerFl_SumGrid_afterRefresh: function(){
        //set color for every row according the drawing
        resetColorFieldValue('abSpHlDpGpPerFl_SumGrid', 'abSpHlDpGpPerFl_DrawingPanel', 'dp.dp_id', 'dp.hpattern_acad', 'abSpHlDpGpPerFl_SumGrid_legend');
    },
    
	loadSvg: function(bl_id, fl_id, drawingName) {
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgName = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
        
		//svgControl object used in ab-sp-common.js
		svgControl = this.svgControl;
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
    	
   	},
   	
   	unloadSvg: function(){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
    	this.svgControl.unload(parameters);
    	
   	},
   	
    onClickAsset: function(params, drawingController){
        var restriction = new Ab.view.Restriction();
        restriction.addClause("gp.gp_id", params['assetId'], "=", true);
        
        var gpDetailPanel = View.panels.get('abSpHlDpGpPerFl_GpDetailPanel');
        gpDetailPanel.refresh(restriction);
        gpDetailPanel.show(true);
        gpDetailPanel.showInWindow({
            width: 500,
            height: 250
        });
        
        // toggle the room if clicked
    	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId']);
    },
    
    closeDetailPanel: function(){
        var gpDetailPanel = View.panels.get('abSpHlDpGpPerFl_GpDetailPanel');
        gpDetailPanel.closeWindow();
    }
});

/**
 * event handler when click the floor level of the tree
 * @param {Object} ob
 */
function onFlTreeClick(ob){
    var drawingPanel = View.panels.get('abSpHlDpGpPerFl_DrawingPanel');
    var currentNode = View.panels.get('abSpHlDpGpPerFl_BlTree').lastNodeClicked;
    var blId = currentNode.parent.data['bl.bl_id'];
    var flId = currentNode.data['fl.fl_id'];
    var dvId = View.user.employee.organization.divisionId;
    var dpId = View.user.employee.organization.departmentId;
    var title = String.format(getMessage('drawingPanelTitle2'), blId + '-' + flId, dvId + "-" + dpId);
    displayFloor(drawingPanel, currentNode, title);
    
    var restriction = new Ab.view.Restriction();
    restriction.addClause("gp.bl_id", blId, "=");
    restriction.addClause("gp.fl_id", flId, "=");
    restriction.addClause("gp.dwgname", currentNode.data['fl.dwgname'], "=");
    View.panels.get('abSpHlDpGpPerFl_SumGrid').refresh(restriction);
}
