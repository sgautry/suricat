/**
 * @author Guo
 */
var controller = View.createController('abSpHlDpRmPerFlTrans_Controller', {

	blConsoleRes : '1=1',
	rmpctConsoleRes : "",
	date : '',
	dateToAddField:'rmpct.date_start',
	dateToUpdateField:'rmpct.date_start',
	console:null,
	timeLine:null,
 
	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgName:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
	// ----------------event handle--------------------
	afterViewLoad : function() {
		
		this.rmpctConsoleRes = " rmpct.dv_id = '"+View.user.employee.organization.divisionId+"'  AND rmpct.dp_id='"+View.user.employee.organization.departmentId+"' ";
		
		//initialize svg control
		this.parameters = new Ab.view.ConfigObject();
		this.parameters['highlightParameters'] = [{'view_file':"ab-sp-share-ds-for-hl-rmpct-by-dp-per-fl.axvw", 'hs_ds': "abSpShareDSForHlRmByDpPerFl_rmHighlightDS", 'label_ds':'abSpShareDSForHlRmByDpPerFl_rmLabelDS'}];
		this.parameters['bordersHighlightSelector'] = 'true'; 
		this.parameters['highlightFilterSelector'] = 'true';
		this.parameters['allowMultipleDrawings'] = 'false';
		this.parameters['divId'] = "svgDiv";
		this.parameters['orderByColumn'] = true;
		this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
		this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'DatasourceSelector': {panelId: "abSpHlDpRmPerFlTrans_floorPlan"},
				'AssetLocator': {divId: "svgDiv"},
				'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpHlDpRmPerFlTrans_floorPlan", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
	},
	    

	afterInitialDataFetch : function() {
		this.date = getCurrentDate();
		this.abSpHlDpRmPerFlTrans_Console.setFieldValue('rmpct.date_start', this.date);
		this.setParameters();
		this.console=this.abSpHlDpRmPerFlTrans_Console;
		this.timeLine=this.timeLineButton;
		this.abSpHlDpRmPerFlTrans_FlTree.refresh();
		this.dateAction = this.timeLine.actions.get('currentDate');
		this.dateAction.setTitle(this.console.getFieldElement(this.dateToAddField).value);
		this.date=this.abSpHlDpRmPerFlTrans_Console.getFieldValue('rmpct.date_start');
		setTimeTitle(this.timeLine);
	},

	abSpHlDpRmPerFlTrans_summaryGrid_afterRefresh : function() {
		this.date = getCurrentDate();
		// set color for every row according the drawing
		resetColorFieldValue('abSpHlDpRmPerFlTrans_summaryGrid', 'abSpHlDpRmPerFlTrans_floorPlan', 'dp.dp_id', 'dp.hpattern_acad', 'abSpHlDpRmPerFlTrans_summaryGrid_legend');
	},

	abSpHlDpRmPerFlTrans_Console_onShowTree : function() {
		var blId = this.abSpHlDpRmPerFlTrans_Console.getFieldValue('rmpct.bl_id');
		this.blConsoleRes = '1=1';

		if (blId) {
			this.blConsoleRes = "bl.bl_id ='" + blId+"'"
		}
		this.resetHighlights();
		this.abSpHlDpRmPerFlTrans_FlTree.refresh();
		this.dateAction = this.timeLine.actions.get('currentDate');
		this.dateAction.setTitle(this.date);
		this.date=this.console.getFieldValue('rmpct.date_start');
	},
	/**
	 * set panel and datasource parameters value
	 */
	setParameters : function() {
		this.date=this.abSpHlDpRmPerFlTrans_Console.getFieldValue('rmpct.date_start')
		var grid = this.abSpHlDpRmPerFlTrans_summaryGrid;
		grid.addParameter('blId', this.blId);
		grid.addParameter('flId', this.flId);
		grid.addParameter('rmpctConsoleRes', this.rmpctConsoleRes);
		grid.addParameter('date', this.date );
		this.abSpHlDpRmPerFlTrans_FlTree.addParameter('date', this.date);
		this.abSpHlDpRmPerFlTrans_FlTree.addParameter('blConsoleRes', this.blConsoleRes);
		this.abSpShareDSForHlRmByDpPerFl_rmHighlightDS.addParameter('rmpctConsoleRes', this.rmpctConsoleRes);
		this.abSpShareDSForHlRmByDpPerFl_rmHighlightDS.addParameter('date', this.date);
		this.abSpShareDSForHlRmByDpPerFl_rmLabelDS.addParameter('rmpctConsoleRes', this.rmpctConsoleRes);
		this.abSpShareDSForHlRmByDpPerFl_rmLabelDS.addParameter('date', this.date);
	},
	resetHighlights : function() {
		this.setParameters();
		if (this.svgControl.drawingController.isDrawingsLoaded) {
			this.reloadSvg();
			this.abSpHlDpRmPerFlTrans_summaryGrid.refresh();
		}
		
	},
	afterTimeButtonClick : function(){
		this.resetHighlights();
	},
	
	loadSvg: function(bl_id, fl_id, drawingName) {
		
		addParameterConfig(this.parameters);
		
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgName = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;

		//svgControl object used in ab-sp-common.js
		svgControl = this.svgControl;
		
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
   	},

   	reloadSvg: function(){
   		
   		addParameterConfig(this.parameters);
   		this.parameters['drawingName'] = this.dwgName;
   		this.svgControl.drawingController.getControl().reload(this.parameters);	
   	},
   	
   	unloadSvg: function(){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
    	this.svgControl.unload(parameters);

   	},
   	
   	closeDetailPanel: function(){
   		
   	}
});

/**
 * event handler when click the floor level of the tree
 * 
 * @param {Object}
 *            ob
 */
function onFlTreeClick(ob) {
	var drawingPanel = controller.abSpHlDpRmPerFlTrans_floorPlan;
	var currentNode = controller.abSpHlDpRmPerFlTrans_FlTree.lastNodeClicked;
	controller.blId = currentNode.parent.data['bl.bl_id'];
	controller.flId = currentNode.data['fl.fl_id'];
	var dvId = View.user.employee.organization.divisionId;
	var dpId = View.user.employee.organization.departmentId;
	var title = String.format(getMessage('drawingPanelTitle2'), controller.blId + '-' + controller.flId, dvId + "-" + dpId);
	controller.setParameters();
	drawingPanel.isLoadDrawing = true;
	displayFloor(drawingPanel, currentNode, title);
	controller.abSpHlDpRmPerFlTrans_summaryGrid.refresh();
}

