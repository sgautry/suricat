/**
 * @author Guo
 */
var filterBlId;
var filterDvId;
var filterDpId;

var controller = View.createController('hlGpByDpPerFlController', {

	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
    //----------------event handle--------------------
    afterViewLoad: function(){
        this.abSpHlGpByDpPerFl_SumGrid.sumaryTableName = 'gp';
        this.abSpHlGpByDpPerFl_SumGrid.buildPostFooterRows = addTotalRowForSummaryPanel;
        this.abSpHlGpByDpPerFl_BlTree.assetTableName = 'gp';
        this.abSpHlGpByDpPerFl_BlTree.createRestrictionForLevel = createResForTreeLevel3ByDwgname;
        
        //initialize svg control
        this.parameters = new Ab.view.ConfigObject();
        this.parameters['highlightParameters'] = [{'view_file':"ab-sp-hl-gp-by-dp-per-fl.axvw", 'hs_ds': "ds_ab-sp-hl-gp-by-dp-per-fl_drawing_gpHighlight", 'label_ds':'ds_ab-sp-hl-gp-by-dp-per-fl_drawing_gpLabel'}];
		
        this.parameters['bordersHighlightSelector'] = 'true'; 
        this.parameters['highlightFilterSelector'] = 'true';
        this.parameters['allowMultipleDrawings'] = 'false';
        this.parameters['divId'] = "svgDiv";
        this.parameters['orderByColumn'] = true;
        this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
        this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
        		'DatasourceSelector': {panelId: "abSpHlGpByDpPerFl_DrawingPanel"},
        		'AssetLocator': {divId: "svgDiv"},
        		'AssetTooltip': {handlers: [{assetType: 'gp', datasource: 'ds_ab-sp-hl-gp-by-dp-per-fl_drawing_gpLabel', fields: 'gp.gp_id;gp.dv_id;gp.dp_id;gp.area'}]},
        		'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
        this.parameters['events'] = [{'eventName': 'click', 'assetType' : 'gp', 'handler' : this.onClickAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpHlGpByDpPerFl_DrawingPanel", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
		
		var selectController = this.svgControl.drawingController.getController("SelectController");
    	//enable diagonal border selection 
    	selectController.setDiagonalSelectionPattern(true);
        
    },
         
    abSpHlGpByDpPerFl_SumGrid_afterRefresh: function(){
        //set color for every row according the drawing
        resetColorFieldValue('abSpHlGpByDpPerFl_SumGrid', 'abSpHlGpByDpPerFl_DrawingPanel', 'dp.dp_id', 'dp.hpattern_acad', 'abSpHlGpByDpPerFl_SumGrid_legend');
    },
    
    abSpHlGpByDpPerFl_filterConsole_onShowTree: function(){
        filterBlId = this.abSpHlGpByDpPerFl_filterConsole.getFieldValue('gp.bl_id');
        filterDvId = this.abSpHlGpByDpPerFl_filterConsole.getFieldValue('gp.dv_id');
        filterDpId = this.abSpHlGpByDpPerFl_filterConsole.getFieldValue('gp.dp_id');
        
        if (filterBlId) {
            this.abSpHlGpByDpPerFl_BlTree.addParameter('blId', " = " + "'" + filterBlId + "'");
        }
        else {
            this.abSpHlGpByDpPerFl_BlTree.addParameter('blId', "IS NOT NULL");
        }
        
        if (filterDvId) {
            this.abSpHlGpByDpPerFl_BlTree.addParameter('dvId', "gp.dv_id = " + "'" + filterDvId + "' AND ");
        }
        else {
            this.abSpHlGpByDpPerFl_BlTree.addParameter('dvId', "");
        }
        
        if (filterDpId) {
            this.abSpHlGpByDpPerFl_BlTree.addParameter('dpId', " = " + "'" + filterDpId + "'");
        }
        else {
            this.abSpHlGpByDpPerFl_BlTree.addParameter('dpId', "IS NOT NULL");
        }
        
        this.abSpHlGpByDpPerFl_BlTree.refresh();
        this.unloadSvg();
        this.closeDetailPanel();
        this.abSpHlGpByDpPerFl_DrawingPanel.lastLoadedBldgFloor = null;
        this.abSpHlGpByDpPerFl_SumGrid.clear();
        this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
    },
    
	loadSvg: function(bl_id, fl_id, drawingName) {
		
		addParameterConfig(this.parameters);
		
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgName = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
		//svgControl object used in ab-sp-common.js
		svgControl = this.svgControl;
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
   	},
   	
   	unloadSvg: function(){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
    	this.svgControl.unload(parameters);
    	
   	},
   	
    onClickAsset: function(params, drawingController){
        var restriction = new Ab.view.Restriction();
        restriction.addClause("gp.gp_id", params['assetId'], "=", true);
        
        var gpDetailPanel = View.panels.get('abSpHlGpByDpPerFl_GpDetailPanel');
        gpDetailPanel.refresh(restriction);
        gpDetailPanel.show(true);
        gpDetailPanel.showInWindow({
            width: 500,
            height: 250
        });
        
        // toggle the room if clicked
    	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId']);
    },
    
    closeDetailPanel: function(){
    	var gpDetailPanel = View.panels.get('abSpHlGpByDpPerFl_GpDetailPanel');
        gpDetailPanel.closeWindow();
    }
});

function generateReport(){
    var filterPanel = View.panels.get("abSpHlGpByDpPerFl_filterConsole");
    var filterBlId = filterPanel.getFieldValue('gp.bl_id');
    var filterDvId = filterPanel.getFieldValue('gp.dv_id');
    var filterDpId = filterPanel.getFieldValue('gp.dp_id');
    var restriction = "";
    if (filterBlId) {
        restriction += "&gp.bl_id='" + filterBlId + "'";
    }
    if (filterDvId) {
        restriction += "&gp.dv_id='" + filterDvId + "'";
    }
    if (filterDpId) {
        restriction += "&gp.dp_id='" + filterDpId + "'";
    }
    View.openDialog("ab-paginated-report-job.axvw?viewName=ab-sp-hl-gp-by-dp-per-fl-prnt.axvw" + restriction)
    
}

/**
 * event handler when click the floor level of the tree
 * @param {Object} ob
 */
function onFlTreeClick(ob){
    var drawingPanel = View.panels.get('abSpHlGpByDpPerFl_DrawingPanel');
    var currentNode = View.panels.get('abSpHlGpByDpPerFl_BlTree').lastNodeClicked;
    var blId = currentNode.parent.data['bl.bl_id'];
    var flId = currentNode.data['fl.fl_id'];
    var highlightResc = "";
    
    if (filterDvId) {
        highlightResc += "gp.dv_id = '" + filterDvId + "'";
    }
    if (filterDpId) {
        if (highlightResc) {
            highlightResc += " AND ";
        }
        highlightResc += "gp.dp_id = '" + filterDpId + "'";
    }
    if (!highlightResc) {
        highlightResc = "gp.dp_id IS NOT NULL";
    }
    View.dataSources.get('ds_ab-sp-hl-gp-by-dp-per-fl_drawing_gpHighlight').addParameter('gpDp', highlightResc);
    
    var title = String.format(getMessage('drawingPanelTitle2'), blId + '-' + flId);
    displayFloor(drawingPanel, currentNode, title);
    
    var summaryRes = new Ab.view.Restriction();
    summaryRes.addClauses(ob.restriction);
    summaryRes.addClause("gp.dwgname", currentNode.data['fl.dwgname'], "=");
    if (filterDvId) {
        summaryRes.addClause("dp.dv_id", filterDvId, "=");
    }
    if (filterDpId) {
        summaryRes.addClause("dp.dp_id", filterDpId, "=");
    }
    
    View.panels.get('abSpHlGpByDpPerFl_SumGrid').refresh(summaryRes);
}

/**
 * event handler when click the dp level of the tree
 * @param {Object} ob
 */
function onDpTreeClick(ob){
    var drawingPanel = View.panels.get('abSpHlGpByDpPerFl_DrawingPanel');
    var currentNode = View.panels.get('abSpHlGpByDpPerFl_BlTree').lastNodeClicked;
    
    var dpId = currentNode.data['gp.dp_id'];
    var dvId = currentNode.data['gp.dv_id'];
    var blId = currentNode.parent.parent.data['bl.bl_id'];
    var flId = currentNode.parent.data['fl.fl_id'];
    
    var restriction = new Ab.view.Restriction();
    restriction.addClause("gp.bl_id", blId, "=");
    restriction.addClause("gp.fl_id", flId, "=");
    restriction.addClause("gp.dwgname", currentNode.parent.data['fl.dwgname'], "=");
    View.dataSources.get('ds_ab-sp-hl-gp-by-dp-per-fl_drawing_gpHighlight').addParameter('gpDp', "gp.dv_id = " + "'" + dvId + "' AND " + " gp.dp_id = " + "'" + dpId + "'");
    
    var title = String.format(getMessage('drawingPanelTitle3'), blId + "-" + flId, dvId + "-" + dpId);
    displayFloor(drawingPanel, currentNode, title);
    
    restriction.addClause("dp.dv_id", dvId, "=");
    restriction.addClause("dp.dp_id", dpId, "=");
    View.panels.get('abSpHlGpByDpPerFl_SumGrid').refresh(restriction);
}

/**
 * event handler lisenner after create the tree node lable
 */
function afterGeneratingTreeNode(treeNode){
    resetDpTreeNodeLable(treeNode, 2, 'gp');
}

