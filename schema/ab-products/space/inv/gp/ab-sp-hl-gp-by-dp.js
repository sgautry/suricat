/**
 * @author Guo
 */
var controller = View.createController('hlGpByDpController', {

	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
    //----------------event handle--------------------
    afterViewLoad: function(){
        this.abSpHlGpByDp_flGrid.addEventListener('onMultipleSelectionChange', function(row){
            var highlightResc = " gp.gp_id IS NOT NULL";
            if (dvId) {
                highlightResc += " AND gp.dv_id = '" + dvId + "'";
            }
            else {
                highlightResc += " AND gp.dv_id IS NOT NULL";
            }
            
            if (dpId) {
                highlightResc += " AND gp.dp_id = '" + dpId + "'";
            }
            else {
                highlightResc += " AND gp.dp_id IS NOT NULL";
            }
            
            View.dataSources.get('ds_ab-sp-hl-gp-by-dp_drawing_gpHighlight').addParameter('gpDp', highlightResc);

            var checked = document.getElementById("abSpHlGpByDp_flGrid_row" + row.index + "_multipleSelectionColumn").checked;
    		
    		if (checked) {
    			// load relevant svg
    			controller.loadSvg(row["gp.bl_id"], row["gp.fl_id"], row["gp.dwgname"]);
    		} else {
    			controller.unloadSvg();
    			controller.closeDetailPanel();
    		}
            setDrawingTitle();
        });
        
        //initialize svg control
        this.parameters = new Ab.view.ConfigObject();
        this.parameters['highlightParameters'] = [{'view_file':"ab-sp-hl-gp-by-dp.axvw", 'hs_ds': "ds_ab-sp-hl-gp-by-dp_drawing_gpHighlight", 'label_ds':'ds_ab-sp-hl-gp-by-dp_drawing_gpLabel'}];
        this.parameters['bordersHighlightSelector'] = 'true'; 
        this.parameters['highlightFilterSelector'] = 'true';
        this.parameters['allowMultipleDrawings'] = 'true';
        this.parameters['multipleSelectionEnabled'] = 'true';
        this.parameters['divId'] = "svgDiv";
        this.parameters['orderByColumn'] = true;
        this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
        this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
        		'DatasourceSelector': {panelId: "abSpHlGpByDp_DrawingPanel"},
        		'AssetLocator': {divId: "svgDiv"},
        		'AssetTooltip': {handlers: [{assetType: 'gp', datasource: 'ds_ab-sp-hl-gp-by-dp_drawing_gpLabel', fields: 'gp.gp_id;gp.dv_id;gp.dp_id;gp.area'}]},
        		'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
        this.parameters['events'] = [{'eventName': 'click', 'assetType' : 'gp', 'handler' : this.onClickAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpHlGpByDp_DrawingPanel", this.parameters);
    },
    
	afterInitialDataFetch: function(){
        this.abSpHlGpByDp_flGrid.enableSelectAll(false);
    },

    abSpHlGpByDp_filterConsole_onShowDpGrid: function(){
        dvId = this.abSpHlGpByDp_filterConsole.getFieldValue('gp.dv_id');
        dpId = this.abSpHlGpByDp_filterConsole.getFieldValue('gp.dp_id');
        blId = this.abSpHlGpByDp_filterConsole.getFieldValue('gp.bl_id');
        var dvRes = " IS NOT NULL";
        var dpRes = " IS NOT NULL";
        var blRes = " IS NOT NULL";
        if (dvId) {
            dvRes = " = '" + dvId + "'";
        }
        if (dpId) {
            dpRes = " = '" + dpId + "'";
        }
        if (blId) {
            blRes = " = '" + blId + "'";
        }
        
        this.unloadSvg();
        this.closeDetailPanel();
        setDrawingTitle();
        
        this.abSpHlGpByDp_dvTree.addParameter('dvRes', dvRes);
        this.abSpHlGpByDp_dvTree.addParameter('dpRes', dpRes);
        this.abSpHlGpByDp_dvTree.refresh();
        
        this.abSpHlGpByDp_flGrid.addParameter('dvRes', dvRes);
        this.abSpHlGpByDp_flGrid.addParameter('dpRes', dpRes);
        this.abSpHlGpByDp_flGrid.addParameter('blRes', blRes);
        this.abSpHlGpByDp_flGrid.refresh();
    },
    
	loadSvg: function(bl_id, fl_id, drawingName) {
		
		addParameterConfig(this.parameters);
		
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgName = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;

    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
   	},
   	
   	unloadSvg: function(){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
    	this.svgControl.unload(parameters);
   	},
   	
    onClickAsset: function(params, drawingController){
        var restriction = new Ab.view.Restriction();
        restriction.addClause("gp.gp_id", params['assetId'], "=", true);
        
        var gpDetailPanel = View.panels.get('abSpHlGpByDp_GpDetailPanel');
        gpDetailPanel.refresh(restriction);
        gpDetailPanel.show(true);
        gpDetailPanel.showInWindow({
            width: 500,
            height: 250
        });
        
        // toggle the room if clicked
    	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId']);
    	
    	setDrawingTitle();
    },
    
    closeDetailPanel: function(){
        var gpDetailPanel = View.panels.get('abSpHlGpByDp_GpDetailPanel');
        gpDetailPanel.closeWindow();
    }    
});

var dvId;
var dpId;
var blId;

/**
 * event handler when click the department level of the tree
 * @param {Object} ob
 */
function onDpTreeClick(ob){
    var currentNode = View.panels.get('abSpHlGpByDp_dvTree').lastNodeClicked;
    dvId = currentNode.parent.data['dv.dv_id'];
    dpId = currentNode.data['dp.dp_id'];
    var dvRes = " = '" + dvId + "'";
    var dpRes = " = '" + dpId + "'";
    var blRes = " IS NOT NULL";
    if (blId) {
        blRes = " = '" + blId + "'";
    }
    var grid = View.panels.get('abSpHlGpByDp_flGrid');
    grid.addParameter('dvRes', dvRes);
    grid.addParameter('dpRes', dpRes);
    grid.addParameter('blRes', blRes);
    grid.refresh();
    
    controller.unloadSvg();
    controller.closeDetailPanel();
    setDrawingTitle();
}

/**
 * set drawing panel title
 */
function setDrawingTitle(){
    if (dvId || dpId) {
        controller.svgControl.getAddOn('InfoWindow').setText(String.format(getMessage('drawingPanelTitle1'), dvId + "-" + dpId));
    }
    else {
    	controller.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle'));
    }
}
