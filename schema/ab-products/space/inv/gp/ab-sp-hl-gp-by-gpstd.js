/**
 * @author Guo
 */
var controller = View.createController('hlGpByStdController', {

	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
    //----------------event handle--------------------
    afterViewLoad: function(){

        this.abSpHlGpByGpStd_SumGrid.sumaryTableName = 'gp';
        this.abSpHlGpByGpStd_SumGrid.buildPostFooterRows = addTotalRowForSummaryPanel;
        this.abSpHlGpByGpStd_BlTree.assetTableName = 'gp';
        this.abSpHlGpByGpStd_BlTree.createRestrictionForLevel = createResForTreeLevel3ByDwgname;
        
        //initialize svg control
		this.parameters = new Ab.view.ConfigObject();
		this.parameters['highlightParameters'] = [{'view_file':"ab-sp-hl-gp-by-gpstd.axvw", 'hs_ds': "ds_ab-sp-hl-gp-by-gpstd_drawing_gpHighlight", 'label_ds':'ds_ab-sp-hl-gp-by-gpstd_drawing_gpLabel'}];
		this.parameters['bordersHighlightSelector'] = 'true'; 
		this.parameters['highlightFilterSelector'] = 'true';
		this.parameters['allowMultipleDrawings'] = 'false';
		this.parameters['divId'] = "svgDiv";
		this.parameters['orderByColumn'] = true;
		this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
		this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'DatasourceSelector': {panelId: "abSpHlGpByGpStd_DrawingPanel"},
				'AssetLocator': {divId: "svgDiv"},
				'AssetTooltip': {handlers: [{assetType: 'gp', datasource: 'ds_ab-sp-hl-gp-by-gpstd_drawing_gpLabel', fields: 'gp.bl_id;gp.fl_id;gp.gp_id;gp.gp_std'}]},
				'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
		this.parameters['events'] = [{'eventName': 'click', 'assetType' : 'gp', 'handler' : this.onClickAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpHlGpByGpStd_DrawingPanel", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
    },
       
    abSpHlGpByGpStd_SumGrid_afterRefresh: function(){
        //set color for every row according the drawing
        resetColorFieldValue('abSpHlGpByGpStd_SumGrid', 'abSpHlGpByGpStd_DrawingPanel', 'gpstd.gp_std', 'gpstd.hpattern_acad', 'abSpHlGpByGpStd_SumGrid_legend');
    },
    
    abSpHlGpByGpStd_filterConsole_onShowTree: function(){
        var filterBlId = this.abSpHlGpByGpStd_filterConsole.getFieldValue('gp.bl_id');
        
        if (filterBlId) {
            this.abSpHlGpByGpStd_BlTree.addParameter('blId', " = " + "'" + filterBlId + "'");
        }
        else {
            this.abSpHlGpByGpStd_BlTree.addParameter('blId', "IS NOT NULL");
        }
        
        this.abSpHlGpByGpStd_BlTree.refresh();
        this.unloadSvg();
        this.abSpHlGpByGpStd_DrawingPanel.lastLoadedBldgFloor = null;
        this.abSpHlGpByGpStd_SumGrid.clear();
        this.closeDetailPanel();
        this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
    },
    
	loadSvg: function(bl_id, fl_id, drawingName) {
		
		addParameterConfig(this.parameters);
		
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgName = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
        
		//svgControl object used in ab-sp-common.js
		svgControl = this.svgControl;
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
    	
   	},
   	
   	unloadSvg: function(){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
    	this.svgControl.unload(parameters);
    	
   	},
   	
    onClickAsset: function(params, drawingController){
    	var restriction = new Ab.view.Restriction();
    	restriction.addClause("gp.gp_id", params['assetId'], "=", true);
         
        var rmDetailPanel = View.panels.get('abSpHlGpByGpStd_GpDetailPanel');
        rmDetailPanel.refresh(restriction);
        rmDetailPanel.show(true);
        rmDetailPanel.showInWindow({
        	width: 500,
        	height: 250
        });
         
        // toggle the room if clicked
    	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId']);
    },
    
    closeDetailPanel: function(){
        var rmDetailPanel = View.panels.get('abSpHlGpByGpStd_GpDetailPanel');
        rmDetailPanel.closeWindow();
    }
       	
});

/**
 * event handler when click the floor level of the tree
 * @param {Object} ob
 */
function onFlTreeClick(ob){
    var drawingPanel = View.panels.get('abSpHlGpByGpStd_DrawingPanel');
    var currentNode = View.panels.get('abSpHlGpByGpStd_BlTree').lastNodeClicked;
    var blId = currentNode.parent.data['bl.bl_id'];
    var flId = currentNode.data['fl.fl_id'];
    View.dataSources.get('ds_ab-sp-hl-gp-by-gpstd_drawing_gpHighlight').addParameter('gpStd', "gp.gp_std IS NOT NULL");
    
    var title = String.format(getMessage('drawingPanelTitle2'), blId + "-" + flId);
    displayFloor(drawingPanel, currentNode, title);
    
    var restriction = new Ab.view.Restriction();
    restriction.addClause("gp.bl_id", blId, "=");
    restriction.addClause("gp.fl_id", flId, "=");
    restriction.addClause("gp.dwgname", currentNode.data['fl.dwgname'], "=");
    View.panels.get('abSpHlGpByGpStd_SumGrid').refresh(restriction);
}

/**
 * event handler when click the gp standard level of the tree
 * @param {Object} ob
 */
function onGpStdTreeClick(ob){
    var drawingPanel = View.panels.get('abSpHlGpByGpStd_DrawingPanel');
    var currentNode = View.panels.get('abSpHlGpByGpStd_BlTree').lastNodeClicked;
    
    var gpStdId = currentNode.data['gp.gp_std'];
    var blId = currentNode.parent.parent.data['bl.bl_id'];
    var flId = currentNode.parent.data['fl.fl_id'];
    
    var restriction = new Ab.view.Restriction();
    restriction.addClause("gp.bl_id", blId, "=");
    restriction.addClause("gp.fl_id", flId, "=");
    restriction.addClause("gp.dwgname", currentNode.parent.data['fl.dwgname'], "=");
    
    View.dataSources.get('ds_ab-sp-hl-gp-by-gpstd_drawing_gpHighlight').addParameter('gpStd', "gp.gp_std = " + "'" + gpStdId + "'");
    
    var title = String.format(getMessage('drawingPanelTitle3'), blId + "-" + flId, gpStdId);
    displayFloor(drawingPanel, currentNode, title);
    
    restriction.addClause("gpstd.gp_std", gpStdId, "=");
    View.panels.get('abSpHlGpByGpStd_SumGrid').refresh(restriction);
}

