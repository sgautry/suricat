/**
 * @author Guo
 */
var controller = View.createController('abSpHlComnRmController', {

	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
    //----------------event handle--------------------
    afterViewLoad: function(){
        this.abSpHlComnRm_SumGrid.buildPostFooterRows = addTotalRowForSummaryPanel;
        
        //initialize svg control
		this.parameters = new Ab.view.ConfigObject();
		this.parameters['highlightParameters'] = [{'view_file':"ab-sp-hl-comn-rm.axvw", 'hs_ds': "ds_ab-sp-hl-comn-rm_drawing_rmHighlight", 'label_ds':'ds_ab-sp-hl-comn-rm_drawing_rmLabel'}];
		this.parameters['bordersHighlightSelector'] = 'true'; 
		this.parameters['highlightFilterSelector'] = 'true';
		this.parameters['allowMultipleDrawings'] = 'false';
		this.parameters['divId'] = "svgDiv";
		this.parameters['orderByColumn'] = true;
		this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
		this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'DatasourceSelector': {panelId: "abSpHlComnRm_DrawingPanel"},
				'AssetLocator': {divId: "svgDiv"},
				'AssetTooltip': {handlers: [{assetType: 'rm', datasource: 'ds_ab-sp-hl-comn-rm_drawing_rmLabel', fields: 'rm.rm_id;rm.prorate;rm.area'}]},
				'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
		this.parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpHlComnRm_DrawingPanel", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
    },
    
    abSpHlComnRm_SumGrid_afterRefresh: function(){
        //set color for every row according the drawing
        resetColorFieldValue('abSpHlComnRm_SumGrid', 'abSpHlComnRm_DrawingPanel', 'rm.prorate', 'hprorate.hpattern_acad', 'abSpHlComnRm_SumGrid_legend');
    },
    
    abSpHlComnRm_filterConsole_onShowTree: function(){
        var filterBlId = this.abSpHlComnRm_filterConsole.getFieldValue('rm.bl_id');
        
        if (filterBlId) {
            this.abSpHlComnRm_BlTree.addParameter('blId', " = " + "'" + filterBlId + "'");
        }
        else {
            this.abSpHlComnRm_BlTree.addParameter('blId', "IS NOT NULL");
        }
        
        this.abSpHlComnRm_BlTree.refresh();
        this.unloadSvg();
        this.closeDetailPanel();
        this.abSpHlComnRm_SumGrid.clear();
        this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));

    },
    
	loadSvg: function(bl_id, fl_id, drawingName) {
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgName = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
        
		//svgControl object used in ab-sp-common.js
		svgControl = this.svgControl;
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
    	
    	this.svgControl.getAddOn('InfoWindow').setText(String.format(getMessage('drawingPanelTitle2'), this.blId + "-" + this.flId));
   	},
   	
   	unloadSvg: function(){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
    	this.svgControl.unload(parameters);

   	},
   	
    onClickAsset: function(params, drawingController){
    	var arrayRoomIDs = params['assetId'].split(";");
    	var restriction = new Ab.view.Restriction();
        restriction.addClause("rm.bl_id", arrayRoomIDs[0], "=", true);
        restriction.addClause("rm.fl_id", arrayRoomIDs[1], "=", true);
        restriction.addClause("rm.rm_id", arrayRoomIDs[2], "=", true);
         
        var rmDetailPanel = View.panels.get('abSpHlComnRm_RmDetailPanel');
        rmDetailPanel.refresh(restriction);
        rmDetailPanel.show(true);
        rmDetailPanel.showInWindow({
        	width: 500,
        	height: 250
        });
         
        // toggle the room if clicked
    	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId']);
    },
    
    closeDetailPanel: function(){
        var rmDetailPanel = View.panels.get('abSpHlComnRm_RmDetailPanel');
        rmDetailPanel.closeWindow();
    }    
});

function generateReport(){
    var filterPanel = View.panels.get("abSpHlComnRm_filterConsole");
    var filterBlId = filterPanel.getFieldValue('rm.bl_id');
    var restriction = "";
    if (filterBlId) {
        restriction += "&rm.bl_id=" + filterBlId;
    }
    View.openDialog("ab-paginated-report-job.axvw?viewName=ab-sp-hl-comn-rm-prnt.axvw" + restriction)
    
}

/**
 * event handler when click the floor level of the tree
 * @param {Object} ob
 */
function onFlTreeClick(ob){
    var drawingPanel = View.panels.get('abSpHlComnRm_DrawingPanel');
    var currentNode = View.panels.get('abSpHlComnRm_BlTree').lastNodeClicked;
    var blId = currentNode.parent.data['bl.bl_id'];
    var flId = currentNode.data['fl.fl_id'];
    var dwgName = currentNode.data['fl.dwgname'];
    controller.closeDetailPanel()
    controller.loadSvg(blId, flId, dwgName);
    var summaryRes = new Ab.view.Restriction();
    summaryRes.addClauses(ob.restriction);
    summaryRes.addClause("rm.dwgname", dwgName, "=");
    View.panels.get('abSpHlComnRm_SumGrid').refresh(summaryRes);
}
