/**
 * @author Guo
 */
var controller = View.createController('abSpHlComnGpController', {

	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
    //----------------event handle--------------------
    afterViewLoad: function(){
//        this.abSpHlComnGp_DrawingPanel.appendInstruction("default", "", getMessage('drawingPanelTitle1'));
        this.abSpHlComnGp_SumGrid.sumaryTableName = 'gp';
        this.abSpHlComnGp_SumGrid.buildPostFooterRows = addTotalRowForSummaryPanel;
        
        //initialize svg control
        this.parameters = new Ab.view.ConfigObject();
        this.parameters['highlightParameters'] = [{'view_file':"ab-sp-hl-comn-gp.axvw", 'hs_ds': "ds_ab-sp-hl-comn-gp_drawing_gpHighlight", 'label_ds':'ds_ab-sp-hl-comn-gp_drawing_gpLabel'}];
        this.parameters['bordersHighlightSelector'] = 'true'; 
        this.parameters['highlightFilterSelector'] = 'true';
        this.parameters['allowMultipleDrawings'] = 'false';
        this.parameters['divId'] = "svgDiv";
        this.parameters['orderByColumn'] = true;
        this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
        this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
        		'DatasourceSelector': {panelId: "abSpHlComnGp_DrawingPanel"},
        		'AssetLocator': {divId: "svgDiv"},
        		'AssetTooltip': {handlers: [{assetType: 'gp', datasource: 'ds_ab-sp-hl-comn-gp_drawing_gpLabel', fields: 'gp.gp_id;gp.prorate;gp.area'}]},
        		'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
        this.parameters['events'] = [{'eventName': 'click', 'assetType' : 'gp', 'handler' : this.onClickAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpHlComnGp_DrawingPanel", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
    },
    
    abSpHlComnGp_SumGrid_afterRefresh: function(){
        //set color for every row according the drawing
        resetColorFieldValue('abSpHlComnGp_SumGrid', 'abSpHlComnGp_DrawingPanel', 'gp.prorate', 'hprorate.hpattern_acad', 'abSpHlComnGp_SumGrid_legend');
    },
    
    abSpHlComnGp_filterConsole_onShowTree: function(){
        var filterBlId = this.abSpHlComnGp_filterConsole.getFieldValue('gp.bl_id');
        
        if (filterBlId) {
            this.abSpHlComnGp_BlTree.addParameter('blId', " = " + "'" + filterBlId + "'");
        }
        else {
            this.abSpHlComnGp_BlTree.addParameter('blId', "IS NOT NULL");
        }
        
        this.abSpHlComnGp_BlTree.refresh();
        this.unloadSvg();
        this.closeDetailPanel();
        this.abSpHlComnGp_SumGrid.clear();
        this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
        
    },
    
	loadSvg: function(bl_id, fl_id, drawingName) {
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgName = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
        
		//svgControl object used in ab-sp-common.js
		svgControl = this.svgControl;
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
    	
    	this.svgControl.getAddOn('InfoWindow').setText(String.format(getMessage('drawingPanelTitle2'), this.blId + "-" + this.flId));
   	},
   	
   	unloadSvg: function(){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
    	this.svgControl.unload(parameters);
    	
   	},
   	
    onClickAsset: function(params, drawingController){
        var restriction = new Ab.view.Restriction();
        restriction.addClause("gp.gp_id", params['assetId'], "=", true);
        
        var gpDetailPanel = View.panels.get('abSpHlComnGp_GpDetailPanel');
        gpDetailPanel.refresh(restriction);
        gpDetailPanel.show(true);
        gpDetailPanel.showInWindow({
            width: 500,
            height: 250
        });
        
        // toggle the room if clicked
    	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId']);
    },
    
    closeDetailPanel: function(){
        var gpDetailPanel = View.panels.get('abSpHlComnGp_GpDetailPanel');
        gpDetailPanel.closeWindow();
    }
});

function generateReport(){
    var filterPanel = View.panels.get("abSpHlComnGp_filterConsole");
    var filterBlId = filterPanel.getFieldValue('gp.bl_id');
    var restriction = "";
    if (filterBlId) {
        restriction += "&gp.bl_id=" + filterBlId;
    }
    View.openDialog("ab-paginated-report-job.axvw?viewName=ab-sp-hl-comn-gp-prnt.axvw" + restriction);
    
}

/**
 * event handler when click the floor level of the tree
 * @param {Object} ob
 */
function onFlTreeClick(ob){
    var drawingPanel = View.panels.get('abSpHlComnGp_DrawingPanel');
    var currentNode = View.panels.get('abSpHlComnGp_BlTree').lastNodeClicked;
    var blId = currentNode.parent.data['bl.bl_id'];
    var flId = currentNode.data['fl.fl_id'];
    var dwgName = currentNode.data['fl.dwgname'];
    controller.closeDetailPanel();
    controller.loadSvg(blId, flId, dwgName);	
    var summaryRes = new Ab.view.Restriction();
    summaryRes.addClauses(ob.restriction);
    summaryRes.addClause("gp.dwgname", dwgName, "=");
    View.panels.get('abSpHlComnGp_SumGrid').refresh(summaryRes);
}
