
var controller = View.createController('abSpAsgnEmToRm_Controller', {

	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	selectableOpt: null,
	
    afterViewLoad: function(){
        
        this.abSpAsgnEmToRm_emSelect.addEventListener('onMultipleSelectionChange', onEmSelectionChange);
        
        //initialize svg control
		this.parameters = new Ab.view.ConfigObject();
		this.parameters['highlightParameters'] = [{'view_file':"ab-sp-asgn-em-to-rm.axvw", 'hs_ds': "ds_ab-sp-asgn-em-to-rm_drawing_availRm", 'label_ds':'ds_ab-sp-asgn-em-to-rm_drawing_rmLabel1'}];
		this.parameters['bordersHighlightSelector'] = 'true'; 
		this.parameters['highlightFilterSelector'] = 'true';
		this.parameters['allowMultipleDrawings'] = 'false';
		this.parameters['multipleSelectionEnabled'] = 'true';
		this.parameters['divId'] = "svgDiv";
		this.parameters['orderByColumn'] = true;
		this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
		this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'DatasourceSelector': {panelId: "abSpAsgnEmToRm_drawingPanel"},
				'AssetLocator': {divId: "svgDiv"},
				'AssetTooltip': {handlers: [{assetType: 'rm', datasource: 'ds_ab-sp-asgn-em-to-rm_drawing_rmLabel1', fields: 'rm.rm_id;rm.count_em;rm.cap_em;rm.rm_cat;rm.rm_type'}]},
				'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
		this.parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpAsgnEmToRm_drawingPanel", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('selectFloor'));
		
		this.svgControl.drawingController.getController("SelectController").setMultipleSelection(true);
    },
        
    afterInitialDataFetch: function(){
        this.abSpAsgnEmToRm_emSelect.enableSelectAll(false);
    },
    
    abSpAsgnEmToRm_filterConsole_onShowTree: function(){
        var filterBlId = this.abSpAsgnEmToRm_filterConsole.getFieldValue('em.bl_id');
        var filterEmId = this.abSpAsgnEmToRm_filterConsole.getFieldValue('em.em_id');
        var filterDvId = this.abSpAsgnEmToRm_filterConsole.getFieldValue('em.dv_id');
        var filterDpId = this.abSpAsgnEmToRm_filterConsole.getFieldValue('em.dp_id');
        var blTreeRes = new Ab.view.Restriction();
        var emGridRes = new Ab.view.Restriction();
        blTreeRes.addClause("bl.bl_id", filterBlId + '%', "LIKE");
        emGridRes.addClause("em.em_id", filterEmId + '%', "LIKE");
        
        //kb:3037756,unable to search for an employee in the filter console of the Assign Employees to Available Rooms task, when the employee has no divison/department
        if(valueExistsNotEmpty(filterDvId)){
        	emGridRes.addClause("em.dv_id", filterDvId + '%', "LIKE");
        	
        	if(valueExistsNotEmpty(filterDpId)){
        		emGridRes.addClause("em.dp_id", filterDpId + '%', "LIKE");
        	}
        }
        
        
        
        this.abSpAsgnEmToRm_blTree.refresh(blTreeRes);
        this.abSpAsgnEmToRm_emSelect.refresh(emGridRes);
        if (this.abSpAsgnEmToRm_drawingPanel.isLoadedDrawing) {
            this.svgControl.getAddOn('InfoWindow').setText(getMessage('selectEm'));
        }
    },
    
	loadSvg: function(bl_id, fl_id, drawingName) {
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgname = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgname;
        
		//svgControl object used in ab-sp-common.js
		svgControl = this.svgControl;
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
    	
   	},
   	
   	unloadSvg: function(){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgname;
    	this.svgControl.unload(parameters);

   	},
   	
    onClickAsset: function(params, drawingController){
    
    	var selectable = controller.selectableOpt[params['assetId']];
    	if(!selectable||emAssigns.length==0)
    		return;
    	
    	var pk = params['assetId'].split(";");
    	var selected = isRoomSelected(params, drawingController);
    	
    	if (checkCount(pk)) {
            View.confirm(getMessage('countOver'), function(button){
                if (button == 'yes') {
                    addAssignmentRows(pk);
                    View.panels.get("abSpAsgnEmToRm_emSelect").setAllRowsSelected(false);
                }
            });
        }
        else {
            addAssignmentRows(pk);
            View.panels.get("abSpAsgnEmToRm_emSelect").setAllRowsSelected(false);
        }
        
        controller.svgControl.getAddOn('InfoWindow').setText(getMessage('selectAnotherEm'));
        controller.reHighLightRms(params, drawingController);

    },
    
    reloadSvg: function(){
    	this.svgControl.drawingController.getControl().reload(this.parameters);
    },
    
    reHighLightRms: function(){
        this.reloadSvg();
        
        var grid = View.panels.get("abSpAsgnEmToRm_emAssigned");
        var rows = grid.rows;
        var assetIds = [];
        for (var i = 0; i < rows.length; i++) {
            var row = rows[i];
            assetIds.push(row['em.bl_id']+";"+row['em.fl_id']+";"+row['em.rm_id']);
            
        }
        var selectController = this.svgControl.drawingController.getController("SelectController");
        var svgId = this.parameters['divId']+"-"+this.dwgname+"-"+"svg"
        selectController.highlightSelectedAssets(svgId, assetIds);
    },
    
    closeDetailPanel: function(){

    },
    
    abSpAsgnEmToRm_legendGrid_afterRefresh: function(){
    	var length = this.abSpAsgnEmToRm_legendGrid.gridRows.length;
    	var text = "";
    	for(var i=0;i<length;i++){
    		var value = this.abSpAsgnEmToRm_legendGrid.gridRows.items[i].dom.innerText;
    		
    		//APP-1261 In IE and FF, innerText value contains \r\n at the beginning, so use indexOf to check.
    		if(value.indexOf('Non-Occupiable')>=0){
    			text = getMessage('legendLevel1');
    		}else if(value.indexOf('Vacant')>=0){
    			text = getMessage('legendLevel2');
    		}else if(value.indexOf('Available')>=0){
    			text = getMessage('legendLevel3');
    		}else if(value.indexOf('At Capacity')>=0){
    			text = getMessage('legendLevel4');
    		}else if(value.indexOf('Exceeds Capacity')>=0){
    			text = getMessage('legendLevel5');
    		}

			this.abSpAsgnEmToRm_legendGrid.gridRows.items[i].dom.childNodes[1].innerText = text;
			
    	}
    	
    }
});

var emAssigns = [];
/**
 * check the room is selected or not
 */
function isRoomSelected(params, drawingController){
	var selectedAssets = drawingController.getController("SelectController").selectedAssets[params.svgId];
	if(valueExistsNotEmpty(selectedAssets)){
		for(var i=0;i<selectedAssets.length;i++)
		if(selectedAssets[i] == params.assetId)
		{
			return false;
		}
	}
	return true;
}

/**
 * event handler when click row of grid 'abSpAsgnEmToRm_emSelect'.
 */
function onEmSelectionChange(rowbbb){
    emAssigns = [];
    var cp = View.panels.get('abSpAsgnEmToRm_drawingPanel');
    if (cp.isLoadedDrawing) {
        var grid = View.panels.get("abSpAsgnEmToRm_emSelect");
        var rows = grid.getSelectedRows();
        if (rows.length < 1) {
            controller.svgControl.getAddOn('InfoWindow').setText(getMessage('selectEm'));
            return;
        }
        
        for (var i = 0; i < rows.length; i++) {
            var row = rows[i];
            controller.svgControl.getAddOn('InfoWindow').setText(getMessage('selectRm'));
            var emAssign = new Ab.data.Record();
            emAssign.setValue("em.em_id", row['em.em_id']);
            emAssign.setValue("em.bl_id_current", row['em.bl_id']);
            emAssign.setValue("em.fl_id_current", row['em.fl_id']);
            emAssign.setValue("em.rm_id_current", row['em.rm_id']);
            emAssigns.push(emAssign);
        }
        
    }
}

/**
 * event handler when click floor node of the tree 'abSpAsgnEmToRm_blTree'.
 * @param {Object} ob
 */
function onTreeClick(ob){
    var cp = View.getControl('', 'abSpAsgnEmToRm_drawingPanel');
    var currentNode = View.panels.get('abSpAsgnEmToRm_blTree').lastNodeClicked;
    var blId = currentNode.parent.data['bl.bl_id'];
    var flId = currentNode.data['fl.fl_id'];
    var dwgName = currentNode.data['fl.dwgname'];
    controller.loadSvg(blId, flId, dwgName);
    setSelectability(ob.restriction);
    cp.isLoadedDrawing = true;
    emAssigns = []
    View.panels.get("abSpAsgnEmToRm_emSelect").setAllRowsSelected(false);
    
    controller.svgControl.getAddOn('InfoWindow').setText(getMessage('selectEm'));
}

/**
 * check is the room is full.
 * @param {Object} pk
 * @return {boolean} isFull
 */
function checkCount(pk){
    var isFull = false;
    var blId = pk[0];
    var flId = pk[1];
    var rmId = pk[2];
    
    var availableCount = getRoomCountVal(blId, flId, rmId, 'rm.cap_em') - getRoomCountVal(blId, flId, rmId, 'rm.countEm');
    var newAssignedCount = 0;
    var grid = View.panels.get("abSpAsgnEmToRm_emAssigned");
    grid.show(true);
    var rows = grid.rows;
    for (var i = 0; i < rows.length; i++) {
        var row = rows[i];
        if (row['em.bl_id'] == blId && row['em.fl_id'] == flId && row['em.rm_id'] == rmId && !isSelectedEm(row['em.em_id'])) {
            newAssignedCount++;
        }
    }
    if ((availableCount - newAssignedCount - emAssigns.length) < 0) {
        isFull = true;
    }
    return isFull;
}

function isSelectedEm(emId){
    var isSelected = false;
    for (var i = 0; i < emAssigns.length; i++) {
        if (emId == emAssigns[i].getValue('em.em_id')) {
            isSelected = true;
            break;
        }
    }
    return isSelected;
}

function submitChanges(){
    var grid = View.panels.get("abSpAsgnEmToRm_emAssigned");
    if (grid.rows.length < 1) {
        View.showMessage(getMessage('noEmSelected'));
        return;
    }
    View.openProgressBar(getMessage('saving'));
    doSubmitChanges.defer(500);
}

/**
 * save the assignment.
 */
function doSubmitChanges(){
    var dsEmp = View.dataSources.get("ds_ab-sp-asgn-em-to-rm_grid_em");
    var grid = View.panels.get("abSpAsgnEmToRm_emAssigned");
    
    try {
    
        for (var i = 0; i < grid.gridRows.length; i++) {
            var row = grid.gridRows.items[i];
            
            var emId = row.getFieldValue("em.em_id");
            var buildingId = row.getFieldValue("em.bl_id");
            var floorId = row.getFieldValue("em.fl_id");
            var roomId = row.getFieldValue("em.rm_id");
            var buildingIdCurrent = row.getFieldValue("em.bl_id_current");
            var floorIdCurrent = row.getFieldValue("em.fl_id_current");
            var roomIdCurrent = row.getFieldValue("em.rm_id_current");
            
            // First set the new room for the employee
            var rec = new Ab.data.Record();
            rec.isNew = false;
            rec.setValue("em.em_id", emId);
            rec.setValue("em.bl_id", buildingId);
            rec.setValue("em.fl_id", floorId);
            rec.setValue("em.rm_id", roomId);
            
            rec.oldValues = new Object();
            rec.oldValues["em.em_id"] = emId;
            
            //var rec = grid.gridRows.items[i].getRecord();
            //var rec = grid.rowToRecord(row);
            dsEmp.saveRecord(rec);
            
            // Update the rm.count_em value
            setRoomEmpCnt(buildingId, floorId, roomId, 1);
            if (buildingIdCurrent && floorIdCurrent && roomIdCurrent) {
                setRoomEmpCnt(buildingIdCurrent, floorIdCurrent, roomIdCurrent, -1);
            }
        }
        
        grid.removeRows(0);
        grid.update();
        View.panels.get("abSpAsgnEmToRm_emSelect").refresh();
        var cp = View.panels.get('abSpAsgnEmToRm_drawingPanel');
        View.closeProgressBar();
        controller.reloadSvg();
    } 
    catch (e) {
        View.closeProgressBar();
        Workflow.handleError(e);
    }
}

/**
 * change the room employee count in database.
 * @param {String} buildingId
 * @param {String} floorId
 * @param {String} roomId
 * @param {int} cnt
 */
function setRoomEmpCnt(buildingId, floorId, roomId, cnt){
    var rec = new Ab.data.Record();
    
    var cntOld = getRoomCountVal(buildingId, floorId, roomId, 'rm.countEm');
    cnt = cntOld + cnt;
    if (cnt < 0) 
        cnt = 0;
    
    rec.isNew = false;
    rec.setValue("rm.bl_id", buildingId);
    rec.setValue("rm.fl_id", floorId);
    rec.setValue("rm.rm_id", roomId);
    rec.setValue("rm.count_em", cnt);
    
    rec.oldValues = new Object();
    rec.oldValues["rm.bl_id"] = buildingId;
    rec.oldValues["rm.fl_id"] = floorId;
    rec.oldValues["rm.rm_id"] = roomId;
    rec.oldValues["rm.count_em"] = cntOld;
    try {
        View.dataSources.get("ds_ab-sp-asgn-em-to-rm_rmCnt").saveRecord(rec);
    } 
    catch (e) {
        View.showException(e);
    }
}

/**
 * get the room employee count or employee capacity from database.
 * @param {String} buildingId
 * @param {String} floorId
 * @param {String} roomId
 * @param {String} fieldName rm.count_em or rm.cap_em
 * @return {int} cnt
 */
function getRoomCountVal(buildingId, floorId, roomId, fieldName){
    var cnt = 0;
    try {
        var restriction = new Ab.view.Restriction();
        restriction.addClause("rm.bl_id", buildingId, "=", true);
        restriction.addClause("rm.fl_id", floorId, "=", true);
        restriction.addClause("rm.rm_id", roomId, "=", true);
        var recs = View.dataSources.get("ds_ab-sp-asgn-em-to-rm_rmCnt").getRecords(restriction);
        if (recs != null) 
            cnt = recs[0].getValue(fieldName);
    } 
    catch (e) {
        View.showException(e);
    }
    return parseInt(cnt, 10);
}


/**
 * remove selected  employee assignment from the grid 'abSpAsgnEmToRm_emAssigned'.
 */
function removeEmpFromList(){
    var grid = View.panels.get("abSpAsgnEmToRm_emAssigned");
    var row = grid.rows[grid.selectedRowIndex];
    grid.removeGridRow(row.row.getIndex());
    grid.update();
    
    controller.reHighLightRms();
}

/**
 * unassign the selected employee.
 */
function unAssign(){
    var grid = View.panels.get("abSpAsgnEmToRm_emSelect");
    var rows = grid.getSelectedRows();
    if (rows.length < 1) {
        View.showMessage(getMessage('noEmSelected'));
        return;
    }
    
    var message = getMessage('confirmMessage');
    
    View.confirm(message, function(button){
        if (button == 'yes') {
            try {
                View.openProgressBar(getMessage('saving'));
                completeEmpUnassign.defer(500, this, [rows]);
            } 
            catch (e) {
                View.closeProgressBar();
                View.showMessage('error', '', e.message, e.data);
            }
        }
    });
}

/**
 * clear location info of the selected employee and changed the rm.count_em.
 * @param {Object} row
 */
function completeEmpUnassign(rows){
    for (var i = 0; i < rows.length; i++) {
        var row = rows[i];
        if (!row['em.rm_id']) 
            continue;
        
        var rec = row.row.getRecord(['em.em_id', 'em.bl_id', 'em.fl_id', 'em.rm_id']);
        rec.setValue('em.bl_id', '');
        rec.setValue('em.fl_id', '');
        rec.setValue('em.rm_id', '');
        
        View.dataSources.get("ds_ab-sp-asgn-em-to-rm_grid_em").saveRecord(rec);
        
        var buildingId = row['em.bl_id'];
        var floorId = row['em.fl_id'];
        var roomId = row['em.rm_id'];
        setRoomEmpCnt(buildingId, floorId, roomId, -1);
    }
    
    View.panels.get("abSpAsgnEmToRm_emSelect").refresh();
    var cp = View.panels.get('abSpAsgnEmToRm_drawingPanel');
    if (cp.isLoadedDrawing) {
        controller.svgControl.getAddOn('InfoWindow').setText(getMessage('selectAnotherEm'));
        controller.reloadSvg();
    }
    clearChanges();
    View.closeProgressBar();
}

/**
 * clear all employee assignments and clear the highlight.
 */
function clearChanges(){
    var grid = View.panels.get("abSpAsgnEmToRm_emAssigned");
    var cp = View.panels.get('abSpAsgnEmToRm_drawingPanel');
    grid.removeRows(0);
    grid.update();
    controller.reHighLightRms();
}

/**
 * set legend text according the legend level value.
 * @param {Object} row
 * @param {Object} column
 * @param {Object} cellElement
 */
function setLegendLabel(row, column, cellElement){
    var value = row[column.id];
    if (column.id == 'legend.value' && value != '') {
        var text = '';
        switch (value) {
            case '1':
                text = getMessage('legendLevel1');
                break;
            case '2':
                text = getMessage('legendLevel2');
                break;
            case '3':
                text = getMessage('legendLevel3');
                break;
            case '4':
                text = getMessage('legendLevel4');
                break;
            case '5':
                text = getMessage('legendLevel5');
                break;
        }
        var contentElement = cellElement.childNodes[0];
        contentElement.nodeValue = text;
    }
}

/**
 * set unoccupiable room unselected.
 * @param {Object} restriction
 */
function setSelectability(restriction){
    var drawingPanel = View.panels.get('abSpAsgnEmToRm_drawingPanel')
    var rmRecords = View.dataSources.get('ds_ab-sp-asgn-em-to-rm_drawing_availRm').getRecords(restriction);
    controller.selectableOpt = {};
    for (var i = 0; i < rmRecords.length; i++) {
        var record = rmRecords[i];
        var occupiable = record.getValue('rmcat.occupiable');
        var blId = record.getValue('rm.bl_id');
        var flId = record.getValue('rm.fl_id');
        var rmId = record.getValue('rm.rm_id');
        var assetId = blId + ';' + flId + ';' + rmId;
        if (occupiable == '0') {
            controller.selectableOpt[assetId] = false;
        }else{
        	controller.selectableOpt[assetId] = true;
        }
    }
}

/**
 * add an assignment row.
 * @param {Array} restriction
 */
function addAssignmentRows(pk){
    var grid = View.panels.get("abSpAsgnEmToRm_emAssigned");
    
    for (var i = 0; i < emAssigns.length; i++) {
        var emAssign = emAssigns[i];
        var bFound = false;
        for (var j = 0; j < grid.rows.length && !bFound; j++) {
            var row = grid.rows[j];
            if (row["em.em_id"] == emAssign.getValue('em.em_id')) {
                grid.removeGridRow(j);
                bFound = true;
            }
        }
        
        emAssign.setValue("em.bl_id", pk[0]);
        emAssign.setValue("em.fl_id", pk[1]);
        emAssign.setValue("em.rm_id", pk[2]);
        grid.addGridRow(emAssign);
    }
    
    controller.svgControl.getAddOn('InfoWindow').setText(getMessage('selectAnotherEm'));
    grid.sortEnabled = false;
    grid.update();
}
