/**
 * @author Guo
 */

var controller = View.createController('abSpHlOccPlanTrans_Controller', {
	blId : '',
	date : '',
	dateToAddField:'rmpct.date_start',
	dateToUpdateField:'rmpct.date_start',
	console:null,
	timeLine:null,
	
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
	// ----------------event handle--------------------
	afterViewLoad : function() {
		
		 //initialize svg control
		this.parameters = new Ab.view.ConfigObject();
		this.parameters['highlightParameters'] = [{'view_file':"ab-sp-hl-occ-plan-trans.axvw", 'hs_ds':"abSpHlOccPlanTransrmHighlight", 'label_ds':'abSpHlOccPlanTransrmLabel'}];
		this.parameters['bordersHighlightSelector'] = 'true'; 
		this.parameters['highlightFilterSelector'] = 'true';
		this.parameters['allowMultipleDrawings'] = 'false';
		this.parameters['divId'] = "svgDiv";
		this.parameters['orderByColumn'] = true;
		this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
		this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'DatasourceSelector': {panelId: "abSpHlOccPlanTrans_floorPlan"},
				'AssetLocator': {divId: "svgDiv"},
				'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpHlOccPlanTrans_floorPlan", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
	},
	   
	afterInitialDataFetch : function() {
		this.date = getCurrentDate();
		this.abSpHlOccPlanTransConsole.setFieldValue('rmpct.date_start', this.date);
		this.console=this.abSpHlOccPlanTransConsole;
		this.timeLine=this.timeLineButton;
		this.dateAction = this.timeLine.actions.get('currentDate');
		this.dateAction.setTitle(this.console.getFieldElement(this.dateToAddField).value);
		this.date=this.abSpHlOccPlanTransConsole.getFieldValue('rmpct.date_start');
		setTimeTitle(this.timeLine);
	},

	/**
	 * on click event handler for show button in the console
	 */
	abSpHlOccPlanTransConsole_onShowTree : function() {
		this.console=this.abSpHlOccPlanTransConsole;
		this.timeLine=this.timeLineButton;
		this.dateAction = this.timeLine.actions.get('currentDate');
		this.dateAction.setTitle(this.console.getFieldElement(this.dateToAddField).value);
		this.date=this.abSpHlOccPlanTransConsole.getFieldValue('rmpct.date_start');
		this.blId = this.abSpHlOccPlanTransConsole.getFieldValue('rmpct.bl_id');
		this.date=this.abSpHlOccPlanTransConsole.getFieldValue('rmpct.date_start');
		if (this.blId) {
			this.abSpHlOccPlanTransBlTree.addParameter('blId', " = " + "'" + this.blId + "'");
		} else {
			this.abSpHlOccPlanTransBlTree.addParameter('blId', "IS NOT NULL");
		}

		this.abSpHlOccPlanTransBlTree.refresh();
		this.unloadSvg();
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
	},
	resetHighlights : function() {
		this.date=this.abSpHlOccPlanTransConsole.getFieldValue('rmpct.date_start');
		this.abSpHlOccPlanTransrmHighlight.addParameter('date', this.date);
		this.abSpHlOccPlanTransrmLabel.addParameter('date', this.date);
		if (this.abSpHlOccPlanTrans_floorPlan.isLoadDrawing) {
			this.reloadSvg();
		}
	},
	afterTimeButtonClick : function(){
		this.resetHighlights();
	},
	
	loadSvg: function(bl_id, fl_id, drawingName) {
		
		addParameterConfig(this.parameters);
		
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgName = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
        
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
    	
    	this.svgControl.getAddOn('InfoWindow').setText(String.format(getMessage('drawingPanelTitle2'), this.blId + "-" + this.flId));
   	},
   	
   	reloadSvg: function(){
   		
   		addParameterConfig(this.parameters);
   		this.parameters['drawingName'] = this.dwgName;
   		this.svgControl.drawingController.getControl().reload(this.parameters);
   	},
   	
   	unloadSvg: function(){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
    	this.svgControl.unload(parameters);
    	
   	}
});

/**
 * generate paginated report
 */
function generateReport() {
	var filterPanel = controller.abSpHlOccPlanTransConsole;
	var filterBlId = filterPanel.getFieldValue('rmpct.bl_id');
	var blRes = '1=1';

	if (filterBlId) {
		blRes = "rm.bl_id='" + filterBlId + "'";
	}

	View.openPaginatedReportDialog("ab-sp-hl-occ-plan-trans-prnt.axvw", null, {
		'date' : controller.date,
		'blRes' : blRes
	});
}

/**
 * event handler when click the floor level of the tree
 * 
 * @param {Object}
 *            ob
 */
function onFlTreeClick(ob) {
	var currentNode = controller.abSpHlOccPlanTransBlTree.lastNodeClicked;
	var blId = currentNode.parent.data['bl.bl_id'];
	var flId = currentNode.data['fl.fl_id'];
	var flId = currentNode.data['fl.fl_id'];
	var drawingPanel = controller.abSpHlOccPlanTrans_floorPlan;
	var title = String.format(getMessage('drawingPanelTitle2'), blId + "-" + flId);

	controller.abSpHlOccPlanTransrmHighlight.addParameter('date', controller.date);
	controller.abSpHlOccPlanTransrmLabel.addParameter('date', controller.date);

	var dwgName = currentNode.data['fl.dwgname'];

	controller.loadSvg(blId, flId, dwgName);
	drawingPanel.isLoadDrawing = true;
	controller.svgControl.getAddOn('InfoWindow').setText(title);
	
}

