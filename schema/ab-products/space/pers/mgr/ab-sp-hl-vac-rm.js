/**
 * @author Guo
 */
var filterBlId;
var filterRmCat;

var controller = View.createController('abSpHlVacRm_Controller', {

	svgControl: null,
	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	openerPanel:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
    //----------------event handle--------------------
    afterViewLoad: function(){
        this.abSpHlVacRm_rmGrid.buildPostFooterRows = addTotalRow;
        
        //initialize svg control
		this.parameters = new Ab.view.ConfigObject();
		this.parameters['highlightParameters'] = [{'view_file':"ab-sp-hl-vac-rm.axvw", 'hs_ds': "ds_ab-sp-hl-vac-rm_drawing_rmHighlight", 'label_ds':'ds_ab-sp-hl-vac-rm_drawing_rmLabel1'}];
		this.parameters['bordersHighlightSelector'] = 'true';
		this.parameters['borderSize'] = 18;
		this.parameters['highlightFilterSelector'] = 'true';
		this.parameters['allowMultipleDrawings'] = 'false';
		this.parameters['divId'] = "svgDiv";
		this.parameters['orderByColumn'] = true;
		this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
		this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'DatasourceSelector': {panelId: "abSpHlVacRm_DrawingPanel"},
				'AssetLocator': {divId: "svgDiv"},
				'AssetTooltip': {handlers: [{assetType: 'rm', datasource: 'ds_ab-sp-hl-vac-rm_drawing_rmLabel1', fields: 'rm.rm_id;rm.count_em;rm.rm_cat;rm.rm_type;rm.area'}]},
				'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
		this.parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickAsset}];
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpHlVacRm_DrawingPanel", this.parameters);
		this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
		
		var selectorId = 'selector_' + 'labels';
		var combo = Ext.get(selectorId);
		combo.dom.onchange = this.onSelectedDatasourceChanged.createDelegate(this, [this, combo]);
		
    },
    
    onSelectedDatasourceChanged:function(selectorControl, combo){
		var selectedValue = combo.dom.value;
		var assetToolTip = this.svgControl.config.addOnsConfig.AssetTooltip.handlers[0];
    	assetToolTip.datasource=selectedValue;
    	var ds = View.dataSources.get(selectedValue);
    	assetToolTip.fields=this.getFieldsOfDatasource(ds);
    	this.svgControl.drawingController.getControl().reload(this.parameters);	
	},
	
	getFieldsOfDatasource: function(ds){
		var toolTipFields = "";
		var fields = ds.fieldDefs.items;
		for(var i=0;i<fields.length;i++){
			if(fields[i].hidden==="false"){
				toolTipFields+=fields[i].fullName+";";
			}
		}
		
		return toolTipFields.substring(0,toolTipFields.lastIndexOf(";"));
	},
    
    abSpHlVacRm_filterConsole_onShowTree: function(){
        filterBlId = this.abSpHlVacRm_filterConsole.getFieldValue('rm.bl_id');
        filterRmCat = this.abSpHlVacRm_filterConsole.getFieldValue('rm.rm_cat');
        
        if (filterBlId) {
            this.abSpHlVacRm_BlTree.addParameter('blId', " = " + "'" + filterBlId + "'");
        }
        else {
            this.abSpHlVacRm_BlTree.addParameter('blId', "IS NOT NULL");
        }
        
        if (filterRmCat) {
            this.abSpHlVacRm_BlTree.addParameter('rmCat', " = " + "'" + filterRmCat + "'");
        }
        else {
            this.abSpHlVacRm_BlTree.addParameter('rmCat', "IS NOT NULL");
        }
        this.abSpHlVacRm_BlTree.refresh();
        this.unloadSvg();
        this.closeDetailPanel();
        this.abSpHlVacRm_rmGrid.clear();
        this.svgControl.getAddOn('InfoWindow').setText(getMessage('drawingPanelTitle1'));
    },
    
	loadSvg: function(bl_id, fl_id, drawingName) {
		
		addParameterConfig(this.parameters);
		
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgName = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
        
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
    	
    	this.svgControl.drawingController.getControl().reload(this.parameters);	
   	},
   	
   	unloadSvg: function(){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgName;
    	this.svgControl.unload(parameters);
    	
   	},
   	
    onClickAsset: function(params, drawingController){
    	var arrayRoomIDs = params['assetId'].split(";");
    	var restriction = new Ab.view.Restriction();
        restriction.addClause("rm.bl_id", arrayRoomIDs[0], "=", true);
        restriction.addClause("rm.fl_id", arrayRoomIDs[1], "=", true);
        restriction.addClause("rm.rm_id", arrayRoomIDs[2], "=", true);
         
        var rmDetailPanel = View.panels.get('abSpHlVacRm_RmDetailPanel');
        rmDetailPanel.refresh(restriction);
        rmDetailPanel.show(true);
        rmDetailPanel.showInWindow({
        	width: 500,
        	height: 250
        });
         
        // toggle the room if clicked
    	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId']);
    },
    
    closeDetailPanel: function(){
        var rmDetailPanel = View.panels.get('abSpHlVacRm_RmDetailPanel');
        rmDetailPanel.closeWindow();
    }
    
});

function generateReport(){
	var filterPanel = View.panels.get("abSpHlVacRm_filterConsole");
    var filterBlId = filterPanel.getFieldValue('rm.bl_id');
    var filterRmCat = filterPanel.getFieldValue('rm.rm_cat');					   
	
	var restriction = " 1=1 ";
    if (filterBlId) {
		restriction =  restriction +  " and  rm.bl_id=${sql.literal('"+filterBlId+"')}";
    }
    if (filterRmCat) {
		restriction = restriction + " and  rm.rm_cat=${sql.literal('"+filterRmCat+"')}" ;
    }
	
	//prepare parameters from filter	for report DataSources
	var parameters = {};
	parameters['consoleRes'] = restriction;

	View.openPaginatedReportDialog("ab-sp-hl-vac-rm-prnt.axvw", null, parameters);	
}

/**
 * event handler when click the floor level of the tree
 * @param {Object} ob
 */
function onFlTreeClick(ob){
    var currentNode = View.panels.get('abSpHlVacRm_BlTree').lastNodeClicked;
    var blId = currentNode.parent.data['bl.bl_id'];
    var flId = currentNode.data['fl.fl_id'];
    
    if (filterRmCat) {
        View.dataSources.get('ds_ab-sp-hl-vac-rm_drawing_rmHighlight').addParameter('rmCat', "rm.rm_cat = '" + filterRmCat + "' AND ");
    }
    else {
        View.dataSources.get('ds_ab-sp-hl-vac-rm_drawing_rmHighlight').addParameter('rmCat', "");
    }
    
    var restriction = new Ab.view.Restriction();
    restriction.addClause("rm.bl_id", blId, "=");
    restriction.addClause("rm.fl_id", flId, "=");
    
    var drawingPanel = View.panels.get('abSpHlVacRm_DrawingPanel');
    var title = String.format(getMessage('drawingPanelTitle2'), blId + "-" + flId);
    var dwgName = currentNode.data['fl.dwgname'];
    controller.loadSvg(blId, flId, dwgName);
    controller.svgControl.getAddOn('InfoWindow').setText(title);
    controller.closeDetailPanel();
    if (filterRmCat) {
        restriction.addClause('rm.rm_cat', filterRmCat, '=', 'AND', true);
    }
    restriction.addClause("rm.dwgname", dwgName, "=");
    View.panels.get('abSpHlVacRm_rmGrid').refresh(restriction);
}

/**
 * add total row
 * @param {Object} parentElement
 */
function addTotalRow(parentElement){
    if (this.rows.length < 2) {
        return;
    }
    
    var totalArea = 0;
    for (var i = 0; i < this.rows.length; i++) {
        var row = this.rows[i];
        var areaRowValue = row['rm.area'];
        
        if (row['rm.area.raw']) {
            areaRowValue = row['rm.area.raw'];
        }
        totalArea += parseFloat(areaRowValue);
    }
    
    totalArea = totalArea.toFixed(2);
    // create new grid row and cells containing statistics
    var gridRow = document.createElement('tr');
    parentElement.appendChild(gridRow);
    // column 1,2,3: empty	
    addColumn(gridRow, 3);
    // column 4: total room area
    addColumn(gridRow, 1, totalArea.toString());
    // column 5: 'Total' title
    addColumn(gridRow, 1, getMessage('total'));
    // column 6,7,8,9: empty		
    addColumn(gridRow, 4);
}

/**
 * add column
 * @param {Object} gridRow
 * @param {int} count
 * @param {String} text
 */
function addColumn(gridRow, count, text){
    for (var i = 0; i < count; i++) {
        var gridCell = document.createElement('th');
        if (text) {
            gridCell.innerHTML = text;
            gridCell.style.textAlign = 'right';
            gridCell.style.color = 'red';
        }
        gridRow.appendChild(gridCell);
    }
}
