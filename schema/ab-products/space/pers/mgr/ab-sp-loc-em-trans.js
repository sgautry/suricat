/**
 * @author Guo Jiangtao
 */
var controller = View.createController('abSpLocEmTrans_controller', {

	//used for parameter of the datasource
	date : '',
	
	//all drawing names
	dwgNames : [],
	bl_ids : [],
	fl_ids : [],
	rm_ids : [],
	rm_colors : [],
	
	//current drawing name when loading more that one drawing 
	currentDrawing : '',
	
	dateToAddField:'rmpct.date_start',
	dateToUpdateField:'rmpct.date_start',
	
	//filter console
	console:null,
	//timteLine panel.
	timeLine:null,

	parameters: null,
	zoom: false, 
	blId:null,
	flId:null,
	rmId:null,
	dwgname:null,
	plan_type:"1 - ALLOCATION",
	showTooltip: true,
	
	afterViewLoad : function() {
		
		//initialize svg control
		this.parameters = new Ab.view.ConfigObject();
		this.parameters['highlightParameters'] = [{'view_file':"ab-sp-loc-em-trans.axvw",'hs_ds':'ds_ab-sp-loc-em_drawing_rmHighlight', 'label_ds':'abSpLocEmTrans_rmLabel'}];
		this.parameters['bordersHighlightSelector'] = 'false'; 
		this.parameters['highlightFilterSelector'] = 'false';
		this.parameters['allowMultipleDrawings'] = 'true';
		this.parameters['multipleSelectionEnabled'] = 'true';
		this.parameters['divId'] = "svgDiv";
		this.parameters['orderByColumn'] = true;
		this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
		this.parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				'DatasourceSelector': {panelId: "abSpLocEmTrans_floorPlan"},
				'AssetLocator': {divId: "svgDiv"},
				'AssetTooltip': {handlers: [{assetType: 'rm', datasource: 'abSpLocEmTrans_rmLabel', fields: 'rm.rm_id;rm.dv_id;rm.dp_id;em.em_id'}]},
				'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}};
		this.svgControl = new Drawing.DrawingControl("svgDiv", "abSpLocEmTrans_floorPlan", this.parameters);
	},
	
	afterInitialDataFetch : function() {

		//set current date to the console
		this.date = getCurrentDate();
		this.abSpLocEmTrans_console.setFieldValue('rmpct.date_start', this.date);
		
		//set datasource parameter
		this.setParameters();
		
		//set timeline button
		this.console=this.abSpLocEmTrans_console;
		this.timeLine=this.timeLineButton;
		this.dateAction = this.timeLine.actions.get('currentDate');
		this.dateAction.setTitle(this.console.getFieldElement(this.dateToAddField).value);
		this.date=this.abSpLocEmTrans_console.getFieldValue('rmpct.date_start');
		setTimeTitle(this.timeLine);
	
		//show legend panel
		$('abSpLocEmTrans_highlightLegend_legendDiv').style.display = '';
	},

	abSpLocEmTrans_console_onShow : function() {
		
		//check the required date field
		if(!this.abSpLocEmTrans_console.canSave()){
			return;
		}
		
		//reset the timeline date from console
		this.dateAction.setTitle(this.console.getFieldElement(this.dateToAddField).value);
		
		//create restriction from console
		var restriction = new Ab.view.Restriction();
		restriction.addClause("rmpct.primary_em", 1, "=");
		var empId = this.abSpLocEmTrans_console.getFieldValue("rmpct.em_id");
		if (empId) {
			restriction.addClause("rmpct.em_id", empId + '%', "LIKE");
		}
		
		//set datasource parameter
		this.setParameters();
		
		//refresh the employee list
		this.abSpLocEmTrans_em_grid.refresh(restriction);

		// clear the drawing in the drawing panel and rows in the employee
		this.unloadAllSvg();
		this.abSpLocEmTrans_emDetails.clear();
		
		this.abSpLocEmTrans_floorPlan.isLoadDrawing = false;
		
        var records = this.abSpLocEmTrans_em_grid.rows.length;
        if(records==1){
        	var drawingPanel = controller.abSpLocEmTrans_floorPlan;
        	this.unloadAllSvg();
        	var grid = controller.abSpLocEmTrans_em_grid;
    		var emId = grid.rows[0]['rmpct.em_id'];
    		var restriction = new Ab.view.Restriction();
    		restriction.addClause("rmpct.em_id", emId, "=");
    		drawingPanel.isLoadDrawing = true;
    		controller.abSpLocEmTrans_emDetails.refresh(restriction);
        }

	},

	abSpLocEmTrans_emDetails_afterRefresh : function() {
		
		this.dwgNames = [];
		this.bl_ids = [];
		this.fl_ids = [];
		this.rm_ids = [];
		this.rm_colors = [];
		var tempNames = '';
		//get the drawing names and related building and floor
		var rows = this.abSpLocEmTrans_emDetails.rows;
		for ( var i = 0; i < rows.length; i++) {
			var dwgName = rows[i]['rm.dwgname'];
			var bl_id = rows[i]['rm.bl_id'];
			var fl_id = rows[i]['rm.fl_id'];
			var rm_id = rows[i]['rm.rm_id'];
			var rm_colors = rows[i]['rmpct.primary_em.raw']=='1'?'#FFFF00':'#0000FF';//yellow 0xFFFF00;blue 0x0000FF;
			if (dwgName && tempNames.indexOf(dwgName) < 0) {
				tempNames += ',' + tempNames;
				this.dwgNames.push(dwgName);
				this.bl_ids.push(bl_id);
				this.fl_ids.push(fl_id);
				this.rm_ids.push(rm_id);
				this.rm_colors.push(rm_colors);
			}
		}
		
		this.addMultipleDrawings();
	},
	
	addMultipleDrawings: function() {
		
		for ( var i = 0; i < this.dwgNames.length; i++) {
			this.currentDrawing = this.dwgNames[i];
			this.loadSvg(this.bl_ids[i], this.fl_ids[i], this.currentDrawing);
			
			var assetId = this.bl_ids[i] + ';' + this.fl_ids[i] + ';' + this.rm_ids[i];
			var svgId = this.svgControl.divId+"-"+this.currentDrawing+"-svg";
			this.svgControl.getDrawingController().getController("HighlightController").highlightAsset(assetId, {svgId: svgId, color: this.rm_colors[i], persistFill: true, overwriteFill: true});
		}
		
	},

	/**
	 * set the datasource patameters
	 */
	setParameters : function() {
		this.date=this.abSpLocEmTrans_console.getFieldValue('rmpct.date_start');
		var list = ['abSpLocEmTrans_em_grid', 'abSpLocEmTrans_emDetails', 'abSpLocEmTrans_rmLabel'];
		for ( var i = 0; i < list.length; i++) {
			var control = View.dataSources.get(list[i]);
			if (!control) {
				control = View.panels.get(list[i]);
			}
			control.addParameter('date', this.date);
		}
	},
	
	/**
	 * call in file : ab-sp-timeline-common.js
	 */
	afterTimeButtonClick : function(){
		this.unloadAllSvg();
		this.setParameters();
		showEmLocations();
		this.reloadSvg();
	},
	
	loadSvg: function(bl_id, fl_id, drawingName) {
		this.blId = bl_id;
		this.flId = fl_id;
		this.dwgname = drawingName;
		
		//set drawing parameters
		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':this.blId, 'fl_id':this.flId};
        parameters['drawingName'] = this.dwgname;

        // load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
   	},
   	  
   	reloadSvg: function(){
   		
   		this.svgControl.drawingController.getControl().reload(this.parameters);	
   	},
   	
   	unloadSvg: function(bl_id, fl_id, drawingName){
   		var parameters = {};
        parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id':fl_id};
        parameters['drawingName'] = drawingName;
    	this.svgControl.unload(parameters);

   	},
   	
   	unloadAllSvg: function(){
    	
   		var rows = this.abSpLocEmTrans_emDetails.rows;
		for ( var i = 0; i < rows.length; i++) {
			var dwgName = rows[i]['rm.dwgname'];
			var bl_id = rows[i]['rm.bl_id'];
			var fl_id = rows[i]['rm.fl_id'];
            this.unloadSvg(bl_id, fl_id, dwgName);
        }

   	}
	
});

/**
 * Open em photo by selected record row.
 */
function openDialogForEm(){
    var grid = View.panels.get('abSpLocEmTrans_emDetails');
    var em_id = grid.rows[grid.selectedRowIndex]["em.em_id"];
    var em_photo = grid.rows[grid.selectedRowIndex]["em.em_photo"];
    
    var restriction = new Ab.view.Restriction();
    restriction.addClause("em.em_id", em_id, "=");
    var distinctPanel = View.panels.get('emPhotoForm');
    distinctPanel.refresh(restriction);
    
    if (valueExistsNotEmpty(em_photo)) {
		distinctPanel.showImageDoc('image_field', 'em.em_id', 'em.em_photo');
	}
	else {
		distinctPanel.fields.get('image_field').dom.src = null;
		distinctPanel.fields.get('image_field').dom.alt = getMessage('noImage');
	}
    
    distinctPanel.showInWindow({
        width: 800,
        height: 600
    });
}
/**
 * show all employeee locations in the grid abSpLocEmTrans_emDetails
 */
function showEmLocations() {
	var drawingPanel = controller.abSpLocEmTrans_floorPlan;
//	drawingPanel.clear();
	controller.unloadAllSvg();
	var grid = controller.abSpLocEmTrans_em_grid;
	if(grid.selectedRowIndex!=-1){
		var emId = grid.rows[grid.selectedRowIndex]['rmpct.em_id'];
		var restriction = new Ab.view.Restriction();
		restriction.addClause("rmpct.em_id", emId, "=");
		drawingPanel.isLoadDrawing = true;
		controller.abSpLocEmTrans_emDetails.refresh(restriction);
	}
}

function generateReport(){
	var grid = View.panels.get("abSpLocEmTrans_emDetails");
	var result = "";
    var records = grid.rows;
    for (var j = 0; j < records.length; j++) {
        var row = records[j];
        result+="'"+row["rmpct.em_id"]+"',"
    }    
    var restriction = " 1=1 ";
    if(result!=""){
    	result = result.substring(0,result.length-1);
        restriction += " and rmpct.em_id in (" + result + ")";
    }
    
    //kb:3037291,1. When generate paginate report, it should show employee's location based on the date selected.
	var parameters = {
	    	'existsBlflrm':' and '+restriction,
	    	'dateParameter':getDateWithISOFormat(controller.console.getFieldElement(controller.dateToAddField).value)
    	}; 
	
    View.openPaginatedReportDialog("ab-sp-loc-em-trans-print.axvw", null , parameters);
}




