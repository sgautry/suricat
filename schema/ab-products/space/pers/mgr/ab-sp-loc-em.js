/**
 * @author keven.xi
 */
var locEmpController = View.createController('locEmp', {
    parameters: null,
    zoom: false,
    blId: null,
    flId: null,
    rmId: null,
    dwgname: null,
    plan_type: "1 - ALLOCATION",
    showTooltip: true,
    afterViewLoad: function () {
        // enable selectionAcrossPagesEnabled to restore previous row selections
        this.locateEmployee_employees.selectionAcrossPagesEnabled = true;
        this.locateEmployee_employees.addEventListener('afterGetData', this.afterGetDataEvent, this);
        //initialize svg control
        this.parameters = new Ab.view.ConfigObject();
        this.parameters['highlightParameters'] = [{'view_file': "ab-sp-loc-em.axvw", 'label_ds': 'ds_ab-sp-loc-em_drawing_rmLabel'}];
        this.parameters['bordersHighlightSelector'] = 'false';
        this.parameters['highlightFilterSelector'] = 'false';
        this.parameters['allowMultipleDrawings'] = 'true';
        this.parameters['multipleSelectionEnabled'] = 'true';
        this.parameters['divId'] = "svgDiv";
        this.parameters['orderByColumn'] = true;
        this.parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
        this.parameters['addOnsConfig'] = {
            'NavigationToolbar': {divId: "svgDiv"},
            'DatasourceSelector': {panelId: "locateEmployee_cadPanel"},
            'AssetLocator': {divId: "svgDiv"},
            'AssetTooltip': {handlers: [{assetType: 'rm', datasource: 'ds_ab-sp-loc-em_drawing_rmLabel', fields: 'rm.rm_id;rm.dv_id;rm.dp_id;em.em_id'}]}
        };
        this.svgControl = new Drawing.DrawingControl("svgDiv", "locateEmployee_cadPanel", this.parameters);
    },
    afterInitialDataFetch: function () {
        var obtThis = this;
        // overrides Grid.onChangeMultipleSelection to load a drawing with a room, not zoomed in
        this.locateEmployee_employees.addEventListener('onMultipleSelectionChange', function (row) {
            var records = View.panels.get("locateEmployee_employees").getSelectedRows();
            if (valueExistsNotEmpty(row["rm.bl_id"]) && valueExistsNotEmpty(row["rm.fl_id"]) && valueExistsNotEmpty(row["rm.dwgname"])) {
                var assetId = row["rm.bl_id"] + ";" + row["rm.fl_id"] + ";" + row["rm.rm_id"],
                    svgId = locEmpController.svgControl.divId + "-" + row["rm.dwgname"] + "-svg";
                if (row.row.isSelected()) {
                    var drawingSvgControl = locEmpController.svgControl.getSvgControl(row["rm.dwgname"]);
                    if (valueExists(drawingSvgControl) && drawingSvgControl.isDrawingLoaded) {
                        // do nothing, drawing is loaded, highlight the employee
                    } else {
                        locEmpController.loadSvg(row["rm.bl_id"], row["rm.fl_id"], row["rm.dwgname"]);
                    }
                    // Call the drawing control to highlight the selected room
                    locEmpController.svgControl.getDrawingController().getController("HighlightController").highlightAsset(assetId, {svgId: svgId, color: 'yellow', persistFill: true, overwriteFill: true});
                } else {
                    if (locEmpController.isUnLoadDrawing(row, records)) {
                        locEmpController.unloadSvg(row["rm.bl_id"], row["rm.fl_id"], row["rm.dwgname"]);
                    } else {
                        // Call the drawing control to clear highlight for the selected room
                        locEmpController.svgControl.getDrawingController().getController("HighlightController").clearAsset(assetId, {svgId: svgId, color: 'yellow', persistFill: false, overwriteFill: true});
                    }
                }
            }
            obtThis.showEmployeesDetails(records);
        })
    },
    /**
     * Called on grid refresh. (console filter, miniconsole filter, sorting, page index).
     * Unload all drawings to restore previous selections.
     */
    afterGetDataEvent: function () {
        // unload all drawings
        var drawingControl = locEmpController.svgControl.getDrawingController().getControl();
        _.each(drawingControl.drawings, function (drawing) {
            this.svgControl.unload(drawing.config);
        }, this);
        this.empDetails.clear();
    },
    showEmployeesDetails: function (records) {
        var res = new Ab.view.Restriction();
        for ( var i = 0; i < records.length; i++ ) {
            var empCode = records[i]["em.em_id"];
            res.addClause("em.em_id", empCode, '=', 'OR');
        }
        var empDetailsGrid = View.getControl('', 'empDetails');
        if (res.clauses.length > 0) {
            empDetailsGrid.refresh(res, null, false);
        } else {
            empDetailsGrid.clear();
        }
    },
    emFilterPanel_onShow: function () {
        //clear the drawing in the drawing panel and rows in the employee details panel
        var records = this.locateEmployee_employees.getSelectedRows();
        this.unloadAllSvg(records);
        this.empDetails.clear();
        var restriction = new Ab.view.Restriction();
        var empId = this.emFilterPanel.getFieldValue("em.em_id");
        if (empId) {
            restriction.addClause("em.em_id", empId + '%', "LIKE");
        }
        this.locateEmployee_employees.refresh(restriction);
        this.locateEmployee_employees.enableSelectAll(false);
        records = this.locateEmployee_employees.rows.length;
        if (records === 1) {
            View.panels.get("locateEmployee_cadPanel").highlightAssets(null, this.locateEmployee_employees.rows[0]);
            this.showEmployeesDetails(this.locateEmployee_employees.rows);
        }
    },
    isUnLoadDrawing: function (row, records) {
        for ( var i = 0; i < records.length; i++ ) {
            var bl = records[i]["rm.bl_id"];
            var fl = records[i]["rm.fl_id"];
            var dwgname = records[i]["rm.dwgname"];
            if (row["rm.bl_id"] === bl && row["rm.fl_id"] === fl && row["rm.dwgname"] === dwgname) {
                return false;
            }
        }
        return true;
    },
    emPhotoForm_afterRefresh: function () {
        var distinctPanel = View.panels.get('emPhotoForm');
        var em_photo = distinctPanel.getFieldValue('em.em_photo').toLowerCase();
        if (valueExistsNotEmpty(em_photo)) {
            distinctPanel.showImageDoc('image_field', 'em.em_id', 'em.em_photo');
        } else {
            distinctPanel.fields.get('image_field').dom.src = null;
            distinctPanel.fields.get('image_field').dom.alt = getMessage('noImage');
        }
    },
    loadSvg: function (bl_id, fl_id, drawingName) {
        this.blId = bl_id;
        this.flId = fl_id;
        this.dwgName = drawingName;
        //set drawing parameters
        var parameters = {};
        parameters['pkeyValues'] = {'bl_id': this.blId, 'fl_id': this.flId};
        parameters['drawingName'] = this.dwgName;
        // load SVG from server and display in SVG panel's  <div id="svgDiv">
        this.svgControl.load(parameters);
    },
    unloadSvg: function (bl_id, fl_id, drawingName) {
        var parameters = {};
        parameters['pkeyValues'] = {'bl_id': bl_id, 'fl_id': fl_id};
        parameters['drawingName'] = drawingName;
        this.svgControl.unload(parameters);
    },
    unloadAllSvg: function (records) {
        for ( var i = 0; i < records.length; i++ ) {
            var bl = records[i]["rm.bl_id"];
            var fl = records[i]["rm.fl_id"];
            var dwgName = records[i]["rm.dwgname"];
            this.unloadSvg(bl, fl, dwgName);
        }
    }
});

function generateReport() {
    var grid = View.panels.get("locateEmployee_employees");
    var records = grid.getSelectedRows();
    if (!records || records.length === 0) {
        View.alert(getMessage('noEmSelected'));
        return;
    }
    var result = "";
    for ( var j = 0; j < records.length; j++ ) {
        var row = records[j];
        result += "'" + row["em.em_id"] + "',"
    }
    var restriction = "";
    if (valueExistsNotEmpty(result)) {
        result = result.substring(0, result.length - 1);
        restriction += " em.em_id in (" + result + ")";
    }
    var parameters = {'existsBlflrm': ' and ' + restriction};
    View.openPaginatedReportDialog("ab-sp-loc-em-print.axvw", null, parameters);
}