View.createController('abApEqPlansCtrl', {
    drawingController: null,
    afterViewLoad: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['rm', 'eq'],
            highlightParameters: [{
                'view_file': "ab-comm-eq-plans.axvw",
                'hs_ds': "ds_abApEqPlans_drawing",
                'label_ds': 'ds_abApEqPlans_drawingLabel',
                showSelector: false
            }],
            assetTooltip: [
                {assetType: 'rm', datasource: 'ds_abApEqPlans_rmTooltip', fields: 'rm.rm_id;rm.rm_std'},
                {assetType: 'eq', datasource: 'eqTooltip', fields: 'eq.eq_id;eq.eq_std;eq.use1;eqstd.description;eqstd.category;eqstd.mfr;eqstd.modelno'}
            ],
            layersPopup: {
                layers: "rm-assets;rm-labels;eq-assets;eq-labels;gros-labels;background",
                defaultLayers: "rm-assets;rm-labels;eq-assets;eq-labels;gros-assets;background"
            },
            listeners: {
                onClickAssetAfter: function (drawingController, svgId, selectedAssetId, assetType, selectedAssets, selected) {
                    if ('eq' === assetType && selected) {
                        var restriction = new Ab.view.Restriction({'eq.eq_id': selectedAssetId});
                        View.openDialog('ab-comm-eq-plans-detail.axvw', restriction, false, {
                            width: 600,
                            height: 800
                        });
                    }
                }
            }
        });
    },
    showFloor: function () {
        var currentNode = this.panel_abApEqPlans_blTree.lastNodeClicked;
        var buildingId = currentNode.parent.data['bl.bl_id'],
            floorId = currentNode.data['bl.state_id'],
            drawingName = currentNode.data['bl.dwgname'];
        this.showDrawing(buildingId, floorId, drawingName);
        this.refreshEquipmentPanel(buildingId, floorId);
    },
    showDrawing: function (buildingId, floorId, drawingName) {
        this.drawingController.showDrawing({
            pkeyValues: {blId: buildingId, flId: floorId},
            drawingName: drawingName
        });
    },
    refreshEquipmentPanel: function (buildingId, floorId) {
        var restriction = new Ab.view.Restriction({
            'eq.bl_id': buildingId,
            'eq.fl_id': floorId
        });
        this.panel_abApEqPlans_eqDetails.refresh(restriction);
    },
    /**
     * Generate paginated report for user selection
     */
    panel_abApEqPlans_console_onPaginatedReport: function () {
        var siteId = this.panel_abApEqPlans_console.getFieldValue("bl.site_id"),
            blId = this.panel_abApEqPlans_console.getFieldValue("bl.bl_id");
        var parameters = null;
        var consoleRestrictionForEq = "";
        if (valueExistsNotEmpty(siteId)) {
            consoleRestrictionForEq += "(eq.bl_id IN (SELECT DISTINCT bl_id FROM bl WHERE site_id='" + siteId + "'))";
        }
        if (valueExistsNotEmpty(blId)) {
            consoleRestrictionForEq += (consoleRestrictionForEq != "" ? " AND " : "");
            consoleRestrictionForEq += "eq.bl_id = '" + blId + "'";
        }
        if (consoleRestrictionForEq != "") {
            consoleRestrictionForEq = "(" + consoleRestrictionForEq + ")";
            parameters = {
                'consoleRestrictionForEq': consoleRestrictionForEq
            };
        }
        View.openPaginatedReportDialog('ab-comm-eq-plans-pgrp.axvw', null, parameters);
    }
});