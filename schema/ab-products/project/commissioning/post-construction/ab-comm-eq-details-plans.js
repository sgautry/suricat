View.createController('commEqDetailsPlansCtrl', {
    drawingController: null,
    afterViewLoad: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['rm', 'eq'],
            showPanelTitle: true,
            assetTooltip: [
                {
                    assetType: 'rm',
                    datasource: 'commEqDetailsPlans_rmTooltip',
                    fields: 'rm.rm_id;rm.rm_std'
                },
                {
                    assetType: 'eq',
                    datasource: 'commEqDetailsPlans_eqLabels',
                    fields: 'eq.eq_id;eq.eq_std;eq.use1;eqstd.description;eqstd.category;eqstd.mfr;eqstd.modelno'
                }
            ],
            listeners: {
                afterDrawingLoaded: function (drawingController, svgId) {
                    // highlight all the equipments
                    drawingController.svgControl.getController("HighlightController").highlightAssetsByType('eq', {
                    	color: '#45F8F8'
                    });
                }
            }
        });
    },
    afterInitialDataFetch: function () {
        this.refreshDrawing();
    },
    refreshDrawing: function () {
        this.drawingController.clearDrawings();
        var record = View.getOpenerView().panels.get('commEqDetailsForm').getRecord();
        if (!valueExists(record)) return;
        var buildingId = record.getValue('eq.bl_id'),
            floorId = record.getValue('eq.fl_id'),
            roomId = record.getValue('eq.rm_id'),
            equipmentId = record.getValue('eq.eq_id');
        this.showDrawing(buildingId, floorId, roomId, equipmentId);
    },
    showDrawing: function (buildingId, floorId, roomId, equipmentId) {
        this.drawingController.showDrawing({
            pkeyValues: {blId: buildingId, flId: floorId},
            findAssets: [{
                assetType: 'rm',
                assetIds: [roomId]
            }],
            highlightAssets: [{
                assetType: 'eq',
                assetIds: [equipmentId]
            }]
        });
    }
});