/**
 * Common base controller for CA highlight reports.
 */
var reportBaseController = Ab.view.Controller.extend({
    mapControl: null,
    drawingController: null,
    afterViewLoad: function () {
        // set message parameter for projectTree_panel panel when no drawing is available
        this.projectTree_panel.addParameter('noDrawing', getMessage('noDrawing'));
        // init map control
        this.initMapControl();
    },
    afterInitialDataFetch: function () {
        // show only map panel
        this.showPanels(true, false, false);
    },
    /**
     * Init map control.
     */
    initMapControl: function () {
        var configObject = new Ab.view.ConfigObject();
        configObject.mapImplementation = 'Esri';
        this.mapControl = new Ab.leaflet.Map('htmlMap', 'objMap', configObject);
        var basemapLayerMenu = this.htmlMap.actions.get('basemapLayerMenu');
        basemapLayerMenu.clear();
        var basemapLayers = this.mapControl.getBasemapLayerList();
        for ( var i = 0; i < basemapLayers.length; i++ ) {
            basemapLayerMenu.addAction(i, basemapLayers[i], this.switchBasemapLayer.createDelegate(this));
        }
        this.mapControl.createMarkers(
            'blMarker_ds',
            ['bl.bl_id'],
            ['bl.lon', 'bl.lat'],
            'bl.name',
            ['bl.site_id', 'bl.bl_id', 'bl.name', 'bl.address'],
            {
                markerActionTitle: getMessage('labelShowDetails'),
                markerActionCallback: this.onBuildingDetails.createDelegate(this)
            }
        );
    },
    /**
     * Switch the mapControl layer based on the passed in layer name.
     * @param item - map layers.
     */
    switchBasemapLayer: function (item) {
        this.mapControl.switchBasemapLayer(item.text);
    },
    /**
     * Init drawing control.
     */
    initDrawingControl: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['rm'],
            assetTooltip: [
                {
                    assetType: 'rm',
                    datasource: 'rmLabel_ds',
                    fields: 'rm.rm_id;rm.rm_cat;rm.rm_type;rm.area'
                }
            ],
            layersPopup: {collapsed:true, divId: "svgDiv", defaultLayers:"rm-assets;rm-labels;background"},
            showPanelTitle: true
        });
    },
    /**
     * Apply console restriction and refresh the page.
     */
    filterPanel_onShow: function () {
        var restriction = this.filterToSQLString(this.filterPanel);
        restriction = restriction.replace(/activity_log\./g, "a.");
        this.projectTree_panel.addParameter('consoleRestriction', restriction);
        this.projectTree_panel.addParameter('consoleRestrictionForCount', restriction.replace(/a\./g, "al."));
        this.projectTree_panel.refresh();
        this.showPanels(true, false, false);
        this.resetMap();
    },
    /**
     * Reset map to initial state.
     */
    resetMap: function () {
        // clear markers
        this.mapControl.clearMarkers();
        // reset the zoom of the map.
        this.mapControl.mapClass.map.setZoom(3);
        // hide geocode button
        this.htmlMap.actions.get('geocode').show(false);
    },
    /**
     * Show/ hide panels.
     * @param {boolean} showMap - Show map panel.
     * @param {boolean} showDrawing - Show drawing panel.
     * @param {boolean} showReport - Show asset report grid.
     */
    showPanels: function (showMap, showDrawing, showReport) {
        this.htmlMap.show(showMap);
        this.drawingPanel.show(showDrawing);
        this.reportPanel.show(showReport);
    },
    /**
     * Generate paginated report for user selection.
     */
    filterPanel_onPaginatedReport: function () {
        if (this.projectTree_panel._nodes.length == 0) {
            View.showMessage(getMessage('err_no_project'));
        } else {
            if (!valueExistsNotEmpty(this.reportViewName)) {
                View.showMessage('noReportViewDefined');
                return;
            }
            this.createPaginatedReport(this.reportViewName, this.filterPanel);
        }
    },
    /**
     * Show building on map.
     * @param {Ab.view.Restriction} restriction - building restriction.
     */
    showBuilding: function (restriction) {
        if (this.mapControl) {
            this.showPanels(true, false, true);
            // show geocode action
            this.htmlMap.actions.get('geocode').show(!this.projectTree_panel.lastNodeClicked.geocoded);
            // clear map markers
            this.mapControl.clearMarkers();
            // show map markers
            this.mapControl.showMarkers('blMarker_ds', restriction);
        }
    },
    /**
     * Geocode a building.
     */
    htmlMap_onGeocode: function () {
        var currentNode = this.projectTree_panel.lastNodeClicked;
        if (!currentNode.geocoded) {
            var geocodeControl = new Ab.leaflet.EsriGeocoder(this.mapControl);
            var restriction = new Ab.view.Restriction();
            if (valueExistsNotEmpty(currentNode)) {
                var blId = currentNode.data['bl.bl_id'];
                restriction.addClause('bl.bl_id', blId, '=');
            }
            geocodeControl.geocode('geoBuilding_ds', restriction, 'bl', 'bl.bl_id', ['bl.lat', 'bl.lon'], ['bl.address1', 'bl.city_id', 'bl.state_id', 'bl.zip', 'bl.ctry_id'], true);
            this.projectTree_panel.refresh();
        }
    },
    /**
     * Show building details map marker action.
     * @param {string} buildingId - Building id.
     */
    onBuildingDetails: function (buildingId) {
        var restriction = new Ab.view.Restriction();
        restriction.addClause('bl.bl_id', buildingId, '=');
        this.blDetails_panel.refresh(restriction);
        this.blDetails_panel.showInWindow({
            width: 1200,
            closeButton: true
        });
    },
    /**
     * Displays the drawing.
     */
    displayDrawing: function (blId, flId, drawingName) {
        if (!valueExists(this.drawingController)) {
            this.initDrawingControl();
        }
        this.drawingController.showDrawing({
            pkeyValues: {blId: blId, flId: flId},
            drawingName: drawingName,
            highlightAssets: [{
                assetType: 'rm',
                assetIds: this.getAllRoomIdsForDrawing(),
                color: 'yellow'
            }]
        });
    },
    /**
     * Refresh report panel.
     * @param {Ab.view.Restriction} restriction
     */
    refreshReportPanel: function (restriction) {
        this.reportPanel.refresh(restriction);
    },
    /**
     * Creates an array of rooms ids to highlight.
     * @returns {Array} room ids.
     */
    getAllRoomIdsForDrawing: function () {
        var roomIds = [];
        var parameters = this.reportPanel.getParametersForRefresh();
        parameters.recordLimit = -1;
        parameters.indexField = "";
        var rms = this.reportPanel.getData(parameters).data.records;
        for ( var i = 0; i < rms.length; i++ ) {
            var rm = rms[i];
            roomIds.push(rm["activity_log.rm_id"]);
        }
        return roomIds;
    },
    /**
     * Start the paginated report job passing user console restriction
     * @param {Object} fileName - name of the paginated report definition file
     * @param {Object} consolePanel - console restriction panel
     */
    createPaginatedReport: function (fileName, consolePanel) {
        var consoleRestriction = this.filterToSQLString(consolePanel);
        var drawPanelRest = consoleRestriction.replace(/activity_log\./g, "a.");
        //prepare a custom printable restrictions - paired title and value (localized)
        var printableRestrictions = [];
        var siteId = consolePanel.getFieldValue('activity_log.site_id');
        if (siteId) {
            printableRestrictions.push({'title': getMessage('siteId'), 'value': siteId});
        }
        var blId = consolePanel.getFieldValue('activity_log.bl_id');
        if (blId) {
            printableRestrictions.push({'title': getMessage('blId'), 'value': blId});
        }
        var flId = consolePanel.getFieldValue('activity_log.fl_id');
        if (flId) {
            printableRestrictions.push({'title': getMessage('flId'), 'value': flId});
        }
        var csiId = consolePanel.getFieldValue('activity_log.csi_id');
        if (csiId) {
            printableRestrictions.push({'title': getMessage('csiId'), 'value': csiId});
        }
        var parameters = {
            'consoleRestriction': consoleRestriction,
            'drawPanelRest': drawPanelRest,
            'printRestriction': true,
            'printableRestriction': printableRestrictions
        };
        View.openPaginatedReportDialog(fileName, null, parameters);
    },
    /**
     * Read console filter as sql string.
     * @param {Object} consolePanel
     */
    filterToSQLString: function (consolePanel) {
        var consoleRestriction = "";
        var site_id = consolePanel.getFieldValue('activity_log.site_id');
        if (valueExistsNotEmpty(site_id)) {
            consoleRestriction += " AND activity_log.site_id = '" + site_id + "'";
        }
        var bl_id = consolePanel.getFieldValue('activity_log.bl_id');
        if (valueExistsNotEmpty(bl_id)) {
            consoleRestriction += " AND activity_log.bl_id = '" + bl_id + "'";
        }
        var fl_id = consolePanel.getFieldValue('activity_log.fl_id');
        if (valueExistsNotEmpty(fl_id)) {
            consoleRestriction += " AND activity_log.fl_id = '" + fl_id + "'";
        }
        var csi_id = consolePanel.getFieldValue('activity_log.csi_id');
        if (valueExistsNotEmpty(csi_id)) {
            consoleRestriction += " AND activity_log.csi_id = '" + csi_id + "'";
        }
        return (consoleRestriction);
    }
});
/**
 * Overwrite function to make add project on node restriction.
 * @param {Object} parentNode
 * @param {Object} level
 */
Ab.tree.TreeControl.prototype._createRestrictionForLevel = function (parentNode, level) {
    var restriction = this.createRestrictionForLevel(parentNode, level);
    if (!restriction) {
        restriction = new Ab.view.Restriction();
        // add the tree restriction to parameter list if not null.
        if (this.restriction && this.restriction.clauses != undefined && this.restriction.clauses.length > 0) {
            restriction.addClauses(this.restriction, true);
        }
        // add the tree level's restriction to parameter list if not null.
        var levelRest = this.getRestrictionForLevel(level);
        if (levelRest && levelRest.clauses != undefined && levelRest.clauses.length > 0) {
            restriction.addClauses(levelRest, true);
        }
        // add the parent node's restriction to parameter list. it should always contain something
        if (!parentNode.isRoot()) {
            if (this.type == 'hierTree' || this.type == 'selectValueHierTree') {
                restriction.addClauses(parentNode.restriction, true);
            } else {
                if (this._panelsData[level].useParentRestriction == true) {
                    restriction.addClauses(parentNode.restriction, true);
                }
            }
        }
        // bldg level, we must add project
        if (level == 2 && parentNode.parent.data['city.city_id'] != undefined) {
            restriction.addClause('bl.city_id', parentNode.parent.data['city.city_id']);
        }
        //floor level, we must add project and site
        if (level == 3 && parentNode.parent.data['site.site_id'] != undefined && parentNode.parent.parent.data['city.city_id'] != undefined) {
            restriction.addClause('rm.site_id', parentNode.parent.data['site.site_id']);
            restriction.addClause('rm.city_id', parentNode.parent.parent.data['city.city_id']);
        }
    }
    return restriction;
};
/**
 * Check if current building is geocoded (lat, lon exist).
 * If not the not_geocoded icon is added to current node
 * @param {Object} treeNode - tree node
 */
function afterGeneratingTreeNode(treeNode) {
    if (treeNode.level.levelIndex == 2 && treeNode.data['bl.bl_id'] != undefined) {
        var geocoded = true;
        var labelText = treeNode.label;
        var itemId = treeNode.data['bl.bl_id'];
        var parameters = {
            tableName: 'bl',
            fieldNames: toJSON(['bl.bl_id', 'bl.lat', 'bl.lon']),
            restriction: toJSON({
                'bl.bl_id': itemId
            })
        };
        var result = Workflow.call('AbCommonResources-getDataRecords', parameters);
        if (result.code == 'executed') {
            geocoded = (valueExistsNotEmpty(result.data.records[0]['bl.lat']) && valueExistsNotEmpty(result.data.records[0]['bl.lon']));
        } else {
            Workflow.handleError(result);
        }
        if (!geocoded) {
            labelText = labelText + "<img alt='" + getMessage('not_geocoded') + "' border='0' src='/archibus/schema/ab-system/graphics/no_geocode.png'/>";
        }
        treeNode.geocoded = geocoded;
        treeNode.setUpLabel(labelText);
    }
}