View.extendController('caHighRmLS', reportBaseController, {
    reportViewName: 'ab-ca-hl-bl-rm-w-safety-pgrp.axvw',
    afterInitialDataFetch: function () {
        //AbProjCommissioning
        if (this.view.taskInfo.activityId == 'AbProjCommissioning') {
            View.setTitle(getMessage('commTitle'));
            this.projectTree_panel.addParameter('projecttype', 'COMMISSIONING');
            this.projectTree_panel.addParameter('activitytype', '%');
            this.projectTree_panel.addParameter('condpriority', '%');
            this.projectTree_panel.refresh();
            this.reportPanel.addParameter('condpriority', '%');
            this.reportViewName = 'ab-comm-hl-bl-rm-w-safety-pgrp.axvw';
        }
        this.showPanels(true, false, false);
    },
    /**
     * Show building details for selected building.
     * @param {Object} node - building node.
     */
    showBuildingDetails: function (node) {
        var blId = node.restriction.clauses[0].value;
        var projectId = this.projectTree_panel.lastNodeClicked.getAncestor().getAncestor().data['city.city_id'];
        if (this.filterPanel.restriction) {
            this.reportPanel.addParameter("consoleRestriction", this.filterPanel.restriction);
        }
        var nodeRestriction = new Ab.view.Restriction();
        nodeRestriction.addClause('activity_log.bl_id', blId, '=');
        nodeRestriction.addClause('activity_log.project_id', projectId, '=');
        nodeRestriction.addClauses(this.filterPanel.getRecord().toRestriction());
        this.refreshReportPanel(nodeRestriction);
        this.showBuilding(new Ab.view.Restriction({'bl.bl_id': blId}));
    },
    /**
     * Show details for selected floor.
     * @param {Object} node - floor node.
     */
    showFloorDetails: function (node) {
        this.showPanels(false, true, true);
        var currentNode = this.projectTree_panel.lastNodeClicked;
        if (this.filterPanel.restriction) {
            this.reportPanel.addParameter("consoleRestriction", this.filterPanel.restriction);
        }
        var nodeRestriction = new Ab.view.Restriction();
        nodeRestriction.addClauses(node.restriction);
        this.refreshReportPanel(nodeRestriction);
        var blId = currentNode.data['rm.bl_id'],
            flId = currentNode.data['rm.fl_id'],
            dwgName = currentNode.data['rm.raw_dwgname'];
        this.displayDrawing(blId, flId, dwgName);
    }
});
/**
 * Show building details.
 * @param {Object} node - building node.
 */
function showBuilding(node) {
    var controller = View.controllers.get('caHighRmLS');
    controller.showBuildingDetails(node);
}
/**
 * Show floor details.
 * @param {Object} node - floor node.
 */
function showFloor(node) {
    var controller = View.controllers.get('caHighRmLS');
    controller.showFloorDetails(node);
}
