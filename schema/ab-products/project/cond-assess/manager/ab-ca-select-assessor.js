/**
 * controller definition
 */
var selectAssessorController = View.createController('selectAssessorCtrl', {
    parameters: null,
    //get view parameters after view loads
    afterViewLoad: function () {
        this.parameters = View.parameters.parameters;
        this.grid_AssessedByNew.addParameter('activityId', this.view.taskInfo.activityId);
        this.grid_AssessedByNew.addParameter('yes', getMessage('msg_yes'));
        this.grid_AssessedByNew.addParameter('no', getMessage('msg_no'));
        
        if (!valueExists(this.view.parameters.selectedIds)) {
            this.caSelectAssessor.getInstructionsEl().hide();
        	jQuery('#caSelectAssessor_assign_selector_labelCell').hide();
        	jQuery('#caSelectAssessor_assign_selector_fieldCell').hide();
        }
    },
    // on pressing Assign button
    caSelectAssessor_onAssign: function () {
        if (valueExists(this.view.parameters.selectedIds)) {
            var assignSelectorValue = this.caSelectAssessor.getFieldValue('assign_selector');
            if (!valueExistsNotEmpty(assignSelectorValue)) {
                View.showMessage(getMessage('noAssignSelected'));
                return;
            }
            var fieldName = "activity_log.assessed_by";
            var fieldValue = this.caSelectAssessor.getRecord().getValue(fieldName);
            var selectedIds = this.view.parameters.selectedIds;
            if ('all' === assignSelectorValue) {
                selectedIds = this.getAllActivityRecords(this.view.parameters.panel);
            }
            View.openProgressBar(getMessage('updateMessage'));
            try {
                Workflow.callMethod('AbCapitalPlanningCA-ConditionAssessmentService-assignItemsToAssessor',
                    selectedIds,
                    fieldValue);
                View.closeProgressBar();
                if (this.view.parameters.refresh && typeof(this.view.parameters.refresh) == "function") {
                    this.view.parameters.refresh.call();
                }
                View.closeThisDialog();
            }
            catch (e) {
                View.closeProgressBar();
                Workflow.handleError(e);
            }
        } else {
            this.parameters.panel.setFieldValue('activity_log.assessed_by', this.caSelectAssessor.getFieldValue('activity_log.assessed_by'));
            View.closeThisDialog();
        }
    },

    getAllActivityRecords: function (panel) {
        var activityLogIds = [];
        var parameters = panel.getParametersForRefresh();
        parameters.recordLimit = -1;
        parameters.indexField = "";
        var records = panel.getData(parameters).data.records;
        for ( var i = 0; i < records.length; i++ ) {
            var record = records[i];
            activityLogIds.push(record["activity_log.activity_log_id"]);
        }
        return activityLogIds;
    }
});
/**
 * set existing user
 * @param {Object} row
 */
function setExistingUser(row) {
    var userName = row['activity_log.assessed_by'];
    selectAssessorController.caSelectAssessor.setFieldValue('activity_log.assessed_by', userName);
    selectAssessorController.grid_AssessedByExisting.closeWindow();
}
/**
 * set new user
 * @param {Object} row
 */
function setNewUser(row) {
    var userName = row['afm_userprocs.user_name'];
    selectAssessorController.caSelectAssessor.setFieldValue('activity_log.assessed_by', userName);
    selectAssessorController.grid_AssessedByNew.closeWindow();
}