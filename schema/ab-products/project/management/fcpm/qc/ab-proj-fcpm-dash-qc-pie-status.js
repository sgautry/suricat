var projFcpmDashQcPieStatusController = View.createController('projFcpmDashQcPieStatus',{

/*	afterViewLoad: function() {
		this.projFcpmDashQcPieStatus_chart.addParameter('pending', getMessage('pendingSummary'));
		this.projFcpmDashQcPieStatus_chart.addParameter('active', getMessage('activeSummary'));
		this.projFcpmDashQcPieStatus_chart.addParameter('onHold', getMessage('onHoldSummary'));
		this.projFcpmDashQcPieStatus_chart.addParameter('done', getMessage('doneSummary'));
		this.projFcpmDashQcPieStatus_chart.addParameter('closed', getMessage('closedSummary'));
	},
*/
	afterInitialDataFetch: function(){
		var chartPanel = this.projFcpmDashQcPieStatus_chart;
		var chartConfig = this.getChartConfig();

		var chartData = this.getChartData();
		chartPanel.show(true, true);
		chartPanel.chartControl = new ChartControl(chartPanel.parentElementId, chartConfig);
		
		if (this.view.type == 'dashboard') {
			chartPanel.chartControl.chart.canvas.labelRadius = '0.5';
			chartPanel.chartControl.chart.canvas.radius= '25%';	
			chartPanel.chartControl.chart.canvas.fontSize='9';
			chartPanel.chartControl.chart.canvas.labelText = "[[value]]";
		} else{
			chartPanel.chartControl.chart.canvas.labelText = chartPanel.chartControl.chart.canvas.labelText + " ([[value]])";
		}
		
		chartPanel.chartControl.setData(chartData);	
		chartPanel.chartControl.addEventListener('clickSlice', selectPieStatus);
	},
	
	getChartConfig: function(){
		var config = new ChartConfig();
		config.chartType = 'pieChart';
		config.legendLocation = 'bottom';
		config.showExportButton = true;
		config.showOnLoad = true;
		config.showLegendOnLoad = false;
		config.showDataTips = true;
		config.showLabels = true;
		config.showUnitPrefixes = true;
		config.showUnitSuffixes = true;
		config.zoomable = false;
		
		config.addGroupingAxis('project.status_group', getMessage('groupingAxisTitle'));
		config.addDataAxis('pie', 'project.project_count', getMessage('dataAxisTitle'));
		config.addValueAxis(0, getMessage('groupingAxisTitle'));
		
		return config;
	},
	
	getChartData: function(){
		this.projFcpmDashQcPieStatus_ds0.addParameter('pending', getMessage('pendingSummary'));
		this.projFcpmDashQcPieStatus_ds0.addParameter('active', getMessage('activeSummary'));
		this.projFcpmDashQcPieStatus_ds0.addParameter('onHold', getMessage('onHoldSummary'));
		this.projFcpmDashQcPieStatus_ds0.addParameter('done', getMessage('doneSummary'));
		this.projFcpmDashQcPieStatus_ds0.addParameter('closed', getMessage('closedSummary'));
		
		var records = this.projFcpmDashQcPieStatus_ds0.getRecords();
		var data = new Array();
		for (var i=0; i < records.length; i++) {
			var rec = records[i];
			
			if (valueExistsNotEmpty(rec.getValue('project.project_count')) 
					&& valueExistsNotEmpty(rec.getValue('project.status_group'))) {
				
				var objData = {
					'project.project_count': rec.getValue('project.project_count'),
					'project.status_group':	rec.getValue('project.status_group')
				};
				data.push(objData);
			}
		}
		return data;
		
	}
});

function selectPieStatus(ctx) {
	var selectedChartData = ctx.dataItem;
	if (valueExists(selectedChartData)) {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('project.status_group', selectedChartData.title);
		View.openDialog('ab-proj-fcpm-dash-qc-pie-status-dtl.axvw', restriction);
	}
}



/**
 * Check if specified view is displayed in dashboard.
 * @param view view object
 * @returns {Boolean}
 */
function isInDashboard(view){
	var result = false;
	if (valueExists(view.parameters) && valueExists(view.parameters["isDialogWindow"]) && view.parameters["isDialogWindow"]) {
		result = false;
	} 
	else if(view.type === 'dashboard' || (typeof(view.getOpenerView) == 'function' && typeof(view.getOpenerView()) == 'object' && view.getOpenerView().type == 'dashboard')) {
		result = true;
	}
	
	return result;
}
