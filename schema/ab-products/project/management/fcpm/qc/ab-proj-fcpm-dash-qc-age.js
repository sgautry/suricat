var projFcpmDashQcAgeController = View.createController('projFcpmDashQcAge',{
	
	
	afterInitialDataFetch: function(){
		var chartPanel = this.projFcpmDashQcAge_chart;
		var chartConfig = this.getChartConfig();
		var chartData = this.getChartData();
		
		chartPanel.show(true, true);

		chartConfig.addGroupingAxis('project.id_name', getMessage('groupingAxisTitleProjectAge'));
		chartConfig.addDataAxis('bar', 'project.age', getMessage('dataAxisTitleProjectAge'));
		chartConfig.addValueAxis(0, getMessage('dataAxisTitleProjectAge'));

		// find a way to identify when is opened in dashboard
		//chartConfig.groupingAxis[0].showLabel = !isInDashboard(this.view);
		chartConfig.groupingAxis[0].showLabel = !(this.view.type == 'dashboard');
		
		chartPanel.chartControl = new ChartControl(chartPanel.parentElementId, chartConfig);
		chartPanel.chartControl.setData(chartData);	
		chartPanel.chartControl.addEventListener('clickGraphItem', selectItem);
	},
	
	getChartConfig: function(){
		var config = new ChartConfig();
		config.chartType = 'barChart';
		config.legendLocation = 'bottom';
		config.showExportButton = true;
		config.showOnLoad = true;
		config.showLegendOnLoad = false;
		config.showDataTips = true;
		config.showLabels = true;
		config.showUnitPrefixes = false;
		config.showUnitSuffixes = false;
		config.zoomable = false;
		return config;
	},
	
	getChartData: function(){
		var records = this.projFcpmDashQcAge_ds0.getRecords();
		var data = new Array();
		for (var i=0; i < records.length; i++) {
			var rec = records[i];
			if (valueExistsNotEmpty(rec.getValue('project.id_name')) 
					&& valueExistsNotEmpty(rec.getValue('project.age'))) {
				
				var objData = {
					'project.id_name': rec.getValue('project.id_name'),
					'project.age':	rec.getValue('project.age')
				};
				data.push(objData);
			}
		}
		return data;
	}
	
});

function selectItem(ctx) {
	var item = ctx.item;
	if (valueExists(item)) {
		var selectedChartData = item.dataContext;
		var projectTitle = selectedChartData['project.id_name'];
		var index = projectTitle.indexOf(']');
		var project_id = projectTitle.substring(1, index);
		var restriction = new Ab.view.Restriction();
		restriction.addClause('project.project_id', project_id);
		View.openDialog('ab-proj-fcpm-dash-qc-projs-dtl.axvw', restriction);
	}
}


/**
 * Check if specified view is displayed in dashboard.
 * @param view view object
 * @returns {Boolean}
 */
function isInDashboard(view){
	var result = false;
	if (valueExists(view.parameters) && valueExists(view.parameters["isDialogWindow"]) && view.parameters["isDialogWindow"]) {
		result = false;
	} 
	else if(view.type === 'dashboard' || (typeof(view.getOpenerView) == 'function' && typeof(view.getOpenerView()) == 'object' && view.getOpenerView().type == 'dashboard')) {
		result = true;
	}
	
	return result;
}