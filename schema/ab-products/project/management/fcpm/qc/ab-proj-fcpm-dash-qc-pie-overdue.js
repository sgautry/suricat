var projFcpmDashQcPieOverdueController = View.createController('projFcpmDashQcPieOverdue',{
	afterViewLoad: function() {
		this.projFcpmDashQcPieOverdue_chart.addParameter('pastDue', getMessage('pastDueSummary'));
		this.projFcpmDashQcPieOverdue_chart.addParameter('timely', getMessage('timelySummary'));
		this.projFcpmDashQcPieOverdue_chart.addParameter('unscheduled', getMessage('unscheduledSummary'));
		this.projFcpmDashQcPieOverdue_chart.addParameter('noValue', getMessage('noValueSummary'));
	},
	
	afterInitialDataFetch: function(){
		var chartPanel = this.projFcpmDashQcPieOverdue_chart;
		var chartConfig = this.getChartConfig();
		var chartData = this.getChartData();
		
		chartPanel.show(true, true);
		
		chartPanel.chartControl = new ChartControl(chartPanel.parentElementId, chartConfig);
		if (this.view.type == 'dashboard') {
			chartPanel.chartControl.chart.canvas.labelRadius = '0.5';
			chartPanel.chartControl.chart.canvas.radius= '25%';	
			chartPanel.chartControl.chart.canvas.fontSize='9';
			chartPanel.chartControl.chart.canvas.labelText = "[[value]]";
		} else{
			chartPanel.chartControl.chart.canvas.labelText = chartPanel.chartControl.chart.canvas.labelText + " ([[value]])";
		}
		
		chartPanel.chartControl.setData(chartData);	
		chartPanel.chartControl.addEventListener('clickSlice', selectPieOverdue);
	},
	
	getChartConfig: function(){
		var config = new ChartConfig();
		config.chartType = 'pieChart';
		config.legendLocation = 'bottom';
		config.showExportButton = true;
		config.showOnLoad = true;
		config.showLegendOnLoad = false;
		config.showDataTips = true;
		config.showLabels = true;
		config.showUnitPrefixes = true;
		config.showUnitSuffixes = true;
		config.zoomable = false;
		
		config.addGroupingAxis('project.status_group', getMessage('groupingAxisTitlePieOverdue'));
		config.addDataAxis('pie', 'project.project_count', getMessage('dataAxisTitlePieOverdue'));
		config.addValueAxis(0, getMessage('groupingAxisTitlePieOverdue'));
		
		return config;
	},
	
	getChartData: function(){
		this.projFcpmDashQcPieOverdue_ds0.addParameter('pastDue', getMessage('pastDueSummary'));
		this.projFcpmDashQcPieOverdue_ds0.addParameter('timely', getMessage('timelySummary'));
		this.projFcpmDashQcPieOverdue_ds0.addParameter('unscheduled', getMessage('unscheduledSummary'));
		this.projFcpmDashQcPieOverdue_ds0.addParameter('noValue', getMessage('noValueSummary'));
		
		var records = this.projFcpmDashQcPieOverdue_ds0.getRecords();
		var data = new Array();
		for (var i=0; i < records.length; i++) {
			var rec = records[i];
			
			if (valueExistsNotEmpty(rec.getValue('project.project_count')) 
					&& valueExistsNotEmpty(rec.getValue('project.status_group'))) {
				
				var objData = {
					'project.project_count': rec.getValue('project.project_count'),
					'project.status_group':	rec.getValue('project.status_group')
				};
				data.push(objData);
			}
		}
		return data;
		
	}
});

function selectPieOverdue(ctx) {
	var selectedChartData = ctx.dataItem;
	if (valueExists(selectedChartData)) {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('project.status_group', selectedChartData.title);
		View.openDialog('ab-proj-fcpm-dash-qc-pie-overdue-dtl.axvw', restriction);
	}
}

/**
 * Check if specified view is displayed in dashboard.
 * @param view view object
 * @returns {Boolean}
 */
function isInDashboard(view){
	var result = false;
	if (valueExists(view.parameters) && valueExists(view.parameters["isDialogWindow"]) && view.parameters["isDialogWindow"]) {
		result = false;
	} 
	else if(view.type === 'dashboard' || (typeof(view.getOpenerView) == 'function' && typeof(view.getOpenerView()) == 'object' && view.getOpenerView().type == 'dashboard')) {
		result = true;
	}
	
	return result;
}
