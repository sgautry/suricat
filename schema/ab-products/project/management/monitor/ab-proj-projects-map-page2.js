View.createController('projProjectsMapPage2', {
    projectId: null,
    blId: null,
    drawingController: null,
    afterViewLoad: function () {
        this.initDrawingControl();
        this.projProjectsMapPage2_floorsGrid.multipleSelectionEnabled = false;
        this.projProjectsMapPage2_floorsGrid.addEventListener('onMultipleSelectionChange', this.onFloorMultipleSelectionChange.createDelegate(this));
        this.projProjectsMapPage2_itemsDetails.addEventListener('onMultipleSelectionChange', this.onActionMultipleSelectionChange.createDelegate(this));
    },
    afterInitialDataFetch: function () {
        this.projectId = View.getOpenerView().controllers.get('projProjectsMapPage1').projectId;
        this.blId = View.getOpenerView().controllers.get('projProjectsMapPage1').blId;
        var restriction = new Ab.view.Restriction();
        if (this.projectId) {
            restriction.addClause('activity_log.project_id', this.projectId);
        }
        if (this.blId) {
            restriction.addClause('activity_log.bl_id', this.blId);
        }
        this.projProjectsMapPage2_floorsGrid.refresh(restriction);
        this.projProjectsMapPage2_itemsDetails.refresh(restriction);
    },
    /**
     * Init drawing control.
     */
    initDrawingControl: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['rm'],
            allowMultipleDrawings: true,
            orderByColumn: false,
            highlightParameters: [{
                'view_file': "ab-proj-projects-map-page2.axvw",
                'hs_ds': "projProjectsMapPage2_highlightDs",
                'label_ds': 'labelDetailsDs'
            }],
            topLayers: 'activity_log',
            layersPopup: {
                layers: "rm-assets;rm-labels;activity_log-assets;activity_log-labels;gros-labels;background",
                defaultLayers: "rm-assets;rm-labels;activity_log-assets;activity_log-labels;gros-assets;background"
            },
            assetTooltip: [
                {
                    assetType: 'rm',
                    datasource: 'labelDetailsDs',
                    fields: 'rm.rm_id;rm.rm_type;rm.area'
                },
                {
                    assetType: 'activity_log',
                    datasource: 'projProjectsMapPage2_detailsDS',
                    fields: 'activity_log.activity_log_id;activity_log.action_title;activity_log.activity_type;activity_log.site_id;project.project_id;project.project_name;activity_log.status;activity_log.date_scheduled;activity_log.date_scheduled_end'
                }
            ]
        });
    },
    /**
     * Load drawing.
     */
    loadDrawing: function (blId, flId, drawingName) {
        this.drawingController.showDrawing({
            pkeyValues: {blId: blId, flId: flId},
            drawingName: drawingName
        });
    },
    /**
     * Unload drawing.
     */
    unloadDrawing: function (blId, flId, drawingName) {
        this.drawingController.unloadDrawing(blId, flId, drawingName);
    },
    /**
     * On floor grid row selection load drawing on row select and unload drawing on row unselect.
     */
    onFloorMultipleSelectionChange: function (row) {
        var record = row.row.getRecord();
        // get selected data from tree
        var blId = record.getValue("activity_log.bl_id"),
            flId = record.getValue("activity_log.fl_id"),
            drawingName = record.getValue("rm.dwgname");
        if (row.row.isSelected()) {
            // load relevant svg
            this.loadDrawing(blId, flId, drawingName);
        } else {
            this.unloadDrawing(blId, flId, drawingName);
        }
        this.refreshActions();
    },
    /**
     * On action row selection, highlight action id on the drawing.
     */
    onActionMultipleSelectionChange: function (row) {
        if (_.isEmpty(this.drawingController.drawings)) {
            row.row.unselect();
            View.showMessage(getMessage('noDrawingSelected'));
            return;
        }
        var record = row.row.getRecord();
        var activityLogId = record.getValue("activity_log.activity_log_id"),
            drawingName = record.getValue("activity_log.rm_activity_dwgname");
        var svgId = this.drawingController.config.container + '-' + drawingName + '-svg';
        var highlightController = this.drawingController.svgControl.getController("HighlightController");
        if (activityLogId) {
            if (row.row.isSelected()) {
                var isAssetHighlighted = highlightController.highlightAsset(activityLogId, {
                    svgId: svgId,
                    color: 'red',
                    persistFill: false,
                    overwriteFill: true
                });
                if (!isAssetHighlighted) {
                    row.row.unselect();
                    View.showMessage(getMessage('noAssetFoundToHighlight'));
                }
            } else {
                highlightController.clearAsset(activityLogId, {
                    svgId: svgId
                });
            }
        }
    },
    /**
     * Refresh actions grid.
     */
    refreshActions: function () {
        var selectedRows = this.projProjectsMapPage2_floorsGrid.getSelectedGridRows(),
            drawingNames = [];
        for ( var i = 0; i < selectedRows.length; i++ ) {
            var drawingName = selectedRows[i].getRecord().getValue('rm.dwgname');
            drawingNames.push(drawingName);
        }
        var restriction = new Ab.view.Restriction();
        restriction.addClause('activity_log.bl_id', this.blId);
        if (drawingNames.length > 0) {
            restriction.addClause('activity_log.rm_activity_dwgname', drawingNames, "IN");
        }
        if (this.projectId) {
            restriction.addClause('activity_log.project_id', this.projectId);
        }
        this.projProjectsMapPage2_itemsDetails.refresh(restriction);
    },
});