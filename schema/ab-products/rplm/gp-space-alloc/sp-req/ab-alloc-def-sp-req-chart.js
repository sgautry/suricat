/**
 * Controller definition.
 */
var abAllocDefSpReqChartCtrl = View.createController('abAllocDefSpReqChartCtrl', {
	// sb_name is project id
	sbName: null,
	
	sbLevel: null,
	
	chartIds: {'bu_req' : 'abAllocSpReqBuChart', 'dv_req' : 'abAllocSpReqDvChart', 'dp_req' : 'abAllocSpReqDpChart', 'fg_req' : 'abAllocSpReqFgChart', 
		'bu_fore' : 'abAllocSpForeBuChart', 'dv_fore' : 'abAllocSpForeDvChart', 'dp_fore' : 'abAllocSpForeDpChart', 'fg_fore' : 'abAllocSpForeFgChart'}, 
	
	fieldsArraysForBu: new Array(
			['sb_items.bu_id']
	),

	fieldsArraysForDv: new Array(
			['sb_items.bu_id'], 
			['sb_items.dv_id']
	),

	fieldsArraysForDp: new Array(
			['sb_items.bu_id'], 
			['sb_items.dv_id'],
			['sb_items.dp_id']
	),

    /**
     * Set URL to YUI charts SWF file.
     */
    afterViewLoad: function() {
    	
    },

	afterInitialDataFetch: function(){
		this.refreshAndShow();
	},
		
	refreshAndShow: function(){
		this.initialLocalVariables();
		this.initialConsole();
		this.refreshChart();
	},

	initialLocalVariables: function(){
		var scnName = View.parentTab.parentPanel.scnName;
		var restriction = new Ab.view.Restriction();
		restriction.addClause('sb.sb_name', scnName, '=');
		var sbRecord = this.abAllocDefSpReqSbDS.getRecord(restriction);
		this.sbName = scnName;
		this.sbLevel = sbRecord.getValue("sb.sb_level");
	},
		
	initialConsole: function(){
		this.abAllocDefSpReqChartConsole.clear();
		
		$("periodSelectionDiv").style.display="";
	
		if ( 'fg'== this.sbLevel) {
			this.abAllocDefSpReqChartConsole.show(false);
		} else {
			this.abAllocDefSpReqChartConsole.show(true);
			if ( 'bu'== this.sbLevel) {
				this.abAllocDefSpReqChartConsole.showField('sb_items.dv_id', false);
				this.abAllocDefSpReqChartConsole.showField('sb_items.dp_id', false);
			} 
			else if ( 'dv'== this.sbLevel) {
				this.abAllocDefSpReqChartConsole.showField('sb_items.dv_id', true);
				this.abAllocDefSpReqChartConsole.showField('sb_items.dp_id', false);
			} 
			else if ( 'dp'== this.sbLevel) {
				this.abAllocDefSpReqChartConsole.showField('sb_items.dv_id', true);
				this.abAllocDefSpReqChartConsole.showField('sb_items.dp_id', true);
			} 
		}  
	},

	refreshChart: function(){
		View.panels.each(function(panel) {
			if ( panel.id != 'abAllocDefSpReqChartConsole' && panel.id!='customSpaceForecastLineChart' ) {
				panel.show(false);
			} else {
				panel.show(true);	
			}
		});
		
		this.refreshCustomChart();
	},
	
	getConsoleRestriction: function(){
		var restriction="1=1";
		if ( 'bu'== this.sbLevel ) {
			restriction = getRestrictionStrFromConsole( this.abAllocDefSpReqChartConsole, this.fieldsArraysForBu );  
		} 
		else if ( 'dv'== this.sbLevel ) {
			restriction = getRestrictionStrFromConsole( this.abAllocDefSpReqChartConsole, this.fieldsArraysForDv );  
		} 
		else if ( 'dp'== this.sbLevel ) {
			restriction = getRestrictionStrFromConsole( this.abAllocDefSpReqChartConsole, this.fieldsArraysForDp );  
		}
		return  restriction;
	},
	
	refreshCustomChart: function(){
		this.createCustomChart();
		this.customSpaceForecastLineChart.setTitle( getMessage('foreTitle')+" " + this.sbName);
	},

    /**
     * Create a custom line chart for Space Forecast, meanwhile hide all other chart panels.
     */
    createCustomChart: function() {
    	var chartPanel = this.customSpaceForecastLineChart;
    	var chartDataByPeriod = this.getChartPeriodData();
    	var chartData = [];
    	var chartConfig=new ChartConfig();
		
		var chartFieldNames = ["org"];
		var chartFieldDisplayNames = ["org"];
		for (var i=0; i<=12; i++){
			var periodCheckElement = document.getElementById("period"+i);
			if ( periodCheckElement.checked) {
				chartFieldNames.push("period"+i);
				chartFieldDisplayNames.push("period"+i); 
			}
		}
		
		for ( var i = 1; i < chartFieldNames.length; i++) {
			var chartRecord = {};
			chartRecord['period'] = getMessage(chartFieldDisplayNames[i]);
			var periodData = chartDataByPeriod[i - 1];
			for ( var r = 0; r < periodData.length; r++) {
				chartRecord['org' + r] = periodData[r]['periodValue'];
			}
			chartData.push(chartRecord);
		}
		
		chartConfig.addGroupingAxis('period','');
		
		if(chartDataByPeriod[0].length > 0){
			for ( var r = 0; r < chartDataByPeriod[0].length; r++) {
				var fieldName = 'org' + r;
				var fieldValue=chartDataByPeriod[0][r]['org'];
				chartConfig.addDataAxis("line",fieldName,fieldValue);
			}
		}else{
			chartConfig.addDataAxis("line",'period','');
		}
		
		chartConfig.addValueAxis(0,getMessage('dataAxisAreaTitle'));
		
		chartConfig.groupingAxis[0].showLabel=true;
		chartConfig.groupingAxis[0].labelRotation="45";
		
		for ( var r = 0; r < chartDataByPeriod[0].length; r++) {
			chartConfig.dataAxis[r].showLabel=true;
			chartConfig.dataAxis[r].labelPosition="none";
			chartConfig.dataAxis[r].labelRotation="0";
			chartConfig.dataAxis[r].autoCalculateTickSizeInterval="true";
			chartConfig.dataAxis[r].unitKey="M";
		}
		
		chartConfig.showLabels=true;
		
		chartPanel.chartControl = new ChartControl('customSpaceForecast_chart', chartConfig);
		chartPanel.chartControl.setData(chartData);	
    },
    
    getChartPeriodData: function() {
		var records = this.getDataRecordsBySbType();
		this.records = records; 
		var chartPeriodData = [];
		for (var i=0; i<=12; i++){
			var periodCheckElement = document.getElementById("period"+i);
			if ( periodCheckElement.checked ) {
				var chartDataByPeriod = [];
				for (var j = 0; j < records.length; j++) {
					var chartRecord = {};
					chartRecord['org'] = records[j].getValue('sb_items.org');
					chartRecord['periodValue'] = records[j].getValue('sb_items.p'+(i<10 ? '0':'')+i+'_value');
					chartDataByPeriod.push(chartRecord);
				}
				chartPeriodData.push(chartDataByPeriod);
			}
		}
		return chartPeriodData;        
	},

	getDataRecordsBySbType: function() {
		var dataSource = this.abAllocSpForeChart_ds; 
		var consoleRestriction = this.getConsoleRestriction();
		dataSource.addParameter('sbLevel', this.sbLevel);
		dataSource.addParameter('sbName', this.sbName);
		dataSource.addParameter('consoleRestriction', consoleRestriction);
		return dataSource.getRecords();
	}
});