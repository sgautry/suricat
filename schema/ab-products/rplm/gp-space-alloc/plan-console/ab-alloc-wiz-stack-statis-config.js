/**
 * Controller for stack diagram tab.
 * @author Qiang for 22.1; ZY for 23.1
 */
var abAllocWizStackStatisConfigController = View.createController('abAllocWizStackStatisConfigController', {
	
	/**The building's statistics fields of stack control . */
	stackControlBlStatisFields: [
		{'name': 'gp.avail.raw', 'format': true, 'selected': true},
		{'name': 'gp.util.raw', 'format': true, 'suffix': '%', 'selected': true},
		{'name': 'gp.availableSeats.raw', 'format': true, 'selected': false},
		{'name': 'gp.occupiedSeats.raw', 'format': true, 'selected': false},
		{'name': 'gp.occupiedPct.raw', 'format': true, 'suffix': '%', 'selected': false}
	],
	/**The building's statistics fields title array of stack control . */
	stackControlBlStatisFieldsTitle: [
	],
	stackControlBlStatisFieldsLimit: 2,

	/**The floor's statistics fields of stack control . */
	stackControlFlStatisFields: [
		{'name': 'gp.util.raw', 'format': true, 'suffix': '%', 'selected': true},
		{'name': 'gp.avail.raw', 'format': true, 'selected': true},
		{'name': 'gp.seats.raw', 'format': true, 'selected': true},	
		{'name': 'gp.availableSeats.raw', 'format': true, 'selected': false},
		{'name': 'gp.occupiedSeats.raw', 'format': true, 'selected': false},
		{'name': 'gp.occupiedPct.raw', 'format': true, 'suffix': '%', 'selected': false}
	],
	/**The building's statistics fields title array of stack control . */
	stackControlFlStatisFieldsTitle: [
	],
	stackControlFlStatisFieldsLimit: 3,
	    
	afterViewLoad: function(){
		if ( valueExists(View.parameters.stackControlBlStatisFields) ){
			this.stackControlBlStatisFields = View.parameters.stackControlBlStatisFields;			
		}
		if ( valueExists(View.parameters.stackControlFlStatisFields) ){
			this.stackControlFlStatisFields = View.parameters.stackControlFlStatisFields;			
		}

		if ( valueExists(View.parameters.callback) ){
			this.callback = View.parameters.callback;			
		}
	},

	/**
	 * Hide locate button if there are no lat and lon for buildings. 
	 */
	afterInitialDataFetch: function() {
		this.stackControl = View.getOpenerView().controllers.get('abAllocWizStackController').stackControl.stackControl;
		this.initialStackStatisticFieldsTitle();
		this.initialSelectionOfStackStatistics();
	},	
	/**
	 * Create the building stack chart.
	 */
	initialStackStatisticFieldsTitle: function() {
		//initial titles of statistics fields
		this.stackControlBlStatisFieldsTitle[0] = getMessage('blAreaAvailStatics');
		this.stackControlBlStatisFieldsTitle[1] = getMessage('blAreaUtilStatics');
		this.stackControlBlStatisFieldsTitle[2] = getMessage('blSeatAvailStatics');
		this.stackControlBlStatisFieldsTitle[3] = getMessage('blSeatOccupStatics');
		this.stackControlBlStatisFieldsTitle[4] = getMessage('blSeatUtilStatics');

		this.stackControlFlStatisFieldsTitle[0] = getMessage('flAreaUtilStatics');
		this.stackControlFlStatisFieldsTitle[1] = getMessage('flAreaAvailStatics');
		this.stackControlFlStatisFieldsTitle[2] = getMessage('flHeadAvailStatics');
		this.stackControlFlStatisFieldsTitle[3] = getMessage('flSeatAvailStatics');
		this.stackControlFlStatisFieldsTitle[4] = getMessage('flSeatOccupStatics');
		this.stackControlFlStatisFieldsTitle[5] = getMessage('flSeatUtilStatics');

		//reconfigure the building's statistics
		//this.configureStackChartBlProfileStatis();
		//this.configureStackChartFlStatis();
	},
		
	configureStackChartBlProfileStatis: function(){
    	var statsFields = this.stackControl.profileDataSourceConfig.statisticFields;
    	var statsTitles = this.stackControl.profileDataSourceConfig.statisticTitles;
		var count=0;
		for (var i=0; i<this.stackControlBlStatisFields.length; i++){
			if ( this.stackControlBlStatisFields[i]['selected']==true){
				statsFields[count++] = this.stackControlBlStatisFields[i];
				statsTitles[count-1] = this.stackControlBlStatisFieldsTitle[i];
			}
		}
    },

	configureStackChartFlStatis: function(){
    	var statsFields = this.stackControl.statisticsDataSourceConfig.statisticFields;
    	var statsTitles = this.stackControl.statisticsDataSourceConfig.statisticTitles;
		var count=0;
		for (var i=0; i<this.stackControlFlStatisFields.length; i++){
			if ( this.stackControlFlStatisFields[i]['selected']==true){
				statsFields[count++] = this.stackControlFlStatisFields[i];
				statsTitles[count-1] = this.stackControlFlStatisFieldsTitle[i];
			}
		}
    },
	
	initialSelectionOfStackStatistics: function(){
		for (var i=0; i<this.stackControlBlStatisFields.length; i++){
			if ( this.stackControlBlStatisFields[i]['selected']==true){
				$('blStats'+i).checked = true;
			} else {
				$('blStats'+i).checked = false;
			}
		}

		for (var i=0; i<this.stackControlFlStatisFields.length; i++){
			if ( this.stackControlFlStatisFields[i]['selected']==true){
				$('flStats'+i).checked = true;
			} else {
				$('flStats'+i).checked = false;
			}
		}
	},

	/**
	 * Configure the statistics of stack's building and floor.
	 */
	abAllocStackStatsConfigureForm_onSaveStats: function() {
		this.configureStackChartBlProfileStatis();
		this.configureStackChartFlStatis();

		if (this.callback)	{
			this.callback(this.stackControlBlStatisFields, this.stackControlFlStatisFields);
		}
	},
	
	onChangeBlStats: function(checkBox) {
    	var value = checkBox.value;
    	if (checkBox.checked) {
			
			var countSelected = 0;
			for (var i=0; i<this.stackControlBlStatisFields.length; i++){
				if ( $('blStats'+i).checked ){
					countSelected++;
				}
			}

			if (countSelected<=this.stackControlBlStatisFieldsLimit){
				this.stackControlBlStatisFields[parseInt(value)]['selected'] = true;    		
			} 
			else {
				View.alert( getMessage('outOfBlStatsLimit'));
				checkBox.checked = false;
			}

    	} else {
			this.stackControlBlStatisFields[parseInt(value)]['selected'] = false;    		
    	}    	
	},

	onChangeFlStats: function(checkBox) {
    	var value = checkBox.value;
    	if (checkBox.checked) {
			
			var countSelected = 0;
			for (var i=0; i<this.stackControlFlStatisFields.length; i++){
				if ( $('flStats'+i).checked ){
					countSelected++;
				}
			}

			if (countSelected<=this.stackControlFlStatisFieldsLimit){
				this.stackControlFlStatisFields[parseInt(value)]['selected'] = true;    		
			} 
			else {
				View.alert( getMessage('outOfFlStatsLimit'));
				checkBox.checked = false;
			}

    	} else {
			this.stackControlFlStatisFields[parseInt(value)]['selected'] = false;    		
    	}    	
	}
});