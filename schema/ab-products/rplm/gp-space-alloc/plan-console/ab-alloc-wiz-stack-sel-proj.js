
/**
 * Controller for stack diagram tab.
 * @author Qiang for 22.1; ZY for 23.1
 */
var abAllocWizStackSelectProjController = View.createController('abAllocWizStackSelectProjController', {
	
	StackView: null,	

    // ---------------------------------------------------------------------------------------------
    // BEGIN PPT generation Section 
    // ---------------------------------------------------------------------------------------------
	/**
     * On Generate PPT .
     */
    onGeneratePPT: function(stackController) {
		this.stackController = 	stackController;
		stackController.stackControl.getImageBytes(this.afterGetImageBytes.createDelegate(this));    	
    },

    /**
     * Callback.  After the image bytes are retrieved, feed them to the PPT generating workflow rule, and restore the image contents on screen.
     */
    afterGetImageBytes: function(imageBytes, nodesToRemove, nodesToRecover) {
		if (!imageBytes) {
			View.alert(getMessage('noStackShown'));
			return;
		}

    	var title = getMessage('pptStackTitle')+" "+this.stackController.asOfDate;

		this.slides=[];
		this.slides.push({'title': title,'images':[imageBytes]});
		
		// determine how many projects have the same name  as current scenario and do coresponding logics
		var records = this.selectProjectDS.getRecords("project_name='"+this.stackController.scn_name+"'");
		if (records.length==0){
			this.callWfrGeneratePPT(null);
		} 
		else if (records.length==1){
			var projectId = records[0].getValue("project.project_id");
			this.callWfrGeneratePPT(projectId);
		}	
		else {
			// if more than one projects has same scenario name then show a pop up list to let user choose
			var itemSelect = $("projectId");
			itemSelect.innerHTML = '';
			for (var i = 0; i < records.length; i++) {
				var item = records[i];
				var option = document.createElement('option');
				option.value = item.getValue("project.project_id");
				option.appendChild(document.createTextNode(item.getValue("project.project_id")));
				itemSelect.appendChild(option);
			}
			//set default value to dropdown list
			itemSelect.options[0].setAttribute('selected', true);
			this.abSpAllocProjectSelectDialog.showInWindow({
				x: 500, 
				y: 200,
				modal: true,
				width: 600,
				height: 100
			});
		}
    },   

	/**
	 * Save the user selected project id.
	 */
	abSpAllocProjectSelectDialog_onSave: function(){
		var projectId =  $("projectId").value;
    	this.abSpAllocProjectSelectDialog.closeWindow();
		this.callWfrGeneratePPT(projectId);
	},

	/**
	 * Call workflow rule to generate the PPT for given project id.
	 */
	callWfrGeneratePPT: function(projectId){
		try{
			var result = Workflow.callMethod('AbCommonResources-PortfolioForecastingService-generatePresentation', this.slides, {}, projectId);
			var me = this;
			result.data = eval('(' + result.jsonExpression + ')');
			var jobId = result.data.jobId;
			View.openJobProgressBar(getMessage("z_MESSAGE_WAIT")+"...", jobId, null, function(status) {
				var url  = status.jobFile.url;
				window.location = url;		
				// restore image content on screen
			}); 			
		}
		catch (e)	{
			Workflow.handleError(e);
			return false;
		}
	}
});