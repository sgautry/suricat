
/**
 * Controller for stack diagram tab.
 * @author Qiang for 22.1; ZY for 23.1
 */
var abAllocWizStackEventTreeController = View.createController('abAllocWizStackEventTreeController', {
	
	/** The last store event name for allocating space. */
	lastStoredEventNameForAllocating: '',
	
	buildingDisplayTitlesMap : {},

    // ---------------------------------------------------------------------------------------------
    // BEGIN view initialize Section 
    // ---------------------------------------------------------------------------------------------
	/**
	 * Create restrictions for building-floor two level tree.
	 */
	afterViewLoad: function() {
		// customize the tree nodes 
		this.eventsTreePanel.createRestrictionForLevel = this.createRestrictionForEventsTreeLevel;
		this.eventsTreePopPanel.createRestrictionForLevel = this.createRestrictionForEventsTreeLevel;		
	},
			
	/**
	 * Hide locate button if there are no lat and lon for buildings. 
	 */
	afterInitialDataFetch: function() {
		this.stackController = View.controllers.get('abAllocWizStackController');
	},
	

	popUpEventsPanel: function() {
		var layoutManager = View.getLayoutManager('mainLayout');
		if (layoutManager.isRegionCollapsed('north')) {
			this.eventsTreePopPanel.showInWindow({
				x: 100, 
				y: 100,
				width: 300,
				height: 300,
				closeButton: true,
				closable: false
			});
		}
		else {
			View.alert(getMessage("eventPopUp"));
		}
	},

    // ---------------------------------------------------------------------------------------------
    // BEGIN 'Events' tree Section 
    // ---------------------------------------------------------------------------------------------
	/**
	 * Create restriction for the events tree.
	 */
	createRestrictionForEventsTreeLevel: function(parentNode, level) {
		if (parentNode.data) {
			var restriction = null;
			if (level == 1) {
				var dvId = parentNode.data['gp.dv_id'];
				restriction = new Ab.view.Restriction();
				restriction.addClause('gp.dv_id', dvId, '=');
			} else if (level == 2) {
				var eventName = parentNode.data['gp.event_name'];
				restriction = new Ab.view.Restriction();
				restriction.addClause('gp.event_name', eventName, '=');
			} else if (level == 3) {
				var gpName = parentNode.data['gp.name'];
				var eventName = parentNode.data['gp.event_name'];
				restriction = new Ab.view.Restriction();
				restriction.addClause('gp.name', gpName, '=');
				restriction.addClause('gp.event_name', eventName, '=', 'AND', true);
			}
			return restriction;
		}
	},
	
	/**
	 * update as of date of stack control and map control.
	 */
	updateAsOfDateFromEventName: function(panel) {
		var eventNode = panel.lastNodeClicked;
		var eventName = eventNode.data['gp.event_name'];
		var date = eventName.substring(0,10);
		this.asOfDate = date;
		//this.buildScenarioThematic();
		this.trigger('app:space:planing:console:buildScenarioThematic');
		//this.setInitialValueForDateInput();
    	this.trigger('app:space:planing:console:updateAsOfDate',this.asOfDate, true);
		//this.refreshStackChartWithNewDate();
		if ( this.toggleEventsHighlight){
			this.highlightGpsOnStackByEventName(eventName);
		}
	},

	/**
	 * update as of date of stack control and map control.
	 */
	updateAsOfDateFromGroupName: function(panel) {
		var currentNode = panel.lastNodeClicked;
		var parentNode = currentNode.parent;
		var eventName = parentNode.data['gp.event_name'];
		var gpName = currentNode.data['gp.name'];
		var date = eventName.substring(0,10);
		this.asOfDate = date;
		//this.buildScenarioThematic();
		this.trigger('app:space:planing:console:buildScenarioThematic');
		//this.setInitialValueForDateInput();
    	this.trigger('app:space:planing:console:updateAsOfDate',this.asOfDate, true);
		//this.refreshStackChartWithNewDate();
		if ( this.toggleEventsHighlight){
			this.highlightGpsOnStackByGpName(eventName, gpName);
		}
	},

	/**
	 * update as of date of stack control and map control.
	 */
	updateAsOfDateFromDescription: function(panel) {
		var currentNode = panel.lastNodeClicked;
		var gpName = currentNode.parent.data['gp.name'];
		var eventNode = currentNode.parent.parent;
		var eventName = eventNode.data['gp.event_name'];
		var date = eventName.substring(0,10);
		this.asOfDate = date;
		var description = currentNode.data['gp.description'];
		var blName = currentNode.data['gp.blName'];
		var blId = null;
		if(description) {
			var descArray = description.split(" ");
			blId = descArray[0];
		}
		//this.buildScenarioThematic();
		this.trigger('app:space:planing:console:buildScenarioThematic');
		
		var originalBuildings = this.stackController.stackControl.config.buildings;
		var needPush = true;
		for (var i = 0; i < originalBuildings.length; i++) {
			if (originalBuildings[i] == blId) {
				needPush = false;
			}
		}
		if (needPush && blId) {
			originalBuildings.push(blId);
			this.buildingDisplayTitlesMap[blId+" "+blName] = getMessage('hideScenarioOnStack'); 
			this.stackBuildingTreePanel_afterRefresh();
		}
		//this.setInitialValueForDateInput();
    	this.trigger('app:space:planing:console:updateAsOfDate',this.asOfDate, true);
		//this.refreshStackChartWithNewDate();
		if ( this.toggleEventsHighlight){
			this.highlightGpsOnStackByGpName(eventName, gpName);
		}
	},
	/**
	 * after Refresh when click tree node.
	 */
	stackBuildingTreePanel_afterRefresh: function (){
    	var rows = Ext.query('.treeTable .ygtvrow', $('stackBuildingTreePanel'));
		for (var i=0; i<rows.length; i++ ){
			var blName = rows[i].children[0].innerText.replace(/\r\n/g,""); 
			if (	 this.buildingDisplayTitlesMap[blName] == getMessage('hideScenarioOnStack') ){
				rows[i].children[2].children[0].children[0].value = getMessage('hideScenarioOnStack'); 
			}
		}
	},

	highlightGpsOnStackByEventName: function(eventName) {
		var highlightRestriction = this.getHighlightRestrictionFromEventName(eventName);
		this.eventsHighlightGpDataSource.addParameter('highlightRestriction', highlightRestriction);
		
		this.addOtherRestrictionToEventHighlightDatasource();
		this.highlightGpsOnStack();
	},

	
	highlightGpsOnStackByGpName: function(eventName, gpName) {
		var highlightRestriction = this.getHighlightRestrictionFromEventName(eventName);
		highlightRestriction = highlightRestriction + " AND gp.name='"+ gpName +"'";
		this.eventsHighlightGpDataSource.addParameter('highlightRestriction', highlightRestriction);
		
		this.addOtherRestrictionToEventHighlightDatasource();
		this.highlightGpsOnStack();
	},

	highlightGpsOnStack: function(){
		var records = this.eventsHighlightGpDataSource.getRecords();
		if ( records && records.length>0) {
			var gpIds = [];
			for (var j=0; j<records.length; j++)	{
				var gpId = records[j].getValue('gp.gp_id');
				this.stackController.stackControl.highlightStackElement(gpId);
			}
		}
	},

	getHighlightRestrictionFromEventName: function( eventName){
		var endIndex = eventName.indexOf("-End");
		var actualEventName = ( endIndex==-1 ?  eventName.substring(11) : eventName.substring(11, endIndex) );
		// add restriction of gp's event name
		var highlightRestriction = ( actualEventName ?  " gp.event_name='"+ actualEventName +"'" : " ( gp.event_name is null or gp.event_name='')  " );

		// add restriction of gp's date_start or date_end
		if(this.stackController.isOracle == 1) {
			startDateAsOfDate = " (gp.date_start <= to_date('" + this.asOfDate + "', 'YYYY-MM-DD'))";
			endDateAsOfDate = " (gp.date_end >= to_date('" + this.asOfDate + "', 'YYYY-MM-DD') OR gp.date_end IS NULL)";
			highlightRestriction = highlightRestriction +  ( endIndex==-1 ?   " AND gp.date_start =  to_date('" + this.asOfDate + "', 'YYYY-MM-DD')"  :  " AND gp.date_end =  to_date('" + this.asOfDate + "', 'YYYY-MM-DD')"  );
		} 
		else {
			highlightRestriction = highlightRestriction +  ( endIndex==-1 ?   " AND gp.date_start = '" + this.asOfDate + "'"  : " AND gp.date_end = '" + this.asOfDate + "'" );
		}

		return highlightRestriction;
	},

	addOtherRestrictionToEventHighlightDatasource: function( ){
		var scnIdRestriction = getScenarioIdRestriction(this.stackController.scnId);
 		this.eventsHighlightGpDataSource.addParameter('scenarioIdRestriction', scnIdRestriction);

		addLocationRestriction([this.eventsHighlightGpDataSource], this.stackController.filter);
		//kb#3050891: Add the ability to filter by organization	
		addOrganizationRestriction([this.eventsHighlightGpDataSource], this.stackController.filter);
	},	
    // ---------------------------------------------------------------------------------------------
    // END 'Events' tree Section 
    // ---------------------------------------------------------------------------------------------
    	
    // ---------------------------------------------------------------------------------------------
    // BEGIN toggle highlights on events function Section 
    // ---------------------------------------------------------------------------------------------
	toggleEventsHighlight: false,
	onToggleHighlightsEvent:function() {
		var toggleAction = this.eventsTreePanel.actions.get('toggleHighlight');
		var popToggleAction = this.eventsTreePopPanel.actions.get('toggleHighlight');

		if ( !this.toggleEventsHighlight ){
			toggleAction.setTitle( getMessage('highlightsOff') );	
			popToggleAction.setTitle( getMessage('highlightsOff') );	
			this.toggleEventsHighlight = true;
		} 
		else {
			toggleAction.setTitle( getMessage('highlightsOn') );	
			popToggleAction.setTitle( getMessage('highlightsOn') );	
			this.toggleEventsHighlight = false;
		}
	}
    // ---------------------------------------------------------------------------------------------
    // END toggle highlights on events function Section 
    // ---------------------------------------------------------------------------------------------
});