/**
 * Controller for stack diagram tab.
 * @author Qiang for 22.1; ZY for 23.1
 */
var abAllocWizStackMapController = View.createController('abAllocWizStackMapController', {
	
	/** Location map control. */
	mapControl: null,	

	/** The legend color of the map.*/
	thematicLegendColor:["#d73027","#fee08b","#1a9850","#ff9900","#990033"],
	
    afterCreate: function() {
        this.on('app:space:planing:console:buildScenarioThematic', this.buildScenarioThematic);
        this.on('app:space:planing:console:locateBuildingInMap', this.locateBuildingInMap);
		this.on('app:space:planing:console:refreshMapChart', this.decideShowMapOrChartPanel);
    },

	/**
	 * Create restrictions for building-floor two level tree.
	 */
	afterViewLoad: function() {		
		//initialize the scenario map control.
		this.initializeScenarioMapControl();		
	},
	
	/**
	 * Initialize the as of date map control.
	 */
	initializeScenarioMapControl: function() {
		var configObject = new Ab.view.ConfigObject();
		configObject.mapImplementation = 'Esri';
		configObject.basemap = View.getLocalizedString('World Light Gray Canvas');
		this.mapControl = new Ab.leaflet.Map('locationsInScenarioPanel', 'scenarioMapDiv', configObject);
		//configure base map layer
		this.configureMapUI();
	},
	
	/**
	 * We configure the map control UI to add more layer options for the user.
	 */
	configureMapUI: function() {
		var basemapLayerMenu = this.locationsInScenarioPanel.actions.get('basemapLayerMenu');
		basemapLayerMenu.clear();
		var basemapLayers = this.mapControl.getBasemapLayerList();
		for (var i=0; i < basemapLayers.length; i++){
			basemapLayerMenu.addAction(i, basemapLayers[i], this.switchBasemapLayer);
		}
	},
	
	/**
	 * When the user selects a different layer, we switch it.
	 */
	switchBasemapLayer: function(item) {
		abAllocWizStackController.mapControl.switchBasemapLayer(item.text);
    },  
    
	/**
	 * Hide locate button if there are no lat and lon for buildings. 
	 */
	afterInitialDataFetch: function() {
		
		//set the style of map control
		var mapControlPanel = document.getElementById("locationsInScenarioPanel");            
		mapControlPanel.className = 'claro';
		
		//decide which panel to show. Map or chart.
		this.decideShowMapOrChartPanel();
	},
	    
    // ---------------------------------------------------------------------------------------------
    // BEGIN Map/Chart showing Section 
    // ---------------------------------------------------------------------------------------------
	/**
	 * According to the building geocode info and Internet status to decide which tab will be display.  
	 */
	decideShowMapOrChartPanel: function() {
		this.addParametersForBuildingTreeDataSource();
		var geocodedBuildings = this.buildingTreeDataSource.getRecords();
		var showGisMap = false;
		for (var i = 0; i < geocodedBuildings.length; i++) {
			var building = geocodedBuildings[i];
			if (building.getValue('gp.lat') && building.getValue('gp.lon')) {
				showGisMap = true;
				break;
			}
		}

		if (!showGisMap) {
			this.locationsInScenarioPanel.show(false);
			this.showChartTabControl(true);
		} else {
			this.showChartTabControl(false);
			this.locationsInScenarioPanel.show(true);
			try{
				this.buildScenarioThematic();
			}catch(e){
				
			}
		}
	},
	
	/**
	 * Initialize the chart tab to display the utilization.
	 */
	showChartTabControl: function(show) {
		var allocWizCtrl=View.getOpenerView().controllers.get('allocWiz');
		var allocStackWizCtrl=View.controllers.get('abAllocWizStackController');
		if(show) {
			this.addParametersForBuildingTreeDataSource();
	    	var buildings = this.buildingTreeDataSource.getRecords();
			var buildingsArray = [];
			for (var i = 0; i < buildings.length; i++) {
				var buildingId = buildings[i].getValue('gp.bl_id');
	    		buildingsArray.push(buildingId);
			}
			var restriction = new Ab.view.Restriction();
			if(buildingsArray.length == 0) {
				buildingsArray.push("NOT EXISTING BUILDING")
			}
			restriction.addClause('gp.bl_id', buildingsArray, 'IN');
			restriction.addClause('gp.portfolio_scenario_id', allocWizCtrl.scnId, '=');
			
			if (allocStackWizCtrl.asOfDate != null) {
				restriction.addClause('gp.date_start', allocStackWizCtrl.asOfDate, '&lt;=');
				restriction.addClause('gp.date_end', allocStackWizCtrl.asOfDate, '&gt;=', ') AND (');
				restriction.addClause('gp.date_end', allocStackWizCtrl.asOfDate, 'IS NULL', 'OR');
	    		this.chartTabBuildingUtilization_chart.setTitle(getMessage('locationsInScenario') + ' ' + allocStackWizCtrl.asOfDate);
	    	}
			this.addParametersForChartTabPanel();
			
			this.chartTabBuildingUtilization_chart.show(true);
			this.chartTabBuildingUtilization_chart.refresh(restriction);
		} else {
			this.chartTabBuildingUtilization_chart.show(false);
		}
	},

    /**
     * Show all buildings in Scenario.
     */
    locationsInScenarioPanel_onShowAllInScenario: function() {
    	this.buildScenarioThematic();
    },
    
	/**
	 * Build the scenario thematic and marker all the buildings that have lat and lon values.
	 */
	buildScenarioThematic: function() {
		var title = getMessage('locationsInScenario') + ' ' + abAllocWizStackController.asOfDate;
		this.locationsInScenarioPanel.setTitle(title);
		
		//create map markers properties
		this.buildThematicLegend();
		
		this.addParametersForBuildingTreeDataSource();
    	var buildings = this.buildingTreeDataSource.getRecords();
		var buildingsArray = [];
		for (var i = 0; i < buildings.length; i++) {
			var buildingId = buildings[i].getValue('gp.bl_id');
			var lon = buildings[i].getValue('gp.lon');
			var lat = buildings[i].getValue('gp.lat');
			if (lon && lat) {
				buildingsArray.push(buildingId);
			}
		}
		var restriction = new Ab.view.Restriction();
		if (buildingsArray.length == 0) {
			buildingsArray.push("NOT EXISTED BUILDING");
			//this.showDefaultMap();
		} 
		else {
			var allocWizStackCtrl=View.controllers.get('abAllocWizStackController');
			restriction.addClause('gp.bl_id', buildingsArray, 'IN');
			restriction.addClause('gp.portfolio_scenario_id', allocWizStackCtrl.scnId, '=');
			if (allocWizStackCtrl.asOfDate) {
				restriction.addClause('gp.date_start', allocWizStackCtrl.asOfDate, '&lt;=');
				restriction.addClause('gp.date_end', allocWizStackCtrl.asOfDate, '&gt;=', ') AND (');
				restriction.addClause('gp.date_end', allocWizStackCtrl.asOfDate, 'IS NULL', 'OR');
			}
			//add parameters to secenarioMarkerDs datasource.
			this.addParametersForScenarioMarkerDs();
			//show map marker
			this.mapControl.showMarkers('scenarioMarkerPropertyDataSource',restriction);
		}
	},

	/**
	 * Build the scenario thematic and marker all the buildings that have lat and lon values.
	 */
	buildThematicLegend: function() {
		var mapMarkerDataSource = 'scenarioMarkerPropertyDataSource';
        var mapMarkerKeyFields = ['gp.bl_id'];
        var mapMarkerGeometryFields = ['bl.lon', 'bl.lat'];
        var mapMarkerTitleField = 'gp.bl_id';
        var mapMarkerContentFields = ['bl.name', 'gp.utilization'];
        var thematicBuckets = [70, 80, 90, 95];	
        
        //build customized color for colorbrewer.
		colorbrewer['WIZ-STATCK-COLOR']={5:this.thematicLegendColor};
		
        var mapMarkerProperties={
            radius: 10,
            //stroke properties
        	stroke: true,
        	strokeColor: '#fff',
        	strokeWeight: 1.0,
        	// required for thematic markers
        	renderer: 'thematic-class-breaks',
        	thematicField: 'gp.utilization',
        	thematicClassBreaks: thematicBuckets,
        	colorBrewerClass: 'WIZ-STATCK-COLOR'
        };
        
		this.mapControl.createMarkers(
            mapMarkerDataSource,
            mapMarkerKeyFields,
            mapMarkerGeometryFields,
            mapMarkerTitleField,
            mapMarkerContentFields,
            mapMarkerProperties
        ); 
		
		// legend menu
    	var legendObj = Ext.get('showMapLegend'); 
	    legendObj.on('click', this.showLegend, this, null);
        
	},	
	
	/**
 	 * Add parameters for the marker data source.
 	 */
 	addParametersForChartTabPanel: function() {
 		var allocWizStackCtrl=View.controllers.get('abAllocWizStackController');
    	if (allocWizStackCtrl.asOfDate != null) {
    		var isoDate = this.asOfDate;
    		var innerDateStartAsOfDate = "";
    		var innerDateEndAsOfDate = "";
    		if (allocWizStackCtrl.isOracle == 1) {
    			innerDateStartAsOfDate = " (gp_inner.date_start <= to_date('" + allocWizStackCtrl.asOfDate + "', 'YYYY-MM-DD'))";
    			innerDateEndAsOfDate = " (gp_inner.date_end >= to_date('" + allocWizStackCtrl.asOfDate + "', 'YYYY-MM-DD') OR gp_inner.date_end IS NULL)";
    		} else {
    			innerDateStartAsOfDate = " (gp_inner.date_start <= '" + allocWizStackCtrl.asOfDate + "')";
    			innerDateEndAsOfDate = " (gp_inner.date_end >= '" + allocWizStackCtrl.asOfDate + "' OR gp_inner.date_end IS NULL)";
    		}
    		this.chartTabBuildingUtilization_chart.addParameter('innerDateStartAsOfDate', innerDateStartAsOfDate);
    		this.chartTabBuildingUtilization_chart.addParameter('innerDateEndAsOfDate', innerDateEndAsOfDate);
    	}
 	},

 	/**
 	 * Add parameters for the marker data source.
 	 */
 	addParametersForScenarioMarkerDs: function(buildingMarkerProperty) {
 		var allocWizStackCtrl=View.controllers.get('abAllocWizStackController');
    	if (allocWizStackCtrl.asOfDate != null) {
    		var isoDate = allocWizStackCtrl.asOfDate;
    		var innerDateStartAsOfDate = "";
    		var innerDateEndAsOfDate = "";
    		
    		
    		if (allocWizStackCtrl.isOracle == 1) {
    			innerDateStartAsOfDate = " (gp_inner.date_start <= to_date('" + allocWizStackCtrl.asOfDate + "', 'YYYY-MM-DD'))";
    			innerDateEndAsOfDate = " (gp_inner.date_end >= to_date('" + allocWizStackCtrl.asOfDate + "', 'YYYY-MM-DD') OR gp_inner.date_end IS NULL)";
    		} else {
    			innerDateStartAsOfDate = " (gp_inner.date_start <= '" + allocWizStackCtrl.asOfDate + "')";
    			innerDateEndAsOfDate = " (gp_inner.date_end >= '" + allocWizStackCtrl.asOfDate + "' OR gp_inner.date_end IS NULL)";
    		}
    		this.scenarioMarkerPropertyDataSource.addParameter('innerDateStartAsOfDate', innerDateStartAsOfDate);
    		this.scenarioMarkerPropertyDataSource.addParameter('innerDateEndAsOfDate', innerDateEndAsOfDate);
    	}
 	},
 	
 	/**
 	 * Add parameters for the building tree datasource.
 	 */
 	addParametersForBuildingTreeDataSource: function() {
 		var allocWizCtrl=View.getOpenerView().controllers.get('allocWiz');
 		var allocWizStackCtrl=View.controllers.get('abAllocWizStackController');
		var scnIdRestriction = getScenarioIdRestriction(allocWizCtrl.scnId);
 		this.buildingTreeDataSource.addParameter('scenarioIdRestriction', scnIdRestriction);
 		
 		var fromDate = allocWizCtrl.filter['from_date'];
    	var endDate = allocWizCtrl.filter['end_date'];
    	var dateRestriction = "1=1";
    	if (fromDate != null && fromDate != "") {
    		if (allocWizStackCtrl.isOracle == 1) {
    			dataRestriction = "gp.date_start >= to_date('" + fromDate + "', 'YYYY-MM-DD')";
    		} else {
    			dateRestriction = "gp.date_start >= '" + fromDate + "'";
    		}
    	}
    	if(endDate != null &&  endDate != "") {
    		if (allocWizStackCtrl.isOracle == 1) {
    			dateRestriction = dateRestriction + " AND gp.date_start <= to_date('" + endDate + "', 'YYYY-MM-DD')";
    		} else {
    			dateRestriction = dateRestriction + " AND gp.date_start <= '" + endDate + "'";
    		}
    	}
    	this.buildingTreeDataSource.addParameter('dateRestriction', dateRestriction);
    	
		addLocationRestriction([this.buildingTreeDataSource], allocWizCtrl.filter);
		addOrganizationRestriction([this.buildingTreeDataSource], allocWizCtrl.filter);
 	},
	
	/**
	 * Display the stack chart title after the map and stack chart.
	 */
	displayStackChartTitlebar: function() {
		jQuery("#stackChartTitlebar").show();
	},
		
   	locateBuildingInMap: function(lon, lat, restriction) {
		if (!!lon && !!lat) {
		   this.mapControl.startLocateAsset(lat,lon);
		}
		
		this.mapControl.finishLocateAsset();
	},
	
	showLegend: function(){
		this.mapControl.showMarkerLegend();
	}
});