/**
 * Controller for group confirm dialog.
 * @author ZY for 23.1
 */
var abAllocWizStackConfirmController = View.createController('abAllocWizStackConfirmController', {
	
	afterInitialDataFetch: function() {
		var form = this.groupMoveConfirmationForm;

		form.setFieldValue('gp.name', this.recordToUpdate['gp.name']);
		form.setFieldValue('gp.date_start', this.stackController.asOfDate);

		form.setFieldValue('from_bl_fl', this.fromBlId + "-" + this.fromFlId);
		form.setFieldValue('from_bl', this.fromBlId );
		form.setFieldValue('from_fl', this.fromFlId );
		form.setFieldValue('to_bl_fl', this.toBlId + "-" + this.toFlId);
		form.setFieldValue('to_bl', this.toBlId);
		form.setFieldValue('to_fl', this.toFlId);
			
		var description = getMessage('moveFrom') + " " + this.fromBlId + " " + this.fromFlId + " " + getMessage('moveTo') + " " + this.toBlId + " " + this.toFlId;
		form.setFieldValue('gp.description', description);

		//kb#3049288: When the FROM building is UNALLOC, copy the gp.event_name value of that group into the "Confirm Allocation Change" form.
		if ("UNALLOC"==this.fromBlId){
			form.setFieldValue('gp.event_name', this.recordToUpdate['gp.event_name']);
		} else {
			//var recentEventName = form.getSidecar().get('recentSelectedEventName');
			if (this.stackController.recentSelectedEventName) {
				form.setFieldValue('gp.event_name', this.stackController.recentSelectedEventName);
			}
		}

		form.setFieldValue('gp.portfolio_scenario_id', this.stackController.scnId);
		form.setFieldValue('gp.gp_id', this.recordToUpdate['gp.gp_id']);
		form.setFieldValue('gp.ls_id', this.lsId);
		form.setFieldValue('gp.allocation_type', this.recordToUpdate['gp.allocation_type']);
		form.setFieldValue('gp.bl_id', this.recordToUpdate['gp.bl_id']);
		form.setFieldValue('gp.hpattern_acad', this.recordToUpdate['gp.hpattern_acad']);

		var restriction = new Ab.view.Restriction();
		restriction.addClause('gp.gp_id', this.recordToUpdate['gp.gp_id'], '=');
		var currentAllocation = this.stackController.groupRecordsDataSource.getRecords(restriction)[0];
		if (currentAllocation.getValue('gp.parent_group_id')) {
			form.enableField('gp.date_start', false);
		}			
	},
    
    /**
     * Confirm the group move, we will update or create new gp record.
     */
    groupMoveConfirmationForm_onConfirmGpAllocationChange: function() {
		var form = this.groupMoveConfirmationForm;
    	//Check if the form can be saved.
    	if(form.canSave()) {
    		var currentDateStart = form.getFieldValue('gp.date_start');
    		var gpName = form.getFieldValue('gp.name');
    		var eventName = form.getFieldValue('gp.event_name');
    		var sourceGpId = form.getFieldValue('gp.gp_id');
    		var description = form.getFieldValue('gp.description');
    		var gpHpattern = form.getFieldValue('gp.hpattern_acad');
    		
			var toBlId = form.getFieldValue('to_bl');
			var toFlId = form.getFieldValue('to_fl');
			
			//get the start date of the target record and compare it to the asOfDate.
			var restriction = new Ab.view.Restriction();
			restriction.addClause('gp.gp_id', sourceGpId, '=');
			var targetGroup = this.stackController.groupRecordsDataSource.getRecords(restriction)[0];
			var targetStartDate = getIsoFormatDate(targetGroup.getValue('gp.date_start'));
			
    		if (currentDateStart == targetStartDate) {
    			//only update the location to the target location;
    			var newSortOrder = this.stackController.calculateNewSortOrder(sourceGpId);
    			targetGroup.setValue('gp.sort_order', newSortOrder);
    			targetGroup.setValue('gp.bl_id', toBlId);
    			targetGroup.setValue('gp.fl_id', toFlId);
    			targetGroup.setValue('gp.event_name', eventName);
    			targetGroup.setValue('gp.description', description);
    			targetGroup.isNew = false;
    			this.stackController.groupRecordsDataSource.saveRecord(targetGroup);
    		} 
			else if (currentDateStart > targetStartDate) {
    			//create a new record
    			var restriction = new Ab.view.Restriction();
    			restriction.addClause('gp.gp_id', sourceGpId, '=');
    			var record = this.stackController.groupRecordsDataSource.getRecords(restriction)[0];
				record.isNew = true;
				record.removeValue('gp.gp_id');
				record.setValue('gp.date_start', currentDateStart);
				record.setValue('gp.name', gpName);
				record.setValue('gp.description',  description);
				record.setValue('gp.portfolio_scenario_id', this.stackController.scnId);
				record.setValue('gp.allocation_type', 'Allocated Area');
				record.setValue('gp.parent_group_id', sourceGpId);
				record.setValue('gp.sort_order', this.stackController.calculateNewSortOrder(sourceGpId));
				record.setValue('gp.bl_id', toBlId);
				record.setValue('gp.fl_id', toFlId);
				record.setValue('gp.event_name', eventName);
				record.setValue('gp.hpattern_acad', encodePattern(gpHpattern));
				
				//add end date if it is leased 
				var originalAllocationType = form.getFieldValue('gp.allocation_type');
				if(originalAllocationType == 'Usable Area - Leased') {
					var lsId = form.getFieldValue('gp.ls_id');
					var restriction = new Ab.view.Restriction();
					restriction.addClause('ls.ls_id', lsId, '=');
					var lsRecords = this.stackController.leaseDataSource.getRecords(restriction);
					if (lsRecords.length > 0) {
						var endDate = lsRecords[0].getValue('ls.date_end');
						record.setValue('gp.date_end', endDate);
					}
				}
				this.stackController.groupRecordsDataSource.saveRecord(record);
				
				//Update the original group's date end.
				var dateEndForOriginal = addDay(currentDateStart, -1);
				targetGroup.setValue('gp.date_end', dateEndForOriginal);
				//	kb#3049630: comment below code since original event_name of ended group should keep.
				// targetGroup.setValue('gp.event_name', eventName);
				targetGroup.isNew = false;
				this.stackController.groupRecordsDataSource.saveRecord(targetGroup);
    		} else {
    			alert("Date of Move is earlier than the group allocation Start Date. Please edit and save again");
    			return;
    		}
    		
    		//form.getSidecar().set('recentSelectedEventName',eventName);
			this.stackController.recentSelectedEventName = eventName;
    		
    		try {
    			this.stackController.eventsTreePanel.refresh();
    			this.stackController.eventsTreePopPanel.refresh();
    			this.stackController.stackControl.refresh(this.toggleUnavailableRestriction);
				//this.stackController.decideShowMapOrChartPanel();
				this.trigger('app:space:planing:console:refreshMapChart');
    			View.getOpenerView().closeDialog();
    		}catch (e){
    			//Keep working.
    		}
    	}
    }
});