
/**
 * Controller for stack diagram tab.
 * @author Qiang for 22.1; ZY for 23.1
 */
var abAllocWizStackBlTreeController = View.createController('abAllocWizStackBlTreeController', {
	
	/**the building tree controller*/
	allocBlTreeCtrl: null,
		
	buildingDisplayTitlesMap : {},


    // ---------------------------------------------------------------------------------------------
    // BEGIN view initialize Section 
    // ---------------------------------------------------------------------------------------------
	/**
	 * Create restrictions for building-floor two level tree.
	 */
	afterViewLoad: function() {
		// customize the tree nodes 
		this.stackBuildingTreePanel.createRestrictionForLevel = this.createRestrictionForBuildingTreeLevel;
	},
			
	/**
	 * Hide locate button if there are no lat and lon for buildings. 
	 */
	afterInitialDataFetch: function() {
		this.addNewSpaceForm.enableFieldActions('gp.fl_id', false);

		this.stackController = View.controllers.get('abAllocWizStackController');
	},
		
    // ---------------------------------------------------------------------------------------------
    // BEGIN handlers for mapped dom element's  events 
    // ---------------------------------------------------------------------------------------------
	/**
	 * Filter the building panel.
	 */
	onFilterAllocatedBuildings: function() {
		var checked = Ext.getDom('buildingRestrictToFilter').checked;
		if (!checked) {
			this.stackBuildingTreePanel.addParameter('scenarioIdRestriction', getScenarioIdRestriction(this.stackController.scnId) );
			this.stackBuildingTreePanel.addParameter('dateRestriction', "1=1");
			this.stackBuildingTreePanel.addParameter('blIdRestriction', "1=1");
			this.stackBuildingTreePanel.addParameter('siteIdRestriction', "1=1");
			this.stackBuildingTreePanel.refresh();
		} else {
			this.setParametersForAllocatedBuildings();
			this.stackBuildingTreePanel.refresh();
		}
	},
	
	setParametersForAllocatedBuildings: function() {
		this.stackController.scnId = this.stackController.filter['scnId'];
		if(this.stackController.filter['scnId']) {
			var scnIdRestriction = getScenarioIdRestriction(this.stackController.scnId);
			this.stackBuildingTreePanel.addParameter('scenarioIdRestriction', scnIdRestriction);
		} 
    	
    	var fromDate = this.stackController.filter['from_date'];
    	var endDate = this.stackController.filter['end_date'];
		addDateRestrictionParameterToPanel(this.stackBuildingTreePanel, 'dateRestriction', 'gp.date_start', fromDate, endDate, this.isOracle)
		addLocationRestriction([this.stackBuildingTreePanel], this.stackController.filter);
		addOrganizationRestriction([this.stackBuildingTreePanel], this.stackController.filter);
	},
		    
    // ---------------------------------------------------------------------------------------------
    // BEGIN 'Allocated Buildings' tree Section 
    // ---------------------------------------------------------------------------------------------   
	/**
	 * Create restriction for the building tree.
	 */
	createRestrictionForBuildingTreeLevel: function(parentNode, level) {
		var restriction = null;
		var buildingIdName = parentNode.data['gp.buildingIdName'];
		var buildingId = parentNode.data['gp.bl_id'];
		if (level == 1) {
			restriction = new Ab.view.Restriction();
			restriction.addClause('gp.buildingIdName',buildingIdName, '=');
			restriction.addClause('gp.buildingIdName', buildingId, '=', 'OR', true);
		}
		return restriction;
	},
	
	stackBuildingTreePanel_onLocateBuilding: function() {
		var node = this.stackBuildingTreePanel.lastNodeClicked;
		var blId = node.data['gp.bl_id'];
		var lat = node.data['gp.lat'];
		var lon = node.data['gp.lon'];
		if(lat && lon) {
			//refresh to zoom to the extent of the current restriction.
			var restriction = new Ab.view.Restriction();
			restriction.addClause('bl.bl_id', blId, "=", "OR");

			this.trigger('app:space:planing:console:locateBuildingInMap', lon, lat, restriction);
		} 
		else {
			View.alert(getMessage('unGeocode'));
		}
	},
	
	/**
	 * Show the building in stack chart area.
	 */
	stackBuildingTreePanel_onShowBuildingOnStack: function(button,panel,node1) {
		try{
			var node = this.stackBuildingTreePanel.lastNodeClicked;
			var blId = node.data['gp.bl_id'];
			var blName = node.data['gp.buildingIdName'];
			if (this.isBuildingDisplayed(blId)) {
				//TODO A tricky solution to change the title. Can core provide function setTitle(newTitle) for button?
				button.attributes.value.value = getMessage('displayScenarioOnStack');
				this.buildingDisplayTitlesMap[blName] = getMessage('displayScenarioOnStack');
				this.stackController.stackControl.removeBuilding(blId);
				this.stackController.resetShowFloorplanActions(false);
			} else {
				button.attributes.value.value = getMessage('hideScenarioOnStack');
				this.buildingDisplayTitlesMap[blName] = getMessage('hideScenarioOnStack');
				this.stackController.stackControl.addBuilding(blId);
			}
			this.toggleUnavailableAreas = true;
		} catch(e) {
			
		}
		
	},
	
	/**
	 * Remove all the buildings from the stack chart.
	 */
	stackBuildingTreePanel_onHideAllBuildingSp: function() {
		this.stackBuildingTreePanel.refresh();
		var buildings = this.buildingTreeDataSource.getRecords();
		for (var i = 0; i < buildings.length; i++) {
			var blId = buildings[i].getValue('gp.bl_id');
			var blName = buildings[i].getValue('gp.buildingIdName');
			try{
				toggleUnavailableAreas.stackControl.removeBuilding(blId);
				this.buildingDisplayTitlesMap[blName] = getMessage('hideScenarioOnStack');
			}catch(e){
				//Keep working.
			}
		}
		this.toggleUnavailableAreas = true;
		//this.changeDisplayButtonValue(getMessage('displayScenarioOnStack'));
	},

	stackBuildingTreePanel_afterRefresh: function (){
    	var rows = Ext.query('.treeTable .ygtvrow', $('stackBuildingTreePanel'));
		for (var i=0; i<rows.length; i++ ){
			var blName = rows[i].children[0].innerText.replace(/\r\n/g,""); 
			if (	 this.buildingDisplayTitlesMap[blName] == getMessage('hideScenarioOnStack') ){
				rows[i].children[2].children[0].children[0].value = getMessage('hideScenarioOnStack'); 
			}
		}
	},

	/**
	 * Open Add Building Inventory pop-up.
	 */
	stackBuildingTreePanel_onAddBuildingsToGp: function() {
		var allocWizController = View.getOpenerView().controllers.get('allocWiz');
		var scnId = allocWizController.scnId;
		this.addNewSpaceFromInventoryPanel.addParameter('scnId', scnId);
		this.addNewSpaceFromInventoryPanel.refresh();
	},
		 
	/**
	 * Open Add Building Inventory pop-up.
	 */
	stackBuildingTreePanel_onAddNewSpaceToGp: function() {
		var action = this.stackBuildingTreePanel.actions.get('addNewBuildingToSp');
		this.addNewSpaceForm.showInWindow({
			anchor: action.button.el.dom, 
			width: 800,
			height: 360,
			title: getMessage('addNewSpaceTitle')
		});
	},
		 
	/**
	 * Decide whether a floor has been displayed. 
	 */
	isBuildingDisplayed: function(buildingId){
		var showedBuilding = this.stackController.stackControl.config.buildings;
		for (var i = 0;  i < showedBuilding.length; i++) {
			if(showedBuilding[i] == buildingId) {
				return true;
			}
		}
		return false;
	},
	
	/**
	 * Open the dialog for editting the building.
	 */
	openEditBuildingDialog: function() {
		var thisController = this;
		var editBuildingDialog = View.openDialog('ab-alloc-wiz-bl-edit.axvw', null, true, {
			width:800,
    		height:600,
    		
    		afterViewLoad: function(dialogView) {
    			var dialogController = dialogView.controllers.get('buildingEditDialogController');
    			var bldgNode = thisController.stackBuildingTreePanel.lastNodeClicked;
    			var buildingId = bldgNode.data['gp.bl_id'];
    			dialogController.selectedBlId = buildingId;
    		}
		});
	},
	
	/**
	 * Open the dialog for the floor tree
	 */
	openEditFloorDialog: function() {
		var thisController = this;
		var editBuildingDialog = View.openDialog('ab-alloc-wiz-fl-edit.axvw', null, true, {
			width:800,
    		height:600,
    		
    		afterViewLoad: function(dialogView) {
    			var dialogController = dialogView.controllers.get('floorEditDialogController');
    			var currentNode = thisController.stackBuildingTreePanel.lastNodeClicked;
    			var flNameId = currentNode.data['gp.floorIdName'];
    			var flId = flNameId.split(" ")[0];
    			
    			var buildingId = currentNode.parent.data['gp.bl_id'];
    			dialogController.selectedBlId = buildingId;
    			dialogController.selectedFlId = flId;
    		}
		});
	},
	
	/**
	 * Delete building record from the group.
	 */
	stackBuildingTreePanel_onDeleteBuilding: function(button, panel, node) {
		
		var buildingId = node.data['gp.bl_id'];
		if (buildingId == 'UNALLOC') {
			View.alert(getMessage('cannotDeleteUnallocBuilding'));
			return;
		}
		
		var warningMessage = getMessage('deleteBuildingFromGroup');
		var innerThis = this;
		View.confirm(warningMessage, function(button){
			if(button == 'yes') {
				var blId = node.data['gp.bl_id'];
				var restriction = new Ab.view.Restriction();
				restriction.addClause('gp.bl_id', blId, '=');
				restriction.addClause('gp.portfolio_scenario_id', innerThis.stackController.scnId, '=', 'AND', true);
				var records = innerThis.addNewSpaceGpDataSource.getRecords(restriction);
				for(var i = 0; i < records.length; i++) {
					var record = records[i];
					innerThis.addNewSpaceGpDataSource.deleteRecord(record);
					innerThis.stackBuildingTreePanel.refresh();
				}
				innerThis.removeBuildingFromStackControl(blId);
				innerThis.trigger('app:space:planing:console:refreshMapChart');
				//innerThis.decideShowMapOrChartPanel();
			}
		});
	},
	
	/**
	 * Delete floor record from the group.
	 */
	floorTree_onDeleteFloor: function(button, panel, node) {
		
		var buildingId = node.parent.data['gp.bl_id'];
		if (buildingId == 'UNALLOC') {
			View.alert(getMessage('cannotDeleteUnallocFloor'));
			return;
		}
		
		var warningMessage = getMessage('deleteFloorFromGroup');
		var innerThis = this;
		var parent = node.parent;
		View.confirm(warningMessage, function(button){
			if(button == 'yes') {
				var blId = node.parent.data['gp.bl_id'];
				
				var flNameId = node.data['gp.floorIdName'];
				var flNameIdArray = flNameId.split(" ");
				var flId = flNameIdArray[0];
				var restriction = new Ab.view.Restriction();
				restriction.addClause('gp.bl_id', blId, '=');
				restriction.addClause('gp.fl_id', flId, '=');
				restriction.addClause('gp.portfolio_scenario_id', innerThis.scnId, '=');
				var records = innerThis.addNewSpaceGpDataSource.getRecords(restriction);
				for(var i = 0; i < records.length; i++) {
					var record = records[i];
					innerThis.addNewSpaceGpDataSource.deleteRecord(record);
				}
				innerThis.stackBuildingTreePanel.refresh();
				innerThis.removeBuildingFromStackControl();
			}
		});
	},
	
	/**
	 * We refresh the stack after the building is removed
	 */
	removeBuildingFromStackControl: function(buildingId) {
		var buildings = this.stackController.stackControl.config.buildings;
		for (var i = 0; i < buildings.length; i++) {
			if (buildingId == buildings[i]) {
				buildings.splice(i, 1);
				break;
			}
		}
		
		try {
			this.stackController.stackControl.refresh(this.toggleUnavailableRestriction);
		}catch(e){
			
		}
	},
	
	/**
	 * Open building details dialog. 
	 */
	openBuildingDetailsDialog: function(row) {
		var bl_id = row["bl.bl_id"];
		var dialog = View.openDialog("ab-alloc-wiz-bl-edit.axvw",null, true, {
			width:800,
    		height:600,
    		
    		afterViewLoad: function(dialogView) {
    			var dialogController = dialogView.controllers.get('buildingEditDialogController');
    			dialogController.selectedBlId = bl_id;
    		}
    	});
	},
	
	openFloorDetailsDialog: function(row) {
		var bl_id = row["bl.bl_id"];
		var controller = this;
		var allocWizController = View.getOpenerView().controllers.get('allocWiz');
		var scnId = allocWizController.scnId;
		var dialog = View.openDialog("ab-alloc-wiz-stack-gp-floors.axvw",null, true, {
			width:800,
    		height:600,
    		closeButton: false,
    		
    		afterViewLoad: function(dialogView) {
    			var dialogController = dialogView.controllers.get('allocWizGpFloorsController');
    			dialogController.selectedBlId = bl_id;
    			dialogController.scnId = scnId;
    			dialogController.callback = View.controllers.get('abAllocWizStackController').createGroupFromFloorDetailsCallback;
    		}
    	});
	},
	
	/**
	 * 
	 */
	createGroupFromFloorDetailsCallback: function() {
		this.stackController.stackBuildingTreePanel.refresh();
		if (this.stackController.stackControl.config.buildings.length > 0) {
			this.stackController.rebuildStackPanelChart();
		}
	},
	
	/**
	 * Create group records from a building and its all floors.
	 */
	addNewSpaceFromInventoryPanel_onCreateGroupFromBuilding: function() {
		var selectedBuildingRecords = this.addNewSpaceFromInventoryPanel.getSelectedRecords();
		if (selectedBuildingRecords.length == 0) {
			View.alert(getMessage('noSelectedBulding'));
			return;
		}
		
		//Call the workflow rule to create group record.
		this.createGroupsForBuildings(selectedBuildingRecords);
		
		this.addNewSpaceFromInventoryPanel.refresh(new Ab.view.Restriction());
		this.stackBuildingTreePanel.refresh();
		this.eventsTreePanel.refresh();
		this.eventsTreePopPanel.refresh();
		this.trigger('app:space:planing:console:refreshMapChart');
		//this.decideShowMapOrChartPanel();
	},
	
	/**
	 * Call the workflow rule to 'AbCommonResources-PortfolioForecastingService-addRmEmLsGroups' to create groups for the buildings.
	 */
	createGroupsForBuildings: function(buildingList) {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('portfolio_scenario.portfolio_scenario_id', this.stackController.scnId, '=');
		var currentSelectedScenario = this.portfolioScenarioDataSource.getRecords(restriction)[0];
		var dateStart = currentSelectedScenario.getValue('portfolio_scenario.date_start');
		var year = dateStart.getFullYear();
		var month = dateStart.getMonth() + 1;
		if(month < 10) {
			month = "0" + month;
		}
		var day = dateStart.getDate();
		if(day < 10) {
			day = "0" + day;
		}
		dateStart = year + "-" + month + "-" + day;
		var scnLevel = this.stackController.scnLevel;
		
		var blDataSet = new Ab.data.DataSetList();
		blDataSet.addRecords(buildingList);
		
		var flDataSet = new Ab.data.DataSetList();
		try{
			var result =  Workflow.callMethod('AbCommonResources-PortfolioForecastingService-createGroupFromInventory', this.stackController.scnId, blDataSet, flDataSet, dateStart, scnLevel, 0, this.stackController.unitTitle);
		}catch(e) {
			Workflow.handleError(e);
		}
	},
	
	/**
	 * Change the default value of this add new space form.
	 */
	addNewSpaceForm_afterRefresh: function() {
		this.addNewSpaceForm.setFieldValue('gp.bl_id', '');
		this.addNewSpaceForm.setFieldValue('gp.fl_id', '');
		this.addNewSpaceForm.setFieldValue('gp.date_start', this.stackController.asOfDate);
	},
	
	/**
	 * Select the distinct building value from the gp table.
	 */
	selectBuildingsForAddNewSpace: function() {
		var controller = this;
		var selectBuildingActionListener =  function(fieldName, newValue, oldValue) {
            controller.addNewSpaceForm.setFieldValue('gp.bl_id', newValue);
            return false;
        };
        var restriction = getScenarioIdRestriction(this.stackController.scnId);
        
		View.selectValue({
			title: getMessage('selectBuilding'),
	    	fieldNames: ['gp.bl_id'],
	    	selectTableName: 'gp',
	    	selectFieldNames: ['gp.bl_id'],
	    	visibleFieldNames: ['gp.bl_id'],
	    	actionListener: selectBuildingActionListener,
	    	restriction: restriction,
	    	width: 500,
	    	height: 350
		});
	},
	
	/**
	 * Save the new space form.
	 */
	addNewSpaceForm_onSaveNewSpace: function() {
		if ( !this.addNewSpaceForm.canSave() ) {
			return; 
		}
		var dateStart = this.addNewSpaceForm.getFieldValue('gp.date_start');
		if (!dateStart) {
			alert(getMessage('dateStartRequired'));
			return;
		}
		var blId = this.addNewSpaceForm.getFieldValue('gp.bl_id');
		var flId = this.addNewSpaceForm.getFieldValue('gp.fl_id');
		
		var allocType = "Usable Area - Leased";
		var allocTypeValue = jQuery('input[name="spaceAllocationType"]').filter(':checked').val();
		if (allocTypeValue == 'owned') {
			allocType= "Usable Area - Owned";
		}
		
		//save the main gp record
		var usableArea = this.addNewSpaceForm.getFieldValue('gp.area_manual');
		
		var mainGpRecord = new Ab.data.Record();
		mainGpRecord.setValue('gp.bl_id', blId);
		mainGpRecord.setValue('gp.fl_id', flId);
		mainGpRecord.setValue('gp.date_start', dateStart);
		mainGpRecord.setValue('gp.area_manual', usableArea);
		mainGpRecord.setValue('gp.allocation_type', allocType);
		mainGpRecord.setValue('gp.event_name', 'Add New Space');
		mainGpRecord.setValue('gp.portfolio_scenario_id', this.stackController.scnId);
		mainGpRecord.setValue('gp.date_end', this.addNewSpaceForm.getFieldValue('gp.date_end'));
		var mainGpRecordSortOrder = 0;
		if (!isNaN(flId)) {
			mainGpRecordSortOrder = Number(flId);
		}
		mainGpRecord.setValue('gp.sort_order', mainGpRecordSortOrder);
		if (allocType == 'Usable Area - Leased'){
			mainGpRecord.setValue('gp.name', 'Leased Floor');
		} else {
			mainGpRecord.setValue('gp.name', 'Owned Floor');
		}
		
		this.addNewSpaceGpDataSource.saveRecord(mainGpRecord);
		
		//check to see if need to create date available space.
		var dateAvailable = this.addNewSpaceForm.getFieldValue('date_available');
		var otherUnavailableArea = this.addNewSpaceForm.getFieldValue('other_unavailable_area');
		if (dateAvailable > dateStart) {
			var dateAvailableRecord = new Ab.data.Record();
			dateAvailableRecord.setValue('gp.bl_id', blId);
			dateAvailableRecord.setValue('gp.fl_id', flId);
			dateAvailableRecord.setValue('gp.date_start', dateStart);
			dateAvailableRecord.setValue('gp.date_end', addDay(dateAvailable, -1));
			dateAvailableRecord.setValue('gp.area_manual', usableArea - otherUnavailableArea);
			dateAvailableRecord.setValue('gp.allocation_type', 'Unavailable Area');
			dateAvailableRecord.setValue('gp.event_name', 'Add New Space');
			dateAvailableRecord.setValue('gp.portfolio_scenario_id', this.stackController.scnId);
			dateAvailableRecord.setValue('gp.name', 'Unavailable Area');
			
			this.addNewSpaceGpDataSource.saveRecord(dateAvailableRecord);
		}
		
		var verticalPenetrationArea = this.addNewSpaceForm.getFieldValue('vertical_penetration_area');
		if (verticalPenetrationArea > 0) {
			var vpaRecord = new Ab.data.Record();
			vpaRecord.setValue('gp.bl_id', blId);
			vpaRecord.setValue('gp.fl_id', flId);
			vpaRecord.setValue('gp.date_start', dateStart);
			vpaRecord.setValue('gp.area_manual', verticalPenetrationArea);
			vpaRecord.setValue('gp.allocation_type', 'Unavailable - Vertical Penetration Area');
			vpaRecord.setValue('gp.event_name', 'Add New Space');
			vpaRecord.setValue('gp.portfolio_scenario_id', this.scnId);
			vpaRecord.setValue('gp.sort_order', -3);
			vpaRecord.setValue('gp.name', 'Unavailable - Vertical Penetration Area');
			this.addNewSpaceGpDataSource.saveRecord(vpaRecord);
		}
		
		var serviceArea = this.addNewSpaceForm.getFieldValue('service_area');
		if(serviceArea > 0) {
			var serviceAreaRecord = new Ab.data.Record();
			serviceAreaRecord.setValue('gp.bl_id', blId);
			serviceAreaRecord.setValue('gp.fl_id', flId);
			serviceAreaRecord.setValue('gp.date_start', dateStart);
			serviceAreaRecord.setValue('gp.area_manual', serviceArea);
			serviceAreaRecord.setValue('gp.allocation_type', 'Unavailable - Service Area');
			serviceAreaRecord.setValue('gp.event_name', 'Add New Space');
			serviceAreaRecord.setValue('gp.portfolio_scenario_id', this.scnId);
			serviceAreaRecord.setValue('gp.sort_order', -2);
			serviceAreaRecord.setValue('gp.name', 'Unavailable - Service Area');
			this.addNewSpaceGpDataSource.saveRecord(serviceAreaRecord);
		}
		
		var remainingArea = this.addNewSpaceForm.getFieldValue('remaining_area');
		if(remainingArea > 0) {
			var remainingAreaRecord = new Ab.data.Record();
			remainingAreaRecord.setValue('gp.bl_id', blId);
			remainingAreaRecord.setValue('gp.fl_id', flId);
			remainingAreaRecord.setValue('gp.date_start', dateStart);
			remainingAreaRecord.setValue('gp.area_manual', remainingArea);
			remainingAreaRecord.setValue('gp.allocation_type', 'Unavailable - Remaining Area');
			remainingAreaRecord.setValue('gp.event_name', 'Add New Space');
			remainingAreaRecord.setValue('gp.portfolio_scenario_id', this.scnId);
			remainingAreaRecord.setValue('gp.sort_order', -1);
			remainingAreaRecord.setValue('gp.name', 'Unavailable - Remaining Area');
			this.addNewSpaceGpDataSource.saveRecord(remainingAreaRecord);
		}
		
		if (otherUnavailableArea > 0) {
			var otherUnavailableAreaRecord = new Ab.data.Record();
			otherUnavailableAreaRecord.setValue('gp.bl_id', blId);
			otherUnavailableAreaRecord.setValue('gp.fl_id', flId);
			otherUnavailableAreaRecord.setValue('gp.date_start', dateStart);
			otherUnavailableAreaRecord.setValue('gp.area_manual', otherUnavailableArea);
			otherUnavailableAreaRecord.setValue('gp.allocation_type', 'Unavailable Area');
			otherUnavailableAreaRecord.setValue('gp.event_name', 'Add New Space');
			otherUnavailableAreaRecord.setValue('gp.portfolio_scenario_id', this.stackController.scnId);
			otherUnavailableAreaRecord.setValue('gp.sort_order', 0);
			otherUnavailableAreaRecord.setValue('gp.name', 'Unavailable Area');
			this.addNewSpaceGpDataSource.saveRecord(otherUnavailableAreaRecord);
		}
		
		this.stackBuildingTreePanel.refresh();
		this.eventsTreePanel.refresh();
		this.eventsTreePopPanel.refresh();
		this.stackController.refreshStackChartWithNewDate();

		this.addNewSpaceForm.displayTemporaryMessage(getMessage('created'), 2000);
		
		this.addNewSpaceForm.closeWindow.defer(2000, this.addNewSpaceForm);
	},
    // ---------------------------------------------------------------------------------------------
    // BEGIN 'Allocated Buildings' tree Section 
    // ---------------------------------------------------------------------------------------------
    
	// ---------------------------------------------------------------------------------------------
    // BEGIN adding sql parameter Section 
    // ---------------------------------------------------------------------------------------------
 	/**
 	 * Add parameters for the building tree datasource.
 	 */
 	addParametersForBuildingTreeDataSource: function() {
		var scnIdRestriction = getScenarioIdRestriction(this.stackController.scnId);
 		this.buildingTreeDataSource.addParameter('scenarioIdRestriction', scnIdRestriction);
 		
 		var fromDate = this.stackController.filter['from_date'];
    	var endDate = this.stackController.filter['end_date'];
    	var dateRestriction = "1=1";
    	if (fromDate != null && fromDate != "") {
    		if (this.stackController.isOracle == 1) {
    			dataRestriction = "gp.date_start >= to_date('" + fromDate + "', 'YYYY-MM-DD')";
    		} else {
    			dateRestriction = "gp.date_start >= '" + fromDate + "'";
    		}
    	}
    	if(endDate != null &&  endDate != "") {
    		if (this.stackController.isOracle == 1) {
    			dateRestriction = dateRestriction + " AND gp.date_start <= to_date('" + endDate + "', 'YYYY-MM-DD')";
    		} else {
    			dateRestriction = dateRestriction + " AND gp.date_start <= '" + endDate + "'";
    		}
    	}
    	this.buildingTreeDataSource.addParameter('dateRestriction', dateRestriction);
    	
		addLocationRestriction([this.buildingTreeDataSource], this.stackController.filter);
		addOrganizationRestriction([this.buildingTreeDataSource], this.stackController.filter);
 	}
});