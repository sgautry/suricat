
/**
 * Controller for stack diagram tab.
 * @author Qiang for 22.1; ZY for 23.1
 */
var abAllocWizStackController = View.createController('abAllocWizStackController', {
	
	/**The scenario id for this.  */
	scnId: null,
	
	/**The scenario name for this.  */
	scnName: null,

	/**The scenario level of current scenatio.*/
	scnLevel: "",

	/**the filter from the filter console. */
	filter: null,

	/**the building tree controller*/
	allocBlTreeCtrl: null,
		
	/**The stack control for displaying building. */
	stackControl: null,
		
	/** The as of date parameter for map control.*/
	asOfDate: null,

	/** If the stack chart will show all the buildings.*/
	isShowAll: false,
	
	/** The unit title of current user.*/
	unitTitle: '',
			
	areaUnitsConversionFactor: 1.0,
	
	isOracle: 0,

	buildingDisplayTitlesMap : {}, 
	
	/** Indicate if current clicked stack element's group has the associated sb_items.*/
	currentGpHasSbItems: false,	
	
    // ---------------------------------------------------------------------------------------------
    // BEGIN view initialize Section 
    // ---------------------------------------------------------------------------------------------
    afterCreate: function() {
        this.on('app:space:planing:console:updateAsOfDate', this.updateAsOfDate);
    },

	/**
	 * Create restrictions for building-floor two level tree.
	 */
	afterViewLoad: function() {		
		//initial the current unit.
		this.initialCurrentUnit();

		this.initialStringStartsWithFunc();
	},
	
	/**
	 * Initially set the unit title and conversion factor of current user.
	 */
	initialCurrentUnit: function() {
		this.unitTitle = getMessage('unitTitleMetric');
		var units = View.project.units;
		if (units == "imperial") {
			this.unitTitle = getMessage('unitTitleImperial');
		}

		if(View.activityParameters["AbCommonResources-ConvertAreasLengthsToUserUnits"]=="1"){
			if(View.user.displayUnits != View.project.units){
				this.areaUnitsConversionFactor = 1.0 / parseFloat(View.user.areaUnits.conversionFactor);
			}
		}
	},
		
	/**
	 * Initially register a startsWith function to string object.
	 */
	initialStringStartsWithFunc: function() {
		//register a startsWith function to string object.
		if (typeof String.prototype.startsWith != 'function') {
			String.prototype.startsWith = function(prefix) {
				return this.slice(0, prefix.length) === prefix;
			};
		}	
	},

	/**
	 * Hide locate button if there are no lat and lon for buildings. 
	 */
	afterInitialDataFetch: function() {
    	this.isOracle = isOracleDataBase(this.checkOracleDataSource);

		this.refreshUponOpeningView();

		//this.initialAsOfDate();
		
		this.createBuildingStackChart();
	},
	
	/**
	 * Set the as of date for map control, chart tab control and stack panel control.
	 */
	initialAsOfDate: function() {
		if ( this.filter && this.filter['from_date'] ) {
			this.asOfDate = this.filter['from_date'];
		} 
		else {
			var restriction = new Ab.view.Restriction();
			restriction.addClause('portfolio_scenario.portfolio_scenario_id', this.scnId, '=');
			var scenario = this.portfolioScenarioDataSource.getRecords(restriction)[0];
			var asOfDate = scenario.getValue('portfolio_scenario.date_start');
			if (asOfDate) {
				this.asOfDate = getIsoFormatDate(asOfDate);
			}
		}
	},

	/**
	 * Create the building stack chart.
	 */
	createBuildingStackChart: function() {
		var thisController = this;
		var defaultStackOrientation =  View.controllers.get('abAllocWizStackActionController') ? View.controllers.get('abAllocWizStackActionController').currentStackOrientation : 'HORIZONTAL' ; 
		var config = new Ab.view.ConfigObject({
			stackOrientation: defaultStackOrientation,
			displayFloorHeight: 20,
			buildings: [],
			groupDatasourceView: 'ab-alloc-wiz-stack-datasource.axvw',
			groupDatasource: 'abAllocWizStackDs_gp',
			buildingDatasourceView: 'ab-alloc-wiz-stack-datasource.axvw',
			buildingDatasource: 'abAllocWizStackStatsDs_bl',
			floorDatasourceView: 'ab-alloc-wiz-stack-datasource.axvw',
			floorDatasource: 'abAllocWizStackStatsDs_fl',
			showAllLabels:false,
			portfolioScenario: thisController.scnId,
			asOfDate: thisController.asOfDate,
			dropHandler: thisController.onDrop,
			clickHandler: thisController.onClickStackChart,
			tooltipHandler: thisController.onTooltip,
			showXAxis: true,
			showHighlightFloor: false
		});
		this.stackControl = new Ab.stack.HtmlStack('scenarioStackContainer', config);
		this.addParameterForStackGroupDataSource();
		
		//reconfigure the labels
		this.configureStackChartGroupLabels();
	},
	
	/**
	 * Add parameters for the stack group datasource.
	 */
	addParameterForStackGroupDataSource: function() {
		this.stackControl.addParameter('portfolio_scenario_id', this.scnId);
		this.stackControl.addParameter('asOfDate', this.asOfDate);
		this.stackControl.addParameter('portfolioScenarioId', getScenarioIdRestriction(this.scnId));
		
		if ( this.isOracle == 1 ) {
			this.addParameterOnOracleDB();
		} 
		else {
			this.addParameterOnOtherDB();			
		}
	},
	
	addParameterOnOracleDB: function() {
		var startDateAsOfDate = " (gp.date_start <= to_date('" + this.asOfDate + "', 'YYYY-MM-DD'))";
		var endDateAsOfDate = " (gp.date_end >= to_date('" + this.asOfDate + "', 'YYYY-MM-DD') OR gp.date_end IS NULL)";
		var innerDateStartAsOfDate = " (gp_inner.date_start <= to_date('" + this.asOfDate + "', 'YYYY-MM-DD'))";
		var innerDateEndAsOfDate = " (gp_inner.date_end >= to_date('" + this.asOfDate + "', 'YYYY-MM-DD') OR gp_inner.date_end IS NULL)";
		this.stackControl.addParameter('dateStartAsOfDate', startDateAsOfDate);
		this.stackControl.addParameter('dateEndAsOfDate', endDateAsOfDate);
		this.stackControl.addParameter('innerDateStartAsOfDate', innerDateStartAsOfDate);
		this.stackControl.addParameter('innerDateEndAsOfDate', innerDateEndAsOfDate);
	},

	addParameterOnOtherDB: function() {
		var startDateAsOfDate = " (gp.date_start <= '" + this.asOfDate + "')";
		var endDateAsOfDate = " (gp.date_end >= '" + this.asOfDate + "' OR gp.date_end IS NULL)";
		var innerDateStartAsOfDate = " (gp_inner.date_start <= '" + this.asOfDate + "')";
		var innerDateEndAsOfDate = " (gp_inner.date_end >= '" + this.asOfDate + "' OR gp_inner.date_end IS NULL)";
		this.stackControl.addParameter('dateStartAsOfDate', startDateAsOfDate);
		this.stackControl.addParameter('dateEndAsOfDate', endDateAsOfDate);
		this.stackControl.addParameter('innerDateStartAsOfDate', innerDateStartAsOfDate);
		this.stackControl.addParameter('innerDateEndAsOfDate', innerDateEndAsOfDate);
	},

	/**
	 * We need to show the different labels.
	 */
	configureStackChartGroupLabels: function() {
		var groupDataSourceConfig = {
			groupLabels: 'gp.name;gp.area_manual;gp.count_em',
			buildingField: 'gp.bl_id',
			floorField: 'gp.fl_id',
			floorNameField: 'gp.fl_name',
			groupField: 'gp.gp_id',
			allocationTypeField: 'gp.allocation_type',
			areaField:  'gp.area_manual',
			headcountField: 'gp.count_em',
			highlightField: 'gp.hpattern_acad',
			sortOrderField: 'gp.sort_order',
			dateStartField: 'gp.date_start',
			dateEndField: 'gp.date_end',
			portfolioScenarioField: 'gp.portfolio_scenario_id'
		};
		this.stackControl.stackControl.setGroupDataSourceConfig(groupDataSourceConfig);
	},
    // ---------------------------------------------------------------------------------------------
    // END view initialize Section 
    // ---------------------------------------------------------------------------------------------

    // ---------------------------------------------------------------------------------------------
    // BEGIN stack control - tooltip Section 
    // ---------------------------------------------------------------------------------------------
	/**
	 * Return a text as tool tip when the mouse is over.
	 */
	onTooltip: function(item) {
		
		var allocationId = item['gp.gp_id'];
		var restriction = new Ab.view.Restriction();
		restriction.addClause('gp.gp_id', allocationId, '=');
		var record = abAllocWizStackController.groupRecordsDataSource.getRecords(restriction)[0];
		if (!record)	{
			return;
		}

		var allocationType = record.getValue('gp.allocation_type');
		var str="";
		if ( allocationType == 'Allocated Area' ) {
			str = abAllocWizStackController.showAllocatedAreaToolTip(item, record);
		} 
		else if ( allocationType.startsWith('Unavailable') ) {
			str = abAllocWizStackController.showUnavailableAreaToolTip(item, record);
		} 
		else {
			str = "<b>"+getMessage("availAreaTooltip")+"</b>";
			str += "<br/><strong>"+getMessage("areaTooltip")+": " + item['gp.area_manual'] + " " + View.user.areaUnits.title + "</strong>";
		}
		return str;
	},
	
	/**
	 * Show the tooltip for allocated area.
	 */
	showAllocatedAreaToolTip: function(item, record) {
		var str = "<b>"+getMessage("allocAreaTooltip")+"</b>";
		str += "<br/><strong>"+getMessage("buTooltip")+": " + record.getValue('gp.planning_bu_id') + "</strong>";

		if ( this.scnLevel == 'dv' || this.scnLevel == 'dp' || this.scnLevel == 'fg' ) {
			str += "<br/><strong>"+getMessage("dvTooltip")+": " + item['gp.dv_id'] + "</strong>";
		}
		if ( this.scnLevel == 'dp' || this.scnLevel == 'fg' ) {
			str += "<br/><strong>"+getMessage("dpTooltip")+": " + record.getValue('gp.dp_id') + "</strong>";
		}
		if ( this.scnLevel == 'fg' ) {
			str += "<br/><strong>"+getMessage("fgTooltip")+": " + record.getValue('gp.gp_function') + "</strong>";
		}

		str += "<br/><strong>"+getMessage("areaTooltip")+": " + item['gp.area_manual'] + " " + View.user.areaUnits.title + "</strong>";
		str += "<br/><strong>"+getMessage("hcTooltip")+": " + record.getValue('gp.count_em') + "</strong>";
		str += "<br/><strong>"+getMessage("startDateTooltip")+": " + abAllocWizStackController.formatToolTipDate(record.getValue('gp.date_start')) + "</strong>";
		if (record.getValue('gp.date_end')) {
			str += "<br/><strong>"+getMessage("endDateTooltip")+": " + abAllocWizStackController.formatToolTipDate(record.getValue('gp.date_end')) + "</strong>";
		} else {
			str += "<br/><strong>"+getMessage("endDateTooltip")+":</strong>";
		}
		
		str += "<br/><strong>"+getMessage("eventTooltip")+": " + record.getValue('gp.event_name') + "</strong>";
		
		if (this.scnLevel == 'fg') {
			str += this.getFunctionGroupToolTipText(item);
		}

		return str;
	},

	/**
	 * Return the tooltip text for Function Group level.
	 */
	getFunctionGroupToolTipText: function(item) {
		var text = "";
		var gpId = item['gp.gp_id'];
		var restriction = new Ab.view.Restriction();
		restriction.addClause('sb_items.gp_id', gpId, '=');
		var records = this.sbItemsForToolTipDs.getRecords(restriction);

		if (records.length > 0) {
			text += "<br/><strong>"+getMessage("fgcTooltip")+":</strong>";
		}

		for ( var i = 0; i < records.length; i++ ) {
			var record = records[i];
			var maxPxValue = record.getValue('sb_items.p01_value');
			for (var j = 1 ; j < 13; j++) {
				var field = '';
				if (j < 10) {
					field = 'sb_items.p0' + j + '_value';
				} else {
					field = 'sb_items.p' + j + '_value';
				}
				var maxValue = record.getValue(field);
				if(maxValue > maxPxValue) {
					maxPxValue = maxValue;
				}
			}
			text += "<br/><strong>" + record.getValue('sb_items.rm_std') + "(" + maxPxValue +")</strong>";
		}

		return text;
	},

	/**
	 * Show the tooltip for unavailable area
	 */
	showUnavailableAreaToolTip: function(item, record) {
		var allocationType = record.getValue('gp.allocation_type');
		var str = "";
		if ( allocationType == 'Unavailable - Vertical Penetration Area' ) {
			str = "<b>"+getMessage("vpAreaTooltip")+"</b>";
		} 
		else if ( allocationType == 'Unavailable - Service Area' ) {
			str = "<b>"+getMessage("servAreaTooltip")+"</b>";
		} 
		else if ( allocationType == 'Unavailable - Remaining Area' ) {
			str = "<b>"+getMessage("remAreaTooltip")+"</b>";
		} 
		else {
			str = "<b>"+getMessage("unavailAreaTooltip")+"</b>";
		}

		str += "<br/><strong>"+getMessage("areaTooltip")+": " + item['gp.area_manual'] + " " + View.user.areaUnits.title + "</strong>";
		str += "<br/><strong>"+getMessage("startDateTooltip")+": " + abAllocWizStackController.formatToolTipDate(record.getValue('gp.date_start')) + "</strong>";
		
		if ( record.getValue('gp.date_end') ) {
			str += "<br/><strong>"+getMessage("endDateTooltip")+": " + abAllocWizStackController.formatToolTipDate(record.getValue('gp.date_end')) + "</strong>";
		} 
		else {
			str += "<br/><strong>"+getMessage("endDateTooltip")+":</strong>";
		}
		
		str += "<br/><strong>"+getMessage("eventTooltip")+": " + record.getValue('gp.event_name') + "</strong>";
		return str;
	},
	
	formatToolTipDate: function(dateFromDs) {
		var year = dateFromDs.getFullYear();
		var month = dateFromDs.getMonth() + 1;
		if(month < 10) {
			month = "0" + month;
		}
		var day = dateFromDs.getDate();
		if(day < 10) {
			day = "0" + day;
		}
		var dateString = FormattingDate(day, month, year, strDateShortPattern);
		return dateString;
	},
    // ---------------------------------------------------------------------------------------------
    // END stack control - tooltip Section 
    // ---------------------------------------------------------------------------------------------
	
    // ---------------------------------------------------------------------------------------------
    // BEGIN stack control - onClick Section 
    // ---------------------------------------------------------------------------------------------
	/**
	 * When user click the stack chart, save the bl_id and fl_id for showing floor plan.
	 */
	onClickStackChart: function(item) {
		var stackController = View.controllers.get('abAllocWizStackController');
		stackController.showBuildingId = item['gp.bl_id'];
		stackController.showFloorId = item['gp.fl_id'];
		stackController.editGroupId = item['gp.gp_id'];

		stackController.resetShowFloorplanActions(true);
		stackController.resetSpaceRquirementActions();
	},
	
	resetSpaceRquirementActions: function() {
		var stackController = View.controllers.get('abAllocWizStackController');
		// process logics related to button 'Space Requirements: Edit' and refreshing of Space Requirement tab.
		if ( stackController.abAllocWizStackDs_hasSbItemsGp.getRecords("gp.gp_id="+stackController.editGroupId).length>0) {
			stackController.currentGpHasSbItems = true;
			jQuery("#editSpaceRequirement").removeClass("x-item-disabled ui-state-disabled actionbar x-btn button:disabled");
		}	
		else {
			// if previously selected a group with associated sb_items but this time selected a group without associated_items,
			// then need to reset the Space Rquirement tab to show all sb_items of current scenario.
			if (stackController.currentGpHasSbItems){
				stackController.resetSpaceRquirementTab();
			}

			stackController.currentGpHasSbItems = false;
			jQuery("#editSpaceRequirement").addClass("x-item-disabled ui-state-disabled actionbar x-btn button:disabled");
		}
	},

	resetSpaceRquirementTab: function() {
		var stackActionController = View.controllers.get('abAllocWizStackActionController');
		var sbCtrl = stackActionController.getSpaceRquirementTabController();
		if (sbCtrl){
			sbCtrl.sbName = this.scnName;
			sbCtrl.gpId = null;
			sbCtrl.refreshGrid();
		} 
		else {
			var tabs = View.parentTab.parentPanel;
			tabs.scnName = this.scnName;
			tabs.associatedGpId = null;
		}
	},
	
	resetShowFloorplanActions: function(isShow) {
		if (isShow) {
			jQuery('#showFloorPlan').removeClass('x-item-disabled ui-state-disabled');
		}	
		else {
			jQuery('#showFloorPlan').addClass('x-item-disabled ui-state-disabled');
		}
	},
    // ---------------------------------------------------------------------------------------------
    // END stack control - onClick Section 
    // ---------------------------------------------------------------------------------------------

    // ---------------------------------------------------------------------------------------------
    // BEGIN stack control - onDrop Section 
    // ---------------------------------------------------------------------------------------------
	/**
	 * 
	 */
	onDrop: function(obj) {
		var recordToUpdate = obj.sourceRecord,
		targetRecord = obj.targetRecord,
		previousRecord = obj.previousRecord;

		var thisController = abAllocWizStackController;
		var actionController = View.controllers.get('abAllocWizStackActionController');

		if ( !thisController.checkIfCanEditAllocation(recordToUpdate['gp.gp_id']) ) {
			View.alert(getMessage('cannotEditThisAllocation'));
			return;
		}
		
		// get record by id
		var restriction = new Ab.view.Restriction();
		restriction.addClause('gp.gp_id', recordToUpdate['gp.gp_id']);
		var records = View.dataSources.get('abAllocWizStackDs_gp').getRecords(restriction); 
	
		// update record with new values
		try {
			if (records.length > 0) {
				var fromBlId = recordToUpdate['gp.bl_id'];
				var fromFlId = recordToUpdate['gp.fl_id'];
				var toBlId = targetRecord['gp.bl_id'];
				var toFlId = targetRecord['gp.fl_id'];
				var targetOrder = targetRecord['gp.sort_order'];
				thisController.sourceObject = obj;
				if (records[0].getValue('gp.option1')=='1')	{
					//do negative logics
					actionController.performopenNegativeAllocation(records[0],toBlId,toFlId);
				} 
				 //if the source bl_id is different from target bl_id or is the source fl_id, then create group move dialog.
				else if (fromBlId != toBlId || fromFlId != toFlId) {
					var groupMoveConfirmDialog = View.openDialog('ab-alloc-wiz-stack-confirm-form.axvw', null, true, {
					width: 800,
					height: 450,
					title: getMessage("groupMoveConfirm"),
					closeButton: false,
					
					afterViewLoad : function(dialogView) {
						var dialogController = dialogView.controllers.get('abAllocWizStackConfirmController');
						dialogController.stackController = 	 thisController;
						dialogController.recordToUpdate = recordToUpdate;
						dialogController.targetRecord	 = targetRecord;

						dialogController.fromBlId = fromBlId;
						dialogController.fromFlId = fromFlId;
						dialogController.toBlId = toBlId;
						dialogController.toFlId = toFlId;
						dialogController.lsId = records[0].getValue('gp.ls_id');

						dialogController.fromGroupName = recordToUpdate['gp.name'];
						dialogController.eventName = recordToUpdate['gp.event_name'];
						dialogController.lastStoredEventNameForAllocating = thisController.lastStoredEventNameForAllocating;

						dialogController.callback = thisController.commonGroupCallback;
					}
				});
				
				} else {
					//only update the sort order.
					var record = records[0];
					record.isNew = false;
			    	var newSortOrder = thisController.calculateNewSortOrder(recordToUpdate['gp.gp_id']);
			    	record.setValue('gp.sort_order', newSortOrder);
			    	// save record
			    	View.dataSources.get('abAllocWizStackDs_gp').saveRecord(record);
			    	// refresh stack
			    	thisController.stackControl.refresh(thisController.toggleUnavailableRestriction);
				}
			}		
		} catch (e) {
			alert(e.message);		
		}
	},
    
	/**
	 * If the allocation is a parent allocation, it can't be editted.
	 */
	checkIfCanEditAllocation: function(allocationId) {
		var restriction = new Ab.view.Restriction();
		restriction.addClause("gp.parent_group_id", allocationId, '=');
		var records = this.groupRecordsDataSource.getRecords(restriction);
		if (records.length > 0) {
			return false;
		}
		return true;
	},
    // ---------------------------------------------------------------------------------------------
    // END stack control - onDrop Section 
    // ---------------------------------------------------------------------------------------------

    /**
     * Calculate the new sort order.
     */
    calculateNewSortOrder: function(sourceGpId) {
    	var thisController = abAllocWizStackController;
    	var updateRecord = thisController.sourceObject.sourceRecord;
    	var targetRecord = thisController.sourceObject.previousRecord;
    	var hasPrevious = true;
    	if(!targetRecord) {
    		targetRecord = thisController.sourceObject.targetRecord;
    		hasPrevious = false;
    	} else {
    		targetRecord = thisController.sourceObject.targetRecord;
    	}
    	
    	var targetSortOrder = targetRecord['gp.sort_order'];
    	var targetAllocationType = targetRecord['gp.allocation_type'];
    	if(!targetAllocationType) {
    		targetAllocationType = targetRecord['gp.allocation_type.raw'];
    	}
    	if (targetAllocationType.startsWith('Unavailable')) {
    		targetSortOrder = 0;
    	} else if(targetAllocationType.startsWith('Available') && !hasPrevious) {
    		targetSortOrder = 0;
    	}
    	var newSortOrder = Number(targetSortOrder) + 1;
    	var restriction = new Ab.view.Restriction();
    	restriction.addClause('gp.portfolio_scenario_id', this.scnId, '=');
    	restriction.addClause('gp.bl_id', targetRecord['gp.bl_id'], '=', 'AND', true);
    	restriction.addClause('gp.fl_id', targetRecord['gp.fl_id'], '=', 'AND', true);
    	restriction.addClause('gp.sort_order', targetSortOrder, '>=', 'AND', true);
    	restriction.addClause('gp.gp_id', sourceGpId, '<>', 'AND', true);
    	
    	var records = this.groupRecordsDataSource.getRecords(restriction);
    	if(records.length > 0) {
    		var smallest = records[0].getValue('gp.sort_order');
        	if (smallest <= newSortOrder) {
        		//update the sort_order of the right records.
    			var currentBiggest = newSortOrder + 1;
    			for (var i = 0; i < records.length - 1; i++) {
    				records[i].setValue('gp.sort_order', currentBiggest);
    				var nextRecord = records[i+1];
    				var nextSortOrder = Number(nextRecord.getValue('gp.sort_order'));
    				if (nextSortOrder <= currentBiggest) {
    					currentBiggest = currentBiggest + 1;
    				} else {
    					currentBiggest = nextSortOrder;
    				}
    			}
    			//update the new sort value for the right records
    			for (var j = 0; j < records.length; j++) {
    				var updateRecord = records[j];
    				updateRecord.isNew = false;
    				this.groupRecordsDataSource.saveRecord(updateRecord);
    			}
        	}
    	}
    	return newSortOrder;
    },
    // ---------------------------------------------------------------------------------------------
    // END group move confirmation form Section 
    // ---------------------------------------------------------------------------------------------
 	
    // ---------------------------------------------------------------------------------------------
    // BEGIN refresh logics Section 
    // ---------------------------------------------------------------------------------------------
	/**
	 * When the scenario is changed, we need to refresh the map, building, stack and chart tab.
	 */
	applyChangedScenario: function(filter) {
		this.refreshTab(filter);
		this.initialAsOfDate();
		this.setPortfolioScenarioLevel();
		//this.updateAsOfDate(this.asOfDate);
		//this.decideShowMapOrChartPanel();
		this.trigger('app:space:planing:console:refreshMapChart');
		this.rebuildStackPanelChart();
		this.eventsTreePanel.refresh();
		this.eventsTreePopPanel.refresh();
    	this.stackBuildingTreePanel.refresh();
		this.trigger('app:space:planing:console:refreshActionStates');
	},
	
	/**
	 * When use select a new portfolio scenario, update the copy of it.
	 */
	setPortfolioScenarioLevel: function() {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('portfolio_scenario.portfolio_scenario_id', this.filter['scnId'], '=');
		var portfolioScenario = this.portfolioScenarioDataSource.getRecords(restriction)[0];
		this.scnLevel = portfolioScenario.getValue('portfolio_scenario.scn_level');
		this.scnName = portfolioScenario.getValue('portfolio_scenario.scn_name');
	},
	
	/**
	 * When the as of date is changed, we need to update the stack chart and map control.
	 */
	asOfDateChanged: function() {
		var newAsOfDate = jQuery("#asOfDateInput").val();
		if (newAsOfDate) {
			this.addNewSpaceForm.setFieldValue('gp.date_start', newAsOfDate);
			var localizedDate = this.addNewSpaceForm.getFieldValue('gp.date_start');
			this.asOfDate = localizedDate;
		}
		this.editGroupId = '';
		this.lastStoredEventNameForAllocating = '';
		try{
			//this.buildScenarioThematic();
			this.trigger('app:space:planing:console:buildScenarioThematic');
		}catch(e) {
			
		}
		this.refreshStackChartWithNewDate();
	},
	
	/**
	 * When the as of date is changed, we need to refresh the stack chart and display the original buildings if they exist.
	 */
	refreshStackChartWithNewDate: function() {
		var originalBuildings = jQuery.extend(true, [], this.stackControl.config.buildings);
		this.rebuildStackPanelChart();
		this.stackControl.config.buildings = originalBuildings;
		this.stackControl.addParameter('portfolio_scenario_id', this.scnId);
		this.addParameterForStackGroupDataSource();
		try{
			this.stackControl.refresh(this.toggleUnavailableRestriction);
			//kb#3049590: also refresh the map/chart when stack is refreshed for asOfDate change or allocation change.
			//View.controllers.get('abAllocWizStackMapController').decideShowMapOrChartPanel();
			this.trigger('app:space:planing:console:refreshMapChart');
		}catch(e) {
			alert(e);
			//keep work when error happens.
		}
	},

	/**
	 * When the tab is opened , we need to filter all the panels with the scenario_id from the select scenario tab.
	 */
	refreshUponOpeningView: function() {
		var allocWizController = View.getOpenerView().controllers.get('allocWiz');
		this.scnId = allocWizController.scnId;
		
		// also include the console's filtr when openning the view firstly
		if ( allocWizController.filter ) {
			this.filter = allocWizController.filter;
		} 
		else {
			this.filter = {};
		}

		var restriction = new Ab.view.Restriction();
		restriction.addClause('portfolio_scenario.portfolio_scenario_id', this.scnId, '=');
		var currentSelectedScenario = this.portfolioScenarioDataSource.getRecords(restriction)[0];
		this.scnLevel = currentSelectedScenario.getValue('portfolio_scenario.scn_level');
		this.scnName = currentSelectedScenario.getValue('portfolio_scenario.scn_name');

		this.filter['scnId'] = this.scnId;
		this.refreshTab(this.filter);
	},

    /**
     * Refresh tabs when there are values from the filter console.
     */
	refreshTab: function(filter) {
		if(filter != null) {
			this.filter = filter;
		}
		this.scnId = this.filter['scnId'];
		if(this.scnId != null && this.scnId != '') {
			var scnIdRestriction = getScenarioIdRestriction(this.scnId);
			this.eventsTreePanel.addParameter('scenarioIdRestriction', scnIdRestriction);
			this.eventsTreePopPanel.addParameter('scenarioIdRestriction', scnIdRestriction);
			this.stackBuildingTreePanel.addParameter('scenarioIdRestriction', scnIdRestriction);
			//kb#3050891: Add the ability to filter by organization	
			addLocationRestriction([this.stackBuildingTreePanel], this.filter);
			addOrganizationRestriction([this.stackBuildingTreePanel], this.filter);
			this.setPortfolioScenarioLevel();
		} 

		var panels = [this.eventsTreePanel, this.eventsTreePopPanel];
		addDateRestriction(panels, this.filter, this.checkOracleDataSource);
		addLocationRestriction(panels, this.filter);
		//kb#3050891: Add the ability to filter by organization	
		addOrganizationRestriction(panels, this.filter);
    	
    	//Add areaConversionFactor for the eventsTreePanel and eventsTreePopPanel
    	this.eventsTreePanel.addParameter('areaUnitsConversionFactor', this.areaUnitsConversionFactor);
    	this.eventsTreePopPanel.addParameter('areaUnitsConversionFactor', this.areaUnitsConversionFactor);
    	
    	this.initialAsOfDate();
    	this.eventsTreePanel.refresh();
    	this.eventsTreePopPanel.refresh();
    	this.stackBuildingTreePanel.refresh();
    	if(this.stackControl) {
			this.updateAsOfDate(this.asOfDate, true);
    		//this.setInitialValueForDateInput();
    		//this.refreshStackChartWithNewDate();
			this.resetShowFloorplanActions(false);
    	}
    	
    	try{
    		//this.buildScenarioThematic();
			this.trigger('app:space:planing:console:buildScenarioThematic');
    	}catch(e){
    		Workflow.handleError(e);
    	}
	},
	
	/**
	 * When the portfolio scenario is changed, the as of date and buildings must be rebuild. We just clear the buildings and then refresh
	 * to force the stack control to fetch data again.
	 */
	rebuildStackPanelChart: function() {
		this.stackControl.config.portfolioScenario = this.scnId;
		this.stackControl.config.asOfDate = this.asOfDate;
		this.stackControl.config.buildings = [];
		this.stackControl.addParameter('portfolio_scenario_id', this.scnId);

		var startDateAsOfDate = " (gp.date_start <= '" + this.asOfDate + "')";
		var endDateAsOfDate = " (gp.date_end >= '" + this.asOfDate + "' OR gp.date_end IS NULL)";
		if(this.isOracle == 1) {
			startDateAsOfDate = " (gp.date_start <= to_date('" + this.asOfDate + "', 'YYYY-MM-DD'))";
			endDateAsOfDate = " (gp.date_end >= to_date('" + this.asOfDate + "', 'YYYY-MM-DD') OR gp.date_end IS NULL)";
		}
		
		this.stackControl.addParameter('dateStartAsOfDate', startDateAsOfDate);
		this.stackControl.addParameter('dateEndAsOfDate', endDateAsOfDate);
		this.stackControl.addParameter('asOfDate', this.asOfDate);
		try {
			this.stackControl.refresh(this.toggleUnavailableRestriction);
		} catch(e){
			//When the buildings is cleared, the core will throw unnecessary errors. 
		} 
	},

		/**
	 * When the portfolio scenario is changed, the as of date and buildings must be rebuild. We just clear the buildings and then refresh
	 * to force the stack control to fetch data again.
	 */
	updateAsOfDate: function(asOfDate, isRefresh) {
		this.asOfDate = asOfDate;
		this.addNewSpaceForm.setFieldValue('gp.date_start', this.asOfDate);
		var localizedValue = this.addNewSpaceForm.getFieldValue('gp.date_start');
		var localizedValueArray = localizedValue.split("-");
		var dateString = FormattingDate(localizedValueArray[2], localizedValueArray[1], localizedValueArray[0], strDateShortPattern);
		jQuery("#asOfDateInput").val(dateString);
		this.addNewSpaceForm.setFieldValue("gp.date_start","");

		if (isRefresh)	{
			this.refreshStackChartWithNewDate();
		}
	},
    // ---------------------------------------------------------------------------------------------
    // END refresh logics Section 
    // ---------------------------------------------------------------------------------------------
		
	// ---------------------------------------------------------------------------------------------
    // BEGIN Common function Section 
    // ---------------------------------------------------------------------------------------------
	
	/**
	 * Display the stack chart title after the map and stack chart.
	 */
	displayStackChartTitlebar: function() {
		jQuery("#stackChartTitlebar").show();
	}		
    // ---------------------------------------------------------------------------------------------
    // END Common function Section 
    // ---------------------------------------------------------------------------------------------

});