
/**
 * Controller for stack diagram tab.
 * @author Qiang for 22.1; ZY for 23.1
 */
var abAllocWizStackActionController = View.createController('abAllocWizStackActionController', {
	
	/**The building's statistics fields of stack control . */
	stackControlBlStatisFields: null,
	/**The floor's statistics fields of stack control . */
	stackControlFlStatisFields:null,

	/** The current stack orientation, defaults to VERTICAL. */
	currentStackOrientation: "HORIZONTAL",
		
	/** The group to edit.*/
	editGroupId: '',
	
	/** Map html DOM elements events to the specific functions.*/
	events: {
		
		"click #addSpaceRequirement": function() {
			this.addSpaceRequirement();
		},
		
		"click #editSpaceRequirement": function() {
			this.editSpaceRequirement();
		},
		
		"click #syncSpaceRequirement": function() {
			this.syncSpaceRequirement();
		},
		
		"click #showFloorPlan": function() {
			this.showFloorPlan();
		},
		
		"click #addFloorTask": function() {
			this.addFloorTask();
		},
		
		"click #editGroup": function() {
			this.editAllocation();
		},
		
		"click #splitGroup": function() {
			this.splitGroup();
		},
		
		"click #selectStatistics": function() {
			this.openStatisticsConfig();
		},
		
		"click #addNewTypeOfAllocation": function() {
			this.showAddNewAllocationSubMenuItems();
		}, 
		
		"click #increaseFloorHeight": function() {
			this.changeFloorHeightByFactor(5);
		},
		
		"click #decreaseFloorHeight": function() {
			this.changeFloorHeightByFactor(-5);
		},
		
		"click #increaseFloorLength": function() {
			this.changeFloorLength(0.1);
		},
		
		"click #decreaseFloorLength": function() {
			this.changeFloorLength(-0.1);
		},
		
		"click #switchOrientation": function() {
			this.stackChartOrientationChanged();
		},
		
		"click #increaseLabelHeight": function() {
			this.changeLabelHeightByFactor(2);
		},
		
		"click #decreaseLabelHeight": function() {
			this.changeLabelHeightByFactor(-2);
		},
		
		"click #toggleXAxis": function() {
			this.toggleXAxis();
		},
		
		"click #toggleUnavail": function() {
			this.toggleUnavail();
		},
		
		"click #buildingRestrictToFilter": function() {
			this.onFilterAllocatedBuildings();
		},

		"click #selectEventImg": function() {
			this.popUpEventsPanel();
		}
	},
	
    // ---------------------------------------------------------------------------------------------
    // BEGIN view initialize Section 
    // ---------------------------------------------------------------------------------------------
	/**
	 * Create restrictions for building-floor two level tree.
	 */
	afterViewLoad: function() {
	},
	
    afterCreate: function() {
        this.on('app:space:planing:console:refreshActionStates', this.initialSpaceRequirementActions);
    },

	/**
	 * Hide locate button if there are no lat and lon for buildings. 
	 */
	afterInitialDataFetch: function() {
		this.stackController = View.controllers.get('abAllocWizStackController');
		//this.setAsOfDate();
		
		this.bindEventsForAsOfDateInput();
				
		//create our own caledar select value button.
		//Set the initial value and convert it.
		this.setCalendarPicForInput();
		this.stackController.updateAsOfDate(this.stackController.asOfDate);
		//this.setInitialValueForDateInput();
		
		this.setTooltipForTitleBarImgButtons();

		this.initialSpaceRequirementActions();
	},
	
	/**
	 * Set proper tooltip for image buttons of stack panel and support localization.
	 */
	setTooltipForTitleBarImgButtons: function() {
		$('increaseFloorHeightImg').title = getMessage('inFlHeight');
		$('decreaseFloorHeightImg').title = getMessage('deFlHeight');
		$('increaseFloorLengthImg').title = getMessage('inFlLen');
		$('decreaseFloorLengthImg').title = getMessage('deFlLen');
		$('increaseLabelHeightImg').title = getMessage('inLabelHeight');
		$('decreaseLabelHeightImg').title = getMessage('deLabelHeight');
		$('switchOrientationImg').title = getMessage('swOri');
		$('toggleXAxisImg').title = getMessage('toggleXAxis');	
		$('toggleUnavailImg').title = getMessage('toggleUnavail');	
	},

	initialSpaceRequirementActions: function() {
		// disable 'Space Requirement' related actions when current scenario is not 'Function Group' level.
		if (this.stackController.scnLevel != 'fg') {
			jQuery("#syncSpaceRequirement").addClass("x-hide-display");
			jQuery("#editSpaceRequirement").addClass("x-hide-display");
			jQuery("#addSpaceRequirement").removeClass("x-hide-display");
		}  else {
			jQuery("#syncSpaceRequirement").removeClass("x-hide-display");
			jQuery("#editSpaceRequirement").removeClass("x-hide-display");
			jQuery("#addSpaceRequirement").addClass("x-hide-display");
		}
	},

	/**
	 * Bind Enter, text changed and lose-focus events for the as of date input text.
	 */
	bindEventsForAsOfDateInput: function() {
		//Bind text changed event.
		var thisController = this;
		jQuery('#asOfDateInput').on('blur', function() {  
			var newDate = jQuery("#asOfDateInput").val();
			if(newDate.length == 8 || newDate.length == 9 || newDate.length == 10) {
				thisController.addNewSpaceForm.setFieldValue('gp.date_start', newDate);
				var newIsoDate = thisController.addNewSpaceForm.getFieldValue('gp.date_start');
				if (newIsoDate != thisController.stackController.asOfDate) {
					thisController.stackController.asOfDateChanged();
				}
			}
		});
		
		jQuery("input[readOnly]").keydown(function(e) {
			e.preventDefault();
		});
	},
	
	/**
	 * Since the title bar is not a standard title bar.we need to define input in the title bar.
	 * Set the calendar for our own use.
	 */
	setCalendarPicForInput: function() {
		//to show the calendar icon correctly
		jQuery("#asOfDateInput").mouseenter(function(){
			jQuery("#asOfDateCalendarImg").css("visibility", "visible");
		});
		
		jQuery("#asOfDateInput").mouseleave(function(){
			jQuery("#asOfDateCalendarImg").css("visibility", "hidden");
		});
		
		//Convert the value of the date when user input
		jQuery("#asOfDateInput").change(function(){
			validationAndConvertionDateInput(this, 'asOfDateInput', null, 'false', true, true); 
			if (window.temp!=this.value) afm_form_values_changed=true;
		});
		
		jQuery("#asOfDateInput").blur(function(){
			validationAndConvertionDateInput(this, 'asOfDateInput', null, 'false', true, true); 
			if (window.temp!=this.value) afm_form_values_changed=true;
		});
		
		jQuery("#asOfDateInput").focus(function(){
			window.temp=this.value;
		});
		
		//Avoid the flicker of the img event
		jQuery("#asOfDateCalendarImg").mouseover(function(){
			jQuery("#asOfDateCalendarImg").css("visibility", "visible");
		});
		
		jQuery("#asOfDateCalendarImg").mouseleave(function(){
			jQuery("#asOfDateCalendarImg").css("visibility", "hidden");
		});
		
		jQuery("#asOfDateCalendarImg").click(function(event){
			Calendar.getControllerForEvent(event, 'asOfDateInput','/archibus/schema/ab-system/graphics'); 
			return false;
		});
	},

	setInitialValueForDateInput: function() {
		this.addNewSpaceForm.setFieldValue('gp.date_start', this.stackController.asOfDate);
		var localizedValue = this.addNewSpaceForm.getFieldValue('gp.date_start');
		var localizedValueArray = localizedValue.split("-");
		var dateString = FormattingDate(localizedValueArray[2], localizedValueArray[1], localizedValueArray[0], strDateShortPattern);
		jQuery("#asOfDateInput").val(dateString);
		this.addNewSpaceForm.setFieldValue("gp.date_start","");
	},

    // ---------------------------------------------------------------------------------------------
    // BEGIN handlers for mapped dom element's  events 
    // ---------------------------------------------------------------------------------------------
	changeFloorHeightByFactor: function(addFactor) {
		var newFloorHeight = this.stackController.stackControl.config.displayFloorHeight + addFactor;
		this.stackController.stackControl.config.displayFloorHeight = newFloorHeight;
		try {
			this.stackController.stackControl.build();
		}catch(e){
			//keep work when error happens.
		}
	},
	
	changeFloorLength: function(scale) {
		var current = this.stackController.stackControl.config.horizontalScale;
		if(!current) {
			current = 1;
		}
		var newCurrent = (1+scale)*current;
		this.stackController.stackControl.config.horizontalScale = newCurrent;
		try {
			this.stackController.stackControl.build();
		}catch(e){
			//keep work when error happens.
		}
	},
	
	changeLabelHeightByFactor: function(addFactor) {
		var originalHeight = 	 this.stackController.stackControl.stackControl.labelLargeTextHeight;
		if (!originalHeight)	{
			 originalHeight	 = 10;
		}
		var newHeight = originalHeight + addFactor;
		if (newHeight<=0) {
			View.alert(getMessage('negativeLabelHeight'));
			return;
		}

		this.stackController.stackControl.stackControl.labelLargeTextHeight = newHeight;
		try {
			this.stackController.stackControl.build();
		}catch(e){
			//keep work when error happens.
		}
	},
	
	toggleXAxis: function() {
		this.stackController.stackControl.config.showXAxis = !this.stackController.stackControl.config.showXAxis;
		try {
			this.stackController.stackControl.build();
		}catch(e){
			//keep work when error happens.
		}
	},

	toggleUnavailableAreas: true, 
	toggleUnavailableRestriction: null, 
	toggleUnavail: function() {
		this.toggleUnavailableRestriction = null; 
		if ( this.toggleUnavailableAreas ) {
			this.toggleUnavailableAreas = false;

        	this.toggleUnavailableRestriction = new Ab.view.Restriction();
        	this.toggleUnavailableRestriction.addClause('gp.allocation_type', 'Unavailable - Vertical Penetration Area', '!=', 'AND'); 
        	this.toggleUnavailableRestriction.addClause('gp.allocation_type', 'Unavailable - Service Area', '!=', 'AND');
        	this.toggleUnavailableRestriction.addClause('gp.allocation_type', 'Unavailable - Remaining Area', '!=', 'AND');
		}
		else {
			this.toggleUnavailableAreas = true;
		}
		this.stackController.stackControl.refresh(this.toggleUnavailableRestriction);
	},
	
	/**
	 * Change the stack orientation.
	 */
	stackChartOrientationChanged: function() {
		if (this.currentStackOrientation == "VERTICAL") {
			this.currentStackOrientation = "HORIZONTAL";
		} else {
			this.currentStackOrientation = "VERTICAL";
		}
		this.stackController.stackControl.config.stackOrientation = this.currentStackOrientation;
		try {
			this.stackController.stackControl.build();
		}catch(e){
			//keep work when error happens.
		}
	},
		
	addSpaceRequirement: function() {
		var me = this;
		View.openDialog('ab-alloc-sp-req-to-gp-move.axvw', null, true, {
			width: 1024,
			height: 800,
			scenarioId: me.stackController.scnId, 
			scenarioName: me.stackController.scnName, 
			sbLevel: me.stackController.scnLevel, 
			asOfDate: me.stackController.asOfDate, 
			unitTitle: me.stackController.unitTitle, 
			callback: function(dateStart){
				if (dateStart) {
					me.stackController.asOfDate = dateStart;
					//me.setInitialValueForDateInput();
				}
				me.trigger('app:space:planing:console:updateAsOfDate',me.stackController.asOfDate, true);
				me.stackBuildingTreePanel.refresh();
				//me.refreshStackChartWithNewDate();
				me.eventsTreePanel.refresh();
				me.eventsTreePopPanel.refresh();
			}
		});		
	},
	
	/**
	 * When the clicked stack element is a group associated by sb_items record, then swtich to the Space Requirement tab to show linked sb_items of clicked group.
	 */
	editSpaceRequirement: function() {
		if ( this.stackController.currentGpHasSbItems ) {
			var sbCtrl = this.getSpaceRquirementTabController();
			var tabs = View.parentTab.parentPanel;
			if (sbCtrl){
				sbCtrl.sbName = this.stackController.scnName;
				sbCtrl.gpId = this.stackController.editGroupId;
				sbCtrl.refreshGrid();
				tabs.selectTab("allocWizSb");
				tabs.showTab("allocWizSb");
			} 
			else {
				tabs.scnName = this.stackController.scnName;
				tabs.associatedGpId = this.stackController.editGroupId;
				tabs.selectTab("allocWizSb");
				tabs.showTab("allocWizSb");
			}
		}
	},
	
	getSpaceRquirementTabController: function() {
		var allocWizCtrl = View.getOpenerView().controllers.get('allocWiz');
		var tabs = allocWizCtrl.allocWizTabs;
		var tab = tabs.findTab("allocWizSb");
		var sbCtrl=null;
		if(tab.hasView() && tab.isContentLoaded) {
			if(!tab.isContentLoading) {
				var iframe = tab.getContentFrame();
				var childView = iframe.View;
				if (valueExists(childView)) {
					sbCtrl = childView.controllers.get("abAllocDefSpReqEditCtrl");
				}
			}
		}
		return sbCtrl;
	},

	syncSpaceRequirement: function() {
		if(this.stackController.scnLevel == 'fg') {
			var me = this;
			View.openDialog('ab-alloc-sync-req-to-gp.axvw', null, true, {
				scenarioId: me.stackController.scnId, 
				scenarioName: me.stackController.scnName, 
				unitTitle: me.stackController.unitTitle, 
				width: 1024,
				height: 800,
				callback: function(dateStart){
					if (dateStart) {
						me.stackController.stackBuildingTreePanel.refresh();
						me.stackController.eventsTreePanel.refresh();
						me.stackController.eventsTreePopPanel.refresh();
						me.stackController.asOfDate = dateStart;
						//me.setInitialValueForDateInput();
						me.stackController.trigger('app:space:planing:console:updateAsOfDate',dateStart, true);
						//me.refreshStackChartWithNewDate();
					}
					else {
						var tabs = View.parentTab.parentPanel;
						tabs.selectTab("allocWizSb");
						tabs.showTab("allocWizSb",true);
					}
				}
			});	
		}
	},
	
	/**
	 * Markup action.
	 */
	addFloorTask: function() {
		var me = this;
		View.openDialog('ab-sp-pfolio-mark-act-item.axvw', null, true, {
			width: 1024,
			height: 800,
			scenarioId: me.stackController.scnId, 
			scenarioName: me.stackController.scnName, 
			blId: me.stackController.showBuildingId, 
			flId: me.stackController.showFloorId, 
			callback: function(){
			}
		});	
	},
	
	/**
	 * Open the pop-up window for showing floor plan.
	 */
	showFloorPlan: function() {
		var thisController = View.controllers.get('abAllocWizStackController');
		if(thisController.showBuildingId &&  thisController.showFloorId) {
			var restriction = new Ab.view.Restriction();
			restriction.addClause("rm.bl_id", thisController.showBuildingId, '=');
			restriction.addClause("rm.fl_id", thisController.showFloorId, '=');
			var dwgs = thisController.floorPlanRoomDataSource.getRecords(restriction);
			if (!dwgs || dwgs.length==0){
				View.alert(getMessage('noFloorDrawing'));
			} 
			else { 
				var floorPlanPopup = View.openDialog('ab-sp-pfolio-svg-floor-plan.axvw', null, true, {
					width: 900,
					height: 600,
					title: getMessage("showFloorPlan")+" - " + thisController.showBuildingId + "-" + thisController.showFloorId,
					buildingId: thisController.showBuildingId,
					floorId: thisController.showFloorId
				});
			}
		}
	},
	
	/**
	 * Edit allocations for allocated record or unavailable record.
	 */
	editAllocation: function() {
		//kb#3049867: alert user if select no group
		if (!this.stackController.editGroupId)	{
			View.alert(getMessage('noSelectGp'));
			return;
		}

		if (!this.stackController.checkIfCanEditAllocation(this.stackController.editGroupId)) {
			View.alert(getMessage('cannotEditThisAllocation'));
		} else {
			this.editGroup();
		}
	},
		
	/**
	 * Edit allocation for the existed allocation record.
	 */
	editGroup: function() {
		//We need to compare the asOfDate and the start date of the selected group.
		var restriction = new Ab.view.Restriction();
		restriction.addClause('gp.gp_id', this.stackController.editGroupId, '=');
		var record = this.groupRecordsDataSource.getRecords(restriction)[0];
		if ( record ){
			var allocationType = record.getValue('gp.allocation_type');
			if (allocationType == 'Allocated Area') {
				this.openEditAllocatedPopupWindow(record);
			} else {
				this.openEditUnavailablePopupWindow(record);
			}
		}
	},
	
	openEditAllocatedPopupWindow: function(record) {
		var startDate = record.getValue('gp.date_start');
		if(startDate) {
			var isoStartDate = getIsoFormatDate(startDate);
			if (isoStartDate != this.stackController.asOfDate) {
				var editGroupParentMenu = Ext.get('editGroup');
				this.showSubMenuItems('GROUP-EDIT', editGroupParentMenu, isoStartDate);
			} else {
				this.openEditGroupPopupWindow(false, this.stackController.asOfDate, 'EDIT-GROUP-FROM-STARTDATE');
			}
		} else {
			this.openEditGroupPopupWindow(false, this.stackController.asOfDate, 'EDIT-GROUP-FROM-STARTDATE');
		}
	},
	
	openEditUnavailablePopupWindow: function(record) {
		var startDate = record.getValue('gp.date_start');
		if(startDate) {
			var isoStartDate = getIsoFormatDate(startDate);
			if (isoStartDate != this.stackController.asOfDate) {
				var editGroupParentMenu = Ext.get('editGroup');
				this.showSubMenuItems('GROUP-EDIT', editGroupParentMenu, isoStartDate);
			} else {
				this.openUnavailableSpaceForm(getMessage('editUnavailableAllocation'), 'EDIT-EXISTING');
			}
		} else {
			this.openUnavailableSpaceForm(getMessage('editUnavailableAllocation'), 'EDIT-EXISTING');
		}
	},
	
	/**
	 * Open the edit group pop up window. 
	 * @param newRecord if the pop up window is new record mode.
	 */
	openEditGroupPopupWindow: function(newRecord, newDate, subType) {
		var me = this;
		var thisController = View.controllers.get('abAllocWizStackController');
		
		var spaceGroupDialog = View.openDialog('ab-alloc-wiz-stack-space-form.axvw', null, newRecord, {
			width: 900,
			height: 900,
			title: getMessage("editSpaceTitle"),
			closeButton: false,
			
			afterViewLoad : function(dialogView) {
				var dialogController = dialogView.controllers.get('abAllocWizStackSpaceFormController');
				dialogController.asOfDate = newDate;
				dialogController.scnId = thisController.scnId;
				dialogController.scnLevel = thisController.scnLevel;
				dialogController.type = 'EDIT';
				dialogController.subType = subType;
				dialogController.editGroupId = thisController.editGroupId;
				dialogController.callback = me.commonGroupCallback;
				dialogController.lastStoredEventNameForAllocating = thisController.lastStoredEventNameForAllocating;
			}
		});
	},
	
	/**
	 * The callback for the edit group pop up window.
	 */
	editGroupCallback: function(dateStart, eventName) {
		var thisController = View.controllers.get('abAllocWizStackController');
		thisController.lastStoredEventNameForAllocating = eventName;
		if (thisController.asOfDate != newAsOfDate) {
			thisController.asOfDate = newAsOfDate;
			//thisController.setInitialValueForDateInput();
		}
		thisController.eventsTreePanel.refresh();
		thisController.eventsTreePopPanel.refresh();
		thisController.trigger('app:space:planing:console:updateAsOfDate',thisController.asOfDate, true);
		//thisController.refreshStackChartWithNewDate();
	},
	
	/**
	 * As for the add new button of Allocations group.
	 */
	showAddNewAllocationSubMenuItems: function() {
		var addNewParentMenu = Ext.get('addNewTypeOfAllocation');
		this.showSubMenuItems('OPEN-GROUP-ADD-BUTTONS', addNewParentMenu, null);
	},
	
	/**
	 * Dynamically add sub items for the parentMenu.
	 */
	showSubMenuItems: function(funGroup, parentMenu, startDate) {
		var subMenuItems = [];
		if (funGroup == 'GROUP-EDIT') {
			var menuTexts = [];
			menuTexts.push(getMessage('createNewGroupFromAsOfDate'));
			menuTexts.push(getMessage('editThisGroupFromStartDate'));
			for (var i = 0; i < menuTexts.length; i++) {
				var subMenuItem = new Ext.menu.Item({text: menuTexts[i],
            		handler: this.editGroupSubItemsHandler.createDelegate(this, [i], startDate)});
				subMenuItems.push(subMenuItem);
			}
		} else if (funGroup == 'GROUP-SPLIT'){
			var menuTexts = [];
			menuTexts.push(getMessage('splitGroupFromAsOfDate'));
			menuTexts.push(getMessage('splitGroupFromStartDate'));
			for (var i = 0; i < menuTexts.length; i++) {
				var subMenuItem = new Ext.menu.Item({text: menuTexts[i],
            		handler: this.splitGroupSubItemsHandler.createDelegate(this, [i], startDate)});
				subMenuItems.push(subMenuItem);
			}
		} else if(funGroup == 'OPEN-GROUP-ADD-BUTTONS') {
			var menuTexts = [];
			menuTexts.push(getMessage('addNewAllocation'));
			menuTexts.push(getMessage('addNewUnavailableArea'));
			for (var i = 0; i < menuTexts.length; i++) {
				var subMenuItem = new Ext.menu.Item({text: menuTexts[i],
            		handler: this.addNewAllocationSubItemsHandler.createDelegate(this, [i])});
				subMenuItems.push(subMenuItem);
			}
		}
		var menu = new Ext.menu.Menu({items: subMenuItems});
		menu.show(parentMenu, 'tl-bl?');
	},
	
	addNewAllocationSubItemsHandler: function(index) {
		switch(index) {
			case 0: {
				//open the add new allocation form.
				this.allocateSpace(getMessage('addAllocTitle'), 'ADD-NEW');
				break;
			}
			case 1: {
				//open the mark space as unavailable space form.
				this.openUnavailableSpaceForm(getMessage('addUnavailableTitle'), 'ADD-NEW');
				break;
			}
		}
	},
	
	editGroupSubItemsHandler: function(index, startDate){
		var restriction = new Ab.view.Restriction();
		restriction.addClause('gp.gp_id', this.stackController.editGroupId, '=');
		var record = this.groupRecordsDataSource.getRecords(restriction)[0];
		var allocationType = record.getValue('gp.allocation_type');
		switch(index) {
			case 0:{
				//create new group from as of date
				if (allocationType == 'Allocated Area') {
					this.openEditGroupPopupWindow(false, this.stackController.asOfDate, 'EDIT-GROUP-FROM-ASOFDATE');
				} else {
					this.openUnavailableSpaceForm(getMessage('editUnavailableAllocation'), 'EDIT-GROUP-FROM-ASOFDATE');
				}
				break;
			}
			case 1: {
				if (allocationType == 'Allocated Area') {
					this.openEditGroupPopupWindow(false, startDate, 'EDIT-GROUP-FROM-STARTDATE');
				} else {
					this.openUnavailableSpaceForm(getMessage('editUnavailableAllocation'), 'EDIT-GROUP-FROM-STARTDATE');
				}
				break;
			}
		}
	},
	
	/**
	 * Split an existing group.
	 */
	splitGroup: function() {
		//kb#3049867: alert user if select no group
		if (!this.stackController.editGroupId)	{
			View.alert(getMessage('noSelectGp'));
			return;
		}
		
		if(!this.stackController.checkIfCanEditAllocation(this.stackController.editGroupId)) {
			View.alert(getMessage('cannotEditThisAllocation'));
		} else {
			var restriction = new Ab.view.Restriction();
			restriction.addClause('gp.gp_id', this.stackController.editGroupId, '=');
			var record = this.groupRecordsDataSource.getRecords(restriction)[0];
			var startDate = record.getValue('gp.date_start');
			if(startDate) {
				var isoStartDate = getIsoFormatDate(startDate);
				if (isoStartDate != this.stackController.asOfDate) {
					//give sub menu items
					var splitGroupParentMenu = Ext.get('splitGroup');
					this.showSubMenuItems('GROUP-SPLIT', splitGroupParentMenu, isoStartDate);
				} else {
					this.openSplitGroupPopupWindow(false, this.stackController.asOfDate, 'SPLIT-GROUP-FROM-STARTDATE');
				}
			} else {
				this.openSplitGroupPopupWindow(false, this.stackController.asOfDate, 'SPLIT-GROUP-FROM-STARTDATE');
			}
		}
	},
	
	/**
	 * Open the split group pop up window.
	 */
	openSplitGroupPopupWindow: function(newRecord, newDate, subType){
		var me = this;
		var thisController = this.stackController;
		var spaceGroupDialog = View.openDialog('ab-alloc-wiz-stack-space-form.axvw', newRecord, true, {
			width: 900,
			height: 950,
			title: getMessage("splitTitle"),
			closeButton: false,
			
			afterViewLoad : function(dialogView) {
				var dialogController = dialogView.controllers.get('abAllocWizStackSpaceFormController');
				dialogController.asOfDate = newDate;
				dialogController.scnId = thisController.scnId;
				dialogController.scnLevel = thisController.scnLevel;
				dialogController.type = 'SPLIT';
				dialogController.subType = subType;
				dialogController.editGroupId = thisController.editGroupId;
				dialogController.callback = me.commonGroupCallback;
				dialogController.lastStoredEventNameForAllocating = thisController.recentSelectedEventName;
			}
		});
	},
	
	/**
	 * Sub menu item for split group.
	 */
	splitGroupSubItemsHandler: function(index, startDate) {
		switch(index) {
			case 0:{
				//create new group from as of date
				this.openSplitGroupPopupWindow(false, this.stackController.asOfDate, 'SPLIT-GROUP-FROM-ASOFDATE');
				break;
			}
			case 1: {
				this.openSplitGroupPopupWindow(false, startDate, 'SPLIT-GROUP-FROM-STARTDATE');
				break;
			}
		}
	},
	
	/**
	 * Allocate space to generate a new group.
	 */
	allocateSpace: function(title, subtype) {
		var me = this;
		var thisController = View.controllers.get('abAllocWizStackController');
		var newRecord = true;
		if (subtype == 'EDIT-EXISTING') {
			newRecord = false;
		}
		var spaceGroupDialog = View.openDialog('ab-alloc-wiz-stack-space-form.axvw', null, newRecord, {
			width: 800,
			height: 2000,
			title: title,
			closeButton: false, 
			
			afterViewLoad : function(dialogView) {
				var dialogController = dialogView.controllers.get('abAllocWizStackSpaceFormController');
				dialogController.asOfDate = thisController.asOfDate;
				dialogController.scnId = thisController.scnId;
				dialogController.scnLevel = thisController.scnLevel;
				dialogController.type = 'ALLOCATE';
				dialogController.subType = subtype;
				dialogController.editGroupId = thisController.editGroupId;
				dialogController.callback = me.commonGroupCallback;
				dialogController.lastStoredEventNameForAllocating = thisController.lastStoredEventNameForAllocating;
				dialogController.showBuildingId = thisController.showBuildingId;
				dialogController.showFloorId = thisController.showFloorId;
			}
		});
	},
	
	/**
	 * The common call back for all the group pop-up window.
	 */
	commonGroupCallback: function(newAsOfDate, eventName) {
		var thisController = View.controllers.get('abAllocWizStackController');
		if(eventName) {
			thisController.lastStoredEventNameForAllocating = eventName;
		}
		if(newAsOfDate) {
			if (thisController.asOfDate != newAsOfDate) {
				thisController.asOfDate = newAsOfDate;
				//thisController.setInitialValueForDateInput();
			}
		}
		thisController.trigger('app:space:planing:console:updateAsOfDate',thisController.asOfDate, true);
		//thisController.refreshStackChartWithNewDate();
		thisController.eventsTreePanel.refresh();
		thisController.eventsTreePopPanel.refresh();
	},
	
	/**
	 * Open the form for unavailable space.
	 */
	openUnavailableSpaceForm: function(title, subtype) {
		var me = this;
		var thisController = View.controllers.get('abAllocWizStackController');
		var newRecord = false;
		if (this.subType == 'ADD-NEW') {
			newRecord = true;
		}
		var spaceGroupDialog = View.openDialog('ab-alloc-wiz-stack-space-form.axvw', null, newRecord, {
			width: 900,
			height: 950,
			title: title,
			closeButton: false,
			
			afterViewLoad : function(dialogView) {
				var dialogController = dialogView.controllers.get('abAllocWizStackSpaceFormController');
				dialogController.asOfDate = thisController.asOfDate;
				dialogController.scnId = thisController.scnId;
				dialogController.scnLevel = thisController.scnLevel;
				dialogController.editGroupId = thisController.editGroupId;
				dialogController.type = 'UNAVAILABLE';
				dialogController.subType = subtype;
				dialogController.callback = me.commonGroupCallback;
				dialogController.lastStoredEventNameForAllocating = thisController.lastStoredEventNameForAllocating;
				dialogController.showBuildingId = thisController.showBuildingId;
				dialogController.showFloorId = thisController.showFloorId;
			}
		});
	},
	
	/**
	 * Configure the statistics of stack's building and floor.
	 */
	openStatisticsConfig: function() {
		var me = this;
		var statisticsConfigDialog = View.openDialog('ab-alloc-wiz-stack-statis-config.axvw', null, true, {
			width: 1024,
			height: 160,
			closeButton: false,
			stackControlBlStatisFields: me.stackControlBlStatisFields,
			stackControlFlStatisFields:me.stackControlFlStatisFields,
			callback: function(blStatisFields, flStatisFields){
				me.stackControlBlStatisFields = blStatisFields;
				me.stackControlFlStatisFields = flStatisFields;
				View.closeDialog();
				me.stackController.refreshStackChartWithNewDate();
			}
		});
	},

	popUpEventsPanel: function() {
		var layoutManager = View.getLayoutManager('mainLayout');
		if (layoutManager.isRegionCollapsed('north')) {
			this.eventsTreePopPanel.showInWindow({
				x: 100, 
				y: 100,
				width: 300,
				height: 300,
				closeButton: true,
				closable: false
			});
		}
		else {
			View.alert(getMessage("eventPopUp"));
		}
	},
    // ---------------------------------------------------------------------------------------------
    // END handlers for mapped dom element's  events  Section
    // ---------------------------------------------------------------------------------------------

	performopenNegativeAllocation: function(negativeGp, toBlId, toFlId){
		var destGp = this.searchDestinationGp(negativeGp, toBlId, toFlId);
		if (destGp){
			this.openNegativeForm(negativeGp, destGp);
		}
	},

	searchDestinationGp: function(negativeGp, toBlId, toFlId){
		var destGpDs = View.dataSources.get('abAllocWizStackDs_destGp'); 
		destGpDs.addParameter('scenarioId', this.stackController.scnId);
		destGpDs.addParameter('asOfDate', this.stackController.asOfDate);
		destGpDs.addParameter('blId', toBlId);
		destGpDs.addParameter('flId', toFlId);
		destGpDs.addParameter("negativeGpId", negativeGp.getValue('gp.gp_id'));
		destGpDs.addParameter("sameOrgCondition", this.getSameOrgConditionByScnLevel());

		var records = destGpDs.getRecords();
		if (records && records.length>0){
			return records[0];
		}
		return null;
	},

	getSameOrgConditionByScnLevel: function(){
		var conditionSql = "1=0 ";
		if ("bu"==this.stackController.scnLevel){
			conditionSql = "( g1.planning_bu_id is null and gp.planning_bu_id is null or g1.planning_bu_id is not null and gp.planning_bu_id is not null  and g1.planning_bu_id= gp.planning_bu_id) "	
		}  
		else if ("dv"==this.stackController.scnLevel) {
			conditionSql = "( g1.dv_id is null and gp.dv_id is null or g1.dv_id is not null and gp.dv_id is not null  and g1.dv_id= gp.dv_id) "	
		} 
		else if ("dp"==this.stackController.scnLevel ){
			conditionSql = "( g1.dv_id is not null and gp.dv_id is not null  and g1.dv_id= gp.dv_id)  and ( g1.dp_id is null and gp.dp_id is null or g1.dp_id is not null and gp.dp_id is not null  and g1.dp_id= gp.dp_id) "	
		}
		return conditionSql;
	},

	openNegativeForm: function(negativeGp, destGp){
		var me = this;
		View.openDialog('ab-alloc-sp-negative-gp.axvw', null, true, {
			width: 1024,
			height: 800,
			scenarioId: me.stackController.scnId, 
			negativeGpId: negativeGp.getValue('gp.gp_id'), 
			gpId: destGp.getValue('gp.gp_id'), 
			asOfDate: me.stackController.asOfDate, 
			callback: function(dateStart){
				View.closeDialog();
				me.stackController.refreshStackChartWithNewDate();
				me.stackController.eventsTreePanel.refresh();
				me.stackController.eventsTreePopPanel.refresh();
			}
		});		
	}
});