/**
 * Controller for Space Report Scenario Grid Panel.
 */
View.createController('spaceScenarioGridPanelController', {
	
	/**
	 * Constructor.
	 */
	afterCreate : function() {
		this.on('app:space:portfolio:report:filter', this.filterScenarioGrid);
	},

	// ----------------------- Action event handler--------------------
 	afterInitialDataFetch : function() {
		if ( $('spRptFilter_filterToProjects') && $('spRptFilter_filterToProjects').checked ) {
			this.filterScenarioGrid();
		}
	},

	/**
	 * Filter Scenario Grid.
	 */
	filterScenarioGrid : function() {
		var scenarioRes = View.controllers.get('spaceReportFilter').scenariosRes;
		this.scenarioGrid.refresh(scenarioRes)
	},

	/**
	 * Filter Scenario Grid.
	 */
	filterByProjects : function() {
		if ( $('spRptFilter_filterToProjects') && $('spRptFilter_filterToProjects').checked ) {
			var projectsRes = View.controllers.get('spaceReportFilter').getProjectRestriction();
			this.scenarioGrid.refresh(projectsRes)
		}
	}
});

function onclickScenario() {
	var grid = View.panels.get('scenarioGrid');
	var selectedRow = grid.rows[grid.selectedRowIndex];
	var scn_id = selectedRow['portfolio_scenario.portfolio_scenario_id'];
	var indexOfQuote = scn_id.indexOf("'");
	var scnId = scn_id;
	if ( indexOfQuote != -1 ){
		scnId	 = scn_id.replace(/\'/g, "''");
	}

	var res = "portfolio_scenario.portfolio_scenario_id ='"+scnId+"'"
	// trigger filter event
	View.controllers.get('spaceScenarioGridPanelController').trigger('app:space:portfolio:report:onclickScenarioLink', res);
}
