var isLoaded = false;
View.createController('mapCtrl', {
    mapControl: null,
    geocodeControl: null,
    mapControlInit: false,
    layerMenu: null,
    menuParent: null,
    menu: [],
    subMenu: [],
    items: [],
    assetType: '',
    markerType: '',
    leaseId: '',
    records: null,
    markerProperty: null,
    highlightColumns: {
        'option_suite_manual_area': 'total_suite_manual_area',
        'option_suite_usable_area': 'total_suite_usable_area',
        'option_others_manual_area': 'manual_area_used_by_others',
        'option_others_usable_area': 'usable_area_used_by_others',
        'option_purchasing_cost': 'purchasing_cost',
        'option_market_value': 'value_market',
        'option_book_value': 'value_book',
        'option_manual_area': 'area_manual',
        'option_cad_area': 'area_cad'
    },
    highlightByOptions: [],
    highlightSelected: 'option_none',
    markers: {
        'BUILDING': {
            dataSource: 'dsBuilding',
            keyFields: ['bl.bl_id'],
            geometryFields: ['bl.lon', 'bl.lat'],
            titleField: 'bl.name',
            contentFields: ['bl.bl_id', 'bl.pr_id', 'bl.name', 'bl.address', 'bl.total_suite_manual_area', 'bl.total_suite_usable_area', 'bl.manual_area_used_by_others', 'bl.usable_area_used_by_others', 'bl.leases_number', 'bl.purchasing_cost', 'bl.value_book', 'bl.value_market'],
            markerProperties: {}
        },
        'LAND': {
            dataSource: 'dsProperty',
            keyFields: ['property.pr_id'],
            geometryFields: ['property.lon', 'property.lat'],
            titleField: 'property.name',
            contentFields: ['property.pr_id', 'property.name', 'property.address', 'property.area_manual', 'property.area_cad', 'property.leases_number', 'property.purchasing_cost', 'property.value_book', 'property.value_market'],
            markerProperties: {}
        },
        'STRUCTURE': {
            dataSource: 'dsProperty',
            keyFields: ['property.pr_id'],
            geometryFields: ['property.lon', 'property.lat'],
            titleField: 'property.name',
            contentFields: ['property.pr_id', 'property.name', 'property.address', 'property.area_manual', 'property.area_cad', 'property.leases_number', 'property.purchasing_cost', 'property.value_book', 'property.value_market'],
            markerProperties: {}
        },
        'LEASE': {
            dataSource: 'dsLease',
            keyFields: ['ls.ls_id'],
            geometryFields: ['ls.lon', 'ls.lat'],
            titleField: 'ls.ls_id',
            contentFields: ['ls.ls_id', 'ls.estimated_area', 'ls.landlord_tenant', 'ls.item_status', 'ls.status'],
            markerProperties: {}
        }
    },
    afterInitialDataFetch: function () {
        //set area titles with UOM
        var uomTitle = "";
        if (View.activityParameters["AbCommonResources-ConvertAreasLengthsToUserUnits"] == 1) {
            uomTitle = " " + View.user.areaUnits.title;
        }
        this.dsBuilding.fieldDefs.get("bl.total_suite_manual_area").title = getMessage("total_suite_manual_area_title") + uomTitle;
        this.dsBuilding.fieldDefs.get("bl.total_suite_usable_area").title = getMessage("total_suite_usable_area_title") + uomTitle;
        this.dsBuilding.fieldDefs.get("bl.manual_area_used_by_others").title = getMessage("manual_area_used_by_others_title") + uomTitle;
        this.dsBuilding.fieldDefs.get("bl.usable_area_used_by_others").title = getMessage("usable_area_used_by_others_title") + uomTitle;
        this.dsLease.fieldDefs.get("ls.estimated_area").title = getMessage("estimated_area_title") + uomTitle;
        //KB3036049 - show selected items in the maximized view
        if (this.view.parameters) {
            if (this.view.parameters.maximize) {
                var openerView = this.view.getOpenerView();
                if (openerView) {
                    var treeController = openerView.controllers.get('bldgTree');
                    var items = [];
                    if (treeController) {
                        items = treeController.selectedItems;
                        if (valueExistsNotEmpty(items)) {
                            this.showSelectedBuildings(items, treeController.showNotGeocodedMessage);
                        }
                    } else {
                        treeController = openerView.controllers.get('structTree');
                        if (treeController) {
                            items = treeController.selectedItems;
                            if (valueExistsNotEmpty(items)) {
                                this.showSelectedStructures(items, treeController.showNotGeocodedMessage);
                            }
                        } else {
                            treeController = openerView.controllers.get('landTree');
                            if (treeController) {
                                items = treeController.selectedItems;
                                if (valueExistsNotEmpty(items)) {
                                    this.showSelectedLands(items, treeController.showNotGeocodedMessage);
                                }
                            }
                        }
                    }
                }
            }
        }
    },
    init: function () {
        if (!this.mapControlInit) {
            var configObject = new Ab.view.ConfigObject();
            configObject.mapImplementation = 'Esri';
            this.mapControl = new Ab.leaflet.Map('htmlMap', 'objMap', configObject);
            //create geocoder
            this.geocodeControl = new Ab.leaflet.EsriGeocoder(this.mapControl);
            this.mapControlInit = true;
            if (!this.mapControl.mapInited) {
                setTimeout(this.configMap(), 500);
            } else {
                this.configMap();
            }
        }
    },
    configMap: function () {
        // basemap layer menu
        var basemapLayerMenu = this.htmlMap.actions.get('basemapLayerMenu');
        basemapLayerMenu.clear();
        var basemapLayers = this.mapControl.getBasemapLayerList();
        for (var i = 0; i < basemapLayers.length; i++) {
            basemapLayerMenu.addAction(i, basemapLayers[i], this.switchBasemapLayer);
        }
        this.setHighlightLabel();
        this.loadMenu();
        //hide dashboard maximize button
        var maximizeButton = this.htmlMap.actions.get('htmlMap_showAsDialog');
        if (valueExists(maximizeButton)) {
            maximizeButton.forceHidden(true);
        }
        isLoaded = true;
    },
    /**
     * initialize label for highlight radio buttons
     */
    setHighlightLabel: function () {
        var radioObj = document.getElementsByName('radioHighlight');
        for (var i = 0; i < radioObj.length; i++) {
            var object = radioObj[i];
            var value = object.value;
            $('span_' + value).innerHTML = getMessage(value);
        }
        this.enableLegend();
    },
    enableLegend: function () {
        this.htmlMap.enableAction('legend', 'option_none' !== this.highlightSelected);
    },
    setAssetType: function (assetType) {
        this.assetType = assetType;
    },
    showMenu: function (e, item) {
        var menuItems = [];
        var typeItem = 0;
        for (var i = 0; i < this.menu.length; i++) {
            var subMenuItems = [];
            for (var j = 0; j < this.subMenu[i].length; j++) {
                var subMenuItem = new Ext.menu.Item({
                    text: getMessage('submenu_' + this.subMenu[i][j]),
                    handler: reports.createDelegate(this, [typeItem], this.items)
                });
                subMenuItems.push(subMenuItem);
                typeItem = typeItem + 1;
            }
            var menuItem = null;
            if (subMenuItems.length > 0) {
                menuItem = new Ext.menu.Item({
                    text: getMessage('menu_' + this.menu[i]),
                    menu: {items: subMenuItems}
                });
            } else {
                menuItem = new Ext.menu.Item({
                    text: getMessage('menu_' + this.menu[i]),
                    handler: reports.createDelegate(this, [typeItem], this.items)
                });
                typeItem = typeItem + 1;
            }
            menuItems.push(menuItem);
        }
        var menu = new Ext.menu.Menu({items: menuItems});
        menu.show(this.menuParent, 'tl-bl?');
    },
    loadMenu: function () {
        this.menuParent = Ext.get('reports');
        this.menuParent.on('click', this.showMenu, this, null);
    },
    switchMapLayer: function (item) {
        View.controllers.get('mapCtrl').mapControl.switchMapLayer(item.text);
    },
    switchBasemapLayer: function (item) {
        View.controllers.get('mapCtrl').mapControl.switchBasemapLayer(item.text);
    },
    showMarkerLegend: function () {
        View.controllers.get('mapCtrl').mapControl.showMarkerLegend();
    },
    switchReferenceLayer: function (item) {
        View.controllers.get('mapCtrl').mapControl.switchReferenceLayer(item.text);
    },
    /**
     * Show selected buildings on map.
     * @param items  - selected building id's
     * @param notGeoCoded
     */
    showSelectedBuildings: function (items, notGeoCoded) {
        if (notGeoCoded) {
            View.showMessage(getMessage('not_geocoded'));
        }
        this.items = items;
        //this.assetType = 'BUILDING';
        this.highlightByOptions = ['option_suite_manual_area', 'option_suite_usable_area', 'option_others_manual_area', 'option_others_usable_area', 'option_purchasing_cost', 'option_market_value', 'option_book_value', 'option_none'];
        this.highlightSelected = 'option_none';
        this.records = this.dsBuilding.getRecords('bl.bl_id IN (' + items.toString() + ')');
        this.showMarkers();
    },
    /**
     * Show selected lands on map.
     * @param {Array} items - selected land id's
     * @param notGeoCoded
     */
    showSelectedLands: function (items, notGeoCoded) {
        if (notGeoCoded) {
            View.showMessage(getMessage('not_geocoded'));
        }
        this.items = items;
        //this.assetType = 'LAND';
        this.highlightByOptions = ['option_manual_area', 'option_cad_area', 'option_purchasing_cost', 'option_market_value', 'option_book_value', 'option_none'];
        this.highlightSelected = 'option_none';
        this.records = this.dsProperty.getRecords('property.pr_id IN (' + items.toString() + ')');
        this.showMarkers();
    },
    /**
     * Show selected lands on map.
     * @param {Array} items - selected structures id's
     * @param notGeoCoded
     */
    showSelectedStructures: function (items, notGeoCoded) {
        if (notGeoCoded) {
            View.showMessage(getMessage('not_geocoded'));
        }
        this.items = items;
        //this.assetType = 'STRUCTURE';
        this.highlightByOptions = ['option_manual_area', 'option_cad_area', 'option_purchasing_cost', 'option_market_value', 'option_book_value', 'option_none'];
        this.highlightSelected = 'option_none';
        this.records = this.dsProperty.getRecords('property.pr_id IN (' + items.toString() + ')');
        this.showMarkers();
    },
    showSelectedLease: function (items, leaseId, markerType, notGeoCoded) {
        if (notGeoCoded) {
            View.showMessage(getMessage('not_geocoded'));
        }
        this.items = items;
        //this.assetType = 'LEASE';
        this.leaseId = leaseId;
        this.markerType = markerType;
        this.dsLease.addParameter('item', leaseId);
        this.dsLease.addParameter('parent', items[0]);
        this.dsLease.addParameter('status_owned', getMessage('status_owned'));
        this.dsLease.addParameter('status_leased', getMessage('status_leased'));
        this.dsLease.addParameter('status_neither', getMessage('status_neither'));
        this.dsLease.addParameter('status_active', getMessage('status_active'));
        this.dsLease.addParameter('status_inactive', getMessage('status_inactive'));
        this.records = this.dsLease.getRecords();
        this.showMarkers();
    },
    showMarkers: function () {
        if (!isLoaded) {
            setTimeout(this.showMarkers(), 500);
            return;
        }
        if ('LEASE' === this.assetType) {
            this.createMarkers(this.items, this.assetType);
        } else {
            this.htmlMap.actions.get('highlight').enable(true);
            this.htmlMap.actions.get('reports').enable(true);
            this.createMarkers(this.items, this.assetType);
        }
    },
    /**
     * show markers on map in standard mode (without highlight)
     */
    createMarkers: function (markersId, assetType) {
        this.mapControl.clearMarkers();
        var markerProperties = {
            useClusters: true
        };
        if ('LEASE' !== assetType) {
            _.extend(markerProperties, {
                markerActionTitle: getMessage('showDetailsMarkerActionTitle'),
                markerActionCallback: this.onClickMarker
            })
        }
        var marker = _.clone(this.markers[assetType]);
        marker.markerProperties = markerProperties;
        if ('BUILDING' === assetType) {
            var restriction = 'bl.bl_id IN (' + markersId.toString() + ')';
            this.mapControl.createMarkers(
                marker.dataSource,
                marker.keyFields,
                marker.geometryFields,
                marker.titleField,
                marker.contentFields,
                marker.markerProperties
            );
            this.mapControl.showMarkers('dsBuilding', restriction);
        } else if ('LAND' === assetType || 'STRUCTURE' === assetType) {
            var restriction = 'property.pr_id IN (' + markersId.toString() + ')';
            this.mapControl.createMarkers(
                marker.dataSource,
                marker.keyFields,
                marker.geometryFields,
                marker.titleField,
                marker.contentFields,
                marker.markerProperties
            );
            this.mapControl.showMarkers('dsProperty', restriction);
        } else if ('LEASE' === assetType) {
            this.dsLease.addParameter('item', this.leaseId);
            this.dsLease.addParameter('parent', this.items[0]);
            this.dsLease.addParameter('status_owned', getMessage('status_owned'));
            this.dsLease.addParameter('status_leased', getMessage('status_leased'));
            this.dsLease.addParameter('status_neither', getMessage('status_neither'));
            this.dsLease.addParameter('status_active', getMessage('status_active'));
            this.dsLease.addParameter('status_inactive', getMessage('status_inactive'));
            this.mapControl.createMarkers(
                marker.dataSource,
                marker.keyFields,
                marker.geometryFields,
                marker.titleField,
                marker.contentFields,
                marker.markerProperties
            );
            this.mapControl.showMarkers('dsLease');
        }
    },
    htmlHighlight_onApply: function () {
        this.highlightSelected = this.htmlHighlight.getFieldValue('radioHighlight');
        this.htmlHighlight.closeWindow();
        if ('option_none' === this.highlightSelected) {
            this.markerProperty = null;
            this.createMarkers(this.items, this.assetType);
        } else {
            this.showHighlight();
        }
        this.enableLegend();
    },
    geoCodeItem: function (assetType, assetCode, callbackFn) {
        this.mapControl.clearMarkers();
        var marker = _.clone(this.markers[assetType]);
        if ('BUILDING' === assetType) {
            marker.dataSource = 'dsGeoBuilding';
            marker.titleField = 'bl.bl_id';
            marker.contentFields = ['bl.address1', 'bl.city_id', 'bl.state_id', 'bl.ctry_id'];
            this.createMarker(marker);
            var restriction = new Ab.view.Restriction();
            restriction.addClause('bl.bl_id', assetCode, "=");
            this.geocodeControl.geocode(
                marker.dataSource,
                restriction,
                'bl',
                marker.keyFields[0],
                marker.geometryFields,
                marker.contentFields,
                true
            );
        } else if ('LAND' === assetType) {
            marker.dataSource = 'dsGeoProperty';
            marker.titleField = 'property.pr_id';
            marker.contentFields = ['property.address1', 'property.city_id', 'property.state_id', 'property.zip', 'property.ctry_id'];
            this.createMarker(marker);
            var restriction = new Ab.view.Restriction();
            restriction.addClause('property.pr_id', assetCode, "=");
            this.geocodeControl.geocode(
                marker.dataSource,
                restriction,
                'property',
                marker.keyFields[0],
                marker.geometryFields,
                marker.contentFields,
                true
            );
        }
        if (callbackFn) {
            callbackFn();
        }
    },
    createMarker: function (marker) {
        this.mapControl.createMarkers(
            marker.dataSource,
            marker.keyFields,
            marker.geometryFields,
            marker.titleField,
            marker.contentFields,
            marker.markerProperties);
    },
    /**
     * initialize highlight radio buttons
     * based on user selection
     */
    setHighlightPanel: function (context) {
        if (this.items.length == 0) {
            this.htmlHighlight.show(false, true);
            this.htmlHighlight.closeWindow();
            View.showMessage(getMessage('error_noselection'));
            return;
        }
        View.panels.get('htmlHighlight').showInWindow({
            title: getMessage('highlightByPanelTitle'),
            anchor: context.el.dom,
            width: 400,
            height: 300,
            maximizable: false,
            modal: true
        });
        this.restoreHighlightPanel(this.highlightByOptions, this.highlightSelected);
    },
    /**
     * restore last selection for highlight radio buttons
     * @param {Object} options
     * @param {Object} selected
     */
    restoreHighlightPanel: function (options, selected) {
        var radioHighlightCell = this.htmlHighlight.getFieldCell('radioHighlight');
        jQuery(radioHighlightCell).find('[type=radio]').each(function (index) {
            var object = this;
            if (options.indexOf(object.value) == -1) {
                object.disabled = true;
            }
            if (object.value == selected) {
                object.checked = true;
            }
        });
    },
    /**
     * show markers on map in higlight mode
     */
    showHighlight: function () {
        // remove thematic legend and info window is exist
        this.mapControl.clearMarkers();
        var selected = this.highlightSelected;
        var assetType = this.assetType;
        var table = '';
        var dataSource = '';
        var field = this.highlightColumns[selected];
        var restriction = '';
        if ('BUILDING' === assetType) {
            table = 'bl';
            dataSource = 'dsBuilding';
            restriction = 'bl.bl_id IN (' + this.items.toString() + ')';
            field = table + '.' + field;
        } else {
            table = 'property';
            dataSource = 'dsProperty';
            restriction = 'property.pr_id IN (' + this.items.toString() + ')';
            field = table + '.' + field;
        }
        var markerProperties = {
            renderer: 'thematic-class-breaks',
            thematicField: field,
            thematicClassBreaks: this.getThematicClassBreaks(field),
            useClusters: true
        };
        var marker = this.markers[assetType];
        marker.markerProperties = markerProperties;
        this.mapControl.createMarkers(
            marker.dataSource,
            marker.keyFields,
            marker.geometryFields,
            marker.titleField,
            marker.contentFields,
            marker.markerProperties
        );
        this.mapControl.showMarkers(dataSource, restriction);
        this.mapControl.showMarkerLegend();
    },
    getThematicClassBreaks: function (field) {
        var thematicClassBreaks = [];
        var records = this.records;
        var minVal = Number.MAX_VALUE;
        var maxVal = (-1) * Number.MAX_VALUE;
        for (var i = 0; i < records.length; i++) {
            var record = records[i];
            var value = parseFloat(record.getValue(field));
            minVal = Math.min(minVal, value);
            maxVal = Math.max(maxVal, value);
        }
        if (minVal != maxVal) {
            for (var i = 0; i < 4; i++) {
                var val = Number(minVal + ((maxVal - minVal) / 5) * (i + 1));
                thematicClassBreaks[i] = parseFloat(val.toFixed(1).toString());
            }
        } else {
            thematicClassBreaks[0] = parseFloat(minVal.toString());
        }
        return thematicClassBreaks;
    },
    /**
     * onClickMarker
     * open a report for selected item
     */
    onClickMarker: function (title) {
        var selected_item = title;
        var assetType = View.controllers.get('mapCtrl').assetType;
        if ('BUILDING' === assetType) {
            View.openDialog('ab-rplm-pfadmin-leases-and-suites-by-building-base-report.axvw', null, true, {
                width: 1280,
                height: 600,
                closeButton: true,
                afterInitialDataFetch: function (dialogView) {
                    var dialogController = dialogView.controllers.get('repLeaseSuitesByBldgBase');
                    dialogController.bl_id = selected_item;
                    dialogController.initializeView();
                }
            });
        } else if ('LAND' === assetType) {
            View.openDialog('ab-rplm-pfadmin-leases-by-land-base-report.axvw', null, true, {
                width: 1280,
                height: 600,
                closeButton: true,
                afterInitialDataFetch: function (dialogView) {
                    var dialogController = dialogView.controllers.get('repLeaseByLandBase');
                    dialogController.pr_id = selected_item;
                    dialogController.initializeView();
                }
            });
        } else if ('STRUCTURE' === assetType) {
            View.openDialog('ab-rplm-pfadmin-leases-by-structure-base-report.axvw', null, true, {
                width: 1280,
                height: 600,
                closeButton: true,
                afterInitialDataFetch: function (dialogView) {
                    var dialogController = dialogView.controllers.get('repLeaseByStructureBase');
                    dialogController.pr_id = selected_item;
                    dialogController.initializeView();
                }
            });
        }
    }
});