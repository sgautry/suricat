var abRplmPfadminGpdAreaByBluseCtrl = View.createController('abRplmPfadminGpdAreaByBluseCtrl', {
	
	objFilter: null,
	
	afterViewLoad: function(){
		this.abRplmPfadminGpdAreaByBluse_chart.controllerId = 'abRplmPfadminGpdAreaByBluseCtrl';
	},
	
	afterInitialDataFetch: function(){
		// try to get restriction object from current view or from opener
		if (View.restriction != null){
			this.objFilter = View.restriction;
		} else if (View.getOpenerView().restriction != null){
			this.objFilter = View.getOpenerView().restriction;
		}
		
		var instructionLabel = '';
		// try to get instructionLabel object from filter controller
		if (View.controllers.get('ctrlGpdFilter') != null){
			instructionLabel = View.controllers.get('ctrlGpdFilter').instructionLabel;
		} else if (View.getOpenerView().controllers.get('ctrlGpdFilter') != null){
			instructionLabel = View.getOpenerView().controllers.get('ctrlGpdFilter').instructionLabel;
		}
		
		// display filter restriction as an instruction for maximized view
		if(this.view.parameters){
			if(this.view.parameters.maximize){
				this.abRplmPfadminGpdAreaByBluse_chart.setInstructions(instructionLabel);
			}
		}
		
		var restriction = this.getSqlRestriction(this.objFilter);
		this.abRplmPfadminGpdAreaByBluse_chart.addParameter("filterRestriction", restriction);
		this.abRplmPfadminGpdAreaByBluseDetails.addParameter("filterRestriction", restriction);
		
		this.refreshChart();
		
		//set chart title
		if(View.activityParameters["AbCommonResources-ConvertAreasLengthsToUserUnits"]==1){
    		this.abRplmPfadminGpdAreaByBluse_chart.setTitle(getMessage("chart_title").replace("{0}"," ("+View.user.areaUnits.title+")"));
    	}else{
    		this.abRplmPfadminGpdAreaByBluse_chart.setTitle(getMessage("chart_title").replace("{0}",""));
    	}
	},

	/**
	 * Rebuilds the chart, adds grouping and data axis, sets titles for axis, defines on click event
	 */
	refreshChart: function(){
		var chartPanel = this.abRplmPfadminGpdAreaByBluse_chart;
		var chartConfig = this.getChartConfig();
		var restriction = this.getSqlRestriction(this.objFilter);
		var chartData = this.getChartData(restriction);
		
		chartConfig.addGroupingAxis('bl.use1', '');
		chartConfig.addDataAxis('pie', 'bl.sum_area_gross_int', '');
		chartConfig.addValueAxis(0, '');
		
		chartPanel.show(true, true);
		
		chartPanel.chartControl = new ChartControl(chartPanel.parentElementId, chartConfig);
		
		if (isViewMaximized(this.view)) {
			chartConfig.showLegendOnLoad = true;
			chartConfig.showExportButton = true;
			chartPanel.chartControl.chart.canvas.radius= '40%';
		} else{
			chartConfig.showLegendOnLoad = false;
			chartConfig.showExportButton = false;
			chartPanel.chartControl.chart.canvas.labelRadius = '0.5';
			chartPanel.chartControl.chart.canvas.radius= '25%';	
			chartPanel.chartControl.chart.canvas.fontSize='9';
		}
		
		chartPanel.chartControl.chart.canvas.startAngle= '180';
		chartPanel.chartControl.chart.canvas.labelText = "[[value]]";
		
		chartPanel.chartControl.controllerId = 'abRplmPfadminGpdAreaByBluseCtrl';
		chartPanel.chartControl.setData(chartData);	
		chartPanel.chartControl.addEventListener('clickSlice', onSliceClick);
	},
	
	/**
	 * Defines chart configuration
	 */
	getChartConfig: function(){
		var config = new ChartConfig();
		config.chartType = 'pieChart';
		config.legendLocation = 'bottom';
		config.showExportButton = true;
		config.showOnLoad = true;
		config.showLegendOnLoad = true;
		config.showDataTips = true;
		config.showLabels = true;
		config.showUnitPrefixes = true;
		config.showUnitSuffixes = true;
		config.zoomable = false;
		return config;
	},
	
	/**
	 * Chart data needed for rebuilding the chart
	 */
	getChartData: function(restriction){
		var data = new Array();
		var dsGroupingData = this.abRplmPfadminGpdAreaByBluse_ds;
		
		dsGroupingData.addParameter('filterRestriction', restriction);

		var recordsGrouping = dsGroupingData.getRecords();
		for (var i = 0; i < recordsGrouping.length; i++) {
			var recordData = recordsGrouping[i];
			if (valueExists(recordData)) {
				var groupingValue = recordData.getValue('bl.use1');
				var dataValues = recordData.getValue('bl.sum_area_gross_int');
				
				var objData = {
					'bl.use1' : groupingValue,
					'bl.sum_area_gross_int' : dataValues
				};
				data.push(objData);
			}
		}
		return data;
	},
	
	getSqlRestriction: function( objFilter ){
		var result = "";
		if (objFilter != null) {
			if (valueExists(objFilter.bu_id)) {
				// is organization
				if(valueExistsNotEmpty(objFilter.dp_id)){
					result += "AND rm.dp_id = '" + objFilter.dp_id + "' ";
				}
				if(valueExistsNotEmpty(objFilter.dv_id)){
					result += "AND rm.dv_id = '" + objFilter.dv_id + "' ";
				}
				if(valueExistsNotEmpty(objFilter.bu_id) && result.length == 0 ){
					result += "AND EXISTS(SELECT dv.dv_id FROM dv WHERE dv.dv_id = rm.dv_id AND dv.bu_id = '" + objFilter.bu_id + "')";
				}
				if (result.length > 0 ) {
					result = "AND EXISTS(SELECT rm.bl_id FROM rm WHERE rm.bl_id = bl.bl_id " + result + ") ";
				}
			}else {
				// is location
				if (valueExistsNotEmpty(objFilter.site_id)) {
					result += "AND bl.site_id = '"+ objFilter.site_id +"' ";
				}
				if (valueExistsNotEmpty(objFilter.ctry_id)) {
					result += "AND bl.ctry_id = '"+ objFilter.ctry_id +"' ";
				}
				if (valueExistsNotEmpty(objFilter.geo_region_id) && result.length == 0 ) {
					result += "AND EXISTS(SELECT ctry.ctry_id FROM ctry WHERE ctry.ctry_id = bl.ctry_id AND ctry.geo_region_id = '" + objFilter.geo_region_id + "') ";
				}
			}
			
			if (valueExistsNotEmpty(objFilter.use1)) {
				result += "AND bl.use1 = '" + objFilter.use1 + "' ";
			}
			
			if (result.length == 0) {
				result = " 1 = 1 ";
			}else {
				if (result.indexOf("AND") == 0) {
					result = result.slice(3);
				}
			}
			
		} else {
			result = " 1 = 1 ";
		}
		return result;
	},
	
	/**
	 * Called from function onSliceClick defined in ab-rplm-pfadmin-gpd-custom-charts.js, to open details panel
	 */
	onClickItemData: function(selectedChartData){
		var restriction = new Ab.view.Restriction();
		
		var detailsPanel = View.panels.get('abRplmPfadminGpdAreaByBluseDetails');
			
		restriction.addClause('bl.use1', selectedChartData['bl.use1']);
		detailsPanel.refresh(restriction);
			
		detailsPanel.show(true);
		detailsPanel.showInWindow({
			    width: 600,
			    height: 400,
			    closeButton: true
		});
	}
	
});