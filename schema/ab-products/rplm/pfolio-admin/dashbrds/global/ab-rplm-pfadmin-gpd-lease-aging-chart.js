var abRplmPfadminGpdLeaseAgingCtrl = View.createController('abRplmPfadminGpdLeaseAgingCtrl', {
	objFilter: null,
	
	afterViewLoad: function(){
		this.abRplmPfadminGpdLeaseAging_chart.controllerId = 'abRplmPfadminGpdLeaseAgingCtrl';
	},
	
	afterInitialDataFetch: function(){
		// try to get restriction object from current view or from opener
		if (View.restriction != null){
			this.objFilter = View.restriction;
		} else if (View.getOpenerView().restriction != null){
			this.objFilter = View.getOpenerView().restriction;
		}
		
		var instructionLabel = '';
		// try to get instructionLabel object from filter controller
		if (View.controllers.get('ctrlGpdFilter') != null){
			instructionLabel = View.controllers.get('ctrlGpdFilter').instructionLabel;
		} else if (View.getOpenerView().controllers.get('ctrlGpdFilter') != null){
			instructionLabel = View.getOpenerView().controllers.get('ctrlGpdFilter').instructionLabel;
		}
		
		// display filter restriction as an instruction for maximized view
		if(this.view.parameters){
			if(this.view.parameters.maximize){
				this.abRplmPfadminGpdLeaseAging_chart.setInstructions(instructionLabel);
			}
		}
		
		var restriction = this.getSqlRestriction(this.objFilter);
		this.abRplmPfadminGpdLeaseAging_chart.addParameter("labelGreaterThan", getMessage("labelGreaterThan").replace("*", ""));
		this.abRplmPfadminGpdLeaseAging_chart.addParameter("filterRestriction", restriction);
		this.abRplmPfadminGpdLeaseAgingDetails.addParameter("labelGreaterThan", getMessage("labelGreaterThan").replace("*", ""));
		this.abRplmPfadminGpdLeaseAgingDetails.addParameter("filterRestriction", restriction);
		this.refreshChart();
	},
	
	
	/**
	 * Rebuilds the chart, adds grouping and data axis, sets titles for axis, defines on click event
	 */
	refreshChart: function(){
		var titleDataAxis = getMessage("dataAxisTitle");
		
		var chartPanel = this.abRplmPfadminGpdLeaseAging_chart;
		var chartConfig = this.getChartConfig();
		var restriction = this.getSqlRestriction(this.objFilter);
		
		chartConfig.addGroupingAxis('bl.vf_age', '');
		chartConfig.addDataAxis('column', 'bl.leases', titleDataAxis);
		chartConfig.addValueAxis(0, '');
		
		chartPanel.show(true, true);
		
		if (isViewMaximized(this.view)) {
			chartConfig.showLegendOnLoad = true;
			chartConfig.showLabels = true;
			chartConfig.showExportButton = true;
		} else{
			chartConfig.showLegendOnLoad = false;
			chartConfig.showLabels = false;
			chartConfig.showExportButton = false;
		}
		
		chartConfig.dataAxis[0].showLabel = true;
		chartConfig.groupingAxis[0].showLabel = true;
		
		chartPanel.chartControl = new ChartControl(chartPanel.parentElementId, chartConfig);
		chartPanel.chartControl.controllerId = 'abRplmPfadminGpdLeaseAgingCtrl';
		
		var data = chartPanel.getDataFromDataSources();
		chartPanel.chartControl.setData(data);
		
		chartPanel.chartControl.addEventListener('clickGraphItem', onGraphClick);
	},
	
	/**
	 * Defines chart configuration
	 */
	getChartConfig: function(){
		var config = new ChartConfig();
		config.chartType = 'columnChart';
		config.legendLocation = 'bottom';
		config.showExportButton = true;
		config.showOnLoad = true;
		config.showLegendOnLoad = true;
		config.showDataTips = true;
		config.showLabels = true;
		config.showUnitPrefixes = true;
		config.showUnitSuffixes = true;
		config.zoomable = false;
		return config;
	},
	
	/**
	 * Called from function onGraphClick defined in ab-rplm-pfadmin-gpd-custom-charts.js, to open details panel
	 */
	onClickItemData: function(selectedChartData){
		var restriction = new Ab.view.Restriction();
		
		var detailsPanel = View.panels.get('abRplmPfadminGpdLeaseAgingDetails');
			
		restriction.addClause('bl.vf_age', selectedChartData['bl.vf_age']);
		detailsPanel.refresh(restriction);
			
		detailsPanel.show(true);
		detailsPanel.showInWindow({
			    width: 600,
			    height: 400,
			    closeButton: true
		});
	},
	
	getSqlRestriction: function( objFilter ){
		var result = "";
		if (objFilter != null) {
			if (valueExists(objFilter.bu_id)) {
				// is organization
				if(valueExistsNotEmpty(objFilter.dp_id)){
					result += "AND rm.dp_id = '" + objFilter.dp_id + "' ";
				}
				if(valueExistsNotEmpty(objFilter.dv_id)){
					result += "AND rm.dv_id = '" + objFilter.dv_id + "' ";
				}
				if(valueExistsNotEmpty(objFilter.bu_id) && result.length == 0 ){
					result += "AND EXISTS(SELECT dv.dv_id FROM dv WHERE dv.dv_id = rm.dv_id AND dv.bu_id = '" + objFilter.bu_id + "')";
				}
				if (result.length > 0 ) {
					result = "AND EXISTS(SELECT rm.bl_id FROM rm WHERE rm.bl_id = bl.bl_id " + result + ") ";
				}
			}else {
				// is location
				if (valueExistsNotEmpty(objFilter.site_id)) {
					result += "AND bl.site_id = '"+ objFilter.site_id +"' ";
				}
				if (valueExistsNotEmpty(objFilter.ctry_id)) {
					result += "AND bl.ctry_id = '"+ objFilter.ctry_id +"' ";
				}
				if (valueExistsNotEmpty(objFilter.geo_region_id) && result.length == 0 ) {
					result += "AND EXISTS(SELECT ctry.ctry_id FROM ctry WHERE ctry.ctry_id = bl.ctry_id AND ctry.geo_region_id = '" + objFilter.geo_region_id + "') ";
				}
			}
			
			if (valueExistsNotEmpty(objFilter.use1)) {
				result += "AND bl.use1 = '" + objFilter.use1 + "' ";
			}
			
			if (result.length == 0) {
				result = " 1 = 1 ";
			}else {
				if (result.indexOf("AND") == 0) {
					result = result.slice(3);
				}
			}
			
		} else {
			result = " 1 = 1 ";
		}
		return result;
	}

});
