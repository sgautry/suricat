var abCostWizTreeCtrl = View.createController('abCostWizTreeCtrl', {
	// wizard mode propeties or account
	wizardMode: null,
	
	wizardController: null,
	
	afterViewLoad: function() {
		// get data from main view
		var openerView = this.view.getOpenerView();
		if (openerView && openerView.controllers.get('abRepmCostWizardController')) {
			this.wizardController = openerView.controllers.get('abRepmCostWizardController');
			this.wizardMode = this.wizardController.wizardMode;
			this.wizardController.treeController =  this;
		}
	},
	
	afterInitialDataFetch: function(){
		if (this.wizardMode == 'properties') {
			translatePropertiesTree();
		}
	},
	
	refreshTree: function(filterValues){
		if (this.wizardMode == 'account') {
			this.refreshAccountTree(filterValues);
		} else if (this.wizardMode == 'properties') {
			this.refreshPropertiesTree(filterValues);
		}
	},
	
	refreshPropertiesTree: function(filterValues) {

		var blGeoRestriction = createGeographicalRestrictionForTable(filterValues, "bl", ["ctry_id", "regn_id", "state_id", "city_id", "site_id", "pr_id"]);
		var prGeoRestriction = createGeographicalRestrictionForTable(filterValues, "property", ["ctry_id", "regn_id", "state_id", "city_id", "site_id"]);
		
		//add lease restriction if is defined
		var isSearchForBl = valueExistsNotEmpty(filterValues['bl_id']);
		var isSearchForPr = valueExistsNotEmpty(filterValues['pr_id']);
		var isSearchForLs = valueExistsNotEmpty(filterValues['ls_id']);
		var isSearchForParcel = valueExistsNotEmpty(filterValues['parcel_id']);

		var blRestriction = "1=2";
		if (valueExistsNotEmpty(filterValues['bl_id'])) {
			var value = filterValues['bl_id'];
			if (isArray(value)) {
				blRestriction = "bl.bl_id IN ('" + makeSafeSqlValue(value).join("','") + "') "; 
			} else {
				blRestriction = "bl.bl_id = '" + makeSafeSqlValue(value) + "'"; 
			}
		}

		var prRestriction = "1=2";
		if (valueExistsNotEmpty(filterValues['pr_id'])) {
			var value = filterValues['pr_id'];
			if (isArray(value)) {
				prRestriction = "property.pr_id IN ('" + makeSafeSqlValue(value).join("','") + "') "; 
			} else {
				prRestriction = "property.pr_id = '" + makeSafeSqlValue(value) + "'"; 
			}
		}

		var leaseRestriction = "1=2";
		if (valueExistsNotEmpty(filterValues['ls_id'])) {
			var value = filterValues['ls_id'];
			if (isArray(value)) {
				leaseRestriction = "ls.ls_id IN ('" + makeSafeSqlValue(value).join("','") + "') "; 
			} else {
				leaseRestriction = "ls.ls_id = '" + makeSafeSqlValue(value) + "'"; 
			}
		}
		
		var parcelRestriction = "1=2";
		if (valueExistsNotEmpty(filterValues['parcel_id'])) {
			var value = filterValues['parcel_id'];
			if (isArray(value)) {
				parcelRestriction = "parcel.parcel_id IN ('" + makeSafeSqlValue(value).join("','") + "') "; 
			} else {
				parcelRestriction = "parcel.parcel_id = '" + makeSafeSqlValue(value) + "'"; 
			}
		}
		
		var restrictionCtry = "";
		var restrictionCity = "";
		var restrictionPropertyPr = "";
		var restrictionPropertyBl = "";
		
		if (isSearchForBl) {
			restrictionCtry += (restrictionCtry.length > 0?" OR ": "") + "EXISTS(SELECT bl.bl_id FROM bl WHERE ctry.ctry_id = bl.ctry_id AND {restriction_geo_bl} AND {restriction_bl}) ";
			restrictionCity += (restrictionCity.length > 0?" OR ": "") + "EXISTS(SELECT bl.bl_id FROM bl WHERE city.city_id = bl.city_id AND city.state_id = bl.state_id AND {restriction_geo_bl} AND {restriction_bl}) ";
			restrictionPropertyBl = " {restriction_bl} ";
			restrictionPropertyPr = " EXISTS(SELECT bl.bl_id FROM bl WHERE property.pr_id = bl.pr_id AND {restriction_geo_bl} AND {restriction_bl})";
		}
		
		if (isSearchForPr) {
			restrictionCtry += (restrictionCtry.length > 0?" OR ": "") + "EXISTS(SELECT property.pr_id FROM property WHERE ctry.ctry_id = property.ctry_id AND {restriction_geo_pr} AND {restriction_pr}) ";
			restrictionCity += (restrictionCity.length > 0?" OR ": "") + "EXISTS(SELECT property.pr_id FROM property WHERE city.city_id = property.city_id AND city.state_id = property.state_id AND {restriction_geo_pr} AND {restriction_pr}) ";
			restrictionPropertyPr += (restrictionCity.length > 0?" OR ": "") + " {restriction_pr} ";
		}
		
		if (isSearchForLs) {
			restrictionCtry += (restrictionCtry.length > 0?" OR ": "") + "EXISTS(SELECT ls.ls_id FROM ls WHERE ls.use_as_template = 0 AND {restriction_lease} AND EXISTS(SELECT bl.bl_id FROM bl WHERE ls.bl_id = bl.bl_id AND ctry.ctry_id = bl.ctry_id AND {restriction_geo_bl})) " +
				" OR EXISTS(SELECT ls.ls_id FROM ls WHERE ls.use_as_template = 0 AND {restriction_lease} AND ls.bl_id IS NULL AND EXISTS(SELECT property.pr_id FROM property WHERE ls.pr_id = property.pr_id AND ctry.ctry_id = property.ctry_id AND {restriction_geo_pr})) ";
			restrictionCity += (restrictionCity.length > 0?" OR ": "") + "EXISTS(SELECT ls.ls_id FROM ls WHERE ls.use_as_template = 0 AND {restriction_lease} AND EXISTS(SELECT bl.bl_id FROM bl WHERE ls.bl_id = bl.bl_id AND city.city_id = bl.city_id AND city.state_id = bl.state_id AND {restriction_geo_bl})) " +
				" OR EXISTS(SELECT ls.ls_id FROM ls WHERE ls.use_as_template = 0 AND {restriction_lease} AND ls.bl_id IS NULL AND EXISTS(SELECT property.pr_id FROM property WHERE ls.pr_id = property.pr_id AND city.city_id = property.city_id AND city.state_id = property.state_id AND {restriction_geo_pr})) ";
			restrictionPropertyBl += (restrictionPropertyBl.length > 0?" OR ": "") + "EXISTS(SELECT ls.ls_id FROM ls WHERE ls.use_as_template = 0 AND {restriction_lease} AND EXISTS(SELECT bl.bl_id FROM bl WHERE ls.bl_id = bl.bl_id AND {restriction_geo_bl})) ";
			restrictionPropertyPr += (restrictionPropertyPr.length > 0?" OR ": "") + "EXISTS(SELECT ls.ls_id FROM ls WHERE ls.use_as_template = 0 AND {restriction_lease} AND ls.bl_id IS NULL AND EXISTS(SELECT property.pr_id FROM property WHERE ls.pr_id = property.pr_id AND {restriction_geo_pr}))" +
					"OR EXISTS(SELECT ls.ls_id FROM ls WHERE ls.use_as_template = 0 AND {restriction_lease} AND EXISTS(SELECT bl.bl_id FROM bl WHERE ls.bl_id = bl.bl_id AND {restriction_geo_bl})) ";
		}
		
		if (isSearchForParcel) {
			restrictionCtry += (restrictionCtry.length > 0?" OR ": "") + "EXISTS(SELECT parcel.parcel_id FROM parcel WHERE {restriction_parcel} AND EXISTS(SELECT property.pr_id FROM property WHERE parcel.pr_id = property.pr_id AND ctry.ctry_id = property.ctry_id AND {restriction_geo_pr})) ";
			restrictionCity += (restrictionCity.length > 0?" OR ": "") + "EXISTS(SELECT parcel.parcel_id FROM parcel WHERE {restriction_parcel} AND EXISTS(SELECT property.pr_id FROM property WHERE parcel.pr_id = property.pr_id AND city.city_id = property.city_id AND city.state_id = property.state_id AND {restriction_geo_pr})) ";
			restrictionPropertyPr += (restrictionPropertyPr.length > 0?" OR ": "") + "EXISTS(SELECT parcel.parcel_id FROM parcel WHERE {restriction_parcel} AND EXISTS(SELECT property.pr_id FROM property WHERE parcel.pr_id = property.pr_id AND {restriction_geo_pr})) ";
		}
		
		if (!isSearchForBl && !isSearchForPr && !isSearchForLs && !isSearchForParcel) {
			restrictionCtry += (restrictionCtry.length > 0?" OR ": "") + "EXISTS(SELECT bl.bl_id FROM bl WHERE ctry.ctry_id = bl.ctry_id AND {restriction_geo_bl} AND {restriction_bl}) ";
			restrictionCity += (restrictionCity.length > 0?" OR ": "") + "EXISTS(SELECT bl.bl_id FROM bl WHERE city.city_id = bl.city_id AND city.state_id = bl.state_id AND {restriction_geo_bl} AND {restriction_bl}) ";
			restrictionCtry += (restrictionCtry.length > 0?" OR ": "") + "EXISTS(SELECT property.pr_id FROM property WHERE ctry.ctry_id = property.ctry_id AND {restriction_geo_pr} AND {restriction_pr}) ";
			restrictionCity += (restrictionCity.length > 0?" OR ": "") + "EXISTS(SELECT property.pr_id FROM property WHERE city.city_id = property.city_id AND city.state_id = property.state_id AND {restriction_geo_pr} AND {restriction_pr}) ";
			restrictionCtry += (restrictionCtry.length > 0?" OR ": "") + "EXISTS(SELECT ls.ls_id FROM ls WHERE ls.use_as_template = 0 AND {restriction_lease} AND EXISTS(SELECT bl.bl_id FROM bl WHERE ls.bl_id = bl.bl_id AND ctry.ctry_id = bl.ctry_id AND {restriction_geo_bl})) " +
				" OR EXISTS(SELECT ls.ls_id FROM ls WHERE ls.use_as_template = 0 AND {restriction_lease} AND ls.bl_id IS NULL AND EXISTS(SELECT property.pr_id FROM property WHERE ls.pr_id = property.pr_id AND ctry.ctry_id = property.ctry_id AND {restriction_geo_pr})) ";
			restrictionCity += (restrictionCity.length > 0?" OR ": "") + "EXISTS(SELECT ls.ls_id FROM ls WHERE ls.use_as_template = 0 AND {restriction_lease} AND EXISTS(SELECT bl.bl_id FROM bl WHERE ls.bl_id = bl.bl_id AND city.city_id = bl.city_id AND city.state_id = bl.state_id AND {restriction_geo_bl})) " +
				" OR EXISTS(SELECT ls.ls_id FROM ls WHERE ls.use_as_template = 0 AND {restriction_lease} AND ls.bl_id IS NULL AND EXISTS(SELECT property.pr_id FROM property WHERE ls.pr_id = property.pr_id AND city.city_id = property.city_id AND city.state_id = property.state_id AND {restriction_geo_pr})) ";

			restrictionCtry += (restrictionCtry.length > 0?" OR ": "") + "EXISTS(SELECT parcel.parcel_id FROM parcel WHERE {restriction_parcel} AND EXISTS(SELECT property.pr_id FROM property WHERE parcel.pr_id = property.pr_id AND ctry.ctry_id = property.ctry_id AND {restriction_geo_pr})) ";
			restrictionCity += (restrictionCity.length > 0?" OR ": "") + "EXISTS(SELECT parcel.parcel_id FROM parcel WHERE {restriction_parcel} AND EXISTS(SELECT property.pr_id FROM property WHERE parcel.pr_id = property.pr_id AND city.city_id = property.city_id AND city.state_id = property.state_id AND {restriction_geo_pr})) ";
			
			restrictionPropertyBl = "1=1";
			restrictionPropertyPr = "1=1";
			
			blRestriction = "1=1";
			prRestriction = "1=1";
			leaseRestriction = "1=1";
			parcelRestriction = "1=1";
			
		}
		
		if (restrictionPropertyBl.length == 0) {
			restrictionPropertyBl = "1=2";
		}
		if (restrictionPropertyPr.length == 0) {
			restrictionPropertyPr = "1=2";
		}
		
		restrictionCtry = "(" + restrictionCtry + ")"; 
		restrictionCity = "(" + restrictionCity + ")"; 
		restrictionPropertyBl = "(" + restrictionPropertyBl + ")"; 
		restrictionPropertyPr = "(" + restrictionPropertyPr + ")"; 
		
		restrictionCtry = replaceAll(restrictionCtry, "{restriction_geo_bl}", blGeoRestriction);
		restrictionCtry = replaceAll(restrictionCtry, "{restriction_geo_pr}", prGeoRestriction);
		restrictionCtry = replaceAll(restrictionCtry, "{restriction_bl}", blRestriction);
		restrictionCtry = replaceAll(restrictionCtry, "{restriction_pr}", prRestriction);
		restrictionCtry = replaceAll(restrictionCtry, "{restriction_parcel}", parcelRestriction);
		restrictionCtry = replaceAll(restrictionCtry, "{restriction_lease}", leaseRestriction);
		
		restrictionCity = replaceAll(restrictionCity, "{restriction_geo_bl}", blGeoRestriction);
		restrictionCity = replaceAll(restrictionCity, "{restriction_geo_pr}", prGeoRestriction);
		restrictionCity = replaceAll(restrictionCity, "{restriction_bl}", blRestriction);
		restrictionCity = replaceAll(restrictionCity, "{restriction_pr}", prRestriction);
		restrictionCity = replaceAll(restrictionCity, "{restriction_parcel}", parcelRestriction);
		restrictionCity = replaceAll(restrictionCity, "{restriction_lease}", leaseRestriction);

		restrictionPropertyBl = replaceAll(restrictionPropertyBl, "{restriction_geo_bl}", blGeoRestriction);
		restrictionPropertyBl = replaceAll(restrictionPropertyBl, "{restriction_geo_pr}", prGeoRestriction);
		restrictionPropertyBl = replaceAll(restrictionPropertyBl, "{restriction_bl}", blRestriction);
		restrictionPropertyBl = replaceAll(restrictionPropertyBl, "{restriction_pr}", prRestriction);
		restrictionPropertyBl = replaceAll(restrictionPropertyBl, "{restriction_parcel}", parcelRestriction);
		restrictionPropertyBl = replaceAll(restrictionPropertyBl, "{restriction_lease}", leaseRestriction);

		restrictionPropertyPr = replaceAll(restrictionPropertyPr, "{restriction_geo_bl}", blGeoRestriction);
		restrictionPropertyPr = replaceAll(restrictionPropertyPr, "{restriction_geo_pr}", prGeoRestriction);
		restrictionPropertyPr = replaceAll(restrictionPropertyPr, "{restriction_bl}", blRestriction);
		restrictionPropertyPr = replaceAll(restrictionPropertyPr, "{restriction_pr}", prRestriction);
		restrictionPropertyPr = replaceAll(restrictionPropertyPr, "{restriction_parcel}", parcelRestriction);
		restrictionPropertyPr = replaceAll(restrictionPropertyPr, "{restriction_lease}", leaseRestriction);


		this.abCostWizardTreeCountry.addParameter("restriction_ctry", restrictionCtry);
		this.abCostWizardTreeCountry.addParameter("restriction_city", restrictionCity);
		this.abCostWizardTreeCountry.addParameter("restriction_property_pr", restrictionPropertyPr);
		this.abCostWizardTreeCountry.addParameter("restriction_property_bl", restrictionPropertyBl);
		this.abCostWizardTreeCountry.addParameter("restriction_building_parcel", parcelRestriction);
		this.abCostWizardTreeCountry.addParameter("restriction_building_bl", restrictionPropertyBl);
		this.abCostWizardTreeCountry.addParameter("restriction_lease", leaseRestriction);
		this.abCostWizardTreeCountry.refresh();
	},
	
	refreshAccountTree: function(filterValues) {
		// work just with account id
		var restriction = "1=1";
		if (valueExistsNotEmpty(filterValues['ac_id'])) {
			var value = filterValues['ac_id'];
			if (isArray(value)) {
				restriction = "x.ac_id IN ('" + makeSafeSqlValue(value).join("','") + "') "; 
			} else {
				restriction = "x.ac_id = '" + makeSafeSqlValue(value) + "'"; 
			}
		}
		this.abCostWizardTreeAccounts.addParameter('restriction_account', restriction);
		this.abCostWizardTreeAccounts.refresh();
	},
	
	showNodeDetails: function(mode, selectedNode){
		this.wizardController.onClickTreeNode(mode, selectedNode);
	}
});


/**
 * Add translatable parameters  to tree panel.
 */
function translatePropertiesTree(){
	var treePanel = View.panels.get('abCostWizardTreeCountry');
	if (treePanel) {
		treePanel.addParameter('label_parcel', getMessage('label_parcel'));
		treePanel.addParameter('label_building', getMessage('label_building'));
		treePanel.addParameter('label_structure', getMessage('label_structure'));
		treePanel.addParameter('label_land', getMessage('label_land'));
		treePanel.addParameter('label_landlord', getMessage('label_landlord'));
		treePanel.addParameter('label_tenant', getMessage('label_tenant'));
	}
}


/**
 * Load details for selected property node.
 * 
 */
function showDetailsForNode(ctx){
	var command = ctx.command;
	var objTreePanel = View.panels.get(command.parentPanelId);
	var treeControler = View.controllers.get('abCostWizTreeCtrl');
	if (objTreePanel) {
		var selectedNode = objTreePanel.lastNodeClicked;
		treeControler.showNodeDetails('properties', selectedNode);
	}
}


/**
 * Load details for selected account node.
 * 
 */
function showDetailsForAccount(ctx){
	var command = ctx.command;
	var objTreePanel = View.panels.get(command.parentPanelId);
	var treeControler = View.controllers.get('abCostWizTreeCtrl');
	if (objTreePanel) {
		var selectedNode = objTreePanel.lastNodeClicked;
		treeControler.showNodeDetails('account', selectedNode);
	}
}

/**
 * Create sql statement  with geographical  fields restriction  for specified table.
 * 
 * @param filterValues  object  with filter values
 * @param tableName  table name (bl or pr)
 * @param  fieldNames  array with field names (short name)
 */
function createGeographicalRestrictionForTable(filterValues, tableName, fieldNames){
	var sqlStatements = new Array();
	if (!valueExists(filterValues)) {
		filterValues = {};
	}
	
	for (var i=0; i < fieldNames.length; i++) {
		var field = fieldNames[i];
		if (valueExistsNotEmpty(filterValues[field])) {
			var value = filterValues[field];
			var sqlValue = makeSafeSqlValue(value);
			var fieldSql = null;
			if (isArray(value)) {
				fieldSql = tableName +"." +field + " IN ('" + value.join("','") + "')"; 
			} else {
				fieldSql = tableName +"." +field + " = '" + value + "'"; 
			}
			sqlStatements.push(fieldSql);
		}
	}
	if (sqlStatements.length == 0) {
		sqlStatements.push("1=1");
	}
	return sqlStatements.join(" AND ");
}

/**
 * Replace All method
 * 
 * Source Javascript  forum 
 */
function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll(str, find, replace) {
	return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}
