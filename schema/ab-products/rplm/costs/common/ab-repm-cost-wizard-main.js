/**
 * Cost wizard main controller.
 */
var abRepmCostWizardController = View.createController('abRepmCostWizardController', {
	// view mode (values: properties, accounts); if cost wizard for Building, properties and lease or is for accounts. 
	wizardMode: 'properties',
	
	// selected tree node
	selectedTreedNode: null,
	
	//selected node type
	nodeType: null,
	
	// tree controller
	treeController : null,
	
	filterValues: null,
	
	queryParameterNames: ['bl_id', 'pr_id'],
	queryParameters: null,
	isDemoMode: false,
	
	// MC & VAT 
	isMcAndVatEnabled: false,
	// currency selection
	displayCurrency: {
		type: '',
		code: '',
		exchangeRateType: 'Budget'
	},	
	
	// statistic values
	statisticAttibutesRecur:{
		formulas:['sum'],
		fields:['cost_tran_recur.amount_income_base_payment', 'cost_tran_recur.amount_income_vat_payment', 'cost_tran_recur.amount_income_total_payment',
   		        'cost_tran_recur.amount_expense_base_payment', 'cost_tran_recur.amount_expense_vat_payment', 'cost_tran_recur.amount_expense_total_payment'],
		currencyCode: '',
		exchangeRateType: '',
		currencyFields: []
	},
	
	statisticAttibutesSched:{
		formulas:['sum'],
		fields:['cost_tran_sched.amount_income_base_payment', 'cost_tran_sched.amount_income_vat_payment', 'cost_tran_sched.amount_income_total_payment',
   		        'cost_tran_sched.amount_expense_base_payment', 'cost_tran_sched.amount_expense_vat_payment', 'cost_tran_sched.amount_expense_total_payment'],
		currencyCode: '',
		exchangeRateType: '',
		currencyFields: []
	},

	statisticAttibutesActual:{
		formulas:['sum'],
		fields:['cost_tran.amount_income_base_payment', 'cost_tran.amount_income_vat_payment', 'cost_tran.amount_income_total_payment',
		        'cost_tran.amount_expense_base_payment', 'cost_tran.amount_expense_vat_payment', 'cost_tran.amount_expense_total_payment'],
		currencyCode: '',
		exchangeRateType: '',
		currencyFields: []
	},
	
	
	afterViewLoad: function() {
		if (valueExistsNotEmpty(viewMode)) {
			this.wizardMode = viewMode;
			$('selectWizardMode').value = viewMode;
		}
		
	    this.isDemoMode = isInDemoMode();
	    this.queryParameters =  readQueryParameters(this.queryParameterNames);
		this.isMcAndVatEnabled = (valueExists(View.activityParameters['AbCommonResources-EnableVatAndMultiCurrency']) 
				&&  parseInt(View.activityParameters['AbCommonResources-EnableVatAndMultiCurrency']) == 1);
		
	},
	
	afterInitialDataFetch: function(){
		// load tree view 
		this.loadTreePanel(this.wizardMode);
		var controller = this;
		
		isTreeLoaded(150, function(){
			if (controller.isMcAndVatEnabled) {
				controller.displayCurrency.type = 'user';
				controller.displayCurrency.code = View.user.userCurrency.code; 
				controller.displayCurrency.exchangeRateType = 'Payment';
				controller.filterConsole.displayCurrency = controller.displayCurrency;
				
				controller.setMcVatAndStatisticAttributes();
			}
			
			controller.initializeFilterConsole();
			controller.enableTabs(false);
			// if is in demo mode  apply filter
			if (controller.isDemoMode) {
				controller.filterConsole_onFilter();
			}
		});
		
	},
	
	initializeFilterConsole: function(){
		// show/hide account  line
		this.filterConsole.showField('ac.ac_id',  this.wizardMode == "account");

		// populate From Year - To Year 
		populateTimeRangeFields();
		
		this.customizeCostPanels();

		//cost type  field and cost category
		this.setCostTypeAndCategory(this.wizardMode, this.selectedTreedNode);
		
	},
	
	customizeCostPanels: function(){
		// CAM field
		var showCam = this.isValidSelectionForCAM();
		this.showCamField(showCam);
		this.showCamButtonAndFields(showCam);

		// Add new cost button
		var showAddNew = this.isValidSelectionForAddNew();
		this.showAddNewCostPanelActionButton(showAddNew);
	},
	
	// on click show 
	filterConsole_onFilter: function(){
		this.filterValues = null;
		if (this.readFilterValues()) {
			
			if (this.isMcAndVatEnabled) {
				if (valueExists(this.filterConsole.displayCurrency)) {
					this.displayCurrency = this.filterConsole.displayCurrency;
				}
				this.setMcVatAndStatisticAttributes();
			}
			
			// refresh tree control
			this.treeController.refreshTree(this.filterValues);

			// refresh the report
			this.refreshTabs(this.wizardMode, this.filterValues);
			this.customizeCostPanels();
			
		}
	},
	
	// reset tree selection
	filterConsole_onClearTreeSelection: function(){
		this.selectedTreedNode = null;
		this.nodeType = null;

		this.customizeCostPanels();
		
		//cost type  field and cost category
		this.setCostTypeAndCategory(this.wizardMode, this.selectedTreedNode);

		// refresh the report
		this.refreshTabs(this.wizardMode, this.filterValues);
	},
	
	// clear  filter console
	filterConsole_onClear: function(){
		this.filterConsole.clear();
		this.filterConsole.setFieldValue('year_type', 'calendar');
		var objFromYear = $('time_range_from');
		var objToYear = $('time_range_to');
		if(objFromYear && objToYear){
			objFromYear.value = new Date().getFullYear();
			objToYear.value = new Date().getFullYear();
		}
		this.filterConsole.setFieldValue('cost_cam', 'ALL');
		this.filterConsole.setCheckbox('showActiveCosts', 'active', true);
	},
	
	onChangeWizardMode: function(mode){
		if (this.wizardMode == mode) {
			return false;
		}
		this.wizardMode = mode;
		// change tree panel and reset wizard to default
		this.selectedTreedNode = null;
		this.nodeType = null;
		
		this.loadTreePanel(mode);
		this.initializeFilterConsole();
		this.filterConsole_onClear();
		this.enableTabs(false);
	},
	
	onClickTreeNode: function(treeType, selectedNode){
		this.selectedTreedNode = selectedNode;
		var nodeType = this.getNodeFieldType(selectedNode);
		if (valueExists(nodeType) && nodeType != this.nodeType) {
			this.nodeType = nodeType;
			
			//cost type  field and cost category
			this.setCostTypeAndCategory(this.wizardMode, this.selectedTreedNode);
		}

		this.customizeCostPanels();

		// refresh the report
		// TODO discuss filter behavior
		this.refreshTabs(this.wizardMode, this.filterValues, this.selectedTreedNode);
	},
	
	// show cam field in filter console
	showCamField: function(visible){
		this.filterConsole.setFieldValue('cost_cam', 'ALL');
		this.filterConsole.showField('cost_cam',  visible);
	},
	
	showCamButtonAndFields: function(visible){

		if (this.recurringCostGrid.visible) {
			this.recurringCostGrid.actions.get("setSelectedRecur").show(visible);
			this.recurringCostGrid.showColumn('cost_tran_recur.cam_cost', visible);
			this.recurringCostGrid.update();
		}
		if (this.scheduledCostGrid.visible) {
			this.scheduledCostGrid.actions.get("setSelectedSched").show(visible);
			this.scheduledCostGrid.showColumn('cost_tran_sched.cam_cost', visible);
			this.scheduledCostGrid.update();
		}
		if (this.actualCostGrid.visible) {
			this.actualCostGrid.actions.get("setSelected").show(visible);
			this.actualCostGrid.showColumn('cost_tran.cam_cost', visible);
			this.actualCostGrid.update();
		}
	},
	
	isValidSelectionForAddNew: function() {
		var isValidSelection = false;
		if (this.wizardMode == 'properties' &&
				('lease' == this.nodeType || 'sublease' == this.nodeType 
						|| 'building' == this.nodeType || 'parcel' == this.nodeType 
						|| 'structure' == this.nodeType || 'land' == this.nodeType )) {
			isValidSelection = true;
		} else if  (this.wizardMode == 'account') {
			isValidSelection = true;
		}
		return isValidSelection;
	},
	
	/**
	 * Show / hide panel actions 
	 */
	showAddNewCostPanelActionButton: function(visible){
		if (this.recurringCostGrid.visible) {
			this.recurringCostGrid.actions.get("new").show(visible);
			this.recurringCostGrid.update();
		}
		if (this.scheduledCostGrid.visible) {
			this.scheduledCostGrid.actions.get("new").show(visible);
			this.scheduledCostGrid.update();
		}
	},
	
	/**
	 * Set statistic attributes to grid object.
	 */
	setParameters: function(panelId, configObject) {
		var objPanel = View.panels.get(panelId);
		if (objPanel) {
			objPanel.setStatisticAttributes(configObject);
		}
	},
	
	enableTabs: function(enabled) {
		if (!enabled){
			// hide panels
			this.recurringCostGrid.show(false, true);
			this.scheduledCostGrid.show(false, true);
			this.actualCostGrid.show(false, true);
		}
		
		this.costWizardTabs.enableTab('costWizardTab_recurring', enabled);
		this.costWizardTabs.enableTab('costWizardTab_scheduled', enabled);
		this.costWizardTabs.enableTab('costWizardTab_actual', enabled);
	},
	
	/**
	 * Load tree panel based on wizard mode
	 */
	loadTreePanel: function(mode){
		if (mode == 'properties') {
			this.treeContainerPanel.loadView('ab-repm-cost-wizard-tree.axvw');
		} else if (mode == 'account') {
			this.treeContainerPanel.loadView('ab-repm-cost-wizard-tree-account.axvw');
		}
	},
	
	/**
	 * Check is current selection is valid for CAM display
	 */
	isValidSelectionForCAM: function(){
		var isValidSelection = false;
		if (this.wizardMode == 'properties' &&
				('lease' == this.nodeType || 'sublease' == this.nodeType 
						|| 'building' == this.nodeType 
						|| 'structure' == this.nodeType || 'land' == this.nodeType )) {
			var nodeId = this.selectedTreedNode.data[this.selectedTreedNode.level.pkFields[0]];
			isValidSelection =  hasLeaseAsLandlord(nodeId, this.nodeType);
		}
		return isValidSelection;
	},
	
	/**
	 * Returns  cost type restriction.
	 * 
	 * @param isSqlString restriction  format (sql string or restriction object)
	 */
	getCostTypeRestriction: function(isSqlString){
		var costTypeRestriction = null;
		if(isSqlString) {
			costTypeRestriction = "cost_cat.rollup_prorate LIKE 'ALL%' ";
			if(this.wizardMode == 'properties') {
				if ('lease' == this.nodeType || 'sublease' == this.nodeType) {
					costTypeRestriction += " OR cost_cat.rollup_prorate LIKE 'LEASE%' ";
				} else if ('building' == this.nodeType) {
					costTypeRestriction += " OR cost_cat.rollup_prorate LIKE 'BLDG%' ";
				} else if ('structure' == this.nodeType || 'land' == this.nodeType || 'parcel' == this.nodeType) {
					costTypeRestriction += " OR cost_cat.rollup_prorate LIKE 'PROP%' ";
				} else {
					costTypeRestriction += " OR cost_cat.rollup_prorate LIKE 'LEASE%' ";
					costTypeRestriction += " OR cost_cat.rollup_prorate LIKE 'BLDG%' ";
					costTypeRestriction += " OR cost_cat.rollup_prorate LIKE 'PROP%' ";
				}
			}
			costTypeRestriction = "(" + costTypeRestriction + ")";
		}else{
			var costTypeRestriction = new Ab.view.Restriction();
			costTypeRestriction.addClause('cost_cat.rollup_prorate', 'ALL%', 'LIKE');
			if(this.wizardMode == 'properties') {
				if ('lease' == this.nodeType || 'sublease' == this.nodeType) {
					costTypeRestriction.addClause('cost_cat.rollup_prorate', 'LEASE%', 'LIKE', 'OR');
				} else if ('building' == this.nodeType) {
					costTypeRestriction.addClause('cost_cat.rollup_prorate', 'BLDG%', 'LIKE', 'OR');
				} else if ('structure' == this.nodeType || 'land' == this.nodeType || 'parcel' == this.nodeType) {
					costTypeRestriction.addClause('cost_cat.rollup_prorate', 'PROP%', 'LIKE', 'OR');
				} else {
					costTypeRestriction.addClause('cost_cat.rollup_prorate', 'LEASE%', 'LIKE', 'OR');
					costTypeRestriction.addClause('cost_cat.rollup_prorate', 'BLDG%', 'LIKE', 'OR');
					costTypeRestriction.addClause('cost_cat.rollup_prorate', 'PROP%', 'LIKE', 'OR');
				}
			}
		}
		return costTypeRestriction;
	},
	
	setCostTypeAndCategory: function(mode, treeNode){
		var costTypeRestriction = this.getCostTypeRestriction(false);
		
		var costCateg_ds = this.view.dataSources.get("abCostType_ds");
		var records = costCateg_ds.getRecords(costTypeRestriction);
		var costTypeField = this.filterConsole.fields.get("cost_cat.cost_type");
		costTypeField.clearOptions();
		costTypeField.addOption("", "");
		for (var i=0; i < records.length; i++) {
			var record = records[i];
			var costType = record.getValue("cost_cat.cost_type");
			costTypeField.addOption(costType, costType);
		}
		this.filterConsole.setFieldValue("cost_cat.cost_type", "");
	},
	
	getNodeFieldType: function(treeNode){
		var fieldType= null;
		if (valueExists(treeNode.data)) {
			for (var fieldName in treeNode.data) {
				if (fieldName.substring(fieldName.indexOf(".") + 1) == "field_type") {
					fieldType = (treeNode.data[fieldName]).toLowerCase();
					break;
				}
			}
		}
		return fieldType;
	},
	
	readFilterValues: function(){
		if (this.filterValues == null){
			this.filterValues = {};
		}

		var yearType = this.filterConsole.getFieldValue('year_type');
		var objFromYear = $('time_range_from');
		var objToYear = $('time_range_to');
		if(objFromYear && objToYear){
			var startYear = parseInt(objFromYear.value);
			var endYear = parseInt(objToYear.value);
			if(startYear > endYear) {
				View.showMessage(getMessage("errTimeRangeLimits"));
				return false;
			}
			var timeRange = getTimeRangeLimits(yearType, startYear, endYear);
			this.filterValues['startDate'] = this.filterConsole_ds.formatValue("bl.date_bl", timeRange['startDate'], false);
			this.filterValues['endDate'] = this.filterConsole_ds.formatValue("bl.date_bl", timeRange['endDate'], false);
		}
		
		var fieldValue = this.getFilterValueForField('bl.ctry_id');
		if (valueExistsNotEmpty(fieldValue)) {
			this.filterValues['ctry_id'] = fieldValue;
		}
		
		var fieldValue = this.getFilterValueForField('bl.regn_id');
		if (valueExistsNotEmpty(fieldValue)) {
			this.filterValues['regn_id'] = fieldValue;
		}

		var fieldValue = this.getFilterValueForField('bl.state_id');
		if (valueExistsNotEmpty(fieldValue)) {
			this.filterValues['state_id'] = fieldValue;
		}

		var fieldValue = this.getFilterValueForField('bl.city_id');
		if (valueExistsNotEmpty(fieldValue)) {
			this.filterValues['city_id'] = fieldValue;
		}
		
		var fieldValue = this.getFilterValueForField('bl.site_id');
		if (valueExistsNotEmpty(fieldValue)) {
			this.filterValues['site_id'] = fieldValue;
		}

		var queryParameter = valueExistsNotEmpty(this.queryParameters['pr_id'])?this.queryParameters['pr_id']: null;
		var fieldValue = this.getFilterValueForField('bl.pr_id', queryParameter);
		if (valueExistsNotEmpty(fieldValue)) {
			this.filterValues['pr_id'] = fieldValue;
		}
		
		var fieldValue = this.getFilterValueForField('parcel.parcel_id');
		if (valueExistsNotEmpty(fieldValue)) {
			this.filterValues['parcel_id'] = fieldValue;
		}

		var queryParameter = valueExistsNotEmpty(this.queryParameters['bl_id'])?this.queryParameters['bl_id']: null;
		var fieldValue = this.getFilterValueForField('bl.bl_id', queryParameter);
		if (valueExistsNotEmpty(fieldValue)) {
			this.filterValues['bl_id'] = fieldValue;
		}
		var fieldValue = this.getFilterValueForField('ls.ls_id');
		if (valueExistsNotEmpty(fieldValue)) {
			this.filterValues['ls_id'] = fieldValue;
		}
		var fieldValue = this.getFilterValueForField('ac.ac_id');
		if (valueExistsNotEmpty(fieldValue)) {
			this.filterValues['ac_id'] = fieldValue;
		}
		var fieldValue = this.getFilterValueForField('cost_cat.cost_type');
		if (valueExistsNotEmpty(fieldValue)) {
			this.filterValues['cost_type'] = fieldValue;
		}

		var fieldValue = this.getFilterValueForField('cost_cat.cost_cat_id');
		if (valueExistsNotEmpty(fieldValue)) {
			this.filterValues['cost_cat_id'] = fieldValue;
		}
		// active cost
		var fieldValue = this.filterConsole.getCheckboxValues('showActiveCosts');
		if (valueExistsNotEmpty(fieldValue)) {
			this.filterValues['active_cost'] = fieldValue.indexOf('active') != -1;
		}
		// cam cost
		var fieldValue = this.getFilterValueForField('cost_cam');
		if (valueExistsNotEmpty(fieldValue)) {
			this.filterValues['cost_cam'] = fieldValue;
		}
		
		return true;
	},
	
	getFilterValueForField: function(fieldName, queryParameterValue){
		var result = null;
		if (this.filterConsole.hasFieldMultipleValues(fieldName)) {
			result = this.filterConsole.getFieldMultipleValues(fieldName);
			if (valueExistsNotEmpty(queryParameterValue)) {
				result.push(queryParameterValue);
			}
			
		}else{
			result = this.filterConsole.getFieldValue(fieldName);
			if (valueExistsNotEmpty(queryParameterValue)) {
				result = new Array();
				result.push(this.filterConsole.getFieldValue(fieldName));
				result.push(queryParameterValue);
			}
		}
		return result;
	},
	
	// Refresh tabs  using filter values and selected node
	refreshTabs: function(wizardMode, filterValues, selectedNode){
		
		var blRestriction = createGeographicalRestrictionForTable(this.filterValues, "bl", ["ctry_id", "regn_id", "state_id", "city_id", "site_id"]);
		var prRestriction = createGeographicalRestrictionForTable(this.filterValues, "property", ["ctry_id", "regn_id", "state_id", "city_id", "site_id"]);
		
		var sqlBlPattern = "(EXISTS(SELECT bl.bl_id FROM bl WHERE {cost_table}.bl_id = bl.bl_id AND {restriction}) " +
				" OR EXISTS(SELECT ls.ls_id FROM ls WHERE ls.use_as_template = 0 AND {cost_table}.ls_id = ls.ls_id " +
				" AND EXISTS(SELECT bl.bl_id FROM bl WHERE ls.bl_id = bl.bl_id AND {restriction}))) "; 

		var sqlPrPattern = "(EXISTS(SELECT property.pr_id FROM property WHERE {cost_table}.pr_id = property.pr_id AND {restriction}) " +
				" OR EXISTS(SELECT ls.ls_id FROM ls WHERE ls.use_as_template = 0 AND {cost_table}.ls_id = ls.ls_id AND ls.bl_id IS NULL " +
				" AND EXISTS(SELECT property.pr_id FROM property WHERE ls.pr_id = property.pr_id AND {restriction}))" +
				" OR EXISTS(SELECT parcel.parcel_id FROM parcel WHERE {cost_table}.parcel_id = parcel.parcel_id " +
				" AND EXISTS(SELECT property.pr_id FROM property WHERE parcel.pr_id = property.pr_id AND {restriction}))) "; 
		
		var nodePattern = "";
		// Append nod restriction here base on node type
		if (valueExistsNotEmpty(selectedNode)) {
			if (wizardMode == 'account') {
				// just add account restriction on cost table
				nodePattern = "AND {cost_table}.ac_id = '" + makeSafeSqlValue(selectedNode.data['ac.ac_id']) + "'";
			} else {
				var nodeType = this.getNodeFieldType(selectedNode);
				var nodeId = selectedNode.data[selectedNode.level.pkFields[0]];
				if ('lease' == nodeType || 'sublease' == nodeType) {
					nodePattern = "AND {cost_table}.ls_id = '" + makeSafeSqlValue(nodeId) + "'";
				} else if ('building' == nodeType) {
					nodePattern = "AND {cost_table}.bl_id = '" + makeSafeSqlValue(nodeId) + "'";
				} else if ('structure' == nodeType || 'land' == nodeType) {
					nodePattern = "AND {cost_table}.pr_id = '" + makeSafeSqlValue(nodeId) + "' AND {cost_table}.parcel_id IS NULL ";
				} else if ('parcel' == nodeType) {
					var parentId = selectedNode.parent.data['property.pr_id.key'];
					nodePattern = "AND {cost_table}.parcel_id = '" + makeSafeSqlValue(nodeId) + "' AND {cost_table}.pr_id = '" + makeSafeSqlValue(parentId) + "'";
				} else {
					// is country or city
					if (valueExistsNotEmpty(selectedNode.data['ctry.ctry_id'])) {
						blRestriction += " AND bl.ctry_id = '" + makeSafeSqlValue(selectedNode.data['ctry.ctry_id']) + "'";
						prRestriction += " AND property.ctry_id = '" + makeSafeSqlValue(selectedNode.data['ctry.ctry_id']) + "'";
					} else if (valueExistsNotEmpty(selectedNode.data['city.city_id'])) {
						blRestriction += " AND bl.city_id = '" + makeSafeSqlValue(selectedNode.data['city.city_id']) + "' " +
								"AND bl.state_id = '" + makeSafeSqlValue(selectedNode.data['city.state_id']) + "'";
						prRestriction += " AND property.city_id = '" + makeSafeSqlValue(selectedNode.data['city.city_id']) + "' " +
								"AND property.state_id = '" + makeSafeSqlValue(selectedNode.data['city.state_id']) + "'";
					}
				}
			}
		}
		
		var costParentClauses = new Array();
		
		if (wizardMode == 'properties') {
			//add lease restriction if is defined
			if (valueExistsNotEmpty(filterValues['ls_id'])) {
				var value = filterValues['ls_id'];
				if (isArray(value)) {
					costParentClauses.push("{cost_table}.ls_id IN ('" + makeSafeSqlValue(value).join("','") + "') ");
					//costParentRestriction += " AND {cost_table}.ls_id IN ('" + makeSafeSqlValue(value).join("','") + "') "; 
				} else {
					costParentClauses.push("{cost_table}.ls_id = '" + makeSafeSqlValue(value) + "'");
					//costParentRestriction += " AND {cost_table}.ls_id = '" + makeSafeSqlValue(value) + "'"; 
				}
			}
			//add building restriction 
			if (valueExistsNotEmpty(filterValues['bl_id'])) {
				var value = filterValues['bl_id'];
				if (isArray(value)) {
					costParentClauses.push("{cost_table}.bl_id IN ('" + makeSafeSqlValue(value).join("','") + "') ");
					//costParentRestriction += " AND {cost_table}.bl_id IN ('" + makeSafeSqlValue(value).join("','") + "') "; 
				} else {
					costParentClauses.push("{cost_table}.bl_id = '" + makeSafeSqlValue(value) + "'");
					//costParentRestriction += " AND {cost_table}.bl_id = '" + makeSafeSqlValue(value) + "'"; 
				}
			}
			//add property restriction 
			if (valueExistsNotEmpty(filterValues['pr_id'])) {
				var value = filterValues['pr_id'];
				if (isArray(value)) {
					costParentClauses.push("{cost_table}.pr_id IN ('" + makeSafeSqlValue(value).join("','") + "') ");
					//costParentRestriction += " AND {cost_table}.pr_id IN ('" + makeSafeSqlValue(value).join("','") + "') "; 
				} else {
					costParentClauses.push("{cost_table}.pr_id = '" + makeSafeSqlValue(value) + "'");
					//costParentRestriction += " AND {cost_table}.pr_id = '" + makeSafeSqlValue(value) + "'"; 
				}
			}
			//add parcel restriction 
			if (valueExistsNotEmpty(filterValues['parcel_id'])) {
				var value = filterValues['parcel_id'];
				if (isArray(value)) {
					costParentClauses.push("{cost_table}.parcel_id IN ('" + makeSafeSqlValue(value).join("','") + "') ");
					//costParentRestriction += " AND {cost_table}.parcel_id IN ('" + makeSafeSqlValue(value).join("','") + "') "; 
				} else {
					costParentClauses.push("{cost_table}.parcel_id = '" + makeSafeSqlValue(value) + "'");
					//costParentRestriction += " AND {cost_table}.parcel_id = '" + makeSafeSqlValue(value) + "'"; 
				}
			}
		}
		
		//add account restriction if is defined
		if (wizardMode == 'account') {
			//nodePattern += " AND {cost_table}.ac_id IS NOT NULL "; 
			if (valueExistsNotEmpty(filterValues['ac_id'])) {
				var value = filterValues['ac_id'];
				if (isArray(value)) {
					costParentClauses.push(" {cost_table}.ac_id IS NOT NULL AND {cost_table}.ac_id IN ('" + makeSafeSqlValue(value).join("','") + "') ");
					//nodePattern += " AND {cost_table}.ac_id IN ('" + makeSafeSqlValue(value).join("','") + "') "; 
				} else {
					costParentClauses.push(" {cost_table}.ac_id IS NOT NULL AND {cost_table}.ac_id = '" + makeSafeSqlValue(value) + "'");
					//nodePattern += " AND {cost_table}.ac_id = '" + makeSafeSqlValue(value) + "'"; 
				}
			}
		}
		
		var costParentRestriction = "";
		if (costParentClauses.length > 0) {
			costParentRestriction = " AND (" + costParentClauses.join(" OR ") + ") ";
		}
		
		var recurBlPattern = replaceAll(sqlBlPattern, "{cost_table}", "cost_tran_recur"); 
		recurBlPattern = replaceAll(recurBlPattern, "{restriction}", blRestriction);
		var recurPrPattern = replaceAll(sqlPrPattern, "{cost_table}", "cost_tran_recur"); 
		recurPrPattern = replaceAll(recurPrPattern, "{restriction}", prRestriction);
		var recurNodeRestriction = replaceAll(nodePattern, "{cost_table}", "cost_tran_recur");
		recurNodeRestriction += replaceAll(costParentRestriction, "{cost_table}", "cost_tran_recur");
		
		var schedBlPattern = replaceAll(sqlBlPattern, "{cost_table}", "cost_tran_sched"); 
		schedBlPattern = replaceAll(schedBlPattern, "{restriction}", blRestriction);
		var schedPrPattern = replaceAll(sqlPrPattern, "{cost_table}", "cost_tran_sched"); 
		schedPrPattern = replaceAll(schedPrPattern, "{restriction}", prRestriction);
		var schedNodeRestriction = replaceAll(nodePattern, "{cost_table}", "cost_tran_sched");
		schedNodeRestriction += replaceAll(costParentRestriction, "{cost_table}", "cost_tran_sched");

		var actualBlPattern = replaceAll(sqlBlPattern, "{cost_table}", "cost_tran"); 
		actualBlPattern = replaceAll(actualBlPattern, "{restriction}", blRestriction);
		var actualPrPattern = replaceAll(sqlPrPattern, "{cost_table}", "cost_tran"); 
		actualPrPattern = replaceAll(actualPrPattern, "{restriction}", prRestriction);
		var actualNodeRestriction = replaceAll(nodePattern, "{cost_table}", "cost_tran");
		actualNodeRestriction += replaceAll(costParentRestriction, "{cost_table}", "cost_tran");
		
		var costTranRecurRestriction = "( " +recurBlPattern + " OR " + recurPrPattern + " ) " + recurNodeRestriction;
		var costTranSchedRestriction = "( " +schedBlPattern + " OR " + schedPrPattern + " ) " + schedNodeRestriction;
		var costTranRestriction = "( " +actualBlPattern + " OR " + actualPrPattern + " ) " + actualNodeRestriction;
		
		if (valueExists(this.filterValues["active_cost"]) && this.filterValues["active_cost"]) {
			costTranRecurRestriction += "AND cost_tran_recur.status_active = 1 ";
		}
		
		if (valueExists(this.filterValues["cost_cam"]) && this.filterValues["cost_cam"] != 'ALL') {
			costTranRecurRestriction += "AND cost_tran_recur.cam_cost = '" + this.filterValues["cost_cam"] + "' ";
			costTranSchedRestriction += "AND cost_tran_sched.cam_cost = '" + this.filterValues["cost_cam"] + "' ";
			costTranRestriction += "AND cost_tran.cam_cost = '" + this.filterValues["cost_cam"] + "' ";
		}
		
		// time range restriction 
		costTranRecurRestriction += " AND ((cost_tran_recur.date_end >=  ${sql.date('"+ this.filterValues["startDate"] +"')} OR cost_tran_recur.date_end IS NULL) " +
				"AND cost_tran_recur.date_start < ${sql.date('" + this.filterValues["endDate"] + "')})";
		
		costTranSchedRestriction += " AND (${sql.isNull('cost_tran_sched.date_paid', 'cost_tran_sched.date_due')} >= ${sql.date('" + this.filterValues["startDate"] + "')} " +
				"AND ${sql.isNull('cost_tran_sched.date_paid', 'cost_tran_sched.date_due')} < ${sql.date('" + this.filterValues["endDate"] + "')} ) ";
		
		costTranRestriction += " AND (${sql.isNull('cost_tran.date_paid', 'cost_tran.date_due')} >= ${sql.date('" + this.filterValues["startDate"] + "')} " +
				"AND ${sql.isNull('cost_tran.date_paid', 'cost_tran.date_due')} < ${sql.date('" + this.filterValues["endDate"] + "')} ) ";
		
		
		this.enableTabs(true);
		this.recurringCostGrid.refresh(costTranRecurRestriction);
		this.scheduledCostGrid.refresh(costTranSchedRestriction);
		this.actualCostGrid.refresh(costTranRestriction);
		
		
	},
	
	setMcVatAndStatisticAttributes: function(){
		if (this.isMcAndVatEnabled) {

			this.statisticAttibutesRecur.currencyCode = this.displayCurrency.code;
			this.statisticAttibutesRecur.exchangeRateType = this.displayCurrency.exchangeRateType;
			this.statisticAttibutesRecur.currencyFields = ['cost_tran_recur.amount_income_base_payment', 'cost_tran_recur.amount_income_vat_payment', 'cost_tran_recur.amount_income_total_payment',
			                               		        'cost_tran_recur.amount_expense_base_payment', 'cost_tran_recur.amount_expense_vat_payment', 'cost_tran_recur.amount_expense_total_payment'];
			
			this.statisticAttibutesSched.currencyCode = this.displayCurrency.code;
			this.statisticAttibutesSched.exchangeRateType = this.displayCurrency.exchangeRateType;
			this.statisticAttibutesSched.currencyFields = ['cost_tran_sched.amount_income_base_payment', 'cost_tran_sched.amount_income_vat_payment', 'cost_tran_sched.amount_income_total_payment',
			                               		        'cost_tran_sched.amount_expense_base_payment', 'cost_tran_sched.amount_expense_vat_payment', 'cost_tran_sched.amount_expense_total_payment'];

			this.statisticAttibutesActual.currencyCode = this.displayCurrency.code;
			this.statisticAttibutesActual.exchangeRateType = this.displayCurrency.exchangeRateType;
			this.statisticAttibutesActual.currencyFields = ['cost_tran.amount_income_base_payment', 'cost_tran.amount_income_vat_payment', 'cost_tran.amount_income_total_payment',
			                                		        'cost_tran.amount_expense_base_payment', 'cost_tran.amount_expense_vat_payment', 'cost_tran.amount_expense_total_payment'];
		}
	}
});

/**
 * Wait until tree is loaded.
 * @param interval milliseconds
 * @returns 
 */
function isTreeLoaded(interval, callback){
	var  wizardController = View.controllers.get('abRepmCostWizardController');
	if (valueExistsNotEmpty(wizardController.treeController)) {
		callback();
	} else {
		setTimeout(function() {
			isTreeLoaded(interval, callback);
        }, interval);
	}
}

/**
 * On change wizard from properties to accounts.
 * Change the tree and reset view settings.
 * 
 */
function onChangeWizard(context){
	var objSelect = context;
	var value = objSelect.value;
	var costWizardController = View.controllers.get('abRepmCostWizardController');
	costWizardController.onChangeWizardMode(value);
}


/**
 * Select value handler for cost type.
 * 
 */
function onSelectCostCategory(){
	var costWizardController = View.controllers.get('abRepmCostWizardController');
	var costTypeRestriction = costWizardController.getCostTypeRestriction(true);
	var costType = costWizardController.filterConsole.getFieldValue("cost_cat.cost_type");

	// open select value
	View.selectValue('filterConsole', 
			getMessage("titleSelectValue_costCategory"), 
			["cost_cat.cost_type", "cost_cat.cost_cat_id"], 
			"cost_cat",
			["cost_cat.cost_type", "cost_cat.cost_cat_id"],
			["cost_cat.cost_cat_id", "cost_cat.cost_type"],
			costTypeRestriction);
	
}

/**
 * Populate  time range  fields from filter console.
 */
function populateTimeRangeFields(){
	var afmYearDs = View.dataSources.get('abAfmCalDateYear_ds');
	var records = afmYearDs.getRecords();
	var objFromYear = $('time_range_from');
	var objToYear = $('time_range_to');
	if(objFromYear && objToYear){
		for (var i = 0; i < records.length; i++) {
			var record = records[i];
			var value = record.getValue("afm_cal_dates.year");
			var optionTo = document.createElement("option");
			optionTo.value = value;
			optionTo.text = value;
			var optionFrom = document.createElement("option");
			optionFrom.value = value;
			optionFrom.text = value;
			
			objFromYear.options.add(optionFrom);
			objToYear.options.add(optionTo);
		}
		objFromYear.value = new Date().getFullYear();
		objToYear.value = new Date().getFullYear();
	}
}

/**
 * Returns full values for time range limits
 * 
 * @param yearType year type (calendar or fiscal)
 * @param startYear start year
 * @param endYear end year
 * @returns {startDate: Date(), endDate: Date()}
 */
function getTimeRangeLimits(yearType, startYear, endYear){
	var startMonth = 0;
	var startDay = 1;
	if (yearType == 'fiscal') {
		// fiscal year starts in previous year
		startYear = startYear - 1;
		var params = {
				tableName: 'afm_scmpref',
				fieldNames: toJSON(['afm_scmpref.fiscalyear_startday', 'afm_scmpref.fiscalyear_startmonth', 'afm_scmpref.afm_scmpref']),
				restriction: toJSON({
					'afm_scmpref.afm_scmpref': 0
				})
			};
		try{
			var result = Workflow.call('AbCommonResources-getDataRecords', params);
			var record = result.dataSet.records[0];
			startDay = record.getValue('afm_scmpref.fiscalyear_startday');
			startMonth = parseInt(record.getValue('afm_scmpref.fiscalyear_startmonth'))-1;
		}catch(e){
			Workflow.handleError(e);
			return false;
		}
	} else {
		// for calendar year  increment end year
		endYear = endYear + 1; 
	}
	
	var dateStart = new Date(startYear, startMonth, startDay, 0, 0, 0, 0);
	var dateEnd = new Date(endYear, startMonth, startDay, 0, 0, 0, 0);
	dateEnd = new Date(dateEnd.getTime() - 1000);
	return {'startDate':dateStart, 'endDate':dateEnd};
}


/**
 * Verify if current item has at least one lease where ls.landlord_tenant = 'LANDLORD' 
 * @param id  id of selected item
 * @param type values: building, structure, land, lease
 * @returns {Boolean}
 */
function hasLeaseAsLandlord(id, type){
	var isLandlord = false;
	var restriction = new Ab.view.Restriction();
	restriction.addClause("ls.use_as_template", 0, "=");
	restriction.addClause("ls.landlord_tenant", ["LANDLORD","N/A"], "IN");
	
	if (type == 'building') {
		restriction.addClause("ls.bl_id", id, "=");
	} else if (type == 'structure' || type == 'land'){
		restriction.addClause("ls.pr_id", id, "=");
		restriction.addClause("ls.bl_id", null, "IS NULL");
	} else if (type == 'lease' || type == 'sublease') {
		restriction.addClause("ls.ls_id", id, "=");
	}
	var params = {
			tableName: "ls",
			fieldNames: toJSON(["ls.landlord_tenant"]),
			restriction: toJSON(restriction)
	};
	try{
		var result = Workflow.call('AbCommonResources-getDataRecords', params);
		if (result.code == 'executed'){
			if(valueExists(result.dataSet) && result.dataSet.records.length > 0){
				isLandlord = true;
			}
		}
	}catch(e){
		Workflow.handleError(e);
	}
	return isLandlord;
}

/**
 * Create sql statement  with geographical  fields restriction  for specified table.
 * 
 * @param filterValues  object  with filter values
 * @param tableName  table name (bl or pr)
 * @param  fieldNames  array with field names (short name)
 */
function createGeographicalRestrictionForTable(filterValues, tableName, fieldNames){
	var sqlStatements = new Array();
	if (!valueExists(filterValues)) {
		filterValues = {};
	}
	
	for (var i=0; i < fieldNames.length; i++) {
		var field = fieldNames[i];
		if (valueExistsNotEmpty(filterValues[field])) {
			var value = filterValues[field];
			var sqlValue = makeSafeSqlValue(value);
			var fieldSql = null;
			if (isArray(value)) {
				fieldSql = tableName +"." +field + " IN ('" + value.join("','") + "')"; 
			} else {
				fieldSql = tableName +"." +field + " = '" + value + "'"; 
			}
			sqlStatements.push(fieldSql);
		}
	}
	if (sqlStatements.length == 0) {
		sqlStatements.push("1=1");
	}
	return sqlStatements.join(" AND ");
}

/**
 * Replace All method
 * 
 * Source Javascript  forum 
 */
function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll(str, find, replace) {
	return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}
