var abRepmStraightLineChartCtrl = View.createController('abRepmStraightLineChartCtrl',{
	dataAxisLabel: null,
	groupingAxisLabel: null,
	dataSet: null,
	panelTitle: null,
	currencyCode: "",
	printableRestriction: [],

	afterViewLoad: function(){
		// read chart parameters
		if(valueExistsNotEmpty(this.view.parameters['dataAxisLabel'])){
			this.dataAxisLabel = this.view.parameters['dataAxisLabel'].replace(":", "");
		}
		if(valueExistsNotEmpty(this.view.parameters['groupingAxisLabel'])){
			this.groupingAxisLabel = this.view.parameters['groupingAxisLabel'];
		}
		if(valueExistsNotEmpty(this.view.parameters['dataSet'])){
			this.dataSet = this.view.parameters['dataSet'];
			if(this.dataSet.length == 0){
				this.dataSet = null;
			}
		}
		if(valueExistsNotEmpty(this.view.parameters['panelTitle'])){
			this.panelTitle = this.view.parameters['panelTitle'];
		}
		if(valueExistsNotEmpty(this.view.parameters['currencyCode'])){
			this.currencyCode = this.view.parameters['currencyCode'];
		}
		if(valueExistsNotEmpty(this.view.parameters['printableRestriction'])){
			this.printableRestriction = this.view.parameters['printableRestriction'];
		}

	},
	
	afterInitialDataFetch: function(){
		this.abRepmStraightLineChart.show(true, true);
		this.abRepmStraightLineChart.setTitle(this.panelTitle);
		this.buildChart();
	},
	
	abRepmStraightLineChart_afterRefresh: function(){
		this.buildChart();
	},
	
	buildChart: function() {

		var config = this.getChartConfig();
		var parentElementId = this.abRepmStraightLineChart.parentElementId;
		
		//grouping
		config.addGroupingAxis("cost_tran_recur.vf_date_start", this.groupingAxisLabel);
		config.groupingAxis[0].labelRotation = '45';
		
		//data- column series
		config.addDataAxis('column', "cost_tran_recur.net_amount_income", this.dataAxisLabel);

		config.addValueAxis(0, this.dataAxisLabel);

		config.dataAxis[0].showLabel = true;
		config.groupingAxis[0].showLabel = true;
		
		var chart = new ChartControl(parentElementId, config);
		chart.setData(this.dataSet);
	},
	
	getChartConfig: function() {
		var config = new ChartConfig();
		config.chartType = 'columnChart';
		config.showExportButton = true;
		config.showOnLoad = true;
		config.showLegendOnLoad = true;
		config.showDataTips = true
		config.showLabels = false;
		config.showUnitPrefixes = false;
		config.showUnitSuffixes = false;
		if (valueExistsNotEmpty(this.currencyCode)) {
			config.currencyFields["cost_tran_recur.net_amount_income"] = View.currencySymbolFor(this.currencyCode);
		}
		return config;
	}
});

