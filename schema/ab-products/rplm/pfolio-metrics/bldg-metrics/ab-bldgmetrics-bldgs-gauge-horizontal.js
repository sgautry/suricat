var gaugeChartPanel;
var arrow;
var axis;
var gaugeHoriz;

function Gauge(controller, gaugeChartPanel) {
	
	this.minValue = 0;
	
	this.restriction = "1=1";
	
	//Max Gauge interval value
	this.maxValue = 0;
	
	this.gaugeChartPanel = gaugeChartPanel;
	
	this.field = null;
	
	this.controller = controller;
	
	//DataSource of the Gauge
	this.dataSource = null; 
	
	this.calculateMinMax = function (){
			
		//get min and max values 
		var record = this.dataSource.getRecord();	
		this.minValue = 0;
		this.maxValue = Math.round(parseFloat(record.getValue(this.field)));
		
		// if the field has the same value in all records then use an 100 units interval
		if(this.minValue == this.maxValue){
		   this.maxValue = this.minValue + 100;
		}
	 };
	
	 /**
	  * Create horizontal gauge using AmCharts libraries
	  * 
	  */
	this.refreshGauge = function(){
		
		this.dataSource = this.controller.items[controller.indexSelected]['dataSource']; 
		this.field = this.controller.items[controller.indexSelected]['field'];
		 
		this.calculateMinMax();
		 
		var minorTickInterval =  ((this.maxValue-this.minValue)/4).toFixed(2);
		
		var majorTickInterval =  ((this.maxValue-this.minValue)/minorTickInterval/2).toFixed(2);
		
		var chart = AmCharts.makeChart("div_linear_gauge_row2col1", {
			 "type": "serial",
			  "rotate": true,
			  "theme": "light",
			  "autoMargins": false,
			  "marginTop": 30,
			  "marginLeft": 80,
			  "marginBottom": 30,
			  "marginRight": 50,
			  "dataProvider": [ {
			    "category": "",
			    "value1": this.minValue,
			    "value2": minorTickInterval,
			    "value3": majorTickInterval,
			    "value4": this.maxValue,
			    "limit": this.maxValue
			  } ],
			  "valueAxes": [ {
			    "maximum": this.maxValue,
			    "stackType": "regular",
			    "gridAlpha": 0
			  }],
			  "startDuration": 1,
			  "graphs": [ {
			    "valueField": "value4",
			    "showBalloon": true,
			    "type": "column",
			    "lineAlpha": 0,
			    "fillAlphas": 0.8,
			    "fillColors": [ "#19d228", "#f6d32b", "#fb2316" ],
			    "gradientOrientation": "horizontal"
			  },
			  {
			    "columnWidth": 0.5,
				"lineColor": "#000000",
				"lineThickness": 3,
				"noStepRisers": true,
				"stackable": false,
				"type": "step",
				"valueField": "limit"
		      }],
			  "columnWidth": 1,
			  "categoryField": "category",
			  "categoryAxis": {
			    "gridAlpha": 0,
			    "position": "left"
			  }
		});
		
		gaugeChartPanel.setContentPanel(Ext.get('div_linear_gauge_row2col1'));
	 };
	 
	this.refreshGauge();
    
}
