var gaugeChartPanel;
var arrow;
var axis;

function Gauge(controller, gaugeChartPanel) {
	
	this.minValue = 0;
	
	this.restriction = "1=1";
	
	//Max Gauge interval value
	this.maxValue = 0;
	
	this.gaugeChartPanel = gaugeChartPanel;
	
	this.field = null;
	
	this.controller = controller;
	
	//DataSource of the Gauge
	this.dataSource = null; 
	
	this.calculateMinMax = function (){
			
		//get min and max values 
		var record = this.dataSource.getRecord();	
		this.minValue = 0;
		this.maxValue = Math.round(parseFloat(record.getValue(this.field)));
		
		// if the field has the same value in all records then use an 100 units interval
		if(this.minValue == this.maxValue){
		   this.maxValue = this.minValue + 100;
		}
	 };
	 
	 /**
	  * Draw Gauge chart using the latest AmChart library
	  */
	 this.refreshGauge = function(){
		 
		 this.dataSource = this.controller.items[controller.indexSelected]['dataSource']; 
		 this.field = this.controller.items[controller.indexSelected]['field'];
		 
		 this.calculateMinMax();
		 
		 var gauge = AmCharts.makeChart( "div_circular_gauge_row3col1", {
			  "type": "gauge",
			  "theme": "light",
			  "axes": [ {
			    "axisThickness": 1,
			    "axisAlpha": 0.2,
			    "tickAlpha": 0.2,
			    "bands": [ {
			      "color": "#84b761",
			      "startValue": this.minValue,
			      "endValue": this.maxValue/3
			    }, {
			      "color": "#fdd400",
			      "startValue": this.maxValue/3,
			      "endValue": 2*this.maxValue/3
			    }, {
			      "color": "#cc4748",
			      "startValue": 2*this.maxValue/3,
			      "endValue": this.maxValue,
			      "innerRadius": "95%",
			    } ],
			  "bottomText": this.maxValue,
			  "bottomTextYOffset": -20,
			  "endValue": this.maxValue
			}],
			"arrows": [ { 
				"value": this.maxValue,
				"alpha": 1
				} 
			],
			"export": {
			    "enabled": true
			 },
			 "marginBottom": 10,
			 "marginTop":10,
			 "fontSize": 9
		 });
		 
		 gaugeChartPanel.setContentPanel(Ext.get('div_circular_gauge_row3col1'));
	 },
	
	this.refreshGauge();
    
}
