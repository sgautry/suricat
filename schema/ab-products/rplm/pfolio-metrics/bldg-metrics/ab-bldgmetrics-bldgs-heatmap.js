var abTableMetricsBuildingsHeatMap_ctrl = View.createController('abTableMetricsBuildingsHeatMap_ctrl', {
	
	selectedMetric_index: 0,
	minMax_record: null,
	heatMap: null,


	afterViewLoad: function(){
		
		this.minMax_record = this.dsMinMaxBuildingsHeatMap.getRecord();
		//set parameters for metric fields query
        
        this.tableMetricFields_bldgs_heatmap.addParameter("cost_per_area", getMessage("cost_per_area"));
        this.tableMetricFields_bldgs_heatmap.addParameter("avg_area_em", getMessage("avg_area_em"));
        this.tableMetricFields_bldgs_heatmap.addParameter("ru_ratio", getMessage("ru_ratio"));
        this.tableMetricFields_bldgs_heatmap.addParameter("usable_area", getMessage("usable_area"));
        this.tableMetricFields_bldgs_heatmap.addParameter("value_book", getMessage("value_book"));
        this.tableMetricFields_bldgs_heatmap.addParameter("value_market", getMessage("value_market"));
        this.tableMetricFields_bldgs_heatmap.addParameter("fci", getMessage("fci"));
        this.tableMetricFields_bldgs_heatmap.addParameter("operating_costs", getMessage("operating_costs"));
        this.tableMetricFields_bldgs_heatmap.addParameter("capital_project_cost", getMessage("capital_project_cost"));
        this.tableMetricFields_bldgs_heatmap.addParameter("area_estimated", getMessage("area_estimated"));
        this.tableMetricFields_bldgs_heatmap.addParameter("efficency_rate", getMessage("efficency_rate"));
        this.tableMetricFields_bldgs_heatmap.addParameter("int_gross_area", getMessage("int_gross_area"));
        this.tableMetricFields_bldgs_heatmap.addParameter("total_lease_neg_area", getMessage("total_lease_neg_area"));
        this.tableMetricFields_bldgs_heatmap.addParameter("total_occup_area", getMessage("total_occup_area"));
        this.tableMetricFields_bldgs_heatmap.addParameter("rentable_area", getMessage("rentable_area"));
        this.tableMetricFields_bldgs_heatmap.addParameter("total_room_area", getMessage("total_room_area"));
        this.tableMetricFields_bldgs_heatmap.addParameter("employee_headcount", getMessage("employee_headcount"));
        this.tableMetricFields_bldgs_heatmap.addParameter("max_bldg_occup", getMessage("max_bldg_occup"));
        this.tableMetricFields_bldgs_heatmap.addParameter("building_occupancy", getMessage("building_occupancy"));
        this.tableMetricFields_bldgs_heatmap.addParameter("vacancy_percent", getMessage("vacancy_percent"));
        this.tableMetricFields_bldgs_heatmap.addParameter("chargeable_cost", getMessage("chargeable_cost"));
        
        this.createFieldsArray();
        
        this.drawMap();
    },
    
	drawMap: function(){
		    this.minMax_record = this.dsMinMaxBuildingsHeatMap.getRecord();
	    	var dataSource = this.dsBuildingsHeatMap;
	    	
	    	var records = dataSource.getRecords();
	    	var latitude = null;
	    	var longitude = null;
	    	var blId = null;
	    	var imagesArray = [];
	    	var image=null;
	    	var scale=null;
	    	var color=null;
	    	
	    	var min = parseFloat(this.minMax_record.getValue(this.fieldsArray[this.selectedMetric_index]+"_min"));
			var max = parseFloat(this.minMax_record.getValue(this.fieldsArray[this.selectedMetric_index]+"_max"));
			
			var fieldArrayIndxValue = this.fieldsArray[this.selectedMetric_index];
			
			if(max == min){
				max = (min >0) ? min*2 : 100;
			}
			var middle = (min+max)/2;
			
	    	for(var i=0;i<records.length;i++){
	    		var record = records[i]; 
	    		latitude = record.getValue('bl.lat');
	        	longitude = record.getValue('bl.lon');
	        	blId = record.getValue('bl.bl_id');
	        	customValue = record.getValue(fieldArrayIndxValue);
	        	
	        	if(valueExistsNotEmpty(customValue)){
	        		if(customValue < 1){
		        		scale= 1;
		        		color = "#f1f902";
		        	}else if(customValue > 1 && customValue<= middle){
		        		scale=2;
		        		color="#efaf00";
		        	}else if(customValue > middle && customValue <= max){
		        		scale=5;
		        		color="#f70c0c";
		        	}
	        	}
	        	
	        	// Create the item using the values
	        	image = new AmCharts.MapImage();
	        	image.zoomLevel=5;
	        	image.scale=scale;
	        	image.type="circle";
	        	image.color=color;
	        	image.longitude=longitude;
	        	image.latitude=latitude;
	        	image.title=blId;
	        	image.value=customValue;
	        	image.balloonText="[[title]]: [[value]]";
	        	
	        	imagesArray.push(image);
	        }
	    	
	       this.heatMap = AmCharts.makeChart("bldgsHeatMap", {
	            type: "map",
	            "colorSteps": 10,
	            "dataProvider": {
	                "map": "worldHigh",
	                "images": imagesArray
	            },
	            "balloonText": "[[title]]: [[value]]",
	            "areasSettings": {
	            			"autoZoom": true
	                  },
	            "smallMap": {},
	            "valueLegend": {
	                        "right": 10,
	                        "minValue": min,
	                        "maxValue": max
	                      },
	                      "responsive": {
	                          "enabled": true
	            }
	          });
	        
	    	this.panelBuildingsHtml.setContentPanel(Ext.get('bldgsHeatMap'));
	},
	
	createFieldsArray: function(){
        this.fieldsArray = new Array();
        this.fieldsArray.push('bl.area_estimated');
        this.fieldsArray.push('bl.area_gross_int');
        this.fieldsArray.push('bl.area_rentable');
        this.fieldsArray.push('bl.area_ls_negotiated');
        this.fieldsArray.push('bl.area_ocup');
        this.fieldsArray.push('bl.area_rm');
        this.fieldsArray.push('bl.area_usable');
        this.fieldsArray.push('bl.active_capital_cost');
        this.fieldsArray.push('bl.chargeable_cost');
        this.fieldsArray.push('bl.operating_costs');
        this.fieldsArray.push('bl.value_book');
        this.fieldsArray.push('bl.value_market');
        this.fieldsArray.push('bl.count_occup');
        this.fieldsArray.push('bl.count_em');
        this.fieldsArray.push('bl.count_max_occup');
        this.fieldsArray.push('bl.vacancy_percent');
        this.fieldsArray.push('bl.area_avg_em');
        this.fieldsArray.push('bl.cost_sqft');
        this.fieldsArray.push('bl.ratio_ur');
        this.fieldsArray.push('bl.fci');
        this.fieldsArray.push('bl.ratio_ru');
    },
	
	// disable multiple selection for grid panel
	tableMetricFields_bldgs_heatmap_multipleSelectionColumn_onClick: function(row){
		var selected = row.isSelected();
		this.tableMetricFields_bldgs_heatmap.setAllRowsSelected(false);
		row.select(selected);
		this.selectedMetric_index = row.getIndex();
	},
	
	tableMetricFields_bldgs_heatmap_onShowMetric: function(){
		this.drawMap();
	}
});

function initializeSelectMetricGrid(){
	var gridPanel = abTableMetricsBuildingsHeatMap_ctrl.tableMetricFields_bldgs_heatmap;
	gridPanel.enableSelectAll(false);
	gridPanel.rows[abTableMetricsBuildingsHeatMap_ctrl.selectedMetric_index].row.select(true);
}