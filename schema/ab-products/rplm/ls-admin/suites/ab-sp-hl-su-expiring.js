var abSpHlSuExpiring_Controller = View.createController('abSpHlSuExpiring_Controller', {
	fromDate: null,
	toDate: null,
	
	blId: null,
	flId: null,
	drawingController: null,
	
	//Statistic config object.
	gridFlds_statConfig: {
		formulas: ["sum"],
		fields: ["su.total_area_usable", "su.total_area_rentable", "su.total_occupancy"]
	},
	
    afterViewLoad: function(){
    	this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['su', 'rm','fl','bl'],
            reset: true,
            showNavigation: true,
            selectAssetHighlightColor: '#ffff00',
            highlightParameters: [{
                'view_file': "ab-sp-hl-su-expiring.axvw",
                'hs_ds': "abSpHlSuExpiring_ds_drawing_highlight",
                'label_ds': 'abSpHlSuExpiring_ds_drawing_Label1',
                'label_ht': '0.60',
                showSelector: true
            }],
            assetTooltip: [
                {assetType: 'su', datasource: 'abSpHlSuExpiring_ds_drawing_Label1', fields: 'su.su_id'}
            ],
            showPanelTitle: true,
            layersPopup: {
                layers: "su-assets;su-labels;rm-assets;rm-labels;gros-assets;gros-labels;background",
                defaultLayers: "su-assets;su-labels;rm-assets;gros-assets;background"
            	
            },
            listeners: {
            	onClickAssetAfter: function (drawingController, svgId, selectedAssetId, assetType, selectedAssets, selected) {
            		var arrayPkIDs = selectedAssetId.split(";");
            		var restriction = new Ab.view.Restriction();
                    restriction.addClause("su.bl_id", arrayPkIDs[0], "=", true);
                    restriction.addClause("su.fl_id", arrayPkIDs[1], "=", true);
                    restriction.addClause("su.su_id", arrayPkIDs[2], "=", true);
                    
                    if(assetType === 'su'){
	                    var suDetailPanel = View.panels.get('abSpHlSuExpiring_detailSu');
	                    suDetailPanel.refresh(restriction);
	                    suDetailPanel.show(true);
	                    suDetailPanel.showInWindow({
	                        width: 500,
	                        height: 250
	                    });
                    }
                    
                }
            }
            
        });
    	this.drawingController.setDrawingPanelTitle(getMessage('drawingPanelTitle1'));
    	
        //add Totals row to grid
        this.abSpHlSuExpiring_gridSu.setStatisticAttributes(this.gridFlds_statConfig);
    },
    
    afterInitialDataFetch: function(){
		clearConsole();
		/*
		 * 06/16/2010 IOAN 
		 * KB 3027996 refresh tree control with default console
		 */
		var console = View.panels.get('abSpHlSuExpiring_console');
		var fromDate = console.getFieldValue('from_date');
		var toDate = console.getFieldValue('to_date');
		var lsDates = "";
		if(fromDate){
			lsDates += " AND ls.date_end >= ${sql.date(\'" + fromDate + " \')}";
		}
		if(toDate){
			lsDates += " AND ls.date_end <= ${sql.date(\'" + toDate + " \')}";
		}
		if(lsDates.length > 0){
			this.abSpHlSuExpiring_treeBl.addParameter('lsDates', lsDates);
		}
		this.abSpHlSuExpiring_treeBl.refresh();
	},
	
	/**
     * Used when filter console is used, to unload the drawing
     * 
     * @param blId - building id of opened drawing
     * @param flId - floor id of opened drawing
     */
    unloadDrawing: function (blId, flId) {
        this.drawingController.unloadDrawing(blId, flId);
        var suDetailPanel = View.panels.get('abSpHlSuExpiring_detailSu');
        if(suDetailPanel){
        	suDetailPanel.show(false);
        	suDetailPanel.closeWindow();
        }
        this.drawingController.setDrawingPanelTitle(getMessage('drawingPanelTitle1'));
    },
	
	/**
     * Show drawing panel 
     * @param buildingId
     * @param floorId
     */
    showDrawing: function (blId, flId) {
    	var drawingPanelView = View.panels.get('drawingPanelView');
    	
    	var parameters = new Ab.view.ConfigObject();
        parameters['blId'] = blId;
        parameters['flId'] = flId;
        
        drawingPanelView.assetParameters = parameters;
        
        var drawing = {
              pkeyValues: {blId: parameters.blId, flId: parameters.flId}
        };
        this.drawingController.showDrawing(drawing);
        var title = String.format(getMessage('drawingPanelTitle2'), blId + "-" + flId);
        this.drawingController.setDrawingPanelTitle(title);
    },
	
    abSpHlSuExpiring_console_onShowTree: function(){
        var filterBlId = this.abSpHlSuExpiring_console.getFieldValue('su.bl_id');
        var filterFacilityTypeId = this.abSpHlSuExpiring_console.getFieldValue('su.facility_type_id');
		var lsDates = "";
        if(this.validateDates()){
			if(this.fromDate){
				lsDates += " AND ls.date_end >= ${sql.date(\'" + this.fromDate + " \')}";
			}
			if(this.toDate){
				lsDates += " AND ls.date_end <= ${sql.date(\'" + this.toDate + " \')}";
			}
		}
		if(lsDates.length > 0){
			this.abSpHlSuExpiring_treeBl.addParameter('lsDates', lsDates);
		}else{
			this.abSpHlSuExpiring_treeBl.addParameter('lsDates', " AND 1 = 1 ");
		}
		
        if (filterBlId) {
            this.abSpHlSuExpiring_treeBl.addParameter('blId', " = " + "'" + filterBlId + "'");
        }
        else {
            this.abSpHlSuExpiring_treeBl.addParameter('blId', "IS NOT NULL");
        }
        
        if (filterFacilityTypeId) {
            this.abSpHlSuExpiring_treeBl.addParameter('facilityTypeId', " = " + "'" + filterFacilityTypeId + "'");
        }
        else {
            this.abSpHlSuExpiring_treeBl.addParameter('facilityTypeId', "IS NOT NULL");
        }
        this.abSpHlSuExpiring_treeBl.refresh();
        this.abSpHlSuExpiring_gridSu.clear();
        
        this.unloadDrawing(this.blId, this.flId);
    },
	validateDates: function(){
		var console = View.panels.get('abSpHlSuExpiring_console');
		var ds = console.getDataSource();
		var fromDate = console.getFieldValue('from_date');
		var toDate = console.getFieldValue('to_date');
		if (fromDate && toDate) {
			var dtFrom = ds.parseValue('ls.date_end', fromDate, false);
			var dtTo = ds.parseValue('ls.date_end', toDate, false);
			if (dtFrom.getTime() > dtTo.getTime()) {
				View.showMessage(getMessage('errToDateSmallerFromDate'));
				return false;
			}
		}
		this.fromDate = fromDate;
		this.toDate = toDate;
		return true;
	}
});

/**
 * reset console to default values
 */
function clearConsole(){
	var toDate = new Date();
	var fromDate = toDate.add(Date.YEAR, -1);
	var console = View.panels.get('abSpHlSuExpiring_console');
	var ds = console.getDataSource();
	console.setFieldValue('from_date', ds.formatValue('ls.date_end', fromDate));
	console.setFieldValue('to_date', ds.formatValue('ls.date_end', toDate));
}

/**
 *  generate paginated report
 */
function generateReport(){
	var controller = View.controllers.get('abSpHlSuExpiring_Controller');
    var filterPanel = View.panels.get("abSpHlSuExpiring_console");
    var filterBlId = filterPanel.getFieldValue('su.bl_id');
    var filterFacilityTypeId = filterPanel.getFieldValue('su.facility_type_id');
	var parameters = {};
	var lsDates = "";
	if(controller.validateDates()){
		if(controller.fromDate){
			lsDates += " AND ls.date_end >= ${sql.date(\'" + controller.fromDate + " \')}";
		}
		if(controller.toDate){
			lsDates += " AND ls.date_end <= ${sql.date(\'" + controller.toDate + " \')}";
		}
	}
	if(lsDates.length > 0){
		parameters['lsDates'] =  lsDates;
	}else{
		parameters['lsDates'] =  " AND 1 = 1 ";
	}
    if (filterBlId) {
		parameters['blId'] = ' AND su.bl_id = \''+convert2validXMLValueAndLiteralizeValue(filterBlId)+'\' ';
    }else{
		parameters['blId'] = ' AND 1 = 1 ';
	}
    if (filterFacilityTypeId) {
		parameters['facilityTypeId'] = ' AND su.facility_type_id = \''+convert2validXMLValueAndLiteralizeValue(filterFacilityTypeId)+'\' ';
    }else{
		parameters['facilityTypeId'] = ' AND 1 = 1 ';
	}
	View.openPaginatedReportDialog('ab-sp-hl-su-expiring-prnt.axvw', null, parameters);
}

/**
 * event handler when click the floor level of the tree
 * @param {Object} ob
 */
function onFlTreeClick(ob){
	var controller = View.controllers.get('abSpHlSuExpiring_Controller');
    var currentNode = View.panels.get('abSpHlSuExpiring_treeBl').lastNodeClicked;
    var blId = currentNode.data['fl.bl_id'];
    var flId = currentNode.data['fl.fl_id'];
    var drawingName = currentNode.data['fl.dwgname'];
    
    var filterPanel = View.panels.get("abSpHlSuExpiring_console");
	var filterFacilityTypeId = filterPanel.getFieldValue('su.facility_type_id');

    var restriction = new Ab.view.Restriction();
    restriction.addClause("su.bl_id", blId, "=");
    restriction.addClause("su.fl_id", flId, "=");
	
    if (filterFacilityTypeId) {
        View.dataSources.get('abSpHlSuExpiring_ds_drawing_highlight').addParameter('facilityTypeId', "su.facility_type_id = '" + filterFacilityTypeId + "' ");
    }
    else {
        View.dataSources.get('abSpHlSuExpiring_ds_drawing_highlight').addParameter('facilityTypeId', " 1 = 1 ");
    }
	var lsDates = "";
	if(controller.fromDate){
		lsDates += " AND ls.date_end >= ${sql.date(\'" + controller.fromDate + " \')}";
		restriction.addClause("ls.date_end", controller.fromDate, ">=", 'AND', false);
	}
	if(controller.toDate){
		lsDates += " AND ls.date_end <= ${sql.date(\'" + controller.toDate + " \')}";
		restriction.addClause("ls.date_end", controller.toDate, "<=", 'AND', false);
	}
    if(lsDates.length > 0){
		View.dataSources.get('abSpHlSuExpiring_ds_drawing_highlight').addParameter('lsDates', lsDates);
	}else{
		View.dataSources.get('abSpHlSuExpiring_ds_drawing_highlight').addParameter('lsDates', " AND 1 = 1 ");
	}
    
    controller.showDrawing(blId, flId);
    
    controller.blId = blId;
    controller.flId = flId;
    
    if (filterFacilityTypeId) {
        restriction.addClause('su.facility_type_id', filterFacilityTypeId, '=', 'AND', true);
    }
	restriction.addClause("su.ls_id", "", "IS NOT NULL");
    View.panels.get('abSpHlSuExpiring_gridSu').refresh(restriction);
}
