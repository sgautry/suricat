var abSpHlSuVacant_Controller = View.createController('abSpHlSuVacant_Controller', {
	
	blId : null,
	flId: null, 
	drawingController: null,
	
	/**
	 * Declares instance of drawingController, defines parameters used to load the drawing, sets addOns, layers and tooltips for the drawing
	 */
    afterViewLoad: function(){
    	this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['su', 'rm','fl','bl'],
            reset: true,
            showNavigation: true,
            selectAssetHighlightColor: '#ffff00',
            highlightParameters: [{
                'view_file': "ab-sp-hl-su-vacant.axvw",
                'hs_ds': "abSpHlSuVacant_ds_drawing_highlight",
                'label_ds': 'abSpHlSuVacant_ds_drawing_Label1',
                'label_ht': '0.60'
            }],
            assetTooltip: [
                {assetType: 'su', datasource: 'abSpHlSuVacant_ds_drawing_Label1', fields: 'su.su_id'}
            ],
            showPanelTitle: true,
            showTooltip: true,
            layersPopup: {
            	layers: "su-assets;su-labels;rm-assets;rm-labels;gros-assets;gros-labels;background",
                defaultLayers: "su-assets;su-labels;rm-assets;gros-assets;background"
            	
            },
            listeners: {
            	onClickAssetAfter: function (drawingController, svgId, selectedAssetId, assetType, selectedAssets, selected) {
            		var arrayPkIDs = selectedAssetId.split(";");
                    var restriction = new Ab.view.Restriction();
                    restriction.addClause('su.bl_id', arrayPkIDs[0]);
            		restriction.addClause('su.fl_id', arrayPkIDs[1]);
            		restriction.addClause('su.su_id', arrayPkIDs[2]);
            		
            		if(assetType === 'su'){
	                    var suDetailPanel = View.panels.get('abSpHlSuVacant_detailSu');
	                    suDetailPanel.refresh(restriction);
	                    suDetailPanel.show(true);
	                    suDetailPanel.showInWindow({
	                        width: 500,
	                        height: 250
	                    });
            		}
                }
            }
        });
    	this.drawingController.setDrawingPanelTitle(getMessage('drawingPanelTitle1'));
    },
    
    /**
     * Show drawing panel 
     * @param buildingId
     * @param floorId
     */
    showDrawing: function (blId, flId) {
    	var drawingPanelView = View.panels.get('drawingPanelView');
    	var parameters = new Ab.view.ConfigObject();
        parameters['blId'] = blId;
        parameters['flId'] = flId;
        
        drawingPanelView.assetParameters = parameters;
        
        var drawing = {
              pkeyValues: {blId: parameters.blId, flId: parameters.flId}
         };
        this.drawingController.showDrawing(drawing);
        var title = String.format(getMessage('drawingPanelTitle2'), blId + "-" + flId);
        this.drawingController.setDrawingPanelTitle(title);
    },
    
    /**
     * Used when filter console is used, to unload the drawing
     * 
     * @param blId - building id of opened drawing
     * @param flId - floor id of opened drawing
     */
    unloadDrawing: function (blId, flId) {
        this.drawingController.unloadDrawing(blId, flId);
        var suDetailPanel = View.panels.get('abSpHlSuVacant_detailSu');
        if(suDetailPanel){
        	suDetailPanel.show(false);
        	suDetailPanel.closeWindow();
        }
        this.drawingController.setDrawingPanelTitle(getMessage('drawingPanelTitle1'));
    },
    
    abSpHlSuVacant_console_onShowTree: function(){
        var filterBlId = this.abSpHlSuVacant_console.getFieldValue('su.bl_id');
        var filterFacilityTypeId = this.abSpHlSuVacant_console.getFieldValue('su.facility_type_id');
        
        if (filterBlId) {
            this.abSpHlSuVacant_treeBl.addParameter('blId', " = " + "'" + filterBlId + "'");
        }
        else {
            this.abSpHlSuVacant_treeBl.addParameter('blId', "IS NOT NULL");
        }
        
        if (filterFacilityTypeId) {
            this.abSpHlSuVacant_treeBl.addParameter('facilityTypeId', " = " + "'" + filterFacilityTypeId + "'");
        }
        else {
            this.abSpHlSuVacant_treeBl.addParameter('facilityTypeId', "IS NOT NULL");
        }
        this.abSpHlSuVacant_treeBl.refresh();
        this.abSpHlSuVacant_gridSu.clear();
		
        this.unloadDrawing(this.blId, this.flId);
    }
});

/**
 *  generate paginated report
 */
function generateReport(){
    var filterPanel = View.panels.get("abSpHlSuVacant_console");
    var filterBlId = filterPanel.getFieldValue('su.bl_id');
    var filterFacilityTypeId = filterPanel.getFieldValue('su.facility_type_id');
	var parameters = {};
    if (filterBlId) {
		parameters['blId'] = ' AND su.bl_id = \''+convert2validXMLValueAndLiteralizeValue(filterBlId)+'\' ';
    }else{
		parameters['blId'] = ' AND 1 = 1 ';
	}
    if (filterFacilityTypeId) {
		parameters['facilityTypeId'] = ' AND su.facility_type_id = \''+convert2validXMLValueAndLiteralizeValue(filterFacilityTypeId)+'\' ';
    }else{
		parameters['facilityTypeId'] = ' AND 1 = 1 ';
	}
	View.openPaginatedReportDialog('ab-sp-hl-su-vacant-prnt.axvw', null, parameters);
}

/**
 * event handler when click the floor level of the tree
 * @param {Object} ob
 */
function onFlTreeClick(ob){
	var controller = View.controllers.get('abSpHlSuVacant_Controller');
    var currentNode = View.panels.get('abSpHlSuVacant_treeBl').lastNodeClicked;
    var blId = currentNode.parent.data['bl.bl_id'];
    var flId = currentNode.data['fl.fl_id'];
    var drawingName = currentNode.data['fl.dwgname'];
    
    var filterPanel = View.panels.get("abSpHlSuVacant_console");
	var filterFacilityTypeId = filterPanel.getFieldValue('su.facility_type_id');
	
    if (filterFacilityTypeId) {
        View.dataSources.get('abSpHlSuVacant_ds_drawing_highlight').addParameter('facilityTypeId', "su.facility_type_id = '" + filterFacilityTypeId + "' AND ");
    }
    else {
        View.dataSources.get('abSpHlSuVacant_ds_drawing_highlight').addParameter('facilityTypeId', "");
    }
    
    var restriction = new Ab.view.Restriction();
    restriction.addClause("su.bl_id", blId, "=");
    restriction.addClause("su.fl_id", flId, "=");
    
    controller.showDrawing(blId, flId);
    controller.blId = blId;
    controller.flId = flId;
    
    if (filterFacilityTypeId) {
        restriction.addClause('su.facility_type_id', filterFacilityTypeId, '=', 'AND', true);
    }
    restriction.addClause("su.dwgname", drawingName, "=");
    View.panels.get('abSpHlSuVacant_gridSu').refresh(restriction);
}
