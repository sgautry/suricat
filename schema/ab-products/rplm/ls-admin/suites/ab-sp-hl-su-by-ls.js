var abSpHlSuByLs_Controller = View.createController('abSpHlSuByLs_Controller', {
	filterBlId: null,
	filterLsId: null,
	
	blId: null,
	flId: null,
	drawingName: null,
	drawingController: null,
	
	//Statistic config object.
	gridFlds_statConfig: {
		formulas: ["sum"],
		fields: ["su.total_area_usable", "su.total_area_rentable", "su.total_occupancy"]
	},
	
	/**
	 * Declares instance of drawingController, defines parameters used to load the drawing, sets addOns, layers and tooltips for the drawing
	 */
    afterViewLoad: function () {
    	this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['su', 'rm','fl','bl'],
            reset: true,
            showNavigation: true,
            highlightParameters: [{
                'view_file': "ab-sp-hl-su-by-ls.axvw",
                'hs_ds': "abSpHlSuByLs_ds_drawing_highlight",
                'label_ds': 'abSpHlSuByLs_ds_drawing_Label1',
                'label_ht': '0.60',
                showSelector: true
            }],
            assetTooltip: [
                //{assetType: 'su', datasource: 'abSpHlSuByLs_ds_drawing_Label1', fields: 'su.su_id'}
                {assetType: 'su'}
            ],
            selectAssetHighlightColor: '#ffff00',
            showPanelTitle: true,
            layersPopup: {
            	//defaultLayers: "su-assets;su-labels;rm-assets;gros-assets;background",
            	defaultLayers: "su-assets;su-labels;gros-assets;background",
            	layers: "su-assets;su-labels;rm-assets;gros-assets;gros-labels;background"
            },
            listeners: {
            	onClickAssetAfter: function (drawingController, svgId, selectedAssetId, assetType, selectedAssets, selected) {
            		var arrayPkIDs = selectedAssetId.split(";");
            		
            		var restriction = new Ab.view.Restriction;
                    restriction.addClause('su.bl_id', arrayPkIDs[0]);
            		 restriction.addClause('su.fl_id', arrayPkIDs[1]);
            		 restriction.addClause('su.su_id', arrayPkIDs[2]);
                    
            		 if(assetType === 'su'){
            			 var suDetailPanel = View.panels.get('abSpHlSuByLs_detailSu');
                         suDetailPanel.refresh(restriction);
                         suDetailPanel.show(true);
                         suDetailPanel.showInWindow({
                             width: 500,
                             height: 250
                         });
            		 }
                    
                }
            }
            
        });
    	this.drawingController.setDrawingPanelTitle(getMessage('drawingPanelTitle1'));
    	
        this.abSpHlSuByLs_gridSu.setStatisticAttributes(this.gridFlds_statConfig);
    },
    
    /**
     * Used when filter console is used, to unload the drawing
     * 
     * @param blId - building id of opened drawing
     * @param flId - floor id of opened drawing
     */
    unloadDrawing: function (blId, flId) {
        this.drawingController.unloadDrawing(blId, flId);
        
        var suDetailPanel = View.panels.get('abSpHlSuByLs_detailSu');
        if(suDetailPanel){
        	suDetailPanel.show(false);
        	suDetailPanel.closeWindow();
        }
        this.drawingController.setDrawingPanelTitle(getMessage('drawingPanelTitle1'));
    },
    
    /**
     * Show drawing panel 
     * @param buildingId
     * @param floorId
     */
    showDrawing: function (blId, flId) {
    	var drawingPanelView = View.panels.get('drawingPanelView');
    	
    	var parameters = new Ab.view.ConfigObject();
        parameters['blId'] = blId;
        parameters['flId'] = flId;
        
        drawingPanelView.assetParameters = parameters;
        
        var drawing = {
              pkeyValues: {blId: parameters.blId, flId: parameters.flId}
        };
        this.drawingController.showDrawing(drawing);
        var title = String.format(getMessage('drawingPanelTitle2'), blId + "-" + flId);
        this.drawingController.setDrawingPanelTitle(title);
    },
	
    /**
     * Called when Show button from filter console is pressed
     */
    abSpHlSuByLs_console_onShowTree: function(){
        this.filterBlId = this.abSpHlSuByLs_console.getFieldValue('su.bl_id');
		this.filterLsId = this.abSpHlSuByLs_console.getFieldValue('su.ls_id');
		
        if (this.filterBlId) {
            this.abSpHlSuByLs_treeBl.addParameter('blId', " = " + "'" + this.filterBlId + "'");
        }
        else {
            this.abSpHlSuByLs_treeBl.addParameter('blId', "IS NOT NULL");
        }
        
        if (this.filterLsId) {
            this.abSpHlSuByLs_treeBl.addParameter('lsId', " = " + "'" + this.filterLsId + "'");
        }
        else {
            this.abSpHlSuByLs_treeBl.addParameter('lsId', "IS NOT NULL");
        }
        this.abSpHlSuByLs_treeBl.refresh();
        this.unloadDrawing(this.blId, this.flId);
        
        this.abSpHlSuByLs_gridSu.clear();
    }
    
});

/**
 *  generate paginated report
 */
function generateReport(){
    var filterPanel = View.panels.get("abSpHlSuByLs_console");
    var filterBlId = filterPanel.getFieldValue('su.bl_id');
    var filterLsId = filterPanel.getFieldValue('su.ls_id');
	var parameters = {};
    if (filterBlId) {
		parameters['blId'] = ' AND su.bl_id = \''+convert2validXMLValueAndLiteralizeValue(filterBlId)+'\' ';
    }else{
		parameters['blId'] = ' AND 1 = 1 ';
	}
    if (filterLsId) {
		parameters['lsId'] = ' AND su.ls_id = \''+convert2validXMLValueAndLiteralizeValue(filterLsId)+'\' ';
    }else{
		parameters['lsId'] = ' AND 1 = 1 ';
	}
	View.openPaginatedReportDialog('ab-sp-hl-su-by-ls-prnt.axvw', null, parameters);
}

/**
 * event handler when click the floor level of the tree
 * @param {Object} obj
 */
function onFlTreeClick(obj){
	var currentNode = View.panels.get('abSpHlSuByLs_treeBl').lastNodeClicked;
	onClickTreeNode(null, currentNode);
}
/**
 * event handler when click the room level of the tree
 * @param {Object} obj
 */
function onRmTreeClick(obj){
	var crtNode = View.panels.get('abSpHlSuByLs_treeBl').lastNodeClicked;
	var parentNode = crtNode.parent;
	onClickTreeNode(crtNode, parentNode);
}
/**
 * handle for click tree node event
 * if floor node was clicked parentNode is current node
 * if room node was clicked crtNode = room node and parentNode =  crt node parent
 * @param {Object} crtNode
 * @param {Object} parentNode
 */
function onClickTreeNode(crtNode, parentNode){
	var controller = View.controllers.get('abSpHlSuByLs_Controller');

    var blId = parentNode.parent.data['bl.bl_id'];
    var flId = parentNode.data['fl.fl_id'];
    var drawingName = parentNode.data['fl.dwgname'];
     
    var restriction = new Ab.view.Restriction();
    restriction.addClause("su.bl_id", blId, "=");
    restriction.addClause("su.fl_id", flId, "=");
	
	var filterLsId = null;
	if(crtNode){
		filterLsId = crtNode.data['rm.rm_id'];
	}else{
		filterLsId = controller.filterLsId;
	}
	
    if (filterLsId) {
        View.dataSources.get('abSpHlSuByLs_ds_drawing_highlight').addParameter('lsId', " = " + "'" + filterLsId + "'");
		restriction.addClause("su.ls_id", filterLsId, "=");
    }
    else {
        View.dataSources.get('abSpHlSuByLs_ds_drawing_highlight').addParameter('lsId', "IS NOT NULL");
		restriction.addClause("su.ls_id", "", "IS NOT NULL");
    }
    
    var dwgElement = document.getElementById(controller.drawingController.config.container);
    
    controller.showDrawing(blId, flId);
   
    controller.blId = blId;
    controller.flId = flId;
    
    View.panels.get('abSpHlSuByLs_gridSu').refresh(restriction);
}
