var abRepmAddeditSelectLeaseController = View.createController('abRepmAddeditSelectLeaseController', {
	displayMode: 'complete',
	
	leaseId: null,

	isDemoMode: false,

	afterViewLoad: function(){
        this.isDemoMode = isInDemoMode();
		this.abLeaseConsole_mainTabs.showTab('abLeaseConsole_detailTab', (this.displayMode == 'standard' || this.displayMode == 'basic'));
		this.abLeaseConsole_mainTabs.showTab('abLeaseConsole_completeTab', this.displayMode == 'complete');
		var displayModeTitle = getMessage('titleDisplayMode') + ' ' + getMessage('titleDisplayMode_' + this.displayMode);
		this.abRepmLeaseConsole_basic.actions.get('showMode').setTitle(displayModeTitle);
		
	},
	
	afterInitialDataFetch: function(){
		applyRestriction();		
	},
	
	/**
	 * Clear filter console handler.
	 */
	abRepmLeaseConsole_basic_onClear: function(panel, action){
		this.abRepmLeaseConsole_basic.clear();
		applyRestriction();		
	},
	
	/**
	 * Filter handler.
	 */
	abRepmLeaseConsole_basic_onFilter: function(panel, action) {
		applyRestriction();
	},
	
	loadLease: function(leaseId, withRefresh) {
		if (valueExists(withRefresh) && withRefresh) {
			// set and apply filter for selected lease
			this.abRepmLeaseConsole_basic_onClear(this.abRepmLeaseConsole_basic, this.abRepmLeaseConsole_basic.actions.get('clear'));
			this.abRepmLeaseConsole_basic.setFieldValue('ls.ls_id', leaseId);
			this.abRepmLeaseConsole_basic_onFilter(this.abRepmLeaseConsole_basic, this.abRepmLeaseConsole_basic.actions.get('filter'));
		}
		var context = {
				restriction: {'ls.ls_id': leaseId}
		};
		showLeaseDetails(context);
	},
	
	onChangeDisplayMode: function(newDisplayMode){
		if (this.displayMode != newDisplayMode) {
			this.displayMode = newDisplayMode;
			var displayModeTitle = getMessage('titleDisplayMode') + ' ' + getMessage('titleDisplayMode_' + this.displayMode);
			this.abRepmLeaseConsole_basic.actions.get('showMode').setTitle(displayModeTitle);
			
			if (valueExistsNotEmpty(this.leaseId)) {
				this.loadLease(this.leaseId);
			}
		}
	}
});

/**
 * Open lease console
 * @param ctx context
 */
function showLeaseDetails(ctx, navigateToTab){
	var restriction = null;
	var tabTitle = "";
	var lsId = null;
	if(navigateToTab == undefined ){
		navigateToTab = true;
	}
	if (valueExists(ctx.restriction)) {
		lsId = ctx.restriction['ls.ls_id'];
		tabTitle = getMessage("titleLeaseDetails").replace('{0}', lsId);
		restriction = new Ab.view.Restriction(ctx.restriction);
		var tabs = View.panels.get('abLeaseConsole_mainTabs');
		if (tabs) {
			var selLsController = View.controllers.get('abRepmAddeditSelectLeaseController');
			selLsController.leaseId = lsId;
			var displayMode = selLsController.displayMode;
			
			tabs.showTab('abLeaseConsole_detailTab', (displayMode == 'standard' || displayMode == 'basic'));
			tabs.showTab('abLeaseConsole_completeTab', displayMode == 'complete');
			var detailsTabName = null;
			var detailsTabControllerName = null;
			if (displayMode == 'complete') {
				detailsTabName = 'abLeaseConsole_completeTab';
				detailsTabControllerName = 'abLsConsoleCompleteController';
			}else{
				detailsTabName = 'abLeaseConsole_detailTab';
				detailsTabControllerName = 'abRepmAddeditLeaseDetailsController';
			}
			
			tabs.enableTab(detailsTabName, true);
			tabs.setTabTitle(detailsTabName, tabTitle);
			var detailsController = View.controllers.get(detailsTabControllerName);
			if (detailsController) {
				detailsController.loadDetailsForLease(lsId, displayMode);
			}
			if(navigateToTab){
				tabs.selectTab(detailsTabName, null, false, false, true);
				showLeaseProfile(ctx, false);
			}
		}
	}
}


function onDisplayModeStandard(){
	View.controllers.get('abRepmAddeditSelectLeaseController').onChangeDisplayMode('standard');
}

function onDisplayModeBasic(){
	View.controllers.get('abRepmAddeditSelectLeaseController').onChangeDisplayMode('basic');
}

function onDisplayModeComplete(){
	View.controllers.get('abRepmAddeditSelectLeaseController').onChangeDisplayMode('complete');
}

/**
 * Open lease profile tab
 * @param ctx command context
 */
function showLeaseProfile(ctx, navigateToTab){
	var lsId = null;
	var blId = null;
	var prId = null;
	var tabTitle = "";
	
	if(navigateToTab == undefined){
		navigateToTab = true;
	}
	if(valueExists(ctx.restriction)){
		lsId = ctx.restriction['ls.ls_id'];
		
		var parameters = {
				tableName: 'ls',
				fieldNames:toJSON(['ls.ls_id', 'ls.bl_id', 'ls.pr_id']),
				restriction: toJSON({'ls.ls_id':lsId})
			}
			var result = Workflow.call('AbCommonResources-getDataRecord', parameters);
			if (result.code == 'executed') {
				blId = result.dataSet.getValue('ls.bl_id');
				prId = result.dataSet.getValue('ls.pr_id');
			}else{
				Workflow.handleError(result);
			}
		
		
		tabTitle = getMessage("titleLeaseProfile").replace('{0}', lsId);
		var tabs = View.panels.get('abLeaseConsole_mainTabs');
		if (tabs){
			tabs.enableTab('abLeaseConsole_profileTab', true);
			tabs.setTabTitle('abLeaseConsole_profileTab', tabTitle);
			var profileController = View.controllers.get('repLeaseDetails');
			if (profileController) {
				profileController.ls_id = lsId;
				profileController.bl_id = blId;
				profileController.pr_id = prId;
				profileController.initializeView();
			}
			if(navigateToTab){
				tabs.selectTab('abLeaseConsole_profileTab', null, false, false, true);
				// initiallize details tab also
				showLeaseDetails(ctx, false);
			}
		}
	}
}

function applyRestriction(){
    var consolePanel = View.panels.get('abRepmLeaseConsole_basic');
    var gridPanel = View.panels.get('abRepmLeaseList');
    var lsId = consolePanel.getFieldValue('ls.ls_id');
    var contId = consolePanel.getFieldValue('ls.cont_id');
    var agencyName = consolePanel.getFieldValue('ar_ag.ag_name');
    var siteId = consolePanel.getFieldValue('bl.site_id');
	var shortBlId = consolePanel.getFieldValue('bl.short_bl_id');
    var flId = consolePanel.getFieldValue('fl.fl_id');
    var tnNameExt = consolePanel.getFieldValue('ls.tn_name_ext');
    var tnName = consolePanel.getFieldValue('ls.tn_name');
    var clId = consolePanel.getFieldValue('ar_client.cl_id');
    
    var restriction = "";
    var condition = " and";
    var condRest = false;
    
    if(lsId){
        if(condRest){
            restriction += condition;
        }   
        restriction += " ls.ls_id='"+lsId+"'";
        condRest = true;
    }
    if(contId){
        if(condRest){
            restriction += condition;
        }
        restriction += " ls.cont_id='"+contId+"'";
        condRest = true;
    }
	if(agencyName){
        if(condRest){
            restriction += condition;
        }
        restriction += " ls.ls_id in (select distinct ls_id from rm inner join bl on bl.bl_id=rm.bl_id inner join ar_ag on bl.ag_id=ar_ag.ag_id where ar_ag.ag_name='"+agencyName+"')";
        condRest = true;
    }
    if(siteId){
        if(condRest){
            restriction += condition;
        }
        restriction += " ls.ls_id in (select distinct ls_id from rm inner join bl on bl.bl_id=rm.bl_id where bl.site_id='"+siteId+"')";
        condRest = true;
    }        
    if(shortBlId){
        if(condRest){
            restriction += condition;
        }
        restriction += " ls.ls_id in (select distinct ls_id from rm inner join bl on bl.bl_id=rm.bl_id where bl.short_bl_id='"+shortBlId+"')";
        condRest = true;
    }
    if(flId){
        if(condRest){
            restriction += condition;
        }
        restriction += " ls.ls_id in (select distinct ls_id from rm inner join fl on fl.bl_id=rm.bl_id and fl.fl_id=rm.fl_id where fl.fl_id='"+flId+"')";
        condRest = true;
    }
    if(tnNameExt){
        if(condRest){
            restriction += condition;
        }
        restriction += " ls.tn_name_ext='"+tnNameExt+"'";
        condRest = true;
    }
    if(tnName){
        if(condRest){
            restriction += condition;
        }
        restriction += " ls.tn_name='"+tnName+"'";
        condRest = true;
    }
    if(clId){
        if(condRest){
            restriction += condition;
        }
        restriction += " (ls.tn_dv_id in (select dv.dv_id from dv where dv.cl_id='"+clId+"') or ls.ld_dv_id in (select dv.dv_id from dv where dv.cl_id='"+clId+"'))";
        condRest = true;
    }
    
    gridPanel.refresh(restriction);
}

function afterSelectFloor(fieldName, selectedValue, previousValue){
	if(fieldName=="bl.ag_id"){
		var consolePanel = View.panels.get('abRepmLeaseConsole_basic');
		var ds_ar_ag = View.dataSources.get('ds_ar_ag');
		var record=ds_ar_ag.getRecord("ag_id="+selectedValue);
		var ag_name=record.getValue('ar_ag.ag_name');
		consolePanel.setFieldValue('ar_ag.ag_name',ag_name);
	}
}

function beforeSelectFloor(command) {
	var consolePanel = View.panels.get('abRepmLeaseConsole_basic');
	var ag_name=consolePanel.getFieldValue('ar_ag.ag_name');
	var site_id=consolePanel.getFieldValue('bl.site_id');
	var short_bl_id=consolePanel.getFieldValue('bl.short_bl_id');
	var restriction="1=1";
	if(valueExistsNotEmpty(ag_name)){
		restriction=" bl.ag_id=(select ag_id from ar_ag where ar_ag.ag_name='"+ag_name+"')";
		if(valueExistsNotEmpty(site_id)){
			restriction+=" AND bl.site_id='"+site_id+"'";
		}
		if(valueExistsNotEmpty(short_bl_id)){
			restriction+=" AND bl.short_bl_id='"+short_bl_id+"'";
		}
	}else if(valueExistsNotEmpty(site_id)){
		restriction=" bl.site_id='"+site_id+"'";
		if(valueExistsNotEmpty(short_bl_id)){
			restriction+=" AND bl.short_bl_id='"+short_bl_id+"'";
		}
	}else if(valueExistsNotEmpty(short_bl_id)){
		restriction=" bl.short_bl_id='"+short_bl_id+"'";
	}
	if(valueExistsNotEmpty(restriction)){
		command.restriction = restriction;
	}
}