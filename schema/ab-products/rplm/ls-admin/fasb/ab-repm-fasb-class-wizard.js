/**
 * Controller definition.
 */
var abFasbClassWizardController = View.createController('abFasbClassWizardController', {

	isExtensionForLeaseAccountingLicensed: false,
	
	afterViewLoad: function(){
		/**
		 * Check if user has license for lease accounting extension.
		 */
		this.isExtensionForLeaseAccountingLicensed = hasLicenseForActivity('AbRPLMLeaseAccountingExtension');
	},
	
	afterInitialDataFetch: function(){
		if (this.isExtensionForLeaseAccountingLicensed) {

		} else {
			if (valueExists(View.getOpenerView().panels.get('viewContent'))) {
				View.getOpenerView().alert(getMessage('msgLeaseAccountingNoLicense'));
				View.getOpenerView().panels.get('viewContent').loadView('ab-blank.axvw');
			}else {
				alert(getMessage('msgLeaseAccountingNoLicense'));
				View.getOpenerView().loadView('ab-blank.axvw');
			}
		}
	}
})