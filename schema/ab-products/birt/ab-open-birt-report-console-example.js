/**
 * JS controller for a view that calls BIRT Reports opener view.
 * 
 */
View.createController('birtReportsViewExample', {
	
	afterViewLoad: function() {
		var protocol = window.location.protocol;
		var hostAndPort = window.location.host;	
		var app = 'archibus';
		var openerView = 'ab-open-birt-report.axvw';
		var loadParam = 'load=in-frame';
		var formatParam = 'outputType=pdf';
		var reportNameParam = 'file=ab-rm-datasource-example.rptdesign';
		var blParam = 'bl_id=JFK A';
		var url = encodeURI( protocol + '//' + hostAndPort + '/' + app + '/' + openerView + '?' + loadParam + '&' + formatParam + '&' + reportNameParam + '&' + blParam);
		window.location = url;
    }
});
