/**
 * JS controller for BIRT Reports opener view.
 * 
 */
View.createController('birtReportsOpenerInFrame', {
    
	fileName: '',
		
	reportContent: null,
	
	callBirtViewer: false,
	
	openInNewTab: false,
	
	format: 'PDF',
	
	dataSourceParameters: {},
	
	afterViewLoad: function() {
		this.reportContent = document.getElementById("birt_report_frame");

        for (var name in window.location.parameters) {
			var val = window.location.parameters[name];
        	if('file' == name.toLowerCase()){
        		this.fileName = val;
			} else if ('load' == name.toLowerCase()) {
				if ('in-viewer' == val.toLowerCase()) {
					this.callBirtViewer = true;
				} else 	if ('in-popup' == val.toLowerCase()) {
					this.openInNewTab = true;
				} 
        	} else if('outputType'.toLowerCase() == name.toLowerCase()){
				var fmt = val.toUpperCase();
				if ("HTML" === fmt) {
					this.format = fmt;
				} else if ("XLSX" === fmt) {
					this.format = "MS_EXCEL";
				} else if ("DOCX" === fmt) {
					this.format = "MS_DOC";
				} 
        	} else {
				val = this.formatStringParam(val);
				this.dataSourceParameters[name] = val;
			}
        }
		
        if(this.fileName!= null && this.fileName.length > 4){
        	this.executeBirtReport();
		} else {
        	this.reportContent.innerHTML = "<br><br><span align='center' height='50px'><font color='red'>" + getMessage('errorMessage') + "</font></span>";
		}
	},
	
	formatStringParam: function (str) {
		var val = str.replace(/^'/, "");
		val = val.replace(/'$/, "");
		val = val.replace(/^"/, "");
		val = val.replace(/"$/, "");

		return val;
	},
	
    executeBirtReport: function () {
    	setTimeout(this.setWaitMessage(), 500);

		var iFrame = document.getElementById('birt_report_frame');

		if (this.callBirtViewer){
			// default - BIRT viewer runs in the same app server
			var viewerHost = window.location.host;
			// check WebApp Param - BIRT viewer runs in different app server
			var viewerHostParam = View.activityParameters['AbCommonResources-birtViewerApplicationServer'];
			if (viewerHostParam && "/#Attribute%//@webAppDirectory%" !== viewerHostParam){
				viewerHost = viewerHostParam;
			}

			var birtReportParams = "";
			if (Object.keys(this.dataSourceParameters).length === 0 && this.dataSourceParameters.constructor === Object) {
				// no parameters
			} else {
				for (x in this.dataSourceParameters) {
					birtReportParams += ("&" + x + "=" + this.dataSourceParameters[x]);
				} 
			}
			
			var url = encodeURI(window.location.protocol + '//' + viewerHost + '/birt-viewer/frameset?__report=' + this.fileName + birtReportParams);
			if (this.openInNewTab){
				window.open(url, "width=800, height=600, resizable=yes");
			} else {
				iFrame.src = url;
			}
		} else {
			var result = Workflow.callMethod('AbCommonResources-BirtService', this.fileName, this.dataSourceParameters, this.format);
			if (result.code == 'executed') {
				var host = View.contextPath;
				var fileTransfer = host + "/projects/users/" + View.user.name.toLowerCase() + "/" + result.message;
				if (this.openInNewTab) {
		            window.open(fileTransfer, "width=800, height=600, resizable=yes");
				} else {
					iFrame.src = fileTransfer;
				}
			} else {
				AFM.workflow.Workflow.handleError(result);
				alert("Error");
			}
		}
    },

	
	setWaitMessage: function(){
		this.reportContent.innerHTML = "<span align='center' height='50px'><font color='red'>" + getMessage('waitMessage') + "</font></span>";
	}
});