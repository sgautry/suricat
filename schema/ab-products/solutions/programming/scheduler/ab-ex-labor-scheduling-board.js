/**
 * The example controller.
 */
View.createController('laborSchedulingBoardExample', {

    /**
     * Initialize the timeline control.
     */
	afterInitialDataFetch: function() {
		View.createControl({
		    control: 'LaborBoardControl',
		    container: 'laborBoardContainer',
		    panel: 'laborBoard',
		    resourcesDataSource: 'resourcesDataSource',
		    scheduledWorkDataSource: 'scheduledWorkDataSource',
		    listeners: {
		    }
		});

		var dataSource = this.workRequestsDataSource;
		var workRequestRecords = dataSource.getRecords();
		var workRequests = _.map(workRequestRecords, function(record) {
		    return {
                wr_id: record.getValue('wr.wr_id'),
		        description: record.getValue('wr.description'),
                status: record.getValue('wr.status'),
                date_escalation_completion: dataSource.formatValue('wr.date_escalation_completion', record.getValue('wr.date_escalation_completion')),
                date_assigned: dataSource.formatValue('wr.date_assigned', record.getValue('wr.date_assigned')),
                priority_label: record.getValue('wr.priority_label')
            };
        });

        var template = View.templates.get('workRequestsTemplate');
        template.render({ workRequests: workRequests }, this.workRequestsList.parentElement);
	}
});

/**
 * The Labor Board control implements https://confluence.archibus.zone/display/PLAT/HTML+Scheduling+Control.
 */
LaborBoardControl = Base.extend({

    /**
     * The configuration object.
     */
    config: null,

    /**
     * The timeline control instance.
     */
    timeline: null,

    // PUBLIC API

    /**
     * Control constructor.
     * @param {config} The control configuration.
     */
    constructor: function(config) {
        this.config = config;

        // Load the timeline.
        var weekStart = new Date(2012, 3, 9);
        this.loadSchedule(null, weekStart);
    },

    /**
     * Sets the control height to specified value.
     * @param {height} The height in pixels.
     */
    setHeight: function(height) {},

    /**
     * Adds a listener to specified control event.
     * @param {eventName} The name of the control event, e.g. 'click' or 'locateBuilding'.
     * @param {listener} The listener function reference.
     */
    addEventListener: function(eventName, listener) {},

    /**
     * Removes listeners from specified control event.
     * @param {eventName} The name of the control event, e.g. 'click' or 'locateBuilding'.
     * @param {listener} The listener function reference. Optional. If not specified, the control removes all event listeners for this event.
     */
    removeEventListener: function(eventName) {},

    // IMPLEMENTATION

    /**
     * Loads the schedule for specified list of resources (e.g. craftspeople) and for specified week.
     * Triggers the afterRefresh event.
     *
     * @param {resourceRestriction} Ab.data.Restriction to restrict the resources data source, or null.
     * @param {weekStart} Date of the week start.
     */
    loadSchedule: function(resourceRestriction, weekStart) {

        // Load the resources.
        var resourceRecords = View.dataSources.get(this.config.resourcesDataSource).getRecords(resourceRestriction);
        var resourcesData = [];
        _.each(resourceRecords, function(record) {
            var color = '#337AB7';
            var cfId = record.getValue('cf.cf_id');
            var name = record.getValue('cf.name');
            if (cfId === 'CHRISTIAN RIDDER') {
                color = '#C91E37';
            }
            resourcesData.push({
                id: cfId,
                text: name,
                color: color
            });
        });

        // Load scheduled work items.
        var scheduledWorkDataSource = View.dataSources.get(this.config.scheduledWorkDataSource);
        var scheduledWorkRecords = scheduledWorkDataSource.getRecords(resourceRestriction);
        var scheduledWork = [];
        _.each(scheduledWorkRecords, function(record) {
            var cfId = record.getValue('wrcf.cf_id');
            var startDate = record.getValue('wrcf.date_start');
            var startTime = record.getValue('wrcf.time_start');
            var endDate = record.getValue('wrcf.date_end');
            var endTime = record.getValue('wrcf.time_end');
            if (startTime !== '' && endTime !== '') {
                scheduledWork.push({
                    cfId: cfId,
                    startDate: new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(),
                        startTime.getHours(), startTime.getMinutes(), startTime.getSeconds()),
                    endDate: new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate(),
                        endTime.getHours(), endTime.getMinutes(), endTime.getSeconds())
                });
                scheduledWork.push({
                    cfId: cfId,
                    startDate: new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(),
                        startTime.getHours() + 4, startTime.getMinutes(), startTime.getSeconds()),
                    endDate: new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate(),
                        endTime.getHours() + 4, endTime.getMinutes(), endTime.getSeconds())
                });
                scheduledWork.push({
                    cfId: cfId,
                    startDate: new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate() + 1,
                        startTime.getHours() + 4, startTime.getMinutes(), startTime.getSeconds()),
                    endDate: new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate() + 1,
                        endTime.getHours() + 4, endTime.getMinutes(), endTime.getSeconds())
                });
                scheduledWork.push({
                    cfId: cfId,
                    startDate: new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate() + 2,
                        startTime.getHours(), startTime.getMinutes(), startTime.getSeconds()),
                    endDate: new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate() + 2,
                        endTime.getHours(), endTime.getMinutes(), endTime.getSeconds())
                });

                if (cfId === 'DAVID VARRISON') {
                    scheduledWork.push({
                        cfId: 'CHRISTIAN RIDDER',
                        startDate: new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(),
                            startTime.getHours() + 4, startTime.getMinutes(), startTime.getSeconds()),
                        endDate: new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate(),
                            endTime.getHours() + 4, endTime.getMinutes(), endTime.getSeconds())
                    });
                    scheduledWork.push({
                        cfId: 'CHRISTIAN RIDDER',
                        startDate: new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate() + 2,
                            startTime.getHours(), startTime.getMinutes(), startTime.getSeconds()),
                        endDate: new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate() + 2,
                            endTime.getHours(), endTime.getMinutes(), endTime.getSeconds())
                    });
                    scheduledWork.push({
                        cfId: 'ANNA PETERS',
                        startDate: new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(),
                            startTime.getHours(), startTime.getMinutes(), startTime.getSeconds()),
                        endDate: new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate(),
                            endTime.getHours() + 4, endTime.getMinutes(), endTime.getSeconds())
                    });
                    scheduledWork.push({
                        cfId: 'ANNA PETERS',
                        startDate: new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate() + 1,
                            startTime.getHours() + 4, startTime.getMinutes(), startTime.getSeconds()),
                        endDate: new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate() + 1,
                            endTime.getHours() + 2, endTime.getMinutes(), endTime.getSeconds())
                    });
                    scheduledWork.push({
                        cfId: 'ANNA PETERS',
                        startDate: new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate() + 1,
                            startTime.getHours() + 5, startTime.getMinutes(), startTime.getSeconds()),
                        endDate: new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate() + 1,
                            endTime.getHours() + 3, endTime.getMinutes(), endTime.getSeconds())
                    });
                }
            }
        });

        var timeline = jQuery("#laborBoardContainer").dxScheduler({
            dataSource: scheduledWork,
            useDropDownViewSwitcher: true,
            views: ["timelineWorkWeek"],
            currentView: "timelineWorkWeek",
            currentDate: weekStart,
            firstDayOfWeek: 0,
            startDayHour: 8,
            endDayHour: 18,
            cellDuration: 60,
            width: "100%",
            height: "calc(100% - 10px)",
            groups: ["cfId"],
            resources: [{
                field: "cfId",
                dataSource: resourcesData,
                label: "Craftsperson",
                useColorAsDefault: true
            }],
            crossScrollingEnabled: true,

            // display the secondary time scale (hours) as vertical text
            timeCellTemplate: function (itemData, itemIndex, itemElement) {
                var text = itemData.text.replace('AM', '').replace('PM', '');

                itemElement.parent().height(50);
                jQuery("<div>").text(text).css("transform", "rotate(-90deg)").appendTo(itemElement);

                if (itemData.date.getHours() === 8) {
                    itemElement.addClass('first-hour');
                }
            },

            // add unscheduled hours to each resource row
            resourceCellTemplate: function(cellData, index, container) {
                container.append(jQuery('<div>').text(cellData.text));
                container.append(jQuery('<div>').addClass('resource-totals').text('32 total'));

                var rowEl = jQuery('.dx-scheduler-date-table').find('tr:nth-child(' + index + ')');
                addUnscheduledHours(rowEl, index, 0, 1000, 'white');
                if (index % 7 == 1) {
                    addUnscheduledHours(rowEl, index, 0, 600, '#D3E0F3', 'Wed', 6);
                    addUnscheduledHours(rowEl, index, 600, 400, '#A8C2E8', 'Fri', 5);
                } else if (index % 7 == 2) {
                    addUnscheduledHours(rowEl, index, 0, 600, '#D3E0F3', 'Wed', 2);
                } else if (index % 7 == 4) {
                    addUnscheduledHours(rowEl, index, 0, 400, '#A8C2E8', 'Tue', 8);
                }
            },

            // display unavailable hours as diagonal stripes
            dataCellTemplate: function(cellData, index, container) {
                var cfId = cellData.groups.cfId;

                if (isFriday(cellData.startDate)) {
                    if  (cfId === 'ANNA PETERS') {
                        container.addClass('unavailable-hours');
                    }
                }

                if (cellData.startDate.getHours() === 8) {
                    container.addClass('first-hour');
                }

                container.on('dragover', function(event) {
                    event.preventDefault();
                });
                container.on('drop', function(event) {
                    event.preventDefault();
                    var wr_id = event.originalEvent.dataTransfer.getData("text");

                    timeline.addAppointment({
                        cfId: cfId,
                        startDate: cellData.startDate,
                        endDate: cellData.startDate
                    });
                });

                return jQuery('<div>').text(cellData.text);
            },
        }).dxScheduler("instance");

        // placeholder for inter-scheduler drag and drop
        jQuery('.dx-scheduler-date-table-cell').on('dxdrop', function(event) {
        });

        jQuery('.dx-scheduler-view-switcher').hide();
        jQuery('.dx-scheduler-view-switcher-label').hide();

        function isFriday(date) {
            var day = date.getDay();
            return day === 5;
        }

        function addUnscheduledHours(rowEl, rowIndex, x, width, backgroundColor, day, unscheduledHours) {
            var unscheduledHoursPanel = document.createElement('div');
            unscheduledHoursPanel.className = 'unscheduled-hours-panel';
            unscheduledHoursPanel.style.top = 75 * (rowIndex + 1) - 100 + 'px';
            unscheduledHoursPanel.style.left = x + 'px';
            unscheduledHoursPanel.style.width = width + 'px';
            unscheduledHoursPanel.style.backgroundColor = backgroundColor;

            if (day) {
                var unscheduledHoursLabel = document.createElement('span');
                unscheduledHoursLabel.className = 'unscheduled-hours-label';
                unscheduledHoursLabel.appendChild(document.createTextNode('Unscheduled hours due ' + day + ':'));

                var unscheduledHoursIcon = document.createElement('span');
                unscheduledHoursIcon.className = 'unscheduled-hours-icon';
                unscheduledHoursIcon.appendChild(document.createTextNode(unscheduledHours));

                unscheduledHoursPanel.append(unscheduledHoursIcon);
                unscheduledHoursPanel.append(unscheduledHoursLabel);
            }

            rowEl.append(unscheduledHoursPanel);
        }
    }
});


