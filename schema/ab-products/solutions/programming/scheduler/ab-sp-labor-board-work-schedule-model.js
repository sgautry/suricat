/**
 * The Labor Board control implements https://confluence.archibus.zone/display/PLAT/HTML+Scheduling+Control.
 */
WorkScheduleModel = Base.extend({

    /**
     * The configuration object.
     */
    config: null,

    /**
     * The array of craftspersons displayed in the scheduler.
     */
    craftspersons: null,

    /**
     * The map containing the same craftspersons, keyed by ID, for fast access.
     */
    craftspersonsById: null,

    /**
     * The map of all craftsperson schedules, keyed by schedule ID.
     */
    craftspersonSchedules: null,

    /**
     * The array of all craftsperson schedule variances.
     */
    craftspersonScheduleVariances: null,

    /**
     * The array of scheduled tasks displayed for the date range in the scheduler.
     */
    scheduledTasks: null,

    /**
     * The array of assigned unscheduled tasks for the date range displayed in the scheduler.
     */
    assignedUnscheduledTasks: null,

    /**
     * Constructor.
     * @param {config} The control configuration.
     */
    constructor: function(config) {
        this.config = config;
        this.craftspersons = [];
        this.scheduledTasks = [];
        this.assignedUnscheduledTasks = [];
        this.craftspersonSchedules = {};
        this.craftspersonScheduleVariances = [];
    },

    /**
     * Loads the schedule for specified list of resources.
     * Triggers the afterRefresh event.
     *
     * @param {restriction} Ab.data.Restriction to restrict the craftspersons data source, or null.
     * @param dateStart The first date of the date range.
     * @param dateEnd The last date of the date range.
     */
    loadSchedule: function(restriction, startDate, endDate) {
        this.craftspersonsById = {};
        this.craftspersons.length = 0;
        var craftspersons = this.craftspersons;

        var model = this;
        var records = View.dataSources.get('craftspersonsDataSource').getRecords(restriction);
        _.each(records, function(record) {
            var color = '#337AB7';
            var cfId = record.getValue('cf.cf_id');
            var name = record.getValue('cf.name');
            var trId = record.getValue('cf.tr_id');

            if (cfId === 'CHRISTIAN RIDDER') {
                color = '#C91E37';
            }

            var craftsperson = {
                id: cfId,
                text: name,
                trade: trId,
                color: color,
                totals: {
                    availableHours: 0,
                    scheduledHours: 0,
                    assignedUnscheduledHours: 0,
                    proratedUnscheduledHours: 0,
                    remainingHours: 0,
                    percentAllocated: 0
                },
                unscheduledTasks: new CraftspersonUnscheduledTasks()
            }
            model.craftspersons.push(craftsperson);
            model.craftspersonsById[cfId] = craftsperson;
        });

        this.scheduledTasks.length = 0;
        this.assignedUnscheduledTasks.length = 0;
        this._loadTasks(startDate, endDate, true, false, this.scheduledTasks);
        this._loadTasks(startDate, endDate, false, true, this.assignedUnscheduledTasks);

        this._calculateCraftspersonTotals(startDate, endDate);
    },

    /**
     * Creates a new scheduled task in the database.
     * @param task
     */
    createTask: function(task) {
        var updateTaskDataSource = View.dataSources.get('updateTaskDataSource');
        var record = new Ab.data.Record();
        record.setValue('wrcf.cf_id', task.cfId);
        record.setValue('wrcf.wr_id', task.wrId);
        record.setValue('wrcf.date_assigned', task.startDate);
        record.setValue('wrcf.time_assigned', task.startDate);
        record.setValue('wrcf.hours_est', hoursBetween(task.startDate, task.endDate));
        updateTaskDataSource.saveRecord(record);
    },

    /**
     * Updates an existing scheduled task in the database.
     * @param task
     */
    updateTask: function(task, oldTask) {
        var updateTaskDataSource = View.dataSources.get('updateTaskDataSource');
        var record = new Ab.data.Record();
        record.isNew = false;
        record.setValue('wrcf.cf_id', task.cfId);
        record.setValue('wrcf.wr_id', task.wrId);
        record.setValue('wrcf.date_assigned', task.startDate);
        record.setValue('wrcf.time_assigned', task.startDate);
        record.setValue('wrcf.hours_est', hoursBetween(task.startDate, task.endDate));
        record.setOldValue('wrcf.cf_id', oldTask.cfId);
        record.setOldValue('wrcf.wr_id', oldTask.wrId);
        record.setOldValue('wrcf.date_assigned', oldTask.startDate);
        record.setOldValue('wrcf.time_assigned', oldTask.startDate);
        updateTaskDataSource.saveRecord(record);
    },

    /**
     * Loads tasks within specified date range.
     *
     * @param dateStart The first date of the date range.
     * @param dateEnd The last date of the date range.
     * @param includeScheduledTasks True to load scheduled tasks.
     * @param includeAssignedUnscheduledTasks True to load assigned unscheduled tasks.
     * @param tasks The array of tasks to populate.
     * @private
     */
    _loadTasks: function(startDate, endDate, includeScheduledTasks, includeAssignedUnscheduledTasks, tasks) {
        var tasksDataSource = View.dataSources.get('tasksDataSource');
        var startDateParameter = tasksDataSource.formatValue('wrcf.date_assigned', startDate, false);
        var endDateParameter = tasksDataSource.formatValue('wrcf.date_assigned', endDate, false);

        tasksDataSource.addParameter('includeScheduledHours', includeScheduledTasks);
        tasksDataSource.addParameter('includeAssignedUnscheduledHours', includeAssignedUnscheduledTasks);
        tasksDataSource.addParameter('dateStart', startDateParameter);
        tasksDataSource.addParameter('dateEnd', endDateParameter);
        var records = tasksDataSource.getRecords();

        var model = this;
        var numberOfScheduledJobsPerPersonPerDay = {};

        _.each(records, function(record) {
            var cfId = record.getValue('wrcf.cf_id');
            var wrId = record.getValue('wrcf.wr_id');
            var description = record.getValue('wr.description');
            var startDate = record.getValue('wrcf.date_assigned');
            var startTime = record.getValue('wrcf.time_assigned');
            var dueDate = record.getValue('wr.date_escalation_completion');
            var estimatedHours = Number(record.getValue('wrcf.hours_est'));

            var craftsperson = model.craftspersonsById[cfId];

            var key = cfId + startDate.toString();
            var numberOfScheduledJobs = numberOfScheduledJobsPerPersonPerDay[key];
            if (numberOfScheduledJobs) {
                numberOfScheduledJobs += 1;
            } else {
                numberOfScheduledJobs = 1;
            }
            numberOfScheduledJobsPerPersonPerDay[key] = numberOfScheduledJobs;

            if (numberOfScheduledJobs <= model.config.maxScheduledJobsPerPersonPerDay) {
                var task = {
                    cfId: cfId,
                    wrId: wrId,
                    description: description,
                    startDate: new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(),
                        startTime.getHours(), 0, 0),
                    endDate: new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(),
                        startTime.getHours() + estimatedHours, 0, 0),
                    dueDate: dueDate,
                    estimatedHours: estimatedHours
                };

                tasks.push(task);

                if (includeAssignedUnscheduledTasks) {
                    craftsperson.unscheduledTasks.addTask(task);
                }
            }

            if (includeScheduledTasks) {
                craftsperson.totals.scheduledHours += estimatedHours;
            }
            if (includeAssignedUnscheduledTasks) {
                craftsperson.totals.assignedUnscheduledHours += estimatedHours;
            }
        });
    },

    /**
     * Loads craftsperson schedules and calculates totals per craftsperson.
     * See https://confluence.archibus.zone/display/AP/Specification+-+Labor+Scheduling+Board,
     * section "4.1.3 Craftsperson Rows - Statistics".
     * @private
     */
    _calculateCraftspersonTotals: function(startDate, endDate) {
        var model = this;

        model.craftspersonSchedules = {};
        var scheduleRecords = View.dataSources.get('craftspersonSchedulesDataSource').getRecords();
        _.each(scheduleRecords, function(record) {
            var schedule = new CraftspersonSchedule(record);
            model.craftspersonSchedules[schedule.id] = schedule;
        });

        var dayRecords = View.dataSources.get('craftspersonScheduleDaysDataSource').getRecords();
        _.each(dayRecords, function(record) {
            var scheduleId = record.getValue('cf_schedules_days.cf_schedule_id');
            var schedule = model.craftspersonSchedules[scheduleId];
            if (schedule) {
                schedule.addDay(new CraftspersonScheduleDay(record));
            }
        });

        model.craftspersonScheduleVariances = [];
        var exceptionRecords = View.dataSources.get('craftspersonScheduleVariancesDataSource').getRecords();
        _.each(exceptionRecords, function(record) {
            model.craftspersonScheduleVariances.push(new CraftspersonScheduleVariance(record));
        });

        // for each craftsperson and day within the visible range:
        // - find the applicable schedule
        // - calculate the number of available hours
        // - adjust hours per schedule details
        // - calculate totals

        // for each craftsperson
        _.each(this.craftspersons, function(craftsperson) {
            craftsperson.totals.availableHours = 0;

            // for each day within the visible range
            for (var date = new Date(startDate.getTime()); date.between(startDate, endDate); date = date.add(Date.DAY, 1)) {

                var schedule = model.findApplicableSchedule(craftsperson, date);
                if (schedule) {
                    var scheduleExceptions = _.filter(model.scheduleExceptions, function(scheduleException) {
                        return scheduleException.appliesTo(craftsperson.id, date);
                    });

                    craftsperson.totals.availableHours += schedule.getAvailableHours(date, scheduleExceptions);
                }
            }

            craftsperson.totals.proratedUnscheduledHours = craftsperson.unscheduledTasks.prorateTasks(startDate, endDate);

            craftsperson.totals.remainingHours = craftsperson.totals.availableHours -
                (craftsperson.totals.scheduledHours + craftsperson.totals.assignedUnscheduleHours + craftsperson.totals.proratedUnscheduledHours);

            craftsperson.totals.percentAllocated =
                (craftsperson.totals.availableHours - craftsperson.totals.remainingHours) / craftsperson.totals.availableHours;
        });
    },

    /**
     * Finds a schedule that applies to specified craftsperson on specified date.
     * @param craftsperson The craftsperson record.
     * @param date The date.
     */
    findApplicableSchedule: function(craftsperson, date) {
        var schedule = null;

        var applicableSeasonalSchedule = null;
        var applicableNonSeasonalSchedule = null;

        _.values(this.craftspersonSchedules, function(schedule) {
            if (schedule.appliesTo(craftsperson.id, date)) {
                if (schedule.isSeasonal) {
                    applicableSeasonalSchedule = schedule;
                } else {
                    applicableNonSeasonalSchedule = schedule;
                }
            }
        });

        if (applicableNonSeasonalSchedule) {
            schedule = applicableNonSeasonalSchedule;
        } else if (applicableSeasonalSchedule) {
            schedule = applicableSeasonalSchedule;
        }

        // TODO: remove!
        if (!schedule) {
            schedule = this.craftspersonSchedules[1];
        }

        return schedule;
    }
});

/**
 * Encapsulates information about a craftsperson schedule for a specific day of the week.
 */
CraftspersonScheduleDay = Base.extend({

    /**
     * 'MONDAY', 'TUESDAY', and so on.
     */
    dayOfWeek: '',

    /**
     * Start time.
     */
    startTime: null,

    /**
     * End time.
     */
    endTime: null,

    /**
     * 'STANDARD', 'OVERTIME', or 'DOUBLETIME'
     */
    rateType: '',

    /**
     * Constructor.
     */
    constructor: function(record) {
        this.dayOfWeek = record.getValue('cf_schedules_days.day_of_week');
        this.startTime = record.getValue('cf_schedules_days.time_start');
        this.endTime = record.getValue('cf_schedules_days.time_end');
        this.rateType = record.getValue('cf_schedules_days.rate_type');
    },

    /**
     * Returns the number of available hours.
     */
    getAvailableHours: function() {
        return this.endTime.getHours() - this.startTime.getHours();
    }
});

/**
 * Encapsulates information about a craftsperson schedule variance.
 */
CraftspersonScheduleVariance = Base.extend({

    /**
     * The craftsperson ID.
     */
    cfId: '',

    /**
     * Date when the exception starts.
     */
    startDate: null,

    /**
     * Date when the exception ends.
     */
    endDate: null,

    /**
     * Start time.
     */
    startTime: null,

    /**
     * End time.
     */
    endTime: null,

    /**
     * 'STANDARD', 'BUSY', or 'PERSONAL'
     */
    varianceType: '',

    /**
     * Constructor.
     */
    constructor: function(record) {
        this.cfId = record.getValue('cf_schedules_variances.cf_id');
        this.startDate = record.getValue('cf_schedules_variances.date_start');
        this.endDate = record.getValue('cf_schedules_variances.date_end');
        this.startTime = record.getValue('cf_schedules_variances.time_start');
        this.endTime = record.getValue('cf_schedules_variances.time_end');
        this.varianceType = record.getValue('cf_schedules_variances.sched_var_type');
    },

    /**
     * Returns true if the schedule is applicable for a specific craftsperson and date.
     * @param cfId
     * @param date
     */
    appliesTo: function(cfId, date) {
        var result = false;

        if (this.cfId === cfId && date.between(this.startDate, this.endDate)) {
            result = this;
        }

        return result;
    },

    /**
     * Returns the number of available hours.
     */
    getHours: function() {
        return this.endTime.getHours() - this.startTime.getHours();
    }
});

/**
 * Encapsulates information about a single craftsperson schedule.
 * A schedule is defined as a set of records in the cf_schedule table.
 */
CraftspersonSchedule = Base.extend({

    /**
     * The unique schedule ID.
     */
    id: '',

    /**
     * The craftsperson ID.
     */
    cfId: '',

    /**
     * Schedule type: CUSTOM SCHEDULE or SHIFT DEFINITION.
     */
    type: '',

    /**
     * Schedule name.
     */
    name: '',

    /**
     * Date when the schedule starts. If null, the schedule is open-started.
     */
    startDate: null,

    /**
     * Date when the schedule ends. If null, the schedule is open-ended.
     */
    endDate: null,

    /**
     * True if the schedule is seasonal.
     */
    isSeasonal: false,

    /**
     * Array of CraftspersonScheduleDay objects. One object defines a schedule for a specific day and pay rate.
     */
    days: null,

    /**
     * Array of CraftspersonScheduleVariance objects. One object defines an exception for a time interval.
     */
    exceptions: null,

    /**
     * Constructor.
     */
    constructor: function(record) {
        this.id = record.getValue('cf_schedules_assign.cf_schedule_id');
        this.cfId = record.getValue('cf_schedules_assign.cf_id');
        this.startDate = record.getValue('cf_schedules_assign.date_start');
        if (!this.startDate) {
            this.startDate = new Date(1, 1);
        }
        this.endDate = record.getValue('cf_schedules_assign.date_end');
        if (!this.endDate) {
            this.endDate = new Date(2999, 1);
        }
        this.isSeasonal = record.getValue('cf_schedules_assign.is_seasonal');
        this.type = record.getValue('cf_schedules.schedule_type');
        this.name = record.getValue('cf_schedules.schedule_name');
        this.days = [];
    },

    /**
     * Adds a day to the schedule.
     * @param day
     */
    addDay: function(day) {
        this.days.push(day);
    },

    /**
     * Returns true if the schedule is applicable for a specific craftsperson and date.
     * @param cfId
     * @param date
     */
    appliesTo: function(cfId, date) {
        var result = false;

        if (this.cfId === cfId && date.between(this.startDate, this.endDate)) {
            result = this;
        }

        return result;
    },

    /**
     * Returns an array of schedule day objects for day of the week matching specified date.
     * @param date
     */
    getDays: function(date) {
        var name = dayName(date);

        return _.filter(this.days, function(day) {
            return day.dayOfWeek === name;
        });
    },

    /**
     * Returns the number of available hours for specified date.
     * @param date
     */
    getAvailableHours: function(date, scheduleExceptions) {
        // 24 hour array, 1 means the hour is available
        var hours = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];

        // helper function to set available hours in the array
        var setAvailableHours = function(startHour, endHour, availability) {
            for (var h = startHour; h <= endHour; h++) {
                hours[h] = availability;
            }
        }

        // set available hours for schedule days
        var days = this.getDays(date);
        _.each(days, function(day) {
            setAvailableHours(day.startTime.getHours(), day.endTime.getHours(), 1);
        });

        // set available or unavailable hours for schedule exceptions
        _.each(scheduleExceptions, function(scheduleException) {
            var availability = (scheduleException.varianceType === 'STANDARD' ? 1 : 0);
            setAvailableHours(scheduleExceptions.startTime.getHours(), scheduleExceptions.endTime.getHours(), availability);
        });

        // calculate and return the total available hours
        return _.reduce(hours, function(memo, hour) {
            return memo + hour;
        });
    }
});

/**
 * An object that prorated assigned unscheduled hours relative to the visible date range.
 */
CraftspersonUnscheduledTasks = Base.extend({

    /**
     * Array of DueTaskList objects, ordered by date due.
     */
    unscheduledTaskSegments: null,

    /**
     * Constructor.
     */
    constructor: function() {
        this.unscheduledTaskSegments = [];
    },

    /**
     * Adds a task.
     * @param task
     */
    addTask: function(task) {
        var dueTasks = _.find(this.unscheduledTaskSegments, function(dueTask) {
            return (task.dueDate === dueTask.dueDate);
        });
        if (!dueTasks) {
            dueTasks = new UnscheduledTaskSegment(task.dueDate);
            this.unscheduledTaskSegments.push(dueTasks);
        }
        dueTasks.tasks.push(task);
    },

    /**
     * Prorates tasks for displayed date range.
     */
    prorateTasks: function(startDate, endDate) {
        this.unscheduledTaskSegments = _.sortBy(this.unscheduledTaskSegments, 'dueDate');

        var proratedUnscheduledHours = _.reduce(this.unscheduledTaskSegments, function(memo, dueTasks) {
            memo = memo + dueTasks.prorateHours(startDate, endDate);
        }, 0);

        return proratedUnscheduledHours;
    }
});

/**
 * Encapsulates the list of tasks due on the same date.
 */
UnscheduledTaskSegment = Base.extend({

    /**
     * Date due.
     */
    dueDate: null,

    /**
     * The array of task objects.
     */
    tasks: null,

    /**
     * The total estimates hours for all tasks.
     */
    totalHours: 0,

    /**
     * The total estimates hours for all tasks, prorated to the displayed date range.
     */
    proratedHours: 0,

    /**
     * Constructor.
     * @param dueDate
     */
    constructor: function(dueDate) {
        this.dueDate = dueDate;
        this.tasks = [];
    },

    /**
     * Calculates the total estimated hours for all tasks due on this date, prorated to specified date range.
     */
    prorateHours: function(startDate, endDate) {
        this.totalHours = _.reduce(this.tasks, function(memo, task) {
            return memo + task.estimatedHours;
        }, 0);

        var totalDays = daysBetween(startDate, this.dueDate);
        var displayedDays = daysBetween(startDate, endDate);

        if (totalDays > displayedDays) {
            this.proratedHours = this.totalHours * (displayedDays / totalDays);
        } else {
            this.proratedHours = this.totalHours;
        }

        return this.proratedHours;
    }
});

/**
 * Help function to calculate the day difference between two dates.
 * @param startDate
 * @param endDate
 * @returns {number}
 */
function daysBetween(startDate, endDate) {
    var oneDay = 1000 * 60 * 60 * 24;

    var date1ms = startDate.getTime();
    var date2ms = endDate.getTime();

    return Math.round((date2ms - date1ms) / oneDay);
}

/**
 * Help function to calculate the hour difference between two dates.
 * @param startDate
 * @param endDate
 * @returns {number}
 */
function hoursBetween(startDate, endDate) {
    var oneHour = 1000 * 60 * 60;

    var date1ms = startDate.getTime();
    var date2ms = endDate.getTime();

    return Math.round((date2ms - date1ms) / oneHour);
}

/**
 * Returns the name of the day of the week.
 * TODO: localize.
 * @param date
 * @returns {string}
 */
function dayName(date) {
    var dayName = '';
    switch (date.getDay()) {
        case 0: dayName = 'SUNDAY'; break;
        case 1: dayName = 'MONDAY'; break;
        case 2: dayName = 'TUESDAY'; break;
        case 3: dayName = 'WEDNESDAY'; break;
        case 4: dayName = 'THURSDAY'; break;
        case 5: dayName = 'FRIDAY'; break;
        case 6: dayName = 'SATURDAY'; break;
    }

    return dayName;
}