/**
 * The example controller.
 */
View.createController('laborSchedulingBoard-workSchedule', {

    /**
     * The WorkScheduleControl instance.
     */
    scheduler: null,

    afterViewLoad: function() {
        this.scheduler = View.createControl({
            control: 'WorkScheduleControl',
            container: 'workScheduleContainer',
            panel: 'workSchedule',
            maxScheduledJobsPerPersonPerDay: 3,
            model: new WorkScheduleModel({
                maxScheduledJobsPerPersonPerDay: 3
            }),
            startDate: new Date(2017, 1, 1)
        });

        this.on('app:sp:laborBoard:showWorkSchedule', this.showWorkSchedule);
        this.showWorkSchedule(null);
    },

    /**
     * Queries and displays work schedule for specified restriction.
     * @param restriction
     */
    showWorkSchedule: function(restriction) {
        this.scheduler.restriction = restriction;
        this.scheduler.loadSchedule();
    }
});

/**
 * The Labor Board control implements https://confluence.archibus.zone/display/PLAT/HTML+Scheduling+Control.
 */
WorkScheduleControl = Base.extend({

    /**
     * The configuration object.
     */
    config: null,

    /**
     * The dxScheduler control instance.
     */
    scheduler: null,

    /**
     * The WorkScheduleModel instance.
     */
    model: null,

    /**
     * Restriction for the schedule.
     */
    restriction: null,

    /**
     * Control constructor.
     * @param {config} The control configuration.
     */
    constructor: function(config) {
        this.config = config;
        this.model = this.config.model;

        this.initializeScheduler();
    },

    /**
     * Sets the control height to specified value.
     * @param {height} The height in pixels.
     */
    setHeight: function(height) {},

    /**
     * Adds a listener to specified control event.
     * @param {eventName} The name of the control event, e.g. 'click' or 'locateBuilding'.
     * @param {listener} The listener function reference.
     */
    addEventListener: function(eventName, listener) {},

    /**
     * Removes listeners from specified control event.
     * @param {eventName} The name of the control event, e.g. 'click' or 'locateBuilding'.
     * @param {listener} The listener function reference. Optional. If not specified, the control removes all event listeners for this event.
     */
    removeEventListener: function(eventName) {},

    /**
     * Initializes the dxScheduler control with customizations required for the Labor Scheduling Board.
     */
    initializeScheduler: function() {
        var control = this;

        var previousTrade = null;

        var scheduler = jQuery("#workScheduleContainer").dxScheduler({
            dataSource: this.model.scheduledTasks,
            useDropDownViewSwitcher: true,
            views: ["timelineWorkWeek"],
            currentView: "timelineWorkWeek",
            currentDate: this.config.startDate,
            firstDayOfWeek: 0,
            startDayHour: 8,
            endDayHour: 18,
            cellDuration: 60,
            width: "100%",
            height: "calc(100% - 10px)",
            groups: ["cfId"],
            resources: [{
                field: "cfId",
                dataSource: this.model.craftspersons,
                label: "Craftsperson",
                useColorAsDefault: true
            }],
            crossScrollingEnabled: true,

            // display the secondary time scale (hours) as vertical text
            timeCellTemplate: function (itemData, itemIndex, itemElement) {
                var text = itemData.text.replace('AM', '').replace('PM', '');

                itemElement.parent().height(50);
                jQuery("<div>").text(text).css("transform", "rotate(-90deg)").appendTo(itemElement);

                if (itemData.date.getHours() === 8) {
                    itemElement.addClass('first-hour');
                }
            },

            // add unscheduled hours to each resource row
            resourceCellTemplate: function(cellData, index, container) {
                container.css('height', 76);

                if (cellData.data.trade !== previousTrade) {
                    previousTrade = cellData.data.trade;
                    var tr = container.parent().parent();
                    var tradeTr = jQuery('<tr><th class="trade"><span class="expanded" onclick="toggleTrade(this)">' + cellData.data.trade +'</span></th></tr>');
                    tradeTr.insertBefore(tr);
                    //container.parent().prepend(jQuery('<div>').addClass('trade').text(cellData.data.trade));
                } else {
                    container.parent().append(jQuery('<div>').addClass('no-trade'));
                }

                container.append(jQuery('<div>').text(cellData.text));
                container.append(jQuery('<div>').addClass('resource-totals').text(cellData.data.totals.scheduledHours + ' ' + getMessage('scheduledHoursMessage')));
                container.append(jQuery('<div>').addClass('resource-totals').text(cellData.data.totals.assignedUnscheduledHours + ' ' + getMessage('assignedUnscheduledHoursMessage')));
                container.append(jQuery('<div>').addClass('resource-totals').text(cellData.data.totals.availableHours + ' ' + getMessage('availableHoursMessage')));

                var rowEl = jQuery('.dx-scheduler-date-table').find('tr:nth-child(' + index + ')');
                var unscheduledPanel = addUnscheduledHours(rowEl, index, 0, 1000, 'rgba(51, 122, 183, 0.1)');
                jQuery(unscheduledPanel).on('drop', function (event) {
                    event.preventDefault();

                    var wrId = event.originalEvent.dataTransfer.getData("wrId");
                    var task = {
                        cfId: cfId,
                        wrId: wrId
                    };

                    // control.model.createTask(task);
                });
                addUnscheduledHours(rowEl, index, 0, 600, 'rgba(51, 122, 183, 0.1)', 'Wed', 6);

                var x = 0;
                var date = control.scheduler.getStartViewDate();
                _.each(cellData.data.unscheduledTasks.unscheduledTaskSegments, function(unscheduledTaskSegment) {
                    var days = daysBetween(unscheduledTaskSegment.dueDate, date);
                    var width = 1000 * (days / 5);
                    addUnscheduledHours(rowEl, index, x, width, '#A8C2E8', dayName(unscheduledTaskSegment.dueDate), unscheduledTaskSegment.totalHours);
                    date = unscheduledTaskSegment.dueDate.add(Date.DAYS, 1);
                });
            },

            // display unavailable hours as diagonal stripes
            dataCellTemplate: function(cellData, index, container) {
                if (cellData.groups) {
                    var cfId = cellData.groups.cfId;

                    // TODO: test code, remove it
                    if (isFriday(cellData.startDate)) {
                        if (cfId === 'ANNA PETERS') {
                            container.addClass('unavailable-hours');
                        }
                    }

                    if (cellData.startDate.getHours() === 8) {
                        container.addClass('first-hour');
                    }

                    container.on('dragover', function (event) {
                        event.preventDefault();
                    });
                    container.on('drop', function (event) {
                        event.preventDefault();

                        var wrId = event.originalEvent.dataTransfer.getData("wrId");
                        var description = event.originalEvent.dataTransfer.getData("description");
                        var task = {
                            cfId: cfId,
                            wrId: wrId,
                            description: description,
                            startDate: cellData.startDate,
                            endDate: cellData.endDate,
                            estimatedHours: 1
                        };
                        scheduler.addAppointment(task);

                        control.model.createTask(task);

                        control.trigger('app:sp:laborBoard:afterWorkRequestScheduled', task);
                    });
                }

                return jQuery('<div>').text(cellData.text);
            },

            /**
             * Define a custom HTML template for the task.
             */
            appointmentTemplate: function(task, itemIndex, itemElement) {
                return jQuery('<div>' + task.wrId + '</div><div>' + task.description + '</div>');
            },

            /**
             * Define a custom task edit form.
             * @param data
             */
            onAppointmentFormCreated: function(data) {
                var form = data.form,
                    task = data.appointmentData;

                form.option('height', 200);
                form.option("items", [{
                    label: {
                        text: "Work Request"
                    },
                    name: "wrId",
                    editorType: "dxTextBox",
                    editorOptions: {
                        value: task.wrId,
                        readOnly: true
                    }
                }, {
                    label: {
                        text: "Description"
                    },
                    name: "description",
                    editorType: "dxTextArea",
                    editorOptions: {
                        height: '150px',
                        value: task.description,
                        readOnly: true
                    },
                }, {
                    dataField: "startDate",
                    editorType: "dxDateBox",
                    editorOptions: {
                        value: task.startDate,
                        type: "datetime"
                    }
                }, {
                    name: "endDate",
                    dataField: "endDate",
                    editorType: "dxDateBox",
                    editorOptions: {
                        value: task.endDate,
                        type: "datetime"
                    }
                }]);
            },

            /**
             * Triggered when the user changes the appointment using either the standard dxScheduler appointment dialog,
             * or by dragging or resizing the appointment.
             * @param event
             */
            onAppointmentUpdating: function(event) {
                control.model.updateTask(event.newData, event.oldData);
            },

            /**
             * Triggered after the user changes the appointment using either the standard dxScheduler appointment dialog,
             * or by dragging or resizing the appointment.
             * @param event
             */
            onAppointmentUpdated: function(event) {
                control.trigger('app:sp:laborBoard:afterWorkRequestUpdated', event.appointmentData);
            },

            onOptionChanged: function(event) {
                if (event.name === 'currentDate') {
                    control.loadSchedule(event.value);
                }
            }
        }).dxScheduler("instance");

        // placeholder for inter-scheduler drag and drop
        jQuery('.dx-scheduler-date-table-cell').on('dxdrop', function(event) {
        });

        jQuery('.dx-scheduler-view-switcher').hide();
        jQuery('.dx-scheduler-view-switcher-label').hide();

        this.scheduler = scheduler;
    },

    /**
     * Loads the schedule for specified list of resources.
     * Triggers the afterRefresh event.
     */
    loadSchedule: function(startDate) {
        if (!startDate) {
            startDate = this.scheduler.getStartViewDate();
        }
        this.model.loadSchedule(this.restriction, startDate, startDate.add(Date.DAY, 4));
        this.scheduler.repaint();
    }
});

/**
 * Helper functions.
 */

function isFriday(date) {
    var day = date.getDay();
    return day === 5;
}

function addUnscheduledHours(rowEl, rowIndex, x, width, backgroundColor, day, unscheduledHours) {
    var unscheduledHoursPanel = document.createElement('div');
    unscheduledHoursPanel.className = 'unscheduled-hours-panel';
    unscheduledHoursPanel.style.zIndex = 1000;
    unscheduledHoursPanel.style.top = (115 * (rowIndex + 1) - 140) + 'px';
    unscheduledHoursPanel.style.left = x + 'px';
    unscheduledHoursPanel.style.width = width + 'px';
    unscheduledHoursPanel.style.backgroundColor = backgroundColor;

    if (day) {
        var unscheduledHoursLabel = document.createElement('span');
        unscheduledHoursLabel.className = 'unscheduled-hours-label';
        unscheduledHoursLabel.appendChild(document.createTextNode('Unscheduled hours due ' + day + ':'));

        var unscheduledHoursIcon = document.createElement('span');
        unscheduledHoursIcon.className = 'unscheduled-hours-icon';
        unscheduledHoursIcon.appendChild(document.createTextNode(unscheduledHours));

        unscheduledHoursPanel.appendChild(unscheduledHoursIcon);
        unscheduledHoursPanel.appendChild(unscheduledHoursLabel);
    }

    rowEl.append(unscheduledHoursPanel);

    return unscheduledHoursPanel;
}

