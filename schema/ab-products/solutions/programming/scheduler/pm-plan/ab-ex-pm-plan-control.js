var PMPlanner = Base.extend({

    _grid: null,
    _config: null,
    _toolbar: null,
    _matrix: null,
    _eventManager: null,

    constructor: function (config) {
        this._config = config;
        this._eventManager = new PlannerEventManager();
        this._matrix = new PlannedMatrix(config, this._eventManager);
        this._toolbar = new PlannerToolbar(config, this._eventManager);

        var scope = this;
        var gridColumns = this._configureGridColumns(),
            gridID = this._createDomGrid();

        var isInitialization = false,
            isRefresh = false;

        this._grid = View.createControl({
            control: 'DataGrid',
            panel: 'gridPanel',
            container: gridID,
            dataSource: 'dataDS',
            showFilter: false,
            showSort: false,
            rowAlternationEnabled: false,
            columns: gridColumns,
            events: {
                onGroupCellPrepared: function (option, row) {
                    scope._matrix.setGroupCellStyle(option);
                    scope._matrix.renderGroupStatistics(option);
                },
                onCellPrepared: function (option, row) {
                    scope._matrix.setDataCellStyle(option);
                    scope._matrix.renderBlock(option);
                    //scope._eventManager.callEvent('onRenderedScheduledBlock', [option]);
                },
                onHeaderCellPrepared: function (option) {
                    scope._matrix.renderTitleHeaderCell(option);
                    scope._matrix.setHeaderCellStyle(option);
                },
                afterLoadedData: function () {
                    scope._matrix.load(this);
                }
            }
        });
        jQuery('#' + gridID).dxDataGrid('instance').option("grouping.autoExpandAll", false);
    },

    setHeight: function (height) {
        this._grid.setHeight(height - this._toolbar.getHeight());
    },

    _configureGridColumns: function () {
        var result = [];
        var dateMatrixColumns = this._matrix.configureColumns(),
            columnDefinitions = this._config.columns;

        for (var i = 0; i < columnDefinitions.length; i++) {
            result.push(columnDefinitions[i]);
        }
        for (var i = 0; i < dateMatrixColumns.length; i++) {
            result.push(dateMatrixColumns[i]);
        }
        return result;
    },

    _createDomGrid: function () {
        var gridID = this._config.container +'Grid';
        var container = DOM.returnDOM(this._config.container);
        container.append(DOM.renderDIV({
            id: gridID
        }));
        return gridID;
    }
});

var PlannerToolbar = Base.extend({

    _config: null,
    _toolbarID: null,
    _eventManager: null,
    _heightToolbar: 40,

    constructor: function (config, eventManager) {
        this._config = config;
        this._eventManager = eventManager;
        this._toolbarID = this._config.container +'Tooltip';

        this._createToolbar();
        this._renderToolbar();
    },
    getHeight: function () {
        return this._heightToolbar;
    },
    _createToolbar: function () {
        var container = DOM.returnDOM(this._config.container);
        container.append(DOM.renderDIV({
            id: this._toolbarID
        }));
    },
    _renderToolbar: function () {
        var toolbarDOM = DOM.returnDOM(this._toolbarID);
        toolbarDOM.dxToolbar({
            height: this._heightToolbar
        });
        this._toolbar = toolbarDOM.dxToolbar('instance');

        var leftArrow = DOM.renderIMG({
            id: PlannerToolbar.ID_LEFT_ARROW,
            image: PlannerToolbar.IMG_LEFT_ARROW,
            cssClass: PlannerToolbar.CSS_LEFT_ARROW,
            scope: this,
            onClick: this._clickLeftArrow
        });

        var rightArrow = DOM.renderIMG({
            id: PlannerToolbar.ID_RIGHT_ARROW,
            image: PlannerToolbar.IMG_RIGHT_ARROW,
            cssClass: PlannerToolbar.CSS_RIGHT_ARROW,
            scope: this,
            onClick: this._clickRightArrow
        });

        this._addElementInToolbar(leftArrow, 'right');
        this._addElementInToolbar(rightArrow, 'right');
    },
    _addElementInToolbar: function (elemnt, position) {
        var toolbarDOM = DOM.returnDOM(this._toolbarID);
        var positionClass;
        switch (position) {
            case 'center': positionClass = '.dx-toolbar-center';
                break;
            case 'right': positionClass = '.dx-toolbar-after';
                break;
            case 'left': positionClass = '.dx-toolbar-before';
                break;
        }
        toolbarDOM
            .find('.dx-toolbar-items-container ' + positionClass)
            .append(elemnt);
    },
    _clickLeftArrow: function () {
        this._eventManager.callEvent('onClickPrevWeeks', []);
    },
    _clickRightArrow: function () {
        this._eventManager.callEvent('onClickNextWeeks', []);
    }

}, {
    ID_LEFT_ARROW: 'leftArrow',
    ID_RIGHT_ARROW: 'rightArrow',
    CSS_LEFT_ARROW: 'toolbar_leftArrow',
    CSS_RIGHT_ARROW: 'toolbar_rightArrow',
    IMG_LEFT_ARROW: 'left-arrow.png',
    IMG_RIGHT_ARROW: 'right-arrow.png',
});

/*
 * Manager events registered event in scheduled and allowing to call
 */
var PlannerEventManager = Base.extend({
    _eventsMap: {},
    /*
     * Constructor
     */
    constructor: function () {
        this._eventsMap = {};
    },
    /*
     * Add event
     *  @eventName: the name of the events
     *  @callback: function
     *  @scope: the scope of the area events
     */
    addEventListener: function (eventName, callback, scope) {
        if (typeof this._eventsMap[eventName] == "undefined"){
            this._eventsMap[eventName] = {};
        }
        this._eventsMap[eventName] = {
            function: callback,
            scope: scope
        };
    },
    /*
     * Call event
     *  @eventName: the name of the events
     *  @fields: variables functions
     */
    callEvent: function (eventName, fields) {
        if (typeof eventName == "string") {
            eventName = {type: eventName};
        }
        var listeners = this._eventsMap[eventName.type];
        if (listeners) {
            var fun = listeners.function;
            return fun.apply(listeners.scope, fields);
        }
    },

    removeEvent: function (eventName) {
          delete this._eventsMap[eventName];
    },

    isEvent: function (eventName) {
        if (this._eventsMap.hasOwnProperty(eventName)) {
            return true;
        }
        return false;
    }


});

var ScheduledConstants = {
    ID_BLOCK: 'matrixCell_',
    ID_BLOCK_TOOLTIP: 'matrixCellTooltip_',

    IMG_RIGHT_ARROW: 'next.png',
    IMG_CHECK: 'check-symbol.png',
    IMG_WARNING: 'warning.png',

    CSS_PAST_CELL_CLASS: 'matrixPastGroupCell',
    CSS_FUTURE_CELL_CLASS: 'matrixFutureGroupCell',
    CSS_EXECUTED_BLOCK: 'matrixGroupExecuted',
    CSS_BACKLOG_BLOCK: 'matrixGroupBacklog',
    CSS_SQUARE: 'matrixFutureGroupCell_square',
    CSS_VALUE: 'matrixFutureGroupCell_value',
    CSS_MISSING_JOB: 'matrixMissingJobCell',
    CSS_COMPLETED_JOB: 'matrixCompletedJobCell',

    TYPEBLOCK_FUTURE_JOB: 'futureGroupCell',
    TYPEBLOCK_PAST_JOB: 'pastGroupCell',
    TYPEBLOCK_MISSED: 'missedCell',
    TYPEBLOCK_COMPLETED: 'completedCell',
    TYPEBLOCK_FUTURE_OPEN_WORK: 'futureOpenWork',
    

    DATAFIELD_PMSD_DATA_TODO: 'pmsd.date_todo',
    DATAFIELD_PMSD_PMS_ID: 'pmsd.pms_id',
    DATAFIELD_PMSD_WO_ID: 'pmsd.wo_id',
    DATAFIELD_PMSD_DATA_LATEST: 'pmsd.date_latest',
    DATAFIELD_PMSD_DATA_COMPLETED: 'pmsd.date_completed',


    CELL_HEIGHT: 25,
    CELL_WIDTH: 80,
    FUTURE_STATISTIC_WIDTH: 250,
};

var DOM = {
    renderIMG: function (option) {
        var img = jQuery('<img src="' + option.image + '"/>');
        if (option.id)
            img.attr('id', option.id);
        if (option.cssClass)
            img.addClass(option.cssClass);
        if (option.onClick)
            img.on('dxclick', function () {
                option.onClick.call(option.scope);
            });
        if (option.cssFields)
            img.css(option.cssFields);

        return img;
    },
    renderDIV: function (option) {
        var div = jQuery('<div></div>');
        if(!option)
            return div;
        if (option.id)
            div.attr('id', option.id);
        if (option.cssClass)
            div.addClass(option.cssClass);
        if (option.cssFields)
            div.css(option.cssFields);

        return div;
    },
    renderTEXT: function (option) {
        var text = jQuery('<div>'+ option.text +'</div>');
        if (option.cssClass)
            text.addClass(option.cssClass);
        if (option.cssFields)
            text.css(option.cssFields);

        return text;
    },
    renderTOOLTIP: function (option) {
        var cell = option.devExpressCell;

        cell.append(DOM.renderDIV({
            id: option.tooltipID
        }));
        cell.on('mousemove', function() {
            var headerTitle = cell.children();
            headerTitle.attr('title', ' ');
        });
        cell.on('mouseover', function () {
            var tooltip = DOM.returnDOM(option.tooltipID);
            tooltip.dxTooltip({
                target: '#' + option.cellID,
                position: option.position,
                contentTemplate: option.onRenderText
            });
            DOM.returnDOM(option.tooltipID).dxTooltip("instance").show();
        });
        cell.on('mouseout', function () {
            DOM.returnDOM(option.tooltipID).dxTooltip("instance").hide();
        });
    },
    returnDOM: function (id) {
        return jQuery('#' + id);
    }
};

var Tools = {
    isColumnWeek: function (option) {
        return option.column.dataField && option.column.dataField.indexOf('week_') != -1;
    },
    isFutureWeek: function (option) {
        var columnDate = option.column.startDate,
            columnWeek = columnDate.getWeek(),
            columnYear = columnDate.getFullYear();

        var currentDate =  new Date(),
            currentWeek = currentDate.getWeek(),
            currentYear = currentDate.getFullYear();

        if (columnYear > currentYear) {
            return true;
        } else {
            if (columnYear == currentYear) {
                if (columnWeek >= currentWeek) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    },
};

