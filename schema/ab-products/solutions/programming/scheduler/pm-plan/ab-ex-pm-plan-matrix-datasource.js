
var PlannedDS = Base.extend({

    _months: null,
    _weeks: null,
    _selectDay: null,

    _statisticsDS: null,
    _worksDS: null,

    _groupID: null,
    _groupStatistics: null,

    _works: null,

    constructor: function () {
        this._months = {};
        this._weeks = {};
        this._groupStatistics = {};
        this._works = {};
        this._groupID = null;
        this._selectDay = null;

        this._statisticsDS = View.dataSources.get('statisticsDS');
        this._worksDS = View.dataSources.get('worksDS');
    },

    load: function (start, end) {
        var restriction = this._getRestriction(start, end);
        this._loadStatistics(restriction);
        this._loadWorks(restriction);
    },
    addMonth: function (id, date) {
        if (date == null) {
            this._months[id] = null;
            return;
        }
        this._months[id] = {
            month: null,
            weeks: 0
        };
        this._months[id].month = new Date(date.getTime());
    },
    addWeek: function (id, date) {
        if (date == null) {
            this._weeks[id] = null;
            return;
        }
        this._weeks[id] = new Date(date.getTime());
    },
    setGroupID: function (groupID) {
        this._groupID = PlannedDS.TableName_PMS + groupID;
    },
    getMonth: function (id) {
        return new Date(this._months[id].month.getTime())
    },
    getCountWeekInMonth: function (id) {
        return  this._months[id].weeks;
    },
    getWeek: function (id) {
        return  new Date(this._weeks[id].getTime());
    },
    getWeekStatistics: function (group, week) {
        if (!this._groupStatistics[group])
            return null;

        if (!this._groupStatistics[group].weeks[week])
            return null;

        var statistics = this._groupStatistics[group].weeks[week];
        if (statistics == undefined) {
            return null;
        }
        return statistics;
    },
    getGroupStatistics: function (group){
        if (!this._groupStatistics[group])
            return null;

        var statistics = this._groupStatistics[group].total;
        if (statistics == undefined) {
            return null;
        }
        if (!statistics.hasOwnProperty(PlannedDS.PastJobs_CompletedLate)) {
            statistics[PlannedDS.PastJobs_CompletedLate] = 0;
        }
        if (!statistics.hasOwnProperty(PlannedDS.PastJobs_CompletedOnTime)) {
            statistics[PlannedDS.PastJobs_CompletedOnTime] = 0;
        }
        if (!statistics.hasOwnProperty(PlannedDS.PastJobs_Missed)) {
            statistics[PlannedDS.PastJobs_Missed] = 0;
        }
        if (!statistics.hasOwnProperty(PlannedDS.FutureJobs_Generated)) {
            statistics[PlannedDS.FutureJobs_Generated] = 0;
        }
        if (!statistics.hasOwnProperty(PlannedDS.FutureJobs_Planned)) {
            statistics[PlannedDS.FutureJobs_Planned] = 0;
        }


        return statistics;
    },
    getWork: function (id, date) {
        var result = null;
        var works = this._works[id];
        if(!works)
            return null;

        for (var i = 0; i < works.length; i++) {
            var workDate = works[i].date;
            if (workDate.getFullYear() == date.getFullYear() && workDate.getWeek() == date.getWeek()) {
                result = works[i];
                break;
            }
        }
        return result;
    },
    getSelectDay: function () {
        return this._selectDay;
    },
    increaseNumWeeksInMonth: function (id) {
        this._months[id].weeks++;
    },
    formatWeek: function (date) {
        var yy = date.getFullYear();
        var ww = date.getWeek();
        if (ww < 10)
            ww = '0' + ww;
        return yy + '-' + ww;
    },
    saveSelectDay: function (dayData) {
        this._selectDay = dayData;
    },

    _formatDate: function(date) {
        var dd = date.getDate();
        if (dd < 10)
            dd = '0' + dd;

        var mm = date.getMonth() + 1;
        if (mm < 10)
            mm = '0' + mm;

        var yy = date.getFullYear();

        return yy + '-' + mm + '-' + dd;
    },

    _loadStatistics: function (restriction) {
        this._groupStatistics = {};

        this._loadDataForStatistics(PlannedDS.PastJobs_CompletedOnTime, restriction);
        this._loadDataForStatistics(PlannedDS.PastJobs_CompletedLate, restriction);
        this._loadDataForStatistics(PlannedDS.PastJobs_Missed, restriction);
        this._loadDataForStatistics(PlannedDS.FutureJobs_Generated, restriction);
        this._loadDataForStatistics(PlannedDS.FutureJobs_Planned, restriction);
    },
    _loadWorks: function (restriction) {
        this._works = {};

        var records = this._worksDS.getDataSet(restriction, {}).records;
        for (var i = 0; i < records.length; i++) {
            var record = records[i];
            var id = record.getValue(PlannedDS.DataField_PmsId);
            if (!this._works.hasOwnProperty(id)) {
                this._works[id] = [];
            }
            this._works[id].push({
                record: record,
                date: (record.getValue(PlannedDS.DataField_DateLatest) == "")? record.getValue(PlannedDS.DataField_DateTodo) : record.getValue(PlannedDS.DataField_DateLatest),
                status: this._getStatus(record)
            });
        }
    },
    _loadDataForStatistics: function (type, restriction) {
        this._statisticsDS.addParameter(type, true);

        var records = this._statisticsDS.getDataSet(restriction, {}).records;
        for (var i = 0; i < records.length; i++) {
            var record = records[i];
            var groupName = record.getValue(this._groupID);
            var week = record.getValue(PlannedDS.DataField_Week);
            if (!this._groupStatistics.hasOwnProperty(groupName)) {
                this._groupStatistics[groupName] = {
                    total: {},
                    weeks: {}
                }
            }
            if (!this._groupStatistics[groupName].total.hasOwnProperty(type))
                this._groupStatistics[groupName].total[type] = 0;

            this._groupStatistics[groupName].total[type] += parseInt(record.getValue(PlannedDS.DataField_Count));

            if (!this._groupStatistics[groupName].weeks.hasOwnProperty(week))
                this._groupStatistics[groupName].weeks[week] = {};

            if (!this._groupStatistics[groupName].weeks[week].hasOwnProperty(type))
                this._groupStatistics[groupName].weeks[week][type] = 0;

            this._groupStatistics[groupName].weeks[week][type] += parseInt(record.getValue(PlannedDS.DataField_Count))
        }

        this._statisticsDS.addParameter(type, false);
    },
    _getRestriction: function (start, end) {
        var restriction = new Ab.view.Restriction();

        restriction.addClause('pmsd.date_todo', this._formatDate(start), '&gt;=');
        restriction.addClause('pmsd.date_todo', this._formatDate(end), '&lt;=');

        return restriction;
    },
    _getStatus: function (record) {
        var today = new Date();
        var date = record.getValue(PlannedDS.DataField_DateLatest);
        if (record.getValue(PlannedDS.DataField_DateLatest) == "") {
            date = record.getValue(PlannedDS.DataField_DateTodo);
        }

        if (date.getTime() >  today.getTime()) {
            if (record.getValue(PlannedDS.DataField_WoId) == null) {
                return PlannedDS.FutureJobs_Planned;
            } else {
                return PlannedDS.FutureJobs_Generated;
            }
            
        } else {
            return PlannedDS.PastJobs_Missed;
        }

    }


}, {
    TableName_PMS: 'pms.',

    DataField_Count: 'pmsd.pmsd_count',
    DataField_Week: 'pmsd.week',
    DataField_DateTodo: 'pmsd.date_todo',
    DataField_PmsId: 'pmsd.pms_id',
    DataField_DateCompleted: 'pmsd.date_completed',
    DataField_DateLatest: 'pmsd.date_latest',
    DataField_WoId: 'pmsd.wo_id',

    PastJobs_CompletedOnTime: 'pastJobs-completedOnTime',
    PastJobs_CompletedLate: 'pastJobs-completedLate',
    PastJobs_Missed: 'pastJobs-missed',
    FutureJobs_Generated: 'futureJobs-generated',
    FutureJobs_Planned: 'futureJobs-planned'
});
