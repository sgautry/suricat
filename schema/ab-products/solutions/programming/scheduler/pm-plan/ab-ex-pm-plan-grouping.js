var GroupStatistics = Base.extend({

    _total: null,
    _weeks: null,

    constructor: function (dataSource) {
        this._dataSource = dataSource;
    },

    renderTotalStatistics: function (devExpressOption) {
        var groupTotalValues =  this._dataSource.getGroupStatistics(devExpressOption.text);

        if (groupTotalValues == null)
            return;

        var total = new GroupTotalStatistics(devExpressOption, groupTotalValues);
        total.render();
    },

    renderWeekStatistics: function (devExpressOption) {
        var week = this._dataSource.getWeek(devExpressOption.column.dataField);

        var weekStatistics = this._dataSource.getWeekStatistics(devExpressOption.data.key, this._dataSource.formatWeek(week));
        if (weekStatistics == null)
            return;

        if (this._isFutureWeek(week)) {

            if (!weekStatistics.hasOwnProperty(PlannedDS.FutureJobs_Generated)) {
                weekStatistics[PlannedDS.FutureJobs_Generated] = 0;
            }
            if (!weekStatistics.hasOwnProperty(PlannedDS.FutureJobs_Planned)) {
                weekStatistics[PlannedDS.FutureJobs_Planned] = 0;
            }

            var value = weekStatistics[PlannedDS.FutureJobs_Generated] + weekStatistics[PlannedDS. FutureJobs_Planned];

            new GroupStatisticsFutureWeek({
                devExpressCell: devExpressOption,
                value: value
            })
        } else {
            if (!weekStatistics.hasOwnProperty(PlannedDS.PastJobs_CompletedLate)) {
                weekStatistics[PlannedDS.PastJobs_CompletedLate] = 0;
            }
            if (!weekStatistics.hasOwnProperty(PlannedDS.PastJobs_CompletedOnTime)) {
                weekStatistics[PlannedDS.PastJobs_CompletedOnTime] = 0;
            }
            if (!weekStatistics.hasOwnProperty(PlannedDS.PastJobs_Missed)) {
                weekStatistics[PlannedDS.PastJobs_Missed] = 0;
            }
            new GroupStatisticsPastWeek({
                devExpressCell: devExpressOption,
                value: weekStatistics
            });
        }


    },


    _isFutureWeek: function (date) {
        var columnWeek = date.getWeek(),
            columnYear = date.getFullYear();

        var currentDate =  new Date(),
            currentWeek = currentDate.getWeek(),
            currentYear = currentDate.getFullYear();

        if (columnYear > currentYear) {
            return true;
        } else {
            if (columnYear == currentYear) {
                if (columnWeek >= currentWeek) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    },
});



var GroupTotalStatistics = Base.extend({
    _charBarID: null,
    _blocksBarID: null,

    constructor: function (devExpressOption, statistics) {
        this._title = devExpressOption.column.caption + ': ' + devExpressOption.text;

        this._devExpressCell = devExpressOption.cellElement;
        this._charBarID = 'charBar' + devExpressOption.text;
        this._blocksBarID = 'blocksBar' + devExpressOption.text;
        this._statisticsGroup = statistics;
        
    },

    render: function () {
        this._renderTitle();
        this._renderStatisticsForPrevWeeks();
        this._renderStatisticsForFutureWeeks();
    },

    _renderTitle: function () {
        this._devExpressCell.html('<div class="groupStatistics_title">' + this._title.toUpperCase() + '</div>');
    },

    _renderStatisticsForPrevWeeks: function () {
        var scope = this;
        var total = this._statisticsGroup[PlannedDS.PastJobs_Missed] + this._statisticsGroup[PlannedDS.PastJobs_CompletedLate] + this._statisticsGroup[PlannedDS.PastJobs_CompletedOnTime];
        var charBar = DOM.renderDIV({
            id: this._charBarID,
            cssClass: 'groupStatistics_char_bar'
        }); 

        if (this._statisticsGroup['pastJobs-completedOnTime'] != 0) {
            charBar.append(DOM.renderIMG({
                image: ScheduledConstants.IMG_CHECK,
                cssClass: 'groupStatistics_char_bar_check'
            }));
        }

        charBar.append(this._renderArea(
            this._statisticsGroup[PlannedDS.PastJobs_Missed],
            total,
            'groupStatistics_char_bar_missing'
        ));
        charBar.append(this._renderArea(
            this._statisticsGroup[PlannedDS.PastJobs_CompletedLate],
            total,
            'groupStatistics_char_bar_expired'
        ));
        this._devExpressCell.append(charBar);
        var percent = (this._statisticsGroup[PlannedDS.PastJobs_CompletedOnTime] / total) * 100;
        if (isNaN(percent)) {
            percent = 0;
        }

        this._devExpressCell.append(DOM.renderTEXT({
            text:  parseInt(percent) + '% on time',
            cssClass: 'groupStatistics_char_bar_text'
        }));

        DOM.renderTOOLTIP({
            cellID: this._charBarID,
            tooltipID: this._charBarID + 'Tooltip',
            position: 'top',
            devExpressCell: charBar,
            onRenderText: function (contentElement){
                contentElement.append(DOM.renderTEXT({
                    text: 'Completed: ' + scope._statisticsGroup[PlannedDS.PastJobs_CompletedOnTime]
                }));
                contentElement.append(DOM.renderTEXT({
                    text: 'Expired: ' + scope._statisticsGroup[PlannedDS.PastJobs_CompletedLate]
                }));
                contentElement.append(DOM.renderTEXT({
                    text: 'Missing: ' + scope._statisticsGroup[PlannedDS.PastJobs_Missed]
                }));
            }
        })

    },

    _renderStatisticsForFutureWeeks: function () {
        var scope = this;
        var width = this._getWidthBlock();
        this._devExpressCell.append(DOM.renderTEXT({
            text:  (parseInt(this._statisticsGroup['futureJobs-planned']) + parseInt(this._statisticsGroup['futureJobs-generated'])) + ' scheduled',
            cssClass: 'groupStatistics_text_future'
        }));

        var futureStatistics = DOM.renderDIV({
            id: this._blocksBarID,
            cssClass: 'groupStatistics_future_statistics',
            cssFields: {
                height: ScheduledConstants.CELL_HEIGHT + 'px'
            }
        });
        futureStatistics.append(this._renderBlocks(
            this._statisticsGroup['futureJobs-generated'],
            'groupStatistics_future_generated_block',
            width
        ));
        futureStatistics.append(this._renderBlocks(
            this._statisticsGroup['futureJobs-planned'],
            'groupStatistics_future_open_block',
            width
        ));
        this._devExpressCell.append(futureStatistics);

        DOM.renderTOOLTIP({
            cellID: this._blocksBarID,
            tooltipID: this._blocksBarID + 'Tooltip',
            position: 'top',
            devExpressCell: futureStatistics,
            onRenderText: function (contentElement){
                contentElement.append(DOM.renderTEXT({
                    text: 'Generated: ' + scope._statisticsGroup['futureJobs-generated']
                }));
                contentElement.append(DOM.renderTEXT({
                    text: 'Planned: ' + scope._statisticsGroup['futureJobs-planned']
                }));

            }
        })
    },

    _getWidthBlock: function () {
        var maxCount = this._statisticsGroup['futureJobs-generated'];
        if (this._statisticsGroup['futureJobs-planned'] > maxCount) {
            maxCount = this._statisticsGroup['futureJobs-planned'];
        }
        var width = parseInt((100 / maxCount) / 100 * ScheduledConstants. FUTURE_STATISTIC_WIDTH);
        if (width > 9) {
            width = 7;
        }
        if (width < 2) {
            width = 2;
        }

        return width;
    },

    _renderBlocks: function (countWork, cssClass, width) {
        var result = DOM.renderDIV();
        for (var i = 0; i < countWork; i++) {
            var block = this._renderBlock(width, cssClass);
            result.append(block);
        }
        return result;
    },

    _renderBlock: function (widthBlock, cssClass) {
        var block = DOM.renderDIV({
            cssFields: {
                float: 'left',
                width: widthBlock + 'px'
            }
        });
        block.append(DOM.renderDIV({
            cssClass: cssClass,
            cssFields: {
                height: ScheduledConstants.CELL_HEIGHT / 2 - 2 + 'px'
            }
        }));
        block.append(DOM.renderDIV({
            cssClass: 'groupStatistics_gap_block',
            cssFields: {
                height: ScheduledConstants.CELL_HEIGHT / 2 + 'px'
            }
        }));

        return block;
    },

    _renderArea: function (value, total, cssClass) {
        var width = (value / total) * 100;
        return DOM.renderDIV({
            cssClass: cssClass,
            cssFields: {
                width: width + '%'
            }
        });
    }
});

var GroupStatisticsFutureWeek = Base.extend({
    _option: null,

    constructor: function (option) {
        this._option = option;
        this._render(option.devExpressCell.cellElement);
    },

    _render: function (cell) {
        cell.css({
            'vertical-align': 'middle',
            padding: 0
        });
        cell.html('');

        cell.append(DOM.renderDIV({
            cssClass: 'groupFutureWeek_square',
            cssFields: {
                float: 'left',
                'margin-left': (ScheduledConstants.CELL_WIDTH / 2 - 15) + 'px'
            }
        }));
        cell.append(DOM.renderTEXT({
            text: this._option.value,
            cssClass: 'groupFutureWeek_text',
            cssFields: {
                'margin-left': (ScheduledConstants.CELL_WIDTH / 2 + 5) + 'px'
            }
        }));
    }
});

var GroupStatisticsPastWeek = Base.extend({
    _option: null,

    constructor: function (option) {
        this._option = option;
        this._render(this._option.devExpressCell.cellElement);
    },

    _render: function (cell) {
        cell.css({
            padding: 0
        });
        cell.html('');

        if (this._hasAddCompletedBlock()) {
            cell.append(this._renderCompletedData());
        }
        if (this._hasAddMissedBlock()) {
            cell.append(this._renderMissedData());
        }
    },

    _renderCompletedData: function () {
        var optionBlock = {
            text: this._option.value[PlannedDS.PastJobs_CompletedOnTime],
            cssClass: 'groupPastWeek_completed'
        };
        if (!this._hasAddMissedBlock()) {
            optionBlock.cssFields = {
                'margin-top': (ScheduledConstants.CELL_HEIGHT / 2 - 5) + 'px',
                'margin-left': (ScheduledConstants.CELL_WIDTH / 2 - 30) + 'px'
            }
        }

        var completedBlock = DOM.renderDIV(optionBlock);
        completedBlock.append(DOM.renderIMG({
            image: ScheduledConstants.IMG_CHECK,
            cssClass: 'groupPastWeek_completed_imgCheck'
        }));
        completedBlock.append(DOM.renderTEXT({
            text: this._option.value[PlannedDS.PastJobs_CompletedOnTime],
            cssClass: 'groupPastWeek_completed_text'
        }));

        return completedBlock;

    },

    _renderMissedData: function () {
        var missedBlock = DOM.renderDIV({
            cssClass: 'groupPastWeek_missed'
        });
        missedBlock.append(DOM.renderDIV({
            cssClass: 'groupPastWeek_missed_triangle'
        }));
        missedBlock.append(DOM.renderTEXT({
            text: this._option.value[PlannedDS.PastJobs_Missed],
            cssClass: 'groupPastWeek_missed_text'
        }));

        return missedBlock;
    },

    _hasAddCompletedBlock: function () {
        var result = false;
        if (this._option.value[PlannedDS.PastJobs_CompletedOnTime]) {
            result = true;
        }
        return result;
    },

    _hasAddMissedBlock: function () {
        var result = false;
        if (this._option.value[PlannedDS.PastJobs_Missed]) {
            result = true;
        }
        return result;
    }
});
