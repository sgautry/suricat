var PlannedMatrix =  Base.extend({

    _NUMBER_OR_MONTHS: 4,
    _WEEK_IN_MONTH: 6,
    _MONTH: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
    _WEEK_DISPLAY: 12,

    _initDate: null,
    _cellIndex: 0,
    _config: null,
    _dataSource: null,
    _eventManager: null,

    constructor: function (config, eventManager) {
        this._initializationFields(config, eventManager);

        Date.prototype.getWeek = function() {
            var target = new Date(this.valueOf()),
                dayNumber = (this.getUTCDay() + 6) % 7,
                firstThursday;

            target.setUTCDate(target.getUTCDate() - dayNumber + 3);
            firstThursday = target.valueOf();
            target.setUTCMonth(0, 1);

            if (target.getUTCDay() !== 4) {
                target.setUTCMonth(0, 1 + ((4 - target.getUTCDay()) + 7) % 7);
            }

            return Math.ceil((firstThursday - target) /  (7 * 24 * 3600 * 1000)) + 1;
        };
        Date.prototype.nextWeek = function (count) {
            var weekCount = 7;
            if (count) {
                weekCount  = count * 7;
            }
            this.setDate(this.getDate() + weekCount);
        };
        Date.prototype.prevWeek = function (count) {
            var weekCount = 7;
            if (count) {
                weekCount  = count * 7;
            }
            this.setDate(this.getDate() - weekCount);
        };
    },
    
    load: function (dataSourceGrid) {
        this._dataSource.setGroupID(dataSourceGrid.getGroupID());

        var start = this._dataSource.getWeek('week_0');
        var end = this._dataSource.getWeek('week_11');

        this._dataSource.load(start, end);
    },
    configureColumns: function () {
        var today = new Date(),
            date = new Date(today.getFullYear(), (today.getMonth() - 1)),
            result = [];

        var weekCount = 0;

        for (var monthIndex = 0; monthIndex < this._NUMBER_OR_MONTHS; monthIndex++) {
            var month = date.getMonth();
            var weekIndex = 0;
            var monthConfig = this._configureMonthColumn(date, monthIndex, month, weekCount);
            this._dataSource.addWeek(monthConfig.fieldName, date);
            this._dataSource.addMonth(monthConfig.groupColumnID, date);
            this._dataSource.increaseNumWeeksInMonth(monthConfig.groupColumnID);
            if (weekCount >= this._WEEK_DISPLAY) {
                monthConfig['visible'] = false;
            } else {
                weekCount++;
                weekIndex++;
                date.nextWeek();
            }
            result.push(monthConfig);

            for (;weekIndex < this._WEEK_IN_MONTH; weekIndex++) {
                var weekConfig = this._configureWeekColumn(date, monthIndex, weekCount);
                this._dataSource.addWeek(weekConfig.fieldName, date);
                if (date.getMonth() != month || weekCount >= this._WEEK_DISPLAY) {
                    weekConfig['visible'] = false;
                } else {
                    this._dataSource.increaseNumWeeksInMonth(monthConfig.groupColumnID);
                    weekCount++;
                    date.nextWeek();
                }
                result.push(weekConfig);
            }
        }

        this._date = date;
        return result;
    },

    setHeaderCellStyle: function (devExpressOption) {
        var cell = devExpressOption.cellElement;
        cell.css('cursor', 'default');

        if (this._isWeekColumn(devExpressOption)) {
            cell.addClass('header_week');
            this._highlightCurrentWeek(devExpressOption);
        } else if (this._isMonthColumn(devExpressOption)) {
            cell.addClass('header_month');
        } else {
            cell.addClass('header_column_data');
        }
    },
    setGroupCellStyle: function (devExpressOption) {
        var cell = devExpressOption.cellElement;
        if (this._isWeekColumn(devExpressOption)) {
            cell.css({
                'border-right': '1px solid #ddd',
                'border-bottom': '1px solid #ddd',
                'border-left': '1px solid #ddd',
                height: ScheduledConstants.CELL_HEIGHT + 'px'
            });
            this._highlightCurrentWeek(devExpressOption);
        } else {
            cell.css('vertical-align', 'middle');
        }
    },
    setDataCellStyle: function (devExpressOption) {
        var cell = devExpressOption.cellElement;
        if (this._isWeekColumn(devExpressOption)) {
            cell.css({
                border: '1px solid #ddd',
                height: ScheduledConstants.CELL_HEIGHT + 'px'
            });
            this._highlightCurrentWeek(devExpressOption);
        } else {
            cell.css('border-right', '1px solid #ddd');
        }
    },

    _isWeekColumn: function (devExpressOption) {
        return devExpressOption.column.dataField && devExpressOption.column.dataField.indexOf('week_') != -1;
    },
    _isMonthColumn: function (devExpressOption) {
        return devExpressOption.column.dataField && devExpressOption.column.dataField.indexOf('group_month_') != -1;
    },
    _highlightCurrentWeek: function (option) {
        var currentDate = new Date();
        var week = currentDate.getWeek(),
            year = currentDate.getFullYear();
        if (this._isWeekColumn(option)) {
            var columnDate = this._dataSource.getWeek(option.column.dataField);
            var columnWeek = columnDate.getWeek();
            var columnYear = columnDate.getFullYear();

            if (columnYear == year && columnWeek == week) {
                option.cellElement.css({'border-left': '3px solid #000'});
            }
        }
    },

    renderTitleHeaderCell: function (devExpressOption) {
        var cell = devExpressOption.cellElement;
        if (this._isMonthColumn(devExpressOption)) {
            cell.html('');
            var id = devExpressOption.column.dataField;
            var weekCount = this._dataSource.getCountWeekInMonth(id);
            var weekPadding = ScheduledConstants.CELL_WIDTH / 8;
            var padding = 0;
            if (weekCount > 1) {
                var currentMonth = this._dataSource.getMonth(id);
                var month = new Date (currentMonth.getFullYear(), currentMonth.getMonth());
                var day = month.getDay();
                if (day == 0) {
                    day = 6;
                } else {
                    day -= 1;
                }
                cell.append(DOM.renderTEXT({
                    text: devExpressOption.column.caption,
                    cssFields: {
                        'padding-left': weekPadding * day
                    }
                }));
            }
        }
    },


    renderGroupStatistics: function (devExpressOption) {
        var key = devExpressOption.data.key;
        if (devExpressOption.column.dataField == 'week_0') {
            var t =0;
        }

        if (devExpressOption.column.groupIndex >= 0) {
            if (!this._groupStatistics.hasOwnProperty(key)) {
                var group = new GroupStatistics(this._dataSource);
                group.renderTotalStatistics(devExpressOption);
                this._groupStatistics[key] = group;
            }
            this._groupStatistics[key].renderTotalStatistics(devExpressOption)
        }
        if (this._isWeekColumn(devExpressOption)) {
            this._groupStatistics[key].renderWeekStatistics(devExpressOption);
        }
    },

    renderBlock: function (devExpressOption) {
        this._initGrid();
        if(this._isWeekColumn(devExpressOption)) {

            var week = this._dataSource.getWeek(devExpressOption.column.dataField);
            var cellDates = this._dataSource.getWork(parseInt(devExpressOption.data.pms_id) + "", week);
            if (cellDates == null) {
                return;
            }

            var cellOption = {
                devExpressOption: devExpressOption,
                blockIndex: this._cellIndex,
                cellDates: [cellDates]
            };
            if(this._isFutureWeek(week)){
                var cellDates = this._dataSource.getWork(parseInt(devExpressOption.data.pms_id) + "", week);
                if (cellDates == null) {
                    return;
                }

                new BlocksDatesInFutureWeeks(cellOption, this._eventManager);
            } else {
                new BlocksDatesInPastWeeks(cellOption, this._eventManager);
            }
            this._cellIndex++;

        }
    },
    _isFutureWeek: function (date) {
        var columnWeek = date.getWeek(),
            columnYear = date.getFullYear();

        var currentDate =  new Date(),
            currentWeek = currentDate.getWeek(),
            currentYear = currentDate.getFullYear();

        if (columnYear > currentYear) {
            return true;
        } else {
            if (columnYear == currentYear) {
                if (columnWeek >= currentWeek) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    },

    renderedScheduledBlock: function (optoin) {
        this._initGrid();
        if (Tools.isColumnWeek(option)) {
            var cellDates = this._dataSource.getIndexDateOfTheWeek(option.data['pms_id'], option.column.startDate);
            if (cellDates.length == 0)
                return;
            var cellOption = {
                devExpressOption: option,
                blockIndex: this._cellIndex,
                cellDates: cellDates
            };
            if (Tools.isFutureWeek(option)) {
                new BlocksDatesInFutureWeeks(cellOption, this._eventManager);
            } else {
                new BlocksDatesInPastWeeks(cellOption);
            }
            this._cellIndex++;
        }

    },
    renderGroupSchedulerBlock: function (option) {
        this._initGrid();
        if (Tools.isColumnWeek(option)) {
            if (Tools.isFutureWeek(option)) {
                new GroupStatisticsFutureWeek({
                    devExpressCell: option,
                    value: 23
                });
            } else {
                new GroupStatisticsPastWeek({
                    devExpressCell: option,
                    value: {
                        completed: 99,
                        missed: 99
                    }
                });
            }
            //this._cellIndex++;
        }
    },

    _initializationFields: function (config, eventManager) {
        this._config = config;
        this._eventManager = eventManager;

        this._cellIndex = 0;
        this._dataSource = new PlannedDS();

        this._groupStatistics = {};

        this._grid = null;
        this._initializationEvents();
    },
    _initializationEvents: function () {
        this._eventManager.addEventListener('onClickPrevWeeks', this._clickPrevWeeks, this);
        this._eventManager.addEventListener('onClickNextWeeks', this._clickNextWeeks, this);

        this._eventManager.addEventListener('getGridCell', this._getCellElement, this);
        this._eventManager.addEventListener('getDataSource', function () {
            return this._dataSource;
        }, this);
    },

    _configureWeekColumn: function (date, month, week) {
        return {
            title: 'W' + date.getWeek(),
            type: 'scheduler',
            width: ScheduledConstants.CELL_WIDTH,
            fieldName: 'week_' + week,
            groupColumnID: 'group_month_' + month,
            visible: true
        };
    },
    _configureMonthColumn: function (date, monthIndex, month, week) {
        var result = this._configureWeekColumn(date, monthIndex, week);
        result['groupColumnTitle'] = this._MONTH[month] + ' ' + date.getFullYear();
        result['groupColumnID'] = 'group_month_' + monthIndex;
        return result;
    },

    _updateGridColumns: function (date) {
        var weekCount = 0;
        var columnIndex = this._config.columns.length;

        for (var monthIndex = 0; monthIndex < this._NUMBER_OR_MONTHS; monthIndex++) {
            var month = date.getMonth();
            var monthID = this._grid.columnOption(columnIndex, 'dataField');
            var weekIndex = 0;
            var isVisibleMonth = true;
            if (weekCount >= this._WEEK_DISPLAY) {
                isVisibleMonth = false;
            }
            this._updateGridColumn(columnIndex, this._MONTH[month] + ' ' + date.getFullYear(), isVisibleMonth);
            this._dataSource.addMonth(monthID, date);
            columnIndex++;

            for (;weekIndex < this._WEEK_IN_MONTH; weekIndex++) {
                var isVisibleWeek = true;
                if (date.getMonth() != month || weekCount >= this._WEEK_DISPLAY) {
                    isVisibleWeek = false;
                }
                this._updateGridColumn(columnIndex, 'W' + date.getWeek(), isVisibleWeek);
                this._dataSource.addWeek(this._grid.columnOption(columnIndex, 'dataField'), date);
                if (isVisibleWeek) {
                    this._dataSource.increaseNumWeeksInMonth(monthID);
                    date.nextWeek();
                    weekCount++;
                }
                columnIndex++;
            }
        }
    },
    _updateGridColumn: function (index, caption, visible) {
        this._grid.columnOption(index, 'caption', caption);
        this._grid.columnOption(index, 'visible', visible);
    },

    _clickPrevWeeks: function () {
        var date = this._dataSource.getWeek("week_0");
        date.prevWeek(4);

        this._updateMatrix(date);
    },
    _clickNextWeeks: function () {
        var date = this._dataSource.getWeek("week_0");
        date.nextWeek(4);
        this._updateMatrix(date);
    },
    _updateMatrix: function (date) {
        this._initGrid();
        var scope = this;
        this._cellIndex = 0;
        this._updateData(date);
        this._grid.beginCustomLoading();
        setTimeout(function() {
            scope._grid.beginUpdate();
            scope._updateGridColumns(date);
            scope._grid.endUpdate();
            scope._grid.endCustomLoading();
            if (scope._eventManager.isEvent('highlightBlock')) {
                scope._eventManager.callEvent('highlightBlock',[]);
            }
        }, 10);

    },

    _updateData: function (date) {
        var start = new Date(date.getTime());
        start.prevWeek(1);
        var end = new Date(date.getTime());
        end.nextWeek(12);
        this._dataSource.load(start, end);
    },

    _initGrid: function () {
        this._grid = jQuery("#" + this._config.container + 'Grid').dxDataGrid('instance');

    },
    _getCellElement: function (rowIndex, columnIndex) {
        var cell = this._grid.getCellElement(rowIndex, columnIndex);
        if (!cell) {
            this.setNextMonths();
            cell = null;
        }
        return cell;
    }

});
