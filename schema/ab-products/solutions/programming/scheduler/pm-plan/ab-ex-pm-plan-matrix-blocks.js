var BlocksDatesInPastWeeks = Base.extend({

    _dates: null,
    _eventManager: null,

    constructor: function (option, eventManager) {
        this._dates = option.cellDates;
        this._eventManager = eventManager;

        this._render(option.devExpressOption.cellElement);
    },

    _render: function (cell) {
        cell.css({
            padding: 0
        });
        cell.html('');

        var status = this._dates[0].status;
        switch (status) {
            case PlannedDS.PastJobs_Missed:
            case 'missed_open':
                cell.addClass('blockDatesPastWeek_missed');
                cell.append(this._renderImage(ScheduledConstants.IMG_WARNING));
                break;
            case 'completed_on_time':
                cell.append(this._renderImage(ScheduledConstants.IMG_CHECK));
                break;
            case 'completed_late':
                if (this._dates[0].phase == 'start') {
                    cell.append(DOM.renderDIV({
                        cssClass: 'blockDatesPastWeek_completed_late_circle',
                        cssFields: {
                            'margin-top': ScheduledConstants.CELL_HEIGHT / 2 - 4 + 'px',
                            'margin-left': ScheduledConstants.CELL_WIDTH / 2 - 7.5 + 'px'
                        }
                    }));
                    cell.append(DOM.renderDIV({
                        cssClass: 'blockDatesPastWeek_completed_late_rectangle',
                        cssFields: {
                            'margin-top': ScheduledConstants.CELL_HEIGHT / 2 + 1 + 'px',
                            'margin-left': ScheduledConstants.CELL_WIDTH / 2 - 7.5 + 'px'
                        }
                    }));
                }
                if (this._dates[0].phase == 'next') {
                    cell.append(DOM.renderDIV({
                        cssClass: 'blockDatesPastWeek_completed_late_rectangle',
                        cssFields: {
                            'margin-top': ScheduledConstants.CELL_HEIGHT / 2 + 1 + 'px'
                        }
                    }));
                }

                if (this._dates[0].phase == 'end') {
                    cell.addClass('blockDatesPastWeek_completed_late_completed');
                    cell.append(this._renderImage(ScheduledConstants.IMG_CHECK));
                }

                break;
        }
    },

    _renderImage: function (image) {
        return DOM.renderIMG({
            image: image,
            cssClass: 'blockDatesPastWeek_image',
            cssFields: {
                'margin-left': ScheduledConstants.CELL_WIDTH / 2 - 7 + 'px'
            }
        });
    }
});

var BlocksDatesInFutureWeeks =  Base.extend({

    _weekID: null,

    constructor: function (option, eventManager) {
        this._gridCell = option.devExpressOption.cellElement;
        this._dates = option.cellDates;
        this._blockIndex = option.blockIndex;
        this._eventManager = eventManager;
        this._cellPosition = {
            columnIndex: option.devExpressOption.columnIndex,
            rowIndex: option.devExpressOption.rowIndex
        };
        this._weekID = 'futureWeek_' + this._blockIndex;
        this._hasHighlight = false;
        this._render();
    },
    
    _render: function () {
        this._gridCell.css({
            padding: 0
        });
        this._gridCell.html('');
        this._renderWeek();
    },

    _renderWeek: function () {
        var week = DOM.renderDIV({
            id: this._weekID,
            cssClass: 'blockDatesFutureWeek'
        });
        this._gridCell.append(week);

        for (var day = 0; day < 7; day++) {
            var isWork = false;
            for (var i = 0; i < this._dates.length; i++) {
                var date = this._dates[i].date;
                if (date.getDay() == day) {
                    var objDay = new WeekDay({
                        id: this._weekID + '_' + day,
                        data: this._dates[i]
                    }, this, week);
                    //objDay.render()
                    week.append(objDay.render());
                    var dataSource = this._eventManager.callEvent('getDataSource', []);
                    var selectData = dataSource.getSelectDay();
                    if (selectData != null) {
                        if (selectData.date.getTime() == this._dates[i].date.getTime()){
                            this._hasHighlight = true;
                        }
                    }
                    if (this._hasHighlight)
                        objDay.highlight();
                    isWork = true;
                } else {

                }
            }
            if (!isWork) {
                var objDay = new WeekDay({
                    id: this._weekID + '_' + day,
                    data: null
                });
                week.append(objDay.render());
            }
        }
        if (this._eventManager.isEvent('highlightBlock')) {
            this._eventManager.callEvent('highlightBlock',[]);
        }
    },

    moveInNextWeek: function (dayData) {
        var column = this._cellPosition.columnIndex + 1;
        this._moveBlock(column, dayData);
    },

    moveInPrevWeek: function (dayData) {
        var column = this._cellPosition.columnIndex - 1;
        this._moveBlock(column, dayData);
    },


    _moveBlock: function (column, dayData) {
        var gridCell = this._eventManager.callEvent('getGridCell', [this._cellPosition.rowIndex, column]);
        this._dates[0] = dayData;

        if (gridCell) {
            //this._renderScheduledCell();
            this._gridCell.find('#' + this._weekID)[0].remove();
            this._gridCell = gridCell;
            this._cellPosition.columnIndex = column;
            this._hasHighlight = true;
            this._render();
            this._hasHighlight = false;
        }
    },
    
    hasMoveInNextWeek: function (day) {
        var newDate = new Date(day.date.getTime());
        newDate.setDate(newDate.getDate() + 7);
        return {
            move: this._isMove(day, newDate),
            date: newDate
        };
    },

    hasMoveInPrevWeek: function (day) {
        var newDate = new Date(day.date.getTime());
        newDate.setDate(newDate.getDate() - 7);
        return {
            move: this._isMove(day, newDate),
            date: newDate
        };
    },

    _isMove: function (day, newDay) {
        var oldWeek = day.date.getWeek();
        var newWeek = newDay.getWeek();

        var today = new Date();
        if (today.getTime() > newDay.getTime()) {
            return false;
        }

        var dataSource = this._eventManager.callEvent('getDataSource', []);
        var is = dataSource.getWork(day.record.getValue(PlannedDS.DataField_PmsId), newDay);
        if (is == null) {
            return true;
        }
        return false;
    }


});

var WeekDay = Base.extend({
    _id: null,
    _data: null,
    _clickCount: 0,
    _isArrowClick: false,

    constructor: function (option, week) {
        this._data = option.data;
        this._week = week;
        this._id = option.id;
        this._clickCount = 0;
        this._isArrowClick = false;
    },

    render: function () {
        var option = {
            id: this._id,
            cssFields: {
                float: 'left',
                width: (100/7) +'%',
                height: 36
            }
        };
        var day = DOM.renderDIV(option);
        if (this._data == null) {
            return day;
        }

        switch (this._data.status) {
            case PlannedDS.FutureJobs_Planned:
                day.addClass('blockDatesFutureWeek_work');
                break;
            case PlannedDS.FutureJobs_Generated:
                day.addClass('blockDatesFutureWeek_open_work');
                break;
        }

        var scope = this;


        day.on('click', function () {
            scope.clickBlock();
        });

        return day;
    },


    clickBlock: function () {
        if (!this._isArrowClick) {
            if (this._clickCount == 0) {
                if (!this._isHighlight) {
                    this.highlight();
                    if (this._week._eventManager.isEvent('rehighlightBlock')) {
                        this._week._eventManager.callEvent('rehighlightBlock',[]);
                    }
                    this._week._eventManager.addEventListener('rehighlightBlock', this.rehighlight, this);
                    this._week._eventManager.addEventListener('highlightBlock', this.highlight, this);
                    this._clickCount++;
                } else {
                    var day = DOM.returnDOM(this._id);
                    var imgs = day.find('img');
                    if (imgs.length == 0) {
                        this.highlight();
                        if (this._week._eventManager.isEvent('rehighlightBlock')) {
                            this._week._eventManager.callEvent('rehighlightBlock',[]);
                        }
                        this._week._eventManager.addEventListener('rehighlightBlock', this.rehighlight, this);
                        this._week._eventManager.addEventListener('highlightBlock', this.highlight, this);
                        this._clickCount++;
                        return;
                    }

                    this.rehighlight();
                    this._week._eventManager.removeEvent('rehighlightBlock');
                    this._week._eventManager.removeEvent('highlightBlock');
                    this._clickCount = 0;
                }
                return;
            }
            if (this._clickCount == 1) {
                this.rehighlight();
                this._week._eventManager.removeEvent('rehighlightBlock');
                this._week._eventManager.removeEvent('highlightBlock');
                this._clickCount = 0;
            }
        }
        this._isArrowClick = false;

    },

    renderArrows: function () {
        var day = DOM.returnDOM(this._id);
        var scope = this;
        var nextDate = this._week.hasMoveInNextWeek(this._data);

        if(nextDate.move) {

            var next = DOM.renderIMG({
                image: 'next-week.png',
                cssClass: 'blockDatesFutureWeek_arrow_next',
                onClick: function () {
                    scope._data.date = nextDate.date;
                    scope._isArrowClick = true;
                    scope._week.moveInNextWeek(scope._data);
                }
            });

            day.append(next);
        }

        var prevDate = this._week.hasMoveInPrevWeek(this._data);

        if(prevDate.move) {
            var prev = DOM.renderIMG({
                image: 'prev-week.png',
                cssClass: 'blockDatesFutureWeek_arrow_prev',
                onClick: function () {
                    scope._data.date = prevDate.date;
                    scope._isArrowClick = true;
                    scope._week.moveInPrevWeek(scope._data);
                }
            });
            day.append(prev);
            var cssLeft = prev.css('left');
            var left = parseInt(cssLeft.substring(0, cssLeft.length - 2)) - 22;
            prev.css('left', left + 'px');
        }
    },

    removeArrows: function () {
        var day = DOM.returnDOM(this._id);
        var imgs = day.find('img');
        for (var i = 0; i < imgs.length; i++) {
            imgs[i].remove();
        }
    },

    refreshHighlight: function () {
        this.rehighlight();
        this.highlight();
    },

    highlight: function () {
        if (!this._isHighlight) {
            var dataSource = this._week._eventManager.callEvent('getDataSource', []);
            dataSource.saveSelectDay(this._data);
            var day =  DOM.returnDOM(this._id);
            day.addClass('blockDatesFutureWeek_highlight');
            this.renderArrows();
            this._isHighlight = true;
        }
    },
    
    rehighlight: function () {
        this._isHighlight = false;
        var dataSource = this._week._eventManager.callEvent('getDataSource', []);
        dataSource.saveSelectDay(null);
        DOM.returnDOM(this._id).removeClass('blockDatesFutureWeek_highlight');
        this.removeArrows();
        this._clickCount = 0;
    }


});

