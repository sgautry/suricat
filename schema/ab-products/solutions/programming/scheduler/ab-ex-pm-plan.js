/*
 * Controller for the test view for the Bali6 advanced grid control.
 * It shows how to initialize the grid control.
 */
View.createController('advancedGridCategories', {

    afterViewLoad: function() {
        var columnDefinitions = [
            {
                id: 'pms_id',
                fieldName: 'pms.pms_id',
                visible: true,
                type: 'string',

            }, {
                id: 'eq_id',
                fieldName: 'pms.eq_id',
                title: 'Equipment Code',
                type: 'string',
                groupBy: true,
            }, {
                id: 'site_id',
                fieldName: 'pms.site_id',
                title: 'Site Code',
                type: 'string'
            }, {
                id: 'location',
                fieldName: {
                    name: 'vf_location',
                    baseField: ['pms.bl_id', 'pms.fl_id', 'pms.rm_id'],
                    displayFormat: '{0}-{1}-{2}'
                },
                title: 'Location',
                type: 'string'
            }, {
                id: 'pmp_id',
                fieldName: 'pms.pmp_id',
                title: 'PM Procedure',
                type: 'string',
            }
        ];

        this._scheduler = View.createControl({
            control: 'PMPlanner',
            panel: 'gridPanel',
            container: 'schedulerContainer',
            dataSource: 'dataDS',
            columns: columnDefinitions
        });
    }
});