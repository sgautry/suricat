/**
 * The example controller.
 */
View.createController('laborBoardExample', {

	afterInitialDataFetch: function() {
		View.createControl({
		    control: 'LaborBoardControl',
		    container: 'laborBoardContainer',
		    panel: 'laborBoard',
		    resourcesDataSource: 'resourcesDataSource',
		    scheduledWorkDataSource: 'scheduledWorkDataSource',
		    listeners: {
		    }
		});
	}
});

/**
 * The Labor Board control implements https://confluence.archibus.zone/display/PLAT/HTML+Scheduling+Control.
 */
LaborBoardControl = Base.extend({

    /**
     * The configuration object.
     */
    config: null,

    /**
     * The timeline control instance.
     */
    timeline: null,

    // PUBLIC API

    /**
     * Control constructor.
     * @param {config} The control configuration.
     */
    constructor: function(config) {
        this.config = config;

        // Set global scheduler control parameters.
        scheduler.locale.labels.timeline_tab = "Timeline";
        scheduler.locale.labels.section_custom = "Section";
        scheduler.locale.labels.section_important = "Important";
        scheduler.locale.labels.new_event = "Task ";
        scheduler.config.details_on_create = true;
        scheduler.config.details_on_dblclick = true;
        scheduler.config.xml_date = "%m/%j/%Y %g:%i %A";

        // Display important events in red.
        scheduler.templates.event_class = function(start, end, event) {
            event.color = (event.important) ? "#C91E37" : "";
            return "";
        };

        // Set working day from 8:00 to 18:00.
        scheduler.ignore_timeline = function(date) {
            if (date.getHours() < 8 || date.getHours() > 17) return true;
        };

        // Set working week.
        scheduler.date.timeline_start = scheduler.date.week_start;
        scheduler.date.add_timeline = function(date, step, something) {
            return scheduler.date.add(date, step * 7, "day");
        };

        // Load the timeline.
        var weekStart = new Date(2012, 3, 1);
        this.loadSchedule(null, weekStart);
    },

    /**
     * Sets the control height to specified value.
     * @param {height} The height in pixels.
     */
    setHeight: function(height) {},

    /**
     * Adds a listener to specified control event.
     * @param {eventName} The name of the control event, e.g. 'click' or 'locateBuilding'.
     * @param {listener} The listener function reference.
     */
    addEventListener: function(eventName, listener) {},

    /**
     * Removes listeners from specified control event.
     * @param {eventName} The name of the control event, e.g. 'click' or 'locateBuilding'.
     * @param {listener} The listener function reference. Optional. If not specified, the control removes all event listeners for this event.
     */
    removeEventListener: function(eventName) {},

    // IMPLEMENTATION

    /**
     * Loads the schedule for specified list of resources (e.g. craftspeople) and for specified week.
     * Triggers the afterRefresh event.
     *
     * @param {resourceRestriction} Ab.data.Restriction to restrict the resources data source, or null.
     * @param {weekStart} Date of the week start.
     */
    loadSchedule: function(resourceRestriction, weekStart) {
        // Load the resources.
        var resourceRecords = View.dataSources.get(this.config.resourcesDataSource).getRecords(resourceRestriction);
        var sections = [];
        _.each(resourceRecords, function(record) {
            sections.push({
                key: record.getValue('cf.cf_id'),
                label: record.getValue('cf.name')
            });
        });

        // Create the timeline control.
        this.timeline = scheduler.createTimelineView({
            name:	"timeline",
            dx: 100,
            dy: 75,
            x_unit:	"hour",
            x_date:	"%H",
            x_step: 1,
            x_size: 24*5,
            y_unit:	sections,
            y_property:	"section_id",
            render:"bar",
            round_position: true,
            collision_limit: 1,
            event_dy: 44,
            second_scale:{
                x_unit: "day",
                x_date: "%F %d"
            }
        });
        scheduler.config.lightbox.sections = [
            { name: "important", map_to: "important", type: "checkbox", checked_value: 1, unchecked_value: 0 },
            { name: "description", height: 50, map_to: "text", type: "textarea", focus: true },
            { name: "custom", height: 23, type: "select", options: sections, map_to: "section_id" },
            { name: "time", height: 72, type: "time", map_to: "auto"}
        ];
        scheduler.init(this.config.container, weekStart, "timeline");

        // Add a custom listener that appends the Unscheduled Hours UI element to each resource section.
        scheduler.attachEvent("onScaleAdd", function (sectionElement, sectionId) {
            var tableBody = sectionElement.getElementsByTagName('table')[0].firstChild;
            var unscheduledHoursRow = document.createElement('tr');
            for (var i = 0; i < 5; i++) {
                var unscheduledHoursCell = document.createElement('td');
                unscheduledHoursCell.colSpan = 24;
                unscheduledHoursCell.className = 'unscheduled_hours_cell';

                // test code
                if (i < 2) {
                    unscheduledHoursCell.bgColor = '#D3E0F3';
                }
                if (i === 2) {
                    unscheduledHoursCell.bgColor = '#D3E0F3';
                    var unscheduledHoursLabel = document.createElement('span');
                    unscheduledHoursLabel.className = 'unscheduled_hours_label';
                    unscheduledHoursLabel.appendChild(document.createTextNode('Unscheduled hours due this Wed'));
                    unscheduledHoursCell.appendChild(unscheduledHoursLabel);
                }

                unscheduledHoursRow.appendChild(unscheduledHoursCell);
            }
            tableBody.appendChild(unscheduledHoursRow);
        });

        // Add a custom listener that displays drag and drop feedback
        scheduler.attachEvent("onMouseMove", function (id, e) {
            var data = scheduler.getActionData(e);
            // console.log(data.section + ' - ' + data.date);
        });

        // Load data into the timeline.
        var scheduledWorkDataSource = View.dataSources.get(this.config.scheduledWorkDataSource);
        var scheduledWorkRecords = scheduledWorkDataSource.getRecords(resourceRestriction);
        var scheduledWork = [];
        _.each(scheduledWorkRecords, function(record) {
            var cf_id = record.getValue('wrcf.cf_id');
            var start_date = scheduledWorkDataSource.formatValue('wrcf.date_start', record.getValue('wrcf.date_start'), true);
            var start_time = scheduledWorkDataSource.formatValue('wrcf.time_start', record.getValue('wrcf.time_start'), true);
            var end_date = scheduledWorkDataSource.formatValue('wrcf.date_end', record.getValue('wrcf.date_end'), true);
            var end_time = scheduledWorkDataSource.formatValue('wrcf.time_end', record.getValue('wrcf.time_end'), true);
            if (start_time !== '' && end_time !== '') {
                scheduledWork.push({
                    start_date: start_date + ' ' + start_time,
                    end_date: end_date + ' ' + end_time,
                    text: record.getValue('wrcf.wr_id'),
                    section_id: cf_id
                });
            }
        });
        scheduler.parse(scheduledWork, 'json');
    }
});
