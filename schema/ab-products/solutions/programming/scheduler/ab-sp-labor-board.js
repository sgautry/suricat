/**
 * The example controller.
 */
View.createController('laborSchedulingBoard', {

    /**
     * Initialize the timeline control.
     */
    afterInitialDataFetch: function() {
    },

    /**
     * Filters the list of work requests.
     */
    workRequestsFilter_onFilterWorkRequests: function() {
        var restriction = this.workRequestsFilter.getFieldRestriction();
        this.trigger('app:sp:laborBoard:showWorkRequests', restriction);
    },

    /**
     * Clears the filter applied to the list of work requests.
     */
    workRequestsFilter_onClearWorkRequests: function() {
        this.workRequestsFilter.clear();
        this.trigger('app:sp:laborBoard:showWorkRequests', null);
    },

    /**
     * Filters the list of work requests.
     */
    workScheduleFilter_onFilterWorkSchedule: function() {
        var restriction = this.workScheduleFilter.getFieldRestriction();
        this.trigger('app:sp:laborBoard:showWorkSchedule', restriction);
    },

    /**
     * Clears the filter applied to the list of work requests.
     */
    workScheduleFilter_onClearWorkSchedule: function() {
        this.workScheduleFilter.clear();
        this.trigger('app:sp:laborBoard:showWorkSchedule', null);
    }
});
