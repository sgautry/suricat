/**
 * The example controller.
 */
var LaborSchedulingBoardWorkRequestsController = View.createController('laborSchedulingBoard-workRequests', {

    afterViewLoad: function() {
        this.on('app:sp:laborBoard:showWorkRequests', this.showWorkRequests);
        this.showWorkRequests(null);
    },

    /**
     * Queries and displays work requests for specified restriction.
     * @param restriction
     */
    showWorkRequests: function(restriction) {
        this.workRequestsPanel.parentElement.innerHTML = '';
        this.workRequestsTotalsPanel.parentElement.innerHTML = '';

        // query work requests
        var dataSource = this.workRequestsDataSource;
        var workRequestRecords = dataSource.getRecords(restriction);
        var workRequests = _.map(workRequestRecords, function(record) {
            return {
                wr_id: record.getValue('wr.wr_id'),
                description: Ext.util.Format.ellipsis(record.getValue('wr.description'), 90),
                date_escalation_completion: dataSource.formatValue('wr.date_escalation_completion', record.getValue('wr.date_escalation_completion')),
                date_assigned: dataSource.formatValue('wr.date_assigned', record.getValue('wr.date_assigned')),
                priority_label: record.getValue('wr.priority_label'),
                site_id: record.getValue('wr.site_id'),
                bl_id: record.getValue('wr.bl_id'),
                fl_id: record.getValue('wr.fl_id'),
                rm_id: record.getValue('wr.rm_id'),
                eq_id: record.getValue('wr.eq_id'),
                eq_std: record.getValue('eq.eq_std'),
                pmp_id: record.getValue('wr.pmp_id'),
                pms_id: record.getValue('wr.pms_id'),
                criticality: record.getValue('pms.criticality'),
                trades: record.getValue('wr.trades'),
                prob_type: record.getValue('wr.prob_type')
            };
        });

        var groupedWorkRequests = _.groupBy(workRequests, 'prob_type');
        var problemTypes = _.keys(groupedWorkRequests);
        problemTypes = _.sortBy(problemTypes, function(name) { return name; });
        var workRequestsByProblemType = [];
        _.each(problemTypes, function(problemType) {
            workRequestsByProblemType.push({
                problemType: problemType,
                workRequests: groupedWorkRequests[problemType]
            });
        });

        // query trades per work request
        var workRequestTrades = this.workRequestTradesDataSource.getRecords(restriction);
        _.each(workRequests, function(workRequest) {
            workRequest.trades = _.reduce(workRequestTrades, function(trades, workRequestTrade) {
                var wr_id = workRequestTrade.getValue('wrtr.wr_id');
                if (wr_id === workRequest.wr_id) {
                    if (trades !== '') {
                        trades = trades + ', ';
                    }
                    trades = trades + workRequestTrade.getValue('wrtr.tr_id');
                }
                return trades;
            }, '');
        });

        // calculate totals
        var totalWorkRequests = workRequestRecords.length;
        var totalWorkRequestHours = _.reduce(workRequestTrades, function(hours, workRequestTrade) {
            return hours + Number(workRequestTrade.getValue('wrtr.hours_est'));
        }, 0);

        // display work requests
        var template = View.templates.get('workRequestsTemplate');
        template.render({ workRequestsByProblemType: workRequestsByProblemType }, this.workRequestsPanel.parentElement);

        // display work request totals
        template = View.templates.get('workRequestsTotalsTemplate');
        template.render({ totalWorkRequests: totalWorkRequests, totalWorkRequestHours: totalWorkRequestHours }, this.workRequestsTotalsPanel.parentElement);

        // update vertical scroll bar
        this.workRequestsPanel.updateHeight();
    }
});

/**
 * Expands or collapses work requests for specified problem type.
 * @param problemTypeBox
 */
function toggleWorkRequestGroup(problemTypeBox) {
    problemTypeBox = jQuery(problemTypeBox);
    var next = problemTypeBox.next();
    if (next.is(':visible')) {
        next.hide();
        problemTypeBox.addClass('collapsed');
        problemTypeBox.removeClass('expanded');
    } else {
        next.show();
        problemTypeBox.removeClass('collapsed');
        problemTypeBox.addClass('expanded');
    }

    View.panels.get('workRequestsPanel').updateHeight();
}

/**
 * Opens the Edit Work Request dialog.
 * @param wrId
 */
function editWorkRequest(wrId) {
    LaborSchedulingBoardWorkRequestsController.trigger('app:sp:laborBoard:editWorkRequestDetails', wrId);
}

