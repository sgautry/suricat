
View.createController('exBuildingsDashboardFilter', {

    /**
     * When the user clicks on the Show button, trigger an event for other components to update.
     *
     * Event name: app:solutions:buildingsDashboard:showBuildings
     *
     * Event properties:
     * - buildings: an array of [0..N] building codes (bl.bl_id values);
     * - buildingOptions: an array of [0..3] selected options.
     *                    Supported options: "showWorkRequests", "showOutboundMoves", "showInboundMoves".
     */
    buildingsFilterPanel_onShow: function() {
        var options = {
            buildings: this.buildingsFilterPanel.getFieldMultipleValues('bl.bl_id'),
            buildingOptions: this.buildingsFilterPanel.getCheckboxValues('buildingOptions')
        };
        this.trigger('app:solutions:buildingsDashboard:showBuildings', options);
    }
});