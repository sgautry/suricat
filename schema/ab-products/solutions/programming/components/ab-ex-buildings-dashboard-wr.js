
View.createController('exBuildingsDashboardWorkRequests', {

    /**
     * Subscribe to events.
     */
    afterInitialDataFetch: function() {
        var panel = this.workRequestsPanel;

        // add event listeners for events triggered by other components
        this.on('app:solutions:buildingsDashboard:showWorkRequests', function(buildingId) {
            var restriction = new Ab.view.Restriction();
            restriction.addClause('wr.bl_id', buildingId, '=');

            panel.show(true);
            panel.refresh(restriction);
            panel.showInWindow({
                x: 400,
                y: 400,
                width : 800,
                height : 400,
                closeButton: true
            });
        });
    }
});
