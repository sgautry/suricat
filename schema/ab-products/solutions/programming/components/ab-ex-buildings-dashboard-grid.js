
var exBuildingsDashboardGirdController = View.createController('exBuildingsDashboardGrid', {

    /**
     * Subscribe to application events.
     */
    afterInitialDataFetch: function() {
        this.on('app:solutions:buildingsDashboard:showBuildings', function(options) {
            this.showBuildings(options);
        });
    },

    /**
     * Shows buildings specified by the options object.
     * The options.buildings property contains an array of building IDs.
     * @param options
     */
    showBuildings: function(options) {
        var dataController = View.controllers.get('exBuildingsDashboardData');
        dataController.queryBuildingsData(options);

        // clear previous content
        var container = document.getElementById('buildingsGridContainer');
        container.innerHTML = '';

        // render building outlines, names, and indicators
        var template = View.templates.get('buildingsGridTemplate');
        template.render(options, container);

        // render building images
        this.renderBuildingImagesAndTooltips(options);

        // update panel height to ensure correct vertical scrolling
        this.buildingsGridPanel.updateHeight();
    },

    /**
     * Renders building images and tooltips. Images cannot be loaded using the regular building data query,
     * and have to be downloaded one by one using the DocumentService API. Tooltips need to be localized.
     * @param options
     */
    renderBuildingImagesAndTooltips: function(options) {
        _.each(options.buildingsData, function(buildingData) {
            // load and display the building image
            var buildingImageId = 'buildingImage_' + buildingData.id;
            var keys = {
                'bl_id': buildingData.id
            };
            DocumentService.getImage(keys, 'bl', 'bldg_photo', '1', true, {
                callback: function(image) {
                    dwr.util.setValue(buildingImageId, image);
                },
                errorHandler: function(m, e) {
                    console.log(e);
                }
            });

            // add localized tooltips to building indicators
            // COMPATIBILITY: this code relies on Ext.JS tooltips
            var indicatorsElement = document.getElementById('buildingIndicators_' + buildingData.id);
            indicatorsElement.children[0].setAttribute('ext:qtip', getMessage('buildingWorkRequests'));
            indicatorsElement.children[1].setAttribute('ext:qtip', getMessage('buildingOutboundMoves'));
            indicatorsElement.children[2].setAttribute('ext:qtip', getMessage('buildingInboundMoves'));
        });
    }
});

/**
 * Event listener for the work requests HTML element.
 * @param buildingId
 */
function onShowWorkRequests(buildingId) {
    exBuildingsDashboardGirdController.trigger('app:solutions:buildingsDashboard:showWorkRequests', buildingId);
}