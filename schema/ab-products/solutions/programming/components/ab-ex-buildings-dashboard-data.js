
View.createController('exBuildingsDashboardData', {

    /**
     * Queries data for specified buildings, and adds it to the options object.
     * The options.buildingsData property will contain an array of additional data, one object per building.
     * @param options
     */
    queryBuildingsData: function(options) {
        var buildingsRestriction = new Ab.view.Restriction();
        if (options.buildings.length > 0 && options.buildings[0] != '') {
            buildingsRestriction.addClause('bl.bl_id', options.buildings, 'IN');
        }

        var showWorkRequests = _.contains(options.buildingOptions, 'showWorkRequests');
        this.buildingsDataSource.addParameter('showWorkRequests', showWorkRequests);

        var showInboundMoves = _.contains(options.buildingOptions, 'showInboundMoves');
        this.buildingsDataSource.addParameter('showInboundMoves', showInboundMoves);

        var showOutboundMoves = _.contains(options.buildingOptions, 'showOutboundMoves');
        this.buildingsDataSource.addParameter('showOutboundMoves', showOutboundMoves);

        var buildingsRecords = this.buildingsDataSource.getRecords(buildingsRestriction);

        options.buildingsData = [];
        _.each(buildingsRecords, function(record) {
            options.buildingsData.push({
                id: record.getValue('bl.bl_id'),
                name: record.getValue('bl.name'),
                work_requests: record.getValue('bl.work_requests'),
                inbound_moves: record.getValue('bl.inbound_moves'),
                outbound_moves: record.getValue('bl.outbound_moves')
            });
        });
    },
});
