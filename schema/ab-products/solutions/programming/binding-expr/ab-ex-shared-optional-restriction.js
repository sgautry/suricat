View.createController('exSharedRestrictions', {

    roomsGrid_onEnableOptionalRestriction: function() {
        this.roomsGrid.addParameter('nonEmptyFloorsOnly', true);
        this.roomsGrid.refresh();
    },

    roomsGrid_onDisableOptionalRestriction: function() {
        this.roomsGrid.addParameter('nonEmptyFloorsOnly', false);
        this.roomsGrid.refresh();
    }
})