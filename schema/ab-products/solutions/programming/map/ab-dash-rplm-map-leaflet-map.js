var abRplmMapLeafletMapCtrl = View.createController('abRplmMapLeafletMapCtrl', {
	
	mapControl: null,
	
	afterViewLoad: function(){
		this.createMapControl();
	},
	
	afterInitialDataFetch: function() {
		  //this.map.afterLayout();

		  this.createMarkers();

		  this.showMarkers();
	},

	createMapControl: function(){
		var configObject = new Ab.view.ConfigObject();
    	configObject.mapImplementation = 'Esri';
    	configObject.basemap =  View.getLocalizedString('World Street Map');
		
		this.mapControl = new Ab.leaflet.Map('mapPanel', 'mapDiv', configObject);

		this.mapControl.mapClass.addEventListener('markerClick', this.onMarkerClick, this);
	},

	createMarkers: function() {
		var dataSource = 'dsBuildingMarkers';
		var keyFields = ['bl.bl_id'];
		var geometryFields = ['bl.lon', 'bl.lat'];
		var titleField = 'bl.name';
		var contentFields = ["bl.bl_id", "bl.pr_id", "bl.name", "bl.address", "bl.total_suite_manual_area", "bl.total_suite_usable_area", 
			"bl.manual_area_used_by_others", "bl.usable_area_used_by_others",
	    	"bl.leases_number", "bl.purchasing_cost", "bl.value_book", "bl.value_market"];
		
		var markerProperties = {
			renderer: 'simple',
			radius: 7,
			fillColor: '#e41a1c',
			fillOpacity: 0.90,
			stroke: true,
			strokeColor: '#fff',
			strokeWeight: 1.0
		};	

		this.mapControl.createMarkers(
			dataSource,
			keyFields,
			geometryFields,
			titleField,
			contentFields,
			markerProperties
		);

	},
	
	showMarkers: function() {
		var restriction = new Ab.view.Restriction();	
		restriction.addClause('bl.state_id', 'PA', '=');
		restriction.addClause('bl.lat', '', 'IS NOT NULL');
		restriction.addClause('bl.lon', '', 'IS NOT NULL');

		this.mapControl.showMarkers('dsBuildingMarkers', restriction);	
	},
	
	onMarkerClick: function(assetId , feature){
		var blId = assetId;
		try{
			var blAbstractController = View.controllers.get('abRepmMapBlAbstractCtrl');
			blAbstractController.refreshView(blId);
	
			var leaseController = View.panels.get('viewRow3Col1').contentView.controllers.get('abMapLeaseCtrl');
			leaseController.refreshView(blId);
		} catch (e){		}
	}
})
