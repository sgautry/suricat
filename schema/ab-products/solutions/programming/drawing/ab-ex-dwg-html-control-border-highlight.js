
var exampleController = View.createController('exampleController', {
	svgControl: null,
	

	afterViewLoad: function() {	
    	var parameters = new Ab.view.ConfigObject();
		parameters['divId'] = "svgDiv";
		parameters['pkeyValues'] = {'bl_id':'SRL', 'fl_id': '03'};
    	parameters['drawingName'] = 'srl03';
    	
    	//define basic highlight parameter
    	parameters['highlightParameters'] = [{'view_file':"ab-ex-dwg-html-control-datasources.axvw", 'hs_ds': "highlightNoneDs", 'label_ds':'labelNoneDs'}, {'view_file':"ab-ex-dwg-html-control-datasources.axvw", 'hs_ds': "iBorderHighlightStandardsDs", 'borderHighlight':true}];
		
    	parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
										'LayersPopup': {divId: "svgDiv"},
										'DatasourceSelector': {panelId: "svg_ctrls"}
									};
		
		//enable bordersHighlight datasource selector
		parameters['bordersHighlightSelector'] = true;
		//pass borderSize for border highlight
		parameters['borderSize'] = 18;
		
		//enable legend panels
		parameters['legendPanel'] = 'legendGrid';
		parameters['borderLegendPanel'] = 'borderLegendGrid';
		
		this.svgControl = new Drawing.DrawingControl("svgDiv", "svg_ctrls", parameters);	
		this.svgControl.load(parameters);	
	},
	/**
	 * Exports current drawing image in Docx. Supports to display passed restriction and title.
	 * Similar as Flash drawing control's export.
	 */
	svg_ctrls_onDocx: function(){
		var dwgname = 'srl03';
		
		//Check callReportJob() in ab-dwgviewer.js
		var parameters = {};
		parameters.outputType = 'docx';
		parameters.fullPage = true;
		//a new required parameter for HTML drawing.
		parameters.isHTML = true;
		parameters.orientation = 'landscape';
		//print restriction
		//parameters.printRestriction = printRestriction;
		//parameters.dataSourceId = this.highlightDataSource;
		
		var scope = this;
		this.svgControl.getImageBytes(dwgname, function(image){
				scope._doDocxReport(parameters, image);
			}
	     );
	},
	_doDocxReport: function(parameters, image){
		var jobId =  Workflow.startJob("AbSystemAdministration-generatePaginatedReport-buildDocxFromChart", 'ab-ex-dwg-html-rpt-pdf.axvw', image, 'HTML Drawing', parameters);
		View.openJobProgressBar("Please wait...", jobId, null, function(status) {
  	   		var url  = status.jobFile.url;
  	   		window.location = url;
  	  });
	},
	
	/**
	 * Exports drawing image in PDF. Not be able to display restriction and title.
	 * A simplified paginated report with one single drawing image grasped from screen.
	 */
	svg_ctrls_onPdf: function(){
		var dwgname = 'srl03';
		var pdfParameters = {};
    	pdfParameters.documentTemplate = "/schema/ab-products/solutions/programming/drawing/report-cadplan-imperial-landscape-17x22.docx";
		//required: pass drawingName 
		pdfParameters.drawingName = dwgname;
		pdfParameters.drawingZoomInfo = {};
		var scope = this;
		this.svgControl.getImageBytes(dwgname, function(image){
				pdfParameters.drawingZoomInfo.image = image;
				scope._doPDFReport(pdfParameters);
			}
	     );
	},
	_doPDFReport: function(pdfParameters){
    	 var jobId = Workflow.startJob('AbSystemAdministration-generatePaginatedReport-buildDocxFromView', 'ab-ex-dwg-html-rpt-pdf.axvw', null, pdfParameters, null);
    	 View.openJobProgressBar("Please wait...", jobId, null, function(status) {
     	   		var url  = status.jobFile.url;
     	   		window.open(url);
     	  });
	}
  
});






