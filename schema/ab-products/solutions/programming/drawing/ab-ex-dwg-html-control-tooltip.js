var exampleController = View.createController('example', {
	
	svgControl: null,

	showTooltip: true,

	bl_id: '',
	fl_id: '',
	drawingName: '',
	
	afterViewLoad: function() {	
    	var parameters = new Ab.view.ConfigObject();
		parameters['divId'] = "svgDiv";
		parameters['allowMultipleDrawings'] = 'true';
    	parameters['orderByColumn'] = 'true';
    	parameters['topLayers'] = 'eq;pn;jk;fp';
		parameters['showTooltip'] = (this.showTooltip ? true : false);
		parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
										'LayersPopup': {divId: "svgDiv",
															layers:"rm-assets;rm-labels;eq-assets;eq-labels;jk-assets;jk-labels;fp-assets;fp-labels;pn-assets;pn-labels;background",
															defaultLayers:"rm-assets;rm-labels;eq-assets;jk-assets;fp-assets;pn-assets"
															},
															//before passing labelDepartmentDs datasource, developers could add runtime parameters or set restriction by DataSource APIs
									   'AssetTooltip': {handlers: [{assetType: 'rm', datasource: View.dataSources.get('labelDepartmentDs'), fields: ['rm.rm_id', 'rm.dv_id', 'rm.dp_id']},
									                               {assetType: 'eq'},
									                               {assetType: 'jk', handler: this.onMouseOverJack}]}
									 };

    	// only construct the drawing control once
    	this.svgControl = new Drawing.DrawingControl("svgDiv", "svg_ctrls", parameters);	
    	
    	// overrides Grid.onChangeMultipleSelection to load a drawing
    	this.floorsonly_floors.addEventListener('onMultipleSelectionChange', function(row) {
    		// get selected data from tree
    		exampleController.bl_id = row["rm.bl_id"];
    		exampleController.fl_id = row["rm.fl_id"];
    		exampleController.drawingName = row["rm.dwgname"];
    		
    		var checked = document.getElementById("floorsonly_floors_row" + row.index + "_multipleSelectionColumn").checked;
    		    		
    		// if previously selected bl and fl are the same as current selection, no need to load the drawing again
    		if (checked) {
    			// load relevant svg
    			exampleController.loadSvg();
    		} else {
    			exampleController.unloadSvg();
    		}
	    });

    	this.floorsonly_floors.multipleSelectionEnabled = false;
	},

	
	loadSvg: function() {

		var parameters = new Ab.view.ConfigObject();
		parameters['pkeyValues'] = {'bl_id':this.bl_id, 'fl_id': this.fl_id};
    	parameters['drawingName'] = this.drawingName;
		parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');

    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);	
    	
    },
     
    unloadSvg: function(){
    	var parameters = {};
		parameters['pkeyValues'] = {'bl_id':this.bl_id, 'fl_id': this.fl_id};
    	parameters['drawingName'] = this.drawingName;
    	this.svgControl.unload(parameters);
    },
    
    
    onMouseOverJack: function(params, drawingController, event){
    	if(drawingController.getAddOn("AssetTooltip").showTooltip){
    		alert("You are on Jack [" + drawingController.getController("AssetController").getOriginalAssetId(params['assetId']) + "]");
    	}
    },
    
    onToggleTooltip: function(){
    	this.showTooltip = !this.showTooltip;
    	
    	var paramMap = {};
    	var drawings = this.svgControl.drawingController.getControl().drawings;
    	for(var drawingName in drawings){
        	var drawing = drawings[drawingName];
        	if(drawing){
        		paramMap[drawingName] = drawing.config;
        		
        		drawing.unload();
        		delete drawings[drawingName];
        	}
    	}

    	for(drawingName in paramMap){
    		var parameters = paramMap[drawingName];
    		parameters['showTooltip'] = (this.showTooltip ? 'true' : 'false');
    		
    		// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
        	this.svgControl.load(parameters);	
    	}
    }
});







