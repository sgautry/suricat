/**
 * This example demonstrates how to enable the Asset Panel add-on.  In this example, you can
 * drag and drop an employee label from one valid location to another.  This could be from one room to another,
 * within the same floor plan or across multiple floor plans.  Alternatively, you can also drag and drop an employee label 
 * into a virtual waiting area (a.k.a. "asset panel" or "waiting room").  Depending upon where the label is dropped, different
 * actions could be taken. (https://confluence.archibus.zone/x/JYDo)
 */
var exampleController = View.createController('floorsOnlyController', {
	
	drawingControl: null,
	
	buildings: [],
	
	floors: [],
	
	drawingNames: [],
	
	orderByColumn: false,
	
	assetPanelLoaded: false,
	
	afterViewLoad: function() {	
	
		// define parameters to be used by server-side job
		var parameters = new Ab.view.ConfigObject();
    	parameters['allowMultipleDrawings'] = 'true';
    	parameters['orderByColumn'] = (this.orderByColumn ? 'true' : 'false');
    	
    	parameters['showTooltip'] = true;
    	parameters['highlightParameters'] = [{'view_file':"ab-ex-dwg-html-control-datasources.axvw", 'hs_ds': "highlightStandardsDs", 'label_ds':'labelEmployeesDs', 'label_ht': 0.6}];    	

    	parameters['addOnsConfig'] = { 
    		NavigationToolbar: {divId: "drawingDiv"},
    		LayersPopup: {divId: "drawingDiv", collapsed: true},
    		DatasourceSelector: {divId: "drawingDiv", panelId: "drawingPanel", afterSelectedDataSourceChanged: this.afterSelectedDataSourceChanged},
    		
    		AssetPanel: {
    			divId: "drawingDiv",
    			assetType: 'em',
    			title: "Waiting Room", 
    			dataSourceName: "employeeDS",
    			collapsible: true, 
    			size: {height: 130, width: 380}, 
    			font: {fontFamily:'Arial', fontSize: 11},
    			restrictToHighlights: true,		// default is false, no restriction.  if true, assets can only be dropped into highlighted rooms/locations
    			rowActions: [
    			    {type:'em', title:"Edit", icon: Ab.view.View.contextPath + "/schema/ab-core/graphics/icons/view/edit.png", listener: this.editAsset}, 
    			    {type:'em', title:"Delete", icon: Ab.view.View.contextPath + "/schema/ab-core/graphics/icons/view/delete.png", listener: this.deleteAsset}],
    			onAssetLocationChange: this.assetDropEventHandler.createDelegate(this)
    		}
    	};

    	// only construct the drawing control once
    	this.drawingControl = new Drawing.DrawingControl("drawingDiv", "drawingPanel", parameters);	
    	
    	// overrides Grid.onChangeMultipleSelection to load a drawing
    	this.floorsonly_floors.addEventListener('onMultipleSelectionChange', function(row) {
    		
    		// get selected data from tree
    		var bl_id = row["rm.bl_id"];
    		var fl_id = row["rm.fl_id"];
    		var drawingName = row["rm.dwgname"];
    		
    		var checked = document.getElementById("floorsonly_floors_row" + row.index + "_multipleSelectionColumn").checked;
    		    		
    		// if previously selected bl and fl are the same as current selection, no need to load the drawing again
    		if (checked) {
    			// load relevant svg
    			exampleController.loadSvg(bl_id, fl_id, drawingName);
    			
    			exampleController.buildings.push(bl_id);
    			exampleController.floors.push(fl_id);
    			exampleController.drawingNames.push(drawingName);
    		} else {
    			exampleController.unloadSvg(bl_id, fl_id, drawingName);

    		    var index = exampleController.buildings.indexOf(bl_id);
    			exampleController.buildings.splice(index, 1);
    			exampleController.floors.splice(index, 1);
    			exampleController.drawingNames.splice(index, 1);   		
    		}
	    });

    	this.floorsonly_floors.multipleSelectionEnabled = false;
	},
	
	onOrderByColumn: function(orderByColumn){
		exampleController.orderByColumn = orderByColumn;

		for(var i = 0; i < exampleController.buildings.length; i++){
			exampleController.unloadSvg(exampleController.buildings[i], exampleController.floors[i], exampleController.drawingNames[i]);
        }
   	
    	for(var i = 0; i < exampleController.buildings.length; i++){
    		exampleController.loadSvg(exampleController.buildings[i], exampleController.floors[i], exampleController.drawingNames[i]);
    	}
	},
	
	loadSvg: function(bl_id, fl_id, drawingName) {

		var parameters = new Ab.view.ConfigObject();
		parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id};
    	parameters['drawingName'] = drawingName;
    	parameters['orderByColumn'] = (exampleController.orderByColumn ? 'true' : 'false');
	
    	// load SVG from server and display in SVG panel's  <div id="drawingDiv">    	
    	this.drawingControl.load(parameters);

    	// attach drag and drop events to the room layer, allowing rooms to be droppable targets
		var layer = 'rm';
		this.drawingControl.getAddOn("AssetPanel").addDragDropEventsToRooms(layer, drawingName);
		
		var drawingController = this.drawingControl.drawingController;
		var datasourceSelectorAddOn = drawingController.getAddOn('DatasourceSelector');
		
		// if using labels dataSource selector, only labels from 'labelEmployeesDs' can be draggable
		if (drawingController.config.labelSelector === false || (drawingController.config.labelSelector === true && datasourceSelectorAddOn && drawingController.config.highlightParameters[0].label_ds === 'labelEmployeesDs')) {
			
			// attach drag and drop events to employee labels on the drawing the layer
			this.drawingControl.getAddOn("AssetPanel").addDragDropEventsToLayer(layer, drawingName);			

			if (!this.assetPanelLoaded) {
				
				// load the asset panel pop-up
				this.drawingControl.getAddOn("AssetPanel").loadAssetPanel();
				
				// display the asset panel pop-up
				this.drawingControl.getAddOn("AssetPanel").showAssetPanel();
				
				this.assetPanelLoaded = true;
			} 
		}      	
    },
	    
    unloadSvg: function(bl_id, fl_id, drawingName){
    	var parameters = {};
		parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id};
    	parameters['drawingName'] = drawingName;
    	this.drawingControl.unload(parameters);
    },
	
	/**
	 * Handles asset panel drop event
	 * @param {Object} record 
	 * @param {String} assetType 
	 */
	assetDropEventHandler: function(record, assetType){
		View.alert("Asset Type: "+assetType + ";" +   toJSON(record) );
		
		//returning true  would enable the panel to display the dropped record, otherwise, the panel would ignore the drop event actions.
		return true;
	},
	
	/**
	 * Edits asset record when the asset panel row edit action is selected
	 * 
	 * @param {Object} rowRecord 
	 * @param {String} dataSourceName 
	 */
	editAsset: function(rowRecord, dataSourceName){
		View.alert(toJSON(rowRecord) +" || datasouce name: "+ dataSourceName);
	},
	/**
	 * Deletes asset record when the asset panel row delete action is selected
	 * 
	 * @param {Object} rowRecord 
	 * @param {String} dataSourceName 
	 */
	deleteAsset: function (rowRecord, dataSourceName){
		View.alert(toJSON(rowRecord) +" || datasouce name: "+ dataSourceName);
	},
	
    afterSelectedDataSourceChanged: function(comboId, comboValue, drawingController){    	
    	var layer = 'rm';
    	var datasourceSelectorAddOn = drawingController.getAddOn('DatasourceSelector');
    	
    	var drawingNames = exampleController.drawingNames;    	
    	for (var i=0; i<drawingNames.length; i++) {
    		var drawingName = drawingNames[i];
    		
    		// attach drag and drop events to employee labels on the drawing layer; restrict to labels from 'labelEmployeesDs'
    		if (drawingController.config.labelSelector === false || (drawingController.config.labelSelector === true && datasourceSelectorAddOn && drawingController.config.highlightParameters[0].label_ds === 'labelEmployeesDs')) {
        		drawingController.getAddOn("AssetPanel").addDragDropEventsToLayer(layer, drawingName);       		
        	}
    		
        	// attach drag and drop events to rooms, allowing them to be droppable targets
        	drawingController.getAddOn("AssetPanel").addDragDropEventsToRooms(layer, drawingName);
    	}
    }   
});





