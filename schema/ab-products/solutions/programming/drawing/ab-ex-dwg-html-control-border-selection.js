var exampleController = View.createController('example', {
	svgControl: null,
	InfoWindow: null,
	
	afterViewLoad: function() {		
    	var parameters = new Ab.view.ConfigObject();
		parameters['divId'] = "svgDiv";
    	parameters['allowMultipleDrawings'] = 'true';
    	parameters['orderByColumn'] = 'true';
    	
    	parameters['highlightParameters'] = [{'view_file':"ab-ex-dwg-html-control-datasources.axvw", 'hs_ds': "highlightDepartmentsDs", 'label_ds':'labelNoneDs'}];
    	
		
		parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
											'InfoWindow': {width: '400px', position: 'bottom'},
											'SelectWindow': {assetType: "rm", customEvent: this.onSelectWindow}
									 };
		
		parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickRoom}];
    	
		
    	this.svgControl = new Drawing.DrawingControl("svgDiv", "svg_ctrls", parameters);
    	
    	var selectController = this.svgControl.drawingController.getController("SelectController");
    	//enable diagonal border selection 
    	selectController.setDiagonalSelectionPattern(true);
    	//set diagonal selection border size (default is 20)
    	selectController.setDiagonalSelectionBorderWidth(15);
    	
    	//enable Multiple Selection
    	selectController.setMultipleSelection(true);
    	
    	
    	this.InfoWindow = this.svgControl.getDrawingController().getAddOn('InfoWindow');
    	
    	this.floorsonly_floors.addEventListener('onMultipleSelectionChange', function(row) {
    		// get selected data from tree
    		var bl_id = row["rm.bl_id"];
    		var fl_id = row["rm.fl_id"];
    		var dwgName = row["rm.dwgname"];
    		
    		if (row.row.isSelected()) {
    			// load relevant svg
    			exampleController.loadSvg(bl_id, fl_id, dwgName);
    		} else {
    			exampleController.unloadSvg(bl_id, fl_id, dwgName);
    		}
	    });
    	this.floorsonly_floors.multipleSelectionEnabled = false;
	},

	loadSvg: function(bl_id, fl_id, dwgName) {
		var parameters = new Ab.view.ConfigObject();
		//TODO: passing dwgname should not require bl_id and fl_id
		parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id};
    	parameters['drawingName'] = dwgName;	
    	this.svgControl.load( parameters); 
    },
     
    unloadSvg: function(bl_id, fl_id, dwgName){
    	//TODO: passing dwgname should not require bl_id and fl_id
    	var parameters = {};
		parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id};
    	parameters['drawingName'] = dwgName;
    	this.svgControl.unload(parameters);
    	
    	//XXX: remove its selected rooms from selected list
    	var drawingController =  this.svgControl.drawingController;
    	var selectAssets = drawingController.getController("SelectController").selectedAssets;
    	exampleController.InfoWindow.setText("Selected rooms:<br> ");
    	for(svgId in selectAssets){
    		if(selectAssets[svgId].length > 0){
    			exampleController.InfoWindow.appendText(svgId + ": [" +selectAssets[svgId].join(" | ") + "]");
    			exampleController.InfoWindow.appendText("<br>");
    		}
    		
    	}
    },
    
    onSelectWindow: function(selectAssets, drawingController){
    	exampleController.InfoWindow.setText("Selected rooms:<br> ");
    	for(var svgId in selectAssets){
    		if(selectAssets[svgId].length <= 0) continue;
    		exampleController.InfoWindow.appendText(svgId + ": [" + selectAssets[svgId].join(" | ") + "]");
    		exampleController.InfoWindow.appendText("<br>");
    	}
    },
    
    onClickRoom: function(params, drawingController){
    	var selectController = drawingController.getController("SelectController");
    	selectController.toggleAssetSelection(params['svgId'], params['assetId']);
    	var selectAssets = selectController.selectedAssets;
    	exampleController.InfoWindow.setText("Selected rooms:<br> ");
    	for(var svgId in selectAssets){
    		if(selectAssets[svgId].length <= 0) continue;
    		exampleController.InfoWindow.appendText(svgId + ": [" + selectAssets[svgId].join(" | ") + "]");
    		exampleController.InfoWindow.appendText("<br>");
    	}
    }
});





