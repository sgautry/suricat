var bl_id='';
var fl_id='';

var exampleController = View.createController('example', {
	
	svgControl: null,
	
	afterViewLoad: function() {	
		
		// define parameters to be used by server-side job
    	// define parameters to be used by server-side job
    	var parameters = new Ab.view.ConfigObject();
		parameters['divId'] = "svgDiv";
		parameters['allowMultipleDrawings'] = 'false';
		parameters['multipleSelectionEnabled'] = 'false';
    	parameters['orderByColumn'] = 'true';
		parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
										'LayersPopup': {divId: "svgDiv"}};

    	// only construct the drawing control once
    	this.svgControl = new Drawing.DrawingControl("svgDiv", "svg_ctrls", parameters);	
    	
	},

    loadSvg: function(bl_id, fl_id, drawingName) {
    	
    	// define parameters to be used by server-side job
    	var parameters = new Ab.view.ConfigObject();
    	parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id':fl_id};
    	parameters['drawingName'] = drawingName;

    	// load SVG from server so that we can get all layers
		var drawingBase = this.svgControl.load(parameters);
		var assetTypes = drawingBase.retrieveAssetTypes();
		var handlers = [];
		var fn = this.onClickAsset;
		for(var i =0; i < assetTypes.length; i++){
       		handlers.push({'eventName': 'click', 'assetType' : assetTypes[i], 'handler' : fn});
		}
		parameters['events'] = handlers;
		
		// load SVG again with all the handlers
		this.svgControl.load(parameters);
		 
    },
    
    onClickAsset: function(params, drawingController){
    	// toggle the room if clicked
    	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId']);
    }
    
});

/**
 * click event for tree items
 */
function onClickTreeNode(){
	var controller = View.controllers.get('example');
	var curTreeNode = View.panels.get("floor_tree").lastNodeClicked;
	
	// get selected data from tree
	bl_id = curTreeNode.data["rm.bl_id"];
	fl_id = curTreeNode.data["rm.fl_id"];
    drawingName = curTreeNode.data["rm.dwgname"];
    
	// if previously selected bl and fl are the same as current selection, no need to load the drawing again
	if (bl_id != controller.bl_id || fl_id != controller.fl_id) {
		// load relevant svg
		controller.loadSvg(bl_id, fl_id, drawingName);		
	}
	
	controller.bl_id = bl_id;
	controller.fl_id = fl_id;
}






