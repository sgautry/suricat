/**
 * This example demonstrates how to enable the Asset Panel add-on and switching between inventory and trial floor plans.
 */
var exampleController = View.createController('floorsOnlyController', {
	
	drawingControl: null,
	
	buildings: [],
	
	floors: [],
	
	drawingNames: [],
	
	orderByColumn: false,
	
	assetPanelLoaded: false,
	
	// highlight parameters for inventory floorplans
	inventoryParameters: [{'view_file':"ab-ex-dwg-html-control-datasources.axvw", 'hs_ds': "highlightStandardsDs", 'label_ds':'labelEmployeesDs', 'label_ht': 0.6}],
	
	// highlight parameters for trial floorplans
	trialParameters: [{'view_file':"ab-ex-dwg-html-control-datasources-rmtrial.axvw", 'hs_ds': "highlightTrialDs", 'label_ds':'labelTrialEmployeesDs', 'label_ht': 0.6}],	
	
	afterViewLoad: function() {	
	
		// define parameters to be used by server-side job
		var parameters = new Ab.view.ConfigObject();
    	parameters['allowMultipleDrawings'] = 'true';
    	parameters['orderByColumn'] = (this.orderByColumn ? 'true' : 'false');
    	parameters['highlightParameters'] = this.inventoryParameters;
    	parameters['addOnsConfig'] = { 
    		NavigationToolbar: {divId: "drawingDiv"},
    		LayersPopup: {divId: "drawingDiv"},
    		AssetPanel: {
    			divId: "drawingDiv",
    			assetType: 'em',
    			title: "Waiting Room", 
    			dataSourceName: "employeeDS",
    			collapsible: true, 
    			size: {height: 130, width: 380}, 
    			font: {fontFamily:'Arial', fontSize: 11},
    			restrictToHighlights: false,		// default is false, no restriction.  if true, assets can only be dropped into highlighted rooms/locations
    			rowActions: [
    			    {type:'em', title:"Edit", icon: Ab.view.View.contextPath + "/schema/ab-core/graphics/icons/view/edit.png", listener: this.editAsset}, 
    			    {type:'em', title:"Delete", icon: Ab.view.View.contextPath + "/schema/ab-core/graphics/icons/view/delete.png", listener: this.deleteAsset}],
    			onAssetLocationChange: this.assetDropEventHandler.createDelegate(this)
    		}
    	};

    	// only construct the drawing control once
    	this.drawingControl = new Drawing.DrawingControl("drawingDiv", "drawingPanel", parameters);	
    	
    	// overrides Grid.onChangeMultipleSelection to load a drawing
    	this.floorsonly_floors.addEventListener('onMultipleSelectionChange', function(row) {
    		
    		// get selected data from tree
    		var bl_id = row["rm.bl_id"];
    		var fl_id = row["rm.fl_id"];
    		var drawingName = row["rm.dwgname"];
    		var type = row["rm.type"];
    		var layer_name = row["rm.layer_name"];
    		
    		var checked = document.getElementById("floorsonly_floors_row" + row.index + "_multipleSelectionColumn").checked;
    		    		
    		// if previously selected bl and fl are the same as current selection, no need to load the drawing again
    		if (checked) {
    			// load relevant svg
    			exampleController.loadSvg(bl_id, fl_id, drawingName, type, layer_name);
    			
    			exampleController.buildings.push(bl_id);
    			exampleController.floors.push(fl_id);
    			exampleController.drawingNames.push(drawingName);
    		} else {
    			exampleController.unloadSvg(bl_id, fl_id, drawingName, type, layer_name);

    		    var index = exampleController.buildings.indexOf(bl_id);
    			exampleController.buildings.splice(index, 1);
    			exampleController.floors.splice(index, 1);
    			exampleController.drawingNames.splice(index, 1);   		
    		}
	    });

    	this.floorsonly_floors.multipleSelectionEnabled = false;
	},
	
	loadSvg: function(bl_id, fl_id, drawingName, type, layer_name) {
		var layerInFocus = "";
		var layerOutFocus = "";
		
		var parameters = new Ab.view.ConfigObject();
    	parameters['drawingName'] = drawingName;
    	parameters['orderByColumn'] = (exampleController.orderByColumn ? 'true' : 'false');
    	
    	// pass in different parameters for trial vs. inventory floorplans
    	if (type == 'Trial') {
    		layerInFocus = 'rm_trial';
    		layerOutFocus = 'rm';
    		parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id, 'layer_name': layer_name};
    		parameters['highlightParameters'] = this.trialParameters;
    	} else {
    		layerInFocus = 'rm';
    		layerOutFocus = 'rm_trial';
    		parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id};
    		parameters['highlightParameters'] = this.inventoryParameters;
    	}	

    	// load SVG from server and display in SVG panel's  <div id="drawingDiv">    	
    	this.drawingControl.load(parameters);

    	// move layer in focus to top (ideally, use drawing control API)
		this.moveLayerToTop(layerInFocus, drawingName);
		
		// remove layer out of focus (ideally, use drawing control API)
		this.removeLayer(layerOutFocus, drawingName);

    	// attach drag and drop events to the room layer, allowing rooms to be droppable targets
		this.drawingControl.getAddOn("AssetPanel").addDragDropEventsToRooms(layerInFocus, drawingName);
		
		var drawingController = this.drawingControl.drawingController;
		var datasourceSelectorAddOn = drawingController.getAddOn('DatasourceSelector');
		
		// only labels from 'labelEmployeesDs' can be draggable
		if (drawingController.config.highlightParameters[0].label_ds === 'labelEmployeesDs' || drawingController.config.highlightParameters[0].label_ds === 'labelTrialEmployeesDs') {
			
			// attach drag and drop events to employee labels on the drawing the layer
			this.drawingControl.getAddOn("AssetPanel").addDragDropEventsToLayer(layerInFocus, drawingName);			

			if (!this.assetPanelLoaded) {
				
				// load the asset panel pop-up
				this.drawingControl.getAddOn("AssetPanel").loadAssetPanel();
				
				// display the asset panel pop-up
				this.drawingControl.getAddOn("AssetPanel").showAssetPanel();
				
				this.assetPanelLoaded = true;
			} 
		}      	
    },
    
    moveLayerToTop: function(layerName, drawingName) {
    	var svg = this.drawingControl.drawingController.getControl().drawings[drawingName].getSvg();
    	var assetLayer = svg.select("#" + layerName + '-assets');

    	if (!assetLayer.empty()) {
        	assetLayer.node().parentNode.appendChild(assetLayer.node());
        	assetLayer.node().style.display = '';
    	}
    	
    	var labelLayer = svg.select("#" + layerName + '-labels');
    	if (!labelLayer.empty()) {
    		labelLayer.node().parentNode.appendChild(labelLayer.node());   
    		labelLayer.node().style.display = '';
    	}
    },
    
    removeLayer: function(layerName, drawingName) {
    	var svg = this.drawingControl.drawingController.getControl().drawings[drawingName].getSvg();
    	var assetLayer = svg.select("#" + layerName + '-assets');
    	if (!assetLayer.empty()) {
    		assetLayer.remove(); 		
    	}    	
    	var labelLayer = svg.select("#" + layerName + '-labels');
    	if (!labelLayer.empty()) {
    		labelLayer.remove();
    	}
    },
	    
    unloadSvg: function(bl_id, fl_id, drawingName, type, layer_name){
    	var parameters = {};
    	
    	// pass in different parameters for trial vs. inventory floorplans
    	if (type == 'Trial') {
    		parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id, 'layer_name': layer_name};
    	} else {
    		parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id};    		
    	}   	
    	parameters['drawingName'] = drawingName;
    	
    	this.drawingControl.unload(parameters);
    },
	
	/**
	 * Handles asset panel drop event
	 * @param {Object} record 
	 * @param {String} assetType 
	 */
	assetDropEventHandler: function(record, assetType){
		View.alert("Asset Type: "+assetType + ";" +   toJSON(record) );
		
		//returning true  would enable the panel to display the dropped record, otherwise, the panel would ignore the drop event actions.
		return true;
	},
	
	/**
	 * Edits asset record when the asset panel row edit action is selected
	 * 
	 * @param {Object} rowRecord 
	 * @param {String} dataSourceName 
	 */
	editAsset: function(rowRecord, dataSourceName){
		View.alert(toJSON(rowRecord) +" || datasouce name: "+ dataSourceName);
	},
	/**
	 * Deletes asset record when the asset panel row delete action is selected
	 * 
	 * @param {Object} rowRecord 
	 * @param {String} dataSourceName 
	 */
	deleteAsset: function (rowRecord, dataSourceName){
		View.alert(toJSON(rowRecord) +" || datasouce name: "+ dataSourceName);
	}
});





