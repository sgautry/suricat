/**
 * called by ab-ex-svg-example.axvw
 */

var bl_id='';
var fl_id='';
var drawingName = '';
var plan_type = "1 - ALLOCATION";

var exampleController = View.createController('example', {
	
	svgControl: null,
	
	drawingState: 'select',
	
	connectAssets: [],
	
	afterViewLoad: function() {

		// define parameters to be used by server-side job
    	var parameters = new Ab.view.ConfigObject();
    	parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id':fl_id};
		parameters['divId'] = "svgDiv";
		parameters['allowMultipleDrawings'] = 'true';
		parameters['topLayers'] = 'eq;pn;jk;fp';
		parameters['events'] = [{'eventName': 'contextmenu', 'assetType' : 'rm', 'handler' : this.onContextMenuRoom},
                                {'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickAsset},
                                {'eventName': 'click', 'assetType' : 'eq', 'handler' : this.onClickEquipment, 'bbox': {width: 25, height: 32}},
                                {'eventName': 'click', 'assetType' : 'pn', 'handler' : this.onClickAsset},
                                {'eventName': 'click', 'assetType' : 'jk', 'handler' : this.onClickAsset},
                                {'eventName': 'click', 'assetType' : 'fp', 'handler' : this.onClickAsset}];
		
		parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
											'LayersPopup': {divId: "svgDiv"},
											'AssetTooltip': {handlers: [{assetType: 'rm', datasource: 'labelDepartmentDs', fields: 'rm.rm_id;rm.dv_id;rm.dp_id'},
										                               {assetType: 'eq'},
										                               {assetType: 'jk', handler: this.onMouseOverJack}]
															},
											'InfoWindow': {width: '400px', position: 'bottom', customEvent: this.onCloseInfoWindow}
									 };

    	// only construct the drawing control once
    	this.svgControl = new Drawing.DrawingControl("svgDiv", "svg_ctrls", parameters);	
    	
    	// overrides Grid.onChangeMultipleSelection to load a drawing
    	this.floorsonly_floors.addEventListener('onMultipleSelectionChange', function(row) {
    		
    		// get selected data from tree
    		var bl_id = row["rm.bl_id"];
    		var fl_id = row["rm.fl_id"];
    		var drawingName = row["rm.dwgname"];
    		
    		var checked = document.getElementById("floorsonly_floors_row" + row.index + "_multipleSelectionColumn").checked;
    		    		
    		// if previously selected bl and fl are the same as current selection, no need to load the drawing again
    		if (checked) {
    			// load relevant svg
    			exampleController.loadSvg(bl_id, fl_id, drawingName);
    		} else {
    			exampleController.unloadSvg(bl_id, fl_id, drawingName);
    		}
	    });

    	this.floorsonly_floors.multipleSelectionEnabled = false;
	},

	
	loadSvg: function(bl_id, fl_id, drawingName) {

		var parameters = {};
		parameters['divId'] = "svgDiv";
		parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id};
    	parameters['drawingName'] = drawingName;
	
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);	
    	
    },
     
    unloadSvg: function(bl_id, fl_id, drawingName){
    	var parameters = {};
		parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id};
    	parameters['drawingName'] = drawingName;
    	this.svgControl.unload(parameters);
    },
    
    onHighlight: function(){
    	var numHighlighted = this.svgControl.getDrawingController().getController("HighlightController").highlightAssetsByType('eq', {color: 'yellow', persistFill: true, overwriteFill: true});
    	var missingAssets = this.svgControl.getDrawingController().getController("HighlightController").getMissingAssets("highlight");
		var missingAssetsMsg = 'Unable to find the following assets:';
		var hasError = false;
		if(missingAssets.assets.length > 0){
			missingAssetsMsg += '[' + missingAssets.assets.toString() + ']';
			hasError = true;
		}
		if(hasError){
			this.svgControl.getDrawingController().getAddOn('InfoWindow').setText(missingAssetsMsg);
			this.svgControl.getDrawingController().getController("HighlightController").resetMissingAssets('highlight');
		} else {
			this.svgControl.getDrawingController().getAddOn('InfoWindow').setText(numHighlighted + " equipment(s) are highlighted.");
		}
    	this.drawingState = 'select';
    },

    onTrace: function(){
    	

    	
    	//KB# 3052520.  Trace assets line sometimes goes off the edge of the drawing.
    	//test all <use> elements asset types.
    	var asset1 = null;
    	var asset2 = null;
    	var svgControl = this.svgControl;
    	var assetTypes = ['eq','jk','fp','pn'];
    	for(var i = 0; i < assetTypes.length; i++){
			var assetGroup = assetTypes[i] + "-assets";
	    	d3.selectAll("#" + assetGroup).selectAll('*').filter( function() { return this.parentNode.id === assetGroup; } )
	        	.each(function () {
		        	if(asset1 == null){
		        		asset1 = this.id;
		        	} else {
		        		asset2 = this.id;
		            	svgControl.getDrawingController().getController("HighlightController")
		            		.traceAssets(this.farthestViewportElement.id, asset1, asset2, 'red');
		      			 asset1 = asset2 = null;
		        	}
		        }
	       ); 
    	}
		
		
    	var missingAssets = this.svgControl.getDrawingController().getController("HighlightController").getMissingAssets("trace");
		var missingAssetsMsg = 'Unable to find the following assets:';
		var hasError = false;
		if(missingAssets.assetFrom.length > 0){
			missingAssetsMsg += '\nassetFrom: [' + missingAssets.assetFrom.toString() + ']';
			hasError = true;
		}
		if(missingAssets.assetTo.length > 0){
			missingAssetsMsg += '\nassetTo: [' + missingAssets.assetTo.toString() + ']';
			hasError = true;
		}
		if(hasError){
			this.svgControl.getDrawingController().getAddOn('InfoWindow').setText(missingAssetsMsg);
			this.svgControl.getDrawingController().getController("HighlightController").resetMissingAssets('trace');
		}
		this.drawingState = 'select';
    },
    
    onSelectWindow: function(selectAssets, drawingController){
    	drawingController.getAddOn('InfoWindow').setText("you have selected the assets:\n " + selectAssets.join("\n"));
    },
    
    onReset: function(){
    	this.svgControl.getDrawingController().getController("HighlightController").resetAll();
		this.svgControl.getDrawingController().getAddOn('InfoWindow').setText("All Highlights are cleared.");
    	this.drawingState = 'select';
    },
    
    onConnect: function(){
    	this.drawingState = 'connect';
    	this.connectAssets = [];
		this.svgControl.getDrawingController().getAddOn('InfoWindow').setText("Please pick your 1st equipment!");
    },
    
    onClickAsset: function(params, drawingController){
    	// toggle the room if clicked
    	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId']);
    },
    
    onCloseInfoWindow: function(drawingController){
    	alert("close info bar");
    },
    
    onClickEquipment: function(params, drawingController) {

    	var controller = View.controllers.get('example');
    	var assetIds = params['assetId'];
    	var svgId = params['svgId'];
    	
    	if(controller.drawingState == 'connect'){
    		controller.connectAssets.push(assetIds);
    		
    		if(controller.connectAssets.length == 1){
    			drawingController.getAddOn('InfoWindow').appendText("<br>1st equipment: [" + assetIds + "]");
    			drawingController.getAddOn('InfoWindow').appendText("<br>Please pick your 2nd asset!");
    		} else if(controller.connectAssets.length == 2){
    			drawingController.getAddOn('InfoWindow').appendText("<br>2st equipment: [" + assetIds + "]");
    			drawingController.getController("HighlightController").traceAssets(svgId, controller.connectAssets[0], controller.connectAssets[1], 'purple');
    			controller.connectAssets = [];
    		}	
    	} else {
        	// toggle the room if clicked
        	drawingController.getController("SelectController").toggleAssetSelection(svgId, assetIds);
    	}
    },
    
    onContextMenuRoom: function(assetObject, drawingController, menu){
    	alert("Add your menu here - " + assetObject.assetId);
    }
});







