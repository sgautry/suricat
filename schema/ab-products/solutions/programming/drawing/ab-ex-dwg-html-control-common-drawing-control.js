View.createController('treeControllerEx', {
    afterViewLoad: function () {
        this.floor_tree.setMultipleSelectionEnabled(0);
        // add custom event listener for additional node processing after the user checks/unchecks any node
        this.floor_tree.addEventListener('onChangeMultipleSelection', this.onChangeNodesSelection.createDelegate(this));
    },
    onChangeNodesSelection: function (node) {
        var blId = node.data['rm.bl_id'],
            flId = node.data['rm.fl_id'];
        if (node.isSelected()) {
            showDrawing(blId, flId);
        } else {
            unloadDrawing(blId, flId);
        }
    }
});
/**
 * click event for tree room items
 */
function onClickTreeNode() {
    var curTreeNode = View.panels.get("floor_tree").lastNodeClicked;
    curTreeNode.parent.setSelected(true);
    // get selected data from tree
    var blId = curTreeNode.data["rm.bl_id"];
    var flId = curTreeNode.data["rm.fl_id"];
    var rmId = curTreeNode.data["rm.rm_id"];
    showDrawing(blId, flId, rmId);
}

function showDrawing(blId, flId, rmId) {
    var drawingPanelView = View.panels.get('drawingPanelView');
    var assetDrawingExController = drawingPanelView.getContentFrame().View.controllers.get('drawingControllerEx');
    // if drawing was already opened, just refresh the drawing
    if (assetDrawingExController) {
        assetDrawingExController.refreshDrawingLocation(blId, flId, rmId);
    } else {
        var parameters = new Ab.view.ConfigObject();
        parameters['blId'] = blId;
        parameters['flId'] = flId;
        parameters['rmId'] = rmId;
        drawingPanelView.assetParameters = parameters;
        drawingPanelView.loadView('ab-ex-dwg-html-control-common-drawing-control-config.axvw', null, null);
    }
}

function unloadDrawing(blId, flId) {
    var drawingPanelView = View.panels.get('drawingPanelView');
    var assetDrawingExController = drawingPanelView.getContentFrame().View.controllers.get('drawingControllerEx');
    // if drawing was already opened, just refresh the drawing
    if (assetDrawingExController) {
        assetDrawingExController.unloadDrawing(blId, flId);
    }
}