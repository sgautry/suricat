
var assetsSelected = {};

var exampleController = View.createController('example', {
	
	svgControl: null,
	
	drawingState: 'select',
	
	connectAssets: [],
	
	afterViewLoad: function() {	
		
    	// define parameters to be used by server-side job
    	var parameters = new Ab.view.ConfigObject();
		parameters['divId'] = "svgDiv";
    	parameters['allowMultipleDrawings'] = 'true';
    	parameters['orderByColumn'] = 'true';
		parameters['multipleSelectionEnabled'] = 'true';
		parameters['topLayers'] = 'eq';

		parameters['events'] = [{'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickAsset},
		                        {'eventName': 'click', 'assetType' : 'eq', 'handler' : this.onClickEquipment, 'bbox': {width: 25, height: 32}}];
		
		parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
											'InfoWindow': {width: '400px', position: 'bottom'},
											'SelectWindow': {assetType: "rm;eq", customEvent: this.onSelectWindow}
									 };

    	// only construct the drawing control once
    	this.svgControl = new Drawing.DrawingControl("svgDiv", "svg_ctrls", parameters);	
    	
    	// overrides Grid.onChangeMultipleSelection to load a drawing
    	this.floorsonly_floors.addEventListener('onMultipleSelectionChange', function(row) {
    		
    		// get selected data from tree
    		var bl_id = row["rm.bl_id"];
    		var fl_id = row["rm.fl_id"];
    		var drawingName = row["rm.dwgname"];
    		
    		var checked = document.getElementById("floorsonly_floors_row" + row.index + "_multipleSelectionColumn").checked;
    		    		
    		// if previously selected bl and fl are the same as current selection, no need to load the drawing again
    		if (checked) {
    			// load relevant svg
    			exampleController.loadSvg(bl_id, fl_id, drawingName);
    		} else {
    			exampleController.unloadSvg(bl_id, fl_id, drawingName);
    		}
	    });

    	this.floorsonly_floors.multipleSelectionEnabled = false;
	},

	
	loadSvg: function(bl_id, fl_id, drawingName) {

		var parameters = new Ab.view.ConfigObject();
		parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id};
    	parameters['drawingName'] = drawingName;
		
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters); 
    	
    },
     
    unloadSvg: function(bl_id, fl_id, drawingName){
    	var parameters = {};
		parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id};
    	parameters['drawingName'] = drawingName;
    	this.svgControl.unload(parameters);
    	
    	var svgId = DrawingCommon.retrieveValidSvgId("svgDiv", drawingName);
    	delete assetsSelected[svgId];
    	
    	setInforWindowContent(this.svgControl.drawingController, assetsSelected);
    },
    
    onSelectWindow: function(selectAssets, drawingController){
    	for (var key in selectAssets) {
    		assetsSelected[key] = selectAssets[key];
    	}
   	
    	setInforWindowContent(drawingController, assetsSelected);
    },
    
    onClickAsset: function(params, drawingController){
    	// toggle the room if clicked
    	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId']);
    },
    
    onClickEquipment: function(params, drawingController) {
       	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId']);
		drawingController.getAddOn('InfoWindow').setText("<br>You clicked equipment: [" + params['assetId'] + "]");
    }
});


function setInforWindowContent(drawingController, selectAssets){
	if(assetsSelected){
			drawingController.getAddOn('InfoWindow').setText("you have selected the assets:<br> ");
			for(svgId in selectAssets){
				drawingController.getAddOn('InfoWindow').appendText(svgId + ": [" + selectAssets[svgId].join(" | ") + "]");
			}
			drawingController.getAddOn('InfoWindow').appendText("<br>");
	}
}

