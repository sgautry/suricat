var highlightRoom = false;
var highlightEqs = false;
var highlightJks = false;

var exampleController = View.createController('exampleController', {
	
	svgControl: null,
	
	afterViewLoad: function() {	
		// define parameters to be used by server-side job
    	var parameters = new Ab.view.ConfigObject();
		parameters['divId'] = "svgDiv";
    	parameters['allowMultipleDrawings'] = 'true';
    	parameters['orderByColumn'] = 'true';
    	parameters['afterDrawingLoad'] = this.afterDrawingLoad;
    	parameters['highlightParameters'] = [{'view_file':"ab-ex-dwg-html-control-datasources.axvw", 'hs_ds': "highlightNoneDs", 'label_ds':'labelNoneDs'}];
		parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
										'LayersPopup': {divId: "svgDiv"},
										'DatasourceSelector': {panelId: "svg_ctrls", afterSelectedDataSourceChanged: this.afterSelectedDataSourceChanged},
										'InfoWindow': {width: '400px', position: 'bottom', customEvent: this.onCloseInfoWindow}
									};
		
		this.svgControl = new Drawing.DrawingControl("svgDiv", "svg_ctrls", parameters);	
		this.loadSvg();
	},
	
	loadSvg: function(){

    	var parameters = new Ab.view.ConfigObject();
		parameters['pkeyValues'] = {'bl_id':'SRL', 'fl_id': '03'};
    	parameters['drawingName'] = 'srl03';
    	this.svgControl.load(parameters);	
	},
	
    //Highlight/Clear Single Room
    onHighlightRoom: function(){
    	if(highlightRoom){
    		this.svgControl.getDrawingController().getController("HighlightController").clearAsset('SRL;03;365B', {svgId: 'svgDiv-srl03-svg', persistFill: false, overwriteFill: true});
    		this.svgControl.getDrawingController().getAddOn('InfoWindow').appendText("Room SRL;03;365B is cleared.<br>");
    	}
    	else {
    		this.svgControl.getDrawingController().getController("HighlightController").highlightAsset('SRL;03;365B', {svgId: 'svgDiv-srl03-svg', color: 'blue', persistFill: true, overwriteFill: true});
    		this.svgControl.getDrawingController().getAddOn('InfoWindow').appendText("Room SRL;03;365B is highlighted.<br>");

    	}
    	highlightRoom = !highlightRoom;
    },

    //Highlight Multiple Equipments
    onHighlightEquipments: function(){
    	if(highlightEqs){
        	this.svgControl.getDrawingController().getController("HighlightController").clearAssets(['745361110481', '745361110482','745361110483','745361110448','745361110449',
        	                                                                                                     '745361110450', '745361110451','745361110452','745361110453'], {svgId: 'svgDiv-srl03-svg', persistFill: false, overwriteFill: true});
    		this.svgControl.getDrawingController().getAddOn('InfoWindow').appendText("Highlighted equipment(s) are cleared.<br>");
    	
    	} else{
    		var numHighlighted = this.svgControl.getDrawingController().getController("HighlightController").highlightAssets({'745361110481': {svgId: 'svgDiv-srl03-svg', color: 'green', persistFill: true, overwriteFill: true},
			   '745361110482': {svgId: 'svgDiv-srl03-svg', color: 'green', persistFill: true, overwriteFill: true},
			   '745361110483': {svgId: 'svgDiv-srl03-svg', color: 'green', persistFill: true, overwriteFill: true},
			   '745361110448': {svgId: 'svgDiv-srl03-svg', color: 'green', persistFill: true, overwriteFill: true},
			   '745361110449': {svgId: 'svgDiv-srl03-svg', color: 'green', persistFill: true, overwriteFill: true},
			   '745361110450': {svgId: 'svgDiv-srl03-svg', color: 'green', persistFill: true, overwriteFill: true},
			   '745361110451': {svgId: 'svgDiv-srl03-svg', color: 'green', persistFill: true, overwriteFill: true},
			   '745361110452': {svgId: 'svgDiv-srl03-svg', color: 'green', persistFill: true, overwriteFill: true},
			   '745361110453': {svgId: 'svgDiv-srl03-svg', color: 'green', persistFill: true, overwriteFill: true}
    		});
    		this.svgControl.getDrawingController().getAddOn('InfoWindow').appendText(numHighlighted + " equipment(s) are highlighted.<br>");
    	}
    	highlightEqs = !highlightEqs;
    	
    },

    //Highlight All Jacks
    onHighlightAllJacks: function(){
    	
    	if(highlightJks){
    		this.svgControl.getDrawingController().getController("HighlightController").clearAssetsByType('jk', {persistFill: false, overwriteFill: true});
			this.svgControl.getDrawingController().getAddOn('InfoWindow').appendText("Highlighted jack(s) are cleared.<br>");
    	} else {
    		var numHighlighted = this.svgControl.getDrawingController().getController("HighlightController").highlightAssetsByType('jk', {color: 'red', persistFill: true, overwriteFill: true});

	    	var missingAssets = this.svgControl.getDrawingController().getController("HighlightController").getMissingAssets("highlight");
			var missingAssetsMsg = 'Unable to find the following assets:';
			var hasError = false;
			if(missingAssets.assets.length > 0){
				missingAssetsMsg += '[' + missingAssets.assets.toString() + ']';
				hasError = true;
			}
			if(hasError){
				this.svgControl.getDrawingController().getAddOn('InfoWindow').appendText(missingAssetsMsg);
				this.svgControl.getDrawingController().getController("HighlightController").resetMissingAssets('highlight');
			} else {
				this.svgControl.getDrawingController().getAddOn('InfoWindow').appendText(numHighlighted + " jack(s) are highlighted.<br>");
			}
    	}
    	highlightJks = !highlightJks;
    },
    
    // clear all highlights
    onReset: function(){
    	this.svgControl.getDrawingController().getController("HighlightController").resetAll();
		this.svgControl.getDrawingController().getAddOn('InfoWindow').setText("All Highlights are cleared.<br>");
		highlightRoom = false;
		highlightEqs = false;
		highlightJks = false;
    },
    
    afterSelectedDataSourceChanged: function(comboId, comboValue, drawingController){
    	drawingController.getAddOn('InfoWindow').appendText("Called afterSelectedDataSourceChanged event handler:<br>");
    	drawingController.getAddOn('InfoWindow').appendText("Selector Combo Id=" + comboId + ", Combo Value=" + comboValue + "<br>");
    },
    
    afterDrawingLoad: function(divId, drawingName, drawingController){
    	drawingController.getAddOn('InfoWindow').appendText("Called afterDrawingLoad event handler:<br>");
    	drawingController.getAddOn('InfoWindow').appendText("divId=" + divId + ", drawingName=" + drawingName + "<br>");
    }
});






