

var multipleDrawingLayoutController = View.createController('floorsOnlyController', {
	
	svgControl: null,
	
	buildings: [],
	
	floors: [],
	
	drawingNames: [],
	
	orderByColumn: true,
	
	afterViewLoad: function() {	
	
		// define parameters to be used by server-side job
		var parameters = new Ab.view.ConfigObject();
    	parameters['allowMultipleDrawings'] = 'true';
    	parameters['orderByColumn'] = (this.orderByColumn ? 'true' : 'false');
    	parameters['highlightParameters'] = [{'view_file':"ab-ex-dwg-html-control-datasources.axvw", 'hs_ds': "highlightNoneDs", 'label_ds':'labelNoneDs'}];
    	
    	parameters['events'] = [
                                {'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickAsset},
                                {'eventName': 'click', 'assetType' : 'eq', 'handler' : this.onClickAsset},
                                {'eventName': 'click', 'assetType' : 'pn', 'handler' : this.onClickAsset},
                                {'eventName': 'click', 'assetType' : 'jk', 'handler' : this.onClickAsset},
                                {'eventName': 'click', 'assetType' : 'fp', 'handler' : this.onClickAsset}];
    	
    	parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
    									'LayersPopup': {divId: "svgDiv"},
    									'DatasourceSelector': {divId: "svgDiv", panelId: "svg_ctrls"},
    									'InfoWindow': {width: '400px', position: 'bottom'}
    								 };

    	// only construct the drawing control once
    	this.svgControl = new Drawing.DrawingControl("svgDiv", "svg_ctrls", parameters);	
    	
    	//Sets layer selector panel to be collapsed
		this.svgControl.getAddOn('LayersPopup').collapsed = true;
		
    	// overrides Grid.onChangeMultipleSelection to load a drawing
    	this.floorsonly_floors.addEventListener('onMultipleSelectionChange', function(row) {
    		
    		// get selected data from tree
    		var bl_id = row["rm.bl_id"];
    		var fl_id = row["rm.fl_id"];
    		var drawingName = row["rm.dwgname"];
    		
    		var checked = document.getElementById("floorsonly_floors_row" + row.index + "_multipleSelectionColumn").checked;
    		    		
    		// if previously selected bl and fl are the same as current selection, no need to load the drawing again
    		if (checked) {
    			// load relevant svg
    			multipleDrawingLayoutController.loadSvg(bl_id, fl_id, drawingName);
    			
    			multipleDrawingLayoutController.buildings.push(bl_id);
    			multipleDrawingLayoutController.floors.push(fl_id);
    			multipleDrawingLayoutController.drawingNames.push(drawingName);
    		} else {
    			multipleDrawingLayoutController.unloadSvg(bl_id, fl_id, drawingName);

    		    var index = multipleDrawingLayoutController.buildings.indexOf(bl_id);
    			multipleDrawingLayoutController.buildings.splice(index, 1);
    			multipleDrawingLayoutController.floors.splice(index, 1);
    			multipleDrawingLayoutController.drawingNames.splice(index, 1);
    		
    		}
	    });

    	this.floorsonly_floors.multipleSelectionEnabled = false;
	},
	
	onOrderByColumn: function(orderByColumn){
		multipleDrawingLayoutController.orderByColumn = orderByColumn;

		for(var i = 0; i < multipleDrawingLayoutController.buildings.length; i++){
    		multipleDrawingLayoutController.unloadSvg(multipleDrawingLayoutController.buildings[i], multipleDrawingLayoutController.floors[i], multipleDrawingLayoutController.drawingNames[i]);
        }
   	
    	for(var i = 0; i < multipleDrawingLayoutController.buildings.length; i++){
    		multipleDrawingLayoutController.loadSvg(multipleDrawingLayoutController.buildings[i], multipleDrawingLayoutController.floors[i], multipleDrawingLayoutController.drawingNames[i]);
    	}
	},
	
	loadSvg: function(bl_id, fl_id, drawingName) {

		this.svgControl.getDrawingController().getAddOn('InfoWindow').appendText("load drawing " + drawingName + "<br>");
		
		var parameters = new Ab.view.ConfigObject();
		parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id};
    	parameters['drawingName'] = drawingName;
    	parameters['orderByColumn'] = (multipleDrawingLayoutController.orderByColumn ? 'true' : 'false');
	
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);	
    	
		this.svgControl.getDrawingController().getAddOn('InfoWindow').appendText("All drawings are loaded? " + this.svgControl.getDrawingController().isDrawingsLoaded + "<br>");
    },
    
    unloadSvg: function(bl_id, fl_id, drawingName){
    	this.svgControl.getDrawingController().getAddOn('InfoWindow').appendText("unload drawing " + drawingName + "<br>");
		
    	var parameters = {};
		parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id};
    	parameters['drawingName'] = drawingName;
    	this.svgControl.unload(parameters);

    	this.svgControl.getDrawingController().getAddOn('InfoWindow').appendText("All drawings are loaded? " + this.svgControl.getDrawingController().isDrawingsLoaded + "<br>");
    },
    
    onClickAsset: function(params, drawingController){
    	// toggle the room if clicked
    	drawingController.getController("SelectController").toggleAssetSelection(params['svgId'], params['assetId']);
    }
});





