var exampleController = View.createController('example', {
	
	svgControl: null,
	
	afterViewLoad: function() {	
		
		// define parameters to be used by server-side job
    	// define parameters to be used by server-side job
    	var parameters = new Ab.view.ConfigObject();
		parameters['divId'] = "svgDiv";
		parameters['allowMultipleDrawings'] = 'true';
    	parameters['orderByColumn'] = 'true';
		parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv"},
				   'LayersPopup': {divId: "svgDiv", layers: "rm$;rm$TXT;eq-assets;eq-labels;jk-assets;jk-labels;background", defaultLayers: "rm$;rm$TXT;eq-assets;jk-assets", customEvent: onCheckBoxChanged}
				 };

    	// only construct the drawing control once
    	this.svgControl = new Drawing.DrawingControl("svgDiv", "svg_ctrls", parameters);	
    	
    	// overrides Grid.onChangeMultipleSelection to load a drawing
    	this.floorsonly_floors.addEventListener('onMultipleSelectionChange', function(row) {
    		
    		// get selected data from tree
    		var bl_id = row["rm.bl_id"];
    		var fl_id = row["rm.fl_id"];
    		var drawingName = row["rm.dwgname"];
    		
    		var checked = document.getElementById("floorsonly_floors_row" + row.index + "_multipleSelectionColumn").checked;
    		    		
    		// if previously selected bl and fl are the same as current selection, no need to load the drawing again
    		if (checked) {
    			// load relevant svg
    			exampleController.loadSvg(bl_id, fl_id, drawingName);
    		} else {
    			exampleController.unloadSvg(bl_id, fl_id, drawingName);
    		}
	    });

    	this.floorsonly_floors.multipleSelectionEnabled = false;
	},

	
	loadSvg: function(bl_id, fl_id, drawingName) {

		var parameters = new Ab.view.ConfigObject();
		parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id};
    	parameters['drawingName'] = drawingName;
	
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);	
   	},
     
   	unloadSvg: function(bl_id, fl_id, drawingName){
    	var parameters = {};
		parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id};
    	parameters['drawingName'] = drawingName;
    	this.svgControl.unload(parameters);
    }
});

function onCheckBoxChanged(checkbox){
	if(checkbox.checked)
		alert("you checked " + checkbox.id);
	else
		alert("you unchecked " + checkbox.id);
		
}







