var exampleController = View.createController('exampleController', {
	
	svgControl: null,
	
	blId: '',
	
	flId: '',
	
	dwgName: '',
	
	layerName: '',
	
	loadOption: 1,
	
	afterInitialDataFetch: function(){
		this.svg_ctrls.actions.get('captureImage').enableButton(false);
   		this.svg_ctrls.actions.get('saveMarkups').enableButton(false);
	},
	
	afterViewLoad: function() {	

    	var parameters = new Ab.view.ConfigObject();
		parameters['divId'] = "svgDiv";
		parameters['allowMultipleDrawings'] = 'false';
		parameters['addOnsConfig'] = { 'NavigationToolbar': {divId: "svgDiv",
															 afterZoom: this.afterZoom,
															 afterPan: this.afterPan
															},
										'MarkupControl': { divId: "svgDiv",
											              panelId: "markupLegendPanel",
											              legendId: "markupLegendDiv",
											              legendTitle: getMessage("redlineSymbols"),
											              colorPickerId: 'markupLegendPanel_head',
											              onChange: this.onMarkupChange,
											              onDrop: this.onMarkupDrop,
											              onSave: this.onSave
													   },
										'LayersPopup': {divId: "svgDiv"}
		
									 };
		
    	// only construct the drawing control once
    	this.svgControl = new Drawing.DrawingControl("svgDiv", "svg_ctrls", parameters);	
    	
    	var radios = document.getElementsByName('loadOption');
        var prev = null;
        var controller = this;
        for(var i = 0; i < radios.length; i++) {
        	radios[i].onclick = function() {
                if(this !== prev) {
                    prev = this;

                    var curTreeNode = controller.floor_tree.lastNodeClicked;
                	var bl_id = curTreeNode.data["rm.bl_id"];
                	var fl_id = curTreeNode.data["rm.fl_id"];

                	controller.loadOption = parseInt(this.value);

                	if(bl_id && fl_id){
                		var dwgname = curTreeNode.data["rm.dwgname"];
                		
                		if(bl_id && fl_id){
                			// remember bl_id, fl_id, and dwgname
                			controller.loadSvg(bl_id, fl_id, dwgname);
                		}
                	}
                }

            };
        }
        
        radios = document.getElementsByName('layerName');
        var prev1 = null;
        for(i = 0; i < radios.length; i++) {
        	radios[i].onclick = function() {
                if(this !== prev1) {
                    prev1 = this;
                    controller.layerName = this.value;
                    
                    var curTreeNode = controller.floor_tree.lastNodeClicked;
                	var bl_id = curTreeNode.data["rm.bl_id"];
                	var fl_id = curTreeNode.data["rm.fl_id"];

                	if(bl_id && fl_id){
                		var dwgname = curTreeNode.data["rm.dwgname"];
                		
                		if(bl_id && fl_id){
                			// remember bl_id, fl_id, and dwgname
                			controller.loadSvg(bl_id, fl_id, dwgname);
                		}
                	}
                }
            };
        }
        
        this.markupLegendPanel.showInWindow({
		    title: '', 
		    modal: false,
		    collapsible: true, 
		    maximizable: false, 
		    width: 450, 
		    height: 250
		});
   	},

   	afterZoom: function(svg, drawingController){
   		View.panels.get("svg_ctrls").actions.get('saveMarkups').enableButton(true);
   	},
   	
   	afterPan: function(svg, drawingController){
   		View.panels.get("svg_ctrls").actions.get('saveMarkups').enableButton(true);
   	},
   	
   	onMarkupChange: function(node, drawingController){
   		View.panels.get("svg_ctrls").actions.get('saveMarkups').enableButton(true);
   	},
   	
   	onMarkupDrop: function(node, drawingController){
   		View.panels.get("svg_ctrls").actions.get('saveMarkups').enableButton(true);
   	},
	
   	onSave: function(params){
   		if(params.autoNumber)
   			alert("activity_log.activity_log_id=" + params.activityLogId + "\nafm_redline.auto_number=" + params.autoNumber);
   		else
   			alert("activity_log.activity_log_id=" + params.activityLogId + "\nNo markups changed.");
   			
   	},
   	
	loadSvg: function(bl_id, fl_id, drawingName) {

    	var radios = document.getElementsByName('loadOption');
    	for (var i = 0, length = radios.length; i < length; i++) {
    	    if (radios[i].checked) {
    	        // do whatever you want with the checked radio
    	    	this.loadOption = parseInt(radios[i].value);

    	        // only one radio can be logically checked, don't check the rest
    	        break;
    	    }
    	}
    	
    	radios = document.getElementsByName('layerName');
    	for (var i = 0, length = radios.length; i < length; i++) {
    	    if (radios[i].checked) {
    	        // do whatever you want with the checked radio
    	    	this.layerName = radios[i].value;

    	        // only one radio can be logically checked, don't check the rest
    	        break;
    	    }
    	}
    	
    	var params = {
    			'activity_log.project_id': '2015-000001',
    			'activity_log.bl_id': bl_id,
    			'activity_log.fl_id': fl_id,
    			'activity_log.created_by': View.user.name,
    			'activity_log.activity_type': 'PROJECT - TASK',
    			'activity_log.action_title': 'Repaire all the carpets',
    			'activity_log.description': 'This is part of a move project'
    			};
    	
    	var restriction = null;
		if(this.loadOption == 2){
			restriction = new Ab.view.Restriction()
			restriction.addClause('activity_log.status', ['COMPLETED', 'CLOSED'], 'NOT IN', 'AND');
    	} 

    	var sortParams = {
				sortValues: [{'fieldName':'activity_log.activity_log_id', 'sortOrder':-1}]
		};
    	
    	var retrievedRecords = this.svgControl.getController("MarkupDataController").retrieveRecordsFromTable('activity_log', params, restriction, sortParams);
    	
		var parameters = {};
		parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id': fl_id};
    	parameters['drawingName'] = drawingName;
    	parameters['projectId'] = '2015-000001';
		parameters['loadOption'] = this.loadOption;
		parameters['layerName'] = this.layerName;

    	if(retrievedRecords && retrievedRecords.length > 0){
    		parameters['activityLogId'] = retrievedRecords[0].getValue('activity_log.activity_log_id');
    	}
    	
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);
    	
    	this.blId = bl_id;
    	this.flId = fl_id;
    	this.dwgName = drawingName;
    	
   		this.svg_ctrls.actions.get('saveMarkups').enableButton(false);
   		this.svg_ctrls.actions.get('captureImage').enableButton(true);

	},
    
    save: function(){
    	var params = {
			'activity_log.project_id': '2015-000001',
			'activity_log.bl_id': this.blId,
			'activity_log.fl_id': this.flId,
			'activity_log.created_by': View.user.name,
			'activity_log.activity_type': 'PROJECT - TASK',
			'activity_log.action_title': 'Repaire all the carpets',
			'activity_log.description': 'This is part of a move project'
			};
    	
    	var sortParams = {
				sortValues: [{'fieldName':'activity_log.activity_log_id', 'sortOrder':-1}]
		};
		var savedRecord = this.svgControl.getController("MarkupDataController").saveRecordToTable('activity_log', params, sortParams, false);
		
		// here, we do not know the id until after the record is saved
		// so we are forced to set the afterwards
		var activityLogId = savedRecord.getValue('activity_log.activity_log_id');
		
    	var parameters = {};
    	parameters.activityLogId = activityLogId;
    	var pKeyValues = {};
    	pKeyValues['bl_id'] = this.blId;
    	pKeyValues['fl_id'] = this.flId;
    	parameters.pKeyValues = pKeyValues;
    	parameters.drawingName = this.dwgName;
    	parameters.layerName = this.layerName;
    	
    	parameters.viewBox = this.svgControl.getViewBox("svgDiv", this.dwgName);
    	
    	//call MarkupControl's save() in order to invoke the onSave() event handler
    	this.svgControl.getAddOn("MarkupControl").save(parameters, false);
    	
  		this.svg_ctrls.actions.get('saveMarkups').enableButton(false);
    }
    
});

/**
 * Click event for tree items related to the floor.
 */
function onClickFloorTreeNode(){
	var curTreeNode = View.panels.get("floor_tree").lastNodeClicked;
	
	// get selected data from tree
	var bl_id = curTreeNode.data["rm.bl_id"];
	var fl_id = curTreeNode.data["rm.fl_id"];
	var dwgname = curTreeNode.data["rm.dwgname"];
	
	if(bl_id && fl_id){
		// remember bl_id, fl_id, and dwgname
		exampleController.loadSvg(bl_id, fl_id, dwgname);
	}
}
