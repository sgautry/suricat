
<!-- 
	Common list of data sources used for highlighting and labels in the Drawing Control 

	By including this .axvw file in another .axvw file that contains a Drawing Control,
	you will automatically get this list of user selectable highlights to apply 
	in the drawing.
	
-->

<view version="2.0">

  <!-- Highlighting Data Sources -->
  <!-- Divisions -->
  <dataSource id="highlightDivisionsDs" type="DrawingControlHighlight">
     <title>Divisions</title>
     <table name="rm" role="main"/>
     <table name="dv" role="standard"/>
     <field table="rm" name = "bl_id"/>
     <field table="rm" name = "fl_id"/>
     <field table="rm" name = "rm_id"/>
     <field table="dv" name="name" legendKey="true"/>
     <field table="dv" name="hpattern_acad"/>

      <parameter name="occupancy" dataType="verbatim" value="1=1"/>
      <restriction type="sql" sql="${parameters['occupancy']}"/>
  </dataSource>

    <!-- Departments -->
    <dataSource id="highlightDepartmentsDs" type="DrawingControlHighlight">
        <title>Departments</title>
        <table name="rm" role="main"/>
        <table name="dp" role="standard"/>
        <field table="rm" name = "bl_id"/>
        <field table="rm" name = "fl_id"/>
        <field table="rm" name = "rm_id"/>
        <field table="dp" name="dv_id" legendKey="true"/>
        <field table="dp" name="name" legendKey="true"/>
        <field table="dp" name="hpattern_acad"/>

        <parameter name="occupancy" dataType="verbatim" value="1=1"/>
        <restriction type="sql" sql="${parameters['occupancy']}"/>
    </dataSource>
    
    <!-- Room Categories -->
  <dataSource id="highlightCategoriesDs" type="DrawingControlHighlight">
     <title>Room Categories</title>
     <table name="rm" role="main"/>
     <table name="rmcat" role="standard"/>
     <field table="rm" name = "bl_id"/>
     <field table="rm" name = "fl_id"/>
     <field table="rm" name = "rm_id"/>
     <field table="rm" name = "rm_cat" legendKey="true"/>
     <field table="rmcat" name="hpattern_acad"/>

      <parameter name="occupancy" dataType="verbatim" value="1=1"/>
      <restriction type="sql" sql="${parameters['occupancy']}"/>
  </dataSource>

    <!-- Room Super-categories same as the one in space console -->
    <dataSource id="highlightSuperCategoriesDs" type="DrawingControlHighlight">
        <title>Room Super Categories</title>
        <table name="rm" role="main"/>
        <table name="rmcat" role="standard"/>
        <field table="rm" name = "bl_id"/>
        <field table="rm" name = "fl_id"/>
        <field table="rm" name = "rm_id"/>
        <field table="rmcat" name = "supercat" legendKey="true"/>
        <field table="rm" name="hpattern_acad" dataType="text">
            <sql dialect="generic">
                case 
                 when rmcat.supercat = 'USBL' then '14 0 3 65280' 
                 when rmcat.supercat = 'VERT' then '14 0 2 16776960' 
                 when rmcat.supercat = 'SERV' then '14 0 5 255'
                 when rmcat.supercat = 'OTHR' then '14 0 7 10066329' 
                end
            </sql>
        </field>

        <parameter name="occupancy" dataType="verbatim" value="1=1"/>
        <restriction type="sql" sql="${parameters['occupancy']}"/>
    </dataSource>

    <!-- Room Standards -->
  <dataSource id="highlightStandardsDs" type="DrawingControlHighlight">
     <title>Room Standards</title>
     <table name="rm" role="main"/>
     <table name="rmstd" role="standard"/>
     <field table="rm" name = "bl_id"/>
     <field table="rm" name = "fl_id"/>
     <field table="rm" name = "rm_id"/>
     <field table="rm" name = "rm_std" legendKey="true"/>
     <field table="rmstd" name="hpattern_acad"/>

      <parameter name="occupancy" dataType="verbatim" value="1=1"/>
      <restriction type="sql" sql="${parameters['occupancy']}"/>
  </dataSource>
  
  <!-- Room Type -->
  <dataSource id="highlightTypesDs" type="DrawingControlHighlight">
     <title>Room Types</title>
     <table name="rm" role="main"/>
     <table name="rmtype" role="standard"/>
     <field table="rm" name = "bl_id"/>
     <field table="rm" name = "fl_id"/>
     <field table="rm" name = "rm_id"/>
     <field table="rm" name = "rm_type" legendKey="true"/>
     <field table="rmtype" name="hpattern_acad"/>

      <parameter name="occupancy" dataType="verbatim" value="1=1"/>
      <restriction type="sql" sql="${parameters['occupancy']}"/>
  </dataSource>
  
  <!-- Vacant Rooms -->
  <dataSource id="highlightVacantRoomsDs" type="DrawingControlHighlight">
     <title>Vacant Rooms</title>
     <table name="rm" role="main"/> 
         <table name="rmcat" role="standard"/> 
         <field name="bl_id" table="rm"/> 
         <field name="fl_id" table="rm"/> 
         <field name="rm_id" table="rm"/> 
         <field name="area" table="rm"/> 
         <field name="rm_cat" table="rm"/> 
         <field name="rm_type" table="rm"/> 
         <field name="rm_std" table="rm"/> 
         <field name="dv_id" table="rm"/> 
         <field name="dp_id" table="rm"/> 
         <field name="occupiable" table="rmcat" hidden="true"/> 
         <restriction type="sql" sql=" NOT EXISTS (select 1 from em where em.bl_id=rm.bl_id and em.fl_id=rm.fl_id and em.rm_id=rm.rm_id) AND rmcat.occupiable=1 AND rm.cap_em>0 "/> 
  </dataSource>

  <dataSource id="highlightEquipmentsDs" type="DrawingControlHighlight">
     <title>Equipment Names</title>
    <table name="eq" role="main"/>
    <field table="eq" name="bl_id" hidden="true"/>
    <field table="eq" name="fl_id" hidden="true"/>
    <field table="eq" name="rm_id"/>
    <field table="eq" name="eq_id" legendKey="true"/>
  </dataSource>
  
   <!--  None -->  
  <dataSource id="highlightNoneDs" type="DrawingControlHighlight">
     <title>None</title>
     <table name="rm" role="main"/>
     <field table="rm" name = "bl_id"/>
     <field table="rm" name = "fl_id"/>
     <field table="rm" name = "rm_id"/>
     <restriction type="sql" sql="rm_id IS NULL"/>
  </dataSource>

    <!-- Labeling Data Sources -->
    <!-- Names only -->
    <!-- duplicated as labelNoneDs??? -->
  <!-- dataSource id="labelNamesDs" type="DrawingControlLabels">
    <title>Names</title>
    <table name="rm" role="main"/>
    <field name="bl_id" hidden="true"/>
    <field name="fl_id" hidden="true"/>
    <field name="rm_id"/>

      <parameter name="occupancy" dataType="verbatim" value="1=1"/>
      <restriction type="sql" sql="${parameters['occupancy']}"/>
  </dataSource-->
  
  <!-- Names, Room Type, and the Area -->
  <dataSource id="labelDetailsDs" type="DrawingControlLabels">
    <title>Details</title>
    <table name="rm" role="main"/>
    <field name="bl_id" hidden="true"/>
    <field name="fl_id" hidden="true"/>
    <field name="rm_id"/>
    <field name="rm_type"/>
    <field name="area"/>

      <parameter name="occupancy" dataType="verbatim" value="1=1"/>
      <restriction type="sql" sql="${parameters['occupancy']}"/>
  </dataSource>
  
  <!-- Names and the Room Standard -->
  <dataSource id="labelStandardsDs" type="DrawingControlLabels">
    <title>Room Standards</title>
    <table name="rm" role="main"/>
    <field name="bl_id" hidden="true"/>
    <field name="fl_id" hidden="true"/>
    <field name="rm_id"/>
    <field name="rm_std"/>

      <parameter name="occupancy" dataType="verbatim" value="1=1"/>
      <restriction type="sql" sql="${parameters['occupancy']}"/>
  </dataSource>
  
  <!-- Names and the Room Categories -->
  <dataSource id="labelCategoriesDs" type="DrawingControlLabels">
    <title>Room Categories</title>
    <table name="rm" role="main"/>
    <field name="bl_id" hidden="true"/>
    <field name="fl_id" hidden="true"/>
    <field name="rm_id"/>
    <field name="rm_cat"/>

      <parameter name="occupancy" dataType="verbatim" value="1=1"/>
      <restriction type="sql" sql="${parameters['occupancy']}"/>
  </dataSource>

    <!-- Names and the Room Super Categories -->
    <dataSource id="labelSuperCategoriesDs" type="DrawingControlLabels">
        <title>Room Super Categories</title>
        <table name="rm" role="main"/>
        <table name="rmcat" role="standard"/>
        <field name="bl_id" hidden="true"/>
        <field name="fl_id" hidden="true"/>
        <field name="rm_id"/>
        <field table="rmcat" name="supercat"/>

        <parameter name="occupancy" dataType="verbatim" value="1=1"/>
        <restriction type="sql" sql="${parameters['occupancy']}"/>
    </dataSource>

    <!-- Names and Departments -->
  <dataSource id="labelDepartmentDs" type="DrawingControlLabels">
    <title>Departments</title>
    <table name="rm" role="main"/>
    <table name="dp" role="standard"/>
    <field table="rm" name="bl_id" hidden="true"/>
    <field table="rm" name="fl_id" hidden="true"/>
     <field table="rm" name="dv_id" hidden="true"/>
    <field table="rm" name="dp_id" hidden="true"/>
    
    <field table="rm" name="rm_id"/>
    <field table="dp" name="name"/>

      <parameter name="occupancy" dataType="verbatim" value="1=1"/>
      <restriction type="sql" sql="${parameters['occupancy']}"/>
  </dataSource>

    <!-- Divisions -->
    <dataSource id="labelDivisionsDs" type="DrawingControlLabels">
        <title>Divisions</title>
        <table name="rm" role="main"/>
        <table name="dv" role="standard"/>

        <field table="rm" name="bl_id" hidden="true"/>
        <field table="rm" name="fl_id" hidden="true"/>
        <field table="rm" name="dv_id" hidden="true"/>
        <field table="rm" name="dp_id" hidden="true"/>

        <field table="rm" name="rm_id"/>
        <field table="dv" name="name"/>

        <parameter name="occupancy" dataType="verbatim" value="1=1"/>
        <restriction type="sql" sql="${parameters['occupancy']}"/>
    </dataSource>


    <!-- Employee Names -->
    <dataSource id="labelEmployeesDs" type="DrawingControlLabels">
        <title>Employees</title>
        <sql dialect="generic">
            SELECT  rm.dwgname, em.em_id ${sql.as} em_id, rm.bl_id ${sql.as} bl_id, rm.fl_id ${sql.as} fl_id, rm.rm_id ${sql.as} rm_id 
            FROM em RIGHT JOIN rm ON em.bl_id = rm.bl_id AND em.fl_id = rm.fl_id AND em.rm_id = rm.rm_id 
        </sql>
              
        <table name="rm" role="main"/>
        <field table="rm" name="bl_id" hidden="true"/>
        <field table="rm" name="fl_id" hidden="true"/>
        <field table="rm" name="rm_id"/>
        <field table="rm" name="em_id" dataType="text"/>

        <parameter name="occupancy" dataType="verbatim" value="1=1"/>
        <restriction type="sql" sql="${parameters['occupancy']}"/>
    </dataSource>
    
    <dataSource id="labelEquipmentsDs" type="DrawingControlLabels">
        <title>Equipment</title>
        <table name="eq" role="main"/>
        <table name="rm" role="standard"/>
        <field table="rm" name="bl_id" hidden="true"/>
        <field table="rm" name="fl_id" hidden="true"/>
        <field table="rm" name="rm_id"/>
        <field table="eq" name="rm_id"/>
        <field table="eq" name="eq_id"/>
    </dataSource>

  <!--  None -->  
  <dataSource id="labelNoneDs" type="DrawingControlLabels">
     <title>None</title>
     <table name="rm" role="main"/>
     <field table="rm" name = "bl_id"/>
     <field table="rm" name = "fl_id"/>
     <field table="rm" name = "rm_id"/>
     <restriction type="sql" sql="rm_id IS NULL"/>
  </dataSource>
  
  
    <!-- Border highlight datasources --> 
  <dataSource id="iBorderHighlightStandardsDs" type="DrawingControlHighlight">
     <title>Room Standards</title>
     <table name="rm" role="main"/>
     <table name="rmstd" role="standard"/>
     <field table="rm" name = "bl_id"/>
     <field table="rm" name = "fl_id"/>
     <field table="rm" name = "rm_id"/>
     <field table="rm" name = "rm_std" legendKey="true"/>
     <field table="rmstd" name="hpattern_acad"/>
  </dataSource>
  <dataSource id="iBorderHighlightTypesDs" type="DrawingControlHighlight">
     <title>Room Type</title>
     <table name="rm" role="main"/>
     <table name="rmtype" role="standard"/>
     <field table="rm" name = "bl_id"/>
     <field table="rm" name = "fl_id"/>
     <field table="rm" name = "rm_id"/>
     <field table="rm" name = "rm_type" legendKey="true"/>
     <field table="rmtype" name="hpattern_acad"/>
  </dataSource>
  <dataSource id="iBorderHighlightVacantRoomsDs" type="DrawingControlHighlight">
     <title>Vacant Rooms</title>
     <table name="rm" role="main"/>
     <field table="rm" name = "bl_id"/>
     <field table="rm" name = "fl_id"/>
     <field table="rm" name = "rm_id"/>
     <field table="rm" name = "count_em" legendKey="true"/>
     <restriction type="parsed">
      <clause relop="AND" op="=" value="0" name="count_em" table="rm"/>
    </restriction>
  </dataSource>
      <dataSource id="iBorderNoneDs" type="DrawingControlHighlight">
     <title>None</title>
     <table name="rm" role="main"/>
     <field table="rm" name = "bl_id"/>
     <field table="rm" name = "fl_id"/>
     <field table="rm" name = "rm_id"/>
     <restriction type="sql" sql="rm_id IS NULL"/>
  </dataSource>
  
  <!-- room selection-->
    <dataSource id="selectionAllRooms_VPA_Applicable_DS" >
        <title>Departments</title>
        <table name="rm" role="main"/>
        <field table="rm" name = "bl_id"/>
        <field table="rm" name = "fl_id"/>
        <field table="rm" name = "rm_id"/>
    </dataSource>
 
</view>