View.createController('drawingControllerEx', {
    drawingController: null,
    afterViewLoad: function () {
        if (valueExists(this.view.parameters) && valueExists(this.view.parameters.drawingConfig)) {
            // do nothing, let the drawing controller handle this
        } else {
            var actions = [];
            actions.push({
                text: "action1",
                id: 'action1',
                visible: function (assetType) {
                    return 'eq' === assetType;
                },
                listener: function (svgId, actionId, assetType, selectedAssetId) {
                    onAction(this, svgId, actionId, assetType, selectedAssetId);
                }
            });
            actions.push({
                text: "action2",
                id: 'action2',
                visible: function (assetType) {
                    return 'jk' === assetType;
                },
                listener: function (svgId, actionId, assetType, selectedAssetId) {
                    onAction(this, svgId, actionId, assetType, selectedAssetId);
                }
            });
            actions.push({
                text: "action3",
                id: 'action3',
                listener: function (svgId, actionId, assetType, selectedAssetId) {
                    onAction(this, svgId, actionId, assetType, selectedAssetId);
                }
            });
            var reports = [];
            reports.push({
                text: "report1",
                listener: function () {
                    onReport(this, 'report1');
                }
            });
            reports.push({
                text: "report2",
                listener: function () {
                    onReport(this, 'report2');
                }
            });
            reports.push({
                text: "report3",
                listener: function () {
                    onReport(this, 'report3');
                }
            });
            this.drawingController = View.createControl({
                control: 'AssetDrawingControl',
                panel: 'drawingPanel',
                container: 'svgDiv',
                assetTypes: ['rm', 'eq', 'jk', 'fp', 'pn'],
                allowMultipleDrawings: true,
                orderByColumn: false, // default true
                multipleSelectionEnabled: true,
                bordersHighlightSelector: true,
                // diagonalSelection: {
                //     borderWidth: 15
                // },
                selectAssetHighlightColor: 'green',
                borderSize: 14,
                showNavigation: true,
                showTooltip: true,
                print: true,
                reset: true,
                maximize: {
                    // showAsDialog: false,
                    // expand: function () {
                    // },
                    // collapse: function () {
                    // },
                    viewName: 'ab-ex-dwg-html-control-common-drawing-control-config.axvw'
                },
                showPanelTitle: true,
                highlightParameters: [{
                    'view_file': "ab-drawing-controller-common-ds.axvw",
                    'hs_ds': "highlightNoneDs",
                    'label_ds': 'labelNamesDs',
                    'label_ht': '0.60'
                }],
                layersPopup: {
                    layers: "rm-assets;rm-labels;eq-assets;eq-labels;jk-assets;jk-labels;fp-assets;fp-labels;pn-assets;pn-labels;gros-assets;gros-labels;background",
                    defaultLayers: "rm-assets;rm-labels;eq-assets;jk-assets;fp-assets;pn-assets;gros-assets"
                },
                assetTooltip: [
                    {assetType: 'rm', datasource: 'labelStandardsDs', fields: 'rm.rm_id;rm.rm_std'},
                    {assetType: 'eq', datasource: 'tooltipEqDs', fields: 'eq.eq_id;eq.eq_std;'},
                    {assetType: 'jk', datasource: 'tooltipJkDs', fields: 'jk.jk_id;jk.jk_std'},
                    {assetType: 'fp', datasource: 'tooltipFpDs', fields: 'fp.fp_id;fp.fp_std'},
                    {assetType: 'pn', datasource: 'tooltipPnDs', fields: 'pn.pn_id;pn.pn_std'}
                ],
                infoWindow: {
                    width: '400px',
                    position: 'bottom',
                    customEvent: customCloseInfoWindow
                },
                actions: actions,
                reports: reports,
                // Event listeners to auto-wire to control events. Optional.
                listeners: {
                    // called after each drawing is loaded
                    afterDrawingLoaded: function (drawingController, svgId) {
                        drawingController.svgControl.getAddOn('InfoWindow').setText('[afterDrawingLoaded]: Drawing loaded OK ' + svgId);
                    },
                    // called after all drawings are loaded
                    afterDrawingsLoaded: function (drawingController, drawings) {
                        drawingController.svgControl.getAddOn('InfoWindow').appendText('<br>' + '[afterDrawingsLoaded]: All drawing loaded OK ');
                        drawingController.svgControl.getAddOn('InfoWindow').appendText('<br>' + _.keys(drawings).join('<br>'));
                    },
                    // overwrite default onClickAsset
                    // onClickAsset: function (drawingController, svgId, selectedAssetId, assetType) {
                    //     console.log(svgId, assetType, selectedAssetId);
                    //     drawingController.svgControl.getAddOn('InfoWindow').setText('[onClickAsset]: ' + svgId + '; ' + selectedAssetId + '; ' + assetType);
                    //     // toggle the asset if clicked
                    //     drawingController.svgControl.getController("SelectController").toggleAssetSelection(svgId, selectedAssetId);
                    // },
                    // called after the default onClickAsset event
                    onClickAssetAfter: function (drawingController, svgId, selectedAssetId, assetType, selectedAssets, selected) {
                        console.log(svgId, assetType, selectedAssetId, selectedAssets);
                        drawingController.svgControl.getAddOn('InfoWindow').setText('[onClickAssetAfter]: ' + svgId + '; ' + selectedAssetId + '; ' + assetType);
                        drawingController.svgControl.getAddOn('InfoWindow').appendText('<br>No. of selected assets: ' + selectedAssets[svgId][assetType].length);
                        if (!selected) {
                            drawingController.svgControl.getAddOn('InfoWindow').appendText('<br>Asset unselected');
                        }
                        drawingController.displaySelectWindowResult();
                    },
                    // overwrite default onContextMenu
                    // onContextMenu: function (drawingController, svgId, selectedAssetId, assetType, el) {
                    //     console.log(svgId, assetType, selectedAssetId, el);
                    //     drawingController.svgControl.getAddOn('InfoWindow').setText('[onContextMenu]: ' + svgId + '; ' + selectedAssetId + '; ' + assetType + '; [' + [el.clientX, el.clientY] + ']');
                    // },
                    // overwrite default maximize
                    //maximize: function (drawingController) {
                    //    drawingController.svgControl.getAddOn('InfoWindow').setText('[onMaximize]');
                    //    console.log('maximize');
                    //},
                    // overwrite default reset
                    //reset: function (drawingController) {
                    //    drawingController.svgControl.getAddOn('InfoWindow').setText('[onReset]');
                    //    console.log('reset');
                    //},
                    // overwrite default print
                    // print: function (drawingController) {
                    //     drawingController.svgControl.getAddOn('InfoWindow').setText('[onPrint]');
                    //     console.log('print');
                    // }
                    // Set legend text according the legend level value.
                    onRenderLegendLabel: function (row, column, cellElement) {
                        var value = row[column.id];
                        if (column.id == 'legend.value' && value != '') {
                            var text = value;
                            switch (value) {
                                case '0':
                                    text = getMessage('legendLevel0');
                                    break;
                                case '1':
                                    text = getMessage('legendLevel1');
                                    break;
                                case '2':
                                    text = getMessage('legendLevel2');
                                    break;
                                case '3':
                                    text = getMessage('legendLevel3');
                                    break;
                                case '4':
                                    text = getMessage('legendLevel4');
                                    break;
                            }
                            var contentElement = cellElement.childNodes[0];
                            if (contentElement) {
                                contentElement.nodeValue = text;
                            }
                        }
                    }
                }
            });
        }
    },
    afterInitialDataFetch: function () {
        if (this.view.parentViewPanel && this.view.parentViewPanel.assetParameters) {
            var assetParameters = this.view.parentViewPanel.assetParameters;
            var drawing = {
                pkeyValues: {blId: assetParameters.blId, flId: assetParameters.flId},
                findAssets: [{
                    assetType: 'rm',
                    assetIds: [assetParameters.rmId],
                    highlightCss: 'zoomed-asset-red-bordered',
                    //zoom: false
                }],
                // highlightAssets: [{
                //     assetType: 'eq',
                //     assetIds: ['45001'],
                //     color: 'blue'
                // }]
            };
            this.drawingController.showDrawing(drawing);
        }
    },
    /**
     *  Refresh drawing by location.
     *  Called from other views.
     * @param blId - Building id.
     * @param flId - Floor id.
     * @param rmId - Room id.
     */
    refreshDrawingLocation: function (blId, flId, rmId) {
        var drawing = {
            pkeyValues: {blId: blId, flId: flId},
            findAssets: [{
                assetType: 'rm',
                assetIds: [rmId],
                highlightCss: 'zoomed-asset-red-bordered',
                //zoom: false
            }],
            // highlightAssets: [{
            //     assetType: 'eq',
            //     assetIds: ['45001', '45002', '45003'],
            //     color: 'blue'
            // }, {
            //     assetType: 'rm',
            //     assetIds: ['370B', '370A', '371CHS'],
            //     color: 'magenta'
            // }]
        };
        this.drawingController.showDrawing(drawing);
    },
    unloadDrawing: function (blId, flId) {
        this.drawingController.unloadDrawing(blId, flId);
    }
});
function onAction(drawingController, svgId, actionId, assetType, selectedAssetId) {
    drawingController.svgControl.getAddOn('InfoWindow').setText(svgId + ' ' + actionId + ' ' + assetType + ' ' + selectedAssetId);
}
function onReport(drawingController, reportId) {
    drawingController.svgControl.getAddOn('InfoWindow').setText('Report ' + reportId + ' was selected');
}
function customCloseInfoWindow() {
    console.log('customCloseInfoWindow');
}