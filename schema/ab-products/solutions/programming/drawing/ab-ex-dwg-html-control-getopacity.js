
var exampleController = View.createController('exampleController', {
	
	svgControl: null,
	
	afterViewLoad: function() {	
		// define parameters to be used by server-side job
    	var parameters = new Ab.view.ConfigObject();
		parameters['divId'] = "svgDiv";
    	parameters['allowMultipleDrawings'] = 'false';
    	parameters['orderByColumn'] = 'true';
    	parameters['highlightParameters'] = [{'view_file':"ab-ex-dwg-html-control-datasources.axvw", 'hs_ds': "highlightNoneDs", 'label_ds':'labelNoneDs'}];
		
		this.svgControl = new Drawing.DrawingControl("svgDiv", "svg_ctrls", parameters);	
	},
	
	loadSvg: function(){

    	var parameters = new Ab.view.ConfigObject();
		parameters['pkeyValues'] = {'bl_id':'SRL', 'fl_id': '03'};
    	parameters['drawingName'] = 'srl03';
    	this.svgControl.load(parameters);	
	},
	
    //Highlight/Clear Single Room
	retrieveOpacity: function(){
		
   		alert(this.svgControl.drawingController.config.defaultConfig.highlights.assigned.fill.opacity);
    }
});






