var exampleController = View.createController('example', {
	
	svgControl: null,
	
	drawingState: 'select',
	
	connectAssets: [],
	
    afterViewLoad: function() {
    	
    	// define parameters to be used by server-side job
    	var config = new Ab.view.ConfigObject();
    	config['multipleSelectionEnabled'] = 'true';
    	config['showTooltip'] = 'true';
    	config['topLayers'] = 'eq;jk;pn;';
    	config['events'] = [
                                {'eventName': 'click', 'assetType' : 'eq', 'handler' : this.onClickEquipmentOrJack, 'bbox': {width: 25, height: 32}},
                                {'eventName': 'click', 'assetType' : 'jk', 'handler' : this.onClickEquipmentOrJack}
                                ];
    	config['addOnsConfig'] = { 'LayersPopup': {divId: "svgDiv",
											layers:"rm-assets;rm-labels;eq-assets;eq-labels;jk-assets;jk-labels;pn-assets;pn-labels",
											defaultLayers:"rm-assets;eq-assets;jk-assets;;pn-assets"},
										'AssetTooltip': {handlers: [{assetType: 'rm'},
										                            {assetType: 'eq'},
										                            {assetType: 'jk'},
										                            {assetType: 'pn'}]
															},
										'InfoWindow': {width: '400px', position: 'top', customEvent: this.onCloseInfoWindow}
									 };
									 
		this.svgControl = new Drawing.DrawingControl("svgDiv", "svg_ctrls", config);	
		this.loadSvg();
    },
    
    loadSvg: function(){
    	var parameters = {};
    	
    	parameters['pkeyValues'] = {'bl_id':'SRL', 'fl_id':'03'};
    	parameters['drawingName'] = 'srl03';
    	
    	// load SVG from server and display in SVG panel's  <div id="svgDiv">    	
    	this.svgControl.load(parameters);	
    },
    
    onTrace: function(){
		/*
		 * test the <use> with no tranform info 
		 */
		this.svgControl.getDrawingController().getController("HighlightController").traceAssets('svgDiv-srl03-svg', '745361110481', '745361110448', 'red');
		this.svgControl.getDrawingController().getController("HighlightController").traceAssets('svgDiv-srl03-svg', '745361110481', '745361110449', 'red');
		this.svgControl.getDrawingController().getController("HighlightController").traceAssets('svgDiv-srl03-svg', '745361110482', '745361110450', 'blue');
		this.svgControl.getDrawingController().getController("HighlightController").traceAssets('svgDiv-srl03-svg', '745361110482', '745361110451', 'blue');
		this.svgControl.getDrawingController().getController("HighlightController").traceAssets('svgDiv-srl03-svg', '745361110483', '745361110452', 'yellow');
		this.svgControl.getDrawingController().getController("HighlightController").traceAssets('svgDiv-srl03-svg', '745361110483', '745361110453', 'yellow');
		
		this.svgControl.getDrawingController().getController("HighlightController").traceAssets('svgDiv-srl03-svg', '45200', 'SRL03-334-1-D', 'purple');
		this.svgControl.getDrawingController().getController("HighlightController").traceAssets('svgDiv-srl03-svg', 'SRL03-334-1-D', 'SRL03-PN05', 'purple');
		this.svgControl.getDrawingController().getController("HighlightController").traceAssets('svgDiv-srl03-svg', 'SRL03-PN05', 'SRL03-PN01', 'purple');
		
		var missingAssets = this.svgControl.getDrawingController().getController("HighlightController").getMissingAssets("trace");
		var missingAssetsMsg = 'Unable to find the following assets:';
		var hasError = false;
		if(missingAssets.assetFrom.length > 0){
			missingAssetsMsg += '\nassetFrom: [' + missingAssets.assetFrom.toString() + ']';
			hasError = true;
		}
		if(missingAssets.assetTo.length > 0){
			missingAssetsMsg += '\nassetTo: [' + missingAssets.assetTo.toString() + ']';
			hasError = true;
		}
		if(hasError){
			this.svgControl.getDrawingController().getAddOn('InfoWindow').setText(missingAssetsMsg);
			this.svgControl.getDrawingController().getController("HighlightController").resetMissingAssets('trace');
		} 
		this.drawingState = 'select';
    },
    
    onReset: function(){
    	this.svgControl.getDrawingController().getController("HighlightController").resetAll();
		this.svgControl.getDrawingController().getAddOn('InfoWindow').setText("All Highlights are cleared.");
    	this.drawingState = 'select';
    },
    
    onConnect: function(){
    	this.drawingState = 'connect';
    	this.connectAssets = [];
		this.svgControl.getDrawingController().getAddOn('InfoWindow').setText("Please click on your 1st equipment or Jack.");
    },
    
    onCloseInfoWindow: function(drawingController){
    	this.svgControl.getDrawingController().getAddOn('InfoWindow').setText("");
    },
    
    onClickEquipmentOrJack: function(params, drawingController) {

    	var controller = View.controllers.get('example');
    	var assetIds = params['assetId'];
    	var svgId = params['svgId'];
    	if(controller.drawingState == 'connect'){
    		controller.connectAssets.push(assetIds);
    		
    		if(controller.connectAssets.length == 1){
    			drawingController.getAddOn('InfoWindow').appendText("<br>1st equipment/jack: [" + assetIds + "]");
    			drawingController.getAddOn('InfoWindow').appendText("<br>Please pick your 2nd asset!");
    		} else if(controller.connectAssets.length == 2){
    			drawingController.getAddOn('InfoWindow').appendText("<br>2st equipment/jack: [" + assetIds + "]");
    			drawingController.getController("HighlightController").traceAssets('svgDiv-srl03-svg', controller.connectAssets[0], controller.connectAssets[1], 'purple');
    			controller.connectAssets = [];
    		}	
    	} else {
        	// toggle the room if clicked
        	drawingController.getController("SelectController").toggleAssetSelection('svgDiv-srl03-svg', assetIds);
    	}
    }
});







