View.createController('mapController', {

    // the Ab.arcgis.Map control
    mapControl: null,

    // the selected marker style
    markerStyle: null,

    afterViewLoad: function() {
        var mapController = View.controllers.get('mapController');

        var configObject = new Ab.view.ConfigObject();
        var mapConfigObject = new Object();

        // build basemap layer list
        var basemapLayerList = new Ext.util.MixedCollection();
        // "//services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer"
        // "//services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer"
        basemapLayerList.add('Basemap', '//services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer');

        // configure marker menu
        var markerMenu = mapController.mapPanel.actions.get('markerMenu');
        markerMenu.addAction(0, 'Occupancy Count', this.onMarkerStyleChange);
        markerMenu.addAction(1, 'Gross Area', this.onMarkerStyleChange);
        markerMenu.addAction(2, 'Cost per SqFt', this.onMarkerStyleChange);

        // set default style
        this.markerStyle = 'Occupancy Count';

        // set mapConfig params
        mapConfigObject.basemapLayerList = basemapLayerList;

        mapController.mapControl = new Ab.arcgis.ArcGISMap('mapPanel', 'map', configObject, mapConfigObject);
    },

    afterInitialDataFetch: function() {
        var reportTargetPanel = document.getElementById("mapPanel");
        reportTargetPanel.className = 'claro';
    },

    onMarkerStyleChange: function(item){
        var mapController = View.controllers.get('mapController');
        mapController.markerStyle = item.text || 'Occupancy Count';

        mapController.mapControl.removeThematicLegend();
        mapController.buildingPanel_onShowBuildings();
    },

    buildingPanel_onShowBuildings: function() {
        var mapController = View.controllers.get('mapController');

        var markerStyle = mapController.markerStyle;

        // create the marker property to specify building markers for the map
        var markerProperty = new Ab.arcgis.ArcGISMarkerProperty(
            'buildingDS', // datasource
            ['bl.lat', 'bl.lon'], // geometry fields
            ['bl.bl_id'], // datasource key
            ['bl.bl_id', 'bl.use1', 'bl.count_occup', 'bl.area_gross_ext', 'bl.cost_sqft'] // infowindow fields
        );

        // set marker properties
        markerProperty.showLabels = false;
        markerProperty.symbolSize = 15;

        var hexColors = colorbrewer.Set1[5];
        var rgbColors = mapController.mapControl.colorbrewer2rgb(hexColors);
        markerProperty.symbolColors = rgbColors;

        // configure thematic markers
        var thematicBuckets;
        switch (markerStyle) {
            case 'Occupancy Count':
                thematicBuckets = [100,500,1000]; 
                markerProperty.setThematic('bl.count_occup', thematicBuckets);
                break;
            case 'Gross Area':
                var thematicBuckets = [10000, 100000, 500000, 1000000];
                markerProperty.setThematic('bl.area_gross_ext', thematicBuckets);
                break;
            case 'Cost per SqFt':
                thematicBuckets = [10, 20, 50, 100];
                markerProperty.setThematic('bl.cost_sqft', thematicBuckets);
                break;
            default:
                break;
        }

        mapController.mapControl.buildThematicLegend(markerProperty);

        var restriction = new Ab.view.Restriction();
        var selectedRows = mapController.buildingPanel.getSelectedRows();
        if (selectedRows.length !== 0) {
            // create a restriction based on selected rows
            for (var i = 0; i < selectedRows.length; i++) {
                restriction.addClause('bl.bl_id', selectedRows[i]['bl.bl_id'], "=", "OR");
            }
        } 

        markerProperty.setRestriction(restriction);
        mapController.mapControl.updateDataSourceMarkerPropertyPair('buildingDS', markerProperty);
        mapController.mapControl.refresh();

    }
})