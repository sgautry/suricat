View.createController('mapController', {
	
	/**
  * The Ab.arcgis.Map control
  */
	mapControl: null,
	
  afterViewLoad: function(){
    this.configMap();
    this.configMarkers();
  },

  /**
  * Configure the map control.
  */
  configMap: function() {
    // configure map properties
    var configObject = new Ab.view.ConfigObject();
    var mapConfigObject = {
      "mapInitExtent" : [-8371868, 4858206, -8360699, 4864369],
      "mapInitExtentWKID" : 102100
    };
    var mapController = this;
    mapConfigObject.onMapLoad = function() {
      mapController.onMapLoad();
    };
    configObject.mapConfigObject = mapConfigObject;

    //create mapControl
    this.mapControl = new Ab.arcgis.Map('mapPanel', 'mapDiv', configObject);

    //apply esri css to panel
    var reportTargetPanel = document.getElementById("mapPanel");            
    reportTargetPanel.className = 'claro';

  },

  /**
  * Configure the building markers.
  */
  configMarkers: function() {
    // create building marker definition
    var blMarkerProperty = new Ab.arcgis.ArcGISMarkerProperty('bl_ds', ['bl.lat', 'bl.lon'],'bl.bl_id',['bl.address1', 'bl.city_id', 'bl.state_id', 'bl.ctry_id']);
    blMarkerProperty.showLabels = false;
    this.mapControl.updateDataSourceMarkerPropertyPair('bl_ds', blMarkerProperty);
    
    //add the call back function to the graphic mouse click event 
    this.mapControl.addMarkerAction('Show Details', this.showBuildingDetails);
  },


  /**
  * Called by the map control when the map is loaded and ready for action.
  */
  onMapLoad: function() {
    View.log('mapController --> Map loaded!');

    // create map event listeners
    this.mapControl.addEventListener('mapViewChange', this.onMapViewChange, this);
    this.mapControl.addEventListener('mapClick', this.onMapClick, this);
    this.mapControl.addEventListener('markerClick', this.onMarkerClick, this);
    this.mapControl.addEventListener('markerMouseOver', this.onMarkerMouseOver, this);
    this.mapControl.addEventListener('markerMouseOut', this.onMarkerMouseOut, this);
  },
  
  /**
  * Called by the map control when the map view changes.
  */
  onMapViewChange: function(event) {
    View.log('mapController --> Map view changed!');
  },

  /**
  * Called by the map control when map is clicked.
  */
  onMapClick: function(event) {
    View.log('mapController --> Map clicked!');
  },

  /**
  * Called by the map control when marker is clicked.
  */
  onMarkerClick: function(assetId, graphic) {
    View.log('mapController --> Marker clicked!');
  },

  /**
  * Called by the map control when the mouse is moved over a marker.
  */
  onMarkerMouseOver: function(assetId) {
    View.log('mapController --> Marker mouse over!');

  },

  /**
  * Called by the map control when the mouse is moved off a marker.
  */
  onMarkerMouseOut: function(assetId) {
    View.log('mapController --> Marker mouse out!');
  },  
	
  /**
  * Called by the map control to display the building detail form.
  */
	showBuildingDetails: function(title, attributes) {
	
  	var bl_id = title;
  		
  	//openDialog: function(url, restriction, newRecord, x, y, width, height) {
		var restriction = {
        	'bl.bl_id': bl_id
    	};
    
    	var allowCreateRecord = false;
    	var defaultDialogX = 20;
    	var defaultDialogY = 40;
    	var defaultDialogWidth = 800;
    	var defaultDialogHeight = 600; 
  		AFM.view.View.openDialog('ab-arcgis-bl-details-dialog.axvw', restriction, false, 20, 40, 800, 600);   	
	},
  
  /**
  * Map panel action to display the building markers.
  */	
	mapPanel_onShowMarkers: function() {

		var blMarkerProperty = this.mapControl.getMarkerPropertyByDataSource('bl_ds');
		
		if( blMarkerProperty == null ){
			blMarkerProperty = new Ab.arcgis.ArcGISMarkerProperty('bl_ds', ['bl.lat', 'bl.lon'],'bl.bl_id',['bl.address1', 'bl.city_id', 'bl.state_id', 'bl.ctry_id']);
  		blMarkerProperty.showLabels = false;
      this.mapControl.updateDataSourceMarkerPropertyPair('bl_ds', blMarkerProperty);
  	}
	   
		var restriction = new Ab.view.Restriction();
  	restriction.addClause('bl.state_id', 'PA', "=", "OR");
	
		//refresh mapControl
		this.mapControl.refresh(restriction);
	},

  /**
  * Map panel action to clear the building markers.
  */
  mapPanel_onClearMarkers: function() {
    //clear all markers and all saved datasource-Ab.arcgis.ArcGISMarkerProperty pairs
    this.mapControl.clear();
  }
    
});



