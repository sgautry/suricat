View.createController('mapController', {
  
    /** 
    * The Ab.arcgis.MapExtensions control.
    */
    mapControl: null,

    afterViewLoad: function(){
        this.configMap();
        this.configLegend();
    },

    /**
    * Configure the map control.
    */
    configMap: function(){
        // create the config object
        var configObject = new Ab.view.ConfigObject();
        configObject.mapConfigObject = this.createMapConfigObject();
        this.mapControl = new Ab.arcgis.MapExtensions('mapPanel', 'mapDiv', configObject);

        // apply esri css to panel
        var reportTargetPanel = document.getElementById("mapPanel");            
        reportTargetPanel.className = 'claro';
    },

    /**
    * Called by the map controller to create the map configuration options.
    * returns {object}.
    */
    createMapConfigObject: function() {
        var mapConfigObject = new Object();

        mapConfigObject.mapInitExtent = [-16828542.56,-4093342.02,14166978.15,11306578.94];
        mapConfigObject.mapInitExtentWKID = 102100;

        var basemapLayerList = new Array();
        basemapLayerList.push({   
          name: 'Basemap',
          url: '//services.arcgisonline.com/arcgis/rest/services/Canvas/World_Light_Gray_Base/MapServer'
        });
        mapConfigObject.basemapLayerList = basemapLayerList;

        var referenceLayerList = new Array();
        referenceLayerList.push({
          name: 'Countries',
          url: '//tiles.arcgis.com/tiles/lq5SzPW1rLR0h1u9/arcgis/rest/services/hq_countries_white/MapServer'
        });
        mapConfigObject.referenceLayerList = referenceLayerList;

        var featureLayerList = new Array();
        featureLayerList.push({
          name: 'Countries',
          url: '//services3.arcgis.com/lq5SzPW1rLR0h1u9/arcgis/rest/services/ne_countries_110m/FeatureServer/0',
          objectIdField: 'objectid',
          opacity: 0.9,
          index: 75,
          outFields: ['*'],
          zoomToResults: false,
          toolTipField: 'name',
          assetIdField: 'iso_a3',
          assetTypeField: 'ab_asset_type'
        });
        mapConfigObject.featureLayerList = featureLayerList;

        var mapController = this;
        mapConfigObject.onMapLoad = function() {
          mapController.onMapLoad();
        };    

        return mapConfigObject;
    },

    /**
    * Configure the panel legend action.
    */
    configLegend: function() {
        var legendObj = Ext.get('showLegend');
        legendObj.on('click', this.showLegend, this, null);
    },

    /**
    * Called by the map control when the map is loaded and ready for action.
    */
    onMapLoad: function() {
        // add map event listeners
        this.configMapEventListeners();

        // set the map view
        this.mapControl.setMapCenterAndZoom(-10, 50, 2); 

        // load the reference layer
        this.mapControl.switchReferenceLayer('Countries');

        // load the countries feature layer
        this.loadCountriesFeatureLayer();  

    },

    /**
    * Configure the map event listeners.
    */
    configMapEventListeners: function() {
        this.mapControl.addEventListener('basemapLayerLoad', this.onBasemapLayerLoad, this);
        this.mapControl.addEventListener('referenceLayerLoad', this.onReferenceLayerLoad, this);
        this.mapControl.addEventListener('featureLayerLoad', this.onFeatureLayerLoad, this);
        this.mapControl.addEventListener('featureClick', this.onFeatureClick, this);
        this.mapControl.addEventListener('featureMouseOver', this.onFeatureMouseOver, this);
        this.mapControl.addEventListener('featureMouseOut', this.onFeatureMouseOut, this);
    },

    /**
    * Called by the map control when the basemap layer is loaded.
    */
    onBasemapLayerLoad: function(){
        View.log('mapController.onBasemapLayerLoad(): start');

        View.log('mapController.onBasemapLayerLoad(): end');
    },

    /**
    * Called by the map control when the reference layer is loaded.
    */
    onReferenceLayerLoad: function(){
        View.log('mapController.onReferenceLayerLoad(): start');

        // load the countries features
        //this.loadCountriesFeatureLayer();

        View.log('mapController.onReferenceLayerLoad(): end');
    },

    /**
    * Called by the map controller to load the countries feature layer
    */
    loadCountriesFeatureLayer: function() {

        //TODO move this to the core control
        var thematicUniqueValues = this.mapControl._getDistinctFieldValues('ctry.geo_region_id', '1=1');

        var thematicColors = this.mapControl.colorbrewerToRGB(colorbrewer.Set1[3]);
        // var thematicColors = [
        //        [55,126,184],  // blue
        //        [77,175,74],   // greeen
        //        [228,26,28]    // red
        // ];

        // get asset ids from data source
        var assetIds = this.getAllAssetIdsFromDataSource('ctryDs');
        var whereClause = "iso_a3 IN (" + assetIds + ")";

        // create the layer options
        var layerOptions = {
            renderer: 'thematic-unique-values',
            thematicDataSource: 'WC_DATASOURCE',
            dataSource: 'ctryDs',
            //dataSourceParameters: parameters,
            dataSourceRestriction: '1=1',
            //legendDataSuffix: '%',
            keyField: 'ctry.ctry_id',
            thematicField: 'ctry.geo_region_id',
            thematicUniqueValues: thematicUniqueValues,
            thematicColors: thematicColors,
            whereClause: whereClause //this will be used against the feature service
            //whereClause: "iso_a3 IN ('ARG','AUS','BEL','BRA','CAN','CHE','CHN','DEU','DNK','ESP','FRA','GBR','GRC','IND','ITA','JPN','KOR','MEX','MYS','NGA','NLD','POL','ROU','SAU','SGP','USA','ZAF')"  
            //whereClause: "1=1" 
        };

        // display the feature layer
        this.mapControl.switchFeatureLayer('Countries', layerOptions, null);

    },

    /**
    * Called by the map control when the feature layer is loaded.
    */
    onFeatureLayerLoad: function() {
        View.log('mapController.onFeatureLayerLoad(): start');

        View.log('mapController.onFeatureLayerLoad(): end');
    },

    /**
    * Called by the map control when a feature is clicked.
    */
    onFeatureClick: function(assetId, assetType, assetGraphic){
        View.log('mapController.onFeatureClick(): start');

        // select the feature 
        var  objectId = assetGraphic.attributes['objectid'];
        this.mapControl.selectFeaturesByObjectIds([objectId]);

        // create the info window content
        var htmlContent = this.createHtmlContentForInfoWindow(assetGraphic);

        // show the info window
        this.mapControl.showFeatureLayerInfoWindow(htmlContent);

        View.log('mapController.onFeatureClick(): end');
    },

    /**
    * Called by the map control when the mouse is over a feature.
    */
    onFeatureMouseOver: function(assetId, assetType, assetGraphic){
        View.log('mapController.onFeatureMouseOver(): start');

        View.log('mapController.onFeatureMouseOver(): end');
    },

    /**
    * Called by the map control when the mouse moves off a feature.
    */
    onFeatureMouseOut: function(){
        View.log('mapController.onFeatureMouseOut(): start');

        View.log('mapController.onFeatureMouseOut(): end');
    },

    /**
    * Called by the map controller to display the feature legend.
    */
    showLegend: function () {
        this.mapControl.showFeatureLayerLegend();
    },

    /**
    * Called by the map controller to create the html for the info window.
    * @param assetGraphic The feature containing the data for the info window.
    @ returns {string}
    */
    createHtmlContentForInfoWindow: function(assetGraphic) {
        var attributes = assetGraphic.attributes;

        var htmlContent = "<table width='100%'>";
        htmlContent += "<tr><td colspan='2' class='featureLayerInfoWindowTitle'>" + attributes.name  + "   ("  + attributes.iso_a3 + ") </td></tr>";
        //htmlContent += "<tr><td class='featureLayerInfoWindowSubTitle'> ("  + attributes.iso_a3 + ") </td></tr>";

        //upload.wikimedia.org/wikipedia/en/thumb/b/ba/Flag_of_Germany.svg/800px-Flag_of_Germany.svg.png
        //htmlContent += "<tr><td class='featureLayerInfoWindowPhoto' id='featureLayerInfoWindowPhoto'></td></tr>";

        htmlContent += "<tr><td class='featureLayerInfoWindowText'>Continent: </td><td class='featureLayerInfoWindowText'>" + attributes.continent + "</td></tr>";
        htmlContent += "<tr><td class='featureLayerInfoWindowText'>Region: </td><td class='featureLayerInfoWindowText'>" + attributes.region_un + "</td></tr>";
        htmlContent += "<tr><td class='featureLayerInfoWindowText'>Sub_Region: </td><td class='featureLayerInfoWindowText'>" + attributes.subregion + "</td></tr>";
        htmlContent += "<tr><td class='featureLayerInfoWindowText'>OBJECTID: </td><td class='featureLayerInfoWindowText'>" + attributes.OBJECTID + "</td></tr>";
        htmlContent += "<tr><td class='featureLayerInfoWindowText'>objectid: </td><td class='featureLayerInfoWindowText'>" + attributes.objectid + "</td></tr>";

        //htmlContent += "<tr><td class='featureLayerInfoWindowText'>Economy: " + attributes.economy + "</td></tr>";
        //htmlContent += "<tr><td class='featureLayerInfoWindowText'>Income Group: " + attributes.income_grp + "</td></tr>";

        htmlContent += "<tr><td colspan='2' class='featureLayerInfoWindowAction'><a href='javascript:openWikipediaLink(" + '"' + attributes.name + '"' + ")'>Wikipedia</a></td></tr>";
        htmlContent += "</table>";

        return htmlContent;
    },

    /**
    * Called by the map controller to open the Wikipedia link.
    */
    openWikipediaLink: function(name) {
        var url = '//en.wikipedia.org/wiki/' + name;
        window.open(url);
    },

    /**
    * Called by the country grid show countries panel action. 
    */
    ctryGrid_onShowCountries: function(rows) {   

        var assetIds = this.getAssetIdsFromSelection();

        var objectIds = this.mapControl.getFeatureLayerObjectIdsByValue( 'iso_a3', 'objectid', assetIds);

        // select the features on the map
        // mapControl.selectFeatures() wont with client-side feature layers 
        // we can use mapControl.selectFeaturesByObjectIds() instead
        this.mapControl.selectFeaturesByObjectIds(objectIds);
    },

    /**
    * Called by the country grid clear countries panel action.
    */
    ctryGrid_onClearCountries: function() {
        // clear the features from the map
        this.mapControl.clearSelectedFeatures();

        // clear the selection from the grid
        this.ctryGrid.setAllRowsUnselected();

        // clear info window content
        //this.mapControl.showFeatureLayerInfoWindow("");

        // hide info window
        this.mapControl.hideFeatureLayerInfoWindow();

        //reset map view
        this.mapControl.map.centerAndZoom([0, 42], 2);
    },

    /**
    * Called by the map controller to get the country ids from the selected rows in the grid.
    * returns {array}.
    */
    getAssetIdsFromSelection: function() {

        //var assetIds = null;
        var assetIds = [];

        var assetIdsObject = this.ctryGrid.getPrimaryKeysForSelectedRows();
        for (i=0; i<assetIdsObject.length; i++) {
          var assetIdObject = assetIdsObject[i];
          // if (i==0){
          //   //assetIds = "'" + assetIdsObject[i]['ctry.ctry_id'] + "'";
          // } else {
          //   assetIds += ",'" + assetIdsObject[i]['ctry.ctry_id'] + "'";
          // }
          assetIds.push(assetIdsObject[i]['ctry.ctry_id']);
        }

        return assetIds;
    },

    /**
    * Called by the map controller to get distinct country ids from the country datasource.
    * returns {string}.
    */
    getAllAssetIdsFromDataSource: function(){

        var assetIdsString = '';
        var assetIdsArray = this.mapControl._getDistinctFieldValues('ctry.ctry_id', '1=1');

        for (i=0; i<assetIdsArray.length; i++) {
          if (i == 0) {
            assetIdsString = "'" + assetIdsArray[i] + "'";
          } else {
            assetIdsString += ",'" + assetIdsArray[i] + "'";
          }
        } 

        return assetIdsString;
    }

});

/**
* Called by the map controller to open the Wikipedia link.
*/
function openWikipediaLink(name) {
  var mapController = View.controllers.get('mapController');
  mapController.openWikipediaLink(name);
}
