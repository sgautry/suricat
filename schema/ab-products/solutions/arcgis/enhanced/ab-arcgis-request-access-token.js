View.createController('mapController', {
	
	afterViewLoad: function(){
	},
			
	afterInitialDataFetch: function() {
	},	
	
	tokenPanel_onRequestAccessToken: function() {
		var accessTokenEl = document.getElementById('accessToken');
		accessTokenEl.innerHTML = "<p>Requesting token...</p>";
		
		var result = Ab.workflow.Workflow.call('AbCommonResources-ArcgisExtensionsService-requestArcgisServerAccessToken');

		if (result.code != 'executed') {
			Ab.workflow.Workflow.handleError(result);
		} else {
			var accessToken = result.message;
			accessTokenEl.innerHTML = "<p>" + accessToken + "</p>";
		}

	}


});


