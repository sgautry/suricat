View.createController('wrMapController', {
    
    // Ab.arcgis.ArcGISMap
    mapControl: null,

    // the map configuration parameters
    configParams: null,

    // water main layer options
    waterMainsLayerOptions: null,

    // parcel layer options
    waterValvesLayerOptions: null,
    
    afterViewLoad: function(){
        //var openerView = View.getOpenerView();

        // get config params from url for map configuration.
        this.getUrlParameterValues();
        
        // configure the map
        this.configMap();
    },
    
    afterInitialDataFetch : function() {

    },

    /**
     * Get URL parameter values.
     */
    getUrlParameterValues : function() {
        var wrMapController = View.controllers.get('wrMapController'),
            configParams = new Object();

        if(!View.getOpenerView()){
            return;
        }
        
        for (var name in window.location.parameters) {
            // URL passed map type info
            if(name.indexOf('map_type') >= 0){
                configParams.map_type = window.location.parameters[name];
            }
            if(name.indexOf('wr_id') >=0){
                configParams.wr_ids = wrMapController.parseWrIdUrlParameter(window.location.parameters[name]);
            }  
        }

        wrMapController.configParams = configParams;
    },

    /**
    * Called by the map controller to configure the map control.
    */
    configMap: function() {
        // create the config object
        var configObject = new Ab.view.ConfigObject();
        configObject.mapConfigObject = this.createMapConfigObject();
        this.mapControl = new Ab.arcgis.MapExtensions('mapPanel', 'mapDiv', configObject);

        // apply esri css to map panel
        var reportTargetPanel = document.getElementById("mapPanel");
        reportTargetPanel.className = 'claro';  
    },

    /**
    * Called by the map controller to create the map configuration options.
    * returns mapConfigObject {object}.
    */
    createMapConfigObject: function() {
        var mapConfigObject = new Object();

        mapConfigObject.mapInitExtent = [-7934782, 5240479, -7933103, 5241488];
        mapConfigObject.mapInitExtentWKID = 102100;
        
        mapConfigObject.basemapLayerList = [
            {
                name: 'World Topographic',
                url: '//services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer'
            },
            {
                name: 'World Imagery', 
                url : '//server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer'
            },
            {
                name: 'World Street Map', 
                url : '//server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer'
            },
            {
                name: 'Light Gray Canvas', 
                url : '//services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer'
            }
        ];
        
        mapConfigObject.referenceLayerList = [
            {
              name: 'Land Base',
              url: '//tiles.arcgis.com/tiles/lq5SzPW1rLR0h1u9/arcgis/rest/services/hq_land_base/MapServer',
              opacity: 0.75,
              index: 90
            }
        ];
        
        mapConfigObject.featureLayerList = [
            {
              name: 'Water Mains',
              url: '//services1.arcgis.com/5mZ8AAgsNa3gSWCQ/arcgis/rest/services/hq_water_utilities_dev/FeatureServer/1',
              opacity: 0.75,
              index: 75,
              outFields: ['*'],
              zoomToResults: true,
              toolTipField: 'EQ_ID',
              assetIdField: 'EQ_ID',
              assetTypeField: 'AB_ASSET_TYPE',
              //TODO
              geometryType: 'line'
            }
            // ,
            // {
            //     name: 'Water Valves',
            //     url: '//services1.arcgis.com/5mZ8AAgsNa3gSWCQ/arcgis/rest/services/hq_water_utilities_dev/FeatureServer/0',
            //     opacity: 0.75,
            //     index: 80,
            //     outFields: ['*'],
            //     zoomToResults: true,
            //     toolTipField: 'NAME`',
            //     assetIdField: 'EQ_ID',
            //     assetTypeField: 'AB_ASSET_TYPE',
            //     // TODO
            //     geometryType: 'point'
            // }
        ];
        
        var wrMapController = this;
        mapConfigObject.onMapLoad = function() {
          wrMapController.onMapLoad();
        };  

        return mapConfigObject;
    },

    /**
    * Called by the map control when the map is loaded and ready for action.
    */
    onMapLoad: function() {
        // configure the map event listeners
        //this.configMapEventListeners();

        // configure the asset menu
        this.configureMapUI();
     
    },

    /**
    * Called by the map controller to onfigure the map event listeners.
    */
    configMapEventListeners: function() {
        this.mapControl.addEventListener('mapClick', this.onMapClick, this);
        //this.mapControl.addEventListener('mapViewChange', mapController.onMapViewChange, mapController);
        //this.mapControl.addEventListener('referenceLayerLoaded', mapController.onReferenceLayerLoad, mapController);
        //this.mapControl.addEventListener('featureLayerLoad', mapController.onFeatureLayerLoad, mapController);
        //this.mapControl.addEventListener('featureMouseOver', mapController.onFeatureMouseOver, mapController);
        //this.mapControl.addEventListener('featureMouseOut', mapController.onFeatureMouseOut, mapController);
        this.mapControl.addEventListener('featureClick', this.onFeatureClick, this);
    },

    /**
    * Called by the map controller to configure the asset menu.
    */
    configureMapUI: function() {
        
        var wrMapController = View.controllers.get('wrMapController');

        // configure basemap layer menu
        var basemapLayerMenu = this.mapPanel.actions.get('basemapLayerMenu');
        basemapLayerMenu.clear();
        var basemapLayers = this.mapControl.getBasemapLayerList();
        for (var i=0; i<basemapLayers.length; i++){
            basemapLayerMenu.addAction(i, basemapLayers[i], this.switchBasemapLayer);
        }


        // map_type options:
        //   ASSET:     loads linear assets.  passes selected asset id to callback.
        //   LOCATION:  passes location of map click to callback.
        //   NULL:      loads linear assets. displays info window for selected asset.

        var mapType = wrMapController.configParams.map_type;
        switch (mapType) {

            case 'ASSET':
                wrMapController.configureAssetMap();
                break;

            case 'LOCATION': 
                wrMapController.configureLocationMap();
                break;

            case 'ACTIVITY':
                wrMapController.configureActivityMap();
                break;

            default:
                wrMapController.configureAssetMap();
                break;

        }

    },

    configureLocationMap: function() {
        // load the reference layer
        this.mapControl.switchReferenceLayer('Land Base');

        // add map event listeners
        this.mapControl.addEventListener('mapClick', this.onMapClick, this);
    },

    configureAssetMap: function() {
        // load the reference layer
        this.mapControl.switchReferenceLayer('Land Base'); 

        // configure feature layer options
        this.configureLayerOptions();  

        // load the feature layer 
        this.mapControl.switchFeatureLayer('Water Mains',this.waterMainsLayerOptions);

        // add map event listeners
        this.mapControl.addEventListener('featureClick', this.onFeatureClick, this);

        // configure asset layer menu
        var assetLayerMenu = this.mapPanel.actions.get('assetLayerMenu');
        assetLayerMenu.show(false);
        assetLayerMenu.clear();
        assetLayerMenu.addAction(0, 'None', this.switchFeatureLayer);

        // configure the feature layers
        var featureLayerList = this.mapControl.mapConfigObject.featureLayerList;
        for ( var i=0; i<featureLayerList.length; i++){
            assetLayerMenu.addAction(i+1, featureLayerList[i].name, this.switchFeatureLayer);
        }  

    },

    configureActivityMap: function() {
        // load the reference layer
        this.mapControl.switchReferenceLayer('Land Base'); 

        // configure feature layer options
        this.configureLayerOptions();  

        // load the feature layer 
        this.mapControl.switchFeatureLayer('Water Mains',this.waterMainsLayerOptions);

        // create marker properties
        var markerProperty = new Ab.arcgis.ArcGISMarkerProperty(
            'activityDS',
            ['activity_log.lat', 'activity_log.lon'],
            ['activity_log.activity_log_id'],
            ['activity_log.eq_id', 'activity_log.wr_id', 'activity_log.status', 'activity_log.description']
        );
        markerProperty.showLabels = false;
        markerProperty.symbolSize = 15;

        //TODO
        var hexColors = colorbrewer.AbStopLight0[2];
        var rgbColors = this.mapControl.colorbrewer2rgb(hexColors);
        markerProperty.symbolColors = rgbColors;

        var thematicBuckets = ['APPROVED','IN PROGRESS'];
        markerProperty.setThematic('activity_log.status', thematicBuckets);
        this.mapControl.buildThematicLegend(markerProperty);

        var sqlRestriction;
        if (this.configParams.wr_ids){
            //restriction.addClause('activity_log.wr_id', this.configParams.wr_ids, 'IN');
            sqlRestriction = "activity_log.wr_id IN " + this.configParams.wr_ids;
        } else {
            sqlRestriction = "activity_log.site_id = 'BEDFORD'";    
        }
        
        markerProperty.setRestriction(sqlRestriction);
        this.mapControl.updateDataSourceMarkerPropertyPair('activityDS', markerProperty);
        this.mapControl.refresh();

    },

    /*
    * Called by the map controller to configure the feature layer display options.
    */
    configureLayerOptions: function() {

        // create the water mains layer options
        this.waterMainsLayerOptions = {
            renderer: 'simple',
            fillColor: [31,121,180],
            thematicDataSource: 'ARCGIS',
            //dataSource: 'blGridDs',
            //dataSourceParameters: parameters,
            //dataSourceRestriction: '1=1',
            //legendDataSuffix: '%',
            //keyField: 'bl_id',
            //thematicField: 'bl.use1',
            //thematicUniqueValues: thematicUniqueValues,
            //thematicColors: thematicColors,
            whereClause: "1=1" //this will be used against the feature service
            //whereClause: "1=1" 
        };        

        // create the water valves layer options
        this.waterValvesLayerOptions = {
            renderer: 'simple',
            fillColor: [255,127,0], //[31,121,180],
            thematicDataSource: 'ARCGIS',
            //dataSource: 'blGridDs',
            //dataSourceParameters: parameters,
            //dataSourceRestriction: '1=1',
            //legendDataSuffix: '%',
            //keyField: 'bl_id',
            //thematicField: 'bl.use1',
            //thematicUniqueValues: thematicUniqueValues,
            //thematicColors: thematicColors,
            whereClause: "1=1" //this will be used against the feature service
            //whereClause: "1=1" 
        };
    },

    /**
    * Called by the map controller to switch the active basemap layer.
    */
    switchBasemapLayer: function(item) {
        var wrMapController = View.controllers.get('wrMapController');
        wrMapController.mapControl.switchBasemapLayer(item.text);
    },  

    /**
    * Called by the map controller to switch the active feature layer.
    */
    switchFeatureLayer: function(item){
        var wrMapController = View.controllers.get('wrMapController');
        if (item.text === 'Water Mains'){
            wrMapController.mapControl.switchFeatureLayer(item.text, wrMapController.waterMainsLayerOptions);
        } else if (item.text === 'Water Valves') {
            wrMapController.mapControl.switchFeatureLayer(item.text, wrMapController.waterValvesLayerOptions);
        } else {
            wrMapController.mapControl.clearFeatures();
        }
    },

    /**
    * Called by the map control when a feature is clicked.
    */
    onFeatureClick: function(assetId, assetType, assetGraphic) {

        // get the location (center) of the geometry to use in the event the asset doesnt have location
        var extent = assetGraphic.geometry.getExtent();
        var mapPoint = esri.geometry.webMercatorToGeographic(extent.getCenter());
        var lon = mapPoint.x.toFixed(6);
        var lat = mapPoint.y.toFixed(6);

        // TODO 
        // get lat-lon from asset datasource

        // if the map is in ASSET mode, pass the asset id and location to the callback.
        // otherwise display the asset info window.      
        if (View.parameters && View.parameters.callback) {
                View.parameters.callback(assetId, lon, lat);
                // The .defer method used here is required for proper functionality with Firefox 2
                View.closeThisDialog.defer(100, View);
        } else {
            // select the feature on the map
            var objectId = assetGraphic.attributes['OBJECTID'];
            this.mapControl.selectFeaturesByObjectIds([objectId]);

            // create the info window content
            var htmlContent = this.createHtmlContentForInfoWindow(assetGraphic);

            // display the info window
            this.mapControl.showFeatureLayerInfoWindow(htmlContent);
        }
    },
     

    /**
    * Called by the map control when the map is clicked.
    */
    onMapClick: function(event) {
        // get the map point
        var mapPoint = esri.geometry.webMercatorToGeographic(event.mapPoint);
        var lon = mapPoint.x.toFixed(6);
        var lat = mapPoint.y.toFixed(6);

        // this view is designed to be opened as a dialog from the work request form
        // when opened directly, this callback will fail.
        

        if (View.parameters.callback) {
            View.parameters.callback(lon, lat);
            // The .defer method used here is required for proper functionality with Firefox 2
            View.closeThisDialog.defer(100, View);
        }
    },

    /**
    * Called by the map controller to create the html for the info window.
    * @param assetGraphic The feature containing the data for the info window.
    * returns {string}
    */
    createHtmlContentForInfoWindow: function(assetGraphic) {
        var attributes = assetGraphic.attributes;

        var htmlContent = "<table width='100%'>";
        htmlContent += "<tr><td class='featureLayerInfoWindowTitle' width='30%'>Eq ID: </td><td class='featureLayerInfoWindowTitle'>" + attributes.EQ_ID  + "</td></tr>";
        //htmlContent += "<tr><td class='featureLayerInfoWindowSubTitle'> ("  + attributes.iso_a3 + ") </td></tr>";

        //htmlContent += "<tr><td class='featureLayerInfoWindowText'>Eq Id: </td><td class='featureLayerInfoWindowText'>" + attributes.EQ_ID + "</td></tr>";
        htmlContent += "<tr><td class='featureLayerInfoWindowText'>Eq Standard: </td><td class='featureLayerInfoWindowText'>" + attributes.EQ_STD + "</td></tr>";
        htmlContent += "<tr><td class='featureLayerInfoWindowText' vertical-align='top'>Description: </td><td class='featureLayerInfoWindowText'>" + attributes.DESCRIPTION + "</td></tr>";

        htmlContent += "<tr><td colspan='2' class='featureLayerInfoWindowAction'><a href='javascript:openWorkOrderConsole(" + '"' + attributes.EQ_ID + '"' + ")'>Create Work Order</a></td></tr>";
        htmlContent += "</table>";

        return htmlContent;
    },

    /**
    * Called by the map controller when the user clicks the create work order info window action.
    */
    openWorkOrderConsole: function(assetId, lon, lat) {
        var url = 'ab-bldgops-console-wr-create-geo.axvw?site_id=BEDFORD&eq_id=' + assetId + "&lon=" + lon + "&lat=" + lat;
    },

    parseWrIdUrlParameter: function(wrIdUrlParameter) {
        var wrIds = '';
        var wrIdsParams = wrIdUrlParameter.split(',');
        
        for (var i=0; i<wrIdsParams.length; i++){
            if (i===0){
                wrIds = "'" + wrIdsParams[i] + "'";
            } else {
                wrIds += ",'" + wrIdsParams[i] + "'";
            }
        }

        wrIds = '(' + wrIds + ')';
        
        return wrIds;
    }
});

/**
* Called by the map controller to open the building ops console.
*/
function openWorkOrderConsole(assetId) {
    var wrMapController = View.controllers.get('wrMapController');
    wrMapController.openWorkOrderConsole(assetId);
}