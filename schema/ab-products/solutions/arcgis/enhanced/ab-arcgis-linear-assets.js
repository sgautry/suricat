View.createController('mapController', {
    
    // Ab.arcgis.ArcGISMap
    mapControl: null,

    // feature layer options
    featureLayerOptions: null,
   
    afterViewLoad: function(){
        this.configMap();
    },
    
    /**
    * Called by the map controller to configure the map control.
    */
    configMap: function() {
        // create the config object
        var configObject = new Ab.view.ConfigObject();
        configObject.mapConfigObject = this.createMapConfigObject();
        this.mapControl = new Ab.arcgis.MapExtensions('mapPanel', 'mapDiv', configObject);

        // apply esri css to map panel
        var reportTargetPanel = document.getElementById("mapPanel");
        reportTargetPanel.className = 'claro';  
    },

    /**
    * Called by the map controller to create the map configuration options.
    * returns mapConfigObject {object}.
    */
    createMapConfigObject: function() {
        var mapConfigObject = new Object();

        mapConfigObject.mapInitExtent = [-7935064, 5239373, -7933362, 5240332];
        mapConfigObject.mapInitExtentWKID = 102100;
        
        mapConfigObject.basemapLayerList = [
            {
                name: 'World Topographic',
                url: '//services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer'
            },
            {
                name: 'World Imagery', 
                url : '//server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer'
            },
            {
                name: 'World Street Map', 
                url : '//server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer'
            },
            {
                name: 'Light Gray Canvas', 
                url : '//services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer'
            }
        ];
        
        mapConfigObject.referenceLayerList = [
            {
              name: 'Land Base',
              url: '//tiles.arcgis.com/tiles/lq5SzPW1rLR0h1u9/arcgis/rest/services/hq_land_base/MapServer',
              opacity: 0.75,
              index: 90
            }
        ];
        
        mapConfigObject.featureLayerList = [
            {
              name: 'Water Mains',
              url: '//services3.arcgis.com/lq5SzPW1rLR0h1u9/arcgis/rest/services/hq_water_utilities/FeatureServer/1',
              opacity: 0.75,
              index: 75,
              outFields: ['*'],
              zoomToResults: true,
              toolTipField: 'EQ_ID',
              assetIdField: 'EQ_ID',
              assetTypeField: 'AB_ASSET_TYPE',
              //TODO
              geometryType: 'line'
            }
        ];
        
        var mapController = this;
        mapConfigObject.onMapLoad = function() {
          mapController.onMapLoad();
        };  

        return mapConfigObject;
    },

    /**
    * Called by the map control when the map is loaded and ready for action.
    */
    onMapLoad: function() {
        // configure the map event listeners
        this.configMapEventListeners();

        // load the reference layer
        this.mapControl.switchReferenceLayer('Land Base');

        // configure the asset menu
        this.configureMapUI();       
    },

    /**
    * Called by the map controller to onfigure the map event listeners.
    */
    configMapEventListeners: function() {
        this.mapControl.addEventListener('mapClick', this.onMapClick, this);
        //this.mapControl.addEventListener('mapViewChange', this.onMapViewChange, this);
        //this.mapControl.addEventListener('referenceLayerLoaded', this.onReferenceLayerLoad, this);
        //this.mapControl.addEventListener('featureLayerLoad', this.onFeatureLayerLoad, this);
        //this.mapControl.addEventListener('featureMouseOver', this.onFeatureMouseOver, this);
        //this.mapControl.addEventListener('featureMouseOut', this.onFeatureMouseOut, this);
        this.mapControl.addEventListener('featureClick', this.onFeatureClick, this);
    },

    /**
    * Called by the map controller to configure the asset menu.
    */
    configureMapUI: function() {
        
        // basemap layer menu
        var basemapLayerMenu = this.mapPanel.actions.get('basemapLayerMenu');
        basemapLayerMenu.clear();
        var basemapLayers = this.mapControl.getBasemapLayerList();
        for (var i=0; i<basemapLayers.length; i++){
            basemapLayerMenu.addAction(i, basemapLayers[i], this.switchBasemapLayer);
        }

        // asset layer menu
        var assetLayerMenu = this.mapPanel.actions.get('assetLayerMenu');
        assetLayerMenu.clear();
        assetLayerMenu.addAction(0, 'None', this.switchFeatureLayer);
        assetLayerMenu.addAction(1, 'Water Mains', this.switchFeatureLayer);
        assetLayerMenu.addAction(2, 'Water Mains by System Type', this.switchFeatureLayer);
        assetLayerMenu.addAction(3, 'Water Mains by Pipe Size', this.switchFeatureLayer);

        // legend menu
        var legendObj = Ext.get('showLegend'); 
        legendObj.on('click', this.showLegend, this, null);        
    },

    /*
    * Called by the map controller to configure the feature layer display options.
    */
    configureLayerOptions: function(layerName) {
        var mapController = View.controllers.get('mapController'); 

        switch (layerName) {
            case 'Water Mains':
                 mapController.configureSimpleRenderer();
                break;
            case 'Water Mains by System Type':
                mapController.configureUniqueValueRenderer();
                break;
            case 'Water Mains by Pipe Size':
                mapController.configureClassBreakRenderer();
                break;
            default:
                break;
        }

    },

    // create the simple renderer layer options
    configureSimpleRenderer: function() {
        var mapController = View.controllers.get('mapController');

        var simpleRenderer = {
            renderer: 'simple',
            fillColor: [31,121,180],
            thematicDataSource: 'ARCGIS',
            whereClause: "1=1" 
        }; 

        mapController.featureLayerOptions = simpleRenderer;
    },

    // create the thematic unique value renderer layer options
    configureUniqueValueRenderer: function() {
        // TODO
        // var thematicUniqueValues = this.getDistinctValuesFromDataSource('eq.csi_id');
        var thematicUniqueValues = ["15140", "15300"];
        var thematicColors = this.mapControl.colorbrewerToRGB(colorbrewer.Set1[3]);
        thematicColors = thematicColors.slice(0, -1);
        thematicColors.reverse();

        var dataSourceRestriction = new Ab.view.Restriction();
        dataSourceRestriction.addClause('eq.csi_id',['15140', '15300'],'IN');

        var thematicUniqueValueRenderer = {
            renderer: 'thematic-unique-values',
            fillColor: [31,121,180],
            thematicDataSource: 'WC_DATASOURCE',
            dataSource: 'eqDs',
            //dataSourceParameters: parameters,
            dataSourceRestriction: dataSourceRestriction,
            legendTitle: 'System Type',
            legendDataLabels: ['Domestic Water', 'Fire Protection'],
            //legendDataSuffix: '%',
            keyField: 'eq.eq_id',
            thematicField: 'eq.csi_id',
            thematicUniqueValues: thematicUniqueValues,
            thematicColors: thematicColors,
            whereClause: "1=1" //this will be used against the feature service
         };  

        this.featureLayerOptions = thematicUniqueValueRenderer;
    },

    // create the thematic class break renderer layer options
    configureClassBreakRenderer: function() {
        var thematicClassBreaks = [8,10,12];
        var thematicColors = this.mapControl.colorbrewerToRGB(colorbrewer.YlOrRd[8]);
        thematicColors.shift();
        thematicColors.shift();
        thematicColors.shift();
        thematicColors.shift();

        var dataSourceRestriction = new Ab.view.Restriction();
        dataSourceRestriction.addClause('eq.csi_id',['15140', '15300'],'IN');

        var thematicClassBreakRenderer = {
            renderer: 'thematic-class-breaks',
            fillColor: [31,121,180],
            thematicDataSource: 'WC_DATASOURCE',
            dataSource: 'eqDs',
            //dataSourceParameters: parameters,
            dataSourceRestriction: dataSourceRestriction,
            legendTitle: 'Pipe Size',
            legendDataSuffix: '"',
            keyField: 'eq.eq_id',
            thematicField: 'eq.option2',
            thematicClassBreaks: thematicClassBreaks,
            thematicColors: thematicColors,
            whereClause: "1=1" //this will be used against the feature service
            //whereClause: "1=1" 
        };  

        this.featureLayerOptions = thematicClassBreakRenderer;
    },

    /**
    * Called by the mapController to display the feature layer legend.
    */
    showLegend: function(){
        var mapController = View.controllers.get('mapController');
        mapController.mapControl.showFeatureLayerLegend();
    },

    /**
    * Called by the mapController to display the equipment details form.
    */
    showEqForm: function(eqId) {

        var mapController = View.controllers.get('mapController');

        var offsetX = 25; 
        var offsetY = $(document).body.offsetHeight - 375;

        var restriction = new Ab.view.Restriction();
        restriction.addClause('eq.eq_id', eqId, '=', 'AND');

        mapController.eqForm.refresh(restriction);
        mapController.eqForm.showInWindow({ 
            width: 400, height: 325, 
            x: offsetX, y: offsetY,
            closeButton: false 
        });        

    },

    /**
    * Called from the eq form when the user clicks the save action.
    */
    eqForm_onSaveForm: function(){
        // get the archibus key
        var eq_id = this.eqForm.getFieldValue('eq.eq_id');
        // get the arcgis key
        var geo_objectid = this.eqForm.getFieldValue('eq.geo_objectid');

        // save the form
        this.eqForm.save();

        // OPTIONAL
        // call the wfr to update the ArcGIS feature data
        // note: the ArcGIS eq connector must be configured accordingly
        //var result = Workflow.callMethod('AbCommonResources-ArcgisExtensionsService-updateArcgisFeatureDataByObjectId', geo_objectid);

        // close the eq form
        View.panels.get('eqForm').closeWindow();

        // OPTIONAL
        // refresh the map
        this.mapControl.switchFeatureLayer('Water Mains', this.featureLayerOptions);
    },

    /**
    * Called from the eq form when the user clicks the cancel action.
    */
    eqForm_onCancelForm: function() {
        // close the eq form
        View.panels.get('eqForm').closeWindow();
    },

    /**
    * Called by the map controller to switch the active basemap layer.
    */
    switchBasemapLayer: function(item) {
        var mapController = View.controllers.get('mapController');
        mapController.mapControl.switchBasemapLayer(item.text);
    },  

    /**
    * Called by the map controller to switch the active feature layer.
    */
    switchFeatureLayer: function(item){
        var mapController = View.controllers.get('mapController');
      
        // close the eq detail form
        View.panels.get('eqForm').closeWindow();

        if (item.text === 'None') {
            mapController.mapControl.clearFeatures();
        } else {
            mapController.configureLayerOptions(item.text);
            mapController.mapControl.switchFeatureLayer('Water Mains', mapController.featureLayerOptions);
            mapController.mapControl.setMapCenterAndZoom(-71.27385784, 42.52462794, 17);
        }
    },

    /**
    * Called by the map control when the map is clicked.
    */
    onMapClick: function(event){
        var mapPoint = esri.geometry.webMercatorToGeographic(event.mapPoint);
        var lon = (mapPoint.x).toFixed(8);
        var lat = (mapPoint.y).toFixed(8);
        View.log("onMapClick()->location= " + lon + " , " + lat);

        // close the eq detail form
        View.panels.get('eqForm').closeWindow();        
    },

    /**
    * Called by the map control when the map view changes
    */
    onMapViewChange: function(event){
        
    },

    /**
    * Called by the map control when a feature is clicked.
    */
    onFeatureClick: function(assetId, assetType, assetGraphic){

        var restriction,
            ds, 
            records,
            objectId,
            htmlContent; 

        var mapController = View.controllers.get('mapController');

        // close the eq form
        View.panels.get('eqForm').closeWindow();

        switch (assetType)
        {
            case "eq_line":
                // select the asset
                if (mapController.featureLayerOptions.renderer === 'simple') {
                    objectId = assetGraphic.attributes['OBJECTID'];
                } else {
                    objectId = assetGraphic.attributes['objectid'];
                }
                this.mapControl.selectFeaturesByObjectIds([objectId]);

                // get the data from the datasource
                var geoObjectId = assetGraphic.attributes['OBJECTID'];
                var restriction = new Ab.view.Restriction();
                restriction.addClause('eq.geo_objectid', geoObjectId, "=", "AND");
                var eqDs = View.dataSources.get('eqDs');
                var dataRecords = eqDs.getRecords(restriction);

                // create the info window content
                if (dataRecords.length > 0) {
                    htmlContent = this.createHtmlContentForInfoWindowFromDataRecord(dataRecords[0]);
                } else {
                    htmlContent = this.createHtmlContentForInforWindowForNullDataRecord(assetGraphic.attributes['EQ_ID']);
                }

                // display the info window
                this.mapControl.showFeatureLayerInfoWindow(htmlContent);
                break;

            default:
                View.alert('Unknown asset type : ' + assetType );
                break;
        }
    },

    /**
    * Called by the map controller to create the html for the info window.
    * @param assetGraphic The feature containing the data for the info window.
    * returns {string}
    */
    createHtmlContentForInfoWindowFromAssetGraphic: function(assetGraphic) {
        var attributes = assetGraphic.attributes;

        var htmlContent = "<table width='100%'>";
        htmlContent += "<tr><td class='featureLayerInfoWindowTitle' width='30%'>Eq ID: </td><td class='featureLayerInfoWindowTitle'>" + attributes.EQ_ID  + "</td></tr>";
        htmlContent += "<tr><td class='featureLayerInfoWindowText' vertical-align='top'>Description: </td><td class='featureLayerInfoWindowText'>" + attributes.DESCRIPTION + "</td></tr>";
        htmlContent += "<tr><td colspan='2' class='featureLayerInfoWindowAction'><a href='javascript:showEqForm(" + '"' + attributes.EQ_ID + '"' + ")'>Show Equipment Details</a></td></tr>";
        htmlContent += "</table>";

        return htmlContent;
    },

    /**
    * Called by the map controller to create the html for the info window.
    * @param dataRecord The data record containing the data for the info window.
    * returns {string}
    */
    createHtmlContentForInfoWindowFromDataRecord: function(dataRecord) {
        var values = dataRecord.values;

        var htmlContent = "<table width='100%'>";
        htmlContent += "<tr><td class='featureLayerInfoWindowTitle' width='30%'>Eq ID: </td><td class='featureLayerInfoWindowTitle'>" + values['eq.eq_id']  + "</td></tr>";
        htmlContent += "<tr><td class='featureLayerInfoWindowText' vertical-align='top'>Description: </td><td class='featureLayerInfoWindowText'>" + values['eq.description'] + "</td></tr>";
        htmlContent += "<tr><td colspan='2' class='featureLayerInfoWindowAction'><a href='javascript:showEqForm(" + '"' + values['eq.eq_id'] + '"' + ")'>Show Equipment Details</a></td></tr>";
        htmlContent += "</table>";

        return htmlContent;
    },

    createHtmlContentForInforWindowForNullDataRecord: function(assetId) {
        var htmlContent = "<table width='100%'>";
        htmlContent += "<tr><td class='featureLayerInfoWindowTitle'>No Data</td></tr>";
        htmlContent += "<tr><td class='featureLayerInfoWindowText'>No asset data found for eq_id=" + assetId + "</td></tr>";
        htmlContent += "</table>";

        return htmlContent;        
    },

    /**
    * Called by the map controller when the user clicks the create work order info window action.
    */
    openWorkOrderConsole: function(assetId) {
        //TODO
        var url = 'ab-bldgops-console-wr-create-geo.axvw?site_id=BEDFORD&eq_id=' + assetId;
        window.open(url);
    }

});

/**
* Called by the map controller to open the building ops console.
*/
function openWorkOrderConsole(assetId) {
    var mapController = View.controllers.get('mapController');
    mapController.openWorkOrderConsole(assetId);
}

/**
* Called by the map controller to open the equipment detaiul form.
*/
function showEqForm(assetId){
    var mapController = View.controllers.get('mapController');
    mapController.showEqForm(assetId);
}