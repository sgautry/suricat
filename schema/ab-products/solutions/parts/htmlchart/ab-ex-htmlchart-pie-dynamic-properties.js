
//custom colors:	b. purple   b. indigo	b. dk. blue	b. blue	b. green	b. lime	b. yellow	b. mustard 	b. red	b. purple       
var customFillColors = ['#7030A0','#002060','#0070C0','#00B0F0','#00B050','#92D050','#FFFF00','#FFC000','#FF0000','#C00000'];
//var customDesturatedFillColors = ['#9A91A1','#575A61','#ACB7BF','#D8E9F0','#9EB0A6','#C7D1BC','#FFFFE5','#FFF9E5','#FFE5E5','#BFACAC'];

//toggle between custom solid color and default solid colors
var customSolidColor = false;
function toggleSolidFillColors()
{
	if(customSolidColor){
		alert("set default solid color.");
		View.panels.get('chartPie_chart').setSolidFillColors([]);
	} else {
		alert("set custom custom color.");
		View.panels.get('chartPie_chart').setSolidFillColors(customFillColors);
	}
	customSolidColor = !customSolidColor;
}
