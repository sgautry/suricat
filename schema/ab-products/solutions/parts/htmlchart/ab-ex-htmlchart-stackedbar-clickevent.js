function getClickedItemData(obj) {
	
	var str = "";

	var objRmDp = obj.selectedChartData['rm.chart_dv_and_dp'];
	str += ('\nValue of Department and division (group axis): '+ objRmDp);
	
	var objRmStd = obj.selectedChartData['selectedSecondaryGrouping'];
	str += ('\nValue of room standards [' +  objRmStd.key + '] (secondary grouping axis): ' + objRmStd.value + '(' + Math.round(obj.selectedChartData['values']['percents']*100)/100 + '%)');
	
	str += ("\nValue of Total area (data axis) retrieved from event: " + obj.selectedChartData['values']['total']);
	
	alert(str);
}

