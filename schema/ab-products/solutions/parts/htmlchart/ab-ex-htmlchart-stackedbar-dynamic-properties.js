
//custom colors:	b. purple   b. indigo	b. dk. blue	b. blue	b. green	b. lime	b. yellow	b. mustard 	b. red	b. purple       

var customFillColors = ['0x7030A0','0x002060','0x0070C0','0x00B0F0','0x00B050','0x92D050','0xFFFF00','0xFFC000','0xFF0000','0xC00000'];

//toggle between custom solid color and default solid colors
var customSolidColor = false;
function toggleSolidFillColors()
{
	if(customSolidColor){
		alert("set default solid color.");
		View.panels.get('chartStackedArea_panel').setSolidFillColors([]);
	} else {
		alert("set custom Solid color.");
		View.panels.get('chartStackedArea_panel').setSolidFillColors(customFillColors);
	}
	customSolidColor = !customSolidColor;
}

var customTitle = false;
function toggleDataAxisTitle(){

	if(customTitle){
		alert("set default data axis title.");
		View.panels.get('chartStackedArea_panel').setDataAxisTitle("Book Value");
	} else {
		alert("set data axis title.");
		View.panels.get('chartStackedArea_panel').setDataAxisTitle("Book Value vs Purchase Cost");
	}
	customTitle = !customTitle;
	
}
