View.createController('afterRefreshExample', {

	chartConsole_chart_afterRefresh: function() {
		alert("invoke afterRefresh handler!");
	}
	
});


function updateChart(){
	
	var restriction = new Ab.view.Restriction();
	restriction.addClause('bl.bl_id', 'HQ' , 'LIKE');
		
	// apply restriction 
	View.panels.get("chartConsole_chart").refresh(restriction);
}


