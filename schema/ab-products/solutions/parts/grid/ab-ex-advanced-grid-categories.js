/* 
 * Controller for the test view for the Bali6 advanced grid control.
 * It shows how to initialize the grid control.
 */
View.createController('advancedGridCategories', {
	
    afterViewLoad: function() {
        var columnDefinitions = [         	
            {
                id: 'status',
                fieldName: 'wr.status',
                title: 'Work Request Status',
                type: 'string',
                groupBy: true,
                width: 100
            }, {
                id: 'wr_id',
                fieldName: 'wr.wr_id',
                title: 'Work Request Code',
                type: 'string'
            }, {
                id: 'requestor',
                fieldName: 'wr.requestor',
                title: 'Requested by',
                type: 'string'
            }, {
                id: 'prob_type',
                fieldName: 'wr.prob_type',
                title: 'Problem Type',
                type: 'string'
            }, {
                id: 'description',
                fieldName: 'wr.description',
                title: 'Work Description',
                type: 'string',
                width: 200
            }, {
                id: 'date_requested',
                fieldName: 'wr.date_requested',
                title: 'Date Work Requested',
                type: 'date'
            }, {
                id: 'date_completed',
                fieldName: 'wr.date_completed',
                title: 'Date Work Completed',
                type: 'date'
            }, {
                id: 'bl_id',
                fieldName: 'wr.bl_id',
                title: 'Building Code',
                groupColumnTitle: 'Identifying codes',
                groupColumnID: 'id',
                type: 'string',
            }, {
                id: 'fl_id',
                fieldName: 'wr.fl_id',
                title: 'Floor Code',
                groupColumnID: 'id',
                type: 'string',
            }, {
                id: 'rm_id',
                fieldName: 'wr.rm_id',
                title: 'Room Code',
                groupColumnID: 'id',
                type: 'string',
            }, {
                id: 'dv_id',
                fieldName: 'wr.dv_id',
                title: 'Division Code',
                groupColumnID: 'id',
                type: 'string'
            }, {
                id: 'dp_id',
                fieldName: 'wr.dp_id',
                title: 'Department Code',
                groupColumnID: 'id',
                type: 'string'
            }, {
                id: 'cost_total',
                fieldName: 'wr.cost_total',
                title: 'Total Cost',
                groupColumnTitle: 'Costs',
                groupColumnID: 'costs',
                groupBackgroundColor: '#90EE90',
                showTotals: true,
                totalsFunction: 'sum',
                type: 'number',
            }, {
                id: 'cost_labor',
                fieldName: 'wr.cost_labor',
                title: 'Cost of Labor',
                groupColumnID: 'costs',
                type: 'number',
            }, {
                id: 'cost_parts',
                fieldName: 'wr.cost_parts',
                title: 'Cost of Parts',
                groupColumnID: 'costs',
                type: 'number',
            }, {
                id: 'cost_tools',
                fieldName: 'wr.cost_tools',
                title: 'Cost of Tools',
                groupColumnID: 'costs',
                type: 'number',
            }, {
                id: 'cost_other',
                fieldName: 'wr.cost_other',
                title: 'Other Costs',
                groupColumnID: 'costs',
                type: 'number',
            },
            {
         		id: 'approve',
                title: 'Approve', 
                icon: 'product',
                buttonText: 'Approve',
                listener: function(row, cell) {
                	View.showMessage('Click for approve column');
                }
         	}, {
                id: 'reject',
                title: 'Reject',
                icon: 'info',
                listener: function (row, cell) {
                    View.showMessage('Click for reject column');
                }
            }
        ];

        this._grid = View.createControl({
        	control: 'DataGrid',
        	panel: 'gridPanel',
        	container: 'gridContainer',
        	dataSource: 'dataDS',
            pageSize: 100,
            multipleSelectionEnabled: true,
            columns: columnDefinitions,
            events: {
                onClickItem: function(row) { 
                	//View.showMessage('Click on work request ' + row.getFieldValue('wr.wr_id'));
                },
                onMultipleSelectionChange: function(row, selected) {
                	if (selected) {
                	    //View.showMessage('Selected work request ' + row.getFieldValue('wr.wr_id'));
                	} else {
                	    //View.showMessage('Unselected work request ' + row.getFieldValue('wr.wr_id'));
                	}
                },
                onCellPrepared: function(options, row) {
                	
                },
                afterRefresh: function () {
                	
                }
            }
        });
        
    },
    
    /**
     * Displays grid records grouped by specified category field. 
     */
    filterPanel_onShow: function() {
    	var groupBy = this.filterPanel.getFieldValue('groupBy');
    	this._grid.groupBy(groupBy);
    },
    
    /**
     * Approves work requests selected in the grid.
     */
    filterPanel_onApprove: function() {
    	var records = this._grid.getAllSelectedRecords();
    	
    	var message = "Work requests to approve: ";
    	_.each(records, function(record) {
    		message = message + record.getValue('wr.wr_id') + ' ';
    	});
    	
    	View.showMessage(message);
    },
    
    /**
     * Rejects work requests selected in the grid.
     */
    filterPanel_onReject: function() {    	
    	var records = this._grid.getAllSelectedRecords();
    	
    	var message = "Work requests to reject: ";
    	_.each(records, function(record) {
    		message = message + record.getValue('wr.wr_id') + ' ';
    	});
    	
    	View.showMessage(message);
    }
        
});
