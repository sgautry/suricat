View.createController('advancedGridScorecard', {
	
	/**
	 * The grid displays records for the current fiscal year.
	 */
	currentFiscalYear: 2016,
	
	/**
	 * The array of finanal_sum records for the previous fiscal year.
	 */
	previousFiscalYearRecords: null,
	
    afterViewLoad: function() {
        this.analysisFields = this.analysisFieldsDataSource.getRecords();
    	this.metricDefinitions = this.metricDefinitionsDataSource.getRecords();
    	
        var columnDefs = [
            {
                id: 'bl_id',
                fieldName: 'finanal_sum.bl_id',
                title: 'Building',
                groupColumnTitle: 'Identifying codes',
                groupColumnID: 'id',
                type: 'string',
            }, {
                id: 'pr_id',
                fieldName: 'finanal_sum.pr_id',
                title: 'Property',
                groupColumnID: 'id',
                type: 'string',
            }, {
                id: 'project_id',
                fieldName: 'finanal_sum.project_id',
                title: 'Project',
                groupColumnID: 'id',
                type: 'string',
            }, {
                id: 'eq_id',
                fieldName: 'finanal_sum.eq_id',
                title: 'Equipment',
                groupColumnID: 'id',
                type: 'string'
            }
        ];
        
        var controller = this;
        _.each(this.getAnalysisFields(), function(analysisField) {
            var analysisTableName = analysisField.getValue('finanal_analyses_flds.analysis_table');
            var analysisFieldName = analysisField.getValue('finanal_analyses_flds.analysis_field');
            var id = analysisTableName + '.' + analysisFieldName;
        	
            var analysisFieldDef = controller.analysisValuesDataSource.fieldDefs.get(id);
            if (analysisFieldDef) {
	            var columnDef = {
	            	id: analysisFieldName,
	            	fieldName: id,
	            	title: analysisFieldDef.title,
	            	groupColumnID: 'metrics',
	                groupColumnTitle: 'Metrics'
	            };
	            
	            if (analysisFieldDef.isNumeric) {
	            	columnDef.showTotals = true;
	            	columnDef.type = 'number';
	            }
	            
	            var metricDefinition = controller.getMetricDefinition(analysisFieldName);
	            if (metricDefinition) {
	                var aggregateAs = metricDefinition.getValue('afm_metric_definitions.report_aggregate_as');
	                if (aggregateAs === 'SUM' || aggregateAs === 'AVG') {
	                    columnDef.showTotals = true;
	                    columnDef.totalsFunction = aggregateAs.toLowerCase();
	                }
	            }
	            
	            columnDefs.push(columnDef);
            }
        });
        
        // add a restriction for the current fiscal year to the data source used by the grid
        var fiscalYearRestriction = new Ab.view.Restriction();
        fiscalYearRestriction.addClause('finanal_sum.fiscal_year', this.currentFiscalYear);
        this.analysisValuesDataSource.setRestriction(fiscalYearRestriction);
        
        this.calculateTrends();
        
        // create the grid control
        this._grid = View.createControl({
        	control: 'DataGrid',
        	panel: 'gridPanel',
        	container: 'gridContainer',
        	dataSource: 'analysisValuesDataSource',
            pageSize: 100,
            columns: columnDefs,
            events: {
            	onCellPrepared: function(options, row) {
            		if (options.rowType === 'data' && row) {
	            		var trendStyle = controller.getTrendStyle(row.getRecord(), options.column.dataField);
	                    if (trendStyle.color) {
	                    	options.cellElement.css('background-color', trendStyle.color);
	                    }
	                    if (trendStyle.icon) {
	                    	options.cellElement.addClass(trendStyle.icon);
	                    }
            		}
            	}
            }
        });
    },
    
    /**
     * Returns background color and icon class for specified data value.
     * 
     *  @param record The current record containing the value.
     *  @param analysisFieldName The name of the analysis field for this value.
     */
    getTrendStyle: function(record, analysisFieldName) { 
    	var metric = this.metrics[analysisFieldName];
        var trendDirection = this.trendDirections[analysisFieldName];
        var trendDirectionColorFunction = this.trendDirectionColorFunctions[analysisFieldName];

        var id = 'finanal_sum.' + analysisFieldName;
        var value = Number(record.getValue(id));

        var color = null;
        if (trendDirectionColorFunction) {
            color = trendDirectionColorFunction(value);
            if (color === 'red') {
                color = '#ffeeee';
            } else if (color === 'yellow') {
                color = '#ffffee';
            } else if (color === 'green') {
                color = '#eeffee';
            }
        }

        var icon = null;
        var previousFiscalYearRecord = this.findPreviousFiscalYearRecord(record);
        if (previousFiscalYearRecord) {
            var previousValue = Number(previousFiscalYearRecord.getValue(id));
            var threshold = Math.abs(previousValue * 0.03);

            var changeGoodness = '';
            if (trendDirection === '2') {
                // on target is better
                var reportLimitHighWarning = metric.getValue('afm_metric_definitions.report_limit_high_warn');
                var reportLimitLowWarning = metric.getValue('afm_metric_definitions.report_limit_low_warn');
                if (value > reportLimitLowWarning && value < reportLimitHighWarning &&
                    previousValue > reportLimitLowWarning && previousValue < reportLimitHighWarning) {
                    changeGoodness = '';
                } else if (value >= reportLimitLowWarning && value < previousValue) {
                    changeGoodness = 'Better';
                } else if (value <= reportLimitHighWarning && value > previousValue) {
                    changeGoodness = 'Better';
                } else {
                    changeGoodness = 'Worse';
                }
            }

            if (value > previousValue + threshold) {
                if (trendDirection === '0') {
                    icon = 'biggerWorse';
                } else if (trendDirection === '1') {
                    icon = 'biggerBetter';
                } else if (trendDirection === '2') {
                    icon = 'bigger' + changeGoodness;
                } else {
                    icon = 'bigger';
                }
            } else if (value < previousValue - threshold) {
                if (trendDirection === '0') {
                    icon = 'smallerBetter';
                } else if (trendDirection === '1') {
                    icon = 'smallerWorse';
                } else if (trendDirection === '2') {
                    icon = 'smaller' + changeGoodness;
                } else {
                    icon = 'smaller';
                }
            }
        }

    	return {
    		'color': color,
    		'icon': icon
    	};
    },
    
    /**
     * Calculates trends for all currently displayed data values.
     */
    calculateTrends: function() {
        // add a restriction for the current fiscal year to the data source used by the grid
        var fiscalYearRestriction = new Ab.view.Restriction();
        fiscalYearRestriction.addClause('finanal_sum.fiscal_year', this.currentFiscalYear);
        
        // get previous fiscal year records 
        this.previousFiscalYearRecords = this.analysisValuesDataSource.getRecords(fiscalYearRestriction);
        
        // generate trend directions for each analysis field
        this.metrics = {};
        this.trendDirections = {};
        this.trendDirectionColorFunctions = {};

        var controller = this;
        _.each(this.analysisFields, function(analysisField) {
            var analysisFieldName = analysisField.getValue('finanal_analyses_flds.analysis_field');

            var metric = controller.getMetricDefinition(analysisFieldName);
            if (metric) {
                var trendDirection = metric.getValue('afm_metric_definitions.report_trend_dir');
                controller.trendDirections[analysisFieldName] = trendDirection;
                controller.metrics[analysisFieldName] = metric;
            }

            var trendDirectionColorFunction = controller.getMetricTrendDirection(analysisFieldName);
            if (trendDirectionColorFunction) {
                controller.trendDirectionColorFunctions[analysisFieldName] = trendDirectionColorFunction;
            }
        });
    },
    
    /**
     * Returns a record that has the same building ID as the supplied record, but for the previous fiscal year.
     * 
     * @param record The current fiscal year record.
     */
    findPreviousFiscalYearRecord: function(record) {
        var bl_id = record.getValue('finanal_sum.bl_id');
        var result = _.find(this.previousFiscalYearRecords, function(previousRecord) {
            return (bl_id === previousRecord.getValue('finanal_sum.bl_id'));
        });

        return result;
    },

    
    /**
     * Returns analysis fields to display in the Asset Scorecard.
     */
    getAnalysisFields: function() {
        var analysisFields = _.filter(this.analysisFields, function(analysisField) {
            return 'Unified Capital and Expense Analysis' === analysisField.getValue('finanal_analyses_flds.analysis_title')
                && 'Asset Scorecard' === analysisField.getValue('finanal_analyses_flds.analysis_console_panel');
        });
        
        analysisFields = _.sortBy(analysisFields, function(analysisField) {
            return analysisField.getValue('finanal_analyses_flds.display_order');
        });
        
        return analysisFields;
    },

    /**
     * Returns a metric definition for specified analysis field.
     * @param analysisField The analysis field name (afm_metric_definitions.analysis_result_field).
     */
    getMetricDefinition: function(analysisField) {
    	var fieldName = analysisField;
        return _.find(this.metricDefinitions, function(metricDefinition, tableName, fieldName) {
            return analysisField === metricDefinition.getValue('afm_metric_definitions.analysis_result_field');
        });
    },

    /**
     * Returns an object that represents the trend direction for specified analysis field.
     * @param analysisField The analysis field name (afm_metric_definitions.analysis_result_field).
     */
    getMetricTrendDirection: function(analysisField) {
        var smallerIsBetter = function(reportLimitHighCritical, reportLimitHighWarning, value) {
            var color = 'green';

            if (reportLimitHighCritical && value >= reportLimitHighCritical) {
                color = 'red';
            } else if (reportLimitHighWarning && value >= reportLimitHighWarning) {
                color = 'yellow';
            }

            return color;
        }

        var largerIsBetter = function(reportLimitLowCritical, reportLimitLowWarning, value) {
            var color = 'green';

            if (reportLimitLowCritical && value <= reportLimitLowCritical) {
                color = 'red';
            } else if (reportLimitLowWarning && value <= reportLimitLowWarning) {
                color = 'yellow';
            }

            return color;
        }

        var onTargetIsBetter = function(reportLimitHighCritical, reportLimitHighWarning, reportLimitLowCritical, reportLimitLowWarning, value) {
            var color = 'green';

            var smallerIsBetterResult = smallerIsBetter(reportLimitHighCritical, reportLimitHighWarning, value);
            var largerIsBetterResult = largerIsBetter(reportLimitLowCritical, reportLimitLowWarning, value);

            if (color !== smallerIsBetterResult) {
                color = smallerIsBetterResult;
            } else if (color !== largerIsBetterResult) {
                color = largerIsBetterResult;
            }

            return color;
        }

        var trendDirection = null;

        var metric = this.getMetricDefinition(analysisField);
        if (metric) {
            var reportTrendDir = metric.getValue('afm_metric_definitions.report_trend_dir');
            var reportLimitHighCritical = metric.getValue('afm_metric_definitions.report_limit_high_crit');
            var reportLimitHighWarning = metric.getValue('afm_metric_definitions.report_limit_high_warn');
            var reportLimitLowCritical = metric.getValue('afm_metric_definitions.report_limit_low_crit');
            var reportLimitLowWarning = metric.getValue('afm_metric_definitions.report_limit_low_warn');

            if (reportTrendDir === '0') {
                trendDirection = function(value) {
                    return smallerIsBetter(reportLimitHighCritical, reportLimitHighWarning, value);
                }
            } else if (reportTrendDir === '1') {
                trendDirection = function(value) {
                    return largerIsBetter(reportLimitLowCritical, reportLimitLowWarning, value);
                }
            } else if (reportTrendDir === '2') {
                trendDirection = function(value) {
                    return onTargetIsBetter(reportLimitHighCritical, reportLimitHighWarning, reportLimitLowCritical, reportLimitLowWarning, value);
                }
            }
        }

        return trendDirection;
    }
    
});
