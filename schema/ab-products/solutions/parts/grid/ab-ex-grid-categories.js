
View.createController('exgridCategories', {
       
    afterInitialDataFetch : function() {
    	this.dataGrid.setCategoryColors({'Com': '#33CC33', 'HA': '#FF3300', 'I': '#336633', 'R': '#003399', 'AA': '#CC66FF', 'A': '#6600CC', 'HL': '#FF3300'});
    	this.dataGrid.setCategoryConfiguration({
    		fieldName: 'wr.status',
    		order: ['R', 'A', 'AA', 'HA', 'HL', 'I', 'Com'],
    		getStyleForCategory: this.getStyleForCategory  	
    	});
    	this.dataGrid.update();
    },
    
    getStyleForCategory: function(record) {
    	var style = {};
    	var status = record.getValue('wr.status');
    	var targetPanel = View.panels.get("dataGrid");
    	style.color = targetPanel.getCategoryColors()[status]; 
    	return style;
    },
    
    dataGrid_onRefresh: function() {
    	this.dataGrid.refresh();
    }
});
