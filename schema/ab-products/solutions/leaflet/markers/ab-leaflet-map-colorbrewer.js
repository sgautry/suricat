View.createController('mapController', {

	// The Ab.leaflet.Map control.
	mapControl: null,

	afterViewLoad: function(){
		this.configMap();
		this.configMenu();
	},

	/**
	* Configure the map control.
	*/
	configMap: function() {
    	var configObject = new Ab.view.ConfigObject();
    	configObject.mapImplementation = 'Esri';
    	configObject.basemap = View.getLocalizedString('World Light Gray Canvas');
    	var mapController = this;
    	configObject.onMapLoad = function() {
    		mapController.onMapLoad.defer(500);
    	};
		this.mapControl = new Ab.leaflet.Map('mapPanel', 'mapDiv', configObject);
	},

	/**
	* Configure the map panel menus.
	*/
	configMenu: function() {
		var mapController = View.controllers.get('mapController');
		
		var markerMenu = this.mapPanel.actions.get('markerMenu');
		markerMenu.clear();
		markerMenu.addAction(0, 'AbStopLight0', mapController.showMarkers);
		markerMenu.addAction(1, 'AbStopLight1', mapController.showMarkers);
		markerMenu.addAction(2, 'AbStopLight2', mapController.showMarkers);

    	var legendObj = Ext.get('showLegend'); 
	    legendObj.on('click', function() {
	    	mapController.showLegend();
	    });  
	},

	/**
	* Called by the map control when the map is loaded and ready for action.
	*/
	onMapLoad: function() {
		View.controllers.get('mapController').showMarkers({text: 'AbStopLight0'});
	},

	/**
	* Called by the map panel menu to display the markers.
	* @param item The selected menu item.
	*/
	showMarkers: function(item) {
		var markerStyle = item.text;		
		var mapController = View.controllers.get('mapController');
		mapController.createMarkers(markerStyle);
		mapController.mapControl.showMarkers('energyDs', null);
		mapController.mapControl.showMarkerLegend();
	},

	/**
	* Called by the map controller to create the marker properties.
	* @param markerStyle The AbStopLight Colorbrewer style.
	*/
	createMarkers: function(markerStyle) {
		var dataSource = 'energyDs';
		var keyFields = ['bill_archive.bl_id'];
		var geometryFields = ['bl.lon', 'bl.lat'];
		var titleField = 'bill_archive.name';
		var contentFields = ['bill_archive.eui'];
		
		var markerProperties = {
			//optional
			radius: 10,
			fillColor: '#e41a1c',
			fillOpacity: 0.90,			
			stroke: true,
			strokeColor: '#fff',
			strokeWeight: 1.0,			
			// required for thematic markers
			renderer: 'thematic-class-breaks',
			thematicField: 'bill_archive.eui',
			// legend overrides
   			thematicLegendTitle: 'Energy Use (kW)',
   			thematicLegendLabels: ['< 500', '500 - 1,000', '1,000 - 2,000', '2,000 - 3,000', '3,000 - 4,000', '4,000 - 5,000', '5,000 - 10,000', '10,000 - 20,000', '> 20,000'],
			thematicClassBreaks: [500,1000,2000,3000,4000,5000,10000,20000],
			colorBrewerClass: markerStyle
		};	

		View.controllers.get('mapController').mapControl.createMarkers(
			dataSource,
			keyFields,
			geometryFields,
			titleField,
			contentFields,
			markerProperties
		);
	},

	/**
	* Called by the map controller to display the marker legend.
	*/
   	showLegend: function(){
		var mapController = View.controllers.get('mapController');
		mapController.mapControl.showMarkerLegend();
	}

	// energyData_onShowData: function(rows) {   
	    
	//  // get selected rows from grid
	// 	var selectedRows = this.energyData.getSelectedRows(rows);  
		    				
	// 	// create restriction based on the selected rows
	// 	var restriction = new Ab.view.Restriction();
	// 	if(selectedRows.length !== 0 ) {
	// 			for (var i = 0; i < selectedRows.length; i++) {
	// 				restriction.addClause('bill_archive.bl_id', selectedRows[i]['bill_archive.bl_id'], "=", "OR");
	// 			}
	// 	}
	// 	else{
	// 		restriction.addClause('bill_archive.bl_id', 'null', "=", "OR");
	// 	}
		    	
	// 	//show the markers on the map
	// 	this.mapControl.showMarkers('energyDs', restriction);

	// } 	 


})

