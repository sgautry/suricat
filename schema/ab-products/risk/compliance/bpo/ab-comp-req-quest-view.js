var compReqQuestViewController = View.createController('compReqQuestView', {
	mainController: null,
	selected_questionnaire_id: 0,
	selected_questionnaire_title: '',
	multipleValueSeparator: Ab.form.Form.MULTIPLE_VALUES_SEPARATOR,
	
	afterViewLoad: function() {				
		$('checkboxShowReqQuest').checked = false;
		
		var grid = this.compReqQuestView_questionnaireList;
		grid.afterCreateCellContent = function(row, column, cellElement) {
			if (column.id == 'questionnaire_ext.questionnaire_title')	{
				cellElement.style.fontWeight = 'bold';
			}
		};
		
		if (View.parameters && View.parameters.showAddButton) this.compReqQuestView_questionnaireList.actions.get('add').show(true);
		if (View.parameters && View.parameters.showSelectButton) this.compReqQuestView_questionnaireView.actions.get('select').show(true);

	},

	afterInitialDataFetch: function () {
        /**
		 * If questionnaireId is passed as parameter, filter the questionnaire grid and select the questionnaire id.
         */
        if (View.parameters && View.parameters.questionnaireId) {
        	this.compReqQuestView_filter.setFieldValue('regrequirement.questionnaire_id', View.parameters.questionnaireId);
        	this.compReqQuestView_filter_onShow();
            var firstRow = this.compReqQuestView_questionnaireList.rows[0];
            if (valueExists(firstRow)) {
                firstRow.row.cells.get(0).dom.childNodes[0].click();
			}
        }
	},
	
	compReqQuestView_filter_onClear: function() {
		this.compReqQuestView_filter.clear();
    	$('checkboxShowReqQuest').checked = false;
	},

	compReqQuestView_filter_onShow: function() {
		var questionnaireId = this.compReqQuestView_filter.getFieldValue('regrequirement.questionnaire_id');
		var regulation = this.compReqQuestView_filter.getFieldValue('regrequirement.regulation');
		var reg_program = this.compReqQuestView_filter.getFieldValue('regrequirement.reg_program');
		var reg_requirement = this.compReqQuestView_filter.getFieldValue('regrequirement.reg_requirement');
		var status = this.compReqQuestView_filter.getFieldValue('questionnaire_ext.status');
		var restriction = "1=1";
		if (regulation != '' || reg_program != '' || reg_requirement != '') {
			restriction = "EXISTS(SELECT 1 FROM regrequirement WHERE regrequirement.questionnaire_id = questionnaire_ext.questionnaire_id";
			if (regulation != '') restriction += " AND regrequirement.regulation = '" + regulation + "' ";
			if (reg_program != '') restriction += " AND regrequirement.reg_program = '" + reg_program + "' ";
			if (reg_requirement != '') restriction += " AND regrequirement.reg_requirement = '" + reg_requirement + "' ";
			restriction += ")";
		}
        if (valueExistsNotEmpty(questionnaireId)) {
            restriction += " AND questionnaire_ext.questionnaire_id=" + questionnaireId;
        }
		if (status != '') {
			restriction += " AND questionnaire_ext.status = '" + status + "' ";
		}
		if ($('checkboxShowReqQuest').checked) restriction += " AND EXISTS(SELECT 1 FROM regrequirement WHERE regrequirement.questionnaire_id = questionnaire_ext.questionnaire_id) ";

		this.compReqQuestView_questionnaireList.refresh(restriction);

	},

	selectQuestionnaire: function(obj) {
		var questionnaire_id = obj.restriction['questionnaire_ext.questionnaire_id'];
		this.selected_questionnaire_id = questionnaire_id;
		var restriction = new Ab.view.Restriction();
		restriction.addClause('questionnaire_ext.questionnaire_id', questionnaire_id);
		var record = View.dataSources.get('questionnairesDataSource').getRecord(restriction);
		this.selected_questionnaire_title = record.getValue('questionnaire_ext.questionnaire_title');

		this.compReqQuestView_questionsTree.addParameter('questionnaireId', questionnaire_id);
		this.compReqQuestView_questionsTree.addParameter('multipleValueSeparator', this.multipleValueSeparator);
		this.compReqQuestView_questionsTree.refresh();
		this.compReqQuestView_questionsTree.appendTitle(this.selected_questionnaire_title);
		this.compReqQuestView_questionsTree.show();
		onTreeExpandAll(this.compReqQuestView_questionsTree, null);
		
		var questionnaireView = View.panels.get('compReqQuestView_questionnaireView').contentView;
		if (questionnaireView) {
			loadQuestionnairePanel();
		} else {
			setTimeout(loadQuestionnairePanel, 5000);
		}
		this.compReqQuestView_questionnaireView.refresh();
		this.compReqQuestView_questionnaireView.show();
	},
	
	compReqQuestView_questionnaireView_onSelect: function() {
		if (View.parameters && View.parameters.callback && this.selected_questionnaire_id) {
			View.parameters.callback(this.selected_questionnaire_id, this.selected_questionnaire_title);
		}
	},
	
	compReqQuestView_questionnaireList_onAdd: function() {
		var controller = this;
		View.openDialog('ab-common-quest-ext-def-questionnaire-edit.axvw', null, true, {
			callback: function(questionnaire_id, questionnaire_title, newRecord) {
				controller.afterSaveQuestionnaire(questionnaire_id, questionnaire_title, newRecord);
			}
		});
	},
	
	afterSaveQuestionnaire: function(questionnaire_id, questionnaire_title, newRecord) {
		if (View.parameters && View.parameters.callback && questionnaire_id) {
			View.parameters.callback(questionnaire_id, questionnaire_title);
			View.closeThisDialog();
		}
	}
});

function loadQuestionnairePanel() {
	var controller = View.controllers.get('compReqQuestView');
	var questionnaireView = View.panels.get('compReqQuestView_questionnaireView').contentView;
	if (questionnaireView) {
		var completeQuestionnaireController = questionnaireView.controllers.get('CompleteQuestionnaire');
	    completeQuestionnaireController.loadQuestionnaire(controller.selected_questionnaire_id, false, null, null);
	}
}

function compReqQuestView_questionsTree_onSelectQuestion() {
	var controller = View.controllers.get('compReqQuestView');
	var tree = View.panels.get('compReqQuestView_questionsTree');
	var lastNodeClicked = tree.lastNodeClicked;
	var question_id = lastNodeClicked.data['question_ext.question_id'];
	
	var restriction = new Ab.view.Restriction();
	restriction.addClause('question_ext.question_id', question_id);
	var record = View.dataSources.get('questionsDataSource').getRecord(restriction);
	var question_type = record.getValue('question_ext.question_type');
	
	var parent_question_id = question_id;
	var hierarchy_ids = record.getValue('question_ext.hierarchy_ids');
	var isChildQuestion = getIsChildQuestion(hierarchy_ids);
	if (isChildQuestion) parent_question_id = hierarchy_ids.substring(0, hierarchy_ids.indexOf('|'));
	
	View.openDialog('ab-common-quest-ext-def-questionnaire-dtl.axvw', null, false, {
		'width':800,
		'question_id': parent_question_id,
		'isChildQuestion': isChildQuestion,
		'child_question_id':question_id,
		'questionnaire_id':controller.selected_questionnaire_id,
		'question_type':question_type
	});
}