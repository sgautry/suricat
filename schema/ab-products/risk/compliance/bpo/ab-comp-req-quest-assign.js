var compReqQuestAssignController = View.createController('compReqQuestAssign', {
	old_questionnaire_id: '',
	old_questionnaire_title: '',
	
	afterInitialDataFetch: function() {

	},
	
	compReqQuestAssign_form_afterRefresh: function() {
		this.old_questionnaire_id = this.compReqQuestAssign_form.getFieldValue('regrequirement.questionnaire_id');
		this.old_questionnaire_title = this.compReqQuestAssign_form.getFieldValue('questionnaire_ext.questionnaire_title');
		
		var questionnaire_id = this.compReqQuestAssign_grid.restriction.findClause('regrequirement.questionnaire_id').value;
		this.compReqQuestAssign_form.setFieldValue('regrequirement.questionnaire_id', questionnaire_id);
		
		var restriction = new Ab.view.Restriction();
		restriction.addClause('questionnaire_ext.questionnaire_id', questionnaire_id);
		var record = View.dataSources.get('questionnairesDataSource').getRecord(restriction);
		var questionnaire_title = record.getValue('questionnaire_ext.questionnaire_title');
		this.compReqQuestAssign_form.setFieldValue('questionnaire_ext.questionnaire_title', questionnaire_title);

	},
	
	compReqQuestAssign_form_onClear: function() {
		this.compReqQuestAssign_form.refresh(this.compReqQuestAssign_grid.restriction, true);
	},
	
	compReqQuestAssign_form_onSave: function() {
        var restriction = new Ab.view.Restriction();
        restriction.addClause('regrequirement.regulation', this.compReqQuestAssign_form.getFieldValue('regrequirement.regulation'));
        restriction.addClause('regrequirement.reg_program', this.compReqQuestAssign_form.getFieldValue('regrequirement.reg_program'));
        restriction.addClause('regrequirement.reg_requirement', this.compReqQuestAssign_form.getFieldValue('regrequirement.reg_requirement'));
        this.compReqQuestAssign_form.refresh(restriction, false);
        
        var questionnaire_id = this.compReqQuestAssign_form.getFieldValue('regrequirement.questionnaire_id');
        var questionnaire_title = this.compReqQuestAssign_form.getFieldValue('questionnaire_ext.questionnaire_title');
        var reg_requirement = this.compReqQuestAssign_form.getFieldValue('regrequirement.reg_requirement');
        if (this.old_questionnaire_id != '' && this.old_questionnaire_id != questionnaire_id) {
        	var controller = this;
        	var message = String.format(getMessage('confirmOverwriteQuestionnaireId'), reg_requirement, this.old_questionnaire_title, questionnaire_title);
        	View.confirm(message, function(button){
    			if (button == 'yes') {
    				if (controller.compReqQuestAssign_form.save()) {
    					controller.compReqQuestAssign_grid.refresh();
    					View.getOpenerView().controllers.get('compReqQuest').refreshQuestionnaireProfile();
    				}
    			} else {
    				return;
    			}
    		});
		} else {
			if (this.compReqQuestAssign_form.save()) {
				this.compReqQuestAssign_grid.refresh(); 
				View.getOpenerView().controllers.get('compReqQuest').refreshQuestionnaireProfile();
			}
		}              
	},
	
	compReqQuestAssign_grid_onUnassignQuestionnaire: function(row) {
		var record = row.getRecord();
		record.setValue('regrequirement.questionnaire_id', '');
		this.compReqQuestAssign_ds.saveRecord(record);
		this.compReqQuestAssign_grid.refresh();
		View.getOpenerView().controllers.get('compReqQuest').refreshQuestionnaireProfile();		
	}
});