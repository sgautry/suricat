/**
 * @author Eric_Maxfield@archibus.com
 * 23.2 New implementation for Compliance Functional Surveys / Questionnaires
 */

var compDefUnitsController =  View.createController('compDefUnitsController', {
	
	/**
	 * unitTypePrefix - This is an application-specific prefix string for unit definitions used to prevent conflicting records between applications.
	 * Do not translate.
	 * Do not modify without taking care to also update any existing data records throughout the database.  
	 */
	unitTypePrefix: "COMP - ",
	
	abCompDefUnitsTypeEditForm_beforeSave: function(){
        var canSave = true;
		// append prefix to unit type text before saving
    	var unitsTypeForm = this.abCompDefUnitsTypeEditForm;
    	var displayText = unitsTypeForm.getFieldValue('bill_type.bill_type_id');
    	if (valueExistsNotEmpty(displayText)) {
            unitsTypeForm.setFieldValue('bill_type.bill_type_id', this.unitTypePrefix + displayText);
		} else {
            unitsTypeForm.fields.get('bill_type.bill_type_id').setInvalid(getMessage('errorMeasurementType'));
            canSave = false;
		}
        return canSave;
	},
	
	/**
	 * abCompDefUnitsUnitEditForm_beforeSave
	 * @parameters: none
	 * @returns: boolean, used by the core to continue saving the record (if true) or not (if false)
	 * 
	 * This method first verifies that the form contains a non-zero value for conversion factor.
	 * Then, because the Compliance application expects a "COMP - " string prefix to all bill_type_id values it uses,
	 * the method ensures that the prefix exists, by first checking, then appending if not present.
	 */
	abCompDefUnitsUnitEditForm_beforeSave: function(){		
		// verify conversion factor input		
		var editPanel = this.abCompDefUnitsUnitEditForm;
		var conversion_factor = editPanel.getFieldValue('bill_unit.conversion_factor');
		if (conversion_factor<=0) {
			View.alert(getMessage('verifyFactor'));
			return false;
		}else {
			//before saving, append display text with unitTypePrefix, if needed			
	    	var displayText = editPanel.getFieldValue('bill_unit.bill_type_id');
	    	if(!displayText.startsWith(this.unitTypePrefix)){
	    		editPanel.setFieldValue('bill_unit.bill_type_id', this.unitTypePrefix + displayText);
	    	}	    	
			return true;
		}
	},
	
	abCompDefUnitsTypeEditForm_afterRefresh: function(){
		this.trimPrefixFromBillType_BillType();
	},

	abCompDefUnitsUnitEditForm_afterRefresh: function(){
		this.trimPrefixFromBillUnit_BillType();
	},
	
	// trim prefix from unit type text for display
	trimPrefixFromBillUnit_BillType : function(){
		var unitsForm = this.abCompDefUnitsUnitEditForm;
		var displayText = unitsForm.getFieldValue('bill_unit.bill_type_id').replace(this.unitTypePrefix,'');
    	unitsForm.setFieldValue('bill_unit.bill_type_id', displayText);
	},
	// trim prefix from bill type text for display
	trimPrefixFromBillType_BillType : function(){	
		var unitsTypeForm = this.abCompDefUnitsTypeEditForm;
		var displayText = unitsTypeForm.getFieldValue('bill_type.bill_type_id').replace(this.unitTypePrefix,'');
    	unitsTypeForm.setFieldValue('bill_type.bill_type_id', displayText);
	},
	
	/**
	 * resetDefaultVal()
	 * @parameters: none
	 * @returns: null
	 * 
	 * if Default=1, then all other records in 'bill_unit' must be set to Default=0
	 * 
	 * The bill_unit table contains an "Is Default" field (is_dflt).  Within each family of units (units belonging to one bill_type_id),
	 * applications expect a single record to be identified as the default.  This often serves to determine which record appears first
	 * in a drop-down selection list and sometimes which unit of measure appears by default in summary reports.  If there were more than
	 * one 'default' record, the application code would, presumably, choose the first one it obtains in a query as the 'default', but
	 * the behavior might not be what users expect.  So the method below ensure that if a user sets the selected record's "Is Default"
	 * flag to true and saves the record, that none of the other related units of measure are not also flagged as default.
	 */
	resetDefaultVal: function() {
		var editPanel = this.abCompDefUnitsUnitEditForm;
		var oldValueIsDefault = editPanel.getOldFieldValues()[("bill_unit.is_dflt")];
		editPanel.save();
		
		var isDefault = editPanel.getFieldValue('bill_unit.is_dflt');
		var bill_unit_id = editPanel.getFieldValue('bill_unit.bill_unit_id');
		//if the current record's default flag has changed to 1 (from zero or null for a new record), then set related units to not default 	
		if (isDefault && isDefault == "1" && oldValueIsDefault!="1") {
			var grid = View.panels.get("abCompDefUnitsUnitGrid");
			var records = [];
			grid.gridRows.each(function(row){
				var record = row.getRecord();
				if (record.getValue("bill_unit.bill_unit_id") != bill_unit_id) {
					record.setValue("bill_unit.is_dflt","0");
					records.push(record);
				}
			});
			
	 		this.abCompDefUnitsUnitDS.saveRecords(records);
		}		
		this.trimPrefixFromBillUnit_BillType();
	}
	
});        


//APP-1899 - define startsWith method for IE browser
if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position){
      position = position || 0;
      return this.indexOf(searchString, position) === position;
  };
}


/**
 * Before deleting a unit definition, append display text with unitTypePrefix, if needed, to avoid message about mismatched selection
 */
function abCompDefUnitsUnitEditForm_deleteRecord(){
	View.confirm(getMessage("messageConfirmDelete"), function(button) {        
		if (button == 'yes') {
			var editPanel = View.panels.get("abCompDefUnitsUnitEditForm");
			var displayText = editPanel.getFieldValue('bill_unit.bill_type_id');
			if(!displayText.startsWith(this.unitTypePrefix)){
				editPanel.setFieldValue('bill_unit.bill_type_id', this.unitTypePrefix + displayText);
			}
			editPanel.deleteRecord();
			editPanel.show(false);
			View.panels.get("abCompDefUnitsUnitGrid").refresh();
		}
	});
}

/**
 * Before allowing deletion of a measurement type, require the user first delete all individual unit records within.
 * This is a protective measure, as an accidental delete of an entire category of units could have devastating effects
 * on an existing data set that references those units.  It would be possible, but tedious, for this application code to 
 * check for the existence of any data record referencing the unit definition; however, edits, and certainly deletions, of
 * these unit definitions should be rare in practice once an application has been deployed.
 */
function abCompDefUnitsTypeEditForm_deleteRecord(){
	View.confirm(getMessage("messageConfirmDelete"), function(button) {        
		if (button == 'yes') {			
			//now check to see if any child (unit) records exist before proceeding
			var unitsGrid = View.panels.get("abCompDefUnitsUnitGrid");
			var unitTypePrefix = View.controllers.get("compDefUnitsController").unitTypePrefix;			
			if(unitsGrid.rows.length == 0){
				var editPanel = View.panels.get("abCompDefUnitsTypeEditForm");
				var displayText = editPanel.getFieldValue('bill_type.bill_type_id');
				if(!displayText.startsWith(this.unitTypePrefix)){
					editPanel.setFieldValue('bill_type.bill_type_id', this.unitTypePrefix + displayText);
				}
				editPanel.deleteRecord();
				editPanel.show(false);
				View.panels.get("abCompDefUnitsTypeGrid").refresh();
				View.panels.get("abCompDefUnitsUnitGrid").show(false);
			} 
			else {
				//display message instructing user to first delete child records
				View.showMessage(getMessage("messageUnitsExist"));
			}
			
		}
	});
}

function abCompDefUnitsTypeEditForm_saveForm(event){	
	var editForm = View.panels.get("abCompDefUnitsTypeEditForm");
	editForm.save();
	
	var restriction = new Ab.view.Restriction();
	var savedBillType = editForm.getFieldValue('bill_type.bill_type_id');
	restriction.addClause("bill_unit.bill_type_id", savedBillType, "=");
	View.panels.get("abCompDefUnitsUnitGrid").refresh(restriction);
	
	View.panels.get("abCompDefUnitsTypeGrid").refresh();
    View.controllers.get("compDefUnitsController").trimPrefixFromBillType_BillType();
}

function abCompDefUnitsUnitEditForm_saveForm(){	
	var controller = View.controllers.get("compDefUnitsController");
	controller.resetDefaultVal();
	View.panels.get("abCompDefUnitsUnitEditForm").save();
	controller.trimPrefixFromBillUnit_BillType();
	View.panels.get("abCompDefUnitsUnitGrid").refresh(); 
}