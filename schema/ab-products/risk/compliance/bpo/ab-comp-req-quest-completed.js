var commonQuestExtCompletedViewController = View.createController('commonQuestExtCompletedView', {
	questionnaire_id: null,
	survey_event_id: 0,
	
	selectRequirement: function(obj) {		
		var regulation = obj.restriction['regrequirement.regulation'];
		var reg_program = obj.restriction['regrequirement.reg_program'];
		var reg_requirement = obj.restriction['regrequirement.reg_requirement'];
		var restriction = new Ab.view.Restriction();
		restriction.addClause('regrequirement.regulation', regulation);
		restriction.addClause('regrequirement.reg_program', reg_program);
		restriction.addClause('regrequirement.reg_requirement', reg_requirement);
		
		var record = View.dataSources.get('commonQuestExtCompletedView_ds0').getRecord(restriction);
		this.questionnaire_id = record.getValue('regrequirement.questionnaire_id');		
		this.commonQuestExtCompletedView_surveyEventsGrid.addParameter('questionnaireId', this.questionnaire_id);
		this.commonQuestExtCompletedView_surveyEventsProfile.addParameter('questionnaireId', this.questionnaire_id);
		this.commonQuestExtCompletedView_surveyEventsGrid.refresh();
	},
	
	selectEvent: function(obj) {	
		var survey_event_id = obj.restriction['activity_log.activity_log_id'];
		this.commonQuestExtCompletedView_answersGrid.refresh("quest_answer_ext.survey_event_id=" + survey_event_id);
		this.commonQuestExtCompletedView_answersGrid.appendTitle(getDescriptiveTitle(obj.restriction, this.questionnaire_id));
		//this.commonQuestExtCompletedView_surveyEventsProfile.refresh(obj.restriction);
		this.survey_event_id = obj.restriction['activity_log.activity_log_id'];
		
		if (this.questionnaire_id != null) {
			var questTab = View.panels.get('commonQuestExtCompletedView_tabs').findTab('commonQuestExtCompletedView_tab1');
			if (questTab.isContentLoaded) {
				loadQuestionnairePanel();
			} else {
				setTimeout(loadQuestionnairePanel, 5000);
			}
		}
	}	
});

function loadQuestionnairePanel() {
	var controller = View.controllers.get('commonQuestExtCompletedView');
	var questTab = View.panels.get('commonQuestExtCompletedView_tabs').findTab('commonQuestExtCompletedView_tab1');
	if (questTab.isContentLoaded) {
		questTab.getContentFrame().View.controllers.get('UpdateQuestionnaire').loadQuestionnaireWithResults(controller.questionnaire_id, true, controller.survey_event_id);
	}
}

function getDescriptiveTitle(restriction, questionnaire_id) {
	var record = View.dataSources.get('eventsDataSource').getRecord(restriction);
	var questRecord = View.dataSources.get('questionnairesDataSource').getRecord('questionnaire_ext.questionnaire_id = ' + questionnaire_id);
	var message = questRecord.getValue('questionnaire_ext.questionnaire_title');
	message += ' - ' + record.getLocalizedValue('activity_log.date_scheduled') + ' ' + record.getValue('activity_log.action_title');
	return message;
	
	
}