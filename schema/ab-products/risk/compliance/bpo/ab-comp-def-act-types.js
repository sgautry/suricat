 

var complianceActivitytypeController = View.createController('complianceActivitytypeController', {

	complianceActivityPrefix: 'COMPLIANCE - ',
	
	handleActivitytypeSelection: function() {
		var selectedRow = this.complianceActivitytypeGrid.rows[this.complianceActivitytypeGrid.selectedRowIndex];
		var activityType = selectedRow['activitytype.activity_type.key'];
		var restriction = new Ab.view.Restriction();
		restriction.addClause('activitytype.activity_type', activityType, '=');
		
		//Parts of the Compliance application depend on the activityType 'COMPLIANCE - EVENT' so we should not alter it;
		//nor should we alter activityType values for which other applications might have hard-coded dependencies.
		var enableActivitytypeField = false;
		if(activityType.substring(0,13) == this.complianceActivityPrefix && activityType != 'COMPLIANCE - EVENT'){
			enableActivitytypeField = true;
		}
		this.setActivitytypeFieldEnabledOrDisabled(enableActivitytypeField);
		
		
		this.complianceActivitytypeForm.refresh(restriction);
	},
	
	setActivitytypeFieldEnabledOrDisabled: function(enableIt) {
		var form = this.complianceActivitytypeForm;
		form.enableField('activitytype.activity_type',enableIt);
	},
	
	setDescriptionFieldEnabledOrDisabled: function(enableIt) {
		var form = this.complianceActivitytypeForm;
		form.enableField('activitytype.description',enableIt);
	},
	
	complianceActivitytypeForm_onCancelEditActivityttype: function() {
		this.complianceActivitytypeForm.show(false);
	},
	
	complianceActivitytypeForm_beforeSave: function(form) {		
		if(form.newRecord) {
			var record = form.getRecord();
			var activityTypeString = record.getValue('activitytype.activity_type');
			if(!activityTypeString.startsWith(this.complianceActivityPrefix) && activityTypeString != '') {
				activityTypeString = this.complianceActivityPrefix.concat(activityTypeString);
				form.setInputValue('activitytype.activity_type',activityTypeString);
				record.setValue('activitytype.activity_type',activityTypeString);
			}
		}
	},
	
	saveRecord: function() {
		var form = this.complianceActivitytypeForm;
		this.complianceActivitytypeForm_beforeSave(form);
		form.save();
		this.complianceActivitytypeGrid.refresh();
	}
});