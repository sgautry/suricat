var compReqQuestController = View.createController('compReqQuest', {
	
	/* Add view parameters to be used by ab-common-quest-ext-def-questionnaire.axvw included common view for defining and editing Questionnaires 
	 * 	1) showAssignButton: show the Assign button in the Questionnaire details panel
	 * 	2) assignButtonTitle: add custom title to the Assign button
	 * 	3) assignQuestionnaire: define callback function for the Assign button.  This will pass the selected questionnaire_id back to the opener view.
	 *  4) clearFilter: define callback function for clearing filter (used after adding a new Questionnaire)
	 *	5) getAdditionalText: define callback function to append application-specific text to Questionnaire details panel (used in Compliance to list the Compliance Requirements associated with the selected Questionnaire)
	 *  6) hideQuestionnaireList: hide the Questionnaire List if a single Questionnaire is pre-selected in the view (used in the Manage Compliance Requirements tabbed view)
	 */
	afterViewLoad: function() {
		var controller = this;
		View.parameters = {
			openerController: controller,
            showAssignButton: true,
            assignButtonTitle: getMessage('assignButtonTitle'),
            assignQuestionnaire: function(questionnaire_id) {
            	controller.assignQuestionnaire(questionnaire_id);
            },
            clearFilter: function() {
            	controller.compReqQuest_filter_onClear();
            },
            getAdditionalText: function(questionnaire_id) {
            	return controller.getAdditionalText(questionnaire_id);
            },
            hideQuestionnaireList: false,
            measurement: {
                unitTypePrefix: 'COMP - ',
                activityId: 'AbRiskCompliance'
			}
        };
		$('checkboxShowReqQuest').checked = false;
	},
	
	compReqQuest_filter_onClear: function() {
		this.compReqQuest_filter.clear();
    	$('checkboxShowReqQuest').checked = false;
	},
	
	getAdditionalText: function(questionnaire_id) {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('regrequirement.questionnaire_id', questionnaire_id);
		var records = this.compReqQuest_ds0.getRecords(restriction);
		var requirements = '';
		for (var i=0;i<records.length;i++) {
			if (requirements != '') requirements += ', ';
			requirements += String.format(getMessage('requirementTitle'), records[i].getValue('regrequirement.reg_requirement'), records[i].getValue('regrequirement.reg_program'));
		}
		var message = '';
		if (requirements == '') requirements = getMessage('none');
		message += '<span style="font-weight: bold">' + getMessage('assignedToReq') + '</span>';
		message += '<span style="color: #4D4B4B"> ' + requirements + '</span>';
		return message;
	},
	
	/* Passes filter restriction to the common questionnaire view controller */
	compReqQuest_filter_onShow: function() {
		var regulation = this.compReqQuest_filter.getFieldValue('regrequirement.regulation');
		var reg_program = this.compReqQuest_filter.getFieldValue('regrequirement.reg_program');
		var reg_requirement = this.compReqQuest_filter.getFieldValue('regrequirement.reg_requirement');
		var status = this.compReqQuest_filter.getFieldValue('questionnaire_ext.status');
		var restriction = "1=1";
		if (regulation != '' || reg_program != '' || reg_requirement != '') {
			restriction = "EXISTS(SELECT 1 FROM regrequirement WHERE regrequirement.questionnaire_id = questionnaire_ext.questionnaire_id";
			if (regulation != '') restriction += " AND regrequirement.regulation = '" + regulation + "' ";
			if (reg_program != '') restriction += " AND regrequirement.reg_program = '" + reg_program + "' ";
			if (reg_requirement != '') restriction += " AND regrequirement.reg_requirement = '" + reg_requirement + "' ";
			restriction += ")";
		}
		if (status != '') {
			restriction += " AND questionnaire_ext.status = '" + status + "' ";
		}
		if ($('checkboxShowReqQuest').checked) restriction += " AND EXISTS(SELECT 1 FROM regrequirement WHERE regrequirement.questionnaire_id = questionnaire_ext.questionnaire_id) ";
		var questionnaireView = View.panels.get('commonDefQuestionnaireView').contentView;
		if (questionnaireView) {
			questionnaireView.controllers.get('abCommonQuestExtQuestionnaireDef').applyFilterRestrictionToQuestionnaires(restriction);
		}
	},
	
	assignQuestionnaire: function(questionnaire_id) {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('regrequirement.questionnaire_id', questionnaire_id);
		View.openDialog('ab-comp-req-quest-assign.axvw', restriction);
	},
	
	/* Called by Assign Questionnaire to Requirement dialog view */
	refreshQuestionnaireProfile: function() {
		var questionnaireView = View.panels.get('commonDefQuestionnaireView').contentView;
		if (questionnaireView) {
			questionnaireView.controllers.get('abCommonQuestExtQuestionnaireDef').questionnaireProfile.refresh();
		}
	}
});