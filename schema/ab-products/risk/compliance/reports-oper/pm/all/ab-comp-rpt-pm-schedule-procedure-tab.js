/**
 * Added for 22.1 Compliance and Building Operations Integration: Compliance Work History report - Work Request Tab.
 */
var abCompRptPmsTabCtrl = View.createController('abCompRptPmsTabCtrl', {
	currentShow: "pms",
	
	isContract:(this.name.substring(0,8) == 'contract') ? true : false,
	
	//Event restriction from table activity_log only related to date
	pmsDateRestriction:" 1=1 ",
	pmsDateFieldsArraysForRes: new Array( ['pms.date_last_completed']),

	pmsRes:" 1=1 ",
	pmsResFieldsArrays: new Array( 
			['pms.site_id'], ['pms.bl_id'], ['pms.fl_id'], 
			['pms.pmp_id'], ['pmp.pmp_type'], ['pms.pm_group'],
			['eqstd.category'], ['eq.eq_std'],  
			['regprogram.regulation'], ['regprogram.reg_program'], ['regrequirement.reg_requirement'], 
			['regrequirement.regreq_cat'], ['regrequirement.regreq_type'], 
			['regprogram.project_id'], ['regprogram.priority']),

	afterInitialDataFetch: function(){
		checkLicense(['AbBldgOpsPM'], this.abCompRptPmsConsole, true); 
		
		if (this.isContract){	
			this.setContractFieldTitles();
		}
	},

	abCompRptPmsConsole_onShow: function(){
		if ( 'pms' == this.currentShow)	 {
			this.abCompRptPmpGrid.show(false);
			this.showAndRefreshGrid(this.abCompRptPmsGrid);
		} else {
			this.abCompRptPmsGrid.show(false);
			this.showAndRefreshGrid(this.abCompRptPmpGrid);
		}
	},
	
	setContractFieldTitles: function(){
		var consolePanel = this.abCompRptPmsConsole;
		
		consolePanel.setFieldLabel('regrequirement.regreq_cat', (getMessage('requirementCategory')));
		consolePanel.setFieldLabel('regrequirement.regreq_type', (getMessage('requirementType')));
		consolePanel.setFieldLabel('regrequirement.reg_requirement', (getMessage('requirement')));
		consolePanel.setFieldLabel('regprogram.reg_program', (getMessage('program')));
		
		var gridPanel = this.abCompRptPmsGrid;
		gridPanel.setFieldLabel('pms.priority', (getMessage('requirementPriority')));
		gridPanel.setFieldLabel('pms.reg_requirement', (getMessage('requirement')));
		gridPanel.setFieldLabel('pms.reg_program', (getMessage('program')));
		
		var gridPanel = this.abCompRptPmpGrid;
		gridPanel.setFieldLabel('pmp.priority', (getMessage('requirementPriority')));
		gridPanel.setFieldLabel('pmp.reg_requirement', (getMessage('requirement')));
		gridPanel.setFieldLabel('pmp.reg_program', (getMessage('program')));
	},

	showAndRefreshGrid: function(gridPanel){
		this.pmsDateRestriction = getDatesRestrictionFromConsole(this.abCompRptPmsConsole, this.pmsDateFieldsArraysForRes);

		this.pmsRes = getRestrictionStrFromConsole( this.abCompRptPmsConsole, this.pmsResFieldsArrays); 
		var wr_status = this.abCompRptPmsConsole.getFieldValue('wr.status');
		if (wr_status != '') {
		  if ('pms' == this.currentShow) {
		    this.pmsRes = this.pmsRes + "  AND EXISTS (SELECT 1 FROM wr WHERE wr.pms_id=pms.pms_id AND wr.status='" + wr_status +"')";
		  }
		  else {
		    this.pmsRes = this.pmsRes + "  AND wr.status='" + wr_status +"'";			  
		  }
		}
		
		var isContract = (this.view.parentTab.name.substring(0,8) == 'contract') ? true : false;		
		if(isContract == true){			
			this.pmsRes += " AND regprogram.is_contract = 1";
		}else{
			this.pmsRes += " AND regprogram.is_contract = 0";			
		}


		gridPanel.addParameter("pmsRes", this.pmsDateRestriction + " and " + this.pmsRes);

		
		gridPanel.setSingleVisiblePanel(true);
		gridPanel.refresh();
		
		if (this.isContract){	
			this.setContractFieldTitles();
		}
		
		gridPanel.show(true);
	},

	abCompRptPmsGrid_onViewPmps: function(){	
		this.abCompRptPmsGrid.show(false);
		this.currentShow = "pmp";
		this.showAndRefreshGrid(this.abCompRptPmpGrid);
	},
		
	abCompRptPmpGrid_onViewPms: function(){	
		this.abCompRptPmpGrid.show(false);
		this.currentShow = "pms";
		this.showAndRefreshGrid(this.abCompRptPmsGrid);
	}
});

