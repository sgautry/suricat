var compDefineQuestController = View.createController('compDefineQuest', {
    mainController: null,
    selected_questionnaire_id: 0,
    isContract: false,
    /* Add view parameters to be used by ab-common-quest-ext-def-questionnaire.axvw included common view for defining and editing Questionnaires
     * 	1) showAssignButton: hide the Assign button in the Questionnaire details panel
     *	2) getAdditionalText: define callback function to append application-specific text to Questionnaire details panel
     *  	(used in Compliance to list the Compliance Requirements associated with the selected Questionnaire)
     *     	(used in Compliance to list the Contract Terms associated with the selected Questionnaire)
     *  3) hideQuestionnaireList: hide the Questionnaire List if a single Questionnaire is pre-selected in the view (used in the Manage Compliance Requirements tabbed view)
     *	4) showSelectButton: show the Select button in the Questionnaire Title (only shown when Questionnaire List is hidden)
     *	5) selectButtonTitle: add custom title to the Select button
     *	6) selectQuestionnaire: define callback function for the Select button
     */
    afterViewLoad: function () {
        var controller = this;
        View.parameters = {
            openerController: controller,
            showAssignButton: false,
            getAdditionalText: function (questionnaire_id) {
                return controller.getAdditionalText(questionnaire_id);
            },
            hideQuestionnaireList: true,
            showSelectButton: true,
            selectButtonTitle: getMessage('selectButtonTitle'),
            selectQuestionnaire: function () {
                controller.selectQuestionnaire();
            }
        };
    },
    afterInitialDataFetch: function () {
        this.mainController = View.getOpenerView().controllers.get(0);
        var restriction = new Ab.view.Restriction();
        restriction.addClause('regrequirement.regulation', this.mainController.regulation);
        restriction.addClause('regrequirement.reg_program', this.mainController.regprogram);
        restriction.addClause('regrequirement.reg_requirement', this.mainController.regrequirement);
        var record = this.compDefineQuest_ds0.getRecord(restriction);
        this.selected_questionnaire_id = record.getValue('regrequirement.questionnaire_id');
        if (this.selected_questionnaire_id == '') this.selected_questionnaire_id = 0;
        this.isContract = record.getValue('regprogram.is_contract') === '1';
        this.refreshPanelsWithQuestionnaire(this.selected_questionnaire_id);
    },
    getAdditionalText: function (questionnaire_id) {
        var restriction = new Ab.view.Restriction();
        restriction.addClause('regrequirement.questionnaire_id', questionnaire_id);
        restriction.addClause('regprogram.is_contract', this.isContract ? '1' : '0');
        var records = this.compDefineQuest_ds0.getRecords(restriction);
        var requirements = '';
        for ( var i = 0; i < records.length; i++ ) {
            if (requirements != '') requirements += ', ';
            if (this.isContract) {
                requirements += String.format(getMessage('contractTitle'), records[i].getValue('regrequirement.reg_requirement'), records[i].getValue('regrequirement.reg_program'));
            } else {
                requirements += String.format(getMessage('requirementTitle'), records[i].getValue('regrequirement.reg_requirement'), records[i].getValue('regrequirement.reg_program'));
            }
        }
        var message = '';
        if (requirements == '') requirements = getMessage('none');
        if (this.isContract) {
            message += '<span style="font-weight: bold">' + getMessage('assignedToContractTerm') + '</span>';
        } else {
            message += '<span style="font-weight: bold">' + getMessage('assignedToReq') + '</span>';
        }
        message += '<span style="color: #4D4B4B"> ' + requirements + '</span>';
        return message;
    },
    selectQuestionnaire: function () {
        var controller = this;
        View.openDialog('ab-comp-req-quest-view.axvw', null, false, {
            width: 1000,
            height: 600,
            showAddButton: true,
            showSelectButton: true,
            callback: function (selectedQuestionnaireId, selectedQuestionnaireTitle) {
                View.closeDialog();
                controller.saveQuestionnaireId(selectedQuestionnaireId, selectedQuestionnaireTitle);
            }
        });
    },
    saveQuestionnaireId: function (questionnaire_id, questionnaire_title) {
        var controller = this;
        var message = String.format(getMessage('saveQuestionnaireForRequirement'), questionnaire_title, controller.mainController.regrequirement, controller.mainController.regprogram);
        View.confirm(message, function (button) {
            if (button === 'yes') {
                var restriction = new Ab.view.Restriction();
                restriction.addClause('regrequirement.regulation', controller.mainController.regulation);
                restriction.addClause('regrequirement.reg_program', controller.mainController.regprogram);
                restriction.addClause('regrequirement.reg_requirement', controller.mainController.regrequirement);
                var record = controller.compDefineQuest_ds0.getRecord(restriction);
                record.setValue('regrequirement.questionnaire_id', questionnaire_id);
                controller.compDefineQuest_ds0.saveRecord(record);
                controller.refreshPanelsWithQuestionnaire(questionnaire_id);
            }
        });
    },
    refreshPanelsWithQuestionnaire: function (questionnaire_id) {
        var questionnaireView = View.panels.get('commonDefQuestionnaireView').contentView;
        if (questionnaireView) {
            var questionnaireController = questionnaireView.controllers.get('abCommonQuestExtQuestionnaireDef');
            questionnaireController.questionnaireReport.refresh('questionnaire_ext.questionnaire_id = ' + questionnaire_id);
            if (questionnaire_id !== 0) questionnaireController.refreshPanelsWithQuestionnaire(questionnaire_id);
            else {
                questionnaireController.collapseQuestionPanels(true, true, true);
                questionnaireController.questionsList.show(false);
                questionnaireController.questionnaireProfile.show(false);
                questionnaireController.questionSelectPanel.show(false);
                questionnaireController.questionSelectTitle.show(false);
                questionnaireController.questionCreatePanel.show(false);
            }
        }
    }
});