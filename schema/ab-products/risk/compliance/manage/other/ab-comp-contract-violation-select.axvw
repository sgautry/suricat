<view version="2.0">
	<layout id="main">
        <north id="consoleRegion" initialSize="191" split="true"/>
        <center id="row1"/>
    </layout>

    <message name="View" translatable="true">View</message>
    <message name="viewViolation" translatable="true">View Contract Violation</message>
    <message name="selectLocation" translatable="true">Select Contract Location</message>

    <panel type="view" id="messageView" file="ab-comp-location-field-title.axvw"/> 

    <panel type="view" id="console" file="ab-comp-contract-violation-console.axvw" layoutRegion="consoleRegion"/> 

    <dataSource id="abCompViolationDS">
        <sql dialect="generic">
        SELECT regviolation.violation_id, regviolation.violation_num, regviolation.regulation, regviolation.reg_program, regviolation.reg_requirement, regviolation.severity, regviolation.violation_type, regviolation.date_assessed, regviolation.authority,
               regviolation.cost_total, regviolation.status, regviolation.date_from, regviolation.date_to, regviolation.location_id, regviolation.doc, regviolation.issued_by, regviolation.summary, regviolation.description, regviolation.penalty,
               regrequirement.priority, regrequirement.regreq_cat, regrequirement.regreq_type, regprogram.regprog_cat, regprogram.regprog_type,  compliance_locations.ctry_id, compliance_locations.pr_id, compliance_locations.regn_id, 
               compliance_locations.bl_id, compliance_locations.state_id, compliance_locations.rm_id, compliance_locations.city_id, compliance_locations.eq_std, compliance_locations.county_id, compliance_locations.eq_id, compliance_locations.site_id,
               compliance_locations.em_id, compliance_locations.fl_id,compliance_locations.lat,compliance_locations.lon,
               ( regviolation.cost_base_payment * ${parameters['exchangeRate']} ) ${sql.as} cost_base_payment,
               ( regviolation.cost_total_payment * ${parameters['exchangeRate']} ) ${sql.as} cost_total_payment,
               ( regviolation.cost_vat_payment * ${parameters['exchangeRate']} ) ${sql.as} cost_vat_payment,
               (${parameters['currencyCode']}) ${sql.as} display_currency
               FROM regviolation
               LEFT OUTER JOIN compliance_locations ON regviolation.location_id=compliance_locations.location_id
               LEFT OUTER JOIN regprogram ON (regviolation.regulation = regprogram.regulation AND regviolation.reg_program = regprogram.reg_program)
               LEFT OUTER JOIN regrequirement ON (regviolation.regulation = regrequirement.regulation AND regviolation.reg_program = regrequirement.reg_program AND regviolation.reg_requirement = regrequirement.reg_requirement)
               WHERE regprogram.is_contract='1'
        </sql>
        <table name="regviolation" role="main"/>
        <table name="regrequirement" role="standard"/>
        <table name="regprogram" role="standard"/>
        <table name="compliance_locations" role="standard"/>
        <table name="regulation" role="standard"/>

        <field table="regrequirement" name="priority" />
        <field table="regrequirement" name="regreq_cat" />
        <field table="regrequirement" name="regreq_type" />

        <field table="regprogram" name="regprog_cat" />
        <field table="regprogram" name="regprog_type" />
        <field table="regprogram" name="is_contract" />

        <field table="compliance_locations" name="ctry_id" />
        <field table="compliance_locations" name="pr_id" />
        <field table="compliance_locations" name="regn_id" />
        <field table="compliance_locations" name="bl_id" />
        <field table="compliance_locations" name="state_id" />
        <field table="compliance_locations" name="rm_id" />
        <field table="compliance_locations" name="city_id" />
        <field table="compliance_locations" name="eq_std" />
        <field table="compliance_locations" name="county_id" />
        <field table="compliance_locations" name="eq_id" />
        <field table="compliance_locations" name="site_id" />
        <field table="compliance_locations" name="em_id" />
        <field table="compliance_locations" name="fl_id" />
        <field table="compliance_locations" name="lat" />
        <field table="compliance_locations" name="lon" />

		<field table="regulation" name="reg_rank" />

		<field table="regviolation" name="violation_id" />
        <field table="regviolation" name="violation_num"/>
        <field table="regviolation" name="regulation"/>
        <field table="regviolation" name="reg_program" />
        <field table="regviolation" name="reg_requirement"/>
        <field table="regviolation" name="severity" />
        <field table="regviolation" name="violation_type" />
        <field table="regviolation" name="date_assessed" />
        <field table="regviolation" name="violation_id" />
        <field table="regviolation" name="authority" />
        <field table="regviolation" name="cost_total"  currencyField="regviolation.display_currency"/>
        <field table="regviolation" name="cost_base_payment"  currencyField="regviolation.display_currency"/>
        <field table="regviolation" name="cost_total_payment"  currencyField="regviolation.display_currency"/>
        <field table="regviolation" name="cost_vat_payment"  currencyField="regviolation.display_currency"/>
        <field table="regviolation" name="status" />
        <field table="regviolation" name="date_from" />
        <field table="regviolation" name="date_to" />
        <field table="regviolation" name="location_id"/>
        <field table="regviolation" name="doc"/>
        <field table="regviolation" name="issued_by"/>
        <field table="regviolation" name="summary" />
        <field table="regviolation" name="description" />
        <field table="regviolation" name="penalty" />
        <field table="regviolation" name="display_currency" dataType="text"/>
        <parameter name="exchangeRate" dataType="verbatim" value="1.0"/>
        <parameter name="currencyCode" dataType="text" value="${project.budgetCurrency.code}"/>
        

   </dataSource>

   <panel type="grid" id="abCompViolationGrid" showOnLoad="true"  dataSource="abCompViolationDS" layoutRegion="row1">
        <title translatable="true">Select Contract Violations</title>

       <sortField table="regviolation" name="date_assessed" ascending="false"/>
       <sortField table="regviolation" name="violation_id"/>

		<action id="addNew">
            <title translatable="true">Add New</title>
        </action>
        <action id="doc">
            <title translatable="true">DOC</title>
        </action>
        <action id="xls">
            <title translatable="true">XLS</title>
            <command type="exportPanel" outputType="xls" panelId="abCompViolationGrid"/>
        </action>

        <field id="edit" controlType="button" >
            <title translatable="true">Edit</title>
        </field>   
		
        <field table="regviolation" name="violation_num"/>
        <field table="regviolation" name="violation_id" />
        <field table="regviolation" name="date_assessed" />
        <field table="regviolation" name="regulation"/>
        <field table="regviolation" name="reg_program">
            <title translatable="true">Contract Code</title>
        </field>
        <field table="regviolation" name="reg_requirement">
            <title translatable="true">Contract Term Code</title>
        </field>
        <field table="regviolation" name="severity" />
        <field table="regviolation" name="violation_type" />
        <field table="regviolation" name="cost_total" hidden="${View.activityParameters['AbCommonResources-EnableVatAndMultiCurrency'] == 1}" />
        <field table="regviolation" name="cost_base_payment" hidden="${View.activityParameters['AbCommonResources-EnableVatAndMultiCurrency'] == 0}" />
        <field table="regviolation" name="cost_vat_payment" hidden="${View.activityParameters['AbCommonResources-EnableVatAndMultiCurrency'] == 0}" />
        <field table="regviolation" name="cost_total_payment" hidden="${View.activityParameters['AbCommonResources-EnableVatAndMultiCurrency'] == 0}" />
        <field table="regviolation" name="doc" controlType="link"/>
        <field table="regviolation" name="authority" />
        <field table="regviolation" name="status" />
        <field table="compliance_locations" name="site_id" />
        <field table="compliance_locations" name="pr_id" />
        <field table="compliance_locations" name="bl_id" />
        <field table="compliance_locations" name="fl_id" />
        <field table="compliance_locations" name="rm_id" />
        <field table="compliance_locations" name="eq_std" />
        <field table="compliance_locations" name="city_id" />
        <field table="compliance_locations" name="county_id" />
        <field table="compliance_locations" name="state_id" />
        <field table="compliance_locations" name="regn_id" />
        <field table="compliance_locations" name="ctry_id" />
        <field table="compliance_locations" name="eq_id" />
        <field table="compliance_locations" name="em_id" />
        <field table="compliance_locations" name="lat" />
        <field table="compliance_locations" name="lon" />

    </panel>

	<js file="ab-comp-contract-violation-select.js"/>
	<js file="ab-repm-cost-mgmt-common.js"/>
	<js file="ab-comp-common.js"/>
</view>
