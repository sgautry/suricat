var abCompQuestSelectController = View.createController('abCompQuestSelectController', {
	
  
    setFormFieldsToDisplay : function(questionType,isChildQuestion) {
    	var form = this.questionnairesForm;
    	
    	// Hide all variable questions by default, then enable as required by specific cases
    	form.showField('question_ext.freeform_width',false);
    	form.showField('question_ext.lookup_table',false);
    	form.showField('question_ext.lookup_field',false);
    	form.showField('question_ext.unit_type',false);
    	form.showField('question_ext.unit_default',false);
    	form.showField('question_ext.activity_type',false);    	
    	
    	switch (questionType) {
	        case ("multiple_choice_single","multiple_choice_multi","count","date","time","document","label"):	        		        	
	            break;	        
	            
	        case "freeform":
	        	form.showField('question_ext.freeform_width',true);	        	
	        	break;
	        		        	
	        case "measurement":
	        	form.showField('question_ext.unit_type',true);
	        	form.showField('question_ext.unit_default',true);	        	
	        	break;
	        	
	        case "lookup":	        	
	        	form.showField('question_ext.lookup_table',true);
	        	form.showField('question_ext.lookup_field',true);	        	
	        	break;

	        case "action":	        	
	        	form.showField('question_ext.activity_type',true);	        		        	
	        	break;
    	}


   		form.showField('question_ext.parent_question_text',isChildQuestion);
   		form.showField('question_ext.parent_answer_condition',isChildQuestion);    		    	
    	
    },
    
	/**
	 * This event handler updates calculated form field values for current user name and date before saving a record.
	 * abCommonQuestExtDefManageQuestionsController_beforeSave
	 */
	questionnairesForm_beforeSave : function() {
		var form = this.questionnairesForm;		
		var ds = this.questionsDataSource;
		
		//Set Updated By User Name (user_name) value to current user's ID
		form.setFieldValue('question_ext.edited_by', this.view.user.name);
		
		//Set Date and Time Last Updated to current date and time 		
		var currentDateObj = new Date();
		var currentDate = ds.formatValue('question_ext.date_last_edit', currentDateObj, false);
		var currentTime = ds.formatValue('question_ext.time_last_edit', currentDateObj, false);
		form.setFieldValue('question_ext.date_last_edit', currentDate);
		form.setFieldValue('question_ext.time_last_edit', currentTime);

	},
	
	/**
	 * This event handler adjusts visibility of some fields according to hierarchical relationship
	 */
	questionnairesForm_beforeRefresh : function() {
		var parentNode = this.getParentNode(this.questionsHierarchyTreePanel, this.questionnairesForm, true);
		var isChildQuestion = (parentNode.depth > 0);  
        this.setFormFieldsToDisplay(this.questionnairesForm.getFieldValue('question_ext.question_type'),isChildQuestion);
	},
	
	
// Methods below this line are based on those written for ab-ac-edit.js
	
	
	/**
	 * get the parent node depending on the action done on the tree nodes
	 * 
	 */
	getParentNode: function(treePanel, detailsPanel, isNewRecord){
		var currentNode = treePanel.lastNodeClicked;
		if(isNewRecord){
			if(currentNode!=null){
				return treePanel.lastNodeClicked;
			}else{
				return treePanel.treeView.getRoot();
			}
			
		}else{
			var oldFieldValue = detailsPanel.getOldFieldValues()["question_ext.question_id"];
			if(currentNode != null){
				for(var i=0;i<currentNode.children.length;i++){
					var tmpNode = currentNode.children[i];
					if(tmpNode.data["question_ext.question_id"] == oldFieldValue){
						currentNode = tmpNode;
						break;
					}
				}
				return currentNode.parent;
			}else{
				return treePanel.treeView.getRoot();
			}
		}
		
    },
	
	/**
	 *  refresh tree based on parent node, expand the child 
	 *  in case the action is done on it
	 */
	refreshTree: function(parentNode){
		var treePanel = this.questionsHierarchyTreePanel;
		if(parentNode.isRoot()){
			treePanel.refresh();
		}else{
			treePanel.expandNode(parentNode);
		}
		
	},
	
	/**
	 * refresh tree panel and set the details panel invisible
	 */
	
	questionsHierarchyTreePanel_onRefresh: function(){
        this.questionsHierarchyTreePanel.refresh();
        this.questionnairesForm.show(false);
	},
	
	
	/**
	 * Action when pressing Filter button
	 * added custom restrictions for database  
	 * Each filter option has also multiple selection options
	 * Check hierarchyIds of records for both parent and children
	 */	
	questionnairesFilter_onFilter: function(){
		var sqlRestriction = "";
		var value = "";

		var filterConsole = this.questionnairesFilter;
		
		if (valueExistsNotEmpty(filterConsole.getFieldValue('questionnaire_ext.questionnaire_id'))){
			value = filterConsole.getFieldValue('questionnaire_ext.questionnaire_id');
			sqlRestriction += (valueExistsNotEmpty(sqlRestriction) ? " AND " : "") + " questionnaire_ext.questionnaire_id="+ value +" ";
		}
		if (valueExistsNotEmpty(filterConsole.getFieldValue('questionnaire_ext.questionnaire_title'))){
			value = filterConsole.getFieldValue('questionnaire_ext.questionnaire_title');
			sqlRestriction += (valueExistsNotEmpty(sqlRestriction) ? " AND " : "") + " questionnaire_ext.questionnaire_title LIKE '%"+ value +"%' ";
		}
		alert(sqlRestriction);
		
		this.questionnairesForm.show(false, true);
	
	}
		
});

/**
 * Take selected questionnaire ID and tittle, return and close the pop-up dialog
 */
function selectAndClose(ctx){
	var form = ctx.command.getParentPanel();
	if(form){
		var questionnaire_id = form.getFieldValue("questionnaire_ext.questionnaire_id");
		var questionnaire_title = form.getFieldValue("questionnaire_ext.questionnaire_title");
		if(View.parameters.callback){
		   View.parameters.callback(questionnaire_id,questionnaire_title);
		}
		View.closeThisDialog();
	}
}