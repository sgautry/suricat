var abCompViolationFormController = View.createController('abCompViolationFormController',
{
	
	isVATAndMCEnabled : null,
	formId: null,
	
	afterViewLoad : function(){
		this.isVATAndMCEnabled = View.activityParameters['AbCommonResources-EnableVatAndMultiCurrency'] == 1;
		if(this.isVATAndMCEnabled){
			this.formId = "abCompViolationFormVat";
			View.panels.get(this.formId).addEventListener("onAutoCompleteSelect", onAutoCompleteSelect);
		}else{
			this.formId = "abCompViolationForm";
		}
		
	},
	
	
	afterInitialDataFetch : function(){
		this.topCtrl = View.getOpenerView().controllers.get(0);
		this.refreshFromFirstTab(this.topCtrl.defineRestricion, this.topCtrl.defineNewRecord);
	},

	
	/**
	* Event Handler of action "Cancel".
	*/
	abCompViolationForm_onCancel :function(){

		// get top controller
		if(!this.topCtrl){
			this.topCtrl = View.getOpenerView().controllers.get(0);
		}

		//Call function of top controller to select first tab
		this.topCtrl.selectFirstTab();
	},
	
	abCompViolationFormVat_onCancel :function(){

		// get top controller
		if(!this.topCtrl){
			this.topCtrl = View.getOpenerView().controllers.get(0);
		}

		//Call function of top controller to select first tab
		this.topCtrl.selectFirstTab();
	},


	/**
	* Event Handler of action "Save and Add New"
	*/
	abCompViolationForm_onSaveAndAddNew : function(){

		this.abCompViolationForm_onSave();
		this.abCompViolationForm.refresh(null, true);

		//save location informations and new location id
		setLocationForm(this.abCompViolationForm2, "", true);
	},
	
	abCompViolationFormVat_onSaveAndAddNew : function(){

		this.abCompViolationFormVat_onSave();
		this.abCompViolationFormVat.refresh(null, true);

		//save location informations and new location id
		setLocationForm(this.abCompViolationForm2, "", true);
	},

	/**
	* Event Handler of action "Save"
	*/
	abCompViolationForm_onSave : function(){
		if(this.abCompViolationForm.canSave()){
			//check if vialation_id is existed
			var innerThis = this;
			if(this.violationIDExists()){
				// confirm if continue save or not.
				View.confirm(getMessage('existedViolationID'), function(button){
					if (button == 'yes') {
						innerThis.createNewViolation(false);
					}  
				});
			}  
			else {
				this.createNewViolation(false);
			}
		}
	},
	
	abCompViolationFormVat_onSave : function(){
		if(this.abCompViolationFormVat.canSave()){
			//check if vialation_id is existed
			var innerThis = this;
			if(this.violationIDExists()){
				// confirm if continue save or not.
				View.confirm(getMessage('existedViolationID'), function(button){
					if (button == 'yes') {
						innerThis.createNewViolation(true);
					}  
				});
			}  
			else {
				this.createNewViolation(true);
			}
		}
	},

	/**
	 * Privation Function :  create new violation record.
	 */
	createNewViolation:function(hasVat){
		//call common JS function to save location informations
		createOrUpdateLocation(View.panels.get(this.formId), this.abCompViolationForm2, "regviolation");

		if(hasVat){
			if (calculateComplianceCosts('abCompViolationFormVat', 'regviolation', 'chk_vat_amount')
					&& View.panels.get(this.formId).save()) {
				// we must convert exchange rate costs
				try{
					// get new cost id 
					var violationId = this.abCompViolationFormVat.getFieldValue("regviolation.violation_num");
					// call convert cost WFR
					var result = Workflow.callMethod('AbCommonResources-CostService-convertViolationCostForVATAndMC', [parseInt(violationId)]);
				}catch(e){
					Workflow.handleError(e);
					return false;
				}
			}
		}else{	
			View.panels.get(this.formId).save();
		}

		// get top controller
		if(!this.topCtrl){
			this.topCtrl = View.getOpenerView().controllers.get(0);
		}
		//set changed sign in top controller
		if(this.topCtrl){
			this.topCtrl.onSaveChange();
			this.topCtrl.violation = View.panels.get(this.formId).getFieldValue("regviolation.violation_num") ;
		}
	},

	/**
	 * Privation Function :  check if violation ID exists in database
	 */
	violationIDExists:function(){
		var form = View.panels.get(this.formId);
		var record = form.getOutboundRecord();	 
		var values = record.values;
		var oldValues = record.oldValues;
		var isNew = 	record.isNew;

		var violationId = form.getFieldValue("regviolation.violation_id");
		var restriction = new Ab.view.Restriction();
		restriction.addClause('regviolation.violation_id' ,violationId);
		
		var records=this.abCompViolationFormDS.getRecords(restriction);
		if(records && records.length>0){
			if(isNew)	 {
				return true;
			}
			else {
				 if( values['regviolation.violation_id'] == oldValues['regviolation.violation_id'] ){
					 return false;
				 }
				 else{ 
					 return true;
				 }
			}
		}
		return false;
	},

	/**
	 * Event Handler for 'Copy As New' action button: 
	 * 	copy current loaded record, create a new record, then fill values to  the new record and empty primary keys
	 */
	abCompViolationForm_onCopyAsNew:function(){
		var violationNum=this.abCompViolationForm.getRecord().getValue("regviolation.violation_num");
		if(!violationNum){
			View.showMessage(getMessage("nullTemplate"));
			return;
		}
		var restriction = new Ab.view.Restriction();
		restriction.addClause('regviolation.violation_num' ,violationNum);
		var records=this.abCompViolationFormDS.getRecords(restriction);
		var record=records[0];
		this.abCompViolationForm.newRecord = true;
		this.abCompViolationForm.setRecord(record);
		this.abCompViolationForm.setFieldValue('regviolation.violation_num','');
	},
	
	abCompViolationFormVat_onCopyAsNew:function(){
		var violationNum=this.abCompViolationFormVat.getRecord().getValue("regviolation.violation_num");
		if(!violationNum){
			View.showMessage(getMessage("nullTemplate"));
			return;
		}
		var restriction = new Ab.view.Restriction();
		restriction.addClause('regviolation.violation_num' ,violationNum);
		var records=this.abCompViolationFormDS.getRecords(restriction);
		var record=records[0];
		this.abCompViolationFormVat.newRecord = true;
		this.abCompViolationFormVat.setRecord(record);
		this.abCompViolationFormVat.setFieldValue('regviolation.violation_num','');
	},

	/**
	 * Event Handler for 'Delete' action button: 
	*/
	abCompViolationForm_onDelete: function(){

		this.abCompViolationForm.deleteRecord();
		//call deleteLocation WFR to Delete compliance_location record.
		if(this.abCompViolationForm.getFieldValue("regviolation.location_id")){
			deleteLocation(this.abCompViolationForm.getFieldValue("regviolation.location_id"),0);
		}
		
		// get top controller
		if(!this.topCtrl){
			this.topCtrl = View.getOpenerView().controllers.get(0);
		}
		if(this.topCtrl){
			this.topCtrl.needRefreshSelectList = true;
			this.topCtrl.selectFirstTab();
		}
	},
	
	abCompViolationFormVat_onDelete: function(){

		this.abCompViolationFormVat.deleteRecord();
		//call deleteLocation WFR to Delete compliance_location record.
		if(this.abCompViolationFormVat.getFieldValue("regviolation.location_id")){
			deleteLocation(this.abCompViolationFormVat.getFieldValue("regviolation.location_id"),0);
		}
		
		// get top controller
		if(!this.topCtrl){
			this.topCtrl = View.getOpenerView().controllers.get(0);
		}
		if(this.topCtrl){
			this.topCtrl.needRefreshSelectList = true;
			this.topCtrl.selectFirstTab();
		}
	},
	
	refreshFromFirstTab : function(res,newRec){
		if(this.isVATAndMCEnabled){
			this.abCompViolationFormVat.refresh(res,newRec);
			this.abCompViolationForm.show(false);
		}else{
			this.abCompViolationForm.refresh(res,newRec);
			this.abCompViolationFormVat.show(false);
		}
	},

	/**
	 * Event Handler of afterRefresh of Violation form
	 */
	abCompViolationForm_afterRefresh: function(){
		var form = this.abCompViolationForm;

		//if not report mode and editing an existed violation record
		if (!form.newRecord) {
			//set location form properly
			var locId = form.getFieldValue("regviolation.location_id");
			setLocationForm(this.abCompViolationForm2, locId);
		} 
		//if creating a new violation record
		else {
			//set location form properly
			setLocationForm(this.abCompViolationForm2, "");
		}
		
		//Requirement Type should be blank if Requirement Code is blank
		if(form.newRecord || !form.getFieldValue("regrequirement.reg_requirement")){
			form.showField("regrequirement.regreq_type", false);
		} else {
			form.showField("regrequirement.regreq_type", true);			
		}

		//disable location_id field but enable its select-valut button
		form.enableField("regviolation.location_id", false);
		form.enableFieldActions("regviolation.location_id", true);
	},
	
	abCompViolationFormVat_afterRefresh: function(){
		var form = View.panels.get(this.formId);

		//if not report mode and editing an existed violation record
		if (!form.newRecord) {
			//set location form properly
			var locId = form.getFieldValue("regviolation.location_id");
			setLocationForm(this.abCompViolationForm2, locId);
		} 
		//if creating a new violation record
		else {
			//set location form properly
			setLocationForm(this.abCompViolationForm2, "");
		}
		
		//Requirement Type should be blank if Requirement Code is blank
		if(form.newRecord || !form.getFieldValue("regrequirement.reg_requirement")){
			form.showField("regrequirement.regreq_type", false);
		} else {
			form.showField("regrequirement.regreq_type", true);			
		}

		//disable location_id field but enable its select-valut button
		form.enableField("regviolation.location_id", false);
		form.enableFieldActions("regviolation.location_id", true);
		
		afterSelectListenerCostCateg = 'afterSelectCostCategory';
		afterSelectListenerCtryId = 'afterSelectCountryId';
		if(form.newRecord || !valueExistsNotEmpty(form.getFieldValue('regviolation.currency_payment'))){
			form.setFieldValue("regviolation.ctry_id", View.user.country);
			form.setFieldValue("regviolation.currency_payment", View.user.userCurrency.code);
		}
		setOverrideField("chk_vat_percent", this.formId, "regviolation.vat_percent_override", 0);
		setOverrideField("chk_vat_amount", this.formId, "regviolation.vat_amount_override", -1);
		setOverrideField("chk_exchange_rate", this.formId, "regviolation.exchange_rate_override", 1);
		
		var fieldCtry = form.fields.get('regviolation.ctry_id');
		if (valueExists(fieldCtry)) {
			var command = fieldCtry.actions.items[0].command.commands[0];
			if(valueExistsNotEmpty(afterSelectListenerCtryId)){
				command.actionListener = afterSelectListenerCtryId;
			}
		}
		
		var fieldCostCat = form.fields.get('regviolation.cost_cat_id');
		if (valueExists(fieldCostCat)) {
			var command = fieldCostCat.actions.items[0].command.commands[0];
			if(valueExistsNotEmpty(afterSelectListenerCostCateg)){
				command.actionListener = afterSelectListenerCostCateg;
			}
		}
	}
});

/**
 * Action Listener of select-value command of field "location_id"
 */
function afterSelectLocationID(fieldName, selectedValue, previousValue){
	//only monitor the field "regviolation.location_id"
	if ( "regviolation.location_id" ==fieldName) {

		//set location form properly
		var ctrl = abCompViolationFormController;
		setLocationForm(ctrl.abCompViolationForm2, selectedValue);
	}
}

/**
* Action Lisenter of custom select value command for field "Requirement"
*/
function afterSelectRequirement(fieldName, selectedValue, previousValue){
	var controller = View.controllers.get("abCompViolationFormController");
	//when select a requirement show its requirement type in form
	if (fieldName == "regviolation.reg_requirement"){
		if(selectedValue){
			onChangeRequirement("regviolation", controller.formId ,selectedValue);
		}
	}
}

/**
 * After Select value event handler.
 * 
 * @param fieldName
 * @param selectedValue
 * @param previousValue
 */
function afterSelectCostCategory(fieldName, selectedValue, previousValue){
	var objForm = View.panels.get('abCompViolationFormVat');
	objForm.setFieldValue(fieldName, selectedValue);
	// get VAT percent for selected country and cost category
	if(getVATPercent(objForm.id, 'regviolation.ctry_id', 'regviolation.cost_cat_id', '' , 'regviolation.vat_percent_value')){
		// reset override vat percent field
		resetOverrideField("chk_vat_percent", 'abCompViolationFormVat', "regviolation.vat_percent_override", 0);
		// reset override vat amount field
		resetOverrideField("chk_vat_amount", 'abCompViolationFormVat', "regviolation.vat_amount_override", -1);
		// calculate costs
		calculateComplianceCosts('abCompViolationFormVat', 'regviolation', 'chk_vat_amount');
	}
}

/**
 * After select cost category event handler
 * @param fieldName
 * @param selectedValue
 * @param previousValue
 */
function afterSelectCountryId(fieldName, selectedValue, previousValue){
	var objForm = View.panels.get('abCompViolationFormVat');
	objForm.setFieldValue(fieldName, selectedValue);
	// get VAT percent for selected country and cost category
	if(getVATPercent(objForm.id, 'regviolation.ctry_id', 'regviolation.cost_cat_id', '' , 'regviolation.vat_percent_value')){
		// reset override vat percent field
		resetOverrideField("chk_vat_percent", 'abCompViolationFormVat', "regviolation.vat_percent_override", 0);
		// reset override vat amount field
		resetOverrideField("chk_vat_amount", 'abCompViolationFormVat', "regviolation.vat_amount_override", -1);
		// calculate costs
		calculateComplianceCosts('abCompViolationFormVat', 'regviolation', 'chk_vat_amount');
	}
}

/**
 * Calculate cost values.
 * 
 * @param formId
 * @param costTable
 * @param chkVatAmount
 * @returns {Boolean}
 */
function calculateComplianceCosts(formId, costTable, chkVatAmount){
	var objForm = View.panels.get(formId);
	var dataSource = objForm.getDataSource();
	
	var costCommBase = objForm.getFieldValue(costTable + ".cost_base_payment");
	
	
	if (valueExistsNotEmpty(costCommBase)) {
		// we can calculate some values
		
		var isVatAmountOverride = document.getElementById(chkVatAmount).checked;
		if (isVatAmountOverride) {
			removeFieldError(objForm, costTable + ".vat_percent_value");
		}
		
		// get cost values
		var costValues = {
				"cost_base_payment": costCommBase,
				"vat_amount_override": objForm.getFieldValue(costTable + ".vat_amount_override"),
				"vat_percent_value": objForm.getFieldValue(costTable + ".vat_percent_value"),
				"cost_vat_payment": objForm.getFieldValue(costTable + ".cost_vat_payment"),
				"cost_total_payment": objForm.getFieldValue(costTable + ".cost_total_payment")
		}
		
		try {
			var result = Workflow.callMethod('AbCommonResources-CostService-calculateViolationCostRuntimeValues', costTable, costValues, isVatAmountOverride);
			var calculatedValues = result.data;
			
			if (valueExistsNotEmpty(calculatedValues["displayValues"]) && calculatedValues["displayValues"] == "0") {
				return false;
			}
			
			// set new values
			if (valueExistsNotEmpty(calculatedValues["cost_vat_payment"])) {
				var parsedValue = new Number(dataSource.parseValue(costTable + ".cost_vat_payment", calculatedValues["cost_vat_payment"], false));
				objForm.setFieldValue(costTable + ".cost_vat_payment", dataSource.formatValue(costTable + ".cost_vat_payment", parsedValue, true));
			}
			if (valueExistsNotEmpty(calculatedValues["cost_total_payment"])) {
				var parsedValue =  new Number(dataSource.parseValue(costTable + ".cost_total_payment", calculatedValues["cost_total_payment"], false));
				objForm.setFieldValue(costTable + ".cost_total_payment", dataSource.formatValue(costTable + ".cost_total_payment", parsedValue, true));
			}
			
		} catch (e){
			Workflow.handleError(e);
		}
	}
	return true;
}

/**
 * Enable / disable override field based on checkbox selection
 */
function onSelect_Override(chkId, formId, fieldId, defaultValue){
	var objCheckbox = document.getElementById(chkId);
	var objForm = View.panels.get(formId);
	if(objCheckbox){
		objForm.enableField(fieldId, objCheckbox.checked);
		var dataSource = objForm.getDataSource();
		if(objCheckbox.checked){
			if(chkId == "chk_vat_percent"){
				document.getElementById("chk_vat_amount").checked = false;
				objForm.setFieldValue("regviolation.vat_amount_override", dataSource.formatValue("regviolation.vat_amount_override", -1, true));
				objForm.enableField("regviolation.vat_amount_override", false);
			}else if(chkId == "chk_vat_amount"){
				document.getElementById("chk_vat_percent").checked = false;
				objForm.setFieldValue("regviolation.vat_percent_override", dataSource.formatValue("regviolation.vat_percent_override", 0, true));
				objForm.enableField("regviolation.vat_percent_override", false);
				getVATPercent('abCompViolationFormVat', 'regviolation.ctry_id', 'regviolation.cost_cat_id', '' , 'regviolation.vat_percent_value', false);
			}
		}else{
			// we must reset field value
			if(valueExists(defaultValue)){
				var currentValue = objForm.getFieldValue(fieldId);
				if(parseFloat(currentValue) != defaultValue){
					objForm.setFieldValue(fieldId, dataSource.formatValue(fieldId, defaultValue, true));
					// get default VAT percent
					if(chkId == "chk_vat_percent"){
						getVATPercent('abCompViolationFormVat', 'regviolation.ctry_id', 'regviolation.cost_cat_id', '' , 'regviolation.vat_percent_value', false);
					}
				}
			}
		}
	}
} 

function onAutoCompleteSelect(form, fieldName, selectedValue){
	if (fieldName == "regviolation.cost_cat_id" || fieldName == "regviolation.ctry_id") {
		if(getVATPercent(form.id, 'regviolation.ctry_id', 'regviolation.cost_cat_id', '' , 'regviolation.vat_percent_value')){
			// reset override vat percent field
			resetOverrideField("chk_vat_percent", 'abCompViolationFormVat', "regviolation.vat_percent_override", 0);
			// reset override vat amount field
			resetOverrideField("chk_vat_amount", 'abCompViolationFormVat', "regviolation.vat_amount_override", -1);
			// calculate costs
			calculateComplianceCosts('abCompViolationFormVat', 'regviolation', 'chk_vat_amount');
		}
	}
}

