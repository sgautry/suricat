var questionnaireTabController = View.createController('questionnaireTabController', {
    questionnaire_id: 0,
    survey_event_id: 0,
    isReadOnly: false,
    //restriction of console
    consoleRes: " 1=1 ",
    //variables indicates current view is manage view or report view
    mode: 'report',
    mainController: null,
    //restriction from select  grid in first tab
    selectRes: " 1=1 ",
    /**
     * @inherit.
     */
    afterViewLoad: function() {
        // APP-1850: The tab contains a frame, a view, and a default layout with the center region.
        // The tab view includes another view, ab-common-quest-ext-complete.axvw.
        // That view has its own default layout with the center region.
        // The tab view erroneously displays vertical scroll bar. Suppress it.
        jQuery('#viewLayout_center_div').parent().css('overflow', 'hidden');
    },
    afterInitialDataFetch: function () {
        this.mainController = View.getOpenerView().controllers.get(0);
        this.onRefresh();
    },
    
    questionnaireView_onSave: function (showMessage) {
        this.questionnaireView_save(showMessage, false);
    },
    
    questionnaireView_save: function(showMessage, checkRequiredAnswers) {
    	var updated = false;
        var questionnaireView = View.panels.get('questionnaireView').contentView;
        if (questionnaireView) {
            var updateQuestionnaireController = questionnaireView.controllers.get('UpdateQuestionnaire');
            var questionnaire = questionnaireView.controllers.get('CompleteQuestionnaire').questionnaire;
            if (this.canSaveQuestionnaire(questionnaire)) {
                updated = updateQuestionnaireController.saveQuestionnaire(this.survey_event_id, checkRequiredAnswers);
            } else {
                var message = getMessage('noQuestionnaireDefined');
                if (questionnaire) {
                    var localizedStatus = questionnaireView.dataSources.get('questionnairesDataSource').fieldDefs.get('questionnaire_ext.status').enumValues[questionnaire.status];
                    message = String.format(getMessage('saveOnlyActiveQuestionnaire'), localizedStatus);
                }
                View.showMessage(message);
            }
            if (updated) {
                showMessage = valueExists(showMessage) ? showMessage : true;
                this.updateSurveyEventStatus('IN PROGRESS', showMessage);
            }
        }
        return updated;
    },
    
    questionnaireView_onSaveSubmit: function () {
        if (this.questionnaireView_save(false, true)) {
            var updateQuestionnaireController = View.panels.get('questionnaireView').contentView.controllers.get('UpdateQuestionnaire');
            updateQuestionnaireController.processQuestionnaire(this.questionnaire_id, this.survey_event_id, this.updateSurveyEventStatus.createDelegate(this, ['COMPLETED', true]));
        }
    },
    updateSurveyEventStatus: function (status, showMessage) {
        var questionnaireView = View.panels.get('questionnaireView').contentView;
        if (valueExists(questionnaireView)) {
            var eventsDataSource = questionnaireView.dataSources.get('eventsDataSource');
            var eventRecord = eventsDataSource.getRecord(new Ab.view.Restriction({'activity_log.activity_log_id': this.survey_event_id}));
            var oldStatus = eventRecord.getValue('activity_log.status');
            // do not set the same status twice since method is called two times on submit questionnaire.
            if ('COMPLETED' !== oldStatus && oldStatus !== status) {
                eventRecord.setValue('activity_log.status', status);
                // save event new status
                eventsDataSource.saveRecord(eventRecord);
                if (showMessage) {
                    // display message that event was updated
                    var localizedStatus  = eventsDataSource.fieldDefs.get('activity_log.status').enumValues[status];
                    View.showMessage(String.format(getMessage('eventStatusUpdated'), localizedStatus));
                }
                // refresh select event list tab
                this.mainController.needRefreshSelectList = true;
                // refresh questionnaire
                this.mainController.eventStatus = status;
                this.onRefresh();
                this.refreshDefineEventTab();

            }
        }
    },
    /**
     * Refresh define event tab to update the status field.
     */
    refreshDefineEventTab: function () {
        var defineEventTab = this.mainController.compTabs.findTab('defineEvent');
        if (defineEventTab && defineEventTab.isContentLoaded) {
            var defineEventController = defineEventTab.getContentFrame().View.controllers.get(this.mainController.tabCtrl['defineEvent']);
            defineEventController.afterInitialDataFetch();
        }
    },
    /**
     * The questionnaire forms should not allow save/submission of any questionnaire with status "Draft" or "Inactive" and only "Active".
     * @param {Object} questionnaire
     * @returns {*|boolean} true if questionnaire status is active.
     */
    canSaveQuestionnaire: function (questionnaire) {
        return valueExists(questionnaire) && 'active' === questionnaire.status;
    },
    /**
     * Private function: for report mode initialize actions, fields of form.
     *
     * @param isReport
     */
    setReportMode: function (isReport) {
    },
    /**
     * Private Function: called to refresh the grid when there are restriction condition changes
     */
    onRefresh: function () {
        if (this.mainController.event && this.mainController.questionnaireId != 0) {
            this.questionnaire_id = this.mainController.questionnaireId;
            this.survey_event_id = this.mainController.event;
            this.isReadOnly = false;
            if (this.mainController.eventStatus === 'CANCELLED' ||
                this.mainController.eventStatus === 'STOPPED' ||
                this.mainController.eventStatus === 'REJECTED' ||
                this.mainController.eventStatus === 'COMPLETED' ||
                this.mainController.eventStatus === 'COMPLETED-V' ||
                this.mainController.eventStatus === 'CLOSED') {
                this.isReadOnly = true;
            } else {
                //check if event is assigned to current employee.
                var isMyEventRecord = View.dataSources.get('abMyEventActivityLogDs').getRecord(new Ab.view.Restriction({'activity_log.activity_log_id': this.survey_event_id}));
                this.isReadOnly =_.isEmpty(isMyEventRecord.values);
            }
            loadQuestionnairePanel();
            this.questionnaireView.enableAction('save', !this.isReadOnly);
            this.questionnaireView.enableAction('saveSubmit', !this.isReadOnly);
        }
    }
});

function loadQuestionnairePanel() {
    var questionnaireViewPanel = View.panels.get('questionnaireView');
    var questionnaireView = questionnaireViewPanel.contentView;
    if (questionnaireView) {
        var controller = View.controllers.get('questionnaireTabController');
        var updateQuestionnaireController = questionnaireView.controllers.get('UpdateQuestionnaire');
        updateQuestionnaireController.loadQuestionnaireWithResults(controller.questionnaire_id, controller.isReadOnly, controller.survey_event_id);

        var parentRegion = questionnaireViewPanel.getLayoutRegion();
        var parentHeight = Ext.get(parentRegion.contentEl).parent().getHeight();
        questionnaireViewPanel.frame.setHeight(parentHeight - 40);
    } else {
        setTimeout(loadQuestionnairePanel, 500);
    }
}

