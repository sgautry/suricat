
var commCostController = View.createController('commCostController',{	
		//restriction from select  grid in first tab
		selectRes: " 1=1 ", 
		mainController:null,
		location_id: null,
		regrequirement: null,
		regprogram: null,
		regulation: null,
		isVATAndMCEnabled : null,
		formId: null,
		newRecord: false,
		firstTime: true,
		
		/**
		 * a common grid show the north, for 'regulation' , 'program' or 'requirement'.
		 */
		grid: null,		
		events : {
			/**
			 * Event handler for check 24 hours service checkbox option.
			 * 
			 * @param event
			 */
			"click #wholeDayOpen" :  function(event) {
				this.setWholeDayOpen(event.target.checked);
			},
			
			"click #wholeDayOpenVat" :  function(event) {
				this.setWholeDayOpen(event.target.checked);
			}
		},
		
		afterViewLoad: function(){

			// check if multi currency is enabled
			this.isVATAndMCEnabled = View.activityParameters['AbCommonResources-EnableVatAndMultiCurrency'] == 1;
			if(this.isVATAndMCEnabled){
				this.formId = "complianceProgramCostsVat_form";
			}else{
				this.formId = "complianceProgramCosts_form";
			}
			
			View.panels.get(this.formId).addEventListener("onAutoCompleteSelect", onAutoCompleteSelect);
		},
		
	    afterInitialDataFetch: function(){
	    	var layoutManager = View.getLayoutManager('main');
	    	this.complianceProgramCosts_form.show(false);
	    	this.complianceProgramCostsVat_form.show(false);
    		layoutManager.expandRegion('north');
    		this.setRestriction();
    		this.setCustomRestriction();
	    	if(this.grid==null){
	    		this.grid=this.complianceProgramCosts_grid;
	    	}
	    	//this.hideFields();
	    	this.grid.refresh(this.selectRes);
	    	if(this.location_id){  	
	    		this.grid.setTitle(getMessage('messageLocGrid'));
	    	}else if(this.regrequirement){   
	    		this.grid.setTitle(getMessage('messageReqGrid'))
	    	}
	    	else if(this.regprogram){
	    		this.grid.setTitle(getMessage('messageProgGrid'))
	    	}
	    },
	    
	    setRestriction : function(){	    	
	    	this.mainController=View.getOpenerView().controllers.get(0);	
	    	if(this.mainController.location_id){  	
	    		this.location_id = this.mainController.location_id;
	    	}
	    	if(this.mainController.regrequirement){   
	    		this.regrequirement = this.mainController.regrequirement;
	    	}
	    	if(this.mainController.regprogram){
	    		this.regprogram = this.mainController.regprogram;
	    	}
	    	if(this.mainController.regulation){
	    		this.regulation = this.mainController.regulation;
	    	}
	    },
	    
	    hideFields : function(){
	    	if(this.location_id){  	
	    	}else if(this.regrequirement){   
	    		this.grid.showColumn("compliance_contract_cost.location_id", false);
	    	}
	    	else if(this.regprogram){
	    		this.grid.showColumn("compliance_contract_cost.reg_requirement", false);
	    		this.grid.showColumn("compliance_contract_cost.location_id", false);
	    	}
	    },
	 
	       
	    complianceProgramCostsVat_form_afterRefresh : function(){
	    	
	    	var objForm = this.complianceProgramCostsVat_form;
	    	
	    	afterSelectListenerCostCateg = 'afterSelectCostCategory';
			afterSelectListenerCtryId = 'afterSelectCountryId';
			if(objForm.isNewRecord){
				objForm.setFieldValue("cost_tran_recur.ctry_id", View.user.country);
				objForm.setFieldValue("cost_tran_recur.currency_payment", View.user.userCurrency.code);
			}
			setOverrideField("chk_vat_percent", this.formId, "compliance_contract_cost.vat_percent_override", 0);
			setOverrideField("chk_vat_amount", this.formId, "compliance_contract_cost.vat_amount_override", -1);
			setOverrideField("chk_exchange_rate", this.formId, "compliance_contract_cost.exchange_rate_override", 1);
			
			var fieldCtry = objForm.fields.get('compliance_contract_cost.ctry_id');
			if (valueExists(fieldCtry)) {
				var command = fieldCtry.actions.items[0].command.commands[0];
				if(valueExistsNotEmpty(afterSelectListenerCtryId)){
					command.actionListener = afterSelectListenerCtryId;
				}
			}
			
			var fieldCostCat = objForm.fields.get('compliance_contract_cost.cost_cat_id');
			if (valueExists(fieldCostCat)) {
				var command = fieldCostCat.actions.items[0].command.commands[0];
				if(valueExistsNotEmpty(afterSelectListenerCostCateg)){
					command.actionListener = afterSelectListenerCostCateg;
				}
			}
	    	
			if(this.firstTime){
				if(!this.newRecord){
		    		var days = this.complianceProgramCostsVat_form.getRecord().getValue('compliance_contract_cost.serv_window_days').split(",", 7);
		    		for ( var i = 0; i < 7; i++) {
		    			var check = $("daysVat" + i);
		    			if (days[i] == 1) {
		    				check.checked = true;
		    			}else{
		    				check.checked = false;
		    			}
		    		}
		    		var months = this.complianceProgramCostsVat_form.getRecord().getValue('compliance_contract_cost.serv_window_months').split(",", 12);
		    		for ( var i = 0; i < 12; i++) {
		    			var check = $("monthVat" + i);
		    			if (months[i] == 1) {
		    				check.checked = true;
		    			}else{
		    				check.checked = false;
		    			}
		    		}
		    		
		    	}
				else{
					for ( var i = 0; i < 7; i++) {
		    			var check = $("daysVat" + i);
		    			check.checked = false;
		    		}
		    		for (var i=0; i<12; i++){
		    			var check = $("monthVat" + i);
		    			check.checked = false;
		    		}
				}
			this.firstTime = false;
			}
	    	if(this.location_id){  	
	    		this.complianceProgramCostsVat_form.setTitle(getMessage('messageLocForm'));
	    	}else if(this.regrequirement){   
	    		this.complianceProgramCostsVat_form.setTitle(getMessage('messageReqForm'))
	    		this.complianceProgramCostsVat_form.showField("compliance_contract_cost.location_id", false);
	    	}
	    	else if(this.regprogram){
	    		this.complianceProgramCostsVat_form.setTitle(getMessage('messageProgForm'))
	    		this.complianceProgramCostsVat_form.showField("compliance_contract_cost.reg_requirement", false);
	    		this.complianceProgramCostsVat_form.showField("compliance_contract_cost.location_id", false);
	    	}
			jQuery('#complianceProgramCostsVat_form_compliance_contract_cost\\.serv_window_start_fieldCell').each(function(){
				this.onmouseover = null;
				this.onmouseout = null;
			});
			jQuery('#complianceProgramCostsVat_form_compliance_contract_cost\\.serv_window_end_fieldCell').each(function(){
				this.onmouseover = null;
				this.onmouseout = null;
			});
			
			if(jQuery('#StoredcomplianceProgramCostsVat_form_compliance_contract_cost\\.serv_window_start').val() == jQuery('#StoredcomplianceProgramCostsVat_form_compliance_contract_cost\\.serv_window_end').val() && jQuery('#StoredcomplianceProgramCostsVat_form_compliance_contract_cost\\.serv_window_start').val() !=''){
				jQuery('#wholeDayOpenVat').get(0).checked = true;
				jQuery('#complianceProgramCostsVat_form_compliance_contract_cost\\.serv_window_end').attr('readonly', true);
			}else{
				jQuery('#wholeDayOpenVat').get(0).checked = false;
				jQuery('#complianceProgramCostsVat_form_compliance_contract_cost\\.serv_window_end').attr('readonly', false);
			}
			
			jQuery('#complianceProgramCostsVat_form_compliance_contract_cost\\.serv_window_start').change(function(){
				if(jQuery('#StoredcomplianceProgramCostsVat_form_compliance_contract_cost\\.serv_window_start').val() == jQuery('#StoredcomplianceProgramCostsVat_form_compliance_contract_cost\\.serv_window_end').val()){
					jQuery('#wholeDayOpenVat').get(0).checked = true;
					jQuery('#complianceProgramCostsVat_form_compliance_contract_cost\\.serv_window_end').attr('readonly', true);
				}else{
					jQuery('#wholeDayOpenVat').get(0).checked = false;
					jQuery('#complianceProgramCostsVat_form_compliance_contract_cost\\.serv_window_end').attr('readonly', false);
				}
			});
			jQuery('#complianceProgramCostsVat_form_compliance_contract_cost\\.serv_window_end').change(function(){
				if(jQuery('#StoredcomplianceProgramCostsVat_form_compliance_contract_cost\\.serv_window_start').val() == jQuery('#StoredcomplianceProgramCostsVat_form_compliance_contract_cost\\.serv_window_end').val()){
					jQuery('#wholeDayOpenVat').get(0).checked = true;
					jQuery('#complianceProgramCostsVat_form_compliance_contract_cost\\.serv_window_end').attr('readonly', true);
				}else{
					jQuery('#wholeDayOpenVat').get(0).checked = false;
					jQuery('#complianceProgramCostsVat_form_compliance_contract_cost\\.serv_window_end').attr('readonly', false);
				}
			});
		},
		
		complianceProgramCosts_form_afterRefresh : function(){
			if(this.firstTime){
		    	if(!this.newRecord){
		    		var days = this.complianceProgramCosts_form.getRecord().getValue('compliance_contract_cost.serv_window_days').split(",", 7);
		    		for ( var i = 0; i < 7; i++) {
		    			var check = $("days" + i);
		    			if (days[i] == 1) {
		    				check.checked = true;
		    			}else{
		    				check.checked = false;
		    			}
		    		}
		    		var months = this.complianceProgramCosts_form.getRecord().getValue('compliance_contract_cost.serv_window_months').split(",", 12);
		    		for ( var i = 0; i < 12; i++) {
		    			var check = $("month" + i);
		    			if (months[i] == 1) {
		    				check.checked = true;
		    			}else{
		    				check.checked = false;
		    			}
		    		}
		    		
		    	}
				else{
					for ( var i = 0; i < 7; i++) {
		    			var check = $("days" + i);
		    			check.checked = false;
		    		}
		    		for (var i=0; i<12; i++){
		    			var check = $("month" + i);
		    			check.checked = false;
		    		}
				}
		    this.firstTime=false;
			}
		    	if(this.location_id){  	
		    		this.complianceProgramCosts_form.setTitle(getMessage('messageLocForm'));
		    	}else if(this.regrequirement){   
		    		this.complianceProgramCosts_form.setTitle(getMessage('messageReqForm'))
		    		this.complianceProgramCosts_form.showField("compliance_contract_cost.location_id", false);
		    	}
		    	else if(this.regprogram){
		    		this.complianceProgramCosts_form.setTitle(getMessage('messageProgForm'))
		    		this.complianceProgramCosts_form.showField("compliance_contract_cost.reg_requirement", false);
		    		this.complianceProgramCosts_form.showField("compliance_contract_cost.location_id", false);
		    	}
				jQuery('#complianceProgramCosts_form_compliance_contract_cost\\.serv_window_start_fieldCell').each(function(){
					this.onmouseover = null;
					this.onmouseout = null;
				});
				jQuery('#complianceProgramCosts_form_compliance_contract_cost\\.serv_window_end_fieldCell').each(function(){
					this.onmouseover = null;
					this.onmouseout = null;
				});
				
				if(jQuery('#StoredcomplianceProgramCosts_form_compliance_contract_cost\\.serv_window_start').val() == jQuery('#StoredcomplianceProgramCosts_form_compliance_contract_cost\\.serv_window_end').val() && jQuery('#StoredcomplianceProgramCosts_form_compliance_contract_cost\\.serv_window_start').val() !=''){
					jQuery('#wholeDayOpen').get(0).checked = true;
					jQuery('#complianceProgramCosts_form_compliance_contract_cost\\.serv_window_end').attr('readonly', true);
				}else{
					jQuery('#wholeDayOpen').get(0).checked = false;
					jQuery('#complianceProgramCosts_form_compliance_contract_cost\\.serv_window_end').attr('readonly', false);
				}
				
				jQuery('#complianceProgramCosts_form_compliance_contract_cost\\.serv_window_start').change(function(){
					if(jQuery('#StoredcomplianceProgramCosts_form_compliance_contract_cost\\.serv_window_start').val() == jQuery('#StoredcomplianceProgramCosts_form_compliance_contract_cost\\.serv_window_end').val()){
						jQuery('#wholeDayOpen').get(0).checked = true;
						jQuery('#complianceProgramCosts_form_compliance_contract_cost\\.serv_window_end').attr('readonly', true);
					}else{
						jQuery('#wholeDayOpen').get(0).checked = false;
						jQuery('#complianceProgramCosts_form_compliance_contract_cost\\.serv_window_end').attr('readonly', false);
					}
				});
				jQuery('#complianceProgramCosts_form_compliance_contract_cost\\.serv_window_end').change(function(){
					if(jQuery('#StoredcomplianceProgramCosts_form_compliance_contract_cost\\.serv_window_start').val() == jQuery('#StoredcomplianceProgramCosts_form_compliance_contract_cost\\.serv_window_end').val()){
						jQuery('#wholeDayOpen').get(0).checked = true;
						jQuery('#complianceProgramCosts_form_compliance_contract_cost\\.serv_window_end').attr('readonly', true);
					}else{
						jQuery('#wholeDayOpen').get(0).checked = false;
						jQuery('#complianceProgramCosts_form_compliance_contract_cost\\.serv_window_end').attr('readonly', false);
					}
				});
			},
	    /**
	     * call after afterInitialDataFetch.
	     */
	    setCustomRestriction: function(){
	    	this.grid = this.complianceProgramCosts_grid;

	    	if(this.location_id){   
	    		this.selectRes = "compliance_contract_cost.location_id='"+this.location_id+"' and compliance_contract_cost.reg_program='"+this.regprogram+"' and compliance_contract_cost.regulation='"+this.regulation+"' ";
	    		if(this.regrequirement){
	    			this.selectRes += "and compliance_contract_cost.reg_requirement='"+this.regrequirement +"'";
	    		}
	    	}
	    	else if(this.regrequirement){
	    		this.selectRes = "compliance_contract_cost.location_id IS NULL and compliance_contract_cost.reg_requirement='"+this.regrequirement+"' and compliance_contract_cost.reg_program='"+this.regprogram+"' and compliance_contract_cost.regulation='"+this.regulation+"' ";
	    	}
	    	else if(this.regprogram){
	    		this.selectRes = "compliance_contract_cost.reg_requirement IS NULL and compliance_contract_cost.location_id IS NULL and compliance_contract_cost.reg_program='"+this.regprogram+"' and compliance_contract_cost.regulation='"+this.regulation+"' ";
	    	}
			this.grid.addParameter("regRequireRes", this.selectRes);
	    },

//		/**
//		 * when add new button click.
//		 */
		invokeAddNew: function(){
			View.panels.get(this.formId).newRecord = true;
			View.panels.get(this.formId).refresh();
			View.panels.get(this.formId).setFieldValue("compliance_contract_cost.regulation",this.regulation);
			View.panels.get(this.formId).setFieldValue("compliance_contract_cost.reg_program",this.regprogram);
		    if(this.regrequirement){
		    	View.panels.get(this.formId).setFieldValue("compliance_contract_cost.reg_requirement",this.regrequirement);
		    }
		    if(this.location_id){
		    	View.panels.get(this.formId).setFieldValue("compliance_contract_cost.location_id",this.location_id);
		    }
		
		},


		
	    complianceProgramCosts_form_onSave: function(){
			 // set service window days
			var servWindoDays = '';
			for ( var i = 0; i < 7; i++) {
				if ($("days" + i).checked) {
					servWindoDays += ',1'
				} else {
					servWindoDays += ',0'
				}
			}
			var servWindoMonths = '';
			for ( var i = 0; i < 12; i++) {
				if ($("month" + i).checked) {
					servWindoMonths += ',1'
				} else {
					servWindoMonths += ',0'
				}
			}
			View.panels.get(this.formId).setFieldValue("compliance_contract_cost.serv_window_days", servWindoDays.substring(1));
			View.panels.get(this.formId).setFieldValue("compliance_contract_cost.serv_window_months", servWindoMonths.substring(1));
			if(jQuery('#StoredcomplianceProgramCosts_form_compliance_contract_cost\\.serv_window_start').val() == ''){
				View.panels.get(this.formId).setFieldValue('compliance_contract_cost.serv_window_start',this.complianceContractCosts_ds.parseValue('compliance_contract_cost.serv_window_start', "09:00.00.000", false).toString());
				View.panels.get(this.formId).setFieldValue('compliance_contract_cost.serv_window_end',this.complianceContractCosts_ds.parseValue('compliance_contract_cost.serv_window_end', "17:00.00.000", false).toString());
			}
			View.panels.get(this.formId).save();
			
		},
		
		complianceProgramCostsVat_form_onSave: function(){
			 // set service window days
			var servWindoDays = '';
			for ( var i = 0; i < 7; i++) {
				if ($("daysVat" + i).checked) {
					servWindoDays += ',1'
				} else {
					servWindoDays += ',0'
				}
			}
			var servWindoMonths = '';
			for ( var i = 0; i < 12; i++) {
				if ($("monthVat" + i).checked) {
					servWindoMonths += ',1'
				} else {
					servWindoMonths += ',0'
				}
			}
			View.panels.get(this.formId).setFieldValue("compliance_contract_cost.serv_window_days", servWindoDays.substring(1));
			View.panels.get(this.formId).setFieldValue("compliance_contract_cost.serv_window_months", servWindoMonths.substring(1));
			if(jQuery('#StoredcomplianceProgramCostsVat_form_compliance_contract_cost\\.serv_window_start').val() == ''){
				View.panels.get(this.formId).setFieldValue('compliance_contract_cost.serv_window_start',this.complianceContractCosts_ds.parseValue('compliance_contract_cost.serv_window_start', "09:00.00.000", false).toString());
				View.panels.get(this.formId).setFieldValue('compliance_contract_cost.serv_window_end',this.complianceContractCosts_ds.parseValue('compliance_contract_cost.serv_window_end', "17:00.00.000", false).toString());
			}
			
			
			if (calculateComplianceCosts('complianceProgramCostsVat_form', 'compliance_contract_cost', 'chk_vat_amount')
					&& View.panels.get(this.formId).save()) {
				// we must convert exchange rate costs
				try{
					// get new cost id 
					var costId = this.complianceProgramCostsVat_form.getFieldValue("compliance_contract_cost.cost_code");
					// call convert cost WFR
					var result = Workflow.callMethod('AbCommonResources-CostService-convertComplianceCostForVATAndMC', [parseInt(costId)]);
					this.complianceProgramCosts_grid.refresh();
				}catch(e){
					Workflow.handleError(e);
					return false;
				}
			}
			
		},
		
		/**
		 * Set whole day open
		 */
		setWholeDayOpen : function(checked) {
			var vat = (this.isVATAndMCEnabled)? 'Vat' : '';
			if(checked){
				// set service window start time and end time 
				if(jQuery('#StoredcomplianceProgramCosts'+vat+'_form_compliance_contract_cost\\.serv_window_start').val() == ''){
					View.panels.get(this.formId).record.setValue('compliance_contract_cost.serv_window_start',this.complianceContractCosts_ds.parseValue('compliance_contract_cost.serv_window_start', "00:00.00.000", false));
					View.panels.get(this.formId).record.setValue('compliance_contract_cost.serv_window_end',this.complianceContractCosts_ds.parseValue('compliance_contract_cost.serv_window_end', "00:00.00.000", false));
				}else{
					View.panels.get(this.formId).record.setValue('compliance_contract_cost.serv_window_start',this.complianceContractCosts_ds.parseValue('compliance_contract_cost.serv_window_end',
							jQuery('#StoredcomplianceProgramCosts'+vat+'_form_compliance_contract_cost\\.serv_window_start').val(), false));
					View.panels.get(this.formId).record.setValue('compliance_contract_cost.serv_window_end',this.complianceContractCosts_ds.parseValue('compliance_contract_cost.serv_window_end',
							jQuery('#StoredcomplianceProgramCosts'+vat+'_form_compliance_contract_cost\\.serv_window_start').val(), false));
				}
				
				View.panels.get(this.formId).fields.get('compliance_contract_cost.serv_window_start').syncToUI();
				View.panels.get(this.formId).fields.get('compliance_contract_cost.serv_window_end').syncToUI();
				
				jQuery('#complianceProgramCosts'+vat+'_form_compliance_contract_cost\\.serv_window_end').attr('readonly', true);
				
			}else{
				// set service window start time and end time
				View.panels.get(this.formId).record.setValue('compliance_contract_cost.serv_window_start',this.complianceContractCosts_ds.parseValue('compliance_contract_cost.serv_window_start', "09:00.00.000", false));
				View.panels.get(this.formId).record.setValue('compliance_contract_cost.serv_window_end',this.complianceContractCosts_ds.parseValue('compliance_contract_cost.serv_window_end', "17:00.00.000", false));
				View.panels.get(this.formId).fields.get('compliance_contract_cost.serv_window_start').syncToUI();
				View.panels.get(this.formId).fields.get('compliance_contract_cost.serv_window_end').syncToUI();
				View.panels.get(this.formId).fields.get('compliance_contract_cost.serv_window_end').fieldDef.readOnly = false;
				jQuery('#complianceProgramCosts'+vat+'_form_compliance_contract_cost\\.serv_window_end').attr('readonly', false);
			}
		},
		/*
		*//**
		 * save and add new.
		 */
		complianceProgramCosts_form_onSaveAndAddNew: function(){
			this.complianceProgramCosts_form_onSave();
			this.invokeAddNew();
		},
		
		/*
		*//**
		 * save and add new.
		 */
		complianceProgramCostsVat_form_onSaveAndAddNew: function(){
			this.complianceProgramCostsVat_form_onSave();
			this.invokeAddNew();
		},
		/**
		 * when delete button click.
		 */
		complianceProgramCosts_form_onDelete: function(){
			
	        var confirmMessage = getMessage("messageConfirmDelete");
	        var innerThis = this;
	        //add confirm when on delete.
	        View.confirm(confirmMessage, function(button){
	        	
	            if (button == 'yes') {
	            	var record = innerThis.complianceProgramCosts_form.getRecord(); 
	            	var dataSource = View.dataSources.get("complianceContractCosts_ds");
	            	dataSource.deleteRecord(record);
	            	innerThis.grid.refresh();
	            	innerThis.complianceProgramCosts_form.show(false);
	            	innerThis.complianceProgramCosts_grid.refresh();
	            }
	        });   
		},
		
		complianceProgramCosts_form_onCancel: function(){
	         
	        this.complianceProgramCosts_form.show(false);
	
		},
		
		complianceProgramCostsVat_form_onDelete: function(){
			
	        var confirmMessage = getMessage("messageConfirmDelete");
	        var innerThis = this;
	        //add confirm when on delete.
	        View.confirm(confirmMessage, function(button){
	        	
	            if (button == 'yes') {
	            	var record = innerThis.complianceProgramCostsVat_form.getRecord(); 
	            	var dataSource = View.dataSources.get("complianceContractCosts_ds");
	            	dataSource.deleteRecord(record);
	            	innerThis.grid.refresh();
	            	innerThis.complianceProgramCostsVat_form.show(false);
	            	innerThis.complianceProgramCosts_grid.refresh();
	            }
	        });   
		},
		
		complianceProgramCostsVat_form_onCancel: function(){
	         
	        this.complianceProgramCostsVat_form.show(false);
	
		}
	});

	/**
	 * when click edit button.
	 */
	function onEdit(){
		var form = commCostController.formId;
		View.panels.get(form).newRecord = false;
		commCostController.newRecord = false;
		commCostController.firstTime = true;
		var grid = commCostController.grid;
		var row = grid.rows[grid.selectedRowIndex];
		var cost_code = row["compliance_contract_cost.cost_code"];
		var restriction = "cost_code = "+cost_code;
		View.panels.get(form).refresh(restriction);
	}
	
	function addNewCost(){
		var form = commCostController.formId;
		var panel = View.panels.get(form);
		panel.newRecord = true;
		commCostController.newRecord = true;
		commCostController.firstTime = true;
		panel.refresh();
		if(valueExistsNotEmpty(commCostController.regulation)){
			panel.setFieldValue("compliance_contract_cost.regulation",commCostController.regulation);	
		}
		if(valueExistsNotEmpty(commCostController.regprogram)){
			panel.setFieldValue("compliance_contract_cost.reg_program",commCostController.regprogram);
		}
		if(valueExistsNotEmpty(commCostController.regrequirement)){
			panel.setFieldValue("compliance_contract_cost.reg_requirement",commCostController.regrequirement);
		}
		if(valueExistsNotEmpty(commCostController.location_id)){
			panel.setFieldValue("compliance_contract_cost.location_id",commCostController.location_id);
		}
		
		if(commCostController.isVATAndMCEnabled){
			panel.setFieldValue("compliance_contract_cost.ctry_id", View.user.country);
			panel.setFieldValue("compliance_contract_cost.currency_payment", View.user.userCurrency.code);
    	}

		
	}
	
	function onAutoCompleteSelect(form, fieldName, selectedValue){
		if (fieldName == "compliance_contract_cost.cost_cat_id" || fieldName == "compliance_contract_cost.ctry_id") {
			if(getVATPercent(form.id, 'compliance_contract_cost.ctry_id', 'compliance_contract_cost.cost_cat_id', '' , 'compliance_contract_cost.vat_percent_value')){
				// reset override vat percent field
				resetOverrideField("chk_vat_percent", 'complianceProgramCostsVat_form', "compliance_contract_cost.vat_percent_override", 0);
				// reset override vat amount field
				resetOverrideField("chk_vat_amount", 'complianceProgramCostsVat_form', "compliance_contract_cost.vat_amount_override", -1);
				// calculate costs
				calculateComplianceCosts('complianceProgramCostsVat_form', 'compliance_contract_cost', 'chk_vat_amount');
			}
		}
	}
	
	/**
	 * After Select value event handler.
	 * 
	 * @param fieldName
	 * @param selectedValue
	 * @param previousValue
	 */
	function afterSelectCostCategory(fieldName, selectedValue, previousValue){
		var objForm = View.panels.get('complianceProgramCostsVat_form');
		objForm.setFieldValue(fieldName, selectedValue);
		// get VAT percent for selected country and cost category
		if(getVATPercent(objForm.id, 'compliance_contract_cost.ctry_id', 'compliance_contract_cost.cost_cat_id', '' , 'compliance_contract_cost.vat_percent_value')){
			// reset override vat percent field
			resetOverrideField("chk_vat_percent", 'complianceProgramCostsVat_form', "compliance_contract_cost.vat_percent_override", 0);
			// reset override vat amount field
			resetOverrideField("chk_vat_amount", 'complianceProgramCostsVat_form', "compliance_contract_cost.vat_amount_override", -1);
			// calculate costs
			calculateComplianceCosts('complianceProgramCostsVat_form', 'compliance_contract_cost', 'chk_vat_amount');
		}
	}
	
	/**
	 * After select cost category event handler
	 * @param fieldName
	 * @param selectedValue
	 * @param previousValue
	 */
	function afterSelectCountryId(fieldName, selectedValue, previousValue){
		var objForm = View.panels.get('complianceProgramCostsVat_form');
		objForm.setFieldValue(fieldName, selectedValue);
		// get VAT percent for selected country and cost category
		if(getVATPercent(objForm.id, 'compliance_contract_cost.ctry_id', 'compliance_contract_cost.cost_cat_id', '' , 'compliance_contract_cost.vat_percent_value')){
			// reset override vat percent field
			resetOverrideField("chk_vat_percent", 'complianceProgramCostsVat_form', "compliance_contract_cost.vat_percent_override", 0);
			// reset override vat amount field
			resetOverrideField("chk_vat_amount", 'complianceProgramCostsVat_form', "compliance_contract_cost.vat_amount_override", -1);
			// calculate costs
			calculateComplianceCosts('complianceProgramCostsVat_form', 'compliance_contract_cost', 'chk_vat_amount');
		}
	}
	
	/**
	 * Calculate cost values.
	 * 
	 * @param formId
	 * @param costTable
	 * @param chkVatAmount
	 * @returns {Boolean}
	 */
	function calculateComplianceCosts(formId, costTable, chkVatAmount){
		var objForm = View.panels.get(formId);
		var dataSource = objForm.getDataSource();
		
		var costCommBase = objForm.getFieldValue(costTable + ".cost_comm_base_payment");
		var costUnitCommBase = objForm.getFieldValue(costTable + ".cost_unit_comm_base_payment");
		var paymentCommBase = objForm.getFieldValue(costTable + ".payment_comm_base_payment");
		var rateHourlyBase = objForm.getFieldValue(costTable + ".rate_hourly_base_payment");
		var rateDoubleBase = objForm.getFieldValue(costTable + ".rate_double_base_payment");
		var rateOverBase = objForm.getFieldValue(costTable + ".rate_over_base_payment");
		var costMaxCommBase = objForm.getFieldValue(costTable + ".cost_max_comm_base_payment");
		
		
		if (valueExistsNotEmpty(costCommBase) || valueExistsNotEmpty(costUnitCommBase) || valueExistsNotEmpty(paymentCommBase) || valueExistsNotEmpty(rateHourlyBase) || valueExistsNotEmpty(rateDoubleBase)
				|| valueExistsNotEmpty(rateOverBase) || valueExistsNotEmpty(costMaxCommBase)) {
			// we can calculate some values
			
			var isVatAmountOverride = document.getElementById(chkVatAmount).checked;
			if (isVatAmountOverride) {
				removeFieldError(objForm, costTable + ".vat_percent_value");
			}
			
			// get cost values
			var costValues = {
					"cost_comm_base_payment": costCommBase,
					"cost_unit_comm_base_payment": costUnitCommBase,
					"payment_comm_base_payment": paymentCommBase,
					"rate_hourly_base_payment": rateHourlyBase,
					"rate_double_base_payment": rateDoubleBase,
					"rate_over_base_payment": rateOverBase,
					"cost_max_comm_base_payment": costMaxCommBase,
					"vat_amount_override": objForm.getFieldValue(costTable + ".vat_amount_override"),
					"vat_percent_value": objForm.getFieldValue(costTable + ".vat_percent_value"),
					"cost_comm_vat_payment": objForm.getFieldValue(costTable + ".cost_comm_vat_payment"),
					"cost_unit_comm_vat_payment": objForm.getFieldValue(costTable + ".cost_unit_comm_vat_payment"), 
					"payment_comm_vat_payment": objForm.getFieldValue(costTable + ".payment_comm_vat_payment"), 
					"rate_hourly_vat_payment": objForm.getFieldValue(costTable + ".rate_hourly_vat_payment"), 
					"rate_double_vat_payment": objForm.getFieldValue(costTable + ".rate_double_vat_payment"), 
					"rate_over_vat_payment": objForm.getFieldValue(costTable + ".rate_over_vat_payment"), 
					"cost_max_comm_vat_payment": objForm.getFieldValue(costTable + ".cost_max_comm_vat_payment"), 
					"cost_comm_total_payment": objForm.getFieldValue(costTable + ".cost_comm_total_payment"),
					"cost_unit_comm_total_payment": objForm.getFieldValue(costTable + ".cost_unit_comm_total_payment"),
					"payment_comm_total_payment": objForm.getFieldValue(costTable + ".payment_comm_total_payment"),
					"rate_hourly_total_payment": objForm.getFieldValue(costTable + ".rate_hourly_total_payment"),
					"rate_double_total_payment": objForm.getFieldValue(costTable + ".rate_double_total_payment"),
					"rate_over_total_payment": objForm.getFieldValue(costTable + ".rate_over_total_payment"),
					"cost_max_comm_total_payment": objForm.getFieldValue(costTable + ".cost_max_comm_total_payment")
			};
			
			try {
				var result = Workflow.callMethod('AbCommonResources-CostService-calculateComplianceCostRuntimeValues', costTable, costValues, isVatAmountOverride);
				var calculatedValues = result.data;
				
				if (valueExistsNotEmpty(calculatedValues["displayValues"]) && calculatedValues["displayValues"] == "0") {
					return false;
				}
				
				// set new values
				if (valueExistsNotEmpty(calculatedValues["cost_comm_vat_payment"])) {
					var parsedValue = new Number(dataSource.parseValue(costTable + ".cost_comm_vat_payment", calculatedValues["cost_comm_vat_payment"], false));
					objForm.setFieldValue(costTable + ".cost_comm_vat_payment", dataSource.formatValue(costTable + ".cost_comm_vat_payment", parsedValue, true));
				}
				if (valueExistsNotEmpty(calculatedValues["cost_comm_total_payment"])) {
					var parsedValue =  new Number(dataSource.parseValue(costTable + ".cost_comm_total_payment", calculatedValues["cost_comm_total_payment"], false));
					objForm.setFieldValue(costTable + ".cost_comm_total_payment", dataSource.formatValue(costTable + ".cost_comm_total_payment", parsedValue, true));
				}

				if (valueExistsNotEmpty(calculatedValues["cost_unit_comm_vat_payment"])) {
					var parsedValue =  new Number(dataSource.parseValue(costTable + ".cost_unit_comm_vat_payment", calculatedValues["cost_unit_comm_vat_payment"], false));
					objForm.setFieldValue(costTable + ".cost_unit_comm_vat_payment", dataSource.formatValue(costTable + ".cost_unit_comm_vat_payment", parsedValue, true));
				}
				if (valueExistsNotEmpty(calculatedValues["cost_unit_comm_total_payment"])) {
					var parsedValue =  new Number(dataSource.parseValue(costTable + ".cost_unit_comm_total_payment", calculatedValues["cost_unit_comm_total_payment"], false));
					objForm.setFieldValue(costTable + ".cost_unit_comm_total_payment", dataSource.formatValue(costTable + ".cost_unit_comm_total_payment", parsedValue, true));
				}
				if (valueExistsNotEmpty(calculatedValues["payment_comm_vat_payment"])) {
					var parsedValue = new Number(dataSource.parseValue(costTable + ".payment_comm_vat_payment", calculatedValues["payment_comm_vat_payment"], false));
					objForm.setFieldValue(costTable + ".payment_comm_vat_payment", dataSource.formatValue(costTable + ".payment_comm_vat_payment", parsedValue, true));
				}
				if (valueExistsNotEmpty(calculatedValues["payment_comm_total_payment"])) {
					var parsedValue =  new Number(dataSource.parseValue(costTable + ".payment_comm_total_payment", calculatedValues["payment_comm_total_payment"], false));
					objForm.setFieldValue(costTable + ".payment_comm_total_payment", dataSource.formatValue(costTable + ".payment_comm_total_payment", parsedValue, true));
				}

				if (valueExistsNotEmpty(calculatedValues["rate_hourly_vat_payment"])) {
					var parsedValue =  new Number(dataSource.parseValue(costTable + ".rate_hourly_vat_payment", calculatedValues["rate_hourly_vat_payment"], false));
					objForm.setFieldValue(costTable + ".rate_hourly_vat_payment", dataSource.formatValue(costTable + ".rate_hourly_vat_payment", parsedValue, true));
				}
				if (valueExistsNotEmpty(calculatedValues["rate_hourly_total_payment"])) {
					var parsedValue =  new Number(dataSource.parseValue(costTable + ".rate_hourly_total_payment", calculatedValues["rate_hourly_total_payment"], false));
					objForm.setFieldValue(costTable + ".rate_hourly_total_payment", dataSource.formatValue(costTable + ".rate_hourly_total_payment", parsedValue, true));
				}
				if (valueExistsNotEmpty(calculatedValues["rate_double_vat_payment"])) {
					var parsedValue = new Number(dataSource.parseValue(costTable + ".rate_double_vat_payment", calculatedValues["rate_double_vat_payment"], false));
					objForm.setFieldValue(costTable + ".rate_double_vat_payment", dataSource.formatValue(costTable + ".rate_double_vat_payment", parsedValue, true));
				}
				if (valueExistsNotEmpty(calculatedValues["rate_double_total_payment"])) {
					var parsedValue =  new Number(dataSource.parseValue(costTable + ".rate_double_total_payment", calculatedValues["rate_double_total_payment"], false));
					objForm.setFieldValue(costTable + ".rate_double_total_payment", dataSource.formatValue(costTable + ".rate_double_total_payment", parsedValue, true));
				}

				if (valueExistsNotEmpty(calculatedValues["rate_over_vat_payment"])) {
					var parsedValue =  new Number(dataSource.parseValue(costTable + ".rate_over_vat_payment", calculatedValues["rate_over_vat_payment"], false));
					objForm.setFieldValue(costTable + ".rate_over_vat_payment", dataSource.formatValue(costTable + ".rate_over_vat_payment", parsedValue, true));
				}
				if (valueExistsNotEmpty(calculatedValues["rate_over_total_payment"])) {
					var parsedValue =  new Number(dataSource.parseValue(costTable + ".rate_over_total_payment", calculatedValues["rate_over_total_payment"], false));
					objForm.setFieldValue(costTable + ".rate_over_total_payment", dataSource.formatValue(costTable + ".rate_over_total_payment", parsedValue, true));
				}
				if (valueExistsNotEmpty(calculatedValues["cost_max_comm_vat_payment"])) {
					var parsedValue = new Number(dataSource.parseValue(costTable + ".cost_max_comm_vat_payment", calculatedValues["cost_max_comm_vat_payment"], false));
					objForm.setFieldValue(costTable + ".cost_max_comm_vat_payment", dataSource.formatValue(costTable + ".cost_max_comm_vat_payment", parsedValue, true));
				}
				if (valueExistsNotEmpty(calculatedValues["cost_max_comm_total_payment"])) {
					var parsedValue =  new Number(dataSource.parseValue(costTable + ".cost_max_comm_total_payment", calculatedValues["cost_max_comm_total_payment"], false));
					objForm.setFieldValue(costTable + ".cost_max_comm_total_payment", dataSource.formatValue(costTable + ".cost_max_comm_total_payment", parsedValue, true));
				}

				
			} catch (e){
				Workflow.handleError(e);
			}
		}
		return true;
	}
	
	/**
	 * Enable / disable override field based on checkbox selection
	 */
	function onSelect_Override(chkId, formId, fieldId, defaultValue){
		var objCheckbox = document.getElementById(chkId);
		var objForm = View.panels.get(formId);
		if(objCheckbox){
			objForm.enableField(fieldId, objCheckbox.checked);
			var dataSource = objForm.getDataSource();
			if(objCheckbox.checked){
				if(chkId == "chk_vat_percent"){
					document.getElementById("chk_vat_amount").checked = false;
					objForm.setFieldValue("compliance_contract_cost.vat_amount_override", dataSource.formatValue("compliance_contract_cost.vat_amount_override", -1, true));
					objForm.enableField("compliance_contract_cost.vat_amount_override", false);
				}else if(chkId == "chk_vat_amount"){
					document.getElementById("chk_vat_percent").checked = false;
					objForm.setFieldValue("compliance_contract_cost.vat_percent_override", dataSource.formatValue("compliance_contract_cost.vat_percent_override", 0, true));
					objForm.enableField("compliance_contract_cost.vat_percent_override", false);
					getVATPercent('complianceProgramCostsVat_form', 'compliance_contract_cost.ctry_id', 'compliance_contract_cost.cost_cat_id', '' , 'compliance_contract_cost.vat_percent_value', false);
				}
			}else{
				// we must reset field value
				if(valueExists(defaultValue)){
					var currentValue = objForm.getFieldValue(fieldId);
					if(parseFloat(currentValue) != defaultValue){
						objForm.setFieldValue(fieldId, dataSource.formatValue(fieldId, defaultValue, true));
						// get default VAT percent
						if(chkId == "chk_vat_percent"){
							getVATPercent('complianceProgramCostsVat_form', 'compliance_contract_cost.ctry_id', 'compliance_contract_cost.cost_cat_id', '' , 'compliance_contract_cost.vat_percent_value', false);
						}
					}
				}
			}
		}
	}