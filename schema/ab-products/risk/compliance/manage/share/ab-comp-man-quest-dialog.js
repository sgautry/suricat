var questionnaireTabController = View.createController('questionnaireTabController', {
    questionnaire_id: 0,
    survey_event_id: 0,
    eventStatus: '',
    isReadOnly: false,
    afterInitialDataFetch: function () {
        if (View.parameters && View.parameters.questionnaire_id) this.questionnaire_id = View.parameters.questionnaire_id;
        if (View.parameters && View.parameters.survey_event_id) this.survey_event_id = View.parameters.survey_event_id;
        if (View.parameters && View.parameters.eventStatus) this.eventStatus = View.parameters.eventStatus;
        this.onRefresh();
    },
    
    questionnaireView_onSave: function (showMessage) {
        this.questionnaireView_save(showMessage, false);
    },
    
    questionnaireView_save: function(showMessage, checkRequiredAnswers) {
    	var updated = false;
        var questionnaireView = View.panels.get('questionnaireView').contentView;
        if (questionnaireView) {
            var updateQuestionnaireController = questionnaireView.controllers.get('UpdateQuestionnaire');
            var questionnaire = questionnaireView.controllers.get('CompleteQuestionnaire').questionnaire;
            if (this.canSaveQuestionnaire(questionnaire)) {
                updated = updateQuestionnaireController.saveQuestionnaire(this.survey_event_id, checkRequiredAnswers);
            } else {
                var message = getMessage('noQuestionnaireDefined');
                if (questionnaire) {
                    var localizedStatus = questionnaireView.dataSources.get('questionnairesDataSource').fieldDefs.get('questionnaire_ext.status').enumValues[questionnaire.status];
                    message = String.format(getMessage('saveOnlyActiveQuestionnaire'), localizedStatus);
                }
                View.showMessage(message);
            }
            if (updated) {
                showMessage = valueExists(showMessage) ? showMessage : true;
                this.updateSurveyEventStatus('IN PROGRESS', showMessage);
            }
        }
        return updated;
    },
    
    questionnaireView_onSaveSubmit: function () {
        if (this.questionnaireView_save(false, true)) {
            var updateQuestionnaireController = View.panels.get('questionnaireView').contentView.controllers.get('UpdateQuestionnaire');
            updateQuestionnaireController.processQuestionnaire(this.questionnaire_id, this.survey_event_id, this.updateSurveyEventStatus.createDelegate(this, ['COMPLETED', true]));
        }
    },
    updateSurveyEventStatus: function (status, showMessage) {
        var questionnaireView = View.panels.get('questionnaireView').contentView;
        if (valueExists(questionnaireView)) {
            var eventsDataSource = questionnaireView.dataSources.get('eventsDataSource');
            var eventRecord = eventsDataSource.getRecord(new Ab.view.Restriction({'activity_log.activity_log_id': this.survey_event_id}));
            var oldStatus = eventRecord.getValue('activity_log.status');
            // do not set the same status twice since method is called two times on submit questionnaire.
            if ('COMPLETED' !== oldStatus && oldStatus !== status) {
                eventRecord.setValue('activity_log.status', status);
                // save event new status
                eventsDataSource.saveRecord(eventRecord);
                if (showMessage) {
                    // display message that event was updated
                    var localizedStatus = eventsDataSource.fieldDefs.get('activity_log.status').enumValues[status];
                    View.showMessage(String.format(getMessage('eventStatusUpdated'), localizedStatus));
                }
                this.eventStatus = status;
                this.onRefresh();
                if (View.parameters && View.parameters.callback) {
                    View.parameters.callback();
                }
            }
        }
    },
    /**
     * The questionnaire forms should not allow save/submission of any questionnaire with status "Draft" or "Inactive" and only "Active".
     * @param {Object} questionnaire
     * @returns {*|boolean} true if questionnaire status is active.
     */
    canSaveQuestionnaire: function (questionnaire) {
        return valueExists(questionnaire) && 'active' === questionnaire.status;
    },
    onRefresh: function () {
    	this.isReadOnly = false;
        if (this.eventStatus === 'CANCELLED' ||
            this.eventStatus === 'STOPPED' ||
            this.eventStatus === 'REJECTED' ||
            this.eventStatus === 'COMPLETED' ||
            this.eventStatus === 'COMPLETED-V' ||
            this.eventStatus === 'CLOSED') {
            this.isReadOnly = true;
        } else {
            //check if event is assigned to current employee.
            var isMyEventRecord = View.dataSources.get('abMyEventActivityLogDs').getRecord(new Ab.view.Restriction({'activity_log.activity_log_id': this.survey_event_id}));
            this.isReadOnly = _.isEmpty(isMyEventRecord.values);
        }
        loadQuestionnairePanel();
        this.questionnaireView.enableAction('save', !this.isReadOnly);
        this.questionnaireView.enableAction('saveSubmit', !this.isReadOnly);
    }
});

function loadQuestionnairePanel() {
    var controller = View.controllers.get('questionnaireTabController');
    var questionnaireViewPanel = View.panels.get('questionnaireView');
    var questionnaireView = questionnaireViewPanel.contentView;
    if (questionnaireView) {
        var updateQuestionnaireController = questionnaireView.controllers.get('UpdateQuestionnaire');
        updateQuestionnaireController.loadQuestionnaireWithResults(controller.questionnaire_id, controller.isReadOnly, controller.survey_event_id);
        var parentRegion = questionnaireViewPanel.getLayoutRegion();
        var parentHeight = Ext.get(parentRegion.contentEl).parent().getHeight();
        questionnaireViewPanel.frame.setHeight(parentHeight - 40);
    } else {
        setTimeout(loadQuestionnairePanel, 500);
    }
}

