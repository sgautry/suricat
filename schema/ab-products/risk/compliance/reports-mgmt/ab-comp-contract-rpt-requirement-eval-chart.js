var abCompContractRptChartController = View.createController('abCompContractRptChartController', {	
	/**
	 * compliance level and level number map
	 */
	complianceLevelNumberdColorMap : {},
	
	/**
	 * compliance level filled color map
	 */
	complianceLevelFilledColorMap : {
		 0:'#FF0000', 1:'#C00000', 2:'#FFC000', 3:'#FFFF00', 4:'#996633', 5:'#7030A0', 6:'#00B0F0', 7:'#0000FF', 8:'#00FF00'
	},
	afterViewLoad : function() {
		//get compliance level and level number map
		this.complianceLevelNumberdColorMap = this.getComplianceLevelNumberdColorMap();
	},
	
	afterInitialDataFetch: function() {
		// get console view controller from opener view
		this.consoleController = View.getOpenerView().controllers.get('controllerConsole');
		//register to console controller
		if (this.consoleController) {
			this.consoleController.controllers.push(this);
		}
		
    } ,
	/**
	 * get compliance level and level number map.
	 */
	getComplianceLevelNumberdColorMap : function() {
		
		var complianceLevelNumberdColorMap = {};
		
		//prepare parameter for WFR AbCommonResources-getDataRecords
		var parameters = {
				tableName: 'regcomplevel',
				fieldNames: toJSON(['regcomplevel.comp_level','regcomplevel.level_number']),
				restriction: toJSON('1=1')
		};
		
		//get all compliance level records and put the data to map
		var result = Workflow.call('AbCommonResources-getDataRecords', parameters);
		if(result.code == "executed" && result.data.records.length > 0){
			for(var i=0;i<result.data.records.length;i++){
				complianceLevelNumberdColorMap[result.data.records[i]['regcomplevel.comp_level']] 
				= result.data.records[i]['regcomplevel.level_number.raw']
			}
		}
		
		//return the map
		return complianceLevelNumberdColorMap;
		
	},
	
	/**
	 * refresh chart from console filter.
	 */
	refreshFromConsole: function() {
		if(this.consoleController){
			//get chart panel
			var chart = this.chartPie_chart;
			chart.addParameter('regProgram',this.consoleController.programParam);
			chart.refresh();
			View.panels.get('chartPie_chart').setSolidFillColors(setCustomFillColor());
		}
	},
});
	

/**
 * over-write the getDataFromDataSources method in the core chart control to reset fill color and format the enum field values
 */
Ab.chart.HtmlChart.prototype.getDataFromDataSources = function(restriction){	 
	try {
	    var parameters = this.getParameters(restriction);
	    //XXX: default time-out is 20S, but for charts, it should be longer (120S?)
        var result = Workflow.call(this.refreshWorkflowRuleId, parameters, 120);
        
        this.rawData = toJSON(result.data);
        
        //KB3036039 - assign colors to priority, severity, and compliance level (level_number) values the same way as in map views
        setCustomFillColor();
        
        //return the data
        return result.data;
	} catch (e) {
		this.handleError(e);
	}
 }

/**
 * extend method resetFillColor in the core chart control to format the enum field values
 */
var setCustomFillColor =  function() {
	var chart = abCompContractRptChartController.chartPie_chart;
	var customFillColors = [];
	var data =  eval("(" + chart.rawData + ")");

		for(var i=0; i<data.length;i++){
					var levelNumber = abCompContractRptChartController.complianceLevelNumberdColorMap[data[i]["regrequirement.comp_level"]];
					//if the value in the map, get color from the map, otherwise set to black(no value)
					if(abCompContractRptChartController.complianceLevelFilledColorMap[levelNumber]){
						customFillColors.push(abCompContractRptChartController.complianceLevelFilledColorMap[levelNumber]);
					}else{
						customFillColors.push('#000000');
					}
		}
		return customFillColors;

	
}