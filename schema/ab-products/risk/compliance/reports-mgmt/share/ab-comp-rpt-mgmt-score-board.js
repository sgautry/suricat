/**
 * @author Guo Jiangtao
 */
var abCompRptCommManScoreBoardController = View.createController('abCompRptCommManScoreBoardController', {

	/**
	 * the main view controller which contain this common chart view
	 */
	mainController : null,
	
	/**
	 * the console view controller
	 */
	consoleController : null,
	calcField2:null,

	/**
	 * reset the panel title after the cross table loaded.
	 */
	afterInitialDataFetch : function() {
		// get console view controller from opener view
		this.consoleController = View.getOpenerView().controllers.get('controllerConsole');
		//register to console controller
		if (this.consoleController) {
			this.consoleController.controllers.push(this);
		}
		
		// get main controller from opener view
		this.mainController = View.getOpenerView().controllers.get(0);

		// get property value from main controller
		if (this.mainController && this.mainController.controls) {

			var controls = this.mainController.controls;
			for ( var i = 0; i < controls.length; i++) {
				if (controls[i].type == 'scoreBoard' && !controls[i].isLoad) {
					this.control = controls[i];
					break;
				}
			}
		}
		
		var calculatedFields = [];
		calculatedFields.push(this.abCompRptCommManScoreBoard.calculatedFields[0]);
		if(this.control.secondCalcField){
			calculatedFields.push(this.abCompRptCommManScoreBoard.calculatedFields[1]);
		}
		this.abCompRptCommManScoreBoard.calculatedFields = calculatedFields;

		// set all parameters
		this.setParameters();
		
		// reset the chart panel title
		this.abCompRptCommManScoreBoard.config.title = this.control.title

		// refresh and show cross table
		if(this.consoleController.consoleRestriction){
			// fix KB3035927 - refreshed according to the console restrictions when switch from one tab to anothe
			this.refreshFromConsole();
		}else{
			this.abCompRptCommManScoreBoard.refresh();
		}
		
		this.control.isLoad = true;
	},

	/**
	 * set required parameters of this view.
	 */
	setParameters : function() {
		var scoreBoard = this.abCompRptCommManScoreBoard;
		this.setQuery();
		scoreBoard.addParameter('firstGroupField', this.control.firstGroupField.name);
		scoreBoard.addParameter('secondGroupField', this.control.secondGroupField.name);
		scoreBoard.addParameter('calcField', this.control.firstCalcField.name);
		scoreBoard.addParameter('firstGroupSortField', this.control.firstGroupSortField.name);
		scoreBoard.addParameter('secondGroupSortField', this.control.secondGroupSortField.name);
		scoreBoard.groupByFields[0].title = this.control.firstGroupField.title;
		scoreBoard.groupByFields[1].title = this.control.secondGroupField.title;
		scoreBoard.calculatedFields[0].title = this.control.firstCalcField.title;
		
		if(this.control.permanentParameter){
			crossTable.addParameter('permanentParameter', this.control.permanentParameter);
		}
		
		if(this.control.secondCalcField){
			scoreBoard.addParameter('calcField2', this.control.secondCalcField.name);
			scoreBoard.calculatedFields[1].title = this.control.secondCalcField.title;
			this.calcField2=this.control.secondCalcField.name
		}else{
			//default value
			scoreBoard.addParameter('calcField2', '1');
		}
	},
	

	/**
	 * set database query base on the main table name.
	 */
	setQuery : function() {
		var scoreBoard = this.abCompRptCommManScoreBoard;
		var mainTable = this.control.mainTable;	
		var byLocation = this.control.byLocation;	
		
		scoreBoard.addParameter('regrequirementQuery', false);
		scoreBoard.addParameter('regrequirementByLocationQuery', false);
		scoreBoard.addParameter('regulationQuery', false);
		scoreBoard.addParameter('regulationByLocationQuery', false);
		scoreBoard.addParameter('regprogramQuery', false);
		scoreBoard.addParameter('regprogramByLocationQuery', false);
		scoreBoard.addParameter('regviolationQuery', false);
		switch(mainTable){
		  case 'regrequirement': 

			  //qurery from table regrequirement
			  if(byLocation){
				  scoreBoard.addParameter('regrequirementByLocationQuery', true);
			  }else{
				  scoreBoard.addParameter('regrequirementQuery', true);
			  }


			  break;
		  case 'regulation': 
			  
			  //qurery from table regrequirement
			  if(byLocation){
				  scoreBoard.addParameter('regulationByLocationQuery', true);
			  }else{
				  scoreBoard.addParameter('regulationQuery', true);
			  }
			  
			  
			  break;  
		  case 'regprogram': 
			  
			  if(byLocation){
				  scoreBoard.addParameter('regprogramByLocationQuery', true);
			  }else{
				  scoreBoard.addParameter('regprogramQuery', true);
			  }

			  break;  

		  case 'regviolation': 
			  
			  //qurery from table regviolation
			  scoreBoard.addParameter('regviolationQuery', true);
			  
			  break;
		}
		
	},

	/**
	 * insert missing values for X and Y axis
	 * 
	 * @param {Object}
	 *            panel
	 * @param {Object}
	 *            dataSet
	 */
	abCompRptCommManScoreBoard_afterGetData : function(panel, dataSet) {
		var defaultColumnSubtotal = new Ab.data.Record({
			'regloc.regloc_id' : {
				l : '0',
				n : '0'
			},
			'regloc.firstGroupField' : {
				l : '0',
				n : '0'
			},
			'regloc.calcField' : {
				l : '0',
				n : '0'
			}
		});
		var defaultRowSubtotal = new Ab.data.Record({
			'regloc.regloc_id' : {
				l : '0',
				n : '0'
			},
			'regloc.secondGroupField' : {
				l : '0',
				n : '0'
			},
			'regloc.calcField' : {
				l : '0',
				n : '0'
			}
		});
		
		if(this.calcField2){
			defaultColumnSubtotal = new Ab.data.Record({
				'regloc.regloc_id' : {
					l : '0',
					n : '0'
				},
				'regloc.firstGroupField' : {
					l : '0',
					n : '0'
				},
				'regloc.calcField' : {
					l : '0',
					n : '0'
				},
				'regloc.calcField2' : {
					l : '0',
					n : '0'
				}
			});
			defaultRowSubtotal = new Ab.data.Record({
				'regloc.regloc_id' : {
					l : '0',
					n : '0'
				},
				'regloc.secondGroupField' : {
					l : '0',
					n : '0'
				},
				'regloc.calcField' : {
					l : '0',
					n : '0'
				},
				'regloc.calcField2' : {
					l : '0',
					n : '0'
				}
			});
		}

		var columnValues = new Ext.util.MixedCollection();
		columnValues.add('1', defaultColumnSubtotal);
		columnValues.add('2', defaultColumnSubtotal);
		columnValues.add('3', defaultColumnSubtotal);
		columnValues.add('4', defaultColumnSubtotal);
		columnValues.add('5', defaultColumnSubtotal);
		columnValues.add('6', defaultColumnSubtotal);
		columnValues.add('7', defaultColumnSubtotal);
		columnValues.add('8', defaultColumnSubtotal);
		columnValues.add('9', defaultColumnSubtotal);

		var levels = this.abCompRptCommManScoreBoard_Level_DS.getRecords();
		var rowValues = new Ext.util.MixedCollection();
		for ( var i = 0; i < levels.length; i++) {
			rowValues.add(levels[i].getValue('regcomplevel.comp_level'), defaultRowSubtotal);
		}

		for ( var c = 0; c < dataSet.columnValues.length; c++) {
			var columnValue = dataSet.columnValues[c].n;
			var columnSubtotal = dataSet.columnSubtotals[c];

			columnValues.replace(columnValue, columnSubtotal);
		}
		// use new column values and sub-totals
		dataSet.columnValues = [];
		dataSet.columnSubtotals = [];
		var priorityEnum = this.abCompRptCommManScoreBoard_regcompliance_DS.fieldDefs.get(0).enumValues;

		columnValues.eachKey(function(columnValue) {
			var columnSubtotal = columnValues.get(columnValue);
			dataSet.columnValues.push({
				l : priorityEnum[columnValue],
				n : columnValue
			});
			dataSet.columnSubtotals.push(columnSubtotal);
		});

		for ( var r = 0; r < dataSet.rowValues.length; r++) {
			var rowValue = dataSet.rowValues[r].n;
			var rowSubtotal = dataSet.rowSubtotals[r];

			rowValues.replace(rowValue, rowSubtotal);
		}
		// use new column values and sub-totals
		dataSet.rowValues = [];
		dataSet.rowSubtotals = [];

		rowValues.eachKey(function(rowValue) {
			var rowSubtotal = rowValues.get(rowValue);

			dataSet.rowValues.push({
				l : rowValue,
				n : rowValue
			});
			dataSet.rowSubtotals.push(rowSubtotal);
		});
	},

	/**
	 * after refresh we need to set the colors
	 */
	abCompRptCommManScoreBoard_afterRefresh : function() {
		this.panelScoreboard_colorcode();
	},

	/**
	 * set scoreboard colors
	 */
	panelScoreboard_colorcode : function() {
		var levels = this.abCompRptCommManScoreBoard_Level_DS.getRecords();
		var styleCode = [
		                 ['1', '1', '2', '3', '4', '5'], 
		                 ['1', '2', '2', '3', '4', '5'], 
		                 ['2', '2', '2', '3', '4', '5'], 
		                 ['2', '2', '3', '3', '4', '5'], 
		                 ['3', '3', '3', '3', '4', '5'],
		                 ['3', '3', '3', '3', '4', '5'], 
		                 ['4', '4', '4', '4', '4', '5'], 
		                 ['4', '4', '4', '4', '5', '5'], 
		                 ['4', '4', '4', '5', '5', '5'], 
		                 ['4', '4', '5', '5', '5', '5'],
		                 ['5', '5', '5', '5', '5', '5'] ];
		for ( var i = 0; i < levels.length; i++) {
			for ( var j = 0; j < 9; j++) {
				colorBlock(i, j, 'PriorityRating5');
				if (i < 10 && j<6) {
					colorBlock(i, j, 'PriorityRating' + styleCode[i][j]);
				} else {
					colorBlock(i, j, 'PriorityRating5');
				}
			}
		}
	},
	
	
	/**
	 * refresh chart from console filter.
	 */
	refreshFromConsole: function() {
		if(this.consoleController){
			var crossTable = this.abCompRptCommManScoreBoard;
			
			//set parameter to the crossTable panel
			if(this.consoleController.consoleRestriction){
				var mainTable = this.control.mainTable;
				var byLocation = this.control.byLocation;	
				//for special crossTable : Compliance Program Count by Compliance Level and Location and Compliance Requirements Count by Compliance Level and Location
				//the console restriction is divided to two parts: 1- console restriction excluding compliance level, 
				//2, calculated compliance level field restriction 
				if((mainTable=='regrequirement'|| mainTable=='regprogram') && byLocation){
					this.setQuery();
					var consoleRestriction = this.consoleController.consoleRestriction;
					if(valueExistsNotEmpty(this.control.permanentParameter)){
						consoleRestriction = consoleRestriction+" AND "+this.control.permanentParameter;
					}
					//add console restriction excluding compliance level to the query
					crossTable.addParameter('queryConsoleRestriction',consoleRestriction);
					crossTable.addParameter('permanentParameter','1=1');
					
					//add calculated compliance level field restriction to the parameter
					crossTable.addParameter('consoleRestriction', this.consoleController.complianceLevelRestriction);
					
				}else if(!byLocation&&mainTable=='regprogram'){
					this.setQuery();
					var subQueryConsoleRestriction = this.consoleController.subQueryConsoleRestriction;
					
					crossTable.addParameter('subQueryConsoleRestriction',subQueryConsoleRestriction);
					crossTable.addParameter('consoleRestriction', this.consoleController.consoleRestriction);
					
				}
				//for the other charts, only need one console restriction parameter 
				else{
					crossTable.addParameter('consoleRestriction', this.consoleController.consoleRestriction);
				}
			}
			
			crossTable.refresh();
		}
	}
});

/**
 * set style for a specific cell
 * 
 * @param {Object}
 *            row
 * @param {Object}
 *            column
 * @param {Object}
 *            class_name
 */
function colorBlock(row, column, class_name) {
	var panel = Ab.view.View.getControl(window, 'abCompRptCommManScoreBoard');
	var node = panel.getCellElement(row, column, 0);
	if (node) {
		node.parentNode.className = class_name;
	}
	
	if(abCompRptCommManScoreBoardController.calcField2){
		var node = panel.getCellElement(row, column, 1);
		if (node) {
			node.parentNode.className = class_name;
		}
	}
}

/**
 * Click event handler for score board.
 * 
 */
function onClickScoreBordItem(ob) {
	ob.selectedChartData = {};
	if(ob.restriction.clauses.length > 0){
		ob.selectedChartData[ob.restriction.clauses[0].name] = ob.restriction.clauses[0].value;
	}
	
	if(ob.restriction.clauses.length > 1){
		ob.selectedChartData[ob.restriction.clauses[1].name] = ob.restriction.clauses[1].value;
	}
	//call main controller method to open pup up
	if(abCompRptCommManScoreBoardController.mainController.openPopUpView){
		abCompRptCommManScoreBoardController.mainController.openPopUpView(ob,abCompRptCommManScoreBoardController.control);
	}
}
