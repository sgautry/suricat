/**
 * @author Guo Jiangtao
 */
var abCompRptCommManCrossTableController = View.createController('abCompRptCommManCrossTableController', {

	/**
	 * the main view controller which contain this common chart view
	 */
	mainController : null,
	
	/**
	 * the console view controller
	 */
	consoleController : null,	

	/**
	 * type of ManageReportControl
	 */
	control : null,
	
	/**
	 * priority field define
	 */
	priorityFiledDef : null,
	
	/**
	 * rank field define
	 */
	rankFiledDef : null,
	
	/**
	 * severity field define
	 */
	severityFiledDef : null,

	/**
	 * reset the panel title after the cross table loaded.
	 */
	afterInitialDataFetch : function() {
		this.priorityFiledDef = View.dataSources.get('abCompRptCommManChart_fieldDef_DS').fieldDefs.get('regrequirement.priority');
		this.rankFiledDef = View.dataSources.get('abCompRptCommManChart_fieldDef_DS').fieldDefs.get('regulation.reg_rank');
		this.severityFiledDef = View.dataSources.get('abCompRptCommManChart_fieldDef_DS').fieldDefs.get('regviolation.severity');
		// get console view controller from opener view
		this.consoleController = View.getOpenerView().controllers.get('controllerConsole');
		//register to console controller
		if (this.consoleController) {
			this.consoleController.controllers.push(this);
		}
		
		// get main controller from opener view
		this.mainController = View.getOpenerView().controllers.get(0);

		// get property value from main controller
		if (this.mainController && this.mainController.controls) {

			var controls = this.mainController.controls;
			for ( var i = 0; i < controls.length; i++) {
				if (controls[i].type == 'crossTable' && !controls[i].isLoad) {
					this.control = controls[i];
					break;
				}
			}
			
			if(this.mainController.beforeControlLoad){
				this.mainController.beforeControlLoad(this.control,this.abCompRptCommManCrossTable,abCompRptCommManCrossTableController);
			}
		}
		
		var calculatedFields = [];
		calculatedFields.push(this.abCompRptCommManCrossTable.calculatedFields[0]);
		if(this.control.secondCalcField){
			calculatedFields.push(this.abCompRptCommManCrossTable.calculatedFields[1]);
		}
		this.abCompRptCommManCrossTable.calculatedFields = calculatedFields;

		// set all parameters
		this.setParameters();
		
		// reset the chart panel title
		this.abCompRptCommManCrossTable.config.title = this.control.title
		
		this.abCompRptCommManCrossTable.addEventListener('afterGetData', this.afterGetData, this);

		this.control.isLoad = true;
		
		//if showOnLoad = true, load the cross table data
		if(this.control.showOnLoad){
			this.showCrossTableAfterLoad();
		}
	},
	
	/**
	 * show cross table after the view load.
	 */
	showCrossTableAfterLoad: function() {
		
		// refresh and show cross table
		if(this.consoleController.consoleRestriction){
			// fix KB3035927 - refreshed according to the console restrictions when switch from one tab to anothe
			this.refreshFromConsole();
		}else{
			this.abCompRptCommManCrossTable.refresh();
		}
	},

	/**
	 * set required parameters of this view.
	 */
	setParameters : function() {
		var crossTable = this.abCompRptCommManCrossTable;
		this.setQuery();
		crossTable.addParameter('firstGroupField', this.control.firstGroupField.name);
		crossTable.addParameter('secondGroupField', this.control.secondGroupField.name);
		crossTable.addParameter('calcField', this.control.firstCalcField.name);
		crossTable.addParameter('firstGroupSortField', this.control.firstGroupSortField.name);
		crossTable.addParameter('secondGroupSortField', this.control.secondGroupSortField.name);

		crossTable.groupByFields[0].title = this.control.firstGroupField.title;
		crossTable.groupByFields[1].title = this.control.secondGroupField.title;
		crossTable.calculatedFields[0].title = this.control.firstCalcField.title;
		
		if(this.control.permanentParameter){
			crossTable.addParameter('permanentParameter', this.control.permanentParameter);
		}
		
		if(this.control.secondCalcField){
			crossTable.addParameter('calcField2', this.control.secondCalcField.name);
			crossTable.calculatedFields[1].title = this.control.secondCalcField.title;
		}
	},
	
	
	/**
	 * set database query base on the main table name.
	 */
	setQuery : function() {
		var crossTable = this.abCompRptCommManCrossTable;
		var mainTable = this.control.mainTable;	
		var byLocation = this.control.byLocation;	
		
		crossTable.addParameter('regrequirementQuery', false);
		crossTable.addParameter('regrequirementByLocationQuery', false);
		crossTable.addParameter('regulationQuery', false);
		crossTable.addParameter('regulationByLocationQuery', false);
		crossTable.addParameter('regprogramQuery', false);
		crossTable.addParameter('regprogramByLocationQuery', false);
		crossTable.addParameter('regviolationQuery', false);
		crossTable.addParameter('activityLogQuery', false);
		crossTable.addParameter('activityLogByLocationQuery', false);
		
		switch(mainTable){
		  case 'regrequirement': 
			  
			  //qurery from table regrequirement
			  if(byLocation){
				  crossTable.addParameter('regrequirementByLocationQuery', true);
			  }else{
				  crossTable.addParameter('regrequirementQuery', true);
			  }

			  break;
		  case 'regulation': 
			  
			  //qurery from table regulation
			  if(byLocation){
				  crossTable.addParameter('regulationByLocationQuery', true);
			  }else{
				  crossTable.addParameter('regulationQuery', true);
			  }
			  
			  break;  
		  case 'regprogram': 
			  
			  if(byLocation){
				  crossTable.addParameter('regprogramByLocationQuery', true);
			  }else{
				  crossTable.addParameter('regprogramQuery', true);
			  }


			  break;  
			  
		  case 'regviolation': 
			  
			  //qurery from table regviolation
			  crossTable.addParameter('regviolationQuery', true);
			  
			  break;

		  case 'activity_log': 
			  
			  if(byLocation){
				  crossTable.addParameter('activityLogByLocationQuery', true);
			  }else{
				  crossTable.addParameter('activityLogQuery', true);
			  }
			  
			  break;
		}
		
	},
	
	
	/**
	 * refresh chart from console filter.
	 */
	refreshFromConsole: function() {
		if(this.consoleController){
			var crossTable = this.abCompRptCommManCrossTable;
			
			//set parameter to the crossTable panel
			if(this.consoleController.consoleRestriction){
				var mainTable = this.control.mainTable;
				var byLocation = this.control.byLocation;	
				//for special crossTable : Compliance Program Count by Compliance Level and Location and Compliance Requirements Count by Compliance Level and Location
				//the console restriction is divided to two parts: 1- console restriction excluding compliance level, 
				//2, calculated compliance level field restriction 
				if((mainTable=='regrequirement'|| mainTable=='regprogram') && byLocation){
					this.setQuery();
					var consoleRestriction = this.consoleController.consoleRestriction;
					if(valueExistsNotEmpty(this.control.permanentParameter)){
						consoleRestriction = consoleRestriction+" AND "+this.control.permanentParameter;
					}
					//add console restriction excluding compliance level to the query
					crossTable.addParameter('queryConsoleRestriction', consoleRestriction);
					crossTable.addParameter('permanentParameter','1=1');
					
					//add calculated compliance level field restriction to the parameter
					if(this.consoleController.complianceLevelRestriction){
						crossTable.addParameter('consoleRestriction', this.consoleController.complianceLevelRestriction);
					}
				}
				else if(!byLocation&&(mainTable=='regulation'||mainTable=='regprogram')){
					this.setQuery();
					var subQueryConsoleRestriction = this.consoleController.subQueryConsoleRestriction;
					crossTable.addParameter('subQueryConsoleRestriction', subQueryConsoleRestriction);
					crossTable.addParameter('consoleRestriction', this.consoleController.consoleRestriction);
					
				}
				//for the other charts, only need one console restriction parameter 
				else{
					if(valueExistsNotEmpty(this.control.permanentParameter)){
						crossTable.addParameter('permanentParameter',this.control.permanentParameter);
					}
					crossTable.addParameter('consoleRestriction', this.consoleController.consoleRestriction);
				}
			}
			
			crossTable.refresh();
		}
	},
	
	afterGetData: function(panel, dataSet){
		var firstGroupFieldDef = abCompRptCommManCrossTableController[abCompRptCommManCrossTableController.control.firstGroupFieldDef];
		var secondGroupFieldDef = abCompRptCommManCrossTableController[abCompRptCommManCrossTableController.control.secondGroupFieldDef];
		
		//integer regular expression
		var intRegExp  = /^-?\d+$/;
		
		if(firstGroupFieldDef){
			for(var i=0; i<dataSet.rowValues.length;i++){
				//if the value is integer, then get the display value in enum 
				if(intRegExp.test(dataSet.rowValues[i].n)){
					dataSet.rowValues[i].l = firstGroupFieldDef.formatValue(dataSet.rowValues[i].l);
				}
			}
		}
	
	    if(secondGroupFieldDef){
	    	for(var i=0; i<dataSet.columnValues.length;i++){
	    		//if the value is integer, then get the display value in enum 
	    		if(intRegExp.test(dataSet.columnValues[i].n)){
	    			dataSet.columnValues[i].l = secondGroupFieldDef.formatValue(dataSet.columnValues[i].l);
	    		}
			}
		}
	}
});