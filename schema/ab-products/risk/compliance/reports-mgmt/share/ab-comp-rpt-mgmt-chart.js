/**
 * @author Guo Jiangtao
 */
var abCompRptCommManChartController = View.createController('abCompRptCommManChartController', {
    /**
     * the main view controller which contain this common chart view
     */
    mainController: null,
    /**
     * the console view controller
     */
    consoleController: null,
    /**
     * type of ManageReportControl
     */
    control: null,
    /**
     * priority field define
     */
    priorityFiledDef: null,
    /**
     * rank field define
     */
    rankFiledDef: null,
    /**
     * severity field define
     */
    severityFiledDef: null,
    /**
     * requirement type field define
     */
    reqTypeFieldDef: null,
    /**
     * compliance level and level number map
     */
    complianceLevelNumberdColorMap: {},
    /**
     * compliance level filled color map
     */
    complianceLevelFilledColorMap: {
        0: '#FF0000', 1: '#FF3300', 2: '#FF6600', 3: '#FF9A00', 4: '#FFCD00', 5: '#FFFF00', 6: '#C9FB00', 7: '#8EF300', 8: '#36CF00'
    },
    /**
     * priority and severity filled color map
     */
    priorityAndSeverityFilledColorMap: {
        1: '#FF0000', 2: '#FF3300', 3: '#FF5100', 4: '#FF6F00', 5: '#FF9100', 6: '#FFAF00', 7: '#FFCD00', 8: '#FFE600', 9: '#FFFF00'
    },
    /**
     * reset the panel title after the chart loaded.
     */
    afterInitialDataFetch: function () {
        //get compliance level and level number map
        this.complianceLevelNumberdColorMap = this.getComplianceLevelNumberdColorMap();
        //get enum type field define
        this.priorityFiledDef = View.dataSources.get('abCompRptCommManChart_fieldDef_DS').fieldDefs.get('regrequirement.priority');
        this.rankFiledDef = View.dataSources.get('abCompRptCommManChart_fieldDef_DS').fieldDefs.get('regulation.reg_rank');
        this.severityFiledDef = View.dataSources.get('abCompRptCommManChart_fieldDef_DS').fieldDefs.get('regviolation.severity');
        this.reqTypeFieldDef = View.dataSources.get('abCompRptCommManChart_fieldDef_DS').fieldDefs.get('regrequirement.regreq_type');
        // get console view controller from opener view
        this.consoleController = View.getOpenerView().controllers.get('controllerConsole');
        //register to console controller
        if (this.consoleController) {
            this.consoleController.controllers.push(this);
        }
        // get main controller from opener view
        this.mainController = View.getOpenerView().controllers.get(0);
        // get property value from main controller
        if (this.mainController && this.mainController.controls) {
            var controls = this.mainController.controls;
            for ( var i = 0; i < controls.length; i++ ) {
                if (controls[i].type == 'chart' && !controls[i].isLoad) {
                    this.control = controls[i];
                    break;
                }
            }
            if (this.mainController.beforeControlLoad) {
                this.mainController.beforeControlLoad(this.control, this.abCompRptCommManChart, abCompRptCommManChartController);
            }
        }
        // set all parameters
        this.setParameters();
        // reset the chart panel title
        this.abCompRptCommManChart.config.title = this.control.title
        //set isload flag
        this.control.isLoad = true;
        this.abCompRptCommManChart.chartControl.chart.addLegend();
        //if showOnLoad = true, load the chart data
        if (this.control.showOnLoad) {
            this.showChartAfterLoad();
        }
        var chart = this.abCompRptCommManChart;
        chart.chartControl.addEventListener("clickGraphItem", function (event) {
            var selectedChartData = event.item.dataContext;
            selectedChartData['regloc.firstGroupField'] = event.item.category;
            selectedChartData['regloc.secondGroupField'] = event.item.graph.valueField;
            var clickObject = {'selectedChartData': selectedChartData};
            onClickChart(clickObject);
        });
    },
    /**
     * show chart panel after the view load.
     */
    showChartAfterLoad: function () {

        // refresh and show chart
        if (this.consoleController.consoleRestriction) {
            // fix KB3035927 - refreshed according to the console restrictions when switch from one tab to another
            this.refreshFromConsole();
        }
    },
    /**
     * get compliance level and level number map.
     */
    getComplianceLevelNumberdColorMap: function () {
        var complianceLevelNumberdColorMap = {};
        //prepare parameter for WFR AbCommonResources-getDataRecords
        var parameters = {
            tableName: 'regcomplevel',
            fieldNames: toJSON(['regcomplevel.comp_level', 'regcomplevel.level_number']),
            restriction: toJSON('1=1')
        };
        //get all compliance level records and put the data to map
        var result = Workflow.call('AbCommonResources-getDataRecords', parameters);
        if (result.code == "executed" && result.data.records.length > 0) {
            for ( var i = 0; i < result.data.records.length; i++ ) {
                complianceLevelNumberdColorMap[result.data.records[i]['regcomplevel.comp_level']]
                    = result.data.records[i]['regcomplevel.level_number.raw']
            }
        }
        //return the map
        return complianceLevelNumberdColorMap;
    },
    /**
     * set required parameters of this view.
     */
    setParameters: function () {
        var chart = this.abCompRptCommManChart;
        this.setQuery();
        chart.chartControl.chart.canvas.valueAxes[0].title = this.control.firstCalcField.title;
        chart.addParameter('firstGroupField', this.control.firstGroupField.name);
        chart.addParameter('secondGroupField', this.control.secondGroupField.name);
        chart.addParameter('firstGroupSortField', this.control.firstGroupSortField.name);
        chart.addParameter('secondGroupSortField', this.control.secondGroupSortField.name);
        chart.addParameter('calcField', this.control.firstCalcField.name);
        if (this.control.permanentParameter) {
            chart.addParameter('permanentParameter', this.control.permanentParameter);
        }
        chart.chartControl.chart.canvas.categoryAxis.title = this.control.firstGroupField.title;
    },
    /**
     * set database query base on the main table name.
     */
    setQuery: function () {
        var chart = this.abCompRptCommManChart;
        var mainTable = this.control.mainTable;
        var byLocation = this.control.byLocation;
        chart.addParameter('regrequirementQuery', false);
        chart.addParameter('regrequirementByLocationQuery', false);
        chart.addParameter('regulationQuery', false);
        chart.addParameter('regulationByLocationQuery', false);
        chart.addParameter('regprogramQuery', false);
        chart.addParameter('regprogramByLocationQuery', false);
        chart.addParameter('regviolationQuery', false);
        chart.addParameter('activityLogQuery', false);
        chart.addParameter('activityLogByLocationQuery', false);
        switch (mainTable) {
            case 'regrequirement':

                //qurery from table regrequirement
                if (byLocation) {
                    chart.addParameter('regrequirementByLocationQuery', true);
                } else {
                    chart.addParameter('regrequirementQuery', true);
                }
                break;
            case 'regulation':

                //qurery from table regulation
                if (byLocation) {
                    chart.addParameter('regulationByLocationQuery', true);
                } else {
                    chart.addParameter('regulationQuery', true);
                }
                break;
            case 'regprogram':
                if (byLocation) {
                    chart.addParameter('regprogramByLocationQuery', true);
                } else {
                    chart.addParameter('regprogramQuery', true);
                }
                break;
            case 'regviolation':
                //qurery from table regviolation
                chart.addParameter('regviolationQuery', true);
                break;
            case 'activity_log':
                if (byLocation) {
                    chart.addParameter('activityLogByLocationQuery', true);
                } else {
                    chart.addParameter('activityLogQuery', true);
                }
                break;
        }
    },
    /**
     * refresh chart from console filter.
     */
    refreshFromConsole: function () {
        if (this.consoleController) {
            //get chart panel
            var chart = this.abCompRptCommManChart;
            //set parameter to the chart panel
            if (this.consoleController.consoleRestriction) {
                var mainTable = this.control.mainTable;
                var byLocation = this.control.byLocation;
                //for special charts : Compliance Program Count by Compliance Level and Location and Compliance Requirements Count by Compliance Level and Location
                //the console restriction is divided to two parts: 1- console restriction excluding compliance level,
                //2, calculated compliance level field restriction
                if ((mainTable == 'regrequirement' || mainTable == 'regprogram') && byLocation) {
                    this.setQuery();
                    var consoleRestriction = this.consoleController.consoleRestriction;
                    if (valueExistsNotEmpty(this.control.permanentParameter)) {
                        consoleRestriction = consoleRestriction + " AND " + this.control.permanentParameter;
                    }
                    //add console restriction excluding compliance level to the query
                    chart.addParameter('queryConsoleRestriction', consoleRestriction);
                    chart.addParameter('permanentParameter', '1=1');
                    //add calculated compliance level field restriction to the parameter
                    chart.addParameter('consoleRestriction', this.consoleController.complianceLevelRestriction);
                }
                //for the other charts, only need one console restriction parameter
                else {
                    if (valueExistsNotEmpty(this.control.permanentParameter)) {
                        chart.addParameter('permanentParameter', this.control.permanentParameter);
                    }
                    chart.addParameter('consoleRestriction', this.consoleController.consoleRestriction);
                }
            }
            //check the chart type and switch between stackedBarChart chart and columnChart chart
            chart.checkChartType();
            chart.chartControl.chart.isSecondaryGroupingAdded = false;
            var canvas = chart.chartControl.chart.canvas;
            canvas.dataProvider = [];
            canvas.graphs = [];
            //refresh the chart data
            chart.refresh();
        }
    },
    /**
     * open the pop up view, and pass the clisck object as parameter.
     */
    openPopUpView: function (ob) {
        //call main controller method to open pup up
        if (this.mainController.openPopUpView) {
            this.mainController.openPopUpView(ob, this.control);
        }
    }
});

/**
 * onclick event handler for the chart panel.
 */
function onClickChart(ob) {
    //get field defines in the ManageReportControl object
    var firstGroupFieldDef = abCompRptCommManChartController[abCompRptCommManChartController.control.firstGroupFieldDef];
    var secondGroupFieldDef = abCompRptCommManChartController[abCompRptCommManChartController.control.secondGroupFieldDef];
    //if exist first field group field define, convert the display value to raw value in the click object base on enum list
    if (firstGroupFieldDef) {
        ob.selectedChartData['regloc.firstGroupField'] = getRawValueFromEnum(firstGroupFieldDef, ob.selectedChartData['regloc.firstGroupField']);
    }
    //if exist second field group field define, convert the display value to raw value in the click object base on enum list
    if (secondGroupFieldDef) {
        ob.selectedChartData['regloc.secondGroupField'] = getRawValueFromEnum(secondGroupFieldDef, ob.selectedChartData['regloc.secondGroupField']);
    }
    //open the pop up view
    abCompRptCommManChartController.openPopUpView(ob);
}

/**
 * convert the display value to raw value base on enum list
 */
function getRawValueFromEnum(fiedDef, displayedValue) {
    for ( var name in fiedDef.enumValues ) {
        if (fiedDef.enumValues[name] == displayedValue) {
            return name;
            break;
        }
    }
}

/**
 * over-write the getDataFromDataSources method in the core chart control to reset fill color and format the enum field values
 */
Ab.chart.HtmlChart.prototype.getDataFromDataSources = function (restriction) {
    try {
        var parameters = this.getParameters(restriction);
        //XXX: default time-out is 20S, but for charts, it should be longer (120S?)
        var result = Workflow.call(this.refreshWorkflowRuleId, parameters, 120);
        this.rawData = toJSON(result.data);
        //KB3036039 - assign colors to priority, severity, and compliance level (level_number) values the same way as in map views
        setCustomFillColor();
        //format data record values for the enum type fields
        formatDataRecords(result);
        //return the data
        return result.data;
    } catch (e) {
        this.handleError(e);
    }
}
/**
 * extend method resetFillColor in the core chart control to format the enum field values
 */
var setCustomFillColor = function () {
    var chart = abCompRptCommManChartController.abCompRptCommManChart;
    var customFillColors = [];
    //get second group field name
    var secondGroupFieldName = chart.parameters['secondGroupField'];
    var data = eval("(" + chart.rawData + ")");
    //if field name is compliance level, using abCompRptCommManChartController.complianceLevelFilledColorMap to get customized color
    if (secondGroupFieldName.indexOf('comp_level') != -1) {
        for ( var i = 0; i < data.length; i++ ) {
            for ( var key in data[i] ) {
                if (key.indexOf('firstGroupField') == -1) {
                    var levelNumber = abCompRptCommManChartController.complianceLevelNumberdColorMap[key];
                    //if the value in the map, get color from the map, otherwise set to black(no value)
                    if (abCompRptCommManChartController.complianceLevelFilledColorMap[levelNumber]) {
                        customFillColors.push(abCompRptCommManChartController.complianceLevelFilledColorMap[levelNumber]);
                    } else {
                        customFillColors.push('#2B2B2B');
                    }
                }
            }
        }
        //set customized color to the chart control
        chart.chartControl.chart.config.colors = customFillColors;
        //if field name is priority or severity, using abCompRptCommManChartController.priorityAndSeverityFilledColorMap to get customized color
    } else if (secondGroupFieldName.indexOf('priority') != -1 || secondGroupFieldName.indexOf('severity') != -1 || secondGroupFieldName.indexOf('reg_rank') != -1) {
        for ( var i = 0; i < data.length; i++ ) {
            for ( var key in data[i] ) {
                if (key.indexOf('firstGroupField') == -1) {
                    //if the value in the map, get color from the map, otherwise set to black(no value)
                    if (abCompRptCommManChartController.priorityAndSeverityFilledColorMap[key]) {
                        customFillColors.push(abCompRptCommManChartController.priorityAndSeverityFilledColorMap[key]);
                    } else {
                        customFillColors.push('#2B2B2B');
                    }
                }
            }
        }
        //set customized color to the chart control
        chart.chartControl.chart.config.colors = customFillColors;
    }
}
/**
 * extend method formatDataRecords in the core chart control to format the enum field values
 */
var formatDataRecords = function (result) {
    //get field defines in the ManageReportControl object
    var firstGroupFieldDef = abCompRptCommManChartController[abCompRptCommManChartController.control.firstGroupFieldDef];
    var secondGroupFieldDef = abCompRptCommManChartController[abCompRptCommManChartController.control.secondGroupFieldDef];
    //if exist second field group field define, convert the raw value to display value in the click object base on enum list
    if (secondGroupFieldDef) {
        for ( var i = 0; i < result.data.length; i++ ) {
            var formatedData = {};
            var data = result.data[i];
            for ( var key in data ) {
                if (key.indexOf('firstGroupField') == -1) {
                    formatedData[secondGroupFieldDef.formatValue(key)] = data[key];
                } else {
                    formatedData[key] = data[key];
                }
            }
            result.data[i] = formatedData;
        }
    }
    //if exist first field group field define, convert the raw value to display value in the click object base on enum list
    if (firstGroupFieldDef) {
        for ( var i = 0; i < result.data.length; i++ ) {
            result.data[i]['regloc.firstGroupField'] = firstGroupFieldDef.formatValue(result.data[i]['regloc.firstGroupField']);
        }
    }
}
/**
 * Check and set chart type -- stackedBarChart chart and columnChart chart
 */
Ab.chart.HtmlChart.prototype.checkChartType = function () {
    if (this.configObj.chartType !== abCompRptCommManChartController.control.chartType) {
        //reset the chart control type
        this.configObj.setChartType(abCompRptCommManChartController.control.chartType);
        this.chartControl.build();
    }
};