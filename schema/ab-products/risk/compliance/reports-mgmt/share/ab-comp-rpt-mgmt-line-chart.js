/**
 * @author Guo Jiangtao
 */
var abCompRptCommManLineChartController = View.createController('abCompRptCommManLineChartController', {

	/**
	 * the main view controller which contain this common chart view
	 */
	mainController : null,
	
	/**
	 * the console view controller
	 */
	consoleController : null,

	/**
	 * type of ManageReportControl
	 */
	control : null,
	
	/**
	 * severity field define
	 */
	severityFiledDef : null,

	/**
	 * reset the panel title after the chart loaded.
	 */
	afterInitialDataFetch : function() {
		//get severity field define from the datasource
		this.severityFiledDef = View.dataSources.get('abCompRptCommManChart_fieldDef_DS').fieldDefs.get('regviolation.severity');
		
		// get console view controller from opener view
		this.consoleController = View.getOpenerView().controllers.get('controllerConsole');
		//register to console controller
		if (this.consoleController) {
			this.consoleController.controllers.push(this);
		}
		
		// get main controller from opener view
		this.mainController = View.getOpenerView().controllers.get(0);

		// get property value from main controller
		if (this.mainController && this.mainController.controls) {

			var controls = this.mainController.controls;
			for ( var i = 0; i < controls.length; i++) {
				if (controls[i].type == 'lineChart' && !controls[i].isLoad) {
					this.control = controls[i];
					break;
				}
			}
			
			if(this.mainController.beforeControlLoad){
				this.mainController.beforeControlLoad(this.control,this.abCompRptCommManChart,abCompRptCommManLineChartController);
			}
		}
		
		// set all parameters
		this.setParameters();
		
		// reset the chart panel title
		this.abCompRptCommManChart.config.title = this.control.title

		// refresh and show chart
		if(this.consoleController.consoleRestriction){
			// fix KB3035927 - refreshed according to the console restrictions when switch from one tab to anothe
			this.refreshFromConsole();
		}else{
			this.abCompRptCommManChart.refresh();
		}

		//set isload flag
		this.control.isLoad = true;
		
		var chart = this.abCompRptCommManChart
		chart.chartControl.addEventListener("clickGraphItem", function(event) {
        	var selectedChartData = event.item.dataContext;
        	selectedChartData['regloc.firstGroupField'] = event.item.category;
        	selectedChartData['regloc.secondGroupField'] = event.item.graph.valueField;
        	var clickObject = {'selectedChartData': selectedChartData};
        	onClickChart(clickObject);
        });
	},

	/**
	 * set required parameters of this view.
	 */
	setParameters : function() {
		var chart = this.abCompRptCommManChart;
		chart.chartControl.chart.canvas.valueAxes[0].title = this.control.firstCalcField.title;
		chart.addParameter('firstGroupField', this.control.firstGroupField.name);
		chart.addParameter('firstGroupField', this.control.firstGroupField.name);
		chart.addParameter('firstGroupSortField', this.control.firstGroupSortField.name);
		chart.addParameter('calcField', this.control.firstCalcField.name);
		
		if(this.control.permanentParameter){
			chart.addParameter('permanentParameter', this.control.permanentParameter);
		}
		
		chart.chartControl.chart.canvas.categoryAxis.title = this.control.firstGroupField.title;
	},
	
	/**
	 * refresh chart from console filter.
	 */
	refreshFromConsole: function() {
		if(this.consoleController){
			var chart = this.abCompRptCommManChart;
			
			//set parameter to the chart panel
			if(this.consoleController.consoleRestriction){
				chart.addParameter('consoleRestriction', this.consoleController.consoleRestriction);
			}
			
			chart.refresh();
		}
	},
	
	openPopUpView: function(ob) {
		if(this.mainController.openPopUpView){
			this.mainController.openPopUpView(ob,this.control);
		}
	}
});

/**
 * on click event handler of the chart.
 */
function onClickChart(ob){
	
	//get field define of first group field and second field define
	var firstGroupFieldDef = abCompRptCommManLineChartController[abCompRptCommManLineChartController.control.firstGroupFieldDef];
	var secondGroupFieldDef = abCompRptCommManLineChartController[abCompRptCommManLineChartController.control.secondGroupFieldDef];
	
	//convert the display value to raw value for first group field 
	if(firstGroupFieldDef){
		ob.selectedChartData['regloc.firstGroupField'] = getRawValueFromEnum(firstGroupFieldDef,ob.selectedChartData['regloc.firstGroupField']);
	}
	
	//convert the display value to raw value for second group field 
    if(secondGroupFieldDef){
    	ob.selectedChartData['regloc.secondGroupField'] = getRawValueFromEnum(secondGroupFieldDef,ob.selectedChartData['regloc.secondGroupField']);
	}
    
    //open pop up
	abCompRptCommManLineChartController.openPopUpView(ob);
	
}

/**
 * get the raw value base on the display value.
 */
function getRawValueFromEnum(fiedDef, displayedValue){
	
	for(var name in fiedDef.enumValues){
		if(fiedDef.enumValues[name] == displayedValue){
			return name;
			break;
		}
	}
	
}

/**
 * over-write the getDataFromDataSources method in the core chart control to reset fill color and format the enum field values
 */
Ab.chart.HtmlChart.prototype.getDataFromDataSources = function(restriction){	 
	try {
	    var parameters = this.getParameters(restriction);
	    //XXX: default time-out is 20S, but for charts, it should be longer (120S?)
        var result = Workflow.call(this.refreshWorkflowRuleId, parameters, 120);
        
        //format data record values for the enum type fields
        formatDataRecords(result);
        
        //return the data
        return result.data;
	} catch (e) {
		this.handleError(e);
	}
 }

/**
 * extend method formatDataRecords in the core chart control to format the enum field values
 */
var formatDataRecords =  function(result) {
	//get field defines in the ManageReportControl object
	var firstGroupFieldDef = abCompRptCommManLineChartController[abCompRptCommManLineChartController.control.firstGroupFieldDef];
	var secondGroupFieldDef = abCompRptCommManLineChartController[abCompRptCommManLineChartController.control.secondGroupFieldDef];
	
	//if exist second field group field define, convert the raw value to display value in the click object base on enum list  
	if(secondGroupFieldDef){
		for(var i=0; i<result.data.length;i++){
			var formatedData = {};
			var data = result.data[i];
			for(var key in data){
				if(key.indexOf('firstGroupField') == -1){
					formatedData[secondGroupFieldDef.formatValue(key)] = data[key];
				}else{
					formatedData[key] = data[key];
				}
			}
			
			result.data[i] = formatedData;
		}
	}
	
	//if exist first field group field define, convert the raw value to display value in the click object base on enum list  
    if(firstGroupFieldDef){
    	for(var i=0; i<result.data.length;i++){
    		result.data[i]['regloc.firstGroupField'] = firstGroupFieldDef.formatValue(result.data[i]['regloc.firstGroupField']);
		}
	}
}