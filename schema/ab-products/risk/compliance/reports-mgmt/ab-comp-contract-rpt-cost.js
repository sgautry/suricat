var abCompContractRptCost = View.createController('abCompContractRptCost', {
    afterInitialDataFetch: function () {
        // get console view controller from opener view
        this.consoleController = View.controllers.get("controllerConsole");
        if (!this.consoleController) {
            this.consoleController = View.getOpenerView().controllers.get('controllerConsole');
        }
        //register to console controller
        if (this.consoleController) {
            this.consoleController.controllers.push(this);
        }
    },
    refreshFromConsole: function () {
        if (this.consoleController) {
            var grid = this.contractCostsGrid;
            var res = "compliance_contract_cost.reg_program = '" + this.consoleController.programParam + "'";
            if (this.consoleController.isMcAndVatEnabled) {
                res += " AND ((compliance_contract_cost.unit_count * compliance_contract_cost.cost_unit_comm_total_payment) + compliance_contract_cost.cost_comm_total_payment  ) > 0";
            } else {
                res += " AND (compliance_contract_cost.unit_count * compliance_contract_cost.cost_unit_comm_budget) + compliance_contract_cost.cost_comm_budget > 0";
            }
            if(this.consoleController.displayCurrency.type == 'budget'){
            	this.consoleController.displayCurrency.type = "custom";
            }
            var contractRes = res + " AND compliance_contract_cost.reg_requirement is NULL AND compliance_contract_cost.location_id is NULL";
            grid.addParameter('exchangeRate', getExchangeRateParameter(this.consoleController, "compliance_contract_cost", "date_used_for_mc_budget"));
            grid.addParameter('currencyCode', this.consoleController.displayCurrency.code);
            grid.addParameter('consoleRestriction', contractRes);
            grid.refresh();

            grid = this.contractTermCostsGrid;
            var termRes = res + " AND compliance_contract_cost.reg_requirement IS NOT NULL AND compliance_contract_cost.location_id is NULL ";
            grid.addParameter('exchangeRate', getExchangeRateParameter(this.consoleController, "compliance_contract_cost", "date_used_for_mc_budget"));
            grid.addParameter('currencyCode', this.consoleController.displayCurrency.code);
            grid.addParameter('consoleRestriction', termRes);
            grid.refresh();

            grid = this.contractLocationCostsGrid;
            var locRes = res + " AND compliance_contract_cost.location_id IS NOT NULL ";
            grid.addParameter('exchangeRate', getExchangeRateParameter(this.consoleController, "compliance_contract_cost", "date_used_for_mc_budget"));
            grid.addParameter('currencyCode', this.consoleController.displayCurrency.code);
            grid.addParameter('consoleRestriction', locRes);
            grid.refresh();

            grid = this.contractViolationGrid;
            grid.addParameter('exchangeRate', getExchangeRateParameter(this.consoleController, "regviolation", "date_used_for_mc_budget"));
            grid.addParameter('currencyCode', this.consoleController.displayCurrency.code);
            var violationRes = "regviolation.reg_program = '" + this.consoleController.programParam + "'";
            grid.refresh(violationRes);

            // update panels scroller
            this.contractCostsGrid.updateHeight();
            this.contractTermCostsGrid.updateHeight();
            this.contractLocationCostsGrid.updateHeight();
            this.contractViolationGrid.updateHeight();
        }
    }
});
