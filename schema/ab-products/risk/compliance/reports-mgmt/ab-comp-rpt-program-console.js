var regprogramConsoleController = View.createController('controllerConsole', {

	controllers : [],

	parameters : null,
	
	consoleRestriction: " 1=1 ",
	
	subQueryConsoleRestriction:' 1=1 ',
	
	programParam: '',
	
	reglocFieldsArraysForRes : new Array(),

	regulationFieldsArray : new Array( ['regprogram.regulation','=','regulation.regulation'],['regulation.reg_class'], ['regulation.reg_cat'], ['regulation.reg_type'], ['regulation.authority'], ['regulation.reg_rank']),

	regprogramFieldsArrays : new Array(['regprogram.reg_program'], ['regprogram.project_id'],['regprogram.regprog_cat'], ['regprogram.regprog_type'], ['regprogram.priority'], ['regprogram.status'],
		['regprogram.project_id']
	),
	
	regrequirementFieldsArray : new Array(),
	
	respPersonAndVn:new Array(['regloc.resp_person','=','regprogram.em_id'],['regloc.vn_id','=','regprogram.vn_id']),
	
	respPersonAndVnForReq:new Array(['regloc.resp_person','=','regrequirement.em_id'],['regloc.vn_id','=','regrequirement.vn_id']),
	
	// currency selection
	displayCurrency: {
		type: '',
		code: '',
		exchangeRateType: ''
	},
	
	// Activity parameter EnableVatAndMultiCurrency
	isMcAndVatEnabled: false,
	
	afterViewLoad : function(){
		this.isMcAndVatEnabled = (View.activityParameters["AbCommonResources-EnableVatAndMultiCurrency"] == 1);
        if (this.isMcAndVatEnabled) {
        	this.displayCurrency.type = 'budget';
        	this.displayCurrency.code = this.view.project.budgetCurrency.code;
        	this.displayCurrency.exchangeRateType = 'Budget';
        }
	},
	
	/**
	 * get restriction from console values.
	 */
	getConsoleRestriction : function() {
				
		//get regulation restriction		
		var regulationRestriction = getRestrictionStrFromConsole(this.abCompDrilldownConsole, this.regulationFieldsArray);	
		
		//get regprogram restriction
		var regprogramRestriction = getRestrictionStrFromConsole(this.abCompDrilldownConsole, this.regprogramFieldsArrays);
	
		//get Responsible Person and Vendor Code restriction
		var responsiblePersonAndVnRestriction = this.getResponsiblePersonAndVnRestriction();
	
		//get location restriction
		var locationRestriction = this.getLocationRestriction();
		
		//get compliance level restriction
		var complianceLevelRestriction = this.getComplianceLevelRestriction();
		
		this.programParam = this.abCompDrilldownConsole.getFieldValue("regprogram.reg_program");
		
		this.consoleRestriction = regulationRestriction + ' AND ' + regprogramRestriction + ' AND ' + responsiblePersonAndVnRestriction  + ' AND ' + locationRestriction + ' AND ' + complianceLevelRestriction;

		
		var subRequirementRes=getRestrictionStrFromConsole(this.abCompDrilldownConsole, this.regrequirementFieldsArray);;
		var subRequirementVnRes=getRestrictionStrFromConsole(this.abCompDrilldownConsole,this.respPersonAndVnForReq);
		var subLocationRes=this.getLocationRestrictionForSubQuery();
		var subCompLevelRes=this.getSubComplianceLevelRestriction();
		
		this.subQueryConsoleRestriction=subRequirementRes+" AND "+subRequirementVnRes+" AND "+subLocationRes+" AND "+subCompLevelRes;
	
	},
	
	abCompDrilldownConsole_onClear : function() {
		this.abCompDrilldownConsole.clear();
		clearConsoleFields();
	},
	
	/**
	 * Get location restriction from console values.
	 */
	getLocationRestrictionForSubQuery : function() {
		
		var locationRestriction = ' 1=1 ';

		if(View.locationRestriction){
			locationRestriction = ' EXISTS(select 1 FROM regloc left join compliance_locations on regloc.location_id = '
				+ 'compliance_locations.location_id WHERE  ' 
				+ '  regloc.regulation = regrequirement.regulation ' 
				+ '  AND regloc.reg_program = regrequirement.reg_program ' 
				+ '  AND regloc.reg_requirement = regrequirement.reg_requirement ' 
				+ View.locationRestriction+')';
		}
		
		return locationRestriction;
		
	},
	
	/**
	 * get location restriction from console values.
	 */
	getLocationRestriction : function() {
		
		var locationRestriction = ' 1=1 ';

		if(View.locationRestriction){
			locationRestriction = ' exists(select 1 from regloc left join compliance_locations on regloc.location_id = '
				+ 'compliance_locations.location_id where  ' 
				+ '  regloc.regulation = regprogram.regulation ' 
				+ ' AND regloc.reg_program = regprogram.reg_program ' 
				+ View.locationRestriction+')';
		}
		
		return locationRestriction;
		
	},
	
	/**
	 * get compliance level restriction from console values.
	 */
	getComplianceLevelRestriction : function() {
		
		var calculatedField = ' ${sql.isNull(\'(case when regprogram.comp_level IS NULL THEN regprogram.comp_level_calc ELSE regprogram.comp_level END)\', "\'Not Entered\'")} ';
		
		var complianceLevelRestriction = getRestrictionStrFromConsole(this.abCompDrilldownConsole,
                new Array(['regloc.comp_level','=',calculatedField]));
		
		return complianceLevelRestriction;
		
	},
	
	/**
	 * get compliance level restriction from console values.
	 */
	getSubComplianceLevelRestriction : function() {
		
		var calculatedField = ' ${sql.isNull(\'(case when regrequirement.comp_level IS NULL THEN regprogram.comp_level ELSE  regprogram.comp_level END)\', "\'Not Entered\'")} ';
		
		var complianceLevelRestriction = getRestrictionStrFromConsole(this.abCompDrilldownConsole,
                new Array(['regloc.comp_level','=',calculatedField]));
		
		return complianceLevelRestriction;
		
	},
	
	
	/**
	 * get Responsible Person and Vendor Code restriction from console values.
	 */
	getResponsiblePersonAndVnRestriction : function() {
		
		return getRestrictionStrFromConsole(this.abCompDrilldownConsole,this.respPersonAndVn);
		 
	},
	
	/**
	 * get regrequirement restriction
	 */
	getRequirementRestriction : function() {
		return '1=1';		 
	},

	/**
	 * event handle when show button click.
	 */
	abCompDrilldownConsole_onShow : function() {
		//get console restriction
		if(!valueExistsNotEmpty(this.abCompDrilldownConsole.getFieldValue('regprogram.reg_program'))){
			View.showMessage(getMessage('requiredField'));
			return;
		}
		this.getConsoleRestriction();
		

		if(valueExists(this.abCompDrilldownConsole.displayCurrency)){
			this.displayCurrency = this.abCompDrilldownConsole.displayCurrency;
		}
		
		//refresh all controls that register to this console
		for ( var i = 0; i < this.controllers.length; i++) {
			this.controllers[i].refreshFromConsole();
		}
	}

});


function selectProgram(formId, tableName,isMultiple, afterSelectLisenter, isContract){
	var form = View.panels.get(formId);

	//Construct restriction from value of regulation
	var res =  " 1=1 ";

	
	var controller = View.controllers.get("controllerConsole");
	controller.getConsoleRestriction();
	
	res = controller.consoleRestriction;
	
	if(valueExistsNotEmpty(isContract)){
		res += " AND regprogram.is_contract = ";
		res += (isContract) ? "'1'" : "'0'";
	}

	// ordered by regulation ascending
	var sortValues = [];
	sortValues.push( {
		fieldName : 'regprogram.regulation',
		sortOrder : 1
	});
	sortValues.push( {
		fieldName : 'regprogram.reg_program',
		sortOrder : 1
	});

	View.selectValue({
		title:getMessage("selProg"),
		formId: formId,
		restriction: res,
		selectValueType: isMultiple?"multiple":null,
		fieldNames: ['regprogram.regulation', 'regprogram.reg_program','regprogram.regprog_type', 'regprogram.regprog_cat','regprogram.status','regulation.reg_cat', 'regulation.reg_type', 'regulation.authority','regulation.reg_rank','regulation.reg_class'],
		selectTableName : 'regprogram',
		selectFieldNames: ['regprogram.regulation', 'regprogram.reg_program','regprogram.regprog_type', 'regprogram.regprog_cat','regprogram.status','regulation.reg_cat', 'regulation.reg_type', 'regulation.authority','regulation.reg_rank','regulation.reg_class'],
		actionListener: afterSelectLisenter?afterSelectLisenter:null,
		sortValues: toJSON(sortValues),
		visibleFields: [
			{fieldName: 'regprogram.regulation'},
			{fieldName: 'regprogram.reg_program'},
			{fieldName: 'regprogram.regprog_type'},
			{fieldName: 'regprogram.regprog_cat'},
			{fieldName: 'regprogram.status'},
			{fieldName: 'regprogram.summary'},
			{fieldName: 'regulation.reg_cat'},
			{fieldName: 'regulation.reg_type'},
			{fieldName: 'regulation.authority'},
			{fieldName: 'regulation.reg_rank'},
			{fieldName: 'regulation.reg_class'}
		]
	});
}