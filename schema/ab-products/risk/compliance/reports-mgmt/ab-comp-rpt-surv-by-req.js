var abCompEventSelectController = View.createController('abCompEventSelectController', {
    //Restriction for several 'My ***' view and reports
    treeRes: " 1=1 ",
    //Restriction for several 'My ***' view and reports
    myRes: " 1=1 ",
    //Event restriction from table activity_log
    eventRes: " 1=1 ",
    eventFieldsArraysForRes: [['activity_log.manager'], ['activity_log.action_title', 'like']],
    //Requirement restriction from table regrequirement
    reqRes: " 1=1 ",
    requirementFieldsArraysForRes: [['regrequirement.regreq_type'], ['regrequirement.regreq_cat'], ['regrequirement.reg_requirement']],
    //Program restriction from table regprogram
    progRes: " 1=1 ",
    programFieldsArraysForRes: [['regrequirement.reg_program', 'regprogram.reg_program'], ['regprogram.regprog_cat'], ['regprogram.regprog_type'], ['regprogram.project_id']],
    //Regulation restriction from table regulation
    regRes: " 1=1 ",
    regulationFieldsArraysForRes: [['regrequirement.regulation', 'regulation.regulation'], ['regulation.reg_rank']],
    //Answer date restriction
    dateRes: " 1=1 ",
    //Location restriction from table regloc
    locRes: " 1=1 ",
    afterViewLoad: function () {
        //disable column filter
        this.surveyResultsMultiChoiceSingleGrid.columns[this.surveyResultsMultiChoiceSingleGrid.findColumnIndex('quest_answer_ext.percentage')].filterEnabled = false;
        this.surveyResultsMultiChoiceMultipleGrid.columns[this.surveyResultsMultiChoiceMultipleGrid.findColumnIndex('quest_answer_option_ext.percentage')].filterEnabled = false;
        this.surveyResultsMultiChoiceSingleGrid.afterCreateCellContent = function (row, column, cellElement) {
            abCompEventSelectController.customizeMultiChoiceSingleColumns(row, column, cellElement);
        };
    },
    customizeMultiChoiceSingleColumns: function (row, column, cellElement) {
        if (column.id === 'quest_answer_ext.percentage') {
            row.grid.getDataSource().addParameter('surveyReportRestriction', row.grid.parameters['surveyReportRestriction']);
            var records = row.grid.getDataSource().getRecords();
            var total = 0;
            for ( var i = 0; i < records.length; i++ ) {
                total += parseInt(records[i].getValue('quest_answer_ext.answer_count'));
            }
            var answerCount = parseInt(row.row.getRecord().getValue('quest_answer_ext.answer_count'));
            cellElement.innerHTML = localizeDecimalSeparator((answerCount * 100 / total).toFixed(2)) + '%';
        }
    },
    /**
     * Event handler for action "Show" of console.
     */
    surveyEventBasicConsole_onShow: function () {
        var console = this.surveyEventBasicConsole;
        //get normal restriction by event, requirement, program and regulation from console
        this.eventRes = getRestrictionStrFromConsole(console, this.eventFieldsArraysForRes);
        this.reqRes = getRestrictionStrFromConsole(console, this.requirementFieldsArraysForRes);
        this.progRes = getRestrictionStrFromConsole(console, this.programFieldsArraysForRes);
        this.regRes = getRestrictionStrFromConsole(console, this.regulationFieldsArraysForRes);
        var date_start = console.getFieldValue('quest_answer_ext.date_from');
        var date_end = console.getFieldValue('quest_answer_ext.date_to');
        this.dateRes = " 1=1 ";
        if (valueExistsNotEmpty(date_start)) {
            this.dateRes += " AND quest_answer_ext.date_recorded >= ${sql.date('" + date_start + "')} ";
        }
        if (valueExistsNotEmpty(date_end)) {
            this.dateRes += " AND quest_answer_ext.date_recorded <= ${sql.date('" + date_end + "')} ";
        }
        //Compliance Level: WHERE regrequirement.comp_level IN (list) OR (regrequirement.comp_level IS NULL AND regprogram.comp_level IN (list))
        var compLevel = this.surveyEventBasicConsole.getFieldValue("regrequirement.comp_level");
        if (compLevel) {
            var levelRes = getMultiSelectFieldRestriction(new Array(['regrequirement.comp_level']), compLevel);
            levelRes = " ( " + levelRes + " or  regrequirement.comp_level IS NULL AND " + levelRes.replace("regrequirement", "regprogram") + "  ) ";
            this.reqRes = this.reqRes + " and " + levelRes;
        }
        //get proper location restriction
        if (View.locationRestriction) {
            this.locRes = " exists (select 1 from compliance_locations "
                + "where activity_log.location_id = compliance_locations.location_id "
                + View.locationRestriction + ")";
        }
        this.onRefresh(this);
    },
    /**
     * Event handler for action "Clear" of console.
     */
    surveyEventBasicConsole_onClear: function () {
        this.surveyEventBasicConsole.clear();
        $("virtual_location").value = "";
        View.locationRestriction = "";
    },
    /**
     *  Public function: refresh the tree and grid
     */
    onRefresh: function (controller) {
        var regRes = ( controller ? controller.regRes : "1=1" );
        var progRes = ( controller ? controller.progRes : "1=1" );
        var reqRes = ( controller ? controller.reqRes : "1=1" );
        var locRes = ( controller ? controller.locRes : "1=1" );
        var eventRes = ( controller ? controller.eventRes : "1=1" );
        var dateRes = ( controller ? controller.dateRes : "1=1" );
        //Set SQL Parameter values to tree
        this.tree.addParameter("noReg", getMessage('noReg'));
        this.tree.addParameter("noProg", getMessage('noProg'));
        this.tree.addParameter("noReq", getMessage('noReq'));
        this.tree.addParameter("regRes", regRes);
        this.tree.addParameter("progRes", progRes);
        this.tree.addParameter("reqRes", reqRes);
        this.tree.addParameter("locRes", locRes);
        this.tree.addParameter("eventRes", eventRes);
        this.tree.addParameter("dateRes", dateRes);
        this.tree.refresh();
        this.questionHierTree.addParameter('complianceTreeRestriction', " 1 = 2 ");
        this.questionHierTree.refresh();
        hideAllReports();
    },
    /*
     * special behaviors for refreshing the hierarchy tree
     */
    questionHierTree_onRefresh: function () {
        this.questionHierTree.addParameter('multipleValueSeparator', Ab.form.Form.MULTIPLE_VALUES_SEPARATOR);
        this.questionHierTree.refresh();
        var treePanel = View.panels.get('questionHierTree');
        onTreeExpandAll(treePanel, null);
        if (this.isHeader) {
            View.panels.get('questionHierTree').show(false, false);
            this.questionHierTree.setTitle('');
        } else {
            View.panels.get('questionHierTree').show(true, true);
        }
    }
});

/**
 * Public function: called when tree node is clicked, refresh the grid by applying restriction from clicked requirement.
 */
function onReqClick() {
    var ctrl = abCompEventSelectController;
    var currentNode = abCompEventSelectController.tree.lastNodeClicked;
    var regulation = currentNode.data['regrequirement.regulation'];
    var program = currentNode.data['regrequirement.reg_program'];
    var requirement = currentNode.data['regrequirement.reg_requirement'];
    if (regulation === getMessage("noReg")) {
        ctrl.treeRes = " activity_log.regulation IS NULL ";
    } else {
        ctrl.treeRes = " activity_log.regulation='" + regulation + "' ";
    }
    if (program === getMessage("noProg")) {
        ctrl.treeRes += " AND activity_log.reg_program IS NULL ";
    } else {
        ctrl.treeRes += " AND activity_log.reg_program='" + program + "' ";
    }
    if (requirement === getMessage("noReq")) {
        ctrl.treeRes += " AND activity_log.reg_requirement IS NULL ";
    } else {
        ctrl.treeRes += " AND activity_log.reg_requirement='" + requirement + "' ";
    }
//	ctrl.abSurveyEventAnswersActivityLogDs.refresh(ctrl.treeRes);
//	hideEmptyColumnsByPrefix(ctrl.abCompEventActivityLogGrid, "compliance_locations.");
    ctrl.questionHierTree.addParameter('complianceTreeRestriction', ctrl.treeRes);
    ctrl.questionHierTree.refresh();
}

/**
 * Overwrite:  each time the tree node is clicked, generate restriction for lower level children nodes.
 *
 * @param parentNode
 * @param level
 *
 * @returns restrictions for next level nodes
 */
function createRestrictionForLevel(parentNode, level) {
    var restriction = null;
    if (level === 1) {
        restriction = new Ab.view.Restriction();
        restriction.addClause('regprogram.regulation', parentNode.data['regulation.regulation'], '=', 'AND', true);
    }
    if (level === 2) {
        restriction = new Ab.view.Restriction();
        restriction.addClause('regrequirement.regulation', parentNode.data['regprogram.regulation'], '=', 'AND', true);
        restriction.addClause('regrequirement.reg_program', parentNode.data['regprogram.reg_program'], '=', 'AND', true);
    }
    return restriction;
}

/**
 * Event handler for click events in questions tree
 *
 */
function questionHierTree_onSelectQuestion() {
    var controller = abCompEventSelectController;
    var centerLayout = View.getLayoutManager("nestedCenterLayout");
    var tree = View.panels.get('questionHierTree');
    var lastNodeClicked = tree.lastNodeClicked;
    var question_id = lastNodeClicked.data['question_ext.question_id'];
    var restriction = '1 = 1';
    restriction += " AND quest_answer_ext.question_id = '" + question_id + "'";
    restriction += " AND " + controller.dateRes;
    var eventRecordsList = getRestrictedEventIds();
    restriction += (eventRecordsList.length > 0) ? " AND quest_answer_ext.survey_event_id IN (" + eventRecordsList.toString() + ")" : "";
    //hide all reporting panels
    hideAllReports();
    var questionType = lastNodeClicked.data['question_ext.question_type.raw'];
    switch (questionType) {
        case 'multiple_choice_single':
            controller.surveyResultsMultiChoiceSinglePieChart.addParameter('surveyReportRestriction', restriction);
            controller.surveyResultsMultiChoiceSinglePieChart.refresh();
            controller.surveyResultsMultiChoiceSinglePieChart.syncHeight();
            controller.surveyResultsMultiChoiceSingleGrid.addParameter('surveyReportRestriction', restriction);
            controller.surveyResultsMultiChoiceSingleGrid.refresh();
            centerLayout.expandRegion('south');
            break;
        case 'multiple_choice_multi':
            controller.surveyResultsMultiChoiceMultipleGrid.show(true);
            controller.abCompRptSurvByReqMultiChoiceMultipleDs.addParameter('surveyReportRestriction', restriction);
            var records = controller.abCompRptSurvByReqMultiChoiceMultipleDs.getRecords();
            var count = {}, totalNumber = 0;
            if (records.length>0){
	            for ( var i = 0; i < records.length; i++ ) {
	                var answer = records[i].getValue('quest_answer_ext.value_text').split(Ab.form.Form.MULTIPLE_VALUES_SEPARATOR);
	                for ( var j = 0; j < answer.length; j++ ) {
	                    totalNumber++;
	                    var answers = valueExistsNotEmpty(answer[j]) ? parseInt(answer[j]) : 0;
	                    if (valueExistsNotEmpty(count[answers])) {
	                        count[answers]++;
	                    } else {
	                        count[answers] = 1;
	                    }
	                }
	            }
	            var questionRestriction = new Ab.view.Restriction();
	            questionRestriction.addClause('quest_answer_option_ext.question_id', question_id);
	            var virtualFields = controller.abCompRptSurvVirtualDs.getRecords(questionRestriction);
	            for ( var i = 0; i < virtualFields.length; i++ ) {
	                var value = (valueExistsNotEmpty(count[virtualFields[i].getValue('quest_answer_option_ext.answer_option_id')])) ? (count[virtualFields[i].getValue('quest_answer_option_ext.answer_option_id')]) : 0;
	                virtualFields[i].setValue("quest_answer_option_ext.answer_count", value);
	                virtualFields[i].setValue("quest_answer_option_ext.percentage", localizeDecimalSeparator((value * 100 / totalNumber).toFixed(2)) + '%');
	            }
	            if (valueExistsNotEmpty(count[0])) {
	                var record = new Ab.data.Record({
	                    'quest_answer_option_ext.answer_option_text': getMessage('noAnswer'),
	                    'quest_answer_option_ext.answer_count': count[0],
	                    'quest_answer_option_ext.percentage': localizeDecimalSeparator((count[0] * 100 / totalNumber).toFixed(2)) + '%'
	                }, true);
	                virtualFields.push(record);
	            }
	            controller.surveyResultsMultiChoiceMultipleGrid.show(true);
	            if (virtualFields.length > 0) {
	                controller.surveyResultsMultiChoiceMultipleGrid.hasNoRecords = false;
	            }
	            controller.surveyResultsMultiChoiceMultipleGrid.setRecords(virtualFields);
            }
            else {
            	controller.surveyResultsMultiChoiceMultipleGrid.refresh('1=2');
            }
            centerLayout.collapseRegion('south');
            break;
        case 'freeform' :
            controller.surveyResultsFreeGrid.refresh(restriction);
            centerLayout.collapseRegion('south');
            break;
        case 'date' :
            controller.surveyResultsDateColumnChart.addParameter('surveyReportRestriction', restriction);
            controller.surveyResultsDateColumnChart.refresh();
            controller.surveyResultsDateColumnChart.syncHeight();
            centerLayout.collapseRegion('south');
            break;
        case 'time' :
            controller.surveyResultsTimeColumnChart.addParameter('surveyReportRestriction', restriction);
            controller.surveyResultsTimeColumnChart.refresh();
            controller.surveyResultsDateColumnChart.syncHeight();
            centerLayout.collapseRegion('south');
            break;
        case 'lookup' :
            controller.surveyResultsLookupPieChart.addParameter('surveyReportRestriction', restriction);
            controller.surveyResultsLookupPieChart.refresh();
            controller.surveyResultsDateColumnChart.syncHeight();
            controller.surveyResultsLookupGrid.addParameter('surveyReportRestriction', restriction);
            controller.surveyResultsLookupGrid.refresh();
            centerLayout.expandRegion('south');
            break;
        case 'count' :
            controller.surveyResultsCountStatsGrid.addParameter('surveyReportRestriction', restriction);
            controller.surveyResultsCountStatsGrid.refresh();
            centerLayout.collapseRegion('south');
            break;
        case 'measurement' :
            controller.surveyResultsMeasurementStatsGrid.addParameter('surveyReportRestriction', restriction);
            controller.surveyResultsMeasurementStatsGrid.refresh();
            centerLayout.collapseRegion('south');
            break;
        case 'document' :
            controller.surveyResultsDocsGrid.refresh(restriction);
            centerLayout.collapseRegion('south');
            break;
        case 'action' :
            //
            break;
        default :
            //
            break;
    }
}

/**
 * Public function: called when tree node is clicked, refresh the grid by applying restriction from clicked requirement.
 */
function getRestrictedEventIds() {
    var ctrl = abCompEventSelectController;
    var recordsSet = ctrl.abSurveyEventAnswersActivityLogDs.getRecords(ctrl.treeRes);
    var eventIdList = [];
    for ( var i = 0; i < recordsSet.length; i++ ) {
        var record = recordsSet[i];
        eventIdList.push(record.getValue('activity_log.activity_log_id'));
    }
    return eventIdList;
}

function hideAllReports() {
    var controller = abCompEventSelectController;
    controller.surveyResultsMultiChoiceSinglePieChart.show(false);
    controller.surveyResultsMultiChoiceSingleGrid.show(false);
    controller.surveyResultsMultiChoiceMultipleGrid.show(false);
    controller.surveyResultsFreeGrid.show(false);
    controller.surveyResultsLookupPieChart.show(false);
    controller.surveyResultsLookupGrid.show(false);
    controller.surveyResultsCountStatsGrid.show(false);
    controller.surveyResultsMeasurementStatsGrid.show(false);
    controller.surveyResultsDocsGrid.show(false);
    controller.surveyResultsDateColumnChart.show(false);
    controller.surveyResultsTimeColumnChart.show(false);
}

