var abCompContractRptRequirementEval = View.createController('abCompContractRptRequirementEval', {	

	
	afterInitialDataFetch: function() {
		// get console view controller from opener view
		this.consoleController = View.controllers.get("controllerConsole");
		if(!this.consoleController){
			this.consoleController = View.getOpenerView().controllers.get('controllerConsole');
		}
		//register to console controller
		if (this.consoleController) {
			this.consoleController.controllers.push(this);
		}
	},
	
	refreshFromConsole: function() {
		if(this.consoleController){
			var grid = this.contractTerm_grid;
			var res =  new Ab.view.Restriction(); 
			res.addClause("regrequirement.reg_program",this.consoleController.programParam,"=");
			grid.refresh(res);
			grid = this.contractViolation_grid;
			res =  new Ab.view.Restriction(); 
			res.addClause("regviolation.reg_program",this.consoleController.programParam,"=");
			grid.refresh(res);
		}
	},
	
	contractTerm_grid_afterRefresh: function(){
		this.contractTerm_grid.gridRows.each(function(row) {
            var value = row.getRecord().getValue('regrequirement.comp_level');
            var compLevel = parseInt(value.substring(0,1));
            if(compLevel<3){
            	row.dom.style.backgroundColor = "#FF6666";
            }else if(compLevel<5){
            	row.dom.style.backgroundColor = "#FFFF99";
            }
        });    
	},
	
	contractViolation_grid_afterRefresh: function(){
		this.contractViolation_grid.gridRows.each(function(row) {
            var value = row.getRecord().getValue('regviolation.severity');
            var violLevel = parseInt(value.substring(0,1));
            if(violLevel<3){
            	row.dom.style.backgroundColor = "#FF6666";
            }else if(violLevel<5){
            	row.dom.style.backgroundColor = "#FFFF99";
            }
        });    
	}
});