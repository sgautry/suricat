/**
 * This view is primarily used in tabs views and and popup dialog.
 * When used in tabs, the method  abCompEventColumnRpt_afterRefresh is always called except when is used in popup dialogs.
 */
var abCompEventColumnRptController = View.createController('abCompEventColumnRptController', {
    questionnaire_id: 0,
    survey_event_id: 0,
    afterInitialDataFetch: function () {
        // only refresh the questionnaire panel when open in dialog window. The abCompEventColumnRpt_afterRefresh is not called because panel is columnReport
        if (valueExists(View.getOpenerView().dialog)) {
            this.refreshQuestionnaire();
        }
    },
    abCompEventColumnRpt_afterRefresh: function () {
        this.refreshQuestionnaire();
    },
    refreshQuestionnaire: function () {
        this.survey_event_id = 0;
        if (valueExistsNotEmpty(this.abCompEventColumnRpt.record.getValue('activity_log.activity_log_id'))) {
            this.survey_event_id = this.abCompEventColumnRpt.record.getValue('activity_log.activity_log_id');
        }
        this.questionnaire_id = getQuestionnaireId(this.survey_event_id);
        if (!valueExistsNotEmpty(this.questionnaire_id)) {
            this.questionnaireView.show(false);
            var layout = View.getLayoutManager('main');
            layout.collapseRegion('south');
            return;
        }
        var questionnaireView = View.panels.get('questionnaireView').contentView;
        if (questionnaireView) {
            loadQuestionnairePanel();
        } else {
            setTimeout(loadQuestionnairePanel, 5000);
        }
    }
});

function loadQuestionnairePanel() {
    /* get this view's controller */
    var controller = View.controllers.get('abCompEventColumnRptController');
    var layout = View.getLayoutManager('main');
    if (layout.isRegionCollapsed('south')) {
        layout.expandRegion('south');
        controller.questionnaireView.show(true);
    }
    var questionnaireView = View.panels.get('questionnaireView').contentView;
    if (questionnaireView) {
        var updateQuestionnaireController = questionnaireView.controllers.get('UpdateQuestionnaire');
        updateQuestionnaireController.loadQuestionnaireWithResults(controller.questionnaire_id, true, controller.survey_event_id);
    }
}

function getQuestionnaireId(surveyEventId) {
    var record = View.dataSources.get('abCompEventColumnRptDs').getRecord("activity_log.activity_log_id = " + surveyEventId);
    return record.getValue('regrequirement.questionnaire_id');
}