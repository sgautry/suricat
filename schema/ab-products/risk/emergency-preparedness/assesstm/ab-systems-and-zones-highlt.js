View.createController('abEpHighltSystemAndZonesController', {
    blId: null,
    flId: null,
    dwgName: null,
    drawing_suffix: '-regcompliance',
    drawingController: null,
    drawingSvgId: null,
    afterViewLoad: function () {
        this.initDrawingControl();
    },
    /**
     * called when view onload,initial the employee list panel
     */
    afterInitialDataFetch: function () {
        this.abEpHighltSystemAndZones_grid_fl.refresh("fl.bl_id=''");
    },
    /**
     * Init drawing control.
     */
    initDrawingControl: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['rm', 'zone', 'regcompliance'],
            topLayers: 'zone',
            layersPopup: {
                layers: "rm-assets;rm-labels;zone-assets;regcompliance-assets;gros-assets;gros-labels;background",
                defaultLayers: "rm-assets;rm-labels;zone-assets;zone-labels;regcompliance-assets;gros-assets;background"
            },
            assetTooltip: [
                {assetType: 'zone', datasource: 'abSystemZones_drawing_zoneLabel', fields: 'zone.bl_id;zone.fl_id;zone.zone_id;zone.description'}
            ],
            listeners: {
                afterDrawingLoaded: function (drawingController, svgId) {
                    // if drawing not found by -regcompliance suffix, load the drawing without it
                    if (!drawingController.svgControl.drawingController.isDrawingsLoaded) {
                        var drawing = drawingController.drawings[svgId];
                        delete drawing.pkeyValues['filename'];
                        drawingController.showDrawing(drawing);
                    }
                }
            }
        });
    },
    loadDrawing: function (blId, flId, drawingName) {
        this.drawingController.showDrawing({
            pkeyValues: {blId: blId, flId: flId, filename: drawingName + this.drawing_suffix},
            drawingName: drawingName
        });
        if (valueExistsNotEmpty(document.getElementById("checkbox_layer-ZONE-HVAC-HL-fills"))) document.getElementById("checkbox_layer-ZONE-HVAC-HL-fills").click();
        if (valueExistsNotEmpty(document.getElementById("checkbox_layer-ZONE-SPRINKLER-HL-fills"))) document.getElementById("checkbox_layer-ZONE-SPRINKLER-HL-fills").click();
        if (valueExistsNotEmpty(document.getElementById("checkbox_layer-ZONE-SECURITY-asset"))) document.getElementById("checkbox_layer-ZONE-SECURITY-asset").click();
        if (valueExistsNotEmpty(document.getElementById("checkbox_layer-ZONE-EMERGENCY-asset"))) document.getElementById("checkbox_layer-ZONE-EMERGENCY-asset").click();
    },

    /**
     * refresh floor grid panel by the first row data in building panel
     */
    onRefreshFloor: function () {
        var restriction = new Ab.view.Restriction();
        if (this.abEpHighltSystemAndZones_grid_bl.rows.length > 0) {
            var firstBlRow = this.abEpHighltSystemAndZones_grid_bl.rows[0];
            restriction.addClause("bl.bl_id", firstBlRow["bl.bl_id"], "=");
            this.abEpHighltSystemAndZones_grid_fl.refresh(restriction);
        }
        else {
            restriction.addClause("fl.bl_id", "", "=");
            this.abEpHighltSystemAndZones_grid_fl.refresh(restriction);
        }
    },
    /**
     * called when user click the Refresh button
     */
    abEpHighltSystemAndZones_grid_bl_onRefresh: function () {
        var restriction = new Ab.view.Restriction();
        this.abEpHighltSystemAndZones_grid_bl.refresh(restriction);
        this.onRefreshFloor();
    },
    /**
     * 1 load drawing by the first row in the floor grid and checkbox values
     * 2 refresh the details grid panel
     */
    abEpHighltSystemAndZones_grid_fl_afterRefresh: function () {
        if (this.abEpHighltSystemAndZones_grid_fl.rows.length > 0) {
            this.abEpHighltSystemAndZones_grid_fl.selectedRowIndex = 0;
            this.showZones();
            var firstFlRow = this.abEpHighltSystemAndZones_grid_fl.rows[0];
            this.refreshDetailsPanel(firstFlRow["fl.bl_id"], firstFlRow["fl.fl_id"]);
        } else {
            this.refreshDetailsPanel("", "");
        }
    },
    /**
     * Refresh details panel
     * @param {string} building --building code
     * @param {string} floor -- floor code
     */
    refreshDetailsPanel: function (building, floor) {
        var restriction = new Ab.view.Restriction();
        restriction.addClause("zone.bl_id", building, "=");
        restriction.addClause("zone.fl_id", floor, "=");
        this.abSystemZone_zonedetailGrid.refresh(restriction);
        this.abSystemZone_zonedetailGrid.show(true);
    },
    showZones: function () {
        var flGrid = View.panels.get('abEpHighltSystemAndZones_grid_fl');
        if (flGrid.selectedRowIndex < 0) {
            return;
        }
        var buildingId = flGrid.rows[flGrid.selectedRowIndex]["fl.bl_id"];
        var floorId = flGrid.rows[flGrid.selectedRowIndex]["fl.fl_id"];
        var dwgName = flGrid.rows[flGrid.selectedRowIndex]["fl.dwgname"];
        var radioValue = this.getSelectedRadioButton('radio');
        View.dataSources.get('abSystemZones_drawing_zoneHighlight').addParameter('drawingLayer', radioValue);
        this.loadDrawing(buildingId, floorId, dwgName);
        this.highlightDrawing(radioValue, buildingId, floorId, dwgName);
        this.refreshDetailsPanel(buildingId, floorId);
    },
    highlightDrawing: function (radioValue, buildingId, floorId, dwgName) {
        this.drawingController.resetHighlight();
        this.abSystemZones_drawing_zoneHighlight.addParameter('drawingLayer', radioValue);
        var restriction = new Ab.view.Restriction();
        restriction.addClause('zone.bl_id', buildingId, '=');
        restriction.addClause('zone.fl_id', floorId, '=');
        var records = this.abSystemZones_drawing_zoneHighlight.getRecords(restriction);
        this.displayLayer(radioValue, buildingId, floorId);
        var highlightController = this.drawingController.svgControl.getController("HighlightController");
        for ( var i = 0; i < records.length; i++ ) {
            var rankPattern = records[i].getValue('zone.hpattern_acad');
            var id = buildingId + ';' + floorId + ';' + records[i].getValue('zone.zone_id');
            var color = rankPattern == '' ? 0x999999 : this.getColorFromPattern(rankPattern);
            var finalColor;
            if (color != undefined) {
                finalColor = '#' + color;
            } else {
                finalColor = 0x999999;
            }
            highlightController.highlightAsset(id, {
                svgId: this.drawingController.config.container + '-' + dwgName.toLowerCase() + '-svg',
                color: finalColor,
                persistFill: true,
                overwriteFill: true
            });
        }
    },
    displayLayer: function (radioValue, buildingId, floorId) {
        var title = String.format(getMessage('drawingPanelTitle1') + "  " + buildingId + "-" + floorId);
        if (valueExistsNotEmpty(document.getElementById("checkbox_layer-ZONE-FIRE-HL-fills"))) {
            if (document.getElementById("checkbox_layer-ZONE-FIRE-HL-fills").checked)
                document.getElementById("checkbox_layer-ZONE-FIRE-HL-fills").click();
        }
        if (valueExistsNotEmpty(document.getElementById("checkbox_layer-ZONE-SPRINKLER-HL-fills"))) {
            if (document.getElementById("checkbox_layer-ZONE-SPRINKLER-HL-fills").checked)
                document.getElementById("checkbox_layer-ZONE-SPRINKLER-HL-fills").click();
        }
        if (valueExistsNotEmpty(document.getElementById("checkbox_layer-ZONE-SECURITY-asset"))) {
            if (document.getElementById("checkbox_layer-ZONE-SECURITY-asset").checked)
                document.getElementById("checkbox_layer-ZONE-SECURITY-asset").click();
        }
        if (valueExistsNotEmpty(document.getElementById("checkbox_layer-ZONE-EMERGENCY-asset"))) {
            if (document.getElementById("checkbox_layer-ZONE-EMERGENCY-asset").checked)
                document.getElementById("checkbox_layer-ZONE-EMERGENCY-asset").click();
        }
        if (valueExistsNotEmpty(document.getElementById("checkbox_layer-ZONE-HVAC-HL-fills"))) {
            if (document.getElementById("checkbox_layer-ZONE-HVAC-HL-fills").checked)
                document.getElementById("checkbox_layer-ZONE-HVAC-HL-fills").click();
        }
        switch (radioValue) {
            case 'ZONE-FIRE':
                title = String.format(getMessage('drawingPanelTitle11') + "  " + buildingId + "-" + floorId);
                if (valueExistsNotEmpty(document.getElementById("checkbox_layer-ZONE-FIRE-HL-fills")))
                    document.getElementById("checkbox_layer-ZONE-FIRE-HL-fills").click();
                break;
            case 'ZONE-SPRINKLER':
                title = String.format(getMessage('drawingPanelTitle12') + "  " + buildingId + "-" + floorId);
                if (valueExistsNotEmpty(document.getElementById("checkbox_layer-ZONE-SPRINKLER-HL-fills")))
                    document.getElementById("checkbox_layer-ZONE-SPRINKLER-HL-fills").click();
                break;
            case 'ZONE-SECURITY':
                title = String.format(getMessage('drawingPanelTitle13') + "  " + buildingId + "-" + floorId);
                if (valueExistsNotEmpty(document.getElementById("checkbox_layer-ZONE-SECURITY-asset")))
                    document.getElementById("checkbox_layer-ZONE-SECURITY-asset").click();
                break;
            case 'ZONE-HVAC':
                title = String.format(getMessage('drawingPanelTitle14') + "  " + buildingId + "-" + floorId);
                if (valueExistsNotEmpty(document.getElementById("checkbox_layer-ZONE-HVAC-HL-fills")))
                    document.getElementById("checkbox_layer-ZONE-HVAC-HL-fills").click();
                break;
            case 'ZONE-EMERGENCY':
                title = String.format(getMessage('drawingPanelTitle15') + "  " + buildingId + "-" + floorId);
                if (valueExistsNotEmpty(document.getElementById("checkbox_layer-ZONE-EMERGENCY-asset")))
                    document.getElementById("checkbox_layer-ZONE-EMERGENCY-asset").click();
        }
        this.drawingController.setDrawingPanelTitle(title);
    },
    //get the radio button
    getSelectedRadioButton: function (name) {
        var radioButtons = document.getElementsByName(name);
        for ( var i = 0; i < radioButtons.length; i++ ) {
            if (radioButtons[i].checked) {
                return radioButtons[i].value;
            }
        }
        return null;
    },
    getColorFromPattern: function (pattern) {
        // return as hex
        var color = gAcadColorMgr.getRGBFromPatternForGrid(pattern, true);
        if (color == "-1") {
            color = gAcadColorMgr.getUnassignedColor(true);
        }
        return color;
    }
});