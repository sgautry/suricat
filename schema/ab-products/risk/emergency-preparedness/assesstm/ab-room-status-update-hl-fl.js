var controller = View.createController('abHazmatPlans', {
	drawingController : null,
	buildingId : null,
	floorId : null,
	dwgname : null,
	svgId : null,
    //----------------event handle--------------------
    afterViewLoad: function () {
        this.abEgressPlans_rmdetailGrid.addEventListener('onMultipleSelectionChange', this.onGridPanelClicked, this);   
        
        this.abEgressPlans_rmdetailGrid.show(false); 
        this.initDrawingControl();
        
    },
    
    /**
     * Init drawing control.
     */
    initDrawingControl: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['rm'],
            allowMultipleDrawings: false,
            orderByColumn: false,
            bordersHighlightSelector: true,
            showPanelTitle: true,
            diagonalSelection:  {borderWidth: 15},
            highlightParameters: [{
                'view_file': "ab-room-status-update-hl-fl.axvw",
                'hs_ds': "abEgressPlans_drawing_rmHighlight",
                'label_ds': 'abEgressPlans_drawing_rmLabel',
                'label_ht': '0.60',
                showSelector: false
            }],
            layersPopup: {
                layers: "rm-assets;rm-labels;activity_log-assets;activity_log-labels;gros-labels;background",
                defaultLayers: "rm-assets;rm-labels;activity_log-assets;activity_log-labels;gros-assets;background"
            },
            assetTooltip: [
                {
                    assetType: 'rm',
                    datasource: 'abEgressPlans_drawing_rmLabel',
                    fields: 'rm.rm_id;rm.rm_type;rm.area;rm.recovery_status'
                }
            ],
            listeners: {
                afterDrawingLoaded: function (drawingController, svgId) {
                    drawingController.svgControl.getController("SelectController").setMultipleSelection(true);
                    var controller = View.controllers.get('abHazmatPlans');
                    controller.svgId = svgId;
                },
                onClickAssetAfter: function (drawingController, svgId, selectedAssetId, assetType, selectedAssets, selected){
                	var objDetailsGrid = View.panels.get("abEgressPlans_rmdetailGrid"); 
                	var pkey = selectedAssetId.split(';');
                	objDetailsGrid.gridRows.each(function(gridRow){
                		if (gridRow.getFieldValue('rm.bl_id') == pkey[0] 
                			&& gridRow.getFieldValue('rm.fl_id') == pkey[1] 
                			&& gridRow.getFieldValue('rm.rm_id') == pkey[2]) {
                			if (selected) {
                				gridRow.select();
                			} else {
                				gridRow.unselect();
                			}
                		}
                	});
                }
            }
        });
    },
    /**
     * Load drawing.
     */
    loadDrawing: function () {
        this.drawingController.showDrawing({
            pkeyValues: {blId: this.buildingId, flId: this.floorId},
            drawingName: this.dwgname
        });
        this.highlightRooms();     
    },
    
    highlightRooms : function(){
    	var highlightController = this.drawingController.svgControl.getController("HighlightController");
        var restriction = new Ab.view.Restriction();
        restriction.addClause("rm.bl_id", this.buildingId);
        restriction.addClause("rm.fl_id", this.floorId);
        var records = this.abEgressPlans_drawing_rmHighlight.getRecords(restriction);
        
        for(var i=0;i<records.length; i++){
        	var rec = records[i];
        	var assetId = this.buildingId + ";" + this.floorId + ";" + rec.getValue('rm.rm_id');
        	var color = '';
        	switch(rec.getValue('rm.recovery_status')){
        		case 'NONE' : 
        			color = '#C0C0C0';
        			break;
        		case 'NONE-REVIEW' : 
        			color = '#8080C0';
        			break;
        		case 'FIT-ONLINE' : 
        			color = '#00FF00';
        			break;
        		case 'FIT-OFFLINE' : 
        			color = '#80FFFF';
        			break;
        		case 'UNFIT-TEMP' : 
        			color = '#FF9933';
        			break;
        		case 'UNFIT-PERM' : 
        			color = '#FF0000';
        	}
        	highlightController.highlightAsset(assetId,{
        		svgId: this.svgId,
                color: color,
                persistFill: true,
                overwriteFill: true
        	});
        	
        }
    },
    
    
    
/*    onDwgPanelClicked: function(pk, selected) {     	      	 
    	this.abEgressPlans_rmdetailGrid.gridRows.each(function(row){
    		if (row.getFieldValue("rm.bl_id") == pk[0] && row.getFieldValue("rm.fl_id") == pk[1] && row.getFieldValue("rm.rm_id") == pk[2]) {
    			if (selected) {
    				row.select();
    			} else {
    				row.unselect();
    			} 
    		}
    	});
    },*/
    
    onGridPanelClicked: function(row) {    
    	var record = row.row.getRecord(),
       	gridRep = row.grid;
    	var blId = record.getValue("rm.bl_id"),
       	flId = record.getValue("rm.fl_id"),
       	rmId = record.getValue("rm.rm_id")

	       var selectController = this.drawingController.svgControl.getController("SelectController");
	       var svgId = this.drawingController.config.container + '-' + controller.dwgname + '-svg',
	           assetId = blId + ';' + flId + ';' + rmId;
	       var selected = row.row.isSelected();
	       if (selected) {
	           if (valueExists(selectController.selectedAssets[svgId]) && selectController.selectedAssets[svgId].indexOf(assetId) >= 0) { // avoid same room selection
	               // do nothing
	           } else {
	               selectController.toggleAssetSelection(svgId, assetId);
	               //setAllRowsSelected(gridRep, false);
	               row.row.select();	     
	               if (!existsSelectedItemInSameRoom(gridRep, row.row, 'rm')) {
	    	           selectController.toggleAssetSelection(svgId, assetId);
	               }
	           }
	       } else if (!existsSelectedItemInSameRoom(gridRep, row.row, 'rm')) {
	           selectController.toggleAssetSelection(svgId, assetId);
	       }
    	
    },
    
    afterInitialDataFetch: function () {
        this.abEgressPlans_select_flooring.refresh("fl.bl_id=''");
        var objSetRecoveryStatus = Ext.get('setRecStatus');
        if (valueExists(objSetRecoveryStatus)) {
            objSetRecoveryStatus.on('click', this.showRoomDetailStatusMenu, this, null);
        }
    },
    
    showRoomDetailStatusMenu: function (e, item) {
        var index = this.abEgressPlans_grid_rmdetail.fieldDefs.indexOfKey('rm.recovery_status');
        var enumValues = this.abEgressPlans_grid_rmdetail.fieldDefs.items[index].enumValues;

        var menuItems = [];
        for (var name in enumValues) {
            menuItems.push({
                text: enumValues[name],
                handler: this.onChangeRoomStatus.createDelegate(this, [name])
            });
        }
        var menu = new Ext.menu.Menu({
            items: menuItems
        });
        menu.showAt(e.getXY());
    },
    
    onChangeRoomStatus: function (menuItemId) {
        var selectedRecords = this.abEgressPlans_rmdetailGrid.getSelectedRecords();
        if (selectedRecords.length < 1) {
            View.showMessage(getMessage('noRecordsSelected'));
            return;
        }
        for (var i = 0; i < selectedRecords.length; i++) {
            selectedRecords[i].setValue("rm.recovery_status", menuItemId);
        }
        try {
            var result = Workflow.callMethod("AbRiskEmergencyPreparedness-EPCommonService-updateRoomRecoveryStatus", selectedRecords, menuItemId);
            if (result.code == "executed") {
                this.abEgressPlans_rmdetailGrid.refresh();
                this.loadDrawing();
                View.showMessage(getMessage("recordsSuccessfullyUpdated"));                
            }
        } catch (e) {
            Workflow.handleError(e);
        }
    }
});



//if the first building has the floor ,display them
function blPanelAfterRefresh() {
    var blPanel = View.panels.get('abEgressPlans-select-building');

    var rows = blPanel.rows;
    if (rows.length > 0) {
        var blId = rows[0]['bl.bl_id'];
        var blRes = new Ab.view.Restriction();
        blRes.addClause('fl.bl_id', blId, '=');
        View.panels.get('abEgressPlans_select_flooring').refresh(blRes);
    }
}
function showDrawing() {
    var buildingDrawing = View.panels.get('abEgressPlans_select_flooring');
    var selectedIndex = buildingDrawing.selectedRowIndex;
    controller.buildingId = buildingDrawing.rows[selectedIndex]["fl.bl_id"];
    controller.floorId = buildingDrawing.rows[selectedIndex]["fl.fl_id"];
    controller.dwgname = buildingDrawing.rows[selectedIndex]["fl.dwgname"];
    disPlayDrawing();
}
/**
 * button for the select layer and assettype of the zone and recompliance
 */
function disPlayDrawing() {
    if (!controller.buildingId || !controller.floorId) {
        return;
    }
//    var drawingPanel = View.panels.get('abEgressPlans_DrawingPanel');
//    drawingPanel.clear();

//    var title = String.format(getMessage('dPTitle_rms') + " : " + buildingId + "-" + floorId);
//    addDrawingByType(null, 'rm', null, 'abEgressPlans_drawing_rmHighlight', 'abEgressPlans_drawing_rmLabel', 'abEgressPlans_rmdetailGrid')
    
    controller.loadDrawing();
    var restriction = new Ab.view.Restriction();
    restriction.addClause('rm.bl_id', controller.buildingId, '=');
    restriction.addClause('rm.fl_id', controller.floorId, '=');
    
    View.panels.get('abEgressPlans_rmdetailGrid').show(true);
    View.panels.get('abEgressPlans_rmdetailGrid').refresh(restriction);

//    drawingPanel.appendInstruction("default", "", title);
//    drawingPanel.processInstruction("default", "");
}

/**
 * Search for another selected item in the grid with the same room
 * @param grid
 * @param rmRow
 * @param tableName location table name (from where come bl_id, fl_id, rm_id) 
 * @returns {Boolean} true is another item with same room found; false otherwise
 */
function existsSelectedItemInSameRoom(grid, rmRow, tableName){
	if(!valueExists(tableName)){
		tableName = 'activity_log';
	}
	var selectedRows = grid.getSelectedRows();
	
	for (var i = 0; i < selectedRows.length; i++) {
		var selRow = selectedRows[i].row;
		if(selRow.getFieldValue(tableName + '.bl_id') == rmRow.getFieldValue(tableName + '.bl_id')
				&& selRow.getFieldValue(tableName + '.fl_id') == rmRow.getFieldValue(tableName + '.fl_id')
				&& selRow.getFieldValue(tableName + '.rm_id') == rmRow.getFieldValue(tableName + '.rm_id'))
			return true;
	}
	
	return false;
}
///**
// set the asset ,currentHighlightDS ,currentLabelsDS  and locate it to the json and swf file
// */
//function addDrawingByType(assetTypesSuffix, tablename, backgroundSuffix, currentHighlightDS, currentLabelsDS, detailgrid, resValue) {
//    var drawingPanel = View.panels.get('abEgressPlans_DrawingPanel');
//    var opts = new DwgOpts();
//    var restriction = new Ab.view.Restriction();
//    var assetType = tablename;
//
//    restriction.addClause(tablename + '.bl_id', buildingId, '=');
//    restriction.addClause(tablename + '.fl_id', floorId, '=');
//    drawingPanel.clear();
//
//    drawingPanel.assetTypes = assetType; //tablename;
//    drawingPanel.currentHighlightDS = currentHighlightDS;
//    drawingPanel.currentLabelsDS = currentLabelsDS;
//
//    var dcl = new Ab.drawing.DwgCtrlLoc(buildingId, floorId, null, dwgname);
//    drawingPanel.addDrawing.defer(200, drawingPanel, [dcl, opts]);
//
//    if (tablename == "zone") {
//        restriction.addClause('zone.layer_name', resValue, '=');
//    }
//    if (tablename == "regcompliance") {
//        restriction.addClause('regcompliance.regulation', resValue, '=');
//    }
//    View.panels.get(detailgrid).refresh(restriction);
//}
