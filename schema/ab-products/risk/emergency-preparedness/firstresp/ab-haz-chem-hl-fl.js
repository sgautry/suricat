/**
 * @author Guo Jiangtao
 */
var overallDrawingRestriction = new Ab.view.Restriction();

var abRiskMsdsRptDrawingController = View.createController('abRiskMsdsRptDrawingController', {

	// selected floor plan
	selectedFloorPlans : null,	
	drawingController: null,

	afterInitialDataFetch : function() { 
		
		this.abRiskMsdsRptDrawingAssignment.addParameter('tier2_notListed', getMessage('msg_tier2_notListed'));
		this.abRiskMsdsRptDrawingAssignment.addParameter('tier2_unknown', getMessage('msg_tier2_unknown'));
		this.abRiskMsdsRptDrawingAssignment.addParameter('tier2_hazardous', getMessage('msg_tier2_hazardous'));
		this.abRiskMsdsRptDrawingAssignment.addParameter('tier2_extremelyHazardous', getMessage('msg_tier2_extremelyHazardous'));
		
		this.abRiskMsdsRptDrawingAssignment_exportToPDF.addParameter('tier2_notListed', getMessage('msg_tier2_notListed'));
		this.abRiskMsdsRptDrawingAssignment_exportToPDF.addParameter('tier2_unknown', getMessage('msg_tier2_unknown'));
		this.abRiskMsdsRptDrawingAssignment_exportToPDF.addParameter('tier2_hazardous', getMessage('msg_tier2_hazardous'));
		this.abRiskMsdsRptDrawingAssignment_exportToPDF.addParameter('tier2_extremelyHazardous', getMessage('msg_tier2_extremelyHazardous'));

		// hide location form default
		this.abRiskMsdsRptDrawingFormLocation.show(false);
		// show drawing legend panel
		$('abRiskMsdsRptDrawingDrawingPanel_legendDiv').style.display = '';

		// if this view is opened as pop up with site and building restriction
		// then directly mark the selected tree node selected
		var siteId = View.getOpenerView().siteId
		if (siteId) {
			var root = this.abRiskMsdsRptDrawingTreeSite.treeView.getRoot();
			if (root) {
				var siteNodes = root.children;
				for ( var i = 0; i < siteNodes.length; i++) {
					// get selected site node
					if (siteNodes[i].data['site.site_id'] == siteId) {
						// mark node selected then refresh and expand the site
						// node
						siteNodes[i].setSelected(true);
						this.abRiskMsdsRptDrawingTreeSite.refreshNode(siteNodes[i]);
						siteNodes[i].expand();

						// get the all building node under the selected site
						// node
						var blId = View.getOpenerView().blId;
						var blNodes = siteNodes[i].children;

						for ( var j = 0; j < blNodes.length; j++) {
							// get the selected building node
							if (blNodes[j].data['bl.bl_id'] == blId) {
								// mark node seleted then refresh and expand the
								// building node
								blNodes[j].setSelected(true);
								this.abRiskMsdsRptDrawingTreeSite.refreshNode(blNodes[j]);
								blNodes[j].expand();

								// get all floor nodes and marke as selected
								var flNodes = blNodes[j].children;
								for ( var m = 0; m < flNodes.length; m++) {
									flNodes[m].setSelected(true);
								}

								break;
							}
						}

						break;
					}
				}
			}
		}
		
	},
	
	/**
     * Init drawing control.
     */
    initDrawingControl: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['rm'],
            diagonalSelection: { borderWidth: 15},
            print : true,
            allowMultipleDrawings: true,
            showPanelTitle: true,
            highlightParameters: [{'view_file': 'ab-haz-chem-hl-fl.axvw',
				'hs_ds': 'abRiskMsdsRptDrawingHlTypeDS',
				'label_ds': 'abRiskMsdsRptDrawingLabelTypeDS',
				'label_ht': '0.60',
				showSelector: true}],
   
            assetTooltip: [
                {
                    assetType: 'rm',
                    datasource: 'abRiskMsdsRptDrawingLabelTypeDS',
                    fields: 'rm.rm_id;rm.count_em;rm.option1'
                }
            ],
            layersPopup: {
                layers: "rm-assets;rm-labels;activity_log-assets;cb_samples-assets;gros-assets;gros-labels;background",
                defaultLayers: "rm-assets;rm-labels;activity_log-assets;activity_log-labels;cb_samples-assets;cb_samples-labels;gros-assets;background"
            },
            listeners: {
                onClickAssetAfter: function (drawingController, svgId, selectedAssetId, assetType, selectedAssets, selected){
                    
                    var controller = View.controllers.get('abRiskMsdsRptDrawingController');
                    var pk = selectedAssetId.split(';');
                    var restriction = "rm.bl_id='" + pk[0] + "' AND " + "rm.fl_id='" + pk[1] + "' AND " + "rm.rm_id='" + pk[2] + "' ";
                	if (selected) {
                		var restriction = new Ab.view.Restriction();
                		restriction.addClause('msds_location.bl_id', pk[0]);
                		restriction.addClause('msds_location.fl_id', pk[1]);
                		restriction.addClause('msds_location.rm_id', pk[2]);
                		controller.abRiskMsdsRptDrawingGridMsds.show(false);
                		controller.abRiskMsdsRptDrawingFormLocation.refresh(restriction);
                		controller.abRiskMsdsRptDrawingAssignment.refresh(restriction);
                	} else {
                		controller.abRiskMsdsRptDrawingGridMsds.show(true);
                		controller.abRiskMsdsRptDrawingFormLocation.show(false);

                		controller.abRiskMsdsRptDrawingAssignment.refresh(overallDrawingRestriction);
                	}
                }
            }
        });
    },
    
    loadDrawing: function (blId, flId, drawingName){
        this.drawingController.showDrawing({
            pkeyValues: {blId: blId, flId: flId},
            drawingName: drawingName
        });
    },
    
 
    unloadDrawings: function () {
        this.drawingController.clearDrawingPanel();
    },

	afterViewLoad : function() {
		if (this.checkLicense()) {  
			// set the all tree level as multi-selected
			this.abRiskMsdsRptDrawingTreeSite.setMultipleSelectionEnabled(0);
			this.abRiskMsdsRptDrawingTreeSite.setMultipleSelectionEnabled(1);
			this.abRiskMsdsRptDrawingTreeSite.setMultipleSelectionEnabled(2);
	
			// kb#3035019 - highlight location details grid rows with colors to match drawing legend for tier2 hazard level
	        this.abRiskMsdsRptDrawingAssignment.afterCreateCellContent = function(row, column, cellElement) {
	        	var tier2 = (row['msds_location.tier2.raw'] != undefined) ? row['msds_location.tier2.raw'] : row['msds_location.tier2'];
				if (column.id == 'msds_location.tier2')	{
					if (tier2 == 'Unknown')	{
						cellElement.style.background = '#61C0C0';
					}
					else if (tier2 == 'Not Listed') {
						cellElement.style.background = '#69FF69';
					}
					else if (tier2 == 'Hazardous') {
						cellElement.style.background = '#FFAD69';
					}
					else if (tier2 == 'Extremely Hazardous') {
						cellElement.style.background = '#FF6969';
					}
				}
	        }
		}
		this.initDrawingControl();
	},

	checkLicense: function() {
		var filterButton = this.abRiskMsdsRptDrawingTreeSite.actions.get("showSeletedFloorPlan");		
		filterButton.show(false);
		try {
			var result = Workflow.callMethod("AbRiskCleanBuilding-CleanBuildingService-isActivityLicense", "AbRiskMSDS");
		} 
		catch (e) {
			Workflow.handleError(e);
			return false;
		}

		if (result.value) {
			filterButton.show(true);
			return true;
		} else {
			View.showMessage(getMessage("msg_no_license"));
			return false;
		} 
	},
	
	abRiskMsdsRptDrawingTreeSite_onShowSeletedFloorPlan : function() {
		if (this.checkSelection()) {
			// get all selected floor plans
			this.selectedFloorPlans = [];
			var flNodes = this.abRiskMsdsRptDrawingTreeSite.getSelectedNodes(2);
			var restriction = '';
			overallDrawingRestriction = '';
			for ( var i = 0; i < flNodes.length; i++) {
				var floorPlan = {};
				floorPlan['bl_id'] = flNodes[i].parent.data['bl.bl_id'];
				floorPlan['fl_id'] = flNodes[i].data["fl.fl_id"]
				floorPlan['dwgname'] = flNodes[i].data["fl.dwgname"]
				this.selectedFloorPlans.push(floorPlan);
				restriction += "OR (msds_location.bl_id='" + flNodes[i].parent.data['bl.bl_id'] + "' and msds_location.fl_id='" + flNodes[i].data["fl.fl_id"] + "') "
				overallDrawingRestriction += "OR (msds_location.bl_id='" + flNodes[i].parent.data['bl.bl_id'] + "' and msds_location.fl_id='" + flNodes[i].data["fl.fl_id"] + "') "
			}
			this.unloadDrawings();
			this.addFirstDrawing();
			// refresh the locations grid			
			this.abRiskMsdsRptDrawingGridMsds.refresh(restriction.substring(2));
			// show and refresh the location assignments grid
			overallDrawingRestriction = (overallDrawingRestriction.substring(2));
			this.abRiskMsdsRptDrawingAssignment.show(true);
			this.abRiskMsdsRptDrawingAssignment.refresh(overallDrawingRestriction);
		}
	},

	/**
	 * check the tree selection
	 */
	checkSelection : function() {
		// get all selected sites nodes
		var siteNodes = this.abRiskMsdsRptDrawingTreeSite.getSelectedNodes(0);
		for ( var i = 0; i < siteNodes.length; i++) {
			// if site node not expand, refresh it to load children
			if (siteNodes[i].children.length == 0) {
				this.abRiskMsdsRptDrawingTreeSite.refreshNode(siteNodes[i]);
			}

			// make all building nodes in this site selected
			var blNodes = siteNodes[i].children;
			for ( var j = 0; j < blNodes.length; j++) {
				if (!blNodes[j].isSelected) {
					blNodes[j].setSelected(true);
				}
			}
		}

		// get all selected building nodes
		var blNodes = this.abRiskMsdsRptDrawingTreeSite.getSelectedNodes(1);
		for ( var i = 0; i < blNodes.length; i++) {
			// if building node not expand, refresh it to load children
			if (blNodes[i].children.length == 0) {
				this.abRiskMsdsRptDrawingTreeSite.refreshNode(blNodes[i]);
			}

			// make all floor nodes in this building selected
			var flNodes = blNodes[i].children;
			for ( var m = 0; m < flNodes.length; m++) {
				if (!flNodes[m].isSelected) {
					flNodes[m].setSelected(true);
				}
			}
		}

		// get all floor nodes, if no one selected, give a warning
		if (this.abRiskMsdsRptDrawingTreeSite.getSelectedNodes(2).length == 0) {
			View.showMessage(getMessage('error_noselection'));
			return false;
		}
		return true;
	},

	/**
	 * add first drawing to the drawing panel
	 */
	addFirstDrawing : function() {
		if (this.selectedFloorPlans.length > 0) {

			this.floorPlan = this.selectedFloorPlans[0];
			this.loadDrawing(this.floorPlan['bl_id'], this.floorPlan['fl_id'], this.floorPlan['dwgname']);

			this.selectedFloorPlans.shift();
			this.addFirstDrawing();
		}
	},

	abRiskMsdsRptDrawingFormLocation_onClearSelectedRoom : function() {
		// show location grid, hide details panel and assignment panel
		this.abRiskMsdsRptDrawingGridMsds.show(true);
		this.abRiskMsdsRptDrawingFormLocation.show(false);

		this.abRiskMsdsRptDrawingAssignment.refresh(overallDrawingRestriction);

		this.drawingController.resetSelectedDrawingAssets();
		
		this.abRiskMsdsRptDrawingTreeSite_onShowSeletedFloorPlan();
	}

});



/**
 * on click event handler when click tree node
 */
function onTreeClick(ob) {
	abRiskMsdsRptDrawingController.abRiskMsdsRptDrawingTreeSite.selectAll(false);	
	abRiskMsdsRptDrawingController.abRiskMsdsRptDrawingTreeSite.lastNodeClicked.setSelected(true);
	var loc = new Ab.drawing.DwgCtrlLoc();
	loc.setFromTreeClick(ob, "abRiskMsdsRptDrawingTreeSite");
	abRiskMsdsRptDrawingController.unloadDrawings();
	abRiskMsdsRptDrawingController.loadDrawing(loc.getBuilding(),loc.getFloor(),loc.getDwgname());
	abRiskMsdsRptDrawingController.abRiskMsdsRptDrawingGridMsds.refresh(ob.restriction);
	abRiskMsdsRptDrawingController.abRiskMsdsRptDrawingFormLocation.show(false);
	
	abRiskMsdsRptDrawingController.abRiskMsdsRptDrawingAssignment.refresh(ob.restriction);
	overallDrawingRestriction = ob.restriction;
}


/**
 * on click event handler of row click event for grid
 * abRiskMsdsRptDrawingGridMsds
 */
function selectRoom(row) {
	// refresh the details panel and assignment panel
	abRiskMsdsRptDrawingController.abRiskMsdsRptDrawingGridMsds.show(false);
	var selectedRecord = row.row.getRecord();
	var restriction = new Ab.view.Restriction();	
	restriction.addClause('msds_location.bl_id', selectedRecord.getValue('msds_location.bl_id'));
	restriction.addClause('msds_location.fl_id', selectedRecord.getValue('msds_location.fl_id'));
	restriction.addClause('msds_location.rm_id', selectedRecord.getValue('msds_location.rm_id'));
	abRiskMsdsRptDrawingController.abRiskMsdsRptDrawingFormLocation.refresh(restriction);
	abRiskMsdsRptDrawingController.abRiskMsdsRptDrawingAssignment.refresh(restriction);
	// make the room selected in the floor plan
	var svgId = abRiskMsdsRptDrawingController.drawingController.config.container + '-' + selectedRecord.getValue('rm.dwgname').toLowerCase() + '-svg',
    assetId = selectedRecord.getValue('rm.bl_id') + ';' + selectedRecord.getValue('rm.fl_id') + ';' + selectedRecord.getValue('rm.rm_id');
	abRiskMsdsRptDrawingController.drawingController.svgControl.getController("SelectController").toggleAssetSelection(svgId,assetId);
}

/**
 * on click event handler of row click event for grid
 * abRiskMsdsRptDrawingAssignment
 */
function showMSDSDetails(row) {
	var restriction = new Ab.view.Restriction();
	restriction.addClause("msds_data.msds_id", row['msds_location.msds_id'], "=");
	View.msdsRestriction = restriction;
	View.openDialog('ab-msds-rpt-map-msds-tab.axvw');
}

function refreshPanel(){
	var parentPanel = View.panels.get('abRiskMsdsRptDrawingAssignment');
	var targetPanel = View.panels.get('abRiskMsdsRptDrawingAssignment_exportToPDF');
	targetPanel.restriction = parentPanel.restriction;
	return true;
}