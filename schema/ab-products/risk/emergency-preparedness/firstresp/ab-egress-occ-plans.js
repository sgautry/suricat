View.createController('abEgressOccPlans', {
    drawingController: null,
    drawing_suffix: '-regcompliance',
    afterViewLoad: function () {
        this.initDrawingControl();
    },
    /**
     * Init drawing control.
     */
    initDrawingControl: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['rm', 'regcompliance'],
            allowMultipleDrawings: false,
            showPanelTitle: true,
            highlightParameters: [{
                'view_file': "ab-egress-occ-plans.axvw",
                'hs_ds': "abEgressPlans_drawing_rmHighlight",
                'label_ds': 'abEgressPlans_drawing_rmLabel',
                'label_ht': '0.60',
                showSelector: false
            }],
            topLayers: 'regcompliance',
            layersPopup: {
                layers: "rm-assets;rm-labels;activity_log-assets;activity_log-labels;gros-labels;gros-assets;background;regcompliance-assets;background",
                defaultLayers: "rm-assets;rm-labels;activity_log-assets;activity_log-labels;regcompliance-assets;background"
            },
            assetTooltip: [
                {
                    assetType: 'rm',
                    datasource: 'abEgressPlans_drawing_rmLabel',
                    fields: 'rm.bl_id;rm.fl_id;rm.rm_id'
                },
                {
                    assetType: 'regcompliance',
                    datasource: 'abEgressPlans_drawing_regcomplianceLabel',
                    fields: 'regcompliance.regcomp_id;regcompliance.description'
                }
            ],
            listeners: {
                afterDrawingLoaded: function (drawingController, svgId) {
                    // if drawing not found by -regcompliance suffix, load the drawing without it
                    if (!drawingController.svgControl.drawingController.isDrawingsLoaded) {
                        var drawing = drawingController.drawings[svgId];
                        delete drawing.pkeyValues['filename'];
                        drawingController.showDrawing(drawing);
                    }
                }
            }
        });
    },
    /**
     * Load drawing.
     */
    loadDrawing: function (blId, flId, drawingName) {
        var restriction = new Ab.view.Restriction();
        restriction.addClause("regcompliance.bl_id", blId);
        restriction.addClause("regcompliance.fl_id", flId);
        var egressIds = this.getEgressIds(restriction);
        this.drawingController.showDrawing({
            pkeyValues: {blId: blId, flId: flId, filename: drawingName + this.drawing_suffix},
            drawingName: drawingName,
            highlightAssets: [{
                assetType: 'regcompliance',
                assetIds: egressIds,
                color: 'red'
            }]
        });
    },
    getEgressIds: function (restriction) {
        var records = this.abEgressPlans_drawing_regcomplianceHighlight.getRecords(restriction);
        var egressIds = [];
        for ( var i = 0; i < records.length; i++ ) {
            var egressId = records[i].getValue('regcompliance.regcomp_id');
            egressIds.push(egressId);
        }
        return egressIds;
    }
});

function showDrawing() {
    var buildingDrawing = View.panels.get('abEgressPlans_select_flooring');
    var selectedIndex = buildingDrawing.selectedRowIndex;
    var buildingId = buildingDrawing.rows[selectedIndex]["fl.bl_id"];
    var floorId = buildingDrawing.rows[selectedIndex]["fl.fl_id"];
    var dwgname = buildingDrawing.rows[selectedIndex]["fl.dwgname"];
    displayDrawing(buildingId, floorId, dwgname);
    var restriction = new Ab.view.Restriction();
    restriction.addClause("em.bl_id", buildingId);
    restriction.addClause("em.fl_id", floorId);
    View.panels.get('abEgressPlans_occupancyGrid').refresh(restriction);
}

function displayDrawing(buildingId, floorId, dwgname) {
    var controller = View.controllers.get("abEgressOccPlans");
    if (!buildingId || !floorId) {
        return;
    }
    controller.loadDrawing(buildingId, floorId, dwgname);
    View.panels.get('abEgressPlans_occupancyGrid').show(true);
}