
var controller = View.createController('abHazmatPlans', {
	
	drawingController: null,

    drawing_suffix: '-regcompliance',

	/**
     * Init drawing control.
     */
    initDrawingControl: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['rm','regcompliance'],
            allowMultipleDrawings: false,
            showPanelTitle: true,
            highlightParameters: [{
                'view_file': "ab-hazmat-plans.axvw",
                'hs_ds': "abEgressPlans_drawing_regcomplianceHighlight",
                'label_ds': 'abEgressPlans_drawing_regcomplianceLabel',
                'label_ht': '0.60',
                'showSelector': false
            }
            ],
            topLayers: 'regcompliance',
            layersPopup: {
                layers: "rm-assets;activity_log-assets;activity_log-labels;gros-labels;background;regcompliance-assets",
                defaultLayers: "rm-assets;activity_log-assets;activity_log-labels;gros-assets;background;regcompliance-assets;regcompliance-labels"
            },
            assetTooltip: [
                {
                	assetType: 'regcompliance',
                    datasource: 'abEgressPlans_drawing_regcomplianceLabel',
                    fields: 'regcompliance.regcomp_id;regcompliance.description;regcompliance.bl_id;regcompliance.fl_id;regcompliance.rm_id'
                }
            ],
            listeners: {
                afterDrawingLoaded: function (drawingController, svgId) {
                    // if drawing not found by -regcompliance suffix, load the drawing without it
                    if (!drawingController.svgControl.drawingController.isDrawingsLoaded) {
                        var drawing = drawingController.drawings[svgId];
                        delete drawing.pkeyValues['filename'];
                        drawingController.showDrawing(drawing);
                    }
                }
            }
        });
    },
    
    /**
     * Load drawing.
     */
    loadDrawing: function (blId, flId, drawingName) {
        this.drawingController.showDrawing({
            pkeyValues: {blId: blId, flId: flId, filename: drawingName + this.drawing_suffix},
            drawingName: drawingName
        });
    },
    
    afterViewLoad: function(){
    	this.initDrawingControl();
        this.abEgressPlans_regdetailGrid.show(false); 
    }, 
	
	afterInitialDataFetch: function() {
		this.abEgressPlans_select_flooring.refresh("fl.bl_id=''");	
	} 
    
    
});


//if the first building has the floor ,display them
function blPanelAfterRefresh(){
    var blPanel = View.panels.get('abEgressPlans-select-building');
    var rows = blPanel.rows;
    if (rows.length > 0) {
        var blId = rows[0]['bl.bl_id'];
        var blRes = new Ab.view.Restriction();
        blRes.addClause('fl.bl_id', blId, '=');
        View.panels.get('abEgressPlans_select_flooring').refresh(blRes);
    }
}

function showDrawing(){

    var buildingDrawing = View.panels.get('abEgressPlans_select_flooring');
	var selectedIndex = buildingDrawing.selectedRowIndex;
    var buildingId = buildingDrawing.rows[selectedIndex]["fl.bl_id"];
    var floorId = buildingDrawing.rows[selectedIndex]["fl.fl_id"];
    var dwgname = buildingDrawing.rows[selectedIndex]["fl.dwgname"];
	
    disPlayDrawing(buildingId,floorId,dwgname);
}


function disPlayDrawing(buildingId,floorId,dwgname){
	if(!buildingId || !floorId){
		return;
	}
	View.controllers.get('abHazmatPlans').loadDrawing(buildingId,floorId,dwgname); 
	View.panels.get('abEgressPlans_regdetailGrid').setTitle(getMessage("detailPanelTitle_hazMat"));

    View.panels.get('abEgressPlans_regdetailGrid').show(true);
    var restriction = new Ab.view.Restriction();

    restriction.addClause('regcompliance.bl_id', buildingId, '=');
    restriction.addClause('regcompliance.fl_id', floorId, '=');
    restriction.addClause('regcompliance.regulation', 'HAZMAT', '=');
    View.panels.get('abEgressPlans_regdetailGrid').refresh(restriction);
}



