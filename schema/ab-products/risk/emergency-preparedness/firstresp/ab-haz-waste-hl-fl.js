
var controller = View.createController('abHazWastePlans', {

	drawingController: null,
	
    //----------------event handle--------------------
    afterViewLoad: function(){  
    	this.initDrawingControl();
        this.abEgressPlans_regdetailGrid.show(false);     
        this.checkLicense();
    }, 
	
	afterInitialDataFetch: function() {
		this.abEgressPlans_select_flooring.refresh("fl.bl_id=''");	
	},
	
	/**
     * Init drawing control.
     */
    initDrawingControl: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['rm'],
            allowMultipleDrawings: false,
            showPanelTitle: true,
            highlightParameters: [{
                'view_file': "ab-haz-waste-hl-fl.axvw",
                'hs_ds': "abEgressPlans_drawing_rmHighlight",
                'label_ds': 'abEgressPlans_drawing_rmLabel',
                'label_ht': '0.60',
                showSelector: false
            }
            ],
            layersPopup: {
                layers: "rm-assets;rm-labels;activity_log-assets;activity_log-labels;gros-labels;gros-assets;background",
                defaultLayers: "rm-assets;rm-labels;activity_log-assets;activity_log-labels;background"
            },
            assetTooltip: [
                {
                    assetType: 'rm',
                    datasource: 'abEgressPlans_drawing_rmLabel',
                    fields: 'rm.rm_id;rm.rm_cat;rm.rm_type'
                },
            ]
        });
    },
    
    /**
     * Load drawing.
     */
    loadDrawing: function (blId, flId, drawingName) {
        this.drawingController.showDrawing({
            pkeyValues: {blId: blId, flId: flId},
            drawingName: drawingName
        });
    },
	
	checkLicense: function() {
		var blPanel = View.panels.get('abEgressPlans-select-building');		
    	blPanel.show(false);
		try {
			var result = Workflow.callMethod("AbRiskCleanBuilding-CleanBuildingService-isActivityLicense", "AbRiskWasteMgmt");
	    } 
	    catch (e) {
	        Workflow.handleError(e);
	        return false;
	    }
	    
	    if (result.value) {
	    	blPanel.show(true);
	    	return true;
	    } else {
	    	View.showMessage(getMessage("msg_no_license"));
	    	return false;
	    } 
    }
    
});


var buildingId = null;
var floorId = null;
var dwgname = null;

//if the first building has the floor ,display them
function blPanelAfterRefresh(){
    var blPanel = View.panels.get('abEgressPlans-select-building');
   
    var rows = blPanel.rows;
    if (rows.length > 0) {
        var blId = rows[0]['bl.bl_id'];
        var blRes = new Ab.view.Restriction();
        blRes.addClause('fl.bl_id', blId, '=');
        View.panels.get('abEgressPlans_select_flooring').refresh(blRes);
    }
}

function showDrawing(){

    var buildingDrawing = View.panels.get('abEgressPlans_select_flooring');
	var selectedIndex = buildingDrawing.selectedRowIndex;
    buildingId = buildingDrawing.rows[selectedIndex]["fl.bl_id"];
    floorId = buildingDrawing.rows[selectedIndex]["fl.fl_id"];
    dwgname = buildingDrawing.rows[selectedIndex]["fl.dwgname"];
	
    disPlayDrawing();
} 

/**button for the select layer and assettype of the zone and recompliance
 *
 * */
function disPlayDrawing(){
	if(!buildingId || !floorId){
		return;
	}
		View.panels.get('abEgressPlans_regdetailGrid').setTitle(getMessage("detailPanelTitle_hazWaste"));
        View.panels.get('abEgressPlans_regdetailGrid').show(true);
        var restriction = new Ab.view.Restriction();
        
        restriction.addClause('waste_out.bl_id', buildingId, '=');
        restriction.addClause('waste_out.fl_id', floorId, '=');
        View.panels.get('abEgressPlans_regdetailGrid').refresh(restriction);     
        controller.loadDrawing(buildingId,floorId,dwgname);

} 
 


