var controller = View.createController('abHazmatPlans', {
	drawingController: null,
	blId: null,
	flId: null,
	dwgname: null,
    drawing_suffix: '-regcompliance',
	
    afterViewLoad: function(){  
	
	    this.abEgressPlans_regdetailGrid.show(false);                                          
	    this.initDrawingControl();
    }, 
                        
    afterInitialDataFetch: function() {
        this.abEgressPlans_select_flooring.refresh("fl.bl_id=''");                 
    },

	/**
     * Init drawing control.
     */
    initDrawingControl: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['rm','regcompliance'],
            topLayers: 'regcompliance',
            allowMultipleDrawings: false,
            showPanelTitle: true,
            diagonalSelection:  {borderWidth: 15},
            highlightParameters: [{'view_file': "ab-haz-overview-hl-fl.axvw",
            						'label_ds': 'abEgressPlans_labelDs_allRm',
            						'label_ht': '0.60',
            						showSelector: false}],
            layersPopup: {
                layers: "rm-assets;rm-labels;activity_log-assets;activity_log-labels;gros-labels;gros-assets;background;regcompliance-assets;background",
                defaultLayers: "rm-assets;rm-labels;activity_log-assets;activity_log-labels;regcompliance-assets;background"
            },
            assetTooltip: [
                {
                    assetType: 'rm',
                    datasource: 'abEgressPlans_labelDs_allRm',
                    fields:'rm.bl_id;rm.fl_id;rm.rm_id'
                },
                {
                	assetType: 'regcompliance',
                    datasource: 'abEgressPlans_drawing_regcomplianceLabel',
                    fields:'regcompliance.regcomp_id;regcompliance.description'
                }
            ],
            listeners: {
                afterDrawingLoaded: function (drawingController, svgId) {
                    // if drawing not found by -regcompliance suffix, load the drawing without it
                    if (!drawingController.svgControl.drawingController.isDrawingsLoaded) {
                        var drawing = drawingController.drawings[svgId];
                        delete drawing.pkeyValues['filename'];
                        drawingController.showDrawing(drawing);
                    }
                },
                onClickAssetAfter: function (drawingController, svgId, selectedAssetId, assetType, selectedAssets, selected){
                	var pk = selectedAssetId.split(';');	
                	var flRestriction = new Ab.view.Restriction();
                	var controller = View.controllers.get('abHazmatPlans');
                	flRestriction.addClause('rm.bl_id', controller.blId);
                	flRestriction.addClause('rm.fl_id', controller.flId);
                	
                	 if (selected) {
                	        var restriction = new Ab.view.Restriction();
                	    	//evaluate pk to determine which kind of asset graphic was clicked and process accordingly
                	    	if(pk.length == 1){
                	    		restriction.addClause('rm.regcomp_id', pk[0]);
                	    	}
                	    	else{
                	    		restriction.addClause('rm.bl_id', pk[0]);
                	            restriction.addClause('rm.fl_id', pk[1]);
                	            restriction.addClause('rm.rm_id', pk[2]);	
                	    	}
                	        View.panels.get('abEgressPlans_regdetailGrid').refresh(restriction);
                	    } else {
                	        View.panels.get('abEgressPlans_regdetailGrid').refresh(flRestriction);
                	    }
                }
            }
        });
    },
    
    /**
     * Load drawing.
     */
    loadDrawing: function (blId, flId, drawingName) {
    	this.blId = blId;
    	this.flId = flId;
    	this.dwgname = drawingName;
    	var hazmatIds = this.getHazmatIds(blId, flId);
    	var rmIds = this.getRmIds(blId, flId);
        this.drawingController.showDrawing({
            pkeyValues: {blId: blId, flId: flId, filename: drawingName + this.drawing_suffix},
            drawingName: drawingName,
            highlightAssets: [{
            					  assetType: 'regcompliance',
            					  assetIds: hazmatIds,
            					  color: 'yellow'
                              },
                              {
                            	  assetType: 'rm',
            					  assetIds: rmIds,
            					  color: 'yellow'
                              }]
        });
    },
    
    
    getHazmatIds : function(blId, flId){
    	var restriction = new Ab.view.Restriction();
        restriction.addClause("regcompliance.bl_id", blId);
        restriction.addClause("regcompliance.fl_id", flId);
    	var records = this.abEgressPlans_dwg_hl_regComp.getRecords(restriction);
    	var hazmatIds = [];
    	for (var i=0; i<records.length; i++){
    		var hazmatId = records[i].getValue('regcompliance.regcomp_id');
    		hazmatIds.push(hazmatId);
    	}
    	return hazmatIds;
    },
    getRmIds: function(blId, flId){
    	var restriction = new Ab.view.Restriction();
        restriction.addClause("rm.bl_id", blId);
        restriction.addClause("rm.fl_id", flId);
    	var records = this.abEgressPlans_drawing_rmHighlight.getRecords(restriction);
    	var rmIds = [];
    	for (var i=0; i<records.length; i++){
    		var rmId = records[i].getValue('rm.rm_id');
    		rmIds.push(rmId);
    	}
    	return rmIds;
    }
});

var buildingId = null;
var floorId = null;
var dwgname = null;
var selectedRow = [];

//if the first building has the floor ,display them
function blPanelAfterRefresh(){
    var blPanel = View.panels.get('abEgressPlans-select-building');
   
    var rows = blPanel.rows;
    if (rows.length > 0) {
        var blId = rows[0]['bl.bl_id'];
        var blRes = new Ab.view.Restriction();
        blRes.addClause('fl.bl_id', blId, '=');
        View.panels.get('abEgressPlans_select_flooring').refresh(blRes);
    }
}

function showDrawing(){
    var buildingDrawing = View.panels.get('abEgressPlans_select_flooring');
    var selectedIndex = buildingDrawing.selectedRowIndex;
    buildingId = buildingDrawing.rows[selectedIndex]["fl.bl_id"];
    floorId = buildingDrawing.rows[selectedIndex]["fl.fl_id"];
    dwgname = buildingDrawing.rows[selectedIndex]["fl.dwgname"];
    disPlayDrawing();
} 

function disPlayDrawing(){
    if(!buildingId || !floorId){
        return;
    }
   
    var restriction = new Ab.view.Restriction();
    restriction.addClause('rm.bl_id', buildingId, '=');
    restriction.addClause('rm.fl_id', floorId, '=');
        
    View.panels.get('abEgressPlans_regdetailGrid').refresh(restriction); 
    View.panels.get('abEgressPlans_regdetailGrid').setTitle(getMessage("detailPanelTitle_hazMat"));
    View.panels.get('abEgressPlans_regdetailGrid').show(true);
    
    controller.loadDrawing(buildingId,floorId,dwgname);

} 
 

/**
* on click event handler when click drawing room
*/
function onRoomClicked(pk, selected) {
	var flRestriction = "rm.bl_id='" + buildingId + "' AND " + "rm.fl_id='" + floorId + "' ";
	  if(selectedRow.length !=0){
	        View.getControl('', 'abEgressPlans_DrawingPanel').unselectAssets(selectedRow);
	        selectedRow = [];
	    }    
    if (selected) {
        var restriction = new Ab.view.Restriction();
    	//evaluate pk to determine which kind of asset graphic was clicked and process accordingly
    	if(pk.length == 1){
    		restriction.addClause('rm.regcomp_id', pk[0]);
    	}
    	else{
    		restriction.addClause('rm.bl_id', pk[0]);
            restriction.addClause('rm.fl_id', pk[1]);
            restriction.addClause('rm.rm_id', pk[2]);	
    	}
        View.panels.get('abEgressPlans_regdetailGrid').refresh(restriction);
    } else {
        View.panels.get('abEgressPlans_regdetailGrid').refresh(flRestriction);
    }
}

/**
* on click event handler of row click event for grid
* abEgressPlans_regdetailGrid
*/
function selectRoom(row) {
    //var opts = new DwgOpts();       
    var selectedRecord = row.row.getRecord();
    var regcomp = selectedRecord.getValue('regcompliance.regcomp_id'); 

    var selectController = controller.drawingController.svgControl.getController("SelectController");
    var svgId = controller.drawingController.config.container + '-' + controller.dwgname + '-svg';
    var assetId = '';
    if(regcomp){
    	assetId = selectedRecord.getValue('regcompliance.regcomp_id');
    } else {
    	assetId = selectedRecord.getValue('rm.bl_id') + ';' + selectedRecord.getValue('rm.fl_id') + ';' + selectedRecord.getValue('rm.rm_id');
    }    
    if (valueExists(selectController.selectedAssets[svgId]) && selectController.selectedAssets[svgId].indexOf(assetId) >= 0) { // avoid same room selection
            // do nothing
    } else {
        selectController.toggleAssetSelection(svgId, assetId); 
    }
}
