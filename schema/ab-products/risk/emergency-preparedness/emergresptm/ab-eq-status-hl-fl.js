
var controller = View.createController('abEgressPlans', {

    //----------------event handle--------------------
    afterViewLoad: function(){
        //hide several panel
        //this.abEgressPlans_DrawingPanel.appendInstruction("default", "", getMessage('dPTitle_equip'));
    	this.initDrawingControl();
        this.abEgressPlans_eqdetailGrid.show(false);  
        // apply custom rule set for highlight
        //ABEP_appendRuleSetForRecoveryStatus("eq", this.abEgressPlans_DrawingPanel, this.abEgressPlans_drawing_eqHighlight);
    }, 
	
	afterInitialDataFetch: function() {
		this.abEgressPlans_select_flooring.refresh("fl.bl_id=''");	
	},
	/**
     * Init drawing control.
     */
    initDrawingControl: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['eq'],
            allowMultipleDrawings: false,
            showPanelTitle: true,
            highlightParameters: [{
                'view_file': "ab-eq-status-hl-fl.axvw",
                'hs_ds': "abEgressPlans_drawing_eqHighlight",
                'label_ds': 'abEgressPlans_drawing_eqLabel',
                'label_ht': '0.60',
                showSelector: false
            }],
            layersPopup: {
                layers: "rm-assets;rm-labels;activity_log-assets;activity_log-labels;gros-labels;gros-assets;background;eq-assets;eq-labels",
                defaultLayers: "rm-assets;rm-labels;activity_log-assets;activity_log-labels;background;eq-assets;eq-labels"
            },
            assetTooltip: [
                {
                    assetType: 'eq',
                    datasource: 'abEgressPlans_drawing_eqLabel',
                    fields: 'eq.bl_id;eq.fl_id;eq.rm_id;eq.eq_id;eq.recovery_status'
                    
                }
            ]
        });
    },
    
    /**
     * Load drawing.
     */
    loadDrawing: function (blId, flId, drawingName) {
        this.drawingController.showDrawing({
            pkeyValues: {blId: blId, flId: flId},
            drawingName: drawingName,
            highlightAssets: [{
            assetType: 'regcompliance',
            assetIds: ['13','12'],
            color: 'blue' 
            }]
        });
    }
    
    
});


var buildingId = null;
var floorId = null;
var dwgname = null;

//if the first building has the floor ,display them
function blPanelAfterRefresh(){
    var blPanel = View.panels.get('abEgressPlans-select-building');
    
    var rows = blPanel.rows;
    if (rows.length > 0) {
        var blId = rows[0]['bl.bl_id'];
        var blRes = new Ab.view.Restriction();
        blRes.addClause('fl.bl_id', blId, '=');
        View.panels.get('abEgressPlans_select_flooring').refresh(blRes);
    }
}

function showDrawing(){
    
    var buildingDrawing = View.panels.get('abEgressPlans_select_flooring');
	var selectedIndex = buildingDrawing.selectedRowIndex;
    buildingId = buildingDrawing.rows[selectedIndex]["fl.bl_id"];
    floorId = buildingDrawing.rows[selectedIndex]["fl.fl_id"];
	dwgname = buildingDrawing.rows[selectedIndex]["fl.dwgname"];
    
    disPlayDrawing();
} 


/**button for the select layer and assettype of the zone and recompliance
 *
 * */
function disPlayDrawing(){
	if(!buildingId || !floorId){
		return;
	}
/*    var drawingPanel = View.panels.get('abEgressPlans_DrawingPanel');
    drawingPanel.clear();*/
 
	/*title = String.format(getMessage('dPTitle_equip') + " : " + buildingId + "-" + floorId);
    addDrawingByType(null, 'eq', null, 'abEgressPlans_drawing_eqHighlight', 'abEgressPlans_drawing_eqLabel', 'abEgressPlans_eqdetailGrid')*/
       
    View.panels.get('abEgressPlans_eqdetailGrid').show(true);
    var restriction = new Ab.view.Restriction();  
    restriction.addClause('eq.bl_id', buildingId, '=');
    restriction.addClause('eq.fl_id', floorId, '=');
    View.panels.get('abEgressPlans_eqdetailGrid').refresh(restriction);
    controller.loadDrawing(buildingId,floorId,dwgname);
    
  /*  drawingPanel.appendInstruction("default", "", title);
    drawingPanel.processInstruction("default", "");*/
}

 
/*
 set the asset ,currentHighlightDS ,currentLabelsDS  and locate it to the json and swf file
 */
function addDrawingByType(assetTypesSuffix, tablename, backgroundSuffix, currentHighlightDS, currentLabelsDS, detailgrid, resValue){
    var drawingPanel = View.panels.get('abEgressPlans_DrawingPanel');
    
    var opts = new DwgOpts();
    
    var restriction = new Ab.view.Restriction();
    
    var assetType = tablename;

    restriction.addClause(tablename + '.bl_id', buildingId, '=');
    restriction.addClause(tablename + '.fl_id', floorId, '=');
    drawingPanel.clear();
 
 
    drawingPanel.assetTypes = assetType; //tablename;

    drawingPanel.currentHighlightDS = currentHighlightDS;
    
    drawingPanel.currentLabelsDS = currentLabelsDS;
	
    var dcl = new Ab.drawing.DwgCtrlLoc(buildingId, floorId, null, dwgname);
    drawingPanel.addDrawing.defer(200, drawingPanel, [dcl, opts]); 
    View.panels.get(detailgrid).refresh(restriction);    
    
}


