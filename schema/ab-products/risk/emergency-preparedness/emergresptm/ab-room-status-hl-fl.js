var controller = View.createController('abHazmatPlans', {
	drawingController : null,
	buildingId : null,
	floorId : null,
	dwgname : null,
	svgId : null,
    //----------------event handle--------------------
    afterViewLoad: function(){
        this.abEgressPlans_rmdetailGrid.show(false);
        this.initDrawingControl();
    }, 
	
	afterInitialDataFetch: function() {
		this.abEgressPlans_select_flooring.refresh("fl.bl_id=''");			
	},
	/**
     * Init drawing control.
     */
    initDrawingControl: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['rm'],
            allowMultipleDrawings: false,
            orderByColumn: false,
            bordersHighlightSelector: true,
            showPanelTitle: true,
            diagonalSelection:  {borderWidth: 15},
            highlightParameters: [{
                'view_file': "ab-room-status-hl-fl.axvw",
                'hs_ds': "abEgressPlans_drawing_rmHighlight",
                'label_ds': 'abEgressPlans_drawing_rmLabel',
                'label_ht': '0.60',
                showSelector: false
            }],
            layersPopup: {
                layers: "rm-assets;rm-labels;activity_log-assets;activity_log-labels;gros-labels;background",
                defaultLayers: "rm-assets;rm-labels;activity_log-assets;activity_log-labels;gros-assets;background"
            },
            assetTooltip: [
                {
                    assetType: 'rm',
                    datasource: 'abEgressPlans_drawing_rmLabel',
                    fields: 'rm.rm_id;rm.rm_type;rm.area;rm.recovery_status'
                }
            ],
            listeners: {
                afterDrawingLoaded: function (drawingController, svgId) {
                    var controller = View.controllers.get('abHazmatPlans');
                    controller.svgId = svgId;
                }
            }
        });
    },
    /**
     * Load drawing.
     */
    loadDrawing: function () {
        this.drawingController.showDrawing({
            pkeyValues: {blId: this.buildingId, flId: this.floorId},
            drawingName: this.dwgname
        });
        this.highlightRooms();     
    },
    
    highlightRooms : function(){
    	var highlightController = this.drawingController.svgControl.getController("HighlightController");
        var restriction = new Ab.view.Restriction();
        restriction.addClause("rm.bl_id", this.buildingId);
        restriction.addClause("rm.fl_id", this.floorId);
        var records = this.abEgressPlans_drawing_rmHighlight.getRecords(restriction);
        
        for(var i=0;i<records.length; i++){
        	var rec = records[i];
        	var assetId = this.buildingId + ";" + this.floorId + ";" + rec.getValue('rm.rm_id');
        	var color = '';
        	switch(rec.getValue('rm.recovery_status')){
        		case 'NONE' : 
        			color = '#C0C0C0';
        			break;
        		case 'NONE-REVIEW' : 
        			color = '#8080C0';
        			break;
        		case 'FIT-ONLINE' : 
        			color = '#00FF00';
        			break;
        		case 'FIT-OFFLINE' : 
        			color = '#80FFFF';
        			break;
        		case 'UNFIT-TEMP' : 
        			color = '#FF9933';
        			break;
        		case 'UNFIT-PERM' : 
        			color = '#FF0000';
        	}
        	highlightController.highlightAsset(assetId,{
        		svgId: this.svgId,
                color: color,
                persistFill: true,
                overwriteFill: true
        	});
        	
        }
    },
    
});


var buildingId = null;
var floorId = null;
var dwgname = null;

//if the first building has the floor ,display them
function blPanelAfterRefresh(){
    var blPanel = View.panels.get('abEgressPlans-select-building');
 
    var rows = blPanel.rows;
    if (rows.length > 0) {
        var blId = rows[0]['bl.bl_id'];
        var blRes = new Ab.view.Restriction();
        blRes.addClause('fl.bl_id', blId, '=');
        View.panels.get('abEgressPlans_select_flooring').refresh(blRes);
    }
}

function showDrawing(){

    var buildingDrawing = View.panels.get('abEgressPlans_select_flooring');
	var selectedIndex = buildingDrawing.selectedRowIndex;
    buildingId = buildingDrawing.rows[selectedIndex]["fl.bl_id"];
    floorId = buildingDrawing.rows[selectedIndex]["fl.fl_id"];
    dwgname = buildingDrawing.rows[selectedIndex]["fl.dwgname"];
    controller.buildingId = buildingId;
    controller.floorId = floorId;
    controller.dwgname = dwgname;
	
    disPlayDrawing();
}
 

function disPlayDrawing(){
	if(!buildingId || !floorId){
		return;
	}

    var restriction = new Ab.view.Restriction();
    restriction.addClause('rm.bl_id', buildingId, '=');
    restriction.addClause('rm.fl_id', floorId, '=');
    View.panels.get('abEgressPlans_rmdetailGrid').refresh(restriction);
    
    View.panels.get('abEgressPlans_rmdetailGrid').show(true);

}


