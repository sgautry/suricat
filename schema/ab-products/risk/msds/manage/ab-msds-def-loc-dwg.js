/**
 * @author Guo Jiangtao
 * Modified V.22.1 Eric_Maxfield@archibus.com
 */
View.createController('abRiskMsdsDefLocDwgController', {
    drawingController: null,
    /**
     * selected floors
     */
    selectedFloors: [],
    /**
     * selected rows in msds grid
     */
    selectedMSDSRows: [],
    /**
     * selected room primary key
     */
    selectedRoomPK: null,
    afterViewLoad: function () {
        this.initDrawingControl();
        // set the all tree level as multi-selected
        this.abRiskMsdsDefLocDwgTreeBl.setMultipleSelectionEnabled(0);
        this.abRiskMsdsDefLocDwgTreeBl.setMultipleSelectionEnabled(1);
        this.abRiskMsdsDefLocDwgTreeBl.setMultipleSelectionEnabled(2);
        this.abRiskMsdsDefLocDwgGridMsds.addEventListener('onMultipleSelectionChange', this.onSelectMaterials.createDelegate(this));
    },
    afterInitialDataFetch: function () {
        this.abRiskMsdsDefLocDwgGridMsds.enableSelectAll(false);
    },
    /**
     * Init drawing control.
     */
    initDrawingControl: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['rm'],
            allowMultipleDrawings: true,
            multipleSelectionEnabled: false,
            orderByColumn: true,
            bordersHighlightSelector: true,
            diagonalSelection: {borderWidth: 15},
            highlightParameters: [{
                'view_file': "ab-msds-def-loc-dwg.axvw",
                'hs_ds': "abRiskMsdsDefLocDwgHlTypeDS",
                'label_ds': 'abRiskMsdsDefLocDwgLabelTypeDS',
                showSelector: false
            }],
            layersPopup: {
                layers: "rm-assets;rm-labels;gros-assets;gros-labels;background;background",
                defaultLayers: "rm-assets;rm-labels;background"
            },
            assetTooltip: [
                {
                    assetType: 'rm',
                    datasource: 'abRiskMsdsDefLocDwgLabelTypeDS',
                    fields: 'rm.bl_id;rm.fl_id;rm.rm_id;rm.rm_type;rm.rm_use;rm.count_em'
                }
            ],
            reports: [{
                text: "DOC", listener: function () {
                    View.controllers.get('abRiskMsdsDefLocDwgController').onSelectedDrawingsPaginatedReport(this);
                }
            }],
            listeners: {
                afterDrawingLoaded: function (drawingController) {
                    drawingController.svgControl.getController("SelectController").setMultipleSelection(false);
                },
                // called after the default onClickAsset event
                onClickAssetAfter: function (drawingController, svgId, selectedAssetId, assetType, selectedAssets, selected) {
                    View.controllers.get('abRiskMsdsDefLocDwgController').onRoomClicked(drawingController, svgId, selectedAssetId, assetType, selectedAssets, selected);
                }
            }
        });
    },
    /**
     * Load drawing.
     */
    loadDrawing: function (blId, flId, drawingName) {
        this.drawingController.showDrawing({
            pkeyValues: {blId: blId, flId: flId},
            drawingName: drawingName
        });
    },
    abRiskMsdsDefLocDwgTreeBl_onShowSeletedFloorPlan: function () {
        if (this.checkSelection()) {
            this.drawingController.clearDrawings();
            // get all selected floor plans
            this.selectedFloorPlans = [];
            this.selectedFloors = [];
            var flNodes = this.abRiskMsdsDefLocDwgTreeBl.getSelectedNodes(2);
            for ( var i = 0; i < flNodes.length; i++ ) {
                var floorPlan = {};
                floorPlan['bl_id'] = flNodes[i].parent.data['bl.bl_id'];
                floorPlan['fl_id'] = flNodes[i].data["fl.fl_id"];
                floorPlan['dwgname'] = flNodes[i].data["fl.dwgname"];
                this.selectedFloorPlans.push(floorPlan);
                this.selectedFloors.push(floorPlan);
                this.loadDrawing(floorPlan['bl_id'], floorPlan['fl_id'], floorPlan['dwgname']);
            }
            this.drawingController.setDrawingPanelTitle(getMessage('selectMsds'));
            //show msds location list
            this.showLocationGrid();
        }
    },
    /**
     * check the tree selection
     */
    checkSelection: function () {
        // get all selected sites nodes
        var siteNodes = this.abRiskMsdsDefLocDwgTreeBl.getSelectedNodes(0);
        for ( var i = 0; i < siteNodes.length; i++ ) {
            // if site node not expand, refresh it to load children
            if (siteNodes[i].children.length === 0) {
                this.abRiskMsdsDefLocDwgTreeBl.refreshNode(siteNodes[i]);
            }
            // make all building nodes in this site selected
            var childrenSiteNodes = siteNodes[i].children;
            for ( var j = 0; j < childrenSiteNodes.length; j++ ) {
                if (!childrenSiteNodes[j].isSelected) {
                    childrenSiteNodes[j].setSelected(true);
                }
            }
        }
        // get all selected building nodes
        var blNodes = this.abRiskMsdsDefLocDwgTreeBl.getSelectedNodes(1);
        for ( var i = 0; i < blNodes.length; i++ ) {
            // if building node not expand, refresh it to load children
            if (blNodes[i].children.length === 0) {
                this.abRiskMsdsDefLocDwgTreeBl.refreshNode(blNodes[i]);
            }
            // make all floor nodes in this building selected
            var flNodes = blNodes[i].children;
            for ( var m = 0; m < flNodes.length; m++ ) {
                if (!flNodes[m].isSelected) {
                    flNodes[m].setSelected(true);
                }
            }
        }
        // get all floor nodes, if no one selected, give a warning
        if (this.abRiskMsdsDefLocDwgTreeBl.getSelectedNodes(2).length === 0) {
            View.showMessage(getMessage('error_noselection'));
            return false;
        }
        return true;
    },
    onSelectMaterials: function (row) {
        var grid = View.panels.get('abRiskMsdsDefLocDwgGridMsds');
        var rows = grid.getSelectedRows();
        this.selectedMSDSRows = rows;
        if (rows.length > 0) {
            if (this.drawingController.svgControl.drawingController.isDrawingsLoaded) {
                var msdsRes = "";
                for ( var i = 0; i < rows.length; i++ ) {
                    var msdsRow = rows[i];
                    var msds = msdsRow['msds_data.msds_id'];
                    if (msdsRow['msds_data.msds_id.raw']) {
                        msds = msdsRow['msds_data.msds_id.raw'];
                    }
                    msdsRes += "OR (msds_location.msds_id=" + msds + ")"
                }
                msdsRes = "(" + msdsRes.substring(2) + ")";
                this.drawingController.addHighlightDataSourceParameter({
                    'msdsRes': msdsRes
                });
                this.reloadHighlight();
                this.drawingController.setDrawingPanelTitle(getMessage('selectRoom'));
            }
        } else {
            if (this.drawingController.svgControl.drawingController.isDrawingsLoaded) {
                this.drawingController.addHighlightDataSourceParameter({
                    'msdsRes': '1=1'
                });
                this.reloadHighlight();
            }
        }
        this.showLocationGrid();
    },
    reloadHighlight: function () {
        var control = this.drawingController.svgControl.drawingController.getControl();
        //reload the drawings
        for ( var drawingName in control.drawings ) {
            var parameters = this.drawingController.svgControl.config;
            parameters['pkeyValues'] = control.drawings[drawingName].config['pkeyValues'];
            parameters['drawingName'] = drawingName;
            parameters['callFromDatasourceSelector'] = 'true';
            control.reload(parameters);
        }
    },
    abRiskMsdsDefLocDwgGridMsds_afterRefresh: function () {
        var grid = this.abRiskMsdsDefLocDwgGridMsds;
        var allRows = this.abRiskMsdsDefLocDwgGridMsds.rows;
        // KB 3048457 - Close the edit form if there are no longer any selected rows (e.g., if user changes all items' location to a value outside the location panel selection)
        if (allRows.length === 0) {
            this.abRiskMsdsDefLocDwgAssignmentForm.closeWindow();
        } else {
            var selectedRows = this.selectedMSDSRows;
            for ( var i = 0; i < selectedRows.length; i++ ) {
                var index = selectedRows[i].row.getIndex();
                grid.selectRowChecked(index, true);
            }
        }
    },
    /**
     * This event handler for abRiskMsdsDefLocDwgAssignmentForm maintains a relationship between the container ID and
     * number of containers fields wherein the number of containers must be one if the record has a container ID value defined.
     */
    abRiskMsdsDefLocDwgAssignmentForm_afterRefresh: function (panelForm) {
        var containerCode = panelForm.getFieldValue('msds_location.container_code');
        var numberContainers = panelForm.getFieldValue('msds_location.num_containers');
        if (numberContainers == 1 || !(valueExistsNotEmpty(numberContainers))) {
            panelForm.enableField('msds_location.container_code', true);
        } else {
            panelForm.enableField('msds_location.container_code', false);
            panelForm.enableField('msds_location.num_containers', true);
        }
        if (!(valueExistsNotEmpty(containerCode))) {
            panelForm.enableField('msds_location.num_containers', true);
        } else {
            panelForm.setFieldValue('msds_location.num_containers', '1');
            panelForm.enableField('msds_location.num_containers', false);
        }
    },
    /**
     * on click event handler of button 'Dispose'
     */
    disposeSelected: function () {
        var doIt = confirm(getMessage("prompt_disposeSelected"));
        if (!doIt) return;
        // first, update and save the record with status Disposed
        var assignmentRecords = this.abRiskMsdsDefLocDwgAssignment.getSelectedRecords();
        if (assignmentRecords.length > 0) {
            var currentDateObj = new Date();
            var currentDate = this.abRiskMsdsDefLocsAssignmentDS.formatValue("msds_location.date_updated", currentDateObj, false);
            // loop all selected assignments, set status to Disposed, save, then delete them
            for ( var i = 0; i < assignmentRecords.length; i++ ) {
                var record = assignmentRecords[i];
                record.setValue("msds_location.date_updated", currentDate);
                record.setValue("msds_location.last_edited_by", View.user.name);
                record.setValue("msds_location.container_status", "DISPOSED");
                this.abRiskMsdsDefLocsAssignmentDS.saveRecord(record);
            }
        }
        // now delete the records
        var assignmentRows = this.abRiskMsdsDefLocDwgAssignment.getSelectedRows();
        if (assignmentRows.length > 0) {
            // loop all selected assignment and delete them
            for ( var i = 0; i < assignmentRows.length; i++ ) {
                var autoNumber = assignmentRows[i]['msds_location.auto_number'];
                if (assignmentRows[i]['msds_location.auto_number.raw']) {
                    autoNumber = assignmentRows[i]['msds_location.auto_number.raw'];
                }
                var record = new Ab.data.Record();
                record.isNew = false;
                record.setValue("msds_location.auto_number", autoNumber);
                this.abRiskMsdsLocationDS.deleteRecord(record);
            }
            // refresh the assignment list and hide the edit form
            this.abRiskMsdsDefLocDwgAssignment.refresh(this.abRiskMsdsDefLocDwgAssignment.restriction);
        }
        this.reloadHighlight();
    },
    /**
     * Execute when user clicks 'Unassign' action button
     */
    unassignSelected: function () {
        var doIt = confirm(getMessage("prompt_unassignSelected"));
        if (!doIt) return;
        var assignmentRecords = this.abRiskMsdsDefLocDwgAssignment.getSelectedRecords();
        if (assignmentRecords.length > 0) {
            var currentDateObj = new Date();
            var currentDate = this.abRiskMsdsDefLocsAssignmentDS.formatValue("msds_location.date_updated", currentDateObj, false);
            // loop all selected assignments, set status to Disposed, save, then delete them
            for ( var i = 0; i < assignmentRecords.length; i++ ) {
                var record = assignmentRecords[i];
                record.setValue("msds_location.date_updated", currentDate);
                record.setValue("msds_location.last_edited_by", View.user.name);
                record.setValue("msds_location.eq_id", "");
                record.setValue("msds_location.bin_id", "");
                record.setValue("msds_location.shelf_id", "");
                record.setValue("msds_location.cabinet_id", "");
                record.setValue("msds_location.aisle_id", "");
                record.setValue("msds_location.rm_id", "");
                record.setValue("msds_location.fl_id", "");
                this.abRiskMsdsDefLocsAssignmentDS.saveRecord(record);
            }
            // refresh the assignment list and hide the edit form
            this.abRiskMsdsDefLocDwgAssignment.refresh(this.abRiskMsdsDefLocDwgAssignment.restriction);
            this.reloadHighlight();
        }
    },
    /**
     *   This event handler adds the current user name and date to the form before saving.
     *   It also prompts the user to confirm save (=unassign/delete) when status is set to "Disposed."
     */
    abRiskMsdsDefLocDwgAssignmentForm_onUpdate: function () {
        var form = this.abRiskMsdsDefLocDwgAssignmentForm;
        var currentDateObj = new Date();
        var currentDate = this.abRiskMsdsDefLocsAssignmentDS.formatValue("msds_location.date_updated", currentDateObj, false);
        form.setFieldValue("msds_location.date_updated", currentDate);
        form.setFieldValue('msds_location.last_edited_by', this.view.user.name);
        var containerStatus = form.getFieldValue("msds_location.container_status");
        if (containerStatus === "DISPOSED") {
            var doIt = confirm(getMessage("prompt_disposeEditForm"));
            if (!doIt) {
                return false;
            } else {
                var autoNumber = form.getRecord().getValue('msds_location.auto_number');
                var record = new Ab.data.Record();
                record.isNew = false;
                record.setValue("msds_location.auto_number", autoNumber);
                this.abRiskMsdsLocationDS.deleteRecord(record);

                // refresh the assignment list and hide the edit form
                this.abRiskMsdsDefLocDwgAssignment.refresh(this.abRiskMsdsDefLocDwgAssignment.restriction);
                this.reloadHighlight();
                this.abRiskMsdsDefLocDwgAssignmentForm.closeWindow();
            }
        } else {
            if (form.save()) {
                this.abRiskMsdsDefLocDwgAssignment.refresh(this.abRiskMsdsDefLocDwgAssignment.restriction);
                this.reloadHighlight();
                this.abRiskMsdsDefLocDwgAssignmentForm.closeWindow();
            }
        }
    },
    abRiskMsdsDefLocDwgGridMsds_onClearMSDS: function () {
        var grid = View.panels.get('abRiskMsdsDefLocDwgGridMsds');
        var rows = grid.getSelectedRows();
        if (rows.length > 0) {
            if (this.drawingController.svgControl.drawingController.isDrawingsLoaded) {
                this.drawingController.addHighlightDataSourceParameter({
                    'msdsRes': '1=1'
                });
                this.reloadHighlight();
                this.drawingController.setDrawingPanelTitle(getMessage('selectMsds'));
            }
            this.selectAllMSDS(false);
        }
    },
    /**
     *
     *Click room event on drawing.
     */
    onRoomClicked: function (drawingController, svgId, selectedAssetId, assetType, selectedAssets, selected) {
        if (selected) {
            var dataSource = View.dataSources.get('abRiskMsdsDefLocsAssignmentDS');
            var pk = selectedAssetId.split(';');
            var selectedRows = this.selectedMSDSRows;
            if (selectedRows.length > 0) {
                // loop all selected msds and add records to msds_location
                for ( var i = 0; i < selectedRows.length; i++ ) {
                    var msds = selectedRows[i]['msds_data.msds_id'];
                    if (selectedRows[i]['msds_data.msds_id.raw']) {
                        msds = selectedRows[i]['msds_data.msds_id.raw'];
                    }
                    if (!recordExists(pk, msds)) {
                        var rec = new Ab.data.Record();
                        rec.isNew = true;
                        rec.setValue("msds_location.msds_id", msds);
                        rec.setValue("msds_location.bl_id", pk[0]);
                        rec.setValue("msds_location.fl_id", pk[1]);
                        rec.setValue("msds_location.rm_id", pk[2]);
                        rec.setValue("msds_location.site_id", getSiteCode(pk[0]));
                        rec.setValue("msds_location.last_edited_by", View.user.name);
                        dataSource.saveRecord(rec);
                    }
                }
                this.reloadHighlight();
                this.drawingController.setDrawingPanelTitle(getMessage('selectRoom'));
            } else {
                this.selectedRoomPK = pk;
            }
        } else {
            this.selectedRoomPK = null;
        }
        // refresh the assignment panels restricted by the selected locations
        this.showLocationGrid();
        this.selectedRoomPK = null;
    },
    /**
     * unselect all msds rows
     */
    selectAllMSDS: function (selected, disableListener) {
        var dataRows = this.abRiskMsdsDefLocDwgGridMsds.getDataRows();
        for ( var r = 0; r < dataRows.length; r++ ) {
            var dataRow = dataRows[r];
            var selectionCheckbox = dataRow.firstChild.firstChild;
            selectionCheckbox.checked = selected;
        }
        if (disableListener) {
            return;
        }
        var listener = this.abRiskMsdsDefLocDwgGridMsds.getEventListener('onMultipleSelectionChange');
        if (listener) {
            listener();
        }
    },
    /**
     * add first drawing to the drawing panel
     */
    addFirstDrawing: function () {
        if (this.selectedFloorPlans.length > 0) {
            this.drawingController.clearDrawings();
            this.floorPlan = this.selectedFloorPlans[0];
            this.loadDrawing(this.floorPlan['bl_id'], this.floorPlan['fl_id'], this.floorPlan['dwgname']);
            this.selectedFloorPlans.shift();
        }
    },
    /**
     * show location grid
     */
    showLocationGrid: function () {
        var restriction = " 1=1 ";
        if (this.selectedRoomPK) {
            restriction += " and (msds_location.bl_id='" + this.selectedRoomPK[0] + "'"
                + " AND msds_location.fl_id='" + this.selectedRoomPK[1] + "'"
                + " AND msds_location.rm_id='" + this.selectedRoomPK[2] + "')";
        } else if (this.selectedFloors.length > 0) {
            var floorRes = "";
            for ( var i = 0; i < this.selectedFloors.length; i++ ) {
                var floor = this.selectedFloors[i];
                floorRes += "OR (msds_location.bl_id='" + floor['bl_id'] + "'"
                    + " AND msds_location.fl_id='" + floor['fl_id'] + "')";
            }
            restriction += " and (" + floorRes.substring(2) + ")";
        }
        if (this.selectedMSDSRows.length > 0) {
            var msdsRes = "";
            for ( var i = 0; i < this.selectedMSDSRows.length; i++ ) {
                var msdsRow = this.selectedMSDSRows[i];
                var msds = msdsRow['msds_data.msds_id'];
                if (msdsRow['msds_data.msds_id.raw']) {
                    msds = msdsRow['msds_data.msds_id.raw'];
                }
                msdsRes += "OR (msds_location.msds_id=" + msds + ")"
            }
            restriction += " and (" + msdsRes.substring(2) + ")";
        }
        this.abRiskMsdsDefLocDwgAssignment.refresh(restriction)
    },

    /**
     * on click event handler when click tree node
     */
    onTreeClick: function (ob) {
        var treePanel = View.panels.get('abRiskMsdsDefLocDwgTreeBl')
        var currentNode = treePanel.lastNodeClicked;
        this.selectedFloorPlans = [];
        this.selectedFloors = [];
        this.selectedRoomPK = null;
        var floorPlan = {};
        floorPlan['bl_id'] = currentNode.parent.data['bl.bl_id'];
        floorPlan['fl_id'] = currentNode.data["fl.fl_id"];
        floorPlan['dwgname'] = currentNode.data["fl.dwgname"];
        this.selectedFloorPlans.push(floorPlan);
        this.selectedFloors.push(floorPlan);
        // clear the drawing panel before loading
        this.abRiskMsdsDefLocDwgDrawingPanel.clear();
        this.addFirstDrawing();
        this.showLocationGrid();
    },
    abRiskMsdsDefLocDwgDrawingPanel_onExportDOCX: function () {
        var doIt = confirm(getMessage("z_DRAWING_PRINTING"));
        if (!doIt) return;
        var panel = this.abRiskMsdsDefLocDwgDrawingPanel;
        var jobId = panel.callDOCXReportJob(getMessage("docxTitle"), panel.restriction, {});
        var jobStatus = Workflow.getJobStatus(jobId);
        while (jobStatus.jobFinished != true && jobStatus.jobStatusCode != 8) {
            jobStatus = Workflow.getJobStatus(jobId);
        }
        if (jobStatus.jobFinished) {
            var url = jobStatus.jobFile.url;
            if (valueExistsNotEmpty(url)) {
                window.location = url;
            }
        }
        View.closeProgressBar();
    }
});

/**
 * on click event handler when click row of MSDSs grid
 */
function selectMSDS(context) {
    var grid = View.panels.get(context.command.parentPanelId);
    View.controllers.get('abRiskMsdsDefLocDwgController').selectAllMSDS(false, true);
    grid.selectRowChecked(grid.selectedRowIndex, true);
}

/**
 * on click event handler when click row of MSDS assignment grid
 */
function selectMSDSLocation(context) {
    var record = context.row.getRecord();
    var autoNumber = record.getValue('msds_location.auto_number');
    var detailsPanel = View.panels.get('abRiskMsdsDefLocDwgAssignmentForm');
    detailsPanel.refresh(new Ab.view.Restriction({'msds_location.auto_number': autoNumber}));
    detailsPanel.showInWindow({
        anchor: context,
        width: 800,
        height: 400,
        modal: true
    });
}

/**
 * judge whether the assignment is existed.
 */
function recordExists(location, msds) {
    var dataSource = View.dataSources.get('abRiskMsdsDefLocsAssignmentDS');
    var restriction = 'msds_location.msds_id=' + msds + " AND (msds_location.bl_id='" + location[0] + "'" + " AND msds_location.fl_id='" + location[1] + "'" + " AND msds_location.rm_id='"
        + location[2] + "')";
    return dataSource.getRecords(restriction).length > 0;
}

/**
 * get site code base on building code
 */
function getSiteCode(blId) {
    var siteId = '';
    var dataSource = View.dataSources.get('abRiskMsdsDefLocDwgTreeSiteDS');
    var restriction = "exists(select 1 from bl where bl.site_id = site.site_id and bl.bl_id='" + blId + "')";
    var records = dataSource.getRecords(restriction);
    if (records.length > 0) {
        siteId = records[0].getValue('site.site_id');
    }
    return siteId;
}