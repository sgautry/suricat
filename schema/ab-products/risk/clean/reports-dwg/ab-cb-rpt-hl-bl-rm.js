/**
 * overwrite function to make add city, site etc. on node restriction
 * @param {Object} parentNode
 * @param {Object} level
 */
Ab.tree.TreeControl.prototype._createRestrictionForLevel = function(parentNode, level) {
    var restriction = this.createRestrictionForLevel(parentNode, level);
	
	if (!restriction) {
		restriction = new Ab.view.Restriction();
		// add the tree restriction to parameter list if not null.
		if (this.restriction && this.restriction.clauses != undefined && this.restriction.clauses.length > 0) {
			restriction.addClauses(this.restriction, true);
		}
		
		// add the tree level's restriction to parameter list if not null.
		var levelRest = this.getRestrictionForLevel(level);
		if (levelRest && levelRest.clauses != undefined && levelRest.clauses.length > 0) {
			restriction.addClauses(levelRest, true);
		}
		
		// add the parent node's restriction to parameter list. it should always contain something
		if (!parentNode.isRoot()){
			if(this.type=='hierTree' || this.type=='selectValueHierTree'){
				restriction.addClauses(parentNode.restriction, true);
		    } else {
		    	if (this._panelsData[level].useParentRestriction==true) {
					restriction.addClauses(parentNode.restriction, true);
				}
			}
		}
		// regn level, we must add ctry
		if(level == 2
				&& parentNode.parent.data['ctry.ctry_id'] != undefined){
			restriction.addClause('bl.ctry_id', parentNode.parent.data['ctry.ctry_id']);
		}
		// state level, we must add ctry and regn
		if(level == 3
				&& parentNode.parent.data['regn.regn_id'] != undefined
				&& parentNode.parent.parent.data['ctry.ctry_id'] != undefined){
			restriction.addClause('bl.regn_id', parentNode.parent.data['regn.regn_id']);
			restriction.addClause('bl.ctry_id', parentNode.parent.parent.data['ctry.ctry_id']);
		}
		// city level, we must add ctry, regn and state
		if(level == 4
				&& parentNode.parent.data['state.state_id'] != undefined
				&& parentNode.parent.parent.data['regn.regn_id'] != undefined
				&& parentNode.parent.parent.parent.data['ctry.ctry_id'] != undefined){
			restriction.addClause('bl.state_id', parentNode.parent.data['state.state_id']);
			restriction.addClause('bl.regn_id', parentNode.parent.parent.data['regn.regn_id']);
			restriction.addClause('bl.ctry_id', parentNode.parent.parent.parent.data['ctry.ctry_id']);
		}
		// bldg level, we must add ctry, regn, state and city
		if(level == 5
				&& parentNode.parent.data['city.city_id'] != undefined
				&& parentNode.parent.parent.data['state.state_id'] != undefined
				&& parentNode.parent.parent.parent.data['regn.regn_id'] != undefined
				&& parentNode.parent.parent.parent.parent.data['ctry.ctry_id'] != undefined){
			restriction.addClause('bl.city_id', parentNode.parent.data['city.city_id']);
			restriction.addClause('bl.state_id', parentNode.parent.parent.data['state.state_id']);
			restriction.addClause('bl.regn_id', parentNode.parent.parent.parent.data['regn.regn_id']);
			restriction.addClause('bl.ctry_id', parentNode.parent.parent.parent.parent.data['ctry.ctry_id']);
		}
		// floor level, we must add ctry, regn, state, city and site
		if(level == 6
				&& parentNode.parent.data['site.site_id'] != undefined
				&& parentNode.parent.parent.data['city.city_id'] != undefined
				&& parentNode.parent.parent.parent.data['state.state_id'] != undefined
				&& parentNode.parent.parent.parent.parent.data['regn.regn_id'] != undefined
				&& parentNode.parent.parent.parent.parent.parent.data['ctry.ctry_id'] != undefined){
			restriction.addClause('rm.site_id', parentNode.parent.data['site.site_id']);
			restriction.addClause('rm.city_id', parentNode.parent.parent.data['city.city_id']);
			restriction.addClause('rm.state_id', parentNode.parent.parent.parent.data['state.state_id']);
			restriction.addClause('rm.regn_id', parentNode.parent.parent.parent.parent.data['regn.regn_id']);
			restriction.addClause('rm.ctry_id', parentNode.parent.parent.parent.parent.parent.data['ctry.ctry_id']);
		}
	}
	
	return restriction;
}

View.createController('abCbRptHlBlRmCtrl',{
	blId: null,
	flId: null,
	mapControl: null,
    drawingController: null,
    drawingSvgId: null,
	filterController: null,
	
	pagRepRestriction: null,
	printableRestriction: null,
	defaultPagRepRestriction: "(EXISTS(SELECT 1 FROM rm WHERE rm.bl_id = activity_log.bl_id AND rm.fl_id = activity_log.fl_id AND rm.rm_id = activity_log.rm_id AND rm.dwgname IS NOT NULL)"
			+ " OR activity_log.dwgname IS NOT NULL)"
			+ " AND activity_log.project_id IS NOT NULL"
			+ " AND EXISTS(SELECT 1 FROM project WHERE project.project_id = activity_log.project_id AND project.project_type='ASSESSMENT - HAZMAT' AND project.is_template = 0)",

	paginatedReportName: 'ab-cb-rpt-hl-bl-rm-pgrp.axvw',

	// true if floor selected, false if building selected
	showDrawing: false,
	
	// drawing name for highlighting the assets
	currentDwgName: null,
	
    afterViewLoad: function(){
		var controller = this;

		this.pagRepRestriction = this.defaultPagRepRestriction;

		this.filterController = View.controllers.get("abCbRptCommonFilterCtrl");
    	this.filterController.panelsCtrl = this;
    	this.filterController.visibleFields = "proj";

        /*
         * specify a handler after resize of the drawing (drag of the layout region bar),
         * to hide/show the drawing panel
         */
//        this.abCbRptHlBlRm_drawingPanel.addEventListener('afterResize', function(){
//        	if (!controller.showDrawing){
//        		controller.abCbRptHlBlRm_drawingPanel.parentElement.style.height ="0px";
//    		}
//        });
		
		// set message parameter for abCbRptHlBlRm_ctryTree panel
		this.abCbRptHlBlRm_ctryTree.addParameter('noDrawing',getMessage('noDrawing'));

		// add onMultipel selection change event listener
		this.abCbRptHlBlRm_gridRep.addEventListener('onMultipleSelectionChange', this.onMultipleSelectionChange.createDelegate(this));
    	
		// init map control
        this.initMapControl();
    },

    afterInitialDataFetch: function(){
      	this.showPanels(false, true, true);
    },
    
    initMapControl:function(){	
    	var configObject = new Ab.view.ConfigObject();
        configObject.mapImplementation = 'Esri';
        this.mapControl = new Ab.leaflet.Map('abCbRptHlBlRm_htmlMap', 'abCbRptHlBlRm_objMap', configObject);
 
        var basemapLayerMenu = this.abCbRptHlBlRm_htmlMap.actions.get('basemapLayerMenu');
        basemapLayerMenu.clear();
        var basemapLayers = this.mapControl.getBasemapLayerList();
        for ( var i = 0; i < basemapLayers.length; i++ ) {
            basemapLayerMenu.addAction(i, basemapLayers[i], this.switchBasemapLayer.createDelegate(this));
        }
        this.mapControl.createMarkers(
                'abCbRptHlBlRm_dsBuilding',
                ['bl.bl_id'],
                ['bl.lon', 'bl.lat'],
                'bl.name',
                ['bl.site_id', 'bl.bl_id', 'bl.name', 'bl.address'],
                {
                    markerActionTitle: getMessage('labelShowDetails'),
                    markerActionCallback: this.onBuildingDetails.createDelegate(this)
                }
        );
    },
    
    switchBasemapLayer: function(item) {
    	//switch the map layer based on the passed in layer name.
    	this.mapControl.switchBasemapLayer(item.text);
    },  
    
    initDrawingControl: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            bordersHighlightSelector: true,
            borderSize: 14,
            diagonalSelection:  {borderWidth: 15},
            assetTypes: ['rm', 'activity_log'],
            highlightParameters: [{
                'view_file': "ab-cb-rpt-hl-ds.axvw",
                'hs_ds': "highlightRoomDs",
                'label_ds': 'labelDetailsDs',
                'label_ht': '0.60',
                 showSelector: true
            }],
            assetTooltip: [
                {
                    assetType: 'rm',
                    datasource: 'labelDetailsDs',
                    fields: 'rm.rm_id;rm.rm_cat;rm.rm_type;rm.area'
                }
            ],
            layersPopup: {
                layers: "rm-assets;rm-labels;activity_log-assets;cb_samples-assets;gros-assets;gros-labels;background",
                defaultLayers: "rm-assets;rm-labels;activity_log-assets;background;activity_log-labels;cb_samples-assets;cb_samples-labels;gros-assets"
            },
            reports: [{
                text: "DOC", listener: function () {
                    View.controllers.get('abCbRptHlBlRmCtrl').abCbRptHlBlRm_drawingPanel_onPaginatedReport();
                }
            }],
            showPanelTitle: true,
            listeners: {
                afterDrawingLoaded: function (drawingController, svgId) {
                    var controller = View.controllers.get('abCbRptHlBlRmCtrl');
                    controller.drawingSvgId = svgId;
                    drawingController.svgControl.getController("SelectController").setMultipleSelection(true);
                },
                onClickAssetAfter: function (drawingController, svgId, selectedAssetId, assetType, selectedAssets, selected){
                	var objDetailsGrid = View.panels.get("abCbRptHlBlRm_gridRep"); 
                	var rmIds = [];
                    _.each(selectedAssets, function (selectedAsset) {
                        var selectedRooms = selectedAsset['rm'];
                        if (selectedRooms) {
                            _.each(selectedRooms, function (selectedRoom) {
                                var assetId = selectedRoom.split(';')[2]; //blId;flId;rmId
                                rmIds.push(assetId);
                            });
                        }
                    });
                    if (rmIds.length > 0) {
                    	View.controllers.get('abCbRptHlBlRmCtrl').setPanelFilterValues(objDetailsGrid, _.uniq(rmIds), 'rm.rm_id');
                        objDetailsGrid.refresh(objDetailsGrid.restriction);
                        setAllRowsSelected(objDetailsGrid, true);
                    } else {
                        objDetailsGrid.clearAllFilterValues();
                        objDetailsGrid.refresh(objDetailsGrid.restriction);
                    }
                }
            }
        });
    },
    
    
     
    abCbRptHlBlRm_gridRep_afterRefresh: function(){
		this.abCbRptHlBlRm_gridRep.enableSelectAll(false);
    },
    
    abCbRptHlBlRm_drawingPanel_onPaginatedReport: function() {

        var floorsRestriction = [];        
        floorsRestriction.push({'title': getMessage("floors"), 'value': this.currentDwgName});
        this.printableRestriction = this.filterController.printableRestriction;
    
    	var parameters = {
			'consoleRestriction': this.filterController.getConsoleRestriction(),
			'userAssignedRestriction': this.getFloorsRestriction(),
			'printRestriction': true, 
			'printableRestriction': this.printableRestriction.concat(floorsRestriction)
    	};

    	View.openPaginatedReportDialog(this.paginatedReportName, null, parameters);    	
    },

    getFloorsRestriction: function() {
        var currentNode = this.abCbRptHlBlRm_ctryTree.lastNodeClicked;       
        var dwgRestr = " (activity_log.bl_id = '" + currentNode.data['rm.bl_id'] + "' AND activity_log.fl_id = '" + currentNode.data['rm.fl_id'] + "')";
        return dwgRestr;
    },
    
	/**
	 * Shows the tree according to the user restrictions
	 */
	refreshOnFilter: function(restriction, instrLabels){
        this.abCbRptHlBlRm_ctryTree.addParameter('consoleRestriction', restriction);
        this.abCbRptHlBlRm_ctryTree.refresh();
        this.showPanels(false, false, false);
	},

	/**
	 * show/ hide panels
	 * @param {boolean} showDrawing
	 * @param {boolean} showRep
	 * @param {boolean} showMap
	 */
    showPanels: function(showDrawing, showRep, showMap){
    	this.showDrawing = showDrawing;               
    	this.drawingPanel.show(showDrawing);
        this.abCbRptHlBlRm_gridRep.show(showRep);
        this.abCbRptHlBlRm_htmlMap.show(showMap);
    },
    
    /**
     * Show building details for selected building.
     * @param {Object} node - building node.
     */
    showBuildingDetails: function (node) {
        //get bl id
        var blId = node.restriction.clauses[0].value;
        var currentNode = View.panels.get('abCbRptHlBlRm_ctryTree').lastNodeClicked;
        this.blId = blId;
      //refresh assessments items panel
        var node_rest = new Ab.view.Restriction();
    	node_rest.addClause('activity_log.bl_id', blId, '=');
    	
        var filterRes = this.filterController.restriction ? this.filterController.restriction : "1=1";
        this.abCbRptHlBlRm_gridRep.addParameter("consoleRestriction", filterRes);
        this.abCbRptHlBlRm_gridRep.refresh(node_rest);
        
        // load ESRI mapControl or building details
        var restriction = new Ab.view.Restriction();
        restriction.addClause('bl.bl_id', this.blId, '=');
        if (this.mapControl) {
            this.showPanels(false, true, true);
            this.showBuilding(restriction);
        }
        
        //hide selection checkboxes
        if (this.abCbRptHlBlRm_gridRep.multipleSelectionEnabled) {
        	this.abCbRptHlBlRm_gridRep.showColumn("multipleSelectionColumn", false);
        	this.abCbRptHlBlRm_gridRep.multipleSelectionEnabled = false;    
        	this.abCbRptHlBlRm_gridRep.update();    
        }
    },
    
    /**
     * Show building on map.
     * @param {Ab.view.Restriction} restriction - building restriction.
     */
    showBuilding: function (restriction) {
        this.mapControl.clearMarkers();
        this.mapControl.showMarkers('abCbRptHlBlRm_dsBuilding', restriction);
    },
    /**
     * Show building details map marker action.
     * @param {string} buildingId - Building id.
    */ 
    onBuildingDetails: function (buildingId) {
        var restriction = new Ab.view.Restriction();
        restriction.addClause('bl.bl_id', buildingId, '=');
        this.abCbRptHlBlRm_blDetailsPanel.refresh(restriction);
        this.abCbRptHlBlRm_blDetailsPanel.showInWindow({
            width: 1200,
            closeButton: true
        });
    },
   
    /**
    * Show details for selected floor.
    * @param {Object} node - floor node.
    */
   showFloorDetails: function (node) {
       this.showPanels(true, true, false);
       var currentNode = View.panels.get('abCbRptHlBlRm_ctryTree').lastNodeClicked;
       var cityId = currentNode.data['rm.city_id'];
       var node_rest = new Ab.view.Restriction();
   	   node_rest.addClauses(node.restriction);
       node_rest.removeClause('rm.city_id');
       node_rest.removeClause('rm.rm_id');
       node_rest.addClause('bl.city_id', cityId, '=', true);
       
       var dwgRestr = this.filterController.restriction ? this.filterController.restriction : "1=1";
       this.abCbRptHlBlRm_gridRep.addParameter("consoleRestriction", dwgRestr);
       this.abCbRptHlBlRm_gridRep.refresh(node_rest);

       dwgRestr += " AND " + " (activity_log.bl_id = '" + currentNode.data['rm.bl_id'] + "' AND activity_log.fl_id = '" + currentNode.data['rm.fl_id'] + "')";

   	   this.pagRepRestriction = this.defaultPagRepRestriction + " AND " + dwgRestr;
  
       var currentNode = View.panels.get('abCbRptHlBlRm_ctryTree').lastNodeClicked;
       this.blId = currentNode.data['rm.bl_id'];
       this.flId = currentNode.data['rm.fl_id'];
       this.currentDwgName = currentNode.data['rm.raw_dwgname'];
       
       //show selection checkboxes
       if (!this.abCbRptHlBlRm_gridRep.multipleSelectionEnabled) {
    	   this.abCbRptHlBlRm_gridRep.showColumn("multipleSelectionColumn", true);
    	   this.abCbRptHlBlRm_gridRep.multipleSelectionEnabled = true;    
    	   this.abCbRptHlBlRm_gridRep.update();
       }
       this.displayDrawing(dwgRestr);
   },
   
   /**
    * Displays the drawing, and select asset based on selected radio buttons.
    */
   displayDrawing: function (dwgRestr) {
       if (!valueExists(this.drawingController)) {
           this.initDrawingControl();
       }
       if (!this.flId) {
           View.showMessage(getMessage("noDrawing"));
           return;
       }
       this.drawingController.addHighlightDataSourceParameter({
           'consoleRestriction': dwgRestr
       });
       this.drawingController.showDrawing({
           pkeyValues: {blId: this.blId, flId: this.flId}
       });
       
   },
   
   /**
    * Called on onMultipleSelectionChange for the grids.
    *
    * @param {Object} row - Selected grid row.
    */
   onMultipleSelectionChange: function (row) {
   	var record = row.row.getRecord(),
       	gridRep = row.grid;
   	var blId = record.getValue("rm.bl_id"),
       	flId = record.getValue("rm.fl_id"),
       	rmId = record.getValue("rm.rm_id")

       var selectController = this.drawingController.svgControl.getController("SelectController");
       var svgId = this.drawingController.config.container + '-' + drawingName + '-svg',
           assetId = blId + ';' + flId + ';' + rmId;
       if (row.row.isSelected()) {
           if (valueExists(selectController.selectedAssets[svgId]) && selectController.selectedAssets[svgId].indexOf(assetId) >= 0) { // avoid same room selection
               // do nothing
           } else {
               selectController.toggleAssetSelection(svgId, assetId);
               setAllRowsSelected(gridRep, false);
               row.row.select();
           }
       } else if (!existsSelectedItemInSameRoom(gridRep, row.row, 'rm')) {
           selectController.toggleAssetSelection(svgId, assetId);
       }
   },
   
   setPanelFilterValues: function (detailsListPanel, selectedAssetIds, pkField) {
       if (selectedAssetIds.length == 1) {
           detailsListPanel.setFilterValue(pkField, selectedAssetIds[0]);
       } else {
           var assetIds = '\"' + selectedAssetIds.join(Ab.form.Form.MULTIPLE_VALUES_SEPARATOR) + '\"';
           detailsListPanel.setFilterValue(pkField, '{' + assetIds.replace(/, \u200C/gi, '\",\"') + '}');
       }
   }
});



function onExportDocxReport(panel, pagRepName){
	var controller = View.controllers.get("abCbRptHlBlRmCtrl");
	var floorsRestriction = [];
    floorsRestriction.push({'title': getMessage("floors"), 'value': controller.currentDwgName});

	controller.printableRestriction = controller.filterController.printableRestriction;
	
	var parameters = {
	        'consoleRestriction': controller.pagRepRestriction,
	        'printRestriction': true, 
	        'printableRestriction': controller.printableRestriction.concat(floorsRestriction)
	};
	
	View.openPaginatedReportDialog('ab-cb-assess-list-pgrpt.axvw', null, parameters);
}

/**
 * Show building details.
 * @param {Object} node - building node.
 */
function showBuilding(node) {
    var controller = View.controllers.get('abCbRptHlBlRmCtrl');
    controller.showBuildingDetails(node);
}
 
/**
 * Show floor details.
 * @param {Object} node - floor node.
 */
function showFloor(node) {
    var controller = View.controllers.get('abCbRptHlBlRmCtrl');
    controller.showFloorDetails(node);
}
