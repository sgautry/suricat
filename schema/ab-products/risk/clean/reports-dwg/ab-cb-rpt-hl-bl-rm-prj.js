/**
 * overwrite function to make add project on node restriction
 * @param {Object} parentNode
 * @param {Object} level
 */
Ab.tree.TreeControl.prototype._createRestrictionForLevel = function(parentNode, level) {
    var restriction = this.createRestrictionForLevel(parentNode, level);
	
	if (!restriction) {
		restriction = new Ab.view.Restriction();
		// add the tree restriction to parameter list if not null.
		if (this.restriction && this.restriction.clauses != undefined && this.restriction.clauses.length > 0) {
			restriction.addClauses(this.restriction, true);
		}
		
		// add the tree level's restriction to parameter list if not null.
		var levelRest = this.getRestrictionForLevel(level);
		if (levelRest && levelRest.clauses != undefined && levelRest.clauses.length > 0) {
			restriction.addClauses(levelRest, true);
		}
		
		// add the parent node's restriction to parameter list. it should always contain something
		if (!parentNode.isRoot()){
			if(this.type=='hierTree' || this.type=='selectValueHierTree'){
				restriction.addClauses(parentNode.restriction, true);
		    } else {
		    	if (this._panelsData[level].useParentRestriction==true) {
					restriction.addClauses(parentNode.restriction, true);
				}
			}
		}
		// bldg level, we must add project
		if(level == 2 && parentNode.parent.data['city.city_id'] != undefined){
			restriction.addClause('bl.city_id', parentNode.parent.data['city.city_id']);
		}
		//floor level, we must add project and site
		if(level == 3 && parentNode.parent.data['site.site_id'] != undefined && parentNode.parent.parent.data['city.city_id'] != undefined){
			restriction.addClause('rm.site_id', parentNode.parent.data['site.site_id']);
			restriction.addClause('rm.city_id', parentNode.parent.parent.data['city.city_id']);
		}
	}
	
	return restriction;
}

View.createController('abCbRptHlBlRmPrjCtrl',{
	blId: null,
	flId: null,
	mapControl: null,
    drawingController: null,
    drawingSvgId: null,
	filterController: null,

	pagRepRestriction: null,
	printableRestriction: null,
	defaultPagRepRestriction: "(EXISTS(SELECT 1 FROM rm WHERE rm.bl_id = activity_log.bl_id AND rm.fl_id = activity_log.fl_id AND rm.rm_id = activity_log.rm_id AND rm.dwgname IS NOT NULL)"
			+ " OR activity_log.dwgname IS NOT NULL)"
			+ " AND activity_log.project_id IS NOT NULL"
			+ " AND EXISTS(SELECT 1 FROM project WHERE project.project_id = activity_log.project_id AND project.project_type='ASSESSMENT - HAZMAT' AND project.is_template = 0)",
	
	paginatedReportName: 'ab-cb-rpt-hl-bl-rm-pgrp.axvw',

	// true if floor selected, false if building selected
	showDrawing: false,

	// drawing name for highlighting the assets
	currentDwgName: null,
	
    afterViewLoad: function(){

		this.pagRepRestriction = this.defaultPagRepRestriction;

    	this.filterController = View.controllers.get("abCbRptCommonFilterCtrl");
    	this.filterController.panelsCtrl = this;
    	this.filterController.visibleFields = "site";
    	this.filterController.paginatedReportName = "ab-cb-rpt-hl-bl-rm-prj-pgrp.axvw";
		
		// set message parameter for esHighRmHazWastProjectTree panel
		this.abCbRptHlBlRmPrj_projectTree.addParameter('noDrawing',getMessage('noDrawing'));
				
		// add onMultipel selection change event listener
		this.abCbRptHlBlRmPrj_gridRep.addEventListener('onMultipleSelectionChange', this.onMultipleSelectionChange.createDelegate(this));
    	
		// init map control
        this.initMapControl();
		
    },
 
    afterInitialDataFetch: function(){
    	this.drawingPanel.show(false); 
        this.abCbRptHlBlRmPrj_htmlMap.show(true);
    },
 
    initMapControl:function(){	
    	var configObject = new Ab.view.ConfigObject();
        configObject.mapImplementation = 'Esri';
        this.mapControl = new Ab.leaflet.Map('abCbRptHlBlRmPrj_htmlMap', 'abCbRptHlBlRmPrj_objMap', configObject);
 
        var basemapLayerMenu = this.abCbRptHlBlRmPrj_htmlMap.actions.get('basemapLayerMenu');
        basemapLayerMenu.clear();
        var basemapLayers = this.mapControl.getBasemapLayerList();
        for ( var i = 0; i < basemapLayers.length; i++ ) {
            basemapLayerMenu.addAction(i, basemapLayers[i], this.switchBasemapLayer.createDelegate(this));
        }
        this.mapControl.createMarkers(
                'abCbRptHlBlRmPrj_dsBuilding',
                ['bl.bl_id'],
                ['bl.lon', 'bl.lat'],
                'bl.name',
                ['bl.site_id', 'bl.bl_id', 'bl.name', 'bl.address'],
                {
                    markerActionTitle: getMessage('labelShowDetails'),
                    markerActionCallback: this.onBuildingDetails.createDelegate(this)
                }
        );
    },
    
    switchBasemapLayer: function(item) {
    	//switch the mapControl layer based on the passed in layer name.
    	this.mapControl.switchBasemapLayer(item.text);
    },   
    
    initDrawingControl: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            bordersHighlightSelector: true,
            borderSize: 14,
            diagonalSelection:  {borderWidth: 15},
            assetTypes: ['rm', 'activity_log'],
            highlightParameters: [{
                'view_file': "ab-cb-rpt-hl-ds.axvw",
                'hs_ds': "highlightRoomDs",
                'label_ds': 'labelDetailsDs',
                'label_ht': '0.60',
                 showSelector: true
            }],
            assetTooltip: [
                {
                    assetType: 'rm',
                    datasource: 'labelDetailsDs',
                    fields: 'rm.rm_id;rm.rm_cat;rm.rm_type;rm.area'
                }
            ],
            layersPopup: {
                layers: "rm-assets;rm-labels;activity_log-assets;cb_samples-assets;gros-assets;gros-labels;background",
                defaultLayers: "rm-assets;rm-labels;activity_log-assets;background;activity_log-labels;cb_samples-assets;cb_samples-labels;gros-assets"
            },
            reports: [{
                text: "DOC", listener: function () {
                    View.controllers.get('abCbRptHlBlRmPrjCtrl').abCbRptHlBlRmPrj_drawingPanel_onPaginatedReport();
                }
            }],
            showPanelTitle: true,
            listeners: {
                afterDrawingLoaded: function (drawingController, svgId) {
                    var controller = View.controllers.get('abCbRptHlBlRmPrjCtrl');
                    controller.drawingSvgId = svgId;
                    drawingController.svgControl.getController("SelectController").setMultipleSelection(true);
                },
                onClickAssetAfter: function (drawingController, svgId, selectedAssetId, assetType, selectedAssets, selected){
                	var objDetailsGrid = View.panels.get("abCbRptHlBlRmPrj_gridRep"); 
                	var rmIds = [];
                    _.each(selectedAssets, function (selectedAsset) {
                        var selectedRooms = selectedAsset['rm'];
                        if (selectedRooms) {
                            _.each(selectedRooms, function (selectedRoom) {
                                var assetId = selectedRoom.split(';')[2]; //blId;flId;rmId
                                rmIds.push(assetId);
                            });
                        }
                    });
                    if (rmIds.length > 0) {
                    	View.controllers.get('abCbRptHlBlRmPrjCtrl').setPanelFilterValues(objDetailsGrid, _.uniq(rmIds), 'rm.rm_id');
                        objDetailsGrid.refresh(objDetailsGrid.restriction);
                        setAllRowsSelected(objDetailsGrid, true);
                    } else {
                        objDetailsGrid.clearAllFilterValues();
                        objDetailsGrid.refresh(objDetailsGrid.restriction);
                    }
                }
            }
        });
    },
    
    /**
     * Show building details for selected building.
     * @param {Object} node - building node.
     */
    showBuildingDetails: function (node) {
        //get bl id
        var blId = node.restriction.clauses[0].value;
        var projectId = View.panels.get('abCbRptHlBlRmPrj_projectTree').lastNodeClicked.getAncestor().getAncestor().data['city.city_id'];
        this.blId = blId;
        if (this.filterController.restriction) {
            this.abCbRptHlBlRmPrj_gridRep.addParameter("consoleRestriction", this.filterController.restriction);
        }
        //refresh rooms panel
        var nodeRestriction = new Ab.view.Restriction();
        nodeRestriction.addClause('activity_log.bl_id', blId, '=');
        nodeRestriction.addClause('activity_log.project_id', projectId, '=');
        this.abCbRptHlBlRmPrj_gridRep.refresh(nodeRestriction);
        setAllRowsSelectable(this.abCbRptHlBlRmPrj_gridRep, false);
        
        // load ESRI mapControl or building details
        var restriction = new Ab.view.Restriction();
        restriction.addClause('bl.bl_id', this.blId, '=');
        if (this.mapControl) {
            this.showPanels(false, true, true);
            this.showBuilding(restriction);
        }
    },
    
    /**
     * Show building on map.
     * @param {Ab.view.Restriction} restriction - building restriction.
     */
    showBuilding: function (restriction) {
        this.mapControl.clearMarkers();
        this.mapControl.showMarkers('abCbRptHlBlRmPrj_dsBuilding', restriction);
    },
    /**
     * Show building details map marker action.
     * @param {string} buildingId - Building id.
    */ 
    onBuildingDetails: function (buildingId) {
        var restriction = new Ab.view.Restriction();
        restriction.addClause('bl.bl_id', buildingId, '=');
        this.abCbRptHlBlRmPrj_blDetailsPanel.refresh(restriction);
        this.abCbRptHlBlRmPrj_blDetailsPanel.showInWindow({
            width: 1200,
            closeButton: true
        });
    },
    /**
     * Show details for selected floor.
     * @param {Object} node - floor node.
     */
    showFloorDetails: function (node) {
        this.showPanels(true, true, false);
        //set restrictions and refresh for abCbRptHlHaz_gridRepRooms panel
        var currentNode = View.panels.get('abCbRptHlBlRmPrj_projectTree').lastNodeClicked;
        var projectId = currentNode.data['rm.city_id'];
        var node_rest = new Ab.view.Restriction();
    	node_rest.addClauses(node.restriction);
        node_rest.removeClause('rm.city_id');
        node_rest.removeClause('rm.rm_id');
    	node_rest.addClause('activity_log.project_id', projectId, '=', true); 
        
        var dwgRestr = this.filterController.restriction ? this.filterController.restriction : "1=1";
        this.abCbRptHlBlRmPrj_gridRep.addParameter("consoleRestriction", dwgRestr);
        this.abCbRptHlBlRmPrj_gridRep.refresh(node_rest);

        dwgRestr += " AND " + " (activity_log.bl_id = '" + currentNode.data['rm.bl_id'] + "' AND activity_log.fl_id = '" + currentNode.data['rm.fl_id'] + "')";
        dwgRestr += " AND " + " (activity_log.project_id = '" + projectId + "')";

    	this.pagRepRestriction = this.defaultPagRepRestriction + " AND " + dwgRestr;
        setAllRowsSelectable(this.abCbRptHlBlRmPrj_gridRep, true);
        //set restrictions and refresh for abCbRptHlHaz_drawingPanel
        var currentNode = View.panels.get('abCbRptHlBlRmPrj_projectTree').lastNodeClicked;
        this.blId = currentNode.data['rm.bl_id'];
        this.flId = currentNode.data['rm.fl_id'];
        this.currentDwgName = currentNode.data['rm.raw_dwgname'];
        
        this.displayDrawing(dwgRestr);
    },
    /**
     * Displays the drawing, and select asset based on selected radio buttons.
     */
    displayDrawing: function (dwgRestr) {
        if (!valueExists(this.drawingController)) {
            this.initDrawingControl();
        }
        if (!this.flId) {
            View.showMessage(getMessage("selectAFloor"));
            return;
        }
        // unselect the rows in the grid
        this.abCbRptHlBlRmPrj_gridRep.unselectAll();
        this.drawingController.addHighlightDataSourceParameter({
            'consoleRestriction': dwgRestr
        });
        this.drawingController.showDrawing({
            pkeyValues: {blId: this.blId, flId: this.flId}
        });
        
    },
    /**
     * Called on onMultipleSelectionChange for the grids.
     *
     * @param {Object} row - Selected grid row.
     */
    onMultipleSelectionChange: function (row) {
    	var record = row.row.getRecord(),
        	gridRep = row.grid;
    	var blId = record.getValue("rm.bl_id"),
        	flId = record.getValue("rm.fl_id"),
        	rmId = record.getValue("rm.rm_id")

        var selectController = this.drawingController.svgControl.getController("SelectController");
        var svgId = this.drawingController.config.container + '-' + drawingName + '-svg',
            assetId = blId + ';' + flId + ';' + rmId;
        if (row.row.isSelected()) {
            if (valueExists(selectController.selectedAssets[svgId]) && selectController.selectedAssets[svgId].indexOf(assetId) >= 0) { // avoid same room selection
                // do nothing
            } else {
                selectController.toggleAssetSelection(svgId, assetId);
                row.row.select();
            }
        } else if (!existsSelectedItemInSameRoom(gridRep, row.row, 'rm')) {
            selectController.toggleAssetSelection(svgId, assetId);
        }
    },
        

    abCbRptHlBlRmPrj_drawingPanel_onPaginatedReport: function() {

        var floorsRestriction = [];        
        floorsRestriction.push({'title': getMessage("floors"), 'value': this.currentDwgName});
        this.printableRestriction = this.filterController.printableRestriction;
    
    	var parameters = {
			'consoleRestriction': this.filterController.getConsoleRestriction(),
			'userAssignedRestriction': this.getFloorsRestriction(),
			'printRestriction': true, 
			'printableRestriction': this.printableRestriction.concat(floorsRestriction)
    	};

    	View.openPaginatedReportDialog(this.paginatedReportName, null, parameters);    	
    },

    getFloorsRestriction: function() {
        var currentNode = this.abCbRptHlBlRmPrj_projectTree.lastNodeClicked;       
        var dwgRestr = " (activity_log.bl_id = '" + currentNode.data['rm.bl_id'] + "' AND activity_log.fl_id = '" + currentNode.data['rm.fl_id'] + "')";
        return dwgRestr;
    },
    
	/**
	 * Shows the tree according to the user restrictions
	 */
	refreshOnFilter: function(restriction){
		restriction =  restriction.replace(/activity_log\./g, "a.");
        this.abCbRptHlBlRmPrj_projectTree.addParameter('consoleRestriction', restriction);
		this.abCbRptHlBlRmPrj_projectTree.addParameter('consoleRestrictionForCount', restriction.replace(/a\./g, "al."));
        this.abCbRptHlBlRmPrj_projectTree.refresh();
        this.showPanels(false, false, false);
	},

	/**
	 * show/ hide panels
	 * @param {boolean} showDrawing
	 * @param {boolean} showRep
	 * @param {boolean} showMap
	 */
    showPanels: function(showDrawing, showRep, showMap){
    	this.drawingPanel.show(showDrawing);
        this.abCbRptHlBlRmPrj_gridRep.show(showRep);
        this.abCbRptHlBlRmPrj_htmlMap.show(showMap);
    },
    
    setPanelFilterValues: function (detailsListPanel, selectedAssetIds, pkField) {
        if (selectedAssetIds.length == 1) {
            detailsListPanel.setFilterValue(pkField, selectedAssetIds[0]);
        } else {
            var assetIds = '\"' + selectedAssetIds.join(Ab.form.Form.MULTIPLE_VALUES_SEPARATOR) + '\"';
            detailsListPanel.setFilterValue(pkField, '{' + assetIds.replace(/, \u200C/gi, '\",\"') + '}');
        }
    }
});

/**
 * Show building details.
 * @param {Object} node - building node.
 */
function showBuilding(node) {
    var controller = View.controllers.get('abCbRptHlBlRmPrjCtrl');
    controller.showBuildingDetails(node);
}
 
/**
 * Show floor details.
 * @param {Object} node - floor node.
 */
function showFloor(node) {
    var controller = View.controllers.get('abCbRptHlBlRmPrjCtrl');
    controller.showFloorDetails(node);
}

function onExportDocxReport(panel, pagRepName){
var controller = View.controllers.get("abCbRptHlBlRmPrjCtrl");
var floorsRestriction = [];
floorsRestriction.push({'title': getMessage("floors"), 'value': controller.currentDwgName});

controller.printableRestriction = controller.filterController.printableRestriction;

var parameters = {
        'consoleRestriction': controller.pagRepRestriction,
        'printRestriction': true, 
        'printableRestriction': controller.printableRestriction.concat(floorsRestriction)
};

View.openPaginatedReportDialog('ab-cb-assess-list-pgrpt.axvw', null, parameters);
}
