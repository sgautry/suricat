View.createController('abCbRptHazBlMapDrilldown', {
	blId: null,
	drawingController: null,
	dwgnames: [],
	floors: [],
	pagRepRestriction: null,
	printableRestriction: null,
	defaultPagRepRestriction: "(EXISTS(SELECT 1 FROM rm WHERE rm.bl_id = activity_log.bl_id AND rm.fl_id = activity_log.fl_id AND rm.rm_id = activity_log.rm_id AND rm.dwgname IS NOT NULL)"
			+ " OR activity_log.dwgname IS NOT NULL)"
			+ " AND activity_log.project_id IS NOT NULL"
			+ " AND EXISTS(SELECT 1 FROM project WHERE project.project_id = activity_log.project_id AND project.project_type='ASSESSMENT - HAZMAT' AND project.is_template = 0)",
	
	openerController: View.getOpenerView().controllers.get('abCbRptHazBlMapCtrl'),
	
	afterViewLoad: function() {
		if (View.parameters && valueExistsNotEmpty(View.parameters.bl_id)) {
			this.blId = View.parameters.bl_id;
		}
		
		this.initDrawingControl();
        this.abCbRptHazBlMapDrilldown_floorsGrid.multipleSelectionEnabled = false;
        this.abCbRptHazBlMapDrilldown_floorsGrid.addEventListener('onMultipleSelectionChange', this.onFloorMultipleSelectionChange.createDelegate(this));
        this.abCbRptHazBlMapDrilldown_itemsDetails.addEventListener('onMultipleSelectionChange', this.onActionMultipleSelectionChange.createDelegate(this));
    	
	},
	
	afterInitialDataFetch: function() {
		var restriction = this.openerController.filterController.restriction ? this.openerController.filterController.restriction : "1=1";
		this.printableRestriction = null;
		
		var buildingPrintableRestriction=[];
		this.blId = this.openerController.blId;
		if (this.blId){
			restriction += " AND activity_log.bl_id = '" + this.blId + "'";
			buildingPrintableRestriction.push({'title': getMessage("buildings"), 'value': this.blId});
			this.printableRestriction = this.openerController.filterController.printableRestriction.concat(buildingPrintableRestriction);
		}else{
			this.printableRestriction = this.openerController.filterController.printableRestriction;
		}
		
		
		this.pagRepRestriction = this.defaultPagRepRestriction + " AND " + restriction;
		
		this.abCbRptHazBlMapDrilldown_floorsGrid.refresh(restriction);
		this.abCbRptHazBlMapDrilldown_itemsDetails.refresh(restriction);
		
	},
	
	/**
     * Init drawing control.
     */
    initDrawingControl: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['rm'],
            allowMultipleDrawings: true,
            orderByColumn: false,
            bordersHighlightSelector: true,
            diagonalSelection:  {borderWidth: 15},
            highlightParameters: [{
                'view_file': "ab-cb-rpt-hl-ds.axvw",
                'hs_ds': "highlightProjDs",
                'label_ds': 'labelDetailsDs',
                'label_ht': '0.60',
                showSelector: true
            }],
            topLayers: 'activity_log',
            layersPopup: {
                layers: "rm-assets;rm-labels;activity_log-assets;activity_log-labels;gros-labels;background",
                defaultLayers: "rm-assets;rm-labels;activity_log-assets;activity_log-labels;gros-assets;background"
            },
            assetTooltip: [
                {
                    assetType: 'rm',
                    datasource: 'labelDetailsDs',
                    fields: 'rm.rm_id;rm.rm_type;rm.area'
                }
            ],
            listeners: {
                afterDrawingLoaded: function (drawingController, svgId) {
                    var controller = View.controllers.get('abCbRptHazBlMapDrilldown');
                    controller.drawingSvgId = svgId;
                    drawingController.svgControl.getController("SelectController").setMultipleSelection(true);
                },
                onClickAssetAfter: function (drawingController, svgId, selectedAssetId, assetType, selectedAssets, selected){
                	var objDetailsGrid = View.panels.get("abCbRptHazBlMapDrilldown_itemsDetails"); 
                	var pkey = selectedAssetId.split(';');
                    objDetailsGrid.gridRows.each(function(gridRow){
                		if (gridRow.getFieldValue('rm.bl_id') == pkey[0] 
                			&& gridRow.getFieldValue('rm.fl_id') == pkey[1] 
                			&& gridRow.getFieldValue('rm.rm_id') == pkey[2]) {
                			if (selected) {
                				gridRow.select();
                			} else {
                				gridRow.unselect();
                			}
                			var suffix = selected ? " selected" : "";
                			var cn = gridRow.dom.className;
                			var j = 0;
                			if ((j = cn.indexOf(" selected")) > 0)
                				cn = cn.substr(0, j);
                			gridRow.dom.className=cn + suffix;
                		}
                	});
                }
            }
        });
    },
    /**
     * Load drawing.
     */
    loadDrawing: function (blId, flId, drawingName) {
        this.drawingController.showDrawing({
            pkeyValues: {blId: blId, flId: flId},
            drawingName: drawingName
        });
        this.floors.push(flId);
    },
    /**
     * Unload drawing.
     */
    unloadDrawing: function (blId, flId, drawingName) {
        this.drawingController.unloadDrawing(blId, flId, drawingName);
        this.floors.remove(flId);
        
    },
    /**
     * On floor grid row selection load drawing on row select and unload drawing on row unselect.
     */
    onFloorMultipleSelectionChange: function (row) {
        var record = row.row.getRecord();
        // get selected data from tree
        var blId = record.getValue("activity_log.bl_id"),
            flId = record.getValue("activity_log.fl_id"),
            drawingName = record.getValue("rm.dwgname");
        if (row.row.isSelected()) {
            // load relevant svg
            this.loadDrawing(blId, flId, drawingName);
            this.dwgnames.push(drawingName);
            var restriction = this.openerController.filterController.restriction ? this.openerController.filterController.restriction : "1=1";
    		this.pagRepRestriction = this.defaultPagRepRestriction + " AND " + restriction;
    		var dwgRestr = " AND (CASE WHEN rm.dwgname IS NULL THEN activity_log.dwgname ELSE rm.dwgname END) IN ('" + this.dwgnames.join("','") + "')";
    		restriction += dwgRestr;
    		this.pagRepRestriction += dwgRestr.replace(/rm\.dwgname/g,
    									"(SELECT rm.dwgname FROM rm"
    									+ " WHERE rm.bl_id = activity_log.bl_id AND rm.fl_id = activity_log.fl_id AND rm.rm_id = activity_log.rm_id"
    									+ ")");
        } else {
            this.unloadDrawing(blId, flId, drawingName);
            this.dwgnames.remove(drawingName);
            var restriction = this.openerController.filterController.restriction ? this.openerController.filterController.restriction : "1=1";
            if (blId){
    			restriction += " AND activity_log.bl_id = '" + this.blId + "'";
            }
    		this.pagRepRestriction = this.defaultPagRepRestriction + " AND " + restriction;
        }
        this.refreshActions();
    },
    /**
     * On action row selection, highlight action id on the drawing.
     */
    onActionMultipleSelectionChange: function (row) {
    	var record = row.row.getRecord(),
       	gridRep = row.grid;
    	var blId = record.getValue("rm.bl_id"),
       	flId = record.getValue("rm.fl_id"),
       	rmId = record.getValue("rm.rm_id")

	       var selectController = this.drawingController.svgControl.getController("SelectController");
	       var svgId = this.drawingController.config.container + '-' + record.getValue('rm.dwgname') + '-svg',
	           assetId = blId + ';' + flId + ';' + rmId;
	       var selected = row.row.isSelected();
	       if (selected) {
	           if (valueExists(selectController.selectedAssets[svgId]) && selectController.selectedAssets[svgId].indexOf(assetId) >= 0) { // avoid same room selection
	               // do nothing
	           } else {
	               selectController.toggleAssetSelection(svgId, assetId);
	               //setAllRowsSelected(gridRep, false);
	               row.row.select();	     
	               if (!existsSelectedItemInSameRoom(gridRep, row.row, 'rm')) {
	    	           selectController.toggleAssetSelection(svgId, assetId);
	               }
	           }
	       } else if (!existsSelectedItemInSameRoom(gridRep, row.row, 'rm')) {
	           selectController.toggleAssetSelection(svgId, assetId);
	       }
//	       var suffix = selected ? " selected" : "";
//   		   var cn = row.row.dom.className;
//   		   var j = 0;
//   		   if ((j = cn.indexOf(" selected")) > 0)
//   				cn = cn.substr(0, j);
//   		   row.row.dom.className=cn + suffix;
   },
   
//   setPanelFilterValues: function (detailsListPanel, selectedAssetIds, pkField) {
//       if (selectedAssetIds.length == 1) {
//           detailsListPanel.setFilterValue(pkField, selectedAssetIds[0]);
//       } else {
//           var assetIds = '\"' + selectedAssetIds.join(Ab.form.Form.MULTIPLE_VALUES_SEPARATOR) + '\"';
//           detailsListPanel.setFilterValue(pkField, '{' + assetIds.replace(/, \u200C/gi, '\",\"') + '}');
//       }
//   
//    },
    
    /**
     * Refresh actions grid.
     */
    refreshActions: function () {
        var selectedRows = this.abCbRptHazBlMapDrilldown_floorsGrid.getSelectedGridRows(),
            drawingNames = [];
        for ( var i = 0; i < selectedRows.length; i++ ) {
            var drawingName = selectedRows[i].getRecord().getValue('rm.dwgname');
            drawingNames.push(drawingName);
        }
 
        var restriction = new Ab.view.Restriction();
        restriction.addClause('rm.bl_id', this.blId);
        if (drawingNames.length > 0) {
            restriction.addClause('activity_log.rm_activity_dwgname', drawingNames, "IN");
        }
        if (this.projectId) {
            restriction.addClause('activity_log.project_id', this.projectId);
        }
        this.abCbRptHazBlMapDrilldown_itemsDetails.refresh(restriction);
    },
 
	
	abCbRptHazBlMapDrilldown_itemsDetails_afterRefresh: function(){
		this.abCbRptHazBlMapDrilldown_itemsDetails.enableSelectAll(false);
	}
	
});


function onExportDocxReport(panel, pagRepName){
	var controller = View.controllers.get("abCbRptHazBlMapDrilldown");
	var floorsRestriction = [];
	
	if(controller.floors.length > 0){
		floorsRestriction.push({'title': getMessage("floors"), 'value': controller.floors.join(", ")});
	}
	
	var parameters = {
	        'consoleRestriction': controller.pagRepRestriction,
	        'printRestriction': true, 
	        'printableRestriction': controller.printableRestriction.concat(floorsRestriction)
	};
	
	View.openPaginatedReportDialog('ab-cb-assess-list-pgrpt.axvw', null, parameters);
}

