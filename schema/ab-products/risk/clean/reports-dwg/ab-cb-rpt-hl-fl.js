View.createController('abCbRptHlFlCtrl', {
    drawingController: null,
    filterController: null,
    pagRepRestriction: null,
    printableRestriction: null,
    defaultPagRepRestriction: "(EXISTS(SELECT 1 FROM rm WHERE rm.bl_id = activity_log.bl_id AND rm.fl_id = activity_log.fl_id AND rm.rm_id = activity_log.rm_id AND rm.dwgname IS NOT NULL)"
    + " OR activity_log.dwgname IS NOT NULL)"
    + " AND activity_log.project_id IS NOT NULL"
    + " AND EXISTS(SELECT 1 FROM project WHERE project.project_id = activity_log.project_id AND project.project_type='ASSESSMENT - HAZMAT' AND project.is_template = 0)",
    selectedFloors: [],
    paginatedReportName: 'ab-cb-rpt-hl-bl-rm-pgrp.axvw',
    afterViewLoad: function () {
        this.initDrawingControl();
        this.initFilterControl();
        // set message parameter for abCbRptHlFl_gridFloor panel
        this.abCbRptHlFl_gridFloor.addParameter('noDrawing', getMessage('noDrawing'));
        this.abCbRptHlFl_gridFloor.addEventListener('onMultipleSelectionChange', this.onFloorMultipleSelectionChange.createDelegate(this));
        this.abCbRptHlFl_gridRep.addEventListener('onMultipleSelectionChange', this.onItemsMultipleSelectionChange.createDelegate(this));
        this.abCbRptHlFl_gridFloor.multipleSelectionEnabled = false;
        this.abCbRptHlFl_gridRep.multipleSelectionEnabled = false;
    },
    afterInitialDataFetch: function () {
        this.showPanels(false, false);
    },
    initFilterControl: function () {
        this.pagRepRestriction = this.defaultPagRepRestriction;
        this.filterController = View.controllers.get("abCbRptCommonFilterCtrl");
        this.filterController.panelsCtrl = this;
        this.filterController.visibleFields = "proj";
    },
    /**
     * Init drawing control.
     */
    initDrawingControl: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['rm'],
            allowMultipleDrawings: true,
            orderByColumn: false,
            bordersHighlightSelector: true,
            diagonalSelection: {borderWidth: 15},
            maximize: {
                showAsDialog: false,
                expand: function () {
                    var layout = View.getLayoutManager('mainLayout');
                    layout.expandRegion('north');
                    layout.expandRegion('west');
                    layout.recalculateLayout();
                },
                collapse: function () {
                    var layout = View.getLayoutManager('mainLayout');
                    layout.collapseRegion('north');
                    layout.collapseRegion('west');
                    layout.recalculateLayout();
                }
            },
            reset: true,
            highlightParameters: [{
                'view_file': "ab-cb-rpt-hl-ds.axvw",
                'hs_ds': "highlightRoomDs",
                'label_ds': 'labelDetailsDs'
            }],
            layersPopup: {
                layers: "rm-assets;rm-labels;gros-labels;background",
                defaultLayers: "rm-assets;rm-labels;gros-assets;background"
            },
            assetTooltip: [{
                assetType: 'rm',
                datasource: 'labelDetailsDs',
                fields: 'rm.rm_id;rm.rm_type;rm.area'
            }],
            reports: [{
                text: "DOC", listener: function () {
                    View.controllers.get('abCbRptHlFlCtrl').onSelectedDrawingsPaginatedReport(this);
                }
            }],
            listeners: {
                afterDrawingLoaded: function (drawingController) {
                    drawingController.svgControl.getController("SelectController").setMultipleSelection(true);
                },
                // called after the default onClickAsset event
                onClickAssetAfter: function (drawingController, svgId, selectedAssetId, assetType, selectedAssets, selected) {
                    View.controllers.get('abCbRptHlFlCtrl').onClickDrawing(drawingController, svgId, selectedAssetId, assetType, selectedAssets, selected);
                },
                reset: function (drawingController) {
                    View.controllers.get('abCbRptHlFlCtrl').resetDrawing(drawingController);
                }
            }
        })
    },
    /**
     * Load drawing.
     */
    loadDrawing: function (blId, flId, drawingName) {
        this.drawingController.showDrawing({
            pkeyValues: {blId: blId, flId: flId},
            drawingName: drawingName
        });
    },
    /**
     * Unload drawing.
     */
    unloadDrawing: function (blId, flId, drawingName) {
        this.drawingController.unloadDrawing(blId, flId, drawingName);
    },
    resetDrawing: function (drawingController) {
        var selectController = drawingController.svgControl.getController("SelectController");
        _.each(_.keys(drawingController.selectedAssets), function (svgId) {
            var selectedSvgAssets = drawingController.selectedAssets[svgId];
            var rmIds = selectedSvgAssets['rm'];
            _.each(rmIds, function (assetId) {
                selectController.toggleAssetSelection(svgId, assetId);
            });
            drawingController.resetSelectedDrawingAssets(svgId);
        });
        var objDetailsGrid = View.panels.get("abCbRptHlFl_gridRep");
        objDetailsGrid.clearAllFilterValues();
        objDetailsGrid.refresh(objDetailsGrid.restriction);
    },
    /**
     * Show rooms and drawing for selected floor.
     * @param {Object} row Selected floor
     */
    onFloorMultipleSelectionChange: function (row) {
        var record = row.row.getRecord();
        var blId = record.getValue("rm.bl_id"),
            flId = record.getValue("rm.fl_id"),
            drawingName = record.getValue("rm.raw_dwgname");
        this.selectedFloors = View.panels.get('abCbRptHlFl_gridFloor').getSelectedRecords();
        if (this.selectedFloors.length == 0) {
            if (valueExistsNotEmpty(drawingName) && !row.row.isSelected()) {
                this.unloadDrawing(blId, flId, drawingName);
            }
            this.showPanels(false, false);
            return;
        }
        this.showPanels(true, true);
        var dwgRestr = this.filterController.restriction ? this.filterController.restriction : "1=1";
        this.abCbRptHlFl_gridRep.addParameter("consoleRestriction", dwgRestr);
        var floorsRestriction = this.getFloorsRestriction();
        this.abCbRptHlFl_gridRep.clearAllFilterValues();
        this.abCbRptHlFl_gridRep.refresh(floorsRestriction);
        dwgRestr += " AND " + floorsRestriction;
        this.pagRepRestriction = this.defaultPagRepRestriction + " AND " + dwgRestr;
        this.drawingController.addHighlightDataSourceParameter({
            'consoleRestriction': dwgRestr
        });
        if (valueExistsNotEmpty(drawingName)) {
            if (row.row.isSelected()) {
                this.loadDrawing(blId, flId, drawingName);
            } else {
                this.unloadDrawing(blId, flId, drawingName);
            }
        } else {
            View.showMessage(String.format(getMessage("noDrawingFoundBlFl"), blId, flId));
        }
    },
    /**
     * On multiple selection change for item details grid panel.
     * @param row current row
     */
    onItemsMultipleSelectionChange: function (row) {
        var record = row.row.getRecord(),
            gridRep = row.grid;
        var blId = record.getValue("rm.bl_id"),
            flId = record.getValue("rm.fl_id"),
            rmId = record.getValue("rm.rm_id"),
            drawingName = record.getValue("rm.dwgname");
        if (!valueExistsNotEmpty(drawingName)) {
            row.row.unselect();
            View.showMessage(getMessage('noDrawing'));
            return;
        }
        var selectController = this.drawingController.svgControl.getController("SelectController");
        var svgId = this.drawingController.config.container + '-' + drawingName + '-svg',
            assetId = blId + ';' + flId + ';' + rmId;
        if (row.row.isSelected()) {
            if (valueExists(selectController.selectedAssets[svgId]) && selectController.selectedAssets[svgId].indexOf(assetId) >= 0) { // avoid same room selection
                // do nothing
            } else {
                selectController.toggleAssetSelection(svgId, assetId);
            }
        } else if (!existsSelectedItemInSameRoom(gridRep, row.row, 'rm')) {
            selectController.toggleAssetSelection(svgId, assetId);
        }
    },
    /**
     *
     *Click room event on drawing.
     */
    onClickDrawing: function (drawingController, svgId, selectedAssetId, assetType, selectedAssets, selected) {
        var blIds = [], flIds = [], rmIds = [];
        _.each(drawingController.drawings, function (drawing) {
            var pkeyValues = drawing.pkeyValues;
            blIds.push(pkeyValues.blId);
            flIds.push(pkeyValues.flId);
        });
        _.each(selectedAssets, function (selectedAsset) {
            var selectedRooms = selectedAsset['rm'];
            if (selectedRooms) {
                _.each(selectedRooms, function (selectedRoom) {
                    var assetId = selectedRoom.split(';')[2]; //blId;flId;rmId
                    rmIds.push(assetId);
                });
            }
        });
        var objDetailsGrid = View.panels.get("abCbRptHlFl_gridRep");
        if (rmIds.length > 0) {
            this.setPanelFilterValues(objDetailsGrid, _.uniq(blIds), 'rm.bl_id');
            this.setPanelFilterValues(objDetailsGrid, _.uniq(flIds), 'rm.fl_id');
            this.setPanelFilterValues(objDetailsGrid, _.uniq(rmIds), 'rm.rm_id');
            objDetailsGrid.refresh(objDetailsGrid.restriction);
            setAllRowsSelected(objDetailsGrid, true);
        } else {
            objDetailsGrid.clearAllFilterValues();
            objDetailsGrid.refresh(objDetailsGrid.restriction);
        }
    },
    setPanelFilterValues: function (detailsListPanel, selectedAssetIds, pkField) {
        if (selectedAssetIds.length == 1) {
            detailsListPanel.setFilterValue(pkField, selectedAssetIds[0]);
        } else {
            var assetIds = '\"' + selectedAssetIds.join(Ab.form.Form.MULTIPLE_VALUES_SEPARATOR) + '\"';
            detailsListPanel.setFilterValue(pkField, '{' + assetIds.replace(/, \u200C/gi, '\",\"') + '}');
        }
    },
    getFloorsRestriction: function () {
        var floorsRestriction = "";
        for ( var i = 0; i < this.selectedFloors.length; i++ ) {
            var blId = this.selectedFloors[i].getValue('rm.bl_id');
            var flId = this.selectedFloors[i].getValue('rm.fl_id');
            floorsRestriction += (i == 0) ? "(" : " OR ";
            floorsRestriction += " (activity_log.bl_id = '" + blId + "' AND activity_log.fl_id = '" + flId + "')";
            if (i == (this.selectedFloors.length - 1))
                floorsRestriction += ")";
        }
        return floorsRestriction;
    },
    /**
     * Shows the tree according to the user restrictions
     */
    refreshOnFilter: function (restriction, instrLabels) {
        this.abCbRptHlFl_gridFloor.addParameter('consoleRestriction', restriction);
        this.abCbRptHlFl_gridFloor.refresh();
        this.showPanels(false, false);
    },
    /**
     * show/ hide panels
     * @param {boolean} showDrawing
     * @param {boolean} showRep
     */
    showPanels: function (showDrawing, showRep) {
        this.drawingPanel.show(showDrawing);
        if (!showDrawing) {
            this.drawingController.clearDrawingPanel();
        }
        this.abCbRptHlFl_gridRep.show(showRep);
    },
    onSelectedDrawingsPaginatedReport: function (drawingController) {
        var floorsRestriction = [];
        var floors = [];
        if (this.selectedFloors.length > 0) {
            for ( var i = 0; i < this.selectedFloors.length; i++ ) {
                floors.push(this.selectedFloors[i].getValue('rm.dwgname'));
            }
            floorsRestriction.push({'title': getMessage("floors"), 'value': floors.join(", ")});
        }
        else return;
        this.printableRestriction = this.filterController.printableRestriction;
        var parameters = {
            'consoleRestriction': this.filterController.getConsoleRestriction(),
            'userAssignedRestriction': this.getFloorsRestriction(),
            'printRestriction': true,
            'printableRestriction': this.printableRestriction.concat(floorsRestriction)
        };
        View.openPaginatedReportDialog(this.paginatedReportName, null, parameters);
    },
    onPaginatedReport: function () {
        var floorsRestriction = [];
        var floors = [];
        if (this.selectedFloors.length > 0) {
            for ( var i = 0; i < this.selectedFloors.length; i++ ) {
                floors.push(this.selectedFloors[i].getValue('rm.dwgname'));
            }
            floorsRestriction.push({'title': getMessage("floors"), 'value': floors.join(", ")});
        }
        this.printableRestriction = this.filterController.printableRestriction;
        var parameters = {
            'consoleRestriction': this.pagRepRestriction,
            'printRestriction': true,
            'printableRestriction': this.printableRestriction.concat(floorsRestriction)
        };
        View.openPaginatedReportDialog('ab-cb-assess-list-pgrpt.axvw', null, parameters);
    }
});