/**
 * Overwrite function to make add city, site etc. on node restriction
 * @param {Object} parentNode
 * @param {Object} level
 */
Ab.tree.TreeControl.prototype._createRestrictionForLevel = function (parentNode, level) {
    var restriction = this.createRestrictionForLevel(parentNode, level);
    if (!restriction) {
        restriction = new Ab.view.Restriction();
        // add the tree restriction to parameter list if not null.
        if (this.restriction && this.restriction.clauses != undefined && this.restriction.clauses.length > 0) {
            restriction.addClauses(this.restriction, true);
        }
        // add the tree level's restriction to parameter list if not null.
        var levelRest = this.getRestrictionForLevel(level);
        if (levelRest && levelRest.clauses != undefined && levelRest.clauses.length > 0) {
            restriction.addClauses(levelRest, true);
        }
        // add the parent node's restriction to parameter list. it should always contain something
        if (!parentNode.isRoot()) {
            if (this.type == 'hierTree' || this.type == 'selectValueHierTree') {
                restriction.addClauses(parentNode.restriction, true);
            } else {
                if (this._panelsData[level].useParentRestriction == true) {
                    restriction.addClauses(parentNode.restriction, true);
                }
            }
        }
        // regn level, we must add ctry
        if (level == 2
            && parentNode.parent.data['ctry.ctry_id'] != undefined) {
            restriction.addClause('bl.ctry_id', parentNode.parent.data['ctry.ctry_id']);
        }
        // state level, we must add ctry and regn
        if (level == 3
            && parentNode.parent.data['regn.regn_id'] != undefined
            && parentNode.parent.parent.data['ctry.ctry_id'] != undefined) {
            restriction.addClause('bl.regn_id', parentNode.parent.data['regn.regn_id']);
            restriction.addClause('bl.ctry_id', parentNode.parent.parent.data['ctry.ctry_id']);
        }
        // city level, we must add ctry, regn and state
        if (level == 4
            && parentNode.parent.data['state.state_id'] != undefined
            && parentNode.parent.parent.data['regn.regn_id'] != undefined
            && parentNode.parent.parent.parent.data['ctry.ctry_id'] != undefined) {
            restriction.addClause('bl.state_id', parentNode.parent.data['state.state_id']);
            restriction.addClause('bl.regn_id', parentNode.parent.parent.data['regn.regn_id']);
            restriction.addClause('bl.ctry_id', parentNode.parent.parent.parent.data['ctry.ctry_id']);
        }
        // bldg level, we must add ctry, regn, state and city
        if (level == 5
            && parentNode.parent.data['city.city_id'] != undefined
            && parentNode.parent.parent.data['state.state_id'] != undefined
            && parentNode.parent.parent.parent.data['regn.regn_id'] != undefined
            && parentNode.parent.parent.parent.parent.data['ctry.ctry_id'] != undefined) {
            restriction.addClause('bl.city_id', parentNode.parent.data['city.city_id']);
            restriction.addClause('bl.state_id', parentNode.parent.parent.data['state.state_id']);
            restriction.addClause('bl.regn_id', parentNode.parent.parent.parent.data['regn.regn_id']);
            restriction.addClause('bl.ctry_id', parentNode.parent.parent.parent.parent.data['ctry.ctry_id']);
        }
        // floor level, we must add ctry, regn, state, city and site
        if (level == 6
            && parentNode.parent.data['site.site_id'] != undefined
            && parentNode.parent.parent.data['city.city_id'] != undefined
            && parentNode.parent.parent.parent.data['state.state_id'] != undefined
            && parentNode.parent.parent.parent.parent.data['regn.regn_id'] != undefined
            && parentNode.parent.parent.parent.parent.parent.data['ctry.ctry_id'] != undefined) {
            restriction.addClause('rm.site_id', parentNode.parent.data['site.site_id']);
            restriction.addClause('rm.city_id', parentNode.parent.parent.data['city.city_id']);
            restriction.addClause('rm.state_id', parentNode.parent.parent.parent.data['state.state_id']);
            restriction.addClause('rm.regn_id', parentNode.parent.parent.parent.parent.data['regn.regn_id']);
            restriction.addClause('rm.ctry_id', parentNode.parent.parent.parent.parent.parent.data['ctry.ctry_id']);
        }
    }
    return restriction;
};
View.createController('abCbRptHlHazCtrl', {
    blId: null,
    flId: null,
    dwgName: null,
    mapControl: null,
    drawingController: null,
    drawingSvgId: null,
    filterController: null,
    // restrict data for the logged in user?
    restrictDataOnUser: false,
    // restriction on assessments: assigned to logged in user
    //TODO - pass this as Optional SQL Restrictions intro the .axvw file
    userAssignedRestriction: "(activity_log.assessed_by = '${user.name}'"
    + " OR activity_log.assigned_to IN (SELECT person_id FROM cb_accredit_person WHERE cb_accredit_person.em_id = ${sql.literal(user.employee.id)} )"
    + " OR activity_log.hcm_abate_by IN (SELECT person_id FROM cb_accredit_person WHERE cb_accredit_person.em_id = ${sql.literal(user.employee.id)}))",
    // true if floor selected, false if building selected
    showDrawing: false,
    afterViewLoad: function () {
        // show only the rooms/hazards/samples assigned to the logged in user, for Field Assessor or Abatement Worker users
        this.restrictDataOnUser = (this.view.taskInfo.processId == "Field Assessor" || this.view.taskInfo.processId == "Abatement Worker");
        this.filterController = View.controllers.get("abCbRptCommonFilterCtrl");
        this.filterController.panelsCtrl = this;
        this.filterController.visibleFields = "proj";
        this.filterController.isDocButtonMenu = true;
        // set userAssignedRestriction for paginated reports
        if (this.restrictDataOnUser) {
            this.filterController.userAssignedRestriction = this.userAssignedRestriction;
        }
        // set userAssignedRestriction to panels
        if (this.restrictDataOnUser) {
            this.abCbRptHlHaz_ctryTree.addParameter('userAssignedRestriction', this.userAssignedRestriction);
            this.abCbRptHlHaz_gridRepRooms.addParameter('userAssignedRestriction', this.userAssignedRestriction);
            this.abCbRptHlHaz_gridRepHazards.addParameter('userAssignedRestriction', this.userAssignedRestriction);
            this.abCbRptHlHaz_gridRepSamples.addParameter('userAssignedRestriction', this.userAssignedRestriction);
        }
        // set message parameter for abCbRptHlHaz_ctryTree panel
        this.abCbRptHlHaz_ctryTree.addParameter('noDrawing', getMessage('noDrawing'));
        // highlight in the displayed drawing, the selected room / the selected room's hazards / the selected room's samples
        this.abCbRptHlHaz_gridRepRooms.addEventListener('onMultipleSelectionChange', this.onGridsMultipleSelectionChange.createDelegate(this));
        // highlight in the displayed drawing, the selected hazard's room / the selected hazard / the selected hazard's samples
        this.abCbRptHlHaz_gridRepHazards.addEventListener('onMultipleSelectionChange', this.onGridsMultipleSelectionChange.createDelegate(this));
        // highlight in the displayed drawing, the selected sample's room / the selected sample's hazard / the selected sample
        this.abCbRptHlHaz_gridRepSamples.addEventListener('onMultipleSelectionChange', this.onGridsMultipleSelectionChange.createDelegate(this));
        // init map control
        this.initMapControl();
    },
    afterInitialDataFetch: function () {
        this.showPanels(false, false, true);
    },
    /**
     * Init map control.
     */
    initMapControl: function () {
        var configObject = new Ab.view.ConfigObject();
        configObject.mapImplementation = 'Esri';
        this.mapControl = new Ab.leaflet.Map('abCbRptHlHaz_htmlMap', 'abCbRptHlHaz_objMap', configObject);
        var basemapLayerMenu = this.abCbRptHlHaz_htmlMap.actions.get('basemapLayerMenu');
        basemapLayerMenu.clear();
        var basemapLayers = this.mapControl.getBasemapLayerList();
        for ( var i = 0; i < basemapLayers.length; i++ ) {
            basemapLayerMenu.addAction(i, basemapLayers[i], this.switchBasemapLayer.createDelegate(this));
        }
        this.mapControl.createMarkers(
            'abCbRptHlHaz_dsBuilding',
            ['bl.bl_id'],
            ['bl.lon', 'bl.lat'],
            'bl.name',
            ['bl.site_id', 'bl.bl_id', 'bl.name', 'bl.address'],
            {
                markerActionTitle: getMessage('labelShowDetails'),
                markerActionCallback: this.onBuildingDetails.createDelegate(this)
            }
        );
    },
    switchBasemapLayer: function (item) {
        //switch the mapControl layer based on the passed in layer name.
        this.mapControl.switchBasemapLayer(item.text);
    },
    /**
     * Init drawing control.
     */
    initDrawingControl: function () {
        this.drawingController = View.createControl({
            control: 'AssetDrawingControl',
            panel: 'drawingPanel',
            container: 'svgDiv',
            assetTypes: ['rm', 'activity_log', 'cb_samples'],
            topLayers: 'activity_log;cb_samples',
            assetTooltip: [
                {
                    assetType: 'rm',
                    datasource: 'abCbRptHlHaz_dsDrawingRmLabel',
                    fields: 'rm.rm_id;rm.rm_cat;rm.rm_type;rm.area'
                },
                {
                    assetType: 'activity_log',
                    datasource: 'abCbRptHlHaz_dsDrawingHazardsLabel',
                    fields: 'activity_log.activity_log_id;activity_log.prob_type;activity_log.rm_id;activity_log.hcm_id;activity_log.action_title;activity_log.vf_qty;activity_log.repair_type;activity_log.project_id'
                },
                {
                    assetType: 'cb_samples',
                    datasource: 'abCbRptHlHaz_dsDrawingSamplesLabel',
                    fields: 'cb_samples.sample_code;cb_samples.sample_loc;cb_samples.sample_desc;cb_samples.sample_type;cb_samples.date_collected;activity_log.bl_id;cb_samples.sample_id'
                }
            ],
            layersPopup: {
                layers: "rm-assets;rm-labels;activity_log-assets;cb_samples-assets;gros-assets;gros-labels;background",
                defaultLayers: "rm-assets;rm-labels;activity_log-assets;activity_log-labels;cb_samples-assets;cb_samples-labels;gros-assets;background"
            },
            showPanelTitle: true,
            listeners: {
                afterDrawingLoaded: function (drawingController, svgId) {
                    var controller = View.controllers.get('abCbRptHlHazCtrl');
                    controller.drawingSvgId = svgId;
                }
            }
        });
    },
    /**
     * Show building details for selected building.
     * @param {Object} node - building node.
     */
    showBuildingDetails: function (node) {
        //get bl id
        var blId = node.restriction.clauses[0].value;
        this.blId = blId;
        if (this.filterController.restriction) {
            this.abCbRptHlHaz_gridRepRooms.addParameter("consoleRestriction", this.filterController.restriction);
            this.abCbRptHlHaz_gridRepHazards.addParameter("consoleRestriction", this.filterController.restriction);
            this.abCbRptHlHaz_gridRepSamples.addParameter("consoleRestriction", this.filterController.restriction);
        }
        //refresh rooms panel
        var nodeRestriction = new Ab.view.Restriction();
        nodeRestriction.addClause('rm.bl_id', blId, '=');
        this.abCbRptHlHaz_gridRepRooms.refresh(nodeRestriction);
        setAllRowsSelectable(this.abCbRptHlHaz_gridRepRooms, false);
        //refresh assessments items panel
        nodeRestriction = new Ab.view.Restriction();
        nodeRestriction.addClause('activity_log.bl_id', blId, '=');
        this.abCbRptHlHaz_gridRepHazards.refresh(nodeRestriction);
        setAllRowsSelectable(this.abCbRptHlHaz_gridRepHazards, false);
        //refresh samples items panel
        this.abCbRptHlHaz_gridRepSamples.refresh(nodeRestriction);
        setAllRowsSelectable(this.abCbRptHlHaz_gridRepSamples, false);
        // load ESRI mapControl or building details
        var restriction = new Ab.view.Restriction();
        restriction.addClause('bl.bl_id', this.blId, '=');
        if (this.mapControl) {
            this.showPanels(false, true, true);
            this.showBuilding(restriction);
        }
    },
    /**
     * Show building on map.
     * @param {Ab.view.Restriction} restriction - building restriction.
     */
    showBuilding: function (restriction) {
        this.mapControl.clearMarkers();
        this.mapControl.showMarkers('abCbRptHlHaz_dsBuilding', restriction);
    },
    /**
     * Show building details map marker action.
     * @param {string} buildingId - Building id.
     */
    onBuildingDetails: function (buildingId) {
        var restriction = new Ab.view.Restriction();
        restriction.addClause('bl.bl_id', buildingId, '=');
        this.abCbRptHlHaz_blDetailsPanel.refresh(restriction);
        this.abCbRptHlHaz_blDetailsPanel.showInWindow({
            width: 1200,
            closeButton: true
        });
    },
    /**
     * Show details for selected floor.
     * @param {Object} node - floor node.
     */
    showFloorDetails: function (node) {
        this.showPanels(true, true, false);
        //set restrictions and refresh for abCbRptHlHaz_gridRepRooms panel
        if (this.filterController.restriction) {
            this.abCbRptHlHaz_gridRepRooms.addParameter("consoleRestriction", this.filterController.restriction);
            this.abCbRptHlHaz_gridRepHazards.addParameter("consoleRestriction", this.filterController.restriction);
            this.abCbRptHlHaz_gridRepSamples.addParameter("consoleRestriction", this.filterController.restriction);
        }
        var nodeRestriction = new Ab.view.Restriction();
        nodeRestriction.addClauses(node.restriction);
        this.abCbRptHlHaz_gridRepRooms.refresh(nodeRestriction);
        setAllRowsSelectable(this.abCbRptHlHaz_gridRepRooms, true);
        //set restrictions and refresh for abCbRptHlHaz_gridRepHazards panel
        nodeRestriction = new Ab.view.Restriction();
        nodeRestriction.addClause('activity_log.bl_id', node.restriction.findClause("rm.bl_id").value, '=', true);
        nodeRestriction.addClause('activity_log.fl_id', node.restriction.findClause("rm.fl_id").value, '=', true);
        this.abCbRptHlHaz_gridRepHazards.refresh(nodeRestriction);
        setAllRowsSelectable(this.abCbRptHlHaz_gridRepHazards, true);
        //set restrictions and refresh for abCbRptHlHaz_gridRepSamples panel
        this.abCbRptHlHaz_gridRepSamples.refresh(nodeRestriction);
        setAllRowsSelectable(this.abCbRptHlHaz_gridRepSamples, true);
        //set restrictions and refresh for abCbRptHlHaz_drawingPanel
        var currentNode = View.panels.get('abCbRptHlHaz_ctryTree').lastNodeClicked;
        this.blId = currentNode.data['rm.bl_id'];
        this.flId = currentNode.data['rm.fl_id'];
        this.dwgName = currentNode.data['rm.raw_dwgname'];
        this.displayDrawing();
    },
    /**
     * Displays the drawing, and select asset based on selected radio buttons.
     */
    displayDrawing: function () {
        if (!valueExists(this.drawingController)) {
            this.initDrawingControl();
        }
        if (!this.flId) {
            View.showMessage(getMessage("selectAFloor"));
            return;
        }
        var assetToDisplay = this.getSelectedRadioButton();
        if (assetToDisplay == 'rooms') {
            this.highlightDrawingByType('rm');
        } else if (assetToDisplay == 'hazards') {
            this.highlightDrawingByType('activity_log');
        } else if (assetToDisplay == 'samples') {
            this.highlightDrawingByType('cb_samples');
        }
        // unselect the rows in the grids
        this.abCbRptHlHaz_gridRepRooms.unselectAll();
        this.abCbRptHlHaz_gridRepHazards.unselectAll();
        this.abCbRptHlHaz_gridRepSamples.unselectAll();
    },
    /**
     * Show drawing and highlight assets based on selected radio buttons.
     * @param {string} assetToDisplay - Selected asset to display.
     */
    highlightDrawingByType: function (assetToDisplay) {
        var highlightAssets = [];
        switch (assetToDisplay) {
            case 'activity_log':
                highlightAssets.push({
                    assetType: 'activity_log',
                    assetIds: this.getAllHazardIdsForDrawing(),
                    color: '#FF8D1A'
                });
                break;
            case 'cb_samples':
                highlightAssets.push({
                    assetType: 'cb_samples',
                    assetIds: this.getAllSampleIdsForDrawing(),
                    color: '#141493'
                });
                break;
            default:
                highlightAssets.push({
                    assetType: 'rm',
                    assetIds: this.getAllRoomIdsForDrawing(),
                    color: '#FFFF00'
                });
        }
        this.drawingController.showDrawing({
            pkeyValues: {blId: this.blId, flId: this.flId},
            highlightAssets: highlightAssets
        });
    },
    /**
     * Called on onMultipleSelectionChange for the grids.
     *
     * If showing rooms asset, when an assessment or a sample is selected, highlight just the associated room.
     * If showing assessments asset and a room is selected, highlight all assessments for that room. If a sample is selected, highlight just the one assessment for the sample.
     * If showing samples and a room or assessment is selected, highlight all samples for that room or assessment.
     *
     * If the row is unselected, remove user-highlight for all items, and highlight again all selected items from the 3 grids
     * @param {Object} row - Selected grid row.
     */
    onGridsMultipleSelectionChange: function (row) {
        var assetToDisplay = this.getSelectedRadioButton();
        var gridId = row.grid.id;
        var items = [];
        switch (assetToDisplay) {
            case "rooms":
                items = this.getRoomIdsForDrawing([row.row]);
                break;
            case "hazards":
                items = this.getHazardIdsForDrawing([row.row]);
                break;
            case "samples":
                items = this.getSampleIdsForDrawing([row.row]);
                break;
            default:
                break;
        }
        if (assetToDisplay == "hazards"
            && (gridId == "abCbRptHlHaz_gridRepRooms" || gridId == "abCbRptHlHaz_gridRepSamples")) {

            // search in Hazards grid all rows for the room or for the sample
            var hazards = this.getAssetRows([row.row], View.panels.get("abCbRptHlHaz_gridRepHazards"));
            items = this.getHazardIdsForDrawing(hazards);
        } else if (assetToDisplay == "samples"
            && (gridId == "abCbRptHlHaz_gridRepRooms" || gridId == "abCbRptHlHaz_gridRepHazards")) {

            // search in Samples grid all rows for the room or for the hazard
            var samples = this.getAssetRows([row.row], View.panels.get("abCbRptHlHaz_gridRepSamples"));
            items = this.getSampleIdsForDrawing(samples);
        }
        if (!row.row.isSelected()) {
            this.clearAssets(assetToDisplay, items);
            items = this.getSelectedAssetIdsFromAllGrids();
        }
        this.highlightAssets(assetToDisplay, items);
    },
    /**
     * Highlight assets.
     * @param {string} assetToDisplay - Selected asset to display.
     * @param {Array.<Object>} items - selected items
     */
    highlightAssets: function (assetToDisplay, items) {
        if (!valueExistsNotEmpty(this.dwgName)) {
            return;
        }
        var color = this.getColorForHighlight(assetToDisplay);
        var assetsToHighlight = {};
        var highlightController = this.drawingController.svgControl.getController("HighlightController");
        for ( var i = 0; i < items.length; i++ ) {
            var item = items[i];
            var blId = item.bl_id;
            var flId = item.fl_id;
            var assetId = "";
            if (assetToDisplay == "rooms") {
                assetId = blId + ";" + flId + ";" + item.rm_id;
            } else if (assetToDisplay == "hazards") {
                assetId = item.activity_log_id;
            } else if (assetToDisplay == "samples") {
                assetId = item.sample_id;
            }
            var highlightedAsset = highlightController.highlightedAssets[assetId];
            if (highlightedAsset && highlightedAsset.color !== color) {
                assetsToHighlight[assetId] = {
                    svgId: this.drawingSvgId,
                    color: color,
                    persistFill: false,
                    overwriteFill: true
                };
            }
        }
        if (!_.isEmpty(assetsToHighlight)) {
            highlightController.highlightAssets(assetsToHighlight);
        }
    },
    /**
     * Clear assets.
     * @param {string} assetToDisplay - Selected asset to display.
     * @param {Array.<Object>} items - selected items
     */
    clearAssets: function (assetToDisplay, items) {
        if (!valueExistsNotEmpty(this.dwgName)) {
            return;
        }
        var assetsToReset = [];
        for ( var i = 0; i < items.length; i++ ) {
            var item = items[i];
            var blId = item.bl_id;
            var flId = item.fl_id;
            var assetId = "";
            if (assetToDisplay == "rooms") {
                assetId = blId + ";" + flId + ";" + item.rm_id;
            } else if (assetToDisplay == "hazards") {
                assetId = item.activity_log_id;
            } else if (assetToDisplay == "samples") {
                assetId = item.sample_id;
            }
            assetsToReset.push(assetId);
        }
        if (assetsToReset.length > 0) {
            this.drawingController.svgControl.getController("HighlightController").clearAssets(assetsToReset, {
                svgId: this.drawingSvgId
            });
        }
    },
    /**
     * Shows the tree according to the user restrictions
     * @param {string} restriction - Console restriction
     */
    refreshOnFilter: function (restriction) {
        this.abCbRptHlHaz_ctryTree.addParameter('consoleRestriction', restriction);
        this.abCbRptHlHaz_ctryTree.refresh();
        this.showPanels(false, false, true);
        this.mapControl.clearMarkers();
        this.mapControl.mapClass.map.setZoom(3); // reset the zoom of the map.
    },
    /**
     * Show/ hide panels.
     * @param {boolean} showDrawing - Show drawing panel.
     * @param {boolean} showReport - Show asset report grid.
     * @param {boolean} showMap - Show map panel.
     */
    showPanels: function (showDrawing, showReport, showMap) {
        this.showDrawing = showDrawing;
        this.drawingPanel.show(showDrawing);
        if (this.drawingController && !showDrawing) {
            this.drawingController.clearDrawingPanel();
        }
        this.abCbRptHlHaz_gridRepRooms.show(showReport);
        this.abCbRptHlHaz_gridRepHazards.show(showReport);
        this.abCbRptHlHaz_gridRepSamples.show(showReport);
        this.abCbRptHlHaz_htmlMap.show(showMap);
    },
    /**
     * Creates an array of room ids to highlight.
     * @param rooms - Array of grid rows to extract room ids from
     * @returns Array.<Object> - Array of {bl_id: bldg id, fl_id: floor id; rm_id: room id; dwgname: drawing name} objects
     */
    getRoomIdsForDrawing: function (rooms) {
        var roomIds = [];
        var tableName = "rm";
        if (rooms.length > 0 && rooms[0].panel.id != "abCbRptHlHaz_gridRepRooms") {
            tableName = "activity_log";
        }
        for ( var i = 0; i < rooms.length; i++ ) {
            var room = rooms[i];
            var dwgName = "";
            if (room.panel.id != "abCbRptHlHaz_gridRepSamples") {
                dwgName = room.getFieldValue(tableName + ".dwgname");
            } else {
                dwgName = room.getFieldValue("cb_samples.dwgname");
            }
            if (dwgName) {
                roomIds.push({
                    bl_id: room.getFieldValue(tableName + ".bl_id"),
                    fl_id: room.getFieldValue(tableName + ".fl_id"),
                    rm_id: room.getFieldValue(tableName + ".rm_id")
                });
            }
        }
        return roomIds;
    },
    /**
     * Creates an array of room ids to highlight.
     * @returns Array of rm.rm_id.
     */
    getAllRoomIdsForDrawing: function () {
        var roomIds = [];
        var grid = View.panels.get('abCbRptHlHaz_gridRepRooms');
        var parameters = grid.getParametersForRefresh();
        parameters.recordLimit = -1;
        parameters.indexField = null;
        var rooms = grid.getData(parameters).data.records;
        for ( var i = 0; i < rooms.length; i++ ) {
            var room = rooms[i];
            var dwgName = room["rm.dwgname"];
            if (dwgName) {
                roomIds.push(room["rm.rm_id"]);
            }
        }
        return roomIds;
    },
    /**
     * Creates an array of hazard ids to highlight.
     * @param {Array} hazards - Array of grid rows to extract hazard ids from.
     * @returns Array of {bl_id: bldg id, fl_id: floor id; rm_id: room id; activity_log_id: hazard id; dwgname: drawing name} objects.
     */
    getHazardIdsForDrawing: function (hazards) {
        var hazardIds = [];
        var tableName = "rm";
        if (hazards.length > 0 && hazards[0].panel.id != "abCbRptHlHaz_gridRepRooms") {
            tableName = "activity_log";
        }
        for ( var i = 0; i < hazards.length; i++ ) {
            var hazard = hazards[i];
            var dwgName = hazard.getFieldValue(tableName + ".dwgname");
            if (dwgName) {
                hazardIds.push({
                    bl_id: hazard.getFieldValue(tableName + ".bl_id"),
                    fl_id: hazard.getFieldValue(tableName + ".fl_id"),
                    rm_id: hazard.getFieldValue(tableName + ".rm_id"),
                    activity_log_id: hazard.getFieldValue("activity_log.activity_log_id")
                });
            }
        }
        return hazardIds;
    },
    /**
     * Creates an array of hazard ids to highlight.
     * @returns Array of activity_log.activity_log_id.
     */
    getAllHazardIdsForDrawing: function () {
        var hazardIds = [];
        var grid = View.panels.get('abCbRptHlHaz_gridRepHazards');
        var parameters = grid.getParametersForRefresh();
        parameters.recordLimit = -1;
        parameters.indexField = null;
        var hazards = grid.getData(parameters).data.records;
        for ( var i = 0; i < hazards.length; i++ ) {
            var hazard = hazards[i];
            var dwgName = hazard["activity_log.dwgname"];
            if (dwgName) {
                hazardIds.push(hazard["activity_log.activity_log_id"]);
            }
        }
        return hazardIds;
    },
    /**
     * Creates an array of sample ids to highlight.
     * @param {Array} samples  - Array of grid rows to extract sample ids from.
     * @returns Array of {bl_id: bldg id, fl_id: floor id; rm_id: room id; sample_id: sample id; dwgname: drawing name} objects.
     */
    getSampleIdsForDrawing: function (samples) {
        var sampleIds = [];
        var tableName = "rm";
        if (samples.length > 0 && samples[0].panel.id != "abCbRptHlHaz_gridRepRooms") {
            tableName = "activity_log";
        }
        for ( var i = 0; i < samples.length; i++ ) {
            var sample = samples[i];
            var dwgName = sample.getFieldValue("cb_samples.dwgname");
            if (dwgName) {
                sampleIds.push({
                    bl_id: sample.getFieldValue(tableName + ".bl_id"),
                    fl_id: sample.getFieldValue(tableName + ".fl_id"),
                    rm_id: sample.getFieldValue(tableName + ".rm_id"),
                    sample_id: sample.getFieldValue("cb_samples.sample_id")
                });
            }
        }
        return sampleIds;
    },
    /**
     * Creates an array of sample ids to highlight.
     * @returns Array of cb_samples.sample_id.
     */
    getAllSampleIdsForDrawing: function () {
        var sampleIds = [];
        var grid = View.panels.get('abCbRptHlHaz_gridRepSamples');
        var parameters = grid.getParametersForRefresh();
        parameters.recordLimit = -1;
        parameters.indexField = null;
        var samples = grid.getData(parameters).data.records;
        for ( var i = 0; i < samples.length; i++ ) {
            var sample = samples[i];
            var dwgName = sample["cb_samples.dwgname"];
            if (dwgName) {
                sampleIds.push(sample["cb_samples.sample_id"]);
            }
        }
        return sampleIds;
    },
    /**
     * Returns an array of rooms, hazards or samples IDs (depending on the assets to highlight)
     * for all the assets selected in the 3 grids (Rooms, Hazards, Samples).
     * @returns {Array} - Array of selected assets.
     */
    getSelectedAssetIdsFromAllGrids: function () {
        var assetToDisplay = this.getSelectedRadioButton();
        var rooms = this.abCbRptHlHaz_gridRepRooms.getSelectedGridRows(),
            hazards = this.abCbRptHlHaz_gridRepHazards.getSelectedGridRows(),
            samples = this.abCbRptHlHaz_gridRepSamples.getSelectedGridRows();
        var assets = [];
        switch (assetToDisplay) {
            case "rooms":
                assets = this.getRoomIdsForDrawing(rooms);
                assets = assets.concat(this.getRoomIdsForDrawing(hazards));
                assets = assets.concat(this.getRoomIdsForDrawing(samples));
                break;
            case "hazards":
                // search in Hazards grid all rows for the room
                var roomHazards = this.getAssetRows(rooms, View.panels.get("abCbRptHlHaz_gridRepHazards"));
                assets = this.getHazardIdsForDrawing(roomHazards);
                assets = assets.concat(this.getHazardIdsForDrawing(hazards));
                // search in Samples grid all rows for the hazard
                var roomSamples = this.getAssetRows(samples, View.panels.get("abCbRptHlHaz_gridRepHazards"));
                assets = assets.concat(this.getHazardIdsForDrawing(roomSamples));
                break;
            case "samples":
                // search in Samples grid all rows for the room
                var roomSamples = this.getAssetRows(rooms, View.panels.get("abCbRptHlHaz_gridRepSamples"));
                assets = this.getSampleIdsForDrawing(roomSamples);
                // search in Samples grid all rows for the hazard
                var hazardSamples = this.getAssetRows(hazards, View.panels.get("abCbRptHlHaz_gridRepSamples"));
                assets = assets.concat(this.getSampleIdsForDrawing(hazardSamples));
                assets = assets.concat(this.getSampleIdsForDrawing(samples));
                break;
            default:
                break;
        }
        return assets;
    },
    /**
     * Gets the hazard rows from Hazards grid
     * for the given room or sample rows
     * OR
     * Gets the sample rows from Samples grid
     * for the given room or hazard rows
     * @param {Array.<Object>} rows  - Selected rows.
     * @param {string} assetGrid  - Asset grid.
     */
    getAssetRows: function (rows, assetGrid) {
        var assets = [];
        for ( var j = 0; j < rows.length; j++ ) {
            var row = rows[j];
            for ( var i = 0; i < assetGrid.rows.length; i++ ) {
                var assetRow = assetGrid.rows[i].row;
                if (row.panel.id == "abCbRptHlHaz_gridRepRooms") {
                    if (assetRow.getFieldValue('activity_log.bl_id') == row.getFieldValue('rm.bl_id')
                        && assetRow.getFieldValue('activity_log.fl_id') == row.getFieldValue('rm.fl_id')
                        && assetRow.getFieldValue('activity_log.rm_id') == row.getFieldValue('rm.rm_id'))
                        assets.push(assetRow);
                } else {
                    if (assetRow.getFieldValue('activity_log.activity_log_id') == row.getFieldValue('activity_log.activity_log_id'))
                        assets.push(assetRow);
                }
            }
        }
        return assets;
    },
    /**
     * Get the Asset Types radio button's selected value.
     * @returns {string} - radio value.
     */
    getSelectedRadioButton: function () {
        var value = null;
        var radioButtons = document.getElementsByName("radioAssetType");
        for ( var i = 0; i < radioButtons.length; i++ ) {
            if (radioButtons[i].checked) {
                value = radioButtons[i].value;
                break;
            }
        }
        return value;
    },
    /**
     * Returns the color for highlight depending on:
     * - if the row is selected
     * - the assets type to display
     * @param {string} assetToDisplay - Selected asset to display.
     * @returns {string} - Asset color..
     */
    getColorForHighlight: function (assetToDisplay) {
        var color = null;
        if (assetToDisplay == 'rooms') {
            color = '#0000FF';	// blue
        } else if (assetToDisplay == 'hazards') {
            color = '#FF0000';	// red
        } else if (assetToDisplay == 'samples') {
            color = '#FF0000';	// red
        }
        return color;
    }
});
/**
 * Show building details.
 * @param {Object} node - building node.
 */
function showBuilding(node) {
    var controller = View.controllers.get('abCbRptHlHazCtrl');
    controller.showBuildingDetails(node);
}
/**
 * Show floor details.
 * @param {Object} node - floor node.
 */
function showFloor(node) {
    var controller = View.controllers.get('abCbRptHlHazCtrl');
    controller.showFloorDetails(node);
}