var abCbRptTotByYearCtrl = View.createController('abCbRptTotByYear', {
	
	selectionLabel: null,
	
	chartType: null,
	
	printableRestriction: [],
	
	afterViewLoad: function(){
		$('label_chartType_line').innerHTML = getMessage("label_line_chart");
		$('label_chartType_stacked').innerHTML = getMessage("label_stacked_chart");
	},
	
	/**
	 * Event listener for 'Show' button from filter panel
	 */
	abCbRptTotByYear_filter_onShow: function(){
    	// validateDates
    	var startDate = this.abCbRptTotByYear_filter.getFieldValue("dateFrom");
    	var endDate = this.abCbRptTotByYear_filter.getFieldValue("dateTo");
    	if(!validateDates(startDate, endDate))
        	return;
    	
    	this.selectionLabel = getFilterSelectionAsLabel(this.abCbRptTotByYear_filter);
    	var restrictions = getFilterRestriction(this.abCbRptTotByYear_filter);
		var restriction = restrictions.restriction;
		this.printableRestriction = restrictions.printableRestriction;
		
		if(document.getElementById('rad_chartType_line').checked){
			this.abCbRptTotByYear_totalChartBar.show(false);
			this.abCbRptTotByYear_totalChartLine.addParameter('none_param_value', getMessage('noneParam'));
			this.abCbRptTotByYear_totalChartLine.addParameter('filterRestriction', restriction);
			this.abCbRptTotByYear_totalChartLine.refresh(restriction);
		}else if(document.getElementById('rad_chartType_stacked').checked){
			this.abCbRptTotByYear_totalChartLine.show(false);
			this.abCbRptTotByYear_totalChartBar.addParameter('none_param_value', getMessage('noneParam'));
			this.abCbRptTotByYear_totalChartBar.addParameter('filterRestriction', restriction);
			this.abCbRptTotByYear_totalChartBar.refresh(restriction);
		}
				
		
		
	},
	
	abCbRptTotByYear_totalChartLine_afterRefresh: function(){
		this.abCbRptTotByYear_totalChartLine.setInstructions(this.selectionLabel);
		
	},
	abCbRptTotByYear_totalChartBar_afterRefresh: function(){
		this.abCbRptTotByYear_totalChartBar.setInstructions(this.selectionLabel);
		
	}
});
