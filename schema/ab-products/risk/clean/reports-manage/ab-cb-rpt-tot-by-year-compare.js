var abCbRptTotByYearCompareCtrl = View.createController('abCbRptTotByYearCompCtrl', {
	
	selectionLabel: null,
	
	printableRestriction: [],
	
	afterViewLoad: function(){
		$('label_chartType_line').innerHTML = getMessage("label_line_chart");
		$('label_chartType_stacked').innerHTML = getMessage("label_stacked_chart");
	},
	
	/**
	 * Event listener for 'Show' button from filter panel
	 */
	abCbRptTotByYearCompare_filter_onShow: function(){
    	// validateDates
    	var startDate = this.abCbRptTotByYearCompare_filter.getFieldValue("dateFrom");
    	var endDate = this.abCbRptTotByYearCompare_filter.getFieldValue("dateTo");
    	if(!validateDates(startDate, endDate))
        	return;
		
		var siteId = this.abCbRptTotByYearCompare_filter.getFieldValue('bl.site_id');
		var blId = this.abCbRptTotByYearCompare_filter.getFieldValue('activity_log.bl_id');
		if(!siteId && !blId){
			View.showMessage(getMessage('errSelection'));
			return;
		}else if(blId){
			this.abCbRptTotByYearCompare_totalChartBar.addParameter('groupBy','activity_log.bl_id');
			this.abCbRptTotByYearCompare_totalChartLine.addParameter('groupBy','activity_log.bl_id');
		}else{
			this.abCbRptTotByYearCompare_totalChartBar.addParameter('groupBy','activity_log.site_id');
			this.abCbRptTotByYearCompare_totalChartLine.addParameter('groupBy','activity_log.site_id');
		}
		this.selectionLabel = getFilterSelectionAsLabel(this.abCbRptTotByYearCompare_filter);
		
		var restrictions = getFilterRestriction(this.abCbRptTotByYearCompare_filter);
		var restriction = restrictions.restriction;
		this.printableRestriction = restrictions.printableRestriction;
		
		if(document.getElementById('rad_chartType_line').checked){
			this.abCbRptTotByYearCompare_totalChartBar.show(false);
			this.abCbRptTotByYearCompare_totalChartLine.addParameter('filterRestriction', restriction);
			this.abCbRptTotByYearCompare_totalChartLine.refresh();
		}else if(document.getElementById('rad_chartType_stacked').checked){
			this.abCbRptTotByYearCompare_totalChartLine.show(false);
			this.abCbRptTotByYearCompare_totalChartBar.addParameter('filterRestriction', restriction);
			this.abCbRptTotByYearCompare_totalChartBar.refresh();
		}
		
	},
	
	
	abCbRptTotByYearCompare_totalChartLine_afterRefresh: function(){
		this.abCbRptTotByYearCompare_totalChartLine.setInstructions(this.selectionLabel);
		
	},
	
	abCbRptTotByYearCompare_totalChartLineBar_afterRefresh: function(){
		this.abCbRptTotByYearCompare_totalChartBar.setInstructions(this.selectionLabel);
		
	}
})


