var abCbRptCostByPrjCtrl = View.createController('abCbRptCostByPrj', {
	
	selectionLabel: null,
	
	printableRestriction: [],
	
	loaded: false,
	
	
	afterViewLoad: function(){
		this.abCbRptCostTabs.addEventListener('afterTabChange', afterTabChange);
	},
	
	/**
	 * Event listener for 'Show' button from filter panel
	 */
	abCbRptCostByPrj_filter_onShow: function(){
    	// validateDates
    	var startDate = this.abCbRptCostByPrj_filter.getFieldValue("dateFrom");
    	var endDate = this.abCbRptCostByPrj_filter.getFieldValue("dateTo");
    	if(!validateDates(startDate, endDate))
        	return;
		
		this.selectionLabel = getFilterSelectionAsLabel(this.abCbRptCostByPrj_filter);
		var restrictions = getFilterRestriction(this.abCbRptCostByPrj_filter, null, getMessage("projectFieldLabel"));
		var restriction = restrictions.restriction;
		this.printableRestriction = restrictions.printableRestriction;
		
		this.abCbRptCostByPrjChart.addParameter('filterRestriction', restriction);
		this.abCbRptCostByPrjChart.refresh(restriction);
		this.abCbRptCostByPrjChartActual.addParameter('filterRestriction', restriction);
		this.abCbRptCostByPrjChartActual.refresh(restriction);
		this.abCbRptCostByPrjChartEstimated.addParameter('filterRestriction', restriction);
		this.abCbRptCostByPrjChartEstimated.refresh(restriction);
		this.loaded = true;

	},
	
	
	abCbRptCostByPrjChart_afterRefresh: function(){
		this.abCbRptCostByPrjChart.setInstructions(this.selectionLabel);
		
	},
	
	abCbRptCostByPrjChartActual_afterRefresh: function(){
		this.abCbRptCostByPrjChartActual.setInstructions(this.selectionLabel);
		
	},
	
	abCbRptCostByPrjChartEstimated_afterRefresh: function(){
		this.abCbRptCostByPrjChartEstimated.setInstructions(this.selectionLabel);
		
	}
});

function afterTabChange(tabPanel, currentTabName){
	// Do the actual refresh of selected tab after 50 milliseconds, workaround for disappearing chart control bug
  setTimeout("afterTabChangeRefresh()",50);	   
}

function afterTabChangeRefresh(){
	var controller = View.controllers.get("abCbRptCostByPrj");
	if(controller.loaded){	
		controller.abCbRptCostByPrj_filter_onShow();
	}
}
