var cashFlowDetailsCtrl = View.createController('cashFlowCtrlDetails', {
	projectionType:null,
	IDs:'',
	isRecurring:null,
	isSchedule:null,
	isActual:null,
	restriction: null,
	restriction_recur:null,
	restriction_sched:null,
	restriction_actual:null,
	ID:null,
	dateStart:null,
	dateEnd:null,

	// user to avoid SQL security issue in details report
	buildingGeographicalRestriction: null,
	propertyGeographicalRestriction: null,
	costTranRecurRestriction: null,
	dateRestrictionRecur: null,
	dateRestriction: null,
	
	setCashFlowParameters:function(result, projectionType, isRecurring, isSchedule, isActual, event, restriction, calculationPeriod, buildingGeoRestriction, propertyGeoRestriction, costTranRecurRestriction){
		var restrictionValue_projectionType = '';
		var restrictionValue_selectedDate = '';
		var date_restriction_recur = '';
		var date_restriction = '';
		
		this.buildingGeographicalRestriction = buildingGeoRestriction;
		this.propertyGeographicalRestriction = propertyGeoRestriction;
		//this.costTranRecurRestriction = costTranRecurRestriction;
		
		for (var i = 0; i < event.restriction.clauses.length; i++)
			{
			if(event.restriction.clauses[i].name == 'cost_tran_recur.ls_id')
				{
				restrictionValue_projectionType = event.restriction.clauses[i].value;
				var index = restrictionValue_projectionType.indexOf(Ab.form.Form.MULTIPLE_VALUES_SEPARATOR);
				restrictionValue_projectionType = restrictionValue_projectionType.substring(0,index);
				}
			if(event.restriction.clauses[i].name == 'cost_tran_recur.year')
				{
				restrictionValue_selectedDate = event.restriction.clauses[i].value;
				}
			}
		this.projectionType = projectionType;
		this.isRecurring = isRecurring;
		this.isSchedule = isSchedule;
		this.isActual = isActual;
		if (restrictionValue_projectionType != ''){
			this.restriction = (this.projectionType == 'property'?'pr':this.projectionType) +'_id'+'='+"'"+restrictionValue_projectionType+"' ";
			this.costTranRecurRestriction = " cost_tran_recur." + (this.projectionType == 'property'?'pr':this.projectionType) +'_id'+'='+"'"+restrictionValue_projectionType+"' ";
		}else{ 
			this.restriction = " 1=1 ";
			this.costTranRecurRestriction = " 1=1 "
		}
		this.ID = restrictionValue_projectionType;
		this.restriction += "AND "+restriction;
		this.costTranRecurRestriction += "AND "+costTranRecurRestriction;
		
		if (restrictionValue_selectedDate != ''){
			this.dateStart = restrictionValue_selectedDate;
			this.dateEnd = calculateDateEnd(calculationPeriod,restrictionValue_selectedDate);
			date_restriction_recur = " AND (";
			date_restriction = " AND (";
			if(this.isRecurring) {
				date_restriction_recur += " cost_tran_recur.date_end >= ${sql.date('"+this.dateStart+"')}";
			}
			if(this.isSchedule || this.isActual) {
				// looking for costs (paid in the selected period) OR (due in the period but not paid yet) 
				date_restriction += " ( (cost_tran_recur.date_paid IS NOT NULL"
										+ " AND cost_tran_recur.date_paid >= ${sql.date('"+this.dateStart+"')}"
										+ " AND cost_tran_recur.date_paid <= ${sql.date('"+this.dateEnd+"')}"
										+ ")"
									+ " OR "
									+ " (cost_tran_recur.date_paid IS NULL"
										+ " AND cost_tran_recur.date_due >= ${sql.date('"+this.dateStart+"')}"
										+ " AND cost_tran_recur.date_due <= ${sql.date('"+this.dateEnd+"')}"
										+ ") ) ";
			}
			date_restriction_recur += " AND cost_tran_recur.date_start <= ${sql.date('"+this.dateEnd+"')} ) ";
			date_restriction += " AND cost_tran_recur.date_start <= ${sql.date('"+this.dateEnd+"')} ) ";
			this.dateRestrictionRecur = date_restriction_recur;
			this.dateRestriction = date_restriction;
			
		}
		if (this.isRecurring){
			this.restriction_recur = this.restriction+date_restriction_recur;
		}
		if (this.isSchedule){
			this.restriction_sched = this.restriction+date_restriction;
			this.restriction_sched = this.restriction_sched.replace(/cost_tran_recur/g,"cost_tran_sched");
			this.restriction_sched = this.restriction_sched.replace(/date_end/g,"date_due");
			this.restriction_sched = this.restriction_sched.replace(/date_start/g,"date_trans_created");
		}
		if (this.isActual){
			this.restriction_actual = this.restriction+date_restriction;
			this.restriction_actual = this.restriction_actual.replace(/cost_tran_recur/g,"cost_tran");
			this.restriction_actual = this.restriction_actual.replace(/date_start/g,"date_trans_created");
			this.restriction_actual = this.restriction_actual.replace(/date_end/g,"date_due");
		}

		this.showData();
	},
	
	showData:function(){
		View.controllers.get('cashFlowCtrlDetails').costTranRecurDetailsPanel.show(false);
		View.controllers.get('cashFlowCtrlDetails').costTranSchedDetailsPanel.show(false);
		View.controllers.get('cashFlowCtrlDetails').costTranActualDetailsPanel.show(false);
		View.controllers.get('cashFlowCtrlDetails').lsDetailsPanel.show(false);
		View.controllers.get('cashFlowCtrlDetails').blDetailsPanel.show(false);
		View.controllers.get('cashFlowCtrlDetails').prDetailsPanel.show(false);
		View.controllers.get('cashFlowCtrlDetails').acDetailsPanel.show(false);
		
		if (this.ID != ''){
		
		switch(this.projectionType){
			case 'ls':	
						View.controllers.get('cashFlowCtrlDetails').lsDetailsPanel.refresh(new Ab.view.Restriction({'ls.ls_id': this.ID}));
						break;
			case 'bl':
						View.controllers.get('cashFlowCtrlDetails').blDetailsPanel.refresh(new Ab.view.Restriction({'bl.bl_id': this.ID}));
						break;
			case 'property':
						View.controllers.get('cashFlowCtrlDetails').prDetailsPanel.refresh(new Ab.view.Restriction({'property.pr_id': this.ID}));
						break;
			case 'ac':
						View.controllers.get('cashFlowCtrlDetails').acDetailsPanel.refresh(new Ab.view.Restriction({'ac.ac_id': this.ID}));
						break;
		}
		}
		
		if (this.isRecurring){
			var costTranRecurRestriction = this.costTranRecurRestriction + this.ifEmptyOrNull(this.dateRestrictionRecur, '');
			View.controllers.get('cashFlowCtrlDetails').costTranRecurDetailsPanel.addParameter('costRestriction', this.ifEmptyOrNull(costTranRecurRestriction, '1=1'));
			View.controllers.get('cashFlowCtrlDetails').costTranRecurDetailsPanel.addParameter('buildingRestriction', this.ifEmptyOrNull(this.buildingGeographicalRestriction, ''));
			View.controllers.get('cashFlowCtrlDetails').costTranRecurDetailsPanel.addParameter('propertyRestriction', this.ifEmptyOrNull(this.propertyGeographicalRestriction, ''));

			if ("ls" == this.projectionType) {
				View.controllers.get('cashFlowCtrlDetails').costTranRecurDetailsPanel.addParameter('isForLease', true);
				View.controllers.get('cashFlowCtrlDetails').costTranRecurDetailsPanel.addParameter('isForBuilding', false);
				View.controllers.get('cashFlowCtrlDetails').costTranRecurDetailsPanel.addParameter('isForProperty', false);
				View.controllers.get('cashFlowCtrlDetails').costTranRecurDetailsPanel.addParameter('isForAccount', false);
			} else if ("bl" == this.projectionType) {
				View.controllers.get('cashFlowCtrlDetails').costTranRecurDetailsPanel.addParameter('isForLease', false);
				View.controllers.get('cashFlowCtrlDetails').costTranRecurDetailsPanel.addParameter('isForBuilding', true);
				View.controllers.get('cashFlowCtrlDetails').costTranRecurDetailsPanel.addParameter('isForProperty', false);
				View.controllers.get('cashFlowCtrlDetails').costTranRecurDetailsPanel.addParameter('isForAccount', false);
			} else if ("property" == this.projectionType) {
				View.controllers.get('cashFlowCtrlDetails').costTranRecurDetailsPanel.addParameter('isForLease', false);
				View.controllers.get('cashFlowCtrlDetails').costTranRecurDetailsPanel.addParameter('isForBuilding', false);
				View.controllers.get('cashFlowCtrlDetails').costTranRecurDetailsPanel.addParameter('isForProperty', true);
				View.controllers.get('cashFlowCtrlDetails').costTranRecurDetailsPanel.addParameter('isForAccount', false);
			} else if ("ac" == this.projectionType) {
				View.controllers.get('cashFlowCtrlDetails').costTranRecurDetailsPanel.addParameter('isForLease', false);
				View.controllers.get('cashFlowCtrlDetails').costTranRecurDetailsPanel.addParameter('isForBuilding', false);
				View.controllers.get('cashFlowCtrlDetails').costTranRecurDetailsPanel.addParameter('isForProperty', false);
				View.controllers.get('cashFlowCtrlDetails').costTranRecurDetailsPanel.addParameter('isForAccount', true);
			}
			View.controllers.get('cashFlowCtrlDetails').costTranRecurDetailsPanel.refresh();
		}
		if (this.isSchedule){
			var costTranSchedRestriction = this.costTranRecurRestriction + this.ifEmptyOrNull(this.dateRestriction, '');
			costTranSchedRestriction = costTranSchedRestriction.replace(/cost_tran_recur/g,"cost_tran_sched");
			costTranSchedRestriction = costTranSchedRestriction.replace(/date_end/g,"date_due");
			costTranSchedRestriction = costTranSchedRestriction.replace(/date_start/g,"date_trans_created");
			
			View.controllers.get('cashFlowCtrlDetails').costTranSchedDetailsPanel.addParameter('costRestriction', this.ifEmptyOrNull(costTranSchedRestriction, '1=1'));
			View.controllers.get('cashFlowCtrlDetails').costTranSchedDetailsPanel.addParameter('buildingRestriction', this.ifEmptyOrNull(this.buildingGeographicalRestriction, ''));
			View.controllers.get('cashFlowCtrlDetails').costTranSchedDetailsPanel.addParameter('propertyRestriction', this.ifEmptyOrNull(this.propertyGeographicalRestriction, ''));
			if ("ls" == this.projectionType) {
				View.controllers.get('cashFlowCtrlDetails').costTranSchedDetailsPanel.addParameter('isForLease', true);
				View.controllers.get('cashFlowCtrlDetails').costTranSchedDetailsPanel.addParameter('isForBuilding', false);
				View.controllers.get('cashFlowCtrlDetails').costTranSchedDetailsPanel.addParameter('isForProperty', false);
				View.controllers.get('cashFlowCtrlDetails').costTranSchedDetailsPanel.addParameter('isForAccount', false);
			} else if ("bl" == this.projectionType) {
				View.controllers.get('cashFlowCtrlDetails').costTranSchedDetailsPanel.addParameter('isForLease', false);
				View.controllers.get('cashFlowCtrlDetails').costTranSchedDetailsPanel.addParameter('isForBuilding', true);
				View.controllers.get('cashFlowCtrlDetails').costTranSchedDetailsPanel.addParameter('isForProperty', false);
				View.controllers.get('cashFlowCtrlDetails').costTranSchedDetailsPanel.addParameter('isForAccount', false);
			} else if ("property" == this.projectionType) {
				View.controllers.get('cashFlowCtrlDetails').costTranSchedDetailsPanel.addParameter('isForLease', false);
				View.controllers.get('cashFlowCtrlDetails').costTranSchedDetailsPanel.addParameter('isForBuilding', false);
				View.controllers.get('cashFlowCtrlDetails').costTranSchedDetailsPanel.addParameter('isForProperty', true);
				View.controllers.get('cashFlowCtrlDetails').costTranSchedDetailsPanel.addParameter('isForAccount', false);
			} else if ("ac" == this.projectionType) {
				View.controllers.get('cashFlowCtrlDetails').costTranSchedDetailsPanel.addParameter('isForLease', false);
				View.controllers.get('cashFlowCtrlDetails').costTranSchedDetailsPanel.addParameter('isForBuilding', false);
				View.controllers.get('cashFlowCtrlDetails').costTranSchedDetailsPanel.addParameter('isForProperty', false);
				View.controllers.get('cashFlowCtrlDetails').costTranSchedDetailsPanel.addParameter('isForAccount', true);
			}
			
			View.controllers.get('cashFlowCtrlDetails').costTranSchedDetailsPanel.refresh();
		}
		if (this.isActual){
			var costTranRestriction = this.costTranRecurRestriction + this.ifEmptyOrNull(this.dateRestriction, '');
			costTranRestriction = costTranRestriction.replace(/cost_tran_recur/g,"cost_tran");
			costTranRestriction = costTranRestriction.replace(/date_start/g,"date_trans_created");
			costTranRestriction = costTranRestriction.replace(/date_end/g,"date_due");
			View.controllers.get('cashFlowCtrlDetails').costTranActualDetailsPanel.addParameter('costRestriction', this.ifEmptyOrNull(costTranRestriction, '1=1'));
			View.controllers.get('cashFlowCtrlDetails').costTranActualDetailsPanel.addParameter('buildingRestriction', this.ifEmptyOrNull(this.buildingGeographicalRestriction, ''));
			View.controllers.get('cashFlowCtrlDetails').costTranActualDetailsPanel.addParameter('propertyRestriction', this.ifEmptyOrNull(this.propertyGeographicalRestriction, ''));
			if ("ls" == this.projectionType) {
				View.controllers.get('cashFlowCtrlDetails').costTranActualDetailsPanel.addParameter('isForLease', true);
				View.controllers.get('cashFlowCtrlDetails').costTranActualDetailsPanel.addParameter('isForBuilding', false);
				View.controllers.get('cashFlowCtrlDetails').costTranActualDetailsPanel.addParameter('isForProperty', false);
				View.controllers.get('cashFlowCtrlDetails').costTranActualDetailsPanel.addParameter('isForAccount', false);
			} else if ("bl" == this.projectionType) {
				View.controllers.get('cashFlowCtrlDetails').costTranActualDetailsPanel.addParameter('isForLease', false);
				View.controllers.get('cashFlowCtrlDetails').costTranActualDetailsPanel.addParameter('isForBuilding', true);
				View.controllers.get('cashFlowCtrlDetails').costTranActualDetailsPanel.addParameter('isForProperty', false);
				View.controllers.get('cashFlowCtrlDetails').costTranActualDetailsPanel.addParameter('isForAccount', false);
			} else if ("property" == this.projectionType) {
				View.controllers.get('cashFlowCtrlDetails').costTranActualDetailsPanel.addParameter('isForLease', false);
				View.controllers.get('cashFlowCtrlDetails').costTranActualDetailsPanel.addParameter('isForBuilding', false);
				View.controllers.get('cashFlowCtrlDetails').costTranActualDetailsPanel.addParameter('isForProperty', true);
				View.controllers.get('cashFlowCtrlDetails').costTranActualDetailsPanel.addParameter('isForAccount', false);
			} else if ("ac" == this.projectionType) {
				View.controllers.get('cashFlowCtrlDetails').costTranActualDetailsPanel.addParameter('isForLease', false);
				View.controllers.get('cashFlowCtrlDetails').costTranActualDetailsPanel.addParameter('isForBuilding', false);
				View.controllers.get('cashFlowCtrlDetails').costTranActualDetailsPanel.addParameter('isForProperty', false);
				View.controllers.get('cashFlowCtrlDetails').costTranActualDetailsPanel.addParameter('isForAccount', true);
			}
			
			View.controllers.get('cashFlowCtrlDetails').costTranActualDetailsPanel.refresh();
		}
	},
	
	ifEmptyOrNull: function(value, replacement){
		if (!valueExistsNotEmpty(value)) {
			return replacement;
		}
		return value;
	}
	
});

function calculateDateEnd(calculationPeriod,startDate) {
		// will calculate the last day of the interval selected to drill down
		var year = parseInt(startDate.substring(0,startDate.indexOf('-')));
		var monthStr = startDate.substring(5,7);
		var month = parseInt(monthStr);
		if (month == 0){
			month = parseInt(monthStr.replace('0',''));
		}
		var m = [31,28,31,30,31,30,31,31,30,31,30,31];
        switch(calculationPeriod)
		{
  			case 'MONTH': 
  				if (month != 2) return year+'-'+monthStr+'-'+m[month - 1];
				if (year%4 != 0) return year+'-'+monthStr+'-'+m[1];
				if (year%100 == 0 && year%400 != 0) return year+'-'+monthStr+'-'+m[1];
  				break;
  			case 'QUARTER': 
  				month = month + 2;
  				if (month <= 9){monthStr = '0'+month;}else monthStr = month;
  				if (month != 2) return year+'-'+monthStr+'-'+m[month - 1];
				if (year%4 != 0) return year+'-'+monthStr+'-'+m[1];
				if (year%100 == 0 && year%400 != 0) return year+'-'+monthStr+'-'+m[1];
  				break;
  			case 'YEAR': 
  				return year+'-12-31';;
  				break;
  		}
}
