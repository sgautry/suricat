var energyConsoleChartTwoYearDtlController = View.createController('energyConsoleChartTwoYearDtl',{	

	afterInitialDataFetch: function(){
    	var time_period = View.parameters.time_period;
    	var parts = time_period.split('-');		 
    	var time_period_prev = (parseInt(parts[0])-1) + "-" + parts[1];
    	addParametersAndRefresh(this.energyConsoleChartTwoYearDtl_billReportPrev, time_period_prev);
    	addParametersAndRefresh(this.energyConsoleChartTwoYearDtl_billReport, time_period);
    }
});

function addParametersAndRefresh(report, time_period) {
	var openerController = View.parameters.openerController;
	report.addParameter('conversionFactor', openerController.conversionFactor);
	report.addParameter('billTypeRestriction', openerController.billTypeRestriction);
	report.addParameter('billUnit', openerController.billUnit);
	report.addParameter('blsRestriction', openerController.blsRestriction);
	report.addParameter('blFilterRestriction', getStringConsoleRestriction(openerController.blFilterRestriction));
		
	var valueFieldRestriction = " 1=1 ";
	if (View.parameters && View.parameters.blId) valueFieldRestriction += " AND bl.bl_id LIKE '" + View.parameters.blId + "'";
	report.addParameter('valueFieldRestriction', valueFieldRestriction);
	report.refresh(" bill_archive.time_period  = '" + time_period + "' ");
}

