function energyBlProfile_projForm_onSave(formId) {
	var form = View.panels.get(formId);
	if (form.getFieldValue('project.project_id') != '') {
		form.clearValidationResult();
		if (!validateDates(form))
			return false;
		return form.save();
	} else {
		/* create project using an auto-generated Project Code to populate the project_id.  
		 * must manually validate all form fields prior to generating project_id. */
		form.clearValidationResult();
		if (!validateFormFields(form))
			return false;
		if (!validateDates(form))
			return false;
		
		var record = form.getOutboundRecord();
		var result = Workflow.callMethod(
				'AbCommonResources-ProjectService-createProject', record);
		if (result.code == 'executed') {
			var newRecord = form.getDataSource().processInboundRecord(result.dataSet);
			var project_id = newRecord.getValue("project.project_id");
			var restriction = new Ab.view.Restriction();
			restriction.addClause('project.project_id', project_id);
			form.setRecord(newRecord);
	        form.newRecord = false;
	        form.refresh(restriction);
	        form.save();
		} else {
			View.showMessage('error', result.code + " :: " + result.message);
			return false;
		}
		return true;
	}
}

function validateFormFields(form) {
	var valid = true;
	if (!checkRequiredValue(form, 'project.project_name'))
		valid = false;
	if (!checkRequiredValue(form, 'project.date_start'))
		valid = false;
	if (!checkRequiredValue(form, 'project.date_end'))
		valid = false;
	if (!checkRequiredValue(form, 'project.project_type'))
		valid = false;
	if (!checkRequiredValue(form, 'project.dept_contact'))
		valid = false;
	if (!validateProjectField(form, 'project.project_type',
			View.dataSources.get('energyBlProfileCommon_projectTypeDs'), 'projecttype.project_type'))
		valid = false;
	if (!validateProjectField(form, 'project.proj_mgr',
			View.dataSources.get('energyBlProfileCommon_emDs'), 'em.em_id'))
		valid = false;
	if (!validateProjectField(form, 'project.dept_contact',
			View.dataSources.get('energyBlProfileCommon_emDs'), 'em.em_id'))
		valid = false;
	if (!valid) {
		View.showMessage('message', getMessage('formMissingValues'));
		form.displayValidationResult('');
	}
	return valid;
}

function checkRequiredValue(form, field_name) {
	if (!form.getFieldValue(field_name)) {
		form.addInvalidField(field_name, '');
		return false;
	}
	return true;
}
function validateDates(form) {
	var curDate = new Date();
	var date_start = getDateObject(form.getFieldValue('project.date_start'));
	var date_end = getDateObject(form.getFieldValue('project.date_end'));
	if (date_end < date_start) {
		View.showMessage('message', getMessage('formMissingValues'));
		form.addInvalidField('project.date_end', getMessage('endBeforeStart'));
		form.displayValidationResult('');
		return false;
	}
	if ((curDate - date_start) / (1000 * 60 * 60 * 24) >= 1) {
		if (!confirm(getMessage('dateBeforeCurrent')))
			return false;
	}
	return true;
}

function validateProjectField(form, formField, ds, dsField) {
	var value = form.getFieldValue(formField);
	if (value) {
		var restriction = new Ab.view.Restriction();
		restriction.addClause(dsField, value);
		var records = ds.getRecords(restriction);
		if (records.length == 0) {
			form.addInvalidField(formField, '');
			return false;
		}
	}
	return true;
}

function requestProject(formId) {
	var form = View.panels.get(formId);
	View.confirm(getMessage('confirmRequestProject'), function(button){
        if (button == 'yes') {
        	if (energyBlProfile_projForm_onSave(formId)) {
    			refreshOpener();
    			var project_id = form.getFieldValue('project.project_id');
    			try{
    	    		var parameters = {};
    	    		parameters.fieldValues = toJSON({'project.project_id': project_id, 'project.status': 'Created'});
    	    		var result = Workflow.callMethodWithParameters('AbCapitalBudgeting-CapitalProjectsService-requestProject', parameters);
    	      		if (result.code == 'executed') {
    	    			View.closeThisDialog();
    	    			refreshOpener();
    	      		} 
    	    	} catch (e) {
    	    		Workflow.handleError(e);
    	    		
    	    	}	    	
    		}
        }
        else {
            
        }
    });
}

function getDateObject(ISODate) {
	var tempArray = ISODate.split('-');
	return new Date(tempArray[0], tempArray[1] - 1, tempArray[2]);
}

function refreshOpener() {
	View.parameters.openerPanel.refresh();
}
