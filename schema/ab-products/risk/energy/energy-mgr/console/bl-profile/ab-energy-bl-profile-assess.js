var energyBlProfileAssessController = View.createController('energyBlProfileAssess', {
	blId: '',
	
	afterViewLoad: function(){	
		this.blId = View.parameters.blId;
		
		this.energyBlProfileAssess_assessItems.addParameter('blId', this.blId);
		
		var grid = this.energyBlProfileAssess_assessItems;
		
		grid.afterCreateCellContent = function(row, column, cellElement) {
		    var value = row[column.id];
			if (column.id == 'activity_log.csi_description')	{
				cellElement.style.fontWeight = 'bold';
			}
		}
	},
	
	afterInitialDataFetch: function() {
		var project_id = this.energyBlProfileAssess_assessProj.getFieldValue('project.project_id');
		this.energyBlProfileAssess_assessProj.appendTitle(project_id);
		this.energyBlProfileAssess_assessItems.appendTitle(project_id + ' - ' + this.blId);
	}
});

