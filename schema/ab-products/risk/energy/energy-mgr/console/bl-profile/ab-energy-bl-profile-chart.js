var energyBlProfileChartController = View.createController('energyBlProfileChart', {
	blId: '',
	openerController: null,
	
	afterViewLoad: function() {
		this.openerController = View.getOpenerView().controllers.get('energyBlProfile').openerController;
		this.blId = View.getOpenerView().controllers.get('energyBlProfile').blId;
	},
	
	afterInitialDataFetch: function() {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('bl.bl_id', this.blId);
		this.energyBlProfileChart_blProfile.refresh(restriction);
		
		setChartTitle();
		addParametersAndRefresh(this.energyBlProfileChart_chart, this.blId);
		this.energyBlProfileChart_chart.chartControl.chart.canvas.colors = ["#f4d499", "#e3c263", "#67b7dc", "#4d90d6", "#c7e38c", "#9986c8", "#ffd1d4", "#5ee1dc", "#b0eead", "#fef85a", "#8badd2"];
		this.energyBlProfileChart_chart.chartControl.chart.canvas.graphs[0].balloonFunction = this.adjustBalloonText; 	
		
		View.getOpenerView().controllers.get('energyBlProfile').refreshTabPanels();
	},
	
	energyBlProfileChart_blProfile_afterRefresh: function() {
		if (this.energyBlProfileChart_blProfile.getFieldValue('bl.project_name') != '') {
			this.energyBlProfileChart_blProfile.setTitle(getMessage('greenBuildingScoreTitle'));
		}
		
		if ($('energyBlProfileChart_blProfile_bl.area_gross_ext')) {
			$('energyBlProfileChart_blProfile_bl.area_gross_ext').style.textAlign = 'left';
			$('energyBlProfileChart_blProfile_bl.area_gross_int').style.textAlign = 'left';
			$('energyBlProfileChart_blProfile_bl.count_max_occup').style.textAlign = 'left';
			$('energyBlProfileChart_blProfile_bl.countEm').style.textAlign = 'left';
		}
	},
	
    adjustBalloonText: function(graphDataItem, graph){ 
		 var parts = graphDataItem.category.split('-');		 
		 var timePeriod = (parseInt(parts[0])-1) + "-" + parts[1];			 
		 var value = graphDataItem.values.value;
		 if (value > 1000) {
			 value = parseInt(value/100)/10 + 'k'
		 }
		 value = value.replace('.',strDecimalSeparator);
		 var title = graphDataItem.graph.title;
		 return  "<b>"+title+"</b><br>"+timePeriod+"<br><b>"+value+"</b>";
	}
});

function setChartTitle() {
	var controller = View.controllers.get('energyBlProfileChart');
	var openerController = controller.openerController;
	var metricType = openerController.metricType;
	var billType = openerController.billType;
	var unit = openerController.billUnit;
	var title = '';
	var type = '';
	if (billType == 'AllEnergy') {
		type += getMessage('panelTitle' + billType);
	} else if (billType == 'ELECTRIC') {
		type += getMessage('panelTitleElectric');
	} else if (billType == 'FUEL OIL 2') {
		type += getMessage('panelTitleOil');
	} else if (billType == 'GAS - NATURAL') {
		type += getMessage('panelTitleGas');
	} else if (billType == 'WATER') {
		type += getMessage('panelTitleWater');
	} else {
		type += billType;
	}
	title += type + ' - ' + getMessage('panelTitleMetric' + metricType);
	var timePeriod = openerController.timePeriod;
	timePeriod = (timePeriod == 'Past12Months')?getMessage('panelTitle' + timePeriod):timePeriod;
	title += ' - ' + timePeriod;
	title += ' (' + unit + ')';
	View.panels.get('energyBlProfileChart_chart').appendTitle(title);
}

function addParametersAndRefresh(panel, blId) {
	var controller = View.controllers.get('energyBlProfileChart');
	var openerController = controller.openerController;
	var metricType = openerController.metricType;
	panel.addParameter('conversionFactor', openerController.conversionFactor);
	panel.addParameter('billFilterRestriction', openerController.billFilterRestriction);
	panel.addParameter('billTypeRestriction', openerController.billTypeRestriction);
	panel.addParameter('billUnit',openerController.billUnit);
	
	var valueField = getValueField('bill_archive', metricType);
	var valueFieldPrev = getValueField('bill_archive_prev', metricType);
	panel.addParameter('valueField', valueField);
	panel.addParameter('valueFieldPrev', valueFieldPrev);
	
	var valueFieldRestriction = ' 1=1 ';
	if (metricType == 'UsePers' || metricType == 'UsePersDay' || metricType == 'CostPers') {
		valueFieldRestriction += ' AND (bill_archive.countEm <> 0) ';
	} else if (metricType == 'UseArea' || metricType == 'CostArea') {
		valueFieldRestriction += ' AND bill_archive.area_gross_ext <> 0 ';
	} else if (metricType == 'CostUnit') {
		valueFieldRestriction += ' AND bill_archive.qtyUse > 0 ';
	}
	panel.addParameter('valueFieldRestriction', valueFieldRestriction);
	panel.addParameter('blsRestriction', "bill_archive.bl_id LIKE '" + blId + "'");
	
	panel.refresh();
	panel.show(true);
}

function getValueField(table, metricType) {
	table = table + '.';
	var valueField = 'SUM(' + table + 'qtyUse)';
	switch(metricType) {
    	case 'Use': valueField = 'SUM(' + table + 'qtyUse)'; break;
    	case 'UseArea': valueField = 'CASE WHEN SUM(' + table + 'area_gross_ext)=0 THEN 0 ELSE (SUM(' + table + 'qtyUse) / SUM(' + table + 'area_gross_ext)) END'; break;
    	case 'UsePers': valueField = 'SUM(' + table + 'qtyUse) / SUM(' + table + 'countEm)'; break;
    	case 'UsePersDay': valueField = 'SUM(' + table + 'qtyUse) / (30.0*SUM(' + table + 'countEm))'; break;
    	case 'Cost': valueField = 'SUM(' + table + 'valueCost)'; break;
    	case 'CostArea': valueField = 'CASE WHEN SUM(' + table + 'area_gross_ext)=0 THEN 0 ELSE (SUM(' + table + 'valueCost) / SUM(' + table + 'area_gross_ext)) END'; break;
    	case 'CostPers': valueField = 'SUM(' + table + 'valueCost) / SUM(' + table + 'countEm)'; break;
    	case 'CostUnit': valueField = 'CASE WHEN SUM(' + table + 'qtyUse)=0 THEN NULL ELSE SUM(' + table + 'valueCost)/SUM(' + table + 'qtyUse) END'; break;
    	default: valueField = 'SUM(' + table + 'qtyUse)';
	}
	return valueField;
}

function selectYearMo(obj) {
	var controller = View.controllers.get('energyBlProfileChart');
	var time_period = obj.selectedChartData['bill_archive.time_period'];
	View.getOpenerView().openDialog('ab-energy-console-chart-two-year-dtl.axvw', null, false, {
        width: 900,
        'time_period': time_period,
        'blId': controller.blId,
        'openerController': controller.openerController
    });
}
