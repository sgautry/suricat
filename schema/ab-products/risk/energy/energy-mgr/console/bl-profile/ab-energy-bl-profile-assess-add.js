var energyBlProfileAssessAddController = View.createController('energyBlProfileAssessAdd', {
	
	afterViewLoad: function() {	

	},
	
	afterInitialDataFetch: function() {
		if (this.energyBlProfileAssessAdd_projForm.newRecord) this.energyBlProfileAssessAdd_projForm.setFieldValue('project.project_type', '');
	},
	
	energyBlProfileAssessAdd_projForm_onSave: function() {
		if (energyBlProfile_projForm_onSave('energyBlProfileAssessAdd_projForm')) {
			refreshOpener();
		}
	}
});

