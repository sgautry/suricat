var energyBlProfileBasDataController = View.createController('energyBlProfileBasData', {
	
	afterViewLoad: function(){
		
	},
	
	afterInitialDataFetch: function() {		
		addParametersAndRefresh(this.energyBlProfileBasData_report, this.energyBlProfileBasData_report.restriction);
	}
});

function addParametersAndRefresh(panel, restriction) {
	panel.addParameter('conversionFactor',View.parameters.conversionFactor);
	panel.addParameter('basDataRestriction',View.parameters.basDataRestriction);
	panel.addParameter('basMeterRestriction',View.parameters.basMeterRestriction);
	panel.addParameter('billUnit',View.parameters.billUnit);
	panel.refresh(restriction);
	panel.show(true);
}
