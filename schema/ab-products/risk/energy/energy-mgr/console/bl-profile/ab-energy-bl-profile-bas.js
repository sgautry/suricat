var energyBlProfileBasController = View.createController('energyBlProfileBas', {
	blId: '',
	meterList: '',
	blId: '',
	openerController: null,
	hideLegend: false,
	
	afterViewLoad: function(){	
		this.blId = View.getOpenerView().controllers.get('energyBlProfile').blId;
		this.openerController = View.getOpenerView().controllers.get('energyBlProfile').openerController;
		this.energyBlProfileBas_chart.chartControl.chart.canvas.legendDiv.hidden = this.hideLegend;
		
		this.energyBlProfileBas_meterReport.show();
		this.energyBlProfileBas_meterReport_beforeRefresh();
	},
	
	afterInitialDataFetch: function() {	
		this.energyBlProfileBas_dsCategory.addParameter('eqCategory', getMessage('eqCategory'));
		this.energyBlProfileBas_dsCategory.addParameter('flRmZoneCategory', getMessage('flRmZoneCategory'));
		this.energyBlProfileBas_dsCategory.addParameter('blCategory', getMessage('blCategory'));
		
		this.energyBlProfileBas_meterReport.setCategoryColors({'0': '#6600CC', '1': '#33CC33', '2': '#003399', 'default': '#003399'});// purple: '#6C54A8', green: '#4ABA4A'
		this.energyBlProfileBas_meterReport.setCategoryConfiguration({
    		fieldName: 'bas_data_point.category',
    		getStyleForCategory: this.getStyleForCategory
    	});
		this.energyBlProfileBas_meterReport.categoryCollapsed = false;
		this.energyBlProfileBas_meterReport.update();
	},
	
	getStyleForCategory: function(record) {
    	var style = {};
    	var categorySort = record.getValue('bas_data_point.categorySort');
    	var targetPanel = View.panels.get('energyBlProfileBas_meterReport');
    	style.color = targetPanel.getCategoryColors()[categorySort]; 
    	return style;
    },
	
	energyBlProfileBas_meterReport_beforeRefresh: function() {		
		var restriction = ' bas_data_point.data_point_id NOT IN (0) ';
		restriction += " AND bas_data_point.bl_id LIKE '" + this.blId + "'";
		restriction += " AND " + this.openerController.basBillTypeRestriction;
		addParametersAndRestriction(this.energyBlProfileBas_meterReport, restriction);		
	},	
	
	energyBlProfileBas_meterReport_afterRefresh: function() {
		var hasRowSelected = false;
		/* do not check virtual meters or whole building meters by default */
		this.energyBlProfileBas_meterReport.gridRows.each(function(row) {
		    var meters_to_include = row.getRecord().getValue('bas_data_point.meters_to_include');
		    var category = row.getRecord().getValue('bas_data_point.category');
		    if (meters_to_include == '' && category != getMessage('blCategory')) {
		    	row.select();
		    	hasRowSelected = true;
		    }
		});
		/* if building only has a building meter, then check that one */
		if (!hasRowSelected && this.energyBlProfileBas_meterReport.gridRows.length > 0) this.energyBlProfileBas_meterReport.gridRows.get(0).select();
		
		this.energyBlProfileBas_meterReport_onShowSelected();
		this.setTitleMeterReport();
		
		if (this.energyBlProfileBas_meterReport.gridRows.length == 0) {
			this.energyBlProfileBas_meterReport.actions.get('showSelected').show(false);
		} else {
			this.energyBlProfileBas_meterReport.actions.get('showSelected').show(true);
		}
	},
	
	energyBlProfileBas_meterReport_onShowSelected: function() {			
		this.meterList = '';
		var controller = this;
		this.energyBlProfileBas_meterReport.gridRows.each(function(row) {
		    if (row.isSelected()) {
		    	if (controller.meterList != '') controller.meterList += ",";
		    	controller.meterList += row.getRecord().getValue('bas_data_point.data_point_id');
		    }
		});
		if (this.meterList == '') this.meterList = '0';
		this.energyBlProfileBas_displayChart();
	},
	
	energyBlProfileBas_displayChart: function() {
		var basBlMeterRestriction = " 1=1 " ;
		basBlMeterRestriction += " AND bas_data_point.data_point_id IN (" + this.meterList + ")";

		var restriction = ' bas_data_point.value_common > 0 ';
		addParametersAndRestriction(this.energyBlProfileBas_chart, restriction);	
		this.energyBlProfileBas_chart.addParameter('basBlMeterRestriction', basBlMeterRestriction);
		this.energyBlProfileBas_chart.refresh(restriction);
		this.setTitleMeterChart();
	},
	
    setTitleMeterReport: function() {
		var billType = this.openerController.billType;
    	var billTypeTitle = '';
    	if (billType == 'AllEnergy') {
    		billTypeTitle += getMessage('panelTitle' + billType);
    	} else if (billType == 'ELECTRIC') {
    		billTypeTitle += getMessage('panelTitleElectric');
    	} else if (billType == 'FUEL OIL 2') {
    		billTypeTitle += getMessage('panelTitleOil');
    	} else if (billType == 'GAS - NATURAL') {
    		billTypeTitle += getMessage('panelTitleGas');
    	} else if (billType == 'WATER') {
    		billTypeTitle += getMessage('panelTitleWater');
    	} else {
    		billTypeTitle += billType;
    	}
    	var title = billTypeTitle + ' ' + getMessage('panelTitleUse');
		this.energyBlProfileBas_meterReport.appendTitle(title);
    },
	
	setTitleMeterChart: function() {
		var unit = this.openerController.billUnit;
		var timePeriod = this.openerController.timePeriod;
		timePeriod = (timePeriod == 'Past12Months')?getMessage('panelTitle' + timePeriod):timePeriod;
		var title = '';
		title += timePeriod;
		title += ' (' + unit + ')';
		this.energyBlProfileBas_chart.appendTitle(title);
    },
    
    energyBlProfileBas_meterReport_addMeter: function() {
    	var billTypeId = this.openerController.billType;
    	var restriction = new Ab.view.Restriction();
    	if (billTypeId != 'AllEnergy') restriction.addClause('bas_data_point.bill_type_id', billTypeId);
    	var controller = this;
    	View.getOpenerView().openDialog('ab-energy-bas-edit-point.axvw', restriction, true, {
    		'blId': controller.blId,
    		callback: function() {
    			controller.energyBlProfileBas_meterReport.refresh();
    		}
    	});
    },
    
    energyBlProfileBas_meterReport_addMeterData: function() {
    	var controller = this;
    	View.getOpenerView().openDialog('ab-energy-bas-edit.axvw', null, false, {
    		maximize: true,
    		'blId': controller.blId,
    		'basBillTypeRestriction': controller.openerController.basBillTypeRestriction,
    		callback: function() {
    			controller.energyBlProfileBas_meterReport.refresh();
    		}
    	});
    },
    
    energyBlProfileBas_meterReport_onShowDetails: function(row, action) {
    	var controller = this;
		var record = row.getRecord();
		var data_point_id = record.getValue('bas_data_point.data_point_id');
		var restriction = new Ab.view.Restriction();
		restriction.addClause('bas_data_point.data_point_id', data_point_id);
		View.getOpenerView().openDialog('ab-energy-bas-dtl.axvw', restriction, false, {
			maximize: false,
			callback: function() {
				controller.energyBlProfileBas_meterReport.refresh();
			}
		});
    },
    
    energyBlProfileBas_chart_onShowHideLegend: function() {
    	this.hideLegend = !this.hideLegend;
    	this.energyBlProfileBas_chart.chartControl.chart.canvas.legendDiv.hidden = this.hideLegend;
    	this.energyBlProfileBas_displayChart();
    }
});

function addParametersAndRestriction(panel, restriction) {
	var openerController = View.controllers.get('energyBlProfileBas').openerController;
	panel.addParameter('conversionFactor',openerController.conversionFactor);
	panel.addParameter('basDataRestriction',openerController.basDataRestriction);
	panel.addParameter('basBillTypeRestriction',openerController.basBillTypeRestriction);
	panel.addParameter('billUnit',openerController.billUnit);
	panel.addParameter('blId',View.getOpenerView().controllers.get('energyBlProfile').blId);
	panel.addParameter('billFilterRestriction', openerController.billFilterRestriction);
	panel.addParameter('otherBilled', getMessage('otherBilled'));
	panel.addParameter('eqCategory', getMessage('eqCategory'));
	panel.addParameter('flRmZoneCategory', getMessage('flRmZoneCategory'));
	panel.addParameter('blCategory', getMessage('blCategory'));
	panel.restriction = restriction;
}

function selectDataPointChart(obj) {
	var controller = View.controllers.get('energyBlProfileBas');
	var name = obj.selectedChartData['bas_data_point.name']; 
	var basMeterRestriction = "bas_measurement_scope.bl_id = '" + controller.blId + "'";	
	basMeterRestriction += " AND bas_data_point.name = '" + getValidRestVal(name) + "'";
	
	View.getOpenerView().openDialog('ab-energy-bl-profile-bas-data.axvw', null, false, {
		conversionFactor: controller.openerController.conversionFactor,
		basDataRestriction: controller.openerController.basDataRestriction,
		basMeterRestriction: basMeterRestriction,
		billUnit: controller.openerController.billUnit
	});
}

function getValidRestVal(value) {
	value = value.replace(/\'/g, "\'\'");
	value = value.replace(/&apos;/g, "\'\'");
	return value;
}
