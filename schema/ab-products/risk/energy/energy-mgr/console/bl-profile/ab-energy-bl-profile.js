var energyBlProfileController = View.createController('energyBlProfile', {
	blId: '',
	openerController: null,
	
	afterViewLoad: function() {
		if (View.parameters && View.parameters.openerController) {
			this.openerController = View.parameters.openerController;
		} else this.openerController = View.getOpenerView().getOpenerView().controllers.get('energyConsole');
	
		var grid = this.energyBlProfile_dpReport;		
		grid.afterCreateCellContent = function(row, column, cellElement) {
		    var value = row[column.id];
			if (column.id == 'dp.dp_name')	{
				cellElement.style.fontWeight = 'bold';
			}
		}
		
		var eqstdGrid = this.energyBlProfile_eqstdReport;		
		eqstdGrid.afterCreateCellContent = function(row, column, cellElement) {
		    var value = row[column.id];
			if (column.id == 'eqstd.eq_std' || column.id == 'eqstd.count_eq' || column.id == 'eqstd.description')	{
				cellElement.style.fontWeight = 'bold';
			}
		}
		
		var projGrid = this.energyBlProfile_projReport;		
		projGrid.afterCreateCellContent = function(row, column, cellElement) {
		    var value = row[column.id];
			if (column.id == 'project.project_name')	{
				cellElement.style.fontWeight = 'bold';
			}
		}
		
		var assessGrid = this.energyBlProfile_assessReport;
		assessGrid.afterCreateCellContent = function(row, column, cellElement) {
		    var value = row[column.id];
			if (column.id == 'project.project_name')	{
				cellElement.style.fontWeight = 'bold';
			}
		}
	},
	
	afterInitialDataFetch: function() {
		this.blId = View.parameters.blId;
		var restriction = new Ab.view.Restriction();
		restriction.addClause('bl.bl_id', this.blId);
		this.energyBlProfile_blForm.refresh(restriction);
		showBlPhoto();
		setViewTitle();
		setEnergyEquivValues(this.openerController, this.blId, 'energyBlProfile_equiv', true, true);					
	},
	
	refreshTabPanels: function() {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('bl.bl_id', this.blId);
		this.energyBlProfile_dpReport.refresh(restriction);
		this.energyBlProfile_eqstdReport.addParameter('blId', this.blId);
		this.energyBlProfile_eqstdReport.refresh();
		this.energyBlProfile_assessReport.refresh(restriction);
		this.energyBlProfile_projReport.refresh(restriction);
		
		hideProjectTabsByLicense();
	},
	
	energyBlProfile_eqstdReport_afterRefresh: function() {
		this.energyBlProfile_eqstdReport.gridRows.each(function (row) {
			var record = row.getRecord();
			var photo_icon = row.actions.get('viewPhoto');
			var imageField = record.getValue('eqstd.doc_graphic');
			if (imageField != '') {
				photo_icon.show(true);
			} else photo_icon.show(false);
		});
	},
		
	energyBlProfile_eqReport_afterRefresh: function() {
		this.energyBlProfile_eqReport.gridRows.each(function (row) {
		   var record = row.getRecord(); 		   
		   var photo_icon = row.actions.get('viewPhoto');
		   var imageField = record.getValue('eq.survey_photo_eq');
		   if (imageField != '') {
			   photo_icon.show(true);
		   } else photo_icon.show(false);
		});
	},
	
	energyBlProfile_onSelectPhotoIcon: function(obj, panelId, keyField, graphicField) {
		var photoPanel = View.panels.get(panelId);
		photoPanel.refresh(obj.restriction);
		photoPanel.showInWindow({
		    width: 800,
		    height: 500,
		    closeButton: true
		});
		var imageFile = photoPanel.getFieldValue(graphicField).toLowerCase();
		if (imageFile != '') {
		   	photoPanel.showField('image_field', true);
		   	photoPanel.showImageDoc('image_field', keyField, graphicField);
		} else {
			photoPanel.showField('image_field', false);
			photoPanel.fields.get('image_field').style = '';
		}
		photoPanel.updateWindowScroller();
	},
	
	selectEqstd: function(obj) {
    	var bl_id = this.blId;
    	var eq_std = obj.restriction['eqstd.eq_std'];
    	var restriction = new Ab.view.Restriction();
    	restriction.addClause('eq.bl_id', bl_id);
    	restriction.addClause('eq.eq_std', eq_std);
    	
    	this.energyBlProfile_eqReport.refresh(restriction);
    	this.energyBlProfile_eqReport.showInWindow({ closeButton:true });
    },
    
	energyBlProfile_assessReport_onSelectProj: function(row, action) {
		var record = row.getRecord();
		var requestor = record.getValue('project.requestor');
		var status = record.getValue('project.status');
		var fileName = 'ab-energy-bl-profile-assess.axvw';
		if (requestor == View.user.employee.id && status == 'Created') {
			fileName = 'ab-energy-bl-profile-assess-add.axvw';
		}
		var project_id = record.getValue('project.project_id');
		var restriction = new Ab.view.Restriction();
		restriction.addClause('project.project_id', project_id);
		var controller = this;
		View.openDialog(fileName, restriction, false, {
			maximize: false,
			closeButton: true,
			blId: controller.blId,
			openerPanel: controller.energyBlProfile_assessReport
		});
	},
	
	energyBlProfile_projReport_onSelectProj: function(row, action) {
		var record = row.getRecord();
		var requestor = record.getValue('project.requestor');
		var status = record.getValue('project.status');
		var fileName = 'ab-energy-bl-profile-proj.axvw';
		if (requestor == View.user.employee.id && status == 'Created') {
			fileName = 'ab-energy-bl-profile-proj-add.axvw';
		}
		var project_id = record.getValue('project.project_id');
		var restriction = new Ab.view.Restriction();
		restriction.addClause('project.project_id', project_id);
		var controller = this;
		View.openDialog(fileName, restriction, false, {
			maximize: false,
			closeButton: true,
			blId: controller.blId,
			openerPanel: controller.energyBlProfile_projReport
		});
	},
	
	energyBlProfile_assessReport_onAdd:function() {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('project.bl_id', this.blId);
		var controller = this;
		View.openDialog('ab-energy-bl-profile-assess-add.axvw', restriction, true, {
			maximize: false,
			closeButton: true,
			openerPanel: controller.energyBlProfile_assessReport
		});
	},
	
	energyBlProfile_projReport_onAdd:function() {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('project.bl_id', this.blId);
		var controller = this;
		View.openDialog('ab-energy-bl-profile-proj-add.axvw', restriction, true, {
			maximize: false,
			closeButton: true,
			openerPanel: controller.energyBlProfile_projReport
		});
	}
});

function showBlPhoto(){
    var detailsPanel = View.panels.get('energyBlProfile_blForm');
    var imageFile = detailsPanel.getFieldValue('bl.bldg_photo').toLowerCase();
    
    if (imageFile != '') {
    	detailsPanel.showField('image_field', true);
    	detailsPanel.showImageDoc('image_field', 'bl.bl_id', 'bl.bldg_photo');
	} else {
		detailsPanel.showField('image_field', false);
		detailsPanel.fields.get('image_field').style = '';
		//detailsPanel.fields.get(detailsPanel.fields.indexOfKey('image_field')).dom.src = null;
	}
    detailsPanel.show();
}

function setViewTitle() {
	var detailsPanel = View.panels.get('energyBlProfile_blForm');
	var title = detailsPanel.getFieldValue('bl.bl_id');
	title += ' - ' + detailsPanel.getFieldValue('bl.name');
	View.setTitle(title);
}

function hideProjectTabsByLicense() {
	var tabs = View.panels.get('energyBlProfile_tabs');
	
	AdminService.getProgramLicense({
		callback: function(license) {
			var showAssessTab = false;
			var showProjTab = false;
			var licenseIds = [];
			var licenses = license.licenses;

			for(i in licenses){
				licenseIds.push(licenses[i].id);
				if(licenses[i].enabled && (licenses[i].id == 'AbProjectManagement' || licenses[i].id == 'AbCapitalBudgeting')){
					showProjTab = true;
				} else if(licenses[i].enabled && licenses[i].id == 'AbCapitalPlanningCA' || licenses[i].id == 'AbRiskES'){
					showAssessTab = true;
				}
			}
			tabs.showTab('energyBlProfile_assessTab', showAssessTab);
			tabs.showTab('energyBlProfile_projTab', showProjTab);
		},				
		errorHandler: function(m, e) {
			View.showException(e);
		}
	});	
}
