var energyBlProfileProjAddController = View.createController('energyBlProfileProjAdd', {
	
	afterViewLoad: function(){	

	},
	
	afterInitialDataFetch: function() {
		
	},
	
	energyBlProfileProjAdd_projForm_onSave: function() {
		if (energyBlProfile_projForm_onSave('energyBlProfileProjAdd_projForm')) {
			refreshOpener();
		}
	},
	
	energyBlProfileProjAdd_projForm_onRequest: function() {
		requestProject('energyBlProfileProjAdd_projForm');
	}
});
