var energyBlProfileProjController = View.createController('energyBlProfileProj', {
	blId: '',
	
	afterViewLoad: function(){	
		this.blId = View.parameters.blId;
		
		var grid = this.energyBlProfileProj_projActions;
		
		grid.afterCreateCellContent = function(row, column, cellElement) {
		    var value = row[column.id];
			if (column.id == 'activity_log.work_pkg_id' || column.id == 'activity_log.action_title')	{
				cellElement.style.fontWeight = 'bold';
			}
		}
	},
	
	afterInitialDataFetch: function() {
		var project_id = this.energyBlProfileProj_projForm.getFieldValue('project.project_id');
		this.energyBlProfileProj_projForm.appendTitle(project_id);
		this.energyBlProfileProj_projActions.appendTitle(project_id);
	}
});
