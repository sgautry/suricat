var energyBlProfileDwgController = View.createController('energyBlProfileDwg', {	
	svgControl: null,	
	blId: '',
	flId: '',
	plan_type: 'DEPARTMENTS',
	dwgname: '',
	openerController: null,
	
	afterViewLoad: function(){	
		this.blId = View.getOpenerView().controllers.get('energyBlProfile').blId;
		this.openerController = View.getOpenerView().controllers.get('energyBlProfile').openerController;
	},
	
	afterInitialDataFetch: function() {
		var restriction = new Ab.view.Restriction();
		restriction.addClause('rm.bl_id', this.blId);
		this.energyBlProfileDwg_floor_tree.refresh(restriction);
		
		var treeNode = this.energyBlProfileDwg_floor_tree.treeView.getRoot();
		if (treeNode.hasChildren()){
			var firstNode = treeNode.children[0];
			showFlDrawing(firstNode);
		}		
	},
    
    loadSvg: function(bl_id, fl_id, dwgname) {
    	this.flId = fl_id;
    	var parameters = new Ab.view.ConfigObject();
    	parameters['divId'] = "svgDiv";
    	parameters['pkeyValues'] = {'bl_id':bl_id, 'fl_id':fl_id};  
    	parameters['drawingName'] = dwgname;
    	parameters['plan_type'] = this.plan_type;
    	parameters['showTooltip'] = 'true';
    	parameters['addOnsConfig'] = { 
    			'NavigationToolbar': {divId: "svgDiv"},
    			'AssetTooltip': {handlers: [
    				{assetType: 'rm', datasource: 'energyBlProfileDwg_ds_room_detail', fields: 'rm.rm_id;rm.rm_std;rm.dv_id;rm.dp_id'},
                    {assetType: 'eq', datasource: 'energyBlProfileDwg_ds_eq_detail', fields: 'eq.eq_id;eq.eq_std;eq.use1;eqstd.category'} ]}
		};
    	parameters['events'] = [
            {'eventName': 'click', 'assetType' : 'rm', 'handler' : this.onClickRm}];
    	this.svgControl = new Drawing.DrawingControl("svgDiv", "svg_ctrls", parameters);	
    	this.svgControl.load(parameters);
    },
      
    onClickRm: function(params, drawingController) {
    	var assetIds = params['assetId'];
    	var arrayRoomIDs = assetIds.split(";");
    	var reportView = View.panels.get("energyBlProfileDwg_room_detail_report");
    	var restriction = new Ab.view.Restriction();
    	restriction.addClause('rm.bl_id', arrayRoomIDs[0]);
    	restriction.addClause('rm.fl_id', arrayRoomIDs[1]);
    	restriction.addClause('rm.rm_id', arrayRoomIDs[2]);
    	reportView.refresh(restriction);

    	reportView.showInWindow({title:getMessage('rmDtlTitle'), modal: true,collapsible: false, maximizable: false, width: 350, height: 250, autoScroll:false});
    },
    
    svg_ctrls_onExportPDF: function(){
    	if(this.blId === '' || this.flId === ''){
    		return;
    	}
    	
    	var pdfParameters = {};
    	pdfParameters.documentTemplate = "report-cadplan-imperial-landscape-17x22.docx";
		pdfParameters.drawingName = this.dwgname;
    	pdfParameters.plan_type = this.plan_type;
    	this._doReport(pdfParameters);    	
    },
    
    _doReport: function(pdfParameters){
    	pdfParameters.showLegend = true;
    	pdfParameters.symbolsHandler = "com.archibus.app.solution.common.report.docx.SymbolsHandlerExample";

    	var jobId = Workflow.startJob('AbSystemAdministration-generatePaginatedReport-buildDocxFromView', 'ab-ex-dwg-rpt-pdf.axvw', null, pdfParameters, null);
    	View.openJobProgressBar(getMessage('pleaseWait'), jobId, null, function(status) {
     	  		var url  = status.jobFile.url;
     	   		window.open(url);
     	});
    }
});

/**
 * click event for tree items
 */
function onClickTreeNode(){
	var curTreeNode = View.panels.get("energyBlProfileDwg_floor_tree").lastNodeClicked;	
	showFlDrawing(curTreeNode);
}

function showFlDrawing(curTreeNode) {
	var controller = View.controllers.get('energyBlProfileDwg');
	var bl_id = curTreeNode.data["rm.bl_id"];
	var fl_id = curTreeNode.data["rm.fl_id"];
	controller.dwgname = curTreeNode.data["rm.dwgname"];
	controller.loadSvg(bl_id, fl_id, controller.dwgname);
	View.panels.get('svg_ctrls').appendTitle(bl_id + ' ' + fl_id);
	
	var restriction = new Ab.view.Restriction();
	restriction.addClause('rm.bl_id', bl_id);
	restriction.addClause('rm.fl_id', fl_id);
	controller.energyBlProfileDwg_legendGrid.refresh(restriction);	
}