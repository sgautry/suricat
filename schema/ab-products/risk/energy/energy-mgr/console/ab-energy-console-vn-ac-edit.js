var energyVnAcEditController = View.createController('energyVnAcEdit',{

    afterViewLoad: function(){
    	if (this.abVnAcEdit_vnGrid) {
	    	this.abVnAcEdit_vnGrid.filterValues.push({"fieldName":  "vn.vendor_type", "filterValue": "Energ"});
    	}
    },
    
    afterInitialDataFetch: function(){
	    this.abVnAcEdit_vnGrid_onShowEnergy();    
    },
    
    abVnAcEdit_vnGrid_onShowAll: function(){
    	this.abVnAcEdit_vnGrid.setFilterValue("vn.vendor_type", "");
	    this.abVnAcEdit_vnGrid.refresh();
	    this.abVnAcEdit_vnGrid.actions.get('showAll').show(false);
	    this.abVnAcEdit_vnGrid.actions.get('showEnergy').show(true);
	    this.abVnAcEdit_vnGrid.setTitle(getMessage('allVendorAccounts'));
    },
    
    abVnAcEdit_vnGrid_onShowEnergy: function(){
    	this.abVnAcEdit_vnGrid.setFilterValue("vn.vendor_type", "Energ");
	    this.abVnAcEdit_vnGrid.refresh();
	    this.abVnAcEdit_vnGrid.actions.get('showEnergy').show(false);
	    this.abVnAcEdit_vnGrid.actions.get('showAll').show(true);
	    this.abVnAcEdit_vnGrid.setTitle(getMessage('energyVendorAccounts')); 
    }
});

