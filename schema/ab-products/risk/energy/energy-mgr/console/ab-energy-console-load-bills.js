/**
 * @author AD
 * @created August 30, 2014
 * 
 * KE 2016-10-21: Modified for use as a dialog by Energy Management Console, using generic Bill Connector
 */
 
var energyConsoleLoadBillsController = View.createController('energyConsoleLoadBills', {
	
	connectorExecuteJobId: 'AbSystemAdministration-ConnectorJob-executeConnector',
	billImportJobId: 0,
	billLineImportJobId: 0,
    jobStatusBillImp: null,
    jobStatusBillLineImp: null,
    uploadPath: 'users/public/dt/risk/energy/bills/',
    //uploadPath: 'users/public/dt/',
    //defaultPath: '/#Attribute%//@webAppPath%/projects/hq/datafeeds/risk/energy/bills/',
    defaultPath: '/archibus/projects/hq/datafeeds/risk/energy/bills/',
    defaultFileName:'abrisk-energy-bill-sample.xlsx',
    localFileName: null,
    
    afterInitialDataFetch: function() {
    	this.energyConsoleLoadBills_detailsPanel.setFieldValue('bill_connector.description', getMessage('billConnectorDescription'));
    },

	energyConsoleLoadBills_detailsPanel_onLoadBill: function(){		
        var fileToUpload = $('fileToUpload');
        if (fileToUpload && fileToUpload.files.length==1) {
          this.localFileName = fileToUpload.files[0].name;
          uploadFileToProjectFolder(fileToUpload, this.uploadPath, this.localFileName, true, this.uploadFileCallback, this);              
        }
        else {
          return;  
        }     		
    },
    
	loadBill: function(billConnectorId, billLineConnectorId, autoApprove, fileName){        
        // Execute the bill connector to import bills
		try {
			this.billImportJobId = Workflow.startJob(this.connectorExecuteJobId, billConnectorId);
			
		} catch(e) {
   			Workflow.handleError(e);
            return false;
		}

        var controller = this;
        View.openJobProgressBar(getMessage('BillProgress'), this.billImportJobId, null, function(jobStatus) {
            controller.jobStatusBillImp = jobStatus;
            // If bills imported without error, import the bill lines
            if (jobStatus.jobFinished && jobStatus.jobStatusCode==3 && billLineConnectorId) {  
                controller.loadBillLine(billLineConnectorId, autoApprove, fileName);                    
            }
            else {
                View.showMessage('error', jobStatus.jobStatusMessage + ":  " + jobStatus.jobMessage, null, null,
                        function() {
                            View.openDialog('ab-energy-connector-log.axvw');
                        });                    
                return;
            }
        });                        
    },

    /**
     * 
     */
	loadBillLine: function(billLineConnectorId, autoApprove, fileName){
        // Execute the bill line connnector to import bill lines
        try{
            this.billLineImportJobId = Workflow.startJob(this.connectorExecuteJobId, billLineConnectorId);    
        }catch(e){
            Workflow.handleError(e);
            return false;
        }

        var controller = this;
        View.openJobProgressBar(getMessage('BillLineProgress'), this.billLineImportJobId, null, function(jobStatus) {
            controller.jobStatusBillLineImp = jobStatus;
            // If bills and bill lines import completed without error, call WFRs to autoApprove and calc Energy values
            if (jobStatus.jobFinished && jobStatus.jobStatusCode==3) {  
                controller.approveAndArchive(autoApprove);                    
            }
            else {
                View.showMessage('error', jobStatus.jobStatusMessage + ":  " + jobStatus.jobMessage, null, null,
                	function() {
                    	View.openDialog('ab-energy-connector-log.axvw');
                	});                    
                return;
            }
        });                        
    },
    
    /**
     * approve and archive all imported bills
     */
	approveAndArchive: function(autoApprove){                
        if (valueExistsNotEmpty(autoApprove)) {
            try { 
                var result = Workflow.callMethod('AbRiskEnergyManagement-ProcessBills-processImportedBills', autoApprove);  
                if (result.code=='executed') {
                    View.showMessage(getMessage('BillImportSuccess'));                    
                }
                else {
                    View.showMessage('error', result.message, null, null,
                        function() {
                    		if (View.parameters && View.parameters.callback) View.parameters.callback();
                            View.openDialog('ab-energy-connector-log.axvw');
                        });
                    return;
                        
                }                
            } catch (e){
                Workflow.handleError(e);
                return false;
            }            
        }        
    },
    
    /**
     * Read form values and call loadBill method.
     * uploadPath contains pathname of uploaded file.
     */
    loadSelectedBill: function(uploadPath) {

        var billConnectorForm = this.energyConsoleLoadBills_detailsPanel;
		var billConnectorId = billConnectorForm.getFieldValue("bill_connector.bill_connector_id");
		var billLineConnectorId = billConnectorForm.getFieldValue("bill_connector.bill_line_connector_id");
		var autoApproveVal = billConnectorForm.getFieldValue("bill_connector.auto_approve");
		var autoApprove = Boolean(parseInt(autoApproveVal));
        var fileName = uploadPath;
        this.loadBill(billConnectorId, billLineConnectorId, autoApprove, fileName);                    
    },
    
    /**
     * Callback for upload file job
     * Local file upload was initiated, wait for it to finish
     */
    uploadFileCallback: function(result) {

        var jobId = result.message;
        var controller = this;
        var destPath = "projects/" + this.uploadPath + this.localFileName;
        View.openJobProgressBar(getMessage('FileUploadProgress') + destPath, jobId, null, function(jobStatus) {
            if (jobStatus.jobFinished && jobStatus.jobStatusCode==3) {  // Local file upload was completed
                var uploadPath = "/#Attribute%//@webAppPath%/projects/" + controller.uploadPath + controller.localFileName;
                controller.loadSelectedBill(uploadPath);
            }
            else {
                View.showMessage(jobStatus.jobStatusMessage + ":  " + jobStatus.jobMessage);
                return;
            }
        });                        
    }    
        
});

function openTemplateFile() {
	var fileName = energyConsoleLoadBillsController.defaultPath + energyConsoleLoadBillsController.defaultFileName;
	var excelWindow = window.open(fileName, 'exportWindow');
}


