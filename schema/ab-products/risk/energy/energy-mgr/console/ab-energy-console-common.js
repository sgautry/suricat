function setEnergyEquivValues(openerController, blId, panelId, calcUseArea, showPctDiffUsePrev) {
	var ds = View.dataSources.get('energyConsoleCommon_dsEquiv');
	if (calcUseArea) ds = View.dataSources.get('energyConsoleCommon_dsEquivUseArea');
	
	ds.addParameter('billFilterRestriction', openerController.billFilterRestriction);
	ds.addParameter('conversionFactor', openerController.conversionFactor);
	ds.addParameter('energyCommonEquiv', openerController.energyCommonEquiv);
	ds.addParameter('elecCommonEquiv', openerController.elecCommonEquiv);
	ds.addParameter('gasCommonEquiv', openerController.gasCommonEquiv);
	ds.addParameter('oilCommonEquiv', openerController.oilCommonEquiv);
	ds.addParameter('waterCommonEquiv', openerController.waterCommonEquiv);
	
	var qtyUsePrev1Record = getQtyUsePrev(openerController, blId, openerController.dateRestrictionPrev1);
	var qtyUsePrev1 = qtyUsePrev1Record.getValue('bill_archive.qtyUse');
	var qtyUsePrev2Record = getQtyUsePrev(openerController, blId, openerController.dateRestrictionPrev2);
	var qtyUsePrev2 = qtyUsePrev2Record.getValue('bill_archive.qtyUse');
	
	ds.addParameter('qtyUsePrev1', qtyUsePrev1);
	ds.addParameter('qtyUsePrev2', qtyUsePrev2);
	
	if (calcUseArea) {
		var convFactor = 1;
		if (View.user.displayUnits == 'metric') convFactor = 10.7639;
		ds.addParameter('energyUseAreaCommonEquiv', openerController.energyUseAreaCommonEquivImperial*convFactor);
		ds.addParameter('elecUseAreaCommonEquiv', openerController.elecUseAreaCommonEquivImperial*convFactor);
		ds.addParameter('gasUseAreaCommonEquiv', openerController.gasUseAreaCommonEquivImperial*convFactor);
	}
	
	if (blId) {
		ds.addParameter('blsRestriction', " bl.bl_id = '" + blId + "' ");
	} else {
		ds.addParameter('blsRestriction', openerController.blsRestriction);
		ds.addParameter('blFilterRestriction', getStringConsoleRestriction(openerController.blFilterRestriction));
	}
	var record = ds.getRecord();
		
	var title = getMessage('annualUse');
	if (blId) { 
		title += ' - ';
		title += (openerController.timePeriod == 'Past12Months')?getMessage('panelTitlePast12Months'):openerController.timePeriod;
	}
	title += ' (' + openerController.billUnit + ')';
	$(panelId + '_annualUseSpan').innerHTML = title;
	
	if (calcUseArea) $(panelId + '_annualUseAreaSpan').innerHTML = getMessage('annualUseArea') + ' (' + openerController.billUnit + '/' + getMessage(View.user.displayUnits) + ')';
	
	var energyUse = record.getValue('bl.energyUse');
	if (energyUse == 0) {
		$(panelId + '_annualUseValue').innerHTML = record.getLocalizedValue('bl.volumeUse');
		if (calcUseArea) $(panelId + '_annualUseAreaValue').innerHTML = record.getLocalizedValue('bl.volumeUseArea');
	} else {
		$(panelId + '_annualUseValue').innerHTML = record.getLocalizedValue('bl.energyUse');
		if (calcUseArea) $(panelId + '_annualUseAreaValue').innerHTML = record.getLocalizedValue('bl.energyUseArea');
	}
	
	$(panelId + '_equivUseIcon').style.visibility = 'visible';
	if (calcUseArea) $(panelId + '_equivUseAreaIcon').style.visibility = 'visible';
	
	if (openerController.billType == 'AllEnergy' || openerController.billType == 'ELECTRIC' || openerController.billType == 'GAS - NATURAL' || openerController.billType == 'FUEL OIL 2' || openerController.billType == 'WATER') {
		$(panelId + '_equivUseSpan').innerHTML = getMessage('equivUse' + openerController.billType.substring(0, 3));
		$(panelId + '_equivUseValue').innerHTML = record.getLocalizedValue('bl.equivUse' + openerController.billType.substring(0, 3));
	} else {
		hideEquivUseFields(panelId);
	}
	
	if (calcUseArea) {
		if (openerController.billType == 'AllEnergy' || openerController.billType == 'ELECTRIC' || openerController.billType == 'GAS - NATURAL') {
			var equivUseArea = record.getValue('bl.equivUseArea' + openerController.billType.substring(0, 3));
			var displayValue = '';
			if (equivUseArea == 0) {
				$(panelId + '_equivUseAreaIcon').style.visibility = 'hidden';
			} else if (equivUseArea > 0) {
				displayValue += '+';
				$(panelId + '_equivUseAreaIcon').src = '/archibus/schema/ab-core/graphics/icons/view/arrow-red-up.png';
			} else {
				$(panelId + '_equivUseAreaIcon').src = '/archibus/schema/ab-core/graphics/icons/view/arrow-green-down.png';
			}
			displayValue += record.getLocalizedValue('bl.equivUseArea' + openerController.billType.substring(0, 3)) + '%';
			$(panelId + '_equivUseAreaValue').innerHTML = displayValue;
		} else hideEquivUseAreaFields(panelId);
	}
	
	if (showPctDiffUsePrev) {
		displayPctUsePrevValues(openerController, panelId, record, 1);
		//displayPctUsePrevValues(openerController, panelId, record, 2);
	}
}

function hideEquivUseFields(panelId) {
	$(panelId + '_equivUseSpan').innerHTML = '';
	$(panelId + '_equivUseValue').innerHTML = '';
	$(panelId + '_equivUseIcon').style.visibility = 'hidden';
}

function hideEquivUseAreaFields(panelId) {
	$(panelId + '_equivUseAreaSpan').innerHTML = '';
	$(panelId + '_equivUseAreaValue').innerHTML = '';
	$(panelId + '_equivUseAreaIcon').style.visibility = 'hidden';
}

function hidePctDiffUsePrevFields(panelId, yearsAgo) {
	$(panelId + '_pctDiffUsePrev' + yearsAgo + 'Span').innerHTML = '';
	$(panelId + '_pctDiffUsePrev' + yearsAgo + 'Value').innerHTML = '';
	$(panelId + '_pctDiffUsePrev' + yearsAgo + 'Icon').style.visibility = 'hidden';
}

function displayPctUsePrevValues(openerController, panelId, record, yearsAgo) {
	var pctDiffUsePrev = record.getValue('bl.pctDiffUsePrev' + yearsAgo);
	if (pctDiffUsePrev == '') {
		hidePctDiffUsePrevFields(panelId, yearsAgo);
	} else {
		var title = getPctUsePrevTitle(openerController, yearsAgo);
		$(panelId + '_pctDiffUsePrev' + yearsAgo + 'Span').innerHTML = title;
		var displayValue = '';
		if (pctDiffUsePrev > 0) {
			displayValue += '+';			
			$(panelId + '_pctDiffUsePrev' + yearsAgo + 'Icon').src = '/archibus/schema/ab-core/graphics/icons/view/arrow-red-up.png';
		} else {
			$(panelId + '_pctDiffUsePrev' + yearsAgo + 'Icon').src = '/archibus/schema/ab-core/graphics/icons/view/arrow-green-down.png';
		}
		displayValue += record.getLocalizedValue('bl.pctDiffUsePrev' + yearsAgo) + '%';
		$(panelId + '_pctDiffUsePrev' + yearsAgo + 'Value').innerHTML = displayValue;
		if (pctDiffUsePrev == 0) {
			$(panelId + '_pctDiffUsePrev' + yearsAgo + 'Icon').style.visibility = 'hidden';
		} else {
			$(panelId + '_pctDiffUsePrev' + yearsAgo + 'Icon').style.visibility = 'visible';
		}
	}
}

function getPctUsePrevTitle(openerController, yearsAgo) {
	var timePeriod = openerController.timePeriod;
	if (timePeriod == 'Past12Months') {
		title = getMessage('pctDiffUsePrev' + yearsAgo + 'Title');
	} else {
		var year = parseInt(openerController.timePeriod) - yearsAgo;
		title = String.format(getMessage('pctDiffUsePrevYrTitle'), year);
	}
	return title;
}

function getStringConsoleRestriction(restriction) {
	var strRestriction = ' 1=1 ';
	if (restriction != null) {
		for (var i = 0; i < restriction.clauses.length; i++) {
			var clause = restriction.clauses[i];
			var value = clause.value;
			var name = clause.name;
			var op = clause.op;
			if(clause.op == 'IN') {
				strRestriction += ' AND ' + name + " IN (" + getMultipleValues(value) + ") ";
			} else {
				strRestriction += ' AND ' + name + " = '" + getValidValue(value) + "' ";
			}
		}
	}
	return strRestriction;
}

function getQtyUsePrev(openerController, blId, dateRestriction) {
	var ds = View.dataSources.get('energyConsoleCommon_dsQtyUse');
	ds.addParameter('conversionFactor', openerController.conversionFactor);
	ds.addParameter('billTypeRestriction', openerController.billTypeRestriction);
	ds.addParameter('dateRestriction', dateRestriction);
	if (blId) {
		ds.addParameter('blsRestriction', " bl.bl_id = '" + blId + "' ");
	} else {
		ds.addParameter('blsRestriction', openerController.blsRestriction);
		ds.addParameter('blFilterRestriction', getStringConsoleRestriction(openerController.blFilterRestriction));
	}
	var record = ds.getRecord();
	return record;
}

function isDemoMode() {
	var isDemoMode = false;
	var ds = View.dataSources.get('energyConsoleCommon_dsDemoMode');
	var restriction = new Ab.view.Restriction();
	restriction.addClause('bl.bl_id', ['SRL','HQ','JFK A'], 'IN');
	var records = ds.getRecords(restriction);
	if (records.length == 3) isDemoMode = true;
	return isDemoMode;
}

function getMultipleValues(values) {
	var valueList = "";
	for (var i=0; i < values.length; i++) {
		if (valueList != '') valueList += ",";
		var value = values[i].trim();
		valueList += "'" + getValidValue(value) + "'";
	}
	return valueList;
}

function getValidValue(fieldValue)
{
	fieldValue = fieldValue.replace(/\'/g, "\'\'");
	return fieldValue;
}

