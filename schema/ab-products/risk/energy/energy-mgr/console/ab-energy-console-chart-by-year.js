var energyConsoleChartByYearController = View.createController('energyConsoleChartByYear',{
	openerController: null,
	
	afterViewLoad: function() {
		this.openerController = View.getOpenerView().controllers.get('energyConsole');
	},
	
    afterInitialDataFetch: function(){
		this.energyConsoleChartByYear_displayChart('Cost');
    },    
    
    energyConsoleChartByYear_displayChart: function(metricType) {
    	View.openProgressBar();
    	View.updateProgressBar(1/4);
    	this.addEnergyChartParametersAndRefresh(metricType);
    	View.updateProgressBar(3/4);
    	this.refreshChartTitle(metricType);
    	View.closeProgressBar();
    },
    
    addEnergyChartParametersAndRefresh: function(metricType) {
    	var chart = this.energyConsoleChartByYear_chart;
    	chart.addParameter('conversionFactor', this.openerController.conversionFactor);
		chart.addParameter('billTypeRestriction', this.openerController.billTypeRestriction);
		chart.addParameter('blsRestriction', this.openerController.blsRestriction);
		chart.addParameter('blFilterRestriction', getStringConsoleRestriction(this.openerController.blFilterRestriction));
		
    	var valueField = 'qtyUse';
    	switch(metricType) {
	    	case 'Use': valueField = 'qtyUse'; break;
	    	case 'Cost': valueField = 'valueCost'; break;
	    	default: valueField = 'qtyUse';
		}
		chart.addParameter('valueField', valueField);
		
		var valueFieldRestriction = ' 1=1 ';
		if (View.parameters && View.parameters.blId) valueFieldRestriction = "bl.bl_id LIKE '" + View.parameters.blId + "'";
		chart.addParameter('valueFieldRestriction', valueFieldRestriction);
		chart.refresh(null);
		chart.show();
    },
    
    refreshChartTitle: function(metricType) {
    	var billType = this.openerController.billType;
    	var unit = this.openerController.billUnit;
    	var title = billType;
    	if (billType == 'AllEnergy') title = getMessage('panelTitle' + billType);
    	title += ' ' + getMessage('panelTitleMetric' + metricType);
    	if (metricType.indexOf('Use') == 0 || metricType.indexOf('CostUnit') == 0) title += ' (' + unit + ')'; 
    	
    	this.energyConsoleChartByYear_chart.setTitle(title);
    },
    
    selectHighlight: function(metricType) {
    	this.energyConsoleChartByYear_displayChart(metricType);	
    }
});

function selectYear(obj) {
	var bill_year = obj.selectedChartData['bill_archive.bill_year'];
	var openerController = View.controllers.get('energyConsoleChartByYear').openerController;
	var report = View.panels.get('energyConsoleChartByYear_billReport');
	report.addParameter('conversionFactor', openerController.conversionFactor);
	report.addParameter('billTypeRestriction', openerController.billTypeRestriction);
	report.addParameter('billUnit', openerController.billUnit);
	report.addParameter('blsRestriction', openerController.blsRestriction);
	report.addParameter('blFilterRestriction', getStringConsoleRestriction(openerController.blFilterRestriction));
	report.addParameter('bill_year', bill_year);
	
	var valueFieldRestriction = " 1=1 ";
	if (View.parameters && View.parameters.blId) valueFieldRestriction = " bl.bl_id LIKE '" + View.parameters.blId + "'";
	report.addParameter('valueFieldRestriction', valueFieldRestriction);
	report.refresh(null);
	report.showInWindow({closeButton:true});
}
