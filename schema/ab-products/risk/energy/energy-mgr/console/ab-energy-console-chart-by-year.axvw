<view version="2.0" showLoadProgress="true">    
    <message name="panelTitleAllEnergy">Energy</message>
    <message name="panelTitleElectric">Electricity</message>
    <message name="panelTitleGas">Natural Gas</message>
    <message name="panelTitleOil">Heating Oil</message>
    <message name="panelTitleWater">Water</message>
    <message name="panelTitleMetricUse">Use by Year</message>
    <message name="panelTitleMetricCost">Costs by Year</message>
    <message name="panelTitlePast12Months">Past 12 Months</message>
    <js file="ab-energy-console-chart-by-year.js"/>      
    
    <dataSource id="energyConsoleChartByYear_ds_groupingAxis">
        <table name="bill_archive"/>
        <sql dialect="generic">
                SELECT SUBSTRING(bill_archive.time_period,1,4) ${sql.as} bill_year               
                FROM bill_archive
                LEFT OUTER JOIN bl ON bl.bl_id = bill_archive.bl_id
                WHERE ${parameters['billTypeRestriction']}  AND ${parameters['blsRestriction']} AND ${parameters['blFilterRestriction']}
                GROUP BY SUBSTRING(bill_archive.time_period,1,4)
        </sql>
        <sql dialect="oracle">
                SELECT SUBSTR(bill_archive.time_period,1,4) ${sql.as} bill_year                   
                FROM bill_archive
                LEFT OUTER JOIN bl ON bl.bl_id = bill_archive.bl_id
                WHERE ${parameters['billTypeRestriction']}  AND ${parameters['blsRestriction']} AND ${parameters['blFilterRestriction']}
                GROUP BY SUBSTR(bill_archive.time_period,1,4)
        </sql>
        <field name="bill_year" table="bill_archive" dataType="text"/>
        <sortField name="bill_year" table="bill_archive" ascending="true"/>
        <parameter name="billTypeRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="blsRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="blFilterRestriction" dataType="verbatim" value="1=1"/>
    </dataSource>
    
    <dataSource id="energyConsoleChartByYear_ds_dataAxis">
        <table name="bl" role="main"/>
        <sql dialect="generic">
            SELECT bill_year, bl.bl_id, bl.name, bl.use1, bl.construction_type, bl.ctry_id, bl.state_id, bl.site_id, bl.area_gross_ext,
                (${parameters['valueField']}) ${sql.as} valueField
            FROM
                (SELECT bill_archive.bl_id, SUBSTRING(bill_archive.time_period,1,4) ${sql.as} bill_year,
                    (CASE WHEN (SUM(qty_energy) IS NULL OR SUM(qty_energy) = 0) 
                        THEN (CASE WHEN SUM(qty_volume) IS NULL THEN 0 ELSE SUM(qty_volume) END) 
                        ELSE SUM(qty_energy) END) / ${parameters['conversionFactor']} ${sql.as} qtyUse,                                      
                    SUM(bill_archive.amount_expense) ${sql.as} valueCost,
                    (SELECT CASE WHEN COUNT(em.em_id) IS NULL THEN 0 ELSE COUNT(em.em_id) END FROM em WHERE em.bl_id = bill_archive.bl_id) ${sql.as} countEm                   
                FROM bill_archive
                WHERE ${parameters['billTypeRestriction']} AND ${parameters['blsRestriction']} 
                GROUP BY bill_archive.bl_id, SUBSTRING(bill_archive.time_period,1,4)) ${sql.as} bill_archive
            LEFT OUTER JOIN bl ON bl.bl_id = bill_archive.bl_id
            WHERE ${parameters['valueFieldRestriction']} AND ${parameters['blFilterRestriction']}
        </sql>
        <sql dialect="oracle">
            SELECT bill_year, bl.bl_id, bl.name, bl.use1, bl.construction_type, bl.ctry_id, bl.state_id, bl.site_id, bl.area_gross_ext,
                (${parameters['valueField']}) ${sql.as} valueField
            FROM
                (SELECT bill_archive.bl_id, SUBSTR(bill_archive.time_period,1,4) ${sql.as} bill_year,
                    (CASE WHEN (SUM(qty_energy) IS NULL OR SUM(qty_energy) = 0) 
                        THEN (CASE WHEN SUM(qty_volume) IS NULL THEN 0 ELSE SUM(qty_volume) END) 
                        ELSE SUM(qty_energy) END) / ${parameters['conversionFactor']} ${sql.as} qtyUse,                                      
                    SUM(bill_archive.amount_expense) ${sql.as} valueCost,
                    (SELECT CASE WHEN COUNT(em.em_id) IS NULL THEN 0 ELSE COUNT(em.em_id) END FROM em WHERE em.bl_id = bill_archive.bl_id) ${sql.as} countEm                   
                FROM bill_archive
                WHERE ${parameters['billTypeRestriction']} AND ${parameters['blsRestriction']} 
                GROUP BY bill_archive.bl_id, SUBSTR(bill_archive.time_period,1,4)) ${sql.as} bill_archive
            LEFT OUTER JOIN bl ON bl.bl_id = bill_archive.bl_id
            WHERE ${parameters['valueFieldRestriction']} AND ${parameters['blFilterRestriction']}       
        </sql>
        <field name="bl_id" table="bl" />
        <field name="valueField" table="bl" dataType="number" decimals="2"/>
        <sortField name="bl_id" table="bl" ascending="true"/>
        <parameter name="summaryValueForThisGroup" dataType="text" value=""/> 
        <restriction type="sql" sql="bill_year = ${parameters['summaryValueForThisGroup']}"/>
        <parameter name="conversionFactor" dataType="number" value="1.000000"/>
        <parameter name="billTypeRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="blsRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="blFilterRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="valueFieldRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="valueField" dataType="verbatim" value="qtyUse"/>
    </dataSource>
    
    <dataSource id="energyConsoleChartByYear_ds0">
        <table name="bill_archive" role="main"/>
        <sql dialect="generic">
            SELECT bill_archive.time_period, bill_archive.bill_id, bill_archive.bill_type_id,
            bill_archive.amount_expense, (CASE WHEN (qty_energy IS NULL OR qty_energy = 0) 
                    THEN (CASE WHEN qty_volume IS NULL THEN 0 ELSE qty_volume END) 
                    ELSE qty_energy END) / ${parameters['conversionFactor']} ${sql.as} qty_field,
            SUBSTRING(bill_archive.time_period,1,4) ${sql.as} bill_year,
            bill_archive.vn_id, bill_archive.vn_ac_id, bill_archive.bl_id, bill_archive.status,
            bill_archive.doc
            FROM bill_archive
            LEFT OUTER JOIN bl ON bill_archive.bl_id = bl.bl_id
            WHERE ${parameters['billTypeRestriction']}
            AND ${parameters['valueFieldRestriction']}
            AND ${parameters['blsRestriction']} 
            AND ${parameters['blFilterRestriction']}
            AND SUBSTRING(bill_archive.time_period,1,4) = ${parameters['bill_year']}
        </sql>
        <sql dialect="oracle">
            SELECT bill_archive.time_period, bill_archive.bill_id, bill_archive.bill_type_id,
            bill_archive.amount_expense, (CASE WHEN (qty_energy IS NULL OR qty_energy = 0) 
                    THEN (CASE WHEN qty_volume IS NULL THEN 0 ELSE qty_volume END) 
                    ELSE qty_energy END) / ${parameters['conversionFactor']} ${sql.as} qty_field,
            SUBSTR(bill_archive.time_period,1,4) ${sql.as} bill_year,
            bill_archive.vn_id, bill_archive.vn_ac_id, bill_archive.bl_id, bill_archive.status
            FROM bill_archive
            LEFT OUTER JOIN bl ON bill_archive.bl_id = bl.bl_id
            WHERE ${parameters['billTypeRestriction']}
            AND ${parameters['valueFieldRestriction']}
            AND ${parameters['blsRestriction']} 
            AND ${parameters['blFilterRestriction']}
            AND SUBSTR(bill_archive.time_period,1,4) = ${parameters['bill_year']}
        </sql>
        <field table="bill_archive" name="time_period"/>
        <field table="bill_archive" name="bill_id"/>
        <field table="bill_archive" name="bill_type_id"/>
        <field name="amount_expense"  table="bill_archive" decimals="2" showTotals="true"/>
        <field name="qty_field" table="bill_archive" dataType="number" decimals="2" showTotals="true">
            <title>Use (${panel.parameters.billUnit})</title>
        </field>
        <field table="bill_archive" name="bill_year" dataType="text"/>
        <field table="bill_archive" name="vn_id"/>
        <field table="bill_archive" name="vn_ac_id"/>
        <field table="bill_archive" name="bl_id"/>
        <field table="bill_archive" name="status"/>
        <field table="bill_archive" name="doc"/>
        <parameter name="billTypeRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="blsRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="blFilterRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="valueFieldRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="conversionFactor" dataType="number" value="1.000000"/>
        <parameter name="bill_year" dataType="text" value="2016"/>
    </dataSource>
    
    <panel id="energyConsoleChartByYear_chart" 
        type="htmlChart" 
        controlType="stackedColumnChart" 
        dataSource="energyConsoleChartByYear_ds_dataAxis"
        showDataTips="true" 
        showLegendOnLoad="true"
        showLegendAsPopUp="false" 
        legendLocation="bottom"
        showExportButton="true"
        backgroundColor="0xf3f7fa"
        showOnLoad="true">  
        <title></title>
        <action id="energyConsoleChartByYear_displayMenu" type="menu" >
            <title>Show</title>
            <action id="energyConsoleChartByYear_displayMenuUse">
                <title>Use</title>
                <command type="callFunction" functionName="energyConsoleChartByYearController.selectHighlight('Use')"/>
            </action>
            <action id="energyConsoleChartByYear_displayMenuCost">
                <title>Cost</title>
                <command type="callFunction" functionName="energyConsoleChartByYearController.selectHighlight('Cost')"/>
            </action>
        </action>
        <event type="onClickItem">
            <command type="callFunction" functionName="selectYear" />           
        </event>
        <sortField name="bl_id" table="bl" ascending="false"/>
        <groupingAxis table="bill_archive" field="bill_year" dataSource="energyConsoleChartByYear_ds_groupingAxis">
            <title>Year</title> 
        </groupingAxis> 
        <secondaryGroupingAxis table="bl" field="bl_id" >
            <title>Building</title> 
        </secondaryGroupingAxis>
        <dataAxis table="bl" field="valueField" showLabel="false">
            <title></title>
        </dataAxis>
    </panel>
    
    <panel type="grid" id="energyConsoleChartByYear_billReport" dataSource="energyConsoleChartByYear_ds0" showOnLoad="false" hidden="true">      
        <sortField name="bl_id" table="bill_archive" ascending="true"/>
        <sortField name="time_period" table="bill_archive" ascending="true"/>
        <sortField name="bill_type_id" table="bill_archive" ascending="true"/>
        <title>Approved Bills</title>
        <action type="menu" id="energyConsoleChartByYear_billReport_toolsMenu" imageName="/schema/ab-core/graphics/icons/view/export.png">
            <tooltip>Export</tooltip>
            <action id="exportDOCX">
                <title>Export to DOCX</title>
                <command type="exportPanel" outputType="docx" orientation="landscape" panelId="energyConsoleChartByYear_billReport"/>
            </action>
            <action id="exportXLS">
                <title>Export to XLS</title>
                <command type="exportPanel" outputType="xls" panelId="energyConsoleChartByYear_billReport"/>
            </action>
            <action id="exportPDF">
                <title>Export to PDF</title>
                <command type="exportPanel" outputType="pdf" orientation="landscape" panelId="energyConsoleChartByYear_billReport"/>
            </action>
        </action>
        <field table="bill_archive" name="time_period" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartByYear_billLines"/>
        </field>
        <field table="bill_archive" name="bill_id" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartByYear_billLines"/>
        </field>
        <field table="bill_archive" name="bill_type_id" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartByYear_billLines"/>
        </field>
        <field name="amount_expense"  table="bill_archive" decimals="2" showTotals="true" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartByYear_billLines"/>
        </field>
        <field name="qty_field" table="bill_archive" dataType="number" decimals="2" showTotals="true" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartByYear_billLines"/>
            <title>Use (${panel.parameters.billUnit})</title> 
        </field>            
        <field table="bill_archive" name="vn_id" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartByYear_billLines"/>
        </field>
        <field table="bill_archive" name="vn_ac_id" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartByYear_billLines"/>
        </field>
        <field table="bill_archive" name="bl_id" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartByYear_billLines"/>
        </field>
        <field table="bill_archive" name="doc" controlType="link"/>
        <field table="bill_archive" name="status" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartByYear_billLines"/>
        </field>        
    </panel>
    
    <dataSource id="energyConsoleChartByYear_ds1">
        <table name="bill_line_archive" role="main"/>
        <table name="vn_rate" role="standard"/>
        <field name="vn_id" table="bill_line_archive"/>
        <field name="bill_id" table="bill_line_archive"/>
        <field name="vn_rate_id" table="bill_line_archive"/>
        <field name="vn_rate_desc" table="vn_rate"/>
        <field name="qty" table="bill_line_archive"/>
        <field name="bill_unit_id" table="bill_line_archive"/>
        <field name="amount_expense" table="bill_line_archive" showTotals="true"/>
        <field name="amount_income" table="bill_line_archive" showTotals="true"/>
        <field name="bill_line_id" table="bill_line_archive"/>  
        <field name="description" table="bill_line_archive"/>
        <field name="vn_meter_id" table="bill_line_archive"/>   
    </dataSource>
    
    <panel type="grid" id="energyConsoleChartByYear_billLines" dataSource="energyConsoleChartByYear_ds1" showOnLoad="false" hidden="true">
        <title>Bill Lines - ${panel.restriction['bill_archive.vn_id'] + ' - ' + panel.restriction['bill_archive.bill_id']}</title>
        <sortField name="bill_line_id" ascending="true"/>
        <action type="menu" id="energyConsoleChartByYear_billLines_toolsMenu" imageName="/schema/ab-core/graphics/icons/view/export.png">
            <tooltip>Export</tooltip>
            <action id="exportDOCX">
                <title>Export to DOCX</title>
                <command type="exportPanel" outputType="docx" orientation="landscape" panelId="energyConsoleChartByYear_billLines"/>
            </action>
            <action id="exportXLS">
                <title>Export to XLS</title>
                <command type="exportPanel" outputType="xls" panelId="energyConsoleChartByYear_billLines"/>
            </action>
            <action id="exportPDF">
                <title>Export to PDF</title>
                <command type="exportPanel" outputType="pdf" orientation="landscape" panelId="energyConsoleChartByYear_billLines"/>
            </action>
        </action>
        <field name="bill_line_id" table="bill_line_archive"/> 
        <field name="vn_rate_desc" table="vn_rate"/>
        <field name="amount_expense" table="bill_line_archive" showTotals="true"/>
        <field name="amount_income" table="bill_line_archive" showTotals="true"/>              
        <field name="qty" table="bill_line_archive"/>
        <field name="bill_unit_id" table="bill_line_archive"/>
        <field name="vn_id" table="bill_line_archive"/>
        <field name="bill_id" table="bill_line_archive"/> 
        <field name="vn_meter_id" table="bill_line_archive"/>
        <field name="description" table="bill_line_archive"/>  
    </panel>  
    
    <panel type="view" id="energyConsoleChartByYear_commonView" file="ab-energy-console-common.axvw" hidden="true"/>
    
</view>
