var energyConsoleChartTwoYearController = View.createController('energyConsoleChartTwoYear',{
	openerController: null,
	hideLegend: true,
	
	afterViewLoad: function() {
		if (View.parameters && View.parameters.maximize) {
			this.openerController = View.parameters.openerController;
			this.hideLegend = false;
		} else {
			this.openerController = View.controllers.get('energyConsole');
		}
	},
	
    afterInitialDataFetch: function(){
    	this.energyConsoleChartTwoYear_displayChart();
		this.energyConsoleChartTwoYear_chart.chartControl.chart.canvas.legendDiv.hidden = this.hideLegend;
		this.energyConsoleChartTwoYear_chart.chartControl.chart.canvas.graphs[0].balloonFunction = this.adjustBalloonText;		
    },    
    
    energyConsoleChartTwoYear_displayChart: function() {    	
    	var metricType = this.openerController.metricType;
    	this.addEnergyChartParametersAndRefresh(metricType);
    	this.refreshChartTitle(metricType);
		this.energyConsoleChartTwoYear_chart.chartControl.chart.canvas.colors = ["#f4d499", "#e3c263", "#67b7dc", "#4d90d6", "#c7e38c", "#9986c8", "#ffd1d4", "#5ee1dc", "#b0eead", "#fef85a", "#8badd2"];
    },    
    
    addEnergyChartParametersAndRefresh: function(metricType) {
    	var chart = this.energyConsoleChartTwoYear_chart;
    	chart.addParameter('conversionFactor', this.openerController.conversionFactor);
		chart.addParameter('billFilterRestriction', this.openerController.billFilterRestriction);
		chart.addParameter('billTypeRestriction', this.openerController.billTypeRestriction);
		chart.addParameter('blsRestriction', this.openerController.blsRestriction);
		
    	var valueField = getValueField('bill_archive', metricType);
		var valueFieldPrev = getValueField('bill_archive_prev', metricType);
		chart.addParameter('valueField', valueField);
		chart.addParameter('valueFieldPrev', valueFieldPrev);
		
		var valueFieldRestriction = ' 1=1 ';
		if (metricType == 'UsePers' || metricType == 'UsePersDay' || metricType == 'CostPers') {
			valueFieldRestriction += ' AND (bill_archive.countEm <> 0) ';
		} else if (metricType == 'UseArea' || metricType == 'CostArea') {
			valueFieldRestriction += ' AND bill_archive.area_gross_ext <> 0 ';
		} else if (metricType == 'CostUnit') {
			valueFieldRestriction += ' AND bill_archive.qtyUse > 0 ';
		}
		chart.addParameter('valueFieldRestriction', valueFieldRestriction);
		chart.addParameter('blFilterRestriction', getStringConsoleRestriction(this.openerController.blFilterRestriction));
		chart.refresh();
		this.energyConsoleChartTwoYear_chart.chartControl.chart.canvas.graphs[0].balloonFunction = this.adjustBalloonText;
		chart.show();
    },
    
    refreshChartTitle: function(metricType) {
    	var billType = this.openerController.billType;
    	var unit = this.openerController.billUnit;
    	var timePeriod = this.openerController.timePeriod;
    	timePeriod = (timePeriod == 'Past12Months')?getMessage('panelTitle' + timePeriod):timePeriod;
    	var title = getMessage('panelTitleYearOverYear') + ' - ';
    	var type = '';
    	if (billType == 'AllEnergy') {
    		type += getMessage('panelTitle' + billType);
    	} else if (billType == 'ELECTRIC') {
    		type += getMessage('panelTitleElectric');
    	} else if (billType == 'FUEL OIL 2') {
    		type += getMessage('panelTitleOil');
    	} else if (billType == 'GAS - NATURAL') {
    		type += getMessage('panelTitleGas');
    	} else if (billType == 'WATER') {
    		type += getMessage('panelTitleWater');
    	} else {
    		type += billType;
    	}
    	title += type + ' - ' + getMessage('panelTitleMetric' + metricType);
    	title += ' - ' + timePeriod;
    	if (metricType.indexOf('Use') == 0 || metricType.indexOf('CostUnit') == 0) title += ' (' + unit + ')'; 
    	
    	this.energyConsoleChartTwoYear_chart.setTitle(title);
    },
    
    energyConsoleChartTwoYear_chart_onMaximizeChart: function() {
    	var openerController = this.openerController;
    	View.openDialog('ab-energy-console-chart-two-year.axvw', null, false, {
    		maximize: true,
    		'openerController': openerController
    	});
    },
    
    energyConsoleChartTwoYear_chart_onShowHideLegend: function() {
    	this.hideLegend = !this.hideLegend;
    	this.energyConsoleChartTwoYear_chart.chartControl.chart.canvas.legendDiv.hidden = this.hideLegend;
    	this.energyConsoleChartTwoYear_displayChart();
    },
    
    adjustBalloonText: function(graphDataItem, graph){ 
		 var parts = graphDataItem.category.split('-');		 
		 var timePeriod = (parseInt(parts[0])-1) + "-" + parts[1];			 
		 var value = graphDataItem.values.value;
		 if (value > 1000000) {
			 value = parseInt(value/100000)/10 + 'M'
		 } else if (value > 1000) {
			 value = parseInt(value/100)/10 + 'k'
		 }
		 value = value.replace('.',strDecimalSeparator);
		 var title = graphDataItem.graph.title;
		 return  "<b>"+title+"</b><br>"+timePeriod+"<br><b>"+value+"</b>";
	}
});

function selectYearMo(obj) {
	var openerController = View.controllers.get('energyConsoleChartTwoYear').openerController;
	var time_period = obj.selectedChartData['bill_archive.time_period'];
	View.openDialog('ab-energy-console-chart-two-year-dtl.axvw', null, false, {
        width: 900,
        'time_period': time_period,
        'openerController': openerController
    });
}

function getValueField(table, metricType) {
	table = table + '.';
	var valueField = 'SUM(' + table + 'qtyUse)';
	switch(metricType) {
    	case 'Use': valueField = 'SUM(' + table + 'qtyUse)'; break;
    	case 'UseArea': valueField = 'CASE WHEN SUM(' + table + 'area_gross_ext)=0 THEN 0 ELSE (SUM(' + table + 'qtyUse) / SUM(' + table + 'area_gross_ext)) END'; break;
    	case 'UsePers': valueField = 'SUM(' + table + 'qtyUse) / SUM(' + table + 'countEm)'; break;
    	case 'UsePersDay': valueField = 'SUM(' + table + 'qtyUse) / (30.0*SUM(' + table + 'countEm))'; break;
    	case 'Cost': valueField = 'SUM(' + table + 'valueCost)'; break;
    	case 'CostArea': valueField = 'CASE WHEN SUM(' + table + 'area_gross_ext)=0 THEN 0 ELSE (SUM(' + table + 'valueCost) / SUM(' + table + 'area_gross_ext)) END'; break;
    	case 'CostPers': valueField = 'SUM(' + table + 'valueCost) / SUM(' + table + 'countEm)'; break;
    	case 'CostUnit': valueField = 'CASE WHEN SUM(' + table + 'qtyUse)=0 THEN NULL ELSE SUM(' + table + 'valueCost)/SUM(' + table + 'qtyUse) END'; break;
    	default: valueField = 'SUM(' + table + 'qtyUse)';
	}
	return valueField;
}