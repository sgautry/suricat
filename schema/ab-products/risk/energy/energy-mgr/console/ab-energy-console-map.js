var energyConsoleMapController = View.createController('energyConsoleMap', {
	mapControl: null,	
	openerController: null,
	
	afterViewLoad: function(){
    	if (View.parameters && View.parameters.maximize) {
			this.openerController = View.parameters.openerController;
		} else this.openerController = View.getOpenerView().controllers.get('energyConsole');
		if (this.openerController == undefined) return;
		
		if (!this.openerController.collapseMapOnLoad) this.initializeMapControl();
    },
    
    afterInitialDataFetch: function() {
    	this.energyConsoleMap_onShowMap();
    },
  	
  	initializeMapControl: function() {
    	var configObject = new Ab.view.ConfigObject();
    	configObject.mapImplementation = 'Esri';
    	configObject.basemap = this.openerController.layer;
    	
  		this.mapControl = new Ab.leaflet.Map('mapPanel', 'mapDiv', configObject);
    	if (this.mapControl != null) {		  	    	
	    	var basemapLayerMenu = this.energyConsoleMap_equiv.actions.get('basemapLayerMenu');
	        basemapLayerMenu.clear();
	        var basemapLayers = this.mapControl.getBasemapLayerList();
	        for (var i = 0; i < basemapLayers.length; i++){
		        basemapLayerMenu.addAction(i, basemapLayers[i], this.switchBasemapLayer);
		    }
	                
	        if (valueExistsNotEmpty(View.parameters)) {
				if (View.parameters.maximize) {			
					this.energyConsoleMap_equiv.actions.get('maximizeMap').show(false);
				}
			}
	        
	        colorbrewer.AbStoplight0 = {
	    		1: ["#1A9641"],
	    		2: ["#1A9641","#FFFF33"],
	    		3: ["#1A9641","#FFFF33","#D7191C"],
	    		4: ["#1A9641","#A6D96A","#F4A582","#D7191C"],
	    		5: ["#1A9641","#A6D96A","#FFFF33","#F4A582","#D7191C"]
	    	};	
	        
		} 
  	},
	
  	switchBasemapLayer: function(item) {
    	energyConsoleMapController.mapControl.switchBasemapLayer(item.text);
    	
		View.controllers.get('energyConsoleMap').openerController.layer = item.text;
		
		// match minimized map with layer change made in maximized map
		if (valueExistsNotEmpty(View.parameters)) {
			if (View.parameters.maximize) {
				var minimizedMapController = View.getOpenerView().panels.get('energyConsole_mapView').contentView.controllers.get('energyConsoleMap');
				minimizedMapController.mapControl.switchBasemapLayer(item.text);
			}
		}
    },
    
	energyConsoleMap_onShowMap: function() {
		var metricType = this.openerController.metricType;
		this.setCompareBySelect(this.openerController.metricType);
		this.energyConsoleMap_equiv.appendTitle(this.getMapTitle(metricType, false));
		setEnergyEquivValues(this.openerController, null, 'energyConsoleMap_equiv', false, true);
		
		if (this.mapControl != null) {
			this.energyConsoleMap_dsHighlight.fieldDefs.get('bl.area_usable').title = this.getMapTitle(metricType, true);
			var title = this.energyConsoleMap_dsHighlight.fieldDefs.get('bl.area_gross_ext').title;
			this.energyConsoleMap_dsHighlight.fieldDefs.get('bl.area_bl_comn_serv').title = title + ' ' + getMessage('thousand');
			this.energyConsoleMap_dsHighlight.addParameter('conversionFactor', this.openerController.conversionFactor);
			this.energyConsoleMap_dsHighlight.addParameter('billFilterRestriction', this.openerController.billFilterRestriction);
			this.energyConsoleMap_dsHighlight.addParameter('blsRestriction', this.openerController.blsRestriction);
			this.energyConsoleMap_dsHighlight.addParameter('valueField', getValueField(metricType));
			var valueFieldRestriction = ' 1=1 ';
			if (metricType == 'UsePers' || metricType == 'UsePersDay' || metricType == 'CostPers') {
				valueFieldRestriction += ' AND (countEm <> 0 OR bl.count_occup <> 0) ';
			} else if (metricType == 'UseArea' || metricType == 'CostArea') {
				valueFieldRestriction += ' AND bl.area_gross_ext <> 0 ';
			} else if (metricType == 'CostUnit') {
				valueFieldRestriction += ' AND qtyUse > 0 ';
			}
			this.energyConsoleMap_dsHighlight.addParameter('valueFieldRestriction', valueFieldRestriction);
			var records = this.energyConsoleMap_dsHighlight.getRecords(this.openerController.blFilterRestriction);
			this.createThematicMarkers(records);
			this.mapControl.showMarkers('energyConsoleMap_dsHighlight', this.openerController.blFilterRestriction);	
			
			if (valueExistsNotEmpty(View.parameters)) {
				if (View.parameters.maximize) {
					this.energyConsoleMap_equiv_onShowLegend();
				}
			}
		}
	},	
    
    createThematicMarkers: function(records) {
    	var dataSource = 'energyConsoleMap_dsHighlight';
        var keyFields = ['bl.bl_id'];
        var geometryFields = ['bl.lon', 'bl.lat'];
        var titleField = 'bl.bl_id';
        var contentFields = ['bl.name', 'bl.site_id', 'bl.ctry_id', 'bl.use1', 'bl.area_bl_comn_serv', 'bl.area_usable'];
        var markerActionTitle = getMessage('labelShowProfile');
        
        var intervals = this.getThematicMarkerIntervals(records);
        var areaIntervals = [100,1000];
        
        if (View.user.displayUnits == 'metric') areaIntervals = [10, 100];
        
        var markerProperties = {    				
    		radius: 7,
    		fillColor: '#e41a1c',
    		fillOpacity: 0.90,
    		stroke: true,
    		strokeColor: '#fff',
    		strokeWeight: 1.0,
    		renderer: 'thematic-graduated-class-breaks',
    		thematicField: 'bl.area_usable',		
    		thematicClassBreaks: intervals,
    		colorBrewerClass: 'AbStoplight0',
    		graduatedField: 'bl.area_bl_comn_serv',
			graduatedClassBreaks: areaIntervals,
			radiusIncrement: 3,
    		markerActionTitle: markerActionTitle,
            markerActionCallback: energyConsoleMapController.showBuildingDetails
    	};	

        this.mapControl.createMarkers(
    		dataSource,
    		keyFields,
    		geometryFields,
    		titleField,
    		contentFields,
    		markerProperties
    	);
    },
	
	getThematicMarkerIntervals: function(records) {		
		var field = 'bl.area_usable';
		var minVal = Number.MAX_VALUE;
		var maxVal = (-1)*Number.MAX_VALUE;
		for(var i = 0; i < records.length;i++){
			var record = records[i];
			var value = parseFloat(record.getValue(field));
			minVal = Math.min(minVal, value);
			maxVal = Math.max(maxVal, value);
		}
		var intervals = new Array();
		if (minVal != maxVal) {
			for(var i = 0; i < 4; i++){
				var val = new Number( minVal + ((maxVal - minVal)/5)*(i+1));
				intervals[i] = parseFloat(val.toExponential(1).toString());
			}
		} else {
			intervals[0] = parseFloat(minVal.toExponential(1).toString());
		}
		
		return intervals;
	},
	
	energyConsoleMap_equiv_onShowLegend: function(){
		energyConsoleMapController.mapControl.showMarkerLegend();
	},
	
    getMapTitle: function(metricType, showShortTitle) {
    	var billType = this.openerController.billType;
    	var unit = this.openerController.billUnit;
    	var timePeriod = this.openerController.timePeriod;
    	timePeriod = (timePeriod == 'Past12Months')?getMessage('panelTitle' + timePeriod):timePeriod;
    	var title = '';
    	var type = '';
    	//if (!showShortTitle) {
    	if (true) {
    		if (billType == 'AllEnergy') {
        		type += getMessage('panelTitle' + billType);
        	} else if (billType == 'ELECTRIC') {
        		type += getMessage('panelTitleElectric');
        	} else if (billType == 'FUEL OIL 2') {
        		type += getMessage('panelTitleOil');
        	} else if (billType == 'GAS - NATURAL') {
        		type += getMessage('panelTitleGas');
        	} else if (billType == 'WATER') {
        		type += getMessage('panelTitleWater');
        	} else {
        		type += billType;
        	}
    	}
    	title += type + ' - ' + getMessage('panelTitleMetric' + metricType);
    	if (!showShortTitle) title += ' - ' + timePeriod;
    	if (metricType.indexOf('Use') == 0 || metricType.indexOf('CostUnit') == 0) title += ' (' + unit + ')'; 
    	return title;
    },
    
    selectHighlight: function(metricType) {
    	var openerController = View.controllers.get('energyConsoleMap').openerController;
    	openerController.selectHighlight(metricType, true);  
    	
		if (valueExistsNotEmpty(View.parameters)) {
			if (View.parameters.maximize) {
				this.energyConsoleMap_onShowMap();
			}
		}
    },
    
    energyConsoleMap_equiv_onMaximizeMap: function() {
    	var openerController = this.openerController;
    	View.getOpenerView().openDialog('ab-energy-console-map.axvw', null, false, {
    		maximize: true,
    		'openerController': openerController
    	});
    },
    
    showBuildingDetails: function(key, attributes) {
    	var blId = key;
    	var openerController = View.controllers.get('energyConsoleMap').openerController; // map action callback does not recognize controller 'this'
		if (View.parameters && View.parameters.maximize) {
			View.openDialog('ab-energy-bl-profile.axvw', null, false, {
		        width: 940,
		        'blId':blId,
		        'openerController': openerController
		    });
		} else {
			View.getOpenerView().openDialog('ab-energy-bl-profile.axvw', null, false, {
		        width: 940,
		        'blId':blId,
		        'openerController': openerController
		    });
		}
    },
	
	setCompareBySelect: function(metricType) {
		$('energyConsoleMap_equiv_compareBySelect').value = metricType;
	}    
});

function getValueField(metricType) {
	var valueField = 'qtyUse';
	switch(metricType) {
    	case 'Use': valueField = 'qtyUse'; break;
    	case 'UseArea': valueField = 'CASE WHEN bl.area_gross_ext=0 THEN 0 ELSE (qtyUse / bl.area_gross_ext) END'; break;
    	case 'UsePers': valueField = 'CASE WHEN (countEm=0 AND bl.count_occup=0) THEN 0 WHEN countEm=0 THEN qtyUse/bl.count_occup ELSE qtyUse/countEm END'; break;
    	case 'UsePersDay': valueField = 'CASE WHEN (countEm=0 AND bl.count_occup=0) THEN 0 WHEN countEm=0 THEN qtyUse/(365.00*bl.count_occup) ELSE qtyUse/(365.00*countEm) END'; break;
    	case 'Cost': valueField = 'valueCost'; break;
    	case 'CostArea': valueField = 'CASE WHEN bl.area_gross_ext=0 THEN 0 ELSE (valueCost / bl.area_gross_ext) END'; break;
    	case 'CostPers': valueField = 'CASE WHEN (countEm=0 AND bl.count_occup=0) THEN 0 WHEN countEm=0 THEN valueCost/bl.count_occup ELSE valueCost/countEm END'; break;
    	case 'CostUnit': valueField = 'CASE WHEN qtyUse=0 THEN NULL ELSE valueCost/qtyUse END'; break;
    	default: valueField = 'qtyUse';
	}
	return valueField;
}


