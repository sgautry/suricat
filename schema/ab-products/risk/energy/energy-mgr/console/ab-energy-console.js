var energyConsoleController = View.createController('energyConsole',{
	hasBillType: null,
	defaultBillType:'AllEnergy',
	billType:'AllEnergy',
	billUnit:'KWH',
	timePeriod:'Past12Months',
	conversionFactor:'1.000000',
	billFilterRestriction:' 1=1 ',
	basDataRestriction:' 1=1 ',
	basBillTypeRestriction:' 1=1 ',
	blFilterRestriction:null,
	blsRestriction:' 1=1 ',
	billTypeRestriction: ' 1=1 ',
	dateRestrictionPrev1: ' 1=1 ',
	dateRestrictionPrev2: ' 1=1 ',
	blId: '%',
	metricType: 'Use',
	layer: '',
	defaultBasemapLayer: 'World Topographic Map',
	energyCommonEquiv: 90, // 90 MMBTU/year
	elecCommonEquiv: 36.863629, // 10,812 kWh/year
	waterCommonEquiv: 195, // 400 gallons/water/day
	gasCommonEquiv: 76.37, // 74 MCF/year
	oilCommonEquiv: 66.468, // 481.5 gallons/year
	energyUseAreaCommonEquivImperial: 0.080, // 80,000 BTU/sq ft
	elecUseAreaCommonEquivImperial: 0.051, // 51,000 BTU/sq ft
	gasUseAreaCommonEquivImperial: 0.038, // 38,000 BTU/sq ft
	isDemoMode: false,
	collapseMapOnLoad: false,
	
	events: {
        'change #energyConsole_filter_selectBillType': function() {
        	changeUnits();
        	energyConsoleController.energyConsole_filter_onShow();
        },
        
        'change #energyConsole_filter_selectTimePeriod': function() {
        	energyConsoleController.energyConsole_filter_onShow();
        },
        
        'change #energyConsole_filter_selectBillUnits': function() {
        	energyConsoleController.energyConsole_filter_onShow();
        }
    },
	
	afterViewLoad: function() {				
		this.isDemoMode = isDemoMode();
		this.layer = View.getLocalizedString(this.defaultBasemapLayer);	
		//var isIE9 = (navigator.appVersion.indexOf("MSIE 9.") != -1)?true:false;
		
		// collapse map if user does not store lat and lon values for any buildings in the bill_archive table
		var allBlMapRecords = this.energyConsoleCommon_dsMapBls.getRecords();
		if (allBlMapRecords.length == 0) {
			this.collapseMapPanel(true);
		}
		else if (!window.navigator.onLine) {
    		var controller = this;
    		View.confirm(getMessage('noInternetConnectivity'), function(button){
                if (button == 'yes') {
                	controller.collapseMapPanel(false);
                }
                else {
                	controller.collapseMapPanel(true);
                }
            });    		
    	}  
		else {
			this.collapseMapPanel(false);			
		}
		
		this.hasBillType = {
				'ELECTRIC': false, 
				'GAS - NATURAL': false,
				'FUEL OIL 2': false,
				'WATER': false
		};
		this.setBillTypeOptions();
		this.setTimePeriodOptions();	
		
		var grid = this.energyConsole_report;
		
		grid.afterCreateCellContent = function(row, column, cellElement) {
		    var value = row[column.id + '.raw'];
			if (column.id == 'bl.bl_id')	{
				cellElement.style.fontWeight = 'bold';
			}
			if (column.id == 'bl.pctDiffUsePrev1' || column.id == 'bl.pctDiffUsePrev2')	{
				var contentElement = cellElement.childNodes[0];
	                if (contentElement) {
	                	contentElement.nodeValue = contentElement.nodeValue + '%';
						if (value <= 0)	{
		                    cellElement.style.color = 'Green';//#2DA053 Green
		                }
		                else {
		                    cellElement.style.color = '#F00000';//#F12E34 Red
		                }
                }
			}
		}
	},
	
    afterInitialDataFetch: function(){
		this.energyConsole_filter_onClear();
    },
    
    setBillTypeOptions: function() { 
    	var field = this.energyConsole_filter.fields.get('selectBillType');
        
    	var records = this.energyConsole_dsBillTypes.getRecords();
    	var isEnergy = '0';
    	var optionText = '';
    	for (i = 0; i < records.length; i++) {
    		optionText = '';
    		var record = records[i];
    		isEnergy = record.getValue('bill_archive.isEnergy');
    		var billTypeId = record.getValue('bill_archive.bill_type_id');
    		//if (isEnergy == '1') optionText += '- ';    		
    		optionText += billTypeId;
    		field.addOption(billTypeId, optionText);
			
			if (billTypeId == 'ELECTRIC' || billTypeId == 'GAS - NATURAL' || billTypeId == 'FUEL OIL 2' || billTypeId == 'WATER') this.hasBillType[billTypeId] = true;
    	}
    	this.energyConsole_filter.setFieldValue('selectBillType', this.defaultBillType);
    },
    
    setTimePeriodOptions: function() {
    	var field = this.energyConsole_filter.fields.get('selectTimePeriod');
    	
    	var records = this.energyConsole_dsYears.getRecords();
    	for (i = 0; i < records.length; i++) {
    		var record = records[i];
    		var year = records[i].getValue('bill_archive.year');
    		field.addOption(year, year);
    	}
    	if (this.isDemoMode) this.energyConsole_filter.setFieldValue('selectTimePeriod', '2015');
    	else this.energyConsole_filter.setFieldValue('selectTimePeriod', 'Past12Months');
    },
    
    energyConsole_filter_onSelectTypeIcon: function(billType) {
    	if (!this.hasBillType[billType]) {
    		var billTypeTitle = '';
    		switch (billType) {
    			case "ELECTRIC":
    				billTypeTitle = getMessage('panelTitleElectric');
    				break;
    	      	case "GAS - NATURAL":
    	      		billTypeTitle = getMessage('panelTitleGas');
    	      		break;
    	      	case "FUEL OIL 2":
    	      		billTypeTitle = getMessage('panelTitleOil');
    				break;
    	      	case "WATER":
    	      		billTypeTitle = getMessage('panelTitleWater');
    	      		break;
    	      	default:
    		}
    		var message = String.format(getMessage('billTypeNotExists'), billTypeTitle);
    		View.alert(message);
    	} else {
    		this.energyConsole_filter.setFieldValue('selectBillType', billType);
    		changeUnits();    	
    		this.energyConsole_filter_onShow();
    	}
    },
    
    energyConsole_filter_onShow: function() {
    	this.billType = this.energyConsole_filter.getFieldValue('selectBillType');
    	this.billUnit = this.energyConsole_filter.getFieldValue('selectBillUnits');
    	this.timePeriod = this.energyConsole_filter.getFieldValue('selectTimePeriod');
    	this.conversionFactor = getBillUnitConversionFactor(this.billType, this.billUnit);
    	this.billFilterRestriction = getBillFilterRestriction(this.billType, this.timePeriod);
    	this.basDataRestriction = getBasDataRestriction(this.timePeriod);
    	this.basBillTypeRestriction = getBasBillTypeRestriction(this.billType);
    	this.blFilterRestriction = getBlFilterRestriction();
    	this.billTypeRestriction = getBillTypeRestriction(this.billType);
    	this.dateRestrictionPrev1 = getDateRestrictionPrev(this.timePeriod, 1);
    	this.dateRestrictionPrev2 = getDateRestrictionPrev(this.timePeriod, 2);
    	this.blsRestriction = " 1=1 ";
    	
    	this.energyConsole_report.addParameter('conversionFactor', this.conversionFactor);    	
    	this.energyConsole_report.addParameter('billUnit', this.billUnit);
    	this.energyConsole_report.addParameter('billFilterRestriction', this.billFilterRestriction);
    	this.energyConsole_report.addParameter('billTypeRestriction', this.billTypeRestriction); 
    	this.energyConsole_report.addParameter('dateRestrictionPrev1', this.dateRestrictionPrev1); 
    	this.energyConsole_report.addParameter('dateRestrictionPrev2', this.dateRestrictionPrev2);
    	
    	this.energyConsole_report.addParameter('pctDiffUsePrev1Title', getPctUsePrevTitle(this, 1));
    	this.energyConsole_report.addParameter('pctDiffUsePrev2Title', getPctUsePrevTitle(this, 2));
    	this.energyConsole_report.refresh(this.blFilterRestriction);
    	
    	this.energyConsole_report_onShowSelected();
    },
    
    energyConsole_report_onShowSelected: function() {
    	var records = View.panels.get('energyConsole_report').getSelectedRecords();
    	if (records.length == 0) {
    		this.blsRestriction = " 1=1 ";
    	} else {
    		var blIds = "";
    		for (var i = 0; i < records.length; i++) {
    			if (blIds != "") blIds += ", ";
    	        var record = records[i];
    	        var bl_id = record.getValue('bl.bl_id');
    	        blIds += "'" + bl_id + "'";
    	    }
    		var blsRestriction = " bill_archive.bl_id IN (" + blIds + ") ";
    		this.blsRestriction = blsRestriction;
    	}
    	this.selectHighlight(this.metricType); 
    },
    
    selectHighlight: function(metricType) {
    	this.metricType = metricType;
    	View.controllers.get('energyConsoleChart').energyConsoleChart_displayChart();
    	View.controllers.get('energyConsoleChartTwoYear').energyConsoleChartTwoYear_displayChart();
    	refreshMapView(metricType);   	
    },
    
    energyConsole_filter_onClear: function() {
    	this.energyConsole_filter.clear();
    	if (this.isDemoMode) this.energyConsole_filter.setFieldValue('selectTimePeriod', '2015');
    	else this.energyConsole_filter.setFieldValue('selectTimePeriod', 'Past12Months');
    	this.energyConsole_filter.setFieldValue('selectBillType', this.defaultBillType);
    	changeUnits();
    	this.energyConsole_filter_onShow();
    },
    
    energyConsole_report_beforeRefresh: function() {
    	View.openProgressBar();
    	View.updateProgressBar(1/2);
    },
    
    energyConsole_report_afterRefresh: function() {
    	var billType = this.billType;
    	var unit = this.billUnit;
    	var timePeriod = this.timePeriod;
    	timePeriod = (timePeriod == 'Past12Months')?getMessage('panelTitle' + timePeriod):timePeriod;
    	var title = '';
    	if (billType == 'AllEnergy') {
    		title += getMessage('panelTitle' + billType);
    	} else if (billType == 'ELECTRIC') {
    		title += getMessage('panelTitleElectric');
    	} else if (billType == 'FUEL OIL 2') {
    		title += getMessage('panelTitleOil');
    	} else if (billType == 'GAS - NATURAL') {
    		title += getMessage('panelTitleGas');
    	} else if (billType == 'WATER') {
    		title += getMessage('panelTitleWater');
    	} else {
    		title += billType;
    	}
    	title += ' - ' + getMessage('panelTitleBills');
    	title += ' - ' + timePeriod;
    	title += ' (' + unit + ')';
    	this.energyConsole_report.setTitle(title);
    	View.closeProgressBar();
    },
	
	selectBuildingSnapshot:function(obj) {
		var controller = this;
		var blId = obj.restriction['bl.bl_id'];
		View.openDialog('ab-energy-bl-profile.axvw', null, false, {
	        width: 940,
	        'blId': blId,
	        'openerController': controller
	    });
	},

    collapseMapPanel: function (collapse) {
        var layout = View.getLayoutManager('nested_center');
        var westRegion = layout.getRegion('west');
        var width = westRegion.width;
        if (width == undefined) {
        	width = Ext.get(document.body).getViewSize().width;
            westRegion.initialSize = '50%';
        }
        if (collapse) {
            this.collapseMapOnLoad = true;
            //layout.expandRegion('west');
            this.energyConsole_report.actions.get('energyConsole_highlightMenu').show(true);
        } else {
            this.collapseMapOnLoad = false;
            layout.setRegionSize('west', width / 2);
            this.energyConsole_report.actions.get('energyConsole_highlightMenu').show(false);
        }
    }
});

function getBlFilterRestriction() {
	var panel = View.panels.get('energyConsole_filter');
	var restriction = panel.getFieldRestriction();
	restriction.removeClause('selectBillType');
	restriction.removeClause('selectTimePeriod');
	restriction.removeClause('selectBillUnits');
	return restriction;
}

function getBillFilterRestriction(billType, timePeriod) {
	var panel = View.panels.get('energyConsole_filter');
	
	var startEndDates = getStartEndDates(timePeriod);
	
	var restriction = "bill_archive.time_period >= '" + startEndDates['startDateISO'].substring(0, 7) + "'";
	restriction += " AND bill_archive.time_period <= '" + startEndDates['endDateISO'].substring(0, 7) + "'";
	
	switch(billType) {
	    case 'AllEnergy': 
	    	restriction += " AND bill_archive.qty_energy > 0 "; break;
		default:
			restriction += " AND bill_archive.bill_type_id = '" + billType + "'";
	}
	return restriction;
}

function getDateRestrictionPrev(timePeriod, yearsAgo) {
	var panel = View.panels.get('energyConsole_filter');
	
	var startEndDates = getStartEndDates(timePeriod);
	var startDate = startEndDates['startDateISO'];
	var endDate = startEndDates['endDateISO'];
	var parts = startDate.split('-');
	startDate = (parseInt(parts[0])-yearsAgo) + "-" + parts[1];
	parts = endDate.split('-');
	endDate = (parseInt(parts[0])-yearsAgo) + "-" + parts[1];
	
	var restriction = "bill_archive.time_period >= '" + startDate + "'";
	restriction += " AND bill_archive.time_period <= '" + endDate + "'";
	return restriction;
}

function getBillTypeRestriction(billType) {
	var panel = View.panels.get('energyConsole_filter');
	
	var restriction = "";
	
	switch(billType) {
	    case 'AllEnergy': 
	    	restriction += " bill_archive.qty_energy > 0 "; break;
		default:
			restriction += " bill_archive.bill_type_id = '" + billType + "'";
	}
	return restriction;
}

function getBasDataRestriction(timePeriod) {
	var startEndDates = getStartEndDates(timePeriod);
	var startDate = startEndDates['startDateISO'];
	var endDate = startEndDates['endDateISO'];
	var restriction = "bas_data_time_norm_num.date_measured >= ${sql.date('" + startDate + "')} ";
	restriction += " AND bas_data_time_norm_num.date_measured <= ${sql.date('" + endDate + "')} ";
	return restriction;
}

function getBasBillTypeRestriction(billType) {
	var restriction = '';
	switch(billType) {
	    case 'AllEnergy': 
	    	restriction = " bas_data_point.bill_type_id IN (SELECT bill_type_id FROM bill_unit WHERE bill_unit.rollup_type = 'Energy')"; break;
		default:
			restriction = " bas_data_point.bill_type_id = '" + billType + "'";
	}
	return restriction;
}

function changeUnits() {
	var panel = View.panels.get('energyConsole_filter');
	var field = panel.fields.get('selectBillUnits');
	var billTypeId = panel.getFieldValue('selectBillType');
	if (billTypeId == 'AllEnergy') billTypeId = 'ELECTRIC';

	var dataSource = View.dataSources.get('energyConsole_dsBillUnits');
	var restriction = new Ab.view.Restriction();
	restriction.addClause('bill_unit.bill_type_id', billTypeId);
	var records = dataSource.getRecords(restriction);
	
	field.clearOptions();
	for (var i = 0; i < records.length; i++){
		var value = records[i].values['bill_unit.bill_unit_id'];	
		field.addOption(value, value);
	}
}

function getBillUnitConversionFactor(billTypeId, billUnitId) {
	if (billTypeId == 'AllEnergy') billTypeId = 'ELECTRIC';	
	var restriction = new Ab.view.Restriction();
	restriction.addClause('bill_unit.bill_type_id', billTypeId);
	restriction.addClause('bill_unit.bill_unit_id', billUnitId);   	
	var dataSource = View.dataSources.get('energyConsole_dsBillUnits');
	var records = dataSource.getRecords(restriction);
	
	var conversionFactor = 1;
	if (records.length > 0) {
		conversionFactor = records[0].getValue('bill_unit.conversion_factor');
		conversionFactor = (conversionFactor == 0)?1:conversionFactor;
	}	
	return conversionFactor;
}

function getStartEndDates(timePeriod) {
	var startDate = new Date();
	var endDate = new Date();
	var startDateISO = '2016-01-01';
	var endDateISO = '2016-12-31';
	if (timePeriod == 'Past12Months') { //This restriction includes up to the end of the current month.  This is so that users can immediately see newly-entered data.  eg. If today is 7/4/2016...
		startDate.setMonth(startDate.getMonth() - 11); 
		endDate.setMonth(startDate.getMonth() + 1); 
		endDate.setDate(1); 
		endDate.setDate(endDate.getDate() - 1);
		
		startDateISO = startDate.getFullYear() + '-' + (((startDate.getMonth()+1) < 10)?'0':'') + (startDate.getMonth()+1) + '-' + '01'; // startDateISO is 2015-08-01
		endDateISO = endDate.getFullYear() + '-' + (((endDate.getMonth()+1) < 10)?'0':'') + (endDate.getMonth()+1) + '-' + ((endDate.getDate() < 10)?'0':'') + endDate.getDate(); // endDateISO is 2016-07-31
	} else { // eg. User selects 2016
		startDateISO = timePeriod + '-' + '01-01'; // startDateISO is 2016-01-01
		endDateISO = timePeriod + '-' + '12-31'; // endDateISO is 2016-12-31
	}
	var startEndDates = {
			startDateISO: startDateISO,
			endDateISO: endDateISO
	};
	return startEndDates;
} 

function loadEnergyBills() {
	var controller = View.controllers.get('energyConsole');
	View.openDialog('ab-energy-console-load-bills.axvw', null, false, {
		callback: function() {
			controller.energyConsole_filter_onShow();
		}
	});
}

function enterBills() {
	var controller = View.controllers.get('energyConsole');
	View.openDialog('ab-energy-console-enter-bills.axvw', null, false, {
		maximize: true,
		closeButton: true,
		callback: function() {
			controller.energyConsole_filter_onShow();
		}
	});
}

function reviewBills() {
	var controller = View.controllers.get('energyConsole');
	View.openDialog('ab-energy-bill-audit-approve-tabs.axvw', null, false, {
		maximize: true,
		closeButton: true,
		callback: function() {
			controller.energyConsole_filter_onShow();
		}
	});
}

function refreshMapView(metricType) {
	var mapView = View.panels.get('energyConsole_mapView').contentView;
	if (mapView) {
		var energyConsoleMapController = mapView.controllers.get('energyConsoleMap');
		energyConsoleMapController.setCompareBySelect(metricType);
		energyConsoleMapController.energyConsoleMap_onShowMap();
	}
}
