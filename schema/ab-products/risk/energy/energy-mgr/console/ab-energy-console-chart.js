var energyConsoleChartController = View.createController('energyConsoleChart',{
	openerController: null,
	
	afterViewLoad: function() {
		this.openerController = View.controllers.get('energyConsole');
		this.energyConsoleChart_chart.chartControl.chart.canvas.legendDiv.hidden = true;
	},
	
    afterInitialDataFetch: function(){
		this.energyConsoleChart_displayChart();
    },    
    
    energyConsoleChart_displayChart: function() {
    	var metricType = this.openerController.metricType;
    	this.addEnergyChartParametersAndRefresh(metricType);
    	this.refreshChartTitle(metricType);
    },    
    
    addEnergyChartParametersAndRefresh: function(metricType) {
    	var chart = this.energyConsoleChart_chart;
    	chart.addParameter('conversionFactor', this.openerController.conversionFactor);
		chart.addParameter('billFilterRestriction', this.openerController.billFilterRestriction);
		chart.addParameter('blsRestriction', this.openerController.blsRestriction);
		
    	var valueField = 'qtyUse';
    	switch(metricType) {
	    	case 'Use': valueField = 'qtyUse'; break;
	    	case 'UseArea': valueField = 'CASE WHEN bl.area_gross_ext=0 THEN 0 ELSE (qtyUse / bl.area_gross_ext) END'; break;
	    	case 'UsePers': valueField = 'CASE WHEN (countEm=0 AND bl.count_occup=0) THEN 0 WHEN countEm=0 THEN qtyUse/bl.count_occup ELSE qtyUse/countEm END'; break;
	    	case 'UsePersDay': valueField = 'CASE WHEN (countEm=0 AND bl.count_occup=0) THEN 0 WHEN countEm=0 THEN qtyUse/(365.00*bl.count_occup) ELSE qtyUse/(365.00*countEm) END'; break;
	    	case 'Cost': valueField = 'valueCost'; break;
	    	case 'CostArea': valueField = 'CASE WHEN bl.area_gross_ext=0 THEN 0 ELSE (valueCost / bl.area_gross_ext) END'; break;
	    	case 'CostPers': valueField = 'CASE WHEN (countEm=0 AND bl.count_occup=0) THEN 0 WHEN countEm=0 THEN valueCost/bl.count_occup ELSE valueCost/countEm END'; break;
	    	case 'CostUnit': valueField = 'CASE WHEN qtyUse=0 THEN NULL ELSE valueCost/qtyUse END'; break;
	    	default: valueField = 'qtyUse';
		}
		chart.addParameter('valueField', valueField);
		
		var valueFieldRestriction = ' 1=1 ';
		if (metricType == 'UsePers' || metricType == 'UsePersDay' || metricType == 'CostPers') {
			valueFieldRestriction += ' AND (bl.count_occup <> 0 OR countEm <> 0) ';
		} else if (metricType == 'UseArea' || metricType == 'CostArea') {
			valueFieldRestriction += ' AND bl.area_gross_ext <> 0 ';
		} else if (metricType == 'CostUnit') {
			valueFieldRestriction += ' AND qtyUse > 0 ';
		}
		chart.addParameter('valueFieldRestriction', valueFieldRestriction);
		chart.refresh(this.openerController.blFilterRestriction);
		chart.show();
    },
    
    refreshChartTitle: function(metricType) {
    	var billType = this.openerController.billType;
    	var unit = this.openerController.billUnit;
    	var timePeriod = this.openerController.timePeriod;
    	timePeriod = (timePeriod == 'Past12Months')?getMessage('panelTitle' + timePeriod):timePeriod;
        var title = getMessage('panelTitleTopTen') + ' ';
    	var type = '';
    	if (billType == 'AllEnergy') {
    		type += getMessage('panelTitle' + billType);
    	} else if (billType == 'ELECTRIC') {
    		type += getMessage('panelTitleElectric');
    	} else if (billType == 'FUEL OIL 2') {
    		type += getMessage('panelTitleOil');
    	} else if (billType == 'GAS - NATURAL') {
    		type += getMessage('panelTitleGas');
    	} else if (billType == 'WATER') {
    		type += getMessage('panelTitleWater');
    	} else {
    		type += billType;
    	}
        title += type + ' - ' + getMessage('panelTitleMetric' + metricType);
    	title += ' - ' + timePeriod;
    	if (metricType.indexOf('Use') == 0 || metricType.indexOf('CostUnit') == 0) title += ' (' + unit + ')'; 
    	
    	this.energyConsoleChart_chart.setTitle(title);
    }
});

function selectBldg(obj) {
	var openerController = View.controllers.get('energyConsoleChart').openerController;
	var bl_id = obj.selectedChartData['bl.bl_id'];
	View.openDialog('ab-energy-bl-profile.axvw', null, false, {
        width: 940,
        'blId': bl_id,
        'openerController': openerController
    });
}