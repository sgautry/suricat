<view version="2.0">
    <js file="ab-energy-console-chart-two-year-dtl.js"/>
    <layout id="main">
        <north initialSize="50%" split="true" autoScroll="true" id="reportSel"/>
        <center autoScroll="true" id="reportPrev"/>
    </layout>
    
    <dataSource id="energyConsoleChartTwoYearDtl_ds1">
        <table name="bill_archive" role="main"/>
        <sql dialect="generic">
            SELECT bill_archive.time_period, bill_archive.bill_id, bill_archive.bill_type_id,
            bill_archive.amount_expense, (CASE WHEN (qty_energy IS NULL OR qty_energy = 0) 
                    THEN (CASE WHEN qty_volume IS NULL THEN 0 ELSE qty_volume END) 
                    ELSE qty_energy END) / ${parameters['conversionFactor']} ${sql.as} qty_energy,
            bill_archive.vn_id, bill_archive.vn_ac_id, bill_archive.bl_id, bill_archive.status,
            bill_archive.doc
            FROM bill_archive
            LEFT OUTER JOIN bl ON bill_archive.bl_id = bl.bl_id
            WHERE ${parameters['billTypeRestriction']}
            AND ${parameters['valueFieldRestriction']}
            AND ${parameters['blsRestriction']} 
            AND ${parameters['blFilterRestriction']}
        </sql>
        <field table="bill_archive" name="time_period"/>
        <field table="bill_archive" name="bill_id"/>
        <field table="bill_archive" name="bill_type_id"/>
        <field name="amount_expense"  table="bill_archive" decimals="2" showTotals="true"/>
        <field name="qty_energy" table="bill_archive" decimals="2" showTotals="true">
            <title>Use (${panel.parameters.billUnit})</title>
        </field>
        <field table="bill_archive" name="vn_id"/>
        <field table="bill_archive" name="vn_ac_id"/>
        <field table="bill_archive" name="bl_id"/>
        <field table="bill_archive" name="status"/>
        <field table="bill_archive" name="doc"/>
        <parameter name="billTypeRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="blsRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="blFilterRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="valueFieldRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="conversionFactor" dataType="number" value="1.000000"/>
    </dataSource>
    
    <panel type="grid" id="energyConsoleChartTwoYearDtl_billReport" dataSource="energyConsoleChartTwoYearDtl_ds1" showOnLoad="false" layoutRegion="reportSel">      
        <sortField name="time_period" table="bill_archive" ascending="true"/>
        <sortField name="bl_id" table="bill_archive" ascending="true"/>
        <sortField name="bill_type_id" table="bill_archive" ascending="true"/>
        <title>Approved Bills - Selected Year</title>
        <action type="menu" id="energyConsoleChartByYear_billReport_toolsMenu" imageName="/schema/ab-core/graphics/icons/view/export.png">
            <tooltip>Export</tooltip>
            <action id="exportDOCX">
                <title>Export to DOCX</title>
                <command type="exportPanel" outputType="docx" orientation="landscape" panelId="energyConsoleChartTwoYearDtl_billReport"/>
            </action>
            <action id="exportXLS">
                <title>Export to XLS</title>
                <command type="exportPanel" outputType="xls" panelId="energyConsoleChartTwoYearDtl_billReport"/>
            </action>
            <action id="exportPDF">
                <title>Export to PDF</title>
                <command type="exportPanel" outputType="pdf" orientation="landscape" panelId="energyConsoleChartTwoYearDtl_billReport"/>
            </action>
        </action>
        <field table="bill_archive" name="time_period" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartTwoYearDtl_billLines"/>
        </field>
        <field table="bill_archive" name="bill_id" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartTwoYearDtl_billLines"/>
        </field>
        <field table="bill_archive" name="bill_type_id" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartTwoYearDtl_billLines"/>
        </field>
        <field name="amount_expense"  table="bill_archive" decimals="2" showTotals="true" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartTwoYearDtl_billLines"/>
        </field>
        <field name="qty_energy" table="bill_archive" dataType="number" decimals="2" showTotals="true" controlType="link">
            <title>Use (${panel.parameters.billUnit})</title>
            <command type="openDialog" panelId="energyConsoleChartTwoYearDtl_billLines"/> 
        </field>            
        <field table="bill_archive" name="vn_id" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartTwoYearDtl_billLines"/>
        </field>
        <field table="bill_archive" name="vn_ac_id" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartTwoYearDtl_billLines"/>
        </field>
        <field table="bill_archive" name="bl_id" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartTwoYearDtl_billLines"/>
        </field>
        <field table="bill_archive" name="doc" controlType="link"/>
        <field table="bill_archive" name="status" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartTwoYearDtl_billLines"/>
        </field>        
    </panel> 
    
    <panel type="grid" id="energyConsoleChartTwoYearDtl_billReportPrev" dataSource="energyConsoleChartTwoYearDtl_ds1" showOnLoad="false" layoutRegion="reportPrev">      
        <sortField name="time_period" table="bill_archive" ascending="true"/>
        <sortField name="bl_id" table="bill_archive" ascending="true"/>
        <sortField name="bill_type_id" table="bill_archive" ascending="true"/>
        <title>Approved Bills - Previous Year</title>
        <action type="menu" id="energyConsoleChartByYear_billReportPrev_toolsMenu" imageName="/schema/ab-core/graphics/icons/view/export.png">
            <tooltip>Export</tooltip>
            <action id="exportDOCX">
                <title>Export to DOCX</title>
                <command type="exportPanel" outputType="docx" orientation="landscape" panelId="energyConsoleChartTwoYearDtl_billReportPrev"/>
            </action>
            <action id="exportXLS">
                <title>Export to XLS</title>
                <command type="exportPanel" outputType="xls" panelId="energyConsoleChartTwoYearDtl_billReportPrev"/>
            </action>
            <action id="exportPDF">
                <title>Export to PDF</title>
                <command type="exportPanel" outputType="pdf" orientation="landscape" panelId="energyConsoleChartTwoYearDtl_billReportPrev"/>
            </action>
        </action>
        <field table="bill_archive" name="time_period" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartTwoYearDtl_billLines"/>
        </field>
        <field table="bill_archive" name="bill_id" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartTwoYearDtl_billLines"/>
        </field>
        <field table="bill_archive" name="bill_type_id" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartTwoYearDtl_billLines"/>
        </field>
        <field name="amount_expense"  table="bill_archive" decimals="2" showTotals="true" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartTwoYearDtl_billLines"/>
        </field>
        <field name="qty_energy" table="bill_archive" dataType="number" decimals="2" showTotals="true" controlType="link">
            <title>Use (${panel.parameters.billUnit})</title> 
            <command type="openDialog" panelId="energyConsoleChartTwoYearDtl_billLines"/>
        </field>            
        <field table="bill_archive" name="vn_id" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartTwoYearDtl_billLines"/>
        </field>
        <field table="bill_archive" name="vn_ac_id" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartTwoYearDtl_billLines"/>
        </field>
        <field table="bill_archive" name="bl_id" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartTwoYearDtl_billLines"/>
        </field>
        <field table="bill_archive" name="doc" controlType="link"/> 
        <field table="bill_archive" name="status" controlType="link">
            <command type="openDialog" panelId="energyConsoleChartTwoYearDtl_billLines"/>
        </field>      
    </panel>
    
    <dataSource id="energyConsoleChartTwoYearDtl_ds0">
        <table name="bill_line_archive" role="main"/>
        <table name="vn_rate" role="standard"/>
        <field name="vn_id" table="bill_line_archive"/>
        <field name="bill_id" table="bill_line_archive"/>
        <field name="vn_rate_id" table="bill_line_archive"/>
        <field name="vn_rate_desc" table="vn_rate"/>
        <field name="qty" table="bill_line_archive"/>
        <field name="bill_unit_id" table="bill_line_archive"/>
        <field name="amount_expense" table="bill_line_archive" showTotals="true"/>
        <field name="amount_income" table="bill_line_archive" showTotals="true"/>
        <field name="bill_line_id" table="bill_line_archive"/>  
        <field name="description" table="bill_line_archive"/>
        <field name="vn_meter_id" table="bill_line_archive"/>   
    </dataSource>
    
    <panel type="grid" id="energyConsoleChartTwoYearDtl_billLines" dataSource="energyConsoleChartTwoYearDtl_ds0" showOnLoad="false" hidden="true">
        <title>Bill Lines</title>
        <sortField name="bill_line_id" ascending="true"/>
        <action type="menu" id="energyConsoleChartTwoYearDtl_billLines_toolsMenu" imageName="/schema/ab-core/graphics/icons/view/export.png">
            <tooltip>Export</tooltip>
            <action id="exportDOCX">
                <title>Export to DOCX</title>
                <command type="exportPanel" outputType="docx" orientation="landscape" panelId="energyConsoleChartTwoYearDtl_billLines"/>
            </action>
            <action id="exportXLS">
                <title>Export to XLS</title>
                <command type="exportPanel" outputType="xls" panelId="energyConsoleChartTwoYearDtl_billLines"/>
            </action>
            <action id="exportPDF">
                <title>Export to PDF</title>
                <command type="exportPanel" outputType="pdf" orientation="landscape" panelId="energyConsoleChartTwoYearDtl_billLines"/>
            </action>
        </action>
        <field name="bill_line_id" table="bill_line_archive"/> 
        <field name="vn_rate_desc" table="vn_rate"/>
        <field name="amount_expense" table="bill_line_archive" showTotals="true"/>
        <field name="amount_income" table="bill_line_archive" showTotals="true"/>              
        <field name="qty" table="bill_line_archive"/>
        <field name="bill_unit_id" table="bill_line_archive"/>
        <field name="vn_id" table="bill_line_archive"/>
        <field name="bill_id" table="bill_line_archive"/> 
        <field name="vn_meter_id" table="bill_line_archive"/>
        <field name="description" table="bill_line_archive"/>  
    </panel>  
    
    <panel type="view" id="energyConsoleChartTwoYearDtl_commonView" file="ab-energy-console-common.axvw" hidden="true"/>
    
</view>
