<view version="2.0">
    <js file="ab-energy-console-chart-two-year.js"/>
    
    <message name="panelTitleAllEnergy">Energy</message>
    <message name="panelTitleElectric">Electricity</message>
    <message name="panelTitleGas">Natural Gas</message>
    <message name="panelTitleOil">Heating Oil</message>
    <message name="panelTitleWater">Water</message>
    <message name="panelTitleMetricUse">Use</message>
    <message name="panelTitleMetricUseArea">Use per Area</message>
    <message name="panelTitleMetricUsePers">Use per Person</message>
    <message name="panelTitleMetricUsePersDay">Use per Person per Day</message>
    <message name="panelTitleMetricCost">Cost</message>
    <message name="panelTitleMetricCostArea">Cost per Area</message>
    <message name="panelTitleMetricCostPers">Cost per Person</message>
    <message name="panelTitleMetricCostUnit">Cost per Unit</message>
    <message name="panelTitlePast12Months">Past 12 Months</message>
    <message name="panelTitleYearOverYear">Two Year Comparison</message>
    
    <dataSource id="energyConsoleChartTwoYear_chartDs" type="grouping">
        <table name="bill_archive" role="main"/>            
        <sql dialect="oracle">
            SELECT bill_archive.time_period,
                (${parameters['valueField']}) ${sql.as} valueField,
                (${parameters['valueFieldPrev']}) ${sql.as} valueFieldPrev
            FROM
                (SELECT bill_archive.bl_id, bill_archive.time_period, MAX(bl.area_gross_ext) ${sql.as} area_gross_ext,
                    TO_CHAR(TRUNC(SUBSTR(time_period, 1, 4), 0)-1) ${sql.concat} SUBSTR(time_period, 5, 3) ${sql.as} time_period_prev,
                    (CASE WHEN (SUM(qty_energy) IS NULL OR SUM(qty_energy) = 0) 
                        THEN (CASE WHEN SUM(qty_volume) IS NULL THEN 0 ELSE SUM(qty_volume) END) 
                        ELSE SUM(qty_energy) END) / ${parameters['conversionFactor']} ${sql.as} qtyUse,                                      
                    SUM(bill_archive.amount_expense) ${sql.as} valueCost,
                    CASE WHEN (SELECT COUNT(em.em_id) FROM em WHERE em.bl_id = bill_archive.bl_id) = 0 THEN MAX(bl.count_occup) 
                        ELSE (SELECT COUNT(em.em_id) FROM em WHERE em.bl_id = bill_archive.bl_id) END ${sql.as} countEm                    
                FROM bill_archive
                LEFT OUTER JOIN bl ON bl.bl_id = bill_archive.bl_id
                WHERE ${parameters['billFilterRestriction']} AND ${parameters['blsRestriction']} AND ${parameters['blFilterRestriction']}
                GROUP BY bill_archive.bl_id, bill_archive.time_period) ${sql.as} bill_archive
            LEFT OUTER JOIN
                (SELECT bill_archive.bl_id, bill_archive.time_period, MAX(bl.area_gross_ext) ${sql.as} area_gross_ext,
                    (CASE WHEN (SUM(qty_energy) IS NULL OR SUM(qty_energy) = 0) 
                        THEN (CASE WHEN SUM(qty_volume) IS NULL THEN 0 ELSE SUM(qty_volume) END) 
                        ELSE SUM(qty_energy) END) / ${parameters['conversionFactor']} ${sql.as} qtyUse,                                      
                    SUM(bill_archive.amount_expense) ${sql.as} valueCost,
                    CASE WHEN (SELECT COUNT(em.em_id) FROM em WHERE em.bl_id = bill_archive.bl_id) = 0 THEN MAX(bl.count_occup) 
                        ELSE (SELECT COUNT(em.em_id) FROM em WHERE em.bl_id = bill_archive.bl_id) END ${sql.as} countEm                   
                FROM bill_archive
                LEFT OUTER JOIN bl ON bl.bl_id = bill_archive.bl_id
                WHERE ${parameters['billTypeRestriction']} AND ${parameters['blsRestriction']} AND ${parameters['blFilterRestriction']}
                GROUP BY bill_archive.bl_id, bill_archive.time_period) ${sql.as} bill_archive_prev
            ON (bill_archive.bl_id = bill_archive_prev.bl_id AND bill_archive.time_period_prev = bill_archive_prev.time_period)
            WHERE ${parameters['valueFieldRestriction']} 
            GROUP BY bill_archive.time_period
        </sql>
        <sql dialect="generic">
            SELECT bill_archive.time_period,
                (${parameters['valueField']}) ${sql.as} valueField,
                (${parameters['valueFieldPrev']}) ${sql.as} valueFieldPrev
            FROM
                (SELECT bill_archive.bl_id, bill_archive.time_period, MAX(bl.area_gross_ext) ${sql.as} area_gross_ext,
                    CONVERT(VARCHAR, (CONVERT(INT,SUBSTRING(time_period, 1, 4))-1)) ${sql.concat} SUBSTRING(time_period, 5, 3) ${sql.as} time_period_prev,
                    (CASE WHEN (SUM(qty_energy) IS NULL OR SUM(qty_energy) = 0) 
                        THEN (CASE WHEN SUM(qty_volume) IS NULL THEN 0 ELSE SUM(qty_volume) END) 
                        ELSE SUM(qty_energy) END) / ${parameters['conversionFactor']} ${sql.as} qtyUse,                                      
                    SUM(bill_archive.amount_expense) ${sql.as} valueCost,
                    CASE WHEN (SELECT COUNT(em.em_id) FROM em WHERE em.bl_id = bill_archive.bl_id) = 0 THEN MAX(bl.count_occup) 
                        ELSE (SELECT COUNT(em.em_id) FROM em WHERE em.bl_id = bill_archive.bl_id) END ${sql.as} countEm                    
                FROM bill_archive
                LEFT OUTER JOIN bl ON bl.bl_id = bill_archive.bl_id
                WHERE ${parameters['billFilterRestriction']} AND ${parameters['blsRestriction']} AND ${parameters['blFilterRestriction']}
                GROUP BY bill_archive.bl_id, bill_archive.time_period) ${sql.as} bill_archive
            LEFT OUTER JOIN
                (SELECT bill_archive.bl_id, bill_archive.time_period, MAX(bl.area_gross_ext) ${sql.as} area_gross_ext,
                    (CASE WHEN (SUM(qty_energy) IS NULL OR SUM(qty_energy) = 0) 
                        THEN (CASE WHEN SUM(qty_volume) IS NULL THEN 0 ELSE SUM(qty_volume) END) 
                        ELSE SUM(qty_energy) END) / ${parameters['conversionFactor']} ${sql.as} qtyUse,                                      
                    SUM(bill_archive.amount_expense) ${sql.as} valueCost,
                    CASE WHEN (SELECT COUNT(em.em_id) FROM em WHERE em.bl_id = bill_archive.bl_id) = 0 THEN MAX(bl.count_occup) 
                        ELSE (SELECT COUNT(em.em_id) FROM em WHERE em.bl_id = bill_archive.bl_id) END ${sql.as} countEm                   
                FROM bill_archive
                LEFT OUTER JOIN bl ON bl.bl_id = bill_archive.bl_id
                WHERE ${parameters['billTypeRestriction']} AND ${parameters['blsRestriction']} AND ${parameters['blFilterRestriction']}
                GROUP BY bill_archive.bl_id, bill_archive.time_period) ${sql.as} bill_archive_prev
            ON (bill_archive.bl_id = bill_archive_prev.bl_id AND bill_archive.time_period_prev = bill_archive_prev.time_period)
            WHERE ${parameters['valueFieldRestriction']} 
            GROUP BY bill_archive.time_period
        </sql>
        <field name="time_period" table="bill_archive" groupBy="true"/>
        <field name="valueField" table="bill_archive" formula="max" baseField="bill_archive.valueField" dataType="number" decimals="2"/>
        <field name="valueFieldPrev" table="bill_archive" formula="max" baseField="bill_archive.valueFieldPrev" dataType="number" decimals="2"/>
        <field name="valueFieldVar" table="bill_archive" dataType="number" decimals="2">
            <sql dialect="generic">CASE WHEN MAX(bill_archive.valueFieldPrev) = 0 THEN 0 ELSE (MAX(bill_archive.valueField) - MAX(bill_archive.valueFieldPrev))*100/MAX(bill_archive.valueFieldPrev) END</sql>
        </field>
        <sortField name="time_period" table="bill_archive" ascending="true"/>
        <parameter name="conversionFactor" dataType="number" value="1.000000"/>
        <parameter name="billFilterRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="billTypeRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="blFilterRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="blsRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="valueFieldRestriction" dataType="verbatim" value="1=1"/>
        <parameter name="valueField" dataType="verbatim" value="CASE WHEN SUM(bill_archive.area_gross_ext)=0 THEN 0 ELSE (SUM(bill_archive.qtyUse) / SUM(bill_archive.area_gross_ext)) END"/>
        <parameter name="valueFieldPrev" dataType="verbatim" value="CASE WHEN SUM(bill_archive_prev.area_gross_ext)=0 THEN 0 ELSE (SUM(bill_archive_prev.qtyUse) / SUM(bill_archive_prev.area_gross_ext)) END"/>
    </dataSource>
    
    <panel id="energyConsoleChartTwoYear_chart"
        type="htmlChart" 
        controlType="columnLineChart" 
        dataSource="energyConsoleChartTwoYear_chartDs"
        showLabels="false"
        showLegendOnLoad="true"
        showExportButton="true"
        backgroundColor="0xf3f7fa"
        layoutRegion="chart"
        showOnLoad="true">
        <title></title>
        <action id="showHideLegend" imageName="/schema/ab-core/graphics/icons/view/ab-arcgis-legend-16.png">
            <tooltip>Show/Hide Legend</tooltip>
        </action>    
        <action id="maximizeChart" hidden="${view.parameters['maximize'] == true}" imageName="/schema/ab-core/graphics/icons/view/maximize.png">
            <title></title>
            <tooltip>Expand</tooltip>
        </action>
        <event type="onClickItem">
            <command type="callFunction" functionName="selectYearMo" />           
        </event>
        <dataAxisTitle translatable="true">Use ${panel.parameters.billUnit}</dataAxisTitle>
        <groupingAxis table="bill_archive" field="time_period" showLabel="true" labelRotation="45">
            <title></title> 
        </groupingAxis>
        <dataAxis table="bill_archive" field="valueFieldPrev" showLabel="true" showTitle="false" >
            <title>Previous Year</title>
        </dataAxis>
        <dataAxis table="bill_archive" field="valueFieldVar" type="line" showLabel="true" showTitle="true" displayAxis="true">
           <title>Difference (%)</title>
        </dataAxis>
        <dataAxis table="bill_archive" field="valueField" showLabel="true" showTitle="true">
            <title>Selected Year</title>
        </dataAxis>        
    </panel>
    
    <panel type="view" id="energyConsoleChartTwoYear_commonView" file="ab-energy-console-common.axvw" hidden="true"/>
    
    
</view>
