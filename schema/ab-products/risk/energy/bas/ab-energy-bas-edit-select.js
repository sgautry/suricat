var energyBasEditSelectController = View.createController('energyBasEditSelect', {
	name: '',
	data_point_id: null,
	
	afterViewLoad: function() {
		if (View.getOpenerView().parameters && View.getOpenerView().parameters.basBillTypeRestriction) {
			var restriction = View.getOpenerView().parameters.basBillTypeRestriction;
	    	restriction += " AND bas_data_point.bl_id LIKE '" + View.getOpenerView().parameters.blId + "'";
	    	this.energyBasEdit_select.restriction = restriction;
	    	var layout = View.getLayoutManager('mainLayout');
	    	layout.collapseRegion('west');
		}
	},
    
    energyBasEdit_select_onSelect: function(row) {
    	var openerController = View.getOpenerView().controllers.get('energyBasEdit');
    	var meters_to_include = row.getRecord().getValue('bas_data_point.meters_to_include');
		if (meters_to_include) {
			openerController.isVirtual = true;
		} else {
			openerController.isVirtual = false;		
		}
    	openerController.data_point_id = row.getRecord().getValue('bas_data_point.data_point_id');
    	openerController.name = row.getRecord().getValue('bas_data_point.name');    	
    	openerController.energyBasEditTabs.selectTab('energyBasEditProcess');
    }
});

