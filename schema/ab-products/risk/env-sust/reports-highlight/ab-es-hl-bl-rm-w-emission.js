View.extendController('esHighRmEm', reportBaseController, {
    reportViewName: 'ab-es-hl-bl-rm-w-emission-pgrp.axvw',
    /**
     * Show building details for selected building.
     * @param {Object} node - building node.
     */
    showBuildingDetails: function (node) {
        var blId = node.restriction.clauses[0].value;
        var projectId = this.projectTree_panel.lastNodeClicked.getAncestor().getAncestor().data['city.city_id'];
        var nodeRestriction = new Ab.view.Restriction();
        nodeRestriction.addClause('activity_log.bl_id', blId, '=');
        nodeRestriction.addClause('activity_log.project_id', projectId, '=');
        nodeRestriction.addClauses(this.filterPanel.getRecord().toRestriction());
        this.refreshReportPanel(nodeRestriction);
        this.showBuilding(new Ab.view.Restriction({'bl.bl_id': blId}));
    },
    /**
     * Show details for selected floor.
     * @param {Object} node - floor node.
     */
    showFloorDetails: function (node) {
        this.showPanels(false, true, true);
        var currentNode = this.projectTree_panel.lastNodeClicked;
        var projectId = currentNode.data['rm.city_id'];
        var nodeRestriction = new Ab.view.Restriction();
        nodeRestriction.addClauses(node.restriction);
        nodeRestriction.removeClause('rm.city_id');
        nodeRestriction.removeClause('rm.rm_id');
        nodeRestriction.addClause('activity_log.project_id', projectId, '=', true);
        nodeRestriction.addClauses(this.filterPanel.getRecord().toRestriction());
        this.refreshReportPanel(nodeRestriction);
        var blId = currentNode.data['rm.bl_id'],
            flId = currentNode.data['rm.fl_id'],
            drawingName = currentNode.data['rm.raw_dwgname'];
        this.displayDrawing(blId, flId, drawingName);
    }
});
/**
 * Show building details.
 * @param {Object} node - building node.
 */
function showBuilding(node) {
    var controller = View.controllers.get('esHighRmEm');
    controller.showBuildingDetails(node);
}
/**
 * Show floor details.
 * @param {Object} node - floor node.
 */
function showFloor(node) {
    var controller = View.controllers.get('esHighRmEm');
    controller.showFloorDetails(node);
}
