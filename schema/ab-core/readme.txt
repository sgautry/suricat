
___________________________________
The webapps/archibus/ab-core Folder

This webapps/archibus/ab-core folder and its sub-folders contain files
internal to the operation of the ARCHIBUS product.  These files are
open for review, but are not intended to be modified in the typical
deployment and modifications to these files are not supported.

This ab-core folder is required for views that use the 2.0 view file format.
