/**
 * Created by Meyer on 10/18/2015.
 *
 * ab-pgnav-editor-view.js
 * Support for the home page editor.
 * Manage the viewable HTML representing the home page and its editing controls
 */
/**
 * Namespace for the home page editor JS classes.
 */
Ab.namespace('homepage');

/**
 * View utilities for the Home Page Editor.
 * Singleton class of functions that manipulate the HTML in the document.
 */
Ab.homepage.EditorView = new (Base.extend({

        // @begin_translatable
        z_LABEL_TITLE: 'Title',
        z_LABEL_EDIT: 'Edit',
        z_LABEL_EDIT_PROPERTIES: 'Edit Properties',
        z_LABEL_EDIT_TASKS: 'Edit Tasks',
        z_LABEL_EDIT_METRICS: 'Edit Metrics',
        z_LABEL_BACK_TO_TOP: 'Back to Top',
        z_LABEL_ROW: 'Row',
        z_LABEL_NEW_PANEL: 'New Panel',
        z_LABEL_NEW_ROW: 'New Row',
        z_LABEL_PROCESS: 'Process',
        z_LABEL_CONTROL_TYPE: 'Control Type',
        z_LABEL_PROPERTIES: 'Properties',
        z_LABEL_TASKS_ASSIGNED: 'Tasks Assigned',
        z_LABEL_DEFINE_SCORECARD: 'Define Scorecard',
        z_LABEL_DEFINE_METRIC: 'Define Metric',
        z_MESSAGE_CONFIRM_DIVIDER_DELETE: 'Do you really want to delete this page divider?',
        z_MESSAGE_CONFIRM_ROW_DELETE: 'Do you really want to delete this whole row?',
        z_MESSAGE_CONFIRM_PANEL_DELETE: 'Do you really want to delete this panel?',
        z_MESSAGE_ASSIGN_ACTIVITY_PROCESS: 'Assign an activity and process.',
        z_MESSAGE_ASSIGN_CONTROL_TYPE: 'Assign a type and control type.',
		z_MESSAGE_ASSIGN_PANEL_TYPE: 'All panels must have a defined type.',
		z_MESSAGE_PUBLISHING_ERROR: 'Publishing error',
        z_TOOLTIP_ADDITIONAL_TASKS: 'Additional Tasks',
        z_TOOLTIP_DIVIDER_PROPERTIES: 'Page Divider Properties',
        z_TOOLTIP_DELETE_ROW: 'Delete row',
        z_TOOLTIP_DUPLICATE_PANEL: 'Duplicate panel',
        z_TOOLTIP_DELETE_PANEL: 'Delete panel',
        z_TOOLTIP_VIEWNAME_CHART_EDIT: 'A chart based on viewname and datasource is completely editable in the properties form.',
        z_TOOLTIP_FAVORITES_EDIT: 'A favorites panel is completely editable in the properties form.',
        z_TOOLTIP_MOVE_UP: 'Move up',
        z_TOOLTIP_MOVE_DOWN: 'Move down',
        z_TOOLTIP_HALF_HEIGHT: 'Half height',
        z_TOOLTIP_THREE_QUARTER_HEIGHT: 'Three quarter height',
        z_TOOLTIP_FULL_HEIGHT: 'Full height',
        z_TOOLTIP_ADD_PANEL: 'Add a new panel',
        z_TOOLTIP_ADD_ROW: 'Add a new row',
        z_TOOLTIP_ADD_PAGE_DIVIDER: 'Add page divider',
        z_TOOLTIP_DELETE_DIVIDER: 'Delete divider',
        // @end_translatable

        /**
         * Return the HTML to display a page model.
         *
         * @param descriptorModel
         */
        createDisplayFromModel: function (descriptorModel) {
            var displayHtml = '<div class="nav-pages"><div id="editorPage" class="editor-page">';
            var blocks = descriptorModel.abstractNavBlocks;
            var blockCount = blocks.length;

            for (var i = 0; i < blockCount; i++) {
                var block = blocks[i];
                if ("page-divider" === block.type) {
                    displayHtml += this.getPageDividerHtml(block);
                }
                else if ("row" === block.type) {
                    displayHtml += this.getPageRowHtml(block, this.getNextAbstractBlockIndex());
                }
                else if ("rule" === block.type) {
                    // TODO
                    ///displayHtml += getHorizontalRuleHtml(block);
                }
            }

            displayHtml += this.getNewRowControlHtml() +
                '</div></div>';

            return displayHtml;
        },

        /**
         * Return the HTML for one page divider element.
         *
         * @param block
         * @returns {string}
         */
        getPageDividerHtml: function (block) {
            // add data attributes including title, backgroundshading -- all editable properties.
            var backgroundshading = block.attributes.backgroundshading ? block.attributes.backgroundshading : '';

            return '<div class="page-break-banner"  id="pageDivider_' + block.index + '" data-title="' + block.attributes.title +
                '" data-backgroundshading="' + backgroundshading + '">' +
                '<div class="page-break-palette">' +
                '<button type="button" class="page-divider-properties" onmouseup="Ab.homepage.EditorController.editPageDividerProperties(this);">' +
                '<span>' + getLocalizedString(this.z_LABEL_EDIT) + '</span></button>' +
                '<a class="page-divider-delete bucket-image-delete" onmouseup="Ab.homepage.EditorController.deletePageDivider(this)"' +
                'title="' + getLocalizedString(this.z_TOOLTIP_DELETE_DIVIDER) + '"/>' +
                '</div>' +
                '<div class="page-break-archibus">' +
                '<div class="page-break-logo"></div>' +
                '<span>' + block.attributes.title + '</span>' +
                '</div>' +
                '<a class="page-top-link" onClick="goToTop();">' + getLocalizedString(this.z_LABEL_BACK_TO_TOP) + '</a>' +
                '</div>';
        },

        /**
         * Return the HTML for one whole row, including row palette & panels.
         *
         * @param rowBlock
         * @param rowIndex
         * @returns {string}
         */
        getPageRowHtml: function (rowBlock, rowIndex) {
            var rowHeight = rowBlock.attributes.height;
            var displayHtml = '<div class="page-row ' + rowHeight + '-height" id="pageRow_' + rowIndex + '">';

            // row palette
            displayHtml += this.getRowPaletteHtml({
                index: rowIndex,
                attributes: {height: rowHeight}
            });

            // panels
            var columnSpan = 0;
            var panelCount = rowBlock.panels.length;
            for (var j = 0; j < panelCount; j++) {
                displayHtml += this.getPanelHtml(rowBlock.panels[j]);
                columnSpan += Ab.homepage.EditorController.getPanelColumnSpan(rowBlock.panels[j]);
            }

            // new panel button
            if (rowBlock.panels.length < 4 && columnSpan < 4) {
                displayHtml += this.getNewPanelControlHtml();
            }
            displayHtml += '</div>';

            return displayHtml;
        },

        /**
         * Return the HTML for a row palette.
         *
         * @param panelConfig
         * @returns {string}
         */
        getRowPaletteHtml: function (panelConfig) {
            var attributes = panelConfig.attributes;
            var index = panelConfig.index;

            var rowPaletteHtml = '<div id="rowPalette_' + index + '" class="row-palette"';
            rowPaletteHtml += this.getElementDataAttributesHtml(attributes);
            rowPaletteHtml += ' oncontextmenu="return false;">';
            rowPaletteHtml += '<h2 class="process-title">' + getLocalizedString(this.z_LABEL_ROW) + index +
                '<a class="row-palette-title-menu bucket-image-delete" onmouseup="Ab.homepage.EditorController.deleteRow(this)" ' +
                'title="' + getLocalizedString(this.z_TOOLTIP_DELETE_ROW) + '"></a>' +
                '</h2>' +
                '<div class="bucket-wrapper" id="wrapper_rowPalette_' + index + '">' +
                '<table class="row-palette-controls">' +
                '<tr>' +
                '<td class="row-palette-shift"><a class="row-move-up" title="' + getLocalizedString(this.z_TOOLTIP_MOVE_UP) +
                '" onmouseup="Ab.homepage.EditorController.shiftRowUp(this)" ></a></td>' +
                '<td><a class="row-move-down" title="' + getLocalizedString(this.z_TOOLTIP_MOVE_DOWN) +
                '" onmouseup="Ab.homepage.EditorController.shiftRowDown(this)" /></td>' +
                '</tr>';
            rowPaletteHtml +=
                '<tr><td>&nbsp;</td></tr>' +
                '<tr><td colspan="2"><a class="row-height row-height-half" title="' + getLocalizedString(this.z_TOOLTIP_HALF_HEIGHT) +
                '" onmouseup="Ab.homepage.EditorController.setRowHeight(this,' + index + ',\'half\')"></a></td></tr>' +
                '<tr><td colspan="2"><a class="row-height row-height-three-quarter" title="' + getLocalizedString(this.z_TOOLTIP_THREE_QUARTER_HEIGHT) +
                '" onmouseup="Ab.homepage.EditorController.setRowHeight(this,' + index + ',\'three-quarter\')"></a></td></tr>' +
                '<tr><td colspan="2"><a class="row-height row-height-full" title="' + getLocalizedString(this.z_TOOLTIP_FULL_HEIGHT) +
                '" onmouseup="Ab.homepage.EditorController.setRowHeight(this,' + index + ',\'full\')"></a></td></tr>' +
                '</table>';
            rowPaletteHtml += '<table class="row-palette-divider-button">' +
                '<tr><td>' +
                '<a class="page-divider-button" title="' + getLocalizedString(this.z_TOOLTIP_ADD_PAGE_DIVIDER) +
                '" onmouseup="Ab.homepage.EditorController.addPageDivider(this)"></a>' +
                '</td></tr>' +
                '</table>';
            rowPaletteHtml += '</div>' +
                '</div>';

            return rowPaletteHtml;
        },

        /**
         * Return a string of HTML data attributes for including within an element tag.
         *
         * @param attributes
         * @returns {string}
         */
        getElementDataAttributesHtml: function (attributes) {
            var dataAttributesHtml = '';
            for (var attributeName in attributes) {
                if (attributes.hasOwnProperty(attributeName)) {
                    dataAttributesHtml += ' data-' + attributeName + '="' + attributes[attributeName] + '"';
                }
            }

            return dataAttributesHtml;
        },

        /**
         * Return the HTML for displaying one panel
         *
         * @param panel
         * @returns {string}
         */
        getPanelHtml: function (panel) {
            var panelHtml = '';
            var panelType = panel.attributes.type;

            if ("process" === panelType) {
                panelHtml = this.getProcessPanelHtml(panel);
            }
            else if ("app-specific" === panelType) {
                panelHtml = this.getAppSpecificPanelHtml(panel);
            }
            else if ("favorites" === panelType) {
                panelHtml = this.getFavoritesPanelHtml(panel);
            }
			else {
				panelHtml = this.getEmptyPanelHtml();
			}

            return panelHtml;
        },



        /**
         * Return the HTML for one process panel in the model.
         *
         * @param panel
         * @returns {string}
         */
        getProcessPanelHtml: function (panel) {
            var attributes = panel.attributes;
            var activityId = attributes.activityid || attributes.activity_id ;
            var processId = attributes.processid || attributes.process_id;
            var idSuffix = activityId + '_' + getCamelCasedString(processId) + '_b_' + this.getNextPanelIndex();

            var panelHtml = '<div id="bucket_' + idSuffix + '" class="bucket-process"' +
                this.getElementDataAttributesHtml(attributes) +
                ' oncontextmenu="return false;">' +
                this.getPanelTitleHtml(attributes);

            panelHtml += '<div class="bucket-wrapper process-bucket" id="wrapper_' + idSuffix + '">' +
                this.getProcessPanelPropertiesTable(idSuffix, activityId, processId, attributes.controltype) +
                this.getPanelActionsTable(attributes) +
                '</div></div>';

            return panelHtml;
        },

        /**
         * Return the abstractBlock counter,
         * after incrementing and storing the new value back to the document data attribute.
         * Abstract blocks include rows that can contain panels; page dividers; horizontal rules.
         *
         * @returns {*}
         */
        getNextAbstractBlockIndex: function() {
            var pageAttributes = jQuery('#pageAttributes');
            var maxBlockIndex = jQuery(pageAttributes).data('abstractBlockCounter');
            jQuery(pageAttributes).data('abstractBlockCounter', ++maxBlockIndex);

            return maxBlockIndex;
        },

        /**
         * Return the panelIndex counter,
         * after incrementing and storing the new value back to the document data attribute.
         * @returns {*}
         */
        getNextPanelIndex: function() {
            var pageAttributes = jQuery('#pageAttributes');
            var maxPanelIndex = jQuery(pageAttributes).data('pagePanelCounter');
            jQuery(pageAttributes).data('pagePanelCounter', ++maxPanelIndex);

            return maxPanelIndex;
        },

        /**
         * Return the HTML for one app-specific panel.
         * @param panel
         * @returns {string}
         */
        getAppSpecificPanelHtml: function (panel) {
            var attributes = panel.attributes;
            var idSuffix = getCamelCasedString(attributes.title) + '_b_' + this.getNextPanelIndex();
            // TODO handle triple-wide or full-width if we allow
            var columnSpan = attributes.columnspan;
            var panelCssClass = (columnSpan && (columnSpan === 2 || columnSpan === "2")) ? 'bucket-process double-wide' : 'bucket-process';

            var panelHtml = '<div id="bucket_' + idSuffix + '" class="' + panelCssClass + '"' +
                this.getElementDataAttributesHtml(attributes) +
                ' oncontextmenu="return false;">' +
                this.getPanelTitleHtml(attributes);

            panelHtml += '<div class="bucket-wrapper app-specific-bucket" id="wrapper_' + idSuffix + '">' +
                this.getAppSpecificPanelPropertiesTable(idSuffix, attributes) +
                this.getPanelActionsTable(attributes) +
                '</div></div>';

            return panelHtml;
        },

        /**
         * Return the HTML for one favorites panel.
         *
         * @param panel
         * @returns {string}
         */
        getFavoritesPanelHtml: function (panel) {
            var panelIndex = this.getNextPanelIndex();
            var panelHtml = '<div id="homeFavoritesBucket_f_' + panelIndex + '" class="bucket-process" ' +
                this.getElementDataAttributesHtml(panel.attributes) +
                'oncontextmenu="return false;">';
            panelHtml += this.getPanelTitleHtml(panel.attributes);

            panelHtml += '<div class="bucket-wrapper favorites-bucket" id="wrapper_favorites_' + panelIndex + '">' +
                this.getFavoritesPanelPropertiesTable() +
                this.getPanelActionsTable(panel.attributes) +
                '</div></div>';

            return panelHtml;
        },

        /**
         * Return the HTML for a panel title ( a panel of any type )
         * @param attributes
         * @returns {string}
         */
        getPanelTitleHtml: function (attributes) {
            var attrTitle = attributes.title;
            var displayTitle = attrTitle;

            // TODO make the real calculation (font, width, etc. simulation and removal)
            if (attrTitle.length > 28 && (!attributes.columnspan || parseInt(attributes.columnspan, 10) < 2)) {
                displayTitle = attrTitle.substr(0,27) + '...';
            }

			// hide the duplicate button unless its a panel worth duping
			var disableDuplication = ('process' === attributes.type || 'app-specific' === attributes.type) ? 
				'' :
                ' style="display:none;"';

            var titleHtml = '<h2 class="process-title" title="' + attributes.tooltip + '">' +
                '<span class="bucket-title" title="' + attrTitle + '">' + displayTitle + '</span>' +
                '<table class="bucket-title-menu">' +
                '<tr><td><a class="bucket-image-duplicate" title="' + getLocalizedString(this.z_TOOLTIP_DUPLICATE_PANEL) + 
				'" onmouseup="Ab.homepage.EditorController.duplicatePanel(this)"' +
				disableDuplication + '/>' + '</td><td>';

            titleHtml += '<a class="bucket-image-delete" title="' + getLocalizedString(this.z_TOOLTIP_DELETE_PANEL) +
                '" onmouseup="Ab.homepage.EditorController.deletePanel(this)"/>' +
                '</td></tr>' +
                '</table></h2>';

            return titleHtml;
        },

        /**
         * Return the HTML table showing a process panel's activity + process ids.
         * @param idSuffix
         * @param activityId
         * @param processId
         * @param controltype
         * @returns {string}
         */
        getProcessPanelPropertiesTable: function (idSuffix, activityId, processId, controltype) {
            var activity = activityId ? activityId : "";
            var process = processId ? processId : "";
            return '<table id="table_' + idSuffix + '" class="bucket-properties-table">' +
                '<tr> <td><b>' + getLocalizedString(this.z_LABEL_PROCESS) + ': </b></td></tr>' +
                '<tr><td>' + activity + '</td></tr>' +
                '<tr><td>' + process + '</td></tr>' +
                '<tr><td>&nbsp;</td></tr>' +
                this.getPanelTypeImageHTML(controltype) +
                '</table>';
        },

        /**
         * Return the HTML table showing the displayed app-specific properties for the panel.
         * @param idSuffix
         * @param attributes
         * @returns {string}
         */
        getAppSpecificPanelPropertiesTable: function (idSuffix, attributes) {
            var tableHtml = '<table id="table_' + idSuffix + '" class="bucket-properties-table">' +
                ' <tr> <td><b>' + getLocalizedString(this.z_LABEL_CONTROL_TYPE) + ':</b></td></tr>' +
                '<tr><td>' + attributes.controltype + '</td> </tr>';

            if (attributes.scorecard) {
                tableHtml += '<tr>' +
                        ///'<td><b>Scorecard: </b></td></tr>' +
                    '<tr><td>' + attributes.scorecard + '</td> </tr>';
            }
            if (attributes.metricname) {
                tableHtml += '<tr>' +
                        ///'<td><b>Metric Name: </b></td></tr>' +
                    '<tr><td>' + attributes.metricname + '</td> </tr>';
            }
            if (attributes.granularitytitle) {
				tableHtml += //'<tr>' +
					///'<td><b>Granularity: </b></td></tr>' +
                    '<tr><td>' + attributes.granularitytitle;
				tableHtml += '</td></tr>';
            }

            if (!attributes.scorecard && !attributes.metricname && !attributes.granularitytitle) {
			    tableHtml += '<tr><td>&nbsp;</td></tr>';
			}

            tableHtml += this.getPanelTypeImageHTML(attributes.controltype);
            tableHtml += '</table>';

            return tableHtml;
        },

        /**
         * Return the displayable contents of a favorites panel.
         * @returns {string}
         */
        getFavoritesPanelPropertiesTable: function () {
            return '<div class="favorites-drop-target">' +
                '<span class="favorites-add-target"></span>' +
                '<span class="favorites-delete-target">&nbsp;</span>' +
                '</div>' +
                '<div class="favorites"></div>';
        },

        /**
         * Return a table row holding the appropriate sample app-specific control image.
         * @param controlType
         * @returns {string}
         */
        getPanelTypeImageHTML: function (controlType) {
            var panelImageHtml = '<tr><td>';

            if (!controlType || '' === controlType) {
                panelImageHtml += '<a class="bucket-thumbnail-process" ></a>';
            }
            else if (controlType === 'processMetrics') {
                panelImageHtml += '<a class="bucket-thumbnail-metrics-scorecard" ></a>';
            }
            else if (controlType === 'horizontalBarChart') {
                panelImageHtml += '<a class="bucket-thumbnail-bar-chart" ></a>';
            }
            else if (controlType === 'alertsList') {
                panelImageHtml += '<a class="bucket-thumbnail-alerts-list" ></a>';
            }
            else if (controlType === 'metricsChart') {
                panelImageHtml += '<a class="bucket-thumbnail-metric-chart" ></a>';
            }
            else if (controlType === 'pgnav-map') {
                panelImageHtml += '<a class="bucket-thumbnail-map" ></a>';
            }
            // TODO and favorites?

            panelImageHtml += '</td></tr>';

            return panelImageHtml;
        },

        /**
         * Return the HTML of the two control elements (buttons) accessing
         * the panel properties and associated tasks/metrics/etc.
         *
         * @param attributes
         * @returns {string}
         */
        getPanelActionsTable: function (attributes) {
            var disableAssociateButton = '';
            var associateTooltip = '';
            var labelText = "app-specific" === attributes.type ?
                [getLocalizedString(Ab.homepage.EditorView.z_LABEL_EDIT_PROPERTIES), getLocalizedString(Ab.homepage.EditorView.z_LABEL_EDIT_METRICS)]:
                [getLocalizedString(Ab.homepage.EditorView.z_LABEL_EDIT_PROPERTIES), getLocalizedString(Ab.homepage.EditorView.z_LABEL_EDIT_TASKS)];

            // a bar chart based on viewname, datasourceId, etc. is editable completely through the properties form.
            if ('horizontalBarChart' === attributes.controltype && attributes.viewname) {
                disableAssociateButton = ' disabled="disabled"';
                associateTooltip = ' title="' + getLocalizedString(this.z_TOOLTIP_VIEWNAME_CHART_EDIT) + '"';
            }
            else if ('favorites' === attributes.type) {
                disableAssociateButton = ' disabled="disabled"';
                associateTooltip = ' title="' + getLocalizedString(this.z_TOOLTIP_FAVORITES_EDIT) + '"';
            }
			else if ('' === attributes.type) {
                disableAssociateButton = ' disabled="disabled"';
			}

            return '<table class="bucket-properties-buttons">' +
                '<tr>' +
                '<td><button type="button" class="prop-button" onclick="Ab.homepage.EditorController.editPanelProperties(this);">' +
                '<span>' + labelText[0] + '</span></button></td>' +
                '<td class="spacer">&nbsp;</td>' +
                '<td><button type="button" class="assoc-button" onclick="Ab.homepage.EditorController.editPanelAssociate(this);"' +
                disableAssociateButton + associateTooltip + '><span>' + labelText[1] + '</span></button></td>' +
                '</tr>' +
                '</table>';
        },

        /**
         * Return the HTML for an initially empty panel.
         *
         * @returns {string}
         */
        getEmptyPanelHtml: function () {
            var attributes = {
                    title: getLocalizedString(this.z_LABEL_TITLE),
                    type: '',
                    activityId: '',
                    processId: '',
                    scorecard: '',
                    metricname: '',
                    granularity: '',
                    granularitytitle: '',
                    columnspan: 1,
                    tooltip: ''
            };
            var idSuffix = 'b_' + this.getNextPanelIndex();

            var panelHtml = '<div id="bucket_' + idSuffix + '" class="bucket-process"' +
                this.getElementDataAttributesHtml(attributes) +
                ' oncontextmenu="return false;">' +
                this.getPanelTitleHtml(attributes);

            panelHtml += '<div class="bucket-wrapper process-bucket" id="wrapper_' + idSuffix + '">' +
				'<table id="table_' + idSuffix + '" class="bucket-properties-table">' +
                '<tr> <td><b>' + attributes.type + ' </b></td></tr>' +
                '<tr><td>' + attributes.activityId + '</td></tr>' +
                '<tr><td>' + attributes.processId + '</td></tr>' +
                '</table>' +
				this.getPanelActionsTable(attributes) +
                '</div></div>';

            return panelHtml;
        },

        /**
         * Return the HTML for the 'New Panel' control
         * @returns {string}
         */
        getNewPanelControlHtml: function () {
            return '<div id="newBucketControlContainer_' + this.getNextPanelIndex() + '"  class="new-panel-container">' +
                '<button type="button" title="' + getLocalizedString(this.z_TOOLTIP_ADD_PANEL) +
                '" onclick="Ab.homepage.EditorController.addEmptyPanelToRow(this)">' +
                '<span>' + getLocalizedString(this.z_LABEL_NEW_PANEL) + '</span>' +
                '</button>' +
                '</div>';
        },

        /**
         * Return the HTML for the 'New Row' control
         * @returns {string}
         */
        getNewRowControlHtml: function () {
            return '<div id="newRowControlContainer" class="new-row-container">' +
                '<button type="button" title="' + getLocalizedString(this.z_TOOLTIP_ADD_ROW) +
                '" onclick="Ab.homepage.EditorController.addNewRow();" >' +
                '<span>' + getLocalizedString(this.z_LABEL_NEW_ROW) + '</span>' +
                '</button>' +
                '</div>';
        },

        /**
         * Set the display to hide or show the new row control.
         * @param show
         */
        showNewRowControl: function (show) {
            var displayStyle = show ? 'block' : 'none';
            jQuery('#newRowControlContainer').css('display', displayStyle);
        },

        /**
         * Set the display to hide or show the new panel control.
         * @param show
         */
        showNewPanelControl: function (show) {
            var displayStyle = show ? 'block' : 'none';
            jQuery('.new-panel-container').css('display', displayStyle);
        }
    })
);

