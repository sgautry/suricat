/**
 * Configurations of the grid. Installing the main parameters of the grid, loading of columns in the grid, loading data.
 */
var DataGridConfigurator = Base.extend(
    {

        _configUser: null,
        _gridConfiguration: null,
        _dataSource: null,

        /**
         * Constructor
         * @param config: the grid configuration specified by the user
         */
        constructor: function (config) {
            this._configUser = config;

            if(!this._configUser.container)
                this._configUser.container = DataGridConfigurator.defaultGridContainer;

            if (!this._configUser.columns)
                return null;

            this._gridConfiguration = this._createConfiguration();
        },

        /**
         * Get the grid configuration
         * @returns {object}
         */
        getConfiguration: function () {
            return this._gridConfiguration;
        },

        /**
         * Get the data source
         * @returns {object DataGridSource}
         */
        getDataSource: function () {
            return this._dataSource;
        },

        /**
         *  To create the grid configurations:executes initialization functions of selection of all rows in the group,
         *  the setting of parameters to filter, sort and summary and custom customize of data
         * @returns the grid configurations.
         * @private
         */
        _createConfiguration: function () {
            this._dataSource = new DataGridSource(this._configUser);
            var gridSelection = new GridRowsSelection(this._configUser);
            var gridColumns = new DataGridColumnConfigurator(this._configUser, this._dataSource, gridSelection);

            
            var scope = this;
            var result = {
                dataSource: this._dataSource.getConfigurationDataSourceGrid(),
                remoteOperations: {
                    paging: true,
                    summary: true,
                    filtering: true,
                    sorting: true,
                    grouping: true
                },
                groupPanel: { visible: false },
                pager: { showNavigationButtons: true },
                columnAutoWidth: true,
                columns: gridColumns.getColumns(),
                onInitialized: function (e) {
                    var grid = e.component;
                    /*if (scope._configUser.multipleSelectionEnabled) {
                        gridSelection.setConfig(grid, scope._dataSource);
                        grid.on('rowClick', gridSelection.onRowClick);
                        grid.on('selectionChanged', gridSelection.onGridSelectionChanged);
                        grid.on('rowCollapsing', gridSelection.onRowCollapsing);
                        grid.on('rowCollapsed', gridSelection.onRowCollapsed);
                        grid.on('rowExpanding', gridSelection.onRowExpanding);
                        grid.on('rowExpanded', gridSelection.onRowExpanded);
                        grid.option('customizeColumns', gridSelection.onCustomizeColumns);
                    }*/

                    scope._eventRegistration(grid);
                },
                onCellPrepared: function (option) {
                    scope._callOnCellPrepared(option, this);
                    gridSelection.overrideFunctionHeaderCheckbox(option);
                    scope._changeTextAlingForNumOrInt(option);
                    scope._changeBackgroundColor(option);
                    scope._changeTreeButton(option);

                },
                onContentReady: function (option) {
                    var afterRefresh = this.option('afterRefresh'+ 'Def');
                    if (afterRefresh) {
                        afterRefresh.call(null);
                    }
                },
                onRowPrepared: function (option) {
                    /*jQuery(window).on('resize',function(){
                        option.component.updateDimensions();
                    });*/
                    
                    var rowPrepared = this.option('onRowPrepared'+ 'Def');
                    if (rowPrepared) {
                        rowPrepared.call(null, option);
                    }
                }
            };

            if (this._configUser.multipleSelectionEnabled) {
                result.selection = gridSelection.getSelectionConfiguration();
            }


            result.summary = this._configureCategoryGrid();
            result.paging = this._configurePager();
            result.filterRow = this._configureFilterPanel();
            result.rowAlternationEnabled = this._configureRowAlternationEnabled();
            result.allowColumnReordering = this._configureColumnReordering();
            result.allowColumnResizing = this._configureColumnResizing();
            result = this._configureSize(result);
            result = this._configureSorting(result);
            result = this._configureGrouping(result);
            result = this._configureLoadPanel(result);

            return result;
        },

        /**
         * Call custom event "OnCellPrepared"
         * @param option: information about the cell. Contains properties that are specific to the grid control implementation.
         * @param grid: the grid object
         * @private
         */
        _callOnCellPrepared: function (option, grid) {
            if (option.rowType == 'data') {
                var event = grid.option('onCellPrepared' + 'Def');
                if (event) {
                    var row = this._dataSource.getAbRow(option.data.record);
                    event.call(null, option, row);
                }
            }
            if (option.rowType == 'group') {
                var event = grid.option('onGroupCellPrepared' + 'Def');
                if (event) {
                    var row = this._dataSource.getAbRow(option.data.record);
                    event.call(null, option, row);
                }
            }
            if (option.rowType == 'header') {
                var event = grid.option('onHeaderCellPrepared' + 'Def');
                if (event) {
                    event.call(null, option);
                }
                this._configureColumnMinWidth(option);
            }
        },

        _changeTextAlingForNumOrInt: function (option) {
            if (option.column.userType == 'number' || option.column.userType == 'integer') {
                if (option.rowType == 'group' || option.rowType == 'totalFooter'){
                    option.cellElement.find('div').css({'text-align': 'right'});
                }
                if (option.rowType == 'data' ) {
                    option.cellElement.css({'text-align': 'right'});
                }
            }
        },

        _changeBackgroundColor: function (option) {
            if (option.column['backgroundColor']) {
                if (option.rowType == 'header')
                    option.cellElement.css({'background-color': option.column['backgroundColor']});
            }
        },

        _changeTreeButton: function (option) {
            if (option.rowType == 'group' && !option.column.dataField) {
                var cell = option.cellElement;
                var image = DataGridConfigurator.IMG_TREE_OPEN;
                if (!option.row.isExpanded) {
                    image = DataGridConfigurator.IMG_TREE_CLOSE;
                }
                var treeElement = jQuery('<img src="' + image + '"/>');
                treeElement.css({
                    'padding-left': '10px',
                    'padding-top': '5px',
                    cursor: 'pointer'
                });

                if (option.data.isContinuation) {
                    treeElement.on('dxclick', function () {
                        option.component.collapseRow([option.data.key]);
                    });
                }

                cell.html('');
                cell.append(treeElement);
                cell.css('width', '50px');
            }

        },

        /**
         * To perform a configuration category for groups and total value for the grid in the specified column
         * @returns {{groupItems: Array, totalItems: Array}}
         * @private
         */
        _configureCategoryGrid: function () {
            var result = {
                groupItems: [],
               totalItems: []
            };

            var columns = this._configUser.columns;

            for(var index = 0; index < columns.length; index++) {
                var column = columns[index];
                if (column['showTotals']) {
                    var item = {
                        column: DataGridSource.configGridFieldName(this._configUser.dataSource, column.fieldName),
                        summaryType: column['totalsFunction'],
                        alignByColumn: true,
                        displayFormat: '{0}'
                    };
                    result.groupItems.push(item);
                    result.totalItems.push(item);
                }

                if (!column['showTotals'] && this._configUser.showFooter && result.totalItems.length == 0) {
                    if (column.fieldName)
                        result.totalItems.push({
                            column: DataGridSource.configGridFieldName(this._configUser.dataSource, column.fieldName)
                        });
                }

                if (column['showWidgetInGroup']) {
                    var columnID = '';
                    switch (typeof column.fieldName) {
                        case 'object':
                            columnID = column.fieldName.name;
                            break;
                        default:
                            columnID = DataGridSource.configGridFieldName(this._configUser.dataSource, column.fieldName);
                            break;
                    }
                    var item = {
                        column: columnID,
                        alignByColumn: true
                    };
                    result.groupItems.push(item);
                    result.totalItems.push(item);
                }

                if (column['type'] == 'scheduler') {
                    var item = {
                        column: column.fieldName,
                        alignByColumn: true
                    };
                    result.groupItems.push(item);
                    result.totalItems.push(item);
                }
            }
            return result;
        },

        /**
         * To perform a configuration size for grid
         * @param config: the grid configuration specified by the user
         * @returns {*}
         * @private
         */
        _configureSize: function (config) {
            if (this._configUser['height']) {
                config.height = this._configUser['height'];
            }
            if (this._configUser['width']) {
                config.width = this._configUser['width']
            }
            return config;
        },

        _configurePager: function () {
            var pageSize = this._configUser['pageSize'];
            if (!pageSize || pageSize == 0) {
                pageSize = 100000;
                this._configUser['pageSize'] = pageSize;
            }
            return { pageSize:  pageSize};
        },

        _configureFilterPanel: function () {
            if (this._configUser['showFilter'] == false) {
                return { visible: false };
            }
            return { visible: true };
        },

        _configureSorting: function (config) {
            if (this._configUser['showSort'] == false) {
                config.sorting = { mode: 'none' };
            } else {
                config.sorting = { mode: 'multiple' };
            }
            return config;
        },

        _configureGrouping: function (config) {
            if (this._configUser['autoExpandAll'] == false) {
                config.grouping = {autoExpandAll: false};
            }
            return config;
        },

        _configureRowAlternationEnabled: function () {
            if (this._configUser['rowAlternationEnabled'] == false) {
                return false;
            }
            return true;
        },

        _configureColumnReordering: function () {
            if (this._configUser['columnReordering'] == false) {
                return false;
            }
            return true;
        },

        _configureColumnResizing: function () {
            if (this._configUser['columnResizing'] == false) {
                return false;
            }
            return true;
        },

        _configureColumnMinWidth: function (option) {
            var column = option.column;
            if (column.minWidth) {
                var cell = option.cellElement;
                if (column.groupIndex >= 0 && column.groupIndex != null) {
                    cell.css('minWidth', 30);
                } else {
                    cell.css('minWidth', column.minWidth);
                }
            }
        },

        _configureLoadPanel: function (config) {
            if (this._configUser['loadPanelText']) {
                config.loadPanel = {text: this._configUser['loadPanelText']};
            }
            return config;
        },

        /**
         * To register custom events in the grid
         * @param grid: the grid object
         * @private
         */
        _eventRegistration: function (grid) {
            for (key in this._configUser.events) {
                grid.option(key + 'Def', this._configUser.events[key]);
            }
        }


    },{
        defaultPageSize: 100,
        defaultGridContainer: 'projectsGridContainer',
        IMG_TREE_OPEN: '/archibus/schema/ab-core/controls/grid/treeOpen.jpg',
        IMG_TREE_CLOSE: '/archibus/schema/ab-core/controls/grid/treeClose.jpg'
    }
);
