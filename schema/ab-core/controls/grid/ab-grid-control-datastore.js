/**
 * The data store contains methods for loading the data into the grid
 */
var DataGridSource =  Base.extend({

    _store: null,
    _config: null,
    _groupID: null,
    _pageData: null,
    _groupData: null,
    _restriction: null,
    _updatedData: false,
    _dataSourceKey: null,
    _dataTotalCount: null,
    _viewDataSource: null,
    _totalValueFields: null,

    /**
     * Constructor
     * @param config: the grid configuration specified by the user
     */
    constructor: function (config) {
        this._initializationFields(config);
        if (!String.prototype.includes) {
            String.prototype.includes = function(search, start) {
                if (typeof start !== 'number') {
                    start = 0;
                }

                if (start + search.length > this.length) {
                    return false;
                } else {
                    return this.indexOf(search, start) !== -1;
                }
            };
        }

        var scope = this;

        this._store = new DevExpress.data.CustomStore({
            load: function (loadOptions) {
                var deferred = jQuery.Deferred();
                if (scope._config.showOnLoad == false) {
                    deferred.resolve([]);
                    return deferred.promise();
                }

                var result = [],
                    dataTotalCount = 0;

                scope._restriction = null;

                if ((loadOptions.group != null && loadOptions.sort) || loadOptions.group != null) {
                    scope._groupID = loadOptions.group[0].selector;
                    result = scope._getGroupsData(loadOptions.filter, loadOptions.group[0].selector);
                    deferred.resolve(result.data, {summary: result.totalSummary});
                }
                else {
                    if (loadOptions.userData) {
                        var skip = loadOptions.skip? loadOptions.skip: 1,
                            take = loadOptions.take? loadOptions.take: scope._config['pageSize'];
                        var loadData = scope._getData(skip, take, loadOptions.filter, loadOptions.sort, loadOptions.requireTotalCount);
                        scope._pageData = result = loadData.data;
                        scope._dataTotalCount = dataTotalCount = loadData.totals;
                        scope._updatedData = true;
                    } else {
                        result = scope._pageData;
                        dataTotalCount = scope._dataTotalCount;
                    }

                    if (scope._dataTotalCount == null) {
                        deferred.resolve(result);
                    } else {
                        deferred.resolve(result, {totalCount: dataTotalCount} );
                    }
                }
                return deferred.promise();
            },
            key: function (data) {
                return data[scope.getPrimaryKey()];
            },
            onLoaded: function () {
                var afterLoadedData = scope._config.events['afterLoadedData'];
                if (afterLoadedData)
                    scope._config.events['afterLoadedData'].call(scope);
            }
        });
    },

    /**
     * To initialize private fields of the class
     * @param config: the grid configuration specified by the user
     * @private
     */
    _initializationFields: function (config) {
        this._config = config;
        this._viewDataSource = View.dataSources.get(config.dataSource);
        this._restriction = null;
        this._dataSourceKey = this._getPrimaryKey();
        this._groupID = null;
        this._groupData = null;
        this._totalValueFields = this._getTotalValueFields();
        this._pageData = null;
        this._dataTotalCount = null;
        this._updatedData = false;
    },

    /**
     * To obtain the configuration data store for the grid
     * @returns {{store: null}}
     */
    getConfigurationDataSourceGrid: function () {
        return {
            store: this._store
        };
    },

    /**
     * To obtain the data source for View
     * @returns {null}
     */
    getViewDataSource: function () {
        return this._viewDataSource;
    },

    /**
     * To obtain the field ID for grouping
     * @returns {null}
     */
    getGroupID: function () {
        return this._groupID;
    },

    getGroupData: function () {
        return this._groupData;
    },

    /**
     * To obtain the primary key of the database
     * @returns {null}
     */
    getPrimaryKey: function () {
        if (this._dataSourceKey == null) {
            return 'primaryKey';
        }
        return this._dataSourceKey;
    },

    /**
     * To obtain the data from the current loaded page
     * @returns {null}
     */
    getPageData: function () {
        return this._pageData;
    },

    /**
     * To set a special settings View
     * @param restriction: special settings
     */
    setRestriction: function (restriction) {
        this._restriction = restriction;
    },

    /**
     * Download new data?
     * @returns {true/false}
     */
    isNewData: function () {
        if (this._updatedData) {
            this._updatedData = false;
            return true;
        }
        return false;
    },

    /**
     * To accomplish the formation of the Ab.grid.Row
     * @param record: record
     * @returns {Ab.grid.Row}
     */
    getAbRow: function (record) {
        var row =  new Ab.grid.Row(this, record);
        var scope = this;
        row.getDataSource = function () {
            return scope._dataSource.getViewDataSource();
        };
        row.getRecord = function () {
            return this.record;
        };
        return row;
    },

    /**
     * To get the array data from the server
     * @param skip: the number of items that want to skip
     * @param take: the number of elements want to select
     * @param filterOption: options to filter
     * @param sortOption: options to sorting
     * @returns array data
     */
    _getData: function (skip, take, filterOption, sortOption, hasNeedTotalCount) {
        var loadData = this._loadData(skip, take, filterOption, sortOption, hasNeedTotalCount);
        var totals = null;
        if (loadData.totals) {
            totals = parseInt(loadData.totals.values['pms.count_of_records']);
        }
        return {
            data: this._convertToGridData(loadData.records),
            totals: totals
        };
    },
    /**
     * Search primary key in the datastore View
     * @returns {*}
     * @private
     */
    _getPrimaryKey: function () {
        var key;
        this._viewDataSource.fieldDefs.each(function (fieldDef) {
            if (fieldDef.primaryKey) {
                key = fieldDef.id;
            }
        });
        if (!key)
            return null;
        return DataGridSource.configGridFieldName(this._config.dataSource, key);
    },

    /**
     * To perform the conversion of data dataSource in data grid
     * @param records: data dataSource
     * @returns {Array}
     * @private
     */
    _convertToGridData: function (records) {
        var grid = jQuery("#"+this._config.container).dxDataGrid('instance');
        var page = grid.pageIndex();
        var data = [];
        var indexKey = 0;
        for (var i = 0; i < records.length; i++) {
            var record = records[i];
            var formattedValues = this.getViewDataSource().formatValues(record.values, true, true);
            var row = {};

            for (var index = 0; index < this._config.columns.length; index++) {
                var column = this._config.columns[index];
                var key = this._getKeyForGridData(column);
                if (key != null) {
                    row[key] = this._getValueForGridData(column, formattedValues);
                }
            }
            for (var name in record.values) {
                var value = record.getValue(name);
                var gridField = DataGridSource.configGridFieldName(this._config.dataSource, name);
                if (!row.hasOwnProperty(gridField)) {
                    row[gridField] = value;
                }
            }
            row['record'] = record;

            if (this._dataSourceKey == null) {
                row['primaryKey'] = 'page' + page + indexKey;
                indexKey++;
            }
            data.push(row);
        }
        return data;
    },

    _getValueForGridData: function (column, record) {
        var result = null;
        var fieldName = column.fieldName;
        if (fieldName) {
            switch (typeof fieldName) {
                case 'object':
                    result = this._formatToDisplayData(fieldName, record);
                    break;
                default:
                    result = record[fieldName];
                    break;
            }
        }
        return result;
    },

    _getKeyForGridData: function (column) {
        var result = null;
        var fieldName = column.fieldName;
        if (fieldName) {
            switch (typeof fieldName) {
                case 'object':
                    result = fieldName.name;
                    break;
                default:
                    result = DataGridSource.configGridFieldName(this._config.dataSource, fieldName);
                    break;
            }
        }
        return result;
    },

    _formatToDisplayData: function (fieldNameObject, record) {

        switch (typeof fieldNameObject.displayFormat) {
            case 'function':
                return fieldNameObject.displayFormat.call(this, record);
                break;
            default:
                var str = fieldNameObject.displayFormat.toString();
                var result;
                var baseFields = fieldNameObject.baseField,
                    lastValue = '';

                for (var i = 0; i < baseFields.length; i++) {
                    var value = record[baseFields[i]];
                    if (i == 0 && value == '') {
                        result = '';
                        break;
                    }
                    if (value == '') {
                        var positionPrepare = result.search(lastValue) + lastValue.length;
                        result = result.substr (0, positionPrepare);
                        break;
                    }
                    var reg = '{'+ i +'}';
                    str = result = str.replace(reg , value);
                    lastValue = value;
                }
                return result;
                break;
        }
    },


    /**
     * To obtain data which allows to form groups of rows in a group
     * @param filter: options to filter for grid
     * @returns groups data
     */
    _getGroupsData: function (filter, groupColumnID) {
        var result = [];
        var dataSource = this.getViewDataSource();

        var split = groupColumnID.split('*');

        var configDataSource = {
            groupByFieldName: dataSource.mainTableName + '.' + split[1],
            filterValues: this._getFilterOptions(filter)
        };
        var totalsValues = this._totalValueFields;
        if (totalsValues != null) {
            configDataSource['showTotals'] = true;
        }

        var serverData = dataSource.getGroups(this._restriction, configDataSource),
            dataTotals = serverData.totals;

        var totalSummary = [];
        if (totalsValues != null) {
            for (var i = 0; i < totalsValues.length; i++) {
                totalSummary[i] = dataSource.formatValue(totalsValues[i].formatField,  dataTotals.getValue(totalsValues[i].serverField), true);
            }
        }


        return {
            data: this._convertToGridGroupData(serverData, groupColumnID, configDataSource.showTotals, totalsValues),
            totalSummary: totalSummary
        };
    },

    /**
     * To convert the data groups to the data grid
     * @param serverData: data dataSource
     * @param groupColumnID: group ID
     * @param showTotals: is show totals
     * @param totalsValues: array totals values for display in grid
     * @returns {Array}
     * @private
     */
    _convertToGridGroupData: function (serverData, groupColumnID, showTotals, totalsValues) {
        var dataGroup = serverData.groups,
            databaseGroupField = this._getRecordFieldName(groupColumnID).mainField,
            result = [];
        for (var index = 0; index < dataGroup.length; index++) {
            var data =  dataGroup[index];
            var value = this.getViewDataSource().formatValue(databaseGroupField, data.getValue(databaseGroupField), true);
            var countField = '';
            for (var field in data.values) {
                if (field.includes('count_of_records')) {
                    countField = field;
                }
            }
             var resultDataGroup = {
                key: value,
                items: null,
                count: parseInt(data.getValue(countField))
            };
            if (showTotals) {
                var summary = [];
                for (var i = 0; i < totalsValues.length; i++) {
                    var summaryValue = '';
                    if (!jQuery.isEmptyObject(totalsValues[i])) {
                        summaryValue = this.getViewDataSource().formatValue(totalsValues[i].formatField, data.getValue(totalsValues[i].serverField), true);
                    }
                    summary.push(summaryValue);

                }
                resultDataGroup.summary = summary;
            }
            result.push(resultDataGroup);
        }
        this._groupData = result;
        return result;
    },

    /**
     * To execute to obtain the name of the field
     * @param name
     * @returns {string}
     * @private
     */
    _getRecordFieldName: function (name) {
        return DataGridSource.parseGridFieldName(name);
    },

    /**
     * To load the array data from the server
     * @param skip: the number of items that want to skip
     * @param take: the number of elements want to select
     * @param filterOption: options to filter for grid
     * @param sortOption: options to sorting for grid
     * @returns array data
     * @private
     */
    _loadData: function (skip, take, filterOption, sortOption, hasNeedTotalCount) {

        var filterValues = this._getFilterOptions(filterOption);
        var sortValues = this._getSortOptions(sortOption);
        var showTotals = false;
        if (hasNeedTotalCount) {
            showTotals = true;
        }

        return this.getViewDataSource().getDataSet(this._restriction, {
            showTotals: showTotals,
            sortValues: sortValues,
            filterValues: filterValues,
            pageStart: skip,
            pageSize: take
        });
    },

    /**
     * To perform receive filtering options for the query to the server
     * @param filterOption: options to filter for grid
     * @returns array data
     * @private
     */
    _getFilterOptions: function (filterOption) {
        var filterValues = [];
        var prevFilterType = '';
        var isOption = '';
        var groupValues = [];

        if (filterOption) {
            if (typeof filterOption[0] != 'object') {

                if (this._getRecordFieldName(this._groupID).mainField != 'pms.none') {
                    if (filterOption[0] == this._groupID) {
                        groupValues.push("'" + filterOption[2] + "'");
                    } else {
                        var filterData = this._configurationFilterData(filterOption[0], filterOption[2], filterOption[1]);
                        filterValues[0] = this._formationFilterQuery(filterOption[0], filterData.filterValue, filterData.filterOperation, false);
                    }
                }
            } else {
                for (var i = 0; i < filterOption.length; i++) {
                    var option = filterOption[i];

                    switch (option) {
                        case 'or':
                            isOption = 'or';
                            break;
                        case 'and':
                            isOption = 'and';
                            break;
                        default:
                            if (typeof option[0] == 'object') {
                                var pageFilter = this._getFilterOptions(option);
                                for (var index = 0; index < pageFilter.length; index++) {
                                    filterValues[filterValues.length] = pageFilter[index];
                                }
                            } else {
                                var filterData = this._configurationFilterData(option[0], option[2], option[1]);

                                if (this._getRecordFieldName(this._groupID).mainField == 'pms.none')
                                    continue;

                                if (option[0] == this._groupID) {
                                    groupValues.push("'" + option[2] + "'");
                                    continue;
                                }

                                switch (isOption) {
                                    case 'or':
                                        /*if (prevFilterType == option[0]) {
                                            var filterValue = filterValues[filterValues.length - 1].filterValue;
                                            var elements = filterValue.split(',');
                                            if (elements.length == 1) {
                                                filterValues[filterValues.length - 1].filterValue = '"' + filterValue + '","' + filterData.filterValue + '"';
                                            } else {
                                                filterValues[filterValues.length - 1].filterValue += ',"' + filterData.filterValue + '"';
                                            }
                                        } else {*/
                                            filterValues.push(this._formationFilterQuery(option[0], filterData.filterValue, filterData.filterOperation, true));
                                       /* }*/

                                        isOption = '';
                                        break;
                                    case 'and':
                                        var pageFilter = this._getFilterOptions(option);
                                        for (var index = 0; index < pageFilter.length; index++) {
                                            filterValues.push(pageFilter[index]);
                                        }
                                        isOption = '';
                                        break;
                                    default:
                                        filterValues.push(this._formationFilterQuery(option[0], filterData.filterValue, filterData.filterOperation, false));
                                        prevFilterType = option[0];
                                        break;
                                }
                            }
                            break;
                    }
                }

                for (var i = 0; i < filterValues.length; i++) {
                     var filterValue = filterValues[i].filterValue;
                     var elements = filterValue.split(',');
                     var startWith = filterValue.split('{');
                     if (elements.length != 1 && startWith.length == 1) {
                        filterValues[i].filterValue = '{' +  filterValues[i].filterValue + '}';
                     }
                 }
            }
        }

        if (groupValues.length != 0) {
            if (this._restriction == null) {
                this._restriction = new Ab.view.Restriction();
            }
            for (var i = 0; i < groupValues.length;i++) {
                var value = groupValues[i],
                    operation = '=';
                if (value == '') {
                    operation = 'IS NULL';
                    value = '';
                }

                if (i != 0) {
                    this._restriction.addClause(this._getRecordFieldName(this._groupID).fieldName, value, operation,'OR');
                } else {
                    this._restriction.addClause(this._getRecordFieldName(this._groupID).fieldName, value, operation);
                }
                /*
                 есть ли рестрикшен
                 addClause(... ')AND(')
                 addClause (... 'OR')
                 addClause(... 'OR')
                 */

            }
        }

        return filterValues;
    },
    /**
     * To perform the data configuration of the filter for the database
     * @param value: the value of the operation
     * @param operation: of the operation
     * @returns {{filterValue: *, filterOperation: string}}
     * @private
     */
    _configurationFilterData: function (name, value, operation) {
        var filterValue = this.getViewDataSource().parseValue(this._getRecordFieldName(name).fieldName, value, true);
        var filterOperation = '=';
        switch (operation) {
            case 'contains':
                filterOperation = 'LIKE';
                filterValue = '%' + value + '%';
                break;
            case 'startswith':
                filterOperation = 'LIKE';
                filterValue = value + '%';
                break;
            case 'endswith':
                filterOperation = 'LIKE';
                filterValue = value + '%';
                break;
            case '<>':
                filterOperation = '<>';
                break;
            case '>=':
                filterOperation = '>=';
                break;
            case '>':
                filterOperation = '>';
                break;
            case '<=':
                filterOperation = '<=';
                break;
            case '<':
                filterOperation = '<';
                break;
        }
        if (value == '') {
            filterOperation = 'IS NULL';
            filterValue = '';
        }
        return {
            filterValue: filterValue,
            filterOperation: filterOperation
        }
    },

    /**
     * To perform the formation of the structure on the query filter for the database
     * @param name: name of the field database
     * @param value: value of the field database
     * @param operation: operation of the field database
     * @param isOr: a subsequent request OR
     * @returns {{fieldName: string, filterValue: *, filterOperation: *}}
     * @private
     */
    _formationFilterQuery: function (name, value, operation, isOr) {
        var result = {
            fieldName: this._getRecordFieldName(name).fieldName,
            filterValue: value,
            filterOperation: operation
        };

        if (isOr || operation == 'IS NULL'){
            result['relativeOperation'] = 'OR';
        }
        return result;
    },

    /**
     * To perform receive sorting options for the query to the server
     * @param sortOption: options to sorting for grid
     * @returns array data
     * @private
     */
    _getSortOptions: function (sortOption) {
        var sortValues = [];
        if (sortOption) {
            for (var index = 0; index < sortOption.length; index++) {
                var option = sortOption[index];
                var sortOrder = option.desc? -1 : 1;
                sortValues[sortValues.length] = {
                    fieldName: this._getRecordFieldName(option.selector).fieldName,
                    sortOrder: sortOrder
                }
            }
        }

        return sortValues;
    },

    /**
     * To perform the formation of the list to display total values
     * @returns {Array total values}
     * @private
     */
    _getTotalValueFields: function () {
        var result = [];
        var columnsDef = this._config.columns;

        for (var i = 0; i < columnsDef.length; i++) {
            var columnDef = columnsDef[i];
            if (columnDef['showTotals']) {
                var splitID = columnDef.fieldName.split('.');
                result.push({
                    serverField: splitID[0] + '.' + columnDef.totalsFunction + '_' + splitID[1],
                    formatField: columnDef.fieldName
                });
            }
            if (columnDef['showWidgetInGroup']) {
                result.push({});
            }
        }
        if (result.length == 0) {
            result = null;
        }
        return result;
    }
}, {
    configGridFieldName: function (dataSourceName, recordFieldName) {
        var result = null;
        switch (typeof recordFieldName) {
            case 'object':
                result = recordFieldName.name;
                break;
            default:
                var dataSource = View.dataSources.get(dataSourceName),
                    mainTable = dataSource.mainTableName;
                var split = recordFieldName.split('.');
                result = mainTable + '('+ split[0] + ')*' + split[1];
                break;
        }
        return result;
    },

    parseGridFieldName: function (fieldName) {
        var splitMainTable =  fieldName.split('(');
        var splitField = splitMainTable[1].split(')*');

        return {
            fieldName: splitField[0] + '.' + splitField[1],
            mainField: splitMainTable[0] + '.' + splitField[1]
        };
    }

});
