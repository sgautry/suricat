/**
 * The formation of the configuration of the grid columns
 */
var DataGridColumnConfigurator = Base.extend({

    _configUser: null,
    _columns: null,
    _databaseConfiguration: null,

    /**
     * Constructor
     * @param config:  grid configuration specified by the user
     */
    constructor: function (config, dataSource, gridSelection) {
        this._configUser = config;
        this._dataSource = dataSource;
        this._gridSelection = gridSelection;
        this._columns = this._configureColumns();
    },

    /**
     * Get the configuration of the grid columns
     * @returns configuration of the grid columns
     */
    getColumns: function () {
        return this._columns;
    },

    /**
     * To convert custom columns in columns for grid
     * @returns columns for grid
     * @private
     */
    _configureColumns: function () {
        var columns = this._configUser.columns;
        var result = [];


        for (var index = 0; index < columns.length; index++) {
            var column = columns[index];
            var columnResult = {};

            if (column['listener']) {
                result.push(this._createColumnListener(column));
                continue;
            }

            if (column['groupColumnTitle']) {
                var columnGroup = this._createColumnsGroup(index, columns);
                result[result.length] = columnGroup.column;
                index = columnGroup.indexNextColumn;
                continue;
            }

            columnResult = this._createColumn(column);
            result.push(columnResult);
        }
        return result;
    },

    /**
     * To create column for grid and if need fixed column or the formation of block columns
     * @param columnDef: The ARCHIBUS view column definition
     * @returns column for grid
     * @private
     */
    _createColumn: function (columnDef) {
        var result = this._createSubColumn(columnDef);

        if (this._configUser.lastLeftFixedColumn) {
            if (this._configUser.lastLeftFixedColumn == columnDef.id) {
                this._configUser.lastLeftFixedColumn = false;
            }
            result['fixed'] = true;
            result['fixedPosition'] = 'left';
        }

        if (this._configUser.firstRightFixedColumn) {
            if (this._configUser.firstRightFixedColumn == columnDef.id) {
                this._configUser.firstRightFixedColumn = 'isFixed';
            }
            if (this._configUser.firstRightFixedColumn == 'isFixed') {
                result['fixed'] = true;
                result['fixedPosition'] = 'right';
            }
        }
        return result;
    },

    /**
     * To create columns contain basic information: the identifier, type, height, width, and other parameters
     * @param columnDef: The ARCHIBUS view column definition
     * @returns column grid
     * @private
     */
    _createSubColumn: function (columnDef) {
        var result = {};

        if (columnDef.title) {
            result['caption'] = columnDef.title;
        }
        if (columnDef.fieldName) {
            switch (typeof columnDef.fieldName) {
                case 'object':
                    result['dataField'] = columnDef.fieldName.name;
                    break;
                default:
                    var splitID = columnDef.fieldName.split('.');
                    result['dataField'] = DataGridSource.configGridFieldName(this._configUser.dataSource, columnDef.fieldName);
                    if (!this._configUser.databaseName) {
                        this._configUser.databaseName =  splitID[0];
                    }
                    break;
            }
        }
        if (columnDef.id) {
            result['id'] = columnDef.id;
        }
        if (columnDef.type) {
            switch (columnDef.type) {
                case 'number':
                case 'integer':
                    result['dataType'] = 'string';
                    result['filterOperations'] = [ '=', '<>', '<', '>', '<=', '>=', 'between' ];
                    result['userType'] = columnDef.type;
                    break;
                case 'scheduler':
                    result['dataType'] = 'string';
                    result['startDate'] = columnDef.startDate;
                    result['dataField'] = columnDef.fieldName;
                    result['allowHiding'] = false;
                    break;
                default:
                    result['dataType'] = columnDef.type;
                    break;
            }
            result['columnType'] = columnDef.type;
        }

        if (columnDef.minWidth) {
            result['minWidth'] = columnDef.minWidth;
        }
        if (columnDef.sortOrder) {
            result['sortOrder'] = columnDef.sortOrder;
        }

        if (columnDef.width) {
            result['width'] = columnDef.width;
        }
        if (columnDef.groupBy) {
            result['groupIndex'] = 0;
        }
        if (columnDef.visible != undefined) {
            result['visible'] = columnDef.visible;
        }
        if (columnDef.showWidgetInGroup) {
            result['showWidgetInGroup'] = true;
        }
        return result;
    },

    /**
     * To create group columns to display in the grid
     * @param indexCurrentColumn: index of column at which to start grouping
     * @param columnsDef: The ARCHIBUS view columns definition
     * @returns group columns
     * @private
     */
    _createColumnsGroup: function (indexCurrentColumn, columnsDef) {
        var groupColumn = columnsDef[indexCurrentColumn];

        var result = {
            indexNextColumn: null,
            column: {
                allowHiding: false,
                caption: groupColumn['groupColumnTitle']
            }
        };

        if (groupColumn != undefined) {
            result.column['visible'] = groupColumn['visible'];
        }

        if (this._configUser.firstRightFixedColumn) {
            if (this._configUser.firstRightFixedColumn == groupColumn.id) {
                this._configUser.firstRightFixedColumn = 'isFixed';
            }
            if (this._configUser.firstRightFixedColumn == 'isFixed') {
                result.column['fixed'] = true;
                result.column['fixedPosition'] = 'right';
            }
        }

        var bandColumns = [];
        for (var i = indexCurrentColumn; i < columnsDef.length; i++) {
            var column = columnsDef[i];
            if (column['groupColumnID'] != groupColumn['groupColumnID']) {
                break;
            }
            var bandColumn = this._createColumn(column);
            if (groupColumn.groupBackgroundColor) {
                bandColumn['backgroundColor'] = groupColumn.groupBackgroundColor;
            }
            bandColumns.push(bandColumn);
        }
        result.column['columns'] = bandColumns;

        if ( i == columnsDef.length) {
            result.indexNextColumn = i;
        } else {
            result.indexNextColumn = --i;
        }
        if (groupColumn.groupBackgroundColor) {
            result.column['backgroundColor'] = groupColumn.groupBackgroundColor;
        }
        if (groupColumn.fieldName) {
            result.column['dataField'] = groupColumn['groupColumnID'];
        }

        return result;
    },

    _createColumnListener: function (column) {
        var scope = this;
        var columnResult = {};
        if (column.fieldName) {
            columnResult.dataField = column.fieldName;
        }
        columnResult.caption = column.title;
        columnResult.alignment = 'center';
        if (column['buttonText']) {
            columnResult['buttonText'] = column['buttonText'];
        }
        if (column['icon']) {
            columnResult['icon'] = column['icon'];
        }
        if (column['cssClass']) {
            columnResult['cssClass'] = column['cssClass'];
        }
        if (column['width']) {
            columnResult['width'] = column['width'];
        }
        columnResult.listener = column['listener'];
        columnResult.cellTemplate =  function (container, options) {
            if (options.rowType == 'data') {
                var buttonColumn = options.column;
                var configButton = {
                    icon: 'preferences',
                    onClick: function (e) {
                        scope._gridSelection.buttonClick();
                        var row = scope._dataSource.getAbRow(options.data.record),
                            abColumn = new Ab.grid.Column(buttonColumn.id, buttonColumn.title);
                        row.dom = this;
                        var returnCell = new Ab.grid.Cell(row, abColumn, this);
                        buttonColumn['listener'].call(this, row, returnCell);
                    }
                };
                if (buttonColumn['buttonText']) {
                    configButton['text'] = buttonColumn['buttonText'];
                }
                if (buttonColumn['icon']) {
                    configButton['icon'] = buttonColumn['icon'];
                }
                if (buttonColumn['cssClass']) {
                    jQuery('<div class ="'+ buttonColumn.cssClass +'"/>').dxButton(configButton).appendTo(container);
                } else {
                    jQuery('<div/>').dxButton(configButton).appendTo(container);
                }

            }
        };

        return columnResult;
    }

});