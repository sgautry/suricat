/**
 * Contains functionality for working with rows which were selected by users in the grid
 */
var GridRowsSelection = Base.extend({

    /**
     * The grid contains the group on the next page. Structure: {name: '...', isSelected: 'true/false'}
     */
    _groupContinuationOnNextPage: null,
    /**
     * The grid contains the group on the previous page. Structure: {name: '...', isSelected: 'true/false'}
     */
    _groupContinuationOnPrevPage: null,
    /**
     * Selected the row using the click event on the row. Structure: {key: '...', checkbox: 'row dxCheckBox'}
     */
    _currentClickRow: null,
    /**
     * The grid object
     */
    _grid: null,
    /**
     * The grid configuration specified by the user
     */
    _config: null,
    /**
     * The key store for the groups on the page
     */
    _keysStory: null,
    /**
     * The data source for grid
     */
    _dataSource: null,
    /**
     * Stores collapsed groups with the status of the checkbox
     */
    _collapsedGroupsStory: null,
    /**
     *  Indicates which pages were done selecting all the rows
     */
    _selectedAllRows: null,

    /**
     * Constructor
     * @param config: The grid configuration specified by the user
     */
    constructor: function (config) {
        this._keysStory = new KeySelectionStory();
        this._selectedAllRows = {
            status: false,
            pages: []
        };
        this._collapsedGroupsStory = {};
        this._config = config;
        this.customSelectionFlag = false;
        var scope = this;
        this._selectedAllRows.pages.remove = function(from, to) {
            var rest = this.slice((to || from) + 1 || this.length);
            this.length = from < 0 ? this.length + from : from;
            return this.push.apply(this, rest);
        };

        /**
         * Specifies a function that customizes grid columns after they are created.
         * @param columns: grid columns.
         */
        this.onCustomizeColumns = function (columns) {
            jQuery.each(columns, function (_, element) {
                if (element.groupIndex == 0) {
                    element.groupCellTemplate = scope.groupCellTemplate;
                }
            });
        };
        /**
         * Fires when a grid record has been selected/deselected.
         * @param selectedItems: the object contains the selected/unselected rows
         */
        this.onGridSelectionChanged = function (selectedItems) {
            if (scope._buttonClick) {
                scope._buttonClick = false;
                if (selectedItems.currentDeselectedRowKeys.length == 1) {
                    scope._grid.selectRows([selectedItems.currentDeselectedRowKeys[0]], true);
                }
                if (selectedItems.currentSelectedRowKeys.length == 1) {
                    scope._grid.deselectRows([selectedItems.currentSelectedRowKeys[0]], true);
                }
                return;
            }
            scope._runSingleOrMultiSelection(selectedItems);
            scope._syncGroupsCheckbox(selectedItems);

        };
        /**
         * Fires when a user clicks a grid row.
         * @param info: object containing information about the grid row
         */
        this.onRowClick = function (info) {
            if (scope._config.multipleSelectionEnabled) {
                if (info.rowType == 'data') {
                    var rowElement = info.rowElement.find('.dx-select-checkbox').dxCheckBox("instance");

                    var multiSelect = false;
                    if (scope._isMultiSelection) {
                        multiSelect = true;
                    } else {
                        scope._currentClickRow = {
                            key: info.key,
                            checkbox: rowElement
                        };
                    }
                    if (info.isSelected) {
                        scope._grid.deselectRows(info.key);
                        scope._currentClickRow = null;
                    } else {
                        scope._grid.selectRows([info.key], true);
                    }
                }
            }
        };
        /**
         * To change the template header of the group for the grid
         * @param groupCell: information about cell group
         * @param info: object containing information about the grid row
         */
        this.groupCellTemplate = function (groupCell, info) {
            scope._createGroupCheckbox(groupCell, info);
        };
        /**
         * Fires before a master or group row is collapsed in the grid.
         * @param option: object containing information about the grid row
         */
        this.onRowCollapsing = function (option) {
            var checkboxName = scope._getGroupCheckboxName(option.key[0]);
            scope._collapsedGroupsStory[checkboxName] = scope._keysStory.getStatus(checkboxName);
            if (scope._selectedAllRows.status) {
                var currentPage = scope._grid.pageIndex();
                var pages = scope._selectedAllRows.pages;
                var index = -1;
                for (var i = 0; i < pages.length; i++) {
                    if (pages[i] == currentPage) {
                        index = i;
                        break;
                    }
                }
                if (index != -1) {
                    scope._selectedAllRows.pages.remove(index);
                }
            }
        };
        /**
         * Fires after a master or group row is collapsed in the grid.
         * @param option: object containing information about the grid row
         */
        this.onRowCollapsed = function (option) {
            var checkboxName = scope._getGroupCheckboxName(option.key[0]);
            var checkbox = scope._getGroupCheckbox(checkboxName);
            var status = scope._collapsedGroupsStory[checkboxName];
            checkbox.option('value', status);
            scope._keysStory.updateStatus(checkboxName, status);
        };
        /**
         * Fires before a master or group row is expanded in the grid.
         * @param option: object containing information about the grid row
         */
        this.onRowExpanding = function (option) {
            var checkboxName = scope._getGroupCheckboxName(option.key[0]);
            var checkbox = scope._getGroupCheckbox(checkboxName);
            scope._collapsedGroupsStory[checkboxName] = checkbox.option('value');
        };
        /**
         * Fires after a master or group row is expanded in the grid.
         * @param option: object containing information about the grid row
         */
        this.onRowExpanded = function (option) {
            var checkboxName = scope._getGroupCheckboxName(option.key[0]);
            var checkbox = scope._getGroupCheckbox(checkboxName);
            var status = scope._collapsedGroupsStory[checkboxName];
            checkbox.option('value', status);
            var rowKeys = scope._keysStory.getKeys(checkboxName);
            switch(status) {
                case true:
                    scope._grid.selectRows(rowKeys, true);
                    break;
                case false:
                    scope._grid.deselectRows(rowKeys);
                    break;
            }
            delete scope._collapsedGroupsStory[checkboxName];
        };
    },

    /**
     * To set the initial data grid and data source
     * @param grid: the grid object
     * @param dataSource: the data source for grid
     */
    setConfig: function (grid, dataSource) {
        this._grid = grid;
        this._dataSource = dataSource;
        this._keysStory.setConfig(grid, dataSource);
    },

    /**
     * Get configuration module selection grid, depending on user settings
     * @returns {{mode: single/multiple}}
     */
    getSelectionConfiguration: function () {
        var result = {
            mode: 'single'
        };
        if (this._config.multipleSelectionEnabled) {
            result.mode = 'multiple';
            result.showCheckBoxesMode = 'always';
            result.allowSelectAll = true;
            result.selectAllMode = 'allPage';
        }
        return result;
    },

    buttonClick: function () {
        this._buttonClick = true;
    },

    /**
     * To perform the override function changes the value of the checkbox header grid
     * @param option: information about the event
     */
    overrideFunctionHeaderCheckbox: function (option) {
        if (option.rowType == 'header' && option['column'].command == 'select') {
            var checkbox = option.cellElement.find('.dx-select-checkbox').dxCheckBox("instance");
            var scope = this;

            checkbox.option('onValueChanged', function (e) {
                switch (e.value) {
                    case true:
                        scope._selectedAllRows.checkbox = checkbox;
                        scope._selectedAllRows.status = true;
                        scope._selectedAllRows.pages.push(scope._grid.pageIndex());
                        scope._grid.selectAll();
                        break;
                    case false:
                        scope._grid.clearSelection();
                        scope._selectedAllRows = {
                            status: false,
                            pages: []
                        };
                        //scope._collapsedGroupsStory = {};
                        break;
                    case 'undefined':
                        break;
                }
            });
        }
    },

    /**
     * To run custom events for single or multi selection of rows in the grid
     * @param selectedItems: the object contains the selected/unselected rows
     * @private
     */
    _runSingleOrMultiSelection: function (selectedItems) {
        if (selectedItems.selectedRowKeys.length > 1) {
            this._isMultiSelection = true;
        } else {
            this._isMultiSelection = false;
        }

        if (this._currentClickRow == null) {
            this._isMultiSelection = true;
            this._multiSelection(selectedItems);
        } else {
            if (this._isMultiSelection) {
                if (this._currentClickRow != null) {
                    this._currentClickRow.checkbox.option('value', true);
                    this._dataSource.getAbRow(this._currentClickRow.key, true);
                    this._currentClickRow = null;
                }
                this._multiSelection(selectedItems);
            } else {
                if (this._currentClickRow != null) {
                    this._currentClickRow.checkbox.option('value', false);
                    if (selectedItems.selectedRowsData.length == 1) {
                        var clickItemEvent = this._grid.option('onClickItem' + 'Def');
                        if (clickItemEvent) {
                            var row = this._dataSource.getAbRow(selectedItems.selectedRowsData[0].record);
                            clickItemEvent.call(null,row);
                        }
                    }
                }
            }
        }

        if (selectedItems.selectedRowKeys.length <= 1) {
            this._isMultiSelection = false;
        }
    },

    /**
     * To run custom events for multi selection/unselection of rows in the grid
     * @param rowKeys: the object contains the selected/unselected rows
     * @private
     */
    _multiSelection: function (rowKeys) {
        var multipleSelectionEvent = this._grid.option('onMultipleSelectionChange' + 'Def');
        if (multipleSelectionEvent) {
            this._multiSelectedRows(rowKeys.currentSelectedRowKeys, true);
            this._multiSelectedRows(rowKeys.currentDeselectedRowKeys, false);
        }
    },

    /**
     * To call a custom function to selection the rows
     * @param rowKeys: the object contains the selected/unselected rows
     * @param isSelected: the row is selected or not
     * @private
     */
    _multiSelectedRows: function (rowKeys, isSelected) {
        for (var i = 0; i < rowKeys.length; i++) {
            var record = this._getValueFromArray(rowKeys[i], 'record');
            if (record != null) {
                var row = this._dataSource.getAbRow(record);
                this._grid.option('onMultipleSelectionChange' + 'Def').call(null, row, isSelected);
            }
        }
    },

    /**
     * Sync groups checkboxes
     * @param selectedItems: the object contains the selected/unselected rows
     * @private
     */
    _syncGroupsCheckbox: function (selectedItems) {
        var grid = selectedItems.component;
        var groupedColumnNames = this._getGroupedColumns(grid);
        if (groupedColumnNames.length == 0)
            return;
        this.customSelectionFlag = true;
        this._synchronizeCheckBoxes(grid, selectedItems.currentDeselectedRowKeys, groupedColumnNames);
        this._synchronizeCheckBoxes(grid, selectedItems.selectedRowKeys, groupedColumnNames);
        this.customSelectionFlag = false;
    },

    /**
     * To create the checkbox for the row group
     * @param groupCell: information about cell group
     * @param info: object containing information about the grid row
     * @private
     */
    _createGroupCheckbox: function (groupCell, info) {
        if (this._dataSource.isNewData()) {
            this._keysStory.clean();
        }
        var groupName = "groupCheckBox" + info.data.key.replace(new RegExp(' ','g'),'');
        this._keysStory.add(groupName, info);
        if (this._collapsedGroupsStory.hasOwnProperty(groupName)) {
            this._keysStory.updateStatus(groupName, this._collapsedGroupsStory[groupName]);
        } else {
            this._keysStory.updateStatus(groupName);
        }

        this._renderGroupCheckbox(groupName, groupCell, info);

        if (this._selectedAllRows.status) {
            var pageIndex = this._grid.pageIndex();
            var selectionPages = this._selectedAllRows.pages;
            var isSelect = true;
            for (var i = 0; i < selectionPages.length; i++) {
                if (selectionPages[i] == pageIndex) {
                    isSelect = false;
                    break;
                }
            }
            if (isSelect) {
                this._grid.selectAll();
                this._selectedAllRows.pages.push(pageIndex);
            }
        }

        if (this._groupContinuationOnNextPage!= null && this._groupContinuationOnNextPage.name == groupName) {
            if (this._groupContinuationOnNextPage.isSelected)
                this._grid.selectRows(this._keysStory.getKeys(groupName), true);
            else
                this._grid.deselectRows(this._keysStory.getKeys(groupName), true);

        }
        if (this._groupContinuationOnPrevPage != null && this._groupContinuationOnPrevPage.name  == groupName) {
            if (this._groupContinuationOnPrevPage.isSelected)
                this._grid.selectRows(this._keysStory.getKeys(groupName), true);
            else
                this._grid.deselectRows(this._keysStory.getKeys(groupName), true);
        }
    },

    /**
     * To perform the render checkbox for the row group
     * @param checkboxName: the ID of the checkbox
     * @param groupCell: information about cell group
     * @param info: object containing information about the grid row
     * @private
     */
    _renderGroupCheckbox: function (checkboxName, groupCell, info) {
        var scope = this;
        jQuery('<div>').addClass("customSelectionCheckBox")
            .attr('id', checkboxName)
            .appendTo(groupCell)
            .dxCheckBox({
                text: info.column.caption + ': ' + info.text,
                value: this._keysStory.getStatus(checkboxName),
                onValueChanged: function (e) {
                    if (scope.customSelectionFlag)
                        return;
                    var rowKeys = scope._keysStory.getKeys(checkboxName);
                    var status = '';
                    if (e.value) {
                        if (info.data.isContinuationOnNextPage) {
                            scope._groupContinuationOnNextPage = {name: checkboxName, isSelected: true};
                        }
                        if (info.data.isContinuation) {
                            scope._groupContinuationOnPrevPage = {name: checkboxName, isSelected: true};
                        }
                        scope._grid.selectRows(rowKeys, true);
                        status = true;
                    }
                    else {
                        if (info.data.isContinuationOnNextPage) {
                            scope._groupContinuationOnNextPage = {name: checkboxName, isSelected: false};
                        }
                        if (info.data.isContinuation) {
                            scope._groupContinuationOnPrevPage = {name: checkboxName, isSelected: false};
                        }
                        scope._grid.deselectRows(rowKeys);
                        status = false;
                    }
                    scope._keysStory.updateStatus(checkboxName, status);
                    if (scope._collapsedGroupsStory.hasOwnProperty(checkboxName)) {
                        scope._collapsedGroupsStory[checkboxName] = status;
                    }
                }
            })
    },

    /**
     * Get the specified field in the record with the required key from the current page
     * @param keyValue: key
     * @param dataField: data field
     * @returns {value}
     * @private
     */
    _getValueFromArray: function (keyValue, dataField) {
        var selection = this._dataSource.getPageData();
        var scope = this;
        var result = jQuery.grep(selection, function (el) {
            return el[scope._dataSource.getPrimaryKey()] == keyValue;
        })[0];
        if (!result) {
            return null;
        }
        return result[dataField];
    },

    /**
     * Get the IDs of the columns are grouping
     * @param dataGrid: the grid object
     * @returns {IDs columns}
     * @private
     */
    _getGroupedColumns: function (dataGrid) {
        var colNames = [];
        for (i = 0; i < dataGrid.columnCount(); i++) {
            if (dataGrid.columnOption(i, "groupIndex") > -1) {
                colNames.push(dataGrid.columnOption(i, "dataField"));
            }
        }
        return colNames;
    },

    /**
     * To synchronize checkbox group
     * @param grid: the grid object
     * @param keys: key rows group
     * @param groupedColumnNames: IDs group columns
     * @private
     */
    _synchronizeCheckBoxes: function (grid, keys, groupedColumnNames) {
        if (!keys || keys.length == 0 || !groupedColumnNames || !grid)
            return;
        var synchronizedCheckBoxes = [];
        for (var j = 0; j < groupedColumnNames.length; j++) {
            for (var i = 0; i < keys.length; i++) {
                var keyValue = keys[i];
                var rowIndex = grid.getRowIndexByKey(keyValue);
                if (rowIndex != -1) {
                    var columnField = groupedColumnNames[j];
                    var groupRowValue = grid.cellValue(rowIndex, columnField);
                    if (groupRowValue == null)
                        continue;
                    var checkboxName =  this._getGroupCheckboxName(groupRowValue);
                    this._keysStory.updateStatus(checkboxName);
                    var value =  this._keysStory.getStatus(checkboxName);
                    var checkbox = this._getGroupCheckbox(checkboxName);
                    if (checkbox)
                        checkbox.option("value", value);
                }
            }

        }
    },

    /**
     * To get the object of checkbox for the specified group
     * @param groupName: group name
     * @returns {*}
     * @private
     */
    _getGroupCheckbox: function (groupName) {
        var name = groupName;
        if (!groupName.startsWith("groupCheckBox")) {
            name = this._getGroupCheckboxName(groupName);
        }
        var checkBoxEl = jQuery("#" + name);
        return jQuery(checkBoxEl).dxCheckBox("instance");
    },

    /**
     * To perform the formation of the names of the checkbox for the group
     * @param groupName: group name
     * @returns {string}
     * @private
     */
    _getGroupCheckboxName: function (groupName) {
        return "groupCheckBox" + groupName.replace(new RegExp(' ','g'),'');
    }

});

/**
 * The key store for the groups on the page
 */
var KeySelectionStory = Base.extend({
    /**
     * The grid object
     */
    _grid: null,
    /**
     * The data source for grid
     */
    _dataSource: null,
    /**
     * The object key store for the groups on the page
     */
    _keyStory: null,

    /**
     * Constructor
     */
    constructor: function () {
        this._keyStory = {};
    },

    /**
     *  Added group in story. Create data records on the group
     * @param groupName: group name
     * @param info: object containing information about the grid row
     */
    add: function (groupName, info) {
        var rowKeys = this._createKeys(info.data, [], info.column.dataField);
        this._keyStory[groupName] = {
            keys: rowKeys,
            status: false
        }
    },

    /**
     * To get the status of group selection
     * @param groupName: group name
     * @returns {boolean|*|string|number}
     */
    getStatus: function (groupName) {
        return this._keyStory[groupName].status;
    },

    /**
     * To row keys for the specified group
     * @param groupName: group name
     * @returns {*}
     */
    getKeys: function (groupName) {
        return this._keyStory[groupName].keys;
    },

    /**
     * To update the status of the specified group
     * @param groupName: group name
     * @param currentStatus: the status of the checkbox group
     */
    updateStatus: function (groupName, currentStatus) {
        var status = currentStatus;
        if(currentStatus == undefined) {
            status = this._checkIfKeysAreSelected(this._keyStory[groupName].keys, this._grid.getSelectedRowKeys())
        }
        this._keyStory[groupName].status = status;
    },

    /**
     * To store cleanup
     */
    clean: function () {
        this._keyStory = {};
    },

    /**
     * To set the initial data grid and data source
     * @param grid: the grid object
     * @param dataSource: the data source for grid
     */
    setConfig: function (grid, dataSource) {
        this._grid = grid;
        this._dataSource = dataSource;
    },

    /**
     * To perform a check of the specified group in the story
     * @param groupName: group name
     * @returns {boolean}
     */
    isContained: function (groupName) {
        var result =  false;
        for (var key in this._keyStory) {
            if (key == groupName) {
                result = true;
                break;
            }
        }
        return result;
    },

    /**
     * To create an array of keys for the specified group
     * @param data: row data
     * @param keys: row keys
     * @param groupedColumnName: group name
     * @param groupKey: group key
     * @returns {*}
     * @private
     */
    _createKeys: function (data, keys, groupedColumnName, groupKey) {
        if (!groupKey)
            groupKey = data.key;
        var dataItems = data.items || data.collapsedItems || data; // check if it's a group row that has nested rows

        for (var i = 0; i < dataItems.length; i++) {
            var childItems = dataItems[i].items || dataItems[i].collapsedItems;
            if (childItems) {
                this._createKeys(dataItems[i], keys, groupedColumnName, groupKey);
            } else
                keys.push(dataItems[i][this._dataSource.getPrimaryKey()]);
        }
        if (data.isContinuation || data.isContinuationOnNextPage)
            this._getKeysFromDataSource(keys, groupKey, groupedColumnName);

        return keys;
    },

    /**
     * To obtain the keys from the database
     * @param keys: keys
     * @param groupValue: group value
     * @param fieldName: column name
     * @private
     */
    _getKeysFromDataSource: function (keys, groupValue, fieldName) {
        var colFields = fieldName.split(".");
        var filteredKeys = jQuery.grep(this._dataSource.getPageData(), function (el) {
            var result = el;
            for (var index = 0; index < colFields.length; index++) {
                var field = colFields[index];
                result = result[field];
                if (!jQuery.isPlainObject(result))
                    break;
            }
            return result == groupValue;
        });
        for (var i = 0; i < filteredKeys.length; i++) {
            var value = filteredKeys[i][this._dataSource.getGroupID()];
            if (value && keys.indexOf(value) == -1) // invisible key
                keys.push(value);
        }
    },

    /**
     * To check the status of the group
     * @param currentKeys: the keys group
     * @param selectedKeys: all the selected row keys
     * @returns {true/false/undefined}
     * @private
     */
    _checkIfKeysAreSelected: function (currentKeys, selectedKeys) {
        var count = 0;

        if (selectedKeys.length == 0)
            return false;
        for (var i = 0; i < currentKeys.length; i++) {
            var keyValue = currentKeys[i];
            if (selectedKeys.indexOf(keyValue) > -1) // key is not selected
                count++;
        }
        if (count == 0)
            return false;
        if (currentKeys.length == count)
            return true;
        else
            return undefined;
    }
});