
/**
 * Contains methods for creating and added features: upgrading the grid, get the selected rows and other methods
 */
var DataGrid = Base.extend({
    /**
     * The object grid
     */
	_grid: null,
    /**
     * The grid configuration specified by the user
     */
    _config: null,
    /**
     * The data source for grid
     */
    _dataSource: null,

    /**
     * Сonstructor grid
     * @param config: he grid configuration specified by the user
     */
    constructor: function (config) {
        this._config = config;
        var configurationGrid = new DataGridConfigurator(config);
        jQuery ("#"+config.container).dxDataGrid(configurationGrid.getConfiguration());
        this._grid = jQuery("#"+config.container).dxDataGrid('instance');
        this._dataSource = configurationGrid.getDataSource();
    },
    
    /**
     * Sets the control height to specified value.
     * @param {height} The height in pixels.
     */
    setHeight: function(height) {
    	this._grid.option('height', height);
    },

    /**
     * group columns in the grid with the specified name
     * @param groupName
     */
    groupBy: function (groupName) {
        /*for (var i = 0; i < this._grid.columnCount(); i++) {
            var column = this._grid.columnOption(i);
            if (column.groupIndex >= 0 && column.groupIndex != null) {
                this._grid.columnOption(i, 'groupIndex', null);
                break;
            }
        }*/
        this._grid.clearGrouping();
        if (groupName != "") {
            var groupBy = groupName
            var splitID = groupName.split('.');
            if (splitID.length != 1) {
                groupBy = splitID[1];
            }
            this._grid.columnOption(groupBy, 'groupIndex', 1);
        }

    },

    /**
     * Returns all currently selected row data objects on the current page. Each object contains data values
     * @returns {Array}
     */
    getSelectedRows: function () {
        var selectionData = this._grid.getSelectedRowsData();
        var result = [];
        for (var i = 0; i < selectionData.length; i++) {
            var item = selectionData[i];
            var isSelect = this._isSelectedRow(item[this._dataSource.getPrimaryKey()]);
            if (isSelect) {
                delete item['record'];
                result.push(item);
            }
        }
        return result;
    },

    /**
     * Returns data records for all currently selected rows on the current page.
     * @returns {Array}
     */
    getSelectedRecords: function () {
        var selectionData = this._grid.getSelectedRowsData();
        var result = [];
        for (var i = 0; i < selectionData.length; i++) {
            var item = selectionData[i];
            var isSelect = this._isSelectedRow(item[this._dataSource.getPrimaryKey()]);
            if (isSelect) {
                result.push(item.record);
            }
        }
        return result;
    },

    /**
     * Returns all currently selected row data objects on all pages. Each object contains data values
     * @returns {*}
     */
    getAllSelectedRows: function () {
        var data = this._grid.getSelectedRowsData();
        var result = [];
        for (var i = 0; i < data.length; i++) {
            var item = data[i];
            delete item['record'];
            result.push(item);
        }
        return result;
    },

    /**
     * Returns data records for all currently selected rows on all pages.
     * @returns {Array}
     */
    getAllSelectedRecords: function () {
        var data = this._grid.getSelectedRowsData(),
            result = [];
        for (var i = 0; i < data.length; i++) {
            result.push(data[i].record);
        }
        return result;
    },

    /**
     * Clears all selections across all pages.
     */
    clearAllSelected: function () {
        this._grid.clearSelection();
    },

    /**
     * To refresh the grid
     * @param restriction: special data source settings
     */
    refresh: function (restriction) {
        if (restriction) {
            this._dataSource.setRestriction(restriction);
        }
        this._grid.refresh();
    },

    /**
     * Add a special parameter for the data source
     * @param field: field parameter
     * @param value: value parameter
     */
    addParameter: function (field, value) {
        this._dataSource.getViewDataSource().addParameter(field, value);
    },

    updateColumn: function (columnIndex, option, value) {
        this._grid.columnOption(columnIndex, option, value);
    },

    addColumn: function (option) {
        this._grid.addColumn(option);
    },

    beginUpdate: function () { this._grid.beginUpdate() },
    beginCustomLoading: function () {this._grid.beginCustomLoading()},
    endCustomLoading: function () {this._grid.endCustomLoading()},
    endUpdate: function () { this._grid.endUpdate()},

    /**
     * Adds a listener to specified control event.
     * @param {eventName} The name of the control event, e.g. 'click' or 'locateBuilding'.
     * @param {listener} The listener function reference.
     */
    addEventListener: function(eventName, listener) {},
    
    /**
     * Removes listeners from specified control event.
     * @param {eventName} The name of the control event, e.g. 'click' or 'locateBuilding'.
     * @param {listener} The listener function reference. Optional. If not specified, the control removes all event listeners for this event. 
     */
    removeEventListener: function(eventName) {},

    /**
     * Check whether  have selected the specified  row
     * @param key: key row
     * @returns {true/false}
     * @private
     */
    _isSelectedRow: function (key) {
        var pageData = this._dataSource.getPageData();
        var dataSourceKey = this._dataSource.getPrimaryKey();
        var isSelect = false;
        for (var j = 0; j < pageData.length; j++) {
            if (pageData[j][dataSourceKey] == key) {
                isSelect = true;
                break;
            }
        }
        return isSelect;
    },

}, {
    
});
