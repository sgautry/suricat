/***
 * Defines HTML5 Base Chart Control from which Pie Chart Control (PieChartControl) and Serial Chart Control (SerialChartControl) inherit from.
 */
var BaseChartControl = Base.extend({

    //unique id for the chart control
	id: '',
	
	//config object
	config: null,
	
	// chart canvas in type of AmCharts.AmPieChart or AmCharts.AmSerialChart
	canvas: null,
	
	// chart data in JSON array.
	data: [],
	
	//default prefix for big numbers
	prefixesOfBigNumbers: [{number:1e+3,prefix:"k"},{number:1e+6,prefix:"M"},{number:1e+9,prefix:"G"}],
	
	isBuilt: false,
	
	 /**
     * Default constructor.
     * 
     * @param {id}   		chart's unique id.
     * @param {configObject}    a ChartConfig object with the configuration parameters
     */
	constructor: function(id, configObject){

		this.id = id;
		
		this.config = configObject;
	},
	
    /**
     * Builds the chart.
     */
	build: function(){
		
		// for secondary grouping, only build if there is data.
		if(this.config.secondaryGroupingAxis && this.config.secondaryGroupingAxis.length > 1 && this.data.length <1)
			return;
		
		if(this.config.showOnLoad){
			var dataSet = this.data;
			if(this.data.length == 0){
				dataSet = this.getEmptyDataSet();
			}
		
			//reverse the bar category data to match Flash behavior
			if(this.isBarType()){
				this.canvas.dataProvider = dataSet.reverse();
			} else {
				this.canvas.dataProvider = dataSet;
			}
		} else
			this.canvas.dataProvider = [];
		
		this.canvas.colors = this.config.colors;
		
		// AXES
	    // category
	    this.setGroupingAxis();
		 
	    // Serial Chart Only
	    // in case you don't want to change default settings of value axis,
	    // you don't need to create it, as one value axis is created automatically.
	    this.setValueAxis();
	    
	    if(!this.config.secondaryGroupingAxis || this.config.secondaryGroupingAxis.length < 1){
		    // GRAPH
		    this.addDataSeries();
	    } else {
	    	//set spacing to 0 for the same category
	    	this.canvas.columnSpacing = 0;
	    	
	    	this.addSecondaryGroupingDataSeries();
	    }
	    
	    // call validateData to redraw after all your graphs have been pushed onto the graphs array
	    this.canvas.validateData();
	    
	    //pie only
	    this.setOutlineProperties();

	    // CURSOR
	    this.addCursor();
	    
	    // LEGEND
		this.addLegend();

		if(this.config.showOnLoad && this.config.showLegendOnLoad)
			this.showLegend(true);
		else
			this.showLegend(false);
		
	    this.addExportButton();
	    
	    this.setUnits();
	    
	    //set path to image folder
	    if(this.canvas.AmExport && this.canvas.AmExport.chart){
	    	this.canvas.AmExport.chart.pathToImages = "/archibus/schema/ab-core/libraries/amcharts/images/";
	    }
	    
	    this.canvas.write(this.id);
	    
	    this.addZoomListener();
	    
        // fit the chart into the container
	    this.syncHeight();
	   
	    if(this.config.hidden)
			//do not display the chart
	    	document.getElementById(this.id).style.display="none";
	    else {
		    //set background color
		    //AmCharts recommend setting background color directly on a chart's DIV instead of using canvas's backgroundColor property.
		    document.getElementById(this.id).style.backgroundColor = this.config.backgroundColor;
	    }
	    
	    if(this.canvas.dataProvider.length == 0 && this.config.showOnLoad){
	    	// add label
	    	this.addEmptyMessage();
	    }
	    this.isBuilt = true;
	},
	
	getBalloonText: function(){
		
	},
	
	getEmptyDataSet: function(){
		return [];
	},
	
	isPieType: function(){
		return (this.config.chartType==this.config.CHARTTYPE_PIE || this.config.chartType==this.config.CHARTTYPE_PIE_3D);
	},
	
	isBarType: function(){
		return (this.config.chartType==this.config.CHARTTYPE_BAR || this.config.chartType==this.config.CHARTTYPE_STACKEDBAR || this.config.chartType==this.config.CHARTTYPE_BAR_3D);
	},
	
	addEmptyMessage: function(){
	},
	
	/**
	 * set custom fill colors. Only solid fill is support as of 23.2.
	 */
	setSolidFillColors: function(colors){
		if (valueExists(colors) && colors.length > 0) {
			var newColors = [];
			for(var index = 0; index < colors.length; index++){
				var color = colors[index];
				if(color){
					newColors[newColors.length] = color.replace("0x", "#");
				}
			}
			this.canvas.colors = newColors;
		} else {
			//use default colors instead
			this.canvas.colors = this.config.colors;
		}
		
		// have to use 'validateData' instead of 'validateNow' for pie chart
		this.canvas.validateData();
	},
	
	/**
	 * Serial only function - set custom title for data axis
	 */
    setDataAxisTitle: function(title) {
	},
    
    /**
     * Set the data array for the chart and refresh the legend.
     * 
     * An example of the data format is:
     * 			[{'rm.dv_id':'(no value)','rm.area':'153314'}, 
	 *           {'rm.dv_id':'ACCESSORIES','rm.area':'3556'}, 
	 *           {'rm.dv_id':'ELECTRONIC SYS.','rm.area':'13495'},
	 *           ...
	 *          ] 
     *			 
     * @param {data}   		an array of data to set.
     */
	setData: function(data){
		
		this.data = data;

		//for secondary grouping or multiple data axis, we will need to rebuild as dataAxis and secondaryGroupingAxis could change
		if(this.config.dataAxis.length > 0 || (this.config.secondaryGroupingAxis && this.config.secondaryGroupingAxis.length > 0)){
			this.build();
		}
		
		var dataSet = this.data;
		if(this.data.length == 0){
			dataSet = this.getEmptyDataSet();
		}
		
		//reverse the bar category data to match Flash behavior
		if(this.isBarType()){
			this.canvas.dataProvider = dataSet.reverse();
		} else {
			this.canvas.dataProvider = dataSet;
		}

		if(!this.isBuilt)
			this.build();
		
		if(this.data && this.data.length > 0){
			if(this.config.secondaryGroupingAxis.length > 0)
				this.addSecondaryGroupingDataSeries();
		}
		
		this.canvas.clearLabels();
		
		if(this.data.length == 0){
			this.addEmptyMessage();
	    } else {
	    	if(this.isPieType()){
	    		this.canvas.alpha = 1;
				this.canvas.labelsEnabled = this.config.showLabels;
	    	} else
	    		// set opacity of the chart div
	    	    this.canvas.chartDiv.style.opacity = 1;
	    }
    	this.canvas.validateData();
	},
	
	/**
	 * Shows or Hides the legend.
	 */
	showLegend: function(show){
		this.canvas.legendDiv.style.display = (show ? "" : "none");
	},
	
	/**
	 * Sets the unit prefixes (such as $) based on the config parameter showUnitPrefixes.
	 */
	setUnits: function(){
		if(this.config.showUnitPrefixes){
			this.canvas.usePrefixes = true;
			this.canvas.precision = parseInt(this.config.unitPrecision);
		} else {
			this.canvas.usePrefixes = false;
		}
	},
	
	/**
	 * Adds the export to JPG or PNG button based on the config parameter "showExportButton".
	 */
	addExportButton: function(){
		this.canvas.export = {};
	    this.canvas.export.enabled = true;
	    this.canvas.export.libs =  {"path": "/archibus/schema/ab-core/libraries/amcharts/plugins/export/libs/",
									"autoLoad": true};
	    if(this.config.showExportButton){
	    	this.canvas.export.menu = [ {
	    	      "class": "export-main",
	    	      "menu": ["PNG", "JPG", "PDF" ]
	    	    } ];
	    } else {
	    	this.canvas.export.menu = [];
	    };
	
	    //position export button lower to avoid overlapping with "show all" zoomable link
	    if(this.config.zoomable && ((this.canvas.legendDiv.style.display !== 'none' && this.config.legendLocation!=="top"&& this.config.legendLocation!=="right") || this.config.legendLocation=="bottom" || this.config.legendLocation=="left")){
		    this.canvas.export.position = "custom-position";
	    }
    },

    /**
	 * Gets the area or length unit title for the data axis with the specified index.
	 * @param {index} the index of the data axis
	 */
	getUnitTitle: function(index){
		if(this.config.showUnitSuffixes && this.config.dataAxis[index].unitTitle != undefined && this.config.dataAxis[index].unitTitle != '')
			return " " + this.config.dataAxis[index].unitTitle;
		else
			return "";
	},
	
	/**
	 * Adds event listener for the chart canvas
	 */
	addEventListener: function(eventName, listener){
		this.canvas.addListener(eventName, listener);
	},
	
	/**
	 * Removes event listener for the chart canvas
	 */
	removeEventListener: function(eventName, listener){
		this.canvas.removeListener(eventName, listener);
	}, 
	
	/**
	 * sync height according to the container
	 */
	syncHeight: function(){
		
		//KB# 3044780 - workaround IE9/IE10 issue with the chart does not display on the first time issue.
		this.canvas.validateNow();
		
		this.canvas.invalidateSize();
	},
	
	setLegendHeight: function(availableHeight){
        var legendDiv = document.getElementsByClassName('amcharts-legend-div');
        if(legendDiv && legendDiv[0]){
        	switch(this.config.legendLocation){
        	case "top":
        		legendDiv[0].style.maxHeight = (availableHeight/4 + "px");
                break;
        	case "bottom":
        		legendDiv[0].style.maxHeight = (availableHeight/4 + "px");
                break;
        	default:    	
        		legendDiv[0].style.maxHeight = (availableHeight + "px");
        		legendDiv[0].style.maxWidth = "300px";
        	}
        }
	}
});
