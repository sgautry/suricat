/**
 * Defines Pie Chart Control such as Pie chart and 3D pie chart.
 */
var PieChartControl = BaseChartControl.extend({
	
	 /**
     * Default constructor.
     * 
     * @param {id}   		chart's unique id.
     * @param {configObject}    a ChartConfig object with the configuration parameters
     */
	constructor: function(id, configObject){

		this.inherit(id, configObject);
		
		this.canvas = new AmCharts.AmPieChart();
		
		if(this.config.chartType == this.config.CHARTTYPE_PIE_3D){
			this.canvas.depth3D = 20;
			this.canvas.angle = 30;
		} else {
			this.canvas.depth3D = 0;
			this.canvas.angle = 0;
		}
		
		this.canvas.prefixesOfBigNumbers = this.prefixesOfBigNumbers;
		
		this.canvas.decimalSeparator = strDecimalSeparator;
		this.canvas.thousandsSeparator = strGroupingSeparator;


	},
	
	/**
	 * Sets the grouping axis field and its properties through the chart's configuration object.
	 * Each pie chart contains only one grouping axis.
	 */
	setGroupingAxis: function(){
		//add grouping field
	    this.canvas.titleField = this.config.groupingAxis[0].id;
	},

	
	/**
	 * Adds the data series field and its properties through the chart's configuration object.
	 * Each pie chart contains only one data axis.
	 */
	addDataSeries: function(){
		// add value field
	    this.canvas.valueField = this.config.dataAxis[0].id;
	     
	    //Specifies whether data labels are visible.
		this.canvas.labelsEnabled = this.config.showLabels;
		
		//tool tips and add the currency code if there is any
		if(this.config.showDataTips)
			this.canvas.balloonText = this.getBalloonText();
		else
			this.canvas.balloonText = "";
	},
	
	getBalloonText: function(){
		
		var dataTipsFormat = 'full';
		if(this.config.dataTipsFormat && this.config.dataTipsFormat.length > 0){
			dataTipsFormat = this.config.dataTipsFormat;
		}
		
		var balloonText = "";
		if(dataTipsFormat === 'full'){
			balloonText = "<b>[[title]]: [[percents]]%</b><br>(<i>" + this.config.dataAxis[0].currencyCode + "[[value]]" + this.getUnitTitle(0) + "</i>)";
		} else if(dataTipsFormat === 'compact'){
			balloonText = "<span style='font-size:9px'><b>[[title]]: [[percents]]%</b>(<i>" + this.config.dataAxis[0].currencyCode + "[[value]]" + this.getUnitTitle(0) + "</i>)</span>";
		} else {
			balloonText = dataTipsFormat;
		}
		
		return balloonText;
	},
	
    getEmptyDataSet: function(){
    	
    	var dataSet = [];
		for(var index = 0; index < 3; index++){
			var dataPoint = {};
			dataPoint[this.config.groupingAxis[0].id] = '';
			dataPoint[this.config.dataAxis[0].id] =1;
			dataSet.push(dataPoint);
		}
		return dataSet;
	},
	
	addEmptyMessage: function(){
		// disable slice labels
		this.canvas.labelsEnabled = false;
		
		var dataPoint = {dummyValue: 0};
		dataPoint[this.canvas.categoryField] = '';
		this.canvas.dataProvider = [dataPoint];
	    
	    // add label to let users know the chart is empty
	    this.canvas.addLabel(0, '50%', View.getLocalizedString(this.z_MESSAGE_NO_DATA), 'center');
	    
	    // dim the whole chart
	    this.canvas.alpha = 0.3;
	    this.canvas.validateNow();
	},
	
	/**
	 * Adds the legend through the chart's configuration object.
	 */
	addLegend: function(){

	    var legend = new AmCharts.AmLegend();
	    legend.position = this.config.legendLocation;
	    legend.align = "center";
	    legend.markerType = "circle";
	    legend.valueText = "";

	    this.canvas.addLegend(legend);
	    
		this.showLegend(this.config.showLegendOnLoad);
	},
	
	/**
	 * Sets outlines properties
	 */
	setOutlineProperties: function(){
		// set outline properties
		this.canvas.outlineColor = "#FFFFFF";
		this.canvas.outlineAlpha = 0.8;
		this.canvas.outlineThickness = 2;
	},
	

	/**
	 * Serial chart only function
	 */
	setValueAxis: function(){
		// no value axis to set
	},

	/**
	 * Serial chart only function
	 */
	addZoomListener: function(){
		//no zooming
	},
	

	/**
	 * Serial chart only function
	 */
	addCursor: function(){

	},
	// @begin_translatable
	z_MESSAGE_NO_DATA: 'No data available in chart'
	// @end_translatable
});
