/**
 * Defines Serial Chart Control such as column, bar, line, plot, area, column/line, stacked bar/column/area charts and their related 3D charts.
 */
var SerialChartControl = BaseChartControl.extend({
	
	//flag if the control has initialized, this is introduce to prevent the chart to zoom at the first time it is loaded.
	initialized: false,
	
	//switch to show category label up/down
	upCatogory: false,
	
	 /**
     * Default constructor.
     * 
     * @param {id}   		chart's unique id.
     * @param {configObject}    a ChartConfig object with the configuration parameters
     */
	constructor: function(id, configObject){

		this.inherit(id, configObject);
		
		this.canvas = new AmCharts.AmSerialChart();
		//this.canvas.synchronizeGrid = true;
		
		if(this.isBarType()){
			this.canvas.rotate = true;
		}	
		
		if(this.config.chartType == this.config.CHARTTYPE_COLUMN_3D ||  this.config.chartType == this.config.CHARTTYPE_BAR_3D){
			this.canvas.depth3D = 20;
			this.canvas.angle = 30;
		} else {
			this.canvas.depth3D = 0;
			this.canvas.angle = 0;
		}
		this.canvas.prefixesOfBigNumbers = this.prefixesOfBigNumbers;
		this.canvas.decimalSeparator = strDecimalSeparator;
		this.canvas.thousandsSeparator = strGroupingSeparator;

	},
	
	/**
	 * Sets the grouping axis field and its properties through the chart's configuration object.
	 * Each chart contains only one grouping axis.
	 */
	setGroupingAxis: function(){
		
		this.canvas.categoryField = this.config.groupingAxis[0].id;
		
		var categoryAxis = this.canvas.categoryAxis;
	    categoryAxis.labelRotation = this.config.groupingAxis[0].labelRotation;
	    categoryAxis.gridPosition = "start";
	    categoryAxis.startOnAxis = this.config.groupingAxis[0].startOnAxis;
	
	    // show labels up and down to avoid overlapping, minic Flash behavior    
	    if(categoryAxis.labelRotation == 0 && !this.isBarType()){
	    	categoryAxis.labelFunction =  this.formatCategoryLabels;
	    }
	
	    categoryAxis.labelsEnabled = this.config.groupingAxis[0].showLabel;
	    
	    if(this.config.groupingAxis[0].showTitle)
	    	categoryAxis.title = this.config.groupingAxis[0].title;
	    else
	    	categoryAxis.title = "";
	    	
	},
	
	formatCategoryLabels: function(value, valueString, axis){
	    if(this.upCatogory || this.chart.chartData.length < 10) {
	        axis.labelOffset=0;
	    }
	    else {
	        axis.labelOffset=25;
	    }
	    this.upCatogory=!this.upCatogory;

	    return value;
	},
	
	/**
	 * Sets the value axis properties through the chart's configuration object.
	 * Each value axis corresponds to one data series.
	 */
	setValueAxis: function(){
		
		var stacked = false;
		if(this.config.chartType==this.config.CHARTTYPE_STACKEDBAR || this.config.chartType==this.config.CHARTTYPE_STACKEDAREA || this.config.chartType==this.config.CHARTTYPE_STACKEDCOLUMN)
			stacked = true;
		
		var barType = this.isBarType();
		
		//record the actual index of the axes that are shown.
		var showIndex = -1;
		
		for(var index=0; index < this.config.dataAxis.length; index++){
			var valueAxis = this.config.dataAxis[index].valueAxis;
			var displayAxis = (index==0) || (valueAxis && valueAxis.displayAxis);
			
			if(displayAxis)
				showIndex++;
			
			if(!valueAxis){
				valueAxis = new ValueAxis();
			}
			
			if(index==0){
				var valueAxisTitle = this.config.dataAxis[0].valueAxis.title;
				if(typeof(valueAxisTitle) != 'undefined' && valueAxisTitle != '')
					valueAxis.title = valueAxisTitle;
				else
					valueAxis.title = this.config.dataAxis[0].title;
			} else {
				valueAxis.title = this.config.dataAxis[index].title;
			}	
				
			if(valueAxis && !(index>0 && stacked)){
				var chartValueAxis;
				if(index==0 && this.canvas.valueAxes[0]){
					chartValueAxis = this.canvas.valueAxes[0];
				} else {
					chartValueAxis = new AmCharts.ValueAxis();
				}
				
				if(typeof displayAxis !== 'undefined' && displayAxis){
					chartValueAxis.id = "v" + index;
				}
				
				// for value axis that do not display, sync against the first value axis
				if(index > 0 && (typeof displayAxis === 'undefined' || !displayAxis )){
					chartValueAxis.synchronizeWith = this.canvas.valueAxes[0];
					chartValueAxis.synchronizationMultiplier = 1;
				}
				
				//axis is place to left for even axis, left for odd axis.
				if(barType){
					chartValueAxis.position = (showIndex % 2 ? "top" : "bottom");
				} else {
					chartValueAxis.position = (showIndex % 2 ? "right" : "left");
				}
				
				chartValueAxis.offset = Math.floor(showIndex/2)*80; 

				//set stack type
				if(stacked)
					chartValueAxis.stackType = "regular";
				else 
					chartValueAxis.stackType = "none";
				
				if(this.config.dataAxis[index].showTitle)
					chartValueAxis.title = valueAxis.title;
				else
					chartValueAxis.title = "";
					
				chartValueAxis.gridAlpha = valueAxis.gridAlpha;
				chartValueAxis.axisAlpha = valueAxis.axisAlpha;
				
				chartValueAxis.unit = this.config.dataAxis[index].currencyCode; 
				chartValueAxis.unitPosition = "left";
				
				if(this.config.showUnitPrefixes){
					chartValueAxis.precision = parseInt(this.config.unitPrecision);
				}
				
				chartValueAxis.labelsEnabled = this.config.dataAxis[index].showLabel;
				
				chartValueAxis.visible = (typeof displayAxis !== 'undefined' && displayAxis ? true : false);
				
				chartValueAxis.axisCreated = true;

				if(this.canvas.valueAxes[index] && this.canvas.valueAxes[index].axisCreated){
					this.canvas.valueAxes[index] == chartValueAxis; 
				} else {
					this.canvas.addValueAxis(chartValueAxis);
				}
			}
		}
	},
	
	/**
	 * set custom title for data axis
	 */
    setDataAxisTitle: function(title) {
		if(this.canvas && this.canvas.valueAxes){
			this.canvas.valueAxes[0].title = title;
			this.canvas.validateNow();
		}
    },
	
    getEmptyDataSet: function(){
    	var dataPoint = {};
		dataPoint[this.config.groupingAxis[0].id] = ' ';
		for(var index = 0; index < this.config.dataAxis.length; index++){
			dataPoint[this.config.dataAxis[index].id] = 0;
		}
		var dataSet = [];
		dataSet.push(dataPoint);
		return dataSet;
	},
	
	addEmptyMessage: function(){
	    // add label to let users know the chart is empty
	    this.canvas.addLabel("50%", "50%", "The chart contains no data", "middle", 15);
		
	    // set opacity of the chart div
	    this.canvas.chartDiv.style.opacity = 0.5;
	},
	
	/**
	 * Adds the data series field and its properties through the chart's configuration object.
	 * You can call the API multiple times to add more than one data series.
	 */
	addDataSeries: function(){	    
		for(var index=0; index < this.config.dataAxis.length; index++){
			var graph = new AmCharts.AmGraph();
			
			graph.title = this.config.dataAxis[index].title;
			graph.valueField = this.config.dataAxis[index].id;
			
			//get currency code
			var currencyCode = this.config.dataAxis[index].currencyCode;
            var unitTitle = this.getUnitTitle(index);
            
			//display data label in chart
			if(this.config.showLabels)
				graph.labelText = currencyCode + "[[value]]" + unitTitle;
			else
				graph.labelText = "";
			
			
			if(this.config.showDataTips)
				graph.balloonText = this.getBalloonText(currencyCode, unitTitle);
			else
				graph.balloonText = "";
			
			if(typeof(this.config.dataAxis[index].type) == undefined){
				this.config.dataAxis[index].type = 'column';
			}

			graph.type = this.config.dataAxis[index].type;
				
			graph.lineAlpha = this.config.dataAxis[index].lineAlpha;
			graph.fillAlphas = this.config.dataAxis[index].fillAlphas;
			graph.lineThickness = this.config.dataAxis[index].lineThickness;

			
			graph.bullet =  this.config.dataAxis[index].bullet;
			graph.bulletSize = 1;
			graph.bulletAlpha =  this.config.dataAxis[index].bulletAlpha;
			
			graph.valueAxis = "v" + index;
			
			if(this.canvas.graphs[index]){
				this.canvas.graphs[index] = graph;
			} else {
				this.canvas.addGraph(graph);
			}
			
			if(this.config.dataAxis[index].dataSourceId==''){
		    	this.config.dataAxis[index].dataSourceId = this.config.dataSourceId;
		    }
		}
	},
	
	addSecondaryGroupingDataSeries: function(){
		var data = this.canvas.dataProvider[0];
        var groupingAxisId = this.config.groupingAxis[0].id;
		var dataAxis = this.config.dataAxis[0];
		
		this.canvas.graphs = [];
		
		for(var key in data){
			if(key == groupingAxisId)
				continue;
			
			for(var index = 0; index < this.config.dataAxis.length; index++){
				if(key == this.config.dataAxis[index].id){
					dataAxis = this.config.dataAxis[index];
					break;
				}
			}	
					
			var graph = new AmCharts.AmGraph();
			graph.title = key;
			graph.valueField = key;
			
			//display data label in chart
			if(this.config.showLabels)
				graph.labelText = "[[value]]";
			else
				graph.labelText = "";
			
			if(this.config.showDataTips)
				graph.balloonText = this.getBalloonText();
			else
				graph.balloonText = "";

			graph.type = dataAxis.type;
			graph.lineAlpha = (typeof dataAxis.lineAlpha === 'undefined' ? 0.8 : dataAxis.lineAlpha);
			graph.fillAlphas = (typeof dataAxis.fillAlphas === 'undefined' ? 0.3 : dataAxis.fillAlphas);
			graph.lineThickness = (typeof dataAxis.lineThickness === 'undefined' ? 1 : dataAxis.lineThickness);
			
			if(graph.type == 'line'){
				graph.fillAlphas = 0;
			} else {
				graph.fillAlphas = dataAxis.fillAlphas;		
			}
			
			graph.bullet =  dataAxis.bullet;
			graph.bulletSize = (typeof dataAxis.bulletSize == 'undefined' ? 1 : dataAxis.bulletSize);
			graph.bulletAlpha =  dataAxis.bulletAlpha;
			
			this.canvas.graphs.push(graph);
		}
	},
	
	getBalloonText: function(currencyCode, unitTitle){
		
		var dataTipsFormat = 'full';
		if(this.config.dataTipsFormat && this.config.dataTipsFormat.length > 0){
			dataTipsFormat = this.config.dataTipsFormat;
		}
		
		var hasSecondaryGrouping = (this.config.secondaryGroupingAxis && this.config.secondaryGroupingAxis.length > 0);
		
		var balloonText = "";
		if(dataTipsFormat === 'full'){
			if(hasSecondaryGrouping){
				balloonText = "<b>[[title]]</b><br>[[category]]<br>[[value]] ([[percents]]%)<br><i>Total:</i> [[total]]";
			} else {
				balloonText = "<b>[[title]]</b><br>[[category]]<br><b>" +  currencyCode + "[[value]]" + unitTitle + "</b>";
			}
		} else if(dataTipsFormat === 'compact'){
			if(hasSecondaryGrouping){
				balloonText = "<span style='font-size:9px'><b>[[title]]</b> [[category]]: <b>[[value]]</b></span>";
			} else {
				balloonText = "<span style='font-size:9px'><b>[[title]]</b> [[category]]: <b>" +  currencyCode + "[[value]]" + unitTitle + "</b></span>";
			}
		} else {
			balloonText = dataTipsFormat;
		}
		
		return balloonText;
	},
	
	/**
	 * Adds chart cursor. 
	 */
	addCursor: function(){
    	// in order for the chart to zoom in Mobile device, you will need to have chartCursor and panEvent enabled.
	    if(this.config.showDataTips || this.config.zoomable){
	    	var chartCursor = new AmCharts.ChartCursor();
		    chartCursor.cursorAlpha = 0;
		    chartCursor.zoomable = this.config.zoomable;
		    chartCursor.cursorPosition = "mouse";
		    chartCursor.categoryBalloonEnabled = false;
		    if(this.config.showOneBalloonOnly){
		    	chartCursor.valueBalloonsEnabled = false;
		    	chartCursor.oneBalloonOnly = true;
		    } else {
		    	chartCursor.valueBalloonsEnabled = true;
			    chartCursor.avoidBalloonOverlapping = true;
		    }
		    this.canvas.addChartCursor(chartCursor);
		} else {
	    	this.canvas.removeChartCursor();
		}
	},
	
	/**
	 * Adds the legend through the chart's configuration object.
	 */
	addLegend: function(){

	    var legend = new AmCharts.AmLegend();
	    legend.useGraphSettings = true;
	    // Known AmCharts Library Issue: legend does not show if it is not "bottom"
		legend.position = this.config.legendLocation;
	    //legend.position ="bottom";
		legend.valueText = "";
		    
		this.canvas.addLegend(legend);
	},
	
	/**
	 * Add the event listener for zooming.
	 */
	addZoomListener: function(){
		// listen for "dataUpdated" event (fired when chart is rendered) and call zoomChart method when it happens
	    if(this.config.zoomable){
	    	this.canvas.addListener("dataUpdated", this.zoomChart);
	        
	    	// in order for the chart to zoom in Mobile device, you will need to have chartCursor and panEvent enabled.
	    	this.canvas.panEventsEnabled = true;
	    }
	    
	},
	
	
	/**
	 * Changes cursor mode from pan to select - called when zooming
	 */
	setPanSelect: function() {
	    if (document.getElementById("rb1").checked) {
	        this.canvas.chartCursor.pan = false;
	        this.canvas.chartCursor.zoomable = true;
	        
	    } else {
	    	this.canvas.chartCursor.pan = true;
	    }
	    this.canvas.validateNow();
	} ,
	

	/**
	 * Zooms the chart. This method is called when chart is first initialized or/and as we listen for "dataUpdated" event
	 */
	zoomChart: function(event) {
	    // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
		var eventChart = event.chart;

		// only zoom AFTER the data loaded the first time
		if(eventChart && this.initialized){
			eventChart.zoomToIndexes(eventChart.chartData.length - 40, eventChart.chartData.length - 1);
			
		}
	},
	
	/**
	 * Pie chart only function
	 */
	setOutlineProperties: function(){
	}
});
