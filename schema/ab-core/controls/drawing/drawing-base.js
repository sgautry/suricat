/**
 * Defines a single SVG drawing base class.
 * 
 * @author Ying Qin
 */
var DrawingBase = Base.extend({

	/**
	 * Configuration.
	 */
	config: {},
	
	/**
	 * Is drawing done loading? true if yes, false otherwise
	 */
	isDrawingLoaded: false,
	
	/**
	 * Reference to drawingController object to access to all controllers and add-ons.
	 */
	drawingController: null,
	
	/**
     * Constructor.
     *
     * @param config configObject
     * @param drawingController reference to DrawingController.
     */
    constructor: function(config, drawingController) {
    	
        this.inherit(drawingController.config.divId, 'html', config);
        
        this.config = config;
        this.config.divId = drawingController.config.divId;
	
        this.drawingController = drawingController;
    },
    
    /**
     * Retrieves the SVG content from server based the specified parameters then displays it, register user-defined event handlers as needed.
     * 
     * @return boolean true if the drawing is loaded successfully, false otherwise.
     */
    load: function() {
    	this.isDrawingLoaded = false;
    	
        var svgText  = this.getSvgText();
		        
        this.processSvg(svgText);
        
        return this.isDrawingLoaded;
    },
    
    
    /**
     * Removes SVG content and adjust layout. Reset pop-up layers' list if any.
     */
    unload: function(){
    	var buildingId = (this.config && this.config.pkeyValues ? this.config.pkeyValues.bl_id : null);
    	var floorId = (this.config && this.config.pkeyValues ? this.config.pkeyValues.fl_id : null);
    	var drawingName = (this.config ? this.config.drawingName : null);
    	
    	if(!buildingId || !floorId || !drawingName){
    		return;
    	}
    	
    	this.isDrawingLoaded = false;
    	
    	// if there is layers popup, then remove the SVG related layers and reset layers list
    	var layersPopup = this.drawingController.getAddOn('LayersPopup');
    	if(layersPopup){
    		layersPopup.showLayers(this.getSvg(), false);
    	}
    
    	//drawing name has be processed by function DrawingCommon.retrieveValidNodeId of drawing-common.js
		
    	var validDrawingName = DrawingCommon.retrieveValidNodeId(drawingName);

    	//remove selected assets.
   		var selectController = this.drawingController.getController("SelectController");
		if(selectController){
        	for(var svgId in selectController.selectedAssets){
        		if(svgId && svgId.indexOf(validDrawingName) >=0){
        			delete selectController.selectedAssets[svgId];
        		}
        	}
		}
	
		//remove highlighted assets
		var highlightController = this.drawingController.getController("HighlightController");
		if(highlightController){
			var highlightedAssets = highlightController.highlightedAssets;
        	for(var assetId in highlightedAssets){
        		var svgId = highlightedAssets[assetId]["svgId"]; 
        		if(svgId && svgId.indexOf(validDrawingName) >=0){
        			delete highlightController.highlightedAssets[assetId];
        		}
        	}
		}
    	
    	
    	this.drawingController.getController("DrawingLayoutController").removeSvg(buildingId, floorId, drawingName);
    	
    	this.isDrawingLoaded = true;

    },
    
    /**
     * Set the config object's drawing name which is used to compose the SVG id.
     * @param drawingName String drawing name.
     */
    setDrawingName: function(drawingName){
    	this.config.drawingName = drawingName;
    },
    
    /**
     * Gets the SVG's <div/> selection.
     * @return object HTMLDivElement
     */
    getDiv: function () {
    	var svgNode = this.getSvg().node();
    	if(svgNode)
    		return d3.select(svgNode.parentNode);
    	else
    		return d3.select("#" + this.config.divId + "-0-0");
    },

    /**
     * Retrieves SVG's parent div id.
     * @return String
     */
    getDivId: function(){
    	return this.getDiv().id;
    },
    
    /**
     * Gets the SVG selection based on config's root <div/> id and drawing name.
     * @return object SVGSVGElement
     */
    getSvg: function () {
   		return d3.select("#" + this.getSvgId());
    },  

    /**
     * Gets the SVG id.
     * @return String
     */
    getSvgId: function () {
   		return DrawingCommon.retrieveValidSvgId(this.config.divId, this.config.drawingName);
    },
    
    /**
	 * Disables label and text selection on the drawing.
	 */
    disableLabelSelection: function () {
    	
    	var svg = this.getSvg();
    	
        svg.select("#asset-labels").classed({'no-selection': true});
        svg.select("#text").classed({'no-selection': true});
    },
    
    /**
     * Retrieves all asset types of the drawing
     */
    retrieveAssetTypes: function(){
    	var svg = this.getSvg();
    	
    	var nodes = svg.select('#assets')[0][0].childNodes;
    	var assetTypes = [];
		for(var i =0; i < nodes.length; i++){
			var node = nodes[i];
			if(node.nodeName == 'g'){
				var id = node.getAttribute("id").replace("-assets", "");
				if(id){
					assetTypes.push(id);
				}
			}
		}
		return assetTypes;
    },
    
    /**
     * Gets SVG XML from server-side by specified parameters.
     * 
     * @return result Object (SVG text) 
     */
    getSvgText: function() {
        var pKeyValues = {};
   		if(typeof this.config.pkeyValues !== 'undefined' && this.config.pkeyValues != null){
            pKeyValues = this.config.pkeyValues;
            if(this.config.drawingName !== undefined && this.config.drawingName !== null){
            	pKeyValues.dwg_name = this.config.drawingName;
            }
        }

        var planType = null;
   		if(typeof this.config.plan_type !== 'undefined' && this.config.plan_type != null){
            planType = this.config.plan_type;
        }

        var highlightParameters = (this.config && this.config.highlightParameters)? this.config.highlightParameters : [];
        
   		var datasourceSelector = this.drawingController.getAddOn('DatasourceSelector');
    	if(datasourceSelector){
    		highlightParameters = datasourceSelector.getHighlightParameters();
    	}
   		
   		var result = null;
        var msg = View.getLocalizedString(this.Z_FAILED_TO_LOAD_MESSAGE) + ' ';
        DrawingSvgService.highlightSvgDrawing(pKeyValues, planType, highlightParameters, {
            async: false,
            callback: function(xml) {
                result = xml;
            },
            errorHandler: function(m) {
                // console.log(msg + m);
            }
        });

        return result;
    },
	
    /**
     * Displays SVG, binds default and user-defined events, and adds or update interactivity components.
     * 
     * @param svgText	String of the SVG contents
     */
    processSvg: function(svgText) {
    	
        var options = {};
        options.buildingId = this.config['pkeyValues']['bl_id'];
        options.floorId = this.config['pkeyValues']['fl_id'];
        options.drawingName = this.config['drawingName'];
        options.allowMultipleDrawings = this.config['allowMultipleDrawings'];
        options.orderByColumn = this.config['orderByColumn'];
        options.callFromDatasourceSelector = this.config['callFromDatasourceSelector'];
        
        this.drawingController.getController("DrawingLayoutController").addSvg(svgText, options);
        
        this.getDiv().classed({'floor-plan': true});
        
        if (svgText) {
        	this.orderLayers();
        	
        	this.setDefaultViewBoxIfNotExists();
        	
        	var svg = this.getSvg();

			var markerAddOn = this.drawingController.getAddOn('Marker');
        	if(markerAddOn){
        		markerAddOn.setSvg(svg);
        	}

			var clusterAddOn = this.drawingController.getAddOn('Cluster');
        	if(clusterAddOn){
        		clusterAddOn.setSvg(svg);
        	}
        	
        	var navToolbarAddOn = this.drawingController.getAddOn('NavigationToolbar');
        	if(navToolbarAddOn){
        		
        		if (clusterAddOn) {
        			navToolbarAddOn.useD3Panning = true;
        			if (!svg.empty()) {
        				navToolbarAddOn.currentSvg = svg;
        			}        			
        		} else {
            		// remove d3 handlers
            		navToolbarAddOn.removeD3Handlers(svg);
        		}

        		navToolbarAddOn.show(true);
        		
        		navToolbarAddOn.updateLinkIcons();
        	}
        	
        	var layersPopup = this.drawingController.getAddOn('LayersPopup');
        	if(layersPopup){
        		layersPopup.showLayers(svg, true);
        	}
            
			var selectWindowAddOn = this.drawingController.getAddOn('SelectWindow');
        	if(selectWindowAddOn){
    			selectWindowAddOn.popluateAssetBBoxMap(svg);
        	} 
 
	        var assetPanelAddOn = this.drawingController.getAddOn('AssetPanel');
        	if(assetPanelAddOn){
        		
        		// embed buildingId and floorId onto drawing ('JFK A' vs. 'JFK__A')
        		svg.attr("buildingId", options.buildingId);
        		svg.attr("floorId", options.floorId);
        		
        		assetPanelAddOn.init(svg, this.config['drawingName']);
        	}
        		        
	        var dsSelector = this.drawingController.getAddOn('DatasourceSelector');
	        if(dsSelector){
	        	this.drawingController.getController("AssetController").patchBackgroundForUseElements(svg.id, true);
	        }

	        var markupAddOn = this.drawingController.getAddOn('MarkupControl');
	        if(markupAddOn){
	        	markupAddOn.loadLegend(svg);

	        	if(this.config.loadOption!==0){
	        		this.drawingController.getController("MarkupController").load(this.config, markupAddOn);
	        	}
	        }
	        
        	this.drawingController.addEventHandlers(svg);
        	
            this.setIsDrawingLoaded(true);
            
            if(this.config.afterDrawingLoad){
            	this.config.afterDrawingLoad(this.config.divId, this.config.drawingName, this.drawingController);
            }
        } else {
            this.setIsDrawingLoaded(false);
        }
        d3.select("#" +  DrawingCommon.retrieveValidNodeId(this.config.divId)).node().focus();

    },
    
    
    /**
     * Setter for 'isDrawingLoaded' property. 
     * @param loaded Boolean true if set to loaded, false otherwise.
     */
    setIsDrawingLoaded: function(loaded) {
        this.isDrawingLoaded = loaded;
    },
    
    /**
     * Sets the default view box parameter if none exists.
     */
    setDefaultViewBoxIfNotExists: function () {
        var svg = this.getSvg(),
            defaultView = d3.select('#' + this.getSvgId() + ' view[id=defaultView]');

        if (defaultView[0][0] === null) {
            svg.append("view").attr("id", "defaultView").attr("viewBox", svg.attr("viewBox"));
        }
    },
    
    /**
     * Re-order the asset layers so that the small assets are placed on the top in order for their related events can fire.
     */
    orderLayers: function(){
    	var layersToSort = [];
    	if(this.config.topLayers)
    		layersToSort = this.config.topLayers.split(";");
    	
    	if(layersToSort.length < 1)
    		return;
    	
    	var validSvgDivId = this.getSvgId();
    	for(var index = 0; index < layersToSort.length; index++){
	    	var assetsLayer = d3.select("#" + validSvgDivId).select("#" + layersToSort[index] + "-assets");
	    	if(assetsLayer && !assetsLayer.empty()){
	    		var clone = d3.select(assetsLayer.node().cloneNode(true));
	    		var parent = assetsLayer.node().parentNode;
	    		parent.removeChild(assetsLayer.node());
	    		d3.select(parent).node().appendChild(clone.node());
	    	}
    	}
    },
    
    /**
     * Creates and return an svg.
     * 
     * @param div       HTMLDivElement <div> selection to hold the svg
     * @param width     Number  Width of the SVG
     * @param height    Number  Height of the SVG
     * @returns SVGSVGElement the svg created.
     */
    createSvg: function(div, width, height) {
    	
    	// div to hold SVG
    	var divId = div.attr("id");
    	
    	// remove the existing svg with the same id.
    	var svgId = this.getSvgId();
    	if (!div.select('#' + svgId).empty()) {
            div.select('#' + svgId).remove();
        }
    	
        var svg = div.append("svg")
            .attr("id", svgId)
            .attr("version", "1.1")
            .attr("xmlns", "http://www.w3.org/2000/svg")
            .attr("xmlns:xmlns:xlink", "http://www.w3.org/1999/xlink")
            .attr("width", width)
            .attr("height", height)
            .attr("viewBox", "0 0 " + width + " " + height);

        svg.append("style");
        svg.append("defs");

        var viewer = svg.append("g")
            .attr("id", "viewer");

        var mirror = viewer.append("g")
            .attr("id", "mirror")
            .attr("stroke-width", "0.9%")
            .attr("transform", "scale(1, 1)");

        mirror.append("g")
            .attr("id", "background")
            .attr("fill", "none");

        mirror.append("g")
            .attr("id", "assets")
            .attr("fill", "none");

        return svg;
    },

    /**
     * Gets the drawing's Image Bytes.
     * 
     * @return image
	 */
    getImageBytes: function(callback){
		var result = '';
		var imageCapture = new ImageCapture();
		var drawingDivId = this.getDiv().node().id;
		imageCapture.captureImage(drawingDivId, false, function(image){
			result = image.substring(22);
			if(callback){
				callback(result);
				return;
			}
 	   	});
		return result;
	},

    /**
     * Loads an image into div.
     * 
     * @param div	HTMLDivElement div to hold the image.
     * @param drawingName   String the drawing name used to compose svg id
     * @param href  String  URL for the image.
     */
    loadImage: function(div, drawingName, href) {
		var control = this;
		var divId = div.attr("id");
		
		// remove the existing svg with the same id.
		var svgId = DrawingCommon.retrieveValidSvgId(divId, drawingName);
		if (!div.select("#" + svgId).empty()) {
		    div.select("#" + svgId).remove();
		}
		
		var img = new Image();
		img.onload = function(){
		      	var svg = control.createSvg(div, this.width, this.height);
		      	var canvas = document.createElement('canvas');
		      	canvas.id = 'LOADIMAGE_TEMP_CANVAS';
		      	var ctx = canvas.getContext('2d');
		      	canvas.height = this.height;
		      	canvas.width = this.width;
		      	ctx.drawImage(this, 0, 0);
		      	var dataURL = canvas.toDataURL( 'image/png');
		      	d3.select("#"+canvas.id).remove();
		          
		        // create image
		      	var g = svg.select("#background").append("g");
		        g.append("image")
		             .attr("id", div.node().id + "_image")
		             .attr("x", 0)
		             .attr("y", 0)
		             .attr("width", '100%')
		             .attr("height", '100%')
		             .attr("xlink:href", dataURL)
		             .html(".");
          }
          
          img.src = href; 
    }
});


