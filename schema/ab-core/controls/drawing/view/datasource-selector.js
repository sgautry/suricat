Drawing.namespace("view");

/**
 * This is temporary and should be replaced with a Drawing Option pop up with datasource selector/label selector, legend, asset layer etc. 
 */

//TODO: provide reset select options objects, customized event handler methods

//TODO: how developers could customize it - their own selector option list or event handler???
Drawing.view.DatasourceSelector = Base.extend({
	// @begin_translatable
	z_MESSAGE_HIGHLIGHTS: 'Highlights',
	z_MESSAGE_LABELS: 'Labels',
	z_MESSAGE_BORDERS: 'Borders',
    z_MESSAGE_BORDER_HIGHLIGHTS: 'Border Highlights',
    z_MESSAGE_ROOM_HIGHLIGHTS: 'Room Highlights',
	z_MESSAGE_TOOLTIP_LEGEND: 'Legend',
	// @end_translatable
	
	legendButtonHtml: '<td id="ext-gen571"><table class="x-btn-wrap x-btn  x-btn-icon " id="{0}" title="{1}" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="x-btn-left"><i>&nbsp;</i></td><td class="x-btn-center" id="ext-gen708"><em unselectable="on"><button class="x-btn-text" type="button" id="ext-gen573" style="background-image: url(&quot;/archibus/schema/ab-core/graphics/icons/legend.png&quot;);">&nbsp;</button></em></td><td class="x-btn-right"><i>&nbsp;</i></td></tr></tbody></table></td>',
	
	config: null,
	
	//reference to drawing controller
	drawingController: null,

	//TODO: how to pass runtime dataSource parameters??
	highlightParameter: null,
	borderHighlightParameter: null,
	
	constructor: function(config) {
		this.config = config;
		
		this.borderHighlightParameter = {};
		
		//XXX: this.config.highlightParameters[0].view_file is required
		this.highlightParameter = (this.config.highlightParameters ? this.config.highlightParameters[0] : {});
		
		this.config.highlightSelector = (this.config.highlightSelector ? this.config.highlightSelector : true);
		this.config.labelSelector = (this.config.labelSelector ? this.config.labelSelector : true);
		
		this.config.bordersHighlightSelector = (this.config.bordersHighlightSelector ? this.config.bordersHighlightSelector : false);
		
		var titleNode = document.getElementById(this.config.addOnsConfig['DatasourceSelector'].panelId + '_title');
        this.appendDatasourceSelectors(titleNode.parentNode.parentNode);
	},
	
	 /**
     * set reference to drawingController
     */
    setDrawingController: function(drawingController){
    	this.drawingController = drawingController;
    },
    
	
	/**
	 * retrieve current highlight parameter from the datasource selector, if any.
	 */
    getHighlightParameters: function(parameters){
		if(this.highlightParameter){
			var highlightElem = Ext.get('selector_hilite');
	 		var highlightDs = (highlightElem && highlightElem.dom.value ? highlightElem.dom.value : '')
	        if(valueExistsNotEmpty(highlightDs)){
	        	this.highlightParameter.hs_ds = highlightDs;
			}
	 		
	 		//XXX: pass dataSource parameters if they're defined in dataSource
	 		//this.highlightParameter.hs_param = toJSON({occupancy: "rm.rm_std='WKSTA'"});;
	 		
	 		{
	 			var borderHighlightElem = Ext.get('selector_IBorders');
	 			var borderHighlightDs = (borderHighlightElem && borderHighlightElem.dom.value ? borderHighlightElem.dom.value : '');
	 			
	 			if(valueExistsNotEmpty(borderHighlightDs)){
	 				this.borderHighlightParameter = {};
	 				
	 				this.borderHighlightParameter.hs_ds = borderHighlightDs;
	 				this.borderHighlightParameter.view_file = this.highlightParameter.view_file;
	 				this.borderHighlightParameter.borderHighlight = true;
	 				if(this.config.borderSize){
	 					this.borderHighlightParameter.borderSize = this.config.borderSize;
	 				}
	 				if(this.highlightParameter.hs_rest){
	 					this.borderHighlightParameter.hs_rest = this.highlightParameter.hs_rest;
	 				}
	 				if(this.highlightParameter.hs_param){
	 					this.borderHighlightParameter.hs_param = this.highlightParameter.hs_param;
	 				}
	 			}
	 		}
			
			var labelsElem = Ext.get('selector_labels');
	 		var labelsDs = (labelsElem && labelsElem.dom.value ? labelsElem.dom.value : '')
	        if(valueExistsNotEmpty(labelsDs))
	        	this.highlightParameter.label_ds = labelsDs;
		}
		if(Object.keys(this.borderHighlightParameter).length > 0){
			return [this.highlightParameter, this.borderHighlightParameter];
		}
		
        return [this.highlightParameter];
    
    },  
    
    /**
     * Gets default Border Highlight dataSource if it's specified by apps.
     */
    getDefaultBorderHighlightDSName: function(){
    	if(this.config.bordersHighlightSelector && this.config.highlightParameters){
    		for(var i=0; i<this.config.highlightParameters.length; i++){
    			if(this.config.highlightParameters[i].borderHighlight){
    				return this.config.highlightParameters[i].hs_ds;
    			}
    		}
    	}
    	return null;
    },

	/**
 	 * Display combo-box for datasource select list.
 	 */
 	appendDatasourceSelectors: function(parentNode) {
 		if (this.config.highlightSelector) {
 			this.appendSelector(parentNode, 'hilite', 'DrawingControlHighlight', View.getLocalizedString(this.z_MESSAGE_HIGHLIGHTS)+":", this.config.highlightParameters[0].hs_ds);
            if (this.config.legendPanel) {
                this.appendLegendButton(parentNode, this.config.legendPanel, false);
            }
        }
 		
 		if(this.config.bordersHighlightSelector){
 			var defaultDSName = this.getDefaultBorderHighlightDSName();
 			if(defaultDSName === null){
 				defaultDSName = 'iBorderNoneDs';
 			}
 			this.appendSelector(parentNode, 'IBorders', 'DrawingControlHighlight', View.getLocalizedString(this.z_MESSAGE_BORDERS)+":", defaultDSName);
 			 if (this.config.borderLegendPanel) {
                 this.appendLegendButton(parentNode, this.config.borderLegendPanel, true);
             }
 		}
 		
        if (this.config.labelSelector) {
 			this.appendSelector(parentNode, 'labels', 'DrawingControlLabels', View.getLocalizedString(this.z_MESSAGE_LABELS) + ":", this.config.highlightParameters[0].label_ds);
        }
    },
   
    /**
     * Appends Legend Button.
     * @param: parentNode - anchor object for the button.
     * @param: legendPanelId - legend panel id.
     * @param: isBorder - if the legend button for the border highlight.
     */
    appendLegendButton: function(parentNode, legendPanelId, isBorder){
    	var id = 'legendButton_'+legendPanelId;
    	var html= String.format(this.legendButtonHtml, id, View.getLocalizedString(this.z_MESSAGE_TOOLTIP_LEGEND));
 		Ext.DomHelper.append(parentNode, html);
 		var button = Ext.get(id);
 		var self = this;
 		button.on('click', function(){
 			  var legendPanel = View.panels.get(legendPanelId);
              if (legendPanel) {
                  legendPanel.showInWindow({
                      anchor: button.dom,
                      width: 300,
                      title: isBorder ? View.getLocalizedString(self.z_MESSAGE_BORDER_HIGHLIGHTS) : View.getLocalizedString(self.z_MESSAGE_ROOM_HIGHLIGHTS)
                  });
                  self.refreshLegendPanel(legendPanel, isBorder);
              }
 		});
    },
    
    /**
     * Refreshes legend panel with retrieved legend data from server-side.
     * 
     * @param legendPanel legend panel object.
     * @param isBorder true/false.
     */
    refreshLegendPanel: function(legendPanel, isBorder){
    	legendPanel.clear();
    	legendPanel.clearGridRows();
    	Ab.grid.Grid.prototype.displayHatchField = function(bitmapName){
    		var index = bitmapName.indexOf('-');
    		bitmapName = bitmapName.substring(index + 1);
    		
    		return  "<img src="+Ab.view.View.contextPath+"/schema/ab-core/graphics/hpatterns/"  + bitmapName.toUpperCase() + ".png hspace='0' border='0' style='width:100%;height:16px;'/>";
    	};
    	var drawingNames = [];
    	for(var drawingName in this.drawingController.control.drawings){
    		drawingNames.push(drawingName);
    	}
    	
    	 {
    		 var highlightParameter = {};
    		 if(isBorder){
    			 highlightParameter = this.borderHighlightParameter;
    			 //XXX: pass runtime restriction
    			 //var restriction = new Ab.view.Restriction();
    		     // restriction.addClause('rm.rm_id', '340');
    			 //highlightParameter.hs_rest = toJSON(restriction);
    		 }else{
    			 highlightParameter = this.highlightParameter;
    		 }
    		 var legends;
    		//gets legend data from server
    		DrawingSvgService.getLegends(drawingNames, highlightParameter, {
    			async: false,
		   	   	callback: function(legendData) {
		   	   		//legendData is Object {legendKeyValue: {name:'', color: '', value: ''}} like {'ELECTRONIC SYS.-Engineering': {name: 'BRASS', color: 'rgb(250,250, 250)', value: '14 1 171 16744319 0.0000 120.0000 BRASS'}, 'FACILITIES-Design': {name: 'Solid', color: 'rgb(100, 100, 100)', value: '14 0 54 8421376'}...}
		   	   		legends = legendData;
		   	   	},
		   	    errorHandler: function(m, e){
		   	        Ab.view.View.showException(e);
		   	    }
    		  });
    		 var record, legendObj;
			 for(var key in legends){
				 legendObj = legends[key];
				 record = new Ab.data.Record({
    					'legend.color': legendObj.value,
    					'legend.value':  key
        			}); 
        			legendPanel.addGridRow(record);	
			 }
    	 }
    	 legendPanel.update();
    },
     
    /**
     * Called by appendDatasourceSelectors().
     */
  	appendSelector: function(parentNode, comboId, dsFilter, comboLabel, defaultVal) {
 		var dsNameIdsMap = {};
 		
 		if(comboId.lastIndexOf('IBorders') >= 0){
 			dsNameIdsMap = this.getSelectorDatasources(dsFilter, 'IBorder');
		}else{
			dsNameIdsMap = this.getSelectorDatasources(dsFilter);
		}

 		// If there are 0 or 1 records, there is no need to display the combo
 		if (Object.keys(dsNameIdsMap).length < 2){
 			return;
 		}

 		this.createComboList(parentNode, comboId, comboLabel, dsNameIdsMap, defaultVal);
  	},
  	
  	createComboList: function(parentNode, comboId, comboLabel, dsNameIdsMap, defaultVal){
  	
  		var selectorId = 'selector_' + comboId;
 		var combo = Ext.get(selectorId);
 		if(!combo){
	 		//add label
 			var cell = Ext.DomHelper.append(parentNode, {tag: 'td'});
	 		var labelElem = Ext.DomHelper.append(cell, '<p>' + comboLabel + '</p>', true);
	 		Ext.DomHelper.applyStyles(labelElem, "x-btn-text");
	 		
	 		//add combo
	 		cell = Ext.DomHelper.append(parentNode, {tag: 'td'});
	 		combo = Ext.DomHelper.append(cell, {tag: 'select', id: selectorId}, true);
	 		
	 		// add combo list items
	 		var i = 0;
	 		for (var name in dsNameIdsMap) {
	 			combo.dom.options[i] = new Option(name, dsNameIdsMap[name]);
	 			i++;
	 		}
 		}
 		combo.dom.value = defaultVal;
 		
		// update combo event with the current parameters, such as pkValues, hs_ds, and labels_ds
 		combo.dom.onchange = this.onSelectedDatasourceChanged.createDelegate(this, [this, combo]);
  	},
  	
 	/**
 	 * retrieve selector datasource as a name/id map from the view file for the specified type.
 	 * 
 	 */
 	getSelectorDatasources: function(type, nameFilter){
 		var results = {};

 		var dataSources = View.dataSources;
 		for (var i = 0; i < dataSources.length; i++) {
			var ds = dataSources.items[i];
			if (ds.type != type){
				continue;
			}
			var name = (ds.title == undefined) ? ds.id : ds.title;
			if(valueExistsNotEmpty(nameFilter)){
				if(ds.id.indexOf('iBorder') >= 0){
					results[name] = ds.id;
				}
			}else{
				if(ds.id.indexOf('iBorder') >= 0){
					continue;
				}
				results[name] = ds.id;
			}
 		}
 		return results;
 	},
 	
 	onSelectedDatasourceChanged: function(selectorControl, combo) {

        View.openProgressBar('Chargement...');
        setTimeout(function(){
 		try{
 			var selectedDatasourceId = '';
 			if(combo.dom){
 				selectedDatasourceId = combo.dom.value;
 			}else{
 				selectedDatasourceId = combo.value;
 			}
 			var panelId = selectorControl.config.panelId;
 			if(typeof panelId === 'undefined' || panelId === null){
 				panelId = selectorControl.config.addOnsConfig.DatasourceSelector.panelId;
 			}
 			
 			var isLabelingAndShowTooltip = combo.id.indexOf('label') >=0 && DrawingCommon.toBoolean(selectorControl.config['showTooltip']);
 			if(isLabelingAndShowTooltip){
 				if(selectorControl.config.addOnsConfig.DatasourceSelector.addTooltips){
 					//customized tooltip addition.
 	 				selectorControl.config.addOnsConfig.DatasourceSelector.addTooltips(selectedDatasourceId, selectorControl);
 	 			}else{
 	 				selectorControl.addTooltips(selectedDatasourceId, selectorControl);
 	 			}
 			}
 			
 			var control = selectorControl.drawingController.getControl();

            if(selectorControl.config.addOnsConfig.DatasourceSelector.beforeSelectedDataSourceChanged){
                selectorControl.config.addOnsConfig.DatasourceSelector.beforeSelectedDataSourceChanged(combo.id, selectedDatasourceId, selectorControl.drawingController);
             }

 			//reload the drawings
 			for(var drawingName in control.drawings){
 				var parameters = selectorControl.config;
 				//disable plan_type case
 				parameters.setConfigParameter("plan_type", null);
 				parameters['pkeyValues'] = control.drawings[drawingName].config['pkeyValues'];
 		    	parameters['drawingName'] = drawingName;
 		    	parameters['callFromDatasourceSelector'] = 'true';
 				control.reload(parameters);
 			}
 			
 			 if(selectorControl.config.addOnsConfig.DatasourceSelector.afterSelectedDataSourceChanged){
 				selectorControl.config.addOnsConfig.DatasourceSelector.afterSelectedDataSourceChanged(combo.id, selectedDatasourceId, selectorControl.drawingController);
                View.closeProgressBar();
             }
 		}catch(e){
 			//console.log(e);
 		}
    },0);
 	},
 	/**
 	 * Adds AssetTooltip.
 	 * 
 	 *  @param:labelDSId - label datasource id.
 	 *  @param: selectorControl - datasource selector control object.
 	 */
 	addTooltips: function(labelDSId, selectorControl){
		var labelDs = View.dataSources.get(labelDSId);
		if(labelDs){
			//TODO: assetType is always the main table name???
			var assetType = labelDs.mainTableName;
			var addOneTooltip = selectorControl.drawingController.addOnsController.getAddOn('AssetTooltip');
			if(addOneTooltip){
				//clear up existing AssetTooltip object from memory
				selectorControl.drawingController.addOnsController.removeAddOn('AssetTooltip');
			}
			selectorControl.drawingController.addOnsController.registerAddOn('AssetTooltip', {handlers: [{assetType:assetType}]});
		}
 	}

}, {});