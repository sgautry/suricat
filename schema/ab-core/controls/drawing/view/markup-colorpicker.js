Drawing.namespace("view");

/***
 * Color Picker Control for Markup Control. This control is constructed by Drawing.view.MarkupControl add-on.
 * 
 * Defines a panel title with Color Picker selector of 3 input element: red, black and custom.
 * When user clicks red control, all the subsequent markup legends will use the red color.
 * When user clicks black control, all the subsequent markup legends will use the black color.
 * When user clicks custom control, a color picker dialog will show for user to pick a color. custom control will update with the user picked color 
 * and all the subsequent markup legends will use the custom color.
 * 
 * @author   Ying Qin
 * @version  23.2
 * @date     1/2017
 */

Drawing.view.MarkupColorPicker = Base.extend({

	//text before color elements or panel title
    title: View.getLocalizedString(this.Z_REDLINES_TITLE),
    
    //user define config for color picker's appearance
    config: {},
    
    //Drawing.view.MarkupControl - used to set the legend colors
    control: null,
    
    // @begin_translatable
    Z_REDLINES_TITLE: 		 'Redlines',
	Z_SCALE_TEXT:            'scale',
	Z_INCREASE_TEXT_SIZE: 	 'Increase legend text size',
	Z_DECREASE_TEXT_SIZE:    'Decrease legend text size',
    // @end_translatable
	
    /**
     * Constructor.
	 * @param divId String Id of <div/> that holds the svg
     * @param panelId String Id of the panel
	 * @param config configObject
     */
    constructor: function(parentDivId, title, control) {

    	this.parentDiv = document.getElementById(parentDivId);
    	
    	if(title)
    		this.title = title;
    	else
    		this.title = View.getLocalizedString(this.Z_REDLINES_TITLE);
    	
        this.config = {divClass: "x-toolbar x-small-editor panelToolbar",
        				   spacerWidth: "20%",
        				   customStyle: "height:22px; width:28px;  min-width: 28px;  max-width: 28px;",
        				   customInputStyle: "height:20px; width:25px",
        				   customColorId: "redlineColor"};
        
        this.control = control;
        
       	this.setup();
        
    },
    
    /**
     * create color picker with three buttons in specified div.
     */
    setup: function(){
    	
    	// create div to contain the table.
    	var div=document.createElement("div");
    	div.className = this.config.divClass;
    	
    	var table = this.createTable();    	
    	div.appendChild(table);

    	if(this.parentDiv){
    		this.parentDiv.innerHTML = '';
    		this.parentDiv.appendChild(div);
    		
        	var jsColor = new jscolor.color(document.getElementById(this.config.customColorId), {valueElement: 'redlineColorValue'});
    	}
    },

    /**
     * create color picker table
     */
    createTable: function(){
    	var table = document.createElement('table');
    	table.style.borderSpacing = "0px";
    	
	    var tbody = document.createElement('tbody');

	    var tr = document.createElement('tr');   
    	
	    tr.appendChild(this.createText("ytb-text", this.title));
    	tr.appendChild(this.createColumn(this.config.spacerWidth));
    	tr.appendChild(this.createColumn());
    	
    	tr.appendChild(this.createText("redlineScaleText", parseInt(this.control.config.currentFontSize*100/10) + "% ", "redlineScale"));
    	tr.appendChild(this.createColumn());
    	tr.appendChild(this.createText("redlineScaleText", View.getLocalizedString(this.Z_SCALE_TEXT)));
    	tr.appendChild(this.createColumn());
    	tr.appendChild(this.createColumn("4%"));
    	tr.appendChild(this.createColumn());
    	
    	
    	var td = this.createFontElement("large");
    	tr.appendChild(td);

    	var td = this.createFontElement("small");
    	tr.appendChild(td);


    	
    	var td = this.createColorElement('redlineColorRed', '#FF0000', this.config.customColorId);
    	tr.appendChild(td);
        
    	tr.appendChild(this.createColumn());
    	
    	td = this.createColorElement('redlineColorBlack', '#000000', this.config.customColorId);
    	tr.appendChild(td);

    	tr.appendChild(this.createColumn());

    	td = this.createCustomElement(this.config.customStyle, this.config.customColorId, this.config.customInputStyle);
    	tr.appendChild(td);
    	
    	tbody.appendChild(tr);
    	
    	table.appendChild(tbody);
    	
    	return table;
    },
    

    /**
     * add the title
     */
    createText: function(className, text, id){
    	
    	var span = document.createElement("span");
    	if(id){
	    	span.id = id;
	    	span.name = name;
    	}
    	span.className = className;
    	span.appendChild( document.createTextNode(text) );
    	
    	var td = document.createElement('td');
    	td.appendChild(span);
    	
    	return td;
    },
    
    /**
     * add column
     */
    createColumn: function(width){
    	var td = document.createElement('td');
    	if(width)
    		td.style.width = width;  
    	td.innerHTML = this.createSpacer();
    	return td;
    },
    
    /**
     * spacer needed between buttons
     */
    createSpacer: function(){
    	
    	var spacerDiv = document.createElement('div');
    	spacerDiv.className = "ytb-spacer";
    	
    	return spacerDiv.outerHTML;
    },
    
    /**
     * red/black picker
     */
   	createColorElement: function(className, color, customColorId){
    	var td = document.createElement('td');
    	td.className =  className; 
    	
    	var control = this.control;
    	td.addEventListener("click", changeRedlineColor.createDelegate(td, [control, color, customColorId]));
    	td.innerHTML = this.createSpacer();
    	return td;
   	},

   	createFontElement: function(type){
    	var td = document.createElement('td');
    	
    	
    	var fontInput = document.createElement('input');
    	fontInput.id = "redline_" + type + "Font";
    	fontInput.src = "/archibus/schema/ab-core/graphics/icons/drawing/label-a-" + type + ".png";
    	
    	if(type == 'large'){
    		fontInput.title = View.getLocalizedString(this.Z_INCREASE_TEXT_SIZE);
    		fontInput.alt = View.getLocalizedString(this.Z_INCREASE_TEXT_SIZE);
    	} else if(type == 'small'){
    		fontInput.title = View.getLocalizedString(this.Z_DECREASE_TEXT_SIZE);
    		fontInput.alt = View.getLocalizedString(this.Z_INCREASE_TEXT_SIZE);
    	}
    	fontInput.className = "image";
    	fontInput.type = "image";
    	td.appendChild(fontInput);

    	var control = this.control;
    	if(type == 'large'){
    		td.width =  '6%';
    		td.addEventListener("click", changeRedlineFont.createDelegate(td, [control, true]));
    	} else {
    		td.width =  '8%';
        	td.addEventListener("click", changeRedlineFont.createDelegate(td, [control, false]));
    	}
    	td.style="padding-top: 5px";
    	
    	return td;
   	},
   	
   	/**
   	 * custom picker with color selector
   	 */
   	createCustomElement: function(customStyle, customColorId, customInputStyle, fn){
   		var td = document.createElement('td');
    	td.style =  customStyle;
   	
    	var colorInput = document.createElement('input');
    	colorInput.id = customColorId;
    	colorInput.className = "color {valueElement: 'redlineColorValue'}";
    	//colorInput.setAttribute("autoComplete", "off");
    	colorInput.style = customInputStyle;
    	
    	td.appendChild(colorInput);

   		var colorValueInput = document.createElement('input');
    	colorValueInput.id = "redlineColorValue";
    	colorValueInput.type = "hidden";
    	colorValueInput.setAttribute("value", "#FF0000");
    	
    	var control = this.control;
    	colorValueInput.addEventListener("change", changeRedlineColor.createDelegate(td, [control, colorValueInput, customColorId])); 
    	td.appendChild(colorValueInput);
    	
    	return td;
    	
   	},

   	updateCustomColor: function(colorValue){
   		if(!colorValue)
   			return;
   		
   		var colorElement = document.getElementById(this.config.customColorId);
   		if(colorElement){
   			colorElement.style.color = "#FFFFFF";
   			colorElement.style.backgroundColor = colorValue;
   		}	
   	}
   	
});

/**
 * event for change redline color.
 * 
 * @param control the redline panel
 * @param color new color
 * @param customColorId  custom color picker id
 */
function changeRedlineColor(control, color, customColorId){

	if(!color)
		return;
	
	var colorValue;
	if(typeof color == 'string'){
		colorValue = color;
	} else {
		colorValue = color.value;
	}

	control.setCurrentColor(colorValue);
	
	var colorElement = document.getElementById(customColorId);
	colorElement.style.color = "#FFFFFF";
	colorElement.style.backgroundColor = colorValue;
}

function changeRedlineFont(control, isIncrease, scale){
	control.setCurrentFontSize(isIncrease);
	
	var scaleSpan = document.getElementById("redlineScale");
	scaleSpan.textContent = parseInt(control.config.currentFontSize*100/10) + "% ";
}

