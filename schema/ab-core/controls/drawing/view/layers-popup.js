/**
 * Layers Popup UI for SVG Drawing control.
 */
Drawing.namespace("view");

Drawing.view.LayersPopup = Base.extend({
	
	/**
	 * a map of SVGs
	 */
	svgs: {}, 
	
	/**
	 * root div for all drawings
	 */
	rootDiv: null,
	
	/**
	 * container
	 */
	container: null,
	
	/**
	 * container id for layers
	 */
	layersContainerId: '',
	
	/**
	 * container for layers
	 */
	layersContainer: null,
	
	
	/**
	 * additional UI options
	 */
	opts: {}, 
	
	/**
	 * the number of columns for layers table
	 */
	numberOfColumns: -1, 
	
	/**
	 * the number of rows for layers table
	 */
	numberOfRows: -1,

	/**
	 * list of all layers to display on the pop-up
	 */
	layers: [],
	
	/**
	 * list of asset layers to display on the popup
	 */
	assetLayers: [],
	
	/**
	 * list of background layers to display on the popup
	 */
	backgroundLayers: [],
	
	/**
	 * default layers to turn on when create/load
	 */
	defaultLayers: [],
	
	/**
	 * common published layers from SVG drawing
	 */
	publishedLayers: [],
	
	/**
	 * retrieve localized titles from afm_layr.title table
	 */
	titleMap: {},
	
	// index for dividers to divide asset layers from background layers.
	dividerIndex: -1,
	
	/**
	 * custom event when user check/uncheck layer's checkbox
	 */
	customEvent: null,
	
	/**
	 * If layer panel is collapsed or opened.
	 */
	collapsed: false,
	
	// @begin_translatable
    Z_LAYER_TITLE: "Labels",
    Z_LAYER_BACKGROUND_TITLE: "Background",
    Z_LAYER_PANEL_TOOLTIP: "Assets & Backgrounds",
    // @end_translatable
	
	constructor: function(config) {
		this.config = config;
		
		this.svgs = {};
		this.rootDiv = d3.select("#" + this.config.divId);
		this.layersContainerId = this.config.divId + "_layerList";

		this.publishedLayers = [];
		this.layers= [];
		this.assetLayers = [];
		this.backgroundLayers = [];
		this.defaultLayers = [];
		this.dividerIndex = -1;
		this.opts = {};
		
		if(config.customEvent){
			this.customEvent = config.customEvent;
		}
	
		if(config.collapsed){
			this.collapsed = config.collapsed;
		}

		//retrieve layers title map from database
		this.populateLayersMap();
	},
	
    /**
     * set reference to drawingController
     */
    setDrawingController: function(drawingController){
    	this.drawingController = drawingController;
    },
	
	getContainerDivId: function(){
 		 return DrawingCommon.retrieveValidNodeId(this.config.divId);
	},
	
	showLayers: function(svg, add){
		d3.select("#" + this.layersContainerId).remove();
		
		if(svg.node() === null)
			return;
		
		var svgId = svg.node().id;
		if(add){
			this.svgs[svgId] = svg;
			this.populateLayers(svg);
		} else {
			
			if(Object.keys(this.svgs).length > 0 && this.svgs[svgId]){
				this.svgs[svgId] = null;
				delete this.svgs[svgId];

				this.resetLayers();
				
				for(var id in this.svgs){
					this.populateLayers(this.svgs[id]);
				}
			}
		}

		if(Object.keys(this.svgs).length > 0){
			this.createLayerPopup(this.rootDiv, this.layersContainerId, this.opts);
			
	        this.afterResize();
		}
	},
	
	/**
	 * reset all layers variables
	 */
	resetLayers: function(){
		this.publishedLayers = [];
		this.layers = [];
		this.assetLayers = [];
		this.backgroundLayers = [];
		this.defaultLayers = [];
		this.dividerIndex = -1;
	},
	
	populateLayers: function(svg){

		this.publishedLayers = this.getAllLayers(svg);

		//retrieve layers map from database
		//this.populateLayersMap();
		
		if(this.config.layers){
			var layersFromConfig = this.config.layers.split(';');
			for(var i = 0; i < layersFromConfig.length; i++){
				var layerName = layersFromConfig[i];
				layerName = layerName.toLowerCase().replace("$txt", "-labels").replace("$", "-assets"); 
	        	
				if(this.publishedLayers.indexOf(layerName) > -1 && this.assetLayers.indexOf(layerName) === -1){
					this.assetLayers.push(layerName);
				} else if(layerName && layerName.toLowerCase() == 'background' && this.publishedLayers.indexOf(layerName) > -1 && this.backgroundLayers.indexOf(layerName) === -1){
					this.backgroundLayers.push(layerName);
				}
			}
			this.dividerIndex = this.assetLayers.length;
			this.layers = this.assetLayers.sort().concat(this.backgroundLayers);
		} else {
			this.layers = this.publishedLayers;
		}
		
		// populate the defaultLayers only when it is not called from datasource selector
		if(this.config.callFromDatasourceSelector != 'true' && this.config.defaultLayers){
			var defaultLayersFromConfig = this.config.defaultLayers.split(';');
			for(var j = 0; j < defaultLayersFromConfig.length; j++){
				var layerName = defaultLayersFromConfig[j];
				// layerName = layerName.toLowerCase().replace("$txt", "-labels").replace("$", "-assets"); 
				layerName = layerName.replace(/\$txt/gi, "-labels").replace("$", "-assets"); 
				if(this.publishedLayers.indexOf(layerName) > -1){
					this.defaultLayers.push(layerName);
				}else if(this.publishedLayers.indexOf(layerName.toLowerCase()) > -1){
					this.defaultLayers.push(layerName.toLowerCase());
				}else if(this.publishedLayers.indexOf("layer-"+layerName) > -1){
					this.defaultLayers.push("layer-"+layerName);
				}else if(layerName && layerName.toLowerCase() == 'background'){
					this.defaultLayers = this.defaultLayers.concat(this.getBackgroundLayers(svg));
				}
			}
		} else {
			// SVG from server side might already have layers on/off from datasource/label selectors.
			for(var k = 0; k < this.layers.length; k++){
				var layerName = this.layers[k];
				// WC-2604 change line
				var node = d3.select("#" + DrawingCommon.d3SelectEscape(layerName)).node();
				if(node && node.style && node.style.display !== 'none' && this.defaultLayers.indexOf(layerName) == -1){
					if(typeof this.config.defaultLayers !== 'undefined' && this.config.defaultLayers){
						if(this.config.defaultLayers.indexOf(layerName) > -1){
							// turn on the layer if the layer is in user-defined default layers list
							this.defaultLayers.push(layerName);
						} else if(node.parentNode.id === 'assets' || node.parentNode.id === 'asset-labels') {
							// turn on the layers if the layers are asset or labels. (this is to prevent all background layers from turning on as the server API turn all background layers on)
							this.defaultLayers.push(layerName);
						}
					} else {
						// always turn on the layers retrieved from server if no default layers defined
						this.defaultLayers.push(layerName);
					}
                } 
			}
		}
		var taskId="";
		if(View.taskRecord!=null){
			taskId= View.taskRecord.values["afm_ptasks.task_id.key"];
		}
		var parameterDefaultLayers=View.activityParameters['AbSpaceRoomInventoryBAR-SpaceConsoleDefaultLayers'];
		if((!this.defaultLayers || this.defaultLayers.length < 1) || (taskId == 'SURICAT Space Console' && parameterDefaultLayers=='-')){
			this.defaultLayers = this.layers;	
		}
		
	},
	
	populateLayersMap: function(){
	    	var datasource = Ab.data.createDataSourceForFields({
		        id: 'svgDrawingControl_afmLayr',
		        tableNames: ['afm_layr'],
		        fieldNames: ['afm_layr.layer_name','afm_layr.title']
		    });	
	    	
			var records = datasource.getRecords();
			if(records && records.length > 0){
				var layerName = '';
				var title = '';
				var formattedTitle = '';
				
	    		for(var i = 0; i < records.length; i++){
	    			if(!records[i])
	    				continue;
		    		
		        	layerName = records[i].getValue("afm_layr.layer_name");
		        	
		        	if(layerName.indexOf('$') < 0)
		        		continue;
		        	
		        	layerName = layerName.toLowerCase().replace("$txt", "-labels").replace("$", "-assets"); 
		        	title = records[i].getValue("afm_layr.title");
		        	if(title)
		        		formattedTitle = datasource.formatValue('afm_layr.title', title, true);
		        	else
		        		formattedTitle = title;
		        	
		        	this.titleMap[layerName] = formattedTitle;
	    		}
	    	} 
	},
	
    createLayerPopup: function(div, layersContainerId, opts) {
        this.setParameters(div, layersContainerId, -1, -1, opts);
        if(this.dividerIndex === -1){
        	this.create(div, layersContainerId, this.layers.length, 2, opts);
        } else {
        	this.create(div, layersContainerId, this.layers.length + 1, 2, opts);
        }
        this.insertTableContent(document.getElementById(this.layersContainerId + "_table"));

        return this;
    },

    create: function(div, layersContainerId, rows, cols, opts) {
        this.setParameters(div, layersContainerId, rows, cols, opts);
        this.createPopup();
    },

    setParameters: function(div, layersContainerId, rows, cols, opts) {
        this.rootDiv = div;
        this.layersContainerId = layersContainerId;
        this.numberOfRows =  rows;
        this.numberOfColumns = cols;
        this.opts = opts;
    },


    show: function(bShow) {
        document.getElementById(this.layersContainerId).style.display = (bShow) ? '' : 'none';
    },

    setText: function(text) {
        var node = document.getElementById(this.container.id + "_infobartext");
        node.innerHTML = text;
    },

    createPopup: function() {
        this.container = document.createElement('div');
        this.container.id = this.layersContainerId;
        this.container.className = "layer-list";

        var button = this.createMinimizeButton();
        this.container.appendChild(button);
        
        // prevent double click from propagating
        this.container.addEventListener("dblclick", function(event){
        	event.stopPropagation();
        });
        this.container.addEventListener("mousemove", function(event){
        	event.stopPropagation();
        });
        
        var table = this.createTable(this.numberOfColumns, this.numberOfRows);
        this.container.appendChild(table);
 
        var firstRowDiv = d3.select("#" + this.config.divId + "-0");
        if(firstRowDiv){
        	this.rootDiv.node().insertBefore(this.container, firstRowDiv.node());
        }
        if(this.collapsed){
        	 this.collapseLayersPanel(button);
        }
    },

    afterResize: function(){
    	if(typeof this.container === 'undefined' || !this.container){
    		this.container = document.getElementById(this.layersContainerId);
    	}
    	
       	if(typeof this.rootDiv.node().style.height !== 'undefined' && this.container){
        	this.container.style.maxHeight = (this.rootDiv.node().style.height.replace("px", "") - 10) + "px";
        }
    },
    
    createMinimizeButton: function() {
        var minimizeButton = document.createElement('div');
        minimizeButton.id='layerPanelButton';
        minimizeButton.className = 'minimize-button';
        minimizeButton.title = View.getLocalizedString(this.Z_LAYER_PANEL_TOOLTIP);
        var me = this;
        minimizeButton.addEventListener('click', function(){
            if (this.parentNode.style.height !== '13px') {
            	me.collapseLayersPanel(this);
            	me.collapsed = true;
            } else {
                this.parentNode.style.height = 'auto';
                this.parentNode.style.width = 'auto';
                this.parentNode.style.overflow = 'auto';
                minimizeButton.className = 'minimize-button';
                me.collapsed = false;
            }
        }, false);

        return minimizeButton;
    },
    /**
     * Collapses layer panel.
     * @param {minimizeButton} optional - layer panel minimize button object.
     * 
     */
    collapseLayersPanel: function(minimizeButton){
    	if(minimizeButton === undefined || minimizeButton === null){
    		minimizeButton = document.getElementById("layerPanelButton"); 
    	}
    	minimizeButton.parentNode.style.height = 13 + 'px';
    	minimizeButton.parentNode.style.width = 16 + 'px';
    	minimizeButton.parentNode.style.overflow = 'hidden';
		minimizeButton.className = 'open-button';
    },
    
    createTable: function(cols, rows) {
        var table = document.createElement("table");
        var caption = table.createCaption();
        caption.innerHTML = '<b>'+View.getLocalizedString(this.Z_LAYER_PANEL_TOOLTIP) + '</b>';
        table.id = this.layersContainerId + "_table";
        this.tableId = table.id;
        var tbody = document.createElement("tbody");
        table.appendChild(tbody);

        this.createRows(tbody, cols, rows);

        return table;
    },

    createRows: function(tbody, cols, rows) {
    	for (var i=0; i<rows; i++) {
        	if(i === this.dividerIndex){
        		var tr = document.createElement("tr");
            	var td = document.createElement("td");
                td.colSpan = cols;
                td.className = "divider";
                tr.appendChild(td);
                tbody.appendChild(tr);
            }
    		var tr = document.createElement("tr");
         	for (var j=0; j<cols; j++) {
                var td = document.createElement("td");
	            td.className = "column" + j;
	            tr.appendChild(td);
	        }
            tbody.appendChild(tr);
        }
        return tbody;
    },

    insertTableContent: function(table) {
        var rows = table.tBodies[0].rows;
        var control = this;

        // we will create the rows for layers without no localized titles later.
        var layersPostponed = [];

        //track actual row #
        var rowIndex = 0;
        for (var i=0; i<this.layers.length; i++) {
            if(this.layers[i] === 'divider'){
            	rowIndex++;
            	continue;
            }
            var localizedTitle = this.getLocalizedTitle(this.layers[i]);
            if(!localizedTitle || localizedTitle.length < 1){
            	layersPostponed[layersPostponed.length] = this.layers[i];
            } else {
                var row = rows[rowIndex];
                var tds = row.childNodes;

                var checkbox = this.createCheckbox(this.layers[i], this.checkboxEvent);
                // if(!this.defaultLayers || this.defaultLayers.length < 1 || (this.defaultLayers && this.defaultLayers.indexOf(this.layers[i]) > -1)){
                // 	checkbox.checked = true;
                // } else {
                // 	checkbox.checked = false;
                // }
                if(this.defaultLayers.length>0 && this.defaultLayers.indexOf(this.layers[i]) > -1){
                	checkbox.checked = true;
                } else {
                	checkbox.checked = false;
                }
                //call the checkbox event handler but do not call the custom event
                this.checkboxEvent(checkbox, this, false);
                tds[0].appendChild(checkbox);
                tds[1].innerHTML = localizedTitle;
                
                rowIndex++;
            }
        }
        
        for(var j = 0; j < layersPostponed.length; j++){
            var row = rows[rowIndex];
            if(typeof row == 'undefined' || row == null || typeof row.childNodes == 'undefined' || row.childNodes == null){
            	continue;
            } else {
	            var tds = row.childNodes;
	            // only add event if it is not a divider
	            if(tds.length > 1){
		            var checkbox = this.createCheckbox(layersPostponed[j], this.checkboxEvent);
		            // if(!this.defaultLayers || this.defaultLayers.length < 1 || (this.defaultLayers && this.defaultLayers.indexOf(layersPostponed[j]) > -1)){
		            // 	checkbox.checked = true;
		            // } else {
		            // 	checkbox.checked = false;
		            // }
		            if(this.defaultLayers.length>0 && this.defaultLayers.indexOf(layersPostponed[j]) > -1){
		            	checkbox.checked = true;
		            } else {
		            	checkbox.checked = false;
		            }
		            //call the checkbox event handler but do not call the custom event
		            this.checkboxEvent(checkbox, this, false);
		            tds[0].appendChild(checkbox);
		            tds[1].innerHTML = layersPostponed[j].replace("layer-", "");
	            } else {
	            	//retain the layersPostponed index after divider
	            	j--;
	            }
            }
            rowIndex++;
        }
    },

    /**
     * find the layer title from the layers title map
     */
    getLocalizedTitle: function(layer){
    	
    	var localizedTitle = "";
    	
   		var index = layer.indexOf("-assets");
   		if(index > -1){
   			//if it is an asset layer
   			localizedTitle = this.titleMap[layer];
   		} else {
   			//if it is a label/text layer
   			index = layer.indexOf("-labels");
   			if(index > -1){
   	   			localizedTitle = this.titleMap[layer];
   	   			if(typeof localizedTitle !== 'undefined' && localizedTitle === this.titleMap[layer.replace("-labels", "-assets")]){
   	   				localizedTitle = this.titleMap[layer] + " " + View.getLocalizedString(this.Z_LAYER_TITLE);
   	   			}
   			} 
   		}
   		
   		if(typeof localizedTitle !== 'undefined' && localizedTitle != null)
   			return localizedTitle;
   		else 
   			return "";
    },
    
    createCheckbox: function(id, eventListener) {
        var checkbox = document.createElement("input");
        checkbox.type = "checkbox";
        checkbox.checked = true;
        
        
        checkbox.id = "checkbox_" + id;
        var control = this;
        //call the checkbox event handler with the custom event onchange
        if (eventListener.createDelegate) {
        	checkbox.addEventListener('change', eventListener.createDelegate(checkbox, [checkbox, control, true]), false);
        } else {
        	checkbox.addEventListener('change', Ext.bind(eventListener, checkbox, [checkbox, control, true]), false);
        }

        return checkbox;
    },

    checkboxEvent: function(checkbox, control, callCustomEvent){
    	
        var display = (checkbox.checked) ? "" : "none";
        for(var svgId in control.svgs){
			// WC-2604 change lines
			var selectId = DrawingCommon.d3SelectEscape(checkbox.id.replace("checkbox_", ""));
	        control.svgs[svgId].selectAll("#" + selectId).style("display", display);
	        control.svgs[svgId].selectAll(".cluster." + selectId).style("display", display);		
	        //control.svgs[svgId].selectAll("#" + checkbox.id.replace("checkbox_", "")).style("display", display);
	        //co//rol.svgs[svgId].selectAll(".cluster." + checkbox.id.replace("checkbox_", "")).style("display", display);
        }
        
        var clusterControl = control.drawingController.getAddOn('Cluster');        
        if (clusterControl && checkbox.id === 'checkbox_' + clusterControl.layerName) {
        	clusterControl.setVisible(checkbox.checked);
        }
        
        if(callCustomEvent && control.customEvent){
        	control.customEvent(checkbox);
        }
    },
    
    getAllLayers: function(svg) {
    	this.assetLayers =  this.getAssetLayers(svg);
    	this.backgroundLayers = this.getBackgroundLayers(svg);
    	return this.assetLayers.concat(this.backgroundLayers);
    },
    
    getAssetLayers: function(svg){
        var assets = svg.selectAll("#assets > g")[0];
        
        var labelNodes = svg.selectAll("#asset-labels").node();
        
        var assetLabels = (labelNodes ? labelNodes.childNodes : []);
        var redline = svg.selectAll("#redline");

        var svgLayers = [];
        for(var j=0; j<assetLabels.length; j++) {
            if (assetLabels[j].nodeName === 'g') {
            	svgLayers.push(assetLabels[j].id);
            }
        }
        
        for(var k=0; k<assets.length; k++) {
        	svgLayers.push(assets[k].id);
        }
        
        if (!redline.empty()) {
        	svgLayers.push(redline.node().id);
        }

        //find out the layers that are common
        var commonLayers = [];
        if(this.config.allowMultipleDrawings === 'false' || this.assetLayers.length == 0){
       		this.resetLayers();
        	commonLayers = svgLayers;
        } else {
	        for(var index = 0; index < this.assetLayers.length; index++){
	        	var layerId = this.assetLayers[index];
	        	if(svgLayers.indexOf(layerId) >= 0){
	        		commonLayers.push(layerId);
				}
				for(var svgId in this.svgs){
					this.svgs[svgId].selectAll("#" + layerId).style("display", "none");
					this.svgs[svgId].selectAll(".cluster." + layerId).style("display", "none");	
				}
	        }
        }
        
        //always put the background layer at the end.
        this.dividerIndex = commonLayers.length;
        return commonLayers.sort();
    },
    
    getBackgroundLayers: function(svg){
    	var bgLayersNode = svg.selectAll("#background").node();
    	var bgLayers = (bgLayersNode ? bgLayersNode.childNodes: []);
    	
    	var svgLayers = [];
        for(var j=0; j<bgLayers.length; j++) {
            if (bgLayers[j].nodeName === 'g') {
            	svgLayers.push(bgLayers[j].id);
				for(var svgId in this.svgs){
					this.svgs[svgId].selectAll("#" + bgLayers[j].id).style("display", "none");
					this.svgs[svgId].selectAll(".cluster." + bgLayers[j].id).style("display", "none");	
				}	
            }
        }
        
        var commonLayers = [];
        if(this.backgroundLayers.length == 0){
        	//if the current drawing is the only drawing added
        	commonLayers = svgLayers;
        } else {
	        for(var index = 0; index < this.backgroundLayers.length; index++){
	        	var layerId = this.backgroundLayers[index];
	        	if(svgLayers.indexOf(layerId) >= 0){
	        		commonLayers.push(layerId);
	        	}
	        }
        }
        
        return commonLayers.sort();
    }
    
}, {});