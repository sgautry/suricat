Drawing.namespace("view");

Drawing.view.NavigationToolbar = Base.extend({
	
	toolbarDiv: null,
	
	drawingController: null,
	
	/**
	 * record mouse state for event handler
	 */
	mouseState: {},
	
	/**
	 * current svg, used for click-move-release action for multiple drawings
	 */
	currentSvg: null,
	
	/**
	 * switch for asset multiple selection - true if on, false otherwise.
	 */
	multipleSelectionOn: false,
	
	/**
	 * switch for type of panning  - true to use default D3 event panning, false to use window event panning
	 */
	useD3Panning: false,
	
    // @begin_translatable
    Z_NAVBAR_ZOOM_IN: "Zoom In",
    Z_NAVBAR_ZOOM_OUT: "Zoom Out",
    Z_NAVBAR_ZOOM_EXTENTS: "Zoom Extents",
    Z_NAVBAR_ZOOM_WINDOW: "Zoom into Selected Window Area",
    Z_NAVBAR_SELECT_MULTIASSETS: "Select Multiple Asset(s)",
    Z_NAVBAR_LABELSIZE_INCREASE: "Increase Label Size",
    Z_NAVBAR_LABELSIZE_DECREASE: "Decrease Label Size",
    // @end_translatable
    

	constructor: function(config) {
		this.config = config;
		
		this.init();
	},
	
	 /**
     * set reference to drawingController
     */
    setDrawingController: function(drawingController){
    	
    	this.drawingController = drawingController;
    
        // extents the drawing
        //var status = this.drawingController.getController("PanZoomController").zoomExtents();
        
        // update icons
        //this.updateLinkIcon(status);
        
        //multiple drawing - add default handlers after layout
        this.addDefaultHandlers();
        
        this.mouseState = {};

    },
    
    
    init: function(){
		var navToolbarContainer = document.createElement('div'); 
		navToolbarContainer.id = this.getContainerDivId() + "_navToolbarContainer";
		navToolbarContainer.className = 'navToolbarContainer';
		
		this.toolbarDiv = document.createElement('ul');
        this.toolbarDiv.id = this.getContainerDivId() + "_navToolbar";
        this.toolbarDiv.className = 'navToolbar';

        this.addLinkDiv('zoomIn', View.getLocalizedString(this.Z_NAVBAR_ZOOM_IN));
        this.addLinkDiv('zoomOut', View.getLocalizedString(this.Z_NAVBAR_ZOOM_OUT));
        this.addLinkDiv('zoomExtents', View.getLocalizedString(this.Z_NAVBAR_ZOOM_EXTENTS));
        if(this.config.zoomWindowEnabled !== 'false'){
            this.addLinkDiv('zoomWindow', View.getLocalizedString(this.Z_NAVBAR_ZOOM_WINDOW));
        }
        if(this.config.multipleSelectionEnabled !== 'false'){
        	this.addLinkDiv('multipleSelection', View.getLocalizedString(this.Z_NAVBAR_SELECT_MULTIASSETS));
        }
        if(this.config.allowAdjustLabelSize !== 'false'){
        	this.addLinkDiv('labelSizeIncrease', View.getLocalizedString(this.Z_NAVBAR_LABELSIZE_INCREASE));
        	this.addLinkDiv('labelSizeDecrease', View.getLocalizedString(this.Z_NAVBAR_LABELSIZE_DECREASE));
        }
        this.multipleSelectionOn = false;
        this.zoomWindowOn = false;
        
        navToolbarContainer.appendChild(this.toolbarDiv);
        
        // prevent double click from propagating
        navToolbarContainer.addEventListener("dblclick", function(event){
        	event.stopPropagation();
        });
        
        
        // append after SVG drawing.
        var containerDiv = d3.select("#" + this.getContainerDivId()).node();
        containerDiv.appendChild(navToolbarContainer);

        this.addLinkEvent('zoomIn');
        this.addLinkEvent('zoomOut');
        this.addLinkEvent('zoomExtents');
        
        if(this.config.zoomWindowEnabled == 'true'){
        	this.addLinkEvent('zoomWindow');
        }
        if(this.config.multipleSelectionEnabled == 'true'){
            this.addLinkEvent('multipleSelection');
        }
        if(this.config.allowAdjustLabelSize !== 'false'){
            this.addLinkEvent('labelSizeIncrease');
            this.addLinkEvent('labelSizeDecrease');
        }
        
        // show the toolbar
        this.toolbarDiv.style.display = 'none';   
        
	},

	addD3Handlers: function(svg){
		svg.call(viewBoxZoom());
	},
	
	removeD3Handlers: function(svg){
		var div = d3.select(svg.node().parentNode);

		if(div){
			// turn off d3 event
	      	div.on(".viewboxzoom", null).on(".zoom", null);	
		}
	},
	
	/**
	 * add the window's handlers to utilize the max/min zooming functionality.
	 */
	addDefaultHandlers: function(){

    	//attach window's event
    	var eventType = (navigator.userAgent.indexOf('Firefox') !=-1) ? "DOMMouseScroll" : "mousewheel"; 
    	var navToolbar = this;
    	window.addEventListener(eventType, function(e){
    			e.preventDefault();
    			
    			// cross-browser wheel delta
    			var e = window.event || e; // old IE support
    			var svg = navToolbar.retrieveEventTargetSvg(e);
    			if(svg && d3.select(svg).attr("viewBox")){
        			var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
        			if(delta > 0){
        				navToolbar.drawingController.getController("PanZoomController").zoomIn(d3.select(svg));
        			} else {
        				navToolbar.drawingController.getController("PanZoomController").zoomOut(d3.select(svg));
        			}
       				navToolbar.updateLinkIcons();
       				
       				if(navToolbar.config.afterZoom){
       					navToolbar.config.afterZoom(svg, navToolbar.drawingController);
       				}
    			}
    	    }, false);
    			
    	window.addEventListener("dblclick", function(e){
    			e.preventDefault();
    			
    			var svg = navToolbar.retrieveEventTargetSvg(e);
    			if(svg && d3.select(svg).attr("viewBox")){
        			navToolbar.drawingController.getController("PanZoomController").zoomIn(d3.select(svg));
        		    navToolbar.updateLinkIcons();
       				if(navToolbar.config.afterZoom){
       					navToolbar.config.afterZoom(svg, navToolbar.drawingController);
       				}
    			}
   	    	}, false);

    	// use window event instead panel, as later is inconsistent with mouseup event and does not work well in Chrome.
    	window.addEventListener("mousedown", function(e){
			var svg = navToolbar.retrieveEventTargetSvg(e);
   		    // skip if user clicks outside of svg
    		if(svg && d3.select(svg).attr("viewBox")){
    			e.preventDefault();
    			navToolbar.mouseDownHandler(e, d3.select(svg));
    		 }
    	}, false);

    	window.addEventListener("mousemove", function(e){
			var svg = navToolbar.retrieveEventTargetSvg(e);
			// skip if user clicks outside of svg
    		if(svg && d3.select(svg).attr("viewBox")){
    			e.preventDefault();
   				navToolbar.mouseMoveHandler(e, d3.select(svg));
    		 }
    	}, false);

    	window.addEventListener("mouseup", function(e){
			//var svg = navToolbar.retrieveEventTargetSvg(e);
    		// skip if user clicks outside of svg
    		//if(svg && d3.select(svg).attr("viewBox")){
   		 		e.preventDefault();
   				navToolbar.mouseUpHandler(e);//, d3.select(svg));
    		//}
    	}, false);
	},    
    
	retrieveEventTargetSvg: function(e){
		var svg = (e.target && e.target.nodeName === "svg" ? e.target: null);
		if(!svg && e.target && e.target.viewportElement && e.target.viewportElement.nodeName === "svg"){
			svg = e.target.viewportElement;
		}

		if(!svg && e.srcElement && e.srcElement.farthestViewportElement	&& e.srcElement.farthestViewportElement.nodeName === "svg"){
			svg = e.srcElement.farthestViewportElement;
		}
		return svg;
	},
	
	mouseDownHandler: function(e, svg){
		this.mouseState = {leftButtonDown : true,
						    ctrlKeyDown : e.ctrlKey,
						    shiftKeyDown : e.shiftKey,
							panning : false,
       						selecting : false,
       						zooming: false,
       						x : e.clientX,
       						y : e.clientY,
       						startX: e.clientX,
       						startY: e.clientY};
		
		this.currentSvg = svg;
	},
	
	mouseMoveHandler: function(e, svg){

		// if left mouse button is not clicked
		if(!this.detectLeftButton(e)){
			//if mouse down event invoked previously
			if(this.mouseState.leftButtonDown){
				this.mouseUpHandler(e);
			}
			return;
		}
		
		if(this.mouseState.leftButtonDown){
			if(this.mouseState.ctrlKeyDown){
				this.mouseState.selecting = true;
				this.setLinkIcon("multipleSelection", true);
				this.setLinkIcon("zoomWindow", false);
			} else if(this.mouseState.shiftKeyDown){
				this.mouseState.zooming = true;
				this.setLinkIcon("multipleSelection", false);
				this.setLinkIcon("zoomWindow", true);
			} else if(this.multipleSelectionOn){
				this.mouseState.selecting = true;
			} else if(this.zoomWindowOn){
				this.mouseState.zooming = true;
			} else {
				this.mouseState.panning = true;
			}
		}

		if(this.mouseState.panning){
			//remove selection window if any.
			d3.selectAll("div.svgSelectWindow").remove();

			// only pan if the mouse action is within the boundary of the current SVG
			if(svg.node() === this.currentSvg.node()){
				if (this.useD3Panning === false) {
					this.drawingController.getController("PanZoomController").pan(e.clientX-this.mouseState.x,e.clientY-this.mouseState.y, this.currentSvg);					
				}

				this.mouseState.x = e.clientX;
				this.mouseState.y = e.clientY;
				
   				if(this.config.afterPan){
   					this.config.afterPan(svg, this.drawingController);
   				}
			}
		} else if(this.mouseState.selecting || this.mouseState.zooming){
			// only select/zoom if the mouse action is within the boundary of the current SVG
			if(svg.node() === this.currentSvg.node()){
				this.drawingController.getAddOn("SelectWindow").update(e.clientX, e.clientY);
				
				if(this.mouseState.zooming && this.config.afterZoom){
					this.config.afterZoom(svg, this.drawingController);
				}
			}
		}
		
	},
	
	mouseUpHandler: function(e){
       	this.mouseState.leftButtonDown = false;

       	if(this.mouseState.panning){
       		if (this.useD3Panning === false) {
  				this.drawingController.getController("PanZoomController").pan(e.clientX-this.mouseState.x,e.clientY-this.mouseState.y, this.currentSvg);       			
       		}
       		this.mouseState.panning = false;
       	} else if(this.mouseState.selecting){
       		this.drawingController.getAddOn("SelectWindow").onAssetsSelected(this.currentSvg);
       		this.drawingController.getAddOn("SelectWindow").remove();
			this.mouseState.selecting = false;
			if(this.mouseState.ctrlKeyDown){
				this.setLinkIcon("multipleSelection", false);
			}
        } else if(this.mouseState.zooming){
        	var width, height, x, y;
        	if(this.mouseState.startY <  e.clientY){
        		height = e.clientY - this.mouseState.startY;
        		y = this.mouseState.startY;
        	}else{
        		height =  this.mouseState.startY - e.clientY;
        		y = e.clientY;
        	}
        	if(this.mouseState.startX <  e.clientX){
        		width = e.clientX - this.mouseState.startX;
        		x = this.mouseState.startX;
        	}else{
        		width =  this.mouseState.startX - e.clientX;
        		x = e.clientX;
        	}
        	this.drawingController.getController("PanZoomController").zoomIntoArea({ x: x, y: y, width: width, height:  height}, this.currentSvg);

        	this.updateLinkIcons();
        	this.drawingController.getAddOn("SelectWindow").remove();
			this.mouseState.zooming = false;
			if(this.mouseState.shiftKeyDown){
				this.setLinkIcon("zoomWindow", false);
			}
        }
	},
	
	detectLeftButton: function(e) {
	    if ('buttons' in e) {
	        return e.buttons === 1;
	    } else if ('which' in e) {
	        return e.which === 1;
	    } else {
	        return e.button === 1;
	    }
	},
	
	
	addLinkDiv: function(linkId, linkTitle){

		var liElem = document.createElement('li');
		liElem.id = "navToolbar_" + linkId;
		
		var linkDiv = document.createElement('a');
        linkDiv.className = linkId;
        linkDiv.id = linkId;
        linkDiv.title = linkTitle;
        
        liElem.appendChild(linkDiv);
        
        this.toolbarDiv.appendChild(liElem);
        
	},
	
	removeLinkDiv: function(linkId){
	    var linkDiv = document.getElementById("navToolbar_" + linkId);
	    if(linkDiv){
	    	this.toolbarDiv.removeChild(linkDiv);
	    }
	},
	
	setLinkIcon: function(linkId, selected){
		 var linkDiv = document.getElementById(linkId);
		 if(linkDiv){
			 linkDiv.className = (selected ? linkId + 'Select' : linkId);
			 if(linkId != 'multipleSelection' && linkId != 'zoomWindow')
				 linkDiv.disabled = selected;
			 else 
				 linkDiv.disabled = false;
		 }
	},
	
	addLinkEvent: function(linkId){
		//Chrome workaround to ensure both click and touch event works
        var eventName = ('ontouchstart' in document.documentElement && navigator.userAgent.toLowerCase().indexOf('chrome') == -1) ? 'touchstart' : 'click';
		var linkDiv = document.getElementById(linkId);
        var self = this;
        linkDiv.addEventListener(eventName,  function(event){
        	event.stopPropagation();
        	event.preventDefault();
        	if(linkDiv.className == 'zoomExtents'){
        		self.onZoomExtents();
        	} else if(linkDiv.className == 'zoomIn'){
        		self.onZoomIn();
        	} else if(linkDiv.className == 'zoomOut'){
        		self.onZoomOut();
        	} else if(linkDiv.className == 'multipleSelection' || linkDiv.className == 'multipleSelectionSelect'){
        		self.toggleMutlipleSelection();
        	} else if(linkDiv.className == 'zoomWindow' || linkDiv.className == 'zoomWindowSelect'){
        		self.toggleZoomWindow();
        	} else if(linkDiv.className == 'labelSizeIncrease'){
        		self.increaseLabelSize();
        	} else if(linkDiv.className == 'labelSizeDecrease'){
        		self.decreaseLabelSize();
        	}
        	
        }, false);
	},
	
	/**
	 * Increase the Label font size by the specified factor, the values will be between 0.1em (min) and 10em (max).
	 * 
	 * @param factor the specified factor
	 */
	increaseLabelSize: function(){

		var reachBoundary = this.drawingController.getControl().changeLabelSize(1.25);
		if(reachBoundary){
			this.setLinkIcon("labelSizeIncrease", true);
		} else {
			this.setLinkIcon("labelSizeIncrease", false);
			this.setLinkIcon("labelSizeDecrease", false);
		}
	},
	
	/**
	 * Decrease the Label font size by the specified factor, the values will be between 0.1em (min) and 10em (max).
	 * 
	 * @param factor the specified factor
	 */
	decreaseLabelSize: function(){
		var reachBoundary = this.drawingController.getControl().changeLabelSize(0.8);
		if(reachBoundary){
			this.setLinkIcon("labelSizeDecrease", true);
		} else {
			this.setLinkIcon("labelSizeIncrease", false);
			this.setLinkIcon("labelSizeDecrease", false);
		}
	},
	
	onZoomExtents: function(){
		var svg;
		var panZoomController = this.drawingController.getController("PanZoomController");
        var clusterControl = this.drawingController.getAddOn('Cluster');
		for(drawingName in this.drawingController.getControl().drawings){
			svg = this.drawingController.getControl().getSvgByDrawingName(drawingName);
			panZoomController.zoomExtents(svg);

			if (clusterControl) { 
				svg.call(clusterControl.zoom);

				if (this.multipleSelectionOn || this.zoomWindowOn) {
					this.removeD3Handlers(svg);
				}
	        }
			
			if(this.config.afterZoom){
				this.config.afterZoom(svg, this.drawingController);
			}
		}

		this.updateLinkIcon(1);
	},

	onZoom: function(type){
		var svg, status;
		var panZoomController = this.drawingController.getController("PanZoomController");
		var inStatus = true;
		var outStatus = true;
		var extentsStatus = true;
        for(drawingName in this.drawingController.getControl().drawings){
        	svg = this.drawingController.getControl().getSvgByDrawingName(drawingName);
	
        	if(type == 'in')
        		status = panZoomController.zoomIn(svg);
        	else
        		status = panZoomController.zoomOut(svg);
        	
        		
			if(status!==1){
				extentsStatus = false;
			} 
			
			if(status!==2){
				inStatus = false;
			} 
			
			if(status!==3){
				outStatus = false;
			}
			
			if(this.config.afterZoom){
				this.config.afterZoom(svg, this.drawingController);
			}
		}
		this.setLinkIcon('zoomExtents', extentsStatus);
		this.setLinkIcon('zoomIn', inStatus);
		this.setLinkIcon('zoomOut', outStatus);
	},
	
	updateLinkIcons: function(){
		var inStatus = true;
		var outStatus = true;
		var extentsStatus = true;
		var panZoomController = this.drawingController.getController("PanZoomController");
		var status = 0; 
        for(drawingName in this.drawingController.getControl().drawings){
        	svg = this.drawingController.getControl().getSvgByDrawingName(drawingName);
	
       		status = panZoomController.getZoomState(svg);
        		
			if(status!==1){
				extentsStatus = false;
			} 
			
			if(status!==2){
				inStatus = false;
			} 
			
			if(status!==3){
				outStatus = false;
			}
		}
		this.setLinkIcon('zoomExtents', extentsStatus);
		this.setLinkIcon('zoomIn', inStatus);
		this.setLinkIcon('zoomOut', outStatus);
		this.setLinkIcon("labelSizeIncrease", false);
		this.setLinkIcon("labelSizeDecrease", false);
	},
	
	onZoomIn: function(){
		this.onZoom("in");
	},

	onZoomOut: function(){
		this.onZoom("out");
	},

	toggleMutlipleSelection: function(){
		this.multipleSelectionOn = !this.multipleSelectionOn;
		this.drawingController.getController('SelectController').setMultipleSelection(this.multipleSelectionOn);
		this.setLinkIcon("multipleSelection", this.multipleSelectionOn);
		
		if(this.multipleSelectionOn){
			this.zoomWindowOn = false;
			this.setLinkIcon("zoomWindow", false);
			
			if (this.useD3Panning && this.currentSvg) {
				this.removeD3Handlers(this.currentSvg);
			}
		} else {
			this.drawingController.getAddOn("SelectWindow").remove();
			if (this.useD3Panning && this.currentSvg) {
				this.addD3Handlers(this.currentSvg);
			}
		}
	},

	
	toggleZoomWindow: function(){
		this.zoomWindowOn = !this.zoomWindowOn;
		this.setLinkIcon("zoomWindow", this.zoomWindowOn);
		
		if(this.zoomWindowOn){
			this.multipleSelectionOn = false;
			this.drawingController.getController('SelectController').setMultipleSelection(false);
			this.setLinkIcon("multipleSelection", false);
			if (this.useD3Panning && this.currentSvg) {
				this.removeD3Handlers(this.currentSvg);
			}			
		} else {
			this.drawingController.getAddOn("SelectWindow").remove();
			if (this.useD3Panning && this.currentSvg) {
				this.addD3Handlers(this.currentSvg);
			}
		}
	},
	
	updateLinkIcon: function(status){
		this.setLinkIcon('zoomExtents', (status==1));
		this.setLinkIcon('zoomIn', (status==2));
		this.setLinkIcon('zoomOut', (status==3));
		this.setLinkIcon("multipleSelection", this.multipleSelectionOn);
	},
	
	show: function(bShow) {
		var navToolbarContainer = document.getElementById(this.getContainerDivId() + "_navToolbarContainer");
		if(!navToolbarContainer){
			this.init();
		}
		
		this.toolbarDiv.style.display = (bShow === true) ? '' : 'none';
		
		this.updateLinkIcons();
    },
    
    showLink: function(linkId, bShow){
    	var linkDiv = document.getElementById(linkId);
    	if(linkDiv){
    		linkDiv.style.display = (bShow === true) ? '' : 'none';
    	}
    },
 
    getContainerDivId: function(){
  		 return DrawingCommon.retrieveValidNodeId(this.config.divId);
    }
}, {});
