/**
 * Markup Control functionality with Redline Legends Panel, Color Picker and drag-drop support
 */
Drawing.namespace("view");

Drawing.view.MarkupControl = Base.extend({

    drawingController: null,
	
    /**
     * default redline legend's configuration
     */
    config: {
    		divId: '',     		// SVG drawing divId
        	panelId: '',   		//legend's panel id
	    	legendId: '',  		// legend's div Id
	    	colorPickerId: '',  // the div id to hold the color picker control
	    	redlineTypes: [ "cloud", "line", "textbox", "arrow" , "area", "swatch", "arrowTextbox", "textOnly"],
	        onDrop: null,
	        onChange: null,
	        onSave: null,
	        currentColor: "#FF0000",
	        legendTitle: '',
	        style: {legendWidth: '75',
	        		legendHeight: '75',
	        		legendMargin: '5px 0px 0px 5px'
	        	   },
	   	    scaleFactor: 2,
	   	    currentFontSize: 10,
	    },
    
    colorPickerControl: null,
    
    //id for redlines
    id: -1,
    
    // @begin_translatable
    z_MESSAGE_ENTER_TEXT: 'Please enter the text box content:',
    z_MESSAGE_DELETE: 'Are you sure you want to delete this object?',
    z_TITLE: 'Redline',
    Z_REDLINETYPES_CLOUD: "Cloud",
    Z_REDLINETYPES_LINE: "Line",
    Z_REDLINETYPES_TEXTBOX: "Textbox",
    Z_REDLINETYPES_ARROW: "Arrow",
    Z_REDLINETYPES_AREA: "Area",
    Z_REDLINETYPES_SWATCH: "Swatch",
    Z_REDLINETYPES_ARROW_TEXTBOX: "Arrow Textbox",
    Z_REDLINETYPES_TEXT_ONLY: "Text Only",
    // @end_translatable

    constructor: function (config) {
    	
    	this.inherit();
    	
        this.readConfig(config);
        
    	this.loadColorPicker();
    },
	
    /**
     * set reference to drawingController
     */
    setDrawingController: function(drawingController){
    	this.drawingController = drawingController;
    },
    
    /**
     * set config object
     */
    readConfig: function(config){
    	
    	for (var key in config){
    		if(key === 'redlineTypes' && config.redlineTypes.length > 0){
            	this.config.redlineTypes = config.redlineTypes;
            	continue;
    		}
    		
    		if(key=='style'){
    			if(config.style.legendWidth)
            		this.config.style.legendWidth = config.style.legendWidth; 
            	
            	if(config.style.legendHeight)
            		this.config.style.legendHeight = config.style.legendHeight; 
            	
            	if(config.style.legendMargin)
            		this.config.style.legendMargin = config.style.legendMargin; 
            	
            	continue;
    		}
    		
    		if(key){
    			this.config[key] = config[key];
    		}
    	}
    	
    	if(typeof this.config.colorPickerId === 'undefined' || this.config.colorPickerId){
    		this.config.colorPickerId = this.config.panelId + "_head";
    	}
    },
    
    /**
     * load redline legend and attach drag-drop event to the given SVG drawing 
     */
    loadLegend: function(svg) {

    	var control = this;
    	control.config.unitsFactor =  DrawingCommon.getUnitsFactor(svg);
    	
    	var fn = control.onChange.createDelegate(this, [d3.select(this).node(), control], true);
        var dragMove = placement.move(fn),
            edit = placement.edit(fn),
            group = placement.group()
                .on("clone", function() {
                    d3.select(this)
                        .call(dragMove)
                        .call(edit);
                });

        svg.call(group);
        
        if (DrawingCommon.isModernBrowser()) {
            svg.style("cursor", "move");
        }

        this.setupMarkupLegends(svg);
    },
    
    /**
     * loads color picker with custom panel title
     */
    loadColorPicker: function(){
    	// only create pick if it is panel
    	if(this.config.colorPickerId && document.getElementById(this.config.colorPickerId)!=null){
    		this.colorPickerControl = new Drawing.view.MarkupColorPicker(this.config.colorPickerId, this.config.legendTitle, this);
    	}
    },
    
    /**
     * Set up markup legends and drag-drop events
     */
    setupMarkupLegends: function(svg) {

    	var control = this;
    	var panel = d3.select("#" + this.config.divId);
		
        redline.z_MESSAGE_ENTER_TEXT = this.z_MESSAGE_ENTER_TEXT;
        redline.z_MESSAGE_DELETE = this.z_MESSAGE_DELETE;
        redline.z_TITLE = this.z_TITLE;
        
        var draggable = d3.select(".redline-legend").selectAll(".draggable")
        	.data(this.config.redlineTypes)
            .enter().append("svg")
            .attr("width", this.config.style.legendWidth)
            .attr("height", this.config.style.legendHeight)
            .style("margin", this.config.style.legendMargin)
            .attr("class", "draggable")
            .append("g")
            .attr("transform", "scale(0.5)")
            .attr("class", function(type) { return "redline " + type; })
            .call(placement.drag()
                .on("drop", function(type, leftOffset) {
                	control.removeGrips();
                	var fn = control.onChange.createDelegate(this, [d3.select(this).node(), control], true);
                    var dragMove = placement.move(fn);
                    d3.select(this.parentNode)
                        .call(dragMove)
                        .on("touchstart.redline-edit", function() {
                            d3.event.stopPropagation();
                            if(d3.event.defaultPrevented) {
                                return;
                            }
                        })
                        .on("click", function() {
                            d3.event.stopPropagation();
                            if(d3.event.defaultPrevented) {
                                return;
                            }
                        });
                    
                    if (type == 'textbox' || type == 'textOnly') {
                        d3.select(this).select(".letterA").remove();                    	
                    } else if (type == 'arrowTextbox') {
                    	d3.select(this).select(".letter1").remove(); 
                    }
                    
                    d3.select(this)
                        .classed("dropped", true)
                        .attr("id", "redline_" + (++control.id))
                        .call(redline[type].edit, [true, control.config.currentFontSize, fn, control.config.showAreaLabel, control.config.unitsFactor, true], true)
                        .style("left", leftOffset +"px");

                    d3.select(this).select("rect").attr("id", "rect_" + (control.id));
                    
                    RedlineCommon.setScale(d3.select(this), control.config.unitsFactor);

                    control.onDrop([d3.select(this).node()], control, type);
                })
                .target(function(target) {
                    var redlineTarget = control.drawingController.getController("MarkupController").createGroupIfNotExists(panel.selectAll("#viewer"), "redlines", "redlines").node();
                    var goodTarget = control.isValidDroppingTarget(target, control.config.divId);
                    if(redlineTarget && goodTarget){
                        var svgDiv = svg.parentNode; 
                        if ((svgDiv && svgDiv.contains(target.correspondingUseElement) || target)){
                            return redlineTarget;
                        }
                    }

                
                
                }));
        draggable.append("rect")
            .style("pointer-events", "all")
            .style("fill", "none")
            .attr("width", control.config.style.legendWidth*control.config.scaleFactor)
            .attr("height", control.config.style.legendHeight*control.config.scaleFactor - 15);
        
        draggable.each(function(type) {
            d3.select(this).call(redline[type]);
            
            var textTyeFontSize;
			if (type == 'textbox' || type == 'textOnly') {
				textTyeFontSize = (36 * control.config.scaleFactor) + 'px';
				d3.select(this).append("g").attr("transform", "translate(" + (12 * control.config.scaleFactor+5) + ", " + (36 * control.config.scaleFactor+5) + ")")
            		.classed({"letterA" : true})
            		.style({
            			'font-family': 'Arial',
            			'fill' : '#f00',
            			'font-size': textTyeFontSize
            		})
            		.append("text")
            		.text("A");
            } else if (type == 'arrowTextbox') {
            	textTyeFontSize = (20 * control.config.scaleFactor) + 'px';
				d3.select(this).append("g").attr("transform", "translate(" + (23 * control.config.scaleFactor) + ", " + (36 * control.config.scaleFactor) + ")")
            		.classed({"letter1" : true})
            		.style({
            			'font-family': 'Arial',
            			'fill' : '#f00',
            			'font-size': textTyeFontSize
            		})
            		.append("text")
            		.text("1");            	
            }
		    
			d3.select(this).append("text")
				.attr("width", control.config.style.legendWidth*control.config.scaleFactor)
	   		 	.attr("height", 15*control.config.scaleFactor)
	   		 	.attr("x",  function(type){
	   			 		return Math.max(0, (control.config.style.legendWidth*control.config.scaleFactor - type.length * 10)/(2*control.config.scaleFactor));
	   		 		})
	   		 	.attr("y",  (control.config.style.legendHeight - 10)*control.config.scaleFactor)
		   		 .style({
	        			'font-family': 'Arial',
		       			'font-size': (10 * control.config.scaleFactor) + 'px',
		       			'align':  'center'})
		   		 .text(function(type){
		   			 	var typeText = "";
		   			 	switch(type){
		   			 		case "cloud": 
		   			 			typeText = View.getLocalizedString(control.Z_REDLINETYPES_CLOUD);
		   			 			break;

		   			 		case "line":
		   			 			typeText = View.getLocalizedString(control.Z_REDLINETYPES_LINE);
		   			 			break;
		   			 			
		   			 		case "textbox":
		   			 			typeText = View.getLocalizedString(control.Z_REDLINETYPES_TEXTBOX);
		   			 			break;
	
		   			 		case "arrow":
		   			 			typeText = View.getLocalizedString(control.Z_REDLINETYPES_ARROW);
		   			 			break;
		   			 		
		   			 		case "area":
		   			 			typeText = View.getLocalizedString(control.Z_REDLINETYPES_AREA);
		   			 			break;
		   			 			
		   			 		case "swatch":
		   			 			typeText = View.getLocalizedString(control.Z_REDLINETYPES_SWATCH);
		   			 			break;
		   			 		
		   			 		case "arrowTextbox":
		   			 			typeText = View.getLocalizedString(control.Z_REDLINETYPES_ARROW_TEXTBOX);
		   			 			break;
		   			 			
		   			 		case "textOnly":
		   			 			typeText = View.getLocalizedString(control.Z_REDLINETYPES_TEXT_ONLY);
		   			 			break;
		   			 			
		   			 		default:
		   			 			typeText = type.toLowerCase().replace(/\b[a-z]/g, function(letter) {
			   			 			return  letter.toUpperCase();
			   			 		});	
		   			 	}
			        
	   			 		return typeText;
		   		 });
        });

        
        this.setCurrentColor(this.config.currentColor);
    },

    /**
     * Calls default events, such as set 'hasUnsavedChanges' and create area size label for Area Box after dropping the markup then calls the custom onDrop event handler.  
     * 
     * @node element dropped on SVG drawing
     * @control Drawing.view.MarkupControl markup control object
     * @type String the legend type
     */
    onDrop: function(params, control, type){
        control.drawingController.getController("MarkupController").hasUnsavedChanges = true;

        if (control.config.onDrop) {
            control.config.onDrop(params[0], control.drawingController);
        }
    },
    
    /**
     * Calls default events, such as set 'hasUnsavedChanges' and create area size label for Area Box after changing the markup then calls the custom onChange event handler.  
     * 
     * @node element changed on SVG drawing
     * @control Drawing.view.MarkupControl markup control object
     * @type String the legend type
     */
    onChange: function(params, node, control){
    	control.drawingController.getController("MarkupController").hasUnsavedChanges = true;
    	
    	if(params[0]){
        	node = params[0];
    	}

    	if(params[3] !== true && d3.select(node).classed('area')){
    		control.appendAreaInfo(params, control, false);
    	}
    	
    	if(control.config.onChange){
    		control.config.onChange(params[0], control.drawingController);
    	}
    },

    /**
     * Creates area text label.
     * 
     * @node element area box to append text label to.
     * @control Drawing.view.MarkupControl markup control object
     * @isEdit boolean true if user clicks to enter into 'edit' mode, false otherwise.
     */
    appendAreaInfo: function(params, control, isEdit){
    
    	var area = control.getArea(params);
    	var node = params[0];
    	
    	var textNode = d3.select(node).select("text");
		if(textNode.empty()){
			d3.select(node)
			.append("text")
            .attr("id", "text_" + this.id)
			.classed({"area-text": true})
			.style("fill", control.config.currentColor)
			.attr("dx", Math.max(area[1]/2-100, 0) + "px")
    	    .attr("dy", "-10px")
    	    .text(area[0]);
		} else {
			if(isEdit){
				textNode.text(area[0]);
			} else {
				textNode.attr("dx", Math.max(area[1]/2-100, 0) + "px")
					.attr("dy", "-10px")
		    	    .text(area[0]);
			}
		}
    },
    /**
     * Retrieves Area information, such as label, width and height for the specified markup node.
     * @node element the specofoed area box
     */
    getArea: function(params){
		
		var node = params[0];
		var svg = d3.select(node.farthestViewportElement);
		var factor = this.drawingController.getController("PanZoomController").getZoomFactor(svg);
		
		var rectNode = d3.select(node).select("rect").node();
		var width = (params[1] ? params[1] : rectNode.width.baseVal.value);
		var height = (params[2] ? params[2] : rectNode.height.baseVal.value);
	
		var units =  DrawingCommon.getUnits(svg);
		var areaSize = (width/factor)*(height*factor);

		return [this.formatArea(areaSize, units), width, height];
    },
	
    /**
     * Formats the area size by converting it into big units(inches->ft, mm/cm->m) then adding decimal and thousands separators for the user's locale.
     * @areaSize Number the area size to convert.
     * @units    String the original unit to convert.
     */
    formatArea: function(areaSize, units){
    	var convertedAreaSize = areaSize;
    	var convertedUnits;

    	switch(units) {
        case "mm":
        	convertedAreaSize = areaSize*1.75/1000000000;
        	convertedUnits = "m\xB2";
            break;
        case "cm":
        	convertedAreaSize = areaSize*1.75/10000000;
        	convertedUnits = "m\xB2";
        	break;
        case "m":
        	convertedAreaSize = areaSize*1.75/1000;
        	convertedUnits = "m\xB2";
            break;
        case "Metres":
        	convertedAreaSize = areaSize*1.75/1000;
        	convertedUnits = "m\xB2";
            break;
        case "inches":
        	convertedAreaSize = areaSize/144;
        	convertedUnits = "ft\xB2";
        	break;
        case "ft":
        	convertedAreaSize = areaSize;
        	convertedUnits = "ft\xB2";
        	break;
        default:
        	convertedAreaSize = areaSize;
        	convertedUnits = units + "\xB2";
        	convertedUnits = "sq " + units ;
    	}
    	
    	var formattedArea = "";
    	if(convertedAreaSize > 1)
    		formattedArea = (DrawingCommon.addDecimalAndGroupingSeparator(convertedAreaSize.toFixed(0)) + " " + convertedUnits);
   		else if (convertedAreaSize > 0.01)
    		formattedArea = (DrawingCommon.addDecimalAndGroupingSeparator(convertedAreaSize.toFixed(2)) + " " + convertedUnits);
   		else if (convertedAreaSize > 0.0001)
    		formattedArea = (DrawingCommon.addDecimalAndGroupingSeparator(convertedAreaSize.toFixed(4)) + " " + convertedUnits);
   		else
    		formattedArea = (DrawingCommon.addDecimalAndGroupingSeparator(convertedAreaSize.toFixed(6)) + " " + convertedUnits);
   
    	return formattedArea;
    },
    
    isValidDroppingTarget: function(target, divId){
    	 var targetId = (target? target.id : null);
         var goodTarget = (targetId && (targetId.substring(0, divId.length) == divId) && (targetId.substring(targetId.length-4) == "-svg"));
         if(!goodTarget && target.farthestViewportElement){
        	 target = target.farthestViewportElement;
        	 targetId = (target? target.id : null);
        	 goodTarget = (targetId && (targetId.substring(0, divId.length) == divId) && (targetId.substring(targetId.length-4) == "-svg"));
         }
         return goodTarget;
        
    },
    
    /**
     * Sets redline control legends to a new color.
     * 
     * @param newColor color to set in HTML color code format, i.e #FF0000
     */
    setCurrentColor: function(newColor){
    	
    	if(!newColor)
    		return;
    	
    	var pos = newColor.search("#");
    	if(pos == -1)
    		newColor = '#' + newColor;
    	
    	this.config.currentColor = newColor;
    	
    	d3.select(".redline-legend").selectAll(".redline-path")
    		.style("stroke", this.config.currentColor);
		
    	d3.select(".redline-legend").selectAll(".arrow-path")
			.style("fill", this.config.currentColor)
			.style("stroke", this.config.currentColor);

    	d3.select(".redline-legend").selectAll(".area-path")
			.style("fill", this.config.currentColor)
			.style("stroke", this.config.currentColor)
			.style("fill-opacity", 0.3);

    	d3.select(".redline-legend").selectAll(".swatch-path")
			.style("fill", this.config.currentColor)
			.style("stroke", this.config.currentColor);

    	d3.select(".redline-legend").select(".textbox-arrow > path")
    		.style("fill", this.config.currentColor)
    		.style("stroke", this.config.currentColor)

    	d3.select(".redline-legend").select(".textbox-arrow > circle")
			.style("stroke", this.config.currentColor);
    	
    	d3.select(".redline-legend").selectAll(".letterA")
    		.style("fill", this.config.currentColor);
    	
    	d3.select(".redline-legend").selectAll(".letter1")
    		.style("fill", this.config.currentColor);
	
    	
    	if(this.colorPickerControl)
    		this.colorPickerControl.updateCustomColor(this.config.currentColor);
 
   },
   
   
   setCurrentFontSize: function(isIncrease){
	
	   var multiplier = (this.config.currentFontSize-10)/5;
	   if(isIncrease)
		   multiplier++;
	   else
		   multiplier--;
			   
	   this.config.currentFontSize = (10 + 5 * multiplier) > 0 ? (10 + 5 * multiplier) : 0;
   },
   
	/**
    * Saves SVG drawing's extent, drawing name, action item id to afm_redlines table.
    * Save redlines and image content (if any) to its document table and update the related file name in afm_redlines table.
    * Capture the floor plan with redlines and highlight, save to activity_log.doc4 field. 
    * 
    * @param parameters parameters needed for saving the drawing, such as activity log id etc.
    * @updateIfExist boolean true if update for the existing record, false will always add a new record.
    */
   save: function(parameters, updateIfExists){
	   var params = this.drawingController.getController("MarkupController").save(parameters, updateIfExists);
	   
	   if(params && this.config.onSave){
		   this.config.onSave(params);
	   }
   },
   
   removeGrips: function(){
	   d3.select(".dropped").each(function(){
	 	  var node = d3.select(this);
		  node.select(".edit").remove();
	   });
   }
}, {});