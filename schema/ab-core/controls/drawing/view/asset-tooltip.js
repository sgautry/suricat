Drawing.namespace("view");

/**
 * Display tooltips.
 * 
 * This is usually invoke through the mouseover event.
 */
Drawing.view.AssetTooltip = Base.extend({

	config: {},
	
	/**
	 * reference to drawingController
	 */
	drawingController: null,
	
	/**
	 * List of tooltips specified by their asset types.
	 */
	tooltipsMap: {},
	
	/**
	 * true to display tooltip, false otherwise
	 */
	showTooltip: false,
	
	/**
	 * tooltip dom container
	 */
	tooltip: null,
	
	/**
	 * value returned by setTimeout to cancal timeout later
	 */
	tooltipTimeout: null,
	
	constructor: function (config) {
    	this.config = config;
    	
    	if(config.showTooltip){
			this.showTooltip = DrawingCommon.toBoolean(config.showTooltip);
		}
    		
    	this.init();

	},

	/**
     * set reference to drawingController
     */
    setDrawingController: function(drawingController){
       this.drawingController = drawingController;
    },

    /**
     * create a tooltip node.
     */
    init: function(){
    	//store tootltip content for reusage
    	this.tooltipsMap.htmlContents = {};
    	//tooltip dom container. 
    	this.tooltip = d3.select("div.svgTooltip");
    	
    	if(this.showTooltip && (typeof this.tooltip === 'undefined' || !this.tooltip || this.tooltip.empty())){
    		//CSS class svgTooltip is defined in htmldrawing.css
    		this.tooltip = d3.select("body").append("div")   
    	    .attr("class", "svgTooltip")               
    	    .style("opacity", 0)
    	    .html('');
    	}
    },
    /**
     * Clears tooltip dom object.
     */
    clear: function(){
    	if(this.tooltip && this.tooltip.length > 0){
    		jQuery(this.tooltip[0]).empty();
    	}
    	this.tooltipsMap.htmlContents = null;
    	this.tooltipsMap = null;
    },
    
    /**
     * setter for showTooltip, commonly used when loading a new drawing to show/hide tooltip.
     * 
     * @param show true if show tooltip and false to hide.
     */
    setShowTooltip: function(show){
    	this.showTooltip = show;
    },
    /**
     * Called by AddOnsController
     */
    registerEvents: function(svg){
    	if(this.config.handlers && this.config.handlers.length > 0){
    		var eventHandlers = [];
    		for(var index = 0; index < this.config.handlers.length; index++){
    			var handler = this.config.handlers[index];
    			var eventHandler = {};
    			var assetType = (handler && handler.assetType ? handler.assetType : null);
    			if(typeof assetType !== 'undefined' && assetType != null){
    				var tooltipListener = this.tooltipHandler;
    				
    				if(handler && handler.datasource){
    					var datasource = handler.datasource;
        				if(_.isString(handler.datasource)){
        					datasource = View.dataSources.get(handler.datasource);
        				}
        				
    					var fields = [];
    					if(handler.fields){
    						if(_.isString(handler.fields)){
    							fields = handler.fields.split(";");
    						}else{
    							fields = handler.fields;
    						}
    					}else{
    						//get visible fields from datasource
    						datasource.fieldDefs.each(function(fieldDef) {
    							if(fieldDef.hidden !== 'true'){
    								fields.push(fieldDef.id);
    							}
    				        });
    					}
    					
    					var keyFields = [];
    					if(handler.keyFields){
    						if(_.isString(handler.keyFields)){
    							keyFields = handler.keyFields.split(";");
    						}else{
    							keyFields = handler.keyFields;
    						}
    					}else{
    						//get primary fields from datasourec 
    						for (var i = 0; i < datasource.fieldDefs.length; i++) {
    				            fieldDef = datasource.fieldDefs.items[i];
    				            if (fieldDef.primaryKey && fieldDef.id.indexOf(datasource.mainTableName + ".") === 0) {
    				            	keyFields.push(fieldDef.id);
    				            }
    				        }
    					}
    					
        				this.tooltipsMap[assetType] = {'datasource':  datasource, 'fields': fields, 'keyFields': keyFields};
    				}
    				var assetOnly = (handler && handler['assetOnly']) ? handler['assetOnly'] : false;
    				var highlightOnly = (handler && handler['highlightOnly']) ? handler['highlightOnly'] : false;
    				
    				var customHandler = (handler && handler['handler'] ? handler['handler'] : null);
    				if(customHandler){
						eventHandlers[eventHandlers.length] = {'eventName': 'mouseover', 'assetType' : assetType, 'assetOnly': assetOnly, 'highlightOnly': highlightOnly, 'handler' : customHandler.bind(this)};
    				} else {
    					eventHandlers[eventHandlers.length] = {'eventName': 'mouseover', 'assetType' : assetType, 'assetOnly': assetOnly, 'highlightOnly': highlightOnly, 'handler' : this.show.bind(this)};
    				}
    				eventHandlers[eventHandlers.length] = {'eventName': 'mouseout', 'assetType' : assetType, 'handler' : this.hide.bind(this)};
    			}
    		}
    		if(eventHandlers && eventHandlers.length > 0){
    			this.drawingController.getController("EventController").addEventHandlers(svg, eventHandlers);
    		}
    	}
    },
    
    /**
     * Shows tooltips.
     * Fired by mouse over event.
     */
    show: function(params, drawingController, event){
    	if(!this.showTooltip){
    		return;
    	}
 
    	var tooltipDiv = this.tooltip;
    	var content =  this.getContent(params.assetType, params.assetId);
    	
    	this.tooltipTimeout = setTimeout(function(){
			tooltipDiv.style("left", (event.clientX+5) + "px")     
				.style("top", (event.clientY+5) + "px")
				.style("opacity", 0.9)
				.html(content);
		}, 300);
    },
    
    /**
     * Gets tooltip content.
     * 
     * @param assetType asset type like rm.
     * @param assetId asset id like 'HQ;19;105'.
     */
    getContent: function(assetType, assetId){
    	var content = '';
		if(this.tooltipsMap[assetType]){
    		//retreats from datasource
			if(this.tooltipsMap.htmlContents[assetId]){
				content = this.tooltipsMap.htmlContents[assetId];
			}else{
				content = this.getContentFromDatasource(assetType, assetId, this.tooltipsMap[assetType]);
				//store it for reuse to reduce server-side data queries
				this.tooltipsMap.htmlContents[assetId] = content;
			}
    		
    	}else{
    		//grasp current SVG asset labels
    		content = this.getContentFromLabels(assetType, assetId);
    	}
		return content;
    },
 
    /**
     * Hides tooltips.
     */
    hide:function(params, drawingController, event){
    	if(!this.showTooltip)
        	return;
    	
    	if(this.tooltipTimeout){
    		clearTimeout(this.tooltipTimeout);
    	}
    	
    	this.tooltip
    				.style("left", "-50px")     
					.style("left", "-50px")     
					.style("top", 0)
    				.style("opacity", 0)
    				.html('');
    },
    
    /**
     * Retrieves tooltip content from specified datasource records.
     */
    getContentFromDatasource: function(assetType, assetId, datasourceMap){
    	var assetPk = assetId.split(";");
		var ds = datasourceMap.datasource;
		var restriction = new Ab.view.Restriction();
	
		if(datasourceMap.keyFields && datasourceMap.keyFields.length > 0){
			for (var i = 0; i < datasourceMap.keyFields.length; i++) {
				restriction.addClause(datasourceMap.keyFields[i], assetPk[i], "=", true);
	        }
		}
		var records = ds.getRecords(restriction);
		var values = [];
		var fields = datasourceMap.fields; 
		for(var i=0, len=records.length, record; i<len; i++){
			record = records[i];
			for(var j=0, field, value; j< fields.length; j++){
				field = fields[j];
				value = record.getValue(field);
				if(value && values.indexOf(value) < 0){
					//TODO: why ecord.getLocalizedValue(field) in  ds.getRecords()????
					values.push(ds.formatValue(field, value, true));
				}
			}
		}
		
		var content = '';
		if(values.length > 0){
			content = values.join('<br>');
		}
		
		if(content){
    		return content;
    	} else {
    		return this.drawingController.getController("AssetController").getOriginalAssetId(assetId);;
    	}
    },
    /**
     * Retrieves tooltip content from targeted SVG asset labels.
     */
    getContentFromLabels: function(assetType, assetId){
    	var content = '';
    	var label = d3.select("[id='l-"+assetType+"-" + assetId + "']");
    	if(label === undefined || label=== null){
    		return assetId;
    	}
    	
    	var texts = label.selectAll('text');
    	var text;
    	texts.each(function() {
			text = d3.select(this).text();
			if(text && text !== ''){
				content += text + '<br>';
			}
		});
    	
    	if(content){
    		return content;
    	} else {
    		return this.drawingController.getController("AssetController").getOriginalAssetId(assetId);
    	}
    }
}, {});