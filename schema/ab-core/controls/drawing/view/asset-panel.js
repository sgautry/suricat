/**
 * Asset Panel Drag and Drop.
 */
Drawing.namespace("view");
Drawing.view.AssetPanel = Base.extend({

	// a map of SVGs; i.e. {drawingDiv-hq17-svg: [svg], drawingDiv-hq15-svg: [svg]}
	svgs: {}, 
	
    drawingController: null,

	DEFAULT_HEIGHT: 130,
	DEFAULT_WIDTH: null,
	DEFAULT_FONT: {fontFamily:'Arial', fontSize: 12},
	DRAG_ICON_HEIGHT: 15,	
	DRAG_ICON_WIDTH: 15,
	DELIMITER: '||^||',
	
	// The following are based on publishing naming conventions
	LABEL_PREFIX: 'l-',
	LABEL_PREFIX_RM: 'l-rm-',
	LABELS_SUFFIX: '-labels',
	ASSETS_SUFFIX: '-assets',
	
	// @begin_translatable
    z_MESSAGE_ASSET_PANEL_TITLE: 'Waiting Room',
    // @end_translatable
	
    constructor: function (config) {
    	this.config = config;
    	this.title = (this.config['title']) ? this.config['title'] : '&nbsp;';
    	if(this.title === this.z_MESSAGE_ASSET_PANEL_TITLE){
    		this.title =  View.getLocalizedString(this.z_MESSAGE_ASSET_PANEL_TITLE);
    	}
    	this.height = (this.config['size'] && this.config['size']['height']) ? this.config['size']['height'] : this.DEFAULT_HEIGHT;
    	this.width = (this.config['size'] && this.config['size']['width']) ? this.config['size']['width'] : this.DEFAULT_WIDTH;
    	this.fontFamily = (this.config['font'] &&  this.config['font']['fontFamily']) ? this.config['font']['fontFamily'] : this.DEFAULT_FONT['fontFamily'];
    	this.fontSize = (this.config['font'] &&  this.config['font']['fontSize']) ? this.config['font']['fontSize'] : this.DEFAULT_FONT['fontSize'];
		this.dragIcon = (this.config['dragIcon']) ? this.config['dragIcon'] : Ab.view.View.contextPath + '/schema/ab-core/graphics/icons/drawing/pion.png';
    	this.layer = (this.config['layer']) ? this.config['layer'] : 'rm';
    	this.layerLabelPrefix = (this.config['layer']) ? this.LABEL_PREFIX + this.config['layer']  + '-'  : this.LABEL_PREFIX_RM;
    	   	
		this.spinner = new Spinner();
	    this.addDragEffectTemplate();
    },
	
    setDrawingController: function(drawingController){
    	this.drawingController = drawingController;
    },
 	
    /**
     * Public.  Load the asset panel.
     * restriction
     */
    loadAssetPanel: function(restriction) {
		this.spinner.spin(document.getElementById(this.drawingController.config.divId));
		var dataSourceName = this.config['dataSourceName'];
		var panelRecords = this.getRecordsFromDataSource(dataSourceName, restriction);
		this.createAssetPanel(panelRecords);  
	    this.spinner.stop();
    },

    /**
     * Public.  Refresh the asset panel. Only updates the tbody.
     * restriction
     */
	refreshAssetPanel: function(restriction){
		var dataSourceName = this.config['dataSourceName'];
		var panelRecords = this.getRecordsFromDataSource(dataSourceName, restriction);
		
		var table = this.assetPanel.select(".ap-table");
		table.select('tbody').html('<tbody></tbody>');
		this.populateTableWithData(table, panelRecords);
		this.syncAssetPanelDimensions();
	},
	
	/**
	 * Public.
	 */
	showAssetPanel: function() {
		if (!this.assetPanel.empty()) {
			this.assetPanel.style("visibility", null);			
		}	
	},
	
	/**
	 * Public
	 */
	closeAssetPanel: function() {
		if (!this.assetPanel.empty()) {
			this.assetPanel.style("visibility", "hidden");			
		}			
	},
	
	getRecordsFromDataSource: function(dataSourceName, restriction) {
		var ds = View.dataSources.get(dataSourceName);		
		if (restriction) {
			ds.setRestriction(restriction);			
		}
		var records = ds.getRecords();
		return records;
	},
	     	
	createAssetPanel: function(panelRecords) {   	    
		if (!d3.select('.ap-panel').empty()) {
			d3.select('.ap-panel').remove();
		}				       
	    	    	    
	    // create asset panel and attach to main drawings div; asset panel is hidden initially
	    var drawingsDiv = d3.select('#' + this.drawingController.config.divId);
		this.assetPanel = drawingsDiv    
	        .append("div")
	        .classed({"ap-panel": true})
	        .style("font-size", this.fontSize + "px")
	        .style("font-family", this.fontFamily)
	        .style("visibility", "hidden");
	    
	    // title bar, with collapse button if applicable
		var titlebarHtml = '<div class="ap-titlebar" droppable="true">' + this.title + '</div>';
		if (this.config['collapsible']) {
			titlebarHtml += '<div class="collapse-button minimize"/>';
		}
	    this.assetPanel.append("div").html(titlebarHtml);
	    	   
		// create table	    
	    var table = this.assetPanel.append("div")
	    	.classed({'ap-tablediv': true})
	        .attr("droppable", true)
	        .html('<table class="ap-table"><thead><tr class="ap-headerrow"></tr></thead><tbody></tbody></table>');
	    
	    this.populateTableWithData(table, panelRecords);	
	    this.makeAssetPanelDraggable();
	    this.makeAssetPanelScrollable();
		this.makeAssetPanelCollapsible();
	    this.syncAssetPanelDimensions();
	},
	
	populateTableWithData: function(table, panelRecords) {
		var control = this;
		
	    // populate asset panel with data
		try {			
			var dataSourceName = this.config['dataSourceName'];
			var ds = View.dataSources.get(dataSourceName);
			var fieldDefs = ds.fieldDefs;
			var keys = ds.fieldDefs.keys;
			this.keys = keys;
			
			// create header cells for data
			for (var i=0; i<keys.length; i++) {	
				var title = ds.fieldDefs.get(keys[i]).title;
				var hidden = ds.fieldDefs.get(keys[i]).hidden;
				if (hidden !== 'true') {
					table.select('.ap-headerrow').append("td").attr("droppable", true).classed({'ap-headercell': true}).text(title);					
				}
			}
			
			// create headers cells for rowActions (edit, delete, etc)
			for (var j=0; j<this.config.rowActions.length; j++) {
				table.select('.ap-headerrow').append('td').attr("droppable", true).classed({'ap-headercell': true});
			}
			
		    // create data rows
			for (var i=0; i<panelRecords.length; i++) {		
				var tr = this.addRowToAssetPanel(panelRecords[i], keys, fieldDefs);					
				//this.addRowToAssetPanel({'em.em_id': 'ABERNATHY,ALLISON', 'em.location': 'HQ-17-102'}, 'em.em_id');
			}
		} catch (e) {
			console.log(e);
		}
	},
	
	addRowToAssetPanel: function(panelRecord, keys, fieldDefs){
		var control = this;		
		var pkeyId = keys[0];
		var rowLength = this.assetPanel.select('.ap-table').select('tbody').node().rows.length;
		var tr = this.assetPanel.select('.ap-table').select("tbody").append("tr").attr("id", "ap-" + rowLength);

		// add text
		for (var j=0; j<keys.length; j++) {
			var hidden = fieldDefs.get(keys[j]).hidden;
			if (hidden !== 'true') {
				var td = tr.append("td");
				td.append("span").text(panelRecord.values[keys[j]]);
				control.addDragDropEventsToAssetPanelItem(td.node(), panelRecord.values);				
			}			
		}

		// add row actions
		for (var i=0; i<this.config.rowActions.length; i++) {
			var rowAction = this.config.rowActions[i];
			var button = tr.append("td").classed({'ap-buttoncell': true})
				.append("div").classed({'ap-button': true});
			
			if (this.config.rowActions[i]['title']) {
				button.attr("title", this.config.rowActions[i]['title']);				
			}
			
			if (this.config.rowActions[i]['icon']) {
				button.style("background-image", 'url(' + this.config.rowActions[i]['icon'] + ')');				
			}

			if (this.config.rowActions[i]['listener']) {
				var dataSourceName = this.config['dataSourceName'];
				button.node().addEventListener('click', rowAction['listener'].createDelegate(this, [panelRecord.values, dataSourceName]));
			}
		}
		
		return tr;
	},

	makeAssetPanelDraggable: function(){
		jQuery( ".ap-panel" ).draggable({'enable': true, 'cancel': '.tablescroll_wrapper'});
	},
	
	makeAssetPanelScrollable: function(){
		// override navigation toolbar's scroll events to allow scrolling within asset panel
    	var eventType = (navigator.userAgent.indexOf('Firefox') !=-1) ? "DOMMouseScroll" : "mousewheel";     	
    	this.assetPanel.node().addEventListener(eventType, function(e){
    		e.stopPropagation();
    	}, true);
	},
	
	makeAssetPanelCollapsible: function(){
	    var control = this;
	    var collapseButton = this.assetPanel.select('.collapse-button');
	    
	    if (!collapseButton.empty()) {
	    	collapseButton.node()
		    	.addEventListener("click", function(event) {
		    		d3.select(this)
		    			.classed({'minimize': control.minimized})
		    			.classed({'maximize': !control.minimized});	
					control.assetPanel.style("width", "auto");
					control.assetPanel.style("height", "auto");
					
					var display = (control.minimized === true) ? "" : "none";
					control.assetPanel.select('.tablescroll').style("display", display);					
		    		control.minimized = !control.minimized;
		    		control.syncAssetPanelDimensions();
				});	    	
	    }	
	},
		
	addDragDropEventsToAssetPanelItem: function(node, panelRecord){
		d3.select(node)
			.attr("draggable", "true")
			.attr("droppable", "true")
			.classed({'ap-item': true});
		
		var control = this;
		node.addEventListener("dragstart", function(event){
			event.stopPropagation(); 
			control.handleDragStartForPanelItem(event, node, control, panelRecord);
		})
		node.addEventListener("dragover", function(event){
			event.stopPropagation(); 
	    	control.allowDrop(event, node);
	    }) 
	    node.addEventListener("dragend", function(event){
	    	event.stopPropagation();
	    	event.preventDefault();
	    });
		
		// for dropping over self
	    node.addEventListener("drop", function(event){
	    	event.stopPropagation();
	    	event.preventDefault();
	    });
	},
	
	handleDragStartForPanelItem: function(ev, source, control, panelRecord) {     
		ev.stopPropagation();     // need        

	    // when dragging, display the icon and label text; use text in first column of asset table
	    var template = d3.select('#dragEffectTemplate').node();
	    var label = d3.select(template).select('.label');
	    var labelText = d3.select(ev.target.parentNode).select("td").text();
	    label.html(labelText);
	    
	    var dt = ev.dataTransfer; 
	    if (typeof dt.setDragImage === 'function') {		// only in supported browsers
	        dt.setDragImage(template, -10, 0); 
	    }
	    
	    dt.setData("text", source.parentNode.id + this.DELIMITER + labelText + this.DELIMITER + toJSON(panelRecord));		// in all browser
	},
	
	/**
	 * Allows item to be dropped.
	 */
	allowDrop: function (ev, source, control) {
		if (ev.preventDefault) {
	        ev.preventDefault();    // need
	    }  
	    
	    if (ev.stopPropagation) {
	        ev.stopPropagation();    // need
	    }
	},
	
	handleDragEndForPanelItem: function(ev, node, control, layer) {
		if (ev && ev.stopPropagation) {
	        ev.stopPropagation();    // need
	    }
		
		if (ev && ev.preventDefault) {
	        ev.preventDefault();    // need
	    }
		
	    var dtData = ev.dataTransfer.getData("text");  
	    var data = dtData.split(this.DELIMITER);
	    var sourceId = data[0];
	    var labelText = data[1];
	    var recordFrom = JSON.parse(data[2]);
	    	    
	    // drag and drop from asset panel to asset label layer
	    if (sourceId !== ev.target.id && !d3.select('.ap-panel #' + sourceId).empty() && !this.isAssetPanel(target)) {  	
	    	var target = ev.target;
	    	var targetSvg = target.ownerSVGElement;
	    	
	    	// create object to expose in callback function
	    	var newLocation = target.id.split(";");
	    	var record = {};
	    	record['from'] = recordFrom;
	    	
	    	var recordTo = {};
	    	/*
	    	recordTo[control.layer + '.bl_id'] = targetSvg.getAttribute("buildingId");
	    	recordTo[control.layer + '.fl_id'] = targetSvg.getAttribute("floorId");
	    	recordTo[control.layer + '.rm_id'] = newLocation[2];
	    	*/
	    	recordTo[layer + '.bl_id'] = targetSvg.getAttribute("buildingId");
	    	recordTo[layer + '.fl_id'] = targetSvg.getAttribute("floorId");
	    	recordTo[layer + '.rm_id'] = newLocation[2];
	    	record['to'] = recordTo;	
	    	
	    	//record['to'] = { 'rm.bl_id': targetSvg.getAttribute("buildingId"), 'rm.fl_id': targetSvg.getAttribute("floorId"), 'rm.rm_id': newLocation[2] };
	    	//record['to'] = { 'rm.bl_id': newLocation[0], 'rm.fl_id': newLocation[1], 'rm.rm_id': newLocation[2] };		// JFK A  vs JFK__A
	    	
	    	// remove row from asset panel
	    	var bDelete = this.config.onAssetLocationChange(record, this.config.assetType);	    	
	    	if (bDelete) {
	    		this.addFeedbackForTarget(target);
	    		
		    	var rowIndex = d3.select('.ap-panel #' + sourceId).node().rowIndex;	 
		    	delete d3.select('.ap-panel #' + sourceId).node().parentNode.deleteRow(rowIndex);
		    	
		    	var newLabel = this.addLabelToLayer(labelText, layer, target);
	        	newLabel.classed({'ap-panel-new-label' : true});
		    	this.addDragDropEventsToLayerItem(newLabel.node(), layer, {});
		    	this.addMouseOverEventToLayerItem(newLabel.node());
		    	this.addMouseOutEventToLayerItem(newLabel.node());
		    	this.syncAssetPanelDimensions();
	    	}	    			   
	    }	    	    
	    //alert("Source:" + sourceId + '(Asset Panel) \nTarget' + ev.target.id);
	},
	
    dockAssetPanel: function(bDock) {
    	var state = (bDock === true) ? 'disable' : 'enable';
    	jQuery('.ap-panel').draggable(state, {'cancel': '.tablescroll_wrapper'});
    },
	
	syncAssetPanelDimensions: function() {
		
		// workaround for tablescroll
		d3.select('.ap-headerrow').selectAll('td').style("width", null );
		d3.select('.ap-table').selectAll('td').style("width", null );
		if (d3.select('.ap-table thead').empty()) {			
			jQuery('.ap-table').prepend(document.createElement('thead'));
			d3.select('.ap-table thead').node().appendChild( d3.select('.ap-headerrow').node() );	
			d3.select('.tablescroll_head').remove();		
		}
		
		jQuery('.ap-table').tableScroll({height: this.height, width: this.width, flush: true, containerClass: 'tablescroll'});		
		d3.selectAll('.ap-panel').style("height", "auto");
	},
	
	addDragEffectTemplate: function() {
	    // add drag effect template to asset panel
	    var dragEffectTemplate = this.createDragEffectTemplate();        
	    d3.select("body").node().appendChild(dragEffectTemplate);
	},
			
	createDragEffectTemplate: function() {
		
	    // container div to hold the icon and label
	    var containerDiv = document.createElement("div");
	    containerDiv.id = "dragEffectTemplate";
	    containerDiv.style.position = "absolute";
	    containerDiv.style.left = 0;
	    containerDiv.style.top = 1000 + 'px';
	    containerDiv.style.zIndex = -999;
	    
	    // drag icon
	    var dragIcon = document.createElement('img');
	    // dragIcon.width = this.DRAG_ICON_WIDTH;		// if width and height specified, may not show first time in FF
	    // dragIcon.height = this.DRAG_ICON_HEIGHT;
	    dragIcon.src = this.dragIcon;
	    dragIcon.className = 'ap-dragicon';
	    dragIcon.style.float = 'left'; 

	    // label
	    var labelDiv = document.createElement("div");
	    labelDiv.style.float = 'left';
	    labelDiv.className = 'label';
	    
	    // add to container
	    containerDiv.appendChild(dragIcon);
	    containerDiv.appendChild(labelDiv);
	    
	    return containerDiv;
	},
	
	/**
	 * Protected. Allows rooms (or room-like assets) to be droppable/targets.  (rm-assets layer)
	 */
	addDragDropEventsToRooms: function(layer, drawingName){
		var control = this;  
		var svg = this.getSvgByDrawingName(drawingName);
		
		var rooms = svg.select("#" + layer + this.ASSETS_SUFFIX).selectAll('path');
		
		// restrict droppable locations to obey highlight
		if (control.config['restrictToHighlights'] === true) {
			rooms = rooms.filter(function(d, i){ return  (d3.select(this).attr("highlighted") === 'true')});			
		}

		rooms.each(function(d,i) {
		        d3.select(this).attr("droppable", true);
		        d3.select(this).classed({"no-selection" : true});        
		        var rmasset = d3.select(this).node();
		        rmasset.addEventListener("dragenter", function(event){
		            control.allowDrop(event);
		        }) 
		        rmasset.addEventListener("dragover", function(event){
		        	control.allowDrop(event);
		        })  
		        rmasset.addEventListener("drop", function(event, node){
		        	event.stopPropagation();
		        	event.preventDefault();
		            control.handleDragEndForPanelItem(event, node, control, layer);
		        });
		        
		        // safari
		        d3.select(rmasset).style("-khtml-user-drag", "element");
			});
	},
		
    /**
     * Protected. Allows asset labels to be draggable/source.  (xx-labels layer)
     * layerName  String. Name of the draggable layer.
     */
	addDragDropEventsToLayer: function(layer, drawingName) {    
		// i.e. rm-labels
		var layerName = layer + this.LABELS_SUFFIX;
		var svg = this.getSvgByDrawingName(drawingName);
        svg.attr("targetLayer", layer);				//  set target layer on a per drawing basis
		
        var roomLayer = svg.select('#' + layerName);
        var gElements = roomLayer.selectAll("g");
                
        var control = this;
        gElements.each(function() {
        	var textElements = d3.select(this).selectAll('text');
        	textElements.each(function(d, i) {
        		// first label is the rm_id
        		if (i > 0) {
        			var parameters = {};
                	control.addDragDropEventsToLayerItem(this, layer, parameters);
                	control.addMouseOverEventToLayerItem(this);
                	control.addMouseOutEventToLayerItem(this);        			
        		} else {
        			d3.select(this).style("pointer-events", "none");
        		}
        	});
        });
    },
    
    addDragDropEventsToLayerItem: function(node, layer, parameters){  
    	var control = this;
        d3.select(node).call(control.dragDropHandlerForLayerItem(node, control, layer, parameters)) ;
    },
    
    dragDropHandlerForLayerItem: function(node, control, layer, parameters) {
    	
		// not a datasource record, just an object composed from drawing layer, rm-label g
    	var rmLabelG = d3.select(node.parentNode);
    	var fromLocation = ['','',''];
    	if (!rmLabelG.empty()) {
        	fromLocation = rmLabelG.attr("id").replace(this.layerLabelPrefix, ''); 
        	fromLocation = fromLocation.split(";");
        	
        	// to handle JFK A instead of JFK___A
    		var sourceSvg = rmLabelG.node().ownerSVGElement;
        	fromLocation[0] = sourceSvg.getAttribute("buildingId");
        	fromLocation[1] = sourceSvg.getAttribute("floorId");
    	}
    	
    	return d3.behavior.drag()
		    .on("dragstart", function(d, i){
		    	d3.event.sourceEvent.stopPropagation();        	
		    	control.enablePointerEvents(node, false);		    	
		    	control.handleDragStartForLayerItem(node, control, layer, parameters);
		    })
		    .on("drag", function(d, i) {
		        d3.event.sourceEvent.stopPropagation();		        
		        control.handleDragForLayerItem(node, control, layer, parameters);
		    })
		    .on("dragend", function(d, i) {
		    	d3.event.sourceEvent.stopPropagation();		    	
		    	control.handleDragEndForLayerItem(node, control, layer, parameters, fromLocation);
		    	control.enablePointerEvents(node, true);
		    })
    },    
        
    handleDragStartForLayerItem: function(textNode, control, layer, parameters) {
        var target = d3.event.sourceEvent.target;

        if (textNode && target) {      	
        	var g = textNode.parentNode;		// room
            var clone = textNode.cloneNode(true);
            var tL = d3.transform(d3.select(g).attr("transform"));
            parameters.originalLabelX = tL.translate[0];
            parameters.originalLabelY = tL.translate[1]; 
      	  	point = [textNode.getBoundingClientRect().left, textNode.getBoundingClientRect().top];
	      	parameters.originalDragCloneLeft = point[0];   
	      	parameters.originalDragCloneTop = point[1];
	      	
            this.removeDraggingClone();  
                        
	      	// hide label text node
	      	d3.select(textNode).classed({"ap-dragsource": "true"}).style("display", "none");
	      	
	      	var dragging = d3.select("body").append("svg")
	      	    .classed({'ap-dragclone' : true})
	      	  	.style("position", "absolute")
	      	  	.style("pointer-events", "none")
	      	  	.style("left", point[0] + 'px')
	      	  	.style("top", point[1] + 'px')
	      	  	.style("z-index", 1000);
	      	
	      	dragging.node().appendChild(clone);
	      	
	      	// <image xlink:href="pion.png" x="0" y="0" height="15px" width="15px"/>
	      	dragging
	      		.append("image")
	      		.attr("xlink:xlink:href", control.dragIcon)
	      		.attr("x", 0)
	      		.attr("y", 0)
	      		.attr("height", control.DRAG_ICON_HEIGHT + 'px')
	      		.attr("width", control.DRAG_ICON_WIDTH + 'px');	      	
	      	
	      	// clear any transforms in g
	      	dragging.select('g').attr("transform", null);
	      	
	      	// text	      	
	      	dragging.select('text')
	      		.attr("font-size", control.fontSize)
	      		.attr("x", control.DRAG_ICON_HEIGHT)
	      		.attr("y", control.fontSize);    	  	
        }
    },
    
    handleDragForLayerItem: function(textNode, control, layer, parameters){  
    	try{    	
        	if (textNode && d3.event.sourceEvent) {               
                var target = d3.event.sourceEvent.target;               
                parameters.labelNode = textNode;

                if ((this.isDroppable(target)  && this.isRoom(target)) || this.isAssetPanel(target)) { 
                	d3.select("body").classed({'ap-notallowed': false});           	                     	
                } else {       	
                	d3.select("body").classed({'ap-notallowed': true}); 
                }
               
                var bodyNode = d3.select("body").node();
                var point = (d3.touch(bodyNode)) ? d3.touch(bodyNode) : d3.mouse(bodyNode);
                var dragging = d3.select('.ap-dragclone');
                dragging
                	.style("left", point[0] + 'px')
                	.style("top", (Number(point[1]) + 5) + 'px');          
        	}     		
    	} catch (e) { 
    		console.log(e);
    	}	      
    },

    handleDragEndForLayerItem: function(textNode, control, layer, parameters, location) {
    	var target = d3.event.sourceEvent.target;
    	var targetSvg = d3.select(target.ownerSVGElement);
		var targetLayer = (!targetSvg.empty()) ? targetSvg.attr("targetLayer") : '';
		
    	if ((this.isDroppable(target)  && this.isRoom(target)) || this.isAssetPanel(target)) { 
        	var labelText = d3.select(textNode).text();
        	
        	// if dropped onto floorplan
        	//if (target.nodeName === 'path' && target.parentNode.id == 'rm-assets') {
        	//if (target.nodeName === 'path' && target.parentNode.id == this.layer + this.ASSETS_SUFFIX) {
        	if (target.nodeName === 'path' && target.parentNode.id == targetLayer + this.ASSETS_SUFFIX) {
            	var newLocation = d3.select(target).attr("id").split(";");
            	
        		// not a datasource record, just an object composed from the drawing target
            	var recordFrom = {};
        		recordFrom['em.em_id'] = labelText;
        		/*
        		recordFrom[control.layer + '.bl_id'] = location[0];
        		recordFrom[control.layer + '.fl_id'] = location[1];
        		recordFrom[control.layer + '.rm_id'] = location[2];
        		*/
        		recordFrom[layer + '.bl_id'] = location[0];
        		recordFrom[layer + '.fl_id'] = location[1];
        		recordFrom[layer + '.rm_id'] = location[2];
        		
        		var recordTo = {};
        		/*
        		recordTo[control.layer + '.bl_id'] = targetSvg.attr("buildingId");
        		recordTo[control.layer + '.fl_id'] = targetSvg.attr("floorId");
        		recordTo[control.layer + '.rm_id'] = newLocation[2];
        		*/
        		recordTo[targetLayer + '.bl_id'] = targetSvg.attr("buildingId");
        		recordTo[targetLayer + '.fl_id'] = targetSvg.attr("floorId");
        		recordTo[targetLayer + '.rm_id'] = newLocation[2];
        		
        		var record = {};
        		record['from'] = recordFrom;
        		record['to'] = recordTo;

        		//record['from'] = { 'em.em_id': labelText, 'rm.bl_id': location[0], 'rm.fl_id': location[1], 'rm.rm_id': location[2] };
        		//record['to'] = { 'rm.bl_id': targetSvg.attr("buildingId"), 'rm.fl_id': targetSvg.attr("floorId"), 'rm.rm_id': newLocation[2] };
        		//record['to'] = { 'rm.bl_id': newLocation[0], 'rm.fl_id': newLocation[1], 'rm.rm_id': newLocation[2] };
        		
        		var bDelete = this.config.onAssetLocationChange(record, this.config.assetType);	    	
    	    	if (bDelete) {
    	    		this.removeDraggingClone();
    	    		
    	    		// delete label from drawing layer
    	    		d3.select(textNode).remove();
    	    		this.addFeedbackForTarget(target);
            		//var newLabelText = control.addLabelToLayer(labelText, layer, target);
    	    		//this.addDragDropEventsToLayerItem(newLabelText.node(), layer, parameters);

    	    		var newLabelText = control.addLabelToLayer(labelText, targetLayer, target);
    	    		newLabelText.classed({'ap-layer-new-label': true});
                	this.addDragDropEventsToLayerItem(newLabelText.node(), targetLayer, parameters);
                	this.addMouseOverEventToLayerItem(newLabelText.node());
                	this.addMouseOutEventToLayerItem(newLabelText.node());                  	  
    	    	} else {
    	    		d3.select(textNode).classed({'ap-dragsource': false});
    	    		control.returnToOriginalPosition(d3.select('.ap-dragclone').node(), parameters.originalDragCloneLeft, parameters.originalDragCloneTop, textNode);
    	    	}          	
    	    	this.removeDragRemnantsFromLayer();
            	//alert("Source: " + assetLabelId + "\nTarget: " + targetId);  
            	
            // if dropped onto asset panel	
        	} else {        		
           	
        		// not a datasource record, just an object composed from the drawing target        		        		
        		var recordFrom = {};
        		recordFrom['em.em_id'] = labelText;
        		/*
        		recordFrom[control.layer + '.bl_id'] = location[0];
        		recordFrom[control.layer + '.fl_id'] = location[1];
        		recordFrom[control.layer + '.rm_id'] = location[2];
        		*/
        		recordFrom[layer + '.bl_id'] = location[0];
        		recordFrom[layer + '.fl_id'] = location[1];
        		recordFrom[layer + '.rm_id'] = location[2];
        		// record['from'] = { 'em.em_id': labelText, 'rm.bl_id': location[0], 'rm.fl_id': location[1], 'rm.rm_id': location[2] };
        		
        		var record = {};
        		record['from'] = recordFrom;
        		record['to'] = null;
        		
        		var bDelete = this.config.onAssetLocationChange(record, this.config.assetType);	    	
    	    	if (bDelete) {
    	    		this.removeDraggingClone(); 
    	    		
    	    		// delete label from drawing layer
    	    		d3.select(textNode).remove();  
    	    		
            		this.refreshAssetPanel(null);
            		this.showAssetPanel(null);
    	    	} else {
    	    		d3.select(textNode).classed({'ap-dragsource': false});
    	    		control.returnToOriginalPosition(d3.select('.ap-dragclone').node(), parameters.originalDragCloneLeft, parameters.originalDragCloneTop, textNode);
    	    	}
    	    	        	         	          	    
        		//alert("Source: " + assetLabelId + "\nTarget: " + d3.select(target.parentNode).attr("id"));
        	}
        } else {       	
        	// not allowed
        	control.returnToOriginalPosition(d3.select('.ap-dragclone').node(), parameters.originalDragCloneLeft, parameters.originalDragCloneTop, parameters.labelNode);
        	d3.select(textNode).classed({'ap-dragsource': false});
    		d3.select(textNode).style("display", null);
        }        
        d3.select("body").classed({'ap-notallowed': false});       
    },	
        
	addMouseOverEventToLayerItem: function(node) {
		node.addEventListener('mouseover', function(event){
			event.stopPropagation();
			
			// only highlight if not in drag mode
            var remnant = d3.selectAll('.ap-dragclone');           
            if (remnant.empty()) {
			  d3.select(this).classed({'ap-highlighted-label': true});	
            }
		});
	},

	addMouseOutEventToLayerItem: function(node) {
		node.addEventListener('mouseout', function(event){
			event.stopPropagation();
			d3.select(this).classed({'ap-highlighted-label': false});
		});
	},	
	
	addFeedbackForTarget: function(target){
		d3.select(target).classed({"ap-highlighted-target": true})
	},
	   
    isDroppable: function(el) {
    	var target = d3.select(el);
    	if (target.empty() || el.nodeName === '#document' || el.nodeName === 'HTML' || el.nodeName === 'DOCUMENT') {
    		return false;
    	}
    	//return (target.empty() || target.attr('droppable') !== null || target.attr('droppable') === 'true');
    	return (target.empty() || target.attr('droppable') !== null || (target.attr('droppable') === 'true' && target.attr("droppable") !== 'false'));
    },
    
    isAssetPanel: function(el) {  	
    	var target = d3.select(el);
    	var className = '';  
    	if (!target.empty()) {
    		className = target.attr("class");
    	}
    	return (className != null && /ap-item|ap-titlebar|ap-headerrow|ap-headercell|tablescroll_wrapper|tablescroll/.test(className));
    },

    isRoom: function(el) {
    	//return ((el) && el.nodeName === 'path' && (el.parentNode) && el.parentNode.id === 'rm-assets');
    	//return ((el) && el.nodeName === 'path' && (el.parentNode) && el.parentNode.id === this.layer + this.ASSETS_SUFFIX);
    	
    	var targetSvg = d3.select(el.ownerSVGElement);
    	var targetLayer = (!targetSvg.empty()) ? targetSvg.attr("targetLayer") : '';
    	return ((el) && el.nodeName === 'path' && (el.parentNode) && el.parentNode.id === targetLayer + this.ASSETS_SUFFIX);
    },
	
	/**
	 * Add label to layer.
	 */
	addLabelToLayer: function(labelText, layer, target) {
		var control = this;		
		var targetId = d3.select(target).attr("id");		//  g node rm, on the rm-assets/rm_trial-assets layer
		
		//var rmLabelContainer = document.getElementById(this.layerLabelPrefix + targetId);   // rm-labels/rm_trial-assets layer
		var layerLabelPrefix = this.LABEL_PREFIX + layer + '-';
		var rmLabelContainer = document.getElementById(layerLabelPrefix + targetId);   // rm-labels/rm_trial-assets layer
		
		var labels = d3.select(rmLabelContainer).selectAll("text")
		.filter(function(d, i){ 
			return  (d3.select(this).text() !== '' || (d3.select(this).attr("class") && d3.select(this).attr("class").indexOf('ap-dragsource') == -1) );
		});
		
		// update positions for all labels currently inside target room
		var lastLabel = null;
		var startingY = (labels[0].length <= 2) ? -0.5 : -Math.floor(labels[0].length/2);
		labels.each(function(d,i){
			var label = d3.select(this);
			label.attr("y",  (startingY + i) + "em");
    		lastLabel = label;
    	});
    	
		// add new label to the end; calculate position based on other labels
    	var newLabel = [];
    	if (labelText) {
        	newLabel = d3.select(rmLabelContainer).append("text").attr("y", (startingY + labels[0].length) + "em").text(labelText);  
        	if (lastLabel) {
        		newLabel.attr("font-size", lastLabel.attr("font-size"));
        	}
    	}
    	
    	return newLabel;
	},
    	
    /**
     * Fly back to original position. 
     */
	returnToOriginalPosition: function (dragNode, toLeft, toTop, labelNode) {			
		var control = this;
		d3.select(dragNode)
			.transition()
			.style("left", toLeft + "px").style("top", toTop + "px")
	        .duration(200)
	        .ease("linear")
	        .each("end", function(){
	        	d3.select(labelNode).style("display", "");
	        	control.removeDraggingClone();              
	        });
	},
    
    enablePointerEvents: function(elem, bEnable) {
    	var state = (bEnable === false) ? 'none' : 'all';
    	d3.select(elem).style({"pointer-events": state});  
    },
    
    removeDraggingClone: function(){
        // remove dragging clone
        var dragging = d3.select('.ap-dragclone');
        if (!dragging.empty()) {
            dragging.remove();
        }    	
    },
    
    removeDragRemnantsFromLayer: function(){
    	d3.selectAll('.ap-dragsource').remove();
    },
		   
	getSvgByDrawingName: function(drawingName) {
		return this.svgs[drawingName];
	},
	
	init: function(svg, drawingName) {
		this.svgs[drawingName] = svg;
		
		/*
		var svgId = svg.node().id;
		this.svgs[svgId] = svg;
		
		// get drawingName from svg id
		var drawingName = svgId.replace(this.config.divId + '-', '');
		if (drawingName.substring(drawingName.length-4, drawingName.length) == '-svg') {
			drawingName = drawingName.substring(0, drawingName.length-4);
		}
		
		// if using dataSource selector, only allow 'labelEmployeesDs'; otherwise, no restriction
		var datasourceSelectorAddOn = this.drawingController.getAddOn('DatasourceSelector');
		if (this.drawingController.config.labelSelector === false || (this.drawingController.config.labelSelector === true && datasourceSelectorAddOn && this.drawingController.config.highlightParameters[0].label_ds === 'labelEmployeesDs')) {
	    	this.addDragDropEventsToLayer(this.layer, drawingName);   // rm --> rm-labels, rm		
		}
    	this.addDragDropEventsToRooms(this.layer, drawingName);
    	*/
	}
}, {});