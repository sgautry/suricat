/**
 * HTML Drawing Control component for Desktop.
 * 
 * @author Ying Qin
 */
Drawing.DrawingControl = Ab.view.Component.extend({

    version: '2.0.0',

    /**
     * root div for SVG drawings
     */
    divId: '',

    /**
     * config object
     */
    config: {},
      
    /**
     * controller to access all controllers and add-ons for SVG drawings
     */
	drawingController: null,
	
    // @begin_translatable
    Z_FAILED_TO_LOAD_MESSAGE: 'Fail to load required svg drawing:',
    // @end_translatable

    /**
     * Constructor.
     * 
     * Initialize default configuration parameters, construct and initialize the DrawingController object, add panel's default events.
     * 
     * @param divId String Id of root <div/> that holds all svg drawings.
     * @param panelId String Id of the panel
     * 
	 * @param config configObject
     */
    constructor: function(divId, panelId, config) {
        
    	this.inherit(divId, 'html', config);
        
    	this.divId = divId;
        this.panelId = panelId;
        this.config = config;
        
        this.config.assetTypes		= config.getConfigParameter('assetTypes', '');
		this.config.selectionMode	= config.getConfigParameter('selectionMode', '2');
		this.config.assignMode		= config.getConfigParameter('assignMode', '0');
		this.config.zoomWindowEnabled = config.getConfigParameter('zoomWindowEnabled', 'true');
		this.config.multipleSelectionEnabled = config.getConfigParameter('multipleSelectionEnabled', 'true');
		
		// assets layers to place on top so that event will fire on them (not blocked by bigger assets, i.e. rm).
		this.config.topLayers = config.getConfigParameter('topLayers', 'jk;fp');
    	
		// show asset tooltips? hide by default.
		this.config.showTooltip = config.getConfigParameter('showTooltip', false);

		//allows multiple drawings to display within a panel
		this.config.allowMultipleDrawings = config.getConfigParameter('allowMultipleDrawings', 'false');
		
		this.config.events = config.getConfigParameter('events', []);
		
		// add on config for the add-on components
		this.config.addOnsConfig = config.getConfigParameter('addOnsConfig', {});

		this.layoutRegion = config.getConfigParameter('layoutRegion', this.getLayoutValueFromPanel('layoutRegion'));
        this.layout = config.getConfigParameter('layout', this.getLayoutValueFromPanel('layout'));
        this.region = config.getConfigParameter('region', this.getLayoutValueFromPanel('region'));

		//initialize the drawing controller object
		this.drawingController = new DrawingController(this.divId, this.config);
		
		//initialize all the controllers
		this.drawingController.init();
		
		// initialize all the add-ons
		this.drawingController.initAddOn();
		
        // add the resize event listener, if not defined
       	this.addDrawingPanelEventHandlers();
       	
       	//place actionbar before drawing div.
       	this.swapActionBar();
    },
	
    /**
     * Retrieves the drawing SVG from server and displays it. Sets add-ons parameters. Updates Navigation toolbar and resizes the drawing to fit panel size.
     * 
     * @param parameters   Object server-side required info such as as bl_id, fl_id, viewName, highlight
     *                     and label dataSource names to get and process corresponding SVG
     * @return DrawingBase reference to the drawing loaded
     */
    load: function(parameters) {
    	
    	// drawing name is required
        var drawingName = parameters['drawingName'];
        if(!drawingName)
        	return;

        var spinner = new Spinner().spin(document.getElementById(this.divId));
	     
        // retrieves highlight parameters for asset and label highlights
        if(this.config.highlightParameters && (typeof parameters['highlightParameters'] === 'undefined' || parameters['highlightParameters'] === null)){
        	parameters['highlightParameters'] = this.config.highlightParameters;
        }
        
        var assetTooltip = this.drawingController.getAddOn("AssetTooltip");
    	if(assetTooltip && parameters['showTooltip']){
    		assetTooltip.setShowTooltip(DrawingCommon.toBoolean(parameters['showTooltip']));
    	}
    			
    	var drawingBase = this.drawingController.getControl().load(parameters);
    	
        // compatibility with IE
        if (Ext.isIE) {
            this.getDiv(drawingName).node().style.overflow = 'hidden';
        }
        
        // show or hide nav toolbar
        this.drawingController.updateNavigationToolbar();
		 
        //kb# 3053571 - trigger resize event for IE10
        //multiple drawings - also need to trigger resize event.
        var drawingPanel = View.panels.get(this.panelId);
        drawingPanel.afterResize();
        
        spinner.stop();
        
        return drawingBase;
    },

    /**
     * Removes the specified SVG drawing from layout. Updates the navigation toolbar and resizes the drawing to fit panel size.
     * 
     * @param parameters    Object required info such as as bl_id, fl_id, drawing name to locate the corresponding SVG
     */
    unload: function(parameters){
    	
    	// locate and remove SVG drawing
    	this.drawingController.getControl().unload(parameters);
    	    	
        // show or hide navigation toolbar
        this.drawingController.updateNavigationToolbar();
        
        //trigger resize event.
        var drawingPanel = View.panels.get(this.panelId);
        drawingPanel.afterResize();

    },
    
    /**
     * Saves SVG drawing's extent, drawing name, action item id to afm_redlines table.
     * Save redlines/markups and image content (if any) to its document table and update the related file name in afm_redlines table.
     * Capture the floor plan with redlines/markups and highlight, save to activity_log.doc4 field. 
     * 
     * @param parameters parameters needed for saving the drawing, such as activity log id etc.
     */
    save: function(parameters){
    	
		var viewBox = this.getViewBox(this.divId);

		// no svg loaded or no activity log id provided.
    	if(!viewBox)
    		return;

    	View.openProgressBar(View.getLocalizedString(this.z_PROGRESS_MESSAGE));  

    	var me = this;
    	var saveChange = function(){
    	
    		parameters.viewBox = viewBox;
    		
    		me.drawingController.getController("MarkupController").save(parameters);
    		
        	View.closeProgressBar();
    	};
    	
    	saveChange.defer(100);
    },
      

    /**
     * Gets the drawing controller.
     * 
     * @return DrawingController
     */
    getDrawingController: function(){
    	return this.drawingController;
    },
    
    /**
     * Returns the viewBox based on <div/> id and drawing name
     * 
     * @param divId root div id
     * @param drawingName the drawing name
     * 
     * @return viewBox Array
     */
    getViewBox: function (divId, drawingName) {
    	return this.drawingController.getControl().getViewBox(divId, drawingName);
    },
    
    /**
     * Retrieves DrawingBase object of the specified drawing name from the list of drawings in layout.
     *  
     * @param drawingName String the specified drawing name
     * @return DrawingBase
     */
    getSvgControl: function(drawingName) {
        return this.drawingController.getControl().getDrawingByName(drawingName);
    },

    /**
     * Retrieves <div/> selection of the specified drawing name.
     * 
     * @param drawingName String the specified drawing name
     * @return object HTMLDivElement
     */
    getDiv: function(drawingName) {
        return this.getSvgControl(drawingName).getDiv();
    },

    
    /**
     * Retrieves the id of <div/> selection of the specified drawing.
     * 
     * @param drawingName String the specified drawing name
     * @return id String
     */
    getDivId: function (drawingName) {
        return this.getDiv(drawingName).id;
    },
    
    /**
     * Retrieves <svg/> selection of the specified drawing name.
     * 
     * @param drawingName String the specified drawing name
     * @return object SVGSVGElement
     */
    getSvg: function(drawingName) {
        return this.getSvgControl(drawingName).getSvg();
    },
    
    /**
     * Retrieves the controller by the specified controller id.
     * 
     * @param controllerId the controller id 
     * @return controller the controller object.
     */
    getController: function(controllerId){
    	return this.drawingController.getController(controllerId);
    },
   
    /**
     * Retrieves the add-on by the specified id.
     * 
     * @params addOnId the specified add-on id.
     * @return addon the add-on object
     */
    getAddOn: function(addOnId){
    	return this.drawingController.getAddOn(addOnId);
    },
    
    /**
     * Removes the specified add-on from add-ons list.
     * 
     * @params addOnId the specified add-on id.
     */
    removeAddOn: function(addOnId){
    	this.drawingController.removeAddOn(addOnId);
    },
    
    
    /**
     * Load an image into div.
     * 
     * @param href String URL for the image.
     */
    loadImage: function(href, drawingName) {
        this.getSvgControl().loadImage(this.getDiv(drawingName), drawingName, href);
    },

    /**
	 * Gets drawing's Image Bytes
	 * 
	 */
    getImageBytes: function(drawingName, callback){
		return  this.getSvgControl(drawingName).getImageBytes(callback);
	},
	
	/**
	 * Disables label selection for the specified drawing name.
	 * 
	 * @param drawingName String the specified drawing name
	 */
	disableLabelSelection: function(drawingName){
    	this.getSvgControl(drawingName).disableLabelSelection(this.getSvg(drawingName));
	},
	
    /**
     * Retrieves the layout or region value from panel
     */
    getLayoutValueFromPanel: function(layoutOrRegion){
		var panel = View.panels.get(this.panelId);
		if(panel){
			return panel[layoutOrRegion];
		} else {
			return '';
		}
    },
	
    
    /**
     * Adds default event handlers to panel, such as afterResize, syncHeight, afterLayout.
     */
    addDrawingPanelEventHandlers: function(){

		var drawingPanel = View.panels.get(this.panelId);
   		var drawingControl = this;

   		drawingPanel.afterResize = function(){
	   		var drawingPanel = View.panels.get(drawingControl.panelId);
	   		var drawingDiv = Ext.get(drawingControl.divId);

	   		var height = drawingPanel.determineHeight();

	   		// make adjustments for tabs and instructions
		   	var adjHeight = 0;
	   		if (this.singleVisibleTabPanel()){
	   			adjHeight = adjHeight + drawingPanel.getTitlebarHeight(); 
	   		}
    		adjHeight = adjHeight + drawingPanel.getInstructionsHeight() + drawingPanel.getActionbarHeight() + drawingPanel.getOtherControlsHeight() + 4; 

			height = height - adjHeight;
            drawingDiv.setHeight(height);
            drawingDiv.parent().setHeight(height);  
            
            //sync height for layers popup.
			var layersPopup = drawingControl.drawingController.getAddOn('LayersPopup');
	        if(layersPopup)
	        	layersPopup.afterResize();
		};

   		drawingPanel.syncHeight = function(){
   			drawingPanel.afterResize();
   		};

   		drawingPanel.afterLayout = function() {
        	var regionPanel = this.getLayoutRegionPanel();
   			if (regionPanel){
   				if(!drawingPanel.resizeListenerAttached){
   					drawingPanel.resizeListenerAttached = true;

   					regionPanel.addListener('resize', function(){
   						drawingPanel.afterResize();
   					});
   					regionPanel.addListener('expand', function() {
		                drawingPanel.afterResize();
		            });
   				}
   			}
   		};

   		drawingPanel.isScrollInLayout = function() {
   			return false;
   		};

   		/**
   	     * Returns the height of the other controls such as forms etc. The other controls need to be in a <div> with id of [panelId]_controls.
   	     */
   		drawingPanel.getOtherControlsHeight = function() {
   	        var height = 0;

   	        var controlsEl = Ext.get(drawingControl.panelId + '_controls');
   	        if (controlsEl) {
   	            height = controlsEl.getHeight();
   	        }

   	        return height;
   	    };
   	    
   		drawingPanel.afterLayout();
	},

	/**
	 * Place actionbar before drawingDiv.
	 */
    swapActionBar: function(){
    	var drawingPanel = View.panels.get(this.panelId);
    	if(drawingPanel.actionbar){
    		var actionBarNode = Ext.get(drawingPanel.actionbar.toolbar.id).dom;
    		var drawingDivNode = Ext.get(this.divId).dom;
    		drawingDivNode.parentNode.insertBefore(actionBarNode, drawingDivNode);
    	};
    }
});

