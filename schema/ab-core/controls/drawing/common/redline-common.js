/**
 * Common API used by drawing control
 */

var RedlineCommon = {};

RedlineCommon.getSVGTranslate = function(g){
	var transforms = g.node().transform.baseVal;

	var t = null;
	var bFound = false;
	for(var index = 0; index < transforms.numberOfItems; index++){
		t = transforms.getItem(index);
		if (t.type === SVGTransform.SVG_TRANSFORM_TRANSLATE) {
			bFound = true;
			break;
		}
	}

	if(!bFound){
		t = g.node().ownerSVGElement.createSVGTransform();
		t.setTranslate(0, 0);
		transforms.appendItem(t);
	}
	return t;
}

RedlineCommon.setScale = function(g, scale){
	var transforms = g.node().transform.baseVal,
				t = transforms.getItem(0);
	if (t.type === SVGTransform.SVG_TRANSFORM_SCALE) {
		t.setScale(scale, scale);
	} else {
		t = g.node().ownerSVGElement.createSVGTransform();
		t.setScale(scale, scale);
		transforms.insertItemBefore(t, 0);
	}
}

RedlineCommon.getSVGAngle = function(g){
	var transforms = g.node().transform.baseVal;

	var t = null;
	for(var index = 0; index < transforms.numberOfItems; index++){
		t = transforms.getItem(index);
		if (t.type === SVGTransform.SVG_TRANSFORM_ROTATE) {
			return t.angle;
		}
	}

	return 0;
}

RedlineCommon.getUnits = function(svg){
	if(!svg || svg.empty())
		return 'inches';
	
	var defaultView = svg.select("#defaultView");
	var units = 'inches';
	if (defaultView[0][0] !== null) {
		units = defaultView.attr("units");
	}
	
	return units;
}

RedlineCommon.getUnitsFactor = function(svg){
	if(!svg || svg.empty())
		return 1;
	
	var units = RedlineCommon.getUnits(svg);
	var unitsFactor = 1;

	switch(units) {
    case "mm":
    	unitsFactor = 500/39.3701;
        break;
    case "cm":
    	unitsFactor = 50/39.3701;
    	break;
    case "Metres":
    	unitsFactor = 0.5/39.3701;
    	break;
    case "m":
    	unitsFactor = 0.5/39.3701;
    	break;
    default:
    	unitsFactor = 1;
	}

	return unitsFactor;
}



