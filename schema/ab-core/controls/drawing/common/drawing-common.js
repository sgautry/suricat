/**
 * Common API used by drawing control
 */

var DrawingCommon = {};

/**
 * Retrieves valid node id.  
 * @param nodeId String
 */
DrawingCommon.retrieveValidNodeId = function(nodeId) {
	return nodeId.replace(/'/g, "___").replace(/ /g, "__");
}

/**
 * Retreives svg's id
 * @returns
 */
 
 // WC-2604 changes
DrawingCommon.retrieveValidSvgId = function(divId, drawingName){
	return DrawingCommon.d3SelectEscape(DrawingCommon.retrieveValidNodeId(divId + "-" + drawingName)) + "-svg";
}

DrawingCommon.d3SelectEscape = function(selectedID){	
	var rcssescape = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
	fcssescape = function( ch, asCodePoint ) {
		if ( asCodePoint ) {

			// U+0000 NULL becomes U+FFFD REPLACEMENT CHARACTER
			if ( ch === "\0" ) {
				return "\uFFFD";
			}

			// Control characters and (dependent upon position) numbers get escaped as code points
			return ch.slice( 0, -1 ) + "\\" + ch.charCodeAt( ch.length - 1 ).toString( 16 ) + " ";
		}

		// Other potentially-special ASCII characters get backslash-escaped
		return "\\" + ch;
	};
	return (selectedID + "").replace( rcssescape, fcssescape );
}
 // WC-2604 end changes
/**
 * detect I
and returns version of IE or false, if browser is not Internet Explorer
 */
DrawingCommon.isIE = function() {
  var ua = window.navigator.userAgent;

  // Test values; Uncomment to check result …

  // IE 10
  // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';
  
  // IE 11
  // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';
  
  // Edge 12 (Spartan)
  // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';
  
  // Edge 13
  // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

  var msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    // IE 10 or older => return version number
    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
  }

  var trident = ua.indexOf('Trident/');
  if (trident > 0) {
    // IE 11 => return version number
    var rv = ua.indexOf('rv:');
    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
  }

  var edge = ua.indexOf('Edge/');
  if (edge > 0) {
    // Edge (IE 12+) => return version number
    return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
  }

  // other browser
  return false;
}

/**
 * Detect is modern browser
 * @returns {boolean}
 */
DrawingCommon.isModernBrowser = function() {
    return (DrawingCommon.isIE () > 9 || !DrawingCommon.isIE());
}

/**
 * add commas to number
 */
DrawingCommon.addDecimalAndGroupingSeparator = function(nStr){
	nStr += '';
	x = nStr.split(strDecimalSeparator);
	x1 = x[0];
	x2 = x.length > 1 ? strDecimalSeparator + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + strGroupingSeparator + '$2');
	}
	return x1 + x2;
}
/**
 * To boolean.
 */
DrawingCommon.toBoolean = function(value){
	if(value === undefined || value === null){
		return false;
	}
	if(_.isString(value)){
		switch(value.toLowerCase()){
			case "true": case "yes": case "1": return true;
			case "false": case "no": case "0": return false;
			default: return Boolean(value);
		}
	}else{
		return value;
	}
	
}

DrawingCommon.getUnits = function(svg){
	if(!svg || svg.empty())
		return 'inches';
	
	var defaultView = svg.select("#defaultView");
	var units = 'inches';
	if (defaultView[0][0] !== null) {
		units = defaultView.attr("units");
	}
	
	return units;
}

DrawingCommon.getUnitsFactor = function(svg){
	if(!svg || svg.empty())
		return 1;
	
	var units = DrawingCommon.getUnits(svg);
	var unitsFactor = 1;

	switch(units) {
    case "mm":
    	unitsFactor = 500/39.3701;
        break;
    case "cm":
    	unitsFactor = 50/39.3701;
    	break;
    case "Metres":
    	unitsFactor = 0.5/39.3701;
    	break;
    case "m":
    	unitsFactor = 0.5/39.3701;
    	break;
    default:
    	unitsFactor = 1;
	}

	return unitsFactor;
}
