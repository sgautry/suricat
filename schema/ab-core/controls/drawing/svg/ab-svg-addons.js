/*
 * Main control for add-on components.
 * 
 * Supported add-on components are:
 * 		Cluster
 * 		Marker
 */
AddOnsController = Base.extend({

	/**
	 * Add-on config, varies based on the add-on type.
	 */
	config: {},
	
	/**
	 * A map of ids and their corresponding add-ons.
	 */
	registeredAddOns: {},	
	
	/**
	 * Constructor. Initializes the drawing controller and registered add-ons map.
	 */
	constructor:  function(config){
		this.config = config;
		
		this.registeredAddOns = {};
	},
	
	/**
	 * Retrieves add-on for the specified add-on id
	 * 
	 * @param addOnId String the add-on id
	 */
	getAddOn: function(addOnId){
		return this.registeredAddOns[addOnId];
	},
	
	setAddOns: function(addOnsConfig){
		
		for (var key in addOnsConfig) {
			// layer popup will be registered by SVG
			if (addOnsConfig.hasOwnProperty(key)){
			  this.config.addOnsConfig[key] = addOnsConfig[key];
			  this.registerAddOn(key);
		  }
		}	
	},
	

	registerDefaultAddOn: function(addOnId){
		var addOn = this.getAddOnConstructorById(addOnId);

		if (typeof (addOn) !== 'undefined' && addOn != null) {
			var registeredAddOn = registeredAddOn = new addOn(this.config);							
			this.registeredAddOns[addOnId] = registeredAddOn;
		}
	},
	
	registerAddOn: function(addOnId){
		var addOn = this.getAddOnConstructorById(addOnId);
		var addOnConfig =  (this.config.addOnsConfig ? this.config.addOnsConfig[addOnId] : null);
		
		if (typeof (addOn) !== 'undefined' && addOn != null && typeof (addOnConfig) !== 'undefined' && addOnConfig != null){
			var registeredAddOn = new addOn(this.propagateConfig(addOnId, addOnConfig));			
			this.registeredAddOns[addOnId] = registeredAddOn;
		}
	},
	
    getAddOnConstructorById: function(addOnId){
    	var addOnConstructor = null;
    	
    	switch(addOnId){
    		case 'Cluster':
    			addOnConstructor = ClusterControl;
    			break;
    		
    		case 'Marker':
    			addOnConstructor = Ab.svg.MarkerControl;
    			break;
    	}
    	
    	return addOnConstructor;
    },
    
    
	/**
	 * adds drawing control's root config to the addOnConfig
	 */
	propagateConfig: function(addOnId, addOnConfig){
		if(addOnId === 'DatasourceSelector'){
			return this.config;
		} else {
			return this.cloneConfig(addOnId, addOnConfig);
		}
	},
	

	/**
	 * copy config's base parameter to the addOnConfig for the add-on component.
	 */
	cloneConfig: function(addOnId, addOnConfig){
		
		if(typeof addOnConfig === 'undefined' || !addOnConfig){
			addOnConfig = {};
		}
		
		for (var key in this.config) {
			if (this.config.hasOwnProperty(key) && (typeof key !== 'function') && (key !== 'addOnsConfig') && !(typeof (addOnConfig[key]) !== 'undefined' && addOnConfig[key] != null)) {
				addOnConfig[key] = this.config[key];
			}
		}
		return addOnConfig;
	},
	
	getAddOn: function(addOnId){
		return this.registeredAddOns[addOnId];
	},
	
	removeAddOn: function(addOnId) {
        delete this.registeredAddOns[addOnId];  
    }
}, {});