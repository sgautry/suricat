/*
 * Main controller to:
 *  - Register controllers and add-ons.
 *  - Direct API calls to different sub-controllers based on the current drawing state.
 *  - Stores the current drawing state/mode: Single Select (default), Pan, Zoom Window or Multiple Select
 *
 * It also stores the reference to DrawingsBase which contains all SVG controls that loaded into the specified root <div>
 * 
 * There will be one and only one drawingController per panel.
 */
DrawingController = Base.extend({
	
	/**
	 * drawing controller config, usually contains div id.
	 */
	config: {},

	/**
	 * A map of ids and their corresponding registered controllers 
	 */
	registeredControllers: {},
	
	/**
	 * References to the add-on controller object
	 */
	addOnsController: null,
	
    /**
     * DrawingsBase control to hold all SVG drawings 
     */ 
    control: null,
	
	/**
	 * Are all drawing loaded?
	 */
	isDrawingsLoaded: false,
	
    /**
	 * Constructs DrawingController and initializes the DrawingsBase  object.
	 */
	constructor:  function(divId, config){
		this.config = config;
		
		this.config.divId = divId;
		
		this.registeredControllers = {};
		
		//initialize the control
		this.control = new DrawingsBase(divId, config);
		this.control.setDrawingController(this);
		
        this.isDrawingsLoaded = false;
	},

	/**
	 * Retrieves DrawingsBase control for all SVG drawings
	 */
	getControl: function(){
		return this.control;
	},
	
    /**
     * Retrieves the controller for the specified controller id.
     * 
     * @param controllerId String controller id
     * @return the corresponding controller object
     */
	getController: function(controllerId){
		return this.registeredControllers[controllerId];
	},
	
    /**
     * Retrieves the add-on for the specified add-on id.
     * 
     * @param addOnId String add-on id
     * @return the corresponding add-on object
     */
    getAddOn: function(addOnId){
    	return (this.addOnsController ? this.addOnsController.getAddOn(addOnId) : null);
    },
    
    /**
     * Show or hide navigation toolbar based on the number of drawings.
     * 
     * If more than one drawing, navigation toolbar will show, otherwise, will hide.
     */
    updateNavigationToolbar: function(){
        var navToolbar = this.getAddOn("NavigationToolbar");
		if(navToolbar){
			if(Object.keys(this.getControl().drawings).length > 0){
				//only show if there is at least one drawing
				navToolbar.show(true);
			} else {
				navToolbar.show(false);
			}
		}
    },
    
    /**
     * Set 'IsDrawingsLoaded' property, update Add-on UI.
     * 
     * @param loaded Boolean true if all drawings complete loading, false otherwise.
     */
    setIsDrawingsLoaded: function(loaded) {
        this.isDrawingsLoaded = loaded;

        if(loaded){
	        // hide navigation toolbar if any
	        var navToolbar = this.getAddOn('NavigationToolbar');
	        if(navToolbar)
	        	navToolbar.show(loaded);
	     }
    },
    
    /**
     * check if all drawings are loaded and set 'isDrawingsLoaded'
     */
    updatedDrawingsLoaded: function(){
    	var loaded = true;

    	if(!this.control.drawings || Object.getOwnPropertyNames(this.control.drawings).length < 1){
    		loaded = false;
    	} else {
	    	for(drawingName in this.control.drawings){
	    		var drawing = this.control.drawings[drawingName];
	    		if(drawing && !drawing.isDrawingLoaded){
	    			loaded = false;
	    		}
	    	}
    	}
    	this.setIsDrawingsLoaded(loaded);
    },
    
    /**
	 * Inits and registers the required controllers.
	 */
	init: function(){

		this.readConfig(this);
		
        //register pan-zoom controller
        this.registerController('PanZoomController');

        //register select controller
        this.registerController('SelectController');
        
        //register highlight controller
        this.registerController('HighlightController');
        
        //register asset controller
        this.registerController('AssetController');

        //register select controller
        this.registerController('EventController');
        
        //register layout controller
        this.registerController('DrawingLayoutController');
        
    },
	
	
	initAddOn: function(){
        this.addOnsController = new AddOnsController(this.config, this);	
        
        //register add on passed through config
        if(this.config.addOnsConfig){
        	this.addOnsController.setAddOns(this.config.addOnsConfig);
        }
	},
	
	registerController: function(controllerId){

		var controller = this.getControllerConstructorById(controllerId);
		if(typeof controller !== 'undefined' && controller != null){
			var registeredController = new controller(this.config);
			
			//set drawing controller if setDrawingController() is defined in controller class.
			if(typeof registeredController.setDrawingController === 'function') {
				registeredController.setDrawingController(this);
            }
			this.registeredControllers[controllerId] = registeredController;
		}
	},

	getControllerConstructorById: function(controllerId){
    	var controllerConstructor = null;
    	
    	switch(controllerId){
    		case 'AssetController':
    			controllerConstructor = AssetController;
    			break;

			case 'EventController':
    			controllerConstructor = EventController;
    			break;

    		case 'SelectController':
    			controllerConstructor = SelectController;
    			break;

    		case 'HighlightController':
    			controllerConstructor = HighlightController;
    			break;

    		case 'PanZoomController':
    			controllerConstructor = PanZoomController;
    			break;

    		case 'DrawingLayoutController':
    			controllerConstructor = DrawingLayoutController;
    			break;

    		case 'MarkupController':
    			controllerConstructor = MarkupController;
    			break;
    			
    		case 'MarkupDataController':
    			controllerConstructor = MarkupDataController;
    			break;

    	}

    	return controllerConstructor;
    },
    
	removeController: function(controllerId) {
        delete this.registeredControllers[controllerId];  
    },
    
    registerAddOn: function(addOnId){
    	this.addOnsController.registerAddOn(addOnId);
    },
    
    removeAddOn: function(addOnId){
    	this.addOnsController.removeAddOn(addOnId);
    },
    
  
    /**
     * add events (both common events applied to all drawings and per-drawing event passed from client)
     * 
     * @svg the SVG drawing to add event to
     * @param events per-drawing event passed from client-side
     */
    addEventHandlers: function(svg, events){
    	// add default events
    	var assetTooltip = this.getAddOn("AssetTooltip");
    	if(assetTooltip){
    		assetTooltip.registerEvents(svg);
    	}
    	
        // bind common events applied to all drawings
        if(this.config.events){
    		this.getController("EventController").addEventHandlers(svg, this.config.events);
        }

        // bind per-drawing events passed from client
        if(events){
    		this.getController("EventController").addEventHandlers(svg, events);
        }
    },
    
    /**
     * Retrieve drawing config from drawing-control.xml.
     */
    readConfig: function(drawingController){
    	AdminService.getDrawingControlConfig({
        	callback: function(ob){
        
        		// add the default assigned color as the first auto assigned color
        		// then add the rest of auto colors.
        		var autoColors = new Array();
        		autoColors[0] = '0x' + gAcadColorMgr.getRGB(ob.highlights.assigned.fill.color, true, true);
        		var index = 1;
        		for (var i=0;i<ob.autoAssignColors.length;i++){
        			//do not add the auto color if it is the same as the default assigned color
        			if(ob.autoAssignColors[i].toLowerCase() != autoColors[0].toLowerCase()){
        				autoColors[index] = ob.autoAssignColors[i];
        				index++;
        			}
        		}
        		ob.autoAssignColors = autoColors;
        		drawingController.config.defaultConfig = ob;
        	},
        	
        	errorHandler: function(m, e){
            	Ab.view.View.showException(e);
       	 }
    	});
    }
}, {});