/**
 * Controller to handle the multiple drawing layout.
 * 
 */

DrawingLayoutController = Base.extend({

    /*
     * config object
     */
	config: {},
	
	/**
	 * true if ordering the buildings by column, meaning the floors from the same building will display on the same column (vertically), 
	 * false otherwise, meaning the floors from the same building will display on the same row (horizontally).
	 */ 
	orderByColumn: true,

	/**
	 * allow multiple SVG?
	 */
	allowMultipleDrawings: false,
	
	/**
	 * list of buildings displayed
	 */ 
	buildings: [],
	
	/**
	 * A map of building id with its corresponding floor id and drawing name.
	 * 
	 *  map of {bl1: [{fl1: dwgname1},
					  {fl2: dwgname2}],
				bl2: ...	  
			  }
	*/
	floorsByBuilding: {},
	
	/**
	 * number of columns
	 */
	numberOfColumns: 0,
	
	/**
	 * number of rows
	 */
	numberOfRows: 0,
	
	
	
	// @begin_translatable
    Z_NO_CONTENTS_MESSAGE: 'No drawing contents found',
    // @end_translatable
    
    /**
     * Constructor. Initialize the config and properties.
     */
	constructor:  function(config){
	
		this.config = config;
		
		this.allowMultipleDrawings = (this.config.allowMultipleDrawings == 'true' ? true : false);
	
		this.orderByColumn = (this.config.orderByColumn === 'false' ? false : true); 
	},

	/**
	 * Inserts SVG content from server to the layout, update width and height. If the drawing is already loaded, remove the old content then replace with the new one.
	 * 
	 * @svgText XML String the SVG content retrieved from server.
	 * @options Map options such as buildingId, floorIf, drawingName, orderByColumn to place SVG. 
	 */
	addSvg: function(svgText, options){
		
		this.orderByColumn = (options.orderByColumn === 'false' ? false : true); 
		
		// add the floor plan
		var svg = this.drawingExists(options.drawingName);
		var viewBox;
		if(svg){
			//retrieve parent id first
			var svgDwgDivId = svg.node().parentNode.id;

			var width = svg.attr("width");
			var height = svg.attr("height");
			
			viewBox = svg.attr("viewBox");
			
			//remove svg
			svg.remove();
			
			// insert the new svg contents and immediately assign id
	        d3.select("#" + svgDwgDivId).node().innerHTML = (svgText) ? svgText : '<div class="empty-drawing-txt">' + View.getLocalizedString(this.Z_NO_CONTENTS_MESSAGE) + '</div>';
	        
	        //var viewBox = options['viewBox'];
	        if(typeof viewBox === 'undefined' || viewBox === null){
		        d3.select("#" + svgDwgDivId).select("svg")
	        	.attr("id", this.config.divId + "-" + DrawingCommon.retrieveValidNodeId(options.drawingName) + "-svg")
	        	.attr("width", width)
	        	.attr("height", height);
	        } else {
		        d3.select("#" + svgDwgDivId).select("svg")
	        	.attr("id", this.config.divId + "-" + DrawingCommon.retrieveValidNodeId(options.drawingName) + "-svg")
	        	.attr("width", width)
	        	.attr("height", height)
	        	.attr("viewBox", viewBox);
	        }
		} else {
			this.addFloorToMap(options.buildingId, options.floorId, options.drawingName)
		}
		
		if(typeof viewBox === 'undefined' || viewBox === null){
			var position = this.calculatePosition(options);
			
			this.buildDivTable();

			this.placeSvg(svgText, position, options.drawingName);
			
			svg = this.drawingExists(options.drawingName);
			if(svg){
				svg.call(viewBoxZoom());
			}
		}
		
	},
	
	/**
	 * Checks if the drawing is already displayed on the layout.
	 * 
	 * @param drawingName String drawing name
	 * @return SVGSVGElement svg selection or null if the drawing is not found.
	 */
	drawingExists: function(drawingName){
		// WC-2604 change line
		var svgDwgId = DrawingCommon.retrieveValidSvgId(this.config.divId , drawingName );
		var svg = d3.select("#" + svgDwgId);
		if(svg && !svg.empty()){
			return svg;
		} else {
			return null;
		}
	},
	
	/**
	 * Removes the specified drawing from layout. Shifts the drawings to the previous column or row as needed. Update buildings and floorsByBuilding map.
	 * 
	 * @param buildingId String buiding id
	 * @param floorId String floor id
	 * @param drawingName String drawing name.
	 */
	removeSvg: function(buildingId, floorId, drawingName){
		var oldColumnIndex = -1;
		var oldRowIndex = -1;
		
		if(this.orderByColumn){
			oldColumnIndex = this.buildings.indexOf(buildingId);
			if(oldColumnIndex < 0)
				return;
		} else {
			oldRowIndex = this.buildings.indexOf(buildingId);
			if(oldRowIndex < 0)
				return;
		}
		
		var floorsArray = this.floorsByBuilding[buildingId];
		var shiftColumn = false;
		var shiftRow = false;
		var shiftSingleColumn = false;
		var shiftSingleRow = false;
	
		for(var i = 0; i < floorsArray.length; i++){
			var floorJson = floorsArray[i];
			if(floorJson && floorJson[floorId] !== undefined){
		
				// match the drawing name
				if(drawingName !== floorJson[floorId])
					continue;
				
				if(this.orderByColumn){
					oldRowIndex = i;
				} else {
					oldColumnIndex = i;
				}
				var svgDwgDivId = this.config.divId + "-" + oldRowIndex + "-" + oldColumnIndex;
				d3.select("#" + svgDwgDivId).select("svg").remove();
				
				if(floorsArray.length ===1 ){
					if(this.orderByColumn){
						this.buildings.splice(oldColumnIndex, 1);
						delete this.floorsByBuilding[buildingId];
						//shift all SVG after building over
						shiftColumn = true;
					} else {
						this.buildings.splice(oldRowIndex, 1);
						delete this.floorsByBuilding[buildingId];
						//shift all SVG after building is over
						shiftRow = true;
					}
				} else {
					if(this.orderByColumn){
						floorsArray.splice(oldRowIndex, 1);
						this.floorsByBuilding[buildingId] = floorsArray;
						shiftRow = this.needShiftRow(oldRowIndex);
						if(oldRowIndex < floorsArray.length){
							shiftSingleColumn = true;
						}
					} else {
						floorsArray.splice(oldColumnIndex, 1);
						this.floorsByBuilding[buildingId] = floorsArray;
						shiftColumn = this.needShiftColumn(oldColumnIndex);
						if(oldColumnIndex < floorsArray.length){
							shiftSingleRow = true;
						}
					}
				}
				break;
			}
		}
		
		if(this.orderByColumn && oldRowIndex < 0)
			return;

		if(!this.orderByColumn && oldColumnIndex < 0)
			return;
		
		var oldNumberOfColumns = this.numberOfColumns;
		var oldNumberOfRows = this.numberOfRows;
		
		this.numberOfColumns = this.calculateNumberOfColumns();
		this.numberOfRows = this.calculateNumberOfRows();
		
		if(shiftColumn || shiftSingleRow){
			this.shiftSvgsAfterColumn(oldRowIndex, oldColumnIndex, shiftSingleRow);
		} 
		
		if(shiftRow || shiftSingleColumn){
			this.shiftSvgsAfterRow(oldRowIndex, oldColumnIndex, shiftSingleColumn);
		}
		
		this.removeEmptyColumn(oldNumberOfColumns, oldNumberOfRows);
			
		this.removeEmptyRow(oldNumberOfRows);

		this.setDimension();
		
	},
	
	needShiftRow: function(oldRowIndex){
		var svg;
		var needShift = true;
		for(var col = 0; col < this.numberOfColumns; col++){
			svg = d3.select("#" + this.config.divId + "-" + oldRowIndex + "-" + col).select("svg");
			if(svg && !svg.empty()){
				needShift = false;
				break;
			}
			
		}
		
		return needShift;
	},
	
	needShiftColumn: function(oldColumnIndex){
		var svg;
		var needShift = true;
		for(var row = 0; row < this.numberOfRows; row++){
			svg = d3.select("#" + this.config.divId + "-" + row + "-" + oldColumnIndex).select("svg");
			if(svg && !svg.empty()){
				needShift = false;
				break;
			}
			
		}
		
		return needShift;
		
	},
	
	/**
	 * shift all SVG to the div on the left of the specified column.
	 */
	shiftSvgsAfterColumn: function(oldRowIndex, oldColumnIndex, shiftSingleRow){
		var rowIndex = (this.orderByColumn ? oldRowIndex: 0);
		var maxRowIndex;
		
		if(shiftSingleRow){
			rowIndex = maxRowIndex = oldRowIndex;
		} else {
			rowIndex = 0;
			maxRowIndex = this.numberOfRows -1;
		}
		
		for(var row = rowIndex; row <= maxRowIndex; row++){
			for(var col = oldColumnIndex + 1; col < this.numberOfColumns + 1; col++){
				var dwgDiv = d3.select("#" + this.config.divId + '-' + row + "-" + col);
				if(!dwgDiv.empty()){
					var removed = dwgDiv.select("svg").remove();
					if(removed && !removed.empty()){
						d3.select("#" + this.config.divId + '-' + row + "-" + (col-1))
							.append(function() {
							  return removed.node();
							});
					}
				}
			}
		}
	},
	
	/**
	 * shift all SVGs to the div above after the specified row for the specified column.
	 */
	shiftSvgsAfterRow: function(oldRowIndex, oldColumnIndex, shiftSingleColumn){
		var colIndex = (this.orderByColumn ? oldColumnIndex: 0);
		var maxColIndex;
		
		if(shiftSingleColumn){
			colIndex = maxColIndex = oldColumnIndex;
		} else {
			colIndex = 0;
			maxColIndex = this.numberOfColumns -1;
		}
		for(var row = oldRowIndex + 1; row < this.numberOfRows + 1; row++){
			for(var col = colIndex; col <= maxColIndex; col++){
				var dwgDiv = d3.select("#" + this.config.divId + '-' + row + "-" + col);
				if(!dwgDiv.empty()){
					var removed = dwgDiv.select("svg").remove();
					if(removed && !removed.empty()){
						d3.select("#" + this.config.divId + '-' + (row-1) + "-" + col)
							.append(function() {
							  return removed.node();
							});
					}
				}
			}
		}
	},
	
	/**
	 * remove the last empty column after shifting SVGs
	 */
	removeEmptyColumn: function(oldNumberOfColumns, oldNumberOfRows){
		if(this.numberOfColumns < oldNumberOfColumns){
			for(var row = 0; row < oldNumberOfRows; row++){
				d3.select("#" + this.config.divId + '-' + row + '-' + this.numberOfColumns).remove();
			}
		}
	},
	
	/**
	 * remove the last empty row after shifting SVGs
	 */
	removeEmptyRow: function(oldNumberOfRows){
		if(this.numberOfRows < oldNumberOfRows){
			for(var row = this.numberOfRows; row < oldNumberOfRows; row++){
				d3.select("#" + this.config.divId + '-' + row).remove();
			}
		}
	},
	
	
	addFloorToMap: function(buildingId, floorId, drawingName){
		if(this.floorsByBuilding[buildingId] !== undefined && this.allowMultipleDrawings){
			var floorsArray = this.floorsByBuilding[buildingId];
			var found = false;
			for(var i =0; i < floorsArray.length; i++){
				for(var flId in floorsArray[i]){
					if(flId === floorId && floorsArray[i][flId] === drawingName){
						found = true;
					}
				}
			}
			
			if(!found){
				var floorJson = {};
				floorJson[floorId] = drawingName;
				floorsArray.push(floorJson);
		
				this.floorsByBuilding[buildingId] = floorsArray;
			}
		} else {
			if(this.allowMultipleDrawings){
				this.buildings.push(buildingId);
				this.floorsByBuilding[buildingId] = [];
				var length = this.floorsByBuilding[buildingId].length;
				this.floorsByBuilding[buildingId][length] = { };
				this.floorsByBuilding[buildingId][length][floorId] = drawingName;
			} else {
				this.buildings = [buildingId];
				this.floorsByBuilding[buildingId] = [];
				this.floorsByBuilding[buildingId][0] = { };
				this.floorsByBuilding[buildingId][0][floorId] = drawingName;
			}
		}
	},
	
	calculatePosition: function(options){

		this.numberOfColumns = this.calculateNumberOfColumns();
		this.numberOfRows = this.calculateNumberOfRows();
		
		var position = {};
		
		if(this.orderByColumn){
			position.columnIndex = this.buildings.indexOf(options.buildingId);
			position.rowIndex = this.floorsByBuilding[options.buildingId].length - 1;
		} else {
			position.columnIndex = this.floorsByBuilding[options.buildingId].length - 1;
			position.rowIndex = this.buildings.indexOf(options.buildingId);
			
		}

		if(position.columnIndex === -1){
			position.columnIndex = 0;
		}

		if(position.rowIndex === -1){
			position.rowIndex = 0;
		}

		return position;
		
	},
	
	buildDivTable: function(){
		
		// clear div table if only one drawing allowed at a time
		this.clearDivTable(false);
		 
		var rowsNumber = (this.numberOfRows < 1 ? 1 : this.numberOfRows);
		var colsNumber = (this.numberOfColumns < 1 ? 1 : this.numberOfColumns);
		
		for(var row = 0; row < rowsNumber; row++){
			var rowDiv = d3.select("#" + this.config.divId + '-' + row);
			if(rowDiv.empty()){
				rowDiv = d3.select("#" + this.config.divId).insert("div", "#" + this.config.divId + '_navToolbarContainer')
			 			.attr("id", this.config.divId + '-' + row);
			}
			 
			for(var column = 0; column < colsNumber; column++){
				var dwgDiv = d3.select("#" + this.config.divId + '-' + row + "-" + column);
				if(dwgDiv.empty()){
					rowDiv.append("div")
						.attr("id", this.config.divId + '-' + row + "-" + column);
				}
	        }
	     }
		
	},
	
	/**
	 * clear all the div content
	 * @forceClear force clear table even if it is multiple drawing (used in datasource selector)
	 * 
	 */
	clearDivTable: function(forceClear){
		if(!this.allowMultipleDrawings || forceClear){
			//clear the div table
			d3.select("#" + this.config.divId).node().innerHTML = "";
		}
	},
	
	placeSvg: function(svgText, position, drawingName){

		var svgDwgDivId = this.config.divId + "-" + position.rowIndex + "-" + position.columnIndex;

		// insert svg contents and immediately assign id
        d3.select("#" + svgDwgDivId).node().innerHTML = (svgText) ? svgText : '<div class="empty-drawing-txt">' + View.getLocalizedString(this.Z_NO_CONTENTS_MESSAGE) + '</div>';
        
        var svg = d3.select("#" + svgDwgDivId).select("svg")
	        	.attr("id", this.config.divId + "-" + DrawingCommon.retrieveValidNodeId(drawingName) + "-svg")
	        	.attr("width", "100%")
	        	.attr("height", "100%"); 

        this.setDimension();
	},
		
	
	setDimension: function(){
		
		//loop through all drawing and set the width and height accordingly
        for(var row = 0; row < this.numberOfRows; row++){
        	d3.selectAll("#" + this.config.divId + "-" + row)
                	.style("width", "100%")
                	.style("height", 100/this.numberOfRows + "%");
    	    
        	for(var column = 0; column < this.numberOfColumns; column++){
	        	d3.selectAll("#" + this.config.divId + "-" + row + "-" + column)
	                	.style("width", 100/this.numberOfColumns + "%")
	                	.style("height", "100%")
	            
	            // only set border when there is more than one drawings
	        	if(this.numberOfRows > 1 || this.numberOfColumns > 1){
	        		d3.selectAll("#" + this.config.divId + "-" + row + "-" + column)
	        	    	.style("display", "inline-block")
	        			.style("box-sizing", "border-box")
	        			.style("-moz-box-sizing", "border-box")
	        			.style("border", "5px solid white")
	        			.style("overflow", "hidden");
	        	}
        	}
        		
        }
	},
	
	/**
	 * 
	 */ 
	calculateNumberOfRows: function(){
		var numberOfRows = 0;
		if(this.orderByColumn){
			for(var i = 0; i < this.buildings.length; i++){
				var floorsArray = this.floorsByBuilding[this.buildings[i]];
				if(numberOfRows < floorsArray.length){
					numberOfRows = floorsArray.length;
				}
			}
		} else {
			numberOfRows = this.buildings.length;
		} 
		return numberOfRows;
	},
	
	calculateNumberOfColumns: function(){
		var numberOfColumns = 0;
		
		if(this.orderByColumn){
			numberOfColumns = this.buildings.length;
		} else {
			for(var i = 0; i < this.buildings.length; i++){
				var floorsArray = this.floorsByBuilding[this.buildings[i]];
				if(numberOfColumns < floorsArray.length){
					numberOfColumns = floorsArray.length;
				}
			}
		}
		return numberOfColumns;
	}	
}, {});