/**
 * Controller to handler the asset selection.
 * 
 * Used by both Desktop and Mobile.
 */
SelectController = Base.extend({

	config: {},
	
	/**
	 * reference to drawingController
	 */
	drawingController: null,
	
	/**
	 * a set of selected asset ids.
	 */
	selectedAssets: [],
	
	/**
	 * true to sets multiple asset selection on, false otherwise.
     */
	multipleSelectionOn: false,
	
	/**
	 * Constant: diagonal pattern id.
	 */
	DIAGONALPATTERN_ID: 'diagonalPattern',
	
	/**
	 * If enabling diagonal Selection Pattern.
	 */
	diagonalSelectionPattern: false,
	/**
	 * diagonal Selection Border Width.
	 */
	diagonalSelectionBorderWidth: 20,
	
	/**
	 * cache diagonal Selection previous border status.
	 */
	selectedCachedPreviousBorderColors: {},
	
	/**
	 * Constructor. Initialize the config.
	 * 
	 * @param config Map config object
	 */
	constructor: function(config){
		this.config = config;

		/**
		 * Can asset be selected?
		 * 0: None
		 * 1: Assign Only for Select-able Asset.
		 * 2: All (Default)
		 */
		this.config.selectionMode = (config.selectionMode ? config.selectionMode : '0');

		//Can user select more than one asset at once? default to false.
		this.multipleSelectionOn = false;
		
		// set options for border highlights
		this.diagonalSelectionPattern = false;
		this.diagonalSelectionBorder = 20;
		this.selectedCachedPreviousBorderColors = {};
		
		//reset the selectedAssets map
		this.selectedAssets = {};
		
	},

	/**
     * set reference to drawingController
     */
    setDrawingController: function(drawingController){
    	this.drawingController = drawingController;
    },
    
    /**
     * set multipleSelectionOn to the specified value
     * 
     * @param enabled Boolean true to set multiple selection on, false otherwise.
     */
    setMultipleSelection: function(enabled){
    	this.multipleSelectionOn = enabled;
    },
    
	/**
	 * Gets the selected assets.
	 * 
	 * @return Array selected assets' ids.
	 */
	getSelectedAssets: function(){
		return this.selectedAssets;
	},
	
    setDiagonalSelectionPattern: function(enabled){
    	this.diagonalSelectionPattern = enabled;
    },
    
    setDiagonalSelectionBorder: function(size){
    	this.diagonalSelectionBorder = size;
    },
 	
	//TODO: clear method to clear all selections
	
	/**
	 * Gets the selected assets.
	 * 
	 * @return Array selected assets' ids.
	 */
	getSelectedAssets: function(){
		return this.selectedAssets;
	},
	
	/**
     * Sets DiagonalSelectionPattern to be enabled or not.
     * @param: enabled true|false.
     */
    setDiagonalSelectionPattern: function(enabled){
    	this.diagonalSelectionPattern = enabled;
    },
    /**
     * Sets Diagonal Selection Border Width.
     * @param: size number (default is 20).
     * 
     */
    setDiagonalSelectionBorderWidth: function(size){
    	this.diagonalSelectionBorderWidth = size;
    },
 	
	//TODO: clear method to clear all selections
	
	/**
	 * Select or unselects the specified asset. Based on the config, the asset will have either solid or border highlight.
	 * 
	 * @param svgId String the svg drawing's id
	 * @param assetId String asset id like 'HQ;17;105'.
	 * @param color highlight color.
	 * @param assetType String type of the asset, like 'rm' or 'eq'.
	 */
	toggleAssetSelection: function(svgId, assetId, color, assetType){
		var selectionMode =  this.config.selectionMode;
		if(selectionMode && selectionMode === '0'){
			return;
		}
		
		var useDiagonalSelection = this.diagonalSelectionPattern;
		//XXX: diagonalSelection for rm assets only
	    if(typeof assetType !== 'undefined' &&  assetType !== null && assetType !== 'rm'){
	    	useDiagonalSelection = false;
	    }
		if(!useDiagonalSelection && (typeof color === 'undefined' || color == null)){
			color = this.formatColor(this.config.defaultConfig.highlights.selected.fill.color, true);
		}
		
		var assetController = this.drawingController.getController("AssetController");
		var index = -1;
		if(this.selectedAssets[svgId]){
			index = this.selectedAssets[svgId].indexOf(assetId); 
		}
		
		if( index >= 0){
			if(useDiagonalSelection){
				this.clearDiagonalSelection(assetController,  svgId, assetId);
			}else{
				this.drawingController.getController("HighlightController").clearAsset(assetId, {'svgId': svgId, 'color': color, 'persistFill': false, 'overwriteFill' : true});
			}
			this.selectedAssets[svgId].splice(index, 1);
		} else {
			// if only one asset can be selected at a time - clear previously selected assets.
			if(!this.multipleSelectionOn){
				if(useDiagonalSelection && this.selectedAssets[svgId] && this.selectedAssets[svgId].length > 0){
					this.clearDiagonalSelection(assetController,  svgId, this.selectedAssets[svgId][0]);
				}else{
					//clear selected asset from all drawings
					for(var selectedSvgId in this.selectedAssets ){
						for(var selectedAssetIndex = 0; this.selectedAssets[selectedSvgId] && selectedAssetIndex < this.selectedAssets[selectedSvgId].length; selectedAssetIndex++){
							this.drawingController.getController("HighlightController").clearAsset(this.selectedAssets[selectedSvgId][selectedAssetIndex], {'svgId': selectedSvgId, 'color': color, 'persistFill': false, 'overwriteFill' : true});
						}
					}
				}
				this.selectedAssets = {};
				this.selectedAssets[svgId] = [];
			}
			
			var isSelected = false;
			if(useDiagonalSelection){
				isSelected = this.diagonalSelection(assetController, svgId, assetId, selectionMode);
			}else{
				isSelected = this.drawingController.getController("HighlightController").highlightAsset(assetId, {'selectionMode': selectionMode, 'svgId': svgId, 'color': color, 'persistFill': true, 'overwriteFill' : true});
			}
			if(isSelected){
				if(this.selectedAssets[svgId] === undefined){
					this.selectedAssets[svgId] = [];
				}
				
				this.selectedAssets[svgId].push(assetId);
			}
		}
	},
	

	/**
	 * Clears DiagonalSelection.
	 */
	clearDiagonalSelection: function(assetController, svgId, assetId){
		var asset = assetController.getAssetById(svgId, assetId);
		var assetNode = asset.node();
		var selectedNode= d3.select(assetNode);
		if(this.selectedCachedPreviousBorderColors[assetId] !== undefined){
			selectedNode.style("stroke", this.selectedCachedPreviousBorderColors[assetId].stroke);
			selectedNode.style("stroke-width", this.selectedCachedPreviousBorderColors[assetId].stroke_width);
		}else{
			selectedNode.style("stroke", "none");
			selectedNode.style("stroke-width", "0px");
		}
		
		
	},
	
	/**
	 * Does diagonalSelection.
	 */
	diagonalSelection: function(assetController, svgId, assetId, selectionMode){
		var asset = assetController.getAssetById(svgId, assetId);
		var assetNode = asset.node();
		var selectedNode =  d3.select(assetNode);
		//XXX: check asset's selectability 
		if((selectionMode && selectionMode === '0') ||(selectionMode && selectionMode === '1' && selectedNode.attr('selectable') !== 'true') ){
			return false;
		}
		
		if(selectedNode.style("stroke") !== undefined){
			this.selectedCachedPreviousBorderColors[assetId] = {'stroke':selectedNode.style("stroke"), 'stroke_width':selectedNode.style("stroke-width")};
		}
		var svg = assetController.getSvg(svgId);
		var units = DrawingCommon.getUnits(svg);
		
		selectedNode.style("stroke", "url(#" + this.DIAGONALPATTERN_ID + units + ")")
		.style("stroke-width", this.scaleBorderSize(units)  +"px")
		.style("stroke-opacity", 1);
		return true;
	},
	
	/**
	 * Scales border size based on published units - metric only
	 */
	scaleBorderSize: function(units){
		var boderSize = this.diagonalSelectionBorder;
		if(units !== 'inches'){
			switch(units) {
		    	case "mm":
		    		boderSize = this.diagonalSelectionBorder * 25.4;
		    		break;
		    	case "cm":
		    		boderSize = this.diagonalSelectionBorder * 2.54;
		    		break;
		    	default:
		    		boderSize = this.diagonalSelectionBorder * 0.0254;
			}
		}
		return boderSize;
	},
	
	/**
	 * Highlight Selected assets.
	 * 
	 * @param svgId String the svg drawing's id
	 * @param: assetIds Array a set of asset ids
	 * @param: color highlight color.
	 */
	highlightSelectedAssets: function(svgId, assetIds, color){
		if(typeof color === 'undefined' || color == null){
			color = this.formatColor(this.config.defaultConfig.highlights.selected.fill.color, true);
		}
	
		if(typeof this.selectedAssets[svgId] === 'undefined' || this.selectedAssets[svgId] === null){
			this.selectedAssets[svgId] = [];
		}
		
		if(this.multipleSelectionOn){
			for(var i = 0; i < assetIds.length; i++){
				var assetId = assetIds[i];
				this.drawingController.getController("HighlightController").highlightAsset(assetId, {'svgId': svgId, 'color': color, 'persistFill': true, 'overwriteFill' : true});
				this.selectedAssets[svgId].push(assetId);
			}
		} else {
			for(var selectedAssetIndex = 0; this.selectedAssets[svgId] && selectedAssetIndex < this.selectedAssets[svgId].length; selectedAssetIndex++){
				this.drawingController.getController("HighlightController").clearAsset(this.selectedAssets[svgId][selectedAssetIndex], {'svgId': svgId, 'color': color, 'persistFill': false, 'overwriteFill' : true});
			}
			this.selectedAssets[svgId] = [];
			if(assetIds && assetIds.length > 0){
				var assetId = assetIds[0];
				this.drawingController.getController("HighlightController").highlightAsset(assetId, {'svgId': svgId, 'color': color, 'persistFill': true, 'overwriteFill' : true});
				this.selectedAssets[svgId].push(assetId);
			}
		
		}
	},
	
	/**
	 * convert the color to HTML hex format.
	 */
	formatColor: function(c, asHex) {	
		return this.ensureValidColor(parseInt(c).toString(asHex == true ? 16 : 10).toString(), asHex);
	},
	
	/**
	 * append/trim the color number to the Hex compatible format. 
	 */
	ensureValidColor: function(c, asHex) {
		if (asHex == true && c.length < 6) {
			var i = c.length;
			c = '#' + ("000000" + c).substring(i);
		}
		
		return c;	
	}

}, {});