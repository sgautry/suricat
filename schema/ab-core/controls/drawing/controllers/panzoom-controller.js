/**
 * Controller to handler pan/zoom functionality.
 * 
 * Used by both Desktop and Mobile.
 */

PanZoomController = Base.extend({
	
	/*
	 * config map
	 */
	config: {},
	
	/**
	 * max zoom factor allowed
	 */
	maxZoomFactor: 100,
	
	/**
	 * min zoom factor allowed
	 */
	minZoomFactor: 0.2,
	
	/**
	 * default relative factor to increase/decrease when zoom in/out
	 */
	defaultFactor: 1.25,

	/**
	 * Constructor. Initialize the max/min zoom factor.
	 */
	constructor: function(config){
		this.config = config;
		
		this.maxZoomFactor = (this.config.maxZoomFactor? this.config.maxZoomFactor : this.maxZoomFactor);
		this.minZoomFactor = (this.config.minZoomFactor? this.config.minZoomFactor : this.minZoomFactor);
	},

	/**
	 * Retrieves the SVG drawing. If not initialized, set it based on config's divId and drawingName.
	 * 
	 * @return SVGSVGElement svg selection
	 */
	getSvg: function(){
		if(!this.config.svg || this.config.svg.empty()){
			this.config.svg = d3.select('#' + DrawingCommon.retrieveValidNodeId(this.config.divId + "-" + this.config.drawingName) + '-svg');
		}
		return this.config.svg;
	},
	
	/**
	 * Sets svg based on the specified drawing name.
	 * 
	 * @param drawingName String
	 */
	setSvg: function(drawingName){
		this.config.drawingName = drawingName;
		this.config.svg = d3.select("#" + DrawingCommon.retrieveValidNodeId(this.config.divId + "-" + drawingName) + "-svg");
	},
	
	/**
	 * Zooms in for the specified SVG.
	 * 
	 * @param svg SVGSVGElement svg selection
	 * @return Number zoom state
	 */
	zoomIn: function(svg){
		return this.zoom(this.defaultFactor, svg);
	},
	
	/**
	 * Zooms out for the specified SVG.
	 * 
	 * @param svg SVGSVGElement svg selection
	 * @return Number zoom state
	 */
	zoomOut: function(svg){
		return this.zoom(1/this.defaultFactor, svg);
	},

	/**
	 * Zooms with the specified factor for the specified SVG.
	 * 
	 * @param factor Number zoom factor
	 * @param svg SVGSVGElement svg selection
	 * @return Number zoom state
	 */
	zoom: function(factor, svg){

		if(!svg)
			svg = this.getSvg();

		try{
			factor = this.calculateFactor(factor, svg);
			
			//the factor changed
			if(factor != 1){
				var box = svg.attr("viewBox").split(/[\s,]/g).map(Number);
			    
				var viewBox = [];
				
				viewBox[2] = Math.round(box[2] / factor);
				viewBox[3] = Math.round(box[3] / factor);
		  
				viewBox[0] = Math.round(box[0] - (viewBox[2] - box[2]) / 2);
				viewBox[1] = Math.round(box[1] - (viewBox[3] - box[3]) / 2);          
			  
				// ??? d3 issue - frequent set viewbox could cause the svg placed in the wrong coordinates.
		  	    svg.attr("viewBox", viewBox);
			}
			
			return this.getZoomState(svg);
		} catch(err){
			throw err;
		}
	},

	/**
	 * Zooms the drawing into the specified bBox.
	 * 
	 * @param bBox bBox map of x, y, width and height.
	 * @param svg SVGSVGElement svg selection
	 * @return Number zoom state
	 */
	zoomIntoArea: function(bBox, svg){		
		if(!svg)
			svg = this.getSvg();
		
		/* var state = this.getZoomState(svg);
		if(state==2)
			return state;
		 
		var mirror = svg.select("#viewer").selectAll("#mirror").node().transform.baseVal.getItem(0).matrix.d;
		*/
        var w = svg.node().clientWidth || +svg.style("width").replace(/px$/, ""),
     		h = svg.node().clientHeight || +svg.style("height").replace(/px$/, "");
		
        var rect = svg.node().getBoundingClientRect();
        
        this.pan(rect.left + w/2 - bBox.x - bBox.width/2, rect.top + h/2 - bBox.y - bBox.height/2, svg);
		 
		var zoomFactor = Math.min(w/bBox.width, h/bBox.height);
     	return this.zoom(zoomFactor, svg);
	},
	
	/**
     * Zooms to the extent of the specified drawing.
     * 
     * @param svg SVGSVGElement svg selection
     * @return 1 if succeed, 0 otherwise.
     */
    zoomExtents: function (svg) {
    	if(!svg)
			svg = this.getSvg();
    	
    	if(!svg || svg.empty())
    		return 0;
    	
        var defaultView = svg.select("#defaultView");

        if (defaultView[0][0] !== null) {
            svg.attr("viewBox", defaultView.attr("viewBox"));
        }
        
        // return status 1 (zoom extents)
        return 1;
    },
    
   
    /**
	 * Checks if svg is zoomed.
	 * 
     * @param svg SVGSVGElement svg selection
	 * @ return true if svg is zoomed, false otherwise
	 */
	isZoomed: function(svg){
		var zoomFactor = this.getZoomFactor(svg);
		return (zoomFactor !== 1);
	},
	
    /**
     * Gets the drawing's zoom factor comparing to the default viewbox.
     * 
     * @param svg SVGSVGElement svg selection
     * @return Number the zoom factor, if the same as the default viewBox, return 1.
     */
	getZoomFactor: function(svg){
		if(!svg)
			svg = this.getSvg();
    	
    	if(!svg || svg.empty())
    		return;
    	
        var defaultView = svg.select("#defaultView");
		var defaultViewBox = '';
		if (defaultView[0][0] !== null) {
			defaultViewBox = defaultView.attr("viewBox");
		}
		
		var currentViewBox = this.getViewBox(svg);
		
		if(currentViewBox.join(" ") === defaultViewBox){
	        return 1;
		} else {
			var defaultBox = defaultViewBox.split(" ");
			return Math.round(defaultBox[2]* 100/currentViewBox[2])/100;
		}
		
	},
	
	/**
	 * Gets the zoom state for the specified SVG drawing:
	 *         0: default
	 *         1: zoom extents
	 *         2: max zoom
	 *         3: min zoom
	 *
     * @param svg SVGSVGElement svg selection
	 * @return Number SVG's zoom state.
	 */
	getZoomState: function(svg){
		var zoomFactor = this.getZoomFactor(svg); 
		var zoomState = 0;
		if(zoomFactor  == 1){
			zoomState = 1;
		} else if (Math.round(zoomFactor) == this.maxZoomFactor){
			zoomState = 2;
		} else if (Math.round(zoomFactor) == this.minZoomFactor){
			zoomState = 3;
		} 
		return zoomState;
	},
	
	/**
	 * Pans the drawing with the x, y value for the specified SVG.
	 * 
	 * @param x Number 
	 * @param y Number
     * @param svg SVGSVGElement svg selection
	 */
	pan: function(x, y, svg){
		if(!svg)
			svg = this.getSvg();
		
		var	box = svg.attr("viewBox").split(/[\s,]/g).map(Number),
			w = svg.node().clientWidth || +svg.style("width").replace(/px$/, ""),
			h = svg.node().clientHeight || +svg.style("height").replace(/px$/, ""),
			p = box[2] / w,
			q = box[3] / h,
			r = Math.max(p, q);

		box[0] = box[0] - x * r;
		box[1] = box[1] - y * r;

		svg.attr("viewBox", box);
	},
	
	/**
	 * Retrieves the viewBox for the specified SVG drawing.
	 * 
     * @param svg SVGSVGElement svg selection
	 * @return viewBox Array
	 */
	getViewBox: function (svg) {

		if(!svg)
			svg = this.getSvg();
		
		var viewBox = svg.attr("viewBox").split(" ");
	    if (viewBox.length === 1) {
	        viewBox = svg.attr("viewBox").split(",");
	    }

	    for (var i = 0; i < viewBox.length; i++) {
	        viewBox[i] = Number(viewBox[i]);
	    }
	    return viewBox;
	},
	
	calculateFactor: function(factor, svg){
		var zoomFactor = this.getZoomFactor(svg);
		var actualFactor = factor;
		if(zoomFactor*factor > this.maxZoomFactor){
			actualFactor =this.maxZoomFactor/zoomFactor;
		} else if(zoomFactor*factor < this.minZoomFactor){
			actualFactor = this.minZoomFactor/zoomFactor;
		}
		
		return actualFactor;
	},
	
	/**
	 * set zoom factor from the mouse wheel event
	 */
	setZoomFactor: function(delta, svg){
		if(delta > 0){
			this.calculateFactor(Math.round(1/delta), svg);
		} else {
			this.calculateFactor(Math.round(-delta*100)/100, svg);
		}
		
		return this.getZoomState(svg);
	}

}, {});

(function() {

	var vendor = (function(p) {
	  var i = -1, n = p.length, s = document.documentElement.style;
	  while (++i < n) if (p[i] + "Transform" in s) return p[i].toLowerCase();
	})(["webkit", "ms", "Moz", "O"]);

	var prefix = vendor ? "-" + vendor.toLowerCase() + "-" : "",
	    use3d = vendor && vendor + "Perspective" in document.documentElement.style;
	
	var modernBrowser = DrawingCommon.isModernBrowser();

	this.viewBoxZoom = viewBoxZoom;

	function viewBoxZoom() {
	  var event = d3.dispatch("zoomstart", "zoom", "zoomend");

	  function viewBoxZoom(svg) {
	    svg
	        //.style(prefix + "transform-origin", "0 0")
	        //.style(prefix + "backface-visibility", "hidden")
	        .each(zoomable);
	    
        if (modernBrowser) {
            svg.style(prefix + "transform-origin", "0 0")
                .style(prefix + "backface-visibility", "hidden");
        }
	  }

	  return d3.rebind(viewBoxZoom, event, "on");

	  function zoomable() {
	    var that = this,
	        svg = d3.select(that),
	        w = d3.select(window),
	        drag = false,
	        dx0 = 0,
	        dy0 = 0,
	        scale0 = this.getScreenCTM().a,
	        zoom = d3.behavior.zoom()
	          .on("zoomstart", function() { event.zoomstart.call(that); })
	          .on("zoom", function() {
	            if (drag) {
	              if (modernBrowser) {	
		              svg.style(prefix + "transform", "translate3d(" +
		                  Math.floor(d3.event.translate[0] - dx0 * d3.event.scale) + "px," +
		                  Math.floor(d3.event.translate[1] - dy0 * d3.event.scale) + "px,0)" +
		                  "scale3d(" + d3.event.scale + "," + d3.event.scale + ",1)");
	              }
	              event.zoom.call(that, d3.event.scale * scale0);
	            } else {
	              svg.call(viewBox);
	              event.zoom.call(that, scale0);
	            }
	          })
	          .on("zoomend", function() { event.zoomend.call(that, scale0); });

	    svg.call(viewBox);

	    d3.select(this.parentNode)
	        .on("touchstart.viewboxzoom", touchstart)
	        .on("mousedown.viewboxzoom", mousedown)
	        .call(zoom)
	        .call(zoom.event);

	    function touchstart() {
	      // Work around a strange issue in Android 4.1.x, where subsequent touch
	      // events are fired with a null "touches" property, probably due to native
	      // scrolling being triggered.
	      //d3.event.preventDefault();
	      drag = true;
	      w
	          .on("touchend.viewboxzoom", function() {
	            var touchById = {},
	                touches = d3.event.touches,
	                changed = d3.event.changedTouches;
	            for (var i = 0, n = changed.length; i < n; ++i) touchById[changed[i].identifier] = i;
	            for (var i = 0, n = touches.length; i < n; ++i) if (!touchById[touches[i].identifier]) return;
	            w.on("touchend.viewboxzoom", null);
	            svg.call(viewBox);
	          });
	    }

	    function mousedown() {
	      drag = true;
	      w
	          .on("mouseup.viewboxzoom", function() {
	            drag = false;
	            w.on("mouseup.viewboxzoom", null);
	            svg.call(viewBox);
	          });
	    }

	    function viewBox(svg) {
	      var t = zoom.translate(),
	          s = zoom.scale(),
	          box = svg.attr("viewBox").split(/[\s,]/g).map(Number),
	          // See https://bugzilla.mozilla.org/show_bug.cgi?id=874811
	          w = svg.node().clientWidth || +svg.style("width").replace(/px$/, ""),
	          h = svg.node().clientHeight || +svg.style("height").replace(/px$/, ""),
	          p = box[2] / w,
	          q = box[3] / h,
	          r = Math.max(p, q);

	      box[0] = box[0] - (t[0] - dx0) * r / s;
	      box[1] = box[1] - (t[1] - dy0) * r / s;
	      box[2] /= s;
	      box[3] /= s;

	      svg
	          .attr("viewBox", box);
	      
          if (modernBrowser) {
              svg.style(prefix + "transform", "translate3d(0, 0, 0)");
          }

	      // If preserveAspectRatio == "xMidYMid".
	      dx0 = q > p && .5 * (w - s * box[2] / q);
	      dy0 = p > q && .5 * (h - s * box[3] / p);

	      zoom.translate([dx0, dy0]).scale(1);
	      scale0 *= s;
	    }
	  }
	}
})();
