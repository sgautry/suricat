/**
 * Controller to handle the events.
 * 
 * Used for both Desktop and Mobile.
 * 
 */

EventController = Base.extend({

	/**
	 * configuration object 
	 */
	config: {},
	
	/**
	 * reference to drawing controller
	 */ 
	drawingController: null,
	
	/**
	 * Constructor. Initialize config object
	 */
	constructor: function(config){
		
		this.config = config;
		
	},

	/**
     * Set reference to drawingController
     */
    setDrawingController: function(drawingController){
    	this.drawingController = drawingController;
    },
    
    	
	/**
     * Adds JS events to assets and/or labels.
     * 
     * @param svg SVGSVGElement the svg drawing to add events to
     * @param eventHandlers Array a set of event handler map.
     */
    addEventHandlers: function (svg, eventHandlers) {
    	var self = this;
        for (var i = 0; i < eventHandlers.length; i++) {
            var eventHandler = eventHandlers[i];
            
            var highlightOnly = false;
            if(eventHandler && eventHandler.highlightOnly == true) 
            	highlightOnly = eventHandler.highlightOnly;
            
            this.addEvent2Assets(svg, self, eventHandler, highlightOnly);
            
            var assetOnly =  false;
            if(eventHandler && eventHandler.assetOnly == true) 
            	assetOnly = eventHandler.assetOnly;
            
            if(!assetOnly){
            	this.addEvent2Labels(svg, self, eventHandler, highlightOnly);
            }
        }
    },

    /**
     * Adds JS event to highlighted Assets.
     * @param svg drawing
     * @param self eventController
     * @param eventHandler event handler object
     * @param highlightOnly true if only add the event for highlighted assets, false to add it to both highlighted and non-highlighted assets
     */
    addEvent2Assets: function (svg, self, eventHandler, highlightOnly) {
        var eventName = eventHandler.eventName;
        var assetGroup = eventHandler.assetType + "-assets";

    	if (this.isNativeAndroid()) {
            svg.selectAll("#" + assetGroup).selectAll('*')
            .each(function () {
                self.addEvent2AssetAndroid(svg, this, eventHandler, eventName, highlightOnly);
            });
    	} else {
            svg.selectAll("#" + assetGroup).selectAll('*')
            .each(function () {
                self.addEvent2Asset(svg, this, eventHandler, eventName, highlightOnly);
                self.addMouseEvents2Asset(this, highlightOnly);
            });
    	}
    },
    
    /**
     * Add mouse event to the asset.
     * 
     * @param self eventController
     * @param highlightOnly true if only add the event for highlighted assets, false to add it to both highlighted and non-highlighted assets.
     */
    addMouseEvents2Asset: function (self, highlightOnly) {
    	var asset = d3.select(self);
        var assetNode = asset.node();

        var isValid = true;
        
        //check if the asset is highlighted
        if(highlightOnly)
        	isValid = (asset.attr('highlighted') === 'true');
        
        if (isValid) {
        	assetNode.addEventListener("mouseover", function (event) {
                event.stopPropagation();
                this.style.cursor="pointer";
        	});
        	assetNode.addEventListener("mouseout", function (event) {
                event.stopPropagation();
                this.style.cursor="all-scroll";
        	});
        }
    },
    
    /**
     * Adds JS event to highlighted Assets.
     * 
     * @param self eventController
     * @param eventHandler event handler object
     */
    addEvent2HighlightedAssets: function (svg, self, eventHandler) {
    	this.addEvent2Assets(svg, self, eventHandler, true);
    },

    /**
     * Add event to the asset.
     * 
     * @param self EventController
     * @param eventHandler event handler object
     * @param eventName String name of the event.
     * @param highlightOnly Boolean true if only add the event for highlighted assets, false to add it to both highlighted and non-highlighted assets.
     */
    addEvent2Asset: function (svg, self, eventHandler, eventName, highlightOnly) {
        var eventController = this;
        var asset = d3.select(self);
        var assetController = this.drawingController.getController("AssetController"); 
        var assetId = assetController.retrieveValidAssetId(self.id);
        var assetNode = asset.node();
        var svgId = svg.node().id;

        var isValid = true;
        
        //check if the asset is highlighted
        if(highlightOnly)
        	isValid = (asset.attr('highlighted') === 'true');
        
        if (isValid) {
        	var addEvent = false;
			// WC-2604 change lines
        	   if(assetNode.nodeName === 'use'){

        		var rect = assetController.getAssetById(svgId, assetId + "-rect");
        		if(rect && d3.select(rect.node()).empty()){
        			addEvent = assetController.patchBackgroundForUseElement(svgId, asset, eventHandler['bbox']);
            		rect = assetController.getAssetById(svgId, assetId + "-rect");

            		assetNode = rect.node();

        		} 
				// WC-2604 end changes
        		} else {
        		addEvent = true;
        	}
        	
        	if(addEvent){
                var flag = 0;
            	// set the fill to 'transparent' to allow event to fire within the boundary for asset such as panel which is only made up with borders. 
            	var fill = d3.select(assetNode).attr("fill");
        		if(typeof fill === 'undefined' || fill == null){
            		d3.select(assetNode).attr("fill", "transparent");
            	}
                // register mouseup event to prvent both "mouseup" and "click" events fired on click.
                if(eventName === 'click'){
                	assetNode.addEventListener("mousedown", function(event){
            			event.preventDefault();
                	    flag = 0;
                	}, false);
                	assetNode.addEventListener("mousemove", function(event){
            			event.preventDefault();
                	    flag = 1;
                	}, false);
                	
                	var navToolbar = this.drawingController.getAddOn("NavigationToolbar"); 
                	assetNode.addEventListener("mouseup", function(event){
            			event.preventDefault();
            			
                		// only execute if it is a left-click
                		if(flag === 0 && event.which === 1){
                			var params = {};
                			params['assetId'] = assetId;
                			params['svgId'] = svgId;
                			params['assetType'] = eventHandler['assetType'];
        	 	            eventHandler.handler(params, eventController.drawingController, event);
                	    } else if (navToolbar && navToolbar.mouseState.selecting){
                	    	navToolbar.mouseUpHandler(event);
                	    }
                		flag = 0;
                	}, false);
                } else {
	        		assetNode.addEventListener(eventName, function (event) {
		                event.stopPropagation();

		                if(eventName === 'contextmenu'){
		                	//stop showing browser's context menu
		                	event.preventDefault();
		                }
		                var params = {};
            			params['assetId'] = assetId;
            			params['svgId'] = svgId;
            			params['assetType'] = eventHandler['assetType'];
    	 	            eventHandler.handler(params, eventController.drawingController, event);
		        	});
                }
        	}
	        	
        }
    },

    /**
     * Adds JS event to highlighted individual Asset.
     * 
     * @param self EventController
     * @param eventHandler event handler object
     * @param eventName String name of the event.
     */
    addEvent2HighlightedAsset: function (svg, self, eventHandler, eventName) {
        var eventController = this;
        var asset = d3.select(self);
        var assetId = self.id;
        var assetNode = asset.node();

        //check if the asset is highlighted
        if (asset.attr('highlighted') === 'true') {
        	var assetController = this.drawingController.getController("AssetController");
        	assetNode.addEventListener(eventName, function (event) {
                event.stopPropagation();
                var params = {};
    			params['assetId'] = assetController.retrieveValidAssetId(assetId);
    			params['svgId'] = svg.node().id;
    			params['assetType'] = eventHandler['assetType'];
 	            eventHandler.handler(params, eventController.drawingController, event);
        	});
        }
    },

    /**
     * Adds JS event to all Assets (for Android).
     * 
     * @param self EventController
     * @param eventHandler event handler object
     * @param eventName String name of the event.
     * @param highlightOnly true if only add event to highlighted assets, false for all assets.
     */
    addEvent2AssetAndroid: function(svg, self, eventHandler, eventName, highlightOnly){
    	
        var eventController = this;
        var asset = d3.select(self);
        var assetId = self.id;
        var assetNode = asset.node();
        var objectMoved = null;
        var svgId = svg.node().id;
        
        var isValid = true;
        if(highlightOnly)
        	isValid = (asset.attr('highlighted') === 'true');
        
        //check if the asset is highlighted
        if (isValid) {
        	var assetController = this.drawingController.getController("AssetController");
        	if(assetNode.nodeName == 'use'){
        		var rect = assetController.getAssetById(svgId, assetId + "-rect");
        		if(d3.select(rect.node()).empty()){
        			addEvent = assetController.patchBackgroundForUseElement(svgId, asset, eventHandler['bbox']);
            		rect = assetController.getAssetById(svgId, assetId);
        		}
        		
        		assetNode = rect.node();
        	}
        	
            assetNode.addEventListener("touchstart", function () {
                objectMoved = null;
            });
            assetNode.addEventListener("touchmove", function (event) {
                objectMoved = event.target;
            });
    		assetNode.addEventListener("touchend", function (event) {
                event.stopPropagation();
                if (event.touches && event.touches.length > 2) {
                    return;
                }
                if (objectMoved && event.target === objectMoved) {
                    return;
                }
                if (eventHandler.handler) {
                    var params = {};
        			params['assetId'] = assetController.retrieveValidAssetId(assetId);
        			params['svgId'] = svg.node().id;
        			params['assetType'] = eventHandler['assetType'];

                	//eventController.addFeedback(assetId, asset, eventHandler);
     	            eventHandler.handler(params, eventController.drawingController);
                }
    		});
        }
    
    },
    
    /**
     * Adds JS event to highlighted individual Asset (for Android).
     * 
     * @param self EventController
     * @param eventHandler event handler object
     * @param eventName String name of the event.
     */
    addEvent2HighlightedAssetAndroid: function (self, eventHandler, eventName) {
    	this.addEvent2AssetAndroid(self, eventHandler, eventName, true);
    },

    addFeedback: function(assetId, asset, eventHandler) {
    	var eventController = this;
    	var assetController = this.drawingController.getController("AssetController");
		var bBox = assetController.getBBox(asset);
    	var assetNode = asset.node();
    	if(assetNode.nodeName == 'use'){
    		d3.select(assetNode.parentNode)
	    			.append("rect")
	    			.attr("id", assetId + "-rect")
	    			.attr("x", bBox.x)
	    			.attr("y", bBox.y)
	    			.attr("width",  bBox.width)
	    			.attr("height",  bBox.height)
	    			.attr("transform", d3.select(assetNode).attr("transform"))
	    			.transition()
	        		.duration(100)
	        		.ease('linear')
	        		.style("fill", "#CCDEF1")
	        		.style("stroke", "#steelblue")
	        		.style("opacity", "1")
	        		.each("end", function(){
	     	            d3.select(this).remove();
	                    var params = {};
	        			params['assetId'] = assetController.retrieveValidAssetId(assetId);
	        			params['svgId'] = asset.farthestViewportElement.id;
	        			params['assetType'] = eventHandler['assetType'];
	     	            eventHandler.handler(params, eventController.drawingController);
	        		});
    	} else{
        	var clone = d3.select(asset.node().cloneNode(true));
        	clone
    		.style("transform-origin", "50% 50%")
    		.style("fill", "#fff")
    		.style("fill", "#CCDEF1")
    		.style("stroke", "steelblue")
    		.style("stroke-width", "0.5%")
    		.style("opacity", "1")
    		.transition()
    		.duration(100)
    		.ease('linear')
    		.style("fill", "#CCDEF1")
    		.style("stroke", "#005CB8")
    		.each("end", function(){
 	            d3.select(this).remove();
                var params = {};
    			params['assetId'] = assetController.retrieveValidAssetId(assetId);
    			params['svgId'] = asset.farthestViewportElement.id;
    			params['assetType'] = eventHandler['assetType'];
 	            eventHandler.handler(params, eventController.drawingController);
    		});
        	asset.node().parentNode.appendChild(clone.node());
    	}
    },

    /**
     * Adds JS event to labels.
     * 
      * @param svg SVG
     * @param self EventController
     * @param eventHandler event handler object
     * @param highlightOnly true if only add event to highlighted labels, false for all assets.
    */
    addEvent2Labels: function (svg, self, eventHandler, highlightOnly) {
        var eventName = eventHandler.eventName;
        var prefix = 'l-' + eventHandler.assetType + '-';
        var labels = d3.select("#" + eventHandler.assetType + "-labels")

        if (this.isNativeAndroid()) {
            labels.selectAll("g")
            .each(function () {
                self.addEvent2LabelAndroid(svg, this, prefix, eventHandler, eventName, highlightOnly);
            });
        } else {
            labels.selectAll("g")
            .each(function () {
                self.addEvent2Label(svg, this, prefix, eventHandler, eventName, highlightOnly);
            });
        }
    },

    /**
     * Adds JS event to highlighted labels.
     * 
     * @param self EventController
     * @param eventHandler event handler object
     */
    addEvent2HighlightedLabels: function (svg, self, eventHandler) {
    	this.addEvent2Labels(svg, self, eventHandler, true);
    },
    
    /**
     * Adds JS event to individual label.
     * 
     * @param svg SVG
     * @param self EventController
     * @param prefix String  The asset type, which is typically the table name, such as 'rm'
     * @param eventHandler event handler object
     * @param eventName String name of the event.
     * @param highlightOnly true if only add event to highlighted label, false for both highlight or non-hihglighted labels.
     */
    addEvent2Label: function (svg, self, prefix, eventHandler, eventName, highlightOnly) {
        var eventController = this;
        var assetLabelId = self.id;
        var assetLabel = d3.select(self);
        var assetLabelNode = assetLabel.node();
        var svgId = svg.node().id;
        var isValid = true;
        if(highlightOnly)
        	isValid = (assetLabel.attr('highlighted') === 'true');
        
        //check if the asset is highlighted
        if (isValid) {
        	var assetController = this.drawingController.getController("AssetController");
        	
        	assetLabelNode.addEventListener(eventName, function (event) {
            	event.stopPropagation();

            	if(eventName === 'contextmenu'){
                	//stop showing browser's context menu
                	event.preventDefault();
                }else if(eventName === 'mouseover'){
            		this.style.cursor = 'pointer';
            	}else if(eventName === 'mouseout'){
            		this.style.cursor = 'all-scroll' ;
            	}
            	
            	if (eventHandler.handler) {
                	var assetId = assetLabelId.substring(assetLabelId.indexOf(prefix) + prefix.length);
                    var params = {};
        			params['assetId'] = assetController.retrieveValidAssetId(assetId);
        			params['svgId'] = svgId;
        			params['assetType'] = eventHandler['assetType'];
     	            eventHandler.handler(params, eventController.drawingController, event);
                }
            });
        }
    },

    /**
     * Adds JS event to highlighted individual label.
     * 
     * @param self EventController
     * @param prefix String  The asset type, which is typically the table name, such as 'rm'
     * @param eventHandler event handler object
     * @param eventName String name of the event (i.e. 'click').
     */
    addEvent2HighlightedLabel: function (self, prefix, eventHandler, eventName) {
    	this.addEvent2Label(self, prefix, eventHandler, eventName, true);
    },

    /**
     * Adds JS event to individual label for Android.
     * 
     * @param self EventController
     * @param prefix String  The asset type, which is typically the table name, such as 'rm'
     * @param eventHandler event handler object
     * @param eventName String name of the event (i.e. 'click').
     */
    addEvent2LabelAndroid: function (svg, self, prefix, eventHandler, eventName, highlightOnly) {
        var eventController = this;
        var assetLabelId = self.id;
        var assetLabel = d3.select(self);
        var assetLabelNode = assetLabel.node();
        var objectMoved = null;
        var svgId = svg.node().id;
        
        var isValid = true;
      
        //check if the asset is highlighted
        if(highlightOnly)
        	isValid = (assetLabel.attr('highlighted') === 'true');
        
        if (isValid) {
            assetLabelNode.addEventListener("touchstart", function () {
                objectMoved = null;
            });
            assetLabelNode.addEventListener("touchmove", function (event) {
                objectMoved = event.target;
            });
            var assetController = this.drawingController.getController("AssetController");
            assetLabelNode.addEventListener('touchend', function (event) {
                event.stopPropagation();

                if (event.touches && event.touches.length > 2) {
                    return;
                }
                if (objectMoved && event.target === objectMoved) {
                    return;
                }

                if (eventHandler.handler) {
                    var assetId = assetLabelId.substring(assetLabelId.indexOf(prefix) + prefix.length);
                    var asset = assetController.getAssetById(svgId, assetId);
                    var params = {};
        			params['assetId'] = assetController.retrieveValidAssetId(assetId);
        			params['svgId'] = svgId;
        			params['assetType'] = eventHandler['assetType'];
     	            eventHandler.handler(params, eventController.drawingController);
                }
            });
        }
    },

    /**
     * TODO: check which functions call it?????
     * Adds JS event to highlighted individual label for Android.
     * 
     * @param self EventController
     * @param prefix String  The asset type, which is typically the table name, such as 'rm'
     * @param eventHandler event handler object
     * @param eventName String name of the event (i.e. 'click').
     */
    addEvent2HighlightedLabelAndroid: function (svg, self, prefix, eventHandler, eventName) {
    	this.addEvent2LabelAndroid(svg, self, prefix, eventHandler, eventName, true);
    },

    /**
     * Stop propagation and prevent default
     */
    stopPropagation: function(){
        d3.event.stopPropagation();
        d3.event.preventDefault();
    },
    
    /**
     * Check if the iOS is Android.
     * 
     * @return boolean true if Android, false otherwise.
     */
    isNativeAndroid: function () {
        var nua = navigator.userAgent;
        var isAndroid = ((nua.indexOf('Mozilla/5.0') > -1 && nua.indexOf('Android ') > -1 && nua.indexOf('AppleWebKit') > -1) && !(nua.indexOf('Chrome') > -1));
        return isAndroid;
    }
}, {});