/**
 * Controller to handle markup functionality such as load, save etc.
 * 
 */
MarkupController = Base.extend({

	/**
	 * is there any change to drawing that are not saved to database?
	 */
	hasUnsavedChanges: false,
	
	/**
	 * Configuration object that contains asset's drawing properties, such as divId, drawingName etc.
	 */
	config: {},

	/**
	 * reference to drawingController
	 */
	drawingController: null,

	/**
	 * reference to MarkupDataController
	 */
	markupDataController: null,
	
	/**
	 * reference to MarkupControl
	 */
	markupControl: null,
	
	// @begin_translatable
    z_MESSAGE_ERROR_SAVE: 'Error saving redline record to db.',
    // @end_translatable
    
	/**
	 * Constructor.
	 */
	constructor: function(config){
		this.config = config;	
		
		if(!this.config.redlineTypes || this.config.redlineTypes.length > 0){
			this.config.redlineTypes = [ "cloud", "line", "textbox", "arrow" , "area", "swatch", "arrowTextbox", "textOnly"];
		}
	},
	
	/**
     * set reference to drawingController
     */
    setDrawingController: function(drawingController){
    	this.drawingController = drawingController;
    	this.markupDataController = drawingController.getController("MarkupDataController");
    },
    
    /**
     * Loads markups for the specified activity_log action item into SVG drawing, if any.
     * 
     * @parameters Map the parameters to retrieve from afm_redline table. 
     * 					The possible keys are: activityLogId (required)
     *                                         dwgName (required)
     *                                         layerName (optional)
     */
    load: function(parameters, markupControl){

    	var activityLogId = (parameters && parameters.activityLogId ? parameters.activityLogId : '');
		// no activity log id provided.
    	if(!activityLogId)
    		return;
    	
    	if(markupControl)
    		this.markupControl = markupControl;
    	
    	// retrieved the saved svg drawing config and download redlines if any
		var record = this.markupDataController.getLastRedlineRecord(parameters);
		if(record && record.getValue("afm_redlines.extents_lux") != null &&  record.getValue("afm_redlines.extents_lux") != ''){
			var viewBox = [];
			
			viewBox[0] = record.getValue("afm_redlines.extents_lux");
			viewBox[1] = record.getValue("afm_redlines.extents_luy");
			viewBox[2] = record.getValue("afm_redlines.extents_rlx");
			viewBox[3] = record.getValue("afm_redlines.extents_rly");
			
			// this will also loads markups
			var html5_redlines = record.getValue("afm_redlines.html5_redlines");
			if(html5_redlines){
				var auto_number = record.getValue("afm_redlines.auto_number");
				this.loadMarkups(auto_number);
				
			}

			var svg = this.getSvg(this.config.drawingName);
			svg.attr("viewBox", viewBox); 
        }
    	
    },
   
    /**
	 * Downloads markups string from afm_redlines table for the specified auto number then add to the svg drawing control
	 * 
	 * @autoNumber Integer primary key vaue afm_redlines.auto_number 
	 */
	loadMarkups: function(autoNumber){

		var fileName = "afm_redlines-" + autoNumber + "-html5_redlines.doc";
		var keys = {'auto_number': autoNumber};
		var parameters = { "tableName": "afm_redlines",
				   "fieldName": "html5_redlines",
				   "documentName": fileName
				  };
		var control = this;
		DrawingSvgService.checkOut(keys, parameters, {
		        callback: function(redmarks) {
		        	if(redmarks){
		        		control.addMarkups(redmarks);
		        	}
		        },
		        errorHandler: function(m, e) {
		            Ab.view.View.showException(e);
		        }
		});
	},
	
   /**
    * add markups to the svg.
    * @param redmarks String. A string that contains all markup nodes from svg drawing. 
    */
	addMarkups: function(redmarks){
	   var svg = d3.select("#" + this.config.divId + "-0-0").select("svg");
		var redlineTarget = this.createGroupIfNotExists(svg.selectAll("#viewer"), "redlines", "redlines").node(); 
		
		// 3048470 - innerHTML does not work in IE for SVG element
		//redlineTarget.innerHTML = redmarks;
		var doc = new DOMParser().parseFromString(redmarks, 'application/xml');
		for (var i = 0; i < doc.documentElement.childNodes.length; i++) {
			var childNode = doc.documentElement.childNodes[i];
			redlineTarget.appendChild(redlineTarget.ownerDocument.importNode(childNode, true));
		}
				
		this.attachRedlineEvents();
   },
   
   /**
    *  Create asset group if not exists
    */
   createGroupIfNotExists: function (parentNode, id, className) {
       var group = parentNode.selectAll("#" + id);
       if(group.empty()) {
           group = parentNode.append("g")
               .attr("id", id)
               .attr("class", className);
       }
       return group;
   },
   
   /**
    * Attaches click events to all the existing redlines.
    */
   attachRedlineEvents: function(){
	   	 
	   	var control = this;
	   	
	   	var stopDefault = function() {
           d3.event.stopPropagation();
           if (d3.event.defaultPrevented) {
               return;
           }
       };
       
	   	var redlines = d3.select("#" + this.config.divId + "-0-0").selectAll("#redlines").selectAll(".dropped");
        redlines.each(function(){
	   		 	 for (var j = 0; j < control.config.redlineTypes.length; ++j) {
	   		 		  var type = control.config.redlineTypes[j];
	   		 		  var fn = control.markupControl.onChange.createDelegate(this, [d3.select(this).node(), control.markupControl], true);
	   		 		  var dragMove = placement.move(fn);
	   		 		  if (d3.select(this).classed(type)) {
			              d3.select(this).call(redline.attachEvent, [type, false, control.markupControl.config.currentFontSize, fn, control.markupControl.config.showAreaLabel, control.markupControl.config.unitsFactor, true])
			              	.on("click", stopDefault)
			              	.on("touchstart.redline-edit", stopDefault);
				          d3.select(this.parentNode).call(dragMove)
				             	.on("click", stopDefault)
				              	.on("touchstart.redline-edit", stopDefault);
			   		  }
	   		 	 }
		 });
   },
   
	/**
     * Saves SVG drawing's extent, drawing name, action item id to afm_redlines table.
     * Save redlines content to its document table and update the related file name in afm_redlines table.
     * Capture the floor plan with redlines, save to activity_log.doc4 field. 
     * 
     * @param parameters parameters needed for saving the drawing, such as activity log id etc.
     * @updateIfExist boolean true if update for the existing record, false will always add a new record.
     */
    save: function(parameters, updateIfExists){
    	
    	
		var activityLogId = (parameters && parameters.activityLogId ? parameters.activityLogId : '');
		// no activity log id provided.
    	if(!activityLogId)
    		return null;
    	
    	
    	var drawingName = (parameters.drawingName ? parameters.drawingName : '');
    	var bl_id = parameters.pKeyValues['bl_id'];
    	var fl_id = parameters.pKeyValues['fl_id'];
    	if(!drawingName && bl_id && fl_id){
    		parameters.drawingName = this.markupDataController.retrieveDrawingName(bl_id, fl_id);
        }
    	
		if(!drawingName)
			return null;
		
		var savedRecord = this.markupDataController.saveRecordToRedlineTable(parameters, updateIfExists);
   		try{
           	if(savedRecord){
            	var redmarksSvg = this.retrieveMarkups(drawingName);
        		if(redmarksSvg){
        			var autoNumber = savedRecord.getValue("afm_redlines.auto_number");
        			this.markupDataController.saveMarkupsToDb(redmarksSvg, autoNumber);
        		}
        	}

        	//capture image and save it to the activity_log table
           	var imageCapture = new DrawingImageCapture();
           	var control = this;
            imageCapture.captureImage(this.config.divId, false, control.saveCapturedImageToDb.createDelegate(control, [activityLogId], 2));
            
        	var params = {};
        	params['activityLogId'] = activityLogId;
        	params['autoNumber'] = autoNumber;
            return params;

    	} catch(e){
       		View.showMessage('error', View.getLocalizedString(this.z_MESSAGE_ERROR_SAVE), e.message, e.data);
    	}
    	
    },
    
    getSvg: function(drawingName){
    	var svg;
    	if(drawingName){
    	   svg = d3.selectAll("#" + DrawingCommon.retrieveValidSvgId(this.config.divId, drawingName));
    	} else {
    	   svg = d3.selectAll("#" + this.config.divId + "-0-0").select("svg");
    	}
    	return svg;
    },
    
    /**
     * Return all the markups from SVG as a string. Include styles when necessary.
     * 
     * @returns String    a string that contains all redline SVG elements.
     */
    retrieveMarkups: function(drawingName){
 	   
    	var svg = this.getSvg(drawingName);
    	var redlines = svg.selectAll("#redlines").selectAll(".dropped");
 	   	redlines.selectAll('.edit').remove();
        
 	   	var redmarks = '';
 	   	var control = this;
        redlines.each( function(){
     	   var clone = d3.select(this).node().cloneNode(true);

     	   // the grip will be removed
     	  control.removeGrips(clone);
     	   
           redmarks += "<g>" + control.serializeXmlNode(clone) + "</g>";
        });
        
 	    //3048470 - save the redlines as a svg to process later as XML in order to append well in IE
        if(redmarks){
     	   redmarks = "<svg xmlns=\"http://www.w3.org/2000/svg\">" + redmarks + "</svg>";
        }
        return redmarks;
    },
    
	  /**
	   * Captures the screen and save the image to activity_log table's doc4 field.
	   */
	  saveCapturedImageToDb: function(imageContent, activityLogId){

		if( !imageContent)
			return;

		var keys = {'activity_log_id': activityLogId};
		var fileName = "activity_log-" + activityLogId + "-doc4.png";
  	
		var datasource = this.markupDataController.datasources["activity_log"];
	  	var parameters = { "tableName": "activity_log",
	  					   "fieldName": "doc4",
	  					   "documentName": fileName
	  					  };
	  	
	  	//get the data after data:image\/png;base64,
	  	var imageDataBytes = imageContent.substr(imageContent.indexOf(",")+1);
	  	
	  	var control = this;
	  	DrawingSvgService.checkin(imageDataBytes, keys, parameters, {
	   	   	callback: function() {
	   	   		var record = datasource.getRecord("activity_log_id =" + activityLogId);
	   	   		if(record){
	   	   			record.setValue('activity_log.doc4', fileName);
	   	   			datasource.saveRecord(record);
	   	   			
	   	   			// save success
	   	   			control.hasUnsavedChanges = false;

	   	   		}
	   	   		
	   	   		return true;
	   	   	},
	   	    errorHandler: function(m, e){
	   	        Ab.view.View.showException(e);
	   	    }
	   	});
	  },
	  
	  /**
	   * Detect is modern browser
	   * @returns {boolean}
	   */
	  isModernBrowser: function() {
	      return (DrawingCommon.isIE () > 9 || !DrawingCommon.isIE());
	  },

	  serializeXmlNode: function(xmlNode) {
	      if (typeof window.XMLSerializer !== "undefined") {
	          return (new window.XMLSerializer()).serializeToString(xmlNode);
	      } else if (typeof xmlNode.xml !== "undefined") {
	          return xmlNode.xml;
	      }
	      return "";
	  },

	  /**
	   * remove grips for the specified node
	   */
	  removeGrips: function(node) {
	      d3.select(node).selectAll('.edit').each( function() {
	          d3.select(this).remove();
	      });
	  }
}, {});