/*
 * Main controller for add-on components.
 * 
 * Supported add-on components are:
 * 		LayersPopup
 * 		InfoBar
 * 		DatasourceSelector
 * 		NavigationToolbar
 * 		AssetLocator
 * 		AssetTooltip
 * 		Cluster
 * 		Marker
 */
AddOnsController = Base.extend({
	/**
	 * Add-on config, varies based on the add-on type.
	 */
	config: {},
	
	/**
	 * A map of ids and their corresponding add-ons.
	 */
	registeredAddOns: {},
	
	/**
	 * reference to drawingController
	 */
	drawingController: null,
	
	/**
	 * Constructor. Initializes the drawing controller and registered add-ons map.
	 */
	constructor:  function(config, drawingController){
		this.config = config;
		this.drawingController = drawingController;
		
		this.registeredAddOns = {};
	},
	
	/**
	 * Retrieves add-on for the specified add-on id
	 * 
	 * @param addOnId String the add-on id
	 */
	getAddOn: function(addOnId){
		return this.registeredAddOns[addOnId];
	},
	
	setAddOns: function(addOnsConfig){
		for (var key in addOnsConfig) {
			// layer popup will be registered by SVG
			if (addOnsConfig.hasOwnProperty(key)){
			  this.config.addOnsConfig[key] = addOnsConfig[key];
			  this.registerAddOn(key);
		  }
		}
		
		if(DrawingCommon.toBoolean(this.config.showTooltip) && !this.getAddOn('AssetTooltip')){
			//default  AssetTooltip object if showTooltip=true
			var assetType = (this.config.assetType) ? this.config.assetType : 'rm';
			this.registerAddOn('AssetTooltip', {handlers: [{assetType: assetType}]})
		}
			
		if((this.config.multipleSelectionEnabled !== 'false' || this.config.zoomWindowEnabled !== 'false') && !this.getAddOn('SelectWindow'))
			this.registerDefaultAddOn('SelectWindow');
	},
	
	registerDefaultAddOn: function(addOnId){
		var addOn = this.getAddOnConstructorById(addOnId);

		if (typeof (addOn) !== 'undefined' && addOn != null) {
			var registeredAddOn = registeredAddOn = new addOn(this.config);
				
			//set drawing controller if setDrawingController() is defined in add on class.
			if(typeof registeredAddOn.setDrawingController === 'function') {
				registeredAddOn.setDrawingController(this.drawingController);
	        }
			
			this.registeredAddOns[addOnId] = registeredAddOn;
		}
	},
	
	registerAddOn: function(addOnId, addOnConfig){
		var addOn = this.getAddOnConstructorById(addOnId);
		if(addOnConfig === undefined){
			addOnConfig =  (this.config.addOnsConfig ? this.config.addOnsConfig[addOnId] : null);
		}

		if (typeof (addOn) !== 'undefined' && addOn != null && typeof (addOnConfig) !== 'undefined' && addOnConfig != null){
			
			// register markup controller only before markup add-on is registered. 
			if(addOnId === 'MarkupControl'){
				this.drawingController.registerController('MarkupDataController');
				this.drawingController.registerController('MarkupController');
			}
			
			var registeredAddOn = new addOn(this.propagateConfig(addOnId, addOnConfig));
				
			//set drawing controller if setDrawingController() is defined in add on class.
			if(typeof registeredAddOn.setDrawingController === 'function') {
				registeredAddOn.setDrawingController(this.drawingController);
	        }
			
			this.registeredAddOns[addOnId] = registeredAddOn;
		}
	},
	
    getAddOnConstructorById: function(addOnId){
    	var addOnConstructor = null;
    	
    	switch(addOnId){
    		case 'LayersPopup':
    			addOnConstructor = Drawing.view.LayersPopup;
    			break;

    		case 'InfoBar':
    			addOnConstructor = Drawing.view.InfoBar;
    			break;

    		case 'InfoWindow':
    			addOnConstructor = Drawing.view.InfoWindow;
    			break;

    		case 'DatasourceSelector':
    			addOnConstructor = Drawing.view.DatasourceSelector;
    			break;
 	
    		case 'AssetLocator':
    			addOnConstructor = Drawing.view.AssetLocator;
    			break;

    		case 'AssetTooltip':
    			addOnConstructor = Drawing.view.AssetTooltip;
    			break;
    			
    		case 'NavigationToolbar':
    			addOnConstructor = Drawing.view.NavigationToolbar;
    			break;
	
    		case 'SelectWindow':
    			addOnConstructor = Drawing.view.SelectWindow;
    			break;

    		case 'Cluster':
    			addOnConstructor = ClusterControl;
    			break;
    		
    		case 'Marker':
    			addOnConstructor = Drawing.controllers.MarkerController;
    			break;
    			
    		case 'AssetPanel':
    			addOnConstructor = Drawing.view.AssetPanel;
    			break;
    			
    		case 'MarkupControl':
    			addOnConstructor = Drawing.view.MarkupControl;
    			break;
    	}
    	
    	return addOnConstructor;
    },
    
    
	/**
	 * adds drawing control's root config to the addOnConfig
	 */
	propagateConfig: function(addOnId, addOnConfig){
		if(addOnId === 'DatasourceSelector'){
			return this.config;
		} else {
			return this.cloneConfig(addOnId, addOnConfig);
		}
	},
	

	/**
	 * copy config's base parameter to the addOnConfig for the add-on component.
	 */
	cloneConfig: function(addOnId, addOnConfig){
		
		if(typeof addOnConfig === 'undefined' || !addOnConfig){
			addOnConfig = {};
		}
		
		for (var key in this.config) {
			if (this.config.hasOwnProperty(key) && (typeof key !== 'function') && (key !== 'addOnsConfig') && !(typeof (addOnConfig[key]) !== 'undefined' && addOnConfig[key] != null)) {
				addOnConfig[key] = this.config[key];
			}
		}
		return addOnConfig;
	},
	
	getAddOn: function(addOnId){
		return this.registeredAddOns[addOnId];
	},
	
	removeAddOn: function(addOnId) {
		if(this.registeredAddOns[addOnId].clear){
			this.registeredAddOns[addOnId].clear();
		}
        delete this.registeredAddOns[addOnId];  
    }
}, {});