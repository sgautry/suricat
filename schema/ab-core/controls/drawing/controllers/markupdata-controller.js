/***
 * Defines the datasource and related query for markup functionality.
 * Provides the APi for application level codes to retrieve records from afm_redlines or activity_log table.
 * 
 * Used by MarkupController and MarkupControl, as well as application views.
 *
 * @author   Ying Qin
 * @version  23.2
 * @date     1/2017
 */
var MarkupDataController = Base.extend({
	
	/**
	 * datasources used by markup control
	 */
	datasources: {},
	
	/**
	 * constructor.
	 */
	constructor: function(config){
		this.config = config;
		
		this.createDatasources();
	},
	
	/**
	 * defines all datasources related with markup control including:
	 * - afm_redlines
	 * - afm_dwgs
	 * - activity_log
	 */
	createDatasources: function(){
    	this.datasources['afm_redlines'] = Ab.data.createDataSourceForFields({
	        id: 'ds_drawingControl_afmRedlines',
	        tableNames: ['afm_redlines'],
	        fieldNames: ['afm_redlines.dwg_name','afm_redlines.activity_log_id', 'afm_redlines.origin','afm_redlines.highlight_defs',
	                     'afm_redlines.image_file','afm_redlines.html5_redlines','afm_redlines.extents_lux','afm_redlines.extents_luy',
	                     'afm_redlines.extents_rlx','afm_redlines.extents_rly','afm_redlines.redlines','afm_redlines.user_name',
	                     'afm_redlines.auto_number', 'afm_redlines.layer_name']
	    });


    	this.datasources['afm_dwgs'] = Ab.data.createDataSourceForFields({
    	        id: 'ds_drawingControl_afmDwgs',
    	        tableNames: ['afm_dwgs'],
    	        fieldNames: ['afm_dwgs.dwg_name','afm_dwgs.space_hier_field_values']
    	});
    	
    	this.datasources['activity_log'] = Ab.data.createDataSourceForFields({
	        id: 'ds_drawingControl_activityLog',
	        tableNames: ['activity_log'],
	        fieldNames: ['activity_log.activity_log_id','activity_log.doc4', 'activity_log.project_id', 'activity_log.bl_id', 'activity_log.fl_id', 
	                     'activity_log.created_by', 'activity_log.activity_type', 'activity_log.action_title', 'activity_log.description', 'activity_log.status']
		});
    },
    
    /**
     * Retrieves drawing name from afm_dwgs table for specified buildingId and floorId.
     * @param buildingId building code
     * @param floorId floor code
     */
    retrieveDrawingName: function(buildingId, floorId){
        
    	var drawingName = '';
    	
    	if(buildingId && floorId){
    		var restriction = new Ab.view.Restriction();
    		restriction.addClause("afm_dwgs.space_hier_field_values", buildingId + ";" + floorId);
        	var records = this.getRecords('afm_dwgs', restriction);
        	if(records!=null && records.length > 0 && records[0]!=null){
        		drawingName = records[0].getValue("afm_dwgs.dwg_name");
        	}
    	}

    	return drawingName;
    },

    /**
     * Retrieves record(s) for specified table, restriction and/or parameters
     * @param tableName the table name
     * @param restrictions a set of restrcition(s)
     * @param parameters orther parameters such as sortField.
     */
    getRecords: function(tableName, restrictions, parameters){    	
    	if(parameters)
    		return this.datasources[tableName].getRecords(restrictions, parameters);
    	else
    		return this.datasources[tableName].getRecords(restrictions);
    },
    
	/**
	 * Saves markups to database afm_redlines.html5_redlines doc field of the specified auto_numeber record.
	 * @redmarks String redmarks to save
	 * @autoNumber Integer the specified auto_numeber record
	 */
    saveMarkupsToDb: function(redmarks, autoNumber){

		var fileName = "afm_redlines-" + autoNumber + "-html5_redlines.doc";
	  	var keys = {'auto_number': autoNumber};
		
	  	var parameters = { "tableName": "afm_redlines",
	  					   "fieldName": "html5_redlines",
	  					   "documentName": fileName
	  					  };
	  	
	  	var datasource = this.datasources["afm_redlines"];
	  	DrawingSvgService.checkin(redmarks, keys, parameters, {
	   	   	callback: function() {
	   	   		var record = datasource.getRecord("auto_number =" + autoNumber);
	   	   		if(record){
	   	   			record.setValue('afm_redlines.html5_redlines', fileName);
	   	   			datasource.saveRecord(record);
	   	   		}
	   	   		return true;
	   	   	},
	   	    errorHandler: function(m, e){
	   	        Ab.view.View.showException(e);
	   	    }
	   	});
  	},
	
    /**
     * Retrieves the last redline record for specified parameters.
     * @param Map a map of parameters including activityLogId(required), drawingName(required), layerName(optional).
     */
    getLastRedlineRecord: function(params){
		var record = null;
    	var parameters = {"afm_redlines.activity_log_id": params.activityLogId,
    					  "afm_redlines.dwg_name": params.drawingName
    					};
    	
    	var restriction = null;
    	if(params.layerName && params.layerName.length > 0){
    		parameters['afm_redlines.layer_name'] = params.layerName;
    	} else {
   			restriction = new Ab.view.Restriction();
    		restriction.addClause('afm_redlines.layer_name', '', 'IS NULL', 'AND'); 
    	}
    	var sortParameters = {
				sortValues: [{'fieldName':'afm_redlines.auto_number', 'sortOrder':-1}]
		};
		
    	var records = this.retrieveRecordsFromTable('afm_redlines', parameters, restriction, sortParameters);
		if (records.length > 0){
			//retrieve the most recent record
			record = records[0];
		}
		
		return record;
    }, 

    
    /**
     * save non-document field and value pair from parameters to afm_redlines table.
     * @parameter Map non-document fields and values pair, including: drawingName(required),  viewBox(array, required), activityLogId(required), layerName(optional)
     * @updateIfExists Boolean If true, will update the exisiting record if any, if false, will always create a new record.
     * 
     * @return the saved record with field names/values
     */
    saveRecordToRedlineTable: function(parameters, updateIfExists){
    	var params = {'afm_redlines.dwg_name': parameters.drawingName,
	       	 'afm_redlines.origin': 'HTML5-based Floor Plan',
	       	 'afm_redlines.extents_lux': parseInt(parameters.viewBox[0]),
	       	 'afm_redlines.extents_luy': parseInt(parameters.viewBox[1]),
	       	 'afm_redlines.extents_rlx': parseInt(parameters.viewBox[2]),
	       	 'afm_redlines.extents_rly': parseInt(parameters.viewBox[3]),
	       	 'afm_redlines.extents_rly': parseInt(parameters.viewBox[3]),
	       	 'afm_redlines.activity_log_id': parameters.activityLogId,
	       	 'afm_redlines.user_name': View.user.name
	       	};

    	if(parameters.layerName && parameters.layerName.length > 0){
    		params['afm_redlines.layer_name'] = parameters.layerName;
    	}
    	var sortParameters = {
				sortValues: [{'fieldName':'afm_redlines.auto_number', 'sortOrder':-1}]
		};
    	
    	return this.saveRecordToTable('afm_redlines', params, sortParameters, updateIfExists);
    },
    
    /**
     * Retrieves the record with the specified values and restrictions from the specified table.
     * 
     * @tableName String the table to retrieve record from.
     * @parameters Map of key and value pair to restrict and retrieve.
     * @restriction additional restriction to restrict
     * @sortParamter Map of the sort field
     * @return retrieved records
     */
    retrieveRecordsFromTable: function(tableName, parameters, restriction, sortParameters){
    	var restrictions = new Ab.view.Restriction();
    	for(var key in parameters){
        	restrictions.addClause(key, parameters[key]);
    	}
    	if(restriction){
    		restrictions.addClauses(restriction);
    	}
       	return this.getRecords(tableName, restrictions, sortParameters);
    	
    },
    
    /**
     * Saves the record with the specified values from parameters to the specified table.
     * 
     * @tableName String the table to save record to.
     * @parameters Map of key and value pair to save to the table.
     * @sortParamter Map of the sort field
     * @updateIfExist boolean true if update for the existing record, false will always add a new record.
     */
    saveRecordToTable: function(tableName, parameters, sortParameters, updateIfExists){
    	
    	var restriction = null;
    	if(tableName === 'afm_redlines' && !parameters.layerName || parameters.layerName < 1 ){
    		restriction = new Ab.view.Restriction();
    		restriction.addClause('afm_redlines.layer_name', '', 'IS NULL', 'AND'); 
    	}
    	
    	var retrievedRecord = this.retrieveRecordsFromTable(tableName, parameters, restriction, sortParameters);
    	if(retrievedRecord && retrievedRecord.length > 0 && updateIfExists){
    		return retrievedRecord[0];
    	}
            
    	var record = new Ab.data.Record();
  		record.isNew = true;
    	
    	for(var key in parameters){
    		record.setValue(key, parameters[key]);
    	}
    	
    	return this.datasources[tableName].saveRecord(record);
    }
});

