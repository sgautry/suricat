/**
 * Defines a set of SVG drawings within the layout.
 * 
 * @author Ying Qin
 */
var DrawingsBase = Base.extend({

	/**
	 * Configuration object
	 */
	config: {},
	
	/**
	 * A map of drawing name and drawing controls (DrawingBase object) in layout
	 */
	drawings: {},
	
	/**
	 * Reference to DrawingController
	 */
	drawingController: null,
	
	/**
     * Constructor.
     *
     * @param divId String root Id of <div/> that holds all the svg drawings
     * @param config configObject
     */
    constructor: function(divId, config) {
        this.inherit(divId, 'html', config);
        
        this.config = config;
        this.config.divId = divId;
        
        this.drawings = {};
    },
    
	/**
     * set reference to drawingController
     */
	setDrawingController: function(drawingController){
	   	this.drawingController = drawingController;
	},
	
	/**
     * Removes the existing SVG drawing from layout, retrieves the drawing SVG from server and displays it.
     * 
     * @param parameters    Object server-side required info such as as bl_id, fl_id, drawingName to get and process corresponding SVG
     *                      
     * @return DrawingBase  the SVG drawing loaded
     */
    load: function(parameters){

    	 // drawing name is required
		 var drawingName = parameters["drawingName"];
		 if(!drawingName)
			 return null;
		 
		 var spinner = new Spinner().spin(document.getElementById(this.config.divId));
		 
	     var dwgConfig = {};
		 for(var key in parameters){
			 if(typeof key == 'function')
				 continue;
				 
			 if(key=='events'){
				 if(this.config.events){
					 dwgConfig.events = this.config.events.concat(parameters.events);
				 } else {
					 dwgConfig.events = parameters.events;
				 }
			 } else {
				 dwgConfig[key] = parameters[key];
			 }
		 }
		 
		 //remove the existing drawings if needed
		 if(this.config.allowMultipleDrawings === 'false'){
			 this.unloadAll();
		 } else {
			 this.unload(parameters); 
		 }
		 		 
		 var drawing = new DrawingBase(dwgConfig, this.drawingController);
		 drawing.load();
		
		 this.drawings[drawingName] = drawing;

		 this.drawingController.updatedDrawingsLoaded();

		 spinner.stop();

		 return drawing;
	},

	/**
     * Removes the specified SVG drawing from layout.
     * 
     * @param parameters    Object required info such as as bl_id, fl_id, drawing name to locate the corresponding SVG
     */
    unload: function(parameters){
		 var drawingName = parameters['drawingName'];
		 if(!drawingName)
			 return null;

		 var drawing = this.drawings[drawingName];
		 if(drawing){
			 
			 drawing.config.drawingName = drawingName;
			 drawing.config.pkeyValues = parameters['pkeyValues'];

			 drawing.unload();
			 delete this.drawings[drawingName];
			 
			 //reset config to the last drawing still on display
			 var lastDrawingName = Object.keys(this.drawings)[Object.keys(this.drawings).length - 1];
			 if(lastDrawingName){
				  	// update control's config
			    	this.config.drawingName = lastDrawingName;
			    	this.drawingController.config.drawingName = lastDrawingName;
			 }
		 }

		 this.drawingController.updatedDrawingsLoaded();
    },

    /**
     * Removes all loaded drawings.
     */
    unloadAll: function(){
    	for(var drawingName in this.drawings){
        	var drawing = this.drawings[drawingName];
        	if(drawing){
        		drawing.unload();
        		delete this.drawings[drawingName];
        	}
    	}
    	this.drawingController.updatedDrawingsLoaded();
    },
    
    /**
     * Replaces an existing SVG with a new drawing.
 	 *
     * @param parameters    Object server-side required info such as as bl_id, fl_id, drawing name to get and process corresponding SVG
     */
    reload: function(parameters){
		 var spinner = new Spinner().spin(document.getElementById(this.config.divId));
		 if(parameters && parameters.drawingName){
			 delete parameters.drawingName;
		 }
		 var scope = this;
		 _.each(scope.drawings, function(drawing, drawingName){
			 Ext.apply(drawing.config, parameters)
			 drawing.load();
		 });
	    
		 this.drawingController.updatedDrawingsLoaded();
		 
		 spinner.stop();
    },
    
    /**
     * Returns the viewBox based on <div/> id and drawing name
     * 
     * @param divId root div id
     * @param drawingName the drawing name
     * 
     * @return viewBox Array
     */
    getViewBox: function (divId, drawingName) {
        var viewBox = [];
    	var svgNode = null;
    	
    	if(drawingName){
	    	var svgId = DrawingCommon.retrieveValidSvgId(divId, drawingName);
	    	svgNode = d3.select("#" + svgId);
    	} else {
    		//if no drawing provide, use the first drawing
    		svgNode = d3.select("#" + divId + "-0-0").select("svg");
    	}
	    
    	if(svgNode){
    		viewBox = svgNode.attr("viewBox").split(" ");
	        if (viewBox.length === 1) {
	            viewBox = svgNode.attr("viewBox").split(",");
	        }
	        for (var i = 0; i < viewBox.length; i++) {
	            viewBox[i] = Number(viewBox[i]);
	        }
    	}
        return viewBox;
    },
    

    
    /**
     * Retrieves a DrawingBase object for the specified drawing name.
     * 
     * @param drawingName String the specified drawing name
     * @return DrawingBase  the SVG drawing object
     */
    getDrawingByName: function(drawingName){
    	var drawing = this.drawings[drawingName];
    	
    	if(drawing)
    		return drawing;
    	else
    		return null;
    	
    },
    
    /**
     * Retrieves the SVG selection for the specified drawing name
     * 
     * @return object SVGSVGElement
     */
    getSvgByDrawingName: function(drawingName) {
    	return d3.select("#" + DrawingCommon.retrieveValidSvgId(this.config.divId, drawingName));
    },

    /**
	 * Change the Label font size by the specified factor, the values will be between 0.2 (min) and 5 (max).
	 * 
	 * @param factor the specified factor
	 * @return the current font size
	 */
	changeLabelSize: function(factor){
		var reachBoundary = true;
		for(drawingName in this.drawings){
			svg = this.getSvgByDrawingName(drawingName);
			var labelFactor = this.getLabelFactor(svg)
			var labels = svg.select('#asset-labels');
	        if(labels && !labels.empty()){
	        	var fontSize = labels.attr("font-size");
	        	if(fontSize){
	           		fontSize = parseFloat(fontSize) * factor * labelFactor;
	           	 	if(fontSize < 5){
	           	 		if(fontSize > 0.2){
		        			reachBoundary = false;
		    	        	labels.attr("font-size", (fontSize/labelFactor) + 'em');
	           	 		} else {
		        			reachBoundary = true;
		    	        	labels.attr("font-size", (0.2/labelFactor) + 'em');
	           	 		}
	        		} else {
	    	        	labels.attr("font-size", (5/labelFactor) + 'em');
	        		}
	        	} else {
	        		fontSize = Math.max(1.5 * factor * labelFactor, 0.1);
	           	 	if(fontSize < 10){
	           	 		if(fontSize > 0.1){
		        			reachBoundary = false;
		    	        	labels.attr("font-size", (fontSize/labelFactor) + 'em');
	           	 		} else {
		        			reachBoundary = true;
		    	        	labels.attr("font-size", (0.1/labelFactor) + 'em');
	           	 		}
	        		} else {
	    	        	labels.attr("font-size", (10/labelFactor) + 'em');
	        		}
	           	}
	        }
		}
		return 	reachBoundary;
	},
	
	/**
	 * Retrieve Label Factor based on drawing's unit.
	 * 
	 * @param svg SVG drawing 
	 * @return label factor
	 */
	getLabelFactor: function(svg){
		if(!svg || svg.empty())
			return 1;
		
		var units = DrawingCommon.getUnits(svg);
		var labelFactor = 1;

		switch(units) {
	    case "mm":
	    	labelFactor = 0.025;
	        break;
	    case "cm":
	    	labelFactor = 0.25;
	    	break;
	    case "Metres":
	    	labelFactor = 25;
	    	break;
	    case "m":
	    	labelFactor = 25
	    	break;
	    default:
	    	labelFactor = 1;
		}

		return labelFactor;
	},
	
    /**
     * Creates and return an svg.
     * 
     * @param div       HTMLDivElement root Div to hold all the svgs
     * @param row       Number row index for the created svg, default to 0
     * @param col       Number col index for the created svg, default to 0
     * @param width     Number  Width of the SVG
     * @param height    Number  Height of the SVG
     * @returns SVGSVGElement the created SVG 
     */
    createSvg: function(div, row, col, width, height) {
    	
    	if(div.attr("id")){
    		this.config.divId = div.attr("id");
    	}

    	if(typeof row === 'undefined' || row == null || row < 0){
    		row = 0;
    	}

    	if(typeof col === 'undefined' || col == null || col < 0){
    		col = 0;
    	}

    	//check if row div exists
    	var divRow = div.select('#' + this.config.divId + "-" + row);
    	if(!divRow){
    		divRow = div.append("div")
    					.attr("id", this.config.divId + "-" + row)
    					.attr("style", "width: 100%; height: 50%;");
    	}
    	
    	//check if row/col div exists
    	var divRowCol = divRow.select('#' + this.config.divId + "-" + row + "-" + col);
    	if(!divRowCol){
    		divRowCol = divRow.append("div")
    						  .attr("id", this.config.divId + "-" + row + "-" + col)
    						  .attr("class", "floor-plan")
    						  .attr("style", "width: 100%; height: 100%; display: inline-block; box-sizing: border-box; border: 5px solid white; overflow: hidden;");
    	}
    	
    	// remove the existing svg with the same id.
    	if (!divRowCol.select("svg").empty()) {
    		divRowCol.select("svg").remove();
        }
    	
        var svg = divRowCol.append("svg")
            .attr("id", this.config.divIdId + "-svg").attr("version", "1.1")
            .attr("xmlns", "http://www.w3.org/2000/svg")
            .attr("xmlns:xmlns:xlink", "http://www.w3.org/1999/xlink")
            .attr("width", width)
            .attr("height", height)
            .attr("viewBox", "0 0 " + width + " " + height);

        svg.append("style");
        svg.append("defs");

        var viewer = svg.append("g")
            .attr("id", "viewer");

        var mirror = viewer.append("g")
            .attr("id", "mirror")
            .attr("stroke-width", "0.9%")
            .attr("transform", "scale(1, 1)");

        mirror.append("g")
            .attr("id", "background")
            .attr("fill", "none");

        mirror.append("g")
            .attr("id", "assets")
            .attr("fill", "none");

        return svg;
    },
    
	 /**
	 * Gets all drawings' Image Bytes
	 * 
	 */
    getImageBytes: function(callback){
		var result = '';
		var imageCapture = new ImageCapture();
		var drawingDivId = this.getDiv().node().id;
		imageCapture.captureImage(drawingDivId, false, function(image){
			result = image.substring(22);
			if(callback){
				callback(result);
				return;
			}
 	   	});
		return result;
	},
	/**
     * Retrieves all asset types of all the drawings
     */
    retrieveAssetTypes: function(){
    	var nodes = d3.selectAll('#assets')[0][0].childNodes;
    	var assetTypes = [];
		for(var i =0; i < nodes.length; i++){
			var node = nodes[i];
			if(node.nodeName == 'g'){
				var id = node.getAttribute("id").replace("-assets", "");
				if(id){
					assetTypes.push(id);
				}
			}
		}
		return assetTypes;
    }

});


