Ab.namespace('arcgis');

/*
 *   This control defines the ArcGIS Enhanced Map Control
 *   This control is used with the Extensions for ArcGIS
 */

Ab.arcgis.MapExtensions = Ab.arcgis.ArcGISMap.extend({

    /**
     *
     *  Map Extensions Properties
     *
     */

    // @begin_translatable
    z_MESSAGE_INVALID_LICENSE: 'A license for the ARCHIBUS Geospatial Extensions for Esri was not found. Please see your system administrator.',
    // @end_translatable

    hasExtensionsForEsriLicense: false,

    /**
     *
     *  Feature Layer Properties
     *
     */

    // Ab.arcgis.FeatureLayer control
    featureLayerControl: null,

    // Ext.util.MixedCollection
    // A list of feature layer names and layer configuration options 
    featureLayerList: null,

    // featureLayer tooltip
    featureLayerTooltip: null,

    // featureLayer tooltip
    featureLayerInfoWindow: null,

    // featureLayer legend
    featureLayerLegend: null,

    // graphics layer for mouseover event
    featureLayerHighlightGraphics: null,

    // feature layer graphics
    featureLayerHighlightSymbol: null,
    featureLayerSelectionSymbol: null,
    featureLayerNoSelectionSymbol: null,

    // the feature layer name (active feature)
    featureLayerName: null,

    // the feature layer url
    featureLayerUrl: null,

    // feature layer options
    featureLayerOptions: null,

    // feature layer geometry type
    featureLayerGeometryType: null,

    // feature layer renderer options
    featureLayerRendererOptions: null,

    // feature layer data options
    featureLayerDataOptions: null,

    // a list of wc feature data
    // Ext.util.MixedCollection
    featureLayerData: null,

    // legacy callback
    featureLayerLoadedCallback: null,

    constructor: function(panelIdParam, divIdParam, configObject) {
        View.log('MapExtensions.constructor(): start');
        
        // check for Extensions for ESRI license
        var result = Ab.workflow.Workflow.call('AbCommonResources-ArcgisExtensionsService-hasExtensionsForEsriLicense');
        if (result.code != 'executed') {
            Ab.workflow.Workflow.handleError(result);
        } else {
            this.hasExtensionsForEsriLicense = result.value;
        }

        // f license exists, proceed with control initialization
        if (this.hasExtensionsForEsriLicense === true) {
            this.inherit(panelIdParam, divIdParam, configObject);

            this._buildFeatureLayerList();
        } else {
            var msg = View.getLocalizedString(this.z_MESSAGE_INVALID_LICENSE);
            View.showMessage(msg);
        }

        View.log('MapExtensions.constructor(): end');
    },

    /**
     *
     *  Map Control Methods
     *
     */

    // initilize the map
    _initMap: function() {
        View.log('MapExtensions.initMap(): start');

        this.inherit();

        this._createFeatureLayerTooltipDomElements();

        this._createFeatureLayerInfoWindowDomElements();

        this._createFeatureLayerLegendDomElements();

        View.log('MapExtensions.initMap(): end');
    },

    _loadDojoLibrary: function() {

        // standard maps
        dojo.require("esri.map");
        dojo.require("esri.InfoTemplate");
        dojo.require("esri.tasks.locator");
        dojo.require("esri.dijit.InfoWindow");
        dojo.require("esri.dijit.Legend");

        dojo.require("dojo.dnd.Moveable");
        dojo.require("dojo.dom-construct");
        dojo.require("dojo.io.script");
        dojo.require("dojo.query");
        dojo.require("dojo.on");
        dojo.require("dojo.ready");

        // enhanced maps  
        dojo.require("esri.Color");
        dojo.require("esri.graphic");
        dojo.require("esri.geometry.Polygon");
        dojo.require("esri.layers.FeatureLayer");
        dojo.require("esri.tasks.query");
        dojo.require("esri.symbols.SimpleFillSymbol");
        dojo.require("esri.symbols.SimpleLineSymbol");
        dojo.require("esri.dijit.Print");
        dojo.require("esri.tasks.PrintTemplate");
        dojo.require("esri.tasks.PrintTask");
        dojo.require("esri.tasks.PrintParameters");
        dojo.require("dojo._base.array");

        // when dojo is ready proceed with map initialization 
        var _mapControl = this;
        dojo.ready(function() {
            //console.log('Ab.arcgis.MapExtensions -> dojo.ready...');
            _mapControl._initMap();
        });
    },

    // callback for map.load event 
    _onMapLoad: function() {
        View.log('MapExtensions.onMapLoad()...');
        
        this.inherit();  
        
        // init the feature layer control
        this._initFeatureLayer();
    },

    _createGraphicsLayers: function() {
        this.inherit();

        this.featureLayerHighlightGraphics = new esri.layers.GraphicsLayer({
            id: "featureLayerHighlightGraphics",
        });
        this.map.addLayer(this.featureLayerHighlightGraphics);

        this.featureLayerDefaultSelectionSymbol = new esri.symbol.SimpleFillSymbol(
            esri.symbol.SimpleFillSymbol.STYLE_SOLID,
            new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([0, 0, 0]), 2.0),
            new dojo.Color([0, 0, 0, 0.5])
        );
    },


    _completeMapLoad: function() {
        //console.log('Ab.arcgis.MapExtensions -> completeMapLoad...');

        // set initialized to true
        this.mapInited = true;

        // call mapConfigObject mapLoadedCallback if it exists 
        if (this.mapConfigObject.hasOwnProperty('mapLoadedCallback')) {
            if (typeof this.mapConfigObject.mapLoadedCallback === 'function') {
                var callback = this.mapConfigObject.mapLoadedCallback;
                this.mapConfigObject.mapLoadedCallback = null;
                callback();
            }
        }

    },

    /**
     *
     *  Feature Layer Methods
     *
     */

    // create the feature layer control
    _initFeatureLayer: function() {
        View.log('MapExtensions.initFeatureLayer(): start');

        var mapControl = this;
        this.featureLayerControl = new Ab.arcgis.FeatureLayer(mapControl);

        var _mapControl = this;
        dojo.connect(_mapControl.map, 'onClick', function(event) {
            // if the user hasnt clicked on a graphic
            // clear selected features
            if (!event.graphic) {
                _mapControl.clearSelectedFeatures();
                _mapControl.hideFeatureLayerInfoWindow();
            }
        });

        View.log('MapExtensions.initFeatureLayer(): end');
    },

    // build the feature layer list
    _buildFeatureLayerList: function() {
        //console.log('Ab.arcgis.MapExtensions -> buildFeatureLayerList...');

        this.featureLayerList = new Ext.util.MixedCollection();

        if (this.mapConfigObject.hasOwnProperty('featureLayerList')) {
            var _featureLayerList = this.mapConfigObject.featureLayerList;

            for (var i = 0; i < _featureLayerList.length; i++) {
                this.featureLayerList.add(_featureLayerList[i].name, {
                    url: _featureLayerList[i].url,
                    opacity: _featureLayerList[i].opacity || 1.0,
                    whereClause: _featureLayerList[i].whereClause || '1=1',
                    outFields: _featureLayerList[i].outFields || '[*]',
                    zoomToResults: _featureLayerList[i].zoomToResults || true,
                    toolTipField: _featureLayerList[i].toolTipField || '',
                    assetIdField: _featureLayerList[i].assetIdField,
                    assetTypeField: _featureLayerList[i].assetTypeField,
                    objectIdField: _featureLayerList[i].objectIdField,
                    //TODO
                    geometryType: _featureLayerList[i].geometryType || 'polygon'
                });
            }

        } else {
            // TODO message user
            View.log('mapConfigObject.featureLayerList is not configured.');
        }
    },

    // create feature layer tooltop dom elements
    _createFeatureLayerTooltipDomElements: function() {
        this.featureLayerTooltip = dojo.create('div', {
            'id': this.divId + '_featureLayerTooltip',
            'class': 'featureLayerTooltip',
            'innerHTML': ""
        }, this.map.container);
        dojo.style(this.featureLayerTooltip, 'position', 'fixed');
        dojo.style(this.featureLayerTooltip, 'display', 'none');

    },

    _hideFeatureLayerTooltip: function() {
        dojo.style(this.featureLayerTooltip, "display", "none");
    },

    // create feature layer info dom elements
    _createFeatureLayerInfoWindowDomElements: function() {
        //console.log('Ab.arcgis.MapExtensions -> createFeatureLayerInfoWindow...');

        // add info window display elements
        this.featureLayerInfoWindow = dojo.create('div', {
            'id': this.divId + '_featureLayerInfoWindow',
            'class': 'featureLayerInfoWindow',
            'innerHTML': ""
        }, this.map.container);
        //dojo.create('div', {'id':'featureLayerInfoWindowCloseButton', 'class':'featureLayerInfoWindowCloseButton'}, this.featureLayerInfoWindow);
        dojo.create('div', {
            'id': this.divId + '_featureLayerInfoWindowContent',
            'class': 'featureLayerInfoWindowContent',
            'innerHTML': ''
        }, this.featureLayerInfoWindow);

        dojo.style(this.featureLayerInfoWindow, 'position', 'fixed');
        dojo.style(this.featureLayerInfoWindow, 'display', 'none');

        // make the info window moveable
        //var esriMoveable = dojo.dnd.Moveable(dojo.byId('featureLayerInfoWindow'));

        // wire up close event to close button
        // var _mapControl = this;
        // dojo.connect(dojo.byId('featureLayerInfoWindow'), 'click', function() {
        //  _mapControl.hideFeatureLayerInfoWindow();
        // });

    },

    _hideFeatureLayerInfoWindow: function() {
        dojo.style(this.featureLayerInfoWindow, "display", "none");
    },

    // create feature layer legend dom elements
    _createFeatureLayerLegendDomElements: function() {
        //console.log('Ab.arcgis.MapExtensions -> createFeatureLayerLegend...');

        var _mapControl = this;

        this.featureLayerLegend = dojo.create('div', {
            'id': this.divId + '_featureLayerLegend',
            'class': 'featureLayerLegend',
            'innerHTML': "",
            'display': 'none'
        }, this.map.container); //this.panelId?
        dojo.create('div', {
            'id': this.divId + '_featureLayerLegendCloseButton',
            'class': 'featureLayerLegendCloseButton',
            'click': function(){_mapControl.hideFeatureLayerLegend()}
        }, this.featureLayerLegend);
        dojo.create('div', {
            'id': this.divId + '_featureLayerLegendContent',
            'class': 'featureLayerLegendContent',
            'innerHTML': ''
        }, this.featureLayerLegend);

        this.hideFeatureLayerLegend();
        this._clearFeatureLayerLegendContent();
 
    },

    showFeatureLayerLegend: function() {
        //console.log('Ab.arcgis.MapExtensions -> showFeatureLayerLegend...');
        document.getElementById(this.divId + '_featureLayerLegend').style.display = 'block';
    },

    hideFeatureLayerLegend: function() {
        //console.log('Ab.arcgis.MapExtensions -> hideFeatureLayerLegend...');
        document.getElementById(this.divId + '_featureLayerLegend').style.display = 'none';
    },

    // update the feature layer legend content
    _updateFeatureLegendContent: function(){
        //console.log('Ab.arcgis.MapExtensions -> updateLegendContent...');  

        var htmlContent;
        var renderer = this.featureLayerRendererOptions.type;

        switch (renderer) {

          // case 'simple':
          //   // TODO 
          //   // var title = this._getFieldTitle(markerProperties.dataSource,  markerProperties.titleField);
          //   // var backgroundColor = markerProperties.markerOptions.fillColor;
          //   // htmlContent = '<table>';
          //   // htmlContent += '<tr><td style=background-color:' + backgroundColor + '>&nbsp;&nbsp;&nbsp;</td><td>Locations</td></tr>';
          //   // htmlContent += '</table>';
          //   htmlContent = "";
          //   break;

          case 'thematic-unique-values':
            var title;
            if (this.featureLayerDataOptions.legendTitle) {
                title = this.featureLayerDataOptions.legendTitle;
            } else {
                title = this.getFieldTitle(this.featureLayerDataOptions.dataSource,  this.featureLayerDataOptions.thematicField) + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
            }
            var thematicColors = this.featureLayerRendererOptions.thematicColors;
            var legendDataLabels = this.featureLayerRendererOptions.thematicUniqueValues;
            if (this.featureLayerDataOptions.legendDataLabels && this.featureLayerDataOptions.legendDataLabels.length == legendDataLabels.length ) {
                legendDataLabels = this.featureLayerDataOptions.legendDataLabels;
            }
            var dataPrefix = this.featureLayerRendererOptions.legendDataPrefix;
            var dataSuffix = this.featureLayerRendererOptions.legendDataSuffix;

            htmlContent = "<table>";
            htmlContent += "<tr><td colspan='2' class='featureLayerLegendTitle'>" + title + "</td></tr>";
            for (i=0; i<legendDataLabels.length; i++) {
              var backgroundColor = this.RGBtoHex(thematicColors[i][0],thematicColors[i][1],thematicColors[i][2]);
              var label = legendDataLabels[i];
              htmlContent += '<tr><td class="featureLayerLegendSwatch" style=background-color:#' + backgroundColor + '>&nbsp;&nbsp;&nbsp;</td><td class="featureLayerLegendLabel">' + label + '</td></tr>';          
            }
            htmlContent += '</table>';
            break;

          case 'thematic-class-breaks':
            var title;
            if (this.featureLayerDataOptions.legendTitle) {
                title = this.featureLayerDataOptions.legendTitle;
            } else {
                title = this.getFieldTitle(this.featureLayerDataOptions.dataSource,  this.featureLayerDataOptions.thematicField) + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
            }            
            var thematicColors = this.featureLayerRendererOptions.thematicColors;
            var legendDataLabels = this.featureLayerRendererOptions.thematicClassBreaks;
            if (this.featureLayerDataOptions.legendDataLabels && this.featureLayerDataOptions.legendDataLabels.length == legendDataLabels.length) {
                legendDataLabels = this.featureLayerDataOptions.legendDataLabels;
            }
            var dataPrefix = this.featureLayerRendererOptions.legendDataPrefix;
            var dataSuffix = this.featureLayerRendererOptions.legendDataSuffix;

            htmlContent = "<table>";
            htmlContent += "<tr><td colspan='2' class='featureLayerLegendTitle'>" + title + "</td></tr>";
            for (i=0; i<legendDataLabels.length + 1; i++) {
              var backgroundColor = this.RGBtoHex(thematicColors[i][0],thematicColors[i][1],thematicColors[i][2]);
              var label;
              if (i==0) {
                label = '< ' + dataPrefix + legendDataLabels[0] + dataSuffix;
              } else if (i==legendDataLabels.length) {
                label = '> ' + dataPrefix + legendDataLabels[i-1] + dataSuffix;
              } else (
                label = dataPrefix + legendDataLabels[i-1] + dataSuffix + ' - ' + dataPrefix + legendDataLabels[i] + dataSuffix
              )
              htmlContent += '<tr><td class="featureLayerLegendSwatch" style=background-color:#' + backgroundColor + '>&nbsp;&nbsp;&nbsp;</td><td class="featureLayerLegendLabel">' + label + '</td></tr>';          
            }
            htmlContent += '</table>';
            break;

           default:
            break;

        }

        document.getElementById(this.divId + '_featureLayerLegendContent').innerHTML = htmlContent;

    },

    _clearFeatureLayerLegendContent: function() {

        var htmlContent;

        var title = 'Legend';
        var content = 'No Content';

        htmlContent = "<table>";
        htmlContent += "<tr><td class='featureLayerLegendTitle'>" + title + "</td></tr>";
        htmlContent += '<tr><td class="featureLayerLegendLabel">' + content + '</td></tr>';          
        htmlContent += '</table>';
    
        document.getElementById(this.divId + '_featureLayerLegendContent').innerHTML = htmlContent;
    },

    /*
     * Switch the active feature layer
     * @param layerNameParam
     * @param whereClause
     * @param callbackParam 
     *
     */
    switchFeatureLayer: function(layerNameParam, layerOptionsParam, callbackParam) {
        View.log('MapExtensions.switchFeatureLayer(): start');

        // set the callback
        this.featureLayerCallback = callbackParam || null;    
        
        // hide tooltip and info window
        this._hideFeatureLayerTooltip();
        this._hideFeatureLayerInfoWindow();

        // clear the legend content
        this._clearFeatureLayerLegendContent();

        // remove existing feature layer
        if (this.featureLayerControl) {
            this.featureLayerControl.remove();
        }

        // prepare layer options for layer constructor
        if (layerNameParam) {
            var layerUrl = this.featureLayerList.get(layerNameParam).url;
            this.featureLayerUrl = layerUrl;

            var layerOptions = {
                id: 'featureLayer',
                mode: esri.layers.FeatureLayer.MODE_SNAPSHOT
            };
            if (this.featureLayerList.get(layerNameParam).outFields) {
                layerOptions.outFields = this.featureLayerList.get(layerNameParam).outFields;
            }
            if (this.featureLayerList.get(layerNameParam).opacity) {
                layerOptions.opacity = this.featureLayerList.get(layerNameParam).opacity;
            }
            if (this.featureLayerList.get(layerNameParam).index) {
                layerOptions.index = this.featureLayerList.get(layerNameParam).index;
            }
        }

        // set the active feature layer name
        this.featureLayerName = layerNameParam;

        // set the feature layer data source
        var thematicDataSource = layerOptionsParam.thematicDataSource || 'ARCGIS';
        var zoomToExtent = layerOptionsParam.zoomToExtent || false;

        // create the feature layer data options    
        switch (thematicDataSource) {
            case 'WEB CENTRAL':
                this.featureLayerDataOptions = {
                    source: 'WC_TABLE',
                    table: layerOptionsParam.table,
                    keyField: layerOptionsParam.keyField,
                    renderer: layerOptionsParam.renderer || 'simple',
                    fillColor: layerOptionsParam.fillColor || [31,121,180],
                    thematicField: layerOptionsParam.thematicField,
                    thematicClassBreaks: layerOptionsParam.thematicClassBreaks,
                    thematicUniqueValues: layerOptionsParam.thematicUniqueValues,                   
                    thematicColors: layerOptionsParam.thematicColors,
                    whereClause: layerOptionsParam.whereClause,
                    legendTitle: layerOptionsParam.legendTitle,
                    legendDataLabels: layerOptionsParam.legendDataLabels,
                    legendDataSuffix: layerOptionsParam.legendDataSuffix || '',
                    legendDataPrefix: layerOptionsParam.legendDataPrefix || '',
                    zoomToExtent: zoomToExtent
                };
                break;
            case 'WC_DATARECORDS':
                this.featureLayerDataOptions = {
                    source: 'WC_DATARECORDS',
                    dataRecords: layerOptionsParam.dataRecords,
                    keyField: layerOptionsParam.keyField,
                    renderer: layerOptionsParam.renderer || 'simple',
                    fillColor: layerOptionsParam.fillColor || [31,121,180],
                    thematicField: layerOptionsParam.thematicField,
                    thematicClassBreaks: layerOptionsParam.thematicClassBreaks,
                    thematicUniqueValues: layerOptionsParam.thematicUniqueValues,                   
                    thematicColors: layerOptionsParam.thematicColors,
                    whereClause: layerOptionsParam.whereClause,
                    legendTitle: layerOptionsParam.legendTitle,
                    legendDataLabels: layerOptionsParam.legendDataLabels,
                    legendDataSuffix: layerOptionsParam.legendDataSuffix || '',
                    legendDataPrefix: layerOptionsParam.legendDataPrefix || ''
                };
                break;
            case 'WC_DATASOURCE':
                this.featureLayerDataOptions = {
                    source: 'WC_DATASOURCE',
                    dataSource: layerOptionsParam.dataSource,
                    dataSourceParameters: layerOptionsParam.dataSourceParameters,
                    dataSourceRestriction: layerOptionsParam.dataSourceRestriction,
                    keyField: layerOptionsParam.keyField,
                    renderer: layerOptionsParam.renderer || 'simple',
                    fillColor: layerOptionsParam.fillColor || [31,121,180],
                    thematicField: layerOptionsParam.thematicField,
                    thematicClassBreaks: layerOptionsParam.thematicClassBreaks,
                    thematicUniqueValues: layerOptionsParam.thematicUniqueValues,                   
                    thematicColors: layerOptionsParam.thematicColors,
                    whereClause: layerOptionsParam.whereClause,
                    legendTitle: layerOptionsParam.legendTitle,
                    legendDataLabels: layerOptionsParam.legendDataLabels,
                    legendDataSuffix: layerOptionsParam.legendDataSuffix || '',
                    legendDataPrefix: layerOptionsParam.legendDataPrefix || '',
                    zoomToExtent: zoomToExtent
                };               
                break;
            default:
                this.featureLayerDataOptions = {
                    source: 'ARCGIS',
                    fillColor: layerOptionsParam.fillColor || [31,121,180],
                    zoomToExtent: zoomToExtent
                };
                break;                
        }

        // set layer options
        this.featureLayerOptions = layerOptions;

        // get the geometry type from the layer
        this._fetchFeatureLayerGeometryType(layerUrl);
        
        View.log('MapExtensions.switchFeatureLayer(): end');
    },

    _createFeatureLayer:function() {
        View.log('MapExtensions.createFeatureLayer(): start');

        // create the feature layer 
        var featureLayer;

        // create the feature layer symbols
        this._createFeatureLayerSymbols();

        // create the feature layer
        switch (this.featureLayerDataOptions.source) {
            case 'WEB_CENTRAL':
            case 'WC_DATARECORDS':
            case 'WC_DATASOURCE':
                featureLayer = this._createFeatureLayerFromWebCentralData();                
                break;         
            case 'JSON':
                //TODO
                featureLayer = this._createFeatureLayerFromJsonData();
                break;
            default:
                featureLayer = this._createArcgisFeatureLayer();
        }

        // set definition expression (where clause)
        var whereClause = this.featureLayerDataOptions.whereClause || '1=1';
        featureLayer.setDefinitionExpression(whereClause);

        // create the feature layer renderer
        var layerRenderer = this.featureLayerDataOptions.renderer || 'simple';
       
        // set the renderer properties
        switch (layerRenderer){
            case 'simple':
                this.featureLayerRendererOptions = {
                    type: 'simple',
                    fillColor: this.featureLayerDataOptions.fillColor
                };
                break;
            case 'thematic-unique-values': 
                this.featureLayerRendererOptions = {
                    type: 'thematic-unique-values',
                    thematicField: this.featureLayerDataOptions.thematicField, 
                    thematicUniqueValues: this.featureLayerDataOptions.thematicUniqueValues,                   
                    thematicColors: this.featureLayerDataOptions.thematicColors
                };
                if (this.featureLayerDataOptions.source == 'WC_DATARECORDS' || this.featureLayerDataOptions.source == 'WC_DATASOURCE') {
                    this.featureLayerRendererOptions.thematicField = 'ab_render_string';
                }
                break;
            case 'thematic-class-breaks':
                this.featureLayerRendererOptions = {
                    type: 'thematic-class-breaks',
                    thematicField: this.featureLayerDataOptions.thematicField,
                    thematicClassBreaks: this.featureLayerDataOptions.thematicClassBreaks,
                    thematicColors: this.featureLayerDataOptions.thematicColors
                };
                if (this.featureLayerDataOptions.source == 'WC_DATARECORDS' || this.featureLayerDataOptions.source == 'WC_DATASOURCE') {
                    this.featureLayerRendererOptions.thematicField = 'ab_render_number';
                }
                break;
        };


        this.featureLayerRendererOptions.legendDataSuffix = this.featureLayerDataOptions.legendDataSuffix || '';
        this.featureLayerRendererOptions.legendDataPrefix = this.featureLayerDataOptions.legendDataPrefix || '';


        var renderer = this._createFeatureLayerRenderer();
        featureLayer.setRenderer(renderer);
        
        // set default selection symbol (assumes polygon) //TODO

        if (featureLayer.getSelectionSymbol() === undefined) {
            featureLayer.setSelectionSymbol(this.featureLayerDefaultSelectionSymbol);
        }

        // create feature layer event handlers
        var _mapControl = this;
        this._createFeatureLayerEventHandlers(featureLayer, this.featureLayerName, _mapControl);

        // LEGACY
        // create feature layer on load callback
        var callbackParam = this.featureLayerCallback;

        if (!callbackParam) {
            callbackParam = null;
        }
        if (typeof callbackParam === 'function') {
            featureLayer.on('load', function(result) {
                callbackParam();
            });
        };

        // add the layer
        this.map.addLayer(featureLayer);
        
        //this.map.addLayers([featureLayer]);
        this.map.reorderLayer(featureLayer, 50);

        // make sure mouse events are enabled
        this.map.graphics.enableMouseEvents();

        View.log('MapExtensions.createFeatureLayer(): end');
    },

    getFeatureLayerObjectIdsByValue: function(dataFieldName, objectIdFieldName, fieldValues){
        var objectIds = [];
        
        var featureLayer = this.map.getLayer('featureLayer');
        var graphics = featureLayer.graphics;

        for (i=0; i<fieldValues.length; i++){
          for (ii=0; ii<graphics.length; ii++){
            var graphic = graphics[ii];
            if (fieldValues[i] == graphic.attributes[dataFieldName]){
              objectIds.push(graphic.attributes[objectIdFieldName]);
            }
          }
        }

        return objectIds;
    },

    _createArcgisFeatureLayer: function() {
        var featureLayer = new esri.layers.FeatureLayer(this.featureLayerUrl, this.featureLayerOptions);
        return featureLayer;
    },

    _createFeatureLayerFromJsonData: function() {

    },

    _createFeatureLayerFromWebCentralData: function() {
        View.log('MapExtensions.createFeatureLayerFromWebCentralData(): start');

        // create the wc feature data
        this._createFeatureData();

        // create the arcgis features
        this._createFeatureSet();
    
        //create a feature collection for polygons
        var featureCollection = {
          "layerDefinition": null,
          "featureSet": {
            "features": [],
            "geometryType": this.featureLayerGeometryType 
          }
        };

        // create the feature layer definition
        featureCollection.layerDefinition = this._createLayerDefinition();  

        var featureLayer = new esri.layers.FeatureLayer(featureCollection, 
            this.featureLayerOptions
        );

        return featureLayer;

        View.log('MapExtensions.createFeatureLayerFromWebCentralData(): start');

    },

    _createLayerDefinition: function() {
        
        View.log('MapExtensions.createLayerDefinition(): start');

        switch (this.featureLayerGeometryType){
            case 'esriGeometryPoint':
                this._createPointLayerDefinition();
                break;

            case 'esriGeometryLine':
            case 'esriGeometryPolyline':
                return this._createPolylineLayerDefinition();
                break;

            case 'esriGeometryPolygon': 
                return this._createPolygonLayerDefinition();
                break;            
        }

        View.log('MapExtensions.createLayerDefinition(): end');

    },

    _createPointLayerDefinition: function() {
        var layerDefinition =  {
          "geometryType": "esriGeometryPoint",
          "objectIdField": "objectid",
          "drawingInfo": {
            "renderer":{
                "type":"simple",
                "symbol":{
                    "type":"esriSMS",
                    "style":"esriSMSCircle",
                    "color": [128,128,128,255],
                    "outline": {
                        "color": [0,0,0,255],
                        "width": 1,
                        "type": "esriSLS",
                        "style": "esriSLSSolid"
                    },                 
                    "size":15
                }
            }
          },
          "fields": [
              {
               "name": "objectid",
               "alias": "objectid",
               "type": "esriFieldTypeOID"
              },
              //TODO  need to configure this key
              {
               "name": "ab_id",
               "type": "esriFieldTypeString",
               "alias": "ab_id",
               "domain": null,
               "editable": true,
               "nullable": true,
               "length": 12
              },
              {
               "name": "ab_name",
               "type": "esriFieldTypeString",
               "alias": "ab_name",
               "domain": null,
               "editable": true,
               "nullable": true,
               "length": 25
              },
              {
               "name": "ab_render_string",
               "type": "esriFieldTypeString",
               "alias": "ab_render_string",
               "domain": null,
               "editable": true,
               "nullable": true,
               "length": 50
              },
              {
               "name": "ab_render_number",
               "type": "esriFieldTypeDouble",
               "alias": "ab_render_number",
               "domain": null,
               "editable": true,
               "nullable": true
              }              
             ]
        };
        return layerDefinition;
    },


    _createPolylineLayerDefinition: function() {
        var layerDefinition =  {
          "geometryType": "esriGeometryPolyline",
          "objectIdField": "objectid",
          "drawingInfo": {
            "renderer":{
                "type":"simple",
                "symbol":{
                     "type":"esriSLS",
                     "style":"esriSLSSolid",
                     "color":[
                        128,
                        128,
                        128,
                        255
                     ],
                     "width":2.25,
                }
            }
          },
          "fields": [
              {
               "name": "objectid",
               "alias": "objectid",
               "type": "esriFieldTypeOID"
              },
              //TODO  need to configure this key
              {
               "name": "ab_id",
               "type": "esriFieldTypeString",
               "alias": "ab_id",
               "domain": null,
               "editable": true,
               "nullable": true,
               "length": 12
              },
              {
               "name": "ab_name",
               "type": "esriFieldTypeString",
               "alias": "ab_name",
               "domain": null,
               "editable": true,
               "nullable": true,
               "length": 25
              },
              {
               "name": "ab_render_string",
               "type": "esriFieldTypeString",
               "alias": "ab_render_string",
               "domain": null,
               "editable": true,
               "nullable": true,
               "length": 50
              },
              {
               "name": "ab_render_number",
               "type": "esriFieldTypeDouble",
               "alias": "ab_render_number",
               "domain": null,
               "editable": true,
               "nullable": true
              }              
             ]
        };
        return layerDefinition;
    },

    _createPolygonLayerDefinition: function() {
        var layerDefinition =  {
          "geometryType": "esriGeometryPolygon",
          "objectIdField": "objectid",
          "drawingInfo": {
            "renderer": {
             "type": "simple",
             "symbol": {
                  "type": "esriSFS",
                  "style": "esriSFSSolid",
                  "color": [
                   128,
                   128,
                   128,
                   255
                  ],
                  "outline": {
                   "type": "esriSLS",
                   "style": "esriSLSSolid",
                   "color": [
                    0,
                    0,
                    0,
                    255
                   ],
                   "width": 0.4
                  }
                 }
            }
          },
          "fields": [
              {
               "name": "objectid",
               "alias": "objectid",
               "type": "esriFieldTypeOID"
              },
              //TODO  need to configure this key
              {
               "name": "ab_id",
               "type": "esriFieldTypeString",
               "alias": "ab_id",
               "domain": null,
               "editable": true,
               "nullable": true,
               "length": 12
              },
              {
               "name": "ab_name",
               "type": "esriFieldTypeString",
               "alias": "ab_name",
               "domain": null,
               "editable": true,
               "nullable": true,
               "length": 25
              },
              {
               "name": "ab_render_string",
               "type": "esriFieldTypeString",
               "alias": "ab_render_string",
               "domain": null,
               "editable": true,
               "nullable": true,
               "length": 50
              },
              {
               "name": "ab_render_number",
               "type": "esriFieldTypeDouble",
               "alias": "ab_render_number",
               "domain": null,
               "editable": true,
               "nullable": true
              }              
             ]
        };
        return layerDefinition;
    },

    /**
    * Gets the (attribute) data from Web Central for the features which will be displayed on the map.
    * Returns a Mixed Collection. 
    *
    */
    _createFeatureData: function(){
        View.log('MapExtensions.createFeatureData(): start');
        
        this.featureLayerData = new Ext.util.MixedCollection();
        var featureRecords;

        // get the data from WC
        switch (this.featureLayerDataOptions.source) {
            case 'WC_DATASOURCE':
                var fields = [this.featureLayerDataOptions.keyField, this.featureLayerDataOptions.thematicField];
                var featureRecords = this.getDataSourceRecords(this.featureLayerDataOptions.dataSource,
                    this.featureLayerDataOptions.dataSourceRestriction,
                    this.featureLayerDataOptions.dataSourceParameters
                );
                break;

            case 'WC_DATARECORDS':
                var fields = [this.featureLayerDataOptions.keyField, this.featureLayerDataOptions.thematicField];
                featureRecords = this.featureLayerDataOptions.dataRecords;
                break;

            case 'WC_TABLE':
                var table = this.featureLayerDataOptions.table;
                var fields = [this.featureLayerDataOptions.keyField, this.featureLayerDataOptions.thematicField];
                var whereClause = this.featureLayerDataOptions.whereClause;
                var featureRecords = this._getRecordsFromWebCentral(table, fields, whereClause);
                break;

        }

        // create a mixed collection to hold the data
        var _featureLayerData = new Ext.util.MixedCollection();
        for (var i = 0; i < featureRecords.length; i++) {
            //console.log(featureRecords[i].values[this.featureLayerDataOptions['keyField']] + ' | ' + featureRecords[i].values[this.featureLayerDataOptions['thematicField']]);
            _featureLayerData.add(featureRecords[i].values[this.featureLayerDataOptions['keyField']], featureRecords[i].values);
        }

        this.featureLayerData = _featureLayerData;

        View.log('MapExtensions.createFeatureData(): end');
    },

    /**
    * Queries the ArcGIS feature service for the (geometry) features which will be displayed on the map.
    *
    */
    _createFeatureSet: function(){
        View.log('MapExtensions.createFeatureSet(): start');

        // initialize query task
        var queryTask = new esri.tasks.QueryTask(this.featureLayerUrl);

        // initialize query
        var query = new esri.tasks.Query();
        query.returnGeometry = true;
        query.outFields = this.featureLayerOptions.outFields; 
        //console.log('Feature layer query where clause : ' + this.featureLayerDataOptions.whereClause);
        query.where = this.featureLayerDataOptions.whereClause; //TODO definition expression handles this?

        // execute query
        //queryTask.execute(query, this._onFeatureQueryComplete);
        queryTask.execute(query).then(dojo.hitch(this, '_onFeatureQueryResults'), dojo.hitch(this, '_onFeatureQueryError'));
        
        View.log('MapExtensions.createFeatureSet(): end');     
    },

    /**
    *
    */
    _onFeatureQueryResults: function(results){
       
        var _mapControl = this;

        var features = dojo.map(results.features, function(object){ 
            var attributes = object.attributes;
            var geometry = object.geometry;
     
            // add archibus attributes to features
            var keyField = _mapControl.featureLayerList.get(_mapControl.featureLayerName)['assetIdField'];
            var keyValue = attributes[keyField];
            var thematicField = _mapControl.featureLayerDataOptions.thematicField;
            // example: _mapControl.featureLayerData.get('B-US-MA-1001')['bl.use1']
            
            if (_mapControl.featureLayerData.get(keyValue) !== undefined) {
                var renderValue = _mapControl.featureLayerData.get(keyValue)[thematicField];          
                //TODO 
                attributes.ab_render_string = renderValue;
                attributes.ab_render_number = Number(renderValue); //parseInt(renderValue);                 
            } else {
                attributes.ab_render_string = null;
                attributes.ab_render_number = null;
            }

            return new esri.Graphic(geometry, null, attributes, null);
            
        });
        
        // add features to feature layer
        var featureLayer = this.map.getLayer('featureLayer');
        featureLayer.applyEdits(features, null, null);        

        // update the feature layer legend
        this._updateFeatureLegendContent();

    },

    _onFeatureQueryError: function(error){

        //console.log('Ab.arcgis.MapExtensions -> _onFeatureQueryError...');

    },

    _createFeatureLayerRenderer: function() {
        // set renderer
        var renderer;

        var markerSymbol = esri.symbol.SimpleMarkerSymbol.STYLE_CIRCLE;
        var lineSymbol = esri.symbol.SimpleLineSymbol.STYLE_SOLID;
        var fillSymbol = esri.symbol.SimpleFillSymbol.STYLE_SOLID;
        
        // TODO -- from featureLayerDataOptions ?
        var markerSize = 15;

        var lineColor = new dojo.Color([0, 0, 0, 0.5]); 
        var fillColor = this.featureLayerRendererOptions.fillColor || [0, 0, 0, 0.5];
        
        var lineWidthLight = 1.0;
        var lineWidthMedium = 2.0;
        var lineWidthHeavy = 4.0;

        var outlineStyle = new esri.symbol.SimpleLineSymbol(lineSymbol, lineColor, lineWidthLight);

        var thematicDataSource = this.featureLayerDataOptions.source || 'ARCGIS';
        var thematicColors = this.featureLayerRendererOptions.thematicColors; //TODO
        var thematicField = this.featureLayerRendererOptions.thematicField;
        var thematicUniqueValues = this.featureLayerRendererOptions.thematicUniqueValues || [];
        var thematicClassBreaks = this.featureLayerRendererOptions.thematicClassBreaks;

        // this is only relevant to WC data source
        var whereClause = this.featureLayerRendererOptions.whereClause || '1=1';
        if (thematicDataSource === 'WEB_CENTRAL' && thematicUniqueValues.length === 0) {
            thematicUniqueValues = this._getDistinctFieldValues(thematicField, whereClause);
        }

        //var geometryType = this.featureLayerList.get(this.featureLayerName).geometryType;
        var geometryType = this.featureLayerGeometryType;

        switch (this.featureLayerRendererOptions.type) {

            case 'simple':
                var symbol = symbol = new esri.symbol.SimpleFillSymbol(fillSymbol, outlineStyle, fillColor);
                if (geometryType === 'esriGeometryPoint') {
                    symbol = new esri.symbol.SimpleMarkerSymbol(markerSymbol, markerSize, outlineStyle, fillColor);
                }
                if (geometryType === 'esriGeometryPolyline' || geometryType === 'esriGeometryLine' ) {
                    symbol = new esri.symbol.SimpleLineSymbol(lineSymbol, fillColor, lineWidthHeavy);                    
                }

                // define simple renderer
                var renderer = new esri.renderer.SimpleRenderer(symbol);
                break;


            case 'thematic-unique-values':
                // create default symbol for renderer
                var defaultSymbol = new esri.symbol.SimpleFillSymbol(fillSymbol, outlineStyle, fillColor);
                if (geometryType === 'esriGeometryPoint') {
                    defaultSymbol = new esri.symbol.SimpleMarkerSymbol(markerSymbol, markerSize, outlineStyle, fillColor);
                }
                if (geometryType === 'esriGeometryPolyline' || geometryType === 'esriGeometryLine'){
                    defaultSymbol = new  esri.symbol.SimpleLineSymbol(lineSymbol, fillColor, lineWidthHeavy);                    
                }

                // define unique value renderer
                renderer = new esri.renderer.UniqueValueRenderer(defaultSymbol, thematicField);
                for (i = 0; i < thematicUniqueValues.length; i++) {
                    var thematicSymbol = new esri.symbol.SimpleFillSymbol(fillSymbol, outlineStyle, thematicColors[i]);
                    if (geometryType === 'esriGeometryPoint') {
                        thematicSymbol = new esri.symbol.SimpleMarkerSymbol(markerStyle, markerSize, outlineStyle, thematicColors[i]);
                    }
                    if (geometryType === 'esriGeometryPolyline' || geometryType === 'esriGeometryLine'){
                        thematicSymbol = new  esri.symbol.SimpleLineSymbol(lineSymbol, thematicColors[i], lineWidthHeavy);                    
                    }
                    // add unique values to renderer
                    renderer.addValue(thematicUniqueValues[i], thematicSymbol);
                }
                break;

            case 'thematic-class-breaks':

                // create default symbol for renderer
                var defaultSymbol = new esri.symbol.SimpleFillSymbol(fillSymbol, outlineStyle, fillColor);
                if (geometryType === 'esriGeometryPoint') {
                    defaultSymbol = new esri.symbol.SimpleMarkerSymbol(markerSymbol, markerSize, outlineStyle, fillColor);
                }
                if (geometryType === 'esriGeometryPolyline' || geometryType === 'esriGeometryLine'){
                    defaultSymbol = new  esri.symbol.SimpleLineSymbol(lineSymbol, fillColor, lineWidthHeavy);                    
                }

                // define class breaks renderer
                renderer = new esri.renderer.ClassBreaksRenderer(defaultSymbol, thematicField);
                for (i = 0; i < thematicClassBreaks.length + 1; i++) {
                    // create default symbol for renderer
                    var thematicSymbol = new esri.symbol.SimpleFillSymbol(fillSymbol, outlineStyle, thematicColors[i]);
                    if (geometryType === 'esriGeometryPoint') {
                        thematicSymbol = new esri.symbol.SimpleMarkerSymbol(markerStyle, markerSize, outlineStyle, thematicColors[i]);
                    }
                    if (geometryType === 'esriGeometryPolyline' || geometryType === 'esriGeometryLine'){
                        thematicSymbol = new  esri.symbol.SimpleLineSymbol(lineSymbol, thematicColors[i], lineWidthHeavy);                    
                    }

                    // add class breaks to renderer
                    if (i == 0) {
                        // first class break
                        renderer.addBreak({
                            minValue: -Infinity,
                            maxValue: thematicClassBreaks[0],
                            symbol: thematicSymbol
                        });
                    } else if (i == this.featureLayerRendererOptions.thematicClassBreaks.length) {
                        // last class break
                        renderer.addBreak({
                            minValue: thematicClassBreaks[i - 1],
                            maxValue: +Infinity,
                            symbol: thematicSymbol
                        });
                    } else {
                        // intermediate class break
                        renderer.addBreak({
                            minValue: thematicClassBreaks[i - 1],
                            maxValue: thematicClassBreaks[i],
                            symbol: thematicSymbol
                        });
                    }
                }
                break;

            default:
                break;
        }
        return renderer;
    },

    _createFeatureLayerEventHandlers: function(featureLayer, featureLayerName, mapControl) {
        var me = this;
        var featureLayerControl = mapControl.featureLayerControl;
        
        // wire up mouse events for tooltip
        dojo.connect(featureLayer, 'onMouseOver', function(event) {
            //console.log('Ab.arcgis.MapExtensions -> onMouseOver...');
            var toolTipField = mapControl.featureLayerList.get(featureLayerName).toolTipField;
            var toolTipValue = event.graphic.attributes[toolTipField];
            if (toolTipValue == ' ') {
                toolTipValue = 'NO DATA';
            }
            var highlightGraphic = new esri.Graphic(event.graphic.geometry, mapControl.featureLayerHighlightSymbol);
            highlightGraphic.attributes = event.graphic.attributes;
            highlightGraphic.id = 'highlightGraphic';

            mapControl.featureLayerHighlightGraphics.clear();
            mapControl.featureLayerHighlightGraphics.add(highlightGraphic);

            dojo.style(mapControl.featureLayerTooltip, "display", "");
            var node = dojo.byId(me.divId + '_featureLayerTooltip');
            node.innerHTML = toolTipValue;
        });

        dojo.connect(featureLayer, "onMouseMove", function(evt) {
            //console.log('Ab.arcgis.MapExtensions -> onMouseMove...');
            var px, py;
            if (evt.clientX || evt.pageY) {
                px = evt.clientX;
                py = evt.clientY;
            } else {
                px = evt.clientX + dojo.body().scrollLeft - dojo.body().clientLeft;
                py = evt.clientY + dojo.body().scrollTop - dojo.body().clientTop;
            }
            dojo.style(mapControl.featureLayerTooltip, "display", "none");
            dojo.style(mapControl.featureLayerTooltip, {
                left: (px + 15) + "px",
                top: (py) + "px"
            });
            dojo.style(mapControl.featureLayerTooltip, "display", "");
        });

        dojo.connect(featureLayer, "onMouseOut", function(evt) {
            //console.log('Ab.arcgis.MapExtensions -> onMouseOut...');
            mapControl.featureLayerTooltip.style.display = 'none';
            mapControl.featureLayerHighlightGraphics.clear();
        });

        // wire up layer on click event (legacy)
        if (this.mapConfigObject.featureLayerClickCallback &&
            typeof this.mapConfigObject.featureLayerClickCallback === 'function') {

            var featureLayerClickCallback = this.mapConfigObject.featureLayerClickCallback;
            var assetIdField = this.featureLayerList.get(featureLayerName).assetIdField;
            var assetTypeField = this.featureLayerList.get(featureLayerName).assetTypeField;

            dojo.connect(featureLayer, 'onClick', function(event) {
                //console.log('Ab.arcgis.MapExtensions -> featureLayerMouseOver...');
                var assetIdValue = event.graphic.attributes[assetIdField];
                var assetTypeValue = event.graphic.attributes[assetTypeField];
                var assetGraphic = esri.Graphic(event.graphic.geometry, null, event.graphic.attributes, null);
                featureLayerClickCallback(assetIdValue, assetTypeValue, assetGraphic);
            });
        }

        // wire up layer mouse over event (legacy)
        if (this.mapConfigObject.featureLayerMouseOverCallback &&
            typeof this.mapConfigObject.featureLayerMouseOverCallback === 'function') {

            var featureLayerMouseOverCallback = this.mapConfigObject.featureLayerMouseOverCallback;
            var assetIdField = this.featureLayerList.get(featureLayerName).assetIdField;
            var assetTypeField = this.featureLayerList.get(featureLayerName).assetTypeField;

            dojo.connect(featureLayer, 'onMouseOver', function(event) {
                //console.log('Ab.arcgis.MapExtensions -> featureLayerMouseOverClick...');
                var assetIdValue = event.graphic.attributes[assetIdField];
                var assetTypeValue = event.graphic.attributes[assetTypeField];
                var assetGraphic = esri.Graphic(event.graphic.geometry, null, event.graphic.attributes, null);
                featureLayerMouseOverCallback(assetIdValue, assetTypeValue, assetGraphic);
            });
        }

        // wire up layer mouse out event (legacy)
        if (this.mapConfigObject.featureLayerMouseOutCallback &&
            typeof this.mapConfigObject.featureLayerMouseOutCallback === 'function') {

            var featureLayerMouseOutCallback = this.mapConfigObject.featureLayerMouseOutCallback;

            dojo.connect(featureLayer, 'onMouseOut', function(event) {
                //console.log('Ab.arcgis.MapExtensions -> featureLayerMouseOut...');
                featureLayerMouseOutCallback();
            });
        }

        //add layer listeners

        // this listener is also defined in the standard control (_afterLayerLoad)
        featureLayer.on('load', function(event) {
            // zoom to the extent of the layer

            if (event.layer && me.featureLayerDataOptions.zoomToExtent){
                var layerExtent = event.layer.fullExtent;
                me.map.setExtent(layerExtent);
            }

            var featureLayerLoad = me.getEventListener('featureLayerLoad');
            if (featureLayerLoad){
                featureLayerLoad();
            }
        });

        featureLayer.on('click', function(event){
            var featureClick = me.getEventListener('featureClick');
            if (featureClick) {
                var assetIdField = me.featureLayerList.get(featureLayerName).assetIdField;
                var assetTypeField = me.featureLayerList.get(featureLayerName).assetTypeField;
                var assetIdValue = event.graphic.attributes[assetIdField];
                var assetTypeValue = event.graphic.attributes[assetTypeField];
                var assetGraphic = esri.Graphic(event.graphic.geometry, null, event.graphic.attributes, null);
                featureClick(assetIdValue, assetTypeValue, assetGraphic);
            }
        }); 

        featureLayer.on('mouse-over', function(event){
            var featureMouseOver = me.getEventListener('featureMouseOver');
            if (featureMouseOver) {
                var assetIdField = me.featureLayerList.get(featureLayerName).assetIdField;
                var assetTypeField = me.featureLayerList.get(featureLayerName).assetTypeField;
                var assetIdValue = event.graphic.attributes[assetIdField];
                var assetTypeValue = event.graphic.attributes[assetTypeField];
                var assetGraphic = esri.Graphic(event.graphic.geometry, null, event.graphic.attributes, null);
                featureMouseOver(assetIdValue, assetTypeValue, assetGraphic);
            }
        }); 

        featureLayer.on('mouse-out', function(event){
            var featureMouseOut = me.getEventListener('featureMouseOut');
            if (featureMouseOut) {
                var assetIdField = me.featureLayerList.get(featureLayerName).assetIdField;
                var assetTypeField = me.featureLayerList.get(featureLayerName).assetTypeField;
                var assetIdValue = event.graphic.attributes[assetIdField];
                var assetTypeValue = event.graphic.attributes[assetTypeField];
                var assetGraphic = esri.Graphic(event.graphic.geometry, null, event.graphic.attributes, null)
                featureMouseOut(assetIdValue, assetTypeValue, assetGraphic);
            }
        }); 

    },

    /*
     * Selects/highlights assets in the active feature layer
     * @param featureLayerName
     * @param whereClause
     * @param highlightFeatures 
     *
     */
    selectFeatures: function(featureLayerName, whereClause, selectOptions) {
        //console.log('Ab.arcgis.MapExtensions -> selectFeatures...');

        var highlightFeatures = true,
            zoomToFeatures = false;
        
        if (selectOptions && selectOptions.hasOwnProperty('highlightFeatures')) {
            highlightFeatures = selectOptions.highlightFeatures;
        }
        if (selectOptions && selectOptions.hasOwnProperty('zoomToFeatures')) {
            zoomToFeatures = selectOptions.zoomToFeatures;
        }

        // hide tooltip
        this._hideFeatureLayerTooltip();

        // switch current feature layer
        // if (this.featureLayerName !== featureLayerName) {
        //     this.switchFeatureLayer(featureLayerName);
        // }

        // remove selection graphics
        this.clearSelectedFeatures();

        // prepare the feature layer query
        var query = new esri.tasks.Query();
        query.returnGeometry = true;
        query.where = whereClause;

        // get the feature layer from the map
        var featureLayer = this.map.getLayer('featureLayer');

        // set the selection symbol
        if (highlightFeatures === false) {
            featureLayer.setSelectionSymbol(this.featureLayerNoSelectionSymbol);
        } else {
            featureLayer.setSelectionSymbol(this.featureLayerSelectionSymbol);
        }

        // query features from feature layer
        if (featureLayer) {
            var mapControl = this;

            var dojoHandle = featureLayer.on('selection-complete', function(result) {
                if (zoomToFeatures) {
                    mapControl.selectFeaturesComplete(result);    
                }
                dojoHandle.remove();
            });

            //selectFeatures(query, selectionMethod?, callback?, errback?)
            featureLayer.selectFeatures(query, esri.layers.FeatureLayer.SELECTION_NEW);
        }
    },

    selectFeaturesByObjectIds: function(objectIds, selectOptions){
        //console.log('Ab.arcgis.MapExtensions -> selectFeaturesByObjectIds...');
        var highlightFeatures = true,
            zoomToFeatures = false;
        
        if (selectOptions && selectOptions.hasOwnProperty('highlightFeatures')) {
            highlightFeatures = selectOptions.highlightFeatures;
        }
        if (selectOptions && selectOptions.hasOwnProperty('zoomToFeatures')) {
            zoomToFeatures = selectOptions.zoomToFeatures;
        }

        // hide tooltip
        this._hideFeatureLayerTooltip();

        // remove selection graphics
        this.clearSelectedFeatures();

        // prepare the feature layer query
        var query = new esri.tasks.Query();
        query.objectIds = objectIds;

        // get the feature layer from the map
        var featureLayer = this.map.getLayer('featureLayer');

        // set the selection symbol
        if (highlightFeatures === false) {
            featureLayer.setSelectionSymbol(this.featureLayerNoSelectionSymbol);
        } else {
            featureLayer.setSelectionSymbol(this.featureLayerSelectionSymbol);
        }

        if (featureLayer) {
            var mapControl = this;

            var dojoHandle = featureLayer.on('selection-complete', function(result) {
                if (zoomToFeatures) {
                    mapControl.selectFeaturesComplete(result);    
                }
                dojoHandle.remove();
            });

            //selectFeatures(query, selectionMethod?, callback?, errback?)
            featureLayer.selectFeatures(query, esri.layers.FeatureLayer.SELECTION_NEW);
        }        

    },

    selectFeaturesComplete: function(result) {
        //console.log('Ab.arcgis.MapExtensions -> selectFeaturesComplete...');

        // zoom to the exent of the features
        if (result.features.length > 0) {
            var extent = esri.graphicsExtent(result.features);
            this.map.setExtent(extent, true)
        } else {
            View.alert('Feature(s) not found.');
        }

    },

    showFeatureLayer: function() {
        if (this.featureLayerControl) {
            this.featureLayerControl.show();
        }
    },

    hideFeatureLayer: function() {
        if (this.featureLayerControl) {
            this.featureLayerControl.hide();
        }
    },

    removeFeatureLayer: function() {
        if (this.featureLayerControl) {
            this.featureLayerControl.remove();
        }
    },

    _fetchFeatureLayerGeometryType: function(layerUrl) {
        View.log('MapExtensions.fetchFeatureLayerGeometryType(): start');

        var _mapControl = this;
        dojo.io.script.get({
            url: layerUrl,
            callbackParamName: "callback",
            content: {
                f: 'json'
            },
            load: function(results) {
                var geometryType = results.geometryType;
                _mapControl.featureLayerGeometryType = geometryType;
                _mapControl._createFeatureLayer();
            },
            error: function(error) {
                View.alert("Error loading feature layer: " + layerUrl);
            }
        });

        View.log('MapExtensions.fetchFeatureLayerGeometryType(): end');
    },

    _createFeatureLayerSymbols: function() {
        View.log('MapExtensions.createFeatureLayerSymbols(): start');

        var markerStyle = esri.symbol.SimpleMarkerSymbol.STYLE_CIRCLE;
        var markerSize = 15;
        
        var lineWidthLight = 1.0;
        var lineWidthMedium = 2.0;
        var lineWidthHeavy = 4.0;

        var lineSymbol = esri.symbol.SimpleLineSymbol.STYLE_SOLID;
        var fillSymbol = esri.symbol.SimpleFillSymbol.STYLE_SOLID;

        var selectionFillColor = new dojo.Color([0, 0, 0, 0.5]);
        var selectionLineColor = new dojo.Color([0, 0, 0]);
        var selectionOutline = new esri.symbol.SimpleLineSymbol(lineSymbol, selectionLineColor, lineWidthMedium);

        var noSelectionFillColor = new dojo.Color([0, 0, 0, 0.0]);
        var noSelectionLineColor = new dojo.Color([0, 0, 0]);
        var noSelectionOutline = new esri.symbol.SimpleLineSymbol(lineSymbol, noSelectionLineColor, lineWidthMedium);

        var highlightFillColor = new dojo.Color([0, 0, 0, 0.5]);
        var highlightLineColor = new dojo.Color([0, 0, 0, 0.5]);
        var highlightOutline = new esri.symbol.SimpleLineSymbol(lineSymbol, highlightLineColor, lineWidthMedium);

        switch (this.featureLayerGeometryType) {

            case 'esriGeometryPoint':
                this.featureLayerSelectionSymbol = new esri.symbol.SimpleMarkerSymbol(markerStyle, markerSize, selectionOutline, selectionFillColor);
                this.featureLayerNoSelectionSymbol = new esri.symbol.SimpleMarkerSymbol(markerStyle, markerSize, noSelectionOutline, noSelectionFillColor);
                this.featureLayerHighlightSymbol = new esri.symbol.SimpleMarkerSymbol(markerStyle, markerSize, highlightOutline, highlightFillColor);
                break;

            case 'esriGeometryLine':
            case 'esriGeometryPolyline':
                this.featureLayerSelectionSymbol = new esri.symbol.SimpleLineSymbol(lineSymbol, selectionLineColor, lineWidthHeavy);
                this.featureLayerNoSelectionSymbol = new esri.symbol.SimpleLineSymbol(lineSymbol, noSelectionLineColor, lineWidthHeavy);
                this.featureLayerHighlightSymbol = new esri.symbol.SimpleLineSymbol(lineSymbol, highlightLineColor, lineWidthHeavy);
                break;

            case 'esriGeometryPolygon':
                this.featureLayerSelectionSymbol = new esri.symbol.SimpleFillSymbol(fillSymbol, selectionOutline, selectionFillColor);
                this.featureLayerNoSelectionSymbol = new esri.symbol.SimpleFillSymbol(fillSymbol, noSelectionOutline, noSelectionFillColor);
                this.featureLayerHighlightSymbol = new esri.symbol.SimpleFillSymbol(fillSymbol, highlightOutline, highlightFillColor);
                break;
        }

        View.log('MapExtensions.createFeatureLayerSymbols(): end');

    },

    // clear selected features from the feature layer
    clearSelectedFeatures: function() {
        if (this.featureLayerControl) {
            this.featureLayerControl.clearSelection();
        }
    },

    // clear all feature graphics from the feature layer
    clearFeatures: function() {
        if (this.featureLayerControl) {
            this.featureLayerControl.clear();
            // clear the legend content
            this._clearFeatureLayerLegendContent();
            // clear the info window content
            this._clearFeatureLayerInfoWindowContent();
            this.hideFeatureLayerInfoWindow();
        }
    },

    showFeatureLayerInfoWindow: function(htmlContent) {
        dojo.style(this.divId + '_featureLayerInfoWindow', {
            'display': 'block'
        });
        this._updateFeatureLayerInfoWindowContent(htmlContent);
    },

    hideFeatureLayerInfoWindow: function() {
        dojo.style(this.divId + '_featureLayerInfoWindow', {
            'display': 'none'
        });
    },

    _updateFeatureLayerInfoWindowContent: function(htmlContent) {
        document.getElementById(this.divId + '_featureLayerInfoWindowContent').innerHTML = htmlContent;
    },

    _clearFeatureLayerInfoWindowContent: function() {
        document.getElementById(this.divId + '_featureLayerInfoWindowContent').innerHTML = '';
    },

    _getDistinctFieldValues: function(field, whereClause){
        var values = [];
        try {
            var temp = field.split(".");
            var table = temp[0];
            var parameters = {
                tableName: table,
                fieldNames: toJSON([field]),
                sortValues: toJSON([{'fieldName': field, 'sortOrder':1}]), 
                recordLimit: 0,         
                isDistinct: true,
                restriction: toJSON(whereClause)
            };
  
            var result = Workflow.call('AbCommonResources-getDataRecords', parameters);
            var rows = result.data.records;
            for(var i=0; i<rows.length; i++){
                values.push(rows[i][field]);
            }
            return values;
        } catch (e) {
            Workflow.handleError(e);
        }
        return values;
    },

    _getRecordsFromWebCentral: function(table, fields, whereClause) {
        var featureRecords;

        if (!whereClause) {var whereClause = '1=1';}
        
        try {
            var parameters = {
                tableName: table,
                fieldNames: toJSON(fields),
                restriction: toJSON(whereClause)
            };
            var result = Workflow.call('AbCommonResources-getDataRecords', parameters);
            featureRecords = result.data.records;

        } catch (e) {
            Workflow.handleError(e);
        }

        return featureRecords;
    }

});

/*
 *   This control defines the ArcGIS Feature Layer 
 *   This control is used with the Extensions for ArcGIS
 *   It displays features from an ArcGIS map service
 */


Ab.arcgis.FeatureLayer = Base.extend({

    // @begin_translatable
    //z_MESSAGE_QUERY_xxx: '',
    //z_MESSAGE_QUERY_yyy: '',
    // @end_translatable

    //the Ab.arcgis.ArcGISMap associated with the tool
    mapControl: null,

    /*
     *  The constructor.
     *  @param mapParam. The associated Ab.arcgis.MapExtensions control.
     *
     */
    constructor: function(mapParam) {
        View.log('FeatureLayer.constructor.: start');

        this.mapControl = mapParam;

        View.log('FeatureLayer.constructor.: end');
    },



    /*
     * Clear the current feature layer and
     * remove any associated graphics from the map.
     */
    clear: function() {
        // clear all features from feature layer
        var featureLayer = this.mapControl.map.getLayer('featureLayer');
        if (featureLayer) {
            featureLayer.clear();
            //this.mapControl.featureLayerName = null;
        }

        // clear graphics layers
        this.mapControl.featureLayerHighlightGraphics.clear();

    },

    clearSelection: function() {
        // clear selected features from feature layer
        var featureLayer = this.mapControl.map.getLayer('featureLayer');
        if (featureLayer) {
            featureLayer.clearSelection();
            //this.mapControl.activeFeatureLayerName = null;
        }

        // clear graphics layers
        this.mapControl.featureLayerHighlightGraphics.clear();
    },

    hide: function() {
        var featureLayer = this.mapControl.map.getLayer('featureLayer');
        if (featureLayer) {
            featureLayer.hide();
        }
    },

    remove: function() {
        var featureLayer = this.mapControl.map.getLayer('featureLayer');
        if (featureLayer) {
            this.mapControl.map.removeLayer(featureLayer);
            this.mapControl.featureLayerName = null;
            this.mapControl.featureLayerUrl = null;
            this.mapControl.featureLayerOptions = null;
            this.mapControl.featureLayerData = null;
            this.mapControl.featureLayerRendererOptions = null;
            this.mapControl.featureLayerDataOptions = null;
        }
    },

    show: function() {
        var featureLayer = this.mapControl.map.getLayer('featureLayer');
        if (featureLayer) {
            featureLayer.show();
        }
    }

});