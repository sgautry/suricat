
var resetPasswordController = View.createController('ab-reset-password', {

    isPasswordUppercase: false,
    
    afterViewLoad: function() {
        // focus on the New Password control
    	$('newPassword').focus();
    },
    
    resetPasswordForm_onCancel: function(){
	    View.closeThisDialog();
    },
    
    resetPasswordForm_onReset: function(){
        var newPassword = $('newPassword').value;
        var newPasswordConfirm = $('newPasswordConfirm').value;
        
        if(this.isPasswordUppercase){
        	if(valueExistsNotEmpty(newPassword)){
        		newPassword = newPassword.toUpperCase();
        	}
        	if(valueExistsNotEmpty(newPasswordConfirm)){
        		newPasswordConfirm = newPasswordConfirm.toUpperCase();
        	}
        }
        
        // verify that both new passwords match
        if (newPassword != newPasswordConfirm) {
            // display error message to the user
            var message = getMessage('newPasswordsNoMatch');
            View.showMessage('error', message);
            
            // clear new password controls
            $('newPassword').value = '';
            $('newPasswordConfirm').value = '';
            
            return;
        }
        
        this.resetPassword(newPassword);
    },
    
    resetPassword: function(newPassword){
        var controller = this;
        SecurityService.resetPassword(doSecure(newPassword), {
            callback: function(){
                View.showMessage('message', getMessage('passwordWasReset'), '', '', function() {
                    View.closeThisDialog();
                });
            },
            errorHandler: function(m, e){
                // show error message
                View.showException(e, '');
            }
        });
    }
});
