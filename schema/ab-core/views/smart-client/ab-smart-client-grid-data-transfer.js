// ab-smart-client-grid-data-transfer.js
//
// Controller for SmartClient's export / report / dataTransfer view
// Read the url parameter as the name of the dynamic dataSource view to load into the framed panel.
// Set up an afterLoad event listener to run after the dynamically loaded dataSource & grid has been loaded into the view.
// EventListener gets a handle to the grid containing the data, then creates an exportPanel command to transfer the data in the desired format.
//
// To run outside of SmartClient, call with 
// http://localhost:8080/archibus/schema/ab-core/views/smart-client/ab-smart-client-grid-data-transfer.axvw?dataSource=ab-grid-data-transfer-ds-ai.axvw&transferType=Export
//
// Backwardly compatible with .Net native browser control pre-Bali6 using window.external.GetDataTransferType()

var transferController = View.createController('transferController', {
	
	dataTransferType: '',

	dynamicDataSourceView: '',

	afterInitialDataFetch: function() {
		this.dynamicDataSourcePanel.addEventListener('afterLoad', this.afterDataSourcesLoaded.createDelegate(this));
		
		for (var name in window.location.parameters) {
			if (name === 'dataSource') {
				this.dynamicDataSourceView = window.location.parameters[name];
			}
			else if (name === 'transferType') {
				this.dataTransferType = window.location.parameters[name].toLowerCase();			}
		}

		this.dynamicDataSourcePanel.loadView(this.dynamicDataSourceView);
	},		
	
    /**
     * The view panel calls the afterLoad event listener after the content view has been loaded.
	 * Get the grid and create an exportPanel command on it.
     */
    afterDataSourcesLoaded: function() {
      	// The content view has a separate View object nested in the child frame. 
		// The grid with data is a control within that view.
        var contentFrame = this.dynamicDataSourcePanel.getContentFrame();
        var contentView = contentFrame.View;		
		var grid = contentView.getControl('', 'smartClientDataTransferDataSource_grid');
		var ctx = grid.createEvaluationContext();

		this.noticePanel.show(false, true);
		
		var outputType;
		var commandConfig = {};
		
		if (this.dataTransferType.length == 0) {
			try {				
				this.dataTransferType = window.external.GetDataTransferType();
				this.dataTransferType = this.dataTransferType.toLowerCase();
			}
			catch(e) {
				alert("Unable to get transfer type.");
			}
		}
		
		if (this.dataTransferType == 'datatransfer') {
			outputType = 'txfr';
			this.dynamicDataSourcePanel.title = 'DATA';
			commandConfig.isExportDocument = "true";
			commandConfig.isImportDocument = "true";
		}
		else if (this.dataTransferType == 'export') {
			outputType = 'xls';
			this.dynamicDataSourcePanel.title = 'XLS';
		}
		else if (this.dataTransferType == 'report') {
			outputType = "docx";
			this.dynamicDataSourcePanel.title = 'DOC';
		}
		else {
			return false;
		}

		commandConfig.type = 'exportPanel';
		commandConfig.outputType = outputType;
		commandConfig.target = 'dynamicDataSourcePanel';
		commandConfig.parentPanelId = 'dynamicDataSourcePanel';
		commandConfig.useDialog = "true";
		commandConfig.panelId = 'smartClientDataTransferDataSource_grid';
		commandConfig.recordLimit = 0;
		
		var command = new Ab.command.exportPanel(commandConfig);
		this.dataTransferType = '';
		this.dynamicDataSourceView = '';
		command.handle(ctx);
	}
});
