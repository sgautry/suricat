/*
 * ab-pgnav
 * Copyright(c) 2006-2008, ARCHIBUS Inc.
 * 
 * 
 */


var Ab = window.Ab || {};


Ab.namespace = function( sNameSpace ) {

    if (!sNameSpace || !sNameSpace.length) {
        return null;
    }

    var levels = sNameSpace.split(".");

    var currentNS = Ab;

    // Ab is implied, so it is ignored if it is included
    for (var i=(levels[0] == "Ab") ? 1 : 0; i<levels.length; ++i) {
        currentNS[levels[i]] = currentNS[levels[i]] || {};
        currentNS = currentNS[levels[i]];
    }

    return currentNS;
};

// from common.js

function valueExists(value) {
    return (typeof(value) != 'undefined' && value != null);
}


function getValueIfExists(value, defaultValue) {
    return valueExists(value) ? value : defaultValue;
}


function valueExistsNotEmpty(value) {
    return (valueExists(value) && (typeof value != 'string' || $.trim(value) != ''));
}


function endsWith(str, suffix){
	return str.indexOf(suffix, str.length - suffix.length) !== -1;
}


function getLocalizedString(key3) {
	var localString = key3;
	if (typeof(Ab.localization) != 'undefined') {
		for (var i=0, msg; msg = Ab.localization.Localization.localizedStrings[i]; i++) {
			if (msg.key3 == key3) {
				(msg.value == '') ? localString = '*' + msg.key3 : localString = msg.value;
				return localString;
			}
		}
	}
		
	if (valueExistsNotEmpty(localString)){
		localString = convertFromXMLValue(localString);
	}
		
	return localString;
}

 
 function convertFromXMLValue(fieldValue){
 	if(typeof fieldValue != "undefined" && fieldValue != null){
		fieldValue = fieldValue.replace(/&amp;/g, '&')
		fieldValue = fieldValue.replace(/&gt;/g, '>');
		fieldValue = fieldValue.replace(/&lt;/g, '<');
		fieldValue = fieldValue.replace(/&apos;/g, '\'');
		fieldValue = fieldValue.replace(/&quot;/g, '\"');
		return fieldValue;
	}
	return "";
 }



$.extend(Function.prototype, {
     
    createCallback : function(){
        // make args available, in function below
        var args = arguments;
        var method = this;
        return function() {
            return method.apply(window, args);
        };
    },
    
    createDelegate : function(obj, args, appendArgs){
        var method = this;
        return function() {
            var callArgs = args || arguments;
            if(appendArgs === true){
                callArgs = Array.prototype.slice.call(arguments, 0);
                callArgs = callArgs.concat(args);
            }else if(typeof appendArgs == "number"){
                callArgs = Array.prototype.slice.call(arguments, 0); // copy arguments first
                var applyArgs = [appendArgs, 0].concat(args); // create method call params
                Array.prototype.splice.apply(callArgs, applyArgs); // splice them in
            }
            return method.apply(obj || window, callArgs);
        };
    },

    
    defer : function(millis, obj, args, appendArgs){
        var fn = this.createDelegate(obj, args, appendArgs);
        if(millis){
            return setTimeout(fn, millis);
        }
        fn();
        return 0;
    }
});


Ab.namespace('workflow');


Ab.workflow.Workflow = Base.extend({
    // no instance data or methods    
},
{
    // static data and methods

	// moved from ab-view.js because that is not included in navigator
    // set to true when the session timeout is detected
    sessionTimeoutDetected: false,

    
    // default WFR call timeout: 10 seconds
    DEFAULT_TIMEOUT: 10,
    
    // DWR error message when script session has expired or is invalid
    ERROR_SCRIPT_SESSION: 'Attempt to fix script session',

    // DWR error message when the server returns no data (happens in SSO mode when script session has expired)
    ERROR_NO_DATA: 'No data received from server',

    // @begin_translatable
    z_MESSAGE_RULE_EXECUTED: 'Workflow rule executed',
    z_MESSAGE_RULE_FAILED: 'Workflow rule failed',
    z_MESSAGE_RULE_NOT_FOUND: 'Workflow rule not found',
    z_MESSAGE_RULE_NOT_PERMITTED: 'Workflow rule not permitted',
    z_MESSAGE_CONTAINER_FAILED: 'Workflow rules container failed',
    z_MESSAGE_SESSION_TIMEOUT_TITLE: 'Session Timeout',
    z_MESSAGE_SESSION_TIMEOUT: 'Your session has expired, and you have been signed out of WebCentral. To continue working, please sign in again.',
    // @end_translatable
    
    
    runRule: function(workflowRuleId, parameters, callbackFunction, callbackObject, timeout) {
        try {
            if (!valueExists(timeout)) {
                timeout = Ab.workflow.Workflow.DEFAULT_TIMEOUT;
            }
            var systemCallback = new Ab.workflow.Callback(callbackFunction, callbackObject);
            var callbackDelegate = systemCallback.afterRuleExecuted.createDelegate(systemCallback);
            var options = {
                'callback': callbackDelegate,
                'async': false,
                'timeout': timeout * 1000
            }
            this.prapareParameters(parameters);
            workflow.runWorkflowRule(workflowRuleId, parameters, options);
        } catch (e) {
            e.description = 'Workflow rule failed';//View.getLocalizedString(Ab.workflow.Workflow.z_MESSAGE_RULE_FAILED) + workflowRuleId;
            throw e;
        }
    },
    
    
    runRuleAndReturnResult: function(workflowRuleId, parameters, timeout) {
        try {
            if (!valueExists(timeout)) {
                timeout = Ab.workflow.Workflow.DEFAULT_TIMEOUT;
            }
            var systemCallback = new Ab.workflow.Callback(null, null);
            var callbackDelegate = systemCallback.afterRuleExecuted.createDelegate(systemCallback);
            var options = {
                'callback': callbackDelegate,
                'async': false,
                'timeout': timeout * 1000
            }
            
            this.prapareParameters(parameters);
            workflow.runWorkflowRule(workflowRuleId, parameters, null, options);
            var result = systemCallback.result;
            
			if (!result) {
				result = {};
				result.code = 'ruleFailed';
			}
            else if (!valueExists(result.message)) {
                result.message = 'Workflow rule executed: ' + workflowRuleId;
            }
            
            return result;
        } catch (e) {
            e.description = 'Workflow rule failed: '+ workflowRuleId;//View.getLocalizedString(Ab.workflow.Workflow.z_MESSAGE_RULE_FAILED) + ': ' + workflowRuleId;
            throw e;
        }
    },
    
    
    runRuleWithUpload: function(workflowRuleId, parameters, uploadElement, callbackFunction, callbackObject, timeout) {
        try {
            if (!valueExists(timeout)) {
                timeout = Ab.workflow.Workflow.DEFAULT_TIMEOUT;
            }
            var systemCallback = new Ab.workflow.Callback(callbackFunction, callbackObject);
            var callbackDelegate = systemCallback.afterRuleExecuted.createDelegate(systemCallback);
            var options = {
                'callback': callbackDelegate,
                'timeout': timeout * 1000
            }
            
            this.prapareParameters(parameters);
            workflow.runWorkflowRule(workflowRuleId, parameters, uploadElement, options);
            
        } catch (e) {
            e.description = 'Workflow rule failed';//View.getLocalizedString(Ab.workflow.Workflow.z_MESSAGE_RULE_FAILED) + ': ' + workflowRuleId;
            throw e;
        }
    },
    
    
    call: function(workflowRuleId, parameters, timeout) {
        if (!valueExists(parameters)) {
            parameters = {};
        }
        if (parameters.constructor == Ab.data.Record) {
            parameters = {
                record: toJSON(parameters)
            };
        } else if (parameters.constructor == Ab.data.DataSetList) {
            parameters = {
                records: toJSON(parameters)
            };
        } else if (parameters.constructor == Array) {
            parameters = {
                records: toJSON(parameters)
            };
        }
        var result = this.runRuleAndReturnResult(workflowRuleId, parameters, timeout);
        // check if there was a session timeout
        if (result.code == 'sessionTimeout') {
            this.handleSessionTimeout(result.message);
        } else if (result.code != 'executed') {
            throw result;
        }
        return result;
    },
    
    
    callMethod: function(workflowRuleId) {
        var methodParameters = [];
        for (var i = 1; i < arguments.length; i++) {
            methodParameters.push(arguments[i]);
        }
        var result = this.call(workflowRuleId, {
            methodParameters: toJSON(methodParameters)
        });
        return result;
    },
    
    
    callMethodWithParameters: function(workflowRuleId, parameters) {
        return this.call(workflowRuleId, parameters);
    },
    
    
    handleError: function(result, callback, winLocation) {

		alert('Error ' + result.message);
    },
    
    
    handleSessionTimeout: function(url) {
        // set flag to avoid Service call on logout & prohibit all message dialogs
        this.sessionTimeoutDetected = true;

		// alert('Session timeout ' + url);

        // top.View.sessionTimeoutDetected = true;

        //var title = Ab.workflow.Workflow.z_MESSAGE_SESSION_TIMEOUT_TITLE;
        var message = getLocalizedString(Ab.workflow.Workflow.z_MESSAGE_SESSION_TIMEOUT);
        alert(message);
        // when the user clicks OK, redirect to the logout page
        top.location = url;
    },

     
    handleDwrError: function(message, ex) {
        // DWR has its own session timeout check which may bypass our server-side timeout check
    	// KB 3029607: in SSO mode, if the security interceptor throws an exception, there is no response data 
		alert('Workflow error ' + message);
    }, 
    
     
    handleDwrWarning: function(message, ex) {
        //View.log(message, 'warn', 'Workflow');
		alert('Workflow warning ' + message);
    },
    
    resultCodeToMessage: function(code) {
        var message = 'Workflow rule executed';//View.getLocalizedString(Ab.workflow.Workflow.z_MESSAGE_RULE_EXECUTED);
        if (code == 'ruleFailed') {
            message = 'Workflow rule failed';//View.getLocalizedString(Ab.workflow.Workflow.z_MESSAGE_RULE_FAILED);
        } else if (code == 'ruleNotFound') {
            message = 'Workflow rule not found';//View.getLocalizedString(Ab.workflow.Workflow.z_MESSAGE_RULE_NOT_FOUND);
        } else if (code == 'containerFailed') {
            message = 'Workflow container failed';//View.getLocalizedString(Ab.workflow.Workflow.z_MESSAGE_CONTAINER_FAILED);
        } else if (code == 'ruleNotPermitted') {
            message = 'Workflow rule not permitted';//View.getLocalizedString(Ab.workflow.Workflow.z_MESSAGE_RULE_NOT_PERMITTED);
        } else if (code == 'sessionTimeout') {
            message = 'Workflow session timeout';//View.getLocalizedString(Ab.workflow.Workflow.z_MESSAGE_SESSION_TIMEOUT);
        }
        return message;
    } ,
    
    // ----------------------- job-related methods ------------------------------------------------

    
    startJob: function(workflowRuleId) {
        var methodParameters = [];
        for (var i = 1; i < arguments.length; i++) {
            methodParameters.push(arguments[i]);
        }
        
        methodParameters = toJSON(methodParameters);
        
        var result = this.call(workflowRuleId, {
            methodParameters: methodParameters,
            startAsJob: true
        }, null, arguments[1]);
        
        return result.message;
    },

    
    startJobWithUpload: function(workflowRuleId, uploadElement, callbackFunction, callbackObject) {
        var methodParameters = [];
        for (var i = 4; i < arguments.length; i++) {
            methodParameters.push(arguments[i]);
        }
        var result = this.runRuleWithUpload(workflowRuleId, {
            methodParameters: toJSON(methodParameters),
            startAsJob: true
        }, uploadElement, callbackFunction, callbackObject);
    },

    
    getJobStatus: function(jobId) {
        try {
            var result = Workflow.call('AbCommonResources-getJobStatus', {jobId: jobId});
            var jobStatus = result.data;
            if (valueExists(jobStatus.data)) {
                jobStatus.dataSet = Ab.data.createDataSet(jobStatus.data)
            }
            return jobStatus;
        } catch (e) {
            Workflow.handleError(e);
        }
    },
    
    
    stopJob: function(jobId){
        try {
            var result = Workflow.call('AbCommonResources-stopJob', {jobId: jobId});
            return result.data;
        } catch (e) {
            Workflow.handleError(e);
        }
    },
    
    
    terminateJob: function(jobId){
        try {
            var result = Workflow.call('AbCommonResources-terminateJob', {jobId: jobId});
            return result;
        } catch (e) {
            Workflow.handleError(e);
        }
    },
    
    
    getJobStatusesForUser: function(){
        try {
            var result = Workflow.call('AbCommonResources-getJobStatusesForUser');
            return result.data;
        } catch (e) {
            Workflow.handleError(e);
        }
    },
    
    
    prapareParameters: function(parameters) {
    	// ensure that the view version is set to 2.0 (this class can be only used by 2.0 views)
        if (valueExists(parameters) && !valueExists(parameters.version)) {
            parameters.version = '2.0';
        }
        
        // encode reserved SQL words in the SQL restriction string (but not in the parsed restriction)
        if (valueExists(parameters) && valueExists(parameters.restriction) && 
        		parameters.restriction.constructor === String &&
        		parameters.restriction.length > 0 &&
        		parameters.restriction.charAt(0) !== '{' &&
        		parameters.restriction.charAt(0) !== '[') {
        	parameters.restriction = doSecure(parameters.restriction);
        }
    }
});


Workflow = Ab.workflow.Workflow;



Ab.workflow.Callback = Base.extend({

    // user-defined callback function reference (can be global function or object method)
    callbackFunction: null,
    
    // user-defined callback object instance (undefined if the callback is a global function)
    callbackObject: null,
    
    // placeholder for the WFR result object
    result: null,

        
    constructor: function(callbackFunction, callbackObject) {
        this.callbackFunction = callbackFunction;
        this.callbackObject = callbackObject;
    },
    
    
    afterRuleExecuted: function(result) {
        this.result = result;
        
        //View.log('rule executed: [' + result.jsonExpression + ']', 'info', 'workflow');
        
        // evaluate the JSON expression and save the data into the result object
        result.data = null;
        result.dataSet = null;
        if (valueExists(result.jsonExpression) && result.jsonExpression != '') {
            result.data = eval('(' + result.jsonExpression + ')');

            //View.log('data evaluated', 'info', 'workflow');
            
            if (valueExists(result.data.type)) {
                result.dataSet = Ab.data.createDataSet(result.data);
            } else if (valueExists(result.data.records)) {
                result.dataSet = new Ab.data.DataSetList(result.data);
            }

            //View.log('data set created', 'info', 'workflow');
        }
        
        // call user-defined callback function
        var fn = this.callbackFunction;
        if (valueExists(fn)) {
            if (valueExists(this.callbackObject)) {
                fn.call(this.callbackObject, result);
            } else {
                fn.call(window, result);
            }
        }
    }
});



 

//// mouse events ////


// use globals to track mouse position
var hovertipMouseX;
var hovertipMouseY;
function hovertipMouseUpdate(e) {
  var mouse = hovertipMouseXY(e);
  hovertipMouseX = mouse[0] - 25;
  hovertipMouseY = mouse[1] + 15; // move location down to accomodate arrow
}

// http://www.howtocreate.co.uk/tutorials/javascript/eventinfo
function hovertipMouseXY(e) {
  if( !e ) {
    if( window.event ) {
      //Internet Explorer
      e = window.event;
    } else {
      //total failure, we have no way of referencing the event
      return;
    }
  }
  if( typeof( e.pageX ) == 'number' ) {
    //most browsers
    var xcoord = e.pageX;
    var ycoord = e.pageY;
  } else if( typeof( e.clientX ) == 'number' ) {
    //Internet Explorer and older browsers
    //other browsers provide this, but follow the pageX/Y branch
    var xcoord = e.clientX;
    var ycoord = e.clientY;
    var badOldBrowser = ( window.navigator.userAgent.indexOf( 'Opera' ) + 1 ) ||
      ( window.ScriptEngine && ScriptEngine().indexOf( 'InScript' ) + 1 ) ||
      ( navigator.vendor == 'KDE' );
    if( !badOldBrowser ) {
      if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
        //IE 4, 5 & 6 (in non-standards compliant mode)
        xcoord += document.body.scrollLeft;
        ycoord += document.body.scrollTop;
      } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
        //IE 6 (in standards compliant mode)
        xcoord += document.documentElement.scrollLeft;
        ycoord += document.documentElement.scrollTop;
      }
    }
  } else {
    //total failure, we have no way of obtaining the mouse coordinates
    return;
  }
  return [xcoord, ycoord];
};



//// target selectors ////




targetSelectById = function(el, config) {
  var id;
  var selector;
  if (id = el.getAttribute('id')) {
    selector = '*['+config.attribute+'=\''+id+'\']';
    return jQuery(selector);
  }
};


targetSelectByTargetAttribute = function(el, config) {
  target_list = el.getAttribute('target');
  if (target_list) {
    // use for attribute to specify targets
    target_ids = target_list.split(' ');
    var selector = '#' + target_ids.join(',#');
    return jQuery(selector);
  }
};


targetSelectByPrevious = function(el, config) {
  return jQuery(el.previousSibling);
}


targetSelectBySiblings = function(el, config) {
  return jQuery(el).siblings();
};

//// prepare tip elements ////



// adds a close link to clicktips
clicktipPrepareWithCloseLink = function(o, config) {
  return o.append("<a class='clicktip_close'><span>close</span></a>")
  .find('a.clicktip_close').click(function(e) {
      o.hide();
      return false;
    }).end(); 
};

// ensure that hovertips do not disappear when the mouse is over them.
// also position the hovertip as an absolutely positioned child of body.
hovertipPrepare = function(o, config) {
  return o.hover(function() {
      hovertipHideCancel(this);
    }, function() {
      hovertipHideLater(this);
    }).css('position', 'absolute').each(hovertipPosition);
};

// do not modify tooltips when preparing
hovertipPrepareNoOp = function(o, config) {
  return o;
};


//// manipulate tip elements /////


// move tooltips to body, so they are not descended from other absolutely
// positioned elements.
hovertipPosition = function(i) {
  document.body.appendChild(this);
};


hovertipIsVisible = function(el) {
  return (jQuery.css(el, 'display') != 'none');
};


// show the tooltip under the mouse.
// Introduce a delay, so tip appears only if cursor rests on target for more than an instant.
hovertipShowUnderMouse = function(el) {
  hovertipHideCancel(el);
  if (!hovertipIsVisible(el)) {
    el.ht.showing = // keep reference to timer
      window.setTimeout(function() {
          el.ht.tip.css({
              'position':'absolute',
                'top': hovertipMouseY + 'px',
                'left': hovertipMouseX + 'px',
                'z-index': 1200
          })
            .show();
        }, el.ht.config.showDelay);
  }
};

// do not hide
hovertipHideCancel = function(el) {
  if (el.ht.hiding) {
    window.clearTimeout(el.ht.hiding);
    el.ht.hiding = null;
  }  
};

// Hide a tooltip, but only after a delay.
// The delay allow the tip to remain when user moves mouse from target to tooltip
hovertipHideLater = function(el) {
  if (el.ht.showing) {
    window.clearTimeout(el.ht.showing);
    el.ht.showing = null;
  }
  if (el.ht.hiding) {
    window.clearTimeout(el.ht.hiding);
    el.ht.hiding = null;
  }
  el.ht.hiding = 
  window.setTimeout(function() {
      if (el.ht.hiding) {
        // fadeOut, slideUp do not work on Konqueror
        el.ht.tip.hide();
      }
    }, el.ht.config.hideDelay);
};


//// prepare target elements ////


// when clicked on target, toggle visibilty of tooltip
clicktipTargetPrepare = function(o, el, config) {
  return o.addClass(config.attribute + '_target')
  .click(function() {
      el.ht.tip.toggle();
      return false;
    });
};

// when hover over target, make tooltip appear
hovertipTargetPrepare = function(o, el, config) {
  return o.addClass(config.attribute + '_target')
  .hover(function() {
      // show tip when mouse over target
      hovertipShowUnderMouse(el);
    },
    function() {
      // hide the tip
      // add a delay so user can move mouse from the target to the tip
      hovertipHideLater(el);
    });
};



jQuery.fn.hovertipActivate = function(config, targetSelect, tipPrepare, targetPrepare) {
  //alert('activating ' + this.size());
  // unhide so jquery show/hide will work.
  return this.css('display', 'block')
  .hide() // don't show it until click
  .each(function() {
      if (!this.ht)
        this.ht = new Object();
      this.ht.config = config;
      
      // find our targets
      var targets = targetSelect(this, config);
      if (targets && targets.size()) {
        if (!this.ht.targets)
          this.ht.targets = targetPrepare(targets, this, config);
        else
          this.ht.targets.add(targetPrepare(targets, this, config));
        
        // listen to mouse move events so we know exactly where to place hovertips
        targets.mousemove(hovertipMouseUpdate);
        
        // prepare the tooltip element
        // is it bad form to call jQuery(this) here?
        if (!this.ht.tip)
          this.ht.tip = tipPrepare(jQuery(this), config);
      }
      
    })
  ;
};


function hovertipInit() {
  // specify the attribute name we use for our clicktips
  var clicktipConfig = {'attribute':'clicktip'};
  
  
  window.setTimeout(function() {
    jQuery('.clicktip').hovertipActivate(clicktipConfig,
                                    targetSelectById,
                                    clicktipPrepareWithCloseLink,
                                    clicktipTargetPrepare);
  }, 0);
  
  
  window.setTimeout(function() {
    jQuery('.clicktip').hovertipActivate(clicktipConfig,
                                    targetSelectByTargetAttribute,
                                    clicktipPrepareWithCloseLink,
                                    clicktipTargetPrepare);
  }, 0);
  
  // specify our configuration for hovertips, including delay times (millisec)
  var hovertipConfig = {'attribute':'hovertip',
                        'showDelay': 300,
                        'hideDelay': 700};
  
  // use <div class='hovertip'>blah blah</div>
  var hovertipSelect = 'div.hovertip';
  
var test1 =  jQuery(hovertipSelect);

  // OPTIONAL: here we wrap each hovertip to apply special effect. (i.e. drop shadow):
  jQuery(hovertipSelect).css('display', 'block').addClass('hovertip_wrap3').
    wrap("<div class='hovertip_wrap0'><div class='hovertip_wrap1'><div class='hovertip_wrap2'>" +
         "</div></div></div>").each(function() {
           // fix class and attributes for newly wrapped elements
           var tooltip = this.parentNode.parentNode.parentNode;
           if (this.getAttribute('target'))
             tooltip.setAttribute('target', this.getAttribute('target'));
           if (this.getAttribute('id')) {
             var id = this.getAttribute('id');
             this.removeAttribute('id');
             tooltip.setAttribute('id', id);
           }
         });

  jQuery('div.hovertip_wrap3').before('<div class="hovertip-arrow"></div>');

var test3 = jQuery('div.hovertip_wrap3').length;

  hovertipSelect = 'div.hovertip_wrap0';
  
  // end optional FX section
  
  
  window.setTimeout(function() {
    jQuery(hovertipSelect).hovertipActivate(hovertipConfig,
                                       targetSelectById,
                                       hovertipPrepare,
                                       hovertipTargetPrepare);
  }, 0);

  
  window.setTimeout(function() {
    jQuery(hovertipSelect).hovertipActivate(hovertipConfig,
                                       targetSelectByTargetAttribute,
                                       hovertipPrepare,
                                       hovertipTargetPrepare);
  }, 0);
  
  
  var hovertipSpanSelect = 'span.hovertip';
  // activate hovertips with wrappers for FX (drop shadow):
  jQuery(hovertipSpanSelect).css('display', 'block').addClass('hovertip_wrap3').
    wrap("<span class='hovertip_wrap0'><span class='hovertip_wrap1'><span class='hovertip_wrap2'>" + 
         "</span></span></span>").each(function() {
           // fix class and attributes for newly wrapped elements
           var tooltip = this.parentNode.parentNode.parentNode;
           if (this.getAttribute('target'))
             tooltip.setAttribute('target', this.getAttribute('target'));
           if (this.getAttribute('id')) {
             var id = this.getAttribute('id');
             this.removeAttribute('id');
             tooltip.setAttribute('id', id);
           }
         });
  hovertipSpanSelect = 'span.hovertip_wrap0';

  window.setTimeout(function() {
    jQuery(hovertipSpanSelect)
      .hovertipActivate(hovertipConfig,
                        targetSelectByPrevious,
                        hovertipPrepare,
                        hovertipTargetPrepare);
  }, 0);
}



(function(window, document, undefined){

"use strict";

var _html2canvas = {},
previousElement,
computedCSS,
html2canvas;

_html2canvas.Util = {};

_html2canvas.Util.log = function(a) {
  if (_html2canvas.logging && window.console && window.console.log) {
    window.console.log(a);
  }
};

_html2canvas.Util.trimText = (function(isNative){
  return function(input) {
    return isNative ? isNative.apply(input) : ((input || '') + '').replace( /^\s+|\s+$/g , '' );
  };
})(String.prototype.trim);

_html2canvas.Util.asFloat = function(v) {
  return parseFloat(v);
};

(function() {
  // TODO: support all possible length values
  var TEXT_SHADOW_PROPERTY = /((rgba|rgb)\([^\)]+\)(\s-?\d+px){0,})/g;
  var TEXT_SHADOW_VALUES = /(-?\d+px)|(#.+)|(rgb\(.+\))|(rgba\(.+\))/g;
  _html2canvas.Util.parseTextShadows = function (value) {
    if (!value || value === 'none') {
      return [];
    }

    // find multiple shadow declarations
    var shadows = value.match(TEXT_SHADOW_PROPERTY),
      results = [];
    for (var i = 0; shadows && (i < shadows.length); i++) {
      var s = shadows[i].match(TEXT_SHADOW_VALUES);
      results.push({
        color: s[0],
        offsetX: s[1] ? s[1].replace('px', '') : 0,
        offsetY: s[2] ? s[2].replace('px', '') : 0,
        blur: s[3] ? s[3].replace('px', '') : 0
      });
    }
    return results;
  };
})();


_html2canvas.Util.parseBackgroundImage = function (value) {
    var whitespace = ' \r\n\t',
        method, definition, prefix, prefix_i, block, results = [],
        c, mode = 0, numParen = 0, quote, args;

    var appendResult = function(){
        if(method) {
            if(definition.substr( 0, 1 ) === '"') {
                definition = definition.substr( 1, definition.length - 2 );
            }
            if(definition) {
                args.push(definition);
            }
            if(method.substr( 0, 1 ) === '-' &&
                    (prefix_i = method.indexOf( '-', 1 ) + 1) > 0) {
                prefix = method.substr( 0, prefix_i);
                method = method.substr( prefix_i );
            }
            results.push({
                prefix: prefix,
                method: method.toLowerCase(),
                value: block,
                args: args
            });
        }
        args = []; //for some odd reason, setting .length = 0 didn't work in safari
        method =
            prefix =
            definition =
            block = '';
    };

    appendResult();
    for(var i = 0, ii = value.length; i<ii; i++) {
        c = value[i];
        if(mode === 0 && whitespace.indexOf( c ) > -1){
            continue;
        }
        switch(c) {
            case '"':
                if(!quote) {
                    quote = c;
                }
                else if(quote === c) {
                    quote = null;
                }
                break;

            case '(':
                if(quote) { break; }
                else if(mode === 0) {
                    mode = 1;
                    block += c;
                    continue;
                } else {
                    numParen++;
                }
                break;

            case ')':
                if(quote) { break; }
                else if(mode === 1) {
                    if(numParen === 0) {
                        mode = 0;
                        block += c;
                        appendResult();
                        continue;
                    } else {
                        numParen--;
                    }
                }
                break;

            case ',':
                if(quote) { break; }
                else if(mode === 0) {
                    appendResult();
                    continue;
                }
                else if (mode === 1) {
                    if(numParen === 0 && !method.match(/^url$/i)) {
                        args.push(definition);
                        definition = '';
                        block += c;
                        continue;
                    }
                }
                break;
        }

        block += c;
        if(mode === 0) { method += c; }
        else { definition += c; }
    }
    appendResult();

    return results;
};

_html2canvas.Util.Bounds = function (element) {
  var clientRect, bounds = {};

  if (element.getBoundingClientRect){
    clientRect = element.getBoundingClientRect();

    // TODO add scroll position to bounds, so no scrolling of window necessary
    bounds.top = clientRect.top;
    bounds.bottom = clientRect.bottom || (clientRect.top + clientRect.height);
    bounds.left = clientRect.left;

    bounds.width = element.offsetWidth;
    bounds.height = element.offsetHeight;
  }

  return bounds;
};

// TODO ideally, we'd want everything to go through this function instead of Util.Bounds,
// but would require further work to calculate the correct positions for elements with offsetParents
_html2canvas.Util.OffsetBounds = function (element) {
  var parent = element.offsetParent ? _html2canvas.Util.OffsetBounds(element.offsetParent) : {top: 0, left: 0};

  return {
    top: element.offsetTop + parent.top,
    bottom: element.offsetTop + element.offsetHeight + parent.top,
    left: element.offsetLeft + parent.left,
    width: element.offsetWidth,
    height: element.offsetHeight
  };
};

function toPX(element, attribute, value ) {
    var rsLeft = element.runtimeStyle && element.runtimeStyle[attribute],
        left,
        style = element.style;

    // Check if we are not dealing with pixels, (Opera has issues with this)
    // Ported from jQuery css.js
    // From the awesome hack by Dean Edwards
    // http://erik.eae.net/archives/2007/07/27/18.54.15/#comment-102291

    // If we're not dealing with a regular pixel number
    // but a number that has a weird ending, we need to convert it to pixels

    if ( !/^-?[0-9]+\.?[0-9]*(?:px)?$/i.test( value ) && /^-?\d/.test(value) ) {
        // Remember the original values
        left = style.left;

        // Put in the new values to get a computed value out
        if (rsLeft) {
            element.runtimeStyle.left = element.currentStyle.left;
        }
        style.left = attribute === "fontSize" ? "1em" : (value || 0);
        value = style.pixelLeft + "px";

        // Revert the changed values
        style.left = left;
        if (rsLeft) {
            element.runtimeStyle.left = rsLeft;
        }
    }

    if (!/^(thin|medium|thick)$/i.test(value)) {
        return Math.round(parseFloat(value)) + "px";
    }

    return value;
}

function asInt(val) {
    return parseInt(val, 10);
}

function parseBackgroundSizePosition(value, element, attribute, index) {
    value = (value || '').split(',');
    value = value[index || 0] || value[0] || 'auto';
    value = _html2canvas.Util.trimText(value).split(' ');

    if(attribute === 'backgroundSize' && (!value[0] || value[0].match(/cover|contain|auto/))) {
        //these values will be handled in the parent function
    } else {
        value[0] = (value[0].indexOf( "%" ) === -1) ? toPX(element, attribute + "X", value[0]) : value[0];
        if(value[1] === undefined) {
            if(attribute === 'backgroundSize') {
                value[1] = 'auto';
                return value;
            } else {
                // IE 9 doesn't return double digit always
                value[1] = value[0];
            }
        }
        value[1] = (value[1].indexOf("%") === -1) ? toPX(element, attribute + "Y", value[1]) : value[1];
    }
    return value;
}

_html2canvas.Util.getCSS = function (element, attribute, index) {
    if (previousElement !== element) {
      computedCSS = document.defaultView.getComputedStyle(element, null);
    }

    var value = computedCSS[attribute];

    if (/^background(Size|Position)$/.test(attribute)) {
        return parseBackgroundSizePosition(value, element, attribute, index);
    } else if (/border(Top|Bottom)(Left|Right)Radius/.test(attribute)) {
      var arr = value.split(" ");
      if (arr.length <= 1) {
          arr[1] = arr[0];
      }
      return arr.map(asInt);
    }

  return value;
};

_html2canvas.Util.resizeBounds = function( current_width, current_height, target_width, target_height, stretch_mode ){
  var target_ratio = target_width / target_height,
    current_ratio = current_width / current_height,
    output_width, output_height;

  if(!stretch_mode || stretch_mode === 'auto') {
    output_width = target_width;
    output_height = target_height;
  } else if(target_ratio < current_ratio ^ stretch_mode === 'contain') {
    output_height = target_height;
    output_width = target_height * current_ratio;
  } else {
    output_width = target_width;
    output_height = target_width / current_ratio;
  }

  return {
    width: output_width,
    height: output_height
  };
};

function backgroundBoundsFactory( prop, el, bounds, image, imageIndex, backgroundSize ) {
    var bgposition =  _html2canvas.Util.getCSS( el, prop, imageIndex ) ,
    topPos,
    left,
    percentage,
    val;

    if (bgposition.length === 1){
      val = bgposition[0];

      bgposition = [];

      bgposition[0] = val;
      bgposition[1] = val;
    }

    if (bgposition[0].toString().indexOf("%") !== -1){
      percentage = (parseFloat(bgposition[0])/100);
      left = bounds.width * percentage;
      if(prop !== 'backgroundSize') {
        left -= (backgroundSize || image).width*percentage;
      }
    } else {
      if(prop === 'backgroundSize') {
        if(bgposition[0] === 'auto') {
          left = image.width;
        } else {
          if (/contain|cover/.test(bgposition[0])) {
            var resized = _html2canvas.Util.resizeBounds(image.width, image.height, bounds.width, bounds.height, bgposition[0]);
            left = resized.width;
            topPos = resized.height;
          } else {
            left = parseInt(bgposition[0], 10);
          }
        }
      } else {
        left = parseInt( bgposition[0], 10);
      }
    }


    if(bgposition[1] === 'auto') {
      topPos = left / image.width * image.height;
    } else if (bgposition[1].toString().indexOf("%") !== -1){
      percentage = (parseFloat(bgposition[1])/100);
      topPos =  bounds.height * percentage;
      if(prop !== 'backgroundSize') {
        topPos -= (backgroundSize || image).height * percentage;
      }

    } else {
      topPos = parseInt(bgposition[1],10);
    }

    return [left, topPos];
}

_html2canvas.Util.BackgroundPosition = function( el, bounds, image, imageIndex, backgroundSize ) {
    var result = backgroundBoundsFactory( 'backgroundPosition', el, bounds, image, imageIndex, backgroundSize );
    return { left: result[0], top: result[1] };
};

_html2canvas.Util.BackgroundSize = function( el, bounds, image, imageIndex ) {
    var result = backgroundBoundsFactory( 'backgroundSize', el, bounds, image, imageIndex );
    return { width: result[0], height: result[1] };
};

_html2canvas.Util.Extend = function (options, defaults) {
  for (var key in options) {
    if (options.hasOwnProperty(key)) {
      defaults[key] = options[key];
    }
  }
  return defaults;
};



_html2canvas.Util.Children = function( elem ) {
  var children;
  try {
    children = (elem.nodeName && elem.nodeName.toUpperCase() === "IFRAME") ? elem.contentDocument || elem.contentWindow.document : (function(array) {
      var ret = [];
      if (array !== null) {
        (function(first, second ) {
          var i = first.length,
          j = 0;

          if (typeof second.length === "number") {
            for (var l = second.length; j < l; j++) {
              first[i++] = second[j];
            }
          } else {
            while (second[j] !== undefined) {
              first[i++] = second[j++];
            }
          }

          first.length = i;

          return first;
        })(ret, array);
      }
      return ret;
    })(elem.childNodes);

  } catch (ex) {
    _html2canvas.Util.log("html2canvas.Util.Children failed with exception: " + ex.message);
    children = [];
  }
  return children;
};

_html2canvas.Util.isTransparent = function(backgroundColor) {
  return (backgroundColor === "transparent" || backgroundColor === "rgba(0, 0, 0, 0)");
};
_html2canvas.Util.Font = (function () {

  var fontData = {};

  return function(font, fontSize, doc) {
    if (fontData[font + "-" + fontSize] !== undefined) {
      return fontData[font + "-" + fontSize];
    }

    var container = doc.createElement('div'),
    img = doc.createElement('img'),
    span = doc.createElement('span'),
    sampleText = 'Hidden Text',
    baseline,
    middle,
    metricsObj;

    container.style.visibility = "hidden";
    container.style.fontFamily = font;
    container.style.fontSize = fontSize;
    container.style.margin = 0;
    container.style.padding = 0;

    doc.body.appendChild(container);

    // http://probablyprogramming.com/2009/03/15/the-tiniest-gif-ever (handtinywhite.gif)
    img.src = "data:image/gif;base64,R0lGODlhAQABAIABAP///wAAACwAAAAAAQABAAACAkQBADs=";
    img.width = 1;
    img.height = 1;

    img.style.margin = 0;
    img.style.padding = 0;
    img.style.verticalAlign = "baseline";

    span.style.fontFamily = font;
    span.style.fontSize = fontSize;
    span.style.margin = 0;
    span.style.padding = 0;

    span.appendChild(doc.createTextNode(sampleText));
    container.appendChild(span);
    container.appendChild(img);
    baseline = (img.offsetTop - span.offsetTop) + 1;

    container.removeChild(span);
    container.appendChild(doc.createTextNode(sampleText));

    container.style.lineHeight = "normal";
    img.style.verticalAlign = "super";

    middle = (img.offsetTop-container.offsetTop) + 1;
    metricsObj = {
      baseline: baseline,
      lineWidth: 1,
      middle: middle
    };

    fontData[font + "-" + fontSize] = metricsObj;

    doc.body.removeChild(container);

    return metricsObj;
  };
})();

(function(){
  var Util = _html2canvas.Util,
    Generate = {};

  _html2canvas.Generate = Generate;

  var reGradients = [
  /^(-webkit-linear-gradient)\(([a-z\s]+)([\w\d\.\s,%\(\)]+)\)$/,
  /^(-o-linear-gradient)\(([a-z\s]+)([\w\d\.\s,%\(\)]+)\)$/,
  /^(-webkit-gradient)\((linear|radial),\s((?:\d{1,3}%?)\s(?:\d{1,3}%?),\s(?:\d{1,3}%?)\s(?:\d{1,3}%?))([\w\d\.\s,%\(\)\-]+)\)$/,
  /^(-moz-linear-gradient)\(((?:\d{1,3}%?)\s(?:\d{1,3}%?))([\w\d\.\s,%\(\)]+)\)$/,
  /^(-webkit-radial-gradient)\(((?:\d{1,3}%?)\s(?:\d{1,3}%?)),\s(\w+)\s([a-z\-]+)([\w\d\.\s,%\(\)]+)\)$/,
  /^(-moz-radial-gradient)\(((?:\d{1,3}%?)\s(?:\d{1,3}%?)),\s(\w+)\s?([a-z\-]*)([\w\d\.\s,%\(\)]+)\)$/,
  /^(-o-radial-gradient)\(((?:\d{1,3}%?)\s(?:\d{1,3}%?)),\s(\w+)\s([a-z\-]+)([\w\d\.\s,%\(\)]+)\)$/
  ];

  
  Generate.parseGradient = function(css, bounds) {
    var gradient, i, len = reGradients.length, m1, stop, m2, m2Len, step, m3, tl,tr,br,bl;

    for(i = 0; i < len; i+=1){
      m1 = css.match(reGradients[i]);
      if(m1) {
        break;
      }
    }

    if(m1) {
      switch(m1[1]) {
        case '-webkit-linear-gradient':
        case '-o-linear-gradient':

          gradient = {
            type: 'linear',
            x0: null,
            y0: null,
            x1: null,
            y1: null,
            colorStops: []
          };

          // get coordinates
          m2 = m1[2].match(/\w+/g);
          if(m2){
            m2Len = m2.length;
            for(i = 0; i < m2Len; i+=1){
              switch(m2[i]) {
                case 'top':
                  gradient.y0 = 0;
                  gradient.y1 = bounds.height;
                  break;

                case 'right':
                  gradient.x0 = bounds.width;
                  gradient.x1 = 0;
                  break;

                case 'bottom':
                  gradient.y0 = bounds.height;
                  gradient.y1 = 0;
                  break;

                case 'left':
                  gradient.x0 = 0;
                  gradient.x1 = bounds.width;
                  break;
              }
            }
          }
          if(gradient.x0 === null && gradient.x1 === null){ // center
            gradient.x0 = gradient.x1 = bounds.width / 2;
          }
          if(gradient.y0 === null && gradient.y1 === null){ // center
            gradient.y0 = gradient.y1 = bounds.height / 2;
          }

          // get colors and stops
          m2 = m1[3].match(/((?:rgb|rgba)\(\d{1,3},\s\d{1,3},\s\d{1,3}(?:,\s[0-9\.]+)?\)(?:\s\d{1,3}(?:%|px))?)+/g);
          if(m2){
            m2Len = m2.length;
            step = 1 / Math.max(m2Len - 1, 1);
            for(i = 0; i < m2Len; i+=1){
              m3 = m2[i].match(/((?:rgb|rgba)\(\d{1,3},\s\d{1,3},\s\d{1,3}(?:,\s[0-9\.]+)?\))\s*(\d{1,3})?(%|px)?/);
              if(m3[2]){
                stop = parseFloat(m3[2]);
                if(m3[3] === '%'){
                  stop /= 100;
                } else { // px - stupid opera
                  stop /= bounds.width;
                }
              } else {
                stop = i * step;
              }
              gradient.colorStops.push({
                color: m3[1],
                stop: stop
              });
            }
          }
          break;

        case '-webkit-gradient':

          gradient = {
            type: m1[2] === 'radial' ? 'circle' : m1[2], // TODO: Add radial gradient support for older mozilla definitions
            x0: 0,
            y0: 0,
            x1: 0,
            y1: 0,
            colorStops: []
          };

          // get coordinates
          m2 = m1[3].match(/(\d{1,3})%?\s(\d{1,3})%?,\s(\d{1,3})%?\s(\d{1,3})%?/);
          if(m2){
            gradient.x0 = (m2[1] * bounds.width) / 100;
            gradient.y0 = (m2[2] * bounds.height) / 100;
            gradient.x1 = (m2[3] * bounds.width) / 100;
            gradient.y1 = (m2[4] * bounds.height) / 100;
          }

          // get colors and stops
          m2 = m1[4].match(/((?:from|to|color-stop)\((?:[0-9\.]+,\s)?(?:rgb|rgba)\(\d{1,3},\s\d{1,3},\s\d{1,3}(?:,\s[0-9\.]+)?\)\))+/g);
          if(m2){
            m2Len = m2.length;
            for(i = 0; i < m2Len; i+=1){
              m3 = m2[i].match(/(from|to|color-stop)\(([0-9\.]+)?(?:,\s)?((?:rgb|rgba)\(\d{1,3},\s\d{1,3},\s\d{1,3}(?:,\s[0-9\.]+)?\))\)/);
              stop = parseFloat(m3[2]);
              if(m3[1] === 'from') {
                stop = 0.0;
              }
              if(m3[1] === 'to') {
                stop = 1.0;
              }
              gradient.colorStops.push({
                color: m3[3],
                stop: stop
              });
            }
          }
          break;

        case '-moz-linear-gradient':

          gradient = {
            type: 'linear',
            x0: 0,
            y0: 0,
            x1: 0,
            y1: 0,
            colorStops: []
          };

          // get coordinates
          m2 = m1[2].match(/(\d{1,3})%?\s(\d{1,3})%?/);

          // m2[1] == 0%   -> left
          // m2[1] == 50%  -> center
          // m2[1] == 100% -> right

          // m2[2] == 0%   -> top
          // m2[2] == 50%  -> center
          // m2[2] == 100% -> bottom

          if(m2){
            gradient.x0 = (m2[1] * bounds.width) / 100;
            gradient.y0 = (m2[2] * bounds.height) / 100;
            gradient.x1 = bounds.width - gradient.x0;
            gradient.y1 = bounds.height - gradient.y0;
          }

          // get colors and stops
          m2 = m1[3].match(/((?:rgb|rgba)\(\d{1,3},\s\d{1,3},\s\d{1,3}(?:,\s[0-9\.]+)?\)(?:\s\d{1,3}%)?)+/g);
          if(m2){
            m2Len = m2.length;
            step = 1 / Math.max(m2Len - 1, 1);
            for(i = 0; i < m2Len; i+=1){
              m3 = m2[i].match(/((?:rgb|rgba)\(\d{1,3},\s\d{1,3},\s\d{1,3}(?:,\s[0-9\.]+)?\))\s*(\d{1,3})?(%)?/);
              if(m3[2]){
                stop = parseFloat(m3[2]);
                if(m3[3]){ // percentage
                  stop /= 100;
                }
              } else {
                stop = i * step;
              }
              gradient.colorStops.push({
                color: m3[1],
                stop: stop
              });
            }
          }
          break;

        case '-webkit-radial-gradient':
        case '-moz-radial-gradient':
        case '-o-radial-gradient':

          gradient = {
            type: 'circle',
            x0: 0,
            y0: 0,
            x1: bounds.width,
            y1: bounds.height,
            cx: 0,
            cy: 0,
            rx: 0,
            ry: 0,
            colorStops: []
          };

          // center
          m2 = m1[2].match(/(\d{1,3})%?\s(\d{1,3})%?/);
          if(m2){
            gradient.cx = (m2[1] * bounds.width) / 100;
            gradient.cy = (m2[2] * bounds.height) / 100;
          }

          // size
          m2 = m1[3].match(/\w+/);
          m3 = m1[4].match(/[a-z\-]*/);
          if(m2 && m3){
            switch(m3[0]){
              case 'farthest-corner':
              case 'cover': // is equivalent to farthest-corner
              case '': // mozilla removes "cover" from definition :(
                tl = Math.sqrt(Math.pow(gradient.cx, 2) + Math.pow(gradient.cy, 2));
                tr = Math.sqrt(Math.pow(gradient.cx, 2) + Math.pow(gradient.y1 - gradient.cy, 2));
                br = Math.sqrt(Math.pow(gradient.x1 - gradient.cx, 2) + Math.pow(gradient.y1 - gradient.cy, 2));
                bl = Math.sqrt(Math.pow(gradient.x1 - gradient.cx, 2) + Math.pow(gradient.cy, 2));
                gradient.rx = gradient.ry = Math.max(tl, tr, br, bl);
                break;
              case 'closest-corner':
                tl = Math.sqrt(Math.pow(gradient.cx, 2) + Math.pow(gradient.cy, 2));
                tr = Math.sqrt(Math.pow(gradient.cx, 2) + Math.pow(gradient.y1 - gradient.cy, 2));
                br = Math.sqrt(Math.pow(gradient.x1 - gradient.cx, 2) + Math.pow(gradient.y1 - gradient.cy, 2));
                bl = Math.sqrt(Math.pow(gradient.x1 - gradient.cx, 2) + Math.pow(gradient.cy, 2));
                gradient.rx = gradient.ry = Math.min(tl, tr, br, bl);
                break;
              case 'farthest-side':
                if(m2[0] === 'circle'){
                  gradient.rx = gradient.ry = Math.max(
                    gradient.cx,
                    gradient.cy,
                    gradient.x1 - gradient.cx,
                    gradient.y1 - gradient.cy
                    );
                } else { // ellipse

                  gradient.type = m2[0];

                  gradient.rx = Math.max(
                    gradient.cx,
                    gradient.x1 - gradient.cx
                    );
                  gradient.ry = Math.max(
                    gradient.cy,
                    gradient.y1 - gradient.cy
                    );
                }
                break;
              case 'closest-side':
              case 'contain': // is equivalent to closest-side
                if(m2[0] === 'circle'){
                  gradient.rx = gradient.ry = Math.min(
                    gradient.cx,
                    gradient.cy,
                    gradient.x1 - gradient.cx,
                    gradient.y1 - gradient.cy
                    );
                } else { // ellipse

                  gradient.type = m2[0];

                  gradient.rx = Math.min(
                    gradient.cx,
                    gradient.x1 - gradient.cx
                    );
                  gradient.ry = Math.min(
                    gradient.cy,
                    gradient.y1 - gradient.cy
                    );
                }
                break;

            // TODO: add support for "30px 40px" sizes (webkit only)
            }
          }

          // color stops
          m2 = m1[5].match(/((?:rgb|rgba)\(\d{1,3},\s\d{1,3},\s\d{1,3}(?:,\s[0-9\.]+)?\)(?:\s\d{1,3}(?:%|px))?)+/g);
          if(m2){
            m2Len = m2.length;
            step = 1 / Math.max(m2Len - 1, 1);
            for(i = 0; i < m2Len; i+=1){
              m3 = m2[i].match(/((?:rgb|rgba)\(\d{1,3},\s\d{1,3},\s\d{1,3}(?:,\s[0-9\.]+)?\))\s*(\d{1,3})?(%|px)?/);
              if(m3[2]){
                stop = parseFloat(m3[2]);
                if(m3[3] === '%'){
                  stop /= 100;
                } else { // px - stupid opera
                  stop /= bounds.width;
                }
              } else {
                stop = i * step;
              }
              gradient.colorStops.push({
                color: m3[1],
                stop: stop
              });
            }
          }
          break;
      }
    }

    return gradient;
  };

  function addScrollStops(grad) {
    return function(colorStop) {
      try {
        grad.addColorStop(colorStop.stop, colorStop.color);
      }
      catch(e) {
        Util.log(['failed to add color stop: ', e, '; tried to add: ', colorStop]);
      }
    };
  }

  Generate.Gradient = function(src, bounds) {
    if(bounds.width === 0 || bounds.height === 0) {
      return;
    }

    var canvas = document.createElement('canvas'),
    ctx = canvas.getContext('2d'),
    gradient, grad;

    canvas.width = bounds.width;
    canvas.height = bounds.height;

    // TODO: add support for multi defined background gradients
    gradient = _html2canvas.Generate.parseGradient(src, bounds);

    if(gradient) {
      switch(gradient.type) {
        case 'linear':
          grad = ctx.createLinearGradient(gradient.x0, gradient.y0, gradient.x1, gradient.y1);
          gradient.colorStops.forEach(addScrollStops(grad));
          ctx.fillStyle = grad;
          ctx.fillRect(0, 0, bounds.width, bounds.height);
          break;

        case 'circle':
          grad = ctx.createRadialGradient(gradient.cx, gradient.cy, 0, gradient.cx, gradient.cy, gradient.rx);
          gradient.colorStops.forEach(addScrollStops(grad));
          ctx.fillStyle = grad;
          ctx.fillRect(0, 0, bounds.width, bounds.height);
          break;

        case 'ellipse':
          var canvasRadial = document.createElement('canvas'),
            ctxRadial = canvasRadial.getContext('2d'),
            ri = Math.max(gradient.rx, gradient.ry),
            di = ri * 2;

          canvasRadial.width = canvasRadial.height = di;

          grad = ctxRadial.createRadialGradient(gradient.rx, gradient.ry, 0, gradient.rx, gradient.ry, ri);
          gradient.colorStops.forEach(addScrollStops(grad));

          ctxRadial.fillStyle = grad;
          ctxRadial.fillRect(0, 0, di, di);

          ctx.fillStyle = gradient.colorStops[gradient.colorStops.length - 1].color;
          ctx.fillRect(0, 0, canvas.width, canvas.height);
          ctx.drawImage(canvasRadial, gradient.cx - gradient.rx, gradient.cy - gradient.ry, 2 * gradient.rx, 2 * gradient.ry);
          break;
      }
    }

    return canvas;
  };

  Generate.ListAlpha = function(number) {
    var tmp = "",
    modulus;

    do {
      modulus = number % 26;
      tmp = String.fromCharCode((modulus) + 64) + tmp;
      number = number / 26;
    }while((number*26) > 26);

    return tmp;
  };

  Generate.ListRoman = function(number) {
    var romanArray = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"],
    decimal = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1],
    roman = "",
    v,
    len = romanArray.length;

    if (number <= 0 || number >= 4000) {
      return number;
    }

    for (v=0; v < len; v+=1) {
      while (number >= decimal[v]) {
        number -= decimal[v];
        roman += romanArray[v];
      }
    }

    return roman;
  };
})();
function h2cRenderContext(width, height) {
  var storage = [];
  return {
    storage: storage,
    width: width,
    height: height,
    clip: function() {
      storage.push({
        type: "function",
        name: "clip",
        'arguments': arguments
      });
    },
    translate: function() {
      storage.push({
        type: "function",
        name: "translate",
        'arguments': arguments
      });
    },
    fill: function() {
      storage.push({
        type: "function",
        name: "fill",
        'arguments': arguments
      });
    },
    save: function() {
      storage.push({
        type: "function",
        name: "save",
        'arguments': arguments
      });
    },
    restore: function() {
      storage.push({
        type: "function",
        name: "restore",
        'arguments': arguments
      });
    },
    fillRect: function () {
      storage.push({
        type: "function",
        name: "fillRect",
        'arguments': arguments
      });
    },
    createPattern: function() {
      storage.push({
        type: "function",
        name: "createPattern",
        'arguments': arguments
      });
    },
    drawShape: function() {

      var shape = [];

      storage.push({
        type: "function",
        name: "drawShape",
        'arguments': shape
      });

      return {
        moveTo: function() {
          shape.push({
            name: "moveTo",
            'arguments': arguments
          });
        },
        lineTo: function() {
          shape.push({
            name: "lineTo",
            'arguments': arguments
          });
        },
        arcTo: function() {
          shape.push({
            name: "arcTo",
            'arguments': arguments
          });
        },
        bezierCurveTo: function() {
          shape.push({
            name: "bezierCurveTo",
            'arguments': arguments
          });
        },
        quadraticCurveTo: function() {
          shape.push({
            name: "quadraticCurveTo",
            'arguments': arguments
          });
        }
      };

    },
    drawImage: function () {
      storage.push({
        type: "function",
        name: "drawImage",
        'arguments': arguments
      });
    },
    fillText: function () {
      storage.push({
        type: "function",
        name: "fillText",
        'arguments': arguments
      });
    },
    setVariable: function (variable, value) {
      storage.push({
        type: "variable",
        name: variable,
        'arguments': value
      });
      return value;
    }
  };
}
_html2canvas.Parse = function (images, options) {
  window.scroll(0,0);

  var element = (( options.elements === undefined ) ? document.body : options.elements[0]), // select body by default
  numDraws = 0,
  doc = element.ownerDocument,
  Util = _html2canvas.Util,
  support = Util.Support(options, doc),
  ignoreElementsRegExp = new RegExp("(" + options.ignoreElements + ")"),
  body = doc.body,
  getCSS = Util.getCSS,
  pseudoHide = "___html2canvas___pseudoelement",
  hidePseudoElements = doc.createElement('style');

  hidePseudoElements.innerHTML = '.' + pseudoHide + '-before:before { content: "" !important; display: none !important; }' +
  '.' + pseudoHide + '-after:after { content: "" !important; display: none !important; }';

  body.appendChild(hidePseudoElements);

  images = images || {};

  function documentWidth () {
    return Math.max(
      Math.max(doc.body.scrollWidth, doc.documentElement.scrollWidth),
      Math.max(doc.body.offsetWidth, doc.documentElement.offsetWidth),
      Math.max(doc.body.clientWidth, doc.documentElement.clientWidth)
      );
  }

  function documentHeight () {
    return Math.max(
      Math.max(doc.body.scrollHeight, doc.documentElement.scrollHeight),
      Math.max(doc.body.offsetHeight, doc.documentElement.offsetHeight),
      Math.max(doc.body.clientHeight, doc.documentElement.clientHeight)
      );
  }

  function getCSSInt(element, attribute) {
    var val = parseInt(getCSS(element, attribute), 10);
    return (isNaN(val)) ? 0 : val; // borders in old IE are throwing 'medium' for demo.html
  }

  function renderRect (ctx, x, y, w, h, bgcolor) {
    if (bgcolor !== "transparent"){
      ctx.setVariable("fillStyle", bgcolor);
      ctx.fillRect(x, y, w, h);
      numDraws+=1;
    }
  }

  function capitalize(m, p1, p2) {
    if (m.length > 0) {
      return p1 + p2.toUpperCase();
    }
  }

  function textTransform (text, transform) {
    switch(transform){
      case "lowercase":
        return text.toLowerCase();
      case "capitalize":
        return text.replace( /(^|\s|:|-|\(|\))([a-z])/g, capitalize);
      case "uppercase":
        return text.toUpperCase();
      default:
        return text;
    }
  }

  function noLetterSpacing(letter_spacing) {
    return (/^(normal|none|0px)$/.test(letter_spacing));
  }

  function drawText(currentText, x, y, ctx){
    if (currentText !== null && Util.trimText(currentText).length > 0) {
      ctx.fillText(currentText, x, y);
      numDraws+=1;
    }
  }

  function setTextVariables(ctx, el, text_decoration, color) {
    var align = false,
    bold = getCSS(el, "fontWeight"),
    family = getCSS(el, "fontFamily"),
    size = getCSS(el, "fontSize"),
    shadows = Util.parseTextShadows(getCSS(el, "textShadow"));

    switch(parseInt(bold, 10)){
      case 401:
        bold = "bold";
        break;
      case 400:
        bold = "normal";
        break;
    }

    ctx.setVariable("fillStyle", color);
    ctx.setVariable("font", [getCSS(el, "fontStyle"), getCSS(el, "fontVariant"), bold, size, family].join(" "));
    ctx.setVariable("textAlign", (align) ? "right" : "left");

    if (shadows.length) {
      // TODO: support multiple text shadows
      // apply the first text shadow
      ctx.setVariable("shadowColor", shadows[0].color);
      ctx.setVariable("shadowOffsetX", shadows[0].offsetX);
      ctx.setVariable("shadowOffsetY", shadows[0].offsetY);
      ctx.setVariable("shadowBlur", shadows[0].blur);
    }

    if (text_decoration !== "none"){
      return Util.Font(family, size, doc);
    }
  }

  function renderTextDecoration(ctx, text_decoration, bounds, metrics, color) {
    switch(text_decoration) {
      case "underline":
        // Draws a line at the baseline of the font
        // TODO As some browsers display the line as more than 1px if the font-size is big, need to take that into account both in position and size
        renderRect(ctx, bounds.left, Math.round(bounds.top + metrics.baseline + metrics.lineWidth), bounds.width, 1, color);
        break;
      case "overline":
        renderRect(ctx, bounds.left, Math.round(bounds.top), bounds.width, 1, color);
        break;
      case "line-through":
        // TODO try and find exact position for line-through
        renderRect(ctx, bounds.left, Math.ceil(bounds.top + metrics.middle + metrics.lineWidth), bounds.width, 1, color);
        break;
    }
  }

  function getTextBounds(state, text, textDecoration, isLast, transform) {
    var bounds;
    if (support.rangeBounds && !transform) {
      if (textDecoration !== "none" || Util.trimText(text).length !== 0) {
        bounds = textRangeBounds(text, state.node, state.textOffset);
      }
      state.textOffset += text.length;
    } else if (state.node && typeof state.node.nodeValue === "string" ){
      var newTextNode = (isLast) ? state.node.splitText(text.length) : null;
      bounds = textWrapperBounds(state.node, transform);
      state.node = newTextNode;
    }
    return bounds;
  }

  function textRangeBounds(text, textNode, textOffset) {
    var range = doc.createRange();
    range.setStart(textNode, textOffset);
    range.setEnd(textNode, textOffset + text.length);
    return range.getBoundingClientRect();
  }

  function textWrapperBounds(oldTextNode, transform) {
    var parent = oldTextNode.parentNode,
    wrapElement = doc.createElement('wrapper'),
    backupText = oldTextNode.cloneNode(true);

    wrapElement.appendChild(oldTextNode.cloneNode(true));
    parent.replaceChild(wrapElement, oldTextNode);

    var bounds = transform ? Util.OffsetBounds(wrapElement) : Util.Bounds(wrapElement);
    parent.replaceChild(backupText, wrapElement);
    return bounds;
  }

  function renderText(el, textNode, stack) {
    var ctx = stack.ctx,
    color = getCSS(el, "color"),
    textDecoration = getCSS(el, "textDecoration"),
    textAlign = getCSS(el, "textAlign"),
    metrics,
    textList,
    state = {
      node: textNode,
      textOffset: 0
    };

    if (Util.trimText(textNode.nodeValue).length > 0) {
      textNode.nodeValue = textTransform(textNode.nodeValue, getCSS(el, "textTransform"));
      textAlign = textAlign.replace(["-webkit-auto"],["auto"]);

      textList = (!options.letterRendering && /^(left|right|justify|auto)$/.test(textAlign) && noLetterSpacing(getCSS(el, "letterSpacing"))) ?
      textNode.nodeValue.split(/(\b| )/)
      : textNode.nodeValue.split("");

      metrics = setTextVariables(ctx, el, textDecoration, color);

      if (options.chinese) {
        textList.forEach(function(word, index) {
          if (/.*[\u4E00-\u9FA5].*$/.test(word)) {
            word = word.split("");
            word.unshift(index, 1);
            textList.splice.apply(textList, word);
          }
        });
      }

      textList.forEach(function(text, index) {
        var bounds = getTextBounds(state, text, textDecoration, (index < textList.length - 1), stack.transform.matrix);
        if (bounds) {
          drawText(text, bounds.left, bounds.bottom, ctx);
          renderTextDecoration(ctx, textDecoration, bounds, metrics, color);
        }
      });
    }
  }

  function listPosition (element, val) {
    var boundElement = doc.createElement( "boundelement" ),
    originalType,
    bounds;

    boundElement.style.display = "inline";

    originalType = element.style.listStyleType;
    element.style.listStyleType = "none";

    boundElement.appendChild(doc.createTextNode(val));

    element.insertBefore(boundElement, element.firstChild);

    bounds = Util.Bounds(boundElement);
    element.removeChild(boundElement);
    element.style.listStyleType = originalType;
    return bounds;
  }

  function elementIndex(el) {
    var i = -1,
    count = 1,
    childs = el.parentNode.childNodes;

    if (el.parentNode) {
      while(childs[++i] !== el) {
        if (childs[i].nodeType === 1) {
          count++;
        }
      }
      return count;
    } else {
      return -1;
    }
  }

  function listItemText(element, type) {
    var currentIndex = elementIndex(element), text;
    switch(type){
      case "decimal":
        text = currentIndex;
        break;
      case "decimal-leading-zero":
        text = (currentIndex.toString().length === 1) ? currentIndex = "0" + currentIndex.toString() : currentIndex.toString();
        break;
      case "upper-roman":
        text = _html2canvas.Generate.ListRoman( currentIndex );
        break;
      case "lower-roman":
        text = _html2canvas.Generate.ListRoman( currentIndex ).toLowerCase();
        break;
      case "lower-alpha":
        text = _html2canvas.Generate.ListAlpha( currentIndex ).toLowerCase();
        break;
      case "upper-alpha":
        text = _html2canvas.Generate.ListAlpha( currentIndex );
        break;
    }

    return text + ". ";
  }

  function renderListItem(element, stack, elBounds) {
    var x,
    text,
    ctx = stack.ctx,
    type = getCSS(element, "listStyleType"),
    listBounds;

    if (/^(decimal|decimal-leading-zero|upper-alpha|upper-latin|upper-roman|lower-alpha|lower-greek|lower-latin|lower-roman)$/i.test(type)) {
      text = listItemText(element, type);
      listBounds = listPosition(element, text);
      setTextVariables(ctx, element, "none", getCSS(element, "color"));

      if (getCSS(element, "listStylePosition") === "inside") {
        ctx.setVariable("textAlign", "left");
        x = elBounds.left;
      } else {
        return;
      }

      drawText(text, x, listBounds.bottom, ctx);
    }
  }

  function loadImage (src){
    var img = images[src];
    return (img && img.succeeded === true) ? img.img : false;
  }

  function clipBounds(src, dst){
    var x = Math.max(src.left, dst.left),
    y = Math.max(src.top, dst.top),
    x2 = Math.min((src.left + src.width), (dst.left + dst.width)),
    y2 = Math.min((src.top + src.height), (dst.top + dst.height));

    return {
      left:x,
      top:y,
      width:x2-x,
      height:y2-y
    };
  }

  function setZ(element, stack, parentStack){
    var newContext,
    isPositioned = stack.cssPosition !== 'static',
    zIndex = isPositioned ? getCSS(element, 'zIndex') : 'auto',
    opacity = getCSS(element, 'opacity'),
    isFloated = getCSS(element, 'cssFloat') !== 'none';

    // https://developer.mozilla.org/en-US/docs/Web/Guide/CSS/Understanding_z_index/The_stacking_context
    // When a new stacking context should be created:
    // the root element (HTML),
    // positioned (absolutely or relatively) with a z-index value other than "auto",
    // elements with an opacity value less than 1. (See the specification for opacity),
    // on mobile WebKit and Chrome 22+, position: fixed always creates a new stacking context, even when z-index is "auto" (See this post)

    stack.zIndex = newContext = h2czContext(zIndex);
    newContext.isPositioned = isPositioned;
    newContext.isFloated = isFloated;
    newContext.opacity = opacity;
    newContext.ownStacking = (zIndex !== 'auto' || opacity < 1);

    if (parentStack) {
      parentStack.zIndex.children.push(stack);
    }
  }

  function renderImage(ctx, element, image, bounds, borders) {

    var paddingLeft = getCSSInt(element, 'paddingLeft'),
    paddingTop = getCSSInt(element, 'paddingTop'),
    paddingRight = getCSSInt(element, 'paddingRight'),
    paddingBottom = getCSSInt(element, 'paddingBottom');

    drawImage(
      ctx,
      image,
      0, //sx
      0, //sy
      image.width, //sw
      image.height, //sh
      bounds.left + paddingLeft + borders[3].width, //dx
      bounds.top + paddingTop + borders[0].width, // dy
      bounds.width - (borders[1].width + borders[3].width + paddingLeft + paddingRight), //dw
      bounds.height - (borders[0].width + borders[2].width + paddingTop + paddingBottom) //dh
      );
  }

  function getBorderData(element) {
    return ["Top", "Right", "Bottom", "Left"].map(function(side) {
      return {
        width: getCSSInt(element, 'border' + side + 'Width'),
        color: getCSS(element, 'border' + side + 'Color')
      };
    });
  }

  function getBorderRadiusData(element) {
    return ["TopLeft", "TopRight", "BottomRight", "BottomLeft"].map(function(side) {
      return getCSS(element, 'border' + side + 'Radius');
    });
  }

  var getCurvePoints = (function(kappa) {

    return function(x, y, r1, r2) {
      var ox = (r1) * kappa, // control point offset horizontal
      oy = (r2) * kappa, // control point offset vertical
      xm = x + r1, // x-middle
      ym = y + r2; // y-middle
      return {
        topLeft: bezierCurve({
          x:x,
          y:ym
        }, {
          x:x,
          y:ym - oy
        }, {
          x:xm - ox,
          y:y
        }, {
          x:xm,
          y:y
        }),
        topRight: bezierCurve({
          x:x,
          y:y
        }, {
          x:x + ox,
          y:y
        }, {
          x:xm,
          y:ym - oy
        }, {
          x:xm,
          y:ym
        }),
        bottomRight: bezierCurve({
          x:xm,
          y:y
        }, {
          x:xm,
          y:y + oy
        }, {
          x:x + ox,
          y:ym
        }, {
          x:x,
          y:ym
        }),
        bottomLeft: bezierCurve({
          x:xm,
          y:ym
        }, {
          x:xm - ox,
          y:ym
        }, {
          x:x,
          y:y + oy
        }, {
          x:x,
          y:y
        })
      };
    };
  })(4 * ((Math.sqrt(2) - 1) / 3));

  function bezierCurve(start, startControl, endControl, end) {

    var lerp = function (a, b, t) {
      return {
        x:a.x + (b.x - a.x) * t,
        y:a.y + (b.y - a.y) * t
      };
    };

    return {
      start: start,
      startControl: startControl,
      endControl: endControl,
      end: end,
      subdivide: function(t) {
        var ab = lerp(start, startControl, t),
        bc = lerp(startControl, endControl, t),
        cd = lerp(endControl, end, t),
        abbc = lerp(ab, bc, t),
        bccd = lerp(bc, cd, t),
        dest = lerp(abbc, bccd, t);
        return [bezierCurve(start, ab, abbc, dest), bezierCurve(dest, bccd, cd, end)];
      },
      curveTo: function(borderArgs) {
        borderArgs.push(["bezierCurve", startControl.x, startControl.y, endControl.x, endControl.y, end.x, end.y]);
      },
      curveToReversed: function(borderArgs) {
        borderArgs.push(["bezierCurve", endControl.x, endControl.y, startControl.x, startControl.y, start.x, start.y]);
      }
    };
  }

  function parseCorner(borderArgs, radius1, radius2, corner1, corner2, x, y) {
    if (radius1[0] > 0 || radius1[1] > 0) {
      borderArgs.push(["line", corner1[0].start.x, corner1[0].start.y]);
      corner1[0].curveTo(borderArgs);
      corner1[1].curveTo(borderArgs);
    } else {
      borderArgs.push(["line", x, y]);
    }

    if (radius2[0] > 0 || radius2[1] > 0) {
      borderArgs.push(["line", corner2[0].start.x, corner2[0].start.y]);
    }
  }

  function drawSide(borderData, radius1, radius2, outer1, inner1, outer2, inner2) {
    var borderArgs = [];

    if (radius1[0] > 0 || radius1[1] > 0) {
      borderArgs.push(["line", outer1[1].start.x, outer1[1].start.y]);
      outer1[1].curveTo(borderArgs);
    } else {
      borderArgs.push([ "line", borderData.c1[0], borderData.c1[1]]);
    }

    if (radius2[0] > 0 || radius2[1] > 0) {
      borderArgs.push(["line", outer2[0].start.x, outer2[0].start.y]);
      outer2[0].curveTo(borderArgs);
      borderArgs.push(["line", inner2[0].end.x, inner2[0].end.y]);
      inner2[0].curveToReversed(borderArgs);
    } else {
      borderArgs.push([ "line", borderData.c2[0], borderData.c2[1]]);
      borderArgs.push([ "line", borderData.c3[0], borderData.c3[1]]);
    }

    if (radius1[0] > 0 || radius1[1] > 0) {
      borderArgs.push(["line", inner1[1].end.x, inner1[1].end.y]);
      inner1[1].curveToReversed(borderArgs);
    } else {
      borderArgs.push([ "line", borderData.c4[0], borderData.c4[1]]);
    }

    return borderArgs;
  }

  function calculateCurvePoints(bounds, borderRadius, borders) {

    var x = bounds.left,
    y = bounds.top,
    width = bounds.width,
    height = bounds.height,

    tlh = borderRadius[0][0],
    tlv = borderRadius[0][1],
    trh = borderRadius[1][0],
    trv = borderRadius[1][1],
    brh = borderRadius[2][0],
    brv = borderRadius[2][1],
    blh = borderRadius[3][0],
    blv = borderRadius[3][1],

    topWidth = width - trh,
    rightHeight = height - brv,
    bottomWidth = width - brh,
    leftHeight = height - blv;

    return {
      topLeftOuter: getCurvePoints(
        x,
        y,
        tlh,
        tlv
        ).topLeft.subdivide(0.5),

      topLeftInner: getCurvePoints(
        x + borders[3].width,
        y + borders[0].width,
        Math.max(0, tlh - borders[3].width),
        Math.max(0, tlv - borders[0].width)
        ).topLeft.subdivide(0.5),

      topRightOuter: getCurvePoints(
        x + topWidth,
        y,
        trh,
        trv
        ).topRight.subdivide(0.5),

      topRightInner: getCurvePoints(
        x + Math.min(topWidth, width + borders[3].width),
        y + borders[0].width,
        (topWidth > width + borders[3].width) ? 0 :trh - borders[3].width,
        trv - borders[0].width
        ).topRight.subdivide(0.5),

      bottomRightOuter: getCurvePoints(
        x + bottomWidth,
        y + rightHeight,
        brh,
        brv
        ).bottomRight.subdivide(0.5),

      bottomRightInner: getCurvePoints(
        x + Math.min(bottomWidth, width + borders[3].width),
        y + Math.min(rightHeight, height + borders[0].width),
        Math.max(0, brh - borders[1].width),
        Math.max(0, brv - borders[2].width)
        ).bottomRight.subdivide(0.5),

      bottomLeftOuter: getCurvePoints(
        x,
        y + leftHeight,
        blh,
        blv
        ).bottomLeft.subdivide(0.5),

      bottomLeftInner: getCurvePoints(
        x + borders[3].width,
        y + leftHeight,
        Math.max(0, blh - borders[3].width),
        Math.max(0, blv - borders[2].width)
        ).bottomLeft.subdivide(0.5)
    };
  }

  function getBorderClip(element, borderPoints, borders, radius, bounds) {
    var backgroundClip = getCSS(element, 'backgroundClip'),
    borderArgs = [];

    switch(backgroundClip) {
      case "content-box":
      case "padding-box":
        parseCorner(borderArgs, radius[0], radius[1], borderPoints.topLeftInner, borderPoints.topRightInner, bounds.left + borders[3].width, bounds.top + borders[0].width);
        parseCorner(borderArgs, radius[1], radius[2], borderPoints.topRightInner, borderPoints.bottomRightInner, bounds.left + bounds.width - borders[1].width, bounds.top + borders[0].width);
        parseCorner(borderArgs, radius[2], radius[3], borderPoints.bottomRightInner, borderPoints.bottomLeftInner, bounds.left + bounds.width - borders[1].width, bounds.top + bounds.height - borders[2].width);
        parseCorner(borderArgs, radius[3], radius[0], borderPoints.bottomLeftInner, borderPoints.topLeftInner, bounds.left + borders[3].width, bounds.top + bounds.height - borders[2].width);
        break;

      default:
        parseCorner(borderArgs, radius[0], radius[1], borderPoints.topLeftOuter, borderPoints.topRightOuter, bounds.left, bounds.top);
        parseCorner(borderArgs, radius[1], radius[2], borderPoints.topRightOuter, borderPoints.bottomRightOuter, bounds.left + bounds.width, bounds.top);
        parseCorner(borderArgs, radius[2], radius[3], borderPoints.bottomRightOuter, borderPoints.bottomLeftOuter, bounds.left + bounds.width, bounds.top + bounds.height);
        parseCorner(borderArgs, radius[3], radius[0], borderPoints.bottomLeftOuter, borderPoints.topLeftOuter, bounds.left, bounds.top + bounds.height);
        break;
    }

    return borderArgs;
  }

  function parseBorders(element, bounds, borders){
    var x = bounds.left,
    y = bounds.top,
    width = bounds.width,
    height = bounds.height,
    borderSide,
    bx,
    by,
    bw,
    bh,
    borderArgs,
    // http://www.w3.org/TR/css3-background/#the-border-radius
    borderRadius = getBorderRadiusData(element),
    borderPoints = calculateCurvePoints(bounds, borderRadius, borders),
    borderData = {
      clip: getBorderClip(element, borderPoints, borders, borderRadius, bounds),
      borders: []
    };

    for (borderSide = 0; borderSide < 4; borderSide++) {

      if (borders[borderSide].width > 0) {
        bx = x;
        by = y;
        bw = width;
        bh = height - (borders[2].width);

        switch(borderSide) {
          case 0:
            // top border
            bh = borders[0].width;

            borderArgs = drawSide({
              c1: [bx, by],
              c2: [bx + bw, by],
              c3: [bx + bw - borders[1].width, by + bh],
              c4: [bx + borders[3].width, by + bh]
            }, borderRadius[0], borderRadius[1],
            borderPoints.topLeftOuter, borderPoints.topLeftInner, borderPoints.topRightOuter, borderPoints.topRightInner);
            break;
          case 1:
            // right border
            bx = x + width - (borders[1].width);
            bw = borders[1].width;

            borderArgs = drawSide({
              c1: [bx + bw, by],
              c2: [bx + bw, by + bh + borders[2].width],
              c3: [bx, by + bh],
              c4: [bx, by + borders[0].width]
            }, borderRadius[1], borderRadius[2],
            borderPoints.topRightOuter, borderPoints.topRightInner, borderPoints.bottomRightOuter, borderPoints.bottomRightInner);
            break;
          case 2:
            // bottom border
            by = (by + height) - (borders[2].width);
            bh = borders[2].width;

            borderArgs = drawSide({
              c1: [bx + bw, by + bh],
              c2: [bx, by + bh],
              c3: [bx + borders[3].width, by],
              c4: [bx + bw - borders[3].width, by]
            }, borderRadius[2], borderRadius[3],
            borderPoints.bottomRightOuter, borderPoints.bottomRightInner, borderPoints.bottomLeftOuter, borderPoints.bottomLeftInner);
            break;
          case 3:
            // left border
            bw = borders[3].width;

            borderArgs = drawSide({
              c1: [bx, by + bh + borders[2].width],
              c2: [bx, by],
              c3: [bx + bw, by + borders[0].width],
              c4: [bx + bw, by + bh]
            }, borderRadius[3], borderRadius[0],
            borderPoints.bottomLeftOuter, borderPoints.bottomLeftInner, borderPoints.topLeftOuter, borderPoints.topLeftInner);
            break;
        }

        borderData.borders.push({
          args: borderArgs,
          color: borders[borderSide].color
        });

      }
    }

    return borderData;
  }

  function createShape(ctx, args) {
    var shape = ctx.drawShape();
    args.forEach(function(border, index) {
      shape[(index === 0) ? "moveTo" : border[0] + "To" ].apply(null, border.slice(1));
    });
    return shape;
  }

  function renderBorders(ctx, borderArgs, color) {
    if (color !== "transparent") {
      ctx.setVariable( "fillStyle", color);
      createShape(ctx, borderArgs);
      ctx.fill();
      numDraws+=1;
    }
  }

  function renderFormValue (el, bounds, stack){

    var valueWrap = doc.createElement('valuewrap'),
    cssPropertyArray = ['lineHeight','textAlign','fontFamily','color','fontSize','paddingLeft','paddingTop','width','height','border','borderLeftWidth','borderTopWidth'],
    textValue,
    textNode;

    cssPropertyArray.forEach(function(property) {
      try {
        valueWrap.style[property] = getCSS(el, property);
      } catch(e) {
        // Older IE has issues with "border"
        Util.log("html2canvas: Parse: Exception caught in renderFormValue: " + e.message);
      }
    });

    valueWrap.style.borderColor = "black";
    valueWrap.style.borderStyle = "solid";
    valueWrap.style.display = "block";
    valueWrap.style.position = "absolute";

    if (/^(submit|reset|button|text|password)$/.test(el.type) || el.nodeName === "SELECT"){
      valueWrap.style.lineHeight = getCSS(el, "height");
    }

    valueWrap.style.top = bounds.top + "px";
    valueWrap.style.left = bounds.left + "px";

    textValue = (el.nodeName === "SELECT") ? (el.options[el.selectedIndex] || 0).text : el.value;
    if(!textValue) {
      textValue = el.placeholder;
    }

    textNode = doc.createTextNode(textValue);

    valueWrap.appendChild(textNode);
    body.appendChild(valueWrap);

    renderText(el, textNode, stack);
    body.removeChild(valueWrap);
  }

  function drawImage (ctx) {
    ctx.drawImage.apply(ctx, Array.prototype.slice.call(arguments, 1));
    numDraws+=1;
  }

  function getPseudoElement(el, which) {
    var elStyle = window.getComputedStyle(el, which);
    if(!elStyle || !elStyle.content || elStyle.content === "none" || elStyle.content === "-moz-alt-content" || elStyle.display === "none") {
      return;
    }
    var content = elStyle.content + '',
    first = content.substr( 0, 1 );
    //strips quotes
    if(first === content.substr( content.length - 1 ) && first.match(/'|"/)) {
      content = content.substr( 1, content.length - 2 );
    }

    var isImage = content.substr( 0, 3 ) === 'url',
    elps = document.createElement( isImage ? 'img' : 'span' );

    elps.className = pseudoHide + "-before " + pseudoHide + "-after";

    Object.keys(elStyle).filter(indexedProperty).forEach(function(prop) {
      // Prevent assigning of read only CSS Rules, ex. length, parentRule
      try {
        elps.style[prop] = elStyle[prop];
      } catch (e) {
        Util.log(['Tried to assign readonly property ', prop, 'Error:', e]);
      }
    });

    if(isImage) {
      elps.src = Util.parseBackgroundImage(content)[0].args[0];
    } else {
      elps.innerHTML = content;
    }
    return elps;
  }

  function indexedProperty(property) {
    return (isNaN(window.parseInt(property, 10)));
  }

  function injectPseudoElements(el, stack) {
    var before = getPseudoElement(el, ':before'),
    after = getPseudoElement(el, ':after');
    if(!before && !after) {
      return;
    }

    if(before) {
      el.className += " " + pseudoHide + "-before";
      el.parentNode.insertBefore(before, el);
      parseElement(before, stack, true);
      el.parentNode.removeChild(before);
      el.className = el.className.replace(pseudoHide + "-before", "").trim();
    }

    if (after) {
      el.className += " " + pseudoHide + "-after";
      el.appendChild(after);
      parseElement(after, stack, true);
      el.removeChild(after);
      el.className = el.className.replace(pseudoHide + "-after", "").trim();
    }

  }

  function renderBackgroundRepeat(ctx, image, backgroundPosition, bounds) {
    var offsetX = Math.round(bounds.left + backgroundPosition.left),
    offsetY = Math.round(bounds.top + backgroundPosition.top);

    ctx.createPattern(image);
    ctx.translate(offsetX, offsetY);
    ctx.fill();
    ctx.translate(-offsetX, -offsetY);
  }

  function backgroundRepeatShape(ctx, image, backgroundPosition, bounds, left, top, width, height) {
    var args = [];
    args.push(["line", Math.round(left), Math.round(top)]);
    args.push(["line", Math.round(left + width), Math.round(top)]);
    args.push(["line", Math.round(left + width), Math.round(height + top)]);
    args.push(["line", Math.round(left), Math.round(height + top)]);
    createShape(ctx, args);
    ctx.save();
    ctx.clip();
    renderBackgroundRepeat(ctx, image, backgroundPosition, bounds);
    ctx.restore();
  }

  function renderBackgroundColor(ctx, backgroundBounds, bgcolor) {
    renderRect(
      ctx,
      backgroundBounds.left,
      backgroundBounds.top,
      backgroundBounds.width,
      backgroundBounds.height,
      bgcolor
      );
  }

  function renderBackgroundRepeating(el, bounds, ctx, image, imageIndex) {
    var backgroundSize = Util.BackgroundSize(el, bounds, image, imageIndex),
    backgroundPosition = Util.BackgroundPosition(el, bounds, image, imageIndex, backgroundSize),
    backgroundRepeat = getCSS(el, "backgroundRepeat").split(",").map(Util.trimText);

    image = resizeImage(image, backgroundSize);

    backgroundRepeat = backgroundRepeat[imageIndex] || backgroundRepeat[0];

    switch (backgroundRepeat) {
      case "repeat-x":
        backgroundRepeatShape(ctx, image, backgroundPosition, bounds,
          bounds.left, bounds.top + backgroundPosition.top, 99999, image.height);
        break;

      case "repeat-y":
        backgroundRepeatShape(ctx, image, backgroundPosition, bounds,
          bounds.left + backgroundPosition.left, bounds.top, image.width, 99999);
        break;

      case "no-repeat":
        backgroundRepeatShape(ctx, image, backgroundPosition, bounds,
          bounds.left + backgroundPosition.left, bounds.top + backgroundPosition.top, image.width, image.height);
        break;

      default:
        renderBackgroundRepeat(ctx, image, backgroundPosition, {
          top: bounds.top,
          left: bounds.left,
          width: image.width,
          height: image.height
        });
        break;
    }
  }

  function renderBackgroundImage(element, bounds, ctx) {
    var backgroundImage = getCSS(element, "backgroundImage"),
    backgroundImages = Util.parseBackgroundImage(backgroundImage),
    image,
    imageIndex = backgroundImages.length;

    while(imageIndex--) {
      backgroundImage = backgroundImages[imageIndex];

      if (!backgroundImage.args || backgroundImage.args.length === 0) {
        continue;
      }

      var key = backgroundImage.method === 'url' ?
      backgroundImage.args[0] :
      backgroundImage.value;

      image = loadImage(key);

      // TODO add support for background-origin
      if (image) {
        renderBackgroundRepeating(element, bounds, ctx, image, imageIndex);
      } else {
        Util.log("html2canvas: Error loading background:", backgroundImage);
      }
    }
  }

  function resizeImage(image, bounds) {
    if(image.width === bounds.width && image.height === bounds.height) {
      return image;
    }

    var ctx, canvas = doc.createElement('canvas');
    canvas.width = bounds.width;
    canvas.height = bounds.height;
    ctx = canvas.getContext("2d");
    drawImage(ctx, image, 0, 0, image.width, image.height, 0, 0, bounds.width, bounds.height );
    return canvas;
  }

  function setOpacity(ctx, element, parentStack) {
    return ctx.setVariable("globalAlpha", getCSS(element, "opacity") * ((parentStack) ? parentStack.opacity : 1));
  }

  function removePx(str) {
    return str.replace("px", "");
  }

  var transformRegExp = /(matrix)\((.+)\)/;

  function getTransform(element, parentStack) {
    var transform = getCSS(element, "transform") || getCSS(element, "-webkit-transform") || getCSS(element, "-moz-transform") || getCSS(element, "-ms-transform") || getCSS(element, "-o-transform");
    var transformOrigin = getCSS(element, "transform-origin") || getCSS(element, "-webkit-transform-origin") || getCSS(element, "-moz-transform-origin") || getCSS(element, "-ms-transform-origin") || getCSS(element, "-o-transform-origin") || "0px 0px";

    transformOrigin = transformOrigin.split(" ").map(removePx).map(Util.asFloat);

    var matrix;
    if (transform && transform !== "none") {
      var match = transform.match(transformRegExp);
      if (match) {
        switch(match[1]) {
          case "matrix":
            matrix = match[2].split(",").map(Util.trimText).map(Util.asFloat);
            break;
        }
      }
    }

    return {
      origin: transformOrigin,
      matrix: matrix
    };
  }

  function createStack(element, parentStack, bounds, transform) {
    var ctx = h2cRenderContext((!parentStack) ? documentWidth() : bounds.width , (!parentStack) ? documentHeight() : bounds.height),
    stack = {
      ctx: ctx,
      opacity: setOpacity(ctx, element, parentStack),
      cssPosition: getCSS(element, "position"),
      borders: getBorderData(element),
      transform: transform,
      clip: (parentStack && parentStack.clip) ? Util.Extend( {}, parentStack.clip ) : null
    };

    setZ(element, stack, parentStack);

    // TODO correct overflow for absolute content residing under a static position
    if (options.useOverflow === true && /(hidden|scroll|auto)/.test(getCSS(element, "overflow")) === true && /(BODY)/i.test(element.nodeName) === false){
      stack.clip = (stack.clip) ? clipBounds(stack.clip, bounds) : bounds;
    }

    return stack;
  }

  function getBackgroundBounds(borders, bounds, clip) {
    var backgroundBounds = {
      left: bounds.left + borders[3].width,
      top: bounds.top + borders[0].width,
      width: bounds.width - (borders[1].width + borders[3].width),
      height: bounds.height - (borders[0].width + borders[2].width)
    };

    if (clip) {
      backgroundBounds = clipBounds(backgroundBounds, clip);
    }

    return backgroundBounds;
  }

  function getBounds(element, transform) {
    var bounds = (transform.matrix) ? Util.OffsetBounds(element) : Util.Bounds(element);
    transform.origin[0] += bounds.left;
    transform.origin[1] += bounds.top;
    return bounds;
  }

  function renderElement(element, parentStack, pseudoElement, ignoreBackground) {
    var transform = getTransform(element, parentStack),
    bounds = getBounds(element, transform),
    image,
    stack = createStack(element, parentStack, bounds, transform),
    borders = stack.borders,
    ctx = stack.ctx,
    backgroundBounds = getBackgroundBounds(borders, bounds, stack.clip),
    borderData = parseBorders(element, bounds, borders),
    backgroundColor = (ignoreElementsRegExp.test(element.nodeName)) ? "#efefef" : getCSS(element, "backgroundColor");


    createShape(ctx, borderData.clip);

    ctx.save();
    ctx.clip();

    if (backgroundBounds.height > 0 && backgroundBounds.width > 0 && !ignoreBackground) {
      renderBackgroundColor(ctx, bounds, backgroundColor);
      renderBackgroundImage(element, backgroundBounds, ctx);
    } else if (ignoreBackground) {
      stack.backgroundColor =  backgroundColor;
    }

    ctx.restore();

    borderData.borders.forEach(function(border) {
      renderBorders(ctx, border.args, border.color);
    });

    if (!pseudoElement) {
      injectPseudoElements(element, stack);
    }

    switch(element.nodeName){
      case "IMG":
        if ((image = loadImage(element.getAttribute('src')))) {
          renderImage(ctx, element, image, bounds, borders);
        } else {
          Util.log("html2canvas: Error loading <img>:" + element.getAttribute('src'));
        }
        break;
      case "INPUT":
        // TODO add all relevant type's, i.e. HTML5 new stuff
        // todo add support for placeholder attribute for browsers which support it
        if (/^(text|url|email|submit|button|reset)$/.test(element.type) && (element.value || element.placeholder || "").length > 0){
          renderFormValue(element, bounds, stack);
        }
        break;
      case "TEXTAREA":
        if ((element.value || element.placeholder || "").length > 0){
          renderFormValue(element, bounds, stack);
        }
        break;
      case "SELECT":
        if ((element.options||element.placeholder || "").length > 0){
          renderFormValue(element, bounds, stack);
        }
        break;
      case "LI":
        renderListItem(element, stack, backgroundBounds);
        break;
      case "CANVAS":
        renderImage(ctx, element, element, bounds, borders);
        break;
    }

    return stack;
  }

  function isElementVisible(element) {
    return (getCSS(element, 'display') !== "none" && getCSS(element, 'visibility') !== "hidden" && !element.hasAttribute("data-html2canvas-ignore"));
  }

  function parseElement (element, stack, pseudoElement) {
    if (isElementVisible(element)) {
      stack = renderElement(element, stack, pseudoElement, false) || stack;
      if (!ignoreElementsRegExp.test(element.nodeName)) {
        parseChildren(element, stack, pseudoElement);
      }
    }
  }

  function parseChildren(element, stack, pseudoElement) {
    Util.Children(element).forEach(function(node) {
      if (node.nodeType === node.ELEMENT_NODE) {
        parseElement(node, stack, pseudoElement);
      } else if (node.nodeType === node.TEXT_NODE) {
        renderText(element, node, stack);
      }
    });
  }

  function init() {
    var background = getCSS(document.documentElement, "backgroundColor"),
      transparentBackground = (Util.isTransparent(background) && element === document.body),
      stack = renderElement(element, null, false, transparentBackground);
    parseChildren(element, stack);

    if (transparentBackground) {
      background = stack.backgroundColor;
    }

    body.removeChild(hidePseudoElements);
    return {
      backgroundColor: background,
      stack: stack
    };
  }

  return init();
};

function h2czContext(zindex) {
  return {
    zindex: zindex,
    children: []
  };
}

_html2canvas.Preload = function( options ) {

  var images = {
    numLoaded: 0,   // also failed are counted here
    numFailed: 0,
    numTotal: 0,
    cleanupDone: false
  },
  pageOrigin,
  Util = _html2canvas.Util,
  methods,
  i,
  count = 0,
  element = options.elements[0] || document.body,
  doc = element.ownerDocument,
  domImages = element.getElementsByTagName('img'), // Fetch images of the present element only
  imgLen = domImages.length,
  link = doc.createElement("a"),
  supportCORS = (function( img ){
    return (img.crossOrigin !== undefined);
  })(new Image()),
  timeoutTimer;

  link.href = window.location.href;
  pageOrigin  = link.protocol + link.host;

  function isSameOrigin(url){
    link.href = url;
    link.href = link.href; // YES, BELIEVE IT OR NOT, that is required for IE9 - http://jsfiddle.net/niklasvh/2e48b/
    var origin = link.protocol + link.host;
    return (origin === pageOrigin);
  }

  function start(){
    Util.log("html2canvas: start: images: " + images.numLoaded + " / " + images.numTotal + " (failed: " + images.numFailed + ")");
    if (!images.firstRun && images.numLoaded >= images.numTotal){
      Util.log("Finished loading images: # " + images.numTotal + " (failed: " + images.numFailed + ")");

      if (typeof options.complete === "function"){
        options.complete(images);
      }

    }
  }

  // TODO modify proxy to serve images with CORS enabled, where available
  function proxyGetImage(url, img, imageObj){
    var callback_name,
    scriptUrl = options.proxy,
    script;

    link.href = url;
    url = link.href; // work around for pages with base href="" set - WARNING: this may change the url

    callback_name = 'html2canvas_' + (count++);
    imageObj.callbackname = callback_name;

    if (scriptUrl.indexOf("?") > -1) {
      scriptUrl += "&";
    } else {
      scriptUrl += "?";
    }
    scriptUrl += 'url=' + encodeURIComponent(url) + '&callback=' + callback_name;
    script = doc.createElement("script");

    window[callback_name] = function(a){
      if (a.substring(0,6) === "error:"){
        imageObj.succeeded = false;
        images.numLoaded++;
        images.numFailed++;
        start();
      } else {
        setImageLoadHandlers(img, imageObj);
        img.src = a;
      }
      window[callback_name] = undefined; // to work with IE<9  // NOTE: that the undefined callback property-name still exists on the window object (for IE<9)
      try {
        delete window[callback_name];  // for all browser that support this
      } catch(ex) {}
      script.parentNode.removeChild(script);
      script = null;
      delete imageObj.script;
      delete imageObj.callbackname;
    };

    script.setAttribute("type", "text/javascript");
    script.setAttribute("src", scriptUrl);
    imageObj.script = script;
    window.document.body.appendChild(script);

  }

  function loadPseudoElement(element, type) {
    var style = window.getComputedStyle(element, type),
    content = style.content;
    if (content.substr(0, 3) === 'url') {
      methods.loadImage(_html2canvas.Util.parseBackgroundImage(content)[0].args[0]);
    }
    loadBackgroundImages(style.backgroundImage, element);
  }

  function loadPseudoElementImages(element) {
    loadPseudoElement(element, ":before");
    loadPseudoElement(element, ":after");
  }

  function loadGradientImage(backgroundImage, bounds) {
    var img = _html2canvas.Generate.Gradient(backgroundImage, bounds);

    if (img !== undefined){
      images[backgroundImage] = {
        img: img,
        succeeded: true
      };
      images.numTotal++;
      images.numLoaded++;
      start();
    }
  }

  function invalidBackgrounds(background_image) {
    return (background_image && background_image.method && background_image.args && background_image.args.length > 0 );
  }

  function loadBackgroundImages(background_image, el) {
    var bounds;

    _html2canvas.Util.parseBackgroundImage(background_image).filter(invalidBackgrounds).forEach(function(background_image) {
      if (background_image.method === 'url') {
        methods.loadImage(background_image.args[0]);
      } else if(background_image.method.match(/\-?gradient$/)) {
        if(bounds === undefined) {
          bounds = _html2canvas.Util.Bounds(el);
        }
        loadGradientImage(background_image.value, bounds);
      }
    });
  }

  function getImages (el) {
    var elNodeType = false;

    // Firefox fails with permission denied on pages with iframes
    try {
      Util.Children(el).forEach(getImages);
    }
    catch( e ) {}

    try {
      elNodeType = el.nodeType;
    } catch (ex) {
      elNodeType = false;
      Util.log("html2canvas: failed to access some element's nodeType - Exception: " + ex.message);
    }

    if (elNodeType === 1 || elNodeType === undefined) {
      loadPseudoElementImages(el);
      try {
        loadBackgroundImages(Util.getCSS(el, 'backgroundImage'), el);
      } catch(e) {
        Util.log("html2canvas: failed to get background-image - Exception: " + e.message);
      }
      loadBackgroundImages(el);
    }
  }

  function setImageLoadHandlers(img, imageObj) {
    img.onload = function() {
      if ( imageObj.timer !== undefined ) {
        // CORS succeeded
        window.clearTimeout( imageObj.timer );
      }

      images.numLoaded++;
      imageObj.succeeded = true;
      img.onerror = img.onload = null;
      start();
    };
    img.onerror = function() {
      if (img.crossOrigin === "anonymous") {
        // CORS failed
        window.clearTimeout( imageObj.timer );

        // let's try with proxy instead
        if ( options.proxy ) {
          var src = img.src;
          img = new Image();
          imageObj.img = img;
          img.src = src;

          proxyGetImage( img.src, img, imageObj );
          return;
        }
      }

      images.numLoaded++;
      images.numFailed++;
      imageObj.succeeded = false;
      img.onerror = img.onload = null;
      start();
    };
  }

  methods = {
    loadImage: function( src ) {
      var img, imageObj;
      if ( src && images[src] === undefined ) {
        img = new Image();
        if ( src.match(/data:image\/.*;base64,/i) ) {
          img.src = src.replace(/url\(['"]{0,}|['"]{0,}\)$/ig, '');
          imageObj = images[src] = {
            img: img
          };
          images.numTotal++;
          setImageLoadHandlers(img, imageObj);
        } else if ( isSameOrigin( src ) || options.allowTaint ===  true ) {
          imageObj = images[src] = {
            img: img
          };
          images.numTotal++;
          setImageLoadHandlers(img, imageObj);
          img.src = src;
        } else if ( supportCORS && !options.allowTaint && options.useCORS ) {
          // attempt to load with CORS

          img.crossOrigin = "anonymous";
          imageObj = images[src] = {
            img: img
          };
          images.numTotal++;
          setImageLoadHandlers(img, imageObj);
          img.src = src;
        } else if ( options.proxy ) {
          imageObj = images[src] = {
            img: img
          };
          images.numTotal++;
          proxyGetImage( src, img, imageObj );
        }
      }

    },
    cleanupDOM: function(cause) {
      var img, src;
      if (!images.cleanupDone) {
        if (cause && typeof cause === "string") {
          Util.log("html2canvas: Cleanup because: " + cause);
        } else {
          Util.log("html2canvas: Cleanup after timeout: " + options.timeout + " ms.");
        }

        for (src in images) {
          if (images.hasOwnProperty(src)) {
            img = images[src];
            if (typeof img === "object" && img.callbackname && img.succeeded === undefined) {
              // cancel proxy image request
              window[img.callbackname] = undefined; // to work with IE<9  // NOTE: that the undefined callback property-name still exists on the window object (for IE<9)
              try {
                delete window[img.callbackname];  // for all browser that support this
              } catch(ex) {}
              if (img.script && img.script.parentNode) {
                img.script.setAttribute("src", "about:blank");  // try to cancel running request
                img.script.parentNode.removeChild(img.script);
              }
              images.numLoaded++;
              images.numFailed++;
              Util.log("html2canvas: Cleaned up failed img: '" + src + "' Steps: " + images.numLoaded + " / " + images.numTotal);
            }
          }
        }

        // cancel any pending requests
        if(window.stop !== undefined) {
          window.stop();
        } else if(document.execCommand !== undefined) {
          document.execCommand("Stop", false);
        }
        if (document.close !== undefined) {
          document.close();
        }
        images.cleanupDone = true;
        if (!(cause && typeof cause === "string")) {
          start();
        }
      }
    },

    renderingDone: function() {
      if (timeoutTimer) {
        window.clearTimeout(timeoutTimer);
      }
    }
  };

  if (options.timeout > 0) {
    timeoutTimer = window.setTimeout(methods.cleanupDOM, options.timeout);
  }

  Util.log('html2canvas: Preload starts: finding background-images');
  images.firstRun = true;

  getImages(element);

  Util.log('html2canvas: Preload: Finding images');
  // load <img> images
  for (i = 0; i < imgLen; i+=1){
    methods.loadImage( domImages[i].getAttribute( "src" ) );
  }

  images.firstRun = false;
  Util.log('html2canvas: Preload: Done.');
  if (images.numTotal === images.numLoaded) {
    start();
  }

  return methods;
};

_html2canvas.Renderer = function(parseQueue, options){

  // http://www.w3.org/TR/CSS21/zindex.html
  function createRenderQueue(parseQueue) {
    var queue = [],
    rootContext;

    rootContext = (function buildStackingContext(rootNode) {
      var rootContext = {};
      function insert(context, node, specialParent) {
        var zi = (node.zIndex.zindex === 'auto') ? 0 : Number(node.zIndex.zindex),
        contextForChildren = context, // the stacking context for children
        isPositioned = node.zIndex.isPositioned,
        isFloated = node.zIndex.isFloated,
        stub = {node: node},
        childrenDest = specialParent; // where children without z-index should be pushed into

        if (node.zIndex.ownStacking) {
          // '!' comes before numbers in sorted array
          contextForChildren = stub.context = { '!': [{node:node, children: []}]};
          childrenDest = undefined;
        } else if (isPositioned || isFloated) {
          childrenDest = stub.children = [];
        }

        if (zi === 0 && specialParent) {
          specialParent.push(stub);
        } else {
          if (!context[zi]) { context[zi] = []; }
          context[zi].push(stub);
        }

        node.zIndex.children.forEach(function(childNode) {
          insert(contextForChildren, childNode, childrenDest);
        });
      }
      insert(rootContext, rootNode);
      return rootContext;
    })(parseQueue);

    function sortZ(context) {
      Object.keys(context).sort().forEach(function(zi) {
        var nonPositioned = [],
        floated = [],
        positioned = [],
        list = [];

        // positioned after static
        context[zi].forEach(function(v) {
          if (v.node.zIndex.isPositioned || v.node.zIndex.opacity < 1) {
            // http://www.w3.org/TR/css3-color/#transparency
            // non-positioned element with opactiy < 1 should be stacked as if it were a positioned element with ‘z-index: 0’ and ‘opacity: 1’.
            positioned.push(v);
          } else if (v.node.zIndex.isFloated) {
            floated.push(v);
          } else {
            nonPositioned.push(v);
          }
        });

        (function walk(arr) {
          arr.forEach(function(v) {
            list.push(v);
            if (v.children) { walk(v.children); }
          });
        })(nonPositioned.concat(floated, positioned));

        list.forEach(function(v) {
          if (v.context) {
            sortZ(v.context);
          } else {
            queue.push(v.node);
          }
        });
      });
    }

    sortZ(rootContext);

    return queue;
  }

  function getRenderer(rendererName) {
    var renderer;

    if (typeof options.renderer === "string" && _html2canvas.Renderer[rendererName] !== undefined) {
      renderer = _html2canvas.Renderer[rendererName](options);
    } else if (typeof rendererName === "function") {
      renderer = rendererName(options);
    } else {
      throw new Error("Unknown renderer");
    }

    if ( typeof renderer !== "function" ) {
      throw new Error("Invalid renderer defined");
    }
    return renderer;
  }

  return getRenderer(options.renderer)(parseQueue, options, document, createRenderQueue(parseQueue.stack), _html2canvas);
};

_html2canvas.Util.Support = function (options, doc) {

  function supportSVGRendering() {
    var img = new Image(),
    canvas = doc.createElement("canvas"),
    ctx = (canvas.getContext === undefined) ? false : canvas.getContext("2d");
    if (ctx === false) {
      return false;
    }
    canvas.width = canvas.height = 10;
    img.src = [
    "data:image/svg+xml,",
    "<svg xmlns='http://www.w3.org/2000/svg' width='10' height='10'>",
    "<foreignObject width='10' height='10'>",
    "<div xmlns='http://www.w3.org/1999/xhtml' style='width:10;height:10;'>",
    "sup",
    "</div>",
    "</foreignObject>",
    "</svg>"
    ].join("");
    try {
      ctx.drawImage(img, 0, 0);
      canvas.toDataURL();
    } catch(e) {
      return false;
    }
    _html2canvas.Util.log('html2canvas: Parse: SVG powered rendering available');
    return true;
  }

  // Test whether we can use ranges to measure bounding boxes
  // Opera doesn't provide valid bounds.height/bottom even though it supports the method.

  function supportRangeBounds() {
    var r, testElement, rangeBounds, rangeHeight, support = false;

    if (doc.createRange) {
      r = doc.createRange();
      if (r.getBoundingClientRect) {
        testElement = doc.createElement('boundtest');
        testElement.style.height = "123px";
        testElement.style.display = "block";
        doc.body.appendChild(testElement);

        r.selectNode(testElement);
        rangeBounds = r.getBoundingClientRect();
        rangeHeight = rangeBounds.height;

        if (rangeHeight === 123) {
          support = true;
        }
        doc.body.removeChild(testElement);
      }
    }

    return support;
  }

  return {
    rangeBounds: supportRangeBounds(),
    svgRendering: options.svgRendering && supportSVGRendering()
  };
};
window.html2canvas = function(elements, opts) {
  elements = (elements.length) ? elements : [elements];
  var queue,
  canvas,
  options = {
    // general
    logging: false,
    elements: elements,
    background: "#fff",

    // preload options
    proxy: null,
    timeout: 0,    // no timeout
    useCORS: false, // try to load images as CORS (where available), before falling back to proxy
    allowTaint: false, // whether to allow images to taint the canvas, won't need proxy if set to true

    // parse options
    svgRendering: false, // use svg powered rendering where available (FF11+)
    ignoreElements: "IFRAME|OBJECT|PARAM",
    useOverflow: true,
    letterRendering: false,
    chinese: false,

    // render options

    width: null,
    height: null,
    taintTest: true, // do a taint test with all images before applying to canvas
    renderer: "Canvas"
  };

  options = _html2canvas.Util.Extend(opts, options);

  _html2canvas.logging = options.logging;
  options.complete = function( images ) {

    if (typeof options.onpreloaded === "function") {
      if ( options.onpreloaded( images ) === false ) {
        return;
      }
    }
    queue = _html2canvas.Parse( images, options );

    if (typeof options.onparsed === "function") {
      if ( options.onparsed( queue ) === false ) {
        return;
      }
    }

    canvas = _html2canvas.Renderer( queue, options );

    if (typeof options.onrendered === "function") {
      options.onrendered( canvas );
    }


  };

  // for pages without images, we still want this to be async, i.e. return methods before executing
  window.setTimeout( function(){
    _html2canvas.Preload( options );
  }, 0 );

  return {
    render: function( queue, opts ) {
      return _html2canvas.Renderer( queue, _html2canvas.Util.Extend(opts, options) );
    },
    parse: function( images, opts ) {
      return _html2canvas.Parse( images, _html2canvas.Util.Extend(opts, options) );
    },
    preload: function( opts ) {
      return _html2canvas.Preload( _html2canvas.Util.Extend(opts, options) );
    },
    log: _html2canvas.Util.log
  };
};

window.html2canvas.log = _html2canvas.Util.log; // for renderers
window.html2canvas.Renderer = {
  Canvas: undefined // We are assuming this will be used
};
_html2canvas.Renderer.Canvas = function(options) {
  options = options || {};

  var doc = document,
  safeImages = [],
  testCanvas = document.createElement("canvas"),
  testctx = testCanvas.getContext("2d"),
  Util = _html2canvas.Util,
  canvas = options.canvas || doc.createElement('canvas');

  function createShape(ctx, args) {
    ctx.beginPath();
    args.forEach(function(arg) {
      ctx[arg.name].apply(ctx, arg['arguments']);
    });
    ctx.closePath();
  }

  function safeImage(item) {
    if (safeImages.indexOf(item['arguments'][0].src ) === -1) {
      testctx.drawImage(item['arguments'][0], 0, 0);
      try {
        testctx.getImageData(0, 0, 1, 1);
      } catch(e) {
        testCanvas = doc.createElement("canvas");
        testctx = testCanvas.getContext("2d");
        return false;
      }
      safeImages.push(item['arguments'][0].src);
    }
    return true;
  }

  function renderItem(ctx, item) {
    switch(item.type){
      case "variable":
        ctx[item.name] = item['arguments'];
        break;
      case "function":
        switch(item.name) {
          case "createPattern":
            if (item['arguments'][0].width > 0 && item['arguments'][0].height > 0) {
              try {
                ctx.fillStyle = ctx.createPattern(item['arguments'][0], "repeat");
              }
              catch(e) {
                Util.log("html2canvas: Renderer: Error creating pattern", e.message);
              }
            }
            break;
          case "drawShape":
            createShape(ctx, item['arguments']);
            break;
          case "drawImage":
            if (item['arguments'][8] > 0 && item['arguments'][7] > 0) {
              if (!options.taintTest || (options.taintTest && safeImage(item))) {
                ctx.drawImage.apply( ctx, item['arguments'] );
              }
            }
            break;
          default:
            ctx[item.name].apply(ctx, item['arguments']);
        }
        break;
    }
  }

  return function(parsedData, options, document, queue, _html2canvas) {
    var ctx = canvas.getContext("2d"),
    newCanvas,
    bounds,
    fstyle,
    zStack = parsedData.stack;

    canvas.width = canvas.style.width =  options.width || zStack.ctx.width;
    canvas.height = canvas.style.height = options.height || zStack.ctx.height;

    fstyle = ctx.fillStyle;
    ctx.fillStyle = (Util.isTransparent(zStack.backgroundColor) && options.background !== undefined) ? options.background : parsedData.backgroundColor;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = fstyle;

    queue.forEach(function(storageContext) {
      // set common settings for canvas
      ctx.textBaseline = "bottom";
      ctx.save();

      if (storageContext.transform.matrix) {
        ctx.translate(storageContext.transform.origin[0], storageContext.transform.origin[1]);
        ctx.transform.apply(ctx, storageContext.transform.matrix);
        ctx.translate(-storageContext.transform.origin[0], -storageContext.transform.origin[1]);
      }

      if (storageContext.clip){
        ctx.beginPath();
        ctx.rect(storageContext.clip.left, storageContext.clip.top, storageContext.clip.width, storageContext.clip.height);
        ctx.clip();
      }

      if (storageContext.ctx.storage) {
        storageContext.ctx.storage.forEach(function(item) {
          renderItem(ctx, item);
        });
      }

      ctx.restore();
    });

    Util.log("html2canvas: Renderer: Canvas renderer done - returning canvas obj");

    if (options.elements.length === 1) {
      if (typeof options.elements[0] === "object" && options.elements[0].nodeName !== "BODY") {
        // crop image to the bounds of selected (single) element
        bounds = _html2canvas.Util.Bounds(options.elements[0]);
        newCanvas = document.createElement('canvas');
        newCanvas.width = Math.ceil(bounds.width);
        newCanvas.height = Math.ceil(bounds.height);
        ctx = newCanvas.getContext("2d");

        try {
            ctx.drawImage(canvas, bounds.left, bounds.top, bounds.width, bounds.height, 0, 0, bounds.width, bounds.height);
        }
          catch(e) {
              console.log("html2canvas err.");
          }
        canvas = null;
        return newCanvas;
      }
    }

    return canvas;
  };
};
})(window,document);


(function($, undefined){
    
    // TODO: -
        // ARIA stuff: menuitem, menuitemcheckbox und menuitemradio
        // create <menu> structure if $.support[htmlCommand || htmlMenuitem] and !opt.disableNative

// determine html5 compatibility
$.support.htmlMenuitem = ('HTMLMenuItemElement' in window);
$.support.htmlCommand = ('HTMLCommandElement' in window);
$.support.eventSelectstart = ("onselectstart" in document.documentElement);


if (!$.ui || !$.ui.widget) {
    // duck punch $.cleanData like jQueryUI does to get that remove event
    // https://github.com/jquery/jquery-ui/blob/master/ui/jquery.ui.widget.js#L16-24
    var _cleanData = $.cleanData;
    $.cleanData = function( elems ) {
        for ( var i = 0, elem; (elem = elems[i]) != null; i++ ) {
            try {
                $( elem ).triggerHandler( "remove" );
                // http://bugs.jquery.com/ticket/8235
            } catch( e ) {}
        }
        _cleanData( elems );
    };
}

var // currently active contextMenu trigger
    $currentTrigger = null,
    // is contextMenu initialized with at least one menu?
    initialized = false,
    // window handle
    $win = $(window),
    // number of registered menus
    counter = 0,
    // mapping selector to namespace
    namespaces = {},
    // mapping namespace to options
    menus = {},
    // custom command type handlers
    types = {},
    // default values
    defaults = {
        // selector of contextMenu trigger
        selector: null,
        // where to append the menu to
        appendTo: null,
        // method to trigger context menu ["right", "left", "hover"]
        trigger: "right",
        // hide menu when mouse leaves trigger / menu elements
        autoHide: false,
        // ms to wait before showing a hover-triggered context menu
        delay: 200,
        // flag denoting if a second trigger should simply move (true) or rebuild (false) an open menu
        // as long as the trigger happened on one of the trigger-element's child nodes
        reposition: true,
        // determine position to show menu at
        determinePosition: function($menu) {
            // position to the lower middle of the trigger element
            if ($.ui && $.ui.position) {
                // .position() is provided as a jQuery UI utility
                // (...and it won't work on hidden elements)
                $menu.css('display', 'block').position({
                    my: "center top",
                    at: "center bottom",
                    of: this,
                    offset: "0 5",
                    collision: "fit"
                }).css('display', 'none');
            } else {
                // determine contextMenu position
                var offset = this.offset();
                offset.top += this.outerHeight();
                offset.left += this.outerWidth() / 2 - $menu.outerWidth() / 2;
                $menu.css(offset);
            }
        },
        // position menu
        position: function(opt, x, y) {
            var $this = this,
                offset;
            // determine contextMenu position
            if (!x && !y) {
                opt.determinePosition.call(this, opt.$menu);
                return;
            } else if (x === "maintain" && y === "maintain") {
                // x and y must not be changed (after re-show on command click)
                offset = opt.$menu.position();
            } else {
                // x and y are given (by mouse event)
                offset = {top: y, left: x};
            }
            
            // correct offset if viewport demands it
            var bottom = $win.scrollTop() + $win.height(),
                right = $win.scrollLeft() + $win.width(),
                height = opt.$menu.height(),
                width = opt.$menu.width();
            
            if (offset.top + height > bottom) {
                offset.top -= height;
            }
            
            if (offset.left + width > right) {
                offset.left -= width;
            }
            
            opt.$menu.css(offset);
        },
        // position the sub-menu
        positionSubmenu: function($menu) {
            if ($.ui && $.ui.position) {
                // .position() is provided as a jQuery UI utility
                // (...and it won't work on hidden elements)
                $menu.css('display', 'block').position({
                    my: "left top",
                    at: "right top",
                    of: this,
                    collision: "flipfit fit"
                }).css('display', '');
            } else {
                // determine contextMenu position
                var offset = {
                    top: 0,
                    left: this.outerWidth()
                };
                $menu.css(offset);
            }
        },
        // offset to add to zIndex
        zIndex: 1,
        // show hide animation settings
        animation: {
            duration: 50,
            show: 'slideDown',
            hide: 'slideUp'
        },
        // events
        events: {
            show: $.noop,
            hide: $.noop
        },
        // default callback
        callback: null,
        // list of contextMenu items
        items: {}
    },
    // mouse position for hover activation
    hoveract = {
        timer: null,
        pageX: null,
        pageY: null
    },
    // determine zIndex
    zindex = function($t) {
        var zin = 0,
            $tt = $t;

        while (true) {
            zin = Math.max(zin, parseInt($tt.css('z-index'), 10) || 0);
            $tt = $tt.parent();
            if (!$tt || !$tt.length || "html body".indexOf($tt.prop('nodeName').toLowerCase()) > -1 ) {
                break;
            }
        }
        
        return zin;
    },
    // event handlers
    handle = {
        // abort anything
        abortevent: function(e){
            e.preventDefault();
            e.stopImmediatePropagation();
        },
        
        // contextmenu show dispatcher
        contextmenu: function(e) {
            var $this = $(this);
            
            // disable actual context-menu
            e.preventDefault();
            e.stopImmediatePropagation();
            
            // abort native-triggered events unless we're triggering on right click
            if (e.data.trigger != 'right' && e.originalEvent) {
                return;
            }
            
            // abort event if menu is visible for this trigger
            if ($this.hasClass('context-menu-active')) {
                return;
            }
            
            if (!$this.hasClass('context-menu-disabled')) {
                // theoretically need to fire a show event at <menu>
                // http://www.whatwg.org/specs/web-apps/current-work/multipage/interactive-elements.html#context-menus
                // var evt = jQuery.Event("show", { data: data, pageX: e.pageX, pageY: e.pageY, relatedTarget: this });
                // e.data.$menu.trigger(evt);
                
                $currentTrigger = $this;
                if (e.data.build) {
                    var built = e.data.build($currentTrigger, e);
                    // abort if build() returned false
                    if (built === false) {
                        return;
                    }
                    
                    // dynamically build menu on invocation
                    e.data = $.extend(true, {}, defaults, e.data, built || {});

                    // abort if there are no items to display
                    if (!e.data.items || $.isEmptyObject(e.data.items)) {
                        // Note: jQuery captures and ignores errors from event handlers
                        if (window.console) {
                            (console.error || console.log)("No items specified to show in contextMenu");
                        }
                        
                        throw new Error('No Items specified');
                    }
                    
                    // backreference for custom command type creation
                    e.data.$trigger = $currentTrigger;
                    
                    op.create(e.data);
                }
                // show menu
                op.show.call($this, e.data, e.pageX, e.pageY);
            }
        },
        // contextMenu left-click trigger
        click: function(e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            $(this).trigger($.Event("contextmenu", { data: e.data, pageX: e.pageX, pageY: e.pageY }));
        },
        // contextMenu right-click trigger
        mousedown: function(e) {
            // register mouse down
            var $this = $(this);
            
            // hide any previous menus
            if ($currentTrigger && $currentTrigger.length && !$currentTrigger.is($this)) {
                $currentTrigger.data('contextMenu').$menu.trigger('contextmenu:hide');
            }
            
            // activate on right click
            if (e.button == 2) {
                $currentTrigger = $this.data('contextMenuActive', true);
            }
        },
        // contextMenu right-click trigger
        mouseup: function(e) {
            // show menu
            var $this = $(this);
            if ($this.data('contextMenuActive') && $currentTrigger && $currentTrigger.length && $currentTrigger.is($this) && !$this.hasClass('context-menu-disabled')) {
                e.preventDefault();
                e.stopImmediatePropagation();
                $currentTrigger = $this;
                $this.trigger($.Event("contextmenu", { data: e.data, pageX: e.pageX, pageY: e.pageY }));
            }
            
            $this.removeData('contextMenuActive');
        },
        // contextMenu hover trigger
        mouseenter: function(e) {
            var $this = $(this),
                $related = $(e.relatedTarget),
                $document = $(document);
            
            // abort if we're coming from a menu
            if ($related.is('.context-menu-list') || $related.closest('.context-menu-list').length) {
                return;
            }
            
            // abort if a menu is shown
            if ($currentTrigger && $currentTrigger.length) {
                return;
            }
            
            hoveract.pageX = e.pageX;
            hoveract.pageY = e.pageY;
            hoveract.data = e.data;
            $document.on('mousemove.contextMenuShow', handle.mousemove);
            hoveract.timer = setTimeout(function() {
                hoveract.timer = null;
                $document.off('mousemove.contextMenuShow');
                $currentTrigger = $this;
                $this.trigger($.Event("contextmenu", { data: hoveract.data, pageX: hoveract.pageX, pageY: hoveract.pageY }));
            }, e.data.delay );
        },
        // contextMenu hover trigger
        mousemove: function(e) {
            hoveract.pageX = e.pageX;
            hoveract.pageY = e.pageY;
        },
        // contextMenu hover trigger
        mouseleave: function(e) {
            // abort if we're leaving for a menu
            var $related = $(e.relatedTarget);
            if ($related.is('.context-menu-list') || $related.closest('.context-menu-list').length) {
                return;
            }
            
            try {
                clearTimeout(hoveract.timer);
            } catch(e) {}
            
            hoveract.timer = null;
        },
        
        // click on layer to hide contextMenu
        layerClick: function(e) {
            var $this = $(this),
                root = $this.data('contextMenuRoot'),
                mouseup = false,
                button = e.button,
                x = e.pageX,
                y = e.pageY,
                target, 
                offset,
                selectors;
                
            e.preventDefault();
            e.stopImmediatePropagation();
            
            setTimeout(function() {
                var $window, hideshow, possibleTarget;
                var triggerAction = ((root.trigger == 'left' && button === 0) || (root.trigger == 'right' && button === 2));
                
                // find the element that would've been clicked, wasn't the layer in the way
                if (document.elementFromPoint) {
                    root.$layer.hide();
                    target = document.elementFromPoint(x - $win.scrollLeft(), y - $win.scrollTop());
                    root.$layer.show();
                }
                
                if (root.reposition && triggerAction) {
                    if (document.elementFromPoint) {
                        if (root.$trigger.is(target) || root.$trigger.has(target).length) {
                            root.position.call(root.$trigger, root, x, y);
                            return;
                        }
                    } else {
                        offset = root.$trigger.offset();
                        $window = $(window);
                        // while this looks kinda awful, it's the best way to avoid
                        // unnecessarily calculating any positions
                        offset.top += $window.scrollTop();
                        if (offset.top <= e.pageY) {
                            offset.left += $window.scrollLeft();
                            if (offset.left <= e.pageX) {
                                offset.bottom = offset.top + root.$trigger.outerHeight();
                                if (offset.bottom >= e.pageY) {
                                    offset.right = offset.left + root.$trigger.outerWidth();
                                    if (offset.right >= e.pageX) {
                                        // reposition
                                        root.position.call(root.$trigger, root, x, y);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
                
                if (target && triggerAction) {
                    root.$trigger.one('contextmenu:hidden', function() {
                        $(target).contextMenu({x: x, y: y});
                    });
                }

                root.$menu.trigger('contextmenu:hide');
            }, 50);
        },
        // key handled :hover
        keyStop: function(e, opt) {
            if (!opt.isInput) {
                e.preventDefault();
            }
            
            e.stopPropagation();
        },
        key: function(e) {
            var opt = $currentTrigger.data('contextMenu') || {};

            switch (e.keyCode) {
                case 9:
                case 38: // up
                    handle.keyStop(e, opt);
                    // if keyCode is [38 (up)] or [9 (tab) with shift]
                    if (opt.isInput) {
                        if (e.keyCode == 9 && e.shiftKey) {
                            e.preventDefault();
                            opt.$selected && opt.$selected.find('input, textarea, select').blur();
                            opt.$menu.trigger('prevcommand');
                            return;
                        } else if (e.keyCode == 38 && opt.$selected.find('input, textarea, select').prop('type') == 'checkbox') {
                            // checkboxes don't capture this key
                            e.preventDefault();
                            return;
                        }
                    } else if (e.keyCode != 9 || e.shiftKey) {
                        opt.$menu.trigger('prevcommand');
                        return;
                    }
                    // omitting break;
                    
                // case 9: // tab - reached through omitted break;
                case 40: // down
                    handle.keyStop(e, opt);
                    if (opt.isInput) {
                        if (e.keyCode == 9) {
                            e.preventDefault();
                            opt.$selected && opt.$selected.find('input, textarea, select').blur();
                            opt.$menu.trigger('nextcommand');
                            return;
                        } else if (e.keyCode == 40 && opt.$selected.find('input, textarea, select').prop('type') == 'checkbox') {
                            // checkboxes don't capture this key
                            e.preventDefault();
                            return;
                        }
                    } else {
                        opt.$menu.trigger('nextcommand');
                        return;
                    }
                    break;
                
                case 37: // left
                    handle.keyStop(e, opt);
                    if (opt.isInput || !opt.$selected || !opt.$selected.length) {
                        break;
                    }
                
                    if (!opt.$selected.parent().hasClass('context-menu-root')) {
                        var $parent = opt.$selected.parent().parent();
                        opt.$selected.trigger('contextmenu:blur');
                        opt.$selected = $parent;
                        return;
                    }
                    break;
                    
                case 39: // right
                    handle.keyStop(e, opt);
                    if (opt.isInput || !opt.$selected || !opt.$selected.length) {
                        break;
                    }
                    
                    var itemdata = opt.$selected.data('contextMenu') || {};
                    if (itemdata.$menu && opt.$selected.hasClass('context-menu-submenu')) {
                        opt.$selected = null;
                        itemdata.$selected = null;
                        itemdata.$menu.trigger('nextcommand');
                        return;
                    }
                    break;
                
                case 35: // end
                case 36: // home
                    if (opt.$selected && opt.$selected.find('input, textarea, select').length) {
                        return;
                    } else {
                        (opt.$selected && opt.$selected.parent() || opt.$menu)
                            .children(':not(.disabled, .not-selectable)')[e.keyCode == 36 ? 'first' : 'last']()
                            .trigger('contextmenu:focus');
                        e.preventDefault();
                        return;
                    }
                    break;
                    
                case 13: // enter
                    handle.keyStop(e, opt);
                    if (opt.isInput) {
                        if (opt.$selected && !opt.$selected.is('textarea, select')) {
                            e.preventDefault();
                            return;
                        }
                        break;
                    }
                    opt.$selected && opt.$selected.trigger('mouseup');
                    return;
                    
                case 32: // space
                case 33: // page up
                case 34: // page down
                    // prevent browser from scrolling down while menu is visible
                    handle.keyStop(e, opt);
                    return;
                    
                case 27: // esc
                    handle.keyStop(e, opt);
                    opt.$menu.trigger('contextmenu:hide');
                    return;
                    
                default: // 0-9, a-z
                    var k = (String.fromCharCode(e.keyCode)).toUpperCase();
                    if (opt.accesskeys[k]) {
                        // according to the specs accesskeys must be invoked immediately
                        opt.accesskeys[k].$node.trigger(opt.accesskeys[k].$menu
                            ? 'contextmenu:focus'
                            : 'mouseup'
                        );
                        return;
                    }
                    break;
            }
            // pass event to selected item, 
            // stop propagation to avoid endless recursion
            e.stopPropagation();
            opt.$selected && opt.$selected.trigger(e);
        },

        // select previous possible command in menu
        prevItem: function(e) {
            e.stopPropagation();
            var opt = $(this).data('contextMenu') || {};

            // obtain currently selected menu
            if (opt.$selected) {
                var $s = opt.$selected;
                opt = opt.$selected.parent().data('contextMenu') || {};
                opt.$selected = $s;
            }
            
            var $children = opt.$menu.children(),
                $prev = !opt.$selected || !opt.$selected.prev().length ? $children.last() : opt.$selected.prev(),
                $round = $prev;
            
            // skip disabled
            while ($prev.hasClass('disabled') || $prev.hasClass('not-selectable')) {
                if ($prev.prev().length) {
                    $prev = $prev.prev();
                } else {
                    $prev = $children.last();
                }
                if ($prev.is($round)) {
                    // break endless loop
                    return;
                }
            }
            
            // leave current
            if (opt.$selected) {
                handle.itemMouseleave.call(opt.$selected.get(0), e);
            }
            
            // activate next
            handle.itemMouseenter.call($prev.get(0), e);
            
            // focus input
            var $input = $prev.find('input, textarea, select');
            if ($input.length) {
                $input.focus();
            }
        },
        // select next possible command in menu
        nextItem: function(e) {
            e.stopPropagation();
            var opt = $(this).data('contextMenu') || {};

            // obtain currently selected menu
            if (opt.$selected) {
                var $s = opt.$selected;
                opt = opt.$selected.parent().data('contextMenu') || {};
                opt.$selected = $s;
            }

            var $children = opt.$menu.children(),
                $next = !opt.$selected || !opt.$selected.next().length ? $children.first() : opt.$selected.next(),
                $round = $next;

            // skip disabled
            while ($next.hasClass('disabled') || $next.hasClass('not-selectable')) {
                if ($next.next().length) {
                    $next = $next.next();
                } else {
                    $next = $children.first();
                }
                if ($next.is($round)) {
                    // break endless loop
                    return;
                }
            }
            
            // leave current
            if (opt.$selected) {
                handle.itemMouseleave.call(opt.$selected.get(0), e);
            }
            
            // activate next
            handle.itemMouseenter.call($next.get(0), e);
            
            // focus input
            var $input = $next.find('input, textarea, select');
            if ($input.length) {
                $input.focus();
            }
        },
        
        // flag that we're inside an input so the key handler can act accordingly
        focusInput: function(e) {
            var $this = $(this).closest('.context-menu-item'),
                data = $this.data(),
                opt = data.contextMenu,
                root = data.contextMenuRoot;

            root.$selected = opt.$selected = $this;
            root.isInput = opt.isInput = true;
        },
        // flag that we're inside an input so the key handler can act accordingly
        blurInput: function(e) {
            var $this = $(this).closest('.context-menu-item'),
                data = $this.data(),
                opt = data.contextMenu,
                root = data.contextMenuRoot;

            root.isInput = opt.isInput = false;
        },
        
        // :hover on menu
        menuMouseenter: function(e) {
            var root = $(this).data().contextMenuRoot;
            root.hovering = true;
        },
        // :hover on menu
        menuMouseleave: function(e) {
            var root = $(this).data().contextMenuRoot;
            if (root.$layer && root.$layer.is(e.relatedTarget)) {
                root.hovering = false;
            }
        },
        
        // :hover done manually so key handling is possible
        itemMouseenter: function(e) {
            var $this = $(this),
                data = $this.data(),
                opt = data.contextMenu,
                root = data.contextMenuRoot;
            
            root.hovering = true;

            // abort if we're re-entering
            if (e && root.$layer && root.$layer.is(e.relatedTarget)) {
                e.preventDefault();
                e.stopImmediatePropagation();
            }

            // make sure only one item is selected
            (opt.$menu ? opt : root).$menu
                .children('.hover').trigger('contextmenu:blur');

            if ($this.hasClass('disabled') || $this.hasClass('not-selectable')) {
                opt.$selected = null;
                return;
            }
            
            $this.trigger('contextmenu:focus');
        },
        // :hover done manually so key handling is possible
        itemMouseleave: function(e) {
            var $this = $(this),
                data = $this.data(),
                opt = data.contextMenu,
                root = data.contextMenuRoot;

            if (root !== opt && root.$layer && root.$layer.is(e.relatedTarget)) {
                root.$selected && root.$selected.trigger('contextmenu:blur');
                e.preventDefault();
                e.stopImmediatePropagation();
                root.$selected = opt.$selected = opt.$node;
                return;
            }
            
            $this.trigger('contextmenu:blur');
        },
        // contextMenu item click
        itemClick: function(e) {
            var $this = $(this),
                data = $this.data(),
                opt = data.contextMenu,
                root = data.contextMenuRoot,
                key = data.contextMenuKey,
                callback;

            // abort if the key is unknown or disabled or is a menu
            if (!opt.items[key] || $this.is('.disabled, .context-menu-submenu, .context-menu-separator, .not-selectable')) {
                return;
            }

            e.preventDefault();
            e.stopImmediatePropagation();

            if ($.isFunction(root.callbacks[key]) && Object.prototype.hasOwnProperty.call(root.callbacks, key)) {
                // item-specific callback
                callback = root.callbacks[key];
            } else if ($.isFunction(root.callback)) {
                // default callback
                callback = root.callback;                
            } else {
                // no callback, no action
                return;
            }

            // hide menu if callback doesn't stop that
            if (callback.call(root.$trigger, key, root) !== false) {
                root.$menu.trigger('contextmenu:hide');
            } else if (root.$menu.parent().length) {
                op.update.call(root.$trigger, root);
            }
        },
        // ignore click events on input elements
        inputClick: function(e) {
            e.stopImmediatePropagation();
        },
        
        // hide <menu>
        hideMenu: function(e, data) {
            var root = $(this).data('contextMenuRoot');
            op.hide.call(root.$trigger, root, data && data.force);
        },
        // focus <command>
        focusItem: function(e) {
            e.stopPropagation();
            var $this = $(this),
                data = $this.data(),
                opt = data.contextMenu,
                root = data.contextMenuRoot;

            $this.addClass('hover')
                .siblings('.hover').trigger('contextmenu:blur');
            
            // remember selected
            opt.$selected = root.$selected = $this;
            
            // position sub-menu - do after show so dumb $.ui.position can keep up
            if (opt.$node) {
                root.positionSubmenu.call(opt.$node, opt.$menu);
            }
        },
        // blur <command>
        blurItem: function(e) {
            e.stopPropagation();
            var $this = $(this),
                data = $this.data(),
                opt = data.contextMenu,
                root = data.contextMenuRoot;
            
            $this.removeClass('hover');
            opt.$selected = null;
        }
    },
    // operations
    op = {
        show: function(opt, x, y) {
            var $trigger = $(this),
                offset,
                css = {};

            // hide any open menus
            $('#context-menu-layer').trigger('mousedown');

            // backreference for callbacks
            opt.$trigger = $trigger;

            // show event
            if (opt.events.show.call($trigger, opt) === false) {
                $currentTrigger = null;
                return;
            }

            // create or update context menu
            op.update.call($trigger, opt);
            
            // position menu
            opt.position.call($trigger, opt, x, y);

            // make sure we're in front
            if (opt.zIndex) {
                css.zIndex = zindex($trigger) + opt.zIndex;
            }
            
            // add layer
            op.layer.call(opt.$menu, opt, css.zIndex);
            
            // adjust sub-menu zIndexes
            opt.$menu.find('ul').css('zIndex', css.zIndex + 1);
            
            // position and show context menu
            opt.$menu.css( css )[opt.animation.show](opt.animation.duration, function() {
                $trigger.trigger('contextmenu:visible');
            });
            // make options available and set state
            $trigger
                .data('contextMenu', opt)
                .addClass("context-menu-active");
            
            // register key handler
            $(document).off('keydown.contextMenu').on('keydown.contextMenu', handle.key);
            // register autoHide handler
            if (opt.autoHide) {
                // mouse position handler
                $(document).on('mousemove.contextMenuAutoHide', function(e) {
                    // need to capture the offset on mousemove,
                    // since the page might've been scrolled since activation
                    var pos = $trigger.offset();
                    pos.right = pos.left + $trigger.outerWidth();
                    pos.bottom = pos.top + $trigger.outerHeight();
                    
                    if (opt.$layer && !opt.hovering && (!(e.pageX >= pos.left && e.pageX <= pos.right) || !(e.pageY >= pos.top && e.pageY <= pos.bottom))) {
                        // if mouse in menu...
                        opt.$menu.trigger('contextmenu:hide');
                    }
                });
            }
        },
        hide: function(opt, force) {
            var $trigger = $(this);
            if (!opt) {
                opt = $trigger.data('contextMenu') || {};
            }
            
            // hide event
            if (!force && opt.events && opt.events.hide.call($trigger, opt) === false) {
                return;
            }
            
            // remove options and revert state
            $trigger
                .removeData('contextMenu')
                .removeClass("context-menu-active");
            
            if (opt.$layer) {
                // keep layer for a bit so the contextmenu event can be aborted properly by opera
                setTimeout((function($layer) {
                    return function(){
                        $layer.remove();
                    };
                })(opt.$layer), 10);
                
                try {
                    delete opt.$layer;
                } catch(e) {
                    opt.$layer = null;
                }
            }
            
            // remove handle
            $currentTrigger = null;
            // remove selected
            opt.$menu.find('.hover').trigger('contextmenu:blur');
            opt.$selected = null;
            // unregister key and mouse handlers
            //$(document).off('.contextMenuAutoHide keydown.contextMenu'); // http://bugs.jquery.com/ticket/10705
            $(document).off('.contextMenuAutoHide').off('keydown.contextMenu');
            // hide menu
            opt.$menu && opt.$menu[opt.animation.hide](opt.animation.duration, function (){
                // tear down dynamically built menu after animation is completed.
                if (opt.build) {
                    opt.$menu.remove();
                    $.each(opt, function(key, value) {
                        switch (key) {
                            case 'ns':
                            case 'selector':
                            case 'build':
                            case 'trigger':
                                return true;

                            default:
                                opt[key] = undefined;
                                try {
                                    delete opt[key];
                                } catch (e) {}
                                return true;
                        }
                    });
                }
                
                setTimeout(function() {
                    $trigger.trigger('contextmenu:hidden');
                }, 10);
            });
        },
        create: function(opt, root) {
            if (root === undefined) {
                root = opt;
            }
            // create contextMenu
            opt.$menu = $('<ul class="context-menu-list"></ul>').addClass(opt.className || "").data({
                'contextMenu': opt,
                'contextMenuRoot': root
            });
            
            $.each(['callbacks', 'commands', 'inputs'], function(i,k){
                opt[k] = {};
                if (!root[k]) {
                    root[k] = {};
                }
            });
            
            root.accesskeys || (root.accesskeys = {});
            
            // create contextMenu items
            $.each(opt.items, function(key, item){
                var $t = $('<li class="context-menu-item"></li>').addClass(item.className || ""),
                    $label = null,
                    $input = null;
                
                // iOS needs to see a click-event bound to an element to actually
                // have the TouchEvents infrastructure trigger the click event
                $t.on('click', $.noop);
                
                item.$node = $t.data({
                    'contextMenu': opt,
                    'contextMenuRoot': root,
                    'contextMenuKey': key
                });
                
                // register accesskey
                // NOTE: the accesskey attribute should be applicable to any element, but Safari5 and Chrome13 still can't do that
                if (item.accesskey) {
                    var aks = splitAccesskey(item.accesskey);
                    for (var i=0, ak; ak = aks[i]; i++) {
                        if (!root.accesskeys[ak]) {
                            root.accesskeys[ak] = item;
                            item._name = item.name.replace(new RegExp('(' + ak + ')', 'i'), '<span class="context-menu-accesskey">$1</span>');
                            break;
                        }
                    }
                }
                
                if (typeof item == "string") {
                    $t.addClass('context-menu-separator not-selectable');
                } else if (item.type && types[item.type]) {
                    // run custom type handler
                    types[item.type].call($t, item, opt, root);
                    // register commands
                    $.each([opt, root], function(i,k){
                        k.commands[key] = item;
                        if ($.isFunction(item.callback)) {
                            k.callbacks[key] = item.callback;
                        }
                    });
                } else {
                    // add label for input
                    if (item.type == 'html') {
                        $t.addClass('context-menu-html not-selectable');
                    } else if (item.type) {
                        $label = $('<label></label>').appendTo($t);
                        $('<span></span>').html(item._name || item.name).appendTo($label);
                        $t.addClass('context-menu-input');
                        opt.hasTypes = true;
                        $.each([opt, root], function(i,k){
                            k.commands[key] = item;
                            k.inputs[key] = item;
                        });
                    } else if (item.items) {
                        item.type = 'sub';
                    }
                
                    switch (item.type) {
                        case 'text':
                            $input = $('<input type="text" value="1" name="" value="">')
                                .attr('name', 'context-menu-input-' + key)
                                .val(item.value || "")
                                .appendTo($label);
                            break;
                    
                        case 'textarea':
                            $input = $('<textarea name=""></textarea>')
                                .attr('name', 'context-menu-input-' + key)
                                .val(item.value || "")
                                .appendTo($label);

                            if (item.height) {
                                $input.height(item.height);
                            }
                            break;

                        case 'checkbox':
                            $input = $('<input type="checkbox" value="1" name="" value="">')
                                .attr('name', 'context-menu-input-' + key)
                                .val(item.value || "")
                                .prop("checked", !!item.selected)
                                .prependTo($label);
                            break;

                        case 'radio':
                            $input = $('<input type="radio" value="1" name="" value="">')
                                .attr('name', 'context-menu-input-' + item.radio)
                                .val(item.value || "")
                                .prop("checked", !!item.selected)
                                .prependTo($label);
                            break;
                    
                        case 'select':
                            $input = $('<select name="">')
                                .attr('name', 'context-menu-input-' + key)
                                .appendTo($label);
                            if (item.options) {
                                $.each(item.options, function(value, text) {
                                    $('<option></option>').val(value).text(text).appendTo($input);
                                });
                                $input.val(item.selected);
                            }
                            break;
                        
                        case 'sub':
                            // FIXME: shouldn't this .html() be a .text()?
                            $('<span></span>').html(item._name || item.name).appendTo($t);
                            item.appendTo = item.$node;
                            op.create(item, root);
                            $t.data('contextMenu', item).addClass('context-menu-submenu');
                            item.callback = null;
                            break;
                        
                        case 'html':
                            $(item.html).appendTo($t);
                            break;
                        
                        default:
                            $.each([opt, root], function(i,k){
                                k.commands[key] = item;
                                if ($.isFunction(item.callback)) {
                                    k.callbacks[key] = item.callback;
                                }
                            });
                            // FIXME: shouldn't this .html() be a .text()?
                            $('<span></span>').html(item._name || item.name || "").appendTo($t);
                            break;
                    }
                    
                    // disable key listener in <input>
                    if (item.type && item.type != 'sub' && item.type != 'html') {
                        $input
                            .on('focus', handle.focusInput)
                            .on('blur', handle.blurInput);
                        
                        if (item.events) {
                            $input.on(item.events, opt);
                        }
                    }
                
                    // add icons
                    if (item.icon) {
                        $t.addClass("icon icon-" + item.icon);
                    }
                }
                
                // cache contained elements
                item.$input = $input;
                item.$label = $label;

                // attach item to menu
                $t.appendTo(opt.$menu);
                
                // Disable text selection
                if (!opt.hasTypes && $.support.eventSelectstart) {
                    // browsers support user-select: none, 
                    // IE has a special event for text-selection
                    // browsers supporting neither will not be preventing text-selection
                    $t.on('selectstart.disableTextSelect', handle.abortevent);
                }
            });
            // attach contextMenu to <body> (to bypass any possible overflow:hidden issues on parents of the trigger element)
            if (!opt.$node) {
                opt.$menu.css('display', 'none').addClass('context-menu-root');
            }
            opt.$menu.appendTo(opt.appendTo || document.body);
        },
        resize: function($menu, nested) {
            // determine widths of submenus, as CSS won't grow them automatically
            // position:absolute within position:absolute; min-width:100; max-width:200; results in width: 100;
            // kinda sucks hard...

            // determine width of absolutely positioned element
            $menu.css({position: 'absolute', display: 'block'});
            // don't apply yet, because that would break nested elements' widths
            // add a pixel to circumvent word-break issue in IE9 - #80
            $menu.data('width', Math.ceil($menu.width()) + 1);
            // reset styles so they allow nested elements to grow/shrink naturally
            $menu.css({
                position: 'static',
                minWidth: '0px',
                maxWidth: '100000px'
            });
            // identify width of nested menus
            $menu.find('> li > ul').each(function() {
                op.resize($(this), true);
            });
            // reset and apply changes in the end because nested
            // elements' widths wouldn't be calculatable otherwise
            if (!nested) {
                $menu.find('ul').andSelf().css({
                    position: '', 
                    display: '',
                    minWidth: '',
                    maxWidth: ''
                }).width(function() {
                    return $(this).data('width');
                });
            }
        },
        update: function(opt, root) {
            var $trigger = this;
            if (root === undefined) {
                root = opt;
                op.resize(opt.$menu);
            }
            // re-check disabled for each item
            opt.$menu.children().each(function(){
                var $item = $(this),
                    key = $item.data('contextMenuKey'),
                    item = opt.items[key],
                    disabled = ($.isFunction(item.disabled) && item.disabled.call($trigger, key, root)) || item.disabled === true;

                // dis- / enable item
                $item[disabled ? 'addClass' : 'removeClass']('disabled');
                
                if (item.type) {
                    // dis- / enable input elements
                    $item.find('input, select, textarea').prop('disabled', disabled);
                    
                    // update input states
                    switch (item.type) {
                        case 'text':
                        case 'textarea':
                            item.$input.val(item.value || "");
                            break;
                            
                        case 'checkbox':
                        case 'radio':
                            item.$input.val(item.value || "").prop('checked', !!item.selected);
                            break;
                            
                        case 'select':
                            item.$input.val(item.selected || "");
                            break;
                    }
                }
                
                if (item.$menu) {
                    // update sub-menu
                    op.update.call($trigger, item, root);
                }
            });
        },
        layer: function(opt, zIndex) {
            // add transparent layer for click area
            // filter and background for Internet Explorer, Issue #23
            var $layer = opt.$layer = $('<div id="context-menu-layer" style="position:fixed; z-index:' + zIndex + '; top:0; left:0; opacity: 0; filter: alpha(opacity=0); background-color: #000;"></div>')
                .css({height: $win.height(), width: $win.width(), display: 'block'})
                .data('contextMenuRoot', opt)
                .insertBefore(this)
                .on('contextmenu', handle.abortevent)
                .on('mousedown', handle.layerClick);
            
            // IE6 doesn't know position:fixed;
            if (!$.support.fixedPosition) {
                $layer.css({
                    'position' : 'absolute',
                    'height' : $(document).height()
                });
            }
            
            return $layer;
        }
    };

// split accesskey according to http://www.whatwg.org/specs/web-apps/current-work/multipage/editing.html#assigned-access-key
function splitAccesskey(val) {
    var t = val.split(/\s+/),
        keys = [];
        
    for (var i=0, k; k = t[i]; i++) {
        k = k[0].toUpperCase(); // first character only
        // theoretically non-accessible characters should be ignored, but different systems, different keyboard layouts, ... screw it.
        // a map to look up already used access keys would be nice
        keys.push(k);
    }
    
    return keys;
}

// handle contextMenu triggers
$.fn.contextMenu = function(operation) {
    if (operation === undefined) {
        this.first().trigger('contextmenu');
    } else if (operation.x && operation.y) {
        this.first().trigger($.Event("contextmenu", {pageX: operation.x, pageY: operation.y}));
    } else if (operation === "hide") {
        var $menu = this.data('contextMenu').$menu;
        $menu && $menu.trigger('contextmenu:hide');
    } else if (operation === "destroy") {
        $.contextMenu("destroy", {context: this});
    } else if ($.isPlainObject(operation)) {
        operation.context = this;
        $.contextMenu("create", operation);
    } else if (operation) {
        this.removeClass('context-menu-disabled');
    } else if (!operation) {
        this.addClass('context-menu-disabled');
    }
    
    return this;
};

// manage contextMenu instances
$.contextMenu = function(operation, options) {
    if (typeof operation != 'string') {
        options = operation;
        operation = 'create';
    }
    
    if (typeof options == 'string') {
        options = {selector: options};
    } else if (options === undefined) {
        options = {};
    }
    
    // merge with default options
    var o = $.extend(true, {}, defaults, options || {});
    var $document = $(document);
    var $context = $document;
    var _hasContext = false;
    
    if (!o.context || !o.context.length) {
        o.context = document;
    } else {
        // you never know what they throw at you...
        $context = $(o.context).first();
        o.context = $context.get(0);
        _hasContext = o.context !== document;
    }
    
    switch (operation) {
        case 'create':
            // no selector no joy
            if (!o.selector) {
                throw new Error('No selector specified');
            }
            // make sure internal classes are not bound to
            if (o.selector.match(/.context-menu-(list|item|input)($|\s)/)) {
                throw new Error('Cannot bind to selector "' + o.selector + '" as it contains a reserved className');
            }
            if (!o.build && (!o.items || $.isEmptyObject(o.items))) {
                throw new Error('No Items specified');
            }
            counter ++;
            o.ns = '.contextMenu' + counter;
            if (!_hasContext) {
                namespaces[o.selector] = o.ns;
            }
            menus[o.ns] = o;
            
            // default to right click
            if (!o.trigger) {
                o.trigger = 'right';
            }
            
            if (!initialized) {
                // make sure item click is registered first
                $document
                    .on({
                        'contextmenu:hide.contextMenu': handle.hideMenu,
                        'prevcommand.contextMenu': handle.prevItem,
                        'nextcommand.contextMenu': handle.nextItem,
                        'contextmenu.contextMenu': handle.abortevent,
                        'mouseenter.contextMenu': handle.menuMouseenter,
                        'mouseleave.contextMenu': handle.menuMouseleave
                    }, '.context-menu-list')
                    .on('mouseup.contextMenu', '.context-menu-input', handle.inputClick)
                    .on({
                        'mouseup.contextMenu': handle.itemClick,
                        'contextmenu:focus.contextMenu': handle.focusItem,
                        'contextmenu:blur.contextMenu': handle.blurItem,
                        'contextmenu.contextMenu': handle.abortevent,
                        'mouseenter.contextMenu': handle.itemMouseenter,
                        'mouseleave.contextMenu': handle.itemMouseleave
                    }, '.context-menu-item');

                initialized = true;
            }
            
            // engage native contextmenu event
            $context
                .on('contextmenu' + o.ns, o.selector, o, handle.contextmenu);
            
            if (_hasContext) {
                // add remove hook, just in case
                $context.on('remove' + o.ns, function() {
                    $(this).contextMenu("destroy");
                });
            }
            
            switch (o.trigger) {
                case 'hover':
                        $context
                            .on('mouseenter' + o.ns, o.selector, o, handle.mouseenter)
                            .on('mouseleave' + o.ns, o.selector, o, handle.mouseleave);                    
                    break;
                    
                case 'left':
                        $context.on('click' + o.ns, o.selector, o, handle.click);
                    break;
                
            }
            
            // create menu
            if (!o.build) {
                op.create(o);
            }
            break;
        
        case 'destroy':
            var $visibleMenu;
            if (_hasContext) {
                // get proper options 
                var context = o.context;
                $.each(menus, function(ns, o) {
                    if (o.context !== context) {
                        return true;
                    }
                    
                    $visibleMenu = $('.context-menu-list').filter(':visible');
                    if ($visibleMenu.length && $visibleMenu.data().contextMenuRoot.$trigger.is($(o.context).find(o.selector))) {
                        $visibleMenu.trigger('contextmenu:hide', {force: true});
                    }

                    try {
                        if (menus[o.ns].$menu) {
                            menus[o.ns].$menu.remove();
                        }

                        delete menus[o.ns];
                    } catch(e) {
                        menus[o.ns] = null;
                    }

                    $(o.context).off(o.ns);
                    
                    return true;
                });
            } else if (!o.selector) {
                $document.off('.contextMenu .contextMenuAutoHide');
                $.each(menus, function(ns, o) {
                    $(o.context).off(o.ns);
                });
                
                namespaces = {};
                menus = {};
                counter = 0;
                initialized = false;
                
                $('#context-menu-layer, .context-menu-list').remove();
            } else if (namespaces[o.selector]) {
                $visibleMenu = $('.context-menu-list').filter(':visible');
                if ($visibleMenu.length && $visibleMenu.data().contextMenuRoot.$trigger.is(o.selector)) {
                    $visibleMenu.trigger('contextmenu:hide', {force: true});
                }
                
                try {
                    if (menus[namespaces[o.selector]].$menu) {
                        menus[namespaces[o.selector]].$menu.remove();
                    }
                    
                    delete menus[namespaces[o.selector]];
                } catch(e) {
                    menus[namespaces[o.selector]] = null;
                }
                
                $document.off(namespaces[o.selector]);
            }
            break;
        
        case 'html5':
            // if <command> or <menuitem> are not handled by the browser,
            // or options was a bool true,
            // initialize $.contextMenu for them
            if ((!$.support.htmlCommand && !$.support.htmlMenuitem) || (typeof options == "boolean" && options)) {
                $('menu[type="context"]').each(function() {
                    if (this.id) {
                        $.contextMenu({
                            selector: '[contextmenu=' + this.id +']',
                            items: $.contextMenu.fromMenu(this)
                        });
                    }
                }).css('display', 'none');
            }
            break;
        
        default:
            throw new Error('Unknown operation "' + operation + '"');
    }
    
    return this;
};

// import values into <input> commands
$.contextMenu.setInputValues = function(opt, data) {
    if (data === undefined) {
        data = {};
    }
    
    $.each(opt.inputs, function(key, item) {
        switch (item.type) {
            case 'text':
            case 'textarea':
                item.value = data[key] || "";
                break;

            case 'checkbox':
                item.selected = data[key] ? true : false;
                break;
                
            case 'radio':
                item.selected = (data[item.radio] || "") == item.value ? true : false;
                break;
            
            case 'select':
                item.selected = data[key] || "";
                break;
        }
    });
};

// export values from <input> commands
$.contextMenu.getInputValues = function(opt, data) {
    if (data === undefined) {
        data = {};
    }
    
    $.each(opt.inputs, function(key, item) {
        switch (item.type) {
            case 'text':
            case 'textarea':
            case 'select':
                data[key] = item.$input.val();
                break;

            case 'checkbox':
                data[key] = item.$input.prop('checked');
                break;
                
            case 'radio':
                if (item.$input.prop('checked')) {
                    data[item.radio] = item.value;
                }
                break;
        }
    });
    
    return data;
};

// find <label for="xyz">
function inputLabel(node) {
    return (node.id && $('label[for="'+ node.id +'"]').val()) || node.name;
}

// convert <menu> to items object
function menuChildren(items, $children, counter) {
    if (!counter) {
        counter = 0;
    }
    
    $children.each(function() {
        var $node = $(this),
            node = this,
            nodeName = this.nodeName.toLowerCase(),
            label,
            item;
        
        // extract <label><input>
        if (nodeName == 'label' && $node.find('input, textarea, select').length) {
            label = $node.text();
            $node = $node.children().first();
            node = $node.get(0);
            nodeName = node.nodeName.toLowerCase();
        }
        
        
        
        // http://www.whatwg.org/specs/web-apps/current-work/multipage/commands.html#concept-command
        switch (nodeName) {
            // http://www.whatwg.org/specs/web-apps/current-work/multipage/interactive-elements.html#the-menu-element
            case 'menu':
                item = {name: $node.attr('label'), items: {}};
                counter = menuChildren(item.items, $node.children(), counter);
                break;
            
            // http://www.whatwg.org/specs/web-apps/current-work/multipage/commands.html#using-the-a-element-to-define-a-command
            case 'a':
            // http://www.whatwg.org/specs/web-apps/current-work/multipage/commands.html#using-the-button-element-to-define-a-command
            case 'button':
                item = {
                    name: $node.text(),
                    disabled: !!$node.attr('disabled'),
                    callback: (function(){ return function(){ $node.click(); }; })()
                };
                break;
            
            // http://www.whatwg.org/specs/web-apps/current-work/multipage/commands.html#using-the-command-element-to-define-a-command

            case 'menuitem':
            case 'command':
                switch ($node.attr('type')) {
                    case undefined:
                    case 'command':
                    case 'menuitem':
                        item = {
                            name: $node.attr('label'),
                            disabled: !!$node.attr('disabled'),
                            callback: (function(){ return function(){ $node.click(); }; })()
                        };
                        break;
                        
                    case 'checkbox':
                        item = {
                            type: 'checkbox',
                            disabled: !!$node.attr('disabled'),
                            name: $node.attr('label'),
                            selected: !!$node.attr('checked')
                        };
                        break;
                        
                    case 'radio':
                        item = {
                            type: 'radio',
                            disabled: !!$node.attr('disabled'),
                            name: $node.attr('label'),
                            radio: $node.attr('radiogroup'),
                            value: $node.attr('id'),
                            selected: !!$node.attr('checked')
                        };
                        break;
                        
                    default:
                        item = undefined;
                }
                break;
 
            case 'hr':
                item = '-------';
                break;
                
            case 'input':
                switch ($node.attr('type')) {
                    case 'text':
                        item = {
                            type: 'text',
                            name: label || inputLabel(node),
                            disabled: !!$node.attr('disabled'),
                            value: $node.val()
                        };
                        break;
                        
                    case 'checkbox':
                        item = {
                            type: 'checkbox',
                            name: label || inputLabel(node),
                            disabled: !!$node.attr('disabled'),
                            selected: !!$node.attr('checked')
                        };
                        break;
                        
                    case 'radio':
                        item = {
                            type: 'radio',
                            name: label || inputLabel(node),
                            disabled: !!$node.attr('disabled'),
                            radio: !!$node.attr('name'),
                            value: $node.val(),
                            selected: !!$node.attr('checked')
                        };
                        break;
                    
                    default:
                        item = undefined;
                        break;
                }
                break;
                
            case 'select':
                item = {
                    type: 'select',
                    name: label || inputLabel(node),
                    disabled: !!$node.attr('disabled'),
                    selected: $node.val(),
                    options: {}
                };
                $node.children().each(function(){
                    item.options[this.value] = $(this).text();
                });
                break;
                
            case 'textarea':
                item = {
                    type: 'textarea',
                    name: label || inputLabel(node),
                    disabled: !!$node.attr('disabled'),
                    value: $node.val()
                };
                break;
            
            case 'label':
                break;
            
            default:
                item = {type: 'html', html: $node.clone(true)};
                break;
        }
        
        if (item) {
            counter++;
            items['key' + counter] = item;
        }
    });
    
    return counter;
}

// convert html5 menu
$.contextMenu.fromMenu = function(element) {
    var $this = $(element),
        items = {};
        
    menuChildren(items, $this.children());
    
    return items;
};

// make defaults accessible
$.contextMenu.defaults = defaults;
$.contextMenu.types = types;
// export internal functions - undocumented, for hacking only!
$.contextMenu.handle = handle;
$.contextMenu.op = op;
$.contextMenu.menus = menus;

})(jQuery);



(function (factory) {
    if ( typeof define === 'function' && define.amd ) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS style for Browserify
        module.exports = factory;
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

    var toFix = ['wheel', 'mousewheel', 'DOMMouseScroll', 'MozMousePixelScroll'];
    var toBind = 'onwheel' in document || document.documentMode >= 9 ? ['wheel'] : ['mousewheel', 'DomMouseScroll', 'MozMousePixelScroll'];
    var lowestDelta, lowestDeltaXY;

    if ( $.event.fixHooks ) {
        for ( var i = toFix.length; i; ) {
            $.event.fixHooks[ toFix[--i] ] = $.event.mouseHooks;
        }
    }

    $.event.special.mousewheel = {
        setup: function() {
            if ( this.addEventListener ) {
                for ( var i = toBind.length; i; ) {
                    this.addEventListener( toBind[--i], handler, false );
                }
            } else {
                this.onmousewheel = handler;
            }
        },

        teardown: function() {
            if ( this.removeEventListener ) {
                for ( var i = toBind.length; i; ) {
                    this.removeEventListener( toBind[--i], handler, false );
                }
            } else {
                this.onmousewheel = null;
            }
        }
    };

    $.fn.extend({
        mousewheel: function(fn) {
            return fn ? this.bind("mousewheel", fn) : this.trigger("mousewheel");
        },

        unmousewheel: function(fn) {
            return this.unbind("mousewheel", fn);
        }
    });


    function handler(event) {
        var orgEvent = event || window.event,
            args = [].slice.call(arguments, 1),
            delta = 0,
            deltaX = 0,
            deltaY = 0,
            absDelta = 0,
            absDeltaXY = 0,
            fn;
        event = $.event.fix(orgEvent);
        event.type = "mousewheel";

        // Old school scrollwheel delta
        if ( orgEvent.wheelDelta ) { delta = orgEvent.wheelDelta; }
        if ( orgEvent.detail )     { delta = orgEvent.detail * -1; }

        // New school wheel delta (wheel event)
        if ( orgEvent.deltaY ) {
            deltaY = orgEvent.deltaY * -1;
            delta  = deltaY;
        }
        if ( orgEvent.deltaX ) {
            deltaX = orgEvent.deltaX;
            delta  = deltaX * -1;
        }

        // Webkit
        if ( orgEvent.wheelDeltaY !== undefined ) { deltaY = orgEvent.wheelDeltaY; }
        if ( orgEvent.wheelDeltaX !== undefined ) { deltaX = orgEvent.wheelDeltaX * -1; }

        // Look for lowest delta to normalize the delta values
        absDelta = Math.abs(delta);
        if ( !lowestDelta || absDelta < lowestDelta ) { lowestDelta = absDelta; }
        absDeltaXY = Math.max(Math.abs(deltaY), Math.abs(deltaX));
        if ( !lowestDeltaXY || absDeltaXY < lowestDeltaXY ) { lowestDeltaXY = absDeltaXY; }

        // Get a whole value for the deltas
        fn = delta > 0 ? 'floor' : 'ceil';
        delta  = Math[fn](delta / lowestDelta);
        deltaX = Math[fn](deltaX / lowestDeltaXY);
        deltaY = Math[fn](deltaY / lowestDeltaXY);

        // Add event and delta to the front of the arguments
        args.unshift(event, delta, deltaX, deltaY);

        return ($.event.dispatch || $.event.handle).apply(this, args);
    }

}));





(function(factory) {
    if(typeof define === 'function' && define.amd) {
		define(['jquery'], factory);
	}
	else {
		factory(jQuery);
	}
}
(function($) {
    'use strict';

    var UNSET_OPTION = {},
        getDefaults, createClass, SPFormat, clipval, quartile, normalizeValue, normalizeValues,
        remove, isNumber, all, sum, addCSS, ensureArray, formatNumber, RangeMap,
        MouseHandler, Tooltip, barHighlightMixin,
        line, bar, tristate, discrete, bullet, pie, box, defaultStyles, initStyles,
         VShape, VCanvas_base, VCanvas_canvas, VCanvas_vml, pending, shapeCount = 0;

    
    getDefaults = function () {
        return {
            // Settings common to most/all chart types
            common: {
                type: 'line',
                lineColor: '#000',
                fillColor: false,
                defaultPixelsPerValue: 3,
                width: 'auto',
                height: 'auto',
                composite: false,
                tagValuesAttribute: 'values',
                tagOptionsPrefix: 'spark',
                enableTagOptions: false,
                enableHighlight: true,
                highlightLighten: 1.4,
                tooltipSkipNull: true,
                tooltipPrefix: '',
                tooltipSuffix: '',
                disableHiddenCheck: false,
                numberFormatter: false,
                numberDigitGroupCount: 3,
                numberDigitGroupSep: ',',
                numberDecimalMark: '.',
                disableTooltips: false,
                disableInteraction: false
            },
            // Defaults for line charts
            line: {
                spotColor: '#000',
                highlightSpotColor: '#5f5',
                highlightLineColor: '#f22',
                spotRadius: 2,
                minSpotColor: '#000',
                maxSpotColor: '#000',
                lineWidth: 1,
                normalRangeMin: undefined,
                normalRangeMax: undefined,
                normalRangeColor: '#ccc',
                drawNormalOnTop: false,
                chartRangeMin: undefined,
                chartRangeMax: undefined,
                chartRangeMinX: undefined,
                chartRangeMaxX: undefined,
                tooltipFormat: new SPFormat('<span style="color: {{color}}">&#9679;</span> {{prefix}}{{y}}{{suffix}}')
            },
            // Defaults for bar charts
            bar: {
                barColor: '#3366cc',
                negBarColor: '#f44',
                stackedBarColor: ['#3366cc', '#dc3912', '#ff9900', '#109618', '#66aa00',
                    '#dd4477', '#0099c6', '#990099'],
                zeroColor: undefined,
                nullColor: undefined,
                zeroAxis: true,
                barWidth: 4,
                barSpacing: 1,
                chartRangeMax: undefined,
                chartRangeMin: undefined,
                chartRangeClip: false,
                colorMap: undefined,
                tooltipFormat: new SPFormat('<span style="color: {{color}}">&#9679;</span> {{prefix}}{{value}}{{suffix}}')
            },
            // Defaults for tristate charts
            tristate: {
                barWidth: 4,
                barSpacing: 1,
                posBarColor: '#6f6',
                negBarColor: '#f44',
                zeroBarColor: '#999',
                colorMap: {},
                tooltipFormat: new SPFormat('<span style="color: {{color}}">&#9679;</span> {{value:map}}'),
                tooltipValueLookups: { map: { '-1': 'Loss', '0': 'Draw', '1': 'Win' } }
            },
            // Defaults for discrete charts
            discrete: {
                lineHeight: 'auto',
                thresholdColor: undefined,
                thresholdValue: 0,
                chartRangeMax: undefined,
                chartRangeMin: undefined,
                chartRangeClip: false,
                tooltipFormat: new SPFormat('{{prefix}}{{value}}{{suffix}}')
            },
            // Defaults for bullet charts
            bullet: {
                targetColor: '#f33',
                targetWidth: 3, // width of the target bar in pixels
                performanceColor: '#33f',
                rangeColors: ['#d3dafe', '#a8b6ff', '#7f94ff'],
                base: undefined, // set this to a number to change the base start number
                tooltipFormat: new SPFormat('{{fieldkey:fields}} - {{value}}'),
                tooltipValueLookups: { fields: {r: 'Range', p: 'Performance', t: 'Target'} }
            },
            // Defaults for pie charts
            pie: {
                offset: 0,
                sliceColors: ['#3366cc', '#dc3912', '#ff9900', '#109618', '#66aa00',
                    '#dd4477', '#0099c6', '#990099'],
                borderWidth: 0,
                borderColor: '#000',
                tooltipFormat: new SPFormat('<span style="color: {{color}}">&#9679;</span> {{value}} ({{percent.1}}%)')
            },
            // Defaults for box plots
            box: {
                raw: false,
                boxLineColor: '#000',
                boxFillColor: '#cdf',
                whiskerColor: '#000',
                outlierLineColor: '#333',
                outlierFillColor: '#fff',
                medianColor: '#f00',
                showOutliers: true,
                outlierIQR: 1.5,
                spotRadius: 1.5,
                target: undefined,
                targetColor: '#4a2',
                chartRangeMax: undefined,
                chartRangeMin: undefined,
                tooltipFormat: new SPFormat('{{field:fields}}: {{value}}'),
                tooltipFormatFieldlistKey: 'field',
                tooltipValueLookups: { fields: { lq: 'Lower Quartile', med: 'Median',
                    uq: 'Upper Quartile', lo: 'Left Outlier', ro: 'Right Outlier',
                    lw: 'Left Whisker', rw: 'Right Whisker'} }
            }
        };
    };

    // You can have tooltips use a css class other than jqstooltip by specifying tooltipClassname
    defaultStyles = '.jqstooltip { ' +
            'position: absolute;' +
            'left: 0px;' +
            'top: 0px;' +
            'visibility: hidden;' +
            'background: rgb(0, 0, 0) transparent;' +
            'background-color: rgba(0,0,0,0.6);' +
            'filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);' +
            '-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";' +
            'color: white;' +
            'font: 10px arial, san serif;' +
            'text-align: left;' +
            'white-space: nowrap;' +
            'padding: 5px;' +
            'border: 1px solid white;' +
            'z-index: 10000;' +
            '}' +
            '.jqsfield { ' +
            'color: white;' +
            'font: 10px arial, san serif;' +
            'text-align: left;' +
            '}';

    

    createClass = function () {
        var Class, args;
        Class = function () {
            this.init.apply(this, arguments);
        };
        if (arguments.length > 1) {
            if (arguments[0]) {
                Class.prototype = $.extend(new arguments[0](), arguments[arguments.length - 1]);
                Class._super = arguments[0].prototype;
            } else {
                Class.prototype = arguments[arguments.length - 1];
            }
            if (arguments.length > 2) {
                args = Array.prototype.slice.call(arguments, 1, -1);
                args.unshift(Class.prototype);
                $.extend.apply($, args);
            }
        } else {
            Class.prototype = arguments[0];
        }
        Class.prototype.cls = Class;
        return Class;
    };

    
    $.SPFormatClass = SPFormat = createClass({
        fre: /\{\{([\w.]+?)(:(.+?))?\}\}/g,
        precre: /(\w+)\.(\d+)/,

        init: function (format, fclass) {
            this.format = format;
            this.fclass = fclass;
        },

        render: function (fieldset, lookups, options) {
            var self = this,
                fields = fieldset,
                match, token, lookupkey, fieldvalue, prec;
            return this.format.replace(this.fre, function () {
                var lookup;
                token = arguments[1];
                lookupkey = arguments[3];
                match = self.precre.exec(token);
                if (match) {
                    prec = match[2];
                    token = match[1];
                } else {
                    prec = false;
                }
                fieldvalue = fields[token];
                if (fieldvalue === undefined) {
                    return '';
                }
                if (lookupkey && lookups && lookups[lookupkey]) {
                    lookup = lookups[lookupkey];
                    if (lookup.get) { // RangeMap
                        return lookups[lookupkey].get(fieldvalue) || fieldvalue;
                    } else {
                        return lookups[lookupkey][fieldvalue] || fieldvalue;
                    }
                }
                if (isNumber(fieldvalue)) {
                    if (options.get('numberFormatter')) {
                        fieldvalue = options.get('numberFormatter')(fieldvalue);
                    } else {
                        fieldvalue = formatNumber(fieldvalue, prec,
                            options.get('numberDigitGroupCount'),
                            options.get('numberDigitGroupSep'),
                            options.get('numberDecimalMark'));
                    }
                }
                return fieldvalue;
            });
        }
    });

    // convience method to avoid needing the new operator
    $.spformat = function(format, fclass) {
        return new SPFormat(format, fclass);
    };

    clipval = function (val, min, max) {
        if (val < min) {
            return min;
        }
        if (val > max) {
            return max;
        }
        return val;
    };

    quartile = function (values, q) {
        var vl;
        if (q === 2) {
            vl = Math.floor(values.length / 2);
            return values.length % 2 ? values[vl] : (values[vl-1] + values[vl]) / 2;
        } else {
            if (values.length % 2 ) { // odd
                vl = (values.length * q + q) / 4;
                return vl % 1 ? (values[Math.floor(vl)] + values[Math.floor(vl) - 1]) / 2 : values[vl-1];
            } else { //even
                vl = (values.length * q + 2) / 4;
                return vl % 1 ? (values[Math.floor(vl)] + values[Math.floor(vl) - 1]) / 2 :  values[vl-1];

            }
        }
    };

    normalizeValue = function (val) {
        var nf;
        switch (val) {
            case 'undefined':
                val = undefined;
                break;
            case 'null':
                val = null;
                break;
            case 'true':
                val = true;
                break;
            case 'false':
                val = false;
                break;
            default:
                nf = parseFloat(val);
                if (val == nf) {
                    val = nf;
                }
        }
        return val;
    };

    normalizeValues = function (vals) {
        var i, result = [];
        for (i = vals.length; i--;) {
            result[i] = normalizeValue(vals[i]);
        }
        return result;
    };

    remove = function (vals, filter) {
        var i, vl, result = [];
        for (i = 0, vl = vals.length; i < vl; i++) {
            if (vals[i] !== filter) {
                result.push(vals[i]);
            }
        }
        return result;
    };

    isNumber = function (num) {
        return !isNaN(parseFloat(num)) && isFinite(num);
    };

    formatNumber = function (num, prec, groupsize, groupsep, decsep) {
        var p, i;
        num = (prec === false ? parseFloat(num).toString() : num.toFixed(prec)).split('');
        p = (p = $.inArray('.', num)) < 0 ? num.length : p;
        if (p < num.length) {
            num[p] = decsep;
        }
        for (i = p - groupsize; i > 0; i -= groupsize) {
            num.splice(i, 0, groupsep);
        }
        return num.join('');
    };

    // determine if all values of an array match a value
    // returns true if the array is empty
    all = function (val, arr, ignoreNull) {
        var i;
        for (i = arr.length; i--; ) {
            if (ignoreNull && arr[i] === null) continue;
            if (arr[i] !== val) {
                return false;
            }
        }
        return true;
    };

    // sums the numeric values in an array, ignoring other values
    sum = function (vals) {
        var total = 0, i;
        for (i = vals.length; i--;) {
            total += typeof vals[i] === 'number' ? vals[i] : 0;
        }
        return total;
    };

    ensureArray = function (val) {
        return $.isArray(val) ? val : [val];
    };

    // http://paulirish.com/2008/bookmarklet-inject-new-css-rules/
    addCSS = function(css) {
        var tag;
        //if ('\v' == 'v')  {
        if (document.createStyleSheet) {
            document.createStyleSheet().cssText = css;
        } else {
            tag = document.createElement('style');
            tag.type = 'text/css';
            document.getElementsByTagName('head')[0].appendChild(tag);
            tag[(typeof document.body.style.WebkitAppearance == 'string')  ? 'innerText' : 'innerHTML'] = css;
        }
    };

    // Provide a cross-browser interface to a few simple drawing primitives
    $.fn.simpledraw = function (width, height, useExisting, interact) {
        var target, mhandler;
        if (useExisting && (target = this.data('_jqs_vcanvas'))) {
            return target;
        }
        if (width === undefined) {
            width = $(this).innerWidth();
        }
        if (height === undefined) {
            height = $(this).innerHeight();
        }
        if ($.browser.hasCanvas) {
            target = new VCanvas_canvas(width, height, this, interact);
        } else if ($.browser.msie) {
            target = new VCanvas_vml(width, height, this);
        } else {
            return false;
        }
        mhandler = $(this).data('_jqs_mhandler');
        if (mhandler) {
            mhandler.registerCanvas(target);
        }
        return target;
    };

    $.fn.cleardraw = function () {
        var target = this.data('_jqs_vcanvas');
        if (target) {
            target.reset();
        }
    };

    $.RangeMapClass = RangeMap = createClass({
        init: function (map) {
            var key, range, rangelist = [];
            for (key in map) {
                if (map.hasOwnProperty(key) && typeof key === 'string' && key.indexOf(':') > -1) {
                    range = key.split(':');
                    range[0] = range[0].length === 0 ? -Infinity : parseFloat(range[0]);
                    range[1] = range[1].length === 0 ? Infinity : parseFloat(range[1]);
                    range[2] = map[key];
                    rangelist.push(range);
                }
            }
            this.map = map;
            this.rangelist = rangelist || false;
        },

        get: function (value) {
            var rangelist = this.rangelist,
                i, range, result;
            if ((result = this.map[value]) !== undefined) {
                return result;
            }
            if (rangelist) {
                for (i = rangelist.length; i--;) {
                    range = rangelist[i];
                    if (range[0] <= value && range[1] >= value) {
                        return range[2];
                    }
                }
            }
            return undefined;
        }
    });

    // Convenience function
    $.range_map = function(map) {
        return new RangeMap(map);
    };

    MouseHandler = createClass({
        init: function (el, options) {
            var $el = $(el);
            this.$el = $el;
            this.options = options;
            this.currentPageX = 0;
            this.currentPageY = 0;
            this.el = el;
            this.splist = [];
            this.tooltip = null;
            this.over = false;
            this.displayTooltips = !options.get('disableTooltips');
            this.highlightEnabled = !options.get('disableHighlight');
        },

        registerSparkline: function (sp) {
            this.splist.push(sp);
            if (this.over) {
                this.updateDisplay();
            }
        },

        registerCanvas: function (canvas) {
            var $canvas = $(canvas.canvas);
            this.canvas = canvas;
            this.$canvas = $canvas;
            $canvas.mouseenter($.proxy(this.mouseenter, this));
            $canvas.mouseleave($.proxy(this.mouseleave, this));
            $canvas.click($.proxy(this.mouseclick, this));
        },

        reset: function (removeTooltip) {
            this.splist = [];
            if (this.tooltip && removeTooltip) {
                this.tooltip.remove();
                this.tooltip = undefined;
            }
        },

        mouseclick: function (e) {
            var clickEvent = $.Event('sparklineClick');
            clickEvent.originalEvent = e;
            clickEvent.sparklines = this.splist;
            this.$el.trigger(clickEvent);
        },

        mouseenter: function (e) {
            $(document.body).unbind('mousemove.jqs');
            $(document.body).bind('mousemove.jqs', $.proxy(this.mousemove, this));
            this.over = true;
            this.currentPageX = e.pageX;
            this.currentPageY = e.pageY;
            this.currentEl = e.target;
            if (!this.tooltip && this.displayTooltips) {
                this.tooltip = new Tooltip(this.options);
                this.tooltip.updatePosition(e.pageX, e.pageY);
            }
            this.updateDisplay();
        },

        mouseleave: function () {
            $(document.body).unbind('mousemove.jqs');
            var splist = this.splist,
                 spcount = splist.length,
                 needsRefresh = false,
                 sp, i;
            this.over = false;
            this.currentEl = null;

            if (this.tooltip) {
                this.tooltip.remove();
                this.tooltip = null;
            }

            for (i = 0; i < spcount; i++) {
                sp = splist[i];
                if (sp.clearRegionHighlight()) {
                    needsRefresh = true;
                }
            }

            if (needsRefresh) {
                this.canvas.render();
            }
        },

        mousemove: function (e) {
            this.currentPageX = e.pageX;
            this.currentPageY = e.pageY;
            this.currentEl = e.target;
            if (this.tooltip) {
                this.tooltip.updatePosition(e.pageX, e.pageY);
            }
            this.updateDisplay();
        },

        updateDisplay: function () {
            var splist = this.splist,
                 spcount = splist.length,
                 needsRefresh = false,
                 offset = this.$canvas.offset(),
                 localX = this.currentPageX - offset.left,
                 localY = this.currentPageY - offset.top,
                 tooltiphtml, sp, i, result, changeEvent;
            if (!this.over) {
                return;
            }
            for (i = 0; i < spcount; i++) {
                sp = splist[i];
                result = sp.setRegionHighlight(this.currentEl, localX, localY);
                if (result) {
                    needsRefresh = true;
                }
            }
            if (needsRefresh) {
                changeEvent = $.Event('sparklineRegionChange');
                changeEvent.sparklines = this.splist;
                this.$el.trigger(changeEvent);
                if (this.tooltip) {
                    tooltiphtml = '';
                    for (i = 0; i < spcount; i++) {
                        sp = splist[i];
                        tooltiphtml += sp.getCurrentRegionTooltip();
                    }
                    this.tooltip.setContent(tooltiphtml);
                }
                if (!this.disableHighlight) {
                    this.canvas.render();
                }
            }
            if (result === null) {
                this.mouseleave();
            }
        }
    });


    Tooltip = createClass({
        sizeStyle: 'position: static !important;' +
            'display: block !important;' +
            'visibility: hidden !important;' +
            'float: left !important;',

        init: function (options) {
            var tooltipClassname = options.get('tooltipClassname', 'jqstooltip'),
                sizetipStyle = this.sizeStyle,
                offset;
            this.container = options.get('tooltipContainer') || document.body;
            this.tooltipOffsetX = options.get('tooltipOffsetX', 10);
            this.tooltipOffsetY = options.get('tooltipOffsetY', 12);
            // remove any previous lingering tooltip
            $('#jqssizetip').remove();
            $('#jqstooltip').remove();
            this.sizetip = $('<div/>', {
                id: 'jqssizetip',
                style: sizetipStyle,
                'class': tooltipClassname
            });
            this.tooltip = $('<div/>', {
                id: 'jqstooltip',
                'class': tooltipClassname
            }).appendTo(this.container);
            // account for the container's location
            offset = this.tooltip.offset();
            this.offsetLeft = offset.left;
            this.offsetTop = offset.top;
            this.hidden = true;
            $(window).unbind('resize.jqs scroll.jqs');
            $(window).bind('resize.jqs scroll.jqs', $.proxy(this.updateWindowDims, this));
            this.updateWindowDims();
        },

        updateWindowDims: function () {
            this.scrollTop = $(window).scrollTop();
            this.scrollLeft = $(window).scrollLeft();
            this.scrollRight = this.scrollLeft + $(window).width();
            this.updatePosition();
        },

        getSize: function (content) {
            this.sizetip.html(content).appendTo(this.container);
            this.width = this.sizetip.width() + 1;
            this.height = this.sizetip.height();
            this.sizetip.remove();
        },

        setContent: function (content) {
            if (!content) {
                this.tooltip.css('visibility', 'hidden');
                this.hidden = true;
                return;
            }
            this.getSize(content);
            this.tooltip.html(content)
                .css({
                    'width': this.width,
                    'height': this.height,
                    'visibility': 'visible'
                });
            if (this.hidden) {
                this.hidden = false;
                this.updatePosition();
            }
        },

        updatePosition: function (x, y) {
            if (x === undefined) {
                if (this.mousex === undefined) {
                    return;
                }
                x = this.mousex - this.offsetLeft;
                y = this.mousey - this.offsetTop;

            } else {
                this.mousex = x = x - this.offsetLeft;
                this.mousey = y = y - this.offsetTop;
            }
            if (!this.height || !this.width || this.hidden) {
                return;
            }

            y -= this.height + this.tooltipOffsetY;
            x += this.tooltipOffsetX;

            if (y < this.scrollTop) {
                y = this.scrollTop;
            }
            if (x < this.scrollLeft) {
                x = this.scrollLeft;
            } else if (x + this.width > this.scrollRight) {
                x = this.scrollRight - this.width;
            }

            this.tooltip.css({
                'left': x,
                'top': y
            });
        },

        remove: function () {
            this.tooltip.remove();
            this.sizetip.remove();
            this.sizetip = this.tooltip = undefined;
            $(window).unbind('resize.jqs scroll.jqs');
        }
    });

    initStyles = function() {
        addCSS(defaultStyles);
    };

    $(initStyles);

    pending = [];
    $.fn.sparkline = function (userValues, userOptions) {
        return this.each(function () {
            var options = new $.fn.sparkline.options(this, userOptions),
                 $this = $(this),
                 render, i;
            render = function () {
                var values, width, height, tmp, mhandler, sp, vals;
                if (userValues === 'html' || userValues === undefined) {
                    vals = this.getAttribute(options.get('tagValuesAttribute'));
                    if (vals === undefined || vals === null) {
                        vals = $this.html();
                    }
                    values = vals.replace(/(^\s*<!--)|(-->\s*$)|\s+/g, '').split(',');
                } else {
                    values = userValues;
                }

                width = options.get('width') === 'auto' ? values.length * options.get('defaultPixelsPerValue') : options.get('width');
                if (options.get('height') === 'auto') {
                    if (!options.get('composite') || !$.data(this, '_jqs_vcanvas')) {
                        // must be a better way to get the line height
                        tmp = document.createElement('span');
                        tmp.innerHTML = 'a';
                        $this.html(tmp);
                        height = $(tmp).innerHeight() || $(tmp).height();
                        $(tmp).remove();
                        tmp = null;
                    }
                } else {
                    height = options.get('height');
                }

                if (!options.get('disableInteraction')) {
                    mhandler = $.data(this, '_jqs_mhandler');
                    if (!mhandler) {
                        mhandler = new MouseHandler(this, options);
                        $.data(this, '_jqs_mhandler', mhandler);
                    } else if (!options.get('composite')) {
                        mhandler.reset();
                    }
                } else {
                    mhandler = false;
                }

                if (options.get('composite') && !$.data(this, '_jqs_vcanvas')) {
                    if (!$.data(this, '_jqs_errnotify')) {
                        alert('Attempted to attach a composite sparkline to an element with no existing sparkline');
                        $.data(this, '_jqs_errnotify', true);
                    }
                    return;
                }

                sp = new $.fn.sparkline[options.get('type')](this, values, options, width, height);

                sp.render();

                if (mhandler) {
                    mhandler.registerSparkline(sp);
                }
            };
            // jQuery 1.3.0 completely changed the meaning of :hidden :-/
            if (($(this).html() && !options.get('disableHiddenCheck') && $(this).is(':hidden')) || ($.fn.jquery < '1.3.0' && $(this).parents().is(':hidden')) || !$(this).parents('body').length) {
                if (!options.get('composite') && $.data(this, '_jqs_pending')) {
                    // remove any existing references to the element
                    for (i = pending.length; i; i--) {
                        if (pending[i - 1][0] == this) {
                            pending.splice(i - 1, 1);
                        }
                    }
                }
                pending.push([this, render]);
                $.data(this, '_jqs_pending', true);
            } else {
                render.call(this);
            }
        });
    };

    $.fn.sparkline.defaults = getDefaults();


    $.sparkline_display_visible = function () {
        var el, i, pl;
        var done = [];
        for (i = 0, pl = pending.length; i < pl; i++) {
            el = pending[i][0];
            if ($(el).is(':visible') && !$(el).parents().is(':hidden')) {
                pending[i][1].call(el);
                $.data(pending[i][0], '_jqs_pending', false);
                done.push(i);
            } else if (!$(el).closest('html').length && !$.data(el, '_jqs_pending')) {
                // element has been inserted and removed from the DOM
                // If it was not yet inserted into the dom then the .data request
                // will return true.
                // removing from the dom causes the data to be removed.
                $.data(pending[i][0], '_jqs_pending', false);
                done.push(i);
            }
        }
        for (i = done.length; i; i--) {
            pending.splice(done[i - 1], 1);
        }
    };


    
    $.fn.sparkline.options = createClass({
        init: function (tag, userOptions) {
            var extendedOptions, defaults, base, tagOptionType;
            this.userOptions = userOptions = userOptions || {};
            this.tag = tag;
            this.tagValCache = {};
            defaults = $.fn.sparkline.defaults;
            base = defaults.common;
            this.tagOptionsPrefix = userOptions.enableTagOptions && (userOptions.tagOptionsPrefix || base.tagOptionsPrefix);

            tagOptionType = this.getTagSetting('type');
            if (tagOptionType === UNSET_OPTION) {
                extendedOptions = defaults[userOptions.type || base.type];
            } else {
                extendedOptions = defaults[tagOptionType];
            }
            this.mergedOptions = $.extend({}, base, extendedOptions, userOptions);
        },


        getTagSetting: function (key) {
            var prefix = this.tagOptionsPrefix,
                val, i, pairs, keyval;
            if (prefix === false || prefix === undefined) {
                return UNSET_OPTION;
            }
            if (this.tagValCache.hasOwnProperty(key)) {
                val = this.tagValCache.key;
            } else {
                val = this.tag.getAttribute(prefix + key);
                if (val === undefined || val === null) {
                    val = UNSET_OPTION;
                } else if (val.substr(0, 1) === '[') {
                    val = val.substr(1, val.length - 2).split(',');
                    for (i = val.length; i--;) {
                        val[i] = normalizeValue(val[i].replace(/(^\s*)|(\s*$)/g, ''));
                    }
                } else if (val.substr(0, 1) === '{') {
                    pairs = val.substr(1, val.length - 2).split(',');
                    val = {};
                    for (i = pairs.length; i--;) {
                        keyval = pairs[i].split(':', 2);
                        val[keyval[0].replace(/(^\s*)|(\s*$)/g, '')] = normalizeValue(keyval[1].replace(/(^\s*)|(\s*$)/g, ''));
                    }
                } else {
                    val = normalizeValue(val);
                }
                this.tagValCache.key = val;
            }
            return val;
        },

        get: function (key, defaultval) {
            var tagOption = this.getTagSetting(key),
                result;
            if (tagOption !== UNSET_OPTION) {
                return tagOption;
            }
            return (result = this.mergedOptions[key]) === undefined ? defaultval : result;
        }
    });


    $.fn.sparkline._base = createClass({
        disabled: false,

        init: function (el, values, options, width, height) {
            this.el = el;
            this.$el = $(el);
            this.values = values;
            this.options = options;
            this.width = width;
            this.height = height;
            this.currentRegion = undefined;
        },

        
        initTarget: function () {
            var interactive = !this.options.get('disableInteraction');
            if (!(this.target = this.$el.simpledraw(this.width, this.height, this.options.get('composite'), interactive))) {
                this.disabled = true;
            } else {
                this.canvasWidth = this.target.pixelWidth;
                this.canvasHeight = this.target.pixelHeight;
            }
        },

        
        render: function () {
            if (this.disabled) {
                this.el.innerHTML = '';
                return false;
            }
            return true;
        },

        
        getRegion: function (x, y) {
        },

        
        setRegionHighlight: function (el, x, y) {
            var currentRegion = this.currentRegion,
                highlightEnabled = !this.options.get('disableHighlight'),
                newRegion;
            if (x > this.canvasWidth || y > this.canvasHeight || x < 0 || y < 0) {
                return null;
            }
            newRegion = this.getRegion(el, x, y);
            if (currentRegion !== newRegion) {
                if (currentRegion !== undefined && highlightEnabled) {
                    this.removeHighlight();
                }
                this.currentRegion = newRegion;
                if (newRegion !== undefined && highlightEnabled) {
                    this.renderHighlight();
                }
                return true;
            }
            return false;
        },

        
        clearRegionHighlight: function () {
            if (this.currentRegion !== undefined) {
                this.removeHighlight();
                this.currentRegion = undefined;
                return true;
            }
            return false;
        },

        renderHighlight: function () {
            this.changeHighlight(true);
        },

        removeHighlight: function () {
            this.changeHighlight(false);
        },

        changeHighlight: function (highlight)  {},

        
        getCurrentRegionTooltip: function () {
            var options = this.options,
                header = '',
                entries = [],
                fields, formats, formatlen, fclass, text, i,
                showFields, showFieldsKey, newFields, fv,
                formatter, format, fieldlen, j;
            if (this.currentRegion === undefined) {
                return '';
            }
            fields = this.getCurrentRegionFields();
            formatter = options.get('tooltipFormatter');
            if (formatter) {
                return formatter(this, options, fields);
            }
            if (options.get('tooltipChartTitle')) {
                header += '<div class="jqs jqstitle">' + options.get('tooltipChartTitle') + '</div>\n';
            }
            formats = this.options.get('tooltipFormat');
            if (!formats) {
                return '';
            }
            if (!$.isArray(formats)) {
                formats = [formats];
            }
            if (!$.isArray(fields)) {
                fields = [fields];
            }
            showFields = this.options.get('tooltipFormatFieldlist');
            showFieldsKey = this.options.get('tooltipFormatFieldlistKey');
            if (showFields && showFieldsKey) {
                // user-selected ordering of fields
                newFields = [];
                for (i = fields.length; i--;) {
                    fv = fields[i][showFieldsKey];
                    if ((j = $.inArray(fv, showFields)) != -1) {
                        newFields[j] = fields[i];
                    }
                }
                fields = newFields;
            }
            formatlen = formats.length;
            fieldlen = fields.length;
            for (i = 0; i < formatlen; i++) {
                format = formats[i];
                if (typeof format === 'string') {
                    format = new SPFormat(format);
                }
                fclass = format.fclass || 'jqsfield';
                for (j = 0; j < fieldlen; j++) {
                    if (!fields[j].isNull || !options.get('tooltipSkipNull')) {
                        $.extend(fields[j], {
                            prefix: options.get('tooltipPrefix'),
                            suffix: options.get('tooltipSuffix')
                        });
                        text = format.render(fields[j], options.get('tooltipValueLookups'), options);
                        entries.push('<div class="' + fclass + '">' + text + '</div>');
                    }
                }
            }
            if (entries.length) {
                return header + entries.join('\n');
            }
            return '';
        },

        getCurrentRegionFields: function () {},

        calcHighlightColor: function (color, options) {
            var highlightColor = options.get('highlightColor'),
                lighten = options.get('highlightLighten'),
                parse, mult, rgbnew, i;
            if (highlightColor) {
                return highlightColor;
            }
            if (lighten) {
                // extract RGB values
                parse = /^#([0-9a-f])([0-9a-f])([0-9a-f])$/i.exec(color) || /^#([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i.exec(color);
                if (parse) {
                    rgbnew = [];
                    mult = color.length === 4 ? 16 : 1;
                    for (i = 0; i < 3; i++) {
                        rgbnew[i] = clipval(Math.round(parseInt(parse[i + 1], 16) * mult * lighten), 0, 255);
                    }
                    return 'rgb(' + rgbnew.join(',') + ')';
                }

            }
            return color;
        }

    });

    barHighlightMixin = {
        changeHighlight: function (highlight) {
            var currentRegion = this.currentRegion,
                target = this.target,
                shapeids = this.regionShapes[currentRegion],
                newShapes;
            // will be null if the region value was null
            if (shapeids) {
                newShapes = this.renderRegion(currentRegion, highlight);
                if ($.isArray(newShapes) || $.isArray(shapeids)) {
                    target.replaceWithShapes(shapeids, newShapes);
                    this.regionShapes[currentRegion] = $.map(newShapes, function (newShape) {
                        return newShape.id;
                    });
                } else {
                    target.replaceWithShape(shapeids, newShapes);
                    this.regionShapes[currentRegion] = newShapes.id;
                }
            }
        },

        render: function () {
            var values = this.values,
                target = this.target,
                regionShapes = this.regionShapes,
                shapes, ids, i, j;

            if (!this.cls._super.render.call(this)) {
                return;
            }
            for (i = values.length; i--;) {
                shapes = this.renderRegion(i);
                if (shapes) {
                    if ($.isArray(shapes)) {
                        ids = [];
                        for (j = shapes.length; j--;) {
                            shapes[j].append();
                            ids.push(shapes[j].id);
                        }
                        regionShapes[i] = ids;
                    } else {
                        shapes.append();
                        regionShapes[i] = shapes.id; // store just the shapeid
                    }
                } else {
                    // null value
                    regionShapes[i] = null;
                }
            }
            target.render();
        }
    };

    
    $.fn.sparkline.line = line = createClass($.fn.sparkline._base, {
        type: 'line',

        init: function (el, values, options, width, height) {
            line._super.init.call(this, el, values, options, width, height);
            this.vertices = [];
            this.regionMap = [];
            this.xvalues = [];
            this.yvalues = [];
            this.yminmax = [];
            this.hightlightSpotId = null;
            this.lastShapeId = null;
            this.initTarget();
        },

        getRegion: function (el, x, y) {
            var i,
                regionMap = this.regionMap; // maps regions to value positions
            for (i = regionMap.length; i--;) {
                if (regionMap[i] !== null && x >= regionMap[i][0] && x <= regionMap[i][1]) {
                    return regionMap[i][2];
                }
            }
            return undefined;
        },

        getCurrentRegionFields: function () {
            var currentRegion = this.currentRegion;
            return {
                isNull: this.yvalues[currentRegion] === null,
                x: this.xvalues[currentRegion],
                y: this.yvalues[currentRegion],
                color: this.options.get('lineColor'),
                fillColor: this.options.get('fillColor'),
                offset: currentRegion
            };
        },

        renderHighlight: function () {
            var currentRegion = this.currentRegion,
                target = this.target,
                vertex = this.vertices[currentRegion],
                options = this.options,
                spotRadius = options.get('spotRadius'),
                highlightSpotColor = options.get('highlightSpotColor'),
                highlightLineColor = options.get('highlightLineColor'),
                highlightSpot, highlightLine;

            if (!vertex) {
                return;
            }
            if (spotRadius && highlightSpotColor) {
                highlightSpot = target.drawCircle(vertex[0], vertex[1],
                    spotRadius, undefined, highlightSpotColor);
                this.highlightSpotId = highlightSpot.id;
                target.insertAfterShape(this.lastShapeId, highlightSpot);
            }
            if (highlightLineColor) {
                highlightLine = target.drawLine(vertex[0], this.canvasTop, vertex[0],
                    this.canvasTop + this.canvasHeight, highlightLineColor);
                this.highlightLineId = highlightLine.id;
                target.insertAfterShape(this.lastShapeId, highlightLine);
            }
        },

        removeHighlight: function () {
            var target = this.target;
            if (this.highlightSpotId) {
                target.removeShapeId(this.highlightSpotId);
                this.highlightSpotId = null;
            }
            if (this.highlightLineId) {
                target.removeShapeId(this.highlightLineId);
                this.highlightLineId = null;
            }
        },

        scanValues: function () {
            var values = this.values,
                valcount = values.length,
                xvalues = this.xvalues,
                yvalues = this.yvalues,
                yminmax = this.yminmax,
                i, val, isStr, isArray, sp;
            for (i = 0; i < valcount; i++) {
                val = values[i];
                isStr = typeof(values[i]) === 'string';
                isArray = typeof(values[i]) === 'object' && values[i] instanceof Array;
                sp = isStr && values[i].split(':');
                if (isStr && sp.length === 2) { // x:y
                    xvalues.push(Number(sp[0]));
                    yvalues.push(Number(sp[1]));
                    yminmax.push(Number(sp[1]));
                } else if (isArray) {
                    xvalues.push(val[0]);
                    yvalues.push(val[1]);
                    yminmax.push(val[1]);
                } else {
                    xvalues.push(i);
                    if (values[i] === null || values[i] === 'null') {
                        yvalues.push(null);
                    } else {
                        yvalues.push(Number(val));
                        yminmax.push(Number(val));
                    }
                }
            }
            if (this.options.get('xvalues')) {
                xvalues = this.options.get('xvalues');
            }

            this.maxy = this.maxyorg = Math.max.apply(Math, yminmax);
            this.miny = this.minyorg = Math.min.apply(Math, yminmax);

            this.maxx = Math.max.apply(Math, xvalues);
            this.minx = Math.min.apply(Math, xvalues);

            this.xvalues = xvalues;
            this.yvalues = yvalues;
            this.yminmax = yminmax;

        },

        processRangeOptions: function () {
            var options = this.options,
                normalRangeMin = options.get('normalRangeMin'),
                normalRangeMax = options.get('normalRangeMax');

            if (normalRangeMin !== undefined) {
                if (normalRangeMin < this.miny) {
                    this.miny = normalRangeMin;
                }
                if (normalRangeMax > this.maxy) {
                    this.maxy = normalRangeMax;
                }
            }
            if (options.get('chartRangeMin') !== undefined && (options.get('chartRangeClip') || options.get('chartRangeMin') < this.miny)) {
                this.miny = options.get('chartRangeMin');
            }
            if (options.get('chartRangeMax') !== undefined && (options.get('chartRangeClip') || options.get('chartRangeMax') > this.maxy)) {
                this.maxy = options.get('chartRangeMax');
            }
            if (options.get('chartRangeMinX') !== undefined && (options.get('chartRangeClipX') || options.get('chartRangeMinX') < this.minx)) {
                this.minx = options.get('chartRangeMinX');
            }
            if (options.get('chartRangeMaxX') !== undefined && (options.get('chartRangeClipX') || options.get('chartRangeMaxX') > this.maxx)) {
                this.maxx = options.get('chartRangeMaxX');
            }

        },

        drawNormalRange: function (canvasLeft, canvasTop, canvasHeight, canvasWidth, rangey) {
            var normalRangeMin = this.options.get('normalRangeMin'),
                normalRangeMax = this.options.get('normalRangeMax'),
                ytop = canvasTop + Math.round(canvasHeight - (canvasHeight * ((normalRangeMax - this.miny) / rangey))),
                height = Math.round((canvasHeight * (normalRangeMax - normalRangeMin)) / rangey);
			height = Math.max(height, 1);
			if (height == 1) {
	            this.target.drawLine(canvasLeft, ytop, canvasWidth, ytop, this.options.get('normalRangeColor'), 1).append();
			}
			else {
	            this.target.drawRect(canvasLeft, ytop, canvasWidth, height, undefined, this.options.get('normalRangeColor')).append();
			}
        },

        render: function () {
            var options = this.options,
                target = this.target,
                canvasWidth = this.canvasWidth,
                canvasHeight = this.canvasHeight,
                vertices = this.vertices,
                spotRadius = options.get('spotRadius'),
                regionMap = this.regionMap,
                rangex, rangey, yvallast,
                canvasTop, canvasLeft,
                vertex, path, paths, x, y, xnext, xpos, xposnext,
                last, next, yvalcount, lineShapes, fillShapes, plen,
                valueSpots, hlSpotsEnabled, color, xvalues, yvalues, i;

            if (!line._super.render.call(this)) {
                return;
            }

            this.scanValues();
            this.processRangeOptions();

            xvalues = this.xvalues;
            yvalues = this.yvalues;

            if (!this.yminmax.length || this.yvalues.length < 2) {
                // empty or all null valuess
                return;
            }

            canvasTop = canvasLeft = 0;

            rangex = this.maxx - this.minx === 0 ? 1 : this.maxx - this.minx;
            rangey = this.maxy - this.miny === 0 ? 1 : this.maxy - this.miny;
            yvallast = this.yvalues.length - 1;

            if (spotRadius && (canvasWidth < (spotRadius * 4) || canvasHeight < (spotRadius * 4))) {
                spotRadius = 0;
            }
            if (spotRadius) {
                // adjust the canvas size as required so that spots will fit
                hlSpotsEnabled = options.get('highlightSpotColor') &&  !options.get('disableInteraction');
                if (hlSpotsEnabled || options.get('minSpotColor') || (options.get('spotColor') && yvalues[yvallast] === this.miny)) {
                    canvasHeight -= Math.ceil(spotRadius);
                }
                if (hlSpotsEnabled || options.get('maxSpotColor') || (options.get('spotColor') && yvalues[yvallast] === this.maxy)) {
                    canvasHeight -= Math.ceil(spotRadius);
                    canvasTop += Math.ceil(spotRadius);
                }
                if (hlSpotsEnabled ||
                     ((options.get('minSpotColor') || options.get('maxSpotColor')) && (yvalues[0] === this.miny || yvalues[0] === this.maxy))) {
                    canvasLeft += Math.ceil(spotRadius);
                    canvasWidth -= Math.ceil(spotRadius);
                }
                if (hlSpotsEnabled || options.get('spotColor') ||
                    (options.get('minSpotColor') || options.get('maxSpotColor') &&
                        (yvalues[yvallast] === this.miny || yvalues[yvallast] === this.maxy))) {
                    canvasWidth -= Math.ceil(spotRadius);
                }
            }


            canvasHeight--;

            if (options.get('normalRangeMin') !== undefined && !options.get('drawNormalOnTop')) {
                this.drawNormalRange(canvasLeft, canvasTop, canvasHeight, canvasWidth, rangey);
            }

            path = [];
            paths = [path];
            last = next = null;
            yvalcount = yvalues.length;
            for (i = 0; i < yvalcount; i++) {
                x = xvalues[i];
                xnext = xvalues[i + 1];
                y = yvalues[i];
                xpos = canvasLeft + Math.round((x - this.minx) * (canvasWidth / rangex));
                xposnext = i < yvalcount - 1 ? canvasLeft + Math.round((xnext - this.minx) * (canvasWidth / rangex)) : canvasWidth;
                next = xpos + ((xposnext - xpos) / 2);
                regionMap[i] = [last || 0, next, i];
                last = next;
                if (y === null) {
                    if (i) {
                        if (yvalues[i - 1] !== null) {
                            path = [];
                            paths.push(path);
                        }
                        vertices.push(null);
                    }
                } else {
                    if (y < this.miny) {
                        y = this.miny;
                    }
                    if (y > this.maxy) {
                        y = this.maxy;
                    }
                    if (!path.length) {
                        // previous value was null
                        path.push([xpos, canvasTop + canvasHeight]);
                    }
                    vertex = [xpos, canvasTop + Math.round(canvasHeight - (canvasHeight * ((y - this.miny) / rangey)))];
                    path.push(vertex);
                    vertices.push(vertex);
                }
            }

            lineShapes = [];
            fillShapes = [];
            plen = paths.length;
            for (i = 0; i < plen; i++) {
                path = paths[i];
                if (path.length) {
                    if (options.get('fillColor')) {
                        path.push([path[path.length - 1][0], (canvasTop + canvasHeight)]);
                        fillShapes.push(path.slice(0));
                        path.pop();
                    }
                    // if there's only a single point in this path, then we want to display it
                    // as a vertical line which means we keep path[0]  as is
                    if (path.length > 2) {
                        // else we want the first value
                        path[0] = [path[0][0], path[1][1]];
                    }
                    lineShapes.push(path);
                }
            }

            // draw the fill first, then optionally the normal range, then the line on top of that
            plen = fillShapes.length;
            for (i = 0; i < plen; i++) {
                target.drawShape(fillShapes[i],
                    options.get('fillColor'), options.get('fillColor')).append();
            }

            if (options.get('normalRangeMin') !== undefined && options.get('drawNormalOnTop')) {
                this.drawNormalRange(canvasLeft, canvasTop, canvasHeight, canvasWidth, rangey);
            }

            plen = lineShapes.length;
            for (i = 0; i < plen; i++) {
                target.drawShape(lineShapes[i], options.get('lineColor'), undefined,
                    options.get('lineWidth')).append();
            }

            if (spotRadius && options.get('valueSpots')) {
                valueSpots = options.get('valueSpots');
                if (valueSpots.get === undefined) {
                    valueSpots = new RangeMap(valueSpots);
                }
                for (i = 0; i < yvalcount; i++) {
                    color = valueSpots.get(yvalues[i]);
                    if (color) {
                        target.drawCircle(canvasLeft + Math.round((xvalues[i] - this.minx) * (canvasWidth / rangex)),
                            canvasTop + Math.round(canvasHeight - (canvasHeight * ((yvalues[i] - this.miny) / rangey))),
                            spotRadius, undefined,
                            color).append();
                    }
                }

            }
            if (spotRadius && options.get('spotColor')) {
                target.drawCircle(canvasLeft + Math.round((xvalues[xvalues.length - 1] - this.minx) * (canvasWidth / rangex)),
                    canvasTop + Math.round(canvasHeight - (canvasHeight * ((yvalues[yvallast] - this.miny) / rangey))),
                    spotRadius, undefined,
                    options.get('spotColor')).append();
            }
            if (this.maxy !== this.minyorg) {
                if (spotRadius && options.get('minSpotColor')) {
                    x = xvalues[$.inArray(this.minyorg, yvalues)];
                    target.drawCircle(canvasLeft + Math.round((x - this.minx) * (canvasWidth / rangex)),
                        canvasTop + Math.round(canvasHeight - (canvasHeight * ((this.minyorg - this.miny) / rangey))),
                        spotRadius, undefined,
                        options.get('minSpotColor')).append();
                }
                if (spotRadius && options.get('maxSpotColor')) {
                    x = xvalues[$.inArray(this.maxyorg, yvalues)];
                    target.drawCircle(canvasLeft + Math.round((x - this.minx) * (canvasWidth / rangex)),
                        canvasTop + Math.round(canvasHeight - (canvasHeight * ((this.maxyorg - this.miny) / rangey))),
                        spotRadius, undefined,
                        options.get('maxSpotColor')).append();
                }
            }

            this.lastShapeId = target.getLastShapeId();
            this.canvasTop = canvasTop;
            target.render();
        }
    });

    
    $.fn.sparkline.bar = bar = createClass($.fn.sparkline._base, barHighlightMixin, {
        type: 'bar',

        init: function (el, values, options, width, height) {
            var barWidth = parseInt(options.get('barWidth'), 10),
                barSpacing = parseInt(options.get('barSpacing'), 10),
                chartRangeMin = options.get('chartRangeMin'),
                chartRangeMax = options.get('chartRangeMax'),
                chartRangeClip = options.get('chartRangeClip'),
                stackMin = Infinity,
                stackMax = -Infinity,
                isStackString, groupMin, groupMax, stackRanges,
                numValues, i, vlen, range, zeroAxis, xaxisOffset, min, max, clipMin, clipMax,
                stacked, vlist, j, slen, svals, val, yoffset, yMaxCalc, canvasHeightEf;
            bar._super.init.call(this, el, values, options, width, height);

            // scan values to determine whether to stack bars
            for (i = 0, vlen = values.length; i < vlen; i++) {
                val = values[i];
                isStackString = typeof(val) === 'string' && val.indexOf(':') > -1;
                if (isStackString || $.isArray(val)) {
                    stacked = true;
                    if (isStackString) {
                        val = values[i] = normalizeValues(val.split(':'));
                    }
                    val = remove(val, null); // min/max will treat null as zero
                    groupMin = Math.min.apply(Math, val);
                    groupMax = Math.max.apply(Math, val);
                    if (groupMin < stackMin) {
                        stackMin = groupMin;
                    }
                    if (groupMax > stackMax) {
                        stackMax = groupMax;
                    }
                }
            }

            this.stacked = stacked;
            this.regionShapes = {};
            this.barWidth = barWidth;
            this.barSpacing = barSpacing;
            this.totalBarWidth = barWidth + barSpacing;
            this.width = width = (values.length * barWidth) + ((values.length - 1) * barSpacing);

            this.initTarget();

            if (chartRangeClip) {
                clipMin = chartRangeMin === undefined ? -Infinity : chartRangeMin;
                clipMax = chartRangeMax === undefined ? Infinity : chartRangeMax;
            }

            numValues = [];
            stackRanges = stacked ? [] : numValues;
            var stackTotals = [];
            var stackRangesNeg = [];
            for (i = 0, vlen = values.length; i < vlen; i++) {
                if (stacked) {
                    vlist = values[i];
                    values[i] = svals = [];
                    stackTotals[i] = 0;
                    stackRanges[i] = stackRangesNeg[i] = 0;
                    for (j = 0, slen = vlist.length; j < slen; j++) {
                        val = svals[j] = chartRangeClip ? clipval(vlist[j], clipMin, clipMax) : vlist[j];
                        if (val !== null) {
                            if (val > 0) {
                                stackTotals[i] += val;
                            }
                            if (stackMin < 0 && stackMax > 0) {
                                if (val < 0) {
                                    stackRangesNeg[i] += Math.abs(val);
                                } else {
                                    stackRanges[i] += val;
                                }
                            } else {
                                stackRanges[i] += Math.abs(val - (val < 0 ? stackMax : stackMin));
                            }
                            numValues.push(val);
                        }
                    }
                } else {
                    val = chartRangeClip ? clipval(values[i], clipMin, clipMax) : values[i];
                    val = values[i] = normalizeValue(val);
                    if (val !== null) {
                        numValues.push(val);
                    }
                }
            }
            this.max = max = Math.max.apply(Math, numValues);
            this.min = min = Math.min.apply(Math, numValues);
            this.stackMax = stackMax = stacked ? Math.max.apply(Math, stackTotals) : max;
            this.stackMin = stackMin = stacked ? Math.min.apply(Math, numValues) : min;

            if (options.get('chartRangeMin') !== undefined && (options.get('chartRangeClip') || options.get('chartRangeMin') < min)) {
                min = options.get('chartRangeMin');
            }
            if (options.get('chartRangeMax') !== undefined && (options.get('chartRangeClip') || options.get('chartRangeMax') > max)) {
                max = options.get('chartRangeMax');
            }

            this.zeroAxis = zeroAxis = options.get('zeroAxis', true);
            if (min <= 0 && max >= 0 && zeroAxis) {
                xaxisOffset = 0;
            } else if (zeroAxis == false) {
                xaxisOffset = min;
            } else if (min > 0) {
                xaxisOffset = min;
            } else {
                xaxisOffset = max;
            }
            this.xaxisOffset = xaxisOffset;

            range = stacked ? (Math.max.apply(Math, stackRanges) + Math.max.apply(Math, stackRangesNeg)) : max - min;

            // as we plot zero/min values a single pixel line, we add a pixel to all other
            // values - Reduce the effective canvas size to suit
            this.canvasHeightEf = (zeroAxis && min < 0) ? this.canvasHeight - 2 : this.canvasHeight - 1;

            if (min < xaxisOffset) {
                yMaxCalc = (stacked && max >= 0) ? stackMax : max;
                yoffset = (yMaxCalc - xaxisOffset) / range * this.canvasHeight;
                if (yoffset !== Math.ceil(yoffset)) {
                    this.canvasHeightEf -= 2;
                    yoffset = Math.ceil(yoffset);
                }
            } else {
                yoffset = this.canvasHeight;
            }
            this.yoffset = yoffset;

            if ($.isArray(options.get('colorMap'))) {
                this.colorMapByIndex = options.get('colorMap');
                this.colorMapByValue = null;
            } else {
                this.colorMapByIndex = null;
                this.colorMapByValue = options.get('colorMap');
                if (this.colorMapByValue && this.colorMapByValue.get === undefined) {
                    this.colorMapByValue = new RangeMap(this.colorMapByValue);
                }
            }

            this.range = range;
        },

        getRegion: function (el, x, y) {
            var result = Math.floor(x / this.totalBarWidth);
            return (result < 0 || result >= this.values.length) ? undefined : result;
        },

        getCurrentRegionFields: function () {
            var currentRegion = this.currentRegion,
                values = ensureArray(this.values[currentRegion]),
                result = [],
                value, i;
            for (i = values.length; i--;) {
                value = values[i];
                result.push({
                    isNull: value === null,
                    value: value,
                    color: this.calcColor(i, value, currentRegion),
                    offset: currentRegion
                });
            }
            return result;
        },

        calcColor: function (stacknum, value, valuenum) {
            var colorMapByIndex = this.colorMapByIndex,
                colorMapByValue = this.colorMapByValue,
                options = this.options,
                color, newColor;
            if (this.stacked) {
                color = options.get('stackedBarColor');
            } else {
                color = (value < 0) ? options.get('negBarColor') : options.get('barColor');
            }
            if (value === 0 && options.get('zeroColor') !== undefined) {
                color = options.get('zeroColor');
            }
            if (colorMapByValue && (newColor = colorMapByValue.get(value))) {
                color = newColor;
            } else if (colorMapByIndex && colorMapByIndex.length > valuenum) {
                color = colorMapByIndex[valuenum];
            }
            return $.isArray(color) ? color[stacknum % color.length] : color;
        },

        
        renderRegion: function (valuenum, highlight) {
            var vals = this.values[valuenum],
                options = this.options,
                xaxisOffset = this.xaxisOffset,
                result = [],
                range = this.range,
                stacked = this.stacked,
                target = this.target,
                x = valuenum * this.totalBarWidth,
                canvasHeightEf = this.canvasHeightEf,
                yoffset = this.yoffset,
                y, height, color, isNull, yoffsetNeg, i, valcount, val, minPlotted, allMin;

            vals = $.isArray(vals) ? vals : [vals];
            valcount = vals.length;
            val = vals[0];
            isNull = all(null, vals);
            allMin = all(xaxisOffset, vals, true);

            if (isNull) {
                if (options.get('nullColor')) {
                    color = highlight ? options.get('nullColor') : this.calcHighlightColor(options.get('nullColor'), options);
                    y = (yoffset > 0) ? yoffset - 1 : yoffset;
                    return target.drawRect(x, y, this.barWidth - 1, 0, color, color);
                } else {
                    return undefined;
                }
            }
            yoffsetNeg = yoffset;
            for (i = 0; i < valcount; i++) {
                val = vals[i];

                if (stacked && val === xaxisOffset) {
                    if (!allMin || minPlotted) {
                        continue;
                    }
                    minPlotted = true;
                }

                if (range > 0) {
                    height = Math.floor(canvasHeightEf * ((Math.abs(val - xaxisOffset) / range))) + 1;
                } else {
                    height = 1;
                }
                if (val < xaxisOffset || (val === xaxisOffset && yoffset === 0)) {
                    y = yoffsetNeg;
                    yoffsetNeg += height;
                } else {
                    y = yoffset - height;
                    yoffset -= height;
                }
                color = this.calcColor(i, val, valuenum);
                if (highlight) {
                    color = this.calcHighlightColor(color, options);
                }
                result.push(target.drawRect(x, y, this.barWidth - 1, height - 1, color, color));
            }
            if (result.length === 1) {
                return result[0];
            }
            return result;
        }
    });

    
    $.fn.sparkline.tristate = tristate = createClass($.fn.sparkline._base, barHighlightMixin, {
        type: 'tristate',

        init: function (el, values, options, width, height) {
            var barWidth = parseInt(options.get('barWidth'), 10),
                barSpacing = parseInt(options.get('barSpacing'), 10);
            tristate._super.init.call(this, el, values, options, width, height);

            this.regionShapes = {};
            this.barWidth = barWidth;
            this.barSpacing = barSpacing;
            this.totalBarWidth = barWidth + barSpacing;
            this.values = $.map(values, Number);
            this.width = width = (values.length * barWidth) + ((values.length - 1) * barSpacing);

            if ($.isArray(options.get('colorMap'))) {
                this.colorMapByIndex = options.get('colorMap');
                this.colorMapByValue = null;
            } else {
                this.colorMapByIndex = null;
                this.colorMapByValue = options.get('colorMap');
                if (this.colorMapByValue && this.colorMapByValue.get === undefined) {
                    this.colorMapByValue = new RangeMap(this.colorMapByValue);
                }
            }
            this.initTarget();
        },

        getRegion: function (el, x, y) {
            return Math.floor(x / this.totalBarWidth);
        },

        getCurrentRegionFields: function () {
            var currentRegion = this.currentRegion;
            return {
                isNull: this.values[currentRegion] === undefined,
                value: this.values[currentRegion],
                color: this.calcColor(this.values[currentRegion], currentRegion),
                offset: currentRegion
            };
        },

        calcColor: function (value, valuenum) {
            var values = this.values,
                options = this.options,
                colorMapByIndex = this.colorMapByIndex,
                colorMapByValue = this.colorMapByValue,
                color, newColor;

            if (colorMapByValue && (newColor = colorMapByValue.get(value))) {
                color = newColor;
            } else if (colorMapByIndex && colorMapByIndex.length > valuenum) {
                color = colorMapByIndex[valuenum];
            } else if (values[valuenum] < 0) {
                color = options.get('negBarColor');
            } else if (values[valuenum] > 0) {
                color = options.get('posBarColor');
            } else {
                color = options.get('zeroBarColor');
            }
            return color;
        },

        renderRegion: function (valuenum, highlight) {
            var values = this.values,
                options = this.options,
                target = this.target,
                canvasHeight, height, halfHeight,
                x, y, color;

            canvasHeight = target.pixelHeight;
            halfHeight = Math.round(canvasHeight / 2);

            x = valuenum * this.totalBarWidth;
            if (values[valuenum] < 0) {
                y = halfHeight;
                height = halfHeight - 1;
            } else if (values[valuenum] > 0) {
                y = 0;
                height = halfHeight - 1;
            } else {
                y = halfHeight - 1;
                height = 2;
            }
            color = this.calcColor(values[valuenum], valuenum);
            if (color === null) {
                return;
            }
            if (highlight) {
                color = this.calcHighlightColor(color, options);
            }
            return target.drawRect(x, y, this.barWidth - 1, height - 1, color, color);
        }
    });

    
    $.fn.sparkline.discrete = discrete = createClass($.fn.sparkline._base, barHighlightMixin, {
        type: 'discrete',

        init: function (el, values, options, width, height) {
            discrete._super.init.call(this, el, values, options, width, height);

            this.regionShapes = {};
            this.values = values = $.map(values, Number);
            this.min = Math.min.apply(Math, values);
            this.max = Math.max.apply(Math, values);
            this.range = this.max - this.min;
            this.width = width = options.get('width') === 'auto' ? values.length * 2 : this.width;
            this.interval = Math.floor(width / values.length);
            this.itemWidth = width / values.length;
            if (options.get('chartRangeMin') !== undefined && (options.get('chartRangeClip') || options.get('chartRangeMin') < this.min)) {
                this.min = options.get('chartRangeMin');
            }
            if (options.get('chartRangeMax') !== undefined && (options.get('chartRangeClip') || options.get('chartRangeMax') > this.max)) {
                this.max = options.get('chartRangeMax');
            }
            this.initTarget();
            if (this.target) {
                this.lineHeight = options.get('lineHeight') === 'auto' ? Math.round(this.canvasHeight * 0.3) : options.get('lineHeight');
            }
        },

        getRegion: function (el, x, y) {
            return Math.floor(x / this.itemWidth);
        },

        getCurrentRegionFields: function () {
            var currentRegion = this.currentRegion;
            return {
                isNull: this.values[currentRegion] === undefined,
                value: this.values[currentRegion],
                offset: currentRegion
            };
        },

        renderRegion: function (valuenum, highlight) {
            var values = this.values,
                options = this.options,
                min = this.min,
                max = this.max,
                range = this.range,
                interval = this.interval,
                target = this.target,
                canvasHeight = this.canvasHeight,
                lineHeight = this.lineHeight,
                pheight = canvasHeight - lineHeight,
                ytop, val, color, x;

            val = clipval(values[valuenum], min, max);
            x = valuenum * interval;
            ytop = Math.round(pheight - pheight * ((val - min) / range));
            color = (options.get('thresholdColor') && val < options.get('thresholdValue')) ? options.get('thresholdColor') : options.get('lineColor');
            if (highlight) {
                color = this.calcHighlightColor(color, options);
            }
            return target.drawLine(x, ytop, x, ytop + lineHeight, color);
        }
    });

    
    $.fn.sparkline.bullet = bullet = createClass($.fn.sparkline._base, {
        type: 'bullet',

        init: function (el, values, options, width, height) {
            var min, max, vals;
            bullet._super.init.call(this, el, values, options, width, height);

            // values: target, performance, range1, range2, range3
            this.values = values = normalizeValues(values);
            // target or performance could be null
            vals = values.slice();
            vals[0] = vals[0] === null ? vals[2] : vals[0];
            vals[1] = values[1] === null ? vals[2] : vals[1];
            min = Math.min.apply(Math, values);
            max = Math.max.apply(Math, values);
            if (options.get('base') === undefined) {
                min = min < 0 ? min : 0;
            } else {
                min = options.get('base');
            }
            this.min = min;
            this.max = max;
            this.range = max - min;
            this.shapes = {};
            this.valueShapes = {};
            this.regiondata = {};
            this.width = width = options.get('width') === 'auto' ? '4.0em' : width;
            this.target = this.$el.simpledraw(width, height, options.get('composite'));
            if (!values.length) {
                this.disabled = true;
            }
            this.initTarget();
        },

        getRegion: function (el, x, y) {
            var shapeid = this.target.getShapeAt(el, x, y);
            return (shapeid !== undefined && this.shapes[shapeid] !== undefined) ? this.shapes[shapeid] : undefined;
        },

        getCurrentRegionFields: function () {
            var currentRegion = this.currentRegion;
            return {
                fieldkey: currentRegion.substr(0, 1),
                value: this.values[currentRegion.substr(1)],
                region: currentRegion
            };
        },

        changeHighlight: function (highlight) {
            var currentRegion = this.currentRegion,
                shapeid = this.valueShapes[currentRegion],
                shape;
            delete this.shapes[shapeid];
            switch (currentRegion.substr(0, 1)) {
                case 'r':
                    shape = this.renderRange(currentRegion.substr(1), highlight);
                    break;
                case 'p':
                    shape = this.renderPerformance(highlight);
                    break;
                case 't':
                    shape = this.renderTarget(highlight);
                    break;
            }
            this.valueShapes[currentRegion] = shape.id;
            this.shapes[shape.id] = currentRegion;
            this.target.replaceWithShape(shapeid, shape);
        },

        renderRange: function (rn, highlight) {
            var rangeval = this.values[rn],
                rangewidth = Math.round(this.canvasWidth * ((rangeval - this.min) / this.range)),
                color = this.options.get('rangeColors')[rn - 2];
            if (highlight) {
                color = this.calcHighlightColor(color, this.options);
            }
            return this.target.drawRect(0, 0, rangewidth - 1, this.canvasHeight - 1, color, color);
        },

        renderPerformance: function (highlight) {
            var perfval = this.values[1],
                perfwidth = Math.round(this.canvasWidth * ((perfval - this.min) / this.range)),
                color = this.options.get('performanceColor');
            if (highlight) {
                color = this.calcHighlightColor(color, this.options);
            }
            return this.target.drawRect(0, Math.round(this.canvasHeight * 0.3), perfwidth - 1,
                Math.round(this.canvasHeight * 0.4) - 1, color, color);
        },

        renderTarget: function (highlight) {
            var targetval = this.values[0],
                x = Math.round(this.canvasWidth * ((targetval - this.min) / this.range) - (this.options.get('targetWidth') / 2)),
                targettop = Math.round(this.canvasHeight * 0.10),
                targetheight = this.canvasHeight - (targettop * 2),
                color = this.options.get('targetColor');
            if (highlight) {
                color = this.calcHighlightColor(color, this.options);
            }
            return this.target.drawRect(x, targettop, this.options.get('targetWidth') - 1, targetheight - 1, color, color);
        },

        render: function () {
            var vlen = this.values.length,
                target = this.target,
                i, shape;
            if (!bullet._super.render.call(this)) {
                return;
            }
            for (i = 2; i < vlen; i++) {
                shape = this.renderRange(i).append();
                this.shapes[shape.id] = 'r' + i;
                this.valueShapes['r' + i] = shape.id;
            }
            if (this.values[1] !== null) {
                shape = this.renderPerformance().append();
                this.shapes[shape.id] = 'p1';
                this.valueShapes.p1 = shape.id;
            }
            if (this.values[0] !== null) {
                shape = this.renderTarget().append();
                this.shapes[shape.id] = 't0';
                this.valueShapes.t0 = shape.id;
            }
            target.render();
        }
    });

    
    $.fn.sparkline.pie = pie = createClass($.fn.sparkline._base, {
        type: 'pie',

        init: function (el, values, options, width, height) {
            var total = 0, i;

            pie._super.init.call(this, el, values, options, width, height);

            this.shapes = {}; // map shape ids to value offsets
            this.valueShapes = {}; // maps value offsets to shape ids
            this.values = values = $.map(values, Number);

            if (options.get('width') === 'auto') {
                this.width = this.height;
            }

            if (values.length > 0) {
                for (i = values.length; i--;) {
                    total += values[i];
                }
            }
            this.total = total;
            this.initTarget();
            this.radius = Math.floor(Math.min(this.canvasWidth, this.canvasHeight) / 2);
        },

        getRegion: function (el, x, y) {
            var shapeid = this.target.getShapeAt(el, x, y);
            return (shapeid !== undefined && this.shapes[shapeid] !== undefined) ? this.shapes[shapeid] : undefined;
        },

        getCurrentRegionFields: function () {
            var currentRegion = this.currentRegion;
            return {
                isNull: this.values[currentRegion] === undefined,
                value: this.values[currentRegion],
                percent: this.values[currentRegion] / this.total * 100,
                color: this.options.get('sliceColors')[currentRegion % this.options.get('sliceColors').length],
                offset: currentRegion
            };
        },

        changeHighlight: function (highlight) {
            var currentRegion = this.currentRegion,
                 newslice = this.renderSlice(currentRegion, highlight),
                 shapeid = this.valueShapes[currentRegion];
            delete this.shapes[shapeid];
            this.target.replaceWithShape(shapeid, newslice);
            this.valueShapes[currentRegion] = newslice.id;
            this.shapes[newslice.id] = currentRegion;
        },

        renderSlice: function (valuenum, highlight) {
            var target = this.target,
                options = this.options,
                radius = this.radius,
                borderWidth = options.get('borderWidth'),
                offset = options.get('offset'),
                circle = 2 * Math.PI,
                values = this.values,
                total = this.total,
                next = offset ? (2*Math.PI)*(offset/360) : 0,
                start, end, i, vlen, color;

            vlen = values.length;
            for (i = 0; i < vlen; i++) {
                start = next;
                end = next;
                if (total > 0) {  // avoid divide by zero
                    end = next + (circle * (values[i] / total));
                }
                if (valuenum === i) {
                    color = options.get('sliceColors')[i % options.get('sliceColors').length];
                    if (highlight) {
                        color = this.calcHighlightColor(color, options);
                    }

                    return target.drawPieSlice(radius, radius, radius - borderWidth, start, end, undefined, color);
                }
                next = end;
            }
        },

        render: function () {
            var target = this.target,
                values = this.values,
                options = this.options,
                radius = this.radius,
                borderWidth = options.get('borderWidth'),
                shape, i;

            if (!pie._super.render.call(this)) {
                return;
            }
            if (borderWidth) {
                target.drawCircle(radius, radius, Math.floor(radius - (borderWidth / 2)),
                    options.get('borderColor'), undefined, borderWidth).append();
            }
            for (i = values.length; i--;) {
                if (values[i]) { // don't render zero values
                    shape = this.renderSlice(i).append();
                    this.valueShapes[i] = shape.id; // store just the shapeid
                    this.shapes[shape.id] = i;
                }
            }
            target.render();
        }
    });

    
    $.fn.sparkline.box = box = createClass($.fn.sparkline._base, {
        type: 'box',

        init: function (el, values, options, width, height) {
            box._super.init.call(this, el, values, options, width, height);
            this.values = $.map(values, Number);
            this.width = options.get('width') === 'auto' ? '4.0em' : width;
            this.initTarget();
            if (!this.values.length) {
                this.disabled = 1;
            }
        },

        
        getRegion: function () {
            return 1;
        },

        getCurrentRegionFields: function () {
            var result = [
                { field: 'lq', value: this.quartiles[0] },
                { field: 'med', value: this.quartiles[1] },
                { field: 'uq', value: this.quartiles[2] }
            ];
            if (this.loutlier !== undefined) {
                result.push({ field: 'lo', value: this.loutlier});
            }
            if (this.routlier !== undefined) {
                result.push({ field: 'ro', value: this.routlier});
            }
            if (this.lwhisker !== undefined) {
                result.push({ field: 'lw', value: this.lwhisker});
            }
            if (this.rwhisker !== undefined) {
                result.push({ field: 'rw', value: this.rwhisker});
            }
            return result;
        },

        render: function () {
            var target = this.target,
                values = this.values,
                vlen = values.length,
                options = this.options,
                canvasWidth = this.canvasWidth,
                canvasHeight = this.canvasHeight,
                minValue = options.get('chartRangeMin') === undefined ? Math.min.apply(Math, values) : options.get('chartRangeMin'),
                maxValue = options.get('chartRangeMax') === undefined ? Math.max.apply(Math, values) : options.get('chartRangeMax'),
                canvasLeft = 0,
                lwhisker, loutlier, iqr, q1, q2, q3, rwhisker, routlier, i,
                size, unitSize;

            if (!box._super.render.call(this)) {
                return;
            }

            if (options.get('raw')) {
                if (options.get('showOutliers') && values.length > 5) {
                    loutlier = values[0];
                    lwhisker = values[1];
                    q1 = values[2];
                    q2 = values[3];
                    q3 = values[4];
                    rwhisker = values[5];
                    routlier = values[6];
                } else {
                    lwhisker = values[0];
                    q1 = values[1];
                    q2 = values[2];
                    q3 = values[3];
                    rwhisker = values[4];
                }
            } else {
                values.sort(function (a, b) { return a - b; });
                q1 = quartile(values, 1);
                q2 = quartile(values, 2);
                q3 = quartile(values, 3);
                iqr = q3 - q1;
                if (options.get('showOutliers')) {
                    lwhisker = rwhisker = undefined;
                    for (i = 0; i < vlen; i++) {
                        if (lwhisker === undefined && values[i] > q1 - (iqr * options.get('outlierIQR'))) {
                            lwhisker = values[i];
                        }
                        if (values[i] < q3 + (iqr * options.get('outlierIQR'))) {
                            rwhisker = values[i];
                        }
                    }
                    loutlier = values[0];
                    routlier = values[vlen - 1];
                } else {
                    lwhisker = values[0];
                    rwhisker = values[vlen - 1];
                }
            }
            this.quartiles = [q1, q2, q3];
            this.lwhisker = lwhisker;
            this.rwhisker = rwhisker;
            this.loutlier = loutlier;
            this.routlier = routlier;

            unitSize = canvasWidth / (maxValue - minValue + 1);
            if (options.get('showOutliers')) {
                canvasLeft = Math.ceil(options.get('spotRadius'));
                canvasWidth -= 2 * Math.ceil(options.get('spotRadius'));
                unitSize = canvasWidth / (maxValue - minValue + 1);
                if (loutlier < lwhisker) {
                    target.drawCircle((loutlier - minValue) * unitSize + canvasLeft,
                        canvasHeight / 2,
                        options.get('spotRadius'),
                        options.get('outlierLineColor'),
                        options.get('outlierFillColor')).append();
                }
                if (routlier > rwhisker) {
                    target.drawCircle((routlier - minValue) * unitSize + canvasLeft,
                        canvasHeight / 2,
                        options.get('spotRadius'),
                        options.get('outlierLineColor'),
                        options.get('outlierFillColor')).append();
                }
            }

            // box
            target.drawRect(
                Math.round((q1 - minValue) * unitSize + canvasLeft),
                Math.round(canvasHeight * 0.1),
                Math.round((q3 - q1) * unitSize),
                Math.round(canvasHeight * 0.8),
                options.get('boxLineColor'),
                options.get('boxFillColor')).append();
            // left whisker
            target.drawLine(
                Math.round((lwhisker - minValue) * unitSize + canvasLeft),
                Math.round(canvasHeight / 2),
                Math.round((q1 - minValue) * unitSize + canvasLeft),
                Math.round(canvasHeight / 2),
                options.get('lineColor')).append();
            target.drawLine(
                Math.round((lwhisker - minValue) * unitSize + canvasLeft),
                Math.round(canvasHeight / 4),
                Math.round((lwhisker - minValue) * unitSize + canvasLeft),
                Math.round(canvasHeight - canvasHeight / 4),
                options.get('whiskerColor')).append();
            // right whisker
            target.drawLine(Math.round((rwhisker - minValue) * unitSize + canvasLeft),
                Math.round(canvasHeight / 2),
                Math.round((q3 - minValue) * unitSize + canvasLeft),
                Math.round(canvasHeight / 2),
                options.get('lineColor')).append();
            target.drawLine(
                Math.round((rwhisker - minValue) * unitSize + canvasLeft),
                Math.round(canvasHeight / 4),
                Math.round((rwhisker - minValue) * unitSize + canvasLeft),
                Math.round(canvasHeight - canvasHeight / 4),
                options.get('whiskerColor')).append();
            // median line
            target.drawLine(
                Math.round((q2 - minValue) * unitSize + canvasLeft),
                Math.round(canvasHeight * 0.1),
                Math.round((q2 - minValue) * unitSize + canvasLeft),
                Math.round(canvasHeight * 0.9),
                options.get('medianColor')).append();
            if (options.get('target')) {
                size = Math.ceil(options.get('spotRadius'));
                target.drawLine(
                    Math.round((options.get('target') - minValue) * unitSize + canvasLeft),
                    Math.round((canvasHeight / 2) - size),
                    Math.round((options.get('target') - minValue) * unitSize + canvasLeft),
                    Math.round((canvasHeight / 2) + size),
                    options.get('targetColor')).append();
                target.drawLine(
                    Math.round((options.get('target') - minValue) * unitSize + canvasLeft - size),
                    Math.round(canvasHeight / 2),
                    Math.round((options.get('target') - minValue) * unitSize + canvasLeft + size),
                    Math.round(canvasHeight / 2),
                    options.get('targetColor')).append();
            }
            target.render();
        }
    });

    // Setup a very simple "virtual canvas" to make drawing the few shapes we need easier
    // This is accessible as $(foo).simpledraw()

    if ($.browser.msie && document.namespaces && !document.namespaces.v) {
        document.namespaces.add('v', 'urn:schemas-microsoft-com:vml', '#default#VML');
    }

    if ($.browser.hasCanvas === undefined) {
        $.browser.hasCanvas = document.createElement('canvas').getContext !== undefined;
    }

    VShape = createClass({
        init: function (target, id, type, args) {
            this.target = target;
            this.id = id;
            this.type = type;
            this.args = args;
        },
        append: function () {
            this.target.appendShape(this);
            return this;
        }
    });

    VCanvas_base = createClass({
        _pxregex: /(\d+)(px)?\s*$/i,

        init: function (width, height, target) {
            if (!width) {
                return;
            }
            this.width = width;
            this.height = height;
            this.target = target;
            this.lastShapeId = null;
            if (target[0]) {
                target = target[0];
            }
            $.data(target, '_jqs_vcanvas', this);
        },

        drawLine: function (x1, y1, x2, y2, lineColor, lineWidth) {
            return this.drawShape([[x1, y1], [x2, y2]], lineColor, lineWidth);
        },

        drawShape: function (path, lineColor, fillColor, lineWidth) {
            return this._genShape('Shape', [path, lineColor, fillColor, lineWidth]);
        },

        drawCircle: function (x, y, radius, lineColor, fillColor, lineWidth) {
            return this._genShape('Circle', [x, y, radius, lineColor, fillColor, lineWidth]);
        },

        drawPieSlice: function (x, y, radius, startAngle, endAngle, lineColor, fillColor) {
            return this._genShape('PieSlice', [x, y, radius, startAngle, endAngle, lineColor, fillColor]);
        },

        drawRect: function (x, y, width, height, lineColor, fillColor) {
            return this._genShape('Rect', [x, y, width, height, lineColor, fillColor]);
        },

        getElement: function () {
            return this.canvas;
        },

        
        getLastShapeId: function () {
            return this.lastShapeId;
        },

        
        reset: function () {
            alert('reset not implemented');
        },

        _insert: function (el, target) {
            $(target).html(el);
        },

        
        _calculatePixelDims: function (width, height, canvas) {
            // XXX This should probably be a configurable option
            var match;
            match = this._pxregex.exec(height);
            if (match) {
                this.pixelHeight = match[1];
            } else {
                this.pixelHeight = $(canvas).height();
            }
            match = this._pxregex.exec(width);
            if (match) {
                this.pixelWidth = match[1];
            } else {
                this.pixelWidth = $(canvas).width();
            }
        },

        
        _genShape: function (shapetype, shapeargs) {
            var id = shapeCount++;
            shapeargs.unshift(id);
            return new VShape(this, id, shapetype, shapeargs);
        },

        
        appendShape: function (shape) {
            alert('appendShape not implemented');
        },

        
        replaceWithShape: function (shapeid, shape) {
            alert('replaceWithShape not implemented');
        },

        
        insertAfterShape: function (shapeid, shape) {
            alert('insertAfterShape not implemented');
        },

        
        removeShapeId: function (shapeid) {
            alert('removeShapeId not implemented');
        },

        
        getShapeAt: function (el, x, y) {
            alert('getShapeAt not implemented');
        },

        
        render: function () {
            alert('render not implemented');
        }
    });

    VCanvas_canvas = createClass(VCanvas_base, {
        init: function (width, height, target, interact) {
            VCanvas_canvas._super.init.call(this, width, height, target);
            this.canvas = document.createElement('canvas');
            if (target[0]) {
                target = target[0];
            }
            $.data(target, '_jqs_vcanvas', this);
            $(this.canvas).css({ display: 'inline-block', width: width, height: height, verticalAlign: 'top' });
            this._insert(this.canvas, target);
            this._calculatePixelDims(width, height, this.canvas);
            this.canvas.width = this.pixelWidth;
            this.canvas.height = this.pixelHeight;
            this.interact = interact;
            this.shapes = {};
            this.shapeseq = [];
            this.currentTargetShapeId = undefined;
            $(this.canvas).css({width: this.pixelWidth, height: this.pixelHeight});
        },

        _getContext: function (lineColor, fillColor, lineWidth) {
            var context = this.canvas.getContext('2d');
            if (lineColor !== undefined) {
                context.strokeStyle = lineColor;
            }
            context.lineWidth = lineWidth === undefined ? 1 : lineWidth;
            if (fillColor !== undefined) {
                context.fillStyle = fillColor;
            }
            return context;
        },

        reset: function () {
            var context = this._getContext();
            context.clearRect(0, 0, this.pixelWidth, this.pixelHeight);
            this.shapes = {};
            this.shapeseq = [];
            this.currentTargetShapeId = undefined;
        },

        _drawShape: function (shapeid, path, lineColor, fillColor, lineWidth) {
            var context = this._getContext(lineColor, fillColor, lineWidth),
                i, plen;
            context.beginPath();
            context.moveTo(path[0][0] + 0.5, path[0][1] + 0.5);
            for (i = 1, plen = path.length; i < plen; i++) {
                context.lineTo(path[i][0] + 0.5, path[i][1] + 0.5); // the 0.5 offset gives us crisp pixel-width lines
            }
            if (lineColor !== undefined) {
                context.stroke();
            }
            if (fillColor !== undefined) {
                context.fill();
            }
            if (this.targetX !== undefined && this.targetY !== undefined &&
                context.isPointInPath(this.targetX, this.targetY)) {
                this.currentTargetShapeId = shapeid;
            }
        },

        _drawCircle: function (shapeid, x, y, radius, lineColor, fillColor, lineWidth) {
            var context = this._getContext(lineColor, fillColor, lineWidth);
            context.beginPath();
            context.arc(x, y, radius, 0, 2 * Math.PI, false);
            if (this.targetX !== undefined && this.targetY !== undefined &&
                context.isPointInPath(this.targetX, this.targetY)) {
                this.currentTargetShapeId = shapeid;
            }
            if (lineColor !== undefined) {
                context.stroke();
            }
            if (fillColor !== undefined) {
                context.fill();
            }
        },

        _drawPieSlice: function (shapeid, x, y, radius, startAngle, endAngle, lineColor, fillColor) {
            var context = this._getContext(lineColor, fillColor);
            context.beginPath();
            context.moveTo(x, y);
            context.arc(x, y, radius, startAngle, endAngle, false);
            context.lineTo(x, y);
            context.closePath();
            if (lineColor !== undefined) {
                context.stroke();
            }
            if (fillColor) {
                context.fill();
            }
            if (this.targetX !== undefined && this.targetY !== undefined &&
                context.isPointInPath(this.targetX, this.targetY)) {
                this.currentTargetShapeId = shapeid;
            }
        },

        _drawRect: function (shapeid, x, y, width, height, lineColor, fillColor) {
            return this._drawShape(shapeid, [[x, y], [x + width, y], [x + width, y + height], [x, y + height], [x, y]], lineColor, fillColor);
        },

        appendShape: function (shape) {
            this.shapes[shape.id] = shape;
            this.shapeseq.push(shape.id);
            this.lastShapeId = shape.id;
            return shape.id;
        },

        replaceWithShape: function (shapeid, shape) {
            var shapeseq = this.shapeseq,
                i;
            this.shapes[shape.id] = shape;
            for (i = shapeseq.length; i--;) {
                if (shapeseq[i] == shapeid) {
                    shapeseq[i] = shape.id;
                }
            }
            delete this.shapes[shapeid];
        },

        replaceWithShapes: function (shapeids, shapes) {
            var shapeseq = this.shapeseq,
                shapemap = {},
                sid, i, first;

            for (i = shapeids.length; i--;) {
                shapemap[shapeids[i]] = true;
            }
            for (i = shapeseq.length; i--;) {
                sid = shapeseq[i];
                if (shapemap[sid]) {
                    shapeseq.splice(i, 1);
                    delete this.shapes[sid];
                    first = i;
                }
            }
            for (i = shapes.length; i--;) {
                shapeseq.splice(first, 0, shapes[i].id);
                this.shapes[shapes[i].id] = shapes[i];
            }

        },

        insertAfterShape: function (shapeid, shape) {
            var shapeseq = this.shapeseq,
                i;
            for (i = shapeseq.length; i--;) {
                if (shapeseq[i] === shapeid) {
                    shapeseq.splice(i + 1, 0, shape.id);
                    this.shapes[shape.id] = shape;
                    return;
                }
            }
        },

        removeShapeId: function (shapeid) {
            var shapeseq = this.shapeseq,
                i;
            for (i = shapeseq.length; i--;) {
                if (shapeseq[i] === shapeid) {
                    shapeseq.splice(i, 1);
                    break;
                }
            }
            delete this.shapes[shapeid];
        },

        getShapeAt: function (el, x, y) {
            this.targetX = x;
            this.targetY = y;
            this.render();
            return this.currentTargetShapeId;
        },

        render: function () {
            var shapeseq = this.shapeseq,
                shapes = this.shapes,
                shapeCount = shapeseq.length,
                context = this._getContext(),
                shapeid, shape, i;
            context.clearRect(0, 0, this.pixelWidth, this.pixelHeight);
            for (i = 0; i < shapeCount; i++) {
                shapeid = shapeseq[i];
                shape = shapes[shapeid];
                this['_draw' + shape.type].apply(this, shape.args);
            }
            if (!this.interact) {
                // not interactive so no need to keep the shapes array
                this.shapes = {};
                this.shapeseq = [];
            }
        }

    });

    VCanvas_vml = createClass(VCanvas_base, {
        init: function (width, height, target) {
            var groupel;
            VCanvas_vml._super.init.call(this, width, height, target);
            if (target[0]) {
                target = target[0];
            }
            $.data(target, '_jqs_vcanvas', this);
            this.canvas = document.createElement('span');
            $(this.canvas).css({ display: 'inline-block', position: 'relative', overflow: 'hidden', width: width, height: height, margin: '0px', padding: '0px', verticalAlign: 'top'});
            this._insert(this.canvas, target);
            this._calculatePixelDims(width, height, this.canvas);
            this.canvas.width = this.pixelWidth;
            this.canvas.height = this.pixelHeight;
            groupel = '<v:group coordorigin="0 0" coordsize="' + this.pixelWidth + ' ' + this.pixelHeight + '"' +
                    ' style="position:absolute;top:0;left:0;width:' + this.pixelWidth + 'px;height=' + this.pixelHeight + 'px;"></v:group>';
            this.canvas.insertAdjacentHTML('beforeEnd', groupel);
            this.group = $(this.canvas).children()[0];
            this.rendered = false;
            this.prerender = '';
        },

        _drawShape: function (shapeid, path, lineColor, fillColor, lineWidth) {
            var vpath = [],
                initial, stroke, fill, closed, vel, plen, i;
            for (i = 0, plen = path.length; i < plen; i++) {
                vpath[i] = '' + (path[i][0]) + ',' + (path[i][1]);
            }
            initial = vpath.splice(0, 1);
            lineWidth = lineWidth === undefined ? 1 : lineWidth;
            stroke = lineColor === undefined ? ' stroked="false" ' : ' strokeWeight="' + lineWidth + 'px" strokeColor="' + lineColor + '" ';
            fill = fillColor === undefined ? ' filled="false"' : ' fillColor="' + fillColor + '" filled="true" ';
            closed = vpath[0] === vpath[vpath.length - 1] ? 'x ' : '';
            vel = '<v:shape coordorigin="0 0" coordsize="' + this.pixelWidth + ' ' + this.pixelHeight + '" ' +
                 ' id="jqsshape' + shapeid + '" ' +
                 stroke +
                 fill +
                ' style="position:absolute;left:0px;top:0px;height:' + this.pixelHeight + 'px;width:' + this.pixelWidth + 'px;padding:0px;margin:0px;" ' +
                ' path="m ' + initial + ' l ' + vpath.join(', ') + ' ' + closed + 'e">' +
                ' </v:shape>';
            return vel;
        },

        _drawCircle: function (shapeid, x, y, radius, lineColor, fillColor, lineWidth) {
            var stroke, fill, vel;
            x -= radius;
            y -= radius;
            stroke = lineColor === undefined ? ' stroked="false" ' : ' strokeWeight="' + lineWidth + 'px" strokeColor="' + lineColor + '" ';
            fill = fillColor === undefined ? ' filled="false"' : ' fillColor="' + fillColor + '" filled="true" ';
            vel = '<v:oval ' +
                 ' id="jqsshape' + shapeid + '" ' +
                stroke +
                fill +
                ' style="position:absolute;top:' + y + 'px; left:' + x + 'px; width:' + (radius * 2) + 'px; height:' + (radius * 2) + 'px"></v:oval>';
            return vel;

        },

        _drawPieSlice: function (shapeid, x, y, radius, startAngle, endAngle, lineColor, fillColor) {
            var vpath, startx, starty, endx, endy, stroke, fill, vel;
            if (startAngle === endAngle) {
                return;  // VML seems to have problem when start angle equals end angle.
            }
            if ((endAngle - startAngle) === (2 * Math.PI)) {
                startAngle = 0.0;  // VML seems to have a problem when drawing a full circle that doesn't start 0
                endAngle = (2 * Math.PI);
            }

            startx = x + Math.round(Math.cos(startAngle) * radius);
            starty = y + Math.round(Math.sin(startAngle) * radius);
            endx = x + Math.round(Math.cos(endAngle) * radius);
            endy = y + Math.round(Math.sin(endAngle) * radius);

            // Prevent very small slices from being mistaken as a whole pie
            if (startx === endx && starty === endy && (endAngle - startAngle) < Math.PI) {
                return;
            }

            vpath = [x - radius, y - radius, x + radius, y + radius, startx, starty, endx, endy];
            stroke = lineColor === undefined ? ' stroked="false" ' : ' strokeWeight="1px" strokeColor="' + lineColor + '" ';
            fill = fillColor === undefined ? ' filled="false"' : ' fillColor="' + fillColor + '" filled="true" ';
            vel = '<v:shape coordorigin="0 0" coordsize="' + this.pixelWidth + ' ' + this.pixelHeight + '" ' +
                 ' id="jqsshape' + shapeid + '" ' +
                 stroke +
                 fill +
                ' style="position:absolute;left:0px;top:0px;height:' + this.pixelHeight + 'px;width:' + this.pixelWidth + 'px;padding:0px;margin:0px;" ' +
                ' path="m ' + x + ',' + y + ' wa ' + vpath.join(', ') + ' x e">' +
                ' </v:shape>';
            return vel;
        },

        _drawRect: function (shapeid, x, y, width, height, lineColor, fillColor) {
            return this._drawShape(shapeid, [[x, y], [x, y + height], [x + width, y + height], [x + width, y], [x, y]], lineColor, fillColor);
        },

        reset: function () {
            this.group.innerHTML = '';
        },

        appendShape: function (shape) {
            var vel = this['_draw' + shape.type].apply(this, shape.args);
            if (this.rendered) {
                this.group.insertAdjacentHTML('beforeEnd', vel);
            } else {
                this.prerender += vel;
            }
            this.lastShapeId = shape.id;
            return shape.id;
        },

        replaceWithShape: function (shapeid, shape) {
            var existing = $('#jqsshape' + shapeid),
                vel = this['_draw' + shape.type].apply(this, shape.args);
            existing[0].outerHTML = vel;
        },

        replaceWithShapes: function (shapeids, shapes) {
            // replace the first shapeid with all the new shapes then toast the remaining old shapes
            var existing = $('#jqsshape' + shapeids[0]),
                replace = '',
                slen = shapes.length,
                i;
            for (i = 0; i < slen; i++) {
                replace += this['_draw' + shapes[i].type].apply(this, shapes[i].args);
            }
            existing[0].outerHTML = replace;
            for (i = 1; i < shapeids.length; i++) {
                $('#jqsshape' + shapeids[i]).remove();
            }
        },

        insertAfterShape: function (shapeid, shape) {
            var existing = $('#jqsshape' + shapeid),
                 vel = this['_draw' + shape.type].apply(this, shape.args);
            existing[0].insertAdjacentHTML('afterEnd', vel);
        },

        removeShapeId: function (shapeid) {
            var existing = $('#jqsshape' + shapeid);
            this.group.removeChild(existing[0]);
        },

        getShapeAt: function (el, x, y) {
            var shapeid = el.id.substr(8);
            return shapeid;
        },

        render: function () {
            if (!this.rendered) {
                // batch the intial render into a single repaint
                this.group.innerHTML = this.prerender;
                this.rendered = true;
            }
        }
    });

}));


(function( $, undefined ) {

$.ui = $.ui || {};

var cachedScrollbarWidth,
	max = Math.max,
	abs = Math.abs,
	round = Math.round,
	rhorizontal = /left|center|right/,
	rvertical = /top|center|bottom/,
	roffset = /[\+\-]\d+%?/,
	rposition = /^\w+/,
	rpercent = /%$/,
	_position = $.fn.position;

function getOffsets( offsets, width, height ) {
	return [
		parseInt( offsets[ 0 ], 10 ) * ( rpercent.test( offsets[ 0 ] ) ? width / 100 : 1 ),
		parseInt( offsets[ 1 ], 10 ) * ( rpercent.test( offsets[ 1 ] ) ? height / 100 : 1 )
	];
}

function parseCss( element, property ) {
	return parseInt( $.css( element, property ), 10 ) || 0;
}

function getDimensions( elem ) {
	var raw = elem[0];
	if ( raw.nodeType === 9 ) {
		return {
			width: elem.width(),
			height: elem.height(),
			offset: { top: 0, left: 0 }
		};
	}
	if ( $.isWindow( raw ) ) {
		return {
			width: elem.width(),
			height: elem.height(),
			offset: { top: elem.scrollTop(), left: elem.scrollLeft() }
		};
	}
	if ( raw.preventDefault ) {
		return {
			width: 0,
			height: 0,
			offset: { top: raw.pageY, left: raw.pageX }
		};
	}
	return {
		width: elem.outerWidth(),
		height: elem.outerHeight(),
		offset: elem.offset()
	};
}

$.position = {
	scrollbarWidth: function() {
		if ( cachedScrollbarWidth !== undefined ) {
			return cachedScrollbarWidth;
		}
		var w1, w2,
			div = $( "<div style='display:block;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>" ),
			innerDiv = div.children()[0];

		$( "body" ).append( div );
		w1 = innerDiv.offsetWidth;
		div.css( "overflow", "scroll" );

		w2 = innerDiv.offsetWidth;

		if ( w1 === w2 ) {
			w2 = div[0].clientWidth;
		}

		div.remove();

		return (cachedScrollbarWidth = w1 - w2);
	},
	getScrollInfo: function( within ) {
		var overflowX = within.isWindow ? "" : within.element.css( "overflow-x" ),
			overflowY = within.isWindow ? "" : within.element.css( "overflow-y" ),
			hasOverflowX = overflowX === "scroll" ||
				( overflowX === "auto" && within.width < within.element[0].scrollWidth ),
			hasOverflowY = overflowY === "scroll" ||
				( overflowY === "auto" && within.height < within.element[0].scrollHeight );
		return {
			width: hasOverflowX ? $.position.scrollbarWidth() : 0,
			height: hasOverflowY ? $.position.scrollbarWidth() : 0
		};
	},
	getWithinInfo: function( element ) {
		var withinElement = $( element || window ),
			isWindow = $.isWindow( withinElement[0] );
		return {
			element: withinElement,
			isWindow: isWindow,
			offset: withinElement.offset() || { left: 0, top: 0 },
			scrollLeft: withinElement.scrollLeft(),
			scrollTop: withinElement.scrollTop(),
			width: isWindow ? withinElement.width() : withinElement.outerWidth(),
			height: isWindow ? withinElement.height() : withinElement.outerHeight()
		};
	}
};

$.fn.position = function( options ) {
	if ( !options || !options.of ) {
		return _position.apply( this, arguments );
	}

	// make a copy, we don't want to modify arguments
	options = $.extend( {}, options );

	var atOffset, targetWidth, targetHeight, targetOffset, basePosition, dimensions,
		target = $( options.of ),
		within = $.position.getWithinInfo( options.within ),
		scrollInfo = $.position.getScrollInfo( within ),
		collision = ( options.collision || "flip" ).split( " " ),
		offsets = {};

	dimensions = getDimensions( target );
	if ( target[0].preventDefault ) {
		// force left top to allow flipping
		options.at = "left top";
	}
	targetWidth = dimensions.width;
	targetHeight = dimensions.height;
	targetOffset = dimensions.offset;
	// clone to reuse original targetOffset later
	basePosition = $.extend( {}, targetOffset );

	// force my and at to have valid horizontal and vertical positions
	// if a value is missing or invalid, it will be converted to center
	$.each( [ "my", "at" ], function() {
		var pos = ( options[ this ] || "" ).split( " " ),
			horizontalOffset,
			verticalOffset;

		if ( pos.length === 1) {
			pos = rhorizontal.test( pos[ 0 ] ) ?
				pos.concat( [ "center" ] ) :
				rvertical.test( pos[ 0 ] ) ?
					[ "center" ].concat( pos ) :
					[ "center", "center" ];
		}
		pos[ 0 ] = rhorizontal.test( pos[ 0 ] ) ? pos[ 0 ] : "center";
		pos[ 1 ] = rvertical.test( pos[ 1 ] ) ? pos[ 1 ] : "center";

		// calculate offsets
		horizontalOffset = roffset.exec( pos[ 0 ] );
		verticalOffset = roffset.exec( pos[ 1 ] );
		offsets[ this ] = [
			horizontalOffset ? horizontalOffset[ 0 ] : 0,
			verticalOffset ? verticalOffset[ 0 ] : 0
		];

		// reduce to just the positions without the offsets
		options[ this ] = [
			rposition.exec( pos[ 0 ] )[ 0 ],
			rposition.exec( pos[ 1 ] )[ 0 ]
		];
	});

	// normalize collision option
	if ( collision.length === 1 ) {
		collision[ 1 ] = collision[ 0 ];
	}

	if ( options.at[ 0 ] === "right" ) {
		basePosition.left += targetWidth;
	} else if ( options.at[ 0 ] === "center" ) {
		basePosition.left += targetWidth / 2;
	}

	if ( options.at[ 1 ] === "bottom" ) {
		basePosition.top += targetHeight;
	} else if ( options.at[ 1 ] === "center" ) {
		basePosition.top += targetHeight / 2;
	}

	atOffset = getOffsets( offsets.at, targetWidth, targetHeight );
	basePosition.left += atOffset[ 0 ];
	basePosition.top += atOffset[ 1 ];

	return this.each(function() {
		var collisionPosition, using,
			elem = $( this ),
			elemWidth = elem.outerWidth(),
			elemHeight = elem.outerHeight(),
			marginLeft = parseCss( this, "marginLeft" ),
			marginTop = parseCss( this, "marginTop" ),
			collisionWidth = elemWidth + marginLeft + parseCss( this, "marginRight" ) + scrollInfo.width,
			collisionHeight = elemHeight + marginTop + parseCss( this, "marginBottom" ) + scrollInfo.height,
			position = $.extend( {}, basePosition ),
			myOffset = getOffsets( offsets.my, elem.outerWidth(), elem.outerHeight() );

		if ( options.my[ 0 ] === "right" ) {
			position.left -= elemWidth;
		} else if ( options.my[ 0 ] === "center" ) {
			position.left -= elemWidth / 2;
		}

		if ( options.my[ 1 ] === "bottom" ) {
			position.top -= elemHeight;
		} else if ( options.my[ 1 ] === "center" ) {
			position.top -= elemHeight / 2;
		}

		position.left += myOffset[ 0 ];
		position.top += myOffset[ 1 ];

		// if the browser doesn't support fractions, then round for consistent results
		if ( !$.support.offsetFractions ) {
			position.left = round( position.left );
			position.top = round( position.top );
		}

		collisionPosition = {
			marginLeft: marginLeft,
			marginTop: marginTop
		};

		$.each( [ "left", "top" ], function( i, dir ) {
			if ( $.ui.position[ collision[ i ] ] ) {
				$.ui.position[ collision[ i ] ][ dir ]( position, {
					targetWidth: targetWidth,
					targetHeight: targetHeight,
					elemWidth: elemWidth,
					elemHeight: elemHeight,
					collisionPosition: collisionPosition,
					collisionWidth: collisionWidth,
					collisionHeight: collisionHeight,
					offset: [ atOffset[ 0 ] + myOffset[ 0 ], atOffset [ 1 ] + myOffset[ 1 ] ],
					my: options.my,
					at: options.at,
					within: within,
					elem : elem
				});
			}
		});

		if ( options.using ) {
			// adds feedback as second argument to using callback, if present
			using = function( props ) {
				var left = targetOffset.left - position.left,
					right = left + targetWidth - elemWidth,
					top = targetOffset.top - position.top,
					bottom = top + targetHeight - elemHeight,
					feedback = {
						target: {
							element: target,
							left: targetOffset.left,
							top: targetOffset.top,
							width: targetWidth,
							height: targetHeight
						},
						element: {
							element: elem,
							left: position.left,
							top: position.top,
							width: elemWidth,
							height: elemHeight
						},
						horizontal: right < 0 ? "left" : left > 0 ? "right" : "center",
						vertical: bottom < 0 ? "top" : top > 0 ? "bottom" : "middle"
					};
				if ( targetWidth < elemWidth && abs( left + right ) < targetWidth ) {
					feedback.horizontal = "center";
				}
				if ( targetHeight < elemHeight && abs( top + bottom ) < targetHeight ) {
					feedback.vertical = "middle";
				}
				if ( max( abs( left ), abs( right ) ) > max( abs( top ), abs( bottom ) ) ) {
					feedback.important = "horizontal";
				} else {
					feedback.important = "vertical";
				}
				options.using.call( this, props, feedback );
			};
		}

		elem.offset( $.extend( position, { using: using } ) );
	});
};

$.ui.position = {
	fit: {
		left: function( position, data ) {
			var within = data.within,
				withinOffset = within.isWindow ? within.scrollLeft : within.offset.left,
				outerWidth = within.width,
				collisionPosLeft = position.left - data.collisionPosition.marginLeft,
				overLeft = withinOffset - collisionPosLeft,
				overRight = collisionPosLeft + data.collisionWidth - outerWidth - withinOffset,
				newOverRight;

			// element is wider than within
			if ( data.collisionWidth > outerWidth ) {
				// element is initially over the left side of within
				if ( overLeft > 0 && overRight <= 0 ) {
					newOverRight = position.left + overLeft + data.collisionWidth - outerWidth - withinOffset;
					position.left += overLeft - newOverRight;
				// element is initially over right side of within
				} else if ( overRight > 0 && overLeft <= 0 ) {
					position.left = withinOffset;
				// element is initially over both left and right sides of within
				} else {
					if ( overLeft > overRight ) {
						position.left = withinOffset + outerWidth - data.collisionWidth;
					} else {
						position.left = withinOffset;
					}
				}
			// too far left -> align with left edge
			} else if ( overLeft > 0 ) {
				position.left += overLeft;
			// too far right -> align with right edge
			} else if ( overRight > 0 ) {
				position.left -= overRight;
			// adjust based on position and margin
			} else {
				position.left = max( position.left - collisionPosLeft, position.left );
			}
		},
		top: function( position, data ) {
			var within = data.within,
				withinOffset = within.isWindow ? within.scrollTop : within.offset.top,
				outerHeight = data.within.height,
				collisionPosTop = position.top - data.collisionPosition.marginTop,
				overTop = withinOffset - collisionPosTop,
				overBottom = collisionPosTop + data.collisionHeight - outerHeight - withinOffset,
				newOverBottom;

			// element is taller than within
			if ( data.collisionHeight > outerHeight ) {
				// element is initially over the top of within
				if ( overTop > 0 && overBottom <= 0 ) {
					newOverBottom = position.top + overTop + data.collisionHeight - outerHeight - withinOffset;
					position.top += overTop - newOverBottom;
				// element is initially over bottom of within
				} else if ( overBottom > 0 && overTop <= 0 ) {
					position.top = withinOffset;
				// element is initially over both top and bottom of within
				} else {
					if ( overTop > overBottom ) {
						position.top = withinOffset + outerHeight - data.collisionHeight;
					} else {
						position.top = withinOffset;
					}
				}
			// too far up -> align with top
			} else if ( overTop > 0 ) {
				position.top += overTop;
			// too far down -> align with bottom edge
			} else if ( overBottom > 0 ) {
				position.top -= overBottom;
			// adjust based on position and margin
			} else {
				position.top = max( position.top - collisionPosTop, position.top );
			}
		}
	},
	flip: {
		left: function( position, data ) {
			var within = data.within,
				withinOffset = within.offset.left + within.scrollLeft,
				outerWidth = within.width,
				offsetLeft = within.isWindow ? within.scrollLeft : within.offset.left,
				collisionPosLeft = position.left - data.collisionPosition.marginLeft,
				overLeft = collisionPosLeft - offsetLeft,
				overRight = collisionPosLeft + data.collisionWidth - outerWidth - offsetLeft,
				myOffset = data.my[ 0 ] === "left" ?
					-data.elemWidth :
					data.my[ 0 ] === "right" ?
						data.elemWidth :
						0,
				atOffset = data.at[ 0 ] === "left" ?
					data.targetWidth :
					data.at[ 0 ] === "right" ?
						-data.targetWidth :
						0,
				offset = -2 * data.offset[ 0 ],
				newOverRight,
				newOverLeft;

			if ( overLeft < 0 ) {
				newOverRight = position.left + myOffset + atOffset + offset + data.collisionWidth - outerWidth - withinOffset;
				if ( newOverRight < 0 || newOverRight < abs( overLeft ) ) {
					position.left += myOffset + atOffset + offset;
				}
			}
			else if ( overRight > 0 ) {
				newOverLeft = position.left - data.collisionPosition.marginLeft + myOffset + atOffset + offset - offsetLeft;
				if ( newOverLeft > 0 || abs( newOverLeft ) < overRight ) {
					position.left += myOffset + atOffset + offset;
				}
			}
		},
		top: function( position, data ) {
			var within = data.within,
				withinOffset = within.offset.top + within.scrollTop,
				outerHeight = within.height,
				offsetTop = within.isWindow ? within.scrollTop : within.offset.top,
				collisionPosTop = position.top - data.collisionPosition.marginTop,
				overTop = collisionPosTop - offsetTop,
				overBottom = collisionPosTop + data.collisionHeight - outerHeight - offsetTop,
				top = data.my[ 1 ] === "top",
				myOffset = top ?
					-data.elemHeight :
					data.my[ 1 ] === "bottom" ?
						data.elemHeight :
						0,
				atOffset = data.at[ 1 ] === "top" ?
					data.targetHeight :
					data.at[ 1 ] === "bottom" ?
						-data.targetHeight :
						0,
				offset = -2 * data.offset[ 1 ],
				newOverTop,
				newOverBottom;
			if ( overTop < 0 ) {
				newOverBottom = position.top + myOffset + atOffset + offset + data.collisionHeight - outerHeight - withinOffset;
				if ( ( position.top + myOffset + atOffset + offset) > overTop && ( newOverBottom < 0 || newOverBottom < abs( overTop ) ) ) {
					position.top += myOffset + atOffset + offset;
				}
			}
			else if ( overBottom > 0 ) {
				newOverTop = position.top -  data.collisionPosition.marginTop + myOffset + atOffset + offset - offsetTop;
				if ( ( position.top + myOffset + atOffset + offset) > overBottom && ( newOverTop > 0 || abs( newOverTop ) < overBottom ) ) {
					position.top += myOffset + atOffset + offset;
				}
			}
		}
	},
	flipfit: {
		left: function() {
			$.ui.position.flip.left.apply( this, arguments );
			$.ui.position.fit.left.apply( this, arguments );
		},
		top: function() {
			$.ui.position.flip.top.apply( this, arguments );
			$.ui.position.fit.top.apply( this, arguments );
		}
	}
};

// fraction support test
(function () {
	var testElement, testElementParent, testElementStyle, offsetLeft, i,
		body = document.getElementsByTagName( "body" )[ 0 ],
		div = document.createElement( "div" );

	//Create a "fake body" for testing based on method used in jQuery.support
	testElement = document.createElement( body ? "div" : "body" );
	testElementStyle = {
		visibility: "hidden",
		width: 0,
		height: 0,
		border: 0,
		margin: 0,
		background: "none"
	};
	if ( body ) {
		$.extend( testElementStyle, {
			position: "absolute",
			left: "-1000px",
			top: "-1000px"
		});
	}
	for ( i in testElementStyle ) {
		testElement.style[ i ] = testElementStyle[ i ];
	}
	testElement.appendChild( div );
	testElementParent = body || document.documentElement;
	testElementParent.insertBefore( testElement, testElementParent.firstChild );

	div.style.cssText = "position: absolute; left: 10.7432222px;";

	offsetLeft = $( div ).offset().left;
	$.support.offsetFractions = offsetLeft > 10 && offsetLeft < 11;

	testElement.innerHTML = "";
	testElementParent.removeChild( testElement );
})();

}( jQuery ) );




function loadAlertsBuckets() {
	$(".alertsList").each( function(n) {
        var elBucket = $(this).parents(".bucket-process").attr("id");
        var elDisplayControl = elBucket + '_alertsList_' + n;
        $(this).attr("id", elDisplayControl);

        var scoreCardInput = $(this).attr("scorecard");
        var granularityInput = $(this).attr("granularity");

        var parameters = {
            'controlType': 'alertsList',
            'scorecard': scoreCardInput,
            'granularity': granularityInput
        };

        var alertsRecords = getMetricsRecordsForScorecardAndGranularity(parameters);
        alertsRecords = sortAlertsRecords(alertsRecords);
        drawAlertsList(elDisplayControl, alertsRecords, n);
    });
}


function getMetricsRecordsForScorecardAndGranularity(parameters) {
    try {
        var result = Ab.workflow.Workflow.callMethod('AbCommonResources-MetricsService-getTrendValuesForScorecard', parameters.scorecard, parameters.granularity);

        if (Ab.workflow.Workflow.sessionTimeoutDetected) {
            result = false;
        }
        else if (result.data) {
            result = result.data;
        }
        else {
            result = false;
        }
    }
    catch (e) {
        Workflow.handleError(e);
        result = false;
    }

    return result;
}



function sortAlertsRecords(unsortedRecords) {
    var sortedRecords = [];
    var mediumRecords = [];
    var lowRecords = [];

    for (var rowNum = 0, record; record = unsortedRecords[rowNum]; rowNum++) {
        if (!record.stoplightColor){ continue;}

        var alertBulletColor = record.stoplightColor;
        if (alertBulletColor.toLowerCase()  === 'red') {
            sortedRecords.push(unsortedRecords[rowNum]);
        }
        else if (alertBulletColor === 'yellow') {
            mediumRecords.push(unsortedRecords[rowNum]);
        }
        else {
            lowRecords.push(unsortedRecords[rowNum]);
        }
    }

    for (var i = 0, mediumRecord; mediumRecord = mediumRecords[i]; i++) {
        sortedRecords.push(mediumRecord);
    }

    for (var j = 0, lowRecord; lowRecord = lowRecords[j]; j++) {
        sortedRecords.push(lowRecord);
    }

    return sortedRecords;
}


function drawAlertsList(displayControlId, alertsRecords, n) {
    var displayControlElement = $("#" + displayControlId);
    displayControlElement.empty();

    var pageIndex = 1;
    var pageIdPrefix = displayControlId + '_page';
    var rowIdPrefix = displayControlId.substring(displayControlId.indexOf('_') + 1);
    rowIdPrefix = rowIdPrefix.substring(rowIdPrefix.indexOf('_') + 1);

    var itemsHtmlString = formAlertsItemsString(pageIdPrefix, rowIdPrefix, pageIndex, alertsRecords);
    displayControlElement.append(itemsHtmlString);

    var allowableBucketContentHeight = getBucketContentHeight(displayControlId);
    var unalteredHeight = displayControlElement.height();
    var removeCount = 0;

    if (unalteredHeight > allowableBucketContentHeight) {
        for (var i = alertsRecords.length - 1; unalteredHeight > allowableBucketContentHeight; i--) {
            var findSelector = '#'+ rowIdPrefix + pageIndex + '_row_' + i;
            $(findSelector).remove();
            removeCount++;
            unalteredHeight = displayControlElement.height();
        }

        var pageSelector = '#'+ pageIdPrefix + pageIndex;
        var pageRowHeight = getPageRowHeight(pageSelector);
        $(pageSelector).wrap('<div class="scrollable-wrapper scrollable-wrapper-' + pageRowHeight + '"></div>');
        $(pageSelector).wrap('<div class="scrollable-root scrollable-' + pageRowHeight + '" id="' + rowIdPrefix + //pageIndex +
            '_scroller"></div>');
        $(pageSelector).wrap('<div class="items"></div>');

       appendAdditionalAlertsPages(allowableBucketContentHeight, pageIdPrefix, rowIdPrefix, pageIndex, alertsRecords, removeCount);

        displayControlElement.append('<div class="navi" id="alertsNavi_' + n + '"></div>');
        $('#' + rowIdPrefix + '_scroller').scrollable({vertical: false, mousewheel: true}).navigator("#alertsNavi_" + n);
    }
}


function formAlertsItemsString(pageIdPrefix, rowIdPrefix, pageIndex, alertsRecords) {
    var itemsHtmlString = '<ol id="' + pageIdPrefix + pageIndex +'" class="alert-items">';
    var emptyListLength = itemsHtmlString.length;
    var recordCount = alertsRecords.length;
    var record;

    for (var rowNum = 0; rowNum < recordCount; rowNum++) {
        record = alertsRecords[rowNum];
        if (!record.metricTitle) { continue; }

        var alertText = record.metricTitle;
        var alertsCount = record.metricValue;
        
        var alertBulletColor = record.stoplightColor.toLowerCase();
        var alertBulletClass = 'alert-low';
        if (alertBulletColor === 'red') {
            alertBulletClass = 'alert-high';
        }
        else if (alertBulletColor === 'yellow') {
            alertBulletClass = 'alert-medium';
        }

        var drilldownAttribute = record.drillDownView;
        drilldownAttribute = valueExists(drilldownAttribute) ? ' rel="' + drilldownAttribute + '" onMouseUp="openAlertDrilldown(this, event);"' : '';

        itemsHtmlString += '<li id="' + rowIdPrefix + pageIndex +'_row_' + rowNum + '" class="' + alertBulletClass + '"' +
            drilldownAttribute + '>';
        if (drilldownAttribute.length > 0) {
            itemsHtmlString += '<a>';
        }
        itemsHtmlString += '<span class="alert-description">[' + alertsCount + '] ' + alertText + '</span>';
        if (drilldownAttribute.length > 0) {
            itemsHtmlString += '</a>';
        }
        itemsHtmlString += '</li>';
    }

    if (emptyListLength === itemsHtmlString.length) {
        itemsHtmlString += '<li >' + getLocalizedString(pageNavStrings.z_PAGENAV_BUCKET_NODATA) +' </li>';
    }

    itemsHtmlString += '</ol>';

    return itemsHtmlString;
}


function openAlertDrilldown(elem, event) {
    var popupViewNameSuffix = '-popup.axvw';

    if (signOutOnTimeOut()) { return false; }

    event = event || window.event;
    if (event) {
        elem = event.target || event.srcElement;
    }
    if (event.button === 2 || event.which === 3)
    {
        return false;
    }

    var drilldownViewFile = $(elem).attr('rel');
    if (!drilldownViewFile) {
        drilldownViewFile = $(elem).parents('li').attr('rel');
    }
    if (drilldownViewFile) {
        var drilldownSource = PageNavUserInfo.webAppContextPath + drilldownViewFile;

        // open in modal dialog
        if (drilldownViewFile.indexOf(popupViewNameSuffix) === drilldownViewFile.length - popupViewNameSuffix.length) {
            // redesign for Bali4
            var newDialog = '<div id="alert-drilldown" style="padding:0;">' +
                '<iframe id="alert-drilldown-frame" src="' + drilldownSource + '" width="99%" height="99%"></iframe>' +
                '</div>';

            var windowElement = $(window);
            var drilldown = $(newDialog).dialog({
                autoOpen: false,
                height: windowElement.height() - 12,
                width: windowElement.width() - 14,
                modal: true,
                open: function () {
                    $(".ui-dialog-titlebar-close").show();

                    var drilldownFrame = $("#alert-drilldown-frame")[0];
                    var drilldownTitleElemText = $(drilldownFrame).contents().find("#viewToolbar_title").text();
                    $(this).dialog("option", "title", drilldownTitleElemText);
                },
                close: function () {
                    $("#alert-drilldown").remove();
                }
            });

            drilldown.dialog('open');
        }
        // open in new window (tab or browser window)
        else {
            window.open(drilldownSource);
        }
    }
}






function pageTallProcessBuckets(){
    $(".bucket-process > .bucket-wrapper").each( function(n) {
        var allowableUnpagedBucketHeight = getUnpagedBucketHeight(this, 'process');
        var allowableBucketHeight = allowableUnpagedBucketHeight - 25;
        var measuredBucketHeight = $(this).height();

        var taskGroupParentElements = $(this).children('ol.ptask-labels');

        if (measuredBucketHeight > allowableUnpagedBucketHeight && taskGroupParentElements.length > 0) {
            var taskGroupLabelElements = $(taskGroupParentElements).children('li');
            var taskGroupTaskElements = $(taskGroupParentElements).children('ol.process-tasks');

            var hasTaskGroupLabels = taskGroupTaskElements.length > 1;
            var breakTaskGroups = mustTaskGroupsBeDivided(this, allowableBucketHeight, taskGroupLabelElements, taskGroupTaskElements);

            var removedTaskElements = removePTaskElementsToCorrectHeight(allowableBucketHeight, taskGroupLabelElements, taskGroupTaskElements, hasTaskGroupLabels, breakTaskGroups, this);

            // add page wrappers
            var pageRootElement = taskGroupParentElements.get(0);
            var pageRowHeight = getPageRowHeight(pageRootElement);
            var bucketId = $(this).attr('id');
            $(pageRootElement).wrap('<div class="scrollable-wrapper scrollable-wrapper-' + pageRowHeight + '"></div>');
            $(pageRootElement).wrap('<div class="scrollable-root scrollable-' + pageRowHeight + '" id="' + bucketId + n + '_scroller"></div>');
            $(pageRootElement).wrap('<div class="items"></div>');

            appendAdditionalTaskPages(allowableBucketHeight, pageRootElement, bucketId, n, 1, hasTaskGroupLabels, breakTaskGroups, removedTaskElements);

            // add navi element & initialize
            $("#" + bucketId).append('<div class="navi" id="pTaskNavi_' + n + '"></div>');
            $("#" + bucketId + n + '_scroller').scrollable({vertical: false, mousewheel: true}).navigator("#pTaskNavi_" + n);
        }
    });
}


function getUnpagedBucketHeight(bucketWrapper, bucketType) {
    var wrapperParentElement = $(bucketWrapper).parent(".bucket-" + bucketType);
    var bucketHeight = $(wrapperParentElement).height();
    var bucketTop = $(bucketWrapper).css('padding-top');
    bucketTop = parseInt(bucketTop);
    var bucketBottom = $(bucketWrapper).css('padding-bottom');
    bucketBottom = parseInt(bucketBottom);

    var processTitleElem = $(bucketWrapper).siblings("." + bucketType + "-title").get(0);
    if (!processTitleElem) {
        processTitleElem = $(bucketWrapper).siblings("." + bucketType + "s-process-title").get(0);
    }
    var bucketTitleHeight = $(processTitleElem).height();
    var bucketTitleTop = $(processTitleElem).css('padding-top');
    bucketTitleTop = parseInt(bucketTitleTop);
    var bucketTitleBottom = $(processTitleElem).css('padding-bottom');
    bucketTitleBottom = parseInt(bucketTitleBottom);

    return bucketHeight - bucketTitleHeight - bucketTop - bucketBottom - bucketTitleTop - bucketTitleBottom;
}


function mustTaskGroupsBeDivided(bucketWrapper, allowableBucketHeight, taskGroupLabelElements, taskGroupTaskElements) {
    var breakTaskGroups = false;
    if (taskGroupTaskElements.length > 1) {
        var bucketMarginTop = $(bucketWrapper).css('padding-top');
        var taskGroupHeights = getTaskGroupHeights(parseInt(bucketMarginTop), taskGroupLabelElements, taskGroupTaskElements);
        var taskGroupCount = taskGroupHeights.length;

        for (var j = 0; j < taskGroupCount; j++) {
            if (taskGroupHeights[j] > allowableBucketHeight) {
                breakTaskGroups = true;
                break;
            }
        }
    }

    return breakTaskGroups;
}


function getTaskGroupHeights(topMargin, taskGroupLabelElements,taskGroupTaskElements) {
    var groupHeights = [];
    var taskGroupLabelCount = taskGroupLabelElements.length;
    for (var i = 0; i < taskGroupLabelCount; i++) {
        // simulated wrapper of task group in paged scroller
        $('body').append('<div id="test_paging_scroller" class="page bucket-process" style="position:absolute;visibility:hidden">' +
            '<div class="scrollable-full-height">' +
            '<ol id="test_paging_insert_root" class="ptask-labels">' +
            '</ol></div></div>');
        var testPageInsert = $('#test_paging_insert_root');

        // append a clone of the task group label and the tasks in the group
        testPageInsert.append($($(taskGroupLabelElements).get(i)).clone(false));
        testPageInsert.append($($(taskGroupTaskElements).get(i)).clone(false));

        //var ht = testPageInsert.height();
        groupHeights.push(topMargin + testPageInsert.height());

        $('#test_paging_scroller').remove();
    }

    return groupHeights;
}


function removePTaskElementsToCorrectHeight(allowableBucketHeight, taskGroupLabelElements, taskGroupTaskElements, hasTaskGroupLabels, breakTaskGroups, pageHeightElement) {
    var measuredBucketHeight = $(pageHeightElement).height();
    var removedTaskElements = [];
    //var elementToRemove;
    var taskElements;

    // remove & measure loop
    if (hasTaskGroupLabels && breakTaskGroups) {
        for (var i = taskGroupTaskElements.length - 1; i >= 0 && measuredBucketHeight > allowableBucketHeight; i--) {
            var taskGroupTasks = taskGroupTaskElements.get(i);
            taskElements = $(taskGroupTasks).children('li');
            for (var j = taskElements.length - 1; j >= 0 && measuredBucketHeight > allowableBucketHeight; j--) {
                measuredBucketHeight = detachElementAndRemeasure(taskElements, j, removedTaskElements, pageHeightElement);
            }
            taskElements = $(taskGroupTasks).children('li');
            if (taskElements.length === 0) {
                detachElementAndRemeasure(taskGroupTaskElements, i, removedTaskElements, pageHeightElement);
                detachElementAndRemeasure(taskGroupLabelElements, i, removedTaskElements, pageHeightElement);
            }
            measuredBucketHeight = $(pageHeightElement).height();
        }
    }
    else if (hasTaskGroupLabels) {
        for (var k = taskGroupTaskElements.length - 1; k >= 0 && measuredBucketHeight > allowableBucketHeight; k--) {
            detachElementAndRemeasure(taskGroupTaskElements, k, removedTaskElements, pageHeightElement);
            measuredBucketHeight = detachElementAndRemeasure(taskGroupLabelElements, k, removedTaskElements, pageHeightElement);
        }

        if (removedTaskElements.length > 0 && removedTaskElements[0].nodeName === 'LI' && removedTaskElements[0].innerHTML === '&nbsp;') {
            removedTaskElements = removedTaskElements.slice(1);
        }
     }
    else {
        taskElements = $(taskGroupTaskElements).children('li');
        for (var m = taskElements.length - 1; m >= 0 && measuredBucketHeight > allowableBucketHeight; m--) {
            measuredBucketHeight = detachElementAndRemeasure(taskElements, m, removedTaskElements, pageHeightElement);
        }
    }
    return removedTaskElements;
}


function detachElementAndRemeasure(elementGroup, index, removedTaskElements, pageHeightRoot) {
    var elementToRemove = elementGroup.get(index);
    if (elementToRemove) {
        removedTaskElements.unshift($(elementToRemove).detach().get(0));
    }

    return $(pageHeightRoot).height();
}


function appendAdditionalTaskPages(allowableBucketHeight, pageRootElement, bucketId, n, pageIndex, hasTaskGroupLabels, breakTaskGroups, removedTaskElements) {
    var rootId = bucketId + n + '_' + pageIndex + '_insert_root';
    $(pageRootElement).after('<ol class="ptask-labels" id="' + rootId + '"></ol>');
    var insertRoot = $("#" + rootId );

    if (hasTaskGroupLabels && breakTaskGroups) {
        appendAdditionalTaskPagesBrokenTaskGroups(rootId, removedTaskElements);
    }
    else {
        if (!hasTaskGroupLabels) {
            insertRoot.append('<li></li><ol class="process-tasks"></ol>');
        }

        var removedTaskParent = hasTaskGroupLabels ? insertRoot : insertRoot.find('ol');
        var removedTaskElementCount = removedTaskElements.length;
        for (var i = 0; i < removedTaskElementCount; i++) {
            removedTaskParent.append(removedTaskElements[i]);
        }
    }

    var measuredBucketHeight = insertRoot.height();
    if (measuredBucketHeight > allowableBucketHeight) {
        var taskGroupTaskElements = insertRoot.children('ol.process-tasks');
        var taskGroupLabelElements = insertRoot.children('li');

        var nextRemovedTaskElements = removePTaskElementsToCorrectHeight(allowableBucketHeight, taskGroupLabelElements, taskGroupTaskElements,
            hasTaskGroupLabels, breakTaskGroups, insertRoot);
        appendAdditionalTaskPages(allowableBucketHeight, insertRoot, bucketId, n, pageIndex + 1, hasTaskGroupLabels, breakTaskGroups, nextRemovedTaskElements);
    }
}


function appendAdditionalTaskPagesBrokenTaskGroups(rootId, removedTaskElements) {
    if ($(removedTaskElements[0]).children('a').length > 0) {
        $("#" + rootId).append('<li></li><ol class="process-tasks"></ol>');
    }
    var removedTaskElementCount = removedTaskElements.length;
    for (var i = 0; i < removedTaskElementCount; i++) {
        if ($(removedTaskElements[i]).children('a').length > 0) {
            $("#" + rootId+ ' > ol:last').append(removedTaskElements[i]);
        }
        else {
            $("#" + rootId ).append(removedTaskElements[i]);
        }
    }
}


function appendAdditionalAlertsPages(allowableHeight, pageIdPrefix, rowIdPrefix, pageIndex, alertsRecords, removeCount) {
    var additionalPageIndex = pageIndex + 1;
    var pageRootSelector = '#'+ pageIdPrefix + pageIndex;

    var additionalPageRecords = [];
    for (var j = removeCount; j > 0; j--) {
        additionalPageRecords.push(alertsRecords[alertsRecords.length - j]);
    }
    var itemsHtmlString = formAlertsItemsString(pageIdPrefix, rowIdPrefix, additionalPageIndex, additionalPageRecords);
    $(itemsHtmlString).insertAfter(pageRootSelector);

    var additionalPageSelector = "#" + pageIdPrefix + (additionalPageIndex);
    var unalteredHeight = $(additionalPageSelector).height();

    if (unalteredHeight > allowableHeight) {
        removeCount = 0;

        for (var i = additionalPageRecords.length - 1; unalteredHeight > allowableHeight; i--) {
            var findSelector = '#'+ rowIdPrefix + additionalPageIndex + '_row_' + i;
            $(findSelector).remove();
            removeCount++;
            unalteredHeight = $(additionalPageSelector).height();
        }

        appendAdditionalAlertsPages(allowableHeight, pageIdPrefix, rowIdPrefix, additionalPageIndex, additionalPageRecords, removeCount);
    }
}


function getBucketContentHeight(controlId) {
    var controlElement = $("#" + controlId);
    var bucketHeight = controlElement.parents('.bucket-process').height();
    var bucketTitleHeight = controlElement.parents('.bucket-process').children('.process-title').height();
    return bucketHeight - bucketTitleHeight - 55;
}


function getPageRowHeight(startingSelector) {
    var pageRowHeight = 'half-height';
    var rowParentClassAttr = $(startingSelector).parents(".page-row").first().attr("class");
    var rowParentClasses = rowParentClassAttr.split(' ');
    var rowParentClassCount = rowParentClasses.length;

    for (var i = 0; i < rowParentClassCount; i++) {
        var testString = $.trim(rowParentClasses[i]);
        if (testString.indexOf('height') === (testString.length - 6)) {
            pageRowHeight = testString;
            break;
        }
    }

    return pageRowHeight;
}




function pageTallPTaskBuckets(){
    $('#applicationsProcessView').find(".bucket-application").each( function(n) {
        //var bucketHeight = $(this).height();
        var bucketWrapper = $(this).find(".bucket-wrapper");

        var allowableUnpagedBucketHeight = getUnpagedBucketHeight(bucketWrapper, 'application');
        var allowableBucketHeight = allowableUnpagedBucketHeight - 25;
        var measuredBucketHeight = $(bucketWrapper).height();

        var taskGroupParentElements = $(bucketWrapper).children('ol.ptask-labels');

        if (measuredBucketHeight > allowableUnpagedBucketHeight && taskGroupParentElements.length > 0) {
            var taskGroupTaskElements = $(taskGroupParentElements).children('ol.process-tasks');
            var taskGroupLabelElements = $(taskGroupParentElements).children('li');

            var hasTaskGroupLabels = taskGroupTaskElements.length > 1;
            var breakTaskGroups = mustTaskGroupsBeDivided(bucketWrapper, allowableBucketHeight, taskGroupLabelElements, taskGroupTaskElements);

            var removedTaskElements = removePTaskElementsToCorrectHeight(allowableBucketHeight, taskGroupLabelElements, taskGroupTaskElements, hasTaskGroupLabels, breakTaskGroups, bucketWrapper);

            var pageRootElement = taskGroupParentElements.get(0);
            var pageRowHeight = getPageRowHeight(pageRootElement);
            var bucketId = $(bucketWrapper).attr('id');

            // add page wrappers
            $(pageRootElement).wrap('<div class="scrollable-wrapper scrollable-wrapper-' + pageRowHeight + '"></div>');
            $(pageRootElement).wrap('<div class="scrollable-root scrollable-' + pageRowHeight + '" id="' + bucketId + n + '_scroller"></div>');
            $(pageRootElement).wrap('<div class="items"></div>');

            appendAdditionalTaskPages(allowableBucketHeight, pageRootElement, bucketId, n, 1, hasTaskGroupLabels, breakTaskGroups, removedTaskElements);

            // add navi element & initialize
            $("#" + bucketId).append('<div class="navi" id="appTaskNavi_' + bucketId + n + '"></div>');
            $("#" + bucketId + n + '_scroller').scrollable({circular: false, vertical: false, mousewheel: true}).navigator("#appTaskNavi_" + bucketId + n);
        }
    });
}


function pageTallApplicationBuckets(){
    $('#applicationsTabView').find(".bucket-application").each( function(n) {
        var bucketHeight = $(this).height();
        var productTitleElem = $(this).find(".product-title").get(0);
        var bucketWrapper = $(this).find(".bucket-wrapper");
        var bucketTitleHeight = $(productTitleElem).height();
        var allowableUnpagedBucketHeight = bucketHeight - bucketTitleHeight - 20;
        var allowableBucketHeight = bucketHeight - bucketTitleHeight - 40;
        var measuredBucketHeight = $(bucketWrapper).height();
        var productActivityElements = $(bucketWrapper).children('ol.app-processes');

        if (measuredBucketHeight > allowableUnpagedBucketHeight && productActivityElements.length > 0) {
            //alert('pTAB ' + n + ' mBH = ' + measuredBucketHeight + 'aBH = ' + allowableUnpagedBucketHeight);
            var pageRootElement = productActivityElements.get(0);
            var activityElements = $(productActivityElements).children('li');

            var removedActivityElements = removeActivityElementsToCorrectHeight(allowableBucketHeight, activityElements, bucketWrapper);

            var pageRowHeight = getPageRowHeight(pageRootElement);
            var bucketId = $(bucketWrapper).attr('id');

            // add page wrappers
            $(pageRootElement).wrap('<div class="scrollable-wrapper scrollable-wrapper-' + pageRowHeight + '"></div>');
            $(pageRootElement).wrap('<div class="scrollable-root scrollable-' + pageRowHeight + '" id="' + bucketId + n + '_scroller"></div>');
            $(pageRootElement).wrap('<div class="items"></div>');

            appendAdditionalActivityPages(allowableBucketHeight, pageRootElement, bucketId, n, 0, removedActivityElements);

            // add navi element & initialize
            $("#" + bucketId).append('<div class="navi" id="activityNavi_' + n + '"></div>');
            $("#" + bucketId + n + '_scroller').scrollable({circular: false, vertical: false, mousewheel: true}).navigator("#activityNavi_" + n);
        }
    });
}


function removeActivityElementsToCorrectHeight(allowableBucketHeight, activityElements, pageHeightElement) {
    var measuredBucketHeight = $(pageHeightElement).height();
    var removedActivityElements = [];

    // remove & measure loop
    //var taskElements = $(taskGroupTaskElements).children('li');
    for (var i = activityElements.length; i > 0 && measuredBucketHeight > allowableBucketHeight; i--) {
        var elementToRemove = activityElements.get(i - 1);
        removedActivityElements.unshift($(elementToRemove).detach().get(0));
        measuredBucketHeight = $(pageHeightElement).height();
    }

    return removedActivityElements;
}


function appendAdditionalActivityPages(allowableBucketHeight, pageRootElement, bucketId, n, pageIndex, removedActivityElements) {
    var rootId = bucketId + n + '_' + pageIndex + '_insert_root';

    $(pageRootElement).after('<ol class="app-processes" id="' + rootId + '"></ol>');
    var insertRoot = $("#" + rootId);
    var removedActivityCount = removedActivityElements.length;

    for (var i = 0; i < removedActivityCount; i++) {
        insertRoot.append(removedActivityElements[i]);
    }

    var measuredBucketHeight = insertRoot.height();
    if (measuredBucketHeight > allowableBucketHeight) {
        var activityElements = insertRoot.children('li');
        var nextRemovedTaskElements = removeActivityElementsToCorrectHeight(allowableBucketHeight, activityElements, insertRoot);
        appendAdditionalActivityPages(allowableBucketHeight, insertRoot, bucketId, n, pageIndex + 1, nextRemovedTaskElements);
    }
}


function modifyProductTitleDisplay() {
    var bucketAppHeading = $('.bucket-application h2');
    var bucketApplicationHeight = parseInt(bucketAppHeading.css('height'));
    var bucketApplicationPaddingLeft = parseInt(bucketAppHeading.css('padding-left'));
    var imageLeft = parseInt($('.application-title-icon').css('left'));
    var bucketsPerRow = 5;
    var appBucketRows = [[],[],[]];
    var tallTitleRows = [false, false, false];

    $('h2.product-title').each( function(n) {
        var target = $(this).children('span.product-title-text');
        var title = $(target).text();
        var titleWidth = $(target).width();
        var titleHeight = $(target).height();

        // when title width overlaps image, fade the image
        if ((titleWidth + bucketApplicationPaddingLeft) >= imageLeft) {
            $(this).children('.application-title-icon').css('opacity', 0.45);
        }

        // set the row's 'title is too tall' flag when text height is moe than standard
        if (titleHeight > bucketApplicationHeight) {
            tallTitleRows[Math.floor(n / bucketsPerRow)] = true;
        }

        // hold the title elements by row in case they need to be taller
        appBucketRows[Math.floor(n / bucketsPerRow)].push(this);
    });

    // make any row with tallTitleRows[i] == true taller to allow title display and even out heights.
    var tallTitleRowCount = tallTitleRows.length;
    for (var i = 0; i < tallTitleRowCount; i++) {
        if (tallTitleRows[i]) {
            for (var j = 0; j < appBucketRows[i].length; j++) {
                var titleElem = appBucketRows[i][j];
                $(titleElem).css('height', '65px');
            }
        }
    }
}


function modifyProcessTitleDisplay() {
    var bucketTitleHeight = parseInt($('.bucket-application h2').css('height'));
    var bucketsPerRow = 5;
    var processBucketRows = [[], [], [], [], []];
    var tallTitleRows = [false, false, false, false, false];

    $('h2.applications-process-title').each( function(n) {
        var target = $(this).children('span.process-title-text');
        ///var title = $(target).text();

        // set the row's 'title is too tall' flag when text height is moe than standard
        if ($(target).height() > bucketTitleHeight) {
            tallTitleRows[Math.floor(n / bucketsPerRow)] = true;
        }

        // hold the title elements by row in case they need to be taller
        processBucketRows[Math.floor(n / bucketsPerRow)].push(this);
    });

    // make any row with tallTitleRows[i] == true taller to allow title display and even out heights.
    var tallTitleRowCount = tallTitleRows.length;
    for (var i = 0; i < tallTitleRowCount; i++) {
        if (tallTitleRows[i]) {
            for (var j = 0; j < processBucketRows[i].length; j++) {
                var titleElem = processBucketRows[i][j];
                $(titleElem).css('height', '65px');
                ///var scrollableWrapper = $(titleElem).parent('div.bucket-application').children('div.bucket-wrapper');
                var bucketWrapper = $(titleElem).siblings('div.bucket-wrapper');
                if (bucketWrapper && bucketWrapper.length > 0) {
                    $(bucketWrapper).css('padding-top', '2px');
                }
            }
        }
    }
}



function bindTaskContextMenu() {
    // bucket and divider right-click context menus
    $.contextMenu({
        selector: 'a.ui-tabs-anchor,a[rel$="Task"],h2.product-title,h2.application-title,h3.application-title,a.application-title,h2.process-title,h2.applications-process-title',
        // this callback is executed every time the menu is shown
        // its results are destroyed every time the menu is hidden
        // e is the original contextmenu event, containing e.currentTarget, e.pageX, e.pageY etc.
        build: function($trigger, e) {
            var contextMenu;
            var rel = $(e.currentTarget).attr('rel');
            var cssClass = e.currentTarget.className;
            //var targetId = $(e.currentTarget).attr('id');

            if (rel === 'eTask' || rel === 'pTask') {
                var taskName = e.currentTarget.innerHTML;
                if (taskName.indexOf('<img') > 0) {
                    taskName = $.trim(taskName.substring(0, taskName.indexOf('<img')));
                }
                contextMenu = returnTaskMenu(e, taskName, e.currentTarget.href);
            }
            else if (isFavoritesProcessHeader(e.currentTarget)) {
                showFavoritesDialog(e.currentTarget);
                contextMenu = false;
            }
            // is app-specific bucket
            else if ($(e.currentTarget).siblings('.app-specific-bucket').length > 0) {
                contextMenu = returnAppSpecificMenu();
            }
            else if (cssClass === 'process-title' || cssClass === 'applications-process-title'){
                contextMenu = returnProcessMenu(e);
            }
            else if (cssClass === 'application-title') {
                contextMenu = returnActivityOrProductMenu(e);
            }
            else if (cssClass === 'product-title') {
                contextMenu = returnActivityOrProductMenu(e);
            }

            return contextMenu;
        }
    });
}


function isFavoritesProcessHeader(target) {
    var isFavorites = false;
    var wrapper = $(target).siblings('.bucket-wrapper');
    if (wrapper && wrapper.length > 0) {
        var fav = $(wrapper).find('.favorites');
        if (fav && fav.length > 0) {
            isFavorites = true;
        }
    }

    return isFavorites
}


function returnTaskMenu(event, taskName, hRef) {
    var taskFile = $(event.currentTarget).attr('href'); //.substring(hRef.lastIndexOf('/') + 1);
    var pathPrefix = hRef.substring(0, hRef.indexOf('schema/'));

    var bucketElem = $(event.currentTarget).parents('div.bucket-application, div.bucket-process');
    if (bucketElem.length > 0) {
        var processTaskPath = getProcessPath(bucketElem, event.currentTarget);
        processTaskPath = processTaskPath ? processTaskPath + taskName : "";

        var activityFolder = $(bucketElem).attr('data-subfolder');
        if (!activityFolder) {
            activityFolder = "";
        }

        return {
            callback: function(key, options) {
                var copyText = options.commands[key].name;
                copyText = copyText.substr(copyText.lastIndexOf('>') + 1);
                copyToClipboard(copyText);
            },
            items: {
                "fold1": {
                    "name": getLocalizedString(pageNavStrings.z_NAVIGATOR_MENUITEM_DETAILS),
                    "items": {
                        "fold1key1": {"name": "<strong>" + getLocalizedString(pageNavStrings.z_NAVIGATOR_MENU_TASK) + ":</strong> " + taskName},
                        "fold2key2": {"name": "<strong>" + getLocalizedString(pageNavStrings.z_NAVIGATOR_MENU_TASK_FILE) + ":</strong> " + taskFile},
                        "fold1key3": {"name": "<strong>" + getLocalizedString(pageNavStrings.z_NAVIGATOR_MENU_ACTIVITY_FOLDER) + ":</strong> " + activityFolder}
                    }
                },
                "fold2": {
                    "name": getLocalizedString(pageNavStrings.z_NAVIGATOR_MENUITEM_SHOW_URL),
                    "items": {
                        "fold2key1": {name: "<strong>" + getLocalizedString(pageNavStrings.z_NAVIGATOR_MENU_PATH) + ":</strong> " + processTaskPath},
                        "fold2key2": {name: "<strong>" + getLocalizedString(pageNavStrings.z_NAVIGATOR_MENU_URL) + ":</strong> " + pathPrefix + taskFile}
                    }
                }
            }
        };
    }
}


function returnProcessMenu(event) {
    var contextMenu;
    var targetElem = event.currentTarget;
    var bucketElem = $(targetElem).parents('div.bucket-application, div.bucket-process');
    if (bucketElem.length > 0) {
        contextMenu = getBasisMenuProductActivityProcess(event);
        if (context.keyValue && context.keyValue.length > 0) {
            var dashboardLayout = $(bucketElem).attr('data-dashboardLayout');
            var dashboardView = $(bucketElem).attr('data-dashboardView');

            contextMenu.items.fold2.items.fold2key3 = {"name": "<strong>" + getLocalizedString(pageNavStrings.z_NAVIGATOR_MENUITEM_DASHBOARDLAYOUT) + ":</strong> " + dashboardLayout ? dashboardLayout : ""};
            contextMenu.items.fold2.items.fold2key4 = {"name": "<strong>" + getLocalizedString(pageNavStrings.z_NAVIGATOR_MENUITEM_DASHBOARDVIEW) + ":</strong> " + dashboardView ? dashboardView : ""};
        }

        if (contextMenu.showHelp) {
            addHelpToContextMenu(contextMenu, event);
        }
    }

    return contextMenu;
}


function returnActivityOrProductMenu(event) {
    var contextMenu = getBasisMenuProductActivityProcess(event);

    if (contextMenu && contextMenu.showHelp) {
        addHelpToContextMenu(contextMenu, event);
    }

    return contextMenu;
}


function getBasisMenuProductActivityProcess(event) {
    var contextMenu;
    var targetElem = event.currentTarget;
    var bucketElem = $(targetElem).parents('div.bucket-application, div.bucket-process');
    if (bucketElem.length > 0) {
        var keyValue = getKeyValue(bucketElem, targetElem);
        var summaryText = getSummaryText(bucketElem, targetElem);
        var helpLink = getHelpLink(bucketElem, targetElem);

        var summaryLabel = getLocalizedString(pageNavStrings.z_NAVIGATOR_MENUITEM_SUMMARY);
        var showHelpButton = true;
        if (!helpLink) {
            helpLink = "";
            showHelpButton = false;
        }

        contextMenu = {
            items: {
                "fold1": {
                    "name": summaryLabel,
                    "items": {
                        "fold1key1": {"name": "<strong>" + summaryLabel + ":</strong> " + summaryText}
                    }
                },
                "fold2": {
                    "name": getLocalizedString(pageNavStrings.z_NAVIGATOR_MENUITEM_DETAILS),
                    "items": {
                        "fold2key1": {"name": "<strong>" + getLocalizedString(pageNavStrings.z_NAVIGATOR_MENU_KEY) + ":</strong> " + keyValue},
                        "fold2key2": {"name": "<strong>" + getLocalizedString(pageNavStrings.z_NAVIGATOR_MENU_HELPLINK) + ":</strong> " + helpLink}
                    }
                }
            }
        };
        contextMenu.keyValue = keyValue;
        contextMenu.showHelp = showHelpButton;
        contextMenu.helpLink = helpLink;
    }

    return contextMenu;
}


function getProcessPath(bucketElem, targetElem) {
    var productTitle = $(bucketElem).attr('data-productTitle');
    if (!productTitle) {
        productTitle = $(targetElem).attr('data-productTitle');
    }

    var activityTitle = $(bucketElem).attr('data-activityTitle');
    if (!activityTitle) {
        activityTitle = $(targetElem).attr('data-activityTitle');
    }

    var processTitle = $(bucketElem).attr('data-processTitle');
    if (!processTitle) {
        processTitle = $(targetElem).attr('data-processTitle');
    }

    return productTitle ? productTitle + '/' + activityTitle + '/' + processTitle + '/' : false;
}


function getKeyValue(bucketElem, targetElem) {
    var productId = $(bucketElem).attr('data-productId');

    var activityId = $(bucketElem).attr('data-activityId');
    if (!activityId) {
        activityId = $(targetElem).attr('data-activityId');
    }

    var processId = $(bucketElem).attr('data-processId');

    var keyValue = "";
    if (processId) {
        keyValue = processId;
    }
    else if (activityId) {
        keyValue = activityId;
    }
    else if (productId) {
        keyValue = productId;
    }

    return keyValue;
}


function getSummaryText(bucketElem, targetElem) {
    var summaryText = $(targetElem).attr('title');
    if (!summaryText) {
        summaryText = $(bucketElem).children('h2').attr('title');
    }
    if (!summaryText) {
        summaryText = '';
    }

    return summaryText;
}


function getHelpLink(bucketElem, targetElem) {
    var helpLink = $(targetElem).attr('data-helpLink');
    if (!helpLink) {
        helpLink = $(bucketElem).attr('data-helpLink');
    }

    return helpLink;
}


function addHelpToContextMenu(contextMenu, event) {
    var helpSystem = $(event.currentTarget).attr('data-helpSystem');
    if (!helpSystem) {
        var bucketElem = $(event.currentTarget).parents('div.bucket-application, div.bucket-process');
        helpSystem = $(bucketElem).attr('data-helpSystem');
    }

    contextMenu.items.help = {
        name: "<strong>" + getLocalizedString(pageNavStrings.z_NAVIGATOR_MENUITEM_HELP) + "</strong>",
        // supersedes any "global" callback
        callback: function(key, options) {
            if (contextMenu.helpLink && $.trim(contextMenu.helpLink).length > 0) {
                var helpWindowSettings = 'toolbar=yes,menubar=no,resizable=yes,scrollbars=yes,status=yes,width=800,height=600,left=10,top=10';
                var baseHelpUrl = context.archibusHelpLink.replace('(user.helpExtension)', context.userHelpExtension);
                if (helpSystem && helpSystem === 'SYSTEM') {
                    baseHelpUrl = context.systemMgmtHelpLink;
                }
                var helpUrl = baseHelpUrl + "#.." + contextMenu.helpLink.replace(/\\/g, "/");
                return window.open(helpUrl, getLocalizedString(pageNavStrings.z_PAGENAV_ARCHIBUS_HELP), helpWindowSettings);
            }
            else {
                contextMenu.helpLink = "an undefined help link.";
                var m = "help was clicked for " + contextMenu.helpLink;
                window.console && console.log(m) || alert(m);
            }
        }
    };

    return contextMenu;
}


function copyToClipboard (text) {
    window.prompt(getLocalizedString(pageNavStrings.z_PAGENAV_COPY_TO_CLIPBOARD), text);
}


function returnAppSpecificMenu() {
    return {
        callback: function(key, options) {
            if (key === 'report') {
                //var bucketWrapper = $(this).siblings('.app-specific-bucket');
                //var wrapperId = $(bucketWrapper).attr('id');

                var bucketElement = $(this).parent('.bucket-process');
                var bucketId = $(bucketElement).attr('id');
                //var bucketTitle = $(bucketElement).children('h2').text();
                //var mapElement = $(bucketWrapper).children('.pgnav-map');
                //var isMapControl = mapElement.length > 0;

                html2canvas($('#' + bucketId), {
                    proxy: '/html2canvasproxy',
                    allowTaint: true,
                    useCORS: true,
                    onrendered: function(canvas) {
                        // canvas is the final rendered <canvas> element
                        var myImage = canvas.toDataURL("image/png");

                        saveToDisk(myImage, 'testMeNow.png', bucketElement[0]);
                        //window.open(myImage);
                    }
                });
            }
        },
        items: {
            "notice": {name: getLocalizedString(pageNavStrings.z_PAGENAV_RUNTIME_CONTEXT_MENU)}
            //"report": {name: getLocalizedString(pageNavStrings.z_NAVIGATOR_USERMENU_PRESENTATIONS)}
        }
    };
}


function saveToDisk(imageUrl, fileName, el) {
    // for non-IE
    if (!window.ActiveXObject) {
        var save = document.createElement('a');
        save.href = imageUrl;
        save.target = '_blank';
        save.download = fileName || imageUrl;

        var event = new MouseEvent('click', {
            'view': window,
            'bubbles': true,
            'cancelable': true
        });
        save.dispatchEvent(event);
        (window.URL || window.webkitURL).revokeObjectURL(save.href);
    }
    // for IE
    else if ( !! window.ActiveXObject && document.execCommand)     {
        var e = jQuery.Event( "SaveAs" );
        jQuery( el ).trigger( e );

        var html = "<p>Right-click on image below and Save-Picture-As</p>";
        html += "<img src='" + imageUrl + "' alt='from canvas'/>";
        var tab = window.open();
        tab.document.write(html);
        tab.execCommand('SaveAs', true, fileName);
        tab.close();
    }
}





var PageNavUserInfo = {
    locale: 'en_US',
    localeDirectory: '',
    roleDirectory: '',
    redirectIntervalId: 0,
    redirectWaitCounter: 0,
    loggedInUser: '',
    webAppContextPath: '/archibus/',
    cssDirectory: '/archibus/schema/ab-core/css/',
    cssFilePrefix: 'ab-page-navigation-',
    generationDirectory: 'schema/ab-products/common/views/page-navigation/generated/',
    userInterfaceTheme: '',
    languageExtension: '',
    homeTabFragment: null,
    applicationsTabHtmlFragment: null,
    roleSplitRegex: /[\\/\s]+/
};


function fileExists(url) {
    if (url) {
        var targetUrl = url.substr(url.lastIndexOf(PageNavUserInfo.webAppContextPath));

		var req = new XMLHttpRequest();
        req.open('GET', targetUrl, false);
        req.send();
        return req.status === 200;
    }
    else {
        return false;
    }
}


function setPageNavUserInfo(userInfo) {
    setUserLocaleDirectory(userInfo.locale, userInfo.languageExtension);
    setUserRoleDirectory(userInfo.role);
    setUserColorScheme(userInfo.colorScheme);
}

function setUserLocaleDirectory(afmUsersLocale, languageExtension) {
    setUserLanguage(languageExtension);
    if (afmUsersLocale) {
        var locale = afmUsersLocale;

        if (!languageExtension || languageExtension.length === 0) {
            if (locale === 'zh_CN') {
                languageExtension = 'ch'
            }
            else {
                languageExtension = locale.substr(0, 2);
            }
        }
        else if (languageExtension.length === 3) {
            languageExtension = languageExtension.substring(1);
        }

        PageNavUserInfo.locale = locale;
        PageNavUserInfo.localeDirectory = languageExtension + "/";
    }
}


function setUserLanguage(languageExtension) {
    if (languageExtension) {
        PageNavUserInfo.languageExtension = languageExtension;
    }
}


function setUserRoleDirectory(roleName) {
    PageNavUserInfo.roleDirectory = getCamelCasedString(roleName) + "/";
    return PageNavUserInfo.roleDirectory;
}
function getUserRoleDirectory(roleName) {
    return getCamelCasedString(roleName) + "/";
}


function setUserColorScheme(colorScheme) {
    if (colorScheme && colorScheme.length > 0) {
        PageNavUserInfo.userInterfaceTheme = $.trim(colorScheme).toLowerCase().replace(" ", "-");

    }
}


function getCamelCasedString(inputString) {
    var camelCasedString = "";
    if (inputString && inputString.length > 0) {
        var words = inputString.split(PageNavUserInfo.roleSplitRegex);
        var wordCount = words.length;
        for (var i = 0; i < wordCount; i++) {
            if (words[i].length === 1 &&
                (words[i].charAt(0) === '&' ||
                words[i].charAt(0) === '_')) {
                continue;
            }
            if (words[i].charAt(0) === '(') {
                words[i] = words[i].substr(1);
            }
            var length = words[i].length;
            if (words[i].charAt(length - 1) === ')') {
                words[i] = words[i].substr(0, length - 1);
            }

            if (i === 0) {
                camelCasedString = words[i].toLocaleLowerCase();
            }
            else {
                camelCasedString += words[i].charAt(0).toLocaleUpperCase();
                camelCasedString += words[i].substr(1).toLocaleLowerCase();
            }
        }
    }

    return camelCasedString;
}


function getRoleDashed(roleName) {
    var dashedRoleName = "";
    var words = roleName.toLocaleLowerCase().split(PageNavUserInfo.roleSplitRegex);
    var wordCount = words.length;
    for (var i = 0; i < wordCount; i++) {
        if (words[i].length === 1 && (words[i].charAt(0) === '&' || words[i].charAt(0) === '-')) {
            continue;
        }
        if (words[i].charAt(0) === '(') {
            words[i] = words[i].substr(1);
        }
        var len = words[i].length;
        if (words[i].charAt(len - 1) === ')') {
            words[i] = words[i].substr(0, len - 1);
        }

            dashedRoleName +=  (i === 0) ? words[i] : "-" + words[i];
        }

    return dashedRoleName;
}


function getUserInfo() {
    var returnValue;

    try {
        var result = Ab.workflow.Workflow.call('AbCommonResources-getUser', {});

        if (Ab.workflow.Workflow.sessionTimeoutDetected) {
            returnValue = {'name': "timeout", 'role': "timeout"};
        }
        else if (result.data) {
            var adminPath = AdminService._path;
            var webAppContextPath = '';
            if (adminPath) {
                webAppContextPath = adminPath.substr(0, adminPath.indexOf('/', 2) + 1);
            }

            returnValue = {
                'name': result.data.user_name,
                'role': result.data.role_name,
                'groups': result.data.groups,
                'projectName': result.data.projectName,
                'serverVersion': result.data.serverVersion,
                'locale': result.data.locale,
                'languageExtension': result.data.languageExtension,
                'colorScheme': result.data.colorScheme,
                'webAppContextPath': webAppContextPath
            };
        }
    }
    catch (e) {
        Workflow.handleError(e);
    }

    return returnValue;
}




// function finishSetup(userInfo) {
// ...    
//    setUpHistory(tabsWidget, 'ul.ui-tabs-nav a');
// ...
// }



function setUpHistory(tabs, tabsLinkSelector) {

    // Define our own click handler for the tabs, overriding the default.
    tabs.find( tabsLinkSelector ).click(function(){
        // Get the id of this tab widget.
        var id = $(this).closest( '.ui-tabs' ).attr( 'id' );

        // Get the index of the clicked tab.
        var index = $(this).parent().prevAll().length;
        pushState(id, index);
    });

    // Define our own click handler for the anchor elements to
    // prevent the default click behavior from directly opening the href.
    $('#navigationTabs').find('a').click(function(){return false;});

    // Bind an event to window.onhashchange so that when the history state changes:
    // 1) selects the appropriate tab, changing the current tab as necessary.
    // 2) loads the home tab task OR loads the process view and/or loads the process task
    $(window).bind( 'hashchange', function(e) {
        if (signOutOnTimeOut()) { return; }

        tabs.each(function(){
            // Get the index for this tab widget from the hash, based on the appropriate id property.
            var selectedTabIndex = $.bbq.getState( this.id, true ) || 0;
            $('#navigationTabs').tabs("option", "active", selectedTabIndex);
        });

        // Get the hash (fragment), with any leading # removed, converted to an object
        var urlParams = $.deparam.fragment();
        if ((!urlParams.eTask && !urlParams.process  && !urlParams.pTask )) {
            var selectedTab = urlParams.navigationTabs || 0;
            restoreTab(selectedTab);
        }

        if (urlParams.eTask) {
            //var taskElem = $('a[href="' + urlParams.eTask + '"]');
            //if (taskElem.length > 0) {
            //    taskElem = taskElem[0];
            //}
            if (!isTaskAlreadyShown('#taskFrame', urlParams.eTask)) {
                setDisplayContainers('#taskFrame', urlParams.eTask);
            }
        }
        else if (urlParams.pTask) {
            //var pTaskElem = $('a[href="' + urlParams.pTask + '"]');
            //if (pTaskElem.length > 0) {
            //    pTaskElem = pTaskElem[0];
            //}
            if (!isTaskAlreadyShown('#pTaskFrame', urlParams.pTask)) {
                setDisplayContainers('#pTaskFrame', urlParams.pTask);
            }
        }
        else  if (urlParams.process) {
            if (!isProcessAlreadyShown('#applicationsProcessView', urlParams.process)) {
                setDisplayContainers('#applicationsProcessView', urlParams.process);
            }
            $('#breadCrumbContainer').hide();
        }
    });

    // Since the event is only triggered when the hash changes, we need to trigger
    // the event now, to handle the hash the page may have loaded with.
    $(window).trigger( 'hashchange' );
}



function getStateCopy() {
    // Set the state!
    var state = {};
    var currentState = $.bbq.getState();
    if (currentState.navigationTabs) {
        state.navigationTabs = currentState.navigationTabs;
    }
    if (currentState.role) {
        state.role = currentState.role;
    }
    if (currentState.process) {
        state.process = currentState.process;
    }
    if (currentState.taskFile) {
        state.taskFile = currentState.taskFile;
    }
    if (currentState.pTask) {
        state.pTask = currentState.pTask;
    }

    return state;
}


function pushState(attribute, value) {
    var state = getStateCopy();
    state[ attribute ] = value;
    $.bbq.pushState( state );
}


function isTaskAlreadyShown(displayElementId, displaySourceFile) {
    var showing = false;
    var frameSrc = $(displayElementId).attr('src');
    if (frameSrc && frameSrc.lastIndexOf('/') > 0) {
        frameSrc = frameSrc.substr(frameSrc.lastIndexOf('/') + 1);
        if (frameSrc === displaySourceFile) {
			showing = true;
		}
    }

    return showing;
}


function isProcessAlreadyShown(displayElementId, displaySourceFile) {
    var showing = false;
    var frameSrc = $(displayElementId).attr('rel');
    if (frameSrc && frameSrc.lastIndexOf('/') > 0) {
        frameSrc = frameSrc.substr(frameSrc.lastIndexOf('/') + 1);
        if (frameSrc === displaySourceFile) {
			showing = true;
		}
    }

    return showing;
}




function loadLocalization(languageCode) {
    if (languageCode && languageCode.length > 1 ) {
        var langFile = PageNavUserInfo.webAppContextPath + "schema/ab-core/controls/lang/ui-controls-lang-" + languageCode + ".js";
        if (langFileExists(langFile)) {
            $.getScript(langFile, function(data, textStatus, jqxhr) {
                console.log(textStatus);
                console.log('Load of locale strings was performed.');
                localizeDisplayText();
                localizeFavoritesDisplayText();
            });
        }
        else {
            localizeDisplayText();
            localizeFavoritesDisplayText();
        }
    }
}


function langFileExists(url) {
    if (url) {
        var targetUrl = window.location.href;// url.substr(url.lastIndexOf('/') + 1);
        var prefix = targetUrl.substr(0, targetUrl.indexOf('/', 10));
        targetUrl = prefix + url;

        var req = new XMLHttpRequest();
        req.open('GET', targetUrl, false);
        req.send();
        return req.status === 200;
    }
    else {
        return false;
    }
}


function getLocalizedString(key) {
    var localString = key;
    if (Ab.localization) {
        var localStrings = Ab.localization.Localization.localizedStrings;
        for (var i = 0, msg; msg = localStrings[i]; i++) {
            if (msg.key3 === key) {
                (msg.value === '') ? localString = '*' + msg.key3 : localString = msg.value;
                return localString;
            }
        }
    }

    if (valueExistsNotEmpty(localString)) {
        localString = convertFromXMLValue(localString);
    }

    return localString;
}


function convertFromXMLValue(fieldValue){
    var returnValue = "";
    if (fieldValue && jQuery.isNumeric(fieldValue)) {
        returnValue = fieldValue;
    }
    else if (fieldValue && typeof fieldValue == "string") {
        returnValue = fieldValue.replace(/&amp;/g, '&');
        returnValue = returnValue.replace(/&gt;/g, '>');
        returnValue = returnValue.replace(/&lt;/g, '<');
        returnValue = returnValue.replace(/&apos;/g, '\'');
        returnValue = returnValue.replace(/&quot;/g, '\"');
    }

    return returnValue;
}

function convertToXMLValue(fieldValue){
    var returnValue = "";
    if (fieldValue && jQuery.isNumeric(fieldValue)) {
        returnValue = fieldValue;
    }
    else if (fieldValue && typeof fieldValue === "string") {
        returnValue = fieldValue.replace(/&/g, '&amp;');
        returnValue = returnValue.replace(/>/g, '&gt;');
        returnValue = returnValue.replace(/</g, '&lt;');
        returnValue = returnValue.replace(/\'/g, '&apos;');
        returnValue = returnValue.replace(/\"/g, '&quot;');
    }
    else if (typeof fieldValue === "boolean") {
        returnValue = fieldValue ? "true" : "false";
    }

    return returnValue;
}


// TODO unify the syntax with z_PAGENAV_
// TODO transform this from a global to a data member
var pageNavStrings = {
// @begin_translatable
    z_NAVIGATOR_MENU_KEY: 'Key',
    z_NAVIGATOR_MENU_HELPLINK: 'Help Link',
    z_NAVIGATOR_MENU_TASK: 'Task',
    z_NAVIGATOR_MENU_TASK_FILE: 'Task File',
    z_NAVIGATOR_MENU_ACTIVITY_FOLDER: 'Activity Folder',
    z_NAVIGATOR_MENU_PATH: 'Path',
    z_NAVIGATOR_MENU_URL: 'URL',
    z_NAVIGATOR_MENUITEM_SUMMARY: 'Summary',
    z_NAVIGATOR_MENUITEM_DETAILS: 'Details',
    z_NAVIGATOR_MENUITEM_HELP: 'Help',
    z_NAVIGATOR_MENUITEM_SHOW_URL: 'Show Task Information',
    z_NAVIGATOR_USERMENU_PROJECT: 'Project:',
    z_NAVIGATOR_USERMENU_ROLE: 'Role:',
    z_NAVIGATOR_USERMENU_VERSION: 'Version:',
    z_NAVIGATOR_USERMENU_PROFILE: 'My Profile',
    z_NAVIGATOR_USERMENU_JOBS: 'My Jobs',
    z_NAVIGATOR_USERMENU_PRESENTATIONS: 'Create Presentation',
    z_NAVIGATOR_USERMENU_EDITOR: 'Edit Home Page',
    z_NAVIGATOR_MENUITEM_DASHBOARDLAYOUT: 'Dashboard Layout',
    z_NAVIGATOR_MENUITEM_DASHBOARDVIEW: 'Dashboard View',
    z_NAVIGATOR_MENUITEM_DESCRIPTORFILE: 'Descriptor File',
    z_NAVIGATOR_MENUITEM_DESCRIPTORPROCESS: 'Descriptor Process',
    z_PAGENAV_SESSION_TIME_OUT: 'Session has timed out. Please sign in again.',
    z_PAGENAV_SIGNOUT_JOBS_MESSAGE: 'Some of the jobs you have started are still running. Are you sure you want to Sign Out?',
    z_PAGENAV_BUCKET_NODATA: 'No data found',
    z_PAGENAV_SEARCH_NODATA: 'No search results found',
    z_PAGENAV_GETRECORDS_MORE: 'More values exist',
    z_PAGENAV_FAVORITES_DUPLICATE_WARNING: 'The task <b>{1}</b> is already one of your Favorites!',
    z_PAGENAV_FAVORITES_DUPLICATE_TITLE: 'No Duplicating Favorites',
    z_PAGENAV_FAVORITES_SAVE_ERROR_TITLE: 'Error saving Favorites',
    z_PAGENAV_FAVORITES_DELETE_ERROR_TITLE: 'Error removing Favorites',
    z_PAGENAV_PUBLISH: 'Publish',
    z_PAGENAV_PUBLISH_LOCALE_BEFORE_LOGIN: 'HTML files for the locale {1} do not exist. Please publish before logging in.',
    z_PAGENAV_COPY_TO_CLIPBOARD: 'Copy to clipboard: Ctrl+C, Enter',
    z_PAGENAV_RUNTIME_CONTEXT_MENU: 'No additional information is available for this panel.',
    z_PAGENAV_RUNTIME_BUCKET_REPORT: 'Save as a Powerpoint .ppt file.',
    z_PAGENAV_RUNTIME_PAGE_REPORT: 'Print Home Page to a .ppt file.',

    z_PAGENAV_WINDOW_TITLE: 'ARCHIBUS Web Central',
    z_PAGENAV_BREADCRUMB_LABEL: 'Tasks',
    z_PAGENAV_SEARCHBOX_INITIAL_TEXT: 'Find a form or report',
    z_PAGENAV_SEARCHRESULT_TITLE: 'Search',
    z_PAGENAV_SEARCHRESULT_DOMAIN: 'Domain',
    z_PAGENAV_SEARCHRESULT_APPLICATION: 'Application',
    z_PAGENAV_SEARCHRESULT_PROCESS: 'Process',
    z_PAGENAV_SIGNOUT_BUTTON_LABEL: 'Sign Out',
    z_PAGENAV_HELP_BUTTON_LABEL: 'Help',
    z_PAGENAV_FAVORITES_ADD_TARGET_TEXT: 'Drag a task here to add.',
    z_PAGENAV_FAVORITES_ADD_TARGET_TOOLTIP:  'Drag any task or report into the box to add it.',
    z_PAGENAV_FAVORITES_DELETE_TARGET_TOOLTIP: 'Drag a favorite into the trashcan to remove it.',
    z_PAGENAV_BACK_TO_TOP: 'Back to Top',
    z_PAGENAV_ARCHIBUS_HELP: 'Archibus Help',

    z_PAGENAV_METRICS_METRIC: 'Metric',
    z_PAGENAV_METRICS_CURRENT: 'Current',
    z_PAGENAV_METRICS_CHANGE: 'Change',
    z_PAGENAV_METRICS_PER_YEAR: 'Per Year',
    z_PAGENAV_METRICS_TARGET_PERCENT: '% of Target',
    z_PAGENAV_METRICS_TREND: 'Trend',
    Z_PAGENAV_METRICS_ASSUMPTIONS_DESCRIPTION: "Description",
    Z_PAGENAV_METRICS_ASSUMPTIONS: "Assumptions",
    Z_PAGENAV_METRICS_ASSUMPTIONS_BSN_IMPL: "Business Implications",
    Z_PAGENAV_METRICS_ASSUMPTIONS_RECURRENCE: "Recurrence",
    z_PAGENAV_METRICS_ALL: "All",

    z_PAGENAV_FAVORITES_OK: "OK",
    z_PAGENAV_FAVORITES_SAVE: "Save",
    z_PAGENAV_FAVORITES_CANCEL: "Cancel",
    z_PAGENAV_FAVORITES_RENAME: "Rename",
    z_PAGENAV_FAVORITES_EDIT_TITLE: "Edit Favorites Titles and Ordering",
    z_PAGENAV_FAVORITES_SAVE_ORDER: "Save Order",
    z_PAGENAV_FAVORITES_CANCEL_REORDER: "Discard Changed Order",
    z_PAGENAV_FAVORITES_LOSE_ORDER_MESSAGE: "Closing the dialog now will lose the changed ordering of the favorites.",
    z_PAGENAV_FAVORITES_LOSE_ORDER_QUESTION: "Do you want to close now?",
    z_PAGENAV_FAVORITES_SAVE_FAILED: "Saving new name failed",
    z_PAGENAV_FAVORITES_RENAME_FAIL_MESSAGE: "The renaming was not saved to the server.",
    z_PAGENAV_FAVORITES_RENAME_FAIL_QUESTION: "Ensure that name is unique.",
    z_PAGENAV_FAVORITES_ORDER_SAVED: "Order saved.",
    z_PAGENAV_FAVORITES_SAVE_NOOP: "No need to save.",
    z_PAGENAV_FAVORITES_ORDER_SAVE_FAILED: "Order save failed!",
    z_PAGENAV_BUCKET_DEF_ERROR: 'Bucket Definition Error',
    z_PAGENAV_NO_DATA_AVAILABLE: 'No Data Available',

    z_PAGENAV_MAP_WORLD_IMAGERY: 'World Imagery', 
    z_PAGENAV_MAP_WORLD_IMAGERY_WITH_LABELS: 'World Imagery with Labels',
    z_PAGENAV_MAP_WORLD_LIGHT_GRAY_BASE: 'World Light Gray Canvas',
    z_PAGENAV_MAP_WORLD_DARK_GRAY_BASE: 'World Dark Gray Canvas',
    z_PAGENAV_MAP_WORLD_STREET_MAP: 'World Street Map',  
    z_PAGENAV_MAP_WORLD_SHADED_RELIEF: 'World Shaded Relief',       
    z_PAGENAV_MAP_WORLD_TOPOGRAPHIC_MAP: 'World Topographic',
    z_PAGENAV_MAP_NATGEO_WORLD_MAP: 'National Geographic World Map',
    z_PAGENAV_MAP_OCEAN_BASEMAP: 'Oceans Basemap', 

    z_PAGENAV_MAP_ROADMAP: 'Road Map',
    z_PAGENAV_MAP_SATELLITE: 'Satellite',  
    z_PAGENAV_MAP_HYBRID: 'Satellite with Labels', 
    z_PAGENAV_MAP_TERRAIN: 'Terrain'

// @end_translatable
};




function initDandDFavorites() {
    var activeTabId = getActiveTabId();
    var pageElement = $(activeTabId).find('.nav-pages[display!= "none"]');

    var taskElement = $(activeTabId).find('.process-tasks > li > a');
    $(taskElement).draggable({cursor: 'move', helper: 'clone', containment: pageElement,
        appendTo: pageElement, revert: "invalid", revertDuration: 100});

    // create a myFavorite when dropping a task on the favorites target
    var addTargetElement = $(activeTabId).find('.favorites-add-target');
    $(addTargetElement).droppable({tolerance: "pointer", greedy: true, accept: 'a[data-isFavorites!="true"]'});
    $(addTargetElement).on("drop", handleFavoritesAddDropEvent);

    // delete a myFavorites when dragging an existing favorite and dropping it on the trashcan
    var deleteTargetElement = $(activeTabId).find('.favorites-delete-target');
    $(deleteTargetElement).droppable({tolerance: "pointer", greedy: true, accept: 'a[data-isFavorites="true"]'});
    $(deleteTargetElement).on("drop", handleFavoritesDeleteDropEvent);
}


function localizeFavoritesDisplayText() {
    $('span.favorites-add-target').html(getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_ADD_TARGET_TEXT));
    $('.favorites-add-target').attr('title', getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_ADD_TARGET_TOOLTIP));
    $('span.favorites-delete-target').attr('title', getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_DELETE_TARGET_TOOLTIP));
    $('a.page-top-link').html(getLocalizedString(pageNavStrings.z_PAGENAV_BACK_TO_TOP));
}


function loadFavoritesBuckets() {
    var activeTab = getActiveTabId();
    var favoritesItemsContainer = $(activeTab).find('.bucket-wrapper > .favorites').find('.favorites-items').first();
    if (favoritesItemsContainer) {
        var parameters = {
            viewName: 'ab-page-navigation-datasource.axvw',
            dataSourceId: 'pageNavigationFavorites_ds',
            groupIndex: 0,
            version: '2',
            useHierarchicalSecurityRestriction: false
        };
        var results = getDataRecords(parameters);
        drawFavoritesRecords(favoritesItemsContainer, results.records);
        pageTallFavoritesBuckets();
    }
}


function drawFavoritesRecords(favoritesItemListElement, records) {
    $(favoritesItemListElement).empty();

    if (records && records.length > 0) {
        // To open in the correct  frame, Home Favorites require a rel='eTask' and Applications tab favorites require a rel='pTask'
        var activeTabAnchor = $("li.ui-tabs-active > a");
        var activeTabID = $(activeTabAnchor).attr('id');
        var taskType = activeTabID.toLowerCase().indexOf('home') >= 0 ? 'eTask' : 'pTask';

        for (var i = 0, record; record = records[i]; i++) {
            var taskId = record['afm_ptasks.task_id'];
            var taskName = removeUserNameFromTaskString(taskId);

            $(favoritesItemListElement).append('<li><a href="' + record['afm_ptasks.task_file'] + '" rel="' + taskType +
                '" data-activityId="' + record['afm_ptasks.activity_id']  +
                '" data-processId="' + record['afm_ptasks.process_id']  +
                '" data-taskId="' + taskId  +
                '" data-isFavorites="true">' +
                taskName + '</a></li>');
        }

        var activeTabId = getActiveTabId();
        var favoritesItemsAnchor = $('.favorites-items > li > a');
        favoritesItemsAnchor.draggable({helper: 'clone', containment: $(activeTabId).find('.favorites').parent('.bucket-wrapper')});
        favoritesItemsAnchor.click(function(){ return false; });
    }
}


function removeUserNameFromTaskString(taskString) {
    var userNameUpperCase = PageNavUserInfo.loggedInUser.toUpperCase();
    if (taskString.indexOf('(' + userNameUpperCase + ')') === (taskString.length - userNameUpperCase.length - 2)) {
        taskString = $.trim(taskString.substr(0, taskString.indexOf(userNameUpperCase) - 1));
    }

    return taskString;
}


function handleFavoritesAddDropEvent (evt, ui) {
    var draggable = ui.draggable;
    var viewFileName = draggable.attr("href");
    var taskName = convertFromXMLValue(draggable.get(0).innerHTML);
    var userNameViewSuffix = '-' + PageNavUserInfo.loggedInUser.toLowerCase() + '.axvw';
    var abViewSuffix = '.axvw';

    // if dropped task is already a favorites, ignore
    var existingFavorites = $(getActiveTabId()).find('.bucket-wrapper > .favorites > ol.favorites-items > li > a');
    for (var i = 0, existingFav; existingFav = existingFavorites[i]; i++) {
        var existingFavoritesPath = $(existingFav).attr("href");
        existingFavoritesPath = existingFavoritesPath.substr(existingFavoritesPath.lastIndexOf('\\') + 1);
        var favoriteViewStripped = existingFavoritesPath.replace(userNameViewSuffix, abViewSuffix);

        if (viewFileName === existingFavoritesPath || viewFileName === favoriteViewStripped) {
            showModalMessageDialog(getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_DUPLICATE_TITLE),
                getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_DUPLICATE_WARNING).replace('{1}', taskName));
            return;
        }
    }

    var schemaParent = $(draggable).parents('.bucket-process, .bucket-application');
    if (schemaParent.length > 0) {
        var parameters = {
            viewName: viewFileName,
            activityId : $(schemaParent).attr('data-activityId'),
            processId: $(schemaParent).attr('data-processId'),
            taskId: convertFromXMLValue($(draggable).attr('data-taskId')),
            taskIdLocalized: taskName,
            taskType: convertFromXMLValue($(draggable).attr('data-taskType')),
            taskIcon: convertFromXMLValue($(draggable).attr('data-taskIcon')),
            isWritePtask: true
        };

        var wfrResult = runWorkflowRuleAndConfirm('AbSystemAdministration-addViewToMyFavorites', parameters);
        if (wfrResult.success) {
            refreshFavorites();
        }
        else {
            showModalMessageDialog(getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_SAVE_ERROR_TITLE), wfrResult.message);
        }
    }
}


function handleFavoritesDeleteDropEvent (evt, ui) {
    var draggable = ui.draggable;
    var element = draggable.get(0);
    var body = $('body');
    body.css('cursor', 'progress');

    var removed = removeMyFavoritesTask(element);
    if (removed) {
        $(element).parent().remove();
    }
    else {
        returnElementToUndraggedPosition(element);
    }

    body.css('cursor', 'default');
}


function removeMyFavoritesTask(elem) {
    var taskName = elem.innerHTML;
    var userNameUpperCase = PageNavUserInfo.loggedInUser.toUpperCase();
    var parameters = {
        viewName: $(elem).attr("href"),
        isWritePtask: true,
        taskId: convertFromXMLValue($(elem).attr('data-taskId')),
        taskIdLocalized: convertFromXMLValue(taskName + ' (' + userNameUpperCase + ')'),
        processId: $(elem).attr('data-processId'),
        activityId : $(elem).attr('data-activityId')
    };

    var removed = runWorkflowRuleAndConfirm('AbSystemAdministration-removeViewFromMyFavorites', parameters);
    if (!removed.success) {
        parameters.taskId = $.trim(taskName.substring(0, taskName.indexOf('(' + userNameUpperCase)));
        removed = runWorkflowRuleAndConfirm('AbSystemAdministration-removeViewFromMyFavorites', parameters);
    }

    if (!removed.success) {
        showModalMessageDialog(getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_DELETE_ERROR_TITLE), wfrResult.message);
    }
    else {
        refreshFavorites();
    }

    return removed;
}


function removeImgElemIfExists(text) {
    var imgIndex = text.indexOf("<img ");
    if (imgIndex >= 0) {
        var imgEndIndex = text.indexOf(">", imgIndex);
        text = text.substr(0, imgIndex) +  text.substr(imgEndIndex + 1);
    }

    return $.trim(text);
}


function pageTallFavoritesBuckets() {
    var activeTabId = getActiveTabId();
    var bucketType = activeTabId === '#tabPageNavigationHome' ? 'process' : 'application';

    $(activeTabId).find('.bucket-wrapper > .favorites').each( function(n) {
        var allowableUnpagedBucketHeight = getUnpagedBucketHeight($(this).parent(), bucketType);
        var allowableBucketHeight = allowableUnpagedBucketHeight - 60;
        var measuredBucketHeight = $(this).parent().height();

        if (measuredBucketHeight > allowableUnpagedBucketHeight) {
            var taskListElements = $(this).find('.favorites-items').find('li');
            var pageRootElement = $(this).find('.favorites-items');
            var removedTaskElements = removeActivityElementsToCorrectHeight(allowableBucketHeight, taskListElements, this);

            // add page wrappers
            var pageRowHeight = getPageRowHeight(pageRootElement);
            var bucketId = $(this).parent().attr('id');
            $(pageRootElement).wrap('<div class="scrollable-wrapper scrollable-favorites-wrapper-' + pageRowHeight + '"></div>');
            $(pageRootElement).wrap('<div class="scrollable-root scrollable-favorites-' + pageRowHeight + '" id="' + bucketId + n + '_scroller"></div>');
            $(pageRootElement).wrap('<div class="items"></div>');

            appendAdditionalFavoritesPages(allowableBucketHeight, pageRootElement, bucketId, n, 1, removedTaskElements);

            // add navi element & initialize
            $(this).append('<div class="navi" id="favNavi_' + bucketId + n + '"></div>');
            $("#" + bucketId + n + '_scroller').scrollable({vertical: false, mousewheel: true}).navigator("#favNavi_" + bucketId + n);
        }
    });
}


function appendAdditionalFavoritesPages(allowableBucketHeight, pageRootElement, bucketId, n, pageIndex, removedActivityElements) {
    var rootId = bucketId + n + '_' + pageIndex + '_insert_root';
    $(pageRootElement).after('<ol class="favorites-items" id="' + rootId + '"></ol>');
    var root = $("#" + rootId);

    var activityCount = removedActivityElements.length;
    for (var i = 0; i < activityCount; i++) {
        root.append(removedActivityElements[i])
    }

    var measuredBucketHeight = root.height();
    if (measuredBucketHeight > allowableBucketHeight) {
        var activityElements = root.children('li');
        var nextRemovedTaskElements = removeActivityElementsToCorrectHeight(allowableBucketHeight, activityElements, root);
        appendAdditionalFavoritesPages(allowableBucketHeight, root, bucketId, n, pageIndex + 1, nextRemovedTaskElements);
    }
}


function wasFavoritesDrag(elem) {
    var wasFavoritesTaskDrag = false;

    if ($(elem).attr('data-isFavorites') && wasElementDragged(elem)) {
        wasFavoritesTaskDrag = true;
    }

    return wasFavoritesTaskDrag;
}


function returnElementToUndraggedPosition(element) {
    var cssDiffLeft = parseFloat($(element).css('left'));
    var cssDiffTop = parseFloat($(element).css('top'));

    if (isNumber(cssDiffLeft) && Math.abs(cssDiffLeft) > 0) {
        $(element).css('left', '0px');
    }

    if (isNumber(cssDiffTop) && Math.abs(cssDiffTop) > 0) {
        $(element).css('top', '0px');
    }
}



function runWorkflowRuleAndConfirm(workflowRuleName, parameters) {
    var returnValue = { success: true, message: ""};

    try {
        Ab.workflow.Workflow.call(workflowRuleName, parameters);
    }
    catch (e) {
        returnValue = { success: false, message: e.message};
    }
    return returnValue
}


function showModalMessageDialog(title, message) {
    var messageDialog = $('#messageDialog');
    messageDialog.attr('title',title);
    messageDialog.html('<p>' + message + '</p>');
    messageDialog.dialog({ modal: true });
}

function refreshFavorites() {
    var activeTabId = getActiveTabId();
    var root = $(activeTabId).find('.favorites');
    if (root && $(root).length) {
        $(root).empty();
        $(root).append('<ol class="favorites-items"></ol>');
        loadFavoritesBuckets();

    }
}


var PageNavFavoritesDialogConfig = {
    // Confirm dialog options for use to confirm discarding unsaved order change.
    confirmDiscardReorderDialogOptions: null,
    // Confirm dialog options for use to notify of failure when saving name change to server.
    renameFailedDialogOptions: null,
    // Handle to warning confirmation dialog for opening and closing.
    confirmDialog: null,
    // For backwards compatibility with pre-21.3 servers & databases
    // Global Tri-valued flag - is the WFR for reordering myFavorites defined. undefined: false; true.
    reorderFavoritesWfrDefined: false
};


function getReorderFavoritesWfrDefined() {
    if (typeof PageNavFavoritesDialogConfig.reorderFavoritesWfrDefined === 'undefined') {
        // call WFR without favoritesObjects parameter to test for existence of WFR
        var parameters = {
            viewName: 'ab-page-navigation-datasource.axvw',
            dataSourceId: 'pageNavigationFavorites_ds',
            isWritePtask: true,
            favorites: toJSON([])
        };

        try {
            var testWfrExistence = runWorkflowRuleAndConfirm('AbSystemAdministration-updateOrderOfMyFavorites', parameters);
            PageNavFavoritesDialogConfig.reorderFavoritesWfrDefined = testWfrExistence.success;
        }
        catch (e) {
            PageNavFavoritesDialogConfig.reorderFavoritesWfrDefined = false;
        }
    }

    return PageNavFavoritesDialogConfig.reorderFavoritesWfrDefined;
}


function showFavoritesDialog(target) {
    if (!getReorderFavoritesWfrDefined()) {
        return false;
    }

    var parentId = $(target).parent().attr('id');
    var dialogId = parentId + '_dialog';

    // localized strings
    var cancelButtonTitle = getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_CANCEL);
    var confirmDialogHtml =  getConfirmDialogHtml(getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_CANCEL_REORDER),
            getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_LOSE_ORDER_MESSAGE)+ '<br>' +
            getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_LOSE_ORDER_QUESTION));

    // when dialog exists already (has been opened >= once before) don't recreate the root or there will be multiple (hidden) dialogs
    var dialogRoot = $('#' + dialogId);
    var dialogExists = (dialogRoot && dialogRoot.length > 0);
    if (!dialogExists) {
        $(target).append('<div id="' + dialogId + '" class="favorites-dialog" title="' + getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_EDIT_TITLE) + '">' +
            '<div class="favorites-dialog-status" data-status="initial"><span class="favorites-status-display"></span></div>' +
            confirmDialogHtml +
            '<ol></ol>' +
            '</div>');

        dialogRoot = $(target).find('#' + dialogId);
    }
    var dialogListRoot = $(dialogRoot).find('ol');

    var favoriteItems = $(target).siblings('.bucket-wrapper').find('a.ui-draggable');
    var favoritesCount = favoriteItems.length;
    for (var i = 0; i < favoritesCount; i++) {
        var item = favoriteItems[i];
        var displayTitle = item.innerHTML;

        var activityId = $(item).attr('data-activityid');
        var processId = $(item).attr('data-processid');
        // taskId has hot_user_name appended in parens
        var taskId = $(item).attr('data-taskid');
        var taskFile = $(item).attr('href');

        dialogListRoot.append('<li><div class="portlet" data-task-file ="' + taskFile + '"' +
            'data-activity-id="' + activityId + '" data-process-id="' + processId + '" data-task-id="' + taskId + '">' +
            '<div class="portlet-header"><span class="favorites-display-title">' + displayTitle + '</span></div>' +
            '<div class="portlet-content" style="display:none;">' + getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_RENAME) + '<br>' +
            '<input type="text" style="padding-right:0;margin:0;width:99%;" value="' + displayTitle + '"><br>' +
            '<div class="portlet-button-panel">' +
            '<button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only rename-save" type="button" role="button">' +
            '<span class="ui-button-text">' + getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_SAVE) + '</span></button>' +
            '<button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only rename-cancel" type="button" role="button">' +
            '<span class="ui-button-text">' + cancelButtonTitle + '</span></button>' +
            '</div>' +
            '</div>' +
            '</div></li>');
    }

    dialogListRoot.sortable({
        handle: ".portlet-header",
        cancel: ".portlet-toggle",
        placeholder: "portlet-placeholder ui-corner-all",
        change: onFavoritesSortChange
    });

    $('.favorites-dialog-status').css('display', 'none');

    $('.portlet')
        .addClass('ui-widget ui-widget-content ui-helper-clearfix ui-corner-all')
        .find('.portlet-header')
        .addClass('ui-widget-header ui-corner-all')
        .prepend('<span class="ui-icon ui-icon-plusthick portlet-toggle"></span>');

    $('.portlet-toggle').click( function() {
        var icon = $(this);
        icon.toggleClass('ui-icon-minusthick ui-icon-plusthick');
        icon.closest('.portlet').find(".portlet-content").toggle();
    });

    $('.rename-cancel').click( cancelFavoriteRenaming );
    $('.rename-save').click( saveFavoriteRenaming );

    var tabHeight = parseInt( $(target).closest('.ui-tabs-panel').css('height') );
    var dialogHeight =  Math.min(tabHeight, (favoriteItems.length * 45) + 150);

    if (!dialogExists) {
        dialogRoot.dialog({
            autoOpen: false,
            modal: true,
            height: dialogHeight,
            minHeight: dialogHeight,
            maxHeight: tabHeight,
            minWidth: 350,
            beforeClose: checkFavoritesReOrderSave,
            close: onCloseFavoritesDialog,
            buttons: [
                {text: getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_SAVE_ORDER), click: saveFavoritesReOrdering },
                {text: cancelButtonTitle, click: closeFavoritesDialog }
            ]
        });
    }

    PageNavFavoritesDialogConfig.confirmDialog = $('#dialog-confirm').dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        height: 165,
        width: 340,
        minWidth: 340,
        buttons: [
            {text: "OK", click: acceptFavoritesReOrderingWarning },
            {text: cancelButtonTitle, click: closeFavoritesConfirmDialog }
        ]
    });

    if (!PageNavFavoritesDialogConfig.renameFailedDialogOptions) {
        initializeDialogOptions();
    }

    dialogRoot.parent().fadeTo("fast", 1.0);
    dialogRoot.dialog('open');
}


function initializeDialogOptions() {
    PageNavFavoritesDialogConfig.confirmDiscardReorderDialogOptions = {
        title: getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_CANCEL_REORDER),
        message: getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_LOSE_ORDER_MESSAGE) +
            '<br>' + getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_LOSE_ORDER_QUESTION),
        okButtonTitle: getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_SAVE_ORDER),
        okButtonClickMethod: saveFavoritesReOrdering,
        cancelButtonTitle: getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_CANCEL),
        cancelButtonClickMethod: closeFavoritesConfirmDialog
    };
    PageNavFavoritesDialogConfig.renameFailedDialogOptions = {
        title: getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_SAVE_FAILED),
        message: getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_RENAME_FAIL_MESSAGE) +
            '<br>' + getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_RENAME_FAIL_QUESTION),
        okButtonTitle: getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_OK),
        okButtonClickMethod: closeFavoritesConfirmDialog
    };
}


function onCloseFavoritesDialog(event, ui) {
    var dialogListRoot = $(event.target).parent().find('ol');
    dialogListRoot.empty();
}


function onFavoritesSortChange ( event, ui ) {
    //var statusElem = $(event.target).parent().find(".favorites-dialog-status");
    //setFavoritesReOrderStatus(statusElem, "changed");
    setFavoritesReOrderStatusElement($(event.target).parent(), "changed");
}


function setFavoritesReOrderStatusElement(root, statusValue) {
    var statusElem = root.find(".favorites-dialog-status");
    statusElem.attr("data-status", statusValue);
}


function getFavoritesReOrderStatusElement(root) {
    var statusElem = root.find(".favorites-dialog-status");
    if (statusElem && statusElem.length > 0) {
        statusElem = statusElem[0];
    }
    else {
        statusElem = null;
    }

    return statusElem;
}


function getFavoritesReOrderStatus(statusElem) {
    var statusValue = 'unknown';
    if (statusElem && statusElem.length > 0) {
        statusValue = statusElem.attr("data-status");
    }
    return statusValue;
}


function checkFavoritesReOrderSave(event, ui) {
    var statusElem = $(event.target).parent().find(".favorites-dialog-status");
    var status = getFavoritesReOrderStatus(statusElem);
    var unsavedChanges = (status !== "initial" && status !== "saved");
    if (unsavedChanges) {
        // short nicknames for readability
        var theDialog = PageNavFavoritesDialogConfig.confirmDialog;
        var options = PageNavFavoritesDialogConfig.confirmDiscardReorderDialogOptions;
        if (options.title !== theDialog.dialog("option", "title")) {
            theDialog.dialog("option", "title", options.title);
            theDialog.find('p').html(
                    '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 40px 0;"></span>' +
                    options.message);
            theDialog.dialog("option", "buttons",[
                {text: options.okButtonTitle, click: options.okButtonClickMethod},
                {text: options.cancelButtonTitle, click: options.cancelButtonClickMethod}
            ]);
        }

        theDialog.dialog("open");
    }

    return !unsavedChanges;
}


function saveFavoriteRenaming() {
    var button = $(this);
    var portlet = button.closest(".portlet");
    var portletHeader = portlet.find(".portlet-header");
    var portletContent = button.closest(".portlet-content");

    var newName = portletContent.find("input[type='text']").attr("value");
    var activityId = portlet.attr("data-activity-id");
    var processId = portlet.attr("data-process-id");
    var taskId = portlet.attr("data-task-id");
    // clean HTML characters
    taskId = convertFromXMLValue(taskId);

    var newTaskId = convertFromXMLValue(newName);
    if (taskId.indexOf('(' + PageNavUserInfo.loggedInUser + ')') > 0) {
        newTaskId = newName + ' (' + PageNavUserInfo.loggedInUser + ')';
    }

    if (taskId === newTaskId) {
        portletContent.toggle();
        portletHeader.find(".portlet-toggle").toggleClass("ui-icon-minusthick ui-icon-plusthick");
    }
    else if (saveFavoriteRenamingToServer(activityId, processId, taskId, newTaskId)) {
        portletHeader.find("span.favorites-display-title").text(newName);
        portlet.attr("data-task-id", newTaskId);
        portletContent.toggle();
        portletHeader.find(".portlet-toggle").toggleClass("ui-icon-minusthick ui-icon-plusthick");
        refreshFavorites();
    }
    else {
        var confirmDialog = $('.dialog-confirm');
        confirmDialog.dialog("option", "title", PageNavFavoritesDialogConfig.renameFailedDialogOptions.title);
        confirmDialog.find('p').html('<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 40px 0;"></span>' +
            PageNavFavoritesDialogConfig.renameFailedDialogOptions.message);
        confirmDialog.dialog("option", "buttons" , [
             {text: PageNavFavoritesDialogConfig.renameFailedDialogOptions.okButtonTitle,
                 click: PageNavFavoritesDialogConfig.renameFailedDialogOptions.okButtonClickMethod }
         ]);
        confirmDialog.dialog("open");
    }
}


function saveFavoritesReOrdering() {
    // localized messages
    var savedStatusMessage = getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_ORDER_SAVED);
    var noopStatusMessage = getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_SAVE_NOOP);
    var failedStatusMessage = getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_ORDER_SAVE_FAILED);

    var dialogRoot = $( this );
    var favoriteObjects = [];
    var favoriteItems = dialogRoot.find("div.portlet");
    var status = getFavoritesReOrderStatus(dialogRoot.find(".favorites-dialog-status"));

    // no unsaved reordering
    if (!favoriteItems || favoriteItems.length === 0 || status === "initial" || status === "saved") {
        showStatusMessage(dialogRoot, noopStatusMessage);
        fadeDialogAndClose(dialogRoot);
        return;
    }

    var favoritesCount = favoriteItems.length;
    for (var i = 0; i < favoritesCount; i++) {
        var item = favoriteItems[i];
        favoriteObjects.push( {
            activityId: $(item).attr('data-activity-id'),
            processId: $(item).attr('data-process-id'),
            taskId: convertFromXMLValue($(item).attr('data-task-id')),
            taskFile: $(item).attr('data-task-file'),
            displayOrder: (i + 1) * 100,
            hotUserName: PageNavUserInfo.loggedInUser
        });
    }

    // call WFR with favoritesObjects to update records within ONE transaction
    var parameters = {
        viewName: 'ab-page-navigation-datasource.axvw',
        dataSourceId: 'pageNavigationFavorites_ds',
        isWritePtask: true,
        favorites: toJSON(favoriteObjects)
    };

    var reOrdered = runWorkflowRuleAndConfirm('AbSystemAdministration-updateOrderOfMyFavorites', parameters);
    if (reOrdered.success) {
        showStatusMessage(dialogRoot, savedStatusMessage);

        setFavoritesReOrderStatusElement(dialogRoot, "saved");
        refreshFavorites();

        fadeDialogAndClose(dialogRoot);
    }
    else {
        showStatusMessage(dialogRoot, failedStatusMessage);
    }
}


function saveFavoriteRenamingToServer(activityId, processId, taskId, newTaskName) {
    var status = false;

    var oldFieldValues = {
        "afm_ptasks.activity_id": activityId,
        "afm_ptasks.process_id": processId,
        "afm_ptasks.task_id": taskId,
        "afm_ptasks.hot_user_name": PageNavUserInfo.loggedInUser
    };
    var fieldValues = {
        "afm_ptasks.activity_id": activityId,
        "afm_ptasks.process_id": processId,
        "afm_ptasks.task_id": newTaskName,
        "afm_ptasks.hot_user_name": PageNavUserInfo.loggedInUser
    };

    var parameters = {
        viewName: "ab-page-navigation-datasource.axvw",
        dataSourceId: "pageNavigationFavorites_ds",
        isNewRecord: false,
        version:     "2.0",
        oldFieldValues: toJSON(oldFieldValues),
        fieldValues: toJSON(fieldValues)
    };

    var updated =  Ab.workflow.Workflow.call('AbCommonResources-saveDataRecord', parameters);
    if (updated.code === "executed") {
        status = true;
    }

    return status;
}


function cancelFavoriteRenaming() {
    var button = $(this);
    var portletHeader = button.closest(".portlet").find(".portlet-header");
    var portletContent = button.closest(".portlet-content");
    var currentTitle = portletHeader.find("span.favorites-display-title").text();

    portletContent.find("input[type='text']").attr("value", currentTitle);
    portletContent.toggle();
    portletHeader.find(".portlet-toggle").toggleClass("ui-icon-minusthick ui-icon-plusthick");
}


function closeFavoritesDialog () {
    $( this ).dialog( "close" );
}


function closeFavoritesConfirmDialog() {
    confirmDialog.dialog( "close" );
}


function showStatusMessage(rootElement, message) {
    var statusElem = getFavoritesReOrderStatusElement(rootElement);
    $(statusElem).html('<span class="status-display">' + message + '</span>');
    $(statusElem).css('display', 'block');
}


function fadeDialogAndClose(rootElement) {
    rootElement.parent().fadeTo("slow", 0.7, function() {
        rootElement.dialog( "close" );
    });
}


function getConfirmDialogHtml(confirmDialogTitle, confirmDialogMessage) {
    return '<div id="dialog-confirm" class="dialog-confirm" title="' + confirmDialogTitle + '" style="display:none;">' +
        '<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 40px 0;"></span>' +
        confirmDialogMessage +
        '</p>' +
        '</div>';
}


function acceptFavoritesReOrderingWarning() {
    // reset the status so that the main dialog can close
    var favoritesDialog = $('.favorites-dialog');
    setFavoritesReOrderStatusElement(favoritesDialog, "initial");
    $( this ).dialog( "close" );
    // close main dialog
    favoritesDialog.dialog( "close" );

}


function cancelFavoritesReOrderingWarning() {
    confirmDialog.dialog( "close" );
}



function convertFromXMLValue(stringValue) {
    if(typeof stringValue != "undefined" && stringValue != null){
        stringValue = stringValue.replace(/&amp;/g, '&');
        stringValue = stringValue.replace(/&gt;/g, '>');
        stringValue = stringValue.replace(/&lt;/g, '<');
        stringValue = stringValue.replace(/&apos;/g, '\'');
        stringValue = stringValue.replace(/&quot;/g, '\"');
        return stringValue;
    }
    return "";
}



// TODO namespace these global vars


var homeTabScroller = null;


var applicationsTabScroller = null;


var applicationsTabHtmlFragment = '3053784';


var tabsWidget;



function startInitialization() {
    $('.task-view').hide();
    tabsWidget = setUpTabs();

    // execute timing event to test whether DWR has been initialized.
    // Set user values when it is ready.
    //
    // Trigger completion of setup only after initial WFR completes
    PageNavUserInfo.redirectIntervalId = setInterval(initialWfrCall, 100);
}


function initialWfrCall() {
    if ( valueExists(dwr.engine._scriptSessionId)) {
        var userInfo = getUserInfo();

        if (userInfo && "timeout" !== userInfo.name) {
            setPageNavUserInfo(userInfo);

            if (typeof loadLocalization === 'function') {
                loadLocalization(PageNavUserInfo.localeDirectory.replace('/', ''));
            }

            if (typeof finishSetup === 'function') {
                finishSetup(userInfo);
            }

        }
        clearInterval(PageNavUserInfo.redirectIntervalId);
    }
    // else wait and do nothing but increment counter
    else {
        PageNavUserInfo.redirectWaitCounter++;
    }
}


function setUpTabs() {
    // Enable tabs. The `event` property must be overridden so that the tabs aren't changed on click,
    // and any custom event name can be specified.
    return $('#navigationTabs').tabs({ event: 'change' });
}


function loadColorScheme() {
    if (PageNavUserInfo.userInterfaceTheme && PageNavUserInfo.userInterfaceTheme.length > 0) {
        loadCSS(PageNavUserInfo.cssDirectory + PageNavUserInfo.cssFilePrefix + PageNavUserInfo.userInterfaceTheme + ".css");
    }
}


function finishSetup(userInfo) {
    setUpSearchDialog();
    setUserHeader(userInfo);
    setFragmentFileNamesFromRole(userInfo.role);
    loadColorScheme();
    setUpHistory(tabsWidget, 'ul.ui-tabs-nav a');
    positionBreadCrumbs();

    // hide the search input if on Android or iPad
    if (!useScroller()) {
        $('#searchText').hide();
    }

    $(window).resize(function() {setPageDimensions();});

    // initialize the hovertips for static elements in the DOM
    window.setTimeout(hovertipInit, 100);

    var urlParams = deparam(location.hash);
	var taskSource = urlParams.pTask || urlParams.eTask;
	if (taskSource == null || taskSource === "") {
		if (context.includeHomeTab && urlParams.navigationTabs == 0) {
			goHome();
		}
		else if (context.includeAppTab) {
			goApps();
		}
    }
}


function dynamicallyLoadCodeFile(fileName) {
    if (fileName.substr(fileName.length - 3) === '.js' || fileName.indexOf('/js?') > 0) {
        var customCodeFile = fileName;
        $.ajax({
                type: "GET",
                url: customCodeFile,
                dataType: "script",
                crossDomain: true,
                statusCode: { 404: function() { alert("page not found"); }}
            })
            .done(function() {console.log('Load of ' + fileName + ' was successful.');})
            .fail(function() {console.log( "Error loading " + fileName);});
    }
    else if (fileName.substr(fileName.length - 4) === '.css') {
        var cssLink = $("<link rel='stylesheet' type='text/css' href='" + fileName +"'>");
        $("head").append(cssLink);
        console.log('Load of ' + fileName + ' was successful.');
    }
    else {
        console.log( "Error loading " + fileName + ". It must be a js or css file.");
    }
}



function localizeDisplayText() {
    window.document.title = getLocalizedString(pageNavStrings.z_PAGENAV_WINDOW_TITLE);
    $('#breadCrumbs').html(getLocalizedString(pageNavStrings.z_PAGENAV_BREADCRUMB_LABEL) + '<b class="caret"></b>');
    var searchTip = getLocalizedString(pageNavStrings.z_PAGENAV_SEARCHBOX_INITIAL_TEXT);
    $('#searchText').attr('value', searchTip).attr('title', searchTip);
    $('#signOutLink').html(getLocalizedString(pageNavStrings.z_PAGENAV_SIGNOUT_BUTTON_LABEL));
    $('#helpLink').html(getLocalizedString(pageNavStrings.z_PAGENAV_HELP_BUTTON_LABEL));
    $('a.page-top-link').html(getLocalizedString(pageNavStrings.z_PAGENAV_BACK_TO_TOP));

    $('#userMenuProject').html(getLocalizedString(pageNavStrings.z_NAVIGATOR_USERMENU_PROJECT));
    $('#userMenuRole').html(getLocalizedString(pageNavStrings.z_NAVIGATOR_USERMENU_ROLE));
    $('#userMenuVersion').html(getLocalizedString(pageNavStrings.z_NAVIGATOR_USERMENU_VERSION));
    $('#myProfileMenuLink').html(getLocalizedString(pageNavStrings.z_NAVIGATOR_USERMENU_PROFILE));
    $("#myJobsMenuLink").html(getLocalizedString(pageNavStrings.z_NAVIGATOR_USERMENU_JOBS));
    $("#myPresentationsMenuLink").html(getLocalizedString(pageNavStrings.z_NAVIGATOR_USERMENU_PRESENTATIONS));
    $("#myEditorMenuLink").html(getLocalizedString(pageNavStrings.z_NAVIGATOR_USERMENU_EDITOR));
}


//function localizeFavoritesDisplayText() {
//    $('span.favorites-add-target').html(getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_ADD_TARGET_TEXT));
//    $('.favorites-add-target').attr('title', getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_ADD_TARGET_TOOLTIP));
//    $('span.favorites-delete-target').attr('title', getLocalizedString(pageNavStrings.z_PAGENAV_FAVORITES_DELETE_TARGET_TOOLTIP));
//    $('a.page-top-link').html(getLocalizedString(pageNavStrings.z_PAGENAV_BACK_TO_TOP));
//}


function setPageDimensions() {
    var headBannerHome = $('#headBannerHome');
    var windowElement = $(window);
    var bannerHeight = headBannerHome.height() + parseInt(headBannerHome.css('padding-top'));
    var windowHeight = windowElement.height();
    var windowWidth = windowElement.width();
    var scrollDimension = 16;

    var navigationTabs = $('#navigationTabs');
    navigationTabs.css('width', windowWidth - 2 + "px");

    var visiblePageHeight = 0;
    var homeTabView = $('#homeTabView');
    var applicationsTabView = $('#applicationsTabView');
    var applicationsProcessView = $('#applicationsProcessView');
    if (homeTabView.find('.page-row').is(":visible")) {
        homeTabView.find('.page-row').each(function() {
            visiblePageHeight += $(this).outerHeight();
        });
        visiblePageHeight += homeTabView.find('.page-break-banner').outerHeight();
    }
    else if (applicationsTabView.find('.page-row').is(":visible")) {
        applicationsTabView.find('.page-row').each(function() {
            visiblePageHeight += $(this).outerHeight();
            visiblePageHeight += parseInt($(this).css('margin-top'));
            visiblePageHeight += parseInt($(this).css('margin-bottom'));
        });
    }
    else if (applicationsProcessView.find('.page-row').is(":visible")) {
        applicationsProcessView.find('.page-row').each(function() {
            visiblePageHeight += $(this).outerHeight();
        });
    }
    var pageHeight = Math.max(visiblePageHeight + bannerHeight, windowHeight - bannerHeight - scrollDimension);
    $('.page').css("height",  pageHeight + "px");

    if (homeTabView.is(":visible")) {
        $('.task-view').css("height",  (windowHeight - bannerHeight) + "px");
    } else {
        // suppress unnecessary browser scroll bars when a task view is loaded
        $('.task-view').not(applicationsProcessView).css("height",  (windowHeight - bannerHeight - scrollDimension) + "px");

        // 3043607, applications tab 1st & 2nd page are not exactly task views but they use the class. size them differently.
        applicationsProcessView.css("height",  (pageHeight - bannerHeight + 25) + "px");
    }

    if (useScroller()) {
        // configure the home tab scroller
        ///homeTabView.css("width", (windowWidth - 22) + "px");
        ///homeTabView.parent().css("width", (windowWidth - 20) + "px");
        homeTabView.css("height",  (windowHeight - bannerHeight - 2) + "px");
        homeTabView.parent().css("height",  (windowHeight - bannerHeight) + "px");
        homeTabView.css("overflow", "hidden");
        navigationTabs.css("min-width", "0px");
        headBannerHome.css({"position":"inherit","min-width":"0px"});
        $('#bannerMenu').css("right","10px");
        if (homeTabScroller) {
            homeTabScroller.update();
        } else {
            homeTabScroller = new Ab.view.Scroller(homeTabView);
            homeTabView.css("margin-top", "0");
        }

        // configure the applications tab scroller
        var applicationsViewParent = $('#applicationsViewParent');

        ///applicationsViewParent.css("width", (windowWidth - 20) + "px");
        ///applicationsViewParent.parent().css("width", (windowWidth - 22) + "px");
        applicationsViewParent.parent().css("height",  (windowHeight - bannerHeight) + "px");
        applicationsViewParent.css("overflow", "hidden");
        if (applicationsTabScroller) {
            applicationsTabScroller.update();
        } else {
            applicationsTabScroller = new Ab.view.Scroller(applicationsViewParent);
        }

        var appTaskFrame = $('#pTaskFrame');
        var homeTaskFrame = $('#taskFrame');
        if (homeTaskFrame.is(":visible")) {
            homeTaskFrame.css("height",  (windowHeight - bannerHeight - 5) + "px");
            homeTaskFrame.css("width",  (windowWidth - 2) + "px");
        }
        else if (appTaskFrame.is(":visible")) {
            appTaskFrame.css("height",  (windowHeight - bannerHeight - 5) + "px");
            appTaskFrame.css("width",  (windowWidth - 18) + "px");
        }
        else {
            $('.task-view').css("width", (windowWidth - 20) + "px");
        }
    }
    else {
        $('.task-view').css("width", windowWidth + "px");
    }

    var searchDialog = $('#searchDialog');
    if (searchDialog.dialog( 'instance') && searchDialog.dialog('isOpen')) {
        searchDialog.dialog( 'option', 'position', {my:"right top", at:"right-15 top"});
    }
}


function useScroller() {
    return navigator.userAgent.match(/Android/i) === null && navigator.userAgent.match(/iPhone|iPad/i) === null;
}


function goHome() {
    if (signOutOnTimeOut()) { return; }

    var url = window.location.href;
    restoreTab(0);
    if (PageNavUserInfo.homeTabHtmlFragment) {
        loadQuasiFrameAndDisplay("#homeTabView", PageNavUserInfo.homeTabHtmlFragment, url.substring(0,  url.lastIndexOf(PageNavUserInfo.webAppContextPath) +
            PageNavUserInfo.webAppContextPath.length));
    }
    refreshFavorites();
}


function goApps() {
    if (signOutOnTimeOut()) { return; }

    var pTaskFrame = $('#pTaskFrame');
    if (pTaskFrame.length > 0 && $(pTaskFrame).css('display') !== 'none') {
        var currentState = $.bbq.getState();
        var newState = {};
        if (currentState.navigationTabs) {
            newState.navigationTabs = currentState.navigationTabs;
        }

        if (currentState.process) {
            newState.process = currentState.process;
        }

        $('#tabApplications').parent('li').removeClass('ui-task-tab-active').addClass('ui-state-active');
        $('#breadCrumbContent').find('.hovertip_wrap3').empty();
        $('#breadCrumbContainer').hide();

        $.bbq.pushState( newState );

        hidePTaskFrame();
        var applicationsProcessView = $('#applicationsProcessView');
        applicationsProcessView.show();
        applicationsProcessView.find('.nav-pages').show();
        setPageDimensions();
    }
    else if (PageNavUserInfo.applicationsTabHtmlFragment) {
        var url = window.location.href;
        var urlPrefix = url.substring(0,  url.lastIndexOf(PageNavUserInfo.webAppContextPath) + PageNavUserInfo.webAppContextPath.length);
        loadQuasiFrameAndDisplay("#applicationsTabView", PageNavUserInfo.applicationsTabHtmlFragment, urlPrefix);
        restoreTab(1);
    }
}


function restoreTab(index) {
    $('.nav-pages').show();
    $('#applicationsProcessView').hide();
    hideTaskFrame();
    hidePTaskFrame();
    $('#breadCrumbContainer').hide();
    $('#breadCrumbContent').find('.hovertip_wrap3').empty();

    var state = $.bbq.removeState('eTask', 'pTask', 'process');
    if (!state) {
        state = {};
    }
    state.navigationTabs = index;
    $.bbq.pushState(state);

    showTab(index);
}


function showTab(index) {
    $('#navigationTabs').tabs("option", "active", index);

    // set active tab to white
    $('ul.ui-tabs-nav > li').removeClass('ui-task-tab-active').addClass('ui-state-default');

    if (index === 0) {
        $('#homeTabView').show();
        $('#tabHome').parent('li').removeClass('ui-task-tab-active').addClass('ui-state-active');
    }
    else if (index === 1) {
        $('#applicationsTabView').show();
        $('#tabApplications').parent('li').removeClass('ui-task-tab-active').addClass('ui-state-active');
    }
    setPageDimensions();
}


function hideTaskFrame() {
    hideFrameElement('taskFrame');
}


function hidePTaskFrame() {
    hideFrameElement('pTaskFrame');
}


function hideFrameElement(frameId) {
    var frameElement = $('#' + frameId);
    if (frameElement.length > 0) {
        try {
            var frameBody = $(frameElement).contents().find('body');
            var drawingControlContainer = $(frameBody).find('div.drawing');
            if (drawingControlContainer.length > 0) {
                $(drawingControlContainer).remove();
            }
        }
        catch (e) {
            // Permissions denied error when frame contents not in server domain, such as when showing Help.
            console.log("Frame contents not in server domain.");
        }

        $(frameElement).hide();
        $(frameElement).attr('src', null);
    }
}


function resetViewsToHome() {
    $('.bread-crumb-list').remove();
    $('.nav-pages').show();
    $('.task-view').hide();
    hideTaskFrame();
    hidePTaskFrame();

    $('#navigationTabs').tabs("option", "active", 0);
}


function loadCSS(href) {
    var cssLink = $("<link>");
    cssLink.attr({
        rel: "stylesheet",
        type: "text/css",
        href: href
    });
    $("head").append(cssLink);
}


function setFragmentFileNamesFromRole(roleName) {
    var roleSuffix = getRoleDashed(roleName);
    PageNavUserInfo.applicationsTabHtmlFragment = PageNavUserInfo.generationDirectory + PageNavUserInfo.localeDirectory +
        PageNavUserInfo.roleDirectory + "applications-home-" + roleSuffix +  ".html";
}


function goToTop() {
    if (useScroller()) {
        if (homeTabScroller) {
            homeTabScroller.scrollbars.tweenTo(0,0);
        }
        if (applicationsTabScroller) {
            applicationsTabScroller.scrollbars.tweenTo(0,0);
        }
    } else {
        // this no longer works now that we use ab-scroller.js
        $("html, body").animate({ scrollTop: 0 }, 600);
    }

    return false;
}

function scrollToCorner(left, top) {
    if (useScroller()) {
        if (homeTabScroller) {
            homeTabScroller.scrollbars.scrollTo(left, top);
        }
        if (applicationsTabScroller) {
            applicationsTabScroller.scrollbars.scrollTo(left, top);
        }
    }
    else {
        // this no longer works now that we use ab-scroller.js
        $("html, body").animate({ scrollTop: top}, 600);
    }

    return false;
}


function getRecord(parameters) {
	var result = false;

	var response = getDataRecords(parameters);
	if (response && response.records.length === 1) {
		result = response.records[0];
    }

	return result;
}


function getDataRecords(parameters) {
    var returnValue = false;
    try {
        var result = Ab.workflow.Workflow.call('AbCommonResources-getDataRecords', parameters);
        if (!Ab.workflow.Workflow.sessionTimeoutDetected && result.data && result.data.records) {
            returnValue = result.data;
        }
    }
    catch (e) {
        Workflow.handleError(e);
    }

    return returnValue;
}



function setUpSearchDialog() {
    $('#searchDialog').dialog({ modal: true, autoOpen: false, draggable: false, width: 500 });
    $('.ui-dialog-title').after('<input type="text" id="searchDialogTitleInput" class="search-text" onkeypress="onSearchBoxKeyPress(event);" ' +
        'style="float:right;margin-right:30px;width:240px;">');
    $('#searchDialogTitleInput').prev('.ui-dialog-title').css('width','20%');
}


function onSearchBoxSelect(elem) {
    // clear the text & reset the color
    var val = elem.value;
    if (val === getLocalizedString(pageNavStrings.z_PAGENAV_SEARCHBOX_INITIAL_TEXT)) {
        elem.value = "";
    }

    // TODO change this hard-coded color to a css variable
    $('#searchText').css("color", "black")
}


function onSearchBoxKeyPress(evt) {
    evt = evt || window.event;
    var charCode = evt.keyCode || evt.which;
    if (charCode === 13) {
        var  searchText = $('#searchDialog').dialog('isOpen') ? $.trim($('#searchDialogTitleInput').val()) : $.trim($('#searchText').val());
        if (searchText.length > 0) {
            closeSearchDialog();
            displaySearchDialog(searchText);
        }
     }

}


function displaySearchDialog(searchText) {
    var dialogHeight = 500;
    var dialogWidth = 800;
    var searchDialogElement = $('#searchDialog');

    if (Ab.workflow.Workflow.sessionTimeoutDetected) {
        searchDialogElement.on( "dialogbeforeclose", function( event, ui ) {window.location = context.logoutView;} );
        searchDialogElement.html(getLocalizedString(pageNavStrings.z_PAGENAV_SESSION_TIME_OUT));
    }
    else {
        var drilldownSource = PageNavUserInfo.webAppContextPath + 'ab-page-navigation-view-search.axvw?searchText=' + searchText;
        searchDialogElement.html('<iframe id="alert-drilldown-frame" src="' + drilldownSource + '" width="99%" height="99%"></iframe>');

            /// var results = formatSearchResults(getViewsByName(searchText));
            /// dialogHeight = Math.max(300 , 50 * Math.min(results.count, 10));
            /// searchDialogElement.html(results.formattedSearchResults);
    }

    searchDialogElement.dialog( 'option', "position", {my:"right top", at:"right-10 top"} );
    searchDialogElement.dialog( 'option', "height", dialogHeight );
    searchDialogElement.dialog( 'option', "width", dialogWidth );

    var searchDialogTitleInput = $('#searchDialogTitleInput');
    $(searchDialogTitleInput).attr('value', searchText );

    // Localize the title and tooltip of the search dialog
    var dialogTitleElem = $(searchDialogTitleInput).siblings('.ui-dialog-title');
    var localizedSearchTitle = getLocalizedString(pageNavStrings.z_PAGENAV_SEARCHRESULT_TITLE);
    dialogTitleElem.attr('title', localizedSearchTitle);
    dialogTitleElem.html(localizedSearchTitle);

    setTimeout(function() {"use strict"; searchDialogElement.dialog('open');}, 100);

    // reinitialize the hovertips now that new targets are added to the DOM
    setTimeout(hovertipInit, 100);
}


function closeSearchDialog() {
    var searchDialogElement = $('#searchDialog');

    if (searchDialogElement.dialog('isOpen')) {
        searchDialogElement.dialog('close');
    }
}


function getViewsByName(filterText) {
    var parameters = {
        viewName: 'ab-page-navigation-view-search.axvw',
        dataSourceId: 'taskTitleSearch_pg_ds',
        groupIndex: 0,
        version: '2',
        searchString: filterText,
        useHierarchicalSecurityRestriction: true
    };

    // task view search dataSource is parameterized for localized title columns as well as the search string.
    if (PageNavUserInfo.locale.substr(0, 2) !== 'en') {
        var languageExtension = PageNavUserInfo.localeDirectory.replace('/', '');
        parameters.taskTitleColumn = 'afm_ptasks.task_' + languageExtension;
        parameters.processesTitleColumn = 'afm_processes.title_' + languageExtension;
        parameters.activitiesTitleColumn = 'afm_activities.title_' + languageExtension;
        parameters.productsTitleColumn = 'afm_products.title_' + languageExtension;
    }

    return getDataRecords(parameters);
}


function changeHomeTab(elem, event) {
    var newNav = elem.innerHTML;
    $('#tabLabelHome').html(newNav);

    // reposition tasks dropdown for new tab label
    positionBreadCrumbs();

    // TODO Set the tab's content to the element's href
    var newHomePageFile = elem.href;
    newHomePageFile = newHomePageFile.substring(newHomePageFile.lastIndexOf('/') + 1);

    PageNavUserInfo.homeTabHtmlFragment = PageNavUserInfo.generationDirectory + PageNavUserInfo.localeDirectory +
        PageNavUserInfo.roleDirectory + newHomePageFile;
    goHome();
}


function getArrayFromContextString(contextString) {
    var returnStrings = [];
    if (contextString) {
        var rawTokens = contextString.split(',');
        var tokenCount = rawTokens.length;

        for (var i = 0; i < tokenCount; i++) {
            var token = $.trim(rawTokens[i]);

            if (token.indexOf("[") === 0) {
                token = token.substr(1);
            }
            if (token.indexOf("]") === token.length - 1) {
                token = token.substr(0, token.length - 1);
            }

            returnStrings.push(token);
        }
    }

    return returnStrings;
}


function showHelp() {
    var helpWindowSettings = 'toolbar=yes,menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes,width=800,height=600,left=10,top=10';
    var helpUrl = context.archibusHelpLink.replace('(user.helpExtension)', context.userHelpExtension);

    return window.open(helpUrl, "Archibus_Help", helpWindowSettings);
}


function doSignOut() {
    // check if there are any running jobs for this user
    // ensures that Ab.workflow.Workflow.sessionTimeoutDetected is updated
    var jobs = Workflow.getJobStatusesForUser();
    // if session timeout has been detected
    if (Workflow.sessionTimeoutDetected) {
        // go directly to the signout view and do not call the service
        var currentLocation = location.href;
        var currentDir = currentLocation.substring(0, currentLocation.indexOf("schema"));
        window.location = currentDir + context.logoutView;
        return;
    }

    var running = false;
    for (var i in jobs) {
        if (jobs[i].jobFinished === false && jobs[i].jobStatusCode !== 8
        		// kb 3054127 stopped jobs should not trigger dialog & prevent signout 
        		&& jobs[i].jobStatusCode !== 6) {
            running = true;
        }
    }

    if (running) {
        // warn the user and allow to cancel the sign out
        var controller = this;
        var response = window.confirm(getLocalizedString(pageNavStrings.z_PAGENAV_SIGNOUT_JOBS_MESSAGE));
        if (response == true) {
        	controller.doLogout();
        }
    }
    else {
        this.doLogout();
    }
}


function signOutOnTimeOut() {
    var returnValue = false;
    // ensures that Ab.workflow.Workflow.sessionTimeoutDetected is updated
    var jobs = Workflow.getJobStatusesForUser();
    if (Ab.workflow.Workflow.sessionTimeoutDetected) {
        doLogout();
        returnValue = true;
    }

    return returnValue;
}


function doLogout() {
    SecurityService.logout({
        callback: function(x, y, z) {
            PageNavUserInfo.loggedInUser = '';
            var currentLocation = location.href;
            var currentDir = currentLocation.substring(0, currentLocation.indexOf("schema"));
            window.location = currentDir + context.logoutView;
        },
        errorHandler: function(message, e) {
            // DWR has its own session timeout check which may bypass our server-side timeout check
            if (message === 'Attempt to fix script session' || message.indexOf('expired') !== -1) {
                window.location = context.logoutView;
            }
        }
    });
}


function configureForSmartClient() {
    $('#signOutLink').css('display', 'none');
}


function deparam(inputString) {
	var firstChar = inputString.substr(0,1);
	var paramString = (firstChar == "?" || firstChar == "#") ? inputString.substring(1) : inputString;
	var paramArray = paramString.split('&');
	var params = {};
	var oneParam = [];
	
	for (var i = paramArray.length - 1; i >= 0; i--) {
		oneParam = paramArray[i].split("=");
		params[decodeURIComponent(oneParam[0])] = decodeURIComponent(oneParam[1]);
	}
	
	return params;
}


var PageNavReportingConfig = {
    
    capturedCount: 0,

    
    includeMapBuckets: false,

    
    bucketMapHash: null
};


function writeAppSpecificBucketReport(elem, event) {
    var spinner = $('#presentation_job_spinner');
    $(spinner).show();
    var isLeafletImageSafeBrowser = true; // isLeafletImageEnabledBrowser();
    var imagesContainer = {};
    var appSpecificBuckets = $('div.app-specific-bucket');
    var totalImageCount = appSpecificBuckets.length;
    var showMapBuckets = false;
    PageNavReportingConfig.capturedCount = 0;

    // collect all app-specific buckets' properties
    $(appSpecificBuckets).each(function (n) {
        var bucketElement = $(this).parent('.bucket-process');
        var bucketId = $(bucketElement).attr('id');
        var bucketTitle = $(bucketElement).children('h2')[0].innerHTML;
        var isMap = false;

        if ($(bucketElement).find('.pgnav-map').length > 0) {
            showMapBuckets = true;
            isMap = true;
        }

        imagesContainer[bucketId] = {
            index: n,
            title: bucketTitle,
            isMapBucket: isMap,
            images: {}
        };
    });

    // open the app-specific buckets in a dialog if
    // 1) the collection includes a map bucket;
    // 2) the browser is IE > 10, or FF/Chrome/Safari
    if (showMapBuckets && isLeafletImageSafeBrowser && PageNavReportingConfig.includeMapBuckets) {
        $(spinner).hide();
        openAppSpecificReportDialog();
    }
    // else capture all non-map buckets
    else {
        captureReportImages(imagesContainer, totalImageCount);
    }
}


function captureReportImages(imagesContainer, length) {
    var totalImageCount = length;
    for (var bucketIdentifier in imagesContainer) {
        // skip map buckets
        if (imagesContainer[bucketIdentifier].isMapBucket) {
            totalImageCount -= 1;
            continue;
        }

        // test whether bucket has multiple pages and if so add them to the total count.
        var bucketElement = $('#' + bucketIdentifier);
        var pagingControl = $(bucketElement).find('.navi');
        if (pagingControl.length > 0) {
            var pageCount = $(pagingControl).children('a').length;
            totalImageCount = totalImageCount + pageCount - 1;
        }

        captureBucketImage(bucketIdentifier, 0, imagesContainer, totalImageCount);
    }
}


function captureBucketImage(bucketId, pageIndex, imagesContainer, totalImageCount) {
    try {
        html2canvas($('#' + bucketId), {
            allowTaint: true,
            useCORS: true,
            onrendered: function (canvas) {
                addAdditionalPagesIfNeeded(bucketId, pageIndex, imagesContainer, totalImageCount);
                processCanvasToImageContainer(canvas, bucketId, pageIndex, imagesContainer, totalImageCount);
            }
        });
    }
    catch (e) {
        console.log("Error in call to html2canvas for bucket " + bucketId);
    }
}


function addAdditionalPagesIfNeeded(bucketId, pageIndex, imagesContainer, totalImageCount) {
    var bucketElement = $('#' + bucketId);
    var pagingControl = $(bucketElement).find('.navi');
    if (pagingControl.length > 0) {
        var pageCount = $(pagingControl).children('a').length;
        if (pageIndex + 1 < pageCount) {
            var api = $(bucketElement).find(".scrollable-root").data("scrollable");
            api.move(1);
            setTimeout(function(){captureBucketImage(bucketId, pageIndex + 1, imagesContainer, totalImageCount)}, 2000);
        }
    }
}


function captureMapBucketImage(mapObject, bucketId, imagesContainer, totalImageCount) {
    leafletImage(mapObject, function(err, canvas){
        // here you can use img for drawing to canvas and handling
        ///var testString = canvas.toDataURL();
        processCanvasToImageContainer(canvas, bucketId, 0, imagesContainer, totalImageCount);
    });
}


function processCanvasToImageContainer(canvas, bucketId, pageIndex, imagesContainer, totalImageCount) {
    var encodingPrefix = "base64,";

    try {
        var myImage = canvas.toDataURL();
        var contentStartIndex = myImage.indexOf(encodingPrefix) + encodingPrefix.length;
        imagesContainer[bucketId].images[pageIndex] =  myImage.substring(contentStartIndex);
        PageNavReportingConfig.capturedCount += 1;
        $('#presentation_job_spinner').hide();

        if (totalImageCount === PageNavReportingConfig.capturedCount) {
            generatePPTX(formSlidesCollectionFromImagesContainer(imagesContainer));
        }
    }
    catch (e) {
        // Permissions denied error when frame contents not in server domain, such as when showing Help.
        console.log("Exception taking canvas to image string for bucket " + bucketId + " : " + pageIndex);
        console.log(e);
    }
}


function formSlidesCollectionFromImagesContainer(imagesContainer) {
    var slides = [];
    for (var bucketIdentifier in imagesContainer) {
        // transfer image strings from each bucket's object container to its array
        imagesContainer[bucketIdentifier].imageArray = [];
        var imageCount = Object.keys(imagesContainer[bucketIdentifier].images).length;
        for (var k = 0; k < imageCount; k++) {
            imagesContainer[bucketIdentifier].imageArray.push(imagesContainer[bucketIdentifier].images[k]);
        }

        if (k > 0) {
            slides.push({
                title: imagesContainer[bucketIdentifier].title,
                images: imagesContainer[bucketIdentifier].imageArray
            });
        }
    }

    return slides;
}


function generatePPTX(slides){
    var jobId = Ab.workflow.Workflow.startJob('AbSystemAdministration-generatePaginatedReport-generatePpt', slides, {fileName: 'my-presentation.pptx'});

    var jobStatus =  Ab.workflow.Workflow.getJobStatus(jobId);
    //XXX: finished or failed
    while (jobStatus.jobFinished != true && jobStatus.jobStatusCode != 8) {
        jobStatus = Ab.workflow.Workflow.getJobStatus(jobId);
    }

    if (jobStatus.jobFinished) {
        window.location = jobStatus.jobFile.url;
    }
    $('#presentation_job_spinner').hide();
}


function openAppSpecificReportDialog() {
    var dialogHeight = 870;
    var dialogWidth = 1150;
    var frameSource = PageNavUserInfo.webAppContextPath + PageNavUserInfo.homeTabHtmlFragment.replace('.html', '-rpt.html');

    var dialogElement = $('#reportingDialog');
    dialogElement.attr('title','Graphics Sectors Report');
    dialogElement.attr('style','padding: 0');
    dialogElement.html('<iframe id="asb-report-frame" src="' + frameSource + '" width="99%" height="99%"></iframe>');

    dialogElement.dialog({ modal: true, autoOpen: false, draggable: false, width: 500 });
    var uiDialogTitle = $('.ui-dialog-title');
    $(uiDialogTitle).css('width', '75%');
    // TODO localize buttonlabel
    $(uiDialogTitle).after('<button class="ui-widget" id="reportButton" type="button" onClick="onReportButtonClick(event);" ' +
        'style="float:right;margin-right:30px;">'  + 'Run Report' + '</button>' );
    dialogElement.dialog( 'option', "position", {my:"right top", at:"right-10 top"} );
    dialogElement.dialog( 'option', "height", dialogHeight );
    dialogElement.dialog( 'option', "width", dialogWidth );
    dialogElement.dialog('open');
}

function closeAppSpecificReportDialog() {
    $('#reportingDialog').dialog('close');
}




function onReportButtonClick(evt) {
    var imagesContainer = {};
    PageNavReportingConfig.capturedCount = 0;

    // collect all app-specific buckets' properties
    var appSpecificBuckets = $('div.app-specific-bucket');
    $(appSpecificBuckets).each(function (n) {
        var bucketElement = $(this).parent('.bucket-process');
        var bucketId = $(bucketElement).attr('id');
        var bucketTitle = $(bucketElement).children('h2')[0].innerHTML;

        imagesContainer[bucketId] = {
            index: n,
            title: bucketTitle,
            images: {}
        };
    });

    captureReportImages(imagesContainer, appSpecificBuckets.length);
}



function constructRuntimeBuckets() {
    // loadMapControls() is conditionally called depending on whether there is a map control in the page.
    // include support files first to give time for the files to be loaded.
    //var includesMapControl = includeMapControlRuntimes();

    loadHorizontalBarCharts();

    loadProcessMetricsControls();

    loadAlertsBuckets();

    loadFavoritesBuckets();

    PageNavReportingConfig.bucketMapHash = loadMapControls();


    loadCustomCodeFiles();
}

//- Horizontal Bar Chart Control

function loadHorizontalBarCharts() {
    //- Loop thru each bucket having class horizontalBarChart
    $(".horizontalBarChart").each( function(n) {
        var parentContainer = $(this);
        var elBucket = parentContainer.parents(".bucket-process").attr("id");
        var elPlot = elBucket + '_plot' + n;
        var config = getHorizontalBarChartConfig(parentContainer, elBucket, n);
        if (!config.elementId) { return; }

        getDataAndConstructBarChart(config, elPlot, parentContainer);
    });
}


function getDataAndConstructBarChart(config, elPlot, parentContainer) {
    var plotData = getPlotData(config, elPlot);
    if (!plotData || plotData.length === 0) { return; }

    plotData = preprocessBarChartData(config, plotData);
    var chartMarkup = horizontalBarChartBucket_Plot(config, elPlot, plotData);
    parentContainer.append(chartMarkup);
    bindDrilldownBarHover();
}


function getHorizontalBarChartConfig(container, elBucket, index) {
	var config = {
        'elementId': elBucket,
        'bucketWidth': $("#"+elBucket).width(),
        'sortOrder': 'desc',
        'valueOnTop': 'largest',
        'abbreviateValues': 'true',
        'recordLimit': '5',
        // do not change
        'useStoplightColors': 'false',
        'subtitle': '',
        controlIndex: index
    };

    //- parameters required by process metrics charting:
    if (container.attr("metricName")) { config.metricName = container.attr("metricName");}
    if (container.attr("granularity")) { config.granularity = container.attr("granularity");}
    if (container.attr("sortOrder")) { config.sortOrder = container.attr("sortOrder");}
    if (container.attr("scorecard")) { config.scorecard = container.attr("scorecard");}
    if (container.attr("useStoplightColors")) { config.useStoplightColors = container.attr("useStoplightColors") === "true" ? "true" : "false";}

    //- common parameters:
    if (container.attr("recordLimit")) { config.recordLimit = container.attr("recordLimit");}
    if (container.attr("valueOnTop")) { config.valueOnTop = container.attr("valueOnTop");}
    if (container.attr("subtitle")) { config.subtitle = container.attr("subtitle");}
    if (container.attr("abbreviateValues")) { config.abbreviateValues = container.attr("abbreviateValues");}

    //- parameters required by adhoc charting:
    if (container.attr("viewName")) { config.viewName = container.attr("viewName");}
    if (container.attr("dataSourceId")) { config.dataSourceId = container.attr("dataSourceId");}
    if (container.attr("valueField")) { config.valueField = container.attr("valueField");}
    if (container.attr("labelField")) { config.labelField = container.attr("labelField");}

    return config;
}


function getPlotData(config, elPlot) {
    var plotData = [];
    try {
        if (valueExists(dwr.engine._scriptSessionId)) {
            var result;
            var records;
            if (config.metricName) {
                result = Ab.workflow.Workflow.callMethod('AbCommonResources-MetricsService-getTrendValuesForMetric',  config.metricName, config.granularity, config.sortOrder);
                records = result.data;
            } else {
                var parameters = { viewName: config.viewName, dataSourceId: config.dataSourceId, restriction: config.restriction };
                result = Ab.workflow.Workflow.call('AbCommonResources-getDataRecords', parameters);
                records = result.data.records;
                var valueField = config.valueField;
                var labelField = config.labelField;
            }

            if (Ab.workflow.Workflow.sessionTimeoutDetected) {
                plotData = false;
            }
            else if (!records || records.length === 0) {
                chartBucketErrorMessage(config, elPlot, 'nodata');
                plotData = false;
            }
            else {
                if (records.length < config.recordLimit) {
                    config.recordLimit = records.length;
                }

                var recordLimit = config.recordLimit;
                for (var i = 0; i < recordLimit; i++) {
                    var record = records[i];
                    if (!record) { break; }

                    var rawval = 0;
                    var value = 'N/A';
                    var barLabel = 'None';
                    var drillDownView = '';
                    if (config.metricName) {
                        rawval = record.metricValueRaw;
                        value = record.metricValue;

                        if (typeof(record.granularityValue) !== 'undefined' && config.granularity !== 'all') {
                            barLabel = record.granularityValue;
                        }
                        else {
                            barLabel = getLocalizedString(pageNavStrings.z_PAGENAV_METRICS_ALL);
                        }
                        drillDownView = record.drillDownView;
                    } else {
                        value = record[valueField];
                        rawval = record[valueField + ".raw"];
                        if (!rawval) {
                            rawval = value;
                        }
                        barLabel = record[labelField];
                    }

                    var processedRecord = {
                        //- pass as string in case of commas
                        value: convertValue(rawval.toString()),
                        formattedValue: value,
                        barLabel: barLabel,
                        pointLabel: (config.abbreviateValues === "true" && !config.metricName) ?
                            abbreviateValue(rawval.toString(), 2, 1) :
                            value,
                        //(config.abbreviateValues === "true") ? abbreviateValue(rawval.toString(), 2, 1) : rawval,
                        barClass: getBarClass(config.useStoplightColors, record.stoplightColor, rawval),
                        drillDownView: drillDownView
                    };
                    plotData.push(processedRecord);
                }

                if (isReOrderDataNeeded(config, plotData)) {
                    plotData.reverse();
                }
            }
        }
        else {
            console.log("Get plot data No DWR sessionId !");
        }
    }
    catch (e) {
        chartBucketErrorMessage(config, elPlot, 'wfrerror');
        plotData = false;
        ///Workflow.handleError(e);
    }

    return plotData;
}


function isReOrderDataNeeded(config, data) {
    var reorderNeeded = false;

    // determine sort direction of the recordset (non metrics charts are [should be] sorted in the dataSource)
    var firstValue = data[0].value;
    var lastValue = data[data.length - 1].value;

    var arrSortDir = (firstValue >= lastValue) ? 'desc' : 'asc';
    if ((arrSortDir === 'desc' && config.valueOnTop === 'smallest') || (arrSortDir === 'asc' && config.valueOnTop === 'largest')) {
        reorderNeeded = true;
    }

    return reorderNeeded;
}


function getBarClass(useStoplightColors, stoplightColor, rawValue) {
    var barClass = 'pos';

    if (useStoplightColors && useStoplightColors === 'true') {
        barClass = stoplightColor;
    } else if (rawValue < 0) {
        barClass = 'neg';
    }

    return barClass;
}


function preprocessBarChartData(config, data) {
    //- sets space for point label. Increase as required if wrapping occurs
    var maxBarLength = config.bucketWidth - 75;
    var maxValue = getMaxRecordValue(data);
    var dataCount = data.length;

    for (var i = 0; i < dataCount; i++) {
        if (data[i].value < 0) {
            data[i].value = data[i].value * -1;
        }

        data[i].barLength = (data[i].value === 0) ? 1 : (data[i].value * maxBarLength) / maxValue;
    }

    return data;
}


function horizontalBarChartBucket_Plot(config, elPlot, data) {
    //- sets the background style of the bucket
    var elemId = config.elementId;
    $("#"+elemId).addClass("bar-metrics");

    var chartHtml = '';
    if (elPlot !== 'modal-drilldown') {
        chartHtml += '<div id="' + elPlot + '" class="hchart">';
    } else {
        chartHtml += '<div class="jsscroll hchart">';
    }


    if (config.subtitle && config.subtitle.length > 0) {
        if (config.subtitle === 'hidden') {
            config.subtitle = '&nbsp;';
        }
        chartHtml += '<div class="hchart-title">' + config.subtitle + '</div>';
    }

    var dataCount = data.length;
    for (var i = 0; i < dataCount; i++) {
        var record = data[i];

		//- set the replaceable URL parameters in the drill down view, if they exist
        var drillDownViewInstance = record.drillDownView;
        var drillDownSnippet = '';
        var drillDownClass = '';
        if (drillDownViewInstance && drillDownViewInstance.length > 0) {
            var valueList = record.barLabel.split(";");
            var valueCount = valueList.length;
            for (var j = 0; j < valueCount; j++) {
                drillDownViewInstance = drillDownViewInstance.replace("{" + j + "}", valueList[j]);
            }
            drillDownSnippet = ' rel="' + drillDownViewInstance + '" onMouseUp="openAlertDrilldown(this, event);"';
            drillDownClass = 'ellipses';
        }

        chartHtml += '<div class="bar-row"><span class="bar-title">' + record.barLabel + '</span><div class="bar ' + record.barClass + ' ' + drillDownClass +
            '" style="width:' + record.barLength + 'px;"' + drillDownSnippet + '>&nbsp;</div><div class="bar-point">' + record.pointLabel + '</div></div>';
    }
    chartHtml += '</div>';

    return chartHtml;
}


function getMaxRecordValue(records) {
    var maxValue = 0;
    var recordCount = records.length;
    for (var i = 0; i < recordCount; i++) {
        var recordValue = Math.abs(records[i].value)
        if (recordValue > maxValue) {
            maxValue = recordValue;
        }
    }

    return maxValue;
}


function bindDrilldownBarHover() {
	$(".ellipses").hover( function () {
            $(this).html('...');
        },
        function () {
            $(this).html('&nbsp;');
        }
	);
}


function abbreviateValue(num, decPlaces, suffix) {
    var abbrev = [ "K", "M", "B", "T" ];
	var negSign = '';
	var number = num ? num.replace(/,/g, '') : 0;
    // in case of negative values
	if (number < 0) {
        number = Math.abs(number);
        negSign = '-';
    }

    if ( number >= 1000) {
	    for (var i = abbrev.length - 1; i >= 0; i--) {
            var size = Math.pow(10, ( i + 1) * 3);
            if (size <= number) {
                number = Math.round(number * Math.pow(10, decPlaces) / size) / Math.pow(10, decPlaces);
                if ((number === 1000) && (i < abbrev.length - 1)) {
                    number = 1;
                    i++;
                }
                number = number.toLocaleString(PageNavUserInfo.locale.replace(/_/g, '-'), { minimumFractionDigits: decPlaces });
                number = (suffix === 1) ? negSign + number + abbrev[i] : negSign + number;
                break;
            }
	    }
	}
    else {
        number = parseFloat(number).toLocaleString(PageNavUserInfo.locale.replace(/_/g, '-'), { minimumFractionDigits: decPlaces });
    }

    return number;
}


function convertValue(num) {
    var number = num ? num.replace(/,/g, '') : 0;
    return number / 1000;
}

//===========================================================


function loadProcessMetricsControls() {
    //- Loop thru each bucket having class processMetrics
    // KB 3051379 <table/> element also uses class="processMetrics" (?)
    // so specify the element name to prevent duplicate table and excess recursion
    jQuery("div.processMetrics").each( function(n) {
        var parentContainer = jQuery(this);
        var elBucket = parentContainer.parents(".bucket-process").attr("id");
        var elPlot = elBucket + '_prmetric_' + n;
        var config = getProcessMetricsConfig(parentContainer, elBucket, n);

        if ( valueExists(dwr.engine._scriptSessionId) ) {
            var plotData = getProcessMetricData(config, elPlot);
            if (plotData) {
                var markup = constructProcessMetricsMarkup(config, elPlot, plotData);
                parentContainer.append(markup);
                percentCompleteCharts(config.useStoplightColors);
                sparklineCharts(plotData, n);
                bindAssumptions();
                bindProcessMetricDrilldown();
            }
        }
    });
}


function getProcessMetricsConfig(container, elBucket, index) {
    var config = {
        'elementId': elBucket,
        'bucketWidth': jQuery("#" + elBucket).width(),
        'useStoplightColors': 'false',
        controlIndex: index
    };

    if (container.attr("granularity")) {
        config.granularity = container.attr("granularity");
    }

    if (container.attr("scorecard")) {
        config.scorecard = container.attr("scorecard");
    }

    if (container.attr("useStoplightColors")) {
        config.useStoplightColors = container.attr("useStoplightColors");
    }

    return config;
}


function getProcessMetricData(config, elPlot) {
    var plotData = null;

    try {
        var result = Ab.workflow.Workflow.callMethod('AbCommonResources-MetricsService-getTrendValuesForScorecard', config.scorecard, config.granularity);
        if (Ab.workflow.Workflow.sessionTimeoutDetected) {
            plotData = false;
        }
        else {
            plotData = [];
            var dataCount = result.data.length;
            for (var i = 0; result.data && i < dataCount; i++) {
                var record = result.data[i];

                var processedRecord = {
                    metricName: '',
                    metricTitle: encodeURIComponent(record.metricTitle),
                    metricValue: 0,
                    metricValueRaw: 0,
                    metricPreviousValues: 0,
                    metricValueChange:  0,
                    metricValueChangePerYear: 0,
                    reportLimitTarget: -1,
                    reportTrendDir: 0,
                    barClass: '',
                    description: '',
                    assumptions: '',
                    businessImplication: '',
                    recurrence: ''
                };

                // KB 3048852 when record.metricValue is undefined, we still need to set metricName, assumptions, etc.
                if (record.metricName) {
                    processedRecord.metricName = encodeURIComponent(record.metricName);
                }
                if (record.assumptions) {
                    //- replace Line breaks
                    processedRecord.assumptions = encodeURIComponent(record.assumptions)
                        .replace(/%0D%0A/g,'<br/>');
                }
                if (record.description) {
                    processedRecord.description = encodeURIComponent(record.description)
                        .replace(/%0D%0A/g, '<br/>');
                }
                if (record.businessImplication) {
                    processedRecord.businessImplication = encodeURIComponent(record.businessImplication)
                        .replace(/%0D%0A/g,'<br/>');
                }
                if (record.recurrence) {
                    processedRecord.recurrence = encodeURIComponent(record.recurrence);
                }

                if (record.metricValue) {
                    processedRecord.metricValue = record.metricValue;
                    processedRecord.metricValueRaw = record.metricValueRaw;
                    processedRecord.reportLimitTarget = record.reportLimitTarget;
                    processedRecord.metricValueChange = record.metricValueChange;
                    processedRecord.metricValueChangePerYear = record.metricValueChangePerYear;
                    processedRecord.reportTrendDir = record.reportTrendDir;
                    processedRecord.metricPreviousValues = record.metricPreviousValues + ';' + record.metricValueRaw;
                    processedRecord.metricPreviousValues = processedRecord.metricPreviousValues.replace(/,/g, '').replace(/;/g, ',');
                    if (config.useStoplightColors && config.useStoplightColors === 'true') {
                        processedRecord.barClass = record.stoplightColor;
                    }
                }

                plotData.push(processedRecord);
            }
        }
    }
    catch (e) {
        chartBucketErrorMessage(config, elPlot, 'wfrerror');
        plotData = false;
        /// Workflow.handleError(e);
    }

    return plotData;
}


function constructProcessMetricsMarkup(config, elPlot, data) {
    var markup = '';
    if (!config.elementId) { return markup; }

    var changeHeaderTitle = getLocalizedString(pageNavStrings.z_PAGENAV_METRICS_CHANGE);

    markup += '<table id="' + elPlot + '" class="process-metrics" style="width: ' + config.bucketWidth + 'px">';
    markup += '<thead><tr><th class="mtitle">' + getLocalizedString(pageNavStrings.z_PAGENAV_METRICS_METRIC) +
        '</th><th class="value">' + getLocalizedString(pageNavStrings.z_PAGENAV_METRICS_CURRENT) +
        '</th><th class="value">' + changeHeaderTitle +
        '</th><th class="value">' + changeHeaderTitle + '<br>' + getLocalizedString(pageNavStrings.z_PAGENAV_METRICS_PER_YEAR) +
        '</th><th class="chart">' + getLocalizedString(pageNavStrings.z_PAGENAV_METRICS_TARGET_PERCENT) +
        '</th><th class="spark">' + getLocalizedString(pageNavStrings.z_PAGENAV_METRICS_TREND) + '</th></tr></thead>';

    var dataCount = data.length;
    for (var rowIndex = 0; rowIndex < dataCount; rowIndex++) {
        var record = data[rowIndex];
        var pmValueCurrent = record.metricValue;
        var pmValueTarget = record.reportLimitTarget;
        var rowClass = (rowIndex % 2 === 0) ? 'even' : 'odd';
        var barClass = (config.useStoplightColors === 'true') ? record.barClass : '';
        var pChartClass = (record.reportTrendDir != 3) ? 'pchart' : '';
        var currentValue = getPmCurrentValue(pmValueCurrent, pmValueTarget, record.metricValueRaw);
        var valueType = getCurrentValueType(pmValueTarget);

        markup += '<tr class="' + rowClass + '">' +
            '<td class="pmtitle" data-description="' + record.description + '" data-assumptions="'+ record.assumptions + '" data-business-implication="' + record.businessImplication +
            '" data-recurrence="' + record.recurrence + '">' +
            '<span>' + decodeURIComponent(record.metricTitle) + '</span></td>' +
            '<td class="value">' + pmValueCurrent + '</td><td class="value ' + barClass + '">' + record.metricValueChange + '</td>' +
            '<td class="value ' + barClass + '">' + record.metricValueChangePerYear + '</td>' +
            '<td style="padding-left: 15px" class="' + pChartClass + ' processMetricDrilldown" data-metric-name="'+ record.metricName + '" data-bar-class="' + barClass + '"' +
            ' data-current-value="' + currentValue + '" data-value-type="' + valueType + '">' +
            '<div class="target"></div></td>' +
            '<td id="sparkline_' + config.controlIndex + '_' + rowIndex + '" class="sparkline">' + record.metricPreviousValues + '</td></tr>';

    }
    markup += '</table>';

    return markup;
}


function getPmCurrentValue(pmValueCurrent, pmValueTarget, metricValueRaw) {
    var value = -1;

    if (pmValueTarget > 0) {
        value = ((metricValueRaw / pmValueTarget) * 100).toFixed(0);
    } else if (pmValueTarget === 0) {
        value = pmValueCurrent;
    }

    return value;
}


function getCurrentValueType(pmValueTarget) {
    var valueType = 'none';

    if (pmValueTarget > 0) {
        valueType = 'pct';
    } else if (pmValueTarget === 0) {
        valueType = 'int';
    }

    return valueType;
}


function sparklineCharts(data, controlIndex) {
    var dataCount = data.length;
    for (var rowIndex = 0; rowIndex < dataCount; rowIndex++) {
        var record = data[rowIndex];
        var pmValueTarget = record.reportLimitTarget;
        // KB 3051792 temporary solution for 23.1ML, change to use of velocity context decimalMark in Bali 6.
        var lang = PageNavUserInfo.locale.substr(0,2);
        var decimalMark = lang === 'en' || lang === 'zh'  || lang === 'ja'  || lang === 'ko' ? '.' : ',';
        var digitGroupSeparator = decimalMark === '.' ? ',' : '.';
        var sparklineOptions = {width: '70px', normalRangeMin: pmValueTarget, normalRangeMax: pmValueTarget,
            numberDecimalMark: decimalMark, numberDigitGroupSep: digitGroupSeparator};
        var test = jQuery("#sparkline_" + controlIndex + '_' + rowIndex);
        if (test) {
        jQuery("#sparkline_" + controlIndex + '_' + rowIndex).sparkline('html', sparklineOptions);
        }
    }
}


function percentCompleteCharts(useStoplightColors) {
	jQuery('.pchart').each( function() {
		var cellWidth = jQuery(this).width();
        var dataSet = this.dataset;
        if (!dataSet) {
            dataSet = {
                currentValue: jQuery(this).attr("data-current-value"),
                valueType: jQuery(this).attr("data-value-type")
            };
        }
		var currentValue = dataSet.currentValue;
		var valueType = dataSet.valueType;
        //- add 1px to show a bar for 0 value
		var barLength = (valueType === 'pct') ? ((cellWidth * currentValue)/100)+1 : cellWidth;
		if (barLength > cellWidth) {barLength = cellWidth;}
		var cval_display = (valueType === 'pct') ? currentValue + '%' : currentValue;
		if (valueType !== 'none') {
			var cellClass = 'bar100';
			if (useStoplightColors === 'true') {
				cellClass = jQuery(this).attr("data-bar-class");
			} else {
				if (currentValue >= 0 && currentValue <= 25) { cellClass = 'bar25'; }
					else if (currentValue > 25 && currentValue <= 50) { cellClass = 'bar50'; }
					else if (currentValue > 50 && currentValue <= 75) {	cellClass = 'bar75'; }
			}
			jQuery(this).html('<div class="target" style="width: ' + cellWidth + 'px"><div class="current ' + cellClass + '" style="width:' + barLength + 'px">' +
						 '</div></div><span class="' + cellClass + '">' + cval_display + '</span>');
		}
	});
}


function chartBucketErrorMessage(config, elPlot, data) {
    if (!config.elementId) { return; }
	var error_msg = (data === 'wfrerror') ?
        getLocalizedString(pageNavStrings.z_PAGENAV_BUCKET_DEF_ERROR) :
        getLocalizedString(pageNavStrings.z_PAGENAV_NO_DATA_AVAILABLE);
	if (elPlot === 'modal-drilldown') {
		jQuery("#modal-drilldown").html(error_msg);
	} else {
	    var elemId = config.elementId;
        var chartElement = jQuery("#"+elemId);
	    jQuery("#"+elPlot).remove();
        chartElement.addClass("bar-metrics");
        chartElement.append('<div id="' + elPlot + '" class="hchart error"><span class="error">'+error_msg+'</span></div>');
	}
}


function bindProcessMetricDrilldown() {
    jQuery.contextMenu({
        selector: '.processMetricDrilldown',
        trigger: 'left',
        build: function(jQuerytrigger, e) {
            var metricName = jQuery(e.currentTarget).attr('data-metric-name');
            var metricTitle = jQuery(e.currentTarget).prevAll('.pmtitle').children().html();
            var metricName_restriction = "afm_metric_grans.metric_name='" + metricName + "'";
		    try {
				var parameters = { viewName: 'ab-metric-drilldown-granularities', dataSourceId: 'metricDrilldownData_ds', restriction: metricName_restriction };
				var result = Ab.workflow.Workflow.call('AbCommonResources-getDataRecords', parameters);

				var records = result.data.records;
		        if (Ab.workflow.Workflow.sessionTimeoutDetected) {
		            return {'name': "timeout", 'role': "timeout"};
		        }
		        else if (records && records.length > 0) {
					var drilldownItems = {};
                    var recordCount = records.length;
                    for (var i = 0; i < recordCount; i++) {
		        		var record = records[i];
		        		var gran = record['afm_metric_gran_defs.collect_group_by'];
                        var granTitle = record['afm_metric_gran_defs.granularity_title'];
						var newitem = {};
						newitem[gran] = {
							name: granTitle,
							icon: "chart",
							callback: function(key, opt) { popupBarchart(metricName, metricTitle, key);}
						};
						jQuery.extend(drilldownItems,newitem);
                        // check if gran is mapabble
                        if (gran.indexOf("bl_id") >= 0 || gran.indexOf("site_id") >= 0){
                            var newitem = {};
                            newitem[gran + ' Location'] = {
                                name: granTitle + ' Location',
                                icon: "map",
                                callback: function(key, opt) { popupMap(metricName, metricTitle, key);}
                            };
                            jQuery.extend(drilldownItems,newitem);
                        }
                        // check if gran is mappable
			        }
					return {
                        callback: function(key, options) {
                            var m = "clicked: " + key;
                            window.console && console.log(m);
                        },
                        items: drilldownItems
                    };
				}
				return false;
		    }
		    catch (e) {
		        Workflow.handleError(e);
		    }
		}
   });
}


function popupBarchart(metricName, metricTitle, granularity) {
	var newDialog = '<div id="modal-drilldown" style="padding:12px 8px"></div>';
	var drilldown =  jQuery( newDialog ).dialog({
  		title: metricTitle,
 		autoOpen: false,
 		height: 400,
 		width: 280,
 		resizable: false,
 		modal: true,
		open:  function() {	jQuery(".ui-dialog-titlebar-close").show(); },
 		close: function() {	jQuery("#modal-drilldown").remove(); }
 	});

    var config = {
    	'elementId': 'modal-drilldown',
        'bucketWidth': 260,
        'sortOrder': 'desc',
        'valueOnTop': 'largest',
        'recordLimit': '200',
        'useStoplightColors': 'true',
        'abbreviateValues': 'true',
        'subtitle': '',
        'metricName': metricName,
        'granularity': granularity
	};

	drilldown.dialog('open');
    getDataAndConstructBarChart(config, 'modal-drilldown', jQuery('#modal-drilldown') );
	Ab.view.Scroller(jQuery('.jsscroll'));

	return false;
}


function popupMap(metricName, metricTitle, granularity) {
    var newDialog = '<div id="modal-drilldown" style="padding:12px 8px"></div>';
    var drilldown =  jQuery( newDialog ).dialog({
        title: metricTitle,
        autoOpen: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        modal: true,
        open:  function() { jQuery(".ui-dialog-titlebar-close").show(); },
        close: function() { jQuery("#modal-drilldown").remove(); }
    });

    // remove ' Location' string that we added to granularity drilldown
    if (granularity.indexOf(" Location") >= 0) {
        granularity = granularity.replace(' Location', '');
    }

    // get location table for granularity
    var granularityLocation;
    if (granularity.indexOf('bl_id') >= 0 ) {
       granularityLocation = 'bl';
    } else if (granularity.indexOf('site_id') >= 0 ) {
        granularityLocation = 'site';
    }

    var config = {
        'elementId': 'modal-drilldown',
        'bucketWidth': 550,
        'sortOrder': 'desc',
        'valueOnTop': 'largest',
        'recordLimit': '200',
        'useStoplightColors': 'true',
        'abbreviateValues': 'true',
        'subtitle': '',
        'metricName': metricName,
        'granularity': granularity,
        'granularityLocation': granularityLocation,
        'mapImplementation' : 'ESRI',
        'basemapLayer' : 'World Light Gray Canvas',
        'markerRadius': '5',
        'popupMap': true
    };

    drilldown.dialog('open');
    getDataAndConstructMap(config, 'modal-drilldown', jQuery('#modal-drilldown') );
    Ab.view.Scroller(jQuery('.jsscroll'));

    return false;
}


function bindAssumptions() {
	jQuery(".pmtitle").click( function () {
        var dataSet = this.dataset;
        if (!dataSet) {
            dataSet = {
                description: jQuery(this).attr("data-description"),
                assumptions: jQuery(this).attr("data-assumptions"),
                businessImplication: jQuery(this).attr("data-business-implication"),
                recurrence: jQuery(this).attr("data-recurrence")
            };
        }
		var dialogTitle = jQuery(this).children("span").html().replace(/&amp;/g, '&');
		popupAssumptions(dataSet.description, dataSet.assumptions, dataSet.businessImplication, dataSet.recurrence, dialogTitle);
	});
}


function popupAssumptions(desc, asmp, bimp, rcur, dlgtitle) {
    var newDialog = '<div id="modal-otherdata" style="padding:16px"></div>';
    var otherdata = jQuery(newDialog).dialog({
        title: dlgtitle,
        autoOpen: false,
        height: 350,
        width: 500,
        resizable: false,
        modal: true,
        open: function () {
            jQuery(".ui-dialog-titlebar-close").show();
        },
        close: function () {
            jQuery("#modal-otherdata").remove();
        }
    });
    var content = '<div class="jsscroll" style="padding: 16px">';
    content += '<div style="font-weight: bold">' + getLocalizedString(pageNavStrings.Z_PAGENAV_METRICS_ASSUMPTIONS_DESCRIPTION) + ':</div>';
    content += '<div style="margin-bottom: 8px">' + decodeURIComponent(desc) + '</div>';
    content += '<div style="font-weight: bold">' + getLocalizedString(pageNavStrings.Z_PAGENAV_METRICS_ASSUMPTIONS) + ':</div>';
    content += '<div style="margin-bottom: 8px">' + decodeURIComponent(asmp) + '</div>';
    content += '<div style="font-weight: bold">' + getLocalizedString(pageNavStrings.Z_PAGENAV_METRICS_ASSUMPTIONS_BSN_IMPL) + ':</div>';
    content += '<div style="margin-bottom: 8px">' + decodeURIComponent(bimp) + '</div>';
    content += '<div style="font-weight: bold">' + getLocalizedString(pageNavStrings.Z_PAGENAV_METRICS_ASSUMPTIONS_RECURRENCE) + ':</div>';
    content += '<div style="margin-bottom: 8px">' + decodeURIComponent(rcur) + '</div>';
    content += '</div>';
    otherdata.dialog('open').html(content);
    Ab.view.Scroller(jQuery('.jsscroll'));

    return false;
}


function includeMapControlRuntimes() {
    var usesMapPanel = false;

    var mapPanels = jQuery("#homeTabView").find('div.pgnav-map');
    if (mapPanels && mapPanels.length > 0) {
        usesMapPanel = true;
        if (typeof loadMapControls !== 'function') {
            dynamicallyLoadCodeFile(PageNavUserInfo.webAppContextPath + "schema/ab-core/page-navigation/js/vendor/leaflet.js");
            dynamicallyLoadCodeFile(PageNavUserInfo.webAppContextPath + "schema/ab-core/page-navigation/css/vendor/leaflet.css");
            dynamicallyLoadCodeFile(PageNavUserInfo.webAppContextPath + "schema/ab-core/page-navigation/css/ab-pgnav-map.css");
            dynamicallyLoadCodeFile(PageNavUserInfo.webAppContextPath + "schema/ab-core/page-navigation/css/MarkerCluster.Default.css");
            dynamicallyLoadCodeFile(PageNavUserInfo.webAppContextPath + "schema/ab-core/page-navigation/css/MarkerCluster.css");
            dynamicallyLoadCodeFile(PageNavUserInfo.webAppContextPath + "schema/ab-core/page-navigation/js/vendor/esri-leaflet.js");
            dynamicallyLoadCodeFile(PageNavUserInfo.webAppContextPath + "schema/ab-core/page-navigation/js/vendor/google-maps-api.js");
            dynamicallyLoadCodeFile(PageNavUserInfo.webAppContextPath + "schema/ab-core/page-navigation/js/leaflet-google.js");
            dynamicallyLoadCodeFile(PageNavUserInfo.webAppContextPath + "schema/ab-core/page-navigation/js/ab-pgnav-map.js");
            dynamicallyLoadCodeFile(PageNavUserInfo.webAppContextPath + "schema/ab-core/page-navigation/js/leaflet-markercluster.js");
        }
    }

    return usesMapPanel;
}




function setBreadCrumbs(taskElement, taskType) {
    if (!taskElement || taskElement.length === 0) {
        return;
    }
    // test whether breadcrumbs already exist, as when using history back/forward
    var breadCrumbContent = $('#breadCrumbContent');
    var breadCrumbList = breadCrumbContent.find('ol');
    var breadCrumbsExist = breadCrumbList.length > 0;
    var createdBreadCrumbList = false;

    // when breadcrumbs don't already exist, create and append a select element with tasks' siblings
    if (!breadCrumbsExist) {
        // get taskFileName for link's id e.g., id="taskFile_ab-ex-rpt-grid.axvw" or id="favoritesFile_ab-ex-rpt-grid.axvw"
        /// var taskName = removeImgElemIfExists(taskElement.innerHTML);
        // div to which tip html is appended
        var target = breadCrumbContent.find('.hovertip_wrap3');

        var appTitleElems = $(taskElement).parents('.bucket-process').siblings('.application-process-title').filter(":first");
        if (appTitleElems && appTitleElems.length > 0) {
            target.append('<div class="bread-crumb-application-title">' + toCapitalized(appTitleElems.text()) + '</div>');
        }

        var procTitleElems = $(taskElement).parents('.bucket-wrapper').siblings('.process-title').filter(":first");
        if (procTitleElems && procTitleElems.length > 0) {
            target.append('<div class="bread-crumb-process-title">' + procTitleElems.text() + '</div>');
        }

        var taskTree = $(taskElement).parents('.bucket-wrapper').find('ol.ptask-labels');
        if (taskTree.length > 0) {
            // TODO append divs for multi-page navi
            var taskCount = taskTree.length;
            for (var j = 0; j < taskCount; j++) {
                var pTaskGroup = $(taskTree[j]).clone();
                $(pTaskGroup).removeClass('ptask-labels').addClass('bread-crumb-ptask-label');
                $(pTaskGroup).find('.process-tasks').removeClass('process-tasks').addClass('bread-crumb-process-tasks');
                target.append($(pTaskGroup).first());
                createdBreadCrumbList = true;
            }
        }
        else {
            // get link's sibling links' innerHTML text & link id for breadCrumbs
            var taskLabels = [];
            var taskViewFileNames = [];
            getSiblingTaskLabelsAndLinkIds(taskElement, taskLabels, taskViewFileNames);
            // form select element with options for each sibling task as breadCrumbs
            if (taskLabels.length > 1) {
                target.append(createListElementFromTaskSiblings(taskLabels, taskViewFileNames, taskType));
                createdBreadCrumbList = true;
            }
        }

        breadCrumbContent.find('a').click(function(){return false;});
    }

    if (createdBreadCrumbList) {
        $('#breadCrumbContainer').show();
    }
}



function positionBreadCrumbs() {
    var tabsStart = 285;
    var padding = 30;
    var tabAnchor = $('.ui-tabs-nav li a');
    var paddingLeft = parseFloat(tabAnchor.first().css('padding-left'));
    var paddingRight = parseFloat(tabAnchor.first().css('padding-right'));
    var tabsWidth = $('#tabLabelHome').width() + $('#tabLabelApplication').width() + (2 * paddingLeft) + (2 * paddingRight);

    $('#breadCrumbContainer').css('left', tabsStart + tabsWidth + padding);
}


function getSiblingTaskLabelsAndLinkIds(element, taskLabels, linkIds) {
    var taskListItem = $(element).parent();
    // list of all <ul> nodes' child <a> nodes
    var taskSiblingLinkItems = $(taskListItem).parent().parent().children('ol').children().children();

    // iterate over possibly longer list
    var siblingCount = taskSiblingLinkItems.length;
    for (var i = 0; i < siblingCount; i++) {
        if ($(taskSiblingLinkItems[i]).hasClass('favorites-target')) {
            continue;
        }
        var itemLabel = removeImgElemIfExists(taskSiblingLinkItems[i].innerHTML);
        if (taskLabels && taskLabels.length > 0 && taskLabels.indexOf(itemLabel) < 0) {
            taskLabels.push(itemLabel);
            linkIds.push($(taskSiblingLinkItems[i]).attr('href'));
        }
    }
}


function createListElementFromTaskSiblings(taskLabels, taskViewFileNames, taskType) {
    var listElem = '<ul class="bread-crumb-list">';
    var labelCount = taskLabels.length;
    for (var j = 0; j < labelCount; j++) {
        listElem += '<li class="bread-crumb-link"><a href="' + taskViewFileNames[j] + '" rel="' + taskType + '">' + taskLabels[j] + '</a></li>';
    }
    listElem += '</ul>';

    return listElem;
}


function breadCrumbSelectChanged(elem, event) {
    event = event || window.event;
    if (event) {
        elem = event.target || event.srcElement;
    }

    $('#breadCrumbContent').hide();

    var taskType = elem.rel;
    var url = window.location.href;
    var locationIndex = url.lastIndexOf(PageNavUserInfo.webAppContextPath);
    var href = $(elem).attr('href');
    if (href.substr(0, 7) !== 'http://' && href.lastIndexOf('/') > 0) {
        href = href.substr(href.lastIndexOf('/') + 1);
    }

    if ((taskType === 'eTask' || taskType === 'pTask') && locationIndex > 0) {
        var parentSelector = (taskType === 'eTask') ? '#tabPageNavigationHome' : '#applicationsViewParent';
        var frameId = (taskType === 'eTask') ? 'taskFrame' : 'pTaskFrame';
        var selector = '#' + frameId;
        $(selector).remove();
        $(parentSelector).prepend('<iframe class="task-view" id="' + frameId + '"></iframe>');

        var targetUrl = href.substr(0, 7) === 'http://' ? href  : url.substring(0,  locationIndex + PageNavUserInfo.webAppContextPath.length)  + href;
        $(selector).attr('src', targetUrl);
        pushState(taskType, href);
    }
    else if (taskType === 'process' && locationIndex > 0) {
        var targetSrc = window.location.href.substring(0, locationIndex + 9)  + PageNavUserInfo.generationDirectory + href;
        var applicationsProcessView = $('#applicationsProcessView');
        applicationsProcessView.empty();
        applicationsProcessView.load(targetSrc);
        applicationsProcessView.show();
        hideTaskFrame();
        hidePTaskFrame();
    }
    setPageDimensions();
}


function removeImgElemIfExists(text) {
    var imgIndex = text.indexOf("<img ");
    if (imgIndex >= 0) {
        var imgEndIndex = text.indexOf(">", imgIndex);
        text = text.substr(0, imgIndex) +  text.substr(imgEndIndex + 1);
    }

    return $.trim(text);
}


function toCapitalized(phrase) {
    var phraseWords = phrase.toLowerCase().split(' ');
    var capPhrase = '';
    for (var i = 0, word; word = phraseWords[i]; i++) {
        if ($.trim(word).length > 0) {
            capPhrase += word.charAt(0).toUpperCase() + word.substr(1) + ' ';
        }
    }

    return $.trim(capPhrase);
}





function setUserHeader(userInfo) {
    if (userInfo && userInfo.name) {
        var homeTabTitles = getArrayFromContextString(context.homeTabTitles);
        var homeTabFiles = getArrayFromContextString(context.homeTabFileNames);
        if (homeTabFiles && homeTabFiles.length > 0) {
            PageNavUserInfo.homeTabHtmlFragment = PageNavUserInfo.generationDirectory + PageNavUserInfo.localeDirectory +
                PageNavUserInfo.roleDirectory + homeTabFiles[0];
        }

        var tblHTML = '<div><table class="profile-menu" id="profileMenuTable">' +
            '<tr><td id="userMenuProject" class="profile-item-title">' + getLocalizedString(pageNavStrings.z_NAVIGATOR_USERMENU_PROJECT) + '</td></tr>' +
            '<tr><td>' + userInfo.projectName + '</td></tr>' +
            '<tr><td id="userMenuRole" class="profile-item-title">' + getLocalizedString(pageNavStrings.z_NAVIGATOR_USERMENU_ROLE) + '</td></tr>' +
            '<tr><td>' + userInfo.role + '</td></tr>' +
            '<tr><td id="userMenuVersion" class="profile-item-title">' + getLocalizedString(pageNavStrings.z_NAVIGATOR_USERMENU_VERSION) + '</td></tr>' +
            '<tr><td>V ' + userInfo.serverVersion + '</td></tr>' +
            '<tr><td style="height:1px;"></td></tr>' +
            '<tr><td><a id="myProfileMenuLink" href="ab-my-user-profile.axvw" onClick="openSoloTask(this, event);" rel="eTask">' +
            getLocalizedString(pageNavStrings.z_NAVIGATOR_USERMENU_PROFILE) + '</a></td></tr>' +
            '<tr><td><a id="myJobsMenuLink" href="ab-my-jobs.axvw" onClick="openSoloTask(this, event);" rel="eTask">' +
            getLocalizedString(pageNavStrings.z_NAVIGATOR_USERMENU_JOBS) + '</a></td></tr>' +
            '<tr><td><a id="myPresentationsMenuLink" href="ab.axvw" onClick="writeAppSpecificBucketReport(this, event);" rel="eTask">' +
            getLocalizedString(pageNavStrings.z_NAVIGATOR_USERMENU_PRESENTATIONS) + '</a>' +
            '<div id="presentation_job_spinner" style="display:none" class="spinner"></div></td></tr>';

        var usesHomePages = homeTabTitles.length > 0;
        var securityGroupCount = userInfo.groups.length;

        for (var i = 0; usesHomePages && i < securityGroupCount; i++) {
            if (userInfo.groups[i] === "SYS-VIEW-ALTER") {
                tblHTML += '<tr><td><a id="myEditorMenuLink" href="ab-pgnav-manage-homepage-editor.axvw" onClick="openPageEditorTask(this, event);" rel="eTask">' +
                    getLocalizedString(pageNavStrings.z_NAVIGATOR_USERMENU_EDITOR) + '</a></td></tr>';
                break;
            }
        }

        if (usesHomePages) {
            tblHTML += '<tr><td><hr style="margin:0.2em 0;"></td></tr>';
        }
        for (var j = 0, tabTitle; tabTitle = homeTabTitles[j]; j++) {
            tblHTML += '<tr><td><a href="' + homeTabFiles[j] + '" onclick="changeHomeTab(this,event)">' + tabTitle + '</a></td></tr>'
        }
        tblHTML += '</table></div>';

        $('#bannerMenu').append('<a id="bannerMenuUserLink">'+
                // KB 3050107 Prevent wrapping of long username in banner by CSS and substring-ing the name
            '<div class="banner-user-menu-container">' + userInfo.name.substr(0, 28).trim() + '<b class="caret"/></div></a>' +
            '<div class="hovertip" target="bannerMenuUserLink">' + '&nbsp;' + '</div>');

        var userMenuLink = $("div.hovertip[target='bannerMenuUserLink']");
        $(userMenuLink).empty();
        $(userMenuLink).append(tblHTML);

        // disable default <a/> behavior
        $('a#bannerMenuUserLink').click(function(){return false;});
        $('#profileMenuTable').find('a').click(function(){return false;});
    }
}






// TODO modularize the preamble to open[p]Task()

function openTask(elem, event) {
    event = event || window.event;
    if (event) {
        elem = event.target || event.srcElement;
    }
    if (event && (event.button === 2 || event.which === 3 ||
        signOutOnTimeOut() || wasFavoritesDrag(elem))) {
        return false;
    }

    var eTaskFile = $(elem).attr('href');
    var taskType = $(elem).attr('rel');

    if (eTaskFile && taskType) {
        // KB 3042652: store the activity and process ID for the task view; will be read in ab-view.js constructor
        window.taskInfo = getTaskInfo(elem);
        setBreadCrumbs(elem, taskType);

        if ('http' === eTaskFile.substr(0, 4) && eTaskFile.indexOf('_help/archibus_help/archibus.htm#') > 0) {
            eTaskFile = context.archibusHelpLink.substring(0, context.archibusHelpLink.indexOf('archibus_help/user') + 18) +
                context.userHelpExtension + '/archibus.htm#' +
                eTaskFile.substring(eTaskFile.indexOf('archibus.htm#') + 13);

            eTaskFile = eTaskFile.replace(/\\/g, "/");
        }
        else if ('http' !== eTaskFile.substr(0, 4)) {
            eTaskFile = eTaskFile.replace('(user.helpExtension)', context.userHelpExtension);
            setDisplayContainers('#taskFrame', eTaskFile);
        }

        pushState(taskType, eTaskFile);
        setPageDimensions();
    }

    return false;
}


function openPtask(elem, event) {
    event = event || window.event;
    if (event) {
        elem = event.target || event.srcElement;
    }
    if (event && (event.button === 2 || event.which === 3 || signOutOnTimeOut())) {
        return false;
    }

    if (wasElementDragged(elem) && $(elem).is('a')) {
        returnElementToUndraggedPosition(elem);
        return false;
    }

    var pTaskFile = $(elem).attr('href');
    var taskType = $(elem).attr('rel');

    if (pTaskFile && taskType) {
        // KB 3042652: store the activity and process ID for the task view; will be read in ab-view.js constructor
        window.taskInfo = getTaskInfo(elem);
        setBreadCrumbs(elem, taskType);
        pTaskFile = standardizeTaskFile(pTaskFile);
        pushState(taskType, pTaskFile);
        setDisplayContainers('#pTaskFrame', pTaskFile);

        var applicationsProcessView = $('#applicationsProcessView');
        applicationsProcessView.hide();
        applicationsProcessView.css('display', 'none');
        $('#applicationsTabView').hide();

        // set applications tab to blue

        setPageDimensions();
    }

    return false;
}


function openProcess(elem, event) {
    event = event || window.event;
    if (event) {
        elem = event.target || event.srcElement;
    }
    if (event.button === 2 || event.which === 3) {
        return false;
    }

    if (signOutOnTimeOut()) {
        return false;
    }

    var processFile = $(elem).attr('href');
    var taskType = $(elem).attr('rel');

    if (processFile && taskType) {
        pushState('process', processFile);
        setDisplayContainers('#applicationsProcessView', processFile);
        $('#applicationsTabView').hide();
    }

    return false;
}


function wasElementDragged(elem) {
    var wasDragged = false;
    var cssLeft = parseFloat($(elem).css('left'));
    var cssTop = parseFloat($(elem).css('top'));

    if (isNumber(cssLeft) && (Math.abs(cssLeft) > 2.0 || isNumber(cssTop)) && Math.abs(cssTop) > 2.0) {
        wasDragged = true;
    }

    return wasDragged;
}


function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}


function loadQuasiFrameAndDisplay(displayElementId, displaySourceFile, urlPrefix) {
    var frameSrc = $(displayElementId).attr('rel');
    if (!frameSrc ||
        (frameSrc && frameSrc !== displaySourceFile) ||
        (frameSrc && $(displayElementId).children().length === 0)) {
        var targetSrc = urlPrefix  + displaySourceFile;
        $(displayElementId).empty();
        $(displayElementId).attr('rel', displaySourceFile);
        $(displayElementId).load(targetSrc);
    }

    $(displayElementId).show();
}


function setDisplayContainers(displayElementId, displaySourceFile) {
    var url = window.location.href;
    var urlPrefix = url.substring(0,  url.lastIndexOf(PageNavUserInfo.webAppContextPath) + PageNavUserInfo.webAppContextPath.length);
    var targetUrl = displaySourceFile.indexOf('http') === 0 ? displaySourceFile  : urlPrefix  + displaySourceFile;

    $('ul.ui-tabs-nav > li').removeClass('ui-task-tab-active').addClass('ui-state-default');

    if (displayElementId === '#applicationsProcessView') {
        loadQuasiFrameAndDisplay(displayElementId, PageNavUserInfo.generationDirectory + PageNavUserInfo.localeDirectory +
            PageNavUserInfo.roleDirectory + displaySourceFile, urlPrefix);
        /// loadFavoritesBuckets();
        $('.nav-pages').show();
        $('#navPagesApps').hide();
        hideTaskFrame();
        hidePTaskFrame();
        $('li.ui-tabs-active').removeClass('ui-task-tab-active').addClass('ui-state-active').addClass('ui-state-default');
    }
    else if ((displayElementId === '#taskFrame' || displayElementId === '#pTaskFrame') && $(displayElementId).length) {
        var frameSrc = $(displayElementId).attr('src');
        if (!frameSrc ||
            (frameSrc && frameSrc.lastIndexOf('/') > 0 && frameSrc.substr(frameSrc.lastIndexOf('/') + 1) !== displaySourceFile)) {
            $(displayElementId).attr('src', targetUrl);
            $('.nav-pages').hide();
            $(displayElementId).show();
            $('#homeTabView').hide();
            $('li.ui-tabs-active').removeClass('ui-task-tab-active').addClass('ui-state-active').addClass('ui-state-default');
        }
    }
    else if (displayElementId === '#taskFrame') {
        showTaskFrame(displayElementId, $('#tabPageNavigationHome'), $('#homeTabView'), targetUrl);
    }
    else if (displayElementId === '#pTaskFrame') {
        showTaskFrame(displayElementId, $('#applicationsViewParent'), null, targetUrl);
    }
    setPageDimensions();
}


function showTaskFrame(displayElementId, frameParent, tabViewToHide, targetUrl) {
    $('li.ui-tabs-active').removeClass('ui-state-active').removeClass('ui-state-default').addClass('ui-task-tab-active');
    if ($(displayElementId).length === 0) {
        $(frameParent).prepend('<iframe class="task-view" id="' + displayElementId.substr(1) + '"></iframe>');
    }
    else {
        $(displayElementId).show();
    }

    // quick 'viewToolbar' before view actually loads
    ///$(taskFrameBody).append('<div class="quick-view-toolbar">&nbsp;</div>')
    var taskFrameBody = $(displayElementId).contents().find('body');
    var selectedTab = $('#navigationTabs').find('ul.ui-widget-header').find('li.ui-task-tab-active[aria-selected="true"]:first');
    var tabColor = $(selectedTab).css('background-color');
    $(taskFrameBody).append('<div style="position:relative;top:-8px;left:-10px;height:28px;width:100%;margin:0;background:' + tabColor + ';">&nbsp;</div>');

    $(displayElementId).show();
    if (tabViewToHide) {
        $(tabViewToHide).hide();
    }
    $('.nav-pages').hide();
    setPageDimensions();
    $(displayElementId).attr('src', targetUrl);
}


function openSoloTask(elem, event) {
    // if home tab exists open task in tab(0) else open it in tab(1)
    restoreTab(0);
    if ( $('#tabPageNavigationHome').length) {
        openTask(elem, event);
    }
    else {
        $(elem).attr('rel', 'pTask');
        openPtask(elem, event);
    }
}


function openPageEditorTask(elem, event) {
    var taskFile = $(elem).attr('href');
    var paramIndex = taskFile.indexOf('?');
    taskFile = paramIndex > 0 ? taskFile.substr(0, paramIndex) : taskFile;
    var descriptorPath = PageNavUserInfo.homeTabHtmlFragment;
    descriptorPath = descriptorPath.substr(descriptorPath.lastIndexOf('/') + 1);
    descriptorPath = descriptorPath.replace('.html', '.xml');
    var processId = $('#tabLabelHome').html();

    var url = taskFile + '?descriptorfile=' + descriptorPath + '&processid=' + processId;
    $(elem).attr('href', url);
    openSoloTask(elem, event);
}



function getActiveTabId() {
    var activeTabId;

    if ($('#tabHome').parent('li').hasClass('ui-state-active')) {
        activeTabId = '#tabPageNavigationHome';
    }
    else if ($('#tabApplications').parent('li').hasClass('ui-state-active')) {
        activeTabId = '#tabPageApplications';
    }
    else {
        activeTabId = 'unknown';
    }

    return activeTabId;
}


function standardizeTaskFile(filePath) {
    var standardizedPath = filePath;

    if (standardizedPath.indexOf('ab-products') === 0) {
        standardizedPath = 'schema/' + standardizedPath;
    }

    return standardizedPath;
}


function getTaskInfo(elem) {
    var taskInfo = {
        activityId: '',
        processId: '',
        taskId: $(elem).attr('data-taskId')
    };

    // a favorite has its activity and process as data attributes directly on the <a/>.
    // an ordinary task has them on the bucket's top-level <div/>.
    var elementAttrActivity = $(elem).attr('data-activityId');
    if (elementAttrActivity) {
        taskInfo.activityId = elementAttrActivity;
        taskInfo.processId = $(elem).attr('data-processId');
    }
    else {
        var bucketElem = $(elem).parents('.bucket-application,.bucket-process');
        if (bucketElem.length > 0) {
            bucketElem = $(bucketElem)[0];
            taskInfo.activityId = $(bucketElem).attr('data-activityId');
            taskInfo.processId = $(bucketElem).attr('data-processId');
        }
    }
    return taskInfo;
}


Ab.namespace('view');

Ab.view.type = 'page-navigation';


window.location.parameters = {};
// Avoid `console` errors in browsers that lack a console.
if (!(window.console && console.log)) {
    (function() {
        var noop = function() {};
        var methods = ['assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error', 'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log', 'markTimeline', 'profile', 'profileEnd', 'markTimeline', 'table', 'time', 'timeEnd', 'timeStamp', 'trace', 'warn'];
        var length = methods.length;
        var console = window.console = {};
        while (length--) {
            console[methods[length]] = noop;
        }
    }());
}

// Place any jQuery/helper plugins in here.



Ab.namespace('homepage');


Ab.homepage.EditorController = new (Base.extend({
        
        maxColumnsPerRow: 4,

        
        loadDescriptor: function (descriptorProcessId, descriptorFileName) {
            // fetch descriptor via WFR
            var descriptor = Ab.homepage.EditorServices.fetchDescriptor('', descriptorProcessId, descriptorFileName);
            this.loadDescriptorIntoEditor(descriptor);
        },

        
        loadDescriptorIntoEditor: function (descriptor) {
            var titleElem = jQuery('#pageEditorPanel_title');
            var processId = '';
            var currentDescriptorFile = '';
            var appTabView = jQuery('#applicationsTabView');
            jQuery(appTabView).empty();
            jQuery('#pageAttributes').data('abstractBlockCounter', '0');
            jQuery('#pageAttributes').data('pagePanelCounter', '0');

            if (descriptor != null && descriptor.descriptorXml != null && descriptor.descriptorXml.length > 0) {
                processId = decodeURI(descriptor.processId);
                currentDescriptorFile = descriptor.descriptorFileName;
                jQuery('#pageAttributes').data('currentDescriptorFile', currentDescriptorFile);
                jQuery('#pageAttributes').data('currentDescriptorProcess', processId);
                jQuery(appTabView).append(this.getViewFromDescriptorXml(descriptor.descriptorXml));
                jQuery(appTabView).show();

                this.initializeRowMovementControls();
                this.initializeRowHeightSelections();
                this.initializeRowDividerButtons();
                this.setRowNames();
                Ab.homepage.EditorSortable.initializeDragDropSortables();
            }
            else {
            	jQuery('#pageAttributes').data('currentDescriptorProcess', processId);
            	jQuery('#pageAttributes').data('currentDescriptorFile', currentDescriptorFile);
            }
            
            jQuery(titleElem).text(processId);
            jQuery(titleElem).attr('title', currentDescriptorFile);
            pgnavPageEditorController.isDirty = false;
        },

        
        getViewFromDescriptorXml: function (descriptorXml) {
            // parse XML string into XML Document object and then into a page model object
            var parser = new DOMParser();
            var xmlDoc = parser.parseFromString(descriptorXml, "text/xml");
            var pageModel = Ab.homepage.EditorParser.parseDescriptorToModel(xmlDoc);

            return Ab.homepage.EditorView.createDisplayFromModel(pageModel);
        },

        
        initializeRowHeightSelections: function () {
            jQuery('.row-palette').each(function () {
                var rowHeight = jQuery(this).data('height');
                var anchor = jQuery(this).find('a.row-height-' + rowHeight);
                jQuery(anchor).addClass('active');
            });
        },

        
        initializeRowMovementControls: function () {
            var rowPalettes = jQuery('div.row-palette');
            var rowCount = rowPalettes.length;
            var rowIndex = 0;
            jQuery(rowPalettes).each(function () {
                var upArrow = jQuery(this).find('a.row-move-up');
                var downArrow = jQuery(this).find('a.row-move-down');

                jQuery(upArrow).css('display', (rowIndex == 0) ? 'none' : 'block');
                jQuery(downArrow).css('display', (rowIndex == (rowCount - 1)) ? 'none' : 'block');
                rowIndex++;
            });
        },

        
        initializeRowDividerButtons: function() {
            var page = jQuery('#editorPage');
            var abstractBlocks = jQuery(page).children('div').filter('.page-row,.page-break-banner');
            var blockCount = abstractBlocks.length;
            for (var i = 1; i < blockCount; i++) {
                var block = abstractBlocks[i];
                var previous = abstractBlocks[i - 1];
                var addDividerButton = jQuery(previous).find('.page-divider-button');

                var displayValue = 'block';
                if (jQuery(block).hasClass("page-break-banner") && jQuery(previous).hasClass("page-row")) {
                    displayValue = 'none';
                }

                jQuery(addDividerButton).css('display', displayValue);
            }
        },

        
        deleteRow: function (triggerElem) {
            var parentRow = jQuery(triggerElem).closest(".page-row");
            var controller = this;
            var message = getLocalizedString(Ab.homepage.EditorView.z_MESSAGE_CONFIRM_ROW_DELETE);

            View.confirm(message, function (button) {
                if (button === 'yes') {
                    pgnavPageEditorController.isDirty = true;
                    jQuery(parentRow).remove();
                    controller.initializeRowMovementControls();
                    controller.initializeRowDividerButtons();
                    controller.setRowNames();
                }
            });
        },

        
        deleteBucket: function (triggerElem) {
            var parentBucket = jQuery(triggerElem).closest(".bucket-process");
            var message = getLocalizedString(Ab.homepage.EditorView.z_MESSAGE_CONFIRM_PANEL_DELETE);

            var controller = this;

            View.confirm(message, function (button) {
                if (button === 'yes') {
                    pgnavPageEditorController.isDirty = true;
                    jQuery(parentBucket).remove();
                    controller.addNewBucketControlWhereNeeded();
                }
            });
        },

        
        duplicateBucket: function (triggerElem) {
            pgnavPageEditorController.isDirty = true;
            var sourceBucket = jQuery(triggerElem).closest(".bucket-process");
            var bucketConfig = {attributes: Ab.homepage.EditorParser.getBucketDataAttributes(sourceBucket, false)};

            var duplicatePanelHtml = Ab.homepage.EditorView.getBucketHtml(bucketConfig);
            jQuery(sourceBucket).after(duplicatePanelHtml);

            this.flowBucketsToMaxFourColumnsPerRow();
            this.initializeRowMovementControls();
            this.addNewBucketControlWhereNeeded();
            this.setRowNames();
            Ab.homepage.EditorSortable.initializeDragDropSortables();
        },

        
        addPageDivider: function(triggerElem) {
            pgnavPageEditorController.isDirty = true;
            var triggerRow = jQuery(triggerElem).closest('div.page-row');
            var pageDividerHtml = Ab.homepage.EditorView.getPageDividerHtml({
                attributes: {
                    title: getLocalizedString(Ab.homepage.EditorView.z_TOOLTIP_ADDITIONAL_TASKS),
                    index: Ab.homepage.EditorView.getNextAbstractBlockIndex()
                }});
            jQuery(triggerRow).after(pageDividerHtml);
            jQuery(triggerElem).css('display','none');
        },

        
        editPageDividerProperties: function(triggerElem) {
            var parentDivider = jQuery(triggerElem).closest(".page-break-banner");
            var dividerId = jQuery(parentDivider).attr('id');
            var dividerAttributes = Ab.homepage.EditorParser.getBucketDataAttributes(parentDivider, false);

            pgnavPageEditorController.pageDividerForm.showInWindow({
                width: 600,
                closeButton: true,
                modal: true,
                title: getLocalizedString(Ab.homepage.EditorView.z_TOOLTIP_DIVIDER_PROPERTIES)
            });
            pgnavPageEditorController.initializePageDividerForm(dividerAttributes, dividerId);
        },

        
        deletePageDivider: function(triggerElem) {
            var parentDivider = jQuery(triggerElem).closest(".page-break-banner");
            var message = getLocalizedString(Ab.homepage.EditorView.z_MESSAGE_CONFIRM_DIVIDER_DELETE);

            View.confirm(message, function (button) {
                if (button === 'yes') {
                    pgnavPageEditorController.isDirty = true;
                    // un-hide the previous row's 'add page divider' button
                    var previousRow = jQuery(parentDivider).prev();
                    var rowPalette =  jQuery(previousRow).find('.row-palette');
                    var button = jQuery(rowPalette).find('.page-divider-button');
                    jQuery(button).css('display', 'block');

                    jQuery(parentDivider).remove();
                }
            });
        },

        
        flowBucketsToMaxFourColumnsPerRow: function () {
            var rows = jQuery('.editor-page').children('div.page-row');
            var rowCount = rows.length;
            for (var rowIndex = 0; rowIndex < rowCount; rowIndex++) {
                var row = jQuery(rows.get(rowIndex));
                var columnSpanCount = this.getRowBucketsColumnSpan(jQuery(row).children('.bucket-process'));
                while (columnSpanCount > this.maxColumnsPerRow) {
                    // when last row in rows[] -- no next -- make new row
                    if (rowIndex >= (rows.length - 1)) {
                        this.addEmptyRow();
                    }

                    var rowBuckets = jQuery(row).children('.bucket-process');
                    var rowBucketLast = jQuery(rowBuckets)[rowBuckets.length - 1];
                    var nextRowPalette = jQuery(rows.get(rowIndex + 1)).find('.row-palette');
                    jQuery(rowBucketLast).insertAfter(nextRowPalette);

                    rows = jQuery('.editor-page').children('div.page-row');
                    row = jQuery(rows.get(rowIndex));
                    columnSpanCount = this.getRowBucketsColumnSpan(jQuery(row).children('.bucket-process'))
                }
            }

            Ab.homepage.EditorSortable.initializeDragDropSortables();
        },

        
        shiftRowUp: function (triggerElem) {
            this.shiftRow(triggerElem, true);
        },

        
        shiftRowDown: function (triggerElem) {
            this.shiftRow(triggerElem, false);
        },

        
        shiftRow: function (triggerElem, shiftUp) {
            var rows = jQuery('.page-row');
            var triggerRow = jQuery(triggerElem).closest('div.page-row');
            var triggerRowId = jQuery(triggerRow).attr('id');

            var rowCount = rows.length;
            for (var i = 0; i < rowCount; i++) {
                var testRowId = jQuery(rows[i]).attr('id');
                if (triggerRowId === testRowId) {
                    pgnavPageEditorController.isDirty = true;
                    // TODO when triggerRow and previous/nextRow sandwich a divider keep the divider between the two rows after shift.
                    if (shiftUp) {
                        // exchange places of previousRow and triggerRow
                        jQuery(rows[i - 1]).before(jQuery(triggerRow));
                    }
                    else if ((i < rowCount - 1)) {
                        // exchange places of triggerRow and nextRow
                        jQuery(triggerRow).before(jQuery(rows[i + 1]));
                    }

                    this.initializeRowMovementControls();
                    this.initializeRowDividerButtons();
                    this.setRowNames();
                    break;
                }
            }
        },

        
        editBucketProperties: function (triggerElem) {
            var parentBucket = jQuery(triggerElem).closest(".bucket-process");
            var bucketId = jQuery(parentBucket).attr('id');
            var bucketAttributes = Ab.homepage.EditorParser.getBucketDataAttributes(parentBucket, false);
            var dialogRestriction = this.getRestrictionFromAttributes(bucketAttributes);

            // set properties dialog height, default is 400
            var dialogHeight = 400;
            if ('horizontalBarChart' === bucketAttributes.controltype || 'pgnav-map' === bucketAttributes.controltype) {
                dialogHeight = 500;
            }

            // TODO get controller name dynamically r.t. hard-code the name here
            View.dialogRestriction = dialogRestriction;
            pgnavPageEditorController.bucketPropertiesForm.showInWindow({
                width: 600,
                closeButton: true,
                modal: true,
                height: dialogHeight,
                title: getLocalizedString(Ab.homepage.EditorView.z_LABEL_PROPERTIES) + ': ' + bucketAttributes.title
            });
            pgnavPageEditorController.initializeBucketPropertiesForm(dialogRestriction, bucketId);
        },

        
        editBucketAssociate: function (triggerElem) {
            var parentBucket = jQuery(triggerElem).closest(".bucket-process");
            var parentBucketId = jQuery(parentBucket).attr('id');
            var bucketAttributes = Ab.homepage.EditorParser.getBucketDataAttributes(parentBucket, false);
            var restriction = this.getRestrictionFromAttributes(bucketAttributes);
            View.dialogRestriction = restriction;
            var controller = this;

            if ('process' === bucketAttributes.type && bucketAttributes.activity_id && bucketAttributes.process_id) {
                pgnavPageEditorController.assignedTasksGrid.showInWindow({
                    width: 1200,
                    modal: true,
                    closeButton: true,
                    title: getLocalizedString(Ab.homepage.EditorView.z_LABEL_TASKS_ASSIGNED) + ': ' + bucketAttributes.process_id
                });
                pgnavPageEditorController.assignedTasksGrid.refresh(restriction, false, false);
            }
            // show metrics scorecard definition dialogs
            else if ('processMetrics' === bucketAttributes.controltype || 'alertsList' === bucketAttributes.controltype) {
                View.openDialog('ab-metric-scorecard-def.axvw?scorecard=' + bucketAttributes.scorecard + '&parentview=homepageeditor', null, true, {
                    width: 1050,
                    height: 1200,
                    closeButton: false,
                    title: getLocalizedString(Ab.homepage.EditorView.z_LABEL_DEFINE_SCORECARD),
                    callback: function (currentValue) {
                        controller.updateAppSpecificScorecard(parentBucketId, currentValue);
                    }
                });
            }
            // show metrics definition dialogs
            else if ('horizontalBarChart' === bucketAttributes.controltype || 'pgnav-map' === bucketAttributes.controltype) {
                if (bucketAttributes.metricname) {
                    View.openDialog('ab-metric-metrics-def.axvw?metricname=' + bucketAttributes.metricname + '&parentview=homepageeditor', null, true, {
                        width: 1050,
                        height: 1200,
                        closeButton: false,
                        title: getLocalizedString(Ab.homepage.EditorView.z_LABEL_DEFINE_METRIC),
                        callback: function (currentValue) {
                            controller.updateAppSpecificMetric(parentBucketId, currentValue);
                        }
                    });
                }
                else if (bucketAttributes.viewname) {
                    View.showMessage(getLocalizedString(Ab.homepage.EditorView.z_TOOLTIP_VIEWNAME_CHART_EDIT));
                }
            }
            else if ('process' === bucketAttributes.type) {
                View.showMessage(getLocalizedString(Ab.homepage.EditorView.z_MESSAGE_ASSIGN_ACTIVITY_PROCESS));
            }
            else {
                View.showMessage(getLocalizedString(Ab.homepage.EditorView.z_MESSAGE_ASSIGN_CONTROL_TYPE));
            }
        },

        
        updateAppSpecificMetric: function(parentBucketId, currentValue) {
            pgnavPageEditorController.isDirty = true;
            var parentBucket = jQuery('#' + parentBucketId);
            jQuery(parentBucket).data('metricname', currentValue);
            var idSuffix = parentBucketId.substr('bucket_'.length);
            var fieldValues = jQuery(parentBucket).data();
            jQuery('#table_' + idSuffix).replaceWith(Ab.homepage.EditorView.getAppSpecificBucketPropertiesTable(idSuffix, fieldValues));
        },

        
        updateAppSpecificScorecard: function(parentBucketId, currentValue) {
            pgnavPageEditorController.isDirty = true;
            var parentBucket = jQuery('#' + parentBucketId);
            jQuery(parentBucket).data('scorecard', currentValue);
            var idSuffix = parentBucketId.substr('bucket_'.length);
            var fieldValues = jQuery(parentBucket).data();
            jQuery('#table_' + idSuffix).replaceWith(Ab.homepage.EditorView.getAppSpecificBucketPropertiesTable(idSuffix, fieldValues));
        },

        
        addNewBucketControlWhereNeeded: function () {
            var rows = jQuery('.editor-page').children('div.page-row');
            var rowCount = rows.length;
            for (var rowIndex = 0; rowIndex < rowCount; rowIndex++) {
                var row = jQuery(rows.get(rowIndex));
                var columnSpanCount = this.getRowBucketsColumnSpan(jQuery(row).children('.bucket-process'));
                var rowsNewBucketControl = jQuery(row).children('.new-panel-container');
                if (columnSpanCount < this.maxColumnsPerRow && rowsNewBucketControl.length === 0) {
                    jQuery(row).append(Ab.homepage.EditorView.getNewBucketControlHtml());
                }
                else if (columnSpanCount == this.maxColumnsPerRow && rowsNewBucketControl.length > 0) {
                    jQuery(rowsNewBucketControl).css('display', 'none');
                }
                else if (columnSpanCount < this.maxColumnsPerRow && rowsNewBucketControl.length === 1) {
                    // ensure new panel control ('.new-panel-container') is last child.
                    var rowChildren = jQuery(row).children();
                    var lastChild = rowChildren[rowChildren.length - 1];
                    if (jQuery(lastChild).hasClass('bucket-process')) {
                        jQuery(rowsNewBucketControl).remove();
                        jQuery(row).append(Ab.homepage.EditorView.getNewBucketControlHtml());
                    }
                    else if (jQuery(lastChild).hasClass('new-panel-container') && 'none' === jQuery(lastChild).css('display')) {
                        jQuery(lastChild).css('display', 'block');
                    }
                }
            }
        },

        
        addNewRow: function () {
            this.addEmptyRow();
            this.setRowNames();
            this.initializeRowMovementControls();
            //this.initializeRowDividerButtons();
            //this.initializeRowHeightSelections();
            Ab.homepage.EditorSortable.initializeDragDropSortables();
        },

        
        addEmptyRow: function () {
            pgnavPageEditorController.isDirty = true;
            var newRowIndex =  Ab.homepage.EditorView.getNextAbstractBlockIndex();
            var rowPaletteConfig = {
                index: newRowIndex,
                attributes: {height: 'half'}
            };
            var newRowHtml = '<div class="page-row half-height" id="pageRow_' + newRowIndex + '">' +
                Ab.homepage.EditorView.getRowPaletteHtml(rowPaletteConfig) +

                // Ab.homepage.EditorView.getEmptyBucketHtml() +
                    
                Ab.homepage.EditorView.getNewBucketControlHtml() +
                '</div>';
            jQuery('#newRowControlContainer').before(newRowHtml);

            return newRowIndex;
        },

        
        addEmptyBucketToRow: function (triggerElem) {
            var rowElem = jQuery(triggerElem).closest('div.page-row');
            if (rowElem && rowElem.length > 0) {
                var columnSpan = this.getRowBucketsColumnSpan(jQuery(rowElem).children('.bucket-process'));
                // ensure total column spans before event < 4
                if (columnSpan >= this.maxColumnsPerRow) {
                    alert("You've had enough!");
                    return;
                }

                pgnavPageEditorController.isDirty = true;
                var rowNewBucketControl = jQuery(rowElem).find('.new-panel-container');
                jQuery(rowNewBucketControl).before(Ab.homepage.EditorView.getEmptyBucketHtml());

                columnSpan = this.getRowBucketsColumnSpan(jQuery(rowElem).children('.bucket-process'));
                if (columnSpan >= this.maxColumnsPerRow) {
                    jQuery(rowNewBucketControl).hide();
                }
            }
        },

        
        getRowBucketsColumnSpan: function (buckets) {
            var columnSpan = 0;
            var bucketCount = buckets.length;
            for (var j = 0; j < bucketCount; j++) {
                columnSpan += this.getBucketColumnSpan(buckets[j]);
            }

            return columnSpan;
        },

        
        getBucketColumnSpan: function (bucket) {
            var columnSpan = 1;

            var colSpnAttribute = bucket.attributes['columnSpan'] || bucket.attributes['columnspan'];
            var colSpnData = jQuery(bucket).data('columnspan') || jQuery(bucket).data('columnSpan');
            if ((colSpnAttribute && colSpnAttribute === '2') || (colSpnData && colSpnData === 2)) {
                columnSpan = 2;
            }

            return columnSpan;
        },

        
        setRowNames: function () {
            var rowPalettes = jQuery('.row-palette');
            var index = 1;
            jQuery(rowPalettes).each(function (n) {
                var titleElem = jQuery(this).find('h2');
                var titleContent = jQuery(titleElem).html();
                var newContent = titleContent.substr(0, titleContent.indexOf(' ')) + ' ' + index + titleContent.substr(titleContent.indexOf('<a '));
                jQuery(titleElem).html(newContent);
                index++;
            });
        },

        
        setRowHeight: function (triggerElem, rowIndex, height) {
            pgnavPageEditorController.isDirty = true;
            var row = jQuery("#pageRow_" + rowIndex);
            var wrapperElem = jQuery(triggerElem).closest('div.bucket-wrapper');
            jQuery(row).removeClass("three-quarter-height half-height full-height");
            jQuery(row).addClass(height + "-height");
            jQuery(row).children(".row-palette").data('height', height);
            jQuery(wrapperElem).find('a.row-height').removeClass('active');
            jQuery(triggerElem).addClass('active');
        },

        
        getRestrictionFromAttributes: function (bucketAttributes) {
            var dialogRestriction = new Ab.view.Restriction();
            for (var attributeName in bucketAttributes) {
                if (bucketAttributes.hasOwnProperty(attributeName)) {
                    dialogRestriction.addClause('afm_ptasks.' + attributeName, bucketAttributes[attributeName], "=");
                }
            }

            return dialogRestriction;
        }
    })
);


Ab.namespace('homepage');


Ab.homepage.EditorSortable = new (Base.extend({
        
        initializeDragDropSortables: function () {
            // sort buckets, even across rows
            jQuery('.page-row').sortable({
                handle: '.process-title',
                containment: '#editorPage',
                cancel: '.row-palette',
                connectWith: ".page-row",
                helper: "clone",
                placeholder: "sortable-placeholder",
                update: Ab.homepage.EditorSortable.onChange
            });
        },

        
        onChange: function (event, ui) {
            Ab.homepage.EditorController.flowBucketsToMaxFourColumnsPerRow();
            Ab.homepage.EditorController.addNewBucketControlWhereNeeded();
            Ab.homepage.EditorController.initializeRowMovementControls();
            Ab.homepage.EditorController.setRowNames();
            pgnavPageEditorController.isDirty = true;
        }
    })
);



Ab.namespace('homepage');


Ab.homepage.EditorParser = new (Base.extend({
        
        descriptorAttributeNames: JSON.parse('{"activityid": "activity_id",' +
            '"abbreviatevalues": "abbreviateValues",' +
            '"backgroundshading": "backgroundShading",' +
            '"basemaplayer": "basemapLayer",' +
            '"columnspan": "columnSpan",' +
            '"controltype": "controlType",' +
            '"datasourceid": "dataSourceId",' +
            '"granularitylocation": "granularityLocation",' +
            '"labelfield": "labelField",' +
            '"mapimplementation": "mapImplementation",' +
            '"markerradius": "markerRadius",' +
            '"metricname": "metricName",' +
            '"processid": "process_id",' +
            '"recordlimit": "recordLimit",' +
            '"sortorder": "sortOrder",' +
            '"type": "type",' +
            '"usestoplightcolors": "useStoplightColors",' +
            '"valuefield": "valueField",' +
            '"valueontop": "valueOnTop",' +
            '"viewname": "viewName"}'),

        
        parseDescriptorToModel: function (xmlDoc) {
            var model = {};
            model.abstractNavBlocks = [];
            if (xmlDoc != null) {
                var root = xmlDoc.getElementsByTagName("navigation-page");
                if (root.length > 0) {
                    // for IE use childNodes r.t. children even though it also returns text and comment nodes
                    var blocks = root[0].childNodes;
                    var blockCount = blocks.length;
                    for (var i = 0; blocks != null && i < blockCount; i++) {
                        var block = blocks[i];
                        // filter out text and comment child nodes
                        if (block.nodeName !== 'row' && block.nodeName !== 'page-divider') {
                            continue;
                        }
                        model.abstractNavBlocks.push({
                            type: block.nodeName,
                            index: i,
                            attributes: this.getAttributesObject(block.attributes),
                            buckets: this.parseBucketCollection(block.childNodes)
                        });
                    }
                }
            }

            return model;
        },

        
        getAttributesObject: function (attributesNodeMap) {
            var attributeObject = {};
            var attributeCount = attributesNodeMap.length;
            for (var i = 0; i < attributeCount; i++) {
                var node = attributesNodeMap.item(i);
                var nodeName = node.name.toLowerCase().replace('_', '').replace('-', '');
                attributeObject[nodeName] = node.value;

                if ("height" === node.name && "three-quarters" === node.value) {
                    attributeObject[node.name] = "three-quarter";
                }
            }
            return attributeObject;
        },

        
        parseBucketCollection: function (bucketCollection) {
            var bucketArray = [];
            var bucketCount = bucketCollection.length;
            for (var j = 0; bucketCollection != null && j < bucketCount; j++) {
                var bucketElem = bucketCollection[j];
                if (bucketElem.nodeName !== 'bucket') {
                    continue;
                }
                bucketArray.push({
                    type: bucketElem.getAttribute("type"),
                    title: bucketElem.getAttribute("title"),
                    index: j,
                    // turn NamedNodeMap into a simple (attribute name indexed) object
                    attributes: this.getAttributesObject(bucketElem.attributes)
                });
            }

            return bucketArray;
        },

//===============

        
        parseDisplayToModel: function () {
            var pageModel = {};
            pageModel.abstractNavBlocks = [];
            var editorPageNodes = jQuery('.editor-page');
            if (editorPageNodes.length > 0 && editorPageNodes[0].hasChildNodes) {
                //
                // TODO class="page-break-banner" || class="page-row
                var abstrNavBlockElements = jQuery(editorPageNodes[0]).children('div');
                var blockCount = abstrNavBlockElements.length;
                for (var i = 0; i < blockCount; i++) {
                    var block = abstrNavBlockElements[i];
                    var isPgRow = jQuery(block).hasClass("page-row");
                    var isPgDivider = jQuery(block).hasClass("page-break-banner");

                    pageModel.abstractNavBlocks.push({
                        index: i,
                        blockType: isPgRow ? "row" : isPgDivider ? "page-divider" : "rule",
                        attributes: this.getAbstractNavBlockAttributes(block, isPgRow, isPgDivider),
                        buckets: []
                    });

                    if (isPgRow) {
                        // add page-row to model
                        var rowBuckets = jQuery(block).find(".bucket-process");
                        // add row buckets to model
                        var bucketCount = rowBuckets.length;
                        for (var j = 0; j < bucketCount; j++) {
                            // form bucket objects and push onto array
                            pageModel.abstractNavBlocks[i].buckets.push({
                                index: j,
                                id: jQuery(rowBuckets[j]).attr("id"),
                                title: jQuery(rowBuckets[j]).attr("title"),
                                attributes: this.getBucketDataAttributes(rowBuckets[j], true)
                            });
                        }
                    }
                }
            }
            else {
                alert("No editor-page found!");
            }

            return pageModel;
        },

        
        parseDisplayToXml: function () {
            var pageModel = this.parseDisplayToModel();
            var descriptorXml = '<navigation-page>';
            var blockCount = pageModel.abstractNavBlocks.length;
            for (var i = 0; i < blockCount; i++) {
                var abstractNavBlock = pageModel.abstractNavBlocks[i];
                if ("page-divider" === abstractNavBlock.blockType) {
                    // TODO modularize these two conditions
                    descriptorXml += '<page-divider';
                    if (abstractNavBlock.attributes.title) {
                        descriptorXml += ' title="' + abstractNavBlock.attributes.title + '"';
                    }
                    if (abstractNavBlock.attributes.backgroundshading){
                        descriptorXml += ' backgroundShading="' + abstractNavBlock.attributes.backgroundshading + '"';
                    }
                    descriptorXml += '></page-divider>';
                }
                else if ("row" === abstractNavBlock.blockType) {
                    descriptorXml += '<row height="' + abstractNavBlock.attributes.height + '">';
                    var bucketCount = abstractNavBlock.buckets.length;
                    for (var j = 0; j < bucketCount; j++) {
                        var bucketAttributes = abstractNavBlock.buckets[j].attributes;
                        descriptorXml += '<bucket ';
                        for (var attributeName in bucketAttributes) {
                            if (bucketAttributes.hasOwnProperty(attributeName) &&
                                ("" !== bucketAttributes[attributeName] || 'activityid' === attributeName || 'processid' === attributeName)) {
                                descriptorXml += ' ' + attributeName + '="' + bucketAttributes[attributeName] + '"';
                            }
                        }
                        descriptorXml += '/>';
                    }
                    descriptorXml += '</row>';
                }
            }
            descriptorXml += '</navigation-page>';

            return descriptorXml;
        },

        
        getAbstractNavBlockAttributes: function (abstractNavBlock, isPgRow, isPgDivider) {
            var attributes = {};
            var dataAttr = jQuery(abstractNavBlock).data();
            if (isPgDivider) {
                for (attributeName in dataAttr) {
                    attributes[attributeName] = dataAttr[attributeName];
                }
                //var textNode = jQuery(abstractNavBlock).find("div.page-break-archibus > span");
                //if (textNode.length === 1) { attributes.title = dataAttr.title; }
                //attributes.backgroundshading = dataAttr.backgroundshading;
            }
            else if (isPgRow) {
                var isHalfHt = jQuery(abstractNavBlock).hasClass("half-height");
                var isThreeQuarterHt = jQuery(abstractNavBlock).hasClass("three-quarter-height");
                var isFullHt = jQuery(abstractNavBlock).hasClass("full-height");

                // alternate method. above more efficient?
                ///var rowPalette = jQuery(abstractNavBlock).children('.row-palette');
                ///var dataHt = jQuery(rowPalette).data('height');

                attributes.height = isHalfHt ? 'half' : isThreeQuarterHt ? 'three-quarters' : isFullHt ? 'full' : '';
            }

            return attributes;
        },

        
        getBucketDataAttributes: function (bucketElem, forServer) {
            var cleanedAttributes = {};
            var bucketAttributes = jQuery(bucketElem).data();

            for (var attributeName in bucketAttributes) {
                if (bucketAttributes.hasOwnProperty(attributeName)) {
                    var formattedName = this.descriptorAttributeNames[attributeName] ? this.descriptorAttributeNames[attributeName] : attributeName;
                    var formattedNameLowerCase = formattedName.toLowerCase();
                    if ('sortableitem' === formattedNameLowerCase || 'rowindex' === formattedNameLowerCase || 'columnindex' === formattedNameLowerCase) {
                        continue;
                    }

                    // writing descriptor to server uses camel-cased attribute names, client-side process uses all lowercase
                    formattedName = forServer ? formattedName : formattedNameLowerCase;
                    // TODO make data value XML-compatible e.g., '&' -> parse error
                    cleanedAttributes[formattedName] = convertToXMLValue(bucketAttributes[attributeName]);
                }
            }

            return cleanedAttributes;
        },

        
        ensureDescriptorFileNameValidity: function(descriptorFileName) {
            var validName = descriptorFileName.trim();
            while (validName.indexOf(' ') > 0) {
                validName = validName.replace(' ', '-');
            }

            if (validName && validName.length > 0) {
                var index = validName.toLowerCase().indexOf('.xml');
                if (index < 0 || index !== (validName.length - 4)) {
                    validName = validName + '.xml';
                }
            }

            return validName;
        }
    })
);




Ab.namespace('homepage');


Ab.homepage.EditorServices = new (Base.extend({
    
    fetchDescriptor: function (activityId, processId, filename) {
        var descriptorObject = {};
        try {
            var result = Workflow.callMethod('AbSystemAdministration-HomePageDescriptorService-getPageDescriptorFile', activityId, processId, filename);
            descriptorObject = JSON.parse(result.jsonExpression);
            descriptorObject.processId = processId;
        }
        catch (e) {
            Workflow.handleError(e);
        }

        return descriptorObject;
    },


    
    saveDescriptor: function (filename, descriptorXml) {
        var resultCode = 'na';
        try {
            var result = Workflow.callMethod('AbSystemAdministration-HomePageDescriptorService-savePageDescriptorFile', filename, descriptorXml);
            resultCode = result.code;
        }
        catch (e) {
            resultCode = e.code;
            // let the view or form show exception as a validation error
            // Workflow.handleError(e);
        }

        return resultCode;
    },

    
    copyDescriptorProcess: function (parameters) {
        var resultCode = 'na';
        try {
            var result = Workflow.callMethod('AbSystemAdministration-HomePageDescriptorService-copyPageDescriptorFile', parameters);
            resultCode = result.code;
        }
        catch (e) {
            resultCode = e.code;
            // let the form show exception as a validation error
            // Workflow.handleError(e);
        }

        return resultCode;
    },

    
    createLinkingRoleProcsRecord: function(parameters) {
        var resultCode = 'na';
        try {
            var result = Workflow.callMethod('AbSystemAdministration-HomePageDescriptorService-createRoleProcsRecord', parameters);
            resultCode = result.code;
        }
        catch (e) {
            resultCode = e.code;
            Workflow.handleError(e);
        }

        return resultCode;
    },

    
    saveAddedProcessTasks: function (taskRecords, parameters) {
        var resultCode = 'na';
        try {
            var dataSet = new Ab.data.DataSetList();
            dataSet.addRecords(taskRecords);
            var result = Workflow.callMethod('AbSystemAdministration-HomePageDescriptorService-saveTransferredTaskRecords', dataSet, parameters);
            resultCode = result.code;
        }
        catch (e) {
            resultCode = e.code;
            Workflow.handleError(e);
        }

        return resultCode;
    },

    
    publishDescriptor: function (descriptorName, processId, title) {
        var returnValue = false;
        try {
            var result = Ab.workflow.Workflow.call('AbCommonResources-publishHomePageForRole', {
                descriptorFileName: descriptorName,
                title: title,
                processId: processId
            });

            if (Ab.workflow.Workflow.sessionTimeoutDetected) {
                returnValue = false;
            }
            else if (result.data && result.data.value > 0) {
                // TODO localize
                View.showMessage("Publishing error: " + result.data.message);
                returnValue = false;
            }
            else {
                returnValue = true;
            }
        }
        catch (e) {
            Workflow.handleError(e);
        }

        return returnValue;
    }
}));



Ab.namespace('homepage');


Ab.homepage.EditorView = new (Base.extend({

        // @begin_translatable
        z_LABEL_TITLE: 'Title',
        z_LABEL_EDIT: 'Edit',
        z_LABEL_EDIT_PROPERTIES: 'Edit Properties',
        z_LABEL_EDIT_TASKS: 'Edit Tasks',
        z_LABEL_EDIT_METRICS: 'Edit Metrics',
        z_LABEL_BACK_TO_TOP: 'Back to Top',
        z_LABEL_ROW: 'Row',
        z_LABEL_NEW_PANEL: 'New Panel',
        z_LABEL_NEW_ROW: 'New Row',
        z_LABEL_PROCESS: 'Process',
        z_LABEL_CONTROL_TYPE: 'Control Type',
        z_LABEL_PROPERTIES: 'Properties',
        z_LABEL_TASKS_ASSIGNED: 'Tasks Assigned',
        z_LABEL_DEFINE_SCORECARD: 'Define Scorecard',
        z_LABEL_DEFINE_METRIC: 'Define Metric',
        z_MESSAGE_CONFIRM_DIVIDER_DELETE: 'Do you really want to delete this page divider?',
        z_MESSAGE_CONFIRM_ROW_DELETE: 'Do you really want to delete this whole row?',
        z_MESSAGE_CONFIRM_PANEL_DELETE: 'Do you really want to delete this panel?',
        z_MESSAGE_ASSIGN_ACTIVITY_PROCESS: 'Assign an activity and process.',
        z_MESSAGE_ASSIGN_CONTROL_TYPE: 'Assign a type and control type.',
        z_TOOLTIP_ADDITIONAL_TASKS: 'Additional Tasks',
        z_TOOLTIP_DIVIDER_PROPERTIES: 'Page Divider Properties',
        z_TOOLTIP_DELETE_ROW: 'Delete row',
        z_TOOLTIP_DUPLICATE_PANEL: 'Duplicate panel',
        z_TOOLTIP_DELETE_PANEL: 'Delete panel',
        z_TOOLTIP_VIEWNAME_CHART_EDIT: 'A chart based on viewname and datasource is completely editable in the properties form.',
        z_TOOLTIP_FAVORITES_EDIT: 'A favorites panel is completely editable in the properties form.',
        z_TOOLTIP_MOVE_UP: 'Move up',
        z_TOOLTIP_MOVE_DOWN: 'Move down',
        z_TOOLTIP_HALF_HEIGHT: 'Half height',
        z_TOOLTIP_THREE_QUARTER_HEIGHT: 'Three quarter height',
        z_TOOLTIP_FULL_HEIGHT: 'Full height',
        z_TOOLTIP_ADD_PANEL: 'Add a new panel',
        z_TOOLTIP_ADD_ROW: 'Add a new row',
        z_TOOLTIP_ADD_PAGE_DIVIDER: 'Add page divider',
        z_TOOLTIP_DELETE_DIVIDER: 'Delete divider',
        // @end_translatable

        
        createDisplayFromModel: function (descriptorModel) {
            var displayHtml = '<div class="nav-pages"><div id="editorPage" class="editor-page">';
            var blocks = descriptorModel.abstractNavBlocks;
            var blockCount = blocks.length;

            for (var i = 0; i < blockCount; i++) {
                var block = blocks[i];
                if ("page-divider" === block.type) {
                    displayHtml += this.getPageDividerHtml(block);
                }
                else if ("row" === block.type) {
                    displayHtml += this.getPageRowHtml(block, this.getNextAbstractBlockIndex());
                }
                else if ("rule" === block.type) {
                    // TODO
                    ///displayHtml += getHorizontalRuleHtml(block);
                }
            }

            displayHtml += this.getNewRowControlHtml() +
                '</div></div>';

            return displayHtml;
        },

        
        getPageDividerHtml: function (block) {
            // add data attributes including title, backgroundshading -- all editable properties.
            var backgroundshading = block.attributes.backgroundshading ? block.attributes.backgroundshading : '';

            return '<div class="page-break-banner"  id="pageDivider_' + block.index + '" data-title="' + block.attributes.title +
                '" data-backgroundshading="' + backgroundshading + '">' +
                '<div class="page-break-palette">' +
                '<button type="button" class="page-divider-properties" onmouseup="Ab.homepage.EditorController.editPageDividerProperties(this);">' +
                '<span>' + getLocalizedString(this.z_LABEL_EDIT) + '</span></button>' +
                '<a class="page-divider-delete bucket-image-delete" onmouseup="Ab.homepage.EditorController.deletePageDivider(this)"' +
                'title="' + getLocalizedString(this.z_TOOLTIP_DELETE_DIVIDER) + '"/>' +
                '</div>' +
                '<div class="page-break-archibus">' +
                '<div class="page-break-logo"></div>' +
                '<span>' + block.attributes.title + '</span>' +
                '</div>' +
                '<a class="page-top-link" onClick="goToTop();">' + getLocalizedString(this.z_LABEL_BACK_TO_TOP) + '</a>' +
                '</div>';
        },

        
        getPageRowHtml: function (rowBlock, rowIndex) {
            var rowHeight = rowBlock.attributes.height;
            var displayHtml = '<div class="page-row ' + rowHeight + '-height" id="pageRow_' + rowIndex + '">';

            // row palette
            displayHtml += this.getRowPaletteHtml({
                index: rowIndex,
                attributes: {height: rowHeight}
            });

            // buckets
            var columnSpan = 0;
            var bucketCount = rowBlock.buckets.length;
            for (var j = 0; j < bucketCount; j++) {
                displayHtml += this.getBucketHtml(rowBlock.buckets[j]);
                columnSpan += Ab.homepage.EditorController.getBucketColumnSpan(rowBlock.buckets[j]);
            }

            // new bucket button
            if (rowBlock.buckets.length < 4 && columnSpan < 4) {
                displayHtml += this.getNewBucketControlHtml();
            }
            displayHtml += '</div>';

            return displayHtml;
        },

        
        getRowPaletteHtml: function (bucketConfig) {
            var attributes = bucketConfig.attributes;
            var index = bucketConfig.index;

            var rowPaletteHtml = '<div id="rowPalette_' + index + '" class="row-palette"';
            rowPaletteHtml += this.getElementDataAttributesHtml(attributes);
            rowPaletteHtml += ' oncontextmenu="return false;">';
            rowPaletteHtml += '<h2 class="process-title">' + getLocalizedString(this.z_LABEL_ROW) + index +
                '<a class="row-palette-title-menu bucket-image-delete" onmouseup="Ab.homepage.EditorController.deleteRow(this)" ' +
                'title="' + getLocalizedString(this.z_TOOLTIP_DELETE_ROW) + '"></a>' +
                '</h2>' +
                '<div class="bucket-wrapper" id="wrapper_rowPalette_' + index + '">' +
                '<table class="row-palette-controls">' +
                '<tr>' +
                '<td class="row-palette-shift"><a class="row-move-up" title="' + getLocalizedString(this.z_TOOLTIP_MOVE_UP) +
                '" onmouseup="Ab.homepage.EditorController.shiftRowUp(this)" ></a></td>' +
                '<td><a class="row-move-down" title="' + getLocalizedString(this.z_TOOLTIP_MOVE_DOWN) +
                '" onmouseup="Ab.homepage.EditorController.shiftRowDown(this)" /></td>' +
                '</tr>';
            rowPaletteHtml +=
                '<tr><td>&nbsp;</td></tr>' +
                '<tr><td colspan="2"><a class="row-height row-height-half" title="' + getLocalizedString(this.z_TOOLTIP_HALF_HEIGHT) +
                '" onmouseup="Ab.homepage.EditorController.setRowHeight(this,' + index + ',\'half\')"></a></td></tr>' +
                '<tr><td colspan="2"><a class="row-height row-height-three-quarter" title="' + getLocalizedString(this.z_TOOLTIP_THREE_QUARTER_HEIGHT) +
                '" onmouseup="Ab.homepage.EditorController.setRowHeight(this,' + index + ',\'three-quarter\')"></a></td></tr>' +
                '<tr><td colspan="2"><a class="row-height row-height-full" title="' + getLocalizedString(this.z_TOOLTIP_FULL_HEIGHT) +
                '" onmouseup="Ab.homepage.EditorController.setRowHeight(this,' + index + ',\'full\')"></a></td></tr>' +
                '</table>';
            rowPaletteHtml += '<table class="row-palette-divider-button">' +
                '<tr><td>' +
                '<a class="page-divider-button" title="' + getLocalizedString(this.z_TOOLTIP_ADD_PAGE_DIVIDER) +
                '" onmouseup="Ab.homepage.EditorController.addPageDivider(this)"></a>' +
                '</td></tr>' +
                '</table>';
            rowPaletteHtml += '</div>' +
                '</div>';

            return rowPaletteHtml;
        },

        
        getElementDataAttributesHtml: function (attributes) {
            var dataAttributesHtml = '';
            for (var attributeName in attributes) {
                if (attributes.hasOwnProperty(attributeName)) {
                    dataAttributesHtml += ' data-' + attributeName + '="' + attributes[attributeName] + '"';
                }
            }

            return dataAttributesHtml;
        },

        
        getBucketHtml: function (bucket) {
            var bucketHtml = '';
            var bucketType = bucket.attributes.type;

            if ("process" === bucketType) {
                bucketHtml = this.getProcessBucketHtml(bucket);
            }
            else if ("app-specific" === bucketType) {
                bucketHtml = this.getAppSpecificBucketHtml(bucket);
            }
            else if ("favorites" === bucketType) {
                bucketHtml = this.getFavoritesBucketHtml(bucket);
            }
            return bucketHtml;
        },

        
        getProcessBucketHtml: function (bucket) {
            var attributes = bucket.attributes;
            var activityId = attributes.activityid || attributes.activity_id ;
            var processId = attributes.processid || attributes.process_id;
            var idSuffix = activityId + '_' + getCamelCasedString(processId) + '_b_' + this.getNextPanelIndex();

            var bucketHtml = '<div id="bucket_' + idSuffix + '" class="bucket-process"' +
                this.getElementDataAttributesHtml(attributes) +
                ' oncontextmenu="return false;">' +
                this.getBucketTitleHtml(attributes);

            bucketHtml += '<div class="bucket-wrapper process-bucket" id="wrapper_' + idSuffix + '">' +
                this.getProcessBucketPropertiesTable(idSuffix, activityId, processId, attributes.controltype) +
                this.getBucketActionsTable(attributes) +
                '</div></div>';

            return bucketHtml;
        },

        
        getNextAbstractBlockIndex: function() {
            var pageAttributes = jQuery('#pageAttributes');
            var maxBlockIndex = jQuery(pageAttributes).data('abstractBlockCounter');
            jQuery(pageAttributes).data('abstractBlockCounter', ++maxBlockIndex);

            return maxBlockIndex;
        },

        
        getNextPanelIndex: function() {
            var pageAttributes = jQuery('#pageAttributes');
            var maxPanelIndex = jQuery(pageAttributes).data('pagePanelCounter');
            jQuery(pageAttributes).data('pagePanelCounter', ++maxPanelIndex);

            return maxPanelIndex;
        },

        
        getAppSpecificBucketHtml: function (bucket) {
            var attributes = bucket.attributes;
            var idSuffix = getCamelCasedString(attributes.title) + '_b_' + this.getNextPanelIndex();
            // TODO handle triple-wide or full-width if we allow
            var columnSpan = attributes.columnspan;
            var bucketCssClass = (columnSpan && (columnSpan === 2 || columnSpan === "2")) ? 'bucket-process double-wide' : 'bucket-process';

            var bucketHtml = '<div id="bucket_' + idSuffix + '" class="' + bucketCssClass + '"' +
                this.getElementDataAttributesHtml(attributes) +
                ' oncontextmenu="return false;">' +
                this.getBucketTitleHtml(attributes);

            bucketHtml += '<div class="bucket-wrapper app-specific-bucket" id="wrapper_' + idSuffix + '">' +
                this.getAppSpecificBucketPropertiesTable(idSuffix, attributes) +
                this.getBucketActionsTable(attributes) +
                '</div></div>';

            return bucketHtml;
        },

        
        getFavoritesBucketHtml: function (bucket) {
            var panelIndex = this.getNextPanelIndex();
            var bucketHtml = '<div id="homeFavoritesBucket_f_' + panelIndex + '" class="bucket-process" ' +
                this.getElementDataAttributesHtml(bucket.attributes) +
                'oncontextmenu="return false;">';
            bucketHtml += this.getBucketTitleHtml(bucket.attributes);

            bucketHtml += '<div class="bucket-wrapper favorites-bucket" id="wrapper_favorites_' + panelIndex + '">' +
                this.getFavoritesBucketPropertiesTable() +
                this.getBucketActionsTable(bucket.attributes) +
                '</div></div>';

            return bucketHtml;
        },

        
        getBucketTitleHtml: function (attributes) {
            var attrTitle = attributes.title;
            var displayTitle = attrTitle;
            // TODO make the real calculation (font, width, etc. simulation and removal)
            if (attrTitle.length > 28 && (!attributes.columnspan || parseInt(attributes.columnspan, 10) < 2)) {
                displayTitle = attrTitle.substr(0,27) + '...';
            }

            var titleHtml = '<h2 class="process-title" title="' + attributes.tooltip + '">' +
                '<span class="bucket-title" title="' + attrTitle + '">' + displayTitle + '</span>' +
                '<table class="bucket-title-menu">' +
                '<tr><td>';

            if ('process' === attributes.type || 'app-specific' === attributes.type) {
                titleHtml += '<a class="bucket-image-duplicate panel-title-button" title="' + getLocalizedString(this.z_TOOLTIP_DUPLICATE_PANEL) +
                    '" onmouseup="Ab.homepage.EditorController.duplicateBucket(this)"/>' +
                    '</td><td>';
            }

            titleHtml += '<a class="bucket-image-delete panel-title-button" title="' + getLocalizedString(this.z_TOOLTIP_DELETE_PANEL) +
                '" onmouseup="Ab.homepage.EditorController.deleteBucket(this)"/>' +
                '</td></tr>' +
                '</table></h2>';

            return titleHtml;
        },

        
        getProcessBucketPropertiesTable: function (idSuffix, activityId, processId, controltype) {
            var activity = activityId ? activityId : "";
            var process = processId ? processId : "";
            return '<table id="table_' + idSuffix + '" class="bucket-properties-table">' +
                '<tr> <td><b>' + getLocalizedString(this.z_LABEL_PROCESS) + ': </b></td></tr>' +
                '<tr><td>' + activity + '</td></tr>' +
                '<tr><td>' + process + '</td></tr>' +

                this.getBucketTypeImageHTML(controltype) +

                '</table>';
        },

        
        getAppSpecificBucketPropertiesTable: function (idSuffix, attributes) {
            var tableHtml = '<table id="table_' + idSuffix + '" class="bucket-properties-table">' +
                ' <tr> <td><b>' + getLocalizedString(this.z_LABEL_CONTROL_TYPE) + ':</b></td></tr>' +
                '<tr><td>' + attributes.controltype + '</td> </tr>';

            if (attributes.scorecard) {
                tableHtml += '<tr>' +
                        ///'<td><b>Scorecard: </b></td></tr>' +
                    '<tr><td>' + attributes.scorecard + '</td> </tr>';
            }
            if (attributes.metricname) {
                tableHtml += '<tr>' +
                        ///'<td><b>Metric Name: </b></td></tr>' +
                    '<tr><td>' + attributes.metricname + '</td> </tr>';
            }
            if (attributes.granularity) {
                tableHtml += '<tr>' +
                        ///'<td><b>Granularity: </b></td></tr>' +
                    '<tr><td>' + attributes.granularity + '</td></tr>';
            }

            tableHtml += this.getBucketTypeImageHTML(attributes.controltype);
            tableHtml += '</table>';

            return tableHtml;
        },

        
        getFavoritesBucketPropertiesTable: function () {
            return '<div class="favorites-drop-target">' +
                '<span class="favorites-add-target"></span>' +
                '<span class="favorites-delete-target">&nbsp;</span>' +
                '</div>' +
                '<div class="favorites"></div>';
        },

        
        getBucketTypeImageHTML: function (controlType) {
            var bucketImageHtml = '<tr><td>';

            if (!controlType || '' === controlType) {
                bucketImageHtml += '<a class="bucket-thumbnail-process" ></a>';
            }
            else if (controlType === 'processMetrics') {
                bucketImageHtml += '<a class="bucket-thumbnail-metrics-scorecard" ></a>';
            }
            else if (controlType === 'horizontalBarChart') {
                bucketImageHtml += '<a class="bucket-thumbnail-bar-chart" ></a>';
            }
            else if (controlType === 'alertsList') {
                bucketImageHtml += '<a class="bucket-thumbnail-alerts-list" ></a>';
            }
            else if (controlType === 'metricsChart') {
                bucketImageHtml += '<a class="bucket-thumbnail-metric-chart" ></a>';
            }
            else if (controlType === 'pgnav-map') {
                bucketImageHtml += '<a class="bucket-thumbnail-map" ></a>';
            }
            // TODO and favorites?

            bucketImageHtml += '</td></tr>';

            return bucketImageHtml;
        },

        
        getBucketActionsTable: function (attributes) {
            var disableAssociateButton = '';
            var associateTooltip = '';
            var labelText = "app-specific" === attributes.type ?
                [getLocalizedString(Ab.homepage.EditorView.z_LABEL_EDIT_PROPERTIES), getLocalizedString(Ab.homepage.EditorView.z_LABEL_EDIT_METRICS)]:
                [getLocalizedString(Ab.homepage.EditorView.z_LABEL_EDIT_PROPERTIES), getLocalizedString(Ab.homepage.EditorView.z_LABEL_EDIT_TASKS)];

            // a bar chart based on viewname, datasourceId, etc. is editable completely through the properties form.
            if ('horizontalBarChart' === attributes.controltype && attributes.viewname) {
                disableAssociateButton = ' disabled="disabled"';
                associateTooltip = ' title="' + getLocalizedString(this.z_TOOLTIP_VIEWNAME_CHART_EDIT) + '"';
            }
            else if ('favorites' === attributes.type) {
                disableAssociateButton = ' disabled="disabled"';
                associateTooltip = ' title="' + getLocalizedString(this.z_TOOLTIP_FAVORITES_EDIT) + '"';
            }

            return '<table class="bucket-properties-buttons">' +
                '<tr>' +
                '<td><button type="button" class="prop-button" onclick="Ab.homepage.EditorController.editBucketProperties(this);">' +
                '<span>' + labelText[0] + '</span></button></td>' +
                '<td class="spacer">&nbsp;</td>' +
                '<td><button type="button" class="assoc-button" onclick="Ab.homepage.EditorController.editBucketAssociate(this);"' +
                disableAssociateButton + associateTooltip + '><span>' + labelText[1] + '</span></button></td>' +
                '</tr>' +
                '</table>';
        },

        
        getEmptyBucketHtml: function () {
            var bucketConfig = {
                attributes: {
                    title: getLocalizedString(this.z_LABEL_TITLE),
                    type: '',
                    activityId: '',
                    processId: '',
                    scorecard: '',
                    metricname: '',
                    granularity: '',
                    columnspan: 1,
                    tooltip: ''
                }
            };

            return this.getProcessBucketHtml(bucketConfig);
        },

        
        getNewBucketControlHtml: function () {
            return '<div id="newBucketControlContainer_' + this.getNextPanelIndex() + '"  class="new-panel-container">' +
                '<button type="button" title="' + getLocalizedString(this.z_TOOLTIP_ADD_PANEL) +
                '" onclick="Ab.homepage.EditorController.addEmptyBucketToRow(this)">' +
                '<span>' + getLocalizedString(this.z_LABEL_NEW_PANEL) + '</span>' +
                '</button>' +
                '</div>';
        },

        
        getNewRowControlHtml: function () {
            return '<div id="newRowControlContainer" class="new-row-container">' +
                '<button type="button" title="' + getLocalizedString(this.z_TOOLTIP_ADD_ROW) +
                '" onclick="Ab.homepage.EditorController.addNewRow();" >' +
                '<span>' + getLocalizedString(this.z_LABEL_NEW_ROW) + '</span>' +
                '</button>' +
                '</div>';
        },

        
        showNewRowControl: function (show) {
            var displayStyle = show ? 'block' : 'none';
            jQuery('#newRowControlContainer').css('display', displayStyle);
        },

        
        showNewBucketControl: function (show) {
            var displayStyle = show ? 'block' : 'none';
            jQuery('.new-panel-container').css('display', displayStyle);
        }
    })
);



!function(t,e,i){var n=t.L,o={};o.version="0.7.3","object"==typeof module&&"object"==typeof module.exports?module.exports=o:"function"==typeof define&&define.amd&&define(o),o.noConflict=function(){return t.L=n,this},t.L=o,o.Util={extend:function(t){var e,i,n,o,s=Array.prototype.slice.call(arguments,1);for(i=0,n=s.length;n>i;i++){o=s[i]||{};for(e in o)o.hasOwnProperty(e)&&(t[e]=o[e])}return t},bind:function(t,e){var i=arguments.length>2?Array.prototype.slice.call(arguments,2):null;return function(){return t.apply(e,i||arguments)}},stamp:function(){var t=0,e="_leaflet_id";return function(i){return i[e]=i[e]||++t,i[e]}}(),invokeEach:function(t,e,i){var n,o;if("object"==typeof t){o=Array.prototype.slice.call(arguments,3);for(n in t)e.apply(i,[n,t[n]].concat(o));return!0}return!1},limitExecByInterval:function(t,e,i){var n,o;return function s(){var a=arguments;return n?void(o=!0):(n=!0,setTimeout(function(){n=!1,o&&(s.apply(i,a),o=!1)},e),void t.apply(i,a))}},falseFn:function(){return!1},formatNum:function(t,e){var i=Math.pow(10,e||5);return Math.round(t*i)/i},trim:function(t){return t.trim?t.trim():t.replace(/^\s+|\s+$/g,"")},splitWords:function(t){return o.Util.trim(t).split(/\s+/)},setOptions:function(t,e){return t.options=o.extend({},t.options,e),t.options},getParamString:function(t,e,i){var n=[];for(var o in t)n.push(encodeURIComponent(i?o.toUpperCase():o)+"="+encodeURIComponent(t[o]));return(e&&-1!==e.indexOf("?")?"&":"?")+n.join("&")},template:function(t,e){return t.replace(/\{ *([\w_]+) *\}/g,function(t,n){var o=e[n];if(o===i)throw new Error("No value provided for variable "+t);return"function"==typeof o&&(o=o(e)),o})},isArray:Array.isArray||function(t){return"[object Array]"===Object.prototype.toString.call(t)},emptyImageUrl:"data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="},function(){function e(e){var i,n,o=["webkit","moz","o","ms"];for(i=0;i<o.length&&!n;i++)n=t[o[i]+e];return n}function i(e){var i=+new Date,o=Math.max(0,16-(i-n));return n=i+o,t.setTimeout(e,o)}var n=0,s=t.requestAnimationFrame||e("RequestAnimationFrame")||i,a=t.cancelAnimationFrame||e("CancelAnimationFrame")||e("CancelRequestAnimationFrame")||function(e){t.clearTimeout(e)};o.Util.requestAnimFrame=function(e,n,a,r){return e=o.bind(e,n),a&&s===i?void e():s.call(t,e,r)},o.Util.cancelAnimFrame=function(e){e&&a.call(t,e)}}(),o.extend=o.Util.extend,o.bind=o.Util.bind,o.stamp=o.Util.stamp,o.setOptions=o.Util.setOptions,o.Class=function(){},o.Class.extend=function(t){var e=function(){this.initialize&&this.initialize.apply(this,arguments),this._initHooks&&this.callInitHooks()},i=function(){};i.prototype=this.prototype;var n=new i;n.constructor=e,e.prototype=n;for(var s in this)this.hasOwnProperty(s)&&"prototype"!==s&&(e[s]=this[s]);t.statics&&(o.extend(e,t.statics),delete t.statics),t.includes&&(o.Util.extend.apply(null,[n].concat(t.includes)),delete t.includes),t.options&&n.options&&(t.options=o.extend({},n.options,t.options)),o.extend(n,t),n._initHooks=[];var a=this;return e.__super__=a.prototype,n.callInitHooks=function(){if(!this._initHooksCalled){a.prototype.callInitHooks&&a.prototype.callInitHooks.call(this),this._initHooksCalled=!0;for(var t=0,e=n._initHooks.length;e>t;t++)n._initHooks[t].call(this)}},e},o.Class.include=function(t){o.extend(this.prototype,t)},o.Class.mergeOptions=function(t){o.extend(this.prototype.options,t)},o.Class.addInitHook=function(t){var e=Array.prototype.slice.call(arguments,1),i="function"==typeof t?t:function(){this[t].apply(this,e)};this.prototype._initHooks=this.prototype._initHooks||[],this.prototype._initHooks.push(i)};var s="_leaflet_events";o.Mixin={},o.Mixin.Events={addEventListener:function(t,e,i){if(o.Util.invokeEach(t,this.addEventListener,this,e,i))return this;var n,a,r,h,l,u,c,d=this[s]=this[s]||{},p=i&&i!==this&&o.stamp(i);for(t=o.Util.splitWords(t),n=0,a=t.length;a>n;n++)r={action:e,context:i||this},h=t[n],p?(l=h+"_idx",u=l+"_len",c=d[l]=d[l]||{},c[p]||(c[p]=[],d[u]=(d[u]||0)+1),c[p].push(r)):(d[h]=d[h]||[],d[h].push(r));return this},hasEventListeners:function(t){var e=this[s];return!!e&&(t in e&&e[t].length>0||t+"_idx"in e&&e[t+"_idx_len"]>0)},removeEventListener:function(t,e,i){if(!this[s])return this;if(!t)return this.clearAllEventListeners();if(o.Util.invokeEach(t,this.removeEventListener,this,e,i))return this;var n,a,r,h,l,u,c,d,p,_=this[s],m=i&&i!==this&&o.stamp(i);for(t=o.Util.splitWords(t),n=0,a=t.length;a>n;n++)if(r=t[n],u=r+"_idx",c=u+"_len",d=_[u],e){if(h=m&&d?d[m]:_[r]){for(l=h.length-1;l>=0;l--)h[l].action!==e||i&&h[l].context!==i||(p=h.splice(l,1),p[0].action=o.Util.falseFn);i&&d&&0===h.length&&(delete d[m],_[c]--)}}else delete _[r],delete _[u],delete _[c];return this},clearAllEventListeners:function(){return delete this[s],this},fireEvent:function(t,e){if(!this.hasEventListeners(t))return this;var i,n,a,r,h,l=o.Util.extend({},e,{type:t,target:this}),u=this[s];if(u[t])for(i=u[t].slice(),n=0,a=i.length;a>n;n++)i[n].action.call(i[n].context,l);r=u[t+"_idx"];for(h in r)if(i=r[h].slice())for(n=0,a=i.length;a>n;n++)i[n].action.call(i[n].context,l);return this},addOneTimeEventListener:function(t,e,i){if(o.Util.invokeEach(t,this.addOneTimeEventListener,this,e,i))return this;var n=o.bind(function(){this.removeEventListener(t,e,i).removeEventListener(t,n,i)},this);return this.addEventListener(t,e,i).addEventListener(t,n,i)}},o.Mixin.Events.on=o.Mixin.Events.addEventListener,o.Mixin.Events.off=o.Mixin.Events.removeEventListener,o.Mixin.Events.once=o.Mixin.Events.addOneTimeEventListener,o.Mixin.Events.fire=o.Mixin.Events.fireEvent,function(){var n="ActiveXObject"in t,s=n&&!e.addEventListener,a=navigator.userAgent.toLowerCase(),r=-1!==a.indexOf("webkit"),h=-1!==a.indexOf("chrome"),l=-1!==a.indexOf("phantom"),u=-1!==a.indexOf("android"),c=-1!==a.search("android [23]"),d=-1!==a.indexOf("gecko"),p=typeof orientation!=i+"",_=t.navigator&&t.navigator.msPointerEnabled&&t.navigator.msMaxTouchPoints&&!t.PointerEvent,m=t.PointerEvent&&t.navigator.pointerEnabled&&t.navigator.maxTouchPoints||_,f="devicePixelRatio"in t&&t.devicePixelRatio>1||"matchMedia"in t&&t.matchMedia("(min-resolution:144dpi)")&&t.matchMedia("(min-resolution:144dpi)").matches,g=e.documentElement,v=n&&"transition"in g.style,y="WebKitCSSMatrix"in t&&"m11"in new t.WebKitCSSMatrix&&!c,P="MozPerspective"in g.style,L="OTransition"in g.style,x=!t.L_DISABLE_3D&&(v||y||P||L)&&!l,w=!t.L_NO_TOUCH&&!l&&function(){var t="ontouchstart";if(m||t in g)return!0;var i=e.createElement("div"),n=!1;return i.setAttribute?(i.setAttribute(t,"return;"),"function"==typeof i[t]&&(n=!0),i.removeAttribute(t),i=null,n):!1}();o.Browser={ie:n,ielt9:s,webkit:r,gecko:d&&!r&&!t.opera&&!n,android:u,android23:c,chrome:h,ie3d:v,webkit3d:y,gecko3d:P,opera3d:L,any3d:x,mobile:p,mobileWebkit:p&&r,mobileWebkit3d:p&&y,mobileOpera:p&&t.opera,touch:w,msPointer:_,pointer:m,retina:f}}(),o.Point=function(t,e,i){this.x=i?Math.round(t):t,this.y=i?Math.round(e):e},o.Point.prototype={clone:function(){return new o.Point(this.x,this.y)},add:function(t){return this.clone()._add(o.point(t))},_add:function(t){return this.x+=t.x,this.y+=t.y,this},subtract:function(t){return this.clone()._subtract(o.point(t))},_subtract:function(t){return this.x-=t.x,this.y-=t.y,this},divideBy:function(t){return this.clone()._divideBy(t)},_divideBy:function(t){return this.x/=t,this.y/=t,this},multiplyBy:function(t){return this.clone()._multiplyBy(t)},_multiplyBy:function(t){return this.x*=t,this.y*=t,this},round:function(){return this.clone()._round()},_round:function(){return this.x=Math.round(this.x),this.y=Math.round(this.y),this},floor:function(){return this.clone()._floor()},_floor:function(){return this.x=Math.floor(this.x),this.y=Math.floor(this.y),this},distanceTo:function(t){t=o.point(t);var e=t.x-this.x,i=t.y-this.y;return Math.sqrt(e*e+i*i)},equals:function(t){return t=o.point(t),t.x===this.x&&t.y===this.y},contains:function(t){return t=o.point(t),Math.abs(t.x)<=Math.abs(this.x)&&Math.abs(t.y)<=Math.abs(this.y)},toString:function(){return"Point("+o.Util.formatNum(this.x)+", "+o.Util.formatNum(this.y)+")"}},o.point=function(t,e,n){return t instanceof o.Point?t:o.Util.isArray(t)?new o.Point(t[0],t[1]):t===i||null===t?t:new o.Point(t,e,n)},o.Bounds=function(t,e){if(t)for(var i=e?[t,e]:t,n=0,o=i.length;o>n;n++)this.extend(i[n])},o.Bounds.prototype={extend:function(t){return t=o.point(t),this.min||this.max?(this.min.x=Math.min(t.x,this.min.x),this.max.x=Math.max(t.x,this.max.x),this.min.y=Math.min(t.y,this.min.y),this.max.y=Math.max(t.y,this.max.y)):(this.min=t.clone(),this.max=t.clone()),this},getCenter:function(t){return new o.Point((this.min.x+this.max.x)/2,(this.min.y+this.max.y)/2,t)},getBottomLeft:function(){return new o.Point(this.min.x,this.max.y)},getTopRight:function(){return new o.Point(this.max.x,this.min.y)},getSize:function(){return this.max.subtract(this.min)},contains:function(t){var e,i;return t="number"==typeof t[0]||t instanceof o.Point?o.point(t):o.bounds(t),t instanceof o.Bounds?(e=t.min,i=t.max):e=i=t,e.x>=this.min.x&&i.x<=this.max.x&&e.y>=this.min.y&&i.y<=this.max.y},intersects:function(t){t=o.bounds(t);var e=this.min,i=this.max,n=t.min,s=t.max,a=s.x>=e.x&&n.x<=i.x,r=s.y>=e.y&&n.y<=i.y;return a&&r},isValid:function(){return!(!this.min||!this.max)}},o.bounds=function(t,e){return!t||t instanceof o.Bounds?t:new o.Bounds(t,e)},o.Transformation=function(t,e,i,n){this._a=t,this._b=e,this._c=i,this._d=n},o.Transformation.prototype={transform:function(t,e){return this._transform(t.clone(),e)},_transform:function(t,e){return e=e||1,t.x=e*(this._a*t.x+this._b),t.y=e*(this._c*t.y+this._d),t},untransform:function(t,e){return e=e||1,new o.Point((t.x/e-this._b)/this._a,(t.y/e-this._d)/this._c)}},o.DomUtil={get:function(t){return"string"==typeof t?e.getElementById(t):t},getStyle:function(t,i){var n=t.style[i];if(!n&&t.currentStyle&&(n=t.currentStyle[i]),(!n||"auto"===n)&&e.defaultView){var o=e.defaultView.getComputedStyle(t,null);n=o?o[i]:null}return"auto"===n?null:n},getViewportOffset:function(t){var i,n=0,s=0,a=t,r=e.body,h=e.documentElement;do{if(n+=a.offsetTop||0,s+=a.offsetLeft||0,n+=parseInt(o.DomUtil.getStyle(a,"borderTopWidth"),10)||0,s+=parseInt(o.DomUtil.getStyle(a,"borderLeftWidth"),10)||0,i=o.DomUtil.getStyle(a,"position"),a.offsetParent===r&&"absolute"===i)break;if("fixed"===i){n+=r.scrollTop||h.scrollTop||0,s+=r.scrollLeft||h.scrollLeft||0;break}if("relative"===i&&!a.offsetLeft){var l=o.DomUtil.getStyle(a,"width"),u=o.DomUtil.getStyle(a,"max-width"),c=a.getBoundingClientRect();("none"!==l||"none"!==u)&&(s+=c.left+a.clientLeft),n+=c.top+(r.scrollTop||h.scrollTop||0);break}a=a.offsetParent}while(a);a=t;do{if(a===r)break;n-=a.scrollTop||0,s-=a.scrollLeft||0,a=a.parentNode}while(a);return new o.Point(s,n)},documentIsLtr:function(){return o.DomUtil._docIsLtrCached||(o.DomUtil._docIsLtrCached=!0,o.DomUtil._docIsLtr="ltr"===o.DomUtil.getStyle(e.body,"direction")),o.DomUtil._docIsLtr},create:function(t,i,n){var o=e.createElement(t);return o.className=i,n&&n.appendChild(o),o},hasClass:function(t,e){if(t.classList!==i)return t.classList.contains(e);var n=o.DomUtil._getClass(t);return n.length>0&&new RegExp("(^|\\s)"+e+"(\\s|$)").test(n)},addClass:function(t,e){if(t.classList!==i)for(var n=o.Util.splitWords(e),s=0,a=n.length;a>s;s++)t.classList.add(n[s]);else if(!o.DomUtil.hasClass(t,e)){var r=o.DomUtil._getClass(t);o.DomUtil._setClass(t,(r?r+" ":"")+e)}},removeClass:function(t,e){t.classList!==i?t.classList.remove(e):o.DomUtil._setClass(t,o.Util.trim((" "+o.DomUtil._getClass(t)+" ").replace(" "+e+" "," ")))},_setClass:function(t,e){t.className.baseVal===i?t.className=e:t.className.baseVal=e},_getClass:function(t){return t.className.baseVal===i?t.className:t.className.baseVal},setOpacity:function(t,e){if("opacity"in t.style)t.style.opacity=e;else if("filter"in t.style){var i=!1,n="DXImageTransform.Microsoft.Alpha";try{i=t.filters.item(n)}catch(o){if(1===e)return}e=Math.round(100*e),i?(i.Enabled=100!==e,i.Opacity=e):t.style.filter+=" progid:"+n+"(opacity="+e+")"}},testProp:function(t){for(var i=e.documentElement.style,n=0;n<t.length;n++)if(t[n]in i)return t[n];return!1},getTranslateString:function(t){var e=o.Browser.webkit3d,i="translate"+(e?"3d":"")+"(",n=(e?",0":"")+")";return i+t.x+"px,"+t.y+"px"+n},getScaleString:function(t,e){var i=o.DomUtil.getTranslateString(e.add(e.multiplyBy(-1*t))),n=" scale("+t+") ";return i+n},setPosition:function(t,e,i){t._leaflet_pos=e,!i&&o.Browser.any3d?t.style[o.DomUtil.TRANSFORM]=o.DomUtil.getTranslateString(e):(t.style.left=e.x+"px",t.style.top=e.y+"px")},getPosition:function(t){return t._leaflet_pos}},o.DomUtil.TRANSFORM=o.DomUtil.testProp(["transform","WebkitTransform","OTransform","MozTransform","msTransform"]),o.DomUtil.TRANSITION=o.DomUtil.testProp(["webkitTransition","transition","OTransition","MozTransition","msTransition"]),o.DomUtil.TRANSITION_END="webkitTransition"===o.DomUtil.TRANSITION||"OTransition"===o.DomUtil.TRANSITION?o.DomUtil.TRANSITION+"End":"transitionend",function(){if("onselectstart"in e)o.extend(o.DomUtil,{disableTextSelection:function(){o.DomEvent.on(t,"selectstart",o.DomEvent.preventDefault)},enableTextSelection:function(){o.DomEvent.off(t,"selectstart",o.DomEvent.preventDefault)}});else{var i=o.DomUtil.testProp(["userSelect","WebkitUserSelect","OUserSelect","MozUserSelect","msUserSelect"]);o.extend(o.DomUtil,{disableTextSelection:function(){if(i){var t=e.documentElement.style;this._userSelect=t[i],t[i]="none"}},enableTextSelection:function(){i&&(e.documentElement.style[i]=this._userSelect,delete this._userSelect)}})}o.extend(o.DomUtil,{disableImageDrag:function(){o.DomEvent.on(t,"dragstart",o.DomEvent.preventDefault)},enableImageDrag:function(){o.DomEvent.off(t,"dragstart",o.DomEvent.preventDefault)}})}(),o.LatLng=function(t,e,n){if(t=parseFloat(t),e=parseFloat(e),isNaN(t)||isNaN(e))throw new Error("Invalid LatLng object: ("+t+", "+e+")");this.lat=t,this.lng=e,n!==i&&(this.alt=parseFloat(n))},o.extend(o.LatLng,{DEG_TO_RAD:Math.PI/180,RAD_TO_DEG:180/Math.PI,MAX_MARGIN:1e-9}),o.LatLng.prototype={equals:function(t){if(!t)return!1;t=o.latLng(t);var e=Math.max(Math.abs(this.lat-t.lat),Math.abs(this.lng-t.lng));return e<=o.LatLng.MAX_MARGIN},toString:function(t){return"LatLng("+o.Util.formatNum(this.lat,t)+", "+o.Util.formatNum(this.lng,t)+")"},distanceTo:function(t){t=o.latLng(t);var e=6378137,i=o.LatLng.DEG_TO_RAD,n=(t.lat-this.lat)*i,s=(t.lng-this.lng)*i,a=this.lat*i,r=t.lat*i,h=Math.sin(n/2),l=Math.sin(s/2),u=h*h+l*l*Math.cos(a)*Math.cos(r);return 2*e*Math.atan2(Math.sqrt(u),Math.sqrt(1-u))},wrap:function(t,e){var i=this.lng;return t=t||-180,e=e||180,i=(i+e)%(e-t)+(t>i||i===e?e:t),new o.LatLng(this.lat,i)}},o.latLng=function(t,e){return t instanceof o.LatLng?t:o.Util.isArray(t)?"number"==typeof t[0]||"string"==typeof t[0]?new o.LatLng(t[0],t[1],t[2]):null:t===i||null===t?t:"object"==typeof t&&"lat"in t?new o.LatLng(t.lat,"lng"in t?t.lng:t.lon):e===i?null:new o.LatLng(t,e)},o.LatLngBounds=function(t,e){if(t)for(var i=e?[t,e]:t,n=0,o=i.length;o>n;n++)this.extend(i[n])},o.LatLngBounds.prototype={extend:function(t){if(!t)return this;var e=o.latLng(t);return t=null!==e?e:o.latLngBounds(t),t instanceof o.LatLng?this._southWest||this._northEast?(this._southWest.lat=Math.min(t.lat,this._southWest.lat),this._southWest.lng=Math.min(t.lng,this._southWest.lng),this._northEast.lat=Math.max(t.lat,this._northEast.lat),this._northEast.lng=Math.max(t.lng,this._northEast.lng)):(this._southWest=new o.LatLng(t.lat,t.lng),this._northEast=new o.LatLng(t.lat,t.lng)):t instanceof o.LatLngBounds&&(this.extend(t._southWest),this.extend(t._northEast)),this},pad:function(t){var e=this._southWest,i=this._northEast,n=Math.abs(e.lat-i.lat)*t,s=Math.abs(e.lng-i.lng)*t;return new o.LatLngBounds(new o.LatLng(e.lat-n,e.lng-s),new o.LatLng(i.lat+n,i.lng+s))},getCenter:function(){return new o.LatLng((this._southWest.lat+this._northEast.lat)/2,(this._southWest.lng+this._northEast.lng)/2)},getSouthWest:function(){return this._southWest},getNorthEast:function(){return this._northEast},getNorthWest:function(){return new o.LatLng(this.getNorth(),this.getWest())},getSouthEast:function(){return new o.LatLng(this.getSouth(),this.getEast())},getWest:function(){return this._southWest.lng},getSouth:function(){return this._southWest.lat},getEast:function(){return this._northEast.lng},getNorth:function(){return this._northEast.lat},contains:function(t){t="number"==typeof t[0]||t instanceof o.LatLng?o.latLng(t):o.latLngBounds(t);var e,i,n=this._southWest,s=this._northEast;return t instanceof o.LatLngBounds?(e=t.getSouthWest(),i=t.getNorthEast()):e=i=t,e.lat>=n.lat&&i.lat<=s.lat&&e.lng>=n.lng&&i.lng<=s.lng},intersects:function(t){t=o.latLngBounds(t);var e=this._southWest,i=this._northEast,n=t.getSouthWest(),s=t.getNorthEast(),a=s.lat>=e.lat&&n.lat<=i.lat,r=s.lng>=e.lng&&n.lng<=i.lng;return a&&r},toBBoxString:function(){return[this.getWest(),this.getSouth(),this.getEast(),this.getNorth()].join(",")},equals:function(t){return t?(t=o.latLngBounds(t),this._southWest.equals(t.getSouthWest())&&this._northEast.equals(t.getNorthEast())):!1},isValid:function(){return!(!this._southWest||!this._northEast)}},o.latLngBounds=function(t,e){return!t||t instanceof o.LatLngBounds?t:new o.LatLngBounds(t,e)},o.Projection={},o.Projection.SphericalMercator={MAX_LATITUDE:85.0511287798,project:function(t){var e=o.LatLng.DEG_TO_RAD,i=this.MAX_LATITUDE,n=Math.max(Math.min(i,t.lat),-i),s=t.lng*e,a=n*e;return a=Math.log(Math.tan(Math.PI/4+a/2)),new o.Point(s,a)},unproject:function(t){var e=o.LatLng.RAD_TO_DEG,i=t.x*e,n=(2*Math.atan(Math.exp(t.y))-Math.PI/2)*e;return new o.LatLng(n,i)}},o.Projection.LonLat={project:function(t){return new o.Point(t.lng,t.lat)},unproject:function(t){return new o.LatLng(t.y,t.x)}},o.CRS={latLngToPoint:function(t,e){var i=this.projection.project(t),n=this.scale(e);return this.transformation._transform(i,n)},pointToLatLng:function(t,e){var i=this.scale(e),n=this.transformation.untransform(t,i);return this.projection.unproject(n)},project:function(t){return this.projection.project(t)},scale:function(t){return 256*Math.pow(2,t)},getSize:function(t){var e=this.scale(t);return o.point(e,e)}},o.CRS.Simple=o.extend({},o.CRS,{projection:o.Projection.LonLat,transformation:new o.Transformation(1,0,-1,0),scale:function(t){return Math.pow(2,t)}}),o.CRS.EPSG3857=o.extend({},o.CRS,{code:"EPSG:3857",projection:o.Projection.SphericalMercator,transformation:new o.Transformation(.5/Math.PI,.5,-.5/Math.PI,.5),project:function(t){var e=this.projection.project(t),i=6378137;return e.multiplyBy(i)}}),o.CRS.EPSG900913=o.extend({},o.CRS.EPSG3857,{code:"EPSG:900913"}),o.CRS.EPSG4326=o.extend({},o.CRS,{code:"EPSG:4326",projection:o.Projection.LonLat,transformation:new o.Transformation(1/360,.5,-1/360,.5)}),o.Map=o.Class.extend({includes:o.Mixin.Events,options:{crs:o.CRS.EPSG3857,fadeAnimation:o.DomUtil.TRANSITION&&!o.Browser.android23,trackResize:!0,markerZoomAnimation:o.DomUtil.TRANSITION&&o.Browser.any3d},initialize:function(t,e){e=o.setOptions(this,e),this._initContainer(t),this._initLayout(),this._onResize=o.bind(this._onResize,this),this._initEvents(),e.maxBounds&&this.setMaxBounds(e.maxBounds),e.center&&e.zoom!==i&&this.setView(o.latLng(e.center),e.zoom,{reset:!0}),this._handlers=[],this._layers={},this._zoomBoundLayers={},this._tileLayersNum=0,this.callInitHooks(),this._addLayers(e.layers)},setView:function(t,e){return e=e===i?this.getZoom():e,this._resetView(o.latLng(t),this._limitZoom(e)),this},setZoom:function(t,e){return this._loaded?this.setView(this.getCenter(),t,{zoom:e}):(this._zoom=this._limitZoom(t),this)},zoomIn:function(t,e){return this.setZoom(this._zoom+(t||1),e)},zoomOut:function(t,e){return this.setZoom(this._zoom-(t||1),e)},setZoomAround:function(t,e,i){var n=this.getZoomScale(e),s=this.getSize().divideBy(2),a=t instanceof o.Point?t:this.latLngToContainerPoint(t),r=a.subtract(s).multiplyBy(1-1/n),h=this.containerPointToLatLng(s.add(r));return this.setView(h,e,{zoom:i})},fitBounds:function(t,e){e=e||{},t=t.getBounds?t.getBounds():o.latLngBounds(t);var i=o.point(e.paddingTopLeft||e.padding||[0,0]),n=o.point(e.paddingBottomRight||e.padding||[0,0]),s=this.getBoundsZoom(t,!1,i.add(n)),a=n.subtract(i).divideBy(2),r=this.project(t.getSouthWest(),s),h=this.project(t.getNorthEast(),s),l=this.unproject(r.add(h).divideBy(2).add(a),s);return s=e&&e.maxZoom?Math.min(e.maxZoom,s):s,this.setView(l,s,e)},fitWorld:function(t){return this.fitBounds([[-90,-180],[90,180]],t)},panTo:function(t,e){return this.setView(t,this._zoom,{pan:e})},panBy:function(t){return this.fire("movestart"),this._rawPanBy(o.point(t)),this.fire("move"),this.fire("moveend")},setMaxBounds:function(t){return t=o.latLngBounds(t),this.options.maxBounds=t,t?(this._loaded&&this._panInsideMaxBounds(),this.on("moveend",this._panInsideMaxBounds,this)):this.off("moveend",this._panInsideMaxBounds,this)},panInsideBounds:function(t,e){var i=this.getCenter(),n=this._limitCenter(i,this._zoom,t);return i.equals(n)?this:this.panTo(n,e)},addLayer:function(t){var e=o.stamp(t);return this._layers[e]?this:(this._layers[e]=t,!t.options||isNaN(t.options.maxZoom)&&isNaN(t.options.minZoom)||(this._zoomBoundLayers[e]=t,this._updateZoomLevels()),this.options.zoomAnimation&&o.TileLayer&&t instanceof o.TileLayer&&(this._tileLayersNum++,this._tileLayersToLoad++,t.on("load",this._onTileLayerLoad,this)),this._loaded&&this._layerAdd(t),this)},removeLayer:function(t){var e=o.stamp(t);return this._layers[e]?(this._loaded&&t.onRemove(this),delete this._layers[e],this._loaded&&this.fire("layerremove",{layer:t}),this._zoomBoundLayers[e]&&(delete this._zoomBoundLayers[e],this._updateZoomLevels()),this.options.zoomAnimation&&o.TileLayer&&t instanceof o.TileLayer&&(this._tileLayersNum--,this._tileLayersToLoad--,t.off("load",this._onTileLayerLoad,this)),this):this},hasLayer:function(t){return t?o.stamp(t)in this._layers:!1},eachLayer:function(t,e){for(var i in this._layers)t.call(e,this._layers[i]);return this},invalidateSize:function(t){if(!this._loaded)return this;t=o.extend({animate:!1,pan:!0},t===!0?{animate:!0}:t);var e=this.getSize();this._sizeChanged=!0,this._initialCenter=null;var i=this.getSize(),n=e.divideBy(2).round(),s=i.divideBy(2).round(),a=n.subtract(s);return a.x||a.y?(t.animate&&t.pan?this.panBy(a):(t.pan&&this._rawPanBy(a),this.fire("move"),t.debounceMoveend?(clearTimeout(this._sizeTimer),this._sizeTimer=setTimeout(o.bind(this.fire,this,"moveend"),200)):this.fire("moveend")),this.fire("resize",{oldSize:e,newSize:i})):this},addHandler:function(t,e){if(!e)return this;var i=this[t]=new e(this);return this._handlers.push(i),this.options[t]&&i.enable(),this},remove:function(){this._loaded&&this.fire("unload"),this._initEvents("off");try{delete this._container._leaflet}catch(t){this._container._leaflet=i}return this._clearPanes(),this._clearControlPos&&this._clearControlPos(),this._clearHandlers(),this},getCenter:function(){return this._checkIfLoaded(),this._initialCenter&&!this._moved()?this._initialCenter:this.layerPointToLatLng(this._getCenterLayerPoint())},getZoom:function(){return this._zoom},getBounds:function(){var t=this.getPixelBounds(),e=this.unproject(t.getBottomLeft()),i=this.unproject(t.getTopRight());return new o.LatLngBounds(e,i)},getMinZoom:function(){return this.options.minZoom===i?this._layersMinZoom===i?0:this._layersMinZoom:this.options.minZoom},getMaxZoom:function(){return this.options.maxZoom===i?this._layersMaxZoom===i?1/0:this._layersMaxZoom:this.options.maxZoom},getBoundsZoom:function(t,e,i){t=o.latLngBounds(t);var n,s=this.getMinZoom()-(e?1:0),a=this.getMaxZoom(),r=this.getSize(),h=t.getNorthWest(),l=t.getSouthEast(),u=!0;i=o.point(i||[0,0]);do s++,n=this.project(l,s).subtract(this.project(h,s)).add(i),u=e?n.x<r.x||n.y<r.y:r.contains(n);while(u&&a>=s);return u&&e?null:e?s:s-1},getSize:function(){return(!this._size||this._sizeChanged)&&(this._size=new o.Point(this._container.clientWidth,this._container.clientHeight),this._sizeChanged=!1),this._size.clone()},getPixelBounds:function(){var t=this._getTopLeftPoint();return new o.Bounds(t,t.add(this.getSize()))},getPixelOrigin:function(){return this._checkIfLoaded(),this._initialTopLeftPoint},getPanes:function(){return this._panes},getContainer:function(){return this._container},getZoomScale:function(t){var e=this.options.crs;return e.scale(t)/e.scale(this._zoom)},getScaleZoom:function(t){return this._zoom+Math.log(t)/Math.LN2},project:function(t,e){return e=e===i?this._zoom:e,this.options.crs.latLngToPoint(o.latLng(t),e)},unproject:function(t,e){return e=e===i?this._zoom:e,this.options.crs.pointToLatLng(o.point(t),e)},layerPointToLatLng:function(t){var e=o.point(t).add(this.getPixelOrigin());return this.unproject(e)},latLngToLayerPoint:function(t){var e=this.project(o.latLng(t))._round();return e._subtract(this.getPixelOrigin())},containerPointToLayerPoint:function(t){return o.point(t).subtract(this._getMapPanePos())},layerPointToContainerPoint:function(t){return o.point(t).add(this._getMapPanePos())},containerPointToLatLng:function(t){var e=this.containerPointToLayerPoint(o.point(t));return this.layerPointToLatLng(e)},latLngToContainerPoint:function(t){return this.layerPointToContainerPoint(this.latLngToLayerPoint(o.latLng(t)))},mouseEventToContainerPoint:function(t){return o.DomEvent.getMousePosition(t,this._container)},mouseEventToLayerPoint:function(t){return this.containerPointToLayerPoint(this.mouseEventToContainerPoint(t))},mouseEventToLatLng:function(t){return this.layerPointToLatLng(this.mouseEventToLayerPoint(t))},_initContainer:function(t){var e=this._container=o.DomUtil.get(t);if(!e)throw new Error("Map container not found.");if(e._leaflet)throw new Error("Map container is already initialized.");e._leaflet=!0},_initLayout:function(){var t=this._container;o.DomUtil.addClass(t,"leaflet-container"+(o.Browser.touch?" leaflet-touch":"")+(o.Browser.retina?" leaflet-retina":"")+(o.Browser.ielt9?" leaflet-oldie":"")+(this.options.fadeAnimation?" leaflet-fade-anim":""));var e=o.DomUtil.getStyle(t,"position");"absolute"!==e&&"relative"!==e&&"fixed"!==e&&(t.style.position="relative"),this._initPanes(),this._initControlPos&&this._initControlPos()},_initPanes:function(){var t=this._panes={};this._mapPane=t.mapPane=this._createPane("leaflet-map-pane",this._container),this._tilePane=t.tilePane=this._createPane("leaflet-tile-pane",this._mapPane),t.objectsPane=this._createPane("leaflet-objects-pane",this._mapPane),t.shadowPane=this._createPane("leaflet-shadow-pane"),t.overlayPane=this._createPane("leaflet-overlay-pane"),t.markerPane=this._createPane("leaflet-marker-pane"),t.popupPane=this._createPane("leaflet-popup-pane");var e=" leaflet-zoom-hide";this.options.markerZoomAnimation||(o.DomUtil.addClass(t.markerPane,e),o.DomUtil.addClass(t.shadowPane,e),o.DomUtil.addClass(t.popupPane,e))},_createPane:function(t,e){return o.DomUtil.create("div",t,e||this._panes.objectsPane)},_clearPanes:function(){this._container.removeChild(this._mapPane)},_addLayers:function(t){t=t?o.Util.isArray(t)?t:[t]:[];for(var e=0,i=t.length;i>e;e++)this.addLayer(t[e])},_resetView:function(t,e,i,n){var s=this._zoom!==e;n||(this.fire("movestart"),s&&this.fire("zoomstart")),this._zoom=e,this._initialCenter=t,this._initialTopLeftPoint=this._getNewTopLeftPoint(t),i?this._initialTopLeftPoint._add(this._getMapPanePos()):o.DomUtil.setPosition(this._mapPane,new o.Point(0,0)),this._tileLayersToLoad=this._tileLayersNum;var a=!this._loaded;this._loaded=!0,this.fire("viewreset",{hard:!i}),a&&(this.fire("load"),this.eachLayer(this._layerAdd,this)),this.fire("move"),(s||n)&&this.fire("zoomend"),this.fire("moveend",{hard:!i})},_rawPanBy:function(t){o.DomUtil.setPosition(this._mapPane,this._getMapPanePos().subtract(t))},_getZoomSpan:function(){return this.getMaxZoom()-this.getMinZoom()},_updateZoomLevels:function(){var t,e=1/0,n=-1/0,o=this._getZoomSpan();for(t in this._zoomBoundLayers){var s=this._zoomBoundLayers[t];isNaN(s.options.minZoom)||(e=Math.min(e,s.options.minZoom)),isNaN(s.options.maxZoom)||(n=Math.max(n,s.options.maxZoom))}t===i?this._layersMaxZoom=this._layersMinZoom=i:(this._layersMaxZoom=n,this._layersMinZoom=e),o!==this._getZoomSpan()&&this.fire("zoomlevelschange")},_panInsideMaxBounds:function(){this.panInsideBounds(this.options.maxBounds)},_checkIfLoaded:function(){if(!this._loaded)throw new Error("Set map center and zoom first.")},_initEvents:function(e){if(o.DomEvent){e=e||"on",o.DomEvent[e](this._container,"click",this._onMouseClick,this);var i,n,s=["dblclick","mousedown","mouseup","mouseenter","mouseleave","mousemove","contextmenu"];for(i=0,n=s.length;n>i;i++)o.DomEvent[e](this._container,s[i],this._fireMouseEvent,this);this.options.trackResize&&o.DomEvent[e](t,"resize",this._onResize,this)}},_onResize:function(){o.Util.cancelAnimFrame(this._resizeRequest),this._resizeRequest=o.Util.requestAnimFrame(function(){this.invalidateSize({debounceMoveend:!0})},this,!1,this._container)},_onMouseClick:function(t){!this._loaded||!t._simulated&&(this.dragging&&this.dragging.moved()||this.boxZoom&&this.boxZoom.moved())||o.DomEvent._skipped(t)||(this.fire("preclick"),this._fireMouseEvent(t))},_fireMouseEvent:function(t){if(this._loaded&&!o.DomEvent._skipped(t)){var e=t.type;if(e="mouseenter"===e?"mouseover":"mouseleave"===e?"mouseout":e,this.hasEventListeners(e)){"contextmenu"===e&&o.DomEvent.preventDefault(t);var i=this.mouseEventToContainerPoint(t),n=this.containerPointToLayerPoint(i),s=this.layerPointToLatLng(n);this.fire(e,{latlng:s,layerPoint:n,containerPoint:i,originalEvent:t})}}},_onTileLayerLoad:function(){this._tileLayersToLoad--,this._tileLayersNum&&!this._tileLayersToLoad&&this.fire("tilelayersload")},_clearHandlers:function(){for(var t=0,e=this._handlers.length;e>t;t++)this._handlers[t].disable()},whenReady:function(t,e){return this._loaded?t.call(e||this,this):this.on("load",t,e),this},_layerAdd:function(t){t.onAdd(this),this.fire("layeradd",{layer:t})},_getMapPanePos:function(){return o.DomUtil.getPosition(this._mapPane)},_moved:function(){var t=this._getMapPanePos();return t&&!t.equals([0,0])},_getTopLeftPoint:function(){return this.getPixelOrigin().subtract(this._getMapPanePos())},_getNewTopLeftPoint:function(t,e){var i=this.getSize()._divideBy(2);return this.project(t,e)._subtract(i)._round()},_latLngToNewLayerPoint:function(t,e,i){var n=this._getNewTopLeftPoint(i,e).add(this._getMapPanePos());return this.project(t,e)._subtract(n)},_getCenterLayerPoint:function(){return this.containerPointToLayerPoint(this.getSize()._divideBy(2))},_getCenterOffset:function(t){return this.latLngToLayerPoint(t).subtract(this._getCenterLayerPoint())},_limitCenter:function(t,e,i){if(!i)return t;var n=this.project(t,e),s=this.getSize().divideBy(2),a=new o.Bounds(n.subtract(s),n.add(s)),r=this._getBoundsOffset(a,i,e);return this.unproject(n.add(r),e)},_limitOffset:function(t,e){if(!e)return t;var i=this.getPixelBounds(),n=new o.Bounds(i.min.add(t),i.max.add(t));return t.add(this._getBoundsOffset(n,e))},_getBoundsOffset:function(t,e,i){var n=this.project(e.getNorthWest(),i).subtract(t.min),s=this.project(e.getSouthEast(),i).subtract(t.max),a=this._rebound(n.x,-s.x),r=this._rebound(n.y,-s.y);return new o.Point(a,r)},_rebound:function(t,e){return t+e>0?Math.round(t-e)/2:Math.max(0,Math.ceil(t))-Math.max(0,Math.floor(e))},_limitZoom:function(t){var e=this.getMinZoom(),i=this.getMaxZoom();return Math.max(e,Math.min(i,t))}}),o.map=function(t,e){return new o.Map(t,e)},o.Projection.Mercator={MAX_LATITUDE:85.0840591556,R_MINOR:6356752.314245179,R_MAJOR:6378137,project:function(t){var e=o.LatLng.DEG_TO_RAD,i=this.MAX_LATITUDE,n=Math.max(Math.min(i,t.lat),-i),s=this.R_MAJOR,a=this.R_MINOR,r=t.lng*e*s,h=n*e,l=a/s,u=Math.sqrt(1-l*l),c=u*Math.sin(h);c=Math.pow((1-c)/(1+c),.5*u);var d=Math.tan(.5*(.5*Math.PI-h))/c;return h=-s*Math.log(d),new o.Point(r,h)},unproject:function(t){for(var e,i=o.LatLng.RAD_TO_DEG,n=this.R_MAJOR,s=this.R_MINOR,a=t.x*i/n,r=s/n,h=Math.sqrt(1-r*r),l=Math.exp(-t.y/n),u=Math.PI/2-2*Math.atan(l),c=15,d=1e-7,p=c,_=.1;Math.abs(_)>d&&--p>0;)e=h*Math.sin(u),_=Math.PI/2-2*Math.atan(l*Math.pow((1-e)/(1+e),.5*h))-u,u+=_;
return new o.LatLng(u*i,a)}},o.CRS.EPSG3395=o.extend({},o.CRS,{code:"EPSG:3395",projection:o.Projection.Mercator,transformation:function(){var t=o.Projection.Mercator,e=t.R_MAJOR,i=.5/(Math.PI*e);return new o.Transformation(i,.5,-i,.5)}()}),o.TileLayer=o.Class.extend({includes:o.Mixin.Events,options:{minZoom:0,maxZoom:18,tileSize:256,subdomains:"abc",errorTileUrl:"",attribution:"",zoomOffset:0,opacity:1,unloadInvisibleTiles:o.Browser.mobile,updateWhenIdle:o.Browser.mobile},initialize:function(t,e){e=o.setOptions(this,e),e.detectRetina&&o.Browser.retina&&e.maxZoom>0&&(e.tileSize=Math.floor(e.tileSize/2),e.zoomOffset++,e.minZoom>0&&e.minZoom--,this.options.maxZoom--),e.bounds&&(e.bounds=o.latLngBounds(e.bounds)),this._url=t;var i=this.options.subdomains;"string"==typeof i&&(this.options.subdomains=i.split(""))},onAdd:function(t){this._map=t,this._animated=t._zoomAnimated,this._initContainer(),t.on({viewreset:this._reset,moveend:this._update},this),this._animated&&t.on({zoomanim:this._animateZoom,zoomend:this._endZoomAnim},this),this.options.updateWhenIdle||(this._limitedUpdate=o.Util.limitExecByInterval(this._update,150,this),t.on("move",this._limitedUpdate,this)),this._reset(),this._update()},addTo:function(t){return t.addLayer(this),this},onRemove:function(t){this._container.parentNode.removeChild(this._container),t.off({viewreset:this._reset,moveend:this._update},this),this._animated&&t.off({zoomanim:this._animateZoom,zoomend:this._endZoomAnim},this),this.options.updateWhenIdle||t.off("move",this._limitedUpdate,this),this._container=null,this._map=null},bringToFront:function(){var t=this._map._panes.tilePane;return this._container&&(t.appendChild(this._container),this._setAutoZIndex(t,Math.max)),this},bringToBack:function(){var t=this._map._panes.tilePane;return this._container&&(t.insertBefore(this._container,t.firstChild),this._setAutoZIndex(t,Math.min)),this},getAttribution:function(){return this.options.attribution},getContainer:function(){return this._container},setOpacity:function(t){return this.options.opacity=t,this._map&&this._updateOpacity(),this},setZIndex:function(t){return this.options.zIndex=t,this._updateZIndex(),this},setUrl:function(t,e){return this._url=t,e||this.redraw(),this},redraw:function(){return this._map&&(this._reset({hard:!0}),this._update()),this},_updateZIndex:function(){this._container&&this.options.zIndex!==i&&(this._container.style.zIndex=this.options.zIndex)},_setAutoZIndex:function(t,e){var i,n,o,s=t.children,a=-e(1/0,-1/0);for(n=0,o=s.length;o>n;n++)s[n]!==this._container&&(i=parseInt(s[n].style.zIndex,10),isNaN(i)||(a=e(a,i)));this.options.zIndex=this._container.style.zIndex=(isFinite(a)?a:0)+e(1,-1)},_updateOpacity:function(){var t,e=this._tiles;if(o.Browser.ielt9)for(t in e)o.DomUtil.setOpacity(e[t],this.options.opacity);else o.DomUtil.setOpacity(this._container,this.options.opacity)},_initContainer:function(){var t=this._map._panes.tilePane;if(!this._container){if(this._container=o.DomUtil.create("div","leaflet-layer"),this._updateZIndex(),this._animated){var e="leaflet-tile-container";this._bgBuffer=o.DomUtil.create("div",e,this._container),this._tileContainer=o.DomUtil.create("div",e,this._container)}else this._tileContainer=this._container;t.appendChild(this._container),this.options.opacity<1&&this._updateOpacity()}},_reset:function(t){for(var e in this._tiles)this.fire("tileunload",{tile:this._tiles[e]});this._tiles={},this._tilesToLoad=0,this.options.reuseTiles&&(this._unusedTiles=[]),this._tileContainer.innerHTML="",this._animated&&t&&t.hard&&this._clearBgBuffer(),this._initContainer()},_getTileSize:function(){var t=this._map,e=t.getZoom()+this.options.zoomOffset,i=this.options.maxNativeZoom,n=this.options.tileSize;return i&&e>i&&(n=Math.round(t.getZoomScale(e)/t.getZoomScale(i)*n)),n},_update:function(){if(this._map){var t=this._map,e=t.getPixelBounds(),i=t.getZoom(),n=this._getTileSize();if(!(i>this.options.maxZoom||i<this.options.minZoom)){var s=o.bounds(e.min.divideBy(n)._floor(),e.max.divideBy(n)._floor());this._addTilesFromCenterOut(s),(this.options.unloadInvisibleTiles||this.options.reuseTiles)&&this._removeOtherTiles(s)}}},_addTilesFromCenterOut:function(t){var i,n,s,a=[],r=t.getCenter();for(i=t.min.y;i<=t.max.y;i++)for(n=t.min.x;n<=t.max.x;n++)s=new o.Point(n,i),this._tileShouldBeLoaded(s)&&a.push(s);var h=a.length;if(0!==h){a.sort(function(t,e){return t.distanceTo(r)-e.distanceTo(r)});var l=e.createDocumentFragment();for(this._tilesToLoad||this.fire("loading"),this._tilesToLoad+=h,n=0;h>n;n++)this._addTile(a[n],l);this._tileContainer.appendChild(l)}},_tileShouldBeLoaded:function(t){if(t.x+":"+t.y in this._tiles)return!1;var e=this.options;if(!e.continuousWorld){var i=this._getWrapTileNum();if(e.noWrap&&(t.x<0||t.x>=i.x)||t.y<0||t.y>=i.y)return!1}if(e.bounds){var n=e.tileSize,o=t.multiplyBy(n),s=o.add([n,n]),a=this._map.unproject(o),r=this._map.unproject(s);if(e.continuousWorld||e.noWrap||(a=a.wrap(),r=r.wrap()),!e.bounds.intersects([a,r]))return!1}return!0},_removeOtherTiles:function(t){var e,i,n,o;for(o in this._tiles)e=o.split(":"),i=parseInt(e[0],10),n=parseInt(e[1],10),(i<t.min.x||i>t.max.x||n<t.min.y||n>t.max.y)&&this._removeTile(o)},_removeTile:function(t){var e=this._tiles[t];this.fire("tileunload",{tile:e,url:e.src}),this.options.reuseTiles?(o.DomUtil.removeClass(e,"leaflet-tile-loaded"),this._unusedTiles.push(e)):e.parentNode===this._tileContainer&&this._tileContainer.removeChild(e),o.Browser.android||(e.onload=null,e.src=o.Util.emptyImageUrl),delete this._tiles[t]},_addTile:function(t,e){var i=this._getTilePos(t),n=this._getTile();o.DomUtil.setPosition(n,i,o.Browser.chrome),this._tiles[t.x+":"+t.y]=n,this._loadTile(n,t),n.parentNode!==this._tileContainer&&e.appendChild(n)},_getZoomForUrl:function(){var t=this.options,e=this._map.getZoom();return t.zoomReverse&&(e=t.maxZoom-e),e+=t.zoomOffset,t.maxNativeZoom?Math.min(e,t.maxNativeZoom):e},_getTilePos:function(t){var e=this._map.getPixelOrigin(),i=this._getTileSize();return t.multiplyBy(i).subtract(e)},getTileUrl:function(t){return o.Util.template(this._url,o.extend({s:this._getSubdomain(t),z:t.z,x:t.x,y:t.y},this.options))},_getWrapTileNum:function(){var t=this._map.options.crs,e=t.getSize(this._map.getZoom());return e.divideBy(this._getTileSize())._floor()},_adjustTilePoint:function(t){var e=this._getWrapTileNum();this.options.continuousWorld||this.options.noWrap||(t.x=(t.x%e.x+e.x)%e.x),this.options.tms&&(t.y=e.y-t.y-1),t.z=this._getZoomForUrl()},_getSubdomain:function(t){var e=Math.abs(t.x+t.y)%this.options.subdomains.length;return this.options.subdomains[e]},_getTile:function(){if(this.options.reuseTiles&&this._unusedTiles.length>0){var t=this._unusedTiles.pop();return this._resetTile(t),t}return this._createTile()},_resetTile:function(){},_createTile:function(){var t=o.DomUtil.create("img","leaflet-tile");return t.style.width=t.style.height=this._getTileSize()+"px",t.galleryimg="no",t.onselectstart=t.onmousemove=o.Util.falseFn,o.Browser.ielt9&&this.options.opacity!==i&&o.DomUtil.setOpacity(t,this.options.opacity),o.Browser.mobileWebkit3d&&(t.style.WebkitBackfaceVisibility="hidden"),t},_loadTile:function(t,e){t._layer=this,t.onload=this._tileOnLoad,t.onerror=this._tileOnError,this._adjustTilePoint(e),t.src=this.getTileUrl(e),this.fire("tileloadstart",{tile:t,url:t.src})},_tileLoaded:function(){this._tilesToLoad--,this._animated&&o.DomUtil.addClass(this._tileContainer,"leaflet-zoom-animated"),this._tilesToLoad||(this.fire("load"),this._animated&&(clearTimeout(this._clearBgBufferTimer),this._clearBgBufferTimer=setTimeout(o.bind(this._clearBgBuffer,this),500)))},_tileOnLoad:function(){var t=this._layer;this.src!==o.Util.emptyImageUrl&&(o.DomUtil.addClass(this,"leaflet-tile-loaded"),t.fire("tileload",{tile:this,url:this.src})),t._tileLoaded()},_tileOnError:function(){var t=this._layer;t.fire("tileerror",{tile:this,url:this.src});var e=t.options.errorTileUrl;e&&(this.src=e),t._tileLoaded()}}),o.tileLayer=function(t,e){return new o.TileLayer(t,e)},o.TileLayer.WMS=o.TileLayer.extend({defaultWmsParams:{service:"WMS",request:"GetMap",version:"1.1.1",layers:"",styles:"",format:"image/jpeg",transparent:!1},initialize:function(t,e){this._url=t;var i=o.extend({},this.defaultWmsParams),n=e.tileSize||this.options.tileSize;i.width=i.height=e.detectRetina&&o.Browser.retina?2*n:n;for(var s in e)this.options.hasOwnProperty(s)||"crs"===s||(i[s]=e[s]);this.wmsParams=i,o.setOptions(this,e)},onAdd:function(t){this._crs=this.options.crs||t.options.crs,this._wmsVersion=parseFloat(this.wmsParams.version);var e=this._wmsVersion>=1.3?"crs":"srs";this.wmsParams[e]=this._crs.code,o.TileLayer.prototype.onAdd.call(this,t)},getTileUrl:function(t){var e=this._map,i=this.options.tileSize,n=t.multiplyBy(i),s=n.add([i,i]),a=this._crs.project(e.unproject(n,t.z)),r=this._crs.project(e.unproject(s,t.z)),h=this._wmsVersion>=1.3&&this._crs===o.CRS.EPSG4326?[r.y,a.x,a.y,r.x].join(","):[a.x,r.y,r.x,a.y].join(","),l=o.Util.template(this._url,{s:this._getSubdomain(t)});return l+o.Util.getParamString(this.wmsParams,l,!0)+"&BBOX="+h},setParams:function(t,e){return o.extend(this.wmsParams,t),e||this.redraw(),this}}),o.tileLayer.wms=function(t,e){return new o.TileLayer.WMS(t,e)},o.TileLayer.Canvas=o.TileLayer.extend({options:{async:!1},initialize:function(t){o.setOptions(this,t)},redraw:function(){this._map&&(this._reset({hard:!0}),this._update());for(var t in this._tiles)this._redrawTile(this._tiles[t]);return this},_redrawTile:function(t){this.drawTile(t,t._tilePoint,this._map._zoom)},_createTile:function(){var t=o.DomUtil.create("canvas","leaflet-tile");return t.width=t.height=this.options.tileSize,t.onselectstart=t.onmousemove=o.Util.falseFn,t},_loadTile:function(t,e){t._layer=this,t._tilePoint=e,this._redrawTile(t),this.options.async||this.tileDrawn(t)},drawTile:function(){},tileDrawn:function(t){this._tileOnLoad.call(t)}}),o.tileLayer.canvas=function(t){return new o.TileLayer.Canvas(t)},o.ImageOverlay=o.Class.extend({includes:o.Mixin.Events,options:{opacity:1},initialize:function(t,e,i){this._url=t,this._bounds=o.latLngBounds(e),o.setOptions(this,i)},onAdd:function(t){this._map=t,this._image||this._initImage(),t._panes.overlayPane.appendChild(this._image),t.on("viewreset",this._reset,this),t.options.zoomAnimation&&o.Browser.any3d&&t.on("zoomanim",this._animateZoom,this),this._reset()},onRemove:function(t){t.getPanes().overlayPane.removeChild(this._image),t.off("viewreset",this._reset,this),t.options.zoomAnimation&&t.off("zoomanim",this._animateZoom,this)},addTo:function(t){return t.addLayer(this),this},setOpacity:function(t){return this.options.opacity=t,this._updateOpacity(),this},bringToFront:function(){return this._image&&this._map._panes.overlayPane.appendChild(this._image),this},bringToBack:function(){var t=this._map._panes.overlayPane;return this._image&&t.insertBefore(this._image,t.firstChild),this},setUrl:function(t){this._url=t,this._image.src=this._url},getAttribution:function(){return this.options.attribution},_initImage:function(){this._image=o.DomUtil.create("img","leaflet-image-layer"),this._map.options.zoomAnimation&&o.Browser.any3d?o.DomUtil.addClass(this._image,"leaflet-zoom-animated"):o.DomUtil.addClass(this._image,"leaflet-zoom-hide"),this._updateOpacity(),o.extend(this._image,{galleryimg:"no",onselectstart:o.Util.falseFn,onmousemove:o.Util.falseFn,onload:o.bind(this._onImageLoad,this),src:this._url})},_animateZoom:function(t){var e=this._map,i=this._image,n=e.getZoomScale(t.zoom),s=this._bounds.getNorthWest(),a=this._bounds.getSouthEast(),r=e._latLngToNewLayerPoint(s,t.zoom,t.center),h=e._latLngToNewLayerPoint(a,t.zoom,t.center)._subtract(r),l=r._add(h._multiplyBy(.5*(1-1/n)));i.style[o.DomUtil.TRANSFORM]=o.DomUtil.getTranslateString(l)+" scale("+n+") "},_reset:function(){var t=this._image,e=this._map.latLngToLayerPoint(this._bounds.getNorthWest()),i=this._map.latLngToLayerPoint(this._bounds.getSouthEast())._subtract(e);o.DomUtil.setPosition(t,e),t.style.width=i.x+"px",t.style.height=i.y+"px"},_onImageLoad:function(){this.fire("load")},_updateOpacity:function(){o.DomUtil.setOpacity(this._image,this.options.opacity)}}),o.imageOverlay=function(t,e,i){return new o.ImageOverlay(t,e,i)},o.Icon=o.Class.extend({options:{className:""},initialize:function(t){o.setOptions(this,t)},createIcon:function(t){return this._createIcon("icon",t)},createShadow:function(t){return this._createIcon("shadow",t)},_createIcon:function(t,e){var i=this._getIconUrl(t);if(!i){if("icon"===t)throw new Error("iconUrl not set in Icon options (see the docs).");return null}var n;return n=e&&"IMG"===e.tagName?this._createImg(i,e):this._createImg(i),this._setIconStyles(n,t),n},_setIconStyles:function(t,e){var i,n=this.options,s=o.point(n[e+"Size"]);i=o.point("shadow"===e?n.shadowAnchor||n.iconAnchor:n.iconAnchor),!i&&s&&(i=s.divideBy(2,!0)),t.className="leaflet-marker-"+e+" "+n.className,i&&(t.style.marginLeft=-i.x+"px",t.style.marginTop=-i.y+"px"),s&&(t.style.width=s.x+"px",t.style.height=s.y+"px")},_createImg:function(t,i){return i=i||e.createElement("img"),i.src=t,i},_getIconUrl:function(t){return o.Browser.retina&&this.options[t+"RetinaUrl"]?this.options[t+"RetinaUrl"]:this.options[t+"Url"]}}),o.icon=function(t){return new o.Icon(t)},o.Icon.Default=o.Icon.extend({options:{iconSize:[25,41],iconAnchor:[12,41],popupAnchor:[1,-34],shadowSize:[41,41]},_getIconUrl:function(t){var e=t+"Url";if(this.options[e])return this.options[e];o.Browser.retina&&"icon"===t&&(t+="-2x");var i=o.Icon.Default.imagePath;if(!i)throw new Error("Couldn't autodetect L.Icon.Default.imagePath, set it manually.");return i+"/marker-"+t+".png"}}),o.Icon.Default.imagePath=function(){var t,i,n,o,s,a=e.getElementsByTagName("script"),r=/[\/^]leaflet[\-\._]?([\w\-\._]*)\.js\??/;for(t=0,i=a.length;i>t;t++)if(n=a[t].src,o=n.match(r))return s=n.split(r)[0],(s?s+"/":"")+"images"}(),o.Marker=o.Class.extend({includes:o.Mixin.Events,options:{icon:new o.Icon.Default,title:"",alt:"",clickable:!0,draggable:!1,keyboard:!0,zIndexOffset:0,opacity:1,riseOnHover:!1,riseOffset:250},initialize:function(t,e){o.setOptions(this,e),this._latlng=o.latLng(t)},onAdd:function(t){this._map=t,t.on("viewreset",this.update,this),this._initIcon(),this.update(),this.fire("add"),t.options.zoomAnimation&&t.options.markerZoomAnimation&&t.on("zoomanim",this._animateZoom,this)},addTo:function(t){return t.addLayer(this),this},onRemove:function(t){this.dragging&&this.dragging.disable(),this._removeIcon(),this._removeShadow(),this.fire("remove"),t.off({viewreset:this.update,zoomanim:this._animateZoom},this),this._map=null},getLatLng:function(){return this._latlng},setLatLng:function(t){return this._latlng=o.latLng(t),this.update(),this.fire("move",{latlng:this._latlng})},setZIndexOffset:function(t){return this.options.zIndexOffset=t,this.update(),this},setIcon:function(t){return this.options.icon=t,this._map&&(this._initIcon(),this.update()),this._popup&&this.bindPopup(this._popup),this},update:function(){if(this._icon){var t=this._map.latLngToLayerPoint(this._latlng).round();this._setPos(t)}return this},_initIcon:function(){var t=this.options,e=this._map,i=e.options.zoomAnimation&&e.options.markerZoomAnimation,n=i?"leaflet-zoom-animated":"leaflet-zoom-hide",s=t.icon.createIcon(this._icon),a=!1;s!==this._icon&&(this._icon&&this._removeIcon(),a=!0,t.title&&(s.title=t.title),t.alt&&(s.alt=t.alt)),o.DomUtil.addClass(s,n),t.keyboard&&(s.tabIndex="0"),this._icon=s,this._initInteraction(),t.riseOnHover&&o.DomEvent.on(s,"mouseover",this._bringToFront,this).on(s,"mouseout",this._resetZIndex,this);var r=t.icon.createShadow(this._shadow),h=!1;r!==this._shadow&&(this._removeShadow(),h=!0),r&&o.DomUtil.addClass(r,n),this._shadow=r,t.opacity<1&&this._updateOpacity();var l=this._map._panes;a&&l.markerPane.appendChild(this._icon),r&&h&&l.shadowPane.appendChild(this._shadow)},_removeIcon:function(){this.options.riseOnHover&&o.DomEvent.off(this._icon,"mouseover",this._bringToFront).off(this._icon,"mouseout",this._resetZIndex),this._map._panes.markerPane.removeChild(this._icon),this._icon=null},_removeShadow:function(){this._shadow&&this._map._panes.shadowPane.removeChild(this._shadow),this._shadow=null},_setPos:function(t){o.DomUtil.setPosition(this._icon,t),this._shadow&&o.DomUtil.setPosition(this._shadow,t),this._zIndex=t.y+this.options.zIndexOffset,this._resetZIndex()},_updateZIndex:function(t){this._icon.style.zIndex=this._zIndex+t},_animateZoom:function(t){var e=this._map._latLngToNewLayerPoint(this._latlng,t.zoom,t.center).round();this._setPos(e)},_initInteraction:function(){if(this.options.clickable){var t=this._icon,e=["dblclick","mousedown","mouseover","mouseout","contextmenu"];o.DomUtil.addClass(t,"leaflet-clickable"),o.DomEvent.on(t,"click",this._onMouseClick,this),o.DomEvent.on(t,"keypress",this._onKeyPress,this);for(var i=0;i<e.length;i++)o.DomEvent.on(t,e[i],this._fireMouseEvent,this);o.Handler.MarkerDrag&&(this.dragging=new o.Handler.MarkerDrag(this),this.options.draggable&&this.dragging.enable())}},_onMouseClick:function(t){var e=this.dragging&&this.dragging.moved();(this.hasEventListeners(t.type)||e)&&o.DomEvent.stopPropagation(t),e||(this.dragging&&this.dragging._enabled||!this._map.dragging||!this._map.dragging.moved())&&this.fire(t.type,{originalEvent:t,latlng:this._latlng})},_onKeyPress:function(t){13===t.keyCode&&this.fire("click",{originalEvent:t,latlng:this._latlng})},_fireMouseEvent:function(t){this.fire(t.type,{originalEvent:t,latlng:this._latlng}),"contextmenu"===t.type&&this.hasEventListeners(t.type)&&o.DomEvent.preventDefault(t),"mousedown"!==t.type?o.DomEvent.stopPropagation(t):o.DomEvent.preventDefault(t)},setOpacity:function(t){return this.options.opacity=t,this._map&&this._updateOpacity(),this},_updateOpacity:function(){o.DomUtil.setOpacity(this._icon,this.options.opacity),this._shadow&&o.DomUtil.setOpacity(this._shadow,this.options.opacity)},_bringToFront:function(){this._updateZIndex(this.options.riseOffset)},_resetZIndex:function(){this._updateZIndex(0)}}),o.marker=function(t,e){return new o.Marker(t,e)},o.DivIcon=o.Icon.extend({options:{iconSize:[12,12],className:"leaflet-div-icon",html:!1},createIcon:function(t){var i=t&&"DIV"===t.tagName?t:e.createElement("div"),n=this.options;return i.innerHTML=n.html!==!1?n.html:"",n.bgPos&&(i.style.backgroundPosition=-n.bgPos.x+"px "+-n.bgPos.y+"px"),this._setIconStyles(i,"icon"),i},createShadow:function(){return null}}),o.divIcon=function(t){return new o.DivIcon(t)},o.Map.mergeOptions({closePopupOnClick:!0}),o.Popup=o.Class.extend({includes:o.Mixin.Events,options:{minWidth:50,maxWidth:300,autoPan:!0,closeButton:!0,offset:[0,7],autoPanPadding:[5,5],keepInView:!1,className:"",zoomAnimation:!0},initialize:function(t,e){o.setOptions(this,t),this._source=e,this._animated=o.Browser.any3d&&this.options.zoomAnimation,this._isOpen=!1},onAdd:function(t){this._map=t,this._container||this._initLayout();var e=t.options.fadeAnimation;e&&o.DomUtil.setOpacity(this._container,0),t._panes.popupPane.appendChild(this._container),t.on(this._getEvents(),this),this.update(),e&&o.DomUtil.setOpacity(this._container,1),this.fire("open"),t.fire("popupopen",{popup:this}),this._source&&this._source.fire("popupopen",{popup:this})},addTo:function(t){return t.addLayer(this),this},openOn:function(t){return t.openPopup(this),this},onRemove:function(t){t._panes.popupPane.removeChild(this._container),o.Util.falseFn(this._container.offsetWidth),t.off(this._getEvents(),this),t.options.fadeAnimation&&o.DomUtil.setOpacity(this._container,0),this._map=null,this.fire("close"),t.fire("popupclose",{popup:this}),this._source&&this._source.fire("popupclose",{popup:this})},getLatLng:function(){return this._latlng},setLatLng:function(t){return this._latlng=o.latLng(t),this._map&&(this._updatePosition(),this._adjustPan()),this},getContent:function(){return this._content},setContent:function(t){return this._content=t,this.update(),this},update:function(){this._map&&(this._container.style.visibility="hidden",this._updateContent(),this._updateLayout(),this._updatePosition(),this._container.style.visibility="",this._adjustPan())},_getEvents:function(){var t={viewreset:this._updatePosition};return this._animated&&(t.zoomanim=this._zoomAnimation),("closeOnClick"in this.options?this.options.closeOnClick:this._map.options.closePopupOnClick)&&(t.preclick=this._close),this.options.keepInView&&(t.moveend=this._adjustPan),t},_close:function(){this._map&&this._map.closePopup(this)},_initLayout:function(){var t,e="leaflet-popup",i=e+" "+this.options.className+" leaflet-zoom-"+(this._animated?"animated":"hide"),n=this._container=o.DomUtil.create("div",i);this.options.closeButton&&(t=this._closeButton=o.DomUtil.create("a",e+"-close-button",n),t.href="#close",t.innerHTML="&#215;",o.DomEvent.disableClickPropagation(t),o.DomEvent.on(t,"click",this._onCloseButtonClick,this));var s=this._wrapper=o.DomUtil.create("div",e+"-content-wrapper",n);o.DomEvent.disableClickPropagation(s),this._contentNode=o.DomUtil.create("div",e+"-content",s),o.DomEvent.disableScrollPropagation(this._contentNode),o.DomEvent.on(s,"contextmenu",o.DomEvent.stopPropagation),this._tipContainer=o.DomUtil.create("div",e+"-tip-container",n),this._tip=o.DomUtil.create("div",e+"-tip",this._tipContainer)},_updateContent:function(){if(this._content){if("string"==typeof this._content)this._contentNode.innerHTML=this._content;else{for(;this._contentNode.hasChildNodes();)this._contentNode.removeChild(this._contentNode.firstChild);this._contentNode.appendChild(this._content)}this.fire("contentupdate")}},_updateLayout:function(){var t=this._contentNode,e=t.style;e.width="",e.whiteSpace="nowrap";var i=t.offsetWidth;i=Math.min(i,this.options.maxWidth),i=Math.max(i,this.options.minWidth),e.width=i+1+"px",e.whiteSpace="",e.height="";var n=t.offsetHeight,s=this.options.maxHeight,a="leaflet-popup-scrolled";s&&n>s?(e.height=s+"px",o.DomUtil.addClass(t,a)):o.DomUtil.removeClass(t,a),this._containerWidth=this._container.offsetWidth},_updatePosition:function(){if(this._map){var t=this._map.latLngToLayerPoint(this._latlng),e=this._animated,i=o.point(this.options.offset);e&&o.DomUtil.setPosition(this._container,t),this._containerBottom=-i.y-(e?0:t.y),this._containerLeft=-Math.round(this._containerWidth/2)+i.x+(e?0:t.x),this._container.style.bottom=this._containerBottom+"px",this._container.style.left=this._containerLeft+"px"}},_zoomAnimation:function(t){var e=this._map._latLngToNewLayerPoint(this._latlng,t.zoom,t.center);o.DomUtil.setPosition(this._container,e)},_adjustPan:function(){if(this.options.autoPan){var t=this._map,e=this._container.offsetHeight,i=this._containerWidth,n=new o.Point(this._containerLeft,-e-this._containerBottom);this._animated&&n._add(o.DomUtil.getPosition(this._container));var s=t.layerPointToContainerPoint(n),a=o.point(this.options.autoPanPadding),r=o.point(this.options.autoPanPaddingTopLeft||a),h=o.point(this.options.autoPanPaddingBottomRight||a),l=t.getSize(),u=0,c=0;s.x+i+h.x>l.x&&(u=s.x+i-l.x+h.x),s.x-u-r.x<0&&(u=s.x-r.x),s.y+e+h.y>l.y&&(c=s.y+e-l.y+h.y),s.y-c-r.y<0&&(c=s.y-r.y),(u||c)&&t.fire("autopanstart").panBy([u,c])}},_onCloseButtonClick:function(t){this._close(),o.DomEvent.stop(t)}}),o.popup=function(t,e){return new o.Popup(t,e)},o.Map.include({openPopup:function(t,e,i){if(this.closePopup(),!(t instanceof o.Popup)){var n=t;t=new o.Popup(i).setLatLng(e).setContent(n)}return t._isOpen=!0,this._popup=t,this.addLayer(t)},closePopup:function(t){return t&&t!==this._popup||(t=this._popup,this._popup=null),t&&(this.removeLayer(t),t._isOpen=!1),this}}),o.Marker.include({openPopup:function(){return this._popup&&this._map&&!this._map.hasLayer(this._popup)&&(this._popup.setLatLng(this._latlng),this._map.openPopup(this._popup)),this},closePopup:function(){return this._popup&&this._popup._close(),this},togglePopup:function(){return this._popup&&(this._popup._isOpen?this.closePopup():this.openPopup()),this},bindPopup:function(t,e){var i=o.point(this.options.icon.options.popupAnchor||[0,0]);return i=i.add(o.Popup.prototype.options.offset),e&&e.offset&&(i=i.add(e.offset)),e=o.extend({offset:i},e),this._popupHandlersAdded||(this.on("click",this.togglePopup,this).on("remove",this.closePopup,this).on("move",this._movePopup,this),this._popupHandlersAdded=!0),t instanceof o.Popup?(o.setOptions(t,e),this._popup=t):this._popup=new o.Popup(e,this).setContent(t),this},setPopupContent:function(t){return this._popup&&this._popup.setContent(t),this},unbindPopup:function(){return this._popup&&(this._popup=null,this.off("click",this.togglePopup,this).off("remove",this.closePopup,this).off("move",this._movePopup,this),this._popupHandlersAdded=!1),this},getPopup:function(){return this._popup},_movePopup:function(t){this._popup.setLatLng(t.latlng)}}),o.LayerGroup=o.Class.extend({initialize:function(t){this._layers={};var e,i;if(t)for(e=0,i=t.length;i>e;e++)this.addLayer(t[e])},addLayer:function(t){var e=this.getLayerId(t);return this._layers[e]=t,this._map&&this._map.addLayer(t),this},removeLayer:function(t){var e=t in this._layers?t:this.getLayerId(t);return this._map&&this._layers[e]&&this._map.removeLayer(this._layers[e]),delete this._layers[e],this},hasLayer:function(t){return t?t in this._layers||this.getLayerId(t)in this._layers:!1},clearLayers:function(){return this.eachLayer(this.removeLayer,this),this},invoke:function(t){var e,i,n=Array.prototype.slice.call(arguments,1);for(e in this._layers)i=this._layers[e],i[t]&&i[t].apply(i,n);return this},onAdd:function(t){this._map=t,this.eachLayer(t.addLayer,t)},onRemove:function(t){this.eachLayer(t.removeLayer,t),this._map=null},addTo:function(t){return t.addLayer(this),this},eachLayer:function(t,e){for(var i in this._layers)t.call(e,this._layers[i]);return this},getLayer:function(t){return this._layers[t]},getLayers:function(){var t=[];for(var e in this._layers)t.push(this._layers[e]);return t},setZIndex:function(t){return this.invoke("setZIndex",t)},getLayerId:function(t){return o.stamp(t)}}),o.layerGroup=function(t){return new o.LayerGroup(t)},o.FeatureGroup=o.LayerGroup.extend({includes:o.Mixin.Events,statics:{EVENTS:"click dblclick mouseover mouseout mousemove contextmenu popupopen popupclose"},addLayer:function(t){return this.hasLayer(t)?this:("on"in t&&t.on(o.FeatureGroup.EVENTS,this._propagateEvent,this),o.LayerGroup.prototype.addLayer.call(this,t),this._popupContent&&t.bindPopup&&t.bindPopup(this._popupContent,this._popupOptions),this.fire("layeradd",{layer:t}))},removeLayer:function(t){return this.hasLayer(t)?(t in this._layers&&(t=this._layers[t]),t.off(o.FeatureGroup.EVENTS,this._propagateEvent,this),o.LayerGroup.prototype.removeLayer.call(this,t),this._popupContent&&this.invoke("unbindPopup"),this.fire("layerremove",{layer:t})):this},bindPopup:function(t,e){return this._popupContent=t,this._popupOptions=e,this.invoke("bindPopup",t,e)},openPopup:function(t){for(var e in this._layers){this._layers[e].openPopup(t);break}return this},setStyle:function(t){return this.invoke("setStyle",t)},bringToFront:function(){return this.invoke("bringToFront")},bringToBack:function(){return this.invoke("bringToBack")},getBounds:function(){var t=new o.LatLngBounds;return this.eachLayer(function(e){t.extend(e instanceof o.Marker?e.getLatLng():e.getBounds())}),t},_propagateEvent:function(t){t=o.extend({layer:t.target,target:this},t),this.fire(t.type,t)}}),o.featureGroup=function(t){return new o.FeatureGroup(t)},o.Path=o.Class.extend({includes:[o.Mixin.Events],statics:{CLIP_PADDING:function(){var e=o.Browser.mobile?1280:2e3,i=(e/Math.max(t.outerWidth,t.outerHeight)-1)/2;return Math.max(0,Math.min(.5,i))}()},options:{stroke:!0,color:"#0033ff",dashArray:null,lineCap:null,lineJoin:null,weight:5,opacity:.5,fill:!1,fillColor:null,fillOpacity:.2,clickable:!0},initialize:function(t){o.setOptions(this,t)},onAdd:function(t){this._map=t,this._container||(this._initElements(),this._initEvents()),this.projectLatlngs(),this._updatePath(),this._container&&this._map._pathRoot.appendChild(this._container),this.fire("add"),t.on({viewreset:this.projectLatlngs,moveend:this._updatePath},this)},addTo:function(t){return t.addLayer(this),this},onRemove:function(t){t._pathRoot.removeChild(this._container),this.fire("remove"),this._map=null,o.Browser.vml&&(this._container=null,this._stroke=null,this._fill=null),t.off({viewreset:this.projectLatlngs,moveend:this._updatePath},this)},projectLatlngs:function(){},setStyle:function(t){return o.setOptions(this,t),this._container&&this._updateStyle(),this},redraw:function(){return this._map&&(this.projectLatlngs(),this._updatePath()),this}}),o.Map.include({_updatePathViewport:function(){var t=o.Path.CLIP_PADDING,e=this.getSize(),i=o.DomUtil.getPosition(this._mapPane),n=i.multiplyBy(-1)._subtract(e.multiplyBy(t)._round()),s=n.add(e.multiplyBy(1+2*t)._round());this._pathViewport=new o.Bounds(n,s)}}),o.Path.SVG_NS="http://www.w3.org/2000/svg",o.Browser.svg=!(!e.createElementNS||!e.createElementNS(o.Path.SVG_NS,"svg").createSVGRect),o.Path=o.Path.extend({statics:{SVG:o.Browser.svg},bringToFront:function(){var t=this._map._pathRoot,e=this._container;return e&&t.lastChild!==e&&t.appendChild(e),this},bringToBack:function(){var t=this._map._pathRoot,e=this._container,i=t.firstChild;return e&&i!==e&&t.insertBefore(e,i),this},getPathString:function(){},_createElement:function(t){return e.createElementNS(o.Path.SVG_NS,t)},_initElements:function(){this._map._initPathRoot(),this._initPath(),this._initStyle()},_initPath:function(){this._container=this._createElement("g"),this._path=this._createElement("path"),this.options.className&&o.DomUtil.addClass(this._path,this.options.className),this._container.appendChild(this._path)},_initStyle:function(){this.options.stroke&&(this._path.setAttribute("stroke-linejoin","round"),this._path.setAttribute("stroke-linecap","round")),this.options.fill&&this._path.setAttribute("fill-rule","evenodd"),this.options.pointerEvents&&this._path.setAttribute("pointer-events",this.options.pointerEvents),this.options.clickable||this.options.pointerEvents||this._path.setAttribute("pointer-events","none"),this._updateStyle()},_updateStyle:function(){this.options.stroke?(this._path.setAttribute("stroke",this.options.color),this._path.setAttribute("stroke-opacity",this.options.opacity),this._path.setAttribute("stroke-width",this.options.weight),this.options.dashArray?this._path.setAttribute("stroke-dasharray",this.options.dashArray):this._path.removeAttribute("stroke-dasharray"),this.options.lineCap&&this._path.setAttribute("stroke-linecap",this.options.lineCap),this.options.lineJoin&&this._path.setAttribute("stroke-linejoin",this.options.lineJoin)):this._path.setAttribute("stroke","none"),this.options.fill?(this._path.setAttribute("fill",this.options.fillColor||this.options.color),this._path.setAttribute("fill-opacity",this.options.fillOpacity)):this._path.setAttribute("fill","none")},_updatePath:function(){var t=this.getPathString();t||(t="M0 0"),this._path.setAttribute("d",t)},_initEvents:function(){if(this.options.clickable){(o.Browser.svg||!o.Browser.vml)&&o.DomUtil.addClass(this._path,"leaflet-clickable"),o.DomEvent.on(this._container,"click",this._onMouseClick,this);for(var t=["dblclick","mousedown","mouseover","mouseout","mousemove","contextmenu"],e=0;e<t.length;e++)o.DomEvent.on(this._container,t[e],this._fireMouseEvent,this)}},_onMouseClick:function(t){this._map.dragging&&this._map.dragging.moved()||this._fireMouseEvent(t)},_fireMouseEvent:function(t){if(this.hasEventListeners(t.type)){var e=this._map,i=e.mouseEventToContainerPoint(t),n=e.containerPointToLayerPoint(i),s=e.layerPointToLatLng(n);this.fire(t.type,{latlng:s,layerPoint:n,containerPoint:i,originalEvent:t}),"contextmenu"===t.type&&o.DomEvent.preventDefault(t),"mousemove"!==t.type&&o.DomEvent.stopPropagation(t)}}}),o.Map.include({_initPathRoot:function(){this._pathRoot||(this._pathRoot=o.Path.prototype._createElement("svg"),this._panes.overlayPane.appendChild(this._pathRoot),this.options.zoomAnimation&&o.Browser.any3d?(o.DomUtil.addClass(this._pathRoot,"leaflet-zoom-animated"),this.on({zoomanim:this._animatePathZoom,zoomend:this._endPathZoom})):o.DomUtil.addClass(this._pathRoot,"leaflet-zoom-hide"),this.on("moveend",this._updateSvgViewport),this._updateSvgViewport())
},_animatePathZoom:function(t){var e=this.getZoomScale(t.zoom),i=this._getCenterOffset(t.center)._multiplyBy(-e)._add(this._pathViewport.min);this._pathRoot.style[o.DomUtil.TRANSFORM]=o.DomUtil.getTranslateString(i)+" scale("+e+") ",this._pathZooming=!0},_endPathZoom:function(){this._pathZooming=!1},_updateSvgViewport:function(){if(!this._pathZooming){this._updatePathViewport();var t=this._pathViewport,e=t.min,i=t.max,n=i.x-e.x,s=i.y-e.y,a=this._pathRoot,r=this._panes.overlayPane;o.Browser.mobileWebkit&&r.removeChild(a),o.DomUtil.setPosition(a,e),a.setAttribute("width",n),a.setAttribute("height",s),a.setAttribute("viewBox",[e.x,e.y,n,s].join(" ")),o.Browser.mobileWebkit&&r.appendChild(a)}}}),o.Path.include({bindPopup:function(t,e){return t instanceof o.Popup?this._popup=t:((!this._popup||e)&&(this._popup=new o.Popup(e,this)),this._popup.setContent(t)),this._popupHandlersAdded||(this.on("click",this._openPopup,this).on("remove",this.closePopup,this),this._popupHandlersAdded=!0),this},unbindPopup:function(){return this._popup&&(this._popup=null,this.off("click",this._openPopup).off("remove",this.closePopup),this._popupHandlersAdded=!1),this},openPopup:function(t){return this._popup&&(t=t||this._latlng||this._latlngs[Math.floor(this._latlngs.length/2)],this._openPopup({latlng:t})),this},closePopup:function(){return this._popup&&this._popup._close(),this},_openPopup:function(t){this._popup.setLatLng(t.latlng),this._map.openPopup(this._popup)}}),o.Browser.vml=!o.Browser.svg&&function(){try{var t=e.createElement("div");t.innerHTML='<v:shape adj="1"/>';var i=t.firstChild;return i.style.behavior="url(#default#VML)",i&&"object"==typeof i.adj}catch(n){return!1}}(),o.Path=o.Browser.svg||!o.Browser.vml?o.Path:o.Path.extend({statics:{VML:!0,CLIP_PADDING:.02},_createElement:function(){try{return e.namespaces.add("lvml","urn:schemas-microsoft-com:vml"),function(t){return e.createElement("<lvml:"+t+' class="lvml">')}}catch(t){return function(t){return e.createElement("<"+t+' xmlns="urn:schemas-microsoft.com:vml" class="lvml">')}}}(),_initPath:function(){var t=this._container=this._createElement("shape");o.DomUtil.addClass(t,"leaflet-vml-shape"+(this.options.className?" "+this.options.className:"")),this.options.clickable&&o.DomUtil.addClass(t,"leaflet-clickable"),t.coordsize="1 1",this._path=this._createElement("path"),t.appendChild(this._path),this._map._pathRoot.appendChild(t)},_initStyle:function(){this._updateStyle()},_updateStyle:function(){var t=this._stroke,e=this._fill,i=this.options,n=this._container;n.stroked=i.stroke,n.filled=i.fill,i.stroke?(t||(t=this._stroke=this._createElement("stroke"),t.endcap="round",n.appendChild(t)),t.weight=i.weight+"px",t.color=i.color,t.opacity=i.opacity,t.dashStyle=i.dashArray?o.Util.isArray(i.dashArray)?i.dashArray.join(" "):i.dashArray.replace(/( *, *)/g," "):"",i.lineCap&&(t.endcap=i.lineCap.replace("butt","flat")),i.lineJoin&&(t.joinstyle=i.lineJoin)):t&&(n.removeChild(t),this._stroke=null),i.fill?(e||(e=this._fill=this._createElement("fill"),n.appendChild(e)),e.color=i.fillColor||i.color,e.opacity=i.fillOpacity):e&&(n.removeChild(e),this._fill=null)},_updatePath:function(){var t=this._container.style;t.display="none",this._path.v=this.getPathString()+" ",t.display=""}}),o.Map.include(o.Browser.svg||!o.Browser.vml?{}:{_initPathRoot:function(){if(!this._pathRoot){var t=this._pathRoot=e.createElement("div");t.className="leaflet-vml-container",this._panes.overlayPane.appendChild(t),this.on("moveend",this._updatePathViewport),this._updatePathViewport()}}}),o.Browser.canvas=function(){return!!e.createElement("canvas").getContext}(),o.Path=o.Path.SVG&&!t.L_PREFER_CANVAS||!o.Browser.canvas?o.Path:o.Path.extend({statics:{CANVAS:!0,SVG:!1},redraw:function(){return this._map&&(this.projectLatlngs(),this._requestUpdate()),this},setStyle:function(t){return o.setOptions(this,t),this._map&&(this._updateStyle(),this._requestUpdate()),this},onRemove:function(t){t.off("viewreset",this.projectLatlngs,this).off("moveend",this._updatePath,this),this.options.clickable&&(this._map.off("click",this._onClick,this),this._map.off("mousemove",this._onMouseMove,this)),this._requestUpdate(),this.fire("remove"),this._map=null},_requestUpdate:function(){this._map&&!o.Path._updateRequest&&(o.Path._updateRequest=o.Util.requestAnimFrame(this._fireMapMoveEnd,this._map))},_fireMapMoveEnd:function(){o.Path._updateRequest=null,this.fire("moveend")},_initElements:function(){this._map._initPathRoot(),this._ctx=this._map._canvasCtx},_updateStyle:function(){var t=this.options;t.stroke&&(this._ctx.lineWidth=t.weight,this._ctx.strokeStyle=t.color),t.fill&&(this._ctx.fillStyle=t.fillColor||t.color)},_drawPath:function(){var t,e,i,n,s,a;for(this._ctx.beginPath(),t=0,i=this._parts.length;i>t;t++){for(e=0,n=this._parts[t].length;n>e;e++)s=this._parts[t][e],a=(0===e?"move":"line")+"To",this._ctx[a](s.x,s.y);this instanceof o.Polygon&&this._ctx.closePath()}},_checkIfEmpty:function(){return!this._parts.length},_updatePath:function(){if(!this._checkIfEmpty()){var t=this._ctx,e=this.options;this._drawPath(),t.save(),this._updateStyle(),e.fill&&(t.globalAlpha=e.fillOpacity,t.fill()),e.stroke&&(t.globalAlpha=e.opacity,t.stroke()),t.restore()}},_initEvents:function(){this.options.clickable&&(this._map.on("mousemove",this._onMouseMove,this),this._map.on("click",this._onClick,this))},_onClick:function(t){this._containsPoint(t.layerPoint)&&this.fire("click",t)},_onMouseMove:function(t){this._map&&!this._map._animatingZoom&&(this._containsPoint(t.layerPoint)?(this._ctx.canvas.style.cursor="pointer",this._mouseInside=!0,this.fire("mouseover",t)):this._mouseInside&&(this._ctx.canvas.style.cursor="",this._mouseInside=!1,this.fire("mouseout",t)))}}),o.Map.include(o.Path.SVG&&!t.L_PREFER_CANVAS||!o.Browser.canvas?{}:{_initPathRoot:function(){var t,i=this._pathRoot;i||(i=this._pathRoot=e.createElement("canvas"),i.style.position="absolute",t=this._canvasCtx=i.getContext("2d"),t.lineCap="round",t.lineJoin="round",this._panes.overlayPane.appendChild(i),this.options.zoomAnimation&&(this._pathRoot.className="leaflet-zoom-animated",this.on("zoomanim",this._animatePathZoom),this.on("zoomend",this._endPathZoom)),this.on("moveend",this._updateCanvasViewport),this._updateCanvasViewport())},_updateCanvasViewport:function(){if(!this._pathZooming){this._updatePathViewport();var t=this._pathViewport,e=t.min,i=t.max.subtract(e),n=this._pathRoot;o.DomUtil.setPosition(n,e),n.width=i.x,n.height=i.y,n.getContext("2d").translate(-e.x,-e.y)}}}),o.LineUtil={simplify:function(t,e){if(!e||!t.length)return t.slice();var i=e*e;return t=this._reducePoints(t,i),t=this._simplifyDP(t,i)},pointToSegmentDistance:function(t,e,i){return Math.sqrt(this._sqClosestPointOnSegment(t,e,i,!0))},closestPointOnSegment:function(t,e,i){return this._sqClosestPointOnSegment(t,e,i)},_simplifyDP:function(t,e){var n=t.length,o=typeof Uint8Array!=i+""?Uint8Array:Array,s=new o(n);s[0]=s[n-1]=1,this._simplifyDPStep(t,s,e,0,n-1);var a,r=[];for(a=0;n>a;a++)s[a]&&r.push(t[a]);return r},_simplifyDPStep:function(t,e,i,n,o){var s,a,r,h=0;for(a=n+1;o-1>=a;a++)r=this._sqClosestPointOnSegment(t[a],t[n],t[o],!0),r>h&&(s=a,h=r);h>i&&(e[s]=1,this._simplifyDPStep(t,e,i,n,s),this._simplifyDPStep(t,e,i,s,o))},_reducePoints:function(t,e){for(var i=[t[0]],n=1,o=0,s=t.length;s>n;n++)this._sqDist(t[n],t[o])>e&&(i.push(t[n]),o=n);return s-1>o&&i.push(t[s-1]),i},clipSegment:function(t,e,i,n){var o,s,a,r=n?this._lastCode:this._getBitCode(t,i),h=this._getBitCode(e,i);for(this._lastCode=h;;){if(!(r|h))return[t,e];if(r&h)return!1;o=r||h,s=this._getEdgeIntersection(t,e,o,i),a=this._getBitCode(s,i),o===r?(t=s,r=a):(e=s,h=a)}},_getEdgeIntersection:function(t,e,i,n){var s=e.x-t.x,a=e.y-t.y,r=n.min,h=n.max;return 8&i?new o.Point(t.x+s*(h.y-t.y)/a,h.y):4&i?new o.Point(t.x+s*(r.y-t.y)/a,r.y):2&i?new o.Point(h.x,t.y+a*(h.x-t.x)/s):1&i?new o.Point(r.x,t.y+a*(r.x-t.x)/s):void 0},_getBitCode:function(t,e){var i=0;return t.x<e.min.x?i|=1:t.x>e.max.x&&(i|=2),t.y<e.min.y?i|=4:t.y>e.max.y&&(i|=8),i},_sqDist:function(t,e){var i=e.x-t.x,n=e.y-t.y;return i*i+n*n},_sqClosestPointOnSegment:function(t,e,i,n){var s,a=e.x,r=e.y,h=i.x-a,l=i.y-r,u=h*h+l*l;return u>0&&(s=((t.x-a)*h+(t.y-r)*l)/u,s>1?(a=i.x,r=i.y):s>0&&(a+=h*s,r+=l*s)),h=t.x-a,l=t.y-r,n?h*h+l*l:new o.Point(a,r)}},o.Polyline=o.Path.extend({initialize:function(t,e){o.Path.prototype.initialize.call(this,e),this._latlngs=this._convertLatLngs(t)},options:{smoothFactor:1,noClip:!1},projectLatlngs:function(){this._originalPoints=[];for(var t=0,e=this._latlngs.length;e>t;t++)this._originalPoints[t]=this._map.latLngToLayerPoint(this._latlngs[t])},getPathString:function(){for(var t=0,e=this._parts.length,i="";e>t;t++)i+=this._getPathPartStr(this._parts[t]);return i},getLatLngs:function(){return this._latlngs},setLatLngs:function(t){return this._latlngs=this._convertLatLngs(t),this.redraw()},addLatLng:function(t){return this._latlngs.push(o.latLng(t)),this.redraw()},spliceLatLngs:function(){var t=[].splice.apply(this._latlngs,arguments);return this._convertLatLngs(this._latlngs,!0),this.redraw(),t},closestLayerPoint:function(t){for(var e,i,n=1/0,s=this._parts,a=null,r=0,h=s.length;h>r;r++)for(var l=s[r],u=1,c=l.length;c>u;u++){e=l[u-1],i=l[u];var d=o.LineUtil._sqClosestPointOnSegment(t,e,i,!0);n>d&&(n=d,a=o.LineUtil._sqClosestPointOnSegment(t,e,i))}return a&&(a.distance=Math.sqrt(n)),a},getBounds:function(){return new o.LatLngBounds(this.getLatLngs())},_convertLatLngs:function(t,e){var i,n,s=e?t:[];for(i=0,n=t.length;n>i;i++){if(o.Util.isArray(t[i])&&"number"!=typeof t[i][0])return;s[i]=o.latLng(t[i])}return s},_initEvents:function(){o.Path.prototype._initEvents.call(this)},_getPathPartStr:function(t){for(var e,i=o.Path.VML,n=0,s=t.length,a="";s>n;n++)e=t[n],i&&e._round(),a+=(n?"L":"M")+e.x+" "+e.y;return a},_clipPoints:function(){var t,e,i,n=this._originalPoints,s=n.length;if(this.options.noClip)return void(this._parts=[n]);this._parts=[];var a=this._parts,r=this._map._pathViewport,h=o.LineUtil;for(t=0,e=0;s-1>t;t++)i=h.clipSegment(n[t],n[t+1],r,t),i&&(a[e]=a[e]||[],a[e].push(i[0]),(i[1]!==n[t+1]||t===s-2)&&(a[e].push(i[1]),e++))},_simplifyPoints:function(){for(var t=this._parts,e=o.LineUtil,i=0,n=t.length;n>i;i++)t[i]=e.simplify(t[i],this.options.smoothFactor)},_updatePath:function(){this._map&&(this._clipPoints(),this._simplifyPoints(),o.Path.prototype._updatePath.call(this))}}),o.polyline=function(t,e){return new o.Polyline(t,e)},o.PolyUtil={},o.PolyUtil.clipPolygon=function(t,e){var i,n,s,a,r,h,l,u,c,d=[1,4,2,8],p=o.LineUtil;for(n=0,l=t.length;l>n;n++)t[n]._code=p._getBitCode(t[n],e);for(a=0;4>a;a++){for(u=d[a],i=[],n=0,l=t.length,s=l-1;l>n;s=n++)r=t[n],h=t[s],r._code&u?h._code&u||(c=p._getEdgeIntersection(h,r,u,e),c._code=p._getBitCode(c,e),i.push(c)):(h._code&u&&(c=p._getEdgeIntersection(h,r,u,e),c._code=p._getBitCode(c,e),i.push(c)),i.push(r));t=i}return t},o.Polygon=o.Polyline.extend({options:{fill:!0},initialize:function(t,e){o.Polyline.prototype.initialize.call(this,t,e),this._initWithHoles(t)},_initWithHoles:function(t){var e,i,n;if(t&&o.Util.isArray(t[0])&&"number"!=typeof t[0][0])for(this._latlngs=this._convertLatLngs(t[0]),this._holes=t.slice(1),e=0,i=this._holes.length;i>e;e++)n=this._holes[e]=this._convertLatLngs(this._holes[e]),n[0].equals(n[n.length-1])&&n.pop();t=this._latlngs,t.length>=2&&t[0].equals(t[t.length-1])&&t.pop()},projectLatlngs:function(){if(o.Polyline.prototype.projectLatlngs.call(this),this._holePoints=[],this._holes){var t,e,i,n;for(t=0,i=this._holes.length;i>t;t++)for(this._holePoints[t]=[],e=0,n=this._holes[t].length;n>e;e++)this._holePoints[t][e]=this._map.latLngToLayerPoint(this._holes[t][e])}},setLatLngs:function(t){return t&&o.Util.isArray(t[0])&&"number"!=typeof t[0][0]?(this._initWithHoles(t),this.redraw()):o.Polyline.prototype.setLatLngs.call(this,t)},_clipPoints:function(){var t=this._originalPoints,e=[];if(this._parts=[t].concat(this._holePoints),!this.options.noClip){for(var i=0,n=this._parts.length;n>i;i++){var s=o.PolyUtil.clipPolygon(this._parts[i],this._map._pathViewport);s.length&&e.push(s)}this._parts=e}},_getPathPartStr:function(t){var e=o.Polyline.prototype._getPathPartStr.call(this,t);return e+(o.Browser.svg?"z":"x")}}),o.polygon=function(t,e){return new o.Polygon(t,e)},function(){function t(t){return o.FeatureGroup.extend({initialize:function(t,e){this._layers={},this._options=e,this.setLatLngs(t)},setLatLngs:function(e){var i=0,n=e.length;for(this.eachLayer(function(t){n>i?t.setLatLngs(e[i++]):this.removeLayer(t)},this);n>i;)this.addLayer(new t(e[i++],this._options));return this},getLatLngs:function(){var t=[];return this.eachLayer(function(e){t.push(e.getLatLngs())}),t}})}o.MultiPolyline=t(o.Polyline),o.MultiPolygon=t(o.Polygon),o.multiPolyline=function(t,e){return new o.MultiPolyline(t,e)},o.multiPolygon=function(t,e){return new o.MultiPolygon(t,e)}}(),o.Rectangle=o.Polygon.extend({initialize:function(t,e){o.Polygon.prototype.initialize.call(this,this._boundsToLatLngs(t),e)},setBounds:function(t){this.setLatLngs(this._boundsToLatLngs(t))},_boundsToLatLngs:function(t){return t=o.latLngBounds(t),[t.getSouthWest(),t.getNorthWest(),t.getNorthEast(),t.getSouthEast()]}}),o.rectangle=function(t,e){return new o.Rectangle(t,e)},o.Circle=o.Path.extend({initialize:function(t,e,i){o.Path.prototype.initialize.call(this,i),this._latlng=o.latLng(t),this._mRadius=e},options:{fill:!0},setLatLng:function(t){return this._latlng=o.latLng(t),this.redraw()},setRadius:function(t){return this._mRadius=t,this.redraw()},projectLatlngs:function(){var t=this._getLngRadius(),e=this._latlng,i=this._map.latLngToLayerPoint([e.lat,e.lng-t]);this._point=this._map.latLngToLayerPoint(e),this._radius=Math.max(this._point.x-i.x,1)},getBounds:function(){var t=this._getLngRadius(),e=this._mRadius/40075017*360,i=this._latlng;return new o.LatLngBounds([i.lat-e,i.lng-t],[i.lat+e,i.lng+t])},getLatLng:function(){return this._latlng},getPathString:function(){var t=this._point,e=this._radius;return this._checkIfEmpty()?"":o.Browser.svg?"M"+t.x+","+(t.y-e)+"A"+e+","+e+",0,1,1,"+(t.x-.1)+","+(t.y-e)+" z":(t._round(),e=Math.round(e),"AL "+t.x+","+t.y+" "+e+","+e+" 0,23592600")},getRadius:function(){return this._mRadius},_getLatRadius:function(){return this._mRadius/40075017*360},_getLngRadius:function(){return this._getLatRadius()/Math.cos(o.LatLng.DEG_TO_RAD*this._latlng.lat)},_checkIfEmpty:function(){if(!this._map)return!1;var t=this._map._pathViewport,e=this._radius,i=this._point;return i.x-e>t.max.x||i.y-e>t.max.y||i.x+e<t.min.x||i.y+e<t.min.y}}),o.circle=function(t,e,i){return new o.Circle(t,e,i)},o.CircleMarker=o.Circle.extend({options:{radius:10,weight:2},initialize:function(t,e){o.Circle.prototype.initialize.call(this,t,null,e),this._radius=this.options.radius},projectLatlngs:function(){this._point=this._map.latLngToLayerPoint(this._latlng)},_updateStyle:function(){o.Circle.prototype._updateStyle.call(this),this.setRadius(this.options.radius)},setLatLng:function(t){return o.Circle.prototype.setLatLng.call(this,t),this._popup&&this._popup._isOpen&&this._popup.setLatLng(t),this},setRadius:function(t){return this.options.radius=this._radius=t,this.redraw()},getRadius:function(){return this._radius}}),o.circleMarker=function(t,e){return new o.CircleMarker(t,e)},o.Polyline.include(o.Path.CANVAS?{_containsPoint:function(t,e){var i,n,s,a,r,h,l,u=this.options.weight/2;for(o.Browser.touch&&(u+=10),i=0,a=this._parts.length;a>i;i++)for(l=this._parts[i],n=0,r=l.length,s=r-1;r>n;s=n++)if((e||0!==n)&&(h=o.LineUtil.pointToSegmentDistance(t,l[s],l[n]),u>=h))return!0;return!1}}:{}),o.Polygon.include(o.Path.CANVAS?{_containsPoint:function(t){var e,i,n,s,a,r,h,l,u=!1;if(o.Polyline.prototype._containsPoint.call(this,t,!0))return!0;for(s=0,h=this._parts.length;h>s;s++)for(e=this._parts[s],a=0,l=e.length,r=l-1;l>a;r=a++)i=e[a],n=e[r],i.y>t.y!=n.y>t.y&&t.x<(n.x-i.x)*(t.y-i.y)/(n.y-i.y)+i.x&&(u=!u);return u}}:{}),o.Circle.include(o.Path.CANVAS?{_drawPath:function(){var t=this._point;this._ctx.beginPath(),this._ctx.arc(t.x,t.y,this._radius,0,2*Math.PI,!1)},_containsPoint:function(t){var e=this._point,i=this.options.stroke?this.options.weight/2:0;return t.distanceTo(e)<=this._radius+i}}:{}),o.CircleMarker.include(o.Path.CANVAS?{_updateStyle:function(){o.Path.prototype._updateStyle.call(this)}}:{}),o.GeoJSON=o.FeatureGroup.extend({initialize:function(t,e){o.setOptions(this,e),this._layers={},t&&this.addData(t)},addData:function(t){var e,i,n,s=o.Util.isArray(t)?t:t.features;if(s){for(e=0,i=s.length;i>e;e++)n=s[e],(n.geometries||n.geometry||n.features||n.coordinates)&&this.addData(s[e]);return this}var a=this.options;if(!a.filter||a.filter(t)){var r=o.GeoJSON.geometryToLayer(t,a.pointToLayer,a.coordsToLatLng,a);return r.feature=o.GeoJSON.asFeature(t),r.defaultOptions=r.options,this.resetStyle(r),a.onEachFeature&&a.onEachFeature(t,r),this.addLayer(r)}},resetStyle:function(t){var e=this.options.style;e&&(o.Util.extend(t.options,t.defaultOptions),this._setLayerStyle(t,e))},setStyle:function(t){this.eachLayer(function(e){this._setLayerStyle(e,t)},this)},_setLayerStyle:function(t,e){"function"==typeof e&&(e=e(t.feature)),t.setStyle&&t.setStyle(e)}}),o.extend(o.GeoJSON,{geometryToLayer:function(t,e,i,n){var s,a,r,h,l="Feature"===t.type?t.geometry:t,u=l.coordinates,c=[];switch(i=i||this.coordsToLatLng,l.type){case"Point":return s=i(u),e?e(t,s):new o.Marker(s);case"MultiPoint":for(r=0,h=u.length;h>r;r++)s=i(u[r]),c.push(e?e(t,s):new o.Marker(s));return new o.FeatureGroup(c);case"LineString":return a=this.coordsToLatLngs(u,0,i),new o.Polyline(a,n);case"Polygon":if(2===u.length&&!u[1].length)throw new Error("Invalid GeoJSON object.");return a=this.coordsToLatLngs(u,1,i),new o.Polygon(a,n);case"MultiLineString":return a=this.coordsToLatLngs(u,1,i),new o.MultiPolyline(a,n);case"MultiPolygon":return a=this.coordsToLatLngs(u,2,i),new o.MultiPolygon(a,n);case"GeometryCollection":for(r=0,h=l.geometries.length;h>r;r++)c.push(this.geometryToLayer({geometry:l.geometries[r],type:"Feature",properties:t.properties},e,i,n));return new o.FeatureGroup(c);default:throw new Error("Invalid GeoJSON object.")}},coordsToLatLng:function(t){return new o.LatLng(t[1],t[0],t[2])},coordsToLatLngs:function(t,e,i){var n,o,s,a=[];for(o=0,s=t.length;s>o;o++)n=e?this.coordsToLatLngs(t[o],e-1,i):(i||this.coordsToLatLng)(t[o]),a.push(n);return a},latLngToCoords:function(t){var e=[t.lng,t.lat];return t.alt!==i&&e.push(t.alt),e},latLngsToCoords:function(t){for(var e=[],i=0,n=t.length;n>i;i++)e.push(o.GeoJSON.latLngToCoords(t[i]));return e},getFeature:function(t,e){return t.feature?o.extend({},t.feature,{geometry:e}):o.GeoJSON.asFeature(e)},asFeature:function(t){return"Feature"===t.type?t:{type:"Feature",properties:{},geometry:t}}});var a={toGeoJSON:function(){return o.GeoJSON.getFeature(this,{type:"Point",coordinates:o.GeoJSON.latLngToCoords(this.getLatLng())})}};o.Marker.include(a),o.Circle.include(a),o.CircleMarker.include(a),o.Polyline.include({toGeoJSON:function(){return o.GeoJSON.getFeature(this,{type:"LineString",coordinates:o.GeoJSON.latLngsToCoords(this.getLatLngs())})}}),o.Polygon.include({toGeoJSON:function(){var t,e,i,n=[o.GeoJSON.latLngsToCoords(this.getLatLngs())];if(n[0].push(n[0][0]),this._holes)for(t=0,e=this._holes.length;e>t;t++)i=o.GeoJSON.latLngsToCoords(this._holes[t]),i.push(i[0]),n.push(i);return o.GeoJSON.getFeature(this,{type:"Polygon",coordinates:n})}}),function(){function t(t){return function(){var e=[];return this.eachLayer(function(t){e.push(t.toGeoJSON().geometry.coordinates)}),o.GeoJSON.getFeature(this,{type:t,coordinates:e})}}o.MultiPolyline.include({toGeoJSON:t("MultiLineString")}),o.MultiPolygon.include({toGeoJSON:t("MultiPolygon")}),o.LayerGroup.include({toGeoJSON:function(){var e,i=this.feature&&this.feature.geometry,n=[];if(i&&"MultiPoint"===i.type)return t("MultiPoint").call(this);var s=i&&"GeometryCollection"===i.type;return this.eachLayer(function(t){t.toGeoJSON&&(e=t.toGeoJSON(),n.push(s?e.geometry:o.GeoJSON.asFeature(e)))}),s?o.GeoJSON.getFeature(this,{geometries:n,type:"GeometryCollection"}):{type:"FeatureCollection",features:n}}})}(),o.geoJson=function(t,e){return new o.GeoJSON(t,e)},o.DomEvent={addListener:function(t,e,i,n){var s,a,r,h=o.stamp(i),l="_leaflet_"+e+h;return t[l]?this:(s=function(e){return i.call(n||t,e||o.DomEvent._getEvent())},o.Browser.pointer&&0===e.indexOf("touch")?this.addPointerListener(t,e,s,h):(o.Browser.touch&&"dblclick"===e&&this.addDoubleTapListener&&this.addDoubleTapListener(t,s,h),"addEventListener"in t?"mousewheel"===e?(t.addEventListener("DOMMouseScroll",s,!1),t.addEventListener(e,s,!1)):"mouseenter"===e||"mouseleave"===e?(a=s,r="mouseenter"===e?"mouseover":"mouseout",s=function(e){return o.DomEvent._checkMouse(t,e)?a(e):void 0},t.addEventListener(r,s,!1)):"click"===e&&o.Browser.android?(a=s,s=function(t){return o.DomEvent._filterClick(t,a)},t.addEventListener(e,s,!1)):t.addEventListener(e,s,!1):"attachEvent"in t&&t.attachEvent("on"+e,s),t[l]=s,this))},removeListener:function(t,e,i){var n=o.stamp(i),s="_leaflet_"+e+n,a=t[s];return a?(o.Browser.pointer&&0===e.indexOf("touch")?this.removePointerListener(t,e,n):o.Browser.touch&&"dblclick"===e&&this.removeDoubleTapListener?this.removeDoubleTapListener(t,n):"removeEventListener"in t?"mousewheel"===e?(t.removeEventListener("DOMMouseScroll",a,!1),t.removeEventListener(e,a,!1)):"mouseenter"===e||"mouseleave"===e?t.removeEventListener("mouseenter"===e?"mouseover":"mouseout",a,!1):t.removeEventListener(e,a,!1):"detachEvent"in t&&t.detachEvent("on"+e,a),t[s]=null,this):this},stopPropagation:function(t){return t.stopPropagation?t.stopPropagation():t.cancelBubble=!0,o.DomEvent._skipped(t),this},disableScrollPropagation:function(t){var e=o.DomEvent.stopPropagation;return o.DomEvent.on(t,"mousewheel",e).on(t,"MozMousePixelScroll",e)},disableClickPropagation:function(t){for(var e=o.DomEvent.stopPropagation,i=o.Draggable.START.length-1;i>=0;i--)o.DomEvent.on(t,o.Draggable.START[i],e);return o.DomEvent.on(t,"click",o.DomEvent._fakeStop).on(t,"dblclick",e)},preventDefault:function(t){return t.preventDefault?t.preventDefault():t.returnValue=!1,this},stop:function(t){return o.DomEvent.preventDefault(t).stopPropagation(t)},getMousePosition:function(t,e){if(!e)return new o.Point(t.clientX,t.clientY);var i=e.getBoundingClientRect();return new o.Point(t.clientX-i.left-e.clientLeft,t.clientY-i.top-e.clientTop)},getWheelDelta:function(t){var e=0;return t.wheelDelta&&(e=t.wheelDelta/120),t.detail&&(e=-t.detail/3),e},_skipEvents:{},_fakeStop:function(t){o.DomEvent._skipEvents[t.type]=!0},_skipped:function(t){var e=this._skipEvents[t.type];return this._skipEvents[t.type]=!1,e},_checkMouse:function(t,e){var i=e.relatedTarget;if(!i)return!0;try{for(;i&&i!==t;)i=i.parentNode}catch(n){return!1}return i!==t},_getEvent:function(){var e=t.event;if(!e)for(var i=arguments.callee.caller;i&&(e=i.arguments[0],!e||t.Event!==e.constructor);)i=i.caller;return e},_filterClick:function(t,e){var i=t.timeStamp||t.originalEvent.timeStamp,n=o.DomEvent._lastClick&&i-o.DomEvent._lastClick;return n&&n>100&&500>n||t.target._simulatedClick&&!t._simulated?void o.DomEvent.stop(t):(o.DomEvent._lastClick=i,e(t))}},o.DomEvent.on=o.DomEvent.addListener,o.DomEvent.off=o.DomEvent.removeListener,o.Draggable=o.Class.extend({includes:o.Mixin.Events,statics:{START:o.Browser.touch?["touchstart","mousedown"]:["mousedown"],END:{mousedown:"mouseup",touchstart:"touchend",pointerdown:"touchend",MSPointerDown:"touchend"},MOVE:{mousedown:"mousemove",touchstart:"touchmove",pointerdown:"touchmove",MSPointerDown:"touchmove"}},initialize:function(t,e){this._element=t,this._dragStartTarget=e||t},enable:function(){if(!this._enabled){for(var t=o.Draggable.START.length-1;t>=0;t--)o.DomEvent.on(this._dragStartTarget,o.Draggable.START[t],this._onDown,this);this._enabled=!0}},disable:function(){if(this._enabled){for(var t=o.Draggable.START.length-1;t>=0;t--)o.DomEvent.off(this._dragStartTarget,o.Draggable.START[t],this._onDown,this);this._enabled=!1,this._moved=!1}},_onDown:function(t){if(this._moved=!1,!(t.shiftKey||1!==t.which&&1!==t.button&&!t.touches||(o.DomEvent.stopPropagation(t),o.Draggable._disabled||(o.DomUtil.disableImageDrag(),o.DomUtil.disableTextSelection(),this._moving)))){var i=t.touches?t.touches[0]:t;this._startPoint=new o.Point(i.clientX,i.clientY),this._startPos=this._newPos=o.DomUtil.getPosition(this._element),o.DomEvent.on(e,o.Draggable.MOVE[t.type],this._onMove,this).on(e,o.Draggable.END[t.type],this._onUp,this)}},_onMove:function(t){if(t.touches&&t.touches.length>1)return void(this._moved=!0);var i=t.touches&&1===t.touches.length?t.touches[0]:t,n=new o.Point(i.clientX,i.clientY),s=n.subtract(this._startPoint);(s.x||s.y)&&(o.Browser.touch&&Math.abs(s.x)+Math.abs(s.y)<3||(o.DomEvent.preventDefault(t),this._moved||(this.fire("dragstart"),this._moved=!0,this._startPos=o.DomUtil.getPosition(this._element).subtract(s),o.DomUtil.addClass(e.body,"leaflet-dragging"),this._lastTarget=t.target||t.srcElement,o.DomUtil.addClass(this._lastTarget,"leaflet-drag-target")),this._newPos=this._startPos.add(s),this._moving=!0,o.Util.cancelAnimFrame(this._animRequest),this._animRequest=o.Util.requestAnimFrame(this._updatePosition,this,!0,this._dragStartTarget)))},_updatePosition:function(){this.fire("predrag"),o.DomUtil.setPosition(this._element,this._newPos),this.fire("drag")},_onUp:function(){o.DomUtil.removeClass(e.body,"leaflet-dragging"),this._lastTarget&&(o.DomUtil.removeClass(this._lastTarget,"leaflet-drag-target"),this._lastTarget=null);for(var t in o.Draggable.MOVE)o.DomEvent.off(e,o.Draggable.MOVE[t],this._onMove).off(e,o.Draggable.END[t],this._onUp);o.DomUtil.enableImageDrag(),o.DomUtil.enableTextSelection(),this._moved&&this._moving&&(o.Util.cancelAnimFrame(this._animRequest),this.fire("dragend",{distance:this._newPos.distanceTo(this._startPos)})),this._moving=!1}}),o.Handler=o.Class.extend({initialize:function(t){this._map=t},enable:function(){this._enabled||(this._enabled=!0,this.addHooks())},disable:function(){this._enabled&&(this._enabled=!1,this.removeHooks())},enabled:function(){return!!this._enabled}}),o.Map.mergeOptions({dragging:!0,inertia:!o.Browser.android23,inertiaDeceleration:3400,inertiaMaxSpeed:1/0,inertiaThreshold:o.Browser.touch?32:18,easeLinearity:.25,worldCopyJump:!1}),o.Map.Drag=o.Handler.extend({addHooks:function(){if(!this._draggable){var t=this._map;this._draggable=new o.Draggable(t._mapPane,t._container),this._draggable.on({dragstart:this._onDragStart,drag:this._onDrag,dragend:this._onDragEnd},this),t.options.worldCopyJump&&(this._draggable.on("predrag",this._onPreDrag,this),t.on("viewreset",this._onViewReset,this),t.whenReady(this._onViewReset,this))}this._draggable.enable()},removeHooks:function(){this._draggable.disable()},moved:function(){return this._draggable&&this._draggable._moved},_onDragStart:function(){var t=this._map;t._panAnim&&t._panAnim.stop(),t.fire("movestart").fire("dragstart"),t.options.inertia&&(this._positions=[],this._times=[])},_onDrag:function(){if(this._map.options.inertia){var t=this._lastTime=+new Date,e=this._lastPos=this._draggable._newPos;this._positions.push(e),this._times.push(t),t-this._times[0]>200&&(this._positions.shift(),this._times.shift())}this._map.fire("move").fire("drag")},_onViewReset:function(){var t=this._map.getSize()._divideBy(2),e=this._map.latLngToLayerPoint([0,0]);this._initialWorldOffset=e.subtract(t).x,this._worldWidth=this._map.project([0,180]).x},_onPreDrag:function(){var t=this._worldWidth,e=Math.round(t/2),i=this._initialWorldOffset,n=this._draggable._newPos.x,o=(n-e+i)%t+e-i,s=(n+e+i)%t-e-i,a=Math.abs(o+i)<Math.abs(s+i)?o:s;this._draggable._newPos.x=a},_onDragEnd:function(t){var e=this._map,i=e.options,n=+new Date-this._lastTime,s=!i.inertia||n>i.inertiaThreshold||!this._positions[0];if(e.fire("dragend",t),s)e.fire("moveend");else{var a=this._lastPos.subtract(this._positions[0]),r=(this._lastTime+n-this._times[0])/1e3,h=i.easeLinearity,l=a.multiplyBy(h/r),u=l.distanceTo([0,0]),c=Math.min(i.inertiaMaxSpeed,u),d=l.multiplyBy(c/u),p=c/(i.inertiaDeceleration*h),_=d.multiplyBy(-p/2).round();_.x&&_.y?(_=e._limitOffset(_,e.options.maxBounds),o.Util.requestAnimFrame(function(){e.panBy(_,{duration:p,easeLinearity:h,noMoveStart:!0})})):e.fire("moveend")}}}),o.Map.addInitHook("addHandler","dragging",o.Map.Drag),o.Map.mergeOptions({doubleClickZoom:!0}),o.Map.DoubleClickZoom=o.Handler.extend({addHooks:function(){this._map.on("dblclick",this._onDoubleClick,this)},removeHooks:function(){this._map.off("dblclick",this._onDoubleClick,this)},_onDoubleClick:function(t){var e=this._map,i=e.getZoom()+(t.originalEvent.shiftKey?-1:1);"center"===e.options.doubleClickZoom?e.setZoom(i):e.setZoomAround(t.containerPoint,i)}}),o.Map.addInitHook("addHandler","doubleClickZoom",o.Map.DoubleClickZoom),o.Map.mergeOptions({scrollWheelZoom:!0}),o.Map.ScrollWheelZoom=o.Handler.extend({addHooks:function(){o.DomEvent.on(this._map._container,"mousewheel",this._onWheelScroll,this),o.DomEvent.on(this._map._container,"MozMousePixelScroll",o.DomEvent.preventDefault),this._delta=0},removeHooks:function(){o.DomEvent.off(this._map._container,"mousewheel",this._onWheelScroll),o.DomEvent.off(this._map._container,"MozMousePixelScroll",o.DomEvent.preventDefault)},_onWheelScroll:function(t){var e=o.DomEvent.getWheelDelta(t);this._delta+=e,this._lastMousePos=this._map.mouseEventToContainerPoint(t),this._startTime||(this._startTime=+new Date);var i=Math.max(40-(+new Date-this._startTime),0);clearTimeout(this._timer),this._timer=setTimeout(o.bind(this._performZoom,this),i),o.DomEvent.preventDefault(t),o.DomEvent.stopPropagation(t)},_performZoom:function(){var t=this._map,e=this._delta,i=t.getZoom();e=e>0?Math.ceil(e):Math.floor(e),e=Math.max(Math.min(e,4),-4),e=t._limitZoom(i+e)-i,this._delta=0,this._startTime=null,e&&("center"===t.options.scrollWheelZoom?t.setZoom(i+e):t.setZoomAround(this._lastMousePos,i+e))}}),o.Map.addInitHook("addHandler","scrollWheelZoom",o.Map.ScrollWheelZoom),o.extend(o.DomEvent,{_touchstart:o.Browser.msPointer?"MSPointerDown":o.Browser.pointer?"pointerdown":"touchstart",_touchend:o.Browser.msPointer?"MSPointerUp":o.Browser.pointer?"pointerup":"touchend",addDoubleTapListener:function(t,i,n){function s(t){var e;if(o.Browser.pointer?(_.push(t.pointerId),e=_.length):e=t.touches.length,!(e>1)){var i=Date.now(),n=i-(r||i);h=t.touches?t.touches[0]:t,l=n>0&&u>=n,r=i}}function a(t){if(o.Browser.pointer){var e=_.indexOf(t.pointerId);if(-1===e)return;_.splice(e,1)}if(l){if(o.Browser.pointer){var n,s={};for(var a in h)n=h[a],s[a]="function"==typeof n?n.bind(h):n;h=s}h.type="dblclick",i(h),r=null}}var r,h,l=!1,u=250,c="_leaflet_",d=this._touchstart,p=this._touchend,_=[];t[c+d+n]=s,t[c+p+n]=a;var m=o.Browser.pointer?e.documentElement:t;return t.addEventListener(d,s,!1),m.addEventListener(p,a,!1),o.Browser.pointer&&m.addEventListener(o.DomEvent.POINTER_CANCEL,a,!1),this},removeDoubleTapListener:function(t,i){var n="_leaflet_";return t.removeEventListener(this._touchstart,t[n+this._touchstart+i],!1),(o.Browser.pointer?e.documentElement:t).removeEventListener(this._touchend,t[n+this._touchend+i],!1),o.Browser.pointer&&e.documentElement.removeEventListener(o.DomEvent.POINTER_CANCEL,t[n+this._touchend+i],!1),this}}),o.extend(o.DomEvent,{POINTER_DOWN:o.Browser.msPointer?"MSPointerDown":"pointerdown",POINTER_MOVE:o.Browser.msPointer?"MSPointerMove":"pointermove",POINTER_UP:o.Browser.msPointer?"MSPointerUp":"pointerup",POINTER_CANCEL:o.Browser.msPointer?"MSPointerCancel":"pointercancel",_pointers:[],_pointerDocumentListener:!1,addPointerListener:function(t,e,i,n){switch(e){case"touchstart":return this.addPointerListenerStart(t,e,i,n);case"touchend":return this.addPointerListenerEnd(t,e,i,n);case"touchmove":return this.addPointerListenerMove(t,e,i,n);default:throw"Unknown touch event type"}},addPointerListenerStart:function(t,i,n,s){var a="_leaflet_",r=this._pointers,h=function(t){o.DomEvent.preventDefault(t);for(var e=!1,i=0;i<r.length;i++)if(r[i].pointerId===t.pointerId){e=!0;
break}e||r.push(t),t.touches=r.slice(),t.changedTouches=[t],n(t)};if(t[a+"touchstart"+s]=h,t.addEventListener(this.POINTER_DOWN,h,!1),!this._pointerDocumentListener){var l=function(t){for(var e=0;e<r.length;e++)if(r[e].pointerId===t.pointerId){r.splice(e,1);break}};e.documentElement.addEventListener(this.POINTER_UP,l,!1),e.documentElement.addEventListener(this.POINTER_CANCEL,l,!1),this._pointerDocumentListener=!0}return this},addPointerListenerMove:function(t,e,i,n){function o(t){if(t.pointerType!==t.MSPOINTER_TYPE_MOUSE&&"mouse"!==t.pointerType||0!==t.buttons){for(var e=0;e<a.length;e++)if(a[e].pointerId===t.pointerId){a[e]=t;break}t.touches=a.slice(),t.changedTouches=[t],i(t)}}var s="_leaflet_",a=this._pointers;return t[s+"touchmove"+n]=o,t.addEventListener(this.POINTER_MOVE,o,!1),this},addPointerListenerEnd:function(t,e,i,n){var o="_leaflet_",s=this._pointers,a=function(t){for(var e=0;e<s.length;e++)if(s[e].pointerId===t.pointerId){s.splice(e,1);break}t.touches=s.slice(),t.changedTouches=[t],i(t)};return t[o+"touchend"+n]=a,t.addEventListener(this.POINTER_UP,a,!1),t.addEventListener(this.POINTER_CANCEL,a,!1),this},removePointerListener:function(t,e,i){var n="_leaflet_",o=t[n+e+i];switch(e){case"touchstart":t.removeEventListener(this.POINTER_DOWN,o,!1);break;case"touchmove":t.removeEventListener(this.POINTER_MOVE,o,!1);break;case"touchend":t.removeEventListener(this.POINTER_UP,o,!1),t.removeEventListener(this.POINTER_CANCEL,o,!1)}return this}}),o.Map.mergeOptions({touchZoom:o.Browser.touch&&!o.Browser.android23,bounceAtZoomLimits:!0}),o.Map.TouchZoom=o.Handler.extend({addHooks:function(){o.DomEvent.on(this._map._container,"touchstart",this._onTouchStart,this)},removeHooks:function(){o.DomEvent.off(this._map._container,"touchstart",this._onTouchStart,this)},_onTouchStart:function(t){var i=this._map;if(t.touches&&2===t.touches.length&&!i._animatingZoom&&!this._zooming){var n=i.mouseEventToLayerPoint(t.touches[0]),s=i.mouseEventToLayerPoint(t.touches[1]),a=i._getCenterLayerPoint();this._startCenter=n.add(s)._divideBy(2),this._startDist=n.distanceTo(s),this._moved=!1,this._zooming=!0,this._centerOffset=a.subtract(this._startCenter),i._panAnim&&i._panAnim.stop(),o.DomEvent.on(e,"touchmove",this._onTouchMove,this).on(e,"touchend",this._onTouchEnd,this),o.DomEvent.preventDefault(t)}},_onTouchMove:function(t){var e=this._map;if(t.touches&&2===t.touches.length&&this._zooming){var i=e.mouseEventToLayerPoint(t.touches[0]),n=e.mouseEventToLayerPoint(t.touches[1]);this._scale=i.distanceTo(n)/this._startDist,this._delta=i._add(n)._divideBy(2)._subtract(this._startCenter),1!==this._scale&&(e.options.bounceAtZoomLimits||!(e.getZoom()===e.getMinZoom()&&this._scale<1||e.getZoom()===e.getMaxZoom()&&this._scale>1))&&(this._moved||(o.DomUtil.addClass(e._mapPane,"leaflet-touching"),e.fire("movestart").fire("zoomstart"),this._moved=!0),o.Util.cancelAnimFrame(this._animRequest),this._animRequest=o.Util.requestAnimFrame(this._updateOnMove,this,!0,this._map._container),o.DomEvent.preventDefault(t))}},_updateOnMove:function(){var t=this._map,e=this._getScaleOrigin(),i=t.layerPointToLatLng(e),n=t.getScaleZoom(this._scale);t._animateZoom(i,n,this._startCenter,this._scale,this._delta,!1,!0)},_onTouchEnd:function(){if(!this._moved||!this._zooming)return void(this._zooming=!1);var t=this._map;this._zooming=!1,o.DomUtil.removeClass(t._mapPane,"leaflet-touching"),o.Util.cancelAnimFrame(this._animRequest),o.DomEvent.off(e,"touchmove",this._onTouchMove).off(e,"touchend",this._onTouchEnd);var i=this._getScaleOrigin(),n=t.layerPointToLatLng(i),s=t.getZoom(),a=t.getScaleZoom(this._scale)-s,r=a>0?Math.ceil(a):Math.floor(a),h=t._limitZoom(s+r),l=t.getZoomScale(h)/this._scale;t._animateZoom(n,h,i,l)},_getScaleOrigin:function(){var t=this._centerOffset.subtract(this._delta).divideBy(this._scale);return this._startCenter.add(t)}}),o.Map.addInitHook("addHandler","touchZoom",o.Map.TouchZoom),o.Map.mergeOptions({tap:!0,tapTolerance:15}),o.Map.Tap=o.Handler.extend({addHooks:function(){o.DomEvent.on(this._map._container,"touchstart",this._onDown,this)},removeHooks:function(){o.DomEvent.off(this._map._container,"touchstart",this._onDown,this)},_onDown:function(t){if(t.touches){if(o.DomEvent.preventDefault(t),this._fireClick=!0,t.touches.length>1)return this._fireClick=!1,void clearTimeout(this._holdTimeout);var i=t.touches[0],n=i.target;this._startPos=this._newPos=new o.Point(i.clientX,i.clientY),n.tagName&&"a"===n.tagName.toLowerCase()&&o.DomUtil.addClass(n,"leaflet-active"),this._holdTimeout=setTimeout(o.bind(function(){this._isTapValid()&&(this._fireClick=!1,this._onUp(),this._simulateEvent("contextmenu",i))},this),1e3),o.DomEvent.on(e,"touchmove",this._onMove,this).on(e,"touchend",this._onUp,this)}},_onUp:function(t){if(clearTimeout(this._holdTimeout),o.DomEvent.off(e,"touchmove",this._onMove,this).off(e,"touchend",this._onUp,this),this._fireClick&&t&&t.changedTouches){var i=t.changedTouches[0],n=i.target;n&&n.tagName&&"a"===n.tagName.toLowerCase()&&o.DomUtil.removeClass(n,"leaflet-active"),this._isTapValid()&&this._simulateEvent("click",i)}},_isTapValid:function(){return this._newPos.distanceTo(this._startPos)<=this._map.options.tapTolerance},_onMove:function(t){var e=t.touches[0];this._newPos=new o.Point(e.clientX,e.clientY)},_simulateEvent:function(i,n){var o=e.createEvent("MouseEvents");o._simulated=!0,n.target._simulatedClick=!0,o.initMouseEvent(i,!0,!0,t,1,n.screenX,n.screenY,n.clientX,n.clientY,!1,!1,!1,!1,0,null),n.target.dispatchEvent(o)}}),o.Browser.touch&&!o.Browser.pointer&&o.Map.addInitHook("addHandler","tap",o.Map.Tap),o.Map.mergeOptions({boxZoom:!0}),o.Map.BoxZoom=o.Handler.extend({initialize:function(t){this._map=t,this._container=t._container,this._pane=t._panes.overlayPane,this._moved=!1},addHooks:function(){o.DomEvent.on(this._container,"mousedown",this._onMouseDown,this)},removeHooks:function(){o.DomEvent.off(this._container,"mousedown",this._onMouseDown),this._moved=!1},moved:function(){return this._moved},_onMouseDown:function(t){return this._moved=!1,!t.shiftKey||1!==t.which&&1!==t.button?!1:(o.DomUtil.disableTextSelection(),o.DomUtil.disableImageDrag(),this._startLayerPoint=this._map.mouseEventToLayerPoint(t),void o.DomEvent.on(e,"mousemove",this._onMouseMove,this).on(e,"mouseup",this._onMouseUp,this).on(e,"keydown",this._onKeyDown,this))},_onMouseMove:function(t){this._moved||(this._box=o.DomUtil.create("div","leaflet-zoom-box",this._pane),o.DomUtil.setPosition(this._box,this._startLayerPoint),this._container.style.cursor="crosshair",this._map.fire("boxzoomstart"));var e=this._startLayerPoint,i=this._box,n=this._map.mouseEventToLayerPoint(t),s=n.subtract(e),a=new o.Point(Math.min(n.x,e.x),Math.min(n.y,e.y));o.DomUtil.setPosition(i,a),this._moved=!0,i.style.width=Math.max(0,Math.abs(s.x)-4)+"px",i.style.height=Math.max(0,Math.abs(s.y)-4)+"px"},_finish:function(){this._moved&&(this._pane.removeChild(this._box),this._container.style.cursor=""),o.DomUtil.enableTextSelection(),o.DomUtil.enableImageDrag(),o.DomEvent.off(e,"mousemove",this._onMouseMove).off(e,"mouseup",this._onMouseUp).off(e,"keydown",this._onKeyDown)},_onMouseUp:function(t){this._finish();var e=this._map,i=e.mouseEventToLayerPoint(t);if(!this._startLayerPoint.equals(i)){var n=new o.LatLngBounds(e.layerPointToLatLng(this._startLayerPoint),e.layerPointToLatLng(i));e.fitBounds(n),e.fire("boxzoomend",{boxZoomBounds:n})}},_onKeyDown:function(t){27===t.keyCode&&this._finish()}}),o.Map.addInitHook("addHandler","boxZoom",o.Map.BoxZoom),o.Map.mergeOptions({keyboard:!0,keyboardPanOffset:80,keyboardZoomOffset:1}),o.Map.Keyboard=o.Handler.extend({keyCodes:{left:[37],right:[39],down:[40],up:[38],zoomIn:[187,107,61,171],zoomOut:[189,109,173]},initialize:function(t){this._map=t,this._setPanOffset(t.options.keyboardPanOffset),this._setZoomOffset(t.options.keyboardZoomOffset)},addHooks:function(){var t=this._map._container;-1===t.tabIndex&&(t.tabIndex="0"),o.DomEvent.on(t,"focus",this._onFocus,this).on(t,"blur",this._onBlur,this).on(t,"mousedown",this._onMouseDown,this),this._map.on("focus",this._addHooks,this).on("blur",this._removeHooks,this)},removeHooks:function(){this._removeHooks();var t=this._map._container;o.DomEvent.off(t,"focus",this._onFocus,this).off(t,"blur",this._onBlur,this).off(t,"mousedown",this._onMouseDown,this),this._map.off("focus",this._addHooks,this).off("blur",this._removeHooks,this)},_onMouseDown:function(){if(!this._focused){var i=e.body,n=e.documentElement,o=i.scrollTop||n.scrollTop,s=i.scrollLeft||n.scrollLeft;this._map._container.focus(),t.scrollTo(s,o)}},_onFocus:function(){this._focused=!0,this._map.fire("focus")},_onBlur:function(){this._focused=!1,this._map.fire("blur")},_setPanOffset:function(t){var e,i,n=this._panKeys={},o=this.keyCodes;for(e=0,i=o.left.length;i>e;e++)n[o.left[e]]=[-1*t,0];for(e=0,i=o.right.length;i>e;e++)n[o.right[e]]=[t,0];for(e=0,i=o.down.length;i>e;e++)n[o.down[e]]=[0,t];for(e=0,i=o.up.length;i>e;e++)n[o.up[e]]=[0,-1*t]},_setZoomOffset:function(t){var e,i,n=this._zoomKeys={},o=this.keyCodes;for(e=0,i=o.zoomIn.length;i>e;e++)n[o.zoomIn[e]]=t;for(e=0,i=o.zoomOut.length;i>e;e++)n[o.zoomOut[e]]=-t},_addHooks:function(){o.DomEvent.on(e,"keydown",this._onKeyDown,this)},_removeHooks:function(){o.DomEvent.off(e,"keydown",this._onKeyDown,this)},_onKeyDown:function(t){var e=t.keyCode,i=this._map;if(e in this._panKeys){if(i._panAnim&&i._panAnim._inProgress)return;i.panBy(this._panKeys[e]),i.options.maxBounds&&i.panInsideBounds(i.options.maxBounds)}else{if(!(e in this._zoomKeys))return;i.setZoom(i.getZoom()+this._zoomKeys[e])}o.DomEvent.stop(t)}}),o.Map.addInitHook("addHandler","keyboard",o.Map.Keyboard),o.Handler.MarkerDrag=o.Handler.extend({initialize:function(t){this._marker=t},addHooks:function(){var t=this._marker._icon;this._draggable||(this._draggable=new o.Draggable(t,t)),this._draggable.on("dragstart",this._onDragStart,this).on("drag",this._onDrag,this).on("dragend",this._onDragEnd,this),this._draggable.enable(),o.DomUtil.addClass(this._marker._icon,"leaflet-marker-draggable")},removeHooks:function(){this._draggable.off("dragstart",this._onDragStart,this).off("drag",this._onDrag,this).off("dragend",this._onDragEnd,this),this._draggable.disable(),o.DomUtil.removeClass(this._marker._icon,"leaflet-marker-draggable")},moved:function(){return this._draggable&&this._draggable._moved},_onDragStart:function(){this._marker.closePopup().fire("movestart").fire("dragstart")},_onDrag:function(){var t=this._marker,e=t._shadow,i=o.DomUtil.getPosition(t._icon),n=t._map.layerPointToLatLng(i);e&&o.DomUtil.setPosition(e,i),t._latlng=n,t.fire("move",{latlng:n}).fire("drag")},_onDragEnd:function(t){this._marker.fire("moveend").fire("dragend",t)}}),o.Control=o.Class.extend({options:{position:"topright"},initialize:function(t){o.setOptions(this,t)},getPosition:function(){return this.options.position},setPosition:function(t){var e=this._map;return e&&e.removeControl(this),this.options.position=t,e&&e.addControl(this),this},getContainer:function(){return this._container},addTo:function(t){this._map=t;var e=this._container=this.onAdd(t),i=this.getPosition(),n=t._controlCorners[i];return o.DomUtil.addClass(e,"leaflet-control"),-1!==i.indexOf("bottom")?n.insertBefore(e,n.firstChild):n.appendChild(e),this},removeFrom:function(t){var e=this.getPosition(),i=t._controlCorners[e];return i.removeChild(this._container),this._map=null,this.onRemove&&this.onRemove(t),this},_refocusOnMap:function(){this._map&&this._map.getContainer().focus()}}),o.control=function(t){return new o.Control(t)},o.Map.include({addControl:function(t){return t.addTo(this),this},removeControl:function(t){return t.removeFrom(this),this},_initControlPos:function(){function t(t,s){var a=i+t+" "+i+s;e[t+s]=o.DomUtil.create("div",a,n)}var e=this._controlCorners={},i="leaflet-",n=this._controlContainer=o.DomUtil.create("div",i+"control-container",this._container);t("top","left"),t("top","right"),t("bottom","left"),t("bottom","right")},_clearControlPos:function(){this._container.removeChild(this._controlContainer)}}),o.Control.Zoom=o.Control.extend({options:{position:"topleft",zoomInText:"+",zoomInTitle:"Zoom in",zoomOutText:"-",zoomOutTitle:"Zoom out"},onAdd:function(t){var e="leaflet-control-zoom",i=o.DomUtil.create("div",e+" leaflet-bar");return this._map=t,this._zoomInButton=this._createButton(this.options.zoomInText,this.options.zoomInTitle,e+"-in",i,this._zoomIn,this),this._zoomOutButton=this._createButton(this.options.zoomOutText,this.options.zoomOutTitle,e+"-out",i,this._zoomOut,this),this._updateDisabled(),t.on("zoomend zoomlevelschange",this._updateDisabled,this),i},onRemove:function(t){t.off("zoomend zoomlevelschange",this._updateDisabled,this)},_zoomIn:function(t){this._map.zoomIn(t.shiftKey?3:1)},_zoomOut:function(t){this._map.zoomOut(t.shiftKey?3:1)},_createButton:function(t,e,i,n,s,a){var r=o.DomUtil.create("a",i,n);r.innerHTML=t,r.href="#",r.title=e;var h=o.DomEvent.stopPropagation;return o.DomEvent.on(r,"click",h).on(r,"mousedown",h).on(r,"dblclick",h).on(r,"click",o.DomEvent.preventDefault).on(r,"click",s,a).on(r,"click",this._refocusOnMap,a),r},_updateDisabled:function(){var t=this._map,e="leaflet-disabled";o.DomUtil.removeClass(this._zoomInButton,e),o.DomUtil.removeClass(this._zoomOutButton,e),t._zoom===t.getMinZoom()&&o.DomUtil.addClass(this._zoomOutButton,e),t._zoom===t.getMaxZoom()&&o.DomUtil.addClass(this._zoomInButton,e)}}),o.Map.mergeOptions({zoomControl:!0}),o.Map.addInitHook(function(){this.options.zoomControl&&(this.zoomControl=new o.Control.Zoom,this.addControl(this.zoomControl))}),o.control.zoom=function(t){return new o.Control.Zoom(t)},o.Control.Attribution=o.Control.extend({options:{position:"bottomright",prefix:'<a href="http://leafletjs.com" title="A JS library for interactive maps">Leaflet</a>'},initialize:function(t){o.setOptions(this,t),this._attributions={}},onAdd:function(t){this._container=o.DomUtil.create("div","leaflet-control-attribution"),o.DomEvent.disableClickPropagation(this._container);for(var e in t._layers)t._layers[e].getAttribution&&this.addAttribution(t._layers[e].getAttribution());return t.on("layeradd",this._onLayerAdd,this).on("layerremove",this._onLayerRemove,this),this._update(),this._container},onRemove:function(t){t.off("layeradd",this._onLayerAdd).off("layerremove",this._onLayerRemove)},setPrefix:function(t){return this.options.prefix=t,this._update(),this},addAttribution:function(t){return t?(this._attributions[t]||(this._attributions[t]=0),this._attributions[t]++,this._update(),this):void 0},removeAttribution:function(t){return t?(this._attributions[t]&&(this._attributions[t]--,this._update()),this):void 0},_update:function(){if(this._map){var t=[];for(var e in this._attributions)this._attributions[e]&&t.push(e);var i=[];this.options.prefix&&i.push(this.options.prefix),t.length&&i.push(t.join(", ")),this._container.innerHTML=i.join(" | ")}},_onLayerAdd:function(t){t.layer.getAttribution&&this.addAttribution(t.layer.getAttribution())},_onLayerRemove:function(t){t.layer.getAttribution&&this.removeAttribution(t.layer.getAttribution())}}),o.Map.mergeOptions({attributionControl:!0}),o.Map.addInitHook(function(){this.options.attributionControl&&(this.attributionControl=(new o.Control.Attribution).addTo(this))}),o.control.attribution=function(t){return new o.Control.Attribution(t)},o.Control.Scale=o.Control.extend({options:{position:"bottomleft",maxWidth:100,metric:!0,imperial:!0,updateWhenIdle:!1},onAdd:function(t){this._map=t;var e="leaflet-control-scale",i=o.DomUtil.create("div",e),n=this.options;return this._addScales(n,e,i),t.on(n.updateWhenIdle?"moveend":"move",this._update,this),t.whenReady(this._update,this),i},onRemove:function(t){t.off(this.options.updateWhenIdle?"moveend":"move",this._update,this)},_addScales:function(t,e,i){t.metric&&(this._mScale=o.DomUtil.create("div",e+"-line",i)),t.imperial&&(this._iScale=o.DomUtil.create("div",e+"-line",i))},_update:function(){var t=this._map.getBounds(),e=t.getCenter().lat,i=6378137*Math.PI*Math.cos(e*Math.PI/180),n=i*(t.getNorthEast().lng-t.getSouthWest().lng)/180,o=this._map.getSize(),s=this.options,a=0;o.x>0&&(a=n*(s.maxWidth/o.x)),this._updateScales(s,a)},_updateScales:function(t,e){t.metric&&e&&this._updateMetric(e),t.imperial&&e&&this._updateImperial(e)},_updateMetric:function(t){var e=this._getRoundNum(t);this._mScale.style.width=this._getScaleWidth(e/t)+"px",this._mScale.innerHTML=1e3>e?e+" m":e/1e3+" km"},_updateImperial:function(t){var e,i,n,o=3.2808399*t,s=this._iScale;o>5280?(e=o/5280,i=this._getRoundNum(e),s.style.width=this._getScaleWidth(i/e)+"px",s.innerHTML=i+" mi"):(n=this._getRoundNum(o),s.style.width=this._getScaleWidth(n/o)+"px",s.innerHTML=n+" ft")},_getScaleWidth:function(t){return Math.round(this.options.maxWidth*t)-10},_getRoundNum:function(t){var e=Math.pow(10,(Math.floor(t)+"").length-1),i=t/e;return i=i>=10?10:i>=5?5:i>=3?3:i>=2?2:1,e*i}}),o.control.scale=function(t){return new o.Control.Scale(t)},o.Control.Layers=o.Control.extend({options:{collapsed:!0,position:"topright",autoZIndex:!0},initialize:function(t,e,i){o.setOptions(this,i),this._layers={},this._lastZIndex=0,this._handlingClick=!1;for(var n in t)this._addLayer(t[n],n);for(n in e)this._addLayer(e[n],n,!0)},onAdd:function(t){return this._initLayout(),this._update(),t.on("layeradd",this._onLayerChange,this).on("layerremove",this._onLayerChange,this),this._container},onRemove:function(t){t.off("layeradd",this._onLayerChange,this).off("layerremove",this._onLayerChange,this)},addBaseLayer:function(t,e){return this._addLayer(t,e),this._update(),this},addOverlay:function(t,e){return this._addLayer(t,e,!0),this._update(),this},removeLayer:function(t){var e=o.stamp(t);return delete this._layers[e],this._update(),this},_initLayout:function(){var t="leaflet-control-layers",e=this._container=o.DomUtil.create("div",t);e.setAttribute("aria-haspopup",!0),o.Browser.touch?o.DomEvent.on(e,"click",o.DomEvent.stopPropagation):o.DomEvent.disableClickPropagation(e).disableScrollPropagation(e);var i=this._form=o.DomUtil.create("form",t+"-list");if(this.options.collapsed){o.Browser.android||o.DomEvent.on(e,"mouseover",this._expand,this).on(e,"mouseout",this._collapse,this);var n=this._layersLink=o.DomUtil.create("a",t+"-toggle",e);n.href="#",n.title="Layers",o.Browser.touch?o.DomEvent.on(n,"click",o.DomEvent.stop).on(n,"click",this._expand,this):o.DomEvent.on(n,"focus",this._expand,this),o.DomEvent.on(i,"click",function(){setTimeout(o.bind(this._onInputClick,this),0)},this),this._map.on("click",this._collapse,this)}else this._expand();this._baseLayersList=o.DomUtil.create("div",t+"-base",i),this._separator=o.DomUtil.create("div",t+"-separator",i),this._overlaysList=o.DomUtil.create("div",t+"-overlays",i),e.appendChild(i)},_addLayer:function(t,e,i){var n=o.stamp(t);this._layers[n]={layer:t,name:e,overlay:i},this.options.autoZIndex&&t.setZIndex&&(this._lastZIndex++,t.setZIndex(this._lastZIndex))},_update:function(){if(this._container){this._baseLayersList.innerHTML="",this._overlaysList.innerHTML="";var t,e,i=!1,n=!1;for(t in this._layers)e=this._layers[t],this._addItem(e),n=n||e.overlay,i=i||!e.overlay;this._separator.style.display=n&&i?"":"none"}},_onLayerChange:function(t){var e=this._layers[o.stamp(t.layer)];if(e){this._handlingClick||this._update();var i=e.overlay?"layeradd"===t.type?"overlayadd":"overlayremove":"layeradd"===t.type?"baselayerchange":null;i&&this._map.fire(i,e)}},_createRadioElement:function(t,i){var n='<input type="radio" class="leaflet-control-layers-selector" name="'+t+'"';i&&(n+=' checked="checked"'),n+="/>";var o=e.createElement("div");return o.innerHTML=n,o.firstChild},_addItem:function(t){var i,n=e.createElement("label"),s=this._map.hasLayer(t.layer);t.overlay?(i=e.createElement("input"),i.type="checkbox",i.className="leaflet-control-layers-selector",i.defaultChecked=s):i=this._createRadioElement("leaflet-base-layers",s),i.layerId=o.stamp(t.layer),o.DomEvent.on(i,"click",this._onInputClick,this);var a=e.createElement("span");a.innerHTML=" "+t.name,n.appendChild(i),n.appendChild(a);var r=t.overlay?this._overlaysList:this._baseLayersList;return r.appendChild(n),n},_onInputClick:function(){var t,e,i,n=this._form.getElementsByTagName("input"),o=n.length;for(this._handlingClick=!0,t=0;o>t;t++)e=n[t],i=this._layers[e.layerId],e.checked&&!this._map.hasLayer(i.layer)?this._map.addLayer(i.layer):!e.checked&&this._map.hasLayer(i.layer)&&this._map.removeLayer(i.layer);this._handlingClick=!1,this._refocusOnMap()},_expand:function(){o.DomUtil.addClass(this._container,"leaflet-control-layers-expanded")},_collapse:function(){this._container.className=this._container.className.replace(" leaflet-control-layers-expanded","")}}),o.control.layers=function(t,e,i){return new o.Control.Layers(t,e,i)},o.PosAnimation=o.Class.extend({includes:o.Mixin.Events,run:function(t,e,i,n){this.stop(),this._el=t,this._inProgress=!0,this._newPos=e,this.fire("start"),t.style[o.DomUtil.TRANSITION]="all "+(i||.25)+"s cubic-bezier(0,0,"+(n||.5)+",1)",o.DomEvent.on(t,o.DomUtil.TRANSITION_END,this._onTransitionEnd,this),o.DomUtil.setPosition(t,e),o.Util.falseFn(t.offsetWidth),this._stepTimer=setInterval(o.bind(this._onStep,this),50)},stop:function(){this._inProgress&&(o.DomUtil.setPosition(this._el,this._getPos()),this._onTransitionEnd(),o.Util.falseFn(this._el.offsetWidth))},_onStep:function(){var t=this._getPos();return t?(this._el._leaflet_pos=t,void this.fire("step")):void this._onTransitionEnd()},_transformRe:/([-+]?(?:\d*\.)?\d+)\D*, ([-+]?(?:\d*\.)?\d+)\D*\)/,_getPos:function(){var e,i,n,s=this._el,a=t.getComputedStyle(s);if(o.Browser.any3d){if(n=a[o.DomUtil.TRANSFORM].match(this._transformRe),!n)return;e=parseFloat(n[1]),i=parseFloat(n[2])}else e=parseFloat(a.left),i=parseFloat(a.top);return new o.Point(e,i,!0)},_onTransitionEnd:function(){o.DomEvent.off(this._el,o.DomUtil.TRANSITION_END,this._onTransitionEnd,this),this._inProgress&&(this._inProgress=!1,this._el.style[o.DomUtil.TRANSITION]="",this._el._leaflet_pos=this._newPos,clearInterval(this._stepTimer),this.fire("step").fire("end"))}}),o.Map.include({setView:function(t,e,n){if(e=e===i?this._zoom:this._limitZoom(e),t=this._limitCenter(o.latLng(t),e,this.options.maxBounds),n=n||{},this._panAnim&&this._panAnim.stop(),this._loaded&&!n.reset&&n!==!0){n.animate!==i&&(n.zoom=o.extend({animate:n.animate},n.zoom),n.pan=o.extend({animate:n.animate},n.pan));var s=this._zoom!==e?this._tryAnimatedZoom&&this._tryAnimatedZoom(t,e,n.zoom):this._tryAnimatedPan(t,n.pan);if(s)return clearTimeout(this._sizeTimer),this}return this._resetView(t,e),this},panBy:function(t,e){if(t=o.point(t).round(),e=e||{},!t.x&&!t.y)return this;if(this._panAnim||(this._panAnim=new o.PosAnimation,this._panAnim.on({step:this._onPanTransitionStep,end:this._onPanTransitionEnd},this)),e.noMoveStart||this.fire("movestart"),e.animate!==!1){o.DomUtil.addClass(this._mapPane,"leaflet-pan-anim");var i=this._getMapPanePos().subtract(t);this._panAnim.run(this._mapPane,i,e.duration||.25,e.easeLinearity)}else this._rawPanBy(t),this.fire("move").fire("moveend");return this},_onPanTransitionStep:function(){this.fire("move")},_onPanTransitionEnd:function(){o.DomUtil.removeClass(this._mapPane,"leaflet-pan-anim"),this.fire("moveend")},_tryAnimatedPan:function(t,e){var i=this._getCenterOffset(t)._floor();return(e&&e.animate)===!0||this.getSize().contains(i)?(this.panBy(i,e),!0):!1}}),o.PosAnimation=o.DomUtil.TRANSITION?o.PosAnimation:o.PosAnimation.extend({run:function(t,e,i,n){this.stop(),this._el=t,this._inProgress=!0,this._duration=i||.25,this._easeOutPower=1/Math.max(n||.5,.2),this._startPos=o.DomUtil.getPosition(t),this._offset=e.subtract(this._startPos),this._startTime=+new Date,this.fire("start"),this._animate()},stop:function(){this._inProgress&&(this._step(),this._complete())},_animate:function(){this._animId=o.Util.requestAnimFrame(this._animate,this),this._step()},_step:function(){var t=+new Date-this._startTime,e=1e3*this._duration;e>t?this._runFrame(this._easeOut(t/e)):(this._runFrame(1),this._complete())},_runFrame:function(t){var e=this._startPos.add(this._offset.multiplyBy(t));o.DomUtil.setPosition(this._el,e),this.fire("step")},_complete:function(){o.Util.cancelAnimFrame(this._animId),this._inProgress=!1,this.fire("end")},_easeOut:function(t){return 1-Math.pow(1-t,this._easeOutPower)}}),o.Map.mergeOptions({zoomAnimation:!0,zoomAnimationThreshold:4}),o.DomUtil.TRANSITION&&o.Map.addInitHook(function(){this._zoomAnimated=this.options.zoomAnimation&&o.DomUtil.TRANSITION&&o.Browser.any3d&&!o.Browser.android23&&!o.Browser.mobileOpera,this._zoomAnimated&&o.DomEvent.on(this._mapPane,o.DomUtil.TRANSITION_END,this._catchTransitionEnd,this)}),o.Map.include(o.DomUtil.TRANSITION?{_catchTransitionEnd:function(t){this._animatingZoom&&t.propertyName.indexOf("transform")>=0&&this._onZoomTransitionEnd()},_nothingToAnimate:function(){return!this._container.getElementsByClassName("leaflet-zoom-animated").length},_tryAnimatedZoom:function(t,e,i){if(this._animatingZoom)return!0;if(i=i||{},!this._zoomAnimated||i.animate===!1||this._nothingToAnimate()||Math.abs(e-this._zoom)>this.options.zoomAnimationThreshold)return!1;var n=this.getZoomScale(e),o=this._getCenterOffset(t)._divideBy(1-1/n),s=this._getCenterLayerPoint()._add(o);return i.animate===!0||this.getSize().contains(o)?(this.fire("movestart").fire("zoomstart"),this._animateZoom(t,e,s,n,null,!0),!0):!1},_animateZoom:function(t,e,i,n,s,a,r){r||(this._animatingZoom=!0),o.DomUtil.addClass(this._mapPane,"leaflet-zoom-anim"),this._animateToCenter=t,this._animateToZoom=e,o.Draggable&&(o.Draggable._disabled=!0),o.Util.requestAnimFrame(function(){this.fire("zoomanim",{center:t,zoom:e,origin:i,scale:n,delta:s,backwards:a})},this)},_onZoomTransitionEnd:function(){this._animatingZoom=!1,o.DomUtil.removeClass(this._mapPane,"leaflet-zoom-anim"),this._resetView(this._animateToCenter,this._animateToZoom,!0,!0),o.Draggable&&(o.Draggable._disabled=!1)}}:{}),o.TileLayer.include({_animateZoom:function(t){this._animating||(this._animating=!0,this._prepareBgBuffer());var e=this._bgBuffer,i=o.DomUtil.TRANSFORM,n=t.delta?o.DomUtil.getTranslateString(t.delta):e.style[i],s=o.DomUtil.getScaleString(t.scale,t.origin);e.style[i]=t.backwards?s+" "+n:n+" "+s},_endZoomAnim:function(){var t=this._tileContainer,e=this._bgBuffer;t.style.visibility="",t.parentNode.appendChild(t),o.Util.falseFn(e.offsetWidth),this._animating=!1},_clearBgBuffer:function(){var t=this._map;!t||t._animatingZoom||t.touchZoom._zooming||(this._bgBuffer.innerHTML="",this._bgBuffer.style[o.DomUtil.TRANSFORM]="")},_prepareBgBuffer:function(){var t=this._tileContainer,e=this._bgBuffer,i=this._getLoadedTilesPercentage(e),n=this._getLoadedTilesPercentage(t);return e&&i>.5&&.5>n?(t.style.visibility="hidden",void this._stopLoadingImages(t)):(e.style.visibility="hidden",e.style[o.DomUtil.TRANSFORM]="",this._tileContainer=e,e=this._bgBuffer=t,this._stopLoadingImages(e),void clearTimeout(this._clearBgBufferTimer))},_getLoadedTilesPercentage:function(t){var e,i,n=t.getElementsByTagName("img"),o=0;for(e=0,i=n.length;i>e;e++)n[e].complete&&o++;return o/i},_stopLoadingImages:function(t){var e,i,n,s=Array.prototype.slice.call(t.getElementsByTagName("img"));for(e=0,i=s.length;i>e;e++)n=s[e],n.complete||(n.onload=o.Util.falseFn,n.onerror=o.Util.falseFn,n.src=o.Util.emptyImageUrl,n.parentNode.removeChild(n))}}),o.Map.include({_defaultLocateOptions:{watch:!1,setView:!1,maxZoom:1/0,timeout:1e4,maximumAge:0,enableHighAccuracy:!1},locate:function(t){if(t=this._locateOptions=o.extend(this._defaultLocateOptions,t),!navigator.geolocation)return this._handleGeolocationError({code:0,message:"Geolocation not supported."}),this;var e=o.bind(this._handleGeolocationResponse,this),i=o.bind(this._handleGeolocationError,this);return t.watch?this._locationWatchId=navigator.geolocation.watchPosition(e,i,t):navigator.geolocation.getCurrentPosition(e,i,t),this},stopLocate:function(){return navigator.geolocation&&navigator.geolocation.clearWatch(this._locationWatchId),this._locateOptions&&(this._locateOptions.setView=!1),this},_handleGeolocationError:function(t){var e=t.code,i=t.message||(1===e?"permission denied":2===e?"position unavailable":"timeout");this._locateOptions.setView&&!this._loaded&&this.fitWorld(),this.fire("locationerror",{code:e,message:"Geolocation error: "+i+"."})},_handleGeolocationResponse:function(t){var e=t.coords.latitude,i=t.coords.longitude,n=new o.LatLng(e,i),s=180*t.coords.accuracy/40075017,a=s/Math.cos(o.LatLng.DEG_TO_RAD*e),r=o.latLngBounds([e-s,i-a],[e+s,i+a]),h=this._locateOptions;if(h.setView){var l=Math.min(this.getBoundsZoom(r),h.maxZoom);this.setView(n,l)}var u={latlng:n,bounds:r,timestamp:t.timestamp};for(var c in t.coords)"number"==typeof t.coords[c]&&(u[c]=t.coords[c]);this.fire("locationfound",u)}})}(window,document);

(function (factory) {
  //define an AMD module that relies on 'leaflet'
  if (typeof define === 'function' && define.amd) {
    define(['leaflet'], function (L) {
      return factory(L);
    });
  //define a common js module that relies on 'leaflet'
  } else if (typeof module === 'object' && typeof module.exports === 'object') {
    module.exports = factory(require('leaflet'));
  }

  if(typeof window !== 'undefined' && window.L){
    factory(window.L);
  }
}(function (L) {

var EsriLeaflet={VERSION:"1.0.0-rc.5",Layers:{},Services:{},Controls:{},Tasks:{},Util:{},Support:{CORS:!!(window.XMLHttpRequest&&"withCredentials"in new XMLHttpRequest),pointerEvents:""===document.documentElement.style.pointerEvents}};"undefined"!=typeof window&&window.L&&(window.L.esri=EsriLeaflet),function(a){function b(a){var b={};for(var c in a)a.hasOwnProperty(c)&&(b[c]=a[c]);return b}function c(a,b){for(var c=0;c<a.length;c++)if(a[c]!==b[c])return!1;return!0}function d(a){return c(a[0],a[a.length-1])||a.push(a[0]),a}function e(a){var b,c=0,d=0,e=a.length,f=a[d];for(d;e-1>d;d++)b=a[d+1],c+=(b[0]-f[0])*(b[1]+f[1]),f=b;return c>=0}function f(a,b,c,d){var e=(d[0]-c[0])*(a[1]-c[1])-(d[1]-c[1])*(a[0]-c[0]),f=(b[0]-a[0])*(a[1]-c[1])-(b[1]-a[1])*(a[0]-c[0]),g=(d[1]-c[1])*(b[0]-a[0])-(d[0]-c[0])*(b[1]-a[1]);if(0!==g){var h=e/g,i=f/g;if(h>=0&&1>=h&&i>=0&&1>=i)return!0}return!1}function g(a,b){for(var c=0;c<a.length-1;c++)for(var d=0;d<b.length-1;d++)if(f(a[c],a[c+1],b[d],b[d+1]))return!0;return!1}function h(a,b){for(var c=!1,d=-1,e=a.length,f=e-1;++d<e;f=d)(a[d][1]<=b[1]&&b[1]<a[f][1]||a[f][1]<=b[1]&&b[1]<a[d][1])&&b[0]<(a[f][0]-a[d][0])*(b[1]-a[d][1])/(a[f][1]-a[d][1])+a[d][0]&&(c=!c);return c}function i(a,b){var c=g(a,b),d=h(a,b[0]);return!c&&d?!0:!1}function j(a){for(var b,c,f,h=[],j=[],k=0;k<a.length;k++){var l=d(a[k].slice(0));if(!(l.length<4))if(e(l)){var m=[l];h.push(m)}else j.push(l)}for(var n=[];j.length;){f=j.pop();var o=!1;for(b=h.length-1;b>=0;b--)if(c=h[b][0],i(c,f)){h[b].push(f),o=!0;break}o||n.push(f)}for(;n.length;){f=n.pop();var p=!1;for(b=h.length-1;b>=0;b--)if(c=h[b][0],g(c,f)){h[b].push(f),p=!0;break}p||h.push([f.reverse()])}return 1===h.length?{type:"Polygon",coordinates:h[0]}:{type:"MultiPolygon",coordinates:h}}function k(a){var b=[],c=a.slice(0),f=d(c.shift().slice(0));if(f.length>=4){e(f)||f.reverse(),b.push(f);for(var g=0;g<c.length;g++){var h=d(c[g].slice(0));h.length>=4&&(e(h)&&h.reverse(),b.push(h))}}return b}function l(a){for(var b=[],c=0;c<a.length;c++)for(var d=k(a[c]),e=d.length-1;e>=0;e--){var f=d[e].slice(0);b.push(f)}return b}var m=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.msRequestAnimationFrame||function(a){return window.setTimeout(a,1e3/60)};a.Util.extentToBounds=function(a){var b=new L.LatLng(a.ymin,a.xmin),c=new L.LatLng(a.ymax,a.xmax);return new L.LatLngBounds(b,c)},a.Util.boundsToExtent=function(a){return a=L.latLngBounds(a),{xmin:a.getSouthWest().lng,ymin:a.getSouthWest().lat,xmax:a.getNorthEast().lng,ymax:a.getNorthEast().lat,spatialReference:{wkid:4326}}},a.Util.arcgisToGeojson=function(c,d){var e={};return"number"==typeof c.x&&"number"==typeof c.y&&(e.type="Point",e.coordinates=[c.x,c.y]),c.points&&(e.type="MultiPoint",e.coordinates=c.points.slice(0)),c.paths&&(1===c.paths.length?(e.type="LineString",e.coordinates=c.paths[0].slice(0)):(e.type="MultiLineString",e.coordinates=c.paths.slice(0))),c.rings&&(e=j(c.rings.slice(0))),(c.geometry||c.attributes)&&(e.type="Feature",e.geometry=c.geometry?a.Util.arcgisToGeojson(c.geometry):null,e.properties=c.attributes?b(c.attributes):null,c.attributes&&(e.id=c.attributes[d]||c.attributes.OBJECTID||c.attributes.FID)),e},a.Util.geojsonToArcGIS=function(c,d){d=d||"OBJECTID";var e,f={wkid:4326},g={};switch(c.type){case"Point":g.x=c.coordinates[0],g.y=c.coordinates[1],g.spatialReference=f;break;case"MultiPoint":g.points=c.coordinates.slice(0),g.spatialReference=f;break;case"LineString":g.paths=[c.coordinates.slice(0)],g.spatialReference=f;break;case"MultiLineString":g.paths=c.coordinates.slice(0),g.spatialReference=f;break;case"Polygon":g.rings=k(c.coordinates.slice(0)),g.spatialReference=f;break;case"MultiPolygon":g.rings=l(c.coordinates.slice(0)),g.spatialReference=f;break;case"Feature":c.geometry&&(g.geometry=a.Util.geojsonToArcGIS(c.geometry,d)),g.attributes=c.properties?b(c.properties):{},c.id&&(g.attributes[d]=c.id);break;case"FeatureCollection":for(g=[],e=0;e<c.features.length;e++)g.push(a.Util.geojsonToArcGIS(c.features[e],d));break;case"GeometryCollection":for(g=[],e=0;e<c.geometries.length;e++)g.push(a.Util.geojsonToArcGIS(c.geometries[e],d))}return g},a.Util.responseToFeatureCollection=function(b,c){var d;if(c)d=c;else if(b.objectIdFieldName)d=b.objectIdFieldName;else if(b.fields){for(var e=0;e<=b.fields.length-1;e++)if("esriFieldTypeOID"===b.fields[e].type){d=b.fields[e].name;break}}else d="OBJECTID";var f={type:"FeatureCollection",features:[]},g=b.features||b.results;if(g.length)for(var h=g.length-1;h>=0;h--)f.features.push(a.Util.arcgisToGeojson(g[h],d));return f},a.Util.cleanUrl=function(a){return a=a.replace(/\s\s*/g,""),"/"!==a[a.length-1]&&(a+="/"),a},a.Util.isArcgisOnline=function(a){return/\.arcgis\.com/g.test(a)},a.Util.geojsonTypeToArcGIS=function(a){var b;switch(a){case"Point":b="esriGeometryPoint";break;case"MultiPoint":b="esriGeometryMultipoint";break;case"LineString":b="esriGeometryPolyline";break;case"MultiLineString":b="esriGeometryPolyline";break;case"Polygon":b="esriGeometryPolygon";break;case"MultiPolygon":b="esriGeometryPolygon"}return b},a.Util.requestAnimationFrame=L.Util.bind(m,window)}(EsriLeaflet),function(a){function b(a){var b="";a.f=a.f||"json";for(var c in a)if(a.hasOwnProperty(c)){var d,e=a[c],f=Object.prototype.toString.call(e);b.length&&(b+="&"),d="[object Array]"===f?"[object Object]"===Object.prototype.toString.call(e[0])?JSON.stringify(e):e.join(","):"[object Object]"===f?JSON.stringify(e):"[object Date]"===f?e.valueOf():e,b+=encodeURIComponent(c)+"="+encodeURIComponent(d)}return b}function c(a,b){var c=new XMLHttpRequest;return c.onerror=function(){a.call(b,{error:{code:500,message:"XMLHttpRequest error"}},null)},c.onreadystatechange=function(){var d,e;if(4===c.readyState){try{d=JSON.parse(c.responseText)}catch(f){d=null,e={code:500,message:"Could not parse response as JSON."}}!e&&d.error&&(e=d.error,d=null),a.call(b,e,d)}},c}var d=0;window._EsriLeafletCallbacks={},a.Request={request:function(a,d,e,f){var g=b(d),h=c(e,f),i=(a+"?"+g).length;if(2e3>=i&&L.esri.Support.CORS)h.open("GET",a+"?"+g),h.send(null);else if(i>2e3&&L.esri.Support.CORS)h.open("POST",a),h.setRequestHeader("Content-Type","application/x-www-form-urlencoded"),h.send(g);else{if(2e3>=i&&!L.esri.Support.CORS)return L.esri.Request.get.JSONP(a,d,e,f);if(console&&console.warn)return void console.warn("a request to "+a+" was longer then 2000 characters and this browser cannot make a cross-domain post request. Please use a proxy http://esri.github.io/esri-leaflet/api-reference/request.html")}return h},post:{XMLHTTP:function(a,d,e,f){var g=c(e,f);return g.open("POST",a),g.setRequestHeader("Content-Type","application/x-www-form-urlencoded"),g.send(b(d)),g}},get:{CORS:function(a,d,e,f){var g=c(e,f);return g.open("GET",a+"?"+b(d),!0),g.send(null),g},JSONP:function(a,c,e,f){var g="c"+d;c.callback="window._EsriLeafletCallbacks."+g;var h=L.DomUtil.create("script",null,document.body);return h.type="text/javascript",h.src=a+"?"+b(c),h.id=g,window._EsriLeafletCallbacks[g]=function(a){if(window._EsriLeafletCallbacks[g]!==!0){var b,c=Object.prototype.toString.call(a);"[object Object]"!==c&&"[object Array]"!==c&&(b={error:{code:500,message:"Expected array or object as JSONP response"}},a=null),!b&&a.error&&(b=a,a=null),e.call(f,b,a),window._EsriLeafletCallbacks[g]=!0}},d++,{id:g,url:h.src,abort:function(){window._EsriLeafletCallbacks._callback[g]({code:0,message:"Request aborted."})}}}}},a.get=a.Support.CORS?a.Request.get.CORS:a.Request.get.JSONP,a.post=a.Request.post.XMLHTTP,a.request=a.Request.request}(EsriLeaflet),EsriLeaflet.Services.Service=L.Class.extend({includes:L.Mixin.Events,options:{proxy:!1,useCors:EsriLeaflet.Support.CORS},initialize:function(a){a=a||{},this._requestQueue=[],this._authenticating=!1,L.Util.setOptions(this,a),this.options.url=EsriLeaflet.Util.cleanUrl(this.options.url)},get:function(a,b,c,d){return this._request("get",a,b,c,d)},post:function(a,b,c,d){return this._request("post",a,b,c,d)},request:function(a,b,c,d){return this._request("request",a,b,c,d)},metadata:function(a,b){return this._request("get","",{},a,b)},authenticate:function(a){return this._authenticating=!1,this.options.token=a,this._runQueue(),this},_request:function(a,b,c,d,e){this.fire("requeststart",{url:this.options.url+b,params:c,method:a});var f=this._createServiceCallback(a,b,c,d,e);if(this.options.token&&(c.token=this.options.token),this._authenticating)return void this._requestQueue.push([a,b,c,d,e]);var g=this.options.proxy?this.options.proxy+"?"+this.options.url+b:this.options.url+b;return"get"!==a&&"request"!==a||this.options.useCors?EsriLeaflet[a](g,c,f):EsriLeaflet.Request.get.JSONP(g,c,f)},_createServiceCallback:function(a,b,c,d,e){var f=[a,b,c,d,e];return L.Util.bind(function(g,h){!g||499!==g.code&&498!==g.code?(d.call(e,g,h),g?this.fire("requesterror",{url:this.options.url+b,params:c,message:g.message,code:g.code,method:a}):this.fire("requestsuccess",{url:this.options.url+b,params:c,response:h,method:a}),this.fire("requestend",{url:this.options.url+b,params:c,method:a})):(this._authenticating=!0,this._requestQueue.push(f),this.fire("authenticationrequired",{authenticate:L.Util.bind(this.authenticate,this)}))},this)},_runQueue:function(){for(var a=this._requestQueue.length-1;a>=0;a--){var b=this._requestQueue[a],c=b.shift();this[c].apply(this,b)}this._requestQueue=[]}}),EsriLeaflet.Services.service=function(a){return new EsriLeaflet.Services.Service(a)},EsriLeaflet.Services.FeatureLayer=EsriLeaflet.Services.Service.extend({options:{idAttribute:"OBJECTID"},query:function(){return new EsriLeaflet.Tasks.Query(this)},addFeature:function(a,b,c){return delete a.id,a=EsriLeaflet.Util.geojsonToArcGIS(a),this.post("addFeatures",{features:[a]},function(a,c){var d=c&&c.addResults?c.addResults[0]:void 0;b&&b.call(this,a||c.addResults[0].error,d)},c)},updateFeature:function(a,b,c){return a=EsriLeaflet.Util.geojsonToArcGIS(a,this.options.idAttribute),this.post("updateFeatures",{features:[a]},function(a,d){var e=d&&d.updateResults?d.updateResults[0]:void 0;b&&b.call(c,a||d.updateResults[0].error,e)},c)},deleteFeature:function(a,b,c){return this.post("deleteFeatures",{objectIds:a},function(a,d){var e=d&&d.deleteResults?d.deleteResults[0]:void 0;b&&b.call(c,a||d.deleteResults[0].error,e)},c)}}),EsriLeaflet.Services.featureLayer=function(a){return new EsriLeaflet.Services.FeatureLayer(a)},EsriLeaflet.Services.MapService=EsriLeaflet.Services.Service.extend({identify:function(){return new EsriLeaflet.Tasks.identifyFeatures(this)},find:function(){return new EsriLeaflet.Tasks.Find(this)},query:function(){return new EsriLeaflet.Tasks.Query(this)}}),EsriLeaflet.Services.mapService=function(a){return new EsriLeaflet.Services.MapService(a)},EsriLeaflet.Services.ImageService=EsriLeaflet.Services.Service.extend({query:function(){return new EsriLeaflet.Tasks.Query(this)},identify:function(){return new EsriLeaflet.Tasks.IdentifyImage(this)}}),EsriLeaflet.Services.imageService=function(a){return new EsriLeaflet.Services.ImageService(a)},EsriLeaflet.Tasks.Task=L.Class.extend({options:{proxy:!1,useCors:EsriLeaflet.Support.CORS},generateSetter:function(a,b){return L.Util.bind(function(b){return this.params[a]=b,this},b)},initialize:function(a){if(a.request&&a.options?(this._service=a,L.Util.setOptions(this,a.options)):(L.Util.setOptions(this,a),this.options.url=L.esri.Util.cleanUrl(a.url)),this.params=L.Util.extend({},this.params||{}),this.setters)for(var b in this.setters){var c=this.setters[b];this[b]=this.generateSetter(c,this)}},token:function(a){return this._service?this._service.authenticate(a):this.params.token=a,this},request:function(a,b){return this._service?this._service.request(this.path,this.params,a,b):this._request("request",this.path,this.params,a,b)},_request:function(a,b,c,d,e){var f=this.options.proxy?this.options.proxy+"?"+this.options.url+b:this.options.url+b;return"get"!==a&&"request"!==a||this.options.useCors?EsriLeaflet[a](f,c,d,e):EsriLeaflet.Request.get.JSONP(f,c,d,e)}}),EsriLeaflet.Tasks.Query=EsriLeaflet.Tasks.Task.extend({setters:{offset:"offset",limit:"limit",fields:"outFields",precision:"geometryPrecision",featureIds:"objectIds",returnGeometry:"returnGeometry",token:"token"},path:"query",params:{returnGeometry:!0,where:"1=1",outSr:4326,outFields:"*"},within:function(a){return this._setGeometry(a),this.params.spatialRel="esriSpatialRelContains",this},intersects:function(a){return this._setGeometry(a),this.params.spatialRel="esriSpatialRelIntersects",this},contains:function(a){return this._setGeometry(a),this.params.spatialRel="esriSpatialRelWithin",this},overlaps:function(a){return this._setGeometry(a),this.params.spatialRel="esriSpatialRelOverlaps",this},nearby:function(a,b){return a=L.latLng(a),this.params.geometry=[a.lng,a.lat],this.params.geometryType="esriGeometryPoint",this.params.spatialRel="esriSpatialRelIntersects",this.params.units="esriSRUnit_Meter",this.params.distance=b,this.params.inSr=4326,this},where:function(a){return this.params.where=a.replace(/"/g,"'"),this},between:function(a,b){return this.params.time=[a.valueOf(),b.valueOf()],this},simplify:function(a,b){var c=Math.abs(a.getBounds().getWest()-a.getBounds().getEast());return this.params.maxAllowableOffset=c/a.getSize().y*b,this},orderBy:function(a,b){return b=b||"ASC",this.params.orderByFields=this.params.orderByFields?this.params.orderByFields+",":"",this.params.orderByFields+=[a,b].join(" "),this},run:function(a,b){return this._cleanParams(),EsriLeaflet.Util.isArcgisOnline(this.options.url)?(this.params.f="geojson",this.request(function(c,d){a.call(b,c,d,d)},b)):this.request(function(c,d){a.call(b,c,d&&EsriLeaflet.Util.responseToFeatureCollection(d),d)},b)},count:function(a,b){return this._cleanParams(),this.params.returnCountOnly=!0,this.request(function(b,c){a.call(this,b,c&&c.count,c)},b)},ids:function(a,b){return this._cleanParams(),this.params.returnIdsOnly=!0,this.request(function(b,c){a.call(this,b,c&&c.objectIds,c)},b)},bounds:function(a,b){return this._cleanParams(),this.params.returnExtentOnly=!0,this.request(function(c,d){a.call(b,c,d&&d.extent&&EsriLeaflet.Util.extentToBounds(d.extent),d)},b)},pixelSize:function(a){return a=L.point(a),this.params.pixelSize=[a.x,a.y],this},layer:function(a){return this.path=a+"/query",this},_cleanParams:function(){delete this.params.returnIdsOnly,delete this.params.returnExtentOnly,delete this.params.returnCountOnly},_setGeometry:function(a){return this.params.inSr=4326,a instanceof L.LatLngBounds?(this.params.geometry=EsriLeaflet.Util.boundsToExtent(a),void(this.params.geometryType="esriGeometryEnvelope")):(a.getLatLng&&(a=a.getLatLng()),a instanceof L.LatLng&&(a={type:"Point",coordinates:[a.lng,a.lat]}),a instanceof L.GeoJSON&&(a=a.getLayers()[0].feature.geometry,this.params.geometry=EsriLeaflet.Util.geojsonToArcGIS(a),this.params.geometryType=EsriLeaflet.Util.geojsonTypeToArcGIS(a.type)),a.toGeoJSON&&(a=a.toGeoJSON()),"Feature"===a.type&&(a=a.geometry),"Point"===a.type||"LineString"===a.type||"Polygon"===a.type?(this.params.geometry=EsriLeaflet.Util.geojsonToArcGIS(a),void(this.params.geometryType=EsriLeaflet.Util.geojsonTypeToArcGIS(a.type))):void(console&&console.warn&&console.warn("invalid geometry passed to spatial query. Should be an L.LatLng, L.LatLngBounds or L.Marker or a GeoJSON Point Line or Polygon object")))}}),EsriLeaflet.Tasks.query=function(a){return new EsriLeaflet.Tasks.Query(a)},EsriLeaflet.Tasks.Find=EsriLeaflet.Tasks.Task.extend({setters:{contains:"contains",text:"searchText",fields:"searchFields",spatialReference:"sr",sr:"sr",layers:"layers",returnGeometry:"returnGeometry",maxAllowableOffset:"maxAllowableOffset",precision:"geometryPrecision",dynamicLayers:"dynamicLayers",returnZ:"returnZ",returnM:"returnM",gdbVersion:"gdbVersion",token:"token"},path:"find",params:{sr:4326,contains:!0,returnGeometry:!0,returnZ:!0,returnM:!1},layerDefs:function(a,b){return this.params.layerDefs=this.params.layerDefs?this.params.layerDefs+";":"",this.params.layerDefs+=[a,b].join(":"),this},simplify:function(a,b){var c=Math.abs(a.getBounds().getWest()-a.getBounds().getEast());return this.params.maxAllowableOffset=c/a.getSize().y*b,this},run:function(a,b){return this.request(function(c,d){a.call(b,c,d&&EsriLeaflet.Util.responseToFeatureCollection(d),d)},b)}}),EsriLeaflet.Tasks.find=function(a){return new EsriLeaflet.Tasks.Find(a)},EsriLeaflet.Tasks.Identify=EsriLeaflet.Tasks.Task.extend({path:"identify",between:function(a,b){return this.params.time=[a.valueOf(),b.valueOf()],this}}),EsriLeaflet.Tasks.IdentifyImage=EsriLeaflet.Tasks.Identify.extend({setters:{setMosaicRule:"mosaicRule",setRenderingRule:"renderingRule",setPixelSize:"pixelSize",returnCatalogItems:"returnCatalogItems",returnGeometry:"returnGeometry"},params:{returnGeometry:!1},at:function(a){return a=L.latLng(a),this.params.geometry=JSON.stringify({x:a.lng,y:a.lat,spatialReference:{wkid:4326}}),this.params.geometryType="esriGeometryPoint",this},getMosaicRule:function(){return this.params.mosaicRule},getRenderingRule:function(){return this.params.renderingRule},getPixelSize:function(){return this.params.pixelSize},run:function(a,b){return this.request(function(c,d){a.call(b,c,d&&this._responseToGeoJSON(d),d)},this)},_responseToGeoJSON:function(a){var b=a.location,c=a.catalogItems,d=a.catalogItemVisibilities,e={pixel:{type:"Feature",geometry:{type:"Point",coordinates:[b.x,b.y]},crs:{type:"EPSG",properties:{code:b.spatialReference.wkid}},properties:{OBJECTID:a.objectId,name:a.name,value:a.value},id:a.objectId}};if(a.properties&&a.properties.Values&&(e.pixel.properties.values=a.properties.Values),c&&c.features&&(e.catalogItems=EsriLeaflet.Util.responseToFeatureCollection(c),d&&d.length===e.catalogItems.features.length))for(var f=d.length-1;f>=0;f--)e.catalogItems.features[f].properties.catalogItemVisibility=d[f];return e}}),EsriLeaflet.Tasks.identifyImage=function(a){return new EsriLeaflet.Tasks.IdentifyImage(a)},EsriLeaflet.Tasks.IdentifyFeatures=EsriLeaflet.Tasks.Identify.extend({setters:{layers:"layers",precision:"geometryPrecision",tolerance:"tolerance",returnGeometry:"returnGeometry"},params:{sr:4326,layers:"all",tolerance:3,returnGeometry:!0},on:function(a){var b=EsriLeaflet.Util.boundsToExtent(a.getBounds()),c=a.getSize();return this.params.imageDisplay=[c.x,c.y,96],this.params.mapExtent=[b.xmin,b.ymin,b.xmax,b.ymax],this},at:function(a){return a=L.latLng(a),this.params.geometry=[a.lng,a.lat],this.params.geometryType="esriGeometryPoint",this},layerDef:function(a,b){return this.params.layerDefs=this.params.layerDefs?this.params.layerDefs+";":"",this.params.layerDefs+=[a,b].join(":"),this},simplify:function(a,b){var c=Math.abs(a.getBounds().getWest()-a.getBounds().getEast());return this.params.maxAllowableOffset=c/a.getSize().y*(1-b),this},run:function(a,b){return this.request(function(c,d){a.call(b,c,d&&EsriLeaflet.Util.responseToFeatureCollection(d),d)},b)}}),EsriLeaflet.Tasks.identifyFeatures=function(a){return new EsriLeaflet.Tasks.IdentifyFeatures(a)},function(a){var b="https:"!==window.location.protocol?"http:":"https:";a.Layers.BasemapLayer=L.TileLayer.extend({statics:{TILES:{Streets:{urlTemplate:b+"//{s}.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}",attributionUrl:"https://static.arcgis.com/attribution/World_Street_Map",options:{hideLogo:!1,logoPosition:"bottomright",minZoom:1,maxZoom:19,subdomains:["server","services"],attribution:"Esri"}},Topographic:{urlTemplate:b+"//{s}.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}",attributionUrl:"https://static.arcgis.com/attribution/World_Topo_Map",options:{hideLogo:!1,logoPosition:"bottomright",minZoom:1,maxZoom:19,subdomains:["server","services"],attribution:"Esri"}},Oceans:{urlTemplate:b+"//{s}.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Base/MapServer/tile/{z}/{y}/{x}",attributionUrl:"https://static.arcgis.com/attribution/Ocean_Basemap",options:{hideLogo:!1,logoPosition:"bottomright",minZoom:1,maxZoom:16,subdomains:["server","services"],attribution:"Esri"}},OceansLabels:{urlTemplate:b+"//{s}.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Reference/MapServer/tile/{z}/{y}/{x}",options:{hideLogo:!0,logoPosition:"bottomright",minZoom:1,maxZoom:16,subdomains:["server","services"]}},NationalGeographic:{urlTemplate:b+"//{s}.arcgisonline.com/ArcGIS/rest/services/NatGeo_World_Map/MapServer/tile/{z}/{y}/{x}",options:{hideLogo:!1,logoPosition:"bottomright",minZoom:1,maxZoom:16,subdomains:["server","services"],attribution:"Esri"}},DarkGray:{urlTemplate:b+"//{s}.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Dark_Gray_Base/MapServer/tile/{z}/{y}/{x}",options:{hideLogo:!1,logoPosition:"bottomright",minZoom:1,maxZoom:16,subdomains:["server","services"],attribution:"Esri, DeLorme, HERE"}},DarkGrayLabels:{urlTemplate:b+"//{s}.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Dark_Gray_Reference/MapServer/tile/{z}/{y}/{x}",options:{hideLogo:!0,logoPosition:"bottomright",minZoom:1,maxZoom:16,subdomains:["server","services"]}},Gray:{urlTemplate:b+"//{s}.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}",options:{hideLogo:!1,logoPosition:"bottomright",minZoom:1,maxZoom:16,subdomains:["server","services"],attribution:"Esri, NAVTEQ, DeLorme"}},GrayLabels:{urlTemplate:b+"//{s}.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Reference/MapServer/tile/{z}/{y}/{x}",options:{hideLogo:!0,logoPosition:"bottomright",minZoom:1,maxZoom:16,subdomains:["server","services"]}},Imagery:{urlTemplate:b+"//{s}.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}",options:{hideLogo:!1,logoPosition:"bottomright",minZoom:1,maxZoom:19,subdomains:["server","services"],attribution:"Esri, DigitalGlobe, GeoEye, i-cubed, USDA, USGS, AEX, Getmapping, Aerogrid, IGN, IGP, swisstopo, and the GIS User Community"}},ImageryLabels:{urlTemplate:b+"//{s}.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places/MapServer/tile/{z}/{y}/{x}",options:{hideLogo:!0,logoPosition:"bottomright",minZoom:1,maxZoom:19,subdomains:["server","services"]}},ImageryTransportation:{urlTemplate:b+"//{s}.arcgisonline.com/ArcGIS/rest/services/Reference/World_Transportation/MapServer/tile/{z}/{y}/{x}",options:{hideLogo:!0,logoPosition:"bottomright",minZoom:1,maxZoom:19,subdomains:["server","services"]}},ShadedRelief:{urlTemplate:b+"//{s}.arcgisonline.com/ArcGIS/rest/services/World_Shaded_Relief/MapServer/tile/{z}/{y}/{x}",options:{hideLogo:!1,logoPosition:"bottomright",minZoom:1,maxZoom:13,subdomains:["server","services"],attribution:"ESRI, NAVTEQ, DeLorme"}},ShadedReliefLabels:{urlTemplate:b+"//{s}.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places_Alternate/MapServer/tile/{z}/{y}/{x}",options:{hideLogo:!0,logoPosition:"bottomright",minZoom:1,maxZoom:12,subdomains:["server","services"]}},Terrain:{urlTemplate:b+"//{s}.arcgisonline.com/ArcGIS/rest/services/World_Terrain_Base/MapServer/tile/{z}/{y}/{x}",options:{hideLogo:!1,logoPosition:"bottomright",minZoom:1,maxZoom:13,subdomains:["server","services"],attribution:"Esri, USGS, NOAA"}},TerrainLabels:{urlTemplate:b+"//{s}.arcgisonline.com/ArcGIS/rest/services/Reference/World_Reference_Overlay/MapServer/tile/{z}/{y}/{x}",options:{hideLogo:!0,logoPosition:"bottomright",minZoom:1,maxZoom:13,subdomains:["server","services"]}}}},initialize:function(b,c){var d;if("object"==typeof b&&b.urlTemplate&&b.options)d=b;else{if("string"!=typeof b||!a.BasemapLayer.TILES[b])throw new Error('L.esri.BasemapLayer: Invalid parameter. Use one of "Streets", "Topographic", "Oceans", "OceansLabels", "NationalGeographic", "Gray", "GrayLabels", "DarkGray", "DarkGrayLabels", "Imagery", "ImageryLabels", "ImageryTransportation", "ShadedRelief", "ShadedReliefLabels", "Terrain" or "TerrainLabels"');d=a.BasemapLayer.TILES[b]}var e=L.Util.extend(d.options,c);L.TileLayer.prototype.initialize.call(this,d.urlTemplate,L.Util.setOptions(this,e)),d.attributionUrl&&this._getAttributionData(d.attributionUrl),this._logo=new a.Controls.Logo({position:this.options.logoPosition})},onAdd:function(a){this.options.hideLogo||a._hasEsriLogo||(this._logo.addTo(a),a._hasEsriLogo=!0),L.TileLayer.prototype.onAdd.call(this,a),a.on("moveend",this._updateMapAttribution,this)},onRemove:function(a){this._logo&&(a.removeControl(this._logo),a._hasEsriLogo=!1),L.TileLayer.prototype.onRemove.call(this,a),a.off("moveend",this._updateMapAttribution,this)},getAttribution:function(){var a='<span class="esri-attributions" style="line-height:14px; vertical-align: -3px; text-overflow:ellipsis; white-space:nowrap; overflow:hidden; display:inline-block;">'+this.options.attribution+"</span>";return a},_getAttributionData:function(b){a.get(b,{},function(a,b){this._attributions=[];for(var c=0;c<b.contributors.length;c++)for(var d=b.contributors[c],e=0;e<d.coverageAreas.length;e++){var f=d.coverageAreas[e],g=new L.LatLng(f.bbox[0],f.bbox[1]),h=new L.LatLng(f.bbox[2],f.bbox[3]);this._attributions.push({attribution:d.attribution,score:f.score,bounds:new L.LatLngBounds(g,h),minZoom:f.zoomMin,maxZoom:f.zoomMax})}this._attributions.sort(function(a,b){return b.score-a.score}),this._updateMapAttribution()},this)},_updateMapAttribution:function(){if(this._map&&this._map.attributionControl&&this._attributions){for(var a="",b=this._map.getBounds(),c=this._map.getZoom(),d=0;d<this._attributions.length;d++){var e=this._attributions[d],f=e.attribution;!a.match(f)&&b.intersects(e.bounds)&&c>=e.minZoom&&c<=e.maxZoom&&(a+=", "+f)}a=a.substr(2);var g=this._map.attributionControl._container.querySelector(".esri-attributions");g.innerHTML=a,g.style.maxWidth=.65*this._map.getSize().x+"px",this.fire("attributionupdated",{attribution:a})}}}),a.BasemapLayer=a.Layers.BasemapLayer,a.Layers.basemapLayer=function(b,c){return new a.Layers.BasemapLayer(b,c)},a.basemapLayer=function(b,c){return new a.Layers.BasemapLayer(b,c)}}(EsriLeaflet),EsriLeaflet.Layers.RasterLayer=L.Class.extend({includes:L.Mixin.Events,options:{opacity:1,position:"front",f:"image"},onAdd:function(a){if(this._map=a,this._update=L.Util.limitExecByInterval(this._update,this.options.updateInterval,this),a.options.crs&&a.options.crs.code){var b=a.options.crs.code.split(":")[1];this.options.bboxSR=b,this.options.imageSR=b}a.on("moveend",this._update,this),this._currentImage&&this._currentImage._bounds.equals(this._map.getBounds())?a.addLayer(this._currentImage):this._currentImage&&(this._map.removeLayer(this._currentImage),this._currentImage=null),this._update(),this._popup&&(this._map.on("click",this._getPopupData,this),this._map.on("dblclick",this._resetPopupState,this))},bindPopup:function(a,b){return this._shouldRenderPopup=!1,this._lastClick=!1,this._popup=L.popup(b),this._popupFunction=a,this._map&&(this._map.on("click",this._getPopupData,this),this._map.on("dblclick",this._resetPopupState,this)),this},unbindPopup:function(){return this._map&&(this._map.closePopup(this._popup),this._map.off("click",this._getPopupData,this),this._map.off("dblclick",this._resetPopupState,this)),this._popup=!1,this},onRemove:function(){this._currentImage&&this._map.removeLayer(this._currentImage),this._popup&&(this._map.off("click",this._getPopupData,this),this._map.off("dblclick",this._resetPopupState,this)),this._map.off("moveend",this._update,this),this._map=null},addTo:function(a){return a.addLayer(this),this},removeFrom:function(a){return a.removeLayer(this),this},bringToFront:function(){return this.options.position="front",this._currentImage&&this._currentImage.bringToFront(),this},bringToBack:function(){return this.options.position="back",this._currentImage&&this._currentImage.bringToBack(),this},getAttribution:function(){return this.options.attribution},getOpacity:function(){return this.options.opacity},setOpacity:function(a){return this.options.opacity=a,this._currentImage.setOpacity(a),this},getTimeRange:function(){return[this.options.from,this.options.to]},setTimeRange:function(a,b){return this.options.from=a,this.options.to=b,this._update(),this},metadata:function(a,b){return this._service.metadata(a,b),this},authenticate:function(a){return this._service.authenticate(a),this},_renderImage:function(a,b){if(this._map){var c=new L.ImageOverlay(a,b,{opacity:0}).addTo(this._map);c.once("load",function(a){var c=a.target,d=this._currentImage;c._bounds.equals(b)?(this._currentImage=c,"front"===this.options.position?this.bringToFront():this.bringToBack(),this._map&&this._currentImage._map?this._currentImage.setOpacity(this.options.opacity):this._currentImage._map.removeLayer(this._currentImage),d&&this._map&&this._map.removeLayer(d),d&&d._map&&d._map.removeLayer(d)):this._map.removeLayer(c),this.fire("load",{bounds:b})},this),this.fire("loading",{bounds:b})}},_update:function(){if(this._map){var a=this._map.getZoom(),b=this._map.getBounds();if(!this._animatingZoom&&!(this._map._panTransition&&this._map._panTransition._inProgress||a>this.options.maxZoom||a<this.options.minZoom)){var c=this._buildExportParams();this._requestExport(c,b)}}},_renderPopup:function(a,b,c,d){if(a=L.latLng(a),this._shouldRenderPopup&&this._lastClick.equals(a)){var e=this._popupFunction(b,c,d);e&&this._popup.setLatLng(a).setContent(e).openOn(this._map)}},_resetPopupState:function(a){this._shouldRenderPopup=!1,this._lastClick=a.latlng},_propagateEvent:function(a){a=L.extend({layer:a.target,target:this},a),this.fire(a.type,a)}}),EsriLeaflet.Layers.DynamicMapLayer=EsriLeaflet.Layers.RasterLayer.extend({options:{updateInterval:150,layers:!1,layerDefs:!1,timeOptions:!1,format:"png24",transparent:!0},initialize:function(a,b){b=b||{},b.url=EsriLeaflet.Util.cleanUrl(a),this._service=new EsriLeaflet.Services.MapService(b),this._service.on("authenticationrequired requeststart requestend requesterror requestsuccess",this._propagateEvent,this),L.Util.setOptions(this,b)},getLayers:function(){return this.options.layers},setLayers:function(a){return this.options.layers=a,this._update(),this},getLayerDefs:function(){return this.options.layerDefs},setLayerDefs:function(a){return this.options.layerDefs=a,this._update(),this},getTimeOptions:function(){return this.options.timeOptions},setTimeOptions:function(a){return this.options.timeOptions=a,this._update(),this},query:function(){return this._service.query()},identify:function(){return this._service.identify()},find:function(){return this._service.find()},_getPopupData:function(a){var b=L.Util.bind(function(b,c,d){setTimeout(L.Util.bind(function(){this._renderPopup(a.latlng,b,c,d)},this),300)},this),c=this.identify().on(this._map).at(a.latlng);c.layers(this.options.layers?"visible:"+this.options.layers.join(","):"visible"),c.run(b),this._shouldRenderPopup=!0,this._lastClick=a.latlng},_buildExportParams:function(){var a=this._map.getBounds(),b=this._map.getSize(),c=this._map.options.crs.project(a._northEast),d=this._map.options.crs.project(a._southWest),e={bbox:[d.x,d.y,c.x,c.y].join(","),size:b.x+","+b.y,dpi:96,format:this.options.format,transparent:this.options.transparent,bboxSR:this.options.bboxSR,imageSR:this.options.imageSR};return this.options.layers&&(e.layers="show:"+this.options.layers.join(",")),this.options.layerDefs&&(e.layerDefs=JSON.stringify(this.options.layerDefs)),this.options.timeOptions&&(e.timeOptions=JSON.stringify(this.options.timeOptions)),this.options.from&&this.options.to&&(e.time=this.options.from.valueOf()+","+this.options.to.valueOf()),this._service.options.token&&(e.token=this._service.options.token),e},_requestExport:function(a,b){"json"===this.options.f?this._service.get("export",a,function(a,c){this._renderImage(c.href,b)},this):(a.f="image",this._renderImage(this.options.url+"export"+L.Util.getParamString(a),b))}}),EsriLeaflet.DynamicMapLayer=EsriLeaflet.Layers.DynamicMapLayer,EsriLeaflet.Layers.dynamicMapLayer=function(a,b){return new EsriLeaflet.Layers.DynamicMapLayer(a,b)},EsriLeaflet.dynamicMapLayer=function(a,b){return new EsriLeaflet.Layers.DynamicMapLayer(a,b)
},EsriLeaflet.Layers.ImageMapLayer=EsriLeaflet.Layers.RasterLayer.extend({options:{updateInterval:150,format:"jpgpng"},query:function(){return this._service.query()},identify:function(){return this._service.identify()},initialize:function(a,b){b=b||{},b.url=EsriLeaflet.Util.cleanUrl(a),this._service=new EsriLeaflet.Services.ImageService(b),this._service.on("authenticationrequired requeststart requestend requesterror requestsuccess",this._propagateEvent,this),L.Util.setOptions(this,b)},setPixelType:function(a){return this.options.pixelType=a,this._update(),this},getPixelType:function(){return this.options.pixelType},setBandIds:function(a){return this.options.bandIds=L.Util.isArray(a)?a.join(","):a.toString(),this._update(),this},getBandIds:function(){return this.options.bandIds},setNoData:function(a,b){return this.options.noData=L.Util.isArray(a)?a.join(","):a.toString(),b&&(this.options.noDataInterpretation=b),this._update(),this},getNoData:function(){return this.options.noData},getNoDataInterpretation:function(){return this.options.noDataInterpretation},setRenderingRule:function(a){this.options.renderingRule=a,this._update()},getRenderingRule:function(){return this.options.renderingRule},setMosaicRule:function(a){this.options.mosaicRule=a,this._update()},getMosaicRule:function(){return this.options.mosaicRule},_getPopupData:function(a){var b=L.Util.bind(function(b,c,d){setTimeout(L.Util.bind(function(){this._renderPopup(a.latlng,b,c,d)},this),300)},this),c=this.identify().at(a.latlng);this.options.mosaicRule&&c.setMosaicRule(this.options.mosaicRule),c.run(b),this._shouldRenderPopup=!0,this._lastClick=a.latlng},_buildExportParams:function(){var a=this._map.getBounds(),b=this._map.getSize(),c=this._map.options.crs.project(a._northEast),d=this._map.options.crs.project(a._southWest),e={bbox:[d.x,d.y,c.x,c.y].join(","),size:b.x+","+b.y,format:this.options.format,bboxSR:this.options.bboxSR,imageSR:this.options.imageSR};return this.options.from&&this.options.to&&(e.time=this.options.from.valueOf()+","+this.options.to.valueOf()),this.options.pixelType&&(e.pixelType=this.options.pixelType),this.options.interpolation&&(e.interpolation=this.options.interpolation),this.options.compressionQuality&&(e.compressionQuality=this.options.compressionQuality),this.options.bandIds&&(e.bandIds=this.options.bandIds),this.options.noData&&(e.noData=this.options.noData),this.options.noDataInterpretation&&(e.noDataInterpretation=this.options.noDataInterpretation),this._service.options.token&&(e.token=this._service.options.token),this.options.renderingRule&&(e.renderingRule=JSON.stringify(this.options.renderingRule)),this.options.mosaicRule&&(e.mosaicRule=JSON.stringify(this.options.mosaicRule)),e},_requestExport:function(a,b){"json"===this.options.f?this._service.get("exportImage",a,function(a,c){this._renderImage(c.href,b)},this):(a.f="image",this._renderImage(this.options.url+"exportImage"+L.Util.getParamString(a),b))}}),EsriLeaflet.ImageMapLayer=EsriLeaflet.Layers.ImageMapLayer,EsriLeaflet.Layers.imageMapLayer=function(a,b){return new EsriLeaflet.Layers.ImageMapLayer(a,b)},EsriLeaflet.imageMapLayer=function(a,b){return new EsriLeaflet.Layers.ImageMapLayer(a,b)},EsriLeaflet.Layers.TiledMapLayer=L.TileLayer.extend({initialize:function(a,b){b=b||{},b.url=EsriLeaflet.Util.cleanUrl(a),b=L.Util.setOptions(this,b),this.tileUrl=L.esri.Util.cleanUrl(a)+"tile/{z}/{y}/{x}",this._service=new L.esri.Services.MapService(b),this._service.on("authenticationrequired requeststart requestend requesterror requestsuccess",this._propagateEvent,this),this.tileUrl.match("://tiles.arcgisonline.com")&&(this.tileUrl=this.tileUrl.replace("://tiles.arcgisonline.com","://tiles{s}.arcgisonline.com"),b.subdomains=["1","2","3","4"]),this.options.token&&(this.tileUrl+="?token="+this.options.token),L.TileLayer.prototype.initialize.call(this,this.tileUrl,b)},metadata:function(a,b){return this._service.metadata(a,b),this},identify:function(){return this._service.identify()},authenticate:function(a){var b="?token="+a;return this.tileUrl=this.options.token?this.tileUrl.replace(/\?token=(.+)/g,b):this.tileUrl+b,this.options.token=a,this._service.authenticate(a),this},_propagateEvent:function(a){a=L.extend({layer:a.target,target:this},a),this.fire(a.type,a)}}),L.esri.TiledMapLayer=L.esri.Layers.tiledMapLayer,L.esri.Layers.tiledMapLayer=function(a,b){return new L.esri.Layers.TiledMapLayer(a,b)},L.esri.tiledMapLayer=function(a,b){return new L.esri.Layers.TiledMapLayer(a,b)},EsriLeaflet.Layers.FeatureGrid=L.Class.extend({includes:L.Mixin.Events,options:{cellSize:512,updateInterval:150},initialize:function(a){a=L.setOptions(this,a)},onAdd:function(a){this._map=a,this._update=L.Util.limitExecByInterval(this._update,this.options.updateInterval,this),this._map.addEventListener(this.getEvents(),this),this._reset(),this._update()},onRemove:function(){this._map.removeEventListener(this.getEvents(),this),this._removeCells()},getEvents:function(){var a={viewreset:this._reset,moveend:this._update,zoomend:this._onZoom};return a},addTo:function(a){return a.addLayer(this),this},removeFrom:function(a){return a.removeLayer(this),this},_onZoom:function(){var a=this._map.getZoom();a>this.options.maxZoom||a<this.options.minZoom?(this.removeFrom(this._map),this._map.addEventListener("zoomend",this.getEvents().zoomend,this)):this._map.hasLayer(this)||(this._map.removeEventListener("zoomend",this.getEvents().zoomend,this),this.addTo(this._map))},_reset:function(){this._removeCells(),this._cells={},this._activeCells={},this._cellsToLoad=0,this._cellsTotal=0,this._resetWrap()},_resetWrap:function(){var a=this._map,b=a.options.crs;if(!b.infinite){var c=this._getCellSize();b.wrapLng&&(this._wrapLng=[Math.floor(a.project([0,b.wrapLng[0]]).x/c),Math.ceil(a.project([0,b.wrapLng[1]]).x/c)]),b.wrapLat&&(this._wrapLat=[Math.floor(a.project([b.wrapLat[0],0]).y/c),Math.ceil(a.project([b.wrapLat[1],0]).y/c)])}},_getCellSize:function(){return this.options.cellSize},_update:function(){if(this._map){var a=this._map.getPixelBounds(),b=this._map.getZoom(),c=this._getCellSize(),d=[c/2,c/2];if(!(b>this.options.maxZoom||b<this.options.minZoom)){var e=a.min.subtract(d).divideBy(c).floor();e.x=Math.max(e.x,0),e.y=Math.max(e.y,0);var f=L.bounds(e,a.max.add(d).divideBy(c).floor());this._removeOtherCells(f),this._addCells(f)}}},_addCells:function(a){var b,c,d,e=[],f=a.getCenter(),g=this._map.getZoom();for(b=a.min.y;b<=a.max.y;b++)for(c=a.min.x;c<=a.max.x;c++)d=new L.Point(c,b),d.z=g,e.push(d);var h=e.length;if(0!==h)for(this._cellsToLoad+=h,this._cellsTotal+=h,e.sort(function(a,b){return a.distanceTo(f)-b.distanceTo(f)}),c=0;h>c;c++)this._addCell(e[c])},_cellCoordsToBounds:function(a){var b=this._map,c=this.options.cellSize,d=a.multiplyBy(c),e=d.add([c,c]),f=b.unproject(d,a.z).wrap(),g=b.unproject(e,a.z).wrap();return new L.LatLngBounds(f,g)},_cellCoordsToKey:function(a){return a.x+":"+a.y},_keyToCellCoords:function(a){var b=a.split(":"),c=parseInt(b[0],10),d=parseInt(b[1],10);return new L.Point(c,d)},_removeOtherCells:function(a){for(var b in this._cells)a.contains(this._keyToCellCoords(b))||this._removeCell(b)},_removeCell:function(a){var b=this._activeCells[a];b&&(delete this._activeCells[a],this.cellLeave&&this.cellLeave(b.bounds,b.coords),this.fire("cellleave",{bounds:b.bounds,coords:b.coords}))},_removeCells:function(){for(var a in this._cells){var b=this._cells[a].bounds,c=this._cells[a].coords;this.cellLeave&&this.cellLeave(b,c),this.fire("cellleave",{bounds:b,coords:c})}},_addCell:function(a){this._wrapCoords(a);var b=this._cellCoordsToKey(a),c=this._cells[b];c&&!this._activeCells[b]&&(this.cellEnter&&this.cellEnter(c.bounds,a),this.fire("cellenter",{bounds:c.bounds,coords:a}),this._activeCells[b]=c),c||(c={coords:a,bounds:this._cellCoordsToBounds(a)},this._cells[b]=c,this._activeCells[b]=c,this.createCell&&this.createCell(c.bounds,a),this.fire("cellcreate",{bounds:c.bounds,coords:a}))},_wrapCoords:function(a){a.x=this._wrapLng?L.Util.wrapNum(a.x,this._wrapLng):a.x,a.y=this._wrapLat?L.Util.wrapNum(a.y,this._wrapLat):a.y}}),function(a){function b(a){this.values=a||[]}a.Layers.FeatureManager=a.Layers.FeatureGrid.extend({options:{where:"1=1",fields:["*"],from:!1,to:!1,timeField:!1,timeFilterMode:"server",simplifyFactor:0,precision:6},initialize:function(c,d){if(a.Layers.FeatureGrid.prototype.initialize.call(this,d),d=d||{},d.url=a.Util.cleanUrl(c),d=L.setOptions(this,d),this._service=new a.Services.FeatureLayer(d),"*"!==this.options.fields[0]){for(var e=!1,f=0;f<this.options.fields.length;f++)this.options.fields[f].match(/^(OBJECTID|FID|OID|ID)$/i)&&(e=!0);e===!1&&console&&console.warn&&console.warn("no known esriFieldTypeOID field detected in fields Array.  Please add an attribute field containing unique IDs to ensure the layer can be drawn correctly.")}this._service.on("authenticationrequired requeststart requestend requesterror requestsuccess",function(a){a=L.extend({target:this},a),this.fire(a.type,a)},this),this.options.timeField.start&&this.options.timeField.end?(this._startTimeIndex=new b,this._endTimeIndex=new b):this.options.timeField&&(this._timeIndex=new b),this._cache={},this._currentSnapshot=[],this._activeRequests=0,this._pendingRequests=[]},onAdd:function(b){return a.Layers.FeatureGrid.prototype.onAdd.call(this,b)},onRemove:function(b){return a.Layers.FeatureGrid.prototype.onRemove.call(this,b)},getAttribution:function(){return this.options.attribution},createCell:function(a,b){this._requestFeatures(a,b)},_requestFeatures:function(b,c,d){return this._activeRequests++,1===this._activeRequests&&this.fire("loading",{bounds:b}),this._buildQuery(b).run(function(e,f,g){g&&g.exceededTransferLimit&&this.fire("drawlimitexceeded"),this._activeRequests--,!e&&f.features.length&&a.Util.requestAnimationFrame(L.Util.bind(function(){this._addFeatures(f.features,c)},this)),d&&d.call(this,e,f),this._activeRequests<=0&&this.fire("load",{bounds:b})},this)},_cacheKey:function(a){return a.z+":"+a.x+":"+a.y},_addFeatures:function(a,b){var c=this._cacheKey(b);this._cache[c]=this._cache[c]||[];for(var d=a.length-1;d>=0;d--){var e=a[d].id;this._currentSnapshot.push(e),this._cache[c].push(e)}this.options.timeField&&this._buildTimeIndexes(a);var f=this._map.getZoom();f>this.options.maxZoom||f<this.options.minZoom||this.createLayers(a)},_buildQuery:function(a){var b=this._service.query().intersects(a).where(this.options.where).fields(this.options.fields).precision(this.options.precision);return this.options.simplifyFactor&&b.simplify(this._map,this.options.simplifyFactor),"server"===this.options.timeFilterMode&&this.options.from&&this.options.to&&b.between(this.options.from,this.options.to),b},setWhere:function(b,c,d){this.options.where=b&&b.length?b:"1=1";for(var e=[],f=[],g=0,h=null,i=L.Util.bind(function(b,i){if(b&&(h=b),i)for(var j=i.features.length-1;j>=0;j--)f.push(i.features[j].id);g--,0>=g&&(this._currentSnapshot=f,a.Util.requestAnimationFrame(L.Util.bind(function(){this.removeLayers(e),this.addLayers(f),c&&c.call(d,h)},this)))},this),j=this._currentSnapshot.length-1;j>=0;j--)e.push(this._currentSnapshot[j]);for(var k in this._activeCells){g++;var l=this._keyToCellCoords(k),m=this._cellCoordsToBounds(l);this._requestFeatures(m,k,i)}return this},getWhere:function(){return this.options.where},getTimeRange:function(){return[this.options.from,this.options.to]},setTimeRange:function(a,b,c,d){var e=this.options.from,f=this.options.to,g=0,h=null,i=L.Util.bind(function(i){i&&(h=i),this._filterExistingFeatures(e,f,a,b),g--,c&&0>=g&&c.call(d,h)},this);if(this.options.from=a,this.options.to=b,this._filterExistingFeatures(e,f,a,b),"server"===this.options.timeFilterMode)for(var j in this._activeCells){g++;var k=this._keyToCellCoords(j),l=this._cellCoordsToBounds(k);this._requestFeatures(l,j,i)}},refresh:function(){for(var a in this._activeCells){var b=this._keyToCellCoords(a),c=this._cellCoordsToBounds(b);this._requestFeatures(c,a)}},_filterExistingFeatures:function(b,c,d,e){var f=b&&c?this._getFeaturesInTimeRange(b,c):this._currentSnapshot,g=this._getFeaturesInTimeRange(d,e);if(g.indexOf)for(var h=0;h<g.length;h++){var i=f.indexOf(g[h]);i>=0&&f.splice(i,1)}a.Util.requestAnimationFrame(L.Util.bind(function(){this.removeLayers(f),this.addLayers(g)},this))},_getFeaturesInTimeRange:function(a,b){var c,d=[];if(this.options.timeField.start&&this.options.timeField.end){var e=this._startTimeIndex.between(a,b),f=this._endTimeIndex.between(a,b);c=e.concat(f)}else c=this._timeIndex.between(a,b);for(var g=c.length-1;g>=0;g--)d.push(c[g].id);return d},_buildTimeIndexes:function(a){var b,c;if(this.options.timeField.start&&this.options.timeField.end){var d=[],e=[];for(b=a.length-1;b>=0;b--)c=a[b],d.push({id:c.id,value:new Date(c.properties[this.options.timeField.start])}),e.push({id:c.id,value:new Date(c.properties[this.options.timeField.end])});this._startTimeIndex.bulkAdd(d),this._endTimeIndex.bulkAdd(e)}else{var f=[];for(b=a.length-1;b>=0;b--)c=a[b],f.push({id:c.id,value:new Date(c.properties[this.options.timeField])});this._timeIndex.bulkAdd(f)}},_featureWithinTimeRange:function(a){if(!this.options.from||!this.options.to)return!0;var b=+this.options.from.valueOf(),c=+this.options.to.valueOf();if("string"==typeof this.options.timeField){var d=+a.properties[this.options.timeField];return d>=b&&c>=d}if(this.options.timeField.start&&this.options.timeField.end){var e=+a.properties[this.options.timeField.start],f=+a.properties[this.options.timeField.end];return e>=b&&c>=e||f>=b&&c>=f}},authenticate:function(a){return this._service.authenticate(a),this},metadata:function(a,b){return this._service.metadata(a,b),this},query:function(){return this._service.query()},addFeature:function(a,b,c){return this._service.addFeature(a,function(a,d){a||this.refresh(),b&&b.call(c,a,d)},this),this},updateFeature:function(a,b,c){return this._service.updateFeature(a,function(a,d){a||this.refresh(),b&&b.call(c,a,d)},this)},deleteFeature:function(a,b,c){return this._service.deleteFeature(a,function(a,d){!a&&d.objectId&&this.removeLayers([d.objectId],!0),b&&b.call(c,a,d)},this)}}),b.prototype._query=function(a){for(var b,c,d,e=0,f=this.values.length-1;f>=e;)if(d=b=(e+f)/2|0,c=this.values[Math.round(b)],+c.value<+a)e=b+1;else{if(!(+c.value>+a))return b;f=b-1}return~f},b.prototype.sort=function(){this.values.sort(function(a,b){return+b.value-+a.value}).reverse(),this.dirty=!1},b.prototype.between=function(a,b){this.dirty&&this.sort();var c=this._query(a),d=this._query(b);return 0===c&&0===d?[]:(c=Math.abs(c),d=0>d?Math.abs(d):d+1,this.values.slice(c,d))},b.prototype.bulkAdd=function(a){this.dirty=!0,this.values=this.values.concat(a)}}(EsriLeaflet),EsriLeaflet.Layers.FeatureLayer=EsriLeaflet.Layers.FeatureManager.extend({statics:{EVENTS:"click dblclick mouseover mouseout mousemove contextmenu popupopen popupclose"},options:{cacheLayers:!0},initialize:function(a,b){EsriLeaflet.Layers.FeatureManager.prototype.initialize.call(this,a,b),b=L.setOptions(this,b),this._layers={},this._leafletIds={},this._key="c"+(1e9*Math.random()).toString(36).replace(".","_")},onAdd:function(a){return a.on("zoomstart zoomend",function(a){this._zooming="zoomstart"===a.type},this),EsriLeaflet.Layers.FeatureManager.prototype.onAdd.call(this,a)},onRemove:function(a){for(var b in this._layers)a.removeLayer(this._layers[b]);return EsriLeaflet.Layers.FeatureManager.prototype.onRemove.call(this,a)},createNewLayer:function(a){return L.GeoJSON.geometryToLayer(a,this.options.pointToLayer,L.GeoJSON.coordsToLatLng,this.options)},createLayers:function(a){for(var b=a.length-1;b>=0;b--){var c,d=a[b],e=this._layers[d.id];if(e&&!this._map.hasLayer(e)&&this._map.addLayer(e),e&&e.setLatLngs){var f=this.createNewLayer(d);e.setLatLngs(f.getLatLngs())}e||(c=this.createNewLayer(d),c.feature=d,c.defaultOptions=c.options,c._leaflet_id=this._key+"_"+d.id,this._leafletIds[c._leaflet_id]=d.id,c.on(EsriLeaflet.Layers.FeatureLayer.EVENTS,this._propagateEvent,this),this._popup&&c.bindPopup&&c.bindPopup(this._popup(c.feature,c),this._popupOptions),this.options.onEachFeature&&this.options.onEachFeature(c.feature,c),this._layers[c.feature.id]=c,this.resetStyle(c.feature.id),this.fire("createfeature",{feature:c.feature}),(!this.options.timeField||this.options.timeField&&this._featureWithinTimeRange(d))&&this._map.addLayer(c))}},addLayers:function(a){for(var b=a.length-1;b>=0;b--){var c=this._layers[a[b]];c&&(this.fire("addfeature",{feature:c.feature}),this._map.addLayer(c))}},removeLayers:function(a,b){for(var c=a.length-1;c>=0;c--){var d=a[c],e=this._layers[d];e&&(this.fire("removefeature",{feature:e.feature,permanent:b}),this._map.removeLayer(e)),e&&b&&delete this._layers[d]}},cellEnter:function(a,b){this._zooming||EsriLeaflet.Util.requestAnimationFrame(L.Util.bind(function(){var a=this._cacheKey(b),c=this._cellCoordsToKey(b),d=this._cache[a];this._activeCells[c]&&d&&this.addLayers(d)},this))},cellLeave:function(a,b){this._zooming||EsriLeaflet.Util.requestAnimationFrame(L.Util.bind(function(){var a=this._cacheKey(b),c=this._cellCoordsToKey(b),d=this._cache[a],e=this._map.getBounds();if(!this._activeCells[c]&&d){for(var f=!0,g=0;g<d.length;g++){var h=this._layers[d[g]];h&&h.getBounds&&e.intersects(h.getBounds())&&(f=!1)}f&&this.removeLayers(d,!this.options.cacheLayers),!this.options.cacheLayers&&f&&(delete this._cache[a],delete this._cells[c],delete this._activeCells[c])}},this))},resetStyle:function(a){var b=this._layers[a];return b&&(b.options=b.defaultOptions,this.setFeatureStyle(b.feature.id,this.options.style)),this},setStyle:function(a){return this.options.style=a,this.eachFeature(function(b){this.setFeatureStyle(b.feature.id,a)},this),this},setFeatureStyle:function(a,b){var c=this._layers[a];if("function"==typeof b)b=b(c.feature);else if(!b&&!c.defaultOptions){{new L.Path}b=L.Path.prototype.options,b.fill=!0}c.setStyle&&c.setStyle(b)},bindPopup:function(a,b){this._popup=a,this._popupOptions=b;for(var c in this._layers){var d=this._layers[c],e=this._popup(d.feature,d);d.bindPopup(e,b)}return this},unbindPopup:function(){this._popup=!1;for(var a in this._layers){var b=this._layers[a];if(b.unbindPopup)b.unbindPopup();else if(b.getLayers){var c=b.getLayers();for(var d in c){var e=c[d];e.unbindPopup()}}}return this},eachFeature:function(a,b){for(var c in this._layers)a.call(b,this._layers[c]);return this},getFeature:function(a){return this._layers[a]},_propagateEvent:function(a){a.layer=this._layers[this._leafletIds[a.target._leaflet_id]],a.target=this,this.fire(a.type,a)}}),EsriLeaflet.FeatureLayer=EsriLeaflet.Layers.FeatureLayer,EsriLeaflet.Layers.featureLayer=function(a,b){return new EsriLeaflet.Layers.FeatureLayer(a,b)},EsriLeaflet.featureLayer=function(a,b){return new EsriLeaflet.Layers.FeatureLayer(a,b)},EsriLeaflet.Controls.Logo=L.Control.extend({options:{position:"bottomright",marginTop:0,marginLeft:0,marginBottom:0,marginRight:0},onAdd:function(){var a=L.DomUtil.create("div","esri-leaflet-logo");return a.style.marginTop=this.options.marginTop,a.style.marginLeft=this.options.marginLeft,a.style.marginBottom=this.options.marginBottom,a.style.marginRight=this.options.marginRight,a.innerHTML='<a href="https://developers.arcgis.com" style="border: none;"><img src="https://js.arcgis.com/3.11/esri/images/map/logo-med.png" style="border: none;"></a>',a}}),EsriLeaflet.Controls.logo=function(a){return new L.esri.Controls.Logo(a)};
//# sourceMappingURL=esri-leaflet.js.map

  return EsriLeaflet;
}));


window.google = window.google || {};
google.maps = google.maps || {};
(function() {
  
  function getScript(src) {
    document.write('<' + 'script src="' + src + '"><' + '/script>');
  }
  
  var modules = google.maps.modules = {};
  google.maps.__gjsload__ = function(name, text) {
    modules[name] = text;
  };
  
  google.maps.Load = function(apiLoad) {
    delete google.maps.Load;
    apiLoad([0.009999999776482582,[[["http://mt0.googleapis.com/maps/vt?lyrs=m@339000000\u0026src=api\u0026hl=en-US\u0026","http://mt1.googleapis.com/maps/vt?lyrs=m@339000000\u0026src=api\u0026hl=en-US\u0026"],null,null,null,null,"m@339000000",["https://mts0.google.com/maps/vt?lyrs=m@339000000\u0026src=api\u0026hl=en-US\u0026","https://mts1.google.com/maps/vt?lyrs=m@339000000\u0026src=api\u0026hl=en-US\u0026"]],[["http://khm0.googleapis.com/kh?v=198\u0026hl=en-US\u0026","http://khm1.googleapis.com/kh?v=198\u0026hl=en-US\u0026"],null,null,null,1,"198",["https://khms0.google.com/kh?v=198\u0026hl=en-US\u0026","https://khms1.google.com/kh?v=198\u0026hl=en-US\u0026"]],null,[["http://mt0.googleapis.com/maps/vt?lyrs=t@132,r@339000000\u0026src=api\u0026hl=en-US\u0026","http://mt1.googleapis.com/maps/vt?lyrs=t@132,r@339000000\u0026src=api\u0026hl=en-US\u0026"],null,null,null,null,"t@132,r@339000000",["https://mts0.google.com/maps/vt?lyrs=t@132,r@339000000\u0026src=api\u0026hl=en-US\u0026","https://mts1.google.com/maps/vt?lyrs=t@132,r@339000000\u0026src=api\u0026hl=en-US\u0026"]],null,null,[["http://cbk0.googleapis.com/cbk?","http://cbk1.googleapis.com/cbk?"]],[["http://khm0.googleapis.com/kh?v=96\u0026hl=en-US\u0026","http://khm1.googleapis.com/kh?v=96\u0026hl=en-US\u0026"],null,null,null,null,"96",["https://khms0.google.com/kh?v=96\u0026hl=en-US\u0026","https://khms1.google.com/kh?v=96\u0026hl=en-US\u0026"]],[["http://mt0.googleapis.com/mapslt?hl=en-US\u0026","http://mt1.googleapis.com/mapslt?hl=en-US\u0026"]],[["http://mt0.googleapis.com/mapslt/ft?hl=en-US\u0026","http://mt1.googleapis.com/mapslt/ft?hl=en-US\u0026"]],[["http://mt0.googleapis.com/maps/vt?hl=en-US\u0026","http://mt1.googleapis.com/maps/vt?hl=en-US\u0026"]],[["http://mt0.googleapis.com/mapslt/loom?hl=en-US\u0026","http://mt1.googleapis.com/mapslt/loom?hl=en-US\u0026"]],[["https://mts0.googleapis.com/mapslt?hl=en-US\u0026","https://mts1.googleapis.com/mapslt?hl=en-US\u0026"]],[["https://mts0.googleapis.com/mapslt/ft?hl=en-US\u0026","https://mts1.googleapis.com/mapslt/ft?hl=en-US\u0026"]],[["https://mts0.googleapis.com/mapslt/loom?hl=en-US\u0026","https://mts1.googleapis.com/mapslt/loom?hl=en-US\u0026"]]],["en-US","US",null,0,null,null,"http://maps.gstatic.com/mapfiles/","http://csi.gstatic.com","https://maps.googleapis.com","http://maps.googleapis.com",null,"https://maps.google.com","https://gg.google.com","http://maps.gstatic.com/maps-api-v3/api/images/","https://www.google.com/maps",0,"https://www.google.com"],["http://maps.google.com/maps-api-v3/api/js/23/7","3.23.7"],[3801039637],1,null,null,null,null,null,"",null,null,0,"http://khm.googleapis.com/mz?v=198\u0026",null,"https://earthbuilder.googleapis.com","https://earthbuilder.googleapis.com",null,"http://mt.googleapis.com/maps/vt/icon",[["http://maps.google.com/maps/vt"],["https://maps.google.com/maps/vt"],null,null,null,null,null,null,null,null,null,null,["https://www.google.com/maps/vt"],"/maps/vt",339000000,132],2,500,[null,"http://g0.gstatic.com/landmark/tour","http://g0.gstatic.com/landmark/config",null,"http://www.google.com/maps/preview/log204","","http://static.panoramio.com.storage.googleapis.com/photos/",["http://geo0.ggpht.com/cbk","http://geo1.ggpht.com/cbk","http://geo2.ggpht.com/cbk","http://geo3.ggpht.com/cbk"],"https://maps.googleapis.com/maps/api/js/GeoPhotoService.GetMetadata","https://maps.googleapis.com/maps/api/js/GeoPhotoService.SingleImageSearch",["http://lh3.ggpht.com/","http://lh4.ggpht.com/","http://lh5.ggpht.com/","http://lh6.ggpht.com/"]],["https://www.google.com/maps/api/js/master?pb=!1m2!1u23!2s7!2sen-US!3sUS!4s23/7","https://www.google.com/maps/api/js/widget?pb=!1m2!1u23!2s7!2sen-US"],null,0,null,"/maps/api/js/ApplicationService.GetEntityDetails",0,null,null,null,null,[],["23.7"]], loadScriptTime);
  };
  var loadScriptTime = (new Date).getTime();
})();
// inlined
(function(_){'use strict';var Da,Ea,Qa,$a,fb,gb,hb,ib,nb,ob,rb,ub,qb,vb,wb,Ab,Hb,Lb,Pb,Yb,ac,dc,ec,gc,ic,mc,fc,hc,oc,rc,sc,wc,Hc,Ic,Mc,Nc,Oc,Pc,Rc,Tc,Vc,Xc,Wc,cd,ed,jd,kd,pd,wd,xd,yd,Nd,Od,Qd,Td,Vd,Ud,Wd,ce,de,ge,ke,me,ne,oe,re,te,ue,ve,we,xe,ye,ze,Ae,De,Ee,Ne,Oe,Pe,Qe,Re,Ye,Ze,$e,cf,ff,Be,mf,of,rf,uf,Ef,Gf,Jf,Kf,Lf,Mf,Of,Pf,Qf,Vf,Xf,fg,gg,lg,og,pg,tg,wg,xg,Eg,Fg,Ig,Jg,Kg,Lg,Mg,Aa,Ba;_.aa="ERROR";_.ba="INVALID_REQUEST";_.ca="MAX_DIMENSIONS_EXCEEDED";_.da="MAX_ELEMENTS_EXCEEDED";_.ea="MAX_WAYPOINTS_EXCEEDED";
_.ga="NOT_FOUND";_.ha="OK";_.ia="OVER_QUERY_LIMIT";_.ja="REQUEST_DENIED";_.ka="UNKNOWN_ERROR";_.la="ZERO_RESULTS";_.ma=function(){return function(a){return a}};_.k=function(){return function(){}};_.na=function(a){return function(b){this[a]=b}};_.m=function(a){return function(){return this[a]}};_.oa=function(a){return function(){return a}};_.qa=function(a){return function(){return _.pa[a].apply(this,arguments)}};_.ra=function(a){return void 0!==a};_.ta=_.k();
_.ua=function(a){a.Mc=function(){return a.Ob?a.Ob:a.Ob=new a}};
_.va=function(a){var b=typeof a;if("object"==b)if(a){if(a instanceof Array)return"array";if(a instanceof Object)return b;var c=Object.prototype.toString.call(a);if("[object Window]"==c)return"object";if("[object Array]"==c||"number"==typeof a.length&&"undefined"!=typeof a.splice&&"undefined"!=typeof a.propertyIsEnumerable&&!a.propertyIsEnumerable("splice"))return"array";if("[object Function]"==c||"undefined"!=typeof a.call&&"undefined"!=typeof a.propertyIsEnumerable&&!a.propertyIsEnumerable("call"))return"function"}else return"null";
else if("function"==b&&"undefined"==typeof a.call)return"object";return b};_.wa=function(a){var b=_.va(a);return"array"==b||"object"==b&&"number"==typeof a.length};_.xa=function(a){return"string"==typeof a};_.ya=function(a){return"number"==typeof a};_.za=function(a){var b=typeof a;return"object"==b&&null!=a||"function"==b};_.Ca=function(a){return a[Aa]||(a[Aa]=++Ba)};Da=function(a,b,c){return a.call.apply(a.bind,arguments)};
Ea=function(a,b,c){if(!a)throw Error();if(2<arguments.length){var d=Array.prototype.slice.call(arguments,2);return function(){var c=Array.prototype.slice.call(arguments);Array.prototype.unshift.apply(c,d);return a.apply(b,c)}}return function(){return a.apply(b,arguments)}};_.u=function(a,b,c){_.u=Function.prototype.bind&&-1!=Function.prototype.bind.toString().indexOf("native code")?Da:Ea;return _.u.apply(null,arguments)};_.Fa=function(){return+new Date};
_.v=function(a,b){function c(){}c.prototype=b.prototype;a.qd=b.prototype;a.prototype=new c;a.prototype.constructor=a;a.Kr=function(a,c,f){for(var g=Array(arguments.length-2),h=2;h<arguments.length;h++)g[h-2]=arguments[h];return b.prototype[c].apply(a,g)}};_.x=function(a){return a?a.length:0};_.Ga=function(a,b){return function(c){return b(a(c))}};_.Ia=function(a,b){_.Ha(b,function(c){a[c]=b[c]})};_.Ja=function(a){for(var b in a)return!1;return!0};
_.Ka=function(a,b,c){null!=b&&(a=Math.max(a,b));null!=c&&(a=Math.min(a,c));return a};_.La=function(a,b,c){c=c-b;return((a-b)%c+c)%c+b};_.Ma=function(a,b,c){return Math.abs(a-b)<=(c||1E-9)};_.Na=function(a,b){for(var c=[],d=_.x(a),e=0;e<d;++e)c.push(b(a[e],e));return c};_.Pa=function(a,b){for(var c=_.Oa(void 0,_.x(b)),d=_.Oa(void 0,0);d<c;++d)a.push(b[d])};Qa=function(a){return null==a};_.B=function(a){return"undefined"!=typeof a};_.D=function(a){return"number"==typeof a};
_.Ra=function(a){return"object"==typeof a};_.Oa=function(a,b){return null==a?b:a};_.Sa=function(a){return"string"==typeof a};_.Ua=function(a){return a===!!a};_.G=function(a,b){for(var c=0,d=_.x(a);c<d;++c)b(a[c],c)};_.Ha=function(a,b){for(var c in a)b(c,a[c])};_.Wa=function(a,b,c){var d=_.Va(arguments,2);return function(){return b.apply(a,d)}};_.Va=function(a,b,c){return Function.prototype.call.apply(Array.prototype.slice,arguments)};
_.Xa=function(a){return null!=a&&"object"==typeof a&&"number"==typeof a.length};_.Za=function(a){return function(){var b=this,c=arguments;_.Ya(function(){a.apply(b,c)})}};_.Ya=function(a){return window.setTimeout(a,0)};$a=function(a,b){if(Object.prototype.hasOwnProperty.call(a,b))return a[b]};_.ab=function(a){window.console&&window.console.error&&window.console.error(a)};_.db=function(a){a=a||window.event;_.bb(a);_.cb(a)};_.bb=function(a){a.cancelBubble=!0;a.stopPropagation&&a.stopPropagation()};
_.cb=function(a){a.preventDefault&&_.B(a.defaultPrevented)?a.preventDefault():a.returnValue=!1};_.eb=function(a){a.handled=!0;_.B(a.bubbles)||(a.returnValue="handled")};fb=function(a,b){a.__e3_||(a.__e3_={});var c=a.__e3_;c[b]||(c[b]={});return c[b]};gb=function(a,b){var c,d=a.__e3_||{};if(b)c=d[b]||{};else{c={};for(var e in d)_.Ia(c,d[e])}return c};hb=function(a,b){return function(c){return b.call(a,c,this)}};
ib=function(a,b,c){return function(d){var e=[b,a];_.Pa(e,arguments);_.I.trigger.apply(this,e);c&&_.eb.apply(null,arguments)}};nb=function(a,b,c,d){this.Ob=a;this.R=b;this.j=c;this.S=null;this.U=d;this.id=++jb;fb(a,b)[this.id]=this;lb&&"tagName"in a&&(mb[this.id]=this)};
ob=function(a){return a.S=function(b){b||(b=window.event);if(b&&!b.target)try{b.target=b.srcElement}catch(d){}var c;c=a.j.apply(a.Ob,[b]);return b&&"click"==b.type&&(b=b.srcElement)&&"A"==b.tagName&&"javascript:void(0)"==b.href?!1:c}};_.pb=function(a){return""+(_.za(a)?_.Ca(a):a)};_.J=_.k();rb=function(a,b){var c=b+"_changed";if(a[c])a[c]();else a.changed(b);var c=qb(a,b),d;for(d in c){var e=c[d];rb(e.Rd,e.jc)}_.I.trigger(a,b.toLowerCase()+"_changed")};
_.tb=function(a){return sb[a]||(sb[a]=a.substr(0,1).toUpperCase()+a.substr(1))};ub=function(a){a.gm_accessors_||(a.gm_accessors_={});return a.gm_accessors_};qb=function(a,b){a.gm_bindings_||(a.gm_bindings_={});a.gm_bindings_.hasOwnProperty(b)||(a.gm_bindings_[b]={});return a.gm_bindings_[b]};vb=_.k();wb=function(a){this.message=a;this.name="InvalidValueError";this.stack=Error().stack};_.xb=function(a,b){var c="";if(null!=b){if(!(b instanceof wb))return b;c=": "+b.message}return new wb(a+c)};
_.yb=function(a){if(!(a instanceof wb))throw a;_.ab(a.name+": "+a.message)};_.zb=function(a,b){return function(c){if(!c||!_.Ra(c))throw _.xb("not an Object");var d={},e;for(e in c)if(d[e]=c[e],!b&&!a[e])throw _.xb("unknown property "+e);for(e in a)try{var f=a[e](d[e]);if(_.B(f)||Object.prototype.hasOwnProperty.call(c,e))d[e]=a[e](d[e])}catch(g){throw _.xb("in property "+e,g);}return d}};Ab=function(a){try{return!!a.cloneNode}catch(b){return!1}};
_.Bb=function(a,b,c){return c?function(c){if(c instanceof a)return c;try{return new a(c)}catch(e){throw _.xb("when calling new "+b,e);}}:function(c){if(c instanceof a)return c;throw _.xb("not an instance of "+b);}};_.Cb=function(a){return function(b){for(var c in a)if(a[c]==b)return b;throw _.xb(b);}};_.Db=function(a){return function(b){if(!_.Xa(b))throw _.xb("not an Array");return _.Na(b,function(b,d){try{return a(b)}catch(e){throw _.xb("at index "+d,e);}})}};
_.Eb=function(a,b){return function(c){if(a(c))return c;throw _.xb(b||""+c);}};_.Fb=function(a){var b=arguments;return function(a){for(var d=[],e=0,f=b.length;e<f;++e){var g=b[e];try{(g.Yh||g)(a)}catch(h){if(!(h instanceof wb))throw h;d.push(h.message);continue}return(g.then||g)(a)}throw _.xb(d.join("; and "));}};_.Gb=function(a){return function(b){return null==b?b:a(b)}};Hb=function(a){return function(b){if(b&&null!=b[a])return b;throw _.xb("no "+a+" property");}};
_.Ib=function(a){return a.replace(/^[\s\xa0]+|[\s\xa0]+$/g,"")};_.Kb=function(){return-1!=_.Jb.toLowerCase().indexOf("webkit")};
_.Mb=function(a,b){for(var c=0,d=_.Ib(String(a)).split("."),e=_.Ib(String(b)).split("."),f=Math.max(d.length,e.length),g=0;0==c&&g<f;g++){var h=d[g]||"",l=e[g]||"",n=/(\d*)(\D*)/g,p=/(\d*)(\D*)/g;do{var q=n.exec(h)||["","",""],r=p.exec(l)||["","",""];if(0==q[0].length&&0==r[0].length)break;c=Lb(0==q[1].length?0:(0,window.parseInt)(q[1],10),0==r[1].length?0:(0,window.parseInt)(r[1],10))||Lb(0==q[2].length,0==r[2].length)||Lb(q[2],r[2])}while(0==c)}return c};Lb=function(a,b){return a<b?-1:a>b?1:0};
_.Nb=function(a,b,c){c=null==c?0:0>c?Math.max(0,a.length+c):c;if(_.xa(a))return _.xa(b)&&1==b.length?a.indexOf(b,c):-1;for(;c<a.length;c++)if(c in a&&a[c]===b)return c;return-1};_.Ob=function(a,b,c){for(var d=a.length,e=_.xa(a)?a.split(""):a,f=0;f<d;f++)f in e&&b.call(c,e[f],f,a)};Pb=function(a,b){for(var c=a.length,d=_.xa(a)?a.split(""):a,e=0;e<c;e++)if(e in d&&b.call(void 0,d[e],e,a))return e;return-1};_.Rb=function(a,b){var c=_.Nb(a,b),d;(d=0<=c)&&_.Qb(a,c);return d};
_.Qb=function(a,b){Array.prototype.splice.call(a,b,1)};_.Sb=function(a){return a*Math.PI/180};_.Ub=function(a){return 180*a/Math.PI};_.K=function(a,b,c){if(a&&(a.lat||a.lng))try{Vb(a),b=a.lng,a=a.lat,c=!1}catch(d){_.yb(d)}a-=0;b-=0;c||(a=_.Ka(a,-90,90),180!=b&&(b=_.La(b,-180,180)));this.lat=function(){return a};this.lng=function(){return b}};_.Wb=function(a){return _.Sb(a.lat())};_.Xb=function(a){return _.Sb(a.lng())};Yb=function(a,b){var c=Math.pow(10,b);return Math.round(a*c)/c};
_.Zb=function(a){try{if(a instanceof _.K)return a;a=Vb(a);return new _.K(a.lat,a.lng)}catch(b){throw _.xb("not a LatLng or LatLngLiteral",b);}};_.$b=function(a){this.j=_.Zb(a)};ac=function(a){if(a instanceof vb)return a;try{return new _.$b(_.Zb(a))}catch(b){}throw _.xb("not a Geometry or LatLng or LatLngLiteral object");};_.bc=function(a,b){if(a)return function(){--a||b()};b();return _.ta};
_.cc=function(a,b,c){var d=a.getElementsByTagName("head")[0];a=a.createElement("script");a.type="text/javascript";a.charset="UTF-8";a.src=b;c&&(a.onerror=c);d.appendChild(a);return a};dc=function(a){for(var b="",c=0,d=arguments.length;c<d;++c){var e=arguments[c];e.length&&"/"==e[0]?b=e:(b&&"/"!=b[b.length-1]&&(b+="/"),b+=e)}return b};ec=function(a){this.S=window.document;this.j={};this.R=a};gc=function(){this.U={};this.R={};this.V={};this.j={};this.S=new fc};
ic=function(a,b){a.U[b]||(a.U[b]=!0,hc(a.S,function(c){for(var d=c.Dj[b],e=d?d.length:0,f=0;f<e;++f){var g=d[f];a.j[g]||ic(a,g)}c=c.Qo;c.j[b]||_.cc(c.S,dc(c.R,b)+".js")}))};mc=function(a,b){var c=lc;this.Qo=a;this.Dj=c;var d={},e;for(e in c)for(var f=c[e],g=0,h=f.length;g<h;++g){var l=f[g];d[l]||(d[l]=[]);d[l].push(e)}this.hq=d;this.mn=b};fc=function(){this.j=[]};hc=function(a,b){a.R?b(a.R):a.j.push(b)};
_.L=function(a,b,c){var d=gc.Mc();a=""+a;d.j[a]?b(d.j[a]):((d.R[a]=d.R[a]||[]).push(b),c||ic(d,a))};_.nc=function(a,b){gc.Mc().j[""+a]=b};oc=function(a,b,c){var d=[],e=_.bc(a.length,function(){b.apply(null,d)});_.Ob(a,function(a,b){_.L(a,function(a){d[b]=a;e()},c)})};_.pc=function(a){a=a||{};this.S=a.id;this.j=null;try{this.j=a.geometry?ac(a.geometry):null}catch(b){_.yb(b)}this.R=a.properties||{}};_.M=function(a,b){this.x=a;this.y=b};
rc=function(a){if(a instanceof _.M)return a;try{_.zb({x:_.qc,y:_.qc},!0)(a)}catch(b){throw _.xb("not a Point",b);}return new _.M(a.x,a.y)};_.N=function(a,b,c,d){this.width=a;this.height=b;this.ma=c||"px";this.W=d||"px"};sc=function(a){if(a instanceof _.N)return a;try{_.zb({height:_.qc,width:_.qc},!0)(a)}catch(b){throw _.xb("not a Size",b);}return new _.N(a.width,a.height)};_.O=function(a){return function(){return this.get(a)}};
_.tc=function(a,b){return b?function(c){try{this.set(a,b(c))}catch(d){_.yb(_.xb("set"+_.tb(a),d))}}:function(b){this.set(a,b)}};_.vc=function(a,b){_.Ha(b,function(b,d){var e=_.O(b);a["get"+_.tb(b)]=e;d&&(e=_.tc(b,d),a["set"+_.tb(b)]=e)})};_.xc=function(a){this.j=a||[];wc(this)};wc=function(a){a.set("length",a.j.length)};_.yc=function(a){this.S=a||_.pb;this.R={}};_.zc=function(a,b){var c=a.R,d=a.S(b);c[d]||(c[d]=b,_.I.trigger(a,"insert",b),a.j&&a.j(b))};
_.Ac=function(a,b,c){this.heading=a;this.pitch=_.Ka(b,-90,90);this.zoom=Math.max(0,c)};_.Bc=function(){this.__gm=new _.J;this.S=null};_.Cc=_.ma();_.Dc=function(a,b,c){for(var d in a)b.call(c,a[d],d,a)};_.Ec=function(a){return-1!=_.Jb.indexOf(a)};_.Fc=function(){return _.Ec("Opera")||_.Ec("OPR")};_.Gc=function(){return _.Ec("Trident")||_.Ec("MSIE")};Hc=function(){return(_.Ec("Chrome")||_.Ec("CriOS"))&&!_.Fc()&&!_.Ec("Edge")};Ic=function(a,b,c){this.U=c;this.S=a;this.V=b;this.R=0;this.j=null};
Mc=function(){this.R=this.j=null};Nc=function(){this.next=this.j=this.Md=null};Oc=function(a,b){return function(c){return c.Md==a&&c.context==(b||null)}};Pc=function(a){this.Ea=[];this.j=a&&a.Fe||_.ta;this.R=a&&a.He||_.ta};_.Qc=function(){this.Ea=new Pc({Fe:(0,_.u)(this.Fe,this),He:(0,_.u)(this.He,this)})};Rc=_.k();Tc=function(a){var b=a;if(a instanceof Array)b=Array(a.length),_.Sc(b,a);else if(a instanceof Object){var c=b={},d;for(d in a)a.hasOwnProperty(d)&&(c[d]=Tc(a[d]))}return b};
_.Sc=function(a,b){for(var c=0;c<b.length;++c)b.hasOwnProperty(c)&&(a[c]=Tc(b[c]))};_.P=function(a,b){a[b]||(a[b]=[]);return a[b]};_.Uc=function(a,b){return a[b]?a[b].length:0};Vc=_.k();Xc=function(a,b,c){for(var d=1;d<b.ra.length;++d){var e=b.ra[d],f=a[d+b.qa];if(null!=f&&e)if(3==e.label)for(var g=0;g<f.length;++g)Wc(f[g],d,e,c);else Wc(f,d,e,c)}};
Wc=function(a,b,c,d){if("m"==c.type){var e=d.length;Xc(a,c.ka,d);d.splice(e,0,[b,"m",d.length-e].join(""))}else"b"==c.type&&(a=a?"1":"0"),d.push([b,c.type,(0,window.encodeURIComponent)(a)].join(""))};_.Yc=function(){return _.Ec("iPhone")&&!_.Ec("iPod")&&!_.Ec("iPad")};cd=function(){var a=_.Jb;if(_.Zc)return/rv\:([^\);]+)(\)|;)/.exec(a);if(_.$c)return/Edge\/([\d\.]+)/.exec(a);if(_.ad)return/\b(?:MSIE|rv)[: ]([^\);]+)(\)|;)/.exec(a);if(_.bd)return/WebKit\/(\S+)/.exec(a)};
ed=function(){var a=_.dd.document;return a?a.documentMode:void 0};_.hd=function(a){return fd[a]||(fd[a]=0<=_.Mb(_.gd,a))};_.id=function(a,b){this.j=a||0;this.R=b||0};jd=_.k();kd=function(a,b){-180==a&&180!=b&&(a=180);-180==b&&180!=a&&(b=180);this.j=a;this.R=b};_.ld=function(a){return a.j>a.R};_.nd=function(a,b){return 1E-9>=Math.abs(b.j-a.j)%360+Math.abs(_.md(b)-_.md(a))};_.od=function(a,b){var c=b-a;return 0<=c?c:b+180-(a-180)};_.md=function(a){return a.isEmpty()?0:_.ld(a)?360-(a.j-a.R):a.R-a.j};
pd=function(a,b){this.R=a;this.j=b};_.qd=function(a){return a.isEmpty()?0:a.j-a.R};_.rd=function(a,b){a=a&&_.Zb(a);b=b&&_.Zb(b);if(a){b=b||a;var c=_.Ka(a.lat(),-90,90),d=_.Ka(b.lat(),-90,90);this.R=new pd(c,d);c=a.lng();d=b.lng();360<=d-c?this.j=new kd(-180,180):(c=_.La(c,-180,180),d=_.La(d,-180,180),this.j=new kd(c,d))}else this.R=new pd(1,-1),this.j=new kd(180,-180)};_.sd=function(a,b,c,d){return new _.rd(new _.K(a,b,!0),new _.K(c,d,!0))};
_.ud=function(a){if(a instanceof _.rd)return a;try{return a=td(a),_.sd(a.south,a.west,a.north,a.east)}catch(b){throw _.xb("not a LatLngBounds or LatLngBoundsLiteral",b);}};_.vd=_.na("__gm");wd=function(){this.j={};this.S={};this.R={}};xd=function(){this.j={}};yd=function(a){this.j=new xd;var b=this;_.I.addListenerOnce(a,"addfeature",function(){_.L("data",function(c){c.j(b,a,b.j)})})};_.Cd=function(a){this.j=[];try{this.j=zd(a)}catch(b){_.yb(b)}};_.Ed=function(a){this.j=(0,_.Dd)(a)};
_.Gd=function(a){this.j=Fd(a)};_.Hd=function(a){this.j=(0,_.Dd)(a)};_.Id=function(a){this.j=(0,_.Dd)(a)};_.Kd=function(a){this.j=Jd(a)};_.Md=function(a){this.j=Ld(a)};Nd=function(a){a=a||{};a.clickable=_.Oa(a.clickable,!0);a.visible=_.Oa(a.visible,!0);this.setValues(a);_.L("marker",_.ta)};Od=function(a){var b=_,c=gc.Mc().S;a=c.R=new mc(new ec(a),b);for(var b=0,d=c.j.length;b<d;++b)c.j[b](a);c.j.length=0};_.Pd=function(a){this.__gm={set:null,Ff:null};Nd.call(this,a)};
Qd=function(a){a=a||{};a.visible=_.Oa(a.visible,!0);return a};_.Rd=function(a){return a&&a.radius||6378137};Td=function(a){return a instanceof _.xc?Sd(a):new _.xc((0,_.Dd)(a))};Vd=function(a){var b;_.Xa(a)?0==_.x(a)?b=!0:(b=a instanceof _.xc?a.getAt(0):a[0],b=_.Xa(b)):b=!1;return b?a instanceof _.xc?Ud(Sd)(a):new _.xc(_.Db(Td)(a)):new _.xc([Td(a)])};
Ud=function(a){return function(b){if(!(b instanceof _.xc))throw _.xb("not an MVCArray");b.forEach(function(b,d){try{a(b)}catch(e){throw _.xb("at index "+d,e);}});return b}};Wd=function(a){this.set("latLngs",new _.xc([new _.xc]));this.setValues(Qd(a));_.L("poly",_.ta)};_.Zd=function(a){Wd.call(this,a)};_.$d=function(a){Wd.call(this,a)};
_.ae=function(a,b,c){function d(a){if(!a)throw _.xb("not a Feature");if("Feature"!=a.type)throw _.xb('type != "Feature"');var b=a.geometry;try{b=null==b?null:e(b)}catch(d){throw _.xb('in property "geometry"',d);}var f=a.properties||{};if(!_.Ra(f))throw _.xb("properties is not an Object");var g=c.idPropertyName;a=g?f[g]:a.id;if(null!=a&&!_.D(a)&&!_.Sa(a))throw _.xb((g||"id")+" is not a string or number");return{id:a,geometry:b,properties:f}}function e(a){if(null==a)throw _.xb("is null");var b=(a.type+
"").toLowerCase(),c=a.coordinates;try{switch(b){case "point":return new _.$b(h(c));case "multipoint":return new _.Hd(n(c));case "linestring":return g(c);case "multilinestring":return new _.Gd(p(c));case "polygon":return f(c);case "multipolygon":return new _.Md(r(c))}}catch(d){throw _.xb('in property "coordinates"',d);}if("geometrycollection"==b)try{return new _.Cd(w(a.geometries))}catch(d){throw _.xb('in property "geometries"',d);}throw _.xb("invalid type");}function f(a){return new _.Kd(q(a))}function g(a){return new _.Ed(n(a))}
function h(a){a=l(a);return _.Zb({lat:a[1],lng:a[0]})}if(!b)return[];c=c||{};var l=_.Db(_.qc),n=_.Db(h),p=_.Db(g),q=_.Db(function(a){a=n(a);if(!a.length)throw _.xb("contains no elements");if(!a[0].j(a[a.length-1]))throw _.xb("first and last positions are not equal");return new _.Id(a.slice(0,-1))}),r=_.Db(f),w=_.Db(e),y=_.Db(d);if("FeatureCollection"==b.type){b=b.features;try{return _.Na(y(b),function(b){return a.add(b)})}catch(z){throw _.xb('in property "features"',z);}}if("Feature"==b.type)return[a.add(d(b))];
throw _.xb("not a Feature or FeatureCollection");};ce=function(a){var b=this;this.setValues(a||{});this.j=new wd;_.I.forward(this.j,"addfeature",this);_.I.forward(this.j,"removefeature",this);_.I.forward(this.j,"setgeometry",this);_.I.forward(this.j,"setproperty",this);_.I.forward(this.j,"removeproperty",this);this.R=new yd(this.j);this.R.bindTo("map",this);this.R.bindTo("style",this);_.G(_.be,function(a){_.I.forward(b.R,a,b)});this.S=!1};de=function(a){a.S||(a.S=!0,_.L("drawing_impl",function(b){b.po(a)}))};
_.ee=function(a){this.j=a||[]};_.fe=function(a){this.j=a||[]};ge=function(a){this.j=a||[]};_.he=function(a){this.j=a||[]};_.ie=function(a){this.j=a||[]};_.je=function(a){function b(){d||(d=!0,_.L("infowindow",function(a){a.Om(c)}))}window.setTimeout(function(){_.L("infowindow",_.ta)},100);var c=this,d=!1;_.I.addListenerOnce(this,"anchor_changed",b);_.I.addListenerOnce(this,"map_changed",b);this.setValues(a)};ke=function(a){this.setValues(a)};_.le=_.k();me=_.k();ne=_.k();oe=_.k();
_.pe=function(){_.L("geocoder",_.ta)};_.qe=function(a,b,c){this.va=null;this.set("url",a);this.set("bounds",_.Gb(_.ud)(b));this.setValues(c)};re=function(a,b){_.Sa(a)?(this.set("url",a),this.setValues(b)):this.setValues(a)};_.se=function(){this.va=null;_.L("layers",_.ta)};te=function(){this.va=null;_.L("layers",_.ta)};ue=function(){this.va=null;_.L("layers",_.ta)};ve=function(a){this.j=a||[]};we=function(a){this.j=a||[]};xe=function(a){this.j=a||[]};ye=function(a){this.j=a||[]};
ze=function(a){this.j=a||[]};Ae=function(a){this.j=a||[]};De=function(){var a=Be().j[10];return a?new ze(a):Ce};Ee=function(){var a=De().j[8];return null!=a?a:0};_.Fe=function(a){this.j=a||[]};_.Ge=function(a){this.j=a||[]};_.He=function(a){this.j=a||[]};_.Ie=function(a){this.j=a||[]};Ne=function(a){this.j=a||[]};Oe=function(a){this.j=a||[]};Pe=function(a){this.j=a||[]};Qe=function(a){this.j=a||[]};Re=function(a){this.j=a||[]};_.Se=function(a){this.j=a||[]};_.Te=function(a){this.j=a||[]};
_.Ue=function(a){a=a.j[0];return null!=a?a:""};_.Ve=function(a){a=a.j[1];return null!=a?a:""};_.Xe=function(){var a=_.We(_.Q).j[9];return null!=a?a:""};Ye=function(){var a=_.We(_.Q).j[7];return null!=a?a:""};Ze=function(){var a=_.We(_.Q).j[12];return null!=a?a:""};$e=function(a){a=a.j[0];return null!=a?a:""};_.af=function(a){a=a.j[1];return null!=a?a:""};cf=function(){var a=_.Q.j[4],a=(a?new Pe(a):bf).j[0];return null!=a?a:0};_.df=function(){var a=_.Q.j[0];return null!=a?a:1};
_.ef=function(a){a=a.j[6];return null!=a?a:""};ff=function(){var a=_.Q.j[11];return null!=a?a:""};_.gf=function(){var a=_.Q.j[16];return null!=a?a:""};_.We=function(a){return(a=a.j[2])?new Ne(a):hf};_.kf=function(){var a=_.Q.j[3];return a?new Oe(a):jf};Be=function(){var a=_.Q.j[33];return a?new we(a):lf};mf=function(a){return _.P(_.Q.j,8)[a]};of=function(){var a=_.Q.j[36],a=(a?new Re(a):nf).j[0];return null!=a?a:""};
rf=function(a,b){_.Bc.call(this);this.__gm=new _.J;this.R=null;b&&b.client&&(this.R=_.pf[b.client]||null);var c=this.controls=[];_.Ha(_.qf,function(a,b){c[b]=new _.xc});this.U=!0;this.j=a;this.setPov(new _.Ac(0,0,1));b&&b.Dc&&!_.D(b.Dc.zoom)&&(b.Dc.zoom=_.D(b.zoom)?b.zoom:1);this.setValues(b);void 0==this.getVisible()&&this.setVisible(!0);this.__gm.Qd=b&&b.Qd||new _.yc;_.I.addListenerOnce(this,"pano_changed",_.Za(function(){_.L("marker",(0,_.u)(function(a){a.j(this.__gm.Qd,this)},this))}))};
_.tf=function(){this.U=[];this.R=this.j=this.S=null};uf=function(a,b,c){this.Ia=b;this.V=new _.yc;this.Ca=new _.xc;this.ma=new _.yc;this.ta=new _.yc;this.S=new _.yc;this.Qd=new _.yc;this.wa=[];var d=this.Qd;d.j=function(){delete d.j;_.L("marker",_.Za(function(b){b.j(d,a)}))};this.R=new rf(b,{visible:!1,enableCloseButton:!0,Qd:d});this.R.bindTo("reportErrorControl",a);this.R.U=!1;this.j=new _.tf;this.Ma=c};_.vf=function(){this.Ea=new Pc};
_.wf=function(){this.j=new _.M(128,128);this.S=256/360;this.U=256/(2*Math.PI);this.R=!0};_.xf=function(a){this.ya=this.Aa=window.Infinity;this.Fa=this.Da=-window.Infinity;_.G(a,(0,_.u)(this.extend,this))};_.yf=function(a,b,c,d){var e=new _.xf;e.Aa=a;e.ya=b;e.Da=c;e.Fa=d;return e};_.zf=function(a,b,c){if(a=a.fromLatLngToPoint(b))c=Math.pow(2,c),a.x*=c,a.y*=c;return a};
_.Af=function(a,b){var c=a.lat()+_.Ub(b);90<c&&(c=90);var d=a.lat()-_.Ub(b);-90>d&&(d=-90);var e=Math.sin(b),f=Math.cos(_.Sb(a.lat()));if(90==c||-90==d||1E-6>f)return new _.rd(new _.K(d,-180),new _.K(c,180));e=_.Ub(Math.asin(e/f));return new _.rd(new _.K(d,a.lng()-e),new _.K(c,a.lng()+e))};_.R=function(a){this.Wl=a||0;_.I.bind(this,"forceredraw",this,this.ma)};_.Bf=function(a,b){var c=a.style;c.width=b.width+b.ma;c.height=b.height+b.W};_.Cf=function(a){return new _.N(a.offsetWidth,a.offsetHeight)};
_.Df=function(){return window.devicePixelRatio||window.screen.deviceXDPI&&window.screen.deviceXDPI/96||1};Ef=function(a){this.j=a||[]};Gf=function(a){this.j=a||[]};_.Hf=function(){_.Qc.call(this)};_.If=function(a){_.Qc.call(this);this.j=a};Jf=function(a){this.j=a||[]};Kf=function(a){this.j=a||[]};Lf=function(a){this.j=a||[]};
Mf=function(a,b,c,d){_.R.call(this);this.V=b;this.U=new _.wf;this.W=c+"/maps/api/js/StaticMapService.GetMapImage";this.R=this.j=null;this.S=d;this.set("div",a);this.set("loading",!0)};Of=function(a){var b=a.get("tilt")||a.get("mapMaker")||_.x(a.get("styles"));a=a.get("mapTypeId");return b?null:Nf[a]};Pf=function(a){a.parentNode&&a.parentNode.removeChild(a)};
Qf=function(a,b,c,d,e){var f=_.T[15]?Ze():Ye();this.j=a;this.R=d;this.S=_.ra(e)?e:_.Fa();var g=f+"/csi?v=2&s=mapsapi3&v3v="+of()+"&action="+a;_.Dc(c,function(a,b){g+="&"+(0,window.encodeURIComponent)(b)+"="+(0,window.encodeURIComponent)(a)});b&&(g+="&e="+b);this.U=g};_.Sf=function(a,b){var c={};c[b]=void 0;_.Rf(a,c)};
_.Rf=function(a,b){var c="";_.Dc(b,function(a,b){var d=(null!=a?a:_.Fa())-this.S;c&&(c+=",");c+=b+"."+Math.round(d);null==a&&window.performance&&window.performance.mark&&window.performance.mark("mapsapi:"+this.j+":"+b)},a);var d=a.U+"&rt="+c;a.R.createElement("img").src=d;var e=_.dd.__gm_captureCSI;e&&e(d)};
_.Tf=function(a,b){var c=b||{},d=c.Mp||{},e=_.P(_.Q.j,12).join(",");e&&(d.libraries=e);var e=_.ef(_.Q),f=Be(),g=[];e&&g.push(e);_.Ob(f.$(),function(a,b){a&&_.Ob(a,function(a,c){null!=a&&g.push(b+1+"_"+(c+1)+"_"+a)})});c.Gn&&(g=g.concat(c.Gn));return new Qf(a,g.join(","),d,c.document||window.document,c.startTime)};Vf=function(){this.R=_.Tf("apiboot2",{startTime:_.Uf});_.Sf(this.R,"main");this.j=!1};Xf=function(){var a=Wf;a.j||(a.j=!0,_.Sf(a.R,"firstmap"))};_.Yf=_.k();_.Zf=function(){this.j=""};
_.$f=function(a){var b=new _.Zf;b.j=a;return b};_.bg=function(){this.ah="";this.Xl=_.ag;this.j=null};_.cg=function(a,b){var c=new _.bg;c.ah=a;c.j=b;return c};_.dg=function(a,b){b.parentNode&&b.parentNode.insertBefore(a,b.nextSibling)};_.eg=function(a){return a&&a.parentNode?a.parentNode.removeChild(a):null};fg=function(a,b,c,d,e){this.j=!!b;this.node=null;this.R=0;this.S=!1;this.U=!c;a&&this.setPosition(a,d);this.depth=void 0!=e?e:this.R||0;this.j&&(this.depth*=-1)};
gg=function(a,b,c,d){fg.call(this,a,b,c,null,d)};_.ig=function(a){for(var b;b=a.firstChild;)_.hg(b),a.removeChild(b)};_.hg=function(a){a=new gg(a);try{for(;;)_.I.clearInstanceListeners(a.next())}catch(b){if(b!==_.jg)throw b;}};
_.ng=function(a,b){var c=_.Fa();Wf&&Xf();var d=new _.vf;_.vd.call(this,new uf(this,a,d));var e=b||{};_.B(e.mapTypeId)||(e.mapTypeId="roadmap");this.setValues(e);this.__gm.La=e.La;this.mapTypes=new jd;this.features=new _.J;_.kg.push(a);this.notify("streetView");var f=_.Cf(a);e.noClear||_.ig(a);var g=this.__gm,h=_.dd.gm_force_experiments;h&&(g.wa=h);var l=null,g=!!_.Q&&lg(e.useStaticMap,f);_.Q&&+Ee()&&(g=!1);g&&(l=new Mf(a,_.mg,_.Xe(),new _.If(null)),_.I.forward(l,"staticmaploaded",this),l.set("size",
f),l.bindTo("center",this),l.bindTo("zoom",this),l.bindTo("mapTypeId",this),l.bindTo("styles",this),l.bindTo("mapMaker",this));this.overlayMapTypes=new _.xc;var n=this.controls=[];_.Ha(_.qf,function(a,b){n[b]=new _.xc});var p=this,q=!0;_.L("map",function(a){a.R(p,e,l,q,c,d)});q=!1;this.data=new ce({map:this})};lg=function(a,b){if(_.B(a))return!!a;var c=b.width,d=b.height;return 384E3>=c*d&&800>=c&&800>=d};og=function(){_.L("maxzoom",_.ta)};
pg=function(a,b){!a||_.Sa(a)||_.D(a)?(this.set("tableId",a),this.setValues(b)):this.setValues(a)};_.qg=_.k();_.rg=function(a){this.setValues(Qd(a));_.L("poly",_.ta)};_.sg=function(a){this.setValues(Qd(a));_.L("poly",_.ta)};tg=function(){this.j=null};_.ug=function(){this.j=null};
_.vg=function(a){this.tileSize=a.tileSize||new _.N(256,256);this.name=a.name;this.alt=a.alt;this.minZoom=a.minZoom;this.maxZoom=a.maxZoom;this.S=(0,_.u)(a.getTileUrl,a);this.j=new _.yc;this.R=null;this.set("opacity",a.opacity);_.dd.window&&_.I.addDomListener(window,"online",(0,_.u)(this.Ip,this));var b=this;_.L("map",function(a){var d=b.R=a.j,e=b.tileSize||new _.N(256,256);b.j.forEach(function(a){var c=a.__gmimt,h=c.Oa,l=c.zoom,n=b.S(h,l);c.Ec=d(h,l,e,a,n,function(){_.I.trigger(a,"load")})})})};
wg=function(a,b){null!=a.style.opacity?a.style.opacity=b:a.style.filter=b&&"alpha(opacity="+Math.round(100*b)+")"};xg=function(a){a=a.get("opacity");return"number"==typeof a?a:1};_.yg=_.k();_.zg=function(a,b){this.set("styles",a);var c=b||{};this.j=c.baseMapTypeId||"roadmap";this.minZoom=c.minZoom;this.maxZoom=c.maxZoom||20;this.name=c.name;this.alt=c.alt;this.projection=null;this.tileSize=new _.N(256,256)};
_.Dg=function(a,b){_.Eb(Ab,"container is not a Node")(a);this.setValues(b);_.L("controls",(0,_.u)(function(b){b.fn(this,a)},this))};Eg=_.na("j");Fg=function(a,b,c){for(var d=Array(b.length),e=0,f=b.length;e<f;++e)d[e]=b.charCodeAt(e);d.unshift(c);a=a.j;c=b=0;for(e=d.length;c<e;++c)b*=1729,b+=d[c],b%=a;return b};
Ig=function(){var a=cf(),b=new Eg(131071),c=(0,window.unescape)("%26%74%6F%6B%65%6E%3D");return function(d){d=d.replace(Gg,"%27");var e=d+c;Hg||(Hg=/(?:https?:\/\/[^/]+)?(.*)/);d=Hg.exec(d);return e+Fg(b,d&&d[1],a)}};Jg=function(){var a=new Eg(2147483647);return function(b){return Fg(a,b,0)}};Kg=function(a){for(var b=a.split("."),c=window,d=window,e=0;e<b.length;e++)if(d=c,c=c[b[e]],!c)throw _.xb(a+" is not a function");return function(){c.apply(d)}};
Lg=function(){for(var a in Object.prototype)window.console&&window.console.error("This site adds property <"+a+"> to Object.prototype. Extending Object.prototype breaks JavaScript for..in loops, which are used heavily in Google Maps API v3.")};Mg=function(a){(a="version"in a)&&window.console&&window.console.error("You have included the Google Maps API multiple times on this page. This may cause unexpected errors.");return a};_.pa=[];_.dd=this;Aa="closure_uid_"+(1E9*Math.random()>>>0);Ba=0;var lb,mb;_.I={};lb="undefined"!=typeof window.navigator&&-1!=window.navigator.userAgent.toLowerCase().indexOf("msie");mb={};_.I.addListener=function(a,b,c){return new nb(a,b,c,0)};_.I.hasListeners=function(a,b){var c=a.__e3_,c=c&&c[b];return!!c&&!_.Ja(c)};_.I.removeListener=function(a){a&&a.remove()};_.I.clearListeners=function(a,b){_.Ha(gb(a,b),function(a,b){b&&b.remove()})};_.I.clearInstanceListeners=function(a){_.Ha(gb(a),function(a,c){c&&c.remove()})};
_.I.trigger=function(a,b,c){if(_.I.hasListeners(a,b)){var d=_.Va(arguments,2),e=gb(a,b),f;for(f in e){var g=e[f];g&&g.j.apply(g.Ob,d)}}};_.I.addDomListener=function(a,b,c,d){if(a.addEventListener){var e=d?4:1;a.addEventListener(b,c,d);c=new nb(a,b,c,e)}else a.attachEvent?(c=new nb(a,b,c,2),a.attachEvent("on"+b,ob(c))):(a["on"+b]=c,c=new nb(a,b,c,3));return c};_.I.addDomListenerOnce=function(a,b,c,d){var e=_.I.addDomListener(a,b,function(){e.remove();return c.apply(this,arguments)},d);return e};
_.I.Ka=function(a,b,c,d){return _.I.addDomListener(a,b,hb(c,d))};_.I.bind=function(a,b,c,d){return _.I.addListener(a,b,(0,_.u)(d,c))};_.I.addListenerOnce=function(a,b,c){var d=_.I.addListener(a,b,function(){d.remove();return c.apply(this,arguments)});return d};_.I.forward=function(a,b,c){return _.I.addListener(a,b,ib(b,c))};_.I.Cb=function(a,b,c,d){return _.I.addDomListener(a,b,ib(b,c,!d))};_.I.xk=function(){var a=mb,b;for(b in a)a[b].remove();mb={};(a=_.dd.CollectGarbage)&&a()};
_.I.Yp=function(){lb&&_.I.addDomListener(window,"unload",_.I.xk)};var jb=0;nb.prototype.remove=function(){if(this.Ob){switch(this.U){case 1:this.Ob.removeEventListener(this.R,this.j,!1);break;case 4:this.Ob.removeEventListener(this.R,this.j,!0);break;case 2:this.Ob.detachEvent("on"+this.R,this.S);break;case 3:this.Ob["on"+this.R]=null}delete fb(this.Ob,this.R)[this.id];this.S=this.j=this.Ob=null;delete mb[this.id]}};_.t=_.J.prototype;_.t.get=function(a){var b=ub(this);a=a+"";b=$a(b,a);if(_.B(b)){if(b){a=b.jc;var b=b.Rd,c="get"+_.tb(a);return b[c]?b[c]():b.get(a)}return this[a]}};_.t.set=function(a,b){var c=ub(this);a=a+"";var d=$a(c,a);if(d){var c=d.jc,d=d.Rd,e="set"+_.tb(c);if(d[e])d[e](b);else d.set(c,b)}else this[a]=b,c[a]=null,rb(this,a)};_.t.notify=function(a){var b=ub(this);a=a+"";(b=$a(b,a))?b.Rd.notify(b.jc):rb(this,a)};
_.t.setValues=function(a){for(var b in a){var c=a[b],d="set"+_.tb(b);if(this[d])this[d](c);else this.set(b,c)}};_.t.setOptions=_.J.prototype.setValues;_.t.changed=_.k();var sb={};_.J.prototype.bindTo=function(a,b,c,d){a=a+"";c=(c||a)+"";this.unbind(a);var e={Rd:this,jc:a},f={Rd:b,jc:c,Ii:e};ub(this)[a]=f;qb(b,c)[_.pb(e)]=e;d||rb(this,a)};_.J.prototype.unbind=function(a){var b=ub(this),c=b[a];c&&(c.Ii&&delete qb(c.Rd,c.jc)[_.pb(c.Ii)],this[a]=this.get(a),b[a]=null)};
_.J.prototype.unbindAll=function(){var a=(0,_.u)(this.unbind,this),b=ub(this),c;for(c in b)a(c)};_.J.prototype.addListener=function(a,b){return _.I.addListener(this,a,b)};_.Ng={ROADMAP:"roadmap",SATELLITE:"satellite",HYBRID:"hybrid",TERRAIN:"terrain"};_.qf={TOP_LEFT:1,TOP_CENTER:2,TOP:2,TOP_RIGHT:3,LEFT_CENTER:4,LEFT_TOP:5,LEFT:5,LEFT_BOTTOM:6,RIGHT_TOP:7,RIGHT:7,RIGHT_CENTER:8,RIGHT_BOTTOM:9,BOTTOM_LEFT:10,BOTTOM_CENTER:11,BOTTOM:11,BOTTOM_RIGHT:12,CENTER:13};var Og={pr:"Point",nr:"LineString",POLYGON:"Polygon"};_.v(wb,Error);_.qc=_.Eb(_.D,"not a number");_.Pg=_.Eb(_.Sa,"not a string");_.Qg=_.Gb(_.qc);_.Rg=_.Gb(_.Pg);_.Sg=_.Gb(_.Eb(_.Ua,"not a boolean"));var Vb=_.zb({lat:_.qc,lng:_.qc},!0);_.K.prototype.toString=function(){return"("+this.lat()+", "+this.lng()+")"};_.K.prototype.toJSON=function(){return{lat:this.lat(),lng:this.lng()}};_.K.prototype.j=function(a){return a?_.Ma(this.lat(),a.lat())&&_.Ma(this.lng(),a.lng()):!1};_.K.prototype.equals=_.K.prototype.j;_.K.prototype.toUrlValue=function(a){a=_.B(a)?a:6;return Yb(this.lat(),a)+","+Yb(this.lng(),a)};_.Dd=_.Db(_.Zb);_.v(_.$b,vb);_.$b.prototype.getType=_.oa("Point");_.$b.prototype.get=_.m("j");var zd=_.Db(ac);_.ua(gc);gc.prototype.Rc=function(a,b){var c=this,d=c.V;hc(c.S,function(e){for(var f=e.Dj[a]||[],g=e.hq[a]||[],h=d[a]=_.bc(f.length,function(){delete d[a];b(e.mn);for(var f=c.R[a],h=f?f.length:0,l=0;l<h;++l)f[l](c.j[a]);delete c.R[a];l=0;for(f=g.length;l<f;++l)h=g[l],d[h]&&d[h]()}),l=0,n=f.length;l<n;++l)c.j[f[l]]&&h()})};_.t=_.pc.prototype;_.t.getId=_.m("S");_.t.getGeometry=_.m("j");_.t.setGeometry=function(a){var b=this.j;try{this.j=a?ac(a):null}catch(c){_.yb(c);return}_.I.trigger(this,"setgeometry",{feature:this,newGeometry:this.j,oldGeometry:b})};_.t.getProperty=function(a){return $a(this.R,a)};_.t.setProperty=function(a,b){if(void 0===b)this.removeProperty(a);else{var c=this.getProperty(a);this.R[a]=b;_.I.trigger(this,"setproperty",{feature:this,name:a,newValue:b,oldValue:c})}};
_.t.removeProperty=function(a){var b=this.getProperty(a);delete this.R[a];_.I.trigger(this,"removeproperty",{feature:this,name:a,oldValue:b})};_.t.forEachProperty=function(a){for(var b in this.R)a(this.getProperty(b),b)};_.t.toGeoJson=function(a){var b=this;_.L("data",function(c){c.R(b,a)})};_.Tg=new _.M(0,0);_.M.prototype.toString=function(){return"("+this.x+", "+this.y+")"};_.M.prototype.j=function(a){return a?a.x==this.x&&a.y==this.y:!1};_.M.prototype.equals=_.M.prototype.j;_.M.prototype.round=function(){this.x=Math.round(this.x);this.y=Math.round(this.y)};_.M.prototype.Lf=_.qa(0);_.Ug=new _.N(0,0);_.N.prototype.toString=function(){return"("+this.width+", "+this.height+")"};_.N.prototype.j=function(a){return a?a.width==this.width&&a.height==this.height:!1};_.N.prototype.equals=_.N.prototype.j;var Vg={CIRCLE:0,FORWARD_CLOSED_ARROW:1,FORWARD_OPEN_ARROW:2,BACKWARD_CLOSED_ARROW:3,BACKWARD_OPEN_ARROW:4};_.v(_.xc,_.J);_.t=_.xc.prototype;_.t.getAt=function(a){return this.j[a]};_.t.indexOf=function(a){for(var b=0,c=this.j.length;b<c;++b)if(a===this.j[b])return b;return-1};_.t.forEach=function(a){for(var b=0,c=this.j.length;b<c;++b)a(this.j[b],b)};_.t.setAt=function(a,b){var c=this.j[a],d=this.j.length;if(a<d)this.j[a]=b,_.I.trigger(this,"set_at",a,c),this.U&&this.U(a,c);else{for(c=d;c<a;++c)this.insertAt(c,void 0);this.insertAt(a,b)}};
_.t.insertAt=function(a,b){this.j.splice(a,0,b);wc(this);_.I.trigger(this,"insert_at",a);this.R&&this.R(a)};_.t.removeAt=function(a){var b=this.j[a];this.j.splice(a,1);wc(this);_.I.trigger(this,"remove_at",a,b);this.S&&this.S(a,b);return b};_.t.push=function(a){this.insertAt(this.j.length,a);return this.j.length};_.t.pop=function(){return this.removeAt(this.j.length-1)};_.t.getArray=_.m("j");_.t.clear=function(){for(;this.get("length");)this.pop()};_.vc(_.xc.prototype,{length:null});_.yc.prototype.remove=function(a){var b=this.R,c=this.S(a);b[c]&&(delete b[c],_.I.trigger(this,"remove",a),this.onRemove&&this.onRemove(a))};_.yc.prototype.contains=function(a){return!!this.R[this.S(a)]};_.yc.prototype.forEach=function(a){var b=this.R,c;for(c in b)a.call(this,b[c])};var Wg=_.zb({zoom:_.Qg,heading:_.qc,pitch:_.qc});_.v(_.Bc,_.J);var Xg=function(a){return function(){return a}}(null);a:{var Yg=_.dd.navigator;if(Yg){var Zg=Yg.userAgent;if(Zg){_.Jb=Zg;break a}}_.Jb=""};Ic.prototype.get=function(){var a;0<this.R?(this.R--,a=this.j,this.j=a.next,a.next=null):a=this.S();return a};_.$g=new Ic(function(){return new Nc},function(a){a.reset()},100);Mc.prototype.add=function(a,b){var c=_.$g.get();c.set(a,b);this.R?this.R.next=c:this.j=c;this.R=c};Mc.prototype.remove=function(){var a=null;this.j&&(a=this.j,this.j=this.j.next,this.j||(this.R=null),a.next=null);return a};Nc.prototype.set=function(a,b){this.Md=a;this.j=b;this.next=null};Nc.prototype.reset=function(){this.next=this.j=this.Md=null};_.ah=new Mc;Pc.prototype.addListener=function(a,b,c){c=c?{Li:!1}:null;var d=!this.Ea.length,e;e=this.Ea;var f=Pb(e,Oc(a,b));(e=0>f?null:_.xa(e)?e.charAt(f):e[f])?e.Ie=e.Ie&&c:this.Ea.push({Md:a,context:b||null,Ie:c});d&&this.R();return a};Pc.prototype.addListenerOnce=function(a,b){this.addListener(a,b,!0);return a};Pc.prototype.removeListener=function(a,b){if(this.Ea.length){var c=this.Ea,d=Pb(c,Oc(a,b));0<=d&&_.Qb(c,d);this.Ea.length||this.j()}};
Pc.prototype.forEach=function(a,b){var c=this.Ea.slice(0),d=this;(function(){_.Ob(c,function(c){a.call(b||null,function(a){if(c.Ie){if(c.Ie.Li)return;c.Ie.Li=!0;_.Rb(d.Ea,c);d.Ea.length||d.j()}c.Md.call(c.context,a)})})})()};_.t=_.Qc.prototype;_.t.He=_.k();_.t.Fe=_.k();_.t.addListener=function(a,b){return this.Ea.addListener(a,b)};_.t.addListenerOnce=function(a,b){return this.Ea.addListenerOnce(a,b)};_.t.removeListener=function(a,b){return this.Ea.removeListener(a,b)};_.t.Of=function(){this.Ea.forEach(function(a){a(this.get())},this)};_.v(Rc,_.J);var ch;_.bh=new Vc;ch=/'/g;Vc.prototype.j=function(a,b){var c=[];Xc(a,b,c);return c.join("&").replace(ch,"%27")};var fd,kh;_.dh=_.Fc();_.ad=_.Gc();_.$c=_.Ec("Edge");_.Zc=_.Ec("Gecko")&&!(_.Kb()&&!_.Ec("Edge"))&&!(_.Ec("Trident")||_.Ec("MSIE"))&&!_.Ec("Edge");_.bd=_.Kb()&&!_.Ec("Edge");_.eh=_.Ec("Macintosh");_.fh=_.Ec("Windows");_.gh=_.Ec("Linux")||_.Ec("CrOS");_.hh=_.Ec("Android");_.ih=_.Yc();_.jh=_.Ec("iPad");_.gd=function(){if(_.dh&&_.dd.opera){var a;var b=_.dd.opera.version;try{a=b()}catch(c){a=b}return a}a="";(b=cd())&&(a=b?b[1]:"");return _.ad&&(b=ed(),null!=b&&b>(0,window.parseFloat)(a))?String(b):a}();
fd={};kh=_.dd.document;_.lh=kh&&_.ad?ed()||("CSS1Compat"==kh.compatMode?(0,window.parseInt)(_.gd,10):5):void 0;_.mh=_.Ec("Firefox");_.nh=_.Yc()||_.Ec("iPod");_.oh=_.Ec("iPad");_.ph=_.Ec("Android")&&!(Hc()||_.Ec("Firefox")||_.Fc()||_.Ec("Silk"));_.qh=Hc();_.rh=_.Ec("Safari")&&!(Hc()||_.Ec("Coast")||_.Fc()||_.Ec("Edge")||_.Ec("Silk")||_.Ec("Android"))&&!(_.Yc()||_.Ec("iPad")||_.Ec("iPod"));_.id.prototype.heading=_.m("j");_.id.prototype.Jb=_.qa(1);_.id.prototype.toString=function(){return this.j+","+this.R};_.sh=new _.id;_.v(jd,_.J);jd.prototype.set=function(a,b){if(null!=b&&!(b&&_.D(b.maxZoom)&&b.tileSize&&b.tileSize.width&&b.tileSize.height&&b.getTile&&b.getTile.apply))throw Error("Expected value implementing google.maps.MapType");return _.J.prototype.set.apply(this,arguments)};_.t=kd.prototype;_.t.isEmpty=function(){return 360==this.j-this.R};_.t.intersects=function(a){var b=this.j,c=this.R;return this.isEmpty()||a.isEmpty()?!1:_.ld(this)?_.ld(a)||a.j<=this.R||a.R>=b:_.ld(a)?a.j<=c||a.R>=b:a.j<=c&&a.R>=b};_.t.contains=function(a){-180==a&&(a=180);var b=this.j,c=this.R;return _.ld(this)?(a>=b||a<=c)&&!this.isEmpty():a>=b&&a<=c};_.t.extend=function(a){this.contains(a)||(this.isEmpty()?this.j=this.R=a:_.od(a,this.j)<_.od(this.R,a)?this.j=a:this.R=a)};
_.t.Kc=function(){var a=(this.j+this.R)/2;_.ld(this)&&(a=_.La(a+180,-180,180));return a};_.t=pd.prototype;_.t.isEmpty=function(){return this.R>this.j};_.t.intersects=function(a){var b=this.R,c=this.j;return b<=a.R?a.R<=c&&a.R<=a.j:b<=a.j&&b<=c};_.t.contains=function(a){return a>=this.R&&a<=this.j};_.t.extend=function(a){this.isEmpty()?this.j=this.R=a:a<this.R?this.R=a:a>this.j&&(this.j=a)};_.t.Kc=function(){return(this.j+this.R)/2};_.t=_.rd.prototype;_.t.getCenter=function(){return new _.K(this.R.Kc(),this.j.Kc())};_.t.toString=function(){return"("+this.getSouthWest()+", "+this.getNorthEast()+")"};_.t.toJSON=function(){return{south:this.R.R,west:this.j.j,north:this.R.j,east:this.j.R}};_.t.toUrlValue=function(a){var b=this.getSouthWest(),c=this.getNorthEast();return[b.toUrlValue(a),c.toUrlValue(a)].join()};
_.t.fl=function(a){if(!a)return!1;a=_.ud(a);var b=this.R,c=a.R;return(b.isEmpty()?c.isEmpty():1E-9>=Math.abs(c.R-b.R)+Math.abs(b.j-c.j))&&_.nd(this.j,a.j)};_.rd.prototype.equals=_.rd.prototype.fl;_.t=_.rd.prototype;_.t.contains=function(a){return this.R.contains(a.lat())&&this.j.contains(a.lng())};_.t.intersects=function(a){a=_.ud(a);return this.R.intersects(a.R)&&this.j.intersects(a.j)};_.t.extend=function(a){this.R.extend(a.lat());this.j.extend(a.lng());return this};
_.t.union=function(a){a=_.ud(a);if(!a||a.isEmpty())return this;this.extend(a.getSouthWest());this.extend(a.getNorthEast());return this};_.t.getSouthWest=function(){return new _.K(this.R.R,this.j.j,!0)};_.t.getNorthEast=function(){return new _.K(this.R.j,this.j.R,!0)};_.t.toSpan=function(){return new _.K(_.qd(this.R),_.md(this.j),!0)};_.t.isEmpty=function(){return this.R.isEmpty()||this.j.isEmpty()};var td=_.zb({south:_.qc,west:_.qc,north:_.qc,east:_.qc},!1);_.v(_.vd,_.J);_.kg=[];_.t=wd.prototype;_.t.contains=function(a){return this.j.hasOwnProperty(_.pb(a))};_.t.getFeatureById=function(a){return $a(this.R,a)};
_.t.add=function(a){a=a||{};a=a instanceof _.pc?a:new _.pc(a);if(!this.contains(a)){var b=a.getId();if(b){var c=this.getFeatureById(b);c&&this.remove(c)}c=_.pb(a);this.j[c]=a;b&&(this.R[b]=a);var d=_.I.forward(a,"setgeometry",this),e=_.I.forward(a,"setproperty",this),f=_.I.forward(a,"removeproperty",this);this.S[c]=function(){_.I.removeListener(d);_.I.removeListener(e);_.I.removeListener(f)};_.I.trigger(this,"addfeature",{feature:a})}return a};
_.t.remove=function(a){var b=_.pb(a),c=a.getId();if(this.j[b]){delete this.j[b];c&&delete this.R[c];if(c=this.S[b])delete this.S[b],c();_.I.trigger(this,"removefeature",{feature:a})}};_.t.forEach=function(a){for(var b in this.j)a(this.j[b])};xd.prototype.get=function(a){return this.j[a]};xd.prototype.set=function(a,b){var c=this.j;c[a]||(c[a]={});_.Ia(c[a],b);_.I.trigger(this,"changed",a)};xd.prototype.reset=function(a){delete this.j[a];_.I.trigger(this,"changed",a)};xd.prototype.forEach=function(a){_.Ha(this.j,a)};_.v(yd,_.J);yd.prototype.overrideStyle=function(a,b){this.j.set(_.pb(a),b)};yd.prototype.revertStyle=function(a){a?this.j.reset(_.pb(a)):this.j.forEach((0,_.u)(this.j.reset,this.j))};_.v(_.Cd,vb);_.Cd.prototype.getType=_.oa("GeometryCollection");_.Cd.prototype.getLength=function(){return this.j.length};_.Cd.prototype.getAt=function(a){return this.j[a]};_.Cd.prototype.getArray=function(){return this.j.slice()};_.v(_.Ed,vb);_.Ed.prototype.getType=_.oa("LineString");_.Ed.prototype.getLength=function(){return this.j.length};_.Ed.prototype.getAt=function(a){return this.j[a]};_.Ed.prototype.getArray=function(){return this.j.slice()};var Fd=_.Db(_.Bb(_.Ed,"google.maps.Data.LineString",!0));_.v(_.Gd,vb);_.Gd.prototype.getType=_.oa("MultiLineString");_.Gd.prototype.getLength=function(){return this.j.length};_.Gd.prototype.getAt=function(a){return this.j[a]};_.Gd.prototype.getArray=function(){return this.j.slice()};_.v(_.Hd,vb);_.Hd.prototype.getType=_.oa("MultiPoint");_.Hd.prototype.getLength=function(){return this.j.length};_.Hd.prototype.getAt=function(a){return this.j[a]};_.Hd.prototype.getArray=function(){return this.j.slice()};_.v(_.Id,vb);_.Id.prototype.getType=_.oa("LinearRing");_.Id.prototype.getLength=function(){return this.j.length};_.Id.prototype.getAt=function(a){return this.j[a]};_.Id.prototype.getArray=function(){return this.j.slice()};var Jd=_.Db(_.Bb(_.Id,"google.maps.Data.LinearRing",!0));_.v(_.Kd,vb);_.Kd.prototype.getType=_.oa("Polygon");_.Kd.prototype.getLength=function(){return this.j.length};_.Kd.prototype.getAt=function(a){return this.j[a]};_.Kd.prototype.getArray=function(){return this.j.slice()};var Ld=_.Db(_.Bb(_.Kd,"google.maps.Data.Polygon",!0));_.v(_.Md,vb);_.Md.prototype.getType=_.oa("MultiPolygon");_.Md.prototype.getLength=function(){return this.j.length};_.Md.prototype.getAt=function(a){return this.j[a]};_.Md.prototype.getArray=function(){return this.j.slice()};var th=_.zb({source:_.Pg,webUrl:_.Rg,iosDeepLinkId:_.Rg});var uh=_.Ga(_.zb({placeId:_.Rg,query:_.Rg,location:_.Zb}),function(a){if(a.placeId&&a.query)throw _.xb("cannot set both placeId and query");if(!a.placeId&&!a.query)throw _.xb("must set one of placeId or query");return a});_.v(Nd,_.J);
_.vc(Nd.prototype,{position:_.Gb(_.Zb),title:_.Rg,icon:_.Gb(_.Fb(_.Pg,{Yh:Hb("url"),then:_.zb({url:_.Pg,scaledSize:_.Gb(sc),size:_.Gb(sc),origin:_.Gb(rc),anchor:_.Gb(rc),labelOrigin:_.Gb(rc),path:_.Eb(Qa)},!0)},{Yh:Hb("path"),then:_.zb({path:_.Fb(_.Pg,_.Cb(Vg)),anchor:_.Gb(rc),labelOrigin:_.Gb(rc),fillColor:_.Rg,fillOpacity:_.Qg,rotation:_.Qg,scale:_.Qg,strokeColor:_.Rg,strokeOpacity:_.Qg,strokeWeight:_.Qg,url:_.Eb(Qa)},!0)})),label:_.Gb(_.Fb(_.Pg,{Yh:Hb("text"),then:_.zb({text:_.Pg,fontSize:_.Rg,fontWeight:_.Rg,
fontFamily:_.Rg},!0)})),shadow:_.Cc,shape:_.Cc,cursor:_.Rg,clickable:_.Sg,animation:_.Cc,draggable:_.Sg,visible:_.Sg,flat:_.Cc,zIndex:_.Qg,opacity:_.Qg,place:_.Gb(uh),attribution:_.Gb(th)});var lc={main:[],common:["main"],util:["common"],adsense:["main"],controls:["util"],data:["util"],directions:["util","geometry"],distance_matrix:["util"],drawing:["main"],drawing_impl:["controls"],elevation:["util","geometry"],geocoder:["util"],geojson:["main"],imagery_viewer:["main"],geometry:["main"],infowindow:["util"],kml:["onion","util","map"],layers:["map"],map:["common"],marker:["util"],maxzoom:["util"],onion:["util","map"],overlay:["common"],panoramio:["main"],places:["main"],places_impl:["controls"],
poly:["util","map","geometry"],search:["main"],search_impl:["onion"],stats:["util"],streetview:["util","geometry"],usage:["util"],visualization:["main"],visualization_impl:["onion"],weather:["main"],zombie:["main"]};var vh=_.dd.google.maps,wh=gc.Mc(),xh=(0,_.u)(wh.Rc,wh);vh.__gjsload__=xh;_.Ha(vh.modules,xh);delete vh.modules;_.yh=_.Gb(_.Bb(_.vd,"Map"));var zh=_.Gb(_.Bb(_.Bc,"StreetViewPanorama"));_.v(_.Pd,Nd);_.Pd.prototype.map_changed=function(){this.__gm.set&&this.__gm.set.remove(this);var a=this.get("map");this.__gm.set=a&&a.__gm.Qd;this.__gm.set&&_.zc(this.__gm.set,this)};_.Pd.MAX_ZINDEX=1E6;_.vc(_.Pd.prototype,{map:_.Fb(_.yh,zh)});var Sd=Ud(_.Bb(_.K,"LatLng"));_.v(Wd,_.J);Wd.prototype.map_changed=Wd.prototype.visible_changed=function(){var a=this;_.L("poly",function(b){b.R(a)})};Wd.prototype.getPath=function(){return this.get("latLngs").getAt(0)};Wd.prototype.setPath=function(a){try{this.get("latLngs").setAt(0,Td(a))}catch(b){_.yb(b)}};_.vc(Wd.prototype,{draggable:_.Sg,editable:_.Sg,map:_.yh,visible:_.Sg});_.v(_.Zd,Wd);_.Zd.prototype.Eb=!0;_.Zd.prototype.getPaths=function(){return this.get("latLngs")};_.Zd.prototype.setPaths=function(a){this.set("latLngs",Vd(a))};_.v(_.$d,Wd);_.$d.prototype.Eb=!1;_.be="click dblclick mousedown mousemove mouseout mouseover mouseup rightclick".split(" ");_.v(ce,_.J);_.t=ce.prototype;_.t.contains=function(a){return this.j.contains(a)};_.t.getFeatureById=function(a){return this.j.getFeatureById(a)};_.t.add=function(a){return this.j.add(a)};_.t.remove=function(a){this.j.remove(a)};_.t.forEach=function(a){this.j.forEach(a)};_.t.addGeoJson=function(a,b){return _.ae(this.j,a,b)};_.t.loadGeoJson=function(a,b,c){var d=this.j;_.L("data",function(e){e.In(d,a,b,c)})};_.t.toGeoJson=function(a){var b=this.j;_.L("data",function(c){c.Fn(b,a)})};
_.t.overrideStyle=function(a,b){this.R.overrideStyle(a,b)};_.t.revertStyle=function(a){this.R.revertStyle(a)};_.t.controls_changed=function(){this.get("controls")&&de(this)};_.t.drawingMode_changed=function(){this.get("drawingMode")&&de(this)};_.vc(ce.prototype,{map:_.yh,style:_.Cc,controls:_.Gb(_.Db(_.Cb(Og))),controlPosition:_.Gb(_.Cb(_.qf)),drawingMode:_.Gb(_.Cb(Og))});_.ee.prototype.$=_.m("j");_.fe.prototype.$=_.m("j");_.Ah=new _.ee;_.Bh=new _.ee;ge.prototype.$=_.m("j");_.Ch=new _.he;_.he.prototype.$=_.m("j");_.Dh=new _.ee;_.Eh=new ge;_.ie.prototype.$=_.m("j");_.Fh=new _.fe;_.Gh=new _.ie;_.Hh={METRIC:0,IMPERIAL:1};_.Ih={DRIVING:"DRIVING",WALKING:"WALKING",BICYCLING:"BICYCLING",TRANSIT:"TRANSIT"};_.Jh={BEST_GUESS:"bestguess",OPTIMISTIC:"optimistic",PESSIMISTIC:"pessimistic"};_.Kh={BUS:"BUS",RAIL:"RAIL",SUBWAY:"SUBWAY",TRAIN:"TRAIN",TRAM:"TRAM"};_.Lh={LESS_WALKING:"LESS_WALKING",FEWER_TRANSFERS:"FEWER_TRANSFERS"};var Mh=_.zb({routes:_.Db(_.Eb(_.Ra))},!0);_.v(_.je,_.J);_.vc(_.je.prototype,{content:_.Fb(_.Rg,_.Eb(Ab)),position:_.Gb(_.Zb),size:_.Gb(sc),map:_.Fb(_.yh,zh),anchor:_.Gb(_.Bb(_.J,"MVCObject")),zIndex:_.Qg});_.je.prototype.open=function(a,b){this.set("anchor",b);this.set("map",a)};_.je.prototype.close=function(){this.set("map",null)};_.v(ke,_.J);ke.prototype.changed=function(a){if("map"==a||"panel"==a){var b=this;_.L("directions",function(c){c.R(b,a)})}};_.vc(ke.prototype,{directions:Mh,map:_.yh,panel:_.Gb(_.Eb(Ab)),routeIndex:_.Qg});_.Nh=new _.le;me.prototype.route=function(a,b){_.L("directions",function(c){c.j(a,b,!0)})};ne.prototype.getDistanceMatrix=function(a,b){_.L("distance_matrix",function(c){c.j(a,b)})};oe.prototype.getElevationAlongPath=function(a,b){_.L("elevation",function(c){c.j(a,b)})};oe.prototype.getElevationForLocations=function(a,b){_.L("elevation",function(c){c.R(a,b)})};_.Oh=_.Bb(_.rd,"LatLngBounds");_.pe.prototype.geocode=function(a,b){_.L("geocoder",function(c){c.geocode(a,b)})};_.v(_.qe,_.J);_.qe.prototype.map_changed=function(){var a=this;_.L("kml",function(b){b.j(a)})};_.vc(_.qe.prototype,{map:_.yh,url:null,bounds:null,opacity:_.Qg});_.Qh={UNKNOWN:"UNKNOWN",OK:_.ha,INVALID_REQUEST:_.ba,DOCUMENT_NOT_FOUND:"DOCUMENT_NOT_FOUND",FETCH_ERROR:"FETCH_ERROR",INVALID_DOCUMENT:"INVALID_DOCUMENT",DOCUMENT_TOO_LARGE:"DOCUMENT_TOO_LARGE",LIMITS_EXCEEDED:"LIMITS_EXECEEDED",TIMED_OUT:"TIMED_OUT"};_.v(re,_.J);_.t=re.prototype;_.t.We=function(){var a=this;_.L("kml",function(b){b.R(a)})};_.t.url_changed=re.prototype.We;_.t.driveFileId_changed=re.prototype.We;_.t.map_changed=re.prototype.We;_.t.zIndex_changed=re.prototype.We;_.vc(re.prototype,{map:_.yh,defaultViewport:null,metadata:null,status:null,url:_.Rg,screenOverlays:_.Sg,zIndex:_.Qg});_.v(_.se,_.J);_.se.prototype.map_changed=function(){var a=this;_.L("layers",function(b){b.j(a)})};_.vc(_.se.prototype,{map:_.yh});_.v(te,_.J);te.prototype.map_changed=function(){var a=this;_.L("layers",function(b){b.R(a)})};_.vc(te.prototype,{map:_.yh});_.v(ue,_.J);ue.prototype.map_changed=function(){var a=this;_.L("layers",function(b){b.S(a)})};_.vc(ue.prototype,{map:_.yh});_.pf={japan_prequake:20,japan_postquake2010:24};_.Rh={NEAREST:"nearest",BEST:"best"};_.Sh={DEFAULT:"default",OUTDOOR:"outdoor"};ve.prototype.$=_.m("j");_.Th=new ve;var Uh,Vh,Wh,Xh,Yh;we.prototype.$=_.m("j");var Zh=new xe,$h=new ye,Ce=new ze,ai=new Ae;xe.prototype.$=_.m("j");ye.prototype.$=_.m("j");ze.prototype.$=_.m("j");Ae.prototype.$=_.m("j");_.Fe.prototype.$=_.m("j");_.bi=new _.Fe;_.ci=new _.Fe;var hf,jf,bf,lf,nf;_.Ge.prototype.$=_.m("j");_.Ge.prototype.getUrl=function(a){return _.P(this.j,0)[a]};_.Ge.prototype.setUrl=function(a,b){_.P(this.j,0)[a]=b};_.He.prototype.$=_.m("j");_.Ie.prototype.$=_.m("j");_.di=new _.Ge;_.ei=new _.Ge;_.fi=new _.Ge;_.gi=new _.Ge;_.hi=new _.Ge;Ne.prototype.$=_.m("j");Oe.prototype.$=_.m("j");Pe.prototype.$=_.m("j");Qe.prototype.$=_.m("j");_.ii=new _.Ie;_.ji=new _.He;hf=new Ne;jf=new Oe;bf=new Pe;_.ki=new _.Se;_.li=new _.Te;lf=new we;nf=new Re;Re.prototype.$=_.m("j");
_.Se.prototype.$=_.m("j");_.Te.prototype.$=_.m("j");_.v(rf,_.Bc);rf.prototype.visible_changed=function(){var a=this;!a.V&&a.getVisible()&&(a.V=!0,_.L("streetview",function(b){var c;a.R&&(c=a.R);b.Jp(a,c)}))};_.vc(rf.prototype,{visible:_.Sg,pano:_.Rg,position:_.Gb(_.Zb),pov:_.Gb(Wg),photographerPov:null,location:null,links:_.Db(_.Eb(_.Ra)),status:null,zoom:_.Qg,enableCloseButton:_.Sg});rf.prototype.registerPanoProvider=_.tc("panoProvider");_.t=_.tf.prototype;_.t.jf=_.qa(2);_.t.tc=_.qa(3);_.t.Qe=_.qa(4);_.t.Pe=_.qa(5);_.t.Oe=_.qa(6);_.v(uf,Rc);_.vf.prototype.addListener=function(a,b){this.Ea.addListener(a,b)};_.vf.prototype.addListenerOnce=function(a,b){this.Ea.addListenerOnce(a,b)};_.vf.prototype.removeListener=function(a,b){this.Ea.removeListener(a,b)};_.vf.prototype.j=_.qa(7);_.T={};_.wf.prototype.fromLatLngToPoint=function(a,b){var c=b||new _.M(0,0),d=this.j;c.x=d.x+a.lng()*this.S;var e=_.Ka(Math.sin(_.Sb(a.lat())),-(1-1E-15),1-1E-15);c.y=d.y+.5*Math.log((1+e)/(1-e))*-this.U;return c};_.wf.prototype.fromPointToLatLng=function(a,b){var c=this.j;return new _.K(_.Ub(2*Math.atan(Math.exp((a.y-c.y)/-this.U))-Math.PI/2),(a.x-c.x)/this.S,b)};_.xf.prototype.isEmpty=function(){return!(this.Aa<this.Da&&this.ya<this.Fa)};_.xf.prototype.extend=function(a){a&&(this.Aa=Math.min(this.Aa,a.x),this.Da=Math.max(this.Da,a.x),this.ya=Math.min(this.ya,a.y),this.Fa=Math.max(this.Fa,a.y))};_.xf.prototype.getCenter=function(){return new _.M((this.Aa+this.Da)/2,(this.ya+this.Fa)/2)};_.mi=_.yf(-window.Infinity,-window.Infinity,window.Infinity,window.Infinity);_.ni=_.yf(0,0,0,0);_.v(_.R,_.J);_.R.prototype.Ba=function(){var a=this;a.Ca||(a.Ca=window.setTimeout(function(){a.Ca=void 0;a.Na()},a.Wl))};_.R.prototype.ma=function(){this.Ca&&window.clearTimeout(this.Ca);this.Ca=void 0;this.Na()};var oi,pi;Ef.prototype.$=_.m("j");Gf.prototype.$=_.m("j");var qi=new Ef;_.v(_.Hf,_.Qc);_.Hf.prototype.set=function(a){this.ik(a);this.notify()};_.Hf.prototype.notify=function(){this.Of()};_.v(_.If,_.Hf);_.If.prototype.get=_.m("j");_.If.prototype.ik=_.na("j");var ri,si;Jf.prototype.$=_.m("j");Kf.prototype.$=_.m("j");var ti;Lf.prototype.$=_.m("j");Lf.prototype.getZoom=function(){var a=this.j[2];return null!=a?a:0};Lf.prototype.setZoom=function(a){this.j[2]=a};var ui=new Jf,vi=new Kf,wi=new Gf,xi=new we;_.v(Mf,_.R);var Nf={roadmap:0,satellite:2,hybrid:3,terrain:4},yi={0:1,2:2,3:2,4:2};_.t=Mf.prototype;_.t.fj=_.O("center");_.t.ui=_.O("zoom");_.t.changed=function(){var a=this.fj(),b=this.ui(),c=Of(this);if(a&&!a.j(this.ua)||this.ta!=b||this.wa!=c)Pf(this.R),this.Ba(),this.ta=b,this.wa=c;this.ua=a};
_.t.Na=function(){var a="",b=this.fj(),c=this.ui(),d=Of(this),e=this.get("size");if(b&&(0,window.isFinite)(b.lat())&&(0,window.isFinite)(b.lng())&&1<c&&null!=d&&e&&e.width&&e.height&&this.j){_.Bf(this.j,e);var f;(b=_.zf(this.U,b,c))?(f=new _.xf,f.Aa=Math.round(b.x-e.width/2),f.Da=f.Aa+e.width,f.ya=Math.round(b.y-e.height/2),f.Fa=f.ya+e.height):f=null;b=yi[d];if(f){var a=new Lf,g=1<(22>c&&_.Df())?2:1,h=De().j[12];null!=h&&h&&(g=1);a.j[0]=a.j[0]||[];h=new Jf(a.j[0]);h.j[0]=f.Aa*g;h.j[1]=f.ya*g;a.j[1]=
b;a.setZoom(c);a.j[3]=a.j[3]||[];c=new Kf(a.j[3]);c.j[0]=(f.Da-f.Aa)*g;c.j[1]=(f.Fa-f.ya)*g;1<g&&(c.j[2]=2);a.j[4]=a.j[4]||[];c=new Gf(a.j[4]);c.j[0]=d;c.j[4]=_.Ue(_.We(_.Q));c.j[5]=_.Ve(_.We(_.Q)).toLowerCase();c.j[9]=!0;c.j[11]=!0;d=this.W+(0,window.unescape)("%3F");ti||(c=[],ti={qa:-1,ra:c},ri||(b=[],ri={qa:-1,ra:b},b[1]={type:"i",label:1,T:0},b[2]={type:"i",label:1,T:0}),c[1]={type:"m",label:1,T:ui,ka:ri},c[2]={type:"e",label:1,T:0},c[3]={type:"u",label:1,T:0},si||(b=[],si={qa:-1,ra:b},b[1]={type:"u",
label:1,T:0},b[2]={type:"u",label:1,T:0},b[3]={type:"e",label:1,T:1}),c[4]={type:"m",label:1,T:vi,ka:si},pi||(b=[],pi={qa:-1,ra:b},b[1]={type:"e",label:1,T:0},b[2]={type:"b",label:1,T:!1},b[3]={type:"b",label:1,T:!1},b[5]={type:"s",label:1,T:""},b[6]={type:"s",label:1,T:""},oi||(f=[],oi={qa:-1,ra:f},f[1]={type:"e",label:3},f[2]={type:"b",label:1,T:!1}),b[9]={type:"m",label:1,T:qi,ka:oi},b[10]={type:"b",label:1,T:!1},b[11]={type:"b",label:1,T:!1},b[12]={type:"b",label:1,T:!1},b[100]={type:"b",label:1,
T:!1}),c[5]={type:"m",label:1,T:wi,ka:pi},Uh||(b=[],Uh={qa:-1,ra:b},Vh||(f=[],Vh={qa:-1,ra:f},f[1]={type:"b",label:1,T:!1}),b[1]={type:"m",label:1,T:Zh,ka:Vh},Wh||(f=[],Wh={qa:-1,ra:f},f[1]={type:"b",label:1,T:!1}),b[12]={type:"m",label:1,T:$h,ka:Wh},Xh||(f=[],Xh={qa:-1,ra:f},f[9]={type:"j",label:1,T:0},f[10]={type:"j",label:1,T:0},f[11]={type:"j",label:1,T:0},f[12]={type:"j",label:1,T:0},f[13]={type:"b",label:1,T:!1},f[14]={type:"s",label:1,T:""}),b[11]={type:"m",label:1,T:Ce,ka:Xh},Yh||(f=[],Yh=
{qa:-1,ra:f},f[1]={type:"b",label:1,T:!1},f[2]={type:"b",label:1,T:!1}),b[10]={type:"m",label:1,T:ai,ka:Yh}),c[6]={type:"m",label:1,T:xi,ka:Uh});a=_.bh.j(a.j,ti);a=this.V(d+a)}}this.R&&e&&(_.Bf(this.R,e),e=a,a=this.R,e!=a.src?(Pf(a),a.onload=_.Wa(this,this.vi,!0),a.onerror=_.Wa(this,this.vi,!1),a.src=e):!a.parentNode&&e&&this.j.appendChild(a))};
_.t.vi=function(a){var b=this.R;b.onload=null;b.onerror=null;a&&(b.parentNode||this.j.appendChild(b),_.Bf(b,this.get("size")),_.I.trigger(this,"staticmaploaded"),this.S.set(_.Fa()));this.set("loading",!1)};
_.t.div_changed=function(){var a=this.get("div"),b=this.j;if(a)if(b)a.appendChild(b);else{b=this.j=window.document.createElement("div");b.style.overflow="hidden";var c=this.R=window.document.createElement("img");_.I.addDomListener(b,"contextmenu",_.cb);c.ontouchstart=c.ontouchmove=c.ontouchend=c.ontouchcancel=_.db;_.Bf(c,_.Ug);a.appendChild(b);this.Na()}else b&&(Pf(b),this.j=null)};var Wf;_.jg="StopIteration"in _.dd?_.dd.StopIteration:{message:"StopIteration",stack:""};_.Yf.prototype.next=function(){throw _.jg;};_.Yf.prototype.Ag=function(){return this};_.Zf.prototype.dh=!0;_.Zf.prototype.nd=_.qa(9);_.Zf.prototype.sj=!0;_.Zf.prototype.Bf=_.qa(11);_.$f("about:blank");_.bg.prototype.sj=!0;_.bg.prototype.Bf=_.qa(10);_.bg.prototype.dh=!0;_.bg.prototype.nd=_.qa(8);_.ag={};_.cg("<!DOCTYPE html>",0);_.cg("",0);_.cg("<br>",0);!_.Zc&&!_.ad||_.ad&&9<=+_.lh||_.Zc&&_.hd("1.9.1");_.ad&&_.hd("9");_.v(fg,_.Yf);fg.prototype.setPosition=function(a,b,c){if(this.node=a)this.R=_.ya(b)?b:1!=this.node.nodeType?0:this.j?-1:1;_.ya(c)&&(this.depth=c)};
fg.prototype.next=function(){var a;if(this.S){if(!this.node||this.U&&0==this.depth)throw _.jg;a=this.node;var b=this.j?-1:1;if(this.R==b){var c=this.j?a.lastChild:a.firstChild;c?this.setPosition(c):this.setPosition(a,-1*b)}else(c=this.j?a.previousSibling:a.nextSibling)?this.setPosition(c):this.setPosition(a.parentNode,-1*b);this.depth+=this.R*(this.j?-1:1)}else this.S=!0;a=this.node;if(!this.node)throw _.jg;return a};
fg.prototype.splice=function(a){var b=this.node,c=this.j?1:-1;this.R==c&&(this.R=-1*c,this.depth+=this.R*(this.j?-1:1));this.j=!this.j;fg.prototype.next.call(this);this.j=!this.j;for(var c=_.wa(arguments[0])?arguments[0]:arguments,d=c.length-1;0<=d;d--)_.dg(c[d],b);_.eg(b)};_.v(gg,fg);gg.prototype.next=function(){do gg.qd.next.call(this);while(-1==this.R);return this.node};_.Ai=_.dd.document&&_.dd.document.createElement("div");_.v(_.ng,_.vd);_.t=_.ng.prototype;_.t.streetView_changed=function(){this.get("streetView")||this.set("streetView",this.__gm.R)};_.t.getDiv=function(){return this.__gm.Ia};_.t.panBy=function(a,b){var c=this.__gm;_.L("map",function(){_.I.trigger(c,"panby",a,b)})};_.t.panTo=function(a){var b=this.__gm;a=_.Zb(a);_.L("map",function(){_.I.trigger(b,"panto",a)})};_.t.panToBounds=function(a){var b=this.__gm,c=_.ud(a);_.L("map",function(){_.I.trigger(b,"pantolatlngbounds",c)})};
_.t.fitBounds=function(a){var b=this;a=_.ud(a);_.L("map",function(c){c.fitBounds(b,a)})};_.vc(_.ng.prototype,{bounds:null,streetView:zh,center:_.Gb(_.Zb),zoom:_.Qg,mapTypeId:_.Rg,projection:null,heading:_.Qg,tilt:_.Qg});og.prototype.getMaxZoomAtLatLng=function(a,b){_.L("maxzoom",function(c){c.getMaxZoomAtLatLng(a,b)})};_.v(pg,_.J);pg.prototype.changed=function(a){if("suppressInfoWindows"!=a&&"clickable"!=a){var b=this;_.L("onion",function(a){a.j(b)})}};_.vc(pg.prototype,{map:_.yh,tableId:_.Qg,query:_.Gb(_.Fb(_.Pg,_.Eb(_.Ra,"not an Object")))});_.v(_.qg,_.J);_.qg.prototype.map_changed=function(){var a=this;_.L("overlay",function(b){b.j(a)})};_.vc(_.qg.prototype,{panes:null,projection:null,map:_.Fb(_.yh,zh)});_.v(_.rg,_.J);_.rg.prototype.map_changed=_.rg.prototype.visible_changed=function(){var a=this;_.L("poly",function(b){b.j(a)})};_.rg.prototype.center_changed=function(){_.I.trigger(this,"bounds_changed")};_.rg.prototype.radius_changed=_.rg.prototype.center_changed;_.rg.prototype.getBounds=function(){var a=this.get("radius"),b=this.get("center");if(b&&_.D(a)){var c=this.get("map"),c=c&&c.__gm.get("mapType");return _.Af(b,a/_.Rd(c))}return null};
_.vc(_.rg.prototype,{center:_.Gb(_.Zb),draggable:_.Sg,editable:_.Sg,map:_.yh,radius:_.Qg,visible:_.Sg});_.v(_.sg,_.J);_.sg.prototype.map_changed=_.sg.prototype.visible_changed=function(){var a=this;_.L("poly",function(b){b.S(a)})};_.vc(_.sg.prototype,{draggable:_.Sg,editable:_.Sg,bounds:_.Gb(_.ud),map:_.yh,visible:_.Sg});_.v(tg,_.J);tg.prototype.map_changed=function(){var a=this;_.L("streetview",function(b){b.Pm(a)})};_.vc(tg.prototype,{map:_.yh});_.ug.prototype.getPanorama=function(a,b){var c=this.j||void 0;_.L("streetview",function(d){_.L("geometry",function(e){d.Tn(a,b,e.computeHeading,c)})})};_.ug.prototype.getPanoramaByLocation=function(a,b,c){this.getPanorama({location:a,radius:b,preference:50>(b||0)?"best":"nearest"},c)};_.ug.prototype.getPanoramaById=function(a,b){this.getPanorama({pano:a},b)};_.v(_.vg,_.J);_.t=_.vg.prototype;_.t.getTile=function(a,b,c){if(!a||!c)return null;var d=c.createElement("div");c={Oa:a,zoom:b,Ec:null};d.__gmimt=c;_.zc(this.j,d);var e=xg(this);1!=e&&wg(d,e);if(this.R){var e=this.tileSize||new _.N(256,256),f=this.S(a,b);c.Ec=this.R(a,b,e,d,f,function(){_.I.trigger(d,"load")})}return d};_.t.releaseTile=function(a){a&&this.j.contains(a)&&(this.j.remove(a),(a=a.__gmimt.Ec)&&a.release())};_.t.Tg=_.qa(12);_.t.Ip=function(){this.R&&this.j.forEach(function(a){a.__gmimt.Ec.dc()})};
_.t.opacity_changed=function(){var a=xg(this);this.j.forEach(function(b){wg(b,a)})};_.t.ae=!0;_.vc(_.vg.prototype,{opacity:_.Qg});_.v(_.yg,_.J);_.yg.prototype.getTile=Xg;_.yg.prototype.tileSize=new _.N(256,256);_.yg.prototype.ae=!0;_.v(_.zg,_.yg);_.v(_.Dg,_.J);_.vc(_.Dg.prototype,{attribution:_.Gb(th),place:_.Gb(uh)});var Bi={Animation:{BOUNCE:1,DROP:2,qr:3,or:4},Circle:_.rg,ControlPosition:_.qf,Data:ce,GroundOverlay:_.qe,ImageMapType:_.vg,InfoWindow:_.je,LatLng:_.K,LatLngBounds:_.rd,MVCArray:_.xc,MVCObject:_.J,Map:_.ng,MapTypeControlStyle:{DEFAULT:0,HORIZONTAL_BAR:1,DROPDOWN_MENU:2,INSET:3,INSET_LARGE:4},MapTypeId:_.Ng,MapTypeRegistry:jd,Marker:_.Pd,MarkerImage:function(a,b,c,d,e){this.url=a;this.size=b||e;this.origin=c;this.anchor=d;this.scaledSize=e;this.labelOrigin=null},NavigationControlStyle:{DEFAULT:0,SMALL:1,
ANDROID:2,ZOOM_PAN:3,rr:4,vm:5},OverlayView:_.qg,Point:_.M,Polygon:_.Zd,Polyline:_.$d,Rectangle:_.sg,ScaleControlStyle:{DEFAULT:0},Size:_.N,StreetViewPreference:_.Rh,StreetViewSource:_.Sh,StrokePosition:{CENTER:0,INSIDE:1,OUTSIDE:2},SymbolPath:Vg,ZoomControlStyle:{DEFAULT:0,SMALL:1,LARGE:2,vm:3},event:_.I};
_.Ia(Bi,{BicyclingLayer:_.se,DirectionsRenderer:ke,DirectionsService:me,DirectionsStatus:{OK:_.ha,UNKNOWN_ERROR:_.ka,OVER_QUERY_LIMIT:_.ia,REQUEST_DENIED:_.ja,INVALID_REQUEST:_.ba,ZERO_RESULTS:_.la,MAX_WAYPOINTS_EXCEEDED:_.ea,NOT_FOUND:_.ga},DirectionsTravelMode:_.Ih,DirectionsUnitSystem:_.Hh,DistanceMatrixService:ne,DistanceMatrixStatus:{OK:_.ha,INVALID_REQUEST:_.ba,OVER_QUERY_LIMIT:_.ia,REQUEST_DENIED:_.ja,UNKNOWN_ERROR:_.ka,MAX_ELEMENTS_EXCEEDED:_.da,MAX_DIMENSIONS_EXCEEDED:_.ca},DistanceMatrixElementStatus:{OK:_.ha,
NOT_FOUND:_.ga,ZERO_RESULTS:_.la},ElevationService:oe,ElevationStatus:{OK:_.ha,UNKNOWN_ERROR:_.ka,OVER_QUERY_LIMIT:_.ia,REQUEST_DENIED:_.ja,INVALID_REQUEST:_.ba,lr:"DATA_NOT_AVAILABLE"},FusionTablesLayer:pg,Geocoder:_.pe,GeocoderLocationType:{ROOFTOP:"ROOFTOP",RANGE_INTERPOLATED:"RANGE_INTERPOLATED",GEOMETRIC_CENTER:"GEOMETRIC_CENTER",APPROXIMATE:"APPROXIMATE"},GeocoderStatus:{OK:_.ha,UNKNOWN_ERROR:_.ka,OVER_QUERY_LIMIT:_.ia,REQUEST_DENIED:_.ja,INVALID_REQUEST:_.ba,ZERO_RESULTS:_.la,ERROR:_.aa},KmlLayer:re,
KmlLayerStatus:_.Qh,MaxZoomService:og,MaxZoomStatus:{OK:_.ha,ERROR:_.aa},SaveWidget:_.Dg,StreetViewCoverageLayer:tg,StreetViewPanorama:rf,StreetViewService:_.ug,StreetViewStatus:{OK:_.ha,UNKNOWN_ERROR:_.ka,ZERO_RESULTS:_.la},StyledMapType:_.zg,TrafficLayer:te,TrafficModel:_.Jh,TransitLayer:ue,TransitMode:_.Kh,TransitRoutePreference:_.Lh,TravelMode:_.Ih,UnitSystem:_.Hh});_.Ia(ce,{Feature:_.pc,Geometry:vb,GeometryCollection:_.Cd,LineString:_.Ed,LinearRing:_.Id,MultiLineString:_.Gd,MultiPoint:_.Hd,MultiPolygon:_.Md,Point:_.$b,Polygon:_.Kd});var Gg=/'/g,Hg;_.nc("main",{});window.google.maps.Load(function(a,b){var c=window.google.maps;Lg();var d=Mg(c);_.Q=new Qe(a);_.Ci=Math.random()<_.df();_.Di=Math.round(1E15*Math.random()).toString(36);_.mg=Ig();_.Ph=Jg();_.zi=new _.xc;_.Uf=b;for(var e=0;e<_.Uc(_.Q.j,8);++e)_.T[mf(e)]=!0;e=_.kf();Od($e(e));_.Ha(Bi,function(a,b){c[a]=b});c.version=_.af(e);window.setTimeout(function(){oc(["util","stats"],function(a,b){a.R.j();a.S();d&&b.j.j({ev:"api_alreadyloaded",client:_.ef(_.Q),key:_.gf()})})},5E3);_.I.Yp();Wf=new Vf;(e=ff())&&
oc(_.P(_.Q.j,12),Kg(e),!0)});}).call(this,{});






L.Google = L.Class.extend({
	includes: L.Mixin.Events,

	options: {
		minZoom: 0,
		maxZoom: 18,
		tileSize: 256,
		subdomains: 'abc',
		errorTileUrl: '',
		attribution: '',
		opacity: 1,
		continuousWorld: false,
		noWrap: false,
		mapOptions: {
			backgroundColor: '#dddddd'
		}
	},

	// Possible types: SATELLITE, ROADMAP, HYBRID, TERRAIN
	initialize: function(type, options) {
		L.Util.setOptions(this, options);

		this._ready = google.maps.Map !== undefined;
		if (!this._ready) L.Google.asyncWait.push(this);

		this._type = type || 'SATELLITE';
	},

	onAdd: function(map, insertAtTheBottom) {
		this._map = map;
		this._insertAtTheBottom = insertAtTheBottom;

		// create a container div for tiles
		this._initContainer();
		this._initMapObject();

		// set up events
		map.on('viewreset', this._resetCallback, this);

		this._limitedUpdate = L.Util.limitExecByInterval(this._update, 150, this);
		map.on('move', this._update, this);

		map.on('zoomanim', this._handleZoomAnim, this);

		//20px instead of 1em to avoid a slight overlap with google's attribution
		map._controlCorners.bottomright.style.marginBottom = '20px';

		this._reset();
		this._update();
	},

	onRemove: function(map) {
		map._container.removeChild(this._container);

		map.off('viewreset', this._resetCallback, this);

		map.off('move', this._update, this);

		map.off('zoomanim', this._handleZoomAnim, this);

		map._controlCorners.bottomright.style.marginBottom = '0em';
	},

	getAttribution: function() {
		return this.options.attribution;
	},

	setOpacity: function(opacity) {
		this.options.opacity = opacity;
		if (opacity < 1) {
			L.DomUtil.setOpacity(this._container, opacity);
		}
	},

	setElementSize: function(e, size) {
		e.style.width = size.x + 'px';
		e.style.height = size.y + 'px';
	},

	_initContainer: function() {
		var tilePane = this._map._container,
			first = tilePane.firstChild;

		if (!this._container) {
			this._container = L.DomUtil.create('div', 'leaflet-google-layer leaflet-top leaflet-left');
			this._container.id = '_GMapContainer_' + L.Util.stamp(this);
			this._container.style.zIndex = 'auto';
		}

		tilePane.insertBefore(this._container, first);

		this.setOpacity(this.options.opacity);
		this.setElementSize(this._container, this._map.getSize());
	},

	_initMapObject: function() {
		if (!this._ready) return;
		this._google_center = new google.maps.LatLng(0, 0);
		var map = new google.maps.Map(this._container, {
			center: this._google_center,
			zoom: 0,
			tilt: 0,
			mapTypeId: google.maps.MapTypeId[this._type],
			disableDefaultUI: true,
			keyboardShortcuts: false,
			draggable: false,
			disableDoubleClickZoom: true,
			scrollwheel: false,
			streetViewControl: false,
			styles: this.options.mapOptions.styles,
			backgroundColor: this.options.mapOptions.backgroundColor
		});

		var _this = this;
		this._reposition = google.maps.event.addListenerOnce(map, 'center_changed',
			function() { _this.onReposition(); });
		this._google = map;

		google.maps.event.addListenerOnce(map, 'idle',
			function() { _this._checkZoomLevels(); });
		//Reporting that map-object was initialized.
		this.fire('MapObjectInitialized', { mapObject: map });
	},

	_checkZoomLevels: function() {
		//setting the zoom level on the Google map may result in a different zoom level than the one requested
		//(it won't go beyond the level for which they have data).
		// verify and make sure the zoom levels on both Leaflet and Google maps are consistent
		if (this._google.getZoom() !== this._map.getZoom()) {
			//zoom levels are out of sync. Set the leaflet zoom level to match the google one
			this._map.setZoom( this._google.getZoom() );
		}
	},

	_resetCallback: function(e) {
		this._reset(e.hard);
	},

	_reset: function(clearOldContainer) {
		this._initContainer();
	},

	_update: function(e) {
		if (!this._google) return;
		this._resize();

		var center = this._map.getCenter();
		var _center = new google.maps.LatLng(center.lat, center.lng);

		this._google.setCenter(_center);
		this._google.setZoom(Math.round(this._map.getZoom()));

		this._checkZoomLevels();
	},

	_resize: function() {
		var size = this._map.getSize();
		if (this._container.style.width === size.x &&
				this._container.style.height === size.y)
			return;
		this.setElementSize(this._container, size);
		this.onReposition();
	},


	_handleZoomAnim: function (e) {
		var center = e.center;
		var _center = new google.maps.LatLng(center.lat, center.lng);

		this._google.setCenter(_center);
		this._google.setZoom(Math.round(e.zoom));
	},


	onReposition: function() {
		if (!this._google) return;
		google.maps.event.trigger(this._google, 'resize');
	}
});

L.Google.asyncWait = [];
L.Google.asyncInitialize = function() {
	var i;
	for (i = 0; i < L.Google.asyncWait.length; i++) {
		var o = L.Google.asyncWait[i];
		o._ready = true;
		if (o._container) {
			o._initMapObject();
			o._update();
		}
	}
	L.Google.asyncWait = [];
};

(function (window, document, undefined) {

L.MarkerClusterGroup = L.FeatureGroup.extend({

	options: {
		maxClusterRadius: 80, //A cluster will cover at most this many pixels from its center
		iconCreateFunction: null,

		spiderfyOnMaxZoom: true,
		showCoverageOnHover: true,
		zoomToBoundsOnClick: true,
		singleMarkerMode: false,

		disableClusteringAtZoom: null,

		// Setting this to false prevents the removal of any clusters outside of the viewpoint, which
		// is the default behaviour for performance reasons.
		removeOutsideVisibleBounds: true,

		//Whether to animate adding markers after adding the MarkerClusterGroup to the map
		// If you are adding individual markers set to true, if adding bulk markers leave false for massive performance gains.
		animateAddingMarkers: false,

		//Increase to increase the distance away that spiderfied markers appear from the center
		spiderfyDistanceMultiplier: 1,

		// When bulk adding layers, adds markers in chunks. Means addLayers may not add all the layers in the call, others will be loaded during setTimeouts
		chunkedLoading: false,
		chunkInterval: 200, // process markers for a maximum of ~ n milliseconds (then trigger the chunkProgress callback)
		chunkDelay: 50, // at the end of each interval, give n milliseconds back to system/browser
		chunkProgress: null, // progress callback: function(processed, total, elapsed) (e.g. for a progress indicator)

		//Options to pass to the L.Polygon constructor
		polygonOptions: {}
	},

	initialize: function (options) {
		L.Util.setOptions(this, options);
		if (!this.options.iconCreateFunction) {
			this.options.iconCreateFunction = this._defaultIconCreateFunction;
		}

		this._featureGroup = L.featureGroup();
		this._featureGroup.on(L.FeatureGroup.EVENTS, this._propagateEvent, this);

		this._nonPointGroup = L.featureGroup();
		this._nonPointGroup.on(L.FeatureGroup.EVENTS, this._propagateEvent, this);

		this._inZoomAnimation = 0;
		this._needsClustering = [];
		this._needsRemoving = []; //Markers removed while we aren't on the map need to be kept track of
		//The bounds of the currently shown area (from _getExpandedVisibleBounds) Updated on zoom/move
		this._currentShownBounds = null;

		this._queue = [];
	},

	addLayer: function (layer) {

		if (layer instanceof L.LayerGroup) {
			var array = [];
			for (var i in layer._layers) {
				array.push(layer._layers[i]);
			}
			return this.addLayers(array);
		}

		//Don't cluster non point data
		if (!layer.getLatLng) {
			this._nonPointGroup.addLayer(layer);
			return this;
		}

		if (!this._map) {
			this._needsClustering.push(layer);
			return this;
		}

		if (this.hasLayer(layer)) {
			return this;
		}


		//If we have already clustered we'll need to add this one to a cluster

		if (this._unspiderfy) {
			this._unspiderfy();
		}

		this._addLayer(layer, this._maxZoom);

		//Work out what is visible
		var visibleLayer = layer,
			currentZoom = this._map.getZoom();
		if (layer.__parent) {
			while (visibleLayer.__parent._zoom >= currentZoom) {
				visibleLayer = visibleLayer.__parent;
			}
		}

		if (this._currentShownBounds.contains(visibleLayer.getLatLng())) {
			if (this.options.animateAddingMarkers) {
				this._animationAddLayer(layer, visibleLayer);
			} else {
				this._animationAddLayerNonAnimated(layer, visibleLayer);
			}
		}
		return this;
	},

	removeLayer: function (layer) {

		if (layer instanceof L.LayerGroup)
		{
			var array = [];
			for (var i in layer._layers) {
				array.push(layer._layers[i]);
			}
			return this.removeLayers(array);
		}

		//Non point layers
		if (!layer.getLatLng) {
			this._nonPointGroup.removeLayer(layer);
			return this;
		}

		if (!this._map) {
			if (!this._arraySplice(this._needsClustering, layer) && this.hasLayer(layer)) {
				this._needsRemoving.push(layer);
			}
			return this;
		}

		if (!layer.__parent) {
			return this;
		}

		if (this._unspiderfy) {
			this._unspiderfy();
			this._unspiderfyLayer(layer);
		}

		//Remove the marker from clusters
		this._removeLayer(layer, true);

		if (this._featureGroup.hasLayer(layer)) {
			this._featureGroup.removeLayer(layer);
			if (layer.setOpacity) {
				layer.setOpacity(1);
			}
		}

		return this;
	},

	//Takes an array of markers and adds them in bulk
	addLayers: function (layersArray) {
		var fg = this._featureGroup,
			npg = this._nonPointGroup,
			chunked = this.options.chunkedLoading,
			chunkInterval = this.options.chunkInterval,
			chunkProgress = this.options.chunkProgress,
			newMarkers, i, l, m;

		if (this._map) {
			var offset = 0,
				started = (new Date()).getTime();
			var process = L.bind(function () {
				var start = (new Date()).getTime();
				for (; offset < layersArray.length; offset++) {
					if (chunked && offset % 200 === 0) {
						// every couple hundred markers, instrument the time elapsed since processing started:
						var elapsed = (new Date()).getTime() - start;
						if (elapsed > chunkInterval) {
							break; // been working too hard, time to take a break :-)
						}
					}

					m = layersArray[offset];

					//Not point data, can't be clustered
					if (!m.getLatLng) {
						npg.addLayer(m);
						continue;
					}

					if (this.hasLayer(m)) {
						continue;
					}

					this._addLayer(m, this._maxZoom);

					//If we just made a cluster of size 2 then we need to remove the other marker from the map (if it is) or we never will
					if (m.__parent) {
						if (m.__parent.getChildCount() === 2) {
							var markers = m.__parent.getAllChildMarkers(),
								otherMarker = markers[0] === m ? markers[1] : markers[0];
							fg.removeLayer(otherMarker);
						}
					}
				}

				if (chunkProgress) {
					// report progress and time elapsed:
					chunkProgress(offset, layersArray.length, (new Date()).getTime() - started);
				}

				if (offset === layersArray.length) {
					//Update the icons of all those visible clusters that were affected
					this._featureGroup.eachLayer(function (c) {
						if (c instanceof L.MarkerCluster && c._iconNeedsUpdate) {
							c._updateIcon();
						}
					});

					this._topClusterLevel._recursivelyAddChildrenToMap(null, this._zoom, this._currentShownBounds);
				} else {
					setTimeout(process, this.options.chunkDelay);
				}
			}, this);

			process();
		} else {
			newMarkers = [];
			for (i = 0, l = layersArray.length; i < l; i++) {
				m = layersArray[i];

				//Not point data, can't be clustered
				if (!m.getLatLng) {
					npg.addLayer(m);
					continue;
				}

				if (this.hasLayer(m)) {
					continue;
				}

				newMarkers.push(m);
			}
			this._needsClustering = this._needsClustering.concat(newMarkers);
		}
		return this;
	},

	//Takes an array of markers and removes them in bulk
	removeLayers: function (layersArray) {
		var i, l, m,
			fg = this._featureGroup,
			npg = this._nonPointGroup;

		if (!this._map) {
			for (i = 0, l = layersArray.length; i < l; i++) {
				m = layersArray[i];
				this._arraySplice(this._needsClustering, m);
				npg.removeLayer(m);
			}
			return this;
		}

		for (i = 0, l = layersArray.length; i < l; i++) {
			m = layersArray[i];

			if (!m.__parent) {
				npg.removeLayer(m);
				continue;
			}

			this._removeLayer(m, true, true);

			if (fg.hasLayer(m)) {
				fg.removeLayer(m);
				if (m.setOpacity) {
					m.setOpacity(1);
				}
			}
		}

		//Fix up the clusters and markers on the map
		this._topClusterLevel._recursivelyAddChildrenToMap(null, this._zoom, this._currentShownBounds);

		fg.eachLayer(function (c) {
			if (c instanceof L.MarkerCluster) {
				c._updateIcon();
			}
		});

		return this;
	},

	//Removes all layers from the MarkerClusterGroup
	clearLayers: function () {
		//Need our own special implementation as the LayerGroup one doesn't work for us

		//If we aren't on the map (yet), blow away the markers we know of
		if (!this._map) {
			this._needsClustering = [];
			delete this._gridClusters;
			delete this._gridUnclustered;
		}

		if (this._noanimationUnspiderfy) {
			this._noanimationUnspiderfy();
		}

		//Remove all the visible layers
		this._featureGroup.clearLayers();
		this._nonPointGroup.clearLayers();

		this.eachLayer(function (marker) {
			delete marker.__parent;
		});

		if (this._map) {
			//Reset _topClusterLevel and the DistanceGrids
			this._generateInitialClusters();
		}

		return this;
	},

	//Override FeatureGroup.getBounds as it doesn't work
	getBounds: function () {
		var bounds = new L.LatLngBounds();

		if (this._topClusterLevel) {
			bounds.extend(this._topClusterLevel._bounds);
		}

		for (var i = this._needsClustering.length - 1; i >= 0; i--) {
			bounds.extend(this._needsClustering[i].getLatLng());
		}

		bounds.extend(this._nonPointGroup.getBounds());

		return bounds;
	},

	//Overrides LayerGroup.eachLayer
	eachLayer: function (method, context) {
		var markers = this._needsClustering.slice(),
			i;

		if (this._topClusterLevel) {
			this._topClusterLevel.getAllChildMarkers(markers);
		}

		for (i = markers.length - 1; i >= 0; i--) {
			method.call(context, markers[i]);
		}

		this._nonPointGroup.eachLayer(method, context);
	},

	//Overrides LayerGroup.getLayers
	getLayers: function () {
		var layers = [];
		this.eachLayer(function (l) {
			layers.push(l);
		});
		return layers;
	},

	//Overrides LayerGroup.getLayer, WARNING: Really bad performance
	getLayer: function (id) {
		var result = null;

		this.eachLayer(function (l) {
			if (L.stamp(l) === id) {
				result = l;
			}
		});

		return result;
	},

	//Returns true if the given layer is in this MarkerClusterGroup
	hasLayer: function (layer) {
		if (!layer) {
			return false;
		}

		var i, anArray = this._needsClustering;

		for (i = anArray.length - 1; i >= 0; i--) {
			if (anArray[i] === layer) {
				return true;
			}
		}

		anArray = this._needsRemoving;
		for (i = anArray.length - 1; i >= 0; i--) {
			if (anArray[i] === layer) {
				return false;
			}
		}

		return !!(layer.__parent && layer.__parent._group === this) || this._nonPointGroup.hasLayer(layer);
	},

	//Zoom down to show the given layer (spiderfying if necessary) then calls the callback
	zoomToShowLayer: function (layer, callback) {

		var showMarker = function () {
			if ((layer._icon || layer.__parent._icon) && !this._inZoomAnimation) {
				this._map.off('moveend', showMarker, this);
				this.off('animationend', showMarker, this);

				if (layer._icon) {
					callback();
				} else if (layer.__parent._icon) {
					var afterSpiderfy = function () {
						this.off('spiderfied', afterSpiderfy, this);
						callback();
					};

					this.on('spiderfied', afterSpiderfy, this);
					layer.__parent.spiderfy();
				}
			}
		};

		if (layer._icon && this._map.getBounds().contains(layer.getLatLng())) {
			//Layer is visible ond on screen, immediate return
			callback();
		} else if (layer.__parent._zoom < this._map.getZoom()) {
			//Layer should be visible at this zoom level. It must not be on screen so just pan over to it
			this._map.on('moveend', showMarker, this);
			this._map.panTo(layer.getLatLng());
		} else {
			var moveStart = function () {
				this._map.off('movestart', moveStart, this);
				moveStart = null;
			};

			this._map.on('movestart', moveStart, this);
			this._map.on('moveend', showMarker, this);
			this.on('animationend', showMarker, this);
			layer.__parent.zoomToBounds();

			if (moveStart) {
				//Never started moving, must already be there, probably need clustering however
				showMarker.call(this);
			}
		}
	},

	//Overrides FeatureGroup.onAdd
	onAdd: function (map) {
		this._map = map;
		var i, l, layer;

		if (!isFinite(this._map.getMaxZoom())) {
			throw "Map has no maxZoom specified";
		}

		this._featureGroup.onAdd(map);
		this._nonPointGroup.onAdd(map);

		if (!this._gridClusters) {
			this._generateInitialClusters();
		}

		for (i = 0, l = this._needsRemoving.length; i < l; i++) {
			layer = this._needsRemoving[i];
			this._removeLayer(layer, true);
		}
		this._needsRemoving = [];

		//Remember the current zoom level and bounds
		this._zoom = this._map.getZoom();
		this._currentShownBounds = this._getExpandedVisibleBounds();

		this._map.on('zoomend', this._zoomEnd, this);
		this._map.on('moveend', this._moveEnd, this);

		if (this._spiderfierOnAdd) { //TODO FIXME: Not sure how to have spiderfier add something on here nicely
			this._spiderfierOnAdd();
		}

		this._bindEvents();

		//Actually add our markers to the map:
		l = this._needsClustering;
		this._needsClustering = [];
		this.addLayers(l);
	},

	//Overrides FeatureGroup.onRemove
	onRemove: function (map) {
		map.off('zoomend', this._zoomEnd, this);
		map.off('moveend', this._moveEnd, this);

		this._unbindEvents();

		//In case we are in a cluster animation
		this._map._mapPane.className = this._map._mapPane.className.replace(' leaflet-cluster-anim', '');

		if (this._spiderfierOnRemove) { //TODO FIXME: Not sure how to have spiderfier add something on here nicely
			this._spiderfierOnRemove();
		}



		//Clean up all the layers we added to the map
		this._hideCoverage();
		this._featureGroup.onRemove(map);
		this._nonPointGroup.onRemove(map);

		this._featureGroup.clearLayers();

		this._map = null;
	},

	getVisibleParent: function (marker) {
		var vMarker = marker;
		while (vMarker && !vMarker._icon) {
			vMarker = vMarker.__parent;
		}
		return vMarker || null;
	},

	//Remove the given object from the given array
	_arraySplice: function (anArray, obj) {
		for (var i = anArray.length - 1; i >= 0; i--) {
			if (anArray[i] === obj) {
				anArray.splice(i, 1);
				return true;
			}
		}
	},

	//Internal function for removing a marker from everything.
	//dontUpdateMap: set to true if you will handle updating the map manually (for bulk functions)
	_removeLayer: function (marker, removeFromDistanceGrid, dontUpdateMap) {
		var gridClusters = this._gridClusters,
			gridUnclustered = this._gridUnclustered,
			fg = this._featureGroup,
			map = this._map;

		//Remove the marker from distance clusters it might be in
		if (removeFromDistanceGrid) {
			for (var z = this._maxZoom; z >= 0; z--) {
				if (!gridUnclustered[z].removeObject(marker, map.project(marker.getLatLng(), z))) {
					break;
				}
			}
		}

		//Work our way up the clusters removing them as we go if required
		var cluster = marker.__parent,
			markers = cluster._markers,
			otherMarker;

		//Remove the marker from the immediate parents marker list
		this._arraySplice(markers, marker);

		while (cluster) {
			cluster._childCount--;

			if (cluster._zoom < 0) {
				//Top level, do nothing
				break;
			} else if (removeFromDistanceGrid && cluster._childCount <= 1) { //Cluster no longer required
				//We need to push the other marker up to the parent
				otherMarker = cluster._markers[0] === marker ? cluster._markers[1] : cluster._markers[0];

				//Update distance grid
				gridClusters[cluster._zoom].removeObject(cluster, map.project(cluster._cLatLng, cluster._zoom));
				gridUnclustered[cluster._zoom].addObject(otherMarker, map.project(otherMarker.getLatLng(), cluster._zoom));

				//Move otherMarker up to parent
				this._arraySplice(cluster.__parent._childClusters, cluster);
				cluster.__parent._markers.push(otherMarker);
				otherMarker.__parent = cluster.__parent;

				if (cluster._icon) {
					//Cluster is currently on the map, need to put the marker on the map instead
					fg.removeLayer(cluster);
					if (!dontUpdateMap) {
						fg.addLayer(otherMarker);
					}
				}
			} else {
				cluster._recalculateBounds();
				if (!dontUpdateMap || !cluster._icon) {
					cluster._updateIcon();
				}
			}

			cluster = cluster.__parent;
		}

		delete marker.__parent;
	},

	_isOrIsParent: function (el, oel) {
		while (oel) {
			if (el === oel) {
				return true;
			}
			oel = oel.parentNode;
		}
		return false;
	},

	_propagateEvent: function (e) {
		if (e.layer instanceof L.MarkerCluster) {
			//Prevent multiple clustermouseover/off events if the icon is made up of stacked divs (Doesn't work in ie <= 8, no relatedTarget)
			if (e.originalEvent && this._isOrIsParent(e.layer._icon, e.originalEvent.relatedTarget)) {
				return;
			}
			e.type = 'cluster' + e.type;
		}

		this.fire(e.type, e);
	},

	//Default functionality
	_defaultIconCreateFunction: function (cluster) {
		var childCount = cluster.getChildCount();

		var c = ' marker-cluster-';
		if (childCount < 10) {
			c += 'small';
		} else if (childCount < 100) {
			c += 'medium';
		} else {
			c += 'large';
		}

		return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>', className: 'marker-cluster' + c, iconSize: new L.Point(40, 40) });
	},

	_bindEvents: function () {
		var map = this._map,
		    spiderfyOnMaxZoom = this.options.spiderfyOnMaxZoom,
		    showCoverageOnHover = this.options.showCoverageOnHover,
		    zoomToBoundsOnClick = this.options.zoomToBoundsOnClick;

		//Zoom on cluster click or spiderfy if we are at the lowest level
		if (spiderfyOnMaxZoom || zoomToBoundsOnClick) {
			this.on('clusterclick', this._zoomOrSpiderfy, this);
		}

		//Show convex hull (boundary) polygon on mouse over
		if (showCoverageOnHover) {
			this.on('clustermouseover', this._showCoverage, this);
			this.on('clustermouseout', this._hideCoverage, this);
			map.on('zoomend', this._hideCoverage, this);
		}
	},

	_zoomOrSpiderfy: function (e) {
		var map = this._map;
		if (map.getMaxZoom() === map.getZoom()) {
			if (this.options.spiderfyOnMaxZoom) {
				e.layer.spiderfy();
			}
		} else if (this.options.zoomToBoundsOnClick) {
			e.layer.zoomToBounds();
		}

		// Focus the map again for keyboard users.
		if (e.originalEvent && e.originalEvent.keyCode === 13) {
			map._container.focus();
		}
	},

	_showCoverage: function (e) {
		var map = this._map;
		if (this._inZoomAnimation) {
			return;
		}
		if (this._shownPolygon) {
			map.removeLayer(this._shownPolygon);
		}
		if (e.layer.getChildCount() > 2 && e.layer !== this._spiderfied) {
			this._shownPolygon = new L.Polygon(e.layer.getConvexHull(), this.options.polygonOptions);
			map.addLayer(this._shownPolygon);
		}
	},

	_hideCoverage: function () {
		if (this._shownPolygon) {
			this._map.removeLayer(this._shownPolygon);
			this._shownPolygon = null;
		}
	},

	_unbindEvents: function () {
		var spiderfyOnMaxZoom = this.options.spiderfyOnMaxZoom,
			showCoverageOnHover = this.options.showCoverageOnHover,
			zoomToBoundsOnClick = this.options.zoomToBoundsOnClick,
			map = this._map;

		if (spiderfyOnMaxZoom || zoomToBoundsOnClick) {
			this.off('clusterclick', this._zoomOrSpiderfy, this);
		}
		if (showCoverageOnHover) {
			this.off('clustermouseover', this._showCoverage, this);
			this.off('clustermouseout', this._hideCoverage, this);
			map.off('zoomend', this._hideCoverage, this);
		}
	},

	_zoomEnd: function () {
		if (!this._map) { //May have been removed from the map by a zoomEnd handler
			return;
		}
		this._mergeSplitClusters();

		this._zoom = this._map._zoom;
		this._currentShownBounds = this._getExpandedVisibleBounds();
	},

	_moveEnd: function () {
		if (this._inZoomAnimation) {
			return;
		}

		var newBounds = this._getExpandedVisibleBounds();

		this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, this._zoom, newBounds);
		this._topClusterLevel._recursivelyAddChildrenToMap(null, this._map._zoom, newBounds);

		this._currentShownBounds = newBounds;
		return;
	},

	_generateInitialClusters: function () {
		var maxZoom = this._map.getMaxZoom(),
			radius = this.options.maxClusterRadius,
			radiusFn = radius;
	
		//If we just set maxClusterRadius to a single number, we need to create
		//a simple function to return that number. Otherwise, we just have to
		//use the function we've passed in.
		if (typeof radius !== "function") {
			radiusFn = function () { return radius; };
		}

		if (this.options.disableClusteringAtZoom) {
			maxZoom = this.options.disableClusteringAtZoom - 1;
		}
		this._maxZoom = maxZoom;
		this._gridClusters = {};
		this._gridUnclustered = {};
	
		//Set up DistanceGrids for each zoom
		for (var zoom = maxZoom; zoom >= 0; zoom--) {
			this._gridClusters[zoom] = new L.DistanceGrid(radiusFn(zoom));
			this._gridUnclustered[zoom] = new L.DistanceGrid(radiusFn(zoom));
		}

		this._topClusterLevel = new L.MarkerCluster(this, -1);
	},

	//Zoom: Zoom to start adding at (Pass this._maxZoom to start at the bottom)
	_addLayer: function (layer, zoom) {
		var gridClusters = this._gridClusters,
		    gridUnclustered = this._gridUnclustered,
		    markerPoint, z;

		if (this.options.singleMarkerMode) {
			layer.options.icon = this.options.iconCreateFunction({
				getChildCount: function () {
					return 1;
				},
				getAllChildMarkers: function () {
					return [layer];
				}
			});
		}

		//Find the lowest zoom level to slot this one in
		for (; zoom >= 0; zoom--) {
			markerPoint = this._map.project(layer.getLatLng(), zoom); // calculate pixel position

			//Try find a cluster close by
			var closest = gridClusters[zoom].getNearObject(markerPoint);
			if (closest) {
				closest._addChild(layer);
				layer.__parent = closest;
				return;
			}

			//Try find a marker close by to form a new cluster with
			closest = gridUnclustered[zoom].getNearObject(markerPoint);
			if (closest) {
				var parent = closest.__parent;
				if (parent) {
					this._removeLayer(closest, false);
				}

				//Create new cluster with these 2 in it

				var newCluster = new L.MarkerCluster(this, zoom, closest, layer);
				gridClusters[zoom].addObject(newCluster, this._map.project(newCluster._cLatLng, zoom));
				closest.__parent = newCluster;
				layer.__parent = newCluster;

				//First create any new intermediate parent clusters that don't exist
				var lastParent = newCluster;
				for (z = zoom - 1; z > parent._zoom; z--) {
					lastParent = new L.MarkerCluster(this, z, lastParent);
					gridClusters[z].addObject(lastParent, this._map.project(closest.getLatLng(), z));
				}
				parent._addChild(lastParent);

				//Remove closest from this zoom level and any above that it is in, replace with newCluster
				for (z = zoom; z >= 0; z--) {
					if (!gridUnclustered[z].removeObject(closest, this._map.project(closest.getLatLng(), z))) {
						break;
					}
				}

				return;
			}

			//Didn't manage to cluster in at this zoom, record us as a marker here and continue upwards
			gridUnclustered[zoom].addObject(layer, markerPoint);
		}

		//Didn't get in anything, add us to the top
		this._topClusterLevel._addChild(layer);
		layer.__parent = this._topClusterLevel;
		return;
	},

	//Enqueue code to fire after the marker expand/contract has happened
	_enqueue: function (fn) {
		this._queue.push(fn);
		if (!this._queueTimeout) {
			this._queueTimeout = setTimeout(L.bind(this._processQueue, this), 300);
		}
	},
	_processQueue: function () {
		for (var i = 0; i < this._queue.length; i++) {
			this._queue[i].call(this);
		}
		this._queue.length = 0;
		clearTimeout(this._queueTimeout);
		this._queueTimeout = null;
	},

	//Merge and split any existing clusters that are too big or small
	_mergeSplitClusters: function () {

		//Incase we are starting to split before the animation finished
		this._processQueue();

		if (this._zoom < this._map._zoom && this._currentShownBounds.intersects(this._getExpandedVisibleBounds())) { //Zoom in, split
			this._animationStart();
			//Remove clusters now off screen
			this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, this._zoom, this._getExpandedVisibleBounds());

			this._animationZoomIn(this._zoom, this._map._zoom);

		} else if (this._zoom > this._map._zoom) { //Zoom out, merge
			this._animationStart();

			this._animationZoomOut(this._zoom, this._map._zoom);
		} else {
			this._moveEnd();
		}
	},

	//Gets the maps visible bounds expanded in each direction by the size of the screen (so the user cannot see an area we do not cover in one pan)
	_getExpandedVisibleBounds: function () {
		if (!this.options.removeOutsideVisibleBounds) {
			return this._map.getBounds();
		}

		var map = this._map,
			bounds = map.getBounds(),
			sw = bounds._southWest,
			ne = bounds._northEast,
			latDiff = L.Browser.mobile ? 0 : Math.abs(sw.lat - ne.lat),
			lngDiff = L.Browser.mobile ? 0 : Math.abs(sw.lng - ne.lng);

		return new L.LatLngBounds(
			new L.LatLng(sw.lat - latDiff, sw.lng - lngDiff, true),
			new L.LatLng(ne.lat + latDiff, ne.lng + lngDiff, true));
	},

	//Shared animation code
	_animationAddLayerNonAnimated: function (layer, newCluster) {
		if (newCluster === layer) {
			this._featureGroup.addLayer(layer);
		} else if (newCluster._childCount === 2) {
			newCluster._addToMap();

			var markers = newCluster.getAllChildMarkers();
			this._featureGroup.removeLayer(markers[0]);
			this._featureGroup.removeLayer(markers[1]);
		} else {
			newCluster._updateIcon();
		}
	}
});

L.MarkerClusterGroup.include(!L.DomUtil.TRANSITION ? {

	//Non Animated versions of everything
	_animationStart: function () {
		//Do nothing...
	},
	_animationZoomIn: function (previousZoomLevel, newZoomLevel) {
		this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, previousZoomLevel);
		this._topClusterLevel._recursivelyAddChildrenToMap(null, newZoomLevel, this._getExpandedVisibleBounds());

		//We didn't actually animate, but we use this event to mean "clustering animations have finished"
		this.fire('animationend');
	},
	_animationZoomOut: function (previousZoomLevel, newZoomLevel) {
		this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, previousZoomLevel);
		this._topClusterLevel._recursivelyAddChildrenToMap(null, newZoomLevel, this._getExpandedVisibleBounds());

		//We didn't actually animate, but we use this event to mean "clustering animations have finished"
		this.fire('animationend');
	},
	_animationAddLayer: function (layer, newCluster) {
		this._animationAddLayerNonAnimated(layer, newCluster);
	}
} : {

	//Animated versions here
	_animationStart: function () {
		this._map._mapPane.className += ' leaflet-cluster-anim';
		this._inZoomAnimation++;
	},
	_animationEnd: function () {
		if (this._map) {
			this._map._mapPane.className = this._map._mapPane.className.replace(' leaflet-cluster-anim', '');
		}
		this._inZoomAnimation--;
		this.fire('animationend');
	},
	_animationZoomIn: function (previousZoomLevel, newZoomLevel) {
		var bounds = this._getExpandedVisibleBounds(),
		    fg = this._featureGroup,
		    i;

		//Add all children of current clusters to map and remove those clusters from map
		this._topClusterLevel._recursively(bounds, previousZoomLevel, 0, function (c) {
			var startPos = c._latlng,
				markers = c._markers,
				m;

			if (!bounds.contains(startPos)) {
				startPos = null;
			}

			if (c._isSingleParent() && previousZoomLevel + 1 === newZoomLevel) { //Immediately add the new child and remove us
				fg.removeLayer(c);
				c._recursivelyAddChildrenToMap(null, newZoomLevel, bounds);
			} else {
				//Fade out old cluster
				c.setOpacity(0);
				c._recursivelyAddChildrenToMap(startPos, newZoomLevel, bounds);
			}

			//Remove all markers that aren't visible any more
			//TODO: Do we actually need to do this on the higher levels too?
			for (i = markers.length - 1; i >= 0; i--) {
				m = markers[i];
				if (!bounds.contains(m._latlng)) {
					fg.removeLayer(m);
				}
			}

		});

		this._forceLayout();

		//Update opacities
		this._topClusterLevel._recursivelyBecomeVisible(bounds, newZoomLevel);
		//TODO Maybe? Update markers in _recursivelyBecomeVisible
		fg.eachLayer(function (n) {
			if (!(n instanceof L.MarkerCluster) && n._icon) {
				n.setOpacity(1);
			}
		});

		//update the positions of the just added clusters/markers
		this._topClusterLevel._recursively(bounds, previousZoomLevel, newZoomLevel, function (c) {
			c._recursivelyRestoreChildPositions(newZoomLevel);
		});

		//Remove the old clusters and close the zoom animation
		this._enqueue(function () {
			//update the positions of the just added clusters/markers
			this._topClusterLevel._recursively(bounds, previousZoomLevel, 0, function (c) {
				fg.removeLayer(c);
				c.setOpacity(1);
			});

			this._animationEnd();
		});
	},

	_animationZoomOut: function (previousZoomLevel, newZoomLevel) {
		this._animationZoomOutSingle(this._topClusterLevel, previousZoomLevel - 1, newZoomLevel);

		//Need to add markers for those that weren't on the map before but are now
		this._topClusterLevel._recursivelyAddChildrenToMap(null, newZoomLevel, this._getExpandedVisibleBounds());
		//Remove markers that were on the map before but won't be now
		this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, previousZoomLevel, this._getExpandedVisibleBounds());
	},
	_animationZoomOutSingle: function (cluster, previousZoomLevel, newZoomLevel) {
		var bounds = this._getExpandedVisibleBounds();

		//Animate all of the markers in the clusters to move to their cluster center point
		cluster._recursivelyAnimateChildrenInAndAddSelfToMap(bounds, previousZoomLevel + 1, newZoomLevel);

		var me = this;

		//Update the opacity (If we immediately set it they won't animate)
		this._forceLayout();
		cluster._recursivelyBecomeVisible(bounds, newZoomLevel);

		//TODO: Maybe use the transition timing stuff to make this more reliable
		//When the animations are done, tidy up
		this._enqueue(function () {

			//This cluster stopped being a cluster before the timeout fired
			if (cluster._childCount === 1) {
				var m = cluster._markers[0];
				//If we were in a cluster animation at the time then the opacity and position of our child could be wrong now, so fix it
				m.setLatLng(m.getLatLng());
				if (m.setOpacity) {
					m.setOpacity(1);
				}
			} else {
				cluster._recursively(bounds, newZoomLevel, 0, function (c) {
					c._recursivelyRemoveChildrenFromMap(bounds, previousZoomLevel + 1);
				});
			}
			me._animationEnd();
		});
	},
	_animationAddLayer: function (layer, newCluster) {
		var me = this,
			fg = this._featureGroup;

		fg.addLayer(layer);
		if (newCluster !== layer) {
			if (newCluster._childCount > 2) { //Was already a cluster

				newCluster._updateIcon();
				this._forceLayout();
				this._animationStart();

				layer._setPos(this._map.latLngToLayerPoint(newCluster.getLatLng()));
				layer.setOpacity(0);

				this._enqueue(function () {
					fg.removeLayer(layer);
					layer.setOpacity(1);

					me._animationEnd();
				});

			} else { //Just became a cluster
				this._forceLayout();

				me._animationStart();
				me._animationZoomOutSingle(newCluster, this._map.getMaxZoom(), this._map.getZoom());
			}
		}
	},

	//Force a browser layout of stuff in the map
	// Should apply the current opacity and location to all elements so we can update them again for an animation
	_forceLayout: function () {
		//In my testing this works, infact offsetWidth of any element seems to work.
		//Could loop all this._layers and do this for each _icon if it stops working

		L.Util.falseFn(document.body.offsetWidth);
	}
});

L.markerClusterGroup = function (options) {
	return new L.MarkerClusterGroup(options);
};


L.MarkerCluster = L.Marker.extend({
	initialize: function (group, zoom, a, b) {

		L.Marker.prototype.initialize.call(this, a ? (a._cLatLng || a.getLatLng()) : new L.LatLng(0, 0), { icon: this });


		this._group = group;
		this._zoom = zoom;

		this._markers = [];
		this._childClusters = [];
		this._childCount = 0;
		this._iconNeedsUpdate = true;

		this._bounds = new L.LatLngBounds();

		if (a) {
			this._addChild(a);
		}
		if (b) {
			this._addChild(b);
		}
	},

	//Recursively retrieve all child markers of this cluster
	getAllChildMarkers: function (storageArray) {
		storageArray = storageArray || [];

		for (var i = this._childClusters.length - 1; i >= 0; i--) {
			this._childClusters[i].getAllChildMarkers(storageArray);
		}

		for (var j = this._markers.length - 1; j >= 0; j--) {
			storageArray.push(this._markers[j]);
		}

		return storageArray;
	},

	//Returns the count of how many child markers we have
	getChildCount: function () {
		return this._childCount;
	},

	//Zoom to the minimum of showing all of the child markers, or the extents of this cluster
	zoomToBounds: function () {
		var childClusters = this._childClusters.slice(),
			map = this._group._map,
			boundsZoom = map.getBoundsZoom(this._bounds),
			zoom = this._zoom + 1,
			mapZoom = map.getZoom(),
			i;

		//calculate how far we need to zoom down to see all of the markers
		while (childClusters.length > 0 && boundsZoom > zoom) {
			zoom++;
			var newClusters = [];
			for (i = 0; i < childClusters.length; i++) {
				newClusters = newClusters.concat(childClusters[i]._childClusters);
			}
			childClusters = newClusters;
		}

		if (boundsZoom > zoom) {
			this._group._map.setView(this._latlng, zoom);
		} else if (boundsZoom <= mapZoom) { //If fitBounds wouldn't zoom us down, zoom us down instead
			this._group._map.setView(this._latlng, mapZoom + 1);
		} else {
			this._group._map.fitBounds(this._bounds);
		}
	},

	getBounds: function () {
		var bounds = new L.LatLngBounds();
		bounds.extend(this._bounds);
		return bounds;
	},

	_updateIcon: function () {
		this._iconNeedsUpdate = true;
		if (this._icon) {
			this.setIcon(this);
		}
	},

	//Cludge for Icon, we pretend to be an icon for performance
	createIcon: function () {
		if (this._iconNeedsUpdate) {
			this._iconObj = this._group.options.iconCreateFunction(this);
			this._iconNeedsUpdate = false;
		}
		return this._iconObj.createIcon();
	},
	createShadow: function () {
		return this._iconObj.createShadow();
	},


	_addChild: function (new1, isNotificationFromChild) {

		this._iconNeedsUpdate = true;
		this._expandBounds(new1);

		if (new1 instanceof L.MarkerCluster) {
			if (!isNotificationFromChild) {
				this._childClusters.push(new1);
				new1.__parent = this;
			}
			this._childCount += new1._childCount;
		} else {
			if (!isNotificationFromChild) {
				this._markers.push(new1);
			}
			this._childCount++;
		}

		if (this.__parent) {
			this.__parent._addChild(new1, true);
		}
	},

	//Expand our bounds and tell our parent to
	_expandBounds: function (marker) {
		var addedCount,
		    addedLatLng = marker._wLatLng || marker._latlng;

		if (marker instanceof L.MarkerCluster) {
			this._bounds.extend(marker._bounds);
			addedCount = marker._childCount;
		} else {
			this._bounds.extend(addedLatLng);
			addedCount = 1;
		}

		if (!this._cLatLng) {
			// when clustering, take position of the first point as the cluster center
			this._cLatLng = marker._cLatLng || addedLatLng;
		}

		// when showing clusters, take weighted average of all points as cluster center
		var totalCount = this._childCount + addedCount;

		//Calculate weighted latlng for display
		if (!this._wLatLng) {
			this._latlng = this._wLatLng = new L.LatLng(addedLatLng.lat, addedLatLng.lng);
		} else {
			this._wLatLng.lat = (addedLatLng.lat * addedCount + this._wLatLng.lat * this._childCount) / totalCount;
			this._wLatLng.lng = (addedLatLng.lng * addedCount + this._wLatLng.lng * this._childCount) / totalCount;
		}
	},

	//Set our markers position as given and add it to the map
	_addToMap: function (startPos) {
		if (startPos) {
			this._backupLatlng = this._latlng;
			this.setLatLng(startPos);
		}
		this._group._featureGroup.addLayer(this);
	},

	_recursivelyAnimateChildrenIn: function (bounds, center, maxZoom) {
		this._recursively(bounds, 0, maxZoom - 1,
			function (c) {
				var markers = c._markers,
					i, m;
				for (i = markers.length - 1; i >= 0; i--) {
					m = markers[i];

					//Only do it if the icon is still on the map
					if (m._icon) {
						m._setPos(center);
						m.setOpacity(0);
					}
				}
			},
			function (c) {
				var childClusters = c._childClusters,
					j, cm;
				for (j = childClusters.length - 1; j >= 0; j--) {
					cm = childClusters[j];
					if (cm._icon) {
						cm._setPos(center);
						cm.setOpacity(0);
					}
				}
			}
		);
	},

	_recursivelyAnimateChildrenInAndAddSelfToMap: function (bounds, previousZoomLevel, newZoomLevel) {
		this._recursively(bounds, newZoomLevel, 0,
			function (c) {
				c._recursivelyAnimateChildrenIn(bounds, c._group._map.latLngToLayerPoint(c.getLatLng()).round(), previousZoomLevel);

				//TODO: depthToAnimateIn affects _isSingleParent, if there is a multizoom we may/may not be.
				//As a hack we only do a animation free zoom on a single level zoom, if someone does multiple levels then we always animate
				if (c._isSingleParent() && previousZoomLevel - 1 === newZoomLevel) {
					c.setOpacity(1);
					c._recursivelyRemoveChildrenFromMap(bounds, previousZoomLevel); //Immediately remove our children as we are replacing them. TODO previousBounds not bounds
				} else {
					c.setOpacity(0);
				}

				c._addToMap();
			}
		);
	},

	_recursivelyBecomeVisible: function (bounds, zoomLevel) {
		this._recursively(bounds, 0, zoomLevel, null, function (c) {
			c.setOpacity(1);
		});
	},

	_recursivelyAddChildrenToMap: function (startPos, zoomLevel, bounds) {
		this._recursively(bounds, -1, zoomLevel,
			function (c) {
				if (zoomLevel === c._zoom) {
					return;
				}

				//Add our child markers at startPos (so they can be animated out)
				for (var i = c._markers.length - 1; i >= 0; i--) {
					var nm = c._markers[i];

					if (!bounds.contains(nm._latlng)) {
						continue;
					}

					if (startPos) {
						nm._backupLatlng = nm.getLatLng();

						nm.setLatLng(startPos);
						if (nm.setOpacity) {
							nm.setOpacity(0);
						}
					}

					c._group._featureGroup.addLayer(nm);
				}
			},
			function (c) {
				c._addToMap(startPos);
			}
		);
	},

	_recursivelyRestoreChildPositions: function (zoomLevel) {
		//Fix positions of child markers
		for (var i = this._markers.length - 1; i >= 0; i--) {
			var nm = this._markers[i];
			if (nm._backupLatlng) {
				nm.setLatLng(nm._backupLatlng);
				delete nm._backupLatlng;
			}
		}

		if (zoomLevel - 1 === this._zoom) {
			//Reposition child clusters
			for (var j = this._childClusters.length - 1; j >= 0; j--) {
				this._childClusters[j]._restorePosition();
			}
		} else {
			for (var k = this._childClusters.length - 1; k >= 0; k--) {
				this._childClusters[k]._recursivelyRestoreChildPositions(zoomLevel);
			}
		}
	},

	_restorePosition: function () {
		if (this._backupLatlng) {
			this.setLatLng(this._backupLatlng);
			delete this._backupLatlng;
		}
	},

	//exceptBounds: If set, don't remove any markers/clusters in it
	_recursivelyRemoveChildrenFromMap: function (previousBounds, zoomLevel, exceptBounds) {
		var m, i;
		this._recursively(previousBounds, -1, zoomLevel - 1,
			function (c) {
				//Remove markers at every level
				for (i = c._markers.length - 1; i >= 0; i--) {
					m = c._markers[i];
					if (!exceptBounds || !exceptBounds.contains(m._latlng)) {
						c._group._featureGroup.removeLayer(m);
						if (m.setOpacity) {
							m.setOpacity(1);
						}
					}
				}
			},
			function (c) {
				//Remove child clusters at just the bottom level
				for (i = c._childClusters.length - 1; i >= 0; i--) {
					m = c._childClusters[i];
					if (!exceptBounds || !exceptBounds.contains(m._latlng)) {
						c._group._featureGroup.removeLayer(m);
						if (m.setOpacity) {
							m.setOpacity(1);
						}
					}
				}
			}
		);
	},

	//Run the given functions recursively to this and child clusters
	// boundsToApplyTo: a L.LatLngBounds representing the bounds of what clusters to recurse in to
	// zoomLevelToStart: zoom level to start running functions (inclusive)
	// zoomLevelToStop: zoom level to stop running functions (inclusive)
	// runAtEveryLevel: function that takes an L.MarkerCluster as an argument that should be applied on every level
	// runAtBottomLevel: function that takes an L.MarkerCluster as an argument that should be applied at only the bottom level
	_recursively: function (boundsToApplyTo, zoomLevelToStart, zoomLevelToStop, runAtEveryLevel, runAtBottomLevel) {
		var childClusters = this._childClusters,
		    zoom = this._zoom,
			i, c;

		if (zoomLevelToStart > zoom) { //Still going down to required depth, just recurse to child clusters
			for (i = childClusters.length - 1; i >= 0; i--) {
				c = childClusters[i];
				if (boundsToApplyTo.intersects(c._bounds)) {
					c._recursively(boundsToApplyTo, zoomLevelToStart, zoomLevelToStop, runAtEveryLevel, runAtBottomLevel);
				}
			}
		} else { //In required depth

			if (runAtEveryLevel) {
				runAtEveryLevel(this);
			}
			if (runAtBottomLevel && this._zoom === zoomLevelToStop) {
				runAtBottomLevel(this);
			}

			//TODO: This loop is almost the same as above
			if (zoomLevelToStop > zoom) {
				for (i = childClusters.length - 1; i >= 0; i--) {
					c = childClusters[i];
					if (boundsToApplyTo.intersects(c._bounds)) {
						c._recursively(boundsToApplyTo, zoomLevelToStart, zoomLevelToStop, runAtEveryLevel, runAtBottomLevel);
					}
				}
			}
		}
	},

	_recalculateBounds: function () {
		var markers = this._markers,
			childClusters = this._childClusters,
			i;

		this._bounds = new L.LatLngBounds();
		delete this._wLatLng;

		for (i = markers.length - 1; i >= 0; i--) {
			this._expandBounds(markers[i]);
		}
		for (i = childClusters.length - 1; i >= 0; i--) {
			this._expandBounds(childClusters[i]);
		}
	},


	//Returns true if we are the parent of only one cluster and that cluster is the same as us
	_isSingleParent: function () {
		//Don't need to check this._markers as the rest won't work if there are any
		return this._childClusters.length > 0 && this._childClusters[0]._childCount === this._childCount;
	}
});



L.DistanceGrid = function (cellSize) {
	this._cellSize = cellSize;
	this._sqCellSize = cellSize * cellSize;
	this._grid = {};
	this._objectPoint = { };
};

L.DistanceGrid.prototype = {

	addObject: function (obj, point) {
		var x = this._getCoord(point.x),
		    y = this._getCoord(point.y),
		    grid = this._grid,
		    row = grid[y] = grid[y] || {},
		    cell = row[x] = row[x] || [],
		    stamp = L.Util.stamp(obj);

		this._objectPoint[stamp] = point;

		cell.push(obj);
	},

	updateObject: function (obj, point) {
		this.removeObject(obj);
		this.addObject(obj, point);
	},

	//Returns true if the object was found
	removeObject: function (obj, point) {
		var x = this._getCoord(point.x),
		    y = this._getCoord(point.y),
		    grid = this._grid,
		    row = grid[y] = grid[y] || {},
		    cell = row[x] = row[x] || [],
		    i, len;

		delete this._objectPoint[L.Util.stamp(obj)];

		for (i = 0, len = cell.length; i < len; i++) {
			if (cell[i] === obj) {

				cell.splice(i, 1);

				if (len === 1) {
					delete row[x];
				}

				return true;
			}
		}

	},

	eachObject: function (fn, context) {
		var i, j, k, len, row, cell, removed,
		    grid = this._grid;

		for (i in grid) {
			row = grid[i];

			for (j in row) {
				cell = row[j];

				for (k = 0, len = cell.length; k < len; k++) {
					removed = fn.call(context, cell[k]);
					if (removed) {
						k--;
						len--;
					}
				}
			}
		}
	},

	getNearObject: function (point) {
		var x = this._getCoord(point.x),
		    y = this._getCoord(point.y),
		    i, j, k, row, cell, len, obj, dist,
		    objectPoint = this._objectPoint,
		    closestDistSq = this._sqCellSize,
		    closest = null;

		for (i = y - 1; i <= y + 1; i++) {
			row = this._grid[i];
			if (row) {

				for (j = x - 1; j <= x + 1; j++) {
					cell = row[j];
					if (cell) {

						for (k = 0, len = cell.length; k < len; k++) {
							obj = cell[k];
							dist = this._sqDist(objectPoint[L.Util.stamp(obj)], point);
							if (dist < closestDistSq) {
								closestDistSq = dist;
								closest = obj;
							}
						}
					}
				}
			}
		}
		return closest;
	},

	_getCoord: function (x) {
		return Math.floor(x / this._cellSize);
	},

	_sqDist: function (p, p2) {
		var dx = p2.x - p.x,
		    dy = p2.y - p.y;
		return dx * dx + dy * dy;
	}
};




(function () {
	L.QuickHull = {

		
		getDistant: function (cpt, bl) {
			var vY = bl[1].lat - bl[0].lat,
				vX = bl[0].lng - bl[1].lng;
			return (vX * (cpt.lat - bl[0].lat) + vY * (cpt.lng - bl[0].lng));
		},

		
		findMostDistantPointFromBaseLine: function (baseLine, latLngs) {
			var maxD = 0,
				maxPt = null,
				newPoints = [],
				i, pt, d;

			for (i = latLngs.length - 1; i >= 0; i--) {
				pt = latLngs[i];
				d = this.getDistant(pt, baseLine);

				if (d > 0) {
					newPoints.push(pt);
				} else {
					continue;
				}

				if (d > maxD) {
					maxD = d;
					maxPt = pt;
				}
			}

			return { maxPoint: maxPt, newPoints: newPoints };
		},


		
		buildConvexHull: function (baseLine, latLngs) {
			var convexHullBaseLines = [],
				t = this.findMostDistantPointFromBaseLine(baseLine, latLngs);

			if (t.maxPoint) { // if there is still a point "outside" the base line
				convexHullBaseLines =
					convexHullBaseLines.concat(
						this.buildConvexHull([baseLine[0], t.maxPoint], t.newPoints)
					);
				convexHullBaseLines =
					convexHullBaseLines.concat(
						this.buildConvexHull([t.maxPoint, baseLine[1]], t.newPoints)
					);
				return convexHullBaseLines;
			} else {  // if there is no more point "outside" the base line, the current base line is part of the convex hull
				return [baseLine[0]];
			}
		},

		
		getConvexHull: function (latLngs) {
			// find first baseline
			var maxLat = false, minLat = false,
				maxPt = null, minPt = null,
				i;

			for (i = latLngs.length - 1; i >= 0; i--) {
				var pt = latLngs[i];
				if (maxLat === false || pt.lat > maxLat) {
					maxPt = pt;
					maxLat = pt.lat;
				}
				if (minLat === false || pt.lat < minLat) {
					minPt = pt;
					minLat = pt.lat;
				}
			}
			var ch = [].concat(this.buildConvexHull([minPt, maxPt], latLngs),
								this.buildConvexHull([maxPt, minPt], latLngs));
			return ch;
		}
	};
}());

L.MarkerCluster.include({
	getConvexHull: function () {
		var childMarkers = this.getAllChildMarkers(),
			points = [],
			p, i;

		for (i = childMarkers.length - 1; i >= 0; i--) {
			p = childMarkers[i].getLatLng();
			points.push(p);
		}

		return L.QuickHull.getConvexHull(points);
	}
});


//This code is 100% based on https://github.com/jawj/OverlappingMarkerSpiderfier-Leaflet
//Huge thanks to jawj for implementing it first to make my job easy :-)

L.MarkerCluster.include({

	_2PI: Math.PI * 2,
	_circleFootSeparation: 25, //related to circumference of circle
	_circleStartAngle: Math.PI / 6,

	_spiralFootSeparation:  28, //related to size of spiral (experiment!)
	_spiralLengthStart: 11,
	_spiralLengthFactor: 5,

	_circleSpiralSwitchover: 9, //show spiral instead of circle from this marker count upwards.
								// 0 -> always spiral; Infinity -> always circle

	spiderfy: function () {
		if (this._group._spiderfied === this || this._group._inZoomAnimation) {
			return;
		}

		var childMarkers = this.getAllChildMarkers(),
			group = this._group,
			map = group._map,
			center = map.latLngToLayerPoint(this._latlng),
			positions;

		this._group._unspiderfy();
		this._group._spiderfied = this;

		//TODO Maybe: childMarkers order by distance to center

		if (childMarkers.length >= this._circleSpiralSwitchover) {
			positions = this._generatePointsSpiral(childMarkers.length, center);
		} else {
			center.y += 10; //Otherwise circles look wrong
			positions = this._generatePointsCircle(childMarkers.length, center);
		}

		this._animationSpiderfy(childMarkers, positions);
	},

	unspiderfy: function (zoomDetails) {
		/// <param Name="zoomDetails">Argument from zoomanim if being called in a zoom animation or null otherwise</param>
		if (this._group._inZoomAnimation) {
			return;
		}
		this._animationUnspiderfy(zoomDetails);

		this._group._spiderfied = null;
	},

	_generatePointsCircle: function (count, centerPt) {
		var circumference = this._group.options.spiderfyDistanceMultiplier * this._circleFootSeparation * (2 + count),
			legLength = circumference / this._2PI,  //radius from circumference
			angleStep = this._2PI / count,
			res = [],
			i, angle;

		res.length = count;

		for (i = count - 1; i >= 0; i--) {
			angle = this._circleStartAngle + i * angleStep;
			res[i] = new L.Point(centerPt.x + legLength * Math.cos(angle), centerPt.y + legLength * Math.sin(angle))._round();
		}

		return res;
	},

	_generatePointsSpiral: function (count, centerPt) {
		var legLength = this._group.options.spiderfyDistanceMultiplier * this._spiralLengthStart,
			separation = this._group.options.spiderfyDistanceMultiplier * this._spiralFootSeparation,
			lengthFactor = this._group.options.spiderfyDistanceMultiplier * this._spiralLengthFactor,
			angle = 0,
			res = [],
			i;

		res.length = count;

		for (i = count - 1; i >= 0; i--) {
			angle += separation / legLength + i * 0.0005;
			res[i] = new L.Point(centerPt.x + legLength * Math.cos(angle), centerPt.y + legLength * Math.sin(angle))._round();
			legLength += this._2PI * lengthFactor / angle;
		}
		return res;
	},

	_noanimationUnspiderfy: function () {
		var group = this._group,
			map = group._map,
			fg = group._featureGroup,
			childMarkers = this.getAllChildMarkers(),
			m, i;

		this.setOpacity(1);
		for (i = childMarkers.length - 1; i >= 0; i--) {
			m = childMarkers[i];

			fg.removeLayer(m);

			if (m._preSpiderfyLatlng) {
				m.setLatLng(m._preSpiderfyLatlng);
				delete m._preSpiderfyLatlng;
			}
			if (m.setZIndexOffset) {
				m.setZIndexOffset(0);
			}

			if (m._spiderLeg) {
				map.removeLayer(m._spiderLeg);
				delete m._spiderLeg;
			}
		}

		group._spiderfied = null;
	}
});

L.MarkerCluster.include(!L.DomUtil.TRANSITION ? {
	//Non Animated versions of everything
	_animationSpiderfy: function (childMarkers, positions) {
		var group = this._group,
			map = group._map,
			fg = group._featureGroup,
			i, m, leg, newPos;

		for (i = childMarkers.length - 1; i >= 0; i--) {
			newPos = map.layerPointToLatLng(positions[i]);
			m = childMarkers[i];

			m._preSpiderfyLatlng = m._latlng;
			m.setLatLng(newPos);
			if (m.setZIndexOffset) {
				m.setZIndexOffset(1000000); //Make these appear on top of EVERYTHING
			}

			fg.addLayer(m);


			leg = new L.Polyline([this._latlng, newPos], { weight: 1.5, color: '#222' });
			map.addLayer(leg);
			m._spiderLeg = leg;
		}
		this.setOpacity(0.3);
		group.fire('spiderfied');
	},

	_animationUnspiderfy: function () {
		this._noanimationUnspiderfy();
	}
} : {
	//Animated versions here
	SVG_ANIMATION: (function () {
		return document.createElementNS('http://www.w3.org/2000/svg', 'animate').toString().indexOf('SVGAnimate') > -1;
	}()),

	_animationSpiderfy: function (childMarkers, positions) {
		var me = this,
			group = this._group,
			map = group._map,
			fg = group._featureGroup,
			thisLayerPos = map.latLngToLayerPoint(this._latlng),
			i, m, leg, newPos;

		//Add markers to map hidden at our center point
		for (i = childMarkers.length - 1; i >= 0; i--) {
			m = childMarkers[i];

			//If it is a marker, add it now and we'll animate it out
			if (m.setOpacity) {
				m.setZIndexOffset(1000000); //Make these appear on top of EVERYTHING
				m.setOpacity(0);
			
				fg.addLayer(m);

				m._setPos(thisLayerPos);
			} else {
				//Vectors just get immediately added
				fg.addLayer(m);
			}
		}

		group._forceLayout();
		group._animationStart();

		var initialLegOpacity = L.Path.SVG ? 0 : 0.3,
			xmlns = L.Path.SVG_NS;


		for (i = childMarkers.length - 1; i >= 0; i--) {
			newPos = map.layerPointToLatLng(positions[i]);
			m = childMarkers[i];

			//Move marker to new position
			m._preSpiderfyLatlng = m._latlng;
			m.setLatLng(newPos);
			
			if (m.setOpacity) {
				m.setOpacity(1);
			}


			//Add Legs.
			leg = new L.Polyline([me._latlng, newPos], { weight: 1.5, color: '#222', opacity: initialLegOpacity });
			map.addLayer(leg);
			m._spiderLeg = leg;

			//Following animations don't work for canvas
			if (!L.Path.SVG || !this.SVG_ANIMATION) {
				continue;
			}

			//How this works:
			//http://stackoverflow.com/questions/5924238/how-do-you-animate-an-svg-path-in-ios
			//http://dev.opera.com/articles/view/advanced-svg-animation-techniques/

			//Animate length
			var length = leg._path.getTotalLength();
			leg._path.setAttribute("stroke-dasharray", length + "," + length);

			var anim = document.createElementNS(xmlns, "animate");
			anim.setAttribute("attributeName", "stroke-dashoffset");
			anim.setAttribute("begin", "indefinite");
			anim.setAttribute("from", length);
			anim.setAttribute("to", 0);
			anim.setAttribute("dur", 0.25);
			leg._path.appendChild(anim);
			anim.beginElement();

			//Animate opacity
			anim = document.createElementNS(xmlns, "animate");
			anim.setAttribute("attributeName", "stroke-opacity");
			anim.setAttribute("attributeName", "stroke-opacity");
			anim.setAttribute("begin", "indefinite");
			anim.setAttribute("from", 0);
			anim.setAttribute("to", 0.5);
			anim.setAttribute("dur", 0.25);
			leg._path.appendChild(anim);
			anim.beginElement();
		}
		me.setOpacity(0.3);

		//Set the opacity of the spiderLegs back to their correct value
		// The animations above override this until they complete.
		// If the initial opacity of the spiderlegs isn't 0 then they appear before the animation starts.
		if (L.Path.SVG) {
			this._group._forceLayout();

			for (i = childMarkers.length - 1; i >= 0; i--) {
				m = childMarkers[i]._spiderLeg;

				m.options.opacity = 0.5;
				m._path.setAttribute('stroke-opacity', 0.5);
			}
		}

		setTimeout(function () {
			group._animationEnd();
			group.fire('spiderfied');
		}, 200);
	},

	_animationUnspiderfy: function (zoomDetails) {
		var group = this._group,
			map = group._map,
			fg = group._featureGroup,
			thisLayerPos = zoomDetails ? map._latLngToNewLayerPoint(this._latlng, zoomDetails.zoom, zoomDetails.center) : map.latLngToLayerPoint(this._latlng),
			childMarkers = this.getAllChildMarkers(),
			svg = L.Path.SVG && this.SVG_ANIMATION,
			m, i, a;

		group._animationStart();

		//Make us visible and bring the child markers back in
		this.setOpacity(1);
		for (i = childMarkers.length - 1; i >= 0; i--) {
			m = childMarkers[i];

			//Marker was added to us after we were spidified
			if (!m._preSpiderfyLatlng) {
				continue;
			}

			//Fix up the location to the real one
			m.setLatLng(m._preSpiderfyLatlng);
			delete m._preSpiderfyLatlng;
			//Hack override the location to be our center
			if (m.setOpacity) {
				m._setPos(thisLayerPos);
				m.setOpacity(0);
			} else {
				fg.removeLayer(m);
			}

			//Animate the spider legs back in
			if (svg) {
				a = m._spiderLeg._path.childNodes[0];
				a.setAttribute('to', a.getAttribute('from'));
				a.setAttribute('from', 0);
				a.beginElement();

				a = m._spiderLeg._path.childNodes[1];
				a.setAttribute('from', 0.5);
				a.setAttribute('to', 0);
				a.setAttribute('stroke-opacity', 0);
				a.beginElement();

				m._spiderLeg._path.setAttribute('stroke-opacity', 0);
			}
		}

		setTimeout(function () {
			//If we have only <= one child left then that marker will be shown on the map so don't remove it!
			var stillThereChildCount = 0;
			for (i = childMarkers.length - 1; i >= 0; i--) {
				m = childMarkers[i];
				if (m._spiderLeg) {
					stillThereChildCount++;
				}
			}


			for (i = childMarkers.length - 1; i >= 0; i--) {
				m = childMarkers[i];

				if (!m._spiderLeg) { //Has already been unspiderfied
					continue;
				}


				if (m.setOpacity) {
					m.setOpacity(1);
					m.setZIndexOffset(0);
				}

				if (stillThereChildCount > 1) {
					fg.removeLayer(m);
				}

				map.removeLayer(m._spiderLeg);
				delete m._spiderLeg;
			}
			group._animationEnd();
		}, 200);
	}
});


L.MarkerClusterGroup.include({
	//The MarkerCluster currently spiderfied (if any)
	_spiderfied: null,

	_spiderfierOnAdd: function () {
		this._map.on('click', this._unspiderfyWrapper, this);

		if (this._map.options.zoomAnimation) {
			this._map.on('zoomstart', this._unspiderfyZoomStart, this);
		}
		//Browsers without zoomAnimation or a big zoom don't fire zoomstart
		this._map.on('zoomend', this._noanimationUnspiderfy, this);

		if (L.Path.SVG && !L.Browser.touch) {
			this._map._initPathRoot();
			//Needs to happen in the pageload, not after, or animations don't work in webkit
			//  http://stackoverflow.com/questions/8455200/svg-animate-with-dynamically-added-elements
			//Disable on touch browsers as the animation messes up on a touch zoom and isn't very noticable
		}
	},

	_spiderfierOnRemove: function () {
		this._map.off('click', this._unspiderfyWrapper, this);
		this._map.off('zoomstart', this._unspiderfyZoomStart, this);
		this._map.off('zoomanim', this._unspiderfyZoomAnim, this);

		this._unspiderfy(); //Ensure that markers are back where they should be
	},


	//On zoom start we add a zoomanim handler so that we are guaranteed to be last (after markers are animated)
	//This means we can define the animation they do rather than Markers doing an animation to their actual location
	_unspiderfyZoomStart: function () {
		if (!this._map) { //May have been removed from the map by a zoomEnd handler
			return;
		}

		this._map.on('zoomanim', this._unspiderfyZoomAnim, this);
	},
	_unspiderfyZoomAnim: function (zoomDetails) {
		//Wait until the first zoomanim after the user has finished touch-zooming before running the animation
		if (L.DomUtil.hasClass(this._map._mapPane, 'leaflet-touching')) {
			return;
		}

		this._map.off('zoomanim', this._unspiderfyZoomAnim, this);
		this._unspiderfy(zoomDetails);
	},


	_unspiderfyWrapper: function () {
		/// <summary>_unspiderfy but passes no arguments</summary>
		this._unspiderfy();
	},

	_unspiderfy: function (zoomDetails) {
		if (this._spiderfied) {
			this._spiderfied.unspiderfy(zoomDetails);
		}
	},

	_noanimationUnspiderfy: function () {
		if (this._spiderfied) {
			this._spiderfied._noanimationUnspiderfy();
		}
	},

	//If the given layer is currently being spiderfied then we unspiderfy it so it isn't on the map anymore etc
	_unspiderfyLayer: function (layer) {
		if (layer._spiderLeg) {
			this._featureGroup.removeLayer(layer);

			layer.setOpacity(1);
			//Position will be fixed up immediately in _animationUnspiderfy
			layer.setZIndexOffset(0);

			this._map.removeLayer(layer._spiderLeg);
			delete layer._spiderLeg;
		}
	}
});


}(window, document));
//- Process Map Control



function loadMapControls() {
    var bucketMapHash = {};

    //- Loop thru each bucket having class horizontalBarChart
    $(".pgnav-map").each( function(n) {
        var parentContainer = $(this);
        var elBucketId = parentContainer.parents(".bucket-process").attr("id");
        // this is the div element containing the map
        // in this position, the resulting id is 'bucket_map_h5'
        // the title is in a child element h2 class='process-title' 
        var elPlot = elBucketId + '_map' + n;

        var config = getMapConfig(parentContainer, elBucketId, n);
        if (config.elementId) {
            bucketMapHash[elBucketId] =  getDataAndConstructMap(config, elPlot, parentContainer);
        }
    });

    return bucketMapHash;
}


function getMapConfig(container, elBucket, index) {
    var config = {
        elementId: elBucket,
        bucketWidth: $("#"+elBucket).width(),
        // common parameters
        abbreviateValues: 'true',
        recordLimit: '200',
        valueOnTop: 'largest',
        // do not change
        useStoplightColors: 'false',
        subtitle: '',
        controlIndex: index
    };

    //- parameters required by process map control:
    if (container.attr("mapImplementation")) { config.mapImplementation = container.attr("mapImplementation").toUpperCase();} else { config.mapImplementation = "ESRI"; }
    if (container.attr("metricName")) { config.metricName = container.attr("metricName");}
    if (container.attr("granularity")) { config.granularity = container.attr("granularity");}
    if (container.attr("granularityLocation")) { config.granularityLocation = container.attr("granularityLocation");}
    if (container.attr("useStoplightColors")) { config.useStoplightColors = container.attr("useStoplightColors") === "true" ? "true" : "false";}
    if (container.attr("basemapLayer")) { config.basemapLayer = container.attr("basemapLayer").toUpperCase();}
    if (container.attr("markerRadius")) { config.markerRadius = container.attr("markerRadius");} else { config.markerRadius ="7"; }
    if (container.attr("markerOpacity")) { config.markerOpacity = container.attr("markerOpacity");} else { config.markerOpacity ="0.9"; }
    if (container.attr("useClusters")) { config.useClusters = container.attr("useClusters");} else { config.markerOpacity ="false"; }

    //- common parameters:
    if (container.attr("recordLimit")) { config.recordLimit = container.attr("recordLimit");}
    if (container.attr("valueOnTop")) { config.valueOnTop = container.attr("valueOnTop");}
    if (container.attr("subtitle")) { config.subtitle = container.attr("subtitle");}
    if (container.attr("abbreviateValues")) { config.abbreviateValues = container.attr("abbreviateValues");}

    return config;
}



function getDataAndConstructMap(config, elPlot, parentContainer) {
    // get metrics data
    var mapData = [];
    var mapData = getPlotData(config, elPlot);
    if (!mapData || mapData.length === 0) { return; }

    // append the map plot to the parent
    var mapMarkup = mapBucket_Plot(config, elPlot, mapData);
    parentContainer.append(mapMarkup);

    // get location for map data
    mapData = preProcessMapData(config, mapData);
    
    // specify the map div 
    var mapDiv = 'pgnavMap';
    if (config.popupMap){
        mapDiv = 'pgnavMapPopup';
    }

    // create the map config
    var mapConfig = {
        'mapImplementation' : config.mapImplementation,
        'basemapLayer': config.basemapLayer || '',
        'markerRadius': config.markerRadius,
        'markerOpacity': config.markerOpacity,
        'useClusters': config.useClusters
    };

    return initMap(mapDiv, mapData, mapConfig);
}


function preProcessMapData(config, mapData) {
    var features = [];

    // get the plot location data
    try {
        var parameters;
        
        if (config.granularity && config.granularityLocation ){
            parameters = {
                tableName: config.granularityLocation,
                fieldNames: toJSON([config.granularity, 'lat', 'lon'])
            };
        }
        
        var result = Workflow.call('AbCommonResources-getDataRecords', parameters);
        var locData = result.data.records;    
        
        if (!locData || locData.length === 0) { return; }

        for (var i=0; i<mapData.length ; i++ ) {

            // properties
            var key = mapData[i].barLabel;
            var label = mapData[i].pointLabel;
            var value = mapData[i].value;
            var color = mapData[i].barClass;
            var drillDownView = mapData[i].drillDownView;

            var location = [];
            var lat, lon;
            var keyField = config.granularityLocation + '.' + config.granularity;
            
            var latField, lonField;            
            
            location =   $.grep(locData, function(v) {
                return v[keyField] === key && v[latField] !== "" && v[lonField] !== ""; 
            }); 

            if (location.length > 0 ) { 
                
                // use raw values if available
                if (config.granularityLocation + '.lat.raw' in location[0]) {
                    latField = config.granularityLocation+ '.lat.raw';
                    lonField = config.granularityLocation + '.lon.raw';
                } else {
                    latField = config.granularityLocation+ '.lat';
                    lonField = config.granularityLocation + '.lon';
                }

                // geometry
                var lat = location[0][latField];    
                var lon = location[0][lonField];
                
                if (lat && lon) {
                    var coordinates = [lon,lat];
                    // create geojson feature
                    var feature = {
                        "type": "Feature",
                        "geometry" : {
                            "type": "Point",
                            "coordinates": coordinates
                        },
                        "properties" : {
                            "key" : key,
                            "label" : label,
                            "value" : value,
                            "color" : color,
                            "drillDownView" : drillDownView
                        }
                    };
                    // add to features array
                    features.push(feature);   
                }
            }
        }

    } catch (e) {
        mapBucketErrorMessage(config, 'wfrerror');
        //Workflow.handleError(e);
    }

    var featureCollection = {
        "type": "FeatureCollection",
        "features": features
    };

    console.log(JSON.stringify(featureCollection));  
    return featureCollection;
}


function mapBucket_Plot(config, elPlot, data) {
    
    var mapDiv = 'pgnavMap';
    if (config.popupMap) {
        mapDiv = 'pgnavMapPopup';
    }

    var mapHtml =  '<div id="' + mapDiv + '"style="width:520px; height:350px">';
        mapHtml += '<div id="pgnavMapMarkerTooltip" class="pgnav-map-marker-tooltip" style="position:fixed; display:none;"></div>';
        mapHtml += '<div id="pgnavMapError" class="pgnav-map-error" style="position:absolute; display:none;"></div>';
        mapHtml += '</div>';
    return mapHtml;

}


function initMap(mapDiv, mapData, mapOptions) {

    // LOCATION : CENTER, ZOOM || BOUNDS
    // World : [ 19.973348786110602, -15.468749999999998 ], 1 || [[-76.6797, -182.8125], [76.6797, 182.8125]]
    // North America : [37.30028, -98.26172], 3 || [[9.4490, -143.9648], [57.7041, -52.5585]]
    // Downtown Boston : [42.35803652353272, -71.06163024902344], 13 || 
    //var map = L.map(mapDiv).setView([37.30028, -98.26172], 3); 
    var map = L.map(mapDiv).fitBounds([[-76.6797, -182.8125], [76.6797, 182.8125]]);

    // get the map implementation
    if (mapOptions.mapImplementation === 'GOOGLE') {
        var googleBasemap = getBasemapLayer('GOOGLE', mapOptions.basemapLayer);
        var googleLayer = new L.Google(googleBasemap);
        map.addLayer(googleLayer);
    } else {
        var esriBasemap = getBasemapLayer('ESRI', mapOptions.basemapLayer);
        if (esriBasemap == 'ImageryLabels') {
            var basemapLayerGroup = L.layerGroup();
            basemapLayerGroup.addTo(map);
            // TODO are layer & labelsLayer intended to be globals? If not, use var layer...
            layer = L.esri.basemapLayer('Imagery',{
                id: 'basemap',
                attribution: ''
            });
           basemapLayerGroup.addLayer(layer);
            labelsLayer = L.esri.basemapLayer('ImageryLabels', {
                id: 'basemapLabels',
                attribution: ''
            });
            basemapLayerGroup.addLayer(labelsLayer);
        } else {
            L.esri.basemapLayer(esriBasemap).addTo(map);            
        }
    }
   
    // event logging for development purposes only
    map.on('click', function(evt){
        var map = evt.target;
        console.log();
    });
    
    // get the features
    var features = mapData.features;
    if (!features || features.length === 0) { return map; }

    // create the geoJson layer
    var geoJsonLayer = L.geoJson(features, {
        pointToLayer: function (feature, latlng) {
            var markerOptions = {
                    color: 'white',
                    fillColor: barClassToColor(feature.properties.color), //TODO do this earlier
                    fillOpacity: mapOptions.markerOpacity,
                    radius: parseInt(mapOptions.markerRadius),
                    riseOnHover: true,
                    stroke: false,
                    title: feature.properties.key + '<br>' + '<b>' + feature.properties.label + '</b>'
            };

            return L.circleMarker(latlng, markerOptions);
        },
        onEachFeature: function(feature, layer){
            console.log();

            layer.on('mouseover', function(evt){
                console.log('pgnav-map-marker-mouseover');

                // get content from marker
                var title = evt.target.options.title;
                var fillOpacity = (evt.target.options.fillOpacity)*0.5;

                // update the marker tooltip
                $(".pgnav-map-marker-tooltip").html(title);

                // get the mouse location
                var px, py;        
                if (evt.originalEvent.clientX && evt.originalEvent.clientY) { 
                  px = evt.originalEvent.clientX;
                  py = evt.originalEvent.clientY;
                }
                console.log('clientX:' + px + ",clientY: " + py);

                // show the tooltip at the mouse location         
                $(".pgnav-map-marker-tooltip").css({"display": "none"});
                $(".pgnav-map-marker-tooltip").css({"left": (px + 15) + "px", "top": (py) + "px" });
                $(".pgnav-map-marker-tooltip").css({"display": ""});  

                // add marker highlight
                var marker = evt.target;
                marker.setStyle({
                    stroke: true,
                    weight: 3,
                    color: '#000',
                    fillOpacity: fillOpacity
                });    

            });

            layer.on('mouseout', function(evt){
                console.log('pgnav-map-marker-mouseout');
                
                var fillOpacity = (evt.target.options.fillOpacity)*2.0;

                $(".pgnav-map-marker-tooltip").css({"display": "none"});

                // reset the marker style
                var marker = evt.target;
                marker.setStyle({
                    stroke: false,
                    fillOpacity: fillOpacity
                });    
            });

            layer.on('click', function(evt){
                //open drilldown view, pass asset id as parameter
                var feature = evt.target.feature;
                if (feature) {
                    var drillDownView = feature.properties.drillDownView;
                    var assetId = feature.properties.key;
                    //alert('Drilldown View: ' + drillDownView + '\nAsset ID: ' + assetId);
                    // TODO use webAppContextPath value in case installation is not to /archibus/ dir
                    window.open("/archibus/" + drillDownView);
                }
            });
        }
    });

    // check for clusters
    if (mapOptions.useClusters == "true"){
        var markerClusters = L.markerClusterGroup({
          polygonOptions: {
            fillColor: '#000',
            color: '#000',
            weight: 2,
            opacity: 1,
            fillOpacity: 0.25
          }, 
          iconCreateFunction: function(cluster){
            var count = cluster.getChildCount();
            return L.divIcon({
                html: '<div style="width:25px;">' + (count) + '</div>',
                className: 'leaflet-div-cluster-marker',
                iconSize: [25,25]
            })
          }            
        });
        markerClusters.addLayer(geoJsonLayer);
        map.addLayer(markerClusters);
        markerClusters.bringToFront();
        map.fitBounds(markerClusters.getBounds());
    } else {
        map.addLayer(geoJsonLayer);
        geoJsonLayer.bringToFront();
        map.fitBounds(geoJsonLayer.getBounds());
    }

    return map;
}


function mapBucketErrorMessage(config, data) {
    var error_msg = (data === 'wfrerror') ?
        getLocalizedString(pageNavStrings.z_PAGENAV_NO_DATA_AVAILABLE):
        getLocalizedString(pageNavStrings.z_PAGENAV_BUCKET_DEF_ERROR);
    var content = '<div id="pgnavMapError">'+error_msg+'</div>';

    if (config.popupMap) {
        $('#pgnavMapPopup').find('#pgnavMapError').html(content);
        $('#pgnavMapPopup').find('#pgnavMapError').css({"display": ""});
    } else {
        $('#pgnavMap').find('#pgnavMapError').html(content);
        $('#pgnavMap').find('#pgnavMapError').css({"display": ""});
    }
}

function getBasemapLayer(mapImplementation, basemapLayerParam) {

    var basemapLayer;
    
//    ESRI BASEMAPS = ['Imagery', 'ImageryLabels', 'Gray', 'DarkGray', 'Streets','Topographic','NationalGeographic','Oceans','ShadedRelief'],
//    GOOGLE BASEMAPS = ['SATTELITE', 'ROADMAP', 'HYBRID', 'TERRAIN'];

    if (mapImplementation === 'ESRI') {
        switch (basemapLayerParam.toLowerCase()) {
            case 'world imagery':
                basemapLayer = 'Imagery';
                break;
            case 'world imagery with labels':
                basemapLayer =  'ImageryLabels';    
                break;
            case 'world light gray canvas':
                basemapLayer =  'Gray';
                break;
            case 'world dark gray canvas':
                basemapLayer =  'DarkGray';
                break;
            case 'world street map':
                basemapLayer = 'Streets';
                break;
            case 'world shaded relief':
                basemapLayer = 'ShadedRelief';
                break;
            case 'world topographic':
                basemapLayer = 'Topographic';
                break;
            case 'national geographic world map':
                basemapLayer = 'NationalGeographic';
                break;
            case 'oceans basemap':
                basemapLayer = 'Oceans';
                break;
            default:
                basemapLayer = 'ImageryLabels';
                break;
        }

    } else if (mapImplementation === 'GOOGLE') {
        switch (basemapLayerParam.toLowerCase()){
            case 'satellite':
                basemapLayer = 'SATELLITE';
                break;
            case 'satellite with labels':
                basemapLayer = 'HYBRID';
                break;
            case 'road map':
                basemapLayer = 'ROADMAP';
                break;
            case 'terrain':
                basemapLayer = 'TERRAIN';
                break;                
            default:
                basemapLayer = 'HYBRID';
                break;
        }

    } 

    return basemapLayer;
}



function barClassToColor(color) {
    var colorCode = '#5f5f5f';

    if (color == "pos"){
        colorCode = '#5f5f5f';
    } else if (color == "neg") {
        colorCode = '#ff0000';
    } else if (color == "red") {
        colorCode = '#9b3636';
    } else if (color == "green")  {
        colorCode = '#589358';
    } else if (color == "yellow") {
        colorCode = '#dab300';
    } else if (color == "black")  {
        colorCode = '#5f5f5f';
    }

    return colorCode;
}


