// JavaScript code for ab-doc-tree-form.axvw

var testController = View.createController('testController', {

	/**
	 * Set the report cell content to be rewritten after being filled
	 * Add an afterrefresh listener on the form
	 */
    afterViewLoad: function(){
	    this.leaseReport.afterCreateCellContent = function(row, column, cellElement) {
			var value = row[column.id];
			// set cell style depending on cell value
			if (column.id == 'ls.doc' && value != '')	{
				var contentElement = cellElement.childNodes[0];
				contentElement.nodeValue = 'X';
				cellElement.style.fontWeight = 'bold';
			}
		}
		// force the lease report to refresh after the for refreshes due to doc field change
        this.leaseForm.addEventListener('afterRefresh', this.refreshReport);
	},
	
	/**
	 * Use this form of getting the panel for refresh due to early call
	 */
	refreshReport: function() {
        var leaseReport = this.Ab.view.View.panels.get('leaseReport');
        leaseReport.refresh();
	}


});
