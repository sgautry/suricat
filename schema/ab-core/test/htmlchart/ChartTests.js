﻿function initChart(){

	setProperty();

	if(document.getElementById("columnLineChart").checked 
			|| document.getElementById("stackedBarChart").checked || document.getElementById("stackedAreaChart").checked
			|| document.getElementById("stackedColumnChart").checked){
		 document.getElementById("groupTitle").value = "Property";
		 document.getElementById("dataTitle").value = "Value By Property";
	} else if(document.getElementById("pieChart").checked || document.getElementById("pieChart3D").checked) {
		document.getElementById("groupTitle").value = "Division";
		document.getElementById("dataTitle").value = "Room Areas by Division"
	} else {
		document.getElementById("groupTitle").value = "Property";
		document.getElementById("dataTitle").value = "Book Value By Property"
	}
	
	build();
}

function setProperty(){
	if(document.getElementById("pieChart").checked || document.getElementById("pieChart3D").checked){
		document.getElementById("zoomable").disabled = true;
		document.getElementById("labelRotation").disabled = true;
	} else {
		document.getElementById("zoomable").disabled = false;
		document.getElementById("labelRotation").disabled = false;
	}
	
	// in order for the chart to zoom in Mobile device, you will need to have chartCursor and panEvent enabled.
    if(!document.getElementById("zoomable").disabled && document.getElementById("zoomable").checked){
		document.getElementById("showDataTips").disabled = false;
    	if(!document.getElementById("showDataTips").checked){
    		alert("In order for the chart to zoom in Mobile device, you will need to have chartCursor enabled!");
    	}
		document.getElementById("showDataTips").checked = true;
	}
		
}

function build(){
	
	setProperty();
	
	if(document.getElementById("pieChart").checked)
		buildPieChart('pieChart', 'pie');
	else if( document.getElementById("pieChart3D").checked)
		buildPieChart('pieChart3D', 'pie');
	else if(document.getElementById("columnLineChart").checked)
		buildMixedChart("columnLineChart", 'column', 'line');
	else if(document.getElementById("stackedBarChart").checked)
		buildMixedChart("stackedBarChart", 'bar', 'bar');
	else if(document.getElementById("stackedAreaChart").checked)
		buildMixedChart("stackedAreaChart", 'area', 'area');
	else if(document.getElementById("stackedColumnChart").checked)
		buildMixedChart("stackedColumnChart", 'column', 'column');
	else if (document.getElementById("columnChart").checked)
		buildChart("columnChart", 'column');
	else if (document.getElementById("columnChart3D").checked )
		buildChart("columnChart3D", 'column');
	else if (document.getElementById("barChart").checked )
		buildChart("barChart", 'bar');
	else if (document.getElementById("barChart3D").checked )
		buildChart("barChart3D", 'bar');
	else if (document.getElementById("lineChart").checked )
		buildChart('lineChart', 'line');
	else if (document.getElementById("areaChart").checked )
		buildChart('areaChart', 'area');
	else if (document.getElementById("plotChart").checked )
		buildChart('plotChart', 'plot');
	else
		buildChart('columnChart', 'column');
}

function getChartConfig(chartType){
	var config = new ChartConfig();
	config.chartType = chartType;
	config.legendLocation = document.getElementById("legendLocation").value;
	config.showExportButton = document.getElementById("showExportButton").checked;
	config.showOnLoad = document.getElementById("showOnLoad").checked;
	config.showLegendOnLoad = document.getElementById("showLegendOnLoad").checked;
	config.showDataTips = document.getElementById("showDataTips").checked;
	config.showLabels = document.getElementById("showLabels").checked;
	config.showUnitPrefixes = document.getElementById("showUnitPrefixes").checked;
	config.currencyFields["property.value_book"] = document.getElementById("moneySymbol").value;
	config.currencyFields["property.value_market"] = document.getElementById("moneySymbol").value;
	config.showUnitSuffixes = document.getElementById("showUnitSuffixes").checked;
	config.unitFields["rm.area"] = "sqft";
	config.backgroundColor = document.getElementById("backgroundColor").value;
	
	if(!document.getElementById("zoomable").disabled)
		config.zoomable = document.getElementById("zoomable").checked;
	
	return config;
}



function buildPieChart(chartType, type){

	var config = getChartConfig(chartType);
	
	//grouping
	config.addGroupingAxis("rm.dv_id", document.getElementById("groupTitle").value);
	config.groupingAxis[0].labelRotation = document.getElementById("labelRotation").value;
	
	//data- column series
	config.addDataAxis(type, "rm.area", "Room Area");

	config.addValueAxis(0, document.getElementById("dataTitle").value);
	
	var chart = new ChartControl('chartContainer', config);
	chart.setData(PIE_DATA_ARRAY);
		
}

function buildChart(chartType, type){

	var config = getChartConfig(chartType);
	
	//grouping
	config.addGroupingAxis("property.pr_id", document.getElementById("groupTitle").value);
	config.groupingAxis[0].labelRotation = document.getElementById("labelRotation").value;
	
	//data- column series
	config.addDataAxis(type, "property.value_book", "Book Value");
	config.dataAxis[0].lineThickness = document.getElementById("lineThickness").value;

	config.addValueAxis(0, document.getElementById("dataTitle").value);
	
	var chart = new ChartControl('chartContainer', config);
	chart.setData(SERIAL_DATA_ARRAY);
		
}
function buildMixedChart(chartType, type1, type2){
	
	var config = getChartConfig(chartType);
		
	//grouping
	config.addGroupingAxis("property.pr_id",  document.getElementById("groupTitle").value);
	config.groupingAxis[0].labelRotation = document.getElementById("labelRotation").value;
	
	//data- column series
	config.addDataAxis(type1, "property.value_book", "Book Value");
	config.dataAxis[0].lineThickness = document.getElementById("lineThickness").value;

	config.addValueAxis(0, document.getElementById("dataTitle").value);
	
	//data - line series
	config.addDataAxis(type2, "property.value_market", "Market Value");
	config.dataAxis[1].bullet = "round";
	config.dataAxis[1].lineThickness = document.getElementById("lineThickness").value;
	
	var chart = new ChartControl('chartContainer', config);
	chart.setData(SERIAL_DATA_ARRAY);
	
	chart.addEventListener("clickGraphItem", onClickItem);
	
}
	
function onClickItem (event) {
	alert("You clicked on " +  event.item.dataContext["property.pr_id"]);
}
