function init(){
    // the loading message is not working well with modified DWR engine.js - disable for now
    // dwr.util.useLoadingMessage();
}

function DocumentService_checkinNewFile(){
    var keys = {
        ls_id: "102"
    };
    
    var newLockStatus = "0";
    var documentName = "lease1.doc";
    var description = "New file description";
    var fieldName = "doc";
    var tableName = "ls";
    
    var file = dwr.util.getValue('file');
    
    DocumentService.checkinNewFile(file, keys, tableName, fieldName, documentName, description, newLockStatus, {
        callback: function(){
            Ab.view.View.showMessage('message', 'OK');
        },
        errorHandler: function(m, e){
            Ab.view.View.showException(e);
        }
    });
}

function DocumentService_getImage(){
    var keys = {
        ls_id: "102"
    };
    
    var fieldName = "doc";
    var tableName = "ls";
    
    var version = "XXX";
    var lastVersion = 'true';
    DocumentService.getImage(keys, tableName, fieldName, version, lastVersion, {
        callback: function(image){
            dwr.util.setValue('image', image);
        },
        errorHandler: function(m, e){
            Ab.view.View.showException(e);
        }
    });
}

/**
 * Check out the document using DWR built-in IFrame as a proxy.
 * Should display the Open/Save As dialog, with the Save As option selected.
 */
function checkOutUsingHiddenIFrame(){
    DocumentService.show({
        ls_id: "102"
    }, 'ls', 'doc', 'ls-102-doc.doc', '1', true, 'checkOut', {
        callback: function(fileTransfer) {
            dwr.engine.openInDownload(fileTransfer);
        },
        errorHandler: function(m, e){
            Ab.view.View.showException(e);
        }
    });
}

/**
 * Show the document using DWR built-in IFrame as a proxy.
 * Should display the Open/Save As dialog, with the Open option selected.
 */
function showUsingHiddenIFrame(){
    DocumentService.show({
        ls_id: "102"
    }, 'ls', 'doc', 'ls-102-doc.doc', '1', true, 'showDocument', {
        callback: function(fileTransfer) {
            dwr.engine.openInDownload(fileTransfer);
        },
        errorHandler: function(m, e){
            Ab.view.View.showException(e);
        }
    });
}

/**
 * Shows the document content inside custom IFrame, defined in this HTML file.
 * Not implemented.
 */
function NOshowInIFrame(){
    DocumentService.show({
        ls_id: "102"
    }, 'ls', 'doc', 'ls-102-doc.doc', '1', true, 'showDocument', {
        callback: function(fileTransfer) {
            dwr.engine.openInDownload(fileTransfer);
        },
        errorHandler: function(m, e){
            Ab.view.View.showException(e);
        }
    });
}


/**
 * Shows the document content inside custom IFrame, defined in this HTML file.
 * Works!
 * In afm-config.xml contentDisposition for PDF must be "inline".
 */
function showInIFrame(){
    DocumentService.show({
        ls_id: "001"
    }, 'ls', 'doc', 'HUD.pdf', '1', true, 'showDocument', {
        callback: function(fileTransfer) {
            var div = document.createElement("div");
            document.body.appendChild(div);
            div.innerHTML = "<iframe width='1000' height='1000' scrolling='no' frameborder='0' src='" + fileTransfer + "'></iframe>";        },
        errorHandler: function(m, e){
            Ab.view.View.showException(e);
        }
    });
}

function testGetProjects(){
    SecurityService.getProjects({
        callback: function(projects){
            Ab.view.View.showMessage('message', projects);
        },
        errorHandler: function(m, e){
            Ab.view.View.showException(e);
        }
    });
}

function testGetLocales(){
    SecurityService.getLocales("en_US", {
        callback: function(locales){
            Ab.view.View.showMessage('message', locales);
        },
        errorHandler: function(m, e){
            Ab.view.View.showException(e);
        }
    });
}

function testLogin(){
    // encoded
    var userId = "F@y^*-M1357902468A ZXw=+";
    var password = userId;
    var projectId = "HQ-Sybase-WebEdition"
    SecurityService.login(userId, password, projectId, null, {
        callback: function(nextRequest){
            Ab.view.View.showMessage('message', nextRequest);
        },
        errorHandler: function(m, e){
            Ab.view.View.showException(e);
        }
    });
}

function testChangePasswordView(){
    var userId = "AFM";
    var projectId = "HQ-Sybase-WebEdition"
    
    // show change password form
    var dialog = View.openDialog('ab-change-password.axvw', null, false, {
        closeButton: false,
        maximize: true,
        
        afterViewLoad: function(dialogView){
            var changePasswordController = dialogView.controllers.get('ab-change-password');
            changePasswordController.username = username;
            changePasswordController.projectId = projectId;
        }
    });
}

function testChangePassword(){
    var userId = "AFM";
    var oldPassword = userId;
    var newPassword = userId;
    var projectId = "HQ-Sybase-WebEdition"
    
    SecurityService.changePassword(doSecure(userId), doSecure(oldPassword), doSecure(newPassword), projectId, {
        callback: function(){
            Ab.view.View.showMessage('message', 'OK');
        },
        errorHandler: function(m, e){
            Ab.view.View.showException(e);
        }
    });
}

function testChangePasswordAndLogin(){
    // encoded
    var userId = "AFM";
    var oldPassword = userId;
    var newPassword = userId;
    var projectId = "HQ-Sybase-WebEdition"
    
    SecurityService.changePassword(doSecure(userId), doSecure(oldPassword), doSecure(newPassword), projectId, {
        callback: function(){
            SecurityService.login(doSecure(userId), doSecure(newPassword), projectId, null, {
                callback: function(viewName){
                    // show next view
                    Ab.view.View.showMessage('message', viewName);
                },
                errorHandler: function(m, e){
                    // show error message
                    Ab.view.View.showException(e);
                }
            });
        },
        errorHandler: function(m, e){
            Ab.view.View.showException(e);
        }
    });
}

function testLogout(){
    SecurityService.logout({
        callback: function(){
            Ab.view.View.showMessage('message', "OK");
        },
        errorHandler: function(m, e){
            Ab.view.View.showException(e);
        }
    });
}

function testIsUsernameUppercase(){
    var projectId = "HQ-Sybase-WebEdition"
    
    SecurityService.isUsernameUppercase(projectId, {
        callback: function(isUsernameUppercase){
            Ab.view.View.showMessage('message', isUsernameUppercase);
        },
        errorHandler: function(m, e){
            Ab.view.View.showException(e);
        }
    });
}

function testIsPasswordUppercase(){
    var projectId = "HQ-Sybase-WebEdition"
    
    SecurityService.isPasswordUppercase(projectId, {
        callback: function(isPasswordUppercase){
            Ab.view.View.showMessage('message', isPasswordUppercase);
        },
        errorHandler: function(m, e){
            Ab.view.View.showException(e);
        }
    });
}

function AdminService_getUserLicense(){
    AdminService.getUserLicense({
        callback: function(licenseVO){
            Ab.view.View.showMessage('message', licenseVO);
        },
        errorHandler: function(m, e){
            Ab.view.View.showException(e);
        }
    });
}

function AdminService_getProgramLicense(){
    AdminService.getProgramLicense({
        callback: function(licenseVO){
            Ab.view.View.showMessage('message', licenseVO);
        },
        errorHandler: function(m, e){
            Ab.view.View.showException(e);
        }
    });
}

function AdminService_getEvaluationInfo(){
    AdminService.getEvaluationInfo({
        callback: function(evaluation){
            Ab.view.View.showMessage('message', evaluation);
        },
        errorHandler: function(m, e){
            Ab.view.View.showException(e);
        }
    });
}

function AdminService_getUser(){
    AdminService.getUser({
        callback: function(userVO){
            Ab.view.View.showMessage('message', userVO);
        },
        errorHandler: function(m, e){
            Ab.view.View.showException(e);
        }
    });
}

function AdminService_getDrawingControlConfig(){
    AdminService.getDrawingControlConfig({
        callback: function(drawingControlConfig){
            Ab.view.View.showMessage('message', drawingControlConfig);
        },
        errorHandler: function(m, e){
            Ab.view.View.showException(e);
        }
    });
}

function AdminService_loadLocalizedString(){
    var key3 = "Test first sentence";
    var locale = "fr_FR";
    var key1 = "/ab-system/system-administration/test1.axvw";
    var key2;
    var translatablePrefix = true;
    
    AdminService.loadLocalizedString(key1, key2, key3, locale, translatablePrefix, {
        callback: function(string){
            Ab.view.View.showMessage('message', string);
        },
        errorHandler: function(m, e){
            Ab.view.View.showException(e);
        }
    });
}

function AdminService_loadLocalizedStrings(){
    var parameters = new Array({
        key3: "Test first sentence",
        locale: "fr_FR",
        key1: "/ab-system/system-administration/test1.axvw",
        translatablePrefix: true
    }, {
        key3: "Test second sentence",
        locale: "es_SP",
        key1: "/ab-system/system-administration/test1.axvw",
        translatablePrefix: true
    }, {
        key3: "Test third sentence",
        locale: "en_US",
        key1: "/ab-system/system-administration/test1.axvw"
    });
    
    AdminService.loadLocalizedStrings(parameters, {
        callback: function(strings){
            Ab.view.View.showMessage('message', strings);
        },
        errorHandler: function(m, e){
            Ab.view.View.showException(e);
        }
    });
}
