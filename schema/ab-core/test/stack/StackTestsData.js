// ASSUMPTIONS: 
//	1.   <sortField name="bl_id" table="gp" ascending="true" />
//       <sortField name="sort_order" table="fl" ascending="true" />        
//       <sortField name="fl_id" table="gp" ascending="false" />
//       <sortField name="sort_order" table="gp" ascending="true" />
//
//  2. that the highlightField contains the correct color, even for allocation_type
//  3. 'Available' label? 
//  data w/hard-coded colors
var records1 = [ 
     {"gp.allocation_type":"Unavailable - Vertical Penetration Area","gp.dv_id":"","gp.sort_order":"100","gp.fl_id":"Floor 5","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"500","gp.bl_id":"HQ","gp.gp_id":"2547","fl.name":"Executive Offices","gp.gp_id.key":"2547","gp.count_em":"10","dv.hpattern_acad":"#484848","gp.area_manual.raw":"500"},
	 {"gp.allocation_type":"Unavailable - Service Area","gp.dv_id":"","gp.sort_order":"200","gp.fl_id":"Floor 5","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"500","gp.bl_id":"HQ","gp.gp_id":"2548","fl.name":"Executive Offices","gp.gp_id.key":"2548","gp.count_em":"10","dv.hpattern_acad":"#787878","gp.area_manual.raw":"500"},
	 {"gp.dv_id":"","gp.allocation_type":"Unavailable Area","gp.sort_order":"300","gp.fl_id":"Floor 5","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"800","gp.bl_id":"HQ","gp.gp_id":"2549","fl.name":"Executive Offices","gp.gp_id.key":"2549","gp.count_em":"10","dv.hpattern_acad":"#cccccc"},
	 {"gp.allocation_type":"Allocated Area","gp.dv_id":"SOFTWARE APP.","gp.sort_order":"400","gp.fl_id":"Floor 5","fl.sort_order":"0","gp.dp_id":"ACCOUNTING","gp.area_manual":"2,000","gp.bl_id":"HQ","gp.gp_id":"2559","fl.name":"Executive Offices","gp.gp_id.key":"2559","gp.count_em":"10","dv.hpattern_acad":"#0e6dad","gp.area_manual.raw":"2000"},
	 {"gp.allocation_type":"Allocated Area","gp.dv_id":"EXECUTIVE","gp.sort_order":"500","gp.fl_id":"Floor 5","fl.sort_order":"0","gp.dp_id":"R AND D","gp.area_manual":"2,000","gp.bl_id":"HQ","gp.gp_id":"2558","fl.name":"Executive Offices","gp.gp_id.key":"2558","gp.count_em":"10","dv.hpattern_acad":"#00ff00","gp.area_manual.raw":"2000"},
	 {"gp.dv_id":"","gp.allocation_type":"Allocated Area","gp.sort_order":"600","gp.fl_id":"Floor 5","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"200","gp.bl_id":"HQ","gp.gp_id":"2549","fl.name":"Executive Offices","gp.gp_id.key":"2549","gp.count_em":"10","dv.hpattern_acad":"#cccfff"},
	 {"gp.dv_id":"MANAGEMENT CONS.","gp.allocation_type":"Allocated Area","gp.sort_order":"700","gp.fl_id":"Floor 5","fl.sort_order":"0","gp.dp_id":"CONSULTING SVCS","gp.area_manual":"800","gp.bl_id":"HQ","gp.gp_id":"2560","fl.name":"Executive Offices","gp.gp_id.key":"2560","gp.count_em":"10","dv.hpattern_acad":"#ff00ff"},
	 {"gp.allocation_type":"Usable Area - Leased","gp.dv_id":"AVAILABLE","gp.sort_order":"800","gp.fl_id":"Floor 5","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"4,000","gp.bl_id":"HQ","gp.gp_id":"2531","fl.name":"Executive Offices","gp.gp_id.key":"2531","gp.count_em":"10","dv.hpattern_acad":"#ffffff","gp.area_manual.raw":"4000"},
	 {"gp.allocation_type":"Unavailable - Vertical Penetration Area","gp.dv_id":"","gp.sort_order":"100","gp.fl_id":"Floor 4","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"500","gp.bl_id":"HQ","gp.gp_id":"2544","fl.name":"Mixed Floor","gp.gp_id.key":"2544","gp.count_em":"10","dv.hpattern_acad":"#484848"},
	 {"gp.allocation_type":"Unavailable - Service Area","gp.dv_id":"","gp.sort_order":"200","gp.fl_id":"Floor 4","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"500","gp.bl_id":"HQ","gp.gp_id":"2545","fl.name":"Mixed Floor","gp.gp_id.key":"2545","gp.count_em":"10","dv.hpattern_acad":"#787878"},
	 {"gp.dv_id":"","gp.allocation_type":"Unavailable Area","gp.sort_order":"300","gp.fl_id":"Floor 4","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"402.60","gp.bl_id":"HQ","gp.gp_id":"2546","fl.name":"Mixed Floor","gp.gp_id.key":"2546","gp.count_em":"10","dv.hpattern_acad":"#cccccc"},
	 {"gp.allocation_type":"Allocated Area","gp.dv_id":"ELECTRONIC SYS.","gp.sort_order":"400","gp.fl_id":"Floor 4","fl.sort_order":"0","gp.dp_id":"ADMINISTRATION","gp.area_manual":"1,400","gp.bl_id":"HQ","gp.gp_id":"2557","fl.name":"Mixed Floor","gp.gp_id.key":"2557","gp.count_em":"10","dv.hpattern_acad":"#00ffff","gp.area_manual.raw":"1400"},
	 {"gp.allocation_type":"Allocated Area","gp.dv_id":"SOFTWARE SOLN.","gp.sort_order":"500","gp.fl_id":"Floor 4","fl.sort_order":"0","gp.dp_id":"ENGINEERING","gp.area_manual":"3,500","gp.bl_id":"HQ","gp.gp_id":"2556","fl.name":"Mixed Floor","gp.gp_id.key":"2556","gp.count_em":"10","dv.hpattern_acad":"#00007f","gp.area_manual.raw":"3500"},
	 {"gp.allocation_type":"Usable Area - Owned","gp.dv_id":"AVAILABLE","gp.sort_order":"600","gp.fl_id":"Floor 4","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"4,500","gp.bl_id":"HQ","gp.gp_id":"2530","fl.name":"Mixed Floor","gp.gp_id.key":"2530","gp.count_em":"10","dv.hpattern_acad":"#ffffff","gp.area_manual.raw":"4500"},
	 {"gp.allocation_type":"Unavailable - Vertical Penetration Area","gp.dv_id":"","gp.sort_order":"100","gp.fl_id":"Floor 3","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"500","gp.bl_id":"HQ","gp.gp_id":"2541","fl.name":"Open-Plan Offices","gp.gp_id.key":"2541","gp.count_em":"10","dv.hpattern_acad":"#484848"},
	 {"gp.allocation_type":"Unavailable - Service Area","gp.dv_id":"","gp.sort_order":"200","gp.fl_id":"Floor 3","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"500","gp.bl_id":"HQ","gp.gp_id":"2542","fl.name":"Open-Plan Offices","gp.gp_id.key":"2542","gp.count_em":"10","dv.hpattern_acad":"#787878"},
	 {"gp.dv_id":"","gp.allocation_type":"Unavailable Area","gp.sort_order":"300","gp.fl_id":"Floor 3","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"76.47","gp.bl_id":"HQ","gp.gp_id":"2543","fl.name":"Open-Plan Offices","gp.gp_id.key":"2543","gp.count_em":"10","dv.hpattern_acad":"#cccccc"},
	 {"gp.allocation_type":"Allocated Area","gp.dv_id":"ELECTRONIC SYS.","gp.sort_order":"400","gp.fl_id":"Floor 3","fl.sort_order":"0","gp.dp_id":"ADMINISTRATION","gp.area_manual":"1,400","gp.bl_id":"HQ","gp.gp_id":"2555","fl.name":"Open-Plan Offices","gp.gp_id.key":"2555","gp.count_em":"10","dv.hpattern_acad":"#00ffff","gp.area_manual.raw":"1400"},
	 {"gp.allocation_type":"Allocated Area","gp.dv_id":"HUMAN RESOURCES","gp.sort_order":"500","gp.fl_id":"Floor 3","fl.sort_order":"0","gp.dp_id":"NEW ENGLAND","gp.area_manual":"2,200","gp.bl_id":"HQ","gp.gp_id":"2554","fl.name":"Open-Plan Offices","gp.gp_id.key":"2554","gp.count_em":"10","dv.hpattern_acad":"#ff0000","gp.area_manual.raw":"2200"},
	 {"gp.allocation_type":"Allocated Area","gp.dv_id":"FINANCE","gp.sort_order":"600","gp.fl_id":"Floor 3","fl.sort_order":"0","gp.dp_id":"DOMESTIC","gp.area_manual":"1,600","gp.bl_id":"HQ","gp.gp_id":"2553","fl.name":"Open-Plan Offices","gp.gp_id.key":"2553","gp.count_em":"10","dv.hpattern_acad":"#0e6dad","gp.area_manual.raw":"1600"},
	 {"gp.allocation_type":"Usable Area - Owned","gp.dv_id":"AVAILABLE","gp.sort_order":"700","gp.fl_id":"Floor 3","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"4,500","gp.bl_id":"HQ","gp.gp_id":"2529","fl.name":"Open-Plan Offices","gp.gp_id.key":"2529","gp.count_em":"10","dv.hpattern_acad":"#ffffff","gp.area_manual.raw":"4500"},
	 {"gp.allocation_type":"Unavailable - Vertical Penetration Area","gp.dv_id":"","gp.sort_order":"100","gp.fl_id":"Floor 2","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"500","gp.bl_id":"HQ","gp.gp_id":"2538","fl.name":"Open-Plan Offices","gp.gp_id.key":"2538","gp.count_em":"10","dv.hpattern_acad":"#484848"},
	 {"gp.allocation_type":"Unavailable - Service Area","gp.dv_id":"","gp.sort_order":"200","gp.fl_id":"Floor 2","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"500","gp.bl_id":"HQ","gp.gp_id":"2539","fl.name":"Open-Plan Offices","gp.gp_id.key":"2539","gp.count_em":"10","dv.hpattern_acad":"#787878"},
	 {"gp.dv_id":"","gp.allocation_type":"Unavailable Area","gp.sort_order":"300","gp.fl_id":"Floor 2","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"280","gp.bl_id":"HQ","gp.gp_id":"2540","fl.name":"Open-Plan Offices","gp.gp_id.key":"2540","gp.count_em":"10","dv.hpattern_acad":"#cccccc"},
	 {"gp.allocation_type":"Allocated Area","gp.dv_id":"FINANCE","gp.sort_order":"400","gp.fl_id":"Floor 2","fl.sort_order":"0","gp.dp_id":"INTERNATIONAL","gp.area_manual":"2,700","gp.bl_id":"HQ","gp.gp_id":"2552","fl.name":"Open-Plan Offices","gp.gp_id.key":"2552","gp.count_em":"10","dv.hpattern_acad":"#5698c5","gp.area_manual.raw":"2700"},
	 {"gp.allocation_type":"Allocated Area","gp.dv_id":"FACILITIES","gp.sort_order":"500","gp.fl_id":"Floor 2","fl.sort_order":"0","gp.dp_id":"REAL ESTATE MGMT","gp.area_manual":"2,300","gp.bl_id":"HQ","gp.gp_id":"2551","fl.name":"Open-Plan Offices","gp.gp_id.key":"2551","gp.count_em":"10","dv.hpattern_acad":"#ffff00","gp.area_manual.raw":"2300"},
	 {"gp.allocation_type":"Usable Area - Owned","gp.dv_id":"AVAILABLE","gp.sort_order":"600","gp.fl_id":"Floor 2","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"4,500","gp.bl_id":"HQ","gp.gp_id":"2528","fl.name":"Open-Plan Offices","gp.gp_id.key":"2528","gp.count_em":"10","dv.hpattern_acad":"#ffffff","gp.area_manual.raw":"4500"},
	 {"gp.allocation_type":"Unavailable - Vertical Penetration Area","gp.dv_id":"","gp.sort_order":"100","gp.fl_id":"Floor 1","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"500","gp.bl_id":"HQ","gp.gp_id":"2535","fl.name":"Mixed Floor","gp.gp_id.key":"2535","gp.count_em":"10","dv.hpattern_acad":"#484848"},
	 {"gp.allocation_type":"Unavailable - Service Area","gp.dv_id":"","gp.sort_order":"200","gp.fl_id":"Floor 1","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"500","gp.bl_id":"HQ","gp.gp_id":"2536","fl.name":"Mixed Floor","gp.gp_id.key":"2536","gp.count_em":"10","dv.hpattern_acad":"#787878"},
	 {"gp.dv_id":"","gp.allocation_type":"Unavailable Area","gp.sort_order":"300","gp.fl_id":"Floor 1","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"100","gp.bl_id":"HQ","gp.gp_id":"2537","fl.name":"Mixed Floor","gp.gp_id.key":"2537","gp.count_em":"10","dv.hpattern_acad":"#cccccc"},
	 {"gp.allocation_type":"Allocated Area","gp.dv_id":"EXECUTIVE","gp.sort_order":"400","gp.fl_id":"Floor 1","fl.sort_order":"0","gp.dp_id":"MANAGEMENT","gp.area_manual":"5,200","gp.bl_id":"HQ","gp.gp_id":"2550","fl.name":"Mixed Floor","gp.gp_id.key":"2550","gp.count_em":"10","dv.hpattern_acad":"#00ff00","gp.area_manual.raw":"5200"},
	 {"gp.allocation_type":"Usable Area - Owned","gp.dv_id":"AVAILABLE","gp.sort_order":"500","gp.fl_id":"Floor 1","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"4,500","gp.bl_id":"HQ","gp.gp_id":"2527","fl.name":"Mixed Floor","gp.gp_id.key":"2527","gp.count_em":"10","dv.hpattern_acad":"#ffffff","gp.area_manual.raw":"4500"}];


var records2 =
	/*
	[
	 {"gp.allocation_type":"Usable Area - Owned","gp.dv_id":"AVAILABLE","gp.sort_order":"100","gp.fl_id":"Floor 5","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"0","gp.bl_id":"HQ","gp.gp_id":"2547","fl.name":"Executive Offices","gp.gp_id.key":"2547","gp.count_em":"10","dv.hpattern_acad":"#fff","gp.area_manual.raw":"25000"},
	 {"gp.allocation_type":"Usable Area - Owned","gp.dv_id":"AVAILABLE","gp.sort_order":"100","gp.fl_id":"Floor 4","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"0","gp.bl_id":"HQ","gp.gp_id":"2547","fl.name":"Executive Offices","gp.gp_id.key":"2547","gp.count_em":"10","dv.hpattern_acad":"#fff","gp.area_manual.raw":"25000"},
	 {"gp.allocation_type":"Usable Area - Owned","gp.dv_id":"AVAILABLE","gp.sort_order":"100","gp.fl_id":"Floor 3","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"0","gp.bl_id":"HQ","gp.gp_id":"2547","fl.name":"Executive Offices","gp.gp_id.key":"2547","gp.count_em":"10","dv.hpattern_acad":"#fff","gp.area_manual.raw":"25000"},
	 {"gp.allocation_type":"Usable Area - Owned","gp.dv_id":"AVAILABLE","gp.sort_order":"100","gp.fl_id":"Floor 2","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"0","gp.bl_id":"HQ","gp.gp_id":"2547","fl.name":"Executive Offices","gp.gp_id.key":"2547","gp.count_em":"10","dv.hpattern_acad":"#fff","gp.area_manual.raw":"25000"},
	 {"gp.allocation_type":"Usable Area - Owned","gp.dv_id":"AVAILABLE","gp.sort_order":"100","gp.fl_id":"Floor 1","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"0","gp.bl_id":"HQ","gp.gp_id":"2547","fl.name":"Executive Offices","gp.gp_id.key":"2547","gp.count_em":"10","dv.hpattern_acad":"#fff","gp.area_manual.raw":"25000"},	
	]
	*/	 
	//[{"gp.allocation_type":"Unavailable - Vertical Penetration Area","gp.dv_id":"","gp.sort_order":"100","gp.fl_id":"19","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"1,034.77","gp.bl_id":"HQ","gp.gp_id":"2547","fl.name":"Executive Offices","gp.gp_id.key":"2547","gp.count_em":"10","dv.hpattern_acad":"#484848","gp.area_manual.raw":"1034.77"}];
	
	[{"gp.allocation_type":"Unavailable - Vertical Penetration Area","gp.dv_id":"","gp.sort_order":"100","gp.fl_id":"19","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"1,034.77","gp.bl_id":"HQ","gp.gp_id":"2547","fl.name":"Executive Offices","gp.gp_id.key":"2547","gp.count_em":"10","dv.hpattern_acad":"#484848","gp.area_manual.raw":"1034.77"},
	 {"gp.allocation_type":"Unavailable - Service Area","gp.dv_id":"","gp.sort_order":"200","gp.fl_id":"19","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"1,185.80","gp.bl_id":"HQ","gp.gp_id":"2548","fl.name":"Executive Offices","gp.gp_id.key":"2548","gp.count_em":"10","dv.hpattern_acad":"#787878","gp.area_manual.raw":"1185.80"},
	 {"gp.dv_id":"","gp.allocation_type":"Unavailable Area","gp.sort_order":"300","gp.fl_id":"19","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"668.89","gp.bl_id":"HQ","gp.gp_id":"2549","fl.name":"Executive Offices","gp.gp_id.key":"2549","gp.count_em":"10","dv.hpattern_acad":"#cccccc"},
	 {"gp.allocation_type":"Allocated Area","gp.dv_id":"SOFTWARE APP.","gp.sort_order":"400","gp.fl_id":"19","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"1,056.00","gp.bl_id":"HQ","gp.gp_id":"2559","fl.name":"Executive Offices","gp.gp_id.key":"2559","gp.count_em":"10","dv.hpattern_acad":"#0e6dad","gp.area_manual.raw":"1056.00"},
	 {"gp.allocation_type":"Allocated Area","gp.dv_id":"EXECUTIVE","gp.sort_order":"500","gp.fl_id":"19","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"2,398.00","gp.bl_id":"HQ","gp.gp_id":"2558","fl.name":"Executive Offices","gp.gp_id.key":"2558","gp.count_em":"10","dv.hpattern_acad":"#00ff00","gp.area_manual.raw":"2398.00"},
	 {"gp.dv_id":"MANAGEMENT CONS.","gp.allocation_type":"Allocated Area","gp.sort_order":"600","gp.fl_id":"19","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"890.00","gp.bl_id":"HQ","gp.gp_id":"2560","fl.name":"Executive Offices","gp.gp_id.key":"2560","gp.count_em":"10","dv.hpattern_acad":"#ff00ff"},
	 {"gp.allocation_type":"Usable Area - Leased","gp.dv_id":"","gp.sort_order":"700","gp.fl_id":"19","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"7,576.42","gp.bl_id":"HQ","gp.gp_id":"2531","fl.name":"Executive Offices","gp.gp_id.key":"2531","gp.count_em":"10","dv.hpattern_acad":"#ffffff","gp.area_manual.raw":"7576.42"},
	 {"gp.allocation_type":"Unavailable - Vertical Penetration Area","gp.dv_id":"","gp.sort_order":"100","gp.fl_id":"18","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"1,034.77","gp.bl_id":"HQ","gp.gp_id":"2544","fl.name":"Mixed Floor","gp.gp_id.key":"2544","gp.count_em":"10","dv.hpattern_acad":"#484848","gp.area_manual.raw":"1034.77"},
	 {"gp.allocation_type":"Unavailable - Service Area","gp.dv_id":"","gp.sort_order":"200","gp.fl_id":"18","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"1,054.89","gp.bl_id":"HQ","gp.gp_id":"2545","fl.name":"Mixed Floor","gp.gp_id.key":"2545","gp.count_em":"10","dv.hpattern_acad":"#787878","gp.area_manual.raw":"1054.89"},
	 {"gp.dv_id":"","gp.allocation_type":"Unavailable Area","gp.sort_order":"300","gp.fl_id":"18","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"402.60","gp.bl_id":"HQ","gp.gp_id":"2546","fl.name":"Mixed Floor","gp.gp_id.key":"2546","gp.count_em":"10","dv.hpattern_acad":"#cccccc"},
	 {"gp.allocation_type":"Allocated Area","gp.dv_id":"ELECTRONIC SYS.","gp.sort_order":"400","gp.fl_id":"18","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"1,298.00","gp.bl_id":"HQ","gp.gp_id":"2557","fl.name":"Mixed Floor","gp.gp_id.key":"2557","gp.count_em":"10","dv.hpattern_acad":"#00ffff","gp.area_manual.raw":"1298.00"},
	 {"gp.allocation_type":"Allocated Area","gp.dv_id":"SOFTWARE SOLN.","gp.sort_order":"500","gp.fl_id":"18","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"3,590.00","gp.bl_id":"HQ","gp.gp_id":"2556","fl.name":"Mixed Floor","gp.gp_id.key":"2556","gp.count_em":"10","dv.hpattern_acad":"#00007f","gp.area_manual.raw":"3590.00"},
	 {"gp.allocation_type":"Usable Area - Owned","gp.dv_id":"","gp.sort_order":"600","gp.fl_id":"18","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"7,576.42","gp.bl_id":"HQ","gp.gp_id":"2530","fl.name":"Mixed Floor","gp.gp_id.key":"2530","gp.count_em":"10","dv.hpattern_acad":"#ffffff","gp.area_manual.raw":"7576.42"},
	 {"gp.allocation_type":"Unavailable - Vertical Penetration Area","gp.dv_id":"","gp.sort_order":"100","gp.fl_id":"17","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"1,034.77","gp.bl_id":"HQ","gp.gp_id":"2541","fl.name":"Open-Plan Offices","gp.gp_id.key":"2541","gp.count_em":"10","dv.hpattern_acad":"#484848","gp.area_manual.raw":"1034.77"},
	 {"gp.allocation_type":"Unavailable - Service Area","gp.dv_id":"","gp.sort_order":"200","gp.fl_id":"17","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"1,054.89","gp.bl_id":"HQ","gp.gp_id":"2542","fl.name":"Open-Plan Offices","gp.gp_id.key":"2542","gp.count_em":"10","dv.hpattern_acad":"#787878","gp.area_manual.raw":"1054.89"},
	 {"gp.dv_id":"","gp.allocation_type":"Unavailable Area","gp.sort_order":"300","gp.fl_id":"17","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"76.47","gp.bl_id":"HQ","gp.gp_id":"2543","fl.name":"Open-Plan Offices","gp.gp_id.key":"2543","gp.count_em":"10","dv.hpattern_acad":"#cccccc"},
	 {"gp.allocation_type":"Allocated Area","gp.dv_id":"ELECTRONIC SYS.","gp.sort_order":"400","gp.fl_id":"17","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"1,395.00","gp.bl_id":"HQ","gp.gp_id":"2555","fl.name":"Open-Plan Offices","gp.gp_id.key":"2555","gp.count_em":"10","dv.hpattern_acad":"#00ffff","gp.area_manual.raw":"1395.00"},
	 {"gp.allocation_type":"Allocated Area","gp.dv_id":"HUMAN RESOURCES","gp.sort_order":"500","gp.fl_id":"17","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"2,390.00","gp.bl_id":"HQ","gp.gp_id":"2554","fl.name":"Open-Plan Offices","gp.gp_id.key":"2554","gp.count_em":"10","dv.hpattern_acad":"#ff0000","gp.area_manual.raw":"2390.00"},
	 {"gp.allocation_type":"Allocated Area","gp.dv_id":"FINANCE","gp.sort_order":"600","gp.fl_id":"17","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"1,598.00","gp.bl_id":"HQ","gp.gp_id":"2553","fl.name":"Open-Plan Offices","gp.gp_id.key":"2553","gp.count_em":"10","dv.hpattern_acad":"#0e6dad","gp.area_manual.raw":"1598.00"},
	 {"gp.allocation_type":"Usable Area - Owned","gp.dv_id":"","gp.sort_order":"700","gp.fl_id":"17","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"7,576.42","gp.bl_id":"HQ","gp.gp_id":"2529","fl.name":"Open-Plan Offices","gp.gp_id.key":"2529","gp.count_em":"10","dv.hpattern_acad":"#ffffff","gp.area_manual.raw":"7576.42"},
	 {"gp.allocation_type":"Unavailable - Vertical Penetration Area","gp.dv_id":"","gp.sort_order":"100","gp.fl_id":"15","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"1,022.94","gp.bl_id":"HQ","gp.gp_id":"2538","fl.name":"Open-Plan Offices","gp.gp_id.key":"2538","gp.count_em":"10","dv.hpattern_acad":"#484848","gp.area_manual.raw":"1022.94"},
	 {"gp.allocation_type":"Unavailable - Service Area","gp.dv_id":"","gp.sort_order":"200","gp.fl_id":"15","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"1,114.84","gp.bl_id":"HQ","gp.gp_id":"2539","fl.name":"Open-Plan Offices","gp.gp_id.key":"2539","gp.count_em":"10","dv.hpattern_acad":"#787878","gp.area_manual.raw":"1114.84"},
	 {"gp.dv_id":"","gp.allocation_type":"Unavailable Area","gp.sort_order":"300","gp.fl_id":"15","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"277.73","gp.bl_id":"HQ","gp.gp_id":"2540","fl.name":"Open-Plan Offices","gp.gp_id.key":"2540","gp.count_em":"10","dv.hpattern_acad":"#cccccc"},
	 {"gp.allocation_type":"Allocated Area","gp.dv_id":"FINANCE","gp.sort_order":"400","gp.fl_id":"15","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"2,708.00","gp.bl_id":"HQ","gp.gp_id":"2552","fl.name":"Open-Plan Offices","gp.gp_id.key":"2552","gp.count_em":"10","dv.hpattern_acad":"#5698c5","gp.area_manual.raw":"2708.00"},
	 {"gp.allocation_type":"Allocated Area","gp.dv_id":"FACILITIES","gp.sort_order":"500","gp.fl_id":"15","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"2,345.00","gp.bl_id":"HQ","gp.gp_id":"2551","fl.name":"Open-Plan Offices","gp.gp_id.key":"2551","gp.count_em":"10","dv.hpattern_acad":"#ffff00","gp.area_manual.raw":"2345.00"},
	 {"gp.allocation_type":"Usable Area - Owned","gp.dv_id":"","gp.sort_order":"600","gp.fl_id":"15","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"7,576.42","gp.bl_id":"HQ","gp.gp_id":"2528","fl.name":"Open-Plan Offices","gp.gp_id.key":"2528","gp.count_em":"10","dv.hpattern_acad":"#ffffff","gp.area_manual.raw":"7576.42"},
	 {"gp.allocation_type":"Unavailable - Vertical Penetration Area","gp.dv_id":"","gp.sort_order":"100","gp.fl_id":"14","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"1,034.77","gp.bl_id":"HQ","gp.gp_id":"2535","fl.name":"Mixed Floor","gp.gp_id.key":"2535","gp.count_em":"10","dv.hpattern_acad":"#484848","gp.area_manual.raw":"1034.77"},
	 {"gp.allocation_type":"Unavailable - Service Area","gp.dv_id":"","gp.sort_order":"200","gp.fl_id":"14","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"1,054.89","gp.bl_id":"HQ","gp.gp_id":"2536","fl.name":"Mixed Floor","gp.gp_id.key":"2536","gp.count_em":"10","dv.hpattern_acad":"#787878","gp.area_manual.raw":"1054.89"},
	 {"gp.dv_id":"","gp.allocation_type":"Unavailable Area","gp.sort_order":"300","gp.fl_id":"14","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"76.47","gp.bl_id":"HQ","gp.gp_id":"2537","fl.name":"Mixed Floor","gp.gp_id.key":"2537","gp.count_em":"10","dv.hpattern_acad":"#cccccc"},
	 {"gp.allocation_type":"Allocated Area","gp.dv_id":"EXECUTIVE","gp.sort_order":"400","gp.fl_id":"14","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"5,210.00","gp.bl_id":"HQ","gp.gp_id":"2550","fl.name":"Mixed Floor","gp.gp_id.key":"2550","gp.count_em":"10","dv.hpattern_acad":"#00ff00","gp.area_manual.raw":"5210.00"},
	 {"gp.allocation_type":"Usable Area - Owned","gp.dv_id":"","gp.sort_order":"500","gp.fl_id":"14","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"7,576.42","gp.bl_id":"HQ","gp.gp_id":"2527","fl.name":"Mixed Floor","gp.gp_id.key":"2527","gp.count_em":"10","dv.hpattern_acad":"#ffffff","gp.area_manual.raw":"7576.42"}];

var records3 = 
	[
	 {"gp.allocation_type":"None","gp.dv_id":"","gp.sort_order":"100","gp.fl_id":"Floor 5","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"5000","gp.bl_id":"UNALLOC","gp.gp_id":"2547","fl.name":"Executive Offices","gp.gp_id.key":"2547","gp.count_em":"10","dv.hpattern_acad":"#fff","gp.area_manual.raw":"5000"},
	 {"gp.allocation_type":"None","gp.dv_id":"","gp.sort_order":"100","gp.fl_id":"Floor 4","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"5000","gp.bl_id":"UNALLOC","gp.gp_id":"2547","fl.name":"Executive Offices","gp.gp_id.key":"2547","gp.count_em":"10","dv.hpattern_acad":"#fff","gp.area_manual.raw":"5000"},
	 {"gp.allocation_type":"None","gp.dv_id":"","gp.sort_order":"100","gp.fl_id":"Floor 3","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"5000","gp.bl_id":"UNALLOC","gp.gp_id":"2547","fl.name":"Executive Offices","gp.gp_id.key":"2547","gp.count_em":"10","dv.hpattern_acad":"#fff","gp.area_manual.raw":"5000"},
	 {"gp.allocation_type":"None","gp.dv_id":"","gp.sort_order":"100","gp.fl_id":"Floor 2","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"5000","gp.bl_id":"UNALLOC","gp.gp_id":"2547","fl.name":"Executive Offices","gp.gp_id.key":"2547","gp.count_em":"10","dv.hpattern_acad":"#fff","gp.area_manual.raw":"5000"},
	 {"gp.allocation_type":"None","gp.dv_id":"","gp.sort_order":"100","gp.fl_id":"Floor 1","fl.sort_order":"0","gp.dp_id":"","gp.area_manual":"5000","gp.bl_id":"UNALLOC","gp.gp_id":"2547","fl.name":"Executive Offices","gp.gp_id.key":"2547","gp.count_em":"10","dv.hpattern_acad":"#fff","gp.area_manual.raw":"5000"},	
	]
	 
var profile1 = {"gp.bl_id": "One Commercial Center", "bl.name": "One Commercial Center", "bl.address1": "One Commerical Center Way", "bl.address2": "Phoenix, AZ",  "bl.city_id": "Philadelphia", "bl.state_id": "PA", "bl.use1": "Zoned Commerical", "bl.image_file": "durham.jpg", "gp.avail.raw": "149779.18", "gp.seats": "310", "gp.util": "50"}
var profile2 = {"gp.bl_id": "HQ", "bl.name": "Headquarters", "bl.address1": "1802 Market Street", "bl.address2": "Philadelphia, PA 19103",  "bl.city_id": "Philadelphia", "bl.state_id": "PA", "bl.use1": "OFFICE", "bl.image_file": "hq.jpg", "gp.avail.raw": "149779.18", "gp.seats": "310", "gp.util": "50"}
profile3 = {"gp.bl_id": "UNALLOC", "bl.name": "Unallocated Space", "bl.image_file": "unallocated.jpg", "bl.address1": "This represents organizational space that is not allocated to any building or floor."};

var statistics1 = 
	[{"gp.bl_id":"HQ","gp.fl_id":"19","gp.util":"100","gp.avail.raw":"14809.88","gp.seats":"70"},
	 {"gp.bl_id":"HQ","gp.fl_id":"18","gp.util":"100","gp.avail.raw":"14956.68","gp.seats":"60"},
	 {"gp.bl_id":"HQ","gp.fl_id":"17","gp.util":"100","gp.avail.raw":"15125.55","gp.seats":"70"},
	 {"gp.bl_id":"HQ","gp.fl_id":"15","gp.util":"100","gp.avail.raw":"15044.93","gp.seats":"60"},
	 {"gp.bl_id":"HQ","gp.fl_id":"14","gp.util":"100","gp.avail.raw":"14952.55","gp.seats":"50"}];


var statistics2 = statistics1;
var statistics3 = [];


/*
var data1 = [{
	"key": "Floor 7",
    "values": [
               { "x": 0, "y": 3000, "color": "#585858"},
               { "x": 1, "y": 4000, "color": "#C8C8C8"},
               { "x": 2, "y": 4000, "color": "green"},
               { "x": 3, "y": 30000, "color": "#ffffff"}
    ]	
}, {
	"key": "Floor 6",
	"values": [
	           { "x": 0, "y": 3000, "color": "#585858"},
	           { "x": 1, "y": 4000, "color": "#C8C8C8"},
               { "x": 2, "y": 4000, "color": "green"},
	           { "x": 3, "y": 15000, "color": "#ffffff"}
	]
}, {
    "key": "Floor 5",
    "values": [
               { "x": 0, "y": 3000, "color": "#585858"},
               { "x": 1, "y": 4000, "color": "#C8C8C8"},
               { "x": 2, "y": 0, "color": "green"},
               { "x": 3, "y": 30000, "color": "#ffffff"}
    ]
}, {
	"key": "Floor 4",
	"values": [
	           { "x": 0, "y": 3000, "color": "#585858"},
	           { "x": 1, "y": 4000, "color": "#C8C8C8"},
               { "x": 2, "y": 4000, "color": "green"},
	           { "x": 3, "y": 30000, "color": "#ffffff"}
	]
}, 
{
	"key": "Floor 3",
	"values": [
	           { "x": 0, "y": 4000, "color": "#585858"},
	           { "x": 1, "y": 3000, "color": "#C8C8C8"},
               { "x": 2, "y": 4000, "color": "green"},
	           { "x": 3, "y": 25000, "color": "#ffffff"}
	]
}, {
	"key": "Floor 2",
	"values": [
	           { "x": 0, "y": 4000, "color": "#585858"},
	           { "x": 1, "y": 7000, "color": "#C8C8C8"},
               { "x": 1, "y": 4000, "color": "green"},
	           { "x": 2, "y": 26000, "color": "#ffffff"}
	]
}, {
	"key": "Floor 1",
	"values": [
	           { "x": 0, "y": 4000, "color": "#585858"},
	           { "x": 1, "y": 12000,"color": "#C8C8C8"},
               { "x": 1, "y": 5000, "color": "green"},
	           { "x": 2, "y": 21000,  "color": "#ffffff"}
	]
}];
*/
