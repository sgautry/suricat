function initStack() {
	build();
}

function getOrientation() {
	var orientation = 'VERTICAL';
	if (document.getElementById("HORIZONTAL").checked) {
		orientation = 'HORIZONTAL';
	} else {
		orientation = 'VERTICAL';
	}
	
	return orientation;
}

function getDisplayFloorHeight() { 
	return Number(document.getElementById("displayFloorHeight").value);
}

function getLabelLargeTextHeight() { 
	return Number(document.getElementById("labelLargeTextHeight").value);
}

function getLabelSmallTextHeight() { 
	return Number(document.getElementById("labelSmallTextHeight").value);
}

function getShowXAxis() { 
	return document.getElementById("showXAxis").checked;
}

function showHighlightFloor() { 
	return document.getElementById("showHighlightFloor").checked;
}

function getNumberOfBuildings() {
	var layouts = document.getElementsByName('layout');
	for (var i=0; i<layouts.length; i++) {
		if(layouts[i].checked) {
			numberOfBuildings = layouts[i].id;
		}
	}

	return numberOfBuildings;
}

function zoomInV(){
	document.getElementById("displayFloorHeight").value = Math.max(Math.round(document.getElementById("displayFloorHeight").value * 1.20), 12);
	build();
}

function zoomOutV(){
	document.getElementById("displayFloorHeight").value = Math.round(document.getElementById("displayFloorHeight").value * .8);
	build();	
}

function zoomInH(){
	document.getElementById("horizontalScale").value = Math.round( (Number(document.getElementById("horizontalScale").value) + 0.10) * 100) / 100 ;
	build();
}

function zoomOutH(){
	document.getElementById("horizontalScale").value = Math.max(Math.round( (Number(document.getElementById("horizontalScale").value) - 0.10) * 100) / 100, 1) ;
	build();	
}


function getHorizontalScale() { 
	return document.getElementById("horizontalScale").value;
}


function build() {		
	var config = {
			stackOrientation: getOrientation(),				// HORIZONTAL or VERTICAL. Whether to arrange the buildings stacked horizontally next to each other or vertically one on top of another.

			displayFloorHeight: getDisplayFloorHeight(),	// 10. The floor height, in user-display units, used to draw the stack plan. 
															
			buildings: [],									// The list of buildings to display in the stack.  e.g. { "HQ", "BOSMED" }. The stack diagram starts empty for every session. 

			groupDatasourceView: 'ab-ex-htmlstack.axvw', // The view (.axvw) file holding the datasource that drive this stack diagram.

			groupDatasource: 'abExHtmlStackDs_gp',			// The datasource used for drawing the floor outline, the roof outline, and each stack group (e.g. a query on the gp table).
																		
			portfolioScenario: 'Headquarters Baseline',		// e.g. "3434 - Marlborough Expansion". This is the gp.portfolio_scenario value to restrict the query on the group table on to get just groups for the current scenario.			asOfDate: '',									// The date for which to draw the stack diagram. The control passes this date to the query that returns the Group data, as groups have start and end dates for their allocation. The control also passes this date to the OnDrop and OnClick event callbacks, as the callback needs this date to update the group record according to the date the action (e.g. a move, a lease signing) happened.
						
			tooltipHandler: onTooltip,
			
			dropHandler: onDrop,
			
			clickHandler: onClickItem,
			
			rightClickHandler: onRightClickItem
		};
	
	var stackControl = new StackControl('stackContainer', config);
	
	var groupDataSourceConfig = {
			groupLabels: 'gp.dv_id;gp.dp_id',						// A list of fields that will make up the labels for each group in the stack.
															// e.g. "gp.planning_bu_id" or "gp.dv_id;gp.dp_id" or "gp.dp_id;gp.gp_function".	
			buildingField: 'gp.bl_id',						// The field holding the Building Code (e.g. gp.bl_id)
			
			floorField: 'gp.fl_id', 						// The field holding the Floor Code (e.g. gp.fl_id)
			
			floorNameField: '01 - Offices', 				// The field holding the floor name (e.g. fl.name or "01 - Offices")
			
			allocationTypeField: 'gp.allocation_type', 		// The field that contains the allocation type, e.g. "gp.allocation_type".
															// See the enumerated values below.
			
			areaField:  'gp.area_manual',					// The field that contains the area of the group, e.g. "gp.area_manual".
			
			headcountField: 'gp.count_em',					// The field that contains the total headcount of the group, e.g. "gp.count_em".
			
			highlightField: 'dv.hpattern_acad',  			//The field that holds the highlight color.		 "dp.hpattern_acad".
			
			sortOrderField: 'gp.sort_order'					// The field that holds the sort order. "gp.sort_order"
		};
	
	stackControl.setGroupDataSourceConfig(groupDataSourceConfig);
	stackControl.labelLargeTextHeight = getLabelLargeTextHeight();
	stackControl.labelSmallTextHeight = getLabelSmallTextHeight();
	
	var numberOfBuildings = getNumberOfBuildings();
	

	for(var i=1; i<=numberOfBuildings; i++) {
		//stackControl.addBuilding("HQ");	
		stackControl.config.buildings.push("HQ");
	}


	stackControl.dataRaw = [records1, records2, records3];

	var stackConverter = new StackConverter('stackContainer', config);
	var colors = stackControl.areaColorAllocatedList.slice().reverse();
	var data1 = stackConverter.convertData(stackControl.dataRaw[0], 1, stackControl, colors, {}, true);
	var data2 = stackConverter.convertData(stackControl.dataRaw[1], 2, stackControl, colors, {}, true);
	var data3 = stackConverter.convertData(stackControl.dataRaw[2], 3, stackControl, colors, {}, true);
		
	var DATA_ARRAY = [];
	for (var i=0; i< getNumberOfBuildings(); i++) {
		DATA_ARRAY.push( eval("data" + (i + 1)) );		
	}
	
	stackControl.setData(DATA_ARRAY);				
	stackControl.setProfileData([profile1, profile2, profile3]);	
	stackControl.setStatisticsData([statistics1, statistics2, statistics3]);	
	stackControl.config.showXAxis = getShowXAxis();
	stackControl.config.showHighlightFloor = showHighlightFloor();
	stackControl.config.horizontalScale = getHorizontalScale();
	
	stackControl.build(config.stackOrientation, numberOfBuildings, DATA_ARRAY, stackControl.statisticsData, stackControl.profileData, stackControl.dataRaw );
}


// tooltip
function onTooltip(d) {
	var str = "<strong>ID:</strong> <span>" + d['gp.gp_id'] + "</span>";
		str += "<br/><strong>Division:</strong> <span>" + d['gp.dv_id'] + "</span>";
		str += "<br/><strong>Type:</strong> <span>" + d['gp.allocation_type'] + "</span>"; 
		str += "<br/><strong>Building:</strong> " + d['gp.bl_id'] + "</span>";
		str += "<br/><strong>Floor:</strong> " + d['gp.fl_id'] + "</span>";
		str += "<br/><strong>Order:</strong> " + d['gp.sort_order'] + "</span>";
		str += "<br/><strong>Color:</strong> " + d['dv.hpattern_acad'] + "</span>";
		str += "<br/><strong>Area:</strong> " + d['gp.area_manual'] + "</span>";
		return str;
}

function onDrop(obj) {   
	var recordToUpdate = obj.sourceRecord;
	var targetRecord = obj.targetRecord; 
	var previousRecord = obj.previousRecord;
	
	// update the bl, fl, and fl sort order
	recordToUpdate['gp.bl_id'] = targetRecord['gp.bl_id'];             	
	recordToUpdate['gp.fl_id'] = targetRecord['gp.fl_id'];                    	
	recordToUpdate['fl.name'] = targetRecord['fl.name'];
	recordToUpdate['fl.sort_order'] = targetRecord['fl.sort_order'];

	// if there is a group before the target, calculate and use midpoint. Otherwise, if this is first group, use same sort order - 1
	var targetOrder = targetRecord['gp.sort_order'];
	var midpoint = targetOrder;
	if (obj.previousRecord != null) {
		var previousOrder = previousRecord['gp.sort_order'];
		midpoint = (Number(previousOrder) + Number(targetOrder)) / 2;                 		
	} else {
		midpoint = targetOrder- 1;
	}
	recordToUpdate['gp.sort_order'] = midpoint;
		
	// if different stacks  (could be the same building name, but in a different stack)                
	if (Number(obj.targetStackNumber) !== Number(obj.sourceStackNumber)) {
		
		// remove record from source records array
		var sourceRecords = eval("records" + (obj.sourceStackNumber));
		sourceRecords.splice(obj.sourceRecordNumber, 1);
				
		// insert record to target records array
		var targetRecords = eval("records" + (obj.targetStackNumber));
		targetRecords.push(recordToUpdate);
	}
	
	build();	
}

function onClickItem(item) { 
	alert('ID ' + item['gp.gp_id'] + ' was selected.')
	
}

function onRightClickItem(item) { 
	alert("Right-click")
}

