
View.createController('test', {
	
    testPanel_onTestParsed: function() {
        var restriction = new Ab.view.Restriction();
        restriction.addClause('rm.bl_id', 'HQ', '=');
        restriction.addClause('rm.fl_id', ['16', '17', '18'], 'IN', 'AND');
        this.testPanel.refresh(restriction);
    },
    
    testPanel_onTestSql: function() {
    	var restriction = "rm.bl_id IN (SELECT bl.bl_id FROM bl WHERE bl.area_ocup > 0 UNION SELECT bl.bl_id FROM bl WHERE bl.area_gp > 0)";
        this.testPanel.refresh(restriction);
    },
    
    testPanel_onTestLocalized: function() {
    	var records = this.ds7.getRecords();
    	var value = records[0].getValue('wr.date_requested');
    	var localizedValue = this.ds7.formatValue('wr.date_requested', value, true);
    	View.alert(value + ' --- ' + localizedValue);
    }
});