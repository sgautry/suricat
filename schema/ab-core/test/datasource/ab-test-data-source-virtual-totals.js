
View.createController('testDataSourceVirtualTotals', {
	
	testVirtualTotals_grid_onShowNetExpense: function() {
	    var restriction = new Ab.view.Restriction();
	    restriction.addClause('ls.vf_net_income', 0, '&lt;');
	    this.testVirtualTotals_grid.refresh(restriction);
    }
});