var testDatasourcesController =  View.createController('testDatasourcesController',{
	
	dataSourceViewName: 'ab-floorsonly-drawing.axvw',
	labelDataSourceName: 'employeesDs',
	highlightDataSourceName: 'highlightTypesDs',
	dwgname: 'hq15',
	
	afterViewLoad:function(){	
		this.dsView.addEventListener('afterLoad',this.afterDataSourcesLoaded.createDelegate(this));
	},
	
	afterDataSourcesLoaded: function(){
		var contentFrame = this.dsView.getContentFrame();
		var contentView = contentFrame.View;
		
		var labelDS = contentView.dataSources.get(this.labelDataSourceName);
		var thisLabelDS = new Ab.data.DataSource(this.labelDataSourceName, labelDS.config);
		View.dataSources.add(thisLabelDS);
		
		var highlightDS = contentView.dataSources.get(this.highlightDataSourceName);
		var thisHighlightDS = new Ab.data.DataSource(this.highlightDataSourceName, highlightDS.config);
		View.dataSources.add(thisHighlightDS);

		this.testDatasources_drawingPanel.currentLabelsDS = this.labelDataSourceName;
		this.testDatasources_drawingPanel.labelsDataSource = this.labelDataSourceName;
		this.testDatasources_drawingPanel.applyDS('label');
		
		this.testDatasources_drawingPanel.currentHighlightDS = this.highlightDataSourceName;
		this.testDatasources_drawingPanel.highlightDataSource = this.highlightDataSourceName;
		this.testDatasources_drawingPanel.applyDS('highlight');
		
		var dcl = new Ab.drawing.DwgCtrlLoc('HQ','15',null,this.dwgname);
		var opts = new DwgOpts();
		opts.rawDwgName = this.dwgname;
		this.testDatasources_drawingPanel.addDrawing(dcl,opts);
	},

	dsView_onLoadDataSources:function(){
		this.dsView.loadView(this.dataSourceViewName);
	}
});