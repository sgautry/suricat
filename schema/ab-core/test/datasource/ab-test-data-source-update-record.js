
View.createController('testDataSourceUpdateRecord', {
	
	em_id: '',
	bl_id: '',
	fl_id: '',
	rm_id: '',
	
	emReport_afterRefresh: function() {
	    this.em_id = this.emReport.getFieldValue('em.em_id');
	    this.bl_id = this.emReport.getFieldValue('em.bl_id');
	    this.fl_id = this.emReport.getFieldValue('em.fl_id');
	    this.rm_id = this.emReport.getFieldValue('em.rm_id');
    },
	
	emReport_onUpdate: function() {
	
	    var rec = new Ab.data.Record();
	    rec.isNew = false;
	    rec.setValue("em.em_id", this.em_id);
	    rec.setValue("em.bl_id", this.emReport.getFieldValue('em.bl_id'));
	    rec.setValue("em.fl_id", this.emReport.getFieldValue('em.fl_id'));
	    rec.setValue("em.rm_id", this.emReport.getFieldValue('em.rm_id'));   
	 
	    rec.setOldValue("em.em_id", this.em_id);
	    
	    // KB 3026207: the code below should not be required. 
	    
	    // rec.setOldValue("em.bl_id", this.bl_id);
	    // rec.setOldValue("em.fl_id", this.fl_id);
	    // rec.setOldValue("em.rm_id", this.rm_id);
	    
	    this.emDS.saveRecord(rec);
    }
});