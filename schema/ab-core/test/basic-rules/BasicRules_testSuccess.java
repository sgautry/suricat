
import org.apache.log4j.Logger;
import com.archibus.eventhandler.BasicRuleBase;

public class BasicRules_testSuccess extends BasicRuleBase {

    public void handle() {
        String message = "Basic rule called";
        log.debug(message);
    }
}
