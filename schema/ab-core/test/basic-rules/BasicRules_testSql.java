
import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;

import org.apache.log4j.Logger;

import com.archibus.context.*;
import com.archibus.datasource.*;
import com.archibus.datasource.data.*;
import com.archibus.datasource.restriction.*;
import com.archibus.eventhandler.*;
import com.archibus.jobmanager.*;
import com.archibus.utility.*;

public class BasicRules_testSql extends BasicRuleBase {

    public String handle() {
        String message = "Basic SQL rule called";
        log.debug(message);
        
        String tableName = "rm";
        String[] fieldNames = { "area", "rm_id" };
        String sql = "SELECT rm_id, area FROM rm WHERE bl_id='HQ' AND fl_id='17'";
        List<DataRecord> records = SqlUtils.executeQuery(tableName, fieldNames, sql);
        
        return message;
    }
}
