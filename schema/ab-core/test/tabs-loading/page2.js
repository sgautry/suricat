
function user_form_onload() {
    var message = 'Page 2 onload: first panel ID = [' + View.panels.get(0).id + '], view name = [' + View.viewName + ']';
    View.log(message);
}

function user_form_afterSelect() {
    View.log('Page 2: afterSelect');
}

function panel1_afterRefresh() {
    View.log('Page 2: afterRefresh');
}
