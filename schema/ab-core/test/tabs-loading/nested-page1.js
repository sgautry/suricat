
var nestedController = View.createController('nested', {
    
    afterViewLoad: function() {
        View.log('Nested page 1: after view load');
        
        // addach event lsistener to the beforeTabChange events of both tab panels
        this.masterTabs.addEventListener('beforeTabChange', this.beforeMasterTabChange.createDelegate(this));
        this.nestedTabs.addEventListener('beforeTabChange', this.beforeNestedTabChange.createDelegate(this));
    },
    
    afterInitialDataFetch: function() {
        View.log('Nested page 1: after initial data fetch');
    },

    /**
     * Called when the user selects any tab in the master tab panel.
     * @param {Object} tabPanel
     * @param {Object} selectedTabName
     * @param {Object} newTabName
     */
    beforeMasterTabChange: function(tabPanel, selectedTabName, newTabName) {
        View.log('Master tab panel: ' + selectedTabName + ' ==> ' + newTabName);
    },
    
    /**
     * Called when the user selects any tab in the nested tab panel.
     * @param {Object} tabPanel
     * @param {Object} selectedTabName
     * @param {Object} newTabName
     */
    beforeNestedTabChange: function(tabPanel, selectedTabName, newTabName) {
        View.log('Nested tab panel: ' + selectedTabName + ' ==> ' + newTabName);
        
        if (newTabName == 'nestedPage1') {
            var restriction = new Ab.view.Restriction();
            restriction.addClause('em.em_id', 'A%', 'LIKE');
            this.nestedPanel1.refresh(restriction);    
        }
        if (newTabName == 'nestedPage2') {
            var restriction = new Ab.view.Restriction();
            restriction.addClause('project.project_id', 'A%', 'LIKE');
            this.nestedPanel2.refresh(restriction);    
        }
    }
});
