
View.createController('test', {
	
	parentGrid_onSelect: function(row) {
		// short version
        var r1 = row.getRecord().toRestriction();
        this.childGrid.refresh(r1);
        
		// long version
        // var r2 = new Ab.view.Restriction();
		// var contactType = row.getRecord().getValue('contact.contact_type');
        // r2.addClause('contact.contact_type', contactType, '=');
        // this.childGrid.refresh(r2);
	}
});
