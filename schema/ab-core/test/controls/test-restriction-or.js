
View.createController('test', {
	testPanel_onTest: function() {
		var restriction = new Ab.view.Restriction();
        restriction.addClause("wr.wr_id", '150000001', "=");
        restriction.addClause("wr.wr_id", '150000002', "=", "OR");
        restriction.addClause("wr.wr_id", '150000003', "=", "OR");
		this.testPanel.refresh(restriction);
	}
});
