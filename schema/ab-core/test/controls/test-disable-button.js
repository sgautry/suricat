
/**
 * This controller disables the test button.
 */
View.createController('testDisableButton', {
    
    /**
     * Buttons can be enabled/disabled after the initial data fetch, 
     * but not immediately after view load.
     */
    afterInitialDataFetch: function() {
        // disable the test button
        this.form.enableButton('test', false);
    },
    
    form_onRefresh: function() {
        // refresh will enable/disable all buttons based on the action/@enabled attribute in AXVW
        this.form.refresh();
        
        // disable the test button again
        this.form.enableButton('test', false);
    }
});