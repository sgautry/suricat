
function testRestriction() {
    var r1 = new Ab.view.Restriction();	
	r1.addClause('c1', 'A');
	r1.addClause('c2', 'B');
	
	var r2 = new Ab.view.Restriction();
	r2.addClause('c2', 'C');
	r2.addClause('c3', 'D');
	
	r1.addClauses(r2);
	
	var c1 = r1.findClause('c1');
    var c2 = r1.findClause('c2');
    var c3 = r1.findClause('c3');
    
    alert(c1.name + '=' + c1.value + '\n' +
          c2.name + '=' + c2.value + '\n' +
          c3.name + '=' + c3.value);
}