/**
 * Controller for the employees tab.
 * @Author Jikai Xu
 * Events:
 * app:teamspace:editteam:changedate
 */
var employeeOnTeamController = View.createController('employeeOnTeamController', {

    teamId:"",

    asOfDate: "",

    //store the from date to check when adding employees
    fromDateInFilter:"",

    //store the to date to check when adding employees
    toDateInFilter:"",

    //to identify the phase for filter(from date input is empty when initializing)
    isInitial: false,


    /**
     * initializing
     */
    afterInitialDataFetch : function(){

        isInitial = true;

        this.asOfDate = this.getCurrentOsOfDate();

        this.teamId = "SALES";

        //render the tab if in edit mode, or not render until a new team added
        if(valueExistsNotEmpty(this.teamId)){
            this.renderPage();
        }
        isInitial = false;

        //set asOfDate to check when assign employees
        this.fromDateInFilter = this.asOfDate;

        //disable 'add selected' button
        this.emAvailablePanel.actions.get("addSelected").enable(false);

    },

    /**
     * assign multiple selected employees to a team
     */
    emAvailablePanel_onAddSelected: function(){
        View.openProgressBar('Waiting',config = {
            interval: 500
        });

        this.addSelectedEmployeesAfterOpenProgressBar.defer(500, this, [isInitial]);



    },

    addSelectedEmployeesAfterOpenProgressBar: function(isInitial){
        var rows = this.emAvailablePanel.getSelectedGridRows();
        var len = rows.length;

        if(len>0&&this.isDateNotModified()){
            var selectedEmIds = [];

            for ( var i = 0; i < len; i++) {
                //check the employee whether could be assigned to the team
                this.validateEmployeesAndAssign(rows[i].record["em.em_id"]);
                View.updateProgressBar(i/len);
            }

            View.closeProgressBar();

            //display employees on team and available employees
            this.refreshEmployeesTab(false);

            //disable 'add selected' button
            this.emAvailablePanel.actions.get("addSelected").enable(false);
        }

    },

    /**
     * load the panel and add custom components
     */
    renderPage:function(){
        //display employees on team and available employees
        this.refreshEmployeesTab(true);

        this.employeeFilterOptions.setFieldValue("team.date_start", this.asOfDate);
        //set end date empty
        this.employeeFilterOptions.setFieldValue("team.date_end", "");

        this.appendUnassignedChbToTitleBar();
    },

    /**
     * render the field of teams to show team list on employees on team panel
     */
    emPanel_afterRefresh: function(){
        var employeeOnTeamController = this;
        this.emPanel.gridRows.each( function(row) {
            var emId = row.getFieldValue('team.em_id');
            emId = employeeOnTeamController.getTeams(emId, false);
            row.setFieldValue("team.teams", emId );
        });
    },

    /**
     * render the field of teams to show team list on available employees
     */
    emAvailablePanel_afterRefresh: function(){
        var employeeOnTeamController = this;
        this.emAvailablePanel.gridRows.each( function(row) {
            var emId = row.getFieldValue('em.em_id');
            emId = employeeOnTeamController.getTeams(emId, true);
            row.setFieldValue("em.teams", emId );
        });
    },

    /**
     * remove one item on the grid
     * @param row
     */
    emPanel_onRemove: function(row) {
        var autonumbered_id = row.record['team.autonumbered_id'];
        var restriction = new Ab.view.Restriction();
        restriction.addClause('team.autonumbered_id', autonumbered_id, '=');
        var record =  this.team_ds.getRecord(restriction);
        if(record)
        {
            this.team_ds.deleteRecord(record);
            this.refreshEmployeesTab(false);
        }
    },

    /**
     * save the selected employees
     */
    onSaveSelectedEmployees : function()
    {
        try{

            var startDate = this.editMembershipDatePanel.getFieldElement("edit_form.team.date_start").value;
            var endDate = this.editMembershipDatePanel.getFieldElement("edit_form.team.date_end").value;

            //validate the date range
            if (valueExistsNotEmpty(startDate) && valueExistsNotEmpty(endDate)) {
                // the compareLocalizedDates() function expects formatted (displayed) date values
                if (compareLocalizedDates(endDate,startDate)){
                    // display the error message defined in AXVW as message element
                    View.alert(getMessage('error_date_range'));
                    return;
                }
            }

            if(!valueExistsNotEmpty(startDate))
            {
                View.alert(getMessage('no_start_date_selected'));
                return;
            }

            var rows = this.emPanel.getSelectedGridRows();
            var len = rows.length;
            var selectedEmIds = [];
            for ( var i = 0; i < len; i++) {
                selectedEmIds.push(rows[i].record["team.autonumbered_id"]);
            }

            //save date to room
            var emRes=new Ab.view.Restriction();
            emRes.addClause('team.autonumbered_id',selectedEmIds,'IN');
            emRes.addClause('team.team_id',this.teamId,'=');
            var records=this.team_ds.getRecords(emRes);
            var employeesWithDateConflicts = "";
            len = records.length;
            for (var i=0;i<len;i++){

                //validate the date range to check whether the employee has been added to this team at this date range
                var emId = records[i].getValue("team.em_id");
                var teamId = records[i].getValue("team.team_id");
                var autonumbered_id = records[i].getValue("team.autonumbered_id");
                //CHECK date range when varies
                var isDateValidate = this.validateDateRangeBeforeSave(autonumbered_id,
                    emId, teamId, getDateWithISOFormat(startDate), getDateWithISOFormat(endDate));
                if(!isDateValidate){
                    employeesWithDateConflicts += emId+"|"
                    continue;
                }

                records[i].setValue("team.pct_time", this.editMembershipDatePanel.getFieldElement("team.pct_time").value);
                if (startDate != '<VARIES>') {
                    records[i].setValue("team.date_start", getDateWithISOFormat(startDate));
                }
                if (endDate != '<VARIES>') {
                    records[i].setValue("team.date_end", getDateWithISOFormat(endDate));
                }

                records[i].isNew = false;
                this.team_ds.saveRecord(records[i]);
            }

            if(valueExistsNotEmpty(employeesWithDateConflicts)){
                employeesWithDateConflicts = employeesWithDateConflicts.substring(0,employeesWithDateConflicts.length-1);
                View.alert(getMessage('date_range_overlap').replace("{0}", employeesWithDateConflicts));
            }else{
                this.emPanel.refresh();
                this.editMembershipDatePanel.show(false);
            }

        }catch(e){
            View.closeProgressBar();
            View.showMessage('error', '', e.message, e.data);
        }

    },

    /**
     * validate the date range for an employee
     * @param autonumbered_id
     * @param emId
     * @param teamId
     * @param startDate
     * @param endDate
     * @return true if no overlap, false if overlap
     */
    validateDateRangeBeforeSave: function(autonumbered_id, emId, teamId, startDate, endDate){
        var isValidate = true;
        var isEmOnTeam = "";
        if (valueExistsNotEmpty(startDate)&&valueExistsNotEmpty(endDate)) {
            isEmOnTeam = " team.em_id ='"+emId+"' and team.team_id='"+teamId+"' and (${sql.yearMonthDayOf('team.date_start')} <='"+endDate+"' and ( team.date_end is null or ${sql.yearMonthDayOf('team.date_end')}>='"+startDate+"'))";
        }
        else if (!valueExistsNotEmpty(endDate)) {
            isEmOnTeam = " team.em_id ='"+emId+"' and team.team_id='"+teamId+"' and (team.date_end is null or ${sql.yearMonthDayOf('team.date_end')}>='"+startDate+"') ";
        }

        this.checkEmOnTeam_ds.addParameter('isEmOnTeam', isEmOnTeam);
        var records = this.checkEmOnTeam_ds.getRecords();
        //iterate the records to check whether having different items in the previous query. if not, could save.
        for (var i=0;i<records.length;i++){

            if(records[i].getValue("team.autonumbered_id")!=autonumbered_id){
                isValidate = false;
            }
        }

        return isValidate;
    },

    /**
     * remove the selected employees
     * @param row
     */
    onRemoveSelectedEmployees : function()
    {
        try{
            var rows = this.emPanel.getSelectedGridRows();
            var len = rows.length;
            var selectedTeamIds = "";
            for ( var i = 0; i < len; i++) {
                var restriction = new Ab.view.Restriction();
                restriction.addClause('team.autonumbered_id', rows[i].record["team.autonumbered_id"], '=');
                var record =  this.team_ds.getRecord(restriction);
                if(record)
                {
                    this.team_ds.deleteRecord(record);
                }
            }
            this.editMembershipDatePanel.show(false);
            this.refreshEmployeesTab(false);
        }catch(e){
            View.closeProgressBar();
            View.showMessage('error', '', e.message, e.data);
        }
    },

    /**
     * edit selected item(s)
     */
    emPanel_onMultipleSelectionChange: function(row) {

        this.editMembershipDatePanel.show(true);

        var rows = this.emPanel.getSelectedGridRows();

        if(rows.length>0)
        {
            var fields = ['em.em_id','team.organization','team.pct_time','team.date_start','team.date_end'];
            var elements = ['em.em_id','team.organization','team.pct_time','edit_form.team.date_start','edit_form.team.date_end'];
            var fldHtmlId = ['ShoweditMembershipDatePanel_em.em_id','ShoweditMembershipDatePanel_team.organization','editMembershipDatePanel_team.pct_time','editMembershipDatePanel_edit_form.team.date_start','editMembershipDatePanel_edit_form.team.date_end'];

            for ( var i = 0; i < fields.length; i++) {
                this.setFieldValue(fields[i], elements[i], rows, fldHtmlId[i]);
            }
            this.editMembershipDatePanel.show(true);
        }
        else
        {
            this.editMembershipDatePanel.clear();
            this.editMembershipDatePanel.show(false);
        }

    },

    /**
     * set field value in the form for multiple records
     */
    setFieldValue : function(fieldName, elementName, records, fldHtmlId) {
        var fieldValue = records[0].record[fieldName];
        for ( var i = 0; i < records.length; i++) {
            if (records[i].record[fieldName] != fieldValue) {
                fieldValue = '<VARIES>';
                break;
            }
        }
        if(fieldValue == '<VARIES>')
        {
            if(fldHtmlId.indexOf('date')>0||fldHtmlId.indexOf('pct_time')>0)
            {
                $(fldHtmlId).value = fieldValue;
            }
            else
            {
                $(fldHtmlId).innerText = fieldValue;
            }

        }
        else
        {
            this.editMembershipDatePanel.setFieldValue(elementName, fieldValue, fieldValue, false);
        }
    },

    /**
     * display teams which an employee was assigned to
     */
    getTeamList: function(panelId){
        var emPanel = View.panels.get(panelId);
        var index = emPanel.selectedRowIndex;
        var row = emPanel.gridRows.get(index);

        var startDate = View.panels.get("employeeFilterOptions").getFieldValue("team.date_start");
        var endDate = View.panels.get("employeeFilterOptions").getFieldValue("team.date_end");
        if(isInitial){
            startDate = this.getCurrentOsOfDate();
        }

        var teamRes=new Ab.view.Restriction();

        var emId = row.getFieldValue("em.em_id");
        var isAvailablePanel = false;
        if(panelId=="emPanel"){
            emId = row.getFieldValue("team.em_id");
            isAvailablePanel = false;
        }
        else if(panelId=="emAvailablePanel"){
            emId = row.getFieldValue("em.em_id");
            isAvailablePanel = true;
        }

        var restriction = this.getRestrictionForTeamList(teamRes, emId, startDate, endDate, isAvailablePanel)

        View.openDialog("ab-sp-console-team-edit-em-teams.axvw", restriction, false, {
            width: 1000,
            height: 800,
            title: 'Team List',
            closeButton: false
        });
    },

    /**
     * get the teams that an employee has been added to
     * @param emId
     * @param isAvailablePanel
     * @return teamId
     */
    getTeams : function(emId, isAvailablePanel){

        var startDate = View.panels.get("employeeFilterOptions").getFieldValue("team.date_start");
        var endDate = View.panels.get("employeeFilterOptions").getFieldValue("team.date_end");
        if(isInitial){
            startDate = this.getCurrentOsOfDate();
        }

        var teamList = "";

        var emTeamsDs = View.dataSources.get('employeeTeamsDS');
        emTeamsDs.addParameter('emId', emId);
        if(isAvailablePanel){
            emTeamsDs.addParameter('teamId', "1=1");
        }else{
            emTeamsDs.addParameter('teamId', "team_id!='"+this.teamId+"'");
        }

        emTeamsDs.addParameter('fromDate', startDate);
        emTeamsDs.addParameter('toDate', endDate);
        var teams = emTeamsDs.getRecords();

        for (var i=0; i<teams.length; i++ ){
            teamList += teams[i].getValue('team.team_id')+",";
        }
        teamList = teamList.substring(0, teamList.length-1);

        return teamList;

    },

    /**
     * get the restriction for team list
     * @param teamRes
     * @param startDate
     * @param endDate
     * @param isAvailablePanel
     * @return teamRes
     */
    getRestrictionForTeamList: function(teamRes, emId, startDate, endDate, isAvailablePanel){

        if(valueExistsNotEmpty(startDate)&&!valueExistsNotEmpty(endDate)){
            //from date is available, to date is empty
            teamRes.addClause('team.em_id',emId,'=');
            if(!isAvailablePanel){
                teamRes.addClause("team.team_id", this.teamId, "<>");
            }
            teamRes.addClause("team.date_end", null, "IS NULL", ")AND(");
            teamRes.addClause("team.date_end", startDate, ">=", "OR");
        }else if(valueExistsNotEmpty(startDate)&&valueExistsNotEmpty(endDate)){
            //from date is available, to date is available
            teamRes.addClause('team.em_id',emId,'=');
            if(!isAvailablePanel){
                teamRes.addClause("team.team_id", this.teamId, "<>");
            }
            teamRes.addClause("team.date_start", endDate, "<=");
            teamRes.addClause("team.date_end", null, "IS NULL", ")AND(");
            teamRes.addClause("team.date_end", startDate, ">=", "OR");

        }else if(!valueExistsNotEmpty(startDate)&&!valueExistsNotEmpty(endDate)){
            //from date and to date are both empty
            teamRes.addClause('team.em_id',emId,'=');
            if(!isAvailablePanel){
                teamRes.addClause("team.team_id", this.teamId, "<>");
            }
        }else if(!valueExistsNotEmpty(startDate)&&valueExistsNotEmpty(endDate)){
            //from date is empty, to date is available
            teamRes.addClause('team.em_id',emId,'=');
            if(!isAvailablePanel){
                teamRes.addClause("team.team_id", this.teamId, "<>");
            }
            teamRes.addClause("team.date_start", endDate, "<=");
        }
        return teamRes;
    },

    /**
     * add custom checkbox for unassigned to any team
     */
    appendUnassignedChbToTitleBar: function(){
        var panelTitleNode = this.emAvailablePanel.getTitleEl().dom.parentNode.parentNode.parentNode;
        var tr = Ext.DomHelper.append(panelTitleNode, {
            tag : 'tr',
            id : 'unassignedChbTr'
        });
        var cell = Ext.DomHelper.append(tr, {
            tag : 'td',
            id : 'unassignedChbTd'
        });
        var div = "<div class='x-toolbar x-small-editor panelToolbar' id='unassignedChbTitlebar'/>";
        var divCell = Ext.DomHelper.append(cell, div , true);
        var str = "<div class='checkbox-container'>"+"<input type='checkbox' id='emUnassigned'/>"+"<span translatable='true'>"+View.getLocalizedString(getMessage("unassigned_checkbox"))+"</span></div>";
        var unassignedChb = Ext.DomHelper.append(divCell, str , true);

        document.getElementById('emUnassigned').onclick=function(){
            var isChecked = $('emUnassigned').checked;
            var employeeOnTeamController = View.controllers.get("employeeOnTeamController");
            employeeOnTeamController.getUnassignedEmployees();

            if(!isChecked){
                employeeOnTeamController.refreshEmployeesTab(false);
            }


        };
    },
    getUnassignedEmployees: function(){

        var isChecked = $('emUnassigned').checked;
        var employeeOnTeamController = View.controllers.get("employeeOnTeamController");
        var emAvailablePanel = View.panels.get("emAvailablePanel");
        var employeeFilterOptions = View.panels.get("employeeFilterOptions");
        var startDate = employeeFilterOptions.getFieldValue("team.date_start");
        var endDate = employeeFilterOptions.getFieldValue("team.date_end");

        if(isChecked){
            var con = " NOT EXISTS (select 1 from team where em.em_id=team.em_id and " +
                "( '"+startDate+"'!='' and '"+endDate+"'='' and (team.date_end IS NULL OR ${sql.yearMonthDayOf('team.date_end')} >= '"+startDate+"') " +
                "or '"+startDate+"'!='' and '"+endDate+"'!='' and ${sql.yearMonthDayOf('team.date_start')}<= '"+endDate+"' and (team.date_end IS NULL OR ${sql.yearMonthDayOf('team.date_end')} >= '"+startDate+"') " +
                "or '"+startDate+"'='' and '"+endDate+"'!='' and ${sql.yearMonthDayOf('team.date_start')}<='"+endDate+"' " +
                "or '"+startDate+"'='' and '"+endDate+"'='' and 1=1)) ";
            emAvailablePanel.addParameter('unassigned', con);
            emAvailablePanel.refresh();
            emAvailablePanel.show();
        }
        else{
            emAvailablePanel.addParameter('unassigned', "1=1");
        }

    },

    /**
     * change the add selected button's state according to selected employees
     */
    emAvailablePanel_onMultipleSelectionChange: function(row) {

        var rows = this.emAvailablePanel.getSelectedGridRows();

        if(rows.length>0){
            this.emAvailablePanel.actions.get("addSelected").enable(true);
        }else{
            this.emAvailablePanel.actions.get("addSelected").enable(false);
        }
    },


    /**
     * assign single employee to a team
     */
    emAvailablePanel_onAdd: function(row) {

        var selectedEmId = row.record['em.em_id'];

        if(this.isDateNotModified()){
            //check the employee whether could be assigned to the team
            this.validateEmployeesAndAssign(selectedEmId);
            //display employees on team and available employees
            this.refreshEmployeesTab(false);
        }

    },

    /**
     * check the date whether modified by accident after execute filter
     */
    isDateNotModified: function(){
        var filterValues = this.getFieldsValueOfFilter();
        if(this.fromDateInFilter == filterValues.fromDate &&
            this.toDateInFilter == filterValues.toDate){
            return true;
        }else{
            View.alert(getMessage('addEmErrorMessage'));
        }

        return false;
    },

    /**
     * execute assign operation
     */
    validateEmployeesAndAssign: function(selectedEmId){

        var startDate = View.panels.get("employeeFilterOptions").getFieldValue("team.date_start");
        var endDate = View.panels.get("employeeFilterOptions").getFieldValue("team.date_end");
        var teamId = this.teamId;
        var emId = selectedEmId;

        //could add to team
        if(!valueExistsNotEmpty(endDate)){
            endDate = null;
        }
        var record = new Ab.data.Record({
            'team.team_id': teamId,
            'team.em_id': emId,
            'team.date_start': startDate,
            'team.date_end': endDate
        }, true);
        try {
            //This action will insert a new record into the team table.
            this.team_ds.saveRecord(record);
        }
        catch (e) {
            var message = getMessage('errorSave');
            View.showMessage('error', message, e.message, e.data);
            return;
        }
    },

    /**
     * Filters the employees.
     */
    employeeFilterOptions_onFilterEmployees: function() {
        this.refreshEmployeesTab(false);
    },

    /**
     * combine filter field value into an object
     */
    getFieldsValueOfFilter: function() {
        var filterValues = {};

        filterValues.blId = this.employeeFilterOptions.getFieldValue("rm.bl_id");
        filterValues.flId = this.employeeFilterOptions.getFieldValue("rm.fl_id");
        filterValues.rmId = this.employeeFilterOptions.getFieldValue("rm.rm_id");
        filterValues.emId = this.employeeFilterOptions.getFieldValue("em.em_id");
        filterValues.dvId = this.employeeFilterOptions.getFieldValue("em.dv_id");
        filterValues.dpId = this.employeeFilterOptions.getFieldValue("em.dp_id");
        filterValues.teamId = this.employeeFilterOptions.getFieldValue("team.team_id");
        filterValues.fromDate = this.employeeFilterOptions.getFieldValue("team.date_start");
        filterValues.toDate = this.employeeFilterOptions.getFieldValue("team.date_end");

        return filterValues;
    },

    /**
     * refresh employees tab
     * @param isInitial
     */
    refreshEmployeesTab: function(isInitial) {

        try {

            var filterValues = this.getFieldsValueOfFilter();

            var parameters = this.composeParameters(filterValues, isInitial);

            this.setParameters(parameters);

            this.displayEmployeesOnTeam();

            this.displayAvailableEmployees();


            //store from date to check consistency when adding
            this.fromDateInFilter = filterValues.fromDate;

            //store to date to check consistency when adding
            this.toDateInFilter = filterValues.toDate;

            //hide edit employee form
            this.editMembershipDatePanel.show(false);

        } catch (e) {

            View.showMessage('error', '', e.message, e.data);
        }
    },



    setParameters: function(parameters) {

        this.emPanel.addParameter('asOfDate', this.asOfDate);
        this.emPanel.addParameter('emLocation', parameters.emLocation);
        this.emPanel.addParameter('emId', parameters.emId);
        this.emPanel.addParameter('emOrg', parameters.emOrg);
        this.emPanel.addParameter('additionalTeam', parameters.additionalTeam4OnTeam);
        this.emPanel.addParameter('fromDate', parameters.fromDate);
        this.emPanel.addParameter('toDate', parameters.toDate);
        this.emPanel.addParameter('isInitial', parameters.isInitial);
        this.emPanel.addParameter('editTeamId', parameters.editTeamId);

        this.emAvailablePanel.addParameter('asOfDate', this.asOfDate);
        this.emAvailablePanel.addParameter('emLocation', parameters.emLocation);
        this.emAvailablePanel.addParameter('emId', parameters.emId);
        this.emAvailablePanel.addParameter('emOrg', parameters.emOrg);
        this.emAvailablePanel.addParameter('additionalTeam', parameters.additionalTeam4Available);
        this.emAvailablePanel.addParameter('fromDate', parameters.fromDate);
        this.emAvailablePanel.addParameter('toDate', parameters.toDate);
        this.emAvailablePanel.addParameter('isInitial', parameters.isInitial);
        this.emAvailablePanel.addParameter('editTeamId', parameters.editTeamId);

        if(!isInitial){
            this.getUnassignedEmployees();
        }

    },

    /**
     * show employees on team according to as of date
     */
    displayEmployeesOnTeam: function(){

        this.emPanel.refresh();
        this.emPanel.show();
    },

    /**
     * show available employees
     */
    displayAvailableEmployees: function(){

        this.emAvailablePanel.refresh();
        this.emAvailablePanel.show();
    },

    /**
     * get the sql statement according to the filter fields
     * @param filterValues
     * @param isInitial
     * @return parameters
     */
    composeParameters: function(filterValues, isInitial) {

        var parameters = {};
        var emLocation = "1=1", emId = "1=1", emOrg="1=1", additionalTeam="1=1", fromDate="1=1", toDate="1=1";

        //parameter for emLocation
        if (valueExistsNotEmpty(filterValues.blId)){
            var blRes = this.getFieldRestrictionById('em.bl_id', filterValues.blId);
            emLocation += " and " + blRes
        }
        if (valueExistsNotEmpty(filterValues.flId)){
            var flRes = this.getFieldRestrictionById('em.fl_id', filterValues.flId);
            emLocation += " and " + flRes;
        }
        if (valueExistsNotEmpty(filterValues.rmId)){
            var rmRes = this.getFieldRestrictionById('em.rm_id', filterValues.rmId);
            emLocation += " and " + rmRes;
        }
        parameters.emLocation = emLocation;

        //parameter for emId
        if (valueExistsNotEmpty(filterValues.emId))
        {
            emId = this.getFieldRestrictionById('em.em_id', filterValues.emId);
        }
        parameters.emId = emId;

        //parameter for emOrg
        if (valueExistsNotEmpty(filterValues.dpId))
        {
            var dpRes = this.getFieldRestrictionById('em.dp_id', filterValues.dpId);
            emOrg += " and " + dpRes;
        }
        if (valueExistsNotEmpty(filterValues.dvId))
        {
            var dvRes = this.getFieldRestrictionById('em.dv_id', filterValues.dvId);
            emOrg += " and " + dvRes;
        }
        parameters.emOrg = emOrg;

        //parameter for additionalTeam
        if (valueExistsNotEmpty(filterValues.teamId))
        {
            additionalTeam4OnTeam = " and exists (select 1 from team t where t.team_id = '"+ filterValues.teamId +"' and t.em_id = team.em_id)"
            additionalTeam4Available = " and exists (select 1 from team t where t.team_id = '"+ filterValues.teamId +"' and t.em_id = em.em_id)"
        }
        parameters.additionalTeam = additionalTeam;

        //parameter for fromDate
        parameters.fromDate = filterValues.fromDate;

        //parameter for toDate
        parameters.toDate = filterValues.toDate;

        parameters.isInitial = isInitial;
        parameters.editTeamId = this.teamId;

        return parameters;
    },

    /**
     * transform the values in filter
     */
    getFieldRestrictionById:function(fieldId, fieldValue){
        if ( fieldValue.indexOf(Ab.form.Form.MULTIPLE_VALUES_SEPARATOR) > 0 ) {
            return fieldId+" in " + stringToSqlArray(fieldValue);
        } else {
            return fieldId+" = '" + fieldValue+"'";
        }
    },

    /**
     * get employees records for preparing sql statement
     */
    getEmployeesRecords: function(con){
        this.em_ds.addParameter('conForTeamsTab', con);
        var records = this.em_ds.getRecords();
        var emId = "";
        for(var i = 0; i < records.length; i++) {
            var record = records[i];
            emId += "'"+record.getValue('em.em_id')+"',";
        }
        return emId.substring(0,emId.length-1);
    },

    /**
     * Clears the employees filter.
     */
    employeeFilterOptions_onClearFields: function() {
        this.clearFields();
    },

    /**
     * Clears the employees filter.
     * @isOnDefineTeams clear all fields except from date fields if refreshed on define teams
     */
    clearFields: function(isOnDefineTeams){
        this.employeeFilterOptions.setFieldValue("rm.bl_id","");
        this.employeeFilterOptions.setFieldValue("rm.fl_id","");
        this.employeeFilterOptions.setFieldValue("rm.rm_id","");
        this.employeeFilterOptions.setFieldValue("em.em_id","");
        this.employeeFilterOptions.setFieldValue("em.dv_id","");
        this.employeeFilterOptions.setFieldValue("em.dp_id","");
        this.employeeFilterOptions.setFieldValue("team.team_id","");

        if(isOnDefineTeams){
            this.employeeFilterOptions.setFieldValue("team.date_start",getCurrentDateInISOFormat());
            this.employeeFilterOptions.setFieldValue("team.date_end","");

        }else{
            //query again after clear the fields
            this.refreshEmployeesTab(false);
        }
    },

    /**
     * get as of date from team space console's filter .
     */
    getCurrentOsOfDate: function(){
        var currentAsOfDate = "";
        var teamEditController = View.getOpenerView().controllers.get('teamEditController');
        if(valueExistsNotEmpty(teamEditController)){
            currentAsOfDate = teamEditController.asOfDate;
        }
        else{
            currentAsOfDate = getCurrentDateInISOFormat();
        }
        return currentAsOfDate;
    },

    /**
     * invoked by define teams after select a new team and select this tab
     * @param isInitial
     * @param teamId
     */
    refreshPanels: function(isInitial,teamId){
        this.teamId = teamId;
        this.refreshEmployeesTab(isInitial);
    },

    /**
     * refresh panel for define teams
     * @param isInitial
     * @param teamId
     */
    refreshPanelsForDefineTeams: function(isInitial, teamId){
        this.refreshPanels(isInitial, teamId);

        //clear filter fields after refresh
        this.clearFields(true);

    }


});

function stringToSqlArray(string){
    var values = string.split(Ab.form.Form.MULTIPLE_VALUES_SEPARATOR);
    var resultedString = "('" + values[0] + "'";

    for (i = 1; i < values.length; i++) {
        resultedString += " ,'" + values[i] + "'";
    }

    resultedString += ")";

    return resultedString;
}