<view version="2.0">
    <message name="no_start_date_selected" translatable="true">Please select start date.</message>
    <message name="save_successfully" translatable="true">Save successfully.</message>
    <message name="error_date_range" translatable="true">The End Date cannot be earlier than Start Date.</message>
    <message name="date_range_overlap" translatable="true">Cannot save date range for {0} due to date conflicts.</message>
    <message name="start_date_title" translatable="true">Start Date</message>
    <message name="end_date_title" translatable="true">End Date</message>
    <message name="unassigned_checkbox" translatable="true">Unassigned to any team</message>
    <message name="errorSave" translatable="true">Cannot create new Team Record .</message>
    <message name="addEmErrorMessage" translatable="true">
        You must click the Show button to filter on the selected date range before assigning employees to the team
    </message>

    <js file="kb-3052512.js"/>

    <layout type="borderLayout" id="tabLayout">
        <north autoScroll="true" split="true" initialSize="90" minSize="70" maxSize="90"/>
        <center autoScroll="true" initialSize="50%"/>
        <east autoScroll="true" split="false" initialSize="350"/>
        <south autoScroll="true" split="true" initialSize="50%"/>
    </layout>

    <dataSource id="employeeTeamsDS" distinct="true">
        <table name="team"/>
        <field name = "team_id"/>
        <field name = "em_id"/>
        <restriction type="sql" sql=" em_id= ${parameters['emId']} and ${parameters['teamId']}
            AND (
            '${parameters['fromDate']}'!='' AND '${parameters['toDate']}'='' AND (team.date_end IS NULL OR ${sql.yearMonthDayOf('team.date_end')} &gt;= '${parameters['fromDate']}')
            OR '${parameters['fromDate']}'!='' AND '${parameters['toDate']}'!='' AND ${sql.yearMonthDayOf('team.date_start')} &lt;= '${parameters['toDate']}' AND (team.date_end is null or ${sql.yearMonthDayOf('team.date_end')} &gt;= '${parameters['fromDate']}')
            OR '${parameters['fromDate']}'='' AND '${parameters['toDate']}'!='' AND ${sql.yearMonthDayOf('team.date_start')} &lt;= '${parameters['toDate']}'
            OR '${parameters['fromDate']}'='' AND '${parameters['toDate']}'=''
            )
        "/>
        <parameter name="emId" dataType="text" value=""/>
        <parameter name="teamId" dataType="verbatim" value=""/>
        <parameter name="fromDate" dataType="verbatim" value=""/>
        <parameter name="toDate" dataType="verbatim" value=""/>
        <sortField name="team_Id" ascending="true"/>
    </dataSource>

    <dataSource id="team_ds">
        <table name="team"/>
        <field table="team" name="autonumbered_id"/>
        <field table="team" name="em_id" />
        <field table="team" name="team_id" />
        <field table="team" name="pct_time"/>
        <field table="team" name="date_start"/>
        <field table="team" name="date_end"/>
    </dataSource>

    <dataSource id="em_ds">
        <table name="team" role="main"/>
        <table name="em" role="standard"/>
        <field table="team" name="autonumbered_id"/>
        <field table="team" name="team_id" />
        <field table="team" name="em_id" />
        <field table="team" name="pct_time"/>
        <field table="em" name="em_id" />
        <field table="em" name="bl_id" />
        <field table="em" name="fl_id" />
        <field table="em" name="rm_id" />
        <field table="em" name="dv_id" />
        <field table="em" name="dp_id" />
        <field table="em" name="em_std" />
        <field table="em" name="status" />
        <field table="team" name="date_start" />
        <field table="team" name="date_end" />
        <field name="location" dataType="text">
            <title>Location</title>
            <sql dialect="generic">
                (select em.bl_id${sql.concat}'-'${sql.concat}em.fl_id${sql.concat}'-'${sql.concat}em.rm_id from em where em.em_id=team.em_id)
            </sql>
        </field>
        <field name="organization" dataType="text" size="200">
            <title>Organization</title>
            <sql dialect="generic">
                (select em.dv_id${sql.concat}'-'${sql.concat}em.dp_id from em where em.em_id=team.em_id)
            </sql>
        </field>
        <field name="teams" dataType="text">
            <title>Teams</title>
            <sql dialect="generic">
                ' '
            </sql>
        </field>


        <restriction type="sql" sql="
            ${parameters['emLocation']} AND ${parameters['emId']} AND ${parameters['emOrg']}
            AND ${parameters['additionalTeam']} AND team.team_id='${parameters['editTeamId']}' AND (
            '${parameters['isInitial']}'='false' AND '${parameters['fromDate']}'!='' AND '${parameters['toDate']}'='' AND (team.date_end IS NULL OR ${sql.yearMonthDayOf('team.date_end')} &gt;= '${parameters['fromDate']}')
            OR '${parameters['isInitial']}'='false' AND '${parameters['fromDate']}'!='' AND '${parameters['toDate']}'!='' AND ${sql.yearMonthDayOf('team.date_start')} &lt;= '${parameters['toDate']}' AND (team.date_end is null or ${sql.yearMonthDayOf('team.date_end')} &gt;= '${parameters['fromDate']}')
            OR '${parameters['isInitial']}'='false' AND '${parameters['fromDate']}'='' AND '${parameters['toDate']}'!='' AND ${sql.yearMonthDayOf('team.date_start')} &lt;= '${parameters['toDate']}'
            OR '${parameters['isInitial']}'='false' AND '${parameters['fromDate']}'='' AND '${parameters['toDate']}'='' AND 1=1
            OR '${parameters['isInitial']}'='true' AND (team.date_end IS NULL OR ${sql.yearMonthDayOf('team.date_end')} &gt;= '${parameters['asOfDate']}')
            )" />

        <parameter name="isInitial" dataType="verbatim" value="1=1"/>
        <parameter name="editTeamId" dataType="verbatim" value="1=1"/>
        <parameter name="asOfDate" dataType="verbatim" value="1=1"/>
        <parameter name="emLocation" dataType="verbatim" value="1=1"/>
        <parameter name="emId" dataType="verbatim" value="1=1"/>
        <parameter name="emOrg" dataType="verbatim" value="1=1"/>
        <parameter name="additionalTeam" dataType="verbatim" value="1=1"/>
        <parameter name="fromDate" dataType="verbatim" value="1=1"/>
        <parameter name="toDate" dataType="verbatim" value="1=1"/>
    </dataSource>

    <dataSource id="available_em_ds">
        <table name="em" role="main"/>

        <field table="em" name="em_id" />
        <field table="em" name="bl_id" />
        <field table="em" name="fl_id" />
        <field table="em" name="rm_id" />
        <field table="em" name="dv_id" />
        <field table="em" name="dp_id" />
        <field table="em" name="em_std" />
        <field table="em" name="status" />

        <field name="location" dataType="text">
            <title>Location</title>
            <sql dialect="generic">
                em.bl_id${sql.concat}'-'${sql.concat}em.fl_id${sql.concat}'-'${sql.concat}em.rm_id
            </sql>
        </field>
        <field name="organization" dataType="text" size="200">
            <title>Organization</title>
            <sql dialect="generic">
                em.dv_id${sql.concat}'-'${sql.concat}em.dp_id
            </sql>
        </field>
        <field name="teams" dataType="text">
            <title>Teams</title>
            <sql dialect="generic">
                ' '
            </sql>
        </field>
        <restriction type="sql" sql=" ${parameters['emLocation']} AND ${parameters['emId']}
        AND ${parameters['emOrg']} AND ${parameters['additionalTeam']}
        AND NOT EXISTS (select 1 from team where team.em_id=em.em_id and team.team_id='${parameters['editTeamId']}' AND (
        '${parameters['isInitial']}'='false' AND '${parameters['fromDate']}'!='' AND '${parameters['toDate']}'='' AND (team.date_end IS NULL OR ${sql.yearMonthDayOf('team.date_end')} &gt;= '${parameters['fromDate']}')
        OR '${parameters['isInitial']}'='false' AND '${parameters['fromDate']}'!='' AND '${parameters['toDate']}'!='' AND ${sql.yearMonthDayOf('team.date_start')} &lt;= '${parameters['toDate']}' AND (team.date_end is null or ${sql.yearMonthDayOf('team.date_end')} &gt;= '${parameters['fromDate']}')
        OR '${parameters['isInitial']}'='false' AND '${parameters['fromDate']}'='' AND '${parameters['toDate']}'!='' AND ${sql.yearMonthDayOf('team.date_start')} &lt;= '${parameters['toDate']}'
        OR '${parameters['isInitial']}'='false' AND '${parameters['fromDate']}'='' AND '${parameters['toDate']}'='' AND 1=1
        OR '${parameters['isInitial']}'='true' AND (team.date_end IS NULL OR ${sql.yearMonthDayOf('team.date_end')} &gt;= '${parameters['asOfDate']}')))
        AND ${parameters['unassigned']}
        "/>

        <parameter name="isInitial" dataType="verbatim" value="1=1"/>
        <parameter name="editTeamId" dataType="verbatim" value="1=1"/>
        <parameter name="asOfDate" dataType="verbatim" value="1=1"/>
        <parameter name="emLocation" dataType="verbatim" value="1=1"/>
        <parameter name="emId" dataType="verbatim" value="1=1"/>
        <parameter name="emOrg" dataType="verbatim" value="1=1"/>
        <parameter name="additionalTeam" dataType="verbatim" value="1=1"/>
        <parameter name="fromDate" dataType="verbatim" value="1=1"/>
        <parameter name="toDate" dataType="verbatim" value="1=1"/>
        <parameter name="unassigned" dataType="verbatim" value="1=1"/>
    </dataSource>

    <dataSource id="employeeFilterDS">
        <table name="rm" role="main"/>
        <table name="em" role="standard"/>
        <table name="team" role="standard"/>
        <field table="rm" name="bl_id"/>
        <field table="rm" name="fl_id"/>
        <field table="rm" name="rm_id"/>
        <field table="em" name="dv_id"/>
        <field table="em" name="dp_id"/>
        <field table="rm" name="rm_type"/>
        <field table="rm" name="rm_cat"/>
        <field table="em" name="em_id" />
        <field table="team" name="team_id" />
        <field table="team" name="date_start" />
        <field table="team" name="date_end" />
    </dataSource>

    <dataSource id="checkEmOnTeam_ds">
        <table name="team" role="main"/>
        <field table="team" name="autonumbered_id"/>
        <field table="team" name="team_id" />
        <field table="team" name="em_id" />
        <field table="team" name="date_start" />
        <field table="team" name="date_end" />
        <parameter name="isEmOnTeam" dataType="verbatim" value="1=1"/>
        <restriction type="sql" sql=" ${parameters['isEmOnTeam']} "/>
    </dataSource>
    <!-- filter -->
    <panel type="console" columns="5" id="employeeFilterOptions" dataSource="employeeFilterDS" layout="tabLayout" region="north">
        <title translatable="true">Filter</title>
        <field table="rm" name="bl_id" cssClass="shortField" showLabel="false">
            <title>Building</title>
            <action id="locationSelectBuilding">
                <title>...</title>
                <command
                        type="selectValue"
                        selectValueType="multiple"
                        autoComplete="true"
                        fieldNames="rm.bl_id"
                        selectFieldNames="bl.bl_id"
                        visibleFieldNames="bl.site_id,bl.bl_id,bl.name"/>
            </action>
        </field>
        <field table="rm" name="fl_id" cssClass="shortField" showLabel="false" selectValueType="multiple">
            <title>Floor</title>
        </field>
        <field table="rm" name="rm_id" cssClass="shortField" showLabel="false" selectValueType="multiple">
            <title>Room</title>
        </field>
        <field table="em" name="em_id" cssClass="shortField" showLabel="false" selectValueType="multiple">
            <title>Employee</title>
        </field>
        <field table="em" name="dv_id" cssClass="shortField" showLabel="false" selectValueType="multiple">
            <title>Division</title>
        </field>
        <field table="em" name="dp_id" cssClass="shortField" showLabel="false" selectValueType="multiple">
            <title>Department</title>
        </field>
        <field table="team" name="team_id" cssClass="shortField" showLabel="false" selectValueType="multiple">
            <title>Additional Team Codes</title>
        </field>
        <field table="team" name="date_start" cssClass="shortField" showLabel="true" onchange="employeeOnTeamController.onFromDateChanged(this.value)">
            <title>From Date</title>
        </field>
        <field table="team" name="date_end" cssClass="shortField" showLabel="true" onchange="employeeOnTeamController.onToDateChanged(this.value)">
            <title>To Date</title>
        </field>
        <action id="filterEmployees" cssClass="fieldButtonRight">
            <title>Show</title>
        </action>
        <action id="clearFields" cssClass="fieldButtonRight">
            <title>Clear</title>
        </action>
    </panel>

    <panel type="grid" id="emPanel" dataSource="em_ds" showOnLoad="false" layout="tabLayout" region="center"
           showCounts="false" multipleSelectionEnabled="true">
        <title translatable="true">Employees On Team</title>
        <field table="team" name="em_id" dataType="text">
            <title>Employee Name</title>
        </field>

        <field name="location" dataType="text"  baseField="rm.bl_id">
            <title>Location</title>
        </field>
        <field name="organization" >
            <title>Organization</title>
        </field>
        <field table="team" name="date_start">
            <title>Start Date</title>
        </field>
        <field table="team" name="date_end">
            <title>End Date</title>
        </field>
        <field  name="teams" dataType="text" controlType="link">
            <title>Teams</title>
            <command type="callFunction" functionName="employeeOnTeamController.getTeamList('emPanel')"/>
        </field>
        <field table="em" name="em_std" hidden="true">
            <title>Employee Standard</title>
        </field>
        <field table="em" name="status" hidden="true">
            <title>Employee Status</title>
        </field>
        <field table="team" name="pct_time" hidden="true">
            <title>Percentage of Time</title>
        </field>
        <field table="team" name="autonumbered_id" hidden="true"/>
        <field controlType="button" id="remove">
            <title translatable="true">Remove</title>
        </field>
        <action type="menu" id="employeesOnTeamToolsMenu" imageName="/schema/ab-core/graphics/icons/view/gear.png">
            <title></title>
            <action id="selectEmployeeFields">
                <title>Select Fields</title>
                <command type="selectFields" panelId="emPanel"/>
            </action>
        </action>
    </panel>

    <panel type="grid" id="emAvailablePanel" dataSource="available_em_ds" recordLimit="100" showOnLoad="false" layout="tabLayout" region="south"
           showCounts="false" multipleSelectionEnabled="true">
        <indexField table="em" name="em_id"/>
        <title translatable="true">Available Employees</title>

        <field table="em" name="em_id" dataType="text">
            <title>Employee Name</title>
        </field>

        <field name="location" dataType="text"  baseField="rm.bl_id">
            <title>Location</title>
        </field>
        <field name="organization" >
            <title>Organization</title>
        </field>
        <field  name="teams" dataType="text" controlType="link">
            <title>Teams</title>
            <command type="callFunction" functionName="employeeOnTeamController.getTeamList('emAvailablePanel')"/>
        </field>
        <field table="em" name="em_std" hidden="true">
            <title>Employee Standard</title>
        </field>
        <field table="em" name="status" hidden="true">
            <title>Employee Status</title>
        </field>

        <field id="add" controlType="button">
            <title translatable="true">Add</title>
        </field>
        <action type="menu" id="availableEmployeesToolsMenu" imageName="/schema/ab-core/graphics/icons/view/gear.png">
            <title></title>
            <action id="selectEmployeeFields">
                <title>Select Fields</title>
                <command type="selectFields" panelId="emAvailablePanel"/>
            </action>
        </action>
        <action id="addSelected" cssClass="fieldButtonRight">
            <title>Add Selected</title>
        </action>
    </panel>

    <panel type="form" id="editMembershipDatePanel" dataSource="em_ds" layout="tabLayout" region="east" showOnLoad="false">

        <action id="save" >
            <title translatable="true">Save</title>
            <command type="callFunction" functionName="employeeOnTeamController.onSaveSelectedEmployees()"/>
        </action>
        <action id="remove">
            <title translatable="true">Remove</title>
            <command type="callFunction" functionName="employeeOnTeamController.onRemoveSelectedEmployees()"/>
        </action>
        <action id="cancel">
            <title translatable="true">Cancel</title>
            <command type="showPanel" panelId="editMembershipDatePanel" show="false"/>
        </action>

        <field table="em" name="em_id" readOnly="true" cssClass="shortField">
            <title>Employee Name</title>
        </field>
        <field name="organization" readOnly="true" cssClass="shortField">
            <title>Organization</title>
        </field>
        <field table="team" name="pct_time" cssClass="shortField">
            <title>Percentage of Time</title>
        </field>
        <field table="team" name="date_start" alias="edit_form.team.date_start" required="true" cssClass="shortField">
            <title>Start Date</title>
        </field>
        <field table="team" name="date_end" alias="edit_form.team.date_end" cssClass="shortField">
            <title>End Date</title>
        </field>
    </panel>
</view>