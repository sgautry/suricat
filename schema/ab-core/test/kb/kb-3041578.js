
View.createController('kb3041578', {

    afterViewLoad: function() {
    	
	    this.employeeGrid.actionbar.actions.each(function (action) {
	        action.show(true);
	    });
    },

    employeeGrid_onWaiting: function() {
        View.alert('Clicked on the Waiting button');
    }
});
	