View.createController('Test', {
	afterViewLoad: function() {
	    var ds = View.dataSources.get('commonDS');
	    if (ds) {
	        View.alert("AXVW and JS files have been included.");
	    } else {
	    	View.alert("JS file has been included, AXVW file has not.");
	    }
    }
});