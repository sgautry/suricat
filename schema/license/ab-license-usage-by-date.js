var concurentController = View.createController('concurentController', {
    eq_consolePanel_onRestrict:function () {
        var console = this.eq_consolePanel;
        var login_date_from = console.getFieldValue('login_date_from');
        var login_date_to = console.getFieldValue('login_date_to');
        var restriction = new AFM.view.Restriction();

        if (valueExistsNotEmpty(login_date_from)) {
            restriction.addClause('afm_concurent_users.login_date', login_date_from, '&gt;=');
        }
        if (valueExistsNotEmpty(login_date_to)) {
            restriction.addClause('afm_concurent_users.login_date', login_date_to, '&lt;=');
        }
        this.group_concurent_panel.refresh(restriction);
    }
});

function setDrilldownRestriction(obj) {
    var grid = View.getControl('', 'abUsage_popupPanel');
    var restriction = new AFM.view.Restriction();
    for (var j = 0; j < obj.restriction.clauses.length; j++) {
        restriction.addClause('afm_concurent_users.login_date', obj.restriction.clauses[j].value, '=');
    }
    grid.refresh(restriction);
}  