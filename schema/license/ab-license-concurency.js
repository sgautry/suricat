var reportController = View.createController('reportController', {
    eq_consolePanel_onRestrict:function () {
        var console = this.eq_consolePanel;
        var login_date_from = console.getFieldValue('login_date_from');
        var login_date_to = console.getFieldValue('login_date_to');
        /*
         var restriction = new AFM.view.Restriction();
         if (valueExistsNotEmpty(login_date_from)) {
         restriction.addClause('afm_concurent_users.login_date', login_date_from, '&gt;=');
         }
         if (valueExistsNotEmpty(login_date_to)) {
         restriction.addClause('afm_concurent_users.login_date', login_date_to, '&lt;=');
         }
         */
        var restriction = " 1 = 1 ";

        if (valueExistsNotEmpty(login_date_from)) {
            restriction += "and TO_DATE(afm_concurent_users.login_day, 'dd-MM-yyyy hh24:mi') &gt;= TO_DATE('" + login_date_from + " 00:00', 'YYYY-MM-DD hh24:mi') ";
        }
        if (valueExistsNotEmpty(login_date_to)) {
            restriction += "and TO_DATE(afm_concurent_users.login_day, 'dd-MM-yyyy hh24:mi') &lt;= TO_DATE('" + login_date_to + " 23:59', 'YYYY-MM-DD hh24:mi') ";
        }

        this.abConcurent_detailsPanel.refresh(restriction);
    }
});

function setDrilldownRestriction(obj) {
    var grid = View.getControl('', 'abConcurent_popupPanel');
    grid.addParameter('login_day', obj.selectedChartData['afm_concurent_users.login_day']);
    grid.refresh();
}  