package com.archibus.app.rplm.datachangeevent;

import org.springframework.context.ApplicationEvent;

import com.archibus.core.event.data.IDataEventListener;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * !!!!Added here to avoid startup error - final class will be added with FASB
 * patch. WFR is not active.
 *
 * REPM - Data change event listener. Is configured to be notified by the core
 * when there is a DataEvent (only RecordChangedEvent).
 * <p>
 *
 *
 *
 * @author Ioan Draghici
 * @since 23.1
 *
 */
public class DataChangeEventListener implements IDataEventListener {

	@Override
	public void onApplicationEvent(final ApplicationEvent event) {
		// TODO Auto-generated method stub

	}

}
