package com.archibus.app.rplm.fasb;

import com.archibus.jobmanager.JobBase;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * !!!!Added here to avoid startup error - final class will be added with FASB
 * patch. WFR is not active.
 *
 * @author Ioan Draghici
 * @since 23.1
 *
 */
public class LeaseRecognitionService extends JobBase {

}
