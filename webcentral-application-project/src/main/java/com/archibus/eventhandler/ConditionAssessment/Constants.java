package com.archibus.eventhandler.ConditionAssessment;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Constants for Condition Assessment.
 * <p>
 *
 *
 * @author Ioan Draghici
 * @since 23.1
 *
 */
public class Constants {

	/**
	 * Constant.
	 */
	public static final String RECORDS_FOR_RM_AND_EQ = "RmEq";

	/**
	 * Constant.
	 */
	public static final String RECORDS_FOR_RM = "Rm";

	/**
	 * Constant.
	 */
	public static final String RECORDS_FOR_EQ = "Eq";

	/**
	 * Constant.
	 */
	public static final String COMMA = ",";

	/**
	 * Constant.
	 */
	public static final String DOT = ".";

	/**
	 * Constant.
	 */
	public static final String AND = " AND ";

	/**
	 * Constant.
	 */
	public static final String INVOICE_TABLE = "invoice";

	/**
	 * Constant.
	 */
	public static final String EQUIPMENT_TABLE = "eq";

	/**
	 * Constant.
	 */
	public static final String BUILDING_TABLE = "bl";

	/**
	 * Constant.
	 */
	public static final String ROOM_TABLE = "rm";

	/**
	 * Constant.
	 */
	public static final String ACTIVITY_LOG_TABLE = "activity_log";

	/**
	 * Constant.
	 */
	public static final String CONTACT_TABLE = "contact";

	/**
	 * Constant.
	 */
	public static final String EMPLOYEE_TABLE = "em";

	/**
	 * Constant.
	 */
	public static final String VEDNOR_TABLE = "vn";

	/**
	 * Constant.
	 */
	public static final String ACTIVITY_LOG_ID_FIELD = "activity_log_id";

	/**
	 * Constant.
	 */
	public static final String NUMBER_OF_RECORDS_FIELD = "number_of_records";

	/**
	 * Constant.
	 */
	public static final String DOC_FIELD = "doc";

	/**
	 * Constant.
	 */
	public static final String FIELD_TYPE_DATE = "date";

	/**
	 * Constant.
	 */
	public static final String FIELD_TYPE_TEXT = "text";

	/**
	 * Hidden constructor.
	 */
	protected Constants() {
		// prevents calls from subclass
		throw new UnsupportedOperationException();
	}
}
