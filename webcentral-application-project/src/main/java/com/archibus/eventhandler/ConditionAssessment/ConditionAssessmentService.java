package com.archibus.eventhandler.ConditionAssessment;

import static com.archibus.eventhandler.ConditionAssessment.Constants.*;

import java.util.*;

import com.archibus.context.ContextStore;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;
import com.archibus.jobmanager.JobBase;
import com.archibus.service.DocumentService;
import com.archibus.service.DocumentService.DocumentParameters;
import com.archibus.utility.StringUtil;

/**
 *
 * Provides methods to generate/ update condition assessment records.
 * <p>
 *
 *
 * @author Ioan Draghici
 * @since 18.2
 *
 */
public class ConditionAssessmentService extends JobBase {

	/**
	 * Generates assessment records for user selection. Calculate number of
	 * records and set this as total number for job status.
	 *
	 * @param projectId
	 *            project code
	 * @param siteId
	 *            site code
	 * @param buildingId
	 *            building code
	 * @param floorId
	 *            floor code
	 * @param activityType
	 *            activity type
	 * @param recordsFor
	 *            specify for what to generate records (RmEq, Rm, Eq )
	 */
    public void generateAssessmentRecords(final String projectId, final String siteId,
            final String buildingId, final String floorId, final String activityType,
            final String recordsFor) {
		createAssessmentRecords(projectId, siteId, buildingId, floorId, activityType, recordsFor, null);
    }

	/**
	 * Generate assessment record for user selection and custodian equipments.
	 *
	 * @param projectId
	 *            project code
	 * @param siteId
	 *            site code
	 * @param buildingId
	 *            building code
	 * @param floorId
	 *            floor code
	 * @param activityType
	 *            activity type
	 * @param recordsFor
	 *            specify for what to generate records (RmEq, Rm, Eq )
	 * @param custodian
	 *            selected custodian data record
	 */
	public void generateAssessmentRecordsForCustodian(final String projectId, final String siteId,
			final String buildingId, final String floorId, final String activityType, final String recordsFor,
			final DataRecord custodian) {
		createAssessmentRecords(projectId, siteId, buildingId, floorId, activityType, recordsFor, custodian);
	}

    /**
	 * Assign items to assessor.
	 *
	 * @param itemIds
	 *            list of selected items ids
	 * @param assessorId
	 *            assessor id
	 *
	 *            <p>
	 *            Suppress PMD warning "AvoidUsingSql" in this method.
	 *            <p>
	 *            Justification: CAse 2.2. Statements with UPDATE ... WHERE
	 *            pattern.
	 *
	 */
	@SuppressWarnings("PMD.AvoidUsingSql")
	public void assignItemsToAssessor(final List<Integer> itemIds, final String assessorId) {
        final String selectedIds = listToString(itemIds);
		final String preparedStatement = String.format(
				"UPDATE activity_log SET assessed_by = %s WHERE activity_log_id IN (%s)",
				SqlUtils.formatValueForSql(assessorId),
				selectedIds);
		SqlUtils.executeUpdate(INVOICE_TABLE, preparedStatement);
    }

    /**
	 * Update fields for selected assessment items.
	 *
	 * @param itemIds
	 *            list of selected item id's
	 * @param fields
	 *            fields to be updated (cond_priority, cond_value, rec_action,
	 *            status, date_scheduled)
	 * @param values
	 *            values to be updated
	 * @param types
	 *            types of the fields ("", "text" or "date")
	 *
	 *            <p>
	 *            Suppress PMD warning "AvoidUsingSql" in this method.
	 *            <p>
	 *            Justification: CAse 2.2. Statements with UPDATE ... WHERE
	 *            pattern.
	 *
	 */
	@SuppressWarnings("PMD.AvoidUsingSql")
	public void updateAssessmentItems(final List<Integer> itemIds, final List<String> fields, final List<String> values,
            final List<String> types) {
		final String selectedIds = listToString(itemIds);

        String updateFields = "";

		for (int i = 0; i < fields.size(); i++) {
        	final String field = fields.get(i);
        	final String value = values.get(i);
        	final String type = types.get(i);
        	String fieldStatement = "";
        	if (FIELD_TYPE_TEXT.equals(type)) {
        		fieldStatement = String.format("%s = %s", field, SqlUtils.formatValueForSql(value));
        	} else if (FIELD_TYPE_DATE.equals(type)) {
        		fieldStatement = String.format("%s = ${sql.date(%s)}", field, SqlUtils.formatValueForSql(value));
        	} else {
				fieldStatement = String.format("%s = %s ", field, value);
        	}
			updateFields += (i == 0 ? "" : COMMA) + fieldStatement;
        }

		final String preparedStatement = String.format("UPDATE activity_log SET %s WHERE activity_log_id IN (%s)",
				updateFields, selectedIds);
		SqlUtils.executeUpdate(ACTIVITY_LOG_TABLE, preparedStatement);
    }

    /**
	 * Assign documentation to selected item id's.
	 *
	 * @param itemIds
	 *            list of selected item id's
	 * @param docActionId
	 *            activity_log_id of document to assign
	 * @param activityId
	 *            activity id
	 */
	public void assignDocumentation(final List<Integer> itemIds, final String docActionId,
            final String activityId) {
		final String selectedIds = listToString(itemIds);
		final DataSource dsActivityLog = DataSourceFactory.createDataSourceForFields(ACTIVITY_LOG_TABLE,
				new String[] { ACTIVITY_LOG_ID_FIELD, DOC_FIELD, "doc1", "doc2", "doc3", "doc4", "description" });

		dsActivityLog.addRestriction(Restrictions.in(ACTIVITY_LOG_TABLE, ACTIVITY_LOG_ID_FIELD, selectedIds));
		final List<DataRecord> selectedRecords = dsActivityLog.getRecords();

		dsActivityLog.clearRestrictions();
		dsActivityLog.addRestriction(Restrictions.eq(ACTIVITY_LOG_TABLE, ACTIVITY_LOG_ID_FIELD, docActionId));
		final DataRecord srcDocRecord = dsActivityLog.getRecord();

        int totalNum = selectedRecords.size();

        for (final DataRecord selectedRecord : selectedRecords) {
            final String targetDocField = getFirstEmptyDocField(selectedRecord);
            if (targetDocField == null) {
                totalNum = 0;
            }
        }

        this.status.setTotalNumber(totalNum);

		if (totalNum > 0) {

			final DocumentService documentService =
                    (DocumentService) ContextStore.get().getBean("documentService");
			this.status.setCurrentNumber(0);
            for (final DataRecord selectedRecord : selectedRecords) {

				if (this.status.isStopRequested()) {
					break;
				}

                final String targetDocField = getFirstEmptyDocField(selectedRecord);
                if (targetDocField == null) {
                    continue;
                }

                final String fileName = srcDocRecord.getString("activity_log.doc");
                final String fileDescription = srcDocRecord.getString("activity_log.description");
				final String sourceDocId =
						String.valueOf(srcDocRecord.getInt(ACTIVITY_LOG_TABLE + DOT + ACTIVITY_LOG_ID_FIELD));
                final String targetDocId =
						String.valueOf(selectedRecord.getInt(ACTIVITY_LOG_TABLE + DOT + ACTIVITY_LOG_ID_FIELD));

				final Map<String, String> srcKeys = new HashMap<String, String>();
				srcKeys.put(ACTIVITY_LOG_TABLE, sourceDocId);
				final DocumentParameters sourceDocumentParameters =
						new DocumentParameters(srcKeys, ACTIVITY_LOG_TABLE, DOC_FIELD, null, true);
				final Map<String, String> targetKeys = new HashMap<String, String>();
				targetKeys.put(ACTIVITY_LOG_ID_FIELD, targetDocId);
                final DocumentParameters targetDocParam =
						new DocumentParameters(targetKeys, ACTIVITY_LOG_TABLE, targetDocField, fileName,
                            fileDescription, "0");

				documentService.copyDocument(sourceDocumentParameters, targetDocParam);

				this.status.setCurrentNumber(this.status.getCurrentNumber() + 1);
            }

            this.status.setCurrentNumber(totalNum);
		}

    }

    /**
	 * Return field name of first empty doc field for activity log item.
	 *
	 * @param record
	 *            Data record
	 *
	 * @return String name of field (doc, doc1, doc2, doc3, doc4)
	 */
    private String getFirstEmptyDocField(final DataRecord record) {
		String docField = null;
        String docValue = "";
        final String[] count = { "", "1", "2", "3", "4" };
        for (final String element : count) {
			docField = DOC_FIELD + element;
            docValue = record.getString("activity_log." + docField);
            if (docValue == null) {
				break;
            }
        }
		return docField;
    }

    /**
	 * Calculate number of record that are going to be generated.
	 *
	 * @param siteId
	 *            site code
	 * @param buildingId
	 *            building code
	 * @param floorId
	 *            floor code
	 * @param recordsFor
	 *            generate records for
	 * @param custodian
	 *            custodian record
	 * @return integer
	 *
	 *         <p>
	 *         Suppress PMD warning "AvoidUsingSql" in this method.
	 *         <p>
	 *         Justification: select count() pattern
	 *
	 *         TODO Change count sql statement with formula field
	 */
	@SuppressWarnings("PMD.AvoidUsingSql")
    private int getNumberOfRecordsToGenerate(final String siteId, final String buildingId,
			final String floorId, final String recordsFor, final DataRecord custodian) {
        int recForRoom = 0;
        int recForEq = 0;
        if (recordsFor.equals(RECORDS_FOR_RM) || recordsFor.equals(RECORDS_FOR_RM_AND_EQ)) {
			String restriction = getSqlRestriction(siteId, buildingId, floorId, BUILDING_TABLE);
			if (!custodian.isNew()) {
				restriction += AND + getCustodianRestrictionForRoom(custodian);
			}
			final DataSource dsRoom = DataSourceFactory.createDataSource();
			dsRoom.addTable(ROOM_TABLE, DataSource.ROLE_MAIN);
			dsRoom.addTable(BUILDING_TABLE, DataSource.ROLE_STANDARD);
			dsRoom.addVirtualField(ROOM_TABLE, NUMBER_OF_RECORDS_FIELD, DataSource.DATA_TYPE_INTEGER);
			final String sqlStatement = String.format(
					"SELECT COUNT(*) ${sql.as} %s FROM rm LEFT OUTER JOIN bl ON rm.bl_id = bl.bl_id WHERE %s",
					NUMBER_OF_RECORDS_FIELD,
					restriction);
			dsRoom.addQuery(sqlStatement);
			dsRoom.setApplyVpaRestrictions(false);
			final DataRecord record = dsRoom.getRecord();
			recForRoom = record.getInt(ROOM_TABLE + DOT + NUMBER_OF_RECORDS_FIELD);
        }
        if (recordsFor.equals(RECORDS_FOR_EQ) || recordsFor.equals(RECORDS_FOR_RM_AND_EQ)) {
			String restriction = getSqlRestriction(siteId, buildingId, floorId, EQUIPMENT_TABLE);
			if (!custodian.isNew()) {
				restriction += AND + getCustodianRestrictionForEquipment(custodian);
			}
			final DataSource dsEquipment = DataSourceFactory.createDataSource();
			dsEquipment.addTable(EQUIPMENT_TABLE, DataSource.ROLE_MAIN);
			dsEquipment.addVirtualField(EQUIPMENT_TABLE, NUMBER_OF_RECORDS_FIELD, DataSource.DATA_TYPE_INTEGER);
			final String sqlStatement = String.format(
					"SELECT COUNT(*) ${sql.as} %s FROM eq LEFT OUTER JOIN bl ON eq.bl_id = bl.bl_id WHERE %s",
					NUMBER_OF_RECORDS_FIELD, restriction);
			dsEquipment.addQuery(sqlStatement);
			dsEquipment.setApplyVpaRestrictions(false);
			final DataRecord record = dsEquipment.getRecord();
			recForEq = record.getInt(EQUIPMENT_TABLE + DOT + NUMBER_OF_RECORDS_FIELD);
        }
		return recForRoom + recForEq;
    }

    /**
	 * Generate assessment records for rooms.
	 *
	 * @param projectId
	 *            project code
	 * @param siteId
	 *            site code
	 * @param buildingId
	 *            building code
	 * @param floorId
	 *            floor code
	 * @param activityType
	 *            activity type
	 * @param custodian
	 *            custodian record
	 *
	 *            <p>
	 *            Suppress PMD warning "AvoidUsingSql" in this method.
	 *            <p>
	 *            Justification: 2.1. Statements with INSERT ... SELECT pattern.
	 *
	 */
	@SuppressWarnings("PMD.AvoidUsingSql")
    private void generateRecordForRoom(final String projectId, final String siteId,
			final String buildingId, final String floorId, final String activityType, final DataRecord custodian) {
		String restriction = getSqlRestriction(siteId, buildingId, floorId, "bl");
		if (!custodian.isNew()) {
			restriction += AND + getCustodianRestrictionForRoom(custodian);
		}
		final String sqlStatement = "INSERT INTO activity_log (activity_type, project_id, site_id, bl_id, fl_id, rm_id) "
				+ "SELECT %s, %s, bl.site_id, rm.bl_id, rm.fl_id, rm.rm_id "
				+ "FROM rm, bl WHERE rm.bl_id = bl.bl_id AND %s";
		final String preparedStatement = String.format(sqlStatement, SqlUtils.formatValueForSql(activityType),
				SqlUtils.formatValueForSql(projectId), restriction);
		SqlUtils.executeUpdate(ACTIVITY_LOG_TABLE, preparedStatement);
    }

    /**
	 * Generate assessment records for equipments.
	 *
	 * @param projectId
	 *            project code
	 * @param siteId
	 *            site code
	 * @param buildingId
	 *            building code
	 * @param floorId
	 *            floor code
	 * @param activityType
	 *            activity type
	 * @param custodian
	 *            custodian record
	 *
	 *            <p>
	 *            Suppress PMD warning "AvoidUsingSql" in this method.
	 *            <p>
	 *            Justification: 2.1. Statements with INSERT ... SELECT pattern.
	 *
	 */
	@SuppressWarnings("PMD.AvoidUsingSql")
    private void generateRecordForEquipment(final String projectId, final String siteId,
			final String buildingId, final String floorId, final String activityType, final DataRecord custodian) {
		String restriction = getSqlRestriction(siteId, buildingId, floorId, "eq");
		if (!custodian.isNew()) {
			restriction += AND + getCustodianRestrictionForEquipment(custodian);
		}
        final String sqlStatement =
				"INSERT INTO activity_log (activity_type, project_id, eq_id, site_id, bl_id, fl_id, rm_id, csi_id, questionnaire_id) "
						+ "SELECT %s, %s, eq_id"
                        + ", (CASE WHEN (eq.site_id IS NOT NULL OR eq.bl_id IS NULL) THEN eq.site_id ELSE bl.site_id END)"
                        + ", eq.bl_id, fl_id, rm_id, csi_id, "
                        + " (SELECT questionnaire_map.questionnaire_id "
                        + "     FROM questionnaire_map WHERE questionnaire_map.eq_std = eq.eq_std "
						+ "         AND questionnaire_map.project_type = (SELECT project.project_type FROM project WHERE project.project_id = %s))"
						+ " FROM eq LEFT OUTER JOIN bl ON eq.bl_id = bl.bl_id WHERE %s ";
		final String preparedStatement = String.format(sqlStatement, SqlUtils.formatValueForSql(activityType),
				SqlUtils.formatValueForSql(projectId), SqlUtils.formatValueForSql(projectId), restriction);
		SqlUtils.executeUpdate(ACTIVITY_LOG_TABLE, preparedStatement);
    }

    /**
	 * Create sql restriction.
	 *
	 * @param siteId
	 *            site code
	 * @param buildingId
	 *            building code
	 * @param floorId
	 *            floor code
	 * @param table
	 *            table name
	 * @return String
	 */
	private String getSqlRestriction(final String siteId, final String buildingId,
            final String floorId, final String table) {
		String result = " 1 = 1 ";
		if (StringUtil.notNullOrEmpty(siteId)) {
			final String formattedSiteId = SqlUtils.formatValueForSql(siteId);
			result += String.format("AND (%s.site_id = %s OR bl.site_id = %s ) ", table, formattedSiteId,
					formattedSiteId);
        }
		if (StringUtil.notNullOrEmpty(buildingId)) {
			result += String.format("AND %s.bl_id = %s ", table, SqlUtils.formatValueForSql(buildingId));
        }
		if (StringUtil.notNullOrEmpty(floorId)) {
			if (BUILDING_TABLE.equals(table)) {
				result += String.format("AND  %s.fl_id = %s ", ROOM_TABLE, SqlUtils.formatValueForSql(floorId));
			} else {
				result += String.format("AND %s.fl_id = %s ", table, SqlUtils.formatValueForSql(floorId));
			}
        }
		return result;
    }

	/**
	 * Returns custodian restriction for equipment.
	 *
	 * @param record
	 *            custodian data record
	 * @return String
	 *
	 *         <p>
	 *         Suppress PMD warning "AvoidUsingSql" in this method.
	 *         <p>
	 *         Justification: EXISTS() statement used to restriction data
	 *
	 */
	@SuppressWarnings("PMD.AvoidUsingSql")
	private String getCustodianRestrictionForEquipment(final DataRecord record) {
		final String source = record.getString("team.source_table");
		final String status = record.getString("team.status");
		final String teamType = record.getString("team.team_type");
		final String custodianType = record.getString("team.custodian_type");
		final String custodianNameField = getCustodianNameField(source);
		final String custodianName = record.getString("team." + custodianNameField);

		final String sqlRestriction = String.format(
				"EXISTS(SELECT team.eq_id FROM team WHERE team.eq_id IS NOT NULL AND team.custodian_type = %s "
						+ "AND team.eq_id = eq.eq_id AND team.status = %s AND team.team_type = %s AND team.%s = %s)",
				SqlUtils.formatValueForSql(custodianType),
				SqlUtils.formatValueForSql(status),
				SqlUtils.formatValueForSql(teamType), custodianNameField, SqlUtils.formatValueForSql(custodianName));

		return sqlRestriction;
	}

	/**
	 * Returns custodian restriction for room.
	 *
	 * @param record
	 *            custodian data record
	 * @return String
	 *
	 *         <p>
	 *         Suppress PMD warning "AvoidUsingSql" in this method.
	 *         <p>
	 *         Justification: EXISTS() statement used to restriction data
	 *
	 */
	@SuppressWarnings("PMD.AvoidUsingSql")
	private String getCustodianRestrictionForRoom(final DataRecord record) {
		final String eqRestriction = getCustodianRestrictionForEquipment(record);
		final String sqlRestriction = String.format(
				"EXISTS(SELECT eq.eq_id FROM eq WHERE eq.rm_id = rm.rm_id AND eq.fl_id = rm.fl_id AND eq.bl_id = rm.bl_id AND %s )",
				eqRestriction);
		return sqlRestriction;
	}

	/**
	 * Get custodian name field name.
	 *
	 * @param source
	 *            source table
	 * @return String
	 */
	private String getCustodianNameField(final String source) {
		String result = "";
		if (VEDNOR_TABLE.equals(source)) {
			result = "vn_id";
		} else if (CONTACT_TABLE.equals(source)) {
			result = "contact_id";
		} else if (EMPLOYEE_TABLE.equals(source)) {
			result = "em_id";
		}
		return result;
	}

	/**
	 * Convert list to comma separated string.
	 *
	 * @param itemIds
	 *            list with selected id's
	 * @return String
	 */
	private String listToString(final List<Integer> itemIds) {
		String result = "";
		for (int i = 0; i < itemIds.size(); i++) {
			result += (result.length() == 0 ? "" : COMMA) + itemIds.get(i);
		}
		return result;
	}

	/**
	 * Create assessment record.
	 *
	 *
	 * @param projectId
	 *            project code
	 * @param siteId
	 *            site code
	 * @param buildingId
	 *            building code
	 * @param floorId
	 *            floor code
	 * @param activityType
	 *            activity type
	 * @param recordsFor
	 *            create records for
	 * @param custodian
	 *            custodian record
	 */
	private void createAssessmentRecords(final String projectId, final String siteId, final String buildingId,
			final String floorId, final String activityType, final String recordsFor, final DataRecord custodian) {
		// total number of records to generate
		final int totalNum = getNumberOfRecordsToGenerate(siteId, buildingId, floorId, recordsFor, custodian);
		this.status.setTotalNumber(totalNum);
		this.status.setCurrentNumber(0);
		if (totalNum > 0) {
			if (!this.status.isStopRequested()
					&& (recordsFor.equals(RECORDS_FOR_RM_AND_EQ) || recordsFor.equals(RECORDS_FOR_RM))) {
				generateRecordForRoom(projectId, siteId, buildingId, floorId, activityType, custodian);
				this.status.setCurrentNumber(this.status.getCurrentNumber() + totalNum / 2);
			}

			if (!this.status.isStopRequested()
					&& (recordsFor.equals(RECORDS_FOR_RM_AND_EQ) || recordsFor.equals(RECORDS_FOR_EQ))) {
				generateRecordForEquipment(projectId, siteId, buildingId, floorId, activityType, custodian);
				this.status.setCurrentNumber(this.status.getCurrentNumber() + totalNum / 2);
			}
		}
		this.status.setCurrentNumber(totalNum);
	}
}
