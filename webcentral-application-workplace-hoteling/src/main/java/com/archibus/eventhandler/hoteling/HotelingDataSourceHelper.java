package com.archibus.eventhandler.hoteling;

import com.archibus.datasource.*;
import com.archibus.datasource.restriction.Restrictions;

/**
 * Helper Class for Hoteling activity that provide datasource related helper code.<br>
 * <p>
 *
 * @author Guo
 * @since 23.2
 */
public final class HotelingDataSourceHelper {

    /**
     * Constructor method for removing warning: 'Utility classes should not have a public or default
     * constructor'.
     *
     */
    private HotelingDataSourceHelper() {
    }

    /**
     * Get RMPCT table DataSource.
     *
     * @return DataSource Object
     */
    public static DataSource getRmpctDataSource() {
        return DataSourceFactory.createDataSourceForFields(HotelingConstants.RMPCT,
            new String[] { HotelingConstants.ACTIVITY_LOG_ID, HotelingConstants.PCT_ID,
                    HotelingConstants.PARENT_PCT_ID, HotelingConstants.DATE_START,
                    HotelingConstants.DATE_END, HotelingConstants.DAY_PART, HotelingConstants.BL_ID,
                    HotelingConstants.FL_ID, HotelingConstants.RM_ID, HotelingConstants.EM_ID,
                    HotelingConstants.RESOURCES, HotelingConstants.VISITOR_ID,
                    HotelingConstants.STATUS, HotelingConstants.DV_ID, HotelingConstants.DP_ID,
                    HotelingConstants.AC_ID, HotelingConstants.RM_CAT, HotelingConstants.RM_TYPE,
                    HotelingConstants.PCT_TIME, HotelingConstants.PCT_SPACE,
                    HotelingConstants.PRORATE, HotelingConstants.PRIMARY_RM,
                    HotelingConstants.PRIMARY_EM, HotelingConstants.CONFIRMED });
    }

    /**
     * Get RM table DataSource.
     *
     * @return DataSource Object
     */
    public static DataSource getRmDataSource() {
        return DataSourceFactory.createDataSourceForFields(HotelingConstants.T_RM,
            new String[] { HotelingConstants.BL_ID, HotelingConstants.FL_ID,
                    HotelingConstants.RM_ID, HotelingConstants.DV_ID, HotelingConstants.DP_ID,
                    HotelingConstants.RM_CAT, HotelingConstants.RM_TYPE, HotelingConstants.CAP_EM,
                    HotelingConstants.PRORATE });
    }

    /**
     * Get ACTIVITY_LOG table DataSource.
     *
     * @return DataSource Object
     */
    public static DataSource getActivityLogDataSource() {
        return DataSourceFactory.createDataSourceForFields(HotelingConstants.ACTIVITY_LOG,
            new String[] { HotelingConstants.ACTIVITY_LOG_ID, HotelingConstants.ACTIVITY_TYPE,
                    HotelingConstants.DATE_REQUESTED, HotelingConstants.TIME_REQUESTED,
                    HotelingConstants.STATUS, HotelingConstants.REQUESTER,
                    HotelingConstants.RECURRING_RULE, HotelingConstants.DATE_APPROVED });
    }

    /**
     * Get visitor table DataSource.
     *
     * @return DataSource Object
     */
    public static DataSource getVisitorDataSource() {

        return DataSourceFactory.createDataSourceForFields(HotelingConstants.VISITORS,
            new String[] { HotelingConstants.VISITOR_ID, HotelingConstants.NAME_FIRST,
                    HotelingConstants.NAME_LAST });
    }

    /**
     * Get RMPCT table DataSource.
     *
     * @return DataSource Object
     */
    public static DataSource getRmpctJoinActivityLogDataSource() {
        final DataSource dataSource = DataSourceFactory.createDataSource();
        dataSource.addTable(HotelingConstants.RMPCT, DataSource.ROLE_MAIN);
        dataSource.addTable(HotelingConstants.ACTIVITY_LOG, DataSource.ROLE_STANDARD);
        dataSource.addField(HotelingConstants.RMPCT,
            new String[] { HotelingConstants.ACTIVITY_LOG_ID, HotelingConstants.PCT_ID,
                    HotelingConstants.PARENT_PCT_ID, HotelingConstants.DATE_START,
                    HotelingConstants.DATE_END, HotelingConstants.DAY_PART, HotelingConstants.BL_ID,
                    HotelingConstants.FL_ID, HotelingConstants.RM_ID, HotelingConstants.EM_ID,
                    HotelingConstants.RESOURCES, HotelingConstants.VISITOR_ID,
                    HotelingConstants.STATUS, HotelingConstants.DV_ID, HotelingConstants.DP_ID,
                    HotelingConstants.AC_ID, HotelingConstants.RM_CAT, HotelingConstants.RM_TYPE,
                    HotelingConstants.PCT_TIME, HotelingConstants.PCT_SPACE,
                    HotelingConstants.PRORATE, HotelingConstants.PRIMARY_RM,
                    HotelingConstants.PRIMARY_EM });
        dataSource.addField(HotelingConstants.ACTIVITY_LOG,
            new String[] { HotelingConstants.RECURRING_RULE });

        return dataSource;
    }

    /**
     * prepare the SelectDataSource for cancel, approve ,reject.
     *
     * @param operationLevel Operation Level
     * @return DataSource
     */
    public static DataSource getDatasourceByOperationLevel(final String operationLevel) {
        final DataSource selectRecordDs = getRmpctJoinActivityLogDataSource();

        String pctOrParentField = "";
        if ("0".equals(operationLevel)) {
            pctOrParentField = HotelingConstants.PARENT_PCT_ID;
        }
        if ("1".equals(operationLevel)) {
            pctOrParentField = HotelingConstants.PCT_ID;
        }
        selectRecordDs
            .addRestriction(Restrictions.sql(pctOrParentField + " =${parameters['pct_id']} "))
            .addParameter(HotelingConstants.PCT_ID, "", DataSource.DATA_TYPE_INTEGER)
            .addSort(HotelingConstants.RMPCT, HotelingConstants.DATE_START);

        return selectRecordDs;
    }

}
