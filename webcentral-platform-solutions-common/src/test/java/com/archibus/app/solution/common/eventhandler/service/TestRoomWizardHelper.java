package com.archibus.app.solution.common.eventhandler.service;

import org.dom4j.Element;
import org.junit.experimental.categories.Category;

import com.archibus.core.test.fixture.category.FastTest;
import com.archibus.utility.ExceptionBase;

import junit.framework.TestCase;

/**
 * @author Valery
 * @created October 31, 2006
 */
@Category({ FastTest.class })
public class TestRoomWizardHelper extends TestCase {

	/**
	 * A unit test for JUnit
	 *
	 * @exception ExceptionBase
	 *                Description of the Exception
	 */
	public void testPrepareXmlResponseBegin() throws ExceptionBase {
		final Element result = RoomWizardHelper.prepareXmlResponseBegin();
		verifyResultNodeBegin(result);

		System.out.println("prepareXmlResponseBegin");
		System.out.println(result.asXML());
	}

	/**
	 * A unit test for JUnit
	 */
	public void testPrepareXmlResponseFinish() {
		final String kweResultCode = "0";
		final String rbResultCode = "0";

		final Element result = RoomWizardHelper.prepareXmlResponseBegin();
		verifyResultNodeBegin(result);

		RoomWizardHelper.prepareXmlResponseFinish(kweResultCode, rbResultCode, result);
		verifyResultNodeFinish(result);

		System.out.println("prepareXmlResponseFinish");
		System.out.println(result.asXML());
	}

	/**
	 * A unit test for JUnit
	 */
	public void testAddBookingNode() {
		final String kweResultCode = "0";
		final String rbResultCode = "0";

		final Element result = RoomWizardHelper.prepareXmlResponseBegin();
		RoomWizardHelper.prepareXmlResponseFinish(kweResultCode, rbResultCode, result);

		final Booking bookingParameters = new Booking();
		bookingParameters.start_date = "20011231";
		bookingParameters.start_time = "235846";
		bookingParameters.end_date = "20011231";
		bookingParameters.end_time = "235846";
		bookingParameters.booking_id = "235846";
		bookingParameters.isPrivate = "no";
		bookingParameters.password_protected = "no";

		final String roomId = "101";

		final Element bookings = result.addElement("rb:bookings").addAttribute("room_id", roomId);
		RoomWizardHelper.addBookingNode(bookings, bookingParameters);

		// TODO verify BookingNode

		System.out.println("addBookingNode");
		System.out.println(result.asXML());
	}

	/**
	 * A unit test for JUnit
	 */
	public void testAddConnectorNode() {
		final Element result = RoomWizardHelper.prepareXmlResponseBegin();
		RoomWizardHelper.addConnectorNode(result);
		verifyConnectorNode(result);

		System.out.println("addConnectorNode");
		System.out.println(result.asXML());
	}

	/**
	 * Description of the Method
	 *
	 * @param result
	 *            Description of the Parameter
	 */
	public static void verifyResultNodeBegin(final Element result) {
		{
			final Element date = (Element) result.selectSingleNode("descendant-or-self::kwe:result/kwe:date");
			assertNotNull(date.getText());
		}
		{
			final Element time = (Element) result.selectSingleNode("descendant-or-self::kwe:result/kwe:time");
			assertNotNull(time.getText());
		}
	}

	/**
	 * Description of the Method
	 *
	 * @param result
	 *            Description of the Parameter
	 */
	public static void verifyResultNodeFinish(final Element result) {
		{
			final Element element = (Element) result.selectSingleNode("descendant-or-self::kwe:result/kwe:result_code");
			assertNotNull(element.getText());
		}
		{
			final Element element = (Element) result.selectSingleNode("descendant-or-self::kwe:result/rb:result_code");
			assertNotNull(element.getText());
		}
	}

	/**
	 * Description of the Method
	 *
	 * @param result
	 *            Description of the Parameter
	 */
	public static void verifyConnectorNode(final Element result) {
		{
			final Element element = (Element) result.selectSingleNode("descendant-or-self::kwe:connector/kwe:name");
			assertNotNull(element.getText());
		}
		{
			final Element element = (Element) result.selectSingleNode("descendant-or-self::kwe:connector/kwe:version");
			assertNotNull(element.getText());
		}
		{
			final Element element = (Element) result.selectSingleNode("descendant-or-self::kwe:connector/kwe:short");
			assertEquals("WebCentral", element.getText());
		}
		{
			final Element element = (Element) result.selectSingleNode("descendant-or-self::kwe:connector/kwe:api");
			assertNotNull(element.getText());
		}
	}
}
