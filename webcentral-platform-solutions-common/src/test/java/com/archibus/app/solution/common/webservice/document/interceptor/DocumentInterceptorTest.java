package com.archibus.app.solution.common.webservice.document.interceptor;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.*;

import com.archibus.datasource.DataSourceTestBase;
import com.archibus.service.*;

public class DocumentInterceptorTest extends DataSourceTestBase {
	private DocumentService documentService;

	public DocumentService getDocumentService() {
		return this.documentService;
	}

	public void setDocumentService(final DocumentService documentService) {
		this.documentService = documentService;
	}

	@Override
	protected String[] getConfigLocations() {
		return new String[] { "/context/core/core-infrastructure.xml", "appContext-test.xml",
				"/com/archibus/app/solution/common/webservice/document/interceptor/documentService.xml" };
	}

	public void testCheckinNewFile() throws MalformedURLException {
		final InputStream inputStream = TestDocumentService.prepareInputStream("New file content.");

		final Map<String, String> keys = new HashMap<String, String>();
		keys.put("ls_id", "101");

		final String fieldName = "doc";
		final String tableName = "ls";
		final String newLockStatus = "0";
		final String documentName = "lease1.doc";
		final String comments = "New file comments";

		this.documentService.checkinNewFile(inputStream, keys, tableName, fieldName, documentName, comments,
				newLockStatus);
		// TODO verify
	}
}
