package com.archibus.app.solution.common.eventhandler.service;

import org.dom4j.Element;
import org.junit.experimental.categories.Category;

import com.archibus.core.test.fixture.category.FastTest;

import junit.framework.TestCase;

/**
 * @author tydykov
 * @created November 1, 2006
 */
@Category({ FastTest.class })
public class TestAboutConnectorCommand extends TestCase {

	/**
	 * A unit test for JUnit
	 *
	 * @exception RoomWizardException
	 *                Description of the Exception
	 */
	public void testAboutConnector() throws RoomWizardException {
		final RoomWizardCommand command = RoomWizardCommand.getInstance(RoomWizardCommand.COMMAND_ADD_BOOKING, null);

		final Element result = command.execute();

		TestRoomWizardHelper.verifyResultNodeBegin(result);
		TestRoomWizardHelper.verifyConnectorNode(result);

		System.out.println(result.asXML());
	}
}
