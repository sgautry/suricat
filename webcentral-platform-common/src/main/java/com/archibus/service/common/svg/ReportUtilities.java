package com.archibus.service.common.svg;

import com.archibus.context.ContextStore;
import com.archibus.ext.report.ReportUtility;
import com.archibus.model.view.report.ReportPropertiesDef;

/**
 *
 * Utilities for SvgReport. Provides methods for loading svg.
 *
 * @author shao
 * @since 21.1
 *
 */
public final class ReportUtilities {
	/**
	 * Private constructor: utility class is non instantiable.
	 */
	private ReportUtilities() {
	}

	/**
	 *
	 * Gets Report PropertiesDef.
	 *
	 * @return ReportPropertiesDef object.
	 */
	public static ReportPropertiesDef getReportPropertiesDef() {
		return ReportUtility.getReportPropertiesDef(ContextStore.get());
	}

}
