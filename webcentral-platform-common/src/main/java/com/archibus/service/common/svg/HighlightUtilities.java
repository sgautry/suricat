package com.archibus.service.common.svg;

import java.util.*;

import org.dom4j.*;

import com.archibus.app.common.drawing.svg.service.domain.HighlightParameters;
import com.archibus.ext.drawing.highlight.drawing.Highlight;
import com.archibus.ext.drawing.highlight.patterns.HighlightPatternUtilities;
import com.archibus.utility.StringUtil;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Utility class. Provides individual asset highlighting.
 *
 * @author Yong Shao
 * @since 23.2
 *
 */
public final class HighlightUtilities {
    /**
     * NEWSCALE.
     */
    public static final String NEWSCALE = " scale(%f)";

    /**
     * HATCHPATTERNID.
     */
    public static final String HATCHPATTERNID = "url(#%s)";

    /**
     * INCHES2MM.
     */
    public static final double INCHES2MM = 25.4;

    /**
     * INCHES2CM.
     */
    public static final double INCHES2CM = 2.54;

    /**
     * INCHES2METER.
     */
    public static final double INCHES2METER = 0.0254;

    /**
     * INCHES.
     */
    public static final String INCHES = "inches";

    /**
     * PATTERNTRANSFORM.
     */
    public static final String PATTERNTRANSFORM = "patternTransform";

    /**
     * SCALEPART.
     */
    public static final String SCALEPART = "scale(";

    /**
     * STROKE.
     */
    public static final String STROKE = "stroke";

    /**
     * STROKE_WIDTH.
     */
    public static final String STROKE_WIDTH = "stroke-width";

    /**
     * STYLE_SEPARATOR.
     */
    public static final char STYLE_SEPARATOR = ':';

    /**
     * Private constructor: utility class is non instantiable.
     */
    private HighlightUtilities() {
    }

    /**
     *
     * Highlights specified Asset element.
     *
     * @param asset Element.
     * @param highlight Highlight.
     * @param svgDefsElement Element.
     * @param svgDocument Document.
     * @param svgPatterns Document.
     * @param highlightParameter HighlightParameters.
     */
    public static void highlightAsset(final Element asset, final Highlight highlight,
            final Element svgDefsElement, final org.dom4j.Document svgDocument,
            final org.dom4j.Document svgPatterns, final HighlightParameters highlightParameter) {
        final boolean isEnableHatchPatterns = highlightParameter.isEnableHatchPatterns();
        final HighlightPatternUtilities.FillStyle fillType = highlight.getStyle();
        if (highlightParameter.isBorderHighlighting()) {
            asset.addAttribute(STROKE, highlight.getRGBColorValue());
            asset.addAttribute(STROKE_WIDTH, highlightParameter.getBorderSize() + "px");
        } else if (isEnableHatchPatterns
                && fillType == HighlightPatternUtilities.FillStyle.HATCHED) {
            highlightWithHatchPatterns(asset, highlight, svgDefsElement, svgDocument, svgPatterns);
        } else if (isEnableHatchPatterns
                && fillType == HighlightPatternUtilities.FillStyle.GRADIENT) {
            highlightWithGradient(asset, highlight, svgDefsElement, svgDocument, svgPatterns);
        } else {
            asset.addAttribute(Constants.STYLE_FILL, highlight.getRGBColorValue());
        }
    }

    /**
     *
     * Hatch pattern highlights.
     *
     * @param asset Element.
     * @param highlight Highlight.
     * @param svgDefsElement Element.
     * @param svgDocument Document.
     * @param svgPatterns Document.
     */
    private static void highlightWithHatchPatterns(final Element asset, final Highlight highlight,
            final Element svgDefsElement, final org.dom4j.Document svgDocument,
            final org.dom4j.Document svgPatterns) {
        final String units = getPublishedUnit(svgDocument);

        final String hatchPatternId =
                highlight.getName() + highlight.getColor().hashCode() + StringUtil.notNull(units);

        // check if it already exists in svg
        Element pattern = (Element) svgDocument
            .selectSingleNode(String.format(Constants.PATTERNS_ELEMENT_XPATH, hatchPatternId));
        if (pattern == null) {
            // retreat the pattern from pattern defs
            pattern = getDefinedPattern(highlight, svgPatterns, hatchPatternId);
            if (pattern != null) {
                // update scale based on published unit
                updatePatternScale(pattern, units);
                // add to svg doc
                svgDefsElement.add(pattern);
            }
        }

        asset.addAttribute(Constants.STYLE_FILL, String.format(HATCHPATTERNID, hatchPatternId));
    }

    /**
     *
     * Gets Published Unit.
     * 
     * @param svgDocument SVG Document.
     * @return unit.
     */
    public static String getPublishedUnit(final org.dom4j.Document svgDocument) {
        String units = INCHES;
        final Element view = (Element) svgDocument.selectSingleNode("//svg:view");
        if (view != null) {
            units = view.attributeValue("units");
        }
        return units;
    }

    /**
     *
     * Updates Pattern Scale based on units.
     *
     * @param pattern Element.
     * @param units String.
     */
    public static void updatePatternScale(final Element pattern, final String units) {
        // only if units is mm, cm and meter
        if (StringUtil.isNullOrEmpty(units) || INCHES.equals(units)) {
            return;
        }

        String patternTransformValue = "";
        double scale = 1.0;
        final Attribute patternTransform = pattern.attribute(PATTERNTRANSFORM);
        if (patternTransform != null) {
            patternTransformValue = patternTransform.getValue();
            final int index = patternTransformValue.indexOf(SCALEPART);
            if (index >= 0) {
                scale = Double.parseDouble(patternTransformValue.substring(
                    index + SCALEPART.length(), patternTransformValue.indexOf(')', index)));

                // remove existing scale
                patternTransformValue = patternTransformValue.replace(patternTransformValue
                    .substring(index, patternTransformValue.indexOf(')', index) + 1), "");
            }
        }

        if ("mm".equals(units)) {
            scale = scale * INCHES2MM;
        } else if ("cm".equals(units)) {
            scale = scale * INCHES2CM;
        } else {
            scale = scale * INCHES2METER;
        }

        // add new scale to patternTransform attribute
        pattern.addAttribute(PATTERNTRANSFORM,
            patternTransformValue + String.format(NEWSCALE, scale));

    }

    /**
     *
     * Gradient Highlights.
     *
     * @param asset Element.
     * @param highlight Highlight.
     * @param svgDefsElement Element.
     * @param svgDocument Document.
     * @param svgPatterns Document.
     */
    private static void highlightWithGradient(final Element asset, final Highlight highlight,
            final Element svgDefsElement, final org.dom4j.Document svgDocument,
            final org.dom4j.Document svgPatterns) {
        final HighlightPatternUtilities.GradientType type = highlight.getGradientType();
        final String hatchPatternId = type.toString() + highlight.getColor().hashCode()
                + highlight.getGradientColor().hashCode();

        Element pattern = null;
        if (type == HighlightPatternUtilities.GradientType.SPHERICAL
                || type == HighlightPatternUtilities.GradientType.INVSPHERICAL) {
            pattern = getPatternElement(svgDocument, hatchPatternId,
                Constants.RADIALGRADIENT_ELEMENT_XPATH);
        } else {
            pattern = getPatternElement(svgDocument, hatchPatternId,
                Constants.LINEARGRADIENT_ELEMENT_XPATH);
        }

        if (pattern == null) {
            // retreat the pattern from pattern defs
            pattern = getDefinedPattern(highlight, svgPatterns, hatchPatternId);
            if (pattern != null) {
                svgDefsElement.add(pattern);
            }
        }
        asset.addAttribute(Constants.STYLE_FILL, String.format(HATCHPATTERNID, hatchPatternId));
    }

    /**
     *
     * Gets Defined Pattern from pattern SVG document.
     *
     * @param highlight Highlight.
     * @param svgPatterns pattern SVG document
     * @param hatchPatternId pattern id.
     * @return Element.
     */
    public static Element getDefinedPattern(final Highlight highlight,
            final org.dom4j.Document svgPatterns, final String hatchPatternId) {
        final List<String> rgbColors = new ArrayList<String>();
        final HighlightPatternUtilities.FillStyle type = highlight.getStyle();
        Element pattern = null;
        if (type == HighlightPatternUtilities.FillStyle.HATCHED) {
            pattern = (Element) svgPatterns.selectSingleNode(
                String.format(Constants.PATTERNS_ELEMENT_XPATH, highlight.getName()));
            rgbColors.add(highlight.getRGBColorValue());
        } else if (type == HighlightPatternUtilities.FillStyle.GRADIENT) {
            pattern = getGradientPattern(highlight, svgPatterns, highlight.getGradientType(),
                rgbColors);
        }
        if (pattern != null) {
            // set up the pattern for usage
            pattern = pattern.createCopy();
            pattern.addAttribute(Constants.ELEMENT_ID, hatchPatternId);
            if (type == HighlightPatternUtilities.FillStyle.GRADIENT) {
                setupGradientPattern(pattern, hatchPatternId, rgbColors);
            } else {
                pattern.addAttribute(STROKE, rgbColors.get(0));
            }
        }
        return pattern;
    }

    /**
     * Gets Processed Gradient Pattern.
     *
     * @param highlight Highlight.
     * @param svgPatterns Document.
     * @param type GradientType
     * @param rgbColors list of RGB color value.
     * @return processed SVG element.
     */
    public static Element getGradientPattern(final Highlight highlight,
            final org.dom4j.Document svgPatterns, final HighlightPatternUtilities.GradientType type,
            final List<String> rgbColors) {
        Element pattern = null;
        final String name = type.toString();
        switch (type) {
            case LINEAR:
                pattern = getPatternElement(svgPatterns, name,
                    Constants.LINEARGRADIENT_ELEMENT_XPATH);
                setupRGBColors(rgbColors, highlight, true);
                break;
            case SPHERICAL:
                pattern = getPatternElement(svgPatterns, name,
                    Constants.RADIALGRADIENT_ELEMENT_XPATH);
                setupRGBColors(rgbColors, highlight, true);
                break;
            case INVSPHERICAL:
                pattern = getPatternElement(svgPatterns, name,
                    Constants.RADIALGRADIENT_ELEMENT_XPATH);
                setupRGBColors(rgbColors, highlight, false);
                break;
            case CYLINDER:
                pattern = getPatternElement(svgPatterns, name,
                    Constants.LINEARGRADIENT_ELEMENT_XPATH);
                setupRGBColors(rgbColors, highlight, true);
                rgbColors.add(highlight.getRGBColorValue());
                break;
            case INVCYLINDER:
                pattern = getPatternElement(svgPatterns, name,
                    Constants.LINEARGRADIENT_ELEMENT_XPATH);
                setupRGBColors(rgbColors, highlight, false);
                rgbColors.add(highlight.getGradientRGBColorValu());
                break;
            case CURVED:
                pattern = getPatternElement(svgPatterns, name,
                    Constants.LINEARGRADIENT_ELEMENT_XPATH);
                setupRGBColors(rgbColors, highlight, true);
                break;
            case INVCURVED:
                pattern = getPatternElement(svgPatterns, name,
                    Constants.LINEARGRADIENT_ELEMENT_XPATH);
                setupRGBColors(rgbColors, highlight, false);
                break;
            case HEMISPHERICAL:
                pattern = getPatternElement(svgPatterns, name,
                    Constants.LINEARGRADIENT_ELEMENT_XPATH);
                setupRGBColors(rgbColors, highlight, true);
                break;
            case INVHEMISPHERICAL:
                pattern = getPatternElement(svgPatterns, name,
                    Constants.LINEARGRADIENT_ELEMENT_XPATH);
                setupRGBColors(rgbColors, highlight, false);
                break;
            default:
                break;
        }

        return pattern;
    }

    /**
     *
     * Gets Pattern Element.
     *
     * @param document Document.
     * @param patternId pattern Id
     * @param xPath String.
     * @return Element.
     */
    private static Element getPatternElement(final org.dom4j.Document document,
            final String patternId, final String xPath) {
        final Node node = document.selectSingleNode(String.format(xPath, patternId));
        if (node == null) {
            return null;
        } else {
            return (Element) node;
        }
    }

    /**
     *
     * Sets up RGB Colors.
     *
     * @param rgbColors List of RGB color value .
     * @param highlight Highlight.
     * @param isRegular boolean regular or reversed.
     */
    private static void setupRGBColors(final List<String> rgbColors, final Highlight highlight,
            final boolean isRegular) {
        if (isRegular) {
            rgbColors.add(highlight.getRGBColorValue());
            rgbColors.add(highlight.getGradientRGBColorValu());
        } else {
            rgbColors.add(highlight.getGradientRGBColorValu());
            rgbColors.add(highlight.getRGBColorValue());
        }
    }

    /**
     *
     * Sets up Gradient Pattern.
     *
     * @param pattern Element.
     * @param hatchPatternId pattern id.
     * @param rgbColors list of RGB color value.
     */
    public static void setupGradientPattern(final Element pattern, final String hatchPatternId,
            final List<String> rgbColors) {
        pattern.addAttribute(Constants.ELEMENT_ID, hatchPatternId);
        @SuppressWarnings("unchecked")
        final List<Element> stopElements = pattern.elements();
        int indexing = 0;
        for (final Element stopElement : stopElements) {
            stopElement.addAttribute("stop-color", rgbColors.get(indexing));
            indexing++;
        }
    }
}