package com.archibus.service.common.svg;

import java.io.InputStream;
import java.util.*;

import org.apache.log4j.Logger;
import org.dom4j.Element;

import com.archibus.app.common.drawing.svg.service.domain.HighlightParameters;
import com.archibus.context.ContextStore;
import com.archibus.datasource.DataSourceFactory;
import com.archibus.datasource.data.DataRecord;
import com.archibus.db.RestrictionSqlBase;
import com.archibus.ext.drawing.highlight.*;
import com.archibus.ext.drawing.highlight.drawing.Highlight;
import com.archibus.ext.drawing.highlight.labeling.AssetLabelUtilities;
import com.archibus.ext.report.ReportUtility;
import com.archibus.model.drawing.HatchPatternConfig;
import com.archibus.model.view.report.ReportPropertiesDef;
import com.archibus.schema.PrimaryKey;
import com.archibus.utility.*;

/**
 *
 * Provides methods for processing html5-based drawing svg document.
 * <p>
 * Loads requested enterprise-graphics svg file.
 * <p>
 * Processes svg document with specified highlight and label datatSources.
 * <p>
 * Invoked by other WFR.
 *
 * @author shao
 * @since 21.1
 *
 */
public class SvgReport {
	/**
	 * Logger for this class and subclasses.
	 */
	protected final Logger logger = Logger.getLogger(this.getClass());

	/**
	 * drawing name like HQ18.
	 */
	private final String drawingName;

	/**
	 * svgDocument svg document object.
	 */
	private final org.dom4j.Document svgDocument;

	/**
	 * svgPatterns Svg patterns.
	 */
	private final org.dom4j.Document svgPatterns;

	/**
	 * asset to highlight.
	 */
	private String assetToHighlight = "rm";

	/**
	 * Hide no highlighted assets.
	 */
	private boolean hideNoHighlightedAssets;

	/**
	 *
	 * Default constructor: initialize svg Document by its inputStream object.
	 *
	 * @param svgFile
	 *            svg inputStream object.
	 * @param drawingName
	 *            the drawing name.
	 */
	public SvgReport(final String drawingName, final InputStream svgFile) {
		this.svgDocument = ReportUtility.loadSvg(svgFile);
		this.drawingName = drawingName;
		// XXX: it's dependent on the new java bean existence???
		final HatchPatternConfig drawingControlHatches = (HatchPatternConfig) ContextStore.get()
				.getBean("drawingControlHatches");
		this.svgPatterns = drawingControlHatches.getSvgPatterns();
	}

	/**
	 *
	 * Processes asset's highlighting and labeling.
	 *
	 * @param highlightParameter
	 *            HighlightParameters
	 * @throws ExceptionBase
	 *             if SvgReport.processAssets() throws an exception.
	 */
	public void processAsset(final HighlightParameters highlightParameter) throws ExceptionBase {
		this.setHideNoHighlightedAssets(highlightParameter.isHideNotHighlightedAssets());
		String viewName = highlightParameter.getViewName();
		com.archibus.datasource.DataSource selectionDataSource = null;
		if (StringUtil.notNullOrEmpty(highlightParameter.getSelectionDatasourceId())) {
			selectionDataSource = DataSourceFactory.loadDataSourceFromFile(viewName,
					highlightParameter.getSelectionDatasourceId());
		}

		com.archibus.datasource.DataSource highlightDataSource = null;
		if (StringUtil.notNullOrEmpty(highlightParameter.getHighlightDatasourceId())) {
			highlightDataSource = DataSourceFactory.loadDataSourceFromFile(viewName,
					highlightParameter.getHighlightDatasourceId());
		}
		com.archibus.datasource.DataSource labelDataSource = null;
		if (StringUtil.notNullOrEmpty(highlightParameter.getLabelDataSourceId())) {
			if (StringUtil.notNullOrEmpty(highlightParameter.getLabelDatasourceViewName())) {
				viewName = highlightParameter.getLabelDatasourceViewName();
			}
			labelDataSource = DataSourceFactory.loadDataSourceFromFile(viewName,
					highlightParameter.getLabelDataSourceId());
		}
		if (highlightParameter.getDataSourceParameters() != null) {
			ReportUtility.applyParameters2DataSource(highlightDataSource, highlightParameter.getDataSourceParameters());
			if (labelDataSource != null) {
				ReportUtility.applyParameters2DataSource(labelDataSource, highlightParameter.getDataSourceParameters());
			}
			if (selectionDataSource != null) {
				ReportUtility.applyParameters2DataSource(selectionDataSource,
						highlightParameter.getDataSourceParameters());
			}
		}

		processAsset(highlightDataSource, labelDataSource, highlightParameter, null, selectionDataSource);
	}

	/**
	 *
	 * Processes svg drawing by highlighting and labeling the asset.
	 *
	 * @param highlightDataSource
	 *            DataSource.
	 * @param labelDataSource
	 *            DataSource.
	 * @param highlightParameter
	 *            HighlightParameters.
	 * @param clientLabelingRestriction
	 *            String.
	 * @param selectionDataSource
	 *            DataSource.
	 * @throws ExceptionBase
	 *             if anything wrong.
	 */
	public void processAsset(final com.archibus.datasource.DataSource highlightDataSource,
			final com.archibus.datasource.DataSource labelDataSource, final HighlightParameters highlightParameter,
			final String clientLabelingRestriction, final com.archibus.datasource.DataSource selectionDataSource)
			throws ExceptionBase {

		if (StringUtil.isNullOrEmpty(highlightParameter.getAssetType()) && highlightDataSource != null) {
			this.setAssetToHighlight(highlightDataSource.getMainTableName());
		} else {
			this.setAssetToHighlight(highlightParameter.getAssetType());
		}

		final DrawingHighlightProperties drawingHighlightProperties = getDrawingHighlightProperties(highlightDataSource,
				labelDataSource, highlightParameter.getRestriction(), clientLabelingRestriction);

		final Element labelsElement = (Element) this.svgDocument.selectSingleNode(Constants.LABELS_ELEMENT_XPATH);

		{
			final Element assetsElement = (Element) this.svgDocument
					.selectSingleNode(String.format(Constants.ASSETS_ELEMENT_XPATH, this.assetToHighlight));

			final List<RestrictionSqlBase.Immutable> selectableRestrictions = RestrictionUtilities.getRestriction(null,
					selectionDataSource, this.drawingName);

			processAssetsSelection(assetsElement, selectionDataSource, selectableRestrictions);

			final Map<String, Highlight> highlights = drawingHighlightProperties.getHighlights();

			highlightAssets(assetsElement, highlights, labelsElement, highlightParameter);
		}

		{
			final Map<String, List<String>> labels = drawingHighlightProperties.getLabels();
			String asset = this.assetToHighlight;
			if (labelDataSource != null) {
				labelDataSource.setContext();
				asset = labelDataSource.getMainTableName();
			}
			LabelUtilities.processLabeling(labelsElement, asset, labels, highlightParameter.getLabelHeight(),
					highlightParameter.getLabelColorName());
		}
	}

	/**
	 *
	 * TODO processAssetsSelection.
	 *
	 * @param assetsElement
	 *            Element.
	 * @param selectionDataSource
	 *            DataSource.
	 * @param selectableRestrictions
	 *            List<RestrictionSqlBase.Immutable> .
	 */
	private void processAssetsSelection(final Element assetsElement,
			final com.archibus.datasource.DataSource selectionDataSource,
			final List<RestrictionSqlBase.Immutable> selectableRestrictions) {
		if (assetsElement == null || selectionDataSource == null) {
			return;
		}
		selectionDataSource.setContext();
		selectionDataSource.setMaxRecords(0);

		final ArrayList<String> keys = new ArrayList<String>();
		final PrimaryKey.Immutable primaryKey = selectionDataSource.getMainTableDef().getPrimaryKey();
		final List<DataRecord> records = selectionDataSource.getRecords(selectableRestrictions);
		for (final DataRecord record : records) {
			keys.add(HighLightUtilities.getValidHTML5IDVALUE(AssetLabelUtilities.getKey(primaryKey, record)));
		}

		@SuppressWarnings("unchecked")
		final List<Element> assets = assetsElement.elements();
		for (final Element asset : assets) {
			final String assetId = asset.attributeValue(Constants.ELEMENT_ID);
			if (keys.contains('"' + assetId + '"')) {
				asset.addAttribute(Constants.SELECTABLE_ASSET, "true");
			} else {
				asset.addAttribute(Constants.SELECTABLE_ASSET, "false");
			}
		}
	}

	/**
	 *
	 * Gets highlighted assets properties.
	 *
	 * @param highlightDataSource
	 *            - highlight DataSource object.
	 * @param labelDataSource
	 *            - label DataSource object.
	 * @param clientHLRestriction
	 *            string.
	 * @param clientLabelRestriction
	 *            string.
	 * @return DrawingHighlightProperties - highlighted assets properties.
	 */
	private DrawingHighlightProperties getDrawingHighlightProperties(
			final com.archibus.datasource.DataSource highlightDataSource,
			final com.archibus.datasource.DataSource labelDataSource, final String clientHLRestriction,
			final String clientLabelRestriction) {
		final ReportPropertiesDef reportPropertiesDef = ReportUtilities.getReportPropertiesDef();
		reportPropertiesDef.setAssetTables(this.getAssetToHighlight());

		final HighlightImageService highlightImageService = new HighlightImageService(reportPropertiesDef);

		return highlightImageService.retrieveDrawingHighlightProperties(this.drawingName, highlightDataSource,
				labelDataSource, true, clientHLRestriction, clientLabelRestriction);
	}

	/**
	 *
	 * Highlights assets.
	 *
	 * @param assetsElement
	 *            svg element to present assets.
	 * @param highlights
	 *            Map<String, Highlight> from highlight dataSource.
	 * @param labelsElement
	 *            - Element.
	 * @param highlightParameter
	 *            - HighlightParameters.
	 *
	 */
	private void highlightAssets(final Element assetsElement, final Map<String, Highlight> highlights,
			final Element labelsElement, final HighlightParameters highlightParameter) {
		if (assetsElement == null || (highlights.isEmpty() && !this.hideNoHighlightedAssets)) {
			return;
		}

		@SuppressWarnings("unchecked")
		final List<Element> assets = assetsElement.elements();
		final Element svgDefsElement = this.svgDocument.getRootElement().element(Constants.SVG_DEFS);

		for (final Element asset : assets) {
			final String assetId = asset.attributeValue(Constants.ELEMENT_ID);
			final Highlight highlight = highlights.get(assetId);

			if (this.hideNoHighlightedAssets && (highlight == null || highlight.getColor() == null)) {
				// XXX: remove asset element.
				asset.detach();
				// XXX: remove corresponding label element.
				removeAssetLabel(assetId, labelsElement);
			}

			if (highlight != null) {
				HighlightUtilities.highlightAsset(asset, highlight, svgDefsElement, this.svgDocument, this.svgPatterns,
						highlightParameter);
			}
			final String highlightedValue = asset.attributeValue(Constants.HIGHLIGHTED_ASSET);
			if (StringUtil.isNullOrEmpty(highlightedValue)
					|| (StringUtil.notNullOrEmpty(highlightedValue) && !StringUtil.toBoolean(highlightedValue))) {
				final Boolean highlighted = highlight != null;
				asset.addAttribute(Constants.HIGHLIGHTED_ASSET, highlighted.toString());
			}
		}
	}

	/**
	 *
	 * Removes asset's label by specified asset id.
	 *
	 * @param assetId
	 *            String like "HQ;17;124"
	 * @param labels
	 *            Element.
	 */
	public void removeAssetLabel(final String assetId, final Element labels) {
		if (labels != null) {
			final org.dom4j.Node label = labels
					.selectSingleNode(String.format(Constants.LABEL_ELEMENT_XPATH, this.assetToHighlight, assetId));
			if (label != null) {
				label.detach();
			}
		}
	}

	/**
	 * Getter for assetToHighlight.
	 *
	 * @see assetToHighlight
	 * @return property.assetToHighlight
	 */
	public String getAssetToHighlight() {
		return this.assetToHighlight;
	}

	/**
	 * Setter for assetToHighlight.
	 *
	 * @see assetToHighlight
	 * @param assetToHighlight
	 *            - String value like "bl"
	 */
	public void setAssetToHighlight(final String assetToHighlight) {
		this.assetToHighlight = assetToHighlight;
	}

	/**
	 *
	 * Gets svg Document.
	 *
	 * @return svg org.dom4j.Document.
	 */
	public org.dom4j.Document getSvgDocument() {
		return this.svgDocument;
	}

	/**
	 * Getter for the hideNoHighlightedAssets property.
	 *
	 * @see hideNoHighlightedAssets
	 * @return the hideNoHighlightedAssets property.
	 */
	public boolean isHideNoHighlightedAssets() {
		return this.hideNoHighlightedAssets;
	}

	/**
	 * Setter for the hideNoHighlightedAssets property.
	 *
	 * @see hideNoHighlightedAssets
	 * @param hideNoHighlightedAssets
	 *            the hideNoHighlightedAssets to set
	 */

	public void setHideNoHighlightedAssets(final boolean hideNoHighlightedAssets) {
		this.hideNoHighlightedAssets = hideNoHighlightedAssets;
	}

	/**
	 *
	 * Adjusts specified Asset Labels Position.
	 *
	 * @param labelPosition
	 *            LabelPosition object.
	 */
	public void adjustAssetLabelsPosition(final LabelPosition labelPosition) {
		if (labelPosition != null) {
			LabelUtilities.adjustAssetLabelsPosition(labelPosition, this.svgDocument);
		}
	}

	/**
	 *
	 * Adds Diagonal pattern.
	 */
	public void addDiagonalPattern() {
		 Element diagonalPattern = (Element) this.svgPatterns
            .selectSingleNode("//svg:pattern[@id='" + Constants.DIAGONALPATTERN_ID + "']");
		if (diagonalPattern != null) {
		    diagonalPattern = diagonalPattern.createCopy();
            final String unit = HighlightUtilities.getPublishedUnit(this.svgDocument);
            diagonalPattern.addAttribute(Constants.ELEMENT_ID,
                Constants.DIAGONALPATTERN_ID + StringUtil.notNull(unit));
            HighlightUtilities.updatePatternScale(diagonalPattern, unit);
			this.svgDocument.getRootElement().element(Constants.SVG_DEFS).add(diagonalPattern);
		}
	}
}
