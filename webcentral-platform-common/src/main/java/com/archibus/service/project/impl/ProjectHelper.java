package com.archibus.service.project.impl;

import com.archibus.datasource.data.DataRecord;
import com.archibus.service.project.dao.IProjectDao;
import com.archibus.service.project.dao.datasource.ProjectDataSource;
import com.archibus.service.project.domain.Project;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Helper class for project management service.
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class ProjectHelper {

	/**
	 * Project data source.
	 */
	private IProjectDao<Project> projectDao;

	/**
	 * Create new Project from project data record.
	 * <p>
	 * Save Project with auto-generated Project Code.
	 *
	 * @param projectRecord
	 *            Project data record.
	 * @return Project Record with primary key value.
	 */
	public DataRecord createProject(final DataRecord projectRecord) {
		if (this.projectDao == null) {
			this.projectDao = new ProjectDataSource();
		}
		return this.projectDao.createProject(projectRecord);
	}
}