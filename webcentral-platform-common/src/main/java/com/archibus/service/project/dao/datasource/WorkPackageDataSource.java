package com.archibus.service.project.dao.datasource;

import static com.archibus.service.project.impl.Constants.*;

import java.util.List;

import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions.Restriction;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.ParsedRestrictionDef;
import com.archibus.service.project.dao.IWorkPackageDao;
import com.archibus.service.project.domain.WorkPackage;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DataSource for project work package. Mapped to work_pkgs database table.
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class WorkPackageDataSource extends ObjectDataSourceImpl<WorkPackage> implements IWorkPackageDao<WorkPackage> {
	/**
	 * Constant : table name: "work_pkgs".
	 */
	private static final String WORK_PKGS = "work_pkgs";
	/**
	 * Constant: field name: "work_pkg_id".
	 */
	private static final String WORK_PKG_ID = "work_pkg_id";

	/**
	 * Constant: field name: "project_id".
	 */
	private static final String PROJECT_ID = "project_id";

	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { PROJECT_ID, "projectId" },
			{ WORK_PKG_ID, "workPackageId" }, { SUMMARY, "summary" }, { DESCRIPTION, "description" },
			{ "days_per_week", "daysPerWeek" }, { "date_est_start", "dateEstimatedStart" },
			{ "date_est_end", "dateEstimatedEnd" } };

	/**
	 * Constructs WorkPackageDataSource, mapped to <code>work_pkgs</code> table,
	 * using <code>workPackage</code> bean.
	 */
	public WorkPackageDataSource() {
		super("workPackage", WORK_PKGS);
	}

	/** {@inheritDoc} */
	@Override
	public List<WorkPackage> getWorkPackagesByProject(final String projectId, final Restriction restriction) {
		final DataSource dataSource = this.createCopy();
		// assemble parsed restriction from projectId.
		final ParsedRestrictionDef restrictionDef = prepareRestriction(projectId);
		if (restriction != null) {
			dataSource.addRestriction(restriction);
		}
		final List<DataRecord> records = dataSource.getRecords(restrictionDef);

		return new DataSourceObjectConverter<WorkPackage>().convertRecordsToObjects(records, this.beanName,
				this.fieldToPropertyMapping, null);
	}

	/**
	 * Prepares parsed restriction using specified projectId.
	 *
	 * @param projectId
	 *            to use in the restriction.
	 * @return restriction for the "work_pkgs.projectId" field with specified
	 *         projectId.
	 */
	private ParsedRestrictionDef prepareRestriction(final String projectId) {
		final ParsedRestrictionDef restrictionDef = new ParsedRestrictionDef();
		restrictionDef.addClause(WORK_PKGS, PROJECT_ID, projectId, Operation.EQUALS);
		return restrictionDef;
	}

	@Override
	protected String[][] getFieldsToProperties() {
		return FIELDS_TO_PROPERTIES.clone();
	}
}