package com.archibus.service.project.dao;

import java.util.List;

import com.archibus.core.dao.IDao;
import com.archibus.datasource.restriction.Restrictions.Restriction;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DAO for project work package. Mapped to work_pkgs database table.
 *
 * @author Radu Bunea
 * @since 23.2
 *
 * @param <WorkPackage>
 *            type of the persistent object
 */
public interface IWorkPackageDao<WorkPackage> extends IDao<WorkPackage> {
	/**
	 * Gets work packages for the specified project ID.
	 *
	 * @param projectId
	 *            to get work packages for.
	 * @param restriction
	 *            optional restriction.
	 * @return List of work packages.
	 */
	List<WorkPackage> getWorkPackagesByProject(final String projectId, Restriction restriction);

}
