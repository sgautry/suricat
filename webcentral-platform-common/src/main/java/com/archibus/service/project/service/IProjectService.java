package com.archibus.service.project.service;

import com.archibus.datasource.data.DataRecord;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides methods for Capital Project Management Service.
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public interface IProjectService {
	/**
	 * Create new Project from project data record.
	 * <p>
	 * Save Project with auto-generated Project Code.
	 *
	 * @param projectRecord
	 *            Project data record.
	 * @return Project Record with primary key value.
	 */
	DataRecord createProject(final DataRecord projectRecord);

	/**
	 * Copy template project, work packages and actions.
	 * <p>
	 * Copy project and action documents.
	 *
	 * @param projectIdTemplate
	 *            Project ID of template project.
	 * @param projectIdDestination
	 *            Project ID of new project.
	 * @param isNewProject
	 *            Project is new.
	 */
	void copyTemplateProject(final String projectIdTemplate, final String projectIdDestination,
			final boolean isNewProject);

	/**
	 * Cascade all task dependencies.
	 * <p>
	 * Adjust all task dependencies dates for project actions where predecessors
	 * are defined.
	 *
	 * @return Message with list of adjusted tasks.
	 */
	String cascadeAllTaskDependencies();

	/**
	 *
	 * Adjust task dependencies for a project id.
	 *
	 * @param projectId
	 *            Project id.
	 * @return Message with list of adjusted tasks.
	 */
	String cascadeProjectTaskDependencies(final String projectId);

	/**
	 * Cascade all task dependencies for a project action.
	 *
	 * @param activityLogId
	 *            Action id.
	 * @return Message with list of adjusted tasks.
	 */
	String cascadeTaskDependencies(final int activityLogId);
}