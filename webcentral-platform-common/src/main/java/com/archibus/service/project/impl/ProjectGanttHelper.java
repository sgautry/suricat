package com.archibus.service.project.impl;

import static com.archibus.service.project.impl.Constants.*;

import java.util.*;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import com.archibus.context.ContextStore;
import com.archibus.datasource.*;
import com.archibus.datasource.DataSource.RecordHandler;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;
import com.archibus.eventhandler.EventHandlerBase;
import com.archibus.utility.StringUtil;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 *
 * Provides methods to adjust project tasks dates.
 * <p>
 * Invoked by web central, used by gantt controller.
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class ProjectGanttHelper {

	/**
	 * Constant: baseline date fields.
	 */
	private static final String[] BASELINE_DATE_FIELDS = { "activity_log.date_planned_for",
			"activity_log.date_planned_end", "activity_log.duration_est_baseline" };

	/**
	 * Constant: design date fields.
	 */
	private static final String[] DESIGN_DATE_FIELDS = { "activity_log.date_scheduled",
			"activity_log.date_scheduled_end", "activity_log.duration" };
	/**
	 * Constant: default days per week.
	 */
	private static final int DEFAULT_DAYS_PER_WEEK = 5;
	/**
	 * Constant: default start week day.
	 */
	private static final int DEFAULT_START_WEEK_DAY = 7;

	/**
	 * Message dependency exists.
	 */
	// @translatable
	private static final String MSG_DEPENDENCY_EXISTS = "A dependency loop exists involving the following task id(s). Please correct this issue before proceeding:";

	/**
	 * Logger for this class.
	 */
	protected final Logger logger = Logger.getLogger(this.getClass());
	/**
	 * Message to display on gantt view.
	 */
	private StringBuffer messageBuffer;

	/**
	 * Dependency tasks.
	 */
	private String dependencyLoopTasks = "";

	/**
	 * List of dependency tasks.
	 */
	private final List<String> dependencyList = new ArrayList<String>();

	/**
	 * Cascade all task dependencies.
	 * <p>
	 * Adjust all task dependencies dates for project actions where predecessors
	 * are defined.
	 *
	 * @return Message with list of adjusted tasks.
	 */
	public String cascadeAllTaskDependencies() {
		// @non-translatable
		final String operation = "Cascade all task dependencies: %s";
		if (this.logger.isInfoEnabled()) {
			final String message = String.format(operation, LOGGER_MSG_START);
			this.logger.info(message);
		}

		this.messageBuffer = new StringBuffer("");

		final DataSource dataSource = DataSourceFactory.createDataSourceForFields(ACTIVITYLOG_TABLE,
				new String[] { ACTIVITY_LOG_ID }, false);
		dataSource.addRestriction(Restrictions.sql(" activity_log.predecessors IS NOT NULL "));

		dataSource.queryRecords(new RecordHandler() {

			@Override
			public boolean handleRecord(final DataRecord record) {
				clearDependencyList();
				final int recordTaskId = record.getInt(ACTIVITYLOG_TABLE + DOT + ACTIVITY_LOG_ID);
				adjustTaskDates(recordTaskId);
				return true;
			}
		});
		if (StringUtil.notNullOrEmpty(this.dependencyLoopTasks)) {
			this.messageBuffer
					.append(LINE_BREAK
							+ EventHandlerBase.localizeString(ContextStore.get().getEventHandlerContext(),
									MSG_DEPENDENCY_EXISTS, this.getClass().getName())
							+ LINE_BREAK + this.dependencyLoopTasks);
		}
		if (this.logger.isInfoEnabled()) {
			final String message = String.format(operation, LOGGER_MSG_END);
			this.logger.info(message);
		}
		return this.messageBuffer.toString();
	}

	/**
	 * Cascade all task dependencies for a project action.
	 *
	 * @param activityLogId
	 *            Action id.
	 * @return Message with list of adjusted tasks.
	 */
	public String cascadeTaskDependencies(final int activityLogId) {
		// @non-translatable
		final String operation = "Cascade task dependencies: %s";
		if (this.logger.isInfoEnabled()) {
			final String message = String.format(operation, LOGGER_MSG_START);
			this.logger.info(message);
		}

		this.messageBuffer = new StringBuffer("");

		adjustAllTaskDates(activityLogId);
		cascadeChanges(activityLogId);

		if (StringUtil.notNullOrEmpty(this.dependencyLoopTasks)) {
			this.messageBuffer
					.append(LINE_BREAK + EventHandlerBase.localizeString(ContextStore.get().getEventHandlerContext(),
							MSG_DEPENDENCY_EXISTS, this.getClass().getName()) + this.dependencyLoopTasks);
		}
		if (this.logger.isInfoEnabled()) {
			final String message = String.format(operation, LOGGER_MSG_END);
			this.logger.info(message);
		}
		return this.messageBuffer.toString();
	}

	/**
	 * Find out if this task id is in predecessor field of any other action and
	 * if so, call adjustAllTaskDates for that action. If a start date is
	 * subsequently adjusted for that action, call cascadeChanges for that
	 * action.
	 *
	 * @param activityLogId
	 *            Action id.
	 */
	private void cascadeChanges(final int activityLogId) {

		final int taskId = activityLogId;
		// Check for dependency loops
		if (this.dependencyList.contains(String.valueOf(taskId))) {
			this.dependencyLoopTasks += LINE_BREAK + taskId;
			return;
		}
		this.dependencyList.add(String.valueOf(taskId));

		final DataSource dataSource = DataSourceFactory.createDataSourceForFields(ACTIVITYLOG_TABLE,
				new String[] { ACTIVITY_LOG_ID, PREDECESSORS }, false);
		dataSource.addRestriction(Restrictions.sql(" activity_log.predecessors LIKE '%" + taskId + "%' "));
		dataSource.queryRecords(new RecordHandler() {

			@Override
			public boolean handleRecord(final DataRecord record) {
				final int recordTaskId = record.getInt(ACTIVITYLOG_TABLE + DOT + ACTIVITY_LOG_ID);
				final String predecessors = record.getString(ACTIVITYLOG_TABLE + DOT + PREDECESSORS);
				if (StringUtil.notNullOrEmpty(predecessors)) {
					final StringTokenizer predecessorSplit = new StringTokenizer(predecessors,
							MULTIPLE_VALUES_SEPARATOR);
					while (predecessorSplit.hasMoreTokens()) {
						final String predecessor = predecessorSplit.nextToken().trim();
						if (predecessor.equals(String.valueOf(taskId))) {
							adjustTaskDates(recordTaskId);
							break;
						}
					}
				}

				return true;
			}
		});
	}

	/**
	 *
	 * Adjust task dates and apply changes when predecessors exist.
	 *
	 * @param taskId
	 *            Task id.
	 */
	private void adjustTaskDates(final int taskId) {
		if (adjustAllTaskDates(taskId)) {
			cascadeChanges(taskId);
		}
	}

	/**
	 * Call adjustTaskDates for the baseline date fields and for the design date
	 * fields.
	 *
	 * @param activityLogId
	 *            Action id.
	 * @return true if a start date was adjusted for this task.
	 */
	private boolean adjustAllTaskDates(final int activityLogId) {

		boolean adjusted = false;

		final DataSource dataSource = DataSourceFactory.createDataSourceForFields(ACTIVITYLOG_TABLE, ACTIVITYLOG_FIELDS,
				false);
		dataSource.addRestriction(Restrictions.eq(ACTIVITYLOG_TABLE, ACTIVITY_LOG_ID, activityLogId));
		final DataRecord record = dataSource.getRecord();

		adjusted = adjustTaskDates(dataSource, record, BASELINE_DATE_FIELDS);
		if (adjustTaskDates(dataSource, record, DESIGN_DATE_FIELDS)) {
			adjusted = true;
		}
		if (adjusted) {
			this.messageBuffer.append(activityLogId + ": " + record.getString(ACTIVITYLOG_TABLE + DOT + ACTION_TITLE));
			this.messageBuffer.append(LINE_BREAK);
		}
		return adjusted;
	}

	/**
	 * If the start date of this task is earlier than any predecessor's end
	 * date, edit this task's start date to come after the end date of the
	 * predecessor. Calculate end date according to adjusted start date and task
	 * duration.
	 *
	 * @param dataSource
	 *            Data source to save record.
	 * @param record
	 *            Data record.
	 * @param dateFields
	 *            Date fields.
	 * @return true if the start date was adjusted for this task.
	 */
	private boolean adjustTaskDates(final DataSource dataSource, final DataRecord record, final String[] dateFields) {
		boolean adjusted = false;
		final int daysPerWeek = getDaysPerWeek(record);
		final String predecessors = record.getString(ACTIVITYLOG_TABLE + DOT + PREDECESSORS);
		if (StringUtil.notNullOrEmpty(predecessors)) {
			final StringTokenizer splitPredecessors = new StringTokenizer(predecessors, MULTIPLE_VALUES_SEPARATOR);
			Calendar dateStart = Calendar.getInstance();
			dateStart.setTime(record.getDate(dateFields[0]));
			while (splitPredecessors.hasMoreTokens()) {
				final String pred = splitPredecessors.nextToken().trim();
				final Calendar predDateEnd = getFieldValueAsCal(pred, dateFields[1]);
				if (predDateEnd == null) {
					continue;
				}
				if (dateStart.before(predDateEnd) || DateUtils.isSameDay(dateStart, predDateEnd)) {
					dateStart = (Calendar) predDateEnd.clone();
					dateStart.add(Calendar.DATE, 1);
					dateStart = adjustStartDateByDaysPerWeek(dateStart, daysPerWeek);
					adjusted = true;
				}
			}
			if (adjusted) {
				final Calendar dateEnd = adjustDateEnd((Calendar) dateStart.clone(), record.getInt(dateFields[2]),
						daysPerWeek);
				record.setValue(dateFields[0], dateStart.getTime());
				record.setValue(dateFields[1], dateEnd.getTime());
				dataSource.saveRecord(record);
			}
		}
		return adjusted;
	}

	/**
	 *
	 * Get field value as calendar object.
	 *
	 * @param activityLogId
	 *            Action id.
	 * @param dateField
	 *            Date field.
	 * @return Calendar date.
	 */
	private Calendar getFieldValueAsCal(final String activityLogId, final String dateField) {
		Calendar dateEnd = null;
		final DataSource dataSource = DataSourceFactory.createDataSourceForFields(ACTIVITYLOG_TABLE,
				new String[] { ACTIVITY_LOG_ID, dateField }, false);
		dataSource.addRestriction(Restrictions.eq(ACTIVITYLOG_TABLE, ACTIVITY_LOG_ID, activityLogId));
		final DataRecord record = dataSource.getRecord();
		if (record != null) {
			dateEnd = Calendar.getInstance();
			dateEnd.setTime(record.getDate(dateField));
		}
		return dateEnd;
	}

	/**
	 *
	 * Adjust action date end by duration and days per week.
	 *
	 * @param date
	 *            Date end.
	 * @param duration
	 *            Duration.
	 * @param daysPerWeek
	 *            Days per week.
	 * @return Calendar date end.
	 */
	private Calendar adjustDateEnd(final Calendar date, final int duration, final int daysPerWeek) {
		Calendar dateEnd = (Calendar) date.clone();
		if (duration > 0) {
			dateEnd.add(Calendar.DATE, -1);
			dateEnd = truncateEndDateByDaysPerWeek(dateEnd, daysPerWeek);
			final int weeks = (int) Math.floor(duration / daysPerWeek);
			dateEnd.add(Calendar.DATE, weeks * DEFAULT_START_WEEK_DAY);
			int durationRemain = duration % daysPerWeek;
			while (durationRemain > 0) {
				dateEnd.add(Calendar.DATE, 1);
				// (0 - 6) 0 = Sunday
				int day = dateEnd.get(Calendar.DAY_OF_WEEK) - 1;
				if (day == 0) {
					// (1 - 7) 1 = Monday
					day = DEFAULT_START_WEEK_DAY;
				}
				if (day <= daysPerWeek) {
					// check if day is a working day
					durationRemain--;
				}
			}
		}
		return dateEnd;
	}

	/**
	 *
	 * Truncate end date by days per week.
	 *
	 * @param dateEnd
	 *            Date end.
	 * @param daysPerWeek
	 *            Days per week.
	 * @return Calendar date end.
	 */
	private Calendar truncateEndDateByDaysPerWeek(final Calendar dateEnd, final int daysPerWeek) {
		// (0 - 6) 0 = Sunday
		int day = dateEnd.get(Calendar.DAY_OF_WEEK) - 1;
		if (day == 0) {
			// (1 - 7) 1 = Monday
			day = DEFAULT_START_WEEK_DAY;
		}
		if (day > daysPerWeek) {
			// truncate non-working days
			dateEnd.add(Calendar.DATE, daysPerWeek - day);
		}
		return dateEnd;
	}

	/**
	 *
	 * Adjust start date by days per week.
	 *
	 * @param dateStart
	 *            Date start.
	 * @param daysPerWeek
	 *            Days per week.
	 * @return Calendar date start.
	 */
	private Calendar adjustStartDateByDaysPerWeek(final Calendar dateStart, final int daysPerWeek) {
		// (0 - 6) 0 = Sunday
		int day = dateStart.get(Calendar.DAY_OF_WEEK) - 1;
		if (day == 0) {
			// (1 - 7) 1 = Monday
			day = DEFAULT_START_WEEK_DAY;
		}
		if (day > daysPerWeek) {
			// adjust start date up to next working day
			dateStart.add(Calendar.DATE, (DEFAULT_START_WEEK_DAY + 1) - day);
		}
		return dateStart;
	}

	/**
	 *
	 * Get days per week.
	 *
	 * @param record
	 *            Date record.
	 * @return number of days per week.
	 */
	private int getDaysPerWeek(final DataRecord record) {
		final String workPackageId = record.getString(ACTIVITYLOG_TABLE + DOT + WORK_PKG_ID);
		int daysPerWeek = DEFAULT_DAYS_PER_WEEK;
		if (StringUtil.notNullOrEmpty(workPackageId)) {
			final DataSource workPackageDataSource = DataSourceFactory.createDataSourceForFields(WORKPKG_TABLE,
					new String[] { WORK_PKG_ID, DAYS_PER_WEEK }, false);
			workPackageDataSource.addRestriction(Restrictions.eq(WORKPKG_TABLE, WORK_PKG_ID, workPackageId));
			final DataRecord workPackageRecord = workPackageDataSource.getRecord();
			daysPerWeek = workPackageRecord.getInt("work_pkgs.days_per_week");
		} else {
			final String projectId = record.getString(ACTIVITYLOG_TABLE + DOT + PROJECT_ID);
			if (StringUtil.notNullOrEmpty(projectId)) {
				final DataSource projectDataSource = DataSourceFactory.createDataSourceForFields(PROJECT_TABLE,
						new String[] { PROJECT_ID, DAYS_PER_WEEK }, false);
				projectDataSource.addRestriction(Restrictions.eq(PROJECT_TABLE, PROJECT_ID, projectId));
				final DataRecord projectRecord = projectDataSource.getRecord();
				daysPerWeek = projectRecord.getInt(PROJECT_TABLE + DOT + DAYS_PER_WEEK);
			}
		}
		return daysPerWeek;
	}

	/**
	 * Clear dependency tasks.
	 */
	private void clearDependencyList() {
		for (int i = 0; i < this.dependencyList.size(); i++) {
			this.dependencyList.remove(i);
		}
	}
}