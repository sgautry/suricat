/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * This package contains API for DAOs for Capital Project Management domain.
 **/
package com.archibus.service.project.dao;