package com.archibus.service.project.dao.datasource;

import static com.archibus.service.project.impl.Constants.*;

import java.util.List;

import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.ParsedRestrictionDef;
import com.archibus.service.project.dao.IActionDao;
import com.archibus.service.project.domain.Action;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DataSource for project action. Mapped to activity_log database table.
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class ActionDataSource extends ObjectDataSourceImpl<Action> implements IActionDao<Action> {
	/**
	 * Constant : table name: "activity_log".
	 */
	private static final String ACTIVITY_LOG = "activity_log";
	/**
	 * Constant: field name: "activity_log_id".
	 */
	private static final String ACTIVITY_LOG_ID = "activity_log_id";
	/**
	 * Constant: field name: "work_pkg_id".
	 */
	private static final String WORK_PKG_ID = "work_pkg_id";

	/**
	 * Constant: field name: "project_id".
	 */
	private static final String PROJECT_ID = "project_id";

	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { PROJECT_ID, "projectId" },
			{ WORK_PKG_ID, "workPackageId" }, { ACTIVITY_LOG_ID, "actionId" }, { ACTION_TITLE, "actionTitle" },
			{ ACTIVITY_TYPE, "activityType" }, { COST_ESTIMATED, "costEstimated" },
			{ "cost_est_cap", "costEstimatedBaseline" }, { "duration_est_baseline", "durationEstimatedBaseline" },
			{ DURATION, "duration" }, { "hours_est_baseline", "hoursEstimatedBaseline" },
			{ "hours_est_design", "hoursEstimatedDesign" }, { WBS_ID, "wbsId" }, { DESCRIPTION, "description" },
			{ DATE_PLANNED_FOR, "datePlannedFor" }, { DATE_PLANNED_END, "datePlannedEnd" },
			{ DATE_SCHEDULED, "dateToPerform" }, { DATE_SCHEDULED_END, "dateCompletion" }, { DOC, "document" },
			{ PREDECESSORS, "predecessors" } };

	/**
	 * Constructs ActionDataSource, mapped to <code>activity_log</code> table,
	 * using <code>action</code> bean.
	 */
	public ActionDataSource() {
		super("action", ACTIVITY_LOG);
	}

	/** {@inheritDoc} */
	@Override
	public List<Action> getActionsByProjectAndWorkPackage(final String projectId, final String workPackageId) {
		final DataSource dataSource = this.createCopy();
		// assemble parsed restriction from projectId and workPackageId.
		final ParsedRestrictionDef restrictionDef = prepareRestriction(projectId, workPackageId);

		final List<DataRecord> records = dataSource.getRecords(restrictionDef);

		return new DataSourceObjectConverter<Action>().convertRecordsToObjects(records, this.beanName,
				this.fieldToPropertyMapping, null);
	}

	/**
	 * Prepares parsed restriction using specified projectId and workPackageId.
	 *
	 * @param projectId
	 *            to use in the restriction.
	 * @param workPackageId
	 *            to use in the restriction.
	 * @return restriction for the "activity_log.projectId" field with specified
	 *         projectId and if workPackageId exists, restriction for the
	 *         "activity_log.work_pkg_id" field with specified workPackageId
	 *
	 */
	private ParsedRestrictionDef prepareRestriction(final String projectId, final String workPackageId) {
		final ParsedRestrictionDef restrictionDef = new ParsedRestrictionDef();
		restrictionDef.addClause(ACTIVITY_LOG, PROJECT_ID, projectId, Operation.EQUALS);
		if (workPackageId != null) {
			restrictionDef.addClause(ACTIVITY_LOG, WORK_PKG_ID, projectId, Operation.EQUALS);
		}
		return restrictionDef;
	}

	@Override
	protected String[][] getFieldsToProperties() {
		return FIELDS_TO_PROPERTIES.clone();
	}
}