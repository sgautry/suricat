package com.archibus.service.project.impl;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Public constants for Capital Project Management.
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public final class Constants {
	/**
	 * Constant: cost_estimated field.
	 */
	public static final String COST_ESTIMATED = "cost_estimated";
	/**
	 * Constant: cost_budget field.
	 */
	public static final String COST_BUDGET = "cost_budget";
	/**
	 * Constant: benefit field.
	 */
	public static final String BENEFIT = "benefit";
	/**
	 * Constant: requestor field.
	 */
	public static final String REQUESTOR = "requestor";
	/**
	 * Constant: action_title field.
	 */
	public static final String ACTION_TITLE = "action_title";
	/**
	 * Constant: scope field.
	 */
	public static final String SCOPE = "scope";
	/**
	 * Constant: summary field.
	 */
	public static final String SUMMARY = "summary";
	/**
	 * Constant: wbs_id field.
	 */
	public static final String WBS_ID = "wbs_id";
	/**
	 * Constant: doc field.
	 */
	public static final String DOC = "doc";
	/**
	 * Constant: predecessors field.
	 */
	public static final String PREDECESSORS = "predecessors";
	/**
	 * Constant: date_scheduled_end field.
	 */
	public static final String DATE_SCHEDULED_END = "date_scheduled_end";
	/**
	 * Constant: date_scheduled field.
	 */
	public static final String DATE_SCHEDULED = "date_scheduled";
	/**
	 * Constant: date_planned_end field.
	 */
	public static final String DATE_PLANNED_END = "date_planned_end";
	/**
	 * Constant: date_planned_for field.
	 */
	public static final String DATE_PLANNED_FOR = "date_planned_for";
	/**
	 * Constant: description field.
	 */
	public static final String DESCRIPTION = "description";
	/**
	 * Constant: activity_type field.
	 */
	public static final String ACTIVITY_TYPE = "activity_type";
	/**
	 * Constant: work_pkg_id field.
	 */
	public static final String WORK_PKG_ID = "work_pkg_id";
	/**
	 * Constant: project_id field.
	 */
	public static final String PROJECT_ID = "project_id";
	/**
	 * Constant: activity_log_id field.
	 */
	public static final String ACTIVITY_LOG_ID = "activity_log_id";

	/**
	 * Constant: hours_est_design field.
	 */
	public static final String HOURS_EST_DESIGN = "hours_est_design";
	/**
	 * Constant: hours_est_baseline field.
	 */
	public static final String HOURS_EST_BASELINE = "hours_est_baseline";
	/**
	 * Constant: duration field.
	 */
	public static final String DURATION = "duration";
	/**
	 * Constant: duration_est field.
	 */
	public static final String DURATION_EST = "duration_est";
	/**
	 * Constant: duration_est_baseline field.
	 */
	public static final String DURATION_EST_BASELINE = "duration_est_baseline";
	/**
	 * Constant: days_per_week field.
	 */
	public static final String DAYS_PER_WEEK = "days_per_week";
	/**
	 * Constant: date_start field.
	 */
	public static final String DATE_START = "date_start";
	/**
	 * Constant: cost_est_cap field.
	 */
	public static final String COST_EST_CAP = "cost_est_cap";

	/**
	 * Constant: template_action_id key.
	 */
	public static final String TEMPLATE_ACTION_ID = "template_action_id";
	/**
	 * Constant: template_predecessors key.
	 */
	public static final String TEMPLATE_PREDECESSORS = "template_predecessors";
	/**
	 * Constant: action_id key.
	 */
	public static final String ACTION_ID = "action_id";
	/**
	 * Constant project table.
	 */
	public static final String PROJECT_TABLE = "project";
	/**
	 * Constant project fields.
	 */
	public static final String[] PROJECT_FIELDS = { PROJECT_ID, "project_name", "project_type", DATE_START, "date_end",
			"dept_contact", "phone_dept_contact", "bl_id", "site_id", REQUESTOR, "phone_req", "contact_id",
			"is_template", SUMMARY, DESCRIPTION, BENEFIT, SCOPE, DURATION_EST, DAYS_PER_WEEK, COST_BUDGET };
	/**
	 * Constant work_pkgs table.
	 */
	public static final String WORKPKG_TABLE = "work_pkgs";
	/**
	 * Constant work_pkgs fields.
	 */
	public static final String[] WORKPKG_FIELDS = { PROJECT_ID, WORK_PKG_ID, SUMMARY, DESCRIPTION, DAYS_PER_WEEK,
			"date_est_start", "date_est_end" };
	/**
	 * Constant activity_log table.
	 */
	public static final String ACTIVITYLOG_TABLE = "activity_log";

	/**
	 * Constant activity_log fields.
	 */
	public static final String[] ACTIVITYLOG_FIELDS = { ACTIVITY_LOG_ID, PROJECT_ID, WORK_PKG_ID, ACTION_TITLE,
			ACTIVITY_TYPE, COST_ESTIMATED, COST_EST_CAP, DURATION_EST_BASELINE, DURATION, HOURS_EST_BASELINE,
			HOURS_EST_DESIGN, WBS_ID, DESCRIPTION, DATE_PLANNED_FOR, DATE_PLANNED_END, DATE_SCHEDULED,
			DATE_SCHEDULED_END, DOC, PREDECESSORS };

	/**
	 * Constant: separator character for multiple values in one field.
	 */
	public static final String MULTIPLE_VALUES_SEPARATOR = ", \u200C";

	/**
	 * Constant: hyphen.
	 */
	public static final String HYPHEN = "-";
	/**
	 * Constant: dot.
	 */
	public static final String DOT = ".";
	/**
	 * Constant: Line break.
	 */
	public static final String LINE_BREAK = "<br/>";
	/**
	 * Constant: Logger message start.
	 */
	public static final String LOGGER_MSG_START = "Started";
	/**
	 * Constant: Logger message end.
	 */
	public static final String LOGGER_MSG_END = "OK";

	/**
	 * Hide default constructor for this utility class - should never be
	 * instantiated.
	 */
	private Constants() {
	}
}
