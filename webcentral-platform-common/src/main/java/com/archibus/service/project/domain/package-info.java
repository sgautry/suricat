/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * This package contains domain objects for Capital Project Management domain.
 **/
package com.archibus.service.project.domain;