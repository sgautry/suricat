package com.archibus.service.project.domain;

import java.util.Date;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for work package. Mapped to work_pkgs database table.
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class WorkPackage {
	/**
	 * Project Id.
	 */
	private String projectId;
	/**
	 * Work Package Id.
	 */
	private String workPackageId;
	/**
	 * Summary.
	 */
	private String summary;
	/**
	 * Description.
	 */
	private String description;
	/**
	 * Days Per Week.
	 */
	private int daysPerWeek;
	/**
	 * Date Estimated Start.
	 */
	private Date dateEstimatedStart;
	/**
	 * Date Estimated End.
	 */
	private Date dateEstimatedEnd;

	/**
	 * Getter for the projectId property.
	 *
	 * @return the projectId property.
	 */
	public String getProjectId() {
		return this.projectId;
	}

	/**
	 * Setter for the projectId property.
	 *
	 * @param projectId
	 *            the projectId to set.
	 */
	public void setProjectId(final String projectId) {
		this.projectId = projectId;
	}

	/**
	 * Getter for the workPackageId property.
	 *
	 * @return the workPackageId property.
	 */
	public String getWorkPackageId() {
		return this.workPackageId;
	}

	/**
	 * Setter for the workPackageId property.
	 *
	 * @param workPackageId
	 *            the workPackageId to set.
	 */
	public void setWorkPackageId(final String workPackageId) {
		this.workPackageId = workPackageId;
	}

	/**
	 * Getter for the summary property.
	 *
	 * @return the summary property.
	 */
	public String getSummary() {
		return this.summary;
	}

	/**
	 * Setter for the summary property.
	 *
	 * @param summary
	 *            the summary to set.
	 */
	public void setSummary(final String summary) {
		this.summary = summary;
	}

	/**
	 * Getter for the description property.
	 *
	 * @return the description property.
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Setter for the description property.
	 *
	 * @param description
	 *            the description to set.
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Getter for the daysPerWeek property.
	 *
	 * @return the daysPerWeek property.
	 */
	public int getDaysPerWeek() {
		return this.daysPerWeek;
	}

	/**
	 * Setter for the daysPerWeek property.
	 *
	 * @param daysPerWeek
	 *            the daysPerWeek to set.
	 */
	public void setDaysPerWeek(final int daysPerWeek) {
		this.daysPerWeek = daysPerWeek;
	}

	/**
	 * Getter for the dateEstimatedStart property.
	 *
	 * @return the dateEstimatedStart property.
	 */
	public Date getDateEstimatedStart() {
		return this.dateEstimatedStart;
	}

	/**
	 * Setter for the dateEstimatedStart property.
	 *
	 * @param dateEstimatedStart
	 *            the dateEstimatedStart to set.
	 */
	public void setDateEstimatedStart(final Date dateEstimatedStart) {
		this.dateEstimatedStart = dateEstimatedStart;
	}

	/**
	 * Getter for the dateEstimatedEnd property.
	 *
	 * @return the dateEstimatedEnd property.
	 */
	public Date getDateEstimatedEnd() {
		return this.dateEstimatedEnd;
	}

	/**
	 * Setter for the dateEstimatedEnd property.
	 *
	 * @param dateEstimatedEnd
	 *            the dateEstimatedEnd to set.
	 */
	public void setDateEstimatedEnd(final Date dateEstimatedEnd) {
		this.dateEstimatedEnd = dateEstimatedEnd;
	}
}