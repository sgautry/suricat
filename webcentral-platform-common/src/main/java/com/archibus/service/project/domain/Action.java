package com.archibus.service.project.domain;

import java.util.Date;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for project action. Mapped to activity_log database table.
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class Action {
	/**
	 * Project Id.
	 */
	private String projectId;
	/**
	 * Work Package Id.
	 */
	private String workPackageId;
	/**
	 * Action Id.
	 */
	private int actionId;
	/**
	 * Action Title.
	 */
	private String actionTitle;
	/**
	 * Activity Type.
	 */
	private String activityType;
	/**
	 * Wbs Id.
	 */
	private String wbsId;
	/**
	 * Description.
	 */
	private String description;
	/**
	 * Predecessors.
	 */
	private String predecessors;
	/**
	 * Document.
	 */
	private String document;
	/**
	 * Duration.
	 */
	private int duration;
	/**
	 * Duration Estimated Baseline.
	 */
	private int durationEstimatedBaseline;
	/**
	 * Hours Estimated Baseline.
	 */
	private int hoursEstimatedBaseline;
	/**
	 * Hours Estimated Design.
	 */
	private int hoursEstimatedDesign;
	/**
	 * Cost Estimated.
	 */
	private double costEstimated;
	/**
	 * Cost Estimated Baseline.
	 */
	private double costEstimatedBaseline;
	/**
	 * Date Planned For.
	 */
	private Date datePlannedFor;
	/**
	 * Date Planned End.
	 */
	private Date datePlannedEnd;
	/**
	 * Date To Perform.
	 */
	private Date dateToPerform;
	/**
	 * Date Completion.
	 */
	private Date dateCompletion;

	/**
	 * Getter for the projectId property.
	 *
	 * @return the projectId property.
	 */
	public String getProjectId() {
		return this.projectId;
	}

	/**
	 * Setter for the projectId property.
	 *
	 * @param projectId
	 *            the projectId to set.
	 */
	public void setProjectId(final String projectId) {
		this.projectId = projectId;
	}

	/**
	 * Getter for the workPackageId property.
	 *
	 * @return the workPackageId property.
	 */
	public String getWorkPackageId() {
		return this.workPackageId;
	}

	/**
	 * Setter for the workPackageId property.
	 *
	 * @param workPackageId
	 *            the workPackageId to set.
	 */
	public void setWorkPackageId(final String workPackageId) {
		this.workPackageId = workPackageId;
	}

	/**
	 * Getter for the actionId property.
	 *
	 * @return the actionId property.
	 */
	public int getActionId() {
		return this.actionId;
	}

	/**
	 * Setter for the actionId property.
	 *
	 * @param actionId
	 *            the actionId to set.
	 */
	public void setActionId(final int actionId) {
		this.actionId = actionId;
	}

	/**
	 * Getter for the actionTitle property.
	 *
	 * @return the actionTitle property.
	 */
	public String getActionTitle() {
		return this.actionTitle;
	}

	/**
	 * Setter for the actionTitle property.
	 *
	 * @param actionTitle
	 *            the actionTitle to set.
	 */
	public void setActionTitle(final String actionTitle) {
		this.actionTitle = actionTitle;
	}

	/**
	 * Getter for the activityType property.
	 *
	 * @return the activityType property.
	 */
	public String getActivityType() {
		return this.activityType;
	}

	/**
	 * Setter for the activityType property.
	 *
	 * @param activityType
	 *            the activityType to set.
	 */
	public void setActivityType(final String activityType) {
		this.activityType = activityType;
	}

	/**
	 * Getter for the wbsId property.
	 *
	 * @return the wbsId property.
	 */
	public String getWbsId() {
		return this.wbsId;
	}

	/**
	 * Setter for the wbsId property.
	 *
	 * @param wbsId
	 *            the wbsId to set.
	 */
	public void setWbsId(final String wbsId) {
		this.wbsId = wbsId;
	}

	/**
	 * Getter for the description property.
	 *
	 * @return the description property.
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Setter for the description property.
	 *
	 * @param description
	 *            the description to set.
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Getter for the predecessors property.
	 *
	 * @return the predecessors property.
	 */
	public String getPredecessors() {
		return this.predecessors;
	}

	/**
	 * Setter for the predecessors property.
	 *
	 * @param predecessors
	 *            the predecessors to set.
	 */
	public void setPredecessors(final String predecessors) {
		this.predecessors = predecessors;
	}

	/**
	 * Getter for the document property.
	 *
	 * @return the document property.
	 */
	public String getDocument() {
		return this.document;
	}

	/**
	 * Setter for the document property.
	 *
	 * @param document
	 *            the document to set.
	 */
	public void setDocument(final String document) {
		this.document = document;
	}

	/**
	 * Getter for the duration property.
	 *
	 * @return the duration property.
	 */
	public int getDuration() {
		return this.duration;
	}

	/**
	 * Setter for the duration property.
	 *
	 * @param duration
	 *            the duration to set.
	 */
	public void setDuration(final int duration) {
		this.duration = duration;
	}

	/**
	 * Getter for the durationEstimatedBaseline property.
	 *
	 * @return the durationEstimatedBaseline property.
	 */
	public int getDurationEstimatedBaseline() {
		return this.durationEstimatedBaseline;
	}

	/**
	 * Setter for the durationEstimatedBaseline property.
	 *
	 * @param durationEstimatedBaseline
	 *            the durationEstimatedBaseline to set.
	 */
	public void setDurationEstimatedBaseline(final int durationEstimatedBaseline) {
		this.durationEstimatedBaseline = durationEstimatedBaseline;
	}

	/**
	 * Getter for the hoursEstimatedBaseline property.
	 *
	 * @return the hoursEstimatedBaseline property.
	 */
	public int getHoursEstimatedBaseline() {
		return this.hoursEstimatedBaseline;
	}

	/**
	 * Setter for the hoursEstimatedBaseline property.
	 *
	 * @param hoursEstimatedBaseline
	 *            the hoursEstimatedBaseline to set.
	 */
	public void setHoursEstimatedBaseline(final int hoursEstimatedBaseline) {
		this.hoursEstimatedBaseline = hoursEstimatedBaseline;
	}

	/**
	 * Getter for the hoursEstimatedDesign property.
	 *
	 * @return the hoursEstimatedDesign property.
	 */
	public int getHoursEstimatedDesign() {
		return this.hoursEstimatedDesign;
	}

	/**
	 * Setter for the hoursEstimatedDesign property.
	 *
	 * @param hoursEstimatedDesign
	 *            the hoursEstimatedDesign to set.
	 */
	public void setHoursEstimatedDesign(final int hoursEstimatedDesign) {
		this.hoursEstimatedDesign = hoursEstimatedDesign;
	}

	/**
	 * Getter for the costEstimated property.
	 *
	 * @return the costEstimated property.
	 */
	public double getCostEstimated() {
		return this.costEstimated;
	}

	/**
	 * Setter for the costEstimated property.
	 *
	 * @param costEstimated
	 *            the costEstimated to set.
	 */
	public void setCostEstimated(final double costEstimated) {
		this.costEstimated = costEstimated;
	}

	/**
	 * Getter for the costEstimatedBaseline property.
	 *
	 * @return the costEstimatedBaseline property.
	 */
	public double getCostEstimatedBaseline() {
		return this.costEstimatedBaseline;
	}

	/**
	 * Setter for the costEstimatedBaseline property.
	 *
	 * @param costEstimatedBaseline
	 *            the costEstimatedBaseline to set.
	 */
	public void setCostEstimatedBaseline(final double costEstimatedBaseline) {
		this.costEstimatedBaseline = costEstimatedBaseline;
	}

	/**
	 * Getter for the datePlannedFor property.
	 *
	 * @return the datePlannedFor property.
	 */
	public Date getDatePlannedFor() {
		return this.datePlannedFor;
	}

	/**
	 * Setter for the datePlannedFor property.
	 *
	 * @param datePlannedFor
	 *            the datePlannedFor to set.
	 */
	public void setDatePlannedFor(final Date datePlannedFor) {
		this.datePlannedFor = datePlannedFor;
	}

	/**
	 * Getter for the datePlannedEnd property.
	 *
	 * @return the datePlannedEnd property.
	 */
	public Date getDatePlannedEnd() {
		return this.datePlannedEnd;
	}

	/**
	 * Setter for the datePlannedEnd property.
	 *
	 * @param datePlannedEnd
	 *            the datePlannedEnd to set.
	 */
	public void setDatePlannedEnd(final Date datePlannedEnd) {
		this.datePlannedEnd = datePlannedEnd;
	}

	/**
	 * Getter for the dateToPerform property (date_scheduled).
	 *
	 * @return the dateToPerform property.
	 */
	public Date getDateToPerform() {
		return this.dateToPerform;
	}

	/**
	 * Setter for the dateToPerform property (date_scheduled).
	 *
	 * @param dateToPerform
	 *            the dateToPerform to set.
	 */
	public void setDateToPerform(final Date dateToPerform) {
		this.dateToPerform = dateToPerform;
	}

	/**
	 * Getter for the dateCompletion property (date_scheduled_end).
	 *
	 * @return the dateCompletion property.
	 */
	public Date getDateCompletion() {
		return this.dateCompletion;
	}

	/**
	 * Setter for the dateCompletion property (date_scheduled_end).
	 *
	 * @param dateCompletion
	 *            the dateCompletion to set.
	 */
	public void setDateCompletion(final Date dateCompletion) {
		this.dateCompletion = dateCompletion;
	}
}