package com.archibus.service.project.service;

import static com.archibus.service.project.impl.Constants.*;

import java.util.*;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.archibus.context.ContextStore;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;
import com.archibus.jobmanager.*;
import com.archibus.service.DocumentService;
import com.archibus.service.DocumentService.DocumentParameters;
import com.archibus.service.project.dao.*;
import com.archibus.service.project.domain.*;
import com.archibus.service.project.impl.ProjectGanttHelper;
import com.archibus.utility.StringUtil;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Copy project template data to new or existing project. Run as scheduled job.
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class ProjectServiceJob extends JobBase {
	/**
	 * Logger for this class.
	 */
	protected final Logger logger = Logger.getLogger(this.getClass());
	/**
	 * Job status.
	 */
	private JobStatus jobStatus;

	/**
	 * Property: DAO for project.
	 */
	private IProjectDao<Project> projectDao;

	/**
	 * Property: DAO for work package.
	 */
	private IWorkPackageDao<WorkPackage> workPackageDao;
	/**
	 * Property: DAO for action.
	 */
	private IActionDao<Action> actionDao;

	/**
	 * Copy template project, work packages and actions.
	 * <p>
	 * Copy project and action documents.
	 *
	 * @param projectIdTemplate
	 *            Project ID of template project.
	 * @param projectIdDestination
	 *            Project ID of new project.
	 * @param isNewProject
	 *            Project is new.
	 *            <p>
	 *            Suppress PMD warning "AvoidUsingSql" in this method.
	 *            <p>
	 *            Justification Case 1.1 Statements with SELECT WHERE EXISTS ...
	 *            pattern.
	 *
	 */
	@SuppressWarnings("PMD.AvoidUsingSql")
	public void copyTemplateProject(final String projectIdTemplate, final String projectIdDestination,
			final boolean isNewProject) {
		// @non-translatable
		final String operation = "Copy project template: %s";
		if (this.logger.isInfoEnabled()) {
			final String message = String.format(operation, LOGGER_MSG_START);
			this.logger.info(message);
		}
		final Project projectTemplate = this.projectDao.get(projectIdTemplate);
		final Project projectDestination = this.projectDao.get(projectIdDestination);

		// Avoid duplicate work package name when applying multiple templates
		final List<WorkPackage> templateWorkPackages = this.workPackageDao.getWorkPackagesByProject(projectIdTemplate,
				new Restrictions.Restriction(
						"NOT EXISTS (SELECT 1 FROM work_pkgs wpkg WHERE work_pkgs.work_pkg_id = wpkg.work_pkg_id AND wpkg.project_id = "
								+ SqlUtils.formatValueForSql(projectIdDestination) + ")"));

		final List<Action> templateActions = this.actionDao.getActionsByProjectAndWorkPackage(projectIdTemplate, null);

		final int totalRecords = templateWorkPackages.size() + templateActions.size();
		this.jobStatus.setTotalNumber(totalRecords);

		if (isNewProject) {
			updateProjectFromTemplate(projectDestination, projectTemplate);
			copyProjectDocuments(projectTemplate, projectIdDestination);
		}

		for (final WorkPackage templateWorkPackage : templateWorkPackages) {
			if (this.jobStatus.isStopRequested()) {
				this.jobStatus.setCode(JobStatus.JOB_STOPPED);
				break;
			}
			this.jobStatus.incrementCurrentNumber();

			createWorkPackage(projectDestination, templateWorkPackage);
		}
		final List<Map<String, Object>> listOfPredecessors = new ArrayList<Map<String, Object>>();
		for (final Action templateAction : templateActions) {
			if (this.jobStatus.isStopRequested()) {
				this.jobStatus.setCode(JobStatus.JOB_STOPPED);
				break;
			}
			this.jobStatus.incrementCurrentNumber();

			final Action action = createAction(projectDestination, templateAction);

			if (StringUtil.notNullOrEmpty(templateAction.getDocument())) {
				copyDocumentFromTemplate(ACTIVITYLOG_TABLE, DOC, ACTIVITY_LOG_ID,
						String.valueOf(templateAction.getActionId()), String.valueOf(action.getActionId()));
			}

			final Map<String, Object> predecessorsMapping = setPredecessorsMapping(String.valueOf(action.getActionId()),
					templateAction);
			listOfPredecessors.add(predecessorsMapping);
		}

		savePredecessors(listOfPredecessors);
		cascadeProjectTaskDependencies(projectIdDestination);

		if (this.logger.isInfoEnabled()) {
			final String message = String.format(operation, LOGGER_MSG_END);
			this.logger.info(message);
		}
	}

	/**
	 *
	 * Update project with data from project template.
	 *
	 * @param projectDestination
	 *            Project destination to set value to
	 * @param projectTemplate
	 *            Project template to copy values from
	 *
	 */
	private void updateProjectFromTemplate(final Project projectDestination, final Project projectTemplate) {
		projectDestination.setSummary(projectTemplate.getSummary());
		projectDestination.setDescription(projectTemplate.getDescription());
		projectDestination.setBenefit(projectTemplate.getBenefit());
		projectDestination.setScope(projectTemplate.getScope());
		projectDestination.setDurationEstimated(projectTemplate.getDurationEstimated());
		projectDestination.setDaysPerWeek(projectTemplate.getDaysPerWeek());
		projectDestination.setCostBudgeted(projectTemplate.getCostBudgeted());
		this.projectDao.update(projectDestination);
	}

	/**
	 *
	 * Create new work package with data from template work package.
	 *
	 * @param projectDestination
	 *            Project destination to set to work package.
	 * @param templateWorkPackage
	 *            Work package template to copy values from.
	 * @return Work package object with new created work package id.
	 */
	private WorkPackage createWorkPackage(final Project projectDestination, final WorkPackage templateWorkPackage) {
		final WorkPackage workPackage = new WorkPackage();
		workPackage.setProjectId(projectDestination.getProjectId());
		workPackage.setWorkPackageId(templateWorkPackage.getWorkPackageId());
		workPackage.setSummary(templateWorkPackage.getSummary());
		workPackage.setDescription(templateWorkPackage.getDescription());
		workPackage.setDaysPerWeek(templateWorkPackage.getDaysPerWeek());

		workPackage.setDateEstimatedStart(projectDestination.getDateStart());
		workPackage.setDateEstimatedEnd(projectDestination.getDateEnd());

		return this.workPackageDao.save(workPackage);
	}

	/**
	 *
	 * Create new action with data from template action.
	 * <p>
	 * Adjust action date end based on duration.
	 *
	 * @param projectDestination
	 *            Project destination to set to action.
	 * @param templateAction
	 *            Action template to copy values from.
	 * @return Action object with new created action id.
	 */
	private Action createAction(final Project projectDestination, final Action templateAction) {
		final Action action = new Action();
		action.setProjectId(projectDestination.getProjectId());
		action.setWorkPackageId(templateAction.getWorkPackageId());
		action.setActionTitle(templateAction.getActionTitle());
		action.setActivityType(templateAction.getActivityType());
		action.setCostEstimated(templateAction.getCostEstimated());
		action.setCostEstimatedBaseline(templateAction.getCostEstimatedBaseline());
		action.setDurationEstimatedBaseline(templateAction.getDurationEstimatedBaseline());
		action.setDuration(templateAction.getDuration());
		action.setHoursEstimatedBaseline(templateAction.getHoursEstimatedBaseline());
		action.setHoursEstimatedDesign(templateAction.getHoursEstimatedDesign());
		action.setWbsId(templateAction.getWbsId());
		action.setDescription(templateAction.getDescription());

		final int durationEst = templateAction.getDurationEstimatedBaseline();
		final Calendar dateStart = Calendar.getInstance();
		dateStart.setTime(projectDestination.getDateStart());
		final Calendar dateEnd = adjustActionDateEnd((Calendar) dateStart.clone(), durationEst);

		action.setDatePlannedFor(dateStart.getTime());
		action.setDatePlannedEnd(dateEnd.getTime());
		action.setDateToPerform(dateStart.getTime());
		action.setDateCompletion(dateEnd.getTime());

		return this.actionDao.save(action);

	}

	/**
	 *
	 * Map new created action id with action template predecessors.
	 *
	 * @param actionId
	 *            Action id.
	 * @param templateAction
	 *            Action template.
	 * @return Map {ACTION_ID: actionId, TEMPLATE_ACTION_ID:templateActionId,
	 *         TEMPLATE_PREDECESSORS: templatePredecessors)}
	 */
	private Map<String, Object> setPredecessorsMapping(final String actionId, final Action templateAction) {
		final Map<String, Object> predecessorsMapping = new HashMap<String, Object>();
		predecessorsMapping.put(ACTION_ID, actionId);
		predecessorsMapping.put(TEMPLATE_ACTION_ID, String.valueOf(templateAction.getActionId()));
		predecessorsMapping.put(TEMPLATE_PREDECESSORS, templateAction.getPredecessors());
		return predecessorsMapping;
	}

	/**
	 *
	 * Adjust task dependencies for a project id.
	 *
	 * @param projectId
	 *            Project id.
	 */
	private void cascadeProjectTaskDependencies(final String projectId) {
		final List<Action> actions = this.actionDao.getActionsByProjectAndWorkPackage(projectId, null);
		for (final Action action : actions) {
			new ProjectGanttHelper().cascadeTaskDependencies(action.getActionId());
		}
	}

	/**
	 * Copy project documents from template project.
	 *
	 * @param projectTemplate
	 *            Project template.
	 * @param projectIdDestination
	 *            Project destination id.
	 */
	private void copyProjectDocuments(final Project projectTemplate, final String projectIdDestination) {
		final String projectIdTemplate = projectTemplate.getProjectId();
		final String documentBusinessCase = projectTemplate.getDocumentBusinessCase();
		if (StringUtil.notNullOrEmpty(documentBusinessCase)) {
			copyDocumentFromTemplate(PROJECT_TABLE, DOC, PROJECT_ID, projectIdTemplate, projectIdDestination);
		}
		projectTemplate.getDocumentRiskManagement();
		if (StringUtil.notNullOrEmpty(documentBusinessCase)) {
			copyDocumentFromTemplate(PROJECT_TABLE, "doc_risk_mgmt", PROJECT_ID, projectIdTemplate,
					projectIdDestination);
		}
		projectTemplate.getDocumentCharter();
		if (StringUtil.notNullOrEmpty(documentBusinessCase)) {
			copyDocumentFromTemplate(PROJECT_TABLE, "doc_charter", PROJECT_ID, projectIdTemplate, projectIdDestination);
		}
		projectTemplate.getDocumentImplementationPlan();
		if (StringUtil.notNullOrEmpty(documentBusinessCase)) {
			copyDocumentFromTemplate(PROJECT_TABLE, "doc_impl_plan", PROJECT_ID, projectIdTemplate,
					projectIdDestination);
		}
	}

	/**
	 * Copy document from template project or action.
	 *
	 * @param documentTable
	 *            Document Table.
	 * @param documentField
	 *            Document Field.
	 * @param primaryKeyField
	 *            Primary Key Field.
	 * @param sourceDocumentId
	 *            Id of template project or action with document.
	 * @param targetDocumentId
	 *            Id of new project or action
	 */
	private void copyDocumentFromTemplate(final String documentTable, final String documentField,
			final String primaryKeyField, final String sourceDocumentId, final String targetDocumentId) {

		final String[] documentFields = { primaryKeyField, documentField };
		final DataSource documentDataSource = DataSourceFactory.createDataSourceForFields(documentTable,
				documentFields);
		documentDataSource.addRestriction(Restrictions.eq(documentTable, primaryKeyField, sourceDocumentId));
		final DataRecord sourceDocumentRecord = documentDataSource.getRecord();
		final String fileName = sourceDocumentRecord.getString(documentTable + DOT + documentField);

		final Map<String, String> sourceKeys = new HashMap<String, String>();
		sourceKeys.put(primaryKeyField, sourceDocumentId);
		final DocumentParameters srcDocParam = new DocumentParameters(sourceKeys, documentTable, documentField, null,
				true);

		final Map<String, String> targetKeys = new HashMap<String, String>();
		targetKeys.put(primaryKeyField, targetDocumentId);

		final DocumentParameters targetDocParam = new DocumentParameters(targetKeys, documentTable, documentField,
				fileName, null, "0");
		final DocumentService documentService = (DocumentService) ContextStore.get().getBean("documentService");
		documentService.copyDocument(srcDocParam, targetDocParam);
	}

	/**
	 *
	 * Adjust project action date end by adding duration.
	 *
	 * @param dateEnd
	 *            Date End.
	 * @param duration
	 *            Duration.
	 * @return Calendar date end.
	 */
	private Calendar adjustActionDateEnd(final Calendar dateEnd, final int duration) {
		if (duration > 0) {
			dateEnd.add(Calendar.DATE, -1);
			dateEnd.add(Calendar.DATE, duration);
		}
		return dateEnd;
	}

	/**
	 * Get action id from a list of predecessors.
	 *
	 * @param listOfPredecessors
	 *            List of predecessors.
	 * @param predecessorId
	 *            Predecessor id.
	 * @return Action id associated with a predecessor id.
	 */
	private String getMappedActionId(final List<Map<String, Object>> listOfPredecessors, final String predecessorId) {
		String actionId = null;
		for (final Map<String, Object> predecessor : listOfPredecessors) {
			final String templateActionId = (String) predecessor.get(TEMPLATE_ACTION_ID);
			if (templateActionId.equals(predecessorId)) {
				final String predecessorActionId = (String) predecessor.get(ACTION_ID);
				actionId = predecessorActionId;
				break;
			}
		}
		return actionId;
	}

	/**
	 *
	 * Save project action predecessors.
	 *
	 * @param listOfPredecessors
	 *            List of action predecessors.
	 */
	private void savePredecessors(final List<Map<String, Object>> listOfPredecessors) {
		final List<Map<String, Object>> predecessors = adjustPredecessors(listOfPredecessors);
		for (final Map<String, Object> actionPred : predecessors) {
			for (final Map.Entry<String, Object> entry : actionPred.entrySet()) {
				final int actionId = Integer.parseInt(entry.getKey());
				final String pred = (String) entry.getValue();
				if (StringUtil.notNullOrEmpty(pred)) {
					final Action action = this.actionDao.get(actionId);
					action.setPredecessors(pred);

					this.actionDao.update(action);
				}
			}
		}
	}

	/**
	 *
	 * Adjust project action predecessors based on template project action
	 * predecessors.
	 *
	 * @param templateActionPredecessors
	 *            List of template project predecessors. <code>
	 *            {actionId:predecessors}
	 *             </code>
	 * @return New mapped list of action predecessors (Map<String, Object>).
	 */
	private List<Map<String, Object>> adjustPredecessors(final List<Map<String, Object>> templateActionPredecessors) {
		final List<Map<String, Object>> mappedPredecessorList = new ArrayList<Map<String, Object>>();
		for (final Map<String, Object> templateActionPredecessor : templateActionPredecessors) {
			final String actionId = (String) templateActionPredecessor.get(ACTION_ID);
			final String templatePredecessors = (String) templateActionPredecessor.get(TEMPLATE_PREDECESSORS);
			final Map<String, Object> actionPredecessorsMapping = new HashMap<String, Object>();
			if (StringUtil.notNullOrEmpty(templatePredecessors)) {
				final StringTokenizer splitPredecessors = new StringTokenizer(templatePredecessors,
						MULTIPLE_VALUES_SEPARATOR);
				final List<String> predecessors = new ArrayList<String>();
				while (splitPredecessors.hasMoreTokens()) {
					final String pred = splitPredecessors.nextToken().trim();
					final String activityLogId = getMappedActionId(templateActionPredecessors, pred);
					if (activityLogId != null) {
						predecessors.add(activityLogId);
					}
				}
				actionPredecessorsMapping.put(actionId, StringUtils.join(predecessors, MULTIPLE_VALUES_SEPARATOR));
			}
			mappedPredecessorList.add(actionPredecessorsMapping);
		}
		return mappedPredecessorList;
	}

	/**
	 * Setter for the jobStatus property.
	 *
	 * @see jobStatus
	 * @param jobStatus
	 *            the jobStatus to set
	 */

	public void setJobStatus(final JobStatus jobStatus) {
		this.jobStatus = jobStatus;
	}

	/**
	 * Getter for the projectDao property.
	 *
	 * @return the projectDao property.
	 */
	public IProjectDao<Project> getProjectDao() {
		return this.projectDao;
	}

	/**
	 * Setter for the projectDao property.
	 *
	 * @param projectDao
	 *            the projectDao to set.
	 */
	public void setProjectDao(final IProjectDao<Project> projectDao) {
		this.projectDao = projectDao;
	}

	/**
	 * Getter for the workPackageDao property.
	 *
	 * @return the workPackageDao property.
	 */
	public IWorkPackageDao<WorkPackage> getWorkPackageDao() {
		return this.workPackageDao;
	}

	/**
	 * Setter for the workPackageDao property.
	 *
	 * @param workPackageDao
	 *            the workPackageDao to set.
	 */
	public void setWorkPackageDao(final IWorkPackageDao<WorkPackage> workPackageDao) {
		this.workPackageDao = workPackageDao;
	}

	/**
	 * Getter for the actionDao property.
	 *
	 * @return the actionDao property.
	 */
	public IActionDao<Action> getActionDao() {
		return this.actionDao;
	}

	/**
	 * Setter for the actionDao property.
	 *
	 * @param actionDao
	 *            the actionDao to set.
	 */
	public void setActionDao(final IActionDao<Action> actionDao) {
		this.actionDao = actionDao;
	}
}