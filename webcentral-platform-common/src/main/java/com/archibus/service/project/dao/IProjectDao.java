package com.archibus.service.project.dao;

import java.util.List;

import com.archibus.core.dao.IDao;
import com.archibus.datasource.data.DataRecord;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DAO for project. Mapped to project database table.
 *
 * @author Radu Bunea
 * @since 23.2
 *
 * @param <Project>
 *            type of the persistent object
 */
public interface IProjectDao<Project> extends IDao<Project> {
	/**
	 * Gets all projects.
	 *
	 * @return List of projects.
	 */
	List<Project> getAllProjects();

	/**
	 * Creates new project with new generated project code using the following
	 * format: 2012-000001.
	 * 
	 * @param projectRecord
	 *            Project record to create.
	 * @return Project Record with primary key value.
	 */
	DataRecord createProject(DataRecord projectRecord);
}
