package com.archibus.service.project.dao;

import java.util.List;

import com.archibus.core.dao.IDao;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DAO for project action. Mapped to activity_log database table.
 *
 * @author Radu Bunea
 * @since 23.2
 *
 * @param <Action>
 *            type of the persistent object
 */
public interface IActionDao<Action> extends IDao<Action> {
	/**
	 * Gets actions for the specified project id and work package id.
	 *
	 * @param projectId
	 *            to get actions for.
	 * @param workPackageId
	 *            to get actions for.
	 * @return List of actions.
	 */
	List<Action> getActionsByProjectAndWorkPackage(final String projectId, String workPackageId);
}
