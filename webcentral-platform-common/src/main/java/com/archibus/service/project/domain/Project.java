package com.archibus.service.project.domain;

import java.util.Date;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Domain object for project. Mapped to project database table.
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class Project {
	/**
	 * Project Id.
	 */
	private String projectId;
	/**
	 * Project Name.
	 */
	private String projectName;
	/**
	 * Project Type.
	 */
	private String projectType;
	/**
	 * Building ID.
	 */
	private String buildingId;
	/**
	 * Site Id.
	 */
	private String siteId;
	/**
	 * Requestor.
	 */
	private String requestor;
	/**
	 * Contact Id.
	 */
	private String contactId;
	/**
	 * Summary.
	 */
	private String summary;
	/**
	 * Description.
	 */
	private String description;
	/**
	 * Benefit.
	 */
	private String benefit;
	/**
	 * Scope.
	 */
	private String scope;
	/**
	 * Document Business Case.
	 */
	private String documentBusinessCase;
	/**
	 * Document Risk Management.
	 */
	private String documentRiskManagement;
	/**
	 * Document Charter.
	 */
	private String documentCharter;
	/**
	 * Document Implementation Plan.
	 */
	private String documentImplementationPlan;
	/**
	 * Duration Estimated.
	 */
	private int durationEstimated;
	/**
	 * Days Per Week.
	 */
	private int daysPerWeek;
	/**
	 * Cost Budgeted.
	 */
	private double costBudgeted;
	/**
	 * Date Start.
	 */
	private Date dateStart;
	/**
	 * Date End.
	 */
	private Date dateEnd;

	/**
	 * Getter for the projectId property.
	 *
	 * @return the projectId property.
	 */
	public String getProjectId() {
		return this.projectId;
	}

	/**
	 * Setter for the projectId property.
	 *
	 * @param projectId
	 *            the projectId to set.
	 */
	public void setProjectId(final String projectId) {
		this.projectId = projectId;
	}

	/**
	 * Getter for the projectName property.
	 *
	 * @return the projectName property.
	 */
	public String getProjectName() {
		return this.projectName;
	}

	/**
	 * Setter for the projectName property.
	 *
	 * @param projectName
	 *            the projectName to set.
	 */
	public void setProjectName(final String projectName) {
		this.projectName = projectName;
	}

	/**
	 * Getter for the projectType property.
	 *
	 * @return the projectType property.
	 */
	public String getProjectType() {
		return this.projectType;
	}

	/**
	 * Setter for the projectType property.
	 *
	 * @param projectType
	 *            the projectType to set.
	 */
	public void setProjectType(final String projectType) {
		this.projectType = projectType;
	}

	/**
	 * Getter for the buildingId property.
	 *
	 * @return the buildingId property.
	 */
	public String getBuildingId() {
		return this.buildingId;
	}

	/**
	 * Setter for the buildingId property.
	 *
	 * @param buildingId
	 *            the buildingId to set.
	 */
	public void setBuildingId(final String buildingId) {
		this.buildingId = buildingId;
	}

	/**
	 * Getter for the siteId property.
	 *
	 * @return the siteId property.
	 */
	public String getSiteId() {
		return this.siteId;
	}

	/**
	 * Setter for the siteId property.
	 *
	 * @param siteId
	 *            the siteId to set.
	 */
	public void setSiteId(final String siteId) {
		this.siteId = siteId;
	}

	/**
	 * Getter for the requestor property.
	 *
	 * @return the requestor property.
	 */
	public String getRequestor() {
		return this.requestor;
	}

	/**
	 * Setter for the requestor property.
	 *
	 * @param requestor
	 *            the requestor to set.
	 */
	public void setRequestor(final String requestor) {
		this.requestor = requestor;
	}

	/**
	 * Getter for the contactId property.
	 *
	 * @return the contactId property.
	 */
	public String getContactId() {
		return this.contactId;
	}

	/**
	 * Setter for the contactId property.
	 *
	 * @param contactId
	 *            the contactId to set.
	 */
	public void setContactId(final String contactId) {
		this.contactId = contactId;
	}

	/**
	 * Getter for the summary property.
	 *
	 * @return the summary property.
	 */
	public String getSummary() {
		return this.summary;
	}

	/**
	 * Setter for the summary property.
	 *
	 * @param summary
	 *            the summary to set.
	 */
	public void setSummary(final String summary) {
		this.summary = summary;
	}

	/**
	 * Getter for the description property.
	 *
	 * @return the description property.
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Setter for the description property.
	 *
	 * @param description
	 *            the description to set.
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Getter for the benefit property.
	 *
	 * @return the benefit property.
	 */
	public String getBenefit() {
		return this.benefit;
	}

	/**
	 * Setter for the benefit property.
	 *
	 * @param benefit
	 *            the benefit to set.
	 */
	public void setBenefit(final String benefit) {
		this.benefit = benefit;
	}

	/**
	 * Getter for the scope property.
	 *
	 * @return the scope property.
	 */
	public String getScope() {
		return this.scope;
	}

	/**
	 * Setter for the scope property.
	 *
	 * @param scope
	 *            the scope to set.
	 */
	public void setScope(final String scope) {
		this.scope = scope;
	}

	/**
	 * Getter for the documentBusinessCase property.
	 *
	 * @return the documentBusinessCase property.
	 */
	public String getDocumentBusinessCase() {
		return this.documentBusinessCase;
	}

	/**
	 * Setter for the documentBusinessCase property.
	 *
	 * @param documentBusinessCase
	 *            the documentBusinessCase to set.
	 */
	public void setDocumentBusinessCase(final String documentBusinessCase) {
		this.documentBusinessCase = documentBusinessCase;
	}

	/**
	 * Getter for the documentRiskManagement property.
	 *
	 * @return the documentRiskManagement property.
	 */
	public String getDocumentRiskManagement() {
		return this.documentRiskManagement;
	}

	/**
	 * Setter for the documentRiskManagement property.
	 *
	 * @param documentRiskManagement
	 *            the documentRiskManagement to set.
	 */
	public void setDocumentRiskManagement(final String documentRiskManagement) {
		this.documentRiskManagement = documentRiskManagement;
	}

	/**
	 * Getter for the documentCharter property.
	 *
	 * @return the documentCharter property.
	 */
	public String getDocumentCharter() {
		return this.documentCharter;
	}

	/**
	 * Setter for the documentCharter property.
	 *
	 * @param documentCharter
	 *            the documentCharter to set.
	 */
	public void setDocumentCharter(final String documentCharter) {
		this.documentCharter = documentCharter;
	}

	/**
	 * Getter for the documentImplementationPlan property.
	 *
	 * @return the documentImplementationPlan property.
	 */
	public String getDocumentImplementationPlan() {
		return this.documentImplementationPlan;
	}

	/**
	 * Setter for the documentImplementationPlan property.
	 *
	 * @param documentImplementationPlan
	 *            the documentImplementationPlan to set.
	 */
	public void setDocumentImplementationPlan(final String documentImplementationPlan) {
		this.documentImplementationPlan = documentImplementationPlan;
	}

	/**
	 * Getter for the durationEstimated property.
	 *
	 * @return the durationEstimated property.
	 */
	public int getDurationEstimated() {
		return this.durationEstimated;
	}

	/**
	 * Setter for the durationEstimated property.
	 *
	 * @param durationEstimated
	 *            the durationEstimated to set.
	 */
	public void setDurationEstimated(final int durationEstimated) {
		this.durationEstimated = durationEstimated;
	}

	/**
	 * Getter for the daysPerWeek property.
	 *
	 * @return the daysPerWeek property.
	 */
	public int getDaysPerWeek() {
		return this.daysPerWeek;
	}

	/**
	 * Setter for the daysPerWeek property.
	 *
	 * @param daysPerWeek
	 *            the daysPerWeek to set.
	 */
	public void setDaysPerWeek(final int daysPerWeek) {
		this.daysPerWeek = daysPerWeek;
	}

	/**
	 * Getter for the costBudgeted property.
	 *
	 * @return the costBudgeted property.
	 */
	public double getCostBudgeted() {
		return this.costBudgeted;
	}

	/**
	 * Setter for the costBudgeted property.
	 *
	 * @param costBudgeted
	 *            the costBudgeted to set.
	 */
	public void setCostBudgeted(final double costBudgeted) {
		this.costBudgeted = costBudgeted;
	}

	/**
	 * Getter for the dateStart property.
	 *
	 * @return the dateStart property.
	 */
	public Date getDateStart() {
		return this.dateStart;
	}

	/**
	 * Setter for the dateStart property.
	 *
	 * @param dateStart
	 *            the dateStart to set.
	 */
	public void setDateStart(final Date dateStart) {
		this.dateStart = dateStart;
	}

	/**
	 * Getter for the dateEnd property.
	 *
	 * @return the dateEnd property.
	 */
	public Date getDateEnd() {
		return this.dateEnd;
	}

	/**
	 * Setter for the dateEnd property.
	 *
	 * @param dateEnd
	 *            the dateEnd to set.
	 */
	public void setDateEnd(final Date dateEnd) {
		this.dateEnd = dateEnd;
	}
}