package com.archibus.service.project;

import static com.archibus.service.project.impl.Constants.*;

import java.util.List;

import com.archibus.context.ContextStore;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;
import com.archibus.jobmanager.JobBase;
import com.archibus.service.project.impl.*;
import com.archibus.service.project.service.*;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provide service methods to create project based on a project template.
 * <p>
 * Invoked by web central, used by Capital Project Management.
 * <p>
 * Registered WFR: 'AbCommonResources-ProjectService'.
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class ProjectService extends JobBase implements IProjectService {

	/** {@inheritDoc} */
	@Override
	public DataRecord createProject(final DataRecord projectRecord) {
		return new ProjectHelper().createProject(projectRecord);
	}

	/** {@inheritDoc} */
	@Override
	public void copyTemplateProject(final String projectIdTemplate, final String projectIdDestination,
			final boolean isNewProject) {
		final ProjectServiceJob projectServiceJob = (ProjectServiceJob) ContextStore.get().getBean("ProjectServiceJob");
		projectServiceJob.setJobStatus(this.status);
		projectServiceJob.copyTemplateProject(projectIdTemplate, projectIdDestination, isNewProject);
	}

	/** {@inheritDoc} */
	@Override
	public String cascadeAllTaskDependencies() {
		return new ProjectGanttHelper().cascadeAllTaskDependencies();
	}

	/** {@inheritDoc} */
	@Override
	public String cascadeProjectTaskDependencies(final String projectId) {
		String message = "";
		final DataSource actionDs = DataSourceFactory.createDataSourceForFields(ACTIVITYLOG_TABLE, ACTIVITYLOG_FIELDS);
		actionDs.addRestriction(Restrictions.eq(ACTIVITYLOG_TABLE, PROJECT_ID, projectId));
		final List<DataRecord> records = actionDs.getRecords();
		for (final DataRecord record : records) {
			final int actionId = record.getInt(ACTIVITYLOG_TABLE + DOT + ACTIVITY_LOG_ID);
			message += cascadeTaskDependencies(actionId) + "<br/>";
		}
		return message;
	}

	/** {@inheritDoc} */
	@Override
	public String cascadeTaskDependencies(final int activityLogId) {
		return new ProjectGanttHelper().cascadeTaskDependencies(activityLogId);
	}
}