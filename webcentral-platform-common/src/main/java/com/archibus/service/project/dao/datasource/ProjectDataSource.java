package com.archibus.service.project.dao.datasource;

import static com.archibus.service.project.impl.Constants.*;

import java.util.*;

import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.grouping.CalculatedFormulaFieldDef.SqlFormula;
import com.archibus.service.project.dao.IProjectDao;
import com.archibus.service.project.domain.Project;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * DataSource for project. Mapped to project database table.
 *
 * @author Radu Bunea
 * @since 23.2
 *
 */
public class ProjectDataSource extends ObjectDataSourceImpl<Project> implements IProjectDao<Project> {
	/**
	 * Constant : table name: "project".
	 */
	private static final String PROJECT = "project";

	/**
	 * Constant: field name: "project_id".
	 */
	private static final String PROJECT_ID = "project_id";

	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { PROJECT_ID, "projectId" },
			{ "project_name", "projectName" }, { "project_type", "projectType" }, { "date_start", "dateStart" },
			{ "date_end", "dateEnd" }, { "bl_id", "buildingId" }, { "site_id", "siteId" }, { REQUESTOR, "requestor" },
			{ "contact_id", "contactId" }, { SUMMARY, "summary" }, { DESCRIPTION, "description" },
			{ BENEFIT, "benefit" }, { SCOPE, "scope" }, { "duration_est", "durationEstimated" },
			{ "days_per_week", "daysPerWeek" }, { "cost_budget", "costBudgeted" }, { DOC, "documentBusinessCase" },
			{ "doc_risk_mgmt", "documentRiskManagement" }, { "doc_charter", "documentCharter" },
			{ "doc_impl_plan", "documentImplementationPlan" } };
	/**
	 * Constant: project code index counter. Used to generate project code.
	 */
	private static final int PRJ_CODE_INDEX_COUNTER = 6;
	/**
	 * Constant: project code index start. Used to generate project code.
	 */
	private static final int PRJ_CODE_INDEX_START = 5;
	/**
	 * Constant: project code index end. Used to generate project code.
	 */
	private static final int PRJ_CODE_INDEX_END = 11;

	/**
	 * Constructs ProjectDataSource, mapped to <code>project</code> table, using
	 * <code>project</code> bean.
	 */
	public ProjectDataSource() {
		super("capitalProject", PROJECT);
	}

	/** {@inheritDoc} */
	@Override
	public List<Project> getAllProjects() {
		final DataSource dataSource = this.createCopy();
		final List<DataRecord> records = dataSource.getRecords();

		return new DataSourceObjectConverter<Project>().convertRecordsToObjects(records, this.beanName,
				this.fieldToPropertyMapping, null);
	}

	/** {@inheritDoc} */
	@Override
	public DataRecord createProject(final DataRecord record) {
		final DataSource dataSource = this.createCopy();
		dataSource.setApplyVpaRestrictions(false);
		record.setValue(PROJECT + DOT + PROJECT_ID, generateProjectCode(dataSource));
		dataSource.saveRecord(record);
		return record;
	}

	/**
	 * Generate new Project Code using the following format: 2012-000001.
	 *
	 * @param projectDataSource
	 *            Project data source.
	 * @return generated project code.
	 */
	private String generateProjectCode(final DataSource projectDataSource) {
		final int year = Calendar.getInstance().get(Calendar.YEAR);
		String maxProjectForYear = null;
		if (SqlUtils.isOracle()) {
			maxProjectForYear = DataStatistics.getString(PROJECT_TABLE, PROJECT_ID, SqlFormula.MAX.toString(),
					" REGEXP_LIKE(project_id, '" + year + "-[0-9]{6}')");
		} else {
			maxProjectForYear = DataStatistics.getString(PROJECT_TABLE, PROJECT_ID, SqlFormula.MAX.toString(),
					" project_id LIKE '" + year + "-[0-9][0-9][0-9][0-9][0-9][0-9]'");
		}
		// if is null change to empty string
		if (maxProjectForYear == null) {
			maxProjectForYear = "";
		}
		final String clause = PROJECT_ID + Operation.EQUALS + SqlUtils.formatValueForSql(maxProjectForYear);
		projectDataSource.addRestriction(Restrictions.sql(clause));

		final DataRecord maxProjectRecord = projectDataSource.getRecord();
		String maxProjectCode = "";
		if (maxProjectRecord != null) {
			maxProjectCode = (String) maxProjectRecord.getValue(PROJECT_TABLE + DOT + PROJECT_ID);
		}
		projectDataSource.clearRestrictions();

		String counter = "";
		if ("".equals(maxProjectCode)) {
			counter = "000001";
		} else {
			counter = String
					.valueOf(Integer.parseInt(maxProjectCode.substring(PRJ_CODE_INDEX_START, PRJ_CODE_INDEX_END)) + 1);
			int length = counter.length();
			while (length < PRJ_CODE_INDEX_COUNTER) {
				counter = "0" + counter;
				length++;
			}
		}
		return year + HYPHEN + counter;
	}

	@Override
	protected String[][] getFieldsToProperties() {
		return FIELDS_TO_PROPERTIES.clone();
	}
}