/**
 * This package contains API and implementation of ComplianceSurveyMobileService
 * for mobile application Compliance Survey.
 */
package com.archibus.app.compliance.mobile.service.impl;