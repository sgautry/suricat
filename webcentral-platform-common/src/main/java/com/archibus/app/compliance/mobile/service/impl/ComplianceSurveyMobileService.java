package com.archibus.app.compliance.mobile.service.impl;

import static com.archibus.app.common.mobile.util.FieldNameConstantsCommon.*;
import static com.archibus.app.common.mobile.util.ServiceConstants.SQL_DOT;
import static com.archibus.app.common.mobile.util.TableNameConstants.*;

import java.util.*;

import org.springframework.util.StringUtils;

import com.archibus.app.common.mobile.util.DocumentsUtilities;
import com.archibus.app.common.questionnaire.impl.QuestionnaireExtService;
import com.archibus.app.compliance.mobile.service.IComplianceSurveyMobileService;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.ParsedRestrictionDef;
import com.archibus.utility.StringUtil;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides services for mobile application Compliance Survey.
 * <p>
 *
 * @author Ana Albu
 * @since 23.2
 *
 */
public class ComplianceSurveyMobileService implements IComplianceSurveyMobileService {

    /**
     * Constant: "COMPLIANCE - EVENT".
     */
    public static final String COMPLIANCE_EVENT = "COMPLIANCE - EVENT";

    /**
     * Constant: "COMPLETED".
     */
    public static final String COMPLETED = "COMPLETED";

    /**
     * Field names of the document type fields.
     */
    public static final String[] DOC_FIELD_NAMES = { DOC };

    /**
     * {@inheritDoc}.
     */
    @Override
    public void syncAnswers(final String userName) {
        copyAnswersFromMobile(userName);
        deleteAnswers(userName);
        deleteAnswersSync(userName);
        updateComplianceEventsTable(userName);

        // generate actions for Completed surveys
        generateActionsForCompletedSurveys(userName);

        populateEventsSyncTable(userName);
        copyAnswersFromWebCentral(userName);
        updateCompletionPercentage(userName);
    }

    /**
     * Copies the answers from the sync table to inventory table.
     *
     * @param userName the mobile user name validated from afm_users.user_name table
     */
    private void copyAnswersFromMobile(final String userName) {
        final DataSource datasource = DataSourceFactory
            .createDataSourceForFields(QUEST_ANSWER_EXT_TABLE, QUEST_ANSWER_EXT_FIELDS);

        final DataSource syncDS = DataSourceFactory
            .createDataSourceForFields(QUEST_ANSWER_EXT_SYNC_TABLE, QUEST_ANSWER_EXT_FIELDS);
        syncDS.addField(AUTO_NUMBER);
        syncDS.addField(MOB_LOCKED_BY);
        syncDS.addField(MOB_IS_CHANGED);
        syncDS.setContext();
        syncDS.setMaxRecords(0);

        final ParsedRestrictionDef syncRestriction = new ParsedRestrictionDef();
        syncRestriction.addClause(QUEST_ANSWER_EXT_SYNC_TABLE, MOB_LOCKED_BY, userName,
            Operation.EQUALS);
        syncRestriction.addClause(QUEST_ANSWER_EXT_SYNC_TABLE, MOB_IS_CHANGED, 1, Operation.EQUALS);

        final List<DataRecord> syncRecords = syncDS.getRecords(syncRestriction);

        for (final DataRecord syncRecord : syncRecords) {
            Integer answerId = 0;

            if (syncRecord.getValue(QUEST_ANSWER_EXT_SYNC_TABLE + SQL_DOT + ANSWER_ID) != null) {
                answerId = syncRecord.getInt(QUEST_ANSWER_EXT_SYNC_TABLE + SQL_DOT + ANSWER_ID);
            }

            DataRecord record = null;
            if (answerId > 0) {
                datasource.clearRestrictions();
                datasource
                    .addRestriction(Restrictions.eq(QUEST_ANSWER_EXT_TABLE, ANSWER_ID, answerId));
                record = datasource.getRecord();
            } else {
                record = datasource.createNewRecord();
            }

            if (record != null) {
                copySyncAnswerValues(syncRecord, record);

                record = datasource.saveRecord(record);

                DocumentsUtilities.copyDocuments(DOC_FIELD_NAMES, syncRecord, record);
            }
        }
    }

    /**
     * Copies field values from sync answer record to answer record.
     *
     * @param syncRecord source record
     * @param record destination record
     */
    private void copySyncAnswerValues(final DataRecord syncRecord, final DataRecord record) {
        for (final String fieldName : QUEST_ANSWER_EXT_FIELDS) {
            if (!ANSWER_ID.equalsIgnoreCase(fieldName)) {
                if (StringUtil.isNullOrEmpty(
                    syncRecord.getValue(QUEST_ANSWER_EXT_SYNC_TABLE + SQL_DOT + fieldName))) {
                    record.setValue(QUEST_ANSWER_EXT_TABLE + SQL_DOT + fieldName, "");
                } else {
                    record.setValue(QUEST_ANSWER_EXT_TABLE + SQL_DOT + fieldName,
                        syncRecord.getValue(QUEST_ANSWER_EXT_SYNC_TABLE + SQL_DOT + fieldName));
                }
            }
        }
    }

    /**
     * Copies the answers from the inventory table to the sync table.
     *
     * @param userName the mobile user name validated from afm_users.user_name table
     */
    private void copyAnswersFromWebCentral(final String userName) {
        // events datasource and restriction
        final String[] eventFieldNames =
                { AUTO_NUMBER, ACTIVITY_LOG_ID, ACTIVITY_TYPE, MOB_LOCKED_BY };
        final DataSource eventsDS = DataSourceFactory
            .createDataSourceForFields(ACTIVITY_LOG_SYNC_TABLE, eventFieldNames);
        eventsDS.setContext();
        eventsDS.setMaxRecords(0);
        final ParsedRestrictionDef eventsRestriction = new ParsedRestrictionDef();
        eventsRestriction.addClause(ACTIVITY_LOG_SYNC_TABLE, MOB_LOCKED_BY, userName,
            Operation.EQUALS);
        eventsRestriction.addClause(ACTIVITY_LOG_SYNC_TABLE, ACTIVITY_TYPE, COMPLIANCE_EVENT,
            Operation.EQUALS);

        // answers datasource
        final DataSource answersDatasource = DataSourceFactory
            .createDataSourceForFields(QUEST_ANSWER_EXT_TABLE, QUEST_ANSWER_EXT_FIELDS);
        answersDatasource.setContext();
        answersDatasource.setMaxRecords(0);

        // sync answers datasource
        final DataSource syncDS = DataSourceFactory
            .createDataSourceForFields(QUEST_ANSWER_EXT_SYNC_TABLE, QUEST_ANSWER_EXT_FIELDS);
        syncDS.addField(MOB_LOCKED_BY);
        syncDS.addField(GUID);

        final List<DataRecord> eventsRecords = eventsDS.getRecords(eventsRestriction);
        for (final DataRecord eventsRecord : eventsRecords) {
            answersDatasource.clearRestrictions();

            final ParsedRestrictionDef answersRestriction = new ParsedRestrictionDef();
            answersRestriction.addClause(QUEST_ANSWER_EXT_TABLE, SURVEY_EVENT_ID,
                eventsRecord.getInt(ACTIVITY_LOG_SYNC_TABLE + SQL_DOT + ACTIVITY_LOG_ID),
                Operation.EQUALS);

            final List<DataRecord> answersRecords =
                    answersDatasource.getRecords(answersRestriction);

            for (final DataRecord answerRecord : answersRecords) {
                DataRecord newSyncAnswerRecord = syncDS.createNewRecord();

                copyAnswerValues(userName, answerRecord, newSyncAnswerRecord);

                newSyncAnswerRecord = syncDS.saveRecord(newSyncAnswerRecord);

                DocumentsUtilities.copyDocuments(DOC_FIELD_NAMES, answerRecord,
                    newSyncAnswerRecord);
            }
        }

    }

    /**
     * Copies field values from answer record to answer sync record.
     *
     * @param userName the mobile user name validated from afm_users.user_name table
     * @param answerRecord source record
     * @param newSyncAnswerRecord destination record
     */
    private void copyAnswerValues(final String userName, final DataRecord answerRecord,
            final DataRecord newSyncAnswerRecord) {
        for (final String fieldName : QUEST_ANSWER_EXT_FIELDS) {
            if (StringUtil.isNullOrEmpty(
                answerRecord.getValue(QUEST_ANSWER_EXT_TABLE + SQL_DOT + fieldName))) {
                newSyncAnswerRecord.setValue(QUEST_ANSWER_EXT_SYNC_TABLE + SQL_DOT + fieldName, "");
            } else {
                newSyncAnswerRecord.setValue(QUEST_ANSWER_EXT_SYNC_TABLE + SQL_DOT + fieldName,
                    answerRecord.getValue(QUEST_ANSWER_EXT_TABLE + SQL_DOT + fieldName));
            }
        }
        newSyncAnswerRecord.setValue(QUEST_ANSWER_EXT_SYNC_TABLE + SQL_DOT + GUID,
            UUID.randomUUID().toString());
        newSyncAnswerRecord.setValue(QUEST_ANSWER_EXT_SYNC_TABLE + SQL_DOT + MOB_LOCKED_BY,
            userName);
    }

    /**
     *
     * Updates the status of the Compliance Events records from the changed Compliance Events Sync
     * records.
     * <p>
     * Use case: user sets the survey as Completed on mobile.
     *
     * @param userName the mobile user name validated from afm_users.user_name table
     */
    @SuppressWarnings("PMD.AvoidUsingSql")
    private void updateComplianceEventsTable(final String userName) {
        final String sqlFormattedUserName = SqlUtils.formatValueForSql(userName);

        final String syncSelect = String.format(
            "SELECT status from activity_log_sync"
                    + " WHERE activity_log_sync.activity_log_id = activity_log.activity_log_id"
                    + " AND activity_log_sync.activity_type = 'COMPLIANCE - EVENT'"
                    + " AND mob_is_changed = 1 AND activity_log_sync.mob_locked_by = %s",
            sqlFormattedUserName);
        final String sql = String.format("UPDATE activity_log SET status = (" + syncSelect + ") "
                + " WHERE (" + syncSelect + ") IS NOT NULL",
            sqlFormattedUserName);
        SqlUtils.executeUpdate(ACTIVITY_LOG_SYNC_TABLE, sql);
    }

    /**
     * Searches the newly Completed surveys and calls the Web Central WFR that generates actions
     * corresponding to the answers of type 'action' for these surveys.
     *
     * @param userName the mobile user name validated from afm_users.user_name table
     */
    private void generateActionsForCompletedSurveys(final String userName) {
        final QuestionnaireExtService questExtService = new QuestionnaireExtService();

        final String[] syncEventFieldNames = { AUTO_NUMBER, ACTIVITY_LOG_ID, ACTIVITY_TYPE, STATUS,
                MOB_LOCKED_BY, MOB_IS_CHANGED, QUESTIONNAIRE_ID_EXT };
        final DataSource syncEventsDS = DataSourceFactory
            .createDataSourceForFields(ACTIVITY_LOG_SYNC_TABLE, syncEventFieldNames);
        syncEventsDS.setContext();
        syncEventsDS.setMaxRecords(0);

        final ParsedRestrictionDef syncEventsRestriction = new ParsedRestrictionDef();
        syncEventsRestriction.addClause(ACTIVITY_LOG_SYNC_TABLE, MOB_LOCKED_BY, userName,
            Operation.EQUALS);
        syncEventsRestriction.addClause(ACTIVITY_LOG_SYNC_TABLE, MOB_IS_CHANGED, 1,
            Operation.EQUALS);
        syncEventsRestriction.addClause(ACTIVITY_LOG_SYNC_TABLE, ACTIVITY_TYPE, COMPLIANCE_EVENT,
            Operation.EQUALS);
        syncEventsRestriction.addClause(ACTIVITY_LOG_SYNC_TABLE, STATUS, COMPLETED,
            Operation.EQUALS);

        final List<DataRecord> syncEventsRecords = syncEventsDS.getRecords(syncEventsRestriction);
        for (final DataRecord syncEventsRecord : syncEventsRecords) {
            questExtService.processQuestionnaireAnswers(
                syncEventsRecord.getInt(ACTIVITY_LOG_SYNC_TABLE + SQL_DOT + QUESTIONNAIRE_ID_EXT),
                syncEventsRecord.getInt(ACTIVITY_LOG_SYNC_TABLE + SQL_DOT + ACTIVITY_LOG_ID));
        }
    }

    /**
     * Populate activity_log_sync table based on activity_log and regrequirement records for
     * conditions:
     * <p>
     * a. The action represents a compliance event (activity_type = 'COMPLIANCE - EVENT' )
     * <p>
     * b. They have a questionnaire assigned to them, meaning: The action item is assigned to a
     * compliance requirement and the requirement has a questionnaire assigned to it.
     * <p>
     * c. The assigned questionnaire is active
     * <p>
     * d. The action item status is not Rejected, Completed, Closed or Stopped >>
     * activity_log.status NOT IN
     * ('REJECTED';'CANCELLED;STOPPED';'COMPLETED';'COMPLETED-V';'CLOSED')
     * <p>
     * e. The action item is assigned to the inspector (activity_log.manager = employee name of for
     * the inspector's user)
     * <p>
     * Then sync the quest_answer_ext_sync table.
     * <p>
     * 1. Deletes from the activity_log_sync table any records that already exist for that user.
     * <p>
     * 2. Copies all activity_log and regrequirement records to the activity_log_sync table.
     * <p>
     *
     * <p>
     * Suppress PMD warning "AvoidUsingSql" in this method.
     * <p>
     * Justification: Case #2: Statement with INSERT ... SELECT pattern.
     *
     * @param userName the mobile user name validated from afm_users.user_name table
     */
    @SuppressWarnings("PMD.AvoidUsingSql")
    private void populateEventsSyncTable(final String userName) {
        final String sqlFormattedUserName = SqlUtils.formatValueForSql(userName);

        String sql = String.format("DELETE FROM activity_log_sync WHERE mob_locked_by = %s",
            sqlFormattedUserName);
        SqlUtils.executeUpdate(ACTIVITY_LOG_SYNC_TABLE, sql);

        final String activityLogFields =
                generateFieldNames(ACTIVITY_LOG_TABLE, ACTIVITY_LOG_FIELDS_COMPLIANCE);
        final String reqrequirementFields =
                generateFieldNames(REGREQUIREMENT_TABLE, REGREQUIREMENT_FIELDS_COMPLIANCE);
        final String activityLogSyncFields = generateFieldNames(ACTIVITY_LOG_SYNC_TABLE,
            ACTIVITY_LOG_FIELDS_COMPLIANCE) + ","
                + generateFieldNames(ACTIVITY_LOG_SYNC_TABLE, REGREQUIREMENT_FIELDS_COMPLIANCE);

        final String managerSql = String.format(
            "(SELECT %s em_id FROM em WHERE em.email ="
                    + " (SELECT email FROM afm_users WHERE user_name = %s) %s) ",
            SqlUtils.isSybase() ? "FIRST" : SqlUtils.isSqlServer() ? "TOP 1" : "",
            sqlFormattedUserName,
            SqlUtils.isSybase() ? "ORDER BY em_id" : SqlUtils.isOracle() ? " AND ROWNUM = 1" : "");

        sql = String.format(
            "INSERT INTO activity_log_sync(%s, questionnaire_id_ext, mob_locked_by) "
                    + "SELECT %s, %s, regrequirement.questionnaire_id, %s FROM activity_log INNER JOIN regrequirement "
                    + "ON activity_log.regulation = regrequirement.regulation AND activity_log.reg_program = regrequirement.reg_program "
                    + "AND activity_log.reg_requirement = regrequirement.reg_requirement "
                    + "WHERE activity_log.activity_type = 'COMPLIANCE - EVENT' "
                    + "AND activity_log.status NOT IN ('REJECTED','CANCELLED','STOPPED','COMPLETED','COMPLETED-V','CLOSED') "
                    + "AND activity_log.manager = " + managerSql
                    + "AND regrequirement.questionnaire_id IS NOT NULL "
                    + "AND regrequirement.questionnaire_id IN "
                    + "(SELECT questionnaire_ext.questionnaire_id FROM questionnaire_ext WHERE questionnaire_ext.status = 'active')",
            activityLogSyncFields, activityLogFields, reqrequirementFields, sqlFormattedUserName);
        SqlUtils.executeUpdate(ACTIVITY_LOG_SYNC_TABLE, sql);
    }

    /**
     * Updates the Completion Percentage of the questionnaires.
     *
     * <p>
     * Suppress PMD warning "AvoidUsingSql" in this method.
     * <p>
     * Justification: Case #2: Statement with INSERT ... SELECT pattern.
     *
     * @param userName the mobile user name validated from afm_users.user_name table
     */
    @SuppressWarnings("PMD.AvoidUsingSql")
    private void updateCompletionPercentage(final String userName) {
        final String sqlFormattedUserName = SqlUtils.formatValueForSql(userName);
        final String parentQuestionsSql =
                "(SELECT COUNT(*) FROM quest_question_ext LEFT JOIN question_ext ON quest_question_ext.question_id=question_ext.question_id "
                        + "WHERE quest_question_ext.questionnaire_id ="
                        + (SqlUtils.isOracle() ? " als" : " activity_log_sync")
                        + ".questionnaire_id_ext AND question_ext.question_type<>'label')";
        final String answersSql = String.format(
            "(SELECT COUNT(*) FROM ("
                    + " SELECT DISTINCT quest_answer_ext_sync.survey_event_id, quest_answer_ext_sync.questionnaire_id, quest_answer_ext_sync.question_id"
                    + " FROM quest_answer_ext_sync"
                    + " LEFT JOIN quest_question_ext ON quest_question_ext.question_id=quest_answer_ext_sync.question_id"
                    + " LEFT JOIN question_ext ON question_ext.question_id=quest_question_ext.question_id"
                    + " LEFT JOIN activity_log_sync ON activity_log_sync.activity_log_id = quest_answer_ext_sync.survey_event_id"
                    + " WHERE quest_answer_ext_sync.survey_event_id = activity_log_sync.activity_log_id"
                    + "  AND activity_log_sync.mob_locked_by = %s"
                    + "  AND ((question_ext.question_type<>'label' AND question_ext.question_type<>'document')"
                    + "       OR (question_ext.question_type='document' AND quest_answer_ext_sync.doc IS NOT NULL))"
                    + ") ${sql.as} temp_answers" + " WHERE temp_answers.survey_event_id = "
                    + (SqlUtils.isOracle() ? "als" : "activity_log_sync") + ".activity_log_id)",
            sqlFormattedUserName);
        final String sql = String.format(
            "UPDATE activity_log_sync" + (SqlUtils.isOracle() ? " ${sql.as} als" : "")
                    + " SET pct_complete = CASE  WHEN %s = 0 THEN 0 ELSE FLOOR( %s *100 / %s) END ",
            answersSql, answersSql, parentQuestionsSql);
        SqlUtils.executeUpdate(ACTIVITY_LOG_SYNC_TABLE, sql);
    }

    /**
     * Returns a list containing the full field names of the fields.
     *
     * @param tableName table name
     * @param fields list of field names
     * @return comma delimited list of fields.
     */
    private static String generateFieldNames(final String tableName, final String[] fields) {
        final List<String> fieldNames = new ArrayList<String>();
        for (final String field : fields) {
            fieldNames.add(tableName + "." + field);
        }

        return StringUtils.collectionToCommaDelimitedString(fieldNames);
    }

    /**
     *
     * Deletes the answers marked as deleted on mobile.
     *
     * @param userName the mobile user name validated from afm_users.user_name table
     */
    @SuppressWarnings("PMD.AvoidUsingSql")
    private void deleteAnswers(final String userName) {
        final String sqlFormattedUserName = SqlUtils.formatValueForSql(userName);

        final String syncSelect = String.format("SELECT 1 from quest_answer_ext_sync"
                + " WHERE quest_answer_ext_sync.answer_id = quest_answer_ext.answer_id"
                + " AND quest_answer_ext_sync.deleted=1"
                + " AND quest_answer_ext_sync.mob_is_changed = 1"
                + " AND quest_answer_ext_sync.mob_locked_by = %s",
            sqlFormattedUserName);

        final String sql =
                String.format("DELETE FROM quest_answer_ext WHERE EXISTS(" + syncSelect + ")");

        SqlUtils.executeUpdate(QUEST_ANSWER_EXT_TABLE, sql);

        // delete the correspondent documents
        deleteOrphanedAnswerDocuments();
    }

    /**
     * Delete answers from the sync table.
     *
     * *
     * <p>
     * Suppress PMD warning "AvoidUsingSql" in this method.
     * <p>
     * Justification: Case #2: Statements with DELETE FROM ... * pattern.
     *
     * @param userName the mobile user name validated from afm_users.user_name table
     */
    @SuppressWarnings("PMD.AvoidUsingSql")
    private void deleteAnswersSync(final String userName) {
        final String sql =
                String.format("DELETE FROM quest_answer_ext_sync WHERE mob_locked_by=%s ",
                    SqlUtils.formatValueForSql(userName));

        SqlUtils.executeUpdate(QUEST_ANSWER_EXT_SYNC_TABLE, sql);

        // delete the correspondent documents
        deleteOrphanedAnswerSyncDocuments(userName);
    }

    /**
     *
     * Deletes records from the document management tables that no longer have an associated record
     * in the QUEST_ANSWER_EXT_SYNC_TABLE table for the user.
     *
     * @param userName User name
     */
    @SuppressWarnings("PMD.AvoidUsingSql")
    private static void deleteOrphanedAnswerSyncDocuments(final String userName) {
        final String userRestriction =
                String.format("mob_locked_by=%s ", SqlUtils.formatValueForSql(userName));

        final String sql = "DELETE FROM %s WHERE table_name='" + QUEST_ANSWER_EXT_SYNC_TABLE + "' "
                + "AND NOT EXISTS(SELECT 1 FROM " + QUEST_ANSWER_EXT_SYNC_TABLE + " WHERE "
                + userRestriction + " AND " + QUEST_ANSWER_EXT_SYNC_TABLE + SQL_DOT + AUTO_NUMBER
                + " = %s" + SQL_DOT + "pkey_value)";

        String sqlStatement = String.format(sql, AFM_DOCVERS_TABLE, AFM_DOCVERS_TABLE);
        SqlUtils.executeUpdate(AFM_DOCVERS_TABLE, sqlStatement);

        sqlStatement = String.format(sql, AFM_DOCVERSARCH_TABLE, AFM_DOCVERSARCH_TABLE);
        SqlUtils.executeUpdate(AFM_DOCVERSARCH_TABLE, sqlStatement);

        sqlStatement = String.format(sql, AFM_DOCS_TABLE, AFM_DOCS_TABLE);
        SqlUtils.executeUpdate(AFM_DOCS_TABLE, sqlStatement);
    }

    /**
     *
     * Deletes records from the document management tables that no longer have an associated record
     * in the QUEST_ANSWER_EXT_TABLE table.
     */
    @SuppressWarnings("PMD.AvoidUsingSql")
    private static void deleteOrphanedAnswerDocuments() {
        final String sql = "DELETE FROM " + "%s WHERE table_name='" + QUEST_ANSWER_EXT_TABLE + "'"
                + " AND NOT EXISTS(SELECT 1 FROM " + QUEST_ANSWER_EXT_TABLE + "  WHERE "
                + QUEST_ANSWER_EXT_TABLE + SQL_DOT + ANSWER_ID + "  = %s" + SQL_DOT
                + "pkey_value) ";

        String sqlStatement = String.format(sql, AFM_DOCVERS_TABLE, AFM_DOCVERS_TABLE);
        SqlUtils.executeUpdate(AFM_DOCVERS_TABLE, sqlStatement);

        sqlStatement = String.format(sql, AFM_DOCVERSARCH_TABLE, AFM_DOCVERSARCH_TABLE);
        SqlUtils.executeUpdate(AFM_DOCVERSARCH_TABLE, sqlStatement);

        sqlStatement = String.format(sql, AFM_DOCS_TABLE, AFM_DOCS_TABLE);
        SqlUtils.executeUpdate(AFM_DOCS_TABLE, sqlStatement);
    }
}
