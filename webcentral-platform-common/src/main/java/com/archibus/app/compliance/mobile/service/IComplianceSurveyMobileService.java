package com.archibus.app.compliance.mobile.service;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * API of the service for mobile application Compliance Survey.
 * <p>
 * Only authenticated users are allowed to invoke methods in this service. Interface to be
 * implemented by classes that facilitate services for mobile application Compliance Survey.
 *
 * @author Ana Albu
 * @since 23.2
 */
public interface IComplianceSurveyMobileService {

    /**
     *
     * Syncs mobile data with Web Central.
     * <p>
     * 1. Copies answers from sync tables to the inventory tables
     * <p>
     * 2. Deletes answers from sync tables
     * <p>
     * 3. Populates sync tables (questionnaires and answers) with data from the inventory tables
     *
     * @param userName User name
     */
    void syncAnswers(final String userName);
}