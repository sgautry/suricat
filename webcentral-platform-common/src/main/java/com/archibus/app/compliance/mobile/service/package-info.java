/**
 * This package contains API of ComplianceSurveyMobileService for Compliance
 * Mobile Survey application.
 **/
package com.archibus.app.compliance.mobile.service;