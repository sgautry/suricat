package com.archibus.app.common.bldgops.dao.datasource;

import com.archibus.app.common.bldgops.dao.IWorkRequestDao;
import com.archibus.app.common.bldgops.domain.WorkRequest;
import com.archibus.datasource.ObjectDataSourceImpl;

/**
 * DataSource for Work Request.
 *
 * @see ObjectDataSourceImpl.
 *
 * @author Zhang Yi
 *
 */
public class WorkRequestDataSource extends ObjectDataSourceImpl<WorkRequest> implements IWorkRequestDao {

	/**
	 * Constant: table name: "wr_log".
	 */
	public static final String ACTIVITY_LOG = "activity_log";

	/**
	 * Constant: field name: "activity_log_id".
	 */
	public static final String ACTIVITY_LOG_ID = "activity_log_id";

	/**
	 * Constant: field name: "action_title".
	 */
	public static final String ACTION_TITLE = "action_title";

	/**
	 * Constant: field name: "comments".
	 */
	public static final String COMMENTS = "comments";

	/**
	 * Constant: field name: "description".
	 */
	public static final String DESCRIPTION = "description";

	/**
	 * Constant: field name: "doc4".
	 */
	public static final String DOC4 = "doc4";

	/**
	 * Constant: domain object's property name: "item_id".
	 */
	public static final String ITEM_ID = "item_id";

	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { ACTIVITY_LOG_ID, "actionItemId" }, { "wr_id", "id" },
			{ "assigned_to", "assignedTo" }, { "assessment_id", "assessmentId" }, { "activity_type", "activityType" },
			{ "bl_id", "buildingId" }, { "fl_id", "floorId" }, { "rm_id", "roomId" }, { "site_id", "siteId" },
			{ "prob_type", "problemType" }, { "date_required", "dateRequired" }, { "date_scheduled", "dateScheduled" },
			{ "cost_estimated", "costEstimated" }, { "hours_est_baseline", "hoursEstBaseline" },
			{ "dv_id", "divisionId" }, { "dp_id", "departmentId" }, { "phone_requestor", "phoneRequestor" },
			{ "ac_id", "accountId" }, { "priority", "priority" }, { "created_by", "createdBy" },
			{ "requestor", "requestor" }, { "description", "description" } };

	/**
	 * Constructs WorkRequestDataSource, mapped to <code>wr</code> table, using
	 * <code>WorkRequest</code> bean.
	 */
	public WorkRequestDataSource() {
		super("workRequest", ACTIVITY_LOG);
	}

	@Override
	protected String[][] getFieldsToProperties() {
		// TODO Auto-generated method stub
		return this.FIELDS_TO_PROPERTIES;
	}

}
