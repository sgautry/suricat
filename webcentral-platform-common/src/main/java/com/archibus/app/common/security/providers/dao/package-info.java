/**
 * This package contains APIs and implementations of security providers DAOs.
 **/
package com.archibus.app.common.security.providers.dao;