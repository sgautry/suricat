package com.archibus.app.common.security.providers.dao;

import org.springframework.security.authentication.*;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;

import com.archibus.context.DatabaseRole;
import com.archibus.security.*;
import com.archibus.utility.CoreUserSessionTemplate;

/**
 * Provides additionalAuthenticationChecks method that handles situation when cached UserAccount has
 * stale password in clustered deployment.
 * <p>
 * Provides user account locking behavior.
 * <p>
 * Used by afm_users security configuration to:
 * <p>
 * - re-read password after failed password validation.
 * <p>
 * - lock user account if number of failed login attempts exceeds allowed.
 * <p>
 * Managed by Spring, has singleton scope.
 *
 * @author Valery Tydykov
 * @since 23.2
 *
 */
public class RetryingDaoAuthenticationProvider extends DaoAuthenticationProvider {
    /**
     * Constant: assert error message.
     */
    private static final String USER_ACCOUNT_MUST_BE_SUPPLIED_IN_USER_DETAILS_IMPL =
            "userAccount must be supplied in UserDetailsImpl";

    /**
     * Property: cachedUserAccountService.
     */
    private CachedUserAccountService cachedUserAccountService;

    /**
     * Getter for the cachedUserAccountService property.
     *
     * @return the cachedUserAccountService property.
     */
    public CachedUserAccountService getCachedUserAccountService() {
        return this.cachedUserAccountService;
    }

    /**
     * Setter for the cachedUserAccountService property.
     *
     * @param cachedUserAccountService the cachedUserAccountService to set.
     */
    public void setCachedUserAccountService(
            final CachedUserAccountService cachedUserAccountService) {
        this.cachedUserAccountService = cachedUserAccountService;
    }

    @Override
    protected void additionalAuthenticationChecks(final UserDetails userDetails,
            final UsernamePasswordAuthenticationToken authentication)
            throws AuthenticationException {
        try {
            super.additionalAuthenticationChecks(userDetails, authentication);
            clearFailedLoginsCounter(userDetails);
        } catch (final BadCredentialsException e) {
            incrementFailedLoginsCounter(userDetails);

            // flush UserAccount and re-try authentication (will force reading password from
            // afm_users table )
            this.cachedUserAccountService.flushUserAccount(userDetails.getUsername());
            // try to authenticate again
            super.additionalAuthenticationChecks(userDetails, authentication);
            clearFailedLoginsCounter(userDetails);
        }
    }

    /**
     * Increments counter of failed logins in the userDetails and in the database.
     *
     * @param userDetails that contains the counter.
     * @throws LockedException if number of failed login attempts exceeds allowed.
     */
    private void incrementFailedLoginsCounter(final UserDetails userDetails)
            throws LockedException {
        final UserAccount.ThreadSafe userAccount =
                (UserAccount.ThreadSafe) ((UserDetailsImpl) userDetails).getUserAccount();
        Assert.notNull(userAccount, USER_ACCOUNT_MUST_BE_SUPPLIED_IN_USER_DETAILS_IMPL);

        try {
            // increment failed logins counter
            userAccount.incrementFailedLoginsCounter();
            saveFailedLoginsCounter(userAccount);

            if (!userDetails.isAccountNonLocked()) {
                // lock user account
                throw new LockedException("User account is locked");
            }
        } finally {
            // Destroy cashed UserAccount object, to allow unlocked user account to be re-loaded.
            destroyCashedUserAccount(userAccount);
        }
    }

    /**
     * Clears failed logins counter in the userDetails and in the database.
     *
     * @param userDetails that contains the counter.
     */
    private void clearFailedLoginsCounter(final UserDetails userDetails) {
        final UserAccount.ThreadSafe userAccount =
                (UserAccount.ThreadSafe) ((UserDetailsImpl) userDetails).getUserAccount();
        Assert.notNull(userAccount, USER_ACCOUNT_MUST_BE_SUPPLIED_IN_USER_DETAILS_IMPL);

        clearFailedLoginsCounter(userAccount);
    }

    /**
     * Clears failed logins counter in the userAccount, saves to the database.
     *
     * @param userAccount that contains the counter.
     */
    private void clearFailedLoginsCounter(final UserAccount.ThreadSafe userAccount) {
        // clear failed logins counter
        userAccount.setNumberFailedLoginAttempts(0);

        saveFailedLoginsCounter(userAccount);
    }

    /**
     * Saves failed logins counter that is a property of userAccount to the database.
     *
     * @param userAccount that contains the counter.
     */
    private void saveFailedLoginsCounter(final UserAccount.ThreadSafe userAccount) {
        // prepare core UserSession context
        final CoreUserSessionTemplate template = new CoreUserSessionTemplate();
        // use SECURITY database
        template.setDatabaseRole(DatabaseRole.SECURITY);
        template.execute(new Runnable() {
            @Override
            public void run() {
                // save failed logins counter
                UserAccountLoaderImpl.saveUserAccount(userAccount);
            }
        });
    }

    /**
     * Destroys cashed UserAccount object.
     *
     * @param userAccount to be destroyed.
     */
    private void destroyCashedUserAccount(final UserAccount.ThreadSafe userAccount) {
        ((UserAccountImpl) userAccount).destroy();
    }
}
