package com.archibus.app.common.util;

import java.text.MessageFormat;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import com.archibus.config.ConfigManager;
import com.archibus.eventhandler.EventHandlerBase;
import com.archibus.utility.*;

/**
 * Provides methods for preparing localized message, preparing SMTP server parameters and sending
 * email.
 *
 * @author Valery Tydykov
 * @since 23.2
 *
 */
public class MailSenderService implements IMailSenderService {
    /**
     * Constant: typical value for content-type.
     */
    private static final String TEXT_PLAIN_CHARSET_UTF_8 = "text/plain; charset=UTF-8";

    /**
     * Logger for this class and subclasses.
     */
    protected final Logger logger = Logger.getLogger(this.getClass());

    /**
     * Property: configManager. Required for getting configuration settings and localizing messages.
     */
    private ConfigManager.Immutable configManager;

    // TODO EmailSender is prototype-scoped
    // optional
    /**
     * Property: mailSender. Required for sending email.
     */
    private MailSender mailSender;

    /**
     * Formats text message according to locale.
     *
     * @param message Text message to be formatted.
     * @param args objects to be injected into formatted message.
     * @param locale to be used for formatting.
     * @return Formatted message.
     */
    public static String formatMessage(final String message, final Object[] args,
            final Locale locale) {
        final MessageFormat formatter = new MessageFormat(message);
        formatter.setLocale(locale);

        return formatter.format(args);
    }

    /**
     * Getter for the configManager property.
     *
     * @return the configManager property.
     */
    public ConfigManager.Immutable getConfigManager() {
        return this.configManager;
    }

    /**
     * Getter for the mailSender property.
     *
     * @return the mailSender property.
     */
    public MailSender getMailSender() {
        return this.mailSender;
    }

    @Override
    public String localizeMessage(final String className, final String message,
            final Locale locale) {
        final boolean translatablePrefix = StringUtil
            .toBoolean(this.configManager.getAttribute(Utility.XPATH_TRANSLATABLE_PREFIX));

        final String localizedMessage = this.configManager.loadLocalizedString(className, "",
            message, locale, translatablePrefix);

        return localizedMessage;
    }

    @Override
    public void prepareAndSendEmail(final EmailParameters emailParameters) {
        final String host = this.configManager.getAttribute(EventHandlerBase.PREFERENCES_MAIL_HOST);
        final String port =
                this.configManager.getAttribute(EventHandlerBase.PREFERENCES_MAIL_HOST_PORT);
        final String userId =
                this.configManager.getAttribute(EventHandlerBase.PREFERENCES_MAIL_HOST_USER_ID);
        final String password =
                this.configManager.getAttribute(EventHandlerBase.PREFERENCES_MAIL_HOST_PASSWORD);

        sendEmail(host, port, userId, password, emailParameters);
    }

    @Override
    public String prepareMessage(final String className, final String message, final Object[] args,
            final Locale locale) {
        final String localizedMessage = localizeMessage(className, message, locale);
        final String formattedMessage = formatMessage(localizedMessage, args, locale);

        return formattedMessage;
    }

    /**
     * Setter for the configManager property.
     *
     * @param configManager the configManager to set.
     */
    public void setConfigManager(final ConfigManager.Immutable configManager) {
        this.configManager = configManager;
    }

    /**
     * Setter for the mailSender property.
     *
     * @param mailSender the mailSender to set.
     */
    public void setMailSender(final MailSender mailSender) {
        this.mailSender = mailSender;
    }

    /**
     * Sends email.
     *
     * @param host SMTP server host address.
     * @param port SMTP server port.
     * @param userId SMTP server userId.
     * @param password SMTP server password.
     * @param emailParameters parameters of email message.
     */
    protected void sendEmail(final String host, final String port, final String userId,
            final String password, final EmailParameters emailParameters) {
        if (this.logger.isDebugEnabled()) {
            this.logger.debug("Sending email=[" + emailParameters + "]");
        }

        Assert.hasLength(host, "sendEmail: host must be supplied");
        Assert.hasLength(emailParameters.getFrom(), "sendEmail: from must be supplied");
        Assert.hasLength(emailParameters.getTo(), "sendEmail: to must be supplied");

        final MailMessage message = new MailMessage();
        message.setActivityId("AbSystemAdministration");
        message.setFrom(emailParameters.getFrom());
        message.setTo(emailParameters.getTo());
        message.setHost(host);
        message.setPort(port);
        message.setSubject(emailParameters.getSubject());
        message.setText(emailParameters.getBody());
        message.setContentType(TEXT_PLAIN_CHARSET_UTF_8);
        message.setCc(emailParameters.getCc());
        message.setBcc(emailParameters.getBcc());
        message.setUser(userId);
        message.setPassword(password);

        // if attachments specified, add them
        if (emailParameters.getAttachmentFileNames() != null) {
            for (final String fileName : emailParameters.getAttachmentFileNames()) {
                message.addFileAttachment(fileName);
            }
        }

        this.mailSender.send(message);
    }
}
