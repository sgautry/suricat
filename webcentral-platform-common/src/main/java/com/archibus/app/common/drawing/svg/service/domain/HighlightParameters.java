package com.archibus.app.common.drawing.svg.service.domain;

import java.util.Map;

/**
 * Domain class for highlight parameters.
 * <p>
 * Mapped to active_plantypes table.
 *
 * <p>
 * Designed to have prototype scope.
 *
 * @author shao
 * @since 21.1
 *
 */
public class HighlightParameters extends LabelParameters {
	/**
	 * the name of the axvw view.
	 * <p>
	 * In view, highlight and label dataSources are defined.
	 */
	private String viewName;

	/**
	 * the id of highlight DataSource.
	 */
	private String highlightDatasourceId;

	/**
	 * the type of the highlight asset.
	 */
	private String assetType;

	/**
	 * Hide assets without being highlighted.
	 */
	private boolean hideNotHighlightedAssets;

	/**
	 * query restriction.
	 */
	private String restriction;

	/**
	 * dataSource parameters.
	 */
	private Map<String, Object> dataSourceParameters;

	/**
	 * enable Hatch patterns. TODO:
	 */
	private boolean enableHatchPatterns = true;

	/**
	 * Is BorderHigh lighting.
	 */
	private boolean isBorderHighlighting;

	/**
	 * borderSize.
	 */
	private int borderSize = 20;

	/**
	 * labelDatasourceViewName.
	 */
	private String labelDatasourceViewName;

	/**
	 * Asset selection Datasource Id
	 */
	private String selectionDatasourceId;

	/**
	 * Getter for the highlightDatasourceId property.
	 *
	 * @see highlightDatasourceId
	 * @return the highlightDatasourceId property.
	 */
	public String getHighlightDatasourceId() {
		return this.highlightDatasourceId;
	}

	/**
	 * Setter for the highlightDatasourceId property.
	 *
	 * @see datasourceId
	 * @param datasourceId
	 *            the highlightDatasourceId to set
	 */

	public void setHighlightDatasourceId(final String datasourceId) {
		this.highlightDatasourceId = datasourceId;
	}

	/**
	 * Getter for the assetType property.
	 *
	 * @see assetType.
	 * @return the assetType property.
	 */
	public String getAssetType() {
		return this.assetType;
	}

	/**
	 * Setter for the assetType property.
	 *
	 * @see assetType.
	 * @param assetType
	 *            the assetType to set.
	 */
	public void setAssetType(final String assetType) {
		this.assetType = assetType;
	}

	/**
	 * Getter for the viewName property.
	 *
	 * @see viewName
	 * @return the viewName property.
	 */
	public String getViewName() {
		return this.viewName;
	}

	/**
	 * Setter for the viewName property.
	 *
	 * @see viewName
	 * @param viewName
	 *            the viewName to set
	 */
	public void setViewName(final String viewName) {
		this.viewName = viewName;
	}

	/**
	 * Getter for the hideNotHighlightedAssets property.
	 *
	 * @see hideNotHighlightedAssets
	 * @return the hideNotHighlightedAssets property.
	 */
	public boolean isHideNotHighlightedAssets() {
		return this.hideNotHighlightedAssets;
	}

	/**
	 * Setter for the hideNotHighlightedAssets property.
	 *
	 * @see hideNotHighlightedAssets
	 * @param hideNotHighlightedAssets
	 *            the hideNotHighlightedAssets to set
	 */

	public void setHideNotHighlightedAssets(final boolean hideNotHighlightedAssets) {
		this.hideNotHighlightedAssets = hideNotHighlightedAssets;
	}

	/**
	 *
	 * Gets Restriction.
	 *
	 * @return string.
	 */
	public String getRestriction() {
		return this.restriction;
	}

	/**
	 *
	 * Sets Restriction.
	 *
	 * @param restriction
	 *            string.
	 */
	public void setRestriction(final String restriction) {
		this.restriction = restriction;
	}

	/**
	 *
	 * Gets DataSourceParameters.
	 *
	 * @return Map<String, Object> .
	 */
	public Map<String, Object> getDataSourceParameters() {
		return this.dataSourceParameters;
	}

	/**
	 *
	 * Sets DataSourceParameters.
	 *
	 * @param dataSourceParameters
	 *            Map<String, Object>.
	 */
	public void setDataSourceParameters(final Map<String, Object> dataSourceParameters) {
		this.dataSourceParameters = dataSourceParameters;
	}

	/**
	 * Getter for the enableHatchPatterns property.
	 *
	 * @return the enableHatchPatterns property.
	 */
	public boolean isEnableHatchPatterns() {
		return this.enableHatchPatterns;
	}

	/**
	 * Setter for the enableHatchPatterns property.
	 *
	 * @param enableHatchPatterns
	 *            the enableHatchPatterns to set.
	 */
	public void setEnableHatchPatterns(final boolean enableHatchPatterns) {
		this.enableHatchPatterns = enableHatchPatterns;
	}

	/**
	 * Getter for the isBorderHighlighting property.
	 *
	 * @return the isBorderHighlighting property.
	 */
	public boolean isBorderHighlighting() {
		return this.isBorderHighlighting;
	}

	/**
	 * Setter for the isBorderHighlighting property.
	 *
	 * @param isBorderHighlighting
	 *            the isBorderHighlighting to set.
	 */
	public void setBorderHighlighting(final boolean isBorderHighlighting) {
		this.isBorderHighlighting = isBorderHighlighting;
	}

	/**
	 * Getter for the borderSize property.
	 *
	 * @return the borderSize property.
	 */
	public int getBorderSize() {
		return this.borderSize;
	}

	/**
	 * Setter for the borderSize property.
	 *
	 * @param borderSize
	 *            the borderSize to set.
	 */
	public void setBorderSize(final int borderSize) {
		this.borderSize = borderSize;
	}

	/**
	 * Getter for the labelDatasourceViewName property.
	 *
	 * @return the labelDatasourceViewName property.
	 */
	public String getLabelDatasourceViewName() {
		return this.labelDatasourceViewName;
	}

	/**
	 * Setter for the labelDatasourceViewName property.
	 *
	 * @param labelDatasourceViewName
	 *            the labelDatasourceViewName to set.
	 */
	public void setLabelDatasourceViewName(final String labelDatasourceViewName) {
		this.labelDatasourceViewName = labelDatasourceViewName;
	}

	/**
	 * Getter for the selectionDatasourceId property.
	 *
	 * @return the selectionDatasourceId property.
	 */
	public String getSelectionDatasourceId() {
		return this.selectionDatasourceId;
	}

	/**
	 * Setter for the selectionDatasourceId property.
	 *
	 * @param selectionDatasourceId
	 *            the selectionDatasourceId to set.
	 */
	public void setSelectionDatasourceId(final String selectionDatasourceId) {
		this.selectionDatasourceId = selectionDatasourceId;
	}

}
