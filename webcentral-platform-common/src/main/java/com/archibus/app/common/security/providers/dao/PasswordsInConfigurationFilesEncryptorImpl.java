package com.archibus.app.common.security.providers.dao;

import java.io.*;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.util.Logger;
import org.jasypt.encryption.pbe.PBEStringEncryptor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.*;

import com.archibus.context.*;
import com.archibus.security.providers.dao.PasswordsInConfigurationFilesEncryptor;
import com.archibus.utility.*;
import com.archibus.utility.regexp.*;

/**
 * Encrypts passwords in configuration files in "config" sub-folders: *.properties files,
 * afm-projects.xml. Replaces "TO_ENCRYPT(<password>)" with "ENC(<encryptedPassword>)"
 *
 * @author tydykov
 *
 */
public class PasswordsInConfigurationFilesEncryptorImpl
        implements InitializingBean, PasswordsInConfigurationFilesEncryptor {

    /**
     * Represents results of file processing.
     */
    static class Results {
        /**
         * Property: number of processed passwords.
         */
        int counter = 0;

        /**
         * Property: processed file content.
         */
        String fileContentProcessed;
    }

    /**
     * Constant: regular expression to match passwords to be encrypted.
     */
    private static RE reToEncrypt;

    static {
        try {
            reToEncrypt = new RE("TO_ENCRYPT\\(([^\\)]+)\\)");
        } catch (final RESyntaxException e) {
            // Should never happen
            e.printStackTrace();
        }
    }

    /**
     * Logger for this class and subclasses.
     */
    protected final Logger logger = Logger.getLogger(this.getClass());

    /**
     * Property: stringEncryptor, used to encrypt passwords.
     */
    private PBEStringEncryptor stringEncryptor;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(this.stringEncryptor, "stringEncryptor must be set");
    }

    @Override
    public String encryptPasswords() throws ExceptionBase {
        this.logger.info("encryptPasswords");

        final Context context = ContextStore.get();
        final File config = FileUtil.determineConfigDir(context.getWebAppPath());

        return encryptPasswords(config);
    }

    /**
     * Getter for the stringEncryptor property.
     *
     * @return the stringEncryptor property.
     */
    public PBEStringEncryptor getStringEncryptor() {
        return this.stringEncryptor;
    }

    /**
     * Setter for the stringEncryptor property.
     *
     * @param stringEncryptor the stringEncryptor to set.
     */
    public void setStringEncryptor(final PBEStringEncryptor stringEncryptor) {
        this.stringEncryptor = stringEncryptor;
    }

    /**
     * Encrypts passwords in configuration files in "config" sub-folders: *.properties files,
     * afm-projects.xml.
     *
     * @param config location of "config" folder.
     * @return message with results: "Success: updated %s files, encrypted %s passwords.".
     * @throws ExceptionBase if file reading or writing fails.
     */
    String encryptPasswords(final File config) throws ExceptionBase {
        // collect list of files to be processed
        // add all *.properties files in config subdirectories
        final String[] extensions = new String[] { "properties" };
        final List<File> filesToProcess =
                (List<File>) FileUtils.listFiles(config, extensions, true);

        {
            // add afm-projects.xml to the list
            final File afmProjectsXml = new File(config, "afm-projects.xml");
            filesToProcess.add(afmProjectsXml);
        }

        int updatedFilesCounter = 0;
        int passwordCounter = 0;
        // for each configuration file
        for (final File file : filesToProcess) {

            final int encryptedPasswordsInFile = processFile(file);
            if (encryptedPasswordsInFile > 0) {
                updatedFilesCounter++;
            }

            passwordCounter += encryptedPasswordsInFile;
        }

        final String result = String.format("Success: updated %s files, encrypted %s passwords.",
            updatedFilesCounter, passwordCounter);
        return result;
    }

    /**
     * Processes file: reads the file, replaces and encrypts passwords, writes the file.
     *
     * @param file to be processed.
     * @return number of processed passwords.
     * @throws ExceptionBase if file reading or writing fails.
     */
    private int processFile(final File file) throws ExceptionBase {
        // @non-translatable
        final String operation = String.format("Processing file=[%s]", file);
        this.logger.info(operation);

        // read file into String
        final String fileContent = readFile(file, operation);

        final Results results = this.replaceAndEncryptPasswords(fileContent);

        // if password was replaced
        if (results.counter > 0) {
            this.logger
                .info(String.format("Encrypted [%s] passwords, writing file", results.counter));

            // write string to file
            writeFile(file, operation, results.fileContentProcessed);
        }

        return results.counter;
    }

    /**
     * Reads file to string.
     *
     * @param file to be read.
     * @param operation to be reported if file reading fails.
     * @return content of the file.
     * @throws ExceptionBase if file reading fails.
     */
    private String readFile(final File file, final String operation) throws ExceptionBase {
        String fileContent = null;
        try {
            final Reader reader = new FileReader(file);
            fileContent = FileCopyUtils.copyToString(reader);
        } catch (final IOException e) {
            final ExceptionBase exception = new ExceptionBase(operation, e);
            throw exception;
        }

        return fileContent;
    }

    /**
     * Replaces and encrypts passwords in fileContent. Replaces "TO_ENCRYPT(<password>)" with
     * "ENC(<encryptedPassword>)"
     *
     * @param fileContent to be processed.
     * @return results of processing: modified fileContent and counter of replaced passwords.
     */
    private Results replaceAndEncryptPasswords(final String fileContent) {
        final Results results = new Results();

        results.fileContentProcessed = fileContent;

        results.counter = 0;
        // Match pre-compiled expression against fileContentProcessed
        // match TO_ENCRYPT(<password>)
        while (reToEncrypt.match(results.fileContentProcessed)) {
            results.counter++;

            final String password = reToEncrypt.getParen(1);
            // inside Parens

            // replace "TO_ENCRYPT(<password>)" with "ENC(<encryptedPassword>)"
            // encrypt <password>
            String passwordEncrypted = this.stringEncryptor.encrypt(password);
            passwordEncrypted = String.format("ENC(%s)", passwordEncrypted);

            // substitute macro with encrypted password value
            results.fileContentProcessed = reToEncrypt.subst(results.fileContentProcessed,
                passwordEncrypted, RE.REPLACE_FIRSTONLY);
        }

        return results;
    }

    /**
     * Writes fileContent to file.
     *
     * @param file to be written to.
     * @param operation to be reported if file writing fails.
     * @param fileContent content to be written.
     * @throws ExceptionBase if file writing fails.
     */
    private void writeFile(final File file, final String operation, final String fileContent)
            throws ExceptionBase {
        try {
            final Writer writer = new FileWriter(file);
            FileCopyUtils.copy(fileContent, writer);
        } catch (final IOException e) {
            final ExceptionBase exception = new ExceptionBase(operation, e);
            throw exception;
        }
    }
}
