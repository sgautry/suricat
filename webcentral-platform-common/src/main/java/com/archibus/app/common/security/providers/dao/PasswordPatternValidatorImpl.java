package com.archibus.app.common.security.providers.dao;

import com.archibus.utility.ExceptionBase;
import com.archibus.utility.regexp.*;

/**
 * Check if new password conforms to the specified password pattern policy.
 *
 * @author Valery Tydykov
 *
 * @see PasswordChangerImpl
 */
public class PasswordPatternValidatorImpl implements PasswordPatternValidator {
    /**
     * Property: Specifies minimum length of password.
     */
    private int minimumLength = 8;

    /**
     * Property: Specifies if password must include numbers.
     */
    private boolean mustIncludeNumbers = false;

    /**
     * Property: Specifies if password must include punctuation.
     */
    private boolean mustIncludePunctuation = false;

    /**
     * Property: Specifies if password must include special characters.
     */
    private boolean mustIncludeSpecialCharacters = false;

    /**
     * Property: Specifies if password must not consist of whitespaces only.
     */
    private boolean mustNotConsistOfWhitespaces = false;

    /**
     * Getter for the minimumLength property.
     *
     * @see minimumLength
     * @return the minimumLength property.
     */
    public int getMinimumLength() {
        return this.minimumLength;
    }

    /**
     * Getter for the mustIncludeNumbers property.
     *
     * @see mustIncludeNumbers
     * @return the mustIncludeNumbers property.
     */
    public boolean isMustIncludeNumbers() {
        return this.mustIncludeNumbers;
    }

    /**
     * Getter for the mustIncludePunctuation property.
     *
     * @see mustIncludePunctuation
     * @return the mustIncludePunctuation property.
     */
    public boolean isMustIncludePunctuation() {
        return this.mustIncludePunctuation;
    }

    /**
     * Getter for the mustIncludeSpecialCharacters property.
     *
     * @return the mustIncludeSpecialCharacters property.
     */
    public boolean isMustIncludeSpecialCharacters() {
        return this.mustIncludeSpecialCharacters;
    }

    /**
     * Getter for the mustNotConsistOfWhitespaces property.
     *
     * @see mustNotConsistOfWhitespaces
     * @return the mustNotConsistOfWhitespaces property.
     */
    public boolean isMustNotConsistOfWhitespaces() {
        return this.mustNotConsistOfWhitespaces;
    }

    /**
     * Setter for the minimumLength property.
     *
     * @see minimumLength
     * @param minimumLength the minimumLength to set.
     */
    public void setMinimumLength(final int minimumLength) {
        this.minimumLength = minimumLength;
    }

    /**
     * Setter for the mustIncludeNumbers property.
     *
     * @see mustIncludeNumbers
     * @param mustIncludeNumbers the mustIncludeNumbers to set
     */
    public void setMustIncludeNumbers(final boolean mustIncludeNumbers) {
        this.mustIncludeNumbers = mustIncludeNumbers;
    }

    /**
     * Setter for the mustIncludePunctuation property.
     *
     * @see mustIncludePunctuation
     * @param mustIncludePunctuation the mustIncludePunctuation to set
     */
    public void setMustIncludePunctuation(final boolean mustIncludePunctuation) {
        this.mustIncludePunctuation = mustIncludePunctuation;
    }

    /**
     * Setter for the mustIncludeSpecialCharacters property.
     *
     * @param mustIncludeSpecialCharacters the mustIncludeSpecialCharacters to set.
     */
    public void setMustIncludeSpecialCharacters(final boolean mustIncludeSpecialCharacters) {
        this.mustIncludeSpecialCharacters = mustIncludeSpecialCharacters;
    }

    /**
     * Setter for the mustNotConsistOfWhitespaces property.
     *
     * @see mustNotConsistOfWhitespaces
     * @param mustNotConsistOfWhitespaces the mustNotConsistOfWhitespaces to set
     */
    public void setMustNotConsistOfWhitespaces(final boolean mustNotConsistOfWhitespaces) {
        this.mustNotConsistOfWhitespaces = mustNotConsistOfWhitespaces;
    }

    @Override
    public void validate(final String password) throws ExceptionBase {
        checkMinimumLength(password);

        if (this.isMustNotConsistOfWhitespaces()) {
            checkMustNotConsistOfWhitespaces(password);
        }

        if (this.isMustIncludeNumbers()) {
            checkMustIncludeNumbers(password);
        }

        if (this.isMustIncludePunctuation()) {
            checkMustIncludePunctuation(password);
        }

        if (this.isMustIncludeSpecialCharacters()) {
            checkMustIncludeSpecialCharacters(password);
        }
    }

    /**
     * Checks if password length is less than minimum length.
     *
     * @param password to be checked.
     * @throws ExceptionBase if check fails.
     */
    private void checkMinimumLength(final String password) throws ExceptionBase {
        if (this.getMinimumLength() > password.length()) {
            // @translatable
            final String message = "Password must have minimum length: {0}";
            throw new ExceptionBase(message, new Object[] { Integer.valueOf(getMinimumLength()) },
                true);
        }
    }

    /**
     * Checks if password includes numbers.
     *
     * @param password to be checked.
     * @throws ExceptionBase if check fails.
     */
    private void checkMustIncludeNumbers(final String password) throws ExceptionBase {
        REProgram reProgram = null;
        try {
            reProgram = new RECompiler().compile("[0-9]+");
        } catch (final RESyntaxException e) {
            // error in RE pattern
            throw new ExceptionBase(null, e);
        }

        final RE re = new RE(reProgram);
        if (!re.match(password)) {
            // @translatable
            final String message = "Password must include numbers";
            throw new ExceptionBase(message, true);
        }
    }

    /**
     * Checks if password includes punctuation marks.
     *
     * @param password to be checked.
     * @throws ExceptionBase if check fails.
     */
    private void checkMustIncludePunctuation(final String password) throws ExceptionBase {
        REProgram reProgram = null;
        try {
            reProgram = new RECompiler().compile("[:punct:]+");
        } catch (final RESyntaxException e) {
            // error in RE pattern
            throw new ExceptionBase(null, e);
        }

        final RE re = new RE(reProgram);
        if (!re.match(password)) {
            // @translatable
            final String message = "Password must include punctuation";
            throw new ExceptionBase(message, true);
        }
    }

    /**
     * Checks if password includes special characters.
     *
     * @param password to be checked.
     * @throws ExceptionBase if check fails.
     */
    private void checkMustIncludeSpecialCharacters(final String password) throws ExceptionBase {
        REProgram reProgram = null;
        try {
            reProgram = new RECompiler().compile("[=\\#\\$&*\\+/<=>@\\^_`{|}~\\[\\]\\\\]+");
        } catch (final RESyntaxException e) {
            // error in RE pattern
            throw new ExceptionBase(null, e);
        }

        final RE re = new RE(reProgram);
        if (!re.match(password)) {
            // @translatable
            final String message = "Password must include special characters";
            throw new ExceptionBase(message, true);
        }
    }

    /**
     * Checks if password consist of whitespaces only.
     *
     * @param password to be checked.
     * @throws ExceptionBase if check fails.
     */
    private void checkMustNotConsistOfWhitespaces(final String password) throws ExceptionBase {
        if (password.trim().isEmpty()) {
            // @translatable
            final String message = "Password must not consist of whitespaces only";
            throw new ExceptionBase(message, true);
        }
    }
}
