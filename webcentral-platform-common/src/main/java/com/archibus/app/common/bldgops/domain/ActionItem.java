package com.archibus.app.common.bldgops.domain;

import java.util.Date;

/**
 * Domain object for Action Item.
 * <p>
 * Mapped to activity_log table.
 *
 * @author Zhang Yi
 *
 */
public class ActionItem {
	/**
	 * Action Title.
	 */
	private String actionTitle;

	/**
	 * Item Description.
	 */
	private String description;

	/**
	 * Item phone Requestor.
	 */
	private String phoneRequestor;

	/**
	 * Item phone Requestor.
	 */
	private String priority;

	/**
	 * Action Item's comments.
	 */
	private String comments;

	/**
	 * Mark Up Document.
	 */
	private String doc4;

	/**
	 * Action Item ID.
	 */
	private Integer id;

	/**
	 * Action Item assigned to.
	 */
	private String assignedTo;

	/**
	 * Action Item requestor.
	 */
	private String requestor;

	/**
	 * Action Item building ids.
	 */
	private String buildingId;

	/**
	 * Action Item floor id.
	 */
	private String floorId;

	/**
	 * Action Item room Id.
	 */
	private String roomId;

	/**
	 * Action Item site Id.
	 */
	private String siteId;

	/**
	 * Action Item problem Type.
	 */
	private String problemType;
	/**
	 *
	 * Action Item date Required.
	 */
	private Date dateRequired;

	/**
	 * Action Item date Scheduled.
	 */
	private Date dateScheduled;
	/**
	 * Action Item cost Estimated.
	 */
	private Double costEstimated;

	/**
	 * Action Item hours Estimate Baseline.
	 */
	private Integer hoursEstBaseline;

	/**
	 * Action Item division Id.
	 */
	private String divisionId;

	/**
	 * Action Item department Id.
	 */
	private String departmentId;

	/**
	 * Action Item account id.
	 */
	private String accountId;

	/**
	 * Action Item created By.
	 */
	private String createdBy;

	/**
	 * Action Item status.
	 */
	private String status;

	/**
	 * Action Item's move order ID.
	 */
	private int moveOrderId;

	/**
	 * Action Item's activity type.
	 */
	private String activityType;

	/**
	 * Action Item request date.
	 */
	private Date dateRequested;

	/**
	 * If auto-create work request.
	 */
	private int autocreateWr;

	/**
	 * Action Item's project id.
	 */
	private String projectId;

	/**
	 * Getter for the problemType property.
	 *
	 * @return the problemType property.
	 */
	public String getProblemType() {
		return this.problemType;
	}

	/**
	 * Getter for the priority property.
	 *
	 * @return the priority property.
	 */
	public String getPriority() {
		return this.priority;
	}

	/**
	 * Setter for the priority property.
	 *
	 * @param priority
	 *            the priority to set.
	 */
	public void setPriority(final String priority) {
		this.priority = priority;
	}

	/**
	 * Getter for the phoneRequestor property.
	 *
	 * @return the phoneRequestor property.
	 */
	public String getPhoneRequestor() {
		return this.phoneRequestor;
	}

	/**
	 * Setter for the phoneRequestor property.
	 *
	 * @param phoneRequestor
	 *            the phoneRequestor to set.
	 */
	public void setPhoneRequestor(final String phoneRequestor) {
		this.phoneRequestor = phoneRequestor;
	}

	/**
	 * Setter for the problemType property.
	 *
	 * @param problemType
	 *            the problemType to set.
	 */
	public void setProblemType(final String problemType) {
		this.problemType = problemType;
	}

	/**
	 * Getter for the dateRequired property.
	 *
	 * @return the dateRequired property.
	 */
	public Date getDateRequired() {
		return this.dateRequired;
	}

	/**
	 * Setter for the dateRequired property.
	 *
	 * @param dateRequired
	 *            the dateRequired to set.
	 */
	public void setDateRequired(final Date dateRequired) {
		this.dateRequired = dateRequired;
	}

	/**
	 * Getter for the dateScheduled property.
	 *
	 * @return the dateScheduled property.
	 */
	public Date getDateScheduled() {
		return this.dateScheduled;
	}

	/**
	 * Setter for the dateScheduled property.
	 *
	 * @param dateScheduled
	 *            the dateScheduled to set.
	 */
	public void setDateScheduled(final Date dateScheduled) {
		this.dateScheduled = dateScheduled;
	}

	/**
	 * Getter for the costEstimated property.
	 *
	 * @return the costEstimated property.
	 */
	public Double getCostEstimated() {
		return this.costEstimated;
	}

	/**
	 * Setter for the costEstimated property.
	 *
	 * @param costEstimated
	 *            the costEstimated to set.
	 */
	public void setCostEstimated(final Double costEstimated) {
		this.costEstimated = costEstimated;
	}

	/**
	 * Getter for the hoursEstBaseline property.
	 *
	 * @return the hoursEstBaseline property.
	 */
	public Integer getHoursEstBaseline() {
		return this.hoursEstBaseline;
	}

	/**
	 * Setter for the hoursEstBaseline property.
	 *
	 * @param hoursEstBaseline
	 *            the hoursEstBaseline to set.
	 */
	public void setHoursEstBaseline(final Integer hoursEstBaseline) {
		this.hoursEstBaseline = hoursEstBaseline;
	}

	/**
	 * Getter for the divisionId property.
	 *
	 * @return the divisionId property.
	 */
	public String getDivisionId() {
		return this.divisionId;
	}

	/**
	 * Setter for the divisionId property.
	 *
	 * @param divisionId
	 *            the divisionId to set.
	 */
	public void setDivisionId(final String divisionId) {
		this.divisionId = divisionId;
	}

	/**
	 * Getter for the departmentId property.
	 *
	 * @return the departmentId property.
	 */
	public String getDepartmentId() {
		return this.departmentId;
	}

	/**
	 * Setter for the departmentId property.
	 *
	 * @param departmentId
	 *            the departmentId to set.
	 */
	public void setDepartmentId(final String departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * Getter for the accountId property.
	 *
	 * @return the accountId property.
	 */
	public String getAccountId() {
		return this.accountId;
	}

	/**
	 * Setter for the accountId property.
	 *
	 * @param accountId
	 *            the accountId to set.
	 */
	public void setAccountId(final String accountId) {
		this.accountId = accountId;
	}

	/**
	 * Getter for the buildingId property.
	 *
	 * @return the buildingId property.
	 */
	public String getBuildingId() {
		return this.buildingId;
	}

	/**
	 * Getter for the floorId property.
	 *
	 * @return the floorId property.
	 */
	public String getFloorId() {
		return this.floorId;
	}

	/**
	 * Setter for the floorId property.
	 *
	 * @param floorId
	 *            the floorId to set.
	 */
	public void setFloorId(final String floorId) {
		this.floorId = floorId;
	}

	/**
	 * Getter for the roomId property.
	 *
	 * @return the roomId property.
	 */
	public String getRoomId() {
		return this.roomId;
	}

	/**
	 * Setter for the roomId property.
	 *
	 * @param roomId
	 *            the roomId to set.
	 */
	public void setRoomId(final String roomId) {
		this.roomId = roomId;
	}

	/**
	 * Setter for the buildingId property.
	 *
	 * @param buildingId
	 *            the buildingId to set.
	 */
	public void setBuildingId(final String buildingId) {
		this.buildingId = buildingId;
	}

	/**
	 * Getter for the actionTitle property.
	 *
	 * @see actionTitle
	 * @return the actionTitle property.
	 */
	public String getActionTitle() {
		return this.actionTitle;
	}

	/**
	 * Setter for the actionTitle property.
	 *
	 * @see actionTitle
	 * @param actionTitle
	 *            the actionTitle to set
	 */

	public void setActionTitle(final String actionTitle) {
		this.actionTitle = actionTitle;
	}

	/**
	 * Getter for the description property.
	 *
	 * @see description
	 * @return the description property.
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Setter for the description property.
	 *
	 * @see description
	 * @param description
	 *            the description to set
	 */

	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Getter for the comments property.
	 *
	 * @see comments
	 * @return the comments property.
	 */
	public String getComments() {
		return this.comments;
	}

	/**
	 * Setter for the comments property.
	 *
	 * @see comments
	 * @param comments
	 *            the comments to set
	 */

	public void setComments(final String comments) {
		this.comments = comments;
	}

	/**
	 * Getter for the doc4 property.
	 *
	 * @see doc4
	 * @return the doc4 property.
	 */
	public String getDoc4() {
		return this.doc4;
	}

	/**
	 * Setter for the doc4 property.
	 *
	 * @see doc4
	 * @param doc4
	 *            the doc4 to set
	 */

	public void setDoc4(final String doc4) {
		this.doc4 = doc4;
	}

	/**
	 * Getter for the id property.
	 *
	 * @see id
	 * @return the id property.
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * Setter for the id property.
	 *
	 * @see id
	 * @param id
	 *            the id to set
	 */

	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * Getter for the assignedTo property.
	 *
	 * @return the assignedTo property.
	 */
	public String getAssignedTo() {
		return this.assignedTo;
	}

	/**
	 * Setter for the assignedTo property.
	 *
	 * @param assignedTo
	 *            the assignedTo to set.
	 */
	public void setAssignedTo(final String assignedTo) {
		this.assignedTo = assignedTo;
	}

	/**
	 * Getter for the requestor property.
	 *
	 * @return the requestor property.
	 */
	public String getRequestor() {
		return this.requestor;
	}

	/**
	 * Setter for the requestor property.
	 *
	 * @param requestor
	 *            the requestor to set.
	 */
	public void setRequestor(final String requestor) {
		this.requestor = requestor;
	}

	/**
	 * Getter for the siteId property.
	 *
	 * @return the siteId property.
	 */
	public String getSiteId() {
		return this.siteId;
	}

	/**
	 * Setter for the siteId property.
	 *
	 * @param siteId
	 *            the siteId to set.
	 */
	public void setSiteId(final String siteId) {
		this.siteId = siteId;
	}

	/**
	 * Getter for the createdBy property.
	 *
	 * @return the createdBy property.
	 */
	public String getCreatedBy() {
		return this.createdBy;
	}

	/**
	 * Setter for the createdBy property.
	 *
	 * @param createdBy
	 *            the createdBy to set.
	 */
	public void setCreatedBy(final String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Getter for the status property.
	 *
	 * @return the status property.
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 * Setter for the status property.
	 *
	 * @param status
	 *            the status to set.
	 */
	public void setStatus(final String status) {
		this.status = status;
	}

	/**
	 * Getter for the moveOrderId property.
	 *
	 * @return the moveOrderId property.
	 */
	public int getMoveOrderId() {
		return this.moveOrderId;
	}

	/**
	 * Setter for the moveOrderId property.
	 *
	 * @param moveOrderId
	 *            the moveOrderId to set.
	 */
	public void setMoveOrderId(final int moveOrderId) {
		this.moveOrderId = moveOrderId;
	}

	/**
	 * Getter for the activityType property.
	 *
	 * @return the activityType property.
	 */
	public String getActivityType() {
		return this.activityType;
	}

	/**
	 * Setter for the activityType property.
	 *
	 * @param activityType
	 *            the activityType to set.
	 */
	public void setActivityType(final String activityType) {
		this.activityType = activityType;
	}

	/**
	 * Getter for the dateRequested property.
	 *
	 * @return the dateRequested property.
	 */
	public Date getDateRequested() {
		return this.dateRequested;
	}

	/**
	 * Setter for the dateRequested property.
	 *
	 * @param dateRequested
	 *            the dateRequested to set.
	 */
	public void setDateRequested(final Date dateRequested) {
		this.dateRequested = dateRequested;
	}

	/**
	 * Getter for the autocreateWr property.
	 *
	 * @return the autocreateWr property.
	 */
	public int getAutocreateWr() {
		return this.autocreateWr;
	}

	/**
	 * Setter for the autocreateWr property.
	 *
	 * @param autocreateWr
	 *            the autocreateWr to set.
	 */
	public void setAutocreateWr(final int autocreateWr) {
		this.autocreateWr = autocreateWr;
	}

	/**
	 * Getter for the projectId property.
	 *
	 * @return the projectId property.
	 */
	public String getProjectId() {
		return this.projectId;
	}

	/**
	 * Setter for the projectId property.
	 *
	 * @param projectId
	 *            the projectId to set.
	 */
	public void setProjectId(final String projectId) {
		this.projectId = projectId;
	}

}
