package com.archibus.app.common.security.domain;

import java.util.*;

/**
 * Domain class for password reset token.
 * <p>
 * Mapped to afm_password_token table.
 *
 * @author Valery Tydykov
 * @since 23.2
 *
 */
public class PasswordResetToken {
    /**
     * Property: date and time when token expires.
     */
    private java.util.Date expirationTime;

    /**
     * Property: id.
     */
    private int id;

    /**
     * Property: token.
     */
    private String token;

    /**
     * Property: user name.
     */
    private String userName;

    /**
     * Default constructor.
     */
    public PasswordResetToken() {
        super();
    }

    /**
     * Constructor specifying token, userName.
     *
     * @param token unique token.
     * @param userName user name.
     * @param expireInMinutes time in minutes after which the token will expire.
     */
    public PasswordResetToken(final String token, final String userName,
            final int expireInMinutes) {
        super();
        this.token = token;
        this.userName = userName;
        this.expirationTime = calculateExpiration(expireInMinutes);
    }

    /**
     * Getter for the expirationTime property.
     *
     * @return the expirationTime property.
     */
    public java.util.Date getExpirationTime() {
        return this.expirationTime;
    }

    /**
     * Getter for the id property.
     *
     * @return the id property.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Getter for the token property.
     *
     * @return the token property.
     */
    public String getToken() {
        return this.token;
    }

    /**
     * Getter for the userName property.
     *
     * @return the userName property.
     */
    public String getUserName() {
        return this.userName;
    }

    /**
     * Setter for the expirationTime property.
     *
     * @param expirationTime the expirationTime to set.
     */
    public void setExpirationTime(final java.util.Date expirationTime) {
        this.expirationTime = expirationTime;
    }

    /**
     * Setter for the id property.
     *
     * @param id the id to set.
     */
    public void setId(final int id) {
        this.id = id;
    }

    /**
     * Setter for the token property.
     *
     * @param token the token to set.
     */
    public void setToken(final String token) {
        this.token = token;
    }

    /**
     * Setter for the userName property.
     *
     * @param userName the userName to set.
     */
    public void setUserName(final String userName) {
        this.userName = userName;
    }

    /**
     * Calculates date and time when token will expire.
     *
     * @param expireInMinutes time in minutes after which the token will expire.
     * @return expiration date and time.
     */
    private Date calculateExpiration(final int expireInMinutes) {
        final Calendar expiration = Calendar.getInstance();
        expiration.add(Calendar.MINUTE, expireInMinutes);

        return expiration.getTime();
    }
}
