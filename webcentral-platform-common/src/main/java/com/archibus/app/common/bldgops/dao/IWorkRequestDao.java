package com.archibus.app.common.bldgops.dao;

import com.archibus.app.common.bldgops.domain.WorkRequest;
import com.archibus.core.dao.IDao;
import com.archibus.datasource.data.DataRecord;

/**
 * DAO for Action Item.
 *
 * @author Zhang Yi
 *
 */
public interface IWorkRequestDao extends IDao<WorkRequest> {

	/**
	 * Converts the data record to a bean instance.
	 *
	 * @param record
	 *            The data record.
	 * @return The bean instance.
	 */
	WorkRequest convertRecordToObject(DataRecord record);
}
