package com.archibus.app.common.depreciation.domain;

import java.util.Date;

/**
 * Furniture domain object. Contains only fields that are required for depreciation calculation.
 * <p>
 *
 * Used by Depreciation service to calculate and update depreciation value. Managed by Spring.
 * Configured in [schema\ab-products\common\resources\appContext-services.xml] file.
 *
 * @author Ioan Draghici
 * @author Radu Bunea
 * @since 23.1
 *
 */
public class Furniture {

    /**
     * Furniture code.
     */
    private String taId;

    /**
     * Property type.
     */
    private String propertyType;

    /**
     * Value salvage.
     */
    private double valueSalvage;

    /**
     * Value original.
     */
    private double valueOriginal;

    /**
     * Cost depreciated value.
     */
    private double costDepValue;

    /**
     * Date depreciated closing month.
     */
    private Date dateDepreciationClosingMonth;

    /**
     * Date delivery.
     */
    private Date dateDelivery;

    /**
     * Furniture Standard.
     */
    private String fnStd;

    /**
     * Furniture building Id.
     */
    private String buildingId;

    /**
     * Furniture floor Id.
     */
    private String floorId;

    /**
     * Furniture room Id.
     */
    private String roomId;

    /**
     * Getter for the taId property.
     *
     * @see taId
     * @return the taId property.
     */
    public String getTaId() {
        return this.taId;
    }

    /**
     * Setter for the taId property.
     *
     * @see taId
     * @param taId the taId to set
     */

    public void setTaId(final String taId) {
        this.taId = taId;
    }

    /**
     * Getter for the propertyType property.
     *
     * @see propertyType
     * @return the propertyType property.
     */
    public String getPropertyType() {
        return this.propertyType;
    }

    /**
     * Setter for the propertyType property.
     *
     * @see propertyType
     * @param propertyType the propertyType to set
     */

    public void setPropertyType(final String propertyType) {
        this.propertyType = propertyType;
    }

    /**
     * Getter for the valueSalvage property.
     *
     * @see valueSalvage
     * @return the valueSalvage property.
     */
    public double getValueSalvage() {
        return this.valueSalvage;
    }

    /**
     * Getter for the buildingId property.
     *
     * @return the buildingId property.
     */
    public String getBuildingId() {
        return this.buildingId;
    }

    /**
     * Setter for the buildingId property.
     *
     * @param buildingId the buildingId to set.
     */
    public void setBuildingId(final String buildingId) {
        this.buildingId = buildingId;
    }

    /**
     * Getter for the floorId property.
     *
     * @return the floorId property.
     */
    public String getFloorId() {
        return this.floorId;
    }

    /**
     * Setter for the floorId property.
     *
     * @param floorId the floorId to set.
     */
    public void setFloorId(final String floorId) {
        this.floorId = floorId;
    }

    /**
     * Getter for the roomId property.
     *
     * @return the roomId property.
     */
    public String getRoomId() {
        return this.roomId;
    }

    /**
     * Setter for the roomId property.
     *
     * @param roomId the roomId to set.
     */
    public void setRoomId(final String roomId) {
        this.roomId = roomId;
    }

    /**
     * Setter for the valueSalvage property.
     *
     * @see valueSalvage
     * @param valueSalvage the valueSalvage to set
     */

    public void setValueSalvage(final double valueSalvage) {
        this.valueSalvage = valueSalvage;
    }

    /**
     * Getter for the costDepValue property.
     *
     * @see costDepValue
     * @return the costDepValue property.
     */
    public double getCostDepValue() {
        return this.costDepValue;
    }

    /**
     * Setter for the costDepValue property.
     *
     * @see costDepValue
     * @param costDepValue the costDepValue to set
     */

    public void setCostDepValue(final double costDepValue) {
        this.costDepValue = costDepValue;
    }

    /**
     * Getter for the dateDepreciationClosingMonth property.
     *
     * @return the dateDepreciationClosingMonth property.
     */
    public Date getDateDepreciationClosingMonth() {
        return this.dateDepreciationClosingMonth;
    }

    /**
     * Setter for the dateDepreciationClosingMonth property.
     *
     * @param dateDepreciationClosingMonth the dateDepreciationClosingMonth to set.
     */
    public void setDateDepreciationClosingMonth(final Date dateDepreciationClosingMonth) {
        this.dateDepreciationClosingMonth = dateDepreciationClosingMonth;
    }

    /**
     * Getter for the valueOriginal property.
     *
     * @see valueOriginal
     * @return the valueOriginal property.
     */
    public double getValueOriginal() {
        return this.valueOriginal;
    }

    /**
     * Setter for the valueOriginal property.
     *
     * @see valueOriginal
     * @param valueOriginal the valueOriginal to set
     */

    public void setValueOriginal(final double valueOriginal) {
        this.valueOriginal = valueOriginal;
    }

    /**
     * Getter for the dateDelivery property.
     *
     * @see dateDelivery
     * @return the dateDelivery property.
     */
    public Date getDateDelivery() {
        return this.dateDelivery;
    }

    /**
     * Setter for the dateDelivery property.
     *
     * @see dateDelivery
     * @param dateDelivery the dateDelivery to set
     */

    public void setDateDelivery(final Date dateDelivery) {
        this.dateDelivery = dateDelivery;
    }

    /**
     * Getter for the fnStd property.
     *
     * @return the fnStd property.
     */
    public String getFnStd() {
        return this.fnStd;
    }

    /**
     * Setter for the fnStd property.
     *
     * @param fnStd the fnStd to set.
     */
    public void setFnStd(final String fnStd) {
        this.fnStd = fnStd;
    }

}
