package com.archibus.app.common.drawing.svg.service.impl;

import java.util.*;
import java.util.Map.Entry;

import com.archibus.app.common.drawing.svg.service.dao.IHighlightParametersDao;
import com.archibus.app.common.drawing.svg.service.domain.HighlightParameters;
import com.archibus.datasource.DataSourceFactory;
import com.archibus.ext.drawing.highlight.*;
import com.archibus.ext.drawing.highlight.drawing.Highlight;
import com.archibus.ext.report.ReportUtility;
import com.archibus.model.view.datasource.AbstractDataSourceDef;
import com.archibus.model.view.datasource.field.AbstractDataSourceFieldDef;
import com.archibus.model.view.report.ReportPropertiesDef;
import com.archibus.service.common.svg.ReportUtilities;
import com.archibus.utility.StringUtil;

/**
 * Copyright (C) ARCHIBUS, Inc. All rights reserved.
 */
/**
 * Provides a helper class to get legend data for DrawingSvgService.
 *
 *
 *
 * @author Yong Shao
 * @since 23.2
 *
 */

public final class LegendUtilities {
	/**
	 * Private default constructor: utility class is non-instantiable.
	 */
	private LegendUtilities() {
	}

	/**
	 *
	 * Gets legend data.
	 *
	 * @param drawingNames
	 *            list of drawing names.
	 * @param parameters
	 *            Map<String, String>.
	 * @param highlightParametersDao
	 *            IHighlightParametersDao.
	 * @return Map<String, Map<String, String>> like {'ELECTRONIC
	 *         SYS.-Administration':{name:'SOLID',value:'210002',color:'rgb(255,
	 *         0,0)'}}.
	 */
	public static Map<String, Map<String, String>> getLegends(final List<String> drawingNames,
			final Map<String, String> parameters, final IHighlightParametersDao highlightParametersDao) {
		final Map<String, Map<String, String>> results = new TreeMap<String, Map<String, String>>();
		final List<Map<String, String>> parametersList = new ArrayList<Map<String, String>>();
		parametersList.add(parameters);
		final List<HighlightParameters> highlightParameters = HighlightUtilities
				.extractHighlightParameters(parametersList);
		final HighlightParameters highlightParameter = highlightParameters.get(0);
		final String viewName = highlightParameter.getViewName();
		com.archibus.datasource.DataSource highlightDataSource = null;
		if (StringUtil.notNullOrEmpty(highlightParameter.getHighlightDatasourceId())) {
			final List<String> legendKeyFields = getLegendKeyFields(viewName,
					highlightParameter.getHighlightDatasourceId());

			highlightDataSource = DataSourceFactory.loadDataSourceFromFile(viewName,
					highlightParameter.getHighlightDatasourceId());
			if (highlightParameter.getDataSourceParameters() != null) {
				ReportUtility.applyParameters2DataSource(highlightDataSource,
						highlightParameter.getDataSourceParameters());
			}
			String assetToHighlight = null;
			if (StringUtil.isNullOrEmpty(highlightParameter.getAssetType()) && highlightDataSource != null) {
				assetToHighlight = highlightDataSource.getMainTableName();
			} else {
				assetToHighlight = highlightParameter.getAssetType();
			}
			final ReportPropertiesDef reportPropertiesDef = ReportUtilities.getReportPropertiesDef();
			reportPropertiesDef.setAssetTables(assetToHighlight);

			final HighlightImageService highlightImageService = new HighlightImageService(reportPropertiesDef);

			retrieveLegends(drawingNames, results, highlightParameter, highlightDataSource, legendKeyFields,
					highlightImageService);
		}

		return results;
	}

	/**
	 * Retrieves legend data.
	 *
	 * @param drawingNames
	 *            list of drawing names.
	 * @param results
	 *            Map<String, Map<String, String>> like {'ELECTRONIC
	 *            SYS.-Administration':{name:'SOLID',value:'210002',color:'rgb(
	 *            255,0,0)'}}.
	 * @param highlightParameter
	 *            HighlightParameters.
	 * @param highlightDataSource
	 *            highlight DataSource.
	 * @param legendKeyFields
	 *            list of legend key field name.
	 * @param highlightImageService
	 *            HighlightImageService.
	 */
	private static void retrieveLegends(final List<String> drawingNames, final Map<String, Map<String, String>> results,
			final HighlightParameters highlightParameter, final com.archibus.datasource.DataSource highlightDataSource,
			final List<String> legendKeyFields, final HighlightImageService highlightImageService) {
		for (final String drawingName : drawingNames) {
			final Map<String, Highlight> highlights = highlightImageService.retrieveLegendData(highlightDataSource,
					RestrictionUtilities.getRestriction(highlightParameter.getRestriction(), highlightDataSource,
							drawingName),
					legendKeyFields);
			for (final Entry<String, Highlight> highlight : highlights.entrySet()) {
				final Highlight highlightValue = highlight.getValue();
				if (!results.containsKey(highlight.getKey())) {
					final Map<String, String> valueObject = new HashMap<String, String>();
					valueObject.put("color", highlightValue.getRGBColorValue());
					valueObject.put("name", highlightValue.getName());
					valueObject.put("value", highlightValue.getValue());
					results.put(highlight.getKey(), valueObject);
				}
			}
		}
	}

	/**
	 *
	 * Gets LegendKey Fields.
	 *
	 * @param viewName
	 *            view name.
	 * @param highlightDatasourceId
	 *            datasource id.
	 * @return list of legend key field name.
	 */
	static List<String> getLegendKeyFields(final String viewName, final String highlightDatasourceId) {
		final List<String> legendKeyFields = new ArrayList<String>();
		final AbstractDataSourceDef dataSourceDef = DataSourceFactory.loadDataSourceDef(viewName,
				highlightDatasourceId);

		for (final AbstractDataSourceFieldDef fieldDef : dataSourceDef.getFieldDefs()) {
			if (fieldDef.getLegendKey()) {
				legendKeyFields.add(fieldDef.getFullName());
			}
		}
		return legendKeyFields;
	}
}
