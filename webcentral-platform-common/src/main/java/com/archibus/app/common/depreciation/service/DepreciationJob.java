package com.archibus.app.common.depreciation.service;

import java.math.BigDecimal;
import java.util.*;

import org.apache.commons.lang.time.DateUtils;

import com.archibus.app.common.depreciation.dao.*;
import com.archibus.app.common.depreciation.dao.datasource.*;
import com.archibus.app.common.depreciation.domain.*;
import com.archibus.app.common.depreciation.impl.DepreciationServiceHelper;
import com.archibus.jobmanager.*;

/**
 * Provide methods to calculate and update depreciation job. Run as scheduled job.
 * <p>
 *
 *
 * @author Ioan Draghici
 * @author Radu Bunea
 * @since 23.1
 *
 */
public class DepreciationJob extends JobBase {

    /**
     * Reference to custom data source used to load depreciable equipment.These references are set
     * by the Web Central container based on the Spring configuration file
     * schema/ab-products/solutions/common/appContext-services.xml.
     */
    private IEquipmentDao<Equipment> equipmentDao;

    /**
     * Reference to custom data source used to load depreciable furniture.These references are set
     * by the Web Central container based on the Spring configuration file
     * schema/ab-products/solutions/common/appContext-services.xml.
     */
    private IFurnitureDao<Furniture> furnitureDao;

    /**
     * Job status.
     */
    private JobStatus jobStatus;

    /**
     * Calculate asset depreciation for all assets types and active depreciation report.
     *
     * @param activeReports List of active depreciation reports.
     */
    public void calculateDepreciation(final List<DepreciationReport> activeReports) {
        initPerRequestState();
        // initialize job status total number
        final int noOfDepreciableEquipment = this.equipmentDao.getCountOfDepreciableEquipment();
        final int noOfDepreciableFurniture = this.furnitureDao.getCountOfDepreciableFurniture();
        this.jobStatus.setTotalNumber(noOfDepreciableEquipment + noOfDepreciableFurniture);
        calculateEquipmentDepreciation(activeReports);
        calculateFurnitureDepreciation(activeReports);
    }

    /**
     * Calculate equipment depreciation for active depreciation report.
     *
     * @param activeReports List of active depreciation reports.
     */
    public void calculateEquipmentDepreciation(final List<DepreciationReport> activeReports) {
        initPerRequestState();
        final int noOfDepreciableEquipment = this.equipmentDao.getCountOfDepreciableEquipment();
        // if equipment depreciation method was called directly job total number is not initialized.
        if (noOfDepreciableEquipment > this.jobStatus.getTotalNumber()) {
            this.jobStatus.setTotalNumber(noOfDepreciableEquipment);
        }

        final List<Equipment> equipmentList = this.equipmentDao.getDepreciableEquipmentList();

        deleteDataForEquipmentReports(activeReports);

        for (final Equipment equipment : equipmentList) {
            if (this.jobStatus.isStopRequested()) {
                this.jobStatus.setCode(JobStatus.JOB_STOPPED);
                break;
            }
            this.jobStatus.incrementCurrentNumber();
            for (final DepreciationReport activeReport : activeReports) {
                calculateEquipmentDepreciationForReport(equipment, activeReport);
            }
            calculateEquipmentCostDepreciationValue(equipment);
        }
    }

    /**
     * Calculate furniture depreciation for active depreciation report.
     *
     * @param activeReports List of active depreciation reports.
     */
    public void calculateFurnitureDepreciation(final List<DepreciationReport> activeReports) {
        initPerRequestState();

        final int noOfDepreciableFurniture = this.furnitureDao.getCountOfDepreciableFurniture();
        // if furniture depreciation method was called directly job total number is not initialized.
        if (noOfDepreciableFurniture > this.jobStatus.getTotalNumber()) {
            this.jobStatus.setTotalNumber(noOfDepreciableFurniture);
        }

        final List<Furniture> furnitureList = this.furnitureDao.getDepreciableFurnitureList();

        deleteDataForFurnitureReports(activeReports);

        for (final Furniture furniture : furnitureList) {
            if (this.jobStatus.isStopRequested()) {
                this.jobStatus.setCode(JobStatus.JOB_STOPPED);
                break;
            }
            this.jobStatus.incrementCurrentNumber();
            for (final DepreciationReport activeReport : activeReports) {
                calculateFurnitureDepreciationForReport(furniture, activeReport);
            }
            calculateFurnitureCostDepreciationValue(furniture);
        }
    }

    /**
     * Setter for the equipmentDao property.
     *
     * @see equipmentDao
     * @param equipmentDao the equipmentDao to set
     */

    public void setEquipmentDao(final IEquipmentDao<Equipment> equipmentDao) {
        this.equipmentDao = equipmentDao;
    }

    /**
     * Setter for the furnitureDao property.
     *
     * @see furnitureDao
     * @param furnitureDao the furnitureDao to set
     */

    public void setFurnitureDao(final IFurnitureDao<Furniture> furnitureDao) {
        this.furnitureDao = furnitureDao;
    }

    /**
     * Setter for the jobStatus property.
     *
     * @see jobStatus
     * @param jobStatus the jobStatus to set
     */

    public void setJobStatus(final JobStatus jobStatus) {
        this.jobStatus = jobStatus;
    }

    /**
     *
     * Delete data for equipment active reports.
     *
     * @param depreciationReports Active reports.
     */
    private void deleteDataForEquipmentReports(final List<DepreciationReport> depreciationReports) {
        final IDepreciationDao<EquipmentDepreciation> equipmentDepreciationDao =
                new EquipmentDepreciationDataSource();
        for (final DepreciationReport depreciationReport : depreciationReports) {
            equipmentDepreciationDao.deleteDataForReport(depreciationReport);
        }
    }

    /**
     *
     * Delete data for furniture active reports.
     *
     * @param depreciationReports Active reports.
     */
    private void deleteDataForFurnitureReports(final List<DepreciationReport> depreciationReports) {
        final IDepreciationDao<FurnitureDepreciation> furnitureDepreciationDao =
                new FurnitureDepreciationDataSource();
        for (final DepreciationReport depreciationReport : depreciationReports) {
            furnitureDepreciationDao.deleteDataForReport(depreciationReport);
        }
    }

    /**
     * Calculate equipment depreciation for depreciation report.
     *
     * @param equipment Equipment
     * @param depreciationReport depreciation report
     */
    private void calculateEquipmentDepreciationForReport(final Equipment equipment,
            final DepreciationReport depreciationReport) {
        final IPropertyTypeDao<PropertyType> propertyTypeDao = new PropertyTypeDataSource();
        final IDepreciationDao<EquipmentDepreciation> equipmentDepreciationDao =
                new EquipmentDepreciationDataSource();

        final PropertyType propertyType =
                propertyTypeDao.getPropertyTypeByName(equipment.getPropertyType());

        final EquipmentDepreciation equipmentDepreciation = DepreciationServiceHelper
            .calculateEquipmentDepreciation(equipment, propertyType, depreciationReport);
        if (equipmentDepreciation != null) {
            equipmentDepreciationDao.save(equipmentDepreciation);
        }
    }

    /**
     * Calculate furniture depreciation for depreciation report.
     *
     * @param furniture Furniture
     * @param depreciationReport depreciation report
     */
    private void calculateFurnitureDepreciationForReport(final Furniture furniture,
            final DepreciationReport depreciationReport) {
        final IPropertyTypeDao<PropertyType> propertyTypeDao = new PropertyTypeDataSource();
        final IDepreciationDao<FurnitureDepreciation> furnitureDepreciationDao =
                new FurnitureDepreciationDataSource();

        final PropertyType propertyType =
                propertyTypeDao.getPropertyTypeByName(furniture.getPropertyType());

        final FurnitureDepreciation furnitureDepreciation = DepreciationServiceHelper
            .calculateFurnitureDepreciation(furniture, propertyType, depreciationReport);
        if (furnitureDepreciation != null) {
            furnitureDepreciationDao.save(furnitureDepreciation);
        }
    }

    /**
     * Calculate equipment cost depreciation value.
     *
     * @param equipment Equipment.
     */
    private void calculateEquipmentCostDepreciationValue(final Equipment equipment) {
        final IDepreciationReportDao<DepreciationReport> reportDao =
                new DepreciationReportDataSource();
        final IDepreciationDao<EquipmentDepreciation> equipmentDepreciationDao =
                new EquipmentDepreciationDataSource();

        final EquipmentDepreciation equipmentDepreciation =
                equipmentDepreciationDao.findCurrentDepreciation(equipment.getEqId());
        if (equipmentDepreciation != null) {
            final DepreciationReport depreciationReport =
                    reportDao.getReportById(equipmentDepreciation.getReportId());
            final Date depreciationReportClosingMonth = depreciationReport.getLastDate();
            final Date equipmentClosingMonth = equipment.getDateDepreciationClosingMonth();
            if (equipmentClosingMonth != null
                    && (equipmentClosingMonth.before(depreciationReportClosingMonth) || DateUtils
                        .isSameDay(equipmentClosingMonth, depreciationReportClosingMonth))) {
                setEquipmentCostDepreciationValue(equipment, equipmentDepreciation,
                    depreciationReportClosingMonth);
            } else {
                equipment.setDateDepreciationClosingMonth(depreciationReportClosingMonth);
                equipment.setCostDepValue(equipmentDepreciation.getValueCurrent());
            }
            this.equipmentDao.update(equipment);
        }
    }

    /**
     * Calculate furniture cost depreciation value.
     *
     * @param furniture Furniture.
     */
    private void calculateFurnitureCostDepreciationValue(final Furniture furniture) {
        final IDepreciationReportDao<DepreciationReport> reportDao =
                new DepreciationReportDataSource();
        final IDepreciationDao<FurnitureDepreciation> furnitureDepreciationDao =
                new FurnitureDepreciationDataSource();

        final FurnitureDepreciation furnitureDepreciation =
                furnitureDepreciationDao.findCurrentDepreciation(furniture.getTaId());
        if (furnitureDepreciation != null) {
            final DepreciationReport depreciationReport =
                    reportDao.getReportById(furnitureDepreciation.getReportId());
            final Date depreciationReportClosingMonth = depreciationReport.getLastDate();
            final Date furnitureClosingMonth = furniture.getDateDepreciationClosingMonth();
            if (furnitureClosingMonth != null
                    && (furnitureClosingMonth.before(depreciationReportClosingMonth) || DateUtils
                        .isSameDay(furnitureClosingMonth, depreciationReportClosingMonth))) {
                setFurnitureCostDepreciationValue(furniture, furnitureDepreciation,
                    depreciationReportClosingMonth);
            } else {
                furniture.setDateDepreciationClosingMonth(depreciationReportClosingMonth);
                furniture.setCostDepValue(furnitureDepreciation.getValueCurrent());
            }
            this.furnitureDao.update(furniture);
        }
    }

    /**
     * Set furniture cost depreciation value.
     *
     * @param furniture Furniture
     * @param furnitureDepreciation Furniture depreciation
     * @param depreciationReportClosingMonth depreciation report closing month
     */
    private void setFurnitureCostDepreciationValue(final Furniture furniture,
            final FurnitureDepreciation furnitureDepreciation,
            final Date depreciationReportClosingMonth) {
        final BigDecimal costDepreciationValue =
                new BigDecimal(furniture.getCostDepValue()).setScale(2, BigDecimal.ROUND_HALF_DOWN);
        final BigDecimal currentValue = new BigDecimal(furnitureDepreciation.getValueCurrent())
            .setScale(2, BigDecimal.ROUND_HALF_DOWN);
        if (costDepreciationValue.compareTo(currentValue) != 0) {
            furniture.setDateDepreciationClosingMonth(depreciationReportClosingMonth);
            furniture.setCostDepValue(furnitureDepreciation.getValueCurrent());
        }
    }

    /**
     * Set equipment cost depreciation value.
     *
     * @param equipment Equipment
     * @param equipmentDepreciation Equipment depreciation
     * @param depreciationReportClosingMonth depreciation report closing month
     */
    private void setEquipmentCostDepreciationValue(final Equipment equipment,
            final EquipmentDepreciation equipmentDepreciation,
            final Date depreciationReportClosingMonth) {
        final BigDecimal costDepreciationValue =
                new BigDecimal(equipment.getCostDepValue()).setScale(2, BigDecimal.ROUND_HALF_DOWN);
        final BigDecimal currentValue = new BigDecimal(equipmentDepreciation.getValueCurrent())
            .setScale(2, BigDecimal.ROUND_HALF_DOWN);
        if (costDepreciationValue.compareTo(currentValue) != 0) {
            equipment.setDateDepreciationClosingMonth(depreciationReportClosingMonth);
            equipment.setCostDepValue(equipmentDepreciation.getValueCurrent());
        }
    }

    /**
     * Initializes per-request state variables.
     */
    private void initPerRequestState() {
        if (this.equipmentDao == null) {
            this.equipmentDao = new EquipmentDataSource();
        }
        if (this.furnitureDao == null) {
            this.furnitureDao = new FurnitureDataSource();
        }
        if (this.jobStatus == null) {
            this.jobStatus = this.status;
        }
    }
}
