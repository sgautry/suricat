package com.archibus.app.common.security.providers.dao;

import java.util.Locale;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.util.Assert;

import com.archibus.app.common.util.IMailSenderService;
import com.archibus.app.common.util.IMailSenderService.EmailParameters;
import com.archibus.config.ConfigManager;
import com.archibus.security.*;
import com.archibus.security.providers.dao.PasswordManager;
import com.archibus.utility.*;

/**
 * Password management methods. Used in PasswordManagerHandler and SecurityServiceImpl.
 *
 * @author Valery Tydykov
 *
 */
public class PasswordManagerImpl implements PasswordManager, InitializingBean {
    /**
     * Constant: value of Activivy ID.
     */
    private static final String ACTIVITY_ID_AB_SYSTEM_ADMINISTRATION = "AbSystemAdministration";

    /**
     * Constant: value of referencedBy.
     */
    private static final String REFERENCED_BY_PASSWORD_MANAGER_IMPL = "PASSWORDMANAGERIMPL";

    /**
     * Constant: value of message ID.
     */
    private static final String SEND_NEW_PASSWORD_BODY = "SEND_NEW_PASSWORD_BODY";

    /**
     * Constant: value of message ID.
     */
    private static final String SEND_NEW_PASSWORD_SUBJECT = "SEND_NEW_PASSWORD_SUBJECT";

    /**
     * Constant: value of message ID.
     */
    private static final String SEND_TEMPORARY_PASSWORD_BODY = "SEND_TEMPORARY_PASSWORD_BODY";

    /**
     * Constant: value of message ID.
     */
    private static final String SEND_TEMPORARY_PASSWORD_SUBJECT = "SEND_TEMPORARY_PASSWORD_SUBJECT";

    /**
     * Logger for this class and subclasses.
     */
    protected final Logger logger = Logger.getLogger(this.getClass());

    /**
     * Property: configManager.
     */
    private ConfigManager.Immutable configManager;

    /**
     * Property: mailSender.
     */
    private IMailSenderService mailSender;

    /**
     * Property: messagesDao. Used to load Email message templates.
     */
    private MessagesDao messagesDao;

    /**
     * Property: passwordEncoder.
     */
    private org.springframework.security.authentication.encoding.PasswordEncoder passwordEncoder;

    /**
     * Property: passwordGenerator. Used to generate temporary password.
     */
    private PasswordGenerator passwordGenerator;

    /**
     * Property: passwordPatternValidator. Validates generated temporary password.
     */
    private PasswordPatternValidator passwordPatternValidator;

    /**
     * Property: messagesDao. Used to load Email message templates. Optional.
     */
    private IPasswordPolicy passwordPolicy;

    /**
     * Property: saltSource, if provided, used to encrypt password. Optional.
     */
    private SaltSource saltSource;

    /**
     * Property: temporaryPasswordExpirationPeriod. Optional.
     */
    private int temporaryPasswordExpirationPeriod = 5;

    /**
     * Property: userDetailsService. Used to load Email message templates.
     */
    private UserDetailsService userDetailsService;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(this.userDetailsService, "userDetailsService must be set");
    }

    @Override
    public void encryptPassword(final String userId) throws ExceptionBase {
        // TODO add authorization: only user in role SYSTEM ADMIN can invoke
        // this method.

        if (this.logger.isInfoEnabled()) {
            this.logger.info("encryptPassword for username=[" + userId + "]");
        }

        Assert.notNull(this.passwordEncoder, "PasswordEncoder must be set");

        final UserDetailsImpl userDetails =
                (UserDetailsImpl) this.userDetailsService.loadUserByUsername(userId);
        final UserAccount.ThreadSafe userAccount =
                (UserAccount.ThreadSafe) userDetails.getUserAccount();

        // get clear text password
        final String password = userAccount.getPassword();

        // encode password
        // get salt
        Object salt = null;
        if (this.saltSource != null) {
            salt = this.saltSource.getSalt(userDetails);
        }

        final String newPasswordEncoded = this.passwordEncoder.encodePassword(password, salt);

        // Update password in user account. Do not update
        // userAccount.datePasswordChanged.
        userAccount.setPassword(newPasswordEncoded);
        userAccount.setPasswordChanged(true);

        // save
        UserAccountLoaderImpl.saveUserAccount(userAccount);
    }

    /**
     * Getter for the configManager property.
     *
     * @return the configManager property.
     */
    public ConfigManager.Immutable getConfigManager() {
        return this.configManager;
    }

    /**
     * Getter for the mailSender property.
     *
     * @return the mailSender property.
     */
    public IMailSenderService getMailSender() {
        return this.mailSender;
    }

    /**
     * Getter for the messagesDao property.
     *
     * @return the messagesDao property.
     */
    public MessagesDao getMessagesDao() {
        return this.messagesDao;
    }

    /**
     * Getter for the passwordEncoder property.
     *
     * @return the passwordEncoder property.
     */
    public org.springframework.security.authentication.encoding.PasswordEncoder getPasswordEncoder() {
        return this.passwordEncoder;
    }

    /**
     * Getter for the passwordGenerator property.
     *
     * @return the passwordGenerator property.
     */
    public PasswordGenerator getPasswordGenerator() {
        return this.passwordGenerator;
    }

    /**
     * Getter for the passwordPatternValidator property.
     *
     * @return the passwordPatternValidator property.
     */
    public PasswordPatternValidator getPasswordPatternValidator() {
        return this.passwordPatternValidator;
    }

    /**
     * Getter for the passwordPolicy property.
     *
     * @return the passwordPolicy property.
     */
    public IPasswordPolicy getPasswordPolicy() {
        return this.passwordPolicy;
    }

    /**
     * Setter for the passwordPolicy property.
     *
     * @param passwordPolicy the passwordPolicy to set.
     */
    public void setPasswordPolicy(final IPasswordPolicy passwordPolicy) {
        this.passwordPolicy = passwordPolicy;
    }

    /**
     * Getter for the saltSource property.
     *
     * @return the saltSource property.
     */
    public SaltSource getSaltSource() {
        return this.saltSource;
    }

    /**
     * Getter for the temporaryPasswordExpirationPeriod property.
     *
     * @return the temporaryPasswordExpirationPeriod property.
     */
    public int getTemporaryPasswordExpirationPeriod() {
        return this.temporaryPasswordExpirationPeriod;
    }

    /**
     * Getter for the userDetailsService property.
     *
     * @return the userDetailsService property.
     */
    public UserDetailsService getUserDetailsService() {
        return this.userDetailsService;
    }

    @Override
    public void requestNewPassword(final String arg0, final String arg1) throws ExceptionBase {
        // @non-translatable
        throw new ExceptionBase("Unsupported method: PasswordManagerImpl.requestNewPassword");
    }

    @Override
    public void resetPassword(final String userId, final String keyPhrase) throws ExceptionBase {
        // TODO add authorization: only user in role SYSTEM ADMIN can invoke
        // this method.

        // generates unique password value for the user.

        if (this.logger.isInfoEnabled()) {
            this.logger.info("resetPassword for username=[" + userId + "]");
        }

        Assert.notNull(this.passwordEncoder, "PasswordEncoder must be set");
        Assert.notNull(this.passwordPolicy, "passwordPolicy must be set");
        Assert.notNull(this.passwordGenerator, "passwordGenerator must be set");

        // load UserAccount
        final UserAccount.ThreadSafe userAccount = loadUserAccount(userId);

        final String newPassword = this.passwordGenerator.generatePassword(userId, keyPhrase);
        // validate new password pattern
        this.passwordPatternValidator.validate(newPassword);

        // update password in user account
        userAccount.changePassword(newPassword);

        {
            // set date password changed field to: current date + temporaryPasswordExpirationPeriod
            // -passwordExpirationPeriod , to make the temporary password expire in
            // temporaryPasswordExpirationPeriod days.
            java.util.Date datePasswordChanged =
                    DateTime.addDays(new java.util.Date(), this.temporaryPasswordExpirationPeriod);
            datePasswordChanged = DateTime.addDays(datePasswordChanged,
                -this.passwordPolicy.getPasswordExpirationPeriod());

            userAccount.setDatePasswordChanged(datePasswordChanged);
        }

        // save
        UserAccountLoaderImpl.saveUserAccount(userAccount);
    }

    @Override
    public void sendNewPassword(final String userId, final String password) throws ExceptionBase {
        // TODO add authorization: only user in role SYSTEM ADMIN can invoke
        // this method.

        // takes user's clear-text password and emails it to him
        if (this.logger.isInfoEnabled()) {
            this.logger.info("sendNewPassword for username=[" + userId + "]");
        }

        final UserAccount.ThreadSafe userAccount = this.loadUserAccount(userId);

        final EmailParameters emailParameters = this.prepareEmailParameters(userAccount, password,
            SEND_NEW_PASSWORD_SUBJECT, SEND_NEW_PASSWORD_BODY);

        this.mailSender.prepareAndSendEmail(emailParameters);
    }

    @Override
    public void sendTemporaryPassword(final String userId) throws ExceptionBase {
        // TODO add authorization: only user in role SYSTEM ADMIN can invoke
        // this method.

        // takes user�s clear-text password and emails it to him, asking to
        // change it as soon as possible

        if (this.logger.isInfoEnabled()) {
            this.logger.info("sendTemporaryPassword for username=[" + userId + "]");
        }

        final UserAccount.ThreadSafe userAccount = this.loadUserAccount(userId);
        final EmailParameters emailParameters = this.prepareEmailParameters(userAccount, null,
            SEND_TEMPORARY_PASSWORD_SUBJECT, SEND_TEMPORARY_PASSWORD_BODY);

        this.mailSender.prepareAndSendEmail(emailParameters);
    }

    /**
     * Setter for the configManager property.
     *
     * @param configManager the configManager to set.
     */
    public void setConfigManager(final ConfigManager.Immutable configManager) {
        this.configManager = configManager;
    }

    /**
     * Setter for the mailSender property.
     *
     * @param mailSender the mailSender to set.
     */
    public void setMailSender(final IMailSenderService mailSender) {
        this.mailSender = mailSender;
    }

    /**
     * Setter for the messagesDao property.
     *
     * @param messagesDao the messagesDao to set.
     */
    public void setMessagesDao(final MessagesDao messagesDao) {
        this.messagesDao = messagesDao;
    }

    /**
     * Setter for the passwordEncoder property.
     *
     * @param passwordEncoder the passwordEncoder to set.
     */
    public void setPasswordEncoder(
            final org.springframework.security.authentication.encoding.PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * Setter for the passwordGenerator property.
     *
     * @param passwordGenerator the passwordGenerator to set.
     */
    public void setPasswordGenerator(final PasswordGenerator passwordGenerator) {
        this.passwordGenerator = passwordGenerator;
    }

    /**
     * Setter for the passwordPatternValidator property.
     *
     * @param passwordPatternValidator the passwordPatternValidator to set.
     */
    public void setPasswordPatternValidator(
            final PasswordPatternValidator passwordPatternValidator) {
        this.passwordPatternValidator = passwordPatternValidator;
    }

    /**
     * Setter for the saltSource property.
     *
     * @param saltSource the saltSource to set.
     */
    public void setSaltSource(final SaltSource saltSource) {
        this.saltSource = saltSource;
    }

    /**
     * Setter for the temporaryPasswordExpirationPeriod property.
     *
     * @param temporaryPasswordExpirationPeriod the temporaryPasswordExpirationPeriod to set.
     */
    public void setTemporaryPasswordExpirationPeriod(final int temporaryPasswordExpirationPeriod) {
        this.temporaryPasswordExpirationPeriod = temporaryPasswordExpirationPeriod;
    }

    /**
     * Setter for the userDetailsService property.
     *
     * @param userDetailsService the userDetailsService to set.
     */
    public void setUserDetailsService(final UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    /**
     * Loads UserAccount for userId.
     *
     * @param userId of the UserAccount to be loaded.
     * @return loaded UserAccount.
     */
    private UserAccount.ThreadSafe loadUserAccount(final String userId) {
        final UserDetailsImpl userDetails =
                (UserDetailsImpl) this.userDetailsService.loadUserByUsername(userId);
        final UserAccount.ThreadSafe userAccount =
                (UserAccount.ThreadSafe) userDetails.getUserAccount();

        return userAccount;
    }

    /**
     * Prepares EmailParameters.
     *
     * @param userAccount of the user the email is prepared for.
     * @param password to be sent to the user.
     * @param subjectMessageId message ID of subject.
     * @param bodyMessageId message ID of body.
     * @return prepared EmailParameters.
     */
    private EmailParameters prepareEmailParameters(final UserAccount.Immutable userAccount,
            final String password, final String subjectMessageId, final String bodyMessageId) {
        Assert.notNull(this.messagesDao, "messagesDao must be set");

        final String subject =
                this.messagesDao.localizeMessage(ACTIVITY_ID_AB_SYSTEM_ADMINISTRATION,
                    REFERENCED_BY_PASSWORD_MANAGER_IMPL, subjectMessageId);
        final String body = this.messagesDao.localizeMessage(ACTIVITY_ID_AB_SYSTEM_ADMINISTRATION,
            REFERENCED_BY_PASSWORD_MANAGER_IMPL, bodyMessageId);

        // get locale, password and email from the account
        final Locale locale = userAccount.getLocale();
        final String userEmail = userAccount.getAttribute("/*/preferences/@email");
        // if password parameter was not supplied, get it from the user account
        String passwordToSend = password;
        if (StringUtil.isNullOrEmpty(passwordToSend)) {
            passwordToSend = userAccount.getPassword();
        }

        // prepare mail body: localize and insert password
        final String bodyLocalized = this.mailSender.prepareMessage(this.getClass().getName(), body,
            new Object[] { passwordToSend }, locale);

        final String subjectLocalized =
                this.mailSender.prepareMessage(this.getClass().getName(), subject, null, locale);

        final String administratorEMail = this.configManager.getAttribute(
            "/*/preferences/mail/addresses/address[@name='administratorEMail']/@value");
        Assert.hasLength(administratorEMail, "Administrator e-mail must be supplied");

        final EmailParameters emailParameters = new EmailParameters();
        emailParameters.setBody(bodyLocalized);
        emailParameters.setFrom(administratorEMail);
        emailParameters.setSubject(subjectLocalized);

        {
            String to = userEmail;
            // if no email was specified in user account, send to administratorEMail
            if (StringUtil.isNullOrEmpty(to)) {
                to = administratorEMail;
            }

            emailParameters.setTo(to);
        }

        return emailParameters;
    }
}
