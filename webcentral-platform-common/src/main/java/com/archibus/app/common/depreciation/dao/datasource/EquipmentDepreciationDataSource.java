package com.archibus.app.common.depreciation.dao.datasource;

import org.apache.commons.lang.ArrayUtils;

import com.archibus.app.common.depreciation.Constants;
import com.archibus.app.common.depreciation.domain.*;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;

/**
 * Equipment depreciation data source. Mapped to eq_dep database table.
 * <p>
 *
 *
 * @author Ioan Draghici
 * @since 23.1
 *
 */
public class EquipmentDepreciationDataSource
        extends AbstractDepreciationDataSource<EquipmentDepreciation> {

    /**
     * Field names to property names mapping. All fields will be added to the DataSource.
     * <p>
     * Only fields specific to Equipment depreciation are specified here, the common fields are
     * specified in the base class.
     */
    private static final String[][] FIELDS_TO_PROPERTIES = { { Constants.EQ_ID, "equipmentId" } };

    /**
     * Constructs EquipmentDepreciationDataSource, mapped to <code>eq_dep</code> table, using
     * <code>equipmentDepreciation</code> bean.
     */
    public EquipmentDepreciationDataSource() {
        super("equipmentDepreciation", "eq_dep");
    }

    @Override
    protected String[][] getFieldsToProperties() {
        // merge fieldsToProperties from the base class with FIELDS_TO_PROPERTIES in this class
        final String[][] fieldsToPropertiesMerged =
                (String[][]) ArrayUtils.addAll(super.getFieldsToProperties(), FIELDS_TO_PROPERTIES);

        return fieldsToPropertiesMerged;
    }

    /**
     * {@inheritDoc}
     * <p>
     * Suppress Warning PMD.AvoidUsingSql.
     *
     * <p>
     * Justification: Case 1.1 Statements with SELECT WHERE EXISTS ... pattern.
     */
    @Override
    @SuppressWarnings("PMD.AvoidUsingSql")
    public EquipmentDepreciation findCurrentDepreciation(final String equipmentId) {
        final DataSource dataSource = this.createCopy();
        dataSource.setApplyVpaRestrictions(false);
        dataSource.addRestriction(Restrictions.eq(this.tableName, Constants.EQ_ID, equipmentId));
        dataSource.addRestriction(Restrictions.sql(
            "eq_dep.report_id = (SELECT dep_reports.report_id FROM dep_reports WHERE eq_dep.report_id=dep_reports.report_id AND dep_reports.last_date = (SELECT MAX(dep_reports.last_date) FROM dep_reports WHERE dep_reports.active='yes'))"));
        final DataRecord record = dataSource.getRecord();
        return new DataSourceObjectConverter<EquipmentDepreciation>().convertRecordToObject(record,
            this.beanName, this.fieldToPropertyMapping, null);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * Suppress Warning PMD.AvoidUsingSql.
     *
     * <p>
     * Justification: Case 2.3. Statements with DELETE FROM ... pattern.
     *
     */

    @Override
    @SuppressWarnings("PMD.AvoidUsingSql")
    public void deleteDataForReport(final DepreciationReport depreciationReport) {
        final String deleteStatement = "DELETE FROM eq_dep WHERE eq_dep.report_id = "
                + SqlUtils.formatValueForSql(depreciationReport.getReportId());
        SqlUtils.executeUpdate(this.mainTableName, deleteStatement);
    }
}
