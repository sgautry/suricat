package com.archibus.app.common.util;

import java.sql.Time;
import java.util.*;

/**
 * Provides methods for splitting and merging java.util.Date (date and time) value into
 * java.sql.Date (date only) and java.sql.Time (time only).
 * <p>
 * Used in DAOs to implement mapping of business value (java.util.Date (date and time)) stored in
 * property of domain object to database fields (where Timestamp type is not supported).
 *
 * @author Valery Tydykov
 * @since 23.2
 */
public final class DateTimeUtilities {

    /**
     * Private default constructor: utility class is non-instantiable.
     */
    private DateTimeUtilities() {
    }

    /**
     * Converts date and time value to time only value.
     *
     * @param dateAndTime date and time value to be converted.
     * @return converted time only value.
     */
    public static Time toTime(final Date dateAndTime) {
        return new java.sql.Time(dateAndTime.getTime());
    }

    /**
     * Converts date and time value to date only value.
     *
     * @param dateAndTime date and time value to be converted.
     * @return converted date only value.
     */
    public static java.sql.Date toDate(final Date dateAndTime) {
        return new java.sql.Date(dateAndTime.getTime());
    }

    /**
     * Merges date value and time value into single date and time value.
     *
     * @param date date only value.
     * @param time time only value.
     * @return merged value with date and time.
     */
    public static Date mergeDateAndTime(final java.sql.Date date, final java.sql.Time time) {
        final Calendar dateAndTime = Calendar.getInstance();
        dateAndTime.setTime(date);
        final Calendar timeOnly = Calendar.getInstance();
        timeOnly.setTime(time);

        dateAndTime.set(Calendar.HOUR_OF_DAY, timeOnly.get(Calendar.HOUR_OF_DAY));
        dateAndTime.set(Calendar.MINUTE, timeOnly.get(Calendar.MINUTE));
        dateAndTime.set(Calendar.SECOND, timeOnly.get(Calendar.SECOND));

        return dateAndTime.getTime();
    }
}
