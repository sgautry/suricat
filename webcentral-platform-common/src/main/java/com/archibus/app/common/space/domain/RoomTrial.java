package com.archibus.app.common.space.domain;

/**
 * Domain object for RoomTrial.
 * <p>
 * Mapped to rm_trial table.
 *
 * @author Jikai Xu
 *
 */
public class RoomTrial {
	/**
	 * Building ID.
	 */
	private String buildingId;

	/**
	 * Floor ID.
	 */
	private String floorId;

	/**
	 * Room ID.
	 */
	private String roomId;

	/**
	 * Room name.
	 */
	private String name;

	/**
	 * layer name.
	 */
	private String layerName;

	/**
	 * Room type.
	 */
	private String roomType;

	/**
	 * Room category.
	 */
	private String roomCategory;

	/**
	 * Department ID.
	 */
	private String departmentId;

	/**
	 * Division ID.
	 */
	private String divisionId;

	/**
	 * Room standard.
	 */
	private String roomStandard;

	/**
	 * Room area.
	 */
	private Double area;

	/**
	 * Employee Capacity.
	 */
	private int employeeCapacity;

	/**
	 * Dwg name.
	 */
	private String dwgname;

	/**
	 * Trial project Id.
	 */
	private String trialProjectId;

	/**
	 * Entity Handle.
	 */
	private String ehandler;

	/**
	 * Getter for the buildingId property.
	 *
	 * @return the buildingId property.
	 */
	public String getBuildingId() {
		return this.buildingId;
	}

	/**
	 * Setter for the buildingId property.
	 *
	 * @param buildingId
	 *            the buildingId to set.
	 */
	public void setBuildingId(final String buildingId) {
		this.buildingId = buildingId;
	}

	/**
	 * Getter for the floorId property.
	 *
	 * @return the floorId property.
	 */
	public String getFloorId() {
		return this.floorId;
	}

	/**
	 * Setter for the floorId property.
	 *
	 * @param floorId
	 *            the floorId to set.
	 */
	public void setFloorId(final String floorId) {
		this.floorId = floorId;
	}

	/**
	 * Getter for the roomId property.
	 *
	 * @return the roomId property.
	 */
	public String getRoomId() {
		return this.roomId;
	}

	/**
	 * Setter for the roomId property.
	 *
	 * @param roomId
	 *            the roomId to set.
	 */
	public void setRoomId(final String roomId) {
		this.roomId = roomId;
	}

	/**
	 * Getter for the name property.
	 *
	 * @return the name property.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Setter for the name property.
	 *
	 * @param name
	 *            the name to set.
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Getter for the layerName property.
	 *
	 * @return the layerName property.
	 */
	public String getLayerName() {
		return this.layerName;
	}

	/**
	 * Setter for the layerName property.
	 *
	 * @param layerName
	 *            the layerName to set.
	 */
	public void setLayerName(final String layerName) {
		this.layerName = layerName;
	}

	/**
	 * Getter for the roomType property.
	 *
	 * @return the roomType property.
	 */
	public String getRoomType() {
		return this.roomType;
	}

	/**
	 * Setter for the roomType property.
	 *
	 * @param roomType
	 *            the roomType to set.
	 */
	public void setRoomType(final String roomType) {
		this.roomType = roomType;
	}

	/**
	 * Getter for the roomCategory property.
	 *
	 * @return the roomCategory property.
	 */
	public String getRoomCategory() {
		return this.roomCategory;
	}

	/**
	 * Setter for the roomCategory property.
	 *
	 * @param roomCategory
	 *            the roomCategory to set.
	 */
	public void setRoomCategory(final String roomCategory) {
		this.roomCategory = roomCategory;
	}

	/**
	 * Getter for the departmentId property.
	 *
	 * @return the departmentId property.
	 */
	public String getDepartmentId() {
		return this.departmentId;
	}

	/**
	 * Setter for the departmentId property.
	 *
	 * @param departmentId
	 *            the departmentId to set.
	 */
	public void setDepartmentId(final String departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * Getter for the divisionId property.
	 *
	 * @return the divisionId property.
	 */
	public String getDivisionId() {
		return this.divisionId;
	}

	/**
	 * Setter for the divisionId property.
	 *
	 * @param divisionId
	 *            the divisionId to set.
	 */
	public void setDivisionId(final String divisionId) {
		this.divisionId = divisionId;
	}

	/**
	 * Getter for the roomStandard property.
	 *
	 * @return the roomStandard property.
	 */
	public String getRoomStandard() {
		return this.roomStandard;
	}

	/**
	 * Setter for the roomStandard property.
	 *
	 * @param roomStandard
	 *            the roomStandard to set.
	 */
	public void setRoomStandard(final String roomStandard) {
		this.roomStandard = roomStandard;
	}

	/**
	 * Getter for the area property.
	 *
	 * @return the area property.
	 */
	public Double getArea() {
		return this.area;
	}

	/**
	 * Setter for the area property.
	 *
	 * @param area
	 *            the area to set.
	 */
	public void setArea(final Double area) {
		this.area = area;
	}

	/**
	 * Getter for the employeeCapacity property.
	 *
	 * @return the employeeCapacity property.
	 */
	public int getEmployeeCapacity() {
		return this.employeeCapacity;
	}

	/**
	 * Setter for the employeeCapacity property.
	 *
	 * @param employeeCapacity
	 *            the employeeCapacity to set.
	 */
	public void setEmployeeCapacity(final int employeeCapacity) {
		this.employeeCapacity = employeeCapacity;
	}

	/**
	 * Getter for the dwgname property.
	 *
	 * @return the dwgname property.
	 */
	public String getDwgname() {
		return this.dwgname;
	}

	/**
	 * Setter for the dwgname property.
	 *
	 * @param dwgname
	 *            the dwgname to set.
	 */
	public void setDwgname(final String dwgname) {
		this.dwgname = dwgname;
	}

	/**
	 * Getter for the trialProjectId property.
	 *
	 * @return the trialProjectId property.
	 */
	public String getTrialProjectId() {
		return this.trialProjectId;
	}

	/**
	 * Setter for the trialProjectId property.
	 *
	 * @param trialProjectId
	 *            the trialProjectId to set.
	 */
	public void setTrialProjectId(final String trialProjectId) {
		this.trialProjectId = trialProjectId;
	}

	/**
	 * Getter for the ehandler property.
	 *
	 * @return the ehandler property.
	 */
	public String getEhandler() {
		return this.ehandler;
	}

	/**
	 * Setter for the ehandler property.
	 *
	 * @param ehandler
	 *            the ehandler to set.
	 */
	public void setEhandler(final String ehandler) {
		this.ehandler = ehandler;
	}

}
