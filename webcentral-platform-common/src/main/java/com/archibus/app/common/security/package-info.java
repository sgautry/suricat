/**
 * This package contains security-related API and implementations, which are shared between
 * applications.
 *
 **/
package com.archibus.app.common.security;
