package com.archibus.app.common.finance.domain;

import java.util.Date;

import com.archibus.utility.Utility;

/**
 * Domain object for ComplianceContractCost.
 * <p>
 * Mapped to compliance_contract_costs table.
 *
 * @author Razvan Croitoru
 *         <p>
 *         SuppressWarnings Justification: this class represent a data base
 *         record. Using nested classes will make the code difficult to
 *         understand.
 *         <p>
 */
@SuppressWarnings({ "PMD.ExcessivePublicCount", "PMD.TooManyFields", "PMD.ConfusingTernary",
		"PMD.ExcessiveClassLength" })
public class ComplianceContractCost {

	/**
	 * Class name constant.
	 */
	static final String CLASS_COMPLIANCE_COST = "ComplianceContractCost";

	// ----------------------- persistent state --------------------------------
	/**
	 * Field.
	 */
	private String assignedRole;

	/**
	 * Field.
	 */
	private String costCatId;

	/**
	 * Field cost_code.
	 */
	private int id;

	/**
	 * Field.
	 */
	private double costCommBaseBudget;

	/**
	 * Field.
	 */
	private double costCommBasePayment;

	/**
	 * Field.
	 */
	private double costCommBudget;

	/**
	 * Field.
	 */
	private double costCommTotalPayment;

	/**
	 * Field.
	 */
	private double costCommVatBudget;

	/**
	 * Field.
	 */
	private double costCommVatPayment;

	/**
	 * Field.
	 */
	private double costMaxCommBaseBudget;

	/**
	 * Field.
	 */
	private double costMaxCommBasePayment;

	/**
	 * Field.
	 */
	private double costMaxCommBudget;

	/**
	 * Field.
	 */
	private double costMaxCommTotalPayment;

	/**
	 * Field.
	 */
	private double costMaxCommVatBudget;

	/**
	 * Field.
	 */
	private double costMaxCommVatPayment;

	/**
	 * Field.
	 */
	private int costMaxDesc;

	/**
	 * Field.
	 */
	private double costUnitCommBaseBudget;

	/**
	 * Field.
	 */
	private double costUnitCommBasePayment;

	/**
	 * Field.
	 */
	private double costUnitCommBudget;

	/**
	 * Field.
	 */
	private double costUnitCommTotalPayment;

	/**
	 * Field.
	 */
	private double costUnitCommVatBudget;

	/**
	 * Field.
	 */
	private double costUnitCommVatPayment;

	/**
	 * Field.
	 */
	private String ctryId;

	/**
	 * Field.
	 */
	private String currencyBudget;

	/**
	 * Field.
	 */
	private String currencyPayment;

	/**
	 * Field.
	 */
	private Date dateUsedForMcBudget;

	/**
	 * Field.
	 */
	private Date dateUsedForMcPayment;

	/**
	 * Field.
	 */
	private String description;

	/**
	 * Field.
	 */
	private double exchangeRateBudget;

	/**
	 * Field.
	 */
	private double exchangeRateOverride;

	/**
	 * Field.
	 */
	private double exchangeRatePayment;

	/**
	 * Field.
	 */
	private double hoursMax;

	/**
	 * Field.
	 */
	private int hoursMaxDesc;

	/**
	 * Field.
	 */
	private double paymentCommBaseBudget;

	/**
	 * Field.
	 */
	private double paymentCommBasePayment;

	/**
	 * Field.
	 */
	private double paymentCommBudget;

	/**
	 * Field.
	 */
	private double paymentCommTotalPayment;

	/**
	 * Field.
	 */
	private double paymentCommVatBudget;

	/**
	 * Field.
	 */
	private double paymentCommVatPayment;

	/**
	 * Field.
	 */
	private int paymentDeferrable;

	/**
	 * Field.
	 */
	private int paymentFrequency;

	/**
	 * Field.
	 */
	private String probType;

	/**
	 * Field.
	 */
	private double rateDoubleBaseBudget;

	/**
	 * Field.
	 */
	private double rateDoubleBasePayment;

	/**
	 * Field.
	 */
	private double rateDoubleBudget;

	/**
	 * Field.
	 */
	private double rateDoubleTotalPayment;

	/**
	 * Field.
	 */
	private double rateDoubleVatBudget;

	/**
	 * Field.
	 */
	private double rateDoubleVatPayment;

	/**
	 * Field.
	 */
	private double rateHourlyBaseBudget;

	/**
	 * Field.
	 */
	private double rateHourlyBasePayment;

	/**
	 * Field.
	 */
	private double rateHourlyBudget;

	/**
	 * Field.
	 */
	private double rateHourlyTotalPayment;

	/**
	 * Field.
	 */
	private double rateHourlyVatBudget;

	/**
	 * Field.
	 */
	private double rateHourlyVatPayment;

	/**
	 * Field.
	 */
	private double rateOverBaseBudget;

	/**
	 * Field.
	 */
	private double rateOverBasePayment;

	/**
	 * Field.
	 */
	private double rateOverBudget;

	/**
	 * Field.
	 */
	private double rateOverTotalPayment;

	/**
	 * Field.
	 */
	private double rateOverVatBudget;

	/**
	 * Field.
	 */
	private double rateOverVatPayment;

	/**
	 * Field.
	 */
	private String regProgram;

	/**
	 * Field.
	 */
	private String regRequirement;

	/**
	 * Field.
	 */
	private String regulation;

	/**
	 * Field.
	 */
	private String servWindowDays;

	/**
	 * Field.
	 */
	private String servWindowMonths;

	/**
	 * Field.
	 */
	private String trId;

	/**
	 * Field.
	 */
	private int unitCount;

	/**
	 * Field.
	 */
	private double vatAmountOverride;

	/**
	 * Field.
	 */
	private double vatPercentOverride;

	/**
	 * Field.
	 */
	private double vatPercentValue;

	/**
	 * Field.
	 */
	private int workType;

	// ----------------------- business methods --------------------------------

	@Override
	public int hashCode() {
		int result = Utility.HASH_CODE_SEED;
		result = Utility.HASH_CODE_PRIME * result + this.id;
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		return obj instanceof ComplianceContractCost && this.id == ((ComplianceContractCost) obj).getId()
				&& this.getCostClass().equals(((ComplianceContractCost) obj).getCostClass());
	}

	/**
	 * Return Cost class type.
	 *
	 * @return class name costant.
	 */
	public String getCostClass() {
		return CLASS_COMPLIANCE_COST;
	}

	// ----------------------- Getters and Setters ----------------------------

	/**
	 * Getter for the assignedRole property.
	 *
	 * @return the assignedRole property.
	 */
	public String getAssignedRole() {
		return this.assignedRole;
	}

	/**
	 * Setter for the assignedRole property.
	 *
	 * @param assignedRole
	 *            the assignedRole to set.
	 */
	public void setAssignedRole(final String assignedRole) {
		this.assignedRole = assignedRole;
	}

	/**
	 * Getter for the costCatId property.
	 *
	 * @return the costCatId property.
	 */
	public String getCostCatId() {
		return this.costCatId;
	}

	/**
	 * Setter for the costCatId property.
	 *
	 * @param costCatId
	 *            the costCatId to set.
	 */
	public void setCostCatId(final String costCatId) {
		this.costCatId = costCatId;
	}

	/**
	 * Getter for the costCommBaseBudget property.
	 *
	 * @return the costCommBaseBudget property.
	 */
	public double getCostCommBaseBudget() {
		return this.costCommBaseBudget;
	}

	/**
	 * Setter for the costCommBaseBudget property.
	 *
	 * @param costCommBaseBudget
	 *            the costCommBaseBudget to set.
	 */
	public void setCostCommBaseBudget(final double costCommBaseBudget) {
		this.costCommBaseBudget = costCommBaseBudget;
	}

	/**
	 * Getter for the costCommBasePayment property.
	 *
	 * @return the costCommBasePayment property.
	 */
	public double getCostCommBasePayment() {
		return this.costCommBasePayment;
	}

	/**
	 * Setter for the costCommBasePayment property.
	 *
	 * @param costCommBasePayment
	 *            the costCommBasePayment to set.
	 */
	public void setCostCommBasePayment(final double costCommBasePayment) {
		this.costCommBasePayment = costCommBasePayment;
	}

	/**
	 * Getter for the costCommBudget property.
	 *
	 * @return the costCommBudget property.
	 */
	public double getCostCommBudget() {
		return this.costCommBudget;
	}

	/**
	 * Setter for the costCommBudget property.
	 *
	 * @param costCommBudget
	 *            the costCommBudget to set.
	 */
	public void setCostCommBudget(final double costCommBudget) {
		this.costCommBudget = costCommBudget;
	}

	/**
	 * Getter for the costCommTotalPayment property.
	 *
	 * @return the costCommTotalPayment property.
	 */
	public double getCostCommTotalPayment() {
		return this.costCommTotalPayment;
	}

	/**
	 * Setter for the costCommTotalPayment property.
	 *
	 * @param costCommTotalPayment
	 *            the costCommTotalPayment to set.
	 */
	public void setCostCommTotalPayment(final double costCommTotalPayment) {
		this.costCommTotalPayment = costCommTotalPayment;
	}

	/**
	 * Getter for the costCommVatBudget property.
	 *
	 * @return the costCommVatBudget property.
	 */
	public double getCostCommVatBudget() {
		return this.costCommVatBudget;
	}

	/**
	 * Setter for the costCommVatBudget property.
	 *
	 * @param costCommVatBudget
	 *            the costCommVatBudget to set.
	 */
	public void setCostCommVatBudget(final double costCommVatBudget) {
		this.costCommVatBudget = costCommVatBudget;
	}

	/**
	 * Getter for the costCommVatPayment property.
	 *
	 * @return the costCommVatPayment property.
	 */
	public double getCostCommVatPayment() {
		return this.costCommVatPayment;
	}

	/**
	 * Setter for the costCommVatPayment property.
	 *
	 * @param costCommVatPayment
	 *            the costCommVatPayment to set.
	 */
	public void setCostCommVatPayment(final double costCommVatPayment) {
		this.costCommVatPayment = costCommVatPayment;
	}

	/**
	 * Getter for the costMaxCommBaseBudget property.
	 *
	 * @return the costMaxCommBaseBudget property.
	 */
	public double getCostMaxCommBaseBudget() {
		return this.costMaxCommBaseBudget;
	}

	/**
	 * Setter for the costMaxCommBaseBudget property.
	 *
	 * @param costMaxCommBaseBudget
	 *            the costMaxCommBaseBudget to set.
	 */
	public void setCostMaxCommBaseBudget(final double costMaxCommBaseBudget) {
		this.costMaxCommBaseBudget = costMaxCommBaseBudget;
	}

	/**
	 * Getter for the costMaxCommBasePayment property.
	 *
	 * @return the costMaxCommBasePayment property.
	 */
	public double getCostMaxCommBasePayment() {
		return this.costMaxCommBasePayment;
	}

	/**
	 * Setter for the costMaxCommBasePayment property.
	 *
	 * @param costMaxCommBasePayment
	 *            the costMaxCommBasePayment to set.
	 */
	public void setCostMaxCommBasePayment(final double costMaxCommBasePayment) {
		this.costMaxCommBasePayment = costMaxCommBasePayment;
	}

	/**
	 * Getter for the costMaxCommBudget property.
	 *
	 * @return the costMaxCommBudget property.
	 */
	public double getCostMaxCommBudget() {
		return this.costMaxCommBudget;
	}

	/**
	 * Setter for the costMaxCommBudget property.
	 *
	 * @param costMaxCommBudget
	 *            the costMaxCommBudget to set.
	 */
	public void setCostMaxCommBudget(final double costMaxCommBudget) {
		this.costMaxCommBudget = costMaxCommBudget;
	}

	/**
	 * Getter for the costMaxCommTotalPayment property.
	 *
	 * @return the costMaxCommTotalPayment property.
	 */
	public double getCostMaxCommTotalPayment() {
		return this.costMaxCommTotalPayment;
	}

	/**
	 * Setter for the costMaxCommTotalPayment property.
	 *
	 * @param costMaxCommTotalPayment
	 *            the costMaxCommTotalPayment to set.
	 */
	public void setCostMaxCommTotalPayment(final double costMaxCommTotalPayment) {
		this.costMaxCommTotalPayment = costMaxCommTotalPayment;
	}

	/**
	 * Getter for the costMaxCommVatBudget property.
	 *
	 * @return the costMaxCommVatBudget property.
	 */
	public double getCostMaxCommVatBudget() {
		return this.costMaxCommVatBudget;
	}

	/**
	 * Setter for the costMaxCommVatBudget property.
	 *
	 * @param costMaxCommVatBudget
	 *            the costMaxCommVatBudget to set.
	 */
	public void setCostMaxCommVatBudget(final double costMaxCommVatBudget) {
		this.costMaxCommVatBudget = costMaxCommVatBudget;
	}

	/**
	 * Getter for the costMaxCommVatPayment property.
	 *
	 * @return the costMaxCommVatPayment property.
	 */
	public double getCostMaxCommVatPayment() {
		return this.costMaxCommVatPayment;
	}

	/**
	 * Setter for the costMaxCommVatPayment property.
	 *
	 * @param costMaxCommVatPayment
	 *            the costMaxCommVatPayment to set.
	 */
	public void setCostMaxCommVatPayment(final double costMaxCommVatPayment) {
		this.costMaxCommVatPayment = costMaxCommVatPayment;
	}

	/**
	 * Getter for the costMaxDesc property.
	 *
	 * @return the costMaxDesc property.
	 */
	public int getCostMaxDesc() {
		return this.costMaxDesc;
	}

	/**
	 * Setter for the costMaxDesc property.
	 *
	 * @param costMaxDesc
	 *            the costMaxDesc to set.
	 */
	public void setCostMaxDesc(final int costMaxDesc) {
		this.costMaxDesc = costMaxDesc;
	}

	/**
	 * Getter for the costUnitCommBaseBudget property.
	 *
	 * @return the costUnitCommBaseBudget property.
	 */
	public double getCostUnitCommBaseBudget() {
		return this.costUnitCommBaseBudget;
	}

	/**
	 * Setter for the costUnitCommBaseBudget property.
	 *
	 * @param costUnitCommBaseBudget
	 *            the costUnitCommBaseBudget to set.
	 */
	public void setCostUnitCommBaseBudget(final double costUnitCommBaseBudget) {
		this.costUnitCommBaseBudget = costUnitCommBaseBudget;
	}

	/**
	 * Getter for the costUnitCommBasePayment property.
	 *
	 * @return the costUnitCommBasePayment property.
	 */
	public double getCostUnitCommBasePayment() {
		return this.costUnitCommBasePayment;
	}

	/**
	 * Setter for the costUnitCommBasePayment property.
	 *
	 * @param costUnitCommBasePayment
	 *            the costUnitCommBasePayment to set.
	 */
	public void setCostUnitCommBasePayment(final double costUnitCommBasePayment) {
		this.costUnitCommBasePayment = costUnitCommBasePayment;
	}

	/**
	 * Getter for the costUnitCommBudget property.
	 *
	 * @return the costUnitCommBudget property.
	 */
	public double getCostUnitCommBudget() {
		return this.costUnitCommBudget;
	}

	/**
	 * Setter for the costUnitCommBudget property.
	 *
	 * @param costUnitCommBudget
	 *            the costUnitCommBudget to set.
	 */
	public void setCostUnitCommBudget(final double costUnitCommBudget) {
		this.costUnitCommBudget = costUnitCommBudget;
	}

	/**
	 * Getter for the costUnitCommTotalPayment property.
	 *
	 * @return the costUnitCommTotalPayment property.
	 */
	public double getCostUnitCommTotalPayment() {
		return this.costUnitCommTotalPayment;
	}

	/**
	 * Setter for the costUnitCommTotalPayment property.
	 *
	 * @param costUnitCommTotalPayment
	 *            the costUnitCommTotalPayment to set.
	 */
	public void setCostUnitCommTotalPayment(final double costUnitCommTotalPayment) {
		this.costUnitCommTotalPayment = costUnitCommTotalPayment;
	}

	/**
	 * Getter for the costUnitCommVatBudget property.
	 *
	 * @return the costUnitCommVatBudget property.
	 */
	public double getCostUnitCommVatBudget() {
		return this.costUnitCommVatBudget;
	}

	/**
	 * Setter for the costUnitCommVatBudget property.
	 *
	 * @param costUnitCommVatBudget
	 *            the costUnitCommVatBudget to set.
	 */
	public void setCostUnitCommVatBudget(final double costUnitCommVatBudget) {
		this.costUnitCommVatBudget = costUnitCommVatBudget;
	}

	/**
	 * Getter for the costUnitCommVatPayment property.
	 *
	 * @return the costUnitCommVatPayment property.
	 */
	public double getCostUnitCommVatPayment() {
		return this.costUnitCommVatPayment;
	}

	/**
	 * Setter for the costUnitCommVatPayment property.
	 *
	 * @param costUnitCommVatPayment
	 *            the costUnitCommVatPayment to set.
	 */
	public void setCostUnitCommVatPayment(final double costUnitCommVatPayment) {
		this.costUnitCommVatPayment = costUnitCommVatPayment;
	}

	/**
	 * Getter for the ctryId property.
	 *
	 * @return the ctryId property.
	 */
	public String getCtryId() {
		return this.ctryId;
	}

	/**
	 * Setter for the ctryId property.
	 *
	 * @param ctryId
	 *            the ctryId to set.
	 */
	public void setCtryId(final String ctryId) {
		this.ctryId = ctryId;
	}

	/**
	 * Getter for the currencyBudget property.
	 *
	 * @return the currencyBudget property.
	 */
	public String getCurrencyBudget() {
		return this.currencyBudget;
	}

	/**
	 * Setter for the currencyBudget property.
	 *
	 * @param currencyBudget
	 *            the currencyBudget to set.
	 */
	public void setCurrencyBudget(final String currencyBudget) {
		this.currencyBudget = currencyBudget;
	}

	/**
	 * Getter for the currencyPayment property.
	 *
	 * @return the currencyPayment property.
	 */
	public String getCurrencyPayment() {
		return this.currencyPayment;
	}

	/**
	 * Setter for the currencyPayment property.
	 *
	 * @param currencyPayment
	 *            the currencyPayment to set.
	 */
	public void setCurrencyPayment(final String currencyPayment) {
		this.currencyPayment = currencyPayment;
	}

	/**
	 * Getter for the dateUsedForMcBudget property.
	 *
	 * @return the dateUsedForMcBudget property.
	 */
	public Date getDateUsedForMcBudget() {
		return this.dateUsedForMcBudget;
	}

	/**
	 * Setter for the dateUsedForMcBudget property.
	 *
	 * @param dateUsedForMcBudget
	 *            the dateUsedForMcBudget to set.
	 */
	public void setDateUsedForMcBudget(final Date dateUsedForMcBudget) {
		this.dateUsedForMcBudget = dateUsedForMcBudget;
	}

	/**
	 * Getter for the dateUsedForMcPayment property.
	 *
	 * @return the dateUsedForMcPayment property.
	 */
	public Date getDateUsedForMcPayment() {
		return this.dateUsedForMcPayment;
	}

	/**
	 * Setter for the dateUsedForMcPayment property.
	 *
	 * @param dateUsedForMcPayment
	 *            the dateUsedForMcPayment to set.
	 */
	public void setDateUsedForMcPayment(final Date dateUsedForMcPayment) {
		this.dateUsedForMcPayment = dateUsedForMcPayment;
	}

	/**
	 * Getter for the description property.
	 *
	 * @return the description property.
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Setter for the description property.
	 *
	 * @param description
	 *            the description to set.
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Getter for the exchangeRateBudget property.
	 *
	 * @return the exchangeRateBudget property.
	 */
	public double getExchangeRateBudget() {
		return this.exchangeRateBudget;
	}

	/**
	 * Setter for the exchangeRateBudget property.
	 *
	 * @param exchangeRateBudget
	 *            the exchangeRateBudget to set.
	 */
	public void setExchangeRateBudget(final double exchangeRateBudget) {
		this.exchangeRateBudget = exchangeRateBudget;
	}

	/**
	 * Getter for the exchangeRateOverride property.
	 *
	 * @return the exchangeRateOverride property.
	 */
	public double getExchangeRateOverride() {
		return this.exchangeRateOverride;
	}

	/**
	 * Setter for the exchangeRateOverride property.
	 *
	 * @param exchangeRateOverride
	 *            the exchangeRateOverride to set.
	 */
	public void setExchangeRateOverride(final double exchangeRateOverride) {
		this.exchangeRateOverride = exchangeRateOverride;
	}

	/**
	 * Getter for the exchangeRatePayment property.
	 *
	 * @return the exchangeRatePayment property.
	 */
	public double getExchangeRatePayment() {
		return this.exchangeRatePayment;
	}

	/**
	 * Setter for the exchangeRatePayment property.
	 *
	 * @param exchangeRatePayment
	 *            the exchangeRatePayment to set.
	 */
	public void setExchangeRatePayment(final double exchangeRatePayment) {
		this.exchangeRatePayment = exchangeRatePayment;
	}

	/**
	 * Getter for the hoursMax property.
	 *
	 * @return the hoursMax property.
	 */
	public double getHoursMax() {
		return this.hoursMax;
	}

	/**
	 * Setter for the hoursMax property.
	 *
	 * @param hoursMax
	 *            the hoursMax to set.
	 */
	public void setHoursMax(final double hoursMax) {
		this.hoursMax = hoursMax;
	}

	/**
	 * Getter for the hoursMaxDesc property.
	 *
	 * @return the hoursMaxDesc property.
	 */
	public int getHoursMaxDesc() {
		return this.hoursMaxDesc;
	}

	/**
	 * Setter for the hoursMaxDesc property.
	 *
	 * @param hoursMaxDesc
	 *            the hoursMaxDesc to set.
	 */
	public void setHoursMaxDesc(final int hoursMaxDesc) {
		this.hoursMaxDesc = hoursMaxDesc;
	}

	/**
	 * Getter for the paymentCommBaseBudget property.
	 *
	 * @return the paymentCommBaseBudget property.
	 */
	public double getPaymentCommBaseBudget() {
		return this.paymentCommBaseBudget;
	}

	/**
	 * Setter for the paymentCommBaseBudget property.
	 *
	 * @param paymentCommBaseBudget
	 *            the paymentCommBaseBudget to set.
	 */
	public void setPaymentCommBaseBudget(final double paymentCommBaseBudget) {
		this.paymentCommBaseBudget = paymentCommBaseBudget;
	}

	/**
	 * Getter for the paymentCommBasePayment property.
	 *
	 * @return the paymentCommBasePayment property.
	 */
	public double getPaymentCommBasePayment() {
		return this.paymentCommBasePayment;
	}

	/**
	 * Setter for the paymentCommBasePayment property.
	 *
	 * @param paymentCommBasePayment
	 *            the paymentCommBasePayment to set.
	 */
	public void setPaymentCommBasePayment(final double paymentCommBasePayment) {
		this.paymentCommBasePayment = paymentCommBasePayment;
	}

	/**
	 * Getter for the paymentCommBudget property.
	 *
	 * @return the paymentCommBudget property.
	 */
	public double getPaymentCommBudget() {
		return this.paymentCommBudget;
	}

	/**
	 * Setter for the paymentCommBudget property.
	 *
	 * @param paymentCommBudget
	 *            the paymentCommBudget to set.
	 */
	public void setPaymentCommBudget(final double paymentCommBudget) {
		this.paymentCommBudget = paymentCommBudget;
	}

	/**
	 * Getter for the paymentCommTotalPayment property.
	 *
	 * @return the paymentCommTotalPayment property.
	 */
	public double getPaymentCommTotalPayment() {
		return this.paymentCommTotalPayment;
	}

	/**
	 * Setter for the paymentCommTotalPayment property.
	 *
	 * @param paymentCommTotalPayment
	 *            the paymentCommTotalPayment to set.
	 */
	public void setPaymentCommTotalPayment(final double paymentCommTotalPayment) {
		this.paymentCommTotalPayment = paymentCommTotalPayment;
	}

	/**
	 * Getter for the paymentCommVatBudget property.
	 *
	 * @return the paymentCommVatBudget property.
	 */
	public double getPaymentCommVatBudget() {
		return this.paymentCommVatBudget;
	}

	/**
	 * Setter for the paymentCommVatBudget property.
	 *
	 * @param paymentCommVatBudget
	 *            the paymentCommVatBudget to set.
	 */
	public void setPaymentCommVatBudget(final double paymentCommVatBudget) {
		this.paymentCommVatBudget = paymentCommVatBudget;
	}

	/**
	 * Getter for the paymentCommVatPayment property.
	 *
	 * @return the paymentCommVatPayment property.
	 */
	public double getPaymentCommVatPayment() {
		return this.paymentCommVatPayment;
	}

	/**
	 * Setter for the paymentCommVatPayment property.
	 *
	 * @param paymentCommVatPayment
	 *            the paymentCommVatPayment to set.
	 */
	public void setPaymentCommVatPayment(final double paymentCommVatPayment) {
		this.paymentCommVatPayment = paymentCommVatPayment;
	}

	/**
	 * Getter for the paymentDeferrable property.
	 *
	 * @return the paymentDeferrable property.
	 */
	public int getPaymentDeferrable() {
		return this.paymentDeferrable;
	}

	/**
	 * Setter for the paymentDeferrable property.
	 *
	 * @param paymentDeferrable
	 *            the paymentDeferrable to set.
	 */
	public void setPaymentDeferrable(final int paymentDeferrable) {
		this.paymentDeferrable = paymentDeferrable;
	}

	/**
	 * Getter for the paymentFrequency property.
	 *
	 * @return the paymentFrequency property.
	 */
	public int getPaymentFrequency() {
		return this.paymentFrequency;
	}

	/**
	 * Setter for the paymentFrequency property.
	 *
	 * @param paymentFrequency
	 *            the paymentFrequency to set.
	 */
	public void setPaymentFrequency(final int paymentFrequency) {
		this.paymentFrequency = paymentFrequency;
	}

	/**
	 * Getter for the probType property.
	 *
	 * @return the probType property.
	 */
	public String getProbType() {
		return this.probType;
	}

	/**
	 * Setter for the probType property.
	 *
	 * @param probType
	 *            the probType to set.
	 */
	public void setProbType(final String probType) {
		this.probType = probType;
	}

	/**
	 * Getter for the rateDoubleBaseBudget property.
	 *
	 * @return the rateDoubleBaseBudget property.
	 */
	public double getRateDoubleBaseBudget() {
		return this.rateDoubleBaseBudget;
	}

	/**
	 * Setter for the rateDoubleBaseBudget property.
	 *
	 * @param rateDoubleBaseBudget
	 *            the rateDoubleBaseBudget to set.
	 */
	public void setRateDoubleBaseBudget(final double rateDoubleBaseBudget) {
		this.rateDoubleBaseBudget = rateDoubleBaseBudget;
	}

	/**
	 * Getter for the rateDoubleBasePayment property.
	 *
	 * @return the rateDoubleBasePayment property.
	 */
	public double getRateDoubleBasePayment() {
		return this.rateDoubleBasePayment;
	}

	/**
	 * Setter for the rateDoubleBasePayment property.
	 *
	 * @param rateDoubleBasePayment
	 *            the rateDoubleBasePayment to set.
	 */
	public void setRateDoubleBasePayment(final double rateDoubleBasePayment) {
		this.rateDoubleBasePayment = rateDoubleBasePayment;
	}

	/**
	 * Getter for the rateDoubleBudget property.
	 *
	 * @return the rateDoubleBudget property.
	 */
	public double getRateDoubleBudget() {
		return this.rateDoubleBudget;
	}

	/**
	 * Setter for the rateDoubleBudget property.
	 *
	 * @param rateDoubleBudget
	 *            the rateDoubleBudget to set.
	 */
	public void setRateDoubleBudget(final double rateDoubleBudget) {
		this.rateDoubleBudget = rateDoubleBudget;
	}

	/**
	 * Getter for the rateDoubleTotalPayment property.
	 *
	 * @return the rateDoubleTotalPayment property.
	 */
	public double getRateDoubleTotalPayment() {
		return this.rateDoubleTotalPayment;
	}

	/**
	 * Setter for the rateDoubleTotalPayment property.
	 *
	 * @param rateDoubleTotalPayment
	 *            the rateDoubleTotalPayment to set.
	 */
	public void setRateDoubleTotalPayment(final double rateDoubleTotalPayment) {
		this.rateDoubleTotalPayment = rateDoubleTotalPayment;
	}

	/**
	 * Getter for the rateDoubleVatBudget property.
	 *
	 * @return the rateDoubleVatBudget property.
	 */
	public double getRateDoubleVatBudget() {
		return this.rateDoubleVatBudget;
	}

	/**
	 * Setter for the rateDoubleVatBudget property.
	 *
	 * @param rateDoubleVatBudget
	 *            the rateDoubleVatBudget to set.
	 */
	public void setRateDoubleVatBudget(final double rateDoubleVatBudget) {
		this.rateDoubleVatBudget = rateDoubleVatBudget;
	}

	/**
	 * Getter for the rateDoubleVatPayment property.
	 *
	 * @return the rateDoubleVatPayment property.
	 */
	public double getRateDoubleVatPayment() {
		return this.rateDoubleVatPayment;
	}

	/**
	 * Setter for the rateDoubleVatPayment property.
	 *
	 * @param rateDoubleVatPayment
	 *            the rateDoubleVatPayment to set.
	 */
	public void setRateDoubleVatPayment(final double rateDoubleVatPayment) {
		this.rateDoubleVatPayment = rateDoubleVatPayment;
	}

	/**
	 * Getter for the rateHourlyBaseBudget property.
	 *
	 * @return the rateHourlyBaseBudget property.
	 */
	public double getRateHourlyBaseBudget() {
		return this.rateHourlyBaseBudget;
	}

	/**
	 * Setter for the rateHourlyBaseBudget property.
	 *
	 * @param rateHourlyBaseBudget
	 *            the rateHourlyBaseBudget to set.
	 */
	public void setRateHourlyBaseBudget(final double rateHourlyBaseBudget) {
		this.rateHourlyBaseBudget = rateHourlyBaseBudget;
	}

	/**
	 * Getter for the rateHourlyBasePayment property.
	 *
	 * @return the rateHourlyBasePayment property.
	 */
	public double getRateHourlyBasePayment() {
		return this.rateHourlyBasePayment;
	}

	/**
	 * Setter for the rateHourlyBasePayment property.
	 *
	 * @param rateHourlyBasePayment
	 *            the rateHourlyBasePayment to set.
	 */
	public void setRateHourlyBasePayment(final double rateHourlyBasePayment) {
		this.rateHourlyBasePayment = rateHourlyBasePayment;
	}

	/**
	 * Getter for the rateHourlyBudget property.
	 *
	 * @return the rateHourlyBudget property.
	 */
	public double getRateHourlyBudget() {
		return this.rateHourlyBudget;
	}

	/**
	 * Setter for the rateHourlyBudget property.
	 *
	 * @param rateHourlyBudget
	 *            the rateHourlyBudget to set.
	 */
	public void setRateHourlyBudget(final double rateHourlyBudget) {
		this.rateHourlyBudget = rateHourlyBudget;
	}

	/**
	 * Getter for the rateHourlyTotalPayment property.
	 *
	 * @return the rateHourlyTotalPayment property.
	 */
	public double getRateHourlyTotalPayment() {
		return this.rateHourlyTotalPayment;
	}

	/**
	 * Setter for the rateHourlyTotalPayment property.
	 *
	 * @param rateHourlyTotalPayment
	 *            the rateHourlyTotalPayment to set.
	 */
	public void setRateHourlyTotalPayment(final double rateHourlyTotalPayment) {
		this.rateHourlyTotalPayment = rateHourlyTotalPayment;
	}

	/**
	 * Getter for the rateHourlyVatBudget property.
	 *
	 * @return the rateHourlyVatBudget property.
	 */
	public double getRateHourlyVatBudget() {
		return this.rateHourlyVatBudget;
	}

	/**
	 * Setter for the rateHourlyVatBudget property.
	 *
	 * @param rateHourlyVatBudget
	 *            the rateHourlyVatBudget to set.
	 */
	public void setRateHourlyVatBudget(final double rateHourlyVatBudget) {
		this.rateHourlyVatBudget = rateHourlyVatBudget;
	}

	/**
	 * Getter for the rateHourlyVatPayment property.
	 *
	 * @return the rateHourlyVatPayment property.
	 */
	public double getRateHourlyVatPayment() {
		return this.rateHourlyVatPayment;
	}

	/**
	 * Setter for the rateHourlyVatPayment property.
	 *
	 * @param rateHourlyVatPayment
	 *            the rateHourlyVatPayment to set.
	 */
	public void setRateHourlyVatPayment(final double rateHourlyVatPayment) {
		this.rateHourlyVatPayment = rateHourlyVatPayment;
	}

	/**
	 * Getter for the rateOverBaseBudget property.
	 *
	 * @return the rateOverBaseBudget property.
	 */
	public double getRateOverBaseBudget() {
		return this.rateOverBaseBudget;
	}

	/**
	 * Setter for the rateOverBaseBudget property.
	 *
	 * @param rateOverBaseBudget
	 *            the rateOverBaseBudget to set.
	 */
	public void setRateOverBaseBudget(final double rateOverBaseBudget) {
		this.rateOverBaseBudget = rateOverBaseBudget;
	}

	/**
	 * Getter for the rateOverBasePayment property.
	 *
	 * @return the rateOverBasePayment property.
	 */
	public double getRateOverBasePayment() {
		return this.rateOverBasePayment;
	}

	/**
	 * Setter for the rateOverBasePayment property.
	 *
	 * @param rateOverBasePayment
	 *            the rateOverBasePayment to set.
	 */
	public void setRateOverBasePayment(final double rateOverBasePayment) {
		this.rateOverBasePayment = rateOverBasePayment;
	}

	/**
	 * Getter for the rateOverBudget property.
	 *
	 * @return the rateOverBudget property.
	 */
	public double getRateOverBudget() {
		return this.rateOverBudget;
	}

	/**
	 * Setter for the rateOverBudget property.
	 *
	 * @param rateOverBudget
	 *            the rateOverBudget to set.
	 */
	public void setRateOverBudget(final double rateOverBudget) {
		this.rateOverBudget = rateOverBudget;
	}

	/**
	 * Getter for the rateOverTotalPayment property.
	 *
	 * @return the rateOverTotalPayment property.
	 */
	public double getRateOverTotalPayment() {
		return this.rateOverTotalPayment;
	}

	/**
	 * Setter for the rateOverTotalPayment property.
	 *
	 * @param rateOverTotalPayment
	 *            the rateOverTotalPayment to set.
	 */
	public void setRateOverTotalPayment(final double rateOverTotalPayment) {
		this.rateOverTotalPayment = rateOverTotalPayment;
	}

	/**
	 * Getter for the rateOverVatBudget property.
	 *
	 * @return the rateOverVatBudget property.
	 */
	public double getRateOverVatBudget() {
		return this.rateOverVatBudget;
	}

	/**
	 * Setter for the rateOverVatBudget property.
	 *
	 * @param rateOverVatBudget
	 *            the rateOverVatBudget to set.
	 */
	public void setRateOverVatBudget(final double rateOverVatBudget) {
		this.rateOverVatBudget = rateOverVatBudget;
	}

	/**
	 * Getter for the rateOverVatPayment property.
	 *
	 * @return the rateOverVatPayment property.
	 */
	public double getRateOverVatPayment() {
		return this.rateOverVatPayment;
	}

	/**
	 * Setter for the rateOverVatPayment property.
	 *
	 * @param rateOverVatPayment
	 *            the rateOverVatPayment to set.
	 */
	public void setRateOverVatPayment(final double rateOverVatPayment) {
		this.rateOverVatPayment = rateOverVatPayment;
	}

	/**
	 * Getter for the regProgram property.
	 *
	 * @return the regProgram property.
	 */
	public String getRegProgram() {
		return this.regProgram;
	}

	/**
	 * Setter for the regProgram property.
	 *
	 * @param regProgram
	 *            the regProgram to set.
	 */
	public void setRegProgram(final String regProgram) {
		this.regProgram = regProgram;
	}

	/**
	 * Getter for the regRequirement property.
	 *
	 * @return the regRequirement property.
	 */
	public String getRegRequirement() {
		return this.regRequirement;
	}

	/**
	 * Setter for the regRequirement property.
	 *
	 * @param regRequirement
	 *            the regRequirement to set.
	 */
	public void setRegRequirement(final String regRequirement) {
		this.regRequirement = regRequirement;
	}

	/**
	 * Getter for the regulation property.
	 *
	 * @return the regulation property.
	 */
	public String getRegulation() {
		return this.regulation;
	}

	/**
	 * Setter for the regulation property.
	 *
	 * @param regulation
	 *            the regulation to set.
	 */
	public void setRegulation(final String regulation) {
		this.regulation = regulation;
	}

	/**
	 * Getter for the servWindowDays property.
	 *
	 * @return the servWindowDays property.
	 */
	public String getServWindowDays() {
		return this.servWindowDays;
	}

	/**
	 * Setter for the servWindowDays property.
	 *
	 * @param servWindowDays
	 *            the servWindowDays to set.
	 */
	public void setServWindowDays(final String servWindowDays) {
		this.servWindowDays = servWindowDays;
	}

	/**
	 * Getter for the servWindowMonths property.
	 *
	 * @return the servWindowMonths property.
	 */
	public String getServWindowMonths() {
		return this.servWindowMonths;
	}

	/**
	 * Setter for the servWindowMonths property.
	 *
	 * @param servWindowMonths
	 *            the servWindowMonths to set.
	 */
	public void setServWindowMonths(final String servWindowMonths) {
		this.servWindowMonths = servWindowMonths;
	}

	/**
	 * Getter for the trId property.
	 *
	 * @return the trId property.
	 */
	public String getTrId() {
		return this.trId;
	}

	/**
	 * Setter for the trId property.
	 *
	 * @param trId
	 *            the trId to set.
	 */
	public void setTrId(final String trId) {
		this.trId = trId;
	}

	/**
	 * Getter for the unitCount property.
	 *
	 * @return the unitCount property.
	 */
	public int getUnitCount() {
		return this.unitCount;
	}

	/**
	 * Setter for the unitCount property.
	 *
	 * @param unitCount
	 *            the unitCount to set.
	 */
	public void setUnitCount(final int unitCount) {
		this.unitCount = unitCount;
	}

	/**
	 * Getter for the vatAmountOverride property.
	 *
	 * @return the vatAmountOverride property.
	 */
	public double getVatAmountOverride() {
		return this.vatAmountOverride;
	}

	/**
	 * Setter for the vatAmountOverride property.
	 *
	 * @param vatAmountOverride
	 *            the vatAmountOverride to set.
	 */
	public void setVatAmountOverride(final double vatAmountOverride) {
		this.vatAmountOverride = vatAmountOverride;
	}

	/**
	 * Getter for the vatPercentOverride property.
	 *
	 * @return the vatPercentOverride property.
	 */
	public double getVatPercentOverride() {
		return this.vatPercentOverride;
	}

	/**
	 * Setter for the vatPercentOverride property.
	 *
	 * @param vatPercentOverride
	 *            the vatPercentOverride to set.
	 */
	public void setVatPercentOverride(final double vatPercentOverride) {
		this.vatPercentOverride = vatPercentOverride;
	}

	/**
	 * Getter for the vatPercentValue property.
	 *
	 * @return the vatPercentValue property.
	 */
	public double getVatPercentValue() {
		return this.vatPercentValue;
	}

	/**
	 * Setter for the vatPercentValue property.
	 *
	 * @param vatPercentValue
	 *            the vatPercentValue to set.
	 */
	public void setVatPercentValue(final double vatPercentValue) {
		this.vatPercentValue = vatPercentValue;
	}

	/**
	 * Getter for the workType property.
	 *
	 * @return the workType property.
	 */
	public int getWorkType() {
		return this.workType;
	}

	/**
	 * Setter for the workType property.
	 *
	 * @param workType
	 *            the workType to set.
	 */
	public void setWorkType(final int workType) {
		this.workType = workType;
	}

	/**
	 * Getter for the id property.
	 *
	 * @return the id property.
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * Setter for the id property.
	 *
	 * @param id.
	 */
	public void setId(final int id) {
		this.id = id;
	}

}
