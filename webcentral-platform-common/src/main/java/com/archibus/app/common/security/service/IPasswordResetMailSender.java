package com.archibus.app.common.security.service;

/**
 * Provides prepareAndSendEmail method to PasswordResetService.
 * <p>
 *
 * @author Valery Tydykov
 * @since 23.2
 *
 */
public interface IPasswordResetMailSender {

    /**
     * Prepares and sends to userId email with password reset instructions.
     *
     * @param userId ID of user to whom the email should be sent.
     * @param projectId to be used as context.
     */
    void prepareAndSendEmail(String userId, String projectId);
}