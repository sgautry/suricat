package com.archibus.app.common.questionnaire.impl;

import java.util.List;

import org.json.JSONArray;

import com.archibus.context.ContextStore;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.jobmanager.*;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.ParsedRestrictionDef;
import com.archibus.utility.StringUtil;

/**
 * Questionnaire Ext Service.
 *
 * Workflow rule to process Questionnaire Answers which have Follow-Up Actions
 * associated with them. An activity_log record will be generated and if an
 * activitytype.action_wfr has been defined it is called.
 *
 * @author Kaori Emery
 */

public class QuestionnaireExtService extends JobBase {
	/**
	 * Constant: '100'.
	 */
	public static final int NO_100 = 100;

	/**
	 * Constant: '-'.
	 */
	public static final String DASH = "-";

	/**
	 * Constant: '.'.
	 */
	public static final String DOT = ".";

	/**
	 * Hyphen: ' - '.
	 */
	public static final String HYPHEN = " - ";

	/**
	 * Constant: table name 'quest_answer_ext'.
	 */
	public static final String QUEST_ANSWER_EXT = "quest_answer_ext";

	/**
	 * Constant: table name 'questionnaire_ext'.
	 */
	public static final String QUESTIONNAIRE_EXT = "questionnaire_ext";

	/**
	 * Constant: table name 'question_ext'.
	 */
	public static final String QUESTION_EXT = "question_ext";

	/**
	 * Constant: table name 'activitytype'.
	 */
	public static final String ACTIVITYTYPE = "activitytype";

	/**
	 * Constant: table name 'activity_log'.
	 */
	public static final String ACTIVITY_LOG = "activity_log";

	/**
	 * Constant: field name 'questionnaire_id'.
	 */
	public static final String QUESTIONNAIRE_ID = "questionnaire_id";

	/**
	 * Constant: field name 'questionnaire_title'.
	 */
	public static final String QUESTIONNAIRE_TITLE = "questionnaire_title";

	/**
	 * Constant: field name 'answer_id'.
	 */
	public static final String ANSWER_ID = "answer_id";

	/**
	 * Constant: field name 'survey_event_id'.
	 */
	public static final String SURVEY_EVENT_ID = "survey_event_id";

	/**
	 * Constant: field name 'activity_log_id'.
	 */
	public static final String ACTIVITY_LOG_ID = "activity_log_id";

	/**
	 * Constant: field name 'question_id'.
	 */
	public static final String QUESTION_ID = "question_id";

	/**
	 * Constant: field name 'question_type'.
	 */
	public static final String QUESTION_TYPE = "question_type";

	/**
	 * Constant: field name 'question_text'.
	 */
	public static final String QUESTION_TEXT = "question_text";

	/**
	 * Constant: question_type value 'action'.
	 */
	public static final String ACTION = "action";

	/**
	 * Constant: field name 'activity_type'.
	 */
	public static final String ACTIVITY_TYPE = "activity_type";

	/**
	 * Constant: field name 'action_wfr'.
	 */
	public static final String ACTION_WFR = "action_wfr";

	/**
	 * Constant: field name 'action_title'.
	 */
	public static final String ACTION_TITLE = "action_title";

	/**
	 * Constant: field name 'bl_id'.
	 */
	public static final String BL_ID = "bl_id";

	/**
	 * Constant: field name 'fl_id'.
	 */
	public static final String FL_ID = "fl_id";

	/**
	 * Constant: field name 'rm_id'.
	 */
	public static final String RM_ID = "rm_id";

	/**
	 * Constant: field name 'location'.
	 */
	public static final String LOCATION = "location";

	/**
	 * Constant: field name 'location_id'.
	 */
	public static final String LOCATION_ID = "location_id";

	/**
	 * Constant: field name 'copied_from'.
	 */
	public static final String COPIED_FROM = "copied_from";

	/**
	 * Constant: field name 'requestor'.
	 */
	public static final String REQUESTOR = "requestor";

	/**
	 * Constant: field name 'manager'.
	 */
	public static final String MANAGER = "manager";

	/**
	 * Constant: field name 'status'.
	 */
	public static final String STATUS = "status";

	/**
	 * Constant: activity_log.status value 'CREATED'.
	 */
	public static final String CREATED = "CREATED";

	/**
	 * Constant: field name 'regulation'.
	 */
	public static final String REGULATION = "regulation";

	/**
	 * Constant: field name 'reg_program'.
	 */
	public static final String REG_PROGRAM = "reg_program";

	/**
	 * Constant: field name 'reg_requirement'.
	 */
	public static final String REG_REQUIREMENT = "reg_requirement";

	/**
	 * Constant: field name 'description'.
	 */
	public static final String DESCRIPTION = "description";

	/**
	 * Action Title for Follow-Up Action.
	 */
	// @translatable
	public static final String FOLLOW_UP_ACTION_TITLE = "Follow-Up Action from Questionnaire Survey";

	/**
	 * DataSource for quest_answer_ext.
	 *
	 */
	private static final DataSource QUEST_ANSWER_DS = DataSourceFactory.createDataSourceForFields(QUEST_ANSWER_EXT,
			new String[] { ANSWER_ID, SURVEY_EVENT_ID, ACTIVITY_LOG_ID });

	/**
	 * processQuestionnaireAnswers - process Questionnaire answers and create
	 * follow-up actions.
	 *
	 * First, create an action item for each Follow-Up Action answer, using the
	 * question_ext.activity_type and passing relevant fields from the Survey
	 * Event. Save the new activity_log_id to the
	 * quest_answer_ext.activity_log_id field.
	 *
	 * Then, call any workflow rules associated with the new action's activity
	 * type.
	 *
	 * @param questionnaireId
	 *            - questionnaire_id
	 *
	 * @param surveyEventId
	 *            - activity_log_id for Questionnaire Event
	 */
	public void processQuestionnaireAnswers(final int questionnaireId, final int surveyEventId) {
		this.status.setTotalNumber(NO_100);
		this.status.setCode(JobStatus.JOB_STARTED);
		this.status.setCurrentNumber(0);

		final DataRecord surveyEvent = getSurveyEvent(surveyEventId);
		final DataSource actionAnswerDs = DataSourceFactory
				.loadDataSourceFromFile("ab-common-quest-ext-data-sources.axvw", "processAnswersDataSource");

		final String restriction = QUEST_ANSWER_EXT + DOT + SURVEY_EVENT_ID + " = " + surveyEventId + " AND "
				+ QUEST_ANSWER_EXT + DOT + QUESTION_TYPE + " = 'action'";
		final List<DataRecord> actionAnswerRecords = actionAnswerDs.getRecords(restriction);

		int index = 0;
		for (final DataRecord actionAnswerRecord : actionAnswerRecords) {
			final int activityLogId = insertNewActionItem(actionAnswerRecord, surveyEvent);
			updateActionId(actionAnswerRecord.getInt(QUEST_ANSWER_EXT + DOT + ANSWER_ID), activityLogId);

			final String actionWfr = actionAnswerRecord.getString(ACTIVITYTYPE + DOT + ACTION_WFR);
			final int answerId = actionAnswerRecord.getInt(QUEST_ANSWER_EXT + DOT + ANSWER_ID);
			final int questionId = actionAnswerRecord.getInt(QUEST_ANSWER_EXT + DOT + QUESTION_ID);
			if (actionWfr != null) {
				runQuestionWorkflowRule(actionWfr, surveyEventId, questionnaireId, questionId, answerId);
			}
			index++;
			this.status.setCurrentNumber(index / actionAnswerRecords.size());
		}

		if (this.status.getCode() != JobStatus.JOB_STOPPED) {
			this.status.setCurrentNumber(NO_100);
			this.status.setCode(JobStatus.JOB_COMPLETE);
		}
	}

	/**
	 * Insert a new activity_log record for the follow-up action.
	 *
	 * @param actionAnswerRecord
	 *            quest_answer_ext record.
	 * @param surveyEvent
	 *            Questionnaire Survey activity_log record.
	 * @return activity_log_id of new action item
	 *
	 */
	private int insertNewActionItem(final DataRecord actionAnswerRecord, final DataRecord surveyEvent) {
		final DataSource datasource = DataSourceFactory.createDataSourceForFields(ACTIVITY_LOG,
				new String[] { ACTIVITY_LOG_ID, ACTION_TITLE, ACTIVITY_TYPE, BL_ID, FL_ID, RM_ID, LOCATION, LOCATION_ID,
						REGULATION, REG_PROGRAM, REG_REQUIREMENT, DESCRIPTION, COPIED_FROM, REQUESTOR, STATUS });
		final DataRecord newAction = datasource.createNewRecord();
		newAction.setValue(ACTIVITY_LOG + DOT + ACTION_TITLE, FOLLOW_UP_ACTION_TITLE);
		newAction.setValue(ACTIVITY_LOG + DOT + ACTIVITY_TYPE,
				actionAnswerRecord.getString(ACTIVITYTYPE + DOT + ACTIVITY_TYPE));
		newAction.setValue(ACTIVITY_LOG + DOT + BL_ID, surveyEvent.getString(ACTIVITY_LOG + DOT + BL_ID));
		newAction.setValue(ACTIVITY_LOG + DOT + FL_ID, surveyEvent.getString(ACTIVITY_LOG + DOT + FL_ID));
		newAction.setValue(ACTIVITY_LOG + DOT + RM_ID, surveyEvent.getString(ACTIVITY_LOG + DOT + RM_ID));
		newAction.setValue(ACTIVITY_LOG + DOT + LOCATION, surveyEvent.getString(ACTIVITY_LOG + DOT + LOCATION));

		final int locationId = surveyEvent.getInt(ACTIVITY_LOG + DOT + LOCATION_ID);
		if (locationId != 0) {
			newAction.setValue(ACTIVITY_LOG + DOT + LOCATION_ID, locationId);
		}
		newAction.setValue(ACTIVITY_LOG + DOT + COPIED_FROM, surveyEvent.getInt(ACTIVITY_LOG + DOT + ACTIVITY_LOG_ID));
		newAction.setValue(ACTIVITY_LOG + DOT + REQUESTOR, surveyEvent.getString(ACTIVITY_LOG + DOT + MANAGER));
		newAction.setValue(ACTIVITY_LOG + DOT + STATUS, CREATED);
		newAction.setValue(ACTIVITY_LOG + DOT + REGULATION, surveyEvent.getString(ACTIVITY_LOG + DOT + REGULATION));
		newAction.setValue(ACTIVITY_LOG + DOT + REG_PROGRAM, surveyEvent.getString(ACTIVITY_LOG + DOT + REG_PROGRAM));
		newAction.setValue(ACTIVITY_LOG + DOT + REG_REQUIREMENT,
				surveyEvent.getString(ACTIVITY_LOG + DOT + REG_REQUIREMENT));
		newAction.setValue(ACTIVITY_LOG + DOT + DESCRIPTION,
				FOLLOW_UP_ACTION_TITLE + HYPHEN
						+ actionAnswerRecord.getString(QUESTIONNAIRE_EXT + DOT + QUESTIONNAIRE_TITLE) + HYPHEN
						+ actionAnswerRecord.getString(QUESTION_EXT + DOT + QUESTION_TEXT));
		datasource.saveRecord(newAction);
		return retrieveActivityLogId(actionAnswerRecord);
	}

	/**
	 * updateActionId - Save new activity_log_id to quest_answer_ext record.
	 *
	 * @param answerId
	 *            - answer_id for questionnaire answer to update.
	 * @param activityLogId
	 *            - follow-up action item id
	 */
	private void updateActionId(final int answerId, final int activityLogId) {
		final ParsedRestrictionDef restriction = new ParsedRestrictionDef();
		restriction.addClause(QUEST_ANSWER_EXT, ANSWER_ID, answerId, Operation.EQUALS);
		final List<DataRecord> answers = QUEST_ANSWER_DS.getRecords(restriction);

		final DataRecord answer = answers.get(0);
		answer.setValue(QUEST_ANSWER_EXT + DOT + ACTIVITY_LOG_ID, activityLogId);
		QUEST_ANSWER_DS.saveRecord(answer);
	}

	/**
	 * runQuestionWorkflowRule.
	 *
	 * @param actionWfr
	 *            - rule id and method name
	 * @param surveyEventId
	 *            - int Questionnaire Survey activity_log record id
	 * @param questionnaireId
	 *            - int Questionnaire Id
	 * @param questionId
	 *            - int Question Id
	 * @param answerId
	 *            - int quest_answer_ext record id
	 */
	private void runQuestionWorkflowRule(final String actionWfr, final int surveyEventId, final int questionnaireId,
			final int questionId, final int answerId) {

		final List<String> ruleParts = StringUtil.tokenizeString(actionWfr, DASH);
		final String ruleId = ruleParts.get(0) + DASH + ruleParts.get(1);
		final String methodName = ruleParts.get(2);

		final EventHandlerContext context = ContextStore.get().getEventHandlerContext();
		final JSONArray parameters = new JSONArray();
		parameters.put(surveyEventId);
		parameters.put(questionnaireId);
		parameters.put(questionId);
		parameters.put(answerId);
		context.addResponseParameter("methodParameters", parameters);

		final WorkflowRulesContainer.ThreadSafe workflowRulesContainer = ContextStore.get().getUserSession()
				.findProject().loadWorkflowRules();
		workflowRulesContainer.runRule(workflowRulesContainer.getWorkflowRule(ruleId), methodName, context);
	}

	/**
	 * getSurveyEvent.
	 *
	 * @param surveyEventId
	 *            Questionnaire Survey activity_log_id
	 * @return surveyEvent DataRecord
	 */
	private DataRecord getSurveyEvent(final int surveyEventId) {
		final DataSource datasource = DataSourceFactory.createDataSourceForFields(ACTIVITY_LOG,
				new String[] { ACTIVITY_LOG_ID, ACTION_TITLE, ACTIVITY_TYPE, BL_ID, FL_ID, RM_ID, LOCATION, LOCATION_ID,
						MANAGER, REGULATION, REG_PROGRAM, REG_REQUIREMENT, DESCRIPTION });
		final ParsedRestrictionDef restriction = new ParsedRestrictionDef();
		restriction.addClause(ACTIVITY_LOG, ACTIVITY_LOG_ID, surveyEventId, Operation.EQUALS);
		final List<DataRecord> surveyEvents = datasource.getRecords(restriction);
		return surveyEvents.get(0);
	}

	/**
	 * retrieveActivityLogId.
	 *
	 * @param actionAnswerRecord
	 *            DataRecord
	 * @return activity_log_id
	 */
	private int retrieveActivityLogId(final DataRecord actionAnswerRecord) {
		final DataSource datasource = DataSourceFactory.createDataSourceForFields(ACTIVITY_LOG,
				new String[] { ACTIVITY_LOG_ID, ACTION_TITLE, ACTIVITY_TYPE, BL_ID, FL_ID, RM_ID, REGULATION,
						REG_PROGRAM, REG_REQUIREMENT, DESCRIPTION });
		final ParsedRestrictionDef restriction = new ParsedRestrictionDef();
		restriction.addClause(ACTIVITY_LOG, DESCRIPTION,
				FOLLOW_UP_ACTION_TITLE + HYPHEN
						+ actionAnswerRecord.getString(QUESTIONNAIRE_EXT + DOT + QUESTIONNAIRE_TITLE) + HYPHEN
						+ actionAnswerRecord.getString(QUESTION_EXT + DOT + QUESTION_TEXT),
				Operation.EQUALS);
		restriction.addClause(ACTIVITY_LOG, ACTIVITY_TYPE,
				actionAnswerRecord.getString(ACTIVITYTYPE + DOT + ACTIVITY_TYPE), Operation.EQUALS);
		datasource.addSort(ACTIVITY_LOG, ACTIVITY_LOG_ID, DataSource.SORT_DESC);
		final DataRecord maxRecord = datasource.getRecords(restriction).get(0);
		return maxRecord.getInt(ACTIVITY_LOG + DOT + ACTIVITY_LOG_ID);
	}
}
