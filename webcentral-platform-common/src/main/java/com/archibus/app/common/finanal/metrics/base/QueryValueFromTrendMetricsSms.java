package com.archibus.app.common.finanal.metrics.base;

import java.util.*;

import com.archibus.app.common.finanal.domain.*;
import com.archibus.app.common.finanal.impl.*;
import com.archibus.app.common.finanal.metrics.*;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;
import com.archibus.utility.StringUtil;

/**
 * Generic metric provider used to query values from trend metric. Used for SMS
 * builder.
 * <p>
 *
 *
 * @author Ioan Draghici
 * @since 23.1
 *
 */
public class QueryValueFromTrendMetricsSms implements MetricProvider {

	/**
	 * Constant.
	 */
	private static final int ONE_HUNDRED = 100;

	/**
	 * List with asset types.
	 */
	private List<String> assetTypes;

	/**
	 * Financial metric object.
	 */
	private FinancialMetric metric;

	/**
	 * Trend metric name.
	 */
	private String trendMetricName;

	/**
	 * Error message.
	 */
	private String errorMessage;

	/** {@inheritDoc} */

	@Override
	public Map<Date, Double> getValues(final FinancialAnalysisParameter financialParameter, final Date dateFrom,
			final Date dateTo) {
		// used when is required to return metric values
		return null;
	}

	/** {@inheritDoc} */

	@Override
	public void calculateValues(final FinancialAnalysisParameter financialParameter, final Date dateFrom,
			final Date dateTo) {
		this.errorMessage = "";
		final AssetType assetType = financialParameter.getAssetType();
		final String assetId = financialParameter.getAssetId();

		final DataSource trendValueDataSource = DataSourceFactory
				.createDataSourceForTable(Constants.AFM_METRIC_TREND_VALUES);
		trendValueDataSource.addRestriction(
				Restrictions.eq(Constants.AFM_METRIC_TREND_VALUES, DbConstants.METRIC_NAME, this.trendMetricName));
		trendValueDataSource.addRestriction(Restrictions.and(
				Restrictions.eq(Constants.AFM_METRIC_TREND_VALUES, "collect_group_by", assetType.getAssetFieldName()),
				Restrictions.eq(Constants.AFM_METRIC_TREND_VALUES, "collect_by_value", assetId)));
		trendValueDataSource.addRestriction(
				Restrictions.or(Restrictions.isNull(Constants.AFM_METRIC_TREND_VALUES, DbConstants.COLLECT_ERR_MSG),
						Restrictions.ne(Constants.AFM_METRIC_TREND_VALUES, DbConstants.COLLECT_ERR_MSG,
								DbConstants.VALUE_EXAMPLE)));

		final Date fiscalYearStart = DateUtils.getCurrentFiscalYearStartDate();
		final Date fiscalYearEnd = DateUtils.getCurrentFiscalYearEndDate();
		trendValueDataSource.addRestriction(Restrictions.and(
				Restrictions.gte(Constants.AFM_METRIC_TREND_VALUES, Constants.METRIC_DATE, fiscalYearStart),
				Restrictions.lte(Constants.AFM_METRIC_TREND_VALUES, Constants.METRIC_DATE, fiscalYearEnd)));


		trendValueDataSource.addSort(Constants.AFM_METRIC_TREND_VALUES, Constants.METRIC_DATE, DataSource.SORT_DESC);

		final DataRecord record = trendValueDataSource.getRecord();
		double metricValue = 0.0;
		if (record != null) {
			metricValue = getMetricValue(this.metric, record);
		}
		MetricProviderUtils.saveToFinancialSummary(financialParameter, dateFrom, this.metric.getResultField(),
				metricValue);
	}

	/** {@inheritDoc} */

	@Override
	public void setMetric(final FinancialMetric metric) {
		this.metric = metric;
	}

	/** {@inheritDoc} */

	@Override
	public boolean isApplicableForAssetType(final AssetType assetType) {
		return this.assetTypes.contains(assetType.toString());
	}

	/** {@inheritDoc} */

	@Override
	public boolean isApplicableForAllAssetTypes() {
		return StringUtil.isNullOrEmpty(this.assetTypes);
	}

	/** {@inheritDoc} */

	@Override
	public String getAssetTypeRestriction() {
		return MetricProviderUtils.getAssetTypeRestrictionForTable(Constants.FINANAL_PARAMS, this.assetTypes);
	}

	/**
	 * Getter for the assetTypes property.
	 *
	 * @see assetTypes
	 * @return the assetTypes property.
	 */
	public List<String> getAssetTypes() {
		return this.assetTypes;
	}

	/**
	 * Setter for the assetTypes property.
	 *
	 * @see assetTypes
	 * @param assetTypes
	 *            the assetTypes to set
	 */

	public void setAssetTypes(final List<String> assetTypes) {
		this.assetTypes = assetTypes;
	}

	/**
	 * Getter for the trendMetricName property.
	 *
	 * @see trendMetricName
	 * @return the trendMetricName property.
	 */
	public String getTrendMetricName() {
		return this.trendMetricName;
	}

	/**
	 * Setter for the trendMetricName property.
	 *
	 * @see trendMetricName
	 * @param trendMetricName
	 *            the trendMetricName to set
	 */

	public void setTrendMetricName(final String trendMetricName) {
		this.trendMetricName = trendMetricName;
	}

	/** {@inheritDoc} */

	@Override
	public String getErrorMessage() {
		return this.errorMessage;
	}

	/**
	 * Get metric value.
	 *
	 * @param financialMetric
	 *            financial metric
	 * @param record
	 *            trend value record
	 * @return double
	 */
	private double getMetricValue(final FinancialMetric financialMetric, final DataRecord record) {
		double metricValue = record.getDouble(Constants.AFM_METRIC_TREND_VALUES + Constants.DOT + "metric_value");
		metricValue = MetricProviderUtils.round(metricValue, financialMetric.getMetricDecimals());
		return metricValue;
	}
}