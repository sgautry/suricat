/**
 *
 */
package com.archibus.app.common.security.providers.dao;

import java.util.Date;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import com.archibus.security.IPasswordPolicy;
import com.archibus.utility.DateTime;

/**
 * Provides password policies: account locking, credentials expiration.
 *
 * @author Valery Tydykov
 *
 * @see PasswordManagerImpl
 * @see UserAccountDao
 */
public class PasswordPolicy implements InitializingBean, IPasswordPolicy {
    /**
     * Property: Number of failed login attempts allowed. User account will be locked after the
     * number of failed login attempts exceeds this value. Value "-1" means "no user account
     * locking".
     */
    private int numberFailedLoginAttemptsAllowed = -1;

    /**
     * Property: Password expiration period, in days. Value "-1" means "never expire".
     */
    private int passwordExpirationPeriod = -1;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.isTrue(
            this.numberFailedLoginAttemptsAllowed > 0
                    || this.numberFailedLoginAttemptsAllowed == -1,
            "numberFailedLoginAttemptsAllowed must be positive or -1");

        Assert.isTrue(this.passwordExpirationPeriod > 0 || this.passwordExpirationPeriod == -1,
            "passwordExpirationPeriod must be positive or -1");
    }

    @Override
    public boolean isNumberFailedLoginAttemptsMoreThanAllowed(final int numberFailedLoginAttempts) {
        boolean more = false;
        // ignore NumberFailedLoginAttemptsAllowed if value is -1
        if (this.numberFailedLoginAttemptsAllowed != -1) {
            // if Number of Failed Login Attempts is more than Allowed or allowed == -1
            if (numberFailedLoginAttempts > this.numberFailedLoginAttemptsAllowed) {
                // lock user account
                more = true;
            }
        }

        return more;
    }

    @Override
    public boolean isCredentialsExpired(final boolean passwordNeverExpires,
            final Date datePasswordChanged) {
        boolean expired = false;
        if (!passwordNeverExpires) {
            // ignore PasswordExpirationPeriod if value is -1
            if (this.passwordExpirationPeriod != -1) {
                // calculate date when password expires
                if (datePasswordChanged != null) {
                    final java.util.Date datePasswordExpires =
                            DateTime.addDays(datePasswordChanged, this.passwordExpirationPeriod);
                    // if password already expired (expiration date is before now)
                    if (datePasswordExpires.before(new java.util.Date())) {
                        expired = true;
                    }
                }
            }
        }

        return expired;
    }

    /**
     * Getter for the numberFailedLoginAttemptsAllowed property.
     *
     * @return the numberFailedLoginAttemptsAllowed property.
     */
    public int getNumberFailedLoginAttemptsAllowed() {
        return this.numberFailedLoginAttemptsAllowed;
    }

    /**
     * Setter for the numberFailedLoginAttemptsAllowed property.
     *
     * @param numberFailedLoginAttemptsAllowed the numberFailedLoginAttemptsAllowed to set.
     */
    public void setNumberFailedLoginAttemptsAllowed(final int numberFailedLoginAttemptsAllowed) {
        this.numberFailedLoginAttemptsAllowed = numberFailedLoginAttemptsAllowed;
    }

    @Override
    public int getPasswordExpirationPeriod() {
        return this.passwordExpirationPeriod;
    }

    /**
     * Setter for the passwordExpirationPeriod property.
     *
     * @param passwordExpirationPeriod the passwordExpirationPeriod to set.
     */
    public void setPasswordExpirationPeriod(final int passwordExpirationPeriod) {
        this.passwordExpirationPeriod = passwordExpirationPeriod;
    }
}
