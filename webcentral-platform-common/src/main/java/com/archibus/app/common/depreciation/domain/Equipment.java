package com.archibus.app.common.depreciation.domain;

import java.util.Date;

/**
 * Equipment domain object. Contains only fields that are required for depreciation calculation.
 * <p>
 *
 * Used by Depreciation service to calculate and update depreciation value. Managed by Spring.
 * Configured in [schema\ab-products\common\resources\appContext-services.xml] file.
 *
 * @author Ioan Draghici
 * @author Radu Bunea
 * @since 23.1
 *
 */
public class Equipment {
    /**
     * Equipment code.
     */
    private String eqId;

    /**
     * Equipment Standard.
     */
    private String eqStd;

    /**
     * Equipment building Id.
     */
    private String buildingId;

    /**
     * Equipment floor Id.
     */
    private String floorId;

    /**
     * Equipment room Id.
     */
    private String roomId;

    /**
     * Equipment division Id.
     */
    private String divisionId;

    /**
     * Equipment department Id.
     */
    private String departmentId;

    /**
     * Property type.
     */
    private String propertyType;

    /**
     * Purchase cost.
     */
    private double costPurchase;

    /**
     * Salvage value.
     */
    private double valueSalvage;

    /**
     * Cost depreciated value.
     */
    private double costDepValue;

    /**
     * Date depreciated closing month.
     */
    private Date dateDepreciationClosingMonth;

    /**
     * Date installed.
     */
    private Date dateInstalled;

    /**
     * Getter for the eqId property.
     *
     * @see eqId
     * @return the eqId property.
     */
    public String getEqId() {
        return this.eqId;
    }

    /**
     * Setter for the eqId property.
     *
     * @see eqId
     * @param eqId the eqId to set
     */

    public void setEqId(final String eqId) {
        this.eqId = eqId;
    }

    /**
     * Getter for the propertyType property.
     *
     * @see propertyType
     * @return the propertyType property.
     */
    public String getPropertyType() {
        return this.propertyType;
    }

    /**
     * Setter for the propertyType property.
     *
     * @see propertyType
     * @param propertyType the propertyType to set
     */

    public void setPropertyType(final String propertyType) {
        this.propertyType = propertyType;
    }

    /**
     * Getter for the costDepValue property.
     *
     * @see costDepValue
     * @return the costDepValue property.
     */
    public double getCostDepValue() {
        return this.costDepValue;
    }

    /**
     * Setter for the costDepValue property.
     *
     * @see costDepValue
     * @param costDepValue the costDepValue to set
     */

    public void setCostDepValue(final double costDepValue) {
        this.costDepValue = costDepValue;
    }

    /**
     * Getter for the dateDepreciationClosingMonth property.
     *
     * @return the dateDepreciationClosingMonth property.
     */
    public Date getDateDepreciationClosingMonth() {
        return this.dateDepreciationClosingMonth;
    }

    /**
     * Setter for the dateDepreciationClosingMonth property.
     *
     * @param dateDepreciationClosingMonth the dateDepreciationClosingMonth to set.
     */
    public void setDateDepreciationClosingMonth(final Date dateDepreciationClosingMonth) {
        this.dateDepreciationClosingMonth = dateDepreciationClosingMonth;
    }

    /**
     * Getter for the costPurchase property.
     *
     * @see costPurchase
     * @return the costPurchase property.
     */
    public double getCostPurchase() {
        return this.costPurchase;
    }

    /**
     * Setter for the costPurchase property.
     *
     * @see costPurchase
     * @param costPurchase the costPurchase to set
     */

    public void setCostPurchase(final double costPurchase) {
        this.costPurchase = costPurchase;
    }

    /**
     * Getter for the valueSalvage property.
     *
     * @see valueSalvage
     * @return the valueSalvage property.
     */
    public double getValueSalvage() {
        return this.valueSalvage;
    }

    /**
     * Setter for the valueSalvage property.
     *
     * @see valueSalvage
     * @param valueSalvage the valueSalvage to set
     */

    public void setValueSalvage(final double valueSalvage) {
        this.valueSalvage = valueSalvage;
    }

    /**
     * Getter for the dateInstalled property.
     *
     * @see dateInstalled
     * @return the dateInstalled property.
     */
    public Date getDateInstalled() {
        return this.dateInstalled;
    }

    /**
     * Setter for the dateInstalled property.
     *
     * @see dateInstalled
     * @param dateInstalled the dateInstalled to set
     */

    public void setDateInstalled(final Date dateInstalled) {
        this.dateInstalled = dateInstalled;
    }

    /**
     * Getter for the eqStd property.
     *
     * @return the eqStd property.
     */
    public String getEqStd() {
        return this.eqStd;
    }

    /**
     * Setter for the eqStd property.
     *
     * @param eqStd the eqStd to set.
     */
    public void setEqStd(final String eqStd) {
        this.eqStd = eqStd;
    }

    /**
     * Getter for the buildingId property.
     *
     * @return the buildingId property.
     */
    public String getBuildingId() {
        return this.buildingId;
    }

    /**
     * Setter for the buildingId property.
     *
     * @param buildingId the buildingId to set.
     */
    public void setBuildingId(final String buildingId) {
        this.buildingId = buildingId;
    }

    /**
     * Getter for the floorId property.
     *
     * @return the floorId property.
     */
    public String getFloorId() {
        return this.floorId;
    }

    /**
     * Setter for the floorId property.
     *
     * @param floorId the floorId to set.
     */
    public void setFloorId(final String floorId) {
        this.floorId = floorId;
    }

    /**
     * Getter for the roomId property.
     *
     * @return the roomId property.
     */
    public String getRoomId() {
        return this.roomId;
    }

    /**
     * Setter for the roomId property.
     *
     * @param roomId the roomId to set.
     */
    public void setRoomId(final String roomId) {
        this.roomId = roomId;
    }

    /**
     * Getter for the divisionId property.
     *
     * @return the divisionId property.
     */
    public String getDivisionId() {
        return this.divisionId;
    }

    /**
     * Setter for the divisionId property.
     *
     * @param divisionId the divisionId to set.
     */
    public void setDivisionId(final String divisionId) {
        this.divisionId = divisionId;
    }

    /**
     * Getter for the departmentId property.
     *
     * @return the departmentId property.
     */
    public String getDepartmentId() {
        return this.departmentId;
    }

    /**
     * Setter for the departmentId property.
     *
     * @param departmentId the departmentId to set.
     */
    public void setDepartmentId(final String departmentId) {
        this.departmentId = departmentId;
    }
}
