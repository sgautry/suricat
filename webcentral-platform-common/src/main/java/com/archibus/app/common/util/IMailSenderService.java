package com.archibus.app.common.util;

import java.util.*;

/**
 * Provides methods for preparing localized message, preparing SMTP server parameters and sending
 * email.
 *
 * @author Valery Tydykov
 * @since 23.2
 *
 */
/**
 * Suppress Warning "PMD.ShortVariable"
 * <p>
 * Justification: constant names "to" and "cc" are common terms.
 */
@SuppressWarnings({ "PMD.ShortVariable" })
public interface IMailSenderService {

    /**
     * Localizes text message using className as key. Optionally adds translatable prefix.
     *
     * @param className to be used as key.
     * @param message Text message to be localized.
     * @param locale to be used for localization.
     * @return Localized text message.
     */
    String localizeMessage(String className, String message, Locale locale);

    /**
     * Localizes and formats text message.
     *
     * @param className to be used as key.
     * @param message Text message to localize and format.
     * @param args objects to be injected into formatted message.
     * @param locale to be used for localization.
     * @return Localized and formatted text message.
     */
    String prepareMessage(String className, String message, Object[] args, Locale locale);

    /**
     * Prepares email SMTP server parameters and sends email. Uses ConfigManager settings to prepare
     * values for SMTP host, port, userId, password.
     *
     * @param emailParameters parameters of email message.
     */
    void prepareAndSendEmail(EmailParameters emailParameters);

    /**
     * Represents parameters of email.
     *
     * @author Valery Tydykov
     * @since 23.2
     *
     */
    class EmailParameters {
        /**
         * Property: attachment list of email.
         */
        private List<String> attachmentFileNames;

        /**
         * Property: bcc list of email.
         */
        private List<String> bcc;

        /**
         * Property: body of email.
         */
        private String body;

        /**
         * Property: cc list of email.
         */
        private List<String> cc;

        /**
         * Property: from address of email.
         */
        private String from;

        /**
         * Property: subject of email.
         */
        private String subject;

        /**
         * Property: to address of email.
         */
        private String to;

        /**
         * Getter for the attachmentFileNames property.
         *
         * @return the attachmentFileNames property.
         */
        public List<String> getAttachmentFileNames() {
            return this.attachmentFileNames;
        }

        /**
         * Getter for the bcc property.
         *
         * @return the bcc property.
         */
        public List<String> getBcc() {
            return this.bcc;
        }

        /**
         * Getter for the body property.
         *
         * @return the body property.
         */
        public String getBody() {
            return this.body;
        }

        /**
         * Getter for the cc property.
         *
         * @return the cc property.
         */
        public List<String> getCc() {
            return this.cc;
        }

        /**
         * Getter for the from property.
         *
         * @return the from property.
         */
        public String getFrom() {
            return this.from;
        }

        /**
         * Getter for the subject property.
         *
         * @return the subject property.
         */
        public String getSubject() {
            return this.subject;
        }

        /**
         * Getter for the to property.
         *
         * @return the to property.
         */
        public String getTo() {
            return this.to;
        }

        /**
         * Setter for the attachmentFileNames property.
         *
         * @param attachmentFileNames the attachmentFileNames to set.
         */
        public void setAttachmentFileNames(final List<String> attachmentFileNames) {
            this.attachmentFileNames = attachmentFileNames;
        }

        /**
         * Setter for the bcc property.
         *
         * @param bcc the bcc to set.
         */
        public void setBcc(final List<String> bcc) {
            this.bcc = bcc;
        }

        /**
         * Setter for the body property.
         *
         * @param body the body to set.
         */
        public void setBody(final String body) {
            this.body = body;
        }

        /**
         * Setter for the cc property.
         *
         * @param cc the cc to set.
         */
        public void setCc(final List<String> cc) {
            this.cc = cc;
        }

        /**
         * Setter for the from property.
         *
         * @param from the from to set.
         */
        public void setFrom(final String from) {
            this.from = from;
        }

        /**
         * Setter for the subject property.
         *
         * @param subject the subject to set.
         */
        public void setSubject(final String subject) {
            this.subject = subject;
        }

        /**
         * Setter for the to property.
         *
         * @param to the to to set.
         */
        public void setTo(final String to) {
            this.to = to;
        }
    }
}