package com.archibus.app.common.mobile.sync.dao;

import java.util.List;

import com.archibus.app.common.mobile.sync.service.Record;
import com.archibus.utility.ExceptionBase;

/**
 * DAO for the DocumentVersion operations.
 *
 * @author jmartin
 * @since 23.1
 *
 */
public interface IDocumentVersionDao {
	/**
	 * Returns the latest document version value for a document record.
	 *
	 * @param tableName
	 *            The table containing the document field.
	 * @param fieldName
	 *            The name of the document field.
	 * @param pkeyValue
	 *            The primary key values as a pipe delimited string.
	 * @return a DataRecord containing the document version information.
	 * @throws ExceptionBase
	 *             if the DataSource throws an exception.
	 *
	 */
	Record getDocumentVersion(final String tableName, final String fieldName, final String pkeyValue)
			throws ExceptionBase;

	/**
	 *
	 * Returns the document versions for the requested documents.
	 *
	 * @param tableName
	 *            the name of the table containing the document records.
	 * @param records
	 *            List of document records to retrieve.
	 * @return List of data version records.
	 * @throws ExceptionBase
	 *             if the DataSource throws an exception.
	 */
	List<Record> getMaxDocumentVersionsForTable(final String tableName, final List<Record> records)
			throws ExceptionBase;
}
