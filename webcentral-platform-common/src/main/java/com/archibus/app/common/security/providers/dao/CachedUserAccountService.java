package com.archibus.app.common.security.providers.dao;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Interface to be implemented by DAOs that cache UserAccount.
 * <p>
 * Provides method that flushes cached UserAccount.
 *
 * @author Valery Tydykov
 * @since 23.2
 *
 */
public interface CachedUserAccountService {
    /**
     * Flushes cached UserAccount.
     *
     * @param username of UserAccount to be flushed.
     * @throws UsernameNotFoundException if the user could not be found or the user has no
     *             GrantedAuthority, or the case of the username must be enforced and does not match
     *             the case of the UserAccount property.
     * @throws DataAccessException if user could not be found for a repository-specific reason.
     */
    void flushUserAccount(String username) throws UsernameNotFoundException, DataAccessException;
}
