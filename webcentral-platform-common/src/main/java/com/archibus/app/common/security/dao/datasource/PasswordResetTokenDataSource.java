package com.archibus.app.common.security.dao.datasource;

import java.util.*;

import com.archibus.app.common.security.dao.IPasswordResetTokenDao;
import com.archibus.app.common.security.domain.PasswordResetToken;
import com.archibus.datasource.*;
import com.archibus.datasource.data.*;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.ParsedRestrictionDef;

/**
 * DataSource for PasswordResetToken.
 *
 * @author Valery Tydykov
 * @since 23.2
 */
public class PasswordResetTokenDataSource extends ObjectDataSourceImpl<PasswordResetToken>
        implements IPasswordResetTokenDao {

    /**
     * Constant: field name "expiration_date".
     */
    static final String EXPIRATION_DATE = "expiration_date";

    /**
     * Constant: field name "expiration_time".
     */
    static final String EXPIRATION_TIME = "expiration_time";

    /**
     * Constant: table name "afm_password_token".
     */
    static final String AFM_PASSWORD_TOKEN = "afm_password_token";

    /**
     * Constant: field name "auto_number".
     */
    private static final String AUTO_NUMBER = "auto_number";

    /**
     * Constant: field name "id".
     */
    /**
     * Suppress Warning "PMD.ShortVariable"
     * <p>
     * Justification: constant name "ID" is a common term.
     */
    @SuppressWarnings({ "PMD.ShortVariable" })
    private static final String ID = "id";

    /**
     * Constant: field name "token".
     */
    private static final String TOKEN = "token";

    /**
     * Field names to property names mapping. All fields will be added to the DataSource.
     */
    private static final String[][] FIELDS_TO_PROPERTIES =
            { { AUTO_NUMBER, ID }, { TOKEN, TOKEN }, { "user_name", "userName" } };

    /**
     * Constructs PasswordResetTokenDataSource, mapped to <code>afm_password_token</code> table,
     * using <code>passwordResetToken</code> bean.
     */
    public PasswordResetTokenDataSource() {
        super("passwordResetToken", AFM_PASSWORD_TOKEN);

        // add database fields that don't have matching properties
        this.addField(EXPIRATION_DATE);
        this.addField(EXPIRATION_TIME);
        this.setDatabaseRole(DB_ROLE_SECURITY);
    }

    @Override
    public PasswordResetToken convertRecordToObject(final DataRecord record) {
        return new PasswordResetTokenObjectConverter().convertRecordToObject(record, this.beanName,
            this.fieldToPropertyMapping, null);
    }

    /**
     * Deletes all expired tokens.
     */
    @Override
    public void deleteAllExpired() {
        final List<PasswordResetToken> tokens = this.find(new ParsedRestrictionDef());
        for (final PasswordResetToken token : tokens) {
            if (token.getExpirationTime().before(new Date(System.currentTimeMillis()))) {
                this.delete(token);
            }
        }
    }

    /**
     * Finds PasswordResetToken by specified token.
     *
     * @param token value of token to be used as filter.
     * @return PasswordResetToken if a single PasswordResetToken found, or null.
     */
    @Override
    public PasswordResetToken findByToken(final String token) {
        final ParsedRestrictionDef restrictionDef = new ParsedRestrictionDef();
        restrictionDef.addClause(AFM_PASSWORD_TOKEN, TOKEN, token, Operation.EQUALS);

        PasswordResetToken result = null;
        final List<PasswordResetToken> tokens = this.find(restrictionDef);
        if (tokens.size() == 1) {
            result = tokens.get(0);
        }

        return result;
    }

    /**
     * Returns the persistent object with the given PK values, or null if there is no such object.
     *
     * @param id primary key value.
     * @return persistent object with the given PK values, or null.
     */
    @Override
    public PasswordResetToken getByPrimaryKey(final int id) {
        final PrimaryKeysValues primaryKeysValues = new PrimaryKeysValues();
        {
            final DataRecordField pkField = new DataRecordField();
            pkField.setName(this.tableName + DOT + AUTO_NUMBER);
            pkField.setValue(id);
            primaryKeysValues.getFieldsValues().add(pkField);
        }

        return this.get(primaryKeysValues);
    }

    @Override
    protected void convertObjectToRecord(final PasswordResetToken bean, final DataRecord record,
            final boolean includeAutonumberedFields) {
        new PasswordResetTokenObjectConverter().convertObjectToRecord(bean, record,
            this.fieldToPropertyMapping, includeAutonumberedFields);
    }

    @Override
    protected String[][] getFieldsToProperties() {
        return FIELDS_TO_PROPERTIES.clone();
    }
}
