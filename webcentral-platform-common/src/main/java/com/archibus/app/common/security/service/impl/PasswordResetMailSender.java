package com.archibus.app.common.security.service.impl;

import java.text.MessageFormat;
import java.util.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.Assert;

import com.archibus.app.common.security.dao.IPasswordResetTokenDao;
import com.archibus.app.common.security.domain.PasswordResetToken;
import com.archibus.app.common.security.providers.dao.MessagesDao;
import com.archibus.app.common.security.service.IPasswordResetMailSender;
import com.archibus.app.common.util.IMailSenderService;
import com.archibus.app.common.util.IMailSenderService.EmailParameters;
import com.archibus.config.*;
import com.archibus.context.*;
import com.archibus.context.Context;
import com.archibus.context.utility.*;
import com.archibus.utility.*;

/**
 * Implementation of the PasswordResetService service.
 * <p>
 * Non-authenticated user [using sing-in page] is expected to invoke methods in this service.
 *
 * @author Valery Tydykov
 * @since 23.2
 *
 */
public class PasswordResetMailSender implements InitializingBean, IPasswordResetMailSender {
    /**
     * Constant: value of Activivy ID.
     */
    private static final String ACTIVITY_ID_AB_SYSTEM_ADMINISTRATION = "AbSystemAdministration";

    /**
     * Constant: value of message ID.
     */
    private static final String SEND_REQUEST_PASSWORD_RESET_BODY =
            "SEND_REQUEST_PASSWORD_RESET_BODY";

    /**
     * Constant: value of message ID.
     */
    private static final String SEND_REQUEST_PASSWORD_RESET_SUBJECT =
            "SEND_REQUEST_PASSWORD_RESET_SUBJECT";

    /**
     * Logger for this class and subclasses.
     */
    protected final Logger logger = Logger.getLogger(this.getClass());

    /**
     * Property: configManager.
     */
    private ConfigManager.Immutable configManager;

    /**
     * Property: mailSender.
     */
    private IMailSenderService mailSender;

    /**
     * Property: messagesDao. Used to load Email message templates.
     */
    private MessagesDao messagesDao;

    /**
     * Property: passwordResetTokenDao.
     */
    private IPasswordResetTokenDao passwordResetTokenDao;

    /**
     * Property: session.
     */
    private Session session;

    /**
     * {@inheritDoc}
     * <p>
     * Suppress Warning "PMD.SignatureDeclareThrowsException"
     * <p>
     * Justification: This method implements Spring interface.
     */
    @Override
    @SuppressWarnings({ "PMD.SignatureDeclareThrowsException" })
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(this.configManager, "configManager must be supplied");
        Assert.notNull(this.passwordResetTokenDao, "passwordResetTokenDao must be supplied");
        Assert.notNull(this.session, "session must be supplied");
        Assert.notNull(this.messagesDao, "messagesDao must be supplied");
        Assert.notNull(this.mailSender, "mailSender must be supplied");
    }

    /**
     * Getter for the configManager property.
     *
     * @return the configManager property.
     */
    public ConfigManager.Immutable getConfigManager() {
        return this.configManager;
    }

    /**
     * Getter for the mailSender property.
     *
     * @return the mailSender property.
     */
    public IMailSenderService getMailSender() {
        return this.mailSender;
    }

    /**
     * Getter for the messagesDao property.
     *
     * @return the messagesDao property.
     */
    public MessagesDao getMessagesDao() {
        return this.messagesDao;
    }

    /**
     * Getter for the passwordResetTokenDao property.
     *
     * @return the passwordResetTokenDao property.
     */
    public IPasswordResetTokenDao getPasswordResetTokenDao() {
        return this.passwordResetTokenDao;
    }

    /**
     * Getter for the session property.
     *
     * @return the session property.
     */
    public Session getSession() {
        return this.session;
    }

    /**
     * Prepares and sends email with password reset instructions to userId.
     *
     * @param userId ID of user to whom the email should be sent.
     * @param projectId to be used as context.
     */
    @Override
    public void prepareAndSendEmail(final String userId, final String projectId) {
        Assert.hasLength(userId, "userId must be supplied");
        Assert.hasLength(projectId, "projectId must be supplied");

        final Project.Immutable project = this.configManager.findProject(projectId);
        Assert.notNull(project, "projectId must match a project");

        final Context context = ContextStore.get();

        try {
            // use core user session as context
            context.setProject(project);
            final CoreUserSessionTemplate template = new CoreUserSessionTemplate();
            template.setDatabaseRole(DatabaseRole.SECURITY);
            template.execute(new Runnable() {
                @Override
                public void run() {
                    final String token = UUID.randomUUID().toString();
                    final EmailParameters emailParameters =
                            prepareEmailParameters(userId, project, token);

                    Assert.notNull(PasswordResetMailSender.this.session,
                        "session must be specified");

                    // set sessionID in context
                    context.getSession().setId(PasswordResetMailSender.this.session.getId());

                    saveToken(userId, token, project);
                    PasswordResetMailSender.this.mailSender.prepareAndSendEmail(emailParameters);
                }
            });
        } catch (final IllegalArgumentException e) {
            // log e here, since it is not passed as nested here
            this.logger.error(
                PasswordResetService.THROWING_EXCEPTION_FOR_NON_AUTHENTICATED_USER_CAUSE + e);

            throw prepareException();
        } catch (final ExceptionBase e) {
            // log e here, since it is not passed as nested here
            this.logger
                .error(PasswordResetService.THROWING_EXCEPTION_FOR_NON_AUTHENTICATED_USER_CAUSE
                        + e.toStringForLogging());

            throw prepareException();
        } catch (final UsernameNotFoundException e) {
            // log e here, since it is not passed as nested here
            this.logger
                .error(PasswordResetService.THROWING_EXCEPTION_FOR_NON_AUTHENTICATED_USER_CAUSE
                        + e.toString());

            throw prepareException();
        }
    }

    /**
     * Setter for the configManager property.
     *
     * @param configManager the configManager to set.
     */
    public void setConfigManager(final ConfigManager.Immutable configManager) {
        this.configManager = configManager;
    }

    /**
     * Setter for the mailSender property.
     *
     * @param mailSender the mailSender to set.
     */
    public void setMailSender(final IMailSenderService mailSender) {
        this.mailSender = mailSender;
    }

    /**
     * Setter for the messagesDao property.
     *
     * @param messagesDao the messagesDao to set.
     */
    public void setMessagesDao(final MessagesDao messagesDao) {
        this.messagesDao = messagesDao;
    }

    /**
     * Setter for the passwordResetTokenDao property.
     *
     * @param passwordResetTokenDao the passwordResetTokenDao to set.
     */
    public void setPasswordResetTokenDao(final IPasswordResetTokenDao passwordResetTokenDao) {
        this.passwordResetTokenDao = passwordResetTokenDao;
    }

    /**
     * Setter for the session property.
     *
     * @param session the session to set.
     */
    public void setSession(final Session session) {
        this.session = session;
    }

    /**
     * Prepares email body and subject. Body will contain URL. The URL loads login page, has URL
     * parameters resetPassword=true, token, projectId. Loads templates for subject and body from
     * messages table.
     *
     * @param project to be used as context.
     * @param token to be sent in email.
     * @param userSession to be used as context.
     * @return prepared body and subject.
     */
    private IMailSenderService.EmailParameters prepareBodyAndSubject(
            final Project.Immutable project, final String token,
            final UserSession.Immutable userSession) {
        final IMailSenderService.EmailParameters emailParameters =
                new IMailSenderService.EmailParameters();
        {
            // save current user session
            final Context context = ContextStore.get();
            final UserSession.Immutable savedUserSession = context.getUserSession();
            try {
                context.setUserSession(userSession);

                final TransactionTemplate transactionTemplate =
                        new TransactionTemplate(this.logger);
                transactionTemplate.setRole(DatabaseRole.SCHEMA);
                transactionTemplate.setReadOnly(false);

                transactionTemplate.doWithContext(new CallbackWithWrappedException() {
                    @Override
                    public Object doWithContext(final Context context) throws ExceptionBase {
                        // set sessionID in context
                        context.getSession()
                            .setId(PasswordResetServiceUtilities.RUN_AS_USER_SESSION_ID);

                        final String url = PasswordResetServiceUtilities.buildUri(project.getName(),
                            token, context.getBaseURL());

                        final String bodyTemplate = PasswordResetMailSender.this.messagesDao
                            .localizeMessage(
                                ACTIVITY_ID_AB_SYSTEM_ADMINISTRATION, PasswordResetMailSender.this
                                    .getClass().getSimpleName().toUpperCase(),
                                SEND_REQUEST_PASSWORD_RESET_BODY);

                        emailParameters.setBody(MessageFormat.format(bodyTemplate, url));

                        final String subject = PasswordResetMailSender.this.messagesDao
                            .localizeMessage(
                                ACTIVITY_ID_AB_SYSTEM_ADMINISTRATION, PasswordResetMailSender.this
                                    .getClass().getSimpleName().toUpperCase(),
                                SEND_REQUEST_PASSWORD_RESET_SUBJECT);
                        emailParameters.setSubject(subject);

                        return null;
                    }
                });
            } finally {
                // restore saved UserSession
                context.setUserSession(savedUserSession);
            }
        }

        return emailParameters;
    }

    /**
     * Prepares EmailParameters.
     *
     * @param userId of the user the email is prepared for.
     * @param project project to be used as context.
     * @param token to be sent.
     * @return prepared EmailParameters.
     */
    private IMailSenderService.EmailParameters prepareEmailParameters(final String userId,
            final Project.Immutable project, final String token) {
        final UserSession.Immutable userSession =
                PasswordResetServiceUtilities.createUserSession(project, userId);

        final IMailSenderService.EmailParameters emailParameters =
                prepareBodyAndSubject(project, token, userSession);

        // get locale and email from the account
        final Locale locale = userSession.getUserAccount().getLocale();
        final String userEmail = userSession.getUserAccount().getAttribute("/*/preferences/@email");
        if (StringUtil.isNullOrEmpty(userEmail)) {
            // @non-translatable
            final String message =
                    MessageFormat.format("e-mail was not supplied for user [{0}]", userId);
            throw new ExceptionBase(message);
        }

        // prepare mail body: localize and insert userId and email
        final String bodyLocalized = this.mailSender.prepareMessage(this.getClass().getName(),
            emailParameters.getBody(), null, locale);

        final String subjectLocalized = this.mailSender.prepareMessage(this.getClass().getName(),
            emailParameters.getSubject(), null, locale);

        final String administratorEMail = this.configManager.getAttribute(
            "/*/preferences/mail/addresses/address[@name='administratorEMail']/@value");
        Assert.hasLength(administratorEMail, "Administrator e-mail must be supplied");

        emailParameters.setBody(bodyLocalized);
        emailParameters.setFrom(administratorEMail);
        emailParameters.setSubject(subjectLocalized);
        emailParameters.setTo(userEmail);

        return emailParameters;
    }

    /**
     * Prepares ExceptionBase for non-authenticated user.
     *
     * @return prepared ExceptionBase.
     */
    private ExceptionBase prepareException() {
        // Don't show the original error message with security details to
        // non-authenticated user.
        // Replace e with new ExceptionBase, with error message suitable for
        // non-authenticated end-user.
        // @translatable
        final String message =
                "There is a problem resetting this ARCHIBUS user account. Please contact your administrator.";

        final ExceptionBase exception = new ExceptionBase(message);
        exception.setTranslatable(true);

        return exception;
    }

    /**
     * Saves token to database. Uses activity parameter expirePasswordResetTokenInMinutes to
     * determine when the token should expire.
     *
     * @param userId of the user the email is prepared for.
     * @param token token to be saved.
     * @param project to be used as context.
     */
    private void saveToken(final String userId, final String token,
            final Project.Immutable project) {
        final String parameterValue = project.getActivityParameterManager()
            .getParameterValue("AbSystemAdministration-PasswordResetTokenExpiration");
        Assert.hasLength(parameterValue,
            "Activity parameter AbSystemAdministration-PasswordResetTokenExpiration must be specified.");

        final int expireInMinutes = Integer.parseInt(parameterValue);
        final PasswordResetToken passwordResetToken =
                new PasswordResetToken(token, userId, expireInMinutes);
        this.passwordResetTokenDao.save(passwordResetToken);
    }
}
