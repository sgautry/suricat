package com.archibus.app.common.finance.dao;

import java.util.List;

import com.archibus.core.dao.IDao;
import com.archibus.datasource.restriction.Restrictions.Restriction;

/**
 * Provides common methods to load Violation Cost object that match specified
 * criteria.
 *
 * @author Razvan Croitoru
 *
 * @param <ViolationCost>
 *            Type of persistent object.
 */
public interface IViolationCostDao<ViolationCost> extends IDao<ViolationCost> {

	/**
	 * Generates WHERE clause from specified cost IDs.
	 *
	 * @param costIds
	 *            List of primary key values in the corresponding cost table.
	 * @return WHERE clause.
	 */
	String createSqlRestrictionForCosts(final List<Integer> costIds);

	/**
	 * Finds costs for specified cost IDs.
	 *
	 * @param costIds
	 *            List of primary key values in the cost table.
	 * @return list of costs.
	 */
	List<ViolationCost> findByCostIds(final List<Integer> costIds);

	/**
	 * Find cost by specified restriction.
	 *
	 * @param restriction
	 *            restriction
	 * @return list of costs
	 */
	List<ViolationCost> findByRestriction(final Restriction restriction);

	/**
	 * Get cost record by cost id.
	 *
	 * @param costId
	 *            cost id
	 * @return cost object
	 */
	ViolationCost getRecord(final int costId);
}
