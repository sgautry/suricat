package com.archibus.app.common.security.service.impl;

import java.util.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import com.archibus.app.common.security.dao.IPasswordResetTokenDao;
import com.archibus.app.common.security.domain.PasswordResetToken;
import com.archibus.app.common.security.providers.dao.PasswordPatternValidator;
import com.archibus.app.common.security.service.IPasswordResetMailSender;
import com.archibus.config.*;
import com.archibus.context.*;
import com.archibus.context.Context;
import com.archibus.context.utility.*;
import com.archibus.security.*;
import com.archibus.security.service.IPasswordResetService;
import com.archibus.utility.*;
import com.ibm.icu.text.MessageFormat;

/**
 * Implementation of the PasswordResetService service.
 * <p>
 * Non-authenticated user [using sing-in page] is expected to invoke methods in this service.
 *
 * @author Valery Tydykov
 * @since 23.2
 *
 */
public class PasswordResetService implements IPasswordResetService, InitializingBean {
    /**
     * Constant: ID of the "RunAsUser" session.
     */
    public static final String RUN_AS_USER_SESSION_ID = "runAsUserSessionId";

    /**
     * Constant: message for logger.
     */
    static final String THROWING_EXCEPTION_FOR_NON_AUTHENTICATED_USER_CAUSE =
            "Throwing exception for non-authenticated user, cause: ";

    /**
     * Constant: assert message.
     */
    private static final String PROJECT_ID_MUST_BE_SUPPLIED = "projectId must be supplied";

    /**
     * Constant: assert message.
     */
    private static final String PROJECT_ID_MUST_MATCH_A_PROJECT = "projectId must match a project";

    /**
     * Constant: error message.
     */
    // @translatable
    private static final String THE_LINK_TO_RESET_YOUR_PASSWORD_HAS_EXPIRED =
            "The link to reset your password has expired.";

    /**
     * Logger for this class and subclasses.
     */
    protected final Logger logger = Logger.getLogger(this.getClass());

    /**
     * Property: configManager.
     */
    private ConfigManager.Immutable configManager;

    /**
     * Property: passwordPatternValidator. Checks if new password conforms to the specified password
     * pattern policy.
     */
    private PasswordPatternValidator passwordPatternValidator;

    /**
     * Property: passwordResetMailSender. Prepares and sends to userId email with password reset
     * instructions.
     */
    private IPasswordResetMailSender passwordResetMailSender;

    /**
     * Property: passwordResetTokenDao.
     */
    private IPasswordResetTokenDao passwordResetTokenDao;

    /**
     * {@inheritDoc}
     * <p>
     * Suppress Warning "PMD.SignatureDeclareThrowsException"
     * <p>
     * Justification: This method implements Spring interface.
     */
    @Override
    @SuppressWarnings({ "PMD.SignatureDeclareThrowsException" })
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(this.configManager, "configManager must be supplied");
        Assert.notNull(this.passwordResetTokenDao, "passwordResetTokenDao must be supplied");
        Assert.notNull(this.passwordPatternValidator, "passwordPatternValidator must be supplied");
        Assert.notNull(this.passwordResetMailSender, "passwordResetMailSender must be supplied");
    }

    /**
     * Getter for the configManager property.
     *
     * @return the configManager property.
     */
    public ConfigManager.Immutable getConfigManager() {
        return this.configManager;
    }

    /**
     * Getter for the passwordPatternValidator property.
     *
     * @return the passwordPatternValidator property.
     */
    public PasswordPatternValidator getPasswordPatternValidator() {
        return this.passwordPatternValidator;
    }

    /**
     * Getter for the passwordResetMailSender property.
     *
     * @return the passwordResetMailSender property.
     */
    public IPasswordResetMailSender getPasswordResetMailSender() {
        return this.passwordResetMailSender;
    }

    /**
     * Getter for the passwordResetTokenDao property.
     *
     * @return the passwordResetTokenDao property.
     */
    public IPasswordResetTokenDao getPasswordResetTokenDao() {
        return this.passwordResetTokenDao;
    }

    @Override
    public boolean isPasswordResetPermitted(final String projectId) {
        Assert.hasLength(projectId, PROJECT_ID_MUST_BE_SUPPLIED);

        final Project.Immutable project = this.configManager.findProject(projectId);
        Assert.notNull(project, PROJECT_ID_MUST_MATCH_A_PROJECT);

        final String parameterValue = project.getActivityParameterManager()
            .getParameterValue("AbSystemAdministration-PasswordResetPermitted");

        return StringUtil.toBoolean(parameterValue);
    }

    @Override
    public void removeExpiredTokens() throws ExceptionBase {
        if (this.logger.isInfoEnabled()) {
            this.logger.info("removeExpiredTokens");
        }

        this.passwordResetTokenDao.deleteAllExpired();
    }

    @Override
    public void requestPasswordReset(final String userId, final String projectId)
            throws ExceptionBase {
        if (this.logger.isInfoEnabled()) {
            this.logger.info(MessageFormat.format(
                "requestPasswordReset for userId=[{0}], projectId=[{1}]", userId, projectId));
        }

        Assert.hasLength(userId, "userId must be supplied");
        Assert.hasLength(projectId, PROJECT_ID_MUST_BE_SUPPLIED);

        this.passwordResetMailSender.prepareAndSendEmail(userId, projectId);
    }

    @Override
    public void resetPassword(final String password) throws ExceptionBase {
        if (this.logger.isInfoEnabled()) {
            this.logger.info("resetPassword");
        }

        // User is not logged in

        // validate new password pattern
        this.passwordPatternValidator.validate(password);

        // Use user session saved in SecurityController as current context
        final Context context = ContextStore.get();
        final UserSession.Immutable userSession = context.getSecurityController().getUserSession();
        Assert.notNull(userSession, "userSession must be set in SecurityController");

        // Treat user as authenticated with limited access
        // Do not hide translatable exceptions
        final UserAccount.ThreadSafe userAccount =
                (UserAccount.ThreadSafe) userSession.getUserAccount();
        {
            // save current user session
            final UserSession.Immutable savedUserSession = context.getUserSession();
            try {
                context.setUserSession(userSession);

                final TransactionTemplate transactionTemplate =
                        new TransactionTemplate(this.logger);
                transactionTemplate.setRole(DatabaseRole.SECURITY);
                transactionTemplate.setReadOnly(false);

                transactionTemplate.doWithContext(new CallbackWithWrappedException() {
                    @Override
                    public Object doWithContext(final Context context) throws ExceptionBase {
                        // update password in user account
                        userAccount.changePassword(password);
                        // save new password
                        UserAccountLoaderImpl.saveUserAccount(userAccount);

                        return null;
                    }
                });
            } finally {
                // restore saved UserSession
                context.setUserSession(savedUserSession);

                // Do not allow password reset on next request
                ContextStore.get().getSecurityController().transitionToNotAuthenticatedState();
            }
        }
    }

    /**
     * Setter for the configManager property.
     *
     * @param configManager the configManager to set.
     */
    public void setConfigManager(final ConfigManager.Immutable configManager) {
        this.configManager = configManager;
    }

    /**
     * Setter for the passwordPatternValidator property.
     *
     * @param passwordPatternValidator the passwordPatternValidator to set.
     */
    public void setPasswordPatternValidator(
            final PasswordPatternValidator passwordPatternValidator) {
        this.passwordPatternValidator = passwordPatternValidator;
    }

    /**
     * Setter for the passwordResetMailSender property.
     *
     * @param passwordResetMailSender the passwordResetMailSender to set.
     */
    public void setPasswordResetMailSender(final IPasswordResetMailSender passwordResetMailSender) {
        this.passwordResetMailSender = passwordResetMailSender;
    }

    /**
     * Setter for the passwordResetTokenDao property.
     *
     * @param passwordResetTokenDao the passwordResetTokenDao to set.
     */
    public void setPasswordResetTokenDao(final IPasswordResetTokenDao passwordResetTokenDao) {
        this.passwordResetTokenDao = passwordResetTokenDao;
    }

    @Override
    public String validateAndRemoveTokenCreateUserSession(final String token,
            final String projectId) throws ExceptionBase {
        if (this.logger.isInfoEnabled()) {
            this.logger.info(MessageFormat.format(
                "validateAndRemoveTokenCreateUserSession for token=[{0}], projectId=[{1}]", token,
                projectId));
        }

        Project.Immutable project = null;
        try {
            Assert.hasLength(projectId, PROJECT_ID_MUST_BE_SUPPLIED);
            Assert.hasLength(token, "token must be supplied");

            project = this.configManager.findProject(projectId);
            Assert.notNull(project, PROJECT_ID_MUST_MATCH_A_PROJECT);

            // User is not authenticated

            final PasswordResetToken passwordResetToken = validateAndRemoveToken(token, project);

            checkExpiration(passwordResetToken.getExpirationTime());

            final UserSession.Immutable userSession = PasswordResetServiceUtilities
                .createUserSession(project, passwordResetToken.getUserName());

            return userSession.getKey();
        } catch (final IllegalArgumentException e) {
            // log e here, since it is not passed as nested here
            this.logger.error(
                PasswordResetService.THROWING_EXCEPTION_FOR_NON_AUTHENTICATED_USER_CAUSE + e);

            throw prepareExceptionInvalidLink();
        } catch (final ExceptionBase e) {
            // log e here, since it is not passed as nested here
            this.logger.error(
                THROWING_EXCEPTION_FOR_NON_AUTHENTICATED_USER_CAUSE + e.toStringForLogging());

            // User should see either link expired or invalid link message.
            ExceptionBase exception = e;
            if (!e.getPattern().equals(THE_LINK_TO_RESET_YOUR_PASSWORD_HAS_EXPIRED)) {
                exception = prepareExceptionInvalidLink();
            }

            throw exception;
        }
    }

    /**
     * Checks if expirationTime is in the past.
     *
     * @param date date and time to be checked.
     * @throws ExceptionBase If expirationTime is in the past.
     */
    private void checkExpiration(final Date date) throws ExceptionBase {
        // if current time is after expiration time
        if (Calendar.getInstance().getTime().after(date)) {
            final ExceptionBase exception =
                    new ExceptionBase(THE_LINK_TO_RESET_YOUR_PASSWORD_HAS_EXPIRED);
            exception.setTranslatable(true);

            throw exception;
        }
    }

    /**
     * Prepares ExceptionBase with message "The link to reset your password is invalid.".
     *
     * @return prepared exception.
     */
    private ExceptionBase prepareExceptionInvalidLink() {
        // @translatable
        final String message =
                "The link to reset your password has expired.  Please click on 'I forgot my password' again.";

        final ExceptionBase exception = new ExceptionBase(message);
        exception.setTranslatable(true);

        return exception;
    }

    /**
     * Validates token. If the token is found, removes it, so that it could not be used anymore.
     *
     * @param token to be validated.
     * @param project to be used as context.
     * @return found PasswordResetToken.
     * @throws ExceptionBase If token not found.
     */
    private PasswordResetToken validateAndRemoveToken(final String token,
            final Project.Immutable project) throws ExceptionBase {
        final PasswordResetToken result = new PasswordResetToken();

        final Context context = ContextStore.get();
        context.setProject(project);
        // use core user session as context
        final CoreUserSessionTemplate template = new CoreUserSessionTemplate();
        template.setDatabaseRole(DatabaseRole.SECURITY);
        template.execute(new Runnable() {
            @Override
            public void run() {
                final PasswordResetToken tokenFound =
                        PasswordResetService.this.passwordResetTokenDao.findByToken(token);
                if (tokenFound == null) {
                    throw prepareExceptionInvalidLink();
                }

                // Delete the token, so that it could not be used anymore
                PasswordResetService.this.passwordResetTokenDao.delete(tokenFound);

                result.setExpirationTime(tokenFound.getExpirationTime());
                result.setUserName(tokenFound.getUserName());
            }
        });

        return result;
    }
}
