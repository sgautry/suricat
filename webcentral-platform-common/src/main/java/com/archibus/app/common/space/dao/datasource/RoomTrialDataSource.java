package com.archibus.app.common.space.dao.datasource;

import java.util.List;

import com.archibus.app.common.space.dao.IRoomTrialDao;
import com.archibus.app.common.space.domain.RoomTrial;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecordField;
import com.archibus.model.view.datasource.ClauseDef.Operation;
import com.archibus.model.view.datasource.ParsedRestrictionDef;

/**
 * DataSource for RoomTrial.
 *
 * @see ObjectDataSourceImpl.
 *
 * @author Jikai Xu
 *
 */
public class RoomTrialDataSource extends ObjectDataSourceImpl<RoomTrial> implements IRoomTrialDao {

	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { "bl_id", "buildingId" }, { "fl_id", "floorId" },
			{ "rm_id", "roomId" }, { "layer_name", "layerName" }, { "rm_type", "roomType" },
			{ "rm_cat", "roomCategory" }, { "dp_id", "departmentId" }, { "dv_id", "divisionId" },
			{ "rm_std", "roomStandard" }, { "area", "area" }, { "cap_em", "employeeCapacity" },
			{ "dwgname", "dwgname" }, { "trial_project_id", "trialProjectId" }, { "ehandler", "ehandler" } };

	/**
	 * Constructs RoomDataSource, mapped to <code>rm</code> table, using
	 * <code>roomBean</code> bean.
	 */
	public RoomTrialDataSource() {
		super("roomTrialBean", "rm_trial");
	}

	/** {@inheritDoc} */
	@Override
	public RoomTrial getByPrimaryKey(final RoomTrial roomTrial) {
		final PrimaryKeysValues primaryKeysValues = new PrimaryKeysValues();
		{
			final DataRecordField pkField = new DataRecordField();
			pkField.setName(this.tableName + DOT + Constants.RM_ID);
			pkField.setValue(roomTrial.getRoomId());
			primaryKeysValues.getFieldsValues().add(pkField);
		}

		{
			final DataRecordField pkField = new DataRecordField();
			pkField.setName(this.tableName + DOT + Constants.BL_ID);
			pkField.setValue(roomTrial.getBuildingId());
			primaryKeysValues.getFieldsValues().add(pkField);
		}

		{
			final DataRecordField pkField = new DataRecordField();
			pkField.setName(this.tableName + DOT + Constants.FL_ID);
			pkField.setValue(roomTrial.getFloorId());
			primaryKeysValues.getFieldsValues().add(pkField);
		}

		{
			final DataRecordField pkField = new DataRecordField();
			pkField.setName(this.tableName + DOT + Constants.LAYERNAME);
			pkField.setValue(roomTrial.getLayerName());
			primaryKeysValues.getFieldsValues().add(pkField);
		}

		return this.get(primaryKeysValues);
	}

	/**
	 * Find by floor.
	 *
	 * @param blId
	 *            the bl id
	 * @param flId
	 *            the fl id
	 * @return the list
	 */
	public final List<RoomTrial> findByFloor(final String blId, final String flId) {
		final ParsedRestrictionDef restriction = new ParsedRestrictionDef();
		restriction.addClause(this.tableName, "bl_id", blId, Operation.EQUALS);
		restriction.addClause(this.tableName, "fl_id", flId, Operation.EQUALS);
		return this.find(restriction);
	}

	@Override
	protected String[][] getFieldsToProperties() {
		return FIELDS_TO_PROPERTIES.clone();
	}
}
