package com.archibus.app.common.bldgops.domain;

import java.util.Date;

/**
 * Domain object for Work Request.
 * <p>
 * Mapped to wr table.
 *
 * @author Zhang Yi
 *
 */
public class WorkRequest {

	/**
	 * Activity Log Id.
	 */
	private String actionItemId;

	/**
	 * Work Request Id.
	 */
	private String id;

	/**
	 * Work Request Assigned to.
	 */
	private String assignedTo;

	/**
	 * Work Request assessment Id.
	 */
	private Integer assessmentId;

	/**
	 * Work Request activity Type
	 */
	private String activityType;

	/**
	 * Work Request building Id.
	 */
	private String buildingId;

	/**
	 * Work Request floor id.
	 */
	private String floorId;

	/**
	 * Work Request room Id.
	 */
	private String roomId;

	/**
	 * Work Request site Id.
	 */
	private String siteId;

	/**
	 * Work Request problem Type.
	 */
	private String problemType;

	/**
	 * Work Request date Required.
	 */
	private Date dateRequired;

	/**
	 * Work Request date Scheduled.
	 */
	private Date dateScheduled;

	/**
	 * Work Request cost Estimated.
	 */
	private Double costEstimated;

	/**
	 * Work Request hours estimated baseline.
	 */
	private Integer hoursEstBaseline;

	/**
	 * Work Request division Id.
	 */
	private String divisionId;

	/**
	 * Work Request department Id.
	 */
	private String departmentId;

	/**
	 * Work Request phone of Requestor.
	 */
	private String phoneRequestor;

	/**
	 * Work Request account Id.
	 */
	private String accountId;

	/**
	 * Work Request priority.
	 */
	private int priority;

	/**
	 * Work Request created By.
	 */
	private String createdBy;

	/**
	 * Work Request requestor.
	 */
	private String requestor;

	/**
	 * Work Request Description.
	 */
	private String description;

	/**
	 * Getter for the actionItemId property.
	 *
	 * @return the actionItemId property.
	 */
	public String getActionItemId() {
		return this.actionItemId;
	}

	/**
	 * Setter for the actionItemId property.
	 *
	 * @param actionItemId
	 *            the actionItemId to set.
	 */
	public void setActionItemId(final String actionItemId) {
		this.actionItemId = actionItemId;
	}

	/**
	 * Getter for the id property.
	 *
	 * @return the id property.
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * Setter for the id property.
	 *
	 * @param id
	 *            the id to set.
	 */
	public void setId(final String id) {
		this.id = id;
	}

	/**
	 * Getter for the assignedTo property.
	 *
	 * @return the assignedTo property.
	 */
	public String getAssignedTo() {
		return this.assignedTo;
	}

	/**
	 * Setter for the assignedTo property.
	 *
	 * @param assignedTo
	 *            the assignedTo to set.
	 */
	public void setAssignedTo(final String assignedTo) {
		this.assignedTo = assignedTo;
	}

	/**
	 * Getter for the assessmentId property.
	 *
	 * @return the assessmentId property.
	 */
	public Integer getAssessmentId() {
		return this.assessmentId;
	}

	/**
	 * Setter for the assessmentId property.
	 *
	 * @param assessmentId
	 *            the assessmentId to set.
	 */
	public void setAssessmentId(final Integer assessmentId) {
		this.assessmentId = assessmentId;
	}

	/**
	 * Getter for the activityType property.
	 *
	 * @return the activityType property.
	 */
	public String getActivityType() {
		return this.activityType;
	}

	/**
	 * Setter for the activityType property.
	 *
	 * @param activityType
	 *            the activityType to set.
	 */
	public void setActivityType(final String activityType) {
		this.activityType = activityType;
	}

	/**
	 * Getter for the buildingId property.
	 *
	 * @return the buildingId property.
	 */
	public String getBuildingId() {
		return this.buildingId;
	}

	/**
	 * Setter for the buildingId property.
	 *
	 * @param buildingId
	 *            the buildingId to set.
	 */
	public void setBuildingId(final String buildingId) {
		this.buildingId = buildingId;
	}

	/**
	 * Getter for the floorId property.
	 *
	 * @return the floorId property.
	 */
	public String getFloorId() {
		return this.floorId;
	}

	/**
	 * Setter for the floorId property.
	 *
	 * @param floorId
	 *            the floorId to set.
	 */
	public void setFloorId(final String floorId) {
		this.floorId = floorId;
	}

	/**
	 * Getter for the roomId property.
	 *
	 * @return the roomId property.
	 */
	public String getRoomId() {
		return this.roomId;
	}

	/**
	 * Setter for the roomId property.
	 *
	 * @param roomId
	 *            the roomId to set.
	 */
	public void setRoomId(final String roomId) {
		this.roomId = roomId;
	}

	/**
	 * Getter for the siteId property.
	 *
	 * @return the siteId property.
	 */
	public String getSiteId() {
		return this.siteId;
	}

	/**
	 * Setter for the siteId property.
	 *
	 * @param siteId
	 *            the siteId to set.
	 */
	public void setSiteId(final String siteId) {
		this.siteId = siteId;
	}

	/**
	 * Getter for the problemType property.
	 *
	 * @return the problemType property.
	 */
	public String getProblemType() {
		return this.problemType;
	}

	/**
	 * Setter for the problemType property.
	 *
	 * @param problemType
	 *            the problemType to set.
	 */
	public void setProblemType(final String problemType) {
		this.problemType = problemType;
	}

	/**
	 * Getter for the dateRequired property.
	 *
	 * @return the dateRequired property.
	 */
	public Date getDateRequired() {
		return this.dateRequired;
	}

	/**
	 * Setter for the dateRequired property.
	 *
	 * @param dateRequired
	 *            the dateRequired to set.
	 */
	public void setDateRequired(final Date dateRequired) {
		this.dateRequired = dateRequired;
	}

	/**
	 * Getter for the dateScheduled property.
	 *
	 * @return the dateScheduled property.
	 */
	public Date getDateScheduled() {
		return this.dateScheduled;
	}

	/**
	 * Setter for the dateScheduled property.
	 *
	 * @param dateScheduled
	 *            the dateScheduled to set.
	 */
	public void setDateScheduled(final Date dateScheduled) {
		this.dateScheduled = dateScheduled;
	}

	/**
	 * Getter for the costEstimated property.
	 *
	 * @return the costEstimated property.
	 */
	public Double getCostEstimated() {
		return this.costEstimated;
	}

	/**
	 * Setter for the costEstimated property.
	 *
	 * @param costEstimated
	 *            the costEstimated to set.
	 */
	public void setCostEstimated(final Double costEstimated) {
		this.costEstimated = costEstimated;
	}

	/**
	 * Getter for the hoursEstBaseline property.
	 *
	 * @return the hoursEstBaseline property.
	 */
	public Integer getHoursEstBaseline() {
		return this.hoursEstBaseline;
	}

	/**
	 * Setter for the hoursEstBaseline property.
	 *
	 * @param hoursEstBaseline
	 *            the hoursEstBaseline to set.
	 */
	public void setHoursEstBaseline(final Integer hoursEstBaseline) {
		this.hoursEstBaseline = hoursEstBaseline;
	}

	/**
	 * Getter for the divisionId property.
	 *
	 * @return the divisionId property.
	 */
	public String getDivisionId() {
		return this.divisionId;
	}

	/**
	 * Setter for the divisionId property.
	 *
	 * @param divisionId
	 *            the divisionId to set.
	 */
	public void setDivisionId(final String divisionId) {
		this.divisionId = divisionId;
	}

	/**
	 * Getter for the departmentId property.
	 *
	 * @return the departmentId property.
	 */
	public String getDepartmentId() {
		return this.departmentId;
	}

	/**
	 * Setter for the departmentId property.
	 *
	 * @param departmentId
	 *            the departmentId to set.
	 */
	public void setDepartmentId(final String departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * Getter for the phoneRequestor property.
	 *
	 * @return the phoneRequestor property.
	 */
	public String getPhoneRequestor() {
		return this.phoneRequestor;
	}

	/**
	 * Setter for the phoneRequestor property.
	 *
	 * @param phoneRequestor
	 *            the phoneRequestor to set.
	 */
	public void setPhoneRequestor(final String phoneRequestor) {
		this.phoneRequestor = phoneRequestor;
	}

	/**
	 * Getter for the accountId property.
	 *
	 * @return the accountId property.
	 */
	public String getAccountId() {
		return this.accountId;
	}

	/**
	 * Setter for the accountId property.
	 *
	 * @param accountId
	 *            the accountId to set.
	 */
	public void setAccountId(final String accountId) {
		this.accountId = accountId;
	}

	/**
	 * Getter for the priority property.
	 *
	 * @return the priority property.
	 */
	public int getPriority() {
		return this.priority;
	}

	/**
	 * Setter for the priority property.
	 *
	 * @param priority
	 *            the priority to set.
	 */
	public void setPriority(final int priority) {
		this.priority = priority;
	}

	/**
	 * Getter for the createdBy property.
	 *
	 * @return the createdBy property.
	 */
	public String getCreatedBy() {
		return this.createdBy;
	}

	/**
	 * Setter for the createdBy property.
	 *
	 * @param createdBy
	 *            the createdBy to set.
	 */
	public void setCreatedBy(final String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Getter for the requestor property.
	 *
	 * @return the requestor property.
	 */
	public String getRequestor() {
		return this.requestor;
	}

	/**
	 * Setter for the requestor property.
	 *
	 * @param requestor
	 *            the requestor to set.
	 */
	public void setRequestor(final String requestor) {
		this.requestor = requestor;
	}

	/**
	 * Getter for the description property.
	 *
	 * @return the description property.
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Setter for the description property.
	 *
	 * @param description
	 *            the description to set.
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

}
