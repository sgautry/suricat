package com.archibus.app.common.finance.dao.datasource;

import java.util.List;

import com.archibus.app.common.finance.dao.IViolationCostDao;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;
import com.archibus.datasource.restriction.Restrictions.Restriction;
import com.archibus.db.ViewField;

/**
 * Class for Violation Cost DataSource.
 * <p>
 *
 * @author Razvan Croitoru
 *
 * @param <ViolationCost>
 *            Type of persistent object.
 */
public class ViolationCostDataSource<ViolationCost> extends ObjectDataSourceImpl<ViolationCost>
		implements IViolationCostDao<ViolationCost> {

	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 * <p>
	 * Fields common for all Cost DataSources are specified here.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { "authority", "authority" },
			{ "cost_base_budget", "costBaseBudget" }, { "cost_base_payment", "costBasePayment" },
			{ "cost_cat_id", "costCatId" }, { "cost_total", "costTotal" }, { "cost_total_payment", "costTotalPayment" },
			{ "cost_vat_budget", "costVatBudget" }, { "cost_vat_payment", "costVatPayment" }, { "ctry_id", "ctryId" },
			{ "currency_budget", "currencyBudget" }, { "currency_payment", "currencyPayment" },
			{ "date_assessed", "dateAssessed" }, { "date_from", "dateFrom" }, { "date_to", "dateTo" },
			{ "date_used_for_mc_budget", "dateUsedForMcBudget" },
			{ "date_used_for_mc_payment", "dateUsedForMcPayment" }, { "description", "description" }, { "doc", "doc" },
			{ "exchange_rate_budget", "exchangeRateBudget" }, { "exchange_rate_override", "exchangeRateOverride" },
			{ "exchange_rate_payment", "exchangeRatePayment" }, { "issued_by", "issuedBy" },
			{ "location_id", "locationId" }, { "penalty", "penalty" }, { "reg_program", "regProgram" },
			{ "reg_requirement", "regRequirement" }, { "regulation", "regulation" }, { "severity", "severity" },
			{ "status", "status" }, { "summary", "summary" }, { "vat_amount_override", "vatAmountOverride" },
			{ "vat_percent_override", "vatPercentOverride" }, { "vat_percent_value", "vatPercentValue" },
			{ "violation_id", "violationId" }, { "violation_num", "id" }, { "violation_type", "violationType" } };

	/**
	 * Constant: SQL keyword: " OR ".
	 */
	private static final String SQL_KEYWORD_OR = " OR ";

	/**
	 * table, using <code>beanName</code> bean.
	 *
	 *
	 */
	public ViolationCostDataSource() {
		super("violationCost", "regviolation");
		setApplyVpaRestrictions(false);
	}

	/** {@inheritDoc} */
	@Override
	public String createSqlRestrictionForCosts(final List<Integer> costIds) {
		final StringBuffer sql = new StringBuffer();

		final DataSourceImpl dataSource = (DataSourceImpl) this.createCopy();
		dataSource.checkSetContext();

		final ViewField.Immutable firstPkField = dataSource.getPrimaryKeyFields().get(0);

		for (int i = 0; i < costIds.size(); i++) {
			final Object costId = costIds.get(i).toString();

			if (i > 0) {
				sql.append(SQL_KEYWORD_OR);
			}

			sql.append(firstPkField.getName());
			sql.append(" = ");
			sql.append(costId);
		}

		return sql.toString();
	}

	/** {@inheritDoc} */
	@Override
	public List<ViolationCost> findByCostIds(final List<Integer> costIds) {
		final DataSourceImpl dataSource = (DataSourceImpl) this.createCopy();
		dataSource.setApplyVpaRestrictions(false);
		dataSource.checkSetContext();

		final String costIdRestriction = createSqlRestrictionForCosts(costIds);
		final List<DataRecord> records = dataSource.getRecords(costIdRestriction);

		return new DataSourceObjectConverter<ViolationCost>().convertRecordsToObjects(records, this.beanName,
				this.fieldToPropertyMapping, null);
	}

	/** {@inheritDoc} */
	@Override
	public List<ViolationCost> findByRestriction(final Restriction restriction) {
		final DataSource dataSource = this.createCopy();
		dataSource.setApplyVpaRestrictions(false);
		dataSource.addRestriction(restriction);
		final List<DataRecord> records = dataSource.getRecords();

		return new DataSourceObjectConverter<ViolationCost>().convertRecordsToObjects(records, this.beanName,
				this.fieldToPropertyMapping, null);
	}

	/** {@inheritDoc} */
	@Override
	public ViolationCost getRecord(final int costId) {
		final DataSourceImpl dataSource = (DataSourceImpl) this.createCopy();
		// Kb 3047002 Don't apply VPA restriction
		dataSource.setApplyVpaRestrictions(false);
		dataSource.checkSetContext();

		final ViewField.Immutable firstPkField = dataSource.getPrimaryKeyFields().get(0);
		dataSource.addRestriction(Restrictions.eq(this.tableName, firstPkField.getName(), costId));
		final DataRecord record = dataSource.getRecord();
		return new DataSourceObjectConverter<ViolationCost>().convertRecordToObject(record, this.beanName,
				this.fieldToPropertyMapping, null);
	}

	@Override
	protected String[][] getFieldsToProperties() {
		return FIELDS_TO_PROPERTIES.clone();
	}
}
