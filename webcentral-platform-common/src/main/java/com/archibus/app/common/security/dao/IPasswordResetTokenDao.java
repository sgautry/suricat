package com.archibus.app.common.security.dao;

import com.archibus.app.common.security.domain.PasswordResetToken;
import com.archibus.core.dao.IDao;

/**
 * DAO for PasswordResetToken.
 *
 * @author Valery Tydykov
 * @since 23.2
 *
 */

public interface IPasswordResetTokenDao extends IDao<PasswordResetToken> {

    /**
     * Deletes all expired tokens.
     */
    void deleteAllExpired();

    /**
     * Finds PasswordResetToken by specified token.
     *
     * @param token value of token to be used as filter.
     * @return PasswordResetToken if a single PasswordResetToken found, or null.
     */
    PasswordResetToken findByToken(String token);

    /**
     * Returns the persistent object with the given PK values, or null if there is no such object.
     *
     * @param id primary key value.
     * @return persistent object with the given PK values, or null.
     */
    PasswordResetToken getByPrimaryKey(int id);
}