package com.archibus.app.common.security.service.impl;

import javax.ws.rs.core.UriBuilder;

import com.archibus.config.*;
import com.archibus.security.UserAccount;

/**
 * Utilities for PasswordResetService.
 *
 * @author Valery Tydykov
 * @since 23.2
 */
public final class PasswordResetServiceUtilities {
    /**
     * Constant: ID of the "RunAsUser" session.
     */
    static final String RUN_AS_USER_SESSION_ID = "runAsUserSessionId";

    /**
     * Private default constructor: utility class is non-instantiable.
     */
    private PasswordResetServiceUtilities() {
    }

    /**
     * Builds URI that loads "Reset Password" dialog.
     *
     * @param projectId of the project to be used as context.
     * @param token to be sent.
     * @param baseUrl ofWebCentral.
     * @return prepared URI.
     */
    static String buildUri(final String projectId, final String token, final String baseUrl) {
        final UriBuilder builder =
                UriBuilder.fromPath(baseUrl).path("login.axvw").queryParam("resetPassword", "true")
                    .queryParam("token", token).queryParam("projectId", projectId);

        return builder.build().toString();
    }

    /**
     * Creates UserSession for userId, project, session ID "runAsUserSessionId".
     *
     * @param project to be used as context.
     * @param userId ID of the user for whom the session should be created.
     * @return created user session.
     */
    static UserSession.Immutable createUserSession(final Project.Immutable project,
            final String userId) {
        final UserAccount.Immutable userAccount =
                project.loadUserAccount(userId, RUN_AS_USER_SESSION_ID, false);

        return project.loadUserSession(RUN_AS_USER_SESSION_ID, userId, userAccount.getKey());
    }
}
