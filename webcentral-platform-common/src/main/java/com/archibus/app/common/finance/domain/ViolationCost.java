package com.archibus.app.common.finance.domain;

import java.util.Date;

import com.archibus.utility.Utility;

/**
 * Domain object for Violation Cost.
 * <p>
 * Mapped to regviolation table.
 *
 * @author Razvan Croitoru
 *         <p>
 *         SuppressWarnings Justification: this class represent a data base
 *         record. Using nested classes will make the code difficult to
 *         understand.
 *         <p>
 */
@SuppressWarnings({ "PMD.ExcessivePublicCount", "PMD.TooManyFields", "PMD.ConfusingTernary",
		"PMD.ExcessiveClassLength" })
public class ViolationCost {

	/**
	 * Class name constant.
	 */
	static final String CLASS_VIOLATION_COST = "ViolationCost";

	// ----------------------- persistent state --------------------------------
	/**
	 * Field.
	 */
	private String authority;

	/**
	 * Field.
	 */
	private double costBaseBudget;

	/**
	 * Field violation_num.
	 */
	private int id;

	/**
	 * Field.
	 */
	private double costBasePayment;

	/**
	 * Field.
	 */
	private String costCatId;

	/**
	 * Field.
	 */
	private double costTotal;

	/**
	 * Field.
	 */
	private double costTotalPayment;

	/**
	 * Field.
	 */
	private double costVatBudget;

	/**
	 * Field.
	 */
	private double costVatPayment;

	/**
	 * Field.
	 */
	private String ctryId;

	/**
	 * Field.
	 */
	private String currencyBudget;

	/**
	 * Field.
	 */
	private String currencyPayment;

	/**
	 * Field.
	 */
	private Date dateAssessed;

	/**
	 * Field.
	 */
	private Date dateUsedForMcBudget;

	/**
	 * Field.
	 */
	private Date dateUsedForMcPayment;

	/**
	 * Field.
	 */
	private Date dateFrom;

	/**
	 * Field.
	 */
	private Date dateTo;

	/**
	 * Field.
	 */
	private double exchangeRateBudget;

	/**
	 * Field.
	 */
	private double exchangeRateOverride;

	/**
	 * Field.
	 */
	private double exchangeRatePayment;

	/**
	 * Field.
	 */
	private String description;

	/**
	 * Field.
	 */
	private String doc;

	/**
	 * Field.
	 */
	private String issuedBy;

	/**
	 * Field.
	 */
	private Integer locationId;

	/**
	 * Field.
	 */
	private String penalty;

	/**
	 * Field.
	 */
	private String regProgram;

	/**
	 * Field.
	 */
	private String regRequirement;

	/**
	 * Field.
	 */
	private String regulation;

	/**
	 * Field.
	 */
	private int severity;

	/**
	 * Field.
	 */
	private String status;

	/**
	 * Field.
	 */
	private String summary;

	/**
	 * Field.
	 */
	private double vatAmountOverride;

	/**
	 * Field.
	 */
	private double vatPercentOverride;

	/**
	 * Field.
	 */
	private double vatPercentValue;

	/**
	 * Field.
	 */
	private String violationId;

	/**
	 * Field.
	 */
	private String violationType;

	// ----------------------- business methods --------------------------------

	@Override
	public int hashCode() {
		int result = Utility.HASH_CODE_SEED;
		result = Utility.HASH_CODE_PRIME * result + this.id;
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		return obj instanceof ViolationCost && this.id == ((ViolationCost) obj).getId()
				&& this.getCostClass().equals(((ViolationCost) obj).getCostClass());
	}

	/**
	 * Return Cost class type.
	 *
	 * @return class name costant.
	 */
	public String getCostClass() {
		return CLASS_VIOLATION_COST;
	}

	/**
	 * Getter for the authority property.
	 *
	 * @return the authority property.
	 */
	public String getAuthority() {
		return this.authority;
	}

	/**
	 * Setter for the authority property.
	 *
	 * @param authority
	 *            the authority to set.
	 */
	public void setAuthority(final String authority) {
		this.authority = authority;
	}

	/**
	 * Getter for the costBaseBudget property.
	 *
	 * @return the costBaseBudget property.
	 */
	public double getCostBaseBudget() {
		return this.costBaseBudget;
	}

	/**
	 * Setter for the costBaseBudget property.
	 *
	 * @param costBaseBudget
	 *            the costBaseBudget to set.
	 */
	public void setCostBaseBudget(final double costBaseBudget) {
		this.costBaseBudget = costBaseBudget;
	}

	/**
	 * Getter for the id property.
	 *
	 * @return the id property.
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * Setter for the id property.
	 *
	 * @param id
	 *            the id to set.
	 */
	public void setId(final int id) {
		this.id = id;
	}

	/**
	 * Getter for the costBasePayment property.
	 *
	 * @return the costBasePayment property.
	 */
	public double getCostBasePayment() {
		return this.costBasePayment;
	}

	/**
	 * Setter for the costBasePayment property.
	 *
	 * @param costBasePayment
	 *            the costBasePayment to set.
	 */
	public void setCostBasePayment(final double costBasePayment) {
		this.costBasePayment = costBasePayment;
	}

	/**
	 * Getter for the costCatId property.
	 *
	 * @return the costCatId property.
	 */
	public String getCostCatId() {
		return this.costCatId;
	}

	/**
	 * Setter for the costCatId property.
	 *
	 * @param costCatId
	 *            the costCatId to set.
	 */
	public void setCostCatId(final String costCatId) {
		this.costCatId = costCatId;
	}

	/**
	 * Getter for the costTotal property.
	 *
	 * @return the costTotal property.
	 */
	public double getCostTotal() {
		return this.costTotal;
	}

	/**
	 * Setter for the costTotal property.
	 *
	 * @param costTotal
	 *            the costTotal to set.
	 */
	public void setCostTotal(final double costTotal) {
		this.costTotal = costTotal;
	}

	/**
	 * Getter for the costTotalPayment property.
	 *
	 * @return the costTotalPayment property.
	 */
	public double getCostTotalPayment() {
		return this.costTotalPayment;
	}

	/**
	 * Setter for the costTotalPayment property.
	 *
	 * @param costTotalPayment
	 *            the costTotalPayment to set.
	 */
	public void setCostTotalPayment(final double costTotalPayment) {
		this.costTotalPayment = costTotalPayment;
	}

	/**
	 * Getter for the costVatBudget property.
	 *
	 * @return the costVatBudget property.
	 */
	public double getCostVatBudget() {
		return this.costVatBudget;
	}

	/**
	 * Setter for the costVatBudget property.
	 *
	 * @param costVatBudget
	 *            the costVatBudget to set.
	 */
	public void setCostVatBudget(final double costVatBudget) {
		this.costVatBudget = costVatBudget;
	}

	/**
	 * Getter for the costVatPayment property.
	 *
	 * @return the costVatPayment property.
	 */
	public double getCostVatPayment() {
		return this.costVatPayment;
	}

	/**
	 * Setter for the costVatPayment property.
	 *
	 * @param costVatPayment
	 *            the costVatPayment to set.
	 */
	public void setCostVatPayment(final double costVatPayment) {
		this.costVatPayment = costVatPayment;
	}

	/**
	 * Getter for the ctryId property.
	 *
	 * @return the ctryId property.
	 */
	public String getCtryId() {
		return this.ctryId;
	}

	/**
	 * Setter for the ctryId property.
	 *
	 * @param ctryId
	 *            the ctryId to set.
	 */
	public void setCtryId(final String ctryId) {
		this.ctryId = ctryId;
	}

	/**
	 * Getter for the currencyBudget property.
	 *
	 * @return the currencyBudget property.
	 */
	public String getCurrencyBudget() {
		return this.currencyBudget;
	}

	/**
	 * Setter for the currencyBudget property.
	 *
	 * @param currencyBudget
	 *            the currencyBudget to set.
	 */
	public void setCurrencyBudget(final String currencyBudget) {
		this.currencyBudget = currencyBudget;
	}

	/**
	 * Getter for the currencyPayment property.
	 *
	 * @return the currencyPayment property.
	 */
	public String getCurrencyPayment() {
		return this.currencyPayment;
	}

	/**
	 * Setter for the currencyPayment property.
	 *
	 * @param currencyPayment
	 *            the currencyPayment to set.
	 */
	public void setCurrencyPayment(final String currencyPayment) {
		this.currencyPayment = currencyPayment;
	}

	/**
	 * Getter for the dateAssessed property.
	 *
	 * @return the dateAssessed property.
	 */
	public Date getDateAssessed() {
		return this.dateAssessed;
	}

	/**
	 * Setter for the dateAssessed property.
	 *
	 * @param dateAssessed
	 *            the dateAssessed to set.
	 */
	public void setDateAssessed(final Date dateAssessed) {
		this.dateAssessed = dateAssessed;
	}

	/**
	 * Getter for the dateUsedForMcBudget property.
	 *
	 * @return the dateUsedForMcBudget property.
	 */
	public Date getDateUsedForMcBudget() {
		return this.dateUsedForMcBudget;
	}

	/**
	 * Setter for the dateUsedForMcBudget property.
	 *
	 * @param dateUsedForMcBudget
	 *            the dateUsedForMcBudget to set.
	 */
	public void setDateUsedForMcBudget(final Date dateUsedForMcBudget) {
		this.dateUsedForMcBudget = dateUsedForMcBudget;
	}

	/**
	 * Getter for the dateUsedForMcPayment property.
	 *
	 * @return the dateUsedForMcPayment property.
	 */
	public Date getDateUsedForMcPayment() {
		return this.dateUsedForMcPayment;
	}

	/**
	 * Setter for the dateUsedForMcPayment property.
	 *
	 * @param dateUsedForMcPayment
	 *            the dateUsedForMcPayment to set.
	 */
	public void setDateUsedForMcPayment(final Date dateUsedForMcPayment) {
		this.dateUsedForMcPayment = dateUsedForMcPayment;
	}

	/**
	 * Getter for the dateFrom property.
	 *
	 * @return the dateFrom property.
	 */
	public Date getDateFrom() {
		return this.dateFrom;
	}

	/**
	 * Setter for the dateFrom property.
	 *
	 * @param dateFrom
	 *            the dateFrom to set.
	 */
	public void setDateFrom(final Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	/**
	 * Getter for the dateTo property.
	 *
	 * @return the dateTo property.
	 */
	public Date getDateTo() {
		return this.dateTo;
	}

	/**
	 * Setter for the dateTo property.
	 *
	 * @param dateTo
	 *            the dateTo to set.
	 */
	public void setDateTo(final Date dateTo) {
		this.dateTo = dateTo;
	}

	/**
	 * Getter for the exchangeRateBudget property.
	 *
	 * @return the exchangeRateBudget property.
	 */
	public double getExchangeRateBudget() {
		return this.exchangeRateBudget;
	}

	/**
	 * Setter for the exchangeRateBudget property.
	 *
	 * @param exchangeRateBudget
	 *            the exchangeRateBudget to set.
	 */
	public void setExchangeRateBudget(final double exchangeRateBudget) {
		this.exchangeRateBudget = exchangeRateBudget;
	}

	/**
	 * Getter for the exchangeRateOverride property.
	 *
	 * @return the exchangeRateOverride property.
	 */
	public double getExchangeRateOverride() {
		return this.exchangeRateOverride;
	}

	/**
	 * Setter for the exchangeRateOverride property.
	 *
	 * @param exchangeRateOverride
	 *            the exchangeRateOverride to set.
	 */
	public void setExchangeRateOverride(final double exchangeRateOverride) {
		this.exchangeRateOverride = exchangeRateOverride;
	}

	/**
	 * Getter for the exchangeRatePayment property.
	 *
	 * @return the exchangeRatePayment property.
	 */
	public double getExchangeRatePayment() {
		return this.exchangeRatePayment;
	}

	/**
	 * Setter for the exchangeRatePayment property.
	 *
	 * @param exchangeRatePayment
	 *            the exchangeRatePayment to set.
	 */
	public void setExchangeRatePayment(final double exchangeRatePayment) {
		this.exchangeRatePayment = exchangeRatePayment;
	}

	/**
	 * Getter for the description property.
	 *
	 * @return the description property.
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Setter for the description property.
	 *
	 * @param description
	 *            the description to set.
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Getter for the doc property.
	 *
	 * @return the doc property.
	 */
	public String getDoc() {
		return this.doc;
	}

	/**
	 * Setter for the doc property.
	 *
	 * @param doc
	 *            the doc to set.
	 */
	public void setDoc(final String doc) {
		this.doc = doc;
	}

	/**
	 * Getter for the issuedBy property.
	 *
	 * @return the issuedBy property.
	 */
	public String getIssuedBy() {
		return this.issuedBy;
	}

	/**
	 * Setter for the issuedBy property.
	 *
	 * @param issuedBy
	 *            the issuedBy to set.
	 */
	public void setIssuedBy(final String issuedBy) {
		this.issuedBy = issuedBy;
	}

	/**
	 * Getter for the locationId property.
	 *
	 * @return the locationId property.
	 */
	public Integer getLocationId() {
		return this.locationId;
	}

	/**
	 * Setter for the locationId property.
	 *
	 * @param locationId
	 *            the locationId to set.
	 */
	public void setLocationId(final Integer locationId) {
		this.locationId = locationId;
	}

	/**
	 * Getter for the penalty property.
	 *
	 * @return the penalty property.
	 */
	public String getPenalty() {
		return this.penalty;
	}

	/**
	 * Setter for the penalty property.
	 *
	 * @param penalty
	 *            the penalty to set.
	 */
	public void setPenalty(final String penalty) {
		this.penalty = penalty;
	}

	/**
	 * Getter for the regProgram property.
	 *
	 * @return the regProgram property.
	 */
	public String getRegProgram() {
		return this.regProgram;
	}

	/**
	 * Setter for the regProgram property.
	 *
	 * @param regProgram
	 *            the regProgram to set.
	 */
	public void setRegProgram(final String regProgram) {
		this.regProgram = regProgram;
	}

	/**
	 * Getter for the regRequirement property.
	 *
	 * @return the regRequirement property.
	 */
	public String getRegRequirement() {
		return this.regRequirement;
	}

	/**
	 * Setter for the regRequirement property.
	 *
	 * @param regRequirement
	 *            the regRequirement to set.
	 */
	public void setRegRequirement(final String regRequirement) {
		this.regRequirement = regRequirement;
	}

	/**
	 * Getter for the regulation property.
	 *
	 * @return the regulation property.
	 */
	public String getRegulation() {
		return this.regulation;
	}

	/**
	 * Setter for the regulation property.
	 *
	 * @param regulation
	 *            the regulation to set.
	 */
	public void setRegulation(final String regulation) {
		this.regulation = regulation;
	}

	/**
	 * Getter for the severity property.
	 *
	 * @return the severity property.
	 */
	public int getSeverity() {
		return this.severity;
	}

	/**
	 * Setter for the severity property.
	 *
	 * @param severity
	 *            the severity to set.
	 */
	public void setSeverity(final int severity) {
		this.severity = severity;
	}

	/**
	 * Getter for the status property.
	 *
	 * @return the status property.
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 * Setter for the status property.
	 *
	 * @param status
	 *            the status to set.
	 */
	public void setStatus(final String status) {
		this.status = status;
	}

	/**
	 * Getter for the summary property.
	 *
	 * @return the summary property.
	 */
	public String getSummary() {
		return this.summary;
	}

	/**
	 * Setter for the summary property.
	 *
	 * @param summary
	 *            the summary to set.
	 */
	public void setSummary(final String summary) {
		this.summary = summary;
	}

	/**
	 * Getter for the vatAmountOverride property.
	 *
	 * @return the vatAmountOverride property.
	 */
	public double getVatAmountOverride() {
		return this.vatAmountOverride;
	}

	/**
	 * Setter for the vatAmountOverride property.
	 *
	 * @param vatAmountOverride
	 *            the vatAmountOverride to set.
	 */
	public void setVatAmountOverride(final double vatAmountOverride) {
		this.vatAmountOverride = vatAmountOverride;
	}

	/**
	 * Getter for the vatPercentOverride property.
	 *
	 * @return the vatPercentOverride property.
	 */
	public double getVatPercentOverride() {
		return this.vatPercentOverride;
	}

	/**
	 * Setter for the vatPercentOverride property.
	 *
	 * @param vatPercentOverride
	 *            the vatPercentOverride to set.
	 */
	public void setVatPercentOverride(final double vatPercentOverride) {
		this.vatPercentOverride = vatPercentOverride;
	}

	/**
	 * Getter for the vatPercentValue property.
	 *
	 * @return the vatPercentValue property.
	 */
	public double getVatPercentValue() {
		return this.vatPercentValue;
	}

	/**
	 * Setter for the vatPercentValue property.
	 *
	 * @param vatPercentValue
	 *            the vatPercentValue to set.
	 */
	public void setVatPercentValue(final double vatPercentValue) {
		this.vatPercentValue = vatPercentValue;
	}

	/**
	 * Getter for the violationId property.
	 *
	 * @return the violationId property.
	 */
	public String getViolationId() {
		return this.violationId;
	}

	/**
	 * Setter for the violationId property.
	 *
	 * @param violationId
	 *            the violationId to set.
	 */
	public void setViolationId(final String violationId) {
		this.violationId = violationId;
	}

	/**
	 * Getter for the violationType property.
	 *
	 * @return the violationType property.
	 */
	public String getViolationType() {
		return this.violationType;
	}

	/**
	 * Setter for the violationType property.
	 *
	 * @param violationType
	 *            the violationType to set.
	 */
	public void setViolationType(final String violationType) {
		this.violationType = violationType;
	}

}
