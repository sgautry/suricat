package com.archibus.app.common.finance.dao.datasource;

import java.util.List;

import com.archibus.app.common.finance.dao.IComplianceContractCostDao;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.datasource.restriction.Restrictions;
import com.archibus.datasource.restriction.Restrictions.Restriction;
import com.archibus.db.ViewField;

/**
 * Class for Compliance Contract Cost DataSource.
 * <p>
 *
 * @author Razvan Croitoru
 *
 * @param <ComplianceContractCost>
 *            Type of persistent object.
 */
public class ComplianceContractCostDataSource<ComplianceContractCost> extends
		ObjectDataSourceImpl<ComplianceContractCost> implements IComplianceContractCostDao<ComplianceContractCost> {

	/**
	 * Field names to property names mapping. All fields will be added to the
	 * DataSource.
	 * <p>
	 * Fields common for all Cost DataSources are specified here.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { "assigned_role", "assignedRole" },
			{ "cost_cat_id", "costCatId" }, { "cost_code", "id" }, { "cost_comm_base_budget", "costCommBaseBudget" },
			{ "cost_comm_base_payment", "costCommBasePayment" }, { "cost_comm_budget", "costCommBudget" },
			{ "cost_comm_total_payment", "costCommTotalPayment" }, { "cost_comm_vat_budget", "costCommVatBudget" },
			{ "cost_comm_vat_payment", "costCommVatPayment" }, { "cost_max_comm_base_budget", "costMaxCommBaseBudget" },
			{ "cost_max_comm_base_payment", "costMaxCommBasePayment" }, { "cost_max_comm_budget", "costMaxCommBudget" },
			{ "cost_max_comm_total_payment", "costMaxCommTotalPayment" },
			{ "cost_max_comm_vat_budget", "costMaxCommVatBudget" },
			{ "cost_max_comm_vat_payment", "costMaxCommVatPayment" }, { "cost_max_desc", "costMaxDesc" },
			{ "cost_unit_comm_base_budget", "costUnitCommBaseBudget" },
			{ "cost_unit_comm_base_payment", "costUnitCommBasePayment" },
			{ "cost_unit_comm_budget", "costUnitCommBudget" },
			{ "cost_unit_comm_total_payment", "costUnitCommTotalPayment" },
			{ "cost_unit_comm_vat_budget", "costUnitCommVatBudget" },
			{ "cost_unit_comm_vat_payment", "costUnitCommVatPayment" }, { "ctry_id", "ctryId" },
			{ "currency_budget", "currencyBudget" }, { "currency_payment", "currencyPayment" },
			{ "date_used_for_mc_budget", "dateUsedForMcBudget" },
			{ "date_used_for_mc_payment", "dateUsedForMcPayment" }, { "description", "description" },
			{ "exchange_rate_budget", "exchangeRateBudget" }, { "exchange_rate_override", "exchangeRateOverride" },
			{ "exchange_rate_payment", "exchangeRatePayment" }, { "hours_max", "hoursMax" },
			{ "hours_max_desc", "hoursMaxDesc" }, { "payment_comm_base_budget", "paymentCommBaseBudget" },
			{ "payment_comm_base_payment", "paymentCommBasePayment" }, { "payment_comm_budget", "paymentCommBudget" },
			{ "payment_comm_total_payment", "paymentCommTotalPayment" },
			{ "payment_comm_vat_budget", "paymentCommVatBudget" },
			{ "payment_comm_vat_payment", "paymentCommVatPayment" }, { "payment_deferrable", "paymentDeferrable" },
			{ "payment_frequency", "paymentFrequency" }, { "prob_type", "probType" },
			{ "rate_double_base_budget", "rateDoubleBaseBudget" },
			{ "rate_double_base_payment", "rateDoubleBasePayment" }, { "rate_double_budget", "rateDoubleBudget" },
			{ "rate_double_total_payment", "rateDoubleTotalPayment" },
			{ "rate_double_vat_budget", "rateDoubleVatBudget" }, { "rate_double_vat_payment", "rateDoubleVatPayment" },
			{ "rate_hourly_base_budget", "rateHourlyBaseBudget" },
			{ "rate_hourly_base_payment", "rateHourlyBasePayment" }, { "rate_hourly_budget", "rateHourlyBudget" },
			{ "rate_hourly_total_payment", "rateHourlyTotalPayment" },
			{ "rate_hourly_vat_budget", "rateHourlyVatBudget" }, { "rate_hourly_vat_payment", "rateHourlyVatPayment" },
			{ "rate_over_base_budget", "rateOverBaseBudget" }, { "rate_over_base_payment", "rateOverBasePayment" },
			{ "rate_over_budget", "rateOverBudget" }, { "rate_over_total_payment", "rateOverTotalPayment" },
			{ "rate_over_vat_budget", "rateOverVatBudget" }, { "rate_over_vat_payment", "rateOverVatPayment" },
			{ "reg_program", "regProgram" }, { "reg_requirement", "regRequirement" }, { "regulation", "regulation" },
			{ "serv_window_days", "servWindowDays" }, { "serv_window_months", "servWindowMonths" }, { "tr_id", "trId" },
			{ "unit_count", "unitCount" }, { "vat_amount_override", "vatAmountOverride" },
			{ "vat_percent_override", "vatPercentOverride" }, { "vat_percent_value", "vatPercentValue" },
			{ "work_type", "workType" } };

	/**
	 * Constant: SQL keyword: " OR ".
	 */
	private static final String SQL_KEYWORD_OR = " OR ";

	/**
	 * table, using <code>beanName</code> bean.
	 *
	 *
	 */
	public ComplianceContractCostDataSource() {
		super("complianceContractCost", "compliance_contract_cost");
		setApplyVpaRestrictions(false);
	}

	/** {@inheritDoc} */
	@Override
	public String createSqlRestrictionForCosts(final List<Integer> costIds) {
		final StringBuffer sql = new StringBuffer();

		final DataSourceImpl dataSource = (DataSourceImpl) this.createCopy();
		dataSource.checkSetContext();

		final ViewField.Immutable firstPkField = dataSource.getPrimaryKeyFields().get(0);

		for (int i = 0; i < costIds.size(); i++) {
			final Object costId = costIds.get(i).toString();

			if (i > 0) {
				sql.append(SQL_KEYWORD_OR);
			}

			sql.append(firstPkField.getName());
			sql.append(" = ");
			sql.append(costId);
		}

		return sql.toString();
	}

	/** {@inheritDoc} */
	@Override
	public List<ComplianceContractCost> findByCostIds(final List<Integer> costIds) {
		final DataSourceImpl dataSource = (DataSourceImpl) this.createCopy();
		dataSource.setApplyVpaRestrictions(false);
		dataSource.checkSetContext();

		final String costIdRestriction = createSqlRestrictionForCosts(costIds);
		final List<DataRecord> records = dataSource.getRecords(costIdRestriction);

		return new DataSourceObjectConverter<ComplianceContractCost>().convertRecordsToObjects(records, this.beanName,
				this.fieldToPropertyMapping, null);
	}

	/** {@inheritDoc} */
	@Override
	public List<ComplianceContractCost> findByRestriction(final Restriction restriction) {
		final DataSource dataSource = this.createCopy();
		dataSource.setApplyVpaRestrictions(false);
		dataSource.addRestriction(restriction);
		final List<DataRecord> records = dataSource.getRecords();

		return new DataSourceObjectConverter<ComplianceContractCost>().convertRecordsToObjects(records, this.beanName,
				this.fieldToPropertyMapping, null);
	}

	/** {@inheritDoc} */
	@Override
	public ComplianceContractCost getRecord(final int costId) {
		final DataSourceImpl dataSource = (DataSourceImpl) this.createCopy();
		// Kb 3047002 Don't apply VPA restriction
		dataSource.setApplyVpaRestrictions(false);
		dataSource.checkSetContext();

		final ViewField.Immutable firstPkField = dataSource.getPrimaryKeyFields().get(0);
		dataSource.addRestriction(Restrictions.eq(this.tableName, firstPkField.getName(), costId));
		final DataRecord record = dataSource.getRecord();
		return new DataSourceObjectConverter<ComplianceContractCost>().convertRecordToObject(record, this.beanName,
				this.fieldToPropertyMapping, null);
	}

	@Override
	protected String[][] getFieldsToProperties() {
		return FIELDS_TO_PROPERTIES.clone();
	}
}
