package com.archibus.app.common.mobile.sync.dao.datasource;

import java.util.*;

import org.apache.commons.lang.StringUtils;

import com.archibus.app.common.mobile.sync.dao.IDocumentVersionDao;
import com.archibus.app.common.mobile.sync.domain.DocumentVersion;
import com.archibus.app.common.mobile.sync.service.Record;
import com.archibus.config.Project;
import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.schema.*;
import com.archibus.utility.ExceptionBase;

/**
 *
 * Provides Access to document version data.
 *
 * Managed by Spring, has singleton scope.
 *
 * @author jmartin
 * @since 23.1
 *
 */
public class DocumentVersionDataSource extends ObjectDataSourceImpl<DocumentVersion> implements IDocumentVersionDao {

	/**
	 * Constant: field name: "table_name".
	 */
	private static final String TABLE_NAME = "table_name";

	/**
	 * Constant: field name: "pkey_value".
	 */
	private static final String PKEY_VALUE = "pkey_value";

	/**
	 * Constant: field name: "doc_file".
	 */
	private static final String DOC_FILE = "doc_file";

	/**
	 * Constant: field name: "field_name".
	 */
	private static final String FIELD_NAME = "field_name";

	/**
	 * Constant: table name: "afm_docvers".
	 */
	private static final String AFM_DOCVERS = "afm_docvers";

	/**
	 * Constant: table name: "version".
	 */
	private static final String VERSION = "version";

	/**
	 * Constant: field name: "pkey_fields".
	 */
	private static final String PKEY_FIELDS = "pkey_fields";

	/**
	 * Constant: SQL dot.
	 */
	private static final String SQL_DOT = ".";

	/**
	 * Constant: Pipe character.
	 */
	private static final String PIPE = "|";

	/**
	 * /** Field names to property names mapping. All fields will be added to
	 * the DataSource.
	 */
	private static final String[][] FIELDS_TO_PROPERTIES = { { TABLE_NAME, "tableName" }, { PKEY_VALUE, "pkeyValue" },
			{ FIELD_NAME, "fieldName" }, { VERSION, VERSION }, { DOC_FILE, "docFile" } };

	/**
	 * Current project.
	 */
	private Project.Immutable project;

	/**
	 *
	 * DocumentVersionDataSource constructor.
	 *
	 */
	protected DocumentVersionDataSource() {
		super("documentVersion", AFM_DOCVERS);
	}

	@Override
	protected String[][] getFieldsToProperties() {
		return FIELDS_TO_PROPERTIES.clone();
	}

	/**
	 * Returns the maximum document version for the specified document.
	 *
	 * @param tableName
	 *            the table containing the document field.
	 * @param fieldName
	 *            the name of the document field.
	 * @param pkeyValue
	 *            the primary key value of the document record.
	 * @return the DataRecord containing the version data.
	 * @throws ExceptionBase
	 *             exception.
	 */
	private DataRecord getMaxDocumentVersion(final String tableName, final String fieldName, final String pkeyValue)
			throws ExceptionBase {

		SqlUtils.formatValueForSql(tableName);
		SqlUtils.formatValueForSql(fieldName);
		SqlUtils.formatValueForSql(pkeyValue);

		final String whereClause = " WHERE table_name=" + SqlUtils.formatValueForSql(tableName) + " AND field_name="
				+ SqlUtils.formatValueForSql(fieldName) + " AND " + PKEY_VALUE + "="
				+ SqlUtils.formatValueForSql(pkeyValue);

		final String query = "SELECT version,doc_file FROM afm_docvers " + whereClause + " AND version = ("
				+ "SELECT MAX(version) FROM afm_docvers " + whereClause + ")";

		final String[] fields = { VERSION, DOC_FILE };

		final DataSource dataSource = DataSourceFactory.createDataSourceForFields(AFM_DOCVERS, fields);
		dataSource.addQuery(query);

		final DataRecord record = dataSource.getRecord();

		return record;
	}

	@Override
	public Record getDocumentVersion(final String tableName, final String fieldName, final String pkeyValue) {

		final DataRecord dataRecord = this.getMaxDocumentVersion(tableName, fieldName, pkeyValue);

		if (dataRecord == null) {
			return null;
		} else {
			final TableDef.ThreadSafe tableDef = this.getProject().loadTableDef(tableName);
			final List<String> primaryKeyFields = new ArrayList<String>();

			for (final ArchibusFieldDefBase.Immutable fieldDef : tableDef.getPrimaryKey().getFields()) {
				primaryKeyFields.add(fieldDef.getName());
			}

			final String delimitedKeyFields = StringUtils.join(primaryKeyFields, PIPE);

			final int docVersion = dataRecord.getInt(AFM_DOCVERS + SQL_DOT + VERSION);
			final String docFile = (String) dataRecord.getValue(AFM_DOCVERS + SQL_DOT + DOC_FILE);

			final Record versionRecord = new Record();
			versionRecord.addOrSetFieldValue(TABLE_NAME, tableName);
			versionRecord.addOrSetFieldValue(FIELD_NAME, fieldName);
			versionRecord.addOrSetFieldValue(PKEY_VALUE, pkeyValue);
			versionRecord.addOrSetFieldValue(VERSION, docVersion);
			versionRecord.addOrSetFieldValue(DOC_FILE, docFile);
			versionRecord.addOrSetFieldValue(PKEY_FIELDS, delimitedKeyFields);

			return versionRecord;
		}

	}

	@Override
	public List<Record> getMaxDocumentVersionsForTable(final String tableName, final List<Record> records) {

		final TableDef.ThreadSafe tableDef = this.getProject().loadTableDef(tableName);

		final List<String> primaryKeyFields = new ArrayList<String>();

		for (final ArchibusFieldDefBase.Immutable fieldDef : tableDef.getPrimaryKey().getFields()) {
			primaryKeyFields.add(fieldDef.getName());
		}

		final String delimitedKeyFields = StringUtils.join(primaryKeyFields, PIPE);

		// dtoRecords to return to the client
		final List<Record> dtoRecords = new ArrayList<Record>();

		for (final Record record : records) {
			final String docTableName = (String) record.findValueForFieldName(TABLE_NAME);
			final String docFieldName = (String) record.findValueForFieldName(FIELD_NAME);
			final String docPkeyValue = (String) record.findValueForFieldName(PKEY_VALUE);

			final int docClientVersion = ((Double) record.findValueForFieldName(VERSION)).intValue();

			final DataRecord docVersionRecord = this.getMaxDocumentVersion(docTableName, docFieldName, docPkeyValue);

			if (docVersionRecord != null) {
				final int docVersion = docVersionRecord.getInt(AFM_DOCVERS + SQL_DOT + VERSION);
				final String docFile = (String) docVersionRecord.getValue(AFM_DOCVERS + SQL_DOT + DOC_FILE);

				if (docVersion > docClientVersion) {
					final Record versionRecord = new Record();
					versionRecord.addOrSetFieldValue(TABLE_NAME, docTableName);
					versionRecord.addOrSetFieldValue(FIELD_NAME, docFieldName);
					versionRecord.addOrSetFieldValue(PKEY_VALUE, docPkeyValue);
					versionRecord.addOrSetFieldValue(VERSION, docVersion);
					versionRecord.addOrSetFieldValue(DOC_FILE, docFile);
					versionRecord.addOrSetFieldValue(PKEY_FIELDS, delimitedKeyFields);
					dtoRecords.add(versionRecord);
				}
			}
		}

		return dtoRecords;
	}

	/**
	 * Getter for the project property.
	 *
	 * @return the project property.
	 */
	public Project.Immutable getProject() {
		return this.project;
	}

	/**
	 * Setter for the project property.
	 *
	 * @param project
	 *            the project to set.
	 */
	public void setProject(final Project.Immutable project) {
		this.project = project;
	}
}
