package com.archibus.app.common.mobile.sync.domain;

/**
 * Domain class for the DocumentVersion data source.
 *
 * @author jmartin
 * @since 23.1
 *
 */
public class DocumentVersion {

	/**
	 * DocumentVersion Table Name.
	 */
	private String tableName;

	/**
	 * DocumentVersion Primary key values.
	 */
	private String pkeyValue;

	/**
	 * DocumentVersion Document File Name.
	 */
	private String docFile;

	/**
	 * DocumentVersion Field Name.
	 */
	private String fieldName;

	/**
	 * DocumentVersion Field Name.
	 */
	private int version;

	/**
	 * Getter for the tableName property.
	 *
	 * @see tableName
	 * @return the tableName property.
	 */
	public String getTableName() {
		return this.tableName;
	}

	/**
	 * Setter for the tableName property.
	 *
	 * @see tableName
	 * @param tableName
	 *            the tableName to set
	 */

	public void setTableName(final String tableName) {
		this.tableName = tableName;
	}

	/**
	 * Getter for the pkeyValue property.
	 *
	 * @see pkeyValue
	 * @return the pkeyValue property.
	 */
	public String getPkeyValue() {
		return this.pkeyValue;
	}

	/**
	 * Setter for the pkeyValue property.
	 *
	 * @see pkeyValue
	 * @param pkeyValue
	 *            the pkeyValue to set
	 */

	public void setPkeyValue(final String pkeyValue) {
		this.pkeyValue = pkeyValue;
	}

	/**
	 * Getter for the docFile property.
	 *
	 * @see docFile
	 * @return the docFile property.
	 */
	public String getDocFile() {
		return this.docFile;
	}

	/**
	 * Setter for the docFile property.
	 *
	 * @see docFile
	 * @param docFile
	 *            the docFile to set
	 */

	public void setDocFile(final String docFile) {
		this.docFile = docFile;
	}

	/**
	 * Getter for the fieldName property.
	 *
	 * @see fieldName
	 * @return the fieldName property.
	 */
	public String getFieldName() {
		return this.fieldName;
	}

	/**
	 * Setter for the fieldName property.
	 *
	 * @see fieldName
	 * @param fieldName
	 *            the fieldName to set
	 */

	public void setFieldName(final String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * Getter for the version property.
	 *
	 * @see version
	 * @return the version property.
	 */
	public int getVersion() {
		return this.version;
	}

	/**
	 * Setter for the version property.
	 *
	 * @see version
	 * @param version
	 *            the version to set
	 */

	public void setVersion(final int version) {
		this.version = version;
	}

}
