package com.archibus.app.common.security.dao.datasource;

import java.sql.Time;
import java.util.*;

import com.archibus.app.common.security.domain.PasswordResetToken;
import com.archibus.app.common.util.DateTimeUtilities;
import com.archibus.datasource.DataSourceObjectConverter;
import com.archibus.datasource.data.DataRecord;
import com.archibus.utility.ExceptionBase;

/**
 * Provides custom property to fields mapping for PasswordResetToken.
 * <p>
 * Maps expiration_date and expiration_time fields to expirationTime property: splits/merges date
 * and time values into a single value.
 * <p>
 *
 * @author Valery Tydykov
 * @since 23.2
 *
 */
public class PasswordResetTokenObjectConverter
        extends DataSourceObjectConverter<PasswordResetToken> {

    /**
     * Constant: "." separator for table and field names.
     */
    private static final String DOT = ".";

    /**
     * Constant: full field name for "expiration_time".
     */
    private static final String EXPIRATION_TIME = PasswordResetTokenDataSource.AFM_PASSWORD_TOKEN
            + DOT + PasswordResetTokenDataSource.EXPIRATION_TIME;

    /**
     * Constant: full field name for "expiration_date".
     */
    private static final String EXPIRATION_DATE = PasswordResetTokenDataSource.AFM_PASSWORD_TOKEN
            + DOT + PasswordResetTokenDataSource.EXPIRATION_DATE;

    @Override
    public void convertObjectToRecord(final PasswordResetToken bean, final DataRecord record,
            final Map<String, String> fieldToPropertyMapping,
            final boolean includeAutonumberedFields) throws ExceptionBase {
        super.convertObjectToRecord(bean, record, fieldToPropertyMapping,
            includeAutonumberedFields);

        // split date and time value into date, time values
        final Date expirationTime = bean.getExpirationTime();
        record.setValue(EXPIRATION_DATE, DateTimeUtilities.toDate(expirationTime));
        record.setValue(EXPIRATION_TIME, DateTimeUtilities.toTime(expirationTime));
    }

    @Override
    public void convertObjectToRecord(final PasswordResetToken bean,
            final PasswordResetToken oldBean, final DataRecord record,
            final Map<String, String> fieldToPropertyMapping,
            final boolean includeAutonumberedFields) throws ExceptionBase {
        // updating existing record is not supported
        throw new UnsupportedOperationException();
    }

    @Override
    public PasswordResetToken convertRecordToObject(final DataRecord record, final String beanName,
            final Map<String, String> fieldToPropertyMapping, final AfterCreateCallback callback)
            throws ExceptionBase {
        final PasswordResetToken bean =
                super.convertRecordToObject(record, beanName, fieldToPropertyMapping, callback);

        if (record.valueExists(EXPIRATION_DATE) && record.valueExists(EXPIRATION_TIME)) {
            // merge date and time value into date + time value
            final java.sql.Date expirationDate = (java.sql.Date) record.getValue(EXPIRATION_DATE);
            final java.sql.Time expirationTime = (Time) record.getValue(EXPIRATION_TIME);

            bean.setExpirationTime(
                DateTimeUtilities.mergeDateAndTime(expirationDate, expirationTime));
        }

        return bean;
    }
}
