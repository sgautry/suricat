package com.archibus.datasource.cascade.loader;

import java.util.*;

import com.archibus.context.ContextStore;
import com.archibus.datasource.cascade.CascadeHandler;
import com.archibus.datasource.cascade.loader.tabletree.*;
import com.archibus.datasource.cascade.sqlbuilder.*;
import com.archibus.datasource.data.DataValue;
import com.archibus.schema.*;

/**
 * 
 * Provides methods to load the cascade tables for update cascade. It will also generate the SQL
 * commands.
 * 
 * 
 * @author Catalin Purice
 * @since 20.3
 * 
 */
public class TablesLoaderUpdateImpl extends AbstractTablesLoader implements TablesLoader {
    
    /**
     * Constructor.
     * 
     * @param cascadeMan Cascade Manager
     * @param
     */
    public TablesLoaderUpdateImpl(final CascadeHandler cascadeMan) {
        super(cascadeMan, new CascadeTableDefImpl(cascadeMan.getParentTableName(),
            cascadeMan.getChangedFields()));
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void processCascadeTables() {
        
        if (this.log.isDebugEnabled()) {
            this.log.debug("Cascade Handler: TablesLoaderDeleteImpl.loadChildren(CascadeTableDef) "
                    + "--> Loading tables");
        }
        
        loadChildren(getRootTable());
        
        if (this.log.isDebugEnabled()) {
            this.log.debug("Cascade Handler: TablesLoaderDeleteImpl.loadChildren(CascadeTableDef)"
                    + " --> Tables loaded successfully.");
            this.log
                .debug("Cascade Handler: TablesLoaderDeleteImpl.buildSqlCommands(CascadeTableDef) "
                        + "--> Sql commands loaded successfully.");
        }
        
        buildSqlCommands(getRootTable());
    }
    
    @Override
    boolean hasDependentChildren(final CascadeTableDef childTableDef) {
        return childTableDef.hasPrimaryKeyReference();
    }
    
    @Override
    void saveSqlCommands(final CascadeTableDef cascadeTable,
            final SqlCommandsBuilder sqlCommandsObject) {

        cascadeTable
            .setSqlCommand(((CascadeUpdateCommandsImpl) sqlCommandsObject).getSqlStatements());
    }
    
    @Override
    SqlCommandsBuilder createCommandsBuilder(final CascadeTableDef childTable,
            final SqlRestriction sqlRestriction) {
        return new CascadeUpdateCommandsImpl(childTable, sqlRestriction,
            getCascManager().isMergePrimaryKey());
    }

    /** {@inheritDoc} */
    @Override
    CascadeTableDef buildCascadeTable(final CascadeTableDef tableNode, final String table) {

        final List<DataValue> changedFields = new ArrayList<DataValue>();

        final TableDef.ThreadSafe tableDefn = ContextStore.get().getProject().loadTableDef(table);

        final ForeignKey.Immutable fKey =
                tableDefn.findForeignKeyByReferenceTable(tableNode.getName());

        for (final DataValue fieldValue : tableNode.getChangedPkFields()) {
            for (final String pKeyName : fKey.getPrimaryColumns()) {
                if (pKeyName.equalsIgnoreCase(fieldValue.getFieldDef().getName())) {
                    changedFields.add(fieldValue);
                }
            }
        }

        return new CascadeTableDefImpl(table, changedFields);
    }

}
