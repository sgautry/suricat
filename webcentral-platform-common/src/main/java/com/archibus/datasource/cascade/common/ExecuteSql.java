package com.archibus.datasource.cascade.common;

import java.util.List;

import com.archibus.context.ContextStore;
import com.archibus.eventhandler.EventHandlerBase;

/**
 * Provides methods that executes SQL statements on database.
 *
 * @author Catalin Purice
 *
 */
public final class ExecuteSql extends EventHandlerBase {

    /**
     * Runs the SQL command on the database.
     *
     * @param sqlCommand SQL command
     */
    public static void runCommand(final String sqlCommand) {
        executeDbSql(ContextStore.get().getEventHandlerContext(), sqlCommand, false);
    }

    /**
     * Runs the SQL commands on the database.
     *
     * @param sqlCommands SQL commands
     */
    public static void runCommands(final List<String> sqlCommands) {
        for (final String sqlCommand : sqlCommands) {
            runCommand(sqlCommand);
        }
    }
}
