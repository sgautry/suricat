package com.archibus.service.space;

import org.junit.experimental.categories.Category;

import com.archibus.core.test.fixture.category.DatabaseTest;
import com.archibus.datasource.DataSourceTestBase;

@Category({ DatabaseTest.class })
public class TestAllRoomAreaUpdate extends DataSourceTestBase {

	public void testCalculateOccupiable() {
		AllRoomAreaUpdate.calculateOccupiable();
	}

	public void testCalculateNonoccupiable() {
		AllRoomAreaUpdate.calculateNonoccupiable();
	}
}
