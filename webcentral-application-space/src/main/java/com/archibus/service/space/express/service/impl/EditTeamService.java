package com.archibus.service.space.express.service.impl;

import static com.archibus.service.space.express.util.TeamSpaceConstant.*;

import com.archibus.service.space.express.dao.*;
import com.archibus.service.space.express.domain.*;
import com.archibus.service.space.express.service.IEditTeamService;

/**
 * service for edit team.
 * <p>
 * A service class to deal with businesses in edit team module.
 * <p>
 * configured in
 * schema/ab-products/space/common/src/main/com/archibus/service/space/express/team-space-context.
 * xml
 *
 * <p>
 * Designed to have prototype scope.
 *
 * @author Jikai XU
 * @since 23.2
 *
 */
public class EditTeamService implements IEditTeamService {

    /** The team data source. */
    private ITeamDao<Team> teamDataSource;

    /** The rmteam data source. */
    private IRmTeamDao<RmTeam> rmTeamDataSource;

    @Override
    public void updateEndDateOnAssoc(final String teamId, final String dateStart,
            final String dateEnd, final String tableName) {

        if (TEAM.equals(tableName)) {

            this.teamDataSource.getLatestItemOnTeamAndUpdateEndDate(teamId, dateStart, dateEnd,
                tableName);

        } else if (RM_TEAM.equals(tableName)) {

            this.rmTeamDataSource.getLatestItemOnRmTeamAndUpdateEndDate(teamId, dateStart, dateEnd,
                tableName);
        }
    }

    /**
     * inject team data source.
     *
     * @param teamDataSource team's data source
     */
    public void setTeamDataSource(final ITeamDao<Team> teamDataSource) {
        this.teamDataSource = teamDataSource;
    }

    /**
     * inject rmteam data source.
     *
     * @param rmTeamDataSource rmteam's data source
     */
    public void setRmTeamDataSource(final IRmTeamDao<RmTeam> rmTeamDataSource) {
        this.rmTeamDataSource = rmTeamDataSource;
    }

}
