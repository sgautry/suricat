package com.archibus.service.space.express.dao;

import com.archibus.core.dao.IDao;

/**
 * Dao for team object. Mapped to <code>team</code> table.
 * <p>
 *
 * Provides interface to get the latest item for start date associated with a specific team.
 *
 * @author Jikai XU
 * @since 23.2
 *
 * @param <Team> type of the persistent object
 */
public interface ITeamDao<Team> extends IDao<Team> {

    /**
     * distribute the logic according to tables.
     *
     * @param teamId team's id
     * @param dateStart the start date
     * @param dateEnd the end date
     * @param tableName the table name which is to update
     */
    void getLatestItemOnTeamAndUpdateEndDate(final String teamId, final String dateStart,
            final String dateEnd, final String tableName);
}
