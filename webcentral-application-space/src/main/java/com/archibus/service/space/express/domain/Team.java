package com.archibus.service.space.express.domain;

/**
 * Domain object for team.
 * <p>
 *
 * Used by team association to update the date_end in team table. Managed by Spring, has prototype
 * scope. Configured in team-space-context.xml file.
 *
 * @author Jikai XU
 * @since 23.2
 *
 */
public class Team extends DateRange {

    /** The auto numbered id. */
    private int autonumberedId;

    /** The team code. */
    private String teamId;

    /**
     * Getter for the autonumberedId property.
     *
     * @see autonumberedId
     * @return the autonumberedId property.
     */
    public int getAutonumberedId() {
        return this.autonumberedId;
    }

    /**
     * Setter for the autonumberedId property.
     *
     * @see autonumberedId
     * @param autonumberedId the autonumberedId to set
     */

    public void setAutonumberedId(final int autonumberedId) {
        this.autonumberedId = autonumberedId;
    }

    /**
     * Getter for the teamId property.
     *
     * @see teamId
     * @return the teamId property.
     */
    public String getTeamId() {
        return this.teamId;
    }

    /**
     * Setter for the teamId property.
     *
     * @see teamId
     * @param teamId the teamId to set
     */

    public void setTeamId(final String teamId) {
        this.teamId = teamId;
    }
}
