package com.archibus.service.space.express.dao;

import com.archibus.core.dao.IDao;

/**
 * Dao for rmteam object. Mapped to <code>rm_team</code> table.
 * <p>
 *
 * Provides interface to get the latest item for start date associated with a specific team.
 *
 * @author Jikai XU
 * @since 23.2
 *
 * @param <RmTeam> type of the persistent object
 */
public interface IRmTeamDao<RmTeam> extends IDao<RmTeam> {

    /**
     * distribute the logic according to tables.
     *
     * @param teamId team's id
     * @param dateStart the start date
     * @param dateEnd the end date
     * @param tableName the table name which is to update
     */
    void getLatestItemOnRmTeamAndUpdateEndDate(final String teamId, final String dateStart,
            final String dateEnd, final String tableName);
}
