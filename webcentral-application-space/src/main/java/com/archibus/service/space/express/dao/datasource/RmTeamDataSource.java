package com.archibus.service.space.express.dao.datasource;

import static com.archibus.service.space.express.util.TeamSpaceConstant.*;

import java.util.List;

import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.service.space.express.dao.IRmTeamDao;
import com.archibus.service.space.express.domain.RmTeam;
import com.archibus.utility.DateTime;

/**
 * DataSource for RmTeam.
 * <p>
 * A bean class named as "rmTeamDataSource".
 * <p>
 * configured in
 * schema/ab-products/space/common/src/main/com/archibus/service/space/express/team-space-context.
 * xml
 *
 * <p>
 * Designed to have prototype scope.
 *
 * @author Jikai XU
 * @since 23.2
 *
 */
public class RmTeamDataSource extends ObjectDataSourceImpl<RmTeam> implements IRmTeamDao<RmTeam> {

    /**
     * Field names to property names mapping. All fields will be added to the DataSource.
     */
    private static final String[][] FIELDS_TO_PROPERTIES = { { "team_id", "teamId" },
            { "rm_team_id", "rmTeamId" }, { "date_end", "dateEnd" }, { "date_start", "dateStart" },
            { "bl_id", "blId" }, { "fl_id", "flId" }, { "rm_id", "rmId" } };

    /**
     * Constructs TeamDataSource, mapped to <code>rm_team</code> table.
     */
    public RmTeamDataSource() {
        super("RmTeamDataSource", "rm_team");
    }

    /**
     * distribute the logic according to tables.
     *
     * @param teamId team's id
     * @param dateStart the start date
     * @param dateEnd the end date
     * @param tableName the table name which is to update
     *
     *            SQL expression used as sub-query.
     *            <p>
     *            Suppress PMD warning "AvoidUsingSql" in this method.
     *            <p>
     *            Justification: Case #1: SQL statements with subqueries.
     */
    @SuppressWarnings("PMD.AvoidUsingSql")
    @Override
    public void getLatestItemOnRmTeamAndUpdateEndDate(final String teamId, final String dateStart,
            final String dateEnd, final String tableName) {
        final StringBuilder sql = new StringBuilder();
        sql.append("select rm_team.rm_team_id from ");
        sql.append("(select bl_id, fl_id, rm_id, max(date_start) as maxDate");
        sql.append(" from  rm_team where team_id=" + getQuotedValue(teamId));
        sql.append(" and ${sql.yearMonthDayOf('date_start')}<=" + getQuotedValue(dateEnd));
        sql.append(" and (date_end is null or ${sql.yearMonthDayOf('date_end')}>=");
        sql.append(getQuotedValue(dateStart));
        sql.append(" ) group by bl_id, fl_id, rm_id) t , rm_team");
        sql.append(
            " where t.bl_id=rm_team.bl_id and t.fl_id=rm_team.fl_id and t.rm_id=rm_team.rm_id");
        sql.append(
            " and ${sql.yearMonthDayOf('t.maxDate')}=${sql.yearMonthDayOf('rm_team.date_start')} ");
        sql.append(" and rm_team.team_id=" + getQuotedValue(teamId));

        final DataSource dataSource =
                DataSourceFactory.createDataSource().addTable(RM_TEAM, DataSource.ROLE_MAIN)
                    .addVirtualField(RM_TEAM, RM_TEAM_ID, DataSource.DATA_TYPE_INTEGER)
                    .addQuery(sql.toString());

        final List<DataRecord> records = dataSource.getAllRecords();

        for (final DataRecord record : records) {
            updateEndDate(Integer.parseInt(record.getValue("rm_team.rm_team_id").toString()),
                dateEnd);
        }
    }

    /**
     * update the end date.
     *
     * @param id primary key
     * @param dateEnd the end date
     */
    public void updateEndDate(final int id, final String dateEnd) {
        final RmTeam rmTeam = new RmTeam();
        rmTeam.setRmTeamId(id);
        rmTeam.setDateEnd(DateTime.stringToDate(dateEnd, "yyyy-MM-dd"));
        update(rmTeam);
    }

    @Override
    protected String[][] getFieldsToProperties() {
        return FIELDS_TO_PROPERTIES.clone();
    }

    /**
     * set the end date according to table.
     *
     * @param fieldValue add quotes for the value
     * @return string value with quotes
     */
    private String getQuotedValue(final String fieldValue) {
        return QUOTE + fieldValue + QUOTE;
    }

}
