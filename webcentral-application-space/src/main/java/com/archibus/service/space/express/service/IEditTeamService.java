package com.archibus.service.space.express.service;

/**
 * an interface of service for edit team.
 * <p>
 * A service class to deal with businesses in edit team module.
 * <p>
 * configured in
 * schema/ab-products/space/common/src/main/com/archibus/service/space/express/team-space-context.
 * xml
 *
 * <p>
 * Designed to have prototype scope.
 *
 * @author Jikai XU
 * @since 23.2
 *
 */
public interface IEditTeamService {

    /**
     *
     * set end date for employees/rooms.
     *
     * @param teamId team's id
     * @param dateStart the start date
     * @param dateEnd the end date
     * @param tableName the table name which is to update
     * @return
     */
    void updateEndDateOnAssoc(final String teamId, final String dateStart, final String dateEnd,
            final String tableName);
}
