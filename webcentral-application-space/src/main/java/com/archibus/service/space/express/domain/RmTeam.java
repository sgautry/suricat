package com.archibus.service.space.express.domain;

import java.util.Date;

/**
 * Domain object for rmteam.
 * <p>
 *
 * Used by team association to update the date_end in rm_team table. Managed by Spring, has
 * prototype scope. Configured in team-space-context.xml file.
 *
 * @author Jikai XU
 * @since 23.2
 *
 */
public class RmTeam extends DateRange {

    /** the room team code. */
    private int rmTeamId;

    /** the team code. */
    private String teamId;

    /** the room code. */
    private Date rmId;

    /** the building code. */
    private Date blId;

    /** the floor code. */
    private String flId;

    /**
     * Getter for the rmTeamId property.
     *
     * @see rmTeamId
     * @return the rmTeamId property.
     */
    public int getRmTeamId() {
        return this.rmTeamId;
    }

    /**
     * Setter for the rmTeamId property.
     *
     * @see rmTeamId
     * @param rmTeamId the rmTeamId to set
     */

    public void setRmTeamId(final int rmTeamId) {
        this.rmTeamId = rmTeamId;
    }

    /**
     * Getter for the teamId property.
     *
     * @see teamId
     * @return the teamId property.
     */
    public String getTeamId() {
        return this.teamId;
    }

    /**
     * Setter for the teamId property.
     *
     * @see teamId
     * @param teamId the teamId to set
     */

    public void setTeamId(final String teamId) {
        this.teamId = teamId;
    }

    /**
     * Getter for the rmId property.
     *
     * @see rmId
     * @return the rmId property.
     */
    public Date getRmId() {
        return this.rmId;
    }

    /**
     * Setter for the rmId property.
     *
     * @see rmId
     * @param rmId the rmId to set
     */

    public void setRmId(final Date rmId) {
        this.rmId = rmId;
    }

    /**
     * Getter for the blId property.
     *
     * @see blId
     * @return the blId property.
     */
    public Date getBlId() {
        return this.blId;
    }

    /**
     * Setter for the blId property.
     *
     * @see blId
     * @param blId the blId to set
     */

    public void setBlId(final Date blId) {
        this.blId = blId;
    }

    /**
     * Getter for the flId property.
     *
     * @see flId
     * @return the flId property.
     */
    public String getFlId() {
        return this.flId;
    }

    /**
     * Setter for the flId property.
     *
     * @see flId
     * @param flId the flId to set
     */

    public void setFlId(final String flId) {
        this.flId = flId;
    }
}