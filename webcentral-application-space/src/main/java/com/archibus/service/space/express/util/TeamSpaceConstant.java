package com.archibus.service.space.express.util;

/**
 *
 * Team Space's Edit Team Constant.
 *
 * @author Jikai XU
 * @since 23.1
 *
 */
public final class TeamSpaceConstant {
    /** Property name 'autonumbered_id'. */
    public static final String AUTONUMBERED_ID = "autonumbered_id";

    /** Property name 'team'. */
    public static final String TEAM = "team";

    /** Property name 'rm_team'. */
    public static final String RM_TEAM = "rm_team";

    /** Property name 'rm_team_id'. */
    public static final String RM_TEAM_ID = "rm_team_id";

    /** Property name "'". */
    public static final String QUOTE = "'";

    /**
     * private Constructor to prevent instantiation.
     */
    private TeamSpaceConstant() {

    }
}
