package com.archibus.service.space.express.domain;

import java.util.Date;

/**
 * super class for RmTeam and Team.
 * <p>
 *
 * provide dateStart and dateEnd properties for RmTeam and Team class.
 *
 * @author Jikai XU
 * @since 23.2
 *
 */
public class DateRange {

    /** the start date. */
    private Date dateStart;

    /** the end date. */
    private Date dateEnd;

    /**
     * Getter for the dateStart property.
     *
     * @see dateStart
     * @return the dateStart property.
     */
    public Date getDateStart() {
        return this.dateStart;
    }

    /**
     * Setter for the dateStart property.
     *
     * @see dateStart
     * @param dateStart the dateStart to set
     */

    public void setDateStart(final Date dateStart) {
        this.dateStart = dateStart;
    }

    /**
     * Getter for the dateEnd property.
     *
     * @see dateEnd
     * @return the dateEnd property.
     */
    public Date getDateEnd() {
        return this.dateEnd;
    }

    /**
     * Setter for the dateEnd property.
     *
     * @see dateEnd
     * @param dateEnd the dateEnd to set
     */

    public void setDateEnd(final Date dateEnd) {
        this.dateEnd = dateEnd;
    }
}
