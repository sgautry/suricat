/**
 * This package contains implementation for data sources of Team Space Edit
 * Team.
 **/
package com.archibus.service.space.express.dao.datasource;