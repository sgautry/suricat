package com.archibus.service.space.express.dao.datasource;

import static com.archibus.service.space.express.util.TeamSpaceConstant.*;

import java.util.List;

import com.archibus.datasource.*;
import com.archibus.datasource.data.DataRecord;
import com.archibus.service.space.express.dao.ITeamDao;
import com.archibus.service.space.express.domain.Team;
import com.archibus.utility.DateTime;

/**
 * DataSource for Team.
 * <p>
 * A bean class named as "teamDataSource".
 * <p>
 * configured in
 * schema/ab-products/space/common/src/main/com/archibus/service/space/express/team-space-context.
 * xml
 *
 * <p>
 * Designed to have prototype scope.
 *
 * @author Jikai XU
 * @since 23.2
 *
 */
public class TeamDataSource extends ObjectDataSourceImpl<Team> implements ITeamDao<Team> {

    /**
     * Field names to property names mapping. All fields will be added to the DataSource.
     */
    private static final String[][] FIELDS_TO_PROPERTIES =
            { { "autonumbered_id", "autonumberedId" }, { "team_id", "teamId" },
                    { "date_end", "dateEnd" }, { "date_start", "dateStart" } };

    /**
     * Constructs TeamDataSource, mapped to <code>team</code> table.
     */
    public TeamDataSource() {
        super("TeamDataSource", "team");
    }

    /**
     * get the latest item by date on team table.
     *
     * @param teamId team's id
     * @param dateStart the start date
     * @param dateEnd the end date
     * @param tableName the table name which is to update
     *
     *            SQL expression used as sub-query.
     *            <p>
     *            Suppress PMD warning "AvoidUsingSql" in this method.
     *            <p>
     *            Justification: Case #1: SQL statements with subqueries.
     */
    @SuppressWarnings("PMD.AvoidUsingSql")
    @Override
    public void getLatestItemOnTeamAndUpdateEndDate(final String teamId, final String dateStart,
            final String dateEnd, final String tableName) {
        final StringBuilder sql = new StringBuilder();
        sql.append("select team.autonumbered_id from ");
        sql.append("(select em_id, max(date_start) as maxDate ");
        sql.append(" from team where team_id=" + getQuotedValue(teamId));
        sql.append(" and ${sql.yearMonthDayOf('date_start')}<= " + getQuotedValue(dateEnd));
        sql.append(" and (date_end is null or ${sql.yearMonthDayOf('date_end')}>= "
                + getQuotedValue(dateStart));
        sql.append(") group by em_id) t , team ");
        sql.append("where t.em_id=team.em_id and ");
        sql.append("${sql.yearMonthDayOf('t.maxDate')}=${sql.yearMonthDayOf('team.date_start')}");
        sql.append(" and team.team_id=" + getQuotedValue(teamId));

        final DataSource dataSource =
                DataSourceFactory.createDataSource().addTable(TEAM, DataSource.ROLE_MAIN)
                    .addVirtualField(TEAM, AUTONUMBERED_ID, DataSource.DATA_TYPE_INTEGER)
                    .addQuery(sql.toString());

        final List<DataRecord> records = dataSource.getAllRecords();

        for (final DataRecord record : records) {
            updateEndDate(Integer.parseInt(record.getValue("team.autonumbered_id").toString()),
                dateEnd);
        }
    }

    /**
     * update the end date.
     *
     * @param id primary key
     * @param dateEnd the end date
     */
    public void updateEndDate(final int id, final String dateEnd) {
        final Team team = new Team();
        team.setAutonumberedId(id);
        team.setDateEnd(DateTime.stringToDate(dateEnd, "yyyy-MM-dd"));
        update(team);
    }

    @Override
    protected String[][] getFieldsToProperties() {
        return FIELDS_TO_PROPERTIES.clone();
    }

    /**
     * set the end date according to table.
     *
     * @param fieldValue add quotes for the value
     * @return string value with quotes
     */
    private String getQuotedValue(final String fieldValue) {
        return QUOTE + fieldValue + QUOTE;
    }
}
